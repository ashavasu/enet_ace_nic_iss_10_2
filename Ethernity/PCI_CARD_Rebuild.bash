#!/bin/bash

echo text "www.Ethernitynet.com PCIE_Bord"

export BASE_DIR=$(pwd)
export MV_DEV_LOCAL=$BASE_DIR;
export MY_PLATFORM="pcicard"

export MEA_BOARD_TYPE=linux-pc
export MV_ROOT=""
export MV_TOOL_CHAIN=""
export CROSS_COMPILE=""
export ARCH="linux"

#export MEA_NO_READLINE="yes"
export READLINE_PATH="$MV_TOOL_CHAIN/usr/include"


export MEA_DPDK="no"

export MEA_ETHERNITY="yes"
#export LINUX_PC="yes"

##################################
export MEA_ETHERNITY_MAIN="no"
export ADAP_NP_ENABLE="yes"
#################################

export MEA_TIMER_PERIODIC="yes"
#export LINUX_PC="yes"

#export AXI_TRANS="yes"
#export HW_BOARD_IS_PCI="yes"
export MEA_PORTS_128_255="yes"


if [[ `$BASE_DIR/platform_is_vm.sh` == "vm" ]];
then
echo "##############  vm  ####################### "
export MEA_OS_DEVICE_MEMORY_SIMULATION="yes"
#export HW_BOARD_IS_PCI="yes"
else
echo "############## physical ##############"
#export MEA_OS_DEVICE_MEMORY_SIMULATION="yes"
export HW_BOARD_IS_PCI="yes"
fi

export MEA_ENV_S1_K7_NO_SYSLOG="yes"
#export MEA_RPC_ENG="yes"


export MEA_EXTRA_SUBSYSTEM=L1/$MY_PLATFORM;

# TSCTL or ETHER SYSFS /*Kernel Enet*/ or SYSFS
PCI_DRIVER=SYSFS

echo text "PCI card type $PCI_DRIVER"

export GLOBAL_CC_FLAGS="-DMEA_OS_LINUX -DMEA_ETHERNITY  -m64 -D$PCI_DRIVER";



if [ "$MEA_RPC_ENG" = yes ];
then
echo text "MEA_RPC_ENG "
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS  -DMEA_RPC_ENG "
fi

if [ "$HW_BOARD_IS_PCI" = yes ];
then
echo text "HW_BOARD_IS_PCI "
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS -DHW_BOARD_IS_PCI "
fi

if [ "$MEA_OS_DEVICE_MEMORY_SIMULATION" = yes ];
then
echo text "MEA_OS_DEVICE_MEMORY_SIMULATION"
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS -DMEA_OS_DEVICE_MEMORY_SIMULATION "
fi

if [ "$MEA_ETHERNITY_MAIN" = yes ];
then
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS -DMEA_ETHERNITY_MAIN "
fi

if [ "$MEA_TIMER_PERIODIC" = yes ];
then
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS -DMEA_TIMER_PERIODIC "
fi

if [ "$LINUX_PC" = yes ];
then
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS -DLINUX_PC "
fi

if [ "$MEA_PORTS_128_255" = yes ];
then
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS -DMEA_PORTS_128_255 "
fi
if [ "$ADAP_NP_ENABLE" = yes ];
then
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS -DADAP_NP_VER_ENABLE -DLPM_ADAPTOR_WANTED -DNAT_ADAPTOR_WANTED -DIPSEC_ADAPTOR_WANTED"
fi


if [ "$BL_PLATFORM" = yes ];
then
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS -DBL_PLATFORM"
else
if [ "$NIC_PLATFORM" = yes ];
then
export GLOBAL_CC_FLAGS="$GLOBAL_CC_FLAGS -DNIC_PLATFORM"
fi
fi


echo text ">>> GLOBAL_CC_FLAGS=$GLOBAL_CC_FLAGS"

#exit











pwd;
cd $MV_DEV_LOCAL;
pwd;
echo $pwd
cd Make


if [ "$MEA_ETHERNITY_MAIN" = yes ];
then
echo " ####################### MEA_ETHERNITY_MAIN ###################################"
fi
make clean_all;

pwd;
echo $pwd
#make -j9 MEAd ;

if [ "$MEA_ETHERNITY_MAIN" = yes ];
then
echo " ####################### MEA_ETHERNITY_MAIN ###################################"
make MEAd
else
make MEALIBd
fi

pwd;
echo $pwd;


echo text " mkdir $MV_DEV_LOCAL/lib/$MY_PLATFORM "
mkdir -p $MV_DEV_LOCAL/lib/$MY_PLATFORM
if [ "$MEA_ETHERNITY_MAIN" = yes ];
then
echo text "Copy mea.ex to lib/$MY_PLATFORM/"$MY_PLATFORM"_mea.ex"

cp $MV_DEV_LOCAL/lib/L1/$MY_PLATFORM/mea.ex $MV_DEV_LOCAL/lib/$MY_PLATFORM/"$MY_PLATFORM"_mea.ex
fi
echo text "Alex-Weis cli compile "

cd $MV_DEV_LOCAL/tools/MEA_cli_client ;

make clean; 

make

if [ ! -d "$MV_DEV_LOCAL/lib/$MY_PLATFORM" ]; then
# Control will enter here if $DIRECTORY doesn't exist.
mkdir $MV_DEV_LOCAL/lib/$MY_PLATFORM
fi

cp meaCli.elf $MV_DEV_LOCAL/lib/$MY_PLATFORM/meaCli

if [ "$MEA_RPC_ENG" = yes ];
then
echo text "Alex-Weis web-server compile "
echo text "MEA_RPC_ENG >>>>>> $MEA_RPC_ENG "

echo text "create static lib remote client "
cd $MV_DEV_LOCAL/tools/MEA_remote_client/build ;

make clean; 

make

cd $MV_DEV_LOCAL/tools/MEA_web_server/build ;

make clean; 

make

fi


echo text "ENET.e compile "

cd $MV_DEV_LOCAL/tools/ENET_cli_client ;

make clean; 

make

cp ENET.e  $MV_DEV_LOCAL/lib/$MY_PLATFORM/.



if [ "$MEA_ETHERNITY_MAIN" = yes ];
then
echo "Please enter"
read name1  
exit
fi





