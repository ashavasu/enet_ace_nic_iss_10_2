MEA CLI client program.

This client program allowed the user to run CLI commands from any process on the linux ,
and not necessarly from the process running the driver.
Its done via socket # 4443 on the lo inteface 127.0.0.1 

How To Compile:
- Use the regular compile enviroment of the driver (don't care simulation or not).
- cd Ethernity/tools/MEA_cli_client ; make 


How to run the program:
Download the output file Ethernity/tools/MEA_cli_client.elf to the board , and simple run it via ssh/telent to the cpu linux on the board.
Example:  ./MEA_cli_client.elf  /MEA port ingress show all -p 

Note: We recomend to put / at the begining of the CLI command to avoid problem with the current location
      in the cli tree from other sessions.

Note: All the output cli and logs of this command will be print to the correct ssh/telnet screen.

