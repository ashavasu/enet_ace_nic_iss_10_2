#include <sys/types.h>
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>

#include <sys/time.h>

#include <signal.h>
#include <pthread.h>


#include <errno.h>  
#include <unistd.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>





#define MEA_MAGIC_NUMBER_VALUE "babeface"
#define MEA_MAGIC_NUMBER_LEN   8

typedef struct {
       char       magic_number[MEA_MAGIC_NUMBER_LEN];
       char       pad1;
       char       type;
       char       pad2;
       char       len[3];
} hdr_buf_t;



int main (int argc, char* argv[]) {


   int sock;
   struct sockaddr_in to;
   socklen_t          to_len = sizeof(to);
   hdr_buf_t hdr_buf;
   unsigned short port = 4443;
   unsigned long  host = 0x7f000001; /* 127.0.0.1 - loopback */
   char  cli_cmd[3000];
   int   cli_cmd_len;
   char  cli_out[1000];
   int   cli_out_len;
   int   rc;
   int   i;


   if (argc <= 1) {
      printf ("Usage: %s <cli cmd str>\n",argv[0]);
      printf ("Example: %s MEA debug ver\n",argv[0]);
      return -1;
   }

   cli_cmd[0]='\0';
   for (i=1;i<argc;i++) {
       if (strcmp(argv[i],"-host") == 0) {
           if (i+1 >= argc) {
               printf("Error: Missing host ip address\n");
               return -1;
           }
           i++;
           host  = inet_addr(argv[i]);
       } else 
       if (strcmp(argv[i],"-card") == 0) {
           static int card = 0;
           if (i+1 >= argc) {
               printf("Error: Missing card id\n");
               return -2;
           }
           if (card != 0) {
               printf("Error: Option -card already used\n");
               return -3;
           }
           i++;
           card = atoi(argv[i]);
           if (card <= 0) {
               printf("Error: card id should be great than zero\n");
               return -4;
           }
           port += card;
       } else {
           strcat(cli_cmd,argv[i]);
           strcat(cli_cmd," ");
       }
   }
   cli_cmd_len = strlen(cli_cmd)+1;


   /* create socket */
   if ((sock=socket(PF_INET, 
                    SOCK_STREAM, 
                    0))<0) {
        printf("socket creation failed (errno=%d '%s')\n",
               errno,
               strerror(errno));
		return -1;
	}
	


    /* create bind info for input */
	memset(&(to),'\0',sizeof(to));
    to.sin_family      = AF_INET;
    to.sin_addr.s_addr = htonl(host);
    to.sin_port = htons(port);

    if (connect (sock,(struct sockaddr*)&to,to_len) != 0) {
        printf ("connect failed (errno=%d '%s',host=0x%08lx,port=%d)\n",
                errno,
                strerror(errno),
                host,
		port);
        close(sock);
        return -1;
    }


    memset(&hdr_buf,0,sizeof(hdr_buf));
    memcpy(hdr_buf.magic_number,MEA_MAGIC_NUMBER_VALUE,sizeof(hdr_buf.magic_number));
    hdr_buf.pad1 = ' ';
    hdr_buf.pad2 = ' ';
    hdr_buf.type = '2';
    hdr_buf.len[0] = (cli_cmd_len  / 100)       + '0';
    hdr_buf.len[1] = ((cli_cmd_len % 100) / 10) + '0';
    hdr_buf.len[2] = (cli_cmd_len  %  10)       + '0';

    if (write (sock,
               (char*)&hdr_buf,
               sizeof(hdr_buf)) != sizeof (hdr_buf)) {
       printf("write hdr failed (errno=%d '%s')\n",
              errno,
              strerror(errno));
        close(sock);
        return -1;
    }

    if (write(sock,
              cli_cmd,
              cli_cmd_len) != cli_cmd_len) {
        printf ("write cmd failed (errno=%d '%s')\n",
                errno,
                strerror(errno));
        close(sock);
        return -1;
    }

    while (1) {
        rc = read (sock,
                  (char*)&hdr_buf,
                  sizeof(hdr_buf));
        if (rc != sizeof (hdr_buf)) {
            printf("read hdr failed (errno=%d '%s' , rc=%d)\n",
                   errno,
                   strerror(errno),
                   rc);
            break;
        }

#if 0
        printf ("magic= '%c' '%c' '%c' '%c' '%c' '%c' '%c' '%c' \n",
                hdr_buf.magic_number[0],
                hdr_buf.magic_number[1],
                hdr_buf.magic_number[2],
                hdr_buf.magic_number[3],
                hdr_buf.magic_number[4],
                hdr_buf.magic_number[5],
                hdr_buf.magic_number[6],
                hdr_buf.magic_number[7]);
        printf ("type = '%c'  %d\n",hdr_buf.type,hdr_buf.type);
  
        printf ("len = '%c' '%c' '%c' \n",
                hdr_buf.len[0],
                hdr_buf.len[1],
                hdr_buf.len[2]);
#endif


        if (memcmp(hdr_buf.magic_number,
                   MEA_MAGIC_NUMBER_VALUE,
                   sizeof(hdr_buf.magic_number)) != 0) {
            printf("wrong  reply - no valid magic number \n");
            break;
        }
        if (hdr_buf.type != '2') {
            printf("wrong  reply - no valid type \n");
            break;
        }
        cli_out_len = (((hdr_buf.len[0] - '0') * 100) +
                       ((hdr_buf.len[1] - '0') *  10) +
                       ((hdr_buf.len[2] - '0') *   1) );
        if (cli_out_len==0) {
            break;
        }
        rc = read (sock,
                  cli_out,
                  cli_out_len);
        if (rc != cli_out_len) {
            printf("read body failed (errno=%d '%s' , rc=%d)\n",
                   errno,
                   strerror(errno),
                   rc);
            break;
        }

		cli_out[cli_out_len] = '\0';//0;
        printf("%s",cli_out);
    }

    memset(&hdr_buf,0,sizeof(hdr_buf));
    memcpy(hdr_buf.magic_number,MEA_MAGIC_NUMBER_VALUE,sizeof(hdr_buf.magic_number));
    hdr_buf.pad1 = ' ';
    hdr_buf.pad2 = ' ';
    hdr_buf.type = '2';
    cli_cmd_len = 0;
    hdr_buf.len[0] = (cli_cmd_len  / 100)       + '0';
    hdr_buf.len[1] = ((cli_cmd_len % 100) / 10) + '0';
    hdr_buf.len[2] = (cli_cmd_len  %  10)       + '0';

    if (write (sock,
               (char*)&hdr_buf,
               sizeof(hdr_buf)) != sizeof (hdr_buf)) {
       printf("write hdr finish failed (errno=%d '%s')\n",
              errno,
              strerror(errno));
       return -1;
    }

    close(sock);


    return 0;
}
