/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: enet3000_user_space.c,v $
#
#------------------------------------------------------------------------------
# $Source: /home/ethernity/cvsroot/Ethernity/tools/enet3000_user_space/enet3000_user_space.c,v $
#------------------------------------------------------------------------------
# $Author: lior $
#------------------------------------------------------------------------------
# $Revision: 1.1 $
#------------------------------------------------------------------------------
# $Log: enet3000_user_space.c,v $
# Revision 1.1  2007/06/03 09:37:26  lior
#    New program  that run in user space and get cli commands
#    and write it to proc fifo interface and read the proc fifo file
#    and print to the screen.
#    This program is use to intreract between user space and
#    kernel space for the CLI.
#
#
#
#------------------------------------------------------------------------------
*/




/*-------------------------------- Includes ------------------------------------------*/


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "MEA_platform_os_pf.h"


#define RD_BUF_SIZE	1024

#define MEA_PROMPT_CLI_COMMAND "prompt"
char prompt_str[100];

int CLI_procFifoCmd(char* pStr)
{
        char *fileName;
        int fd;
        char inBuf[RD_BUF_SIZE];
        int wrtSize;
        int rdCnt;
        int retVal;

        fileName = MEA_OS_PF_FULL_PATH;

        /* Open The file */
        if ( (fd = open(fileName,O_RDWR)) < 0 )
        {
                printf("Error opening %s\n",fileName);
                return(-1);
        }

	wrtSize = strlen(pStr) + 1;

	retVal = write(fd,pStr,wrtSize);

	if(retVal != wrtSize)
	{
		printf("Error writing \'%s\' to %s\n",pStr,fileName);
		close(fd);
		return(-1);
	}

        while((rdCnt = read(fd,inBuf,RD_BUF_SIZE - 1)) > 0)
        {
                inBuf[rdCnt] = 0;
                if (strcmp(pStr,MEA_PROMPT_CLI_COMMAND) == 0) {
                   strncpy(prompt_str,inBuf,sizeof(prompt_str)-1);
                   prompt_str[sizeof(prompt_str)-1] = '\0';
                } else {
                  printf("%s",inBuf);
                }
        }

        close(fd);

        return(0);
}



int  main (int argc , char* argv[]) {

    char* pStr;

    printf("\n\nWelcome to FPGA CLI Environment\n\n");  

    while (1) {

       prompt_str[0]='\0';
       CLI_procFifoCmd(MEA_PROMPT_CLI_COMMAND);

       pStr=readline(prompt_str);

       if (pStr == NULL)  {
          break;
       }

       if (strcmp(pStr,"exit") == 0)  {
          free (pStr);
          break;
       }

       CLI_procFifoCmd(pStr);

       add_history (pStr);


       free(pStr);
    }
    
    printf("\nCLI exiting\n");

    return 0;

}


