# .bashrc

#--------------------------------------------
#   evaluation board
#--------------------------------------------
#export MEA_BOARD_TYPE=MPC8260ADS
#export MEA_PLAT_EVALUATION_BOARD="yes"

#--------------------------------------------
#   linux simulator
#--------------------------------------------
#export MEA_BOARD_TYPE=linux-pc
#export MEA_PLAT_EVALUATION_BOARD="yes"

#--------------------------------------------
#   freescale Xcomp for MPC8306
#--------------------------------------------
#export MEA_BOARD_TYPE=freescale
export MEA_BOARD_TYPE=ppc_6xx


#--------------------------------------------
#   The location of the directory of Ethernity and add the tools to the path
#   Note: The MV_DEV_LOCAL will be use also in the Makefiles
#--------------------------------------------

# T.B.D = Modificare con il path di Ethernity
export MV_DEV_LOCAL=$PWD
export PATH="$MV_DEV_LOCAL/tools:$PATH"


#--------------------------------------------
#   
#--------------------------------------------
export MEA_EXTRA_SUBSYSTEM=L1 

#------------------------------------------
# Defines according to the BOARD Type 
#----------------------------------------------
if [ "$MEA_BOARD_TYPE" == "linux-pc" ]; then
   export EXTRA_GLOBAL_CC_FLAGS=-DMEA_OS_DEVICE_MEMORY_SIMULATION
   export MV_ROOT=""
   export MV_TOOL_CHAIN=""
   export CROSS_COMPILE=""
elif [ "$MEA_BOARD_TYPE" == "freescale" ]; then
   export FREESCALE_COMPILER="yes"
   export MV_ROOT="/opt/freescale"
   export MV_TOOL_CHAIN="$MV_ROOT/usr/local/gcc-4.1.78-eglibc-2.5.78-1/powerpc-e300c3-linux-gnu"
   export CROSS_COMPILE="$MV_TOOL_CHAIN/bin/powerpc-e300c3-linux-gnu-"
   # N.B. = Comment the line below to work without simulation on the board
#   export EXTRA_GLOBAL_CC_FLAGS=-DMEA_OS_DEVICE_MEMORY_SIMULATION
   export READLINE_PATH="$MV_TOOL_CHAIN/target/usr/include"
elif [ "$MEA_BOARD_TYPE" == "ppc_6xx" ]; then
   export DENX_COMPILER="yes"
   export MV_ROOT="/opt/eldk-4.2"
   export MV_TOOL_CHAIN="$MV_ROOT/ppc_6xx"
   export CROSS_COMPILE="$MV_ROOT/usr/bin/ppc_6xx-"
   export ARCH="powerpc"
   # N.B. = Comment the line below to work without simulation on the board
#   export EXTRA_GLOBAL_CC_FLAGS=-DMEA_OS_DEVICE_MEMORY_SIMULATION
   # N.B. = The readline and ncurses library manually copied from the FREESCALE toolchain 
   export READLINE_PATH="$MV_TOOL_CHAIN/target/usr/include"
else
   # MONTAVISTA environment 
   export MONTAVISTA_COMPILER="yes"
   export MV_ROOT="/opt/montavista/pro"
   export MV_TOOL_CHAIN="$MV_ROOT/devkit/arm/xscale_be"
   export CROSS_COMPILE="$MV_TOOL_CHAIN/bin/xscale_be-"
   # N.B. = Comment the line below to work without simulation on the board
#   export EXTRA_GLOBAL_CC_FLAGS=-DMEA_OS_DEVICE_MEMORY_SIMULATION
# For MONTAVISTA is the same of the normal CC_INCLUDE in defs.bsp
   export READLINE_PATH="/usr/include/"
fi

#------------------------------------------
# the basic defines require for the user  
#----------------------------------------------
export GLOBAL_CC_FLAGS="-DMEA_OS_LINUX $EXTRA_GLOBAL_CC_FLAGS" 
# Comment the line below to obtain the CLI executable
#export MEA_ENV_S1_MAIN="yes"
export MEA_ENV_S1="yes"
export MEA_PLAT_S1="yes"
export MEA_S1_XILINX="yes"
export MEA_ETHERNITY="yes"
export MEA_ENV_S1_K7="yes"
export MEA_ENV_S1_K7_NO_SYSLOG="yes"


alias met="cd $MV_DEV_LOCAL/Make;make clean_all;make;cd -"

echo
echo " Setting parameters... "
echo
echo "   MEA_BOARD_TYPE=\"$MEA_BOARD_TYPE\" "
echo "   MEA_PLAT_EVALUATION_BOARD=\"$MEA_PLAT_EVALUATION_BOARD\" "
echo "   MEA_ENV_S1=\"$MEA_ENV_S1\" "
echo "   MEA_ENV_S1_MAIN=\"$MEA_ENV_S1_MAIN\" "
echo "   MEA_PLAT_S1=\"$MEA_PLAT_S1\" "
echo "   MEA_S1_XILINX=\"$MEA_S1_XILINX\" "
echo "   MEA_ETHERNITY=\"$MEA_ETHERNITY\" "
echo "   MONTAVISTA_COMPILER=\"$MONTAVISTA_COMPILER\" "
echo "   FREESCALE_COMPILER=\"$FREESCALE_COMPILER\" "
echo "   DENX_COMPILER=\"$DENX_COMPILER\" "
echo
echo "   MV_DEV_LOCAL=\"$MV_DEV_LOCAL\" "
echo "   MV_TOOL_CHAIN=\"$MV_TOOL_CHAIN\" "
echo "   CROSS_COMPILE=\"$CROSS_COMPILE\" "
echo
echo "   GLOBAL_CC_FLAGS=\"$GLOBAL_CC_FLAGS\" "
echo " - Use \"met\" command to perform a \"make clean_all\" and \"make\" "
echo









