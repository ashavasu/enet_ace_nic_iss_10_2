#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"

MEA_Status MEA_API_COMM_Get_Filter_KeyType(MEA_Unit_t unit_i,
                                      MEA_Filter_Key_dbt *key_i,
                                      MEA_Filter_Key_Type_te *keyType_o)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComFilterKey *filterKeyType;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.filterEntry.unit_i = unit_i;
	memcpy(&tMsgReq.tMsgBody.filterEntry.key,key_i,sizeof(MEA_Filter_Key_dbt));
	memcpy(&tMsgReq.tMsgBody.filterEntry.keyType,keyType_o,sizeof(MEA_Filter_Key_Type_te));


	tMsgReq.class_level_type = D_CLASS_FILTER_ENTRY;
	tMsgReq.message_level_type = D_MSG_GET_FILTER_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to MEA_API_COMM_Get_Filter_KeyType\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	filterKeyType = (tComFilterKey *)&pBobyMsgRcv->info[0];

	memcpy(key_i,&filterKeyType->key,sizeof(MEA_Filter_Key_dbt));
	memcpy(keyType_o,&filterKeyType->keyType,sizeof(MEA_Filter_Key_Type_te));

	return ret;
}


MEA_Status MEA_API_COMM_GetFirst_Filter (MEA_Unit_t     unit,
                                    MEA_Filter_t *o_filterId,
                                    MEA_Bool     *o_found)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComFilterKey *filterKeyType;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.filterEntry.unit_i = unit;


	tMsgReq.class_level_type = D_CLASS_FILTER_ENTRY;
	tMsgReq.message_level_type = D_MSG_GETFIRST_FILTER_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to MEA_API_COMM_GetFirst_Filter\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	filterKeyType = (tComFilterKey *)&pBobyMsgRcv->info[0];

	(*o_filterId) = filterKeyType->filterId;
	(*o_found) = filterKeyType->found;

	return ret;
}

MEA_Status MEA_API_COMM_GetNext_Filter  (MEA_Unit_t     unit,
                                    MEA_Filter_t *io_filterId ,
                                    MEA_Bool     *o_found)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComFilterKey *filterKeyType;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.filterEntry.unit_i = unit;
	tMsgReq.tMsgBody.filterEntry.filterId = *io_filterId;

	tMsgReq.class_level_type = D_CLASS_FILTER_ENTRY;
	tMsgReq.message_level_type = D_MSG_GET_FILTER_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to MEA_API_COMM_GetNext_Filter\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	filterKeyType = (tComFilterKey *)&pBobyMsgRcv->info[0];

	(*io_filterId) = filterKeyType->filterId;
	(*o_found) = filterKeyType->found;

	return ret;
}

MEA_Status  MEA_API_COMM_Get_Filter     (MEA_Unit_t                      unit_i,
                                    MEA_Filter_t                    filterId,
								    MEA_Filter_Key_dbt             *key_from,
								    MEA_Filter_Key_dbt             *key_to,
				                    MEA_Filter_Data_dbt			   *data,
							        MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
							        MEA_Policer_Entry_dbt          *Policer_Entry,
							        MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComFilterDataKey *filterKey;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.filterEntry.unit_i = unit_i;
	tMsgReq.tMsgBody.filterDataEntry.o_filterId=filterId;


	tMsgReq.class_level_type = D_CLASS_FILTER_ENTRY;
	tMsgReq.message_level_type = D_MSG_GET_FILTER_DATA_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to MEA_API_COMM_Get_Filter_KeyType\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	filterKey = (tComFilterDataKey *)&pBobyMsgRcv->info[0];

	if(data != NULL)
	{
		memcpy(data,&filterKey->data,sizeof(MEA_Filter_Data_dbt));
	}
	if(key_from != NULL)
	{
		memcpy(key_from,&filterKey->key_from,sizeof(MEA_Filter_Key_dbt));
	}
	if(key_to != NULL)
	{
		memcpy(key_to,&filterKey->key_to,sizeof(MEA_Filter_Key_dbt));
	}
	if(OutPorts_Entry != NULL)
	{
		memcpy(OutPorts_Entry,&filterKey->OutPorts_Entry,sizeof(MEA_OutPorts_Entry_dbt));
	}
	if(Policer_Entry != NULL)
	{
		memcpy(Policer_Entry,&filterKey->Policer_entry,sizeof(MEA_Policer_Entry_dbt));
	}
	if(EHP_Entry != NULL)
	{
		memcpy(EHP_Entry,&filterKey->EHP_Entry,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
		if(EHP_Entry->num_of_entries > 0)
		{
			memcpy(&EHP_Entry->ehp_info[0],&filterKey->ehp_info,sizeof(MEA_EHP_Info_dbt));
		}
	}


	return ret;
}

MEA_Status MEA_API_COMM_Create_Filter (MEA_Unit_t                           unit,
                                  MEA_Filter_Key_dbt                  *key_from,
                                  MEA_Filter_Key_dbt                  *key_to,
                                  MEA_Filter_Data_dbt                 *data,
                                  MEA_OutPorts_Entry_dbt               *OutPorts_Entry,
                                  MEA_Policer_Entry_dbt                *Policer_entry,
                                  MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry,
                                  MEA_Filter_t                         *o_filterId)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComFilterDataKey *filterKey;

	memset(&tMsgReq,0,sizeof(tMsgReq));
	if (o_filterId == NULL) {
		fprintf(stdout, "failed to MEA_API_COMM_Create_Filter\r\n");
		return MEA_ERROR;
	}
	if (key_from == NULL) {
		fprintf(stdout, "failed to MEA_API_COMM_Create_Filter\r\n");
		return MEA_ERROR;
	}

	tMsgReq.tMsgBody.filterDataEntry.unit = unit;
	
	tMsgReq.tMsgBody.filterDataEntry.o_filterId=*o_filterId;
	
	
	memcpy(&tMsgReq.tMsgBody.filterDataEntry.data,data,sizeof(MEA_Filter_Data_dbt));
	
	
	memcpy(&tMsgReq.tMsgBody.filterDataEntry.key_from,key_from,sizeof(MEA_Filter_Key_dbt));
	
	if(key_to != NULL)
	{
		memcpy(&tMsgReq.tMsgBody.filterDataEntry.key_to,key_to,sizeof(MEA_Filter_Key_dbt));
	}
	if(OutPorts_Entry != NULL)
	{
		memcpy(&tMsgReq.tMsgBody.filterDataEntry.OutPorts_Entry,OutPorts_Entry,sizeof(MEA_OutPorts_Entry_dbt));
	}
	if(Policer_entry != NULL)
	{
		memcpy(&tMsgReq.tMsgBody.filterDataEntry.Policer_entry,Policer_entry,sizeof(MEA_Policer_Entry_dbt));
	}
	if(EHP_Entry != NULL)
	{
		memcpy(&tMsgReq.tMsgBody.filterDataEntry.EHP_Entry,EHP_Entry,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
		if(EHP_Entry->num_of_entries > 0)
		{
			memcpy(&tMsgReq.tMsgBody.filterDataEntry.ehp_info,&EHP_Entry->ehp_info[0],sizeof(MEA_EHP_Info_dbt));
		}
	}

	tMsgReq.class_level_type = D_CLASS_FILTER_ENTRY;
	tMsgReq.message_level_type = D_MSG_CREATE_FILTER_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to MEA_API_COMM_Create_Filter\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	filterKey = (tComFilterDataKey *)&pBobyMsgRcv->info[0];

	(*o_filterId) = filterKey->o_filterId;

	return ret;

}

/* Note : Only filterId is mandatory */
MEA_Status  MEA_API_COMM_Set_Filter    (MEA_Unit_t                      unit,
                                   MEA_Filter_t                    filterId,
                                   MEA_Filter_Data_dbt            *data,
                                   MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                   MEA_Policer_Entry_dbt          *Policer_entry,
                                   MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.filterDataEntry.unit = unit;
	tMsgReq.tMsgBody.filterDataEntry.o_filterId=filterId;

	if(data != NULL)
	{
		memcpy(&tMsgReq.tMsgBody.filterDataEntry.data,data,sizeof(MEA_Filter_Data_dbt));
	}

	if(OutPorts_Entry != NULL)
	{
		memcpy(&tMsgReq.tMsgBody.filterDataEntry.OutPorts_Entry,OutPorts_Entry,sizeof(MEA_OutPorts_Entry_dbt));
	}
	if(Policer_entry != NULL)
	{
		memcpy(&tMsgReq.tMsgBody.filterDataEntry.Policer_entry,Policer_entry,sizeof(MEA_Policer_Entry_dbt));
	}
	if(EHP_Entry != NULL)
	{
		memcpy(&tMsgReq.tMsgBody.filterDataEntry.EHP_Entry,EHP_Entry,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
		if(EHP_Entry->num_of_entries > 0)
		{
			memcpy(&tMsgReq.tMsgBody.filterDataEntry.ehp_info,&EHP_Entry->ehp_info[0],sizeof(MEA_EHP_Info_dbt));
		}
	}

	tMsgReq.class_level_type = D_CLASS_FILTER_ENTRY;
	tMsgReq.message_level_type = D_MSG_SET_FILTER_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to MEA_API_COMM_Set_Filter\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	return ret;

}

MEA_Status  MEA_API_COMM_Delete_Filter(MEA_Unit_t                      unit,
                                   MEA_Filter_t                    filterId)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.filterDataEntry.unit = unit;
	tMsgReq.tMsgBody.filterDataEntry.o_filterId=filterId;



	tMsgReq.class_level_type = D_CLASS_FILTER_ENTRY;
	tMsgReq.message_level_type = D_MSG_DELETE_FILTER_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to MEA_API_Delete_Filter\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	return ret;
}
