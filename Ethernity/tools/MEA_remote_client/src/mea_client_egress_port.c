#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"


MEA_Status MEA_API_COMM_Set_EgressPort_Entry(MEA_Unit_t                unit,
                                        MEA_Port_t                port,
                                        MEA_EgressPort_Entry_dbt* entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	if(entry == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.egressPort.unit = unit;
	tMsgReq.tMsgBody.egressPort.egress_port = port;
	tMsgReq.class_level_type = D_CLASS_EGRESS_PORT;
	tMsgReq.message_level_type = D_MSG_SET_EGRESS_CONFIGURATION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	memcpy(&tMsgReq.tMsgBody.egressPort.portEgress,entry,sizeof(MEA_EgressPort_Entry_dbt));
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get egress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}

	return ret;

}

MEA_Status MEA_API_COMM_Get_EgressPort_Entry(MEA_Unit_t                unit,
                                        MEA_Port_t                port,
                                        MEA_EgressPort_Entry_dbt* entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComEgressPort  *pEressPortInfo;

	if(entry == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.egressPort.unit = unit;
	tMsgReq.tMsgBody.egressPort.egress_port = port;
	tMsgReq.class_level_type = D_CLASS_EGRESS_PORT;
	tMsgReq.message_level_type = D_MSG_GET_EGRESS_CONFIGURATION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pEressPortInfo = (tComEgressPort *)&pBobyMsgRcv->info[0];

		memcpy(entry,&pEressPortInfo->portEgress,sizeof(MEA_EgressPort_Entry_dbt));
	}

	return ret;
}

