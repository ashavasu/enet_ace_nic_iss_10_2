#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"



MEA_Status MEA_API_COMM_Create_Action (MEA_Unit_t                              unit,
                                   	   MEA_Action_Entry_Data_dbt             *Action_Data_pio,
                                   	   MEA_OutPorts_Entry_dbt                *Action_OutPorts_pi,
                                   	   MEA_Uint16                     	   	 policer_id,
                                   	   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_pi,
                                   	   MEA_Action_t                          *Action_Id_io)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComAction  *pAction;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.action.unit = unit;
	tMsgReq.tMsgBody.action.data_valid=0;
	if(Action_Data_pio != NULL)
	{
		tMsgReq.tMsgBody.action.data_valid=1;
		memcpy(&tMsgReq.tMsgBody.action.Action_Data,Action_Data_pio,sizeof(MEA_Action_Entry_Data_dbt));
	}

	tMsgReq.tMsgBody.action.num_of_entries=0;
	if(Action_Editing_pi != NULL)
	{
		if(Action_Editing_pi->num_of_entries > D_MAX_NUM_OF_ACTION_EDITINGS)
		{
			fprintf(stdout,"number of entries cannot be greater then %d\r\n",D_MAX_NUM_OF_ACTION_EDITINGS);
			return ret;
		}
		tMsgReq.tMsgBody.action.num_of_entries = Action_Editing_pi->num_of_entries;
		memcpy(&tMsgReq.tMsgBody.action.ehp_info[0],Action_Editing_pi->ehp_info,sizeof(MEA_EHP_Info_dbt)*Action_Editing_pi->num_of_entries);
	}
	tMsgReq.tMsgBody.action.policer_id = policer_id;
	tMsgReq.tMsgBody.action.Action_Id=0;
	tMsgReq.tMsgBody.action.ACM_Mode=0;
	if(Action_Id_io != NULL)
	{
		tMsgReq.tMsgBody.action.Action_Id = *Action_Id_io;
	}

	if(Action_OutPorts_pi != NULL)
	{
		tMsgReq.tMsgBody.action.outport_valid=1;
		memcpy(&tMsgReq.tMsgBody.action.Action_OutPorts,Action_OutPorts_pi,sizeof(MEA_OutPorts_Entry_dbt));
	}

	tMsgReq.class_level_type = D_CLASS_ACTION_ID;
	tMsgReq.message_level_type = D_MSG_CREATE_ACTION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pAction = (tComAction *)&pBobyMsgRcv->info[0];
		if(Action_Id_io != NULL)
		{
			(*Action_Id_io) =  pAction->Action_Id;
		}
	}



	return ret;
}


MEA_Status  MEA_API_COMM_Set_Action(MEA_Unit_t                             unit,
                                   	MEA_Action_t                           Action_Id_i,
                                   	MEA_Action_Entry_Data_dbt             *Action_Data_pio,
                                   	MEA_OutPorts_Entry_dbt                *Action_OutPorts_pi,
                                   	MEA_Uint16                     	   	 	policer_id,
                                   	MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_pi)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.action.unit = unit;
	tMsgReq.tMsgBody.action.data_valid=0;
	if(Action_Data_pio != NULL)
	{
		tMsgReq.tMsgBody.action.data_valid=1;
		memcpy(&tMsgReq.tMsgBody.action.Action_Data,Action_Data_pio,sizeof(MEA_Action_Entry_Data_dbt));
	}

	tMsgReq.tMsgBody.action.num_of_entries=0;
	if(Action_Editing_pi != NULL)
	{
		if(Action_Editing_pi->num_of_entries > D_MAX_NUM_OF_ACTION_EDITINGS)
		{
			fprintf(stdout,"number of entries cannot be greater then %d\r\n",D_MAX_NUM_OF_ACTION_EDITINGS);
			return ret;
		}
		tMsgReq.tMsgBody.action.num_of_entries = Action_Editing_pi->num_of_entries;
		memcpy(&tMsgReq.tMsgBody.action.ehp_info[0],Action_Editing_pi->ehp_info,sizeof(MEA_EHP_Info_dbt)*Action_Editing_pi->num_of_entries);
	}
	tMsgReq.tMsgBody.action.policer_id = policer_id;

	tMsgReq.tMsgBody.action.Action_Id = Action_Id_i;

	if(Action_OutPorts_pi != NULL)
	{
		tMsgReq.tMsgBody.action.outport_valid=1;
		memcpy(&tMsgReq.tMsgBody.action.Action_OutPorts,Action_OutPorts_pi,sizeof(MEA_OutPorts_Entry_dbt));
	}


	tMsgReq.class_level_type = D_CLASS_ACTION_ID;
	tMsgReq.message_level_type = D_MSG_SET_ACTION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	return ret;
}

MEA_Status  MEA_API_COMM_Get_Action(MEA_Unit_t                             unit,
                                   MEA_Action_t                           Action_Id_i,
                                   MEA_Action_Entry_Data_dbt             *Action_Data_po,
                                   MEA_OutPorts_Entry_dbt                *Action_OutPorts_po,
                                   MEA_Uint16                     	   	 *policer_id,
                                   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_po)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComAction  *pAction;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.action.unit = unit;
	tMsgReq.tMsgBody.action.data_valid=0;
	if(Action_Data_po != NULL)
	{
		tMsgReq.tMsgBody.action.data_valid=1;
	}

	tMsgReq.tMsgBody.action.num_of_entries=0;
	if(Action_Editing_po != NULL)
	{
		if(Action_Editing_po->num_of_entries > D_MAX_NUM_OF_ACTION_EDITINGS)
		{
			fprintf(stdout,"number of entries cannot be greater then %d\r\n",D_MAX_NUM_OF_ACTION_EDITINGS);
			return ret;
		}
		tMsgReq.tMsgBody.action.num_of_entries = Action_Editing_po->num_of_entries;
		memcpy(&tMsgReq.tMsgBody.action.ehp_info[0],Action_Editing_po->ehp_info,sizeof(MEA_EHP_Info_dbt)*Action_Editing_po->num_of_entries);
	}
	tMsgReq.tMsgBody.action.Action_Id = Action_Id_i;

	tMsgReq.tMsgBody.action.outport_valid=1;

	tMsgReq.class_level_type = D_CLASS_ACTION_ID;
	tMsgReq.message_level_type = D_MSG_GET_ACTION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pAction = (tComAction *)&pBobyMsgRcv->info[0];

		if(Action_Data_po != NULL)
		{
			memcpy(Action_Data_po,&tMsgReq.tMsgBody.action.Action_Data,sizeof(MEA_Action_Entry_Data_dbt));
		}
		if(Action_OutPorts_po != NULL)
		{
			memcpy(Action_OutPorts_po,&tMsgReq.tMsgBody.action.Action_OutPorts,sizeof(MEA_OutPorts_Entry_dbt));
		}
		if(Action_Editing_po != NULL)
		{
			Action_Editing_po->num_of_entries = tMsgReq.tMsgBody.action.num_of_entries;
			memcpy(&Action_Editing_po->ehp_info[0],&tMsgReq.tMsgBody.action.ehp_info[0],sizeof(MEA_EHP_Info_dbt)*tMsgReq.tMsgBody.action.num_of_entries);
		}
		if(policer_id != NULL)
		{
			(*policer_id) = tMsgReq.tMsgBody.action.policer_id;
		}
	}

	return ret;

}


MEA_Status  MEA_API_COMM_Delete_Action (MEA_Unit_t                       unit,
                                   MEA_Action_t                          Action_Id_i)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;



	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.action.unit = unit;
	tMsgReq.tMsgBody.action.Action_Id = Action_Id_i;


	tMsgReq.class_level_type = D_CLASS_ACTION_ID;
	tMsgReq.message_level_type = D_MSG_DELETE_ACTION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	return ret;
}


MEA_Status MEA_API_COMM_GetFirst_Action (MEA_Unit_t                     unit_i,
                                    MEA_Action_t                        *Action_Id_o ,
                                    MEA_Bool                            *found_o)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComAction  *pAction;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.action.unit = unit_i;


	tMsgReq.class_level_type = D_CLASS_ACTION_ID;
	tMsgReq.message_level_type = D_MSG_GETFIRST_ACTION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get first action database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	pAction = (tComAction *)&pBobyMsgRcv->info[0];
	(*Action_Id_o) = pAction->Action_Id;
	(*found_o) = pAction->found;
	return ret;
}

MEA_Status MEA_API_COMM_GetNext_Action  (MEA_Unit_t                      unit_i,
                                    MEA_Action_t                        *Action_Id_io,
                                    MEA_Bool                            *found_o)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComAction  *pAction;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.action.unit = unit_i;
	tMsgReq.tMsgBody.action.Action_Id = (*Action_Id_io);


	tMsgReq.class_level_type = D_CLASS_ACTION_ID;
	tMsgReq.message_level_type = D_MSG_GETNEXT_ACTION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get first action database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	pAction = (tComAction *)&pBobyMsgRcv->info[0];
	(*Action_Id_io) = pAction->Action_Id;
	(*found_o) = pAction->found;
	return ret;
}
