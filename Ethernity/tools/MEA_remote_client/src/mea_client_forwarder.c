#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"

MEA_Status MEA_API_COMM_Create_SE_Entry (MEA_Unit_t  unit, MEA_SE_Entry_dbt *entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.forwarder.unit = unit;
	memcpy(&tMsgReq.tMsgBody.forwarder.entry,entry,sizeof(MEA_SE_Entry_dbt));

	fprintf(stdout,"********* create forwarder key-type:%d **********\r\n",tMsgReq.tMsgBody.forwarder.entry.key.type);

	tMsgReq.class_level_type = D_CLASS_FORWARDER_ID;
	tMsgReq.message_level_type = D_MSG_CREATE_FORWARDER;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to create search engine\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	return ret;
}
MEA_Status MEA_API_COMM_Delete_SE_Entry(MEA_Unit_t  unit_i,MEA_SE_Entry_key_dbt *key_pi)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.forwarder.unit = unit_i;
	memcpy(&tMsgReq.tMsgBody.forwarder.entry.key,key_pi,sizeof(MEA_SE_Entry_key_dbt));


	tMsgReq.class_level_type = D_CLASS_FORWARDER_ID;
	tMsgReq.message_level_type = D_MSG_DELETE_FORWARDER;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to create search engine\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	return ret;
}
MEA_Status MEA_API_COMM_Set_SE_Entry(MEA_Unit_t   unit,MEA_SE_Entry_dbt  *entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.forwarder.unit = unit;
	memcpy(&tMsgReq.tMsgBody.forwarder.entry,entry,sizeof(MEA_SE_Entry_dbt));


	tMsgReq.class_level_type = D_CLASS_FORWARDER_ID;
	tMsgReq.message_level_type = D_MSG_SET_FORWARDER;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to create search engine\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	return ret;
}
MEA_Status MEA_API_COMM_Get_SE_Entry(MEA_Unit_t   unit,MEA_SE_Entry_dbt  *entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComForwarder  *pForwarder;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.forwarder.unit = unit;
	memcpy(&tMsgReq.tMsgBody.forwarder.entry,entry,sizeof(MEA_SE_Entry_dbt));


	tMsgReq.class_level_type = D_CLASS_FORWARDER_ID;
	tMsgReq.message_level_type = D_MSG_GET_FORWARDER;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get search engine port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pForwarder = (tComForwarder *)&pBobyMsgRcv->info[0];

		memcpy(entry,&pForwarder->entry,sizeof(MEA_SE_Entry_dbt));

	}

	return ret;
}


MEA_Status MEA_API_COMM_GetFirst_SE_Entry   (MEA_Unit_t unit,MEA_SE_Entry_dbt  *entry,MEA_Bool*  found)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComForwarder  *pForwarder;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.forwarder.unit = unit;


	tMsgReq.class_level_type = D_CLASS_FORWARDER_ID;
	tMsgReq.message_level_type = D_MSG_GETFIRST_FORWARDER;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get first search engine port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pForwarder = (tComForwarder *)&pBobyMsgRcv->info[0];

		memcpy(entry,&pForwarder->entry,sizeof(MEA_SE_Entry_dbt));
		(*found) = pForwarder->found;
	}

	return ret;
}


MEA_Status MEA_API_COMM_GetNext_SE_Entry   (MEA_Unit_t  unit,MEA_SE_Entry_dbt *entry,MEA_Bool*  found)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComForwarder  *pForwarder;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.forwarder.unit = unit;
	memcpy(&tMsgReq.tMsgBody.forwarder.entry,entry,sizeof(MEA_SE_Entry_dbt));


	tMsgReq.class_level_type = D_CLASS_FORWARDER_ID;
	tMsgReq.message_level_type = D_MSG_GETNEXT_FORWARDER;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get search engine port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pForwarder = (tComForwarder *)&pBobyMsgRcv->info[0];

		memcpy(entry,&pForwarder->entry,sizeof(MEA_SE_Entry_dbt));
		(*found) = pForwarder->found;
	}

	return ret;
}


MEA_Status MEA_API_COMM_DeleteAll_SE (MEA_Unit_t unit)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.forwarder.unit = unit;


	tMsgReq.class_level_type = D_CLASS_FORWARDER_ID;
	tMsgReq.message_level_type = D_MSG_CLEAR_ALL_FORWARDER;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);


	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get search engine port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	return ret;
}

MEA_Status MEA_API_COMM_GetRange_SE_Entry   (MEA_Unit_t unit,MEA_SE_Entry_key_dbt *key_Array,MEA_Bool first,MEA_Uint32 *pMax_num_of_entries)
{
	tMsgReq tMsgReq;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComIndexDb *pRetEntry;
	tComForwardPortsDb	*pForwarderKey;
	MEA_Uint32 curr_entries=0;
	MEA_Uint32 idx=0;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.forwarder.unit = unit;
	tMsgReq.tMsgBody.forwarder.first = first;
	if(first == 0)
	{
		memcpy(&tMsgReq.tMsgBody.forwarder.entry.key,&key_Array[0],sizeof(MEA_SE_Entry_key_dbt));
	}


	tMsgReq.class_level_type = D_CLASS_FORWARDER_ID;
	tMsgReq.message_level_type = D_MSG_GETRANGE_FORWARDER;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);


	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get search engine port database\r\n");
		return ret;
	}
	pRetEntry = (tComIndexDb *)bufferRcv;
	pForwarderKey = (tComForwardPortsDb *)&pRetEntry->next[0];

	for(idx=0;idx<(MEA_Uint32) pRetEntry->number_of_ports;idx++)
	{
		if(curr_entries < (*pMax_num_of_entries) )
		{
			memcpy(&key_Array[idx],&pForwarderKey->key,sizeof(MEA_SE_Entry_key_dbt));
			pForwarderKey = (tComForwardPortsDb *)&pForwarderKey->next[0];
			curr_entries++;
		}
		else
		{
			break;
		}
	}
	(*pMax_num_of_entries) = curr_entries;

	return ret;
}
