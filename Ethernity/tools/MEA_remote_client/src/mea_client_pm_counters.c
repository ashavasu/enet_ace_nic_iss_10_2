#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"

MEA_Status MEA_API_COMM_Clear_Counters_PM(MEA_Unit_t                    unit,
                                          MEA_PmId_t                    pmId)


{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
   // tComm_PmId_dbt *pmIdData;


   

    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.pmInfo.unit = unit;
    tMsgReq.tMsgBody.pmInfo.PmId = pmId;
    tMsgReq.class_level_type = D_CLASS_PM_ID;
    tMsgReq.message_level_type = D_MSG_CLEAR_PM;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
      //  pmIdData = (tComm_PmId_dbt *)&pBobyMsgRcv->info[0];

        //memcpy(entry, &pmIdData->Data, sizeof(MEA_Counters_PM_dbt));
    }
    return ret;

}

MEA_Status MEA_API_COMM_Clear_Counters_PMs(MEA_Unit_t                    unit)


{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    //tComm_PmId_dbt *pmIdData;



    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.pmInfo.unit = unit;
    //tMsgReq.tMsgBody.pmInfo.PmId = 0;
    tMsgReq.class_level_type = D_CLASS_PM_ID;
    tMsgReq.message_level_type = D_MSG_CLEAR_ALL_PM;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
        //pmIdData = (tComm_PmId_dbt *)&pBobyMsgRcv->info[0];

        //memcpy(entry, &pmIdData->Data, sizeof(MEA_Counters_PM_dbt));
    }
    return ret;

}


MEA_Status MEA_API_COMM_Get_Counters_PM(MEA_Unit_t                    unit,
                                        MEA_PmId_t                    pmId,
                                        MEA_Counters_PM_dbt*          entry)

{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
    tComm_PmId_dbt *pmIdData;


	if(entry == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

    tMsgReq.tMsgBody.pmInfo.unit = unit;
    tMsgReq.tMsgBody.pmInfo.PmId = pmId;
    tMsgReq.class_level_type = D_CLASS_PM_ID;
    tMsgReq.message_level_type = D_MSG_GET_PMID;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get egress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}
    else
    {
        pmIdData = (tComm_PmId_dbt *)&pBobyMsgRcv->info[0];

        memcpy(entry, &pmIdData->Data, sizeof(MEA_Counters_PM_dbt));
    }
	return ret;

}

MEA_Status MEA_API_COMM_GetFirst_Counters_PM(MEA_Unit_t                    unit,
                                            MEA_PmId_t                    *pmId,
                                            MEA_Counters_PM_dbt           *entry,
                                            MEA_Bool                     *valid)

{
    tMsgReq MsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    tComm_PmId_dbt *pmIdData;


    fprintf(stdout, "1\r\n");


    if (entry == NULL)
    {
        fprintf(stdout, "received NULL data entry\r\n");
        return ret;
    }

    fprintf(stdout, "2\r\n");


    memset(&MsgReq, 0, sizeof(tMsgReq));

    fprintf(stdout, "3\r\n");

    MsgReq.tMsgBody.pmInfo.unit = unit;
    MsgReq.tMsgBody.pmInfo.PmId = *pmId;
    MsgReq.class_level_type = D_CLASS_PM_ID;
    MsgReq.message_level_type = D_MSG_GETFIRST_PM;
    MsgReq.MaxBufferRcv = BUFFER_SIZE;

    fprintf(stdout, "send message\r\n");

    MsgReq.bufferRcv = &bufferRcv[0];


    ret = mea_client_build_packet(&MsgReq);

    fprintf(stdout, "rcv message\r\n");

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
    	fprintf(stdout, "fill message\r\n");
        pmIdData = (tComm_PmId_dbt *)&pBobyMsgRcv->info[0];
        (*valid) = pmIdData->valid;
        (*pmId) = pmIdData->PmId;
        if (*valid)
            memcpy(entry, &pmIdData->Data, sizeof(MEA_Counters_PM_dbt));
    }
    return ret;

}

MEA_Status MEA_API_COMM_GetNext_Counters_PM(MEA_Unit_t                    unit,
                                            MEA_PmId_t                    *pmId,
                                            MEA_Counters_PM_dbt          *entry,
                                            MEA_Bool                      *valid)

{
    tMsgReq MsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    tComm_PmId_dbt *pmIdData;


    if (entry == NULL)
    {
        fprintf(stdout, "received NULL data entry\r\n");
        return ret;
    }

    memset(&MsgReq, 0, sizeof(tMsgReq));

    MsgReq.tMsgBody.pmInfo.unit = unit;
    MsgReq.tMsgBody.pmInfo.PmId = *pmId;
    MsgReq.class_level_type = D_CLASS_PM_ID;
    MsgReq.message_level_type = D_MSG_GETNEXT_PM;
    MsgReq.MaxBufferRcv = BUFFER_SIZE;

    MsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&MsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
        pmIdData = (tComm_PmId_dbt *)&pBobyMsgRcv->info[0];
        (*valid) = pmIdData->valid;
        (*pmId) = pmIdData->PmId;
        if (*valid)
            memcpy(entry, &pmIdData->Data, sizeof(MEA_Counters_PM_dbt));
    }
    return ret;

}

MEA_Status  MEA_API_COMM_GetRange_PM (MEA_Unit_t  unit,MEA_Uint32 pmStart,MEA_PmId_t  *pmIdArry,MEA_Uint32 *pMaxEntries)
{
	tMsgReq MsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComIndexDb *pIndexEntry;
	MEA_Uint32 max_num_of_pm=0;
	int i=0;
	tComIndex *pNextPort;

	if(pmIdArry == NULL)
	{
		fprintf(stdout,"pmIdArry is NULL\r\n");
		return MEA_ERROR;
	}

	if(pMaxEntries == NULL)
	{
		fprintf(stdout,"pMaxEntries is NULL\r\n");
		return MEA_ERROR;
	}

    memset(&MsgReq, 0, sizeof(tMsgReq));

    MsgReq.tMsgBody.pmInfo.unit = unit;
    MsgReq.tMsgBody.pmInfo.PmId = (MEA_PmId_t)pmStart;
    MsgReq.class_level_type = D_CLASS_PM_ID;
    MsgReq.message_level_type = D_MSG_GET_RANGE_PM;
    MsgReq.MaxBufferRcv = BUFFER_SIZE;

    MsgReq.bufferRcv = &bufferRcv[0];

	fprintf(stdout,"receive size %d\r\n",MsgReq.ActBufferRcv);

	ret = mea_client_build_packet(&MsgReq);


	pBobyMsgRcv = (tMsgBody *)bufferRcv;

	ret = pBobyMsgRcv->hdr.ret;

	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	pIndexEntry = (tComIndexDb *)&pBobyMsgRcv->info[0];

	pNextPort = (tComIndex *)&pIndexEntry->next[0];

	for(i=0; i< pIndexEntry->number_of_ports;i++)
	{
		if(max_num_of_pm < (*pMaxEntries))
		{
			pmIdArry[i] = pNextPort->ingress_port;
			max_num_of_pm++;
		}
		else
		{
			break;
		}
		pNextPort = (tComIndex *)&pNextPort->next[0];
	}
	(*pMaxEntries) = max_num_of_pm;
	return ret;

}


/************************************************************************/
/*  RMON  Counters                                                      */
/************************************************************************/

MEA_Status MEA_API_COMM_Clear_Counters_RMON(MEA_Unit_t                    unit,
    MEA_Port_t                    port)


{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    // tComm_PmId_dbt *pmIdData;




    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.RmonInfo.unit = unit;
    tMsgReq.tMsgBody.RmonInfo.port = port;
    tMsgReq.class_level_type = D_CLASS_RMON_ID;
    tMsgReq.message_level_type = D_MSG_CLEAR_RMON;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
        //  pmIdData = (tComm_PmId_dbt *)&pBobyMsgRcv->info[0];

        //memcpy(entry, &pmIdData->Data, sizeof(MEA_Counters_PM_dbt));
    }
    return ret;

}

MEA_Status MEA_API_COMM_Clear_Counters_RMONs(MEA_Unit_t                    unit)


{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    //tComm_PmId_dbt *pmIdData;



    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.pmInfo.unit = unit;
    tMsgReq.tMsgBody.pmInfo.PmId = 0;
    tMsgReq.class_level_type = D_CLASS_RMON_ID;
    tMsgReq.message_level_type = D_MSG_CLEAR_ALL_RMON;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
        //pmIdData = (tComm_PmId_dbt *)&pBobyMsgRcv->info[0];

        //memcpy(entry, &pmIdData->Data, sizeof(MEA_Counters_PM_dbt));
    }
    return ret;

}


MEA_Status MEA_API_COMM_Get_Counters_RMON(MEA_Unit_t                    unit,
                                          MEA_Port_t                    port,
                                          MEA_Counters_RMON_dbt         *entry)

{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    tComm_RMON_dbt *rmonData;


    if (entry == NULL)
    {
        fprintf(stdout, "received NULL data entry\r\n");
        return ret;
    }

    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.RmonInfo.unit = unit;
    tMsgReq.tMsgBody.RmonInfo.port = port;
    tMsgReq.class_level_type = D_CLASS_RMON_ID;
    tMsgReq.message_level_type = D_MSG_GET_RMON;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
        rmonData = (tComm_RMON_dbt *)&pBobyMsgRcv->info[0];

        memcpy(entry, &rmonData->Data, sizeof(MEA_Counters_RMON_dbt));
    }
    return ret;

}

MEA_Status MEA_API_COMM_GetFirst_Counters_RMON(MEA_Unit_t                    unit,
                                               MEA_Port_t                    *port,
                                               MEA_Counters_RMON_dbt         *entry,
                                               MEA_Bool                      *valid)

{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    tComm_RMON_dbt *rmonData;


    if (entry == NULL)
    {
        fprintf(stdout, "received NULL data entry\r\n");
        return ret;
    }

    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.RmonInfo.unit = unit;
    tMsgReq.tMsgBody.RmonInfo.port = *port;
    tMsgReq.class_level_type = D_CLASS_RMON_ID;
    tMsgReq.message_level_type = D_MSG_GETFIRST_RMON;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
        rmonData = (tComm_RMON_dbt *)&pBobyMsgRcv->info[0];
        (*valid) = rmonData->valid;
        (*port) = rmonData->port;
        if (*valid)
            memcpy(entry, &rmonData->Data, sizeof(MEA_Counters_RMON_dbt));
    }
    return ret;

}

MEA_Status MEA_API_COMM_GetNext_Counters_RMON(MEA_Unit_t                    unit,
                                              MEA_Port_t                    *port,
                                              MEA_Counters_RMON_dbt         *entry,
                                               MEA_Bool                      *valid)

{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    tComm_RMON_dbt *rmonData;


    if (entry == NULL)
    {
        fprintf(stdout, "received NULL data entry\r\n");
        return ret;
    }

    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.pmInfo.unit = unit;
    tMsgReq.tMsgBody.RmonInfo.port = *port;
    tMsgReq.class_level_type = D_CLASS_RMON_ID;
    tMsgReq.message_level_type = D_MSG_GETNEXT_RMON;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
        rmonData = (tComm_RMON_dbt *)&pBobyMsgRcv->info[0];
        (*valid) = rmonData->valid;
        (*port) = rmonData->port;
        if (*valid)
            memcpy(entry, &rmonData->Data, sizeof(MEA_Counters_RMON_dbt));
    }
    return ret;

}

/****************************************************************************************************/
MEA_Status MEA_API_COMM_Get_Counters_CCM(MEA_Unit_t                    unit,
										MEA_CcmId_t Id,
										MEA_Counters_CCM_Defect_dbt *entry)

{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    tComCcmCounter *ccmData;


    if (entry == NULL)
    {
        fprintf(stdout, "received NULL data entry\r\n");
        return ret;
    }

    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.ccmCounter.unit = unit;
    tMsgReq.tMsgBody.ccmCounter.Id = Id;
    tMsgReq.class_level_type = D_CLASS_CCM_COUNTER_ENTRY;
    tMsgReq.message_level_type = D_MSG_GET_CCM_COUNTER_ENTRY;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
    	ccmData = (tComCcmCounter *)&pBobyMsgRcv->info[0];

        memcpy(entry, &ccmData->entry, sizeof(MEA_Counters_CCM_Defect_dbt));
    }
    return ret;

}


MEA_Status MEA_API_COMM_Clear_Counters_CCM(MEA_Unit_t              unit,
                                      	  MEA_CcmId_t             id_i)
{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
//    tComCcmCounter *ccmData;


    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.ccmCounter.unit = unit;
    tMsgReq.tMsgBody.ccmCounter.Id = id_i;
    tMsgReq.class_level_type = D_CLASS_CCM_COUNTER_ENTRY;
    tMsgReq.message_level_type = D_MSG_CLR_CCM_COUNTER_ENTRY;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    return ret;

}

MEA_Status MEA_API_COMM_Get_Counters_LM(MEA_Unit_t             unit,
                                   MEA_LmId_t               id_i,
                                   MEA_Counters_LM_dbt   *entry)
{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    tComLmCounter *lmData;


    if (entry == NULL)
    {
        fprintf(stdout, "received NULL data entry\r\n");
        return ret;
    }

    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.lmCounter.unit = unit;
    tMsgReq.tMsgBody.lmCounter.Id = id_i;
    tMsgReq.class_level_type = D_CLASS_LM_COUNTER_ENTRY;
    tMsgReq.message_level_type = D_MSG_GET_LM_COUNTER_ENTRY;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
    	lmData = (tComLmCounter *)&pBobyMsgRcv->info[0];

        memcpy(entry, &lmData->entry, sizeof(MEA_Counters_LM_dbt));
    }
    return ret;
}

MEA_Status MEA_API_COMM_Clear_Counters_LM(MEA_Unit_t           unit,
                                     MEA_LmId_t             id_i)
{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;


    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.lmCounter.unit = unit;
    tMsgReq.tMsgBody.lmCounter.Id = id_i;
    tMsgReq.class_level_type = D_CLASS_LM_COUNTER_ENTRY;
    tMsgReq.message_level_type = D_MSG_CLR_LM_COUNTER_ENTRY;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    return ret;
}

/******************** EVENT COUNTER ******************************************/
MEA_Status MEA_API_COMM_Get_CC_Event_GroupId(MEA_Unit_t   unit,MEA_Uint32  group,MEA_Uint32 *pEvent)
{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    tComPckEventGroup *eventData;


    memset(&tMsgReq, 0, sizeof(tMsgReq));

    tMsgReq.tMsgBody.anaEventGroup.unit = unit;
    tMsgReq.tMsgBody.anaEventGroup.group = group;
    tMsgReq.class_level_type = D_CLASS_EVENT_GROUP_ENTRY;
    tMsgReq.message_level_type = D_MSG_GET_EVENT_GROUP_ENTRY;
    tMsgReq.MaxBufferRcv = BUFFER_SIZE;

    tMsgReq.bufferRcv = &bufferRcv[0];

    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to get egress port database\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
    }
    else
    {
    	eventData = (tComPckEventGroup *)&pBobyMsgRcv->info[0];

    	(*pEvent) = eventData->event;
    }
    return ret;
}
