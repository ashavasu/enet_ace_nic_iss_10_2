#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"



MEA_Status MEA_API_COMM_Create_PacketGen_Profile_info(MEA_Unit_t              unit_i,
                          MEA_PacketGen_drv_Profile_info_entry_dbt      *entry_pi,
                          MEA_PacketGen_profile_info_t                  *id_io)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckGenProfile  *pInfo;


	if(entry_pi == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenProfile.unit = unit_i;
	tMsgReq.tMsgBody.pckGenProfile.id = (*id_io);
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_PROFILE;
	tMsgReq.message_level_type = D_MSG_CREATE_PACKETGEN_PROFILE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	memcpy(&tMsgReq.tMsgBody.pckGenProfile.entry,entry_pi,sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set packet generator profile database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	pInfo = (tComPckGenProfile *)&pBobyMsgRcv->info[0];
	(*id_io) = pInfo->id;
	return ret;

}

MEA_Status MEA_API_COMM_Set_PacketGen_Profile_info_Entry(MEA_Unit_t                           unit_i,
                           MEA_PacketGen_profile_info_t                 id_i,
                           MEA_PacketGen_drv_Profile_info_entry_dbt     *entry_pi)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	if(entry_pi == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenProfile.unit = unit_i;
	tMsgReq.tMsgBody.pckGenProfile.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_PROFILE;
	tMsgReq.message_level_type = D_MSG_SET_PACKETGEN_PROFILE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	memcpy(&tMsgReq.tMsgBody.pckGenProfile.entry,entry_pi,sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set packet generator profile database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}

	return ret;

}

MEA_Status MEA_API_COMM_Delete_PacketGen_Profile_info_Entry(MEA_Unit_t                    unit_i,
                                                       MEA_PacketGen_profile_info_t  id_i)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;



	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenProfile.unit = unit_i;
	tMsgReq.tMsgBody.pckGenProfile.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_PROFILE;
	tMsgReq.message_level_type = D_MSG_DELETE_PACKETGEN_PROFILE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set packet generator profile database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}

	return ret;

}

MEA_Status MEA_API_COMM_Get_PacketGen_Profile_info_Entry(MEA_Unit_t           unit_i,
                               MEA_PacketGen_profile_info_t                           id_i,
                               MEA_PacketGen_drv_Profile_info_entry_dbt  *entry_po)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckGenProfile  *pInfo;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenProfile.unit = unit_i;
	tMsgReq.tMsgBody.pckGenProfile.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_PROFILE;
	tMsgReq.message_level_type = D_MSG_GET_PACKETGEN_PROFILE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComPckGenProfile *)&pBobyMsgRcv->info[0];

		memcpy(entry_po,&pInfo->entry,sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));
	}

	return ret;
}

MEA_Status MEA_API_COMM_GetFirst_PacketGen_Profile_info_Entry(MEA_Unit_t          unit_i,
                                MEA_PacketGen_profile_info_t                *id_o,
                                MEA_PacketGen_drv_Profile_info_entry_dbt    *entry_po,
                                MEA_Bool                                    *found_o)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckGenProfile  *pInfo;

	(*found_o) = MEA_FALSE;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenProfile.unit = unit_i;
	tMsgReq.tMsgBody.pckGenProfile.id = *id_o;
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_PROFILE;
	tMsgReq.message_level_type = D_MSG_GETFIRST_PACKETGEN_PROFILE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get first packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComPckGenProfile *)&pBobyMsgRcv->info[0];

		memcpy(entry_po,&pInfo->entry,sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));
		(*found_o) = pInfo->found;
		(*id_o) = pInfo->id;
	}

	return ret;
}


MEA_Status MEA_API_COMM_GetNext_PacketGen_Profile_info_Entry(MEA_Unit_t                      unit_i,
                               MEA_PacketGen_profile_info_t                 *id_io,
                               MEA_PacketGen_drv_Profile_info_entry_dbt     *entry_po,
                               MEA_Bool                                     *found_o)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckGenProfile  *pInfo;

	(*found_o) = MEA_FALSE;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenProfile.unit = unit_i;
	tMsgReq.tMsgBody.pckGenProfile.id = (*id_io);
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_PROFILE;
	tMsgReq.message_level_type = D_MSG_GETNEXT_PACKETGEN_PROFILE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get next packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComPckGenProfile *)&pBobyMsgRcv->info[0];

		memcpy(entry_po,&pInfo->entry,sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));
		(*found_o) = pInfo->found;
		(*id_io) = pInfo->id;
	}

	return ret;
}


/********************************************************************/
/*          packet gen entry 										*/
/********************************************************************/


MEA_Status MEA_API_COMM_Create_PacketGen_Entry(MEA_Unit_t              unit_i,
												MEA_PacketGen_Entry_dbt      *entry_pi,
												MEA_PacketGen_t                  *id_io)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckGenEntry  *pInfo;


	if(entry_pi == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckGenEntry.id = (*id_io);
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_ENTRY;
	tMsgReq.message_level_type = D_MSG_CREATE_PACKETGEN_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	memcpy(&tMsgReq.tMsgBody.pckGenEntry.entry,entry_pi,sizeof(MEA_PacketGen_Entry_dbt));
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to create packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	pInfo = (tComPckGenEntry *)&pBobyMsgRcv->info[0];
	(*id_io) = pInfo->id;
	fprintf(stdout,"create packet gen:%d\r\n",pInfo->id);
	return ret;


}

MEA_Status MEA_API_COMM_Set_PacketGen_Entry(MEA_Unit_t                       unit_i,
                                       MEA_PacketGen_t                  id_i,
                                       MEA_PacketGen_Entry_dbt         *entry_pi)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	if(entry_pi == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckGenEntry.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_ENTRY;
	tMsgReq.message_level_type = D_MSG_SET_PACKETGEN_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	memcpy(&tMsgReq.tMsgBody.pckGenEntry.entry,entry_pi,sizeof(MEA_PacketGen_Entry_dbt));
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set packet generator profile database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}

	return ret;

}

MEA_Status MEA_API_COMM_Delete_PacketGen_Entry(MEA_Unit_t                    unit_i,
                                          MEA_PacketGen_t               id_i)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;



	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckGenEntry.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_ENTRY;
	tMsgReq.message_level_type = D_MSG_DELETE_PACKETGEN_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set packet generator profile database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}

	return ret;

}

MEA_Status MEA_API_COMM_Get_PacketGen_Entry(MEA_Unit_t                       unit_i,
                                       MEA_PacketGen_t                  id_i,
                                       MEA_PacketGen_Entry_dbt         *entry_po)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckGenEntry  *pInfo;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckGenEntry.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_ENTRY;
	tMsgReq.message_level_type = D_MSG_GET_PACKETGEN_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComPckGenEntry *)&pBobyMsgRcv->info[0];

		memcpy(entry_po,&pInfo->entry,sizeof(MEA_PacketGen_Entry_dbt));
	}

	return ret;
}

MEA_Status MEA_API_COMM_GetFirst_PacketGen_Entry(MEA_Unit_t                  unit_i,
                                            MEA_PacketGen_t            *id_o,
                                            MEA_PacketGen_Entry_dbt    *entry_po, /* Can be NULL */
                                            MEA_Bool                   *found_o)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckGenEntry  *pInfo;

	(*found_o) = MEA_FALSE;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckGenEntry.id = *id_o;
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_ENTRY;
	tMsgReq.message_level_type = D_MSG_GETFIRST_PACKETGEN_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get first packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComPckGenEntry *)&pBobyMsgRcv->info[0];

		memcpy(entry_po,&pInfo->entry,sizeof(MEA_PacketGen_Entry_dbt));
		(*found_o) = pInfo->found;
		(*id_o) = pInfo->id;
	}

	return ret;
}


MEA_Status MEA_API_COMM_GetNext_PacketGen_Entry(MEA_Unit_t                   unit_i,
                                           MEA_PacketGen_t             *id_io,
                                           MEA_PacketGen_Entry_dbt     *entry_po, /* Can be NULL */
                                           MEA_Bool                    *found_o)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckGenEntry  *pInfo;

	(*found_o) = MEA_FALSE;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckGenEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckGenEntry.id = (*id_io);
	tMsgReq.class_level_type = D_CLASS_PACKETGEN_ENTRY;
	tMsgReq.message_level_type = D_MSG_GETNEXT_PACKETGEN_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get next packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComPckGenEntry *)&pBobyMsgRcv->info[0];

		memcpy(entry_po,&pInfo->entry,sizeof(MEA_PacketGen_Entry_dbt));
		(*found_o) = pInfo->found;
		(*id_io) = pInfo->id;
	}

	return ret;
}
