#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"


static MEA_Status internal_GW_Global_function(MEA_Unit_t                 		unit,
                                                MEA_Gw_Global_dbt         	*Entry_i,
                                          unsigned int						message_level_type)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
    tCom_Gw_Global_dbt *pGw_Global;
	if(Entry_i == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	

	memset(&tMsgReq,0,sizeof(tMsgReq));

	

    tMsgReq.class_level_type = D_CLASS_GW_GLOBAL_ENTRY;
	tMsgReq.tMsgBody.GW_global.unit = unit;
	tMsgReq.message_level_type = message_level_type;
    memcpy(&tMsgReq.tMsgBody.GW_global.entry, Entry_i, sizeof(MEA_Gw_Global_dbt));
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];



	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to Get Set Global GW\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
    if (message_level_type == D_MSG_GET_GW_GLOBAL_ENTRY){
        pGw_Global = (tCom_Gw_Global_dbt *)&pBobyMsgRcv->info[0];
        memcpy(Entry_i, &pGw_Global->entry, sizeof(MEA_Gw_Global_dbt));
    }


	return ret;
}





MEA_Status MEA_API_COMM_Set_GW_Global_Entry(MEA_Unit_t      unit,
    MEA_Gw_Global_dbt         *entry)
{

    return internal_GW_Global_function(unit, entry, D_MSG_SET_GW_GLOBAL_ENTRY);

}

MEA_Status MEA_API_COMM_Get_GW_Global_Entry(MEA_Unit_t                 unit,
    MEA_Gw_Global_dbt          *entry)
{


    return internal_GW_Global_function(unit, entry, D_MSG_GET_GW_GLOBAL_ENTRY);

}




static MEA_Status internal_RootFilter_Entry_function(MEA_Unit_t                 		unit,
    MEA_root_Filter_type_t     index,
    MEA_Bool         	*enable,
    unsigned int						message_level_type)
{
    tMsgReq tMsgReq;
    tMsgBody *pBobyMsgRcv;
    char bufferRcv[BUFFER_SIZE];
    MEA_Status ret = MEA_OK;
    tCom_rootFilterEntry_dbt *prootFilterEntry;
    if (enable == NULL)
    {
        fprintf(stdout, "received NULL data entry\r\n");
        return ret;
    }



    memset(&tMsgReq, 0, sizeof(tMsgReq));



    tMsgReq.class_level_type = D_CLASS_ROOT_FILTER_ENTRY;
    tMsgReq.tMsgBody.rootFilterEntry.unit = unit;
    tMsgReq.tMsgBody.rootFilterEntry.index = index;
    tMsgReq.message_level_type = message_level_type;
    tMsgReq.tMsgBody.rootFilterEntry.enable = *enable;

    

    tMsgReq.MaxBufferRcv = BUFFER_SIZE;
    tMsgReq.bufferRcv = &bufferRcv[0];



    ret = mea_client_build_packet(&tMsgReq);

    if (ret != MEA_OK)
    {
        fprintf(stdout, "failed to Get Set Global GW\r\n");
        return ret;
    }
    pBobyMsgRcv = (tMsgBody *)bufferRcv;
    ret = pBobyMsgRcv->hdr.ret;
    if (ret != MEA_OK)
    {
        fprintf(stdout, "message received with error 0x%x\r\n", pBobyMsgRcv->hdr.message_result);
        return ret;
    }
    if (message_level_type == D_MSG_GET_ROOT_FILTER_ENTRY ){
        prootFilterEntry = (tCom_rootFilterEntry_dbt *)&pBobyMsgRcv->info[0]; 
        *enable = prootFilterEntry->enable;

    }


    return ret;
}


MEA_Status MEA_API_COMM_Set_RootFilter_Entry(MEA_Unit_t                 unit,
    MEA_root_Filter_type_t     index,
    MEA_Bool                   enable)
{

    return internal_RootFilter_Entry_function(unit, index, &enable, D_MSG_SET_ROOT_FILTER_ENTRY);

}

MEA_Status MEA_API_COMM_Get_RootFilter_Entry(MEA_Unit_t                  unit,
    MEA_root_Filter_type_t      index,
    MEA_Bool                    *enable)
{


    return internal_RootFilter_Entry_function(unit, index,  enable, D_MSG_GET_ROOT_FILTER_ENTRY);
    
}




