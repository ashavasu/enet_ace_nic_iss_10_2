//#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#if defined(MEA_OS_LINUX)
#include <sched.h>

#include <string.h>
#include <sys/errno.h>
#include <sys/ioctl.h>
#include <getopt.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#endif

#include "mea_api.h"
#include "mea_comm_msg.h"

unsigned int gSequence_id=0;


#define D_COMMCLIENT_UDP_SOCKET D_COMMMESSAGE_UDP_SOCKET+1


MEA_Status mea_client_send_udp_packet(char *bufferSnd,int sendlen,char *bufferRcv,int maxRcvBuff,int *pByteRvc)
{
	int sock;
	struct sockaddr_in server;
	struct sockaddr_in client;
	int serverlen,clientlen;
	fd_set readSet;
	struct timeval tv;
	int ret_code=0;
	int wait_for_message_receive=5;
	MEA_Status ret= MEA_OK;
	int retval;


	// create UDP socket
	if((sock = socket(PF_INET, SOCK_DGRAM,IPPROTO_UDP)) < 0)
	{
		printf("failed to create socket\r\n");
		return MEA_ERROR;
	}

	memset(&server,0,sizeof(server));
	memset(&client,0,sizeof(client));

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = D_COMMCLIENT_UDP_SOCKET;

	//setting to client parameters in case send the packet first
	client.sin_family = AF_INET;
	//client.sin_addr.s_addr = inet_addr("127.0.0.1");
	//inet_aton("127.0.0.1",&client.sin_addr);
    
    client.sin_port = D_COMMMESSAGE_UDP_SOCKET;

	//bind the socket
	serverlen = sizeof(struct sockaddr_in);
	if((ret_code = bind(sock,(struct sockaddr *) &server,serverlen)) < 0)
	{
		fprintf(stdout,"failed to bind server socket ret code %d\r\n",errno);
		ret= MEA_ERROR;
		goto exit_code;
	}

	// first send message
	if((ret_code = sendto(sock,bufferSnd,sendlen,0,(struct sockaddr *)&client,sizeof(client))) != sendlen)
	{
		fprintf(stdout,"failed sending packet %d\r\n",errno);
		ret= MEA_ERROR;
		goto exit_code;
	}


	while(wait_for_message_receive)
	{
		FD_ZERO(&readSet);
		FD_SET(sock,&readSet);

		wait_for_message_receive--;


		tv.tv_sec = 10;
		tv.tv_usec = 0;

		retval = select(sock+1,&readSet,NULL,NULL,&tv);
		if(retval > 0)
		{
			if(FD_ISSET(sock,&readSet))
			{
				// received packet
				clientlen = sizeof(client);
				if(((*pByteRvc) = recv(sock,bufferRcv,maxRcvBuff,0)) < 0)
				{
					fprintf(stdout,"failed to received message\r\n");
					wait_for_message_receive=0;
					ret= MEA_ERROR;
				}
			}
			else
			{
				fprintf(stdout,"FD_ISSET error retval:%d\r\n",retval);
				wait_for_message_receive=0;
				ret= MEA_ERROR;
			}
		}
		else if(retval == 0)
		{
			fprintf(stdout,"got timeout\r\n");
			wait_for_message_receive=0;
			ret= MEA_ERROR;
		}
#if defined(MEA_OS_LINUX)
		else if(errno == EINTR)
		{
			fprintf(stdout,"received EINTR try again\r\n");
		}
		else
		{
			fprintf(stdout,"============ MESSAGE NOT RECEIVED error:%s\r\n",strerror(errno));
			wait_for_message_receive=0;
			ret= MEA_ERROR;
		}
#endif
	}
exit_code:
	close(sock);
	return ret;
}

MEA_Status mea_client_build_packet(tMsgReq *pMsgReq)
{
	tMsgBody *pBobyMsgSend;
	char bufferSnd[BUFFER_SIZE];
	int snd_len=0;
	MEA_Status ret=MEA_OK;



	memset(pMsgReq->bufferRcv,0,pMsgReq->MaxBufferRcv);
	memset(bufferSnd,0,sizeof(BUFFER_SIZE));

	pBobyMsgSend = (tMsgBody *)bufferSnd;

	pBobyMsgSend->hdr.sequence_id = gSequence_id++;
	pBobyMsgSend->hdr.class_level_type = pMsgReq->class_level_type;
	pBobyMsgSend->hdr.message_level_type = pMsgReq->message_level_type;
	snd_len = sizeof(tMsgBody);

	memcpy(&pBobyMsgSend->info[0],&pMsgReq->tMsgBody,sizeof(tMsgCommDataBody));

	snd_len += sizeof(tMsgCommDataBody);

	ret = mea_client_send_udp_packet(bufferSnd,snd_len,pMsgReq->bufferRcv,pMsgReq->MaxBufferRcv,&pMsgReq->ActBufferRcv);



	if(ret == MEA_OK)
	{
		fprintf(stdout,"succeeded to communication with server received %d bytes\r\n",pMsgReq->ActBufferRcv);
	}
	else
	{
		ret = MEA_ERROR;
		fprintf(stdout,"failed to communication with server\r\n");
	}
	return ret;
}


