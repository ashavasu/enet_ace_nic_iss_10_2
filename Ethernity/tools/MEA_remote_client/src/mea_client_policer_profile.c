#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"


static MEA_Status internal_Policer_ACM_function(MEA_Unit_t                 		unit,
                                          MEA_Uint16                    	*policer_id,
                                          MEA_AcmMode_t             		ACM_Mode,
                                          MEA_IngressPort_Proto_t        	port_proto_prof_i,
                                          MEA_Policer_Entry_dbt         	*Entry_i,
                                          unsigned int						message_level_type)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPolicerProfile *pPolicerProfileEntry;

//	if(Entry_i == NULL)
//	{
//		fprintf(stdout,"received NULL data entry\r\n");
//		return ret;
//	}

	fprintf(stdout,"profile_id=%d\r\n",*policer_id);

	memset(&tMsgReq,0,sizeof(tMsgReq));

	if((*policer_id) == 0)
	{
		(*policer_id) = MEA_PLAT_GENERATE_NEW_ID;
	}

	tMsgReq.tMsgBody.policerProfile.unit = unit;
	tMsgReq.tMsgBody.policerProfile.policer_id = (*policer_id);
	tMsgReq.tMsgBody.policerProfile.ACM_Mode = ACM_Mode;
	tMsgReq.tMsgBody.policerProfile.port_proto_prof_i = port_proto_prof_i;
	tMsgReq.class_level_type = D_CLASS_POLICER_PROFILE;
	tMsgReq.message_level_type = message_level_type;

	if(Entry_i != NULL)
	{
		memcpy(&tMsgReq.tMsgBody.policerProfile.policer_Entry,Entry_i,sizeof(MEA_Policer_Entry_dbt));
	}
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];



	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to create/delete/set policer profile database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	pPolicerProfileEntry = (tComPolicerProfile *)&pBobyMsgRcv->info[0];

	(*policer_id) = pPolicerProfileEntry->policer_id;
	return ret;
}


MEA_Status MEA_API_COMM_Create_Policer_ACM_Profile(MEA_Unit_t                 unit,
                                          MEA_Uint16                    	*policer_id,
                                          MEA_AcmMode_t             		ACM_Mode,
                                          MEA_IngressPort_Proto_t        	port_proto_prof_i,
                                          MEA_Policer_Entry_dbt         	*Entry_i )
{

	return internal_Policer_ACM_function(unit,
	                                     policer_id,
	                                     ACM_Mode,
	                                     port_proto_prof_i,
	                                     Entry_i,
	                                     D_MSG_CREATE_POLICER_PROFILE);
}

MEA_Status MEA_API_COMM_Set_Policer_ACM_Profile(MEA_Unit_t                 	unit,
                                       MEA_Uint16                     		policer_id,
                                       MEA_AcmMode_t             			ACM_Mode,
                                       MEA_IngressPort_Proto_t        		port_proto_prof_i,
                                       MEA_Policer_Entry_dbt         		*Entry_i )
{

	return internal_Policer_ACM_function(unit,
	                                     &policer_id,
	                                     ACM_Mode,
	                                     port_proto_prof_i,
	                                     Entry_i,
	                                     D_MSG_SET_POLICER_PROFILE);

}

MEA_Status MEA_API_COMM_Delete_Policer_ACM_Profile(MEA_Unit_t              	unit,
                                          MEA_Uint16                		policer_id,
                                          MEA_AcmMode_t          			ACM_Mode)
{

	return internal_Policer_ACM_function(unit,
	                                     &policer_id,
	                                     ACM_Mode,
	                                     0,
	                                     0,
	                                     D_MSG_DELETE_POLICER_PROFILE);

}

MEA_Status MEA_API_COMM_Get_Policer_ACM_Profile(MEA_Unit_t                  unit,
                                       MEA_Uint16                      		policer_id,
                                       MEA_AcmMode_t              			ACM_Mode,
                                       MEA_Policer_Entry_dbt          		*Entry_o )
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPolicerProfile  *pPolicerProfile;

	if(Entry_o == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.policerProfile.unit = unit;
	tMsgReq.tMsgBody.policerProfile.policer_id = policer_id;
	tMsgReq.class_level_type = D_CLASS_POLICER_PROFILE;
	tMsgReq.message_level_type = D_MSG_GET_POLICER_PROFILE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pPolicerProfile = (tComPolicerProfile *)&pBobyMsgRcv->info[0];

		memcpy(Entry_o,&pPolicerProfile->policer_Entry,sizeof(MEA_Policer_Entry_dbt));
	}

	return ret;
}


int MEA_API_COMM_GetAll_Policer_ACM_Profile(MEA_Unit_t unit,MEA_Uint16  *pPolicerArray,MEA_Uint32 *pNumOfPolicers)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComIndexDb *pIndexEntry;
	MEA_Uint32 currNumOfProfiles=0;
	tComIndex *pNextPort;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.policerProfile.unit = unit;
    tMsgReq.class_level_type = D_CLASS_POLICER_PROFILE;
    tMsgReq.message_level_type = D_MSG_GET_ALL_POLICER_IDS;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];



	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}

	fprintf(stdout,"receive size %d\r\n",tMsgReq.ActBufferRcv);


	pBobyMsgRcv = (tMsgBody *)bufferRcv;

	ret = pBobyMsgRcv->hdr.ret;


	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}
	else
	{
		pIndexEntry = (tComIndexDb *)&pBobyMsgRcv->info[0];
		pNextPort = (tComIndex *)&pIndexEntry->next[0];

		fprintf(stdout,"number of policers %d\r\n",pIndexEntry->number_of_ports);

        for (currNumOfProfiles = 0; currNumOfProfiles<(MEA_Uint32)pIndexEntry->number_of_ports; currNumOfProfiles++)
		{
			if(currNumOfProfiles < (*pNumOfPolicers))
			{
				pPolicerArray[currNumOfProfiles] = pNextPort->ingress_port;
				pNextPort = (tComIndex *)&pNextPort->next[0];
			}
			else
			{
				break;
			}
		}
		(*pNumOfPolicers) = currNumOfProfiles;


		fprintf(stdout,"receive number of profiles %d\r\n",pIndexEntry->number_of_ports);

		//buildXml_policer_profiles_id(pIndexEntry,file_name);
		// build XML file
	}
	return ret;
}

