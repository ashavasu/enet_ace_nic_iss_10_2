#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"
#include "comm_mea_api.h"

MEA_Status MEA_API_COMM_Create_Service_vlan(MEA_Unit_t  unit,MEA_Service_t  *serviceId,tServiceVlanParams   *entry)
{
	MEA_Service_Entry_Key_dbt           	key;
	MEA_Service_Entry_Data_dbt          	data;
	MEA_EgressHeaderProc_Array_Entry_dbt   	EHP_Entry;
    MEA_EHP_Info_dbt 						ehp_info[D_MAX_NUM_OF_ACTION_EDITINGS];    //max num EHP
    MEA_Status								ret=0;


    memset(&key,0,sizeof(MEA_Service_Entry_Key_dbt));
    memset(&data,0,sizeof(MEA_Service_Entry_Data_dbt));
    memset(&EHP_Entry,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
    memset(&ehp_info,0,sizeof(MEA_EHP_Info_dbt)*D_MAX_NUM_OF_ACTION_EDITINGS);

    EHP_Entry.ehp_info = &ehp_info[0];
    EHP_Entry.num_of_entries=0;


    key.L2_protocol_type=entry->eIngressType;
    key.src_port         = entry->srcPort;

    memcpy(&ehp_info[0].output_info,&entry->outputPort,sizeof(MEA_OutPorts_Entry_dbt));


	switch(key.L2_protocol_type)
	{
		case MEA_PARSING_L2_KEY_Untagged:
			key.pri=7;
			key.net_tag = MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
			break;
		case MEA_PARSING_L2_KEY_Ctag:
			key.net_tag = entry->outer_vlanId & 0xFFF;
			key.net_tag |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
			key.evc_enable = MEA_TRUE;

			break;
		case MEA_PARSING_L2_KEY_Stag:
			key.net_tag = entry->outer_vlanId & 0xFFF;
			key.net_tag |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
			key.evc_enable = MEA_TRUE;
			break;
		case MEA_PARSING_L2_KEY_Stag_Ctag:
			key.inner_netTag_from_value = entry->inner_vlanId & 0xFFF;
			key.net_tag = entry->outer_vlanId & 0xFFF;

			key.net_tag   = entry->outer_vlanId;
			key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
			key.inner_netTag_from_valid =MEA_TRUE;
			key.inner_netTag_from_value |= 0xFFF000;
			key.external_internal = MEA_FALSE;
			key.evc_enable = MEA_TRUE;
			break;
		default:
			break;
	}
	if(entry->priority_aware == MEA_TRUE)
	{
		key.evc_enable = MEA_FALSE;
		key.priType = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP;
		key.pri=entry->filter_priority;
		
	}
	if(entry->filter_enable == MEA_TRUE)
	{
// 		data.filter_enable=MEA_TRUE;
// 		data.filter_key_type=entry->filter_id;
// 		data.filter_mask.mask_0_31 =entry->filter_mask[0];
// 		data.filter_mask.mask_32_63 =entry->filter_mask[1];
	}
	ehp_info[EHP_Entry.num_of_entries].ehp_data.wbrg_info.val.data_wbrg.flow_type = entry->vlanEdit.flow_type;
	if(EHP_Entry.ehp_info[EHP_Entry.num_of_entries].ehp_data.wbrg_info.val.data_wbrg.flow_type == MEA_EHP_FLOW_TYPE_QTAG)
	{
		if(entry->vlanEdit.inner_edit_command == MEA_EGRESS_HEADER_PROC_CMD_TRANS)
		{
			ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.command = entry->vlanEdit.outer_edit_command;
			ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.val.vlan.vid = entry->vlanEdit.outer_vlan;
			ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		}
		else
		{
			ehp_info[EHP_Entry.num_of_entries].ehp_data.atm_info.double_tag_cmd = entry->vlanEdit.outer_edit_command;
			ehp_info[EHP_Entry.num_of_entries].ehp_data.atm_info.val.double_vlan_tag.vid = entry->vlanEdit.outer_vlan;
			ehp_info[EHP_Entry.num_of_entries].ehp_data.atm_info.val.double_vlan_tag.eth_type = 0x8100;
			ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.command = entry->vlanEdit.inner_edit_command;
			ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.val.vlan.vid = entry->vlanEdit.inner_vlan;
			ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		}

		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
	}
	else if(ehp_info[EHP_Entry.num_of_entries].ehp_data.wbrg_info.val.data_wbrg.flow_type == MEA_EHP_FLOW_TYPE_TAG)
	{
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.command = entry->vlanEdit.outer_edit_command;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.val.vlan.vid = entry->vlanEdit.outer_vlan;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		if(entry->priority_edit_enable == MEA_TRUE)
		{
			ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.val.vlan.pri = entry->edit_priority;
		}
	}
	else // untagged
	{
		ehp_info[EHP_Entry.num_of_entries].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.command           = entry->vlanEdit.outer_edit_command;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.val.vlan.vid      = 0;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.stamp_color       = MEA_FALSE;
		ehp_info[EHP_Entry.num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_FALSE;

	}
	EHP_Entry.num_of_entries++;


	data.DSE_forwarding_enable   = MEA_TRUE;
	data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	data.DSE_learning_enable     = MEA_FALSE;
	data.DSE_learning_actionId_valid = MEA_FALSE;
	data.DSE_learning_srcPort_force = MEA_FALSE;

	data.vpn = entry->vpnId;

	data.tmId   = 0;
	data.tmId_disable = MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL;
	data.pmId = entry->pmId;
	data.editId = 0;

    /* Set the other upstream service data attributes to defaults */
	data.ADM_ENA            = MEA_FALSE;
	data.CM                 = MEA_FALSE;
	data.L2_PRI_FORCE       = MEA_FALSE;
	data.L2_PRI             = 0;
	data.COLOR_FORSE        = MEA_TRUE;
	data.COLOR              = 2;
	data.COS_FORCE          = MEA_FALSE;
	data.COS                = 0;
	data.protocol_llc_force = MEA_TRUE;
	data.Protocol           = 1;
	data.Llc                = MEA_FALSE;


    ret = MEA_API_COMM_Create_Service(unit,&key,&data,&entry->outputPort,entry->policerId,&EHP_Entry,serviceId);

	return ret;

}

MEA_Status MEA_API_COMM_Create_Service (MEA_Unit_t                           unit,
                                   MEA_Service_Entry_Key_dbt           *key,
                                   MEA_Service_Entry_Data_dbt          *data,
                                   MEA_OutPorts_Entry_dbt              *OutPorts_Entry,
                                   MEA_Uint16                     	   policer_id,
                                   MEA_EgressHeaderProc_Array_Entry_dbt   *EHP_Entry,
                                   MEA_Service_t                       *o_serviceId)
{

	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComService  *pService;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.service.unit = unit;
	tMsgReq.tMsgBody.service.key_valid=0;
	if(key != NULL)
	{
		tMsgReq.tMsgBody.service.key_valid=1;
		memcpy(&tMsgReq.tMsgBody.service.key,key,sizeof(MEA_Service_Entry_Key_dbt));
	}
	tMsgReq.tMsgBody.service.data_valid=0;
	if(data != NULL)
	{
		tMsgReq.tMsgBody.service.data_valid=1;
		memcpy(&tMsgReq.tMsgBody.service.data,data,sizeof(MEA_Service_Entry_Data_dbt));
		data->policer_prof_id=policer_id;
	}
	tMsgReq.tMsgBody.service.outport_valid = 0;
	if(OutPorts_Entry != NULL)
	{
		tMsgReq.tMsgBody.service.outport_valid=1;
		memcpy(&tMsgReq.tMsgBody.service.OutPorts_Entry,OutPorts_Entry,sizeof(MEA_OutPorts_Entry_dbt));
	}
	tMsgReq.tMsgBody.service.num_of_entries=0;
	if(EHP_Entry != NULL)
	{
		if(EHP_Entry->num_of_entries > D_MAX_NUM_OF_ACTION_EDITINGS)
		{
			fprintf(stdout,"number of entries cannot be greater then %d\r\n",D_MAX_NUM_OF_ACTION_EDITINGS);
			return ret;
		}
		tMsgReq.tMsgBody.service.num_of_entries = EHP_Entry->num_of_entries;
		memcpy(&tMsgReq.tMsgBody.service.ehp_info[0],EHP_Entry->ehp_info,sizeof(MEA_EHP_Info_dbt)*EHP_Entry->num_of_entries);
	}
	tMsgReq.tMsgBody.service.policer_id = policer_id;
	tMsgReq.tMsgBody.service.serviceId=0;
	if(o_serviceId != NULL)
	{
		tMsgReq.tMsgBody.service.serviceId = *o_serviceId;
	}

	tMsgReq.class_level_type = D_CLASS_SERVICE_ID;
	tMsgReq.message_level_type = D_MSG_CREATE_SERVICE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	fprintf(stdout,"profile id %d\r\n",tMsgReq.tMsgBody.service.policer_id);

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pService = (tComService *)&pBobyMsgRcv->info[0];
		if(o_serviceId != NULL)
		{
			(*o_serviceId) =  pService->serviceId;
		}
		fprintf(stdout,"create service id:%d\r\n",pService->serviceId);
	}

	return ret;
}

/* Note : Only ServiceId is mandatory */
MEA_Status  MEA_API_COMM_Set_Service    (MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Uint16                     	   policer_id,
                                    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.service.unit = unit;
	tMsgReq.tMsgBody.service.key_valid=0;
	tMsgReq.tMsgBody.service.data_valid=0;
	if(data != NULL)
	{
		tMsgReq.tMsgBody.service.data_valid=1;
		memcpy(&tMsgReq.tMsgBody.service.data,data,sizeof(MEA_Service_Entry_Data_dbt));
	}
	tMsgReq.tMsgBody.service.outport_valid = 0;
	if(OutPorts_Entry != NULL)
	{
		tMsgReq.tMsgBody.service.outport_valid=1;
		memcpy(&tMsgReq.tMsgBody.service.OutPorts_Entry,OutPorts_Entry,sizeof(MEA_OutPorts_Entry_dbt));
	}
	tMsgReq.tMsgBody.service.num_of_entries=0;
	if(EHP_Entry != NULL)
	{
		if(EHP_Entry->num_of_entries > D_MAX_NUM_OF_ACTION_EDITINGS)
		{
			fprintf(stdout,"number of entries cannot be greater then %d\r\n",D_MAX_NUM_OF_ACTION_EDITINGS);
			return ret;
		}
		tMsgReq.tMsgBody.service.num_of_entries = EHP_Entry->num_of_entries;
		memcpy(&tMsgReq.tMsgBody.service.ehp_info[0],&EHP_Entry->ehp_info[0],sizeof(MEA_EHP_Info_dbt)*EHP_Entry->num_of_entries);
	}
	tMsgReq.tMsgBody.service.policer_id = policer_id;
	tMsgReq.tMsgBody.service.serviceId = serviceId;

	tMsgReq.class_level_type = D_CLASS_SERVICE_ID;
	tMsgReq.message_level_type = D_MSG_SET_SERVICE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	//fprintf(stdout,"succeeded to set num of entries %lu\r\n",tMsgReq.tMsgBody.service.num_of_entries);
	//fprintf(stdout,"service id %d\r\n",serviceId);
	return ret;
}

/* Note : Only ServiceId is mandatory */
MEA_Status  MEA_API_COMM_Get_Service    (MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Key_dbt      *key,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Uint16                     *policer_id,
                                    MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComService  *pService;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.service.unit = unit;

	tMsgReq.tMsgBody.service.serviceId = serviceId;

	tMsgReq.class_level_type = D_CLASS_SERVICE_ID;
	tMsgReq.message_level_type = D_MSG_GET_SERVICE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pService = (tComService *)&pBobyMsgRcv->info[0];

		if(key != NULL)
		{
			memcpy(key,&pService->key,sizeof(MEA_Service_Entry_Key_dbt));
		}
		if(data != NULL)
		{
			memcpy(data,&pService->data,sizeof(MEA_Service_Entry_Data_dbt));
		}
		if(OutPorts_Entry != NULL)
		{
			memcpy(OutPorts_Entry,&pService->OutPorts_Entry,sizeof(MEA_OutPorts_Entry_dbt));
		}
		if(policer_id != NULL)
		{
			(*policer_id) = pService->policer_id;
		}
		if(EHP_Entry != NULL)
		{
			if(EHP_Entry->num_of_entries > pService->num_of_entries)
			{
				EHP_Entry->num_of_entries = pService->num_of_entries;
			}
			memcpy(&EHP_Entry->ehp_info[0],&pService->ehp_info[0],sizeof(MEA_EHP_Info_dbt)*EHP_Entry->num_of_entries);
		}
	}


	//fprintf(stdout,"succeeded to get num of entries %lu\r\n",pService->num_of_entries);
	//fprintf(stdout,"max num of clusters %lu\r\n",pService->num_of_clusters);
	//fprintf(stdout,"service id %d\r\n",serviceId);
	return ret;

}

MEA_Status  MEA_API_COMM_Get_Service_vlan    (MEA_Unit_t   unit,
                                    			MEA_Service_t          serviceId,
                                    			tServiceVlanParams   *entry)
{
	MEA_Status ret=MEA_OK;
	MEA_Service_Entry_Key_dbt       		key;
	MEA_Service_Entry_Data_dbt     			data;
	MEA_EgressHeaderProc_Array_Entry_dbt  	EHP_Entry;
	MEA_EHP_Info_dbt 						ehp_info[8];

	EHP_Entry.ehp_info = &ehp_info[0];
	EHP_Entry.num_of_entries = D_MAX_NUM_OF_ACTION_EDITINGS;

	ret =MEA_API_COMM_Get_Service(unit,serviceId,
	                                 	 &key,
	                                 	 &data,
	                                    &entry->outputPort,
	                                    &entry->policerId,
	                                    &EHP_Entry);
	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get service vlan\r\n");
		return ret;
	}

	entry->inner_vlanId=0;
	entry->outer_vlanId=0;
	entry->eIngressType=key.L2_protocol_type;
	entry->srcPort = key.src_port;

	switch(key.L2_protocol_type)
	{
		case MEA_PARSING_L2_KEY_Untagged:
			break;
		case MEA_PARSING_L2_KEY_Ctag:
			entry->outer_vlanId=key.net_tag & 0xFFF;
			break;
		case MEA_PARSING_L2_KEY_Stag:
			entry->outer_vlanId=key.net_tag & 0xFFF;
			break;
		case MEA_PARSING_L2_KEY_Stag_Ctag:
			entry->inner_vlanId=key.inner_netTag_from_value & 0xFFF;
			entry->outer_vlanId=key.net_tag & 0xFFF;
			break;
		default:
			break;
	}

	if(key.evc_enable == MEA_FALSE)
	{
		entry->priority_aware = MEA_TRUE;
		entry->filter_priority=key.pri;

	}
	if(data.filter_enable==MEA_TRUE)
	{
// 		entry->filter_enable =MEA_TRUE;
// 		entry->filter_id=data.filter_key_type;
// 		entry->filter_mask[0]=data.filter_mask.mask_0_31;
// 		entry->filter_mask[1]=data.filter_mask.mask_32_63;
	}

	entry->num_of_clusters = 255;

	entry->vlanEdit.flow_type = 0;
	entry->vlanEdit.outer_edit_command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	entry->vlanEdit.outer_vlan = 0;
	entry->vlanEdit.inner_edit_command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	entry->vlanEdit.inner_vlan = 0;
	if(EHP_Entry.num_of_entries > 0)
	{
		entry->vlanEdit.flow_type = EHP_Entry.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type;
		if(EHP_Entry.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type == MEA_EHP_FLOW_TYPE_QTAG)
		{
			entry->vlanEdit.outer_edit_command = EHP_Entry.ehp_info[0].ehp_data.atm_info.double_tag_cmd;
			entry->vlanEdit.outer_vlan = EHP_Entry.ehp_info[0].ehp_data.atm_info.val.double_vlan_tag.vid;
			entry->vlanEdit.inner_edit_command = EHP_Entry.ehp_info[0].ehp_data.eth_info.command;
			entry->vlanEdit.inner_vlan = EHP_Entry.ehp_info[0].ehp_data.eth_info.val.vlan.vid;
		}
		if(EHP_Entry.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type == MEA_EHP_FLOW_TYPE_TAG)
		{
			entry->vlanEdit.outer_edit_command = EHP_Entry.ehp_info[0].ehp_data.eth_info.command;
			entry->vlanEdit.outer_vlan = EHP_Entry.ehp_info[0].ehp_data.eth_info.val.vlan.vid;
		}
	}



	entry->pmId = data.pmId;


	return ret;

}


MEA_Status  MEA_API_COMM_Delete_Service (MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.service.unit = unit;

	tMsgReq.tMsgBody.service.serviceId = serviceId;

	tMsgReq.class_level_type = D_CLASS_SERVICE_ID;
	tMsgReq.message_level_type = D_MSG_DELETE_SERVICE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	return ret;
}

MEA_Status  MEA_API_COMM_GetRange_Service (MEA_Unit_t  unit,MEA_Uint32 serviceStart,MEA_Service_t  *serviceIdArry,MEA_Uint32 *pMaxEntries)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComIndexDb *pIndexEntry;
	MEA_Uint32 max_num_of_service=0;
	int i=0;
	tComIndex *pNextPort;

	if(serviceIdArry == NULL)
	{
		fprintf(stdout,"serviceArry is NULL\r\n");
		return MEA_ERROR;
	}

	if(pMaxEntries == NULL)
	{
		fprintf(stdout,"pMaxEntries is NULL\r\n");
		return MEA_ERROR;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.policerProfile.unit = unit;
    tMsgReq.tMsgBody.service.serviceId = (MEA_Service_t)serviceStart;
	tMsgReq.class_level_type = D_CLASS_SERVICE_ID;
	tMsgReq.message_level_type = D_MSG_GETRANGE_SERVICE;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	fprintf(stdout,"receive size %d\r\n",tMsgReq.ActBufferRcv);


	pBobyMsgRcv = (tMsgBody *)bufferRcv;

	ret = pBobyMsgRcv->hdr.ret;

	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	pIndexEntry = (tComIndexDb *)&pBobyMsgRcv->info[0];

	fprintf(stdout,"number of ports %d\r\n",pIndexEntry->number_of_ports);

	pNextPort = (tComIndex *)&pIndexEntry->next[0];

	for(i=0; i< pIndexEntry->number_of_ports;i++)
	{
		if(max_num_of_service < (*pMaxEntries))
		{
			serviceIdArry[max_num_of_service++] = pNextPort->ingress_port;
//			max_num_of_service++;
		}
		else
		{
			break;
		}
		pNextPort = (tComIndex *)&pNextPort->next[0];
	}
	(*pMaxEntries) = max_num_of_service;
	return ret;

}

int getall_serviceId(int unit,char *file_name)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComIndexDb *pIndexEntry;


	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.policerProfile.unit = unit;
	tMsgReq.class_level_type = D_CLASS_SERVICE_ID;
	tMsgReq.message_level_type = D_MSG_GET_ALL_SERVICES;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];



	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}

	fprintf(stdout,"receive size %d\r\n",tMsgReq.ActBufferRcv);


	pBobyMsgRcv = (tMsgBody *)bufferRcv;

	ret = pBobyMsgRcv->hdr.ret;

	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}
	else
	{
		pIndexEntry = (tComIndexDb *)&pBobyMsgRcv->info[0];



	}
	return ret;
}

