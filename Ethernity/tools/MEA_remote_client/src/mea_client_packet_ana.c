#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"



MEA_Status MEA_API_COMM_Create_CCM_Entry(MEA_Unit_t                  unit_i,
                                    MEA_CCM_Configure_dbt      *entry_pi,
                                    MEA_CcmId_t                *id_io)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComCcmEntry  *pInfo;


	if(entry_pi == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckCcmEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckCcmEntry.id = (*id_io);
	tMsgReq.class_level_type = D_CLASS_PACKETCCM_ENTRY;
	tMsgReq.message_level_type = D_MSG_CREATE_PACKETCCM_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	memcpy(&tMsgReq.tMsgBody.pckCcmEntry.entry,entry_pi,sizeof(MEA_CCM_Configure_dbt));
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set packet generator profile database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	pInfo = (tComCcmEntry *)&pBobyMsgRcv->info[0];
	(*id_io) = pInfo->id;
	return ret;

}

MEA_Status MEA_API_COMM_Set_CCM_Entry(MEA_Unit_t                     unit_i,
                                 MEA_CcmId_t                    id_i,
                                 MEA_CCM_Configure_dbt         *entry_pi)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	if(entry_pi == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckCcmEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckCcmEntry.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKETCCM_ENTRY;
	tMsgReq.message_level_type = D_MSG_SET_PACKETCCM_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	memcpy(&tMsgReq.tMsgBody.pckCcmEntry.entry,entry_pi,sizeof(MEA_CCM_Configure_dbt));
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set packet generator profile database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}

	return ret;

}

MEA_Status MEA_API_COMM_Delete_CCM_Entry(MEA_Unit_t                unit_i,
                                    MEA_CcmId_t               id_i)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;



	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckCcmEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckCcmEntry.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKETCCM_ENTRY;
	tMsgReq.message_level_type = D_MSG_DELETE_PACKETCCM_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set packet generator profile database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}

	return ret;

}

MEA_Status MEA_API_COMM_Get_CCM_Entry(MEA_Unit_t                     unit_i,
                                 MEA_CcmId_t                    id_i,
                                 MEA_CCM_Configure_dbt         *entry_po)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComCcmEntry  *pInfo;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckCcmEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckCcmEntry.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKETCCM_ENTRY;
	tMsgReq.message_level_type = D_MSG_GET_PACKETCCM_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComCcmEntry *)&pBobyMsgRcv->info[0];
		memcpy(entry_po,&pInfo->entry,sizeof(MEA_CCM_Configure_dbt));
	}

	return ret;
}


MEA_Status MEA_API_COMM_GetFirst_CCM_Entry(MEA_Unit_t                unit_i,
                                      MEA_CcmId_t              *id_o,
                                      MEA_CCM_Configure_dbt    *entry_po,
                                      MEA_Bool                 *found_o)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComCcmEntry  *pInfo;

	(*found_o) = MEA_FALSE;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckCcmEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckCcmEntry.id = *id_o;
	tMsgReq.class_level_type = D_CLASS_PACKETCCM_ENTRY;
	tMsgReq.message_level_type = D_MSG_GETFIRST_PACKETCCM_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get first packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComCcmEntry *)&pBobyMsgRcv->info[0];

		memcpy(entry_po,&pInfo->entry,sizeof(MEA_CCM_Configure_dbt));
		(*found_o) = pInfo->found;
		(*id_o) = pInfo->id;
	}

	return ret;
}


MEA_Status MEA_API_COMM_GetNext_CCM_Entry(MEA_Unit_t                   unit_i,
                                     MEA_CcmId_t                 *id_io,
                                     MEA_CCM_Configure_dbt       *entry_po,
                                     MEA_Bool                    *found_o)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComCcmEntry  *pInfo;

	(*found_o) = MEA_FALSE;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckCcmEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckCcmEntry.id = (*id_io);
	tMsgReq.class_level_type = D_CLASS_PACKETCCM_ENTRY;
	tMsgReq.message_level_type = D_MSG_GETNEXT_PACKETCCM_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get next packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComCcmEntry *)&pBobyMsgRcv->info[0];

		memcpy(entry_po,&pInfo->entry,sizeof(tComCcmEntry));
		(*found_o) = pInfo->found;
		(*id_io) = pInfo->id;
	}

	return ret;
}

/**********************************************************************************/
/*    packet analyzer 																*/
/*********************************************************************************/
MEA_Status MEA_API_COMM_Set_Analyzer(MEA_Unit_t                    unit_i,
                                MEA_Analyzer_t                id_i,
                                MEA_Analyzer_configure_dbt    *entry_pi)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	if(entry_pi == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckAnaEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckAnaEntry.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKET_ANALYZER_ENTRY;
	tMsgReq.message_level_type = D_MSG_SET_PACKET_ANALYZER_ENTRY;
	memcpy(&tMsgReq.tMsgBody.pckAnaEntry.entry,entry_pi,sizeof(MEA_Analyzer_configure_dbt));
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set packet analyzer database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}

	return ret;
}
MEA_Status MEA_API_COMM_Get_Analyzer(MEA_Unit_t                    unit_i,
                                MEA_Analyzer_t                id_i,
                                MEA_Analyzer_configure_dbt    *entry_po)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckAnalyzer  *pInfo;

	if(entry_po == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.pckAnaEntry.unit = unit_i;
	tMsgReq.tMsgBody.pckAnaEntry.id = id_i;
	tMsgReq.class_level_type = D_CLASS_PACKET_ANALYZER_ENTRY;
	tMsgReq.message_level_type = D_MSG_SET_PACKET_ANALYZER_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComPckAnalyzer *)&pBobyMsgRcv->info[0];
		memcpy(entry_po,&pInfo->entry,sizeof(MEA_Analyzer_configure_dbt));
	}

	return ret;
}

MEA_Status MEA_API_COMM_Get_Counters_ANALYZER(MEA_Unit_t  unit_i, MEA_Analyzer_t id_i,MEA_Counters_Analyzer_dbt *pEntry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComPckCounterAna  *pInfo;

	if(pEntry == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.analyzerCounter.unit = unit_i;
	tMsgReq.tMsgBody.analyzerCounter.id = id_i;
	tMsgReq.class_level_type = D_CLASS_ANALYZER_COUNTER_ENTRY;
	tMsgReq.message_level_type = D_MSG_GET_ANALYZER_COUNTER_ENTRY;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get packet generator database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pInfo = (tComPckCounterAna *)&pBobyMsgRcv->info[0];
		memcpy(pEntry,&pInfo->counter,sizeof(MEA_Counters_Analyzer_dbt));
	}

	return ret;
}

