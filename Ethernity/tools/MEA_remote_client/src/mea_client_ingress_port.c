#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mea_api.h"
#include "mea_comm_msg.h"


MEA_Status MEA_API_COMM_Set_IngressPort_Entry(MEA_Unit_t                 unit,
                                         MEA_Port_t                 port,
                                         MEA_IngressPort_Entry_dbt* entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;


	if(entry == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	fprintf(stdout,"ingress port %d\r\n",port);

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.ingressPort.unit = unit;
	tMsgReq.tMsgBody.ingressPort.ingress_port = port;
	tMsgReq.class_level_type = D_CLASS_INGRESS_PORT;
	tMsgReq.message_level_type = D_MSG_SET_INGRESS_CONFIGURATION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	memcpy(&tMsgReq.tMsgBody.ingressPort.portIngress,entry,sizeof(MEA_IngressPort_Entry_dbt));
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to set ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}

	return ret;

}

MEA_Status MEA_API_COMM_Get_IngressPort_Entry(MEA_Unit_t                 unit,
                                         	 	MEA_Port_t                 port,
                                         	 	MEA_IngressPort_Entry_dbt* entry)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComIngressPort  *pIgressPortInfo;

	if(entry == NULL)
	{
		fprintf(stdout,"received NULL data entry\r\n");
		return ret;
	}

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.ingressPort.unit = unit;
	tMsgReq.tMsgBody.ingressPort.ingress_port = port;
	tMsgReq.class_level_type = D_CLASS_INGRESS_PORT;
	tMsgReq.message_level_type = D_MSG_GET_INGRESS_CONFIGURATION;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];

	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}
	pBobyMsgRcv = (tMsgBody *)bufferRcv;
	ret = pBobyMsgRcv->hdr.ret;
	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
		return ret;
	}
	else
	{
		pIgressPortInfo = (tComIngressPort *)&pBobyMsgRcv->info[0];

		memcpy(entry,&pIgressPortInfo->portIngress,sizeof(MEA_IngressPort_Entry_dbt));
	}

	return ret;
}

int get_ingress_port_information(int unit,int 	ingress_port,char *file_name,tComIngressPort *pIgressPortInfo)
{
	MEA_Status ret=MEA_OK;
	MEA_IngressPort_Entry_dbt tEntry;


	ret = MEA_API_COMM_Get_IngressPort_Entry(unit,ingress_port,&tEntry);
	if(ret != MEA_OK)
	{
		return ret;
	}

	fprintf(stdout,"message received unit=%d port=%d\r\n",unit,ingress_port);

	pIgressPortInfo->unit = unit;
	pIgressPortInfo->ingress_port = ingress_port;
	memcpy(&pIgressPortInfo->portIngress,&tEntry,sizeof(MEA_IngressPort_Entry_dbt));
	//ret = buildXml_ingrss_port_info(pIgressPortInfo,file_name);



	return ret;
}

int get_ingress_ports_mapping(int unit,int *portArry,int *pMaxNumberOfPorts)
{
	tMsgReq tMsgReq;
	tMsgBody *pBobyMsgRcv;
	char bufferRcv[BUFFER_SIZE];
	MEA_Status ret=MEA_OK;
	tComIngressPortsDb *pIngressPortEntry;
	tComIngressPortId *pNextPort;
	int i=0;
	int max_allow_ports=0;

	memset(&tMsgReq,0,sizeof(tMsgReq));

	tMsgReq.tMsgBody.ingressPort.unit = unit;
	tMsgReq.class_level_type = D_CLASS_INGRESS_PORT;
	tMsgReq.message_level_type = D_MSG_GET_INGRESS_PORTS_INFO;
	tMsgReq.MaxBufferRcv = BUFFER_SIZE;
	tMsgReq.bufferRcv = &bufferRcv[0];



	ret = mea_client_build_packet(&tMsgReq);

	if(ret != MEA_OK)
	{
		fprintf(stdout,"failed to get ingress port database\r\n");
		return ret;
	}

	fprintf(stdout,"receive size %d\r\n",tMsgReq.ActBufferRcv);


	pBobyMsgRcv = (tMsgBody *)bufferRcv;

	ret = pBobyMsgRcv->hdr.ret;

	if(ret != MEA_OK)
	{
		fprintf(stdout,"message received with error 0x%x\r\n",pBobyMsgRcv->hdr.message_result);
	}
	else
	{
		pIngressPortEntry = (tComIngressPortsDb *)&pBobyMsgRcv->info[0];


		fprintf(stdout,"receive number of ports %d\r\n",pIngressPortEntry->number_of_ports);
		pNextPort = (tComIngressPortId *)pIngressPortEntry->next;
		fprintf(stdout,"ingress port %d\r\n",pNextPort->ingress_port);

		max_allow_ports = pIngressPortEntry->number_of_ports;
		if(pIngressPortEntry->number_of_ports > *pMaxNumberOfPorts)
		{
			max_allow_ports = *pMaxNumberOfPorts;
		}
		for(i=0;i<max_allow_ports;i++)
		{
			portArry[i] = pNextPort->ingress_port;
			pNextPort = (tComIngressPortId *)&pNextPort->next[0];
		}
		(*pMaxNumberOfPorts) = max_allow_ports;

	}
	return ret;
}

