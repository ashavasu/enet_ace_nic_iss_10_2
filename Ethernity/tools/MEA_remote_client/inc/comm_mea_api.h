/*
 * comm_mae_api.h
 *
 *  Created on: 30 ???? 2014
 *      Author: GilM
 */

#ifndef COMM_MAE_API_H_
#define COMM_MAE_API_H_


typedef struct
{
	MEA_Uint32	flow_type;
	MEA_Uint32  inner_edit_command;
	MEA_Uint32	inner_vlan;
	MEA_Uint32  outer_edit_command;
	MEA_Uint32	outer_vlan;
}tEditingVlan;

typedef enum
{
	MEA_INGRESS_SERVICE_UNTAGED,
	MEA_INGRESS_SERVICE_TAGGED,
	MEA_INGRESS_SERVICE_DOUBLE_TAGGED,
}EingressVlan;


typedef struct
{
	MEA_Uint32				srcPort;
	EingressVlan			eIngressType;
	MEA_Uint16 				inner_vlanId;
	MEA_Uint16 				outer_vlanId;
	MEA_Uint32				pmId_valid;
	MEA_Uint16				policerId;
	MEA_PmId_t				pmId;
	MEA_Uint32				num_of_clusters;
	MEA_OutPorts_Entry_dbt	outputPort;
	tEditingVlan			vlanEdit;
	MEA_Uint16				vpnId;
	MEA_Bool				filter_enable;
	MEA_Uint32				filter_id;
	MEA_Uint32				filter_mask[2]; // 0-31 and 32-63
	MEA_Bool				priority_edit_enable;
	MEA_Uint16				edit_priority;
	MEA_Bool				priority_aware;
	MEA_Uint16				filter_priority;				
}tServiceVlanParams;
/************************************************************************/
/*                                                                      */
/************************************************************************/

MEA_Status MEA_API_COMM_Set_IngressPort_Entry(MEA_Unit_t                 unit,
                                         MEA_Port_t                 port,
                                         MEA_IngressPort_Entry_dbt* entry);

MEA_Status MEA_API_COMM_Get_IngressPort_Entry(MEA_Unit_t                 unit,
                                         	 	MEA_Port_t                 port,
                                         	 	MEA_IngressPort_Entry_dbt* entry);

/************************************************************************/
/*                                                                      */
/************************************************************************/


MEA_Status MEA_API_COMM_Set_EgressPort_Entry(MEA_Unit_t                unit,
                                        MEA_Port_t                port,
                                        MEA_EgressPort_Entry_dbt* entry);


MEA_Status MEA_API_COMM_Get_EgressPort_Entry(MEA_Unit_t                unit,
                                        MEA_Port_t                port,
                                        MEA_EgressPort_Entry_dbt* entry);
/************************************************************************/
/*                                                                      */
/************************************************************************/

MEA_Status MEA_API_COMM_Create_Policer_ACM_Profile(MEA_Unit_t                 unit,
                                          MEA_Uint16                    *policer_id,
                                          MEA_AcmMode_t             ACM_Mode,
                                          MEA_IngressPort_Proto_t        port_proto_prof_i,
                                          MEA_Policer_Entry_dbt         *Entry_i );

MEA_Status MEA_API_COMM_Set_Policer_ACM_Profile(MEA_Unit_t                 unit,
                                       MEA_Uint16                     policer_id,
                                       MEA_AcmMode_t             ACM_Mode,
                                       MEA_IngressPort_Proto_t        port_proto_prof_i,
                                       MEA_Policer_Entry_dbt         *Entry_i );

MEA_Status MEA_API_COMM_Delete_Policer_ACM_Profile(MEA_Unit_t              unit,
                                          MEA_Uint16                 policer_id,
                                          MEA_AcmMode_t          ACM_Mode);

MEA_Status MEA_API_COMM_Get_Policer_ACM_Profile(MEA_Unit_t                  unit,
                                       MEA_Uint16                      policer_id,
                                       MEA_AcmMode_t              ACM_Mode,
                                       MEA_Policer_Entry_dbt          *Entry_o );


MEA_Status MEA_API_COMM_GetAll_Policer_ACM_Profile(MEA_Unit_t unit,MEA_Uint16  *pPolicerArray,MEA_Uint32 *pNumOfPolicers);

/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_API_COMM_Create_Service (MEA_Unit_t                           unit,
                                   MEA_Service_Entry_Key_dbt           *key,
                                   MEA_Service_Entry_Data_dbt          *data,
                                   MEA_OutPorts_Entry_dbt              *OutPorts_Entry,
                                   MEA_Uint16                     	   policer_id,
                                   MEA_EgressHeaderProc_Array_Entry_dbt   *EHP_Entry,
                                   MEA_Service_t                       *o_serviceId);

MEA_Status  MEA_API_COMM_Set_Service    (MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Uint16                     	   policer_id,
                                    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry);

MEA_Status  MEA_API_COMM_Get_Service    (MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Key_dbt      *key,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Uint16                     *policer_id,
                                    MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry);

MEA_Status  MEA_API_COMM_Delete_Service (MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId);

MEA_Status  MEA_API_COMM_GetRange_Service (MEA_Unit_t  unit,
											MEA_Uint32 serviceStart,
											MEA_Service_t  *serviceIdArry,
											MEA_Uint32 *pMaxEntries);

MEA_Status  MEA_API_COMM_Get_Service_vlan    (MEA_Unit_t   unit,
                                    			MEA_Service_t          serviceId,
                                    			tServiceVlanParams   *entry);

MEA_Status MEA_API_COMM_Create_Service_vlan(MEA_Unit_t  unit,
											MEA_Service_t  *serviceId,
											tServiceVlanParams   *entry);


/*************************************************************************/
/* action																*/
/*************************************************************************/

MEA_Status MEA_API_COMM_Create_Action (MEA_Unit_t                              unit,
                                   	   MEA_Action_Entry_Data_dbt             *Action_Data_pio,
                                   	   MEA_OutPorts_Entry_dbt                *Action_OutPorts_pi,
                                   	   MEA_Uint16                     	   	 policer_id,
                                   	   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_pi,
                                   	   MEA_Action_t                          *Action_Id_io);

MEA_Status  MEA_API_COMM_Set_Action(MEA_Unit_t                             unit,
                                   	MEA_Action_t                           Action_Id_i,
                                   	MEA_Action_Entry_Data_dbt             *Action_Data_pio,
                                   	MEA_OutPorts_Entry_dbt                *Action_OutPorts_pi,
                                   	MEA_Uint16                     	   	 	policer_id,
                                   	MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_pi);

MEA_Status  MEA_API_COMM_Get_Action(MEA_Unit_t                             unit,
                                   MEA_Action_t                           Action_Id_i,
                                   MEA_Action_Entry_Data_dbt             *Action_Data_po,
                                   MEA_OutPorts_Entry_dbt                *Action_OutPorts_po,
                                   MEA_Uint16                     	   	 *policer_id,
                                   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_po);

MEA_Status  MEA_API_COMM_Delete_Action (MEA_Unit_t                       unit,
                                   MEA_Action_t                          Action_Id_i);

MEA_Status MEA_API_COMM_GetFirst_Action (MEA_Unit_t                     unit_i,
                                    MEA_Action_t                        *Action_Id_o ,
                                    MEA_Bool                            *found_o);

MEA_Status MEA_API_COMM_GetNext_Action  (MEA_Unit_t                      unit_i,
                                    MEA_Action_t                        *Action_Id_io,
                                    MEA_Bool                            *found_o);

/************************************************************************/
/*  PM Counters                                                         */
/************************************************************************/
MEA_Status MEA_API_COMM_Clear_Counters_PM(MEA_Unit_t                    unit,
                                          MEA_PmId_t                    pmId);

MEA_Status MEA_API_COMM_Clear_Counters_PMs(MEA_Unit_t                    unit);
MEA_Status MEA_API_COMM_Get_Counters_PM(MEA_Unit_t                    unit,
                                        MEA_PmId_t                    pmId,
                                        MEA_Counters_PM_dbt          *entry);

MEA_Status MEA_API_COMM_GetFirst_Counters_PM(MEA_Unit_t                    unit,
                                            MEA_PmId_t                    *pmId,
                                            MEA_Counters_PM_dbt          *entry,
                                            MEA_Bool                     *valid);

MEA_Status MEA_API_COMM_GetNext_Counters_PM(MEA_Unit_t                    unit,
                                            MEA_PmId_t                    *pmId,
                                            MEA_Counters_PM_dbt          *entry,
                                            MEA_Bool                      *valid);

MEA_Status  MEA_API_COMM_GetRange_PM (MEA_Unit_t  unit,
										MEA_Uint32 pmStart,
										MEA_PmId_t  *pmIdArry,
										MEA_Uint32 *pMaxEntries);

int get_ingress_ports_mapping(int unit,int *portArry,int *pMaxNumberOfPorts);
/************************************************************************/
/* RMON Counter                                                         */
/************************************************************************/
MEA_Status MEA_API_COMM_Clear_Counters_RMON(MEA_Unit_t                    unit,
    MEA_Port_t                    port);

MEA_Status MEA_API_COMM_Clear_Counters_RMONs(MEA_Unit_t                    unit);
MEA_Status MEA_API_COMM_Get_Counters_RMON(MEA_Unit_t                    unit,
    MEA_Port_t                    port,
    MEA_Counters_RMON_dbt          *entry);

MEA_Status MEA_API_COMM_GetFirst_Counters_RMON(MEA_Unit_t                    unit,
    MEA_Port_t                    *port,
    MEA_Counters_RMON_dbt          *entry,
    MEA_Bool                     *valid);

MEA_Status MEA_API_COMM_GetNext_Counters_RMON(MEA_Unit_t                    unit,
    MEA_Port_t                    *port,
    MEA_Counters_RMON_dbt          *entry,
    MEA_Bool                      *valid);

/************************************************************************/
/*   Forwarder                                                                   */
/************************************************************************/

MEA_Status MEA_API_COMM_Create_SE_Entry (MEA_Unit_t  unit, MEA_SE_Entry_dbt *entry);

MEA_Status MEA_API_COMM_Delete_SE_Entry(MEA_Unit_t  unit_i,MEA_SE_Entry_key_dbt *key_pi);

MEA_Status MEA_API_COMM_Set_SE_Entry(MEA_Unit_t   unit,MEA_SE_Entry_dbt  *entry);

MEA_Status MEA_API_COMM_Get_SE_Entry(MEA_Unit_t   unit,MEA_SE_Entry_dbt  *entry);

MEA_Status MEA_API_COMM_GetFirst_SE_Entry   (MEA_Unit_t unit,MEA_SE_Entry_dbt  *entry,MEA_Bool*  found);

MEA_Status MEA_API_COMM_GetNext_SE_Entry   (MEA_Unit_t  unit,MEA_SE_Entry_dbt *entry,MEA_Bool*  found);

MEA_Status MEA_API_COMM_DeleteAll_SE (MEA_Unit_t unit);

MEA_Status MEA_API_COMM_GetRange_SE_Entry   (MEA_Unit_t unit,MEA_SE_Entry_key_dbt *key_Array,MEA_Bool first,MEA_Uint32 *pMax_num_of_entries);

/****************************************************************************/
/*    packet generator profile                                              */
/***************************************************************************/
MEA_Status MEA_API_COMM_Create_PacketGen_Profile_info(MEA_Unit_t              unit_i,
                          MEA_PacketGen_drv_Profile_info_entry_dbt      *entry_pi,
                          MEA_PacketGen_profile_info_t                  *id_io);

MEA_Status MEA_API_COMM_Set_PacketGen_Profile_info_Entry(MEA_Unit_t                           unit_i,
                           MEA_PacketGen_profile_info_t                 id_i,
                           MEA_PacketGen_drv_Profile_info_entry_dbt     *entry_pi);

MEA_Status MEA_API_COMM_Delete_PacketGen_Profile_info_Entry(MEA_Unit_t                    unit_i,
                                                       MEA_PacketGen_profile_info_t  id_i);

MEA_Status MEA_API_COMM_Get_PacketGen_Profile_info_Entry(MEA_Unit_t           unit_i,
                               MEA_PacketGen_profile_info_t                           id_i,
                               MEA_PacketGen_drv_Profile_info_entry_dbt  *entry_po);

MEA_Status MEA_API_COMM_GetFirst_PacketGen_Profile_info_Entry(MEA_Unit_t          unit_i,
                                MEA_PacketGen_profile_info_t                *id_o,
                                MEA_PacketGen_drv_Profile_info_entry_dbt    *entry_po,
                                MEA_Bool                                    *found_o);

MEA_Status MEA_API_COMM_GetNext_PacketGen_Profile_info_Entry(MEA_Unit_t                      unit_i,
                               MEA_PacketGen_profile_info_t                 *id_io,
                               MEA_PacketGen_drv_Profile_info_entry_dbt     *entry_po,
                               MEA_Bool                                     *found_o);


/*******************************************************************************/
/*    packet generator                                                         */
/******************************************************************************/
MEA_Status MEA_API_COMM_Create_PacketGen_Entry(MEA_Unit_t              unit_i,
												MEA_PacketGen_Entry_dbt      *entry_pi,
												MEA_PacketGen_t                  *id_io);

MEA_Status MEA_API_COMM_Set_PacketGen_Entry(MEA_Unit_t                       unit_i,
                                       MEA_PacketGen_t                  id_i,
                                       MEA_PacketGen_Entry_dbt         *entry_pi);

MEA_Status MEA_API_COMM_Delete_PacketGen_Entry(MEA_Unit_t                    unit_i,
                                          MEA_PacketGen_t               id_i);

MEA_Status MEA_API_COMM_Get_PacketGen_Entry(MEA_Unit_t                       unit_i,
                                       MEA_PacketGen_t                  id_i,
                                       MEA_PacketGen_Entry_dbt         *entry_po);

MEA_Status MEA_API_COMM_GetFirst_PacketGen_Entry(MEA_Unit_t                  unit_i,
                                            MEA_PacketGen_t            *id_o,
                                            MEA_PacketGen_Entry_dbt    *entry_po, /* Can be NULL */
                                            MEA_Bool                   *found_o);

MEA_Status MEA_API_COMM_GetNext_PacketGen_Entry(MEA_Unit_t                   unit_i,
                                           MEA_PacketGen_t             *id_io,
                                           MEA_PacketGen_Entry_dbt     *entry_po, /* Can be NULL */
                                           MEA_Bool                    *found_o);

/***************************************************************************/
/* ccm 																		*/
/***************************************************************************/
MEA_Status MEA_API_COMM_Create_CCM_Entry(MEA_Unit_t                  unit_i,
                                    MEA_CCM_Configure_dbt      *entry_pi,
                                    MEA_CcmId_t                *id_io);

MEA_Status MEA_API_COMM_Set_CCM_Entry(MEA_Unit_t                     unit_i,
                                 MEA_CcmId_t                    id_i,
                                 MEA_CCM_Configure_dbt         *entry_pi);

MEA_Status MEA_API_COMM_Delete_CCM_Entry(MEA_Unit_t                unit_i,
                                    MEA_CcmId_t               id_i);

MEA_Status MEA_API_COMM_Get_CCM_Entry(MEA_Unit_t                     unit_i,
                                 MEA_CcmId_t                    id_i,
                                 MEA_CCM_Configure_dbt         *entry_po);

MEA_Status MEA_API_COMM_GetFirst_CCM_Entry(MEA_Unit_t                unit_i,
                                      MEA_CcmId_t              *id_o,
                                      MEA_CCM_Configure_dbt    *entry_po,
                                      MEA_Bool                 *found_o);

MEA_Status MEA_API_COMM_GetNext_CCM_Entry(MEA_Unit_t                   unit_i,
                                     MEA_CcmId_t                 *id_io,
                                     MEA_CCM_Configure_dbt       *entry_po,
                                     MEA_Bool                    *found_o);

/*****************************************************************************/
/*   packet analyzer 														*/
/****************************************************************************/
MEA_Status MEA_API_COMM_Set_Analyzer(MEA_Unit_t                    unit_i,
                                MEA_Analyzer_t                id_i,
                                MEA_Analyzer_configure_dbt    *entry_pi);

MEA_Status MEA_API_COMM_Get_Analyzer(MEA_Unit_t                    unit_i,
                                MEA_Analyzer_t                id_i,
                                MEA_Analyzer_configure_dbt    *entry_po);


MEA_Status MEA_API_COMM_Get_Counters_ANALYZER(MEA_Unit_t  unit_i,
												MEA_Analyzer_t id_i,
												MEA_Counters_Analyzer_dbt *pEntry);

/************************************************************************/
/*      GW                                                              */
/************************************************************************/
MEA_Status MEA_API_COMM_Set_GW_Global_Entry(MEA_Unit_t                 unit,
    MEA_Gw_Global_dbt         *entry);

MEA_Status MEA_API_COMM_Get_GW_Global_Entry(MEA_Unit_t                 unit,
    MEA_Gw_Global_dbt          *entry);



MEA_Status MEA_API_COMM_Set_RootFilter_Entry(MEA_Unit_t                 unit,
    MEA_root_Filter_type_t     index,
    MEA_Bool                   enable);

MEA_Status MEA_API_COMM_Get_RootFilter_Entry(MEA_Unit_t                  unit,
    MEA_root_Filter_type_t      index,
    MEA_Bool                    *enable);


/************************************************************************/
/*      CCM counter                                                     */
/************************************************************************/
MEA_Status MEA_API_COMM_Get_Counters_CCM(MEA_Unit_t                    unit,
										MEA_CcmId_t Id,
										MEA_Counters_CCM_Defect_dbt *entry);


MEA_Status MEA_API_COMM_Clear_Counters_CCM(MEA_Unit_t              unit,
                                      	  MEA_CcmId_t             id_i);


MEA_Status MEA_API_COMM_Clear_Counters_LM(MEA_Unit_t           unit,
                                     MEA_LmId_t             id_i);

MEA_Status MEA_API_COMM_Get_Counters_LM(MEA_Unit_t             unit,
                                   MEA_LmId_t               id_i,
                                   MEA_Counters_LM_dbt   *entry);

MEA_Status MEA_API_COMM_Get_CC_Event_GroupId(MEA_Unit_t   unit,
									MEA_Uint32  group,
									MEA_Uint32 *pEvent);
/************************************************************************/
/*      global                                                          */
/************************************************************************/
MEA_Status MEA_API_COMM_Get_Globals_Entry(MEA_Unit_t unit,MEA_Globals_Entry_dbt *entry);

MEA_Status MEA_API_COMM_Set_Globals_Entry(MEA_Unit_t unit,MEA_Globals_Entry_dbt *entry);

/************************************************************************/
/*      filter                                                          */
/************************************************************************/
MEA_Status MEA_API_COMM_Get_Filter_KeyType(MEA_Unit_t unit_i,
                                      MEA_Filter_Key_dbt *key_i,
                                      MEA_Filter_Key_Type_te *keyType_o);

MEA_Status MEA_API_COMM_GetFirst_Filter (MEA_Unit_t     unit,
                                    MEA_Filter_t *o_filterId,
                                    MEA_Bool     *o_found);

MEA_Status MEA_API_COMM_GetNext_Filter  (MEA_Unit_t     unit,
                                    MEA_Filter_t *io_filterId ,
                                    MEA_Bool     *o_found);

MEA_Status MEA_API_COMM_Create_Filter (MEA_Unit_t                           unit,
                                  MEA_Filter_Key_dbt                  *key_from,
                                  MEA_Filter_Key_dbt                  *key_to,
                                  MEA_Filter_Data_dbt                 *data,
                                  MEA_OutPorts_Entry_dbt               *OutPorts_Entry,
                                  MEA_Policer_Entry_dbt                *Policer_entry,
                                  MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry,
                                  MEA_Filter_t                         *o_filterId);

MEA_Status  MEA_API_COMM_Get_Filter     (MEA_Unit_t                      unit_i,
                                    MEA_Filter_t                    filterId,
								    MEA_Filter_Key_dbt             *key_from,
								    MEA_Filter_Key_dbt             *key_to,
				                    MEA_Filter_Data_dbt			   *data,
							        MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
							        MEA_Policer_Entry_dbt          *Policer_Entry,
							        MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry);

MEA_Status  MEA_API_COMM_Set_Filter(MEA_Unit_t                      unit,
                                   MEA_Filter_t                    filterId,
                                   MEA_Filter_Data_dbt            *data,
                                   MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                   MEA_Policer_Entry_dbt          *Policer_entry,
                                   MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry);


MEA_Status  MEA_API_Delete_Filter (MEA_Unit_t                      unit,
                                   MEA_Filter_t                    filterId);



int         get_cliect_policer_profile(int unit, char *file_name);
MEA_Status  get_policer_profile_info(int unit, int profile_id, char * filename);
int getall_serviceId(int unit,char *file_name);


#endif /* COMM_MAE_API_H_ */

