
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <sched.h>
#include <string.h>
#include <sys/errno.h>
#include <sys/ioctl.h>
#include <getopt.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/socket.h>

#include <sys/time.h>

#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>




#if defined(MEA_READLINE)
#include <readline/readline.h>
#include <readline/history.h>
#endif

#define BUFFER_SIZE		1024
#define HOST_PORT       4443
#define MAX_CMD_LEN 1000


#define MEA_MAGIC_NUMBER_VALUE "babeface"
#define MEA_MAGIC_NUMBER_LEN   8

typedef struct {
       char       magic_number[MEA_MAGIC_NUMBER_LEN];
       char       pad1;
       char       type;
       char       pad2;
       char       len[3];
} hdr_buf_t;

int     ENET_card = 0;

int ENET_send_message_to_socket(char *msg,int len)
{
   int sock;
   struct sockaddr_in to;
   socklen_t          to_len = sizeof(to);
   hdr_buf_t hdr_buf;
   unsigned short port = 4443 + ENET_card;
   unsigned long  host = 0x7f000001; /* 127.0.0.1 - loopback */
   char  cli_cmd[3000];
   int   cli_cmd_len;
   char  cli_out[1000];
   int   cli_out_len;
   int   rc;
   //int   i;
   
   
   cli_cmd[0]='\0';
   strcat(cli_cmd,msg);
   strcat(cli_cmd," ");
   
   //printf("%s\r\n",cli_cmd);


   cli_cmd_len = strlen(cli_cmd)+1;


   /* create socket */
   if ((sock=socket(PF_INET, 
                    SOCK_STREAM, 
                    0))<0) {
        printf("socket creation failed (errno=%d '%s')\n",
               errno,
               strerror(errno));
		return -1;
	}
	


    /* create bind info for input */
	memset(&(to),'\0',sizeof(to));
    to.sin_family      = AF_INET;
    to.sin_addr.s_addr = htonl(host);
    to.sin_port = htons(port);

    if (connect (sock,(struct sockaddr*)&to,to_len) != 0) {
        printf ("connect failed (errno=%d '%s',host=0x%08lx,port=%d)\n",
                errno,
                strerror(errno),
                host,
		port);
        close(sock);
        return -1;
    }


    memset(&hdr_buf,0,sizeof(hdr_buf));
    memcpy(hdr_buf.magic_number,MEA_MAGIC_NUMBER_VALUE,sizeof(hdr_buf.magic_number));
    hdr_buf.pad1 = ' ';
    hdr_buf.pad2 = ' ';
    hdr_buf.type = '2';
    hdr_buf.len[0] = (cli_cmd_len  / 100)       + '0';
    hdr_buf.len[1] = ((cli_cmd_len % 100) / 10) + '0';
    hdr_buf.len[2] = (cli_cmd_len  %  10)       + '0';

    if (write (sock,
               (char*)&hdr_buf,
               sizeof(hdr_buf)) != sizeof (hdr_buf)) {
       printf("write hdr failed (errno=%d '%s')\n",
              errno,
              strerror(errno));
        close(sock);
        return -1;
    }

    if (write(sock,
              cli_cmd,
              cli_cmd_len) != cli_cmd_len) {
        printf ("write cmd failed (errno=%d '%s')\n",
                errno,
                strerror(errno));
        close(sock);
        return -1;
    }

    while (1) {
        rc = read (sock,
                  (char*)&hdr_buf,
                  sizeof(hdr_buf));
		if(rc == 0){
			rc = read (sock,
                  (char*)&hdr_buf,
                  sizeof(hdr_buf));
			printf("***read hdr other hdr***\n");
			close(sock);
			return 0;
		}	
		
        if (rc != sizeof (hdr_buf)) {
            printf("read hdr failed (errno=%d '%s' , rc=%d)\n",
                   errno,
                   strerror(errno),
                   rc);
            break;
        }

#if 0
        printf ("magic= '%c' '%c' '%c' '%c' '%c' '%c' '%c' '%c' \n",
                hdr_buf.magic_number[0],
                hdr_buf.magic_number[1],
                hdr_buf.magic_number[2],
                hdr_buf.magic_number[3],
                hdr_buf.magic_number[4],
                hdr_buf.magic_number[5],
                hdr_buf.magic_number[6],
                hdr_buf.magic_number[7]);
        printf ("type = '%c'  %d\n",hdr_buf.type,hdr_buf.type);
  
        printf ("len = '%c' '%c' '%c' \n",
                hdr_buf.len[0],
                hdr_buf.len[1],
                hdr_buf.len[2]);
#endif


        if (memcmp(hdr_buf.magic_number,
                   MEA_MAGIC_NUMBER_VALUE,
                   sizeof(hdr_buf.magic_number)) != 0) {
            printf("wrong messgage reply - no valid magic number \n");
            break;
        }
        if (hdr_buf.type != '2') {
            printf("wrong messgage reply - no valid type \n");
            break;
        }
        cli_out_len = (((hdr_buf.len[0] - '0') * 100) +
                       ((hdr_buf.len[1] - '0') *  10) +
                       ((hdr_buf.len[2] - '0') *   1) );
        if (cli_out_len==0) {
            break;
        }
        rc = read (sock,
                  cli_out,
                  cli_out_len);
        if (rc != cli_out_len) {
            printf("read body failed (errno=%d '%s' , rc=%d)\n",
                   errno,
                   strerror(errno),
                   rc);
            break;
        }

		cli_out[cli_out_len] = '\0';//0;
		printf("%s", cli_out);
    }

    memset(&hdr_buf,0,sizeof(hdr_buf));
    memcpy(hdr_buf.magic_number,MEA_MAGIC_NUMBER_VALUE,sizeof(hdr_buf.magic_number));
    hdr_buf.pad1 = ' ';
    hdr_buf.pad2 = ' ';
    hdr_buf.type = '2';
    cli_cmd_len = 0;
    hdr_buf.len[0] = (cli_cmd_len  / 100)       + '0';
    hdr_buf.len[1] = ((cli_cmd_len % 100) / 10) + '0';
    hdr_buf.len[2] = (cli_cmd_len  %  10)       + '0';

    if (write (sock,
               (char*)&hdr_buf,
               sizeof(hdr_buf)) != sizeof (hdr_buf)) {
       printf("write hdr finish failed (errno=%d '%s')\n",
              errno,
              strerror(errno));
       return -1;
    }

    close(sock);
	return 0;

}


int main(int argc, char **argv)
{
    int i;
    int count = 0;
    //char ch=0;
    static char s[3000];
    //	  HIST_ENTRY *pEntry;
#if   defined(MEA_READLINE) 
 char *pStr;
#endif


    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-card") == 0) {
            int card;
            if (i + 1 >= argc) {
                fprintf(stderr, "Error: Missing card id\n");
                return -1;
            }
            if (ENET_card != 0) {
                fprintf(stderr, "Error: -card option already used\n");
                return -2;
            }
            i++;
            card = atoi(argv[i]);
            if (card <= 0) {
                fprintf(stderr, "Error: card id should be great than zero\n");
                return -3;
            }
            ENET_card = card;
        }
        else {
            fprintf(stderr, "Error: Unknown paramter\n");
            return -4;
        }
    }

    fprintf(stdout, "\n\nWait for cli prompt\n\n");


    sleep(1);

    fprintf(stdout, "\n\nWelcome to FPGA CLI Environment\n\n");

    count = sprintf(s, "%s", "prompt");
    ENET_send_message_to_socket(s, count);
 #if   defined(MEA_READLINE)
    using_history();
#endif
    for (;;)
    {
        count = 0;
#if  !defined(MEA_READLINE)
        {

            int count = 0;
            char ch = 0;
            static char s[MAX_CMD_LEN + 1];

            while (count < sizeof(s) && ch != '\n') {
                ch = getchar();
                if (ch != 255) {
                    s[count++] = ch;
                }
            }
            s[--count] = '\0';
            if (strncmp(s, "mea KILL", 8) == 0)
            {
                s[0] = '\0';
                strcat(s, "mea KILL");
                count = strlen(s);
                ENET_send_message_to_socket(s, count);
                // destroy the server thread

                break;
            }
            else if (strncmp(s, "exit", 4) != 0)
            {
                
                count = strlen(s);
                ENET_send_message_to_socket(s, count);
                //if(count == 0)
                {
                    s[0] = '\0';
                    //memset(s,0,3000);
                    strcat(s, "prompt");
                    count = strlen(s);
                    //fprintf(stdout,"send %s\r\n",s);
                    ENET_send_message_to_socket(s, count);
                }
            }
            else
            {
                s[0] = '\0';
            strcat(s,"top");
            count = strlen(s);
            ENET_send_message_to_socket(s,count);
            // destroy the server thread
            fprintf(stdout,"\n\n Exit for Ethernity CLI \n\n");
            break;
            }


          }
#else	
		pStr=readline(NULL);
		
		if(pStr != NULL)
		{
      
		
			memset(s,0,3000);
			if(strncmp(pStr,"mea KILL",8) == 0)
			{
				s[0]='\0';
				strcat(s,"mea KILL");
				count = strlen(s);				
				ENET_send_message_to_socket(s,count);
				// destroy the server thread
				free(pStr);
				break;				
			}
			else if(strncmp(pStr,"exit",4) != 0)
			{
				strcpy(s,pStr);
				count = strlen(s);
				ENET_send_message_to_socket(s,count);
				//if(count == 0)
				{
					s[0]='\0';
					//memset(s,0,3000);
				  strcat(s,"prompt");
				  count = strlen(s);
				  //fprintf(stdout,"send %s\r\n",s);
				  ENET_send_message_to_socket(s,count); 
				}
			}
			else
			{
				s[0]='\0';
				strcat(s,"top");
				count = strlen(s);				
				ENET_send_message_to_socket(s,count);
				// destroy the server thread
				fprintf(stdout,"\n\n Exit for Ethernity CLI \n\n");
				break;
			}
			if(pStr[0])
			{
				add_history(pStr);
			}
			free(pStr);
		}
#endif		
        
	}

	return 1;
}


