/*
 ============================================================================
 Name        : webserverApp.c
 Author      : gil
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mongoose.h"
#include "mea_api.h"
#include "comm_mea_api.h"
#include "mea_api.h"

#include "mea_comm_msg.h"
#include "mea_client_api.h"

static int vpnId=100;
static int init_global=0;

tCcmConfiguration ccmConfigDataBase[D_MAX_CCM_STREAMS];
tLbConfiguration  lbConfigDataBase[D_MAX_LBM_STREAMS];
tDmConfiguration  dmConfigDataBase[D_MAX_DMM_STREAMS];
tLmIdDatabase	lmIdDatabase[D_MAX_LMID_STREAMS];
tAnalyzerDatabase	analyzerDatabase[D_MAX_ANALYZERS];

void init_cfm_oam_condifuration(void)
{
	int i=0;
	memset(&ccmConfigDataBase[0],0,sizeof(tCcmConfiguration)*D_MAX_CCM_STREAMS);
	memset(&lbConfigDataBase[0],0,sizeof(tLbConfiguration)*D_MAX_LBM_STREAMS);
	memset(&dmConfigDataBase[0],0,sizeof(tDmConfiguration)*D_MAX_DMM_STREAMS);
	memset(&lmIdDatabase[0],0,sizeof(tLmIdDatabase)*D_MAX_LMID_STREAMS);
	memset(&analyzerDatabase[0],0,sizeof(tAnalyzerDatabase)*D_MAX_ANALYZERS);

	for(i=0;i<D_MAX_LMID_STREAMS;i++)
	{
		lmIdDatabase[i].valid=0;
		lmIdDatabase[i].lmId=i+1;
	}

	for(i=0;i<D_MAX_ANALYZERS;i++)
	{
		analyzerDatabase[i].valid=0;
		analyzerDatabase[i].analyzerId=i;
	}
}

int AllocateAnalyzer()
{
	int idx;
	for(idx=0;idx<D_MAX_LMID_STREAMS;idx++)
	{
		if(analyzerDatabase[idx].valid == 0)
		{
			analyzerDatabase[idx].valid=1;
			return analyzerDatabase[idx].analyzerId;
		}
	}
	return -1;
}


int AllocateLmId()
{
	int idx;
	for(idx=0;idx<D_MAX_LMID_STREAMS;idx++)
	{
		if(ccmConfigDataBase[idx].valid == 0)
		{
			ccmConfigDataBase[idx].valid=1;
			return lmIdDatabase[idx].lmId;
		}
	}

	return -1;
}

tCcmConfiguration *getCcmConfigData(int streamId)
{
	int idx;
	for(idx=0;idx<D_MAX_CCM_STREAMS;idx++)
	{
		if( (ccmConfigDataBase[idx].valid == 1) && (ccmConfigDataBase[idx].stream_id == streamId) )
		{
			return &ccmConfigDataBase[idx];
		}
	}

	return NULL;
}

void freeCcmConfigData(int streamId)
{
	int idx;
	for(idx=0;idx<D_MAX_CCM_STREAMS;idx++)
	{
		if( (ccmConfigDataBase[idx].valid == 1) && (ccmConfigDataBase[idx].stream_id == streamId) )
		{
			ccmConfigDataBase[idx].valid=0;
			return;
		}
	}
}
tCcmConfiguration *AllocateCcmConfigData(int streamId)
{
	int idx;
	for(idx=0;idx<D_MAX_CCM_STREAMS;idx++)
	{
		if(ccmConfigDataBase[idx].valid == 0)
		{
			ccmConfigDataBase[idx].valid=1;
			ccmConfigDataBase[idx].stream_id=streamId;
			return &ccmConfigDataBase[idx];
		}
	}

	return NULL;
}

tLbConfiguration *getLbConfigData(int streamId)
{
	int idx;
	for(idx=0;idx<D_MAX_LBM_STREAMS;idx++)
	{
		if( (lbConfigDataBase[idx].valid == 1) &&(lbConfigDataBase[idx].stream_id == streamId) )
		{
			return &lbConfigDataBase[idx];
		}
	}

	return NULL;
}
tLbConfiguration *AllocateLbConfigData(int streamId)
{
	int idx;
	for(idx=0;idx<D_MAX_LBM_STREAMS;idx++)
	{
		if(lbConfigDataBase[idx].valid == 0)
		{
			lbConfigDataBase[idx].valid=1;
			lbConfigDataBase[idx].stream_id = streamId;
			return &lbConfigDataBase[idx];
		}
	}

	return NULL;
}

void freeLbConfigData(int streamId)
{
	int idx;
	for(idx=0;idx<D_MAX_LBM_STREAMS;idx++)
	{
		if( (lbConfigDataBase[idx].valid == 1) && (lbConfigDataBase[idx].stream_id == streamId) )
		{
			lbConfigDataBase[idx].valid=0;
			return;
		}
	}
}

tDmConfiguration *getDmConfigData(int streamId)
{
	int idx;
	for(idx=0;idx<D_MAX_DMM_STREAMS;idx++)
	{
		if( (dmConfigDataBase[idx].valid == 1) && (dmConfigDataBase[idx].stream_id == streamId) )
		{
			return &dmConfigDataBase[idx];
		}
	}

	return NULL;
}
tDmConfiguration *AllocateDmConfigData(int streamId)
{
	int idx;
	for(idx=0;idx<D_MAX_DMM_STREAMS;idx++)
	{
		if(dmConfigDataBase[idx].valid == 0)
		{
			dmConfigDataBase[idx].valid=1;
			dmConfigDataBase[idx].stream_id=streamId;
			return &dmConfigDataBase[idx];
		}
	}

	return NULL;
}

void freeDmConfigData(int streamId)
{
	int idx;
	for(idx=0;idx<D_MAX_DMM_STREAMS;idx++)
	{
		if( (dmConfigDataBase[idx].valid == 1) && (dmConfigDataBase[idx].stream_id == streamId) )
		{
			dmConfigDataBase[idx].valid=0;
			return;
		}
	}
}

MEA_uint64 MEA_web_atoiNum64(char *str)
{

   MEA_uint64 num;
   char* tstr;

   if (str == NULL) {/*
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - str == NULL\n",
                        __FUNCTION__);*/
      return 0;
   }

   tstr = (char*) str;
   num = 0;
   while (*tstr) {
      if (((*tstr) < '0') || ((*tstr) > '9')) {/*
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - str is not valid number \n",
                            __FUNCTION__);*/
          return 0;
      }
      num *= 10;
      num  = num + ((*tstr) - '0');
      tstr++;
   }

   return num;

}

typedef int (*webpage_callback)(struct mg_connection *conn);

typedef struct
{
	char 				Uri_name[80];
	webpage_callback	cb_func;
}tPageMethod;

#if 0
static char *message_204_no_content[] =
{
	"HTTP/1.1 204 No Content",
	"Content-Type: text/plain; charset=UTF-8",
};
#endif
static char *message_200_ok[] =
{
	"HTTP/1.1 200 OK",
	"Content-Type: text/plain; charset=UTF-8",
};
#if 0
static char *message_500_error_content[] =
{
//		"HTTP/1.1 500 Internal Server Error Content-Type: application/xml Content-Length: 121 Date: Mon, 28 Nov 2011 18:19:37 GMT",
//		"HTTP/1.1 500 Internal Server Error Content-Type: text/plain; charset=UTF-8",
		"statusCode: 500",
		"error: Internal Server Error",
		"message: server can't create",
};

#endif
int allocate_vpn_entry(void)
{
	return vpnId++;
}

MEA_Status convert_hexChar_to_number(char c_char,int *number)
{
	if( (c_char >= '0') && (c_char <= '9') )
	{
		(*number) |= c_char-'0';
	}
	else if( (c_char >= 'a') && (c_char <= 'f') )
	{
		(*number) |= c_char-'a' +10;
	}
	else if( (c_char >= 'A') && (c_char <= 'F') )
	{
		(*number) |= c_char-'A' +10;
	}
	else
	{
		return MEA_ERROR;
	}
	return MEA_OK;
}

MEA_Status convert_str_to_macAddr(char *str,MEA_MacAddr *pVal)
{
    int i;
    int number;
    int j=0;

    if (str == NULL) {
       fprintf(stdout,"str == NULL\n");
       return MEA_ERROR;
    }

    if (pVal == NULL) {
    	fprintf(stdout,"pointer mac address == NULL\n");
       return MEA_ERROR;
    }

    if (strlen(str) != 17) {
    	fprintf(stdout,"string mac address must be len of 17 :%s\n",str);
       return MEA_ERROR;
    }

    number=0;
	for (i = 0; i<(int)strlen(str); i++)
	{
		if( ( isalpha(str[i])>0 ) || (isdigit(str[i]) > 0 ) )
		{
			number = number << 4;
			convert_hexChar_to_number(str[i],&number);
		}
		else if( (str[i] == ':') || (str[i] == '-') )
		{
			pVal->b[j] = number;
			j++;
			number=0;
		}
		else
		{
			return MEA_ERROR;
		}
	}
	if(j == 5)
	{
		pVal->b[j] = number;
		j++;
		number=0;
	}
	else
	{
		return MEA_ERROR;
	}

     return MEA_OK;

}


//int get_ingress_ports_mapping(int unit,char *file_name,int *portArry,int *pMaxNumberOfPorts);
//int get_ingress_port_information(int unit,int 	ingress_port,char *file_name,tComIngressPort *pIgressPortInfo);
#if 0
static int send_message_204(struct mg_connection *conn)
{
	int i;

	for(i=0;i<sizeof(message_204_no_content)/sizeof(message_204_no_content[0]);i++)
	{
		mg_send_data(conn,message_204_no_content[i],strlen(message_204_no_content[i]));
	}
	return MG_TRUE;
}
#endif
static int send_message_200(struct mg_connection *conn)
{
	int i;

	for(i=0;i<sizeof(message_200_ok)/sizeof(message_200_ok[0]);i++)
	{
		mg_send_data(conn,message_200_ok[i],strlen(message_200_ok[i]));
	}
	return MG_TRUE;
}

#if 0
static int send_message_500(struct mg_connection *conn)
{
	int i;


	fprintf(stdout,"send 500 message\r\n");
	conn->status_code=500;
	for(i=0;i<sizeof(message_500_error_content)/sizeof(message_500_error_content[0]);i++)
	{
		mg_send_data(conn,message_500_error_content[i],strlen(message_500_error_content[i]));
	}
	return MG_TRUE;
}
#endif

static int load_text_file(struct mg_connection *conn,char *filename)
{
	FILE *file = fopen(filename,"r");
	char line[50];
//	int count=0;

	if(file == NULL)
	{
		fprintf(stdout,"failed to open file %s\r\n",filename);
		return MG_FALSE;
	}


	while(fgets(line,sizeof(line),file) != NULL)
	{
		//mg_printf(conn,"%s\n",line);
		//fprintf(stdout,"send data %s",line);

		//mg_printf_data(conn,"%s",line);
		mg_send_data(conn,line,strlen(line));
		memset(line,0,sizeof(line));
	}

	fclose(file);
	return MG_TRUE;
}
static int load_bin_file(struct mg_connection *conn,char *filename)
{
	FILE *file = fopen(filename,"rb");
	char line[2001];
	unsigned int fileLen=0;
	unsigned int Len=0;

	if(file == NULL)
	{
		fprintf(stdout,"failed to open file %s\r\n",filename);
		return MG_FALSE;
	}


	fseek(file, 0, SEEK_END);
	fileLen=ftell(file);
	fseek(file, 0, SEEK_SET);


	while(fileLen)
	{
		Len = fread((char *)&line[0], 1, 2000 , file);
		mg_send_data(conn,line,Len);
		//fprintf(stdout,"fileLen:%lu len:%lu \r\n",fileLen,Len);
		fileLen -= Len;
	}

	fclose(file);
	return MG_TRUE;
}

static int get_simple_page(struct mg_connection *conn)
{
	char filename[]="/home/application/sdp_client/ENET_web/ingress_port.html";

	return load_text_file(conn,filename);
}

static int get_xml_port_mapping(struct mg_connection *conn)
{
	char filename[]="/home/application/sdp_client/ENET_web/meaPortMapping.xml";
	char var[80];
	int len;
	int unit=0;
	int portArry[50];
	int MaxNumberOfPorts=50;

	fprintf(stdout,"process meaPortMapping.xml\r\n");

	len = mg_get_var(conn,"unit",var,sizeof(var));
	if(len > 0)
	{
		unit = atoi(var);
	}

	if(get_ingress_ports_mapping(0,&portArry[0],&MaxNumberOfPorts) != MEA_OK)
	{
		fprintf(stdout,"failed to get meaPortMapping.xml\r\n");
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	buildXml_ingrss_port_mapping(&portArry[0],MaxNumberOfPorts,0,filename);
	// build XML file

	return load_text_file(conn,filename);
}

MEA_Status get_policer_profile_info(int unit, int profile_id, char * filename)
{
    MEA_Policer_Entry_dbt Entry;
    if (MEA_API_COMM_Get_Policer_ACM_Profile(unit, profile_id, 0, &Entry) != MEA_OK)
    {
        return MEA_ERROR;
    }

    buildXml_getPolicerProfile_xml(&Entry, filename);

    return MEA_OK;

}
static int get_RmonCounters_html(struct mg_connection *conn)
{
	char filename[]="/home/application/sdp_client/ENET_web/rmonCounters.html";

	return load_text_file(conn,filename);
}

static int get_packet_gen_byId_xml (struct mg_connection *conn)
{
    int ret = MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    int profileId = 0;
    MEA_PacketGen_drv_Profile_info_entry_dbt          entry;

    char filename[] = "/home/application/sdp_client/ENET_web/getPacketGenById.xml";

    len = mg_get_var(conn, "unit", var, sizeof(var));
    if (len > 0)
    {
        unit = atoi(var);
    }
    len = mg_get_var(conn, "packetGenId", var, sizeof(var));
    if (len > 0)
    {
    	profileId = atoi(var);
    }

    ret = MEA_API_COMM_Get_PacketGen_Profile_info_Entry(unit,profileId,&entry);
    if(ret != MEA_OK)
    {
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
    }

    buildXml_getPacketGenProfile_xml(&entry, profileId, filename);

    ret = load_text_file(conn, filename);
    return MG_TRUE;
}



static int delete_packet_gen_profile_html(struct mg_connection *conn)
{
	char var[80];
	int len;
	int unit=0;
	int b_unit=0;
	int b_profile_id=0;
	MEA_PacketGen_profile_info_t        	   port_proto_prof_id;

	len = mg_get_var(conn,"unit",var,sizeof(var));
	if(len > 0)
	{
		b_unit=1;
		unit = atoi(var);
		fprintf(stdout,"got unit:%d\r\n",unit);
	}

	len = mg_get_var(conn,"packetGenId",var,sizeof(var));
	if(len > 0)
	{
		b_profile_id=1;
		port_proto_prof_id = atoi(var);
		fprintf(stdout,"got profileId:%d\r\n",port_proto_prof_id);
	}


	if(b_profile_id)
	{
		if(MEA_API_COMM_Delete_PacketGen_Profile_info_Entry(unit,
												            port_proto_prof_id) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}

	}
	send_message_200(conn);
	return MG_TRUE;
}

static int create_packet_gen_profile_html(struct mg_connection *conn)
{
	char var[80];
	int len;
	int unit=0;
	int b_unit=0;
	int b_profile_id=0;
	MEA_PacketGen_profile_info_t        	   port_proto_prof_id;
	MEA_PacketGen_drv_Profile_info_entry_dbt   create_packet_gen_Entry;


	memset(&create_packet_gen_Entry,0,sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));


	len = mg_get_var(conn,"unit",var,sizeof(var));
	if(len > 0)
	{
		b_unit=1;
		unit = atoi(var);
		fprintf(stdout,"got unit:%d\r\n",unit);
	}

	len = mg_get_var(conn,"packetGenId",var,sizeof(var));
	if(len > 0)
	{
		b_profile_id=1;
		port_proto_prof_id = atoi(var);
		fprintf(stdout,"got profileId:%d\r\n",port_proto_prof_id);
	}

	len = mg_get_var(conn,"forceErrorPrbs",var,sizeof(var));
	if(len > 0)
	{
		create_packet_gen_Entry.forceErrorPrbs = atoi(var);
		fprintf(stdout,"got forceErrorPrbs:%d\r\n",create_packet_gen_Entry.forceErrorPrbs);
	}

	len = mg_get_var(conn,"seqStampingEnable",var,sizeof(var));
	if(len > 0)
	{
		create_packet_gen_Entry.seq_stamping_enable = atoi(var);
		fprintf(stdout,"got seqStampingEnable:%d\r\n",create_packet_gen_Entry.seq_stamping_enable);
	}

	len = mg_get_var(conn,"headerLength",var,sizeof(var));
	if(len > 0)
	{
		create_packet_gen_Entry.header_length = atoi(var);
		fprintf(stdout,"got headerLength:%d\r\n",create_packet_gen_Entry.header_length);
	}

	len = mg_get_var(conn,"offsetType",var,sizeof(var));
	if(len > 0)
	{
		create_packet_gen_Entry.seq_offset_type = atoi(var);
		fprintf(stdout,"got offsetType:%d\r\n",create_packet_gen_Entry.seq_offset_type);
	}

	len = mg_get_var(conn,"streamType",var,sizeof(var));
	if(len > 0)
	{
		create_packet_gen_Entry.stream_type = atoi(var);
		fprintf(stdout,"got streamType:%d\r\n",create_packet_gen_Entry.stream_type);
	}

	if(b_profile_id)
	{
		if(MEA_API_COMM_Create_PacketGen_Profile_info(unit,
												  &create_packet_gen_Entry,
												  &port_proto_prof_id) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}

	}
	send_message_200(conn);
	return MG_TRUE;

}

static int getpolicerProfile_html(struct mg_connection *conn)
{
	char filename[]="/home/application/sdp_client/ENET_web/getpolicerProfile.html";

	return load_text_file(conn,filename);

}


static int policer_profile_html(struct mg_connection *conn)
{
    //char filename[] = "/home/application/sdp_client/ENET_web/policerProfile.html";
	char var[80];
	int len;
	int unit=0;
	int b_unit=0;
	int b_profile_id=0;
	MEA_Uint16                     	policer_id=0;
	MEA_AcmMode_t             		ACM_Mode=0;
	MEA_IngressPort_Proto_t        	port_proto_prof_i=MEA_INGRESS_PORT_PROTO_TRANS;
	MEA_Policer_Entry_dbt         	policer_Entry;


	memset(&policer_Entry,0,sizeof(MEA_Policer_Entry_dbt));


	len = mg_get_var(conn,"unit",var,sizeof(var));
	if(len > 0)
	{
		b_unit=1;
		unit = atoi(var);
		fprintf(stdout,"got unit:%d\r\n",unit);
	}

	len = mg_get_var(conn,"profileId",var,sizeof(var));
	if(len > 0)
	{
		b_profile_id=1;
		policer_id = atoi(var);
		fprintf(stdout,"got profileId:%d\r\n",policer_id);
	}
	len = mg_get_var(conn,"CIR",var,sizeof(var));
	if(len > 0)
	{

        policer_Entry.CIR = MEA_web_atoiNum64(var);
		fprintf(stdout,"got CIR:%llu\r\n",policer_Entry.CIR);
	}
	len = mg_get_var(conn,"EIR",var,sizeof(var));
	if(len > 0)
	{
        policer_Entry.EIR = MEA_web_atoiNum64(var);
		fprintf(stdout,"got EIR:%llu\r\n",policer_Entry.EIR);
	}
	len = mg_get_var(conn,"CBS",var,sizeof(var));
	if(len > 0)
	{
		policer_Entry.CBS = atoi(var);
		//fprintf(stdout,"got CBS:%lu\r\n",policer_Entry.CBS);
	}
	len = mg_get_var(conn,"EBS",var,sizeof(var));
	if(len > 0)
	{
		policer_Entry.EBS = atoi(var);
		//fprintf(stdout,"got EBS:%lu\r\n",policer_Entry.EBS);
	}

	len = mg_get_var(conn,"gn_type",var,sizeof(var));
	if(len > 0)
	{
		policer_Entry.gn_type = atoi(var);
		fprintf(stdout,"got gn_type:%d\r\n",policer_Entry.gn_type);
	}
	if(b_profile_id)
	{
		if(MEA_API_COMM_Create_Policer_ACM_Profile(unit,
													&policer_id,
													ACM_Mode,
													port_proto_prof_i,
													&policer_Entry ) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}

    //return load_text_file(conn, filename);
	send_message_200(conn);
	//mg_send_status(conn,200);
	return MG_TRUE;
}

static int get_egress_port_info(struct mg_connection *conn)
{
	char var[80];
	int len;
	int unit=0;
	int b_unit=0;
	int portId=0;
//	int b_portId=0;
//	int tx_enable=0;
//	int b_tx_enable=0;
//	int crc_check=0;
//	int b_crc_check=0;
//	int ret=MG_TRUE;
	MEA_EgressPort_Entry_dbt entry;

	char filename[]="/home/application/sdp_client/ENET_web/meaEgressPortInfo.xml";

	len = mg_get_var(conn,"unit",var,sizeof(var));
	if(len > 0)
	{
		b_unit=1;
		unit = atoi(var);
		fprintf(stdout,"got unit:%d\r\n",unit);

	}
	len = mg_get_var(conn,"port_id",var,sizeof(var));
	if(len > 0)
	{
		//b_portId=1;
		portId = atoi(var);
		fprintf(stdout,"portid:%d\r\n",portId);
	}

	if(MEA_API_COMM_Get_EgressPort_Entry(unit,portId,&entry) != MEA_OK)
	{
		// return web server error
		fprintf(stdout,"failed to get egress port database\r\n");
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}


	buildXml_egress_port_info(&entry,unit,portId,filename);

	return load_text_file(conn,filename);

}

static int rcv_setting_egress_port_info(struct mg_connection *conn)
{
	char var[80];
	int len;
	int unit=0;
	int b_unit=0;
	int portId=0;
	int b_portId=0;
	int tx_enable=0;
	int b_tx_enable=0;
	int crc_check=0;
	int b_crc_check=0;
//	int ret=MG_TRUE;
	MEA_EgressPort_Entry_dbt entry;

	//char filename[]="/home/application/sdp_client/ENET_web/ingress_port_setting.html";

	len = mg_get_var(conn,"unit",var,sizeof(var));
	if(len > 0)
	{
		b_unit=1;
		unit = atoi(var);
		fprintf(stdout,"got unit:%d\r\n",unit);

	}
	len = mg_get_var(conn,"port_id",var,sizeof(var));
	if(len > 0)
	{
		b_portId=1;
		portId = atoi(var);
		fprintf(stdout,"portid:%d\r\n",portId);
	}
	len = mg_get_var(conn,"user",var,sizeof(var));
	if(len > 0)
	{
		fprintf(stdout,"user:%s\r\n",var);
	}
	len = mg_get_var(conn,"pwd",var,sizeof(var));
	if(len > 0)
	{
		fprintf(stdout,"pwd:%s\r\n",var);
	}
	len = mg_get_var(conn,"tx_enable",var,sizeof(var));
	if(len > 0)
	{
		b_tx_enable =1;
		tx_enable = atoi(var);
		fprintf(stdout,"tx_enable:%d\r\n",tx_enable);
	}
	len = mg_get_var(conn,"crc_check",var,sizeof(var));
	if(len > 0)
	{
		b_crc_check=1;
		crc_check = atoi(var);
		fprintf(stdout,"crc_check:%d\r\n",crc_check);
	}

	if(b_portId==1)
	{
		if(MEA_API_COMM_Get_EgressPort_Entry(unit,portId,&entry) != MEA_OK)
		{
			// return web server error
			fprintf(stdout,"failed to get egress port database\r\n");
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}

		if(b_tx_enable ==1)
		{
			entry.tx_enable = tx_enable;
		}
		if(b_crc_check == 1)
		{
			entry.calc_crc= crc_check;
		}

		if(MEA_API_COMM_Set_EgressPort_Entry(0,portId,&entry) != MEA_OK)
		{
			// return web server error
			fprintf(stdout,"failed to set egress port database\r\n");
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}

	//ret = load_text_file(conn,filename);
	send_message_200(conn);
	return MG_TRUE;
}

static int rcv_setting_port_info(struct mg_connection *conn)
{
	char var[80];
	int len;
	int unit=0;
	int b_unit=0;
	int portId=0;
	int b_portId=0;
	int rx_enable=0;
	int b_rx_enable=0;
	int crc_check=0;
	int b_crc_check=0;
	int b_my_mac=0;
	int ret=MG_TRUE;
	MEA_MacAddr macAddr;
	MEA_IngressPort_Entry_dbt entry;

	//char filename[]="/home/application/sdp_client/ENET_web/ingress_port_setting.html";

	len = mg_get_var(conn,"unit",var,sizeof(var));
	if(len > 0)
	{
		b_unit=1;
		unit = atoi(var);
		fprintf(stdout,"got unit:%d\r\n",unit);

	}
	len = mg_get_var(conn,"port_id",var,sizeof(var));
	if(len > 0)
	{
		b_portId=1;
		portId = atoi(var);
		fprintf(stdout,"portid:%d\r\n",portId);
	}
	len = mg_get_var(conn,"user",var,sizeof(var));
	if(len > 0)
	{
		fprintf(stdout,"user:%s\r\n",var);
	}
	len = mg_get_var(conn,"pwd",var,sizeof(var));
	if(len > 0)
	{
		fprintf(stdout,"pwd:%s\r\n",var);
	}
	len = mg_get_var(conn,"rx_enable",var,sizeof(var));
	if(len > 0)
	{
		b_rx_enable =1;
		rx_enable = atoi(var);
		fprintf(stdout,"rx_enable:%d\r\n",rx_enable);
	}
	len = mg_get_var(conn,"crc_check",var,sizeof(var));
	if(len > 0)
	{
		b_crc_check=1;
		crc_check = atoi(var);
		fprintf(stdout,"crc_check:%d\r\n",crc_check);
	}

	len = mg_get_var(conn,"my_mac",var,sizeof(var));
	if(len > 0)
	{
		b_my_mac=1;
		convert_str_to_macAddr(var,&macAddr);
		crc_check = atoi(var);
		fprintf(stdout,"my_mac:%s\r\n",var);
	}

	if(b_portId==1)
	{
		if(MEA_API_COMM_Get_IngressPort_Entry(0,portId,&entry) != MEA_OK)
		{
			// return web server error
			fprintf(stdout,"failed to get ingress port database\r\n");
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
		if(b_rx_enable ==1)
		{
			entry.rx_enable = rx_enable;
		}
		if(b_crc_check == 1)
		{
			entry.crc_check = crc_check;
		}
		if(b_my_mac == 1)
		{
			memcpy(&entry.parser_info.my_Mac.b[0],&macAddr.b[0],6);
		}
		if(MEA_API_COMM_Set_IngressPort_Entry(0,portId,&entry) != MEA_OK)
		{
			// return web server error
			fprintf(stdout,"failed to set ingress port database\r\n");
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}

	//ret = load_text_file(conn,filename);
	send_message_200(conn);
	return ret;
}


static int get_xml_ingress_port_info(struct mg_connection *conn)
{
	char filename[]="/home/application/sdp_client/ENET_web/meaIngressPortInfo.xml";
	char var[80];
	int len;
	int unit=0;
	int port_id=0;
	MEA_IngressPort_Entry_dbt entry;

	len = mg_get_var(conn,"unit",var,sizeof(var));
	if(len > 0)
	{
		unit = atoi(var);
	}
	len = mg_get_var(conn,"port",var,sizeof(var));
	if(len > 0)
	{
		port_id = atoi(var);
	}
	len = mg_get_var(conn,"port_id",var,sizeof(var));
	if(len > 0)
	{
		port_id = atoi(var);
	}
	len = mg_get_var(conn,"port_id",var,sizeof(var));
	if(len > 0)
	{
		port_id = atoi(var);
	}
	fprintf(stdout,"Received unit=%d port=%d\r\n",unit,port_id);

	 if(MEA_API_COMM_Get_IngressPort_Entry(unit,port_id,&entry) != MEA_OK)
	 {
		fprintf(stdout,"failed to set ingress port database\r\n");
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	 }
	 buildXml_ingrss_port_info(unit,port_id,&entry,filename);

	 load_text_file(conn, filename);

	return MG_TRUE;
//	return get_ingress_port_information(unit,port_id,filename,&tIgressPortInfo);
}


static int get_style_css(struct mg_connection *conn)
{
	char filename[]="/home/application/sdp_client/ENET_web/style.css";

	return load_text_file(conn,filename);
}


static int get_all_policer_profile_xml(struct mg_connection *conn)
{
	MEA_Uint16 PolicerArray[200];
	MEA_Uint32 maxNumOfProfiles=200;
    char filename[] = "/home/application/sdp_client/ENET_web/getAllPolicerProfiles.xml";

    if(MEA_API_COMM_GetAll_Policer_ACM_Profile(MEA_UNIT_0,&PolicerArray[0],&maxNumOfProfiles) != MEA_OK)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
        return MG_FALSE;
    }

    buildXml_policer_profiles_id(&PolicerArray[0],maxNumOfProfiles,MEA_UNIT_0,filename);

    return load_text_file(conn, filename);
}

static int rcv_all_services(struct mg_connection *conn)
{
    char filename[] = "/home/application/sdp_client/ENET_web/serviceMaping.xml";
    MEA_Service_t  serviceIdArry[1000];
    MEA_Uint32 max_number_of_services=1000;

    fprintf(stdout,"process serviceIds\r\n");

    if(MEA_API_COMM_GetRange_Service (0,0,&serviceIdArry[0],&max_number_of_services) != MEA_OK)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
    	return MG_FALSE;
    }

    buildXml_service_id(&serviceIdArry[0],max_number_of_services,0,filename);


    return load_text_file(conn, filename);
}

static int delete_policerProfile(struct mg_connection *conn)
{
    char var[80];
    int len;
    int unit = 0;
    int profile_id = 0;



    len = mg_get_var(conn, "unit", var, sizeof(var));
    if (len > 0)
    {
        unit = atoi(var);
    }
    len = mg_get_var(conn, "profileId", var, sizeof(var));
    if (len > 0)
    {
        profile_id = atoi(var);
    }
    fprintf(stdout,"try to delete  profileId:%d\r\n",profile_id);
    if(MEA_API_COMM_Delete_Policer_ACM_Profile(unit,profile_id,0) != MEA_OK)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
    	return MG_FALSE;
    }
    send_message_200(conn);
    return MG_TRUE;
}

static int get_policer_profile_byId_xml(struct mg_connection *conn)
{
    char var[80];
    int len;
    int unit = 0;
    int profile_id = 0;
    char filename[] = "/home/application/sdp_client/ENET_web/getPolicerProfile.xml";



    len = mg_get_var(conn, "unit", var, sizeof(var));
    if (len > 0)
    {
        unit = atoi(var);
    }
    len = mg_get_var(conn, "profileId", var, sizeof(var));
    if (len > 0)
    {
        profile_id = atoi(var);
    }

    if (get_policer_profile_info(unit, profile_id, filename) != MEA_OK)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
        return MG_FALSE;
    }
    
    fprintf(stdout, "Received unit=%d profileId=%d\r\n", unit, profile_id);

    return load_text_file(conn, filename);


}
static int rcv_service_entry_html(struct mg_connection *conn)
{
	int ret=MG_TRUE;

	char filename[]="/home/application/sdp_client/ENET_web/get_service_entry.html";


	ret = load_text_file(conn,filename);
	return ret;
}
static int clear_PmCounter_byId_html(struct mg_connection *conn)
{
//    int ret = MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    MEA_PmId_t pm_id = 0;


//    char filename[] = "/home/application/sdp_client/ENET_web/pmCounters.html";

    len = mg_get_var(conn, "unit", var, sizeof(var));
     if (len > 0)
     {
         unit = atoi(var);
     }
     len = mg_get_var(conn, "pmId", var, sizeof(var));
     if (len > 0)
     {
    	 pm_id = atoi(var);
    	 // clear all
    	 fprintf(stdout, "pm id %d\r\n",pm_id);
    	 if(pm_id == 0)
    	 {
    		 if(MEA_API_COMM_Clear_Counters_PMs(unit) != MEA_OK)
    		 {
    			 //send_message_500(conn);
    			 mg_send_status(conn,500);
    			 return MEA_FALSE;
    		 }
    	 }
    	 else
    	 {
    		 if(MEA_API_COMM_Clear_Counters_PM(unit,pm_id) != MEA_OK)
    		 {
    			 //send_message_500(conn);
    			 mg_send_status(conn,500);
    			 return MEA_FALSE;
    		 }
    	 }
     }

     send_message_200(conn);
    return MG_TRUE;

}




static int getfirst_PmCounter_byId_xml(struct mg_connection *conn)
{
    int ret = MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    MEA_PmId_t pm_id = 0;
    MEA_Counters_PM_dbt          entry;
    MEA_Bool                     valid;


    char filename[] = "/home/application/sdp_client/ENET_web/meaFirstPmCounter.xml";

    len = mg_get_var(conn, "unit", var, sizeof(var));
    if (len > 0)
    {
        unit = atoi(var);
    }

    fprintf(stdout, "unit:%d\r\n",unit);

    if(MEA_API_COMM_GetFirst_Counters_PM(unit,&pm_id,&entry,&valid) != MEA_OK)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
    	return MG_FALSE;
    }

    if(valid == MEA_FALSE)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
    	return MG_FALSE;
    }

    buildXml_getPmId_xml(&entry, pm_id, filename);

    ret = load_text_file(conn, filename);
    return MG_TRUE;
}
static int getnext_PmCounter_byId_xml(struct mg_connection *conn)
{
    int ret = MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    MEA_PmId_t pm_id = 0;
    MEA_Counters_PM_dbt          entry;
    MEA_Bool                     valid=MEA_FALSE;

    char filename[] = "/home/application/sdp_client/ENET_web/meaNextPmCounter.xml";

    len = mg_get_var(conn, "unit", var, sizeof(var));
    if (len > 0)
    {
        unit = atoi(var);
    }
    len = mg_get_var(conn, "pmId", var, sizeof(var));
    if (len > 0)
    {
        pm_id = atoi(var);
    }

    if(MEA_API_COMM_GetNext_Counters_PM(unit,&pm_id,&entry,&valid) != MEA_OK)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
    	return MG_FALSE;
    }

    if(valid == MEA_FALSE)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
    	return MG_FALSE;
    }

    buildXml_getPmId_xml(&entry, pm_id, filename);

    ret = load_text_file(conn, filename);
    return ret;
}

static int get_RmonCounter_byId_xml (struct mg_connection *conn)
{
    int ret = MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    int port_id = 0;
    MEA_Counters_RMON_dbt          entry;

    char filename[] = "/home/application/sdp_client/ENET_web/meaRmonCounter.xml";

    len = mg_get_var(conn, "unit", var, sizeof(var));
    if (len > 0)
    {
        unit = atoi(var);
    }
    len = mg_get_var(conn, "portId", var, sizeof(var));
    if (len > 0)
    {
    	port_id = atoi(var);
    }
    len = mg_get_var(conn, "port_Id", var, sizeof(var));
    if (len > 0)
    {
    	port_id = atoi(var);
    }

    if(MEA_API_COMM_Get_Counters_RMON(unit,port_id,&entry) != MEA_OK)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
    	return MG_FALSE;
    }


    buildXml_getRmon_xml(&entry, port_id, filename);

    ret = load_text_file(conn, filename);
    return MG_TRUE;
}

static int get_PmAll_byId_xml(struct mg_connection *conn)
{
    int ret = MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    MEA_PmId_t  pmIdArry[300];
    MEA_Uint32 MaxEntries=300;

    char filename[] = "/home/application/sdp_client/ENET_web/meaAllPmCounters.xml";

    len = mg_get_var(conn, "unit", var, sizeof(var));
    if (len > 0)
    {
        unit = atoi(var);
    }

    if(MEA_API_COMM_GetRange_PM (unit,0,&pmIdArry[0],&MaxEntries) != MEA_OK)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
    	return MG_FALSE;
    }

    buildXml_getAllPmId_xml(unit,pmIdArry,MaxEntries,filename);

    ret = load_text_file(conn, filename);
    return ret;
}


static int get_PmCounter_byId_xml(struct mg_connection *conn)
{
    int ret = MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    int pm_id = 0;
    MEA_Counters_PM_dbt          entry;

    char filename[] = "/home/application/sdp_client/ENET_web/meaPmCounter.xml";

    len = mg_get_var(conn, "unit", var, sizeof(var));
    if (len > 0)
    {
        unit = atoi(var);
    }
    len = mg_get_var(conn, "pmId", var, sizeof(var));
    if (len > 0)
    {
        pm_id = atoi(var);
    }

    if(MEA_API_COMM_Get_Counters_PM(unit, pm_id, &entry) != MEA_OK)
    {
    	//send_message_500(conn);
    	mg_send_status(conn,500);
    	return MG_FALSE;
    }

    buildXml_getPmId_xml(&entry, pm_id, filename);

    ret = load_text_file(conn, filename);
    return ret;
}

static int del_lb_configuration(struct mg_connection *conn)
{
//	int ret=MG_TRUE;
    char var[80];
    int len;
    tLbConfiguration *pLbConfig;
    int stream_id=0;

    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }
	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}


	pLbConfig = getLbConfigData(stream_id);
	if(pLbConfig == NULL)
    {
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
    }

	if(pLbConfig->lbm_side)
	{
		if(delete_Lbm_Configuration(pLbConfig) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}
	else
	{
		if(delete_Lbr_Configuration(pLbConfig) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}
	freeLbConfigData(stream_id);
	send_message_200(conn);
    // delete the LBM
    return MG_TRUE;
}

/**************************************************************************************/
// Function Name:  MEA_OS_atohex_MAC
//
// Description:	   converts ascii string to its hexadecimal representation
//				   of MAC address
//                 Example:  01050e030405 will be convert to
//                           pVal->(a.msw=0x01050e03,a.lss=0x0405)
//
// Arguments:	   str - string to be converted, pVal - result storage
//
// Return:		   pointer to MAC value.
/**************************************************************************************/

static int set_lb_configuration(struct mg_connection *conn)
{
	int ret=MG_TRUE;
    char var[80];
    int len;
    tLbConfiguration *pLbConfig;
    int stream_id=0;

    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }
	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}


    if(getLbConfigData(stream_id) != NULL)
    {
		fprintf(stdout,"stream_id allready exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
    }

    pLbConfig = AllocateLbConfigData(stream_id);

    if(pLbConfig == NULL)
    {
		fprintf(stdout,"failed to allocate mem for LB:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
    }
    pLbConfig->stream_id=stream_id;
	len = mg_get_var(conn, "unit", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->unit = atoi(var);
		fprintf(stdout,"unit :%d\r\n",pLbConfig->unit);
	}
	len = mg_get_var(conn, "destMac", var, sizeof(var));
	if (len > 0)
	{
		convert_str_to_macAddr(var,&pLbConfig->destMac);
		fprintf(stdout,"dest-mac :%s\r\n",var);
	}
	len = mg_get_var(conn, "vlanId", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->vlanId = atoi(var);
		fprintf(stdout,"vlanId :%d\r\n",pLbConfig->vlanId);
	}

	len = mg_get_var(conn, "srcPort", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->srcPort = atoi(var);
		fprintf(stdout,"clusterId :%d\r\n",pLbConfig->srcPort);
	}
	len = mg_get_var(conn, "megLevel", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->megLevel = atoi(var);
		fprintf(stdout,"megLevel :%d\r\n",pLbConfig->megLevel);
	}
	len = mg_get_var(conn, "cfmVersion", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->cfmVersion = atoi(var);
		fprintf(stdout,"cfmVersion :%d\r\n",pLbConfig->cfmVersion);
	}
	len = mg_get_var(conn, "policerId", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->policerId = atoi(var);
		fprintf(stdout,"policerId :%d\r\n",pLbConfig->policerId);
	}
	len = mg_get_var(conn, "Priority", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->u1CcmPriority = atoi(var);
		fprintf(stdout,"u1CcmPriority :%d\r\n",pLbConfig->u1CcmPriority);
	}
	len = mg_get_var(conn, "lbmSide", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->lbm_side = atoi(var);
		fprintf(stdout,"lbmSide :%d\r\n",pLbConfig->lbm_side);
	}
	len = mg_get_var(conn, "prbsMode", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->prbs = atoi(var);
		fprintf(stdout,"prbs-mode :%d\r\n",pLbConfig->prbs);
	}
	len = mg_get_var(conn, "injectError", var, sizeof(var));
	if (len > 0)
	{
		pLbConfig->inject_error = atoi(var);
		fprintf(stdout,"inject-error :%d\r\n",pLbConfig->inject_error);
	}

	pLbConfig->lmEnable = MEA_TRUE;
	pLbConfig->lmId=AllocateLmId();
	pLbConfig->packetLbm.analyzerId=AllocateAnalyzer();


	if(pLbConfig->lbm_side)
	{
		pLbConfig->packetLbm.VpnIndex = allocate_vpn_entry();
		if(create_Lbm_Configuration(pLbConfig) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}
	else
	{
		if(create_Lbr_Configuration(pLbConfig) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}



	send_message_200(conn);
	return ret;
}

static int del_dm_configuration(struct mg_connection *conn)
{
//	int ret=MG_TRUE;
    char var[80];
    int len;
    tDmConfiguration *pDmConfig;
    int stream_id=0;


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}

	pDmConfig = getDmConfigData(stream_id);
	if(pDmConfig == NULL)
    {
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
    }

	if(pDmConfig->Dm_side)
	{
		if(delete_Dmm_Configuration(pDmConfig) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}
	else
	{
		if(delete_Dmr_Configuration(pDmConfig) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}

	freeDmConfigData(stream_id);
	send_message_200(conn);
    return MG_TRUE;
}

static int set_dm_configuration(struct mg_connection *conn)
{
	int ret=MG_TRUE;
    char var[80];
    int len;
    tDmConfiguration *pDmConfig;
    int stream_id=0;


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}

    if(getDmConfigData(stream_id) != NULL)
    {
		fprintf(stdout,"stream_id allready exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
    }

    pDmConfig = AllocateDmConfigData(stream_id);

    if(pDmConfig == NULL)
	{
		fprintf(stdout,"failed to allocate Dm:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

    pDmConfig->stream_id=stream_id;
	len = mg_get_var(conn, "unit", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->unit = atoi(var);
		fprintf(stdout,"unit :%d\r\n",pDmConfig->unit);
	}
	len = mg_get_var(conn, "destMac", var, sizeof(var));
	if (len > 0)
	{
		convert_str_to_macAddr(var,&pDmConfig->destMac);
		fprintf(stdout,"dest-mac :%s\r\n",var);
	}
	len = mg_get_var(conn, "vlanId", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->vlanId = atoi(var);
		fprintf(stdout,"vlanId :%d\r\n",pDmConfig->vlanId);
	}

	len = mg_get_var(conn, "srcPort", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->srcPort = atoi(var);
		fprintf(stdout,"clusterId :%d\r\n",pDmConfig->srcPort);
	}
	len = mg_get_var(conn, "megLevel", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->megLevel = atoi(var);
		fprintf(stdout,"megLevel :%d\r\n",pDmConfig->megLevel);
	}
	len = mg_get_var(conn, "cfmVersion", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->cfmVersion = atoi(var);
		fprintf(stdout,"cfmVersion :%d\r\n",pDmConfig->cfmVersion);
	}
	len = mg_get_var(conn, "policerId", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->policerId = atoi(var);
		fprintf(stdout,"policerId :%d\r\n",pDmConfig->policerId);
	}
	len = mg_get_var(conn, "Priority", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->u1CcmPriority = atoi(var);
		fprintf(stdout,"u1CcmPriority :%d\r\n",pDmConfig->u1CcmPriority);
	}
	len = mg_get_var(conn, "dmmSide", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->Dm_side = atoi(var);
		fprintf(stdout,"dmmSide :%d\r\n",pDmConfig->Dm_side);
	}
	len = mg_get_var(conn, "prbsMode", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->prbs = atoi(var);
		fprintf(stdout,"prbs-mode :%d\r\n",pDmConfig->prbs);
	}
	len = mg_get_var(conn, "injectError", var, sizeof(var));
	if (len > 0)
	{
		pDmConfig->inject_error = atoi(var);
		fprintf(stdout,"inject-error :%d\r\n",pDmConfig->inject_error);
	}

	pDmConfig->lmEnable = MEA_TRUE;
	pDmConfig->lmId=AllocateLmId();
	pDmConfig->packetDmm.analyzerId=AllocateAnalyzer();


	if(pDmConfig->Dm_side)
	{
		pDmConfig->packetDmm.VpnIndex = allocate_vpn_entry();
		if(create_Dmm_Configuration(pDmConfig) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}
	else
	{
		if(create_Dmr_Configuration(pDmConfig) != MEA_OK)
		{
			//send_message_500(conn);
			mg_send_status(conn,500);
			return MG_FALSE;
		}
	}

	send_message_200(conn);
	return ret;
}

static int get_dmm_analizer_statistics(struct mg_connection *conn)
{
    char var[80];
    int len;
    tDmConfiguration *pDmConfig;
    int stream_id=0;
    MEA_Counters_Analyzer_dbt tEntry;

    char filename[] = "/home/application/sdp_client/ENET_web/meaGetDmmStatistics.xml";


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}
	pDmConfig = getDmConfigData(stream_id);
	if(pDmConfig == NULL)
	{
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	if(MEA_API_COMM_Get_Counters_ANALYZER(0,pDmConfig->packetDmm.analyzerId,&tEntry) != MEA_OK)
	{
		fprintf(stdout,"MEA_API_COMM_Get_Counters_ANALYZER failed:%d\r\n",pDmConfig->packetDmm.analyzerId);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	buildXml_Lbm_statistics_xml(&tEntry, filename);

	load_text_file(conn, filename);
	return MG_TRUE;
}


static int get_dm_configuration(struct mg_connection *conn)
{
    char var[80];
    int len;
    tDmConfiguration *pDmConfig;
    int stream_id=0;

    char filename[] = "/home/application/sdp_client/ENET_web/meaGetDmConfig.xml";


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}
	pDmConfig = getDmConfigData(stream_id);
	if(pDmConfig == NULL)
	{
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	buildXml_Dm_xml(pDmConfig, filename);


	load_text_file(conn, filename);
	return MG_TRUE;


}
static int get_lbm_analizer_statistics(struct mg_connection *conn)
{
    char var[80];
    int len;
    tLbConfiguration *pLbConfig;
    int stream_id=0;
    MEA_Counters_Analyzer_dbt tEntry;

    char filename[] = "/home/application/sdp_client/ENET_web/meaGetLbmStatistics.xml";


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}
	pLbConfig = getLbConfigData(stream_id);
	if(pLbConfig == NULL)
	{
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	if(MEA_API_COMM_Get_Counters_ANALYZER(0,pLbConfig->packetLbm.analyzerId,&tEntry) != MEA_OK)
	{
		fprintf(stdout,"MEA_API_COMM_Get_Counters_ANALYZER failed:%d\r\n",pLbConfig->packetLbm.analyzerId);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	buildXml_Lbm_statistics_xml(&tEntry, filename);

	load_text_file(conn, filename);
	return MG_TRUE;
}

static int get_lb_configuration(struct mg_connection *conn)
{
    char var[80];
    int len;
    tLbConfiguration *pLbConfig;
    int stream_id=0;

    char filename[] = "/home/application/sdp_client/ENET_web/meaGetLbConfig.xml";


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}
	pLbConfig = getLbConfigData(stream_id);
	if(pLbConfig == NULL)
	{
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	buildXml_Lb_xml(pLbConfig, filename);

	load_text_file(conn, filename);
	return MG_TRUE;

}

static int get_ccmlm_statistics(struct mg_connection *conn)
{
    char var[80];
    int len;
    tCcmConfiguration *pCcmConfig;
    int stream_id=0;
    int unit=0;
    MEA_Counters_LM_dbt entry;

    char filename[] = "/home/application/sdp_client/ENET_web/meaGetCcmLmState.xml";


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}
	pCcmConfig = getCcmConfigData(stream_id);
	if(pCcmConfig == NULL)
	{
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;

	}

	if(MEA_API_COMM_Get_Counters_LM(unit,pCcmConfig->lmId,&entry) != MEA_OK)
	{
		fprintf(stdout,"failed to get ccm lm statistics:%d\r\n",pCcmConfig->lmId);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}
	buildXml_Lm_counterXml(unit,&entry,filename);
	load_text_file(conn, filename);
	return MG_TRUE;

}

static int get_ccm_defect_statistics(struct mg_connection *conn)
{
    char var[80];
    int len;
    tCcmConfiguration *pCcmConfig;
    int stream_id=0;
    int unit=0;
    MEA_Counters_CCM_Defect_dbt entry;
    MEA_Uint32 event_loss;

    char filename[] = "/home/application/sdp_client/ENET_web/meaGetCcmDefectState.xml";


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}
	pCcmConfig = getCcmConfigData(stream_id);
	if(pCcmConfig == NULL)
	{
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;

	}


	if(MEA_API_COMM_Get_Counters_CCM(unit,pCcmConfig->packetAna.id_io,&entry) != MEA_OK)
	{
		fprintf(stdout,"failed to get ccm defect statistics:%d\r\n",pCcmConfig->packetAna.id_io);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}
	if(MEA_API_COMM_Get_CC_Event_GroupId(unit,0,&event_loss) != MEA_OK)
	{
		fprintf(stdout,"failed to get ccm loss statistics:%d\r\n",pCcmConfig->packetAna.id_io);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	fprintf(stdout,"ccm loss statistics:0x%x\r\n",(unsigned int)event_loss);

	event_loss = event_loss & (1 << pCcmConfig->packetAna.id_io);

	buildXml_Ccm_counterXml(unit,&entry,(event_loss==0)?0:1,filename);
	load_text_file(conn, filename);
	return MG_TRUE;

}
static int get_ccm_configuration(struct mg_connection *conn)
{
    char var[80];
    int len;
    tCcmConfiguration *pCcmConfig;
    int stream_id=0;

    char filename[] = "/home/application/sdp_client/ENET_web/meaGetCcmConfig.xml";


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}
	pCcmConfig = getCcmConfigData(stream_id);
	if(pCcmConfig == NULL)
	{
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;

	}

	buildXml_Ccm_xml(pCcmConfig, filename);
	load_text_file(conn, filename);

	return MG_TRUE;


}

static int del_ccm_configuration(struct mg_connection *conn)
{
    char var[80];
    int len;
    tCcmConfiguration *pCcmConfig;
    int stream_id=0;


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}
	pCcmConfig = getCcmConfigData(stream_id);
	if(pCcmConfig == NULL)
	{
		fprintf(stdout,"stream_id not exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;

	}
	if(delete_Ccm_Configuration(pCcmConfig) != MEA_OK)
	{
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	freeCcmConfigData(stream_id);
	send_message_200(conn);

	return MG_TRUE;
}

static int set_ccm_configuration(struct mg_connection *conn)
{
	int ret=MG_TRUE;
    char var[80];
    int len;
    tCcmConfiguration *pCcmConfig;
    int stream_id=0;


    if(init_global == 0)
    {
    	init_cfm_oam_condifuration();
    	FsMiEcfmHwInit();
    	init_global=1;
    }

	len = mg_get_var(conn, "stream_id", var, sizeof(var));
	if (len > 0)
	{
		stream_id = atoi(var);
		fprintf(stdout,"stream_id :%d\r\n",stream_id);
	}
	if(getCcmConfigData(stream_id) != NULL)
	{
		fprintf(stdout,"stream_id allready exist:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}
	pCcmConfig = AllocateCcmConfigData(stream_id);
	if(pCcmConfig == NULL)
	{
		fprintf(stdout,"failed to allocate ccm database:%d\r\n",stream_id);
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}
	pCcmConfig->stream_id = stream_id;

	len = mg_get_var(conn, "unit", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->unit = atoi(var);
		fprintf(stdout,"unit :%d\r\n",pCcmConfig->unit);
	}
	len = mg_get_var(conn, "destMac", var, sizeof(var));
	if (len > 0)
	{
		convert_str_to_macAddr(var,&pCcmConfig->destMac);
		fprintf(stdout,"dest-mac :%s\r\n",var);
	}
	len = mg_get_var(conn, "vlanId", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->vlanId = atoi(var);
		fprintf(stdout,"vlanId :%d\r\n",pCcmConfig->vlanId);
	}

	len = mg_get_var(conn, "srcPort", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->srcPort = atoi(var);
		fprintf(stdout,"srcPort :%d\r\n",pCcmConfig->srcPort);
	}
	len = mg_get_var(conn, "megLevel", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->megLevel = atoi(var);
		fprintf(stdout,"megLevel :%d\r\n",pCcmConfig->megLevel);
	}
	len = mg_get_var(conn, "cfmVersion", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->cfmVersion = atoi(var);
		fprintf(stdout,"cfmVersion :%d\r\n",pCcmConfig->cfmVersion);
	}
	len = mg_get_var(conn, "ccmPeriod", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->ccmPeriod = atoi(var);
		fprintf(stdout,"ccmPeriod :%d\r\n",pCcmConfig->ccmPeriod);
	}
	len = mg_get_var(conn, "rdiEnable", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->rdiEnable = atoi(var);
		fprintf(stdout,"rdiEnable :%d\r\n",pCcmConfig->rdiEnable);
	}
	len = mg_get_var(conn, "megId", var, sizeof(var));
	if (len > 0)
	{
		strncpy(pCcmConfig->megId,var,48);
		fprintf(stdout,"megId :%s\r\n",pCcmConfig->megId);
	}
	len = mg_get_var(conn, "lmEnable", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->lmEnable = atoi(var);
		fprintf(stdout,"lmEnable :%d\r\n",pCcmConfig->lmEnable);
	}
	len = mg_get_var(conn, "remoteMepId", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->remoteMepId = atoi(var);
		fprintf(stdout,"remoteMepId :%d\r\n",pCcmConfig->remoteMepId);
	}
	len = mg_get_var(conn, "localMepId", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->localMepId = atoi(var);
		fprintf(stdout,"remoteMepId :%d\r\n",pCcmConfig->localMepId);
	}

	len = mg_get_var(conn, "policerId", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->policerId = atoi(var);
		fprintf(stdout,"policerId :%d\r\n",pCcmConfig->policerId);
	}
	len = mg_get_var(conn, "outServiceId", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->outgoing_service_id = atoi(var);
		fprintf(stdout,"outServiceId :%d\r\n",pCcmConfig->outgoing_service_id);
	}
	len = mg_get_var(conn, "inServiceId", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->incoming_service_id = atoi(var);
		fprintf(stdout,"inServiceId :%d\r\n",pCcmConfig->incoming_service_id);
	}
	len = mg_get_var(conn, "Priority", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->u1CcmPriority = atoi(var);
		fprintf(stdout,"u1CcmPriority :%d\r\n",pCcmConfig->u1CcmPriority);
	}

	len = mg_get_var(conn, "activate", var, sizeof(var));
	if (len > 0)
	{
		pCcmConfig->active = atoi(var);
		fprintf(stdout,"activate :%d\r\n",pCcmConfig->active);
	}

	pCcmConfig->lmId=AllocateLmId();
	//pCcmConfig->lmEnable=MEA_TRUE;
	if(create_Ccm_Configuration(pCcmConfig) != MEA_OK)
	{
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}
	send_message_200(conn);
	return ret;

}

static int rcv_delete_servicevlan(struct mg_connection *conn)
{
	int ret=MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    MEA_Service_t service_id = 0;

	len = mg_get_var(conn, "unit", var, sizeof(var));
	if (len > 0)
	{
		unit = atoi(var);
		fprintf(stdout,"unit :%d\r\n",unit);
	}
	len = mg_get_var(conn, "serviceId", var, sizeof(var));
	if (len > 0)
	{
		service_id = atoi(var);
		fprintf(stdout,"service_id :%d\r\n",service_id);
	}

	if(MEA_API_COMM_Delete_Service (unit,service_id) != MEA_OK)
	{
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}
	send_message_200(conn);
	return ret;
}

static int rcv_create_servicevlan(struct mg_connection *conn)
{
	int ret=MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    MEA_Service_t service_id = 0;
    int clusterId=0;
    tServiceVlanParams   entry;

    memset(&entry,0,sizeof(tServiceVlanParams));

	len = mg_get_var(conn, "unit", var, sizeof(var));
	if (len > 0)
	{
		unit = atoi(var);
		fprintf(stdout,"unit :%d\r\n",unit);
	}
	len = mg_get_var(conn, "serviceId", var, sizeof(var));
	if (len > 0)
	{
		service_id = atoi(var);
		fprintf(stdout,"service_id :%d\r\n",service_id);
	}
	len = mg_get_var(conn, "policerId", var, sizeof(var));
	if (len > 0)
	{
		entry.policerId = atoi(var);
		fprintf(stdout,"policerId :%d\r\n",entry.policerId);
	}

	len = mg_get_var(conn, "pmId", var, sizeof(var));
	if (len > 0)
	{
		entry.pmId = atoi(var);
		fprintf(stdout,"pmid :%d\r\n",entry.pmId);
	}

	len = mg_get_var(conn, "eIngressType", var, sizeof(var));
	if (len > 0)
	{
		entry.eIngressType = atoi(var);
		fprintf(stdout,"eIngressType :%d\r\n",entry.eIngressType);
	}
	len = mg_get_var(conn, "outer_vlanId", var, sizeof(var));
	if (len > 0)
	{
		entry.outer_vlanId = atoi(var);
		fprintf(stdout,"outer_vlanId :%d\r\n",entry.outer_vlanId);
	}
	len = mg_get_var(conn, "inner_vlanId", var, sizeof(var));
	if (len > 0)
	{
		entry.inner_vlanId = atoi(var);
		fprintf(stdout,"inner_vlanId :%d\r\n",entry.inner_vlanId);
	}
	len = mg_get_var(conn, "clusterId", var, sizeof(var));
	if (len > 0)
	{
		clusterId = atoi(var);
		MEA_SET_OUTPORT(&entry.outputPort,clusterId);
		fprintf(stdout,"clusterId :%d\r\n",clusterId);
	}

	len = mg_get_var(conn, "vlanEdit_flowtype", var, sizeof(var));
	if (len > 0)
	{
		entry.vlanEdit.flow_type = atoi(var);
		//fprintf(stdout,"vlanEdit.flow_type :%lu\r\n",entry.vlanEdit.flow_type);
	}
	len = mg_get_var(conn, "vlanEdit_outer_command", var, sizeof(var));
	if (len > 0)
	{
		entry.vlanEdit.outer_edit_command = atoi(var);
		//fprintf(stdout,"vlanEdit.outer_edit_command :%lu\r\n",entry.vlanEdit.outer_edit_command);
	}
	len = mg_get_var(conn, "vlanEdit_outer_vlan", var, sizeof(var));
	if (len > 0)
	{
		entry.vlanEdit.outer_vlan = atoi(var);
		//fprintf(stdout,"vlanEdit.outer_vlan :%lu\r\n",entry.vlanEdit.outer_vlan);
	}
	len = mg_get_var(conn, "vlanEdit_inner_command", var, sizeof(var));
	if (len > 0)
	{
		entry.vlanEdit.inner_edit_command = atoi(var);
		//fprintf(stdout,"vlanEdit.inner_edit_command :%lu\r\n",entry.vlanEdit.inner_edit_command);
	}
	len = mg_get_var(conn, "vlanEdit_inner_vlan", var, sizeof(var));
	if (len > 0)
	{
		entry.vlanEdit.inner_vlan = atoi(var);
		//fprintf(stdout,"vlanEdit.inner_vlan :%lu\r\n",entry.vlanEdit.inner_vlan);
	}
	len = mg_get_var(conn, "srcPort", var, sizeof(var));
	if (len > 0)
	{
		entry.srcPort = atoi(var);
		//fprintf(stdout,"srcPort :%lu\r\n",entry.srcPort);
	}
	len = mg_get_var(conn, "port_id", var, sizeof(var));
	if (len > 0)
	{
		entry.srcPort = atoi(var);
		//fprintf(stdout,"srcPort :%lu\r\n",entry.srcPort);
	}



	entry.vpnId = allocate_vpn_entry();

	if( MEA_API_COMM_Create_Service_vlan(unit,&service_id,&entry) != MEA_OK)
	{
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}


	send_message_200(conn);
	return ret;

}

static int get_filter_mask_configuration(int unit,int service_id,MEA_Uint32 filter_type,MEA_Uint32 *pfilter_mask)
{
    MEA_Filter_Key_dbt             key_from;
    MEA_Filter_Key_dbt             key_to;
    MEA_Filter_Data_dbt			   data;
    MEA_OutPorts_Entry_dbt         OutPorts_Entry;
    MEA_Policer_Entry_dbt          Policer_Entry;
    MEA_EgressHeaderProc_Array_Entry_dbt  EHP_Entry;
    FilterDataStr					strFilterData;
    MEA_Uint32						mask;
    MEA_Uint32						i;
    MEA_Filter_t 					filterId;
    MEA_Bool     					found;
	MEA_Uint32                      numofShiftBit = 0;
    char 							zero_mac[6]={0,0,0,0,0,0};


    char filename[]="/home/application/sdp_client/ENET_web/serviceFilterDetail.xml";

    strcpy(strFilterData.destMac,"any");
    strcpy(strFilterData.srcMac,"any");
    strcpy(strFilterData.srcIP,"any");
    strcpy(strFilterData.dstIP,"any");
    strcpy(strFilterData.srcPort,"any");
    strcpy(strFilterData.dstPort,"any");
    strcpy(strFilterData.ipProto,"any");

    strcpy(strFilterData.editVlan,"none");
    strcpy(strFilterData.editPri,"none");
    strcpy(strFilterData.editSrcMac,"none");
    strcpy(strFilterData.editDstMac,"none");

    if(MEA_API_COMM_GetFirst_Filter (unit,&filterId,&found) != MEA_OK)
    {
		return MG_FALSE;
    }

    while(found == MEA_TRUE)
    {
    	memset(&key_from,0,sizeof(MEA_Filter_Key_dbt));
    	memset(&key_to,0,sizeof(MEA_Filter_Key_dbt));
    	memset(&data,0,sizeof(MEA_Filter_Data_dbt));
    	memset(&OutPorts_Entry,0,sizeof(MEA_OutPorts_Entry_dbt));
    	memset(&Policer_Entry,0,sizeof(MEA_Policer_Entry_dbt));
    	memset(&EHP_Entry,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));

    	if(MEA_API_COMM_Get_Filter(unit,filterId,&key_from,&key_to,&data,&OutPorts_Entry,&Policer_Entry,&EHP_Entry) != MEA_OK)
    	{
    		return MG_FALSE;
    	}

    	if(key_from.interface_key.acl_prof == service_id)
    	{
    		break;
    	}
        if(MEA_API_COMM_GetNext_Filter (unit,&filterId,&found) != MEA_OK)
        {
    		return MG_FALSE;
        }
    }

    if(found == MEA_FALSE)
	{
    	return MG_FALSE;
	}

    // get modify
    if(EHP_Entry.num_of_entries > 0)
    {
    	if(EHP_Entry.ehp_info->ehp_data.eth_info.command == MEA_EGRESS_HEADER_PROC_CMD_SWAP)
    	{
    		sprintf(strFilterData.editVlan,"%d",EHP_Entry.ehp_info->ehp_data.eth_info.val.vlan.vid);
    	}
    	else if(EHP_Entry.ehp_info->ehp_data.eth_info.command == MEA_EGRESS_HEADER_PROC_CMD_EXTRACT)
    	{
    		sprintf(strFilterData.editVlan,"%s","extract");
    	}
    	else
    	{
    		sprintf(strFilterData.editVlan,"%s","transparent");
    	}
    	sprintf(strFilterData.editPri,"%d",EHP_Entry.ehp_info->ehp_data.eth_info.val.vlan.pri);

    	if(memcmp(&EHP_Entry.ehp_info->ehp_data.martini_info.DA.b[0],zero_mac,6) != 0)
    	{
    		sprintf(strFilterData.editDstMac,"%2.2d-%2.2d-%2.2d-%2.2d-%2.2d-%2.2d",
    								EHP_Entry.ehp_info->ehp_data.martini_info.DA.b[0],
    								EHP_Entry.ehp_info->ehp_data.martini_info.DA.b[1],
    								EHP_Entry.ehp_info->ehp_data.martini_info.DA.b[2],
    								EHP_Entry.ehp_info->ehp_data.martini_info.DA.b[3],
    								EHP_Entry.ehp_info->ehp_data.martini_info.DA.b[4],
    								EHP_Entry.ehp_info->ehp_data.martini_info.DA.b[5]);

    	}
    	else
    	{
    		sprintf(strFilterData.editSrcMac,"%s","none");
    	}
    	if(memcmp(&EHP_Entry.ehp_info->ehp_data.martini_info.SA.b[0],zero_mac,6) != 0)
    	{
    		sprintf(strFilterData.editSrcMac,"%2.2d-%2.2d-%2.2d-%2.2d-%2.2d-%2.2d",
    								EHP_Entry.ehp_info->ehp_data.martini_info.SA.b[0],
    								EHP_Entry.ehp_info->ehp_data.martini_info.SA.b[1],
    								EHP_Entry.ehp_info->ehp_data.martini_info.SA.b[2],
    								EHP_Entry.ehp_info->ehp_data.martini_info.SA.b[3],
    								EHP_Entry.ehp_info->ehp_data.martini_info.SA.b[4],
    								EHP_Entry.ehp_info->ehp_data.martini_info.SA.b[5]);

    	}
    }


	switch(filter_type)
	{
	case MEA_FILTER_KEY_TYPE_DA_MAC:
		sprintf(strFilterData.destMac,"%2.2d-%2.2d-%2.2d-%2.2d-%2.2d-%2.2d",
									key_from.layer2.ethernet.DA.b[0],
									key_from.layer2.ethernet.DA.b[1],
									key_from.layer2.ethernet.DA.b[2],
									key_from.layer2.ethernet.DA.b[3],
									key_from.layer2.ethernet.DA.b[4],
									key_from.layer2.ethernet.DA.b[5]);
		break;
	case MEA_FILTER_KEY_TYPE_SA_MAC:
		sprintf(strFilterData.srcMac,"%2.2d-%2.2d-%2.2d-%2.2d-%2.2d-%2.2d",
									key_from.layer2.ethernet.SA.b[0],
									key_from.layer2.ethernet.SA.b[1],
									key_from.layer2.ethernet.SA.b[2],
									key_from.layer2.ethernet.SA.b[3],
									key_from.layer2.ethernet.SA.b[4],
									key_from.layer2.ethernet.SA.b[5]);
		break;
	case MEA_FILTER_KEY_TYPE_DST_IPV4_DST_PORT:
		mask = (pfilter_mask[1] & 0xFFFF) << 16;
		mask |= (pfilter_mask[0] & 0xFFFF0000) >> 16;
		for(i=0;i<32;i++)
		{
			numofShiftBit = (31 - i);
			if( ((mask) & (1<<(numofShiftBit))) == 0)
			{
				break;
			}
		}
		sprintf(strFilterData.dstIP,"%3.3d.%3.3d.%3.3d.%3.3d/%d",
								(key_from.layer3.IPv4.dst_ip & 0xFF000000)>>24,
								(key_from.layer3.IPv4.dst_ip & 0x00FF0000)>>16,
								(key_from.layer3.IPv4.dst_ip & 0x0000FF00)>>8,
								(key_from.layer3.IPv4.dst_ip & 0x000000FF)>>0,
								(32-i));

		mask = (pfilter_mask[0] & 0x0000FFFF);
		if(mask)
		{
			sprintf(strFilterData.dstPort,"%d",key_from.layer4.dst_port);
		}
		break;
	case MEA_FILTER_KEY_TYPE_SRC_IPV4_DST_PORT:
		mask = (pfilter_mask[1] & 0xFFFF) << 16;
		mask |= (pfilter_mask[0] & 0xFFFF0000) >> 16;
		for(i=0;i<32;i++)
		{
			numofShiftBit = (31 - i);
			if( ((mask) & (1<<(numofShiftBit))) == 0)
			{
				break;
			}
		}
		sprintf(strFilterData.srcIP,"%3.3d.%3.3d.%3.3d.%3.3d/%d",
								(key_from.layer3.IPv4.src_ip & 0xFF000000)>>24,
								(key_from.layer3.IPv4.src_ip & 0x00FF0000)>>16,
								(key_from.layer3.IPv4.src_ip & 0x0000FF00)>>8,
								(key_from.layer3.IPv4.src_ip & 0x000000FF)>>0,
								(32-i));

		mask = (pfilter_mask[0] & 0x0000FFFF);
		if(mask)
		{
			sprintf(strFilterData.dstPort,"%d",key_from.layer4.dst_port);
		}
		//key_from.layer4.dst_port;
		//key_from.layer3.IPv4.src_ip;
		break;
	case MEA_FILTER_KEY_TYPE_DST_IPV4_SRC_PORT:
		mask = (pfilter_mask[1] & 0xFFFF) << 16;
		mask |= (pfilter_mask[0] & 0xFFFF0000) >> 16;
		for(i=0;i<32;i++)
		{
			numofShiftBit = ( 31 - i);
			if( ((mask) & (1<<(numofShiftBit))) == 0)
			{
				break;
			}
		}
		sprintf(strFilterData.dstIP,"%3.3d.%3.3d.%3.3d.%3.3d/%d",
								(key_from.layer3.IPv4.dst_ip & 0xFF000000)>>24,
								(key_from.layer3.IPv4.dst_ip & 0x00FF0000)>>16,
								(key_from.layer3.IPv4.dst_ip & 0x0000FF00)>>8,
								(key_from.layer3.IPv4.dst_ip & 0x000000FF)>>0,
								(32-i));

		mask = (pfilter_mask[0] & 0x0000FFFF);
		if(mask)
		{
			sprintf(strFilterData.srcPort,"%d",key_from.layer4.src_port);
		}
		//key_from.layer3.IPv4.dst_ip;
		//key_from.layer4.src_port;
		break;
	case MEA_FILTER_KEY_TYPE_SRC_IPV4_SRC_PORT:
		mask = (pfilter_mask[1] & 0xFFFF) << 16;
		mask |= (pfilter_mask[0] & 0xFFFF0000) >> 16;
		for(i=0;i<32;i++)
		{
			numofShiftBit = (31 - i);
			if( ((mask) & (1<<(numofShiftBit))) == 0)
			{
				break;
			}
		}
		sprintf(strFilterData.srcIP,"%3.3d.%3.3d.%3.3d.%3.3d/%d",
								(key_from.layer3.IPv4.src_ip & 0xFF000000)>>24,
								(key_from.layer3.IPv4.src_ip & 0x00FF0000)>>16,
								(key_from.layer3.IPv4.src_ip & 0x0000FF00)>>8,
								(key_from.layer3.IPv4.src_ip & 0x000000FF)>>0,
								(32-i));

		mask = (pfilter_mask[0] & 0x0000FFFF);
		if(mask)
		{
			sprintf(strFilterData.srcPort,"%d",key_from.layer4.src_port);
		}
		//key_from.layer3.IPv4.src_ip;
		//key_from.layer4.src_port;
		break;
	case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG:
		break;
	case MEA_FILTER_KEY_TYPE_SRC_IPV4_APPL_ETHERTYPE:
		break;
	case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT:
		break;
	case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_SRC_PORT_DST_PORT:
		mask = pfilter_mask[1] & 0x000000FF;
		if(mask != 0)
		{
			sprintf(strFilterData.ipProto,"%d",key_from.layer3.ip_protocol);
		}
		mask = pfilter_mask[0] & 0x0000FFFF;
		if(mask != 0)
		{
			sprintf(strFilterData.dstPort,"%d",key_from.layer4.dst_port);
		}
		mask = pfilter_mask[0] & 0xFFFF0000;
		if(mask != 0)
		{
			sprintf(strFilterData.srcPort,"%d",key_from.layer4.src_port);
		}
		//key_from.layer4.src_port;
		break;
	case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_INNER_VLAN_TAG_OUTER_ETHERTYPE:
		break;
	case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_DST_PORT_SRC_PORT:
		break;
	case MEA_FILTER_KEY_TYPE_INNER_VLAN_TAG_DST_PORT_SRC_PORT:
		break;
	case MEA_FILTER_KEY_TYPE_PPPOE_SESSION_ID_PPP_PROTO_SA_MAC_LSB:
		break;
	case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_APPL_ETHERTYPE_OUTER_VLAN_TAG:
		break;

	case MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4:
		break;
#ifdef MEA_ACL_IPV6_SUPPORT
    
#endif
	case MEA_FILTER_KEY_TYPE_LAST:
	default:
		break;
	}


	buildXml_serviceId_filter_Params(filename,unit,service_id,&strFilterData);


	return MG_TRUE;

}

static int get_serviceFilter_xml(struct mg_connection *conn)
{
//	int ret=MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    int service_id = 0;
    tServiceVlanParams   entry;




	len = mg_get_var(conn, "unit", var, sizeof(var));
	if (len > 0)
	{
		unit = atoi(var);
	}
	len = mg_get_var(conn, "serviceId", var, sizeof(var));
	if (len > 0)
	{
		service_id = atoi(var);
	}
	if(MEA_API_COMM_Get_Service_vlan(unit,service_id,&entry) != MEA_OK)
	{
		fprintf(stdout,"service id :%d not exist",service_id);
		mg_send_status(conn,500);
		return MG_FALSE;
	}
	if(entry.filter_id != MEA_TRUE)
	{
		fprintf(stdout,"service id :%d filter id not exist",service_id);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	if(get_filter_mask_configuration(unit,service_id,entry.filter_id,&entry.filter_mask[0]) != MEA_TRUE)
	{
		mg_send_status(conn,500);
		return MG_FALSE;
	}
	return MG_TRUE;

}

static int get_servicevlan_byId_xml(struct mg_connection *conn)
{
	int ret=MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    int service_id = 0;
    tServiceVlanParams   entry;


	char filename[]="/home/application/sdp_client/ENET_web/serviceVlanDetail.xml";

	len = mg_get_var(conn, "unit", var, sizeof(var));
	if (len > 0)
	{
		unit = atoi(var);
	}
	len = mg_get_var(conn, "serviceId", var, sizeof(var));
	if (len > 0)
	{
		service_id = atoi(var);
	}

	if(MEA_API_COMM_Get_Service_vlan(unit,service_id,&entry) != MEA_OK)
	{
		//send_message_500(conn);
		mg_send_status(conn,500);
		return MG_FALSE;
	}

	buildXml_serviceId_vlan_Params(&entry,filename,unit,service_id);
	ret = load_text_file(conn,filename);


	return MG_TRUE;
}

static int get_service_byId_xml(struct mg_connection *conn)
{
	int ret=MG_TRUE;
    char var[80];
    int len;
    int unit = 0;
    int service_id = 0;
    MEA_EgressHeaderProc_Array_Entry_dbt  EHP_Entry;
    tComService 				tServiceEntry;


    MEA_OS_memset(&tServiceEntry,0,sizeof(tComService));
    MEA_OS_memset(&EHP_Entry,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));


	char filename[]="/home/application/sdp_client/ENET_web/serviceDetail.xml";

	len = mg_get_var(conn, "unit", var, sizeof(var));
	if (len > 0)
	{
		unit = atoi(var);
	}
	len = mg_get_var(conn, "serviceId", var, sizeof(var));
	if (len > 0)
	{
		service_id = atoi(var);
	}

	EHP_Entry.ehp_info = &tServiceEntry.ehp_info[0];
	EHP_Entry.num_of_entries = D_MAX_NUM_OF_ACTION_EDITINGS;



	ret =  MEA_API_COMM_Get_Service(unit,
										service_id,
	                                    &tServiceEntry.key,
	                                    &tServiceEntry.data,
	                                    &tServiceEntry.OutPorts_Entry,
	                                    &tServiceEntry.policer_id,
	                                    &EHP_Entry);

	tServiceEntry.num_of_entries = EHP_Entry.num_of_entries;

	tServiceEntry.num_of_clusters = 255;
	tServiceEntry.serviceId = service_id;
	buildXml_serviceId_Params(&tServiceEntry,filename);


	ret = load_text_file(conn,filename);


	return ret;
}
static int get_index_page(struct mg_connection *conn)
{
	int ret=MG_TRUE;
	char filename[]="/home/application/sdp_client/ENET_web/index.html";

	ret = load_text_file(conn,filename);



	return ret;
}
static int get_cfmStatistics_page(struct mg_connection *conn)
{
	int ret=MG_TRUE;
	char filename[]="/home/application/sdp_client/ENET_web/CfmStatistics.html";

	ret = load_text_file(conn,filename);


	return ret;
}

static int get_left_page(struct mg_connection *conn)
{
	int ret=MG_TRUE;
	char filename[]="/home/application/sdp_client/ENET_web/leftPage.html";

	ret = load_text_file(conn,filename);


	return ret;

}

static int get_solutionOverview(struct mg_connection *conn)
{
	int ret=MG_TRUE;
	char filename[]="/home/application/sdp_client/ENET_web/EthernitySolutionOverview.html";

	ret = load_text_file(conn,filename);


	return ret;
}
static int get_solutionPng(struct mg_connection *conn)
{
	int ret=MG_TRUE;
	char filename[]="/home/application/sdp_client/ENET_web/EthernityWebPage.png";

	ret = load_bin_file(conn,filename);


	return ret;
}

static int get_leftIcon(struct mg_connection *conn)
{
	int ret=MG_TRUE;
	char filename[]="/home/application/sdp_client/ENET_web/plus_minus_icons.png";

	ret = load_bin_file(conn,filename);


	return ret;
}
static int get_topStartFrame(struct mg_connection *conn)
{
	int ret=MG_TRUE;
	char filename[]="/home/application/sdp_client/ENET_web/TopStartFrame.html";

	ret = load_text_file(conn,filename);


	return ret;
}

static int get_olg_rss_css(struct mg_connection *conn)
{
	int ret=MG_TRUE;
	char filename[]="/home/application/sdp_client/ENET_web/olg_rss.css";

	ret = load_text_file(conn,filename);


	return ret;
}



tPageMethod pageMethodArray[] =
{
	{"/",get_index_page},
	{"/index.html",get_index_page},
	{"/CfmStatistics.html",get_cfmStatistics_page},
	{"/leftPage.html",get_left_page},
	{"/EthernitySolutionOverview.html",get_solutionOverview},
	{"/EthernityWebPage.png",get_solutionPng},
	{"/plus_minus_icons.png",get_leftIcon},
	{"/TopStartFrame.html",get_topStartFrame},
	{"/ingress_port.html",get_simple_page},
	{"/meaPortMapping.xml",get_xml_port_mapping},
	{"/meaIngressPortInfo.xml",get_xml_ingress_port_info},
	{"/ingress_port_setting.html",rcv_setting_port_info},
	{ "/deletePackeGenProfile.html", delete_packet_gen_profile_html},
	{ "/getPacketGenById.xml", get_packet_gen_byId_xml},
	{ "/createPacketGenProfile.html", create_packet_gen_profile_html},
    { "/policerProfile.html", policer_profile_html},
    { "/policerAllProfiles.xml", get_all_policer_profile_xml },
    { "/policerProfile.xml", get_policer_profile_byId_xml},
    { "/meaPmCounter.xml", get_PmCounter_byId_xml },
    { "/meaAllPmCounters.xml", get_PmAll_byId_xml },
    { "/meaFirstPmCounter.xml", getfirst_PmCounter_byId_xml },
    { "/meaNextPmCounter.xml", getnext_PmCounter_byId_xml },
    { "/clearPmCounters.html", clear_PmCounter_byId_html },
    { "/olg_rss.css", get_olg_rss_css },
    { "/egress_port_setting.html",rcv_setting_egress_port_info},
    { "/meaEgressPortInfo.xml",get_egress_port_info},
    { "/get_service_entry.html",rcv_service_entry_html},
    {"/serviceMapping.xml",rcv_all_services},
    {"/serviceDetail.xml",get_service_byId_xml},
	{"/style.css",get_style_css},
	{"/serviceVlanDetail.xml",get_servicevlan_byId_xml},
	{"/createServiceVlan.html",rcv_create_servicevlan},
	{"/deleteServiceVlan.html",rcv_delete_servicevlan},
	{"/serviceFilterDetail.xml",get_serviceFilter_xml},
	{"/ccmSetting.html",set_ccm_configuration},
	{"/deleteLb.html",del_lb_configuration},
	{"/deleteDm.html",del_dm_configuration},
	{"/deleteCcm.html",del_ccm_configuration},
	{"/lbSetting.html",set_lb_configuration},
	{"/dmSetting.html",set_dm_configuration},
	{"/meaGetCcmConfig.xml",get_ccm_configuration},
	{"/meaGetCcmDefectState.xml",get_ccm_defect_statistics},
	{"/meaGetCcmLmState.xml",get_ccmlm_statistics},
	{"/meaGetLbmStatistics.xml",get_lbm_analizer_statistics},
	{"/meaGetDmmStatistics.xml",get_dmm_analizer_statistics},
	{"/meaGetLbConfig.xml",get_lb_configuration},
	{"/meaGetDmConfig.xml",get_dm_configuration},
	{ "/meaRmonCounter.xml", get_RmonCounter_byId_xml },
	{ "/delPolicerProfile.html", delete_policerProfile },
	{"/rmonCounters.html", get_RmonCounters_html},
	{"/getpolicerProfile.html",getpolicerProfile_html},



};


static int web_handler(struct mg_connection *conn,enum mg_event ev)
{
	int i=0;


	//fprintf(stdout,"Received event %d\r\n",ev);
	if(conn == NULL)
	{
		fprintf(stdout,"Received connection NULL\r\n");
		return MG_FALSE;
	}

	switch(ev)
	{
	case MG_AUTH:
		return MG_TRUE;
		break;
	case MG_REQUEST:
		if(conn->uri == NULL)
		{
			fprintf(stdout,"Received uri NULL\r\n");
			return MG_FALSE;
		}
		fprintf(stdout,"Received request page %s\r\n",conn->uri);

		for(i=0;i<sizeof(pageMethodArray)/sizeof(pageMethodArray[0]);i++)
		{
			if(strcmp(conn->uri,pageMethodArray[i].Uri_name) == 0)
			{
				return pageMethodArray[i].cb_func(conn);
			}
		}
		fprintf(stdout,"page %s not found\r\n",conn->uri);
		break;
	default:
		//fprintf(stdout,"Received event %d\r\n",ev);
		break;

	}
	return MG_FALSE;
}
#if defined(MEA_OS_LINUX)
int main(void) {
	struct mg_server *server;
	const char *error_msg=NULL;

	fprintf(stdout,"application starts!!!\r\n");


	server = mg_create_server(NULL,web_handler);

	fprintf(stdout,"application starts\r\n");

	error_msg =mg_set_option(server,"document_root",".");

	if(error_msg != NULL)
		fprintf(stdout,"%s\r\n",error_msg);

	error_msg = mg_set_option(server,"listening_port","8080");

	if(error_msg != NULL)
		fprintf(stdout,"%s\r\n",error_msg);

	fprintf(stdout,"mg_set_option\r\n");

	fprintf(stdout,"Starting on port %s\r\n",mg_get_option(server,"listening_port"));

	for(;;)
	{
		mg_poll_server(server,1000);
	}

	return EXIT_SUCCESS;
}
#endif


