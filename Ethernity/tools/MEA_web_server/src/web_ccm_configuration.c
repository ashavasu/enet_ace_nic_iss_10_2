#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mongoose.h"
#include "mea_api.h"
#include "comm_mea_api.h"
#include "mea_api.h"

#include "mea_comm_msg.h"
#include "mea_client_api.h"

MEA_Status FsMiHwDeleteRxForwarder(MEA_Uint16 vlanId,MEA_Uint16 VpnIndex, MEA_Uint16 u1MdLevel,MEA_Uint32 op_code,MEA_Uint32 Src_port)
{
	MEA_SE_Entry_key_dbt key;
	MEA_SE_Entry_dbt     entry;

	memset(&key,0,sizeof(MEA_SE_Entry_key_dbt));
	key.type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
	key.oam_vpn.label_1 = vlanId;
	key.oam_vpn.VPN = VpnIndex;
	key.oam_vpn.MD_level = u1MdLevel;
	key.oam_vpn.op_code = op_code; //D_CCM_OPCODE_VALUE;
	key.oam_vpn.packet_is_untag_mpls = 0;
	key.oam_vpn.Src_port = Src_port;

	memcpy(&entry.key,&key,sizeof(MEA_SE_Entry_key_dbt));

	if(MEA_API_COMM_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		return MEA_OK;
	}

	if(MEA_API_COMM_Delete_SE_Entry(MEA_UNIT_0,&key) != MEA_OK)
	{
		return MEA_ERROR;
	}
	return MEA_OK;


}

MEA_Status FsMiHwDeleteTxForwarder(char *pMac,MEA_Uint16 VpnIndex)
{
	MEA_SE_Entry_key_dbt key;


	key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	memcpy(&key.mac_plus_vpn.MAC.b,pMac, 6);
	key.mac_plus_vpn.VPN = VpnIndex;

	if(MEA_API_COMM_Delete_SE_Entry(MEA_UNIT_0,&key) != MEA_OK)
	{
		fprintf(stdout,"vpn:%d mac address:%2.2x::%2.2x::%2.2x::%2.2x::%2.2x::%2.2x\r\n",
				VpnIndex,
				key.mac_plus_vpn.MAC.b[0],
				key.mac_plus_vpn.MAC.b[1],
				key.mac_plus_vpn.MAC.b[2],
				key.mac_plus_vpn.MAC.b[3],
				key.mac_plus_vpn.MAC.b[4],
				key.mac_plus_vpn.MAC.b[5]);

		return MEA_OK;
	}
	return MEA_OK;
}

MEA_Status FsMiHwCreateTxForwarder(MEA_Action_t action_id,char *pMac,MEA_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts)
{
	MEA_SE_Entry_dbt                      entry;

	MEA_OS_memset(&entry, 0, sizeof(entry));

	//entry.key.vlan_vpn.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	memcpy(&entry.key.mac_plus_vpn.MAC.b[0],pMac, 6);

	entry.key.mac_plus_vpn.VPN = VpnIndex;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;
	entry.data.actionId_valid  = MEA_TRUE;

	entry.data.actionId = action_id;

	memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

	if(MEA_API_COMM_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateForwarder FAIL: key allready exist!!!\n");
		return MEA_OK;
	}


	MEA_OS_memset(&entry, 0, sizeof(entry));
	//entry.key.vlan_vpn.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	memcpy(&entry.key.mac_plus_vpn.MAC.b[0],pMac, 6);

	entry.key.mac_plus_vpn.VPN = VpnIndex;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;
	entry.data.actionId_valid  = MEA_TRUE;

	entry.data.actionId = action_id;

	memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));


   if(MEA_API_COMM_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
	   fprintf(stdout,"FsMiHwCreateForwarder FAIL: MEA_API_Create_SE_Entry for Tx CCM traffic!!!\n");
		return MEA_ERROR;
	}
	return MEA_OK;
}

MEA_Status FsMiHwSetInterfaceCfmOamTagged (MEA_Port_t enetPort,int cfm_oam_qtag,int cfm_oam_tag,int cfm_oam_untag)
{
 	MEA_IngressPort_Entry_dbt entry;


	if (MEA_API_COMM_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        fprintf(stdout,"FsMiHwSetInterfaceCfmOamTagged: MEA_API_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	entry.parser_info.cfm_oam_qtag      =        cfm_oam_qtag;
	entry.parser_info.cfm_oam_tag       =        cfm_oam_tag;
	entry.parser_info.cfm_oam_untag      =       cfm_oam_untag;


	if (MEA_API_COMM_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwSetInterfaceCfmOamTagged: MEA_API_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}



    return (MEA_OK);
}


MEA_Status FsMiHwCreateCfmAction(MEA_Action_t *pAction_id,MEA_EHP_Info_dbt *pEhp_info,int num_of_entries,MEA_OutPorts_Entry_dbt *pOutPorts,MEA_Uint16 policer_id,MEA_Ingress_TS_type_t type)
{
	MEA_Action_Entry_Data_dbt               Action_data;
	MEA_Policer_Entry_dbt                   Action_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt   Action_editing;


	MEA_OS_memset(&Action_data,0,sizeof(MEA_Action_Entry_Data_dbt));
	MEA_OS_memset(&Action_policer,0,sizeof(MEA_Policer_Entry_dbt));
	MEA_OS_memset(&Action_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));

	(*pAction_id) = MEA_PLAT_GENERATE_NEW_ID;

	Action_data.proto_llc_valid = MEA_TRUE;
	Action_data.Llc = 0;
	Action_data.protocol_llc_force = MEA_TRUE;
	Action_data.Protocol = 1;
	Action_data.ed_id_valid  =  1;
	Action_data.ed_id        =  0;
	Action_data.pm_id_valid = 1;
	Action_data.pm_id        =  0;
	Action_data.fwd_ingress_TS_type = type;

//	ehp_info.ehp_data.eth_info.val.all        = 0;
//	ehp_info.ehp_data.eth_info.stamp_color    = 0;
//	ehp_info.ehp_data.eth_info.stamp_priority = 0;
//	ehp_info.ehp_data.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_TRANS;

	Action_editing.ehp_info = &pEhp_info[0];
	Action_editing.num_of_entries = num_of_entries;


	fprintf(stdout,"FsMiHwCreateCfmAction policer id:%d\n",policer_id);


	if (MEA_API_COMM_Create_Action (MEA_UNIT_0,
							   &Action_data,
							   pOutPorts,
							   policer_id,
							   &Action_editing,
							   pAction_id) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateForwarderCfmAction FAIL: MEA_API_COMM_Create_Action failed for action:%d\n",(*pAction_id));
	   return MEA_ERROR;
	}
	return MEA_OK;
}




MEA_Status FsMiHwPacketGenProfInfoCreate(int header_length,MEA_PacketGen_profile_info_t  *pId_io,MEA_Uint32 stream_type)
{
    MEA_PacketGen_drv_Profile_info_entry_dbt      entry;

	MEA_OS_memset(&entry,0,sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));

	entry.header_length = header_length;
	entry.stream_type = stream_type; //CCM=3 LBM/LBR=1 DMM/DMR=2
	entry.seq_stamping_enable  = 1;
	entry.seq_offset_type     = MEA_PACKETGEN_SEQ_TYPE_2;
	entry.forceErrorPrbs	= 0;


	(*pId_io)=MEA_PLAT_GENERATE_NEW_ID;

   if (MEA_API_COMM_Create_PacketGen_Profile_info(MEA_UNIT_0,
                                      &entry,
                                      pId_io)!=MEA_OK)
	{
	   fprintf(stdout,"Error: MEA_API_Create_PacketGen_Profile_info fail\n");
        return MEA_ERROR;
    }
	return MEA_OK;
}

MEA_Status FsMiHwGetInterfaceCfmOamUCMac (MEA_Port_t enetPort,char* macAddr)
{
 	MEA_IngressPort_Entry_dbt entry;
	MEA_Uint16					idx;


	if (MEA_API_COMM_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwSetInterfaceCfmOamUCMac: MEA_API_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	for(idx=0;idx<6;idx++)
	{
		macAddr[idx] = entry.parser_info.my_Mac.b[idx];
	}
    return (MEA_OK);
}

MEA_Status FsMiHwPacketGenDmmCreate(tPacketGenDmmHeader *pLDmHeader,MEA_PacketGen_profile_info_t Id_io,MEA_PacketGen_t  *pId_io,MEA_EirCir_t packetPerSecond)
{
	MEA_PacketGen_Entry_dbt entry;
//	MEA_Uint32	idx=0;
	MEA_Uint32  oamInfo=0;

	MEA_OS_memset(&entry,0,sizeof(MEA_PacketGen_Entry_dbt));


	entry.profile_info.valid =1;
	entry.profile_info.id =Id_io;
	entry.profile_info.headerType =0; //120 bytes

	MEA_OS_memcpy(&entry.header.data[entry.header.length],pLDmHeader->DA.b,6);
	entry.header.length	+= 6; //DA
	MEA_OS_memcpy(&entry.header.data[entry.header.length],pLDmHeader->SA.b,6);
	entry.header.length	+= 6; //SA

	if(pLDmHeader->num_of_vlans)
	{
		MEA_OS_memcpy(&entry.header.data[entry.header.length],&pLDmHeader->outerVlan,sizeof(MEA_Uint32));
		entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		if(pLDmHeader->num_of_vlans > 1)
		{
			MEA_OS_memcpy(&entry.header.data[entry.header.length],&pLDmHeader->innerVlan,sizeof(MEA_Uint32));
			entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		}
	}
	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pLDmHeader->cfmEtherType,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //etherType

	oamInfo =  ( ( pLDmHeader->meg_level & 0x000007) << 5);
	oamInfo |=  (pLDmHeader->version & 0x0000F8);
	entry.header.data[entry.header.length++] = oamInfo & 0xFF;
	entry.header.data[entry.header.length++] = pLDmHeader->opCode & 0xFF;
	entry.header.data[entry.header.length++] = pLDmHeader->flags & 0xFF;
	entry.header.data[entry.header.length++] = pLDmHeader->tlvOffset & 0xFF;


	entry.control.numOf_packets	= 0xFFFF; //forever

	entry.control.active = 0; //active=1 start transmit

	entry.control.loop = 0; //

	entry.control.Packet_error=0; // insert error to packet

	entry.control.Burst_size=1;

	entry.setting.resetPrbs	= 1; //case send prbs it could to reset the prbs


	entry.data.pattern_type   = MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE; // or MEA_PACKET_GEN_DATA_PATTERN_TYPE_PRBS// MEA_PacketGen_DataPatternType_te


    entry.data.pattern_value.s  = MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55;

	entry.data.withCRC        = 0; // done by HW


	entry.length.type  = MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE; //MEA_PacketGen_LengthType_te
	entry.length.start = 128;
	entry.length.end   = 128;
	entry.length.step = 1;

	entry.shaper.shaper_enable = ENET_TRUE;
	entry.shaper.shaper_info.CIR  = packetPerSecond ;
	entry.shaper.shaper_info.CBS = 0;
	entry.shaper.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_PKT;
	entry.shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
	entry.shaper.shaper_info.overhead = 0 ;
	entry.shaper.shaper_info.Cell_Overhead = 0;

	fprintf(stdout,"CIR=%llu\n",entry.shaper.shaper_info.CIR);



	(*pId_io)=MEA_PLAT_GENERATE_NEW_ID;
	if(MEA_API_COMM_Create_PacketGen_Entry(MEA_UNIT_0,
                                      &entry,
                                      pId_io)!=MEA_OK)
	{
		fprintf(stdout,"Error: can't lbm create PacketGen \n");
        return MEA_ERROR;
    }
	return MEA_OK;

}


MEA_Status FsMiHwPacketGenLbmCreate(tPacketGenLbmHeader *pLbmHeader,MEA_PacketGen_profile_info_t Id_io,MEA_PacketGen_t  *pId_io,MEA_EirCir_t packetPerSecond,int prbs)
{
	MEA_PacketGen_Entry_dbt entry;
//	MEA_Uint32	idx=0;
	MEA_Uint32  oamInfo=0;

	MEA_OS_memset(&entry,0,sizeof(MEA_PacketGen_Entry_dbt));

	pLbmHeader->flags=0;


	entry.profile_info.valid =1;
	entry.profile_info.id =Id_io;
	entry.profile_info.headerType =0; //120 bytes

	MEA_OS_memcpy(&entry.header.data[entry.header.length],pLbmHeader->DA.b,6);
	entry.header.length	+= 6; //DA
	MEA_OS_memcpy(&entry.header.data[entry.header.length],pLbmHeader->SA.b,6);
	entry.header.length	+= 6; //SA

	if(pLbmHeader->num_of_vlans)
	{
		MEA_OS_memcpy(&entry.header.data[entry.header.length],&pLbmHeader->outerVlan,sizeof(MEA_Uint32));
		entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		if(pLbmHeader->num_of_vlans > 1)
		{
			MEA_OS_memcpy(&entry.header.data[entry.header.length],&pLbmHeader->innerVlan,sizeof(MEA_Uint32));
			entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		}
	}
	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pLbmHeader->cfmEtherType,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //etherType

	oamInfo =  ( ( pLbmHeader->meg_level & 0x000007) << 5);
	oamInfo |=  (pLbmHeader->version & 0x0000F8);
	entry.header.data[entry.header.length++] = oamInfo & 0xFF;
	entry.header.data[entry.header.length++] = pLbmHeader->opCode & 0xFF;
	entry.header.data[entry.header.length++] = pLbmHeader->flags & 0xFF;
	entry.header.data[entry.header.length++] = pLbmHeader->tlvOffset & 0xFF;

	//fprintf(stdout,"***************** tlv offset:%lu flag:%lu********************* \n",pLbmHeader->tlvOffset,pLbmHeader->flags);



	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pLbmHeader->initSeqNum,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //seq number

//	pLbmHeader->Length = 1000 - 18 - 8; //header and lbm header

	entry.header.data[entry.header.length++] = pLbmHeader->type & 0xFF;
//	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pLbmHeader->Length,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //length
	entry.header.data[entry.header.length++] = pLbmHeader->Pattern_Type & 0xFF; // tlv offset



	entry.control.numOf_packets	= 0xFFFF; //forever

	entry.control.active = 0; //active=1 start transmit

	entry.control.loop = 0; //

	entry.control.Packet_error=0; // insert error to packet

	entry.control.Burst_size=1;

	entry.setting.resetPrbs	= 1; //case send prbs it could to reset the prbs


	entry.data.pattern_type   = MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE; //MEA_PACKET_GEN_DATA_PATTERN_TYPE_PRBS;

	// -loop 0 -error 0 -reset_prbs 1 -data 1 0000 0 -len 3 125 125 1 -s 1 2 0 0 1 0 0"


    entry.data.pattern_value.s  = MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55;

	if(prbs==1)
	{
		entry.data.pattern_type   = MEA_PACKET_GEN_DATA_PATTERN_TYPE_PRBS;
		entry.data.pattern_value.s = MEA_PACKET_GEN_DATA_PATTERN_VALUE_0000;
	}

	entry.data.withCRC        = 0; // done by HW


	entry.length.type  = MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE; //MEA_PacketGen_LengthType_te
	entry.length.start = 125;
	entry.length.end   = 125;
	entry.length.step = 0;

	entry.shaper.shaper_enable = ENET_TRUE;
	entry.shaper.shaper_info.CIR  = packetPerSecond ;
	entry.shaper.shaper_info.CBS = 0;
	entry.shaper.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_PKT;
	entry.shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
	entry.shaper.shaper_info.overhead = 0 ;
	entry.shaper.shaper_info.Cell_Overhead = 0;




	(*pId_io)=MEA_PLAT_GENERATE_NEW_ID;
	if(MEA_API_COMM_Create_PacketGen_Entry(MEA_UNIT_0,
                                      &entry,
                                      pId_io)!=MEA_OK)
	{
		fprintf(stdout,"Error: can't lbm create PacketGen \n");
        return MEA_ERROR;
    }
	return MEA_OK;

}

MEA_Status FsMiHwPacketGenDeleteiNFO(MEA_PacketGen_t  Id_io)
{

	return MEA_API_COMM_Delete_PacketGen_Entry(MEA_UNIT_0,Id_io);
}

MEA_Status FsMiHwPacketGenCreateiNFO(tPacketGenHeader *pPacketGen,MEA_PacketGen_profile_info_t Id_io,MEA_PacketGen_t  *pId_io,MEA_Uint32 packetPerSecond)
{
	MEA_PacketGen_Entry_dbt entry;
	MEA_Uint32	idx=0;
	MEA_Uint32  oamInfo=0;

	MEA_OS_memset(&entry,0,sizeof(MEA_PacketGen_Entry_dbt));


	entry.profile_info.valid =1;
	entry.profile_info.id =Id_io;
	entry.profile_info.headerType =1; //120 bytes

	MEA_OS_memcpy(&entry.header.data[entry.header.length],pPacketGen->DA.b,6);
	entry.header.length	+= 6; //DA
	MEA_OS_memcpy(&entry.header.data[entry.header.length],pPacketGen->SA.b,6);
	entry.header.length	+= 6; //SA

	if(pPacketGen->num_of_vlans)
	{
		MEA_OS_memcpy(&entry.header.data[entry.header.length],&pPacketGen->outerVlan,sizeof(MEA_Uint32));
		entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		if(pPacketGen->num_of_vlans > 1)
		{
			MEA_OS_memcpy(&entry.header.data[entry.header.length],&pPacketGen->innerVlan,sizeof(MEA_Uint32));
			entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		}
	}
	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pPacketGen->cfmEtherType,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //etherType

	oamInfo =  ( ( pPacketGen->meg_level & 0x000007) << 5);
	oamInfo |=  (pPacketGen->version & 0x0000F8);
	entry.header.data[entry.header.length++] = oamInfo & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->opCode & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->flags & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->tlvOffset & 0xFF;


	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pPacketGen->initSeqNum,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //seq number

	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pPacketGen->mepId,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //mepid

	for(idx=0;idx<pPacketGen->megLen;idx++)
	{
		entry.header.data[entry.header.length++]= pPacketGen->megData[idx];
	}

	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pPacketGen->TxFCf,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //TxFCf information

	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pPacketGen->RxFCb,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //RxFCb information

	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pPacketGen->TxFCb,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //TxFCb information

	MEA_OS_memcpy(&entry.header.data[entry.header.length],&pPacketGen->TxFCb,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //TxFCb information

	entry.header.length++;

	entry.control.numOf_packets	= 0xFFFF; //forever

	entry.control.active = 0; //active=1 start transmit

	entry.control.loop = 0; //

	entry.control.Packet_error=0; // insert error to packet

	entry.control.Burst_size=1;

	entry.setting.resetPrbs	= 1; //case send prbs it could to reset the prbs


	entry.data.pattern_type   = MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE; // MEA_PacketGen_DataPatternType_te


    entry.data.pattern_value.s  = MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55;

	entry.data.withCRC        = 0; // done by HW


	entry.length.type  = MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE; //MEA_PacketGen_LengthType_te
	entry.length.start = 128;
	entry.length.end   = 128;
	entry.length.step = 1;

	entry.shaper.shaper_enable = ENET_TRUE;
	entry.shaper.shaper_info.CIR  = packetPerSecond ;
	entry.shaper.shaper_info.CBS = 0;
	entry.shaper.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_PKT;
	entry.shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
	entry.shaper.shaper_info.overhead = 0 ;
	entry.shaper.shaper_info.Cell_Overhead = 0;


	(*pId_io)=MEA_PLAT_GENERATE_NEW_ID;
	if(MEA_API_COMM_Create_PacketGen_Entry(MEA_UNIT_0,
                                      &entry,
                                      pId_io)!=MEA_OK)
	{
		fprintf(stdout,"Error: can't create PacketGen \n");
        return MEA_ERROR;
    }
	return MEA_OK;
}

MEA_Status FsMiHwCreateCfmService(MEA_Service_t *pService_id,
											MEA_Uint32 vpn,
											MEA_Uint16 vlanId,
											 MEA_Uint16 EnetPolilcingProfile,
											 MEA_Bool b_lmEnable,
											 MEA_Uint16 lmId,
											 MEA_Uint16 src_port,
											 MEA_Uint32 keyType,
											 MEA_Uint32 me_valid,
											 MEA_Uint32 me_level)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	MEA_OutPorts_Entry_dbt 				  OutPorts;
	MEA_EHP_Info_dbt 					  tInfo;



	MEA_OS_memset(&Service_editing , 0 , sizeof(MEA_EgressHeaderProc_Array_Entry_dbt ));
	MEA_OS_memset(&Service_key    , 0 , sizeof(MEA_Service_Entry_Key_dbt ));
	MEA_OS_memset(&Service_data , 0 , sizeof(MEA_Service_Entry_Data_dbt ));
	MEA_OS_memset(&OutPorts    , 0 , sizeof(MEA_OutPorts_Entry_dbt ));
	MEA_OS_memset(&tInfo    , 0 , sizeof(MEA_EHP_Info_dbt ));


	Service_data.policer_prof_id = EnetPolilcingProfile;

	Service_key.src_port         = src_port;
	Service_key.L2_protocol_type =MEA_PARSING_L2_KEY_Ctag; /* Ethertypes: 88a8/8100*/
	Service_key.net_tag          = vlanId; //configure vlan
	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	Service_key.evc_enable = MEA_TRUE;
	Service_key.pri              = 0;

	// MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN
	// MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN

	Service_data.DSE_forwarding_enable   = MEA_TRUE;
	Service_data.DSE_forwarding_key_type = keyType;
	Service_data.DSE_learning_enable     = MEA_FALSE;
	Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	Service_data.DSE_learning_srcPort_force = MEA_FALSE;

	Service_data.CFM_OAM_ME_Level_Threshold_valid = me_valid;
	Service_data.CFM_OAM_ME_Level_Threshold_value = me_level;



	Service_data.vpn = vpn; //vpn

    /* Set the upstream service data attribute for policing */
    Service_data.tmId   = 0; /* generate new id */
    Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL; /* No upstream policing */

    /* Set the upstream service data attribute for pm (counters) */
    Service_data.pmId   = 0; /* generate new id */

    /* Set the upstream service data attribute for policing */
    Service_data.editId = 0; /* generate new id */

    /* Set the other upstream service data attributes to defaults */
    Service_data.ADM_ENA            = MEA_FALSE;
    Service_data.CM                 = MEA_FALSE;
    Service_data.L2_PRI_FORCE       = MEA_FALSE;
    Service_data.L2_PRI             = 0;
    Service_data.COLOR_FORSE        = MEA_TRUE;
    Service_data.COLOR              = 2;
    Service_data.COS_FORCE          = MEA_FALSE;
    Service_data.COS                = 0;
    Service_data.protocol_llc_force = MEA_TRUE;
    Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
    Service_data.Llc                = MEA_FALSE;


	(*pService_id) = MEA_PLAT_GENERATE_NEW_ID;

	Service_editing.num_of_entries = 1;

	tInfo.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
	tInfo.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	tInfo.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tInfo.ehp_data.eth_info.val.vlan.vid      = 0;

	tInfo.ehp_data.eth_info.stamp_color       = MEA_FALSE;
	tInfo.ehp_data.eth_info.stamp_priority    = MEA_FALSE;

	Service_editing.ehp_info = &tInfo;

	if(b_lmEnable == MEA_TRUE)
	{
		tInfo.ehp_data.LmCounterId_info.valid = MEA_TRUE;
		tInfo.ehp_data.LmCounterId_info.LmId = lmId;
		tInfo.ehp_data.LmCounterId_info.Command_dasa=0;
		tInfo.ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_STAMP_TX;
	}



    if (MEA_API_COMM_Create_Service(MEA_UNIT_0,
                               &Service_key,
                               &Service_data,
                               &OutPorts,
                               EnetPolilcingProfile,
                               &Service_editing,
                               pService_id) != MEA_OK)
    {
    	fprintf(stdout,"FsMiHwCreateCfmService FAIL: Unable to create the new service %d policerId:%d\n", (*pService_id),EnetPolilcingProfile);
        /*T.B.D. Rollback some of the previous actions if fail! */
        return MEA_ERROR;

    }
	return (MEA_OK);
}

MEA_Status FsMiHwPacketGenActive(MEA_PacketGen_t  Id_io,MEA_Bool active)
{
	MEA_PacketGen_Entry_dbt         entry;


	if(MEA_API_COMM_Get_PacketGen_Entry(MEA_UNIT_0,Id_io, &entry)!=MEA_OK)
	{
		fprintf(stdout,"Error: can't get PacketGen \n");
        return MEA_ERROR;
    }

	entry.control.active = active;

	if(MEA_API_COMM_Set_PacketGen_Entry(MEA_UNIT_0,Id_io, &entry)!=MEA_OK)
	{
		fprintf(stdout,"Error: can't set PacketGen \n");
        return MEA_ERROR;
    }
//	if(FsMiHwPacketGenCreateiNFO(&tPacketGen,pConfig->packetGen.packetGenProfileId,&pConfig->packetGen.packetGenId,packetsPerSecond) != MEA_OK)
//	{
//		fprintf(stdout,"FsCreateHwPacketGenerator: failed to create packet gen id:%d\n",pConfig->packetGen.packetGenId);
//		return MEA_ERROR;
//	}


	return MEA_OK;

}

int delete_Ccm_generator(tCcmConfiguration *pConfig)
{

	// delete generator
	FsMiHwPacketGenDeleteiNFO(pConfig->packetGen.packetGenId);

	// delete profile generator
	 MEA_API_COMM_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,pConfig->packetGen.packetGenProfileId);

	// delete service
	MEA_API_COMM_Delete_Service (MEA_UNIT_0,pConfig->packetGen.Service_id);

	//delete forwarder
	FsMiHwDeleteTxForwarder((char *)&pConfig->destMac.b[0],pConfig->packetGen.VpnIndex);


	//delete action
	MEA_API_COMM_Delete_Action (MEA_UNIT_0,pConfig->packetGen.action_id);

	return MEA_OK;

}

int create_Ccm_generator(tCcmConfiguration *pConfig)
{
	MEA_EHP_Info_dbt tEhp_info;
	MEA_OutPorts_Entry_dbt tOutPorts;
	MEA_Uint16 VpnIndex=101;
	char srcmacAddr[6];
	tPacketGenHeader tPacketGen;
	MEA_Uint16 packetsPerSecond=0;

	memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));
	memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	if(FsMiHwSetInterfaceCfmOamTagged (pConfig->srcPort,1,1,1) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwSetInterfaceCfmOamTagged: set action failed sourcePort:%d\n",pConfig->srcPort);
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwSetInterfaceCfmOamTagged: port:%d succeeded\n",pConfig->srcPort);


	VpnIndex = allocate_vpn_entry();

	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = pConfig->vlanId;

	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;



	MEA_SET_OUTPORT(&tEhp_info.output_info,pConfig->srcPort);
	MEA_SET_OUTPORT(&tOutPorts,pConfig->srcPort);



	if(FsMiHwCreateCfmAction(&pConfig->packetGen.action_id,&tEhp_info,1,&tOutPorts,pConfig->policerId,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: create action failed actionId:%d\n",pConfig->packetGen.action_id);
		return MEA_ERROR;
	}
	fprintf(stdout,"FsMiHwCreateCfmAction: actionId:%d succeeded\n",pConfig->packetGen.action_id);


	pConfig->packetGen.VpnIndex=VpnIndex;
	if(FsMiHwCreateTxForwarder(pConfig->packetGen.action_id,(char *)&pConfig->destMac.b[0],VpnIndex,&tOutPorts) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateTxForwarder: create forwarder failed src_port:%d\n",pConfig->srcPort);
		return MEA_ERROR;
	}
	fprintf(stdout,"FsMiHwCreateTxForwarder: actionId:%d succeeded\n",pConfig->packetGen.action_id);

	if(FsMiHwPacketGenProfInfoCreate(93,&pConfig->packetGen.packetGenProfileId,3) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: create packet gen profile failed\n");
		return MEA_ERROR;
	}
	fprintf(stdout,"FsMiHwPacketGenProfInfoCreate: profileId:%d succeeded\n",pConfig->packetGen.packetGenProfileId);

	if(FsMiHwGetInterfaceCfmOamUCMac (pConfig->srcPort,&srcmacAddr[0]) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwGetInterfaceCfmOamUCMac: create packet gen profile failed\n");
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwGetInterfaceCfmOamUCMac succeeded\n");

	MEA_OS_memcpy(&tPacketGen.DA.b[0],(char *)&pConfig->destMac.b[0],6);
	MEA_OS_memcpy(&tPacketGen.SA.b[0],srcmacAddr,6);
	tPacketGen.num_of_vlans=1;
	tPacketGen.outerVlan=0x81000000 | pConfig->vlanId | ( (pConfig->u1CcmPriority & 0x7) << 13);
	tPacketGen.innerVlan=0x81000000;
	tPacketGen.cfmEtherType=0x8902;
	tPacketGen.meg_level=pConfig->megLevel; //3 bits
	tPacketGen.version=0; //5 bits
	tPacketGen.opCode=D_CCM_OPCODE_VALUE; // 8 bits

	tPacketGen.flags=pConfig->ccmPeriod; // 8 bits

	tPacketGen.tlvOffset=70; // 8 bits
	tPacketGen.initSeqNum=0;
	tPacketGen.mepId=pConfig->localMepId;
	tPacketGen.megLen=48;
	memcpy(&tPacketGen.megData[0],pConfig->megId,48);
	tPacketGen.TxFCf=0;
	tPacketGen.RxFCb=0;
	tPacketGen.TxFCb=0;

	switch(pConfig->ccmPeriod)
	{
	case D_CCM_3_3MS_PERIOD:
		packetsPerSecond=333;
		break;
	case D_CCM_10MS_PERIOD:
		packetsPerSecond=100;
		break;
	case D_CCM_100MS_PERIOD:
		packetsPerSecond=10;
		break;
	case D_CCM_1SEC_PERIOD:
	case D_CCM_10SEC_PERIOD:
	case D_CCM_1MIN_PERIOD:
	case D_CCM_10MIN_PERIOD:
		packetsPerSecond=1;
		break;
	default:
		break;
	}


	if(FsMiHwPacketGenCreateiNFO(&tPacketGen,pConfig->packetGen.packetGenProfileId,&pConfig->packetGen.packetGenId,packetsPerSecond) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: failed to create packet gen id:%d\n",pConfig->packetGen.packetGenId);
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwPacketGenCreateiNFO genEntry:%d succeeded\n",pConfig->packetGen.packetGenId);

	pConfig->packetGen.Service_id=MEA_PLAT_GENERATE_NEW_ID;
	if(FsMiHwCreateCfmService(&pConfig->packetGen.Service_id,
									pConfig->packetGen.VpnIndex,
									pConfig->vlanId,
									pConfig->policerId,
									pConfig->lmEnable,
									pConfig->lmId,
									120,
									MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN,
									MEA_FALSE,0) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: failed to create packet gen id\n");
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwCreateCfmService service:%d succeeded\n",pConfig->packetGen.Service_id);

	if(FsMiHwPacketGenActive(pConfig->packetGen.packetGenId,MEA_TRUE) != MEA_OK)
	{
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwPacketGenActive generator:%d succeeded\n",pConfig->packetGen.packetGenId);

	return MEA_OK;
}

MEA_Status FsMiHwCreateAnalyzer(MEA_CcmId_t  *id_io,int mepId,int period,int seqNumber,int ccm_lmr,char *megData)
{
	MEA_CCM_Configure_dbt      entry;


	MEA_OS_memset(&entry,0,sizeof(entry));
	(*id_io) = MEA_PLAT_GENERATE_NEW_ID;


	entry.expected_MEP_Id=mepId;
	entry.mode_ccm_lmr=ccm_lmr; //ccm only
	entry.expected_period=period; //3.3
	entry.period_event=period; // 3.3
	entry.enable_clearSeq=0;
	entry.value_nextSeq=seqNumber;
	memcpy(&entry.MEG_data[0],megData,MEA_ANALYZER_CCM_MAX_MEG_DATA);

   if(MEA_API_COMM_Create_CCM_Entry( MEA_UNIT_0,&entry,id_io)!=MEA_OK)
	{
	   fprintf(stdout,"Error: can't create CCM_Entry \n");
	   return MEA_ERROR;
    }

	return MEA_OK;
}

MEA_Status FsMiHwCreateRxForwarder(MEA_Action_t action_id,MEA_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts,MEA_Uint32  MD_level,MEA_Uint32 op_code,MEA_Uint16 vlanId,MEA_Uint32 Src_port)
{
	MEA_SE_Entry_dbt                      	entry;

	MEA_OS_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
    //entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	entry.key.oam_vpn.label_1 = vlanId;
	entry.key.oam_vpn.VPN = VpnIndex;
	entry.key.oam_vpn.MD_level = MD_level;
	entry.key.oam_vpn.op_code = op_code;
	entry.key.oam_vpn.packet_is_untag_mpls = 0;
	entry.key.oam_vpn.Src_port = Src_port;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;
	entry.data.actionId_valid  = MEA_TRUE;


	entry.data.actionId = action_id;


	// check if allready exist
	if(MEA_API_COMM_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateRxForwarder: key allready exist!!!\n");
		return MEA_OK;
	}

	memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

   if(MEA_API_COMM_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
	   	fprintf(stdout,"FsMiHwCreateRxForwarder FAIL: MEA_API_Create_SE_Entry FAIL!!!\n");
		return MEA_ERROR;
	}
	return MEA_OK;
}

int SetServiceVpnAndMdLevel(int service_id,int level,MEA_Bool valid,MEA_Uint16 *pVpnIndex,MEA_Bool lmEnable,MEA_Uint16 lmId,MEA_Uint16 machine)
{
	MEA_Service_Entry_Key_dbt      key;
	MEA_Service_Entry_Data_dbt     data;
	MEA_OutPorts_Entry_dbt         OutPorts_Entry;
	MEA_Uint16                     policer_id;
	MEA_EgressHeaderProc_Array_Entry_dbt  EHP_Entry;
    MEA_EHP_Info_dbt 				ehp_info[8];

    memset(&key,0,sizeof(MEA_Service_Entry_Key_dbt));
    memset(&data,0,sizeof(MEA_Service_Entry_Data_dbt));
    memset(&ehp_info,0,sizeof(MEA_EHP_Info_dbt)*8);

    EHP_Entry.ehp_info = &ehp_info[0];
    EHP_Entry.num_of_entries = 8;


    if(service_id == 0)
    {
	   	fprintf(stdout,"SetServiceVpnAndMdLevel FAIL: service id 0!!!\n");
		return MEA_ERROR;
    }

	if(MEA_API_COMM_Get_Service(MEA_UNIT_0,service_id,&key,&data,&OutPorts_Entry,&policer_id,&EHP_Entry) != MEA_OK)
	{
	   	fprintf(stdout,"SetServiceVpnAndMdLevel FAIL: MEA_API_COMM_Get_Service FAIL!!!\n");
		return MEA_ERROR;
	}

	data.CFM_OAM_ME_Level_Threshold_valid = valid;
	data.CFM_OAM_ME_Level_Threshold_value = level;

	(*pVpnIndex) = (MEA_Uint16)data.vpn;
	data.editId=0;

	if(lmEnable)
	{
		EHP_Entry.ehp_info->ehp_data.LmCounterId_info.valid = MEA_TRUE;
		EHP_Entry.ehp_info->ehp_data.LmCounterId_info.LmId = lmId;
		EHP_Entry.ehp_info->ehp_data.LmCounterId_info.Command_dasa=0;
		EHP_Entry.ehp_info->ehp_data.LmCounterId_info.Command_cfm=machine;
	}

	if (MEA_API_COMM_Set_Service(MEA_UNIT_0,service_id,&data,&OutPorts_Entry,policer_id,&EHP_Entry) != MEA_OK)
	{
		fprintf(stdout,"DPL_MiVlanSetServiceMdLevel ERROR: MEA_API_Set_Service failed for service:%d\n", service_id);
		return MEA_ERROR;
	}

	return MEA_OK;


}

MEA_Status FsMiHwSetGenericAnalyzer(MEA_Analyzer_t id,int seqNumber)
{
    MEA_Analyzer_configure_dbt    entry;

	 MEA_OS_memset(&entry,0,sizeof(entry));


	if(MEA_API_COMM_Get_Analyzer(MEA_UNIT_0,id,&entry) !=  MEA_OK)
	{
		fprintf(stdout,"Error: can't get MEA_API_Get_Analyzer entry:%d \n",id);
         return MEA_ERROR;
	}
	entry.activate = 1;
	entry.enable_clearSeq=1;
	entry.resetPrbs=1;
	entry.value_nextSeq=seqNumber;

    if(MEA_API_COMM_Set_Analyzer(MEA_UNIT_0,id,&entry)!=MEA_OK)
	{
    	fprintf(stdout,"Error: can't get MEA_API_Set_Analyzer entry:%d \n",id);
         return MEA_ERROR;
    }
	return MEA_OK;

}


int delete_Ccm_analyzer(tCcmConfiguration *pConfig)
{
	// delete forwarder
	FsMiHwDeleteRxForwarder(pConfig->vlanId,
							pConfig->packetAna.VpnIndex,
							pConfig->megLevel,
							D_CCM_OPCODE_VALUE,pConfig->srcPort);

	// delete action
	MEA_API_COMM_Delete_Action (MEA_UNIT_0,pConfig->packetAna.action_id);

	// delete analyzer
	MEA_API_COMM_Delete_CCM_Entry(MEA_UNIT_0,pConfig->packetAna.id_io);


	return MEA_OK;

}


int create_Ccm_analyzer(tCcmConfiguration *pConfig)
{
	MEA_EHP_Info_dbt 	tEhp_info[2];
	int 				num_of_entries=0;
	MEA_OutPorts_Entry_dbt tOutPorts;
	MEA_Uint16 VpnIndex=0;
//	int expected_period=0;



	if(SetServiceVpnAndMdLevel(pConfig->outgoing_service_id,pConfig->megLevel,MEA_TRUE,&VpnIndex,pConfig->lmEnable,pConfig->lmId,MEA_LM_COMMAND_TYPE_COUNT_CCM_TX) != MEA_OK)
	{
		fprintf(stdout,"GetServiceVpn: failed to create analyzer profile\r\n");
		return MEA_ERROR;
	}
	if(SetServiceVpnAndMdLevel(pConfig->incoming_service_id,pConfig->megLevel,MEA_TRUE,&VpnIndex,pConfig->lmEnable,pConfig->lmId,MEA_LM_COMMAND_TYPE_COUNT_CCM_RX) != MEA_OK)
	{
		fprintf(stdout,"GetServiceVpn: failed to create analyzer profile\r\n");
		return MEA_ERROR;
	}



	memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));



	tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = pConfig->lmId;

	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	if(pConfig->lmEnable == MEA_TRUE)
	{
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.valid = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.LmId = pConfig->lmId;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.Command_dasa=0;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_STAMP_RX;

	}

	MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,120);
	MEA_SET_OUTPORT(&tOutPorts, 120);


	pConfig->packetAna.ccm_interval = pConfig->ccmPeriod;


	// create analyzer
	// MEA analyzer CCM set create 1 -mep_id 2222 -time_event 1 -clear_segid 1 0 -period_id 1
	if(FsMiHwCreateAnalyzer(&pConfig->packetAna.id_io,
							pConfig->remoteMepId,
							pConfig->ccmPeriod,
							0,
							(pConfig->lmEnable == MEA_TRUE)?1:0,
							(char *)pConfig->megId) != MEA_OK)
	{
		fprintf(stdout,"FsCreatePacketAnalyzer: failed to create analyzer profile\r\n");
		return MEA_ERROR;
	}

	pConfig->packetAna.VpnIndex = VpnIndex;

	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = pConfig->packetAna.id_io;
	num_of_entries++;

	// create action
	// MEA action set create -f 1033 -pm 1 103 -ed 1 0 -h 81000001 0 0 3 -lmid 1 1 0 3
	if(FsMiHwCreateCfmAction(&pConfig->packetAna.action_id,&tEhp_info[0],num_of_entries,&tOutPorts,pConfig->policerId,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		fprintf(stdout,"FsCreatePacketAnalyzer: failed to create action\n");
		return MEA_ERROR;
	}


	// MEA forwarder add 4 2 102 2 1 0 3 1 0 1 120 -action 1 1033
	if(FsMiHwCreateRxForwarder(pConfig->packetAna.action_id,
								VpnIndex,
								&tOutPorts,pConfig->megLevel,
								D_CCM_OPCODE_VALUE,
								pConfig->vlanId,
								pConfig->srcPort) != MEA_OK)
	{
		fprintf(stdout,"FsCreatePacketAnalyzer: failed to create forwarder\r\n");
		return MEA_ERROR;
	}

/*

	DPL_MiVlanSetServiceMdLevel (pConfig->vlanId,
								pConfig->srcPort,
								MEA_TRUE,
								tOutPorts,pConfig->megLevel);



	if(DPL_MiVlanSetServiceLmId (pConfig->vlanId,
									pConfig->incoming_service_id,
									MEA_TRUE,
									pConfig->lmId,
									MEA_LM_COMMAND_TYPE_COUNT_CCM_RX) != MEA_OK)
	{
		fprintf(stdout,"DPL_MiVlanSetServiceLmId: failed to create forwarder\r\n");
		return MEA_ERROR;
	}

	// set rx
	if(DPL_MiVlanSetServiceLmId (pConfig->vlanId,
									pConfig->outgoing_service_id,
									MEA_TRUE,
									pConfig->lmId,
									MEA_LM_COMMAND_TYPE_COUNT_CCM_TX) != MEA_OK)
	{
		fprintf(stdout,"DPL_MiVlanSetServiceLmId: failed to create forwarder\r\n");
		return MEA_ERROR;
	}
*/
	return MEA_OK;


}

int delete_Ccm_Configuration(tCcmConfiguration *pConfig)
{

	if(delete_Ccm_generator(pConfig) != MEA_OK)
	{
		fprintf(stdout,"delete_Ccm_generator: failed to create packet gen id\n");
		return MEA_ERROR;
	}
	fprintf(stdout,"succeeded to delete_Ccm_generator\n");


	if(delete_Ccm_analyzer(pConfig) != MEA_OK)
	{
		fprintf(stdout,"delete_Ccm_analyzer: failed to create packet gen id\n");
		return MEA_ERROR;
	}
	fprintf(stdout,"succeeded to delete_Ccm_analyzer\n");
	return MEA_OK;
}


int create_Ccm_Configuration(tCcmConfiguration *pConfig)
{

	if(create_Ccm_generator(pConfig) != MEA_OK)
	{
		fprintf(stdout,"create_Ccm_generator: failed to create packet gen id\n");
		return MEA_ERROR;
	}
	fprintf(stdout,"succeeded to create_Ccm_generator\n");


	if(create_Ccm_analyzer(pConfig) != MEA_OK)
	{
		fprintf(stdout,"create_Ccm_analyzer: failed to create packet gen id\n");
		return MEA_ERROR;
	}
	fprintf(stdout,"succeeded to create_Ccm_analyzer\n");
	return MEA_OK;
}


int delete_Lbm_Configuration(tLbConfiguration *pConfig)
{

	// analyzer side

	// delete analyzer forwarder
	FsMiHwDeleteRxForwarder(pConfig->vlanId,pConfig->packetLbm.VpnIndex,pConfig->megLevel,D_LBR_OPCODE_VALUE,pConfig->srcPort);

	// delete analyzer action
	MEA_API_COMM_Delete_Action (MEA_UNIT_0,pConfig->packetLbm.analyzer_action_id);

	// delete analyzer service
	MEA_API_COMM_Delete_Service (MEA_UNIT_0,pConfig->packetLbm.analyzer_Service_id);

	// delete forwarder
	FsMiHwDeleteTxForwarder((char *)&pConfig->destMac.b[0],pConfig->packetLbm.VpnIndex-1);

	// delete generator

	// delete generator action
	MEA_API_COMM_Delete_Action (MEA_UNIT_0,pConfig->packetLbm.action_id);

	// delete generator service
	MEA_API_COMM_Delete_Service (MEA_UNIT_0,pConfig->packetLbm.Service_id);

	// delete packet generator
	FsMiHwPacketGenDeleteiNFO(pConfig->packetLbm.packetGenId);

	// delete generator profile
	 MEA_API_COMM_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,pConfig->packetLbm.packetProfileId);


	fprintf(stdout,"succeeded to delete_Lbm_Configuration\n");
	return MEA_OK;
}

int create_Lbm_Configuration(tLbConfiguration *pConfig)
{
	tPacketGenLbmHeader lbmHeader;
	char srcmacAddr[6];
	MEA_EHP_Info_dbt tEhp_info;
	MEA_OutPorts_Entry_dbt tOutPorts;

	memset(&lbmHeader,0,sizeof(tPacketGenLbmHeader));

	//create packet gen profile
	if(FsMiHwPacketGenProfInfoCreate(30,&pConfig->packetLbm.packetProfileId,1) != MEA_OK)
	{
		fprintf(stdout,"create_Lbm_Configuration: failed to create packet gen id\n");
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwPacketGenProfInfoCreate: succeeded to create packet gen profile\n");

	if(FsMiHwGetInterfaceCfmOamUCMac (pConfig->srcPort,&srcmacAddr[0]) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwGetInterfaceCfmOamUCMac: create packet gen profile failed\n");
		return MEA_ERROR;
	}
	fprintf(stdout,"FsMiHwGetInterfaceCfmOamUCMac: succeeded to get source mac address\n");
	//create packet gen entry
	MEA_OS_memcpy(&lbmHeader.DA.b[0],(char *)&pConfig->destMac.b[0],6);
	MEA_OS_memcpy(&lbmHeader.SA.b[0],srcmacAddr,6);
	lbmHeader.num_of_vlans=1;
	lbmHeader.outerVlan=0x81000000 | pConfig->vlanId | ( (pConfig->u1CcmPriority & 0x7) << 13);
	lbmHeader.innerVlan=0x81000000;
	lbmHeader.cfmEtherType=0x8902;
	lbmHeader.meg_level=pConfig->megLevel; //3 bits
	lbmHeader.version=0; //5 bits
	lbmHeader.opCode=D_LBM_OPCODE_VALUE; // 8 bits
	lbmHeader.type=32;
	lbmHeader.Length=0;
	lbmHeader.Pattern_Type=6; //prbs-2; AA55-6
	lbmHeader.tlvOffset=4; // 8 bits
	lbmHeader.initSeqNum=0;



	if(pConfig->prbs==1)
	{
		lbmHeader.Pattern_Type=2; //prbs-2; AA55-6
	}

	if(FsMiHwPacketGenLbmCreate(&lbmHeader,pConfig->packetLbm.packetProfileId,&pConfig->packetLbm.packetGenId,1,pConfig->prbs) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: failed to create packet gen id\n");
		return MEA_ERROR;
	}
	fprintf(stdout,"FsCreateHwPacketGenerator: succeeded to create packet gen\n");

	//pConfig->packetLbm.VpnIndex = allocate_vpn_entry();
	//create service
	if(FsMiHwCreateCfmService(&pConfig->packetLbm.Service_id,
								pConfig->packetLbm.VpnIndex,
								pConfig->vlanId,
								pConfig->policerId,
								pConfig->lmEnable,
								pConfig->lmId,
								120,
								MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN,
								MEA_FALSE,0) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateCfmService: failed to create service\n");
		return MEA_ERROR;
	}


	fprintf(stdout,"FsMiHwCreateCfmService: succeeded to create service\n");


	memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));


	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = pConfig->vlanId;

/*
	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
*/


	MEA_SET_OUTPORT(&tEhp_info.output_info,pConfig->srcPort);
	MEA_SET_OUTPORT(&tOutPorts, pConfig->srcPort);


	//create action
	if(FsMiHwCreateCfmAction(&pConfig->packetLbm.action_id,&tEhp_info,1,&tOutPorts,pConfig->policerId,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: create action failed\n");
		return MEA_ERROR;
	}

	fprintf(stdout,"FsCreateHwPacketGenerator: succeeded to create action\n");


	if(FsMiHwCreateTxForwarder(pConfig->packetLbm.action_id,(char *)&pConfig->destMac.b[0],pConfig->packetLbm.VpnIndex,&tOutPorts) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: create forwarder failed\n");
		return MEA_ERROR;
	}



	fprintf(stdout,"FsMiHwCreateTxForwarder: succeeded to create entry \n");

	if(FsMiHwPacketGenActive(pConfig->packetLbm.packetGenId,MEA_TRUE) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwPacketGenActive: activate generator failed\n");
		return MEA_ERROR;
	}

	/*******************         packet analyzer ******************************/
	/**************************************************************************/

	pConfig->packetLbm.VpnIndex = allocate_vpn_entry();
	//create service
	if(FsMiHwCreateCfmService(&pConfig->packetLbm.analyzer_Service_id,
								pConfig->packetLbm.VpnIndex,
								pConfig->vlanId,
								pConfig->policerId,
								MEA_FALSE,
								0,
								pConfig->srcPort,
								MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN,
								MEA_TRUE,pConfig->megLevel) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateCfmService: failed to create service\n");
		return MEA_ERROR;
	}


	memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));


	FsMiHwSetGenericAnalyzer(pConfig->packetLbm.analyzerId,0);

	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = pConfig->packetLbm.analyzerId;


	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	MEA_SET_OUTPORT(&tEhp_info.output_info,120);
	MEA_SET_OUTPORT(&tOutPorts, 120);

	//create action
	if(FsMiHwCreateCfmAction(&pConfig->packetLbm.analyzer_action_id,&tEhp_info,1,&tOutPorts,pConfig->policerId,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: create action failed\n");
		return MEA_ERROR;
	}

	if(FsMiHwCreateRxForwarder(pConfig->packetLbm.analyzer_action_id,pConfig->packetLbm.VpnIndex,&tOutPorts,pConfig->megLevel,D_LBR_OPCODE_VALUE,pConfig->vlanId,pConfig->srcPort) != MEA_OK)
	//if(FsMiHwCreateTxForwarder(pConfig->packetLbm.analyzer_action_id,&srcmacAddr[0],pConfig->packetLbm.VpnIndex,&tOutPorts) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateTxForwarder: failed to create forwarder for packet to analyzer\n");
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwPacketGenActive generator:%d succeeded\n",pConfig->packetLbm.packetGenId);

	//create forwarder
	return MEA_OK;

}

int delete_Lbr_Configuration(tLbConfiguration *pConfig)
{
	// delete forwarder
	FsMiHwDeleteRxForwarder(pConfig->vlanId,pConfig->packetLbr.VpnIndex,pConfig->megLevel,D_LBM_OPCODE_VALUE,pConfig->srcPort);

	// delete action
	MEA_API_COMM_Delete_Action (MEA_UNIT_0,pConfig->packetLbr.action_id);

	// delete service
	MEA_API_COMM_Delete_Service (MEA_UNIT_0,pConfig->packetLbr.Service_id);

	return MEA_OK;
}

int create_Lbr_Configuration(tLbConfiguration *pConfig)
{
	MEA_EHP_Info_dbt 	tEhp_info[2];
	int 				num_of_entries=0;
	MEA_OutPorts_Entry_dbt tOutPorts;
	MEA_Uint16 VpnIndex=200;


	memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));


	if( FsMiHwCreateCfmService(&pConfig->packetLbr.Service_id,
									VpnIndex,
									pConfig->vlanId,
									pConfig->policerId,
									MEA_FALSE,
									0,
									pConfig->srcPort,
									MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN,MEA_FALSE,0) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateCfmService: failed to create action\n");
		return MEA_ERROR;
	}

	tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = pConfig->vlanId;

/*
	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
*/
	if(pConfig->lmEnable == MEA_TRUE)
	{
		fprintf(stdout,"create_Lbr_Configuration: lm id:%d\n",pConfig->lmId);
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.valid = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.LmId = 0;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.Command_dasa=1;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_SWAP_OAM;

		tEhp_info[num_of_entries].ehp_data.martini_info.DA.b[5]=0x0b;
		tEhp_info[num_of_entries].ehp_data.martini_info.SA.b[5]=0x0b;

	}

	MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,pConfig->srcPort);
	MEA_SET_OUTPORT(&tOutPorts, pConfig->srcPort);


	num_of_entries++;

	// create action
	// MEA action set create -f 1033 -pm 1 103 -ed 1 0 -h 81000001 0 0 3 -lmid 1 1 0 3
	if(FsMiHwCreateCfmAction(&pConfig->packetLbr.action_id,&tEhp_info[0],num_of_entries,&tOutPorts,pConfig->policerId,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		fprintf(stdout,"FsCreatePacketAnalyzer: failed to create action\n");
		return MEA_ERROR;
	}



	// MEA forwarder add 4 2 102 2 1 0 3 1 0 1 120 -action 1 1033
	if(FsMiHwCreateRxForwarder(pConfig->packetLbr.action_id,
								VpnIndex,
								&tOutPorts,pConfig->megLevel,
								D_LBM_OPCODE_VALUE,
								pConfig->vlanId,
								pConfig->srcPort) != MEA_OK)
	{
		fprintf(stdout,"FsCreatePacketAnalyzer: failed to create forwarder\r\n");
		return MEA_ERROR;
	}

	return MEA_OK;
}

MEA_Status FsMiHwSetGlobalsPacketGen(int generator_enable)
{
	MEA_Globals_Entry_dbt entry;

    if ( MEA_API_COMM_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
    	fprintf(stdout,"MEA_API_Get_Globals_Entry failed\n");
       return MEA_ERROR;
    }
    entry.if_global0.val.generator_enable = generator_enable;

    if (MEA_API_COMM_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
    	fprintf(stdout,"MEA_API_Set_Globals_Entry failed\n");
       return MEA_ERROR;
    }

    return MEA_OK;

}


MEA_Status FsMiHwSetGlobalsPacketAnalyzerEnable(int enable)
{
	MEA_Globals_Entry_dbt entry;

    if (MEA_API_COMM_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
    	fprintf(stdout,"MEA_API_Get_Globals_Entry failed\n");
       return MEA_ERROR;
    }
    entry.PacketAnalyzer.val.enable = enable;

    if (MEA_API_COMM_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
    	fprintf(stdout,"MEA_API_Set_Globals_Entry failed\n");
       return MEA_ERROR;
    }
    return MEA_OK;

}

MEA_Status FsMiHwGlobalsCfmOamValid(int valid)
{
    MEA_Globals_Entry_dbt entry;

    if (MEA_API_COMM_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
    	fprintf(stdout,"MEA_API_Get_Globals_Entry failed\n");
       return MEA_OK;
    }
        entry.CFM_OAM.valid = valid;
    if (MEA_API_COMM_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
    	fprintf(stdout,"MEA_API_Set_Globals_Entry failed\n");
       return MEA_OK;
    }
    return MEA_OK;
}

MEA_Status FsMiHwSetInternalInterface(MEA_Port_t	enetPort,int rx_enable,int crc_check)
{
 	MEA_IngressPort_Entry_dbt entry;

	if (MEA_API_COMM_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwSetInternalInterface: MEA_API_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	entry.rx_enable=rx_enable;
	entry.crc_check=crc_check;


	if (MEA_API_COMM_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwSetInternalInterface: MEA_API_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}
    return (MEA_OK);
}

static MEA_Status FsMiHwSetEgressInterface(MEA_Port_t	enetPort,int tx_enable,int crc_check)
{
	MEA_EgressPort_Entry_dbt entry;


   if (MEA_API_COMM_Get_EgressPort_Entry(MEA_UNIT_0,enetPort,&entry) != MEA_OK)
   {
	   fprintf(stdout,"Error: MEA_API_Get_EgressPort_Entry for port %d failed\n",enetPort);
        return MEA_ERROR;
    }

	entry.tx_enable = tx_enable;
	entry.calc_crc = crc_check;

    if (MEA_API_COMM_Set_EgressPort_Entry(MEA_UNIT_0,enetPort,&entry) != MEA_OK)
    {
    	fprintf(stdout,"Error: MEA_API_Set_EgressPort_Entry for port %d failed\n",enetPort);
        return MEA_ERROR;
    }
	return MEA_OK;
}
void FsMiEcfmHwInit(void)
{

	if(FsMiHwSetGlobalsPacketGen(1) != MEA_OK)
	{
    	fprintf(stdout,"Error: FsMiHwSetGlobalsPacketGen\n");
    	return;
	}
	if(FsMiHwSetGlobalsPacketAnalyzerEnable(1) != MEA_OK)
	{
    	fprintf(stdout,"Error: FsMiHwSetGlobalsPacketAnalyzerEnabl\n");
    	return;
	}
	if(FsMiHwGlobalsCfmOamValid(1) != MEA_OK)
	{
		fprintf(stdout,"Error: FsMiHwGlobalsCfmOamValid\n");
		return;
	}
	if(FsMiHwSetInternalInterface(120,1,0) != MEA_OK)
	{
		fprintf(stdout,"Error: FsMiHwSetInternalInterface for port %d failed\n",120);
		return;
	}
	if(FsMiHwSetEgressInterface(120,1,0) != MEA_OK)
	{
		fprintf(stdout,"Error: FsMiHwSetEgressInterface for port %d failed\n",120);
		return;
	}

}

int delete_Dmm_Configuration(tDmConfiguration *pConfig)
{
	// analyzer side

	// delete analyzer forwarder
	FsMiHwDeleteRxForwarder(pConfig->vlanId,pConfig->packetDmm.VpnIndex,pConfig->megLevel,D_DMR_OPCODE_VALUE,pConfig->srcPort);

	// delete analyzer action
	MEA_API_COMM_Delete_Action (MEA_UNIT_0,pConfig->packetDmm.analyzer_action_id);

	// delete analyzer service
	MEA_API_COMM_Delete_Service (MEA_UNIT_0,pConfig->packetDmm.analyzer_Service_id);

	// delete forwarder
	FsMiHwDeleteTxForwarder((char *)&pConfig->destMac.b[0],pConfig->packetDmm.VpnIndex-1);

	// delete generator

	// delete generator action
	MEA_API_COMM_Delete_Action (MEA_UNIT_0,pConfig->packetDmm.action_id);

	// delete generator service
	MEA_API_COMM_Delete_Service (MEA_UNIT_0,pConfig->packetDmm.Service_id);

	// delete packet generator
	FsMiHwPacketGenDeleteiNFO(pConfig->packetDmm.packetGenId);

	// delete generator profile
	 MEA_API_COMM_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,pConfig->packetDmm.packetProfileId);

	fprintf(stdout,"succeeded to delete_Dmm_Configuration\n");
	return MEA_OK;
}



int create_Dmm_Configuration(tDmConfiguration *pConfig)
{
	tPacketGenDmmHeader dmmHeader;
	char srcmacAddr[6];
	MEA_EHP_Info_dbt tEhp_info;
	MEA_OutPorts_Entry_dbt tOutPorts;

	//create packet gen profile
	if(FsMiHwPacketGenProfInfoCreate(55,&pConfig->packetDmm.packetProfileId,1) != MEA_OK)
	{
		fprintf(stdout,"create_Dmm_Configuration: failed to create packet gen id\n");
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwPacketGenProfInfoCreate: succeeded to create packet gen profile\n");

	if(FsMiHwGetInterfaceCfmOamUCMac (pConfig->srcPort,&srcmacAddr[0]) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwGetInterfaceCfmOamUCMac: create packet gen profile failed\n");
		return MEA_ERROR;
	}
	fprintf(stdout,"FsMiHwGetInterfaceCfmOamUCMac: succeeded to get source mac address\n");
	//create packet gen entry
	MEA_OS_memcpy(&dmmHeader.DA.b[0],(char *)&pConfig->destMac.b[0],6);
	MEA_OS_memcpy(&dmmHeader.SA.b[0],srcmacAddr,6);
	dmmHeader.num_of_vlans=1;
	dmmHeader.outerVlan=0x81000000 | pConfig->vlanId | ( (pConfig->u1CcmPriority & 0x7) << 13);
	dmmHeader.innerVlan=0x81000000;
	dmmHeader.cfmEtherType=0x8902;
	dmmHeader.meg_level=pConfig->megLevel; //3 bits
	dmmHeader.version=0; //5 bits
	dmmHeader.opCode=D_DMM_OPCODE_VALUE; // 8 bits
	dmmHeader.flags=0;
	dmmHeader.tlvOffset=32;

	if(FsMiHwPacketGenDmmCreate(&dmmHeader,pConfig->packetDmm.packetProfileId,&pConfig->packetDmm.packetGenId,1) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: failed to create packet gen id\n");
		return MEA_ERROR;
	}
	fprintf(stdout,"FsCreateHwPacketGenerator: succeeded to create packet gen\n");



	//create service
	if(FsMiHwCreateCfmService(&pConfig->packetDmm.Service_id,
								pConfig->packetDmm.VpnIndex,
								pConfig->vlanId,
								pConfig->policerId,
								pConfig->lmEnable,
								pConfig->lmId,
								120,
								MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN,MEA_FALSE,0) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateCfmService: failed to tx create service policer id:%d\n",pConfig->policerId);
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwCreateCfmService: succeeded to create service\n");


	memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));


	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = pConfig->vlanId;

	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;


	tEhp_info.ehp_data.LmCounterId_info.valid = MEA_TRUE;
	tEhp_info.ehp_data.LmCounterId_info.LmId = pConfig->lmId; // create lmId pConfig->lmId;
	tEhp_info.ehp_data.LmCounterId_info.Command_dasa=0;
	tEhp_info.ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_STAMP_TX;
	//create action

/*
	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
*/


	MEA_SET_OUTPORT(&tEhp_info.output_info,pConfig->srcPort);
	MEA_SET_OUTPORT(&tOutPorts, pConfig->srcPort);


	//create action
	if(FsMiHwCreateCfmAction(&pConfig->packetDmm.action_id,&tEhp_info,1,&tOutPorts,pConfig->policerId,MEA_INGGRESS_TS_TYPE_ENABLE) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: create action failed\n");
		return MEA_ERROR;
	}

	fprintf(stdout,"FsCreateHwPacketGenerator: succeeded to create action\n");


	if(FsMiHwCreateTxForwarder(pConfig->packetDmm.action_id,(char *)&pConfig->destMac.b[0],pConfig->packetDmm.VpnIndex,&tOutPorts) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: create forwarder failed\n");
		return MEA_ERROR;
	}



	fprintf(stdout,"FsMiHwCreateTxForwarder: succeeded to create entry \n");

	if(FsMiHwPacketGenActive(pConfig->packetDmm.packetGenId,MEA_TRUE) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwPacketGenActive: activate generator failed\n");
		return MEA_ERROR;
	}

	/*******************         packet analyzer ******************************/
	/**************************************************************************/

	pConfig->packetDmm.VpnIndex = allocate_vpn_entry();

	//create service
	if(FsMiHwCreateCfmService(&pConfig->packetDmm.analyzer_Service_id,
								pConfig->packetDmm.VpnIndex,
								pConfig->vlanId,
								pConfig->policerId,
								MEA_FALSE,
								0,
								pConfig->srcPort,
								MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN,MEA_TRUE,pConfig->megLevel) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateCfmService: failed to create rx service policerid:%d\n",pConfig->policerId);
		return MEA_ERROR;
	}


	memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));


	FsMiHwSetGenericAnalyzer(pConfig->packetDmm.analyzerId,0);

	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = pConfig->packetDmm.analyzerId;


	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	tEhp_info.ehp_data.LmCounterId_info.valid = MEA_TRUE;
	tEhp_info.ehp_data.LmCounterId_info.LmId = pConfig->lmId; // create lmId pConfig->lmId;
	tEhp_info.ehp_data.LmCounterId_info.Command_dasa=0;
	tEhp_info.ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_STAMP_RX;

	MEA_SET_OUTPORT(&tEhp_info.output_info,120);
	MEA_SET_OUTPORT(&tOutPorts, 120);

	//create action
	if(FsMiHwCreateCfmAction(&pConfig->packetDmm.analyzer_action_id,&tEhp_info,1,&tOutPorts,pConfig->policerId,MEA_INGGRESS_TS_TYPE_ENABLE) != MEA_OK)
	{
		fprintf(stdout,"FsCreateHwPacketGenerator: create action failed\n");
		return MEA_ERROR;
	}

	if(FsMiHwCreateRxForwarder(pConfig->packetDmm.analyzer_action_id,pConfig->packetDmm.VpnIndex,&tOutPorts,pConfig->megLevel,D_DMR_OPCODE_VALUE,pConfig->vlanId,pConfig->srcPort) != MEA_OK)
	//if(FsMiHwCreateTxForwarder(pConfig->packetLbm.analyzer_action_id,&srcmacAddr[0],pConfig->packetLbm.VpnIndex,&tOutPorts) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateTxForwarder: failed to create forwarder for packet to analyzer\n");
		return MEA_ERROR;
	}

	fprintf(stdout,"FsMiHwPacketGenActive generator:%d succeeded\n",pConfig->packetDmm.packetGenId);

	//create forwarder
	return MEA_OK;


}

int delete_Dmr_Configuration(tDmConfiguration *pConfig)
{
	// delete forwarder
	FsMiHwDeleteRxForwarder(pConfig->vlanId,pConfig->packetDmr.VpnIndex,pConfig->megLevel,D_DMM_OPCODE_VALUE,pConfig->srcPort);

	// delete action
	MEA_API_COMM_Delete_Action (MEA_UNIT_0,pConfig->packetDmr.action_id);

	// delete service
	MEA_API_COMM_Delete_Service (MEA_UNIT_0,pConfig->packetDmr.Service_id);

	return MEA_OK;

	return MEA_OK;
}

int create_Dmr_Configuration(tDmConfiguration *pConfig)
{
	MEA_EHP_Info_dbt 	tEhp_info[2];
	int 				num_of_entries=0;
	MEA_OutPorts_Entry_dbt tOutPorts;
	MEA_Uint16 VpnIndex=200;


	memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));


	if( FsMiHwCreateCfmService(&pConfig->packetDmr.Service_id,
									VpnIndex,
									pConfig->vlanId,
									pConfig->policerId,
									MEA_FALSE,
									0,
									pConfig->srcPort,
									MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN,MEA_FALSE,0) != MEA_OK)
	{
		fprintf(stdout,"FsMiHwCreateCfmService: failed to create action\n");
		return MEA_ERROR;
	}

	tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = pConfig->vlanId;

/*
	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
*/
	if(pConfig->lmEnable == MEA_TRUE)
	{
		fprintf(stdout,"create_Lbr_Configuration: lm id:%d\n",pConfig->lmId);
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.valid = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.LmId = 0;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.Command_dasa=1;
		tEhp_info[num_of_entries].ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_SWAP_OAM;

		tEhp_info[num_of_entries].ehp_data.martini_info.DA.b[5]=0x0b;
		tEhp_info[num_of_entries].ehp_data.martini_info.SA.b[5]=0x0b;

	}

	MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,pConfig->srcPort);
	MEA_SET_OUTPORT(&tOutPorts, pConfig->srcPort);


	num_of_entries++;

	// create action
	// MEA action set create -f 1033 -pm 1 103 -ed 1 0 -h 81000001 0 0 3 -lmid 1 1 0 3
	if(FsMiHwCreateCfmAction(&pConfig->packetDmr.action_id,&tEhp_info[0],num_of_entries,&tOutPorts,pConfig->policerId,MEA_INGGRESS_TS_TYPE_ENABLE) != MEA_OK)
	{
		fprintf(stdout,"FsCreatePacketAnalyzer: failed to create action\n");
		return MEA_ERROR;
	}




	// MEA forwarder add 4 2 102 2 1 0 3 1 0 1 120 -action 1 1033
	if(FsMiHwCreateRxForwarder(pConfig->packetDmr.action_id,
								VpnIndex,
								&tOutPorts,pConfig->megLevel,
								D_DMM_OPCODE_VALUE,
								pConfig->vlanId,
								pConfig->srcPort) != MEA_OK)
	{
		fprintf(stdout,"FsCreatePacketAnalyzer: failed to create forwarder\r\n");
		return MEA_ERROR;
	}

	return MEA_OK;
}

