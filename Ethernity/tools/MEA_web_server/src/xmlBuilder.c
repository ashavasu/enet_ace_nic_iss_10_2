#include "stdio.h"
#include "string.h"
#include <sys/types.h>
#include <sys/stat.h>

#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#endif

#include "mea_api.h"
#include "mea_comm_msg.h"
#include "comm_mea_api.h"
#include "mea_client_api.h"




const char *ingress_policer_type[] =
{
	"multicast_type",
	"broadcast_type",
	"unknowncast_type",
	"unknown_type",
	"total_type",
};


/* create xml version <?xml version="1.0" encoding="UTF-8"?> */
int xmlutil_create_xml_version(char *buff,char *version,char *encodeing)
{
	int len=0;

	if( (buff == NULL) || (version == NULL) || (encodeing == NULL) )
		return 0;

	len = sprintf(buff,"<?xml version=\"%s\" encoding=\"%s\"?>\r\n",version,encodeing);

	return len;
}
int xmlutil_create_xml_style(char *buff)
{
	int len=0;

	if( (buff == NULL))
		return 0;


	len = sprintf(buff,"<?xml-stylesheet type=\"%s\" href==\"%s\" ?>\r\n","text/css","olg_rss.css");

	return len;
}
/* <meaAction xmlns="http://www.ethernity-net.com/enet/action"> */
int xmlutil_create_xml_namespace(char *buff,char *rootname,char *httpname)
{
	int len=0;

	if( (buff == NULL) || (rootname == NULL) || (httpname == NULL) )
		return 0;

	len = sprintf(buff,"<%s xmlns=\"%s\">\r\n",rootname,httpname);

	return len;
}
/* <action_Data> */
int xmlutil_open_groupname(char *buff,char *groupname,int num_of_tabs)
{
	int len=0;
	int i=0;

	if( (buff == NULL) || (groupname == NULL) )
		return 0;

	for(i=0;i<num_of_tabs;i++)
	{
		len += sprintf(&buff[len],"  ");
	}
	len += sprintf(&buff[len],"<%s>\r\n",groupname);

	return len;
}
/* </action_Data> */
int xmlutil_close_groupname(char *buff,char *groupname,int num_of_tabs)
{
	int len=0;
	int i=0;

	if( (buff == NULL) || (groupname == NULL) )
		return 0;

	for(i=0;i<num_of_tabs;i++)
	{
		len += sprintf(&buff[len],"  ");
	}
	len += sprintf(&buff[len],"</%s>\r\n",groupname);

	return len;
}
/* <afdx_A_B>0</afdx_A_B> */
int xmlutil_createNode_groupname_int(char *buff,char *nodeName,int num_of_tabs,int value)
{
	int len=0;
	int i=0;

	if( (buff == NULL) || (nodeName == NULL) )
		return 0;

	for(i=0;i<num_of_tabs;i++)
	{
		len += sprintf(&buff[len],"  ");
	}
	len += sprintf(&buff[len],"<%s>%d</%s>\r\n",nodeName,value,nodeName);

	return len;
}


int xmlutil_createNode_groupname_uint(char *buff,char *nodeName,int num_of_tabs,unsigned int value)
{
	int len=0;
	int i=0;

	if( (buff == NULL) || (nodeName == NULL) )
		return 0;

	for(i=0;i<num_of_tabs;i++)
	{
		len += sprintf(&buff[len],"  ");
	}
	len += sprintf(&buff[len],"<%s>%u</%s>\r\n",nodeName,value,nodeName);

	return len;
}


int xmlutil_createNode_groupname_int64(char *buff, char *nodeName, int num_of_tabs, MEA_Counter_t value)
{
    int len = 0;
    int i = 0;

    if ((buff == NULL) || (nodeName == NULL))
        return 0;

    for (i = 0; i < num_of_tabs; i++)
    {
        len += sprintf(&buff[len], "  ");
    }
    len += sprintf(&buff[len], "<%s>%d</%s>\r\n", nodeName, value.s.lsw, nodeName);

    return len;
}

int xmlutil_createNode_groupname_char(char *buff,char *nodeName,int num_of_tabs,char *value)
{
	int len=0;
	int i=0;

	if( (buff == NULL) || (nodeName == NULL) || (value == NULL))
		return 0;

	for(i=0;i<num_of_tabs;i++)
	{
		len += sprintf(&buff[len],"  ");
	}
	len += sprintf(&buff[len],"<%s>%s</%s>\r\n",nodeName,value,nodeName);

	return len;
}

int xmlutil_createNode_groupname_macaddr(char *buff,char *nodeName,int num_of_tabs,MEA_Uint8 *value)
{
	char char_mac[100];

	if( (buff == NULL) || (nodeName == NULL) || (value == NULL))
		return 0;

	sprintf(char_mac,"%2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x",value[0],value[1],value[2],value[3],value[4],value[5]);

	fprintf(stdout,"%s",char_mac);

	return xmlutil_createNode_groupname_char(buff,nodeName,num_of_tabs,char_mac);


}

int xml_write_to_file(FILE **fd,char *buffer,int len)
{
	int num_of_bytes;
	num_of_bytes = fwrite(buffer,1,len,(*fd));

	if(num_of_bytes != len)
		return 1;

	return 0;
}

int file_exist (char *filename)
{
  struct stat   buffer;
  return (stat (filename, &buffer) == 0);
}



int xml_test_build(char *filename,int unit,int action_id)
{
	char arrXml[200];
	int len=0;
	FILE *fd=NULL;

	if (file_exist (filename))
	{
		unlink (filename);
	}

	fd = fopen(filename,"w");

	if(fd == NULL)
		return 0;


	len = xmlutil_create_xml_version(arrXml,"1.0","UTF-8");
	xml_write_to_file(&fd,arrXml,len);

	len = xmlutil_create_xml_namespace(arrXml,"meaAction","http://www.ethernity-net.com/enet/action");
	xml_write_to_file(&fd,arrXml,len);
	{
		len = xmlutil_open_groupname(arrXml,"action_Data",1);
		xml_write_to_file(&fd,arrXml,len);
		{
			len = xmlutil_createNode_groupname_int(arrXml,"unit",2,unit);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"action_id",2,action_id);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"afdx_A_B",2,12);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"afdx_Bag_info",2,12);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"afdx_Rx_sessionId",2,12);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"afdx_Rx_sessionId",2,12);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"COLOR",2,12);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"COS",2,12);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"direction",2,12);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ed_id_valid",2,12);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"flowCoSMappingProfile_force",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"flowCoSMappingProfile_id",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"flowMarkingMappingProfile_force",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"flowMarkingMappingProfile_id",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"force_color_valid",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"force_cos_valid",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"force_l2_pri_valid",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"fwd_ingress_TS_type",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"l2_PRI",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"llc",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"lxcp_win",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"opposite_direction_actionId_valid",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"output_ports_valid",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"pad2",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"pad3",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"pm_id_valid",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"pm_type",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"policer_prof_id_valid",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"proto_llc_valid",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"protocol",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"protocol_llc_force",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"set_1588",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"tdm_cesId",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"tdm_ces_type",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"tdm_packet_type",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"tdm_remove_byte",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"tdm_used_rtp",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"tdm_used_vlan",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"tmId_disable",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"tm_id_valid",2,0);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"wred_profId",2,0);
			xml_write_to_file(&fd,arrXml,len);

			}

		len = xmlutil_close_groupname(arrXml,"action_Data",1);
		xml_write_to_file(&fd,arrXml,len);
	}
	len = xmlutil_close_groupname(arrXml,"meaAction",0);
	xml_write_to_file(&fd,arrXml,len);

	fclose(fd);
	return 0;
}
int buildXml_serviceId_filter_Params(char *file_name,int unit,int service, FilterDataStr	*pStrFilterData)
{
	char arrXml[200];
	int len=0;
	FILE *fd=NULL;
	//MEA_Uint32 i=0;
	int xml_level=1;

	if (file_exist (file_name))
	{
		unlink (file_name);
	}

	fd = fopen(file_name,"w");

	if(fd == NULL)
		return 0;

	len = xmlutil_create_xml_version(arrXml,"1.0","UTF-8");
	xml_write_to_file(&fd,arrXml,len);

	len = xmlutil_create_xml_namespace(arrXml,"meaGetServiceFilterId","http://www.ethernity-net.com/enet/ServiceFilterParams");
	xml_write_to_file(&fd,arrXml,len);
	{
		len = xmlutil_open_groupname(arrXml,"serviceFilter",xml_level++);
		xml_write_to_file(&fd,arrXml,len);
		{
			len = xmlutil_createNode_groupname_int(arrXml,"unit",xml_level,unit);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"serviceId",xml_level,service);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_char(arrXml,"destMac",xml_level,pStrFilterData->destMac);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_char(arrXml,"srcMac",xml_level,pStrFilterData->srcMac);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_char(arrXml,"srcIP",xml_level,pStrFilterData->srcIP);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_char(arrXml,"dstIP",xml_level,pStrFilterData->dstIP);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_char(arrXml,"ipProto",xml_level,pStrFilterData->ipProto);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_char(arrXml,"srcPort",xml_level,pStrFilterData->srcPort);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_char(arrXml,"dstPort",xml_level,pStrFilterData->dstPort);
			xml_write_to_file(&fd,arrXml,len);
		}
		len = xmlutil_close_groupname(arrXml,"serviceFilter",--xml_level);
		xml_write_to_file(&fd,arrXml,len);
	}
	len = xmlutil_close_groupname(arrXml,"meaGetServiceFilterId",--xml_level);
	xml_write_to_file(&fd,arrXml,len);
	fclose(fd);
	return 0;
}
int buildXml_serviceId_vlan_Params(tServiceVlanParams *pServiceEntry,char *file_name,int unit,int service)
{
	char arrXml[200];
	int len=0;
	FILE *fd=NULL;
	MEA_Uint32 i=0;
	int xml_level=1;

	if (file_exist (file_name))
	{
		unlink (file_name);
	}

	fd = fopen(file_name,"w");

	if(fd == NULL)
		return 0;

	len = xmlutil_create_xml_version(arrXml,"1.0","UTF-8");
	xml_write_to_file(&fd,arrXml,len);

	len = xmlutil_create_xml_namespace(arrXml,"meaGetServiceVlanId","http://www.ethernity-net.com/enet/ServiceParams");
	xml_write_to_file(&fd,arrXml,len);
	{

		len = xmlutil_open_groupname(arrXml,"serviceDataBase",xml_level++);
		xml_write_to_file(&fd,arrXml,len);
		{
			len = xmlutil_createNode_groupname_int(arrXml,"unit",xml_level,unit);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"serviceId",xml_level,service);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"policerId",xml_level,pServiceEntry->policerId);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"pmId",xml_level,pServiceEntry->pmId);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"eIngressType",xml_level,pServiceEntry->eIngressType);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"outer_vlanId",xml_level,pServiceEntry->outer_vlanId);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"inner_vlanId",xml_level,pServiceEntry->inner_vlanId);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"src_port",xml_level,pServiceEntry->srcPort);
			xml_write_to_file(&fd,arrXml,len);


			len = xmlutil_open_groupname(arrXml,"serviceOutputPort",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				for(i=0;i<pServiceEntry->num_of_clusters;i++)
				{
					if(MEA_IS_SET_OUTPORT(&pServiceEntry->outputPort,i) != MEA_FALSE)
					{
						len = xmlutil_open_groupname(arrXml,"cluster",xml_level++);
						xml_write_to_file(&fd,arrXml,len);
						{
							len = xmlutil_createNode_groupname_int(arrXml,"clusterId",xml_level,i);
							xml_write_to_file(&fd,arrXml,len);
						}
						len = xmlutil_close_groupname(arrXml,"cluster",--xml_level);
						xml_write_to_file(&fd,arrXml,len);
					}
				}
			}
			len = xmlutil_close_groupname(arrXml,"serviceOutputPort",--xml_level);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"vlanEdit_flowtype",xml_level,pServiceEntry->vlanEdit.flow_type);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"vlanEdit_outer_command",xml_level,pServiceEntry->vlanEdit.outer_edit_command);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"vlanEdit_outer_vlan",xml_level,pServiceEntry->vlanEdit.outer_vlan);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"vlanEdit_inner_command",xml_level,pServiceEntry->vlanEdit.inner_edit_command);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"vlanEdit_inner_vlan",xml_level,pServiceEntry->vlanEdit.inner_vlan);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"filter_enable",xml_level,pServiceEntry->filter_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"filter_mask_0_31",xml_level,pServiceEntry->filter_mask[0]);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"filter_mask_32_63",xml_level,pServiceEntry->filter_mask[1]);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"filter_id",xml_level,pServiceEntry->filter_id);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"pri_aware_enable",xml_level,pServiceEntry->priority_aware);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"pri_aware_id",xml_level,pServiceEntry->filter_priority);
			xml_write_to_file(&fd,arrXml,len);
		}
		len = xmlutil_close_groupname(arrXml,"serviceDataBase",--xml_level);
		xml_write_to_file(&fd,arrXml,len);
	}
	len = xmlutil_close_groupname(arrXml,"meaGetServiceVlanId",--xml_level);
	xml_write_to_file(&fd,arrXml,len);
	fclose(fd);
	return 0;
}

int buildXml_serviceId_Params(tComService *pServiceEntry,char *file_name)
{
	char arrXml[200];
	int len=0;
	FILE *fd=NULL;
	MEA_Uint32 i=0;
    MEA_Uint32 j = 0;
	int xml_level=1;


	if (file_exist (file_name))
	{
		unlink (file_name);
	}

	fd = fopen(file_name,"w");

	if(fd == NULL)
		return 0;

	len = xmlutil_create_xml_version(arrXml,"1.0","UTF-8");
	xml_write_to_file(&fd,arrXml,len);

	len = xmlutil_create_xml_namespace(arrXml,"meaGetServiceId","http://www.ethernity-net.com/enet/ServiceParams");
	xml_write_to_file(&fd,arrXml,len);
	{
		len = xmlutil_open_groupname(arrXml,"serviceDataBase",xml_level++);
		xml_write_to_file(&fd,arrXml,len);
		{
			len = xmlutil_createNode_groupname_int(arrXml,"unit",2,pServiceEntry->unit);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"serviceId",2,pServiceEntry->serviceId);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"policerId",2,pServiceEntry->policer_id);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_open_groupname(arrXml,"serviceData",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				len = xmlutil_createNode_groupname_int(arrXml,"frwdEnaData",xml_level,pServiceEntry->data.DSE_forwarding_enable);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"frwdKeyTypeData",xml_level,pServiceEntry->data.DSE_forwarding_key_type);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"learnEnaData",xml_level,pServiceEntry->data.DSE_learning_enable);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"learnKeyTypeData",xml_level,pServiceEntry->data.DSE_learning_key_type);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"learnActData",xml_level,pServiceEntry->data.DSE_learning_actionId);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"learnActValidData",xml_level,pServiceEntry->data.DSE_learning_actionId_valid);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"learnSrcPortData",xml_level,pServiceEntry->data.DSE_learning_srcPort);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"learnSrcForceData",xml_level,pServiceEntry->data.DSE_learning_srcPort_force);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"tmIdData",xml_level,pServiceEntry->data.tmId);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"tmDisData",xml_level,pServiceEntry->data.tmId_disable);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"pmIdData",xml_level,pServiceEntry->data.pmId);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"editIdData",xml_level,pServiceEntry->data.editId);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"admEnaData",xml_level,pServiceEntry->data.ADM_ENA);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"cmData",xml_level,pServiceEntry->data.CM);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"l2PriForceData",xml_level,pServiceEntry->data.L2_PRI_FORCE);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"priData",xml_level,pServiceEntry->data.L2_PRI);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"colorForceData",xml_level,pServiceEntry->data.COLOR_FORSE);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"colorData",xml_level,pServiceEntry->data.COLOR);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"cosForceData",xml_level,pServiceEntry->data.COS_FORCE);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"cosData",xml_level,pServiceEntry->data.COS);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"proLlcForceData",xml_level,pServiceEntry->data.protocol_llc_force);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"protoData",xml_level,pServiceEntry->data.Protocol);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"llcData",xml_level,pServiceEntry->data.Llc);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"markProForceEnaData",xml_level,pServiceEntry->data.flowMarkingMappingProfile_force);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"markProIdData",xml_level,pServiceEntry->data.flowMarkingMappingProfile_id);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"lxcpEnaData",xml_level,pServiceEntry->data.LxCp_enable);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"lxcpIdData",xml_level,pServiceEntry->data.LxCp_Id) ;
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"policerIdData",xml_level,pServiceEntry->data.policer_prof_id);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"cosProfForceEnaData",xml_level,pServiceEntry->data.flowCoSMappingProfile_force);
				xml_write_to_file(&fd,arrXml,len);
				len = xmlutil_createNode_groupname_int(arrXml,"cosProfForceIdData",xml_level,pServiceEntry->data.flowCoSMappingProfile_id);
				xml_write_to_file(&fd,arrXml,len);
			}
			len = xmlutil_close_groupname(arrXml,"serviceData",--xml_level);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_open_groupname(arrXml,"serviceOutputPort",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				for(i=0;i<pServiceEntry->num_of_clusters;i++)
				{
					if(MEA_IS_SET_OUTPORT(&pServiceEntry->OutPorts_Entry,i) != MEA_FALSE)
					{
						len = xmlutil_open_groupname(arrXml,"cluster",xml_level++);
						xml_write_to_file(&fd,arrXml,len);
						{
							len = xmlutil_createNode_groupname_int(arrXml,"clusterId",xml_level,i);
							xml_write_to_file(&fd,arrXml,len);
						}
						len = xmlutil_close_groupname(arrXml,"cluster",--xml_level);
						xml_write_to_file(&fd,arrXml,len);
					}
				}
			}
			len = xmlutil_close_groupname(arrXml,"serviceOutputPort",--xml_level);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_open_groupname(arrXml,"serviceKey",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				len = xmlutil_createNode_groupname_int(arrXml,"sourcePortKey",xml_level,pServiceEntry->key.src_port);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"netTagKey",xml_level,pServiceEntry->key.net_tag);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"l2ProtoTypeKey",xml_level,pServiceEntry->key.L2_protocol_type);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"evcKey",xml_level,pServiceEntry->key.evc_enable);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"priKey",xml_level,pServiceEntry->key.pri);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"netTagInnerValidKey",xml_level,pServiceEntry->key.inner_netTag_from_valid);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"netTagInnerValueKey",xml_level,pServiceEntry->key.inner_netTag_from_value);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"intExtKey",xml_level,pServiceEntry->key.external_internal);
				xml_write_to_file(&fd,arrXml,len);

			}

			len = xmlutil_close_groupname(arrXml,"serviceKey",--xml_level);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_open_groupname(arrXml,"serviceEditing",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				for(i=0;i<pServiceEntry->num_of_entries;i++)
				{
					len = xmlutil_open_groupname(arrXml,"editingParams",xml_level++);
					xml_write_to_file(&fd,arrXml,len);
					{
						len = xmlutil_createNode_groupname_int(arrXml,"serviceEditingId",xml_level,i);
						xml_write_to_file(&fd,arrXml,len);

						len = xmlutil_createNode_groupname_int(arrXml,"flowTypeedit",xml_level,pServiceEntry->ehp_info[i].ehp_data.wbrg_info.val.data_wbrg.flow_type);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"doubleTagCmdEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.atm_info.double_tag_cmd);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"doubleTagEthTypeEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.atm_info.val.double_vlan_tag.eth_type);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"doubleTagVlanIdEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.atm_info.val.double_vlan_tag.vid);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"tagCmdEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_info.command);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"tagEthTypeEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_info.val.vlan.eth_type);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"tagVlanIdEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_info.val.vlan.vid);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"stampColorEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_info.stamp_color);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"stampPriEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_info.stamp_priority);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"colorBit0ValidEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.color_bit0_valid);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"colorBit0locEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.color_bit0_loc);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"colorBit1ValidEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.color_bit1_valid);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"colorBit1locEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.color_bit1_loc);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"priBit0ValidEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.pri_bit0_valid);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"priBit0LocEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.pri_bit0_loc);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"priBit1ValidEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.pri_bit1_valid);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"priBit1LocEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.pri_bit1_loc);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"priBit2ValidEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.pri_bit2_valid);
						xml_write_to_file(&fd,arrXml,len);
						len = xmlutil_createNode_groupname_int(arrXml,"priBit2LocEdit",xml_level,pServiceEntry->ehp_info[i].ehp_data.eth_stamping_info.pri_bit2_loc);
						xml_write_to_file(&fd,arrXml,len);

						for(j=0;j<pServiceEntry->num_of_clusters;j++)
						{
							if(MEA_IS_SET_OUTPORT(&pServiceEntry->ehp_info[i].output_info,j) != MEA_FALSE)
							{
								len = xmlutil_open_groupname(arrXml,"cluster",xml_level++);
								xml_write_to_file(&fd,arrXml,len);
								{
									len = xmlutil_createNode_groupname_int(arrXml,"clusterId",xml_level,j);
									xml_write_to_file(&fd,arrXml,len);
								}
								len = xmlutil_close_groupname(arrXml,"cluster",--xml_level);
								xml_write_to_file(&fd,arrXml,len);
							}
						}
					}
					len = xmlutil_close_groupname(arrXml,"editingParams",--xml_level);
					xml_write_to_file(&fd,arrXml,len);
				}
			}
			len = xmlutil_close_groupname(arrXml,"serviceEditing",--xml_level);
			xml_write_to_file(&fd,arrXml,len);
		}
		len = xmlutil_close_groupname(arrXml,"serviceDataBase",--xml_level);
		xml_write_to_file(&fd,arrXml,len);
	}
	len = xmlutil_close_groupname(arrXml,"meaGetServiceId",--xml_level);
	xml_write_to_file(&fd,arrXml,len);
	fclose(fd);
	return 0;
}
int buildXml_service_id(MEA_Service_t  *pServiceIdArry,MEA_Uint32 number_of_services,int unit,char *file_name)
{
	char arrXml[200];
	int len=0;
	FILE *fd=NULL;
	MEA_Uint32 i=0;
	int xml_level=1;

	if (file_exist (file_name))
	{
		unlink (file_name);
	}

	fd = fopen(file_name,"w");

	if(fd == NULL)
		return 0;

	len = xmlutil_create_xml_version(arrXml,"1.0","UTF-8");
	xml_write_to_file(&fd,arrXml,len);

	len = xmlutil_create_xml_namespace(arrXml,"meaServiceMap","http://www.ethernity-net.com/enet/ServiceMapping");
	xml_write_to_file(&fd,arrXml,len);
	{
		for(i=0; i< (MEA_Uint32)number_of_services;i++)
		{
			len = xmlutil_open_groupname(arrXml,"ServiceMapping",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				len = xmlutil_createNode_groupname_int(arrXml,"unit",xml_level,unit);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"serviceId",xml_level,pServiceIdArry[i]);
				xml_write_to_file(&fd,arrXml,len);
			}
			len = xmlutil_close_groupname(arrXml,"ServiceMapping",--xml_level);
			xml_write_to_file(&fd,arrXml,len);
		}
	}
	len = xmlutil_close_groupname(arrXml,"meaServiceMap",--xml_level);
	xml_write_to_file(&fd,arrXml,len);
	fclose(fd);
	return 0;
}


int buildXml_getPolicerProfile_xml(MEA_Policer_Entry_dbt *pEntry, char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
//    int i = 0;
    int xml_level = 1;
    MEA_Counter_t value;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml, "meaPolicerProfile", "http://www.ethernity-net.com/enet/PolicerProfile");
    xml_write_to_file(&fd, arrXml, len);
    {
        len = xmlutil_open_groupname(arrXml, "PolicerData", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        {
            len = xmlutil_createNode_groupname_int(arrXml, "Type", xml_level, pEntry->type_mode);
            xml_write_to_file(&fd, arrXml, len);
            value.val=pEntry->CIR;
            len = xmlutil_createNode_groupname_int64(arrXml, "CIR", xml_level, value);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "CBS", xml_level, pEntry->CBS);
            xml_write_to_file(&fd, arrXml, len);
            value.val=pEntry->EIR;
            len = xmlutil_createNode_groupname_int64(arrXml, "EIR", xml_level, value);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "EBS", xml_level, pEntry->EBS);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int(arrXml, "gn_sign", xml_level, pEntry->gn_sign);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int(arrXml, "gn_type", xml_level, pEntry->gn_type);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int(arrXml, "comp", xml_level, pEntry->comp);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int(arrXml, "Color", xml_level, pEntry->color_aware);
            xml_write_to_file(&fd, arrXml, len);
        }
        len = xmlutil_close_groupname(arrXml, "PolicerData", --xml_level);
        xml_write_to_file(&fd, arrXml, len);

    }
    len = xmlutil_close_groupname(arrXml, "meaPolicerProfile", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;

}


int buildXml_policer_profiles_id(MEA_Uint16 *Profles,MEA_Uint32 numOfProfiles,int unit,char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    MEA_Uint32 i = 0;
    int xml_level = 1;

	if (file_exist (file_name))
	{
		unlink (file_name);
	}

	fd = fopen(file_name,"w");

	if(fd == NULL)
		return 0;

	len = xmlutil_create_xml_version(arrXml,"1.0","UTF-8");
	xml_write_to_file(&fd,arrXml,len);

	len = xmlutil_create_xml_namespace(arrXml,"meaPolicerAllProfiles","http://www.ethernity-net.com/enet/PolicerAllProfiles");
	xml_write_to_file(&fd,arrXml,len);
	{
		for(i=0;i< (MEA_Uint32)numOfProfiles;i++)
		{
			len = xmlutil_open_groupname(arrXml,"policerProfId",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				len = xmlutil_createNode_groupname_int(arrXml,"unit",xml_level,unit);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"profileId",xml_level,Profles[i]);
				xml_write_to_file(&fd,arrXml,len);
			}
			len = xmlutil_close_groupname(arrXml,"policerProfId",--xml_level);
			xml_write_to_file(&fd,arrXml,len);
		}
	}
	len = xmlutil_close_groupname(arrXml,"meaPolicerAllProfiles",--xml_level);
	xml_write_to_file(&fd,arrXml,len);
	fclose(fd);
	return 0;
}

int buildXml_ingrss_port_mapping(int *pPortArry ,int MaxNumberOfPorts,int unit,char *file_name)
{
	char arrXml[200];
	int len=0;
	FILE *fd=NULL;
	MEA_Uint32 i=0;
	int xml_level=1;

	if (file_exist (file_name))
	{
		unlink (file_name);
	}

	fd = fopen(file_name,"w");

	if(fd == NULL)
		return 0;


	len = xmlutil_create_xml_version(arrXml,"1.0","UTF-8");
	xml_write_to_file(&fd,arrXml,len);

	len = xmlutil_create_xml_namespace(arrXml,"meaPortMapping","http://www.ethernity-net.com/enet/igressPortMapping");
	xml_write_to_file(&fd,arrXml,len);
	{
		for(i=0;i<(MEA_Uint32)MaxNumberOfPorts;i++)
		{
			len = xmlutil_open_groupname(arrXml,"portMapping",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				len = xmlutil_createNode_groupname_int(arrXml,"unit",xml_level,unit);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"port",xml_level,pPortArry[i]);
				xml_write_to_file(&fd,arrXml,len);
			}
			len = xmlutil_close_groupname(arrXml,"portMapping",--xml_level);
			xml_write_to_file(&fd,arrXml,len);
		}
	}
	len = xmlutil_close_groupname(arrXml,"meaPortMapping",--xml_level);
	xml_write_to_file(&fd,arrXml,len);
	fclose(fd);
	return 0;
}

int buildXml_ingrss_port_info(int unit,int port_id,MEA_IngressPort_Entry_dbt *pIgressPortInfo,char *file_name)
{
	char arrXml[200];
	int len=0;
	FILE *fd=NULL;
//	int i=0;
	int xml_level=1;

	if (file_exist (file_name))
	{
		unlink (file_name);
	}

	fd = fopen(file_name,"w");

	if(fd == NULL)
		return 0;

	len = xmlutil_create_xml_version(arrXml,"1.0","UTF-8");
	xml_write_to_file(&fd,arrXml,len);

	len = xmlutil_create_xml_namespace(arrXml,"meaIngressPort","http://www.ethernity-net.com/enet/igressPort");
	xml_write_to_file(&fd,arrXml,len);
	{
		len = xmlutil_open_groupname(arrXml,"ingress_Data",xml_level++);
		xml_write_to_file(&fd,arrXml,len);
		{
			len = xmlutil_createNode_groupname_int(arrXml,"unit",xml_level,unit);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"port_id",xml_level,port_id);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"rx_enable",xml_level,pIgressPortInfo->rx_enable);
			xml_write_to_file(&fd,arrXml,len);
#if 0
			len = xmlutil_createNode_groupname_int(arrXml,"utopia",xml_level,pIgressPortInfo->utopia);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"mtu",xml_level,pIgressPortInfo->.MTU);
			xml_write_to_file(&fd,arrXml,len);
#endif

			len = xmlutil_createNode_groupname_int(arrXml,"crc_check",xml_level,pIgressPortInfo->crc_check);
			xml_write_to_file(&fd,arrXml,len);
#if 0
			len = xmlutil_createNode_groupname_int(arrXml,"gigaDisable",xml_level,pIgressPortInfo->.GigaDisable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"user_parser",xml_level,pIgressPortInfo->userParser);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"autoneg_enable",xml_level,pIgressPortInfo->autoneg_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ing_hw_switch_protection",xml_level,pIgressPortInfo->ing_hw_switch_protection);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ingress_fifo_Water_mark_low_thresh",xml_level,pIgressPortInfo->ingress_fifo.val.Water_mark_low_thresh);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ingress_fifo_Water_mark_high_thresh",xml_level,pIgressPortInfo->ingress_fifo.val.Water_mark_high_thresh);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ingress_fifo_ingress_mac_thersh",xml_level,pIgressPortInfo->ingress_fifo.val.ingress_mac_thersh);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ingress_fifo_working_mode",xml_level,pIgressPortInfo->ingress_fifo.val.working_mode);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ingress_fifo_fragment",xml_level,pIgressPortInfo->ingress_fifo.val.fragment);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ingress_fifo_pause_enable",xml_level,pIgressPortInfo->ingress_fifo.val.pause_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"bonding_group_valid",xml_level,pIgressPortInfo->bonding_group.val.valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"bonding_groupgroupId",xml_level,pIgressPortInfo->bonding_group.val.groupId);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_port_proto_prof",xml_level,pIgressPortInfo->parser_info.port_proto_prof);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_port_ip_aw",xml_level,pIgressPortInfo->parser_info.port_ip_aw);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_port_ip_aw",xml_level,pIgressPortInfo->pparser_info.port_ip_aw);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_port_cos_aw",xml_level,pIgressPortInfo->parser_info.port_cos_aw);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_port_col_aw",xml_level,pIgressPortInfo->parser_info.port_col_aw);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_trap_discard_2_cpu",xml_level,pIgressPortInfo->parser_info.trap_discard_2_cpu);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_net_wildcard_valid",xml_level,pIgressPortInfo->parser_info.net_wildcard_valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_net_wildcard",xml_level,pIgressPortInfo->parser_info.net_wildcard);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_pri_wildcard_valid",xml_level,pIgressPortInfo->parser_info.pri_wildcard_valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_pri_wildcard",xml_level,pIgressPortInfo->parser_info.pri_wildcard);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_sp_wildcard_valid",xml_level,pIgressPortInfo->parser_info.sp_wildcard_valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_sp_wildcard",xml_level,pIgressPortInfo->parser_info.sp_wildcard);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_fwd_only_DA_valid",xml_level,pIgressPortInfo->parser_info.fwd_only_DA_valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_fwd_only_DA_offset",xml_level,pIgressPortInfo->parser_info.fwd_only_DA_offset);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_macaddr(arrXml,"parser_info_fwd_only_DA_value",xml_level,pIgressPortInfo->parser_info.fwd_only_DA_value.b);
			xml_write_to_file(&fd,arrXml,len);
#endif
			len = xmlutil_createNode_groupname_macaddr(arrXml,"parser_info_my_Mac",xml_level,pIgressPortInfo->parser_info.my_Mac.b);
			xml_write_to_file(&fd,arrXml,len);
#if 0
			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_cfm_oam_untag",xml_level,pIgressPortInfo->parser_info.cfm_oam_untag);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_cfm_oam_tag",xml_level,pIgressPortInfo->parser_info.cfm_oam_tag);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_cfm_oam_qtag",xml_level,pIgressPortInfo->parser_info.cfm_oam_qtag);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_network_OAM_L2CP_Suffix",xml_level,pIgressPortInfo->parser_info.network_OAM_L2CP_Suffix);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_network_OAM_Location",xml_level,pIgressPortInfo->parser_info.network_OAM_Location);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_network_OAM_EthType_value",xml_level,pIgressPortInfo->parser_info.network_OAM_EthType_value);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_network_L2CP_action_valid",xml_level,pIgressPortInfo->parser_info.network_L2CP_action_valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_network_L2CP_action_table",xml_level,pIgressPortInfo->parser_info.network_L2CP_action_table.f.Act_MacSuffix_0_15);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_Act_MacSuffix_0_15",xml_level,pIgressPortInfo->parser_info.network_L2CP_action_table.f.Act_MacSuffix_0_15);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_Act_MacSuffix_16_31",xml_level,pIgressPortInfo->parser_info.network_L2CP_action_table.f.Act_MacSuffix_16_31);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_Act_MacSuffix_32_47",xml_level,pIgressPortInfo->parser_info.network_L2CP_action_table.f.Act_MacSuffix_32_47);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_Act_MacSuffix_48_63",xml_level,pIgressPortInfo->parser_info.network_L2CP_action_table.f.Act_MacSuffix_48_63);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_default_sid_info_valid",xml_level,pIgressPortInfo->parser_info.default_sid_info.valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_default_sid_info_def_sid",xml_level,pIgressPortInfo->parser_info.default_sid_info.def_sid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_default_sid_info_action",xml_level,pIgressPortInfo->parser_info.default_sid_info.action);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_default_pri",xml_level,pIgressPortInfo->parser_info.default_pri);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_afdx_enable",xml_level,pIgressPortInfo->parser_info.afdx_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_default_net_tag_value",xml_level,pIgressPortInfo->parser_info.default_net_tag_value);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_allowed_tagged",xml_level,pIgressPortInfo->parser_info.allowed_tagged);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_allowed_untagged",xml_level,pIgressPortInfo->parser_info.allowed_untagged);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_DSA_Type_enable",xml_level,pIgressPortInfo->parser_info.DSA_Type_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_L4_SRC_DST",xml_level,pIgressPortInfo->parser_info.L4_SRC_DST);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_IP_pri_mask_valid",xml_level,pIgressPortInfo->parser_info.IP_pri_mask_valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_IP_pri_mask_IPv4",xml_level,pIgressPortInfo->parser_info.IP_pri_mask_IPv4);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_IP_pri_mask_IPv6",xml_level,pIgressPortInfo->parser_info.IP_pri_mask_IPv6);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_evc_external_internal",xml_level,pIgressPortInfo->parser_info.evc_external_internal);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_def_sid_port_type",xml_level,pIgressPortInfo->parser_info.def_sid_port_type);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_LAG_src_port_value",xml_level,pIgressPortInfo->parser_info.LAG_src_port_value);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_LAG_src_port_valid",xml_level,pIgressPortInfo->parser_info.LAG_src_port_valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_mapping_enable",xml_level,pIgressPortInfo->parser_info.mapping_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_mapping_profile",xml_level,pIgressPortInfo->parser_info.mapping_profile);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_filter_key_interface_type",xml_level,pIgressPortInfo->parser_info.filter_key_interface_type);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_ENNI_enable",xml_level,pIgressPortInfo->parser_info.ENNI_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_vlanrange_prof_Id",xml_level,pIgressPortInfo->parser_info.vlanrange_prof_Id);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_vlanrange_enable",xml_level,pIgressPortInfo->parser_info.vlanrange_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_lag_distribution_mask",xml_level,pIgressPortInfo->parser_info.lag_distribution_mask);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_net2_wildcard_valid",xml_level,pIgressPortInfo->parser_info.net2_wildcard_valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_net2_wildcard",xml_level,pIgressPortInfo->parser_info.net2_wildcard);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_l2_protocol_valid",xml_level,pIgressPortInfo->parser_info.l2_protocol_valid);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"parser_info_l2_protocol_wildcard",xml_level,pIgressPortInfo->parser_info.l2_protocol_wildcard);
			xml_write_to_file(&fd,arrXml,len);

			for(i=0;i<MEA_INGRESS_PORT_POLICER_TYPE_LAST;i++)
			{
				len = xmlutil_open_groupname(arrXml,"policer_vec",xml_level++);
				xml_write_to_file(&fd,arrXml,len);
				{
					len = xmlutil_createNode_groupname_char(arrXml,"vec_type",xml_level,(char *)ingress_policer_type[i]);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"tmId_enable",xml_level,pIgressPortInfo->policer_vec[i].tmId_enable);
					xml_write_to_file(&fd,arrXml,len);


					len = xmlutil_createNode_groupname_int(arrXml,"tmId",xml_level,pIgressPortInfo->policer_vec[i].tmId);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"sla_params_CBS",xml_level,pIgressPortInfo->policer_vec[i].sla_params.CBS);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"sla_params_CIR",xml_level,pIgressPortInfo->policer_vec[i].sla_params.CIR);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"sla_params_EBS",xml_level,pIgressPortInfo->policer_vec[i].sla_params.EBS);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"sla_params_EIR",xml_level,pIgressPortInfo->policer_vec[i].sla_params.EIR);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"sla_params_color_aware",xml_level,pIgressPortInfo->policer_vec[i].sla_params.color_aware);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"sla_params_comp",xml_level,pIgressPortInfo->policer_vec[i].sla_params.comp);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"sla_params_cup",xml_level,pIgressPortInfo->policer_vec[i].sla_params.cup);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"gn_sign",xml_level,pIgressPortInfo->policer_vec[i].sla_params.gn_sign);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"gn_type",xml_level,pIgressPortInfo->policer_vec[i].sla_params.gn_type);
					xml_write_to_file(&fd,arrXml,len);

					len = xmlutil_createNode_groupname_int(arrXml,"gn_type",xml_level,pIgressPortInfo->policer_vec[i].sla_params.type_mode);
					xml_write_to_file(&fd,arrXml,len);
				}
				len = xmlutil_close_groupname(arrXml,"policer_vec",--xml_level);
				xml_write_to_file(&fd,arrXml,len);
			}
			len = xmlutil_createNode_groupname_int(arrXml,"ts_cpu_offset_time_stampeL",xml_level,pIgressPortInfo->ts_cpu_offset.time_stampeL);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ts_cpu_offset_add_sub",xml_level,pIgressPortInfo->ts_cpu_offset.add_sub);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ts_cpu_offset_time_stampeM",xml_level,pIgressPortInfo->ts_cpu_offset.time_stampeM);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ProtectType",xml_level,pIgressPortInfo->ProtectType);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"Protect_1p1_info_enable",xml_level,pIgressPortInfo->Protect_1p1_info.enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"Protect_1p1_info_master_secondary",xml_level,pIgressPortInfo->Protect_1p1_info.master_secondary);
			xml_write_to_file(&fd,arrXml,len);
#endif
		}
		len = xmlutil_close_groupname(arrXml,"ingress_Data",--xml_level);
		xml_write_to_file(&fd,arrXml,len);
	}
	len = xmlutil_close_groupname(arrXml,"meaIngressPort",--xml_level);
	xml_write_to_file(&fd,arrXml,len);

	fclose(fd);
	return 0;
}
int buildXml_egress_port_info(MEA_EgressPort_Entry_dbt *pEntry,int unit,int portId,char *file_name)
{
	char arrXml[200];
	int len=0;
	FILE *fd=NULL;
	int xml_level=1;


	if (file_exist (file_name))
	{
		unlink (file_name);
	}

	fd = fopen(file_name,"w");

	if(fd == NULL)
		return 0;

	len = xmlutil_create_xml_version(arrXml,"1.0","UTF-8");
	xml_write_to_file(&fd,arrXml,len);

	len = xmlutil_create_xml_namespace(arrXml,"meaEgressPort","http://www.ethernity-net.com/enet/egressPort");
	xml_write_to_file(&fd,arrXml,len);
	{
		len = xmlutil_open_groupname(arrXml,"egress_Data",xml_level++);
		xml_write_to_file(&fd,arrXml,len);
		{
			len = xmlutil_createNode_groupname_int(arrXml,"unit",xml_level,unit);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"port_id",xml_level,portId);
			xml_write_to_file(&fd,arrXml,len);
#if 0
			len = xmlutil_createNode_groupname_int(arrXml,"phy_port",xml_level,pEgressPortInfo->portEgress.phy_port);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"halt",xml_level,pEgressPortInfo->portEgress.halt);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"flush",xml_level,pEgressPortInfo->portEgress.flush);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"MTU",xml_level,pEgressPortInfo->portEgress.MTU);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"proto",xml_level,pEgressPortInfo->portEgress.proto);
			xml_write_to_file(&fd,arrXml,len);

#endif
			len = xmlutil_createNode_groupname_int(arrXml,"tx_enable",xml_level,pEntry->tx_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"calc_crc",xml_level,pEntry->calc_crc);
			xml_write_to_file(&fd,arrXml,len);
#if 0
			len = xmlutil_createNode_groupname_int(arrXml,"shaper_enable",xml_level,pEgressPortInfo->portEgress.shaper_enable);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"Shaper_compensation",xml_level,pEgressPortInfo->portEgress.Shaper_compensation);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"g9991_frg_scheduler",xml_level,pEgressPortInfo->portEgress.g9991_frg_scheduler);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"ing_hw_switch_protection",xml_level,pEgressPortInfo->portEgress.ing_hw_switch_protection);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"shaper_Id",xml_level,pEgressPortInfo->portEgress.shaper_Id);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_open_groupname(arrXml,"shaper_info",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				len = xmlutil_createNode_groupname_int(arrXml,"CBS",xml_level,pEgressPortInfo->portEgress.shaper_info.CBS);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"CIR",xml_level,pEgressPortInfo->portEgress.shaper_info.CIR);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"Cell_Overhead",xml_level,pEgressPortInfo->portEgress.shaper_info.Cell_Overhead);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"mode",xml_level,pEgressPortInfo->portEgress.shaper_info.mode);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"overhead",xml_level,pEgressPortInfo->portEgress.shaper_info.overhead);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"resolution_type",xml_level,pEgressPortInfo->portEgress.shaper_info.resolution_type);
				xml_write_to_file(&fd,arrXml,len);
			}
			len = xmlutil_close_groupname(arrXml,"shaper_info",--xml_level);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_macaddr(arrXml,"My_Mac",xml_level,pEgressPortInfo->portEgress.My_Mac.b);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_open_groupname(arrXml,"pause_send",xml_level++);
			xml_write_to_file(&fd,arrXml,len);
			{
				len = xmlutil_createNode_groupname_int(arrXml,"CRC_XOFF",xml_level,pEgressPortInfo->portEgress.pause_send.CRC_XOFF);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"CRC_XON",xml_level,pEgressPortInfo->portEgress.pause_send.CRC_XON);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"AF_threshold",xml_level,pEgressPortInfo->portEgress.pause_send.AF_threshold);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"Transmit_threshold",xml_level,pEgressPortInfo->portEgress.pause_send.Transmit_threshold);
				xml_write_to_file(&fd,arrXml,len);

				len = xmlutil_createNode_groupname_int(arrXml,"Working_mode",xml_level,pEgressPortInfo->portEgress.pause_send.Working_mode);
				xml_write_to_file(&fd,arrXml,len);
			}
			len = xmlutil_close_groupname(arrXml,"pause_send",--xml_level);
			xml_write_to_file(&fd,arrXml,len);

			len = xmlutil_createNode_groupname_int(arrXml,"protectType",xml_level,pEgressPortInfo->portEgress.protectType);
			xml_write_to_file(&fd,arrXml,len);
#endif

		}
		len = xmlutil_close_groupname(arrXml,"egress_Data",--xml_level);
		xml_write_to_file(&fd,arrXml,len);
	}
	len = xmlutil_close_groupname(arrXml,"meaEgressPort",--xml_level);
	xml_write_to_file(&fd,arrXml,len);

	fclose(fd);
	return 0;
}


int buildXml_getRmon_xml(MEA_Counters_RMON_dbt *pEntry, int port_id, char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;
    MEA_Counter_t value;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml, "meaRmonCounter", "http://www.ethernity-net.com/enet/RmonCounter");
    xml_write_to_file(&fd, arrXml, len);
    {
        len = xmlutil_open_groupname(arrXml, "RmonCounter", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        {
            len = xmlutil_createNode_groupname_int(arrXml, "portId", xml_level, port_id);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Rx64OctectPackets", xml_level, pEntry->Rx_64Octets_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Rx65-127OctectPackets", xml_level, pEntry->Rx_65to127Octets_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Rx128-255OctectPackets", xml_level, pEntry->Rx_128to255Octets_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Rx256-511OctectPackets", xml_level, pEntry->Rx_256to511Octets_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Rx512-1023OctectPackets", xml_level, pEntry->Rx_512to1023Octets_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Rx1024-1518OctectPackets", xml_level, pEntry->Rx_1024to1518Octets_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Rx1519-2047OctectPackets", xml_level, pEntry->Rx_1519to2047Octets_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Rx2048-MaxOctectPackets", xml_level, pEntry->Rx_2048toMaxOctets_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxBroadCastPackets", xml_level, pEntry->Rx_BC_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxBytes", xml_level, pEntry->Rx_Bytes);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxCRCError", xml_level, pEntry->Rx_CRC_Err);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxDropBytes", xml_level, pEntry->Rx_Drop_Bytes);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxErrorPackets", xml_level, pEntry->Rx_Error_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxMCPackets", xml_level, pEntry->Rx_MC_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxMacDropSmallfragmentPackets", xml_level, pEntry->Rx_Mac_DropSmallfragment_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxMacDropPackets", xml_level, pEntry->Rx_Mac_Drop_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxOversizeBytes", xml_level, pEntry->Rx_Oversize_Bytes);
            xml_write_to_file(&fd, arrXml, len);


            len = xmlutil_createNode_groupname_int64(arrXml, "RxPackets", xml_level, pEntry->Rx_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxUCPackets", xml_level, pEntry->Rx_UC_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "RxFragmantPackets", xml_level, pEntry->Rx_fragmant_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            value.val=pEntry->Rx_rate.val;
            len = xmlutil_createNode_groupname_int64(arrXml, "RxRate", xml_level, value);
            xml_write_to_file(&fd, arrXml, len);

            value.val=pEntry->Rx_ratePacket.val;
            len = xmlutil_createNode_groupname_int64(arrXml, "RxRatePackets", xml_level, value);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Tx64OctectPackets", xml_level, pEntry->Tx_64Octets_Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Tx65-127OctectPackets", xml_level, pEntry->Tx_65to127Octets_Pkts);
            xml_write_to_file(&fd, arrXml, len);
        }
        len = xmlutil_close_groupname(arrXml, "RmonCounter", --xml_level);
        xml_write_to_file(&fd, arrXml, len);

    }
    len = xmlutil_close_groupname(arrXml, "meaRmonCounter", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;

}

int buildXml_getPacketGenProfile_xml(MEA_PacketGen_drv_Profile_info_entry_dbt *pEntry, int profile_id, char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;


    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml,"meaPacketGenProfile","http://www.ethernity-net.com/enet/meaPacketGenProfile");
    xml_write_to_file(&fd, arrXml, len);
    {
        len = xmlutil_open_groupname(arrXml, "PacketGenProfile", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        {
            len = xmlutil_createNode_groupname_int(arrXml, "profileId", xml_level, profile_id);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "forceErrorPrbs", xml_level, pEntry->forceErrorPrbs);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "headerLength", xml_level, pEntry->header_length);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "seqOffsetType", xml_level, pEntry->seq_offset_type);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "seqStampingEnable", xml_level, pEntry->seq_stamping_enable);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "streamType", xml_level, pEntry->stream_type);
            xml_write_to_file(&fd, arrXml, len);

        }
        len = xmlutil_close_groupname(arrXml, "PacketGenProfile", --xml_level);
        xml_write_to_file(&fd, arrXml, len);

    }
    len = xmlutil_close_groupname(arrXml, "meaPacketGenProfile", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;
}

int buildXml_getAllPmId_xml(int unit,MEA_PmId_t  *pPmIdArry,int num_of_pms,char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;
    int idx=0;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml, "meaPmCounter", "http://www.ethernity-net.com/enet/AllPmCounters");
    xml_write_to_file(&fd, arrXml, len);
    {
    	for(idx=0;idx<num_of_pms;idx++)
    	{
			len = xmlutil_open_groupname(arrXml, "PmCounterId", xml_level++);
			xml_write_to_file(&fd, arrXml, len);
			{
				len = xmlutil_createNode_groupname_int(arrXml, "unit", xml_level, unit);
				xml_write_to_file(&fd, arrXml, len);
				len = xmlutil_createNode_groupname_int(arrXml, "pmId", xml_level, pPmIdArry[idx]);
				xml_write_to_file(&fd, arrXml, len);
			}
			len = xmlutil_close_groupname(arrXml, "PmCounterId", --xml_level);
			xml_write_to_file(&fd, arrXml, len);
    	}
    }
    len = xmlutil_close_groupname(arrXml, "meaPmCounter", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;


}

int buildXml_getPmId_xml(MEA_Counters_PM_dbt *pEntry, int pmId, char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;
    MEA_Counter_t value;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml, "meaPmCounter", "http://www.ethernity-net.com/enet/PmCounter");
    xml_write_to_file(&fd, arrXml, len);
    {
        len = xmlutil_open_groupname(arrXml, "PmCounter", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        {
//            FwdGreen        FwdYellow         DisGreen        DisYellow           DisRed         DisOther           DisMtu

            len = xmlutil_createNode_groupname_int(arrXml, "pmId", xml_level, pmId);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int64(arrXml, "FwdGreen-pkts", xml_level, pEntry->green_fwd_pkts);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int64(arrXml, "FwdGreen-bytes", xml_level, pEntry->green_fwd_bytes);
            xml_write_to_file(&fd, arrXml, len);
            value.val = pEntry->rate_yellow_fwd_pkts.val;
            len = xmlutil_createNode_groupname_int64(arrXml, "FwdYellow-pkts", xml_level, value);
            xml_write_to_file(&fd, arrXml, len);
            value.val = pEntry->rate_yellow_fwd_bytes.val;
            len = xmlutil_createNode_groupname_int64(arrXml, "FwdYellow-bytes", xml_level, value);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int64(arrXml, "DisGreen-pkts", xml_level, pEntry->green_dis_pkts);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int64(arrXml, "DisGreen-bytes", xml_level, pEntry->green_dis_bytes);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int64(arrXml, "DisYellow-pkts", xml_level, pEntry->yellow_dis_pkts);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int64(arrXml, "DisYellowRed-bytes", xml_level, pEntry->yellow_or_red_dis_bytes);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int64(arrXml, "DisRed-pkts", xml_level, pEntry->red_dis_pkts);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int64(arrXml, "DisOther", xml_level, pEntry->other_dis_pkts);
            xml_write_to_file(&fd, arrXml, len);
            len = xmlutil_createNode_groupname_int64(arrXml, "DisMtu", xml_level, pEntry->dis_mtu_pkts);
            xml_write_to_file(&fd, arrXml, len);
            value.val = pEntry->rate_green_fwd_bytes.val;
            len = xmlutil_createNode_groupname_int64(arrXml, "FwdGreenRate", xml_level, value);
            xml_write_to_file(&fd, arrXml, len);
            value.val = pEntry->rate_yellow_fwd_bytes.val;
            len = xmlutil_createNode_groupname_int64(arrXml, "FwdYellowRate", xml_level, value);
            xml_write_to_file(&fd, arrXml, len);

            
        }
        len = xmlutil_close_groupname(arrXml, "PmCounter", --xml_level);
        xml_write_to_file(&fd, arrXml, len);

    }
    len = xmlutil_close_groupname(arrXml, "meaPmCounter", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;

}

int buildXml_Dm_xml(tDmConfiguration *pDmConfig, char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml, "meaDmConfiguration", "http://www.ethernity-net.com/enet/DmConfiguration");
    xml_write_to_file(&fd, arrXml, len);
    {
        len = xmlutil_open_groupname(arrXml, "dmParameters", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        {
            len = xmlutil_createNode_groupname_int(arrXml, "unit", xml_level, pDmConfig->unit);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "Dm_side", xml_level, pDmConfig->Dm_side);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "cfmVersion", xml_level, pDmConfig->cfmVersion);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "policerId", xml_level, pDmConfig->policerId);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "policerId", xml_level, pDmConfig->srcPort);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_macaddr(arrXml, "destMac", xml_level, &pDmConfig->destMac.b[0]);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "megLevel", xml_level, pDmConfig->megLevel);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "Priority", xml_level, pDmConfig->u1CcmPriority);
            xml_write_to_file(&fd, arrXml, len);
        }
        len = xmlutil_close_groupname(arrXml, "dmParameters", --xml_level);
        xml_write_to_file(&fd, arrXml, len);
    }
    len = xmlutil_close_groupname(arrXml, "meaDmConfiguration", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;
}

int buildXml_Lbm_statistics_xml(MEA_Counters_Analyzer_dbt *pEntry,char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml, "meaStatistics", "http://www.ethernity-net.com/enet/LbmDmmStatistics");
    xml_write_to_file(&fd, arrXml, len);
    {
        len = xmlutil_open_groupname(arrXml, "lbmDmmStatistics", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        {
            len = xmlutil_createNode_groupname_int(arrXml, "unit", xml_level, 0);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_uint(arrXml, "AVG_latency", xml_level, pEntry->AVG_latency);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Bytes", xml_level, pEntry->Bytes);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_uint(arrXml, "MAX_jitter", xml_level, pEntry->MAX_jitter);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_uint(arrXml, "MIN_jitter", xml_level, pEntry->MIN_jitter);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "Pkts", xml_level, pEntry->Pkts);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "drop", xml_level, pEntry->drop);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "lastseqID", xml_level, pEntry->lastseqID);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "num_Of_Bits_Error", xml_level, pEntry->num_Of_Bits_Error);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "seq_ID_err", xml_level, pEntry->seq_ID_err);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml, "seq_ID_reorder", xml_level, pEntry->seq_ID_reorder);
            xml_write_to_file(&fd, arrXml, len);
        }
        len = xmlutil_close_groupname(arrXml, "lbmDmmStatistics", --xml_level);
        xml_write_to_file(&fd, arrXml, len);
    }
    len = xmlutil_close_groupname(arrXml, "meaStatistics", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;

}

int buildXml_Lb_xml(tLbConfiguration *pLbConfig, char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml, "meaLmConfiguration", "http://www.ethernity-net.com/enet/LmConfiguration");
    xml_write_to_file(&fd, arrXml, len);
    {
        len = xmlutil_open_groupname(arrXml, "lbParameters", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        {
            len = xmlutil_createNode_groupname_int(arrXml, "unit", xml_level, pLbConfig->unit);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "stream_id", xml_level, pLbConfig->stream_id);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "srcPort", xml_level, pLbConfig->srcPort);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "vlanId", xml_level, pLbConfig->vlanId);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "Priority", xml_level, pLbConfig->u1CcmPriority);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "Version", xml_level, pLbConfig->cfmVersion);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml, "megLevel", xml_level, pLbConfig->megLevel);
            xml_write_to_file(&fd, arrXml, len);
        }
        len = xmlutil_close_groupname(arrXml, "lbParameters", --xml_level);
        xml_write_to_file(&fd, arrXml, len);
    }
    len = xmlutil_close_groupname(arrXml, "meaLmConfiguration", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;
}


int buildXml_Ccm_xml(tCcmConfiguration *pCcmConfig, char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);
    memset(arrXml,0,sizeof(arrXml));

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);
    memset(arrXml,0,sizeof(arrXml));

    len = xmlutil_create_xml_namespace(arrXml, "meaCcmConfiguration", "http://www.ethernity-net.com/enet/CcmConfiguration");
    xml_write_to_file(&fd, arrXml, len);
    memset(arrXml,0,sizeof(arrXml));
    {
        len = xmlutil_open_groupname(arrXml, "ccmParameters", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        memset(arrXml,0,sizeof(arrXml));
        {
            len = xmlutil_createNode_groupname_int(arrXml, "unit", xml_level, pCcmConfig->unit);
            xml_write_to_file(&fd, arrXml, len);
            memset(arrXml,0,sizeof(arrXml));

            len = xmlutil_createNode_groupname_int(arrXml, "stream_id", xml_level, pCcmConfig->stream_id);
            xml_write_to_file(&fd, arrXml, len);
            memset(arrXml,0,sizeof(arrXml));

            len = xmlutil_createNode_groupname_int(arrXml, "ccmPeriod", xml_level, pCcmConfig->ccmPeriod);
            xml_write_to_file(&fd, arrXml, len);
            memset(arrXml,0,sizeof(arrXml));

            len = xmlutil_createNode_groupname_int(arrXml, "active", xml_level, pCcmConfig->active);
            xml_write_to_file(&fd, arrXml, len);
            memset(arrXml,0,sizeof(arrXml));

            len = xmlutil_createNode_groupname_int(arrXml, "cfmVersion", xml_level, pCcmConfig->cfmVersion);
            xml_write_to_file(&fd, arrXml, len);
            memset(arrXml,0,sizeof(arrXml));

            len = xmlutil_createNode_groupname_macaddr(arrXml,"destMac",xml_level,&pCcmConfig->destMac.b[0]);
            xml_write_to_file(&fd, arrXml, len);
            memset(arrXml,0,sizeof(arrXml));

            len = xmlutil_createNode_groupname_int(arrXml,"incoming_service_id",xml_level,pCcmConfig->incoming_service_id);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"lmEnable",xml_level,pCcmConfig->lmEnable);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"lmId",xml_level,pCcmConfig->lmId);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"localMepId",xml_level,pCcmConfig->localMepId);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_char(arrXml,"megId",xml_level,pCcmConfig->megId);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"megLevel",xml_level,pCcmConfig->megLevel);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"outgoing_service_id",xml_level,pCcmConfig->outgoing_service_id);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"policerId",xml_level,pCcmConfig->policerId);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"rdiEnable",xml_level,pCcmConfig->rdiEnable);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"remoteMepId",xml_level,pCcmConfig->remoteMepId);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"srcPort",xml_level,pCcmConfig->srcPort);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"u1CcmPriority",xml_level,pCcmConfig->u1CcmPriority);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"vlanId",xml_level,pCcmConfig->vlanId);
            xml_write_to_file(&fd, arrXml, len);


        }
        len = xmlutil_close_groupname(arrXml, "ccmParameters", --xml_level);
        xml_write_to_file(&fd, arrXml, len);
        memset(arrXml,0,sizeof(arrXml));
    }
    len = xmlutil_close_groupname(arrXml, "meaCcmConfiguration", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    memset(arrXml,0,sizeof(arrXml));
    fclose(fd);
    return 0;
}

int buildXml_Lm_counterXml(int unit, MEA_Counters_LM_dbt   *entry, char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml, "meaLmCounter", "http://www.ethernity-net.com/enet/lmCounter");
    xml_write_to_file(&fd, arrXml, len);
    {
        len = xmlutil_open_groupname(arrXml, "lmParameters", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        {
            len = xmlutil_createNode_groupname_int(arrXml,"unit",xml_level,unit);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml,"FarEndLoss",xml_level,entry->FarEndLoss);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml,"NearEndLoss",xml_level,entry->NearEndLoss);
            xml_write_to_file(&fd, arrXml, len);

        }
        len = xmlutil_close_groupname(arrXml, "lmParameters", --xml_level);
        xml_write_to_file(&fd, arrXml, len);
    }
    len = xmlutil_close_groupname(arrXml, "meaLmCounter", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;
}


int buildXml_Ccm_counterXml(int unit, MEA_Counters_CCM_Defect_dbt   *entry,MEA_Uint32 event_loss, char *file_name)
{
    char arrXml[200];
    int len = 0;
    FILE *fd = NULL;
    //    int i = 0;
    int xml_level = 1;

    if (file_exist(file_name))
    {
        unlink(file_name);
    }

    fd = fopen(file_name, "w");

    if (fd == NULL)
        return 0;

    len = xmlutil_create_xml_version(arrXml, "1.0", "UTF-8");
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_style(arrXml);
    xml_write_to_file(&fd, arrXml, len);

    len = xmlutil_create_xml_namespace(arrXml, "meaCcmCounter", "http://www.ethernity-net.com/enet/CcmCounter");
    xml_write_to_file(&fd, arrXml, len);
    {
        len = xmlutil_open_groupname(arrXml, "CcmDefectCount", xml_level++);
        xml_write_to_file(&fd, arrXml, len);
        {
            len = xmlutil_createNode_groupname_int(arrXml,"unit",xml_level,unit);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"LastSequenc",xml_level,entry->LastSequenc);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml,"Unexpected_MEG_ID",xml_level,entry->Unexpected_MEG_ID);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml,"Unexpected_MEP_ID",xml_level,entry->Unexpected_MEP_ID);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int64(arrXml,"reorder",xml_level,entry->reorder);
            xml_write_to_file(&fd, arrXml, len);

            len = xmlutil_createNode_groupname_int(arrXml,"eventLoss",xml_level,event_loss);
            xml_write_to_file(&fd, arrXml, len);

        }
        len = xmlutil_close_groupname(arrXml, "CcmDefectCount", --xml_level);
        xml_write_to_file(&fd, arrXml, len);
    }
    len = xmlutil_close_groupname(arrXml, "meaCcmCounter", --xml_level);
    xml_write_to_file(&fd, arrXml, len);
    fclose(fd);
    return 0;
}

