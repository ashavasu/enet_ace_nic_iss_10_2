#define					D_CCM_OPCODE_VALUE					1
#define					D_LBM_OPCODE_VALUE					3
#define					D_LBR_OPCODE_VALUE					2
#define					D_LTM_OPCODE_VALUE					5
#define					D_LTR_OPCODE_VALUE					4
#define					D_AIS_OPCODE_VALUE					33
#define					D_LCK_OPCODE_VALUE					35
#define					D_TST_OPCODE_VALUE					37
#define					D_APS_OPCODE_VALUE					39
#define					D_MCC_OPCODE_VALUE					41
#define					D_LMM_OPCODE_VALUE					43
#define					D_LMR_OPCODE_VALUE					42
#define					D_1DM_OPCODE_VALUE					45
#define					D_DMR_OPCODE_VALUE					46
#define					D_DMM_OPCODE_VALUE					47



#define					D_CCM_RDI_BIT_FLAG					0x80
#define					D_CCM_3_3MS_PERIOD					0x01
#define					D_CCM_10MS_PERIOD					0x02
#define					D_CCM_100MS_PERIOD					0x03
#define					D_CCM_1SEC_PERIOD					0x04
#define					D_CCM_10SEC_PERIOD					0x05
#define					D_CCM_1MIN_PERIOD					0x06
#define					D_CCM_10MIN_PERIOD					0x07


#define					D_MAX_CCM_STREAMS					10
#define					D_MAX_LBM_STREAMS					10
#define					D_MAX_DMM_STREAMS					10
#define					D_MAX_LMID_STREAMS					16
#define					D_MAX_ANALYZERS						16

typedef struct
{
	MEA_MacAddr	DA;
	MEA_MacAddr	SA;
	MEA_Uint32  num_of_vlans;
	MEA_Uint32  outerVlan;
	MEA_Uint32  innerVlan;
	MEA_Uint16	cfmEtherType;
	MEA_Uint32	meg_level; //3 bits
	MEA_Uint32	version; //5 bits
	MEA_Uint32	opCode; // 8 bits
	MEA_Uint32	flags; // 8 bits
	MEA_Uint32	tlvOffset; // 8 bits
	MEA_Uint32	initSeqNum;
	MEA_Uint16	mepId;
	MEA_Uint32	megLen;
	MEA_Uint8	megData[48];
	MEA_Uint32	TxFCf;
	MEA_Uint32	RxFCb;
	MEA_Uint32	TxFCb;

}tPacketGenHeader;

typedef struct
{
	MEA_MacAddr	DA;
	MEA_MacAddr	SA;
	MEA_Uint32  num_of_vlans;
	MEA_Uint32  outerVlan;
	MEA_Uint32  innerVlan;
	MEA_Uint16	cfmEtherType;
	MEA_Uint32	meg_level; //3 bits
	MEA_Uint32	version; //5 bits
	MEA_Uint32	opCode; // 8 bits
	MEA_Uint32	flags; // 8 bits
	MEA_Uint32	tlvOffset; // 8 bits
	MEA_Uint32	initSeqNum;
	MEA_Uint32   type;
	MEA_Uint16  Length;
	MEA_Uint32  Pattern_Type;

}tPacketGenLbmHeader;

typedef struct
{
	MEA_MacAddr	DA;
	MEA_MacAddr	SA;
	MEA_Uint32  num_of_vlans;
	MEA_Uint32  outerVlan;
	MEA_Uint32  innerVlan;
	MEA_Uint16	cfmEtherType;
	MEA_Uint32	meg_level; //3 bits
	MEA_Uint32	version; //5 bits
	MEA_Uint32	opCode; // 8 bits
	MEA_Uint32	flags; // 8 bits
	MEA_Uint32	tlvOffset; // 8 bits
	MEA_Uint64	TxTimeStampf;
	MEA_Uint64  RxTimeStampf;
	MEA_Uint64  TxTimeStampb;
	MEA_Uint64  DmrRcv;

}tPacketGenDmmHeader;

typedef struct
{
	MEA_Service_t Service_id;
	MEA_PacketGen_t packetGenId;
	MEA_PacketGen_profile_info_t  packetGenProfileId;
	MEA_Action_t action_id;
	MEA_Bool	active;
	MEA_Uint16 		VpnIndex;

}tEnetPacketGen;

typedef struct
{
	MEA_Action_t action_id;
	MEA_CcmId_t  id_io;
	MEA_Counter_t         green_fwd_pkts;
	MEA_Counters_CCM_Defect_dbt DefectEntry;
	MEA_Uint16 						VpnIndex;
	MEA_Uint32						ccm_interval;
	MEA_Bool					sendPacketToCpu;
}tEnetPacketAna;

typedef struct
{
	MEA_PacketGen_profile_info_t  	packetProfileId;
	MEA_PacketGen_t 				packetGenId;
	MEA_Service_t 					Service_id;
	MEA_Action_t 					action_id;
	MEA_Action_t 					analyzer_action_id;
	MEA_Uint16						VpnIndex;
	MEA_Analyzer_t					analyzerId;
	MEA_Service_t 					analyzer_Service_id;
}tEnetPacketLbm,tEnetPacketDmm;

typedef struct
{
	MEA_Action_t 					action_id;
	MEA_Uint16						VpnIndex;
	MEA_Service_t 					Service_id;
}tEnetPacketLbr,tEnetPacketDmr;

typedef struct
{
	int valid;
	int stream_id;
	int active;
    int unit ;
    MEA_MacAddr destMac;
    int srcPort;
    int vlanId;
    int megLevel;
    int cfmVersion;
    int ccmPeriod;
    int rdiEnable;
    char megId[48];
    MEA_Bool lmEnable;
    MEA_Uint16	lmId;
    int remoteMepId;
    int localMepId;
    int u1CcmPriority;
    int policerId;
    int outgoing_service_id;
    int incoming_service_id;
    tEnetPacketGen packetGen;
    tEnetPacketAna packetAna;
}tCcmConfiguration;

typedef struct
{
	int valid;
	int stream_id;
    int unit ;
    MEA_MacAddr destMac;
    int srcPort;
    int vlanId;
    int megLevel;
    int cfmVersion;
    int ccmPeriod;
    int rdiEnable;
    char megId[48];
    MEA_Bool lmEnable;
    MEA_Uint16	lmId;
    int remoteMepId;
    int localMepId;
    int u1CcmPriority;
    int policerId;
    int outgoing_service_id;
    int incoming_service_id;
    int prbs;
    int inject_error;
    tEnetPacketLbm packetLbm;
    tEnetPacketLbr packetLbr;
    int lbm_side;
}tLbConfiguration;

typedef struct
{
	int valid;
	int stream_id;
    int unit ;
    MEA_MacAddr destMac;
    int srcPort;
    int vlanId;
    int megLevel;
    int cfmVersion;
    int ccmPeriod;
    int rdiEnable;
    char megId[48];
    MEA_Bool lmEnable;
    MEA_Uint16	lmId;
    int remoteMepId;
    int localMepId;
    int u1CcmPriority;
    int policerId;
    int outgoing_service_id;
    int incoming_service_id;
    int prbs;
    int inject_error;
    tEnetPacketDmm packetDmm;
    tEnetPacketDmr packetDmr;
    int Dm_side;
}tDmConfiguration;

typedef struct
{
	int		 valid;
	int		 lmId;
}tLmIdDatabase;

typedef struct
{
	int		 valid;
	int		 analyzerId;
}tAnalyzerDatabase;


#define 	MAX_SIZE_FILTER_STR 		20


typedef struct
{
	char destMac[MAX_SIZE_FILTER_STR];
	char srcMac[MAX_SIZE_FILTER_STR];
	char srcIP[MAX_SIZE_FILTER_STR];
	char dstIP[MAX_SIZE_FILTER_STR];
	char dstPort[MAX_SIZE_FILTER_STR];
	char srcPort[MAX_SIZE_FILTER_STR];
	char ipProto[MAX_SIZE_FILTER_STR];

	char editVlan[MAX_SIZE_FILTER_STR];
	char editPri[MAX_SIZE_FILTER_STR];
	char editSrcMac[MAX_SIZE_FILTER_STR];
	char editDstMac[MAX_SIZE_FILTER_STR];
}FilterDataStr;


MEA_Status mea_client_send_udp_packet(char *bufferSnd,int sendlen,char *bufferRcv,int maxRcvBuff,int *pByteRvc);
int create_Ccm_Configuration(tCcmConfiguration *pConfig);
int delete_Ccm_Configuration(tCcmConfiguration *pConfig);
int allocate_vpn_entry(void);
int create_Lbm_Configuration(tLbConfiguration *pConfig);
int delete_Lbm_Configuration(tLbConfiguration *pConfig);
int create_Lbr_Configuration(tLbConfiguration *pConfig);
int delete_Lbr_Configuration(tLbConfiguration *pConfig);
void FsMiEcfmHwInit(void);
int create_Dmm_Configuration(tDmConfiguration *pConfig);
int delete_Dmm_Configuration(tDmConfiguration *pConfig);
int create_Dmr_Configuration(tDmConfiguration *pConfig);
int delete_Dmr_Configuration(tDmConfiguration *pConfig);

int buildXml_ingrss_port_info(int unit,int port_id,MEA_IngressPort_Entry_dbt *pIgressPortInfo,char *file_name);
int buildXml_ingrss_port_mapping(int *pPortArry ,int MaxNumberOfPorts,int unit,char *file_name);
int buildXml_policer_profiles_id(MEA_Uint16 *Profles,MEA_Uint32 numOfProfiles,int unit,char *file_name);
int buildXml_serviceId_Params(tComService *pServiceEntry,char *file_name);
int buildXml_getPolicerProfile_xml(MEA_Policer_Entry_dbt *pEntry, char *file_name);
int buildXml_getPmId_xml(MEA_Counters_PM_dbt *pEntry, int pmId,char *file_name);
int buildXml_getAllPmId_xml(int unit,MEA_PmId_t  *pPmIdArry,int num_of_pms,char *file_name);
int buildXml_getRmon_xml(MEA_Counters_RMON_dbt *pEntry, int port_id, char *file_name);
int buildXml_service_id(MEA_Service_t  *pServiceIdArry,MEA_Uint32 number_of_services,int unit,char *file_name);
int buildXml_serviceId_vlan_Params(tServiceVlanParams *pServiceEntry,char *file_name,int unit,int service);
int buildXml_serviceId_filter_Params(char *file_name,int unit,int service, FilterDataStr	*pStrFilterData);
int buildXml_getPacketGenProfile_xml(MEA_PacketGen_drv_Profile_info_entry_dbt *pEntry, int profile_id, char *file_name);
int buildXml_Ccm_xml(tCcmConfiguration *pCcmConfig, char *file_name);
int buildXml_Dm_xml(tDmConfiguration *pDmConfig, char *file_name);
int buildXml_Lb_xml(tLbConfiguration *pLbConfig, char *file_name);
int buildXml_Lbm_statistics_xml(MEA_Counters_Analyzer_dbt *pEntry,char *file_name);
int buildXml_Ccm_counterXml(int unit, MEA_Counters_CCM_Defect_dbt   *entry, MEA_Uint32 event_loss,char *file_name);
int buildXml_Lm_counterXml(int unit, MEA_Counters_LM_dbt   *entry, char *file_name);
int buildXml_egress_port_info(MEA_EgressPort_Entry_dbt *pEntry,int unit,int portId,char *file_name);




