# .bashrc

echo text "Alex----Weis MEA_OS_ZYNQ7000"

export BASE_DIR=$(pwd)
export MV_DEV_LOCAL=$BASE_DIR;
export MY_PLATFORM="mea_PicoZ"

export MEA_BOARD_TYPE=arm7

export MV_ROOT="/opt/petalinux/linux-i386"
export MV_TOOL_CHAIN="$MV_ROOT/arm-xilinx-linux-gnueabi/arm-xilinx-linux-gnueabi"
export CROSS_COMPILE="$MV_ROOT/arm-xilinx-linux-gnueabi/bin/arm-xilinx-linux-gnueabi-"
export ARCH="arm"
   # N.B. = Comment the line below to work without simulation on the board
#  export EXTRA_GLOBAL_CC_FLAGS=-DMEA_OS_DEVICE_MEMORY_SIMULATION
   # N.B. = The readline and ncurses library manually copied from the FREESCALE toolchain 
export READLINE_PATH="$MV_TOOL_CHAIN/libc/usr/include"
  
   
   

export ENET_ZYNQ="yes"
export MEA_ETHERNITY="yes"
export MEA_OS_ZYNQ7000="yes"

export MEA_ENV_S1_K7_NO_SYSLOG="yes"
#export MEA_RPC_ENG="yes"


export MEA_EXTRA_SUBSYSTEM=L1/$MY_PLATFORM;


if [ "$MEA_RPC_ENG" = yes ];
then
echo text "MEA_RPC_ENG >>>> YES  YES YES YES YES YES YES >>>[ $MEA_RPC_ENG ]"
export GLOBAL_CC_FLAGS="-DMEA_OS_LINUX -DMEA_ETHERNITY -DMEA_OS_ZYNQ7000 -DMEA_ENV_S1_K7_NO_SYSLOG -DMEA_RPC_ENG";

else
echo text "NNNNNNNNNNNNNNNNNN "
export GLOBAL_CC_FLAGS="-DMEA_OS_LINUX -DMEA_ETHERNITY -DMEA_OS_ZYNQ7000 -DMEA_ENV_S1_K7_NO_SYSLOG ";
fi


pwd;
cd $MV_DEV_LOCAL;
pwd;
echo $pwd
cd Make

make clean_all;

pwd;
echo $pwd
#make -j9 MEAd ;
make MEAd

pwd;
echo $pwd;


echo text "Copy mea.ex to lib/$MY_PLATFORM/"$MY_PLATFORM"_mea.ex"
cp $MV_DEV_LOCAL/lib/L1/$MY_PLATFORM/mea.ex $MV_DEV_LOCAL/lib/$MY_PLATFORM/"$MY_PLATFORM"_mea.ex

echo text "Alex-Weis cli compile "

cd $MV_DEV_LOCAL/tools/MEA_cli_client ;

make clean; 

make

cp MEA_cli_client.elf $MV_DEV_LOCAL/lib/$MY_PLATFORM/meaCli

if [ "$MEA_RPC_ENG" = yes ];
then
echo text "Alex-Weis web-server compile "
echo text "MEA_RPC_ENG >>>>>> $MEA_RPC_ENG "

echo text "create static lib remote client "
cd $MV_DEV_LOCAL/tools/MEA_remote_client/build ;

make clean; 

make

cd $MV_DEV_LOCAL/tools/MEA_web_server/build ;

make clean; 

make

fi



echo text "Alex-Weis ENET.e compile "

cd $MV_DEV_LOCAL/tools/ENET_cli_client ;

make clean; 

make

cp ENET.e  $MV_DEV_LOCAL/lib/$MY_PLATFORM/.





echo "Please enter"
read name1  
exit






