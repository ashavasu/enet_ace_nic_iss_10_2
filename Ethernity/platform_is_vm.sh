#!/bin/bash

platform_desc=`dmidecode -s system-product-name`

if grep -qi "vmware" <<< "${platform_desc}"; then
	echo "vm"
	exit 1
elif grep -qi "virtualbox" <<< "${platform_desc}"; then
	echo "vm"
	exit 1
elif grep -qi "kvm" <<< "${platform_desc}"; then
	echo "vm"
	exit 1
else
	echo "${platform_desc}"
	exit 0
fi

