#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif

MEA_Status MEA_comm_Create_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status 			ret = MEA_OK;
	tComForwarder *pForwardEntry = (tComForwarder *)bufferRcv;
	tComForwarder *pGetForwardeEntry = (tComForwarder *)bufferSnd;

	MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "create forwarder key type:%d\r\n",pForwardEntry->entry.key.type);
	ret = MEA_API_Create_SE_Entry (pForwardEntry->unit,&pForwardEntry->entry);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Create_SE_Entry\r\n");
        (*pResult) = D_MSG_ENTRY_CANNOT_CREATE_COM_ERR;
        return ret;
    }

    memcpy(pGetForwardeEntry, pForwardEntry, sizeof(tComForwarder));
    (*pSend_bytes) = sizeof(tComForwarder);

	return ret;
}
MEA_Status MEA_comm_Delete_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status 			ret = MEA_OK;
	tComForwarder *pForwardEntry = (tComForwarder *)bufferRcv;
	tComForwarder *pGetForwardeEntry = (tComForwarder *)bufferSnd;


	ret = MEA_API_Delete_SE_Entry (pForwardEntry->unit,&pForwardEntry->entry.key);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Delete_SE_Entry\r\n");
        (*pResult) = D_MSG_ENTRY_CANNOT_DELETE_COM_ERR;
        return ret;
    }

    memcpy(pGetForwardeEntry, pForwardEntry, sizeof(tComForwarder));
    (*pSend_bytes) = sizeof(tComForwarder);

	return ret;
}
MEA_Status MEA_comm_Set_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status 			ret = MEA_OK;
	tComForwarder *pForwardEntry = (tComForwarder *)bufferRcv;
	tComForwarder *pGetForwardeEntry = (tComForwarder *)bufferSnd;


	ret = MEA_API_Set_SE_Entry (pForwardEntry->unit,&pForwardEntry->entry);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Set_SE_Entry\r\n");
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }

    memcpy(pGetForwardeEntry, pForwardEntry, sizeof(tComForwarder));
    (*pSend_bytes) = sizeof(tComForwarder);

	return ret;
}
MEA_Status MEA_comm_Get_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status 			ret = MEA_OK;
	tComForwarder *pForwardEntry = (tComForwarder *)bufferRcv;
	tComForwarder *pGetForwardeEntry = (tComForwarder *)bufferSnd;


	ret = MEA_API_Get_SE_Entry (pForwardEntry->unit,&pForwardEntry->entry);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_SE_Entry\r\n");
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }

    memcpy(pGetForwardeEntry, pForwardEntry, sizeof(tComForwarder));
    (*pSend_bytes) = sizeof(tComForwarder);

	return ret;
}
MEA_Status MEA_comm_GetFirst_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status 			ret = MEA_OK;
	tComForwarder *pForwardEntry = (tComForwarder *)bufferRcv;
	tComForwarder *pGetForwardeEntry = (tComForwarder *)bufferSnd;


	ret = MEA_API_GetFirst_SE_Entry (pForwardEntry->unit,&pForwardEntry->entry,&pForwardEntry->found);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_GetFirst_SE_Entry\r\n");
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }

    memcpy(pGetForwardeEntry, pForwardEntry, sizeof(tComForwarder));
    (*pSend_bytes) = sizeof(tComForwarder);

	return ret;
}
MEA_Status MEA_comm_GetNext_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status 			ret = MEA_OK;
	tComForwarder *pForwardEntry = (tComForwarder *)bufferRcv;
	tComForwarder *pGetForwardeEntry = (tComForwarder *)bufferSnd;


	ret = MEA_API_GetNext_SE_Entry (pForwardEntry->unit,&pForwardEntry->entry,&pForwardEntry->found);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_GetNext_SE_Entry\r\n");
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }

    memcpy(pGetForwardeEntry, pForwardEntry, sizeof(tComForwarder));
    (*pSend_bytes) = sizeof(tComForwarder);

	return ret;
}

MEA_Status MEA_comm_clearall_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status 			ret = MEA_OK;
	tComForwarder *pForwardEntry = (tComForwarder *)bufferRcv;
	tComForwarder *pGetForwardeEntry = (tComForwarder *)bufferSnd;


	MEA_API_DeleteAll_SE (pForwardEntry->unit);

    memcpy(pGetForwardeEntry, pForwardEntry, sizeof(tComForwarder));
    (*pSend_bytes) = sizeof(tComForwarder);

	return ret;
}

MEA_Status MEA_comm_GetRange_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status 			ret = MEA_OK;
	tComForwarder *pForwardEntry = (tComForwarder *)bufferRcv;
	tComIndexDb *pRetEntry = (tComIndexDb *)bufferSnd;
	MEA_SE_Entry_dbt	entry;
	MEA_Bool            found;
	tComForwardPortsDb	*pForwarderKey;


	if(pForwardEntry->first)
	{
		ret = MEA_API_GetFirst_SE_Entry (pForwardEntry->unit,&entry,&found);

	    if (ret != MEA_OK)
	    {
	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_GetFirst_SE_Entry\r\n");
	        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
	        return ret;
	    }
	}
	else
	{
		memcpy(&entry,&pForwardEntry->entry,sizeof(MEA_SE_Entry_dbt));
		ret = MEA_API_GetNext_SE_Entry (pForwardEntry->unit,&entry,&found);

	    if (ret != MEA_OK)
	    {
	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_GetNext_SE_Entry\r\n");
	        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
	        return ret;
	    }
	}
	pRetEntry->number_of_ports=0;
	pForwarderKey = (tComForwardPortsDb *)&pRetEntry->next[0];
	while( (found) && (pRetEntry->number_of_ports < 100) )
	{
		pRetEntry->number_of_ports++;
		memcpy(&pForwarderKey->key,&entry.key,sizeof(MEA_SE_Entry_key_dbt));
		pForwarderKey = (tComForwardPortsDb *)&pForwarderKey->next[0];
		ret = MEA_API_GetNext_SE_Entry (pForwardEntry->unit,&entry,&found);
	    if (ret != MEA_OK)
	    {
	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_GetNext_SE_Entry\r\n");
	        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
	        return ret;
	    }
	}
    (*pSend_bytes) = sizeof(tComIndexDb)+pRetEntry->number_of_ports*sizeof(tComForwardPortsDb);

	return ret;
}


