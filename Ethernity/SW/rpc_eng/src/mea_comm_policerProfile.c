#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"
#include "mea_policer_profile_drv.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif



MEA_Status MEA_comm_create_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComPolicerProfile *pSetPolicerProfileEntry = (tComPolicerProfile *)bufferRcv;
    tComPolicerProfile *pPolicerProfileEntry = (tComPolicerProfile *)bufferSnd;
    MEA_Status ret = MEA_OK;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "server:create policerId=%d\r\n", pSetPolicerProfileEntry->policer_id);


    ret = MEA_API_Create_Policer_ACM_Profile(pSetPolicerProfileEntry->unit,
        &pSetPolicerProfileEntry->policer_id,
        pSetPolicerProfileEntry->ACM_Mode,
        pSetPolicerProfileEntry->port_proto_prof_i,
        &pSetPolicerProfileEntry->policer_Entry);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Create_Policer_ACM_Profile :%d\r\n", pSetPolicerProfileEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_CANNOT_CREATE_COM_ERR;
    }

    memcpy(pPolicerProfileEntry, pSetPolicerProfileEntry, sizeof(tComPolicerProfile));
    (*pSend_bytes) = sizeof(tComPolicerProfile);

    return ret;
}
MEA_Status MEA_comm_set_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComPolicerProfile *pSetPolicerProfileEntry = (tComPolicerProfile *)bufferRcv;
    tComPolicerProfile *pPolicerProfileEntry = (tComPolicerProfile *)bufferSnd;
    MEA_Policer_Entry_dbt   EntryOut;
    MEA_Status ret = MEA_OK;

    ret = MEA_API_Get_Policer_ACM_Profile(pSetPolicerProfileEntry->unit,
        pSetPolicerProfileEntry->policer_id,
        pSetPolicerProfileEntry->ACM_Mode,
        &EntryOut);
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Policer_ACM_Profile :%d\r\n", pSetPolicerProfileEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


    ret = MEA_API_Set_Policer_ACM_Profile(pSetPolicerProfileEntry->unit,
        pSetPolicerProfileEntry->policer_id,
        pSetPolicerProfileEntry->ACM_Mode,
        pSetPolicerProfileEntry->port_proto_prof_i,
        &pSetPolicerProfileEntry->policer_Entry);
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_Policer_ACM_Profile :%d\r\n", pSetPolicerProfileEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }

    memcpy(pPolicerProfileEntry, pSetPolicerProfileEntry, sizeof(tComPolicerProfile));
    (*pSend_bytes) = sizeof(tComPolicerProfile);

    return ret;
}

MEA_Status MEA_comm_delete_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComPolicerProfile *pSetPolicerProfileEntry = (tComPolicerProfile *)bufferRcv;
    tComPolicerProfile *pPolicerProfileEntry = (tComPolicerProfile *)bufferSnd;
    MEA_Policer_Entry_dbt   EntryOut;
    MEA_Status ret = MEA_OK;

    ret = MEA_API_Get_Policer_ACM_Profile(pSetPolicerProfileEntry->unit,
        pSetPolicerProfileEntry->policer_id,
        pSetPolicerProfileEntry->ACM_Mode,
        &EntryOut);
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Policer_ACM_Profile :%d\r\n", pSetPolicerProfileEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }

    ret = MEA_API_Delete_Policer_ACM_Profile(pSetPolicerProfileEntry->unit,
        pSetPolicerProfileEntry->policer_id,
        pSetPolicerProfileEntry->ACM_Mode);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_Policer_ACM_Profile :%d\r\n", pSetPolicerProfileEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_CANNOT_DELETE_COM_ERR;
        return ret;
    }

    memcpy(pPolicerProfileEntry, pSetPolicerProfileEntry, sizeof(tComPolicerProfile));
    (*pSend_bytes) = sizeof(tComPolicerProfile);

    return ret;
}


MEA_Status MEA_comm_get_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComPolicerProfile *pSetPolicerProfileEntry = (tComPolicerProfile *)bufferRcv;
    tComPolicerProfile *pPolicerProfileEntry = (tComPolicerProfile *)bufferSnd;
    MEA_Status ret = MEA_OK;


    ret = MEA_API_Get_Policer_ACM_Profile(pSetPolicerProfileEntry->unit,
        pSetPolicerProfileEntry->policer_id,
        pSetPolicerProfileEntry->ACM_Mode,
        &pSetPolicerProfileEntry->policer_Entry);
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Policer_ACM_Profile :%d\r\n", pSetPolicerProfileEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }

    memcpy(pPolicerProfileEntry, pSetPolicerProfileEntry, sizeof(tComPolicerProfile));
    (*pSend_bytes) = sizeof(tComPolicerProfile);

    return ret;
}

MEA_Status MEA_comm_getall_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComPolicerProfile *pGetPolicerEntry = (tComPolicerProfile *)&bufferRcv[0];
    tComIndexDb *pRetPolicerEntry = (tComIndexDb *)&bufferSnd[0];
    tComIndex *pNextPort;
    MEA_Uint16        			policer_id;
    MEA_AcmMode_t     			ACM_Mode = 0;
    //	MEA_Policer_Entry_dbt       Entry_o;
    MEA_Bool                    found;
    MEA_Status ret = MEA_OK;
    MEA_Uint16					start_id;
    MEA_Uint16					stop_id;


    pRetPolicerEntry->number_of_ports = 0;
    pNextPort = (tComIndex *)&pRetPolicerEntry->next[0];

    start_id = 1;
    stop_id = MEA_POLICER_MAX_PROF - 1;//63 

    for (policer_id = start_id; policer_id <= stop_id; policer_id++)
    {
        if (mea_drv_Policer_Profile_IsExist(MEA_UNIT_0, ACM_Mode, policer_id, &found) != MEA_OK)
        {
            continue;
        }
        if (found)
        {
            pRetPolicerEntry->number_of_ports++;
            pNextPort->ingress_port = policer_id;
            pNextPort->unit = pGetPolicerEntry->unit;
            pNextPort = (tComIndex *)&pNextPort->next[0];
        }
    }

    (*pSend_bytes) = sizeof(tComIndexDb)+pRetPolicerEntry->number_of_ports*sizeof(tComIndex);

    return ret;
}

