#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif


MEA_Status MEA_comm_clear_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
    MEA_Bool   pmId_exist = MEA_FALSE;
    tComm_PmId_dbt *pRcvPM_ID = (tComm_PmId_dbt *)bufferRcv;
    tComm_PmId_dbt *pPM_ID_Entry = (tComm_PmId_dbt *)bufferSnd;

    pPM_ID_Entry->PmId = pRcvPM_ID->PmId;
    pPM_ID_Entry->unit = pRcvPM_ID->unit;



    ret = MEA_API_IsExist_PmId(pPM_ID_Entry->unit,
        pRcvPM_ID->PmId,
        &pmId_exist,
        NULL);



    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_IsExist_PmId PMId:%d\r\n", pRcvPM_ID->PmId);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
    }
    else if (pmId_exist != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to pmId_exist %d PMId:%d\r\n", pRcvPM_ID->PmId, pmId_exist);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        pPM_ID_Entry->valid = pmId_exist;
    }
    else
    {

        ret = MEA_API_Clear_Counters_PM(pPM_ID_Entry->unit,
            pRcvPM_ID->PmId);
        

    }
    //(*pSend_bytes) = sizeof(tComm_PmId_dbt);
    return ret;


}
MEA_Status MEA_comm_clear_All_Pm(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{


    MEA_Status ret = MEA_OK;
    tComm_PmId_dbt *pRcvPM_ID = (tComm_PmId_dbt *)bufferRcv;
    tComm_PmId_dbt *pPM_ID_Entry = (tComm_PmId_dbt *)bufferSnd;


    pPM_ID_Entry->PmId = pRcvPM_ID->PmId;
    pPM_ID_Entry->unit = pRcvPM_ID->unit;


   ret = MEA_API_Clear_Counters_PMs(pPM_ID_Entry->unit);


   // (*pSend_bytes) = sizeof(tComm_PmId_dbt);
    return ret;



}
MEA_Status MEA_comm_get_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
    MEA_Bool   pmId_exist = MEA_FALSE;
    tComm_PmId_dbt *pRcvPM_ID = (tComm_PmId_dbt *)bufferRcv;
    tComm_PmId_dbt *pPM_ID_Entry = (tComm_PmId_dbt *)bufferSnd;

    pPM_ID_Entry->PmId = pRcvPM_ID->PmId;
    pPM_ID_Entry->unit = pRcvPM_ID->unit;

    

    ret = MEA_API_IsExist_PmId(pPM_ID_Entry->unit,
        pRcvPM_ID->PmId,
        &pmId_exist,
        NULL);



    if (ret != MEA_OK )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_IsExist_PmId PMId:%d\r\n", pRcvPM_ID->PmId);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
    }
    else if (pmId_exist != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to pmId_exist %d PMId:%d\r\n", pRcvPM_ID->PmId, pmId_exist);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        pPM_ID_Entry->valid = pmId_exist;
    }
    else
    {

        ret  = MEA_API_Get_Counters_PM(pPM_ID_Entry->unit,
            pRcvPM_ID->PmId, &pPM_ID_Entry->Data);
            pPM_ID_Entry->valid = pmId_exist;
        
    }
    (*pSend_bytes) = sizeof(tComm_PmId_dbt);
    return ret;
}

MEA_Status MEA_comm_get_first_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
    MEA_Bool   pmId_exist = MEA_FALSE;
    tComm_PmId_dbt *pRcvPM_ID = (tComm_PmId_dbt *)bufferRcv;
    tComm_PmId_dbt *pPM_ID_Entry = (tComm_PmId_dbt *)bufferSnd;

    pPM_ID_Entry->PmId = pRcvPM_ID->PmId;
    pPM_ID_Entry->unit = pRcvPM_ID->unit;



    MEA_PmId_t               pmId;
    MEA_PmId_t               first_pmId=1;
    MEA_PmId_t               last_pmId = MEA_MAX_NUM_OF_PM_ID - 1;;



    
    for (pmId = first_pmId; pmId <= last_pmId; pmId++) {

        /* Check valid last_pmId */
        if (MEA_API_IsExist_PmId(MEA_UNIT_0,
            pmId,
            &pmId_exist, NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_IsExist_PmId PMId:%d\r\n", pmId);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
            break;
        }

        if (pmId_exist == MEA_FALSE) {
            continue;
        }


        ret = MEA_API_Get_Counters_PM(pPM_ID_Entry->unit,pmId, &pPM_ID_Entry->Data);

        pPM_ID_Entry->PmId = pmId;
        pPM_ID_Entry->valid = pmId_exist;

        if (ret != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Counters_PM PMId:%d\r\n", pRcvPM_ID->PmId);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
            
        }

       


        break;

    }




    (*pSend_bytes) = sizeof(tComm_PmId_dbt);
    return ret;
}

MEA_Status MEA_comm_get_next_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
    MEA_Bool   pmId_exist = MEA_FALSE;
    tComm_PmId_dbt *pRcvPM_ID = (tComm_PmId_dbt *)bufferRcv;
    tComm_PmId_dbt *pPM_ID_Entry = (tComm_PmId_dbt *)bufferSnd;

    pPM_ID_Entry->PmId = pRcvPM_ID->PmId;
    pPM_ID_Entry->unit = pRcvPM_ID->unit;



    MEA_PmId_t               pmId;
    MEA_PmId_t               first_pmId = pRcvPM_ID->PmId + 1;
    MEA_PmId_t               last_pmId = MEA_MAX_NUM_OF_PM_ID - 1;;


    pPM_ID_Entry->valid = MEA_FALSE;

    for (pmId = first_pmId; pmId <= last_pmId; pmId++) {

        /* Check valid last_pmId */
        if (MEA_API_IsExist_PmId(MEA_UNIT_0,
            pmId,
            &pmId_exist, NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_IsExist_PmId PMId:%d\r\n", pmId);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
            break;
        }

        if (pmId_exist == MEA_FALSE) {
            continue;
        }


        ret = MEA_API_Get_Counters_PM(pPM_ID_Entry->unit, pmId, &pPM_ID_Entry->Data);

        pPM_ID_Entry->PmId = pmId;
        pPM_ID_Entry->valid = pmId_exist;

        if (ret != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Counters_PM PMId:%d\r\n", pmId);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        }

       
        break;
    }




    (*pSend_bytes) = sizeof(tComm_PmId_dbt);
    return ret;
}

MEA_Status  MEA_comm_get_range_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComm_PmId_dbt *pRcvPM_ID = (tComm_PmId_dbt *)bufferRcv;
    tComIndexDb *pRetEntry = (tComIndexDb *)&bufferSnd[0];
    tComIngressPortId *pNextPort;
//    MEA_Uint32    		valid;
//    MEA_Status 			ret = MEA_OK;
    MEA_PmId_t          pmId;
    MEA_PmId_t          last_pmId = MEA_MAX_NUM_OF_PM_ID - 1;
    MEA_Bool   			pmId_exist = MEA_FALSE;

    if(pRcvPM_ID->PmId == 0)
    {
    	pRcvPM_ID->PmId=1;
    }
    pRetEntry->number_of_ports=0;
    pNextPort = (tComIngressPortId *)&pRetEntry->next[0];
	for (pmId = pRcvPM_ID->PmId; pmId <= last_pmId; pmId++)
	{

		/* Check valid last_pmId */
		if (MEA_API_IsExist_PmId(MEA_UNIT_0,
			pmId,
			&pmId_exist, NULL) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_IsExist_PmId PMId:%d\r\n", pmId);
			(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
			break;
		}

		if (pmId_exist == MEA_FALSE) {
			continue;
		}
    	pRetEntry->number_of_ports++;
        pNextPort->ingress_port = pmId;
        pNextPort->unit = pRcvPM_ID->unit;

        if(pRetEntry->number_of_ports > 1000)
        {
        	break;
        }
        pNextPort = (tComIndex *)&pNextPort->next[0];

	}
    (*pSend_bytes) = sizeof(tComIndexDb)+pRetEntry->number_of_ports*sizeof(tComIndex);

    return MEA_OK;
}

/************************************************************************/
/*       RMON Counter                                                 */
/************************************************************************/
MEA_Status MEA_comm_clear_RMON_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
    MEA_Bool   pmId_exist = MEA_FALSE;
    tComm_PmId_dbt *pRcvPM_ID = (tComm_PmId_dbt *)bufferRcv;
    tComm_PmId_dbt *pPM_ID_Entry = (tComm_PmId_dbt *)bufferSnd;

    pPM_ID_Entry->PmId = pRcvPM_ID->PmId;
    pPM_ID_Entry->unit = pRcvPM_ID->unit;



    ret = MEA_API_IsExist_PmId(pPM_ID_Entry->unit,
        pRcvPM_ID->PmId,
        &pmId_exist,
        NULL);



    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_IsExist_PmId PMId:%d\r\n", pRcvPM_ID->PmId);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
    }
    else if (pmId_exist != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to pmId_exist %d PMId:%d\r\n", pRcvPM_ID->PmId, pmId_exist);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        pPM_ID_Entry->valid = pmId_exist;
    }
    else
    {

        ret = MEA_API_Clear_Counters_PM(pPM_ID_Entry->unit,
            pRcvPM_ID->PmId);


    }
    //(*pSend_bytes) = sizeof(tComm_PmId_dbt);
    return ret;


}
MEA_Status MEA_comm_clear_All_RMON(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{


    MEA_Status ret = MEA_OK;
    MEA_Bool   pmId_exist = MEA_FALSE;
    tComm_PmId_dbt *pRcvPM_ID = (tComm_PmId_dbt *)bufferRcv;
    tComm_PmId_dbt *pPM_ID_Entry = (tComm_PmId_dbt *)bufferSnd;

    pPM_ID_Entry->PmId = pRcvPM_ID->PmId;
    pPM_ID_Entry->unit = pRcvPM_ID->unit;



    ret = MEA_API_IsExist_PmId(pPM_ID_Entry->unit,
        pRcvPM_ID->PmId,
        &pmId_exist,
        NULL);



    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_IsExist_PmId PMId:%d\r\n", pRcvPM_ID->PmId);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
    }
    else if (pmId_exist != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to pmId_exist %d PMId:%d\r\n", pRcvPM_ID->PmId, pmId_exist);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        pPM_ID_Entry->valid = pmId_exist;
    }
    else
    {

        ret = MEA_API_Clear_Counters_PMs(pPM_ID_Entry->unit);


    }
    // (*pSend_bytes) = sizeof(tComm_PmId_dbt);
    return ret;



}
MEA_Status MEA_comm_get_RMON_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
//    MEA_Bool   pmId_exist = MEA_FALSE;
    tComm_RMON_dbt *pRcv_ID = (tComm_RMON_dbt *)bufferRcv;
    tComm_RMON_dbt *pEntry = (tComm_RMON_dbt *)bufferSnd;



	ret = MEA_API_Get_Counters_RMON(pRcv_ID->unit,
			pRcv_ID->port, &pRcv_ID->Data);

	if(ret != MEA_OK)
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Counters_RMON :%d\r\n", pRcv_ID->port);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
	}

	memcpy(pEntry,pRcv_ID,sizeof(tComm_RMON_dbt));
    (*pSend_bytes) = sizeof(tComm_RMON_dbt);
    return ret;
}

MEA_Status MEA_comm_get_first_RMON_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
    MEA_Bool   rmon_exist = MEA_FALSE;
    tComm_RMON_dbt *pRcvRMON_ID = (tComm_RMON_dbt *)bufferRcv;
    tComm_RMON_dbt *pRMON_ID_Entry = (tComm_RMON_dbt *)bufferSnd;

    pRMON_ID_Entry->port = pRcvRMON_ID->port;
    pRMON_ID_Entry->unit = pRcvRMON_ID->unit;



    MEA_Port_t               port;
    MEA_Port_t               first_port = 0;
    MEA_Port_t               last_port = MEA_MAX_PORT_NUMBER;




    for (port = first_port; port <= last_port; port++) {

        
        /* Check for valid port parameter */
        rmon_exist = MEA_API_Get_IsRmonPortValid(port, MEA_TRUE);

        if (rmon_exist == MEA_FALSE) {
            continue;
        }


        ret = MEA_API_Get_Counters_RMON(pRMON_ID_Entry->unit,
            port,
            &pRcvRMON_ID->Data);

        

        pRcvRMON_ID->port = port;

        if (ret != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Counters_RMON PMId:%d\r\n", pRcvRMON_ID->port);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;

        }


        break;

    }




    (*pSend_bytes) = sizeof(tComm_RMON_dbt);
    return ret;
}

MEA_Status MEA_comm_get_next_RMON_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

   
    MEA_Status ret = MEA_OK;
    MEA_Bool   rmon_exist = MEA_FALSE;
    tComm_RMON_dbt *pRcvRMON_ID = (tComm_RMON_dbt *)bufferRcv;
    tComm_RMON_dbt *pRMON_ID_Entry = (tComm_RMON_dbt *)bufferSnd;

    pRMON_ID_Entry->port = pRcvRMON_ID->port+1;
    pRMON_ID_Entry->unit = pRcvRMON_ID->unit;



    MEA_Port_t               port;
    MEA_Port_t               first_port = 0;
    MEA_Port_t               last_port = MEA_MAX_PORT_NUMBER;





    for (port = first_port; port <= last_port; port++) {

        /* Check for valid port parameter */
        rmon_exist = MEA_API_Get_IsRmonPortValid(port, MEA_TRUE);

        if (rmon_exist == MEA_FALSE) {
            continue;
        }


        ret = MEA_API_Get_Counters_RMON(pRMON_ID_Entry->unit,
            port,
            &pRcvRMON_ID->Data);



        pRcvRMON_ID->port = port;

        if (ret != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Counters_RMON PMId:%d\r\n", pRcvRMON_ID->port);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;

        }


        break;

        break;
    }




    (*pSend_bytes) = sizeof(tComm_PmId_dbt);
    return ret;
}

/**************************************************** ccm counter ***************************/
MEA_Status MEA_comm_get_CCM_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
//    MEA_Bool   pmId_exist = MEA_FALSE;
    tComCcmCounter *pRcv_ID = (tComCcmCounter *)bufferRcv;
    tComCcmCounter *pEntry = (tComCcmCounter *)bufferSnd;


   if (MEA_API_IsExist_CCM(MEA_UNIT_0,
		   	   	   	   	   	   pRcv_ID->Id,
		   	   	   	   	   	   &pRcv_ID->exist) != MEA_OK)
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_IsExist_CCM PMId:%d\r\n", pRcv_ID->Id);
		return MEA_ERROR;
	}

	if(pRcv_ID->exist == MEA_TRUE)
	{
		if (MEA_API_Get_Counters_CCM(MEA_UNIT_0,
									pRcv_ID->Id,
									&pRcv_ID->entry) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Counters_CCM PMId:%d\r\n", pRcv_ID->Id);
			return MEA_ERROR;
		}
	}

	memcpy(pEntry,pRcv_ID,sizeof(tComCcmCounter));
    (*pSend_bytes) = sizeof(tComCcmCounter);
    return ret;
}

MEA_Status MEA_comm_clean_CCM_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
//    MEA_Bool   pmId_exist = MEA_FALSE;
    tComCcmCounter *pRcv_ID = (tComCcmCounter *)bufferRcv;
    tComCcmCounter *pEntry = (tComCcmCounter *)bufferSnd;


   if (MEA_API_IsExist_CCM(MEA_UNIT_0,
		   	   	   	   	   	   pRcv_ID->Id,
		   	   	   	   	   	   &pRcv_ID->exist) != MEA_OK)
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_IsExist_CCM PMId:%d\r\n", pRcv_ID->Id);
		return MEA_ERROR;
	}

	if(pRcv_ID->exist == MEA_TRUE)
	{
		if (MEA_API_Clear_Counters_CCM(MEA_UNIT_0,
									pRcv_ID->Id) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Counters_CCM PMId:%d\r\n", pRcv_ID->Id);
			return MEA_ERROR;
		}
	}

	memcpy(pEntry,pRcv_ID,sizeof(tComCcmCounter));
    (*pSend_bytes) = sizeof(tComCcmCounter);
    return ret;
}

/***********************************************************************************************/
MEA_Status MEA_comm_Get_Counters_LM(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    MEA_Status ret = MEA_OK;
//    MEA_Bool   pmId_exist = MEA_FALSE;
    tComLmCounter *pRcv_ID = (tComLmCounter *)bufferRcv;
    tComLmCounter *pEntry = (tComLmCounter *)bufferSnd;


	if (MEA_API_Get_Counters_LM(MEA_UNIT_0,
								pRcv_ID->Id,
								&pRcv_ID->entry) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Counters_LM PMId:%d\r\n", pRcv_ID->Id);
		return MEA_ERROR;
	}

	memcpy(pEntry,pRcv_ID,sizeof(tComLmCounter));
    (*pSend_bytes) = sizeof(tComLmCounter);
    return ret;
}
/***********************************************************************************************/
MEA_Status MEA_comm_Clear_Counters_LM(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    MEA_Status ret = MEA_OK;
//    MEA_Bool   pmId_exist = MEA_FALSE;
    tComLmCounter *pRcv_ID = (tComLmCounter *)bufferRcv;
    tComLmCounter *pEntry = (tComLmCounter *)bufferSnd;


	if (MEA_API_Clear_Counters_LM(MEA_UNIT_0,
								pRcv_ID->Id) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Clear_Counters_LM PMId:%d\r\n", pRcv_ID->Id);
		return MEA_ERROR;
	}

	memcpy(pEntry,pRcv_ID,sizeof(tComLmCounter));
    (*pSend_bytes) = sizeof(tComLmCounter);
    return ret;
}
