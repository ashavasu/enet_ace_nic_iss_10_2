#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif



MEA_Status MEA_comm_create_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComAction *pSetActionEntry = (tComAction *)bufferRcv;
    tComAction *pActionEntry = (tComAction *)bufferSnd;
    MEA_Policer_Entry_dbt          policerEntry;
    MEA_EgressHeaderProc_Array_Entry_dbt   EHP_Entry;
    MEA_Status ret = MEA_OK;
    MEA_Action_Entry_Data_dbt      		*pdata = NULL;



    // check if policer found
    ret = MEA_API_Get_Policer_ACM_Profile(pSetActionEntry->unit,
        pSetActionEntry->policer_id,
        pSetActionEntry->ACM_Mode,
        &policerEntry);
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Policer_ACM_Profile :%d\r\n", pSetActionEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "MEA_API_Create_Action :policerId:%d ACM_MODE:%d\r\n", pSetActionEntry->policer_id,pSetActionEntry->ACM_Mode);
    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "MEA_API_Create_Action :policerId:%d CIR:%llu\r\n", pSetActionEntry->policer_id,policerEntry.CIR);
    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "MEA_API_Create_Action :policerId:%d CBS:%lu\r\n", pSetActionEntry->policer_id,policerEntry.CBS);

    if (pSetActionEntry->data_valid == 1)
    {
        pdata = (MEA_Action_Entry_Data_dbt *)&pSetActionEntry->Action_Data;
        pdata->policer_prof_id_valid=1;
        pdata->policer_prof_id=pSetActionEntry->policer_id;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "set profile id:%d\r\n",pSetActionEntry->policer_id);
    }

    EHP_Entry.num_of_entries = pSetActionEntry->num_of_entries;
    EHP_Entry.ehp_info = (MEA_EHP_Info_dbt *)&pSetActionEntry->ehp_info[0];

    ret = MEA_API_Create_Action(pSetActionEntry->unit,
        pdata,
        &pSetActionEntry->Action_OutPorts,
        &policerEntry,
        &EHP_Entry,
        &pSetActionEntry->Action_Id);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Create_Action :%d\r\n", pSetActionEntry->Action_Id);
        (*pResult) = D_MSG_ENTRY_CANNOT_CREATE_COM_ERR;
        return ret;
    }


    memcpy(pActionEntry, pSetActionEntry, sizeof(tComService));
    (*pSend_bytes) = sizeof(tComAction);

    return ret;
}
MEA_Status MEA_comm_set_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComAction *pSetActionEntry = (tComAction *)bufferRcv;
    tComAction *pActionEntry = (tComAction *)bufferSnd;
    MEA_Policer_Entry_dbt          policerEntry;
    MEA_EgressHeaderProc_Array_Entry_dbt   EHP_Entry;
    MEA_Status ret = MEA_OK;
    MEA_Action_Entry_Data_dbt      		*pdata = NULL;
    MEA_OutPorts_Entry_dbt					*pOutput = NULL;

    // check if policer found
    ret = MEA_API_Get_Policer_ACM_Profile(pSetActionEntry->unit,
        pSetActionEntry->policer_id,
        pSetActionEntry->ACM_Mode,
        &policerEntry);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Policer_ACM_Profile :%d\r\n", pSetActionEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }
    if (pSetActionEntry->data_valid == 1)
    {
        pdata = (MEA_Action_Entry_Data_dbt *)&pSetActionEntry->Action_Data;
    }
    if (pSetActionEntry->outport_valid == 1)
    {
        pOutput = (MEA_OutPorts_Entry_dbt *)&pSetActionEntry->Action_OutPorts;
    }

    EHP_Entry.num_of_entries = pSetActionEntry->num_of_entries;
    EHP_Entry.ehp_info = (MEA_EHP_Info_dbt *)&pSetActionEntry->ehp_info[0];

    ret = MEA_API_Set_Action(pSetActionEntry->unit,
        pSetActionEntry->Action_Id,
        pdata,
        pOutput,
        &policerEntry,
        &EHP_Entry);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_Action :%d\r\n", pSetActionEntry->Action_Id);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }


    memcpy(pActionEntry, pSetActionEntry, sizeof(tComService));
    (*pSend_bytes) = sizeof(tComAction);

    return ret;
}
MEA_Status MEA_comm_get_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComAction *pSetActionEntry = (tComAction *)bufferRcv;
    tComAction *pActionEntry = (tComAction *)bufferSnd;
    MEA_Policer_Entry_dbt          policerEntry;
    MEA_EgressHeaderProc_Array_Entry_dbt   EHP_Entry;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pSetActionEntry->Action_Data, 0, sizeof(MEA_Action_Entry_Data_dbt));
    MEA_OS_memset(&pSetActionEntry->Action_OutPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
    MEA_OS_memset(&pSetActionEntry->ehp_info[0], 0, sizeof(MEA_EHP_Info_dbt)*D_MAX_NUM_OF_ACTION_EDITINGS);
    MEA_OS_memset(&EHP_Entry, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));


    ret =  MEA_API_Get_Action(pSetActionEntry->unit,
    							pSetActionEntry->Action_Id,
    							&pSetActionEntry->Action_Data,
    							&pSetActionEntry->Action_OutPorts,
    							&policerEntry,
    							&EHP_Entry);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Action :%d\r\n", pSetActionEntry->Action_Id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }

    if (EHP_Entry.num_of_entries > 0)
     {
         EHP_Entry.ehp_info = (MEA_EHP_Info_dbt *)&pSetActionEntry->ehp_info[0];

         ret =  MEA_API_Get_Action(pSetActionEntry->unit,
         							pSetActionEntry->Action_Id,
         							&pSetActionEntry->Action_Data,
         							&pSetActionEntry->Action_OutPorts,
         							&policerEntry,
         							&EHP_Entry);
         if (ret != MEA_OK)
         {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Action :%d\r\n", pSetActionEntry->Action_Id);
             (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
             return ret;
         }
         MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "get second time actionCom serviceId:%d\r\n", pSetActionEntry->Action_Id);
     }

	memcpy(pActionEntry, pSetActionEntry, sizeof(tComAction));
	pActionEntry->num_of_entries = EHP_Entry.num_of_entries;
	pActionEntry->policer_id = pSetActionEntry->policer_id;

	(*pSend_bytes) = sizeof(tComAction);

    return MEA_OK;
}
MEA_Status MEA_comm_getall_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    return MEA_OK;
}
MEA_Status MEA_comm_delete_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComAction *pSetActionEntry = (tComAction *)bufferRcv;
	tComAction *pActionEntry = (tComAction *)bufferSnd;
	MEA_Status ret = MEA_OK;

	ret = MEA_API_Delete_Action (pSetActionEntry->unit,pSetActionEntry->Action_Id);

    if (ret != MEA_OK)
     {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Delete_Action :%d\r\n", pSetActionEntry->Action_Id);
         (*pResult) = D_MSG_ENTRY_CANNOT_DELETE_COM_ERR;
         return ret;
     }
    memcpy(pActionEntry, pSetActionEntry, sizeof(tComAction));
    (*pSend_bytes) = sizeof(tComAction);
    return MEA_OK;
}
MEA_Status MEA_comm_getfirst_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComAction *pSetActionEntry = (tComAction *)bufferRcv;
	tComAction *pActionEntry = (tComAction *)bufferSnd;
	MEA_Status ret = MEA_OK;

	ret = MEA_API_GetFirst_Action (pSetActionEntry->unit,
										&pSetActionEntry->Action_Id ,
										&pSetActionEntry->found,
										pSetActionEntry->Action_type);

	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_GetFirst_Action :%d\r\n", pSetActionEntry->Action_Id);
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}

	memcpy(pActionEntry, pSetActionEntry, sizeof(tComAction));
	(*pSend_bytes) = sizeof(tComAction);
    return MEA_OK;
}
MEA_Status MEA_comm_getnext_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComAction *pSetActionEntry = (tComAction *)bufferRcv;
	tComAction *pActionEntry = (tComAction *)bufferSnd;
	MEA_Status ret = MEA_OK;

	ret = MEA_API_GetNext_Action (pSetActionEntry->unit,
										&pSetActionEntry->Action_Id ,
										&pSetActionEntry->found,
										pSetActionEntry->Action_type);

	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_GetNext_Action :%d\r\n", pSetActionEntry->Action_Id);
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}

	memcpy(pActionEntry, pSetActionEntry, sizeof(tComAction));
	(*pSend_bytes) = sizeof(tComAction);
    return MEA_OK;
}

MEA_Status MEA_comm_getrange_actions(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComAction *pSetActionEntry = (tComAction *)bufferRcv;
	tComIndexDb *pRetEntry = (tComIndexDb *)&bufferSnd[0];
    tComIngressPortId *pNextPort=NULL;
    MEA_Bool    		valid;
    MEA_Service_t 		actionId;
    MEA_Status 			ret = MEA_OK;

    if(pSetActionEntry->Action_Id == 0)
    {
    	//start from get first
	   if (MEA_API_GetFirst_Action(pSetActionEntry->unit,
			&actionId,
			&valid,
			pSetActionEntry->Action_type) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "action db is empty:%d\r\n", pSetActionEntry->unit);
			return MEA_OK;
		}
    }
    else
    {
    	actionId = pSetActionEntry->Action_Id;
    	ret = MEA_API_GetNext_Action(pSetActionEntry->unit,
             &actionId,
             &valid,
			 pSetActionEntry->Action_type);

    	if(ret != MEA_OK)
         {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Service :%d\r\n", actionId);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;;
			return ret;
         }
    }
    while ( (valid) && (pRetEntry->number_of_ports < 1000) )
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "actionId:%d\r\n", actionId);
    	pRetEntry->number_of_ports++;
        pNextPort->ingress_port = actionId;
        pNextPort->unit = pSetActionEntry->unit;
        pNextPort = (tComIngressPortId *)&pNextPort->next[0];

		if (MEA_API_GetNext_Action(pSetActionEntry->unit, &actionId, &valid, pSetActionEntry->Action_type) != MEA_OK)
		{
			break;
		}

    }
    (*pSend_bytes) = sizeof(tComIndexDb)+pRetEntry->number_of_ports*sizeof(tComIndex);

    return MEA_OK;
}



