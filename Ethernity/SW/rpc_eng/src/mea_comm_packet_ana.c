#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif

MEA_Status MEA_comm_getRange_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status ret = MEA_OK;

	return ret;
}
MEA_Status MEA_comm_get_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComCcmEntry *pGetEntry = (tComCcmEntry *)bufferRcv;
	tComCcmEntry *pEntry = (tComCcmEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_CCM_Configure_dbt));

    ret = MEA_API_Get_CCM_Entry(pGetEntry->unit,
    													pGetEntry->id,
    													&pGetEntry->entry);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComCcmEntry));

	(*pSend_bytes) = sizeof(tComCcmEntry);

    return ret;
}


MEA_Status MEA_comm_getfirst_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComCcmEntry *pGetEntry = (tComCcmEntry *)bufferRcv;
	tComCcmEntry *pEntry = (tComCcmEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_CCM_Configure_dbt));

    ret = MEA_API_GetFirst_CCM_Entry(pGetEntry->unit,
    													&pGetEntry->id,
    													&pGetEntry->entry,
    													&pGetEntry->found);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_GetFirst_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComCcmEntry));

	(*pSend_bytes) = sizeof(tComCcmEntry);

    return ret;
}

MEA_Status MEA_comm_getnext_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComCcmEntry *pGetEntry = (tComCcmEntry *)bufferRcv;
	tComCcmEntry *pEntry = (tComCcmEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_CCM_Configure_dbt));

    ret = MEA_API_GetNext_CCM_Entry(pGetEntry->unit,
    													&pGetEntry->id,
    													&pGetEntry->entry,
    													&pGetEntry->found);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_GetNext_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComCcmEntry));

	(*pSend_bytes) = sizeof(tComCcmEntry);

    return ret;
}

MEA_Status MEA_comm_create_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComCcmEntry *pGetEntry = (tComCcmEntry *)bufferRcv;
	tComCcmEntry *pEntry = (tComCcmEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_CCM_Configure_dbt));

    ret = MEA_API_Create_CCM_Entry(pGetEntry->unit,
    													&pGetEntry->entry,
    													&pGetEntry->id);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Create_PacketGen_Profile_info :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_CREATE_COM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComCcmEntry));

	(*pSend_bytes) = sizeof(tComCcmEntry);

    return ret;
}

MEA_Status MEA_comm_set_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComCcmEntry *pGetEntry = (tComCcmEntry *)bufferRcv;
	tComCcmEntry *pEntry = (tComCcmEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_CCM_Configure_dbt));

    ret = MEA_API_Set_CCM_Entry(pGetEntry->unit,
    													pGetEntry->id,
    													&pGetEntry->entry);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComCcmEntry));

	(*pSend_bytes) = sizeof(tComCcmEntry);

    return ret;
}

MEA_Status MEA_comm_delete_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComCcmEntry *pGetEntry = (tComCcmEntry *)bufferRcv;
	tComCcmEntry *pEntry = (tComCcmEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_CCM_Configure_dbt));

    ret = MEA_API_Delete_CCM_Entry(pGetEntry->unit,
    													pGetEntry->id);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Delete_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComCcmEntry));

	(*pSend_bytes) = sizeof(tComCcmEntry);

    return ret;
}
/********************* packet analyzer ******************************************/
MEA_Status MEA_comm_get_analyzer_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckAnalyzer *pGetEntry = (tComPckAnalyzer *)bufferRcv;
	tComPckAnalyzer *pEntry = (tComPckAnalyzer *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_Analyzer_configure_dbt));

    ret = MEA_API_Get_Analyzer(pGetEntry->unit,
    													pGetEntry->id,
    													&pGetEntry->entry);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Analyzer :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckAnalyzer));

	(*pSend_bytes) = sizeof(tComPckAnalyzer);

    return ret;
}

MEA_Status MEA_comm_set_analyzer_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckAnalyzer *pGetEntry = (tComPckAnalyzer *)bufferRcv;
	tComPckAnalyzer *pEntry = (tComPckAnalyzer *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_Analyzer_configure_dbt));

    ret = MEA_API_Set_Analyzer(pGetEntry->unit,
    													pGetEntry->id,
    													&pGetEntry->entry);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_Analyzer :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckAnalyzer));

	(*pSend_bytes) = sizeof(tComPckAnalyzer);

    return ret;
}

MEA_Status MEA_API_Get_comm_Counters_ANALYZER(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckCounterAna *pGetEntry = (tComPckCounterAna *)bufferRcv;
	tComPckCounterAna *pEntry = (tComPckCounterAna *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->counter, 0, sizeof(MEA_Counters_Analyzer_dbt));

    ret = MEA_API_Get_Counters_ANALYZER(pGetEntry->unit,pGetEntry->id,&pGetEntry->counter);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Counters_ANALYZER :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckCounterAna));

	(*pSend_bytes) = sizeof(tComPckCounterAna);

    return ret;
}
MEA_Status MEA_API_Clear_comm_Counters_ANALYZER(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckCounterAna *pGetEntry = (tComPckCounterAna *)bufferRcv;
	tComPckCounterAna *pEntry = (tComPckCounterAna *)bufferSnd;
    MEA_Status ret = MEA_OK;



    ret = MEA_API_Clear_Counters_ANALYZER(pGetEntry->unit,pGetEntry->id);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_Analyzer :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckCounterAna));

	(*pSend_bytes) = sizeof(tComPckCounterAna);

    return ret;
}

/****************************************** event group *****************************************/
MEA_Status MEA_API_Get_eventGroup_ANALYZER(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckEventGroup *pGetEntry = (tComPckEventGroup *)bufferRcv;
	tComPckEventGroup *pEntry = (tComPckEventGroup *)bufferSnd;
    MEA_Status ret = MEA_OK;

    pGetEntry->event=0;


	ret = MEA_API_Get_CC_Event_GroupId(MEA_UNIT_0,
									pGetEntry->group,
									&pGetEntry->event);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_CC_Event_GroupId :%d\r\n", pGetEntry->group);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckEventGroup));

	(*pSend_bytes) = sizeof(tComPckEventGroup);

    return ret;
}
