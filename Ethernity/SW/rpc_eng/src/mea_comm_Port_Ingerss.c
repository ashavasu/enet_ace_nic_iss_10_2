#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif




MEA_Status MEA_comm_get_ingress_port(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{

    MEA_Status ret = MEA_OK;
    tComIngressPort *pRcvIngressPort = (tComIngressPort *)bufferRcv;
    tComIngressPort *pIngressPortEntry = (tComIngressPort *)bufferSnd;

    pIngressPortEntry->ingress_port = pRcvIngressPort->ingress_port;
    pIngressPortEntry->unit = pRcvIngressPort->unit;

    ret = MEA_API_Get_IsPortValid(pRcvIngressPort->ingress_port, MEA_PORTS_TYPE_INGRESS_TYPE,MEA_FALSE);
	

    if (ret != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_comm_get_ingress_port ERROR: failed to get MEA_API_Get_IsPortValid port:%d\r\n", pRcvIngressPort->ingress_port);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return MEA_ERROR;
    }
    else
    {
        ret = MEA_API_Get_IngressPort_Entry(pIngressPortEntry->unit, pIngressPortEntry->ingress_port, &pIngressPortEntry->portIngress);

        if (ret != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_comm_get_ingress_port ERROR: failed to get MEA_API_Get_IngressPort_Entry portid:%d\r\n", pRcvIngressPort->ingress_port);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
            return ret;
        }
    }
    (*pSend_bytes) = sizeof(tComIngressPort);
    return ret;
}

MEA_Status MEA_comm_set_ingress_port(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    MEA_Status ret = MEA_OK;
    tComIngressPort *pSetIngressPortEntry = (tComIngressPort *)bufferRcv;
    tComIngressPort *pIngressPortEntry = (tComIngressPort *)bufferSnd;


    ret = MEA_API_Get_IsPortValid(pSetIngressPortEntry->ingress_port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_FALSE);
	
    if (ret != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_comm_set_ingress_port ERROR: failed to get MEA_API_Get_IsPortValid port:%d\r\n", pSetIngressPortEntry->ingress_port);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return MEA_ERROR;
    }
    else
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "MEA_API_Set_IngressPort_Entry port:%d rx_enable:%d\r\n", pSetIngressPortEntry->ingress_port, pSetIngressPortEntry->portIngress.rx_enable);
        ret = MEA_API_Set_IngressPort_Entry(pSetIngressPortEntry->unit, pSetIngressPortEntry->ingress_port, &pSetIngressPortEntry->portIngress);

        if (ret != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_comm_set_ingress_port ERROR: failed to set MEA_API_Set_IngressPort_Entry port:%d\r\n", pSetIngressPortEntry->ingress_port);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
            return ret;
        }
    }


    memcpy(pIngressPortEntry, pSetIngressPortEntry, sizeof(tComIngressPort));
    (*pSend_bytes) = sizeof(tComIngressPort);

    return ret;
}

MEA_Status MEA_comm_get_ingress_Allport(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    MEA_Status ret = MEA_OK;
    tComIngressPort *pSetIngressPortEntry = (tComIngressPort *)&bufferRcv[0];
    tComIngressPortsDb *pIngressPortEntry = (tComIngressPortsDb *)&bufferSnd[0];
    tComIngressPortId *pNextPort;
    int i = 0;

    pIngressPortEntry->number_of_ports = 0;
    pNextPort = (tComIngressPortId *)&pIngressPortEntry->next[0];


    for (i = 0; i <= 127; i++)
    {
        if (i == 120)
            continue;

        ret = MEA_API_Get_IsPortValid(i, MEA_PORTS_TYPE_INGRESS_TYPE,MEA_TRUE);
		
        if (ret == MEA_TRUE)
        {
            pIngressPortEntry->number_of_ports++;
            pNextPort->ingress_port = i;
            pNextPort->unit = pSetIngressPortEntry->unit;
            pNextPort = (tComIngressPortId *)&pNextPort->next[0];
        }
    }

    (*pSend_bytes) = sizeof(tComIngressPortsDb)+pIngressPortEntry->number_of_ports*sizeof(tComIngressPortId);

    return MEA_OK;
}

