#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif

MEA_Status MEA_API_comm_Get_Globals_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    MEA_Status ret = MEA_OK;
    tComGlobal *pRcvGlobal = (tComGlobal *)bufferRcv;
    tComGlobal *pEGlobal = (tComGlobal *)bufferSnd;

	ret = MEA_API_Get_Globals_Entry(pRcvGlobal->unit, &pRcvGlobal->entry);

	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Globals_Entry\r\n");
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}
	memcpy(pEGlobal,pRcvGlobal,sizeof(tComGlobal));
    (*pSend_bytes) = sizeof(tComGlobal);
    return ret;
}

MEA_Status MEA_API_comm_Set_Globals_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    MEA_Status ret = MEA_OK;
    tComGlobal *pRcvGlobal = (tComGlobal *)bufferRcv;
    tComGlobal *pEGlobal = (tComGlobal *)bufferSnd;

	ret = MEA_API_Set_Globals_Entry(pRcvGlobal->unit, &pRcvGlobal->entry);

	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Globals_Entry\r\n");
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}
	memcpy(pEGlobal,pRcvGlobal,sizeof(tComGlobal));
    (*pSend_bytes) = sizeof(tComGlobal);

    return ret;
}

