#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif

MEA_Status MEA_comm_get_filter_typeEntry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComFilterKey *pRcvEntry = (tComFilterKey *)bufferRcv;
	tComFilterKey *SendEntry = (tComFilterKey *)bufferSnd;
    MEA_Status ret = MEA_OK;



    ret = MEA_API_Get_Filter_KeyType(pRcvEntry->unit_i,
    										&pRcvEntry->key,
    										&pRcvEntry->keyType);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Filter_KeyType\r\n");
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }



    memcpy(SendEntry, pRcvEntry, sizeof(tComFilterKey));
    (*pSend_bytes) = sizeof(tComFilterKey);

    return ret;
}


MEA_Status MEA_comm_getfirst_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComFilterKey *pRcvEntry = (tComFilterKey *)bufferRcv;
	tComFilterKey *SendEntry = (tComFilterKey *)bufferSnd;
	MEA_Status ret = MEA_OK;

	ret = MEA_API_GetFirst_Filter (pRcvEntry->unit_i,
									&pRcvEntry->filterId,
									&pRcvEntry->found);

	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_GetFirst_Filter\r\n");
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}


	memcpy(SendEntry, pRcvEntry, sizeof(tComFilterKey));
	(*pSend_bytes) = sizeof(tComFilterKey);
    return MEA_OK;
}

MEA_Status MEA_comm_getnext_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComFilterKey *pRcvEntry = (tComFilterKey *)bufferRcv;
	tComFilterKey *SendEntry = (tComFilterKey *)bufferSnd;
	MEA_Status ret = MEA_OK;


	ret = MEA_API_GetNext_Filter (pRcvEntry->unit_i,
									&pRcvEntry->filterId,
									&pRcvEntry->found);

	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_GetNext_Filter\r\n");
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}

	memcpy(SendEntry, pRcvEntry, sizeof(tComFilterKey));
	(*pSend_bytes) = sizeof(tComFilterKey);
    return MEA_OK;
}


MEA_Status MEA_comm_get_filter_typeDataEntry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComFilterDataKey *pRcvEntry = (tComFilterDataKey *)bufferRcv;
	tComFilterDataKey *SendEntry = (tComFilterDataKey *)bufferSnd;
	MEA_Status ret = MEA_OK;


	ret = MEA_API_Get_Filter (pRcvEntry->unit,
								pRcvEntry->o_filterId,
								&pRcvEntry->key_from,
								&pRcvEntry->key_to,
								&pRcvEntry->data,
								&pRcvEntry->OutPorts_Entry,
								&pRcvEntry->Policer_entry,
								&pRcvEntry->EHP_Entry);

	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Filter\r\n");
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}

	if(pRcvEntry->EHP_Entry.num_of_entries > 0)
	{
		memcpy(pRcvEntry->EHP_Entry.ehp_info,&pRcvEntry->ehp_info,sizeof(MEA_EHP_Info_dbt));
	}


	memcpy(SendEntry, pRcvEntry, sizeof(tComFilterDataKey));
	(*pSend_bytes) = sizeof(tComFilterDataKey);
    return MEA_OK;
}

MEA_Status MEA_comm_create_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComFilterDataKey *pRcvEntry = (tComFilterDataKey *)bufferRcv;
	tComFilterDataKey *SendEntry = (tComFilterDataKey *)bufferSnd;
	MEA_Status ret = MEA_OK;

	if(pRcvEntry->EHP_Entry.num_of_entries > 0)
	{
		memcpy(pRcvEntry->EHP_Entry.ehp_info,&pRcvEntry->ehp_info,sizeof(MEA_EHP_Info_dbt));

	}

	ret = MEA_API_Create_Filter (pRcvEntry->unit,
								&pRcvEntry->key_from,
								NULL,
								&pRcvEntry->data,
								&pRcvEntry->OutPorts_Entry,
								&pRcvEntry->Policer_entry,
								&pRcvEntry->EHP_Entry,
								&pRcvEntry->o_filterId);


	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Create_Filter\r\n");
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}

	memcpy(SendEntry, pRcvEntry, sizeof(tComFilterDataKey));
	(*pSend_bytes) = sizeof(tComFilterDataKey);
    return MEA_OK;
}

MEA_Status MEA_comm_set_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComFilterDataKey *pRcvEntry = (tComFilterDataKey *)bufferRcv;
	tComFilterDataKey *SendEntry = (tComFilterDataKey *)bufferSnd;
	MEA_Status ret = MEA_OK;

	if(pRcvEntry->EHP_Entry.num_of_entries > 0)
	{
		memcpy(pRcvEntry->EHP_Entry.ehp_info,&pRcvEntry->ehp_info,sizeof(MEA_EHP_Info_dbt));

	}

	ret = MEA_API_Set_Filter (pRcvEntry->unit,
								pRcvEntry->o_filterId,
								&pRcvEntry->data,
								&pRcvEntry->OutPorts_Entry,
								&pRcvEntry->Policer_entry,
								&pRcvEntry->EHP_Entry);

	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_comm_set_filter_Entry\r\n");
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}

	memcpy(SendEntry, pRcvEntry, sizeof(tComFilterDataKey));
	(*pSend_bytes) = sizeof(tComFilterDataKey);
    return MEA_OK;
}

MEA_Status MEA_comm_delete_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComFilterDataKey *pRcvEntry = (tComFilterDataKey *)bufferRcv;
	tComFilterDataKey *SendEntry = (tComFilterDataKey *)bufferSnd;
	MEA_Status ret = MEA_OK;


	ret = MEA_API_Delete_Filter (pRcvEntry->unit,
								pRcvEntry->o_filterId);

	if (ret != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_comm_delete_filter_Entry\r\n");
		(*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
		return ret;
	}

	memcpy(SendEntry, pRcvEntry, sizeof(tComFilterDataKey));
	(*pSend_bytes) = sizeof(tComFilterDataKey);
    return MEA_OK;
}

