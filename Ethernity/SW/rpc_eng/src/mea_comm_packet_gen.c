#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif

MEA_Status MEA_comm_PacketGen_getRange_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status ret = MEA_OK;

	return ret;
}
MEA_Status MEA_comm_PacketGen_get_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenProfile *pGetEntry = (tComPckGenProfile *)bufferRcv;
	tComPckGenProfile *pEntry = (tComPckGenProfile *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));

    ret = MEA_API_Get_PacketGen_Profile_info_Entry(pGetEntry->unit,
    													pGetEntry->id,
    													&pGetEntry->entry);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenProfile));

	(*pSend_bytes) = sizeof(tComPckGenProfile);

    return ret;
}


MEA_Status MEA_comm_PacketGen_getfirst_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenProfile *pGetEntry = (tComPckGenProfile *)bufferRcv;
	tComPckGenProfile *pEntry = (tComPckGenProfile *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));

    ret = MEA_API_GetFirst_PacketGen_Profile_info_Entry(pGetEntry->unit,
    													&pGetEntry->id,
    													&pGetEntry->entry,
    													&pGetEntry->found);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_GetFirst_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenProfile));

	(*pSend_bytes) = sizeof(tComPckGenProfile);

    return ret;
}

MEA_Status MEA_comm_PacketGen_getnext_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenProfile *pGetEntry = (tComPckGenProfile *)bufferRcv;
	tComPckGenProfile *pEntry = (tComPckGenProfile *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));

    ret = MEA_API_GetNext_PacketGen_Profile_info_Entry(pGetEntry->unit,
    													&pGetEntry->id,
    													&pGetEntry->entry,
    													&pGetEntry->found);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_GetNext_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenProfile));

	(*pSend_bytes) = sizeof(tComPckGenProfile);

    return ret;
}

MEA_Status MEA_comm_PacketGen_create_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenProfile *pGetEntry = (tComPckGenProfile *)bufferRcv;
	tComPckGenProfile *pEntry = (tComPckGenProfile *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));

    ret = MEA_API_Create_PacketGen_Profile_info(pGetEntry->unit,
    													&pGetEntry->entry,
    													&pGetEntry->id);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Create_PacketGen_Profile_info :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_CREATE_COM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenProfile));

	(*pSend_bytes) = sizeof(tComPckGenProfile);

    return ret;
}

MEA_Status MEA_comm_PacketGen_set_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenProfile *pGetEntry = (tComPckGenProfile *)bufferRcv;
	tComPckGenProfile *pEntry = (tComPckGenProfile *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));

    ret = MEA_API_Set_PacketGen_Profile_info_Entry(pGetEntry->unit,
    													pGetEntry->id,
    													&pGetEntry->entry);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenProfile));

	(*pSend_bytes) = sizeof(tComPckGenProfile);

    return ret;
}

MEA_Status MEA_comm_PacketGen_delete_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenProfile *pGetEntry = (tComPckGenProfile *)bufferRcv;
	tComPckGenProfile *pEntry = (tComPckGenProfile *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));

    ret = MEA_API_Delete_PacketGen_Profile_info_Entry(pGetEntry->unit,
    													pGetEntry->id);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Delete_PacketGen_Profile_info_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenProfile));

	(*pSend_bytes) = sizeof(tComPckGenProfile);

    return ret;
}


/*******************************************************************************
* packet gen
*
 ******************************************************************************/
MEA_Status MEA_comm_PacketGen_getRange_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	MEA_Status ret = MEA_OK;

	return ret;
}
MEA_Status MEA_comm_PacketGen_get_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenEntry *pGetEntry = (tComPckGenEntry *)bufferRcv;
	tComPckGenEntry *pEntry = (tComPckGenEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_Entry_dbt));

    ret = MEA_API_Get_PacketGen_Entry(pGetEntry->unit,
    													pGetEntry->id,
    													&pGetEntry->entry);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_PacketGen_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenEntry));

	(*pSend_bytes) = sizeof(tComPckGenEntry);

    return ret;
}


MEA_Status MEA_comm_PacketGen_getfirst_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenEntry *pGetEntry = (tComPckGenEntry *)bufferRcv;
	tComPckGenEntry *pEntry = (tComPckGenEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_Entry_dbt));

    ret = MEA_API_GetFirst_PacketGen_Entry(pGetEntry->unit,
    													&pGetEntry->id,
    													&pGetEntry->entry,
    													&pGetEntry->found);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_GetFirst_PacketGen_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenEntry));

	(*pSend_bytes) = sizeof(tComPckGenEntry);

    return ret;
}

MEA_Status MEA_comm_PacketGen_getnext_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenEntry *pGetEntry = (tComPckGenEntry *)bufferRcv;
	tComPckGenEntry *pEntry = (tComPckGenEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_Entry_dbt));

    ret = MEA_API_GetNext_PacketGen_Entry(pGetEntry->unit,
    													&pGetEntry->id,
    													&pGetEntry->entry,
    													&pGetEntry->found);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_GetNext_PacketGen_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenEntry));

	(*pSend_bytes) = sizeof(tComPckGenEntry);

    return ret;
}

MEA_Status MEA_comm_PacketGen_create_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenEntry *pGetEntry = (tComPckGenEntry *)bufferRcv;
	tComPckGenEntry *pEntry = (tComPckGenEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


//    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_Entry_dbt));

    ret = MEA_API_Create_PacketGen_Entry(pGetEntry->unit,
    													&pGetEntry->entry,
    													&pGetEntry->id);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Create_PacketGen_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_CREATE_COM_ERR;
        return ret;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "create MEA_API_Create_PacketGen_Entry id:%d sizeof:%d\r\n", pGetEntry->id,sizeof(tComPckGenEntry));

	memcpy(pEntry, pGetEntry, sizeof(tComPckGenEntry));

	(*pSend_bytes) = sizeof(tComPckGenEntry);

    return ret;
}


MEA_Status MEA_comm_PacketGen_set_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenEntry *pGetEntry = (tComPckGenEntry *)bufferRcv;
	tComPckGenEntry *pEntry = (tComPckGenEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_Entry_dbt));

    ret = MEA_API_Set_PacketGen_Entry(pGetEntry->unit,
    													pGetEntry->id,
    													&pGetEntry->entry);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_PacketGen_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenEntry));

	(*pSend_bytes) = sizeof(tComPckGenEntry);

    return ret;
}

MEA_Status MEA_comm_PacketGen_delete_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenEntry *pGetEntry = (tComPckGenEntry *)bufferRcv;
	tComPckGenEntry *pEntry = (tComPckGenEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_Entry_dbt));

    ret = MEA_API_Delete_PacketGen_Entry(pGetEntry->unit,
    													pGetEntry->id);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Delete_PacketGen_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenEntry));

	(*pSend_bytes) = sizeof(tComPckGenEntry);

    return ret;
}

MEA_Status MEA_comm_PacketGen_deleteAll_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
	tComPckGenEntry *pGetEntry = (tComPckGenEntry *)bufferRcv;
	tComPckGenEntry *pEntry = (tComPckGenEntry *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_PacketGen_Entry_dbt));

    ret = MEA_API_Delete_PacketGen_All_Entry(pGetEntry->unit);


    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Delete_PacketGen_Entry :%d\r\n", pGetEntry->id);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckGenEntry));

	(*pSend_bytes) = sizeof(tComPckGenEntry);

    return ret;
}
