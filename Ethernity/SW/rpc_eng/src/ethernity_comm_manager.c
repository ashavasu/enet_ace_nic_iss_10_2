#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_drv_common.h"
#include "mea_comm_msg.h"
#include "mea_comm.h"
#include "mea_policer_profile_drv.h"

/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif



#ifdef MEA_RPC_ENG
//#endif



#ifdef MEA_OS_LINUX
static pthread_t       ENET_thread_comm_manager;
#endif











tFuncMessage messageDb[] = {
		{D_CLASS_INGRESS_PORT,D_MSG_GET_INGRESS_CONFIGURATION,MEA_comm_get_ingress_port},
		{D_CLASS_INGRESS_PORT,D_MSG_SET_INGRESS_CONFIGURATION,MEA_comm_set_ingress_port},
        { D_CLASS_INGRESS_PORT, D_MSG_GET_INGRESS_PORTS_INFO, MEA_comm_get_ingress_Allport},

		{D_CLASS_EGRESS_PORT,D_MSG_GET_EGRESS_CONFIGURATION,MEA_comm_get_egress_port},
		{D_CLASS_EGRESS_PORT,D_MSG_SET_EGRESS_CONFIGURATION,MEA_comm_set_egress_port},

		{D_CLASS_POLICER_PROFILE,D_MSG_CREATE_POLICER_PROFILE,MEA_comm_create_policer_profile},
		{D_CLASS_POLICER_PROFILE,D_MSG_GET_POLICER_PROFILE,MEA_comm_get_policer_profile},
		{D_CLASS_POLICER_PROFILE,D_MSG_GET_ALL_POLICER_IDS,MEA_comm_getall_policer_profile},
		{D_CLASS_POLICER_PROFILE,D_MSG_DELETE_POLICER_PROFILE,MEA_comm_delete_policer_profile},
		{D_CLASS_POLICER_PROFILE,D_MSG_SET_POLICER_PROFILE,MEA_comm_set_policer_profile},

		{D_CLASS_SERVICE_ID,D_MSG_CREATE_SERVICE,MEA_comm_create_service_id},
		{D_CLASS_SERVICE_ID,D_MSG_SET_SERVICE,MEA_comm_set_service_id},
		{D_CLASS_SERVICE_ID,D_MSG_GET_SERVICE,MEA_comm_get_service_id},
		{D_CLASS_SERVICE_ID,D_MSG_GET_ALL_SERVICES,MEA_comm_getall_service_id},
		{D_CLASS_SERVICE_ID,D_MSG_DELETE_SERVICE,MEA_comm_delete_service_id},
		{D_CLASS_SERVICE_ID,D_MSG_GETRANGE_SERVICE,MEA_comm_getrange_services},

		{D_CLASS_ACTION_ID,D_MSG_CREATE_ACTION,MEA_comm_create_action_id},
		{D_CLASS_ACTION_ID,D_MSG_SET_ACTION,MEA_comm_set_action_id},
		{D_CLASS_ACTION_ID,D_MSG_GET_ACTION,MEA_comm_get_action_id},
		{D_CLASS_ACTION_ID,D_MSG_GET_ALL_ACTIONS,MEA_comm_getall_action_id},
		{D_CLASS_ACTION_ID,D_MSG_DELETE_ACTION,MEA_comm_delete_action_id},
		{D_CLASS_ACTION_ID,D_MSG_GETFIRST_ACTION,MEA_comm_getfirst_action_id},
		{D_CLASS_ACTION_ID,D_MSG_GETNEXT_ACTION,MEA_comm_getnext_action_id},
		{D_CLASS_ACTION_ID,D_MSG_GETRANGE_ACTION,MEA_comm_getrange_actions},

        {D_CLASS_PM_ID, D_MSG_GET_PMID, MEA_comm_get_Pm_id },
        {D_CLASS_PM_ID, D_MSG_GETFIRST_PM, MEA_comm_get_first_Pm_id },
        {D_CLASS_PM_ID, D_MSG_GETNEXT_PM, MEA_comm_get_next_Pm_id },
        {D_CLASS_PM_ID, D_MSG_CLEAR_ALL_PM ,MEA_comm_clear_All_Pm},
        {D_CLASS_PM_ID, D_MSG_CLEAR_PM , MEA_comm_clear_Pm_id},
        {D_CLASS_PM_ID, D_MSG_GET_RANGE_PM, MEA_comm_get_range_Pm_id},


		{D_CLASS_RMON_ID, D_MSG_GET_RMON, MEA_comm_get_RMON_id },
		{D_CLASS_RMON_ID, D_MSG_GETFIRST_RMON, MEA_comm_get_first_RMON_id },
		{D_CLASS_RMON_ID, D_MSG_GETNEXT_RMON, MEA_comm_get_next_RMON_id },
		{D_CLASS_RMON_ID, D_MSG_CLEAR_RMON ,MEA_comm_clear_RMON_id},
		{D_CLASS_RMON_ID, D_MSG_CLEAR_ALL_RMON , MEA_comm_clear_All_RMON},

        {D_CLASS_FORWARDER_ID,D_MSG_GET_FORWARDER,MEA_comm_Get_SE_Entry},
        {D_CLASS_FORWARDER_ID,D_MSG_GETFIRST_FORWARDER,MEA_comm_GetFirst_SE_Entry},
        {D_CLASS_FORWARDER_ID,D_MSG_GETNEXT_FORWARDER,MEA_comm_GetNext_SE_Entry},
        {D_CLASS_FORWARDER_ID,D_MSG_CREATE_FORWARDER,MEA_comm_Create_SE_Entry},
        {D_CLASS_FORWARDER_ID,D_MSG_SET_FORWARDER,MEA_comm_Set_SE_Entry},
        {D_CLASS_FORWARDER_ID,D_MSG_DELETE_FORWARDER,MEA_comm_Delete_SE_Entry},
        {D_CLASS_FORWARDER_ID,D_MSG_CLEAR_ALL_FORWARDER,MEA_comm_clearall_SE_Entry},
        {D_CLASS_FORWARDER_ID,D_MSG_GETRANGE_FORWARDER,MEA_comm_GetRange_SE_Entry},


        {D_CLASS_PACKETGEN_PROFILE,D_MSG_GET_PACKETGEN_PROFILE,MEA_comm_PacketGen_get_Profile_info_Entry},
        {D_CLASS_PACKETGEN_PROFILE,D_MSG_GETFIRST_PACKETGEN_PROFILE,MEA_comm_PacketGen_getfirst_Profile_info_Entry},
        {D_CLASS_PACKETGEN_PROFILE,D_MSG_GETNEXT_PACKETGEN_PROFILE,MEA_comm_PacketGen_getnext_Profile_info_Entry},
        {D_CLASS_PACKETGEN_PROFILE,D_MSG_CREATE_PACKETGEN_PROFILE,MEA_comm_PacketGen_create_Profile_info_Entry},
        {D_CLASS_PACKETGEN_PROFILE,D_MSG_SET_PACKETGEN_PROFILE,MEA_comm_PacketGen_set_Profile_info_Entry},
        {D_CLASS_PACKETGEN_PROFILE,D_MSG_DELETE_PACKETGEN_PROFILE,MEA_comm_PacketGen_delete_Profile_info_Entry},
        {D_CLASS_PACKETGEN_PROFILE,D_MSG_GETRANGE_PACKETGEN_PROFILE,MEA_comm_PacketGen_getRange_Profile_info_Entry},

        {D_CLASS_PACKETGEN_ENTRY,D_MSG_GET_PACKETGEN_ENTRY,MEA_comm_PacketGen_get_info_Entry},
        {D_CLASS_PACKETGEN_ENTRY,D_MSG_GETFIRST_PACKETGEN_ENTRY,MEA_comm_PacketGen_getfirst_info_Entry},
        {D_CLASS_PACKETGEN_ENTRY,D_MSG_GETNEXT_PACKETGEN_ENTRY,MEA_comm_PacketGen_getnext_info_Entry},
        {D_CLASS_PACKETGEN_ENTRY,D_MSG_CREATE_PACKETGEN_ENTRY,MEA_comm_PacketGen_create_info_Entry},
        {D_CLASS_PACKETGEN_ENTRY,D_MSG_SET_PACKETGEN_ENTRY,MEA_comm_PacketGen_set_info_Entry},
        {D_CLASS_PACKETGEN_ENTRY,D_MSG_DELETE_PACKETGEN_ENTRY,MEA_comm_PacketGen_delete_info_Entry},
        {D_CLASS_PACKETGEN_ENTRY,D_MSG_GETRANGE_PACKETGEN_ENTRY,MEA_comm_PacketGen_getRange_info_Entry},

        {D_CLASS_PACKETCCM_ENTRY,D_MSG_GET_PACKETCCM_ENTRY,MEA_comm_get_ccm_info_Entry},
        {D_CLASS_PACKETCCM_ENTRY,D_MSG_GETFIRST_PACKETCCM_ENTRY,MEA_comm_getfirst_ccm_info_Entry},
        {D_CLASS_PACKETCCM_ENTRY,D_MSG_GETNEXT_PACKETCCM_ENTRY,MEA_comm_getnext_ccm_info_Entry},
        {D_CLASS_PACKETCCM_ENTRY,D_MSG_CREATE_PACKETCCM_ENTRY,MEA_comm_create_ccm_info_Entry},
        {D_CLASS_PACKETCCM_ENTRY,D_MSG_SET_PACKETCCM_ENTRY,MEA_comm_set_ccm_info_Entry},
        {D_CLASS_PACKETCCM_ENTRY,D_MSG_DELETE_PACKETCCM_ENTRY,MEA_comm_delete_ccm_info_Entry},
        {D_CLASS_PACKETCCM_ENTRY,D_MSG_GETRANGE_PACKETCCM_ENTRY,MEA_comm_getRange_ccm_info_Entry},



        {D_CLASS_PACKET_ANALYZER_ENTRY,D_MSG_GET_PACKET_ANALYZER_ENTRY,MEA_comm_get_analyzer_info_Entry},
//        {D_CLASS_PACKET_ANALYZER_ENTRY,D_MSG_GETFIRST_PACKET_ANALYZER_ENTRY,MEA_comm_getfirst_ccm_info_Entry},
//        {D_CLASS_PACKET_ANALYZER_ENTRY,D_MSG_GETNEXT_PACKET_ANALYZER_ENTRY,MEA_comm_getnext_ccm_info_Entry},
//        {D_CLASS_PACKET_ANALYZER_ENTRY,D_MSG_CREATE_PACKET_ANALYZER_ENTRY,MEA_comm_create_ccm_info_Entry},
        {D_CLASS_PACKET_ANALYZER_ENTRY,D_MSG_SET_PACKET_ANALYZER_ENTRY,MEA_comm_set_analyzer_info_Entry},
//        {D_CLASS_PACKET_ANALYZER_ENTRY,D_MSG_DELETE_PACKET_ANALYZER_ENTRY,MEA_comm_delete_ccm_info_Entry},
//        {D_CLASS_PACKET_ANALYZER_ENTRY,D_MSG_GETRANGE_PACKET_ANALYZER_ENTRY,MEA_comm_getRange_ccm_info_Entry},

        {D_CLASS_CCM_COUNTER_ENTRY,D_MSG_GET_CCM_COUNTER_ENTRY,MEA_comm_get_CCM_id},
        {D_CLASS_CCM_COUNTER_ENTRY,D_MSG_CLR_CCM_COUNTER_ENTRY,MEA_comm_clean_CCM_id},

        {D_CLASS_LM_COUNTER_ENTRY,D_MSG_GET_LM_COUNTER_ENTRY,MEA_comm_Get_Counters_LM},
        {D_CLASS_LM_COUNTER_ENTRY,D_MSG_CLR_LM_COUNTER_ENTRY,MEA_comm_Clear_Counters_LM},

    
    /************************************************************************/
    /* Global GW                                                            */
    /************************************************************************/
           
        { D_CLASS_GW_GLOBAL_ENTRY, D_MSG_SET_GW_GLOBAL_ENTRY, MEA_comm_mea_api_Set_GW_Global_Entry },
        { D_CLASS_GW_GLOBAL_ENTRY, D_MSG_GET_GW_GLOBAL_ENTRY, MEA_comm_mea_api_Get_GW_Global_Entry },

        { D_CLASS_ROOT_FILTER_ENTRY, D_MSG_SET_ROOT_FILTER_ENTRY, MEA_comm_mea_api_Set_RootFilter_Entry },
        { D_CLASS_ROOT_FILTER_ENTRY, D_MSG_GET_ROOT_FILTER_ENTRY, MEA_comm_mea_api_Get_RootFilter_Entry },

        { D_CLASS_GLOBAL_ENTRY, D_MSG_SET_GLOBAL_ENTRY, MEA_API_comm_Set_Globals_Entry },
        { D_CLASS_GLOBAL_ENTRY, D_MSG_GET_GLOBAL_ENTRY, MEA_API_comm_Get_Globals_Entry },

        { D_CLASS_ANALYZER_COUNTER_ENTRY, D_MSG_GET_ANALYZER_COUNTER_ENTRY, MEA_API_Get_comm_Counters_ANALYZER },
        { D_CLASS_ANALYZER_COUNTER_ENTRY, D_MSG_CLR_ANALYZER_COUNTER_ENTRY, MEA_API_Clear_comm_Counters_ANALYZER },

        {D_CLASS_EVENT_GROUP_ENTRY,D_MSG_GET_EVENT_GROUP_ENTRY,MEA_API_Get_eventGroup_ANALYZER},

        /************************************************************************/
        /* FILTER                                                            */
        /************************************************************************/
        {D_CLASS_FILTER_ENTRY,D_MSG_GET_FILTER_ENTRY,MEA_comm_get_filter_typeEntry},
        {D_CLASS_FILTER_ENTRY,D_MSG_GETFIRST_FILTER_ENTRY,MEA_comm_getfirst_filter_Entry},
        {D_CLASS_FILTER_ENTRY,D_MSG_GETNEXT_FILTER_ENTRY,MEA_comm_getnext_filter_Entry},
        {D_CLASS_FILTER_ENTRY,D_MSG_CREATE_FILTER_ENTRY,MEA_comm_create_filter_Entry},
        {D_CLASS_FILTER_ENTRY,D_MSG_SET_FILTER_ENTRY,MEA_comm_set_filter_Entry},
        {D_CLASS_FILTER_ENTRY,D_MSG_DELETE_FILTER_ENTRY,MEA_comm_delete_filter_Entry},
        {D_CLASS_FILTER_ENTRY,D_MSG_GET_FILTER_DATA_ENTRY,MEA_comm_get_filter_typeDataEntry},


};


MEA_Status MEA_comm_msg_parse(char *bufferRcv,int received_bytes,char *bufferSnd,int *pSend_bytes)
{
	int i=0;
	MEA_Status ret = MEA_OK;
	unsigned int res_status;
	tMsgBody *pBodyRcv = (tMsgBody *)bufferRcv;
	tMsgBody *pBodySend = (tMsgBody *)bufferSnd;
	int body_bytes=0;
	unsigned int found=0;

	memcpy(pBodySend,pBodyRcv,sizeof(tMsgBody));
	(*pSend_bytes) = sizeof(tMsgBody);
	for(i=0;i<sizeof(messageDb)/sizeof(messageDb[0]);i++)
	{
		if( (pBodyRcv->hdr.class_level_type == messageDb[i].class_level_type) && (pBodyRcv->hdr.message_level_type == messageDb[i].message_level_type) )
		{
			found=1;
			MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"cls_lvl=%d type_lvl=%d\r\n",messageDb[i].class_level_type,messageDb[i].message_level_type);
			ret = messageDb[i].callback_function(&pBodyRcv->info[0],
													received_bytes-sizeof(tMsgBody),
													&pBodySend->info[0],
													&body_bytes,
													&res_status);
			//memcpy(pBodySend,pBodyRcv,sizeof(tMsgBody));
			pBodySend->hdr.ret = ret;
			pBodySend->hdr.message_result=res_status;
			if(ret != MEA_OK)
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"action class=%d msgId=%d request failed res=0x%x\r\n",
						pBodyRcv->hdr.class_level_type,
						pBodyRcv->hdr.message_level_type,
						pBodySend->hdr.message_result);
			}
			else
			{
				(*pSend_bytes) += body_bytes;

			}
			break;
		}
	}
	if(found == 0)
	{
		pBodySend->hdr.ret = MEA_ERROR;
		pBodySend->hdr.message_result=D_MSG_ID_NOT_FOUND_COMM_ERR;

		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"action class=%d msgId=%d not found\r\n",
											pBodyRcv->hdr.class_level_type,
												pBodyRcv->hdr.message_level_type);
	}

	return ret;

}
#if defined(MEA_OS_LINUX)
void*  ENET_thread_comm_mgr(void* arg)
#else
DWORD WINAPI  ENET_thread_comm_mgr(void* arg)
#endif
{
#if defined(MEA_OS_LINUX)
    socklen_t clientlen;

#else
    ENET_OS_socket_t clientlen;
#endif

	int sock;
	struct sockaddr_in server;
	struct sockaddr_in client;
	int serverlen;
	
	fd_set readSet ;
	struct timeval tv;


	char bufferRcv[BUFFER_SIZE];
	char bufferSnd[BUFFER_SIZE];
	int received,sendlen;
	int ret_code=0;
   

	// create UDP socket
	if((sock = socket(PF_INET, SOCK_DGRAM,IPPROTO_UDP)) < 0)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"ERROR: failed to create socket");
		return 0;
	}

	memset(&server,0,sizeof(server));

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = (D_COMMMESSAGE_UDP_SOCKET);

	//setting to client parameters in case send the packet first
//	client.sin_family = AF_INET;
//	client.sin_addr.s_addr = inet_addr("10.100.1.1");
//	client.sin_port = htonl(2000);

	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"socket function pid=%d",getpid());

	//bind the socket
	serverlen = sizeof(struct sockaddr_in);
	if((ret_code = bind(sock,(struct sockaddr *) &server,serverlen)) < 0)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"ERROR: failed to bind server socket ret_code %d",ret_code);
		return 0;
	}

    memset(&readSet, 0, sizeof(fd_set));

	while(1)
	{

		FD_SET(sock,&readSet);

		tv.tv_sec = 5;
		tv.tv_usec = 0;

		if(select(sock+1,&readSet,NULL,NULL,&tv) >= 0)
		{
			// msg received
			if(FD_ISSET(sock,&readSet))
			{
				// received packet
				clientlen = sizeof(client);

				if((received = recvfrom(sock,bufferRcv,BUFFER_SIZE,0,(struct sockaddr *)&client,&clientlen)) < 0)
				{
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"ERROR: failed to received message");
					continue;
				}

				sendlen=0;
				MEA_comm_msg_parse(bufferRcv,received,bufferSnd,&sendlen);

				//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"send:%d bytes\n",sendlen);

				// check the packet and send to other side
				if(sendto(sock,bufferSnd,sendlen,0,(struct sockaddr *)&client,sizeof(client)) != sendlen)
				{
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"ERROR: failed sending packet");
				}
				else
				{
					MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"send packet sizeof:%d",sendlen);
				}
			}
		}

	} // end while 1

	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "messaging thread ended");
	return 0;
}

void MEA_create_comm_thread(void)
{
#ifdef MEA_OS_LINUX
	if (pthread_create (&ENET_thread_comm_manager,
							NULL,
							ENET_thread_comm_mgr,
							NULL)!=ENET_OK)
	{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "rpc thread creation failed\n");
	}
#else
   // LPDWORD  thread_id;
    //ENET_OS_THREAD_CREATE(p_thread_id, func, param)

 //  ENET_OS_THREAD_CREATE(thread_id, ENET_thread_comm_mgr, NULL);
                   



#endif
}

#endif
