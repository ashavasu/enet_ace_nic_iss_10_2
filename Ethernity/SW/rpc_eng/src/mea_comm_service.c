#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif




MEA_Status MEA_comm_create_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComService *pSetServiceEntry = (tComService *)bufferRcv;
    tComService *pServiceEntry = (tComService *)bufferSnd;
    MEA_Policer_Entry_dbt          policerEntry;
    MEA_EgressHeaderProc_Array_Entry_dbt   EHP_Entry;
    MEA_Status ret = MEA_OK;
    MEA_Service_Entry_Key_dbt       		*pkey = NULL;
    MEA_Service_Entry_Data_dbt      		*pdata = NULL;


    // check if policer found
    ret = MEA_API_Get_Policer_ACM_Profile(pSetServiceEntry->unit,
        pSetServiceEntry->policer_id,
        pSetServiceEntry->ACM_Mode,
        &policerEntry);
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Policer_ACM_Profile :%d\r\n", pSetServiceEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }

    if (pSetServiceEntry->key_valid == 1)
    {
        pkey = (MEA_Service_Entry_Key_dbt *)&pSetServiceEntry->key;
    }
    if (pSetServiceEntry->data_valid == 1)
    {
        pdata = (MEA_Service_Entry_Data_dbt *)&pSetServiceEntry->data;
    }

    EHP_Entry.num_of_entries = pSetServiceEntry->num_of_entries;
    EHP_Entry.ehp_info = (MEA_EHP_Info_dbt *)&pSetServiceEntry->ehp_info[0];

    ret = MEA_API_Create_Service(pSetServiceEntry->unit,
        pkey,
        pdata,
        &pSetServiceEntry->OutPorts_Entry,
        &policerEntry,
        &EHP_Entry,
        &pSetServiceEntry->serviceId);
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Create_Service :%d\r\n", pSetServiceEntry->serviceId);
        (*pResult) = D_MSG_ENTRY_CANNOT_CREATE_COM_ERR;
        return ret;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "created MEA_API_Create_Service id:%d\r\n", pSetServiceEntry->serviceId);

    memcpy(pServiceEntry, pSetServiceEntry, sizeof(tComService));
    (*pSend_bytes) = sizeof(tComService);

    return ret;
}

MEA_Status MEA_comm_set_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComService *pSetServiceEntry = (tComService *)bufferRcv;
    tComService *pServiceEntry = (tComService *)bufferSnd;
    MEA_Policer_Entry_dbt          policerEntry;
    MEA_EgressHeaderProc_Array_Entry_dbt   EHP_Entry;
    MEA_Status ret = MEA_OK;
    MEA_Service_Entry_Data_dbt      		*pdata = NULL;
    MEA_OutPorts_Entry_dbt					*pOutput = NULL;

    // check if policer found
    ret = MEA_API_Get_Policer_ACM_Profile(pSetServiceEntry->unit,
        pSetServiceEntry->policer_id,
        pSetServiceEntry->ACM_Mode,
        &policerEntry);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_Policer_ACM_Profile :%d\r\n", pSetServiceEntry->policer_id);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }
    if (pSetServiceEntry->data_valid == 1)
    {
        pdata = (MEA_Service_Entry_Data_dbt *)&pSetServiceEntry->data;
    }
    if (pSetServiceEntry->outport_valid == 1)
    {
        pOutput = (MEA_OutPorts_Entry_dbt *)&pSetServiceEntry->OutPorts_Entry;
    }

    EHP_Entry.num_of_entries = pSetServiceEntry->num_of_entries;
    EHP_Entry.ehp_info = (MEA_EHP_Info_dbt *)&pSetServiceEntry->ehp_info[0];

    ret = MEA_API_Set_Service(pSetServiceEntry->unit,
        pSetServiceEntry->serviceId,
        pdata,
        pOutput,
        &policerEntry,
        &EHP_Entry);
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_Service :%d\r\n", pSetServiceEntry->serviceId);
        (*pResult) = D_MSG_ENTRY_CANNOT_SET_COM_ERR;
        return ret;
    }


    memcpy(pServiceEntry, pSetServiceEntry, sizeof(tComService));
    (*pSend_bytes) = sizeof(tComService);

    return ret;
}

MEA_Status  MEA_comm_get_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    MEA_Policer_Entry_dbt   Policer_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt  EHP_Entry;
    tComService *pServiceEntry = (tComService *)bufferRcv;
    tComService *pGetServiceEntry = (tComService *)bufferSnd;
    MEA_Status ret = MEA_OK;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "sizeof serviceCom :%d\r\n", sizeof(tComService));

    MEA_OS_memset(&pServiceEntry->key, 0, sizeof(MEA_Service_Entry_Key_dbt));
    MEA_OS_memset(&pServiceEntry->data, 0, sizeof(MEA_Service_Entry_Data_dbt));
    MEA_OS_memset(&pServiceEntry->OutPorts_Entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
    MEA_OS_memset(&Policer_entry, 0, sizeof(MEA_Policer_Entry_dbt));
    MEA_OS_memset(&EHP_Entry, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));

    ret = MEA_API_Get_Service(pServiceEntry->unit,
        pServiceEntry->serviceId,
        &pServiceEntry->key,
        &pServiceEntry->data,
        &pServiceEntry->OutPorts_Entry,
        &Policer_entry,
        &EHP_Entry);
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Service :%d\r\n", pServiceEntry->serviceId);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "get first time serviceCom serviceId:%d num of entries :%d\r\n", pServiceEntry->serviceId, EHP_Entry.num_of_entries);

    if (EHP_Entry.num_of_entries > 0)
    {
        EHP_Entry.ehp_info = (MEA_EHP_Info_dbt *)&pServiceEntry->ehp_info[0];

        ret = MEA_API_Get_Service(pServiceEntry->unit,
            pServiceEntry->serviceId,
            &pServiceEntry->key,
            &pServiceEntry->data,
            &pServiceEntry->OutPorts_Entry,
            &Policer_entry,
            &EHP_Entry);
        if (ret != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Service :%d\r\n", pServiceEntry->serviceId);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
            return ret;
        }
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "get second time serviceCom serviceId:%d\r\n", pServiceEntry->serviceId);
    }
    pServiceEntry->num_of_clusters = 255;



    memcpy(pGetServiceEntry, pServiceEntry, sizeof(tComService));
    pGetServiceEntry->num_of_entries = EHP_Entry.num_of_entries;
    pGetServiceEntry->policer_id = pServiceEntry->data.policer_prof_id;
    (*pSend_bytes) = sizeof(tComService);


    return ret;
}

MEA_Status MEA_comm_getrange_services(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComService *pSetServiceEntry = (tComService *)bufferRcv;
    tComIndexDb *pRetEntry = (tComIndexDb *)&bufferSnd[0];
    tComIngressPortId *pNextPort;
    MEA_ULong_t    		valid;
    MEA_Service_t 		serviceId;
    MEA_Status 			ret = MEA_OK;

    if(pSetServiceEntry->serviceId == 0)
    {
    	//start from get first
	   if (MEA_API_GetFirst_Service(pSetServiceEntry->unit,
			&serviceId,
			&valid) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "service db is empty:%d\r\n", pSetServiceEntry->unit);
			return MEA_OK;
		}
    }
    else
    {
    	serviceId = pSetServiceEntry->serviceId;
    	ret = MEA_API_GetNext_Service(pSetServiceEntry->unit,
             &serviceId,
             &valid);

    	if(ret != MEA_OK)
         {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to get MEA_API_Get_Service :%d\r\n", pSetServiceEntry->serviceId);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;;
			return ret;
         }
    }
    pRetEntry->number_of_ports=0;
    pNextPort = (tComIngressPortId *)&pRetEntry->next[0];
    while ( (valid) && (pRetEntry->number_of_ports < 1000) )
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "serviceId:%d\r\n", serviceId);
    	pRetEntry->number_of_ports++;
        pNextPort->ingress_port = serviceId;
        pNextPort->unit = pSetServiceEntry->unit;
        pNextPort = (tComIngressPortId *)&pNextPort->next[0];

		if (MEA_API_GetNext_Service(pSetServiceEntry->unit,&serviceId,&valid) != MEA_OK)
		{
			break;
		}
    }
    (*pSend_bytes) = sizeof(tComIndexDb)+pRetEntry->number_of_ports*sizeof(tComIndex);

    return MEA_OK;
}

MEA_Status MEA_comm_getall_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComService *pSetServiceEntry = (tComService *)bufferRcv;
    tComIndexDb *pRetPolicerEntry = (tComIndexDb *)&bufferSnd[0];
    tComIngressPortId *pNextPort;
    MEA_ULong_t    valid;
    MEA_Service_t serviceId;


    pRetPolicerEntry->number_of_ports = 0;
    pNextPort = (tComIngressPortId *)&pRetPolicerEntry->next[0];

    if (MEA_API_GetFirst_Service(pSetServiceEntry->unit,
        &serviceId,
        &valid) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "get_all_services empty unit:%d\r\n", pSetServiceEntry->unit);
        return MEA_OK;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "get_all_services valid:%d\r\n", valid);



    while ( (valid) && (pRetPolicerEntry->number_of_ports < 1000) )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "serviceId:%d\r\n", serviceId);
        pRetPolicerEntry->number_of_ports++;
        pNextPort->ingress_port = serviceId;
        pNextPort->unit = pSetServiceEntry->unit;
        pNextPort = (tComIngressPortId *)&pNextPort->next[0];

        if (MEA_API_GetNext_Service(pSetServiceEntry->unit,
            &serviceId,
            &valid) != MEA_OK)
        {
            break;
        }
    }

    (*pSend_bytes) = sizeof(tComIndexDb)+pRetPolicerEntry->number_of_ports*sizeof(tComIndex);

    return MEA_OK;
}
MEA_Status MEA_comm_delete_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tComService *pSetServiceEntry = (tComService *)bufferRcv;
    tComService *pServiceEntry = (tComService *)bufferSnd;
    MEA_Status ret = MEA_OK;

    ret = MEA_API_Delete_Service(pSetServiceEntry->unit,
        pSetServiceEntry->serviceId);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_OK :%d\r\n", pSetServiceEntry->serviceId);
        (*pResult) = D_MSG_ENTRY_CANNOT_DELETE_COM_ERR;
        return ret;
    }


    memcpy(pServiceEntry, pSetServiceEntry, sizeof(tComService));
    (*pSend_bytes) = sizeof(tComService);

    return ret;
}





