#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif



MEA_Status MEA_comm_mea_api_Get_GW_Global_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tCom_Gw_Global_dbt *pGetEntry = (tCom_Gw_Global_dbt *)bufferRcv;
    tCom_Gw_Global_dbt *pEntry = (tCom_Gw_Global_dbt *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_Gw_Global_dbt));

    ret = MEA_API_Get_GW_Global_Entry(pGetEntry->unit,&pGetEntry->entry);
   

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_GW_Global_Entry :%d\r\n");
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


	memcpy(pEntry, pGetEntry, sizeof(tComPckAnalyzer));

	(*pSend_bytes) = sizeof(tComPckAnalyzer);

    return ret;
}

MEA_Status MEA_comm_mea_api_Set_GW_Global_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tCom_Gw_Global_dbt *pGetEntry = (tCom_Gw_Global_dbt *)bufferRcv;
    tCom_Gw_Global_dbt *pEntry = (tCom_Gw_Global_dbt *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->entry, 0, sizeof(MEA_Gw_Global_dbt));

    ret = MEA_API_Set_GW_Global_Entry(pGetEntry->unit, &pGetEntry->entry);

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_GW_Global_Entry :%d\r\n");
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


    memcpy(pEntry, pGetEntry, sizeof(tCom_Gw_Global_dbt));

    (*pSend_bytes) = sizeof(tCom_Gw_Global_dbt);

    return ret;
}


/************************************************************************/
/* rootFilter                                                           */
/************************************************************************/

MEA_Status MEA_comm_mea_api_Get_RootFilter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tCom_rootFilterEntry_dbt *pGetEntry = (tCom_rootFilterEntry_dbt *)bufferRcv;
    tCom_rootFilterEntry_dbt *pEntry = (tCom_rootFilterEntry_dbt *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry, 0, sizeof(tCom_rootFilterEntry_dbt));

    ret = MEA_API_Get_RootFilter_Entry(pGetEntry->unit, pGetEntry->index, &pGetEntry->enable);
   
    
    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Get_RootFilter_Entry :%d\r\n");
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


    //memcpy(pEntry->enable, pGetEntry->enable, sizeof(MEA_Bool));
    pEntry->enable = pGetEntry->enable;

    (*pSend_bytes) = sizeof(tCom_rootFilterEntry_dbt);

    return ret;
}

MEA_Status MEA_comm_mea_api_Set_RootFilter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    tCom_rootFilterEntry_dbt *pGetEntry = (tCom_rootFilterEntry_dbt *)bufferRcv;
    tCom_rootFilterEntry_dbt *pEntry = (tCom_rootFilterEntry_dbt *)bufferSnd;
    MEA_Status ret = MEA_OK;


    MEA_OS_memset(&pEntry->enable, 0, sizeof(MEA_Bool));

    ret = MEA_API_Set_RootFilter_Entry(pGetEntry->unit, pGetEntry->index, pGetEntry->enable);

    

    if (ret != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: failed to set MEA_API_Set_RootFilter_Entry :%d\r\n");
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }


    memcpy(pEntry, pGetEntry, sizeof(tCom_rootFilterEntry_dbt));

    (*pSend_bytes) = sizeof(tCom_rootFilterEntry_dbt);

    return ret;
}



