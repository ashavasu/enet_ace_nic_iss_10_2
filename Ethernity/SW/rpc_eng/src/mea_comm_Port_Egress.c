#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_comm.h"
#include "mea_comm_msg.h"


/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#endif


MEA_Status MEA_comm_get_egress_port(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    MEA_Status ret = MEA_OK;
    tComEgressPort *pRcvEgressPort = (tComEgressPort *)bufferRcv;
    tComEgressPort *pEgressPortEntry = (tComEgressPort *)bufferSnd;

    pEgressPortEntry->egress_port = pRcvEgressPort->egress_port;
    pEgressPortEntry->unit = pRcvEgressPort->unit;

    ret = MEA_API_Get_IsPortValid(pRcvEgressPort->egress_port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE);
	


    if (ret != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_comm_get_egress_port ERROR: failed to get MEA_API_Get_IsPortValid port:%d\r\n", pRcvEgressPort->egress_port);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }
    else
    {
        ret = MEA_API_Get_EgressPort_Entry(pEgressPortEntry->unit, pEgressPortEntry->egress_port, &pEgressPortEntry->portEgress);

        if (ret != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_comm_get_egress_port ERROR: failed to get MEA_API_Get_EgressPort_Entry portid:%d\r\n", pRcvEgressPort->egress_port);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
            return ret;
        }
    }
    (*pSend_bytes) = sizeof(tComIngressPort);
    return ret;
}

MEA_Status MEA_comm_set_egress_port(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult)
{
    MEA_Status ret = MEA_OK;
    tComEgressPort *pSetEgressPortEntry = (tComEgressPort *)bufferRcv;
    tComEgressPort *pEgressPortEntry = (tComEgressPort *)bufferSnd;


    ret = MEA_API_Get_IsPortValid(pSetEgressPortEntry->egress_port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE);
	
    if (ret != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_comm_set_egress_port ERROR: failed to get MEA_API_Get_IsPortValid port:%d\r\n", pSetEgressPortEntry->egress_port);
        (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
        return ret;
    }
    else
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "MEA_API_Set_EgressPort_Entry port:%d tx_enable:%d\r\n", pSetEgressPortEntry->egress_port, pSetEgressPortEntry->portEgress.tx_enable);
        ret = MEA_API_Set_EgressPort_Entry(pSetEgressPortEntry->unit, pSetEgressPortEntry->egress_port, &pSetEgressPortEntry->portEgress);

        if (ret != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_comm_set_egress_port ERROR: failed to set MEA_API_Set_IngressPort_Entry port:%d\r\n", pSetEgressPortEntry->egress_port);
            (*pResult) = D_MSG_ENTRY_NOT_FOUND_COMM_ERR;
            return ret;
        }
    }


    memcpy(pEgressPortEntry, pSetEgressPortEntry, sizeof(tComIngressPort));
    (*pSend_bytes) = sizeof(tComIngressPort);

    return ret;
}

