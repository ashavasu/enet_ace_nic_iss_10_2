#ifndef _MEA_COMM_H_
#define _MEA_COMM_H_

#ifdef __cplusplus
extern "C" {
#endif 
    
    
/************************************************************************/
/*        Port Ingress                                                  */
/************************************************************************/
MEA_Status MEA_comm_set_ingress_port(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_ingress_port(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_ingress_Allport(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/************************************************************************/
/*        Port Egress                                                   */
/************************************************************************/
MEA_Status MEA_comm_get_egress_port(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_set_egress_port(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/************************************************************************/
/*  Action                                                             */
/************************************************************************/
MEA_Status MEA_comm_create_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_set_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_getall_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_delete_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_getfirst_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_getnext_action_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_getrange_actions(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
/************************************************************************/
/*  Service                                                             */
/************************************************************************/

MEA_Status MEA_comm_create_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_comm_set_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status  MEA_comm_get_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_comm_getall_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_comm_delete_service_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_comm_getrange_services(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/************************************************************************/
/*       PolicerProfile                                                 */
/************************************************************************/
MEA_Status MEA_comm_create_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_set_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_delete_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_getall_policer_profile(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/************************************************************************/
/*       PM Counter                                                     */
/************************************************************************/
MEA_Status MEA_comm_clear_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_clear_All_Pm(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_first_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_next_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_range_Pm_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
/************************************************************************/
/*       RMON Counter                                                 */
/************************************************************************/
MEA_Status MEA_comm_clear_RMON_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_clear_All_RMON(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_RMON_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_first_RMON_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_next_RMON_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);


/**************************************************************************/
/*   CCM counter                                                         */
/*************************************************************************/
MEA_Status MEA_comm_get_CCM_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_clean_CCM_id(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/**************************************************************************/
/*   LM counter                                                         */
/*************************************************************************/
MEA_Status MEA_comm_Get_Counters_LM(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_Clear_Counters_LM(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);


/************************************************************************/
/*       forwarder Counter                                                 */
/************************************************************************/
MEA_Status MEA_comm_Create_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_Delete_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_Set_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_Get_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_GetFirst_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_GetNext_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_clearall_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_GetRange_SE_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/************************************************************************/
/*       packet generator ptofile                                        */
/************************************************************************/
MEA_Status MEA_comm_PacketGen_getRange_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_get_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_getfirst_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_getnext_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_create_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_set_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_delete_Profile_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/************************************************************************/
/*       packet generator entry                                        */
/************************************************************************/

MEA_Status MEA_comm_PacketGen_getRange_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_get_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_getfirst_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_getnext_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_create_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_set_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_PacketGen_delete_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/************************************************************************/
/*       packet ccm entry                                        */
/************************************************************************/
MEA_Status MEA_comm_getRange_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_getfirst_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_getnext_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_create_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_set_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_delete_ccm_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/************************************************************************/
/*       packet analyzer entry                                        */
/************************************************************************/
MEA_Status MEA_comm_get_analyzer_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_set_analyzer_info_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_API_Get_comm_Counters_ANALYZER(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_API_Clear_comm_Counters_ANALYZER(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_API_Get_eventGroup_ANALYZER(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
/************************************************************************/
/* GW                                                                      */
/************************************************************************/
MEA_Status MEA_comm_mea_api_Set_GW_Global_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_comm_mea_api_Get_GW_Global_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_comm_mea_api_Set_RootFilter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_comm_mea_api_Get_RootFilter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

/**********************************************************************/
/* global 																*/
/*********************************************************************/

MEA_Status MEA_API_comm_Get_Globals_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);

MEA_Status MEA_API_comm_Set_Globals_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);


/**********************************************************************/
/* filter 																*/
/*********************************************************************/
MEA_Status MEA_comm_get_filter_typeEntry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_getfirst_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_getnext_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_create_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_set_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_delete_filter_Entry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);
MEA_Status MEA_comm_get_filter_typeDataEntry(char *bufferRcv, int received_bytes, char *bufferSnd, int *pSend_bytes, unsigned int *pResult);




#ifdef __cplusplus
}
#endif


#endif



