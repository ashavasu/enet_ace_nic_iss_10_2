

#ifndef MEA_RPC_ENG_H
#define MEA_RPC_ENG_H

#ifdef __cplusplus
extern "C" {
#endif

#define BUFFER_SIZE 						5000
#define D_COMMMESSAGE_UDP_SOCKET			4444


#define 	D_GENERAL_ERROR_COMM_ERR						0x80000000
#define		D_MSG_ID_NOT_FOUND_COMM_ERR						D_GENERAL_ERROR_COMM_ERR + 0x00000001
#define		D_MSG_ENTRY_NOT_FOUND_COMM_ERR					D_GENERAL_ERROR_COMM_ERR + 0x00000002
#define		D_MSG_ENTRY_CANNOT_CREATE_COM_ERR				D_GENERAL_ERROR_COMM_ERR + 0x00000003
#define		D_MSG_ENTRY_CANNOT_SET_COM_ERR					D_GENERAL_ERROR_COMM_ERR + 0x00000004
#define		D_MSG_ENTRY_CANNOT_DELETE_COM_ERR				D_GENERAL_ERROR_COMM_ERR + 0x00000005



#define		D_MAX_NUM_OF_ACTION_EDITINGS					8

typedef struct
{
	unsigned int 	class_level_type;
	unsigned int	message_level_type;

	MEA_Status		ret;
	unsigned int 	message_result;
	unsigned int 	sequence_id;
}tHdrMsg;

typedef struct
{
	tHdrMsg hdr;
	char	info[1];
}tMsgBody;

typedef MEA_Status (*message_callback)(char *bufferRcv,int received_bytes,char *bufferSnd,int *pSend_bytes,unsigned int *pResult);

typedef struct
{
	unsigned int 		class_level_type;
	unsigned int 		message_level_type;
	message_callback 	callback_function;
}tFuncMessage;


#define			D_CLASS_INGRESS_PORT			1
	#define			D_MSG_GET_INGRESS_CONFIGURATION		1
	#define			D_MSG_SET_INGRESS_CONFIGURATION		2
	#define			D_MSG_GET_INGRESS_PORTS_INFO		3

#define			D_CLASS_EGRESS_PORT				2
	#define			D_MSG_GET_EGRESS_CONFIGURATION		1
	#define			D_MSG_SET_EGRESS_CONFIGURATION		2

#define			D_CLASS_POLICER_PROFILE			3
	#define			D_MSG_CREATE_POLICER_PROFILE		1
	#define			D_MSG_SET_POLICER_PROFILE			2
	#define			D_MSG_GET_POLICER_PROFILE			3
	#define			D_MSG_GET_ALL_POLICER_IDS			4
	#define			D_MSG_DELETE_POLICER_PROFILE		5

#define			D_CLASS_SERVICE_ID				4
	#define			D_MSG_CREATE_SERVICE				1
	#define			D_MSG_SET_SERVICE					2
	#define			D_MSG_GET_SERVICE					3
	#define			D_MSG_GET_ALL_SERVICES				4
	#define			D_MSG_DELETE_SERVICE				5
	#define			D_MSG_GETRANGE_SERVICE				6

#define			D_CLASS_ACTION_ID				5
	#define			D_MSG_CREATE_ACTION					1
	#define			D_MSG_SET_ACTION					2
	#define			D_MSG_GET_ACTION					3
	#define			D_MSG_GET_ALL_ACTIONS				4
	#define			D_MSG_DELETE_ACTION					5
	#define			D_MSG_GETFIRST_ACTION				6
	#define			D_MSG_GETNEXT_ACTION				7
	#define			D_MSG_GETRANGE_ACTION				8


#define			D_CLASS_PM_ID                   6
    #define			D_MSG_GET_PMID					1
    #define			D_MSG_GETFIRST_PM				2
    #define			D_MSG_GETNEXT_PM				3
    #define			D_MSG_CLEAR_PM				    4
    #define			D_MSG_CLEAR_ALL_PM				5
	#define			D_MSG_GET_RANGE_PM				6

#define			D_CLASS_RMON_ID                 7
    #define			D_MSG_GET_RMON					1
    #define			D_MSG_GETFIRST_RMON				2
    #define			D_MSG_GETNEXT_RMON				3
    #define			D_MSG_CLEAR_RMON				4
    #define			D_MSG_CLEAR_ALL_RMON			5

#define			D_CLASS_FORWARDER_ID             8
    #define			D_MSG_GET_FORWARDER				1
    #define			D_MSG_GETFIRST_FORWARDER		2
    #define			D_MSG_GETNEXT_FORWARDER			3
    #define			D_MSG_CREATE_FORWARDER			4
	#define			D_MSG_SET_FORWARDER				5
	#define			D_MSG_DELETE_FORWARDER			6
	#define			D_MSG_CLEAR_ALL_FORWARDER		7
	#define			D_MSG_GETRANGE_FORWARDER		8

#define			D_CLASS_PACKETGEN_PROFILE        9
    #define			D_MSG_GET_PACKETGEN_PROFILE				1
    #define			D_MSG_GETFIRST_PACKETGEN_PROFILE		2
    #define			D_MSG_GETNEXT_PACKETGEN_PROFILE			3
    #define			D_MSG_CREATE_PACKETGEN_PROFILE			4
	#define			D_MSG_SET_PACKETGEN_PROFILE				5
	#define			D_MSG_DELETE_PACKETGEN_PROFILE			6
	#define			D_MSG_GETRANGE_PACKETGEN_PROFILE		7

#define			D_CLASS_PACKETGEN_ENTRY        10
    #define			D_MSG_GET_PACKETGEN_ENTRY				1
    #define			D_MSG_GETFIRST_PACKETGEN_ENTRY			2
    #define			D_MSG_GETNEXT_PACKETGEN_ENTRY			3
    #define			D_MSG_CREATE_PACKETGEN_ENTRY			4
	#define			D_MSG_SET_PACKETGEN_ENTRY				5
	#define			D_MSG_DELETE_PACKETGEN_ENTRY			6
	#define			D_MSG_GETRANGE_PACKETGEN_ENTRY			7

#define			D_CLASS_PACKETCCM_ENTRY        11
    #define			D_MSG_GET_PACKETCCM_ENTRY				1
    #define			D_MSG_GETFIRST_PACKETCCM_ENTRY			2
    #define			D_MSG_GETNEXT_PACKETCCM_ENTRY			3
    #define			D_MSG_CREATE_PACKETCCM_ENTRY			4
	#define			D_MSG_SET_PACKETCCM_ENTRY				5
	#define			D_MSG_DELETE_PACKETCCM_ENTRY			6
	#define			D_MSG_GETRANGE_PACKETCCM_ENTRY			7

#define			D_CLASS_PACKET_ANALYZER_ENTRY        12
    #define			D_MSG_GET_PACKET_ANALYZER_ENTRY				1
    #define			D_MSG_GETFIRST_PACKET_ANALYZER_ENTRY		2
    #define			D_MSG_GETNEXT_PACKET_ANALYZER_ENTRY			3
    #define			D_MSG_CREATE_PACKET_ANALYZER_ENTRY			4
	#define			D_MSG_SET_PACKET_ANALYZER_ENTRY				5
	#define			D_MSG_DELETE_PACKET_ANALYZER_ENTRY			6
	#define			D_MSG_GETRANGE_PACKET_ANALYZER_ENTRY		7

#define			D_CLASS_GW_GLOBAL_ENTRY        		13
    #define			D_MSG_SET_GW_GLOBAL_ENTRY           		1
    #define			D_MSG_GET_GW_GLOBAL_ENTRY           		2

#define			D_CLASS_ROOT_FILTER_ENTRY           14
    #define			D_MSG_SET_ROOT_FILTER_ENTRY           		1
    #define			D_MSG_GET_ROOT_FILTER_ENTRY           		2

#define			D_CLASS_GLOBAL_ENTRY        		15
    #define			D_MSG_SET_GLOBAL_ENTRY           			1
    #define			D_MSG_GET_GLOBAL_ENTRY           			2

#define			D_CLASS_ANALYZER_COUNTER_ENTRY       16
    #define			D_MSG_GET_ANALYZER_COUNTER_ENTRY           	1
    #define			D_MSG_CLR_ANALYZER_COUNTER_ENTRY           	2


#define			D_CLASS_CCM_COUNTER_ENTRY       17
    #define			D_MSG_GET_CCM_COUNTER_ENTRY           	1
    #define			D_MSG_CLR_CCM_COUNTER_ENTRY           	2

#define			D_CLASS_LM_COUNTER_ENTRY       18
    #define			D_MSG_GET_LM_COUNTER_ENTRY           	1
    #define			D_MSG_CLR_LM_COUNTER_ENTRY           	2

#define			D_CLASS_EVENT_GROUP_ENTRY       19
    #define			D_MSG_GET_EVENT_GROUP_ENTRY           	1

#define			D_CLASS_FILTER_ENTRY        20
    #define			D_MSG_GET_FILTER_ENTRY				1
	#define			D_MSG_GET_FILTER_DATA_ENTRY			2
    #define			D_MSG_GETFIRST_FILTER_ENTRY			3
    #define			D_MSG_GETNEXT_FILTER_ENTRY			4
    #define			D_MSG_CREATE_FILTER_ENTRY			5
	#define			D_MSG_SET_FILTER_ENTRY				6
	#define			D_MSG_DELETE_FILTER_ENTRY			7


/***************************************************************************/

typedef struct
{
    MEA_Unit_t					unit;
    MEA_PmId_t 					PmId;
    MEA_Bool                    valid;
    MEA_Counters_PM_dbt 	    Data;
}tComm_PmId_dbt;

typedef struct
{
    MEA_Unit_t					unit;
    MEA_Port_t 					port;
    MEA_Bool                    valid;
    MEA_Counters_RMON_dbt 	    Data;
}tComm_RMON_dbt;

typedef struct
{
	MEA_Unit_t					unit;
	MEA_Port_t 					ingress_port;
	MEA_IngressPort_Entry_dbt 	portIngress;
}tComIngressPort;

typedef struct
{
	MEA_Unit_t					unit;
	MEA_Port_t 					egress_port;
	MEA_EgressPort_Entry_dbt 	portEgress;
}tComEgressPort;


typedef struct
{
	MEA_Unit_t					unit;
	MEA_Globals_Entry_dbt entry;
}tComGlobal;


typedef struct
{
	MEA_Unit_t					unit;
	MEA_Port_t 					ingress_port;
	char						next[1];
}tComIngressPortId,tComIndex;

typedef struct
{
	MEA_Unit_t                 		unit;
	MEA_Uint16                     	policer_id;
	MEA_AcmMode_t             		ACM_Mode;
	MEA_IngressPort_Proto_t        	port_proto_prof_i;
	MEA_Policer_Entry_dbt         	policer_Entry;
}tComPolicerProfile;

typedef struct
{
	MEA_Unit_t                      		unit;
	MEA_Uint32								key_valid;
    MEA_Service_Entry_Key_dbt       		key;
    MEA_Uint32								data_valid;
    MEA_Service_Entry_Data_dbt      		data;
    MEA_Uint32								outport_valid;
    MEA_OutPorts_Entry_dbt           		OutPorts_Entry;
    MEA_Uint16                     			policer_id;
    MEA_AcmMode_t             				ACM_Mode;
    MEA_Service_t                   		serviceId;
    MEA_Uint32        						num_of_entries; //ehp number of entries
    MEA_EHP_Info_dbt 						ehp_info[D_MAX_NUM_OF_ACTION_EDITINGS];    //max num EHP
    MEA_Uint32        						num_of_clusters; //ehp of clusters
}tComService;

typedef struct
{
	MEA_Unit_t                              unit;
	MEA_Uint32								data_valid;
	MEA_Action_Entry_Data_dbt             	Action_Data;
	MEA_Uint32								outport_valid;
	MEA_OutPorts_Entry_dbt                	Action_OutPorts;
	MEA_Uint16                     	   	 	policer_id;
	MEA_AcmMode_t             				ACM_Mode;
    MEA_Uint32        						num_of_entries; //ehp number of entries
    MEA_EHP_Info_dbt 						ehp_info[D_MAX_NUM_OF_ACTION_EDITINGS];    //max num EHP
	MEA_Action_t                          	Action_Id;
	mea_action_type_te                      Action_type;
	MEA_Bool                            	found;
}tComAction;

typedef struct
{
	MEA_Unit_t 								unit_i;
	MEA_Filter_Key_dbt 						key;
    MEA_Filter_Key_Type_te 					keyType;
    MEA_Bool     							found;
    MEA_Filter_t							filterId;
}tComFilterKey;

typedef struct
{
	MEA_Unit_t                           	unit;
	MEA_Filter_Key_dbt                  	key_from;
	MEA_Filter_Key_dbt                  	key_to;
	MEA_Filter_Data_dbt                 	data;
	MEA_OutPorts_Entry_dbt               	OutPorts_Entry;
	MEA_Policer_Entry_dbt                	Policer_entry;
	MEA_EgressHeaderProc_Array_Entry_dbt 	EHP_Entry;
	MEA_EHP_Info_dbt						ehp_info;
	MEA_Filter_t                         	o_filterId;
}tComFilterDataKey;

typedef struct
{
	MEA_Unit_t                              unit;
	MEA_SE_Entry_dbt      					entry;
	MEA_Bool                            	found;
	MEA_Bool								first;
}tComForwarder;

typedef struct
{
    MEA_PacketGen_drv_Profile_info_entry_dbt      	entry;
    MEA_PacketGen_profile_info_t                  	id;
	MEA_Unit_t                              		unit;
	MEA_Bool                                    	found;
}tComPckGenProfile;

typedef struct
{
	MEA_Unit_t                    					unit;
    MEA_PacketGen_Entry_dbt       					entry;
    MEA_PacketGen_t               					id;
    MEA_Bool                                    	found;
}tComPckGenEntry;

typedef struct
{
	MEA_Unit_t                  					unit;
	MEA_CCM_Configure_dbt      						entry;
   MEA_CcmId_t                						id;
   MEA_Bool                                    		found;
}tComCcmEntry;

typedef struct
{
	MEA_Unit_t                    					unit;
    MEA_Analyzer_t                					id;
    MEA_Analyzer_configure_dbt    					entry;
    MEA_Bool                                    	found;
}tComPckAnalyzer;

typedef struct
{
	MEA_Unit_t                    					unit;
    MEA_Analyzer_t                					id;
    MEA_Counters_Analyzer_dbt						counter;
}tComPckCounterAna;


typedef struct
{
	MEA_Unit_t                    					unit;
	MEA_Uint32                						group;
	MEA_Uint32										event;
}tComPckEventGroup;

typedef struct
{
	MEA_Bool										exist;
	MEA_Unit_t                    					unit;
	MEA_CcmId_t 									Id;
	MEA_Counters_CCM_Defect_dbt 					entry;
}tComCcmCounter;

typedef struct
{
	MEA_Bool										exist;
	MEA_Unit_t                    					unit;
	MEA_LmId_t 										Id;
	MEA_Counters_LM_dbt 							entry;
}tComLmCounter;

typedef struct
{
	MEA_Bool										exist;
	MEA_Unit_t                    					unit;
	MEA_Analyzer_t 									Id;
	MEA_Counters_Analyzer_dbt 						entry;
}tComAnalyzerCounter;


typedef struct
{
	int				number_of_ports;
	char						next[1];
}tComIngressPortsDb,tComIndexDb;


typedef struct
{
	MEA_SE_Entry_key_dbt  key;
	char				next[1];
}tComForwardPortsDb;



typedef struct{
    MEA_Unit_t          unit;
    MEA_Gw_Global_dbt   entry;

}tCom_Gw_Global_dbt;

typedef struct{
    MEA_Unit_t                  unit;
    MEA_root_Filter_type_t      index;
    MEA_Bool                    enable;

}tCom_rootFilterEntry_dbt;


/************************************************************************/
/*                                                                      */
/************************************************************************/
typedef union
{
	tComIngressPort 			ingressPort;
	tComEgressPort				egressPort;
	tComPolicerProfile			policerProfile;
	tComService					service;
	tComAction					action;
    tComm_PmId_dbt              pmInfo;
    tComm_RMON_dbt              RmonInfo;
    tComForwarder				forwarder;
    tComPckGenProfile			pckGenProfile;
    tComPckGenEntry				pckGenEntry;
    tComCcmEntry				pckCcmEntry;
    tComPckAnalyzer				pckAnaEntry;
    tCom_Gw_Global_dbt          GW_global;
    tCom_rootFilterEntry_dbt    rootFilterEntry;
    tComGlobal					global;
    tComPckCounterAna			analyzerCounter;
    tComCcmCounter				ccmCounter;
    tComLmCounter				lmCounter;
    tComAnalyzerCounter			anaCounter;
    tComPckEventGroup			anaEventGroup;
    tComFilterKey				filterEntry;
    tComFilterDataKey			filterDataEntry;
}tMsgCommDataBody;


typedef struct
{
	tMsgCommDataBody 	tMsgBody;
	unsigned int 		class_level_type;
	unsigned int 		message_level_type;
	char 				*bufferRcv;
	int 				MaxBufferRcv;
	int					ActBufferRcv;
}tMsgReq;






MEA_Status mea_client_build_packet(tMsgReq *pMsgReq);

#ifdef __cplusplus
}
#endif

#endif


