/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*-------------------------------- Includes ------------------------------------------*/

#ifndef __KERNEL__

#include <stdio.h>
#include <stdlib.h>
/*#include <sys/types.h> */
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

#include "MEA_platform.h"
#include "mea_cli.h"
#include "cli_eng.h"
#include "mea_api.h"

#if defined(MEA_OS_LINUX) && defined(MEA_ETHERNITY) && (!defined(MEA_NO_READLINE)) /* !defined(MEA_ENV_S1) && !defined(MEA_ENV_TDN1) && !defined(MEA_ENV_TDN2) && !defined(MEA_ENV_S1) && !defined(MEA_OS_SYAC)*/

#include <readline/readline.h>
#include <readline/history.h>
#endif

#endif



/* #define CLI_ENG_VPRINT_SUPPORT */

#ifdef MEA_OS_PSOS
int MEAG_getline(char *buf, int maxlen);
#endif

/*--------------------------------- Definitions ---------------------------------------*/

#define MEA_CLI_ENG_MAX_PRINTF MEA_OS_MAX_PRINTF
#define MAX_PATH_STR_SIZE 256

typedef struct Cmd {
	char *cmdName;
	CmdFunc cmdFunc;
	char *shortHelpP;
	char *longHelpP;
	MEA_Uint32 cmdLen;
	char cmdPrivilege;
} Cmd;

struct tree_node_s;

typedef struct Tree {
	Cmd command;
	struct tree_node_s *subCommands;
	struct Tree *parentP;
} Tree;

typedef struct tree_node_s {
	struct tree_node_s *next;
	struct tree_node_s *prev;
	Tree *tree;
} tree_node_t;

#define MAX_ARGV 2000
#define MAX_CMD_LEN 1000

#define SEP " \t,"
#define MAX_PROMPT_SIZE 35

#define DEFAULT_PRIVILEGE 0
#define DEFAULT_LEVEL CLI_USER

typedef struct User {
	char *name;
	char privilege;
	MEA_Bool inUse;
} User;

/*-------------------------------- External Functions --------------------------------*/

/*-------------------------------- External Variables --------------------------------*/

/*-------------------------------- Forward Declarations ------------------------------*/

static Tree *findCmd(char *name, int len, tree_node_t * l);
static Tree *findCmdShort(char *name, tree_node_t * l);
static void treeTraverse(Tree * treeP);
static char *romstrtok(char *str, char *sep, MEA_Uint32 * lenP);
static void clihelp(int argc, char *argv[]);

/*-------------------------------- Local Variables -----------------------------------*/

static Tree cmdRoot;
static Tree *cmdLevel;
static int traverseLimit;
static int privilegeLevel;
static char rootPrompt[MAX_PROMPT_SIZE];
static char curr_path_str[MAX_PATH_STR_SIZE];

Tree tree_pool[2000];
int next_free_tree_pool = 0;

/*-------------------------------- Global Variables ----------------------------------*/

static int CLI_print_long_string(char *str)
{
	static char pbuf[MEA_CLI_ENG_MAX_PRINTF];
	int index = 0;
	int len;
	len = MEA_OS_strlen(str);
	while (index < len) {
		MEA_OS_strncpy(pbuf, str + index, sizeof(pbuf) - 1);
		pbuf[sizeof(pbuf) - 1] = 0;
		CLI_print("%s", pbuf);
		index += MEA_OS_strlen(pbuf);
	}
	return index;
}

/**************************************************************************************/
// Function Name:       CLI_eng_main                                                                                                                              
//                                                                                                                                                                        
// Description:         start CLI engine                                                                                                                                  
//                                                                                                                                                                        
// Arguments:           None.                                                                                                                             
//                                                                                                                                                                        
// Return:                      main exit status                                                                                                                                  
/**************************************************************************************/

void CLI_eng_init()
{
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Initialize CLI_eng_init .....\n");
	CLI_init();

	MEA_CLI_AddDebugCmds();

	cmdLevel = &cmdRoot;
	traverseLimit = 0;
	privilegeLevel = DEFAULT_PRIVILEGE;

	return;

}

#ifndef __KERNEL__

void CLI_eng_main(MEA_FUNCPTR my_hook)
{
	CmdRc cmdRc;
	unsigned int add_hist;

	cmdLevel = &cmdRoot;
	traverseLimit = 0;
	privilegeLevel = DEFAULT_PRIVILEGE;

	if (my_hook) {
		my_hook(0);
	}

	
    MEA_PLATFORM_CLI_PRE_WELCOME
	
	CLI_print("\n\nWelcome to FPGA CLI Environment\n\n");  

    for(;;) {
/*XXX*/
#if !defined(MEA_OS_LINUX) || defined(MEA_OS_P1)
       // static char gg[MAX_CMD_LEN+1];
	    static char s[MAX_CMD_LEN+1];
#endif

#if defined(MEA_OS_LINUX) && defined(MEA_NO_READLINE)
        CLI_print("%s",CLI_prompt());
#endif         
#if !defined(MEA_OS_LINUX) 
		MEA_OS_memset(s,0,sizeof(s));
        CLI_print("%s",CLI_prompt());
        //MEA_OS_memset(gg,219,sizeof(gg));

#if ( defined(MEA_OS_Z1) && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION) )
		/*printf ("\n\n\n%s:%d before getstring\n\n\n",__FILE__,__LINE__);*/
        if (getstring(s,sizeof(s),strlen(CLI_prompt())) == NULL) {
  		    /*printf ("\n\n\n%s:%d getstring return NULL\n\n\n",__FILE__,__LINE__);*/
			return;
		}
		/*printf ("\n\n\n%s:%d after getstring (s='%s'\n\n\n",__FILE__,__LINE__,s);*/
#else /* MEA_OS_Z1 */
#ifdef MEA_OS_PSOS
        MEAG_getline(s,MAX_CMD_LEN);
#else /* MEA_OS_PSOS */
        {
          MEA_Uint32 count=0;
          char ch=0;

          while ( count < sizeof(s) && ch != '\n'){
             ch = getchar();
             s[count++]=ch;
          } 
          s[--count] = '\0';
        }
#endif /* MEA_OS_PSOS */
#endif /* MEA_OS_Z1 */
		cmdRc = CLI_execCmd(s,&add_hist);
#else
#if  (defined(MEA_NO_READLINE))
		{
            
			int count = 0;
			char ch = 0;
			static char s[MAX_CMD_LEN + 1];

			while (count < sizeof(s) && ch != '\n') {
				ch = getchar();
				if (ch != 255) {
					s[count++] = ch;
				}
			}
			s[--count] = '\0';
			cmdRc = CLI_execCmd(s, &add_hist);
		}
#else				/* MEA_ENV_S1 */
		{
			char *pStr;

            pStr=(char *)readline(CLI_prompt());

                if (pStr == NULL) {
                   break;
                }

                add_hist = MEA_FALSE;
                cmdRc = CLI_execCmd(pStr,&add_hist);

                if(add_hist) {
                        add_history (pStr);
                }
                
                MEA_OS_free(pStr);
        }
#endif /* MEA_ENV_S1_MAIN */
#endif


		if(cmdRc == CMD_EXIT)
		    break;
    }
    
    CLI_print("\nCLI exiting\n");

}

#endif				/* __KERNEL__ */

/****************************************************************************************/
/* Function Name:	CLI_init 																	  	*/
/* 																						*/
/* Description:		Init the CLI module																	*/
/*					None																	*/
/* Arguments:																			*/
/*																						*/
/* Return:	 		None.																			*/
/*																						*/
/****************************************************************************************/

void CLI_init()
{
	cmdRoot.parentP = NULL;
	cmdRoot.subCommands = NULL;
	cmdLevel = &cmdRoot;
	CLI_setPrompt("ENET>");
    //CLI_setPrompt("fpga>");
}

/****************************************************************************************/
/* Function Name:	CLI_setPrompt 														*/
/* 																						*/
/* Description:		Set the high level prompt											*/
/*					None																*/
/* Arguments:		s - prompt string													*/
/*																						*/
/* Return:	 		None.																*/
/*																						*/
/****************************************************************************************/

void CLI_setPrompt(char *s)
{
	strncpy(rootPrompt, s, MAX_PROMPT_SIZE);
	rootPrompt[MAX_PROMPT_SIZE - 1] = '\0';	// make sure it's null terminated
}

/*******************************************************************
*
* CLI_defineCmdWithPrivilege - add a command to the command tree
*
* ARGUMENTS
*    cmdName - path of command
*    cmdFunc - function doint the real work
*    shortHelpP - short help displayed during "help"
*    longHelpP - long help 
*    privilege - privilege level
*
* RETURNS  void 
*
* NOTES 
*
* INTERNAL 
*
*/

void CLI_defineCmdWithPrivilege(char *cmdName, CmdFunc cmdFunc,
				char *shortHelpP, char *longHelpP,
				char privilege)
{
	Tree *p = NULL;
	char *tok;
	Tree *treeP = &cmdRoot;
	MEA_Uint32 len = strlen(longHelpP);
	tree_node_t *tree_node;

	if (len > MEA_PLATFORM_CLI_MAX_HELP) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
				  " (%s) - too many characters in command    (len=%d)\n",
				  cmdName,len);
	}
	/* Split command to names */
	for (tok = romstrtok(cmdName, SEP, &len); tok != NULL;
	     tok = romstrtok(NULL, SEP, &len)) {
		p = findCmd(tok, len, treeP->subCommands);
		if (p == NULL) {

			// create a new tree node
			//p = (Tree *) MEA_OS_malloc(sizeof(Tree));

			if (next_free_tree_pool ==
			    MEA_NUM_OF_ELEMENTS(tree_pool)) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - too much CLI commands\n",
						  __FUNCTION__);
				return;
			}
			p = &tree_pool[next_free_tree_pool];
			next_free_tree_pool++;

			p->command.cmdName = tok;
			p->command.cmdLen = len;
			p->command.cmdFunc = NULL;
			p->command.shortHelpP = "";
			p->command.longHelpP = "";
			p->command.cmdPrivilege = privilege;

			tree_node = (tree_node_t *)
			    MEA_OS_malloc(sizeof(tree_node_t));
			if (tree_node == NULL) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - No more memory\n",
						  __FUNCTION__);
				return;
			}
			tree_node->tree = p;
			tree_node->prev = NULL;
			tree_node->next = treeP->subCommands;
			if (tree_node->next != NULL) {
				(tree_node->next)->prev = tree_node;
			}
			treeP->subCommands = tree_node;

			p->parentP = treeP;

		}

		else /* found level - check accumulative privilege */
	    if (p->command.cmdPrivilege > privilege)
			p->command.cmdPrivilege = privilege;
		treeP = p;
	}

	// We reached the leaf level, so update the info
	if (p) {
		p->command.cmdFunc = cmdFunc;
		p->command.shortHelpP = shortHelpP;
		p->command.longHelpP = longHelpP;
		p->command.cmdPrivilege = privilege;
	}

}

/*******************************************************************
*
* CLI_defineCmd - Define a command with default privilege
*
* ARGUMENTS
*    cmdName - 
*    cmdFunc - 
*    shortHelpP - 
*    longHelpP - 
*
* RETURNS  void 
*
* NOTES 
*
* INTERNAL 
*	Current default privilege is 0, may be changed later
*/
void CLI_defineCmd(char *cmdName, CmdFunc cmdFunc, char *shortHelpP,
		   char *longHelpP)
{
	CLI_defineCmdWithPrivilege(cmdName, cmdFunc, shortHelpP, longHelpP, DEFAULT_LEVEL);
}

/*******************************************************************
*
* prompt_path - output the prompt as function of current path
*
* ARGUMENTS
*    cmdP - current command path
*
* RETURNS  void
*
* NOTES 
*
* INTERNAL 
*
*/
static void prompt_path(Tree * cmdP)
{
	if (cmdP != NULL && cmdP != &cmdRoot) {
		char s[80];
		prompt_path(cmdP->parentP);
		strncpy(s, cmdP->command.cmdName, cmdP->command.cmdLen);
		s[cmdP->command.cmdLen] = '\0';
		//CLI_print("%s>",s);
		strcat(curr_path_str, s);
		strcat(curr_path_str, ">");

	}
}

/*******************************************************************
*
* CLI_prompt - output the prompt
*
* ARGUMENTS
*    none
*
* RETURNS  void 
*
* NOTES 
*
* INTERNAL 
*
*/
char *CLI_prompt(void)
{
	//CLI_print(rootPrompt);

	MEA_OS_memset(curr_path_str, 0, MAX_PATH_STR_SIZE);
	sprintf(curr_path_str, "%s", rootPrompt);

	prompt_path(cmdLevel);
	strcat(curr_path_str, ">");

	return curr_path_str;

	//CLI_print(">");
}

/*******************************************************************
*
* CLI_execCmd - Command execution engine
*
* ARGUMENTS
*    cmd - string containing the entered command
*
* RETURNS  CmdRc - return code of command
*
* NOTES 
*
* INTERNAL 
*
*/
MEA_Bool CLI_execCmd_isActive = MEA_FALSE;
CmdRc CLI_execCmd(char *cmd, unsigned int *add_hist)
{
	int argc = 0;
	static char *argv[MAX_ARGV];
	static char prevArgv[MAX_ARGV][MAX_CMD_LEN];
	static int prevArgc;
	Tree *cmdP;
	char *tok;
	int i;
	char tmp[MAX_CMD_LEN];
#ifdef MEA_OS_OC
	char *otmp=tmp;
#endif

	CLI_execCmd_isActive = MEA_TRUE;

	*add_hist = 1;

    if(cmd[0] == '#') {
       CLI_execCmd_isActive = MEA_FALSE;
	   return CMD_MEA_OK;
    }

	strcpy(tmp, cmd);
	/* Split command to argc/argv structure */
#ifdef MEA_OS_OC
	for (tok = strsep(&otmp, SEP); (tok != NULL) && (*tok != 0);
	     tok = strsep(&otmp, SEP)) {
#else
	for (tok = strtok(tmp, SEP); tok != NULL; tok = strtok(NULL, SEP)) {
#endif
		if (argc > MAX_ARGV) {
			CLI_printError(MEA_ERROR, "Too much arguments\n");
			CLI_execCmd_isActive = MEA_FALSE;
			return CMD_MEA_OK;
		}

		if (*tok == '!')
			switch (tok[1]) {
			case '!':	// !! replaced by entire previous cmd
				for (i = 0;
				     i < prevArgc && argc < MAX_ARGV;
				     i++, argc++)
					argv[argc] = prevArgv[i];
				break;
			case '*':	// !* replaced by previous cmd args
				for (i = 1;
				     i < prevArgc && argc < MAX_ARGV;
				     i++, argc++)
					argv[argc] = prevArgv[i];
				break;
			case '$':	// !$ replaced by previous cmd last arg
				argv[argc++] = prevArgv[prevArgc - 1];
				break;
			default:	// Assuming numeric - last cmd nth arg
				argv[argc++] =
				    prevArgv[MEA_OS_atoi(tok + 1)];
				break;
		} else {
			argv[argc] = tok;
			argc++;
		}
	}

	if (argc == 0) {
		CLI_execCmd_isActive = MEA_FALSE;
		return CMD_MEA_OK;
	}

	/* execute command */
	if (MEA_OS_strcasecmp(argv[0], "help") == 0 ||
	    MEA_OS_strcasecmp(argv[0], "ls") == 0 ||
	    MEA_OS_strcasecmp(argv[0], "?") == 0)
		clihelp(argc, argv);
	//  exit command        
	else if (MEA_OS_strcasecmp(argv[0], "exit") == 0) {
		CLI_execCmd_isActive = MEA_FALSE;
		return CMD_EXIT;
	} else if (MEA_OS_strcasecmp(argv[0], "top") == 0 ||
		   MEA_OS_strcasecmp(argv[0], "/") == 0)
		cmdLevel = &cmdRoot;
	else if (MEA_OS_strcasecmp(argv[0], "up") == 0 ||
		 MEA_OS_strcasecmp(argv[0], "..") == 0) {
		if (cmdLevel->parentP != NULL)
			cmdLevel = cmdLevel->parentP;
	}

	else if (MEA_OS_strcasecmp(argv[0], "prompt") == 0) {
		CLI_print("%s", CLI_prompt());
	} else if (MEA_OS_strcasecmp(argv[0], "restart") == 0) {
		//Need here somehow to reboot linux - RAFI
	} else {
		if (argv[0][0] == '/') {
			cmdP = &cmdRoot;
			++(argv[0]);
		} else if (MEA_OS_strncmp(argv[0], "../", 3) == 0) {
			cmdP = cmdLevel;
			while (MEA_OS_strncmp(argv[0], "../", 3) == 0) {
				cmdP = cmdP->parentP;
				argv[0] += 3;
			}
		} else
			cmdP = cmdLevel;

		for (i = 0; i < argc && cmdP != NULL; i++) {
			cmdP = findCmdShort(argv[i], cmdP->subCommands);
			if (cmdP != NULL) {

				if (cmdP->subCommands == NULL) {

					// Pass only the leaf command name + argumenst
					argv[i] = cmdP->command.cmdName;
					if (cmdP->command.cmdPrivilege >
					    privilegeLevel) {
						CLI_print
						    ("Command not available\n");
					} else {
						MEA_API_Lock();
						if (MEA_OK !=
						    (*cmdP->command.
						     cmdFunc) (argc - i,
							       argv + i)) {
							CLI_print
							    ("\n  Usage for %s:\n",
							     cmdP->command.
							     cmdName);
							CLI_print_long_string
							    (cmdP->command.
							     longHelpP);
							CLI_print("\n");
						}
						MEA_API_Unlock();
					}
					cmdP = NULL;
				}
				// else cmdLevel = cmdP;
			} else {
				CLI_printError(MEA_ERROR,
					       "Command %s not defined or ambiguous\n",
					       argv[i]);
#if 0
				{
					char *p = argv[i];
					while (*p) {
						printf("%d %c \n", *p, *p);
						p++;
					}
				}
#endif
				*add_hist = 0;
			}
		}

		if (cmdP != NULL) {
			if (cmdP->command.cmdPrivilege > privilegeLevel)
				CLI_print("Command not available\n");
			else
				cmdLevel = cmdP;
		}

	}

	for (i = 0; i < argc; i++)
		strcpy(prevArgv[i], argv[i]);
	prevArgc = argc;

	CLI_execCmd_isActive = MEA_FALSE;
	return CMD_MEA_OK;
}

#ifdef MEA_OS_Z1
int ethernity_cli(int argc, char *argv[], void *p)
{
#if 1
	CLI_eng_main(NULL);
#else

	char cmd[MAX_CMD_LEN];
	unsigned int dummy;
#if 0

	if (fgets(cmd, sizeof(cmd), stdin) == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				  "%s:%d fgets return NULL\n", __FILE__,
				  __LINE__);
		return 0;
	}
#else
	int i;

	p = p;

	cmd[0] = '\0';

	for (i = 1; i < argc; i++) {
		MEA_OS_strcat(cmd, argv[i]);
		MEA_OS_strcat(cmd, " ");
	}
#endif
	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "%s:%d argc=%d cnd='%s'\n",
			  __FILE__, __LINE__, argc, cmd);
	CLI_execCmd(&(cmd[0]), &dummy);
#endif

	return 0;
}
#endif				/* MEA_OS_Z1 */

#ifdef CLI_ENG_VPRINT_SUPPORT
/*******************************************************************
*
* CLI_printSplit - print output in split chunks 
*                  to avoid buffer printing problems
*
* ARGUMENTS :   str - string to print
*
* RETURNS  : none
*
*/
#ifndef __KERNEL__
void CLI_printSplit(char *str)
{
	static char pbuf[MEA_CLI_ENG_MAX_PRINTF];
	int index = 0;
	int len = MEA_OS_strlen(str);
	while (index < len) {
		MEA_OS_strncpy(pbuf, str + index, sizeof(pbuf) - 1);
		pbuf[sizeof(pbuf) - 1] = 0;
#ifdef MEA_OS_PSOS
		if (MEA_PORTINGG_printf(pbuf) == EOF) {
			return;
		}
#else				/* MEA_OS_PSOS */
#ifdef MEA_OS_LINUX
		if (ENET_thread_rpc_cli_active) {
			if (ENET_thread_rpc_cli_print(pbuf) < 0) {
				return;
			}
		} else {
			MEA_OS_printf("%s", pbuf);
			MEA_OS_fflush(stdout);
		}
#else				/* MEA_OS_LINUX */
		MEA_OS_printf("%s", pbuf);
		MEA_OS_fflush(stdout);
#endif				/* MEA_OS_LINUX */
#endif				/* MEA_OS_PSOS */
		index += MEA_OS_strlen(pbuf);
	}
	return;

}
#endif				/* __KERNEL__ */

/*******************************************************************
*
* CLI_vprint - print output in vformat
*
* ARGUMENTS
*    format - format string
*    ...    - args in vformat 
*
* RETURNS  : none
*
*/
#ifndef __KERNEL__
void CLI_vprint(char *format, va_list args)
{
	char *str;
	int len;
	len = MEA_OS_vasprintf(&str, format, args);
	if (len < 0) {
		return;
	}
	CLI_printSplit(str);
	MEA_OS_free(str);
}
#endif				/* __KERNEL__ */
#endif				/* CLI_ENG_VPRINT_SUPPORT */

/*******************************************************************
*
* CLI_print - print output
*
* ARGUMENTS
*    format - format string
*    ...    - arguments
*
* RETURNS  none
*
* NOTES 
*	Should be used by all functions, so if output needs to be treated
*	otherwise than printf, this will be the place to change
* INTERNAL 
*
*/
#ifndef __KERNEL__
void CLI_print(char *format, ...)
{

	va_list args;

	va_start(args, format);
#ifdef CLI_ENG_VPRINT_SUPPORT
	CLI_vprint(format, args);
	//va_end(args);
#else				/* CLI_ENG_VPRINT_SUPPORT */
#ifdef MEA_OS_LINUX
	if (ENET_thread_rpc_cli_active) {
		static char pbuf[5000];
		vsprintf(pbuf, format, args);
		if (ENET_thread_rpc_cli_print(pbuf) < 0) {
			va_end(args);
			return;
		}
	} else {
		vprintf(format, args);
		fflush(stdout);
	}
#else				/* MEA_OS_LINUX */
	vprintf(format, args);
	fflush(stdout);
#endif				/* MEA_OS_LINUX */
#endif				/* CLI_ENG_VPRINT_SUPPORT */
	va_end(args);  /**/
}
#endif				/* __KERNEL__ */

/*******************************************************************
*
* CLI_printError - print an error message
*
* ARGUMENTS
*    errorLevel - error level of message
*    format - 
*    ... - arguments
*
* RETURNS  void 
*
* NOTES 
*
* INTERNAL 
*
*/
#ifndef __KERNEL__
void CLI_printError(int errorLevel, char *format, ...)
{
	va_list args;

	va_start(args, format);
#ifdef CLI_ENG_VPRINT_SUPPORT
	if (errorLevel == MEA_ERROR) {
		CLI_print("MEA_ERROR: ");
	}
	CLI_vprint(format, args);
	//va_end(args);
#else				/* MEA_OS_LINUX */
	if (errorLevel == MEA_ERROR) {
		CLI_print("MEA_ERROR: ");
	}
	{
		static char pbuf[1000];
		vsprintf(pbuf, format, args);
		CLI_print(pbuf);
	}
#endif				/* CLI_ENG_VPRINT_SUPPORT */
	va_end(args);  /**/

}
#endif				/* __KERNEL__ */

/*******************************************************************
*
* clihelp - recursive help on command
*
* ARGUMENTS
*    argc - # of tokens in split command
*    argv[] - the command tokens
*
* RETURNS   void 
*
* NOTES 
*
* INTERNAL 
*
*/
static void clihelp(int argc, char *argv[])
{
	int i;
	Tree *cmdP;

	if (argc > 1 && strcmp(argv[1], "-r") == 0)
		i = 2, traverseLimit = 99;
	else
		i = 1, traverseLimit = 1;
	if (i < argc) {
		for (cmdP = cmdLevel; i < argc; i++) {
			cmdP = findCmdShort(argv[i], cmdP->subCommands);
			if (cmdP == NULL) {
				i++;
				break;	/* if one command in the list is not found than exit  */
			}
		}
		i--;
		if (cmdP != NULL) {
			// command is found in current level
			if (cmdP->subCommands != NULL) {
				// This is not a leaf command, so list all
				CLI_print
				    ("List of Available Commands\n\n");
				treeTraverse(cmdP);
			} else {	// full help on leaf command
				CLI_print_long_string(cmdP->command.
						      longHelpP);
				CLI_print("\n");
			}
		} else		// command not found
			CLI_printError(MEA_ERROR,
				       "Command %s not defined or ambiguous\n",
				       argv[i]);
	} else {
		CLI_print("List of Available Commands\n\n");
		treeTraverse(cmdLevel);
		CLI_print("\n");
		CLI_print("Universal Commands are :\n");
		CLI_print("help / ?  / ls  - help\n");
		CLI_print("up   / ..       - Up level\n");
		CLI_print("top             - Top level\n");
		CLI_print("prompt          - Print current prompt\n");
		CLI_print("exit            - Exit to shell\n");
		CLI_print("\n");
	}

}

/*******************************************************************
*
* findCmd - Find the command in a list
*
* ARGUMENTS
*    name - command name
*    len - length to match, to avoid partial matching
*    l - list to search in
*
* RETURNS  Tree *
*
* NOTES 
*
* INTERNAL 
*
*/
static Tree *findCmd(char *name, int len, tree_node_t * l)
{
	tree_node_t *tree_node;

	for (tree_node = l;
	     ((tree_node != NULL) &&
	      ((len != (tree_node->tree)->command.cmdLen) ||
	       (MEA_OS_strncmp
		(name, (tree_node->tree)->command.cmdName, len) != 0)
	      )
	     ); tree_node = tree_node->next);

	if (tree_node == NULL) {
		return NULL;
	}

	return tree_node->tree;

}

/*******************************************************************
*
* strSubstr - return 0 if match, 1 if son is subset of dad, else 2
*
* ARGUMENTS
*    dad - 
*    dadLen - 
*    son - 
*
* RETURNS   int 
*
* NOTES 
*
* INTERNAL 
*
*/
static int strSubstr(char *dad, int dadLen, char *son)
{
	int result = 0;
	while (toupper(*dad) == toupper(*son) && dadLen > 0
	       && *son != '\0') {
		dad++;
		dadLen--;
		son++;
	}
	if ( /* *dad != '\0' */ dadLen > 0)
		result = 1;
	if (*son != '\0')
		result = 2;
	return result;
}

/*******************************************************************
*
* findCmdShort - find a command supporting command short-cuts
*
* ARGUMENTS
*    name - command name
*    l - list to search
*
* RETURNS   Tree *
*
* NOTES 
*
* INTERNAL 
*
*/
static Tree *findCmdShort(char *name, tree_node_t * l)
{
	Tree *cmd;
	Tree *matchCmd = NULL;
	int matchCounter = 0;
	tree_node_t *tree_node;

	for (tree_node = l; tree_node != NULL; tree_node = tree_node->next) {

		cmd = tree_node->tree;

		switch (strSubstr
			(cmd->command.cmdName, cmd->command.cmdLen,
			 name)) {
		case 0:	/* full match */
			return cmd;
		case 1:	/* name is substring of cmdNamr */
			matchCounter++;
			matchCmd = cmd;
			break;
		default:	/* not match, do nothing */
			break;
		}

	}
	if (matchCounter == 1)	/* if just one match */
		return matchCmd;
	else {
		return NULL;
	}

}

/*******************************************************************
*
* treeTraverse - recursive tree travsersing for command help display
*
* ARGUMENTS
*    tree - 
*
* RETURNS   void 
*
* NOTES 
*
* INTERNAL 
*
*/
static void treeTraverse(Tree * treeP)
{
	static int level = 0;
	int i;
	tree_node_t *tree_node;

	if (treeP == NULL) {
		return;
	}

	if (treeP->command.cmdPrivilege <= privilegeLevel)
		for (i = 0; i < level; i++)
			CLI_print("    ");	/* am */
//        CLI_print("\t");                                                  /* am */
	if (treeP->command.cmdName != NULL
	    && treeP->command.cmdPrivilege <= privilegeLevel) {
		char s[80];
		strncpy(s, treeP->command.cmdName, treeP->command.cmdLen);
		s[treeP->command.cmdLen] = '\0';
		CLI_print("%-10s: %s", s, treeP->command.shortHelpP);
		if (treeP->subCommands != NULL)
			CLI_print(">>");
		CLI_print("\n");
	}
	level++;
	if (level <= traverseLimit)
		for (tree_node = treeP->subCommands; tree_node != NULL;
		     tree_node = tree_node->next)
			treeTraverse(tree_node->tree);

	level--;

}

/*******************************************************************
*
* romstrtok - A version of strtok that works on ROM string
*
* ARGUMENTS
*    str - 
*    sep - 
*    lenP - 
*
* RETURNS   char *
*
* NOTES 
*	It returns a pointer within the ROM string, and the token length
*	in lenP, since it can't write \0 into the ROM
*	The function is non reentrant, as it uses a private working buffer
*
* INTERNAL 
*
*/
static char *romstrtok(char *str, char *sep, MEA_Uint32 * lenP)
{
	static char s[1000];
#ifdef MEA_OS_OC
	static char *os;
#endif
	static char *rom_str;
	char *tok;

	if (str != NULL) {	// 1st time, copy
		strcpy(s, str);
		rom_str = str;
#ifdef MEA_OS_OC
		os = s;
		tok = strsep(&os, sep);
	} else
		tok = strsep(&os, sep);
#else
		tok = strtok(s, sep);
	} else
		tok = strtok(NULL, sep);
#endif

	if (tok == NULL) {
		*lenP = 0;
		return NULL;
	}

	*lenP = strlen(tok);
	return (char *) rom_str + (tok - s);
}

/*******************************************************************
*
* CLI_nameToInteger - Find sample in the array of optional patterns
*
* ARGUMENTS
*    name - sample to find
*    argc - 
*    argv[] - 
*
* RETURNS  int number of matched pattern if MEA_OK, -1 if MEA_ERROR
*
* NOTES 
*
* INTERNAL 
*
*/
int CLI_nameToInteger(char *name, int argc, char *argv[])
{
	int i;
	int match = -1;
	for (i = 0; i < argc; i++) {
		if (strstr(argv[i], name) == argv[i]) {
			if (match == -1) {
				match = i;
			} else {
				match = -1;	/* error: more than one matches */
				break;
			}
		}
	}
	return match;
}

/*******************************************************************
*
* CLI_nameToValue - 
*
* ARGUMENTS
*    name - 
*    argc - 
*    argv[] - 
*
* RETURNS  int
*
* NOTES 
*
* INTERNAL 
*
*/
int CLI_nameToValue(char *name, int argc, NameValue argv[])
{
	int i;
	int match = -1;
	for (i = 0; i < argc; i++) {
		if (strstr(argv[i].name, name) == argv[i].name) {
			if (match == -1) {
				match = argv[i].value;
			} else {
				match = -1;	/* error: more than one matches */
				break;
			}
		}
	}
	return match;
}

/*******************************************************************
*
* CLI_valueToName - 
*
* ARGUMENTS
*    value - 
*    argc - 
*    argv[] - 
*
* RETURNS  char *
*
* NOTES 
*
* INTERNAL 
*
*/
char *CLI_valueToName(int value, int argc, NameValue argv[])
{
	int i;
	char *match = NULL;

	for (i = 0; i < argc; i++) {
		if (argv[i].value == value) {
			match = argv[i].name;
			break;
		}
	}
	return match;
}


