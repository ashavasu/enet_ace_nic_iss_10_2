/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef CLI_ENG_H
#define CLI_ENG_H

#ifdef __cplusplus
extern "C" {
#endif


#include "MEA_platform.h"

#define CLI_USER 0
#define CLI_TECH 1
#define CLI_SUPER 2

typedef int (*CmdFunc) (int argc, char *argv[]);
typedef enum {
	CMD_MEA_OK,
	CMD_LOGOUT,
	CMD_EXIT
} CmdRc;

typedef struct {
	char *name;
	int value;
} NameValue;

extern MEA_Bool CLI_execCmd_isActive;
void CLI_eng_init(void);

 
void CLI_eng_main(MEA_FUNCPTR my_hook); 

 
CmdRc CLI_execCmd(char *cmd, unsigned int *add_hist);
void CLI_init(void);
void CLI_setPrompt(char *s);
void CLI_defineCmd(char *cmdName, CmdFunc cmdFunc, char *shortHelpP,
		   char *longHelpP);
void CLI_defineCmdWithPrivilege(char *cmdName, CmdFunc cmdFunc,
				char *shortHelpP, char *longHelpP,
				char privilege);
#ifdef __KERNEL__
#include "MEA_platform_os_pf.h"
#define CLI_print MEA_OS_pf_printf
#define CLI_printError(errorLevel,args...) MEA_OS_pf_printf(args)
#else
void CLI_print(char *format, ...);
void CLI_printError(int errorLevel, char *format, ...);
#endif
char *CLI_prompt(void);
int CLI_nameToInteger(char *name, int argc, char *argv[]);
int CLI_nameToValue(char *name, int argc, NameValue argv[]);
char *CLI_valueToName(int value, int argc, NameValue argv[]);



#ifdef __cplusplus
}
#endif

#endif
