
#ifndef K7_SFP_INTERFACE_H
#define K7_SFP_INTERFACE_H

#include "MEA_platform.h"
#ifdef __cplusplus
extern "C" {
#endif

/* START OF SFP REGISTERS */
#define PHY_ADDRESS_SFP			0x56
#define SERIAL_ADDRESS_SFP		0x50
#define SERIAL_ADDRESS_I2C1		0x70
#define SERIAL_ADDRESS_I2C2		0x71


/* serial Id: data field */
#define	SFP_TRANCIEVER_VENDOR_NAME_BASE_ADDR			0x14
#define	SFP_TRANCIEVER_VENDOR_LEN						16
#define	SFP_TRANCIEVER_VENDOR_PART_NUMBER_BASE_ADDR		0x28
#define	SFP_TRANCIEVER_VENDOR_PART_NUMBER_LEN			16
#define	SFP_TRANCIEVER_COPPER_LEN_ADDR			        18




/* phy data register */
#define SFP_CONTROL_REGISTER							0x00
	#define	SFP_SOFT_RESET								0x8000
	#define	SFP_LOOPBACK								0x4000
	#define	SFP_SPEED_SELECTION_HIGHBIT					0x2000
	#define	SFP_AUTONEG_ENABLE							0x1000
	#define	SFP_POWER_DOWN								0x0800
	#define	SFP_ISOLATE									0x0400
	#define	SFP_RESTART_AUTONEG							0x0200
	#define	SFP_DUPLEX_MODE								0x0100
	#define	SFP_SPEED_SELECTION_LOWBIT					0x0040

#define	SFP_1G_SPEED								    SFP_SPEED_SELECTION_LOWBIT
#define	SFP_100M_SPEED								    SFP_SPEED_SELECTION_HIGHBIT
	#define	SFP_10M_SPEED								0x0000

#define SFP_STATUS_REGISTER								0x01
	#define	SFP_AUTONEG_STATE							0x0020
	#define	SFP_OPER_LINK_STATE							0x0004

#define SFP_AUTONEG_ADVERTISEMENT_REGISTER				0x04
	#define	SFP_100BASE_FULLDUPLEX						0x0100
	#define	SFP_100BASE_HALFDUPLEX						0x0080
	#define	SFP_10BASE_FULLDUPLEX						0x0040
	#define	SFP_10BASE_HALFDUPLEX						0x0020
	#define	SFP_SELECTOR_FIELD							0x0001

#define SFP_PHY_SPECIFIC_CONTROL_REGISTER				0x16
	#define	SFP_DISABLE_JABBER_FUNCTION					0x0001


#define SFP_PHY_SPECIFIC_STATUS_REGISTER				0x11
	#define	SFP_SPEED_STATUS_HIGHBIT					0x8000
	#define	SFP_SPEED_STATUS_LOWBIT						0x4000
	#define	SFP_SPEED_STATUS_DUPLEX						0x2000
	#define	SFP_SPEED_STATUS_LINK_STATE					0x0400

	#define	SFP_SPEED_1G_STATE							SFP_SPEED_STATUS_HIGHBIT
	#define	SFP_SPEED_100M_STATE						SFP_SPEED_STATUS_LOWBIT
	#define	SFP_SPEED_10M_STATE							0x0000
	#define	SFP_LINKUP_STATE							SFP_SPEED_STATUS_LINK_STATE
	#define	SFP_FULLDUPLEX_STATE						SFP_SPEED_STATUS_DUPLEX

#define SFP_PHY_EXTENDED_PHY_SPECIFIC_STATUS			0x1B
	#define	SFP_FIBER_COPPER_SELECTION					0x8000
	#define	SFP_SERIAL_INTERFACE_AUTONEG_BYPASS			0x1000
	#define	SFP_SGMII_MODE								0x0004
	#define	SFP_SERDES2_MODE							0x0002
	#define	SFP_SERDES1_MODE							0x0001
/* END OF SFP REGISTERS */

/* START OF MDIO REGISTERS */
#define _RGMII0_PHY_ADDR             0x8  /*RGMII#0 *//*-PORT  72?*/
#define _RGMII1_PHY_ADDR             0x18 /*RGMII#1 *//*-PORT  48?*/
#define _RGMII2_PHY_ADDR             0x7  /*RGMII#2 *//*-PORT- 36*/
#define _RGMII3_PHY_ADDR             0x5  /*RGMII#3 *//*-PORT- 24*/
#define _RGMII4_PHY_ADDR             0x6  /*RGMII#4 *//*-PORT- 12*/
#define _RGMII5_PHY_ADDR             0x2  /*RGMII#5 *//*-PORT- 0*/

#define _QSGMII0_0_PHY_ADDR          0x14 /*QSGMII#0 0*//* PORT 100*/
#define _QSGMII0_1_PHY_ADDR          0x15 /*QSGMII#0 1*//* PORT 101*/
#define _QSGMII0_2_PHY_ADDR          0x16 /*QSGMII#0 2*//* PORT 102*/
#define _QSGMII0_3_PHY_ADDR          0x17 /*QSGMII#0 3*//* PORT 103*/

#define _QSGMII1_0_PHY_ADDR         0x10  /*QSGMII#1 0*//* PORT 108*/
#define _QSGMII1_1_PHY_ADDR         0x11  /*QSGMII#1 1*//* PORT 109*/
#define _QSGMII1_2_PHY_ADDR         0x12  /*QSGMII#1 2*//* PORT 110*/
#define _QSGMII1_3_PHY_ADDR         0x13  /*QSGMII#1 3*//* PORT 111*/



#define _GB_PHY_MODEL_BIT_SHIFT 	4
#define _GB_PHY_83E1111    			0x000C<<_GB_PHY_MODEL_BIT_SHIFT  /*Model Marvel Alaska 83E1111 */
#define _GB_PHY_83E13XX    			0x001C<<_GB_PHY_MODEL_BIT_SHIFT  /*Model Marvel Alaska 83E1340S/83E1340/83E1322 */


#define MDIO_GENERAL_CONTROL_REGISTER 				20	// PAGE 6 REGISTER 20
	#define MDIO_SOFTWARE_RESET						0x8000
	#define MDIO_SNOOPING_HIGH						0x0800
	#define MDIO_SNOOPING_LOW						0x0400
	#define MDIO_PTP_POWER_DOWN						0x0200
	#define MDIO_PTP_REF_CLOCK						0x0100
	#define MDIO_PTP_INPUT_SRC						0x0080
	#define MDIO_AUTO_MEDIA_DETECT_HIGH				0x0020
	#define MDIO_AUTO_MEDIA_DETECT_LOW				0x0010
	#define MDIO_PTP_125M_CLOCK_SRC					0x0008
	#define MDIO_MEDIA_MODE2						0x0004
	#define MDIO_MEDIA_MODE1						0x0002
	#define MDIO_MEDIA_MODE0						0x0001

	#define MDIO_MEDIA_QSGMII_TO_COPPER						0x0000
	#define MDIO_MEDIA_SGMII_TO_COPPER						MDIO_MEDIA_MODE0
	#define MDIO_MEDIA_QSGMII_TO_1000BASETX					MDIO_MEDIA_MODE1
	#define MDIO_MEDIA_QSGMII_TO_100BASETX					(MDIO_MEDIA_MODE0 | MDIO_MEDIA_MODE1)
	#define MDIO_MEDIA_QSGMII_TO_SGMII						MDIO_MEDIA_MODE2
	#define MDIO_MEDIA_SGMII_TO_QSGMII						(MDIO_MEDIA_MODE0 | MDIO_MEDIA_MODE2)
	#define MDIO_MEDIA_SGMII_TO_AUTO_DETECT_SGMII			(MDIO_MEDIA_MODE1 | MDIO_MEDIA_MODE2)
	#define MDIO_MEDIA_SGMII_TO_AUTO_DETECT_1000M			(MDIO_MEDIA_MODE0 | MDIO_MEDIA_MODE1 | MDIO_MEDIA_MODE2)


#define MDIO_QSGMII_COPPER_SPECIFIC_CONTROL_REGISTER1 	16	// PAGE 4 REGISTER 16
	#define MDIO_TRANSMIT_FIFO_DEPTH_HIGH				0x8000
	#define MDIO_TRANSMIT_FIFO_DEPTH_LOW				0x4000
	#define MDIO_BLOCK_CARRIER_EXT_BIT					0x2000
	#define MDIO_COPPER_QSGMII_LOOPBACK					0x1000
	#define MDIO_FORCE_LINK_GOOD						0x0400
	#define MDIO_AUTONEG_BYPASS_ENABLE					0x0200
	#define MDIO_ENHANCED_SGMII							0x0080

#define MDIO_QSGMII_FIBER_SPECIFIC_CONTROL_REGISTER1 		16	// PAGE 1 REGISTER 16
	#define MDIO_FIBER_TRANSMIT_FIFO_DEPTH_HIGH				0x8000
	#define MDIO_FIBER_TRANSMIT_FIFO_DEPTH_LOW				0x4000
	#define MDIO_FIBER_BLOCK_CARRIER_EXT_BIT				0x2000
	#define MDIO_FIBER_LOOPBACK								0x1000
	#define MDIO_ASSERT_CRS_ON_TX							0x0800
	#define MDIO_FIBER_FORCE_LINK_GOOD						0x0400
	#define MDIO_SERDES_LOOPBACK							0x0100
	#define MDIO_FIBER_ENHANCED_SGMII						0x0080
	#define MDIO_FIBER_POWER_UP								0x000F


#define MDIO_QSGMII_CONTROL_REGISTER	 				0	// PAGE 4 REGISTER 0
	#define MDIO_QSGMII_SOFTWARE_RESET					0x8000
	#define MDIO_QSGMII_LOOPBACK						0x4000
	#define MDIO_QSGMII_SPEED_SELECT_LSB				0x2000
	#define MDIO_QSGMII_AUTONEG_ENABLE					0x1000
	#define MDIO_QSGMII_POWER_DOWN						0x0800
	#define MDIO_QSGMII_RESTART_FIBER					0x0200
	#define MDIO_QSGMII_SPEED_SELECT_MSB				0x0040


	#define MDIO_QSGMII_1000M_SPEED						MDIO_QSGMII_SPEED_SELECT_LSB
	#define MDIO_QSGMII_100M_SPEED						MDIO_QSGMII_SPEED_SELECT_MSB


#define MDIO_QSGMII_STATUS_REGISTER	 					17	// PAGE 4 REGISTER 17
	#define MDIO_QSGMII_SPEED_HIGH_BYTE					0x8000
	#define MDIO_QSGMII_SPEED_LOW_BYTE					0x4000
	#define MDIO_QSGMII_DUPLEX							0x2000
	#define MDIO_QSGMII_LINK_STATUS						0x0400


#define MDIO_COPPER_CONTROL_REGISTER	 				0	// PAGE 0 REGISTER 0
	#define MDIO_COPPER_RESET							0x8000
	#define MDIO_COPPER_LOOPBACK						0x4000
	#define MDIO_COPPER_SPEED_SELECT_LSB				0x2000
	#define MDIO_COPPER_AUTONEG_ENABLE					0x1000
	#define MDIO_COPPER_POWER_DOWN						0x0800
	#define MDIO_COPPER_RESTART_AUTONEG					0x0200
	#define MDIO_COPPER_DUPLEX_MODE						0x0100
	#define MDIO_COPPER_SPEED_SELECT_MSB				0x0040

#define MDIO_COPPER_STATUS_REGISTER	 					1	// PAGE 0 REGISTER 1
	#define MDIO_COPPER_LINK_STATUS						0x0004

#define MDIO_COPPER_EXTENDED_STATUS_REGISTER	 		17	// PAGE 0 REGISTER 17
	#define	MDIO_COPPER_STATUS_1G_SPEED					0x8000
	#define	MDIO_COPPER_STATUS_100M_SPEED				0x4000
	#define	MDIO_COPPER_STATUS_FULL_DUPLEX				0x2000
	#define	MDIO_COPPER_STATUS_REAL_LINK_UP				0x0400
	#define	MDIO_COPPER_STATUS_SYNC						0x0020

#define MDIO_FIBER_STATUS_REGISTER	 					1	// PAGE 1 REGISTER 1
	#define MDIO_FIBER_LINK_STATUS						0x0004

#define MDIO_FIBER_EXTENDED_STATUS_REGISTER	 			17	// PAGE 0 REGISTER 17
	#define	MDIO_FIBER_STATUS_1G_SPEED					0x8000
	#define	MDIO_FIBER_STATUS_100M_SPEED				0x4000
	#define	MDIO_FIBER_STATUS_FULL_DUPLEX				0x2000
	#define	MDIO_FIBER_STATUS_REAL_LINK_UP				0x0400
	#define	MDIO_FIBER_STATUS_SYNC						0x0020
/* END OF MDIO REGISTERS */


typedef enum
{
	CONFIGURE_SPEED_NONE,
	CONFIGURE_1G_FULL_DUPLEX,
	CONFIGURE_100M_FULL_DUPLEX,
	CONFIGURE_10M_FULL_DUPLEX,
	CONFIGURE_AUTONEG,
	CONFIGURE_LAST_SPEED,
}eSfpSpeedCfg;

typedef enum
{
	SFP_STATE_OUT,
	SFP_STATE_IN,
	SFP_STATE_LAST,
}eSfpState;

typedef struct
{
	eSfpSpeedCfg 	speedStatus;
	int				isLinkUp;
	int				isFullDuplex;
}tSfpStatus,tSgmiiStatus;

typedef enum
{
	LINK_STATUS_DOWN,
	LINK_STATUS_UP
}eLinkStatus;

typedef struct
{
	int isValidInterface;
	int sfp_number;
	int enetinterface;
	int toggle_in_interface;
	int toggle_in_interface_data;
	int toggle_out_interface;
	int toggle_out_interface_data;
	int sfp_state;
	int config_status_complete;
	eSfpSpeedCfg sfpSpeed;
	eLinkStatus lastLinkStatus;
	MEA_InterfaceType_t interfaceType;
	int bitmap_advertisement;
	int	mdio_address;
	int restart_autoneg;
    int link_down_countetr;
    int lastSpeedStataus;
    int fiber;
    int sfp_prev_state;
    int mdio_bus;
    int init_configuration;

}tSfpInterfaceDb;

#define MDIO_CTRL_IOC_MAGIC		'm'

#define MDIO_READ	            	_IO(MDIO_CTRL_IOC_MAGIC, 1)
#define MDIO_WRITE	         		_IO(MDIO_CTRL_IOC_MAGIC, 2)
#define MDIO_WRITE_PAGE_SPECIFIC 	_IO(MDIO_CTRL_IOC_MAGIC, 3)
#define MDIO_READ_PAGE_SPECIFIC  	_IO(MDIO_CTRL_IOC_MAGIC, 4)
#define RMDIO_IOC_MAXNUM	   		4

typedef struct
{
   unsigned short int value;
   int mii_id;
   int regnum;
   int page;
} mdio_interface_params;

void mea_sfp_change_speed(MEA_Interface_t Id,eSfpSpeedCfg speed,int adv);
MEA_Status mea_sfp_open_connection(void);
void mea_sfp_periodic_task(void);
void mea_setting_sfp_interface(MEA_Interface_t Id,MEA_InterfaceType_t interfaceType);
void mea_init_sfp_configuration(void);
MEA_Status mea_sfp_close_connection(void);
MEA_Status mdio_set_parameters(int mdio_device, int mii_id, int reg, unsigned short val,int page);
MEA_Status mdio_get_parameters(int mdio_device, int mii_id, int reg, unsigned short *val,int page);
MEA_Status init_socket_connection_vdslk7(void);
#ifdef __cplusplus
}
#endif
#endif


