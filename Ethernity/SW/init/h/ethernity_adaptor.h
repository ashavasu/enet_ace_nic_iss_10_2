/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/



#include "MEA_platform.h"

#if defined(MEA_OS_Z1) || defined(ADOPT_ETHERNITY_FPGA) 

#if !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)

#include <time.h>
#include "switch.h"
#include "global.h"
#include "os.h"
#include "qlm.h"
#include "qcm.h"
#include "qrm.h"
#include "mbuf.h"
#include "cbuf.h"
#include "filter.h"
#include "spt.h"
#include "timer.h"
#include "chann.h"
#include "avltree.h"
#include "bridge.h"
#include "dixframe.h"
#include "iface.h"
#include "ndis_lib.h"
#include "device.h"
#include "sw_pkt.h"
#include "sw_drv.h"
#include "NEC_QoS_drv.h"
#include "NEC_FPGA_log.h"
#include "NEC_QoS_drv_proc.h"
#include "rpgeneric.h"
#include "rplocal.h"
#include "neclogs.h"
#include "sys_test.h"
#include "isrras.h"
#include "sptgen.h"
#include "nec_qos_drv_init_wrapper.h"
#include "rptagswapcos_api.h"
#include "rptagswapvid_api.h"
#include "rpqos_retagswapvidcos_api.h"
#include "rpqos_retagswapvid_api.h"
#include "rpqosethertype_api.h"
#include "rpqos_api.h"
#include "rpvlan1q_api.h"
#include "rpvlan1q_qos_api.h"
#include "rpvlangroup_api.h"
#include "rpport_api.h"
#include "serviceclassinfo.h"
#include "mar88e1111.h"

typedef Fpga_info_t          MEA_adaptor_Fpga_info_t;
typedef Fpga_Config_Change_t MEA_adaptor_Fpga_Config_Change_t;
typedef GettingRec_ServClass_struct MEA_adaptor_GettingRec_ServClass_struct;

typedef GettingRec_ServClassCnt_struct MEA_adaptor_GettingRec_ServClassCnt_struct;




#else /* MEA_OS_LINUX */ 

#include "MEA_platform.h"
#include "MEA_platform.h"


#define SP_QOS_IPVERSION_ALL            0
#define SP_QOS_IPVERSION_IPV4           1
#define SP_QOS_IPVERSION_IPV6           2

#define SP_QOS_OPTION_DISABLE		    0
#define	SP_QOS_OPTION_TOS_STD		    1
#define SP_QOS_OPTION_PRECEDENCE	    2
#define SP_QOS_OPTION_RETAG	            3

#define SP_CONFIGURED 1

typedef struct
{
	MEA_Uint32 active;
	MEA_Uint32 VidUser_32;		/* user side VLAN_ID */
	MEA_Uint32 VidUplink_32;	/* netowkr site VLAN_ID */
} MEA_adaptor_Tag_Swap_Table_Entry_t;

typedef struct
{
	int num_entry;
	MEA_adaptor_Tag_Swap_Table_Entry_t Tag_Swap_Table_Entry[8];
} MEA_adaptor_Tag_Swap_Table_t;


typedef struct MEA_adaptor_spQosTos2Prio_s {
	MEA_Uint32	flags;
	MEA_Uint8 	tos;
	MEA_Uint8   priority;
	MEA_Uint8 	profile_index;	/*for remote QoS feature, leah*/
	MEA_Uint8	filler;
} MEA_adaptor_spQosTos2Prio_t;

typedef struct MEA_adaptor_spQosPrecedence2Prio_s {
	MEA_Uint32  flags;
	MEA_Uint8	precedence;
	MEA_Uint8   priority;
	MEA_Uint8 	profile_index;	/*for remote QoS feature, leah*/
	MEA_Uint8	filler;
} MEA_adaptor_spQosPrecedence2Prio_t;


typedef struct spQosEtherType_s {
        MEA_Uint32  flags;
        MEA_Uint16  etherType;
        MEA_Uint8   priority;
        MEA_Uint8   profile_index;  /*for remote QoS feature, leah*/
} MEA_adaptor_spQosEtherType_t;

typedef struct
{
	int mode;
	int num_entry;
	MEA_adaptor_spQosEtherType_t etherType[8];
} MEA_adaptor_EtherType_t;

typedef struct
{
	MEA_Uint16 valid;
	MEA_Uint16 vid;
} MEA_adaptor_MVidType_t;

#define NEC_FPGA_API_PORT_CLOSE_VDSL_MASK 0xffff

typedef struct
{
	/* Uplink output limit value */
	MEA_Uint16					uplink_policy_set;					/* API:2, 0~1000 Mbps */

	/* Link Speed of Up & Downlink */
	MEA_Uint8					ul_speed;							/* API:3, 0:0, 1:10, 2:100, 3:1000M */
	MEA_Uint8					dl_speed;							/* API:3, 0:0, 1:100, 2:1000M */
	
	/* Downlink VLAN */
	MEA_Uint32					downlink_vid[17];					/* API:4, Downlink Vlan */
	MEA_Uint32					downlink_vid_entry;					/* API:4, Setting Entry Num */
	
	/* Priority Map */
	MEA_Uint8					userPriority2TrafficClass[8];		/* API:5, Priority to COS */

	/* Class Prolicy Set */
	MEA_Uint32					cosMaxBw[64][8];					/* API:6, Per Port Per Class, unit: kbps */

	/* IPv4/6 COosmap */
	MEA_Uint16					ip_version;							/* API:7.8, IP Version V4, V6, ALL */
	MEA_Uint16					qos_option;							/* API:9.10, Precedence or Tos */
	MEA_adaptor_spQosTos2Prio_t tos[64];							/* API:11~14, TOS to COS */
	MEA_adaptor_spQosPrecedence2Prio_t	precedence[8];						/* API:11~14, Precedence to COS */

	/* EtherType COS */
	MEA_adaptor_EtherType_t				ether_type;							/* API:15~17, EtherType COS Mapping */

	/* PVID Set */
	MEA_Uint16					pvid[64];							/* API:18, Port VID */
	MEA_Uint8					priority[64];						/* API:18, Port Priority */
	MEA_Uint32					pvid_egress_setting;				/* API:19.34, PVID Terminator settint, bit map */

	/* Egress Untag Set */
	MEA_Uint32					egress_drop;						/* API:20, accept or not incoming untag packet */
	
	/* Tag Swap */
	MEA_Uint32					tag_swap_table_vid_map;				/* API:21 */
	MEA_adaptor_Tag_Swap_Table_t tag_swap_table_vid[64];			/* API:22.23 */

	/* Qos Active */
	MEA_Uint8					qos_enable;							/* API:25, QOS on off */

	/* Port Close */
	MEA_Uint32					port_close;							/* API:33, port bitmap Port Close */
	
	/* RCont Down */
	MEA_Uint16					rcont_forward;						/* API:35 */

	/* Up/Downstream Buffer Set */
	MEA_Uint32					upstream_buffer[4];					/* API:39 */
	MEA_Uint32					downstream_buffer[4];				/* API:40 */

	/* Flow Control */
	MEA_Uint32					flow_control;						/* API:59 */

	/* Multicast VID */
	MEA_adaptor_MVidType_t		mvid[4];

	/* MLD priority */
	MEA_Uint16					mld_priority;
	
	/* MLD CoS */
	MEA_Uint16					mld_cos;

	/* MLD enable */
	MEA_Uint16					mld_enable;							/* 1:enable, 0:disable */

} MEA_adaptor_Fpga_info_t;

typedef struct
{
	MEA_Uint32					tos_cos_changed;
	MEA_Uint32					ether_type_changed;
	MEA_Uint32					tag_swap_port_changed;
	MEA_Uint32					buffer_changed;

} MEA_adaptor_Fpga_Config_Change_t;



#define SERVICE_CLASS_TYPE      		4



typedef struct{

    int detect_result;					/*used for High Traffic Observation*/

    MEA_Uint32 ValidFlag;

    MEA_Uint32 Ingress_Rx_Frame_Cnt;		/* as Upstream RX Frame Counter in F7_2 */

    MEA_Uint32 Ingress_Rx_Byte_Cnt;			/* as Upstream RX Byte Counter in F7_2*/

    MEA_Uint32 Ingress_Discard_Frame_Cnt;	/* as Upstream discard Frame Counter in F7_2 */

    MEA_Uint32 Egress_Tx_Frame_Cnt;			/* as Downstream TX Frame Counter in F7_2 */

} MEA_adaptor_GettingRec_ServClassCnt_struct;



/* GettingRec_ServClass_struct, 32-bit counters for gettingRecord in ServiceClassInfo.c */

typedef struct{

    MEA_Uint32 readcnt_flag;		/* used for read count or valid flag */

#ifdef MEA_OS_LINUX

    struct tm sTime;			/* add a struct to record time */

    time_t timeStart;			/* use diff time 900 instead of tick method 2004/06/11 */

#endif /* MEA_OS_LINUX */

    char recordTime[20];		/* due to Stime is a pointer and will be effected by present time */



	/* Per Port Counter */

    MEA_Uint32 Ingress_Rx_Frame_Cnt;		/* as DL_Port_Upstream, UL_Port_Downstream RX Frame Counter in F7_2 */

    MEA_Uint32 Ingress_Rx_Byte_Cnt;			/* as DL_Port_Upstream, UL_Port_Downstream RX Byte Counter in F7_2*/

    MEA_Uint32 Ingress_Discard_Frame_Cnt;	/* as DL_Port_Upstream, UL_Port_Downstream discard Frame Counter in F7_2 */

    MEA_Uint32 Egress_Tx_Frame_Cnt;			/* as DL_Port_Downstream, UL_Port_Upstream TX Frame Counter in F7_2 */

	MEA_Uint32 Error_Frame_Cnt;

	MEA_Uint32 FCS_Frame_Cnt;

	MEA_Uint32 Pause_Frame_Cnt;



    MEA_adaptor_GettingRec_ServClassCnt_struct perClassCnt[SERVICE_CLASS_TYPE];

} MEA_adaptor_GettingRec_ServClass_struct;






#endif /* MEA_OS_LINUX */




ENET_Status ENET_adaptor_init(MEA_adaptor_Fpga_info_t* Fpga_init_info_p);


MEA_Status ENET_adaptor_CounterPerPortGet(int port,

                                         MEA_adaptor_GettingRec_ServClass_struct* ptr);


MEA_Status ENET_adaptor_Link_Status_Change(int port_no,
										   MEA_adaptor_Fpga_info_t* Fpga_init_info_p);

MEA_Status ENET_adaptor_Vdsl_Link_Status_Change(int port_no,
										        MEA_adaptor_Fpga_info_t* fpga_info_p,
												MEA_Uint32 speed /* k bps */);

#endif /* defined(MEA_OS_Z1) || defined(ADOPT_ETHERNITY_FPGA) */










