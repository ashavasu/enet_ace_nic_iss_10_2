#if 0
/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*

   Example for the next test description:
   1. For the following flows, CVID 1-4 are PPPoE and CVID 5-7 are IPoE flows.

   2. The device must perform the following VLAN manipulations in Upstream: 
      a. Translate     flows with 
              CVID=1-6          & SVID=X   , Port P 
              CVID=100*P+X      & SVID=1001;  {X=1,2,�,32; P=1,2,�,8}
      b. Translate     flow  with 
              CVID=7            & SVID=X   , Port P
              CVID=100*P+X+1000 & SVID=2001; {X=1,2,�,32; P=1,2,�,8}
      c. Translate all flows with 
              no CVID           & SVID=X   , any Port and ET=IPv4   
              no CVID           & SVID=4001; {X=1,2,�,32} 

   3. Downstream traffic is symmetric to the upstream traffic. Note that the device must learn the SA and CVID tag in Upstream and, for flows (2.a) it must choose the new CVID based on the Downstream DA
      a. Translate flows with 
              SVID=1001 & CVID=100*P+X 
              SVID=X    & CVID=Learned (1-6);  {X=1,2,�,32; P=1,2,�,8}
      b. Translate flows with 
              SVID=2001 & CVID= CVID=100*P+X+1000'
              SVID=X    & CVID=7;              {X=1,2,�,32; P=1,2,�,8}
      c. Translate flows with SVID=4001 & no CVID '
              no CVID   & SVID=33              (multicast stream)

   4. Downstream multicast data traffic uses SVID=4001 and no CVID tag. 
      This stream must be replicated to all the ports.
      
   5. Shape downstream & upstream traffic to the following rates: [DL] What about policer, as you would like to guaranty Green flows, and yellow flows shall pass through WRED?  We do rate limiting/policng internally per flow, shaping is done per port or logical queue, from what I can see you ask to perform shaping per priority queue, which will require additional design from our side, and can be delivered within a month time frame. For the demo we will do this rate limiting using our MEF 10 compliance policer per flow.  Please also note that we do specific rate limiting per prototcol, sich that if SVID 4000 represent IGMP message we can configure policer for IGMP BPDUs, or any other protocol.
      a. SVID=1001    flows   4.0Mbps each flow (total 7*256*4=7,168Mbps)
      b. SVID=2001    flows   2.0Mbps each flow (total 256*2=512Mbps)
      c. SVID=4001 US flows   0.1Mbps each flow (total 256*0.1=25.6Mbps)
      d. SVID=4001 DS flow   30.0Mbps
      e. Each downstream GMII port is shaped to 970Mbps.
   
   6. Enable IGMPv3 Proxy and IPv4 SSM distribution functionality.
   
   7. Join 239.1.1.1 from IPSA 10.0.1.1 upstream flow SVID=1 & no CVID, port 1
   
   8. Join 239.1.1.1 from IPSA 10.0.2.1 upstream flow SVID=1 & no CVID, port 2
   
   9. Join 239.1.1.2 from IPSA 10.0.2.1 upstream flow SVID=1 & no CVID, port 2
   
  10. Join 239.1.1.2 from IPSA 10.0.2.2 upstream flow SVID=2 & no CVID, port 2
  
  11. Join 239.1.1.2 from IPSA 10.0.3.1 upstream flow SVID=1 & no CVID, port 3
  
  12. Schedueling and prioritization - all the frames are marked as priority 0 
      but the device must classify them according to the following rules, 
      based on the DS egress marking (no change in priority marking):  
      a. CVID=1  lowest priority (0)
      b. CVID=2  priority (1)
      c. CVID=3  priority (2)
      d. CVID=4  priority (3)
      e. CVID=5  priority (4)
      f. CVID=6  priority (5)
      g. CVID=7   priority (6)
      h. SVID=127   highest priority (7)
      i. There must be two levels of scheduling. 
         First frames are egressed according to their priority. 
         Within each priority group, flows are given equal weight in DWRR arbitration.

  13. Check the data content and packet loss condition in both directions 

  14. Check that multicast traffic is distributed according to IGMP

  15. Check that scheduling is performed according to rules defined in section (?12?5)
  
  16. Check that shaping is according to rules in section (?5). 
      In case that there is over subscription on one port, 
      the scheduling rules must be respected so the impact 
      is equally distributed on flows from the lowest priority.
      
  17. Migration prevention: 
      check that if any learned MAC address from the user side changes CVID or SVID, 
      the AT table doesn't permit the migration, 
      the flow is discarded 
      and the management CPU receives an indication of the event, 
      containing original and new tags.         

      */



/*---------------------------------------------------------------------------*/
/*        Includes                                                           */
/*---------------------------------------------------------------------------*/
#include "mea_drv_common.h"
#include "mea_api.h"
#include "enet_queue_drv.h"





/*---------------------------------------------------------------------------*/
/*        Defines                                                            */
/*---------------------------------------------------------------------------*/


#define MEA_EXAMPLE3_MAX_NUMBER_OF_USER_PORTS (12)

#define MEA_EXAMPLE3_MAX_NUMBER_OF_QUEUE_NETWORK (2)

/*---------------------------------------------------------------------------*/
/*        Typedefs                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*        Globals                                                            */
/*---------------------------------------------------------------------------*/

static MEA_Bool   MEA_Example3_Cluster_mod = MEA_FALSE; // 0 STRICT 1 WFQ
static MEA_Bool   MEA_Example3_PRIQueue_mod = MEA_FALSE; // 0 STRICT 1 WFQ

static MEA_Port_t MEA_Example3_network_port ;
static MEA_Uint32 MEA_Example3_num_of_user_ports;
static MEA_Port_t MEA_Example3_user_ports[MEA_EXAMPLE3_MAX_NUMBER_OF_USER_PORTS];
static MEA_Uint32 MEA_Example3_network_portQueue[MEA_EXAMPLE3_MAX_NUMBER_OF_QUEUE_NETWORK];


/*---------------------------------------------------------------------------*/
/*        Implementation                                                      */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_init_globals>                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_init_globals() {

    MEA_Uint32 i;
    MEA_subSysType_te subSysType;
    MEA_Uint32            dstIp;
    MEA_Uint32            sourceIp;
    MEA_Globals_Entry_dbt entry;

 
    subSysType=MEA_subSysType;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                             "MEA_subSysType = %d \n",
                             subSysType);

    /* Init to undefined values */
    MEA_Example3_network_port      = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Example3_num_of_user_ports = 0;
    for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_Example3_user_ports);i++) {
        MEA_Example3_user_ports[i] = MEA_PLAT_GENERATE_NEW_ID;
    }

    /* Init globals */
    MEA_Example3_network_port      = 125;
    
    if(subSysType == MEA_SUB_SYS_TYPE_ENET3500_4G){
        MEA_Example3_num_of_user_ports = 3;
        MEA_Example3_user_ports[0]     = 126;
        MEA_Example3_user_ports[1]     = 48;
        MEA_Example3_user_ports[2]     = 72;
    }
    MEA_Example3_network_portQueue[0] = 125;
    MEA_Example3_network_portQueue[1] = 1;

/* config reg 0x cc*/
#if defined(MEA_ADSL_MODE_64) 
	MEA_API_WriteReg(MEA_UNIT_0,
        ENET_IF_GLOBAL2_REG, 
		0xA0007840,
		MEA_MODULE_IF);
#else
    MEA_API_WriteReg(MEA_UNIT_0,
        ENET_IF_GLOBAL2_REG, 
                     0x40007840,
                     MEA_MODULE_IF);
#endif

#if 1
    MEA_API_WriteReg(MEA_UNIT_0,
                     0x144, 
                     0x00000040,
                     MEA_MODULE_IF);
#endif
    dstIp = MEA_OS_inet_addr("224.1.1.1"); 
    sourceIp = MEA_OS_inet_addr("192.100.10.1");


    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Get_Globals_Entry failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

 
    entry.Globals_Device.MC_ServerIp.DestIp   = dstIp;
    entry.Globals_Device.MC_ServerIp.sourceIp = sourceIp;
    
    entry.policer_enable = MEA_FALSE;
    entry.shaper_cluster_enable  = MEA_TRUE;
    entry.shaper_port_enable     = MEA_TRUE;
    entry.shaper_priQueue_enable = MEA_TRUE;


    entry.bcst_drp_lvl= 500;

    if(MEA_OS_atohex_MAC("00:00:00:00:00:55", &(entry.CFM_OAM.UC_Mac_Address))!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,": MEA_OS_atohex_MAC fail \n");
                     return MEA_ERROR;
    }
    

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Get_Globals_Entry failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }


    
   


    /* return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_services>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_delete_all_services() {

   MEA_Service_t db_serviceId;
   MEA_Service_t temp_db_serviceId;
   MEA_ULong_t    db_cookie;

   /* Get first service */
   if (MEA_API_GetFirst_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK)  {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_GetFirst_Service failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* loop until no more services */
   while (db_cookie != 0) { 


       /* Save the current db_serviceId */
       temp_db_serviceId = db_serviceId;

       /* Make next before delete the object */ 
       if (MEA_API_GetNext_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetNext_Service failed (db_serviceId=%d)\n",
                             __FUNCTION__,db_serviceId);
           return MEA_ERROR;
       }

       /* Delete the save db_serviceId */
       if (MEA_API_Delete_Service(MEA_UNIT_0,temp_db_serviceId)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete service %d\n",
                             __FUNCTION__,temp_db_serviceId);
           return MEA_ERROR;
       }
        
   }
 

   /* Return to caller */
   return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_filters>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_delete_all_filters() {

   MEA_Filter_t      filter_id;
   MEA_Filter_t      temp_filter_id;
   MEA_Bool          found;


   /* Get the first filter */
   if (MEA_API_GetFirst_Filter(MEA_UNIT_0,&filter_id, &found) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - failed to GetFirst filter \n",
                          __FUNCTION__);
        return MEA_ERROR;
   }
 
   /* loop until no more filters */
   while (found) {

       /* Save the filter id that need to be delete , before the getNext */
       temp_filter_id = filter_id;
       
            
       /* Get the next filter */
       if (MEA_API_GetNext_Filter(MEA_UNIT_0,
                                   &filter_id,
                                   &found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to GetNext filter %d\n",
                             __FUNCTION__,filter_id);
            return MEA_ERROR;
       }

       /* Delete the save filter */
       if (MEA_API_Delete_Filter(MEA_UNIT_0,temp_filter_id)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete filter %d\n",
                             __FUNCTION__,temp_filter_id);
           return MEA_ERROR;
       }

   }

    
   /* Return to caller */
   return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_forwrder_entries>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_delete_all_forwarder_entries() {

   if (MEA_API_DeleteAll_SE(MEA_UNIT_0) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_DeleteAll_DSE failed \n",
                         __FUNCTION__);
      return MEA_ERROR;
   }

   /* Return to caller */
   return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_actions>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
 MEA_Status MEA_Example3_delete_all_actions() {
 
       
   return MEA_API_DeleteAll_Action(MEA_UNIT_0);

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_limiters>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_delete_all_limiters() {

   MEA_Limiter_t      db_limiterId;
   MEA_Limiter_t      temp_db_limiterId;
   MEA_Bool           found;

   /* Get the first limiter */
   if (MEA_API_GetFirst_Limiter(MEA_UNIT_0,&db_limiterId,&found) != MEA_OK)  {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_GetFirst_Limiter failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
   }

   /* loop until no more limiter */
   while (found) { 


       /* Save the current db_limiterId */
       temp_db_limiterId = db_limiterId;

       /* Make next before delete the object */
       if (MEA_API_GetNext_Limiter(MEA_UNIT_0,&db_limiterId,&found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetNext_Limiter failed (db_limiterId=%d)\n",
                             __FUNCTION__,db_limiterId);
           return MEA_ERROR;
       }

       /* Ignore the specail limiter */
       if (temp_db_limiterId == MEA_LIMITER_DONT_CARE) {
            continue;
       }
       
       /* Delete the save db_limiterId */
       if (MEA_API_Delete_Limiter(MEA_UNIT_0,temp_db_limiterId)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete limiter %d\n",
                             __FUNCTION__,temp_db_limiterId);
           return MEA_ERROR;
       }
   }

   /* Return to caller */
   return MEA_OK;

}









/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_LxCPs>                                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_delete_all_LxCPs() {

   MEA_LxCp_t id;
   MEA_LxCp_t temp_id;
   MEA_Bool   found;


   /* Get the first filter */
   if (MEA_API_GetFirst_LxCP(MEA_UNIT_0,&id, NULL,&found) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - failed to GetFirst action \n",
                          __FUNCTION__);
        return MEA_ERROR;
   }
 
   /* loop until no more lxcp profiles */
   while (found) {

       /* Save the id that need to be delete , before the getNext */
       temp_id = id;
       
            
       /* Get the next LxCP Profile */
       if (MEA_API_GetNext_LxCP(MEA_UNIT_0,
                                &id,
                                NULL,
                                &found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to GetNext LxCP %d\n",
                             __FUNCTION__,id);
            return MEA_ERROR;
       }

       /* Delete the save LxCP Profile */
       if (MEA_API_Delete_LxCP(MEA_UNIT_0,temp_id)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete LxCP %d\n",
                             __FUNCTION__,temp_id);
           return MEA_ERROR;
       }

   }

    
   /* Return to caller */
   return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_queues>                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_delete_all_queues() {

    ENET_QueueId_t queueId;
    ENET_QueueId_t next_queueId;
    ENET_Bool      found;
    ENET_Queue_dbt entry;

    if (ENET_GetFirst_Queue  (ENET_UNIT_0,
                             &queueId,
                             &entry,
                             &found)!=ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ENET_GetFirst_Queue failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    while (found) {
        next_queueId = queueId;
        if (ENET_GetNext_Queue (ENET_UNIT_0,
                                &next_queueId,
                                &entry,
                                &found)!=ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_GetNext_Queue failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if ((queueId != ENET_PLAT_PORT_127_DEFAULT_CLUSTER   ) && 
            (queueId != ENET_PLAT_PORT_24_TLS_DEFAULT_CLUSTER)  ) {
            if (ENET_Delete_Queue (ENET_UNIT_0,
                                   queueId)!=ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - ENET_Delete_Queue failed \n",
                                  __FUNCTION__);
                return MEA_ERROR;
            }
        }
        queueId = next_queueId;
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_restore_globals>                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_restore_globals() {

    MEA_Globals_Entry_dbt entry;

    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_Globals_Entry failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    entry.if_global1.val.forwarder_enable = 1;
    entry.if_global1.val.Lxcp_Enable = 1;
    entry.policer_enable = 1;
   
     entry.shaper_priQueue_enable   =  1;
     entry.shaper_cluster_enable    =  1;
     entry.shaper_port_enable       =  1;
   

    entry.Cluster_mode = MEA_GLOBAL_CLUSTER_MODE_DEF_VAL;
    entry.pri_q_mode   = MEA_GLOBAL_PRI_Q_MODE_DEF_VAL;
    entry.bm_config_bmqs = 1;
    entry.bm_config_mqs = 0;

    if (MEA_API_Set_Globals_Entry(MEA_UNIT_0,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_Globals_Entry failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_entities>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_delete_all_entities() {


    /* Delete all services */
    if (MEA_Example3_delete_all_services() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_delete_all_services failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Delete all filters */
    if (MEA_Example3_delete_all_filters() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_delete_all_filters failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

     /* Delete all forwarder entries */
    if (MEA_Example3_delete_all_forwarder_entries() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_delete_all_forwarder_entries failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Delete all actions */
    if (MEA_Example3_delete_all_actions() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_delete_all_actions failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Delete all limiters */
    if (MEA_Example3_delete_all_limiters() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_delete_all_limiters failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }


    /* Delete all LxCP */
    if (MEA_Example3_delete_all_LxCPs() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_delete_all_LxCPs failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Delete all Queues */
    if (MEA_Example3_delete_all_queues() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_delete_all_queues failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Restore globals */
    if (MEA_Example3_restore_globals() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_restore_globals failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Return to caller */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_create_queues>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_create_queues() {

    MEA_Uint32 port_num;
    MEA_Uint32 i;
    ENET_Queue_dbt entry;
    ENET_QueueId_t id;
    MEA_Uint32     priQ,real_priQ;
    MEA_Uint32     shaper_vec[8]; 
    MEA_Uint32     shaper_vec_pri[8]; 
    MEA_Uint32     wfq_vec_pri[8];     
    
    shaper_vec[7]= 360000000; 
    shaper_vec[6]= 280000000;
  	shaper_vec[5]= 210000000;
  	shaper_vec[4]= 150000000;
  	shaper_vec[3]= 100000000;
  	shaper_vec[2]= 60000000;
  	shaper_vec[1]= 30000000;
  	shaper_vec[0]= 10000000;
  	

    shaper_vec_pri[0]=  10000000;
    shaper_vec_pri[1]=  20000000;
    shaper_vec_pri[2]=  30000000;
    shaper_vec_pri[3]=  40000000;
    shaper_vec_pri[4]=  50000000;
    shaper_vec_pri[5]=  60000000;
    shaper_vec_pri[6]=  70000000;
    shaper_vec_pri[7]=  80000000;

    wfq_vec_pri[0] = 8;
    wfq_vec_pri[1] = 16;
    wfq_vec_pri[2] = 16;
    wfq_vec_pri[3] = 32;
    wfq_vec_pri[4] = 32;
    wfq_vec_pri[5] = 64;
    wfq_vec_pri[6] = 64;
    wfq_vec_pri[7] = 96;

    
    /* Create downstream cluster queue */
    for (port_num=0;port_num<MEA_Example3_num_of_user_ports;port_num++) {
        for (i=0;i<=7;i++) {
            /* Init the entry */
            MEA_OS_memset(&entry,0,sizeof(entry));
            MEA_OS_strcpy(entry.name,ENET_PLAT_GENERATE_NEW_NAME);
            entry.port.type    = ENET_QUEUE_PORT_TYPE_PORT;
            
            entry.port.id.port = (ENET_PortId_t)(MEA_Example3_user_ports[port_num]);
            if(MEA_Example3_Cluster_mod == MEA_FALSE){ //strict mode
            
              if (i < 7) {
                    entry.mode.type                  = ENET_QUEUE_MODE_TYPE_STRICT;
                    entry.mode.value.strict_priority = (MEA_Uint8)(7-i);
                } else {
                    entry.mode.type                  = ENET_QUEUE_MODE_TYPE_WFQ;
                    entry.mode.value.wfq_weight      = (MEA_Uint8) (8 +((7-i)*8));
                }
            }else{
                entry.mode.type                  = ENET_QUEUE_MODE_TYPE_WFQ;
                entry.mode.value.wfq_weight      = (MEA_Uint8) wfq_vec_pri[7-i];

            }
            
            //entry.MTU = MEA_QUEUE_MTU_MAX_VALUE;
            entry.shaper_enable =MEA_TRUE;
            entry.Shaper_compensation=ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
            entry.shaper_info.CIR = shaper_vec[7-i];
            entry.shaper_info.CBS = 320000;
            entry.shaper_info.resolution_type  = ENET_SHAPER_RESOLUTION_TYPE_BIT;
            entry.shaper_info.overhead = 0;
            entry.shaper_info.Cell_Overhead = 0;
            for(priQ=0;priQ<MEA_NUM_OF_ELEMENTS(entry.pri_queues);priQ++) {
                 
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL(priQ);
                entry.pri_queues[priQ].max_q_size_Byte    = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL(priQ);
                if(MEA_Example3_PRIQueue_mod == MEA_TRUE){                
                    entry.pri_queues[priQ].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                    entry.pri_queues[priQ].mode.value.wfq_weight=(ENET_Uint8)wfq_vec_pri[priQ];
                } else {
                entry.pri_queues[priQ].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                entry.pri_queues[priQ].mode.value.strict_priority= 0;//(ENET_Uint8)priQ;
                }
                entry.pri_queues[priQ].shaperPri_enable =MEA_TRUE;
                entry.pri_queues[priQ].shaper_info.CIR = shaper_vec_pri[priQ];
                entry.pri_queues[priQ].shaper_info.CBS = 32000;
                entry.pri_queues[priQ].shaper_info.Cell_Overhead = 0;
                entry.pri_queues[priQ].shaper_info.overhead = 0; 
                entry.pri_queues[priQ].shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
                entry.pri_queues[priQ].Shaper_compensation         = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
                
            }
            /* create the queue */
            id = (ENET_QueueId_t)(37+(8*port_num)-i);
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," port %d cluster id=%d\n",MEA_Example3_user_ports[port_num],id);

            if (ENET_Create_Queue (ENET_UNIT_0,
                                   &entry,
                                   &id) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - ENET_Create_Queue failed (id=%d) port %d\n",
                                  __FUNCTION__,id,MEA_Example3_user_ports[port_num]);
              //  return MEA_ERROR; 
            }
        } // 
    }

    /* Create upstream cluster queue */
    MEA_OS_memset(&entry,0,sizeof(entry));
        for (i=0;i<MEA_EXAMPLE3_MAX_NUMBER_OF_QUEUE_NETWORK;i++) {
            /* Init the entry */
            MEA_OS_memset(&entry,0,sizeof(entry));
            MEA_OS_strcpy(entry.name,ENET_PLAT_GENERATE_NEW_NAME);
            entry.port.type    = ENET_QUEUE_PORT_TYPE_PORT;
            entry.port.id.port = (ENET_PortId_t)(MEA_Example3_network_port);
            if (MEA_Example3_network_port != 125) {
                if (i < 7) {
                    entry.mode.type                  = ENET_QUEUE_MODE_TYPE_STRICT;
                    entry.mode.value.strict_priority = (MEA_Uint8)(7-i);
                } else {
                    entry.mode.type                  = ENET_QUEUE_MODE_TYPE_WFQ;
                    entry.mode.value.wfq_weight      = 20;
                }
            } else {
                if (i < 6) {
                    entry.mode.type                  = ENET_QUEUE_MODE_TYPE_STRICT;
                    entry.mode.value.strict_priority = (MEA_Uint8)(7-i);
                } else {
                    entry.mode.type                  = ENET_QUEUE_MODE_TYPE_WFQ;
                    entry.mode.value.wfq_weight      = 20;
                }
            }
            //entry.MTU = MEA_QUEUE_MTU_MAX_VALUE;
            entry.shaper_enable =MEA_TRUE;
            entry.Shaper_compensation=ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
            entry.shaper_info.CIR = 1000000000;
            entry.shaper_info.CBS = 320000;
            entry.shaper_info.resolution_type  = ENET_SHAPER_RESOLUTION_TYPE_BIT;
            entry.shaper_info.overhead = 20;
            entry.shaper_info.Cell_Overhead = 0;
            for(priQ=0;priQ< MEA_NUM_OF_ELEMENTS(entry.pri_queues);priQ++) {
                real_priQ= 7 -priQ; 
                 entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL(priQ);
                entry.pri_queues[priQ].max_q_size_Byte    = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL(priQ);
#if 0
                entry.pri_queues[priQ].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                entry.pri_queues[priQ].mode.value.wfq_weight=20;
#else
                entry.pri_queues[priQ].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                entry.pri_queues[priQ].mode.value.strict_priority = 0;//(ENET_Uint8)priQ;

#endif
               
            }

           
            entry.shaper_enable = MEA_FALSE;
            


            /* create the queue */
            id = (ENET_QueueId_t)(MEA_Example3_network_portQueue[i]);
            if (ENET_Create_Queue (ENET_UNIT_0,
                                   &entry,
                                   &id) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - ENET_Create_Queue failed (id=%d) network_portQueue %d\n",
                                  __FUNCTION__,id,MEA_Example3_network_port);
               // return MEA_ERROR; 
            }
        }




    /* Return to caller */
    return MEA_OK;

}

MEA_Status MEA_Example3_configIngressAdminON()
{
    
    MEA_Port_t port;
    MEA_IngressPort_Entry_dbt entry;

        
    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++){
    
        if (MEA_API_Get_IsPortValid(port,MEA_TRUE)==MEA_FALSE) {
            continue;
        }


        if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Get_IngressPort_Entry failed (id=%d)\n",
                      __FUNCTION__,port);
            return MEA_ERROR;
        }
        entry.rx_enable=MEA_TRUE;
        if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                  port,
                                  &entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Set_IngressPort_Entry failed (port=%d)\n",
                      __FUNCTION__,
                      port);
        return MEA_ERROR;
        }

    }

    
    return MEA_OK;
}

MEA_Status MEA_Example3_configIngressAdminOFF()
{

    MEA_Port_t port;
    MEA_IngressPort_Entry_dbt entry;

        
    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++){
    
        if (MEA_API_Get_IsPortValid(port,MEA_TRUE)==MEA_FALSE) {
            continue;
        }


        if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Get_IngressPort_Entry failed (id=%d)\n",
                      __FUNCTION__,port);
            return MEA_ERROR;
        }
        entry.rx_enable=MEA_FALSE;
        if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                  port,
                                  &entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Set_IngressPort_Entry failed (port=%d)\n",
                      __FUNCTION__,
                      port);
        return MEA_ERROR;
        }

    }

    
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_config_Shaper_port>                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/


MEA_Status MEA_Example3_config_Shaper_port(MEA_Port_t  port, MEA_Bool enable, MEA_EirCir_t CIR)
{

    MEA_EgressPort_Entry_dbt  egressPort_entry; 

   
    if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }


    egressPort_entry.shaper_enable   = enable;
    egressPort_entry.shaper_info.CIR =CIR;
    egressPort_entry.shaper_info.CBS = 32000;
    egressPort_entry.shaper_info.Cell_Overhead = 0;
    egressPort_entry.shaper_info.overhead = 0;
    
    egressPort_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
    egressPort_entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_config_port>                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_config_port(MEA_Port_t              port,
                                    MEA_IngressPort_Proto_t ingressProtocol) 
{

    MEA_IngressPort_Entry_dbt ingressPort_entry; 
    MEA_EgressPort_Entry_dbt  egressPort_entry; 
    MEA_Parser_Entry_dbt      parser_entry;
    MEA_EirCir_t CIR;

    if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
                                      port,
                                      &ingressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_IngressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }

    ingressPort_entry.parser_info.port_proto_prof = ingressProtocol;
    ingressPort_entry.parser_info.port_cos_aw = 1;
    ingressPort_entry.parser_info.port_col_aw = 0;
    ingressPort_entry.parser_info.port_ip_aw = 0;
    ingressPort_entry.parser_info.sp_wildcard_valid  = 0;
    ingressPort_entry.parser_info.sp_wildcard        = 0;
    ingressPort_entry.parser_info.net_wildcard_valid = 0;
    ingressPort_entry.parser_info.net_wildcard       = 0;
    
    //ingressPort_entry.parser_info.cfm_oam_qtag  = MEA_TRUE;
    //ingressPort_entry.parser_info.cfm_oam_tag   = MEA_TRUE;
    //ingressPort_entry.parser_info.cfm_oam_untag = MEA_TRUE;

    if (port != MEA_Example3_network_port) {
        ingressPort_entry.parser_info.pri_wildcard_valid = 0;
        ingressPort_entry.parser_info.pri_wildcard       = 0;
    } else {
        ingressPort_entry.parser_info.pri_wildcard_valid = 1;
        ingressPort_entry.parser_info.pri_wildcard       = 7;
    }
    //ingressPort_entry.rx_enable = 1;

    if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                      port,
                                      &ingressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_IngressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }

    if (port != MEA_Example3_network_port) {
        if (MEA_API_Get_Parser_Entry(MEA_UNIT_0,
                                     port,
                                     &parser_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_Parser_Entry failed (port=%d)\n",
                              __FUNCTION__,
                              port);
            return MEA_ERROR;
        }

        /* Define the classifier priority as the c-vid */
        parser_entry.f.vlan_priority_valid  = 1;
        parser_entry.f.vlan_priority_offset = 19;
        parser_entry.f.vlan_priority_mask   = 0x7;

        
        /* Define the network priority to be always zero by take the lsb of the etherType
          This done to force classifier priority 0 in case of no c-vid */
        parser_entry.f.net_priority_valid  = 1;
        parser_entry.f.net_priority_offset = 13;
        parser_entry.f.net_priority_mask   = 0x7;

        /* Define the priority rule to take first the vlan (c-vid) 
           and then the network (0) */
        parser_entry.f.pri_rule = 2;

        if (MEA_API_Set_Parser_Entry(MEA_UNIT_0,
                                     port,
                                     &parser_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_Parser_Entry failed (port=%d)\n",
                              __FUNCTION__,
                              port);
            return MEA_ERROR;
        }
    }

    if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }

    if (ingressProtocol == MEA_INGRESS_PORT_PROTO_QTAG_OUTER) {
        egressPort_entry.proto = MEA_EGRESS_PORT_PROTO_QTAG;
    } else {
        egressPort_entry.proto = (MEA_EgressPort_Proto_t)ingressProtocol;
    }
    //egressPort_entry.tx_enable = 1;
    egressPort_entry.calc_crc = 1;

    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }

    if (port != MEA_Example3_network_port) {
        
        switch (port){
        case  48 :
            CIR = 350000000;
            break;
        case 72:
            CIR = 840000000;
            break;
        case 126:
            CIR = 100000000;
        break;
        default:
             CIR = 1000000000;
            break;

        }
         
        MEA_Example3_config_Shaper_port(port,MEA_TRUE,CIR);
    }

    /* Return to caller */
    return MEA_OK;

}



MEA_Status MEA_Example3_configEgressAdminON()
{
    
    MEA_Port_t port;
    MEA_EgressPort_Entry_dbt entry;

        
    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++){
    
        if (MEA_API_Get_IsPortValid(port,MEA_TRUE)==MEA_FALSE) {
            continue;
        }
        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
            return MEA_ERROR;
        }
        entry.tx_enable=MEA_TRUE;
       if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
        }

    }

    
    return MEA_OK;
}

MEA_Status MEA_Example3_configEgressAdminOFF()
{

MEA_Port_t port;
    MEA_EgressPort_Entry_dbt entry;

        
    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++){
    
        if (MEA_API_Get_IsPortValid(port,MEA_TRUE)==MEA_FALSE) {
            continue;
        }


       if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
        }
        entry.tx_enable=MEA_FALSE;
        if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
        }

    }
    
    return MEA_OK;
}












/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_define_upstream_servicesTLS>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_Example3_define_upstream_servicesTLS()
{
#if 0
    MEA_LxCp_t                           LxCP_id;
    MEA_LxCP_Protocol_key_dbt            LxCP_Protocol_key;
    MEA_LxCP_Protocol_data_dbt           LxCP_Protocol_data;
    MEA_Uint32                           P,x;
    MEA_Uint32                           usr_svid,usr_cvid;
    MEA_Uint32                           net_svid,net_cvid;
    MEA_Limiter_t                        limiter_id;
    MEA_Limiter_Entry_dbt                limiter_entry;
    MEA_Action_Entry_Data_dbt            action_data; 
    MEA_Policer_Entry_dbt                action_policer;
   	MEA_EHP_Info_dbt                     action_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt action_editing;
    MEA_Action_t                         action_id;
    MEA_Service_Entry_Key_dbt            service_key;
    MEA_Service_Entry_Data_dbt           service_data;
    MEA_OutPorts_Entry_dbt               service_outports;
    MEA_Policer_Entry_dbt                service_policer;
   	MEA_EHP_Info_dbt                     service_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt service_editing;
    MEA_Service_t                        service_id;

    for (P=0;P<MEA_Example3_num_of_user_ports;P++) {
        
        for (x=100;x<=120 ; x++) {
         MEA_OS_printf(".");
            usr_svid=x;
            
            /* 2.a Translate     flows with 
                   CVID=1-6      & SVID=X   , Port P 
                   CVID=100*P+X  & SVID=1001;  {X=1,2,�,32; P=1,2,�,8} */
            for (usr_cvid=0; usr_cvid<=7; usr_cvid++) {
            
                net_cvid=(100*(P+1))+x;
                net_svid=1001;

              
                /* Define the upstream service 
                   that will be use the learning action define above  */
                MEA_OS_memset(&service_key         ,0,sizeof(service_key));
                service_key.src_port = MEA_Example3_user_ports[P]  ;
                service_key.net_tag  = 0x1f000 | usr_svid;
                service_key.pri      = usr_cvid;
                MEA_OS_memset(&service_data        ,0,sizeof(service_data));
                service_data.DSE_forwarding_enable                 = MEA_FALSE;
                service_data.DSE_forwarding_key_type               = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
                service_data.DSE_learning_enable                   = MEA_TRUE;
                service_data.DSE_learning_key_type                 = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
                service_data.vpn                                   = 0;
                service_data.limiter_enable                        = MEA_TRUE;
                service_data.limiter_id                            = limiter_id;
                service_data.DSE_learning_actionId_valid           = MEA_FALSE;
                service_data.DSE_learning_actionId                 = 0;//action_id;
                service_data.DSE_learning_srcPort_force            = MEA_TRUE;
                service_data.DSE_learning_srcPort                  = P;
                service_data.DSE_learning_update_allow_enable      = MEA_FALSE;
                service_data.DSE_learning_update_allow_send_to_cpu = MEA_TRUE;
                service_data.tmId_disable                          = MEA_TRUE;
                
                service_data.pmId                                  =  21;
                
                MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
                MEA_SET_OUTPORT(&service_outports,24);
                MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
                service_policer.CIR = 1000000000;
                service_policer.CBS = 32000;
                service_policer.comp = 0;
                MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
                MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
                service_editing.num_of_entries = 1;
                service_editing.ehp_info       = &(service_editing_info[0]);
                service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000 | 24;
                service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
                service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
                service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
                service_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0;
                service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
                service_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
                service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#if 1                
                if(((MEA_Example3_user_ports[P] == 126) &&  (x == 18) && (usr_cvid ==6))  ||
                    ((MEA_Example3_user_ports[P] == 126) && (x == 22) && (usr_cvid ==4))  || 
                    ((MEA_Example3_user_ports[P] == 126) && (x == 22) && (usr_cvid ==5))  ||
                    ((MEA_Example3_user_ports[P] == 126) && (x == 22) && (usr_cvid ==6))  ||
                    ((MEA_Example3_user_ports[P] == 126) && (x == 30) && (usr_cvid ==4))  || 
                    ((MEA_Example3_user_ports[P] == 126) && (x == 30) && (usr_cvid ==5))  ||
                    ((MEA_Example3_user_ports[P] == 126) && (x == 30) && (usr_cvid ==6))  ){
                //continue;
                } else {
#endif
                if (MEA_API_Create_Service (MEA_UNIT_0,
                                            &service_key,
                                            &service_data,
                                            &service_outports,
                                            &service_policer,
                                            &service_editing,
                                            &service_id) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "\n%s - 1001 MEA_API_Create_Service failed (netTag=0x%08x) port %d pri %d\n",
                                      __FUNCTION__,
                                      service_key.net_tag,
                                      service_key.src_port,
                                      service_key.pri);
                    //return MEA_ERROR;
                }   
                }
            } /* for (usr_cvid=1; usr_cvid<=6; usr_cvid++)  */


        } /* for (x=1;x<=32 ; x++) */
    } /* for (P=0;P<MEA_Example3_num_of_user_ports;P++) */

#endif






return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_define_upstream_services>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_define_upstream_services()
{

    MEA_LxCp_t                           LxCP_id;
    MEA_LxCP_Protocol_key_dbt            LxCP_Protocol_key;
    MEA_LxCP_Protocol_data_dbt           LxCP_Protocol_data;
    MEA_Uint32                           P,x;
    MEA_Uint32                           usr_svid,usr_cvid;
    MEA_Uint32                           net_svid,net_cvid;
    MEA_Limiter_t                        limiter_id;
    MEA_Limiter_Entry_dbt                limiter_entry;
    MEA_Action_Entry_Data_dbt            action_data; 
    MEA_Policer_Entry_dbt                action_policer;
   	MEA_EHP_Info_dbt                     action_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt action_editing;
    MEA_Action_t                         action_id;
    MEA_Service_Entry_Key_dbt            service_key;
    MEA_Service_Entry_Data_dbt           service_data;
    MEA_OutPorts_Entry_dbt               service_outports;
    MEA_Policer_Entry_dbt                service_policer;
   	MEA_EHP_Info_dbt                     service_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt service_editing;
    MEA_Service_t                        service_id;
    MEA_LxCP_Entry_dbt                   lxcp_entry;


    /* Create LxCP profile to trap IGMP packets , and all other to discard */
    MEA_OS_memset(&lxcp_entry,0,sizeof(lxcp_entry));
    LxCP_id = MEA_PLAT_GENERATE_NEW_ID;
    if (MEA_API_Create_LxCP(MEA_UNIT_0,&lxcp_entry,&LxCP_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_LxCP failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    MEA_OS_memset(&LxCP_Protocol_key,0,sizeof(LxCP_Protocol_key));
    for (LxCP_Protocol_key.protocol=0;
         LxCP_Protocol_key.protocol<MEA_LXCP_PROTOCOL_LAST;
         LxCP_Protocol_key.protocol++) {
        MEA_OS_memset(&LxCP_Protocol_data,0,sizeof(LxCP_Protocol_data));
        LxCP_Protocol_data.action_type = MEA_LXCP_PROTOCOL_ACTION_DISCARD;
        if(!MEA_API_IsSupport_LxCP_Protocol(LxCP_Protocol_key.protocol)) {
             continue;
        }
        if (MEA_API_Set_LxCP_Protocol (MEA_UNIT_0,
                                       LxCP_id,
                                       &LxCP_Protocol_key,
                                       &LxCP_Protocol_data
                                      ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_LxCP_Protocol failed (protocol=%d,action=%d) \n",
                              __FUNCTION__,
                              LxCP_Protocol_key.protocol,
                              LxCP_Protocol_data.action_type);
            return MEA_ERROR;
        }
    }

    MEA_OS_memset(&LxCP_Protocol_key,0,sizeof(LxCP_Protocol_key));
    LxCP_Protocol_key.protocol = MEA_LXCP_PROTOCOL_IGMP;
    MEA_OS_memset(&LxCP_Protocol_data,0,sizeof(LxCP_Protocol_data));
    LxCP_Protocol_data.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
    if (MEA_API_Set_LxCP_Protocol (MEA_UNIT_0,
                                   LxCP_id,
                                   &LxCP_Protocol_key,
                                   &LxCP_Protocol_data
                                  ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_LxCP_Protocol failed (protocol=%d,action=%d) \n",
                          __FUNCTION__,
                          LxCP_Protocol_key.protocol,
                          LxCP_Protocol_data.action_type);
        return MEA_ERROR;
    }
                    /* Define learning limiterId */
    MEA_OS_memset(&limiter_entry,0,sizeof(limiter_entry));
    limiter_entry.limit = 1000;
    limiter_entry.action_type = MEA_LIMIT_ACTION_SEND_TO_CPU;
    limiter_id = MEA_PLAT_GENERATE_NEW_ID;
    if (MEA_API_Create_Limiter(MEA_UNIT_0,
                                           &limiter_entry,
                                           &limiter_id
                                           ) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - MEA_API_Create_Limiter failed \n",
                                      __FUNCTION__);
                    return MEA_ERROR;
    }

    for (P=0;P<MEA_Example3_num_of_user_ports;P++) {
#ifndef __KERNEL__
			 MEA_OS_fflush(stdout);
#endif            
        MEA_OS_printf("\n DBG MEA_Example3_user_ports[%ld]= %d \n",P,MEA_Example3_user_ports[P]);
        for (x=1;x<=8 ; x++) {
#if (defined (MEA_PLAT_EVALUATION_BOARD) )
#ifndef __KERNEL__
			 MEA_OS_fflush(stdout);
#endif            
            MEA_OS_printf(".");
#endif
            usr_svid=x;
            
            /* 2.a Translate     flows with 
                   CVID=1-6      & SVID=X   , Port P 
                   CVID=100*P+X  & SVID=1001;  {X=1,2,�,32; P=1,2,�,8} */
            for (usr_cvid=1; usr_cvid<=6; usr_cvid++) {
            
                net_cvid=(100*(P+1))+x;
                net_svid=1001;
                



                /* Define learning action that will be use in the downstream.
                   3.a. Translate flows with 
                         SVID=1001 & CVID=100*P+X 
                         SVID=X    & CVID=Learned (1-6);  {X=1,2,�,32; P=1,2,�,8}*/
                MEA_OS_memset(&action_data,0,sizeof(action_data));
                action_data.ed_id_valid        = MEA_TRUE;
                action_data.ed_id              = 0;
                action_data.tm_id_valid        = MEA_TRUE;
                action_data.tm_id              = (MEA_TmId_t)(x+2);
                action_data.pm_id_valid        = MEA_TRUE;
                 
               if(MEA_Example3_Cluster_mod == MEA_FALSE){ //strict mode
                //(MEA_PmId_t)
                action_data.pm_id              = (MEA_PmId_t) (30 + 64*(P) + 8*(usr_cvid-1) + (usr_svid%8) );
               } else {
                  action_data.pm_id = (MEA_PmId_t) (30+(8*P)+usr_cvid-1);
               }

                action_data.protocol_llc_force = MEA_TRUE;
                action_data.proto_llc_valid    = MEA_TRUE;
                action_data.Protocol           = 1;
                action_data.Llc                = 0;
                action_data.force_cos_valid    = MEA_ACTION_FORCE_COMMAND_VALUE;
                action_data.COS                = (usr_svid-1); /* Define the priority queue */
                action_data.tmId_disable =MEA_TRUE;

                MEA_OS_memset(&action_policer,0,sizeof(action_policer));
                action_policer.CIR = 4000000;
                action_policer.CBS = 32000;
                MEA_OS_memset(&action_editing,0,sizeof(action_editing));
                MEA_OS_memset(&action_editing_info,0,sizeof(action_editing_info));
                action_editing.num_of_entries = 1;
                action_editing.ehp_info       = &(action_editing_info[0]);
                action_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000 | usr_cvid;
                action_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
                action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
                action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
                action_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0x81000000 | usr_svid;
                action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
                action_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
                action_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;
#if 1                
                action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
                action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
                action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
                action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
                action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
                action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
                action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
                action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;

#endif
                action_id = MEA_PLAT_GENERATE_NEW_ID;
                if (MEA_API_Create_Action(MEA_UNIT_0,
                                          &action_data,
                                          NULL, /* outports */
                                          &action_policer, /* policer */
                                          &action_editing,
                                          &action_id
                                          ) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - MEA_API_Create_Action (1) failed \n",
                                      __FUNCTION__);
                    return MEA_ERROR;
                }
              
                /* Define the upstream service 
                   that will be use the learning action define above  */
                MEA_OS_memset(&service_key         ,0,sizeof(service_key));
                service_key.src_port = MEA_Example3_user_ports[P]  ;
                service_key.net_tag  = 0x1f000 | usr_svid;
                service_key.pri      = usr_cvid;
                MEA_OS_memset(&service_data        ,0,sizeof(service_data));
                service_data.DSE_forwarding_enable                 = MEA_FALSE;
                service_data.DSE_forwarding_key_type               = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
                service_data.DSE_learning_enable                   = MEA_TRUE;
                service_data.DSE_learning_key_type                 = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
                service_data.vpn                                   = 0;
                service_data.limiter_enable                        = MEA_FALSE;
                service_data.limiter_id                            = limiter_id;
                
               // service_data.CFM_OAM_ME_Level_Threshold_valid      = MEA_TRUE;
               // service_data.CFM_OAM_ME_Level_Threshold_value      = P + 10;
                
                service_data.DSE_learning_actionId_valid           = MEA_TRUE;
                service_data.DSE_learning_actionId                 = action_id;
                service_data.DSE_learning_srcPort_force            = MEA_TRUE;
                
                service_data.DSE_learning_srcPort                  = 30+(8*P)+usr_cvid-1;
                
                service_data.DSE_learning_update_allow_enable      = MEA_FALSE;
                service_data.DSE_learning_update_allow_send_to_cpu = MEA_TRUE;
                service_data.tmId_disable                          = MEA_TRUE;
                service_data.tmId = 2;
                   
               
                service_data.pmId = (MEA_PmId_t) (300 + (0 + 64*(P) +  (usr_svid%8)));
                
                MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
                MEA_SET_OUTPORT(&service_outports,MEA_Example3_network_portQueue[0]);
                MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
                service_policer.CIR = 1000000000;
                service_policer.CBS = 32000;
                service_policer.comp = 0;
                MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
                MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
                service_editing.num_of_entries = 1;
                service_editing.ehp_info       = &(service_editing_info[0]);
                service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000 | net_cvid;
                service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
                service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
                service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
                service_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0x81000000 | net_svid;
                service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
                service_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
                service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;
#if 1                
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
                service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif     
                    if (MEA_API_Create_Service (MEA_UNIT_0,
                                                &service_key,
                                                &service_data,
                                                &service_outports,
                                                &service_policer,
                                                &service_editing,
                                                &service_id) != MEA_OK) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "\n%s - 1001 MEA_API_Create_Service failed (netTag=0x%08x) port %d pri %d\n",
                                          __FUNCTION__,
                                          service_key.net_tag,
                                          service_key.src_port,
                                          service_key.pri);
                        //return MEA_ERROR;
                    }

            } /* for (usr_cvid=1; usr_cvid<=6; usr_cvid++)  */

            /* 2.b. Translate     flow  with 
                    CVID=7            & SVID=X   , Port P
                    CVID=100*P+X+1000 & SVID=2001; {X=1,2,�,32; P=1,2,�,8} */
            usr_cvid=7;
            net_cvid=(100*(P+1))+x+1000;
            net_svid=2001;
#if 0
            /* Define learning limiterId */
            MEA_OS_memset(&limiter_entry,0,sizeof(limiter_entry));
            limiter_entry.limit = 1000;
            limiter_entry.action_type = MEA_LIMIT_ACTION_SEND_TO_CPU;
            limiter_id = MEA_PLAT_GENERATE_NEW_ID;
            if (MEA_API_Create_Limiter(MEA_UNIT_0,
                                       &limiter_entry,
                                       &limiter_id
                                       ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "\n %s - MEA_API_Create_Limiter failed \n",
                                  __FUNCTION__);
                return MEA_ERROR;
            }
#endif
            /* Define learning action that will be use in the downstream.
               3.b. Translate flows with 
                    SVID=2001 & CVID= CVID=100*P+X+1000'
                    SVID=X    & CVID=7;              {X=1,2,�,32; P=1,2,�,8} */
            MEA_OS_memset(&action_data,0,sizeof(action_data));
            action_data.ed_id_valid        = MEA_TRUE;
            action_data.ed_id              = 0;
            action_data.tm_id_valid        = MEA_TRUE;
            action_data.tm_id              = (MEA_TmId_t)(33+x+2);
            action_data.pm_id_valid        = MEA_TRUE;
                
            if(MEA_Example3_Cluster_mod == MEA_FALSE){ //strict mode
                //(MEA_PmId_t)
                action_data.pm_id              = (MEA_PmId_t) (30 + 64*(P) + 8*(usr_cvid-1) + (usr_svid%8) );
               } else {
                  action_data.pm_id = (MEA_PmId_t) (30+(8*P)+usr_cvid-1);
            }
            
 
               
            action_data.protocol_llc_force = MEA_TRUE;
            action_data.proto_llc_valid    = MEA_TRUE;
            action_data.Protocol           = 1;
            action_data.Llc                = 0;
            action_data.force_cos_valid    = MEA_ACTION_FORCE_COMMAND_VALUE;
            action_data.COS                = (usr_svid-1); /* Define the priority queue */
            action_data.tmId_disable       = MEA_TRUE;
            MEA_OS_memset(&action_policer,0,sizeof(action_policer));
            action_policer.CIR = 2000000;
            action_policer.CBS = 32000;
            MEA_OS_memset(&action_editing,0,sizeof(action_editing));
            MEA_OS_memset(&action_editing_info,0,sizeof(action_editing_info));
            action_editing.num_of_entries = 1;
            action_editing.ehp_info       = &(action_editing_info[0]);
            action_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000 | usr_cvid;
            action_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
            action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
            action_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0x81000000 | usr_svid;
            action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            action_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
            action_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;
#if 1            
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif     
            action_id = MEA_PLAT_GENERATE_NEW_ID;
            if (MEA_API_Create_Action(MEA_UNIT_0,
                                      &action_data,
                                      NULL, /* outports */
                                      &action_policer, /* policer */
                                      &action_editing,
                                      &action_id
                                     ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Create_Action (2) failed \n",
                                  __FUNCTION__);
                return MEA_ERROR;
            }

            /* Define the upstream service 
               that will be use the learning action define above */
            MEA_OS_memset(&service_key         ,0,sizeof(service_key));
            service_key.src_port = MEA_Example3_user_ports[P] ;
            service_key.net_tag  = 0x1f000 | usr_svid;
            service_key.pri      = usr_cvid;
            MEA_OS_memset(&service_data        ,0,sizeof(service_data));
            service_data.DSE_forwarding_enable                 = MEA_FALSE;
            service_data.DSE_forwarding_key_type               = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            service_data.DSE_learning_enable                   = MEA_TRUE;
            service_data.DSE_learning_key_type                 = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
            service_data.vpn                                   = 0;
            service_data.limiter_enable                        = MEA_FALSE;
            service_data.limiter_id                            = limiter_id;
           // service_data.CFM_OAM_ME_Level_Threshold_valid      = MEA_TRUE;
           // service_data.CFM_OAM_ME_Level_Threshold_value      = P + 10;
            service_data.DSE_learning_actionId_valid           = MEA_TRUE;
            service_data.DSE_learning_actionId                 = action_id;
            service_data.DSE_learning_srcPort_force            = MEA_TRUE;
            
            service_data.DSE_learning_srcPort                  = 30+(8*P)+usr_cvid-1;
            
            service_data.DSE_learning_update_allow_enable      = MEA_FALSE;
            service_data.DSE_learning_update_allow_send_to_cpu = MEA_TRUE;
            service_data.tmId_disable                          = MEA_TRUE;
            service_data.tmId = 2;
             
            service_data.pmId = (MEA_PmId_t) (300 + (0 + 64*(P) +  (usr_svid%8)));
             
            
            
            MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
            MEA_SET_OUTPORT(&service_outports,MEA_Example3_network_portQueue[1]);
            MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
            service_policer.CIR = 1000000000;
            service_policer.CBS = 32000;
            service_policer.comp = 0;
            MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
            MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
            service_editing.num_of_entries = 1;
            service_editing.ehp_info       = &(service_editing_info[0]);
            service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000 | net_cvid;
            service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
            service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
            service_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0x81000000 | net_svid;
            service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            service_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
            service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;
#if 1           
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif            
                if (MEA_API_Create_Service (MEA_UNIT_0,
                                            &service_key,
                                            &service_data,
                                            &service_outports,
                                            &service_policer,
                                            &service_editing,
                                            &service_id) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "\n %s -2001 MEA_API_Create_Service failed (netTag=0x%08x) port %d pri %d\n",
                                      __FUNCTION__,service_key.net_tag,
                                      service_key.src_port,
                                      service_key.pri);
                    //return MEA_ERROR;
                }
            /* 2.c. Translate all flows with 
                  no CVID  & SVID=X   , any Port and ET=IPv4   
                  no CVID  & SVID=4001; {X=1,2,�,32}  */
            usr_cvid=0;
            net_cvid=0; /* not relevant */
            net_svid=4001;

#if 0
            /* Define learning limiterId */
            MEA_OS_memset(&limiter_entry,0,sizeof(limiter_entry));
            limiter_entry.limit = 1000;
            limiter_entry.action_type = MEA_LIMIT_ACTION_SEND_TO_CPU;
            limiter_id = MEA_PLAT_GENERATE_NEW_ID;
            if (MEA_API_Create_Limiter(MEA_UNIT_0,
                                       &limiter_entry,
                                       &lim5iter_id
                                       ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "\n %s - MEA_API_Create_Limiter failed \n",
                                  __FUNCTION__);
                return MEA_ERROR;
            }
#endif
 
            MEA_OS_memset(&action_data,0,sizeof(action_data));
            action_data.ed_id_valid        = MEA_TRUE;
            action_data.ed_id              = 0;
            action_data.tm_id_valid        = MEA_TRUE;
            action_data.tm_id              = (MEA_TmId_t)(400+x+2);
            action_data.pm_id_valid        = MEA_TRUE;
                
            if(MEA_Example3_Cluster_mod == MEA_FALSE){ //strict mode
                //(MEA_PmId_t)
                action_data.pm_id              = (MEA_PmId_t) (30 + 64*(P) + 8*(usr_cvid-1) + (usr_svid%8) );
               } else {
                  action_data.pm_id = (MEA_PmId_t) (30+(8*P)+usr_cvid-1);
            }
            
 
               
            action_data.protocol_llc_force = MEA_TRUE;
            action_data.proto_llc_valid    = MEA_TRUE;
            action_data.Protocol           = 1;
            action_data.Llc                = 0;
            action_data.force_cos_valid    = MEA_ACTION_FORCE_COMMAND_VALUE;
            action_data.COS                = (usr_svid-1); /* Define the priority queue */
            action_data.tmId_disable       = MEA_TRUE;
            MEA_OS_memset(&action_policer,0,sizeof(action_policer));
            action_policer.CIR = 2000000;
            action_policer.CBS = 32000;
            MEA_OS_memset(&action_editing,0,sizeof(action_editing));
            MEA_OS_memset(&action_editing_info,0,sizeof(action_editing_info));
            action_editing.num_of_entries = 1;
            action_editing.ehp_info       = &(action_editing_info[0]);
            action_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000 | usr_svid;
            action_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
            action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
            //action_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0x81000000 | usr_svid;
            //action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            //action_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
            //action_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;
#if 1            
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif     
            action_id = MEA_PLAT_GENERATE_NEW_ID;
            if (MEA_API_Create_Action(MEA_UNIT_0,
                                      &action_data,
                                      NULL, /* outports */
                                      &action_policer, /* policer */
                                      &action_editing,
                                      &action_id
                                     ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Create_Action (2) failed \n",
                                  __FUNCTION__);
                return MEA_ERROR;
            }

            /* Define the upstream service 
               that will be use the learning action define above */
            MEA_OS_memset(&service_key         ,0,sizeof(service_key));
            service_key.src_port = MEA_Example3_user_ports[P] ;
            service_key.net_tag  = 0x1f000 | usr_svid;
            service_key.pri      = usr_cvid;
            MEA_OS_memset(&service_data        ,0,sizeof(service_data));
            service_data.DSE_forwarding_enable                 = MEA_FALSE;
            service_data.DSE_forwarding_key_type               = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            service_data.DSE_learning_enable                   = MEA_TRUE;
            service_data.DSE_learning_key_type                 = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
            service_data.vpn                                   = 0;
            service_data.limiter_enable                        = MEA_FALSE;
            service_data.limiter_id                            = limiter_id;
           // service_data.CFM_OAM_ME_Level_Threshold_valid      = MEA_TRUE;
           // service_data.CFM_OAM_ME_Level_Threshold_value      = P + 10;
            service_data.DSE_learning_actionId_valid           = MEA_TRUE;
            service_data.DSE_learning_actionId                 = action_id;
            service_data.DSE_learning_srcPort_force            = MEA_TRUE;
            
            service_data.DSE_learning_srcPort                  = 37+(8*P);
            
            service_data.DSE_learning_update_allow_enable      = MEA_FALSE;
            service_data.DSE_learning_update_allow_send_to_cpu = MEA_TRUE;
            service_data.tmId_disable                          = MEA_TRUE;
            service_data.tmId = 2;
             
            service_data.pmId = (MEA_PmId_t) (300 + (0 + 64*(P) +  (usr_svid%8)));
             
            
            
            MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
            MEA_SET_OUTPORT(&service_outports,MEA_Example3_network_portQueue[1]);
            MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
            service_policer.CIR = 1000000000;
            service_policer.CBS = 32000;
            service_policer.comp = 0;
            MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
            MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
            service_editing.num_of_entries = 1;
            service_editing.ehp_info       = &(service_editing_info[0]);
            service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000 | net_svid;
            service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
            service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
                
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;        
            
            if (MEA_API_Create_Service (MEA_UNIT_0,
                                            &service_key,
                                            &service_data,
                                            &service_outports,
                                            &service_policer,
                                            &service_editing,
                                            &service_id) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "\n %s -4001 MEA_API_Create_Service failed (netTag=0x%08x) port %d pri %d\n",
                                      __FUNCTION__,service_key.net_tag,
                                      service_key.src_port,
                                      service_key.pri);
                    //return MEA_ERROR;
                } 

            

//-------------------------------

#if 0
            MEA_OS_memset(&service_key         ,0,sizeof(service_key));
            service_key.src_port = MEA_Example3_user_ports[P]  ;
            service_key.net_tag  = 0x1f000 | usr_svid;
            service_key.pri      = usr_cvid;
            MEA_OS_memset(&service_data        ,0,sizeof(service_data));
            service_data.DSE_learning_enable                   = MEA_TRUE;
            service_data.DSE_learning_key_type                 = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;

            //service_data.CFM_OAM_ME_Level_Threshold_valid      = MEA_TRUE;
           // service_data.CFM_OAM_ME_Level_Threshold_value      = P + 10;

            service_data.tmId_disable                          = MEA_TRUE;
            service_data.tmId                                  = 2;
            service_data.DSE_learning_srcPort_force            = MEA_TRUE;
                
            
                service_data.DSE_learning_srcPort                  = 30+(8*P)+usr_cvid+7;
             
              
            //service_data.pmId                                  =  20;
                
          
            service_data.pmId = (MEA_PmId_t) (300 + (0 + 64*(P)  + (usr_svid%8)));
          
            service_data.LxCp_enable = MEA_TRUE;
            service_data.LxCp_Id = LxCP_id;
            MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
            MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
            service_policer.CIR = 1000000000;
            service_policer.CBS = 32000;
            service_policer.comp = 0;
            MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
            MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
            service_editing.num_of_entries = 1;
            service_editing.ehp_info       = &(service_editing_info[0]);
            service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000 | 4001;/*net_cvid*/;
            service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
            service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1           
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
            service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif     
            MEA_SET_OUTPORT(&service_outports,MEA_Example3_network_portQueue[1]);
            if (MEA_API_Create_Service (MEA_UNIT_0,
                                        &service_key,
                                        &service_data,
                                        &service_outports,
                                        &service_policer,
                                        &service_editing,
                                        &service_id) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - 4001 MEA_API_Create_Service failed (netTag=0x%08x) port %d\n",
                                  __FUNCTION__,service_key.net_tag,service_key.src_port);
                //return MEA_ERROR;
            }  
#endif


        } /* for (x=1;x<=32 ; x++) */
    } /* for (P=0;P<MEA_Example3_num_of_user_ports;P++) */


    /* Return to caller */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_define_downstream_services>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_define_downstream_services()
{

    MEA_Service_Entry_Key_dbt            service_key;
    MEA_Service_Entry_Data_dbt           service_data;
    MEA_OutPorts_Entry_dbt               service_outports;
    MEA_Policer_Entry_dbt                service_policer;
   	MEA_EHP_Info_dbt                     service_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt service_editing;
    MEA_Service_t                        service_id;
    MEA_LxCp_t                           LxCP_id;
    MEA_LxCP_Protocol_key_dbt            LxCP_Protocol_key;
    MEA_LxCP_Protocol_data_dbt           LxCP_Protocol_data;
    MEA_LxCP_Entry_dbt                   lxcp_entry;

    /* Create downstream for S-VID 1001 */
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = MEA_Example3_network_port;
    service_key.net_tag  = 0x1f000 | 1001;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                     = 0;
    service_data.pmId                    = 11;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 4000000;
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;

#endif
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }


    /* Create downstream for S-VID 2001 */
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = MEA_Example3_network_port;
    service_key.net_tag  = 0x1f000 | 2001;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                     = 0;
    service_data.pmId                    = 12;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 2000000;
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1    
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }


    /* Create LxCP profile to trap IGMP packets */
    MEA_OS_memset(&lxcp_entry,0,sizeof(lxcp_entry));
    lxcp_entry.direction = MEA_DIRECTION_DOWNSTREAM;
    LxCP_id = MEA_PLAT_GENERATE_NEW_ID;
    if (MEA_API_Create_LxCP(MEA_UNIT_0,&lxcp_entry,&LxCP_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_LxCP failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    MEA_OS_memset(&LxCP_Protocol_key,0,sizeof(LxCP_Protocol_key));
    LxCP_Protocol_key.protocol = MEA_LXCP_PROTOCOL_IGMP;
    MEA_OS_memset(&LxCP_Protocol_data,0,sizeof(LxCP_Protocol_data));
    LxCP_Protocol_data.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
    if (MEA_API_Set_LxCP_Protocol (MEA_UNIT_0,
                                   LxCP_id,
                                   &LxCP_Protocol_key,
                                   &LxCP_Protocol_data
                                  ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_LxCP_Protocol failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Create downstream for S-VID 4001 */
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = MEA_Example3_network_port;
    service_key.net_tag  = 0x1f000 | 4001;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN; //
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                     = 0;
    service_data.pmId                    = 3;
    service_data.LxCp_enable             = MEA_FALSE;
    service_data.LxCp_Id                 = LxCP_id;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 30000000;
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }
    
/**  Foward IP  3001 **/
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = MEA_Example3_network_port;
    service_key.net_tag  = 0x1f000 | 3001;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                     = 0;
    service_data.pmId                    = 3;
    service_data.LxCp_enable             = MEA_TRUE;
    service_data.LxCp_Id                 = LxCP_id;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 300000000;
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }




    /* Return to caller */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_define_multicast_mac_entries>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_define_multicast_mac_entries()
{

    MEA_SE_Entry_dbt                     se_entry;
    MEA_Action_Entry_Data_dbt            action_data; 
   	MEA_EHP_Info_dbt                     action_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt action_editing;
    MEA_Action_t                         action_id;


    /* create action to make swap s-vid to 33 0x21 */
    MEA_OS_memset(&action_data,0,sizeof(action_data));
    action_data.ed_id = 0;
    action_data.ed_id_valid = MEA_TRUE;
    action_data.protocol_llc_force = MEA_TRUE;
    action_data.proto_llc_valid    = MEA_TRUE;
    action_data.Protocol           = 1;
    action_data.Llc                = 0;
    MEA_OS_memset(&action_editing,0,sizeof(action_editing));
    MEA_OS_memset(&action_editing_info,0,sizeof(action_editing_info));
    action_editing.num_of_entries = 1;
    action_editing.ehp_info       = &(action_editing_info[0]);
    action_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000021;
    action_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
    action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif
    action_id = MEA_PLAT_GENERATE_NEW_ID;
    if (MEA_API_Create_Action(MEA_UNIT_0,
                              &action_data,
                              NULL, /* outports */
                              NULL, /* policer */
                              &action_editing,
                              &action_id
                              ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Action (1) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
                              


    /* Multicast entry for 239.1.1.1 (01:00:5e:01:01:01) 
       send to port 1 and 2 
       and swap s-vid to 33 */


    MEA_OS_memset(&se_entry,0,sizeof(se_entry));
    se_entry.key.mac_plus_vpn.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    se_entry.key.mac_plus_vpn.VPN = 0;
    se_entry.key.mac_plus_vpn.MAC.b[0] = 0x01;
    se_entry.key.mac_plus_vpn.MAC.b[1] = 0x00;
    se_entry.key.mac_plus_vpn.MAC.b[2] = 0x5e;
    se_entry.key.mac_plus_vpn.MAC.b[3] = 0x01;
    se_entry.key.mac_plus_vpn.MAC.b[4] = 0x01;
    se_entry.key.mac_plus_vpn.MAC.b[5] = 0x01;
    se_entry.data.aging = 3;
    se_entry.data.static_flag = 1;
    se_entry.data.sa_discard = 0;
    se_entry.data.limiterId_valid = MEA_FALSE;
    se_entry.data.valid = MEA_TRUE;
    se_entry.data.actionId_valid = MEA_TRUE;
    se_entry.data.actionId       = action_id;
    MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(0*8)+7));
    if(MEA_Example3_num_of_user_ports > 1){
        MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(1*8)+7));
    }
    if (MEA_API_Create_SE_Entry(MEA_UNIT_0,&se_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_SE_Entry failed "
                          "for 239.1.1.1 (01:00:5e:01:01:01)\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Multicast entry for 239.1.1.2 (01:00:5e:01:01:02) 
       send to port 2 and 3 
       and swap s-vid to 33 */
    MEA_OS_memset(&se_entry,0,sizeof(se_entry));
    se_entry.key.mac_plus_vpn.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    se_entry.key.mac_plus_vpn.VPN = 0;
    se_entry.key.mac_plus_vpn.MAC.b[0] = 0x01;
    se_entry.key.mac_plus_vpn.MAC.b[1] = 0x00;
    se_entry.key.mac_plus_vpn.MAC.b[2] = 0x5e;
    se_entry.key.mac_plus_vpn.MAC.b[3] = 0x01;
    se_entry.key.mac_plus_vpn.MAC.b[4] = 0x01;
    se_entry.key.mac_plus_vpn.MAC.b[5] = 0x02;
    se_entry.data.aging = 3;
    se_entry.data.static_flag = 1;
    se_entry.data.sa_discard = 0;
    se_entry.data.limiterId_valid = MEA_FALSE;
    se_entry.data.valid = MEA_TRUE;
    se_entry.data.actionId_valid = MEA_TRUE;
    se_entry.data.actionId       = action_id;
    if(MEA_Example3_num_of_user_ports > 1){
        MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(1*8)+7));
        MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(2*8)+7));
    } else {
        MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(0*8)+7));
    }
    if (MEA_API_Create_SE_Entry(MEA_UNIT_0,&se_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_SE_Entry failed "
                          "for 239.1.1.2 (01:00:5e:01:01:02)\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Return to caller */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <MEA_Example3_define_multicast_IP_entries>             */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_define_multicast_IP_entries()
{

    MEA_SE_Entry_dbt                     se_entry;
    MEA_Action_Entry_Data_dbt            action_data; 
   	MEA_EHP_Info_dbt                     action_editing_info[1];

    //MEA_EHP_Info_dbt                    action_editing_infoM[2];
    MEA_OutPorts_Entry_dbt               action_outPorts;

    MEA_EgressHeaderProc_Array_Entry_dbt action_editing;
    MEA_Action_t                         action_id[8];
    MEA_Uint32                           i;
    MEA_Uint32               srcIp;
    MEA_Uint32               dstIp;
    char                     srcIp_str[20];
    char                     dstIp_str[20];
    //MEA_Uint32               P;
   

            
/*******************************************/
    for(i=0;i<8;i++){
    MEA_OS_memset(&action_outPorts,0,sizeof(action_outPorts));
    MEA_OS_memset(&action_data,0,sizeof(action_data));
    action_data.ed_id = 0;
    action_data.ed_id_valid = MEA_TRUE;
    action_data.protocol_llc_force = MEA_TRUE;
    action_data.proto_llc_valid    = MEA_TRUE;
    action_data.Protocol           = 1;
    action_data.Llc                = 0;
    action_data.output_ports_valid = 1;
    MEA_OS_memset(&action_editing,0,sizeof(action_editing));
    MEA_OS_memset(&action_editing_info,0,sizeof(action_editing_info));
    /*********************************/
     action_editing.num_of_entries = 1;
    /*********************************/
    MEA_OS_memset(&action_editing_info,0,sizeof(action_editing_info));
    
    action_editing.ehp_info       = &(action_editing_info[0]);
    MEA_OS_memset(&action_editing.ehp_info[0].output_info,0,sizeof(action_editing.ehp_info[0].output_info));

    if(i== 0) {
        action_editing.ehp_info[0].ehp_data.eth_info.val.all   = 0x81000000 | 123; // 1:3 48 72 126
        action_editing.ehp_info[0].ehp_data.eth_info.command   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(0*8)+7));
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(1*8)+7));
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(2*8)+7));
    }
    if(i== 1) {
        action_editing.ehp_info[0].ehp_data.eth_info.val.all   = 0x81000000 | 111; // 1:1 126
        action_editing.ehp_info[0].ehp_data.eth_info.command   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(0*8)+7));
        
    }
    if(i== 2) {
        action_editing.ehp_info[0].ehp_data.eth_info.val.all   = 0x81000000 | 222; // 1:1 72
        action_editing.ehp_info[0].ehp_data.eth_info.command   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(1*8)+7));
       }
    if(i== 3) {
        action_editing.ehp_info[0].ehp_data.eth_info.val.all   = 0x81000000 | 333; // 1:1 126
        action_editing.ehp_info[0].ehp_data.eth_info.command   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(2*8)+7));
    }
    if(i== 4) {
        action_editing.ehp_info[0].ehp_data.eth_info.val.all   = 0x81000000 | 321; // 1:3 48 72 126
        action_editing.ehp_info[0].ehp_data.eth_info.command   = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(0*8)+7));
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(1*8)+7));
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(2*8)+7));
    }
    if(i== 5){
        action_editing.ehp_info[0].ehp_data.eth_info.val.all   = 0x81000000 | 1122;
        action_editing.ehp_info[0].ehp_data.eth_info.command   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(0*8)+7));
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(1*8)+7));
    }
    if(i== 6) {
        action_editing.ehp_info[0].ehp_data.eth_info.val.all   = 0x81000000 | 1133;
        action_editing.ehp_info[0].ehp_data.eth_info.command   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(0*8)+7));
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(2*8)+7));
    } 
    if(i== 7) {
        action_editing.ehp_info[0].ehp_data.eth_info.val.all   = 0x81000000 | 2233;
        action_editing.ehp_info[0].ehp_data.eth_info.command   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(1*8)+7));
        MEA_SET_OUTPORT((& action_editing.ehp_info[0].output_info),(30+(2*8)+7));
    }
    
    action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif    
    action_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0x0 ;
    action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    action_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
    action_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;

    action_id[i] = MEA_PLAT_GENERATE_NEW_ID;
     if (MEA_API_Create_Action(MEA_UNIT_0,
                              &action_data,
                              &action_outPorts, /* outports */
                              NULL, /* policer */
                              &action_editing,
                              &action_id[i]
                              ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Action (1) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    }// for




                              

    MEA_OS_memset(&se_entry,0,sizeof(se_entry));

    /* Multicast entry for 239.1.1.2  
       send to port 2 and 3 
       and swap s-vid to 33 */
    MEA_OS_memset(&se_entry,0,sizeof(se_entry));
    se_entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
    
    se_entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4 = MEA_OS_inet_addr("192.100.10.1");
    se_entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 = MEA_OS_inet_addr("224.0.0.0");
    
    srcIp = se_entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4;
    dstIp = se_entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4;

    MEA_OS_inet_ntoa(MEA_OS_htonl(dstIp), dstIp_str, sizeof(dstIp_str));
    MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), srcIp_str, sizeof(srcIp_str));
    
    se_entry.data.aging = 3;
    se_entry.data.static_flag = 1;
    se_entry.data.sa_discard = 0;
    se_entry.data.limiterId_valid = MEA_FALSE;
    se_entry.data.valid = MEA_TRUE;
    se_entry.data.actionId_valid = MEA_TRUE;
    



    for (i=0;i< 8;i++){
        
        
        
        
        MEA_OS_memset(&se_entry.data.OutPorts,0,sizeof(se_entry.data.OutPorts));
        switch (i) {
            case 0:
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(0*8)+7));
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(1*8)+7));
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(2*8)+7));
            break;
            case 1:
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(0*8)+7));
                
            break;
            case 2:
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(1*8)+7));
                
            break;
            case 3:
                
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(2*8)+7));
                
            break;
            case 4:
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(0*8)+7));
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(1*8)+7));
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(2*8)+7));
            break;
            case 5:
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(0*8)+7));
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(1*8)+7));
                
            break;
            case 6:
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(0*8)+7));
                
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(2*8)+7));
            break;
            case 7:
                
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(1*8)+7));
                MEA_SET_OUTPORT((&se_entry.data.OutPorts),(30+(2*8)+7));
            break;


        }


        se_entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4++;
        
        se_entry.data.actionId_valid = MEA_TRUE;
        se_entry.data.actionId       = action_id[i];
        
        if (MEA_API_Create_SE_Entry(MEA_UNIT_0,&se_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "Example3- MEA_API_Create_SE_Entry failed "
                              "for  DestIp %s  sourceIp %s   i=%d\n"
                              ,dstIp_str,srcIp_str,i);
            return MEA_ERROR;
        }
        
    }
    /* Return to caller */
    return MEA_OK;

}







#if 0
static MEA_Status MEA_Example3_define_CFM_Create()
{

    MEA_Service_Entry_Key_dbt            service_key;
    MEA_Service_Entry_Data_dbt           service_data;
    MEA_OutPorts_Entry_dbt               service_outports;
    MEA_Policer_Entry_dbt                service_policer;
   	MEA_EHP_Info_dbt                     service_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt service_editing;
    MEA_Service_t                        service_id;

   MEA_Action_Entry_Data_dbt            action_data; 
   MEA_Policer_Entry_dbt                action_policer;
   	MEA_EHP_Info_dbt                     action_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt action_editing;
    MEA_Action_t                         action_id;

    /* Create downstream for 127 PacketGenrator to out port 48 */
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = 127;
    service_key.net_tag  = 0x1f000 | 50;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_FALSE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                     = 0;
    service_data.pmId                    = 0;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 100000000; //100M
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 0
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;

#endif
     MEA_SET_OUTPORT(&service_outports,37);  // to port 48
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }


/* Create downstream for 125 PacketGenrator to out port 48 */
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = 126;
    service_key.net_tag  = 0x1f000 | 50;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    
    service_data.CFM_OAM_ME_Level_Threshold_valid = MEA_TRUE;
    service_data.CFM_OAM_ME_Level_Threshold_value = 5;
    
    service_data.vpn                     = 1;
    service_data.pmId                    = 11;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 100000000; //100M
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x99990000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;

#endif

    MEA_SET_OUTPORT(&service_outports,127);  // to port 127
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }



            MEA_OS_memset(&action_data,0,sizeof(action_data));
            action_data.ed_id_valid        = MEA_TRUE;
            action_data.ed_id              = 0;
            action_data.tm_id_valid        = MEA_TRUE;
            action_data.tm_id              = 0;
            action_data.pm_id_valid        = MEA_TRUE;
                
            //(MEA_PmId_t)
            action_data.pm_id              = 0;
                
               
            action_data.protocol_llc_force = MEA_TRUE;
            action_data.proto_llc_valid    = MEA_TRUE;
            action_data.Protocol           = 1;
            action_data.Llc                = 0;
            action_data.force_cos_valid    = MEA_ACTION_FORCE_COMMAND_VALUE;
            action_data.COS                = 0; /* Define the priority queue */
            MEA_OS_memset(&action_policer,0,sizeof(action_policer));
            action_policer.CIR = 100000000; // 100M
            action_policer.CBS = 32000;
            MEA_OS_memset(&action_editing,0,sizeof(action_editing));
            MEA_OS_memset(&action_editing_info,0,sizeof(action_editing_info));
            action_editing.num_of_entries = 1;
            action_editing.ehp_info       = &(action_editing_info[0]);
            action_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x99990000 | 1;
            action_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
            action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
            action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
            action_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0;
            action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
            action_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
            action_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;
#if 1            
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
            action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif     
            action_id = MEA_PLAT_GENERATE_NEW_ID;
            if (MEA_API_Create_Action(MEA_UNIT_0,
                                      &action_data,
                                      NULL, /* outports */
                                      &action_policer, /* policer */
                                      &action_editing,
                                      &action_id
                                     ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Create_Action (2) failed \n",
                                  __FUNCTION__);
                return MEA_ERROR;
            }










return MEA_OK;

}


#endif
#include "cli_eng.h"
#include "mea_cli.h"

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_main>                                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_main() {

    MEA_Uint32 port_num;

    CLI_print("\n Admin OFF:\n");
    MEA_Example3_configIngressAdminOFF();
    MEA_Example3_configEgressAdminOFF();
#if 0
    /* print version */
    CLI_print("\nVersion information:\n");
    if (MEA_CLI_Debug_ver(0, NULL) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_CLI_Debug_ver failed \n",
                         __FUNCTION__);
       return MEA_ERROR; 
    } 
#endif
    /* init globals */
    CLI_print("\nInit globals\n");
    if (MEA_Example3_init_globals() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_init_globals failed \n",
                         __FUNCTION__);
       return MEA_ERROR; 
    } 

    /* Delete all entities */
    CLI_print("\nDelete all entities\n");
    if (MEA_Example3_delete_all_entities() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_delete_all_entities failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* MEA debug cpu show 1 */
    CLI_print("\nEnable print packets receive to CPU \n");
    if (MEA_API_Set_CpuPort_FramesShowFlag(MEA_UNIT_0,1) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_CpuPort_FramesShowFlag failed \n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Create queues */
    CLI_print("\nCreate queues \n");
    if (MEA_Example3_create_queues() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_create_queues failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Configure ports */
    CLI_print("\nConfigure user ports \n");
    for (port_num=0;port_num<MEA_Example3_num_of_user_ports;port_num++) {
        if (MEA_Example3_config_port(MEA_Example3_user_ports[port_num],
                                     MEA_INGRESS_PORT_PROTO_QTAG_OUTER
                                    ) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_Example3_config_port failed (port=%d)\n",
                             __FUNCTION__,
                             MEA_Example3_user_ports[port_num]);
            return MEA_ERROR; 
        }
    }
    CLI_print("\nConfigure network port \n");
    if (MEA_Example3_config_port(MEA_Example3_network_port,
                                 MEA_INGRESS_PORT_PROTO_QTAG_OUTER
                                ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_config_port failed (port=%d)\n",
                          __FUNCTION__,
                          MEA_Example3_network_port);
        return MEA_ERROR; 
    }

    CLI_print("\nDefine upstream services \n");
    if (MEA_Example3_define_upstream_services() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_define_upstream_services failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    CLI_print("\nDefine downstream services \n");
    if (MEA_Example3_define_downstream_services() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_define_downstream_services failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }
#if 1
    CLI_print("\nDefine multicast mac entries \n");
    if (MEA_Example3_define_multicast_mac_entries() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_define_multicast_mac_entries failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    CLI_print("\nDefine IP entries \n");
    if(MEA_Example3_define_multicast_IP_entries() != MEA_OK){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_define_multicast_IP_entries failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }
#endif


#if 0
     CLI_print("\n LBM LBR seting  \n");
      if(MEA_Example3_define_CFM_Create() != MEA_OK){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_define_multicast_IP_entries failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }
#endif


   CLI_print("\n Admin ON:\n");
    if (MEA_Example3_configIngressAdminON()!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_configIngressAdminON failed\n",
                          __FUNCTION__);
    }
    
    if(MEA_Example3_configEgressAdminON()!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_configEgressAdminON failed\n",
                          __FUNCTION__);
    }
    
    /* Return to caller */
    CLI_print("\n");
    return MEA_OK;

}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_CLI_func>                                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_CLI_func(int argc, char *argv[]) {

    MEA_PmId_t numOfpm; 

    if ((argc != 2) && (argc != 3) && (argc!= 4)) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        return MEA_ERROR;
    }
    
    numOfpm = ENET_BM_TBL_TYP_CNT_PM_XXX_LENGTH(MEA_UNIT_0,0 );
    if(numOfpm < 512){
        CLI_print("Error: ,MEA_Example3_ can't work of pm is %d < 512 \n",numOfpm);
        return MEA_OK;
    }

    if (argc >= 3) {
        
        MEA_Example3_Cluster_mod =  MEA_OS_atoi(argv[2]);
        if(MEA_OS_atoi(argv[2])!=0 && MEA_OS_atoi(argv[2]) !=1){
            CLI_print("Error: ,MEA_Example3_Cluster_mod %d \n",MEA_OS_atoi(argv[2]));
            return MEA_ERROR;
        }
    }
    if(argc >= 4 ){
        MEA_Example3_PRIQueue_mod = MEA_OS_atoi(argv[3]);
        if(MEA_OS_atoi(argv[3]) !=0 && MEA_OS_atoi(argv[3]) !=1){
            CLI_print("Error: ,MEA_Example3_PRIQueue_mod %d \n",MEA_OS_atoi(argv[3]));
            return MEA_ERROR;
        }

    }


    if (MEA_Example3_main() != MEA_OK) {
        CLI_print("Error: ,MEA_Example3_main_failed \n");
    } else {
        CLI_print("Done.\n");
    }
   
 MEA_Example3_Cluster_mod  = MEA_FALSE;
 MEA_Example3_PRIQueue_mod = MEA_FALSE;
   return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_CLI_Init>                                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example3_CLI_Init() {


   CLI_defineCmd("MEA example case3",
                 (CmdFunc)MEA_Example3_CLI_func,
                 "Example case3 configuration \n",
                 "Usage: case3 all [cluster mode]  0-strict 1-wfq \n"
                 "                 [priQueue mode] 0 -strict 1 wfq\n"
                 "----------------------------------\n"
                 "       all - create all entities  \n"
                 "----------------------------------\n"
                 "Examples : - case3 all            \n"
                 "           - case3 all 1  0       \n");


    return MEA_OK;
}
#endif
