/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*************************************************************************************/
/* 	Module Name:		  Init.c													 */
/*																					 */ 
/*  Module Description:	  System Initialization										 */
/*																					 */
/*  Date of Creation:	  20/01/04													 */
/*																					 */
/*  Author Name:			 Alex Weis.												 */
/*************************************************************************************/ 



/*-------------------------------- Includes ------------------------------------------*/
#ifdef MEA_OS_LINUX
#ifndef __KERNEL__
#include <pthread.h>
#endif
#endif

#include "mea_api.h"
#include "ENET_platform.h"
#include "mea_drv_common.h"
#include "MEA_platform_os_memory.h"
#include "mea_swe_ip_drv.h"
#include "enet_json_db.h"




#ifndef __KERNEL__
/*#include <sys/types.h>*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#ifdef ENET_OS_LINUX
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <sys/mman.h>
#include <errno.h>  
#include <fcntl.h>
#define CFG_IMMR 0xF0000000
#define OR_MSK	0x00018fff	/* Address to Chip Select Setup mask	*/
#define OR_VAL	0x0000090e	/* Address to Chip Select Setup mask	*/
#define OR6_OFFSET  0x134
#define OR7_OFFSET  0x13C
#define IMRR_SIZE 0x4000
#endif


#endif /* __KERNEL__ */


#include "cli_eng.h"
#include "mea_cli.h"


#ifndef __KERNEL__
#include "mea_cpu_port_drv.h"
#endif


#ifdef WITH_EAPPL_SUPPORT
#include "eappl_main_pub.h"
#endif

#if defined(MEA_ETHERNITY) && defined(MEA_OS_LINUX) &&  defined(MEA_OS_EPC_ZYNQ7045)
#include "phy_api.h"
#endif



#if defined(MEA_ETHERNITY) && (defined(MEA_ENV_S1_K7) || defined(MEA_PLAT_S1_VDSL_K7))
#include "k7_sfp_interface.h"
#endif

//#define  MEA_ETHERNITY_MAIN 1


#ifdef __cplusplus
extern "C" {
 #endif 
#ifdef WITH_CUSTOMER_SUPPORT  // IPC

/* Add here the include that specific to the customer */
//extern int my_ipclock_start (int argc, char **argv);


extern int ipclock_start (void);

#endif


/************************************************************************/
/*                                                                      */
/************************************************************************/
int Enet_sock = 0;

//extern int ethernity_main1 (int argc, char **argv); 

#ifdef __cplusplus
 }
#endif 







#ifdef WITH_MEA_EXAMPLE_CLI
MEA_Status MEA_Example_CLI_Init();
#endif /* WITH_MEA_EXAMPLE_CLI */
#if 0
#ifdef WITH_MEA_EXAMPLE2_CLI
MEA_Status MEA_Example2_CLI_Init();
#endif /* WITH_MEA_EXAMPLE2_CLI */

#ifdef WITH_MEA_EXAMPLE3_CLI
MEA_Status MEA_Example3_CLI_Init();
#endif /* WITH_MEA_EXAMPLE3_CLI */

#ifdef WITH_MEA_EXAMPLE4_CLI
MEA_Status MEA_Example4_CLI_Init();
#endif /* WITH_MEA_EXAMPLE4_CLI */


#endif

#ifdef WITH_MEA_EXAMPLE5_CLI
MEA_Status MEA_Example5_CLI_Init();
#endif /* WITH_MEA_EXAMPLE5_CLI */

#ifdef WITH_MEA_EXAMPLE_TMUX_CLI
MEA_Status MEA_Example_tmux_CLI_Init();
#endif /* WITH_MEA_EXAMPLE_TMUX_CLI */


/*-------------------------------- Definitions ---------------------------------------*/

#define NO_ENET_OS_PERIODIC_TASK_STR "-p"
#define DBG_KERNEL_DRV		 "-d"
#define DBG_REGS_DRV		 "-r"

#define ENET_DBG_BIST_TEST    "-b"
#define ENET_DIR_UP_DRV       "-c1"
#define ENET_DIR_DOWN_DRV     "-c2"

#define ENET_DBG_BIST_TDM     "-tdm"  //only TDM loop


#define HELP_PLEASE			 "--help"


typedef enum{
	e_no_periodic,
	e_dbg_kernel_drv,
	e_dbg_regs_drv,
	e_help_me,
    e_bist_test,
	e_dir_up,
	e_dir_down,
    e_bist_tdm_test,
	e_invalid_param
}startup_params_t;


/*-------------------------------- External Functions --------------------------------*/

#if defined(ENET_OS_Z1) && defined(ENET_OS_DEVICE_MEMORY_SIMULATION)
#if 0
#include "ethernity_adaptor.h"
#endif 
#endif

	
static int g_rpc_ns = 0;
/*-------------------------------- External Variables --------------------------------*/
#if defined(MEA_ETHERNITY) && defined(MEA_OS_LINUX) &&  defined(MEA_OS_EPC_ZYNQ7045)

#define NO_TEST 0
#define RO_TEST 0

#define MEA_TEST_MEASURE_TIME_PERIODIC  0

static phy_init_t mea_epc7015_Port_PhyInit[] =
{
//    page  reg  value   mask    test         Notes
    { 18,   20,  0x0001, 0xFFFF, RO_TEST }, // Set SGMII to Copper mode
    { 18,   20,  0x8001, 0x0000, NO_TEST }, // Reset PHY
    { 3,    16,  0x1840, 0xFFFF, RO_TEST }, // LED[0] link, LED[2] - activity
    { 3,    17,  0x4405, 0xFFFF, RO_TEST }, // ON - driver HIGH, OFF - driver LOW
    { 0,    0,   0x8000, 0x8000, NO_TEST }, // Init soft reset

};

unsigned short mea_epc7015_Port_PhyInit_PhyInitSize = sizeof(mea_epc7015_Port_PhyInit) / sizeof(phy_init_t);


static phy_init_t mea_Internal_Xilinx_PhyInit_epc7015[] =
{
    //    page  reg  value   mask    test         Notes
    //    ----  ---  ------  ------  -------      -------------------------------------
    { 0,  4,  0xd801, 0x0000, RO_TEST }, // Link UP, Full Duplex, 1G
    { 0,  0,  0x1340, 0x0000, NO_TEST }, // Isolate Off, AN on, AN restart, 
};

unsigned short mea_Internal_Xilinx_PhyInit_epc7015_Size = sizeof(mea_Internal_Xilinx_PhyInit_epc7015) / sizeof(phy_init_t);

static phy_init_t mea_epc_Port_0_7_PhyInit[] =
{
    //    page  reg  value   mask    test         Notes
    //    ----  ---  ------  ------  -------      -------------------------------------
    { 255,  17,  0x2148, 0x0000, NO_TEST },// Release note 4.1 PHY init patch
    { 255,  16,  0x2144, 0x0000, NO_TEST },//
    { 255,  17,  0x0C28, 0x0000, NO_TEST },//
    { 255, 16, 0x2146, 0x0000, NO_TEST },//
    { 255, 17, 0xB233, 0x0000, NO_TEST },//
    { 255, 16, 0x214D, 0x0000, NO_TEST },//
    { 255, 17, 0xDC0C, 0x0000, NO_TEST },//
    { 255, 16, 0x2159, 0x0000, NO_TEST },//
    { 251, 7, 0xC00D, 0x0000, NO_TEST },//

    //    { 4,    16,  0x0001, 0x0001, RO_TEST },// Set SGMII media mode (16_4:0)
    { 4, 0, 0x8140, 0x7F40, RO_TEST },// Set QSGMII side to 1000, AN=off
    { 4, 26, 0x3102, 0xFFFF, RO_TEST },// disable QSGMII signal detect (workaround 2 in 4.6)
    { 0, 0, 0x1140, 0x7F40, NO_TEST },// set 1000, AN enable, full duplex
    { 0, 4, 0x0140, 0x03E0, RO_TEST },// disable 10/100 half duplex advertice capability
    { 0, 9, 0x0200, 0x0300, RO_TEST },// disable 1000 half duplex advertice capability
    { 0, 0, 0x8000, 0x8000, NO_TEST },// Init soft reset
    { 3, 16, 0x8480, 0xFFFF, RO_TEST },// LED[0] link, LED[2] - activity
    { 3, 17, 0x8811, 0xFFFF, RO_TEST },// LED[0] ON-driver HIGH, LED[1] - LOW
    { 0, 22, 0x0000, 0x00FF, RO_TEST },// Set back active page 0
};
unsigned short mea_epc_Port_0_7_PhyInitSize = sizeof(mea_epc_Port_0_7_PhyInit) / sizeof(phy_init_t);

static phy_init_t mea_epc_Port_8_11_PhyInit[] =
{
    { 255,  17,  0x2148, 0x0000, NO_TEST },// Release note 4.1 PHY init patch
    { 255,  16,  0x2144, 0x0000, NO_TEST },//
    { 255,  17,  0x0C28, 0x0000, NO_TEST },//
    { 255,  16,  0x2146, 0x0000, NO_TEST },//
    { 255,  17,  0xB233, 0x0000, NO_TEST },//
    { 255,  16,  0x214D, 0x0000, NO_TEST },//
    { 255,  17,  0xDC0C, 0x0000, NO_TEST },//
    { 255,  16,  0x2159, 0x0000, NO_TEST },//
    { 251,  7,   0xC00D, 0x0000, NO_TEST },//

    //    { 4,    16,  0x0001, 0x0001, RO_TEST },// Set SGMII media mode (16_4:0)
    { 4, 0, 0x8140, 0x7F40, NO_TEST },// Set QSGMII side to 1000, AN=off + soft reset
    { 4, 26, 0x3102, 0xFFFF, RO_TEST },// disable QSGMII signal detect (workaround 2 in 4.6)
    { 0, 0, 0x1140, 0x7F40, NO_TEST },// set 1000, AN enable, full duplex
    { 0, 4, 0x0140, 0x03E0, RO_TEST },// disable 10/100 half duplex advertise capability
    { 0, 9, 0x0200, 0x0300, RO_TEST },// disable 1000 half duplex advertise capability
    { 0, 0, 0x8000, 0x8000, NO_TEST },// Init soft reset
    { 3, 16, 0x8084, 0xFFFF, RO_TEST },// LED[0] link, LED[2] - activity
    { 3, 17, 0x8811, 0xFFFF, RO_TEST },// LED[0] ON-driver HIGH, LED[1] - LOW
    { 0, 22, 0x0000, 0x00FF, RO_TEST },// Set back active page 0
};
unsigned short mea_epc_Port_8_11_PhyInitSize = sizeof(mea_epc_Port_8_11_PhyInit) / sizeof(phy_init_t);

// PHY post initialization. Called after MAC QSGMII up and stable
static phy_init_t mea_epc_PortPhyPostInit[] =
{
    { 4, 26, 0x3002, 0xFFFF, RO_TEST },// enable QSGMII signal detect (workaround 2 in 4.6)
    { 4, 0, 0x8000, 0x8000, NO_TEST },// QSGMII soft reset
    { 4, 16, 0x0400, 0x0400, RO_TEST },// Force link up
    { 0, 22, 0x0000, 0x00FF, RO_TEST },// Set back active page 0
};
unsigned short mea_epc_PortPhyPostInitSize = sizeof(mea_epc_PortPhyPostInit) / sizeof(phy_init_t);










extern int mea_epc_interface_phy[MEA_MAX_PORT_NUMBER + 1];
#endif








/*-------------------------------- Forward Declarations ------------------------------*/





static ENET_Status       init_ENET_Unit(ENET_Unit_t unit);

#ifndef __KERNEL__
#if defined(MEA_ETHERNITY)

static void *           periodic_thrd_func(void* arg);
static void *           periodic_thrd_func_rmon_pm(void* arg);
static void             periodic_timer_handler(int signum);
static ENET_Uint32       createTimer(int sig_type,/*ENET_Uint32*/ void * timer_handler);
static ENET_Uint32       setTimer(int type, ENET_Uint32 msecPeriod);
static ENET_Status       deleteTimer(int type);

#endif
MEA_Status MEA_OS_CpuIfcInit();
#if (defined(MEA_ETHERNITY)) 
static void*  ENET_thread_rpc_func(void* arg);
#endif
#endif


static startup_params_t main_check_passed_param(char *param);
void MEA_create_comm_thread(void);



/*-------------------------------- Local Variables -----------------------------------*/
#ifndef __KERNEL__
#if defined(MEA_ETHERNITY)
static ENET_Semaphore_t periodic_task_sem ENET_SEMAPHORE_INIT_DEF_VALUE;
#else
#if defined(MEA_ETHERNITY)
static pthread_mutex_t periodic_task_sem  = PTHREAD_MUTEX_INITIALIZER;
#endif
#endif
#if (defined(MEA_OS_LINUX) && defined(MEA_ETHERNITY))
static pthread_t       periodic_thread_id;
static pthread_t       ENET_thread_rpc_id;

#endif
#endif


/*-------------------------------- Global Variables ----------------------------------*/

extern  ENET_Bool ENET_Collect_all_counters_enable ;

extern  ENET_Bool    ENET_portEgress_Link_update;

extern  ENET_Bool    ENET_portIngress_Link_update;

extern  ENET_Bool    ENET_Bonding_Link_update;

/**************************************************************************************/
/* Function Name:	process main													  */
/* 																					  */
/* Description:		main process									                  */
/*																					  */
/* Arguments:		None.															  */
/*																					  */
/* Return:			None.															  */
/*																					  */
/**************************************************************************************/
int ethernity_main1 (int argc, char **argv); 
int ethernity_mainBIST(void); 

#if 1 //defined(MEA_ENV_S1_MAIN) || defined(MEA_OS_PSOS) || defined(__KERNEL__) || (defined(ENET_OS_Z1) && !defined(ENET_OS_DEVICE_MEMORY_SIMULATION))
void ethernity_init() 
{

   //char* my_argv[] = {"enet3000.elf"};
   //int my_argc = 1;
   int my_argc = 0;
   MEA_OS_printf(" #######   ethernity_main1 ##################### \n");
   //ethernity_main1 (my_argc , my_argv);
   ethernity_main1 (my_argc , NULL);
}
#endif 

#ifdef MEA_ETHERNITY_MAIN                    //alex_
int main (int argc,char **argv) 
{
#ifdef SPECMAN_SIM
	cvl_connect(NULL,NULL,0);  /*Call predefined function to establish CVL connection*/
#endif

    ethernity_main1 (argc , argv);
    MEA_OS_printf("ethernity_main1 End\n");
   MEA_API_DeInit_Device_End();

   return 0;
}
#endif                                 //alex_



#ifdef __cplusplus
extern "C" {
 #endif 
int ethernity_main1 (int argc, char **argv)
{	
    int i; 
	char *p_str;
    ENET_Bool b_no_periodic    = ENET_FALSE;
    ENET_Bool b_dbg_kernel_drv = ENET_FALSE;
    ENET_Uint32 dbg_kernel_module = IO_DBG_OFF;
    //ENET_Bool b_bist_test       =  ENET_FALSE;
#ifdef WITH_CUSTOMER_SUPPORT  //IPC
    unsigned long  curr_ip,curr_mask;
    char*  ifName;
    char                     Ip_str[20];
    char                     maskIp_str[20];
#endif



        //tun_init(); /* Init the tun driver first */

        for (i = 1; i < argc; i++){
            switch (main_check_passed_param(argv[i])){
            case e_bist_test:
                b_bist_test = ENET_TRUE;
                break;
            case e_no_periodic:
                b_no_periodic = ENET_TRUE;
                if (b_no_periodic == MEA_TRUE)
                    MEA_OS_printf("user set periodic to off\n");

                break;
            case e_bist_tdm_test:
                tdm_bist_test = ENET_TRUE;
                break;
            case e_dbg_kernel_drv:
                b_dbg_kernel_drv = ENET_TRUE;
                if (b_dbg_kernel_drv == MEA_TRUE)
                    MEA_OS_printf("user set dbg_kernel to ON\n");

                p_str = &(argv[i - 1][2]);
                dbg_kernel_module = MEA_OS_atohex(p_str);
                break;

            case e_dbg_regs_drv:
                MEA_OS_DeviceMemoryDebugRegFlag = ENET_TRUE;
                break;

            case e_help_me:
                MEA_OS_printf(" <%s> no periodic task, -r regs debug on\n",
                    NO_ENET_OS_PERIODIC_TASK_STR);
                return 0;
            case e_dir_up:
                if (MEA_CLI_set_memory_mode(MEA_MODE_UPSTREEM_ONLY, MEA_FALSE) != MEA_OK){
                    MEA_OS_printf("error in mea_set_memory_mode,\n");
                    return 0;
                }

                break;
            case e_dir_down:
                if (MEA_CLI_set_memory_mode(MEA_MODE_DOWNSTREEM_ONLY, MEA_FALSE) != MEA_OK){
                    MEA_OS_printf("error in mea_set_memory_mode,\n");
                    return 0;
                }
                break;

            default:
                MEA_OS_printf("Invalid argument, try --help\n");
                return 0;
            }
        }

        /* Enable log messages */
        if (MEA_OS_LOG_init() == ENET_ERROR) {
            MEA_OS_printf("ERROR: %s - MEA_OS_LOG_init failed\n", __FUNCTION__);
            return ENET_ERROR;
        }




        MEA_OS_DeviceMemoryDebug(dbg_kernel_module);

        /* bistTEST */
        if (b_bist_test) {

            MEA_OS_printf("\nXXXXXXX BIST MODE XXXXXXXX\n");

            MEA_IF_HW_access_disable_flag = MEA_TRUE;
            MEA_IF_HW_ACCESS_DISABLE

                MEA_BM_HW_access_disable_flag = MEA_TRUE;
            MEA_BM_HW_ACCESS_DISABLE

                MEA_CPLD_HW_access_disable_flag = MEA_TRUE;
            MEA_CPLD_HW_ACCESS_DISABLE

                MEA_DB_DDR_HW_access_disable_flag = MEA_TRUE;
            MEA_DB_DDR_HW_ACCESS_DISABLE

        }

        if (tdm_bist_test) {
            MEA_OS_printf("\nXXXXXXX BIST TDM MODE XXXXXXXX\n");
        }



	


	if (init_ENET_Unit(ENET_UNIT_0) != ENET_OK) {
 	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - init_ENET_Unit failed\n",
                          __FUNCTION__);
        /* return ENET_ERROR; */
    }

/* T.B.D. MEA_ENV_S1: open a raw socket to the eth interface to get the packet (need source) */ 
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) && defined(MEA_ETHERNITY) 
    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize CPU Port driver ...\n");
    //mea_drv_Init_cpu_port_drv();
#endif




#ifdef WITH_EAPPL_SUPPORT
    EAPPL_MAIN_Init();
#endif /* WITH_EAPPL_SUPPORT */

 

#ifdef WITH_CUSTOMER_SUPPORT  

#endif /* WITH_CUSTOMER_SUPPORT */


#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) /*&& !defined(PCI_CARD)*/
//#ifdef MEA_ETHERNITY_MAIN  //alex no rpc
#if defined(MEA_ETHERNITY) 
    
    if (pthread_create (&ENET_thread_rpc_id, 
                        NULL,
                        ENET_thread_rpc_func, 
                        NULL)!=ENET_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "rpc thread creation failed\n");
    }
#endif
//#endif









#if defined(MEA_ETHERNITY) && defined(MEA_TIMER_PERIODIC)
    if (!b_bist_test){
        if(pthread_create (&periodic_thread_id, NULL,periodic_thrd_func, NULL)!=ENET_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Periodic thread creation failed\n");
        }
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"periodic_thread_id ..%u.\n",periodic_thread_id);

        if(createTimer(SIGALRM ,/*(ENET_Uint32)*/&periodic_timer_handler)!=ENET_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Periodic timer creation failed creation failed\n");
        }

        {


            ENET_Uint32  timer_interval=1000;


            if ( MEA_device_environment_info.MEA_PERIODIC_INTERVAL_enable) {
                timer_interval = MEA_device_environment_info.MEA_PERIODIC_INTERVAL;
            }
            if(setTimer(ITIMER_REAL, timer_interval)==ENET_ERROR){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Periodic timer value =%d  set failed\n",timer_interval);
            }
        }
    }
#endif


#endif

#if 0
	/* send log message to indicate boot protocol terminated successfully */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Card initialization completed successfully \n"); 
#endif


   
    /* add the CLI commands */
   
	CLI_eng_init();



	if (MEA_device_environment_info.MEA_DEBUG_REGS_Enable_On_Init_enable == MEA_TRUE){
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_OS_DeviceMemoryDebugRegFlag >>>>> OFF \n");
		//MEA_OS_DeviceMemoryDebugRegFlag = MEA_FALSE;
	}

{
	if (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) {
		if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045) == 0) ||
            (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_VDSL64) == 0)) {
#if 0
			/************************************************************************/
			/* Create MNG local Service from port                                   */
			/************************************************************************/
			unsigned int dummy = 0;
			CLI_execCmd("mea service set create 100 FF000 FF000 D.C 0 1 0 1000000000 0 32000 0 0  1 127 -ra 0  -l2Type 0 -no_allow_del 1 \0", &dummy);
			dummy = 0;
			CLI_execCmd("mea service set create 127 FF000 FF000 D.C 0 1 0 1000000000 0 32000 0 0  1 100 -ra 0  -l2Type 0 -no_allow_del 1 \0", &dummy);
			CLI_execCmd("mea port ing set 100 -a 1 \0", &dummy);
			CLI_execCmd("mea port egr set 100 -a 1 \0", &dummy);
			CLI_execCmd("mea port ing set 127 -a 1 \0", &dummy);
			CLI_execCmd("mea port egr set 127 -a 1 \0", &dummy);
#endif

		}
	
   
        if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7015) == 0)) {
            
#if 0
            /************************************************************************/
            /* Create MNG local Service from port                                   */
            /************************************************************************/
            unsigned int dummy = 0;
            CLI_execCmd("mea service set create 101 FF000 FF000 D.C 0 1 0 1000000000 0 32000 0 0  1 127 -ra 0  -l2Type 0 -no_allow_del 1 \0", &dummy);
            dummy = 0;
            CLI_execCmd("mea service set create 127 FF000 FF000 D.C 0 1 0 1000000000 0 32000 0 0  1 101 -ra 0  -l2Type 0 -no_allow_del 1 \0", &dummy);
            CLI_execCmd("mea port ing set 101 -a 1 \0", &dummy);
            CLI_execCmd("mea port egr set 101 -a 1 \0", &dummy);
            CLI_execCmd("mea port ing set 127 -a 1 \0", &dummy);
            CLI_execCmd("mea port egr set 127 -a 1 \0", &dummy);

#endif           
            


        }

    }



}



#ifdef WITH_EAPPL_SUPPORT
    EAPPL_MAIN_CLI_Init();
#endif /* WITH_EAPPL_SUPPORT */

#ifdef WITH_CUSTOMER_SUPPORT
    /* CUSTOMER_MAIN_CLI_Init(); */
#endif /* WITH_CUSTOMER_SUPPORT */

#ifdef WITH_MEA_EXAMPLE_CLI
    MEA_Example_CLI_Init();
#endif /* WITH_MEA_EXAMPLE_CLI */

#ifdef WITH_MEA_EXAMPLE2_CLI
    MEA_Example2_CLI_Init();
#endif /* WITH_MEA_EXAMPLE2_CLI */
#ifdef WITH_MEA_EXAMPLE3_CLI
    MEA_Example3_CLI_Init();
#endif /* WITH_MEA_EXAMPLE2_CLI */

#ifdef WITH_MEA_EXAMPLE4_CLI
    MEA_Example4_CLI_Init();
#endif /* WITH_MEA_EXAMPLE4_CLI */
#ifdef WITH_MEA_EXAMPLE5_CLI
    MEA_Example5_CLI_Init();
#endif /* WITH_MEA_EXAMPLE5_CLI */

#ifdef WITH_MEA_EXAMPLE_TMUX_CLI
    MEA_Example_tmux_CLI_Init();
#endif

	/* check if need to change prompt*/
	if (b_bist_test)
		CLI_setPrompt("bist>");
	else if (globalMemoryMode!=MEA_MODE_REGULAR_ENET4000){
		if(MEA_CLI_set_memory_mode(globalMemoryMode,MEA_TRUE)!=MEA_OK){
		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							"init - failed to init prompt\n");
	   }
	}

#ifdef MEA_RPC_ENG
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,
						"call to create coom thread\n");

	MEA_create_comm_thread();
#endif


#ifdef MEA_ETHERNITY_MAIN
          //alex_  #ifdef
#if defined(ENET_OS_LINUX) || defined(ENET_OS_DEVICE_MEMORY_SIMULATION)


    if (MEA_device_environment_info.MEA_CLI_ENG_DISABLE_enable == MEA_TRUE && 
        MEA_device_environment_info.MEA_CLI_ENG_DISABLE == MEA_TRUE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "NO CLI MAIN\n");
        for (;;) {
            if (gMEA_KillEnet == MEA_G_KILL_ENET) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                    "..... THE ENET GO Down ......\n");
                //MEA_OS_sleep(1, 0);
                //close(g_rpc_ns);
                break;
            }
            MEA_OS_sleep(5, 0);
        }

    }
    else {

        //This must be the last call since it is the process main loop
#ifdef MEA_ETHERNITY_MAIN
        CLI_eng_main(NULL);
#endif

#if !defined(ENET_OS_DEVICE_MEMORY_SIMULATION)
        close(g_rpc_ns);
#endif


    }
#endif


#endif 



    











 
  


    return 0;


}

#ifdef __cplusplus
 }
#endif 

int ethernity_mainBIST(void){
 int argc =2;
 char *argv[]={"enet3000.elf","-b"};
 
 return ethernity_main1 (argc , argv);
}

/* Ethernity Init for Aricent*/
#ifdef __cplusplus
extern "C" {
 #endif 
MEA_Status ethernity_aricent_init ()
{	
	/* Enable log messages */
    if(MEA_OS_LOG_init ()==ENET_ERROR) {
       MEA_OS_printf("ERROR: %s - MEA_OS_LOG_init failed\n",__FUNCTION__);
       return ENET_ERROR; 
    }

	/*MEA_OS_DeviceMemoryDebug(dbg_kernel_module);*/

	/* Preform HW Initialization */
	if (init_ENET_Unit(ENET_UNIT_0) != ENET_OK) {
 	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - init_ENET_Unit failed\n",
                          __FUNCTION__);
        return ENET_ERROR;
    }

	/* T.B.D. MEA_ENV_S1: open a raw socket to the eth interface to get the packet (need source) */ 
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) && defined(MEA_ETHERNITY) 
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize CPU Port driver ...\n");
    if (mea_drv_Init_cpu_port_drv() != ENET_OK) {
 	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_cpu_port_drv failed\n",
                          __FUNCTION__);
        return ENET_ERROR;
    }
#endif
  
    /* add the CLI commands */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize CLI_eng_init .....\n");
	CLI_eng_init();

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) 
//#if (defined(MEA_ETHERNITY) && !defined(MEA_OS_ADVA_TDM) && !defined(MEA_OS_ETH_PROJ_2) && !defined(MEA_OS_AOP)) || defined(MEA_ENV_S1_MAIN)
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize RPC thread .....\n");
    if (pthread_create (&ENET_thread_rpc_id, 
                        NULL,
                        ENET_thread_rpc_func, 
                        NULL)!=ENET_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "rpc thread creation failed\n");
        return ENET_ERROR;
    }
//#endif
#endif
#if defined(MEA_ETHERNITY) && defined(MEA_TIMER_PERIODIC)
              if(pthread_create (&periodic_thread_id, NULL,periodic_thrd_func_rmon_pm, NULL)!=ENET_OK){
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Periodic thread creation failed\n");
              }
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"periodic_thread_id ..%u.\n",periodic_thread_id);
      
              if(createTimer(SIGALRM ,periodic_timer_handler)!=ENET_OK){
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Periodic timer creation failed creation failed\n");
              }
      
        {
            char        *timer_interval_env;
 
 
 
            ENET_Uint32  timer_interval=1000;
 
            timer_interval_env = MEA_OS_getenv("MEA_PERIODIC_INTERVAL");
            if (timer_interval_env) {
                timer_interval = (MEA_Uint32)(MEA_OS_atoi(timer_interval_env));
            }
              if(setTimer(ITIMER_REAL, timer_interval)==ENET_ERROR){
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Periodic timer value =%d  set failed\n",timer_interval);
                  }
        }
#endif
   return ENET_OK;
}


#if 0

int test_sock(void) {
	int sock, n;
	char buffer[256]="spapi";
	unsigned char *iphead, *ethhead;
	struct sockaddr_ll mysocket,to;
	struct ifreq ifr;

  
	if ( (sock=socket(PF_PACKET, SOCK_RAW, 
					  htons(ETH_P_ALL)))<0) {
	  perror("socket");
	  exit(1);
	}
#if 0
	strncpy(ifr.ifr_name, "eth0", sizeof(ifr.ifr_name));
	if (ioctl(sock, SIOCGIFINDEX, &ifr) == -1)
	{

	fprintf(stderr, "SIOCGIFINDEX on %s failed: %s\n",
	"eth0",strerror(errno));
	printf("my error\n");
	return 1;
	}

	MEA_OS_memset(&mysocket,'\0',sizeof(mysocket));
	mysocket.sll_family = AF_PACKET;
	mysocket.sll_protocol =htons(ETH_P_ALL);
	mysocket.sll_ifindex  = ifr.ifr_ifindex;
	
	if (bind(sock,(struct sockaddr *)&mysocket, sizeof(mysocket)) <0)
	{
	perror("Bind");
	exit(1);
	}
	
	MEA_OS_memset(&to , 0,sizeof(to));
	to.sll_family = AF_PACKET;
	to.sll_protocol =htons(ETH_P_ALL);
	to.sll_ifindex  = ifr.ifr_ifindex;
	to.sll_halen = ETH_ALEN;
	
#endif

  while (1) {

    printf("----------\n");
    n = recvfrom(sock,buffer,256,0,NULL,NULL);
    printf("%d bytes read\n",n);

    /* Check to see if the packet contains at least 
     * complete Ethernet (14), IP (20) and TCP/UDP 
     * (8) headers.
     */
    if (n<42) {
      perror("recvfrom():");
      printf("Incomplete packet (errno is %d)\n",
             errno);
      close(sock);
      exit(0);
    }

    ethhead = buffer;
    printf("Source MAC address: "
           "%02x:%02x:%02x:%02x:%02x:%02x\n",
           ethhead[0],ethhead[1],ethhead[2],
           ethhead[3],ethhead[4],ethhead[5]);
    printf("Destination MAC address: "
           "%02x:%02x:%02x:%02x:%02x:%02x\n",
           ethhead[6],ethhead[7],ethhead[8],
           ethhead[9],ethhead[10],ethhead[11]);

    iphead = buffer+14; /* Skip Ethernet header */
    if (*iphead==0x45) { /* Double check for IPv4
                          * and no options present */
      printf("Source host %d.%d.%d.%d\n",
             iphead[12],iphead[13],
             iphead[14],iphead[15]);
      printf("Dest host %d.%d.%d.%d\n",
             iphead[16],iphead[17],
             iphead[18],iphead[19]);
      printf("Source,Dest ports %d,%d\n",
             (iphead[20]<<8)+iphead[21],
             (iphead[22]<<8)+iphead[23]);
      printf("Layer-4 protocol %d\n",iphead[9]);
    }

#if 0
	n = sendto(sock,buffer,sizeof(buffer),0x0,(struct sockaddr *)&to,sizeof(to));
	if(n < 0)
	{
	perror("sendto");
	exit(1);
	}
	else  printf("Packet send %d bytes \n",n);
#endif

  }
}

#endif


#if defined(ENET_OS_LINUX) && defined(MEA_ETHERNITY) && !defined(__KERNEL__)
static ENET_Uint32 createTimer(int sig_type,/*ENET_Uint32*/ void* timer_handler) 
{
    struct sigaction sa;
	/* Install timer_handler as the signal handler for SIGVTALRM. */
	MEA_OS_memset (&sa, 0, sizeof (sa));
   sa.sa_handler = /*(ENET_VOIDFUNCPTR)*/ timer_handler;
	sigaction (sig_type, &sa, NULL);
  	return ENET_OK;
}

static ENET_Uint32 setTimer(int type, ENET_Uint32 msecPeriod) 
{
  	
	struct itimerval timer;

	
    if(msecPeriod < 1000){
    
    timer.it_value.tv_sec = 0;
	timer.it_value.tv_usec = msecPeriod * 1000;
	timer.it_interval.tv_sec = 0;
	timer.it_interval.tv_usec = msecPeriod *1000;
    }else{
        timer.it_value.tv_sec = msecPeriod/1000;
        timer.it_value.tv_usec = 0;//msecPeriod * 1000;
        timer.it_interval.tv_sec = msecPeriod/1000;
        timer.it_interval.tv_usec = 0;//msecPeriod *1000;

    }

    if(setitimer (type , &timer, NULL)!= ENET_OK)
		return ENET_ERROR;

	return ENET_OK;
}


static ENET_Status deleteTimer(int type) 
{
   	struct itimerval timer;

	/* Configure the timer to expire after 250 msec... */
	timer.it_value.tv_sec = 0;
	timer.it_value.tv_usec = 0;
	/* ... and every 250 msec after that. */
	timer.it_interval.tv_sec = 0;
	timer.it_interval.tv_usec = 0;

    if(setitimer (type, &timer, NULL)!= ENET_OK)
		return ENET_ERROR;

	return ENET_OK;


}




void periodic_timer_handler(int signum)
{
    int ret=0;

    ret = pthread_mutex_trylock(&periodic_task_sem);
    if(ret == 0 || ret == EBUSY)
    {
        pthread_mutex_unlock(&periodic_task_sem);
    }
}



void * periodic_thrd_func(void* arg)
{
#if MEA_TEST_MEASURE_TIME_PERIODIC
int myCount=0;
struct timeval tvBegin, tvEnd, tvDiff;

    time_t in_t;
    //time_t out_t; 
#endif    
    
     ENET_Collect_all_counters_enable = MEA_device_environment_info.MEA_PERIODIC_ENABLE;
    
    if(pthread_mutex_trylock(&periodic_task_sem)==0){
		//MEA_OS_memcpy(ENET_OS_LOG_DEBUG,"periodic_task_sem was unlocked locking it\n");
	}

    
    if (MEA_device_environment_info.MEA_PORT_EGRESS_UPDATE_LINK_enable) {
        ENET_portEgress_Link_update = (MEA_Uint32)(MEA_device_environment_info.MEA_PORT_EGRESS_UPDATE_LINK);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"ENET_portEgress_Link_update = %d\n",ENET_portEgress_Link_update);
    }

   
    if (MEA_device_environment_info.MEA_PORT_INGRESS_UPDATE_LINK_enable) {
        ENET_portIngress_Link_update = (MEA_Uint32)(MEA_device_environment_info.MEA_PORT_INGRESS_UPDATE_LINK);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"ENET_portIngress_Link_update = %d\n",ENET_portIngress_Link_update);
    }

	
    if (MEA_device_environment_info.MEA_PORT_BONDING_LINK_enable) {
        ENET_Bonding_Link_update = (MEA_Uint32)(MEA_device_environment_info.MEA_PORT_BONDING_LINK);
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"ENET_Bonding_Link_update = %d\n",ENET_Bonding_Link_update);
	}




	while(ENET_TRUE){
		pthread_mutex_lock(&periodic_task_sem);

                ENET_Lock();
               

                if (ENET_Collect_all_counters_enable ==	ENET_FALSE || MEA_reinit_start) {
                   ENET_Unlock();
                   continue;
                }

#if defined(MEA_ETHERNITY) && (defined(MEA_ENV_S1_K7) || defined(MEA_PLAT_S1_VDSL_K7))
                if (MEA_device_environment_info.MEA_SFP_SUPPORT_enable == MEA_TRUE ){
                        mea_sfp_periodic_task();
                    }
                
                

#endif
#if MEA_TEST_MEASURE_TIME_PERIODIC
              myCount++;
               //Alex
               if(myCount==10){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " counters::Start ...\n");
				time(&in_t);
                gettimeofday(&tvBegin, NULL);
               }
#endif			   

                if (MEA_API_Collect_Counters_All(ENET_UNIT_0) != ENET_OK) {
                    MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                                       "%s - ENET_Collect_Counters_All failed "
                                       "- disable periodic \n",
                                       __FUNCTION__);
                    ENET_Collect_all_counters_enable=ENET_FALSE;
                    ENET_Unlock();
                    continue;
                }
		//enet_json_db_periodic_collect();
                 
                MEA_API_Check_Interface_Link_Status(ENET_UNIT_0,MEA_FALSE);

                 if(ENET_Bonding_Link_update){
                     //MEA_OS_LOG_logMsg (MEA_OS_LOG_EVENT,"start Bonding \n");
                     MEA_API_check_bonding_state();
                 }

				ENET_Unlock();
#if MEA_TEST_MEASURE_TIME_PERIODIC				
				if(myCount==10){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " counters::End ...\n");
				 //time(&out_t);
				 gettimeofday(&tvEnd, NULL);
                 MEA_OS_timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
				 MEA_OS_timeval_print(&tvBegin);
                 CLI_print("End      ");
                 MEA_OS_timeval_print(&tvEnd);
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Diff = %ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);
                myCount=0;
               }
#endif			   
	}
	return NULL;

}

void * periodic_thrd_func_rmon_pm(void* arg)
{

    char  *periodic_enable_env;
#if MEA_TEST_MEASURE_TIME_PERIODIC
int myCount=0;
struct timeval tvBegin, tvEnd, tvDiff;

    time_t in_t;
    //time_t out_t; 
#endif    

#if 0    
    char  *periodic_setEgress_env;
    char  *periodic_setIngressUpdate_env;
	char  *periodic_BONDING_Update_env;
#endif
    periodic_enable_env = MEA_OS_getenv("MEA_PERIODIC_ENABLE");
    if (periodic_enable_env) {
        ENET_Collect_all_counters_enable = (MEA_Uint32)(MEA_OS_atoi(periodic_enable_env));
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Collect_all_counters_enable = %d\n",ENET_Collect_all_counters_enable);
    }

    if(pthread_mutex_trylock(&periodic_task_sem)==0){
		//MEA_OS_memcpy(ENET_OS_LOG_DEBUG,"periodic_task_sem was unlocked locking it\n");
	}


#if defined(MEA_ETHERNITY) && defined(MEA_ENV_S1_K7)
	if(mea_sfp_open_connection() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"failed to open device driver connection");
	}
#endif

	while(ENET_TRUE){
		pthread_mutex_lock(&periodic_task_sem);

                ENET_Lock();
               
               

                if (ENET_Collect_all_counters_enable ==	ENET_FALSE || MEA_reinit_start) {
                   ENET_Unlock();
                   continue;
                }
 #if MEA_TEST_MEASURE_TIME_PERIODIC
              myCount++;
               //Alex
               if(myCount==10){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " counters::Start ...\n");
				time(&in_t);
                gettimeofday(&tvBegin, NULL);
               }
#endif	

#if defined(MEA_ETHERNITY) && defined(MEA_ENV_S1_K7)
                 mea_sfp_periodic_task();

#endif


                if (MEA_API_Collect_Counters_PMs(ENET_UNIT_0) != ENET_OK) {
                    MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                                       "%s - MEA_API_Collect_Counters_PMs failed "
                                       "- disable periodic \n",
                                       __FUNCTION__);
                    ENET_Collect_all_counters_enable=ENET_FALSE;
                    ENET_Unlock();
                    continue;
                }

               if (MEA_API_Collect_Counters_RMONs(ENET_UNIT_0) != ENET_OK) {
                    MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                                       "%s - MEA_API_Collect_Counters_RMONs failed "
                                       "- disable periodic \n",
                                       __FUNCTION__);
                    ENET_Collect_all_counters_enable=ENET_FALSE;
                    ENET_Unlock();
                    continue;
                }
				
               if (MEA_API_Collect_Counters_PMs_Block(MEA_UNIT_0, 0) != ENET_OK) {
                                   MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                                                      "%s - MEA_API_Collect_Counters_PMs failed "
                                                      "- disable periodic \n",
                                                      __FUNCTION__);
                                   ENET_Collect_all_counters_enable=ENET_FALSE;
                                   ENET_Unlock();
                                   continue;
                }

	       if (MEA_API_Collect_Counters_Queues(MEA_UNIT_0) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				       "%s - MEA_API_Collect_Counters_Queues failed \n",
				       __FUNCTION__);
                    ENET_Collect_all_counters_enable=ENET_FALSE;
                    ENET_Unlock();
                    continue;
	       }

				if ( MEA_API_Collect_Counters_Forwarder(ENET_UNIT_0) != ENET_OK) {
                    MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                                       "%s -  MEA_API_Collect_Counters_Forwarder failed "
                                       "- disable periodic \n",
                                       __FUNCTION__);
                    ENET_Collect_all_counters_enable=ENET_FALSE;
                    ENET_Unlock();
                    continue;
                }
             
			 MEA_API_Check_Interface_Link_Status(ENET_UNIT_0,MEA_FALSE);

#if defined(MEA_ETHERNITY) && defined(MEA_ENV_S1_K7)
                if (MEA_device_environment_info.MEA_SFP_SUPPORT_enable == MEA_TRUE 
                     ){
                        mea_sfp_periodic_task();
                    }
                
                

#endif
#if MEA_TEST_MEASURE_TIME_PERIODIC				
				if(myCount==10){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " counters::End ...\n");
				 //time(&out_t);
				 gettimeofday(&tvEnd, NULL);
                 MEA_OS_timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
				 MEA_OS_timeval_print(&tvBegin);
                 CLI_print("End      ");
                 MEA_OS_timeval_print(&tvEnd);
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Diff = %ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);
                myCount=0;
               }
#endif	

				ENET_Unlock();
	}
	return NULL;

}

#endif /* ENET_OS_LINUX */




startup_params_t main_check_passed_param(char *param)
{

	if(!strncmp(param,NO_ENET_OS_PERIODIC_TASK_STR,2)){
		return e_no_periodic;
	}
	

	if(!strncmp(param,DBG_KERNEL_DRV,2)){
		return e_dbg_kernel_drv;
	}

	if(!strncmp(param,DBG_REGS_DRV,2)){
		return e_dbg_regs_drv;
	}

	if(!strncmp(param,HELP_PLEASE,2)){
		return e_help_me;
	}
    if(!strncmp(param,ENET_DBG_BIST_TEST,2)){
		return e_bist_test;
	}
   if(!strncmp(param,ENET_DIR_UP_DRV,3)){
		return e_dir_up;
	}
   if(!strncmp(param,ENET_DIR_DOWN_DRV,3)){
		return e_dir_down;
	}
   if(!strncmp(param,ENET_DBG_BIST_TDM,4)){
       return e_bist_tdm_test;
   }

    
	
        return e_invalid_param;


}







ENET_Status init_ENET_Unit(ENET_Unit_t unit_i)
{

    MEA_ULong_t module_base_addr_vec[ENET_MODULE_LAST];
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize base_addr_vec...\n");
    MEA_OS_memset(&(module_base_addr_vec[0]), 0, sizeof(module_base_addr_vec));
    MEA_API_Device_environment_Init(NULL);


#ifndef HW_BOARD_IS_PCI

    module_base_addr_vec[ENET_MODULE_CPLD] = ENET_CPLD_BASE_ADDR;
    module_base_addr_vec[ENET_MODULE_IF] = ENET_IF_BASE_ADDR;
    /* note that BM address may change based on IF systype register */
    module_base_addr_vec[ENET_MODULE_BM] = ENET_BM_BASE_ADDR;
    

    module_base_addr_vec[ENET_MODULE_DATA_DDR] = ENET_DATA_DDR_BASE_ADDR;



#else // !PCI_CARD
#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)
    //MEA_Uint32 pciAddr;
    module_base_addr_vec[ENET_MODULE_CPLD] = 0;
    module_base_addr_vec[ENET_MODULE_IF] = 0;
    module_base_addr_vec[ENET_MODULE_BM] = 0;
    module_base_addr_vec[ENET_MODULE_DATA_DDR] = ENET_DATA_DDR_BASE_ADDR;

   MEA_device_environment_init();

    MEA_PCI_Init_bdf();

    if (MEA_PCIe_Init(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " - The PCIe  card exist \n");
    }


    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " - The PCI card exist \n");
#if 0
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " - Check the size of types \n");

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Uint8      = %d  \n", sizeof(MEA_Uint8));
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Uint16     = %d  \n", sizeof(MEA_Uint16));
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Int32      = %d  \n", sizeof(MEA_Int32));
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Uint32     = %d  \n", sizeof(MEA_Uint32));
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "int            = %d  \n", sizeof(int));

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_int64      = %d  \n", sizeof(MEA_int64));
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_uint64     = %d  \n", sizeof(MEA_uint64));
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_funcParam  = %d  \n", sizeof(MEA_funcParam));
#endif        

#endif


#endif




#ifndef __KERNEL__
#if !defined(ENET_OS_DEVICE_MEMORY_SIMULATION)






    MEA_OS_CpuIfcInit();


#endif //!defined(ENET_OS_DEVICE_MEMORY_SIMULATION)
#endif


#if defined(MEA_ETHERNITY) && defined(MEA_OS_LINUX) &&  defined(MEA_OS_EPC_ZYNQ7045) 
    {
        unsigned char phy;

        /*    PHY reset remove*/
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "PHY reset remove \n");
        system("echo 1,15,1 > /proc/sys/dev/ps7/mio_pin");
        system("echo 1,16,1 > /proc/sys/dev/ps7/mio_pin");
        system("echo 1,17,1 > /proc/sys/dev/ps7/mio_pin");

        if (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "PRODUCT_NAME_enable \n");
            if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045) == 0) ||
                (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_VDSL64) == 0) || 
                (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_1K) == 0)  ) {


                /*call to Init Phy  */
                for (phy = 4; phy <= 11; phy++) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "PHY Init %d \n", phy);
                    phy_init(phy, &mea_epc_Port_0_7_PhyInit[0], mea_epc_Port_0_7_PhyInitSize);
                }


                /**/


                for (phy = 12; phy <= 15; phy++) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "PHY Init %d \n", phy);
                    phy_init(phy, &mea_epc_Port_8_11_PhyInit[0], mea_epc_Port_8_11_PhyInitSize);
                }

            }

            if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7015) == 0)) {


                


                //for (phy = 0; phy <= 1; phy++) {
                //    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "PHY Init %d \n", phy);
                //    phy_init(phy, &mea_epc7015_Port_PhyInit[0], mea_epc7015_Port_PhyInit_PhyInitSize);
                //}
            }

        }




        /*remove the FPGA from reset */
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Remove FPGA HW/SW reset \n");
        system("echo 0,1 > /proc/sys/dev/ps7/pl_reset"); /*software*/
        system("echo 1,1 > /proc/sys/dev/ps7/pl_reset"); /*hw*/
        MEA_OS_sleep(0, 500 * 1000 * 1000);

        if (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) {
            if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7015) == 0) || 
                (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7030) == 0) ) {
                MEA_OS_sleep(0, 500 * 1000 * 1000);
                system("phy.out -i eth1 -a 7 -c write -r 4 -d 0xd801");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth1 -a 7 -c write -r 0 -d 0x1340");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth1 -a 6 -c write -r 4 -d 0xd801");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth1 -a 6 -c write -r 0 -d 0x1340");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth1 -a 5 -c write -r 4 -d 0xd801");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth1 -a 5 -c write -r 0 -d 0x1340");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth1 -a 4 -c write -r 4 -d 0xd801");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth1 -a 4 -c write -r 0 -d 0x0140");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("echo 1,15,0 > /proc/sys/dev/ps7/mio_pin");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("echo 1,16,0 > /proc/sys/dev/ps7/mio_pin");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("echo 1,15,1 > /proc/sys/dev/ps7/mio_pin");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("echo 1,16,1 > /proc/sys/dev/ps7/mio_pin");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 0 -c page -p 18");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 0 -c write -r 20 -d 0x8001");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 0 -c page -p 0");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 1 -c page -p 18");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 1 -c write -r 20 -d 0x8001");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 1 -c page -p 0");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 0 -c page -p 3");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 0 -c write -r 16 -d 0x1840");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 0 -c write -r 17 -d 0x4405");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 1 -c page -p 3");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 1 -c write -r 16 -d 0x1840");
                MEA_OS_sleep(0, 1 * 1000 * 1000);
                system("phy.out -i eth0 -a 1 -c write -r 17 -d 0x4405");
                MEA_OS_sleep(0, 1 * 1000 * 1000);

            }
        }



    }
#endif


    // ENET initialization 
    ENET_Lock();
#if defined(MEA_OS_EPC_ZYNQ7045) || defined(HW_BOARD_IS_PCI) ||  defined(MEA_OS_ZYNQ7000) || defined(MEA_OS_ZYNQ7045) || defined(MEA_OS_ZYNQUS)
    MEA_API_RegisterFpgaSwReset((reset_func) ResetFpga_SW_FORCE);
#endif




    if (MEA_API_Init(MEA_UNIT_0,
        &(module_base_addr_vec[0])) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_Init failed\n", __FUNCTION__);
        ENET_Unlock();
        return ENET_ERROR;
    }


    ENET_Unlock();

#if defined(MEA_ETHERNITY) && defined(MEA_OS_LINUX) &&  defined(MEA_OS_EPC_ZYNQ7045)

    if ( (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) &&
         ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045) == 0) ||
          (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_VDSL64) == 0) ||
          (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_1K) ==  0)    
         )
        )
    
    {

        unsigned char phy, subindex;
        //MEA_SGMII_Entry_dbt  SGMIIentry;
        MEA_QSGMII_Status_Entry_dbt QSGMIIentryStat;
        //        MEA_Port_t interface_port[12];
        MEA_Port_t port;
        int phyId, Id;


        MEA_OS_sleep(0, 100*1000*1000);
        /* post init */
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " BOARD_7045 --->> PHY post init \n");
        for (phy = 4; phy <= 15; phy++) {
            MEA_OS_sleep(0, 100 * 1000 * 1000);
            phy_init(phy, &mea_epc_PortPhyPostInit[0], mea_epc_PortPhyPostInitSize);
        }
        MEA_OS_sleep(1, 0);




        MEA_OS_memset(&mea_epc_interface_phy[0], 0, sizeof(mea_epc_interface_phy));
        mea_epc_interface_phy[105] = 9;
        mea_epc_interface_phy[107] = 11;
        mea_epc_interface_phy[101] = 5;
        mea_epc_interface_phy[103] = 7;
        mea_epc_interface_phy[104] = 8;
        mea_epc_interface_phy[106] = 10;
        mea_epc_interface_phy[100] = 4;
        mea_epc_interface_phy[102] = 6;
        mea_epc_interface_phy[109] = 13;
        mea_epc_interface_phy[110] = 14;
        mea_epc_interface_phy[108] = 12;
        mea_epc_interface_phy[111] = 15;

        for (Id = 0; Id < 127; Id++) {

            phyId = (unsigned char)mea_epc_interface_phy[Id];

 
            if (phyId) {
 
            }

        }



        MEA_OS_sleep(1, 0);
        /* check the link up for 10times */
        MEA_OS_memset(&QSGMIIentryStat, 0, sizeof(QSGMIIentryStat));
        for (phy = 0; phy < 4; phy++) {
            port = 100;
            MEA_OS_memset(&QSGMIIentryStat, 0, sizeof(QSGMIIentryStat));
            MEA_API_Get_Interface_QSGMII_Status_Entry(MEA_UNIT_0, port, &QSGMIIentryStat);
            for (subindex = 0; subindex < 4; subindex++)
            {
                printf("interface-[ %4d] LinkStatus %s \n", port + subindex,
                    (QSGMIIentryStat.qsgmii[subindex].value.link_Status == MEA_TRUE)
                    ? "UP" : "Down");
            }
            MEA_OS_memset(&QSGMIIentryStat, 0, sizeof(QSGMIIentryStat));
            port = 104;
            MEA_API_Get_Interface_QSGMII_Status_Entry(MEA_UNIT_0, port, &QSGMIIentryStat);
            for (subindex = 0; subindex < 4; subindex++)
            {
                printf("interface-[ %4d] LinkStatus %s \n", port + subindex,
                    (QSGMIIentryStat.qsgmii[subindex].value.link_Status == MEA_TRUE)
                    ? "UP" : "Down");
            }
            MEA_OS_memset(&QSGMIIentryStat, 0, sizeof(QSGMIIentryStat));
            port = 108;
            MEA_API_Get_Interface_QSGMII_Status_Entry(MEA_UNIT_0, port, &QSGMIIentryStat);
            for (subindex = 0; subindex < 4; subindex++)
            {
                printf("interface-[ %4d] LinkStatus %s \n", port + subindex,
                    (QSGMIIentryStat.qsgmii[subindex].value.link_Status == MEA_TRUE)
                    ? "UP" : "Down");
            }

        }

    } //end ENET_EPC_7045
    if ((MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) &&
        (strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7015) == 0))

    {
        
            
        

    }//end EPC_7015




#endif


#if defined(MEA_OS_LINUX) &&  defined(MEA_OS_EPC_ZYNQ7045)
 



        if (MEA_device_environment_info.MEA_REINIT_DISABLE_enable == MEA_TRUE) 
        {
            if (MEA_device_environment_info.MEA_REINIT_DISABLE == MEA_TRUE) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Reinit Disable\n\n");
            }
            else {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR \n");

                if (MEA_Platform_ResetDevice(MEA_UNIT_0) != MEA_OK) {
                }

                if (MEA_API_ReInit(MEA_UNIT_0) != MEA_OK) {
                }
            }

        }else {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR \n");

            if (MEA_Platform_ResetDevice(MEA_UNIT_0) != MEA_OK) {
            }

            if (MEA_API_ReInit(MEA_UNIT_0) != MEA_OK) {
            }


        }

#endif
 

    return ENET_OK;	

}




#if defined(ENET_OS_LINUX) && !defined(__KERNEL__)

#if 0
 #define ENET_RPC_HDR_BINARY*
#endif
 #define  ENET_MAGIC_NUMBER 8
typedef struct {

#ifdef ENET_RPC_HDR_BINARY
       ENET_Uint32 magic_number;
       ENET_Uint32 type;
       ENET_Uint32 len;
#else
       char       magic_number[ENET_MAGIC_NUMBER];
       char       pad1;
       char       type;
       char       pad2;
       char       len[3];

#endif
   } hdr_buf_t;


typedef struct {
     char   retOk;
     char   pad;
     char   magic_number[ENET_MAGIC_NUMBER];     
} hdr_buf_ret_t;

#define  BUFFER_SIZE_RPC (16*1024)

ENET_Uint32 Temp_RPC_val[50];

ENET_Bool ENET_thread_rpc_cli_active = ENET_FALSE;
int       ENET_thread_rpc_cli_fd     = -1;

int   ENET_thread_rpc_cli_print(char *str) {

    hdr_buf_t hdr_buf;
    int len = (str==NULL) ? 0 : MEA_OS_strlen(str)+1;
    if (!ENET_thread_rpc_cli_active) {
        return -1;
    }
    if (len > 1023) {
        return -1;
    }
    MEA_OS_memset(&hdr_buf,0,sizeof(hdr_buf));
    hdr_buf.pad1=' ';
    hdr_buf.pad2=' ';
#ifdef ENET_RPC_HDR_BINARY
    hdr_buf.magic_number = MEA_OS_htonl(0xbabeface);
    hdr_buf.len  = MEA_OS_htonl (len);
    hdr_buf.type = MEA_OS_htonl(2);
#else
    MEA_OS_memcpy(hdr_buf.magic_number,"babeface",8);
    hdr_buf.type='2';
    hdr_buf.len[0] = (len / 100) + '0';
    hdr_buf.len[1] = ((len%100) / 10) + '0';
    hdr_buf.len[2] = (len%10)  + '0';
#endif
    if (ENET_OS_WRITE(ENET_thread_rpc_cli_fd,
                      (char*)&hdr_buf,
                      sizeof(hdr_buf)
                      ) != sizeof (hdr_buf)) {
        return -1;
    }
    if (len > 0) {
        if (ENET_OS_WRITE(ENET_thread_rpc_cli_fd,
                          (char*)str,
                          len
                          ) != len) {
            return -1;
        }
    }

    return len;
}
#if defined(MEA_ETHERNITY)
void*  ENET_thread_rpc_func(void* arg) {
    	
    unsigned int add_hist;
    
    struct sockaddr_in from;
    socklen_t          from_len = sizeof(from);
    static char               buff[BUFFER_SIZE_RPC];
    hdr_buf_t       hdr_buf;
    hdr_buf_ret_t   hdr_buf_ret;
    char           *ENET_card_str = MEA_OS_getenv("MEA_CARD");
    int             ENET_card     = ((ENET_card_str) ? atoi(ENET_card_str) : 0);
    unsigned short  port          = 4443 + ENET_card;
#if 0
    MEA_Uint32 valid_client_addr =
#ifdef MEA_PLAT_EVALUATION_BOARD
        INADDR_ANY;
#else
        0x7f000001;
#endif
#endif

    int len;
    int i;
    int type;
    int n;
	int xxx;

//     char                        ip_str[20];
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
        "Init thread_rpc_func\n");

    /* create socket */
    if ((Enet_sock =socket(PF_INET,
                    SOCK_STREAM, 
                    0))<0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - socket creation failed (errno=%d '%s')\n",
                          __FUNCTION__, 
                          errno,
                          strerror(errno));
		return NULL;
	}
	xxx=1;
	if ((setsockopt(Enet_sock, SOL_SOCKET, SO_REUSEADDR, &xxx, sizeof(xxx))) == -1) {
		perror("setsockopt()");
		exit(1);
	}

    /* create bind info for input */
	MEA_OS_memset(&(from),'\0',sizeof(from));
    from.sin_family      = AF_INET;
//Alex    from.sin_addr.s_addr = MEA_OS_htonl(valid_client_addr);
    from.sin_port = htons(port);

    /* make bind for incoming packets */ 	
	if (bind(Enet_sock,(struct sockaddr *)&(from),sizeof(from)) <0) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - bind failed XXXXXX   (errno=%d '%s')\n",
                          __FUNCTION__,
                          errno,
                          strerror(errno));
		//MEA_OS_sleep(1,0);				  
        //close(g_rpc_ns);
	  return NULL;
	}


    if(listen(Enet_sock,5) != 0) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - listen failed (errno=%d '%s')\n",
                          __FUNCTION__,
                          errno,
                          strerror(errno));
		return NULL;

    }
	
    while (1) {
		if ((g_rpc_ns = accept (Enet_sock,(struct sockaddr*)&from,&from_len)) == -1) {
 		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - accept failed (errno=%d '%s')\n",
                           __FUNCTION__,
                          errno,
                          strerror(errno));
           
            continue;
        }   
        while ( 1 ) {
			len = read (g_rpc_ns, (char*)&hdr_buf, sizeof(hdr_buf));
            if (len != sizeof (hdr_buf)) {
#if 0
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - read hdr failed (errno=%d '%s') (len=%d)\n",
                               __FUNCTION__,
                               errno,
                               strerror(errno),
                               len);
#endif
				close(g_rpc_ns);
               
                break;
            }


#if 0
#ifndef ENET_RPC_HDR_BINARY
            printf ("magic= '%c' '%c' '%c' '%c' '%c' '%c' '%c' '%c' \n",
                hdr_buf.magic_number[0],
                hdr_buf.magic_number[1],
                hdr_buf.magic_number[2],
                hdr_buf.magic_number[3],
                hdr_buf.magic_number[4],
                hdr_buf.magic_number[5],
                hdr_buf.magic_number[6],
                hdr_buf.magic_number[7]);
            printf ("type = '%c'  %d\n",hdr_buf.type,hdr_buf.type);
  
            printf ("len = '%c' '%c' '%c' \n",
                hdr_buf.len[0],
                hdr_buf.len[1],
                hdr_buf.len[2]);

#endif 
#endif

#ifdef ENET_RPC_HDR_BINARY
            if (hdr_buf.magic_number  != htonl(0xbabeface)) {
       	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - invalid magic number 0x%08x\n",
                               __FUNCTION__,
                               hdr_buf.magic_number);
				close(g_rpc_ns);
                break;
            }
#else
            if ( MEA_OS_memcmp(hdr_buf.magic_number, MEA_MAGIC_NUMBER_VALUE,8) != 0 ) {
       	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "error %s - invalid magic number %8s\n",
                               __FUNCTION__,
                               hdr_buf.magic_number);
				close(g_rpc_ns);
                break;
            }
#endif


#ifdef ENET_RPC_HDR_BINARY		 
            type=hdr_buf.type; 
#else
            type=hdr_buf.type - '0';             
#endif




#ifdef ENET_RPC_HDR_BINARY
            len = ntohl(hdr_buf.len);
#else
            len = (((hdr_buf.len[0] - '0') * 100) +
                 ((hdr_buf.len[1] - '0') *  10) +
                 ((hdr_buf.len[2] - '0') *   1) );
#if 0
            printf ("hdr_buf_len = '%c' '%c' '%c'  len=%d \n",
                  hdr_buf.len[0],
                  hdr_buf.len[1],
                  hdr_buf.len[2],
                  len);
#endif

#endif

            if (len > sizeof (buff)-1) {
       	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - len too much big 0x%08x %d\n",
                                __FUNCTION__,
                               len,len);
				close(g_rpc_ns);
              break;
            }

            if (len == 0) {
                break;
            }
          
          
			if (read (g_rpc_ns,(char*)&buff,len) != len) {
    		      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "%s - read cmd failed (errno=%d '%s')\n",
                                    __FUNCTION__,
                                    errno,
                                    strerror(errno));
				  close(g_rpc_ns);
                  break;
              }
          
         
          
            switch (type) { 
                case 0:
                buff[len] = '\0';
                
                
                break;
                case 1:
                /* add the var parameters */
#ifdef ARCH64
                n = sscanf(buff,"%ld %ld %ld %ld %ld %ld %ld %ld %ld %ld",
#else                    
                n = sscanf(buff,"%d %d %d %d %d %d %d %d %d %d",
#endif				
                        &Temp_RPC_val[0],
                        &Temp_RPC_val[1],
                        &Temp_RPC_val[2],
                        &Temp_RPC_val[3],
                        &Temp_RPC_val[4],
                        &Temp_RPC_val[5],
                        &Temp_RPC_val[6],
                        &Temp_RPC_val[7],
                        &Temp_RPC_val[8],
                        &Temp_RPC_val[9]);
                
                
                break;
                case 2:
	                buff[len+1]='\0';
                    ENET_thread_rpc_cli_active = ENET_TRUE;
					ENET_thread_rpc_cli_fd = g_rpc_ns;
                    CLI_execCmd(buff,&add_hist);
                    ENET_thread_rpc_cli_print(NULL); /* end message */
                    ENET_thread_rpc_cli_active = ENET_FALSE;
                    ENET_thread_rpc_cli_fd = -1;
                    MEA_OS_memset(buff,'\0',BUFFER_SIZE_RPC);
                    break;
                case 3:     
                    printf("Type 3\n");
                    buff[len] = '\0';
                    for (i=0;i<len;i++) {
                        printf("buff[%d] = '%c' %3d\n",i,buff[i],buff[i]);
                    }
                    {
                      ENET_Uint32 arg1,arg2,arg3,arg4, arg5;
                      ENET_Uint32 retval;
#ifdef ARCH64
                      n = sscanf(buff,"%ld %ld %lx %lx %ld ",
#else                   
                      n = sscanf(buff,"%d %d %x %x %d ",
#endif					  
                                        &arg1, /*read write*/
                                        &arg2, /*Module*/
                                        &arg3, /*register table*/
                                        &arg4,  /*write val1*/
                                        &arg5); /*val2*/

#ifdef ARCH64
                      printf ("n = %d\n",n);
                      printf ("arg1=%ld\n",arg1);
                      printf ("arg2=%ld\n",arg2);
                      printf ("arg3=0x%08lx\n",arg3);
                      printf ("arg4=0x%08lx\n",arg4);

                      


#else
                      printf ("n = %d\n",n);
                      printf ("arg1=%d\n",arg1);
                      printf ("arg2=%d\n",arg2);
                      printf ("arg3=0x%08x\n",arg3);
                      printf ("arg4=0x%08x\n",arg4);
#endif                      
                      switch (arg1) 
                      {
                        case 0:
#ifdef ARCH64                           
                            printf("ENET_ReadReg(ENET_UNIT_0,%lx,%ld)\n",arg3,arg2);
#else							
							printf("ENET_ReadReg(ENET_UNIT_0,%x,%d)\n",arg3,arg2);
#endif                            
							
							ENET_Lock();
                            retval = ENET_ReadReg(ENET_UNIT_0,arg3,arg2);
                            ENET_Unlock();
#ifdef ARCH64
                            printf("retval %lx\n",retval);
#else                                
                            printf("retval %x\n",retval);
#endif                           
                            hdr_buf_ret.retOk = '1';
                            hdr_buf_ret.pad = ' ';
#ifdef ARCH64
                            sprintf(hdr_buf_ret.magic_number, "%lx",retval);
#else    						
                            sprintf(hdr_buf_ret.magic_number, "%x",retval);
#endif                            
							if (write (g_rpc_ns,(char*)&hdr_buf_ret,sizeof(hdr_buf_ret)) != sizeof (hdr_buf_ret)) {
 		                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "\n%s - write to socket ack failed (errno=%d '%s')\n",
                                       __FUNCTION__,
                                       errno,
                                       strerror(errno));
								close(g_rpc_ns);
                            }
                         MEA_OS_memset(buff,'\0',BUFFER_SIZE_RPC);    
                        break;
                        case 1:
#ifdef ARCH64
                        printf("ENET_WriteReg(ENET_UNIT_0,0x%08lx,0x%08lx,%ld)",arg3,arg4,arg2);
#else    						
                        printf("ENET_WriteReg(ENET_UNIT_0,0x%08x,0x%08x,%d)",arg3,arg4,arg2);
#endif						
                        ENET_Lock();
                        ENET_WriteReg(ENET_UNIT_0,arg3,arg4,arg2);
                        ENET_Unlock();
                        hdr_buf_ret.retOk = '1';  /*OK*/
                        hdr_buf_ret.pad = ' ';
#ifdef ARCH64
                        sprintf(hdr_buf_ret.magic_number, "%ld",arg4);
#else    
                        sprintf(hdr_buf_ret.magic_number, "%d",arg4);
#endif                        
						if (write (g_rpc_ns,(char*)&hdr_buf_ret,sizeof(hdr_buf_ret)) != sizeof (hdr_buf_ret)) {
 		                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "\n%s - write to socket ack failed (errno=%d '%s')\n",
                                       __FUNCTION__,
                                       errno,
                                       strerror(errno));
								close(g_rpc_ns);
                            }
                        
                         MEA_OS_memset(buff,'\0',BUFFER_SIZE_RPC);
                        break;
                        
                       } /*end switch*/                   
                    
                    }
       
            
                break;
                default:
#ifdef ENET_RPC_HDR_BINARY
       	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - invalid type 0x%08x\n",
                    __FUNCTION__ , hdr_buf.type);
#else
       	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - invalid type '%c'\n",
                    __FUNCTION__,  hdr_buf.type);
#endif
				close(g_rpc_ns);
                break;
        
        
        
        
            } /*end switch*/
            
           }
		/* 2010.04.27 : close opened MEA_cli_client socket */
		   close (g_rpc_ns);
    } /*end while ( 1)*/



    return NULL;
}
#endif


#endif

MEA_Status MEA_API_Init_Device_Start(void)
{
	ethernity_init();
	return MEA_OK;
}

MEA_Status MEA_API_DeInit_Device_End(void)
{
   
#if  defined(MEA_OS_LINUX) && defined(MEA_ETHERNITY)
	deleteTimer(ITIMER_REAL);
#endif
   
   
   
    ENET_Lock();
    if(!b_bist_test){
        MEA_OS_DeviceMemoryDebugRegFlag = MEA_FALSE;
        ENET_Collect_all_counters_enable = MEA_FALSE;
        mea_drv_ReInit_RxDisable(MEA_UNIT_0);
    MEA_API_Delete_all_entities(MEA_UNIT_0);
    MEA_API_Conclude(MEA_UNIT_0);
    }
    ENET_Unlock();




#if !defined(ENET_OS_DEVICE_MEMORY_SIMULATION)
    MEA_OS_printf("close(g_rpc_ns)\n");
    close(g_rpc_ns);
#endif

#if defined(ENET_OS_DEVICE_MEMORY_SIMULATION)
    MEA_OS_printf("Hit any key to continue for Exit \n");
    getchar();
#endif




#ifdef MEA_OS_LINUX
    //deleteTimer(ITIMER_REAL);
    if (MEA_drv_common_destroy_mutexs() != MEA_OK)
    {

    }
#endif




    return MEA_OK;
}

#ifndef __KERNEL__
MEA_Status MEA_OS_CpuIfcInit()
{
#if defined(MEA_ETHERNITY) && !defined(ENET_OS_DEVICE_MEMORY_SIMULATION)  
 
 #if defined(MEA_OS_ETH_PROJ_0)
 MEA_Uint32   physical_mem_addr = CFG_IMMR;
 MEA_Uint32   num_of_bytes = IMRR_SIZE;
 MEA_Uint32   mem_addr;
 MEA_Int32    mem_id;
 MEA_Uint32   tmp;


	 if (MEA_OS_DeviceMemoryMap  (physical_mem_addr,
								  num_of_bytes,
								  &mem_addr,
								  &mem_id) == MEA_ERROR)  {
	     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						 "%s - MEA_OS_DeviceMemoryMap fail\n",
						 __FUNCTION__);
	     return MEA_ERROR;
	}


	/*IF CS */
		
	tmp = *(MEA_Uint32 *)(mem_addr+ OR6_OFFSET)   ;
	tmp=tmp&(~OR_MSK);
	tmp=tmp|(OR_VAL);
	*(MEA_Uint32 *)(mem_addr+ OR6_OFFSET) = tmp;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"IF OR = 0x%08x \n",tmp);

	/* BM CS */
	tmp = *(MEA_Uint32 *)(mem_addr+ OR7_OFFSET);
	tmp=tmp&(~OR_MSK);
	tmp=tmp|(OR_VAL);
	*(MEA_Uint32 *)(mem_addr+ OR7_OFFSET) = tmp;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"BM OR = 0x%08x \n",tmp);


	if (MEA_OS_DeviceMemoryUnMap (mem_addr,
								  num_of_bytes,
								  mem_id) == MEA_ERROR)  {
	      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						 "%s - MEA_OS_DeviceMemoryUnMap fail\n",
						 __FUNCTION__);
	      return MEA_ERROR;
	}
#endif

#endif
	return MEA_OK;
}
#endif


#ifdef __KERNEL__



#include <linux/config.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/timer.h>
#include <linux/spinlock.h>
//#include <occam/linux/kap.h>
#include <linux/proc_fs.h>
#include <linux/list.h>
//#include <occam/linux/occam_debug.h>
//#include <asm/io.h>

//#include <occam/linux/kfifo.h>
//#include <occam/linux/proc_fifo.h>



#define ENET_IM_NAME "ENET"
  
int __init ethernity_mod_init(void)
{

    /* register the module functions */
    inter_module_register(ENET_IM_NAME ,THIS_MODULE, NULL);

    MEA_OS_pf_init();

    ethernity_init();

    return 0; /* succeed */

}

void __exit ethernity_mod_exit (void)
{

    ENET_Lock();
    ENET_Conclude(ENET_UNIT_0);
    
	ENET_Unlock();

    MEA_OS_pf_conclude();
    mea_sfp_close_connection();

    inter_module_unregister(ENET_IM_NAME);

}  

module_init (ethernity_mod_init);
module_exit (ethernity_mod_exit);


#endif /* __KERNEL__ */




