/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


/*

   Example for the next test description:

      */



/*---------------------------------------------------------------------------*/
/*        Includes                                                           */
/*---------------------------------------------------------------------------*/
#include "mea_drv_common.h"
#include "mea_api.h"
#include "enet_queue_drv.h"





/*---------------------------------------------------------------------------*/
/*        Defines                                                            */
/*---------------------------------------------------------------------------*/


#define MEA_EXAMPLE4_MAX_NUMBER_OF_USER_PORTS    (4) // 48 72 0 12 
#define MEA_EXAMPLE4_MAX_NUMBER_OF_NETWORK_PORTS (2) // 125
#define MEA_EXAMPLE4_MAX_NUMBER_OF_CDN_PORTS     (2) // 126



        
/*---------------------------------------------------------------------------*/
/*        Typedefs                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*        Globals                                                            */
/*---------------------------------------------------------------------------*/

static MEA_Bool   MEA_Example4_Cluster_mod = MEA_FALSE; // 0 STRICT 1 WFQ
static MEA_Bool   MEA_Example4_PRIQueue_mod = MEA_FALSE; // 0 STRICT 1 WFQ

static MEA_Port_t MEA_Example4_user_ports[MEA_EXAMPLE4_MAX_NUMBER_OF_USER_PORTS];
static MEA_Port_t MEA_Example4_network_ports[MEA_EXAMPLE4_MAX_NUMBER_OF_NETWORK_PORTS];
//static MEA_Port_t MEA_Example4_cdn_ports[MEA_EXAMPLE4_MAX_NUMBER_OF_CDN_PORTS];

static MEA_Uint32 MEA_Example4_num_of_user_ports;
static MEA_Uint32 MEA_Example4_num_of_network_ports;
//static MEA_Uint32 MEA_Example4_num_of_cdn_ports;




/*---------------------------------------------------------------------------*/
/*        Implementation                                                      */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_init_globals>                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_init_globals() {

    MEA_Uint32 i;
    MEA_subSysType_te subSysType;
 
    MEA_Globals_Entry_dbt entry;

 
    subSysType=MEA_subSysType;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                             "MEA_subSysType = %d \n",
                             subSysType);

    /* Init to undefined values */
   
    MEA_Example4_num_of_user_ports = 0;
    for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_Example4_user_ports);i++) {
        MEA_Example4_user_ports[i] = MEA_PLAT_GENERATE_NEW_ID;
    }

    MEA_Example4_num_of_user_ports = 2;
    MEA_Example4_user_ports[0]     = 48;
    MEA_Example4_user_ports[1]     = 24;
    


    /* Init globals */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_Example4_network_ports);i++) {
        MEA_Example4_network_ports[i] = MEA_PLAT_GENERATE_NEW_ID;
    }
    MEA_Example4_num_of_network_ports =2;
    MEA_Example4_network_ports[0]      = 125;
    MEA_Example4_network_ports[1]      = 126;

   

    

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Get_Globals_Entry failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

 
    entry.bm_config.val.policer_enable         = MEA_TRUE;
    entry.bm_config.val.shaper_cluster_enable  = MEA_TRUE;
    entry.bm_config.val.shaper_port_enable     = MEA_TRUE;
    entry.bm_config.val.shaper_priQueue_enable = MEA_FALSE;

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Get_Globals_Entry failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }


    
   


    /* return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_delete_all_services>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_delete_all_services() {

   MEA_Service_t db_serviceId;
   MEA_Service_t temp_db_serviceId;
   MEA_ULong_t    db_cookie;

   /* Get first service */
   if (MEA_API_GetFirst_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK)  {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_GetFirst_Service failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* loop until no more services */
   while (db_cookie != 0) { 


       /* Save the current db_serviceId */
       temp_db_serviceId = db_serviceId;

       /* Make next before delete the object */ 
       if (MEA_API_GetNext_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetNext_Service failed (db_serviceId=%d)\n",
                             __FUNCTION__,db_serviceId);
           return MEA_ERROR;
       }

       /* Delete the save db_serviceId */
       if (MEA_API_Delete_Service(MEA_UNIT_0,temp_db_serviceId)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete service %d\n",
                             __FUNCTION__,temp_db_serviceId);
           return MEA_ERROR;
       }
        
   }
 

   /* Return to caller */
   return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_filters>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_delete_all_filters() {

   MEA_Filter_t      filter_id;
   MEA_Filter_t      temp_filter_id;
   MEA_Bool          found;


   /* Get the first filter */
   if (MEA_API_GetFirst_Filter(MEA_UNIT_0,&filter_id, &found) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - failed to GetFirst filter \n",
                          __FUNCTION__);
        return MEA_ERROR;
   }
 
   /* loop until no more filters */
   while (found) {

       /* Save the filter id that need to be delete , before the getNext */
       temp_filter_id = filter_id;
       
            
       /* Get the next filter */
       if (MEA_API_GetNext_Filter(MEA_UNIT_0,
                                   &filter_id,
                                   &found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to GetNext filter %d\n",
                             __FUNCTION__,filter_id);
            return MEA_ERROR;
       }

       /* Delete the save filter */
       if (MEA_API_Delete_Filter(MEA_UNIT_0,temp_filter_id)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete filter %d\n",
                             __FUNCTION__,temp_filter_id);
           return MEA_ERROR;
       }

   }

    
   /* Return to caller */
   return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_forwrder_entries>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_delete_all_forwarder_entries() {

   if (MEA_API_DeleteAll_SE(MEA_UNIT_0) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_DeleteAll_DSE failed \n",
                         __FUNCTION__);
      return MEA_ERROR;
   }

   /* Return to caller */
   return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_delete_all_actions>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
 MEA_Status MEA_Example4_delete_all_actions() {
 
       
   return MEA_API_DeleteAll_Action(MEA_UNIT_0);

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_delete_all_limiters>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_delete_all_limiters() {

   MEA_Limiter_t      db_limiterId;
   MEA_Limiter_t      temp_db_limiterId;
   MEA_Bool           found;

   /* Get the first limiter */
   if (MEA_API_GetFirst_Limiter(MEA_UNIT_0,&db_limiterId,&found) != MEA_OK)  {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_GetFirst_Limiter failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
   }

   /* loop until no more limiter */
   while (found) { 


       /* Save the current db_limiterId */
       temp_db_limiterId = db_limiterId;

       /* Make next before delete the object */
       if (MEA_API_GetNext_Limiter(MEA_UNIT_0,&db_limiterId,&found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetNext_Limiter failed (db_limiterId=%d)\n",
                             __FUNCTION__,db_limiterId);
           return MEA_ERROR;
       }

       /* Ignore the special limiter */
       if (temp_db_limiterId == MEA_LIMITER_DONT_CARE) {
            continue;
       }
       
       /* Delete the save db_limiterId */
       if (MEA_API_Delete_Limiter(MEA_UNIT_0,temp_db_limiterId)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete limiter %d\n",
                             __FUNCTION__,temp_db_limiterId);
           return MEA_ERROR;
       }
   }

   /* Return to caller */
   return MEA_OK;

}









/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_LxCPs>                                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_delete_all_LxCPs() {

   MEA_LxCp_t id;
   MEA_LxCp_t temp_id;
   MEA_Bool   found;


   /* Get the first filter */
   if (MEA_API_GetFirst_LxCP(MEA_UNIT_0,&id, NULL,&found) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - failed to GetFirst action \n",
                          __FUNCTION__);
        return MEA_ERROR;
   }
 
   /* loop until no more lxcp profiles */
   while (found) {

       /* Save the id that need to be delete , before the getNext */
       temp_id = id;
       
            
       /* Get the next LxCP Profile */
       if (MEA_API_GetNext_LxCP(MEA_UNIT_0,
                                &id,
                                NULL,
                                &found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to GetNext LxCP %d\n",
                             __FUNCTION__,id);
            return MEA_ERROR;
       }

       /* Delete the save LxCP Profile */
       if (MEA_API_Delete_LxCP(MEA_UNIT_0,temp_id)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete LxCP %d\n",
                             __FUNCTION__,temp_id);
           return MEA_ERROR;
       }

   }

    
   /* Return to caller */
   return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_delete_all_queues>                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_delete_all_queues() {

    ENET_QueueId_t queueId;
    ENET_QueueId_t next_queueId;
    ENET_Bool      found;
    ENET_Queue_dbt entry;

    if (ENET_GetFirst_Queue  (ENET_UNIT_0,
                             &queueId,
                             &entry,
                             &found)!=ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ENET_GetFirst_Queue failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    while (found) {
        next_queueId = queueId;
        if (ENET_GetNext_Queue (ENET_UNIT_0,
                                &next_queueId,
                                &entry,
                                &found)!=ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_GetNext_Queue failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if ((queueId != ENET_PLAT_PORT_127_DEFAULT_CLUSTER   )) {
            if (ENET_Delete_Queue (ENET_UNIT_0,
                                   queueId)!=ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - ENET_Delete_Queue failed \n",
                                  __FUNCTION__);
                return MEA_ERROR;
            }
        }
        queueId = next_queueId;
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example3_restore_globals>                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_restore_globals() {

    MEA_Globals_Entry_dbt entry;

    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_Globals_Entry failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    entry.if_global1.val.forwarder_enable = 1;
    entry.if_global1.val.Lxcp_Enable = 1;
    entry.bm_config.val.policer_enable = 1;
   
     entry.bm_config.val.shaper_priQueue_enable   =  1;
     entry.bm_config.val.shaper_cluster_enable    =  1;
     entry.bm_config.val.shaper_port_enable       =  1;
   

    entry.bm_config.val.Cluster_mode = MEA_GLOBAL_CLUSTER_MODE_DEF_VAL;
    entry.bm_config.val.pri_q_mode   = MEA_GLOBAL_PRI_Q_MODE_DEF_VAL;
    entry.bm_config.val.bm_config_bmqs = 1;
    entry.bm_config.val.bm_config_mqs = 0;

    if (MEA_API_Set_Globals_Entry(MEA_UNIT_0,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_Globals_Entry failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_delete_all_entities>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_delete_all_entities() {


    /* Delete all services */
    if (MEA_Example4_delete_all_services() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_delete_all_services failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Delete all filters */
    if (MEA_Example4_delete_all_filters() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_delete_all_filters failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

     /* Delete all forwarder entries */
    if (MEA_Example4_delete_all_forwarder_entries() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_delete_all_forwarder_entries failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Delete all actions */
    if (MEA_Example4_delete_all_actions() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_delete_all_actions failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Delete all limiters */
    if (MEA_Example4_delete_all_limiters() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_delete_all_limiters failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }


    /* Delete all LxCP */
    if (MEA_Example4_delete_all_LxCPs() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_delete_all_LxCPs failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Delete all Queues */
    if (MEA_Example4_delete_all_queues() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_delete_all_queues failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Restore globals */
    if (MEA_Example4_restore_globals() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_restore_globals failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Return to caller */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_create_queues>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_create_queues() {

    MEA_Uint32 port_num;
    //MEA_Uint32 i;
    ENET_Queue_dbt entry;
    ENET_QueueId_t id;
    MEA_Uint32     priQ;
    
    MEA_EirCir_t     shaper_vec_pri[8]; 
    MEA_Uint32     wfq_vec_pri[8];     
    

    shaper_vec_pri[0]= 100000000;
    shaper_vec_pri[1]= 100000000;
    shaper_vec_pri[2]= 100000000;
    shaper_vec_pri[3]= 100000000;
    shaper_vec_pri[4]= 100000000;
    shaper_vec_pri[5]= 100000000;
    shaper_vec_pri[6]= 100000000;
    shaper_vec_pri[7]= 100000000;

    wfq_vec_pri[0] = 16;
    wfq_vec_pri[1] = 16;
    wfq_vec_pri[2] = 16;
    wfq_vec_pri[3] = 16;
    wfq_vec_pri[4] = 16;
    wfq_vec_pri[5] = 16;
    wfq_vec_pri[6] = 16;
    wfq_vec_pri[7] = 16;

    
    /* Create cluster queue for user port*/
    for (port_num=0;port_num<MEA_Example4_num_of_user_ports;port_num++) {
            /* Init the entry */
            MEA_OS_memset(&entry,0,sizeof(entry));
            MEA_OS_strcpy(entry.name,ENET_PLAT_GENERATE_NEW_NAME);
            entry.port.type    = ENET_QUEUE_PORT_TYPE_PORT;
            
            entry.port.id.port = (ENET_PortId_t)(MEA_Example4_user_ports[port_num]);
            if(MEA_Example4_Cluster_mod == MEA_FALSE){ //strict mode
            
                    entry.mode.type                  = ENET_QUEUE_MODE_TYPE_STRICT;
                    entry.mode.value.strict_priority = 7;
                
            }else{
                entry.mode.type                  = ENET_QUEUE_MODE_TYPE_WFQ;
                entry.mode.value.wfq_weight      = 20;

            }
            
            //entry.MTU = MEA_QUEUE_MTU_MAX_VALUE;
            entry.shaper_enable =MEA_FALSE;
            entry.Shaper_compensation=ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
            entry.shaper_info.CIR = 1000000000;
            entry.shaper_info.CBS = 320000;
            entry.shaper_info.resolution_type  = ENET_SHAPER_RESOLUTION_TYPE_BIT;
            entry.shaper_info.overhead = 0;
            entry.shaper_info.Cell_Overhead = 0;
            for(priQ=0;priQ<MEA_NUM_OF_ELEMENTS(entry.pri_queues);priQ++) {
                 
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL(priQ);
                entry.pri_queues[priQ].max_q_size_Byte    = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL(priQ);
                if(MEA_Example4_PRIQueue_mod == MEA_TRUE){                
                    entry.pri_queues[priQ].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                    entry.pri_queues[priQ].mode.value.wfq_weight=(ENET_Uint8)wfq_vec_pri[priQ];
                } else {
                entry.pri_queues[priQ].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                entry.pri_queues[priQ].mode.value.strict_priority= 0;//(ENET_Uint8)priQ;
                }
                entry.pri_queues[priQ].shaperPri_enable =MEA_FALSE;
                entry.pri_queues[priQ].shaper_info.CIR = shaper_vec_pri[priQ];
                entry.pri_queues[priQ].shaper_info.CBS = 32000;
                entry.pri_queues[priQ].shaper_info.Cell_Overhead = 0;
                entry.pri_queues[priQ].shaper_info.overhead = 0; 
                entry.pri_queues[priQ].shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
                entry.pri_queues[priQ].Shaper_compensation         = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
                
            }
            /* create the queue */
            id = MEA_Example4_user_ports[port_num];
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," port %d cluster id=%d\n",MEA_Example4_user_ports[port_num],id);

            if (ENET_Create_Queue (ENET_UNIT_0,
                                   &entry,
                                   &id) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - ENET_Create_Queue failed (id=%d) port %d\n",
                                  __FUNCTION__,id,MEA_Example4_user_ports[port_num]);
              //  return MEA_ERROR; 
            }
        
    }



    /* Create cluster queue for network port*/
    for (port_num=0;port_num<MEA_Example4_num_of_network_ports;port_num++) {
        /* Init the entry */
        MEA_OS_memset(&entry,0,sizeof(entry));
        MEA_OS_strcpy(entry.name,ENET_PLAT_GENERATE_NEW_NAME);
        entry.port.type    = ENET_QUEUE_PORT_TYPE_PORT;

        entry.port.id.port = (ENET_PortId_t)(MEA_Example4_network_ports[port_num]);
        if(MEA_Example4_Cluster_mod == MEA_FALSE){ //strict mode

            entry.mode.type                  = ENET_QUEUE_MODE_TYPE_STRICT;
            entry.mode.value.strict_priority = 7;

        }else{
            entry.mode.type                  = ENET_QUEUE_MODE_TYPE_WFQ;
            entry.mode.value.wfq_weight      = 20;

        }

        //entry.MTU = MEA_QUEUE_MTU_MAX_VALUE;
        entry.shaper_enable =MEA_FALSE;
        entry.Shaper_compensation=ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
        entry.shaper_info.CIR = 1000000000;
        entry.shaper_info.CBS = 320000;
        entry.shaper_info.resolution_type  = ENET_SHAPER_RESOLUTION_TYPE_BIT;
        entry.shaper_info.overhead = 0;
        entry.shaper_info.Cell_Overhead = 0;
        for(priQ=0;priQ<MEA_NUM_OF_ELEMENTS(entry.pri_queues);priQ++) {

            entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
            entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL(priQ);
            entry.pri_queues[priQ].max_q_size_Byte    = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL(priQ);
            if(MEA_Example4_PRIQueue_mod == MEA_TRUE){                
                entry.pri_queues[priQ].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                entry.pri_queues[priQ].mode.value.wfq_weight=(ENET_Uint8)wfq_vec_pri[priQ];
            } else {
                entry.pri_queues[priQ].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                entry.pri_queues[priQ].mode.value.strict_priority= 0;//(ENET_Uint8)priQ;
            }
            entry.pri_queues[priQ].shaperPri_enable =MEA_FALSE;
            entry.pri_queues[priQ].shaper_info.CIR = shaper_vec_pri[priQ];
            entry.pri_queues[priQ].shaper_info.CBS = 32000;
            entry.pri_queues[priQ].shaper_info.Cell_Overhead = 0;
            entry.pri_queues[priQ].shaper_info.overhead = 0; 
            entry.pri_queues[priQ].shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
            entry.pri_queues[priQ].Shaper_compensation         = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

        }
        /* create the queue */
        id = MEA_Example4_network_ports[port_num];
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," port %d cluster id=%d\n",MEA_Example4_network_ports[port_num],id);

        if (ENET_Create_Queue (ENET_UNIT_0,
            &entry,
            &id) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_Create_Queue failed (id=%d) port %d\n",
                    __FUNCTION__,id,MEA_Example4_network_ports[port_num]);
                //  return MEA_ERROR; 
        }

    }






    /* Return to caller */
    return MEA_OK;

}

MEA_Status MEA_Example4_configIngressAdminON()
{
    
    MEA_Port_t port;
    MEA_IngressPort_Entry_dbt entry;

        
    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++){
    
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }


        if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Get_IngressPort_Entry failed (id=%d)\n",
                      __FUNCTION__,port);
            return MEA_ERROR;
        }
        entry.rx_enable=MEA_TRUE;
        if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                  port,
                                  &entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Set_IngressPort_Entry failed (port=%d)\n",
                      __FUNCTION__,
                      port);
        return MEA_ERROR;
        }

    }

    
    return MEA_OK;
}

MEA_Status MEA_Example4_configIngressAdminOFF()
{

    MEA_Port_t port;
    MEA_IngressPort_Entry_dbt entry;

        
    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++){
    
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }


        if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Get_IngressPort_Entry failed (id=%d)\n",
                      __FUNCTION__,port);
            return MEA_ERROR;
        }
        entry.rx_enable=MEA_FALSE;
        if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                  port,
                                  &entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Set_IngressPort_Entry failed (port=%d)\n",
                      __FUNCTION__,
                      port);
        return MEA_ERROR;
        }

    }

    
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_config_Shaper_port>                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_config_Shaper_port(MEA_Port_t  port, MEA_Bool enable, MEA_EirCir_t CIR)
{

    MEA_EgressPort_Entry_dbt  egressPort_entry; 

   
    if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }


    egressPort_entry.shaper_enable   = enable;
    egressPort_entry.shaper_info.CIR =CIR;
    egressPort_entry.shaper_info.CBS = 32000;
    egressPort_entry.shaper_info.Cell_Overhead = 0;
    egressPort_entry.shaper_info.overhead = 0;
    
    egressPort_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
    egressPort_entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_config_port>                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_config_port(MEA_Port_t              port,
                                    MEA_IngressPort_Proto_t ingressProtocol,
                                    MEA_Uint16  etherType) 
{

    MEA_IngressPort_Entry_dbt ingressPort_entry; 
    MEA_EgressPort_Entry_dbt  egressPort_entry; 
    MEA_Parser_Entry_dbt      parser_entry;
    

    if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
                                      port,
                                      &ingressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_IngressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }

    ingressPort_entry.parser_info.port_proto_prof = ingressProtocol;
    ingressPort_entry.parser_info.port_cos_aw = 0;
    ingressPort_entry.parser_info.port_col_aw = 0;
    ingressPort_entry.parser_info.port_ip_aw = 0;
    ingressPort_entry.parser_info.sp_wildcard_valid  = 0;
    ingressPort_entry.parser_info.sp_wildcard        = 0;
    ingressPort_entry.parser_info.net_wildcard_valid = 0;
    ingressPort_entry.parser_info.net_wildcard       = 0;
    if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
        port,
        &ingressPort_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Set_IngressPort_Entry failed (port=%d)\n",
                __FUNCTION__,
                port);
            return MEA_ERROR;
    }
    if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
        port,
        &ingressPort_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Get_IngressPort_Entry failed (port=%d)\n",
                __FUNCTION__,
                port);
            return MEA_ERROR;
    }
   
    
    
        if (MEA_API_Get_Parser_Entry(MEA_UNIT_0,
                                     port,
                                     &parser_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_Parser_Entry failed (port=%d)\n",
                              __FUNCTION__,
                              port);
            return MEA_ERROR;
        }
        /* Define the classifier priority as the c-vid */
#if 0
        parser_entry.f.net_ether_type_offset  = 12;
        parser_entry.f.net_ether_type_valid = MEA_TRUE;
        parser_entry.f.net_ether_type_value_0_7       = (etherType & 0xff);
        parser_entry.f.net_ether_type_value_8_15      = (((etherType & 0xff00)>>8)& 0xff);
        parser_entry.f.net_ether_type_unmatch_discard   = MEA_TRUE;
#endif
        
        /* Define the network priority to be always zero by take the lsb of the etherType
          This done to force classifier priority 0 in case of no c-vid */
        

        /* Define the priority rule to take first the vlan (c-vid) 
           and then the network (0) */
        parser_entry.f.pri_rule = 0;

        if (MEA_API_Set_Parser_Entry(MEA_UNIT_0,
                                     port,
                                     &parser_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_Parser_Entry failed (port=%d)\n",
                              __FUNCTION__,
                              port);
            return MEA_ERROR;
        }
    
    ingressPort_entry.userParser = MEA_TRUE;
    if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
        port,
        &ingressPort_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Set_IngressPort_Entry failed (port=%d)\n",
                __FUNCTION__,
                port);
            return MEA_ERROR;
    }
    if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }

    if (ingressProtocol == MEA_INGRESS_PORT_PROTO_QTAG_OUTER) {
        egressPort_entry.proto = MEA_EGRESS_PORT_PROTO_QTAG;
    } else {
        egressPort_entry.proto = (MEA_EgressPort_Proto_t)ingressProtocol;
    }
    
    egressPort_entry.calc_crc = 1;

    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }


    /* Return to caller */
    return MEA_OK;

}



MEA_Status MEA_Example4_configEgressAdminON()
{
    
    MEA_Port_t port;
    MEA_EgressPort_Entry_dbt entry;

        
    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++){
    
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }
        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
            return MEA_ERROR;
        }
        entry.tx_enable=MEA_TRUE;
       if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
        }

    }

    
    return MEA_OK;
}

MEA_Status MEA_Example4_configEgressAdminOFF()
{

MEA_Port_t port;
    MEA_EgressPort_Entry_dbt entry;

        
    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++){
    
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }


       if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
        }
        entry.tx_enable=MEA_FALSE;
        if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
        }

    }
    
    return MEA_OK;
}













/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_define_upstream_services>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_define_upstream_services(MEA_Port_t port_in ,MEA_Port_t port_out)
{

    MEA_LxCp_t                           LxCP_id;
    MEA_LxCP_Protocol_key_dbt            LxCP_Protocol_key;
    MEA_LxCP_Protocol_data_dbt           LxCP_Protocol_data;
    
   
   
  
   
    MEA_Action_Entry_Data_dbt            action_data; 
    MEA_Policer_Entry_dbt                action_policer;
   	MEA_EHP_Info_dbt                     action_editing_info[2];
    MEA_EgressHeaderProc_Array_Entry_dbt action_editing;
    MEA_Action_t                         action_id;
    MEA_OutPorts_Entry_dbt               action_outPorts;
    MEA_Service_Entry_Key_dbt            service_key;
    MEA_Service_Entry_Data_dbt           service_data;
    MEA_OutPorts_Entry_dbt               service_outports;
    MEA_Policer_Entry_dbt                service_policer;
   	MEA_EHP_Info_dbt                     service_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt service_editing;
    MEA_Service_t                        service_id;
    MEA_LxCP_Entry_dbt                   lxcp_entry;


   
    /************************************************************************/
    /*   create action lxcp service      from port  user 48 ----->125        */
    /************************************************************************/
    /************************************************************************/
    /*   create Action for LXCP                                             */
    /************************************************************************/
    MEA_OS_memset(&action_data,0,sizeof(action_data));
    MEA_OS_memset(&action_outPorts,0,sizeof(action_outPorts));
    action_data.ed_id_valid        = MEA_TRUE;
    action_data.ed_id              = 0;
    action_data.tm_id_valid        = MEA_TRUE;
    action_data.tm_id              = (MEA_TmId_t)(0);
    action_data.pm_id_valid        = MEA_TRUE;

    action_data.pm_id = (MEA_PmId_t) (127);
   
    action_data.protocol_llc_force = MEA_TRUE;
    action_data.proto_llc_valid    = MEA_TRUE;
    action_data.Protocol           = 1;
    action_data.Llc                = 0;
    action_data.force_cos_valid    = MEA_ACTION_FORCE_COMMAND_VALUE;
    action_data.COS                = (7); /* Define the priority queue */
    action_data.tmId_disable =MEA_TRUE;

    MEA_OS_memset(&action_policer,0,sizeof(action_policer));
    action_policer.CIR = 10000000;
    action_policer.CBS = 32000;
    MEA_OS_memset(&action_editing,0,sizeof(action_editing));
    MEA_OS_memset(&action_editing_info,0,sizeof(action_editing_info));
    action_editing.num_of_entries = 2;
    action_editing.ehp_info       = &(action_editing_info[0]);
    MEA_SET_OUTPORT(&action_editing.ehp_info[0].output_info,port_out);//125

    action_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x00000000 ;
    action_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
    action_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0x00000000 ;
    action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    action_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
    action_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;
               
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 0;//12;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = 0;//MEA_TRUE;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 0;//13;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = 0;//MEA_TRUE;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 0;//14;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = 0;//MEA_TRUE;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 0;//15;
    action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = 0;//MEA_TRUE;
    
    MEA_SET_OUTPORT(&action_editing.ehp_info[1].output_info,127);//cpu
    action_editing.ehp_info[1].ehp_data.eth_info.val.all                   = 0x81000000 | MEA_Example4_user_ports[0];
    action_editing.ehp_info[1].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_APPEND;  //MEA_EGRESS_HEADER_PROC_CMD_TRANS
    action_editing.ehp_info[1].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    action_editing.ehp_info[1].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
    action_editing.ehp_info[1].ehp_data.atm_info.val.all                   = 0x00000000 ;
    action_editing.ehp_info[1].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    action_editing.ehp_info[1].ehp_data.atm_info.stamp_color               = MEA_FALSE;
    action_editing.ehp_info[1].ehp_data.atm_info.stamp_priority            = MEA_FALSE;

    action_editing.ehp_info[1].ehp_data.eth_stamping_info.color_bit0_loc   = 0;//12;
    action_editing.ehp_info[1].ehp_data.eth_stamping_info.color_bit0_valid = 0;//MEA_TRUE;
    action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit0_loc     = 0;//13;
    action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit0_valid   = 0;//MEA_TRUE;
    action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit1_loc     = 0;//14;
    action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit1_valid   = 0;//MEA_TRUE;
    action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit2_loc     = 0;//15;
    action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit2_valid   = 0;//MEA_TRUE;




    action_id = MEA_PLAT_GENERATE_NEW_ID;

    MEA_SET_OUTPORT(&action_outPorts,port_out);
    MEA_SET_OUTPORT(&action_outPorts,127); //cpu

    if (MEA_API_Create_Action(MEA_UNIT_0,
        &action_data,
        &action_outPorts, /* out ports */
        &action_policer, /* policer */
        &action_editing,
        &action_id
        ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Create_Action (1) failed \n",
                __FUNCTION__);
            return MEA_ERROR;
    }

    
    
    
    
    /************************************************************************/
    /* create LXcp                                                      */
    /************************************************************************/
    /* Create LxCP profile to trap IGMP packets , and all other to discard */
    MEA_OS_memset(&lxcp_entry,0,sizeof(lxcp_entry));
    LxCP_id = MEA_PLAT_GENERATE_NEW_ID;
    if (MEA_API_Create_LxCP(MEA_UNIT_0,&lxcp_entry,&LxCP_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_LxCP failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    MEA_OS_memset(&LxCP_Protocol_key,0,sizeof(LxCP_Protocol_key));
    for (LxCP_Protocol_key.protocol=0;
         LxCP_Protocol_key.protocol<MEA_LXCP_PROTOCOL_LAST;
         LxCP_Protocol_key.protocol++) {
        MEA_OS_memset(&LxCP_Protocol_data,0,sizeof(LxCP_Protocol_data));
        LxCP_Protocol_data.action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
        if(!MEA_API_IsSupport_LxCP_Protocol(LxCP_Protocol_key.protocol)) {
             continue;
        }
        if (MEA_API_Set_LxCP_Protocol (MEA_UNIT_0,
                                       LxCP_id,
                                       &LxCP_Protocol_key,
                                       &LxCP_Protocol_data
                                      ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_LxCP_Protocol failed (protocol=%d,action=%d) \n",
                              __FUNCTION__,
                              LxCP_Protocol_key.protocol,
                              LxCP_Protocol_data.action_type);
            return MEA_ERROR;
        }
    }

    MEA_OS_memset(&LxCP_Protocol_key,0,sizeof(LxCP_Protocol_key));
    LxCP_Protocol_key.protocol = MEA_LXCP_PROTOCOL_PPPoE_DISCOVERY;
    MEA_OS_memset(&LxCP_Protocol_data,0,sizeof(LxCP_Protocol_data));
    LxCP_Protocol_data.action_type = MEA_LXCP_PROTOCOL_ACTION_ACTION;
    LxCP_Protocol_data.ActionId_valid = MEA_TRUE;
    LxCP_Protocol_data.ActionId       = action_id;
    LxCP_Protocol_data.OutPorts_valid =MEA_TRUE;
    MEA_SET_OUTPORT(&LxCP_Protocol_data.OutPorts,port_out);
    MEA_SET_OUTPORT(&LxCP_Protocol_data.OutPorts,127); //cpu
   
    if (MEA_API_Set_LxCP_Protocol (MEA_UNIT_0,
                                   LxCP_id,
                                   &LxCP_Protocol_key,
                                   &LxCP_Protocol_data
                                  ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_LxCP_Protocol failed (protocol=%d,action=%d) \n",
                          __FUNCTION__,
                          LxCP_Protocol_key.protocol,
                          LxCP_Protocol_data.action_type);
        return MEA_ERROR;
    }

    MEA_OS_memset(&LxCP_Protocol_key,0,sizeof(LxCP_Protocol_key));
    LxCP_Protocol_key.protocol = MEA_LXCP_PROTOCOL_PPPoE_ICP_LCP;
    MEA_OS_memset(&LxCP_Protocol_data,0,sizeof(LxCP_Protocol_data));
    LxCP_Protocol_data.action_type = MEA_LXCP_PROTOCOL_ACTION_ACTION;
    LxCP_Protocol_data.ActionId_valid = MEA_TRUE;
    LxCP_Protocol_data.ActionId       = action_id;
    LxCP_Protocol_data.OutPorts_valid =MEA_TRUE;
    MEA_SET_OUTPORT(&LxCP_Protocol_data.OutPorts,port_out);
    MEA_SET_OUTPORT(&LxCP_Protocol_data.OutPorts,127); //cpu
   
        if (MEA_API_Set_LxCP_Protocol (MEA_UNIT_0,
            LxCP_id,
            &LxCP_Protocol_key,
            &LxCP_Protocol_data
            ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_Set_LxCP_Protocol failed (protocol=%d,action=%d) \n",
                    __FUNCTION__,
                    LxCP_Protocol_key.protocol,
                    LxCP_Protocol_data.action_type);
                return MEA_ERROR;
        }



/************************************************************************/
/*  create service                                                       */
/************************************************************************/


    /* Define the upstream service 
    that will be use the learning action define above  */
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = port_in  ;
    service_key.net_tag  = 0x1f001;
#if 0
    service_key.net_tag_to_valid = MEA_TRUE;
    service_key.net_tag_to_value = 0x1f000 | 0x00fff;
#endif

    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable                 = MEA_TRUE;
    service_data.DSE_forwarding_key_type               = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
    service_data.DSE_learning_enable                   = MEA_FALSE;
    service_data.DSE_learning_key_type                 = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                                   = 0;
    service_data.limiter_enable                        = MEA_FALSE;
    service_data.limiter_id                            = 0;

    
    service_data.LxCp_enable                           = MEA_TRUE;
    service_data.LxCp_Id                               =  LxCP_id;
    service_data.DSE_learning_actionId_valid           = MEA_FALSE;
    service_data.DSE_learning_actionId                 = 0;
    service_data.DSE_learning_srcPort_force            = MEA_FALSE;

    service_data.DSE_learning_srcPort                  = 0;

    service_data.DSE_learning_update_allow_enable      = MEA_FALSE;
    service_data.DSE_learning_update_allow_send_to_cpu = MEA_FALSE;
    service_data.tmId_disable                          = MEA_FALSE;
    service_data.tmId = 0;
    service_data.pmId = (MEA_PmId_t) (0);

    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_SET_OUTPORT(&service_outports,port_out);

    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 1000000000;
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x00000000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.atm_info.val.all                   = 0x00000000;
    service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd            = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.atm_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority            = MEA_FALSE;
#if 1                
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 0;//12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = 0;//MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 0;//13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = 0;//MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 0;//14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = 0;//MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 0;//15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = 0;//MEA_TRUE;
#endif     
    if (MEA_API_Create_Service (MEA_UNIT_0,
                    &service_key,
                    &service_data,
                    &service_outports,
                    &service_policer,
                    &service_editing,
                    &service_id) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "\n%s - 1001 MEA_API_Create_Service failed (netTag=0x%08x) port %d pri %d\n",
                __FUNCTION__,
                service_key.net_tag,
                service_key.src_port,
                service_key.pri);
            //return MEA_ERROR;
    }
 
    {

    
    

   /************************************************************************/
   /*                                                                      */
   /************************************************************************/
    MEA_IngressPort_Entry_dbt entry_ingress;
    if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
        port_in,
        &entry_ingress) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Get_IngressPort_Entry failed (port=%d)\n",
                __FUNCTION__,
                port_in);
            return MEA_ERROR;
    }
    
    entry_ingress.parser_info.default_sid_info.action = 2;
    entry_ingress.parser_info.default_sid_info.def_sid= service_id;
#if 1
        if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
            port_in,
            &entry_ingress) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_Set_IngressPort_Entry failed (port=%d)\n",
                    __FUNCTION__,
                    port_in);
                return MEA_ERROR;
        }
#endif
    
    }







    /* Return to caller */
    return MEA_OK;

}






MEA_Status MEA_Example4_define_upstream_static_forwarder(MEA_Uint32 L4pAdd    ,MEA_Uint16 L4port  )
{

   MEA_SE_Entry_dbt               entry;


   
   MEA_OS_memset(&entry,0,sizeof(entry));

   //entry.key.vlan_vpn.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
   entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;


    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4   = L4pAdd;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port = L4port;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = 0;

    entry.data.aging = 3;
    entry.data.static_flag     = MEA_TRUE;
    entry.data.sa_discard      = MEA_FALSE;
    
    entry.data.limiterId_valid = MEA_FALSE;
    entry.data.limiterId       = 0;
    entry.data.actionId_valid  = MEA_FALSE;
    entry.data.actionId        = 0;

    MEA_SET_OUTPORT(&entry.data.OutPorts,127);
    
    if (MEA_API_Create_SE_Entry(MEA_UNIT_0,
        &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Create_SE_Entry failed \n",
                __FUNCTION__
                );
            
            return MEA_OK;
    }


    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_define_downstream_services>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#if 0
MEA_Status MEA_Example4_define_downstream_services()
{

    MEA_Service_Entry_Key_dbt            service_key;
    MEA_Service_Entry_Data_dbt           service_data;
    MEA_OutPorts_Entry_dbt               service_outports;
    MEA_Policer_Entry_dbt                service_policer;
   	MEA_EHP_Info_dbt                     service_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt service_editing;
    MEA_Service_t                        service_id;
    MEA_LxCp_t                           LxCP_id;
    MEA_LxCP_Protocol_key_dbt            LxCP_Protocol_key;
    MEA_LxCP_Protocol_data_dbt           LxCP_Protocol_data;
    MEA_LxCP_Entry_dbt                   lxcp_entry;

    /* Create downstream for S-VID 1001 */
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = MEA_Example4_network_ports[0];
    service_key.net_tag  = 0x1f001;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                     = 0;
    service_data.pmId                    = 11;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 4000000;
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;

#endif
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }


    /* Create downstream for S-VID 2001 */
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = MEA_Example3_network_port;
    service_key.net_tag  = 0x1f000 | 2001;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                     = 0;
    service_data.pmId                    = 12;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 2000000;
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1    
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }


    /* Create LxCP profile to trap IGMP packets */
    MEA_OS_memset(&lxcp_entry,0,sizeof(lxcp_entry));
    lxcp_entry.direction = MEA_DIRECTION_DOWNSTREAM;
    LxCP_id = MEA_PLAT_GENERATE_NEW_ID;
    if (MEA_API_Create_LxCP(MEA_UNIT_0,&lxcp_entry,&LxCP_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_LxCP failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    MEA_OS_memset(&LxCP_Protocol_key,0,sizeof(LxCP_Protocol_key));
    LxCP_Protocol_key.protocol = MEA_LXCP_PROTOCOL_IGMP;
    MEA_OS_memset(&LxCP_Protocol_data,0,sizeof(LxCP_Protocol_data));
    LxCP_Protocol_data.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
    if (MEA_API_Set_LxCP_Protocol (MEA_UNIT_0,
                                   LxCP_id,
                                   &LxCP_Protocol_key,
                                   &LxCP_Protocol_data
                                  ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_LxCP_Protocol failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Create downstream for S-VID 4001 */
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = MEA_Example3_network_port;
    service_key.net_tag  = 0x1f000 | 4001;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN; //
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                     = 0;
    service_data.pmId                    = 3;
    service_data.LxCp_enable             = MEA_FALSE;
    service_data.LxCp_Id                 = LxCP_id;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 30000000;
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }
    
/**  Foward IP  3001 **/
    MEA_OS_memset(&service_key         ,0,sizeof(service_key));
    service_key.src_port = MEA_Example3_network_port;
    service_key.net_tag  = 0x1f000 | 3001;
    service_key.pri      = 7;
    MEA_OS_memset(&service_data        ,0,sizeof(service_data));
    service_data.DSE_forwarding_enable   = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
    service_data.DSE_learning_enable     = MEA_FALSE;
    service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn                     = 0;
    service_data.pmId                    = 3;
    service_data.LxCp_enable             = MEA_TRUE;
    service_data.LxCp_Id                 = LxCP_id;
    MEA_OS_memset(&service_outports    ,0,sizeof(service_outports));
    MEA_OS_memset(&service_policer     ,0,sizeof(service_policer));
    service_policer.CIR = 300000000;
    service_policer.CBS = 32000;
    service_policer.comp = 0;
    MEA_OS_memset(&service_editing_info,0,sizeof(service_editing_info));
    MEA_OS_memset(&service_editing     ,0,sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info       = &(service_editing_info[0]);
    service_editing.ehp_info[0].ehp_data.eth_info.val.all                   = 0x81000000;
    service_editing.ehp_info[0].ehp_data.eth_info.command                   = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
#if 1
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
    service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
#endif
    if (MEA_API_Create_Service (MEA_UNIT_0,
                                &service_key,
                                &service_data,
                                &service_outports,
                                &service_policer,
                                &service_editing,
                                &service_id) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Create_Service failed (netTag=0x%08x)\n",
                          __FUNCTION__,service_key.net_tag);
        return MEA_ERROR;
    }




    /* Return to caller */
    return MEA_OK;

}


#endif
#include "cli_eng.h"
#include "mea_cli.h"

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_main>                                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_main() {

    MEA_Uint32 port_num;

    CLI_print("\n Admin OFF:\n");
    MEA_Example4_configIngressAdminOFF();
    MEA_Example4_configEgressAdminOFF();

    /* init globals */
    CLI_print("\nInit globals\n");
    if (MEA_Example4_init_globals() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_init_globals failed \n",
                         __FUNCTION__);
       return MEA_ERROR; 
    } 

    /* Delete all entities */
    CLI_print("\nDelete all entities\n");
    if (MEA_Example4_delete_all_entities() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_delete_all_entities failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* MEA debug cpu show 1 */
    CLI_print("\nEnable print packets receive to CPU \n");
    if (MEA_API_Set_CpuPort_FramesShowFlag(MEA_UNIT_0,1) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_CpuPort_FramesShowFlag failed \n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Create queues */
    CLI_print("\nCreate queues \n");
    if (MEA_Example4_create_queues() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example3_create_queues failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Configure ports */
    CLI_print("\nConfigure user ports \n");
    for (port_num=0;port_num<MEA_Example4_num_of_user_ports;port_num++) {
        if (MEA_Example4_config_port(MEA_Example4_user_ports[port_num],
                                     MEA_INGRESS_PORT_PROTO_QTAG_OUTER,
                                    0x88a8) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_Example3_config_port failed (port=%d)\n",
                             __FUNCTION__,
                             MEA_Example4_user_ports[port_num]);
            return MEA_ERROR; 
        }
    }
    CLI_print("\nConfigure network port \n");
    for (port_num=0;port_num<MEA_Example4_num_of_network_ports;port_num++) {
    if (MEA_Example4_config_port(MEA_Example4_network_ports[port_num],
                                 MEA_INGRESS_PORT_PROTO_QTAG_OUTER,
                                0x88a8) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_config_port failed (port=%d)\n",
                          __FUNCTION__,
                          MEA_Example4_network_ports[port_num]);
        return MEA_ERROR; 
    }
    }




    CLI_print("\nDefine  services \n");
    if (MEA_Example4_define_upstream_services(MEA_Example4_user_ports[0],MEA_Example4_network_ports[0]) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_define_upstream_services failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }






   CLI_print("\n Admin ON:\n");
    if (MEA_Example4_configIngressAdminON()!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_configIngressAdminON failed\n",
                          __FUNCTION__);
    }
    
    if(MEA_Example4_configEgressAdminON()!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Example3_configEgressAdminON failed\n",
                          __FUNCTION__);
    }
    

    MEA_Example4_define_upstream_static_forwarder(MEA_OS_inet_addr("10.10.1.1"), 80);


    /* Return to caller */
    CLI_print("\n");
    return MEA_OK;

}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_CLI_func>                                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_CLI_func(int argc, char *argv[]) {


    if ((argc != 2) && (argc != 3) && (argc!= 4)) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        return MEA_ERROR;
    }


    if (MEA_Example4_main() != MEA_OK) {
        CLI_print("Error: ,MEA_Example4_main_failed \n");
    } else {
        CLI_print("Done.\n");
    }
   
 
   return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example4_CLI_Init>                                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example4_CLI_Init() {


   CLI_defineCmd("MEA example case4 ",
                 (CmdFunc)MEA_Example4_CLI_func,
                 "Example case4 configuration \n",
                 "Usage: case4 all \n"
                 "                 \n"
                 "----------------------------------\n"
                 "       all - create all entities  \n"
                 "----------------------------------\n"
                 "Examples : - case4 all            \n"
                 "           - case4 all        \n");


    return MEA_OK;
}

