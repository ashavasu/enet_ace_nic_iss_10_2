

#include "mea_drv_common.h"
#include "mea_api.h"
#include "mea_cli.h"
#include "cli_eng.h"

#if defined(WITH_MEA_EXAMPLE5_CLI)
/* MACROs */
#define EXAMPLE5_UPLINK_PORT				118
#define EXAMPLE5_CPU_PORT					127
#define EXAMPLE5_NUM_OF_CUSTOMER_PORTS		8
#define EXAMPLE5_NUM_OF_CUSTOMER_MNG_PORTS	27
#define EXAMPLE5_NUM_OF_CUSTOMER_MNG_UPLINK_PORTS	29
#define EXAMPLE5_NUM_OF_HOST_MNG_PORTS	    4
#define EXAMPLE5_NUM_OF_NET_HOST_PORTS      2

#define EXAMPLE5_CUSTOMER_MNG_PORT_31			31
#define EXAMPLE5_CUSTOMER_MNG_PORT_63			63
#define EXAMPLE5_CUSTOMER_MNG_PORT_95			95


#define EXAMPLE5_CUSTOMER_MNG_PORT_VLAN     64

#define EXAMPLE5_MANAGEMENT_VLAN			4094
#define EXAMPLE5_DEFAULT_VLAN				4095

#define EXAMPLE5_ACT_CPU_QinQ_VLAN			   4094


/* list of customer ports */
MEA_Port_t   customer_ports[EXAMPLE5_NUM_OF_CUSTOMER_PORTS] =
{
		0,
		1,
		2,
		3,
		4,
		5,
		6,
        7
};

MEA_Port_t   customer_and_all_ports[10] =
{
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,31,100
};
MEA_Port_t   networkTo_customer_mng_and_host[28] =
{
    0,1,2,3,4,5,6,7,
    8,9,10,11,12,13,14,15,
    16,17,18,19,20,21,22,23,
    EXAMPLE5_CUSTOMER_MNG_PORT_31,
    EXAMPLE5_CUSTOMER_MNG_PORT_63,
    EXAMPLE5_CUSTOMER_MNG_PORT_95,
    EXAMPLE5_CPU_PORT
    
};




MEA_Port_t   customer_and_MNG_ports[EXAMPLE5_NUM_OF_CUSTOMER_MNG_PORTS] =
{
    0,1,2,3,4,5,6,7,
    8,9,10,11,12,13,14,15,
    16,17,18,19,20,21,22,23,
    EXAMPLE5_CUSTOMER_MNG_PORT_31,
    EXAMPLE5_CUSTOMER_MNG_PORT_63,
    EXAMPLE5_CUSTOMER_MNG_PORT_95
};

MEA_Port_t   customer_and_MNG_Uplink_ports[EXAMPLE5_NUM_OF_CUSTOMER_MNG_UPLINK_PORTS] =
{
    0,1,2,3,4,5,6,7,
    8,9,10,11,12,13,14,15,
    16,17,18,19,20,21,22,23,

    EXAMPLE5_CUSTOMER_MNG_PORT_31,
    EXAMPLE5_CUSTOMER_MNG_PORT_63,
    EXAMPLE5_CUSTOMER_MNG_PORT_95,
    EXAMPLE5_UPLINK_PORT,
    EXAMPLE5_CPU_PORT

};


MEA_Port_t   host_and_MNG_ports[EXAMPLE5_NUM_OF_HOST_MNG_PORTS] =
{
    EXAMPLE5_CPU_PORT,
    EXAMPLE5_CUSTOMER_MNG_PORT_31,
    EXAMPLE5_CUSTOMER_MNG_PORT_63,
    EXAMPLE5_CUSTOMER_MNG_PORT_95
};


MEA_Port_t   network_and_host_ports[EXAMPLE5_NUM_OF_NET_HOST_PORTS] =
{
    EXAMPLE5_UPLINK_PORT,
    EXAMPLE5_CPU_PORT
};

MEA_Port_t   uplink_and_MNG_ports[EXAMPLE5_NUM_OF_HOST_MNG_PORTS] =
{
    EXAMPLE5_UPLINK_PORT,
    EXAMPLE5_CUSTOMER_MNG_PORT_31,
    EXAMPLE5_CUSTOMER_MNG_PORT_63,
    EXAMPLE5_CUSTOMER_MNG_PORT_95
};




/* VLAN per customer port */
MEA_Uint16   customer_vlan[EXAMPLE5_NUM_OF_CUSTOMER_PORTS] =
{
		100,
		200,
		300,
		400,
		500,
		600,
		700,
		800
};

/* structures for services */
typedef struct
{
	MEA_Port_t 								port_id;
	MEA_Bool								valid;
	MEA_Service_t   						service_id;
	MEA_Action_t                            action_id;
	MEA_LxCp_t                              lxcp_Id;
}tServiceDb;


typedef struct
{
    MEA_Bool								valid;
    MEA_Service_t   						service_id;

}tServiceDb_control_mng;

typedef struct
{
    MEA_Bool								valid;
    MEA_Action_t   						Action_Id;
    MEA_Uint16                          policerId;

}tActionDb_control_mng;


/* static variables */
static tServiceDb customer_service_db[EXAMPLE5_NUM_OF_CUSTOMER_PORTS];
static tServiceDb uplink_pointToPoint[EXAMPLE5_NUM_OF_CUSTOMER_PORTS];
static tServiceDb uplink_service_db;
static tServiceDb cpu_service_db;
static tServiceDb customer_mng_service_db;
//static tServiceDb customer_mng_service_tag_db;
static MEA_Bool init_config_params=MEA_FALSE;


static tServiceDb_control_mng cpu_PointToPoint_service_ToChannel_db[EXAMPLE5_NUM_OF_CUSTOMER_MNG_UPLINK_PORTS]; /*also for the network*/

static tActionDb_control_mng       LxcpActionToCpu;
 


/* functions */




static MEA_Status MEA_Example5_Create_ActionCPU_ForLxcp(tActionDb_control_mng *CpuAction)
{
    MEA_Action_Entry_Data_dbt             Action_data;
    MEA_Policer_Entry_dbt                 Action_policer;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
    MEA_EHP_Info_dbt                      Action_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
    MEA_Action_t                          CPU_Action_id;
    MEA_Uint16                            policer_id = 0;
    MEA_AcmMode_t                         acm_mode = 0;
    MEA_Port_t                            setport;
    
    MEA_OS_memset(&Action_data, 0, sizeof(Action_data));
    
    MEA_OS_memset(&Action_policer, 0, sizeof(Action_policer));
    MEA_OS_memset(&Action_outPorts, 0, sizeof(Action_outPorts));
    MEA_OS_memset(&Action_editing_info, 0, sizeof(Action_editing_info));
    MEA_OS_memset(&Action_editing, 0, sizeof(Action_editing));
    
    
    
    
    Action_policer.CIR = 10000000;
    Action_policer.CBS = 64000;
    Action_policer.EIR = 0;
    Action_policer.EBS = 0;
    Action_policer.comp = 0;
    Action_policer.gn_type = 1;

    policer_id = MEA_PLAT_GENERATE_NEW_ID;
    acm_mode = 0;

    if (MEA_API_Create_Policer_ACM_Profile(MEA_UNIT_0,
        &policer_id,
        acm_mode,
        MEA_INGRESS_PORT_PROTO_TRANS,
        &Action_policer) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_API_Create_Policer_ACM_Profile failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }





    /* create reverse action */
    Action_data.output_ports_valid = MEA_TRUE;
    Action_data.force_color_valid = MEA_TRUE;
    Action_data.COLOR = 2;
    Action_data.force_cos_valid = MEA_FALSE;
    Action_data.COS = 0;
    Action_data.force_l2_pri_valid = MEA_FALSE;
    Action_data.L2_PRI = 0;

    Action_data.pm_id_valid = MEA_TRUE;
    Action_data.pm_id = 0; /* Request new id */
    Action_data.tm_id_valid = MEA_TRUE;
    Action_data.tm_id = 0;
    Action_data.ed_id_valid = MEA_TRUE;
    Action_data.ed_id = 0; /* Request new id */
    Action_data.protocol_llc_force = MEA_TRUE;
    Action_data.proto_llc_valid = MEA_TRUE;  /* force protocol and llc */
    Action_data.Protocol = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
    Action_data.Llc = MEA_FALSE;



    Action_editing_info.ehp_data.EditingType                            = MEA_EDITINGTYPE_APPEND_APPEND;
    Action_editing_info.ehp_data.atm_info.double_tag_cmd                = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
    Action_editing_info.ehp_data.atm_info.val.double_vlan_tag.eth_type  = 0x8100;
    Action_editing_info.ehp_data.atm_info.val.double_vlan_tag.vid       = EXAMPLE5_ACT_CPU_QinQ_VLAN;
    


    Action_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
    Action_editing_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
    Action_editing_info.ehp_data.eth_info.val.vlan.vid = 0;


    
    Action_editing_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
    Action_editing_info.ehp_data.eth_info.stamp_priority            = MEA_TRUE;
    Action_editing_info.ehp_data.eth_info.stamp_color               = MEA_TRUE;
    Action_editing_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    Action_editing_info.ehp_data.eth_stamping_info.color_bit0_loc = 12;
    Action_editing_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
    Action_editing_info.ehp_data.eth_stamping_info.color_bit1_loc = 0;
    Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_valid = MEA_TRUE;
    Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_loc = 13;
    Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_valid = MEA_TRUE;
    Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_loc = 14;
    Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_valid = MEA_TRUE;
    Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_loc = 15;
    
    Action_data.output_ports_valid = MEA_TRUE;
    setport = EXAMPLE5_CPU_PORT;
    MEA_SET_OUTPORT(&Action_outPorts, setport);
    MEA_SET_OUTPORT(&Action_editing_info.output_info, setport);


    Action_editing.num_of_entries = 1;               /* Number of the entries in the Editing table */
    Action_editing.ehp_info = &Action_editing_info;

   

    CPU_Action_id = MEA_PLAT_GENERATE_NEW_ID;

    if (MEA_API_Create_Action(MEA_UNIT_0,
        &Action_data,
        &Action_outPorts,
        &Action_policer,
        &Action_editing,
        &CPU_Action_id) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_API_Create_Action failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }
    CpuAction->valid = MEA_TRUE;
    CpuAction->Action_Id = CPU_Action_id;
    CpuAction->policerId = policer_id;


    return MEA_OK;
}


/*************************************************************************
 *                                                                        *
 *    Function Name        : MEA_Example5_initialization                  *
 *                                                                        *
 *    Description          : This function init the service database      *
 *                                                                        *
 *    Input(s)             : none                                         *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : None                                      *
 *                                                                        *
 **************************************************************************/
MEA_Status MEA_Example5_initialization(void)
{
	MEA_Uint32 idx=0;
   	MEA_LxCp_t          UplinkLxCpId=MEA_PLAT_GENERATE_NEW_ID;
   	MEA_LxCp_t          CustomerLxCpId=MEA_PLAT_GENERATE_NEW_ID;
	MEA_LxCP_Entry_dbt  LxCp_entry;

	if(init_config_params == MEA_FALSE)
	{
        MEA_OS_memset(&LxCp_entry, 0, sizeof(LxCp_entry));
        if(MEA_API_Create_LxCP(MEA_UNIT_0, &LxCp_entry, &CustomerLxCpId))
        {
        	 return MEA_ERROR;
        }
        MEA_OS_memset(&LxCp_entry, 0, sizeof(LxCp_entry));
        if(MEA_API_Create_LxCP(MEA_UNIT_0, &LxCp_entry, &UplinkLxCpId))
        {
         	return MEA_ERROR;
        }

		for(idx=0;idx<EXAMPLE5_NUM_OF_CUSTOMER_PORTS;idx++)
		{
			customer_service_db[idx].port_id = customer_ports[idx];
			customer_service_db[idx].service_id = MEA_PLAT_GENERATE_NEW_ID;
			customer_service_db[idx].action_id = MEA_PLAT_GENERATE_NEW_ID;
			customer_service_db[idx].lxcp_Id = CustomerLxCpId;
			customer_service_db[idx].valid = MEA_FALSE;
		}
		for(idx=0;idx<EXAMPLE5_NUM_OF_CUSTOMER_PORTS;idx++)
		{
			uplink_pointToPoint[idx].port_id = EXAMPLE5_UPLINK_PORT;
			uplink_pointToPoint[idx].service_id = MEA_PLAT_GENERATE_NEW_ID;
			uplink_pointToPoint[idx].action_id = MEA_PLAT_GENERATE_NEW_ID;
			uplink_pointToPoint[idx].lxcp_Id = CustomerLxCpId;
			uplink_pointToPoint[idx].valid = MEA_FALSE;
		}

        
        MEA_OS_memset(&cpu_PointToPoint_service_ToChannel_db[0], 0, sizeof(cpu_PointToPoint_service_ToChannel_db));

		uplink_service_db.port_id = EXAMPLE5_UPLINK_PORT;
		uplink_service_db.service_id = MEA_PLAT_GENERATE_NEW_ID;
		uplink_service_db.action_id = MEA_PLAT_GENERATE_NEW_ID;
		uplink_service_db.lxcp_Id = UplinkLxCpId;
		uplink_service_db.valid = MEA_FALSE;

		cpu_service_db.port_id = EXAMPLE5_CPU_PORT;
		cpu_service_db.service_id = MEA_PLAT_GENERATE_NEW_ID;
		cpu_service_db.action_id = MEA_PLAT_GENERATE_NEW_ID;
		cpu_service_db.lxcp_Id = CustomerLxCpId;
		cpu_service_db.valid = MEA_FALSE;

		customer_mng_service_db.port_id = EXAMPLE5_CUSTOMER_MNG_PORT_31;
		customer_mng_service_db.service_id = MEA_PLAT_GENERATE_NEW_ID;
		customer_mng_service_db.action_id = MEA_PLAT_GENERATE_NEW_ID;
		customer_mng_service_db.lxcp_Id = CustomerLxCpId;
		customer_mng_service_db.valid = MEA_FALSE;
        LxcpActionToCpu.valid = 0;

        MEA_Example5_Create_ActionCPU_ForLxcp(&LxcpActionToCpu);

		init_config_params = MEA_TRUE;
	}


    


	
    
    
    return MEA_OK;
}
/*************************************************************************
 *                                                                        *
 *    Function Name        : MEA_Example5_termination                     *
 *                                                                        *
 *    Description          : This function terminate services             *
 *                           ingress port                                 *
 *                                                                        *
 *    Input(s)             : none                                         *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : MEA_OK if succeeded, otherwise            *
 *                              MEA_ERROR                                 *
 *                                                                        *
 **************************************************************************/
MEA_Status MEA_Example5_cleanup(void)
{
	MEA_Uint32 idx=0;

	for(idx=0;idx<EXAMPLE5_NUM_OF_CUSTOMER_PORTS;idx++)
	{
		/*******************/
		if(customer_service_db[idx].service_id != MEA_PLAT_GENERATE_NEW_ID)
		{
			if(MEA_API_Delete_Service(MEA_UNIT_0, customer_service_db[idx].service_id) != MEA_OK)
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Service failed line:%d\r\n",__LINE__);
				 return MEA_ERROR;
			}
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete serviveId:%d line:%d\r\n",customer_service_db[idx].service_id,__LINE__);
			customer_service_db[idx].service_id = MEA_PLAT_GENERATE_NEW_ID;
		}
		if(customer_service_db[idx].action_id != MEA_PLAT_GENERATE_NEW_ID)
		{
			if (MEA_API_Delete_Action (MEA_UNIT_0,customer_service_db[idx].action_id)!=MEA_OK)
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Action failed line:%d\r\n",__LINE__);
				return MEA_ERROR;
			}
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete action:%d line:%d\r\n",customer_service_db[idx].action_id,__LINE__);
			customer_service_db[idx].action_id = MEA_PLAT_GENERATE_NEW_ID;
		}

		/**************/
		if(uplink_pointToPoint[idx].service_id != MEA_PLAT_GENERATE_NEW_ID)
		{
			if(MEA_API_Delete_Service(MEA_UNIT_0, uplink_pointToPoint[idx].service_id) != MEA_OK)
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Service failed line:%d\r\n",__LINE__);
				 return MEA_ERROR;
			}
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete serviveId:%d line:%d\r\n",uplink_pointToPoint[idx].service_id,__LINE__);
			uplink_pointToPoint[idx].service_id = MEA_PLAT_GENERATE_NEW_ID;
		}
		if(uplink_pointToPoint[idx].action_id != MEA_PLAT_GENERATE_NEW_ID)
		{
			if (MEA_API_Delete_Action (MEA_UNIT_0,uplink_pointToPoint[idx].action_id)!=MEA_OK)
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Service failed line:%d\r\n",__LINE__);
				return MEA_ERROR;
			}
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete action:%d line:%d\r\n",uplink_pointToPoint[idx].action_id,__LINE__);
			uplink_pointToPoint[idx].action_id = MEA_PLAT_GENERATE_NEW_ID;
		}
	}

	/*************/
	if(uplink_service_db.service_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if(MEA_API_Delete_Service(MEA_UNIT_0, uplink_service_db.service_id) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Service failed line:%d\r\n",__LINE__);
			 return MEA_ERROR;
		}
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete serviveId:%d line:%d\r\n",uplink_service_db.service_id ,__LINE__);
		uplink_service_db.service_id = MEA_PLAT_GENERATE_NEW_ID;
	}
	if(uplink_service_db.action_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (MEA_API_Delete_Action (MEA_UNIT_0,uplink_service_db.action_id)!=MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Action failed line:%d\r\n",__LINE__);
			return MEA_ERROR;
		}
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete action:%d line:%d\r\n",uplink_service_db.action_id,__LINE__);
		uplink_service_db.action_id = MEA_PLAT_GENERATE_NEW_ID;
	}

	/**************/
	if(cpu_service_db.service_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if(MEA_API_Delete_Service(MEA_UNIT_0, cpu_service_db.service_id) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Service failed line:%d\r\n",__LINE__);
			 return MEA_ERROR;
		}
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete action:%d line:%d\r\n",cpu_service_db.service_id,__LINE__);
		cpu_service_db.service_id = MEA_PLAT_GENERATE_NEW_ID;
	}
	if(cpu_service_db.action_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (MEA_API_Delete_Action (MEA_UNIT_0,cpu_service_db.action_id)!=MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Action failed line:%d\r\n",__LINE__);
			return MEA_ERROR;
		}
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete action:%d line:%d\r\n",cpu_service_db.action_id,__LINE__);
		cpu_service_db.action_id = MEA_PLAT_GENERATE_NEW_ID;
	}

	/**********/
	if(customer_mng_service_db.service_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if(MEA_API_Delete_Service(MEA_UNIT_0, customer_mng_service_db.service_id) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Service failed line:%d\r\n",__LINE__);
			 return MEA_ERROR;
		}
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete action:%d line:%d\r\n",customer_mng_service_db.service_id,__LINE__);
		customer_mng_service_db.service_id = MEA_PLAT_GENERATE_NEW_ID;
	}
	if(customer_mng_service_db.action_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (MEA_API_Delete_Action (MEA_UNIT_0,customer_mng_service_db.action_id)!=MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Delete_Action failed line:%d\r\n",__LINE__);
			return MEA_ERROR;
		}
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"delete action:%d line:%d\r\n",customer_mng_service_db.action_id,__LINE__);
		customer_mng_service_db.action_id = MEA_PLAT_GENERATE_NEW_ID;
	}

    
/*----*/
#if 0
if (customer_mng_service_tag_db.service_id != MEA_PLAT_GENERATE_NEW_ID)
{
    if (MEA_API_Delete_Service(MEA_UNIT_0, customer_mng_service_tag_db.service_id) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_API_Delete_Service failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "delete action:%d line:%d\r\n", customer_mng_service_tag_db.service_id, __LINE__);
    customer_mng_service_tag_db.service_id = MEA_PLAT_GENERATE_NEW_ID;
}
    if (customer_mng_service_tag_db.action_id != MEA_PLAT_GENERATE_NEW_ID)
    {
        if (MEA_API_Delete_Action(MEA_UNIT_0, customer_mng_service_tag_db.action_id) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_API_Delete_Action failed line:%d\r\n", __LINE__);
            return MEA_ERROR;
        }
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "delete action:%d line:%d\r\n", customer_mng_service_tag_db.action_id, __LINE__);
        customer_mng_service_tag_db.action_id = MEA_PLAT_GENERATE_NEW_ID;
    }
#endif

    for (idx = 0; idx < EXAMPLE5_NUM_OF_CUSTOMER_MNG_UPLINK_PORTS; idx++)
    {
        if (cpu_PointToPoint_service_ToChannel_db[idx].valid) {
            if (MEA_API_Delete_Service(MEA_UNIT_0, cpu_PointToPoint_service_ToChannel_db[idx].service_id) != MEA_OK) {
                return MEA_ERROR;
            }
            cpu_PointToPoint_service_ToChannel_db[idx].valid = MEA_FALSE;
            cpu_PointToPoint_service_ToChannel_db[idx].service_id = MEA_PLAT_GENERATE_NEW_ID;
        }
        
    }




	return MEA_OK;
}
/*************************************************************************
 *                                                                        *
 *    Function Name        : MEA_Example5_configIngressAdminON            *
 *                                                                        *
 *    Description          : This function is used to enable the          *
 *                           ingress port                                 *
 *                                                                        *
 *    Input(s)             : none                                         *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : MEA_OK if succeeded, otherwise            *
 *                              MEA_ERROR                                 *
 *                                                                        *
 **************************************************************************/
MEA_Status MEA_Example5_configIngressAdminON(void)
{

    MEA_Port_t port;
    MEA_IngressPort_Entry_dbt entry;


    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++)
    {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE)
        {
            continue;
        }


        if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK)
        {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Get_IngressPort_Entry failed (id=%d)\n",
                      __FUNCTION__,port);
            return MEA_ERROR;
        }
        entry.rx_enable=MEA_TRUE;
        if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                  port,
                                  &entry) != MEA_OK)
        {
        	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Set_IngressPort_Entry failed (port=%d)\n",
                      __FUNCTION__,
                      port);
        	return MEA_ERROR;
        }

    }
    return MEA_OK;
}
/*************************************************************************
 *                                                                        *
 *    Function Name        : MEA_Example5_configEgressAdminON             *
 *                                                                        *
 *    Description          : This function is used to enable the          *
 *                           egress port                                  *
 *                                                                        *
 *    Input(s)             : none                                         *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : MEA_OK if succeeded, otherwise            *
 *                              MEA_ERROR                                 *
 *                                                                        *
 **************************************************************************/
MEA_Status MEA_Example5_configEgressAdminON(void)
{

    MEA_Port_t port;
    MEA_EgressPort_Entry_dbt entry;


    for(port=0; port <=MEA_MAX_PORT_NUMBER; port++)
    {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE)
        {
            continue;
        }
        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
            return MEA_ERROR;
        }
        entry.tx_enable=MEA_TRUE;
       if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &entry) != MEA_OK)
       {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry failed (port=%d)\n",
                          __FUNCTION__,
                          port);
            return MEA_ERROR;
        }

    }


    return MEA_OK;
}

/*************************************************************************
 *                                                                        *
 *    Function Name        : setDefaultService                            *
 *                                                                        *
 *    Description          : This function set the default service        *
 *                           per port                                     *
 *                                                                        *
 *    Input(s)             : none                                         *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : MEA_OK if succeeded, otherwise            *
 *                              MEA_ERROR                                 *
 *                                                                        *
 **************************************************************************/
static MEA_Status setDefaultService (MEA_Port_t port, MEA_Service_t service_id)
{
 	MEA_IngressPort_Entry_dbt entry_ingress;


	if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0, port, &entry_ingress) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Get_IngressPort_Entry failed line:%d\r\n",__LINE__);
        return MEA_ERROR;
	}

    entry_ingress.parser_info.default_sid_info.action = 2;
    entry_ingress.parser_info.default_sid_info.def_sid= service_id;


	if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0, port, &entry_ingress) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Set_IngressPort_Entry failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}
    return MEA_OK;
}
/*************************************************************************
 *                                                                        *
 *    Function Name        : MEA_Example5_configPointToPoint              *
 *                                                                        *
 *    Description          : This function configure point to point       *
 *                           configuration                                *
 *                                                                        *
 *    Input(s)             : pServiceIn - service database                *
 *                           output_port - send to output port            *
 *                           vlanId - vlan id                             *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : MEA_OK if succeeded, otherwise            *
 *                              MEA_ERROR                                 *
 *                                                                        *
 **************************************************************************/
static MEA_Status MEA_Example5_configPointToPoint(tServiceDb *pServiceIn,MEA_Port_t output_port,MEA_Uint16 vlanId,MEA_Uint16 TYPE, MEA_Uint16 add_vlan)
{
	/* for service configuration */
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;

    /* policer configuration */
    MEA_Policer_Entry_dbt                 PolicerEntry;
    MEA_Uint16                            policer_id=0;
    MEA_AcmMode_t                         acm_mode=0;
//	MEA_Uint32 							  idx=0;
//	MEA_Uint32							  num_of_outport;



	/* init */
	MEA_OS_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));
	MEA_OS_memset(&Service_key , 0 , sizeof(Service_key));
	MEA_OS_memset(&Service_data , 0 , sizeof(Service_data));
	MEA_OS_memset(&Service_policer , 0 , sizeof(Service_policer));
	MEA_OS_memset(&Service_editing , 0 , sizeof(Service_editing));
	MEA_OS_memset(&PolicerEntry , 0 , sizeof(PolicerEntry));
	MEA_OS_memset(&Service_editing_info , 0 , sizeof(Service_editing_info));



	Service_policer.CIR = 1000000000;
	Service_policer.CBS = 64000;
	Service_policer.EIR = 0;
	Service_policer.EBS = 0;
	Service_policer.comp = 0;
	Service_policer.gn_type = 1;



	policer_id = MEA_PLAT_GENERATE_NEW_ID;

	Service_policer.CIR = 1000000000;
	Service_policer.CBS = 64000;
	//PolicerEntry.EIR = 90000000;
	//PolicerEntry.EBS = 32000;
	Service_policer.comp = 0;
	Service_policer.gn_type= 1;
	policer_id = MEA_PLAT_GENERATE_NEW_ID;
	acm_mode=0;

	if(MEA_API_Create_Policer_ACM_Profile(MEA_UNIT_0,
										  &policer_id,
										  acm_mode,
										  MEA_INGRESS_PORT_PROTO_TRANS,
										  &Service_policer) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Create_Policer_ACM_Profile failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	Service_data.policer_prof_id = policer_id;




	Service_key.src_port         = pServiceIn->port_id;
	Service_key.net_tag          = 0xff000 | vlanId;
    if(vlanId != 0)
	  Service_key.L2_protocol_type =  MEA_PARSING_L2_KEY_Ctag;
    else
       Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;



	/* Made the service priority-unaware (default priority) */
    Service_key.evc_enable       = MEA_FALSE; //MEA_TRUE;
    Service_key.pri = 7; //0;


	/* Build the upstream service data attributes for the forwarder  */
	Service_data.DSE_forwarding_enable   = MEA_FALSE;
	Service_data.vpn 					 = 0;

	Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	Service_data.DSE_learning_enable     = MEA_FALSE;
	Service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
	Service_data.DSE_learning_actionId   = 0;
	Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	Service_data.DSE_learning_srcPort	 = 0;
	Service_data.DSE_learning_srcPort_force = MEA_FALSE;


	/* Set the upstream service data attribute for policing */
	Service_data.tmId   = 0; /* generate new id */
	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

	/* Set the upstream service data attribute for pm (counters) */
	Service_data.pmId   = 0; /* generate new id */

	/* Set the upstream service data attribute for policing */
	Service_data.editId = 0; /* generate new id */

	/* Set the other upstream service data attributes to defaults */
	Service_data.ADM_ENA            = MEA_FALSE;
	Service_data.CM                 = MEA_FALSE;
	Service_data.L2_PRI_FORCE       = MEA_FALSE;
	Service_data.L2_PRI             = 0;
	Service_data.COLOR_FORSE        = MEA_TRUE;
	Service_data.COLOR              = 2;
	Service_data.COS_FORCE          = MEA_FALSE;
	Service_data.COS                = 0;
	Service_data.protocol_llc_force = MEA_TRUE;
	Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
	Service_data.Llc                = MEA_FALSE;


	Service_editing.ehp_info       = &Service_editing_info;


	Service_editing_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
   Service_editing_info.ehp_data.eth_info.command           = TYPE;

	Service_editing_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
    if (TYPE == MEA_EGRESS_HEADER_PROC_CMD_EXTRACT) {
        Service_editing_info.ehp_data.eth_info.val.vlan.vid = vlanId;
    }
    else {
        Service_editing_info.ehp_data.eth_info.val.vlan.vid = add_vlan;
    }
	Service_editing_info.ehp_data.eth_info.stamp_color       = MEA_FALSE;
	Service_editing_info.ehp_data.eth_info.stamp_priority    = MEA_FALSE;


	/* send to broadcast output ports */
	MEA_SET_OUTPORT(&Service_outPorts, output_port);
	MEA_SET_OUTPORT(&Service_editing_info.output_info,output_port);

	Service_data.LxCp_enable = MEA_FALSE;



	/* Create new service for the given Vlan. */
	if (MEA_API_Create_Service(MEA_UNIT_0,
							   &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceIn->service_id) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Create_Service failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	return MEA_OK;

}




static MEA_Status MEA_Example5_configPointToPointCPU_toPort(tServiceDb_control_mng *pServiceIn, 
                                                            MEA_Uint16 OuterVlanId, 
                                                            MEA_Uint16 vlanId, 
                                                            MEA_Port_t output_port)
{
    /* for service configuration */
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;

    /* policer configuration */
    MEA_Policer_Entry_dbt                 PolicerEntry;
    MEA_Uint16                            policer_id = 0;
    MEA_AcmMode_t                         acm_mode = 0;
    MEA_Service_t                          serviceId;
    //	MEA_Uint32 							  idx=0;
    //	MEA_Uint32							  num_of_outport;



    /* init */
    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    MEA_OS_memset(&Service_key, 0, sizeof(Service_key));
    MEA_OS_memset(&Service_data, 0, sizeof(Service_data));
    MEA_OS_memset(&Service_policer, 0, sizeof(Service_policer));
    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));
    MEA_OS_memset(&PolicerEntry, 0, sizeof(PolicerEntry));
    MEA_OS_memset(&Service_editing_info, 0, sizeof(Service_editing_info));



    Service_policer.CIR = 1000000000;
    Service_policer.CBS = 64000;
    Service_policer.EIR = 0;
    Service_policer.EBS = 0;
    Service_policer.comp = 0;
    Service_policer.gn_type = 1;



    policer_id = MEA_PLAT_GENERATE_NEW_ID;

    Service_policer.CIR = 1000000000;
    Service_policer.CBS = 64000;
    //PolicerEntry.EIR = 90000000;
    //PolicerEntry.EBS = 32000;
    Service_policer.comp = 0;
    Service_policer.gn_type = 1;
    policer_id = MEA_PLAT_GENERATE_NEW_ID;
    acm_mode = 0;

    if (MEA_API_Create_Policer_ACM_Profile(MEA_UNIT_0,
        &policer_id,
        acm_mode,
        MEA_INGRESS_PORT_PROTO_TRANS,
        &Service_policer) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_API_Create_Policer_ACM_Profile failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }

    Service_data.policer_prof_id = policer_id;




    Service_key.src_port = EXAMPLE5_CPU_PORT;
    Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Ctag_Ctag;
    Service_key.net_tag = 0xff000 | OuterVlanId;  
    Service_key.inner_netTag_from_valid = MEA_TRUE;
    Service_key.inner_netTag_from_value = (0xfff000 | vlanId);
        
    



    /* Made the service priority-unaware (default priority) */
    Service_key.evc_enable = MEA_FALSE; //MEA_TRUE;
    Service_key.pri = 7; //0;


                         /* Build the upstream service data attributes for the forwarder  */
    Service_data.DSE_forwarding_enable = MEA_FALSE;
    Service_data.vpn = 100;

    Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    Service_data.DSE_learning_enable = MEA_FALSE;
    Service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    Service_data.DSE_learning_actionId = 0; //LxcpActionToCpu.Action_Id;
    Service_data.DSE_learning_actionId_valid = MEA_FALSE;
    Service_data.DSE_learning_srcPort = 0; //EXAMPLE5_CPU_PORT;
    Service_data.DSE_learning_srcPort_force = MEA_FALSE;


    /* Set the upstream service data attribute for policing */
    Service_data.tmId = 0; /* generate new id */
    Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

                                                                       /* Set the upstream service data attribute for pm (counters) */
    Service_data.pmId = 0; /* generate new id */

                           /* Set the upstream service data attribute for policing */
    Service_data.editId = 0; /* generate new id */

                             /* Set the other upstream service data attributes to defaults */
    Service_data.ADM_ENA = MEA_FALSE;
    Service_data.CM = MEA_FALSE;
    Service_data.L2_PRI_FORCE = MEA_FALSE;
    Service_data.L2_PRI = 0;
    Service_data.COLOR_FORSE = MEA_TRUE;
    Service_data.COLOR = 2;
    Service_data.COS_FORCE = MEA_FALSE;
    Service_data.COS = 0;
    Service_data.protocol_llc_force = MEA_TRUE;
    Service_data.Protocol = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
    Service_data.Llc = MEA_FALSE;


    Service_editing.ehp_info = &Service_editing_info;


    Service_editing_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
    Service_editing_info.ehp_data.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT; /*MEA_EGRESS_HEADER_PROC_CMD_EXTRACT*/
    Service_editing_info.ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
    /* send to broadcast output ports */
    MEA_SET_OUTPORT(&Service_outPorts, output_port);
    //MEA_SET_OUTPORT(&Service_editing_info.output_info, output_port);



    serviceId = MEA_PLAT_GENERATE_NEW_ID;
    /* Create new service for the given Vlan. */
    if (MEA_API_Create_Service(MEA_UNIT_0,
        &Service_key,
        &Service_data,
        &Service_outPorts,
        &Service_policer,
        &Service_editing,
        &serviceId) != MEA_OK)
    {
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_API_Create_Service failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " ServiceFromCPU <Outer=%4d> <inner=%4d>-->  output_port %4d   \r\n", OuterVlanId,vlanId,output_port);
    pServiceIn->service_id = serviceId;
    pServiceIn->valid = MEA_TRUE;


    return MEA_OK;
    
}



/*************************************************************************
 *                                                                        *
 *    Function Name        : MEA_Example5_configFullBridging              *
 *                                                                        *
 *    Description          : This function configure full bridging        *
 *                           configuration                                *
 *                                                                        *
 *    Input(s)             : pServiceIn - service database                *
 *                           pPort - list of output ports                 *
 *                           vlanId - vlan id                             *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : MEA_OK if succeeded, otherwise            *
 *                              MEA_ERROR                                 *
 *                                                                        *
 **************************************************************************/
static MEA_Status MEA_Example5_configFullBridging(MEA_Bool forwarding_enable,
    MEA_Bool learning_enable,
    tServiceDb *pServiceIn,
    MEA_Port_t *pPort,
    MEA_Uint32 num_of_outport,
    MEA_Uint32 vlan,
    MEA_Uint16 TYPE,
    MEA_Uint16 add_vlan)
{
    /* for service configuration */
    MEA_Def_SID_dbt                       DefSIDentry;
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EHP_Info_dbt                      Service_editing_info[2];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;


    MEA_Bool                               en_OneOfOUT_Is_CPU = MEA_FALSE;
    /* policer configuration */
    MEA_Policer_Entry_dbt                 PolicerEntry;
    MEA_Uint16                            policer_id = 0;
    MEA_AcmMode_t                         acm_mode = 0;
    MEA_Uint32 							  idx = 0;

    /* reverse action */
    MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
    //	MEA_Policer_Entry_dbt                 Action_policer;
    MEA_EHP_Info_dbt                      Action_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
    //	MEA_Action_t                          Action_id;


        /* init */
    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    MEA_OS_memset(&Service_key, 0, sizeof(Service_key));
    MEA_OS_memset(&Service_data, 0, sizeof(Service_data));
    MEA_OS_memset(&Service_policer, 0, sizeof(Service_policer));
    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));
    MEA_OS_memset(&PolicerEntry, 0, sizeof(PolicerEntry));
    MEA_OS_memset(&Service_editing_info, 0, sizeof(Service_editing_info));

    MEA_OS_memset(&Action_data, 0, sizeof(Action_data));
    MEA_OS_memset(&Action_outPorts, 0, sizeof(Action_outPorts));
    MEA_OS_memset(&Action_editing_info, 0, sizeof(Action_editing_info));
    MEA_OS_memset(&Action_editing, 0, sizeof(Action_editing));



    Service_policer.CIR = 1000000000;
    Service_policer.CBS = 64000;
    Service_policer.EIR = 0;
    Service_policer.EBS = 0;
    Service_policer.comp = 0;
    Service_policer.gn_type = 1;



    policer_id = MEA_PLAT_GENERATE_NEW_ID;

    Service_policer.CIR = 1000000000;
    Service_policer.CBS = 64000;

    Service_policer.comp = 0;
    Service_policer.gn_type = 1;
    policer_id = MEA_PLAT_GENERATE_NEW_ID;
    acm_mode = 0;

    if (MEA_API_Create_Policer_ACM_Profile(MEA_UNIT_0,
        &policer_id,
        acm_mode,
        MEA_INGRESS_PORT_PROTO_TRANS,
        &Service_policer) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_API_Create_Policer_ACM_Profile failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }

    Service_data.policer_prof_id = policer_id;


    /* create reverse action */
    Action_data.output_ports_valid = MEA_TRUE;
    Action_data.force_color_valid = MEA_TRUE;
    Action_data.COLOR = 2;
    Action_data.force_cos_valid = MEA_FALSE;
    Action_data.COS = 0;
    Action_data.force_l2_pri_valid = MEA_FALSE;
    Action_data.L2_PRI = 0;

    Action_data.pm_id_valid = MEA_TRUE;
    Action_data.pm_id = 0; /* Request new id */
    Action_data.tm_id_valid = MEA_FALSE;
    Action_data.tm_id = 0;
    Action_data.ed_id_valid = MEA_TRUE;
    Action_data.ed_id = 0; /* Request new id */
    Action_data.protocol_llc_force = MEA_TRUE;
    Action_data.proto_llc_valid = MEA_TRUE;  /* force protocol and llc */
    Action_data.Protocol = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
    Action_data.Llc = MEA_FALSE;


    Action_editing.num_of_entries = 1;               /* Number of the entries in the Editing table */
    Action_editing.ehp_info = &Action_editing_info;


    if (vlan == 0) {
        Action_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
    }
    else {
        if (vlan == EXAMPLE5_DEFAULT_VLAN) {
            Action_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        }
        else {
            Action_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        }
    }
    if (pServiceIn->port_id == EXAMPLE5_CPU_PORT) {
        Action_editing_info.ehp_data.EditingType = MEA_EDITINGTYPE_APPEND_APPEND;
        Action_editing_info.ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        Action_editing_info.ehp_data.atm_info.val.double_vlan_tag.eth_type = 0x8100;
        Action_editing_info.ehp_data.atm_info.val.double_vlan_tag.vid = EXAMPLE5_ACT_CPU_QinQ_VLAN;

        Action_editing_info.ehp_data.eth_info.val.vlan.vid = 0;
        Action_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        Action_editing_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
        Action_editing_info.ehp_data.eth_info.val.vlan.vid = 0;
        Action_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;

        Action_editing_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
        Action_editing_info.ehp_data.eth_info.stamp_priority = MEA_TRUE;
        Action_editing_info.ehp_data.eth_info.stamp_color = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.color_bit0_loc = 12;
        Action_editing_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
        Action_editing_info.ehp_data.eth_stamping_info.color_bit1_loc = 0;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_valid = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_loc = 13;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_valid = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_loc = 14;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_valid = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_loc = 15;

    }
    else {
        Action_editing_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
        Action_editing_info.ehp_data.eth_info.val.vlan.vid = add_vlan;
        Action_editing_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
        Action_editing_info.ehp_data.eth_info.stamp_priority = MEA_TRUE;
        Action_editing_info.ehp_data.eth_info.stamp_color = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.color_bit0_loc = 12;
        Action_editing_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
        Action_editing_info.ehp_data.eth_stamping_info.color_bit1_loc = 0;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_valid = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_loc = 13;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_valid = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_loc = 14;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_valid = MEA_TRUE;
        Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_loc = 15;
    }

    MEA_SET_OUTPORT(&Action_outPorts, pServiceIn->port_id);
    MEA_SET_OUTPORT(&Action_editing_info.output_info, pServiceIn->port_id);

    if (learning_enable == MEA_TRUE) {
        if (MEA_API_Create_Action(MEA_UNIT_0,
            &Action_data,
            &Action_outPorts,
            &Service_policer,
            &Action_editing,
            &pServiceIn->action_id) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_API_Create_Action failed line:%d\r\n", __LINE__);
            return MEA_ERROR;
        }
    }

    Service_key.src_port = pServiceIn->port_id;
    if (vlan == EXAMPLE5_DEFAULT_VLAN)
    {
        Service_key.net_tag = 0x10000 | pServiceIn->port_id;
        Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_DEF_SID;
        Service_data.SRV_mode = 1;
    }
    else
    {
        if (vlan != 0) {

            Service_key.net_tag = (0xff000 | vlan);
            Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Ctag;
        }
        else {
            Service_key.net_tag = (0xff000 | vlan);
            Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;
        }
    }

    /* Made the service priority-unaware (default priority) */
    Service_key.evc_enable = MEA_FALSE; //MEA_TRUE;
    Service_key.pri = 7;


    /* Build the upstream service data attributes for the forwarder  */
    Service_data.DSE_forwarding_enable = forwarding_enable;
    if (forwarding_enable || learning_enable)
        Service_data.vpn = 100;
    else
        Service_data.vpn = 0;



    Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    Service_data.DSE_learning_enable = learning_enable;
    Service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    Service_data.DSE_learning_actionId = (learning_enable == MEA_TRUE) ? pServiceIn->action_id : 0;
    Service_data.DSE_learning_actionId_valid = MEA_TRUE;
    Service_data.DSE_learning_srcPort = pServiceIn->port_id;
    Service_data.DSE_learning_srcPort_force = (learning_enable == MEA_TRUE) ? MEA_TRUE : MEA_FALSE;
    if (forwarding_enable || learning_enable)
    {
        Service_data.UC_NotAllowed = MEA_FALSE;
        Service_data.MC_NotAllowed = MEA_TRUE;
        Service_data.BC_NotAllowed = MEA_FALSE;
   }


	/* Set the upstream service data attribute for policing */
	Service_data.tmId   = 0; /* generate new id */
	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

	/* Set the upstream service data attribute for pm (counters) */
	Service_data.pmId   = 0; /* generate new id */

	/* Set the upstream service data attribute for policing */
	Service_data.editId = 0; /* generate new id */

	/* Set the other upstream service data attributes to defaults */
	Service_data.ADM_ENA            = MEA_FALSE;
	Service_data.CM                 = MEA_FALSE;
	Service_data.L2_PRI_FORCE       = MEA_FALSE;
	Service_data.L2_PRI             = 0;
	Service_data.COLOR_FORSE        = MEA_TRUE;
	Service_data.COLOR              = 2;
	Service_data.COS_FORCE          = MEA_FALSE;
	Service_data.COS                = 0;
	Service_data.protocol_llc_force = MEA_TRUE;
	Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
	Service_data.Llc                = MEA_FALSE;


	Service_editing.ehp_info       = &Service_editing_info[0];
	Service_editing.num_of_entries = 1;



	Service_editing_info[0].ehp_data.eth_info.stamp_color       = MEA_FALSE;
    Service_editing_info[0].ehp_data.eth_info.stamp_priority    = MEA_FALSE;
    if (vlan == 0) {
        Service_editing_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        Service_editing_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
        Service_editing_info[0].ehp_data.eth_info.val.vlan.vid = add_vlan;
    }
    else {
        if (vlan == EXAMPLE5_DEFAULT_VLAN) {
            Service_editing_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;  //MEA_EGRESS_HEADER_PROC_CMD_EXTRACT; MEA_EGRESS_HEADER_PROC_CMD_APPEND
        }
        else {
            Service_editing_info[0].ehp_data.eth_info.command = TYPE;  //MEA_EGRESS_HEADER_PROC_CMD_EXTRACT; MEA_EGRESS_HEADER_PROC_CMD_APPEND
            Service_editing_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
            Service_editing_info[0].ehp_data.eth_info.val.vlan.vid = add_vlan;
        }
      }
    
    Service_editing_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
    Service_editing_info[0].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
    Service_editing_info[0].ehp_data.eth_info.stamp_color               = MEA_TRUE;
    Service_editing_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
    Service_editing_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    Service_editing_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
    Service_editing_info[0].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
    Service_editing_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    Service_editing_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    Service_editing_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    Service_editing_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    Service_editing_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
    Service_editing_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;


	/* send to broadcast output ports */
	for(idx=0;idx<num_of_outport;idx++)
	{
        if (pPort[idx] == EXAMPLE5_CPU_PORT) {
            en_OneOfOUT_Is_CPU = MEA_TRUE;
            MEA_SET_OUTPORT(&Service_outPorts, pPort[idx]);
           continue;
        }
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"join port number:%d line:%d\r\n",pPort[idx],__LINE__);
		MEA_SET_OUTPORT(&Service_outPorts, pPort[idx]);
		MEA_SET_OUTPORT(&Service_editing_info[0].output_info,pPort[idx]);
	}
    /*if one of outport id CPU port */
    if (en_OneOfOUT_Is_CPU == MEA_TRUE) {
        MEA_SET_OUTPORT(&Service_editing_info[1].output_info, EXAMPLE5_CPU_PORT);
        Service_editing.num_of_entries = 2;

        Service_editing_info[1].ehp_data.EditingType = MEA_EDITINGTYPE_APPEND_APPEND;
        Service_editing_info[1].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        Service_editing_info[1].ehp_data.atm_info.val.double_vlan_tag.eth_type = 0x8100;
        Service_editing_info[1].ehp_data.atm_info.val.double_vlan_tag.vid = EXAMPLE5_ACT_CPU_QinQ_VLAN;

        Service_editing_info[1].ehp_data.eth_info.val.vlan.vid = 0;
        Service_editing_info[1].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
        Service_editing_info[1].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
        Service_editing_info[1].ehp_data.eth_info.val.vlan.vid = 0;
        Service_editing_info[1].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;

        Service_editing_info[1].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
        Service_editing_info[1].ehp_data.eth_info.stamp_priority = MEA_TRUE;
        Service_editing_info[1].ehp_data.eth_info.stamp_color = MEA_TRUE;
        Service_editing_info[1].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
        Service_editing_info[1].ehp_data.eth_stamping_info.color_bit0_loc = 12;
        Service_editing_info[1].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
        Service_editing_info[1].ehp_data.eth_stamping_info.color_bit1_loc = 0;
        Service_editing_info[1].ehp_data.eth_stamping_info.pri_bit0_valid = MEA_TRUE;
        Service_editing_info[1].ehp_data.eth_stamping_info.pri_bit0_loc = 13;
        Service_editing_info[1].ehp_data.eth_stamping_info.pri_bit1_valid = MEA_TRUE;
        Service_editing_info[1].ehp_data.eth_stamping_info.pri_bit1_loc = 14;
        Service_editing_info[1].ehp_data.eth_stamping_info.pri_bit2_valid = MEA_TRUE;
        Service_editing_info[1].ehp_data.eth_stamping_info.pri_bit2_loc = 15;



    }


	/* Attach the LxCp to the service */
	Service_data.LxCp_enable = 1;
	Service_data.LxCp_Id = pServiceIn->lxcp_Id;

   

	/* Create new service for the given Vlan. */
	if (MEA_API_Create_Service(MEA_UNIT_0,
							   &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &pServiceIn->service_id) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Create_Service failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	if(vlan == EXAMPLE5_DEFAULT_VLAN)
	{

		if(setDefaultService (pServiceIn->port_id, pServiceIn->service_id)!= MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"setDefaultService failed line:%d\r\n",__LINE__);
			return MEA_ERROR;
		}

		DefSIDentry.def_sid = pServiceIn->service_id;
		DefSIDentry.action  = 2;
		DefSIDentry.valid   = MEA_TRUE;

		if(MEA_API_Set_IngressPort_Default_PDUs_Sid(MEA_UNIT_0, pServiceIn->port_id, MEA_DEF_PDUsType_L2CP, &DefSIDentry) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Set_IngressPort_Default_PDUs_Sid failed line:%d\r\n",__LINE__);
			return MEA_ERROR;
		}
	}

	return MEA_OK;

}
/*************************************************************************
 *                                                                        *
 *    Function Name        : MEA_Example5_configLxcpParamaters            *
 *                                                                        *
 *    Description          : This function configure lxcp                 *
 *                           configuration                                *
 *                                                                        *
 *    Input(s)             : opcode - type of protocol type               *
 *                           action - send the traffic to                 *
 *                           lxcp_Id - profile id                         *
 *                           pAction_id - special case edit packet        *
 *                           pAction_id - send to output port             *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : MEA_OK if succeeded, otherwise            *
 *                              MEA_ERROR                                 *
 *                                                                        *
 **************************************************************************/

static MEA_Status MEA_Example5_configLxcpParamaters(MEA_LxCP_Protocol_te opcode,MEA_LxCP_Protocol_Action_te action,MEA_LxCp_t lxcp_Id,MEA_Action_t *pAction_id,MEA_OutPorts_Entry_dbt *pAction_outPorts)
{
    MEA_LxCP_Protocol_key_dbt              LxCp_key;
    MEA_LxCP_Protocol_data_dbt             LxCp_data;


	MEA_OS_memset(&LxCp_key, 0, sizeof(LxCp_key));
	MEA_OS_memset(&LxCp_data, 0, sizeof(LxCp_data));

	LxCp_key.protocol = opcode;

	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, lxcp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Get_LxCP_Protocol failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	// if allready configured with the same then do nothing
	if(LxCp_data.action_type == action)
	{
		return MEA_OK;
	}

	LxCp_data.action_type = action;
	// if need to do special thing
	if(action == MEA_LXCP_PROTOCOL_ACTION_ACTION)
	{
		if( (pAction_id == NULL) || (pAction_outPorts == NULL) )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"pAction_id or pAction_outPorts cannot be NULL");

			return MEA_ERROR;
		}
		LxCp_data.ActionId_valid = MEA_TRUE;
		LxCp_data.ActionId = *pAction_id;
		LxCp_data.OutPorts_valid = MEA_TRUE;
		MEA_OS_memcpy(&LxCp_data.OutPorts,pAction_outPorts,sizeof(LxCp_data.OutPorts));
	}
	else
	{
		LxCp_data.ActionId_valid = MEA_FALSE;
		LxCp_data.OutPorts_valid = MEA_FALSE;
	}

	if(MEA_API_Set_LxCP_Protocol(MEA_UNIT_0, lxcp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_Set_LxCP_Protocol failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	return MEA_OK;

}


static MEA_Status MEA_Example5_Add_MAC_TO_SE(MEA_MacAddr *hotMAC, MEA_Uint16 VPNId, MEA_Port_t port_id)
{

    MEA_SE_Entry_dbt               entry;

    MEA_OS_memset(&entry, 0, sizeof(entry));


    entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    entry.key.mac_plus_vpn.VPN = VPNId;

    MEA_OS_memcpy(&entry.key.mac_plus_vpn.MAC.b[0], hotMAC, sizeof(MEA_MacAddr));

    entry.data.aging = 3;
    entry.data.static_flag = MEA_TRUE;
    entry.data.sa_discard  = MEA_FALSE;
    entry.data.limiterId_valid = MEA_FALSE;
    entry.data.limiterId = 0;
    entry.data.actionId_valid = MEA_TRUE;
    entry.data.actionId = LxcpActionToCpu.Action_Id;

    MEA_SET_OUTPORT(&entry.data.OutPorts, port_id);
    

    if (MEA_API_Create_SE_Entry(MEA_UNIT_0,
            &entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "while create to  SE entry\n");
            return MEA_OK;
    }




    return MEA_OK;
}



/*************************************************************************
 *                                                                        *
 *    Function Name        : MEA_Example5_create_learning_mode_func       *
 *                                                                        *
 *    Description          : This function create full bridging between   *
 *                           uulink port and customer ports               *
 *                                                                        *
 *    Input(s)             : none                                         *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : MEA_OK if succeeded, otherwise            *
 *                              MEA_ERROR                                 *
 *                                                                        *
 **************************************************************************/
static MEA_Status MEA_Example5_create_learning_mode_func(void)
{
	MEA_Uint32 idx=0;
	MEA_OutPorts_Entry_dbt Outlxcp;
    char macAdd[6] = { 0x00,0x0A,0x35,0x00,0x00,0x01 };
    MEA_MacAddr hotMAC;
    

	/* open ingress ports */
	if(MEA_Example5_configIngressAdminON() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_configIngressAdminON failed line:%d\r\n", __LINE__);
		return MEA_ERROR;
	}

	/* open egress ports */
	if(MEA_Example5_configEgressAdminON() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_configEgressAdminON failed line:%d\r\n", __LINE__);
		return MEA_ERROR;
	}

	/* init the database if needed */
	if(MEA_Example5_initialization() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_initialization failed line:%d\r\n", __LINE__);
		return MEA_ERROR;
	}

	/* terminate the database if needed*/
	if(MEA_Example5_cleanup() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_cleanup failed\r\n");
		return MEA_ERROR;
	}

	/* configure the customers, no local bridging forward the traffic only to uplink port */
	for(idx=0;idx<EXAMPLE5_NUM_OF_CUSTOMER_PORTS;idx++)
	{
		if( MEA_Example5_configFullBridging(MEA_TRUE, MEA_TRUE, &customer_service_db[idx],&customer_and_all_ports[0], 10, EXAMPLE5_DEFAULT_VLAN, MEA_EGRESS_HEADER_PROC_CMD_TRANS, 0) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_configFullBridging failed line:%d\r\n", __LINE__);
			return MEA_ERROR;
		}
	}
    
    /* configure the MNG 31 port send to host and network */
    if (MEA_Example5_configFullBridging(MEA_TRUE, MEA_TRUE, &customer_mng_service_db, &network_and_host_ports[0], EXAMPLE5_NUM_OF_HOST_MNG_PORTS, EXAMPLE5_DEFAULT_VLAN, MEA_EGRESS_HEADER_PROC_CMD_TRANS, 0) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_configFullBridging failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }

    /* configure the  port send to host and network */
    if (MEA_Example5_configFullBridging(MEA_TRUE, MEA_TRUE, 
        &cpu_service_db, 
        &customer_and_all_ports[0], 10, 
        EXAMPLE5_DEFAULT_VLAN, 
        MEA_EGRESS_HEADER_PROC_CMD_TRANS, 0) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_configFullBridging failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }
    /* configure the uplink port send to all customers */
    if (MEA_Example5_configFullBridging(MEA_TRUE, MEA_TRUE, 
        &uplink_service_db, 
        &networkTo_customer_mng_and_host[0], 10, 
        EXAMPLE5_DEFAULT_VLAN, MEA_EGRESS_HEADER_PROC_CMD_TRANS, 0) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_configFullBridging failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }

    MEA_OS_memset(&Outlxcp, 0, sizeof(Outlxcp));
    MEA_SET_OUTPORT(&Outlxcp, EXAMPLE5_CPU_PORT);
    
    /* set LXCP catch dhcp destination server send to cpu */
    if (MEA_Example5_configLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER, MEA_LXCP_PROTOCOL_ACTION_ACTION, customer_service_db[0].lxcp_Id, &LxcpActionToCpu.Action_Id , &Outlxcp) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_configLxcpParamaters failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }

    /* set LXCP catch dhcp client destination send to cpu */
    if (MEA_Example5_configLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_CLIENT, MEA_LXCP_PROTOCOL_ACTION_ACTION, uplink_service_db.lxcp_Id, &LxcpActionToCpu.Action_Id, &Outlxcp) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_Example5_configLxcpParamaters failed line:%d\r\n", __LINE__);
        return MEA_ERROR;
    }

    /*create service from CPU to all port base on qinq 4094 <Vlan chanle> */
    for (idx = 0; idx < EXAMPLE5_NUM_OF_CUSTOMER_MNG_UPLINK_PORTS; idx++)
    {
                                              //(tServiceDb_control_mng *pServiceIn, MEA_Uint16 OuterVlanId, MEA_Uint16 vlanId, MEA_Port_t output_port)
        if (MEA_Example5_configPointToPointCPU_toPort(&cpu_PointToPoint_service_ToChannel_db[idx],
            EXAMPLE5_ACT_CPU_QinQ_VLAN,
            customer_and_MNG_Uplink_ports[idx],
            customer_and_MNG_Uplink_ports[idx]) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "configPointToPointCPU_toPort failed line:%d\r\n", __LINE__);
            return MEA_ERROR;

        }
    
    
    }

    /************************************************************************/
    /* Set static MAC on VPN 100 for CPU MAC                               */
   
        MEA_OS_memcpy(&hotMAC.b[0], &macAdd, sizeof(MEA_MacAddr));
   
    MEA_Example5_Add_MAC_TO_SE(&hotMAC, 100, EXAMPLE5_CPU_PORT);

   
  


    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "learning_mode config Done");
	return MEA_OK;
}


static MEA_Status MEA_Example5_create_fixing_mode_func(void)
{
	MEA_Uint32 idx=0;
	MEA_Port_t portArray[2];
    

	/* open ingress ports */
	if(MEA_Example5_configIngressAdminON() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_configIngressAdminON failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	/* open egress ports */
	if(MEA_Example5_configEgressAdminON() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_configEgressAdminON failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	/* init the databe if needed */
	if(MEA_Example5_initialization() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_initialization failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	/* terminate the database if needed*/
	if(MEA_Example5_cleanup() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_cleanup failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	/* point to point configuration */
	for(idx=0;idx<EXAMPLE5_NUM_OF_CUSTOMER_PORTS;idx++)
	{
		/*Uplink service user to network*/
        if(MEA_Example5_configPointToPoint(&customer_service_db[idx],uplink_service_db.port_id,0, MEA_EGRESS_HEADER_PROC_CMD_APPEND , customer_vlan[idx]) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_configPointToPoint failed line:%d\r\n",__LINE__);
			return MEA_ERROR;
		}

        /*Down Link */
		if(MEA_Example5_configPointToPoint(&uplink_pointToPoint[idx], customer_ports[idx],customer_vlan[idx], MEA_EGRESS_HEADER_PROC_CMD_EXTRACT,customer_service_db[idx].port_id) != MEA_OK)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_configPointToPoint failed line:%d\r\n",__LINE__);
			return MEA_ERROR;
		}
	}
	/* create management VLAN between CPU port , uplink port and customers port 999.1 */
	/* configure the uplink port send to all customers */
	portArray[0] = customer_mng_service_db.port_id;
	if( MEA_Example5_configFullBridging(MEA_FALSE, MEA_FALSE, &uplink_service_db,&portArray[0],1,EXAMPLE5_MANAGEMENT_VLAN, MEA_EGRESS_HEADER_PROC_CMD_TRANS,0) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_configFullBridging failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}



	/* customer 999.1 forward to uplink port */
	portArray[0] = uplink_service_db.port_id;
    portArray[1] = cpu_service_db.port_id;
	if( MEA_Example5_configFullBridging(MEA_FALSE, MEA_FALSE, &customer_mng_service_db,&portArray[0],2,4094, MEA_EGRESS_HEADER_PROC_CMD_TRANS, EXAMPLE5_MANAGEMENT_VLAN) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_configFullBridging failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}


    /*create service from CPU to all port base on qinq 4094 <Vlan chanle> */
    for (idx = 0; idx < EXAMPLE5_NUM_OF_CUSTOMER_MNG_UPLINK_PORTS; idx++)
    {
        //(tServiceDb_control_mng *pServiceIn, MEA_Uint16 OuterVlanId, MEA_Uint16 vlanId, MEA_Port_t output_port)
        if (MEA_Example5_configPointToPointCPU_toPort(&cpu_PointToPoint_service_ToChannel_db[idx],
            EXAMPLE5_ACT_CPU_QinQ_VLAN,
            customer_and_MNG_Uplink_ports[idx],
            customer_and_MNG_Uplink_ports[idx]) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "configPointToPointCPU_toPort failed line:%d\r\n", __LINE__);
            return MEA_ERROR;

        }


    }



	return MEA_OK;
}

/*************************************************************************
 *                                                                        *
 *    Function Name        : MEA_Example5_delete_func                     *
 *                                                                        *
 *    Description          : This function configure lxcp                 *
 *                           configuration                                *
 *                                                                        *
 *    Input(s)             : None.                                        *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Returns                 : MEA_OK if succeeded, otherwise            *
 *                              MEA_ERROR                                 *
 *                                                                        *
 **************************************************************************/

static MEA_Status MEA_Example5_delete_func(void)
{
//	MEA_Uint32 idx=0;

	/* open ingress ports */
	if(MEA_Example5_configIngressAdminON() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_configFullBridging failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	/* open egress ports */
	if(MEA_Example5_configEgressAdminON() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_configEgressAdminON failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	/* init the database if needed */
	MEA_Example5_initialization();

	/* terminate the database if needed*/
	if(MEA_Example5_cleanup() != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_Example5_cleanup failed line:%d\r\n",__LINE__);
		return MEA_ERROR;
	}

	return MEA_OK;
}

static MEA_Status MEA_Example5_learning_mode_func(int argc, char *argv[])
{
	MEA_Status ret = MEA_ERROR;
    int i;
    if ((argc < 2) )
    {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"num of argument receive:%d\r\n",argc);
        return ret;
    }

    for (i = 1; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "create"))
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "create_learning_mode\r\n");
            ret = MEA_Example5_create_learning_mode_func();
            
        }
        if (!MEA_OS_strcmp(argv[i], "delete") )
        {
            
            ret = MEA_Example5_delete_func();
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Delete done\r\n");
        }
    }

    return ret;
}

MEA_Status MEA_Example5_fixing_mode_func(int argc, char *argv[])
{
	MEA_Status ret = MEA_ERROR;
    int i;
    if (argc < 2)
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"num of argument receive:%d\r\n",argc);
        return ret;
    }

    for (i = 1; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "create"))
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "create_learning_mode\r\n");
            ret = MEA_Example5_create_fixing_mode_func();
        }
        if (!MEA_OS_strcmp(argv[i], "delete") )
        {
            ret = MEA_Example5_delete_func();
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Delete done\r\n");
        }
    }

    return ret;
}

MEA_Status MEA_Example5_CLI_Init() {


   CLI_defineCmd("MEA example learning mode config",
	   (CmdFunc)MEA_Example5_learning_mode_func,
                 "Example learning configuration \n",
                 "Usage: config \n"
                 "                 "
                 "----------------------------------\n"
                 "   create/delete learning mode entities  \n"
                 "----------------------------------\n"
                 "Examples : - create all      \n"
                 "           - delete all      \n");


   CLI_defineCmd("MEA example fixing mode config",
			    (CmdFunc)MEA_Example5_fixing_mode_func,
                  "Example fixing configuration \n",
                  "Usage: config \n"
                  "                 "
                  "----------------------------------\n"
                  "       all - create all entities  \n"
                  "----------------------------------\n"
                  "Examples : - create  all     \n"
                  "           - delete all      \n");

  

    return MEA_OK;
}
#endif
