/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


/*-------------------------------- Includes ------------------------------------------*/
#ifdef MEA_OS_ADVA_TDM
#ifndef MEA_OS_LINUX
#define MEA_OS_LINUX
#endif
#endif

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)

#include <sys/types.h>
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>  
//#include <linux/in.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
//#include <linux/if.h>
#include <net/if.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/ioctl.h>
#include <errno.h>




#include <fcntl.h>  /* O_RDWR */
#include <sys/ioctl.h> /* ioctl() */

/* includes for struct ifreq, etc */
#include <sys/socket.h>
//#include <linux/if.h>
#include <net/if.h>
#include <linux/if_tun.h>
#endif /* MEA_OS_LINUX */




#include "mea_api.h"
#include "mea_cpu_port_drv.h"
#include "mea_drv_common.h"

/*-------------------------------- Local Variables -----------------------------------*/
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
static pthread_t          MEA_cpu_port_drv_rxThread_id;
static int                MEA_cpu_port_drv_sock;
static struct sockaddr_ll MEA_cpu_port_drv_from,MEA_cpu_port_drv_to;
static struct ifreq       MEA_cpu_port_drv_ifr;
static MEA_CPU_frame_t    MEA_cpu_port_drv_rx_buff;
#endif /* MEA_OS_LINUX */

static MEA_Bool           MEA_cpu_port_drv_ShowCPUFrames=MEA_FALSE;
#ifdef LPM_ADAPTOR_WANTED
char 		*tapdev = "/dev/net/tun";
char 		*tapif = "lpm";
int		i4TapFd = 0;
#endif

static MEA_CpuPort_InputHookFunc MEA_cpu_port_drv_InputHookFunc_Table[MEA_MAX_NUMBER_OF_CPU_PORT_INPUT_HOOK_FUNC];


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_cpu_port_drv_rxThread_func>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
void * mea_drv_cpu_port_drv_rxThread_func(void* arg)
{

#if !defined(MEA_OS_ETH_PROJ_2)&& !defined(MEA_OS_AOP) 
	int len;
    int i;
    MEA_CPU_frame_t* cpu_frame;
    MEA_Port_t       src_port=0;
    MEA_Uint32       vsp=0;
    unsigned char*   ptr;
	MEA_Uint32       countErr=0;
    
#ifdef LPM_ADAPTOR_WANTED
    struct ifreq        IfReq;
    char                au1Cmd[100];
    unsigned char       buf[2000];

    i4TapFd = open (tapdev, O_RDWR);
    if (i4TapFd == -1)
    {
        return NULL;
    }

    memset (&IfReq, 0, sizeof (IfReq));
    IfReq.ifr_flags = IFF_NO_PI;
    IfReq.ifr_flags |= IFF_TAP;
    strncpy (IfReq.ifr_name, tapif, (IFNAMSIZ-1));
    if ((ioctl (i4TapFd, TUNSETIFF, (void *) &IfReq)) < 0)
    {
        close (i4TapFd);
        return NULL;
    }

    memset (au1Cmd, 0, sizeof (au1Cmd));
    sprintf(au1Cmd, "ifconfig %s up", tapif);
    system(au1Cmd);
#endif

    cpu_frame = &MEA_cpu_port_drv_rx_buff;

    while(1) {

        /* receive packet from the raw ethernet socket */
        len = recvfrom(MEA_cpu_port_drv_sock,
                       (char*)(&(cpu_frame->data[0])),
                       sizeof(*cpu_frame),
                       0,
                       NULL,
                       NULL);
        if (len==-1) {
            if(countErr == 1000){
			countErr=0;	
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - recvfrom failed (errno=%d '%s')\n",
                          __FUNCTION__, 
                          errno,
                          strerror(errno));
			countErr=0;			  
			}else{ countErr++; }
			
            continue;
		}


        


		

        /* debug printing */
        if (MEA_cpu_port_drv_ShowCPUFrames) {


                MEA_OS_printf("Packet received (len=%d) \n",
                       len);


		        for (i=0,ptr=(unsigned char*)&(cpu_frame->data[0]);i<len;i++,ptr++) {
					
                    MEA_OS_printf ("%02x ",*ptr);
                    if ((i+1) % 16 == 0) {
                        MEA_OS_printf("\n");
                    }
                }
                MEA_OS_printf("\n");
        }
#ifdef LPM_ADAPTOR_WANTED
		memcpy (buf, cpu_frame->data, len);
		if(buf[12]==0x91 && buf[13]==0x00 &&
		   buf[16]==0x81 && buf[17]==0x00 &&
		   buf[14]==0xf && buf[15]==0xac)
		{
			write (i4TapFd, cpu_frame->data, len);
		}
#endif

        /* handle Input Hook Functions */
        for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_cpu_port_drv_InputHookFunc_Table);i++) {
            if ((MEA_cpu_port_drv_InputHookFunc_Table[i] != NULL) &&
                (len > sizeof(cpu_frame->data)) && 
                (MEA_cpu_port_drv_InputHookFunc_Table[i]
                               (MEA_UNIT_0,
                                src_port,
                                vsp,
                                cpu_frame->data,
                                len) == MEA_TRUE)) {
                break;
            }
        }
	}
#endif

	return NULL;


}

#endif /* MEA_OS_LINUX */


/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_cpu_eth_if>                                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status mea_drv_Init_cpu_port_drv(void)
{


    /* set all InputHookFunc to NULL */
    MEA_OS_memset(&(MEA_cpu_port_drv_InputHookFunc_Table[0]),
                  0,
                  sizeof(MEA_cpu_port_drv_InputHookFunc_Table));

    MEA_OS_memset(&(device_cpu127), 0, sizeof(device_cpu127));


#ifndef LINUX_PC

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)

    /* create socket */
	if ((MEA_cpu_port_drv_sock=socket(PF_PACKET, 
                                      SOCK_RAW, 
                                      htons(ETH_P_ALL)))<0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - socket creation failed (errno=%d '%s')\n",
                          __FUNCTION__, 
                          errno,
                          strerror(errno));
		return MEA_ERROR;
	}
	
#if  !defined(HW_BOARD_IS_PCI)
    /* get the ifreq struct for our interface */
	strncpy(MEA_cpu_port_drv_ifr.ifr_name, 
            MEA_PLATFORM_CPU_PORT_INTERFACE_NAME, 
            sizeof(MEA_cpu_port_drv_ifr.ifr_name));
#else
    if(MEA_device_environment_info.MEA_DEVICE_CPU_ETH_enable == MEA_TRUE){
        //MEA_OS_strcpy(&device_cpu127[0],&MEA_device_environment_info.MEA_DEVICE_CPU_ETH[0]);
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"The device_cpu127 %s   \n",device_cpu127);

        strncpy(MEA_cpu_port_drv_ifr.ifr_name, 
            &MEA_device_environment_info.MEA_DEVICE_CPU_ETH[0], /*&device_cpu127[0],*/ 
            sizeof(MEA_cpu_port_drv_ifr.ifr_name));
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"------ ifr_name %s -----\n",MEA_cpu_port_drv_ifr.ifr_name);

    }else{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"The cpu is not set the ethX  use the MEA_DEVICE_CPU_ETH\n");
        return MEA_ERROR;
    }
    

#endif // !
   


	if (ioctl(MEA_cpu_port_drv_sock, 
              SIOCGIFINDEX, 
              &(MEA_cpu_port_drv_ifr)) == -1) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - SIOCGIFINDEX on %d socket (ifname='%s') failed \n",
                          __FUNCTION__,
                          MEA_cpu_port_drv_sock,
                          MEA_cpu_port_drv_ifr.ifr_name);
		return MEA_ERROR;
	}

    /* create bind info for input */
	MEA_OS_memset(&(MEA_cpu_port_drv_from),'\0',sizeof(MEA_cpu_port_drv_from));
	MEA_cpu_port_drv_from.sll_family   = AF_PACKET;
	MEA_cpu_port_drv_from.sll_protocol = htons(ETH_P_ALL);
	MEA_cpu_port_drv_from.sll_ifindex  = MEA_cpu_port_drv_ifr.ifr_ifindex;

    /* make bind for incoming packets */ 	
	if (bind(MEA_cpu_port_drv_sock,
             (struct sockaddr *)&(MEA_cpu_port_drv_from), 
             sizeof(MEA_cpu_port_drv_from)) <0)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s bind failed\n",__FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Bind succeeds\n");
	
    /* create bind info for output */
	MEA_OS_memset(&(MEA_cpu_port_drv_to) , 0,sizeof(MEA_cpu_port_drv_to));
	MEA_cpu_port_drv_to.sll_family   = AF_PACKET;
	MEA_cpu_port_drv_to.sll_protocol = htons(ETH_P_ALL);
	MEA_cpu_port_drv_to.sll_ifindex  = MEA_cpu_port_drv_ifr.ifr_ifindex;
	MEA_cpu_port_drv_to.sll_halen    = ETH_ALEN;

    /* create thread that will wait for packets */
	if (pthread_create (&MEA_cpu_port_drv_rxThread_id,
                        NULL,
                        mea_drv_cpu_port_drv_rxThread_func,
                        NULL)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet monitor thread creation failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

#endif /* MEA_OS_LINUX */

#endif

    /* return to caller */
    return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_CpuPort_FramesShowFlag>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_CpuPort_FramesShowFlag(MEA_Unit_t unit,
                                              MEA_Bool   flag) 
{
   MEA_cpu_port_drv_ShowCPUFrames=flag;
   return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_CpuPort_FramesShowFlag>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_CpuPort_FramesShowFlag(MEA_Unit_t unit,
                                              MEA_Bool   *flag) 
{
   *flag = MEA_cpu_port_drv_ShowCPUFrames;
   return MEA_OK;
}


 


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_CpuPort_SendPakcet>                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_CpuPort_SendPakcet(MEA_Unit_t unit,
                                          MEA_Port_t dst_port,
                                          MEA_Uint32 net_tag,
                                          char*      packet,
                                          MEA_Uint32 len) 
{


    MEA_Uint32                 i;
    MEA_CPU_frame_t*  cpu_frame=NULL;
    unsigned char* ptr;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (len < 12) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - len (%d) is less the minimum value (12=DA+SA) \n",
                          __FUNCTION__,len);
        return MEA_ERROR;
    }

    /* Check for valid packet parameter */
    if (packet == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - packet==NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* calculate the frame len */
    if (len < 60) {
       len = 60;
    }

    if (len > MEA_CPU_MAX_DATA_SIZE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - len (%d) > max available len (%d) \n",
                         __FUNCTION__,len,MEA_CPU_MAX_DATA_SIZE);
    }

    cpu_frame = MEA_OS_malloc(MEA_CPU_INFO_SIZE+len);

    if (cpu_frame == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - cpu_frame allocate failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    

   // MEA_OS_memset((cpu_frame),0,sizeof(*cpu_frame));
    
    
    /* copy Da and Sa */
	MEA_OS_memcpy(&(cpu_frame->data),packet,len); 
	/* set ethertype and port */
	
	

    
    /* print the packet for debugging */
    if (MEA_cpu_port_drv_ShowCPUFrames) {
#ifdef ARCH64
            MEA_OS_printf("Packet send (len=%ld) \n",len);
#else    
            MEA_OS_printf("Packet send (len=%d) \n",len);
#endif

           MEA_OS_printf("\n");
	    

            MEA_OS_printf("Packet:\n");
	        for (i=0,ptr=(unsigned char*)&(cpu_frame->data[0]);i<len;i++,ptr++) {
				
                MEA_OS_printf ("%02x ",*ptr);
                if ((i+1) % 16 == 0) {
                    MEA_OS_printf("\n");
                }
            }
            MEA_OS_printf("\n");  
    }

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
    MEA_OS_printf(".");
    /* send the packet on the raw socket */
    if (sendto(MEA_cpu_port_drv_sock,
               cpu_frame,
               len,
               0x0,
               (struct sockaddr *)&MEA_cpu_port_drv_to,
               sizeof(MEA_cpu_port_drv_to)) < 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - sendto failed (errno=%d '%s')\n",
                          __FUNCTION__, 
                          errno, strerror(errno));
        
        MEA_OS_printf("Done1:\n"); 
        if (cpu_frame) 
            MEA_OS_free(cpu_frame);
        return MEA_ERROR;
    }
    

#else /* MEA_OS_LINUX */
    
   
#endif
    MEA_OS_printf("Done3:\n");
    if(cpu_frame)
        MEA_OS_free(cpu_frame);
    
    
    return MEA_OK;

}







/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_CpuPort_AddInputHookFunc>                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_CpuPort_AddInputHookFunc(MEA_Unit_t unit,
                                                MEA_CpuPort_InputHookFunc rx_hook_func) 
{


	MEA_Uint32 i;	


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (rx_hook_func == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - rx_hook_func==NULL\n",
                           __FUNCTION__);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



    for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_cpu_port_drv_InputHookFunc_Table);i++) {
        if (MEA_cpu_port_drv_InputHookFunc_Table[i] == rx_hook_func) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - the rx_hook_func (%p) already register\n", 
                              __FUNCTION__,rx_hook_func);
            return MEA_ERROR;
        } else {
            if (MEA_cpu_port_drv_InputHookFunc_Table[i] == NULL) {
               MEA_cpu_port_drv_InputHookFunc_Table[i] = rx_hook_func;
               return MEA_OK;
            }  
         }
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - no free entry for InputHookFunc \n",
                      __FUNCTION__);
    return MEA_ERROR;



}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_CpuPort_DelInputHookFunc>                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_CpuPort_DelInputHookFunc(MEA_Unit_t unit,
                                                MEA_CpuPort_InputHookFunc rx_hook_func)

{

    MEA_Uint32 i;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (rx_hook_func == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - rx_hook_func==NULL\n",
                           __FUNCTION__);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* search for free entry */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_cpu_port_drv_InputHookFunc_Table);i++) {
        if (MEA_cpu_port_drv_InputHookFunc_Table[i] == rx_hook_func) {
            MEA_cpu_port_drv_InputHookFunc_Table[i] = NULL;
            return MEA_OK;
        }
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - rx_hook_func  not found in InputHookFunc Table \n",
                      __FUNCTION__);
    return MEA_ERROR;

}







