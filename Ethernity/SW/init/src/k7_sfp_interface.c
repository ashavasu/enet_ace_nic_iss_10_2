#if defined(MEA_OS_LINUX) && defined(MEA_ETHERNITY) && (defined(MEA_ENV_S1_K7) || defined(MEA_PLAT_S1_VDSL_K7))
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <signal.h>
#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_drv_common.h"
#include "k7_i2c_dev.h"
#include "k7_sfp_interface.h"



MEA_Status MEA_ConfigureInterface_Speed(MEA_Interface_t      id, MEA_Interface_Entry_dbt *entry, MEA_Interface_Entry_dbt *old_entry,int userConfig);

void mea_update_interface_config_from_sfp(MEA_Interface_t  Id,MEA_Interface_Entry_dbt *pEntry);


#ifndef MEA_PLAT_S1_VDSL_K7
#define E2PROM_DEVICE 	"/dev/i2c0"
#else
#define E2PROM_DEVICE 	"/dev/i2c-1"
#endif

#define MDIO_DEVICE 	"/dev/tool_mdio"


static int gI2c_device=0;
static int gMdio_device=0;

static tSfpInterfaceDb MEA_SfpInterfaceDb[MEA_MAX_PORT_NUMBER+1];

const char *vendorIdName_support[]=
{
	"Methode Elec",
};

const char *vendorIdPartNumber_support[]=
{
	"DM7041-R",
};



const char *i2c_reset_command[]=
{
	"/home/application/utility/Xtest R0 0 &>/dev/null",
	"/home/application/utility/Xtest R0 1 &>/dev/null",
	"/home/application/utility/Xtest N0 0 &>/dev/null",
	"/home/application/utility/Xtest N0 1 &>/dev/null",
};

const char *mdio_reset_command[]=
{
	"/home/application/utility/Xtest 0xF8900000 w 0x00 0xff &>/dev/null",
	"/home/application/utility/Xtest 0xF8900000 w 0x00 0x00 &>/dev/null",
};

#if 0

const char *i2c_interface_toggle[]=
{
	"/home/application/utility/pca9548a.out write 0x70 0x00 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x70 0x01 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x70 0x02 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x70 0x04 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x70 0x08 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x70 0x10 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x70 0x20 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x70 0x40 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x70 0x80 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x71 0x00 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x71 0x01 &>/dev/null",
	"/home/application/utility/pca9548a.out write 0x71 0x02 &>/dev/null",
};

#endif




#ifndef MEA_PLAT_S1_VDSL_K7

MEA_Status mdio_set_parameters(int mdio_device, int mii_id, int reg, unsigned short val,int page)
{
	mdio_interface_params    mii_params;
	mdio_interface_params    *ptr_mii = &mii_params;
   unsigned int	  error;


   memset(ptr_mii,0x0,sizeof(mdio_interface_params));

   ptr_mii->mii_id 	= mii_id;

   /* PAGE */
   ptr_mii->page	= page;

   /* SPECIFIC REG on SELECTED PAGE */
   ptr_mii->regnum	= reg;

   ptr_mii->value	= val;

   error = ioctl(mdio_device,MDIO_WRITE_PAGE_SPECIFIC,ptr_mii);
   if(error)
   {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Unable to write ioctl to %s err=%d\n",MDIO_DEVICE,strerror(errno));
       return MEA_ERROR;
   }
//   MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mii_id=%d page=%d reg=%d val 0x%x \n",mii_id,page,reg,val);
   return MEA_OK;
}

MEA_Status mdio_get_parameters(int mdio_device, int mii_id, int reg, unsigned short *val,int page)
{
	mdio_interface_params    mii_params;
	mdio_interface_params    *ptr_mii = &mii_params;
   unsigned int	   error;

   memset(ptr_mii,0x0,sizeof(mdio_interface_params));

   ptr_mii->mii_id 	= mii_id;

   /* PAGE */
   ptr_mii->page	= page;

   /* SPECIFIC REG on SELECTED PAGE */
   ptr_mii->regnum	= reg;

   error = ioctl(mdio_device,MDIO_READ_PAGE_SPECIFIC,ptr_mii);
   if(error)
   {

	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Unable to read from ioctl to %s err=%d\n",MDIO_DEVICE,strerror(errno));
	   return MEA_ERROR;
   }

   *val = ptr_mii->value;

   return MEA_OK;
}

#endif
#define MMAP_REG_MDIO_RESET_BASE_ADDRESS   0xF8900000
#define MMAP_MDIO_RESET_REG				  0x04
static MEA_Status k7_mdio_reset(void)
{
   int						mem_fd;
   volatile unsigned char	*mem_addr;

   if((mem_fd = open("/dev/mem",O_RDWR))<0)
   {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: /dev/mem open failed\n");
      return MEA_ERROR;
   }

   mem_addr = (unsigned char *)mmap(NULL, MMAP_MDIO_RESET_REG, (PROT_READ|PROT_WRITE), MAP_SHARED, mem_fd, MMAP_REG_MDIO_RESET_BASE_ADDRESS);
 //  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "FPGA_REG_OFFSET 0x%08x\n",FPGA_REG_OFFSET);
   if((int)mem_addr <0)
   {
      mem_addr=NULL;
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: mmap addr error\n");
	  close(mem_fd);
      return MEA_ERROR;
   }

   (*mem_addr) = 0xff;

   (*mem_addr) = 0x00;


   if(munmap((void *)mem_addr, MMAP_MDIO_RESET_REG)!=0)
   {
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: munmap failed\n");
	  close(mem_fd);
      return MEA_ERROR;
   }

   if(close(mem_fd)!=0)
   {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: close fd failed\n");
	  return MEA_ERROR;
   }

   sleep(1);
   return MEA_OK;
}

#define MMAP_REG_RESET_BASE_ADDRESS   0xF8600000
#define MMAP_RESET_REG				  0x04
static MEA_Status k7_hardware_reset(void)
{
   int						mem_fd;
   volatile unsigned short	*mem_addr;

   if((mem_fd = open("/dev/mem",O_RDWR))<0)
   {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: /dev/mem open failed\n");
      return MEA_ERROR;
   }

   mem_addr = (unsigned short *)mmap(NULL, MMAP_RESET_REG, (PROT_READ|PROT_WRITE), MAP_SHARED, mem_fd, MMAP_REG_RESET_BASE_ADDRESS);
 //  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "FPGA_REG_OFFSET 0x%08x\n",FPGA_REG_OFFSET);
   if((int)mem_addr <0)
   {
      mem_addr=NULL;
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: mmap addr error\n");
	  close(mem_fd);
      return MEA_ERROR;
   }

   (*mem_addr) = 0xa8a8;

   (*mem_addr) = 0xa8e8;

   (*mem_addr) = 0x6868;

   (*mem_addr) = 0x2828;


   if(munmap((void *)mem_addr, MMAP_RESET_REG)!=0)
   {
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: munmap failed\n");
	  close(mem_fd);
      return MEA_ERROR;
   }

   if(close(mem_fd)!=0)
   {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: close fd failed\n");
	  return MEA_ERROR;
   }


   return MEA_OK;
}

#ifdef MEA_PLAT_S1_VDSL_K7

#define MMAP_REG_BASE_ADDRESS         0xE1000000

#define MMAP_REG_RANGE				  0xFF
#define MMAP_BUS_ADDRESS			  0x90




static MEA_Status set_rgmii_bus_config(int bus)
{
   int						mem_fd;
   volatile unsigned char	*mem_addr;
   volatile unsigned char	*busaddr;
   MEA_Uint32  read_data;

   if((mem_fd = open("/dev/mem",O_RDWR))<0)
   {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: /dev/mem open failed\n");
      return MEA_ERROR;
   }

   mem_addr = (unsigned char *)mmap(NULL, MMAP_REG_RANGE, (PROT_READ|PROT_WRITE), MAP_SHARED, mem_fd, MMAP_REG_BASE_ADDRESS);
 //  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "FPGA_REG_OFFSET 0x%08x\n",FPGA_REG_OFFSET);
   if((int)mem_addr <0)
   {
      mem_addr=NULL;
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: mmap addr error\n");
	  close(mem_fd);
      return MEA_ERROR;
   }

   busaddr = &mem_addr[MMAP_BUS_ADDRESS];

   read_data = *busaddr;

   read_data = (read_data & 0xF0) | bus;

   //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"write to address bus:%d \n",read_data);
   *busaddr = read_data;


   if(munmap((void *)mem_addr, MMAP_REG_RANGE)!=0)
   {
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: munmap failed\n");
	  close(mem_fd);
      return MEA_ERROR;
   }

   if(close(mem_fd)!=0)
   {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR: close fd failed\n");
	  return MEA_ERROR;
   }

   return MEA_OK;
}

#endif

static MEA_Status mdio_set_rgmii_configuration(tSfpInterfaceDb *pSfpCfg,int mdiofd)
{
    MEA_Status ret=MEA_OK;
    unsigned short val=0;

#ifdef MEA_PLAT_S1_VDSL_K7
    ret |= set_rgmii_bus_config(pSfpCfg->mdio_address);
#endif

    val = 0x0CE2;
	ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address ,0x14, val ,0x00);

	val = 0x0000;
	ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address ,0x00, val ,0x00);


	switch(pSfpCfg->sfpSpeed)
	{
		case CONFIGURE_1G_FULL_DUPLEX:
			val = 0x8140;
			break;
		case CONFIGURE_100M_FULL_DUPLEX:
			val = 0xA100;
			break;
		case CONFIGURE_10M_FULL_DUPLEX:
			val = 0x8100;
			break;
		case CONFIGURE_AUTONEG:
			val = 0x9140;
			break;
		default:
			break;

	}

	ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address ,0x00, val ,0x00);


#ifdef MEA_PLAT_S1_VDSL_K7
	// return to default
    ret |= set_rgmii_bus_config(0x01);
#endif
	return ret;

}

/*
	./GestioneGbe.out write_page 0x14 0x14 0x8384 0x06
	./GestioneGbe.out write_page 0x14 0x10 0x60A4 0x04
	./GestioneGbe.out write_page 0x14 0x00 0xB100 0x04
	
	./GestioneGbe.out write_page 0x14 0x10 0x468F 0x01
	./GestioneGbe.out write_page 0x14 0x00 0x9340 0x01
	./GestioneGbe.out write_page 0x14 0x00 0x9340 0x00
	./GestioneGbe.out write_page 0x14 0x00 0x9340 0x01
	./GestioneGbe.out write_page 0x14 0x00 0x9340 0x04
*/

static MEA_Status mdio_set_qsgmii_configuration(tSfpInterfaceDb *pSfpCfg,int mdiofd)
{

    MEA_Status ret=MEA_OK;
    unsigned short val=0;

    //set Qsgmii configuration
    val = 0x8384;
    ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x14, val,6);

    val = 0x60A4;
    ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x10, val,4);

    val = 0xB100;
    ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,4);

	// set fiber configuration
	val = 0x468F;
	ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x10, val,0x1);

    if(pSfpCfg->sfpSpeed == CONFIGURE_AUTONEG)
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s configure autoneg %d \n",__FUNCTION__,pSfpCfg->enetinterface);

    	val = 0x8140;
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);

		//set copper configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x0);

		// set fiber configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);

		//set copper configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x0);

		//set Qsgmii configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,4);


    	// set fiber configuration
		val = 0x9340;
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);

		//set copper configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x0);

		// set fiber configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);

		//set copper configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x0);


		//set Qsgmii configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,4);

    }
    else
    {
    	if(pSfpCfg->sfpSpeed == CONFIGURE_1G_FULL_DUPLEX)
    	{
    		//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s configure full 1000m %d \n",__FUNCTION__,pSfpCfg->enetinterface);
    		val = 0x8140;
    	}
    	else if(pSfpCfg->sfpSpeed == CONFIGURE_100M_FULL_DUPLEX)
    	{
    		//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s configure full 100m %d \n",__FUNCTION__,pSfpCfg->enetinterface);
    		val = 0xA100;
    	}
    	else
    	{
    		//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s configure full 10m %d \n",__FUNCTION__,pSfpCfg->enetinterface);
    		val = 0x8100;
    	}
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);

		//set copper configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x0);

		// set fiber configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);

		//set copper configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x0);

		//set Qsgmii configuration
		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,4);

    }
    return ret;
}
static MEA_Status mdio_get_rgmii_status(tSfpInterfaceDb *pSfpCfg,int mdiofd,tSgmiiStatus *pLinkStatus)
{
	unsigned short val;
	 MEA_Status ret=MEA_OK;
	//check copper status

	 memset(pLinkStatus,0,sizeof(tSfpStatus));

#ifdef MEA_PLAT_S1_VDSL_K7
    ret |= set_rgmii_bus_config(pSfpCfg->mdio_address);
#endif

	 ret = mdio_get_parameters(mdiofd,pSfpCfg->mdio_address,0x01,&val,0);
	 if(ret == MEA_OK)
	 {
		 if(val & MDIO_COPPER_LINK_STATUS)
		 {
			 pLinkStatus->isLinkUp = 1;

			 ret = mdio_get_parameters(mdiofd,pSfpCfg->mdio_address,MDIO_COPPER_EXTENDED_STATUS_REGISTER,&val,0);

			 if(val & MDIO_COPPER_STATUS_1G_SPEED)
			 {
				 pLinkStatus->speedStatus = CONFIGURE_1G_FULL_DUPLEX;
			 }
			 else if(val & MDIO_COPPER_STATUS_100M_SPEED)
			 {
				 pLinkStatus->speedStatus = CONFIGURE_100M_FULL_DUPLEX;
			 }
			 else
			 {
				 pLinkStatus->speedStatus = CONFIGURE_10M_FULL_DUPLEX;
			 }

			 if(val & MDIO_COPPER_STATUS_FULL_DUPLEX)
			 {
				 pLinkStatus->isFullDuplex = 1;
			 }
			 //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s interface:%d speed:%d\n",__FUNCTION__,pSfpCfg->enetinterface,pLinkStatus->speedStatus);

		 }
	 }
#ifdef MEA_PLAT_S1_VDSL_K7
	// return to default
    ret |= set_rgmii_bus_config(0x01);
#endif
	 return ret;
}

static MEA_Status mdio_get_qsgmii_status(tSfpInterfaceDb *pSfpCfg,int mdiofd,tSgmiiStatus *pLinkStatus)
{
	unsigned short val;
	 MEA_Status ret=MEA_OK;
	//check copper status

	 memset(pLinkStatus,0,sizeof(tSfpStatus));

	 ret = mdio_get_parameters(mdiofd,pSfpCfg->mdio_address,MDIO_COPPER_STATUS_REGISTER,&val,0);

	 //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s copper interface %d shows: 0x%x ret=%d\n",__FUNCTION__,pSfpCfg->enetinterface,val,ret);

	 if(ret == MEA_OK)
	 {
		 if(val & MDIO_COPPER_LINK_STATUS)
		 {
			 pLinkStatus->isLinkUp = 1;
			 //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s copper interface %d link up 0x%x \n",__FUNCTION__,pSfpCfg->enetinterface,val);

			 ret = mdio_get_parameters(mdiofd,pSfpCfg->mdio_address,MDIO_COPPER_EXTENDED_STATUS_REGISTER,&val,0);

			 if(val & MDIO_COPPER_STATUS_1G_SPEED)
			 {
				 pLinkStatus->speedStatus = CONFIGURE_1G_FULL_DUPLEX;
			 }
			 else if(val & MDIO_COPPER_STATUS_100M_SPEED)
			 {
				 pLinkStatus->speedStatus = CONFIGURE_100M_FULL_DUPLEX;
			 }
			 else
			 {
				 pLinkStatus->speedStatus = CONFIGURE_10M_FULL_DUPLEX;
			 }

			 if(val & MDIO_COPPER_STATUS_FULL_DUPLEX)
			 {
				 pLinkStatus->isFullDuplex = 1;
			 }

			 //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s copper interface %d status 0x%x \n",__FUNCTION__,pSfpCfg->enetinterface,val);
		 }
	 }


	 if(pLinkStatus->isLinkUp == 0)
	 {
		 ret = mdio_get_parameters(mdiofd,pSfpCfg->mdio_address,MDIO_FIBER_STATUS_REGISTER,&val,1);

		 //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s fiber interface %d shows: 0x%x ret=%d\n",__FUNCTION__,pSfpCfg->enetinterface,val,ret);


		 if(ret == MEA_OK)
		 {
			 if(val & MDIO_FIBER_LINK_STATUS)
			 {

				 //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s fiber interface %d link up 0x%x \n",__FUNCTION__,pSfpCfg->enetinterface,val);

				 pLinkStatus->isLinkUp = 1;
				 //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s copper interface %d link up 0x%x \n",__FUNCTION__,pSfpCfg->enetinterface,val);

				 ret = mdio_get_parameters(mdiofd,pSfpCfg->mdio_address,MDIO_FIBER_EXTENDED_STATUS_REGISTER,&val,1);

				 if(val & MDIO_FIBER_STATUS_1G_SPEED)
				 {
					 pLinkStatus->speedStatus = CONFIGURE_1G_FULL_DUPLEX;
				 }
				 else if(val & MDIO_FIBER_STATUS_100M_SPEED)
				 {
					 pLinkStatus->speedStatus = CONFIGURE_100M_FULL_DUPLEX;
				 }
				 else
				 {
					 pLinkStatus->speedStatus = CONFIGURE_10M_FULL_DUPLEX;
				 }

				 if(val & MDIO_FIBER_STATUS_FULL_DUPLEX)
				 {
					 pLinkStatus->isFullDuplex = 1;
				 }

				 //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s fiber interface %d status 0x%x \n",__FUNCTION__,pSfpCfg->enetinterface,val);
			 }
		 }
	 }
	 return ret;
}



#if 0
static MEA_Status mdio_set_autoneg_configuration(tSfpInterfaceDb *pSfpCfg,int mdiofd)
{
    MEA_Status ret=MEA_OK;
    unsigned short val=0;

    val = MDIO_SOFTWARE_RESET | MDIO_PTP_POWER_DOWN | MDIO_PTP_REF_CLOCK | MDIO_PTP_INPUT_SRC | MDIO_MEDIA_QSGMII_TO_SGMII;

    ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,MDIO_GENERAL_CONTROL_REGISTER, val,6);

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mdio_set_autoneg_configuration configure reg=%d\n",MDIO_GENERAL_CONTROL_REGISTER);
    if(pSfpCfg->sfpSpeed != CONFIGURE_AUTONEG){
        val = MDIO_TRANSMIT_FIFO_DEPTH_LOW | MDIO_BLOCK_CARRIER_EXT_BIT | MDIO_FORCE_LINK_GOOD | MDIO_AUTONEG_BYPASS_ENABLE | 0x0025;
    }else{
        val = MDIO_TRANSMIT_FIFO_DEPTH_LOW | MDIO_BLOCK_CARRIER_EXT_BIT | MDIO_ENHANCED_SGMII |  0x0024;
    }

    ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,MDIO_QSGMII_COPPER_SPECIFIC_CONTROL_REGISTER1, val,4);

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mdio_set_autoneg_configuration configure reg=%d\n",MDIO_QSGMII_COPPER_SPECIFIC_CONTROL_REGISTER1);
    if(pSfpCfg->sfpSpeed != CONFIGURE_AUTONEG)
        val = MDIO_QSGMII_SOFTWARE_RESET | MDIO_QSGMII_1000M_SPEED |  0x0100;
    else
        val = MDIO_QSGMII_SOFTWARE_RESET | MDIO_QSGMII_1000M_SPEED | MDIO_QSGMII_AUTONEG_ENABLE |0x0100; 


    ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,MDIO_QSGMII_CONTROL_REGISTER, val,4);

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mdio_set_autoneg_configuration configure reg=%d\n",MDIO_QSGMII_CONTROL_REGISTER);
         val = 0x468F;

    ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x10, val,0x1);
	
	if(pSfpCfg->sfpSpeed != CONFIGURE_AUTONEG){
   


    val = 0x8140;

    ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x01);
    }

    if(pSfpCfg->sfpSpeed == CONFIGURE_AUTONEG)
    {
        val = 0x9340;

        ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);

        val = 0x9340;

        ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x0);

        val = 0x9340;

        ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);

        val = 0x9340;

        ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x4);
    }
    else
     {
     	if(pSfpCfg->sfpSpeed == CONFIGURE_1G_FULL_DUPLEX)
     	{
     		val = 0x8140;
     	}
     	else if(pSfpCfg->sfpSpeed == CONFIGURE_100M_FULL_DUPLEX)
     	{
     		val = 0xA100;
     	}
     	else
     	{
     		val = 0x8100;
     	}
 		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);

 		//set copper configuration
 		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x0);

 		// set fiber configuration
 		ret |= mdio_set_parameters(mdiofd,pSfpCfg->mdio_address,0x00, val,0x1);
     }
	return ret;
}
static MEA_Status  read_qsgmii_status_register(tSfpInterfaceDb *pSfpCfg,int mdiofd)
{
	MEA_Status ret=MEA_OK;
	unsigned short val=0;

	ret = mdio_get_parameters(mdiofd, pSfpCfg->mdio_address,4,1, &val);
	
	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"read_qsgmii_status_register interfaceId=%d reg=%d value=%d\n",pSfpCfg->enetinterface,1,val);
	
	ret = mdio_get_parameters(mdiofd, pSfpCfg->mdio_address,4,4, &val);
	
	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"read_qsgmii_status_register interface=%d reg=%d value=%d\n",pSfpCfg->enetinterface,4,val);	
	
	ret = mdio_get_parameters(mdiofd, pSfpCfg->mdio_address,4,17, &val);
	
	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"read_qsgmii_status_register interface=%d reg=%d value=%d\n",pSfpCfg->enetinterface,17,val);	
	
	return ret;
}
#endif
/*
 * This function clean the sfp database
 */
void mea_init_sfp_configuration(void)
{
	memset(MEA_SfpInterfaceDb,0,sizeof(MEA_SfpInterfaceDb));
#if defined(MEA_PLAT_S1_VDSL_K7)    
    init_socket_connection_vdslk7();
#endif
}
/*
 * this function change the speed according to user request
 */
void mea_sfp_change_speed(MEA_Interface_t Id,eSfpSpeedCfg speed,int adv)
{
	MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s - id %d speed :%d\n",__FUNCTION__, Id,speed);
	if(Id > MEA_MAX_PORT_NUMBER)
	{
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - sfp_change_speed %d failed \n",__FUNCTION__, Id);
		return;
	}
	if(MEA_SfpInterfaceDb[Id].isValidInterface != 1)
	{
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - sfp_change_speed %d not init \n",__FUNCTION__, Id);
		return;
	}

//   if( (MEA_SfpInterfaceDb[Id].sfpSpeed == speed) && (MEA_SfpInterfaceDb[Id].bitmap_advertisement == adv) )
//    {
// 		return; //same configuration
// 	}

//	MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s - sfp_change_speed %d = speed=%d edv=%d \n",__FUNCTION__, Id,speed,adv);
	MEA_SfpInterfaceDb[Id].sfp_state=SFP_STATE_OUT;
	MEA_SfpInterfaceDb[Id].isValidInterface=1;
	MEA_SfpInterfaceDb[Id].config_status_complete=0;
	MEA_SfpInterfaceDb[Id].sfpSpeed = speed;
	MEA_SfpInterfaceDb[Id].bitmap_advertisement = adv;
	MEA_SfpInterfaceDb[Id].lastLinkStatus = LINK_STATUS_DOWN;
    MEA_SfpInterfaceDb[Id].link_down_countetr=0;
    MEA_SfpInterfaceDb[Id].fiber            = 0;
    MEA_SfpInterfaceDb[Id].sfp_prev_state =0xFFFF;
}
/*
 * this function setting sfp configuration at init state
 */
void mea_setting_sfp_interface(MEA_Interface_t Id,MEA_InterfaceType_t interfaceType)
{
	if(Id > MEA_MAX_PORT_NUMBER)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - init_sfp_configuration %d failed \n",__FUNCTION__, Id);
		return;
	}

	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s - init_sfp_configuration %d\n",__FUNCTION__, Id);

#ifdef MEA_PLAT_S1_VDSL_K7
	switch(Id)
	{
	case 24:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII0_0_PHY_ADDR;
		break;
	case 101:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x02;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII0_1_PHY_ADDR;
		break;
	case 48:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x04;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII0_2_PHY_ADDR;
		break;
	case 102:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x08;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII0_3_PHY_ADDR;
		break;
	case 12:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x10;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII1_0_PHY_ADDR;
		break;
	case 0:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x20;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII1_1_PHY_ADDR;
		break;
	case 100:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x40;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII1_2_PHY_ADDR;
		break;
	case 36:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x80;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII1_3_PHY_ADDR;
		break;
	case 104:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C2;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=0x20;
		MEA_SfpInterfaceDb[Id].mdio_address = -1;
		break;
	case 105:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C2;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x02;
		MEA_SfpInterfaceDb[Id].sfp_number=0x10;
		MEA_SfpInterfaceDb[Id].mdio_address = -1;
		break;
	case 125:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = 4;
		break;
	case 108:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = 4;
		break;
	case 98:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x00;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = 0x00;
		break;
	case 110:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = 0x01;
		break;
	case 109:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x00;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = 0x00;
		break;
	case 96:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = 0x01;
		break;
	case 111:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x00;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = 0x01;
		break;
	case 103:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=0;
		MEA_SfpInterfaceDb[Id].mdio_address = 0x00;
		break;
	default:
		break;
	}
#else
//	MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s int id:%d\n",__FUNCTION__, Id);

	/* assign portid to i2c address */
	switch(Id)
	{
	case 100:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=1;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII0_0_PHY_ADDR;
		break;
	case 101:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x02;
		MEA_SfpInterfaceDb[Id].sfp_number=2;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII0_1_PHY_ADDR;
		break;
	case 102:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x04;
		MEA_SfpInterfaceDb[Id].sfp_number=3;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII0_2_PHY_ADDR;
		break;
	case 103:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x08;
		MEA_SfpInterfaceDb[Id].sfp_number=4;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII0_3_PHY_ADDR;
		break;
	case 108:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x10;
		MEA_SfpInterfaceDb[Id].sfp_number=5;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII1_0_PHY_ADDR;
		break;
	case 109:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x20;
		MEA_SfpInterfaceDb[Id].sfp_number=6;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII1_1_PHY_ADDR;
		break;
	case 110:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x40;
		MEA_SfpInterfaceDb[Id].sfp_number=7;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII1_2_PHY_ADDR;
		break;
	case 111:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x80;
		MEA_SfpInterfaceDb[Id].sfp_number=8;
		MEA_SfpInterfaceDb[Id].mdio_address = _QSGMII1_3_PHY_ADDR;
		break;

	case 104:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C2;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x02;
		MEA_SfpInterfaceDb[Id].sfp_number=9;
		MEA_SfpInterfaceDb[Id].mdio_address = -1;
		break;
	case 105:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C2;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=10;
		MEA_SfpInterfaceDb[Id].mdio_address = -1;
		break;
	case 0:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=1;
		MEA_SfpInterfaceDb[Id].mdio_address = _RGMII5_PHY_ADDR;
		break;
	case 12:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=1;
		MEA_SfpInterfaceDb[Id].mdio_address = _RGMII4_PHY_ADDR;
		break;
	case 72:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=1;
		MEA_SfpInterfaceDb[Id].mdio_address = _RGMII0_PHY_ADDR;
		break;
	case 48:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=1;
		MEA_SfpInterfaceDb[Id].mdio_address = _RGMII1_PHY_ADDR;
		break;
	case 36:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=1;
		MEA_SfpInterfaceDb[Id].mdio_address = _RGMII2_PHY_ADDR;
		break;
	case 24:
		MEA_SfpInterfaceDb[Id].toggle_in_interface=SERIAL_ADDRESS_I2C1;
		MEA_SfpInterfaceDb[Id].toggle_in_interface_data=0x01;
		MEA_SfpInterfaceDb[Id].sfp_number=1;
		MEA_SfpInterfaceDb[Id].mdio_address = _RGMII3_PHY_ADDR;
		break;
	default:
		break;
	}
#endif
	MEA_SfpInterfaceDb[Id].sfp_state=SFP_STATE_OUT;
#ifdef MEA_PLAT_S1_VDSL_K7
    if(Id==104 || Id==105) /*Alex */
	   MEA_SfpInterfaceDb[Id].isValidInterface=1;
#else
    MEA_SfpInterfaceDb[Id].isValidInterface=1;
#endif
    MEA_SfpInterfaceDb[Id].config_status_complete=0;
    MEA_SfpInterfaceDb[Id].sfpSpeed = CONFIGURE_AUTONEG;
	MEA_SfpInterfaceDb[Id].lastLinkStatus = LINK_STATUS_DOWN;
	MEA_SfpInterfaceDb[Id].enetinterface = Id;
	MEA_SfpInterfaceDb[Id].interfaceType = interfaceType;
	MEA_SfpInterfaceDb[Id].init_configuration=0;



}
#if 0
/*
 * this static function read 16 bits from i2c
 */
static MEA_Status ReadReg8Bit(int fdeeprom,int regNum,int address ,MEA_Uint8 *value)
{
    int retval=0;
    /* apertura i2c bus */
    if(fdeeprom==-1)
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Unable to open %s, cause: %s\n",E2PROM_DEVICE,strerror(errno));
        return MEA_ERROR;
    }
    else
    {
        if(ioctl(fdeeprom,I2C_SLAVE_FORCE,address)<0)
        {
        	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"ERROR in ioctl on E2PROM.\n");
            return MEA_ERROR;
        }
    }

    retval = i2c_smbus_read_byte_data(fdeeprom,regNum);
    if(retval == -1)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"Error in IC2 write regNum, cause: %s.\n",strerror(errno));
        return MEA_ERROR;

    }
    (*value)= (MEA_Uint8)(retval & 0xff);
	//fprintf(stdout,"succeeded to read date=0x%x from reg=%d\r\n",*value,regNum);
   return MEA_OK;
}
#endif

static MEA_Status ReadReg16Bit(int fdeeprom,int regNum,int address ,short *value)
{
    /* apertura i2c bus */
    short app;
    if(fdeeprom==-1)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Unable to open %s, cause: %s\n",E2PROM_DEVICE,strerror(errno));
        return MEA_ERROR;
    }
    else
    {
        if(ioctl(fdeeprom,I2C_SLAVE_FORCE,address)<0)
        {
            //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"ERROR in ioctl on E2PROM.\n");
            return MEA_ERROR;
        }
    }

    app = i2c_smbus_read_word_data(fdeeprom,regNum);
    if(app == -1)
    {
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"Error in IC2 write regNum, cause: %s.\n",strerror(errno));
        return MEA_ERROR;

    }

    (*value) = ((app & 0xFF00)>>8) | ((app & 0x00FF)<<8);

    //fprintf(stdout,"succeeded to read date=0x%x from reg=%d\r\n",*value,regNum);
    return MEA_OK;
}

/*
 * this static function write 8 bits to i2c
 */
static MEA_Status WriteReg8Bit(int fdeeprom,int regNum,int address ,unsigned char value)
{
    /* apertura i2c bus */
    if(fdeeprom==-1)
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Unable to open %s, cause: %s\n",E2PROM_DEVICE,strerror(errno));
        return MEA_ERROR;
    }
    else
    {
        if(ioctl(fdeeprom,I2C_SLAVE_FORCE,address)<0)
        {
        	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"ERROR in ioctl on E2PROM.\n");
            return MEA_ERROR;
        }
    }
   if(i2c_smbus_write_byte_data(fdeeprom,regNum,value)==-1)
   {
	   	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"Error in IC2 write regNum, cause: %s.\n",strerror(errno));
		return MEA_ERROR;

	}
	//fprintf(stdout,"succeeded to write date=0x%x to reg=%d\r\n",value,regNum);
   return MEA_OK;
}
/*
 * this static function write 16 bits to i2c
 */
static MEA_Status WriteReg16Bit(int fdeeprom,int regNum,int address ,unsigned short value)
{
    unsigned char lsb,msb;
    unsigned short app;
    /* apertura i2c bus */
    if(fdeeprom==-1)
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Unable to open %s, cause: %s\n",E2PROM_DEVICE,strerror(errno));
        return MEA_ERROR;
    }
    else
    {
        if(ioctl(fdeeprom,I2C_SLAVE_FORCE,address)<0)
        {
        	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"ERROR in ioctl on E2PROM.\n");
            return MEA_ERROR;
        }
    }

   msb = (value & 0xFF00) >> 8;
   lsb = (value & 0x00FF);

   app = (lsb << 8 ) | msb;
   if(i2c_smbus_write_word_data(fdeeprom,regNum,app)==-1)
   {
	  // MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"Error in IC2 write regNum, cause: %s.\n",strerror(errno));
		return MEA_ERROR;

	}
 //  MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"WriteReg16Bit 0x%02x 0x%02x 0x%04x\n",regNum,address , value);
	
   return MEA_OK;
}
/*
 * this function read buffer from i2c
 */
static MEA_Status ReadI2cBuffer(int fdeeprom,int address,int base_addr,unsigned char *buff,int len)
{
    if(fdeeprom==-1)
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Unable to open %s, cause: %s\n",E2PROM_DEVICE,strerror(errno));
        return MEA_ERROR;
    }
    else
    {
        if(ioctl(fdeeprom,I2C_SLAVE_FORCE,address)<0)
        {
        	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"ERROR in ioctl on E2PROM.\n");
            return MEA_ERROR;
        }
    }
	memset(buff,0,len);
	if(i2c_smbus_read_i2c_block_data(fdeeprom,base_addr,len,buff)==-1)
	{
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"Error in IC2 read regNum, cause: %s.\n",strerror(errno));
		return MEA_ERROR;
	}

   //fprintf(stdout,"succeeded to read vendor id %s\r\n",buff);
   return MEA_OK;
}
static MEA_Status check_sfp_fiber(int i2c_device,int print_status,int enet_interface,int *fiber){
    unsigned char bufferVal[20];
    int ret=0;
    
    *fiber= MEA_FALSE;

    ret = ReadI2cBuffer(i2c_device,SERIAL_ADDRESS_SFP,14,bufferVal,5);
    if(ret != 0)
    {
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"failed to read SFP vendor id\r\n");
    	if(print_status == SFP_STATE_IN)
    	{
    		MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp is out\r\n");
    	}
        return MEA_ERROR;
    }
//     if(print_warning){
//         MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"interface %d [0]=%d [1]=%d [2]=%d [3]=%d [4]=%d\r\n",enet_interface,
//             bufferVal[0],bufferVal[1],bufferVal[2],bufferVal[3],bufferVal[4]);
//     }


    if(bufferVal[4] != 100)
    {
    	if(print_status == SFP_STATE_OUT)
    	{
    		MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"interface %d FIBER\r\n",enet_interface);
    	}
        *fiber= MEA_TRUE;
    }
    else
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"interface %d [0]=%d [1]=%d [2]=%d [3]=%d [4]=%d\r\n",enet_interface,
    	             bufferVal[0],bufferVal[1],bufferVal[2],bufferVal[3],bufferVal[4]);
    }

    return MEA_OK;

}

/*
 * this function check if vendor id and vendor name is matched
 */
static MEA_Status check_sfp_vendor(int i2c_device,int print_warning,int enet_interface)
{
	unsigned char vendorName[20];
	unsigned char vendorPartNum[20];
	int ret=0;
	char *str;
    //MEA_Uint8 copper=0;
	int found=0;
	int i=0;
     



    

	// read vendor id
	ret = ReadI2cBuffer(i2c_device,SERIAL_ADDRESS_SFP,SFP_TRANCIEVER_VENDOR_NAME_BASE_ADDR,vendorName,SFP_TRANCIEVER_VENDOR_LEN);
	if(ret != 0)
	{
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"failed to read SFP vendor id\r\n");
		return MEA_ERROR;
	}

	// read part number
	ret = ReadI2cBuffer(i2c_device,SERIAL_ADDRESS_SFP,SFP_TRANCIEVER_VENDOR_PART_NUMBER_BASE_ADDR,vendorPartNum,SFP_TRANCIEVER_VENDOR_PART_NUMBER_LEN);
	if(ret != 0)
	{
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"failed to read SFP vendor part number\r\n");
		return MEA_ERROR;
	}
	found=0;
	for(i=0;i<sizeof(vendorIdName_support)/sizeof(vendorIdName_support[0]);i++)
	{
		str = strstr((char *)vendorName,vendorIdName_support[i]);
		if(str)
		{
			found=1;
			break;
		}
	}

	
	if(print_warning==1)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"found interface %d vendor id %s\r\n",enet_interface,vendorName);
//		return MEA_ERROR;
	}

	found=0;
	for(i=0;i<sizeof(vendorIdPartNumber_support)/sizeof(vendorIdPartNumber_support[0]);i++)
	{
		str = strstr((char *)vendorPartNum,vendorIdPartNumber_support[i]);
		if(str)
		{
			found=1;
			break;
		}
	}
	if( (found==0) && (print_warning==1) )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"interface %d SFP vendor part number unknown %s\r\n",enet_interface,vendorPartNum);
//		return MEA_ERROR;
	}
	return MEA_OK;

}

static MEA_Status sfp_restart_autoneg(int i2c_device,tSfpInterfaceDb *pSfpCfg)
{
	int ret=0;
	unsigned short data=0;


	data = SFP_SOFT_RESET | SFP_AUTONEG_ENABLE | SFP_RESTART_AUTONEG  | SFP_DUPLEX_MODE | SFP_100M_SPEED ;
	ret =WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP,data);
	
	return ret;
}
#if 0
static MEA_Status K7_sfpPowerDown(i2c_device)
{
    int ret=0;
    unsigned short data=0;

    data = SFP_POWER_DOWN;
    ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP, (short)data);
    MEA_OS_sleep(0,100 *1000*1000);
    
    return ret;

}
#endif
/*
 * this function set speed configuration
 */
static MEA_Status sfp_speed_configuration(eSfpSpeedCfg speed,int i2c_device)
{
	int ret=0;
	unsigned short data=0;


	switch(speed)
	{
		case CONFIGURE_1G_FULL_DUPLEX:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"1G_FULL_DUPLEX \n");
			/* 0x9084 */
			data = SFP_FIBER_COPPER_SELECTION | SFP_SERIAL_INTERFACE_AUTONEG_BYPASS | 0x88;
			ret |= WriteReg16Bit(i2c_device,SFP_PHY_EXTENDED_PHY_SPECIFIC_STATUS,PHY_ADDRESS_SFP, data);
			/* 0x8140 */
			data = SFP_SOFT_RESET | SFP_DUPLEX_MODE | SFP_1G_SPEED;
			ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP, (short)data);

// 			data = 0x0E00;
// 			ret |=WriteReg16Bit(i2c_device,9,PHY_ADDRESS_SFP, (short)data);
            data = 0x0000;
            ret |=WriteReg16Bit(i2c_device,0x16,PHY_ADDRESS_SFP, (short)data);
            /* 0x2100 */
            data = SFP_SOFT_RESET | SFP_DUPLEX_MODE| SFP_1G_SPEED ;
			ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP, data);

			break;
		case CONFIGURE_100M_FULL_DUPLEX:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"100M_FULL_DUPLEX \n");
			/* 0x9084 */
			data = SFP_FIBER_COPPER_SELECTION | SFP_SERIAL_INTERFACE_AUTONEG_BYPASS | SFP_SGMII_MODE | 0x80;
			ret |= WriteReg16Bit(i2c_device,SFP_PHY_EXTENDED_PHY_SPECIFIC_STATUS,PHY_ADDRESS_SFP, data);
			/* 0x8140 */
			data = SFP_SOFT_RESET | SFP_DUPLEX_MODE | SFP_1G_SPEED;
			ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP, (short)data);
			/* 0x2100 */
            data = SFP_SPEED_SELECTION_HIGHBIT | SFP_DUPLEX_MODE;
			ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP, data);
			/* 0x0001 */
			data = SFP_DISABLE_JABBER_FUNCTION;
			ret |=WriteReg16Bit(i2c_device,SFP_PHY_SPECIFIC_CONTROL_REGISTER,PHY_ADDRESS_SFP, data);
			/* 0xA100 */
            data = SFP_SOFT_RESET | SFP_DUPLEX_MODE | SFP_100M_SPEED;
			ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP, data);


			break;
		case CONFIGURE_10M_FULL_DUPLEX:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"10M_FULL_DUPLEX \n");
			/* configure 1G full duplex */
			/* 0x9084 */
			data = SFP_FIBER_COPPER_SELECTION | SFP_SERIAL_INTERFACE_AUTONEG_BYPASS | SFP_SGMII_MODE | 0x80;
			ret |= WriteReg16Bit(i2c_device,SFP_PHY_EXTENDED_PHY_SPECIFIC_STATUS,PHY_ADDRESS_SFP, data);
            /* 0x8140 */
            data = SFP_SOFT_RESET | SFP_DUPLEX_MODE | SFP_1G_SPEED;
            ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP, (short)data);
			/* 0x0100 */
			data = SFP_DUPLEX_MODE;
			ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP, data);
			/* 0x000 */
			data = 0x00;
			ret |=WriteReg16Bit(i2c_device,SFP_PHY_SPECIFIC_CONTROL_REGISTER,PHY_ADDRESS_SFP, data);
			/* 0x8100 */
			data = SFP_SOFT_RESET | SFP_DUPLEX_MODE | SFP_10M_SPEED;
			ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP, data);
			break;
		case CONFIGURE_AUTONEG:
          //  MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"CONFIGURE_AUTONEG \n");
			/* configure autneg */
			data = SFP_FIBER_COPPER_SELECTION | SFP_SGMII_MODE ;
			ret |=WriteReg16Bit(i2c_device,SFP_PHY_EXTENDED_PHY_SPECIFIC_STATUS,PHY_ADDRESS_SFP, data);
			data = SFP_100BASE_FULLDUPLEX | SFP_10BASE_FULLDUPLEX | SFP_SELECTOR_FIELD;
			ret |=WriteReg16Bit(i2c_device,SFP_AUTONEG_ADVERTISEMENT_REGISTER,PHY_ADDRESS_SFP, data);
			data = SFP_SOFT_RESET | SFP_AUTONEG_ENABLE | SFP_RESTART_AUTONEG | SFP_DUPLEX_MODE  ;
			ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP,data);

			data = SFP_FIBER_COPPER_SELECTION | SFP_SGMII_MODE ;
			ret |=WriteReg16Bit(i2c_device,SFP_PHY_EXTENDED_PHY_SPECIFIC_STATUS,PHY_ADDRESS_SFP, data);
			data = SFP_100BASE_FULLDUPLEX | SFP_10BASE_FULLDUPLEX | SFP_SELECTOR_FIELD;
			ret |=WriteReg16Bit(i2c_device,SFP_AUTONEG_ADVERTISEMENT_REGISTER,PHY_ADDRESS_SFP, data);
			data = SFP_SOFT_RESET | SFP_AUTONEG_ENABLE | SFP_RESTART_AUTONEG  | SFP_DUPLEX_MODE  ;
			ret |=WriteReg16Bit(i2c_device,SFP_CONTROL_REGISTER,PHY_ADDRESS_SFP,data);
			break;
		default:
			break;
	}
	return ret;
}
/*
 * this function get the sfp link status and speed
 */
static MEA_Status read_link_status(int i2c_device,tSfpStatus *pSfpStatus)
{
	int ret=0;
	short data=0;

	pSfpStatus->isLinkUp=0;
	pSfpStatus->speedStatus=0;
	pSfpStatus->isFullDuplex=0;

//	ret = ReadReg16Bit(i2c_device,19,PHY_ADDRESS_SFP ,&data);
//	fprintf(stdout,"interrupt status 0x%x\n",data);

//	ret = ReadReg16Bit(i2c_device,SFP_STATUS_REGISTER,PHY_ADDRESS_SFP ,&data);
//	fprintf(stdout,"read status 0x%x\n",data);

	ret = ReadReg16Bit(i2c_device,SFP_PHY_SPECIFIC_STATUS_REGISTER,PHY_ADDRESS_SFP ,&data);
	if(ret == MEA_OK)
	{
		if(data & SFP_LINKUP_STATE)
		{
			pSfpStatus->isLinkUp=1;
		}

		if(data & SFP_LINKUP_STATE)
		{
			if(data & SFP_SPEED_1G_STATE)
			{
				pSfpStatus->speedStatus = CONFIGURE_1G_FULL_DUPLEX;
			}
			else if(data & SFP_SPEED_100M_STATE)
			{
				pSfpStatus->speedStatus = CONFIGURE_100M_FULL_DUPLEX;
			}
			else
			{
				pSfpStatus->speedStatus = CONFIGURE_10M_FULL_DUPLEX;
			}
			if(data & SFP_FULLDUPLEX_STATE)
			{
				pSfpStatus->isFullDuplex=1;
			}
			else
			{
				pSfpStatus->isFullDuplex=0;
			}
		}
	}

	return ret;
}
/*
 * this function responsible for sfp state machine
 */
static void sfp_state_machine(tSfpInterfaceDb *pSfpCfg,int i2c_device)
{
	tSfpStatus tSfpStat;
	tSgmiiStatus tSgmiiStatus;
	tSfpStatus tMediaStatus;
	MEA_Interface_Entry_dbt tEntry;


	if(pSfpCfg == NULL)
		return;

	if(pSfpCfg->init_configuration == 0)
	{
		if(pSfpCfg->sfpSpeed == CONFIGURE_AUTONEG)
		{
			tEntry.autoneg = 1;

			tEntry.speed = MEA_INTEFACE_SPEED_GIGA;

			MEA_ConfigureInterface_Speed(pSfpCfg->enetinterface,&tEntry,NULL,0);
		}
		memset(&tEntry,0,sizeof(MEA_Interface_Entry_dbt));
		pSfpCfg->init_configuration=1;
	}


    //toggle out the i2c
	if(pSfpCfg->interfaceType != MEA_INTERFACETYPE_RGMII)
	{
		WriteReg8Bit(i2c_device,0x00,SERIAL_ADDRESS_I2C1 ,0x00);
		WriteReg8Bit(i2c_device,0x00,SERIAL_ADDRESS_I2C2 ,0x00);
		WriteReg8Bit(i2c_device,0x00,pSfpCfg->toggle_in_interface ,pSfpCfg->toggle_in_interface_data);
	}



    switch(pSfpCfg->sfp_state)
    {
    case SFP_STATE_OUT:
        pSfpCfg->config_status_complete=0;
        pSfpCfg->fiber=MEA_FALSE;

		if(check_sfp_fiber(i2c_device,pSfpCfg->sfp_state,pSfpCfg->enetinterface,&pSfpCfg->fiber) == MEA_OK)
		{
			if(pSfpCfg->fiber == 1)
			{
				if(pSfpCfg->lastLinkStatus == LINK_STATUS_DOWN);
				//if(pSfpCfg->sfpSpeed == CONFIGURE_AUTONEG)
				{
					//pSfpCfg->sfpSpeed = CONFIGURE_1G_FULL_DUPLEX;
					tEntry.speed=0;
					tEntry.autoneg=0;
					pSfpCfg->lastLinkStatus == LINK_STATUS_UP;
					MEA_ConfigureInterface_Speed(pSfpCfg->enetinterface,&tEntry,NULL,0);
					mea_update_interface_config_from_sfp(pSfpCfg->enetinterface,&tEntry);
				}
			}
		}
        if(pSfpCfg->interfaceType == MEA_INTERFACETYPE_RGMII)
        {
            if(mdio_set_rgmii_configuration(pSfpCfg,gMdio_device) != MEA_OK)
             {
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mdio %d: return error\n",pSfpCfg->mdio_address);
             }
             pSfpCfg->sfp_state = SFP_STATE_IN;
             pSfpCfg->restart_autoneg=0;
        }
        else if(pSfpCfg->sfp_number == 0)
        {

            if(mdio_set_qsgmii_configuration(pSfpCfg,gMdio_device) != MEA_OK)
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mdio %d: return error\n",pSfpCfg->mdio_address);
            }
            pSfpCfg->sfp_state = SFP_STATE_IN;
        }
        else
        {

            if(check_sfp_vendor(i2c_device,(pSfpCfg->sfp_prev_state != pSfpCfg->sfp_state)?1:0,pSfpCfg->enetinterface) == MEA_OK)
			{
                    /*sfp power down for force mode */

                    pSfpCfg->link_down_countetr++;
                    /* sfp in -configure the sfp*/
                    //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp out state interface %d: vendor identify OK\n",pSfpCfg->enetinterface);
                    if(pSfpCfg->fiber == 0)
                    {
                        if(pSfpCfg->mdio_address != -1)
                        {
                            if(mdio_set_qsgmii_configuration(pSfpCfg,gMdio_device) != MEA_OK)
                            {
                                MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mdio %d: return error\n",pSfpCfg->mdio_address);
                            }
                        }
                        if(sfp_speed_configuration(pSfpCfg->sfpSpeed,i2c_device) == MEA_OK)
                        {
                            pSfpCfg->sfp_state = SFP_STATE_IN;
                            //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp out state %d: configured OK\n",pSfpCfg->enetinterface);


                            pSfpCfg->restart_autoneg=0;
                        }

                    }
                    else if(pSfpCfg->mdio_address != -1)
                    {

                        if(mdio_set_qsgmii_configuration(pSfpCfg,gMdio_device) != MEA_OK)
                        {
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mdio %d: return error\n",pSfpCfg->mdio_address);
                        }
                        pSfpCfg->sfp_state = SFP_STATE_IN;
                    }
                    else
                    {// SGMII only no mdio
                        pSfpCfg->sfp_state = SFP_STATE_IN;
                    }
				}
        	}
            pSfpCfg->sfp_prev_state = pSfpCfg->sfp_state;
			break;
		case SFP_STATE_IN:
			//check if sfp still in
            pSfpCfg->sfp_prev_state = pSfpCfg->sfp_state;

			if( pSfpCfg->fiber == 1)
			{
				if(check_sfp_fiber(i2c_device,pSfpCfg->sfp_state,pSfpCfg->enetinterface,&pSfpCfg->fiber) != MEA_OK)
				{
					pSfpCfg->sfp_state = SFP_STATE_OUT;
					pSfpCfg->lastLinkStatus == LINK_STATUS_DOWN;
					//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: vendor not identify or sfp out\n",pSfpCfg->enetinterface);
					break;
				}
				if( (pSfpCfg->mdio_address == -1) && (pSfpCfg->interfaceType != MEA_INTERFACETYPE_RGMII) )
				{
					if(pSfpCfg->config_status_complete == 0)
					{
						pSfpCfg->config_status_complete=1;
					}
					break;
				}
			}
            if(pSfpCfg->interfaceType != MEA_INTERFACETYPE_RGMII)
            {
				if(pSfpCfg->sfp_number != 0)
            	{
					if(check_sfp_vendor(i2c_device,0,pSfpCfg->enetinterface) != MEA_OK)
					{
						pSfpCfg->sfp_state = SFP_STATE_OUT;
						MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: vendor not identify or sfp out\n",pSfpCfg->enetinterface);
						break;
					}
            	}
            }
			if(pSfpCfg->mdio_address != -1)
			{
				if(pSfpCfg->interfaceType == MEA_INTERFACETYPE_RGMII)
				{
					if(mdio_get_rgmii_status(pSfpCfg,gMdio_device,&tSgmiiStatus) != MEA_OK)
					{
						MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mdio rgmii in state interface %d: failed to read status\n",pSfpCfg->enetinterface);
						break;
					}
				}
				else
				{
					if(mdio_get_qsgmii_status(pSfpCfg,gMdio_device,&tSgmiiStatus) != MEA_OK)
					{
						MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"mdio sgmii in state interface %d: failed to read status\n",pSfpCfg->enetinterface);
						break;
					}
				}
				memcpy(&tMediaStatus,&tSgmiiStatus,sizeof(tSfpStatus));
			}



			if(pSfpCfg->interfaceType != MEA_INTERFACETYPE_RGMII)
			{
				if( ( pSfpCfg->fiber == 0) && (pSfpCfg->sfp_number != 0) )
				{
					//read status
					if(read_link_status(i2c_device,&tSfpStat) != MEA_OK)
					{
						MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: failed to read status\n",pSfpCfg->enetinterface);
						break;
					}
					memcpy(&tMediaStatus,&tSfpStat,sizeof(tSfpStatus));
				}
			}



			if(tMediaStatus.isLinkUp == 1)
			{
				pSfpCfg->lastLinkStatus = LINK_STATUS_UP;
				if( (pSfpCfg->lastSpeedStataus != tMediaStatus.speedStatus) && (pSfpCfg->sfpSpeed == CONFIGURE_AUTONEG ) )
				{
					pSfpCfg->config_status_complete = 0;
				}

			}
			else
			{
				//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: link is down\n",pSfpCfg->enetinterface);
				if(pSfpCfg->config_status_complete==1)
				{
					MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: link is down\n",pSfpCfg->enetinterface);
				}
				pSfpCfg->config_status_complete=0;
				pSfpCfg->lastLinkStatus = LINK_STATUS_DOWN;
				memset(&tEntry,0,sizeof(tEntry));
		//		if(pSfpCfg->sfpSpeed == CONFIGURE_AUTONEG)
		//		{
		//			tEntry.autoneg = (1 << 0);
		//		}
				//Alex TBD  mea_update_interface_config_from_sfp(pSfpCfg->enetinterface,&tEntry);
			}

			if( (pSfpCfg->config_status_complete == 0) && (pSfpCfg->lastLinkStatus == LINK_STATUS_UP) )
			{
				//configure the enet interface
				pSfpCfg->config_status_complete=1;
				tEntry.speed=0;
				tEntry.autoneg=0;
						
				if(pSfpCfg->sfpSpeed == CONFIGURE_AUTONEG)
				{
					tEntry.autoneg = 1;

					pSfpCfg->lastSpeedStataus = tMediaStatus.speedStatus;

					if(tMediaStatus.speedStatus == CONFIGURE_1G_FULL_DUPLEX)
					{
						tEntry.speed = MEA_INTEFACE_SPEED_GIGA;
					}
					else if(tMediaStatus.speedStatus == CONFIGURE_100M_FULL_DUPLEX)
					{
						tEntry.speed = MEA_INTEFACE_SPEED_100M;
					}
					else
					{
						tEntry.speed = MEA_INTEFACE_SPEED_10M;
					}
					MEA_ConfigureInterface_Speed(pSfpCfg->enetinterface,&tEntry,NULL,0);
					mea_update_interface_config_from_sfp(pSfpCfg->enetinterface,&tEntry);
				}
				

                /* enable to port */
//                mea_update_interface_Admin(pSfpCfg->enetinterface,MEA_TRUE); 

				
                if(pSfpCfg->sfpSpeed == CONFIGURE_AUTONEG){
                    if(tMediaStatus.speedStatus == CONFIGURE_1G_FULL_DUPLEX )
                    {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: link is up: ========  autoneg speed=1000M\n",pSfpCfg->enetinterface);
                    }
                    else if(tMediaStatus.speedStatus == CONFIGURE_100M_FULL_DUPLEX)
                    {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: link is up: ======== autoneg speed=100M\n",pSfpCfg->enetinterface);
                    }
                    else
                    {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: link is up: ======== autoneg speed=10M\n",pSfpCfg->enetinterface);
                    }
                }
                else{
                    switch (pSfpCfg->sfpSpeed){
                    case CONFIGURE_1G_FULL_DUPLEX:
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: link is up force mode 1G \n",pSfpCfg->enetinterface);
                            break;
                    case CONFIGURE_100M_FULL_DUPLEX:
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: link is up force mode 100M \n",pSfpCfg->enetinterface);
                        break;
                    case CONFIGURE_10M_FULL_DUPLEX:
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: link is up force mode 10M \n",pSfpCfg->enetinterface);
                        break;
                    default:
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"sfp in state interface %d: link is up force mode ERROR \n",pSfpCfg->enetinterface);
                        break;
                    }
                   
                }
				if( (pSfpCfg->sfpSpeed == CONFIGURE_AUTONEG) && (pSfpCfg->restart_autoneg == 0) )
				{
					if( ( pSfpCfg->fiber == 0) && (pSfpCfg->sfp_number != 0) )
					{
                        sfp_restart_autoneg(i2c_device,pSfpCfg);
//						sfp_speed_configuration(pSfpCfg->sfpSpeed,i2c_device);
					}
					pSfpCfg->restart_autoneg=1;

				}				
			}
			break;
		default:
			break;
	}
}
/*
 * this function called every 1 second checking the interface status
 */
void mea_sfp_periodic_task(void)
{
    int Id=0;



    for(Id=0;Id<MEA_MAX_PORT_NUMBER;Id++)
	{
        if( (MEA_SfpInterfaceDb[Id].isValidInterface == 1)  )
		{
           

            sfp_state_machine(&MEA_SfpInterfaceDb[Id],gI2c_device);
		}
	}
}

MEA_Status sfp_get_link_state(MEA_Bool *pState,ENET_PortId_t port)
{
    int Id=0;

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"interface :%d \n",port);
    (*pState) = MEA_FALSE;
    for(Id=0;Id<MEA_MAX_PORT_NUMBER;Id++)
	{
        if( (MEA_SfpInterfaceDb[Id].isValidInterface == 1)  && (MEA_SfpInterfaceDb[Id].enetinterface == port) )
        {
        	if(MEA_SfpInterfaceDb[Id].lastLinkStatus == LINK_STATUS_UP)
        	{
        		(*pState) = MEA_TRUE;
        	}

        	return MEA_OK;
        }
	}
    return MEA_ERROR;
}

MEA_Status sfp_get_link_speed_status(MEA_Uint32 *pSpeed,ENET_PortId_t port)
{
    int Id=0;

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"interface :%d \n",port);

    for(Id=0;Id<MEA_MAX_PORT_NUMBER;Id++)
	{
        if( (MEA_SfpInterfaceDb[Id].isValidInterface == 1)  && (MEA_SfpInterfaceDb[Id].enetinterface == port) )
		{
        	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"interface :%d found\n",port);
        	if( (MEA_SfpInterfaceDb[Id].sfp_state == SFP_STATE_IN) && (MEA_SfpInterfaceDb[Id].lastLinkStatus == LINK_STATUS_UP) )
        	{
        		if(MEA_SfpInterfaceDb[Id].sfpSpeed == CONFIGURE_AUTONEG)
        		{
        			(*pSpeed) = MEA_SfpInterfaceDb[Id].lastSpeedStataus; //CONFIGURE_1G_FULL_DUPLEX CONFIGURE_100M_FULL_DUPLEX CONFIGURE_10M_FULL_DUPLEX
        		}
        		else
        		{
        			(*pSpeed) =  MEA_SfpInterfaceDb[Id].sfpSpeed;
        		}
        		MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"interface :%d speed:%d\n",port,(*pSpeed));
    			return MEA_OK;
        	}
		}
	}
	return MEA_ERROR;
}
/*
 * this function open the device driver
 */
MEA_Status mea_sfp_open_connection(void)
{
//	int i=0;
//    MEA_Status   ret;

    if ((MEA_device_environment_info.MEA_SFP_SUPPORT_enable == MEA_FALSE) ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"No support SFP \n");
        return MEA_OK;
   }
	// open device driver connection
	gI2c_device = open(E2PROM_DEVICE, O_RDWR);


	if (gI2c_device == -1)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s Unable to open i2c, cause: %s.\n",__FUNCTION__,strerror(errno));
		return MEA_ERROR;
	}
#ifndef MEA_PLAT_S1_VDSL_K7

	gMdio_device = open(MDIO_DEVICE, O_RDWR);
	if (gMdio_device == -1)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s Unable to open mdio, cause: %s.\n",__FUNCTION__,strerror(errno));
		return MEA_ERROR;
	}


	k7_hardware_reset();

//	for(i=0;i<sizeof(i2c_reset_command)/sizeof(i2c_reset_command[0]);i++)
//	{
//		system(i2c_reset_command[i]);
//	}


// 	for(i=0;i<sizeof(mdio_reset_command)/sizeof(mdio_reset_command[0]);i++)
// 	{
// 		system(mdio_reset_command[i]);
// 	}
	k7_mdio_reset();


	sleep(1);
#endif



    /*  */



	return MEA_OK;
}
/*
 * this function close the device driver
 */
MEA_Status mea_sfp_close_connection(void)
{
	close(gI2c_device);
#ifndef MEA_PLAT_S1_VDSL_K7
	close(gMdio_device);
#endif	
    return MEA_OK;
}
#endif /*defined(MEA_OS_LINUX) && defined(MEA_ETHERNITY)*/



