/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifdef MEA_OS_Z1


#include "MEA_platform.h"


#include "ethernity_adaptor.h"


//#include "enet_api.h"
#include "mea_api.h"
#include "mea_fwd_tbl.h"
#include "enet_xPermission_drv.h"
#include "enet_queue_drv.h"

/* to increase the speed of Pm collect and get use adaptor database*/
typedef struct {
	MEA_Port_t          ingressPort;
	MEA_Port_t          egressPort;
	MEA_Service_t       serviceId;
	MEA_Bool            service_Uses_DSE;
	MEA_Uint32          servicePri;
	MEA_Counters_PM_dbt pmCounter;
	MEA_Bool            isPmValid;
} mea_adaptor_Pm_dbt;
mea_adaptor_Pm_dbt pm_dbt[MEA_MAX_NUM_OF_PM_ID];
extern MEA_Status mea_drv_Collect_Counters_PM  (MEA_Unit_t           unit,
                                         MEA_PmId_t           pmId,
                                         MEA_Counters_PM_dbt* entry);
#define  MEA_adaptor_drv_Collect_Counters_PM  mea_drv_Collect_Counters_PM

#define MEA_ADAPTOR_MULTICAST_SUPPORT

#define MEA_ADAPTOR_FIRST_VDSL_PORT   0
#ifdef MEA_OS_Z1_OLD_BOARD
#define MEA_ADAPTOR_LAST_VDSL_PORT   31
#else  /* MEA_OS_Z1_OLD_BOARD */
#define MEA_ADAPTOR_LAST_VDSL_PORT   63
#endif /* MEA_OS_Z1_OLD_BOARD */
#define MEA_ADAPTOR_UPLINK_PORT     125
#define MEA_ADAPTOR_DOWNLINK_PORT   126
#define MEA_ADAPTOR_CPU_PORT        127

#define MEA_ADAPTOR_UPLINK_DEFAULT_CLUSTER     ENET_PLAT_PORT_125_DEFAULT_CLUSTER
#define MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER   ENET_PLAT_PORT_126_DEFAULT_CLUSTER
#define MEA_ADAPTOR_CPU_DEFAULT_CLUSTER        ENET_PLAT_PORT_127_DEFAULT_CLUSTER
#define MEA_ADAPTOR_QUEUE_NAME_TAG 4


#define MEA_ADAPTOR_OPPOSITE_PORT 
#define NEC_FPGA_API_UPLINK_PORT   18
#define NEC_FPGA_API_DOWNLINK_PORT 17
 
#ifdef    MEA_OS_Z1_OLD_BOARD
#define MEA_ADAPTOR_EXTERNAL_PORT(port) \
( (((port) <= MEA_ADAPTOR_LAST_VDSL_PORT) && \
   ((port) >= 24                        )  ) \
   ? (((port)-24)+8) : (port))
#else   /* MEA_OS_Z1_OLD_BOARD */
#ifndef MEA_ADAPTOR_OPPOSITE_PORT
#define MEA_ADAPTOR_EXTERNAL_PORT(port) \
( (((port) <= MEA_ADAPTOR_LAST_VDSL_PORT) && \
   ((port) >= 32                        )  ) \
   ? ((((port)-32)/4)+8) : ((port)/4))
#else
#define MEA_ADAPTOR_EXTERNAL_PORT(port) \
( (((port) <= MEA_ADAPTOR_LAST_VDSL_PORT) && \
   ((port) >= 32                        )  ) \
   ? (((port)/4)-8) : (((port)/4)+8))
#endif
#endif  /* MEA_OS_Z1_OLD_BOARD */

#ifndef MEA_ADAPTOR_OPPOSITE_PORT
#define MEA_ADAPTOR_INTERNAL_PORT(port) (port*4)
#else
#define MEA_ADAPTOR_INTERNAL_PORT(port) \
( (((port) <= 15) && \
   ((port) >= 8)  ) \
   ? (((port)-8)*4) : (((port)+8)*4) )
#endif  /* MEA_OS_Z1_OLD_BOARD */

/* 32,36,40..56,60 -> 32,33,34..38,39*/
/*0,4,8..28        -> 0,1,2..7       */
/*125,126,127      -> 125,126,127    */

MEA_Port_t ConverPortToInterface(MEA_Port_t port) 
{
	MEA_Port_t interfacePort = port;
	if (/*(port>=0)&&*/(port<=28))          
		interfacePort = port/4; 
	else if((port>=32)&&(port<=60))     
		interfacePort = (port-32)/4+32;
	else                                
		interfacePort = port;           

	return (interfacePort);
}

static MEA_Port_t ConverPortFromInterface(MEA_Port_t port)
{
	MEA_Port_t interfacePort = port;
	if (/*(port>=0)&&*/(port<=7))          
		interfacePort = port * 4; 
	else if((port>=32)&&(port<=39))     
		interfacePort = (port-32)*4+32;
	else                                
		interfacePort = port;           

	return (interfacePort);
}


#define MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET (MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL)

#define MEA_ADAPTOR_MAPPING_ETHERTYPE_WEIGHT  2 
#define MEA_ADAPTOR_MAPPING_L3_PRI_WEIGHT     1 
#define MEA_ADAPTOR_MAPPING_L2_PRI_WEIGHT     3 

static MEA_adaptor_Fpga_info_t MEA_adaptor_Fpga_init_info;
static MEA_Status MEA_adaptor_API_Collect_Counters_PMs (MEA_Unit_t  unit) ;
static MEA_Status MEA_adaptor_API_Collect_Counters_All(MEA_Unit_t unit);
 static MEA_Bool poling_active = MEA_FALSE;
MEA_Bool enet_Is_port_enable(int port_i,MEA_Uint32	port_close_bitmap);

 

MEA_Status MEA_adaptor_init_mapping(MEA_adaptor_Fpga_info_t* Fpga_init_info) 
{

   MEA_Mapping_dbt mapping_info;
   MEA_Uint32      mapping_id;
   int             i;

   if(Fpga_init_info->ether_type.mode == 1) {
	   for( i = 0; i < Fpga_init_info->ether_type.num_entry; i++ ) 
	   {
	       MEA_OS_memset(&mapping_info,0,sizeof(mapping_info));
  	       mapping_info.key.type = MEA_MAPPING_TYPE_ETHERTYPE;
	       mapping_info.key.weight = MEA_ADAPTOR_MAPPING_ETHERTYPE_WEIGHT;
		   mapping_info.key.value.ethertype.ethertype = Fpga_init_info->ether_type.etherType[i].etherType;
		   mapping_info.data.stamping_pri_valid       = MEA_TRUE;
		   mapping_info.data.stamping_pri_value       = Fpga_init_info->ether_type.etherType[i].priority;

		   if (MEA_API_Create_Mapping(MEA_UNIT_0,
                                      &mapping_info,
                                      &mapping_id ) != MEA_OK ) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - MEA_API_Create_Mapping failed\n",
                                 __FUNCTION__);
               return MEA_ERROR;
	       }
	   } /* for( i = 0; i < Fpga_init_info->ether_type.num_entry; i++ )  */
	} /* if(Fpga_init_info->ether_type.mode != 1) */



   /* ToS-CoS Setting */
   if (Fpga_init_info->qos_option == SP_QOS_OPTION_TOS_STD       ) {

	   for( i = 0; i < MEA_NUM_OF_ELEMENTS(Fpga_init_info->tos); i++ ) {

		   if (!(Fpga_init_info->tos[i].flags & SP_CONFIGURED/*SP_ACTIVE*/)) {
				continue;
		   }
		   MEA_OS_memset(&mapping_info,0,sizeof(mapping_info));
	  	   mapping_info.key.type          = MEA_MAPPING_TYPE_IPV4_DSCP;
           mapping_info.key.weight        = MEA_ADAPTOR_MAPPING_L3_PRI_WEIGHT;			
		   mapping_info.key.value.ipv4_dscp.dscp = Fpga_init_info->tos[i].tos;
		   mapping_info.data.stamping_pri_valid       = MEA_TRUE;
		   mapping_info.data.stamping_pri_value       = Fpga_init_info->tos[i].priority;
		   		
           if (MEA_API_Create_Mapping(MEA_UNIT_0,
                                      &mapping_info,
                                      &mapping_id ) != MEA_OK ) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - MEA_API_Create_Mapping failed\n",
                                 __FUNCTION__);
               return MEA_ERROR;
	       }

	   } /* for( i = 0; i < MEA_NUM_OF_ELEMENTS(Fpga_init_info->tos); i++ ) */

   } /* if (Fpga_init_info->qos_option == SP_QOS_OPTION_TOS_STD       ) */

   /* Precedence-CoS Setting */
   if (Fpga_init_info->qos_option == SP_QOS_OPTION_PRECEDENCE) {

	   for( i = 0; i < MEA_NUM_OF_ELEMENTS(Fpga_init_info->precedence); i++ ) {

		   if (!(Fpga_init_info->precedence[i].flags & SP_CONFIGURED/*SP_ACTIVE*/)) {
				continue;
		   }
		   MEA_OS_memset(&mapping_info,0,sizeof(mapping_info));
		   mapping_info.key.type          = MEA_MAPPING_TYPE_IPV4_PRECEDENCE;
           mapping_info.key.weight        = MEA_ADAPTOR_MAPPING_L3_PRI_WEIGHT;			
		   mapping_info.key.value.ipv4_precedence.precedence = Fpga_init_info->precedence[i].precedence;
		   mapping_info.data.stamping_pri_valid       = MEA_TRUE;
		   mapping_info.data.stamping_pri_value       = Fpga_init_info->precedence[i].priority;

           if (MEA_API_Create_Mapping(MEA_UNIT_0,
                                      &mapping_info,
                                      &mapping_id ) != MEA_OK ) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - MEA_API_Create_Mapping failed\n",
                                 __FUNCTION__);
               return MEA_ERROR;
	       }

	   } /* for( i = 0; i < MEA_NUM_OF_ELEMENTS(Fpga_init_info->tos); i++ ) */

   } /* if (Fpga_init_info->qos_option == SP_QOS_OPTION_TOS_STD       ) */


   /* define mapping priority to queue */
   for(i=0; i< 8; i++) {
	   MEA_OS_memset(&mapping_info,0,sizeof(mapping_info));
	   mapping_info.key.type             = MEA_MAPPING_TYPE_L2_PRI;
	   mapping_info.key.weight           = MEA_ADAPTOR_MAPPING_L2_PRI_WEIGHT;
	   mapping_info.data.pri_queue_valid         = MEA_TRUE;
	   mapping_info.data.classifier_pri_valid    = MEA_TRUE;
	   mapping_info.key.value.l2_pri.l2_pri = i;

	   mapping_info.data.pri_queue_value = Fpga_init_info->userPriority2TrafficClass[i]; /* T.B.D Izik */


	   if ((Fpga_init_info->mld_enable) && 
		   (mapping_info.data.pri_queue_value >= Fpga_init_info->mld_cos) ) {
           mapping_info.data.pri_queue_value++;
	   }
 

	   mapping_info.data.classifier_pri_value    = (Fpga_init_info->userPriority2TrafficClass[i] * 2) + 1;; /* T.B.D Izik */

	   if (MEA_API_Create_Mapping(MEA_UNIT_0,
                                  &mapping_info,
                                  &mapping_id ) != MEA_OK ) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Create_Mapping failed\n",
                             __FUNCTION__);
           return MEA_ERROR;
      }
   }

	return MEA_OK;

}


MEA_Status MEA_adaptor_Set_DownStreem_queue(MEA_Uint32	downstreem_buffer[4],MEA_adaptor_Fpga_info_t* Fpga_init_info) {

	ENET_QueueId_t Queue_id;
	ENET_QueueId_t next_Queue_id;
	ENET_Bool      found;
	ENET_Unit_t    unit_i= ENET_UNIT_0;
	ENET_Queue_dbt entry;
	MEA_Uint8 i,priQ;

	
	/* Get the first Queue */ 
	if (ENET_GetFirst_Queue  (unit_i,
		                            &Queue_id,
							        &entry, /* entry_o*/
							        &found) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_GetFirst_Queue failed \n",
						   __FUNCTION__);
		return MEA_ERROR;
	}



	/* scan all Queue and delete one by one */
	while(found) {

		next_Queue_id = Queue_id;

		/* set downstreem  only for default clusters */
		if ( (Queue_id%4==0)|| 
			 (Queue_id == MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER) ||
			 (Queue_id == MEA_ADAPTOR_CPU_DEFAULT_CLUSTER) )
		{
	            for(i=0;i<ENET_MAX_NUMBER_OF_PRI_QUEUE;i++)
 	            {
		      entry.pri_queues[i].max_q_size_Packets=0;
		      entry.pri_queues[i].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
		      entry.pri_queues[i].mode.value.strict_priority=0;
		      entry.pri_queues[i].max_q_size_Byte=0;
	           }
	           for(i=0;i<4;i++)
	           {
                     MEA_Uint32 tmp;

		     /* skip multicast priority Queue */
	             if ((Fpga_init_info->mld_enable) && 
		         (i >= Fpga_init_info->mld_cos) ) 
                        priQ=i+1;
	             else
		        priQ=i;

	             entry.pri_queues[priQ].max_q_size_Byte=downstreem_buffer[i]*32;

                     /* round up in case of no granularity of 64 bytes */
                     if ((entry.pri_queues[priQ].max_q_size_Byte % 64) > 0 ) {
                        entry.pri_queues[priQ].max_q_size_Byte /= 64;
                        entry.pri_queues[priQ].max_q_size_Byte++;
                        entry.pri_queues[priQ].max_q_size_Byte *= 64;
                     }

                     /* define the max_q__size_Packets according to max_q_size_Byts */
                     if (entry.pri_queues[priQ].max_q_size_Byte < 1024) {
                        tmp = 64;
                     } else {
                        if (entry.pri_queues[priQ].max_q_size_Byte < 16*1024) {
                           tmp = 256;
                        } else {
                           tmp = 512;
                        }
                    }
  	            entry.pri_queues[priQ].max_q_size_Packets = 
                         entry.pri_queues[priQ].max_q_size_Byte / tmp;
                   if ((entry.pri_queues[priQ].max_q_size_Byte % tmp) != 0) {
                       entry.pri_queues[priQ].max_q_size_Packets++;
                   }

	             if ((Fpga_init_info->mld_enable) && 
		         (i == Fpga_init_info->mld_cos) ) { 
  	                entry.pri_queues[i].max_q_size_Packets = 
  	                   entry.pri_queues[priQ].max_q_size_Packets;
  	                entry.pri_queues[i].max_q_size_Byte = 
  	                   entry.pri_queues[priQ].max_q_size_Byte;
                     }

	           }


			if (ENET_Set_Queue(ENET_UNIT_0,Queue_id,&entry)!=ENET_OK){
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								 "%s - ENET_Set_Queue failed\n",
								 __FUNCTION__);
			   return MEA_ERROR;
		   }
		}
        if (ENET_GetNext_Queue   (unit_i,
		                                &next_Queue_id,
		  					            &entry, /* entry_o */
							            &found) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - enet_GetNext_Queue failed \n",
		 		  		       __FUNCTION__);
		    return MEA_ERROR;
		}
		Queue_id = next_Queue_id;
	}

    /* Return to caller */
    return MEA_OK; 
}


MEA_Status MEA_adaptor_init_queue(MEA_adaptor_Fpga_info_t* Fpga_init_info) {

	ENET_QueueId_t Queue_id;
	ENET_QueueId_t next_Queue_id;
	ENET_Bool      found;
	ENET_Unit_t    unit_i= ENET_UNIT_0;
	ENET_Queue_dbt entry;
	MEA_Uint8 i;

	
	/* Get the first Queue */ 
	if (ENET_GetFirst_Queue  (unit_i,
		                            &Queue_id,
							        &entry, /* entry_o*/
							        &found) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_GetFirst_Queue failed \n",
						   __FUNCTION__);
		return MEA_ERROR;
	}



	/* scan all Queue and delete one by one */
	while(found) {

		next_Queue_id = Queue_id;

			for (i=0;i<ENET_PLAT_QUEUE_NUM_OF_PRI_Q;i++)
			{
				entry.pri_queues[i].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
				entry.pri_queues[i].mode.value.wfq_weight = ENET_PQ_STRICT_DEF_VAL;
			}


			if (ENET_Set_Queue(ENET_UNIT_0,Queue_id,&entry)!=ENET_OK){
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								 "%s - ENET_Set_Queue failed\n",
								 __FUNCTION__);
			   return MEA_ERROR;
		   }		
           if (ENET_GetNext_Queue   (unit_i,
		                                &next_Queue_id,
		  					            &entry, /* entry_o */
							            &found) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - enet_GetNext_Queue failed \n",
		 		  		       __FUNCTION__);
		    return MEA_ERROR;
		}
		Queue_id = next_Queue_id;
	}

    /* Return to caller */
    return MEA_OK; 

}


ENET_Status MEA_adaptor_Create_UpStreem_queue (ENET_PortId_t port,char QueueName[MEA_ADAPTOR_QUEUE_NAME_TAG],MEA_Uint32	upstream_buffer[4],ENET_QueueId_t *Qid,MEA_adaptor_Fpga_info_t* Fpga_init_info)
{
	ENET_Queue_dbt  entry;
	ENET_Uint16		i,priQ;
	*Qid = ENET_PLAT_GENERATE_NEW_ID;

	MEA_OS_memcmp(entry.name,"%4.4s(%ld)",QueueName,port);
	entry.port.type    = ENET_QUEUE_PORT_TYPE_PORT;
	entry.port.id.port = MEA_ADAPTOR_UPLINK_PORT;
	entry.mode.type    = ENET_QUEUE_MODE_TYPE_WFQ;
	entry.mode.value.cwrr_weight = ENET_CLUSTER_CWRR_DEF_VAL;
	entry.MTU          = MEA_MTU_SIZE_DEF_VAL;

	for(i=0;i<ENET_MAX_NUMBER_OF_PRI_QUEUE;i++)
	{
		entry.pri_queues[i].max_q_size_Packets=0;
		entry.pri_queues[i].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
		entry.pri_queues[i].mode.value.strict_priority=0;
		entry.pri_queues[i].max_q_size_Byte=0;
	}
	for(i=0;i<4;i++)
	{
           MEA_Uint32 tmp;

		/* skip multicast priority Queue */
	   if ((Fpga_init_info->mld_enable) && 
		   (i >= Fpga_init_info->mld_cos) ) 
           priQ=i+1;
	   else
		   priQ=i;

	   entry.pri_queues[priQ].max_q_size_Byte=upstream_buffer[i]*32;

           /* round up in case of no granularity of 64 bytes */
           if ((entry.pri_queues[priQ].max_q_size_Byte % 64) > 0 ) {
               entry.pri_queues[priQ].max_q_size_Byte /= 64;
               entry.pri_queues[priQ].max_q_size_Byte++;
               entry.pri_queues[priQ].max_q_size_Byte *= 64;
           }

           /* define the max_q__size_Packets according to max_q_size_Byts */
           if (entry.pri_queues[priQ].max_q_size_Byte < 1024) {
              tmp = 64;
           } else {
              if (entry.pri_queues[priQ].max_q_size_Byte < 16*1024) {
                 tmp = 256;
              } else {
                 tmp = 512;
              }
           }
  	   entry.pri_queues[priQ].max_q_size_Packets = 
                    entry.pri_queues[priQ].max_q_size_Byte / tmp;
           if ((entry.pri_queues[priQ].max_q_size_Byte % tmp) != 0) {
                    entry.pri_queues[priQ].max_q_size_Packets++;
           }
	}

	return (ENET_Create_Queue(ENET_UNIT_0,&entry,Qid));
}


MEA_Status MEA_adaptor_init_ports(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) 
{

    MEA_IngressPort_Entry_dbt ingressPort_entry;
    MEA_EgressPort_Entry_dbt  egressPort_entry;
    MEA_Port_t port;
	MEA_Status rc=MEA_OK;


    for (port  = 0 ;
         port <= MEA_MAX_PORT_NUMBER;
         port++) {

        if (MEA_API_Get_IsPortValid(port,MEA_TRUE/*silent*/)==MEA_FALSE) {
           continue;
        }    

        if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
                                          port,
                                          &ingressPort_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_InressPort_Entry for port %d failed\n",
                              __FUNCTION__,
	                          port);
            rc=MEA_ERROR;
			continue;
        }

        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                         port,
                                         &egressPort_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_EgressPort_Entry for port %d failed\n",
                              __FUNCTION__,
	                          port);
            rc=MEA_ERROR;
			continue;
        }


        ingressPort_entry.parser_info.port_proto_prof    = MEA_INGRESS_PORT_PROTO_VLAN;
        egressPort_entry.proto                           = MEA_EGRESS_PORT_PROTO_VLAN;
		ingressPort_entry.parser_info.port_cos_aw        = MEA_TRUE;
        ingressPort_entry.parser_info.port_ip_aw         = MEA_FALSE;
        ingressPort_entry.parser_info.port_col_aw        = MEA_FALSE;
		ingressPort_entry.parser_info.pri_wildcard_valid = MEA_TRUE;
		ingressPort_entry.parser_info.pri_wildcard       = 0x01;
		ingressPort_entry.parser_info.net_wildcard_valid = MEA_FALSE;
		ingressPort_entry.parser_info.net_wildcard       = 0;

		if( (enet_Is_port_enable(NEC_FPGA_API_UPLINK_PORT,Fpga_init_info_p->port_close) == MEA_TRUE)
		 && (port == MEA_ADAPTOR_UPLINK_PORT) ) {
			/* set shaper for uplink */
			egressPort_entry.shaper_info.CIR             = Fpga_init_info_p->uplink_policy_set*1000000;
			egressPort_entry.shaper_info.CBS             = 10000;
			egressPort_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
			egressPort_entry.shaper_info.overhead        = 20;			/* 12 + 8 */
			egressPort_entry.shaper_info.Cell_Overhead   = 0;
			egressPort_entry.Shaper_compensation         = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
			egressPort_entry.shaper_enable			    = ENET_TRUE;	
		}
		/* Define if the port allowed to receive tagged / untagged packets , and 
		   what is the default values in case of untagged packet for vid and priority */
        if (/*(port >= MEA_ADAPTOR_FIRST_VDSL_PORT) && */ 
            (port <= MEA_ADAPTOR_LAST_VDSL_PORT )  ) {

			ingressPort_entry.parser_info.allowed_tagged = MEA_TRUE;
			ingressPort_entry.parser_info.allowed_untagged = 
              (Fpga_init_info_p->egress_drop & (1<<MEA_ADAPTOR_EXTERNAL_PORT((port)))) ? MEA_FALSE : MEA_TRUE; 
            ingressPort_entry.parser_info.default_net_tag_value = 
               (Fpga_init_info_p->pvid[MEA_ADAPTOR_EXTERNAL_PORT((port))+1]& 0xfff);
			ingressPort_entry.parser_info.default_pri=
               (Fpga_init_info_p->priority[MEA_ADAPTOR_EXTERNAL_PORT((port))+1] & 0x7 );
                          
			ingressPort_entry.parser_info.mapping_profile = MEA_MAPPING_TABLE_PROFILE_0;

		}else {
            if ((port >= MEA_ADAPTOR_CPU_PORT) && 
                (port <= MEA_ADAPTOR_CPU_PORT )  ) {
			   ingressPort_entry.parser_info.allowed_tagged         = MEA_TRUE;
			   ingressPort_entry.parser_info.allowed_untagged       = MEA_TRUE;
               ingressPort_entry.parser_info.default_net_tag_value  = Fpga_init_info_p->pvid[0];
               
               ingressPort_entry.parser_info.default_pri= (Fpga_init_info_p->priority[0] & 0x7);
                                    
			} else {
			   ingressPort_entry.parser_info.allowed_tagged         = MEA_TRUE;
			   ingressPort_entry.parser_info.allowed_untagged       = MEA_FALSE;
               ingressPort_entry.parser_info.default_net_tag_value  = 0;
			   ingressPort_entry.parser_info.mapping_profile = MEA_MAPPING_TABLE_PROFILE_1;
			}
		}

		if(Fpga_init_info_p->ether_type.mode == 1)
            ingressPort_entry.parser_info.port_ip_aw = MEA_TRUE;


		if ((Fpga_init_info_p->qos_option == SP_QOS_OPTION_TOS_STD   ) ||
			(Fpga_init_info_p->qos_option == SP_QOS_OPTION_PRECEDENCE)  ) {

            ingressPort_entry.parser_info.port_ip_aw         = MEA_TRUE;

			ingressPort_entry.parser_info.IP_pri_mask_valid = MEA_TRUE;

			if (Fpga_init_info_p->qos_option == SP_QOS_OPTION_TOS_STD   ) {
			   ingressPort_entry.parser_info.IP_pri_mask_type = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP;
			} else {
			   ingressPort_entry.parser_info.IP_pri_mask_type = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_PRECEDENCE;
			}


		    switch (Fpga_init_info_p->ip_version) {
			case SP_QOS_IPVERSION_ALL: /* Both IP V4 and IP V6 */
				ingressPort_entry.parser_info.IP_pri_mask_IPv4 = MEA_TRUE;
				ingressPort_entry.parser_info.IP_pri_mask_IPv6 = MEA_TRUE;
				break;

			case SP_QOS_IPVERSION_IPV4: /* IP V4 */
				ingressPort_entry.parser_info.IP_pri_mask_IPv4 = MEA_TRUE;
				ingressPort_entry.parser_info.IP_pri_mask_IPv6 = MEA_FALSE;
			    break;

			case SP_QOS_IPVERSION_IPV6: /* IP V6 */
				ingressPort_entry.parser_info.IP_pri_mask_IPv4 = MEA_FALSE;
				ingressPort_entry.parser_info.IP_pri_mask_IPv6 = MEA_TRUE;
			    break;
			default: 
				break;
			}
		}

		if((Fpga_init_info_p->mld_enable ) && (port!=MEA_ADAPTOR_CPU_PORT)){
			   MEA_L2CP_ENTRY_SET_ACTION(ingressPort_entry.parser_info.network_L2CP_action_table,L2CP_ADDR_MLDV2,MEA_L2CP_ACTION_CPU);
		} else {
			   MEA_L2CP_ENTRY_SET_ACTION(ingressPort_entry.parser_info.network_L2CP_action_table,L2CP_ADDR_MLDV2,MEA_L2CP_ACTION_PEER);
		}
 

        if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                          port,
                                          &ingressPort_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_InressPort_Entry for port %d failed\n",
                              __FUNCTION__,
	                          port);
            rc=MEA_ERROR;
        }

        if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                         port,
                                         &egressPort_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_EgressPort_Entry for port %d failed\n",
                              __FUNCTION__,
	                          port);
            rc=MEA_ERROR;
        }

      
    }

    return rc;
}


MEA_Status MEA_adaptor_init_vdsl_services(MEA_adaptor_Fpga_info_t* Fpga_init_info_p,
                                          MEA_Port_t port) {

    MEA_Uint8                       CoS;
    MEA_Service_t                   serviceId;
	MEA_Uint32                     *pPortGrp;                         
	MEA_Uint32                      shiftWord = 0x00000001;            
	MEA_Service_Entry_Key_dbt		service_id_key;
	MEA_Service_Entry_Data_dbt   	service_id_data;
	MEA_EgressHeaderProc_Entry_dbt  ehp_entry;
	MEA_OutPorts_Entry_dbt 	        out_ports_tbl_entry;
	MEA_Policer_Entry_dbt 		    sla_params_entry;
	MEA_Uint8                       default_pri;
    MEA_Bool   swap_enable;
    MEA_Uint32 UserPVid;
    MEA_Uint32 NetworkPVid;
	MEA_Status rc=MEA_OK;
	ENET_QueueId_t uplinkCluster;
	MEA_EgressHeaderProc_Array_Entry_dbt ehp_array;
	MEA_EHP_Info_dbt ehp_info;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	MEA_OS_memset(&ehp_array,0,sizeof(ehp_array));
	MEA_OS_memset(&ehp_info,0,sizeof(ehp_info));

    if (MEA_API_Get_IsPortValid(port,MEA_TRUE/*silent*/)==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - invalid port (%d) \n",
                          __FUNCTION__,port);
        return MEA_ERROR;
    }    


    if (/*(port < MEA_ADAPTOR_FIRST_VDSL_PORT) || */
        (port > MEA_ADAPTOR_LAST_VDSL_PORT )  ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - port (%d) is not in VDSL port range %d..%d \n",
                          __FUNCTION__,
                          port,
                          MEA_ADAPTOR_FIRST_VDSL_PORT,
                          MEA_ADAPTOR_LAST_VDSL_PORT);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


	/* Create uplink cluster */
	if (MEA_adaptor_Create_UpStreem_queue(port,"U_V ",Fpga_init_info_p->upstream_buffer,&uplinkCluster,Fpga_init_info_p)!=MEA_OK){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_adaptor_Create_UpStreem_queue  failed %d\n",
                              __FUNCTION__,uplinkCluster);
            rc=MEA_ERROR;
    }

    /* loop on all CoS */ 
    /* Note: Queue = CoS - 1 */
    for (CoS = 1; CoS < 8 ; CoS += 2) {

        /* zeros all structures */
        MEA_OS_memset(&service_id_key      , 0 , sizeof(service_id_key));
        MEA_OS_memset(&service_id_data     , 0 , sizeof(service_id_data));
        MEA_OS_memset(&ehp_entry           , 0 , sizeof(ehp_entry));
        MEA_OS_memset(&out_ports_tbl_entry , 0 , sizeof(out_ports_tbl_entry));
        MEA_OS_memset(&sla_params_entry    , 0 , sizeof(sla_params_entry));

        /* set default stamping info for any service */
        ehp_entry.eth_stamping_info.color_bit0_valid = MEA_TRUE;
        ehp_entry.eth_stamping_info.color_bit0_loc   = 12;
        ehp_entry.eth_stamping_info.color_bit1_valid = MEA_FALSE;
        ehp_entry.eth_stamping_info.color_bit1_loc   = 0;
        ehp_entry.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
        ehp_entry.eth_stamping_info.pri_bit0_loc     = 13;
        ehp_entry.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
        ehp_entry.eth_stamping_info.pri_bit1_loc     = 14;
        ehp_entry.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
        ehp_entry.eth_stamping_info.pri_bit2_loc     = 15;



        /* set service data information */
	    service_id_data.ADM_ENA           	  =	MEA_FALSE;
		service_id_data.DSE_forwarding_enable = MEA_FALSE;
		service_id_data.DSE_learning_enable   = MEA_FALSE;
            service_id_data.pmId    =   0; /* auto assign */
	    service_id_data.tmId    =   0; /* auto assign */
	    service_id_data.editId  =   0; /* auto assign */
	    service_id_data.valid                 = MEA_TRUE ;


        /* define service key source port */
	    service_id_key.src_port	= port;

        /* Define the service Key priority */
        service_id_key.pri      = CoS;



        /* define service key net_tag and also the ehp_entry if needed  */
        swap_enable = MEA_FALSE;
        UserPVid = Fpga_init_info_p->pvid[MEA_ADAPTOR_EXTERNAL_PORT(port)+1]; 
        NetworkPVid = UserPVid;
        service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | UserPVid;
           

        if ((Fpga_init_info_p->tag_swap_table_vid_map  & 
             (0x00000001 << MEA_ADAPTOR_EXTERNAL_PORT(port)) ) != 0) {
    

            MEA_Uint32 i;

            for (i=0;
                 i<MEA_NUM_OF_ELEMENTS(Fpga_init_info_p->tag_swap_table_vid[0].Tag_Swap_Table_Entry);
                 i++) {

                if (Fpga_init_info_p->tag_swap_table_vid[MEA_ADAPTOR_EXTERNAL_PORT(port)].Tag_Swap_Table_Entry[i].active) {
                    swap_enable = MEA_TRUE;
                    UserPVid    = Fpga_init_info_p->tag_swap_table_vid[MEA_ADAPTOR_EXTERNAL_PORT(port)].Tag_Swap_Table_Entry[i].VidUser_32;
	                NetworkPVid = Fpga_init_info_p->tag_swap_table_vid[MEA_ADAPTOR_EXTERNAL_PORT(port)].Tag_Swap_Table_Entry[i].VidUplink_32;

	                service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | UserPVid;

                    
                    break;
                }
            }

        }
        ehp_entry.eth_info.val.all = 0x81000000 | NetworkPVid; 
        ehp_entry.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
    	ehp_entry.eth_info.stamp_color    = MEA_FALSE;
        ehp_entry.eth_info.stamp_priority = MEA_TRUE;

        /* define the output port of the service */
	    pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
        *(pPortGrp+(uplinkCluster/32)) |= 
                (shiftWord<<(uplinkCluster%32));

        /* define the policer parameters for this service */
	    sla_params_entry.CIR         = Fpga_init_info_p->cosMaxBw[MEA_ADAPTOR_EXTERNAL_PORT(port)+1][CoS/2] * 1000; 
        sla_params_entry.EIR         =  0;
        sla_params_entry.CBS         =  (sla_params_entry.CIR == 0) ?  0 : 10000;
        sla_params_entry.EBS         =	0;
        sla_params_entry.cup	     =  MEA_FALSE;
        sla_params_entry.comp	     =	MEA_POLICER_COMP_ENFORCE;	
        sla_params_entry.color_aware =  MEA_FALSE;


        if ((!(Fpga_init_info_p->egress_drop & (1<<MEA_ADAPTOR_EXTERNAL_PORT(port)))) ) {

    	    MEA_Service_Entry_Key_dbt		service_id_key_untag;
	        MEA_EgressHeaderProc_Entry_dbt  ehp_entry_untag;
			MEA_Uint32                      untag_UserPVid;

			MEA_OS_memcpy(&service_id_key_untag,&service_id_key,sizeof(service_id_key));
			MEA_OS_memcpy(&ehp_entry_untag,&ehp_entry,sizeof(ehp_entry));

            untag_UserPVid = Fpga_init_info_p->pvid[MEA_ADAPTOR_EXTERNAL_PORT(port)+1]; 

			service_id_key_untag.net_tag = untag_UserPVid; 

  		    default_pri = Fpga_init_info_p->priority[MEA_ADAPTOR_EXTERNAL_PORT((port))+1];
			service_id_key_untag.pri  = CoS;

            ehp_entry_untag.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
            ehp_entry_untag.eth_info.val.all = 0x81000000;
	        ehp_entry_untag.eth_info.val.all |= ((Fpga_init_info_p->priority[MEA_ADAPTOR_EXTERNAL_PORT((port))+1] 
                                                 & 0x00000007) << 13);
            ehp_entry_untag.eth_info.val.all |= (untag_UserPVid    & 0x00000fff);
			ehp_entry_untag.eth_info.stamp_color = MEA_FALSE;
			ehp_entry_untag.eth_info.stamp_priority = MEA_TRUE;

		
			ehp_array.num_of_entries = 1;
			
			MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
			MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry_untag, sizeof(ehp_info.ehp_data));
			ehp_array.ehp_info = &ehp_info;

            /* Create the Untag Service */
 	        if (MEA_API_Create_Service(MEA_UNIT_0,
                                       &service_id_key_untag,
                                       &service_id_data,
                                       &out_ports_tbl_entry,
                                       &sla_params_entry,
                                       &ehp_array,
                                       &serviceId) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Create_Sercice failed\n",
                                  __FUNCTION__);
                rc=MEA_ERROR;
	        }
			pm_dbt[service_id_data.pmId].isPmValid        = MEA_TRUE;
			pm_dbt[service_id_data.pmId].egressPort       = MEA_ADAPTOR_UPLINK_PORT;
		    pm_dbt[service_id_data.pmId].ingressPort      = port;
			pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
			pm_dbt[service_id_data.pmId].serviceId        = serviceId;
			pm_dbt[service_id_data.pmId].servicePri       = service_id_key_untag.pri; 
		}
        
            service_id_data.pmId    =   0; /* auto assign */
            /* we want to have same tmId for untag and tag packets 
	    service_id_data.tmId    =   0; */ /* auto assign */
	    service_id_data.editId  =   0; /* auto assign */

		ehp_array.num_of_entries = 1;		
		MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
		MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry, sizeof(ehp_info.ehp_data));
		ehp_array.ehp_info = &ehp_info;

        /* Create the Service */
 	    if (MEA_API_Create_Service(MEA_UNIT_0,
                                   &service_id_key,
                                   &service_id_data,
                                   &out_ports_tbl_entry,
                                   &sla_params_entry,
                                   &ehp_array,
                                   &serviceId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Create_Service failed\n",
                              __FUNCTION__);
            rc=MEA_ERROR;
	    }
		pm_dbt[service_id_data.pmId].isPmValid        = MEA_TRUE;
		pm_dbt[service_id_data.pmId].egressPort       = MEA_ADAPTOR_UPLINK_PORT;
	    pm_dbt[service_id_data.pmId].ingressPort      = port;
		pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
		pm_dbt[service_id_data.pmId].serviceId        = serviceId;
		pm_dbt[service_id_data.pmId].servicePri       = service_id_key.pri; 


        /* define downstram service key source port */
	    service_id_key.src_port	= MEA_ADAPTOR_UPLINK_PORT;

        /* define downstream service key net_tag and also the ehp_entry if needed  */
        service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | NetworkPVid;
        ehp_entry.eth_info.val.all = 0x81000000 | UserPVid; 
        ehp_entry.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
    	ehp_entry.eth_info.stamp_color    = MEA_FALSE;
        ehp_entry.eth_info.stamp_priority = MEA_TRUE;
		if (Fpga_init_info_p->pvid_egress_setting & (1<<MEA_ADAPTOR_EXTERNAL_PORT(port))) {
			ehp_entry.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
			ehp_entry.eth_info.stamp_priority = MEA_FALSE;
		}
		
        service_id_data.pmId    =   0; /* auto assign */
	    service_id_data.tmId    =   0; /* auto assign */
	    service_id_data.editId  =   0; /* auto assign */

        /* Define downstream service policer CIR value to max port bandwith */
	    sla_params_entry.CIR    = 101000000;
	    sla_params_entry.CBS    = 10000;

        /* define the output port of the service */
        MEA_OS_memset(&out_ports_tbl_entry , 0 , sizeof(out_ports_tbl_entry));
	    pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
        *(pPortGrp+(port/32)) |= (shiftWord<<(port%32));

		ehp_array.num_of_entries = 1;		
		MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
		MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry, sizeof(ehp_info.ehp_data));
		ehp_array.ehp_info = &ehp_info;


        /* Create the downstream Service */
 	    if (MEA_API_Create_Service(MEA_UNIT_0,
                                   &service_id_key,
                                   &service_id_data,
                                   &out_ports_tbl_entry,
                                   &sla_params_entry,
                                   &ehp_array,
                                   &serviceId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Create_Sercice downstream failed\n",
                              __FUNCTION__);
            rc=MEA_ERROR;
	    }
		pm_dbt[service_id_data.pmId].isPmValid        = MEA_TRUE;
		pm_dbt[service_id_data.pmId].egressPort       = port;
	    pm_dbt[service_id_data.pmId].ingressPort      = MEA_ADAPTOR_UPLINK_PORT;
		pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
		pm_dbt[service_id_data.pmId].serviceId        = serviceId;
		pm_dbt[service_id_data.pmId].servicePri       = service_id_key.pri; 

    }
    return rc;
} 


MEA_Status MEA_adaptor_init_cpu_services(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) {

    MEA_Uint8                       CoS;
    MEA_Service_t                   serviceId;
	MEA_Uint32                     *pPortGrp;                         
	MEA_Uint32                      shiftWord = 0x00000001;            
	MEA_Service_Entry_Key_dbt		service_id_key;
	MEA_Service_Entry_Data_dbt   	service_id_data;
	MEA_EgressHeaderProc_Entry_dbt  ehp_entry;
	MEA_OutPorts_Entry_dbt 	        out_ports_tbl_entry;
	MEA_Policer_Entry_dbt 		    sla_params_entry;
	MEA_Status rc=MEA_OK;
    MEA_Port_t                      port;
	ENET_Queue_dbt queue_entry;
    MEA_Uint32 i,priQ;
	MEA_EgressHeaderProc_Array_Entry_dbt ehp_array;
	MEA_EHP_Info_dbt ehp_info;

	MEA_OS_memset(&ehp_array,0,sizeof(ehp_array));
	MEA_OS_memset(&ehp_info,0,sizeof(ehp_info));

	if (ENET_Get_Queue  (ENET_UNIT_0,
			     MEA_ADAPTOR_UPLINK_DEFAULT_CLUSTER,
                             &queue_entry) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			           "%s - ENET_Get_Queue failed \n",
						   __FUNCTION__);
		return MEA_ERROR;
        }
	for(i=0;i<ENET_MAX_NUMBER_OF_PRI_QUEUE;i++)
	{
		queue_entry.pri_queues[i].max_q_size_Packets=0;
		queue_entry.pri_queues[i].mode.type=ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
		queue_entry.pri_queues[i].mode.value.strict_priority=0;
		queue_entry.pri_queues[i].max_q_size_Byte=0;
	}
	for(i=0;i<4;i++)
	{
           MEA_Uint32 tmp;

		/* skip multicast priority Queue */
	   if ((Fpga_init_info_p->mld_enable) && 
		   (i >= Fpga_init_info_p->mld_cos) ) 
           priQ=i+1;
	   else
		   priQ=i;

	   queue_entry.pri_queues[priQ].max_q_size_Byte=Fpga_init_info_p->upstream_buffer[i]*32;

           /* round up in case of no granularity of 64 bytes */
           if ((queue_entry.pri_queues[priQ].max_q_size_Byte % 64) > 0 ) {
               queue_entry.pri_queues[priQ].max_q_size_Byte /= 64;
               queue_entry.pri_queues[priQ].max_q_size_Byte++;
               queue_entry.pri_queues[priQ].max_q_size_Byte *= 64;
           }

           /* define the max_q__size_Packets according to max_q_size_Byts */
           if (queue_entry.pri_queues[priQ].max_q_size_Byte < 1024) {
              tmp = 64;
           } else {
              if (queue_entry.pri_queues[priQ].max_q_size_Byte < 16*1024) {
                 tmp = 256;
              } else {
                 tmp = 512;
              }
           }
  	   queue_entry.pri_queues[priQ].max_q_size_Packets = 
                    queue_entry.pri_queues[priQ].max_q_size_Byte / tmp;
           if ((queue_entry.pri_queues[priQ].max_q_size_Byte % tmp) != 0) {
                    queue_entry.pri_queues[priQ].max_q_size_Packets++;
           }
	}
	if (ENET_Set_Queue(ENET_UNIT_0,
			   MEA_ADAPTOR_UPLINK_DEFAULT_CLUSTER,
                           &queue_entry) != ENET_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		  	      "%s - ENET_Set_Queue failed\n",
			      __FUNCTION__);
            return MEA_ERROR;
       }

    switch (Fpga_init_info_p->rcont_forward)
    {
		case 0: /* none */
		   break;
	    case 1: /* local cpu only */
		   break;
		case 2: /* local & remote cpu on same vid */
		   break;
		default: 
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - invalid rcont_forward (%d) value \n",
			                 __FUNCTION__,Fpga_init_info_p->rcont_forward);
		   return MEA_ERROR;
    }

    for (port  = 0 ; port <= MEA_MAX_PORT_NUMBER; port++) {

        if (MEA_API_Get_IsPortValid(port,MEA_TRUE/*silent*/)==MEA_FALSE) {
           continue;
        }    

        if (/*(port >= MEA_ADAPTOR_FIRST_VDSL_PORT) && */
            (port <= MEA_ADAPTOR_LAST_VDSL_PORT )  ) {

#ifndef MEA_OS_Z1_OLD_BOARD
				if ((port % 4) != 0) { 
					continue;
				}
#endif /* MEA_OS_Z1_OLD_BOARD */
		}


        /* loop on all CoS */	
        /* Note: Queue = CoS - 1 */
        for (CoS = 1; CoS < 8 ; CoS += 2) {


             /* zeros all structures */
             MEA_OS_memset(&service_id_key      , 0 , sizeof(service_id_key));
             MEA_OS_memset(&service_id_data     , 0 , sizeof(service_id_data));
             MEA_OS_memset(&ehp_entry           , 0 , sizeof(ehp_entry));
             MEA_OS_memset(&out_ports_tbl_entry , 0 , sizeof(out_ports_tbl_entry));
             MEA_OS_memset(&sla_params_entry    , 0 , sizeof(sla_params_entry));

             /* set default stamping info for any service */
             ehp_entry.eth_stamping_info.color_bit0_valid = MEA_TRUE;
             ehp_entry.eth_stamping_info.color_bit0_loc   = 12;
             ehp_entry.eth_stamping_info.color_bit1_valid = MEA_FALSE;
             ehp_entry.eth_stamping_info.color_bit1_loc   = 0;
             ehp_entry.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
             ehp_entry.eth_stamping_info.pri_bit0_loc     = 13;
             ehp_entry.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
             ehp_entry.eth_stamping_info.pri_bit1_loc     = 14;
             ehp_entry.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
             ehp_entry.eth_stamping_info.pri_bit2_loc     = 15;

             /* set service data information */
			 if (port == MEA_ADAPTOR_CPU_PORT) {
	             service_id_data.ADM_ENA	=	MEA_TRUE;
			 } else {
	             service_id_data.ADM_ENA	=	MEA_FALSE;
			 }
		     service_id_data.DSE_forwarding_enable =   MEA_FALSE;
		     service_id_data.DSE_learning_enable =   MEA_FALSE;
	         service_id_data.pmId    =   0; /* auto assign */
	         service_id_data.tmId    =   0; /* auto assign */
	         service_id_data.editId  =   0; /* auto assign */
	         service_id_data.valid   =   MEA_TRUE ;


             /* define service key source port */
	         service_id_key.src_port	= MEA_ADAPTOR_CPU_PORT;

             /* Define the service Key priority */
             service_id_key.pri      = CoS;

             /* define service key net_tag */
	         service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | port ;


             /* define ehp_entry */ 
             ehp_entry.eth_info.val.all        = 0x00000000 ;
  		     ehp_entry.eth_info.stamp_color    = MEA_FALSE;
		     ehp_entry.eth_info.stamp_priority = MEA_FALSE;
             ehp_entry.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;

             /* define the output port of the service */
	         pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
             if (port <= MEA_ADAPTOR_LAST_VDSL_PORT) {
				 if (port <32) {
                   *(pPortGrp+((port+32)/32)) |= (shiftWord<<((port+32)%32));
				 } else {
                   *(pPortGrp+((port-32)/32)) |= (shiftWord<<((port-32)%32));
				 }
			 } else if(port == MEA_ADAPTOR_UPLINK_PORT) {
                *(pPortGrp+(MEA_ADAPTOR_UPLINK_DEFAULT_CLUSTER/32)) |= (shiftWord<<(MEA_ADAPTOR_UPLINK_DEFAULT_CLUSTER%32));
			 } else if(port == MEA_ADAPTOR_DOWNLINK_PORT) {
                *(pPortGrp+(MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER/32)) |= (shiftWord<<(MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER%32));
			 } else if(port ==MEA_ADAPTOR_CPU_PORT ) {
                *(pPortGrp+(MEA_ADAPTOR_CPU_DEFAULT_CLUSTER/32)) |= (shiftWord<<(MEA_ADAPTOR_CPU_DEFAULT_CLUSTER%32));
			 }else{
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - unknown port number\n",
                                  __FUNCTION__);
                rc=MEA_ERROR;
			 }

             /* define the policer parameters for this service */
			 if (port == MEA_ADAPTOR_UPLINK_PORT) {
				 sla_params_entry.CIR         =  Fpga_init_info_p->cosMaxBw[0][0] * 1000;
			 } else {
	            sla_params_entry.CIR         =  101000000;
			 }
             sla_params_entry.EIR         =  0;
             sla_params_entry.CBS         =  10000;
             sla_params_entry.EBS         =	0;
             sla_params_entry.cup	     =  MEA_FALSE;
             sla_params_entry.comp	     =	MEA_POLICER_COMP_ENFORCE;	
             sla_params_entry.color_aware =  MEA_FALSE;

			ehp_array.num_of_entries = 1;		
			MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
			MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry, sizeof(ehp_info.ehp_data));
			ehp_array.ehp_info = &ehp_info;

            /* Create the Service */
 	        if (MEA_API_Create_Service(MEA_UNIT_0,
                                       &service_id_key,
                                       &service_id_data,
                                       &out_ports_tbl_entry,
                                       &sla_params_entry,
                                       &ehp_array,
                                       &serviceId) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Create_Sercice failed\n",
                                  __FUNCTION__);
                rc=MEA_ERROR;
	        }
			pm_dbt[service_id_data.pmId].isPmValid        = MEA_FALSE;//!!IZIK MEA_TRUE;
			pm_dbt[service_id_data.pmId].egressPort       = port;
			pm_dbt[service_id_data.pmId].ingressPort      = MEA_ADAPTOR_CPU_PORT;
			pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
			pm_dbt[service_id_data.pmId].serviceId        = serviceId;
			pm_dbt[service_id_data.pmId].servicePri       = service_id_key.pri; 




			if ( port == MEA_ADAPTOR_UPLINK_PORT )
			{
				if (Fpga_init_info_p->rcont_forward == 2)
				{
		
					 service_id_data.ADM_ENA	=	MEA_FALSE;
					 
					 service_id_data.DSE_forwarding_enable =   MEA_FALSE;
					 service_id_data.DSE_learning_enable =   MEA_FALSE;
					 service_id_data.pmId    =   0; /* auto assign */
					 service_id_data.tmId    =   0; /* auto assign */
					 service_id_data.editId  =   0; /* auto assign */
					 service_id_data.valid   =   MEA_TRUE ;


					 /* define service key source port */
					 service_id_key.src_port	= MEA_ADAPTOR_DOWNLINK_PORT;

					 /* Define the service Key priority */
					 service_id_key.pri      = CoS;

					 /* define service key net_tag */
					 service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | Fpga_init_info_p->pvid[0] ;


					ehp_entry.eth_info.val.all = 0x81000000 | Fpga_init_info_p->pvid[0]; 
					ehp_entry.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
    				ehp_entry.eth_info.stamp_color    = MEA_FALSE;
					ehp_entry.eth_info.stamp_priority = MEA_TRUE;

					/* define the output port of the service */
					pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
					*(pPortGrp+(MEA_ADAPTOR_UPLINK_DEFAULT_CLUSTER/32)) |= 
							(shiftWord<<(MEA_ADAPTOR_UPLINK_DEFAULT_CLUSTER%32));

					/* define the policer parameters for this service */
					sla_params_entry.CIR         =  100000000;
					sla_params_entry.EIR         =  0;
					sla_params_entry.CBS         =  10000;
					sla_params_entry.EBS         =	0;
					sla_params_entry.cup	     =  MEA_FALSE;
					sla_params_entry.comp	     =	MEA_POLICER_COMP_ENFORCE;	
					sla_params_entry.color_aware =  MEA_FALSE;

					ehp_array.num_of_entries = 1;		
					MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
					MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry, sizeof(ehp_info.ehp_data));
					ehp_array.ehp_info = &ehp_info;

					/* Create the Service */
 					if (MEA_API_Create_Service(MEA_UNIT_0,
											   &service_id_key,
											   &service_id_data,
											   &out_ports_tbl_entry,
											   &sla_params_entry,
											   &ehp_array,
											   &serviceId) != MEA_OK) {
						MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
										  "%s - MEA_API_Create_Sercice failed\n",
										  __FUNCTION__);
						rc=MEA_ERROR;
					}
					pm_dbt[service_id_data.pmId].isPmValid        = MEA_FALSE;//!!IZIK MEA_TRUE;
					pm_dbt[service_id_data.pmId].egressPort       = MEA_ADAPTOR_UPLINK_PORT;
					pm_dbt[service_id_data.pmId].ingressPort      = MEA_ADAPTOR_DOWNLINK_PORT;
					pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
					pm_dbt[service_id_data.pmId].serviceId        = serviceId;
					pm_dbt[service_id_data.pmId].servicePri       = service_id_key.pri; 

				}

			}

			if (port != MEA_ADAPTOR_UPLINK_PORT) {
				continue;
			}

			if (Fpga_init_info_p->rcont_forward == 0) /* none */ {
				continue;
			}

            /* define downstram service key source port */
	        service_id_key.src_port	= MEA_ADAPTOR_UPLINK_PORT;

            /* Define the service Key priority */
            service_id_key.pri      = CoS;

            /* define downstream service key net_tag */
            service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | Fpga_init_info_p->pvid[0];


            service_id_data.pmId    =   0; /* auto assign */
	        service_id_data.tmId    =   0; /* auto assign */
	        service_id_data.editId  =   0; /* auto assign */

			if (Fpga_init_info_p->rcont_forward == 1) /* to CPU */
			{
            /* define ehp_entry - Append vlan that vid will have the source port */
                ehp_entry.eth_info.val.all        = 0x81000000 ;
		        ehp_entry.eth_info.stamp_color    = MEA_FALSE;
		        ehp_entry.eth_info.stamp_priority = MEA_TRUE;
                ehp_entry.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_SWAP;

				/* define the output port of the service */
				MEA_OS_memset(&out_ports_tbl_entry , 0 , sizeof(out_ports_tbl_entry));
				pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
				*(pPortGrp+(MEA_ADAPTOR_CPU_DEFAULT_CLUSTER/32)) |= (shiftWord<<(MEA_ADAPTOR_CPU_DEFAULT_CLUSTER%32));
            }
			else if (Fpga_init_info_p->rcont_forward == 2) /* to CPU and 126 */
			{
#if 0
				service_id_data.PTMT        = MEA_TRUE;
				service_id_data.MC_FID_VALID = MEA_TRUE;
				service_id_data.MC_FID_EDIT = 0;
#endif
				/* define the output port of the service */
				MEA_OS_memset(&out_ports_tbl_entry , 0 , sizeof(out_ports_tbl_entry));
				pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
				*(pPortGrp+(MEA_ADAPTOR_CPU_DEFAULT_CLUSTER/32)) |= (shiftWord<<(MEA_ADAPTOR_CPU_DEFAULT_CLUSTER%32));
				*(pPortGrp+(MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER/32)) |= (shiftWord<<(MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER%32));
			}

             			
			ehp_array.num_of_entries = 1;		
			MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
			MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry, sizeof(ehp_info.ehp_data));
			ehp_array.ehp_info = &ehp_info;

            /* Create the downstream Service */
 	        if (MEA_API_Create_Service(MEA_UNIT_0,
                                       &service_id_key,
                                       &service_id_data,
                                       &out_ports_tbl_entry,
                                       &sla_params_entry,
                                       &ehp_array,
                                       &serviceId) != MEA_OK) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - MEA_API_Create_Sercice downstream failed\n",
                                 __FUNCTION__);
               rc=MEA_ERROR;
	       }
		   /*TBD what about the DownLink*/
		   pm_dbt[service_id_data.pmId].isPmValid        = MEA_FALSE;//!!IZIK MEA_TRUE;
		   pm_dbt[service_id_data.pmId].egressPort       = MEA_ADAPTOR_CPU_PORT;
		   pm_dbt[service_id_data.pmId].ingressPort      = MEA_ADAPTOR_UPLINK_PORT;
		   pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
		   pm_dbt[service_id_data.pmId].serviceId        = serviceId;
		   pm_dbt[service_id_data.pmId].servicePri       = service_id_key.pri; 
		}
			
    }

    return rc;
} 



MEA_Status MEA_adaptor_init_downlink_services
                  (MEA_adaptor_Fpga_info_t* Fpga_init_info_p,
                   MEA_Uint32               vid             ,
				   MEA_Port_t               remoteVdslPort) {


    MEA_Uint8                       CoS;
    MEA_Service_t                   serviceId;
	MEA_Uint32                     *pPortGrp;                         
	MEA_Uint32                      shiftWord = 0x00000001;            
	MEA_Service_Entry_Key_dbt		service_id_key;
	MEA_Service_Entry_Data_dbt   	service_id_data;
	MEA_EgressHeaderProc_Entry_dbt  ehp_entry;
	MEA_OutPorts_Entry_dbt 	        out_ports_tbl_entry;
	MEA_Policer_Entry_dbt 		    sla_params_entry;
	MEA_Status rc=MEA_OK;
	ENET_QueueId_t  remoteUplinkQueue;
	MEA_EgressHeaderProc_Array_Entry_dbt ehp_array;
	MEA_EHP_Info_dbt ehp_info;

	MEA_OS_memset(&ehp_info,0,sizeof(ehp_info));
	MEA_OS_memset(&ehp_array,0,sizeof(ehp_array));

	/* Create uplink cluster */
#if 1
	if (MEA_adaptor_Create_UpStreem_queue(remoteVdslPort,"R_U ",Fpga_init_info_p->upstream_buffer,&remoteUplinkQueue,Fpga_init_info_p)!=MEA_OK){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_adaptor_Create_UpStreem_queue downstream failed %d\n",
                              __FUNCTION__,remoteUplinkQueue);
            rc=MEA_ERROR;
    }
#else
	remoteUplinkQueue = MEA_ADAPTOR_UPLINK_DEFAULT_CLUSTER;
#endif


    /* loop on all CoS */ 
    /* Note: Queue = CoS - 1 */
    for (CoS = 1; CoS < 8 ; CoS += 2) {

        /* zeros all structures */
        MEA_OS_memset(&service_id_key      , 0 , sizeof(service_id_key));
        MEA_OS_memset(&service_id_data     , 0 , sizeof(service_id_data));
        MEA_OS_memset(&ehp_entry           , 0 , sizeof(ehp_entry));
        MEA_OS_memset(&out_ports_tbl_entry , 0 , sizeof(out_ports_tbl_entry));
        MEA_OS_memset(&sla_params_entry    , 0 , sizeof(sla_params_entry));

        /* set default stamping info for any service */
        ehp_entry.eth_stamping_info.color_bit0_valid = MEA_TRUE;
        ehp_entry.eth_stamping_info.color_bit0_loc   = 12;
        ehp_entry.eth_stamping_info.color_bit1_valid = MEA_FALSE;
        ehp_entry.eth_stamping_info.color_bit1_loc   = 0;
        ehp_entry.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
        ehp_entry.eth_stamping_info.pri_bit0_loc     = 13;
        ehp_entry.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
        ehp_entry.eth_stamping_info.pri_bit1_loc     = 14;
        ehp_entry.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
        ehp_entry.eth_stamping_info.pri_bit2_loc     = 15;

        /* set service data information */
	    service_id_data.ADM_ENA	=	MEA_FALSE;
		service_id_data.DSE_forwarding_enable =   MEA_FALSE;
		service_id_data.DSE_learning_enable   =   MEA_FALSE;
            service_id_data.pmId    =   0; /* auto assign */
	    service_id_data.tmId    =   0; /* auto assign */
	    service_id_data.editId  =   0; /* auto assign */
	    service_id_data.valid   =   MEA_TRUE ;


        /* define service key source port */
	    service_id_key.src_port	= MEA_ADAPTOR_DOWNLINK_PORT;

        /* Define the service Key priority */
        service_id_key.pri      = CoS;



        /* define service key net_tag and also the ehp_entry if needed  */
        service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | vid;

        ehp_entry.eth_info.val.all = 0x81000000 | vid; 
        ehp_entry.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
    	ehp_entry.eth_info.stamp_color    = MEA_FALSE;
        ehp_entry.eth_info.stamp_priority = MEA_TRUE;

        /* define the output port of the service */
	    pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
        *(pPortGrp+(remoteUplinkQueue/32)) |= 
                (shiftWord<<(remoteUplinkQueue%32));

        /* define the policer parameters for this service */
	    sla_params_entry.CIR         =  100000000;
        sla_params_entry.EIR         =  0;
        sla_params_entry.CBS         =  10000;
        sla_params_entry.EBS         =	0;
        sla_params_entry.cup	     =  MEA_FALSE;
        sla_params_entry.comp	     =	MEA_POLICER_COMP_ENFORCE;	
        sla_params_entry.color_aware =  MEA_FALSE;


		ehp_array.num_of_entries = 1;		
		MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
		MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry, sizeof(ehp_info.ehp_data));
		ehp_array.ehp_info = &ehp_info;

        /* Create the Service */
 	    if (MEA_API_Create_Service(MEA_UNIT_0,
                                   &service_id_key,
                                   &service_id_data,
                                   &out_ports_tbl_entry,
                                   &sla_params_entry,
                                   &ehp_array,
                                   &serviceId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Create_Sercice failed\n",
                              __FUNCTION__);
            rc=MEA_ERROR;
	    }
	    pm_dbt[service_id_data.pmId].isPmValid        = MEA_TRUE;
	    pm_dbt[service_id_data.pmId].egressPort       = MEA_ADAPTOR_UPLINK_PORT;
	    pm_dbt[service_id_data.pmId].ingressPort      = MEA_ADAPTOR_DOWNLINK_PORT;
	    pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
	    pm_dbt[service_id_data.pmId].serviceId        = serviceId;
	    pm_dbt[service_id_data.pmId].servicePri       = service_id_key.pri; 



        /* define downstram service key source port */
	    service_id_key.src_port	= MEA_ADAPTOR_UPLINK_PORT;

        /* define downstream service key net_tag  
           Note: The create service may change service_id_key.net_tag 
                 by use the or_net_tag_mask from the IngressPort_Entry 
                 of service_id_key.src_port (specaily in case of vlan tagged) 
         */
        service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | vid;

            service_id_data.pmId    =   0; /* auto assign */
	    service_id_data.tmId    =   0; /* auto assign */
	    service_id_data.editId  =   0; /* auto assign */

        /* define the output port of the service */
        MEA_OS_memset(&out_ports_tbl_entry , 0 , sizeof(out_ports_tbl_entry));
	    pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
        *(pPortGrp+(MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER/32)) 
                     |= (shiftWord<<(MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER%32));

		ehp_array.num_of_entries = 1;		
		MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
		MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry, sizeof(ehp_info.ehp_data));
		ehp_array.ehp_info = &ehp_info;

        /* Create the downstream Service */
 	    if (MEA_API_Create_Service(MEA_UNIT_0,
                                   &service_id_key,
                                   &service_id_data,
                                   &out_ports_tbl_entry,
                                   &sla_params_entry,
                                   &ehp_array,
                                   &serviceId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Create_Sercice downstream failed\n",
                              __FUNCTION__);
            rc=MEA_ERROR;
	    }
		pm_dbt[service_id_data.pmId].isPmValid        = MEA_TRUE;
		pm_dbt[service_id_data.pmId].egressPort       = MEA_ADAPTOR_DOWNLINK_PORT;
	    pm_dbt[service_id_data.pmId].ingressPort      = MEA_ADAPTOR_UPLINK_PORT;
		pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
		pm_dbt[service_id_data.pmId].serviceId        = serviceId;
		pm_dbt[service_id_data.pmId].servicePri       = service_id_key.pri; 
    }

    return rc;
} 


MEA_Status MEA_adaptor_init_mc_services
                  (MEA_adaptor_Fpga_info_t* Fpga_init_info_p) {


#ifdef MEA_ADAPTOR_MULTICAST_SUPPORT
    MEA_Uint8                       i, CoS;
    MEA_Service_t                   serviceId;          
	MEA_Service_Entry_Key_dbt		service_id_key;
	MEA_Service_Entry_Data_dbt   	service_id_data;
	MEA_EgressHeaderProc_Entry_dbt  ehp_entry;
	MEA_OutPorts_Entry_dbt 	        out_ports_tbl_entry;
	MEA_Policer_Entry_dbt 		    sla_params_entry;
	MEA_Uint32                     *pPortGrp;                         

    MEA_Uint32 UserPVid;
    MEA_Uint32 NetworkPVid;
	MEA_Uint32 vlan_pri;
	MEA_Status rc=MEA_OK;
	MEA_EgressHeaderProc_Array_Entry_dbt ehp_array;
	MEA_EHP_Info_dbt ehp_info;

	MEA_OS_memset(&ehp_info,0,sizeof(ehp_info));
	MEA_OS_memset(&ehp_array,0,sizeof(ehp_array));

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	if ( Fpga_init_info_p == NULL )
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - null Fpga_init_info_p input\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	if ( ! (Fpga_init_info_p->mld_enable)) {
		return MEA_OK;
	}

    for (i = 0; i < 4 ; i++) 
	{
		for (CoS = 1; CoS < 8 ; CoS += 2)   /* require to avoid the mapping to priQ*/
		{
			if ( (Fpga_init_info_p->mvid[i].valid)  && (Fpga_init_info_p->mvid[i].vid != 0))
			{
				/* zeros all structures */
				MEA_OS_memset(&service_id_key      , 0 , sizeof(service_id_key));
				MEA_OS_memset(&service_id_data     , 0 , sizeof(service_id_data));
				MEA_OS_memset(&ehp_entry           , 0 , sizeof(ehp_entry));
				MEA_OS_memset(&out_ports_tbl_entry , 0 , sizeof(out_ports_tbl_entry));
				MEA_OS_memset(&sla_params_entry    , 0 , sizeof(sla_params_entry));

				/* set default stamping info for any service */
				ehp_entry.eth_stamping_info.color_bit0_valid = MEA_TRUE;
				ehp_entry.eth_stamping_info.color_bit0_loc   = 12;
				ehp_entry.eth_stamping_info.color_bit1_valid = MEA_FALSE;
				ehp_entry.eth_stamping_info.color_bit1_loc   = 0;
				ehp_entry.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
				ehp_entry.eth_stamping_info.pri_bit0_loc     = 13;
				ehp_entry.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
				ehp_entry.eth_stamping_info.pri_bit1_loc     = 14;
				ehp_entry.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
				ehp_entry.eth_stamping_info.pri_bit2_loc     = 15;

			

				/* set service data information */
				service_id_data.ADM_ENA           	  =	MEA_FALSE;
				service_id_data.DSE_forwarding_enable = MEA_TRUE;
				service_id_data.DSE_forwarding_key_type = 0;
				service_id_data.DSE_learning_enable   = MEA_FALSE;
				service_id_data.pmId    =   0; /* auto assign */
				service_id_data.tmId    =   0; /* auto assign */
				service_id_data.editId  =   0; /* auto assign */
				service_id_data.valid                 = MEA_TRUE ;
				service_id_data.vpn = 0 /* Cfox ask this fix - Fpga_init_info_p->mvid[i].vid*/;
				
				service_id_data.COS_FORCE = MEA_TRUE;
				service_id_data.COS = Fpga_init_info_p->mld_priority;


				
				/* define service key source port */
				service_id_key.src_port	= MEA_ADAPTOR_UPLINK_PORT;

				/* Define the service Key priority */
				service_id_key.pri      = CoS;



				/* define service key net_tag and also the ehp_entry if needed  */
				UserPVid = Fpga_init_info_p->mvid[i].vid; 
				NetworkPVid = UserPVid;
				service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | UserPVid;

				vlan_pri = Fpga_init_info_p->mld_cos;


				ehp_entry.eth_info.val.all = 0x81000000 | (vlan_pri << 13) | NetworkPVid; 
				ehp_entry.eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
				ehp_entry.eth_info.stamp_color    = MEA_FALSE;
				ehp_entry.eth_info.stamp_priority = MEA_FALSE;

				/* define the policer parameters for this service */
				sla_params_entry.CIR         =  100000000;
				sla_params_entry.EIR         =  0;
				sla_params_entry.CBS         =  10000;
				sla_params_entry.EBS         =	0;
				sla_params_entry.cup	     =  MEA_FALSE;
				sla_params_entry.comp	     =	MEA_POLICER_COMP_ENFORCE;	
				sla_params_entry.color_aware =  MEA_FALSE;    

				/* define the output port of the service */				
				pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
				*(pPortGrp+(MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER/32)) |= (1<<(MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER%32));

				ehp_array.num_of_entries = 1;		
				MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
				MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry, sizeof(ehp_info.ehp_data));
				ehp_array.ehp_info = &ehp_info;

				/* Create the Service */
 				if (MEA_API_Create_Service(MEA_UNIT_0,
										   &service_id_key,
										   &service_id_data,
										   &out_ports_tbl_entry,
										   &sla_params_entry,
										   &ehp_array,
										   &serviceId) != MEA_OK) 
				{
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - MEA_API_Create_Sercice failed\n",
									  __FUNCTION__);
					rc=MEA_ERROR;
				}
				pm_dbt[service_id_data.pmId].isPmValid        = MEA_FALSE;//!!IZIK MEA_TRUE;
				pm_dbt[service_id_data.pmId].egressPort       = MEA_ADAPTOR_DOWNLINK_PORT;
				pm_dbt[service_id_data.pmId].ingressPort      = MEA_ADAPTOR_UPLINK_PORT;
				pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
				pm_dbt[service_id_data.pmId].serviceId        = serviceId;
				pm_dbt[service_id_data.pmId].servicePri       = service_id_key.pri; 
			}
		}
	}
    
	return rc;
#else
        return MEA_OK;
#endif
} 

MEA_Status MEA_adaptor_init_services(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) 
{

    MEA_Port_t                      port;
	MEA_Status rc = MEA_OK;

    return MEA_OK;
    for (port  = 0 ;
         port <= MEA_MAX_PORT_NUMBER;
         port++) {


        if (MEA_API_Get_IsPortValid(port,MEA_TRUE/*silent*/)==MEA_FALSE) {
           continue;
        }    



        if (/*(port >= MEA_ADAPTOR_FIRST_VDSL_PORT) && */
            (port <= MEA_ADAPTOR_LAST_VDSL_PORT )  ) {

#ifndef MEA_OS_Z1_OLD_BOARD
				if ((port % 4) != 0) { 
					continue;
				}
#endif /* MEA_OS_Z1_OLD_BOARD */
            if (MEA_adaptor_init_vdsl_services(Fpga_init_info_p,port) != MEA_OK) { 
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_adaptor_init_vdsl_services failed (port=%d)\n",
                                  __FUNCTION__,port);
                rc=MEA_ERROR;
			}
		}


        if (port == MEA_ADAPTOR_DOWNLINK_PORT) { 
            MEA_Uint32 i;

            for (i=0; i<Fpga_init_info_p->downlink_vid_entry;i++) {
                if (MEA_adaptor_init_downlink_services
                                (Fpga_init_info_p,
                                 Fpga_init_info_p->downlink_vid[i],
								 (MEA_Port_t) i) != MEA_OK) { 
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - MEA_adaptor_init_downlink_services failed (i=%d,vid=%d)\n",
                                      __FUNCTION__,i,Fpga_init_info_p->downlink_vid[i]);
                    rc=MEA_ERROR; 
				}
				
            }
		}
 


        if (port == MEA_ADAPTOR_CPU_PORT) { 
            if (MEA_adaptor_init_cpu_services (Fpga_init_info_p) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_adaptor_init_cpu_services failed \n",
                                      __FUNCTION__);
                rc=MEA_ERROR;
            }

        }   
	}

		if ( MEA_adaptor_init_mc_services(Fpga_init_info_p) != MEA_OK ) {
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_adaptor_init_mc_services failed \n",
                                      __FUNCTION__);
                rc=MEA_ERROR;
            }


    return rc;

}

MEA_Status MEA_adaptor_delete_AllServices() {


    MEA_Service_t      db_serviceId;
    MEA_Service_t temp_db_serviceId;
    MEA_ULong_t    db_cookie;
    MEA_Status    rc = MEA_OK;



    if (MEA_API_GetFirst_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK)  {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_GetFirst_Service failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    if (db_cookie == 0) {
       return MEA_OK;
    }

    while (db_cookie != 0) { 


        /* Save the current db_serviceId */
        temp_db_serviceId = db_serviceId;

        /* Make next before delete the object  */
        if (MEA_API_GetNext_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK) {
  	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_GetNext_Service failed\n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        /* Delete the save db_serviceId */
 	    if (MEA_API_Delete_Service(MEA_UNIT_0,temp_db_serviceId)!=MEA_OK) { 
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - failed to delete service %d\n",
                              __FUNCTION__,temp_db_serviceId);
            rc=MEA_ERROR; ;
        }
    }

	/* init Pm DataBase */
	MEA_OS_memset(pm_dbt,0,sizeof(pm_dbt[0])*MEA_MAX_NUM_OF_PM_ID);


    /* T.B.D - If ERROR then flush force the service table */

    return rc;

}

MEA_Status MEA_adaptor_delete_AllQueue() {

	ENET_QueueId_t Queue_id;
	ENET_QueueId_t next_Queue_id;
	ENET_Bool      found;
	ENET_Unit_t    unit_i= ENET_UNIT_0;
	
	/* Get the first Queue */
	if (ENET_GetFirst_Queue  (unit_i,
		                            &Queue_id,
							        NULL, /* entry_o*/
							        &found) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_GetFirst_Queue failed \n",
						   __FUNCTION__);
		return MEA_ERROR;
	}


	/* scan all Queue and delete one by one */
	while(found) {

		next_Queue_id = Queue_id;
        if (ENET_GetNext_Queue   (unit_i,
		                                &next_Queue_id,
		  					            NULL, /* entry_o */
							            &found) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - enet_GetNext_Queue failed \n",
		 		  		       __FUNCTION__);
		    return MEA_ERROR;
		}

		/* deleate onlu non default clusters */
		if ( (Queue_id%4!=0)&& 
			 (Queue_id != MEA_ADAPTOR_UPLINK_DEFAULT_CLUSTER) &&
			 (Queue_id != MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER) &&
			 (Queue_id != MEA_ADAPTOR_CPU_DEFAULT_CLUSTER))
        if (ENET_Delete_Queue   (unit_i,Queue_id) != ENET_OK) { 
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - enet_Delete_Queue failed \n",
		 		  		       __FUNCTION__);
		    return MEA_ERROR;
		}


		Queue_id = next_Queue_id;
	}

    /* Return to caller */
    return MEA_OK; 

}


MEA_Status MEA_adaptor_checks(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) {

    return MEA_OK;


}


typedef enum {
   ENET_ADAPTOR_ENCAPSULATION_TYPE_TAGGED_UNTAGGED,
   ENET_ADAPTOR_ENCAPSULATION_TYPE_TAGGED,
   ENET_ADAPTOR_ENCAPSULATION_TYPE_UNTAGGED,
   ENET_ADAPTOR_ENCAPSULATION_TYPE_LAST
} ENET_adaptor_encapsulation_type_te;


ENET_EncapsulationId_t ENET_adaptor_EncapsulationId[ENET_ADAPTOR_ENCAPSULATION_TYPE_LAST];


ENET_Status ENET_adaptor_create_Encapsulations(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) 
{

   ENET_Encapsulation_dbt                   Encapsulation;
   ENET_EncapsulationId_t                  *EncapsulationId;
   ENET_adaptor_encapsulation_type_te       Encapsulation_type;
   ENET_Uint32                              i;
   ENET_GroupElementId_t                    fieldId;
    
    static ENET_Encapsulation_field_type_te fields[] = 
	{ENET_ENCAPSULATION_FIELD_TYPE_DA,
	 ENET_ENCAPSULATION_FIELD_TYPE_SA,
     ENET_ENCAPSULATION_FIELD_TYPE_ETHERTYPE,
	 ENET_ENCAPSULATION_FIELD_TYPE_VID,
	 ENET_ENCAPSULATION_FIELD_TYPE_PRIORITY,
	 ENET_ENCAPSULATION_FIELD_TYPE_COLOR,
	 ENET_ENCAPSULATION_FIELD_TYPE_IP_DSCP, /* /TOS/PRECEDENCE/TC*/
	 ENET_ENCAPSULATION_FIELD_TYPE_IP_PROTOCOL,
	 ENET_ENCAPSULATION_FIELD_TYPE_DEST_IP,
	 ENET_ENCAPSULATION_FIELD_TYPE_SRC_IP,
	 ENET_ENCAPSULATION_FIELD_TYPE_DEST_L4_PORT,
	 ENET_ENCAPSULATION_FIELD_TYPE_SRC_L4_PORT};

    static ENET_Bool first= ENET_TRUE;


	if (first) {


	   for (Encapsulation_type=0;
	        Encapsulation_type<ENET_ADAPTOR_ENCAPSULATION_TYPE_LAST;
			Encapsulation_type++) {

	       ENET_OS_memset(&Encapsulation,0,sizeof(Encapsulation));
   
           switch (Encapsulation_type) {
		   case ENET_ADAPTOR_ENCAPSULATION_TYPE_TAGGED_UNTAGGED:
               ENET_OS_strcpy (Encapsulation.name,"Tagged_Untagged");
	           Encapsulation.proto.Untagged = ENET_TRUE;
	           Encapsulation.proto.VLAN     = ENET_TRUE;
			   break;
		   case ENET_ADAPTOR_ENCAPSULATION_TYPE_TAGGED:
               ENET_OS_strcpy (Encapsulation.name,"Tagged");
	           Encapsulation.proto.Untagged = ENET_FALSE;
	           Encapsulation.proto.VLAN     = ENET_TRUE;
			   break;
		   case ENET_ADAPTOR_ENCAPSULATION_TYPE_UNTAGGED:
               ENET_OS_strcpy (Encapsulation.name,"Untagged");
	           Encapsulation.proto.Untagged = ENET_TRUE;
	           Encapsulation.proto.VLAN     = ENET_FALSE;
			   break;
		   case ENET_ADAPTOR_ENCAPSULATION_TYPE_LAST:
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				                  "%s - Invalid encapsulation type (%d) \n",
								  __FUNCTION__,Encapsulation_type);
			   return ENET_ERROR;
		   }


	       Encapsulation.proto.Ethernet = ENET_TRUE;
	       Encapsulation.proto.IPv4     = ENET_TRUE;
	       Encapsulation.proto.IPv6     = ENET_TRUE;
	       Encapsulation.proto.TCP      = ENET_TRUE;
	       Encapsulation.proto.UDP      = ENET_TRUE;

	       Encapsulation.fields_group_id = ENET_PLAT_GENERATE_NEW_ID;

   	       Encapsulation.priority_mask.enable = ENET_FALSE;
   	       Encapsulation.priority_mask.value  = 0;

           
	       EncapsulationId = &(ENET_adaptor_EncapsulationId[Encapsulation_type]);
	   
 	       *EncapsulationId = ENET_PLAT_GENERATE_NEW_ID;
           if (ENET_Create_Encapsulation
		                  (ENET_UNIT_0,
	 	                   &Encapsulation,
						   EncapsulationId) != ENET_OK) {
		       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                      "%s - ENET_Create_Encapsulation failed \n",
				  			      __FUNCTION__);
		       return ENET_ERROR;
		   }

	       for (i=0;i<ENET_NUM_OF_ELEMENTS(fields);i++) {
	
		       fieldId = (ENET_GroupElementId_t)(fields[i]); 
	       
		       if (ENET_Create_GroupElement(ENET_UNIT_0,
		                                    Encapsulation.fields_group_id,
								            NULL, /* entry_i */
								            0,     /* entry_i_len */
									        &fieldId) != ENET_OK) {
		           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                          "%s - ENET_Create_GroupElement (encapsulation field) failed\n",
				  		    	     __FUNCTION__);
		           return ENET_ERROR;
			   }
		  }

		}


	   first = ENET_FALSE;
	}
	return ENET_OK;
}


ENET_Status ENET_adaptor_create_Queues(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) 
{
    static ENET_Bool first= ENET_TRUE;
    
	if (first) {

		ENET_PortId_t port_id;
		ENET_Bool     found_port;

		if (ENET_GetFirst_Port  (ENET_UNIT_0,
			                     &port_id,
								 NULL, /* entry_o */
								 &found_port) != ENET_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - ENET_GetFirst_Port failed \n",
							  __FUNCTION__);
		   return ENET_ERROR;
		}

		while (found_port) {

		   ENET_Queue_dbt queue;
		   ENET_QueueId_t queue_id;
		   ENET_Uint8     priority_queue;

	       ENET_OS_memset(&queue,0,sizeof(queue));
   
           ENET_OS_strcpy(queue.name,ENET_PLAT_GENERATE_NEW_NAME);

	       queue_id = (ENET_QueueId_t)port_id;

		   queue.port.type = ENET_QUEUE_PORT_TYPE_PORT;
		   queue.port.id.port = port_id;

		   queue.mode.type = ENET_QUEUE_MODE_TYPE_STRICT;
		   queue.mode.value.strict_priority = 0;

		   queue.MTU = MEA_MTU_SIZE_DEF_VAL;

		   for (priority_queue=0;
		        priority_queue<ENET_NUM_OF_ELEMENTS(queue.pri_queues);
		        priority_queue++) 
				{

				queue.pri_queues[priority_queue].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
		        queue.pri_queues[priority_queue].mode.value.strict_priority = priority_queue;
		        queue.pri_queues[priority_queue].max_q_size_Packets = 64;
		        queue.pri_queues[priority_queue].max_q_size_Byte = 64*1522;

					     //ENET_PLAT_MQS_DEF_VAL(queue_id,priority_queue);
                // T.B.D Lior 
				}


           if (ENET_Create_Queue(ENET_UNIT_0,
	 	                         &queue,
							     &queue_id) != ENET_OK) {
		       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                      "%s - ENET_Create_Queue failed \n",
				 			      __FUNCTION__);
		       return ENET_ERROR;
		   }

			if (ENET_GetNext_Port  (ENET_UNIT_0,
			                        &port_id,
							  	    NULL, /* entry_o */
								    &found_port) != ENET_OK) {
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                       "%s - ENET_GetNext_Port failed  (port_id=%d)\n",
					 		       __FUNCTION__,port_id);
		        return ENET_ERROR;
			}

		}



	   first = ENET_FALSE;
	}
	return ENET_OK;
}


typedef struct {
	ENET_VirtualPortId_t  user;
	ENET_VirtualPortId_t  isp;
} ENET_adaptor_VirtualPortId_user_dbt;

typedef struct {
	 ENET_adaptor_VirtualPortId_user_dbt vdsl    [16];
	 ENET_adaptor_VirtualPortId_user_dbt cpu     [ 1];
	 ENET_adaptor_VirtualPortId_user_dbt downlink[17];
} ENET_adaptor_VirtualPortId_dbt;

ENET_adaptor_VirtualPortId_dbt ENET_adaptor_VirtualPortId;

ENET_Status ENET_adaptor_create_VirtualPorts_vdsl(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) 
{

	static ENET_Bool      first = ENET_TRUE;
	ENET_VirtualPort_dbt  VirtualPort;
	ENET_VirtualPortId_t* VirtualPortId;
    ENET_GroupElementId_t GroupElementId;
	ENET_PortId_t         enet_port,port;
	ENET_Uint32           j;
	ENET_Vid_t            vlan,UserPVid,NetworkPVid;
	ENET_Bool             swap_enable;
	ENET_VirtualPort_classification_field_dbt fieldvalue;

	for (enet_port=MEA_ADAPTOR_FIRST_VDSL_PORT;
	     enet_port<=MEA_ADAPTOR_LAST_VDSL_PORT;
		enet_port++) {
		 
        if (MEA_API_Get_IsPortValid(
			                  enet_port,
							  MEA_TRUE/*silent*/)==MEA_FALSE) {
           continue;
        }    
        port = MEA_ADAPTOR_EXTERNAL_PORT(enet_port);

	   for (j=0;j<2;j++) {

			if (j==0) {
				VirtualPortId = &(ENET_adaptor_VirtualPortId.vdsl[port].user);
			} else {
                VirtualPortId = &(ENET_adaptor_VirtualPortId.vdsl[port].isp );
			}

			if (first) {
			   ENET_OS_memset(&VirtualPort,0,sizeof(VirtualPort));

	           ENET_OS_strcpy(VirtualPort.name,ENET_PLAT_GENERATE_NEW_NAME);

			   VirtualPort.ingress_encapsulation_id = 
				   ENET_adaptor_EncapsulationId[ENET_ADAPTOR_ENCAPSULATION_TYPE_TAGGED];
			   VirtualPort.egress_encapsulation_id = 
				   ENET_adaptor_EncapsulationId[ENET_ADAPTOR_ENCAPSULATION_TYPE_TAGGED];
			   VirtualPort.encapsulation_fields_value_group_id = ENET_PLAT_GENERATE_NEW_ID;

			   VirtualPort.priority_mask.enable = ENET_TRUE;
#ifndef MEA_VER_3_5_1_7_ON
			   VirtualPort.priority_mask.mask   = (j==0) ? 0x01 : 0x07;
#else
			   VirtualPort.priority_mask.mask   = 0x01;
#endif


			   if (j==0) {
			      VirtualPort.queue_id = port;
			   } else {
				  VirtualPort.queue_id = MEA_ADAPTOR_UPLINK_PORT;
			   }
				   
	           VirtualPort.rx_enable = ENET_TRUE;
               VirtualPort.tx_enable = ENET_TRUE;

			 
 		       *VirtualPortId = ENET_PLAT_GENERATE_NEW_ID;

			   if (ENET_Create_VirtualPort (ENET_UNIT_0,
					                        &VirtualPort,
											VirtualPortId)  != ENET_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                  "%s - ENET_Create_VirtualPort failed (vdsl=%d,dir=%d)\n",
									  __FUNCTION__,port,j);
				   return ENET_ERROR;
			   }

			   GroupElementId = ENET_ENCAPSULATION_FIELD_TYPE_VID;
			   fieldvalue.value.value=vlan;
			   if (ENET_Create_GroupElement(ENET_UNIT_0,
                                            VirtualPort.encapsulation_fields_value_group_id,
                                            &fieldvalue,
						  				    sizeof(fieldvalue), 
										    &GroupElementId) != ENET_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                  "%s - ENET_Create_GroupElement for VID failed (vdsl=%d,dir=%d)\n",
									  __FUNCTION__,port,j);
				   return ENET_ERROR;
			   }

			} /* if (first) */

			/* update the vlan of this virtual port */

		    swap_enable = MEA_FALSE;
            UserPVid = Fpga_init_info_p->pvid[MEA_ADAPTOR_EXTERNAL_PORT(port)+1]; 
            NetworkPVid = UserPVid;

            if ( ((Fpga_init_info_p->tag_swap_table_vid_map)  & 
                 (0x00000001 << (MEA_ADAPTOR_EXTERNAL_PORT(port)) )) != 0) {
    

                MEA_Uint32 i;

                for (i=0;
                     i<MEA_NUM_OF_ELEMENTS(Fpga_init_info_p->tag_swap_table_vid[0].Tag_Swap_Table_Entry);
                     i++) 
				{

                   if (Fpga_init_info_p->tag_swap_table_vid[MEA_ADAPTOR_EXTERNAL_PORT(port)].Tag_Swap_Table_Entry[i].active) {
                       swap_enable = MEA_TRUE;
                       UserPVid    = Fpga_init_info_p->tag_swap_table_vid[MEA_ADAPTOR_EXTERNAL_PORT(port)].Tag_Swap_Table_Entry[i].VidUser_32;
	                   NetworkPVid = Fpga_init_info_p->tag_swap_table_vid[MEA_ADAPTOR_EXTERNAL_PORT(port)].Tag_Swap_Table_Entry[i].VidUplink_32;
					   break;
				   }
				}
					 
            }
			

			if (j==0) {
				vlan = UserPVid;
			} else {
				vlan = NetworkPVid;
			}
			fieldvalue.value.value=vlan;
		    if (ENET_Set_GroupElement(ENET_UNIT_0,
                                      VirtualPort.encapsulation_fields_value_group_id,
                                      ENET_ENCAPSULATION_FIELD_TYPE_VID,
                                      &fieldvalue,
									  sizeof(fieldvalue)) != ENET_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                  "%s - ENET__SetElement for VID failed (vdsl=%d,dir=%d)\n",
									  __FUNCTION__,port,j);
				   return ENET_ERROR;
			}
			
               
	   }
   }

   if (first) {
		first = ENET_FALSE;
   }
		 

   return ENET_OK;
}


ENET_Status ENET_adaptor_create_VirtualPorts_downlink(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) 
{


	static ENET_Bool      first = ENET_TRUE;
	ENET_VirtualPort_dbt  VirtualPort;
	ENET_VirtualPortId_t* VirtualPortId;
    ENET_GroupElementId_t GroupElementId;
	ENET_Uint32           j;
    ENET_Uint32           downlink_vid;
	ENET_Vid_t            vlan;
    ENET_VirtualPort_classification_field_dbt fieldvalue;

	for (downlink_vid=0;
	     downlink_vid<Fpga_init_info_p->downlink_vid_entry;
		 downlink_vid++) {
		 
	
	   for (j=0;j<2;j++) {

			if (j==0) {
				VirtualPortId = &(ENET_adaptor_VirtualPortId.downlink[downlink_vid].user);
			} else {
                VirtualPortId = &(ENET_adaptor_VirtualPortId.downlink[downlink_vid].isp );
			}

			if (first) {
			   ENET_OS_memset(&VirtualPort,0,sizeof(VirtualPort));

	           ENET_OS_strcpy(VirtualPort.name,ENET_PLAT_GENERATE_NEW_NAME);

			   VirtualPort.ingress_encapsulation_id = 
				   ENET_adaptor_EncapsulationId[ENET_ADAPTOR_ENCAPSULATION_TYPE_TAGGED];
			   VirtualPort.egress_encapsulation_id = 
				   ENET_adaptor_EncapsulationId[ENET_ADAPTOR_ENCAPSULATION_TYPE_TAGGED];
			   VirtualPort.encapsulation_fields_value_group_id = ENET_PLAT_GENERATE_NEW_ID;

			   VirtualPort.priority_mask.enable = ENET_TRUE;
#ifndef MEA_VER_3_5_1_7_ON
			   VirtualPort.priority_mask.mask   = (j==0) ? 0x01 : 0x07;
#else
			   VirtualPort.priority_mask.mask   = 0x01;
#endif


			   VirtualPort.queue_id = (j==0) ? MEA_ADAPTOR_DOWNLINK_PORT 
				                             : MEA_ADAPTOR_UPLINK_PORT;

	           VirtualPort.rx_enable = ENET_TRUE;
               VirtualPort.tx_enable = ENET_TRUE;
			 
 		       *VirtualPortId = ENET_PLAT_GENERATE_NEW_ID;

			   if (ENET_Create_VirtualPort (ENET_UNIT_0,
					                        &VirtualPort,
											VirtualPortId)  != ENET_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                  "%s - ENET_Create_VirtualPort failed (downlink_vid=%d,dir=%d)\n",
									  __FUNCTION__,downlink_vid,j);
				   return ENET_ERROR;
			   }


			   GroupElementId = ENET_ENCAPSULATION_FIELD_TYPE_VID;
			   fieldvalue.value.value=vlan;
			   if (ENET_Create_GroupElement(ENET_UNIT_0,
                                            VirtualPort.encapsulation_fields_value_group_id,
                                            &fieldvalue,
						  				    sizeof(fieldvalue), 
										    &GroupElementId) != ENET_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                  "%s - ENET_Create_GroupElement for VID failed (downlink_vid=%d,dir=%d)\n",
									  __FUNCTION__,downlink_vid,j);
				   return ENET_ERROR;
			   }

			} /* if (first) */

			/* update the vlan of this virtual port */

			vlan = Fpga_init_info_p->downlink_vid[downlink_vid];
			fieldvalue.value.value=vlan;
		    if (ENET_Set_GroupElement(ENET_UNIT_0,
                                      VirtualPort.encapsulation_fields_value_group_id,
                                      ENET_ENCAPSULATION_FIELD_TYPE_VID,
                                      &fieldvalue,
									  sizeof(fieldvalue)) != ENET_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                  "%s - ENET__SetElement for VID failed (downlink_vid=%d,dir=%d)\n",
									  __FUNCTION__,downlink_vid,j);
				   return ENET_ERROR;
			}
			
               
	   }
   }

   if (first) {
		first = ENET_FALSE;
   }
		 
	return ENET_OK;
}


ENET_Status ENET_adaptor_create_VirtualPorts_cpu(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) 
{

	return ENET_OK;
}



ENET_Status ENET_adaptor_create_VirtualPorts(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) 
{

	ENET_Status rc = ENET_OK;

	MEA_OS_memcpy( MEA_OS_LOG_EVENT,"Create Virtual Ports.vdsls ...\n");
	if (ENET_adaptor_create_VirtualPorts_vdsl(Fpga_init_info_p) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_adaptor_init_VirtualPort_vdsl failed\n",
						   __FUNCTION__);
		rc=ENET_ERROR;
	}

	MEA_OS_memcpy( MEA_OS_LOG_EVENT,"Create Virtual Ports.downlink ,,,\n");
	if (ENET_adaptor_create_VirtualPorts_downlink(Fpga_init_info_p) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_adaptor_init_VirtualPort_vdsl failed\n",
						   __FUNCTION__);
		rc=ENET_ERROR;
	}

	MEA_OS_memcpy( MEA_OS_LOG_EVENT,"Create Virtual Ports.CPU ,,,\n");
	if (ENET_adaptor_create_VirtualPorts_cpu(Fpga_init_info_p) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_adaptor_init_VirtualPort_vdsl failed\n",
						   __FUNCTION__);
		rc=ENET_ERROR;
	}

	return rc;

}




ENET_Status ENET_adaptor_init(MEA_adaptor_Fpga_info_t* Fpga_init_info_p) 
{

    static MEA_Bool first_time = MEA_TRUE;

    MEA_API_Lock();


	MEA_OS_LOG_logMsg( MEA_OS_LOG_EVENT,"ethernity_adaptor started \n");

    
	if (first_time) {
        MEA_OS_memset(&MEA_adaptor_Fpga_init_info,
                      0,
                      sizeof(MEA_adaptor_Fpga_init_info));
        first_time = MEA_FALSE; 

		/* init Pm DataBase */
		MEA_OS_memset(pm_dbt,0,sizeof(pm_dbt[0])*MEA_MAX_NUM_OF_PM_ID);
    }

    if (Fpga_init_info_p == NULL) {

        MEA_Port_t port;
		int i;

        MEA_OS_memset(&MEA_adaptor_Fpga_init_info,
                      0,
                      sizeof(MEA_adaptor_Fpga_init_info));

	   MEA_adaptor_Fpga_init_info.upstream_buffer[0] = 5472;
	   MEA_adaptor_Fpga_init_info.upstream_buffer[1] = 5472;
	   MEA_adaptor_Fpga_init_info.upstream_buffer[2] = 2720;
	   MEA_adaptor_Fpga_init_info.upstream_buffer[3] = 2720;

	   MEA_adaptor_Fpga_init_info.downstream_buffer[0] = 5472;
	   MEA_adaptor_Fpga_init_info.downstream_buffer[1] = 5472;
	   MEA_adaptor_Fpga_init_info.downstream_buffer[2] = 2720;
	   MEA_adaptor_Fpga_init_info.downstream_buffer[3] = 2720;

	   MEA_adaptor_Fpga_init_info.ether_type.mode = 1;
       MEA_adaptor_Fpga_init_info.ether_type.num_entry = 8;
       for( i = 0; i < MEA_adaptor_Fpga_init_info.ether_type.num_entry; i++ ) {
	        MEA_adaptor_Fpga_init_info.ether_type.etherType[i].etherType = 0x9000+i;
	        MEA_adaptor_Fpga_init_info.ether_type.etherType[i].priority  = i;
       }
       MEA_adaptor_Fpga_init_info.ip_version = SP_QOS_IPVERSION_IPV4;
       MEA_adaptor_Fpga_init_info.qos_option   = SP_QOS_OPTION_TOS_STD/*PRECEDENCE*/;
       for( i = 0; i < 8; i++ ) {
		    MEA_adaptor_Fpga_init_info.tos[i].flags |= SP_CONFIGURED/*SP_ACTIVE*/;
		    MEA_adaptor_Fpga_init_info.tos[i].tos = 20 + i;
		    MEA_adaptor_Fpga_init_info.tos[i].priority = i;
       }
       for( i = 0; i < 8; i++ ) {
	  	    MEA_adaptor_Fpga_init_info.precedence[i].flags |= SP_CONFIGURED/*SP_ACTIVE*/;
		    MEA_adaptor_Fpga_init_info.precedence[i].precedence = 7-i;
		    MEA_adaptor_Fpga_init_info.precedence[i].priority = i;
       }
  	   for(i=0; i< 8; i++) {
		    MEA_adaptor_Fpga_init_info.userPriority2TrafficClass[i] =  i / 2;
	   }
#ifdef MEA_ADAPTOR_MULTICAST_SUPPORT
	   for (i=0; i < 4; i++)
	   {
		   MEA_adaptor_Fpga_init_info.mvid[i].vid = 900+i;
           MEA_adaptor_Fpga_init_info.mvid[i].valid = MEA_TRUE;
	   }
#endif
	   MEA_adaptor_Fpga_init_info.mld_priority = 3;
	   MEA_adaptor_Fpga_init_info.mld_cos = 1;
	   MEA_adaptor_Fpga_init_info.mld_enable = MEA_TRUE;


        for (port  = 0 ;
             port <= MEA_MAX_PORT_NUMBER;
             port++) {

            if (MEA_API_Get_IsPortValid(port,MEA_TRUE/*silent*/)==MEA_FALSE) {
                continue;
            }    

            if (/*(port >= MEA_ADAPTOR_FIRST_VDSL_PORT) && */
                (port <= MEA_ADAPTOR_LAST_VDSL_PORT )  ) {

#ifndef MEA_OS_Z1_OLD_BOARD
  				    if ((port % 4) != 0) { 
					    continue;
				    } 
#endif /* MEA_OS_Z1_OLD_BOARD */


               MEA_adaptor_Fpga_init_info.pvid[MEA_ADAPTOR_EXTERNAL_PORT(port)+1] = 
                 MEA_ADAPTOR_EXTERNAL_PORT(port)+100;



	           MEA_adaptor_Fpga_init_info.cosMaxBw[0][0] = 100000; /* 100M */

	           MEA_adaptor_Fpga_init_info.cosMaxBw[MEA_ADAPTOR_EXTERNAL_PORT(port)+1][0] = 100000; /* 100M */

	           MEA_adaptor_Fpga_init_info.cosMaxBw[MEA_ADAPTOR_EXTERNAL_PORT(port)+1][1] =  50000; /*  50M */

	           MEA_adaptor_Fpga_init_info.cosMaxBw[MEA_ADAPTOR_EXTERNAL_PORT(port)+1][2] =  10000; /*  10M */

	           MEA_adaptor_Fpga_init_info.cosMaxBw[MEA_ADAPTOR_EXTERNAL_PORT(port)+1][3] =   1000; /*   1M */

            }

            if (port == MEA_ADAPTOR_CPU_PORT) { 
                MEA_adaptor_Fpga_init_info.pvid    [0] = 1000;
                MEA_adaptor_Fpga_init_info.priority[0] = 6;
                MEA_adaptor_Fpga_init_info.rcont_forward = 1;
            }

            if (port == MEA_ADAPTOR_DOWNLINK_PORT) { 
                MEA_Uint32 i;
                MEA_adaptor_Fpga_init_info.downlink_vid_entry=17;
                for (i=0; i<MEA_adaptor_Fpga_init_info.downlink_vid_entry-1;i++) {
                    MEA_adaptor_Fpga_init_info.downlink_vid[i] = i+1;
                }
                MEA_adaptor_Fpga_init_info.downlink_vid[16] = 2000;
            }

       }

        if (MEA_adaptor_checks(&MEA_adaptor_Fpga_init_info) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_adaptor_checks failed \n",
                              __FUNCTION__);

#if 0
            MEA_API_Unlock();
            return MEA_OK;
#endif

        }

    } else {
       
        if (MEA_OS_memcmp(Fpga_init_info_p ,
                          &MEA_adaptor_Fpga_init_info,
                          sizeof(MEA_adaptor_Fpga_init_info)) == 0) {
           MEA_API_Unlock();
           return MEA_OK;
        }

        if (MEA_adaptor_checks(Fpga_init_info_p) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_adaptor_checks failed \n",
                              __FUNCTION__);
#if 0
            MEA_API_Unlock();
            return MEA_OK;
#endif
        }

        MEA_OS_memcpy(&MEA_adaptor_Fpga_init_info,
                      Fpga_init_info_p ,                    
                      sizeof(MEA_adaptor_Fpga_init_info));
    }


    if (MEA_adaptor_delete_AllServices() != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s - MEA_adaptor_delete_AllServices failed \n",
                           __FUNCTION__);
        MEA_API_Unlock();
        return MEA_ERROR;
    }

    if (MEA_adaptor_delete_AllQueue() != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s - MEA_adaptor_delete_AllQueue failed \n",
                           __FUNCTION__);
        MEA_API_Unlock();
        return MEA_ERROR;
    }

	/* delete all mapping entries */
	{
		MEA_Uint32 index;
		MEA_Bool   found;
		MEA_Mapping_dbt mapping_info;
        for ( index = 0; index < MEA_MAPPING_MAX_ENTRIES; index++ ) {
            if ( MEA_API_Get_Mapping ( MEA_UNIT_0,
                                       index,
                                       &mapping_info,
                                       &found ) != MEA_OK ) {
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s -  MEA_API_Get_Mapping failed\n",
                                   __FUNCTION__);
                 return MEA_OK;
        	}         
    
            if ( found ) { 
                if ( MEA_API_Delete_Mapping(MEA_UNIT_0, index) != MEA_OK )
                {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s -  MEA_API_Delete_Mapping failed\n",
                                  __FUNCTION__);
                    return MEA_OK;
        	    }     
        	}
        }
	}


#if 0
    MEA_OS_memcpy( MEA_OS_LOG_EVENT,"Create Queues ,,,\n");
    if (ENET_adaptor_create_Queues(&MEA_adaptor_Fpga_init_info) != ENET_OK) {
        MEA_OS_memcpy (MEA_OS_LOG_ERROR,
                           "%s - ENET_adaptor_init_queues failed \n",
                           __FUNCTION__);
        ENET_Unlock();
        return ENET_ERROR;
    }

    MEA_OS_memcpy( MEA_OS_LOG_EVENT,"Create Encapsulations ,,,\n");
    if (ENET_adaptor_create_Encapsulations(&MEA_adaptor_Fpga_init_info) != ENET_OK) {
        MEA_OS_memcpy (MEA_OS_LOG_ERROR,
                           "%s - ENET_adaptor_init_encapsulations failed \n",
                           __FUNCTION__);
        ENET_Unlock();
        return ENET_ERROR;
    }

    MEA_OS_memcpy( MEA_OS_LOG_EVENT,"Create VirtualPorts ,,,\n");
    if (ENET_adaptor_create_VirtualPorts(&MEA_adaptor_Fpga_init_info) != ENET_OK) {
        MEA_OS_memcpy (MEA_OS_LOG_ERROR,
                           "%s - ENET_adaptor_init_encapsulations failed \n",
                           __FUNCTION__);
        ENET_Unlock();
        return ENET_ERROR;
    }
#endif


    if (MEA_adaptor_init_mapping(&MEA_adaptor_Fpga_init_info) != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s - MEA_adaptor_init_mapping failed \n",
                           __FUNCTION__);
        MEA_API_Unlock();
        return MEA_ERROR;
    }

    if (MEA_adaptor_init_queue(&MEA_adaptor_Fpga_init_info) != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s - MEA_adaptor_init_queue failed \n",
                           __FUNCTION__);
        MEA_API_Unlock();
        return MEA_ERROR;
    }
	

    if (MEA_adaptor_init_ports(&MEA_adaptor_Fpga_init_info) != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s - MEA_adaptor_init_ports failed \n",
                           __FUNCTION__);
        MEA_API_Unlock();
        return MEA_ERROR;
    }

		if (MEA_adaptor_Set_DownStreem_queue(MEA_adaptor_Fpga_init_info.downstream_buffer,&MEA_adaptor_Fpga_init_info) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - MEA_adaptor_Set_DownStreem_queue for \n",
						  __FUNCTION__);
			MEA_API_Unlock();
			return MEA_ERROR;
                 }


    if (MEA_adaptor_init_services(&MEA_adaptor_Fpga_init_info) != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s - MEA_adaptor_init_ports failed \n",
                           __FUNCTION__);
        MEA_API_Unlock();
        return MEA_ERROR;
    }

	MEA_OS_LOG_logMsg( MEA_OS_LOG_EVENT,"ethernity_adaptor ended \n");

    MEA_API_Unlock();
    return MEA_OK;

}

#define MEA_Collect_all_counters_enable ENET_Collect_all_counters_enable
extern MEA_Bool MEA_Collect_all_counters_enable;

MEA_Status ENET_adaptor_CounterPerPortGet(int port_i,
                                         MEA_adaptor_GettingRec_ServClass_struct* ptr_o) 

{

   MEA_Counters_RMON_dbt        Counters_RMON_entry;
   MEA_Counters_IngressPort_dbt Counters_IngressPort_entry;
#if 0
   MEA_Counters_Queue_dbt  Counters_Queue_entry;
#endif
   static MEA_adaptor_GettingRec_ServClass_struct collected_counters[MEA_MAX_PORT_NUMBER+1];
   MEA_Port_t port, port_rx;
   MEA_Uint32 pause_counter = 0;
   MEA_Uint16 pmId;

	if(poling_active)
		return MEA_OK;
	poling_active = MEA_TRUE;

   /* Lock the driver */
   MEA_API_Lock();

#if 0
   /* Check if the periodic is enable */
   if (!MEA_Collect_all_counters_enable) {
      MEA_API_Unlock();
	  poling_active = MEA_FALSE;
      return MEA_OK;
   } 
#endif

   /* In  the first round of this loop collect all the counters */   
   /* ptr_o == NULL, collect from HW */
   if( ptr_o == NULL ) {
	   if( MEA_adaptor_API_Collect_Counters_All(MEA_UNIT_0) != MEA_OK ){
           MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                              "%s - MEA_adaptor_API_Collect_Counters_All failed "
                              "- disable periodic \n",
                              __FUNCTION__);
           MEA_Collect_all_counters_enable=MEA_FALSE;
           MEA_API_Unlock();
		   poling_active = MEA_FALSE;
           return MEA_ERROR;
       }

	   /* clear all entries first, egress counter value will be saved to
	      entry indexed by ingress port number */
	   MEA_OS_memset(collected_counters,0,sizeof(collected_counters));

       for (port  = 0 ; port <= MEA_MAX_PORT_NUMBER; port++) {

           if (MEA_API_Get_IsPortValid(port,MEA_TRUE/*silent*/)==MEA_FALSE) {
               continue;
           }    


           if (MEA_API_Get_Counters_RMON(MEA_UNIT_0,port,&Counters_RMON_entry) != MEA_OK) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - MEA_API_Get_Counters_RMON failed (port=%d)\n",
                                 __FUNCTION__,(int)port);
               MEA_Collect_all_counters_enable = MEA_FALSE;
               MEA_API_Unlock();
			   poling_active = MEA_FALSE;
               return MEA_ERROR;
           }



           if (MEA_API_Get_Counters_IngressPort(MEA_UNIT_0,port,&Counters_IngressPort_entry) != MEA_OK) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - MEA_API_Get_Counters_IngressPort failed (port=%d)\n",
                                 __FUNCTION__,port);
               MEA_Collect_all_counters_enable = MEA_FALSE;
               MEA_API_Unlock();
			   poling_active = MEA_FALSE;
               return MEA_ERROR;
           }
#if 0
           if (MEA_API_Get_Counters_EgressPort(MEA_UNIT_0,port,&Counters_EgressPort_entry) != MEA_OK) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - MEA_API_Get_Counters_EgressPort failed (port=%d)\n",
                                 __FUNCTION__,port);
               MEA_Collect_all_counters_enable = MEA_FALSE;
               MEA_API_Unlock();
			   poling_active = MEA_FALSE;
               return MEA_ERROR;
           }
#endif
           port_rx = ConverPortFromInterface(port);
           collected_counters[port].Ingress_Rx_Frame_Cnt      = Counters_RMON_entry.Rx_Pkts.s.lsw;
           collected_counters[port].Ingress_Rx_Byte_Cnt       = Counters_RMON_entry.Rx_Bytes.s.lsw;
           collected_counters[port].Ingress_Discard_Frame_Cnt = Counters_RMON_entry.Rx_CRC_Err.s.lsw;
           collected_counters[port_rx].Egress_Tx_Frame_Cnt       = Counters_RMON_entry.Tx_Pkts.s.lsw;
	       collected_counters[port].FCS_Frame_Cnt             = Counters_RMON_entry.Rx_CRC_Err.s.lsw;
	       collected_counters[port_rx].Error_Frame_Cnt           = /*Counters_EgressPort_entry.MTU_DRP.s.lsw*/0;
#if 0 /* T.B.D */
	       collected_counters[port].Error_Frame_Cnt          += Counters_IngressPort_entry.MTU_DRP.s.ls
#endif
#if 0 /* T.B.D */
	       collected_counters[port].Pause_Frame_Cnt           = Counters_IngressPort_entry.PausePacket.s.lsw;
	       collected_counters[port].Pause_Frame_Cnt          += Counters_EgressPort_entry.PausePacket.s.lsw;
#endif


	   } /* for (port  = 0 ; port <= MEA_MAX_PORT_NUMBER; port++) */ 

       /* Get service counters */
		for (pmId=0;pmId<MEA_MAX_NUM_OF_PM_ID;pmId++)
		{
			if ((pm_dbt[pmId].isPmValid) &&
			    (!pm_dbt[pmId].service_Uses_DSE)) {
							if( pause_counter++ == 24 )
							{
								MEA_API_Unlock();
								pause_counter = 0;
								MEA_OS_sleep(0, 100*1000*1000);	/* 100ms */
								MEA_API_Lock();
							}

			   /* calculate the ingress counters of the service source port */
			   collected_counters[pm_dbt[pmId].ingressPort].perClassCnt[(pm_dbt[pmId].servicePri >> 1)].Ingress_Rx_Frame_Cnt +=
                           ( pm_dbt[pmId].pmCounter.green_fwd_pkts.s.lsw         +
                             pm_dbt[pmId].pmCounter.yellow_or_red_fwd_pkts.s.lsw +
                             pm_dbt[pmId].pmCounter.green_dis_pkts.s.lsw         +
                             pm_dbt[pmId].pmCounter.yellow_or_red_dis_pkts.s.lsw );
			   collected_counters[pm_dbt[pmId].ingressPort].perClassCnt[(pm_dbt[pmId].servicePri >> 1)].Ingress_Rx_Byte_Cnt += 
                           ( pm_dbt[pmId].pmCounter.green_fwd_bytes.s.lsw         +
                             pm_dbt[pmId].pmCounter.yellow_or_red_fwd_bytes.s.lsw +
                             pm_dbt[pmId].pmCounter.green_dis_bytes.s.lsw         +
                             pm_dbt[pmId].pmCounter.yellow_or_red_dis_bytes.s.lsw );

			   collected_counters[pm_dbt[pmId].ingressPort].Ingress_Discard_Frame_Cnt += 
                           ( pm_dbt[pmId].pmCounter.green_dis_pkts.s.lsw         +
                             pm_dbt[pmId].pmCounter.yellow_or_red_dis_pkts.s.lsw );
		       collected_counters[pm_dbt[pmId].ingressPort].perClassCnt[(pm_dbt[pmId].servicePri >> 1)].Ingress_Discard_Frame_Cnt += 
                           ( pm_dbt[pmId].pmCounter.green_dis_pkts.s.lsw         +
                             pm_dbt[pmId].pmCounter.yellow_or_red_dis_pkts.s.lsw );
               /* calculate the egress counters of the service output port */
			   collected_counters[pm_dbt[pmId].egressPort].perClassCnt[(pm_dbt[pmId].servicePri >> 1)].Egress_Tx_Frame_Cnt += 	
                           ( pm_dbt[pmId].pmCounter.green_fwd_pkts.s.lsw         +
                             pm_dbt[pmId].pmCounter.yellow_or_red_fwd_pkts.s.lsw );				
			}
		}
       MEA_API_Unlock();
	   poling_active = MEA_FALSE;
       return MEA_OK;
   } /*  if ((collected == MEA_FALSE) ||(port_i <= collected_last_port)) */

   /* Convert the port in user format to MEA driver format */
   switch (port_i) {
   case  1:
   case  2:
   case  3:
   case  4:
   case  5:
   case  6:
   case  7:
   case  8:
   case  9:
   case 10:
   case 11:
   case 12:
   case 13:
   case 14:
   case 15:
   case 16:
	   port = MEA_ADAPTOR_INTERNAL_PORT(port_i-1);
       break;
   case 17:  
       port = MEA_ADAPTOR_DOWNLINK_PORT; 
       break;
   case 18:  
       port = MEA_ADAPTOR_UPLINK_PORT; 
       break;
   case 19:  
       port = MEA_ADAPTOR_CPU_PORT; 
       break;
   default:
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - called with unknown port=%d\n",
                         __FUNCTION__,port_i);
       MEA_Collect_all_counters_enable = MEA_FALSE;
       MEA_API_Unlock();
	   poling_active = MEA_FALSE;
       return MEA_ERROR;
   }

   MEA_OS_memcpy(ptr_o,&(collected_counters[port]),sizeof(*ptr_o));

   /* Unlock the driver */
   MEA_API_Unlock();


   /* return to Caller */
   poling_active = MEA_FALSE;
   return MEA_OK;

} 

MEA_Bool enet_Is_port_enable(int port_i,MEA_Uint32	port_close_bitmap)
{
   
   switch (port_i) {

   case 17:  
   case 18:  
   case 19: 
	   return ((port_close_bitmap&(1<<(port_i)))==0); /* 0 - LinkUp , 1 - LinkDown */ 
	   break;
   case  1:
   case  2:
   case  3:
   case  4:
   case  5:
   case  6:
   case  7:
   case  8:
   case  9:
   case 10:
   case 11:
   case 12:
   case 13:
   case 14:
   case 15:
   case 16:
	   return ((port_close_bitmap&(1<<(port_i-1)))==0); /* 0 - LinkUp , 1 - LinkDown */ 
	   break;
   default:
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - called with unknown port=%d\n",
                         __FUNCTION__,port_i);
       return MEA_FALSE;
   }

}

MEA_Status enet_adaptor_LowLevel_SetAdminAndShaper(MEA_Port_t port,MEA_Bool isEnable,MEA_Uint32 speed /* kbps*/) 
{
	MEA_IngressPort_Entry_dbt ingressPort_entry;
    MEA_EgressPort_Entry_dbt  egressPort_entry;

	MEA_Port_t interfacePort = port;
    
	interfacePort = ConverPortToInterface(port);

   /* Lock the driver */
   MEA_API_Lock();

    if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
                                      interfacePort,
                                      &ingressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_InressPort_Entry for port %d failed\n",
                          __FUNCTION__,
                          interfacePort);
        return MEA_ERROR;
    }
    if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     interfacePort,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry for port %d failed\n",
                          __FUNCTION__,
                          interfacePort);
        return MEA_ERROR;
    }

	ingressPort_entry.rx_enable = isEnable;
	egressPort_entry.tx_enable  = isEnable;
	egressPort_entry.flush      = !isEnable; 

	if(isEnable){
		/* set shaper for VDSL port */
		egressPort_entry.shaper_info.CIR             = speed*950/*1000*/; /* kbps*/
		egressPort_entry.shaper_info.CBS             = 10000;
		egressPort_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
		egressPort_entry.shaper_info.overhead        = 20; /* 12 + 8 */
		egressPort_entry.shaper_info.Cell_Overhead   = 0;
		egressPort_entry.Shaper_compensation         = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
		egressPort_entry.shaper_enable			     = ENET_TRUE;
	}


    if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                      interfacePort,
                                      &ingressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_InressPort_Entry for port %d failed\n",
                          __FUNCTION__,
                          interfacePort);
        return MEA_ERROR;
    }
    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     interfacePort,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry for port %d failed\n",
                          __FUNCTION__,
                          interfacePort);
        return MEA_ERROR;
    }
   /* Unlock the driver */
   MEA_API_Unlock();
   return MEA_OK;
}

MEA_Status enet_adaptor_LowLevel_SetAdmin(MEA_Port_t port,MEA_Bool isEnable)
{
	MEA_IngressPort_Entry_dbt ingressPort_entry;
    MEA_EgressPort_Entry_dbt  egressPort_entry;
	MEA_Port_t interfacePort = port;
    
    
	
	interfacePort = ConverPortToInterface(port);

   /* Lock the driver */
   MEA_API_Lock();

    if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
                                      interfacePort,
                                      &ingressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_InressPort_Entry for port %d failed\n",
                          __FUNCTION__,
                          interfacePort);
        return MEA_ERROR;
    }
    if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                     interfacePort,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_EgressPort_Entry for port %d failed\n",
                          __FUNCTION__,
                          interfacePort);
        return MEA_ERROR;
    }

	ingressPort_entry.rx_enable = isEnable;
	egressPort_entry.tx_enable  = isEnable;
	egressPort_entry.flush      = !isEnable; 

    if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                      interfacePort,
                                      &ingressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_InressPort_Entry for port %d failed\n",
                          __FUNCTION__,
                          interfacePort);
        return MEA_ERROR;
    }
    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                     interfacePort,
                                     &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_EgressPort_Entry for port %d failed\n",
                          __FUNCTION__,
                          interfacePort);
        return MEA_ERROR;
    }
   /* Unlock the driver */
   MEA_API_Unlock();
   return MEA_OK;
}

MEA_Status ENET_adaptor_Link_Status_Change(int port_i,
										   MEA_adaptor_Fpga_info_t* Fpga_init_info_p)
{
	
	MEA_Port_t port;
	MEA_Uint16 vdslPort;
    MEA_IngressPort_Entry_dbt ingressPort_entry;
    MEA_EgressPort_Entry_dbt  egressPort_entry;
	MEA_Bool isUpLinkEnable = enet_Is_port_enable(NEC_FPGA_API_UPLINK_PORT,Fpga_init_info_p->port_close); 
	MEA_Bool isPortEnable   = enet_Is_port_enable(port_i,Fpga_init_info_p->port_close);
	MEA_Bool isVdslEnable ;


/* Convert the port in user format to MEA driver format */
   switch (port_i) {

   case 17:  
       port = MEA_ADAPTOR_DOWNLINK_PORT; 
       break;
   case 18:  
       port = MEA_ADAPTOR_UPLINK_PORT; 
       break;
   case  1:
   case  2:
   case  3:
   case  4:
   case  5:
   case  6:
   case  7:
   case  8:
   case  9:
   case 10:
   case 11:
   case 12:
   case 13:
   case 14:
   case 15:
   case 16:
   case 19:  
   default:
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - called with unknown port=%d\n",
                         __FUNCTION__,port_i);
       return MEA_ERROR;
   }

   /* Lock the driver */
   MEA_API_Lock();


  if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
                                    port,
                                    &ingressPort_entry) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Get_InressPort_Entry for port %d failed\n",
                         __FUNCTION__,
                         port);
       MEA_API_Unlock();
       return MEA_ERROR;
   }
   if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                                 port,
                                 &egressPort_entry) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Get_EgressPort_Entry for port %d failed\n",
                      __FUNCTION__,
                      port);
		return MEA_ERROR;
	}
   	if((port != MEA_ADAPTOR_DOWNLINK_PORT)||(isUpLinkEnable != MEA_FALSE))
	{
		ingressPort_entry.rx_enable = isPortEnable;
		egressPort_entry.tx_enable  = isPortEnable;
		egressPort_entry.flush      = !isPortEnable; 
		if (port == MEA_ADAPTOR_UPLINK_PORT)
	    {
		   /* set shaper for uplink */
		   egressPort_entry.shaper_info.CIR             = Fpga_init_info_p->uplink_policy_set*1000000;
		   egressPort_entry.shaper_info.CBS             = 10000;
		   egressPort_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
		   egressPort_entry.shaper_info.overhead        = 20; /* 12 + 8 */
		   egressPort_entry.shaper_info.Cell_Overhead   = 0;
		   egressPort_entry.Shaper_compensation         = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
		   egressPort_entry.shaper_enable			    = ENET_TRUE; 
	    }
	}

   if ((isPortEnable /* link up */ ) &&
       ((port == MEA_ADAPTOR_UPLINK_PORT) || (port == MEA_ADAPTOR_DOWNLINK_PORT))) { 

       /* set speed configuration */
	   switch (port) {
	   case MEA_ADAPTOR_UPLINK_PORT:
		  switch (Fpga_init_info_p->ul_speed) { 
		  case 1:
              ingressPort_entry.GigaDisable = 2; 
              break;
		  case 2:
		     ingressPort_entry.GigaDisable = 1;
			 break;
	      case 3:
		     ingressPort_entry.GigaDisable = 0;
			 break;
		  case 0: /* relevan only in link down */
		  default:
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				  "%s - Invalid speed_i (%d) for port (%d) \n",
				  __FUNCTION__,Fpga_init_info_p->ul_speed,port_i);
              MEA_API_Unlock();
              return MEA_ERROR;
			  break;
		  }
		  break;

	   case MEA_ADAPTOR_DOWNLINK_PORT:
		  switch (Fpga_init_info_p->dl_speed) { 
		  case 1:
		     ingressPort_entry.GigaDisable = 1; 
			 break;
	      case 2:
		     ingressPort_entry.GigaDisable = 0; 
			 break;
		  case 0: /* relevan only in link down */
		  default:
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				  "%s - Invalid speed_i (%d) for port (%d) \n",
				  __FUNCTION__,Fpga_init_info_p->dl_speed,port_i);
              MEA_API_Unlock();
              return MEA_ERROR;
			  break;
		  }
		  break;
	   default:
		   break;
	   }
   }

   if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
                                     port,
                                     &ingressPort_entry) != MEA_OK) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - MEA_API_Set_InressPort_Entry for port %d failed\n",
                           __FUNCTION__,
                           port);
         MEA_API_Unlock();
         return MEA_ERROR;
    }
    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                                 port,
                                 &egressPort_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Set_EgressPort_Entry for port %d failed\n",
                      __FUNCTION__,
                      port);
		return MEA_ERROR;
	}

   /* Unlock the driver */
   MEA_API_Unlock();

   if (port == MEA_ADAPTOR_UPLINK_PORT)
   {
	   for (vdslPort = 1 ; 
		    vdslPort <= 16 ;
			vdslPort++ )
	   {
		  isVdslEnable =enet_Is_port_enable(vdslPort,Fpga_init_info_p->port_close); 
		  if (!isUpLinkEnable)
			  isVdslEnable = MEA_FALSE;

		  if (enet_adaptor_LowLevel_SetAdmin(MEA_ADAPTOR_INTERNAL_PORT(vdslPort-1),isVdslEnable) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - MEA_API_Set_EgressPort_Entry for port %d failed\n",
                      __FUNCTION__,
                      port);
		        return MEA_ERROR;
	      }
	   }
	  isPortEnable   = enet_Is_port_enable(NEC_FPGA_API_DOWNLINK_PORT,Fpga_init_info_p->port_close);;
	  if (!isUpLinkEnable)
		  isPortEnable = MEA_FALSE;

	  if (enet_adaptor_LowLevel_SetAdmin(MEA_ADAPTOR_DOWNLINK_PORT,isPortEnable)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                  "%s - MEA_API_Set_EgressPort_Entry for port %d failed\n",
                  __FUNCTION__,
                  port);
	        return MEA_ERROR;
      }

   }

   return MEA_OK;
}

MEA_Status ENET_adaptor_Vdsl_Link_Status_Change(int port_i,
										        MEA_adaptor_Fpga_info_t* fpga_info_p,
												MEA_Uint32 speed /* k bps */) 
{

	MEA_Port_t port;
	MEA_Bool isUplinkEnable = enet_Is_port_enable(NEC_FPGA_API_UPLINK_PORT,fpga_info_p->port_close);
	MEA_Bool isVdslEnable   = MEA_TRUE; 

/* Convert the port in user format to MEA driver format */
   switch (port_i) {
   case  1:
   case  2:
   case  3:
   case  4:
   case  5:
   case  6:
   case  7:
   case  8:
   case  9:
   case 10:
   case 11:
   case 12:
   case 13:
   case 14:
   case 15:
   case 16:
	   port = MEA_ADAPTOR_INTERNAL_PORT(port_i-1);
       break;

   case 17:  
   case 18:  
   case 19:  
   default:
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - called with unknown port=%d\n",
                         __FUNCTION__,port_i);
       return MEA_ERROR;
   }
   /* if the uplink is down the VDSL shall not be changed to up ( but the speed must be updated) */
	if (!isUplinkEnable)
		isVdslEnable = MEA_FALSE;
	else
		isVdslEnable = enet_Is_port_enable(port_i,fpga_info_p->port_close);
	if (enet_adaptor_LowLevel_SetAdminAndShaper(port,isVdslEnable,speed)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_InressPort_Entry for port %d failed\n",
                          __FUNCTION__,
                          port);
        return MEA_ERROR;
    }
   return MEA_OK;
}


static MEA_Service_t MEA_adaptor_loopback_serviceId[4];

/* Create Service From Specific VDSL Port to RCONT Port Temporarily */
MEA_Status MEA_adaptor_loopback_start(MEA_adaptor_Fpga_info_t* Fpga_init_info_p,
                                          MEA_Port_t port) {

	MEA_Uint32                     *pPortGrp;
	MEA_Uint32                      shiftWord = 0x00000001;
	MEA_Service_Entry_Key_dbt		service_id_key;
	MEA_Service_Entry_Data_dbt   	service_id_data;
	MEA_EgressHeaderProc_Entry_dbt  ehp_entry;
	MEA_OutPorts_Entry_dbt 	        out_ports_tbl_entry;
	MEA_Policer_Entry_dbt 		    sla_params_entry;
	MEA_Uint32 CoS;

	MEA_Status rc=MEA_OK;
	MEA_EgressHeaderProc_Array_Entry_dbt ehp_array;
	MEA_EHP_Info_dbt ehp_info;

	MEA_OS_memset(&ehp_info,0,sizeof(ehp_info));
	MEA_OS_memset(&ehp_array,0,sizeof(ehp_array));

	port = MEA_ADAPTOR_INTERNAL_PORT(port);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (MEA_API_Get_IsPortValid(port,MEA_TRUE/*silent*/)==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - invalid port (%d) \n",
                          __FUNCTION__,port);
        return MEA_ERROR;
    }


    if (/*(port < MEA_ADAPTOR_FIRST_VDSL_PORT) || */
        (port > MEA_ADAPTOR_LAST_VDSL_PORT )  ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - port (%d) is not in VDSL port range %d..%d \n",
                          __FUNCTION__,
                          port,
                          MEA_ADAPTOR_FIRST_VDSL_PORT,
                          MEA_ADAPTOR_LAST_VDSL_PORT);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Lock the driver */
    MEA_API_Lock();

	MEA_OS_memset(&(MEA_adaptor_loopback_serviceId[0]),0,sizeof(MEA_adaptor_loopback_serviceId));

    /* loop on all CoS */ 
    /* Note: Queue = CoS - 1 */
    for (CoS = 1; CoS < 8 ; CoS += 2) {

        /* loopback packet will always be with priority 0 */

	    /* zeros all structures */
	    MEA_OS_memset(&service_id_key      , 0 , sizeof(service_id_key));
	    MEA_OS_memset(&service_id_data     , 0 , sizeof(service_id_data));
	    MEA_OS_memset(&ehp_entry           , 0 , sizeof(ehp_entry));
	    MEA_OS_memset(&out_ports_tbl_entry , 0 , sizeof(out_ports_tbl_entry));
	    MEA_OS_memset(&sla_params_entry    , 0 , sizeof(sla_params_entry));

	    /* set default stamping info for any service */
	    ehp_entry.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	    ehp_entry.eth_stamping_info.color_bit0_loc   = 12;
	    ehp_entry.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	    ehp_entry.eth_stamping_info.color_bit1_loc   = 0;
	    ehp_entry.eth_stamping_info.pri_bit0_valid   = MEA_FALSE;
	    ehp_entry.eth_stamping_info.pri_bit0_loc     = 13;
	    ehp_entry.eth_stamping_info.pri_bit1_valid   = MEA_FALSE;
	    ehp_entry.eth_stamping_info.pri_bit1_loc     = 14;
	    ehp_entry.eth_stamping_info.pri_bit2_valid   = MEA_FALSE;
	    ehp_entry.eth_stamping_info.pri_bit2_loc     = 15;

        /* set service data information */
        service_id_data.ADM_ENA           	  =	MEA_FALSE;
	    service_id_data.DSE_forwarding_enable = MEA_FALSE;
	    service_id_data.DSE_learning_enable   = MEA_FALSE;
        service_id_data.pmId                  = 0; /* auto assign */
        service_id_data.tmId                  = 0; /* auto assign */
	    service_id_data.editId                = 0; /* auto assign */
        service_id_data.valid                 = MEA_TRUE ;


		/* define service key source port */
		service_id_key.src_port	= port;
		
		/* Define the service Key priority */
		service_id_key.pri      = CoS;

		/* Define the service Key net_tag, = RCONT VID */
		service_id_key.net_tag  = MEA_ADAPTOR_NET_TAG_WILDCARD_FOR_TAGGED_PACKET | Fpga_init_info_p->pvid[0];

		/* define the output port of the service */
		pPortGrp = (MEA_Uint32 *)(&out_ports_tbl_entry.out_ports_0_31);
		*(pPortGrp+(MEA_ADAPTOR_CPU_DEFAULT_CLUSTER/32)) |=
				(shiftWord<<(MEA_ADAPTOR_CPU_DEFAULT_CLUSTER%32));

		/* define the policer parameters for this service */
		sla_params_entry.CIR         =  10100000; 
		sla_params_entry.EIR         =  0;
		sla_params_entry.CBS         =  10000;
		sla_params_entry.EBS         =	0;
		sla_params_entry.cup	     =  MEA_FALSE;
		sla_params_entry.comp	     =	MEA_POLICER_COMP_ENFORCE;	
		sla_params_entry.color_aware =  MEA_FALSE;

		ehp_array.num_of_entries = 1;		
		MEA_OS_memcpy(&(ehp_info.output_info),&out_ports_tbl_entry, sizeof(ehp_info.output_info));
		MEA_OS_memcpy(&(ehp_info.ehp_data),&ehp_entry, sizeof(ehp_info.ehp_data));
		ehp_array.ehp_info = &ehp_info;

		/* Create the Service */
		if (MEA_API_Create_Service(MEA_UNIT_0,
								   &service_id_key,
								   &service_id_data,
								   &out_ports_tbl_entry,
								   &sla_params_entry,
								   &ehp_array,
								   &(MEA_adaptor_loopback_serviceId[(CoS-1)/2])) != MEA_OK) {
						MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
										  "%s - MEA_API_Create_Sercice failed\n",
										  __FUNCTION__);
						/* Unlock the driver */
						MEA_API_Unlock();
						return MEA_ERROR;
		}
		pm_dbt[service_id_data.pmId].isPmValid        = MEA_FALSE;//!!IZIK MEA_TRUE;
		pm_dbt[service_id_data.pmId].egressPort       = MEA_ADAPTOR_CPU_PORT;
		pm_dbt[service_id_data.pmId].ingressPort      = port;
		pm_dbt[service_id_data.pmId].service_Uses_DSE = service_id_data.ADM_ENA;
		pm_dbt[service_id_data.pmId].serviceId        = (MEA_adaptor_loopback_serviceId[(CoS-1)/2]);
		pm_dbt[service_id_data.pmId].servicePri       = service_id_key.pri; 
	}

	/* Unlock the driver */
    MEA_API_Unlock();

    return MEA_OK;
}


MEA_Status MEA_adaptor_loopback_stop(MEA_adaptor_Fpga_info_t* Fpga_init_info_p,
                                          MEA_Port_t port) {

    MEA_Status    rc = MEA_OK;
	int i;

    MEA_Service_Entry_Key_dbt      service_key;
    MEA_Service_Entry_Data_dbt     service_data;

    /* Lock the driver */
    MEA_API_Lock();


     for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_adaptor_loopback_serviceId);i++) {

		 if (MEA_adaptor_loopback_serviceId[i]) {
			 /* Get PmId */
	         if (MEA_API_Get_Service(MEA_UNIT_0,
				                     MEA_adaptor_loopback_serviceId[i],
									 &service_key,
									 &service_data,
									 NULL,
									 NULL,
									 NULL)!=MEA_OK) {
			      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                "%s - failed to MEA_API_Get_Service service %d\n",
	                                __FUNCTION__,MEA_adaptor_loopback_serviceId[i]);
	              rc=MEA_ERROR;
	         }
	         /* Delete the save db_serviceId */
	         if (MEA_API_Delete_Service(MEA_UNIT_0,MEA_adaptor_loopback_serviceId[i])!=MEA_OK) {
			      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                "%s - failed to delete service %d\n",
	                                __FUNCTION__,MEA_adaptor_loopback_serviceId[i]);
	              rc=MEA_ERROR;
	         }
			 pm_dbt[service_data.pmId].isPmValid        = MEA_FALSE;
		     pm_dbt[service_data.pmId].egressPort       = 0;
	         pm_dbt[service_data.pmId].ingressPort      = 0;
		     pm_dbt[service_data.pmId].service_Uses_DSE = MEA_FALSE;
		     pm_dbt[service_data.pmId].serviceId        = 0;
		     pm_dbt[service_data.pmId].servicePri       = 0; 

		 }
		
		 MEA_adaptor_loopback_serviceId[i]=0;
	}
	        

	/* Unlock the driver */
    MEA_API_Unlock();

    return rc;
}


MEA_Status MEA_adaptor_forwarder_entry_add(
	MEA_Uint8*	mac,
	MEA_Uint32	portmap) {

	MEA_DSE_Entry_dbt	entry;
	MEA_Port_t			i;
	MEA_Port_t			port;
	MEA_Policer_Entry_dbt          Policer_Entry;
	MEA_EgressHeaderProc_Entry_dbt EHP_Entry;

	MEA_Status rc = MEA_OK;

	/* Lock the driver */
    MEA_API_Lock();

	MEA_OS_memset(&entry, 0, sizeof(entry));
	MEA_OS_memset(&Policer_Entry, 0, sizeof(Policer_Entry));
	MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));

	entry.key.vlan_vpn.type  = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	MEA_OS_memcpy(entry.key.mac_plus_vpn.MAC.b, mac, sizeof(entry.key.mac_plus_vpn.MAC.b));
	entry.key.mac_plus_vpn.VPN = 0;

	entry.data.aging = 3;
	entry.data.static_flag  = 1;
	entry.data.sa_discard = 0;
	entry.data.valid = 1;
	
	for ( i = 0; i < 16; i ++ )
	{
		if( portmap & (1<<i) )
		{
			port = ConverPortToInterface(MEA_ADAPTOR_INTERNAL_PORT(i));
			
			if (MEA_API_Get_IsPortValid(port,MEA_TRUE) != MEA_TRUE) {
			    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                              "%s - Port id %d is not define\n",
	                              __FUNCTION__, port);
            	/* Unlock the driver */
                MEA_API_Unlock();
				return MEA_ERROR;
			}
			
			/* check if the out_port is valid */
			((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port/32] |=  
			(1 << (port%32));
		}
	}

	/* Always add downlink Port to the output port map */
	((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER/32] |=  
	(1 << (MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER%32));
	
	if (MEA_API_Create_DSE_Entry(MEA_UNIT_0,&entry, NULL, NULL) != MEA_OK) {
			    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                              "%s - Error while create to Forwarder Table entry\n",
	                              __FUNCTION__);
		rc = MEA_ERROR;
	}

	/* Unlock the driver */
    MEA_API_Unlock();
	
	return rc;
}


MEA_Status MEA_adaptor_forwarder_entry_update(
	MEA_Uint8*	mac,
	MEA_Uint32	portmap) {
	
	MEA_DSE_Entry_dbt	entry;
	MEA_Port_t			i;
	MEA_Port_t			port;
	MEA_Status rc = MEA_OK;
	
	MEA_API_Lock();
	MEA_OS_memset(&entry, 0, sizeof(entry));
	
	entry.key.vlan_vpn.type  = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	MEA_OS_memcpy(entry.key.mac_plus_vpn.MAC.b, mac, sizeof(entry.key.mac_plus_vpn.MAC.b));
	entry.key.mac_plus_vpn.VPN = 0;

	entry.data.aging = 3;
	entry.data.static_flag  = 1;
	entry.data.sa_discard = 0;
	entry.data.valid = 1;
	
	for ( i = 0; i < 16; i ++ )
	{
		if( portmap & (1<<i) )
		{
			port = ConverPortToInterface(MEA_ADAPTOR_INTERNAL_PORT(i));
			
			if (MEA_API_Get_IsPortValid(port,MEA_TRUE) != MEA_TRUE) {
			    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                              "%s - Port id %d is not define\n",
	                              __FUNCTION__, port);
                MEA_API_Unlock();
				return MEA_ERROR;
			}
			
			/* check if the out_port is valid */
			((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port/32] |=  
			(1 << (port%32));
		}
	}

	/* Always add downlink Port to the output port map */
	((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER/32] |=  
	(1 << (MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER%32));
	
	
	if (MEA_API_Set_DSE_Entry(MEA_UNIT_0,&entry, NULL, NULL) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                              "%s - Error while update to Forwarder Table\n",
	                              __FUNCTION__, port);

		rc = MEA_ERROR;
	}
	
	MEA_API_Unlock();
	
	return rc;
}


MEA_Status MEA_adaptor_forwarder_entry_del(
	MEA_Uint8*	mac){

	MEA_DSE_Entry_dbt	entry;
	MEA_Status rc = MEA_OK;
	
	MEA_OS_memset(&entry, 0, sizeof(entry));

	/* Lock the driver */
    MEA_API_Lock();

	entry.key.vlan_vpn.type  = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	MEA_OS_memcpy(entry.key.mac_plus_vpn.MAC.b, mac, sizeof(entry.key.mac_plus_vpn.MAC.b));
	entry.key.mac_plus_vpn.VPN = 0;
	
	if (MEA_API_Delete_DSE_Entry(MEA_UNIT_0,&entry.key) != MEA_OK) {
			    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                              "%s - Error while create to Forwarder Table entry\n",
	                              __FUNCTION__);
	   rc = MEA_ERROR;
	}

	/* Unlock the driver */
    MEA_API_Unlock();

	return rc;
}
MEA_Status MEA_adaptor_forwarder_entry_get(
	MEA_Uint8*	mac, MEA_Uint32* portmap){
	
	MEA_Forwarder_Result_dbt       result;
	MEA_Port_t                     port;
	ENET_xPermission_dbt           groupEntry;
	MEA_Forwarder_Signature_dbt    signature;
	
    MEA_API_Lock();

	MEA_OS_memset((char*)&signature,0,sizeof(signature));
	
	signature.val.signature.mac_plus_vpn.mac_32_47 = 
		((((MEA_Uint32)((unsigned char)(mac[1]))) <<  0) |  
		(((MEA_Uint32)((unsigned char)(mac[0]))) <<  8) );
		
	signature.val.signature.mac_plus_vpn.mac_0_31 = 
		((((MEA_Uint32)((unsigned char)(mac[5]))) <<  0) |  
		(((MEA_Uint32)((unsigned char)(mac[4]))) <<  8) |
		(((MEA_Uint32)((unsigned char)(mac[3]))) << 16) |  
		(((MEA_Uint32)((unsigned char)(mac[2]))) << 24) );
	
	signature.val.signature.mac_plus_vpn.vpn = 0;
	signature.val.signature.mac_plus_vpn.dont_care2 = 0;
	signature.val.signature.mac_plus_vpn.key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;

	
	if (MEA_fwd_tbl_readByKey( &signature,
								&result) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
              "%s - Error while seraching Forwarder Table\n",
              __FUNCTION__);

	    MEA_API_Unlock();
		return MEA_ERROR;
	}
	
	if (!result.result.val.match) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
              "%s - Not found\n",
              __FUNCTION__);

	    MEA_API_Unlock();
		return MEA_ERROR;
	}
	
	enet_Get_xPermission(MEA_UNIT_0,
						result.result.val.xPermissionId,&groupEntry);

	*portmap = 0;
	for (port = 0;port <=MEA_MAX_PORT_NUMBER; port++)
	{
		if (((ENET_Uint32*)(&(groupEntry.out_ports_0_31)))[port/32] & (1 << (port%32))) {
			if( port != MEA_ADAPTOR_DOWNLINK_DEFAULT_CLUSTER )
				(*portmap) |= (1<<MEA_ADAPTOR_EXTERNAL_PORT(ConverPortFromInterface(port)));
		}
	}

    MEA_API_Unlock();

	return MEA_OK;
}


/****************************** counters functions **********************************/
/* MEA_adaptor_API_Collect_Counters_All is similar to MEA_API_Collect_Counters_All except for the call to MEA_adaptor_API_Collect_Counters_PMs */
/* MEA_adaptor_API_Collect_Counters_PMs is similar to MEA_API_Collect_Counters_PMs except for the sleep*/
/**************************************************************************************/
static MEA_Status MEA_adaptor_API_Collect_Counters_All(MEA_Unit_t unit) {


        /* Collect all provisioned PM counters */
		if( MEA_adaptor_API_Collect_Counters_PMs(MEA_UNIT_0) != MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_PMs failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }

        /* Collect MTU and MQS droped packets for all queues  */
	    if (MEA_API_Collect_Counters_Queues(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_Queues failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }

        /* Collect BM global counters  */
		if (MEA_API_Collect_Counters_BMs(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_BMs failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }
  

        /* Collect IngressPorts counters (parser counters) */		 
		if (MEA_API_Collect_Counters_IngressPorts(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_IngressPorts failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }

        /* Collect all RMONs counters */
		if (MEA_API_Collect_Counters_RMONs(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_RMONs failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }



        return MEA_OK;

}


static MEA_Status MEA_adaptor_API_Collect_Counters_PMs (MEA_Unit_t                unit) {
   
	MEA_Uint16 pmId;
	MEA_Uint16 pause_counter=0;
	for (pmId=0;pmId<MEA_MAX_NUM_OF_PM_ID;pmId++)
	{
		if (pm_dbt[pmId].isPmValid){
			if (MEA_adaptor_drv_Collect_Counters_PM(MEA_UNIT_0,pmId,&pm_dbt[pmId].pmCounter)!=MEA_OK){
 				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - MEA_adaptor_drv_Collect_Counters_PM (%d)\n",
								  __FUNCTION__,pmId);
				return MEA_ERROR;
			}
		}
		if( pause_counter++ == 24 )
		{
			MEA_API_Unlock();
			pause_counter = 0;
			MEA_OS_sleep(0, 100*1000*1000);	/* 100ms */
			MEA_API_Lock();
		}

	}
    /* Return to Callers */
    return MEA_OK;
}
#endif /* MEA_OS_Z1 */










