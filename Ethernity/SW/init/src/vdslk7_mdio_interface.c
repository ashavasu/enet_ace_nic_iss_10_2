#if defined(MEA_OS_LINUX) && defined(MEA_ETHERNITY) && defined(MEA_PLAT_S1_VDSL_K7)
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
//#include <net/if.h>
#include <net/if.h>
#include <string.h>
#include <errno.h>
#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_drv_common.h"
#include "k7_i2c_dev.h"
#include "k7_sfp_interface.h"

//Necessary data, from mii.h

#include <linux/sockios.h>

#include <linux/types.h>

/* This data structure is used for all the MII ioctl's */
struct mii_data {
    __u16	phy_id;
    __u16	reg_num;
    __u16	val_in;
    __u16	val_out;
};

int gskfd=0;


#define MII_BMSR		0x01
#define MII_BMSR_LINK_VALID	0x0004
#define MII_BMCR		0x00

//static struct ifreq ifr;

#define GIGAETH_SET_READ  0x8999
#define SIOCDEVPRIVATE	0x89F0	/* to 89FF */
#define SIOCDEVWRITEPAGESPECIFIC 	0x89F1	/* to 89FF */
#define SIOCDEVREADPAGESPECIFIC 	0x89F2	/* to 89FF */


struct mii_ioctl_data {
	__u16		phy_id;
	__u16		reg_num;
	__u16		val_in;
	__u16		val_out;
};

static inline struct mii_ioctl_data *if_mii(struct ifreq *rq)
{
         return (struct mii_ioctl_data *) &rq->ifr_ifru;
}

MEA_Status init_socket_connection_vdslk7(void)
{
    gskfd =socket(AF_INET,SOCK_DGRAM,0);
    if(gskfd==-1)
    {
        fprintf(stderr,"Unable to open socket, cause: %s.\n",strerror(errno));
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Unable to write ioctl to %s\n",strerror(errno));
        return MEA_ERROR;
    }
    return MEA_OK;
}


MEA_Status mdio_set_parameters(int mdio_device, int mii_id, int reg, unsigned short val,int page)
{
    //int skfd;
    int    ret;
	struct ifreq ifr;

//    struct mii_ioctl_data appoggio;
    struct mii_ioctl_data *ptr;

	memset(&ifr,0x0,sizeof(ifr));

    strcpy(ifr.ifr_name,"eth0");
    ptr = if_mii(&ifr);


    // config the page
	memset(ptr,0x0,sizeof(struct mii_ioctl_data));
	ptr->reg_num = reg;
	ptr->phy_id = mii_id;
	ptr->val_in = val;
	ptr->val_out = page;



    ret=ioctl(gskfd,SIOCDEVWRITEPAGESPECIFIC,&ifr);
    if(ret==0)
	{
    	ptr = (struct mii_ioctl_data *)&(ifr.ifr_ifru);
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"succeeded to write reg:%d val:%x\n",reg,val);

	} /* endif */
    else
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"ioctl SIOCDEVPRIVATE FAIL!!!\n");
    	return MEA_ERROR;
    }
	return MEA_OK;

}

MEA_Status mdio_get_parameters(int mdio_device, int mii_id, int reg, unsigned short *val,int page)
{
    //int skfd;
    int ret;
	struct ifreq ifr;

//    struct mii_ioctl_data appoggio;
    struct mii_ioctl_data *ptr;

	memset(&ifr,0x0,sizeof(ifr));

    strcpy(ifr.ifr_name,"eth0");
    ptr = if_mii(&ifr);


    // config the page
	memset(ptr,0x0,sizeof(struct mii_ioctl_data));
	ptr->reg_num = reg;
	ptr->phy_id = mii_id;
	ptr->val_in = 0;
	ptr->val_out = page;


    ret=ioctl(gskfd,SIOCDEVREADPAGESPECIFIC,&ifr);
    if(ret==0)
	{
    	ptr = (struct mii_ioctl_data *)&(ifr.ifr_ifru);
    	(*val) = ptr->val_out;
    	// MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"READING PAGE ... Access to  PHY 0x%x. Read the Register 0x%x -> 0x%x\n",ptr->phy_id,ptr->reg_num,ptr->val_out);

	} /* endif */
    else
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"ioctl SIOCDEVREADPAGESPECIFIC FAIL!!!\n");

	}
	return MEA_OK;

}
#endif

