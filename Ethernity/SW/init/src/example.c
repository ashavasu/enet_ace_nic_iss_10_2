/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*  Example for switching application configuration                          */
/*  ===============================================                          */
/*                                                                           */
/*  The main function is MEA_Example_main                                    */
/*                                                                           */
/*  Description:                                                             */
/*     - Deletem all entities                                                */
/*     - Configure user    ports                                             */
/*     - Configure network ports                                             */
/*     - Configure cpu     port                                              */
/*     - Configure user    lags                                              */
/*     - Configure network lags                                              */
/*     - Configure management                                                */
/*       - Create filter to IGMP , ARP , DHCP Client , DHCP Server           */
/*       - Create service to allowed CPU inject packets                      */
/*     - Create user vlans                                                   */
/*       - Create reverse action for downstream                              */
/*       - Create limiter                                                    */
/*       - Create upstream service                                           */
/*    - Create network vlans                                                 */
/*       - Create downstream service                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/*        Includes                                                           */
/*---------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_limiter_drv.h"



/*---------------------------------------------------------------------------*/
/*        Defines                                                            */
/*---------------------------------------------------------------------------*/

/* Define to use LxCP as filter instead the Filter object */
#define MEA_USE_LXCP_FOR_FILTER


/* Port numbers */
#if defined(MEA_OS_DEVICE_MEMORY_SIMULATION) 
#define MEA_EXAMPLE_NUM_OF_NETWORK_PORTS (1)
#define MEA_EXAMPLE_NUM_OF_USER_PORTS     (3)
#define MEA_EXAMPLE_NETWORK_PORTS_INIT_VALUE  = {125}
#define MEA_EXAMPLE_USER_PORTS_INIT_VALUE = {72,126,48}
#else /* MEA_OS_DEVICE_MEMORY_SIMULATION */
#define MEA_EXAMPLE_NUM_OF_NETWORK_PORTS (1)
#define MEA_EXAMPLE_NUM_OF_USER_PORTS     (4)
#define MEA_EXAMPLE_NETWORK_PORTS_INIT_VALUE  = {118}
#define MEA_EXAMPLE_USER_PORTS_INIT_VALUE = {72,125,126,48}
#endif /* MEA_OS_DEVICE_MEMORY_SIMULATION */

/* Define the User vlans */
#define MEA_EXAMPLE_NUM_OF_USER_VLANS    1
#define MEA_EXAMPLE_USER_VLANS_INIT_VALUE = \
{ { { 0            ,   /* Key.user_portNum                   */ \
      336          } , /* key.user_vlan                      */ \
    { MEA_TRUE     ,   /* data.network_2_vlans_mode          */ \
	  4            ,   /* data.network_vlan                  */ \
      50           ,   /* data.network_user_vlan             */ \
 	  7            ,   /* data.limit_num_of_macs             */ \
	  {10000     ,     /* data.downstream_sla - CBS          */ \
	   0         ,     /* data.downstream_sla - EBS          */ \
       100000000 ,     /* data.downstream_sla - CIR          */ \
	   0         ,     /* data.downstream_sla - EIR          */ \
	   MEA_FALSE ,     /* data.downstream_sla - cupling flag */ \
	   0         ,     /* data.downstream_sla - comp         */ \
       MEA_FALSE ,     /* data.downstream_sla - color_aware  */ \
	   0         } }   /* data.downstream_sla - pad1         */ \
   } \
}


/* Define the network vlans */
#define MEA_EXAMPLE_NUM_OF_NETWORK_VLANS 1 
#define MEA_EXAMPLE_NETWORK_VLANS_INIT_VALUE = \
{ { { 0            ,   /* Key.network_portNum               */ \
      4            } , /* key.network_vlan                  */ \
    { MEA_TRUE     ,   /* data.network_2_vlans_mode         */ \
	  336          ,   /* data.default_user_vlan            */ \
      1            ,   /* data.num_of_default_user_ports    */ \
	  { 0 }	       }   /* data.default_user_ports           */ \
   } \
}

/* Define the management vlan id */
#define MEA_EXAMPLE_MANAGEMENT_VID_ENABLE (MEA_TRUE)
#define MEA_EXAMPLE_MANAGEMENT_VID_VALUE  (4095)


/*---------------------------------------------------------------------------*/
/*        Typedefs                                                           */
/*---------------------------------------------------------------------------*/

typedef struct {
   struct {
	   MEA_Uint16            user_portNum;
       MEA_Uint16            user_vlan;
   } key;
   struct {
       MEA_Bool              network_2_vlans_mode;
       MEA_Uint16            network_vlan;
       MEA_Uint16            network_user_vlan;
       MEA_Uint32            limit_num_of_macs;
	   MEA_Policer_Entry_dbt downstream_sla;
   } data;
} MEA_Example_User_Vlan_dbt;

typedef struct {
	struct {
		MEA_Uint16 network_portNum;
        MEA_Uint16 network_vlan;
	} key;
   	struct {
		MEA_Bool   network_2_vlans_mode;
        MEA_Uint16 default_user_vlan;
        MEA_Uint16 num_of_default_users_ports;
        MEA_Uint8  default_user_ports[MEA_EXAMPLE_NUM_OF_USER_PORTS];
	} data;
} MEA_Example_Network_Vlan_dbt;

/*---------------------------------------------------------------------------*/
/*        Globals                                                            */
/*---------------------------------------------------------------------------*/
MEA_Port_t MEA_Example_network_ports [MEA_EXAMPLE_NUM_OF_NETWORK_PORTS]
                           MEA_EXAMPLE_NETWORK_PORTS_INIT_VALUE;

MEA_Port_t MEA_Example_user_ports[MEA_EXAMPLE_NUM_OF_USER_PORTS] 
                           MEA_EXAMPLE_USER_PORTS_INIT_VALUE;


MEA_Example_User_Vlan_dbt MEA_Example_user_vlans[MEA_EXAMPLE_NUM_OF_USER_VLANS]
                                      MEA_EXAMPLE_USER_VLANS_INIT_VALUE;

MEA_Example_Network_Vlan_dbt MEA_Example_network_vlans[MEA_EXAMPLE_NUM_OF_NETWORK_VLANS] 
                                      MEA_EXAMPLE_NETWORK_VLANS_INIT_VALUE ;

MEA_Uint16 MEA_Example_management_vid_enable = MEA_EXAMPLE_MANAGEMENT_VID_ENABLE;
MEA_Uint16 MEA_Example_management_vid_value  = MEA_EXAMPLE_MANAGEMENT_VID_VALUE;


#ifdef    MEA_USE_LXCP_FOR_FILTER
MEA_LxCp_t MEA_Example_LxCpId = MEA_PLAT_GENERATE_NEW_ID;
#else  /* MEA_USE_LXCP_FOR_FILTER */
MEA_Uint32 MEA_Example_filter_tmId = 0;
#endif /* MEA_USE_LXCP_FOR_FILTER */
    

ENET_QueueId_t MEA_Example_user_ports_QueueIdToNetwork[MEA_EXAMPLE_NUM_OF_USER_PORTS];


   

   


/*---------------------------------------------------------------------------*/
/*        Implemntation                                                      */
/*---------------------------------------------------------------------------*/




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_delete_all_services>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_delete_all_services() {

   MEA_Service_t db_serviceId;
   MEA_Service_t temp_db_serviceId;
   MEA_ULong_t    db_cookie;

   /* Get first service */
   if (MEA_API_GetFirst_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK)  {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_GetFirst_Service failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* loop until no more services */
   while (db_cookie != 0) { 


       /* Save the current db_serviceId */
       temp_db_serviceId = db_serviceId;

       /* Make next before delete the object */ 
       if (MEA_API_GetNext_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetNext_Service failed (db_serviceId=%d)\n",
                             __FUNCTION__,db_serviceId);
           return MEA_ERROR;
       }

       /* Delete the save db_serviceId */
	   if (MEA_API_Delete_Service(MEA_UNIT_0,temp_db_serviceId)!=MEA_OK) { 
	       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete service %d\n",
                             __FUNCTION__,temp_db_serviceId);
           return MEA_ERROR;
       }
        
   }
 

   /* Return to caller */
   return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_delete_all_limiters>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_delete_all_limiters() {

   MEA_Limiter_t      db_limiterId;
   MEA_Limiter_t      temp_db_limiterId;
   MEA_Bool           found;

   /* Get the first limiter */
   if (MEA_API_GetFirst_Limiter(MEA_UNIT_0,&db_limiterId,&found) != MEA_OK)  {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_GetFirst_Limiter failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
   }

   /* loop until no more limiter */
   while (found) { 


       /* Save the current db_limiterId */
       temp_db_limiterId = db_limiterId;

       /* Make next before delete the object */
	   if (MEA_API_GetNext_Limiter(MEA_UNIT_0,&db_limiterId,&found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetNext_Limiter failed (db_limiterId=%d)\n",
                             __FUNCTION__,db_limiterId);
		   return MEA_ERROR;
       }

	   /* Ignore the specail limiter */
	   if (temp_db_limiterId == MEA_LIMITER_DONT_CARE) {
	    	continue;
	   }
	   
       /* Delete the save db_limiterId */
	   if (MEA_API_Delete_Limiter(MEA_UNIT_0,temp_db_limiterId)!=MEA_OK) { 
	       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete limiter %d\n",
                             __FUNCTION__,temp_db_limiterId);
           return MEA_ERROR;
       }
   }

   /* Return to caller */
   return MEA_OK;

}

#ifdef   MEA_USE_LXCP_FOR_FILTER
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_delete_all_filters>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_delete_all_filters() {

   MEA_LxCp_t id;
   MEA_LxCp_t temp_id;
   MEA_Bool   found;


   /* Get the first filter */
   if (MEA_API_GetFirst_LxCP(MEA_UNIT_0,&id, NULL,&found) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
  	                      "%s - failed to GetFirst action \n",
						  __FUNCTION__);
		return MEA_ERROR;
   }
 
   /* loop until no more lxcp profiles */
   while (found) {

	   /* Save the id that need to be delete , before the getNext */
	   temp_id = id;
	   
			
       /* Get the next LxCP Profile */
	   if (MEA_API_GetNext_LxCP(MEA_UNIT_0,
			                    &id,
                                NULL,
								&found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - failed to GetNext LxCP %d\n",
							 __FUNCTION__,id);
			return MEA_ERROR;
       }

	   /* Delete the save LxCP Profile */
       if (MEA_API_Delete_LxCP(MEA_UNIT_0,temp_id)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - failed to delete LxCP %d\n",
							 __FUNCTION__,temp_id);
		   return MEA_ERROR;
       }

   }

	
   /* Return to caller */
   return MEA_OK;

}

#else  /* MEA_USE_LXCP_FOR_FILTER */

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_delete_all_filters>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_delete_all_filters() {

   MEA_Filter_t      filter_id;
   MEA_Filter_t      temp_filter_id;
   MEA_Bool          found;


   /* Get the first filter */
   if (MEA_API_GetFirst_Filter(MEA_UNIT_0,&filter_id, &found) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
  	                      "%s - failed to GetFirst filter \n",
						  __FUNCTION__);
		return MEA_ERROR;
   }
 
   /* loop until no more filters */
   while (found) {

	   /* Save the filter id that need to be delete , before the getNext */
	   temp_filter_id = filter_id;
	   
			
       /* Get the next filter */
	   if (MEA_API_GetNext_Filter(MEA_UNIT_0,
			                       &filter_id,
								   &found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - failed to GetNext filter %d\n",
							 __FUNCTION__,filter_id);
			return MEA_ERROR;
       }

	   /* Delete the save filter */
       if (MEA_API_Delete_Filter(MEA_UNIT_0,temp_filter_id)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - failed to delete filter %d\n",
							 __FUNCTION__,temp_filter_id);
		   return MEA_ERROR;
       }

   }

	
   /* Return to caller */
   return MEA_OK;

}
#endif /* MEA_USE_LXCP_FOR_FILTER */



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_delete_all_actions>                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_delete_all_actions() {

    MEA_Action_t  ActionId;
    MEA_Bool      exist;
    
    /* Scan all actions */
    for (ActionId=1; ActionId< MEA_FWD_ACTION_TBL_MAX_SIZE; ActionId++){  
    
		/* Check if the actionId is use */
        if (MEA_API_IsExist_Action  (MEA_UNIT_0, ActionId , &exist)!=MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                         "%s - MEA_API_IsExist_Action_ById failed (id=%d) \n",
	 	   			         __FUNCTION__,ActionId);
           return MEA_ERROR;
        }
		/* If the action exist then delete it */
        if(exist == MEA_TRUE) { 
            if (MEA_API_Delete_Action (MEA_UNIT_0,ActionId)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	         	                  "%s - MEA_API_Delete_Action failed (id=%d) \n",
	 						      __FUNCTION__,ActionId);
                return MEA_ERROR;
            }
        
        }
    }
    
    
   /* Return to caller */
   return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_delete_all_forwrder_entries>                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_delete_all_forwarder_entries() {

   if (MEA_API_DeleteAll_SE(MEA_UNIT_0) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                     "%s - MEA_API_DeleteAll_DSE failed \n",
						 __FUNCTION__);
      return MEA_ERROR;
   }

   /* Return to caller */
   return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_delete_all_QueueIdToNetwork>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_delete_all_QueueIdToNetwork() {

   MEA_Uint32    i;
   static MEA_Bool first= MEA_FALSE;
   
   if(!first) {
	   first = MEA_TRUE;
	   return MEA_OK;
   }

   for (i=0;
	   i<MEA_NUM_OF_ELEMENTS(MEA_Example_user_ports_QueueIdToNetwork);
	   i++) {
	   if (ENET_Delete_Queue(ENET_UNIT_0,
                             MEA_Example_user_ports_QueueIdToNetwork[i]) 
							 != ENET_OK) {
	       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - ENET_Delete_Queue failed\n",
                             __FUNCTION__);
           return MEA_ERROR; 
	   }
	   MEA_Example_user_ports_QueueIdToNetwork[i] = ENET_PLAT_GENERATE_NEW_ID;
   }


   /* Return to caller */
   return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_delete_all_entities>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_delete_all_entities() {


    /* Delete all services */
	if (MEA_Example_delete_all_services() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_delete_all_services failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* Delete all forwarder entries */
   if (MEA_Example_delete_all_forwarder_entries() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_delete_all_forwarder_entries failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }
   /* Delete all limiters */
   if (MEA_Example_delete_all_limiters() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_delete_all_limiters failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

    /* Delete all filters */
	if (MEA_Example_delete_all_filters() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_delete_all_filters failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* Delete all actions */
   if (MEA_Example_delete_all_actions() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_delete_all_actions failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }


   


   /* Delete all QueueIdToNetwork */
   if (MEA_Example_delete_all_QueueIdToNetwork() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_delete_all_QueueIdToNetwork failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }
   
   /* Reset values */
   
#ifdef    MEA_USE_LXCP_FOR_FILTER
   MEA_Example_LxCpId = MEA_PLAT_GENERATE_NEW_ID;
#else  /* MEA_USE_LXCP_FOR_FILTER */
   MEA_Example_filter_tmId = 0;
#endif /* MEA_USE_LXCP_FOR_FILTER */

    /* Return to caller */
 	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_user_ports>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_user_ports() {


	MEA_Uint32                portNum;
	MEA_Port_t                portId;
	MEA_IngressPort_Entry_dbt IngressPort_Entry;
	MEA_EgressPort_Entry_dbt   EgressPort_Entry;

	/* Scan all user ports */
	for (portNum=0;portNum<MEA_NUM_OF_ELEMENTS(MEA_Example_user_ports);portNum++) {

		/* Set the portId */
		portId = MEA_Example_user_ports[portNum];

		/* Get the ingress port entry */
		if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
			                              portId,
										  &IngressPort_Entry) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Get_IngressPort_Entry failed (id=%d) \n",
			                 __FUNCTION__,portId);
		   return MEA_ERROR;
		}

		/* Get the egress port entry */
		if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
			                             portId,
										 &EgressPort_Entry) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Get_EgressPort_Entry failed (id=%d) \n",
			                 __FUNCTION__,portId);
		   return MEA_ERROR;
		}

		/* Set the ingress portocol to VLAN and mask the priority for the classification */
		/* Note: port_cos_aw=TRUE mean to decode the priority from the packet and mapped it 
		         to priority queue for scheduling , but for classification we want 
				 to use D.C , therefore we set pri_wildcard to TRUE , and pri_wildcard to 7
				 and this will make "OR mask" with the priority decode from the packet , 
				 and get the value 7 (that use in the service as a key) */
		IngressPort_Entry.parser_info.port_proto_prof    = MEA_INGRESS_PORT_PROTO_VLAN;
		IngressPort_Entry.parser_info.port_cos_aw        = MEA_TRUE;   
		IngressPort_Entry.parser_info.port_col_aw        = MEA_FALSE;
		IngressPort_Entry.parser_info.port_ip_aw         = MEA_FALSE;
		IngressPort_Entry.parser_info.net_wildcard_valid = MEA_FALSE;
		IngressPort_Entry.parser_info.net_wildcard       = 0;
		IngressPort_Entry.parser_info.pri_wildcard_valid = MEA_TRUE;
		IngressPort_Entry.parser_info.pri_wildcard       = MEA_WILDCARD_ALL_PRI;
		IngressPort_Entry.parser_info.sp_wildcard_valid  = MEA_FALSE;
		IngressPort_Entry.parser_info.sp_wildcard        = 0;

		/* Set the egress portocol to VLAN */
		EgressPort_Entry.proto = MEA_EGRESS_PORT_PROTO_VLAN;

		/* Set the Ingress Port entry */
      		if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,
			                              portId,
										  &IngressPort_Entry) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Get_IngressPort_Entry failed (id=%d) \n",
			                 __FUNCTION__,portId);
		   return MEA_ERROR;
		}

		/* Set the egress port entry */
		if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
			                             portId,
										 &EgressPort_Entry) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Set_EgressPort_Entry failed (id=%d) \n",
			                 __FUNCTION__,portId);
		   return MEA_ERROR;
		}

	}

	/* Return to caller */
 	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_network_ports>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_network_ports() {

	MEA_Uint32                portNum,userPortNum;
	MEA_Port_t                portId;
	MEA_IngressPort_Entry_dbt IngressPort_Entry;
	MEA_EgressPort_Entry_dbt   EgressPort_Entry;

	/* Scan all network ports */
	for (portNum=0;portNum<MEA_NUM_OF_ELEMENTS(MEA_Example_network_ports);portNum++) {

		/* Set the portId */
		portId = MEA_Example_network_ports[portNum];

		/* Get the ingress port entry */
		if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
			                              portId,
										  &IngressPort_Entry) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Get_IngressPort_Entry failed (id=%d) \n",
			                 __FUNCTION__,portId);
		   return MEA_ERROR;
		}

		/* Get the egress port entry */
		if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
			                             portId,
										 &EgressPort_Entry) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Get_EgressPort_Entry failed (id=%d) \n",
			                 __FUNCTION__,portId);
		   return MEA_ERROR;
		}

		/* Set the ingress portocol to VLAN and mask the priority for the classification */
		/* Note: port_cos_aw=TRUE mean to decode the priority from the packet and mapped it 
		         to priority queue for scheduling , but for classification we want 
				 to use D.C , therefore we set pri_wildcard to TRUE , and pri_wildcard to 7
				 and this will make "OR mask" with the priority decode from the packet , 
				 and get the value 7 (that use in the service as a key) */
		IngressPort_Entry.parser_info.port_proto_prof = MEA_INGRESS_PORT_PROTO_VLAN;
		IngressPort_Entry.parser_info.port_cos_aw        = MEA_TRUE;   
		IngressPort_Entry.parser_info.port_col_aw        = MEA_FALSE;
		IngressPort_Entry.parser_info.port_ip_aw         = MEA_FALSE;
		IngressPort_Entry.parser_info.net_wildcard_valid = MEA_FALSE;
		IngressPort_Entry.parser_info.net_wildcard       = 0;
		IngressPort_Entry.parser_info.pri_wildcard_valid = MEA_TRUE;
		IngressPort_Entry.parser_info.pri_wildcard       = MEA_WILDCARD_ALL_PRI;
		IngressPort_Entry.parser_info.sp_wildcard_valid  = MEA_FALSE;
		IngressPort_Entry.parser_info.sp_wildcard        = 0;

		/* Set the egress portocol to VLAN */
		EgressPort_Entry.proto = MEA_EGRESS_PORT_PROTO_VLAN;

		/* Set the Ingress Port entry */
		if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
			                              portId,
										  &IngressPort_Entry) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Get_IngressPort_Entry failed (id=%d) \n",
			                 __FUNCTION__,portId);
		   return MEA_ERROR;
		}

		/* Set the egress port entry */
		if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
			                             portId,
										 &EgressPort_Entry) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Set_EgressPort_Entry failed (id=%d) \n",
			                 __FUNCTION__,portId);
		   return MEA_ERROR;
		}


		/* Create for each user ports cluster Queue to the network port 
		   This done to seperate the traffic in the upstream between the
		   user ports , and create fairness between the user ports */
    	for (userPortNum=0;userPortNum<MEA_NUM_OF_ELEMENTS(MEA_Example_user_ports);userPortNum++) {
			ENET_Queue_dbt Queue;
			MEA_Uint32 i;

			MEA_OS_memset(&Queue,0,sizeof(Queue));
            MEA_OS_strcpy(Queue.name,ENET_PLAT_GENERATE_NEW_NAME);
	        Queue.port.type              = ENET_QUEUE_PORT_TYPE_PORT;
	        Queue.port.id.port           = portId;
	        Queue.mode.type              = ENET_QUEUE_MODE_TYPE_WFQ;
            Queue.mode.value.wfq_weight = 10;
            Queue.MTU                    = MEA_QUEUE_CLUSTER_MTU_DEF_VAL;
            Queue.shaper_enable          = MEA_FALSE;
            Queue.Shaper_compensation    = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
 	        for(i=0;i<MEA_NUM_OF_ELEMENTS(Queue.pri_queues);i++) {
		       Queue.pri_queues[i].max_q_size_Byte            = (2*1024*1024);
			   Queue.pri_queues[i].max_q_size_Packets         = 512;
			   Queue.pri_queues[i].mtu				          = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
		       Queue.pri_queues[i].mode.type                  = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
		       Queue.pri_queues[i].mode.value.strict_priority = 0;
	        }
			MEA_Example_user_ports_QueueIdToNetwork[userPortNum] = ENET_PLAT_GENERATE_NEW_ID;
            if (ENET_Create_Queue(ENET_UNIT_0,
				                  &Queue,
								  &(MEA_Example_user_ports_QueueIdToNetwork[userPortNum])
								  ) != ENET_OK) {
		         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                       "%s - ENET_Create_Queue failed (id=%d) \n",
			                      __FUNCTION__,MEA_Example_user_ports[userPortNum]);
		         return MEA_ERROR;
			}
		}

	}

	/* Return to caller */
 	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_cpu_port>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_cpu_port() {


	MEA_IngressPort_Entry_dbt IngressPort_Entry;
	MEA_EgressPort_Entry_dbt   EgressPort_Entry;


	/* Get the ingress port entry */
	if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
		                              MEA_CPU_PORT,
									  &IngressPort_Entry) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
  	                      "%s - MEA_API_Get_IngressPort_Entry failed \n",
			              __FUNCTION__);
	    return MEA_ERROR;
   }

   /* Get the egress port entry */
   if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
			                        MEA_CPU_PORT,
								    &EgressPort_Entry) != MEA_OK) {
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                    "%s - MEA_API_Get_EgressPort_Entry failed \n",
	                    __FUNCTION__);
	  return MEA_ERROR;
   }

   /* Set the ingress portocol to VLAN and mask the priority for the classification */
   IngressPort_Entry.parser_info.port_proto_prof = 
	   (MEA_Example_management_vid_enable) ? MEA_INGRESS_PORT_PROTO_QTAG : MEA_INGRESS_PORT_PROTO_VLAN;
   IngressPort_Entry.parser_info.net_wildcard_valid = MEA_FALSE;
   IngressPort_Entry.parser_info.net_wildcard       = 0;
   IngressPort_Entry.parser_info.pri_wildcard_valid = MEA_TRUE;
   IngressPort_Entry.parser_info.pri_wildcard       = MEA_WILDCARD_ALL_PRI;
   IngressPort_Entry.parser_info.sp_wildcard_valid  = MEA_FALSE;
   IngressPort_Entry.parser_info.sp_wildcard        = 0;

   /* Set the egress portocol to VLAN */
   EgressPort_Entry.proto =
	   (MEA_Example_management_vid_enable) ? MEA_EGRESS_PORT_PROTO_QTAG : MEA_EGRESS_PORT_PROTO_VLAN;

   /* Set the Ingress Port entry */
   if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
		                             MEA_CPU_PORT,
								     &IngressPort_Entry) != MEA_OK) {
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                    "%s - MEA_API_Get_IngressPort_Entry failed \n",
			                 __FUNCTION__);
      return MEA_ERROR;
   }

   /* Set the egress port entry */
   if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
		                            MEA_CPU_PORT,
								    &EgressPort_Entry) != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - MEA_API_Set_EgressPort_Entry failed \n",
			             __FUNCTION__);
	   return MEA_ERROR;
   }


	/* Return to caller */
 	return MEA_OK;

}




#ifdef MEA_USE_LXCP_FOR_FILTER

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_action_to_cpu>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_action_to_cpu() 
{


	MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
	MEA_Policer_Entry_dbt                 Action_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
    MEA_EHP_Info_dbt                      Action_editing_info;


	/* Get the current Action to cpu */
	if (MEA_API_Get_Action(MEA_UNIT_0,
		                   MEA_ACTION_ID_TO_CPU,
						   &Action_data,
						   &Action_outPorts, 
						   &Action_policer, 
						   NULL) /* &Action_editing  */
						   != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - MEA_API_Get_Action failed \n",
		 				 __FUNCTION__);
		return MEA_ERROR;
	}


	/* Build the editing information */
	Action_data.ed_id = 0; /* Create new */
	MEA_OS_memset(&Action_editing     ,0,sizeof(Action_editing));
	MEA_OS_memset(&Action_editing_info,0,sizeof(Action_editing_info));
	Action_editing.num_of_entries = 1;
	Action_editing.ehp_info = &Action_editing_info;
	MEA_OS_memcpy(&Action_editing.ehp_info[0].output_info,
	             &Action_outPorts,
				 sizeof(Action_editing.ehp_info[0].output_info));
    Action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_FALSE;
    Action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_FALSE;
    Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
    Action_editing.ehp_info[0].ehp_data.eth_info.val.all = 0x81000000;  /* Yhe source port will be stamp on the vid */
    if (MEA_Example_management_vid_enable) {
       Action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = 1; /* Append */
       Action_editing.ehp_info[0].ehp_data.atm_info.val.all = (0x81000000 |
	       (MEA_Example_management_vid_value & 0x00000fff));
    } else {
       Action_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = 0; /* Trans */
       Action_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
    }


	/* Update the Action to CPU */
	if (MEA_API_Set_Action(MEA_UNIT_0,
		                   MEA_ACTION_ID_TO_CPU,
						   &Action_data,
						   &Action_outPorts, 
						   &Action_policer, 
						   &Action_editing)
						   != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - MEA_API_Set_Action failed \n",
		 				 __FUNCTION__);
		return MEA_ERROR;
	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_trap_to_cpu>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_trap_to_cpu(MEA_Uint16 etherType_i,
												   MEA_Uint8  ipProtocol_i,
												   MEA_Uint16 L4DstPort_i) 
{

	MEA_LxCP_Protocol_key_dbt  key;
	MEA_LxCP_Protocol_data_dbt data;
    MEA_LxCP_Entry_dbt lxcp_entry;

	/* In first time create LxCP Profile , this LxCP Profile will be use by the services */
	if (MEA_Example_LxCpId == MEA_PLAT_GENERATE_NEW_ID) {
		
		if (MEA_Example_configure_action_to_cpu() != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - MEA_Example_configure_action_to_cpu failed \n",
							  __FUNCTION__);
			return MEA_ERROR;
		}

        MEA_OS_memset(&lxcp_entry,0,sizeof(lxcp_entry));
		if (MEA_API_Create_LxCP(MEA_UNIT_0,
                                &lxcp_entry,
                                &MEA_Example_LxCpId) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - MEA_API_Create_LxCP failed \n",
							  __FUNCTION__);
			return MEA_ERROR;
		}

	}

	/* Build the protocol key */
	MEA_OS_memset((char*)&key  , 0 , sizeof(key ));
	switch(etherType_i) {
	case 0x0806:
		key.protocol = MEA_LXCP_PROTOCOL_ARP;
		break;
	case 0x0800:
		switch (ipProtocol_i) {
	    case 2:  /* IGMP */
			key.protocol = MEA_LXCP_PROTOCOL_IGMPoV4;
			break;
		case 17: /* UDP */
			switch (L4DstPort_i) {
		    case 67:
				key.protocol = MEA_LXCP_PROTOCOL_DHCP_SERVER;
				break;
			case 68:
				key.protocol = MEA_LXCP_PROTOCOL_DHCP_CLIENT;
				break;
			default:
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                      "%s - Unknown L3DstPort (%d) \n",
				       		      __FUNCTION__,
					    	      L4DstPort_i);
		        return MEA_ERROR;
			}
			break;
		default:
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - Unknown ipProtocol (%d) \n",
				    		  __FUNCTION__,
					    	  ipProtocol_i);
		    return MEA_ERROR;
		}
		break;
	default:
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Unknown etherType (0x%04x) \n",
						  __FUNCTION__,
						  etherType_i);
		return MEA_ERROR;
	}

	/* Build the protocol data */
	MEA_OS_memset((char*)&data , 0 , sizeof(data));
	data.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;

	/* Update the action to trap this protocol to CPU */
	if (MEA_API_Set_LxCP_Protocol(MEA_UNIT_0,
		                          MEA_Example_LxCpId,
								  &key,
								  &data) != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - MEA_API_Set_LxCP_Protocol failed (LxCpId=%d,proto=%d)\n",
						 __FUNCTION__,
						 MEA_Example_LxCpId,
						 key.protocol);
	   return MEA_ERROR;
	}





    /* Return to caller */
 	return MEA_OK;


} 

#else /* MEA_USE_LXCP_FOR_FILTER */

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_trap_to_cpu>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_trap_to_cpu(MEA_Uint16 etherType_i,
												   MEA_Uint8  ipProtocol_i,
												   MEA_Uint16 L4DstPort_i) {

   MEA_Filter_Key_dbt                   Filter_key;
   MEA_Filter_Data_dbt                  Filter_data;
   MEA_OutPorts_Entry_dbt               Filter_outPorts;
   MEA_Policer_Entry_dbt                Filter_policer;
   MEA_EHP_Info_dbt                     Filter_editing_info;
   MEA_EgressHeaderProc_Array_Entry_dbt Filter_editing;
   MEA_Filter_t                         Filter_id;

   /* Set Filter key to trap Packets to the CPU */
   MEA_OS_memset(&Filter_key,0,sizeof(Filter_key));
   Filter_key.interface_key.src_port_valid    = MEA_TRUE;
   Filter_key.interface_key.src_port          = 0;  /* The srcPort is mask by the services to zero */
   Filter_key.layer3.ethertype3_valid         = MEA_TRUE;
   Filter_key.layer3.ethertype3               = etherType_i;
   Filter_key.layer3.ip_protocol_valid        = MEA_TRUE;
   Filter_key.layer3.ip_protocol              = ipProtocol_i;
   Filter_key.layer4.dst_port_valid           = MEA_TRUE;
   Filter_key.layer4.dst_port                 = L4DstPort_i;

   /* Set Filter data to trap to CPU Port */
   MEA_OS_memset(&Filter_data,0,sizeof(Filter_data));
   Filter_data.action_type                      = MEA_FILTER_ACTION_TO_ACTION;
   Filter_data.action_params.output_ports_valid = MEA_TRUE;
   Filter_data.action_params.pm_id_valid        = MEA_TRUE;
   Filter_data.action_params.pm_id              = 0; /* Create new one */
   Filter_data.action_params.tm_id_valid        = MEA_TRUE; 
   Filter_data.action_params.tm_id              = MEA_Example_filter_tmId; 
   Filter_data.action_params.ed_id_valid        = MEA_TRUE;
   Filter_data.action_params.ed_id              = 0; /* Create new one */
   Filter_data.action_params.proto_llc_valid    = MEA_TRUE;
   Filter_data.action_params.protocol_llc_force = MEA_TRUE;
   Filter_data.action_params.Protocol           = 1; /* 0-Ttans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
   Filter_data.action_params.Llc                = MEA_FALSE;
   Filter_data.action_params.force_l2_pri_valid = MEA_FALSE;
   Filter_data.action_params.L2_PRI             = 0;
   Filter_data.action_params.force_color_valid  = MEA_FALSE;
   Filter_data.action_params.COLOR              = 0;
   Filter_data.action_params.force_cos_valid    = MEA_FALSE;
   Filter_data.action_params.COS                = 0;

   /* Build the Filter policer */
   MEA_OS_memset(&Filter_policer  , 0 , sizeof(Filter_policer ));
   Filter_policer.CIR         = MEA_CPU_SERVICE_POLICER_CIR_DEF_VAL;
   Filter_policer.CBS         = MEA_CPU_SERVICE_POLICER_CBS_DEF_VAL;
   Filter_policer.EIR         = MEA_CPU_SERVICE_POLICER_EIR_DEF_VAL;
   Filter_policer.EBS         = MEA_CPU_SERVICE_POLICER_EBS_DEF_VAL;
   Filter_policer.cup         = MEA_CPU_SERVICE_POLICER_CUP_DEF_VAL;
   Filter_policer.comp        = MEA_CPU_SERVICE_POLICER_COMP_DEF_VAL;
   Filter_policer.color_aware = MEA_FALSE;

   /* Set the output ports to trap to the CPU port */
   MEA_OS_memset(&Filter_outPorts,0,sizeof(Filter_outPorts));
   ((MEA_Uint32*)(&Filter_outPorts.out_ports_0_31))[MEA_CPU_PORT/32] |= (0x00000001 << (MEA_CPU_PORT%32));

   /* Build the Filter editing */
   MEA_OS_memset(&Filter_editing  , 0 , sizeof(Filter_editing ));
   MEA_OS_memset(&Filter_editing_info,0,sizeof(Filter_editing_info));
   Filter_editing.num_of_entries = 1;
   Filter_editing.ehp_info = &Filter_editing_info;
   MEA_OS_memcpy(&Filter_editing_info.output_info,
	             &Filter_outPorts,
				 sizeof(Filter_editing_info.output_info));
   Filter_editing_info.ehp_data.eth_info.stamp_priority            = MEA_FALSE;
   Filter_editing_info.ehp_data.eth_info.stamp_color               = MEA_FALSE;
   Filter_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
   Filter_editing_info.ehp_data.eth_info.val.all = 0x81000000;  /* Yhe source port will be stamp on the vid */
   if (MEA_Example_management_vid_enable) {
       Filter_editing_info.ehp_data.atm_info.double_tag_cmd = 1; /* Append */
       Filter_editing_info.ehp_data.atm_info.val.all = (0x81000000 |
	       (MEA_Example_management_vid_value & 0x00000fff));
   } else {
       Filter_editing_info.ehp_data.atm_info.double_tag_cmd = 0; /* Trans */
       Filter_editing_info.ehp_data.atm_info.val.all = 0;
   }


   /* Create filter to trap to the CPU port */
  if (MEA_API_Create_Filter(MEA_UNIT_0,
		                      &Filter_key, /* Key from */
							  NULL,        /* key to   */
							  &Filter_data,
							  &Filter_outPorts,
							  &Filter_policer,
							  &Filter_editing,
							  &Filter_id) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - MEA_API_Create_Filter (etherType=0x%04x,IpProto=%d,L4DstPort=%d) failed \n",
						 __FUNCTION__,
						 Filter_key.layer2.outer_vlanTag.ethertype,
						 Filter_key.layer3.ip_protocol,
						 Filter_key.layer4.dst_port);
	   return MEA_ERROR;
	}

    /* Save the last filter tm id to the next filters , to use same tm_id */
    MEA_Example_filter_tmId = Filter_data.action_params.tm_id ;

    /* Return to caller */
 	return MEA_OK;

}


#endif /* MEA_USE_LXCP_FOR_FILTER */


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_inject_from_cpu>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_inject_from_cpu(MEA_Port_t portId_i) {

    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
	MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Service_t                         Service_id;
	MEA_Uint32                            outport;




    /* Build the inject from cpu service key */
	MEA_OS_memset(&Service_key,0,sizeof(Service_key));
	Service_key.src_port  = MEA_CPU_PORT;
	Service_key.pri         = MEA_WILDCARD_ALL_PRI;
	Service_key.net_tag     = portId_i;
    Service_key.net_tag    |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;


	/* Build the inject from cpu service data attributes */
	MEA_OS_memset(&Service_data     , 0 , sizeof(Service_data));
   	Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

    /* Build the inject from cpu service outPorts */
    MEA_OS_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));
	outport = portId_i;
    ((MEA_Uint32*)(&(Service_outPorts.out_ports_0_31)))[outport/32] |= 
					                   (0x00000001 << (outport%32));


	/* Build the inject from cpu Service policing */
	MEA_OS_memset(&Service_policer  , 0 , sizeof(Service_policer));

    /* Build the inject from cpu Service editing */ 
    MEA_OS_memset(&Service_editing  , 0 , sizeof(Service_editing ));
	MEA_OS_memset(&Service_editing_info,0,sizeof(Service_editing_info));
	Service_editing.num_of_entries = 1;
	Service_editing.ehp_info = &Service_editing_info;
	MEA_OS_memcpy(&Service_editing_info.output_info,
	              &Service_outPorts,
				  sizeof(Service_editing_info.output_info));
    Service_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
    if (MEA_Example_management_vid_enable) {
       Service_editing_info.ehp_data.atm_info.double_tag_cmd = 2; /* Extract */
	} else {
       Service_editing_info.ehp_data.atm_info.double_tag_cmd = 0; /* Trans   */
	}

	/* Create the inject from cpu Service */
	if (MEA_API_Create_Service(MEA_UNIT_0,
		                       &Service_key,
							   &Service_data,
							   &Service_outPorts,
							   &Service_policer,
							   &Service_editing,
							   &Service_id) != MEA_OK) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - MEA_API_Create_Service failed \n",
						   __FUNCTION__);
		 return MEA_ERROR;
	}
	
    /* Return to caller */
 	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_management>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_management() {

	MEA_Port_t portId;
	MEA_Uint32 portNum;


	/* Trap ARP packets to CPU */
	if (MEA_Example_configure_trap_to_cpu(0x0806, /* etherType  = ARP        */
		                                        0xFF  , /* ipProtocol = Don't Care */
												0xFFFF) /* L4DstPort  = Don't Care */ 
												!= MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_Example_configure_trap_to_cpu (ARP) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	} 

	/* Trap IGMP packets to CPU */
	if (MEA_Example_configure_trap_to_cpu(0x0800, /* etherType  = IPv4       */
		                                        0x02  , /* ipProtocol = IGMP       */
												0xFFFF) /* L4DstPort  = Don't Care */ 
												!= MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_Example_configure_trap_to_cpu (IGMP) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	} 

	/* Trap DHCP Server packets to CPU */
	if (MEA_Example_configure_trap_to_cpu(0x0800, /* etherType  = IPv4            */
		                                        0x11  , /* ipProtocol = UDP (17)         */
												0x0043) /* L4DstPort  = DHCP Server (67) */ 
												!= MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_Example_configure_trap_to_cpu (DHCP Server) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	} 


	/* Trap DHCP Client packets to CPU */
	if (MEA_Example_configure_trap_to_cpu(0x0800, /* etherType  = IPv4             */
		                                        0x11  , /* ipProtocol = UDP (17)         */
												0x0044) /* L4DstPort  = DHCP Client (68) */ 
												!= MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_Example_configure_trap_to_cpu (DHCP Client) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	} 



	/* Scan all User ports */
	for (portNum=0;portNum<MEA_NUM_OF_ELEMENTS(MEA_Example_user_ports);portNum++) {

       /* Set the portId */
	   portId = MEA_Example_user_ports[portNum];

  	   /* Inject packets from CPU */
	   if (MEA_Example_configure_inject_from_cpu(portId) != MEA_OK) {
	  	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_Example_configure_inject_from_cpu failed (portId=%d)\n",
						  __FUNCTION__,portId);
		   return MEA_ERROR;
	   }
	}


	/* Scan all Networks ports */
	for (portNum=0;portNum<MEA_NUM_OF_ELEMENTS(MEA_Example_network_ports);portNum++) {

       /* Set the portId */
	   portId = MEA_Example_network_ports[portNum];

  	   /* Inject packets from CPU */
	   if (MEA_Example_configure_inject_from_cpu(portId) != MEA_OK) {
	  	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_Example_configure_inject_from_cpu failed (portId=%d)\n",
						  __FUNCTION__,portId);
		   return MEA_ERROR;
	   }
	}


    /* Return to caller */
 	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_user_vlans>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_user_vlans() {

	MEA_Uint32                            vlanNum;
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
	MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Service_t                         Service_id;
    MEA_Uint32                            j;
	MEA_Uint16                            portNum;
	MEA_Port_t                            portId;
	MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
	MEA_Policer_Entry_dbt                 Action_policer;
    MEA_EHP_Info_dbt                      Action_editing_info;
	MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
    MEA_Action_t                          Action_id;
    MEA_Limiter_Entry_dbt                 Limiter_data;
    MEA_Limiter_t                         Limiter_id;
	MEA_Uint32                            outport;



	/* Scan all User vlans */
	for (vlanNum=0;vlanNum<MEA_NUM_OF_ELEMENTS(MEA_Example_user_vlans);vlanNum++) {


		/* Build the action data */
		MEA_OS_memset(&Action_data     , 0 , sizeof(Action_data    ));
		Action_data.force_color_valid = MEA_FALSE;
		Action_data.COLOR             = 0;
		Action_data.force_cos_valid   = MEA_FALSE;
		Action_data.COS               = 0;
		Action_data.force_l2_pri_valid = MEA_FALSE;
		Action_data.L2_PRI             = 0;
		Action_data.output_ports_valid = MEA_FALSE; /* No output ports */
		Action_data.pm_id_valid        = MEA_TRUE;
		Action_data.pm_id              = 0; /* Request new id */
		Action_data.tm_id_valid        = MEA_TRUE;
		Action_data.tm_id              = 0; /* Request new id */
		Action_data.ed_id_valid        = MEA_TRUE;
		Action_data.ed_id              = 0; /* Request new id */
		Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
		Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
		Action_data.Llc                = MEA_FALSE; 

		/* Build the action outPorts 
		   Note: The output ports of the action is not relevant.
		         The upstream forwarder learning proccess will determine the 
				 outport of the downstream. */
		MEA_OS_memset(&Action_outPorts,0,sizeof(Action_outPorts));

		/* Build the action policer */
		MEA_OS_memset(&Action_policer  , 0 , sizeof(Action_policer ));
		Action_policer.CIR = MEA_Example_user_vlans[vlanNum].data.downstream_sla.CIR;
		Action_policer.CBS = MEA_Example_user_vlans[vlanNum].data.downstream_sla.CBS;
		Action_policer.EIR = MEA_Example_user_vlans[vlanNum].data.downstream_sla.EIR;
		Action_policer.EBS = MEA_Example_user_vlans[vlanNum].data.downstream_sla.EBS;
		Action_policer.color_aware = MEA_Example_user_vlans[vlanNum].data.downstream_sla.color_aware;
		Action_policer.cup = MEA_Example_user_vlans[vlanNum].data.downstream_sla.cup;
		Action_policer.comp = MEA_Example_user_vlans[vlanNum].data.downstream_sla.comp;


		/* Build the action editing */
		MEA_OS_memset(&Action_editing  , 0 , sizeof(Action_editing ));
		MEA_OS_memset(&Action_editing_info,0,sizeof(Action_editing_info));
		Action_editing.num_of_entries = 1;
		Action_editing.ehp_info = &Action_editing_info;
		MEA_OS_memcpy(&Action_editing_info.output_info,
			          &Action_outPorts,
			          sizeof(Action_editing_info.output_info));
		Action_editing_info.ehp_data.eth_info.stamp_priority            = MEA_TRUE;
		Action_editing_info.ehp_data.eth_info.stamp_color               = MEA_TRUE;
		Action_editing_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		Action_editing_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		Action_editing_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		Action_editing_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		Action_editing_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		Action_editing_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		Action_editing_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		if (MEA_Example_user_vlans[vlanNum].data.network_2_vlans_mode) {
   		     Action_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		     Action_editing_info.ehp_data.eth_info.val.all = (0x81000000 | 
			      (MEA_Example_user_vlans[vlanNum].data.network_user_vlan & 0x00000fff));
  		     Action_editing_info.ehp_data.atm_info.double_tag_cmd = 1; /* Append */
			 Action_editing_info.ehp_data.atm_info.val.all = (0x81000000 |
			      (MEA_Example_user_vlans[vlanNum].data.network_vlan & 0x00000fff));
		} else {
   		     Action_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		     Action_editing_info.ehp_data.eth_info.val.all = (0x81000000 | 
			      (MEA_Example_user_vlans[vlanNum].data.network_vlan & 0x00000fff));
  		     Action_editing_info.ehp_data.atm_info.double_tag_cmd = 0; /* None */
			 Action_editing_info.ehp_data.atm_info.val.all = 0;
		}



		/* Create the reverse action */ 
		Action_id = MEA_PLAT_GENERATE_NEW_ID;
		if (MEA_API_Create_Action (MEA_UNIT_0,
								   &Action_data,
								   &Action_outPorts, 
								   &Action_policer,
								   &Action_editing,
								   &Action_id) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Create_Action failed \n",
							 __FUNCTION__);
		   return MEA_ERROR;
		}

		

		/* Build the limiter data attributes */
		MEA_OS_memset(&Limiter_data,0,sizeof(Limiter_data));
		Limiter_data.limit       = MEA_Example_user_vlans[vlanNum].data.limit_num_of_macs;
		Limiter_data.action_type = MEA_LIMIT_ACTION_DISCARD;

		/* Ask for new limiter id */
		Limiter_id = MEA_PLAT_GENERATE_NEW_ID;

		/* Create the limiter */
		if (MEA_API_Create_Limiter(MEA_UNIT_0,
			                       &Limiter_data,
								   &Limiter_id) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - MEA_API_Create_Limiter failed \n",
							 __FUNCTION__);
		   return MEA_ERROR;
		}



		/* Set the portNum */
		portNum = MEA_Example_user_vlans[vlanNum].key.user_portNum;

        /* Set the portId */
		portId = MEA_Example_user_ports[portNum];

				
		/* Build the upstream service key */
 	    MEA_OS_memset(&Service_key,0,sizeof(Service_key));
		Service_key.src_port  = portId;
		Service_key.pri         = MEA_WILDCARD_ALL_PRI;
		Service_key.net_tag     = MEA_Example_user_vlans[vlanNum].key.user_vlan;
        Service_key.net_tag    |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;

    	/* Build the upstream service data attributes */
		MEA_OS_memset(&Service_data     , 0 , sizeof(Service_data));

		/* Build the upstream service data attributes for the forwarder  */
  	    Service_data.DSE_forwarding_enable   = MEA_FALSE;
		Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		Service_data.DSE_learning_enable     = MEA_TRUE;
		Service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
		Service_data.vpn                     = MEA_Example_user_vlans[vlanNum].data.network_vlan;
		Service_data.limiter_enable          = MEA_TRUE;
		Service_data.limiter_id              = Limiter_id;
		Service_data.DSE_learning_actionId_valid = MEA_TRUE;
		Service_data.DSE_learning_actionId       = Action_id;
		Service_data.DSE_learning_srcPort_force  = MEA_FALSE;
		Service_data.DSE_learning_srcPort        = portId;


        /* Build the upstream service data attribute for policing */
  	    Service_data.tmId   = 0; /* generate new id */
   		Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

		/* Build the upstream service data attribute for pm (counters) */
		Service_data.pmId   = 0; /* generate new id */

    	/* Build the upstream service data attribute for policing */
		Service_data.editId = 0; /* generate new id */


	    /* Build the upstream service data attribute for filtering ,
		   to filter ARP , IGMP , DHCP Client , DHCP Server 
		   using filter key if IpProtocol+InnerEtherType+L4DstPort */
#ifdef MEA_USE_LXCP_FOR_FILTER
		Service_data.LxCp_enable = MEA_TRUE;
		Service_data.LxCp_Id     = MEA_Example_LxCpId;
#else  /* MEA_USE_LXCP_FOR_FILTER */
		Service_data.filter_enable = MEA_TRUE;
		Service_data.filter_key_type = MEA_FILTER_KEY_TYPE_IP_PROTO_ETHERTYPE3_DST_PORT;
		Service_data.filter_mask.mask_0_31  = 0xFFFFFFFF; /* no mask for etherType and L4 DstPort*/
		Service_data.filter_mask.mask_32_63 = 0x000F00FF; /* no mask IpProto and key type ,but mask src port*/
#endif /* MEA_USE_LXCP_FOR_FILTER */



		/* Build the other upstream service data attributes to defaults */ 
		Service_data.ADM_ENA            = MEA_FALSE;
		Service_data.CM                 = MEA_FALSE;
		Service_data.L2_PRI_FORCE       = MEA_FALSE;
		Service_data.L2_PRI             = 0;
		Service_data.COLOR_FORSE        = MEA_FALSE;
		Service_data.COLOR              = 0;
		Service_data.COS_FORCE          = MEA_FALSE;
		Service_data.COS                = 0;
		Service_data.protocol_llc_force = MEA_FALSE;
		Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
		Service_data.Llc                = MEA_FALSE;

		/* Build the upstream service outPorts */
	    MEA_OS_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));
		for (j=0;j<MEA_NUM_OF_ELEMENTS(MEA_Example_network_ports);j++) {
			outport = MEA_Example_user_ports_QueueIdToNetwork[portId];
    		((MEA_Uint32*)(&(Service_outPorts.out_ports_0_31)))[outport/32] |= 
	                                        (0x00000001 << (outport%32));
		}

		/* Build the Upstream Service policing */
		MEA_OS_memset(&Service_policer  , 0 , sizeof(Service_policer));

		/* Build the Upstream Service editing */ 
        MEA_OS_memset(&Service_editing  , 0 , sizeof(Service_editing ));
        MEA_OS_memset(&Service_editing_info,0,sizeof(Service_editing_info));
        Service_editing.num_of_entries = 1;
        Service_editing.ehp_info = &Service_editing_info;
        MEA_OS_memcpy(&Service_editing_info.output_info,
			          &Service_outPorts,
					  sizeof(Service_editing_info.output_info));
        Service_editing_info.ehp_data.eth_info.stamp_priority            = MEA_TRUE;
        Service_editing_info.ehp_data.eth_info.stamp_color               = MEA_TRUE;
        Service_editing_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
        Service_editing_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
        Service_editing_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
        Service_editing_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
        Service_editing_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
        Service_editing_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
        Service_editing_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
        Service_editing_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
        Service_editing_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
        Service_editing_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
        if (MEA_Example_user_vlans[vlanNum].data.network_2_vlans_mode) {
            Service_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            Service_editing_info.ehp_data.eth_info.val.all = (0x81000000 | 
	           (MEA_Example_user_vlans[vlanNum].data.network_user_vlan & 0x00000fff));
            Service_editing_info.ehp_data.atm_info.double_tag_cmd = 1; /* Append */
	        Service_editing_info.ehp_data.atm_info.val.all = (0x81000000 |
	           (MEA_Example_user_vlans[vlanNum].data.network_vlan & 0x00000fff));
        } else {
            Service_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
            Service_editing_info.ehp_data.eth_info.val.all = (0x81000000 | 
	          (MEA_Example_user_vlans[vlanNum].data.network_vlan & 0x00000fff));
            Service_editing_info.ehp_data.atm_info.double_tag_cmd = 0; /* None */
	        Service_editing_info.ehp_data.atm_info.val.all = 0;
        }


		/* Create the upstream service */
        if (MEA_API_Create_Service(MEA_UNIT_0,
			                       &Service_key,
								   &Service_data,
								   &Service_outPorts,
								   &Service_policer,
								   &Service_editing,
								   &Service_id) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - MEA_API_Create_Service failed \n",
			   			      __FUNCTION__);
			return MEA_ERROR;
		}
    
    }

    /* Return to caller */
 	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_network_vlans>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_network_vlans() {

	MEA_Uint32                            vlanNum;
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
	MEA_EHP_Info_dbt                      Service_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Service_t                         Service_id;
	MEA_Uint16                            netPortNum,portNum;
	MEA_Port_t                            portId;
	MEA_Uint32                            outport;


    /* Scan all ports belong to this vlan */
	for (netPortNum=0;netPortNum<MEA_NUM_OF_ELEMENTS(MEA_Example_network_ports);netPortNum++) {

         /* Set the portId */
 	     portId = MEA_Example_network_ports[netPortNum];
	

	     /* Scan all Network vlans */
	     for (vlanNum=0;vlanNum<MEA_NUM_OF_ELEMENTS(MEA_Example_network_vlans);vlanNum++) {

    			
				/* Build the downstream service key */
				MEA_OS_memset(&Service_key,0,sizeof(Service_key));
				Service_key.src_port    = portId;
				Service_key.pri         = MEA_WILDCARD_ALL_PRI;
				Service_key.net_tag     = MEA_Example_network_vlans[vlanNum].key.network_vlan;
                Service_key.net_tag    |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;


				/* Build the downstream service data attributes */
				MEA_OS_memset(&Service_data     , 0 , sizeof(Service_data));

				/* Build the downstream service data attributes for the forwarder  */
				Service_data.DSE_forwarding_enable   = MEA_TRUE;
				Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
				Service_data.DSE_learning_enable     = MEA_FALSE;
				Service_data.DSE_learning_key_type   = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
				Service_data.vpn                     = MEA_Example_network_vlans[vlanNum].key.network_vlan;
				Service_data.limiter_enable          = MEA_FALSE;
				Service_data.limiter_id              = 0;
				Service_data.DSE_learning_actionId_valid = MEA_FALSE;
				Service_data.DSE_learning_actionId       = 0;
				Service_data.DSE_learning_srcPort_force  = MEA_FALSE;
				Service_data.DSE_learning_srcPort        = 0;



                /* Build the downstream service data attribute for policing */
				Service_data.tmId   = 0; /* generate new id */
   			    Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */

				/* Build the downstream service data attribute for pm (counters) */
				Service_data.pmId   = 0; /* generate new id */
	
				/* Build the downstream service data attribute for policing */
				Service_data.editId = 0; /* generate new id */

				/* Build the downstream service data attribute for filtering
				   ARP , IGMP , DHCP Client , DHCP Server 
				   using filter key if IpProtocol+InnerEtherType+L4DstPort */
				Service_data.filter_enable          = MEA_TRUE;
// 				Service_data.filter_key_type        = MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT;
// 				Service_data.filter_mask.mask_0_31  = 0xFFFFFFFF; /* no mask for etherType and L4 DstPort*/
// 				Service_data.filter_mask.mask_32_63 = 0x000F00FF; /* no mask IpProto and key type ,but mask src port*/

				/* Build the other downstream service data attributes to defaults */ 
				Service_data.ADM_ENA            = MEA_FALSE;
				Service_data.CM                 = MEA_FALSE;
				Service_data.L2_PRI_FORCE       = MEA_FALSE;
				Service_data.L2_PRI             = 0;
				Service_data.COLOR_FORSE        = MEA_FALSE;
				Service_data.COLOR              = 0;
				Service_data.COS_FORCE          = MEA_FALSE;
				Service_data.COS                = 0;
				Service_data.protocol_llc_force = MEA_FALSE;
				Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
				Service_data.Llc                = MEA_FALSE;

	            /* Build the Downstream service outPorts */
  			    MEA_OS_memset(&Service_outPorts , 0 , sizeof(Service_outPorts));
				for (portNum=0;portNum<MEA_Example_network_vlans[vlanNum].data.num_of_default_users_ports;portNum++) {
					outport = MEA_Example_network_vlans[vlanNum].data.default_user_ports[portNum];
					outport = MEA_Example_user_ports[outport];
				    ((MEA_Uint32*)(&(Service_outPorts.out_ports_0_31)))[outport/32] |= 
					                   (0x00000001 << (outport%32));
				}

				/* Build the Downstream Service policing */
				MEA_OS_memset(&Service_policer  , 0 , sizeof(Service_policer));

				/* Build the Downstream Service editing */ 
   		        MEA_OS_memset(&Service_editing  , 0 , sizeof(Service_editing ));
		        MEA_OS_memset(&Service_editing_info,0,sizeof(Service_editing_info));
		        Service_editing.num_of_entries = 1;
	 	        Service_editing.ehp_info = &Service_editing_info;
		        MEA_OS_memcpy(&Service_editing_info.output_info,
					          &Service_outPorts,
							  sizeof(Service_editing_info.output_info));
		        Service_editing_info.ehp_data.eth_info.stamp_priority            = MEA_TRUE;
		        Service_editing_info.ehp_data.eth_info.stamp_color               = MEA_TRUE;
		        Service_editing_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		        Service_editing_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		        Service_editing_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		        Service_editing_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		        Service_editing_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		        Service_editing_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		        Service_editing_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		        Service_editing_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		        Service_editing_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		        Service_editing_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
	            Service_editing_info.ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	            Service_editing_info.ehp_data.eth_info.val.all = (0x81000000 | 
						(MEA_Example_network_vlans[vlanNum].data.default_user_vlan & 0x00000fff));
  		        if (MEA_Example_network_vlans[vlanNum].data.network_2_vlans_mode) {
   		            Service_editing_info.ehp_data.atm_info.double_tag_cmd = 2; /* Extract */
				} else {
  		            Service_editing_info.ehp_data.atm_info.double_tag_cmd = 0; /* None */
				}
		        Service_editing_info.ehp_data.atm_info.val.all = 0;


				/* Create the downstream service */
				if (MEA_API_Create_Service(MEA_UNIT_0,
					                       &Service_key,
										   &Service_data,
										   &Service_outPorts,
										   &Service_policer,
										   &Service_editing,
										   &Service_id) != MEA_OK) {
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						               "%s - MEA_API_Create_Service failed \n",
									   __FUNCTION__);
					 return MEA_ERROR;
				}
		}
	}

    /* Return to caller */
 	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_configure_queues>                                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_configure_queues() {


	ENET_QueueId_t  queueId;
	ENET_Queue_dbt queueEntry;
	MEA_Bool       found;
	MEA_Uint32     i;



	/* Get first cluster queue */
	if (ENET_GetFirst_Queue(ENET_UNIT_0,
		                    &queueId,
							&queueEntry,
							&found) != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - ENET_GetFirst_Queue failed  \n",
		                 __FUNCTION__);
	   return MEA_ERROR;
	}

	/* Scan all the cluster queues */
	while (found) {


		/* Set default value to all priority queues under this cluster queue */
	    for(i=0;i<MEA_NUM_OF_ELEMENTS(queueEntry.pri_queues);i++) {
		    queueEntry.pri_queues[i].max_q_size_Byte            = (2*1024*1024);
			queueEntry.pri_queues[i].max_q_size_Packets         = 512;
		    queueEntry.pri_queues[i].mode.type                  = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
		    queueEntry.pri_queues[i].mode.value.strict_priority = 0;
	    }

		/* Set the cluster Queue */
   	    if (ENET_Set_Queue(ENET_UNIT_0,
			               queueId,
						   &queueEntry) != ENET_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - ENET_Set_Queue failed (id=%d) \n",
			                 __FUNCTION__,queueId);
		   return MEA_ERROR;
		}

		/* Get next cluster queue */
	    if (ENET_GetNext_Queue(ENET_UNIT_0,
		                       &queueId,
						       &queueEntry,
							   &found) != MEA_OK) {
	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                      "%s - ENET_GetNext_Queue failed (id=%d) \n",
		                      __FUNCTION__,queueId);
	        return MEA_ERROR;
	    }
	}

	/* Return to caller */
 	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_main>                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_main() {

	
    /* Delete all entities */
	if (MEA_Example_delete_all_entities() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_delete_all_entities failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* Configure user ports */
	if (MEA_Example_configure_user_ports() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_configure_user_ports failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

    /* Configure network ports */
	if (MEA_Example_configure_network_ports() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_configure_network_ports failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* Configure cpu port */
	if (MEA_Example_configure_cpu_port() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_configure_cpu_port failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }


   /* Configure mangement */
   if (MEA_Example_configure_management() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_configure_management failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* Configure user vlans */
   if (MEA_Example_configure_user_vlans() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_configure_user_vlans failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* Configure network vlans */
   if (MEA_Example_configure_network_vlans() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_configure_network_vlans failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* Configure queues */
   if (MEA_Example_configure_queues() != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Example_configure_queues failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }



   /* Return to caller */
   return MEA_OK;

}

#include "cli_eng.h"



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_CLI_func>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_CLI_func(int argc, char *argv[]) {


	if (argc != 2) {
		return MEA_ERROR;
	}


	if (MEA_OS_strcmp(argv[1],"all") != 0) {
		return MEA_ERROR;
	}


    if (MEA_Example_main() != MEA_OK) {
	    CLI_print("Error: ,MEA_Example_main_failed \n");
	} else {
        CLI_print("Done.\n");
	}

   return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_Example_CLI_Init>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_Example_CLI_Init() {


   CLI_defineCmd("MEA example case1",
			     (CmdFunc)MEA_Example_CLI_func,
				 "Example case1 configuration \n",
				 "Usage: MEA example all            \n"
				 "----------------------------------\n"
				 "       all - create all entities  \n"
				 "----------------------------------\n"
				 "Example: example all              \n");
   
	return MEA_OK;
}

