
#include <stdio.h>
#include <sys/timeb.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include "mea_api.h"

#ifdef ENET_JSON_DB_TEST
void enet_set_port_env(const uint32_t port_id);
#else
#define enet_set_port_env(port_id) /**/;
#endif

#if 0
///////////////
typedef struct{

       MEA_Counter_t Rx_Pkts;
       MEA_Counter_t Rx_BC_Pkts;
       MEA_Counter_t Rx_MC_Pkts;
       MEA_Counter_t Rx_UC_Pkts;
       MEA_Counter_t Rx_64Octets_Pkts;
       MEA_Counter_t Rx_65to127Octets_Pkts;
       MEA_Counter_t Rx_128to255Octets_Pkts;
       MEA_Counter_t Rx_256to511Octets_Pkts;
       MEA_Counter_t Rx_512to1023Octets_Pkts;
       MEA_Counter_t Rx_1024to1518Octets_Pkts;
       MEA_Counter_t Rx_1519to2047Octets_Pkts;
       MEA_Counter_t Rx_2048toMaxOctets_Pkts;
       MEA_Counter_t Rx_CRC_Err;
       MEA_Counter_t Rx_fragmant_Pkts;
       MEA_Counter_t Rx_Oversize_Bytes;
       MEA_Counter_t Rx_Bytes;
       MEA_Counter_t Rx_Drop_Bytes;
       MEA_Counter_t Rx_Error_Pkts;

       MEA_Counter_t Rx_Mac_Drop_Pkts;
       MEA_Counter_t Rx_Mac_DropSmallfragment_Pkts;

       MEA_Uint64    Rx_ratePacket;
       MEA_Uint64    Rx_rate;
   /********* TX Counter **********************/

       MEA_Counter_t Tx_Pkts;
       MEA_Counter_t Tx_BC_Pkts;
       MEA_Counter_t Tx_MC_Pkts;
       MEA_Counter_t Tx_UC_Pkts;
       MEA_Counter_t Tx_64Octets_Pkts;
       MEA_Counter_t Tx_65to127Octets_Pkts;
       MEA_Counter_t Tx_128to255Octets_Pkts;
       MEA_Counter_t Tx_256to511Octets_Pkts;
       MEA_Counter_t Tx_512to1023Octets_Pkts;
       MEA_Counter_t Tx_1024to1518Octets_Pkts;
       MEA_Counter_t Tx_1519to2047Octets_Pkts;
       MEA_Counter_t Tx_2048toMaxOctets_Pkts;
       MEA_Counter_t Tx_Underrun_Pkts;
       MEA_Counter_t Tx_Overrun_Pkts;
       MEA_Counter_t Tx_Oversize_Pkts;
       MEA_Counter_t Tx_Oversize_Bytes;
       MEA_Counter_t Tx_CRC_Err;
       MEA_Uint64    Tx_L2CP_Pkts;
       MEA_Counter_t Tx_Drop_Pkts_egr_rule;
       MEA_Counter_t Tx_Bytes;
       MEA_Counter_t Tx_Drop_Bytes;
       MEA_Uint64    Tx_ratePacket;
       MEA_Uint64    Tx_rate;

       time_t t1;

} MEA_Counters_RMON_dbt;
///////////////
#endif


static uint64_t get_curr_time_ms() {

	struct timeb curr_time;
	uint64_t timestamp_ms;
	ftime(&curr_time);
	timestamp_ms = 
		((uint64_t)curr_time.time) * 1000ll +
		(uint64_t)curr_time.millitm;
	return timestamp_ms;
};


static MEA_Status get_port_data(uint32_t port_id, MEA_Counters_RMON_dbt *counters) {

	enet_set_port_env(port_id);
	return MEA_API_Get_Counters_RMON(MEA_UNIT_0, port_id, counters);
};


static void print_port_data_csv(FILE *fdb, const MEA_Counters_RMON_dbt *counters) {

	fprintf(fdb, "%llu, ", counters->Rx_Mac_Drop_Pkts.val);
	fprintf(fdb, "%llu, ", counters->Rx_ratePacket.val);
	fprintf(fdb, "%llu, ", (counters->Rx_rate.val * 8));
	fprintf(fdb, "%llu, ", counters->Tx_ratePacket.val);
	fprintf(fdb, "%llu ", (counters->Tx_rate.val * 8));
};


#define PHY_TOTAL_COUNTERS_ID (256)

static void print_port_data(FILE *fdb, uint32_t port_id, const MEA_Counters_RMON_dbt *counters, const char delimiter) {

	fprintf(fdb, "{ \n");
	if(port_id == PHY_TOTAL_COUNTERS_ID) {
		fprintf(fdb, "	\"name\": \"phy-total\", \n");
	}
	else {
		fprintf(fdb, "	\"name\": \"%d\", \n", port_id);
	};
	fprintf(fdb, "	\"stats\": [ ");
	print_port_data_csv(fdb, counters);
	fprintf(fdb, " ] \n");
	fprintf(fdb, "}%c \n", delimiter);
};


static void reset_counters(MEA_Counters_RMON_dbt *counters) {

	counters->Rx_Mac_Drop_Pkts.val = 0;
	counters->Rx_ratePacket.val = 0;
	counters->Rx_rate.val = 0;
	counters->Tx_ratePacket.val = 0;
	counters->Tx_rate.val = 0;
};


static void accumulate_counters(MEA_Counters_RMON_dbt *accumulated_counters, const MEA_Counters_RMON_dbt *counters) {

	accumulated_counters->Rx_Mac_Drop_Pkts.val += counters->Rx_Mac_Drop_Pkts.val;
	accumulated_counters->Rx_ratePacket.val += counters->Rx_ratePacket.val;
	accumulated_counters->Rx_rate.val += counters->Rx_rate.val;
	accumulated_counters->Tx_ratePacket.val += counters->Tx_ratePacket.val;
	accumulated_counters->Tx_rate.val += counters->Tx_rate.val;
};


static void print_nic_data(FILE *fdb, uint64_t timestamp_ms, const uint32_t nic_name_len, const char *nic_name, const char delimiter) {

	fprintf(fdb, "{ \n");
	fprintf(fdb, "	\"timestamp_ms\": \"%" PRIu64 "\", \n", timestamp_ms);
	fprintf(fdb, "	\"nic_name\": \"%.*s\", \n", nic_name_len, nic_name);
	fprintf(fdb, "	\"ports\": [ ");
	MEA_Counters_RMON_dbt phy_total_counters;
	reset_counters(&phy_total_counters);
	MEA_Counters_RMON_dbt counters;
	get_port_data(104, &counters); print_port_data(fdb, 104, &counters, ','); accumulate_counters(&phy_total_counters, &counters);
	get_port_data(105, &counters); print_port_data(fdb, 105, &counters, ','); accumulate_counters(&phy_total_counters, &counters);
	get_port_data(106, &counters); print_port_data(fdb, 106, &counters, ','); accumulate_counters(&phy_total_counters, &counters);
	get_port_data(107, &counters); print_port_data(fdb, 107, &counters, ','); accumulate_counters(&phy_total_counters, &counters);
	print_port_data(fdb, PHY_TOTAL_COUNTERS_ID, &phy_total_counters, ',');
	get_port_data(127, &counters); print_port_data(fdb, 127, &counters, ' ');
	fprintf(fdb, "	] \n");
	fprintf(fdb, "}%c \n", delimiter);
};


void enet_json_db_periodic_collect() {

	static const uint32_t fdb_buff_size = 32768;
	static const uint64_t db_update_interval_ms = 1000;
	static uint64_t db_update_timestamp_ms = 0;
	static const uint32_t nic_name_max_len = 64;
	static const uint32_t db_file_max_len = 256;
	static char *nic_name = NULL;
	static const char default_json_db_path[] = "/tmp/enet_json_db";
	static char *json_db_file = NULL;

	uint64_t timestamp_ms = get_curr_time_ms();
	if(nic_name == NULL) {
		nic_name = calloc(nic_name_max_len + 1, sizeof(char));
		json_db_file = calloc(db_file_max_len + 1, sizeof(char));
		const char *got_json_db_path = getenv("MEA_JSON_DB_PATH");
		const char *got_name = getenv("MEA_CARD");
		if(got_name) {
			snprintf(nic_name, nic_name_max_len, "enet_%s", got_name);
		}
		else {
			snprintf(nic_name, nic_name_max_len, "enet_%s_%" PRIu64 "_", "undef_MEA_CARD", timestamp_ms);
		};
		snprintf(json_db_file, db_file_max_len, "%s/%s.json", (got_json_db_path) ? got_json_db_path : default_json_db_path, nic_name);
	};
	if((timestamp_ms - db_update_timestamp_ms) > db_update_interval_ms) {
		char fdb_buff[fdb_buff_size];
		FILE *fdb_mem = fmemopen((void *)fdb_buff, fdb_buff_size, "w");
		fprintf(fdb_mem, "{ \"enet_data\": \n");
		print_nic_data(fdb_mem, timestamp_ms, strlen(nic_name), nic_name, ' ');
		fprintf(fdb_mem, "} \n");
		fclose(fdb_mem);
		FILE *fdb = fopen(json_db_file, "w");
		if(fdb) {
			fprintf(fdb, "%s", fdb_buff);
			fclose(fdb);
		}
		else {
			perror(json_db_file);
		};
		db_update_timestamp_ms = timestamp_ms;
	};
};


