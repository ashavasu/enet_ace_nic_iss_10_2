/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_lpm_drv.h"
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
#include <limits.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#endif
/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

#define NUMOFBITS_IPV4 32
#define NUMOFBITS_IPV6 128
#define NUMOFBITS_IN_QUARTER_ADDRESS_IPV6 (NUMOFBITS_IPV6 / 4)
#define SIZE_OF_HARDWARE_TBL_ENTRY 5
#define MAX_VRF 7
#define NUM_OF_REGISTERS_TO_WRITE_TO_HW 5
#define MIN_PREFIX_LEN_IPV4 8
#define MAX_PREFIX_LEN_IPV4 32
#define MIN_PREFIX_LEN_IPV6 16
#define MAX_PREFIX_LEN_IPV6 64
#define NUM_OF_INTS_IN_IPV6 4
#define NUM_OF_HARDWARE_TBL_DEVISIONS 2
#define MAX_MASKKEYTYPE 3
/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/

typedef struct MEA_LPM_Entry_t MEA_LPM_Entry_t;

/** 
 * @brief Action function to check if a < b
 *
 * @param _a : element to test
 * @param _b : element to test against
 * @return 1 if _a < _b, 0 if _a == _b, -1 otherwise
 */
typedef int (*mea_drv_cmprFunction)(void* _a, void* _b);

/** 
 * @brief Funtion for setting a data entry into a hardware compatible memory setting.
 *
 * @param entry_p : pointer to an entry
 * @return void
 */
typedef void (*mea_drv_setHardwareCompatibleDataFunc)(MEA_LPM_Entry_t* entry_p);

/** 
 * @brief Funtion for zeroing a part (sub-number) of an ip address
 *
 * @param key : pointer to an IP address
 * @return void
 */
typedef void (*mea_drv_zeroSubnumberFunc)(MEA_LPM_Entry_key_t *key);

/** 
 * @brief Funtion for zeroing a part (sub-number) of an ip address
 *
 * @param key : pointer to an IP address
 * @param ipString : pointer to a string buffer with length of an IPv6 address length.
 * @return void
 */
typedef void (*mea_drv_printIPvXToString)(const MEA_LPM_Entry_key_t *key, char ipString[]);

typedef enum
{
    IPv4 = 0,
    IPv6
}MEA_LPM_IP_Version;

struct MEA_LPM_Entry_t
{
    MEA_Bool             valid;
    MEA_LPM_Entry_key_t  key;
    MEA_LPM_Entry_data_t data;
    MEA_Uint32 hwLpmData[SIZE_OF_HARDWARE_TBL_ENTRY];
};

// Bit field for IPv4:
// [159:94] – zero padding
// [93] – LPM valid  ‘0’ - lpm entry not valid, ‘1’- lpm entry valid
// 92:91] – mask_key_type (will be used for the loops on the internal ports
// [90:88] – VRF 
// [87:64] – 24b LSB mask  means : the 8 MSB bits of the dst_ipv4 will be always ‘ff’ on the HW(we don’t support on /7 and lower, according to Roy - not needed) and the 24b will be mask the 24 LSB of the dst_ipv4 - For example: /8 0x000000, /16  0xff0000, /24  0xffff00, /32  0xffffff, /27  0xffffe0, /18  0xffc000.
// [63:32] – dst_ipv4
// [31: 19] - zero padding
// [18] – LPM Action ID Valid
// [17:0] – LPM Action ID same as fwd_action

const MEA_Uint32 hwDataIntervalsIPv4[] = { 18, 1, 13, 32, 24, 3, 2, 1, 66 };

// Bit field for IPv6:
// [159:148] – zero padding
// [147] – LPM valid  ‘0’ - lpm entry not valid, ‘1’- lpm entry valid
// [146:144] – VRF 
// [143:96] – 48b LSB mask of the 64 MSB  means : the 16 MSB bits of the dst_ipv6 will be always ‘ffff’ on the HW(we don’t support on /15 and lower, according to Roy - not needed) and the 48b will be mask the 48 LSB of the dst_ipv6[127:64] - For example:/16  0x0000_00000000, /24  0x0xff00_00000000, /32  0xffff_00000000, /48  0xffff_ffff0000, /64  0xffff_ffffffff,    /50  0xffff_ffffc000.
// [95:32] – dst_ipv6 (64 MSB)
// LPM cntx bits:32b!
// [31: 19] - zero padding
// [18] – LPM Action ID Valid
// [17:0] – LPM Action ID same as fwd_action

const MEA_Uint32 hwDataIntervalsIPv6[] = { 18, 1, 13, 64, 32, 16, 3, 1, 12 };

/** 
 * @brief This struct serves as IPvX tables - means generic in terms of IP address method - IPv4 or IPv6
 */
typedef struct {
    MEA_LPM_Entry_t* softWareLpmTbl;
    MEA_Uint32 numberOfElements;
    MEA_Uint32 capacity;
    mea_drv_cmprFunction cmpr; // This is used to compare 2 IP address
    mea_drv_setHardwareCompatibleDataFunc setHardwareCompatibleData; // this function is used to set the values of an lpm entry to the hardware compatible form.
    mea_drv_zeroSubnumberFunc zeroHostFunc;// This is used to zero the bits of an IP address after the prefix (to the right of the prefix will be zeroed)
    mea_drv_printIPvXToString printIpToString;
} MEA_LPM_IPvX_Table;

typedef struct {
    MEA_LPM_IPvX_Table IPv4Tbl;
    MEA_LPM_IPvX_Table IPv6Tbl;
} MEA_LPM_Manager;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

static MEA_Bool MEA_LPM_managerIsInitialized = 0;
static MEA_LPM_Manager MEA_LPM_lpmManager;
static MEA_Uint32 hardwareTblOffsets[NUM_OF_HARDWARE_TBL_DEVISIONS]; // In the hardware there is one table for both IPv4 and IPv6. First is the IPv4 table, and after is the IPv6 table (with an offset of the size of the IPv4 table.).

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/

static void mea_drv_copyEntryUpOneStep(MEA_LPM_Entry_t* dst_entry_p, MEA_LPM_Entry_t* src_entry_p, MEA_Uint32 sizeofElmnt);
static void mea_drv_zeroHostBitsIPv4(MEA_LPM_Entry_key_t* key);
static void mea_drv_zeroHostBitsIPv6(MEA_LPM_Entry_key_t* key);
static MEA_Status mea_drv_updateTblToHardware(MEA_Uint32 fromIndex, MEA_Uint32 toIndex, MEA_LPM_Entry_t* swTbl, MEA_LPM_IP_Version ipVer);
static MEA_Status mea_drv_createLpmIPvX(MEA_LPM_Entry_key_t *key_pi, MEA_LPM_Entry_data_t *data_pi, MEA_Uint32 *index_o, MEA_LPM_IPvX_Table *lpmTbl);
static MEA_Status mea_drv_deleteLpmIPvX(MEA_LPM_Entry_key_t *key_pi, MEA_Uint32 *index, MEA_LPM_IPvX_Table *lpmTbl);
static MEA_Status mea_drv_checkIpVersion(const MEA_LPM_Entry_key_t *key_pi, MEA_LPM_IP_Version* ipVer);
static MEA_Status mea_drv_getIPvXTable(MEA_LPM_IP_Version IPver, MEA_LPM_IPvX_Table** lpmTbl);
static int mea_drv_cmprIPv4(void* _a, void* _b);
static int mea_drv_cmprIPv6(void* _a, void* _b);
static void mea_drv_printIPv4ToString(const MEA_LPM_Entry_key_t *key, char ipString[]);
static void mea_drv_printIPv6ToString(const MEA_LPM_Entry_key_t *key, char ipString[]);
static void mea_drv_setHardwareCompatibleDataIpv4(MEA_LPM_Entry_t* entry_p);
static void mea_drv_setHardwareCompatibleDataIpv6(MEA_LPM_Entry_t* entry_p);
static int mea_drv_cmprIPv6Address(MEA_Uint32* ip1, MEA_Uint32* ip2);
static void mea_drv_zeroHostBits(MEA_Uint32 *ip, MEA_Uint8 maskInBits);
static MEA_Bool mea_drv_isPrefixLenValid(const MEA_LPM_Entry_key_t* key, MEA_LPM_IP_Version ipVer);
static MEA_Bool mea_drv_isMaskKeyTypeValid(const MEA_LPM_Entry_key_t* key, MEA_LPM_IP_Version ipVer);

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             external functions implementation                              */
/*----------------------------------------------------------------------------*/

MEA_Status mea_drv_Init_Lpm(MEA_Unit_t unit_i)
{
    if((!MEA_LPM_SUPPORT) || (MEA_LPM_managerIsInitialized))
    {
        return MEA_OK;
    }
    // IPv4 table:
    MEA_LPM_lpmManager.IPv4Tbl.softWareLpmTbl = malloc((MEA_LPM_IPV4_SIZE) * sizeof(MEA_LPM_Entry_t));
    if(MEA_LPM_lpmManager.IPv4Tbl.softWareLpmTbl == NULL)
    {
        return MEA_ERROR;
    }
    // IPv6 table:
    MEA_LPM_lpmManager.IPv6Tbl.softWareLpmTbl = malloc((MEA_LPM_IPV6_SIZE) * sizeof(MEA_LPM_Entry_t));
    if(MEA_LPM_lpmManager.IPv6Tbl.softWareLpmTbl == NULL)
    {
        free(MEA_LPM_lpmManager.IPv4Tbl.softWareLpmTbl);
        return MEA_ERROR;
    }
    MEA_OS_memset(MEA_LPM_lpmManager.IPv4Tbl.softWareLpmTbl, 0, (MEA_LPM_IPV4_SIZE) * sizeof(MEA_LPM_Entry_t));
    MEA_OS_memset(MEA_LPM_lpmManager.IPv6Tbl.softWareLpmTbl, 0, (MEA_LPM_IPV6_SIZE) * sizeof(MEA_LPM_Entry_t));
    MEA_LPM_lpmManager.IPv4Tbl.numberOfElements = 0;
    MEA_LPM_lpmManager.IPv6Tbl.numberOfElements = 0;
    MEA_LPM_lpmManager.IPv4Tbl.capacity = MEA_LPM_IPV4_SIZE;
    MEA_LPM_lpmManager.IPv6Tbl.capacity = MEA_LPM_IPV6_SIZE;
    MEA_LPM_lpmManager.IPv4Tbl.cmpr = mea_drv_cmprIPv4;
    MEA_LPM_lpmManager.IPv4Tbl.setHardwareCompatibleData = mea_drv_setHardwareCompatibleDataIpv4;
    MEA_LPM_lpmManager.IPv4Tbl.zeroHostFunc = mea_drv_zeroHostBitsIPv4;
    MEA_LPM_lpmManager.IPv4Tbl.printIpToString = mea_drv_printIPv4ToString;
    MEA_LPM_lpmManager.IPv6Tbl.cmpr = mea_drv_cmprIPv6;
    MEA_LPM_lpmManager.IPv6Tbl.zeroHostFunc = mea_drv_zeroHostBitsIPv6;
    MEA_LPM_lpmManager.IPv6Tbl.printIpToString = mea_drv_printIPv6ToString;
    MEA_LPM_lpmManager.IPv6Tbl.setHardwareCompatibleData = mea_drv_setHardwareCompatibleDataIpv6;

    hardwareTblOffsets[IPv4] = 0;
    hardwareTblOffsets[IPv6] = MEA_LPM_IPV4_SIZE;

    MEA_LPM_managerIsInitialized = 1;
    return MEA_OK;
}

MEA_Status mea_drv_Reinit_Lpm(MEA_Unit_t unit_i)
{
    MEA_Status err = MEA_OK;

    if (!MEA_LPM_SUPPORT){
       return MEA_OK;
    }
    
    
    if(!MEA_LPM_managerIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - LPM not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(MEA_LPM_lpmManager.IPv4Tbl.numberOfElements != 0) {
        err = mea_drv_updateTblToHardware(0, MEA_LPM_lpmManager.IPv4Tbl.numberOfElements - 1, MEA_LPM_lpmManager.IPv4Tbl.softWareLpmTbl, IPv4);
        if(err != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error in reiniting IPv4 LPM table ", __FUNCTION__);
        }
    }
    if(MEA_LPM_lpmManager.IPv6Tbl.numberOfElements != 0){
        err = mea_drv_updateTblToHardware(0, MEA_LPM_lpmManager.IPv6Tbl.numberOfElements - 1, MEA_LPM_lpmManager.IPv6Tbl.softWareLpmTbl, IPv6);
        if(err != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error in reiniting IPv6 LPM table ", __FUNCTION__);
        }
    }
    return err;
}

MEA_Status mea_drv_Conclude_Lpm(MEA_Unit_t unit_i)
{
    if (!MEA_LPM_SUPPORT) {
        return MEA_OK;
    }
    
    if(!MEA_LPM_managerIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - LPM not initialized \n", __FUNCTION__);
        return MEA_ERROR;
    }
	if (MEA_LPM_lpmManager.IPv4Tbl.softWareLpmTbl != NULL) {
		MEA_OS_free(MEA_LPM_lpmManager.IPv4Tbl.softWareLpmTbl);
	}
	if (MEA_LPM_IPV6_SIZE != 0) {
		if (MEA_LPM_lpmManager.IPv6Tbl.softWareLpmTbl != NULL) {
			MEA_OS_free(MEA_LPM_lpmManager.IPv6Tbl.softWareLpmTbl);
		}
	}
	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude LPM \n");

    return MEA_OK;
}

static int mea_drv_cmprIPv4(void* _a, void* _b)
{
    MEA_LPM_Entry_key_t *a = ((MEA_LPM_Entry_key_t *)_a);
    MEA_LPM_Entry_key_t *b = ((MEA_LPM_Entry_key_t *)_b);
    if((a->IpMaskInBits) < (b->IpMaskInBits))
    {
        return 1;
    }
    if((a->IpMaskInBits) == (b->IpMaskInBits))
    {
        if((a->IpAddr.all) < (b->IpAddr.all))
        {
            return 1;
        }
        if((a->IpAddr.all) == (b->IpAddr.all))
        {
            if((a->vrf) < (b->vrf))
            {
                return 1;
            }
            if((a->vrf) == (b->vrf))
            {
                return 0;
            }
        }
    }
    return -1;
}

static int mea_drv_cmprIPv6Address(MEA_Uint32* ip1, MEA_Uint32* ip2)
{
    MEA_Uint32 i = 0;
    for(; i < NUM_OF_INTS_IN_IPV6; ++i)
    {
        if(ip1[i] > ip2[i])
        {
            return -1;
        }
        else if(ip1[i] < ip2[i])
        {
            return 1;
        }
    }
    return 0;
}

static int mea_drv_cmprIPv6(void* _a, void* _b)
{
    int cmprIPs;
    MEA_LPM_Entry_key_t *a = ((MEA_LPM_Entry_key_t *)_a);
    MEA_LPM_Entry_key_t *b = ((MEA_LPM_Entry_key_t *)_b);
    if((a->IpMaskInBits) < (b->IpMaskInBits))
    {
        return 1;
    }
    if((a->IpMaskInBits) == (b->IpMaskInBits))
    {
        cmprIPs = mea_drv_cmprIPv6Address(a->Ipv6Addr, b->Ipv6Addr);
        if(cmprIPs == 1) // a < b 
        {
            return 1;
        }
        if(cmprIPs == 0) // equals
        {
            if((a->vrf) < (b->vrf))
            {
                return 1;
            }
            if((a->vrf) == (b->vrf))
            {
                return 0;
            }
        }
    }
    return -1;
}

static void mea_drv_printIPv4ToString(const MEA_LPM_Entry_key_t *key, char ipString[])
{
    struct in_addr address;
    address.s_addr = MEA_OS_htonl(key->IpAddr.all);
    const char *retStr = inet_ntop(AF_INET, &address, ipString, INET6_ADDRSTRLEN);
    if(retStr == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Problem converting IPv4 address to string ", __FUNCTION__);
    }
}

static void mea_drv_printIPv6ToString(const MEA_LPM_Entry_key_t *key, char ipString[])
{
    struct in6_addr address;
    ((MEA_Uint32 *)(address.s6_addr))[0] = MEA_OS_htonl(key->Ipv6Addr[0]);
    ((MEA_Uint32 *)(address.s6_addr))[1] = MEA_OS_htonl(key->Ipv6Addr[1]);
    ((MEA_Uint32 *)(address.s6_addr))[2] = MEA_OS_htonl(key->Ipv6Addr[2]);
    ((MEA_Uint32 *)(address.s6_addr))[3] = MEA_OS_htonl(key->Ipv6Addr[3]);
    const char *retStr = inet_ntop(AF_INET6, &address, ipString, INET6_ADDRSTRLEN);
    if(retStr == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Problem converting IPv6 address to string ", __FUNCTION__);
    }
}

static void mea_drv_zeroHostBits(MEA_Uint32 *ip, MEA_Uint8 maskInBits)
{
    MEA_Uint32 startPlace = 0;
    MEA_Uint32 numOfBitsToZero = NUMOFBITS_IPV4 - (maskInBits);
    MEA_Uint32 temp = 0;
    temp = ~temp;
    temp <<= numOfBitsToZero;
    temp = ~temp;
    temp <<= startPlace;
    temp = ~temp;
    *ip = (*ip) & temp;
}

static void mea_drv_zeroHostBitsIPv4(MEA_LPM_Entry_key_t* key)
{
    mea_drv_zeroHostBits(&(key->IpAddr.all), key->IpMaskInBits);
}

static void mea_drv_zeroHostBitsIPv6(MEA_LPM_Entry_key_t* key)
{
    MEA_Uint32 i = 0;
    MEA_Uint32 quarter = (key->IpMaskInBits - 1) / NUMOFBITS_IPV4; // will yield the place in the IPv6 address if treated as 4 * 32 bits
    MEA_Uint32 *ip = key->Ipv6Addr;
    //zero all the LSB of the rightmost ints:
    for(i = quarter + 1; i < NUM_OF_INTS_IN_IPV6; ++i)
    {
        MEA_OS_memset(ip + i,'\0', sizeof(MEA_Uint32));
    }
    //now zero the int that the mask ends in:
    mea_drv_zeroHostBits(ip + quarter, key->IpMaskInBits - (quarter * NUMOFBITS_IPV4));
}

static void mea_drv_copyEntryUpOneStep(MEA_LPM_Entry_t* dst_entry_p, MEA_LPM_Entry_t* src_entry_p, MEA_Uint32 sizeofElmnt)
{
    dst_entry_p = src_entry_p;
    dst_entry_p++;

    MEA_OS_memcpy(dst_entry_p, src_entry_p, sizeofElmnt);
}

static void mea_drv_setHardwareCompatibleIpv4(MEA_Uint32 count_shift, MEA_Uint32 dataLength, const MEA_LPM_Entry_key_t* valToSet, MEA_Uint32* target)
{
    MEA_OS_insert_value(count_shift, dataLength, (valToSet->IpAddr.all), target);
}

static void mea_drv_setHardwareCompatibleIpv6(MEA_Uint32 count_shift, MEA_Uint32 dataLength, const MEA_LPM_Entry_key_t* valToSet, MEA_Uint32* target)
{
    // write to hardware only 64 MSB of the IPv6 address.
    MEA_OS_insert_value(count_shift, NUMOFBITS_IN_QUARTER_ADDRESS_IPV6, (valToSet->Ipv6Addr[1]), target);
    MEA_OS_insert_value(count_shift + NUMOFBITS_IN_QUARTER_ADDRESS_IPV6, NUMOFBITS_IN_QUARTER_ADDRESS_IPV6, (valToSet->Ipv6Addr[0]), target);
}

static void mea_drv_setHardwareCompatibleDataIpv4(MEA_LPM_Entry_t* entry_p)
{
    MEA_Uint32 count_shift = 0;
    MEA_Uint32 fieldNum = 0;
    MEA_Uint32 maskBitwise;
    MEA_Uint32 mask = entry_p->key.IpMaskInBits;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv4[fieldNum], (MEA_Uint32)entry_p->data.actionId, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv4[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv4[fieldNum], (MEA_Uint32)entry_p->data.actionId_valid, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv4[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv4[fieldNum], (MEA_Uint32)0, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv4[fieldNum];
    ++fieldNum;

    mea_drv_setHardwareCompatibleIpv4(count_shift, hwDataIntervalsIPv4[fieldNum], &(entry_p->key), (entry_p->hwLpmData));
    count_shift += hwDataIntervalsIPv4[fieldNum];
    ++fieldNum;

    maskBitwise = UINT_MAX;
    maskBitwise <<= (NUMOFBITS_IPV4 - mask);

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv4[fieldNum], maskBitwise, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv4[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv4[fieldNum], (MEA_Uint32)(entry_p->key.vrf), entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv4[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv4[fieldNum], (MEA_Uint8)(entry_p->key.maskKeyType), entry_p->hwLpmData); //mask_key_type
    count_shift += hwDataIntervalsIPv4[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv4[fieldNum], (MEA_Uint32)1, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv4[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv4[fieldNum], (MEA_Uint32)0, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv4[fieldNum];
}

static void mea_drv_setHardwareCompatibleDataIpv6(MEA_LPM_Entry_t* entry_p)
{
    MEA_Uint32 count_shift = 0;
    MEA_Uint32 fieldNum = 0;
    MEA_uint64 maskBitwise;
    MEA_Uint32 maskBitwiseMSB = 0;
    MEA_Uint32 maskBitwiseLSB = 0;
    MEA_Uint32 mask = entry_p->key.IpMaskInBits;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv6[fieldNum], (MEA_Uint32)entry_p->data.actionId, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv6[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv6[fieldNum], (MEA_Uint32)entry_p->data.actionId_valid, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv6[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv6[fieldNum], (MEA_Uint32)0, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv6[fieldNum];
    ++fieldNum;

    mea_drv_setHardwareCompatibleIpv6(count_shift, hwDataIntervalsIPv6[fieldNum], &(entry_p->key), (entry_p->hwLpmData));
    count_shift += hwDataIntervalsIPv6[fieldNum];
    ++fieldNum;

    maskBitwise = ULONG_MAX;
    maskBitwise <<= (((NUMOFBITS_IPV6 / 2) - mask) + MIN_PREFIX_LEN_IPV6);
    maskBitwiseLSB = (MEA_Uint32)(maskBitwise >> (hwDataIntervalsIPv6[fieldNum + 1]));
    maskBitwiseMSB = (MEA_Uint32)(maskBitwise >> (hwDataIntervalsIPv6[fieldNum]));

    // Split the insertion of maskBitwise into 2 separate occasions, because the function 'MEA_OS_insert_value' can only write 32 bits at a time, and the mask is 48 long:
    // mask write 1:
    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv6[fieldNum], maskBitwiseLSB, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv6[fieldNum];
    ++fieldNum;
    // mask write 2:
   	MEA_OS_insert_value(count_shift, hwDataIntervalsIPv6[fieldNum], maskBitwiseMSB, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv6[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv6[fieldNum], (MEA_Uint32)(entry_p->key.vrf), entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv6[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv6[fieldNum], (MEA_Uint32)1, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv6[fieldNum];
    ++fieldNum;

    MEA_OS_insert_value(count_shift, hwDataIntervalsIPv6[fieldNum], (MEA_Uint32)0, entry_p->hwLpmData);
    count_shift += hwDataIntervalsIPv6[fieldNum];
}

static MEA_Status mea_drv_updateTblToHardware(MEA_Uint32 fromIndex, MEA_Uint32 toIndex, MEA_LPM_Entry_t* swTbl, MEA_LPM_IP_Version ipVer)
{
    if(fromIndex < toIndex)
    {
        for(; fromIndex <= toIndex; ++fromIndex)
        {
            if(mea_drv_write_IF_TBL_X_offset_UpdateEntry_pArray(MEA_UNIT_0, ENET_IF_CMD_PARAM_TBL_LPM_PATTERN, fromIndex + hardwareTblOffsets[ipVer], NUM_OF_REGISTERS_TO_WRITE_TO_HW, swTbl[fromIndex].hwLpmData) != MEA_OK) 
            {
                return MEA_ERROR;
            }
        }
    }
    else
    {
        for(; fromIndex > toIndex; --fromIndex)
        {
            if(mea_drv_write_IF_TBL_X_offset_UpdateEntry_pArray(MEA_UNIT_0, ENET_IF_CMD_PARAM_TBL_LPM_PATTERN, fromIndex + hardwareTblOffsets[ipVer], NUM_OF_REGISTERS_TO_WRITE_TO_HW, swTbl[fromIndex].hwLpmData) != MEA_OK) 
            {
                return MEA_ERROR;
            }
        }
        if(fromIndex == toIndex)
        {
            if(mea_drv_write_IF_TBL_X_offset_UpdateEntry_pArray(MEA_UNIT_0, ENET_IF_CMD_PARAM_TBL_LPM_PATTERN, fromIndex + hardwareTblOffsets[ipVer], NUM_OF_REGISTERS_TO_WRITE_TO_HW, swTbl[fromIndex].hwLpmData) != MEA_OK) 
            {
                return MEA_ERROR;
            }
        }
    }
    return MEA_OK;
}

static MEA_Status mea_drv_getIPvXTable(MEA_LPM_IP_Version IPver, MEA_LPM_IPvX_Table** lpmTbl)
{
    if(IPver == IPv4)
    {
        *lpmTbl = &(MEA_LPM_lpmManager.IPv4Tbl);
    }
    else
    {
        *lpmTbl = &(MEA_LPM_lpmManager.IPv6Tbl);
    }
    return MEA_OK;   
}

static MEA_Status mea_drv_checkIpVersion(const MEA_LPM_Entry_key_t *key_pi, MEA_LPM_IP_Version* ipVer)
{
    
    if ((key_pi->Ipv4Addr_en == 1) && (key_pi->Ipv6Addr_en == 1))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Choose only one type IPv4 or IPv6 ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(key_pi->Ipv4Addr_en == 1)
    {
    	if(MEA_LPM_IPV4_SIZE == 0)
    	{
    		return MEA_ERROR;
    	}
        *ipVer = IPv4;
    }
    else if(key_pi->Ipv6Addr_en == 1)
    {
    	if(MEA_LPM_IPV6_SIZE == 0)
    	{
    		return MEA_ERROR;
    	}
        *ipVer = IPv6;
    }
    else
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - No IP version was chosen ", __FUNCTION__);
        return MEA_ERROR;
    }
    return MEA_OK;
}

static MEA_Status mea_drv_createLpmIPvX(MEA_LPM_Entry_key_t *key_pi, MEA_LPM_Entry_data_t *data_pi, MEA_Uint32 *index_o, MEA_LPM_IPvX_Table *lpmTbl)
{
    MEA_LPM_Entry_t *entry_p;
    MEA_Uint32       index;
    MEA_Uint32       cmprRes;
    char ipString[INET6_ADDRSTRLEN];
    for ((index=0, entry_p=&(lpmTbl->softWareLpmTbl[0]));
    ((index < (lpmTbl->capacity)) && (entry_p->valid)) ;
    index++,entry_p++) {
        cmprRes = lpmTbl->cmpr(&(entry_p->key), key_pi);
        if(cmprRes == 1) // (&(entry_p->key) < key_pi)
        {
            break;
        }
        if(cmprRes == 0) // (&(entry_p->key) == key_pi)
        {
            lpmTbl->printIpToString(key_pi, ipString);
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Duplicate LPM Entry - Address:%s Prefix:%u VRF:%u ", __FUNCTION__, ipString, key_pi->IpMaskInBits, key_pi->vrf);
            return MEA_ERROR;   
        }
    }
    if (index > ((lpmTbl->capacity)-1)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - No more free entry in LPM Table\n", __FUNCTION__);
        return MEA_ERROR;
    }
    if(key_pi->vrf > MAX_VRF)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - VRF can't be larger than %d. \n", __FUNCTION__, MAX_VRF);
        return MEA_ERROR;
    }
    if (entry_p->valid != MEA_FALSE) {
        MEA_Uint32       src_index; 
        MEA_LPM_Entry_t *dst_entry_p = 0;
        MEA_LPM_Entry_t *src_entry_p = 0;
        for (src_index = ((lpmTbl->capacity) - 2), src_entry_p=&(lpmTbl->softWareLpmTbl[src_index]);  
            src_entry_p->valid == MEA_FALSE;
            src_index--,src_entry_p--);

        for (; src_index > index; src_index--,src_entry_p--) {
            mea_drv_copyEntryUpOneStep(dst_entry_p, src_entry_p, sizeof(*dst_entry_p));
        }
        mea_drv_copyEntryUpOneStep(dst_entry_p, src_entry_p, sizeof(*dst_entry_p));
    }

    MEA_OS_memset(entry_p,'\0',sizeof(*entry_p));
    MEA_OS_memcpy(&(entry_p->key),key_pi,sizeof(entry_p->key));
    MEA_OS_memcpy(&(entry_p->data),data_pi,sizeof(entry_p->data));
    entry_p->valid = MEA_TRUE;
    lpmTbl->setHardwareCompatibleData(entry_p);
    if (index_o) {
        *index_o = index;
    }
    ++(lpmTbl->numberOfElements);
    return MEA_OK;
}

static MEA_Bool mea_drv_isPrefixLenValid(const MEA_LPM_Entry_key_t* key, MEA_LPM_IP_Version ipVer)
{
	if(ipVer == IPv4)
	{
		if(key->IpMaskInBits > MAX_PREFIX_LEN_IPV4)
			return MEA_FALSE;
	}
	else
	{
		if(key->IpMaskInBits > (MAX_PREFIX_LEN_IPV6))
			return MEA_FALSE;
	}
	return MEA_TRUE;
}

static MEA_Bool mea_drv_isMaskKeyTypeValid(const MEA_LPM_Entry_key_t* key, MEA_LPM_IP_Version ipVer)
{
    if(ipVer == IPv4)
    {
        if(key->maskKeyType > MAX_MASKKEYTYPE)
            return MEA_FALSE;
    }
    return MEA_TRUE;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                         <MEA_API_Create_LPM>                              */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_LPM(MEA_Unit_t            unit_i,
                              MEA_LPM_Entry_key_t *key_pi,
                              MEA_LPM_Entry_data_t *data_pi)
{
    MEA_Status err = MEA_OK;
    MEA_LPM_IPvX_Table* lpmTbl = NULL;
    MEA_LPM_IP_Version ipVer;
    MEA_Uint32 index_o;
    if (!MEA_LPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_LPM_SUPPORT not supported ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(!MEA_LPM_managerIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - LPM not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if((key_pi == NULL) || (data_pi == NULL))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - NULL pointer received ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_checkIpVersion(key_pi, &ipVer) != MEA_OK)
    {
        return MEA_ERROR;
    }
    if(mea_drv_isPrefixLenValid(key_pi, ipVer) != MEA_TRUE)
    {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Prefix length is longer than permitted. ", __FUNCTION__);
    	return MEA_ERROR;
    }
    if(mea_drv_isMaskKeyTypeValid(key_pi, ipVer) != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Mask Key Type is bigger than permitted. ", __FUNCTION__);
        return MEA_ERROR;
    }
    
    mea_drv_getIPvXTable(ipVer, &lpmTbl);
    lpmTbl->zeroHostFunc(key_pi); // set all the bits of the host to 0
    err = mea_drv_createLpmIPvX(key_pi, data_pi, &index_o, lpmTbl);
    if(err == MEA_ERROR)
    {
        return MEA_ERROR;
    }

    err = mea_drv_updateTblToHardware(lpmTbl->numberOfElements - 1, index_o, (lpmTbl->softWareLpmTbl), ipVer); //TODO rollback

    return err;
}

static MEA_Status mea_drv_deleteLpmIPvX(MEA_LPM_Entry_key_t *key_pi, MEA_Uint32 *index, MEA_LPM_IPvX_Table *lpmTbl)
{
    MEA_LPM_Entry_t *entry_p;
    MEA_Uint32       dst_index; 
    MEA_LPM_Entry_t *dst_entry_p;
    MEA_LPM_Entry_t *src_entry_p;
    MEA_Uint32 cmprRes;
    char ipString[NUMOFBITS_IPV6];
    for (*index=0, entry_p=&(lpmTbl->softWareLpmTbl[0]);
        ((*index < (lpmTbl->capacity)) && (entry_p->valid)) ;
        (*index)++,entry_p++) {
        cmprRes = lpmTbl->cmpr(&(entry_p->key), key_pi);
        if(cmprRes == 0) // found to be equal
        {
            break;
        }
    }
    if ((*index > (lpmTbl->capacity-1)) || (entry_p->valid == MEA_FALSE)) {
        lpmTbl->printIpToString(key_pi, ipString);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA LPM Key not found %s ", __FUNCTION__, ipString);
        return MEA_ERROR;
    }
    for (dst_index=*index,dst_entry_p=entry_p;
        dst_index < ((lpmTbl->capacity)-1);
        dst_index++,dst_entry_p++) {
        src_entry_p=dst_entry_p;
        src_entry_p++;
        if (src_entry_p->valid == MEA_FALSE) {
            break;
        }
        MEA_OS_memcpy(dst_entry_p, src_entry_p, sizeof(*dst_entry_p));
    }

    dst_entry_p->valid = MEA_FALSE;
    MEA_OS_memset(dst_entry_p, 0, sizeof(MEA_LPM_Entry_t));
    --(lpmTbl->numberOfElements);
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                         <MEA_API_Delete_LPM>                              */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_LPM(MEA_Unit_t            unit_i,
                              MEA_LPM_Entry_key_t  *key_pi)
{
    MEA_Uint32 index;
    MEA_Status err = MEA_OK;
    MEA_Uint32 numOfElmnts = 0;
    MEA_LPM_IPvX_Table* lpmTbl = NULL;
    MEA_LPM_IP_Version ipVer;

    if (!MEA_LPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_LPM_SUPPORT not supported ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(!MEA_LPM_managerIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - LPM not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(key_pi == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - NULL pointer received ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_checkIpVersion(key_pi, &ipVer) != MEA_OK)
    {
        return MEA_ERROR;
    }
    mea_drv_getIPvXTable(ipVer, &lpmTbl);
    
    lpmTbl->zeroHostFunc(key_pi); // set all the bits of the host to 0
    err = mea_drv_deleteLpmIPvX(key_pi, &index, (lpmTbl));
    if(err == MEA_ERROR)
    {
        return MEA_ERROR;
    }
    numOfElmnts = lpmTbl->numberOfElements;

	err = mea_drv_updateTblToHardware(index, numOfElmnts, (lpmTbl->softWareLpmTbl), ipVer); //TODO rollback

    return err;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                         <MEA_API_Set_LPM>                                 */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_LPM(MEA_Unit_t            unit_i,
                           MEA_LPM_Entry_key_t  *key_pi,
                           MEA_LPM_Entry_data_t *data_pi)
{
    if (!MEA_LPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_LPM_SUPPORT not supported ", __FUNCTION__);
        return MEA_ERROR;
    }

    return MEA_ERROR;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                         <MEA_API_Get_LPM>                                 */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_LPM(MEA_Unit_t            unit_i,
                           MEA_LPM_Entry_key_t  *key_pi,
                           MEA_LPM_Entry_data_t *data_po)
{
    MEA_Uint32       index;
    MEA_LPM_Entry_t *entry_p;
    MEA_LPM_IPvX_Table* lpmTbl;
    MEA_Uint32 capacity;
    MEA_Uint32 cmprRes;
    MEA_LPM_IP_Version ipVer;
    char ipString[NUMOFBITS_IPV6];
    if (!MEA_LPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_LPM_SUPPORT not supported ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(!MEA_LPM_managerIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - LPM not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if((key_pi == NULL) || (data_po == NULL))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - NULL pointer received ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_checkIpVersion(key_pi, &ipVer) != MEA_OK)
    {
        return MEA_ERROR;
    }
    mea_drv_getIPvXTable(ipVer, &lpmTbl);
    capacity = lpmTbl->capacity;
    for (index=0, entry_p=&(lpmTbl->softWareLpmTbl[0]); ((index < capacity) && (entry_p->valid)); index++,entry_p++) {
        cmprRes = lpmTbl->cmpr(&(entry_p->key), key_pi);
        if(cmprRes == 0) // found to be equal
        {
            break;
        }
    }
    if ((index > (capacity-1)) || (entry_p->valid == MEA_FALSE)) {
        lpmTbl->printIpToString(key_pi, ipString);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA LPM Key not found %s ", __FUNCTION__, ipString);
        return MEA_ERROR;
    }
    MEA_OS_memcpy(data_po,&(entry_p->data),sizeof(*data_po));
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                         <MEA_API_GetFirst_LPM>                            */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_LPM(MEA_Unit_t unit_i, MEA_LPM_Entry_key_t *key_po, MEA_LPM_Entry_data_t *data_po, MEA_Bool *found_o)
{
    MEA_Uint32       index=0;
    MEA_LPM_IPvX_Table* lpmTbl;
    MEA_LPM_IP_Version ipVer;
    if (!MEA_LPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_LPM_SUPPORT not supported ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(!MEA_LPM_managerIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - LPM not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if((key_po == NULL) || (found_o == NULL))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - NULL pointer received ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_checkIpVersion(key_po, &ipVer) != MEA_OK)
    {
        return MEA_ERROR;
    }
    mea_drv_getIPvXTable(ipVer, &lpmTbl);

    MEA_LPM_Entry_t *entry_p = &(lpmTbl->softWareLpmTbl[index]);

    if (entry_p->valid == MEA_FALSE) 
    {
        *found_o = MEA_FALSE;
        return MEA_OK;
    }
    MEA_OS_memcpy(key_po,&(entry_p->key),sizeof(*key_po));
    if (data_po) {
        MEA_OS_memcpy(data_po,&(entry_p->data),sizeof(*data_po));
    }
    *found_o = MEA_TRUE;
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                         <MEA_API_GetNext_LPM>                             */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_LPM(MEA_Unit_t            unit_i,
                               MEA_LPM_Entry_key_t  *key_pio,
                               MEA_LPM_Entry_data_t *data_po,
                               MEA_Bool             *found_o)
{
    MEA_Uint32       index;
    MEA_LPM_Entry_t *entry_p;
    MEA_LPM_IPvX_Table* lpmTbl;
    MEA_Uint32 capacity;
    MEA_Uint32 cmprRes;
    MEA_LPM_IP_Version ipVer;
    if (!MEA_LPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_LPM_SUPPORT not supported ", __FUNCTION__);
        return MEA_ERROR;
    }

    if(!MEA_LPM_managerIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - LPM not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if((key_pio == NULL) || (found_o == NULL))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - NULL pointer received ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_checkIpVersion(key_pio, &ipVer) != MEA_OK)
    {
        return MEA_ERROR;
    }
    mea_drv_getIPvXTable(ipVer, &lpmTbl);
    capacity = lpmTbl->capacity;
    for (index=0, entry_p=&(lpmTbl->softWareLpmTbl[0]); ((index < capacity) && (entry_p->valid)); index++,entry_p++) {
        cmprRes = lpmTbl->cmpr(&(entry_p->key), key_pio);
        if(cmprRes == 1)
        {
            break;
        }
    }
    if ((index > capacity - 1) || (entry_p->valid == MEA_FALSE)) {
        *found_o = MEA_FALSE;
        return MEA_OK;
    }
    MEA_OS_memcpy(key_pio,&(entry_p->key),sizeof(*key_pio));
    if(data_po != NULL)
    {
    	MEA_OS_memcpy(data_po,&(entry_p->data),sizeof(*data_po));
    }
    *found_o = MEA_TRUE;
    return MEA_OK;
}