/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_bm_ehp_drv.h"
#include "mea_drv_common.h"
#include "mea_init.h"
#include "enet_queue_drv.h"
#include "mea_action_drv.h"
#include "mea_editingMapping_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_BM_EHP_CNB_MAX_BIT(hwUnit_i)                (17)  //( (mea_drv_Get_DeviceInfo_bmf_stamp802_1p_support(MEA_UNIT_0,(hwUnit_i)) == 1) ? (17) :(14) )

#define MEA_BM_EHP_PROFILE2_ID_LM_CMD_CFM_WIDTH (3)
#define MEA_BM_EHP_PROFILE2_ID_LM_CMD_DASA_WIDTH (3)

#define MEA_BM_EHP_PROFILE2_ID_LM_PROFILE_WIDTH  (7)   //(hwUnit_i)   (MEA_OS_calc_from_size_to_width(ENET_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH((hwUnit_i))))

#define MEA_BM_EHP_PROFILE2_ID_LM_CMD_DSCP_VALUE_WIDTH (6)
#define MEA_BM_EHP_PROFILE2_ID_LM_CMD_DSCP_VALID_WIDTH (1)
#define MEA_BM_EHP_PROFILE2_ID_LM_CMD_ETH_CS_DOWNLINK_PACKET   (1)
#define MEA_BM_EHP_PROFILE2_ID_LM_CMD_IP_CHECSUM_CALC_PACKET   (1)

#define MEA_BM_EHP_IPSec_TYPE             ((MEA_IPSec_SUPPORT == MEA_TRUE) ? (2) : 0)
#define MEA_BM_EHP_PROF_IPSecESP_WIDTH    ((MEA_IPSec_SUPPORT == MEA_TRUE) ? (MEA_MAX_NUM_OF_IPSecESP_WIDTH) : 0)

#define MEA_BM_EHP_IP_IPSecTunnel_WIDTH    ((MEA_IPSec_SUPPORT == MEA_TRUE) ? (32) : 0)


#define MEA_BM_EHP_Command_dscp_type_WIDTH       2



#define MEA_READ_PACKET_TYPE MEA_FALSE
//#define MEA_SWAP_MAC

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/



static MEA_db_dbt                          MEA_EHP_ETH_db                   = NULL;
static MEA_db_dbt                          MEA_EHP_ATM_db                   = NULL;
static MEA_db_dbt                          MEA_EHP_WBRG_db                  = NULL;




static MEA_db_dbt                          MEA_EHP_da_db                    = NULL;
static MEA_db_dbt                          MEA_EHP_sa_lss_db                = NULL;
static MEA_db_dbt                          MEA_EHP_profileId_db             = NULL;
static MEA_db_dbt                          MEA_EHP_profile_stamping_db      = NULL;

static MEA_db_dbt                          MEA_EHP_profile_sa_mss_db        = NULL;
static MEA_db_dbt                          MEA_EHP_profile_ether_type_db    = NULL;
 
static MEA_db_dbt                          MEA_EHP_profile2Id_db            = NULL;
static MEA_db_dbt                          MEA_EHP_profile2_lm_db           = NULL;
static MEA_db_dbt                          MEA_EHP_fragmentSession_db       = NULL;
static MEA_db_dbt                          MEA_EHP_pw_control_db            = NULL;
static MEA_db_dbt                          MEA_EHP_mpls_label_db            = NULL;

MEA_editing_machin_dbt                     MEA_EHP_ProtocolMachine[512];

static MEA_drv_ehp_dbt                    *MEA_EHP_Table                    = NULL;
static MEA_drv_ehp_da_dbt                 *MEA_EHP_da_Table                 = NULL;
static MEA_drv_ehp_sa_lss_dbt             *MEA_EHP_sa_lss_Table             = NULL;
static MEA_drv_ehp_profileId_dbt          *MEA_EHP_profileId_Table          = NULL;
static MEA_drv_ehp_profile_stamping_dbt   *MEA_EHP_profile_stamping_Table   = NULL;

static MEA_drv_ehp_profile_sa_mss_dbt     *MEA_EHP_profile_sa_mss_Table     = NULL;
static MEA_drv_ehp_profile_ether_type_dbt *MEA_EHP_profile_ether_type_Table = NULL;
static MEA_drv_ehp_profile2Id_dbt         *MEA_EHP_profile2Id_Table         = NULL;
static MEA_drv_ehp_profile2_lm_dbt        *MEA_EHP_profile2_lm_Table        = NULL;

static MEA_drv_ehp_fragmentSession_dbt    *MEA_EHP_fragmentSession_Table    = NULL;
static MEA_drv_ehp_pw_control_dbt         *MEA_EHP_pw_control_Table         = NULL;
static MEA_drv_ehp_mpls_label_dbt         *MEA_EHP_mpls_label_Table         = NULL;




/* NotePad Table - helps us to perform grouping of Queues by ED_ID. 
                   index - ED_ID, data - output Qs bit-mask. 
				   SIZE - 512 * 16B = 8KB.
				   If needed - there is a more complicated way that decreases the
				   size to about 2.5KB
*/
static MEA_drv_mc_ehp_dbt *MEA_MC_EHP_Table = NULL;
static MEA_Editing_t *ed_id_arr = NULL;

static MEA_OutPorts_Entry_dbt* mea_drv_ehp_mc_grouping_tbl = NULL;
static MEA_Bool mc_grouping_tbl_in_use_Flag = MEA_FALSE;


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
static void mea_drv_Init_ProtocolMachine(MEA_Unit_t           unit);



static MEA_Status mea_drv_EHP_UpdateHw(MEA_Unit_t                          unit_i,
                                       MEA_Editing_t                       id_i);

MEA_Status mea_drv_Init_atm_ehp_tables              (MEA_Unit_t                      unit_i);

MEA_Status mea_drv_Init_mc_ehp_table                (MEA_Unit_t                      unit_i);

MEA_Status 	mea_drv_Init_eth_ehp_table              (MEA_Unit_t                      unit_i);


static MEA_Bool mea_drv_is_EHP_data_exist           (MEA_Unit_t                      unit_i,
													 MEA_EgressHeaderProc_Entry_dbt *entry_pi,
													 MEA_Editing_t                  *id_o);

static MEA_Status mea_drv_create_ehp_entry          (MEA_Unit_t                      unit_i,
													 MEA_EgressHeaderProc_Entry_dbt *entry_pi, 
													 MEA_Editing_t                  *id_io,
													 MEA_Bool                        force_i);

static MEA_Status mea_drv_ehp_delete_owner          (MEA_Unit_t                      unit_i,
												     MEA_Editing_t                   id_i);

static MEA_Status mea_drv_EHP_UpdateHw_profile_stamping  (MEA_Unit_t                        unit_i,
				 						 	              MEA_Editing_t                     id_i,
											              MEA_drv_ehp_profile_stamping_dbt *entry_pi);



static MEA_Status mea_drv_EHP_UpdateHw_profile_sa_mss    (MEA_Unit_t                          unit_i,
				 						 	              MEA_Editing_t                       id_i,
											              MEA_drv_ehp_profile_sa_mss_dbt      *entry_pi);

static MEA_Status mea_drv_EHP_UpdateHw_profile_ether_type(MEA_Unit_t                          unit_i,
				 						 	              MEA_Editing_t                       id_i,
											              MEA_drv_ehp_profile_ether_type_dbt *entry_pi);

static MEA_Status mea_drv_EHP_UpdateHw_fragmentSession  (MEA_Unit_t                          unit_i,
                                                         MEA_Editing_t                       id_i,
                                                         MEA_drv_ehp_fragmentSession_dbt         *entry_pi);
static MEA_Status mea_drv_EHP_UpdateHw_pw_control  (MEA_Unit_t                          unit_i,
                                                         MEA_Editing_t                       id_i,
                                                         MEA_drv_ehp_pw_control_dbt         *entry_pi);

static MEA_Status mea_drv_EHP_UpdateHw_mpls_label  (MEA_Unit_t                          unit_i,
                                                    MEA_Editing_t                       id_i,
                                                    MEA_drv_ehp_mpls_label_dbt         *entry_pi);



static MEA_Status mea_drv_EHP_UpdateHw_profile2_Lm(MEA_Unit_t                  unit_i,
                                                   MEA_LmId_t               id_i,
                                                   MEA_Uint32 value);
/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


/******************************************************************************/
/*						MC EHP Implementation								  */
/*												  		 				      */
/******************************************************************************/
		

static MEA_Bool mea_drv_IsMC_EHPIdInRange(MEA_Uint32 index)
{

   if(index<(MEA_Uint32)MEA_MAX_NUM_OF_MC_EHP)
   	return MEA_TRUE;
   else
    return MEA_FALSE;		

}

static MEA_Bool mea_drv_IsMC_IdInRange(MEA_Uint32 index)
{

   if(index<MEA_MAX_NUM_OF_MC_ID)
   	return MEA_TRUE;
   else
    return MEA_FALSE;		

}
MEA_Uint32 mea_drv_ehp_mc_get_num_of_queues(MEA_OutPorts_Entry_dbt *output_info)
{
	MEA_Uint32 j, mask, count = 0;
	MEA_Uint32* ptr;

		for (j = 0, ptr = (MEA_Uint32*)(output_info);
             j < (sizeof(*output_info)/sizeof(*ptr));
             j++ , ptr++)
		{

            for (mask = 0x00000001 ; (mask & 0xffffffff) != 0 ; mask <<= 1)
			{
                if (*ptr & mask) 
				{
					count++;
				}
			}
		}

		return count;
}


static MEA_Status mea_drv_mc_ehp_unlock_group_db(void)
{
	if ( !mc_grouping_tbl_in_use_Flag )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - EHP MC Grouping table was not in use\n",
						   __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memset(mea_drv_ehp_mc_grouping_tbl,0,MEA_MAX_NUM_OF_EDIT_ID * sizeof(MEA_OutPorts_Entry_dbt));
	mc_grouping_tbl_in_use_Flag = MEA_FALSE;

	return MEA_OK;
}


static MEA_Status mea_drv_mc_ehp_lock_group_ehp_info(MEA_Uint16               mc_id,
									     			 MEA_OutPorts_Entry_dbt *output_info,
													 MEA_Uint32             *num_of_entries)
{
    MEA_Uint32* ptr;
    MEA_Uint32  mask, i, index;
	MEA_Port_t  outport; 
	MEA_Editing_t ed_id;
	MEA_Uint32 *pPortGrp;

	if ( mc_grouping_tbl_in_use_Flag )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - EHP MC Grouping table already in use\n",
						   __FUNCTION__);
		return MEA_ERROR;
	}

	mc_grouping_tbl_in_use_Flag = MEA_TRUE;
   
	MEA_OS_memset(mea_drv_ehp_mc_grouping_tbl,0,MEA_MAX_NUM_OF_EDIT_ID * sizeof(MEA_OutPorts_Entry_dbt));

	*num_of_entries = 0;
	/* for each output Q + MC ID:
			- retrieve the ED ID. 
			- in the ED_ID index, set the output Q bit in the bit-mask.
	*/
	for (i = 0 , ptr = (MEA_Uint32*)output_info , outport = 0;
		 i < (sizeof(*output_info)/sizeof(*ptr));
		 i++ , ptr++) 
	{
		for (mask = 0x00000001 ; (mask & 0xffffffff) != 0 ; mask <<= 1,outport++)
		{
			if (*ptr & mask) 
			{
				index = enet_cluster_external2internal(outport)*MEA_MAX_NUM_OF_MC_ID + mc_id;

				if ( !mea_drv_IsMC_EHPIdInRange(index) )
				{
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - invalid EHP MC index %d\n",
									  __FUNCTION__,index);
					mc_grouping_tbl_in_use_Flag = MEA_FALSE;
					return MEA_ERROR; 
				}

				if ( !MEA_MC_EHP_Table[index].valid )
				{
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - MC EHP entry %d does not exist\n",
									  __FUNCTION__,index);
					mc_grouping_tbl_in_use_Flag = MEA_FALSE;
					return MEA_ERROR; 
				}

				ed_id = (MEA_Editing_t)MEA_MC_EHP_Table[index].editing_info.mc_editing_id;
				pPortGrp=(MEA_Uint32 *)(&(mea_drv_ehp_mc_grouping_tbl[ed_id].out_ports_0_31));
				pPortGrp +=(outport/32);
				if (*pPortGrp & (0x00000001 << (outport%32)) )
				{
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - Output %d already marked on MC grouping index %d\n",
									  __FUNCTION__,outport,ed_id);
					mc_grouping_tbl_in_use_Flag = MEA_FALSE;
					return MEA_ERROR; 
				}
				*pPortGrp |= 0x00000001 << (outport%32);
			}
		}
	}

	for ( i = 0; i < MEA_MAX_NUM_OF_EDIT_ID; i++ )
	{
		if ( mea_drv_ehp_mc_get_num_of_queues( &(mea_drv_ehp_mc_grouping_tbl[i]) ) != 0 )
			(*num_of_entries)++;
	}

	return MEA_OK;
}


static void mea_drv_ehp_mc_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
										 
{
	MEA_Unit_t          unit_i    = (MEA_Unit_t         )arg1;
    MEA_db_HwUnit_t     hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t       hwId_i = (MEA_db_HwId_t)((long)arg3);
	MEA_drv_mc_ehp_dbt *entry_pi  = (MEA_drv_mc_ehp_dbt*)arg4;
	MEA_Uint32      count_shift;
    MEA_db_HwId_t   editing_hwId;
    MEA_Uint32 val[1];

   val[0]  = 0;

  if ( entry_pi->valid )
   {

	    /* Get the hwUnit and hwId */
	    MEA_DRV_GET_HW_ID(unit_i,
			              mea_drv_Get_EHP_ETH_db,
						  entry_pi->editing_info.mc_editing_id,
	  				      hwUnit_i,
					      editing_hwId,
 				         return;);

    count_shift=0;
    MEA_OS_insert_value(count_shift,
                        MEA_MC_EHP_DWORD_MASK_ED_ID_WIDTH(unit_i,hwUnit_i),
                        (MEA_Uint32)editing_hwId,
                        &val[0]);
	count_shift+=MEA_MC_EHP_DWORD_MASK_ED_ID_WIDTH(unit_i,hwUnit_i);
#if 0
    MEA_OS_insert_value(count_shift,
                        MEA_MC_EHP_DWORD_MASK_PROTO_WIDTH,
                        (MEA_Uint32)entry_pi->editing_info.mc_proto_type,
                        &val[0]);
	count_shift+=MEA_MC_EHP_DWORD_MASK_PROTO_WIDTH;
    MEA_OS_insert_value(count_shift,
                        MEA_MC_EHP_DWORD_MASK_LLC_WIDTH,
                        (MEA_Uint32)entry_pi->editing_info.mc_llc,
                        &val[0]);
	count_shift+=MEA_MC_EHP_DWORD_MASK_LLC_WIDTH;
#endif



   }
   // DBG 
   //hwId_i = hwId_i;

   /* write to hardware */
   MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,val[0],MEA_MODULE_BM);

}


static MEA_Status mea_drv_ehp_mc_UpdateHw( MEA_Unit_t unit_i,
										   MEA_Uint32 index,
                                           MEA_drv_mc_ehp_dbt *entry )
{
	MEA_ind_write_t ind_write;
	MEA_db_HwUnit_t hwUnit;


	if ( !mea_drv_IsMC_EHPIdInRange(index) )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - invalid EHP MC index %d\n",
						  __FUNCTION__,index);
		return MEA_ERROR; 
	}

	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);

    /* set the ind_write parameters */
	ind_write.tableType		= ENET_BM_TBL_TYP_EHP_MULTICAST;
	ind_write.tableOffset	= index;
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)0/*hwId*/;
    ind_write.funcParam4 = (MEA_funcParam)entry;
	ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_ehp_mc_UpdateHwEntry;

    /* Write To HW Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
		return MEA_ERROR;
    }

    /* return to caller */
	return MEA_OK;

}

MEA_Status mea_drv_mc_ehp_Get_Info(MEA_Unit_t unit_i,
                                    MEA_Uint32 index,
                                    MEA_drv_mc_ehp_dbt *data)
{

    if (!mea_drv_IsMC_EHPIdInRange(index))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - invalid EHP MC index %d\n",
            __FUNCTION__, index);
        return MEA_ERROR;
    }

    
    if (MEA_MC_EHP_Table[index].valid == MEA_FALSE){
    
        return MEA_ERROR;
    }

    
    MEA_OS_memcpy(data, &MEA_MC_EHP_Table[index], sizeof(MEA_drv_mc_ehp_dbt));
    return MEA_OK;
}

MEA_Status mea_drv_mc_ehp_Update_HwInfo(MEA_Unit_t unit_i,
                                        MEA_Uint32 index,
                                        MEA_drv_mc_ehp_dbt *data)
{

    if (!mea_drv_IsMC_EHPIdInRange(index))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - invalid EHP MC index %d\n",
            __FUNCTION__, index);
        return MEA_ERROR;
    }
    MEA_OS_memcpy(&MEA_MC_EHP_Table[index], data, sizeof(MEA_drv_mc_ehp_dbt));

    if (mea_drv_ehp_mc_UpdateHw(unit_i, index, &(MEA_MC_EHP_Table[index])) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ehp_mc_UpdateHw failed (index %d)\n",
            __FUNCTION__, index);
        return MEA_ERROR;
    }
    return MEA_OK;
}

MEA_Status mea_drv_mc_ehp_set_owner(MEA_Unit_t unit_i,
									MEA_Uint32 index, 
                                    MEA_Editing_t ed_id,
                                    MEA_Uint32 protocol,
                                    MEA_Bool   llc)
{
	if ( !mea_drv_IsMC_EHPIdInRange(index) )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - invalid EHP MC index %d\n",
						  __FUNCTION__,index);
		return MEA_ERROR; 
	}

	if ( MEA_MC_EHP_Table[index].valid == MEA_FALSE )
	{
		MEA_MC_EHP_Table[index].valid = MEA_TRUE;
		MEA_MC_EHP_Table[index].editing_info.mc_editing_id = ed_id;
		MEA_MC_EHP_Table[index].editing_info.num_of_owners = 1;
		MEA_MC_EHP_Table[index].editing_info.mc_llc = llc;
		MEA_MC_EHP_Table[index].editing_info.mc_proto_type = protocol;

		if ( mea_drv_ehp_mc_UpdateHw(unit_i,index, &(MEA_MC_EHP_Table[index])) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_ehp_mc_UpdateHw failed (index %d)\n",
							  __FUNCTION__,index);
			return MEA_ERROR; 
		}
	}
	else
	{
		if ((MEA_Editing_t)MEA_MC_EHP_Table[index].editing_info.mc_editing_id != ed_id ||
			(MEA_Bool)MEA_MC_EHP_Table[index].editing_info.mc_llc != llc ||
			MEA_MC_EHP_Table[index].editing_info.mc_proto_type != protocol)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - non matching existing EHP MC entry %d when inserting Editing ID %d: Editing ID %d, LLC %d, Protocol %d\n",
							  __FUNCTION__,index, ed_id, MEA_MC_EHP_Table[index].editing_info.mc_editing_id,
							  MEA_MC_EHP_Table[index].editing_info.mc_llc, MEA_MC_EHP_Table[index].editing_info.mc_proto_type);
			return MEA_ERROR; 
		}
		
		MEA_MC_EHP_Table[index].editing_info.num_of_owners++;
	}						
	
	return MEA_OK;
}


MEA_Status mea_drv_mc_ehp_remove_owner(MEA_Unit_t unit_i,
									   MEA_Uint32 index)
{
	if ( !mea_drv_IsMC_EHPIdInRange(index) )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - invalid EHP MC index %d\n",
						  __FUNCTION__,index);
		return MEA_ERROR; 
	}

	if ( MEA_MC_EHP_Table[index].valid == MEA_FALSE )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - EHP MC index %d foes not exist\n",
						  __FUNCTION__,index);
		return MEA_ERROR; 
	}
		
	MEA_MC_EHP_Table[index].editing_info.num_of_owners--;
	if ( MEA_MC_EHP_Table[index].editing_info.num_of_owners == 0 )
	{
		MEA_MC_EHP_Table[index].valid = MEA_FALSE;
		if ( mea_drv_ehp_mc_UpdateHw(unit_i,index, &(MEA_MC_EHP_Table[index])) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_ehp_mc_UpdateHw failed (index %d)\n",
							  __FUNCTION__,index);
			return MEA_ERROR; 
		}
	}
						
	return MEA_OK;
}

MEA_Status mea_drv_ehp_delete(MEA_Unit_t              unit_i,
							  mea_drv_ehp_id_dbt      *ehp_id,
							  MEA_OutPorts_Entry_dbt *output_info)
{
    MEA_Uint32* ptr;
    MEA_Uint32  mask, index, i, num_of_entries;
	MEA_Port_t  outport; 

	if ( ehp_id == NULL )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - NULL input ehp_id\n",
						   __FUNCTION__);
		return MEA_ERROR;
	}


	if ( !(ehp_id->is_mc) )
	{
		if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
												   (MEA_Editing_t)ehp_id->id.uni_id.id) != MEA_OK )
		{
		  MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
							 "%s - MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
							 __FUNCTION__,ehp_id->id.uni_id.id);
		  return MEA_ERROR; 
		}
	}
	else
	{
		if ( mea_drv_IsMC_IdInRange((MEA_Uint32)ehp_id->id.mc_id.id) != MEA_TRUE )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - invalid input MC ID\n",
							   __FUNCTION__, ehp_id->id.mc_id.id);
			return MEA_ERROR;
		}

		if ( output_info == NULL )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - NULL input output_info\n",
							   __FUNCTION__);
			return MEA_ERROR;
		}

		/* Algorithm: 
					1) Fill the Grouping Table ( group Queues with similar ED_ID ).
					2) Remove owner on all Queues+MC_ID MC entries.
					3) Scan the grouping table entries and on each ED_ID - Delete EHP.
		*/

	/*
		1)
	*/
		if ( mea_drv_mc_ehp_lock_group_ehp_info( (MEA_Uint8)(ehp_id->id.mc_id.id), output_info,&num_of_entries ) != MEA_OK )
		{	
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_mc_ehp_group_ehp_info failed\n",
							   __FUNCTION__);
			return MEA_ERROR;
		}

	/*
		2)
	*/
		for (i=0 , ptr = (MEA_Uint32*)output_info , outport = 0;
			 i<(sizeof(*output_info)/sizeof(*ptr));
			 i++ , ptr++) 
		{
			for (mask = 0x00000001 ; (mask & 0xffffffff) != 0 ; mask <<= 1,outport++)
			{
				if (*ptr & mask) 
				{
					index = enet_cluster_external2internal(outport)*MEA_MAX_NUM_OF_MC_ID + ehp_id->id.mc_id.id;

					if ( mea_drv_mc_ehp_remove_owner(unit_i,index) != MEA_OK )
					{
						MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
										  "%s - mea_drv_mc_ehp_set_owner failed\n",
										  __FUNCTION__);
						return MEA_ERROR; 
					}
				}
			}
		}




	/*
		3)
	*/
		for ( i = 0; i < MEA_MAX_NUM_OF_EDIT_ID; i++ )
		{
			if ( mea_drv_ehp_mc_get_num_of_queues( &(mea_drv_ehp_mc_grouping_tbl[i]) ) != 0 )
			{
				if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i, (MEA_Editing_t)i) != MEA_OK )
				{
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - MEA_API_Delete_EgressHeaderProc_Entry failed, ED ID %d\n",
									   __FUNCTION__, i);
					return MEA_ERROR;
				}	
			}
		}

		/* finished using the Grouping Table - unlock it */
		mea_drv_mc_ehp_unlock_group_db();
	}

	return MEA_OK;
}


MEA_Status mea_drv_ehp_info_check(MEA_EgressHeaderProc_Array_Entry_dbt *EHP_array_i, 
								  MEA_OutPorts_Entry_dbt               *OutPorts_Entry)
{
	MEA_OutPorts_Entry_dbt         TMP_OutPorts;
    MEA_Uint32                     i, j;
    MEA_Uint32* ptr1,*ptr3;
    MEA_Uint32  mask;
    MEA_Port_t  outport;


	if (EHP_array_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - EHP_array_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (EHP_array_i->num_of_entries > 0 && EHP_array_i->ehp_info == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - EHP_array_i=(%d,NULL)\n",
						  __FUNCTION__,EHP_array_i->num_of_entries);
		return MEA_ERROR;
	}

    MEA_OS_memset(&TMP_OutPorts, 0, sizeof(TMP_OutPorts));

    /* Check the EHP mapping is valid */
	for ( i = 0; i < EHP_array_i->num_of_entries; i++ )
    {
		for (j = 0, 
			 ptr1 = (MEA_Uint32*)&(EHP_array_i->ehp_info[i].output_info), 
			 ptr3 = (MEA_Uint32*)&(TMP_OutPorts),
			 outport = 0
			 ;
             j < (sizeof(EHP_array_i->ehp_info[i].output_info)/sizeof(*ptr1)) &&
		     j < (sizeof(TMP_OutPorts)/sizeof(*ptr3))
			 ;
             j++ , ptr1++/*, ptr2++*/, ptr3++)
		{
            for (mask = 0x00000001 ; (mask & 0xffffffff) != 0 ; mask <<= 1,outport++)
			{
                if ((*ptr1 & mask) & 0xffffffff) 
				{

					/* Check that it was not already counted */
					if ( (*ptr3 & mask) )
					{
						MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
										  "%s - EHP Q in Entry %d/section %d/place %x appeared more than once\n",
										   __FUNCTION__,i, j, mask);
						return MEA_ERROR;
					}

					*ptr3 |= mask;

					/* check Q validity */
					if (ENET_IsValid_Queue(ENET_UNIT_0,outport,ENET_FALSE)==ENET_FALSE) 
					{ 
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s - out port %d is invalid\n",
                                          __FUNCTION__,outport);
                        return MEA_ERROR; 
                    }
				}
			}
			 }
	}


	return MEA_OK;
}





MEA_Status mea_drv_ehp_mc_is_MC(MEA_EgressHeaderProc_Array_Entry_dbt *ehp_array_i,
								MEA_Bool                             *is_mc_ehp_o,
								MEA_EgressHeaderProc_Entry_dbt       *unicast_ehp_info_o)
{
	MEA_Uint32 i;
	*is_mc_ehp_o = MEA_FALSE;

	MEA_OS_memcpy(unicast_ehp_info_o, &(ehp_array_i->ehp_info[0].ehp_data), sizeof(*unicast_ehp_info_o));

	/* cases when is it not MC:
			- when only 1 entry.
			- when all entries have the same EHP info.
	*/ 

	if( ehp_array_i->num_of_entries == 1 )
	{		
		return MEA_OK;
	}

	for ( i = 1; i < ehp_array_i->num_of_entries; i++ )
	{
		if ( MEA_OS_memcmp(unicast_ehp_info_o, &(ehp_array_i->ehp_info[i].ehp_data), sizeof(*unicast_ehp_info_o)) != 0 )
		{
			*is_mc_ehp_o = MEA_TRUE;
		}
	}

	return MEA_OK;
}

MEA_Bool mea_drv_ehp_mc_is_slot_available(MEA_Uint32 outport, 
           								  MEA_Uint8 slot,
										  MEA_Editing_t ed_id) 
{
	/* Available either if it's empty, or if it contains identical ED ID */

	ENET_QueueId_t index;

	index = enet_cluster_external2internal((ENET_QueueId_t)outport)*MEA_MAX_NUM_OF_MC_ID + slot;

	if ( !mea_drv_IsMC_EHPIdInRange(index) )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - invalid EHP MC index %d\n",
						  __FUNCTION__,index);
		return MEA_ERROR; 
	}

	if ( MEA_MC_EHP_Table[index].valid == MEA_FALSE ||
		 MEA_MC_EHP_Table[index].editing_info.mc_editing_id == ed_id )
	{
		return MEA_TRUE;
	}
	else
		return MEA_FALSE;
}



MEA_Status mea_drv_mc_ehp_release_all_ehp(MEA_Unit_t unit_i,
										  MEA_EgressHeaderProc_Array_Entry_dbt* EHP_Entry_i)
{
	MEA_Uint32 i;
	MEA_Editing_t ed_id;

	for ( i=0; i<EHP_Entry_i->num_of_entries && i < MEA_MAX_NUM_OF_MC_ELEMENTS; i++ )
	{

		if ( mea_drv_is_EHP_data_exist(unit_i,
			                           &(EHP_Entry_i->ehp_info[i].ehp_data), &ed_id) )
		{
			if ( mea_drv_ehp_delete_owner(unit_i,ed_id) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				  "%s - mea_drv_ehp_delete_owner failed, ED ID %d\n",
				  __FUNCTION__, ed_id);
				return MEA_ERROR; 
			}
		}
	}

	return MEA_OK;
}




MEA_Status mea_drv_ehp_set(MEA_Unit_t                            unit_i,
						   MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry_i,
						   mea_drv_ehp_id_dbt                   *ehp_id_io)
{
	MEA_Uint32 i,j, index;
	//MEA_Editing_t ed_id_arr[MEA_MAX_NUM_OF_MC_ELEMENTS];
	MEA_Uint8     slot;
	MEA_Editing_t ed_id;
    MEA_Uint32* ptr;
    MEA_Uint32  mask;
	MEA_Port_t  outport; 
	MEA_Bool    no_match, found_slot;
	MEA_EgressHeaderProc_Entry_dbt  unicast_ehp_info;

    MEA_OS_memset(ed_id_arr, 0, sizeof(*ed_id_arr));
	MEA_OS_memset(&unicast_ehp_info,0,sizeof(unicast_ehp_info));

	if ( ehp_id_io == NULL )
	{
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - NULL input ehp_id_io\n",
						  __FUNCTION__);
	   return MEA_ERROR; 
	}


	if ( EHP_Entry_i == NULL &&
		 ehp_id_io->id.uni_id.id == 0)
	{
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - NULL input EHP_Entry_i with 0 EDitId\n",
						  __FUNCTION__);
	   return MEA_ERROR; 
	}


	/* handle input ed ID != 0 ==> connect to existing one */
	if ( ehp_id_io->id.uni_id.id != 0 )
	{
		if (MEA_API_Set_EgressHeaderProc_Entry(unit_i,
											   NULL,
						 					   &(ehp_id_io->id.uni_id.id),MEA_FALSE) != MEA_OK )
		{
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - MEA_API_Set_EgressHeaderProc_Entry failed\n",
							  __FUNCTION__);
		   return MEA_ERROR; 
		}
		ehp_id_io->is_mc = MEA_FALSE;

		return MEA_OK;
	}
	
    if ( mea_drv_ehp_mc_is_MC(EHP_Entry_i, &(ehp_id_io->is_mc), &unicast_ehp_info) != MEA_OK )
	{
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - mea_drv_ehp_mc_is_MC failed\n",
						  __FUNCTION__);
	   return MEA_ERROR; 
	}

	/* Handle unicast EHP */
	if ( !(ehp_id_io->is_mc) )
	{
		if (MEA_API_Set_EgressHeaderProc_Entry(unit_i,
											   &unicast_ehp_info,
						 					   &(ehp_id_io->id.uni_id.id),MEA_FALSE) != MEA_OK )
		{
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - MEA_API_Set_EgressHeaderProc_Entry failed\n",
							  __FUNCTION__);
		   return MEA_ERROR; 
		}

		return MEA_OK;
	}


	/* Handle MC EHP */

	/* For each entry, get the EHP's edID (either by creating or registering as owner).
	   For each port in the entry, see if in the X slot space there is similar EDId or if there is empty space.
	   If all ports in the array are fixed - the slot space is the mcId */


	/* get EDId */
	for ( i=0; i<EHP_Entry_i->num_of_entries && i < MEA_MAX_NUM_OF_MC_ELEMENTS; i++ )
	{

		if ( mea_drv_is_EHP_data_exist(unit_i,&(EHP_Entry_i->ehp_info[i].ehp_data), &ed_id) )
		{
			if ( mea_drv_ehp_add_owner(ed_id) != MEA_OK )
			{
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_ehp_add_owner failed, index %d\n",
								  __FUNCTION__, ed_id);
				return MEA_ERROR; 
			}
		}
		else
		{
			if ( mea_drv_create_ehp_entry(unit_i,&(EHP_Entry_i->ehp_info[i].ehp_data), &ed_id,MEA_FALSE) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_create_ehp_entry failed\n",
								  __FUNCTION__);
				return MEA_ERROR; 
			}
		}
		ed_id_arr[i] = ed_id;
	}

	/* find available slot */
	found_slot = MEA_FALSE;

	for ( slot = 0; slot < MEA_MAX_NUM_OF_MC_ID; slot++ )
	{
		no_match = MEA_FALSE;

		for ( i = 0; i<EHP_Entry_i->num_of_entries && i < MEA_MAX_NUM_OF_MC_ELEMENTS && no_match != MEA_TRUE; i++ )
		{
			for (j=0 , ptr = (MEA_Uint32*)&(EHP_Entry_i->ehp_info[i].output_info) , outport = 0;
				 j<(sizeof(EHP_Entry_i->ehp_info[i].output_info)/sizeof(*ptr)) && no_match != MEA_TRUE;
				 j++ , ptr++) 
			{
				for (mask = 0x00000001 ; (mask & 0xffffffff) != 0 ; mask <<= 1,outport++)
				{
					if (*ptr & mask) 
					{
						/* Check the slot space in Queue section */
						if ( !mea_drv_ehp_mc_is_slot_available(outport, slot, ed_id_arr[i]) )
						{
							no_match = MEA_TRUE;
							break;
						}
					}
				}
			}
		}

		if ( no_match == MEA_FALSE )
		{
			found_slot = MEA_TRUE;
			break;
		}
	}

	if( !found_slot )
	{
		if ( mea_drv_mc_ehp_release_all_ehp(unit_i,EHP_Entry_i) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_mc_ehp_release failed\n",
							  __FUNCTION__);
			return MEA_ERROR; 
		}

		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - no free MC ID\n",
						  __FUNCTION__);
		return MEA_ERROR; 
	}


	/* set all data into slots */
	for ( i = 0; i<EHP_Entry_i->num_of_entries && i < MEA_MAX_NUM_OF_MC_ELEMENTS; i++ )
	{
		for (j=0 , ptr = (MEA_Uint32*)&(EHP_Entry_i->ehp_info[i].output_info) , outport = 0;
			 j<(sizeof(EHP_Entry_i->ehp_info[i].output_info)/sizeof(*ptr));
			 j++ , ptr++) 
		{
			for (mask = 0x00000001 ; (mask & 0xffffffff) != 0 ; mask <<= 1,outport++)
			{
				if (*ptr & mask) 
				{
                    MEA_Uint32 protocol=1;
                    MEA_Bool   llc=0;
                    MEA_OutPorts_Entry_dbt outport_mask;
                    MEA_OS_memset((char*)&outport_mask,0,sizeof(outport_mask));
                    ((MEA_Uint32*)(&(outport_mask.out_ports_0_31)))
                          [outport/32] |= (1 << (outport%32));

					index = enet_cluster_external2internal(outport)*MEA_MAX_NUM_OF_MC_ID + slot;
                    
                    if(EHP_Entry_i->ehp_info[i].ehp_data.Attribute_info.valid == MEA_TRUE)
                    {
                        protocol= EHP_Entry_i->ehp_info[i].ehp_data.Attribute_info.protocol;
                        llc= EHP_Entry_i->ehp_info[i].ehp_data.Attribute_info.llc;
                    }
                    if (EHP_Entry_i->ehp_info[i].ehp_data.EditingType != 0)
                    {
                        protocol  = MEA_EHP_ProtocolMachine[EHP_Entry_i->ehp_info[i].ehp_data.EditingType].val.protocol;
                        llc       = MEA_EHP_ProtocolMachine[EHP_Entry_i->ehp_info[i].ehp_data.EditingType].val.llc;
                        
                    }
                    

					if ( mea_drv_mc_ehp_set_owner(unit_i,
						                          index, 
                                                  ed_id_arr[i],
                                                  protocol,
                                                  llc) != MEA_OK )
					{
						MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
										  "%s - mea_drv_mc_ehp_set_owner failed\n",
										  __FUNCTION__);
						return MEA_ERROR; 
					}
				}
			}
		}
	}

	
	ehp_id_io->id.mc_id.id = slot;

	return MEA_OK;

}
MEA_Status mea_drv_ehp_mc_get_info( MEA_Unit_t                            unit_i,
									MEA_Uint16                             mc_id_i,
								    MEA_OutPorts_Entry_dbt               *OutPorts_Entry_i,
								    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry_o )
{
	MEA_Uint32 i, j, num_of_entries;
	MEA_Uint32 ehp_arr_count ;

	if ( OutPorts_Entry_i == NULL )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - NULL OutPorts_Entry_i input\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if ( EHP_Entry_o == NULL )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - NULL EHP_Entry_o input\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	ehp_arr_count = EHP_Entry_o->num_of_entries;

	if ( mea_drv_IsMC_IdInRange((MEA_Uint32)mc_id_i) != MEA_TRUE )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - invalid input MC ID\n",
						   __FUNCTION__, mc_id_i);
		return MEA_ERROR;
	}

	if ( ehp_arr_count == 1 )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - given only 1 EHP entry with MC ID\n",
						   __FUNCTION__);
		return MEA_ERROR;
	}

	/* Algorithm:
		- Group the ED IDs using the global MC EHP Grouping Table.
		- For each valid entry on the grouping table:
			- Get the EHP Info using the ED_ID index.
			- put the EHP info + the output bit-mask in the user's array.
	*/

	if ( mea_drv_mc_ehp_lock_group_ehp_info( mc_id_i, OutPorts_Entry_i,&num_of_entries ) != MEA_OK )
	{	
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - mea_drv_mc_ehp_group_ehp_info failed\n",
						   __FUNCTION__);
		return MEA_ERROR;
	}

	/* If the user did not allocate any space - just return the number of entries */
	if ( EHP_Entry_o->ehp_info == NULL )
	{	
		EHP_Entry_o->num_of_entries = num_of_entries;
		mea_drv_mc_ehp_unlock_group_db();
		return MEA_OK;
	}

	if ( num_of_entries != ehp_arr_count )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - invalid number of given entries. Given-%d, Needed-%d\n",
						   __FUNCTION__, ehp_arr_count, num_of_entries);
		mea_drv_mc_ehp_unlock_group_db();
		return MEA_ERROR;
	}

	for ( i = 0, j=0; i < MEA_MAX_NUM_OF_EDIT_ID && j < ehp_arr_count; i++)
	{
		if ( mea_drv_ehp_mc_get_num_of_queues( &(mea_drv_ehp_mc_grouping_tbl[i]) ) != 0 )
		{
			if ( MEA_API_Get_EgressHeaderProc_Entry(unit_i, 
				                                   (MEA_Editing_t)i,
												   &(EHP_Entry_o->ehp_info[j].ehp_data) ) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - MEA_API_Delete_EgressHeaderProc_Entry failed, ED ID %d\n",
								   __FUNCTION__, i);
				mea_drv_mc_ehp_unlock_group_db();
				return MEA_ERROR;
			}	

			MEA_OS_memcpy(&(EHP_Entry_o->ehp_info[j].output_info),
						  &(mea_drv_ehp_mc_grouping_tbl[i]),
						  sizeof(EHP_Entry_o->ehp_info[j].output_info));
			j++;
		}
	}

	/* finished using the Grouping Table - unlock it */
	mea_drv_mc_ehp_unlock_group_db();

	return MEA_OK;
}
	

MEA_Status mea_drv_debug_mc_ehp_show_all(MEA_Uint32 index,MEA_Bool *found, 
                                         MEA_drv_mc_ehp_dbt *entry_o)
{
	
    
    if(entry_o == NULL){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - entry_o == NULL\n",
								   __FUNCTION__);
				return MEA_ERROR;
    }
	if(found == NULL){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - found == NULL\n",
								   __FUNCTION__);
				return MEA_ERROR;
    }
	
    *found=MEA_FALSE;
    if(index >= (MEA_Uint32)MEA_MAX_NUM_OF_MC_EHP){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - index > MEA_MAX_NUM_OF_MC_EHP\n",
								   __FUNCTION__);
				return MEA_ERROR;
    }
	*found=MEA_FALSE;

	if ( MEA_MC_EHP_Table[index].valid )
	{
        *found=MEA_TRUE;
		MEA_OS_memcpy(entry_o,&MEA_MC_EHP_Table[index],sizeof(*entry_o));
           
	}
	
    return MEA_OK;
}

/************************************************************************************/
/************************************************************************************/
/************************************************************************************/



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                    <mea_drv_IsEditingIdInRange>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool mea_drv_IsEditingIdInRange(MEA_Editing_t editId)
{

   if(editId<MEA_MAX_NUM_OF_EDIT_ID)
   	return MEA_TRUE;
   else
    return MEA_FALSE;		

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                    <mea_drv_is_EHP_id_exist>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool mea_drv_is_EHP_id_exist(MEA_Unit_t unit_i,
								 MEA_Editing_t id_i)
{

	if ( !mea_drv_IsEditingIdInRange(id_i) )
		return MEA_FALSE;

	if (MEA_EHP_Table[id_i].valid ) {

		MEA_db_HwUnit_t hwUnit;

		MEA_DRV_GET_HW_UNIT(unit_i,
			                hwUnit,
							return MEA_FALSE);
		if (MEA_EHP_Table[id_i].hwUnit != hwUnit) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - You can not share editId between upstream and downstream (edit_id=%d)\n",
				              __FUNCTION__,id_i);
			return MEA_FALSE;
		}

		return MEA_TRUE;
	} else {
		return MEA_FALSE;
	}
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                    <mea_drv_ehp_get_info_db>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ehp_get_info_db(MEA_Unit_t        unit_i,
                                   MEA_Editing_t     ed_id_i,
                                   MEA_drv_ehp_dbt  *entry_o)
{

    if(entry_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_o == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_is_EHP_id_exist(unit_i,ed_id_i) != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - edit id %d not exit\n",
                          __FUNCTION__,ed_id_i);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry_o,
                  &(MEA_EHP_Table[ed_id_i]),
                  sizeof(*entry_o));

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                    <mea_drv_is_EHP_data_exist>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Bool mea_drv_is_EHP_data_exist(MEA_Unit_t                      unit_i,
										  MEA_EgressHeaderProc_Entry_dbt *entry_i,
										  MEA_Editing_t                  * id_o)
{
	MEA_Editing_t index;

	MEA_db_HwUnit_t hwUnit;

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_FALSE);

	for (index = 0; index < MEA_MAX_NUM_OF_EDIT_ID; index++)
	{ 
		if ((MEA_EHP_Table[index].valid           ) &&
		    (MEA_EHP_Table[index].hwUnit == hwUnit)  )
		{
			if ( MEA_OS_memcmp(entry_i, 
				               &(MEA_EHP_Table[index].editing_info), 
				               sizeof(*entry_i)) == 0 )
			{
				*id_o = index;
				return MEA_TRUE;
			}
		}
	}

	return MEA_FALSE;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_profile_stamping_id>                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_profile_stamping_id(MEA_Unit_t                      unit_i,
														   MEA_EgressHeaderProc_Entry_dbt *entry_pi,
	                                                       MEA_Editing_t                  *id_o)
{
	MEA_Editing_t i;
	MEA_Bool found;
	MEA_db_HwUnit_t hwUnit;

	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);

	found=MEA_FALSE;
	for (i=0;(!found)&&(i<MEA_MAX_NUM_OF_EHP_PROFILE_STAMPING);i++) {
		if ( MEA_EHP_profile_stamping_Table[i].valid ) {
			if  ((MEA_OS_memcmp((char*)(&(MEA_EHP_profile_stamping_Table[i].data)),
				                (char*)(&(entry_pi->eth_stamping_info           )),
							     sizeof(MEA_EHP_profile_stamping_Table[0].data   )) == 0   ) && 
				 (MEA_EHP_profile_stamping_Table[i].hwUnit                        == hwUnit)  ) {
			    MEA_EHP_profile_stamping_Table[i].num_of_owners++;
				found = MEA_TRUE;
				*id_o = i;
			}
		}
	}


	for (i=0;(!found)&&(i<MEA_MAX_NUM_OF_EHP_PROFILE_STAMPING);i++) {
		if (!MEA_EHP_profile_stamping_Table[i].valid ) {
			MEA_db_HwId_t hwId;
            hwId = i; //MEA_DB_GENERATE_NEW_ID;
			if (mea_db_Create(unit_i,
				              MEA_EHP_profile_stamping_db,
							  (MEA_db_SwId_t)i,
							  hwUnit,
							  &hwId) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - mea_db_Create failed (EHP_profile_stamping) (swId=%d,hwUnit=%d) \n",
								  __FUNCTION__,*id_o,hwUnit);
				return MEA_ERROR;
			}
			MEA_OS_memset(&(MEA_EHP_profile_stamping_Table[i]),
				          0,
						  sizeof(MEA_EHP_profile_stamping_Table[0]));
			MEA_OS_memcpy((char*)(&(MEA_EHP_profile_stamping_Table[i].data)),
				          (char*)(&(entry_pi->eth_stamping_info           )),
					      sizeof(MEA_EHP_profile_stamping_Table[0].data)    );
			MEA_EHP_profile_stamping_Table[i].valid         = MEA_TRUE;
			MEA_EHP_profile_stamping_Table[i].hwUnit        = hwUnit;
			MEA_EHP_profile_stamping_Table[i].num_of_owners = 1;
			*id_o = i;
			found = MEA_TRUE;

		    if (mea_drv_EHP_UpdateHw_profile_stamping(unit_i,
                                                      *id_o,
								                      &(MEA_EHP_profile_stamping_Table[*id_o])) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - mea_drv_EHP_UpdateHw_profile_stamping failed (id=%d) \n",
								  __FUNCTION__,
								  *id_o);
			    mea_db_Delete(unit_i,MEA_EHP_profile_stamping_db,(MEA_db_SwId_t)(*id_o),hwUnit);
				MEA_OS_memset(&(MEA_EHP_profile_stamping_Table[*id_o]),
					          0,
							  sizeof(MEA_EHP_profile_stamping_Table[0]));
				return MEA_ERROR;
			}

		}
	}

	if (!found) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - EHP Profile Stamping Type table is full\n",
                          __FUNCTION__);
		return MEA_ERROR;
    } 

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Delete_EHP_profile_stamping_id>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Delete_EHP_profile_stamping_id(MEA_Unit_t     unit_i,
                                                       MEA_Editing_t      id_i)
{
	MEA_db_HwUnit_t hwUnit;

	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE_STAMPING) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE_STAMPING);
		return MEA_ERROR;
	}

	if ( ! MEA_EHP_profile_stamping_Table[id_i].valid ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile_stamping_Table[%d].valid == MEA_FALSE \n",
			             __FUNCTION__,id_i);
		return MEA_ERROR;
	}


	if ( MEA_EHP_profile_stamping_Table[id_i].hwUnit != hwUnit ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile_stamping_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
			             __FUNCTION__,
						 id_i,
						 MEA_EHP_profile_stamping_Table[id_i].hwUnit,
						 hwUnit);
		return MEA_ERROR;
	}

	if ( MEA_EHP_profile_stamping_Table[id_i].num_of_owners == 0 ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile_stamping_Table[%d].num_of_owners == 0 \n",
			             __FUNCTION__,id_i);
		return MEA_ERROR;
	}

	MEA_EHP_profile_stamping_Table[id_i].num_of_owners--;

	if ( MEA_EHP_profile_stamping_Table[id_i].num_of_owners == 0 ) {

		MEA_OS_memset(&(MEA_EHP_profile_stamping_Table[id_i]),
			          0,
					  sizeof(MEA_EHP_profile_stamping_Table[0]));


	    if (mea_drv_EHP_UpdateHw_profile_stamping(unit_i,
		                                          id_i,
								                  &(MEA_EHP_profile_stamping_Table[id_i])) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_EHP_UpdateHw_profile_stamping failed (id=%d) \n",
							  __FUNCTION__,
							  id_i);
			return MEA_ERROR;
		}

	    if (mea_db_Delete(unit_i,
		                  MEA_EHP_profile_stamping_db,
					      (MEA_db_SwId_t)id_i,
					      hwUnit) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_db_Delete failed (EHP_profile_stamping) (swId=%d,hwUnit=%d) \n",
							  __FUNCTION__,id_i,hwUnit);
			return MEA_ERROR;
		}

	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Get_EHP_profile_stamping>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_EHP_profile_stamping(MEA_Unit_t                        unit_i,
                                            MEA_Editing_t                     id_i,
                                            MEA_drv_ehp_profile_stamping_dbt *entry_po)
{

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE_STAMPING) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE_STAMPING);
		return MEA_ERROR;
	}

	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(entry_po,
		          &(MEA_EHP_profile_stamping_Table[id_i]),
				  sizeof(*entry_po));
	
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_profile_sa_mss_id>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_profile_sa_mss_id(MEA_Unit_t                      unit_i,
													     MEA_EgressHeaderProc_Entry_dbt *entry_pi,
	                                                     MEA_Editing_t                  *id_o)
{
	MEA_Editing_t i;
	MEA_Uint32 sa_mss = 0;
	MEA_Bool found;
	MEA_db_HwUnit_t hwUnit;

	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);


    switch (entry_pi->EditingType)
    {
    case 0:
        //sa_mss = entry_pi->martini_info.SA.s[0];  /*msw 16bit only*/
        sa_mss = entry_pi->martini_info.SA.b[0] << 8;
        sa_mss |= entry_pi->martini_info.SA.b[1] << 0;
        break;
    case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_VLAN:
    case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_QTAG:
    case MEA_EDITINGTYPE_IPCS_UL_IPinIP_Extract_SGW:
    

        if (entry_pi->IPCS_UL_info.valid == MEA_TRUE){
            //sa_mss = (MEA_Uint32)entry_pi->IPCS_UL_info.Sa_host_mac.s[0];
            sa_mss = (MEA_Uint32)entry_pi->IPCS_UL_info.Sa_host_mac.b[0] << 8;
            sa_mss |= (MEA_Uint32)entry_pi->IPCS_UL_info.Sa_host_mac.b[1] << 0;

        }
        break;
        case MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent:  /*sa_mss  */
        case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN:
        case MEA_EDITINGTYPE_VPWS_DL_Swap_external_VLAN_TAG:
        case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN_Swap_Internal_VLAN:
        case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent:
        case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN:
        case MEA_EDITINGTYPE_VPWS_DL_Extract_Extract_QInQ:
        case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ:
            break;
        case MEA_EDITINGTYPE_GRE_Append_Tunnel:
        case MEA_EDITINGTYPE_L2TP_Append_Tunnel:
            if (entry_pi->GRE_L2TP_info.valid == MEA_TRUE) {
                sa_mss = entry_pi->GRE_L2TP_info.Sa.b[0] << 8;
                sa_mss |= entry_pi->GRE_L2TP_info.Sa.b[1] << 0;
            }
            break;

    default:
        //sa_mss = entry_pi->martini_info.SA.s[0];
        sa_mss = entry_pi->martini_info.SA.b[0]<<8;
        sa_mss |= entry_pi->martini_info.SA.b[1] <<0;
        break;
    }


   // MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "sa_mss_Table[i].sa_mss 0x%08x\n", sa_mss);

	found=MEA_FALSE;
	for (i=0;(!found)&&(i<MEA_MAX_NUM_OF_EHP_PROFILE_SA_MSS);i++) {
		if ( MEA_EHP_profile_sa_mss_Table[i].valid ) {
			
			if ((MEA_EHP_profile_sa_mss_Table[i].sa_mss == sa_mss) && 
				(MEA_EHP_profile_sa_mss_Table[i].hwUnit == hwUnit)  ) {
 			    MEA_EHP_profile_sa_mss_Table[i].num_of_owners++;
				found = MEA_TRUE;
				*id_o = i;
			}
		}
	}

	for (i=0;(!found)&&(i<MEA_MAX_NUM_OF_EHP_PROFILE_SA_MSS);i++) {
		if (!MEA_EHP_profile_sa_mss_Table[i].valid ) {
			MEA_db_HwId_t hwId;
            hwId = i; //MEA_DB_GENERATE_NEW_ID;
			if (mea_db_Create(unit_i,
				              MEA_EHP_profile_sa_mss_db,
							  (MEA_db_SwId_t)i,
							  hwUnit,
							  &hwId) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - mea_db_Create failed (EHP_profile_sa_mss) (swId=%d,hwUnit=%d) \n",
								  __FUNCTION__,&id_o,hwUnit);
				return MEA_ERROR;
			}
			MEA_OS_memset(&(MEA_EHP_profile_sa_mss_Table[i]),
				          0,
						  sizeof(MEA_EHP_profile_sa_mss_Table[0]));
			//sa_mss = entry_pi->martini_info.SA.a.msw;
		    MEA_EHP_profile_sa_mss_Table[i].sa_mss        = sa_mss;
			MEA_EHP_profile_sa_mss_Table[i].num_of_owners = 1;
			MEA_EHP_profile_sa_mss_Table[i].hwUnit        = hwUnit;
			MEA_EHP_profile_sa_mss_Table[i].valid         = MEA_TRUE;
			*id_o = i;
			found = MEA_TRUE;

		    if (mea_drv_EHP_UpdateHw_profile_sa_mss(unit_i,
                                                    *id_o,
								                    &(MEA_EHP_profile_sa_mss_Table[*id_o])) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - mea_drv_EHP_UpdateHw_profile_sa_mss failed (id=%d) \n",
								  __FUNCTION__,
								  *id_o);
			    mea_db_Delete(unit_i,MEA_EHP_profile_sa_mss_db,(MEA_db_SwId_t)(*id_o),hwUnit);
				MEA_OS_memset(&(MEA_EHP_profile_sa_mss_Table[*id_o]),
					          0,
							  sizeof(MEA_EHP_profile_sa_mss_Table[0]));
				return MEA_ERROR;
			}
		}
	}

	if (!found) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - EHP SA MSS Profile table is full\n",
                          __FUNCTION__);
		return MEA_ERROR;
    } 

	


	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Delete_EHP_profile_sa_mss_id>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Delete_EHP_profile_sa_mss_id(MEA_Unit_t                      unit_i,
                                                       MEA_Editing_t                   id_i)
{
	MEA_db_HwUnit_t hwUnit;

	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE_SA_MSS) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE_SA_MSS);
		return MEA_ERROR;
	}

	if ( ! MEA_EHP_profile_sa_mss_Table[id_i].valid ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile_sa_mss_Table[%d].valid == MEA_FALSE \n",
			             __FUNCTION__,id_i);
		return MEA_ERROR;
	}


	if ( MEA_EHP_profile_sa_mss_Table[id_i].hwUnit != hwUnit ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile_sa_mss_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
			             __FUNCTION__,
						 id_i,
						 MEA_EHP_profile_sa_mss_Table[id_i].hwUnit,
						 hwUnit);
		return MEA_ERROR;
	}

	if ( MEA_EHP_profile_sa_mss_Table[id_i].num_of_owners == 0 ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile_sa_mss_Table[%d].num_of_owners == 0 \n",
			             __FUNCTION__,id_i);
		return MEA_ERROR;
	}

	MEA_EHP_profile_sa_mss_Table[id_i].num_of_owners--;

	if ( MEA_EHP_profile_sa_mss_Table[id_i].num_of_owners == 0 ) {

		MEA_OS_memset(&(MEA_EHP_profile_sa_mss_Table[id_i]),
			          0,
					  sizeof(MEA_EHP_profile_sa_mss_Table[0]));

	    if (mea_drv_EHP_UpdateHw_profile_sa_mss(unit_i,
		                                        id_i,
								                &(MEA_EHP_profile_sa_mss_Table[id_i])) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_EHP_UpdateHw_profile_sa_mss failed (id=%d) \n",
							  __FUNCTION__,
							  id_i);
			return MEA_ERROR;
		}

	    if (mea_db_Delete(unit_i,
		                  MEA_EHP_profile_sa_mss_db,
					      (MEA_db_SwId_t)id_i,
					      hwUnit) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_db_Delete failed (EHP_profile_sa_mss) (swId=%d,hwUnit=%d) \n",
							  __FUNCTION__,id_i,hwUnit);
			return MEA_ERROR;
		}
	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Get_EHP_profile_sa_mss>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_EHP_profile_sa_mss(MEA_Unit_t                      unit_i,
                                          MEA_Editing_t                   id_i,
                                          MEA_drv_ehp_profile_sa_mss_dbt *entry_po)
{

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE_SA_MSS) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE_SA_MSS);
		return MEA_ERROR;
	}

	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(entry_po,
		          &(MEA_EHP_profile_sa_mss_Table[id_i]),
				  sizeof(*entry_po));
	
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_profile_ether_type_id>                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_profile_ether_type_id(MEA_Unit_t                      unit_i,
													         MEA_EgressHeaderProc_Entry_dbt *entry_pi,
	                                                         MEA_Editing_t                  *id_o)
{
	MEA_Editing_t i;
	MEA_Uint16 ether_type = 0;
	MEA_Bool found;
	MEA_db_HwUnit_t hwUnit;

	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);
	found=MEA_FALSE;
	for (i=0;(!found)&&(i<MEA_MAX_NUM_OF_EHP_PROFILE_ETHER_TYPE);i++) {
		if ( MEA_EHP_profile_ether_type_Table[i].valid ) {
			ether_type = entry_pi->martini_info.EtherType;
			if ((MEA_EHP_profile_ether_type_Table[i].ether_type == ether_type) &&
				(MEA_EHP_profile_ether_type_Table[i].hwUnit     == hwUnit    )  ) {
 			    MEA_EHP_profile_ether_type_Table[i].num_of_owners++;
				found = MEA_TRUE;
				*id_o = i;
			}
		}
	}

	for (i=0;(!found)&&(i<MEA_MAX_NUM_OF_EHP_PROFILE_ETHER_TYPE);i++) {
		if (!MEA_EHP_profile_ether_type_Table[i].valid ) {
			MEA_db_HwId_t hwId;
            hwId = i;//MEA_DB_GENERATE_NEW_ID;
			if (mea_db_Create(unit_i,
				              MEA_EHP_profile_ether_type_db,
							  (MEA_db_SwId_t)i,
							  hwUnit,
							  &hwId) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - mea_db_Create failed (EHP_profile_ether_type) (swId=%d,hwUnit=%d) \n",
								  __FUNCTION__,*id_o,hwUnit);
				return MEA_ERROR;
			}
			MEA_OS_memset(&(MEA_EHP_profile_ether_type_Table[i]),
				          0,
						  sizeof(MEA_EHP_profile_ether_type_Table[0]));
			ether_type = entry_pi->martini_info.EtherType;
 	        MEA_EHP_profile_ether_type_Table[i].ether_type    = ether_type;
		    MEA_EHP_profile_ether_type_Table[i].num_of_owners = 1;
		    MEA_EHP_profile_ether_type_Table[i].hwUnit        = hwUnit;
			MEA_EHP_profile_ether_type_Table[i].valid         = MEA_TRUE;
			*id_o = i;
			found = MEA_TRUE;

			if (mea_drv_EHP_UpdateHw_profile_ether_type(unit_i,
                                                        *id_o,
								                        &(MEA_EHP_profile_ether_type_Table[*id_o])) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - mea_drv_EHP_UpdateHw_profile_ether_type failed (id=%d) \n",
								  __FUNCTION__,
								  *id_o);
			    mea_db_Delete(unit_i,MEA_EHP_profile_ether_type_db,(MEA_db_SwId_t)(*id_o),hwUnit);
				MEA_OS_memset(&(MEA_EHP_profile_ether_type_Table[*id_o]),
					          0,
							  sizeof(MEA_EHP_profile_ether_type_Table[0]));
				return MEA_ERROR;
			}
		}
	}

	if (!found) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - EHP Profile Ether Type table is full\n",
                          __FUNCTION__);
		return MEA_ERROR;
    } 

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Delete_EHP_profile_ether_type_id>                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Delete_EHP_profile_ether_type_id(MEA_Unit_t                      unit_i,
                                                           MEA_Editing_t                   id_i)
{
	MEA_db_HwUnit_t hwUnit;

	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE_ETHER_TYPE) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE_ETHER_TYPE);
		return MEA_ERROR;
	}

	if ( ! MEA_EHP_profile_ether_type_Table[id_i].valid ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile_ether_type_Table[%d].valid == MEA_FALSE \n",
			             __FUNCTION__,id_i);
		return MEA_ERROR;
	}


	if ( MEA_EHP_profile_ether_type_Table[id_i].hwUnit != hwUnit ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile_ether_type_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
			             __FUNCTION__,
						 id_i,
						 MEA_EHP_profile_ether_type_Table[id_i].hwUnit,
						 hwUnit);
		return MEA_ERROR;
	}

	if ( MEA_EHP_profile_ether_type_Table[id_i].num_of_owners == 0 ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile_ether_type_Table[%d].num_of_owners == 0 \n",
			             __FUNCTION__,id_i);
		return MEA_ERROR;
	}

	MEA_EHP_profile_ether_type_Table[id_i].num_of_owners--;

	if ( MEA_EHP_profile_ether_type_Table[id_i].num_of_owners == 0 ) {

		MEA_OS_memset(&(MEA_EHP_profile_ether_type_Table[id_i]),
			          0,
					  sizeof(MEA_EHP_profile_ether_type_Table[0]));

	    if (mea_drv_EHP_UpdateHw_profile_ether_type(unit_i,
		                                            id_i,
								                    &(MEA_EHP_profile_ether_type_Table[id_i])) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_EHP_UpdateHw_profile_ether_type failed (id=%d) \n",
							  __FUNCTION__,
							  id_i);
			return MEA_ERROR;
		}

	    if (mea_db_Delete(unit_i,
		                  MEA_EHP_profile_ether_type_db,
					      (MEA_db_SwId_t)id_i,
					      hwUnit) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_db_Delete failed (EHP_profile_ether_type) (swId=%d,hwUnit=%d) \n",
							  __FUNCTION__,id_i,hwUnit);
			return MEA_ERROR;
		}
	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Get_EHP_profile_ether_type>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_EHP_profile_ether_type(MEA_Unit_t                         unit_i,
                                              MEA_Editing_t                      id_i,
                                              MEA_drv_ehp_profile_ether_type_dbt *entry_po)
{

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE_ETHER_TYPE) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE_ETHER_TYPE);
		return MEA_ERROR;
	}

	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(entry_po,
		          &(MEA_EHP_profile_ether_type_Table[id_i]),
				  sizeof(*entry_po));
	
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_profileId>                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_profileId(MEA_Unit_t                      unit_i,
											   MEA_EgressHeaderProc_Entry_dbt *entry_pi,
											   MEA_Editing_t                   eth_id_i) {
 
     MEA_drv_ehp_profileId_dbt profileId_entry;
	 MEA_Editing_t             tmp_editingId=0;
	 MEA_db_HwUnit_t           hwUnit;
	 MEA_db_HwUnit_t           tmp_hwUnit;
	 MEA_db_HwId_t             eth_hwId;
	 MEA_db_HwId_t             hwId;
	 MEA_db_SwId_t             swId;

     MEA_Uint32             martini_cmd;
//  MEA_Editing_t             id;


	 MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
								   mea_drv_Get_EHP_ETH_db,
								   eth_id_i,
								   hwUnit,
								   eth_hwId,
								   return MEA_ERROR;)


     hwId =  (eth_hwId % MEA_BM_TBL_TYP_EHP_PROFILE_ID_LENGTH(hwUnit));
	 for (tmp_hwUnit=0,swId=0;tmp_hwUnit<hwUnit;tmp_hwUnit++) {
		 swId +=  (MEA_db_SwId_t)(MEA_BM_TBL_TYP_EHP_PROFILE_ID_LENGTH(tmp_hwUnit));
	 }
	 swId += (MEA_db_SwId_t)hwId;
// 	 id = (MEA_Editing_t)swId;
//      if (id == 0){
// 
//      }





	 MEA_OS_memset((char*)&profileId_entry,0,sizeof(profileId_entry));
	 profileId_entry.num_of_owners = 1;
	 profileId_entry.hwUnit = hwUnit;
	 profileId_entry.valid = MEA_TRUE;

	 
	if ( MEA_EHP_profileId_Table[swId].valid) {

	    if (MEA_EHP_profileId_Table[swId].hwUnit != hwUnit) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					 		  "%s - MEA_EHP_profileId_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
					         __FUNCTION__,
							 swId,
							 MEA_EHP_profileId_Table[swId].hwUnit,
						     hwUnit);
			return MEA_ERROR;
		}

		if (MEA_OS_memcmp(&(MEA_EHP_profile_stamping_Table[MEA_EHP_profileId_Table[swId].profile_stamping].data),
			              &(entry_pi->eth_stamping_info),
						  sizeof(MEA_EHP_profile_stamping_Table[0].data)) != 0) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - profileId %d already exist with different stamping profile %d value (editId=%d)\n",
							  __FUNCTION__,
							  swId,
							  MEA_EHP_profileId_Table[swId].profile_stamping,
							  eth_id_i);
			return MEA_ERROR;
		}

 
		if ((MEA_Uint16)MEA_EHP_profile_sa_mss_Table[MEA_EHP_profileId_Table[swId].profile_sa_mss].sa_mss != (entry_pi->martini_info.SA.a.msw) ) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - profileId %d already exist with different sa_mss profile %d value (editId=%d)\n",
							  __FUNCTION__,
							  swId,
							  MEA_EHP_profileId_Table[swId].profile_sa_mss,
							  eth_id_i);
			return MEA_ERROR;
		}

		if (MEA_EHP_profile_ether_type_Table[MEA_EHP_profileId_Table[swId].profile_ether_type].ether_type != 
			entry_pi->martini_info.EtherType) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - profileId %d already exist with different ether_type profile %d value (editId=%d)\n",
							  __FUNCTION__,
							  swId,
							  MEA_EHP_profileId_Table[swId].profile_ether_type,
							  eth_id_i);
			return MEA_ERROR;
		}

		MEA_EHP_profileId_Table[swId].num_of_owners++;
		return MEA_OK;
    }


	if (mea_db_Create(unit_i,
		              MEA_EHP_profileId_db,
					  (MEA_db_SwId_t)swId,
					  hwUnit,
					  &hwId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Create failed (EHP_profileId) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,swId,hwUnit);
		return MEA_ERROR;
	}

     if (mea_drv_Create_EHP_profile_stamping_id(unit_i,
		                                        entry_pi,
												&tmp_editingId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_profile_stamping_id failed \n",
                          __FUNCTION__);
		mea_db_Delete(unit_i,MEA_EHP_profileId_db,swId,hwUnit);
		return MEA_ERROR;
	 }
	 profileId_entry.profile_stamping = tmp_editingId;

     if (mea_drv_Create_EHP_profile_sa_mss_id(unit_i,
		                                      entry_pi,
										      &tmp_editingId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_profile_sa_mss_id failed \n",
						  __FUNCTION__);
		mea_drv_Delete_EHP_profile_stamping_id(unit_i,profileId_entry.profile_stamping);
		mea_db_Delete(unit_i,MEA_EHP_profileId_db,swId,hwUnit);
		return MEA_ERROR;
	 }
	 profileId_entry.profile_sa_mss = tmp_editingId;

     if (mea_drv_Create_EHP_profile_ether_type_id(unit_i,
		                                          entry_pi,
										          &tmp_editingId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_profile_ether_type_id failed \n",
						  __FUNCTION__);
		mea_drv_Delete_EHP_profile_stamping_id(unit_i,profileId_entry.profile_stamping);
		mea_drv_Delete_EHP_profile_sa_mss_id(unit_i,profileId_entry.profile_sa_mss);
		mea_db_Delete(unit_i,MEA_EHP_profileId_db,swId,hwUnit);
		return MEA_ERROR;
	 }
	 profileId_entry.profile_ether_type = tmp_editingId;

     if (entry_pi->EditingType == 0) {
         martini_cmd = entry_pi->martini_info.martini_cmd;
     }
     else{ /*MEA_EDITINGTYPE_XXXX   MEA_MACHINE_PROTO_MARTINI*/
         martini_cmd = mea_drv_ProtocolMachine_get_command( entry_pi->EditingType, MEA_MACHINE_PROTO_MARTINI);

     }




     profileId_entry.martini_command = (MEA_Editing_t)martini_cmd;


	 MEA_OS_memcpy(&(MEA_EHP_profileId_Table[swId]),
		           &profileId_entry,
				   sizeof(MEA_EHP_profileId_Table[0]));

	 return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Delete_EHP_profileId>                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Delete_EHP_profileId(MEA_Unit_t               unit_i,
                                               MEA_Editing_t            eth_id_i)
{
	MEA_db_HwUnit_t hwUnit;
	MEA_db_HwId_t   eth_hwId;
	MEA_db_HwId_t   hwId;
	MEA_db_SwId_t   swId;
	MEA_Editing_t   id;

	 MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
								   mea_drv_Get_EHP_ETH_db,
								   eth_id_i,
								   hwUnit,
								   eth_hwId,
								   return MEA_ERROR;)


     hwId =  (eth_hwId % MEA_BM_TBL_TYP_EHP_PROFILE_ID_LENGTH(hwUnit));
	 MEA_DRV_GET_SW_ID(unit_i,
		               mea_drv_Get_EHP_profileId_db,
					   hwUnit,
					   hwId,
					   swId,
					   return MEA_ERROR;)
     id = (MEA_Editing_t)swId;



	if (id >= MEA_MAX_NUM_OF_EHP_PROFILE_ID) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id,MEA_MAX_NUM_OF_EHP_PROFILE_ID);
		return MEA_ERROR;
	}

	if ( ! MEA_EHP_profileId_Table[id].valid ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profileId_Table[%d].valid == MEA_FALSE \n",
			             __FUNCTION__,id);
		return MEA_ERROR;
	}


	if ( MEA_EHP_profileId_Table[id].hwUnit != hwUnit ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profileId_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
			             __FUNCTION__,
						 id,
						 MEA_EHP_profileId_Table[id].hwUnit,
						 hwUnit);
		return MEA_ERROR;
	}

	if ( MEA_EHP_profileId_Table[id].num_of_owners == 0 ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profileId_Table[%d].num_of_owners == 0 \n",
			             __FUNCTION__,id);
		return MEA_ERROR;
	}





	if ( MEA_EHP_profileId_Table[id].num_of_owners > 1 ) {
		MEA_EHP_profileId_Table[id].num_of_owners--;
		return MEA_OK;
	}




    if (mea_drv_Delete_EHP_profile_stamping_id(unit_i,
												MEA_EHP_profileId_Table[id].profile_stamping) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Delete_EHP_profile_stamping_id failed \n",
                          __FUNCTION__);
		return MEA_ERROR;
	 }
     if (mea_drv_Delete_EHP_profile_sa_mss_id(unit_i,
											  MEA_EHP_profileId_Table[id].profile_sa_mss) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Delete_EHP_profie_sa_mss failed \n",
                          __FUNCTION__);
		return MEA_ERROR;
	 }
     if (mea_drv_Delete_EHP_profile_ether_type_id(unit_i,
											      MEA_EHP_profileId_Table[id].profile_ether_type) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Delete_EHP_profie_ether_type failed \n",
                          __FUNCTION__);
		return MEA_ERROR;
	 }

	MEA_OS_memset(&(MEA_EHP_profileId_Table[id]),
		          0,
				  sizeof(MEA_EHP_profileId_Table[0]));


	if (mea_db_Delete(unit_i,
	                  MEA_EHP_profileId_db,
				      swId,
				      hwUnit) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Delete failed (EHP_profileId) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,swId,hwUnit);
		return MEA_ERROR;
	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Get_EHP_profileId>                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_EHP_profileId(MEA_Unit_t                 unit_i,
                                     MEA_Editing_t              id_i,
                                     MEA_drv_ehp_profileId_dbt *entry_po)
{

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE_ID) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE_ID);
		return MEA_ERROR;
	}

	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(entry_po,
		          &(MEA_EHP_profileId_Table[id_i]),
				  sizeof(*entry_po));
	
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_profile2_lm_id>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_profile2_lm_id(MEA_Unit_t                      unit_i,
											 	    MEA_EgressHeaderProc_Entry_dbt *entry_pi,
	                                                MEA_Editing_t                  *id_o)
{
	MEA_Editing_t i;
	MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t hwId;

	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);
    if (ENET_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH(hwUnit) == 0){
        *id_o = 0;
        return MEA_OK;
    }
        

    if(entry_pi->LmCounterId_info.LmId >= ENET_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH(hwUnit)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - LM id is out of range %d >= max %d \n",
            __FUNCTION__ ,
            entry_pi->LmCounterId_info.LmId,
            ENET_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH(hwUnit));
        return MEA_ERROR;
    }

    i = (MEA_Editing_t)(entry_pi->LmCounterId_info.LmId);
    
    if (MEA_EHP_profile2_lm_Table[i].valid ) {
        if (MEA_EHP_profile2_lm_Table[i].hwUnit != hwUnit) {
       	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - not same hwUnit \n",
				  		      __FUNCTION__);
            return MEA_ERROR;
        }
 	    MEA_EHP_profile2_lm_Table[i].num_of_owners++;
        *id_o = i;
        return MEA_OK;
    }
    
	
      hwId=i; 
    if (mea_db_Create(unit_i,
		              MEA_EHP_profile2_lm_db,
					  (MEA_db_SwId_t)i,
					  hwUnit,
					  &hwId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Create failed (EHP_profile2_lm) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,*id_o,hwUnit);
		return MEA_ERROR;
	}
	MEA_OS_memset(&(MEA_EHP_profile2_lm_Table[i]),0,sizeof(MEA_EHP_profile2_lm_Table[0]));
    MEA_EHP_profile2_lm_Table[i].valid         = MEA_TRUE;
 	MEA_EHP_profile2_lm_Table[i].hwUnit        = hwUnit;

    MEA_EHP_profile2_lm_Table[i].num_of_owners = 1;
	*id_o = i;
    /*-----------------------*/
    /* clear the Hw LM counter*/
    /*-----------------------*/
    if( mea_drv_EHP_UpdateHw_profile2_Lm(unit_i,i, 0) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_EHP_UpdateHw_profile2_Lm %d failed\n",
            __FUNCTION__,hwId);
        return MEA_ERROR;
    }



	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Delete_EHP_profile2_lm_id>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Delete_EHP_profile2_lm_id(MEA_Unit_t     unit_i,
                                                    MEA_Editing_t      id_i)
{
	MEA_db_HwUnit_t hwUnit;

	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);

    if (ENET_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH(hwUnit) == 0){
        return MEA_OK;
    }

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE2_LM) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE2_LM);
		return MEA_ERROR;
	}

	if ( ! MEA_EHP_profile2_lm_Table[id_i].valid ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile2_lm_Table[%d].valid == MEA_FALSE \n",
			             __FUNCTION__,id_i);
		return MEA_ERROR;
	}


	if ( MEA_EHP_profile2_lm_Table[id_i].hwUnit != hwUnit ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile2_lm_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
			             __FUNCTION__,
						 id_i,
						 MEA_EHP_profile_stamping_Table[id_i].hwUnit,
						 hwUnit);
		return MEA_ERROR;
	}

	if ( MEA_EHP_profile2_lm_Table[id_i].num_of_owners == 0 ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile2_lm_Table[%d].num_of_owners == 0 \n",
			             __FUNCTION__,id_i);
		return MEA_ERROR;
	}

	MEA_EHP_profile2_lm_Table[id_i].num_of_owners--;

	if ( MEA_EHP_profile2_lm_Table[id_i].num_of_owners == 0 ) {

		MEA_OS_memset(&(MEA_EHP_profile2_lm_Table[id_i]),
			          0,
					  sizeof(MEA_EHP_profile2_lm_Table[0]));

        /* T.B.D - Clear counters */

	    if (mea_db_Delete(unit_i,
		                  MEA_EHP_profile2_lm_db,
					      (MEA_db_SwId_t)id_i,
					      hwUnit) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_db_Delete failed (EHP_profile2_lm) (swId=%d,hwUnit=%d) \n",
							  __FUNCTION__,id_i,hwUnit);
			return MEA_ERROR;
		}

	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Get_EHP_profile2_lm>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_EHP_profile2_lm(MEA_Unit_t                        unit_i,
                                       MEA_Editing_t                     id_i,
                                       MEA_drv_ehp_profile2_lm_dbt      *entry_po)
{
    if (MEA_MAX_NUM_OF_EHP_PROFILE2_LM == 0){
        
        return MEA_OK;
    }

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE2_LM) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE2_LM);
		return MEA_ERROR;
	}

	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(entry_po,
		          &(MEA_EHP_profile2_lm_Table[id_i]),
				  sizeof(*entry_po));
	
	return MEA_OK;

}

//////////////////////////////////////////////////////////////////////////
static void mea_drv_afdx_reset_sequence_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
//    MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
//    MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32            set_value = (MEA_Uint32)((long)arg4);





    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
//    MEA_Uint32                  count_shift;
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
//    count_shift=0;


   
    
         val[0] =set_value;

    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            ENET_BM_IA_WRDATA0 +(i*4),
            val[i],
            MEA_MODULE_BM);  
    }


}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_fragmentSession>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_fragmentSession(MEA_Unit_t                      unit_i,
                                                MEA_EgressHeaderProc_Entry_dbt *entry_pi,
                                                MEA_Editing_t                   eth_id_i) 
{
    MEA_drv_ehp_fragmentSession_dbt entry;
    
    MEA_db_HwUnit_t                 hwUnit;
    MEA_db_HwUnit_t                 tmp_hwUnit;
    MEA_db_HwId_t                   eth_hwId;
    MEA_db_HwId_t                   hwId;
    MEA_db_SwId_t                   swId;
//    MEA_Editing_t                   id;
    

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Create_EHP_fragmentSession \n");
   if(entry_pi->fragmentSession_info.valid == MEA_FALSE){
        return MEA_OK;
    }
	
    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_ETH_db,
        eth_id_i,
        hwUnit,
        eth_hwId,
        return MEA_ERROR;)


        hwId =  (eth_hwId % MEA_BM_TBL_EHP_FRAGMENT_LENGTH(hwUnit));
    for (tmp_hwUnit=0,swId=0;tmp_hwUnit<hwUnit;tmp_hwUnit++) {
        swId +=  (MEA_db_SwId_t)(MEA_BM_TBL_EHP_FRAGMENT_LENGTH(tmp_hwUnit));
    }
    swId += (MEA_db_SwId_t)hwId;
//    id = (MEA_Editing_t)swId;


     
 
    
    if(entry_pi->fragmentSession_info.valid == MEA_TRUE &&
        (! MEA_FRAGMENT_EDITING_SESSION_SUPPORT)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - fragmentSession_info not support  \n",
                __FUNCTION__);
            return MEA_ERROR;
    }

    if(entry_pi->fragmentSession_info.Session_num > MEA_FRAGMENT_EDITING_NUM_OF_SESSION){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - fragmentSession_info.Session_num %d.> max of session %d \n",
            __FUNCTION__,
            entry_pi->fragmentSession_info.Session_num,
            MEA_FRAGMENT_EDITING_NUM_OF_SESSION-1);
        return MEA_ERROR;
    }
#if 0 // no need to afdx 
    if(entry_pi->fragmentSession_info.group > MEA_FRAGMENT_EDITING_NUM_OF_GROUP-1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- fragmentSession_info.group %d.> max of group %d \n",
            __FUNCTION__,
            entry_pi->fragmentSession_info.Session_num,
            MEA_FRAGMENT_EDITING_NUM_OF_GROUP-1);
        return MEA_ERROR;
    }
#endif 



    MEA_OS_memset((char*)&entry,0,sizeof(entry));
    entry.num_of_owners = 1;
    entry.hwUnit = hwUnit;
    entry.valid = MEA_TRUE;
    
    entry.session_num    = entry_pi->fragmentSession_info.Session_num;
    entry.enable         = entry_pi->fragmentSession_info.enable;
    entry.extract_enable = entry_pi->fragmentSession_info.extract_enable;
    entry.editId = eth_id_i; // save the software

   
    if ( MEA_EHP_fragmentSession_Table[swId].valid) {

        if (MEA_EHP_fragmentSession_Table[swId].hwUnit != hwUnit) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_EHP_fragmentSession_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
                __FUNCTION__,
                swId,
                MEA_EHP_fragmentSession_Table[swId].hwUnit,
                hwUnit);
            return MEA_ERROR;
        }
 
        MEA_EHP_fragmentSession_Table[swId].num_of_owners++;

        return MEA_OK;

    }
    if (mea_db_Create(unit_i,
        MEA_EHP_fragmentSession_db,
        (MEA_db_SwId_t)swId,
        hwUnit,
        &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Create failed (EHP_fragmentSession) (swId=%d,hwUnit=%d) \n",
                __FUNCTION__,swId,hwUnit);
            return MEA_ERROR;
    }

// need to clear the sequence on create
    if(MEA_AFDX_SUPPORT)
    {
        MEA_ind_write_t      ind_write;
        MEA_Uint32            value=0;
        /* Prepare ind_write structure */
        ind_write.tableType     = ENET_BM_TBL_TYP_AFDX_SEQUENCE;
        ind_write.tableOffset   = entry.session_num; 
        ind_write.cmdReg        = MEA_BM_IA_CMD;      
        ind_write.cmdMask        = MEA_BM_IA_CMD_MASK;      
        ind_write.statusReg        = MEA_BM_IA_STAT;   
        ind_write.statusMask    = MEA_BM_IA_STAT_MASK;   
        ind_write.tableAddrReg    = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask    = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_afdx_reset_sequence_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
        ind_write.funcParam3 = (MEA_funcParam)0;
        ind_write.funcParam4 = (MEA_funcParam)((long)value);
        if(entry.extract_enable == MEA_FALSE){
            if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_BM)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                    __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_BM);
                return ENET_ERROR;
            }
        }

    }


    if (mea_drv_EHP_UpdateHw_fragmentSession(unit_i,
        swId,
        &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_EHP_UpdateHw_fragmentSession failed (id=%d) \n",
                __FUNCTION__,
                swId);

            mea_db_Delete(unit_i,MEA_EHP_fragmentSession_db,swId,hwUnit);
            return MEA_ERROR;
    }

    MEA_OS_memcpy(&(MEA_EHP_fragmentSession_Table[swId]),
        &entry,
        sizeof(MEA_EHP_fragmentSession_Table[0]));




    return MEA_OK;

}
static MEA_Status mea_drv_Delete_EHP_fragmentSession(MEA_Unit_t          unit_i,
                                                MEA_Editing_t            eth_id_i)
{

    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   eth_hwId;
    MEA_db_HwId_t   hwId;
    MEA_db_SwId_t   swId;
    MEA_Editing_t   id;
// Alex    
    if(!MEA_EHP_Table[eth_id_i].editing_info.fragmentSession_info.valid){
    return MEA_OK;
    }	
	

    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_ETH_db,
        eth_id_i,
        hwUnit,
        eth_hwId,
        return MEA_ERROR;)


        hwId =  (eth_hwId % MEA_BM_TBL_EHP_FRAGMENT_LENGTH(hwUnit));
    MEA_DRV_GET_SW_ID(unit_i,
        mea_drv_Get_EHP_fragmentSession_db,
        hwUnit,
        hwId,
        swId,
        return MEA_ERROR;)
        id = (MEA_Editing_t)swId;


    if (id >= MEA_MAX_NUM_OF_EDIT_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id_i (%d) >= max value (%d) \n",
            __FUNCTION__,id,MEA_MAX_NUM_OF_EDIT_ID);
        return MEA_ERROR;
    }


    if ( ! MEA_EHP_fragmentSession_Table[id].valid ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_EHP_fragmentSession_Table[%d].valid == MEA_FALSE \n",
            __FUNCTION__,id);
        return MEA_ERROR;
    }


    if ( MEA_EHP_fragmentSession_Table[id].hwUnit != hwUnit ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_EHP_profile2Id_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
            __FUNCTION__,
            id,
            MEA_EHP_fragmentSession_Table[id].hwUnit,
            hwUnit);
        return MEA_ERROR;
    }

    if ( MEA_EHP_fragmentSession_Table[id].num_of_owners == 0 ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_EHP_fragmentSession_Table[%d].num_of_owners == 0 \n",
            __FUNCTION__,id);
        return MEA_ERROR;
    }





    if ( MEA_EHP_fragmentSession_Table[id].num_of_owners > 1 ) {
        MEA_EHP_fragmentSession_Table[id].num_of_owners--;
        return MEA_OK;
    }



    MEA_OS_memset(&(MEA_EHP_fragmentSession_Table[id]),
        0,
        sizeof(MEA_EHP_fragmentSession_Table[0]));


    if (mea_drv_EHP_UpdateHw_fragmentSession(unit_i,
        id,
        &(MEA_EHP_fragmentSession_Table[id])) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_EHP_UpdateHw_fragmentSession failed (id=%d) \n",
                __FUNCTION__,
                id);
            return MEA_ERROR;
    }

    if (mea_db_Delete(unit_i,
        MEA_EHP_fragmentSession_db,
        swId,
        hwUnit) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Delete failed (EHP_fragmentSession) (swId=%d,hwUnit=%d) \n",
                __FUNCTION__,swId,hwUnit);
            return MEA_ERROR;
    }

    
    
    
    
    return MEA_OK;
}

MEA_Status mea_drv_Get_EHP_fragmentSession(MEA_Unit_t                        unit_i,
                                      MEA_Editing_t                     id_i,
                                      MEA_drv_ehp_fragmentSession_dbt   *entry_po)
{
    if (id_i >= MEA_MAX_NUM_OF_EDIT_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id_i (%d) >= max value (%d) \n",
            __FUNCTION__,id_i,MEA_MAX_NUM_OF_EDIT_ID);
        return MEA_ERROR;
    }

    if (entry_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_po == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry_po,
        &(MEA_EHP_fragmentSession_Table[id_i]),
        sizeof(*entry_po));

    return MEA_OK;


}

//////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_pw_control>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_pw_control(MEA_Unit_t                      unit_i,
                                                MEA_EgressHeaderProc_Entry_dbt *entry_pi,
                                                MEA_Editing_t                   eth_id_i) 
{
    MEA_drv_ehp_pw_control_dbt entry;

    MEA_db_HwUnit_t                 hwUnit;
    MEA_db_HwUnit_t                 tmp_hwUnit;
    MEA_db_HwId_t                   eth_hwId;
    MEA_db_HwId_t                   hwId;
    MEA_db_SwId_t                   swId;
//    MEA_Editing_t                   id;


    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Create_EHP_fragmentSession \n");
    if(entry_pi->pw_control_info.valid == MEA_FALSE){
        return MEA_OK;
    }

    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_ETH_db,
        eth_id_i,
        hwUnit,
        eth_hwId,
        return MEA_ERROR;)


        hwId =  (eth_hwId % MEA_BM_TBL_EHP_PW_CONTROL_LENGTH(hwUnit));
    for (tmp_hwUnit=0,swId=0;tmp_hwUnit<hwUnit;tmp_hwUnit++) {
        swId +=  (MEA_db_SwId_t)(MEA_BM_TBL_EHP_PW_CONTROL_LENGTH(tmp_hwUnit));
    }
    swId += (MEA_db_SwId_t)hwId;
//    id = (MEA_Editing_t)swId;






    if(entry_pi->pw_control_info.valid == MEA_TRUE &&
        (! MEA_EDITING_PW_CONTROL_SUPPORT)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - pw control not support  \n",
                __FUNCTION__);
            return MEA_ERROR;
    }




    MEA_OS_memset((char*)&entry,0,sizeof(entry));
    entry.num_of_owners = 1;
    entry.hwUnit = hwUnit;
    entry.valid = MEA_TRUE;
    MEA_OS_memcpy(&entry.pw_control,&entry_pi->pw_control_info,sizeof(entry.pw_control));
    
    
    entry.editId = eth_id_i; // save the software


    if ( MEA_EHP_pw_control_Table[swId].valid) {

        if (MEA_EHP_pw_control_Table[swId].hwUnit != hwUnit) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_EHP_pw_control_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
                __FUNCTION__,
                swId,
                MEA_EHP_pw_control_Table[swId].hwUnit,
                hwUnit);
            return MEA_ERROR;
        }

        MEA_EHP_pw_control_Table[swId].num_of_owners++;

        return MEA_OK;

    }
    if (mea_db_Create(unit_i,
        MEA_EHP_pw_control_db,
        (MEA_db_SwId_t)swId,
        hwUnit,
        &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Create failed (EHP_pw_control) (swId=%d,hwUnit=%d) \n",
                __FUNCTION__,swId,hwUnit);
            return MEA_ERROR;
    }


    if (mea_drv_EHP_UpdateHw_pw_control(unit_i,
        swId,
        &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_EHP_UpdateHw_pw_control failed (id=%d) \n",
                __FUNCTION__,
                swId);

            mea_db_Delete(unit_i,MEA_EHP_pw_control_db,swId,hwUnit);
            return MEA_ERROR;
    }

    MEA_OS_memcpy(&(MEA_EHP_pw_control_Table[swId]),
        &entry,
        sizeof(MEA_EHP_pw_control_Table[0]));




    return MEA_OK;

}



static MEA_Status mea_drv_Modify_EHP_pw_control(MEA_Unit_t                      unit_i,
                                                MEA_EgressHeaderProc_Entry_dbt *entry_pi,
                                                MEA_Editing_t                   eth_id_i) 
{
    MEA_drv_ehp_pw_control_dbt entry;

    MEA_db_HwUnit_t                 hwUnit;
    MEA_db_HwUnit_t                 tmp_hwUnit;
    MEA_db_HwId_t                   eth_hwId;
    MEA_db_HwId_t                   hwId;
    MEA_db_SwId_t                   swId;
//    MEA_Editing_t                   id;


    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Create_EHP_fragmentSession \n");
    

    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_ETH_db,
        eth_id_i,
        hwUnit,
        eth_hwId,
        return MEA_ERROR;)


        hwId =  (eth_hwId % MEA_BM_TBL_EHP_PW_CONTROL_LENGTH(hwUnit));
    for (tmp_hwUnit=0,swId=0;tmp_hwUnit<hwUnit;tmp_hwUnit++) {
        swId +=  (MEA_db_SwId_t)(MEA_BM_TBL_EHP_PW_CONTROL_LENGTH(tmp_hwUnit));
    }
    swId += (MEA_db_SwId_t)hwId;
//    id = (MEA_Editing_t)swId;






    if(entry_pi->pw_control_info.valid == MEA_TRUE &&
        (! MEA_EDITING_PW_CONTROL_SUPPORT)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - pw control not support  \n",
                __FUNCTION__);
            return MEA_ERROR;
    }




    MEA_OS_memset((char*)&entry,0,sizeof(entry));
    entry.num_of_owners = MEA_EHP_pw_control_Table[swId].num_of_owners;
    entry.hwUnit = MEA_EHP_pw_control_Table[swId].hwUnit;
    entry.valid = MEA_EHP_pw_control_Table[swId].valid;
    MEA_OS_memcpy(&entry.pw_control,&entry_pi->pw_control_info,sizeof(entry.pw_control));


    entry.editId = eth_id_i; // save the software


    if ( MEA_EHP_pw_control_Table[swId].valid) {

        if (MEA_EHP_pw_control_Table[swId].hwUnit != hwUnit) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_EHP_pw_control_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
                __FUNCTION__,
                swId,
                MEA_EHP_pw_control_Table[swId].hwUnit,
                hwUnit);
            return MEA_ERROR;
        }
        
        if (mea_db_Delete(unit_i,
            MEA_EHP_pw_control_db,
            swId,
            hwUnit) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_db_Delete failed (EHP_pw_control) (swId=%d,hwUnit=%d) \n",
                    __FUNCTION__,swId,hwUnit);
                return MEA_ERROR;
        }

    
    if (mea_db_Create(unit_i,
        MEA_EHP_pw_control_db,
        (MEA_db_SwId_t)swId,
        hwUnit,
        &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Create failed (EHP_pw_control) (swId=%d,hwUnit=%d) \n",
                __FUNCTION__,swId,hwUnit);
            return MEA_ERROR;
    }


    if (mea_drv_EHP_UpdateHw_pw_control(unit_i,
        swId,
        &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_EHP_UpdateHw_pw_control failed (id=%d) \n",
                __FUNCTION__,
                swId);

            mea_db_Delete(unit_i,MEA_EHP_pw_control_db,swId,hwUnit);
            return MEA_ERROR;
    }

    MEA_OS_memcpy(&(MEA_EHP_pw_control_Table[swId]),
        &entry,
        sizeof(MEA_EHP_pw_control_Table[0]));

    return MEA_OK;

    }


    return MEA_OK;

}






static MEA_Status mea_drv_Delete_EHP_pw_control(MEA_Unit_t               unit_i,
                                                MEA_Editing_t            eth_id_i)
{

    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   eth_hwId;
    MEA_db_HwId_t   hwId;
    MEA_db_SwId_t   swId;
    MEA_Editing_t   id;
    
    
    // Alex    
    if(!MEA_EHP_Table[eth_id_i].editing_info.pw_control_info.valid){
        return MEA_OK;
    }	




    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_ETH_db,
        eth_id_i,
        hwUnit,
        eth_hwId,
        return MEA_ERROR;)


        hwId =  (eth_hwId % ENET_BM_TBL_EHP_PW_CONTROL_LENGTH(hwUnit));
    MEA_DRV_GET_SW_ID(unit_i,
        mea_drv_Get_EHP_pw_control_db,
        hwUnit,
        hwId,
        swId,
        return MEA_ERROR;)
        id = (MEA_Editing_t)swId;


    if (id >= MEA_MAX_NUM_OF_EDIT_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id_i (%d) >= max value (%d) \n",
            __FUNCTION__,id,MEA_MAX_NUM_OF_EDIT_ID);
        return MEA_ERROR;
    }


    if ( ! MEA_EHP_pw_control_Table[id].valid ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_EHP_profile2Id_Table[%d].valid == MEA_FALSE \n",
            __FUNCTION__,id);
        return MEA_ERROR;
    }


    if ( MEA_EHP_pw_control_Table[id].hwUnit != hwUnit ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_EHP_pw_control_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
            __FUNCTION__,
            id,
            MEA_EHP_pw_control_Table[id].hwUnit,
            hwUnit);
        return MEA_ERROR;
    }

    if ( MEA_EHP_pw_control_Table[id].num_of_owners == 0 ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_EHP_profile2Id_Table[%d].num_of_owners == 0 \n",
            __FUNCTION__,id);
        return MEA_ERROR;
    }





    if ( MEA_EHP_pw_control_Table[id].num_of_owners > 1 ) {
        MEA_EHP_pw_control_Table[id].num_of_owners--;
        return MEA_OK;
    }



    MEA_OS_memset(&(MEA_EHP_pw_control_Table[id]),
        0,
        sizeof(MEA_EHP_pw_control_Table[0]));


    if (mea_drv_EHP_UpdateHw_pw_control(unit_i,
        id,
        &(MEA_EHP_pw_control_Table[id])) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_EHP_UpdateHw_pw_control failed (id=%d) \n",
                __FUNCTION__,
                id);
            return MEA_ERROR;
    }

    if (mea_db_Delete(unit_i,
        MEA_EHP_pw_control_db,
        swId,
        hwUnit) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Delete failed (EHP_pw_control) (swId=%d,hwUnit=%d) \n",
                __FUNCTION__,swId,hwUnit);
            return MEA_ERROR;
    }





    return MEA_OK;
}

MEA_Status mea_drv_Get_EHP_pw_control(MEA_Unit_t                        unit_i,
                                           MEA_Editing_t                     id_i,
                                           MEA_drv_ehp_pw_control_dbt   *entry_po)
{
    if (id_i >= MEA_MAX_NUM_OF_EDIT_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id_i (%d) >= max value (%d) \n",
            __FUNCTION__,id_i,MEA_MAX_NUM_OF_EDIT_ID);
        return MEA_ERROR;
    }

    if (entry_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_po == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry_po,
        &(MEA_EHP_pw_control_Table[id_i]),
        sizeof(*entry_po));

    return MEA_OK;


}





//////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_mpls_label>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_mpls_label(MEA_Unit_t                      unit_i,
                                                MEA_EgressHeaderProc_Entry_dbt *entry_pi,
                                                MEA_Editing_t                   eth_id_i) 
{
    MEA_drv_ehp_mpls_label_dbt entry;

    MEA_db_HwUnit_t                 hwUnit;
    MEA_db_HwUnit_t                 tmp_hwUnit;
    MEA_db_HwId_t                   eth_hwId;
    MEA_db_HwId_t                   hwId;
    MEA_db_SwId_t                   swId;
//    MEA_Editing_t                   id;


    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Create_EHP_fragmentSession \n");
    if(entry_pi->mpls_label_info.valid == MEA_FALSE){
        return MEA_OK;
    }

    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_ETH_db,
        eth_id_i,
        hwUnit,
        eth_hwId,
        return MEA_ERROR;)


        hwId =  (eth_hwId % ENET_BM_TBL_EHP_MPLS_LABEL_LENGTH(hwUnit));
    for (tmp_hwUnit=0,swId=0;tmp_hwUnit<hwUnit;tmp_hwUnit++) {
        swId +=  (MEA_db_SwId_t)(ENET_BM_TBL_EHP_MPLS_LABEL_LENGTH(tmp_hwUnit));
    }
    swId += (MEA_db_SwId_t)hwId;
//     id = (MEA_Editing_t)swId;
//     if (id == 0){
// 
//     }





    if(entry_pi->mpls_label_info.valid == MEA_TRUE &&
        (! MEA_EDITING_PW_CONTROL_SUPPORT)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - pw control not support  \n",
                __FUNCTION__);
            return MEA_ERROR;
    }




    MEA_OS_memset((char*)&entry,0,sizeof(entry));
    entry.num_of_owners = 1;
    entry.hwUnit = hwUnit;
    entry.valid = MEA_TRUE;
    MEA_OS_memcpy(&entry.mpls_label,&entry_pi->mpls_label_info,sizeof(entry.mpls_label));


    entry.editId = eth_id_i; // save the software


    if ( MEA_EHP_mpls_label_Table[swId].valid) {

        if (MEA_EHP_mpls_label_Table[swId].hwUnit != hwUnit) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_EHP_mpls_label_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
                __FUNCTION__,
                swId,
                MEA_EHP_mpls_label_Table[swId].hwUnit,
                hwUnit);
            return MEA_ERROR;
        }

        MEA_EHP_mpls_label_Table[swId].num_of_owners++;

        return MEA_OK;

    }
    if (mea_db_Create(unit_i,
        MEA_EHP_mpls_label_db,
        (MEA_db_SwId_t)swId,
        hwUnit,
        &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Create failed (EHP_mpls_label) (swId=%d,hwUnit=%d) \n",
                __FUNCTION__,swId,hwUnit);
            return MEA_ERROR;
    }


    if (mea_drv_EHP_UpdateHw_mpls_label(unit_i,
        swId,
        &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_EHP_UpdateHw_pw_control failed (id=%d) \n",
                __FUNCTION__,
                swId);

            mea_db_Delete(unit_i,MEA_EHP_mpls_label_db,swId,hwUnit);
            return MEA_ERROR;
    }

    MEA_OS_memcpy(&(MEA_EHP_mpls_label_Table[swId]),
        &entry,
        sizeof(MEA_EHP_mpls_label_Table[0]));




    return MEA_OK;

}
static MEA_Status mea_drv_Delete_EHP_mpls_label(MEA_Unit_t               unit_i,
                                                MEA_Editing_t            eth_id_i)
{

    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   eth_hwId;
    MEA_db_HwId_t   hwId;
    MEA_db_SwId_t   swId;
    MEA_Editing_t   id;
    // Alex    
    if(!MEA_EHP_Table[eth_id_i].editing_info.mpls_label_info.valid){
        return MEA_OK;
    }	


    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_ETH_db,
        eth_id_i,
        hwUnit,
        eth_hwId,
        return MEA_ERROR;)


        hwId =  (eth_hwId % ENET_BM_TBL_EHP_MPLS_LABEL_LENGTH(hwUnit));
    MEA_DRV_GET_SW_ID(unit_i,
        mea_drv_Get_EHP_mpls_label_db,
        hwUnit,
        hwId,
        swId,
        return MEA_ERROR;)
        id = (MEA_Editing_t)swId;


    if (id >= MEA_MAX_NUM_OF_EDIT_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id_i (%d) >= max value (%d) \n",
            __FUNCTION__,id,MEA_MAX_NUM_OF_EDIT_ID);
        return MEA_ERROR;
    }


    if ( ! MEA_EHP_mpls_label_Table[id].valid ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_EHP_mpls_label_Table[%d].valid == MEA_FALSE \n",
            __FUNCTION__,id);
        return MEA_ERROR;
    }


    if ( MEA_EHP_mpls_label_Table[id].hwUnit != hwUnit ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_EHP_mpls_label_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
            __FUNCTION__,
            id,
            MEA_EHP_mpls_label_Table[id].hwUnit,
            hwUnit);
        return MEA_ERROR;
    }

    if ( MEA_EHP_mpls_label_Table[id].num_of_owners == 0 ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_EHP_mpls_label_Table[%d].num_of_owners == 0 \n",
            __FUNCTION__,id);
        return MEA_ERROR;
    }





    if ( MEA_EHP_mpls_label_Table[id].num_of_owners > 1 ) {
        MEA_EHP_mpls_label_Table[id].num_of_owners--;
        return MEA_OK;
    }



    MEA_OS_memset(&(MEA_EHP_mpls_label_Table[id]),
        0,
        sizeof(MEA_EHP_mpls_label_Table[0]));


    if (mea_drv_EHP_UpdateHw_mpls_label(unit_i,
        id,
        &(MEA_EHP_mpls_label_Table[id])) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_EHP_UpdateHw_pw_control failed (id=%d) \n",
                __FUNCTION__,
                id);
            return MEA_ERROR;
    }

    if (mea_db_Delete(unit_i,
        MEA_EHP_mpls_label_db,
        swId,
        hwUnit) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Delete failed (EHP_mpls_label) (swId=%d,hwUnit=%d) \n",
                __FUNCTION__,swId,hwUnit);
            return MEA_ERROR;
    }





    return MEA_OK;
}


MEA_Status mea_drv_Get_EHP_mpls_label(MEA_Unit_t                        unit_i,
                                      MEA_Editing_t                     id_i,
                                      MEA_drv_ehp_mpls_label_dbt   *entry_po)
{
    if (id_i >= MEA_MAX_NUM_OF_EDIT_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id_i (%d) >= max value (%d) \n",
            __FUNCTION__,id_i,MEA_MAX_NUM_OF_EDIT_ID);
        return MEA_ERROR;
    }

    if (entry_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_po == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry_po,
        &(MEA_EHP_mpls_label_Table[id_i]),
        sizeof(*entry_po));

    return MEA_OK;


}









/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_profile2Id>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_profile2Id(MEA_Unit_t                      unit_i,
                                                MEA_EgressHeaderProc_Entry_dbt *entry_pi,
                                                MEA_Editing_t                   eth_id_i) {
 
     MEA_drv_ehp_profile2Id_dbt profile2Id_entry;
	 MEA_Editing_t              tmp_editingId=0;
	 MEA_db_HwUnit_t            hwUnit;
	 MEA_db_HwUnit_t            tmp_hwUnit;
	 MEA_db_HwId_t              eth_hwId;
	 MEA_db_HwId_t              hwId;
	 MEA_db_SwId_t              swId;

     
     MEA_Bool ETH_CS_packet_is_valid;
     


//MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Create_EHP_profile2Id \n");

     ETH_CS_packet_is_valid = entry_pi->LmCounterId_info.ETH_CS_packet_is_valid;

     switch (entry_pi->EditingType)
     {
     case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_VLAN:
     case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_QTAG:
     case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_VLAN:
     case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_QTAG:
     case MEA_EDITINGTYPE_IPCS_UL_IPinIP_Extract_SGW:
     case MEA_EDITINGTYPE_IPCS_DL_IPinIP_Append_SGW:
     case MEA_EDITINGTYPE_IPCS_DL_UL_SGW_Swap_header:
         break;

     case MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN :
     case MEA_EDITINGTYPE_VPWS_DL_Swap_external_VLAN_TAG:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN_Swap_Internal_VLAN:
     
     case MEA_EDITINGTYPE_VPWS_DL_Extract_Extract_QInQ:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ:

         ETH_CS_packet_is_valid = MEA_TRUE;
         break;
     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QinQ:
     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS:
     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG:
     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Swap_External_TAG:
     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG_Swap_Internal_VLAN:
         break;
     

     } // end switch


	 MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
								   mea_drv_Get_EHP_ETH_db,
								   eth_id_i,
								   hwUnit,
								   eth_hwId,
								   return MEA_ERROR;)


     hwId =  (eth_hwId % MEA_BM_TBL_TYP_EHP_PROFILE2_ID_LENGTH(hwUnit));
	 for (tmp_hwUnit=0,swId=0;tmp_hwUnit<hwUnit;tmp_hwUnit++) {
		 swId +=  (MEA_db_SwId_t)(MEA_BM_TBL_TYP_EHP_PROFILE2_ID_LENGTH(tmp_hwUnit));
	 }
	 swId += (MEA_db_SwId_t)hwId;


    MEA_OS_memset((char*)&profile2Id_entry,0,sizeof(profile2Id_entry));
    profile2Id_entry.num_of_owners = 1;
    profile2Id_entry.hwUnit = hwUnit;
    profile2Id_entry.valid = MEA_TRUE;
    profile2Id_entry.Command_dasa = entry_pi->LmCounterId_info.Command_dasa;
    profile2Id_entry.Command_cfm = entry_pi->LmCounterId_info.Command_cfm;
    profile2Id_entry.profile2_lm = (MEA_Editing_t)entry_pi->LmCounterId_info.LmId;
    profile2Id_entry.Command_dscp_stamp_enable = entry_pi->LmCounterId_info.Command_dscp_stamp_enable;
    profile2Id_entry.Command_dscp_stamp_value = entry_pi->LmCounterId_info.Command_dscp_stamp_value;
    profile2Id_entry.ETH_CS_packet_is_valid = ETH_CS_packet_is_valid;
     
    profile2Id_entry.sw_calcIp_checksum_valid = entry_pi->LmCounterId_info.sw_calcIp_checksum_valid;
    profile2Id_entry.sw_calcIp_checksum_force = entry_pi->LmCounterId_info.sw_calcIp_checksum_force;
    profile2Id_entry.calcIp_checksum = entry_pi->LmCounterId_info.calcIp_checksum;
     

     if (entry_pi->NAT_info.valid == MEA_TRUE){
         profile2Id_entry.Command_dasa = entry_pi->NAT_info.NAT_type;
         profile2Id_entry.calcIp_checksum = MEA_TRUE;
     }
     if (profile2Id_entry.Command_dscp_stamp_enable) {
         profile2Id_entry.calcIp_checksum = MEA_TRUE;
     }

     if (entry_pi->EditingType != 0)
     {
         if (profile2Id_entry.calcIp_checksum == MEA_FALSE && profile2Id_entry.sw_calcIp_checksum_valid == MEA_FALSE){
             profile2Id_entry.calcIp_checksum = mea_drv_ProtocolMachine_get_command(entry_pi->EditingType, MEA_MACHINE_CALC_CHECKSUM);
             /*update the user*/
             entry_pi->LmCounterId_info.calcIp_checksum = profile2Id_entry.calcIp_checksum;
         }
     }

     if (entry_pi->LmCounterId_info.Command_dasa != 0){ /* router enable NAT*/
         profile2Id_entry.calcIp_checksum = MEA_TRUE;
         /*update the user*/
         entry_pi->LmCounterId_info.calcIp_checksum = profile2Id_entry.calcIp_checksum;

     }
 
     if (entry_pi->LmCounterId_info.Command_dscp_stamp_enable == MEA_TRUE && profile2Id_entry.calcIp_checksum == MEA_TRUE){
         profile2Id_entry.calcIp_checksum = MEA_TRUE;
         /*update the user*/
         entry_pi->LmCounterId_info.calcIp_checksum = profile2Id_entry.calcIp_checksum;
     }

     /*if user want to set this */
     if (entry_pi->LmCounterId_info.sw_calcIp_checksum_valid == MEA_TRUE){
         profile2Id_entry.calcIp_checksum = entry_pi->LmCounterId_info.sw_calcIp_checksum_force;
         /*update the user*/
         entry_pi->LmCounterId_info.calcIp_checksum = profile2Id_entry.calcIp_checksum;
     }

    

	 
	if ( MEA_EHP_profile2Id_Table[swId].valid) {

	    if (MEA_EHP_profile2Id_Table[swId].hwUnit != hwUnit) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					 		  "%s - MEA_EHP_profile2Id_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
					         __FUNCTION__,
							 swId,
							 MEA_EHP_profile2Id_Table[swId].hwUnit,
						     hwUnit);
			return MEA_ERROR;
		}
        
		MEA_EHP_profile2Id_Table[swId].num_of_owners++;
		return MEA_OK;
    }
	if (mea_db_Create(unit_i,
		              MEA_EHP_profile2Id_db,
					  (MEA_db_SwId_t)swId,
					  hwUnit,
					  &hwId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Create failed (EHP_profile2Id) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,swId,hwUnit);
		return MEA_ERROR;
	}

     if (mea_drv_Create_EHP_profile2_lm_id(unit_i,
		                                   entry_pi,
									 	   &tmp_editingId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_profile2_lm_id failed \n",
                          __FUNCTION__);
		mea_db_Delete(unit_i,MEA_EHP_profile2Id_db,swId,hwUnit);
		return MEA_ERROR;
	 }
	 profile2Id_entry.profile2_lm = tmp_editingId;


	 MEA_OS_memcpy(&(MEA_EHP_profile2Id_Table[swId]),
		           &profile2Id_entry,
				   sizeof(MEA_EHP_profile2Id_Table[0]));

	 return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Delete_EHP_profile2Id>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Delete_EHP_profile2Id(MEA_Unit_t               unit_i,
                                                MEA_Editing_t            eth_id_i)
{
	MEA_db_HwUnit_t hwUnit;
	MEA_db_HwId_t   eth_hwId;
	MEA_db_HwId_t   hwId;
	MEA_db_SwId_t   swId;
	MEA_Editing_t   id;

//      if(!MEA_EHP_Table[eth_id_i].editing_info.LmCounterId_info.valid ){
//          return MEA_OK;
//      }

	 MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
								   mea_drv_Get_EHP_ETH_db,
								   eth_id_i,
								   hwUnit,
								   eth_hwId,
								   return MEA_ERROR;)


     hwId =  (eth_hwId % MEA_BM_TBL_TYP_EHP_PROFILE2_ID_LENGTH(hwUnit));
	 MEA_DRV_GET_SW_ID(unit_i,
		               mea_drv_Get_EHP_profile2Id_db,
					   hwUnit,
					   hwId,
					   swId,
					   return MEA_ERROR;)
     id = (MEA_Editing_t)swId;


 	 if (id >= MEA_MAX_NUM_OF_EHP_PROFILE2_ID) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id,MEA_MAX_NUM_OF_EHP_PROFILE2_ID);
		return MEA_ERROR;
	}

	if ( ! MEA_EHP_profile2Id_Table[id].valid ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile2Id_Table[%d].valid == MEA_FALSE \n",
			             __FUNCTION__,id);
		return MEA_ERROR;
	}


	if ( MEA_EHP_profile2Id_Table[id].hwUnit != hwUnit ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile2Id_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
			             __FUNCTION__,
						 id,
						 MEA_EHP_profile2Id_Table[id].hwUnit,
						 hwUnit);
		return MEA_ERROR;
	}

	if ( MEA_EHP_profile2Id_Table[id].num_of_owners == 0 ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_profile2Id_Table[%d].num_of_owners == 0 \n",
			             __FUNCTION__,id);
		return MEA_ERROR;
	}





	if ( MEA_EHP_profile2Id_Table[id].num_of_owners > 1 ) {
		MEA_EHP_profile2Id_Table[id].num_of_owners--;
		return MEA_OK;
	}




    if (mea_drv_Delete_EHP_profile2_lm_id(unit_i,
									      MEA_EHP_profile2Id_Table[id].profile2_lm) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Delete_EHP_profile2_lm_id failed \n",
                          __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memset(&(MEA_EHP_profile2Id_Table[id]),
		          0,
				  sizeof(MEA_EHP_profile2Id_Table[0]));


	if (mea_db_Delete(unit_i,
	                  MEA_EHP_profile2Id_db,
				      swId,
				      hwUnit) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Delete failed (EHP_profile2Id) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,swId,hwUnit);
		return MEA_ERROR;
	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Get_EHP_profile2Id>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_EHP_profile2Id(MEA_Unit_t                 unit_i,
                                      MEA_Editing_t              id_i,
                                      MEA_drv_ehp_profile2Id_dbt *entry_po)
{

	if (id_i >= MEA_MAX_NUM_OF_EHP_PROFILE2_ID) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_PROFILE2_ID);
		return MEA_ERROR;
	}

	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(entry_po,
		          &(MEA_EHP_profile2Id_Table[id_i]),
				  sizeof(*entry_po));
	
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_da>                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_da(MEA_Unit_t                      unit_i,
										    MEA_EgressHeaderProc_Entry_dbt *entry_pi,
											MEA_Editing_t                   eth_id_i) {
 
     MEA_drv_ehp_da_dbt    da_entry;
	 MEA_db_HwUnit_t           hwUnit;
	 //MEA_db_HwUnit_t           tmp_hwUnit;
	 MEA_db_HwId_t             eth_hwId;
	 MEA_db_HwId_t             hwId;
	 MEA_db_SwId_t             swId;
	 MEA_Editing_t             id;
     


	 MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
								   mea_drv_Get_EHP_ETH_db,
								   eth_id_i,
								   hwUnit,
								   eth_hwId,
								   return MEA_ERROR;)


#if 0
     hwId =  (eth_hwId % MEA_BM_TBL_TYP_EHP_DA_LENGTH(hwUnit));
	 for (tmp_hwUnit=0,swId=0;tmp_hwUnit<hwUnit;tmp_hwUnit++) {
		 swId +=  (MEA_db_SwId_t)(MEA_BM_TBL_TYP_EHP_DA_LENGTH(tmp_hwUnit));
	 }
	 swId += (MEA_db_SwId_t)hwId;
#else
     hwId = eth_hwId;
     swId = eth_id_i;
#endif
	 id = (MEA_Editing_t)swId;


	 MEA_OS_memset((char*)&da_entry,0,sizeof(da_entry));
     da_entry.num_of_owners = 1;
	 da_entry.hwUnit = (MEA_Uint16)hwUnit;
	 da_entry.valid = MEA_TRUE;
	 da_entry.da.s[0] = entry_pi->martini_info.DA.s[0];
	 da_entry.da.s[1] = entry_pi->martini_info.DA.s[1];
	 da_entry.da.s[2] = entry_pi->martini_info.DA.s[2]; 
     
     if (entry_pi->NAT_info.valid == MEA_TRUE){

             da_entry.da.b[0] = (entry_pi->NAT_info.L4Port>>8) & 0xff;
             da_entry.da.b[1] = (entry_pi->NAT_info.L4Port) & 0xff;
             da_entry.da.b[2] = (entry_pi->NAT_info.Ip >> 24) & 0xff;
             da_entry.da.b[3] = (entry_pi->NAT_info.Ip >> 16) & 0xff;
             da_entry.da.b[4] = (entry_pi->NAT_info.Ip >> 8) & 0xff;
             da_entry.da.b[5] = (entry_pi->NAT_info.Ip >> 0) & 0xff;
     
     }

          
        
     switch (entry_pi->EditingType)
     {
     case 0:

         break;
     case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_VLAN:
     case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_QTAG:
     case MEA_EDITINGTYPE_IPCS_DL_IPinIP_Append_SGW:
     case MEA_EDITINGTYPE_IPCS_DL_UL_SGW_Swap_header:


         da_entry.da.s[0] = entry_pi->IPCS_DL_info.Da_eNB.s[0];
         da_entry.da.s[1] = entry_pi->IPCS_DL_info.Da_eNB.s[1];
         da_entry.da.s[2] = entry_pi->IPCS_DL_info.Da_eNB.s[2];
         break;

     case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_VLAN:
     case MEA_EDITINGTYPE_IPCS_UL_IPinIP_Extract_SGW:


         if (entry_pi->IPCS_UL_info.valid) {
             da_entry.da.s[0] = entry_pi->IPCS_UL_info.Da_next_hop_mac.s[0];
             da_entry.da.s[1] = entry_pi->IPCS_UL_info.Da_next_hop_mac.s[1];
             da_entry.da.s[2] = entry_pi->IPCS_UL_info.Da_next_hop_mac.s[2];
         }

         break;
     case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_QTAG:
         break;

     case MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent:   /* da_entry   Da_eNB*/
     case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN:
     case MEA_EDITINGTYPE_VPWS_DL_Swap_external_VLAN_TAG:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN_Swap_Internal_VLAN:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_Extract_QInQ:
     case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ:
         da_entry.da.s[0] = entry_pi->ETHCS_DL_info.Da_eNB.s[0];
         da_entry.da.s[1] = entry_pi->ETHCS_DL_info.Da_eNB.s[1];
         da_entry.da.s[2] = entry_pi->ETHCS_DL_info.Da_eNB.s[2];
         break;

     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS:
         break;

     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG:
         break;

     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Swap_External_TAG:
         break;

     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG_Swap_Internal_VLAN:
         break;

     case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QinQ:
         break;
     case MEA_EDITINGTYPE_VXLAN_header_Append_vlan:
     case MEA_EDITINGTYPE_VXLAN_header_Append_vlan_Extract_INNER_vlan:
     case MEA_EDITINGTYPE_VXLAN_header:
     case MEA_EDITINGTYPE_VXLAN_header_Extract_INNER_vlan:
     case MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_VXLAN_header:
     case MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_Extract_Inner_vlan_VXLAN_header:
     case MEA_EDITINGTYPE_IPSec_Append_Tunnel_VxLAN:
         if (entry_pi->VxLan_info.valid) {
             da_entry.da.s[0] = entry_pi->VxLan_info.vxlan_Da.s[0];
             da_entry.da.s[1] = entry_pi->VxLan_info.vxlan_Da.s[1];
             da_entry.da.s[2] = entry_pi->VxLan_info.vxlan_Da.s[2];
             
          }
     break;
     case MEA_EDITINGTYPE_NVGRE_Append_Tunnel:
     case MEA_EDITINGTYPE_NVGRE_Append_Tunnel_ADD_Outer_vlan:
        if (entry_pi->NVGre_info.valid == MEA_TRUE) {
            da_entry.da.s[0] = entry_pi->NVGre_info.Outer_da_MAC.s[0];
            da_entry.da.s[1] = entry_pi->NVGre_info.Outer_da_MAC.s[1];
            da_entry.da.s[2] = entry_pi->NVGre_info.Outer_da_MAC.s[2];
        }
    break;
     case MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE:
     case MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE_ADD_Outer_vlan:
         if (entry_pi->IPSec_info.valid == MEA_TRUE) 
         {
             da_entry.da.s[0] = entry_pi->NVGre_info.Outer_da_MAC.s[0];
             da_entry.da.s[1] = entry_pi->NVGre_info.Outer_da_MAC.s[1];
             da_entry.da.s[2] = entry_pi->NVGre_info.Outer_da_MAC.s[2];
         }
         break;




     case MEA_EDITINGTYPE_GRE_Append_Tunnel:
     case MEA_EDITINGTYPE_L2TP_Append_Tunnel:
         if (entry_pi->GRE_L2TP_info.valid == MEA_TRUE) {
             da_entry.da.s[0] = entry_pi->GRE_L2TP_info.Da.s[0];
             da_entry.da.s[1] = entry_pi->GRE_L2TP_info.Da.s[1];
             da_entry.da.s[2] = entry_pi->GRE_L2TP_info.Da.s[2];
         }
         break;

         


     } // end switch





	 
	if ( MEA_EHP_da_Table[id].valid) {

	    if (MEA_EHP_da_Table[id].hwUnit != hwUnit) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					 		  "%s - MEA_EHP_da_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
					         __FUNCTION__,
							 id,
							 MEA_EHP_da_Table[id].hwUnit,
						     hwUnit);
			return MEA_ERROR;
		}

		if ((MEA_EHP_da_Table[id].da.s[0] != da_entry.da.s[0]) ||
            (MEA_EHP_da_Table[id].da.s[1] != da_entry.da.s[1]) ||
            (MEA_EHP_da_Table[id].da.s[2] != da_entry.da.s[2])  ) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - da profile %d already exist value (0x%08x) with different da value (0x%04x%04x%04x)\n",
							  __FUNCTION__,
							  id,
							  MEA_EHP_da_Table[id].da.s[0],
							  MEA_EHP_da_Table[id].da.s[1],
							  MEA_EHP_da_Table[id].da.s[2],
							  da_entry.da);
			return MEA_ERROR;
		}


		MEA_EHP_da_Table[id].num_of_owners++;
		return MEA_OK;
    }


	if (mea_db_Create(unit_i,
		              MEA_EHP_da_db,
					  swId,
					  hwUnit,
					  &hwId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Create failed (EHP_da) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,swId,hwUnit);
		return MEA_ERROR;
	}



	 MEA_OS_memcpy(&(MEA_EHP_da_Table[swId]),
		           &da_entry,
				   sizeof(MEA_EHP_da_Table[0]));

	 return MEA_OK;

}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Modify_EHP_da>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Modify_EHP_da(MEA_Unit_t                      unit_i,
										MEA_EgressHeaderProc_Entry_dbt *entry_pi,
										MEA_Editing_t                   eth_id_i) 
{

	MEA_drv_ehp_da_dbt    da_entry;
	MEA_db_HwUnit_t           hwUnit;
	MEA_db_HwUnit_t           tmp_hwUnit;
	MEA_db_HwId_t             eth_hwId;
	MEA_db_HwId_t             hwId;
	MEA_db_SwId_t             swId;
// MEA_Editing_t             id;








	MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
		mea_drv_Get_EHP_ETH_db,
		eth_id_i,
		hwUnit,
		eth_hwId,
		return MEA_ERROR;)



		hwId =  (eth_hwId % MEA_BM_TBL_TYP_EHP_DA_LENGTH(hwUnit));
	for (tmp_hwUnit=0,swId=0;tmp_hwUnit<hwUnit;tmp_hwUnit++) {
		swId +=  (MEA_db_SwId_t)(MEA_BM_TBL_TYP_EHP_DA_LENGTH(tmp_hwUnit));
	}
	swId += (MEA_db_SwId_t)hwId;
// 	id = (MEA_Editing_t)swId;
//     if (id == 0){
// 
//     }


if (MEA_EHP_da_Table[swId].valid == MEA_TRUE &&
	(MEA_OS_maccmp(&entry_pi->martini_info.DA,&(MEA_EHP_da_Table[swId].da))==0 ))
{
	return MEA_OK;
}




	MEA_OS_memset((char*)&da_entry,0,sizeof(da_entry));
	da_entry.num_of_owners = MEA_EHP_da_Table[swId].num_of_owners;
	da_entry.hwUnit = MEA_EHP_da_Table[swId].hwUnit;
	da_entry.valid = MEA_EHP_da_Table[swId].valid;
	MEA_OS_memcpy(&da_entry.da,&entry_pi->martini_info.DA,sizeof(da_entry.da));

	//da_entry.editId = eth_id_i; // save the software


	if(MEA_EHP_da_Table[swId].valid == MEA_TRUE){

		if (MEA_EHP_da_Table[swId].hwUnit != hwUnit) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_EHP_da_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
				__FUNCTION__,
				swId,
				MEA_EHP_da_Table[swId].hwUnit,
				hwUnit);
			return MEA_ERROR;
		}


		if (mea_db_Delete(unit_i,
			MEA_EHP_da_db,
			swId,
			hwUnit) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s - mea_db_Delete failed (MEA_EHP_da_db) (swId=%d,hwUnit=%d) \n",
					__FUNCTION__,swId,hwUnit);
				return MEA_ERROR;
		}

		if (mea_db_Create(unit_i,
			MEA_EHP_da_db,
			swId,
			hwUnit,
			&hwId) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s - mea_db_Create failed (EHP_da) (swId=%d,hwUnit=%d) \n",
					__FUNCTION__,swId,hwUnit);
				return MEA_ERROR;
		}


	}

	MEA_OS_memcpy(&(MEA_EHP_da_Table[swId]),
		&da_entry,
		sizeof(MEA_EHP_da_Table[0]));

	return MEA_OK;

}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Delete_EHP_da>                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Delete_EHP_da(MEA_Unit_t               unit_i,
                                            MEA_Editing_t            eth_id_i)
{
	MEA_db_HwUnit_t hwUnit;
	MEA_db_SwId_t   swId;
	MEA_Editing_t   id;

#if 0
	//MEA_db_HwId_t   eth_hwId;
	//MEA_db_HwId_t   hwId;

	 MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
								   mea_drv_Get_EHP_ETH_db,
								   eth_id_i,
								   hwUnit,
								   eth_hwId,
								   return MEA_ERROR;)


     hwId =  (eth_hwId % MEA_BM_TBL_TYP_EHP_DA_LENGTH(hwUnit));
	 MEA_DRV_GET_SW_ID(unit_i,
		               mea_drv_Get_EHP_da_db,
					   hwUnit,
					   hwId,
					   swId,
					   return MEA_ERROR;)
#else
	 MEA_DRV_GET_HW_UNIT(unit_i,
					     hwUnit,
 					     return MEA_ERROR;)
     swId = eth_id_i;
#endif
     id = (MEA_Editing_t)swId;



	if (id >= MEA_MAX_NUM_OF_EHP_DA) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id,MEA_MAX_NUM_OF_EHP_DA);
		return MEA_ERROR;
	}

	if ( ! MEA_EHP_da_Table[id].valid ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_da_Table[%d].valid == MEA_FALSE \n",
			             __FUNCTION__,id);
		return MEA_ERROR;
	}


	if ( MEA_EHP_da_Table[id].hwUnit != hwUnit ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_da_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
			             __FUNCTION__,
						 id,
						 MEA_EHP_da_Table[id].hwUnit,
						 hwUnit);
		return MEA_ERROR;
	}

	if ( MEA_EHP_da_Table[id].num_of_owners == 0 ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_da_Table[%d].num_of_owners == 0 \n",
			             __FUNCTION__,id);
		return MEA_ERROR;
	}





	if ( MEA_EHP_da_Table[id].num_of_owners > 1 ) {
		MEA_EHP_da_Table[id].num_of_owners--;
		return MEA_OK;
	}



	MEA_OS_memset(&(MEA_EHP_da_Table[id]),
		          0,
				  sizeof(MEA_EHP_da_Table[0]));


	if (mea_db_Delete(unit_i,
	                  MEA_EHP_da_db,
				      swId,
				      hwUnit) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Delete failed (EHP_da) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,swId,hwUnit);
		return MEA_ERROR;
	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Get_EHP_da>                                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_EHP_da(MEA_Unit_t                 unit_i,
                                  MEA_Editing_t              id_i,
                                  MEA_drv_ehp_da_dbt *entry_po)
{

	if (id_i >= MEA_MAX_NUM_OF_EHP_DA) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_DA);
		return MEA_ERROR;
	}

	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(entry_po,
		          &(MEA_EHP_da_Table[id_i]),
				  sizeof(*entry_po));
	
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Create_EHP_sa_lss>                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Create_EHP_sa_lss(MEA_Unit_t                      unit_i,
										    MEA_EgressHeaderProc_Entry_dbt *entry_pi,
											MEA_Editing_t                   eth_id_i) {
 
     MEA_drv_ehp_sa_lss_dbt    sa_lss_entry;
	 MEA_db_HwUnit_t           hwUnit;
	 //MEA_db_HwUnit_t           tmp_hwUnit;
	 MEA_db_HwId_t             eth_hwId;
	 MEA_db_HwId_t             hwId;
	 MEA_db_SwId_t             swId;
	 MEA_Editing_t             id;
     MEA_Uint32                configure_val;
     MEA_Uint32                value_temp;



	 MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
								   mea_drv_Get_EHP_ETH_db,
								   eth_id_i,
								   hwUnit,
								   eth_hwId,
								   return MEA_ERROR;)


#if 0
     hwId =  (eth_hwId % MEA_BM_TBL_TYP_EHP_SA_LSS_LENGTH(hwUnit));
	 for (tmp_hwUnit=0,swId=0;tmp_hwUnit<hwUnit;tmp_hwUnit++) {
		 swId +=  (MEA_db_SwId_t)(MEA_BM_TBL_TYP_EHP_SA_LSS_LENGTH(tmp_hwUnit));
	 }
	 swId += (MEA_db_SwId_t)hwId;
#else
     hwId = eth_hwId;
     swId = eth_id_i;
#endif
	 id = (MEA_Editing_t)swId;


	 MEA_OS_memset((char*)&sa_lss_entry,0,sizeof(sa_lss_entry));
	 sa_lss_entry.num_of_owners = 1;
	 sa_lss_entry.hwUnit = hwUnit;
	 sa_lss_entry.valid = MEA_TRUE;
     

     if (entry_pi->NAT_info.valid == MEA_TRUE){
         entry_pi->martini_info.SA.b[0] = entry_pi->NAT_info.Da.b[0];
         entry_pi->martini_info.SA.b[1] = entry_pi->NAT_info.Da.b[1];
         entry_pi->martini_info.SA.b[2] = entry_pi->NAT_info.Da.b[2];
         entry_pi->martini_info.SA.b[3] = entry_pi->NAT_info.Da.b[3];
         entry_pi->martini_info.SA.b[4] = entry_pi->NAT_info.Da.b[4];
         entry_pi->martini_info.SA.b[5] = entry_pi->NAT_info.Da.b[5];


   }
     sa_lss_entry.sa_lss = (entry_pi->martini_info.SA.s[1] << 16) | (entry_pi->martini_info.SA.s[2] << 0);
     

     //value_temp = (entry_pi->martini_info.SA.s[1] << 0);
     value_temp = 0;
     value_temp = (entry_pi->martini_info.SA.b[2] << 8);
     value_temp |= (entry_pi->martini_info.SA.b[3] << 0);

        configure_val = value_temp << 16;
        value_temp = 0;
        //value_temp = (entry_pi->martini_info.SA.s[2] << 0);
        value_temp = (entry_pi->martini_info.SA.b[4] << 8);
        value_temp |= (entry_pi->martini_info.SA.b[5] << 0);


        configure_val |= value_temp;
        sa_lss_entry.sa_lss = configure_val;

         
         /*Add TE_ID */
         switch (entry_pi->EditingType)
         {

         case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_VLAN:
         case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_QTAG:
         case MEA_EDITINGTYPE_IPCS_DL_IPinIP_Append_SGW:
         case MEA_EDITINGTYPE_IPCS_DL_UL_SGW_Swap_header:
         
             // The TEID will be set on the AtmDatabase
             break;
         case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_VLAN:
         case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_QTAG:
         case MEA_EDITINGTYPE_IPCS_UL_IPinIP_Extract_SGW:
             if (entry_pi->IPCS_UL_info.valid == MEA_TRUE){
                 
                 //value_temp = (entry_pi->IPCS_UL_info.Sa_host_mac.s[1] << 0);
                 value_temp = 0;
                 value_temp = (entry_pi->IPCS_UL_info.Sa_host_mac.b[2]) << 8;
                 value_temp |= (entry_pi->IPCS_UL_info.Sa_host_mac.b[3]) << 0;
                 configure_val = value_temp << 16;

                 //value_temp = (entry_pi->IPCS_UL_info.Sa_host_mac.s[2] << 0);
                 value_temp = 0;
                 value_temp = (entry_pi->IPCS_UL_info.Sa_host_mac.b[4]) << 8;
                 value_temp |= (entry_pi->IPCS_UL_info.Sa_host_mac.b[5]) << 0;;
                 configure_val |= value_temp;
                 sa_lss_entry.sa_lss = configure_val;
                 //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "sa_lss_entry.sa_lss 0x%08x\n", sa_lss_entry.sa_lss);






             }
             break;
         case MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent: /*sa_lss  */
         case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN:
         case MEA_EDITINGTYPE_VPWS_DL_Swap_external_VLAN_TAG:
         case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN_Swap_Internal_VLAN:
         case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent:
         case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN:
         case MEA_EDITINGTYPE_VPWS_DL_Extract_Extract_QInQ:
         case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ:

             configure_val = entry_pi->ETHCS_DL_info.dst_UE_Ip;
             sa_lss_entry.sa_lss = configure_val;
             break;
         case MEA_EDITINGTYPE_VXLAN_header_Append_vlan:
         case MEA_EDITINGTYPE_VXLAN_header_Append_vlan_Extract_INNER_vlan:
         case MEA_EDITINGTYPE_VXLAN_header:
         case MEA_EDITINGTYPE_VXLAN_header_Extract_INNER_vlan:
         case MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_VXLAN_header:
         case MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_Extract_Inner_vlan_VXLAN_header:
         case MEA_EDITINGTYPE_IPSec_Append_Tunnel_VxLAN:
             if (entry_pi->VxLan_info.valid) {
                 sa_lss_entry.sa_lss = entry_pi->VxLan_info.l4_dest_Port;
                 sa_lss_entry.sa_lss |= entry_pi->VxLan_info.l4_src_Port << 16;
             }
             
             break;
         case MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE:
         case MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE_ADD_Outer_vlan:
             if (entry_pi->IPSec_info.valid == MEA_TRUE) {
                 sa_lss_entry.sa_lss = entry_pi->IPSec_info.IPSec_DST_IP;
             }
             break;
         case MEA_EDITINGTYPE_IPSec_Append_Tunnel_ONLY:
		 case MEA_EDITINGTYPE_IPSec_Append_Tunnel_Extract_vlan:
             if (entry_pi->IPSec_info.valid == MEA_TRUE) {
                 sa_lss_entry.sa_lss = entry_pi->IPSec_info.IPSec_DST_IP;
                     
             }
         case MEA_EDITINGTYPE_GRE_Append_Tunnel:
         case MEA_EDITINGTYPE_L2TP_Append_Tunnel:
             if (entry_pi->GRE_L2TP_info.valid == MEA_TRUE) {

                 value_temp = 0;
                 value_temp = (entry_pi->GRE_L2TP_info.Sa.b[2] << 8);
                 value_temp |= (entry_pi->GRE_L2TP_info.Sa.b[3] << 0);

                 configure_val = value_temp << 16;
                 value_temp = 0;
                 //value_temp = (entry_pi->martini_info.SA.s[2] << 0);
                 value_temp = (entry_pi->GRE_L2TP_info.Sa.b[4] << 8);
                 value_temp |= (entry_pi->GRE_L2TP_info.Sa.b[5] << 0);


                 configure_val |= value_temp;
                 sa_lss_entry.sa_lss = configure_val;

                
             }
             break;



         } // end switch
    
    



     


	 
	if ( MEA_EHP_sa_lss_Table[id].valid) {

	    if (MEA_EHP_sa_lss_Table[id].hwUnit != hwUnit) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					 		  "%s - MEA_EHP_sa_lss_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
					         __FUNCTION__,
							 id,
							 MEA_EHP_sa_lss_Table[id].hwUnit,
						     hwUnit);
			return MEA_ERROR;
		}

		if (MEA_EHP_sa_lss_Table[id].sa_lss != sa_lss_entry.sa_lss) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - sa_lss profile %d already exist value (0x%08x) with different sa_lss value (0x%08x)\n",
							  __FUNCTION__,
							  id,
							  MEA_EHP_sa_lss_Table[id].sa_lss,
							  sa_lss_entry.sa_lss);
			return MEA_ERROR;
		}


		MEA_EHP_sa_lss_Table[id].num_of_owners++;
		return MEA_OK;
    }


	if (mea_db_Create(unit_i,
		              MEA_EHP_sa_lss_db,
					  swId,
					  hwUnit,
					  &hwId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Create failed (EHP_sa_lss) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,swId,hwUnit);
		return MEA_ERROR;
	}



	 MEA_OS_memcpy(&(MEA_EHP_sa_lss_Table[swId]),
		           &sa_lss_entry,
				   sizeof(MEA_EHP_sa_lss_Table[0]));

	 return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Delete_EHP_sa_lss>                                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Delete_EHP_sa_lss(MEA_Unit_t               unit_i,
                                            MEA_Editing_t            eth_id_i)
{
	MEA_db_HwUnit_t hwUnit;
	MEA_db_SwId_t   swId;
	MEA_Editing_t   id;

#if 0
	MEA_db_HwId_t   eth_hwId;
	MEA_db_HwId_t   hwId;

	 MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
								   mea_drv_Get_EHP_ETH_db,
								   eth_id_i,
								   hwUnit,
								   eth_hwId,
								   return MEA_ERROR;)


     hwId =  (eth_hwId % MEA_BM_TBL_TYP_EHP_SA_LSS_LENGTH(hwUnit));
	 MEA_DRV_GET_SW_ID(unit_i,
		               mea_drv_Get_EHP_sa_lss_db,
					   hwUnit,
					   hwId,
					   swId,
					   return MEA_ERROR;)
#else
	 MEA_DRV_GET_HW_UNIT(unit_i,
						 hwUnit,
  					     return MEA_ERROR;)
     swId = eth_id_i;
#endif
     id = (MEA_Editing_t)swId;



	if (id >= MEA_MAX_NUM_OF_EHP_SA_LSS) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id,MEA_MAX_NUM_OF_EHP_SA_LSS);
		return MEA_ERROR;
	}

	if ( ! MEA_EHP_sa_lss_Table[id].valid ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_Sa_lsw_Table[%d].valid == MEA_FALSE \n",
			             __FUNCTION__,id);
		return MEA_ERROR;
	}


	if ( MEA_EHP_sa_lss_Table[id].hwUnit != hwUnit ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_sa_lss_Table[%d].hwUnit (%d) != current hwUnit (%d) \n",
			             __FUNCTION__,
						 id,
						 MEA_EHP_sa_lss_Table[id].hwUnit,
						 hwUnit);
		return MEA_ERROR;
	}

	if ( MEA_EHP_sa_lss_Table[id].num_of_owners == 0 ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - MEA_EHP_sa_lss_Table[%d].num_of_owners == 0 \n",
			             __FUNCTION__,id);
		return MEA_ERROR;
	}





	if ( MEA_EHP_sa_lss_Table[id].num_of_owners > 1 ) {
		MEA_EHP_sa_lss_Table[id].num_of_owners--;
		return MEA_OK;
	}



	MEA_OS_memset(&(MEA_EHP_sa_lss_Table[id]),
		          0,
				  sizeof(MEA_EHP_sa_lss_Table[0]));


	if (mea_db_Delete(unit_i,
	                  MEA_EHP_sa_lss_db,
				      swId,
				      hwUnit) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Delete failed (EHP_sa_lss) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,swId,hwUnit);
		return MEA_ERROR;
	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             <mea_drv_Get_EHP_sa_lss>                                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_EHP_sa_lss(MEA_Unit_t                 unit_i,
                                  MEA_Editing_t              id_i,
                                  MEA_drv_ehp_sa_lss_dbt *entry_po)
{

	if (id_i >= MEA_MAX_NUM_OF_EHP_SA_LSS) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				 		  "%s - id_i (%d) >= max value (%d) \n",
			             __FUNCTION__,id_i,MEA_MAX_NUM_OF_EHP_SA_LSS);
		return MEA_ERROR;
	}

	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(entry_po,
		          &(MEA_EHP_sa_lss_Table[id_i]),
				  sizeof(*entry_po));
	
	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_is_reserved_ehp_id>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool mea_drv_is_reserved_ehp_id(MEA_Editing_t id_i)
{
	if ( id_i == 0 )
	{
		return MEA_TRUE;
	}
	else
		return MEA_FALSE;
}


MEA_Status  MEA_API_Get_NumOfEdit(MEA_Unit_t unit_i, MEA_EgressHeaderProc_numInfo_dbt *Info)
{
    MEA_Uint32 id;
    MEA_Uint32 countEdit = 0;

    if (Info == NULL) {
        return MEA_ERROR;
    }


    for (id = 1; id < MEA_MAX_NUM_OF_EDIT_ID; (id)++)
    {
       

        if (MEA_EHP_Table[id].valid == MEA_FALSE)
        {
           continue;
        }
        countEdit++;

    }

    Info->TotalEdit = MEA_MAX_NUM_OF_EDIT_ID;
    Info->usedEdit = countEdit;

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <mea_drv_get_free_editing_id>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
 MEA_Status MEA_API_Get_EditingFree_id(MEA_Unit_t  unit_i,
	MEA_Editing_t                  *id_o) 
 {
	if (id_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - id_o == NULL \n",
			__FUNCTION__);
		return MEA_ERROR;
	}
	/* need to change and work with link List on next version*/
	for (*id_o = 1; *id_o < MEA_MAX_NUM_OF_EDIT_ID; (*id_o)++)
	{
		if (MEA_EHP_Table[*id_o].valid == MEA_FALSE)
		{
			return MEA_OK;
		}
	}


	return MEA_ERROR;
}


static MEA_Status mea_drv_get_free_editing_id( MEA_EgressHeaderProc_Entry_dbt *entry_i,
	                                           MEA_Editing_t                  *id_o)
{

	for ( *id_o = 1; *id_o < MEA_MAX_NUM_OF_EDIT_ID; (*id_o)++ )
	{
		if ( mea_drv_is_reserved_ehp_id(*id_o) )
			continue;

		if ( MEA_EHP_Table[*id_o].valid == MEA_FALSE )
		{
			return MEA_OK;
		}
	}

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - no free EHP entry\n",
                      __FUNCTION__);
	return MEA_ERROR;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_create_ehp_entry_db>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_create_ehp_entry_db(MEA_Unit_t                      unit_i,
										      MEA_EgressHeaderProc_Entry_dbt *entry_i, 
										      MEA_Editing_t                   id_i) 
{

	MEA_db_HwUnit_t hwUnit;
	MEA_db_HwId_t   hwId;

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);

	
	

        if (id_i) hwId = id_i;
	    else hwId = MEA_DB_GENERATE_NEW_ID;

	
	if (mea_db_Create(unit_i,
		              MEA_EHP_ETH_db,
					  (MEA_db_SwId_t)id_i,
					  hwUnit,
					  &hwId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Create failed (EHP ETH) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,id_i,hwUnit);
		return MEA_ERROR;
	}


	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_delete_ehp_entry_db>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_delete_ehp_entry_db(MEA_Unit_t      unit_i,
										      MEA_Editing_t   id_i) 
{

	MEA_db_HwUnit_t hwUnit;

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);

	/* Delete EHP ETH HwId */
	if (mea_db_Delete(unit_i,
		              MEA_EHP_ETH_db,
					  (MEA_db_SwId_t)id_i,
					  hwUnit) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Delete failed (EHP ETH) (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,id_i,hwUnit);
		return MEA_ERROR;
	}


	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_ehp_add_owner>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ehp_add_owner(MEA_Editing_t id)
{
    if (mea_drv_IsEditingIdInRange(id) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

	if ( MEA_EHP_Table[id].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - none existing EHP entry with ID %d\n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

	MEA_EHP_Table[id].num_of_owners++;

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_ehp_delete_owner>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_ehp_delete_owner(MEA_Unit_t    unit_i,
										   MEA_Editing_t id_i)
{
	MEA_EgressHeaderProc_Entry_dbt* entry;

    if (mea_drv_IsEditingIdInRange(id_i) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }

	if ( MEA_EHP_Table[id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - none existing EHP entry with ID %d\n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }

	MEA_EHP_Table[id_i].num_of_owners--;

	if ( MEA_EHP_Table[id_i].num_of_owners == 0 )
	{

        if (MEA_EHP_Table[id_i].editing_info.eth_info.mapping_enable) {
            if (MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             MEA_EHP_Table[id_i].editing_info.eth_info.mapping_id
                                                             ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_DelOwner_EditingMappingProfile_Entry (eth) for id %d failed (id_i=%d)\n",
                                  __FUNCTION__,
                                  MEA_EHP_Table[id_i].editing_info.eth_info.mapping_id,
                                  id_i);
                MEA_EHP_Table[id_i].num_of_owners++;
                return MEA_ERROR;                                                    
            }
        }
        if (MEA_EHP_Table[id_i].editing_info.atm_info.mapping_enable) {
            if (MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             MEA_EHP_Table[id_i].editing_info.atm_info.mapping_id
                                                             ) != MEA_OK) {     
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_DelOwner_EditingMappingProfile_Entry (atm) for id %d failed (id_i=%d)\n",
                                  __FUNCTION__,
                                  MEA_EHP_Table[id_i].editing_info.atm_info.mapping_id,
                                  id_i);
                MEA_EHP_Table[id_i].num_of_owners++;
                return MEA_ERROR;                                                    
            }
        }
      
            if (mea_drv_Delete_EHP_profile2Id(unit_i,
                id_i) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_drv_Delete_EHP_profile2Id failed (id=%d)\n",
                        __FUNCTION__,id_i);
                    return MEA_ERROR;
            }
        
          if (MEA_EHP_Table[id_i].editing_info.fragmentSession_info.valid) {
          if (mea_drv_Delete_EHP_fragmentSession(unit_i,
                id_i) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_drv_Delete_EHP_fragmentSession failed (id=%d)\n",
                        __FUNCTION__,id_i);
                    return MEA_ERROR;
          }
          }
          if (MEA_EHP_Table[id_i].editing_info.pw_control_info.valid) {
              if (mea_drv_Delete_EHP_pw_control(unit_i,
                  id_i) != MEA_OK) {
                      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Delete_EHP_pw_control failed (id=%d)\n",
                          __FUNCTION__,id_i);
                      return MEA_ERROR;
              }
          }
          if (MEA_EHP_Table[id_i].editing_info.mpls_label_info.valid) {
              if (mea_drv_Delete_EHP_mpls_label(unit_i,
                  id_i) != MEA_OK) {
                      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Delete_EHP_mpls_label failed (id=%d)\n",
                          __FUNCTION__,id_i);
                      return MEA_ERROR;
              }
          }


		MEA_OS_memset(&(MEA_EHP_Table[id_i]), 0, sizeof(MEA_drv_ehp_dbt));
		MEA_EHP_Table[id_i].valid = MEA_FALSE;

		entry = &(MEA_EHP_Table[id_i].editing_info);

        /* fill default values for db shadow entry */
        entry->eth_info.val.all        = 0x0;
        entry->eth_info.stamp_color    = MEA_EGRESS_HEADER_PROC_NO_STAMP_COLOR;
        entry->eth_info.stamp_priority = MEA_EGRESS_HEADER_PROC_NO_STAMP_PRIORITY;
        entry->eth_info.command        = MEA_EGRESS_HEADER_PROC_CMD_TRANS;

        if (mea_drv_EHP_UpdateHw (unit_i,
			                      id_i)  != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_Set_EgressHeaderProc_UpdateHw(%d,...) failed\n",
                              __FUNCTION__,id_i);
			return MEA_ERROR;
		}

		if (mea_drv_Delete_EHP_da(unit_i,
			                          id_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Delete_EHP_da failed (id=%d)\n",
                              __FUNCTION__,id_i);
			return MEA_ERROR;
		}

		if (mea_drv_Delete_EHP_sa_lss(unit_i,
			                          id_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Delete_EHP_sa_lss failed (id=%d)\n",
                              __FUNCTION__,id_i);
			return MEA_ERROR;
		}

		if (mea_drv_Delete_EHP_profileId(unit_i,
			                             id_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Delete_EHP_profileId failed (id=%d)\n",
                              __FUNCTION__,id_i);
			return MEA_ERROR;
		}
        
		if (mea_drv_delete_ehp_entry_db(unit_i,
			                            id_i) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_delete_ehp_entry_db failed (id=%d)\n",
							  __FUNCTION__,
							  id_i);
			return MEA_ERROR;
		}






	}


	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_create_drv_force_id_editing>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_create_drv_force_id_editing(MEA_Unit_t unit_i,
											   MEA_EgressHeaderProc_Entry_dbt *entry_i,
	                                            MEA_Editing_t                  *id_o)
{
	MEA_db_HwUnit_t hwUnit;
    MEA_Bool        editingMappingProfile_eth_addOwner_done = MEA_FALSE;
    MEA_Bool        editingMappingProfile_atm_addOwner_done = MEA_FALSE;
    MEA_Bool        editingMappingProfile_eth_delOwner_done = MEA_FALSE;
    MEA_Bool        editingMappingProfile_atm_delOwner_done = MEA_FALSE;
    MEA_Uint32      editingMappingProfile_eth_delOwner_id   = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Uint32      editingMappingProfile_atm_delOwner_id   = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Bool        Create_profile2Id_done  =MEA_FALSE;
    MEA_Bool        Create_fragmant_done    =MEA_FALSE;
    MEA_Bool        Create_pw_control_done  =MEA_FALSE;

    if (entry_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - entry_i == NULL\n",
                      __FUNCTION__);
	    return MEA_ERROR;  
    }

    if (id_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - id_o == NULL\n",
                      __FUNCTION__);
	    return MEA_ERROR;  
    }

    if ((*id_o == 0) || (*id_o > MEA_MAX_NUM_OF_EDIT_ID)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -  EHP id (%d) is out of range (1..%d)\n",
                         __FUNCTION__,*id_o,MEA_MAX_NUM_OF_EDIT_ID);
	   return MEA_ERROR;
    }

    /*check if the id is free*/
    if (MEA_EHP_Table[*id_o].valid == MEA_TRUE ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -  EHP id %d is not free \n",__FUNCTION__,
                         *id_o);
       return MEA_ERROR;
    }

	/* Create db EHP basic ETH and ATM */
    if (mea_drv_create_ehp_entry_db(unit_i,
		                            entry_i,
									*id_o) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_create_ehp_entry_db failed (id_io=%d) \n",
						  __FUNCTION__,
						  *id_o);
		return MEA_ERROR;
	}

	/* Create db EHP da */
    if (mea_drv_Create_EHP_da(unit_i,
		                          entry_i,
								  *id_o) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_da failed (id=%d) \n",
						  __FUNCTION__,
						  *id_o);
	    mea_drv_delete_ehp_entry_db(unit_i,*id_o);
		return MEA_ERROR;
	}

	/* Create db EHP sa_lss */
    if (mea_drv_Create_EHP_sa_lss(unit_i,
		                          entry_i,
								  *id_o) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_sa_lss failed (id=%d) \n",
						  __FUNCTION__,
						  *id_o);
        mea_drv_Delete_EHP_da(unit_i,*id_o);
	    mea_drv_delete_ehp_entry_db(unit_i,*id_o);
		return MEA_ERROR;
	}

	/* Create db EHP profileId and its dependencies */
    if (mea_drv_Create_EHP_profileId(unit_i,
		                             entry_i,
									 *id_o) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_profileId failed (id=%d) \n",
						  __FUNCTION__,
						  *id_o);
        mea_drv_Delete_EHP_da(unit_i,*id_o);
        mea_drv_Delete_EHP_sa_lss(unit_i,*id_o);
	    mea_drv_delete_ehp_entry_db(unit_i,*id_o);
		return MEA_ERROR;
	}

	/* Create db EHP profile2Id and its dependencies */
   
        if (mea_drv_Create_EHP_profile2Id(unit_i,
		                                  entry_i,
							   	          *id_o) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - mea_drv_Create_EHP_profile2Id failed (id=%d) \n",
						      __FUNCTION__,
						      *id_o);
            mea_drv_Delete_EHP_profileId(unit_i,*id_o);
            mea_drv_Delete_EHP_da(unit_i,*id_o);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_o);
	        mea_drv_delete_ehp_entry_db(unit_i,*id_o);
		    return MEA_ERROR;
	    }
        Create_profile2Id_done=MEA_TRUE;
  
    if(entry_i->fragmentSession_info.valid == MEA_TRUE){
        if(mea_drv_Create_EHP_fragmentSession(unit_i,entry_i,*id_o) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Create_EHP_fragmentSession failed (id=%d) \n",
                    __FUNCTION__,
                    *id_o);
                if(Create_fragmant_done){ mea_drv_Delete_EHP_fragmentSession(unit_i,*id_o);}
                if(Create_profile2Id_done){mea_drv_Delete_EHP_profile2Id(unit_i,*id_o);}
                mea_drv_Delete_EHP_profileId(unit_i,*id_o);
                mea_drv_Delete_EHP_da(unit_i,*id_o);
                mea_drv_Delete_EHP_sa_lss(unit_i,*id_o);
                mea_drv_delete_ehp_entry_db(unit_i,*id_o);
                return MEA_ERROR;
        }
        Create_fragmant_done=MEA_TRUE;

    }
    if(entry_i->pw_control_info.valid == MEA_TRUE){
        if(mea_drv_Create_EHP_pw_control(unit_i,entry_i,*id_o) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Create_EHP_profile2Id failed (id=%d) \n",
                __FUNCTION__,
                *id_o);
            if(Create_fragmant_done){ mea_drv_Delete_EHP_fragmentSession(unit_i,*id_o);}
            if(Create_profile2Id_done){mea_drv_Delete_EHP_profile2Id(unit_i,*id_o);}
            mea_drv_Delete_EHP_profileId(unit_i,*id_o);
            mea_drv_Delete_EHP_da(unit_i,*id_o);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_o);
            mea_drv_delete_ehp_entry_db(unit_i,*id_o);
            return MEA_ERROR;
        }
        Create_pw_control_done=MEA_TRUE;

    }
    /* Add owner to editingMappingProfile (eth) */
    if ((entry_i->eth_info.mapping_enable) &&
        ((!MEA_EHP_Table[*id_o].valid) ||
         ((MEA_EHP_Table[*id_o].editing_info.eth_info.mapping_enable) &&
          (MEA_EHP_Table[*id_o].editing_info.eth_info.mapping_id != entry_i->eth_info.mapping_id)))) {

        if (MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         (entry_i->eth_info.mapping_id)
                                                         ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_AddOwner_EditingMappingProfile_Entry (eth) to id %d (*id_o=%d)\n",
                              __FUNCTION__,
                              entry_i->eth_info.mapping_id,
                              *id_o);

            if(Create_pw_control_done){ mea_drv_Delete_EHP_pw_control(unit_i,*id_o);}
            if(Create_fragmant_done){ mea_drv_Delete_EHP_fragmentSession(unit_i,*id_o);}
            if(Create_profile2Id_done){mea_drv_Delete_EHP_profile2Id(unit_i,*id_o);}
            mea_drv_Delete_EHP_profileId(unit_i,*id_o);
            mea_drv_Delete_EHP_da(unit_i,*id_o);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_o);
            mea_drv_delete_ehp_entry_db(unit_i,*id_o);
            return MEA_ERROR;
        }
        editingMappingProfile_eth_addOwner_done=MEA_TRUE;
    }


    //////////////////////////////////////////////////////////////////////////

    /* Add owner to editingMappingProfile (atm) */
    if ((entry_i->atm_info.mapping_enable) &&
        ((!MEA_EHP_Table[*id_o].valid) ||
         ((MEA_EHP_Table[*id_o].editing_info.atm_info.mapping_enable) &&
          (MEA_EHP_Table[*id_o].editing_info.atm_info.mapping_id != entry_i->atm_info.mapping_id)))) {

        if (MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         (entry_i->atm_info.mapping_id)
                                                         ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_AddOwner_EditingMappingProfile_Entry (atm) to id %d (*id_o=%d)\n",
                              __FUNCTION__,
                              entry_i->atm_info.mapping_id,
                              *id_o);
            if (editingMappingProfile_eth_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                            (MEA_EditingMappingProfile_Id_t)entry_i->eth_info.mapping_id);
            }
            if(Create_pw_control_done){ mea_drv_Delete_EHP_pw_control(unit_i,*id_o);}
            if(Create_fragmant_done){ mea_drv_Delete_EHP_fragmentSession(unit_i,*id_o);}
            if(Create_profile2Id_done){mea_drv_Delete_EHP_profile2Id(unit_i,*id_o);}
            mea_drv_Delete_EHP_profileId(unit_i,*id_o);
            mea_drv_Delete_EHP_da(unit_i,*id_o);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_o);
            mea_drv_delete_ehp_entry_db(unit_i,*id_o);
            return MEA_ERROR;
        }
        editingMappingProfile_atm_addOwner_done = MEA_TRUE;
    }


    /* Del owner to editingMappingProfile (eth) */
    if ((MEA_EHP_Table[*id_o].valid) &&
        (MEA_EHP_Table[*id_o].editing_info.eth_info.mapping_enable) &&
        ((!entry_i->eth_info.mapping_enable) ||
         ((entry_i->eth_info.mapping_enable) &&
          (MEA_EHP_Table[*id_o].editing_info.eth_info.mapping_id != entry_i->eth_info.mapping_id)))) {
        if (MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         (MEA_EHP_Table[*id_o].editing_info.eth_info.mapping_id)
                                                         ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_AddOwner_EditingMappingProfile_Entry (eth) to id %d (*id_o=%d)\n",
                              __FUNCTION__,
                              MEA_EHP_Table[*id_o].editing_info.eth_info.mapping_id,
                              *id_o);
            if (editingMappingProfile_atm_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->atm_info.mapping_id);
            }
            if (editingMappingProfile_eth_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->eth_info.mapping_id);
            }
            if(Create_pw_control_done){ mea_drv_Delete_EHP_pw_control(unit_i,*id_o);}
            if(Create_fragmant_done){ mea_drv_Delete_EHP_fragmentSession(unit_i,*id_o);}
            if(Create_profile2Id_done){mea_drv_Delete_EHP_profile2Id(unit_i,*id_o);}
            mea_drv_Delete_EHP_profileId(unit_i,*id_o);
            mea_drv_Delete_EHP_da(unit_i,*id_o);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_o);
            mea_drv_delete_ehp_entry_db(unit_i,*id_o);
            return MEA_ERROR;
        }
        editingMappingProfile_eth_delOwner_done = MEA_TRUE;
        editingMappingProfile_eth_delOwner_id   = MEA_EHP_Table[*id_o].editing_info.eth_info.mapping_id;
    }

    /* Del owner to editingMappingProfile (atm) */
    if ((MEA_EHP_Table[*id_o].valid) &&
        (MEA_EHP_Table[*id_o].editing_info.atm_info.mapping_enable) &&
        ((!entry_i->atm_info.mapping_enable) ||
         ((entry_i->atm_info.mapping_enable) &&
          (MEA_EHP_Table[*id_o].editing_info.atm_info.mapping_id != entry_i->eth_info.mapping_id)))) {
        if (MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         (MEA_EHP_Table[*id_o].editing_info.atm_info.mapping_id)
                                                         ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_AddOwner_EditingMappingProfile_Entry (atm) to id %d (*id_o=%d)\n",
                              __FUNCTION__,
                              MEA_EHP_Table[*id_o].editing_info.eth_info.mapping_id,
                              *id_o);
            if (editingMappingProfile_eth_delOwner_done) {
                MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             editingMappingProfile_eth_delOwner_id);
            }
            if (editingMappingProfile_atm_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->atm_info.mapping_id);
            }
            if (editingMappingProfile_eth_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->eth_info.mapping_id);
            }
            if(Create_pw_control_done){ mea_drv_Delete_EHP_pw_control(unit_i,*id_o);}
            if(Create_fragmant_done){ mea_drv_Delete_EHP_fragmentSession(unit_i,*id_o);}
            if(Create_profile2Id_done){mea_drv_Delete_EHP_profile2Id(unit_i,*id_o);}
            mea_drv_Delete_EHP_profileId(unit_i,*id_o);
            mea_drv_Delete_EHP_da(unit_i,*id_o);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_o);
            mea_drv_delete_ehp_entry_db(unit_i,*id_o);
            return MEA_ERROR;
        }
        editingMappingProfile_atm_delOwner_done = MEA_TRUE;
        editingMappingProfile_atm_delOwner_id   = MEA_EHP_Table[*id_o].editing_info.eth_info.mapping_id;
    }
    

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);



    /* Update Software shadow */
	MEA_OS_memset((char*)(&(MEA_EHP_Table[*id_o])),
		          0,
				  sizeof(MEA_EHP_Table[0]));
    MEA_EHP_Table[*id_o].valid = MEA_TRUE; /*NEW */
    MEA_EHP_Table[*id_o].hwUnit = hwUnit;
    MEA_EHP_Table[*id_o].num_of_owners=1;
    MEA_OS_memcpy(&(MEA_EHP_Table[*id_o].editing_info), 
		          entry_i, 
				  sizeof(MEA_EHP_Table[0].editing_info));

    /* Update the hardware with the values from the shadow */
    if (mea_drv_EHP_UpdateHw (unit_i,
		                      *id_o) != MEA_OK) 
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Update Hardware failed "
                          "(index=%d)\n",
                          __FUNCTION__,*id_o);
        if (editingMappingProfile_atm_delOwner_done) {
            MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         editingMappingProfile_atm_delOwner_id);
        }
        if (editingMappingProfile_eth_delOwner_done) {
            MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         editingMappingProfile_eth_delOwner_id);
        }
        if (editingMappingProfile_atm_addOwner_done) {
            MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)entry_i->atm_info.mapping_id);
        }
        if (editingMappingProfile_eth_addOwner_done) {
            MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)entry_i->eth_info.mapping_id);
        }
       // MEA_EHP_Table[*id_o].valid = MEA_FALSE;
        if(Create_pw_control_done){ mea_drv_Delete_EHP_pw_control(unit_i,*id_o);}
        if(Create_fragmant_done){ mea_drv_Delete_EHP_fragmentSession(unit_i,*id_o);}
        if(Create_profile2Id_done){mea_drv_Delete_EHP_profile2Id(unit_i,*id_o);}
        mea_drv_Delete_EHP_profileId(unit_i,*id_o);
        mea_drv_Delete_EHP_da(unit_i,*id_o);
        mea_drv_Delete_EHP_sa_lss(unit_i,*id_o);
        mea_drv_delete_ehp_entry_db(unit_i,*id_o);
         MEA_EHP_Table[*id_o].valid = MEA_FALSE;
        return MEA_ERROR; 
    }


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_create_ehp_entry>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_create_ehp_entry(MEA_Unit_t unit_i,
										   MEA_EgressHeaderProc_Entry_dbt *entry_i, 
										   MEA_Editing_t                  *id_io,
										   MEA_Bool                        force_i)
{

	MEA_db_HwUnit_t hwUnit;
	MEA_Uint32 num_of_owners;
    MEA_Bool        editingMappingProfile_eth_addOwner_done = MEA_FALSE;
    MEA_Bool        editingMappingProfile_atm_addOwner_done = MEA_FALSE;
    MEA_Bool        editingMappingProfile_eth_delOwner_done = MEA_FALSE;
    MEA_Bool        editingMappingProfile_atm_delOwner_done = MEA_FALSE;
    MEA_Uint32      editingMappingProfile_eth_delOwner_id   = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Uint32      editingMappingProfile_atm_delOwner_id   = MEA_PLAT_GENERATE_NEW_ID;

    if(force_i==MEA_FALSE){
		if ( mea_drv_get_free_editing_id(entry_i, id_io) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_get_free_editing_id failed\n",
					          __FUNCTION__);
			return MEA_ERROR;
		}
    } 



	if ( MEA_EHP_Table[*id_io].valid == MEA_FALSE ) {
		num_of_owners =  0;

		if (mea_drv_create_ehp_entry_db(unit_i,
			                            entry_i,
										*id_io) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_create_ehp_entry_db failed (id=%d) \n",
							  __FUNCTION__,
							  *id_io);
			return MEA_ERROR;
		}
	} else { /*modify*/
		num_of_owners = MEA_EHP_Table[*id_io].num_of_owners;
	    
        if(MEA_TDM_SUPPORT && entry_i->pw_control_info.valid == MEA_TRUE &&  
            MEA_EHP_Table[*id_io].editing_info.pw_control_info.valid == MEA_TRUE){
          //check now that the change is only on pw_control_info
            
			if( (mea_drv_Modify_EHP_pw_control(unit_i,entry_i,*id_io) == MEA_OK) && 
				(mea_drv_Modify_EHP_da(unit_i,entry_i,*id_io)         == MEA_OK)){
				MEA_OS_memcpy(&(MEA_EHP_Table[*id_io].editing_info), 
								entry_i, 
								sizeof(*entry_i));
				return MEA_OK;
			}
        }
        
        
        
        
        if (mea_drv_Delete_EHP_da(unit_i,
			                          *id_io) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_Delete_EHP_da failed (id=%d) \n",
							  __FUNCTION__,
							  *id_io);
			return MEA_ERROR;
		}
	    if (mea_drv_Delete_EHP_sa_lss(unit_i,
			                          *id_io) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_Delete_EHP_sa_lss failed (id=%d) \n",
							  __FUNCTION__,
							  *id_io);
			return MEA_ERROR;
		}
	    if (mea_drv_Delete_EHP_profileId(unit_i,
			                             *id_io) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_Delete_EHP_profileId failed (id=%d) \n",
							  __FUNCTION__,
							  *id_io);
			return MEA_ERROR;
		}
        if (mea_drv_Delete_EHP_profile2Id(unit_i,*id_io) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Delete_EHP_profileId failed (id=%d) \n",
                    __FUNCTION__,
                    *id_io);
                return MEA_ERROR;
        }
        if (mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Delete_EHP_fragmentSession failed (id=%d) \n",
                __FUNCTION__,
                *id_io);
            return MEA_ERROR;
        }
        if (mea_drv_Delete_EHP_pw_control(unit_i,*id_io) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Delete_EHP_pw_control failed (id=%d) \n",
                __FUNCTION__,
                *id_io);
            return MEA_ERROR;
        }
        if (mea_drv_Delete_EHP_mpls_label(unit_i,*id_io) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Delete_EHP_mpls_label failed (id=%d) \n",
                __FUNCTION__,
                *id_io);
            return MEA_ERROR;
        }

	}//end modify

    if (mea_drv_Create_EHP_da(unit_i,
		                          entry_i,
								  *id_io) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_da failed (id=%d) \n",
						  __FUNCTION__,
						  *id_io);
		if ( MEA_EHP_Table[*id_io].valid) {
            mea_drv_Create_EHP_da(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_sa_lss(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_profileId(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_profile2Id(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_fragmentSession(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_pw_control(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_mpls_label(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);

		} else {
		    mea_drv_delete_ehp_entry_db(unit_i,*id_io);
		}
		return MEA_ERROR;
	}

    if (mea_drv_Create_EHP_sa_lss(unit_i,
		                          entry_i,
								  *id_io) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_sa_lss failed (id=%d) \n",
						  __FUNCTION__,
						  *id_io);
        mea_drv_Delete_EHP_da(unit_i,*id_io);
		if ( MEA_EHP_Table[*id_io].valid) {
            mea_drv_Create_EHP_da(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_sa_lss(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_profileId(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_profile2Id(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_fragmentSession(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_pw_control(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_mpls_label(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
        } else {
		    mea_drv_delete_ehp_entry_db(unit_i,*id_io);
		}
		return MEA_ERROR;
	}

    if (mea_drv_Create_EHP_profileId(unit_i,
		                             entry_i,
									 *id_io) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_profileId failed (id=%d) \n",
						  __FUNCTION__,
						  *id_io);
        mea_drv_Delete_EHP_da(unit_i,*id_io);
        mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
		if ( MEA_EHP_Table[*id_io].valid) {
            mea_drv_Create_EHP_da(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_sa_lss(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_profileId(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_profile2Id(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_fragmentSession(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_pw_control(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_mpls_label(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
        } else {
		    mea_drv_delete_ehp_entry_db(unit_i,*id_io);
		}
		return MEA_ERROR;
	}

    if (mea_drv_Create_EHP_profile2Id(unit_i,
		                             entry_i,
									 *id_io) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Create_EHP_profile2Id failed (id=%d) \n",
						  __FUNCTION__,
						  *id_io);
        
        mea_drv_Delete_EHP_da(unit_i,*id_io);
        mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
        mea_drv_Delete_EHP_profileId(unit_i,*id_io);
		if ( MEA_EHP_Table[*id_io].valid) {
            mea_drv_Create_EHP_da(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_sa_lss(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_profileId(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_profile2Id(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_fragmentSession(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_pw_control(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_mpls_label(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
		} else {
		    mea_drv_delete_ehp_entry_db(unit_i,*id_io);
		}
		return MEA_ERROR;
	}

    if (mea_drv_Create_EHP_fragmentSession(unit_i,
                                           entry_i,
                                           *id_io) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Create_EHP_fragmentSession failed (id=%d) \n",
                __FUNCTION__,
                *id_io);

            mea_drv_Delete_EHP_da(unit_i,*id_io);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
            mea_drv_Delete_EHP_profileId(unit_i,*id_io);
            mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
            if ( MEA_EHP_Table[*id_io].valid) {
                mea_drv_Create_EHP_da(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_sa_lss(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profileId(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profile2Id(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_fragmentSession(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_pw_control(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_mpls_label(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
            } else {
                mea_drv_delete_ehp_entry_db(unit_i,*id_io);
            }
            return MEA_ERROR;
    }

    if (mea_drv_Create_EHP_pw_control(unit_i,
        entry_i,
        *id_io) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Create_EHP_fragmentSession failed (id=%d) \n",
                __FUNCTION__,
                *id_io);

            mea_drv_Delete_EHP_da(unit_i,*id_io);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
            mea_drv_Delete_EHP_profileId(unit_i,*id_io);
            mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
            mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io);
            if ( MEA_EHP_Table[*id_io].valid) {
                mea_drv_Create_EHP_da(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_sa_lss(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profileId(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profile2Id(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_fragmentSession(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_pw_control(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_mpls_label(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
            } else {
                mea_drv_delete_ehp_entry_db(unit_i,*id_io);
            }
            return MEA_ERROR;
    }
    if (mea_drv_Create_EHP_mpls_label(unit_i,
        entry_i,
        *id_io) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Create_EHP_fragmentSession failed (id=%d) \n",
                __FUNCTION__,
                *id_io);

            mea_drv_Delete_EHP_da(unit_i,*id_io);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
            mea_drv_Delete_EHP_profileId(unit_i,*id_io);
            mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
            mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io);
            mea_drv_Delete_EHP_pw_control(unit_i,*id_io);
            if ( MEA_EHP_Table[*id_io].valid) {
                mea_drv_Create_EHP_da(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_sa_lss(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profileId(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profile2Id(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_fragmentSession(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_pw_control(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_mpls_label(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
            } else {
                mea_drv_delete_ehp_entry_db(unit_i,*id_io);
            }
            return MEA_ERROR;
    }




    /* Add owner to editingMappingProfile (eth) */
    if ((entry_i->eth_info.mapping_enable) &&
        ((!MEA_EHP_Table[*id_io].valid) ||
         ((MEA_EHP_Table[*id_io].editing_info.eth_info.mapping_enable) &&
          (MEA_EHP_Table[*id_io].editing_info.eth_info.mapping_id != entry_i->eth_info.mapping_id)))) {

        if (MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         (entry_i->eth_info.mapping_id)
                                                         ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_AddOwner_EditingMappingProfile_Entry (eth) to id %d (*id_o=%d)\n",
                              __FUNCTION__,
                              entry_i->eth_info.mapping_id,
                              *id_io);
            mea_drv_Delete_EHP_da(unit_i,*id_io);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
            mea_drv_Delete_EHP_profileId(unit_i,*id_io);
            mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
            mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io);
            mea_drv_Delete_EHP_pw_control(unit_i,*id_io);
            mea_drv_Delete_EHP_mpls_label(unit_i,*id_io);
            if ( MEA_EHP_Table[*id_io].valid) {
                mea_drv_Create_EHP_da(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_sa_lss(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profileId(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profile2Id(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_fragmentSession(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_pw_control(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_mpls_label(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
            } else {
                mea_drv_delete_ehp_entry_db(unit_i,*id_io);
            }
            return MEA_ERROR;
        }
        editingMappingProfile_eth_addOwner_done=MEA_TRUE;
    }

    /* Add owner to editingMappingProfile (atm) */
    if ((entry_i->atm_info.mapping_enable) &&
        ((!MEA_EHP_Table[*id_io].valid) ||
         ((MEA_EHP_Table[*id_io].editing_info.atm_info.mapping_enable) &&
          (MEA_EHP_Table[*id_io].editing_info.atm_info.mapping_id != entry_i->atm_info.mapping_id)))) {

        if (MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         (entry_i->atm_info.mapping_id)
                                                         ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_AddOwner_EditingMappingProfile_Entry (atm) to id %d (*id_o=%d)\n",
                              __FUNCTION__,
                              entry_i->atm_info.mapping_id,
                              *id_io);
            if (editingMappingProfile_eth_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                            (MEA_EditingMappingProfile_Id_t)entry_i->eth_info.mapping_id);
            }
            mea_drv_Delete_EHP_da(unit_i,*id_io);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
            mea_drv_Delete_EHP_profileId(unit_i,*id_io);
            mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
            mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io);
            mea_drv_Delete_EHP_pw_control(unit_i,*id_io);
            mea_drv_Delete_EHP_mpls_label(unit_i,*id_io);
            if ( MEA_EHP_Table[*id_io].valid) {
                mea_drv_Create_EHP_da(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_sa_lss(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profileId(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profile2Id(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_fragmentSession(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_pw_control(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_mpls_label(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
            } else {
                mea_drv_delete_ehp_entry_db(unit_i,*id_io);
            }
            return MEA_ERROR;
        }
        editingMappingProfile_atm_addOwner_done = MEA_TRUE;
    }


    /* Del owner to editingMappingProfile (eth) */
    if ((MEA_EHP_Table[*id_io].valid) &&
        (MEA_EHP_Table[*id_io].editing_info.eth_info.mapping_enable) &&
        ((!entry_i->eth_info.mapping_enable) ||
         ((entry_i->eth_info.mapping_enable) &&
          (MEA_EHP_Table[*id_io].editing_info.eth_info.mapping_id != entry_i->eth_info.mapping_id)))) {
        if (MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         (MEA_EHP_Table[*id_io].editing_info.eth_info.mapping_id)
                                                         ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_AddOwner_EditingMappingProfile_Entry (eth) to id %d (*id_o=%d)\n",
                              __FUNCTION__,
                              MEA_EHP_Table[*id_io].editing_info.eth_info.mapping_id,
                              *id_io);
            if (editingMappingProfile_atm_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->atm_info.mapping_id);
            }
            if (editingMappingProfile_eth_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->eth_info.mapping_id);
            }
            
            mea_drv_Delete_EHP_da(unit_i,*id_io);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
            mea_drv_Delete_EHP_profileId(unit_i,*id_io);
            mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
            mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io);
            mea_drv_Delete_EHP_pw_control(unit_i,*id_io);
            mea_drv_Delete_EHP_mpls_label(unit_i,*id_io);
            if ( MEA_EHP_Table[*id_io].valid) {
                mea_drv_Create_EHP_da(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_sa_lss(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profileId(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profile2Id(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_fragmentSession(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_pw_control(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_mpls_label(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
            } else {
                mea_drv_delete_ehp_entry_db(unit_i,*id_io);
            }
            return MEA_ERROR;
        }
        editingMappingProfile_eth_delOwner_done = MEA_TRUE;
        editingMappingProfile_eth_delOwner_id   = MEA_EHP_Table[*id_io].editing_info.eth_info.mapping_id;
    }

    /* Del owner to editingMappingProfile (atm) */
    if ((MEA_EHP_Table[*id_io].valid) &&
        (MEA_EHP_Table[*id_io].editing_info.atm_info.mapping_enable) &&
        ((!entry_i->atm_info.mapping_enable) ||
         ((entry_i->atm_info.mapping_enable) &&
          (MEA_EHP_Table[*id_io].editing_info.atm_info.mapping_id != entry_i->eth_info.mapping_id)))) {
        if (MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                         (MEA_EditingMappingProfile_Id_t)
                                                         (MEA_EHP_Table[*id_io].editing_info.atm_info.mapping_id)
                                                         ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_AddOwner_EditingMappingProfile_Entry (atm) to id %d (*id_o=%d)\n",
                              __FUNCTION__,
                              MEA_EHP_Table[*id_io].editing_info.eth_info.mapping_id,
                              *id_io);
            if (editingMappingProfile_eth_delOwner_done) {
                MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             editingMappingProfile_eth_delOwner_id);
            }
            if (editingMappingProfile_atm_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->atm_info.mapping_id);
            }
            if (editingMappingProfile_eth_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->eth_info.mapping_id);
            }
            mea_drv_Delete_EHP_da(unit_i,*id_io);
            mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
            mea_drv_Delete_EHP_profileId(unit_i,*id_io);
            mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
            mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io);
            mea_drv_Delete_EHP_pw_control(unit_i,*id_io);
            mea_drv_Delete_EHP_mpls_label(unit_i,*id_io);
            if ( MEA_EHP_Table[*id_io].valid) {
                mea_drv_Create_EHP_da(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_sa_lss(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profileId(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profile2Id(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_fragmentSession(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_pw_control(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_mpls_label(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
            } else {
                mea_drv_delete_ehp_entry_db(unit_i,*id_io);
            }
            return MEA_ERROR;
        }
        editingMappingProfile_atm_delOwner_done = MEA_TRUE;
        editingMappingProfile_atm_delOwner_id   = MEA_EHP_Table[*id_io].editing_info.eth_info.mapping_id;
    }

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
	                    {
                            mea_drv_Delete_EHP_da(unit_i,*id_io);
                            mea_drv_Delete_EHP_sa_lss(unit_i,*id_io);
	                        mea_drv_Delete_EHP_profileId(unit_i,*id_io);
                            mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
                            mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io);
                            mea_drv_Delete_EHP_pw_control(unit_i,*id_io);
                            mea_drv_Delete_EHP_mpls_label(unit_i,*id_io);
							if ( MEA_EHP_Table[*id_io].valid) {
                                mea_drv_Create_EHP_da(unit_i,
                                    &(MEA_EHP_Table[*id_io].editing_info),
                                    *id_io);
                                mea_drv_Create_EHP_sa_lss(unit_i,
                                    &(MEA_EHP_Table[*id_io].editing_info),
                                    *id_io);
                                mea_drv_Create_EHP_profileId(unit_i,
                                    &(MEA_EHP_Table[*id_io].editing_info),
                                    *id_io);
                                mea_drv_Create_EHP_profile2Id(unit_i,
                                    &(MEA_EHP_Table[*id_io].editing_info),
                                    *id_io);
                                mea_drv_Create_EHP_fragmentSession(unit_i,
                                    &(MEA_EHP_Table[*id_io].editing_info),
                                    *id_io);
                                mea_drv_Create_EHP_pw_control(unit_i,
                                    &(MEA_EHP_Table[*id_io].editing_info),
                                    *id_io);
                                mea_drv_Create_EHP_mpls_label(unit_i,
                                    &(MEA_EHP_Table[*id_io].editing_info),
                                    *id_io);
							} else {
								mea_drv_delete_ehp_entry_db(unit_i,*id_io);
							}
							return MEA_ERROR;
						}
						)

    /* Update Software shadow */
	MEA_OS_memset((char*)(&(MEA_EHP_Table[*id_io])),
		          0,
				  sizeof(MEA_EHP_Table[0]));
    MEA_EHP_Table[*id_io].valid         = MEA_TRUE; //// add_owner
    MEA_EHP_Table[*id_io].hwUnit        = hwUnit;
    MEA_EHP_Table[*id_io].num_of_owners = num_of_owners;
	MEA_OS_memcpy(&(MEA_EHP_Table[*id_io].editing_info), 
		          entry_i, 
				  sizeof(*entry_i));

	if(force_i==MEA_FALSE){
		if ( mea_drv_ehp_add_owner(*id_io) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_ehp_add_owner failed, id %d\n",
					          __FUNCTION__, *id_io);
            if (editingMappingProfile_atm_delOwner_done) {
                MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             editingMappingProfile_atm_delOwner_id);
            }
            if (editingMappingProfile_eth_delOwner_done) {
                MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             editingMappingProfile_eth_delOwner_id);
            }
            if (editingMappingProfile_atm_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->atm_info.mapping_id);
            }
            if (editingMappingProfile_eth_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->eth_info.mapping_id);
            }
            
            mea_drv_Delete_EHP_da(unit_i, *id_io);
            mea_drv_Delete_EHP_sa_lss(unit_i, *id_io);
            mea_drv_Delete_EHP_profileId(unit_i,*id_io);
            mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
            mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io);
            mea_drv_Delete_EHP_pw_control(unit_i,*id_io);
            mea_drv_Delete_EHP_mpls_label(unit_i,*id_io);
            if ( MEA_EHP_Table[*id_io].valid) {
                mea_drv_Create_EHP_da(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_sa_lss(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profileId(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_profile2Id(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_fragmentSession(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_pw_control(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
                mea_drv_Create_EHP_mpls_label(unit_i,
                    &(MEA_EHP_Table[*id_io].editing_info),
                    *id_io);
			} else {
				mea_drv_delete_ehp_entry_db(unit_i,*id_io);
			}
		    return MEA_ERROR;
		}
    }
#ifdef MEA_SWAP_MAC
	/* if CMD is SWAP MAC- no need for HW update */
	if ( entry_i->eth_info.command == MEA_EGRESS_HEADER_PROC_CMD_SWAP_MAC ) {
		return MEA_OK;
    }
#endif

    /* Update the hardware with the values from the shadow */
    if (mea_drv_EHP_UpdateHw (unit_i,
		                      *id_io) != MEA_OK) 
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Update Hardware failed "
                          "(index=%d)\n",
                          __FUNCTION__,*id_io);
            if (editingMappingProfile_atm_delOwner_done) {
                MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             editingMappingProfile_atm_delOwner_id);
            }
            if (editingMappingProfile_eth_delOwner_done) {
                MEA_API_AddOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             editingMappingProfile_eth_delOwner_id);
            }
            if (editingMappingProfile_atm_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->atm_info.mapping_id);
            }
            if (editingMappingProfile_eth_addOwner_done) {
                MEA_API_DelOwner_EditingMappingProfile_Entry(unit_i,
                                                             (MEA_EditingMappingProfile_Id_t)
                                                             entry_i->eth_info.mapping_id);
            }
        mea_drv_Delete_EHP_da(unit_i, *id_io);
        mea_drv_Delete_EHP_sa_lss(unit_i, *id_io);
        mea_drv_Delete_EHP_profileId(unit_i,*id_io);
        mea_drv_Delete_EHP_profile2Id(unit_i,*id_io);
        mea_drv_Delete_EHP_fragmentSession(unit_i,*id_io);
        mea_drv_Delete_EHP_pw_control(unit_i,*id_io);
        mea_drv_Delete_EHP_mpls_label(unit_i,*id_io);
        if ( MEA_EHP_Table[*id_io].valid) {
            mea_drv_Create_EHP_da(unit_i,
                                      &(MEA_EHP_Table[*id_io].editing_info),
                                      *id_io);
            mea_drv_Create_EHP_sa_lss(unit_i,
                                    &(MEA_EHP_Table[*id_io].editing_info),
                                    *id_io);
            mea_drv_Create_EHP_profileId(unit_i,
                                        &(MEA_EHP_Table[*id_io].editing_info),
                                        *id_io);
            mea_drv_Create_EHP_profile2Id(unit_i,
                                         &(MEA_EHP_Table[*id_io].editing_info),
                                          *id_io);
            mea_drv_Create_EHP_fragmentSession(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_pw_control(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
            mea_drv_Create_EHP_mpls_label(unit_i,
                &(MEA_EHP_Table[*id_io].editing_info),
                *id_io);
		} else {
			mea_drv_delete_ehp_entry_db(unit_i,*id_io);
		}
        return MEA_ERROR; 
    }

	return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_ETH>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_EHP_UpdateHwEntry_ETH(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{    
    MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
    //    MEA_db_HwUnit_t         hwUnit_i   = (MEA_db_HwUnit_t)((long)arg2);
    MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32       *data = (MEA_Uint32*)arg4;
    MEA_Uint32 i;
   
	
	for (i=0;i<len;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),data[i],MEA_MODULE_BM);
	}

	

}

/*------------------------------------------------------------------*/

static void      mea_Editing_buildHWData(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t         hwUnit_i,
    MEA_Editing_t                   id_i,
    MEA_EgressHeaderProc_Entry_dbt *entry_pi,
    MEA_Uint32 len,
    MEA_Uint32  *retVal)
{

    MEA_Uint32 command_val = 0;
    MEA_Uint32 vlan_value = 0;
    
    MEA_Uint32 qtag_value = 0;
    MEA_db_HwId_t prof_HwId;

    MEA_Uint32                count_shift;
    MEA_Uint32 val[2];
    MEA_Uint32 CNB_numofbit;

    

    count_shift = 0;
    /************************************************************************/
    /* DA info                                                              */
    /************************************************************************/
    
    val[0] = 0;
    val[1] = 0;
    val[0] = ((MEA_Uint32)(MEA_EHP_da_Table[id_i].da.b[5]) << 0);
    val[0] |= ((MEA_Uint32)(MEA_EHP_da_Table[id_i].da.b[4]) << 8);
    val[0] |= ((MEA_Uint32)(MEA_EHP_da_Table[id_i].da.b[3]) << 16);
    val[0] |= ((MEA_Uint32)(MEA_EHP_da_Table[id_i].da.b[2]) << 24);


    val[1] = ((MEA_Uint32)(MEA_EHP_da_Table[id_i].da.b[1]) << 0);
    val[1] |= ((MEA_Uint32)(MEA_EHP_da_Table[id_i].da.b[0]) << 8);
    
    
        MEA_OS_insert_value(count_shift,
        32,
        val[0],
        retVal);
    count_shift += 32;

    MEA_OS_insert_value(count_shift,
        16,
        val[1],
        retVal);
    count_shift += 16;


    /************************************************************************/
    /* SA_lss info                                                          */
    /************************************************************************/
    val[0]=MEA_EHP_sa_lss_Table[id_i].sa_lss;

    MEA_OS_insert_value(count_shift,
        32,
        val[0],
        retVal);
    count_shift += 32;

    /************************************************************************/
    /* Profile info                                                         */
    /************************************************************************/
    
    val[0] = 0;
        val[0] |= ((((MEA_Uint32)MEA_EHP_profileId_Table[id_i].martini_command) & 0x00000003) << 0);

    MEA_DRV_GET_HW_ID(unit_i,
        mea_drv_Get_EHP_profile_sa_mss_db,
        MEA_EHP_profileId_Table[id_i].profile_sa_mss,
        hwUnit_i,
        prof_HwId,
        return ;);
    val[0] |= ((((MEA_Uint32)prof_HwId) & 0x00000003) << 2);

    MEA_DRV_GET_HW_ID(unit_i,
        mea_drv_Get_EHP_profile_ether_type_db,
        MEA_EHP_profileId_Table[id_i].profile_ether_type,
        hwUnit_i,
        prof_HwId,
        return;);
    val[0] |= ((((MEA_Uint32)prof_HwId) & 0x00000003) << 4);

    MEA_DRV_GET_HW_ID(unit_i,
        mea_drv_Get_EHP_profile_stamping_db,
        MEA_EHP_profileId_Table[id_i].profile_stamping,
        hwUnit_i,
        prof_HwId,
        return;);
    val[0] |= ((((MEA_Uint32)prof_HwId) & 0x00000003) << 6);
     
    MEA_OS_insert_value(count_shift,
        8,
        val[0],
        retVal);
    count_shift += 8;
    /************************************************************************/
    /* VLAN info                                                             */
    /************************************************************************/
    
    /* ProtocolMachine_VLAN */
    command_val = entry_pi->eth_info.command;
    vlan_value = entry_pi->eth_info.val.all;

    if (entry_pi->EditingType == 0) {
        command_val = entry_pi->eth_info.command;
        vlan_value = entry_pi->eth_info.val.all;
    }  else {
        command_val = mea_drv_ProtocolMachine_get_command(entry_pi->EditingType, MEA_MACHINE_PROTO_VLAN);
    }
    
    switch (entry_pi->EditingType)
    {
     case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_VLAN:
    case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_QTAG:
    case MEA_EDITINGTYPE_IPCS_DL_IPinIP_Append_SGW:
    case MEA_EDITINGTYPE_IPCS_DL_UL_SGW_Swap_header:
 
        vlan_value = entry_pi->IPCS_DL_info.dst_eNB_Ip;
        break;

    case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_VLAN:
    case MEA_EDITINGTYPE_IPCS_UL_IPinIP_Extract_SGW:
    
        if (entry_pi->IPCS_UL_info.valid == MEA_TRUE){
            vlan_value = entry_pi->IPCS_UL_info.apn_vlan;
        }
        break;



    case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_QTAG:
 
        break;

    case MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent: /*vlan_value   dst_eNB_Ip */
    case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN:
    case MEA_EDITINGTYPE_VPWS_DL_Swap_external_VLAN_TAG:
    case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN_Swap_Internal_VLAN:
    case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent:
    case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN:
    case MEA_EDITINGTYPE_VPWS_DL_Extract_Extract_QInQ :
    case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ:
        vlan_value = entry_pi->ETHCS_DL_info.dst_eNB_Ip;
        break;

    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS:
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE){
            vlan_value = entry_pi->ETHCS_UL_info.c_vlan;
        }
        break;

    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG:
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE){
            vlan_value = entry_pi->ETHCS_UL_info.c_vlan;
        }

        break;
    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QinQ:
 
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE) {
            vlan_value = entry_pi->ETHCS_UL_info.c_vlan;
        }

        break;


        

    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Swap_External_TAG:
   
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE){
            vlan_value = entry_pi->ETHCS_UL_info.c_vlan;
        }
        break;

    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG_Swap_Internal_VLAN: /*TBD*/
     
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE){
            vlan_value = entry_pi->ETHCS_UL_info.c_vlan;
        }
        break;


    case MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_APPEND_BMAC_APPEND_ITAG:
      
        break;
    case MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_EXTRACT_BMAC_EXTRACT_ITAG:
          break;
    case MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_SWAP_BMAC_SWAP_ITAG:
        break;

    
    case MEA_EDITINGTYPE_VXLAN_header_Append_vlan :
    case MEA_EDITINGTYPE_VXLAN_header_Append_vlan_Extract_INNER_vlan :
    case MEA_EDITINGTYPE_VXLAN_header :
    case MEA_EDITINGTYPE_VXLAN_header_Extract_INNER_vlan:
    case MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_VXLAN_header:
    case MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_Extract_Inner_vlan_VXLAN_header:
    case MEA_EDITINGTYPE_IPSec_Append_Tunnel_VxLAN:
        if(entry_pi->VxLan_info.valid)
            vlan_value = entry_pi->VxLan_info.vxlan_dst_Ip;
   break;
    case MEA_EDITINGTYPE_NVGRE_Append_Tunnel:
    case MEA_EDITINGTYPE_NVGRE_Append_Tunnel_ADD_Outer_vlan:
        if (entry_pi->NVGre_info.valid == MEA_TRUE) {
            vlan_value = entry_pi->NVGre_info.GRE_DST_IP;
        }
   break;
    case MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE:
    case MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE_ADD_Outer_vlan:
        if (entry_pi->IPSec_info.valid == MEA_TRUE) {
            vlan_value = entry_pi->NVGre_info.GRE_DST_IP;
        }
    break;
    case MEA_EDITINGTYPE_IPSec_Append_Tunnel_ONLY:
	case MEA_EDITINGTYPE_IPSec_Append_Tunnel_Extract_vlan:
        if (entry_pi->IPSec_info.valid == MEA_TRUE) {
            vlan_value = entry_pi->IPSec_info.IPSec_src_IP;
        }

        break;
    case MEA_EDITINGTYPE_GRE_Append_Tunnel:
    case MEA_EDITINGTYPE_L2TP_Append_Tunnel:
        if (entry_pi->GRE_L2TP_info.valid == MEA_TRUE) {
            vlan_value = entry_pi->GRE_L2TP_info.dst_IPv4;
            
        }
        break;

    } // end switch

    val[0] = 0;
    val[1] = 0;

    /* build word 1 */
    val[0] |= vlan_value << MEA_OS_calc_shift_from_mask
        (MEA_EGRESS_HEADER_PROC_DWORD_1_MASK_VAL);

    /* build word 2 */
    if (entry_pi->eth_info.mapping_enable) {
        /* in case mapping enable we set the stamp_color/priority to 1/1
        and the command change from add(1) to trans(0) or from swap(3) to extract(2)
        Note: This special combination tell the device to do append/swap with use
        the mapping profile id to map the stamping priority value+policer result color
        to the stamping priority and stamp color that will be use for editing */
        MEA_db_HwId_t mapping_HwId;
        MEA_DRV_GET_HW_ID(unit_i,
            mea_drv_Get_EditingMappingProfile_db,
            (MEA_db_SwId_t)(entry_pi->eth_info.mapping_id),
            hwUnit_i,
            mapping_HwId,
            return;)
            val[0] &= 0xffff0fff;
        val[0] |= (mapping_HwId << 12);
        val[1] |= entry_pi->eth_info.stamp_color << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_COLOR);
        val[1] |= entry_pi->eth_info.stamp_priority << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_PRIORITY);
        val[1] |= command_val << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_COMMAND);
    }
    else {
        val[1] |= entry_pi->eth_info.stamp_color << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_COLOR);
        val[1] |= entry_pi->eth_info.stamp_priority << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_PRIORITY);
        val[1] |= command_val << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_COMMAND);
    }

    MEA_OS_insert_value(count_shift,
        32,
        val[0],
        retVal);
    count_shift += 32;

    MEA_OS_insert_value(count_shift,
        4,
        val[1],
        retVal);
    count_shift += 4;


    /************************************************************************/
    /* ATM info                                                             */
    /************************************************************************/
    
    /* ProtocolMachine _Qtag*/
    command_val = entry_pi->atm_info.double_tag_cmd;
    qtag_value = entry_pi->atm_info.val.all;

    if (entry_pi->EditingType==0) {
        command_val = entry_pi->atm_info.double_tag_cmd;
        qtag_value = entry_pi->atm_info.val.all;
    } else {
        command_val = mea_drv_ProtocolMachine_get_command(entry_pi->EditingType, MEA_MACHINE_PROTO_OUTER);
    }

    switch (entry_pi->EditingType)
    {

    case 0:
        command_val = entry_pi->atm_info.double_tag_cmd;
        qtag_value = entry_pi->atm_info.val.all;
        break;
    case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_VLAN:
    case MEA_EDITINGTYPE_IPCS_DL_IPinIP_Append_SGW:
    case MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_QTAG:
    case MEA_EDITINGTYPE_IPCS_DL_UL_SGW_Swap_header:
        qtag_value = entry_pi->IPCS_DL_info.TE_ID;

        break;


    case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_VLAN:
    case MEA_EDITINGTYPE_IPCS_UL_IPinIP_Extract_SGW:
        break;
    case MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_QTAG:
        break;

    case MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent:  /*qtag_value  */
    case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN:
    case MEA_EDITINGTYPE_VPWS_DL_Swap_external_VLAN_TAG:
    case MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN_Swap_Internal_VLAN:
    case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent:
    case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN:
    case MEA_EDITINGTYPE_VPWS_DL_Extract_Extract_QInQ:
    case MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ:
        qtag_value = entry_pi->ETHCS_DL_info.TE_ID;
        break;


    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS:
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE){
            qtag_value = entry_pi->ETHCS_UL_info.s_vlan;
        }
        break;

    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG:
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE){
            qtag_value = entry_pi->ETHCS_UL_info.s_vlan;
        }
        break;
    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QinQ:
        command_val = mea_drv_ProtocolMachine_get_command(MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QinQ, MEA_MACHINE_PROTO_OUTER);
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE) {
            qtag_value = entry_pi->ETHCS_UL_info.s_vlan;
        }
        break;

        
        

    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Swap_External_TAG:
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE){
            qtag_value = entry_pi->ETHCS_UL_info.s_vlan;
        }
        break;

    case MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG_Swap_Internal_VLAN: /*TBD*/
        if (entry_pi->ETHCS_UL_info.valid == MEA_TRUE){
            qtag_value = entry_pi->ETHCS_UL_info.s_vlan;
        }
        break;
    

    case MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_APPEND_BMAC_APPEND_ITAG:
        break;
    case MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_EXTRACT_BMAC_EXTRACT_ITAG:
        break;
    case MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_SWAP_BMAC_SWAP_ITAG:
        break;
    case MEA_EDITINGTYPE_VXLAN_header_Append_vlan:
    case MEA_EDITINGTYPE_VXLAN_header_Append_vlan_Extract_INNER_vlan:
    case MEA_EDITINGTYPE_VXLAN_header:
    case MEA_EDITINGTYPE_VXLAN_header_Extract_INNER_vlan:
    case MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_VXLAN_header:
    case MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_Extract_Inner_vlan_VXLAN_header:
    case MEA_EDITINGTYPE_IPSec_Append_Tunnel_VxLAN:
        if (entry_pi->VxLan_info.valid) {
            qtag_value = entry_pi->VxLan_info.vxlan_vni;
            qtag_value |= entry_pi->VxLan_info.ttl << 24;
        }

        break;
    case MEA_EDITINGTYPE_NVGRE_Append_Tunnel:
    case MEA_EDITINGTYPE_NVGRE_Append_Tunnel_ADD_Outer_vlan:
        if (entry_pi->NVGre_info.valid == MEA_TRUE) {
            qtag_value = entry_pi->NVGre_info.VSID << 8;
            qtag_value |= (entry_pi->NVGre_info.FlowID & 0xff);
        }
        break;
    case MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE:
    case MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE_ADD_Outer_vlan:
        if (entry_pi->IPSec_info.valid == MEA_TRUE) {
            qtag_value = entry_pi->NVGre_info.VSID << 8;
            qtag_value |= (entry_pi->NVGre_info.FlowID & 0xff);
        }
        break;
    case MEA_EDITINGTYPE_GRE_Append_Tunnel:
    case MEA_EDITINGTYPE_L2TP_Append_Tunnel:
        if (entry_pi->GRE_L2TP_info.valid == MEA_TRUE) {
            if (entry_pi->GRE_L2TP_info.Tunne_type == MEA_Tunnel_TYPE_GRE) {
                qtag_value = entry_pi->GRE_L2TP_info.src_IPv4;
            }
            else {
                //L2TP
                qtag_value = entry_pi->GRE_L2TP_info.SessionId & 0xffff;
                qtag_value |= ((entry_pi->GRE_L2TP_info.TunnelId & 0xffff) << 16);
            }
        
        
        }
        

        break;


    } // end switch


    val[0] = 0;
    val[1] = 0;


    /* build word 1 */
    val[0] |= qtag_value << MEA_OS_calc_shift_from_mask
        (MEA_EGRESS_HEADER_PROC_DWORD_1_MASK_VAL);

    /* build word 2 */
    if (entry_pi->atm_info.mapping_enable) {
        /* in case mapping enable we set the stamp_color/priority to 1/1
        and the command change from add(1) to trans(0) or from swap(3) to extract(2)
        Note: This special combination tell the device to do append/swap with use
        the mapping profile id to map the stamping priority value+policer result color
        to the stamping priority and stamp color that will be use for editing */
        MEA_db_HwId_t mapping_HwId;
        MEA_DRV_GET_HW_ID(unit_i,
            mea_drv_Get_EditingMappingProfile_db,
            (MEA_db_SwId_t)(entry_pi->atm_info.mapping_id),
            hwUnit_i,
            mapping_HwId,
            return;)
            val[0] &= 0xffff0fff;
        val[0] |= (mapping_HwId << 12);
        val[1] |= entry_pi->atm_info.stamp_color << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_COLOR);
        val[1] |= entry_pi->atm_info.stamp_priority << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_PRIORITY);
        val[1] |= command_val << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_COMMAND);

    }
    else {
        val[1] |= entry_pi->atm_info.stamp_color << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_COLOR);
        val[1] |= entry_pi->atm_info.stamp_priority << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_PRIORITY);
        val[1] |= command_val << MEA_OS_calc_shift_from_mask
            (MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_COMMAND);
    }

    MEA_OS_insert_value(count_shift,
        32,
        val[0],
        retVal);
    count_shift += 32;

    MEA_OS_insert_value(count_shift,
        4,
        val[1],
        retVal);
    count_shift += 4;


    /************************************************************************/
    /* WBRG info                                                             */
    /************************************************************************/
    /* build word 1 */
    val[0] = 0;
    val[0] = entry_pi->wbrg_info.val.all;
	
    val[0] &= 0xffffff3f;/*bit 7,6 will be zero */
    val[0] |= (entry_pi->eth_info.mapping_enable << 6);
    val[0] |= (entry_pi->atm_info.mapping_enable << 7);


    if (entry_pi->ETHCS_DL_info.valid == MEA_TRUE){
        val[0] |= (MEA_Uint32)(entry_pi->ETHCS_DL_info.sgw_ipId & 0x7) << 8; /*Not relevant we have only one Sgw*/
    }

    if (entry_pi->IPCS_DL_info.valid == MEA_TRUE){
        val[0] |= (MEA_Uint32)(entry_pi->IPCS_DL_info.sgw_ipId & 0x7) << 8; /*Not relevant we have only one Sgw*/
    }
    if (entry_pi->VxLan_info.valid == MEA_TRUE) {
        val[0] |= (MEA_Uint32)(entry_pi->VxLan_info.sgw_ipId & 0x7) << 8; /*Not relevant we have only one Sgw*/
    }


    if (entry_pi->IPCS_UL_info.valid == MEA_TRUE) {
        val[0] |= (MEA_Uint32)(entry_pi->IPCS_UL_info.id_pdn_IP & 0x7) << 8;
    }


    if ((entry_pi->Attribute_info.valid == MEA_TRUE) && (entry_pi->EditingType == 0)){

        val[0] |= (MEA_Uint32)(entry_pi->Attribute_info.protocol) << 11;
        val[0] |= (MEA_Uint32)(entry_pi->Attribute_info.llc) << 13;

    }



   
    if (entry_pi->EditingType != 0){

        val[0] |= (MEA_Uint32)(mea_drv_ProtocolMachine_get_command(entry_pi->EditingType, MEA_MACHINE_PROTO_PROTO)) << 11;
        val[0] |= (MEA_Uint32)mea_drv_ProtocolMachine_get_command(entry_pi->EditingType, MEA_MACHINE_PROTO_LLC) << 13;


    }
    
    CNB_numofbit = MEA_BM_EHP_CNB_MAX_BIT(hwUnit_i);

    MEA_OS_insert_value(count_shift,
        CNB_numofbit, //14
        val[0],
        retVal);
    count_shift += CNB_numofbit;  //14

    /************************************************************************/
    /* SWAP_EDIT                                                            */
    /************************************************************************/
    val[0] = 0;
    if (MEA_EHP_profile2Id_Table[id_i].valid) {

         MEA_Uint32   countshift = 0;

         MEA_OS_insert_value(countshift,
                MEA_BM_EHP_PROFILE2_ID_LM_CMD_CFM_WIDTH,
                MEA_EHP_profile2Id_Table[id_i].Command_cfm,
                &val[0]);
         countshift += MEA_BM_EHP_PROFILE2_ID_LM_CMD_CFM_WIDTH;

         MEA_OS_insert_value(countshift,
                MEA_BM_EHP_PROFILE2_ID_LM_CMD_DASA_WIDTH,
                MEA_EHP_profile2Id_Table[id_i].Command_dasa,
                &val[0]);
            countshift += MEA_BM_EHP_PROFILE2_ID_LM_CMD_DASA_WIDTH;


            MEA_OS_insert_value(countshift,
               
                MEA_BM_EHP_PROFILE2_ID_LM_PROFILE_WIDTH,
                MEA_EHP_profile2Id_Table[id_i].profile2_lm,
                &val[0]);
            countshift += MEA_BM_EHP_PROFILE2_ID_LM_PROFILE_WIDTH; //MEA_BM_EHP_PROFILE2_ID_LM_PROFILE_WIDTH(hwUnit_i);

            MEA_OS_insert_value(countshift,
                MEA_BM_EHP_PROFILE2_ID_LM_CMD_DSCP_VALUE_WIDTH,
                MEA_EHP_profile2Id_Table[id_i].Command_dscp_stamp_value,
                &val[0]);
            countshift += MEA_BM_EHP_PROFILE2_ID_LM_CMD_DSCP_VALUE_WIDTH;

            MEA_OS_insert_value(countshift,
                MEA_BM_EHP_PROFILE2_ID_LM_CMD_DSCP_VALID_WIDTH,
                MEA_EHP_profile2Id_Table[id_i].Command_dscp_stamp_enable,
                &val[0]);
            countshift += MEA_BM_EHP_PROFILE2_ID_LM_CMD_DSCP_VALID_WIDTH;

            MEA_OS_insert_value(countshift,
                MEA_BM_EHP_PROFILE2_ID_LM_CMD_ETH_CS_DOWNLINK_PACKET, /*first ip checksum*/
                MEA_EHP_profile2Id_Table[id_i].ETH_CS_packet_is_valid,
                &val[0]);
            countshift += MEA_BM_EHP_PROFILE2_ID_LM_CMD_ETH_CS_DOWNLINK_PACKET;

            MEA_OS_insert_value(countshift,
                MEA_BM_EHP_PROFILE2_ID_LM_CMD_IP_CHECSUM_CALC_PACKET,  /* ip checksum */
                MEA_EHP_profile2Id_Table[id_i].calcIp_checksum ,
                &val[0]);
            countshift += MEA_BM_EHP_PROFILE2_ID_LM_CMD_IP_CHECSUM_CALC_PACKET;



        }
    MEA_OS_insert_value(count_shift,
        22,
        val[0],
        retVal);
    count_shift += 22;

    MEA_OS_insert_value(count_shift,
        MEA_BM_EHP_IPSec_TYPE,  
        entry_pi->encryp_decryp_type,
        retVal);

    count_shift += MEA_BM_EHP_IPSec_TYPE;

    MEA_OS_insert_value(count_shift,
        MEA_BM_EHP_PROF_IPSecESP_WIDTH,
        entry_pi->crypto_ESP_Id,
        retVal);

    count_shift += MEA_BM_EHP_PROF_IPSecESP_WIDTH;

/*-------------------*/

    if (MEA_EHP_NEW_DB_SUPPORT == MEA_TRUE) 
    {
        val[0] = 0;
        val[1] = 0;

        switch (entry_pi->EditingType)
        { 

        case 0:

            break;
        case MEA_EDITINGTYPE_IPSec_Append_Tunnel_VxLAN:
            if (entry_pi->IPSec_info.valid == MEA_TRUE) {
                val[0] = entry_pi->IPSec_info.IPSec_DST_IP;
            }
             
            break;
        default:
            break;
        }


        MEA_OS_insert_value(count_shift,
        	MEA_BM_EHP_IP_IPSecTunnel_WIDTH,
            val[0],
            retVal);

        

       
        
        

    }


	MEA_OS_insert_value(count_shift,
			MEA_BM_EHP_Command_dscp_type_WIDTH,
	        entry_pi->LmCounterId_info.Command_IpPrioityType,
	        retVal);

	    count_shift += MEA_BM_EHP_Command_dscp_type_WIDTH;


    
        
       /*-------------------------------------------------------------------------------*/
        
        
        
        
  
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHw_ETH>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_EHP_UpdateHw_ETH(MEA_Unit_t                      unit_i,
				 						   MEA_Editing_t                   id_i,
										   MEA_EgressHeaderProc_Entry_dbt* entry_pi) 
{
	MEA_ind_write_t ind_write;
	MEA_db_HwUnit_t hwUnit;
	MEA_db_HwId_t   hwId;
    MEA_Uint32         numofRegs = 8;
   
    MEA_Uint32 retVal[MEA_EDITING_ADDRES_DDR_MAX_WIDTH];

    MEA_Uint32            addressDDR;
    //MEA_Uint32 i;

	MEA_OS_memset(&retVal,0,sizeof(retVal));

    /* Get the hwUnit and hwId */
	MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
		                          mea_drv_Get_EHP_ETH_db,
								  id_i,
								  hwUnit,
								  hwId,
								  return MEA_ERROR);

    MEA_OS_memset(&retVal, 0, sizeof(retVal));

    mea_Editing_buildHWData(unit_i,
        hwUnit,
        id_i,
        entry_pi,
        8,/*256bit*/
        &retVal[0]);
    
    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit_i, MEA_DB_EDITING_TYPE) == MEA_TRUE)
    {

        /*write to ACTION DB on DDR */
        addressDDR = MEA_EDITING_BASE_ADDRES_DDR_START + ((hwId * MEA_EDITING_BASE_ADDRES_DDR_WIDTH * 4));
        MEA_DRV_WriteDDR_DATA_BLOCK(unit_i, hwId, addressDDR, &retVal[0], MEA_EDITING_BASE_ADDRES_DDR_WIDTH, DDR_EDITING);

    }
    else{
        /* Update the HW */
        ind_write.tableOffset = hwId;
        ind_write.tableType = ENET_BM_TBL_TYP_EHP_ETH;
        ind_write.cmdReg = MEA_BM_IA_CMD;
        ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
        ind_write.statusReg = MEA_BM_IA_STAT;
        ind_write.statusMask = MEA_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_EHP_UpdateHwEntry_ETH;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
        ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
        ind_write.funcParam4 = (MEA_funcParam)&retVal;


        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) == MEA_ERROR) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }
        if (mea_db_Set_Hw_Regs(unit_i,
            MEA_EHP_ETH_db,
            hwUnit,
            hwId,
            numofRegs,
            &retVal[0]) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                __FUNCTION__,
                hwUnit,
                hwId);
        }


    }


	/* Return to Caller */
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_ATM>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_WBRG>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_da>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_sa_lss>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_profileId>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_profile_stamping>               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_EHP_UpdateHwEntry_profile_stamping(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
	MEA_Unit_t                        unit_i   = (MEA_Unit_t                       )arg1;
    MEA_db_HwUnit_t                   hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t                     hwId_i = (MEA_db_HwId_t)((long)arg3);
	MEA_drv_ehp_profile_stamping_dbt *entry_pi = (MEA_drv_ehp_profile_stamping_dbt*)arg4;
    MEA_Uint32      val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
	MEA_Uint32      i;
	MEA_Uint16      num_of_regs;
    MEA_Uint32                  count_shift=0;

	num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
		                     MEA_BM_TBL_TYP_EHP_PROFILE_NUM_OF_REGS(hwUnit_i));
	for (i=0;i<num_of_regs;i++) {
		val[i] = 0;
	}
#if 0
    val[0] = *((MEA_Uint32*)(&(entry_pi->data)));
#else
    MEA_OS_insert_value(count_shift,
        5,
        entry_pi->data.color_bit0_loc ,
        &val[0]);
    count_shift+=5;

    MEA_OS_insert_value(count_shift,
        1,
        entry_pi->data.color_bit0_valid ,
        &val[0]);
    count_shift+=1;
    MEA_OS_insert_value(count_shift,
        5,
        entry_pi->data.color_bit1_loc ,
        &val[0]);
    count_shift+=5;

    MEA_OS_insert_value(count_shift,
        1,
        entry_pi->data.color_bit1_valid ,
        &val[0]);
    count_shift+=1;

    MEA_OS_insert_value(count_shift,
        5,
        entry_pi->data.pri_bit0_loc ,
        &val[0]);
    count_shift+=5;

    MEA_OS_insert_value(count_shift,
        1,
        entry_pi->data.pri_bit0_valid ,
        &val[0]);
    count_shift+=1;

    MEA_OS_insert_value(count_shift,
        5,
        entry_pi->data.pri_bit1_loc ,
        &val[0]);
    count_shift+=5; 
   
    MEA_OS_insert_value(count_shift,
        1,
        entry_pi->data.pri_bit1_valid ,
        &val[0]);
    count_shift+=1;
    MEA_OS_insert_value(count_shift,
        5,
        entry_pi->data.pri_bit2_loc ,
        &val[0]);
    count_shift+=5;

    MEA_OS_insert_value(count_shift,
        1,
        entry_pi->data.pri_bit2_valid ,
        &val[0]);
    count_shift+=1;
    MEA_OS_insert_value(count_shift,
        2,
        entry_pi->data.pad ,
        &val[0]);
    count_shift+=2;
#endif

   





    

	for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
	}

	if (mea_db_Set_Hw_Regs(unit_i,
		                   MEA_EHP_profile_stamping_db,
        				   hwUnit_i,
						   hwId_i,
						   num_of_regs,
						   val) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
						  __FUNCTION__,
						  hwUnit_i,
						  hwId_i);
   }


}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHw_profile_stamping>                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_EHP_UpdateHw_profile_stamping(MEA_Unit_t                        unit_i,
				 						 	            MEA_Editing_t                     id_i,
											            MEA_drv_ehp_profile_stamping_dbt* entry_pi) 
{
	MEA_ind_write_t ind_write;
	MEA_db_HwUnit_t hwUnit;
	MEA_db_HwId_t   hwId;

	/* Get the hwUnit and hwId */
	MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
		                          mea_drv_Get_EHP_profile_stamping_db,
								  id_i,
								  hwUnit,
								  hwId,
								  return MEA_ERROR);

    /* Update the HW */
 	ind_write.tableOffset	= MEA_BM_TBL_TYP_EHP_PROFILE_STAMPING_START(hwUnit) + hwId;
	ind_write.tableType		= MEA_BM_TBL_TYP_EHP_PROFILE;
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_EHP_UpdateHwEntry_profile_stamping;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long) hwId);
    ind_write.funcParam4 = (MEA_funcParam)entry_pi;
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
						  __FUNCTION__,
						  ind_write.tableType,
						  ind_write.tableOffset,
						  MEA_MODULE_BM);
		return MEA_ERROR;
    }   

	/* Return to Caller */
	return MEA_OK;

}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_profile_sa_mss>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_EHP_UpdateHwEntry_profile_sa_mss(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4,
                                                     MEA_funcParam numofregs,
                                                     MEA_funcParam retVal)
{
	MEA_Unit_t                      unit_i   = (MEA_Unit_t                     )arg1;
    MEA_db_HwUnit_t                 hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t                   hwId_i = (MEA_db_HwId_t)((long)arg3);
	MEA_drv_ehp_profile_sa_mss_dbt *entry_pi = (MEA_drv_ehp_profile_sa_mss_dbt*)arg4;
    long                     *ret_Val  = (long*)retVal;
    MEA_Uint32      val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
	MEA_Uint32      i;
	MEA_Uint16      num_of_regs;
    MEA_Uint32      writevalue=0;


	num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
		                     MEA_BM_TBL_TYP_EHP_PROFILE_NUM_OF_REGS(hwUnit_i));
	for (i=0;i<MEA_BM_IA_MAX_NUM_OF_WRITE_REGS;i++) {
		val[i] = 0;
	}

    writevalue= (MEA_Uint32)(entry_pi->sa_mss );
    val[0] = writevalue;

    
    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
	}

	if (mea_db_Set_Hw_Regs(unit_i,
		                   MEA_EHP_profile_sa_mss_db,
        				   hwUnit_i,
						   hwId_i,
						   num_of_regs,
						   val) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
						  __FUNCTION__,
						  hwUnit_i,
						  hwId_i);
   }
    if(ret_Val !=NULL)
    {
        MEA_OS_memcpy((char*)(ret_Val),
            (char*)val,
            MEA_BM_IA_MAX_NUM_OF_WRITE_REGS*sizeof(MEA_Uint32));


    }

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHw_profile_sa_mss>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_EHP_UpdateHw_profile_sa_mss(MEA_Unit_t                      unit_i,
				 						 	          MEA_Editing_t                   id_i,
											          MEA_drv_ehp_profile_sa_mss_dbt* entry_pi) 
{
	MEA_ind_write_t ind_write;
	MEA_db_HwUnit_t hwUnit;
	MEA_db_HwId_t   hwId;
    MEA_Uint32 retVal[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
#if 0 && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)
    MEA_ind_read_t                 ind_read;
    MEA_Uint32                     read_data[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32 i;
#endif

    MEA_OS_memset(&retVal,0,sizeof(retVal));

	/* Get the hwUnit and hwId */
	MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
		                          mea_drv_Get_EHP_profile_sa_mss_db,
								  id_i,
								  hwUnit,
								  hwId,
								  return MEA_ERROR);

    /* Update the HW */
 	ind_write.tableOffset	= MEA_BM_TBL_TYP_EHP_PROFILE_SA_MSS_START(hwUnit) + hwId;
	ind_write.tableType		= ENET_BM_TBL_TYP_EHP_PROFILE;
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_EHP_UpdateHwEntry_profile_sa_mss;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long) hwId);
    ind_write.funcParam4 = (MEA_funcParam)entry_pi;
    ind_write.numofregs = (MEA_funcParam)((long)MEA_BM_IA_MAX_NUM_OF_WRITE_REGS);
    ind_write.retval = (MEA_funcParam)&retVal;

	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
						  __FUNCTION__,
						  ind_write.tableType,
						  ind_write.tableOffset,
						  MEA_MODULE_BM);
		return MEA_ERROR;
    }

	/* Return to Caller */
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_profile_ether_type>             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_EHP_UpdateHwEntry_profile_ether_type(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4,
                                                         MEA_funcParam numofregs,
                                                         MEA_funcParam retVal)
{
	MEA_Unit_t                          unit_i   = (MEA_Unit_t                         )arg1;
    MEA_db_HwUnit_t                     hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t                       hwId_i = (MEA_db_HwId_t)((long)arg3);
	MEA_drv_ehp_profile_ether_type_dbt *entry_pi = (MEA_drv_ehp_profile_ether_type_dbt*)arg4;
    long                         *ret_Val  = (long*)retVal;
    MEA_Uint32      val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
	MEA_Uint32      i;
	MEA_Uint16      num_of_regs;

	num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
		                     MEA_BM_TBL_TYP_EHP_PROFILE_NUM_OF_REGS(hwUnit_i));
	for (i=0;i<MEA_BM_IA_MAX_NUM_OF_WRITE_REGS;i++) {
		val[i] = 0;
	}

	val[0] = (MEA_Uint32)(entry_pi->ether_type);

	for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
	}

	if (mea_db_Set_Hw_Regs(unit_i,
		                   MEA_EHP_profile_ether_type_db,
        				   hwUnit_i,
						   hwId_i,
						   num_of_regs,
						   val) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
						  __FUNCTION__,
						  hwUnit_i,
						  hwId_i);
   }
    if(ret_Val !=NULL)
    {
        MEA_OS_memcpy((char*)(ret_Val),
            (char*)val,
            MEA_BM_IA_MAX_NUM_OF_WRITE_REGS*sizeof(MEA_Uint32));


    }

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHw_profile_ether_type>                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_EHP_UpdateHw_profile_ether_type(MEA_Unit_t                          unit_i,
				 						 	              MEA_Editing_t                       id_i,
											              MEA_drv_ehp_profile_ether_type_dbt* entry_pi) 
{
	MEA_ind_write_t ind_write;
	MEA_db_HwUnit_t hwUnit;
	MEA_db_HwId_t   hwId;
    MEA_Uint32 retVal[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
#if 0 && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)
    MEA_ind_read_t                 ind_read;
    MEA_Uint32                     read_data[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32 i;
#endif

    MEA_OS_memset(&retVal,0,sizeof(retVal));

	/* Get the hwUnit and hwId */
	MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
		                          mea_drv_Get_EHP_profile_ether_type_db,
								  id_i,
								  hwUnit,
								  hwId,
								  return MEA_ERROR);

    /* Update the HW */
 	ind_write.tableOffset	= MEA_BM_TBL_TYP_EHP_PROFILE_ETHER_TYPE_START(hwUnit) + hwId;
	ind_write.tableType		= ENET_BM_TBL_TYP_EHP_PROFILE;
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_EHP_UpdateHwEntry_profile_ether_type;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long) hwId);
    ind_write.funcParam4 = (MEA_funcParam)entry_pi;
    ind_write.numofregs  = (MEA_funcParam)((long)MEA_BM_IA_MAX_NUM_OF_WRITE_REGS);
    ind_write.retval     = (MEA_funcParam)&retVal;

	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
						  __FUNCTION__,
						  ind_write.tableType,
						  ind_write.tableOffset,
						  MEA_MODULE_BM);
		return MEA_ERROR;
    }
//AlexTDM
#if 0
    if(MEA_TDM_SUPPORT == MEA_TRUE && MEA_READ_PACKET_TYPE == MEA_TRUE)  { 

#if !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)

        //check if the write is succeed by read
        for(i=0;i<5;i++){
            ind_read.cmdReg		    = MEA_BM_IA_CMD;      
            ind_read.cmdMask		= MEA_BM_IA_CMD_MASK;      
            ind_read.statusReg		= MEA_BM_IA_STAT;   
            ind_read.statusMask	    = MEA_BM_IA_STAT_MASK;   
            ind_read.tableAddrReg	= MEA_BM_IA_ADDR;
            ind_read.tableAddrMask  = MEA_BM_IA_ADDR_OFFSET_MASK;
            ind_read.tableOffset	= hwId;
            ind_read.read_data	    = &read_data[0];

            ind_read.tableType=  MEA_BM_TBL_TYP_EHP_ETH;      

            if (MEA_API_ReadIndirect(MEA_UNIT_0,
                &ind_read,
                MEA_NUM_OF_ELEMENTS(read_data),
                MEA_MODULE_BM) != MEA_OK) {                 
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - ReadIndirect from tableType %d / tableOffset %d module BM failed "
                        "(pmId=%d)\n",
                        __FUNCTION__,ind_read.tableType,ind_read.tableOffset,id_i);
                    return MEA_ERROR;
            }

            if( MEA_OS_memcmp(&read_data,&retVal,sizeof(read_data)) == 0){
                break;
            }else{
                //write again
                if(i==4){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"The EHP_UpdateHwEntry_ether_type fail on read 5 time \n");
                    break;
                }
                if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
                        __FUNCTION__,
                        ind_write.tableType,
                        ind_write.tableOffset,
                        MEA_MODULE_BM);
                    return MEA_ERROR;
                }
            }
        }
#endif
    }//TDM support
#endif
	/* Return to Caller */
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_fragmentSession>                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_EHP_UpdateHwEntry_fragmentSession(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                      unit_i   = (MEA_Unit_t                     )arg1;
    MEA_db_HwUnit_t                 hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t                   hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_drv_ehp_fragmentSession_dbt *entry_pi = (MEA_drv_ehp_fragmentSession_dbt*)arg4;
    MEA_Uint32                 val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32                 i;
    MEA_Uint16                 num_of_regs;
    MEA_Uint32                 count_shift;

    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
        MEA_BM_TBL_EHP_FRAGMENT_NUM_OF_REGS(hwUnit_i));
    for (i=0;i<num_of_regs;i++) {
        val[i] = 0;
    }
    
    if (entry_pi->valid) {
        count_shift = 0;

        MEA_OS_insert_value(count_shift,
            MEA_BM_TBL_EHP_FRAGMENT_SESSION_SESSION_ID_WIDTH,
            entry_pi->session_num,
            &val[0]);
        count_shift += MEA_BM_TBL_EHP_FRAGMENT_SESSION_SESSION_ID_WIDTH;


        MEA_OS_insert_value(count_shift,
            MEA_BM_TBL_EHP_FRAGMENT_SESSION_SESSION_VALID_WIDTH,
            entry_pi->enable,
            &val[0]);
        count_shift += MEA_BM_TBL_EHP_FRAGMENT_SESSION_SESSION_VALID_WIDTH;


        MEA_OS_insert_value(count_shift,
            MEA_BM_TBL_EHP_FRAGMENT_SESSION_EX_ENABLE_WIDTH,
            entry_pi->extract_enable,
            &val[0]);
        count_shift += MEA_BM_TBL_EHP_FRAGMENT_SESSION_EX_ENABLE_WIDTH;
    }

    
   
    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," value[%d] 0x%08x\n",i,val[0]);
     }

    if (mea_db_Set_Hw_Regs(unit_i,
        MEA_EHP_fragmentSession_db,
        hwUnit_i,
        hwId_i,
        num_of_regs,
        val) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                __FUNCTION__,
                hwUnit_i,
                hwId_i);
    }


}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHw_fragmentSession>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_EHP_UpdateHw_fragmentSession(MEA_Unit_t                  unit_i,
                                                  MEA_Editing_t               id_i,
                                                  MEA_drv_ehp_fragmentSession_dbt* entry_pi)
{
    MEA_ind_write_t ind_write;
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId;

    /* Get the hwUnit and hwId */
    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_fragmentSession_db,
        id_i,
        hwUnit,
        hwId,
        return MEA_ERROR);

    /* Update the HW */
    ind_write.tableOffset	= hwId;
    ind_write.tableType		= ENET_BM_TBL_EHP_FRAGMENT_SESSION;
    ind_write.cmdReg		= MEA_BM_IA_CMD;      
    ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg		= MEA_BM_IA_STAT;   
    ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
    ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_EHP_UpdateHwEntry_fragmentSession;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long) hwId);
    ind_write.funcParam4 = (MEA_funcParam)entry_pi;
    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            MEA_MODULE_BM);
        return MEA_ERROR;
    }   

    /* Return to Caller */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_pw_control>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_EHP_UpdateHwEntry_pw_control(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4,
                                                      MEA_funcParam numofregs,
                                                      MEA_funcParam retVal)
{
    MEA_Unit_t                      unit_i   = (MEA_Unit_t                     )arg1;
    MEA_db_HwUnit_t                 hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t                   hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_drv_ehp_pw_control_dbt *entry_pi = (MEA_drv_ehp_pw_control_dbt*)arg4;
    long *ret_Val = (long*)retVal;	

    MEA_Uint32                 val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32                 i;
    MEA_Uint16                 num_of_regs;
    MEA_Uint32                 count_shift;

    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
        ENET_BM_TBL_EHP_PW_CONTROL_NUM_OF_REGS(hwUnit_i));
    for (i=0;i<MEA_BM_IA_MAX_NUM_OF_WRITE_REGS;i++) {
        val[i] = 0;
    }

    if (entry_pi->valid) {
       
       count_shift = 0;

       MEA_OS_insert_value(count_shift,
           3, 
           entry_pi->pw_control.clock_resolution,
           &val[0]);
       count_shift+=3;   
       MEA_OS_insert_value(count_shift,
           2,
           entry_pi->pw_control.clock_domain,
           &val[0]);
       count_shift+=2;
       MEA_OS_insert_value(count_shift,
           1,
           entry_pi->pw_control.rtp_exist,
           &val[0]);
       count_shift+=1;
       MEA_OS_insert_value(count_shift,
           1,
           entry_pi->pw_control.vlan_exist,
           &val[0]);
       count_shift+=1;
       MEA_OS_insert_value(count_shift,
           2,
           entry_pi->pw_control.ces_type,
           &val[0]);
       count_shift+=2;
       MEA_OS_insert_value(count_shift,
           1,
           entry_pi->pw_control.loss_R,
           &val[0]);
       count_shift+=1;
       MEA_OS_insert_value(count_shift,
           1,
           entry_pi->pw_control.loss_L,
           &val[0]);
       count_shift+=1;
       MEA_OS_insert_value(count_shift,
           1,
           entry_pi->pw_control.valid,
           &val[0]);
       count_shift+=1;


     }


    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," value[%d] 0x%08x\n",i,val[0]);
    }

    if (mea_db_Set_Hw_Regs(unit_i,
        MEA_EHP_pw_control_db,
        hwUnit_i,
        hwId_i,
        num_of_regs,
        val) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                __FUNCTION__,
                hwUnit_i,
                hwId_i);
    }
    if(ret_Val != NULL)
    {
        MEA_OS_memcpy((char*)(ret_Val),
            (char*)val,
            MEA_BM_IA_MAX_NUM_OF_WRITE_REGS*sizeof(MEA_Uint32));


    }

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHw_pw_control>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_EHP_UpdateHw_pw_control(MEA_Unit_t                  unit_i,
                                                  MEA_Editing_t               id_i,
                                                  MEA_drv_ehp_pw_control_dbt* entry_pi)
{
    MEA_ind_write_t ind_write;
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId;
    MEA_Uint32 retVal[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
#if 0 && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)
    MEA_ind_read_t                 ind_read;
    MEA_Uint32                     read_data[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32 i;
#endif
MEA_OS_memset(&retVal,0,sizeof(retVal));
    /* Get the hwUnit and hwId */
    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_pw_control_db,
        id_i,
        hwUnit,
        hwId,
        return MEA_ERROR);

    /* Update the HW */
    ind_write.tableOffset   = hwId;
    ind_write.tableType     = ENET_BM_TBL_EHP_PW_CONTROL;
    ind_write.cmdReg        = MEA_BM_IA_CMD;      
    ind_write.cmdMask       = MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg     = MEA_BM_IA_STAT;   
    ind_write.statusMask    = MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg  = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_EHP_UpdateHwEntry_pw_control;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long) hwId);
    ind_write.funcParam4 = (MEA_funcParam)entry_pi;
    ind_write.numofregs = (MEA_funcParam)((long)MEA_BM_IA_MAX_NUM_OF_WRITE_REGS);
    ind_write.retval = (MEA_funcParam)&retVal;
   
    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            MEA_MODULE_BM);
        return MEA_ERROR;
    }
   
    /* Return to Caller */
    return MEA_OK;

}





//////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_mpls_label>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_EHP_UpdateHwEntry_mpls_label(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4,
                                                 MEA_funcParam numofregs,
                                                 MEA_funcParam retVal)
{
    MEA_Unit_t                      unit_i   = (MEA_Unit_t                     )arg1;
    MEA_db_HwUnit_t                 hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t                   hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_drv_ehp_mpls_label_dbt *entry_pi = (MEA_drv_ehp_mpls_label_dbt*)arg4;
    MEA_Uint32 *ret_Val = (MEA_Uint32*)retVal;
    MEA_Uint32                 val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32                 i;
    MEA_Uint16                 num_of_regs;
//    MEA_Uint32                 count_shift;

    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
        ENET_BM_TBL_EHP_MPLS_LABEL_NUM_OF_REGS(hwUnit_i));
    for (i=0;i<MEA_BM_IA_MAX_NUM_OF_WRITE_REGS;i++) {
        val[i] = 0;
    }

    if (entry_pi->valid) {

//        count_shift = 0;
       val[0]= entry_pi->mpls_label.lable;
 
    }


    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," value[%d] 0x%08x\n",i,val[0]);
    }

    if (mea_db_Set_Hw_Regs(unit_i,
        MEA_EHP_mpls_label_db,
        hwUnit_i,
        hwId_i,
        num_of_regs,
        val) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                __FUNCTION__,
                hwUnit_i,
                hwId_i);
    }
    if(ret_Val !=NULL)
    {
        MEA_OS_memcpy((char*)(ret_Val),
            (char*)val,
            MEA_BM_IA_MAX_NUM_OF_WRITE_REGS*sizeof(MEA_Uint32));


    }

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHw_pw_control>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_EHP_UpdateHw_mpls_label(MEA_Unit_t                  unit_i,
                                                  MEA_Editing_t               id_i,
                                                  MEA_drv_ehp_mpls_label_dbt* entry_pi)
{
    MEA_ind_write_t ind_write;
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId;
MEA_Uint32 retVal[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];   
#if 0 && !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)
    MEA_ind_read_t                 ind_read;
    MEA_Uint32                     read_data[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32 i;
#endif
MEA_OS_memset(&retVal,0,sizeof(retVal));
    /* Get the hwUnit and hwId */
    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_mpls_label_db,
        id_i,
        hwUnit,
        hwId,
        return MEA_ERROR);

    /* Update the HW */
    ind_write.tableOffset   = hwId;
    ind_write.tableType     = ENET_BM_TBL_EHP_MPLS_LABEL;
    ind_write.cmdReg        = MEA_BM_IA_CMD;      
    ind_write.cmdMask       = MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg     = MEA_BM_IA_STAT;   
    ind_write.statusMask    = MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg  = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_EHP_UpdateHwEntry_mpls_label;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long) hwId);
    ind_write.funcParam4 = (MEA_funcParam)entry_pi;
    ind_write.numofregs = (MEA_funcParam)((long)MEA_BM_IA_MAX_NUM_OF_WRITE_REGS);
    ind_write.retval    = (MEA_funcParam)&retVal;
   
    
    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            MEA_MODULE_BM);
        return MEA_ERROR;
    }
 //AlexTDM
#if 0
    if(MEA_TDM_SUPPORT == MEA_TRUE && MEA_READ_PACKET_TYPE == MEA_TRUE)  { 

#if !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)

        //check if the write is succeed by read
        for(i=0;i<5;i++){
            ind_read.cmdReg		    = MEA_BM_IA_CMD;      
            ind_read.cmdMask		= MEA_BM_IA_CMD_MASK;      
            ind_read.statusReg		= MEA_BM_IA_STAT;   
            ind_read.statusMask	    = MEA_BM_IA_STAT_MASK;   
            ind_read.tableAddrReg	= MEA_BM_IA_ADDR;
            ind_read.tableAddrMask  = MEA_BM_IA_ADDR_OFFSET_MASK;
            ind_read.tableOffset	= hwId;
            ind_read.read_data	    = &read_data[0];

            ind_read.tableType=  ENET_BM_TBL_EHP_MPLS_LABEL;      

            if (MEA_API_ReadIndirect(MEA_UNIT_0,
                &ind_read,
                MEA_NUM_OF_ELEMENTS(read_data),
                MEA_MODULE_BM) != MEA_OK) {                 
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - ReadIndirect from tableType %d / tableOffset %d module BM failed "
                        "(pmId=%d)\n",
                        __FUNCTION__,ind_read.tableType,ind_read.tableOffset,id_i);
                    return MEA_ERROR;
            }

            if( MEA_OS_memcmp(&read_data,&retVal,sizeof(read_data)) == 0){
                break;
            }else{
                //write again
                if(i==4){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"The EHP_UpdateHwEntry_mpls_label fail on read 5 time \n");
                    break;
                }
                if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
                        __FUNCTION__,
                        ind_write.tableType,
                        ind_write.tableOffset,
                        MEA_MODULE_BM);
                    return MEA_ERROR;
                }




            }


        }
#endif
    }//TDM support
#endif

    /* Return to Caller */
    return MEA_OK;

}








/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_profile2Id>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHwEntry_profile2Id>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_EHP_UpdateHwEntry_profile2Id_Lm(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                  unit_i   = (MEA_Unit_t                 )arg1;
    //MEA_db_HwUnit_t             hwUnit_i = (MEA_db_HwUnit_t            )arg2;
    //MEA_db_HwId_t               hwId_i   = (MEA_db_HwId_t              )((MEA_Uint32)arg3);
    MEA_Uint32                  value    = (MEA_Uint32)((long)arg4);
    MEA_Uint32                 val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32                 i;
    MEA_Uint16                 num_of_regs;
    //	MEA_db_HwId_t              hwId;
//    MEA_Uint32                 count_shift;

    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
        MEA_BM_TBL_TYP_EHP_PROFILE2_LM_NUM_OF_REGS(hwUnit_i));
    for (i=0;i<num_of_regs;i++) {
        val[i] = 0;
    }

     val[0] = value;
     val[1] = value;
     val[2] = value;

    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }


  

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHw_profile2_Lm>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_EHP_UpdateHw_profile2_Lm(MEA_Unit_t                  unit_i,
                                                   MEA_LmId_t               id_i,
                                                   MEA_Uint32 value) 
{
   /************************************************************************/
   /* Only to clear the counter                                            */
   /************************************************************************/
    MEA_Uint32      i;
    MEA_ind_write_t ind_write;
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId;


    /* Get the hwUnit and hwId */
    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
        mea_drv_Get_EHP_profile2_lm_db,
        id_i,
        hwUnit,
        hwId,
        return MEA_ERROR);



    /* Update the HW */
    ind_write.tableOffset	= hwId;
    ind_write.tableType		= ENET_BM_TBL_TYP_EHP_PROFILE2_LM;
    ind_write.cmdReg		= MEA_BM_IA_CMD;      
    ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg		= MEA_BM_IA_STAT;   
    ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
    ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_EHP_UpdateHwEntry_profile2Id_Lm;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long) hwId);
    ind_write.funcParam4 = (MEA_funcParam)((long) value);
    
    ind_write.tableOffset	= hwId;
// need to check if the BM_bus is 128bit we will make it one 

    if(MEA_DRV_BM_BUS_WIDTH == 0) {
        for(i=0;i<4;i++){
            ind_write.tableOffset=(hwId*4)+i;
            ind_write.funcParam4 = (MEA_funcParam)((long)value);
            if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
                    __FUNCTION__,
                    ind_write.tableType,
                    ind_write.tableOffset,
                    MEA_MODULE_BM);
                return MEA_ERROR;
            }   
        }
    } else {
        if ((MEA_DRV_BM_BUS_WIDTH == 1) || (MEA_DRV_BM_BUS_WIDTH == 2) ){
            for(i=0;i<2;i++){
                ind_write.tableOffset=(hwId*2)+i;
                ind_write.funcParam4 = (MEA_funcParam)((long)value);
                if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
                        __FUNCTION__,
                        ind_write.tableType,
                        ind_write.tableOffset,
                        MEA_MODULE_BM);
                    return MEA_ERROR;
                }   
            }  

        } else {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," %s \n BM BUS width %d  not support",__FUNCTION__ ,MEA_DRV_BM_BUS_WIDTH);
            return MEA_ERROR;

        }
    }
    /* Return to Caller */
    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_EHP_UpdateHw>                                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_EHP_UpdateHw(MEA_Unit_t                          unit_i,
                                       MEA_Editing_t                       id_i) 
{

	MEA_Editing_t   id;
	MEA_db_HwUnit_t hwUnit;

	/* Get the hwUnit */
	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);


	/* Update Hw - ETH */
	id = id_i;
	if (mea_drv_EHP_UpdateHw_ETH(unit_i,
		                         id,
								 &(MEA_EHP_Table[id].editing_info)) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_EHP_UpdateHw_ETH failed (id=%d) \n",
						  __FUNCTION__,
						  id);
		return MEA_ERROR;
	}


    /* Return to Caller */
	return MEA_OK;

}



/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Conclude_EHP>                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Conclude_EHP(MEA_Unit_t unit_i)
{
	if (MEA_EHP_Table != NULL) {
		MEA_OS_free(MEA_EHP_Table);
		MEA_EHP_Table = NULL;
	}

	if (MEA_EHP_da_Table != NULL) {
		MEA_OS_free(MEA_EHP_da_Table);
		MEA_EHP_da_Table = NULL;
	}

	if (MEA_EHP_sa_lss_Table != NULL) {
		MEA_OS_free(MEA_EHP_sa_lss_Table);
		MEA_EHP_sa_lss_Table = NULL;
	}

	if (MEA_EHP_profileId_Table != NULL) {
		MEA_OS_free(MEA_EHP_profileId_Table);
		MEA_EHP_profileId_Table = NULL;
	}

	if (MEA_EHP_profile_stamping_Table != NULL) {
		MEA_OS_free(MEA_EHP_profile_stamping_Table);
		MEA_EHP_profile_stamping_Table = NULL;
	}



	if (MEA_EHP_profile_sa_mss_Table != NULL) {
		MEA_OS_free(MEA_EHP_profile_sa_mss_Table);
		MEA_EHP_profile_sa_mss_Table = NULL;
	}

	if (MEA_EHP_profile_ether_type_Table != NULL) {
		MEA_OS_free(MEA_EHP_profile_ether_type_Table);
		MEA_EHP_profile_ether_type_Table = NULL;
	}
    if (MEA_EHP_fragmentSession_Table != NULL) {
        MEA_OS_free(MEA_EHP_fragmentSession_Table);
        MEA_EHP_fragmentSession_Table = NULL;
    }
    if (MEA_EHP_pw_control_Table != NULL) {
        MEA_OS_free(MEA_EHP_pw_control_Table);
        MEA_EHP_pw_control_Table = NULL;
    }

	if (MEA_EHP_profile2Id_Table != NULL) {
		MEA_OS_free(MEA_EHP_profile2Id_Table);
		MEA_EHP_profile2Id_Table = NULL;
	}

	if (MEA_EHP_profile2_lm_Table != NULL) {
		MEA_OS_free(MEA_EHP_profile2_lm_Table);
		MEA_EHP_profile2_lm_Table = NULL;
	}

	if (mea_drv_ehp_mc_grouping_tbl != NULL) {
		MEA_OS_free(mea_drv_ehp_mc_grouping_tbl);
		mea_drv_ehp_mc_grouping_tbl = NULL;
	}

	if (mea_db_Conclude(unit_i,&MEA_EHP_ETH_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP ETH) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (mea_db_Conclude(unit_i,&MEA_EHP_ATM_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP ATM) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
    if (mea_db_Conclude(unit_i,&MEA_EHP_WBRG_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_dn_Conclude (EHP WBRG) failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

	if (mea_db_Conclude(unit_i,&MEA_EHP_da_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP da) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (mea_db_Conclude(unit_i,&MEA_EHP_sa_lss_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP sa_lss) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (mea_db_Conclude(unit_i,&MEA_EHP_profileId_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP profileId) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (mea_db_Conclude(unit_i,&MEA_EHP_profile_stamping_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP profile_stamping) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}



	if (mea_db_Conclude(unit_i,&MEA_EHP_profile_sa_mss_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP profile_sa_mss) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (mea_db_Conclude(unit_i,&MEA_EHP_profile_ether_type_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP profile_ether_type) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
    if (mea_db_Conclude(unit_i,&MEA_EHP_fragmentSession_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_dn_Conclude (EHP fragmentSession_db) failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
	if (mea_db_Conclude(unit_i,&MEA_EHP_profile2Id_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP profile2Id) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (mea_db_Conclude(unit_i,&MEA_EHP_profile2_lm_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude (EHP profile2_lm) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_EHP_XXX_num_of_sw_size_func>             */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_ETH_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EDIT_ID,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_ATM_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EDIT_ID,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_WBRG_num_of_sw_size_func,
                                         MEA_MAX_NUM_OF_EDIT_ID,
                                         0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_da_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EHP_DA,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_sa_lss_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EHP_SA_LSS,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profileId_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EHP_PROFILE_ID,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_stamping_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EHP_PROFILE_STAMPING,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_sa_dss_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EHP_PROFILE_SA_DSS,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_sa_mss_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EHP_PROFILE_SA_MSS,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_ether_type_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EHP_PROFILE_ETHER_TYPE,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile2Id_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EHP_PROFILE2_ID,
										 0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile2_lm_num_of_sw_size_func,
										 MEA_MAX_NUM_OF_EHP_PROFILE2_LM,
										 0)

MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_fragmentSession_num_of_sw_size_func,
                                         MEA_MAX_NUM_OF_EDIT_ID,
                                         0)
 MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_pw_control_num_of_sw_size_func,
                                         MEA_MAX_NUM_OF_EDIT_ID,
                                         0)
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_mpls_label_num_of_sw_size_func,
                                         MEA_MAX_NUM_OF_EDIT_ID,
                                         0)



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_EHP_XXX_num_of_hw_size_func>             */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_ETH_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_ETH_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_ETH_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_ATM_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_ATM_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_ATM_NUM_OF_REGS(hwUnit_i))

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_WBRG_num_of_hw_size_func,
                                         MEA_BM_TBL_TYP_EHP_WBRG_LENGTH(hwUnit_i),
                                         MEA_BM_TBL_TYP_EHP_WBRG_NUM_OF_REGS(hwUnit_i))

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_da_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_DA_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_DA_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_sa_lss_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_SA_LSS_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_SA_LSS_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profileId_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_PROFILE_ID_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_PROFILE_ID_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_stamping_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_PROFILE_STAMPING_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_PROFILE_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_sa_dss_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_PROFILE_SA_DSS_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_PROFILE_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_sa_mss_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_PROFILE_SA_MSS_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_PROFILE_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_ether_type_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_PROFILE_ETHER_TYPE_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_PROFILE_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile2Id_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_PROFILE2_ID_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_PROFILE2_ID_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile2_lm_num_of_hw_size_func,
										 MEA_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH(hwUnit_i),
										 MEA_BM_TBL_TYP_EHP_PROFILE2_LM_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_fragmentSession_num_of_hw_size_func,
                                         MEA_BM_TBL_EHP_FRAGMENT_LENGTH(hwUnit_i),
                                         MEA_BM_TBL_EHP_FRAGMENT_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_pw_control_num_of_hw_size_func,
                                         MEA_BM_TBL_EHP_PW_CONTROL_LENGTH(hwUnit_i),
                                         MEA_BM_TBL_EHP_PW_CONTROL_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EHP_mpls_label_num_of_hw_size_func,
                                         MEA_BM_TBL_EHP_MPLS_LABEL_LENGTH(hwUnit_i),
                                         MEA_BM_TBL_EHP_MPLS_LABEL_NUM_OF_REGS(hwUnit_i))
                                         

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Init_EHP>                                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_EHP(MEA_Unit_t unit_i)
{
    MEA_Status ret=MEA_OK;
	MEA_Uint32    size;

	/* Init db - Hardware shadow */
	if (mea_db_Init(unit_i,
	                "EHP ETH ",
                    mea_drv_Get_EHP_ETH_num_of_sw_size_func,
		            mea_drv_Get_EHP_ETH_num_of_hw_size_func,
					&MEA_EHP_ETH_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP ETH) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
	if (mea_db_Init(unit_i,
	                "EHP ATM ",
                    mea_drv_Get_EHP_ATM_num_of_sw_size_func,
		            mea_drv_Get_EHP_ATM_num_of_hw_size_func,
					&MEA_EHP_ATM_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP ATM) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
    if (mea_db_Init(unit_i,
        "EHP WBRG ",
        mea_drv_Get_EHP_WBRG_num_of_sw_size_func,
        mea_drv_Get_EHP_WBRG_num_of_hw_size_func,
        &MEA_EHP_WBRG_db) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Init (EHP WBRG) failed \n",
                __FUNCTION__);
            return MEA_ERROR;
    }
	if (mea_db_Init(unit_i,
	                "EHP da ",
                    mea_drv_Get_EHP_da_num_of_sw_size_func,
		            mea_drv_Get_EHP_da_num_of_hw_size_func,
					&MEA_EHP_da_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP da) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
	if (mea_db_Init(unit_i,
	                "EHP sa_lss ",
                    mea_drv_Get_EHP_sa_lss_num_of_sw_size_func,
		            mea_drv_Get_EHP_sa_lss_num_of_hw_size_func,
					&MEA_EHP_sa_lss_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP sa_lss) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
	if (mea_db_Init(unit_i,
	                "EHP profileId ",
                    mea_drv_Get_EHP_profileId_num_of_sw_size_func,
		            mea_drv_Get_EHP_profileId_num_of_hw_size_func,
					&MEA_EHP_profileId_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP profileId) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
	if (mea_db_Init(unit_i,
	                "EHP profile_stamping ",
                    mea_drv_Get_EHP_profile_stamping_num_of_sw_size_func,
		            mea_drv_Get_EHP_profile_stamping_num_of_hw_size_func,
					&MEA_EHP_profile_stamping_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP profile_stamping) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (mea_db_Init(unit_i,
	                "EHP profile_sa_mss ",
                    mea_drv_Get_EHP_profile_sa_mss_num_of_sw_size_func,
		            mea_drv_Get_EHP_profile_sa_mss_num_of_hw_size_func,
					&MEA_EHP_profile_sa_mss_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP profile_sa_mss) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
	if (mea_db_Init(unit_i,
	                "EHP profile_ether_type ",
                    mea_drv_Get_EHP_profile_ether_type_num_of_sw_size_func,
		            mea_drv_Get_EHP_profile_ether_type_num_of_hw_size_func,
					&MEA_EHP_profile_ether_type_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP profile_ether_type) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
	if (mea_db_Init(unit_i,
	                "EHP profile2Id ",
                    mea_drv_Get_EHP_profile2Id_num_of_sw_size_func,
		            mea_drv_Get_EHP_profile2Id_num_of_hw_size_func,
					&MEA_EHP_profile2Id_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP profile2Id) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
	if (mea_db_Init(unit_i,
	                "EHP profile2_lm ",
                    mea_drv_Get_EHP_profile2_lm_num_of_sw_size_func,
		            mea_drv_Get_EHP_profile2_lm_num_of_hw_size_func,
					&MEA_EHP_profile2_lm_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init (EHP profile2_lm) failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
    if (mea_db_Init(unit_i,
        "EHP fragmentSession ",
        mea_drv_Get_EHP_fragmentSession_num_of_sw_size_func,
        mea_drv_Get_EHP_fragmentSession_num_of_hw_size_func,
        &MEA_EHP_fragmentSession_db) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Init (EHP fragmentSession) failed \n",
                __FUNCTION__);
            return MEA_ERROR;
    }
    if (mea_db_Init(unit_i,
        "EHP pw_control ",
        mea_drv_Get_EHP_pw_control_num_of_sw_size_func,
        mea_drv_Get_EHP_pw_control_num_of_hw_size_func,
        &MEA_EHP_pw_control_db) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Init (EHP pw_control) failed \n",
                __FUNCTION__);
            return MEA_ERROR;
    }
    if (mea_db_Init(unit_i,
        "EHP mpls_label ",
        mea_drv_Get_EHP_mpls_label_num_of_sw_size_func,
        mea_drv_Get_EHP_mpls_label_num_of_hw_size_func,
        &MEA_EHP_mpls_label_db) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Init (EHP mpls_label) failed \n",
                __FUNCTION__);
            return MEA_ERROR;
    }

  
    

	/* Allocate MEA_EHP_Table */
	if (MEA_EHP_Table == NULL) {
        size = sizeof(MEA_drv_ehp_dbt);
		size = MEA_MAX_NUM_OF_EDIT_ID * sizeof(MEA_drv_ehp_dbt);
		MEA_EHP_Table = (MEA_drv_ehp_dbt*)MEA_OS_malloc(size);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG," MEA_EHP_Table size=%d \n",size);
		if (MEA_EHP_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_Table,0,size);
	}
	/* Allocate MEA_EHP_session_Table */

	if (MEA_EHP_fragmentSession_Table == NULL) {
		size = MEA_MAX_NUM_OF_EDIT_ID * sizeof(MEA_drv_ehp_fragmentSession_dbt);
		MEA_EHP_fragmentSession_Table = (MEA_drv_ehp_fragmentSession_dbt*)MEA_OS_malloc(size);
		if (MEA_EHP_fragmentSession_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_fragmentSession_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_fragmentSession_Table,0,size);
	}

    if (MEA_EDITING_PW_CONTROL_SUPPORT) {
        /* Allocate MEA_EHP_pw_control_Table */
        if (MEA_EHP_pw_control_Table == NULL) {
            size = MEA_MAX_NUM_OF_EDIT_ID * sizeof(MEA_drv_ehp_pw_control_dbt);
            MEA_EHP_pw_control_Table = (MEA_drv_ehp_pw_control_dbt*)MEA_OS_malloc(size);
            if (MEA_EHP_pw_control_Table == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Allocate MEA_EHP_pw_control_Table failed (size=%d)\n",
                    __FUNCTION__,
                    size);
                return MEA_ERROR;
            }
            MEA_OS_memset(MEA_EHP_pw_control_Table, 0, size);
        }
    }

    if (MEA_EHP_mpls_label_Table == NULL) {
        size = MEA_MAX_NUM_OF_EDIT_ID * sizeof(MEA_drv_ehp_mpls_label_dbt);
        MEA_EHP_mpls_label_Table = (MEA_drv_ehp_mpls_label_dbt*)MEA_OS_malloc(size);
        if (MEA_EHP_mpls_label_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_EHP_mpls_label_Table failed (size=%d)\n",
                __FUNCTION__,
                size);
            return MEA_ERROR;
        }
        MEA_OS_memset(MEA_EHP_mpls_label_Table,0,size);
    }

	/* Allocate MEA_EHP_da_Table */
	if (MEA_EHP_da_Table == NULL) {
		size = MEA_MAX_NUM_OF_EHP_DA * sizeof(MEA_drv_ehp_da_dbt);
		MEA_EHP_da_Table = (MEA_drv_ehp_da_dbt*)MEA_OS_malloc(size);
		if (MEA_EHP_da_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_da_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_da_Table,0,size);
	}

	/* Allocate MEA_EHP_sa_lss_Table */
	if (MEA_EHP_sa_lss_Table == NULL) {
		size = MEA_MAX_NUM_OF_EHP_SA_LSS * sizeof(MEA_drv_ehp_sa_lss_dbt);
		MEA_EHP_sa_lss_Table = (MEA_drv_ehp_sa_lss_dbt*)MEA_OS_malloc(size);
		if (MEA_EHP_sa_lss_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_sa_lss_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_sa_lss_Table,0,size);
	}

	/* Allocate MEA_EHP_profileId_Table */
	if (MEA_EHP_profileId_Table == NULL) {
		size = MEA_MAX_NUM_OF_EHP_PROFILE_ID * sizeof(MEA_drv_ehp_profileId_dbt);
		MEA_EHP_profileId_Table = (MEA_drv_ehp_profileId_dbt*)MEA_OS_malloc(size);
		if (MEA_EHP_profileId_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_profileId_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_profileId_Table,0,size);
	}

	/* Allocate MEA_EHP_profile_stamping_Table */
	if (MEA_EHP_profile_stamping_Table == NULL) {
		size = MEA_MAX_NUM_OF_EHP_PROFILE_STAMPING * 
			   sizeof(MEA_drv_ehp_profile_stamping_dbt);
		MEA_EHP_profile_stamping_Table = (MEA_drv_ehp_profile_stamping_dbt*)MEA_OS_malloc(size);
		if (MEA_EHP_profile_stamping_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_profile_stamping_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_profile_stamping_Table,0,size);
	}



	/* Allocate MEA_EHP_profile_sa_mss_Table */
	if (MEA_EHP_profile_sa_mss_Table == NULL) {
		size = MEA_MAX_NUM_OF_EHP_PROFILE_SA_MSS * 
			   sizeof(MEA_drv_ehp_profile_sa_mss_dbt);
		MEA_EHP_profile_sa_mss_Table = (MEA_drv_ehp_profile_sa_mss_dbt*)MEA_OS_malloc(size);
		if (MEA_EHP_profile_sa_mss_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_profile_sa_mss_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_profile_sa_mss_Table,0,size);
	}

	/* Allocate MEA_EHP_profile_ether_type_Table */
	if (MEA_EHP_profile_ether_type_Table == NULL) {
		size = MEA_MAX_NUM_OF_EHP_PROFILE_ETHER_TYPE * 
			   sizeof(MEA_drv_ehp_profile_ether_type_dbt);
		MEA_EHP_profile_ether_type_Table = (MEA_drv_ehp_profile_ether_type_dbt*)MEA_OS_malloc(size);
		if (MEA_EHP_profile_ether_type_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_profile_ether_type_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_profile_ether_type_Table,0,size);
	}


	

	/* Allocate MEA_EHP_profile2Id_Table */
	if (MEA_EHP_profile2Id_Table == NULL) {
		size = MEA_MAX_NUM_OF_EHP_PROFILE2_ID * sizeof(MEA_drv_ehp_profile2Id_dbt);
		MEA_EHP_profile2Id_Table = (MEA_drv_ehp_profile2Id_dbt*)MEA_OS_malloc(size);
		if (MEA_EHP_profile2Id_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_profile2Id_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_profile2Id_Table,0,size);
	}

	/* Allocate MEA_EHP_profile2_lm_Table */
	if (MEA_EHP_profile2_lm_Table == NULL) {
		size = MEA_MAX_NUM_OF_EHP_PROFILE2_LM * 
			   sizeof(MEA_drv_ehp_profile_stamping_dbt);
		MEA_EHP_profile2_lm_Table = (MEA_drv_ehp_profile2_lm_dbt*)MEA_OS_malloc(size);
		if (MEA_EHP_profile2_lm_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_EHP_profile2_lm_Table failed (size=%d)\n",
							  __FUNCTION__,
							  size);
			return MEA_ERROR;
		}
		MEA_OS_memset(MEA_EHP_profile2_lm_Table,0,size);

        /* T.B.D - Clear Profile2LM Counters */

    }

    /* Allocate mea_drv_ehp_mc_grouping_tbl */
    if (mea_drv_ehp_mc_grouping_tbl == NULL) {
        size = MEA_MAX_NUM_OF_EDIT_ID * sizeof(MEA_OutPorts_Entry_dbt);
        mea_drv_ehp_mc_grouping_tbl = (MEA_OutPorts_Entry_dbt*)MEA_OS_malloc(size);
        if (mea_drv_ehp_mc_grouping_tbl == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_drv_ehp_mc_grouping_tbl failed (size=%d)\n",
                __FUNCTION__,
                size);
            return MEA_ERROR;
        }
        MEA_OS_memset(mea_drv_ehp_mc_grouping_tbl, 0, size);
    }

    
    /*Allocated the MC EHP */
    if (MEA_MC_EHP_Table == NULL) {
        size = MEA_MAX_NUM_OF_MC_EHP * sizeof(MEA_drv_mc_ehp_dbt);
        MEA_MC_EHP_Table = (MEA_drv_mc_ehp_dbt*)MEA_OS_malloc(size);
        if (MEA_MC_EHP_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_MC_EHP_Table failed (size=%d)\n",
                __FUNCTION__,
                size);
            return MEA_ERROR;
        }
        MEA_OS_memset(MEA_MC_EHP_Table, 0, size);
    }

    /*global mc */
    if (ed_id_arr == NULL) {
        size = MEA_MAX_NUM_OF_MC_ELEMENTS * sizeof(MEA_Editing_t);
        ed_id_arr = (MEA_Editing_t*)MEA_OS_malloc(size);
        if (ed_id_arr == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate ed_id_arr failed (size=%d)\n",
                __FUNCTION__,
                size);
            return MEA_ERROR;
        }
        MEA_OS_memset(ed_id_arr, 0, size);
    }


    

       



	mc_grouping_tbl_in_use_Flag = MEA_FALSE;




//    mea_drv_Init_atm_ehp_tables(unit_i);

	mea_drv_Init_mc_ehp_table(unit_i);

	mea_drv_Init_eth_ehp_table(unit_i);


    /*set the all the machine*/
    MEA_OS_memset(&MEA_EHP_ProtocolMachine, 0, sizeof(MEA_EHP_ProtocolMachine));
    mea_drv_Init_ProtocolMachine(unit_i);
    



    /* return to caller */
	return ret;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_EHP_profile_stamping_callback>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_EHP_profile_stamping_callback
                                (MEA_Unit_t unit_i,
                                 MEA_ind_write_t* ind_write_pio)
{
    MEA_db_HwUnit_t          hwUnit;
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);
    ind_write_pio->tableOffset += MEA_BM_TBL_TYP_EHP_PROFILE_STAMPING_START(hwUnit);
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_EHP_profile_sa_dss_callback>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_EHP_profile_sa_dss_callback
                                (MEA_Unit_t unit_i,
                                 MEA_ind_write_t* ind_write_pio)
{
    MEA_db_HwUnit_t          hwUnit;
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);
    ind_write_pio->tableOffset += MEA_BM_TBL_TYP_EHP_PROFILE_SA_DSS_START(hwUnit);
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_EHP_profile_sa_mss_callback>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_EHP_profile_sa_mss_callback
                                (MEA_Unit_t unit_i,
                                 MEA_ind_write_t* ind_write_pio)
{
    MEA_db_HwUnit_t          hwUnit;
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);
    ind_write_pio->tableOffset += MEA_BM_TBL_TYP_EHP_PROFILE_SA_MSS_START(hwUnit);
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_EHP_profile_ether_type_callback>               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_EHP_profile_ether_type_callback
                                (MEA_Unit_t unit_i,
                                 MEA_ind_write_t* ind_write_pio)
{
    MEA_db_HwUnit_t          hwUnit;
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);
    ind_write_pio->tableOffset += MEA_BM_TBL_TYP_EHP_PROFILE_ETHER_TYPE_START(hwUnit);
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_EHP>                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_EHP(MEA_Unit_t unit_i)
{

    MEA_Uint32 i;

    MEA_Bool  EHP_IS_DDR;

    EHP_IS_DDR = mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit_i, MEA_DB_EDITING_TYPE);

    /* ReInit EHP eth table   */
    if (mea_db_ReInit(unit_i,
                      MEA_EHP_ETH_db,
                      MEA_BM_TBL_TYP_EHP_ETH,
                      MEA_MODULE_BM,
                      NULL,
                      EHP_IS_DDR) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit for EHP_ETH failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

    /* ReInit EHP profile table (stamping section)  */
    if (mea_db_ReInit(unit_i,
                      MEA_EHP_profile_stamping_db,
                      ENET_BM_TBL_TYP_EHP_PROFILE,
                      MEA_MODULE_BM,
                      mea_drv_ReInit_EHP_profile_stamping_callback,
                      MEA_FALSE) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit for EHP_PROFILE_STAMPING failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }


    /* ReInit EHP profile table (sa_mss section)  */
    if (mea_db_ReInit(unit_i,
                      MEA_EHP_profile_sa_mss_db,
                      ENET_BM_TBL_TYP_EHP_PROFILE,
                      MEA_MODULE_BM,
                      mea_drv_ReInit_EHP_profile_sa_mss_callback,
                      MEA_FALSE) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit for EHP_PROFILE_SA_MSS failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

    /* ReInit EHP profile table (ether_type section)  */
    if (mea_db_ReInit(unit_i,
        MEA_EHP_profile_ether_type_db,
        ENET_BM_TBL_TYP_EHP_PROFILE,
        MEA_MODULE_BM,
        mea_drv_ReInit_EHP_profile_ether_type_callback,
        MEA_FALSE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_db_ReInit for EHP_PROFILE_ETHER_TYPE failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    /* T.B.D - Clear Profile2LM Counters */

    /* ReInit EHP fragmentSession table   */
    if (mea_db_ReInit(unit_i,
        MEA_EHP_fragmentSession_db,
        MEA_BM_TBL_EHP_FRAGMENT_SESSION,
        MEA_MODULE_BM,
        NULL,
        MEA_FALSE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_ReInit for EHP_fragmentSesion failed \n",
                __FUNCTION__);
            return MEA_ERROR;
    }


 


    /* ReInit EHP Multicast Table */
    for (i = 0; i<(MEA_Uint32)MEA_MAX_NUM_OF_MC_EHP; i++) {
        if (!MEA_MC_EHP_Table[i].valid) {
            continue;
        }
		if ( mea_drv_ehp_mc_UpdateHw(unit_i,
                                     i, 
                                     &(MEA_MC_EHP_Table[i])) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_ehp_mc_UpdateHw failed (index %d)\n",
							  __FUNCTION__,i);
			return MEA_ERROR; 
		}
	}

    /* ReInit EHP ATM header tables */
    //mea_drv_Init_atm_ehp_tables(unit_i);



    /* return to caller */
	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_EHP_XXX_db>                              */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_ETH_db,
							 MEA_EHP_ETH_db) 
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_ATM_db,
							 MEA_EHP_ATM_db)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_WBRG_db,
                             MEA_EHP_WBRG_db) 
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_da_db,
							 MEA_EHP_da_db) 
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_sa_lss_db,
							 MEA_EHP_sa_lss_db)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_profileId_db,
							 MEA_EHP_profileId_db)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_stamping_db,
							 MEA_EHP_profile_stamping_db)

MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_sa_mss_db,
							 MEA_EHP_profile_sa_mss_db)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile_ether_type_db,
							 MEA_EHP_profile_ether_type_db)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile2Id_db,
							 MEA_EHP_profile2Id_db)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_profile2_lm_db,
							 MEA_EHP_profile2_lm_db)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_fragmentSession_db,
                             MEA_EHP_fragmentSession_db)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_pw_control_db,
                             MEA_EHP_pw_control_db)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EHP_mpls_label_db,
                             MEA_EHP_mpls_label_db)



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_EgressHeaderProc_Entry>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_EgressHeaderProc_Entry(MEA_Unit_t                     unit,
                                              MEA_Editing_t                  editId,
                                              MEA_EgressHeaderProc_Entry_dbt *entry)
{


   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS


    /* Check for valid editId parameter */
    if (mea_drv_IsEditingIdInRange(editId) != MEA_TRUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid editId %d \n",
                         __FUNCTION__,editId);
       return MEA_ERROR;  
    }

    /* Check for valid entry parameter */
    if (entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

#endif


	if ( MEA_EHP_Table[editId].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -1 no EHP entry with ID %d\n",
                         __FUNCTION__, editId);
       return MEA_ERROR; 
    }

    /* Update the output parameters */
    MEA_OS_memcpy(entry,
				  &(MEA_EHP_Table[editId].editing_info),
                  sizeof(*entry));


	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Delete_EgressHeaderProc_Entry>                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_EgressHeaderProc_Entry(MEA_Unit_t                     unit_i,
                                                 MEA_Editing_t                  editId)
{


   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS


    /* Check for valid editId parameter */
    if (mea_drv_IsEditingIdInRange(editId) != MEA_TRUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid editId %d \n",
                         __FUNCTION__,editId);
       return MEA_ERROR;  
    }
#endif


	if ( MEA_EHP_Table[editId].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -2 no EHP entry with ID %d\n",
                         __FUNCTION__, editId);
       return MEA_ERROR; 
    }

	if ( mea_drv_ehp_delete_owner(unit_i,editId) != MEA_OK )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ehp_delete_owner failed, id %d\n",
                         __FUNCTION__, editId);
       return MEA_ERROR; 
    }


	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_ED_drv_Set_EgressHeaderProc_Entry>                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ED_Set_EgressHeaderProc_Entry(MEA_Unit_t                     unit_i,
                                                 MEA_EgressHeaderProc_Entry_dbt *entry_i,
											     MEA_Editing_t                  *id_io,
                                                 MEA_Bool                        create_i)
{
	MEA_Editing_t index;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check for valid entry parameter */
    if (id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - id_io == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for valid editId parameter */
    if (mea_drv_IsEditingIdInRange(*id_io) != MEA_TRUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid editId %d \n",
                         __FUNCTION__,*id_io);
       return MEA_ERROR;  
    }

	if ( entry_i != NULL )
	{

        if ((entry_i->eth_info.mapping_enable) &&
            (!MEA_EDITING_MAPPING_PROFILE_SUPPORT)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - eth_info.mapping_enable not support in this platform \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry_i->eth_info.mapping_enable) {
            MEA_EditingMappingProfile_Entry_dbt editingMappingProfile_entry;
            if (MEA_API_Get_EditingMappingProfile_Entry(unit_i,
                                                        (MEA_EditingMappingProfile_Id_t)(entry_i->eth_info.mapping_id),
                                                        &editingMappingProfile_entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Get_EditingMppingProfile_Entry failed (mapping_id=%d)\n",
                                  __FUNCTION__,
                                  entry_i->eth_info.mapping_id);
                return MEA_ERROR;
            }

            switch(editingMappingProfile_entry.direction) {
            case MEA_DIRECTION_GENERAL:
                if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for eth mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->eth_info.mapping_id,
                                      globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            case MEA_DIRECTION_UPSTREAM:
                if (globalMemoryMode != MEA_MODE_UPSTREEM_ONLY) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for eth mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->eth_info.mapping_id,
                                    globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            case MEA_DIRECTION_DOWNSTREAM:
                if (globalMemoryMode != MEA_MODE_DOWNSTREEM_ONLY) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for eth mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->eth_info.mapping_id,
                                    globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown direction %d for eth mapping_id %d \n",
                                  __FUNCTION__,
                                  editingMappingProfile_entry.direction,
                                  entry_i->eth_info.mapping_id);
                return MEA_ERROR;
            }
        }



        /* Check stamp color/priority stamping offsets configuration */
        if ((entry_i->eth_info.mapping_enable) || 
            (entry_i->eth_info.stamp_color   ) || 
            (entry_i->eth_info.stamp_priority)  ) {


            if (((entry_i->eth_info.stamp_color) || (entry_i->eth_info.mapping_enable)) &&
				(!entry_i->eth_stamping_info.color_bit0_valid ) /*&& (!entry_i->eth_stamping_info.color_bit1_valid )*/  
				) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - color stamping offset must be pre-configured \n",
								  __FUNCTION__);
  				return MEA_ERROR;
			}

            if (((entry_i->eth_info.stamp_priority) || (entry_i->eth_info.mapping_enable)) &&
				(!entry_i->eth_stamping_info.pri_bit0_valid ) &&
				(!entry_i->eth_stamping_info.pri_bit1_valid ) &&
				(!entry_i->eth_stamping_info.pri_bit2_valid )  
			   ) {

				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - priority stamping offset must be pre-configured \n",
								  __FUNCTION__);
  				return MEA_ERROR;
			}
        }

        if ((entry_i->atm_info.mapping_enable) &&
            (!MEA_EDITING_MAPPING_PROFILE_SUPPORT)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - atm_info.mapping_enable not support in this platform \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry_i->atm_info.mapping_enable) {
            MEA_EditingMappingProfile_Entry_dbt editingMappingProfile_entry;
            if (MEA_API_Get_EditingMappingProfile_Entry(unit_i,
                                                        (MEA_EditingMappingProfile_Id_t)(entry_i->atm_info.mapping_id),
                                                        &editingMappingProfile_entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Get_EditingMppingProfile_Entry failed (mapping_id=%d)\n",
                                  __FUNCTION__,
                                  entry_i->atm_info.mapping_id);
                return MEA_ERROR;
            }

            switch(editingMappingProfile_entry.direction) {
            case MEA_DIRECTION_GENERAL:
                if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for atm mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->atm_info.mapping_id,
                                      globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            case MEA_DIRECTION_UPSTREAM:
                if (globalMemoryMode != MEA_MODE_UPSTREEM_ONLY) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for atm mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->atm_info.mapping_id,
                                    globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            case MEA_DIRECTION_DOWNSTREAM:
                if (globalMemoryMode != MEA_MODE_DOWNSTREEM_ONLY) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for atm mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->atm_info.mapping_id,
                                    globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown direction %d for atm mapping_id %d \n",
                                  __FUNCTION__,
                                  editingMappingProfile_entry.direction,
                                  entry_i->atm_info.mapping_id);
                return MEA_ERROR;
            }
        }



        /*outer vlan*/
            /* Check stamp color/priority stamping offsets configuration */
        if ((entry_i->atm_info.mapping_enable) ||
            (entry_i->atm_info.stamp_color   ) || 
            (entry_i->atm_info.stamp_priority)) {


                if (((entry_i->atm_info.stamp_color) || (entry_i->atm_info.mapping_enable)) &&
				    (!entry_i->eth_stamping_info.color_bit0_valid ) /*&&  (!entry_i->eth_stamping_info.color_bit1_valid )*/ 
                    ) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - color stamping offset must be pre-configured \n",
								  __FUNCTION__);
  				return MEA_ERROR;
			}
            if (((entry_i->atm_info.stamp_priority) || (entry_i->atm_info.mapping_enable)) &&
				(!entry_i->eth_stamping_info.pri_bit0_valid ) &&
				(!entry_i->eth_stamping_info.pri_bit1_valid ) &&
				(!entry_i->eth_stamping_info.pri_bit2_valid )  
			   ) {

				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - priority stamping offset must be pre configured \n",
								  __FUNCTION__);
  				return MEA_ERROR;
			}
            /**/
			


		}


    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    
/* If input ID == 0 && ehp_info NULL ==> return id 0.
   If input ID == 0 && ehp_info != NULL ==> create/assign entry and update output ID.
   If input ID != 0 : if exist - add owner.
   If input ID != 0 : not exist then create id
*/

	if ( *id_io == 0 && entry_i != NULL )
	{
		/* 
			- Search for existing data:
				- If found, increment the owner and return the ID.
				- If not found, get free index, create new entry, return new ID.
		*/
		if ( mea_drv_is_EHP_data_exist(unit_i,entry_i, &index) )
		{
			if ( mea_drv_ehp_add_owner(index) != MEA_OK )
			{
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_ehp_add_owner failed, index %d\n",
								  __FUNCTION__, index);
				return MEA_ERROR; 
			}
		}
		else
		{
			if ( mea_drv_create_ehp_entry(unit_i,entry_i, &index,MEA_FALSE) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_create_ehp_entry failed\n",
								  __FUNCTION__);
				return MEA_ERROR; 
			}
		}
		*id_io = index;
	}
	else if ( *id_io != 0  &&  MEA_EHP_Table[*id_io].valid == MEA_TRUE && entry_i == NULL )
	{
      if ( mea_drv_ehp_add_owner(*id_io) != MEA_OK )
		{
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_ehp_add_owner failed, index %d\n",
							  __FUNCTION__, *id_io);
			return MEA_ERROR; 
		}
       		return MEA_OK; 
    } 
    else if ( *id_io != 0 ) {
 
        /* check if the id is in the range*/
        if(mea_drv_IsEditingIdInRange(*id_io)==MEA_FALSE){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - EHP entry is out of range\n",
                          __FUNCTION__);
	    return MEA_ERROR;
        }
        
        
        if(MEA_EHP_Table[*id_io].valid ){
        
            if ( mea_drv_is_EHP_data_exist(unit_i,entry_i, &index) )
		    {
                if(index == *id_io) {
                    if ( mea_drv_ehp_add_owner(index) != MEA_OK )
			        {
			           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								          "%s - mea_drv_ehp_add_owner failed, index %d\n",
								          __FUNCTION__, index);
				        return MEA_ERROR; 
			        }
                    return MEA_OK;
                } else {
                 //check the number number of owner
					if(create_i == MEA_TRUE){
                       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								          "%s - internal data are not same, \n",
								          __FUNCTION__);
					}
                    /**/
                    if(MEA_EHP_Table[*id_io].num_of_owners > 1){
                      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								          "%s - num_of_owners %d > 1\n",
								          __FUNCTION__, MEA_EHP_Table[*id_io].num_of_owners);
                        return MEA_ERROR;
                    }else {
                        /* modify */
                        if ( mea_drv_create_ehp_entry(unit_i,entry_i, id_io,MEA_TRUE) != MEA_OK )
			            {
				        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								      "%s - mea_drv_create_ehp_entry failed\n",
								      __FUNCTION__);
				        return MEA_ERROR; 
			            }
                    } 
                }
			} else{
				if(MEA_EHP_Table[*id_io].num_of_owners>1){
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								      "%s - mea_drv_is_EHP_data_exist not exist failed\n",
								      __FUNCTION__);
				
				return MEA_ERROR;
				}else{
					
                        /* modify */
                        if ( mea_drv_create_ehp_entry(unit_i,entry_i, id_io,MEA_TRUE) != MEA_OK )
			            {
				        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								      "%s - mea_drv_create_ehp_entry failed\n",
								      __FUNCTION__);
				        return MEA_ERROR; 
			            }
				}
			}
		} else {
        
			if ( mea_drv_create_drv_force_id_editing(unit_i,entry_i, id_io) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
										  "%s - mea_drv_create_ehp_entry failed\n",
										  __FUNCTION__);
				return MEA_ERROR; 
			}

		}
	}
	else
		*id_io = 0;


	return MEA_OK;

}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_EgressHeaderProc_Entry>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_EgressHeaderProc_Entry(MEA_Unit_t                     unit_i,
                                              MEA_EgressHeaderProc_Entry_dbt *entry_i,
											  MEA_Editing_t                  *id_io,
                                              MEA_Bool                        force_update_i)
{
    MEA_Editing_t index;

   MEA_API_LOG


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check for valid entry parameter */
    if (id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - id_io == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */





#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check for valid editId parameter */
    if (mea_drv_IsEditingIdInRange(*id_io) != MEA_TRUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid editId %d \n",
                         __FUNCTION__,*id_io);
       return MEA_ERROR;  
    }

	if ( entry_i != NULL )
	{

        if ((entry_i->eth_info.mapping_enable) &&
            (!MEA_EDITING_MAPPING_PROFILE_SUPPORT)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - eth_info.mapping_enable not support in this platform \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry_i->eth_info.mapping_enable) {
            MEA_EditingMappingProfile_Entry_dbt editingMappingProfile_entry;
            if (MEA_API_Get_EditingMappingProfile_Entry(unit_i,
                                                        (MEA_EditingMappingProfile_Id_t)(entry_i->eth_info.mapping_id),
                                                        &editingMappingProfile_entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Get_EditingMppingProfile_Entry failed (mapping_id=%d)\n",
                                  __FUNCTION__,
                                  entry_i->eth_info.mapping_id);
                return MEA_ERROR;
            }

            switch(editingMappingProfile_entry.direction) {
            case MEA_DIRECTION_GENERAL:
                if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for eth mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->eth_info.mapping_id,
                                      globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            case MEA_DIRECTION_UPSTREAM:
                if (globalMemoryMode != MEA_MODE_UPSTREEM_ONLY) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for eth mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->eth_info.mapping_id,
                                    globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            case MEA_DIRECTION_DOWNSTREAM:
                if (globalMemoryMode != MEA_MODE_DOWNSTREEM_ONLY) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for eth mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->eth_info.mapping_id,
                                    globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown direction %d for eth mapping_id %d \n",
                                  __FUNCTION__,
                                  editingMappingProfile_entry.direction,
                                  entry_i->eth_info.mapping_id);
                return MEA_ERROR;
            }
        }


        /* Check stamp color/priority stamping offsets configuration */
        if ((entry_i->eth_info.mapping_enable) || 
            (entry_i->eth_info.stamp_color   ) || 
            (entry_i->eth_info.stamp_priority)  ) {


            if (((entry_i->eth_info.stamp_color) || (entry_i->eth_info.mapping_enable)) &&
				(!entry_i->eth_stamping_info.color_bit0_valid ) /*&& (!entry_i->eth_stamping_info.color_bit1_valid )*/  
				) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - color stamping offset must be pre-configured \n",
								  __FUNCTION__);
  				return MEA_ERROR;
			}

            if (((entry_i->eth_info.stamp_priority) || (entry_i->eth_info.mapping_enable)) &&
				(!entry_i->eth_stamping_info.pri_bit0_valid ) &&
				(!entry_i->eth_stamping_info.pri_bit1_valid ) &&
				(!entry_i->eth_stamping_info.pri_bit2_valid )  
			   ) {

				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - priority stamping offset must be pre configured \n",
								  __FUNCTION__);
  				return MEA_ERROR;
			}
        }

#ifdef MEA_SWAP_MAC
        if ( entry_i->eth_info.command == MEA_EGRESS_HEADER_PROC_CMD_SWAP_MAC )
		{
			MEA_OS_memset(&(entry_i->eth_info),0, sizeof(entry_i->eth_info));
			entry_i->eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_SWAP_MAC;
		}       
        /**/
#endif
        if ((entry_i->atm_info.mapping_enable) &&
            (!MEA_EDITING_MAPPING_PROFILE_SUPPORT)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - atm_info.mapping_enable not support in this platform \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry_i->atm_info.mapping_enable) {
            MEA_EditingMappingProfile_Entry_dbt editingMappingProfile_entry;
            if (MEA_API_Get_EditingMappingProfile_Entry(unit_i,
                                                        (MEA_EditingMappingProfile_Id_t)(entry_i->atm_info.mapping_id),
                                                        &editingMappingProfile_entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Get_EditingMppingProfile_Entry failed (mapping_id=%d)\n",
                                  __FUNCTION__,
                                  entry_i->atm_info.mapping_id);
                return MEA_ERROR;
            }

            switch(editingMappingProfile_entry.direction) {
            case MEA_DIRECTION_GENERAL:
                if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for atm mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->atm_info.mapping_id,
                                      globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            case MEA_DIRECTION_UPSTREAM:
                if (globalMemoryMode != MEA_MODE_UPSTREEM_ONLY) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for atm mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->atm_info.mapping_id,
                                    globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            case MEA_DIRECTION_DOWNSTREAM:
                if (globalMemoryMode != MEA_MODE_DOWNSTREEM_ONLY) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - direction %d for atm mapping_id %d is not match globalMemoryMode %d \n",
                                      __FUNCTION__,
                                      editingMappingProfile_entry.direction,
                                      entry_i->atm_info.mapping_id,
                                    globalMemoryMode);
                    return MEA_ERROR;
                }
                break;
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown direction %d for atm mapping_id %d \n",
                                  __FUNCTION__,
                                  editingMappingProfile_entry.direction,
                                  entry_i->atm_info.mapping_id);
                return MEA_ERROR;
            }
        }

        /*outer vlan*/
            /* Check stamp color/priority stamping offsets configuration */
        if ((entry_i->atm_info.mapping_enable) ||
            (entry_i->atm_info.stamp_color   ) || 
            (entry_i->atm_info.stamp_priority)) {
#if 0
                if ((entry_i->atm_info.double_tag_cmd != MEA_EGRESS_HEADER_PROC_CMD_APPEND) &&
                    (entry_i->atm_info.double_tag_cmd != MEA_EGRESS_HEADER_PROC_CMD_SWAP  )  ) {
				    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - entry_i->atm_info.stamp_color(%d)/priority(%d)/mapping_enable(%d) require "
								      "entry_i->atm_info.double_tag_cmd(%d) Append Or Swap\n ",
                                      __FUNCTION__,
                                      entry_i->atm_info.stamp_color,
                                      entry_i->atm_info.stamp_priority,
                                      entry_i->atm_info.mapping_enable,
								      entry_i->atm_info.double_tag_cmd );
				    return MEA_ERROR;
			    }
#endif

                if (((entry_i->atm_info.stamp_color) || (entry_i->atm_info.mapping_enable)) &&
				    (!entry_i->eth_stamping_info.color_bit0_valid ) /*&& (!entry_i->eth_stamping_info.color_bit1_valid )*/ 
                    ) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - color stamping offset must be pre-configured \n",
								  __FUNCTION__);
  				return MEA_ERROR;
			}
            if (((entry_i->atm_info.stamp_priority) || (entry_i->atm_info.mapping_enable)) &&
				(!entry_i->eth_stamping_info.pri_bit0_valid ) &&
				(!entry_i->eth_stamping_info.pri_bit1_valid ) &&
				(!entry_i->eth_stamping_info.pri_bit2_valid )  
			   ) {

				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - priority stamping offset must be pre configured \n",
								  __FUNCTION__);
  				return MEA_ERROR;
			}
            /**/
			


		}

		
	


    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    if( force_update_i ==MEA_TRUE ){
     if ( MEA_EHP_Table[*id_io].valid ==MEA_FALSE)
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - no existing EHP entry with ID %d\n",
							  __FUNCTION__, *id_io);
			return MEA_ERROR; 
		}
     if (entry_i == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - entry_i == NULL with force_update==TRUE\n",
							  __FUNCTION__);
			return MEA_ERROR; 
     }
    if ( mea_drv_create_ehp_entry(unit_i,entry_i, id_io ,MEA_TRUE) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_create_ehp_entry failed\n",
								  __FUNCTION__);
				return MEA_ERROR; 
			}
    return MEA_OK;
    }
    
/* If input ID == 0 && ehp_info NULL ==> return id 0.
   If input ID == 0 && ehp_info != NULL ==> create/assign entry and update output ID.
   If input ID != 0 : if exist - add owner.
*/

	if ( *id_io == 0 && entry_i != NULL )
	{
		/* 
			- Search for existing data:
				- If found, increment the owner and return the ID.
				- If not found, get free index, create new entry, return new ID.
		*/
		if ( mea_drv_is_EHP_data_exist(unit_i,entry_i, &index) )
		{
			if ( mea_drv_ehp_add_owner(index) != MEA_OK )
			{
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_ehp_add_owner failed, index %d\n",
								  __FUNCTION__, index);
				return MEA_ERROR; 
			}
		}
		else
		{
			if ( mea_drv_create_ehp_entry(unit_i,entry_i, &index,MEA_FALSE) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_create_ehp_entry failed\n",
								  __FUNCTION__);
				return MEA_ERROR; 
			}
		}
		*id_io = index;
	}
	else if ( *id_io != 0 )
	{
		/* Just add owner */
		if ( !MEA_EHP_Table[*id_io].valid )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - no existing EHP entry with ID %d\n",
							  __FUNCTION__, *id_io);
			return MEA_ERROR; 
		}

		if ( mea_drv_ehp_add_owner(*id_io) != MEA_OK )
		{
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_ehp_add_owner failed, index %d\n",
							  __FUNCTION__, *id_io);
			return MEA_ERROR; 
		}
	}
	else
		*id_io = 0;

	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_EgressHeaderProc_ProtoEthStampingInfo>            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_EgressHeaderProc_ProtoEthStampingInfo
                                   (MEA_EgressPort_Proto_t        proto,
                                    MEA_Bool                      isMartini_E_L_LSP,
                                    MEA_EgressHeaderProc_EthStampingInfo_dbt *stamp_info)

{

   MEA_API_LOG

	switch(proto) {
        case MEA_EGRESS_PORT_PROTO_TRANS:
            stamp_info->color_bit0_loc   = 0;
			stamp_info->color_bit0_valid = MEA_FALSE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = 0;
		    stamp_info->pri_bit0_valid   = MEA_FALSE;
		    stamp_info->pri_bit1_loc     = 0;
		    stamp_info->pri_bit1_valid   = MEA_FALSE;
			stamp_info->pri_bit2_loc     = 0;   	
			stamp_info->pri_bit2_valid   = MEA_FALSE;
            break;

        case MEA_EGRESS_PORT_PROTO_VLAN:
        case MEA_EGRESS_PORT_PROTO_QTAG:
		case MEA_EGRESS_PORT_PROTO_ETHER_TYPE:
            stamp_info->color_bit0_loc   = MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_CFI_BIT_OFFSET;		
			stamp_info->color_bit0_valid = MEA_TRUE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_PRI_BIT0_OFFSET;
		    stamp_info->pri_bit0_valid   = MEA_TRUE;
		    stamp_info->pri_bit1_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_PRI_BIT1_OFFSET;
		    stamp_info->pri_bit1_valid   = MEA_TRUE;
			stamp_info->pri_bit2_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_PRI_BIT2_OFFSET;   	
			stamp_info->pri_bit2_valid   = MEA_TRUE;
			break;
		case MEA_EGRESS_PORT_PROTO_MPLSoE:
        case MEA_EGRESS_PORT_PROTO_MARTINI:
            if (isMartini_E_L_LSP){
               stamp_info->color_bit0_loc   = MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_E_LSP_CL_BIT_OFFSET;		
			   stamp_info->color_bit0_valid = MEA_TRUE; 
		       stamp_info->color_bit1_valid = MEA_FALSE;	
			   stamp_info->pri_bit0_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_E_LSP_PRI_BIT0_OFFSET;
		       stamp_info->pri_bit0_valid   = MEA_TRUE;
		       stamp_info->pri_bit1_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_E_LSP_PRI_BIT1_OFFSET;
		       stamp_info->pri_bit1_valid   = MEA_TRUE;
			   stamp_info->pri_bit2_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_E_LSP_PRI_BIT2_OFFSET;
			   stamp_info->pri_bit2_valid   = MEA_TRUE;
            } else {
               stamp_info->color_bit0_loc   = MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_L_LSP_CL_BIT_OFFSET;		
			   stamp_info->color_bit0_valid = MEA_TRUE; 
		       stamp_info->color_bit1_valid = MEA_FALSE;	
			   stamp_info->pri_bit0_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_L_LSP_PRI_BIT0_OFFSET;
		       stamp_info->pri_bit0_valid   = MEA_TRUE;
		       stamp_info->pri_bit1_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_L_LSP_PRI_BIT1_OFFSET;
		       stamp_info->pri_bit1_valid   = MEA_TRUE;
			   stamp_info->pri_bit2_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_L_LSP_PRI_BIT2_OFFSET;
			   stamp_info->pri_bit2_valid   = MEA_TRUE;
            }
            break;

        case MEA_EGRESS_PORT_PROTO_EoLLCoATM:
            stamp_info->color_bit0_loc   = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_CFI_BIT_OFFSET;		
			stamp_info->color_bit0_valid = MEA_TRUE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT0_OFFSET;
		    stamp_info->pri_bit0_valid   = MEA_TRUE;
		    stamp_info->pri_bit1_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT1_OFFSET;
		    stamp_info->pri_bit1_valid   = MEA_TRUE;
			stamp_info->pri_bit2_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT2_OFFSET;   	
			stamp_info->pri_bit2_valid   = MEA_TRUE;
			break;

        case MEA_EGRESS_PORT_PROTO_IPoEoLLCoATM:
            stamp_info->color_bit0_loc   = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_CFI_BIT_OFFSET;		
			stamp_info->color_bit0_valid = MEA_TRUE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT0_OFFSET;
		    stamp_info->pri_bit0_valid   = MEA_TRUE;
		    stamp_info->pri_bit1_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT1_OFFSET;
		    stamp_info->pri_bit1_valid   = MEA_TRUE;
			stamp_info->pri_bit2_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT2_OFFSET;   	
			stamp_info->pri_bit2_valid   = MEA_TRUE;
            break;

        case MEA_EGRESS_PORT_PROTO_IP:
            stamp_info->color_bit0_loc   = 0;		
			stamp_info->color_bit0_valid = MEA_FALSE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = 0;
		    stamp_info->pri_bit0_valid   = MEA_FALSE;
		    stamp_info->pri_bit1_loc     = 0;
		    stamp_info->pri_bit1_valid   = MEA_FALSE;
			stamp_info->pri_bit2_loc     = 0;   	
			stamp_info->pri_bit2_valid   = MEA_FALSE;
            break;

        case MEA_EGRESS_PORT_PROTO_IPoVLAN:
            stamp_info->color_bit0_loc   = MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_CFI_BIT_OFFSET;		
			stamp_info->color_bit0_valid = MEA_TRUE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_PRI_BIT0_OFFSET;
		    stamp_info->pri_bit0_valid   = MEA_TRUE;
		    stamp_info->pri_bit1_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_PRI_BIT1_OFFSET;
		    stamp_info->pri_bit1_valid   = MEA_TRUE;
			stamp_info->pri_bit2_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_PRI_BIT2_OFFSET;   	
			stamp_info->pri_bit2_valid   = MEA_TRUE;
			break;

        case MEA_EGRESS_PORT_PROTO_PPPoEoLLCoATM:
            stamp_info->color_bit0_loc   = 0;		
			stamp_info->color_bit0_valid = MEA_FALSE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = 0;
		    stamp_info->pri_bit0_valid   = MEA_FALSE;
		    stamp_info->pri_bit1_loc     = 0;
		    stamp_info->pri_bit1_valid   = MEA_FALSE;
			stamp_info->pri_bit2_loc     = 0;   	
			stamp_info->pri_bit2_valid   = MEA_FALSE;
            break;

		case MEA_EGRESS_PORT_PROTO_PPPoEoVcMUXoATM:
            stamp_info->color_bit0_loc   = 0;		
			stamp_info->color_bit0_valid = MEA_FALSE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = 0;
		    stamp_info->pri_bit0_valid   = MEA_FALSE;
		    stamp_info->pri_bit1_loc     = 0;
		    stamp_info->pri_bit1_valid   = MEA_FALSE;
			stamp_info->pri_bit2_loc     = 0;   	
			stamp_info->pri_bit2_valid   = MEA_FALSE;
            break;


        case MEA_EGRESS_PORT_PROTO_EoVcMUXoATM:
            stamp_info->color_bit0_loc   = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_CFI_BIT_OFFSET;		
			stamp_info->color_bit0_valid = MEA_TRUE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT0_OFFSET;
		    stamp_info->pri_bit0_valid   = MEA_TRUE;
		    stamp_info->pri_bit1_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT1_OFFSET;
		    stamp_info->pri_bit1_valid   = MEA_TRUE;
			stamp_info->pri_bit2_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT2_OFFSET;   	
			stamp_info->pri_bit2_valid   = MEA_TRUE;
			break;

		case MEA_EGRESS_PORT_PROTO_IPoLLCoATM:
            stamp_info->color_bit0_loc   = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_CFI_BIT_OFFSET;		
			stamp_info->color_bit0_valid = MEA_TRUE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT0_OFFSET;
		    stamp_info->pri_bit0_valid   = MEA_TRUE;
		    stamp_info->pri_bit1_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT1_OFFSET;
		    stamp_info->pri_bit1_valid   = MEA_TRUE;
			stamp_info->pri_bit2_loc     = MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT2_OFFSET;   	
			stamp_info->pri_bit2_valid   = MEA_TRUE;
            break;

        case MEA_EGRESS_PORT_PROTO_IPoVcMUXoATM:
            stamp_info->color_bit0_loc   = 0;		
			stamp_info->color_bit0_valid = MEA_FALSE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = 0;
		    stamp_info->pri_bit0_valid   = MEA_FALSE;
		    stamp_info->pri_bit1_loc     = 0;
		    stamp_info->pri_bit1_valid   = MEA_FALSE;
			stamp_info->pri_bit2_loc     = 0;   	
			stamp_info->pri_bit2_valid   = MEA_FALSE;
            break;

        case MEA_EGRESS_PORT_PROTO_PPPoLLCoATM  :
            stamp_info->color_bit0_loc   = 0;		
			stamp_info->color_bit0_valid = MEA_FALSE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = 0;
		    stamp_info->pri_bit0_valid   = MEA_FALSE;
		    stamp_info->pri_bit1_loc     = 0;
		    stamp_info->pri_bit1_valid   = MEA_FALSE;
			stamp_info->pri_bit2_loc     = 0;   	
			stamp_info->pri_bit2_valid   = MEA_FALSE;
            break;

        case MEA_EGRESS_PORT_PROTO_PPPoVcMUXoATM:
            stamp_info->color_bit0_loc   = 0;		
			stamp_info->color_bit0_valid = MEA_FALSE; 
		    stamp_info->color_bit1_valid = MEA_FALSE;	
			stamp_info->pri_bit0_loc     = 0;
		    stamp_info->pri_bit0_valid   = MEA_FALSE;
		    stamp_info->pri_bit1_loc     = 0;
		    stamp_info->pri_bit1_valid   = MEA_FALSE;
			stamp_info->pri_bit2_loc     = 0;   	
			stamp_info->pri_bit2_valid   = MEA_FALSE;
			break;

        default:
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Invalid proto %d\n",__FUNCTION__,proto);
            return MEA_ERROR;
    }

    return MEA_OK;
}


MEA_Status 	mea_drv_Init_mc_ehp_table(MEA_Unit_t unit_i)
{

	return MEA_OK;
}

MEA_Status 	mea_drv_Init_eth_ehp_table(MEA_Unit_t unit_i)
{



	return MEA_OK;
}



MEA_Status mea_drv_get_ehp_pool_entry(MEA_Editing_t index,
									  MEA_Bool      *found,
									  MEA_drv_ehp_dbt *entry_o )
{
	if (found == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - found = NULL \n",
			__FUNCTION__);
	}
	
	*found = MEA_FALSE;

	if (mea_drv_IsEditingIdInRange(index) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid index %d \n",
                         __FUNCTION__,index);
       return MEA_ERROR;  
    }

	if ( MEA_EHP_Table[index].valid == MEA_TRUE )
	{
		if (entry_o != NULL) {
			MEA_OS_memcpy(entry_o,
				&(MEA_EHP_Table[index]),
				sizeof(*entry_o));
		}

		*found = MEA_TRUE;
	}

	return MEA_OK;
}


MEA_Status MEA_API_Clear_Counters_drv_LM(MEA_Unit_t           unit,
                                         MEA_LmId_t           id_i)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_ERROR;)

        if(id_i >= ENET_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH(hwUnit)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - LM_LENGTH %d is out of range %d\n",
                __FUNCTION__,id_i ,ENET_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH(hwUnit));
            return MEA_ERROR;
        }

    if( mea_drv_EHP_UpdateHw_profile2_Lm(unit,id_i, 0) != MEA_OK){
       return MEA_ERROR;
   }

    return MEA_OK;
}



MEA_Uint32 mea_drv_ProtocolMachine_get_command(MEA_Uint16 index, MEA_Uint16 Type)
{
    if (index >= MEA_EDITINGTYPE_LAST)
        return 0;
    if (index >= 512){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s ProtocolMachine_get_command index is out of range \n",
            __FUNCTION__, index);
        return 0;
    }

    switch (Type)
    {
    case MEA_MACHINE_PROTO_LLC:
        return (MEA_Uint32)MEA_EHP_ProtocolMachine[index].val.llc;
        break;
    case   MEA_MACHINE_PROTO_MARTINI:
        return (MEA_Uint32)MEA_EHP_ProtocolMachine[index].val.martini_cmd;
        break;
    case    MEA_MACHINE_PROTO_OUTER :
        return (MEA_Uint32)MEA_EHP_ProtocolMachine[index].val.outer_cmd;
        break;
    case    MEA_MACHINE_PROTO_VLAN:
        return (MEA_Uint32)MEA_EHP_ProtocolMachine[index].val.vlan_cmd;
        break;
    case   MEA_MACHINE_PROTO_PROTO:
        return (MEA_Uint32)MEA_EHP_ProtocolMachine[index].val.protocol;
        break;
    case MEA_MACHINE_VAL:
        return  (MEA_Uint32)MEA_EHP_ProtocolMachine[index].regs[0];
        break;
    case MEA_MACHINE_CALC_CHECKSUM:
        return (MEA_Uint32)MEA_EHP_ProtocolMachine[index].val.calc_checksum;
        break;

    default:
        break;
    }

    return 0;

}

static void mea_drv_Init_ProtocolMachine(MEA_Unit_t           unit)
{


    
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_APPEND                                                                          ].regs[0] = 0x0a0;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_APPEND_APPEND                                                                   ].regs[0] = 0x0a8;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_EXTRACT                                                                         ].regs[0] = 0x0c0;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_EXTRACT_EXTRACT                                                                 ].regs[0] = 0x0d0;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_SWAP                                                                            ].regs[0] = 0x0e0;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_SWAP_APPEND                                                                     ].regs[0] = 0x0e8;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_SWAP_EXTRACT                                                                    ].regs[0] = 0x0f0;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_SWAP_SWAP                                                                       ].regs[0] = 0x0f8;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL                                                          ].regs[0] = 0x120;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_ETHERTYPE_LABEL                                                ].regs[0] = 0x121;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_LABEL                                                    ].regs[0] = 0x128;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_ETHERTYPE_LABEL_LABLE                                          ].regs[0] = 0x129;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_LABEL                                                         ].regs[0] = 0x140;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_ETHERTYPE_LABEL                                               ].regs[0] = 0x141;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_LABEL_LABEL                                                   ].regs[0] = 0x150;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_ETHERTYPE_LABEL_LABLE                                         ].regs[0] = 0x151;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_SWAP_MPLS_LABEL                                                            ].regs[0] = 0x160;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_SWAP_MPLS_LABEL_LABEL                                                      ].regs[0] = 0x178;

    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APEND_MPLS_LABEL_SWAP_CFM_ETH_TO_MPLS_CFM                                  ].regs[0] = 0x021;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_LABEL_APPEND_ETHTYRTYP_MPLS_SWAP_CFM_ETH_TO_MPLS_CFM     ].regs[0] = 0x029;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_Extract_MPLS_LABEL_Extract_ETHTYRTYP_MPLS_SWAP_MPLS_CFM_TO_CFM_ETH         ].regs[0] = 0x041;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_Extract_MPLS_LABEL_LABEL_Extract_ETHTYRTYP_MPLS_SWAP_MPLS_CFM_TO_CFM_ETH   ].regs[0] = 0x051;

    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_APPEND_BMAC_BTAG_ITAG                                           ].regs[0] = 0x1aa;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_EXTRACT_BMAC_BTAG_ITAG                                          ].regs[0] = 0x1d4;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_SWAP_BMAC_BTAG_ITAG                                             ].regs[0] = 0x1fe;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_APPEND_BMAC_APPEND_ITAG                                      ].regs[0] = 0x1ab;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_EXTRACT_BMAC_EXTRACT_ITAG                                    ].regs[0] = 0x1d5;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_SWAP_BMAC_SWAP_ITAG                                          ].regs[0] = 0x1ff;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_APPEND_BTAG_SWAP_ITAG                                        ].regs[0] = 0x1e9;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_SWAP_ISID                                                       ].regs[0] = 0x1e0;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_EXTRACT_BTAG_SWAP_ISID                                          ].regs[0] = 0x1f0;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NoBTAG_SWAP_ITAG                                                     ].regs[0] = 0x1e1;

   


    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_VLAN                                            ].regs[0] = 0x30;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_QTAG                                            ].regs[0] = 0x34;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_VLAN                                            ].regs[0] = 0x48;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_QTAG                                            ].regs[0] = 0x4a;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent                                               ].regs[0] = 0x68;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN                                                   ].regs[0] = 0x6A;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Swap_external_VLAN_TAG                                                  ].regs[0] = 0x6E;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN_Swap_Internal_VLAN                                ].regs[0] = 0x6c;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS                                                           ].regs[0] = 0x70;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG                                               ].regs[0] = 0x74;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Swap_External_TAG                                         ].regs[0] = 0x76;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG_Swap_Internal_VLAN                            ].regs[0] = 0x72;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_OFSET12                                                  ].regs[0] = 0x20;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent                            ].regs[0] = 0x78;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN                                ].regs[0] = 0x7a;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_Swap_DA_SA                                                                      ].regs[0] = 0x79;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QinQ                                               ].regs[0] = 0x2e;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_Extract_QInQ                                                    ].regs[0] = 0x52;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ                                 ].regs[0] = 0x54;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_DL_IPinIP_Append_SGW                                                       ].regs[0] = 0x32;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_UL_IPinIP_Extract_SGW                                                      ].regs[0] = 0x4c;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_DL_UL_SGW_Swap_header                                                      ].regs[0] = 0x7e;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_header_Append_vlan                                                        ].regs[0] = 0x22;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_header_Append_vlan_Extract_INNER_vlan                                     ].regs[0] = 0x23;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_header                                                                    ].regs[0] = 0x24;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_header_Extract_INNER_vlan                                                 ].regs[0] = 0x25;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_NVGRE_Append_Tunnel                 ].regs[0] = 0x2d;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_UL_Extract_header             ].regs[0] = 0x26;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Append_vlan ].regs[0] = 0x27;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Extract_vlan].regs[0] = 0x28;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Extract_OuterVlan_Append_Inner_VLAN].regs[0] = 0x2a;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_VXLAN_header                   ].regs[0] = 0x2b;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_Extract_Inner_vlan_VXLAN_header].regs[0] = 0x2c;

                                
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_NVGRE_Append_Tunnel_ADD_Outer_vlan    ].regs[0] = 0x2f;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE             ].regs[0] = 0x31;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE_ADD_Outer_vlan].regs[0] = 0x33;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_ONLY              ].regs[0] = 0x35;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_VxLAN             ].regs[0] = 0x38;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_PPPoE_DL_Append_header_Extract_valn_Stamp_DA_SA].regs[0] = 0x36;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Append_vlan_Stamp_DA_SA].regs[0] = 0x37;

    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_GRE_Append_Tunnel  ].regs[0] = 0x39;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_GRE_Extract_Tunnel ].regs[0] = 0x3A;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_L2TP_Append_Tunnel ].regs[0] = 0x3B;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_L2TP_Extract_Tunnel].regs[0] = 0x3C;
	MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_Extract_vlan ].regs[0] = 0x3d;
	MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_PPPoE_DL_Append_header_Swap_vlan ].regs[0] = 0x3e;
	MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Swap_vlan].regs[0] = 0x3f;

    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_APPEND                                          ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_APPEND_APPEND                                   ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_EXTRACT                                         ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_EXTRACT_EXTRACT                                 ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_SWAP                                            ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_SWAP_APPEND                                     ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_SWAP_EXTRACT                                    ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_SWAP_SWAP                                       ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL                          ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_ETHERTYPE_LABEL                ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_LABEL                    ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_ETHERTYPE_LABEL_LABLE          ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_LABEL                         ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_ETHERTYPE_LABEL               ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_LABEL_LABEL                   ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_ETHERTYPE_LABEL_LABLE         ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_SWAP_MPLS_LABEL                            ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_SWAP_MPLS_LABEL_LABEL                      ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APEND_MPLS_LABEL_SWAP_CFM_ETH_TO_MPLS_CFM  ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_LABEL_APPEND_ETHTYRTYP_MPLS_SWAP_CFM_ETH_TO_MPLS_CFM].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_Extract_MPLS_LABEL_Extract_ETHTYRTYP_MPLS_SWAP_MPLS_CFM_TO_CFM_ETH].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_Extract_MPLS_LABEL_LABEL_Extract_ETHTYRTYP_MPLS_SWAP_MPLS_CFM_TO_CFM_ETH].val.calc_checksum = MEA_FALSE;


    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_APPEND_BMAC_BTAG_ITAG           ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_EXTRACT_BMAC_BTAG_ITAG          ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_SWAP_BMAC_BTAG_ITAG             ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_APPEND_BMAC_APPEND_ITAG      ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_EXTRACT_BMAC_EXTRACT_ITAG    ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_SWAP_BMAC_SWAP_ITAG          ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_APPEND_BTAG_SWAP_ITAG        ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_SWAP_ISID                       ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_EXTRACT_BTAG_SWAP_ISID          ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MAC_IN_MAC_NoBTAG_SWAP_ITAG                     ].val.calc_checksum = MEA_FALSE;


    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_VLAN                                            ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_QTAG                                            ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_VLAN                                            ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_QTAG                                            ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent                                               ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN                                                   ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Swap_external_VLAN_TAG                                                  ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN_Swap_Internal_VLAN                                ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS                                                           ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG                                               ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Swap_External_TAG                                         ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG_Swap_Internal_VLAN                            ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_OFSET12                                                  ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent                            ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN                                ].val.calc_checksum = MEA_TRUE;
   
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QinQ                                               ].val.calc_checksum = MEA_FALSE;
    
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_Extract_QInQ                                                    ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ                                 ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_DL_IPinIP_Append_SGW                                                       ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_UL_IPinIP_Extract_SGW                                                      ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPCS_DL_UL_SGW_Swap_header                                                      ].val.calc_checksum = MEA_TRUE;
    
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_header_Append_vlan                                                        ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_header_Append_vlan_Extract_INNER_vlan                                     ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_header                                                                    ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_header_Extract_INNER_vlan                                                 ].val.calc_checksum = MEA_TRUE;
    
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_NVGRE_Append_Tunnel                                                             ].val.calc_checksum = MEA_TRUE;

    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_UL_Extract_header                                                         ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Append_vlan                                             ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Extract_vlan                                            ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Extract_OuterVlan_Append_Inner_VLAN                     ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_VXLAN_header                                       ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_Extract_Inner_vlan_VXLAN_header                    ].val.calc_checksum = MEA_TRUE;

    
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_NVGRE_Append_Tunnel_ADD_Outer_vlan   ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE            ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE_ADD_Outer_vlan].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_ONLY             ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_VxLAN            ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_GRE_Append_Tunnel                    ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_GRE_Extract_Tunnel                   ].val.calc_checksum = MEA_FALSE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_L2TP_Append_Tunnel                   ].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_L2TP_Extract_Tunnel                  ].val.calc_checksum = MEA_FALSE;
	MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_IPSec_Append_Tunnel_Extract_vlan     ].val.calc_checksum = MEA_TRUE;
	MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_PPPoE_DL_Append_header_Swap_vlan     ].val.calc_checksum = MEA_TRUE;
	MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Swap_vlan    ].val.calc_checksum = MEA_TRUE;
    
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_PPPoE_DL_Append_header_Extract_valn_Stamp_DA_SA].val.calc_checksum = MEA_TRUE;
    MEA_EHP_ProtocolMachine[MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Append_vlan_Stamp_DA_SA].val.calc_checksum = MEA_TRUE;
    

}






