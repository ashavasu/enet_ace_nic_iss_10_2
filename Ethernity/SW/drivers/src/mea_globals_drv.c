/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "MEA_platform_os_utils.h"
#include "mea_bm_drv.h"
#include "mea_drv_common.h"
#include "mea_if_l2cp_drv.h"
#include "mea_port_drv.h"
#include "mea_fwd_tbl.h"
#include "mea_db_drv.h"
#include "mea_globals_drv.h"
#include "mea_PortState_drv.h"

#if defined(MEA_OS_LINUX) && defined(MEA_PLAT_EVALUATION_BOARD)
#ifdef MEA_ENV_IPC
/*****************************/
/* include the file of IPC*/

/*****************************/

#endif

#endif


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
MEA_uint64 MEA_GLOBAL_SYS_Clock;

MEA_Globals_Entry_dbt MEA_Globals_Entry;
static MEA_Bool              MEA_Globals_Entry_InitDone = MEA_FALSE;

static MEA_Global_Interface_G999_1_dbt  MEA_Global_G999_1_Entry;
static MEA_Bool                         MEA_Global_Interface_G999_1_Entry_InitDone = MEA_FALSE;



/* CAM */
static MEA_CAM_Entry_dbt     MEA_CAM_Table[MEA_CAM_TBL_MAX_ENTRIES];
static MEA_Uint32            mea_drv_num_of_CAM_entries = 0;
static MEA_Bool              MEA_CAM_Tbl_InitDone = MEA_FALSE;
/* Device*/


MEA_Bool                    MEA_Counters_Type_Enable[MEA_COUNTERS_LAST_TYPE];
extern MEA_Bool mea_PM_en_calc_rate;
MEA_Uint32 mea_global_1p1_down_up_count_And_ports_enable;

/*GW  */
MEA_Bool MEA_GW_InitDone = MEA_FALSE;
static MEA_Gw_Global_dbt   MEA_GW_Global_Entry;
static MEA_Uint32          My_rootFilter;

MEA_PolicerTicks_Entry_dbt   MEA_Global_ServicePoliceTick;
MEA_PolicerTicks_Entry_dbt   MEA_Global_IngressFlowPolicerTick;


extern MEA_Uint32 mea_global_QueueFull_Handler_reset_sec;
extern MEA_Uint32 mea_global_QueueFull_Handler_TH_MQS;
extern MEA_Uint32 mea_global_QueueFull_Handler_TH_currentPacket;

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
static MEA_Status mea_Global_TBL_offset_UpdateEntry_pArray(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_Uint32 numofRegs, MEA_Uint32 *new_entry);
static MEA_Status mea_Global_TBL_offset_UpdateEntryBM_pArry(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_Uint32 numofRegs, MEA_Uint32 *new_entry);
 


/* CAM */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_CAM_Tbl>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_CAM_Tbl() {

    MEA_CAM_Tbl_InitDone = MEA_FALSE;
    
    if(b_bist_test){
        return MEA_OK;
    }
    /* Init IF CAM Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF CAM table ...\n");

    MEA_OS_memset(&(MEA_CAM_Table[0]),0,sizeof(MEA_CAM_Table));

    mea_drv_num_of_CAM_entries = 0;

    /* init done */
    MEA_CAM_Tbl_InitDone = MEA_TRUE;

    /* return to defaults */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_CAM_UpdateHwEntry>                                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#if 0
static MEA_Status  mea_CAM_UpdateHwEntry(MEA_CAM_Entry_dbt* entry)
{
	MEA_Uint32 val;
 
    val = 0;

	val  = ((entry->ethertype
              << MEA_OS_calc_shift_from_mask(MEA_IF_CAM_TABLE_ETHERTYPE_MASK)
             ) & MEA_IF_CAM_TABLE_ETHERTYPE_MASK
            ); 

	val |= ((entry->ether_pri
              << MEA_OS_calc_shift_from_mask(MEA_IF_CAM_TABLE_PRI_MASK)
             ) & MEA_IF_CAM_TABLE_PRI_MASK
            );

	val |= ((entry->l2cp
              << MEA_OS_calc_shift_from_mask(MEA_IF_CAM_TABLE_L2CP_MASK)
             ) & MEA_IF_CAM_TABLE_L2CP_MASK
            );

	val |= ((entry->valid
              << MEA_OS_calc_shift_from_mask(MEA_IF_CAM_TABLE_VALID_MASK)
             ) & MEA_IF_CAM_TABLE_VALID_MASK
            );

    MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0),val,MEA_MODULE_IF);

    return MEA_OK;
}
#endif
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_CAM_UpdateHw>                                             */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_CAM_UpdateHw(MEA_Uint32          index,
	                               MEA_CAM_Entry_dbt*  entry,
                                   MEA_CAM_Entry_dbt*  old_entry)
{

	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_CAM_GetFreeEntryIndex>                                    */
/*                                                                           */
/* Pay attention that the function sets the Valid flag and the Mapping ID    */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_CAM_GetFreeEntryIndex(MEA_Uint32* index_o)
{
    if ( index_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - index_o == NULL\n",
                             __FUNCTION__);
       return MEA_ERROR;         
    }

    for ( *index_o = 0; *index_o < MEA_CAM_TBL_MAX_ENTRIES; (*index_o)++ )    
    {         
		if ( MEA_CAM_Table[*index_o].valid == MEA_FALSE )
        {
            MEA_CAM_Table[*index_o].valid = MEA_TRUE;
            return MEA_OK;
        }    
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - no free CAM table entry index\n",
                             __FUNCTION__);
    return MEA_ERROR;         
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_systemClock_calc>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_uint64 MEA_systemClock_calc(MEA_Unit_t  unit_i,MEA_uint64 sysClk)
{
    
    MEA_uint64 clock;      
    if(sysClk == 133){
            clock=sysClk* 1000 * 1000 +333333;
            } else {
                if(sysClk == 100){
                    clock=sysClk* 1000 * 1000;
                } else {
                    if(sysClk == 166){
                      clock=sysClk* 1000 * 1000 + 777777;
                    } else{
                        if(sysClk != 0){
                          clock=sysClk* 1000 * 1000;
                        } else {/*default 100 */clock=100* 1000 * 1000; }
                    }
                }
            }
    return clock;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Is_CAM_EtherType_Exist>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool mea_drv_Is_CAM_EtherType_Exist ( MEA_Uint16 ethertype )                                          
{
    MEA_Uint32 index;

    for ( index = 0; index < MEA_CAM_TBL_MAX_ENTRIES; index++ )
    {
        if ((MEA_CAM_Table[index].valid == MEA_TRUE) && 
			(ethertype == MEA_CAM_Table[index].ethertype))           
        {
		     return MEA_TRUE; 
	    }
    }       

    return MEA_FALSE;
} 



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_CAM_Add_Entry>                                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_CAM_Add_Entry(MEA_CAM_Entry_dbt*  entry_i,
								 MEA_Uint32         *id_o)
{
	MEA_CAM_Entry_dbt entry;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (entry_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (id_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - id_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	/* Check if an entry with such ethertype already exists */
	if ( mea_drv_Is_CAM_EtherType_Exist((MEA_Uint16) entry_i->ethertype ) == MEA_TRUE )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - there is an existing CAM table entry with EtherType %x\n",__FUNCTION__, entry_i->ethertype);
        return MEA_ERROR;
    }   

    /* Get free entry index */
	if (mea_CAM_GetFreeEntryIndex(id_o) != MEA_OK) 
    {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_CAM_GetFreeEntryIndex failed \n",__FUNCTION__);
        return MEA_ERROR;
    }

	MEA_OS_memcpy(&entry, entry_i, sizeof(MEA_CAM_Entry_dbt));
	entry.ether_pri = entry_i->ether_pri;
	entry.valid = MEA_TRUE;

    /* Update the HW */
    if ( mea_CAM_UpdateHw(*id_o,
                          &entry,
                          &(MEA_CAM_Table[*id_o]) ) != MEA_OK )
    {
        /* free the new taken entry */
        MEA_CAM_Table[*id_o].valid = MEA_FALSE;
    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_CAM_UpdateHw failed \n",__FUNCTION__);
        return MEA_ERROR;
    }                          

	MEA_OS_memcpy(&(MEA_CAM_Table[*id_o]) , 
                  &entry, 
                  sizeof(MEA_CAM_Table[0]));

    mea_drv_num_of_CAM_entries++;

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        mea_drv_get_CAM_entry                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                              
MEA_Status mea_drv_get_CAM_entry (MEA_Uint32 	  	   id_i,
                                  MEA_CAM_Entry_dbt    *entry_o,
                                  MEA_Bool             *found_o)
{
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (entry_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (found_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Check for valid index parameter */
    MEA_CHECK_CAM_INDEX_IN_RANGE(id_i)
 
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    *found_o = MEA_FALSE;   
    
    if ( MEA_CAM_Table[id_i].valid == MEA_TRUE )
    {         
        MEA_OS_memcpy(entry_o,
                      &(MEA_CAM_Table[id_i]) , 
                      sizeof(*entry_o)); 
        *found_o = MEA_TRUE;
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                   MEA_API_IsCAMIndexInRange                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/   
MEA_Bool MEA_API_IsCAMIndexInRange(MEA_Uint32 index)
{

   if( index < MEA_CAM_TBL_MAX_ENTRIES )
   	return MEA_TRUE;
   else
    return MEA_FALSE;		

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        mea_drv_delete_CAM_entry                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
MEA_Status mea_drv_delete_CAM_entry (MEA_Uint32 id_i)
{
    MEA_CAM_Entry_dbt entry;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    /* Check for valid index parmeter */
    MEA_CHECK_CAM_INDEX_IN_RANGE(id_i)
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    
    if ( MEA_CAM_Table[id_i].valid == MEA_TRUE )
    {
        /* Delete from the SW shadow */
        MEA_OS_memset(&(MEA_CAM_Table[id_i]),0,sizeof(MEA_CAM_Entry_dbt));

        mea_drv_num_of_CAM_entries--;

        /* Delete from HW */     
		MEA_OS_memset( &entry,
                       0,
                       sizeof(MEA_CAM_Entry_dbt) );
        if ( mea_CAM_UpdateHw(id_i,
                              &entry,
                              0     )   != MEA_OK )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_CAM_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }       
    }
    else
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - No CAM entry on id %d\n",
                         __FUNCTION__, id_i);
       return MEA_ERROR; 
    }    
 
	mea_drv_num_of_CAM_entries--;

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        mea_drv_CAM_Is_Full                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
MEA_Bool mea_drv_CAM_Is_Full (void)
{
    if ( mea_drv_num_of_CAM_entries >= MEA_CAM_TBL_MAX_ENTRIES )
		return MEA_TRUE;
	else
		return MEA_FALSE;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Globals_UpdateHw>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Globals_UpdateHw(MEA_Unit_t unit_i,
    MEA_Globals_Entry_dbt *entry,
    MEA_Globals_Entry_dbt *old_entry)
{


    MEA_Uint32 val;
    MEA_PolicerTicks_Entry_dbt PolicerTicks_Entry;
    MEA_pos2_config_polling pos_config;
    MEA_Uint32 i;
    MEA_Uint32 arryOf_ts[2];

    if (b_bist_test) {
        return MEA_OK;
    }

    if ((!MEA_Globals_Entry_InitDone) ||
        (MEA_OS_memcmp(&(entry->Counters_Enable.val),
            &(old_entry->Counters_Enable.val), sizeof(entry->Counters_Enable.val)) != 0)) {

        MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TYPE] = entry->Counters_Enable.val.Counters_PMs;
        MEA_Counters_Type_Enable[MEA_COUNTERS_QUEUE_TYPE] = entry->Counters_Enable.val.Counters_Queues;
        MEA_Counters_Type_Enable[MEA_COUNTERS_BMS_TYPE] = entry->Counters_Enable.val.Counters_BMs;
        MEA_Counters_Type_Enable[MEA_COUNTERS_INGRESSPORTS_TYPE] = entry->Counters_Enable.val.Counters_IngressPorts;
        MEA_Counters_Type_Enable[MEA_COUNTERS_RMONS_TYPE] = entry->Counters_Enable.val.Counters_RMONs;
        MEA_Counters_Type_Enable[MEA_COUNTERS_ANALYZERS_TYPE] = entry->Counters_Enable.val.Counters_ANALYZERs;
        MEA_Counters_Type_Enable[MEA_COUNTERS_ANALYZERS_LM_TYPE] = entry->Counters_Enable.val.Counters_LMs;
        MEA_Counters_Type_Enable[MEA_COUNTERS_ANALYZERS_CCM_TYPE] = entry->Counters_Enable.val.Counters_CCMs;
        MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TDM_TYPE] = entry->Counters_Enable.val.Counters_PMs_TDM;
        MEA_Counters_Type_Enable[MEA_COUNTERS_RMONS_TDM_TYPE] = entry->Counters_Enable.val.Counters_RMONs_TDM;
        MEA_Counters_Type_Enable[MEA_COUNTERS_CES_EVENT_TDM_TYPE] = entry->Counters_Enable.val.eventCes; // only for simulation
        MEA_Counters_Type_Enable[MEA_COUNTERS_UTOPIA_RMONS_TYPE] = entry->Counters_Enable.val.Counters_UTOPIA_RMONs;
        MEA_Counters_Type_Enable[MEA_COUNTERS_TDM_EGRESS_FIFO_TYPE] = entry->Counters_Enable.val.Counters_EgressFifo_TDM;
        MEA_Counters_Type_Enable[MEA_COUNTERS_BONDING_TYPE] = entry->Counters_Enable.val.Counters_Bonding;
        MEA_Counters_Type_Enable[MEA_COUNTERS_FORWARDER_TYPE] = entry->Counters_Enable.val.Counters_forwarder;
        MEA_Counters_Type_Enable[MEA_COUNTERS_INGRESS_TYPE] = entry->Counters_Enable.val.Counters_Ingress;
        MEA_Counters_Type_Enable[MEA_COUNTERS_HC_RMON_TYPE] = entry->Counters_Enable.val.Counters_HC_RMON;
        MEA_Counters_Type_Enable[MEA_COUNTERS_ACL_TYPE] = entry->Counters_Enable.val.Counters_ACL;
        MEA_Counters_Type_Enable[MEA_COUNTERS_QUEUE_HANDLER_TYPE] = entry->Counters_Enable.val.Counters_QUEUE_HANDLER;
        MEA_Counters_Type_Enable[MEA_COUNTERS_Virtual_Rmon_TYPE] = entry->Counters_Enable.val.Counters_Virtual_Rmon;
    }





    /* Update only if change or in Init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->bm_config.reg[0] != old_entry->bm_config.reg[0])) {

        MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_CONFIG, entry->bm_config.reg[0], MEA_MODULE_BM);

    }
#if 0
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->iTHi != old_entry->iTHi)) {
        if (entry->iTHi != 0)
            MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_SW_PR_TH_IL, entry->iTHi, MEA_MODULE_IF);

    }
#endif


    /*Update policer tic*/
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->ticks_for_fast_service != old_entry->ticks_for_fast_service) ||
        (entry->ticks_for_slow_service != old_entry->ticks_for_slow_service)) {
        PolicerTicks_Entry.ticks_for_slow = entry->ticks_for_slow_service;
        PolicerTicks_Entry.ticks_for_fast = entry->ticks_for_fast_service;
        if (MEA_API_Set_PolicerTicks_Entry(MEA_UNIT_0, &PolicerTicks_Entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Set_PolicerTicks_Entry failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }


    /*Update IngFlow policer tic*/
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->ticks_for_slow_IngFlow != old_entry->ticks_for_slow_IngFlow) ||
        (entry->ticks_for_fast_IngFlow != old_entry->ticks_for_fast_IngFlow)) {
        PolicerTicks_Entry.ticks_for_slow = entry->ticks_for_slow_IngFlow;
        PolicerTicks_Entry.ticks_for_fast = entry->ticks_for_fast_IngFlow;
        if (MEA_API_Set_IngressFlowPolicerTicks_Entry(MEA_UNIT_0, &PolicerTicks_Entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Set_IngressFlowPolicerTicks_Entry failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }


    /*update bm_watchdog*/
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->bm_watchdog_parm.reg[0] != old_entry->bm_watchdog_parm.reg[0])) {

        mea_Global_TBL_offset_UpdateEntryBM(unit_i, (MEA_GLOBAl_TBL_REG_SET_BM_WD), entry->bm_watchdog_parm.reg[0]);

    }

    



    /*update ccm tick*/
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->CCM_Tick.reg != old_entry->CCM_Tick.reg)) {

        MEA_API_WriteReg(unit_i, ENET_BM_CCM_TICK_READ_REG, entry->CCM_Tick.reg, MEA_MODULE_BM);

    }
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->Device_edit_src_ip.my_Ip != old_entry->Device_edit_src_ip.my_Ip)) {
        if (MEA_TDM_SUPPORT)
            MEA_API_WriteReg(unit_i, ENET_BM_SRC_IP, entry->Device_edit_src_ip.my_Ip, MEA_MODULE_BM);

    }


    /* Update only if change or in Init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->max_used_descriptors_high != old_entry->max_used_descriptors_high) ||
        (entry->max_used_buffers_high != old_entry->max_used_buffers_high)) {

        val = MEA_API_ReadReg(MEA_UNIT_0, MEA_BM_DESC_H_LIMIT, MEA_MODULE_BM);

        val &= ~MEA_BM_DESC_H_LIMIT_MAX_USED_DESCRIPTORS_LIMITER_MASK;
        val |= (entry->max_used_descriptors_high <<
            MEA_OS_calc_shift_from_mask
            (MEA_BM_DESC_H_LIMIT_MAX_USED_DESCRIPTORS_LIMITER_MASK));

        val &= ~MEA_BM_DESC_H_LIMIT_MAX_USED_BUFFERS_LIMITER_MASK;
        val |= (entry->max_used_buffers_high <<
            MEA_OS_calc_shift_from_mask
            (MEA_BM_DESC_H_LIMIT_MAX_USED_BUFFERS_LIMITER_MASK));

        MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_DESC_H_LIMIT, val, MEA_MODULE_BM);

    }

    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->CCM_Bucket.val.wm != old_entry->CCM_Bucket.val.wm) ||
        (entry->CCM_Bucket.val.xCir != old_entry->CCM_Bucket.val.xCir)) {

        MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_CCM_BUCK_REG_CONF, entry->CCM_Bucket.reg[0], MEA_MODULE_BM);


    }


    /* Update only if change or in Init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->max_used_descriptors_low != old_entry->max_used_descriptors_low) ||
        (entry->max_used_buffers_low != old_entry->max_used_buffers_low)) {

        val = MEA_API_ReadReg(MEA_UNIT_0, MEA_BM_DESC_L_LIMIT, MEA_MODULE_BM);

        val &= ~MEA_BM_DESC_L_LIMIT_MAX_USED_DESCRIPTORS_LIMITER_MASK;
        val |= (entry->max_used_descriptors_low <<
            MEA_OS_calc_shift_from_mask
            (MEA_BM_DESC_L_LIMIT_MAX_USED_DESCRIPTORS_LIMITER_MASK));

        val &= ~MEA_BM_DESC_L_LIMIT_MAX_USED_BUFFERS_LIMITER_MASK;
        val |= (entry->max_used_buffers_low <<
            MEA_OS_calc_shift_from_mask
            (MEA_BM_DESC_L_LIMIT_MAX_USED_BUFFERS_LIMITER_MASK));

        MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_DESC_L_LIMIT, val, MEA_MODULE_BM);

    }



    /* Update control reg only if change or in init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_global0.reg != old_entry->if_global0.reg))
    {
        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_GLOBAL0_REG, entry->if_global0.reg, MEA_MODULE_IF);

        mea_global_fwd_act = (MEA_Uint16)entry->if_global0.val.fwd_act_offset;
    }

    /* Update control reg only if change or in init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_global1.reg != old_entry->if_global1.reg))
    {
        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_GLOBAL1_REG, entry->if_global1.reg, MEA_MODULE_IF);
    }


    /* Update control reg only if change or in init process */
    if ((entry->if_global2_Gbe.reg != 0) &&
        ((!MEA_Globals_Entry_InitDone) ||
            (entry->if_global2_Gbe.reg != old_entry->if_global2_Gbe.reg)))
    {
        /*only HW set the value */
        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_GLOBAL2_REG, entry->if_global2_Gbe.reg, MEA_MODULE_IF);
    }

    if ((entry->if_threshold_G999.reg != 0) &&
        ((!MEA_Globals_Entry_InitDone) ||
            (entry->if_threshold_G999.reg != old_entry->if_threshold_G999.reg)))
    {
        /*only HW set the value */
        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_G999_TH_REG, entry->if_threshold_G999.reg, MEA_MODULE_IF);
    }

    if ((entry->if_threshold_TLS.reg != 0) &&
        (((!MEA_Globals_Entry_InitDone) ||
            (entry->if_threshold_TLS.reg != old_entry->if_threshold_TLS.reg))))
    {
        /*only HW set the value */
        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_TLS_TH_REG, entry->if_threshold_TLS.reg, MEA_MODULE_IF);
    }

    if ((entry->if_threshold_CPU.reg != 0) &&
        ((!MEA_Globals_Entry_InitDone) ||
            (entry->if_threshold_CPU.reg != old_entry->if_threshold_CPU.reg)))
    {
        /*only HW set the value */
        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_PHY_CPU_TH, entry->if_threshold_CPU.reg, MEA_MODULE_IF);
    }





    /* Update control reg only if change or in init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_configure_1.reg != old_entry->if_configure_1.reg))
    {
        if (MEA_INGRESS_ATM_OVER_POS_SUPPORT) {
            MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_FRAG_ART_EOP_SIZE, entry->if_configure_1.reg, MEA_MODULE_IF);
        }
    }


    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_configure_3.reg != old_entry->if_configure_3.reg))
    {
        if (entry->if_configure_3.val.if_ingress_shaper != 0)
            MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_TO_BM_INGRESS_SHAPER_REG, entry->if_configure_3.reg, MEA_MODULE_IF);

    }

    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_External_srv.reg != old_entry->if_External_srv.reg))
    {

        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_SERVICE_EXTERNAL_REG, entry->if_External_srv.reg, MEA_MODULE_IF);

    }



    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_mirror_ces_port.reg != old_entry->if_mirror_ces_port.reg))
    {

        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_DBG_MIRROR_CES_PORT_REG, entry->if_mirror_ces_port.reg, MEA_MODULE_IF);

    }

    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->Interface_mirror.reg != old_entry->Interface_mirror.reg))
    {

        if (mea_drv_write_IF_TBL_X_offset_UpdateEntry_pArray(MEA_UNIT_0, ENET_IF_CMD_PARAM_TBL_100_REGISTER,
            ENET_REGISTER_TBL100_Configure_INGRESS_MIROR,
            1,
            &entry->Interface_mirror.reg) != MEA_OK) {
            return MEA_ERROR;
        }

 
    }


    /* Update if sssmii_rx_config reg only if change or in init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_sssmii_rx_config.reg != old_entry->if_sssmii_rx_config.reg))
    {
        if (mea_SSSMII_interface_support)
            MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_SSSMII_RX_CONFIG_REG, entry->if_sssmii_rx_config.reg, MEA_MODULE_IF);
    }

    /* Update if sssmii_tx_config0 reg only if change or in init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_sssmii_tx_config0.reg != old_entry->if_sssmii_tx_config0.reg))
    {
        if (mea_SSSMII_interface_support)
            MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_SSSMII_TX_CONFIG0_REG, entry->if_sssmii_tx_config0.reg, MEA_MODULE_IF);
    }

    /* Update if sssmii_tx_config1 reg only if change or in init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_sssmii_tx_config1.reg != old_entry->if_sssmii_tx_config1.reg))
    {
        if (mea_SSSMII_interface_support)
            MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_SSSMII_TX_CONFIG1_REG, entry->if_sssmii_tx_config1.reg, MEA_MODULE_IF);
    }

    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->if_1p1_down_up_count.to_down != old_entry->if_1p1_down_up_count.to_down) ||
        (entry->if_1p1_down_up_count.to_Up != old_entry->if_1p1_down_up_count.to_Up))
    {

        mea_global_1p1_down_up_count_And_ports_enable = mea_global_1p1_down_up_count_And_ports_enable & 0x0000fff;

        mea_global_1p1_down_up_count_And_ports_enable |= entry->if_1p1_down_up_count.to_down << 26;
        mea_global_1p1_down_up_count_And_ports_enable |= entry->if_1p1_down_up_count.to_Up << 16;
        if (MEA_PROTECT_1PLUS1_SUPPORT)
            MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_EG_1P1_ENABLE_REG, mea_global_1p1_down_up_count_And_ports_enable, MEA_MODULE_IF);
    }



    /* Update if bm_config0 regs only if change or in init process */
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->bm_config0.reg[0] != old_entry->bm_config0.reg[0])
        )
    {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_BM_PKT_SHORT_LIMIT_REG,
            ((MEA_Uint32)(entry->bm_config0.val.pkt_short_limit) & MEA_BM_PKT_SHORT_LIMIT_MASK),
            MEA_MODULE_BM);

        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_BM_PKT_HIGH_LIMIT_REG,
            ((MEA_Uint32)(entry->bm_config0.val.pkt_high_limit) & MEA_BM_PKT_HIGH_LIMIT_MASK),
            MEA_MODULE_BM);
    }

    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->bm_config0.reg[1] != old_entry->bm_config0.reg[1])
        )
    {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_BM_CHUNK_SHORT_LIMIT_REG,
            ((MEA_Uint32)(entry->bm_config0.val.chunk_short_limit) & MEA_BM_CHUNK_SHORT_LIMIT_MASK),
            MEA_MODULE_BM);

        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_BM_CHUNK_HIGH_LIMIT_REG,
            ((MEA_Uint32)(entry->bm_config0.val.chunk_high_limit) & MEA_BM_CHUNK_HIGH_LIMIT_MASK),
            MEA_MODULE_BM);
    }

    if ((!MEA_Globals_Entry_InitDone) &&
        (entry->bm_config1.reg[0] != 0) &&
        (entry->bm_config1.reg[0] != old_entry->bm_config1.reg[0]))
    {
        MEA_API_WriteReg(MEA_UNIT_0,
            ENET_BM_BEST_EFFORT_REG,
            (MEA_Uint32)(entry->bm_config1.reg[0]),
            MEA_MODULE_BM);
    }
    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->bm_config2.reg[0] != old_entry->bm_config2.reg[0]))
    {
        MEA_API_WriteReg(MEA_UNIT_0,
            ENET_BM_RED_OFFSET,
            (MEA_Uint32)(entry->bm_config2.reg[0]),
            MEA_MODULE_BM);
    }

    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->WBRG_swap_Eth.reg[0] != old_entry->WBRG_swap_Eth.reg[0]))
    {
        if (MEA_WBRG_SUPPORT) {
            MEA_API_WriteReg(MEA_UNIT_0,
                ENET_BM_WBRG_ETH_REG,
                (MEA_Uint32)(entry->WBRG_swap_Eth.reg[0]),
                MEA_MODULE_BM);
        }
    }

    
        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->reorder_frag_time_out != old_entry->reorder_frag_time_out)){
                {
                    MEA_uint64 sysclck;
                    MEA_Uint32 value=1000;

                    sysclck = MEA_GLOBAL_SYS_Clock;
                    if (sysclck) {
                        value =  (MEA_Uint32)((sysclck/100000000) * entry->reorder_frag_time_out)/256;
                        
                        if (value > 0xFFFF) {
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - reorder_frag_time_out failed \n",
                                __FUNCTION__);
                            return MEA_ERROR;

                        }
                    }
                    MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_REORDER_TIMEOUT,
                            (MEA_Uint32)(value), MEA_MODULE_BM);
                }
        }


    /* Update Shaper fast port cluster  */
    if ((!MEA_Globals_Entry_InitDone)                           ||
		(entry->shaper_fast_port != old_entry->shaper_fast_port) ||  
        (entry->shaper_fast_cluster != old_entry->shaper_fast_cluster))
    {
        
        val = 0;
		val |= (entry->shaper_fast_port << 0 );
        val |= (entry->shaper_fast_cluster << 16 );
        
        MEA_API_WriteReg(MEA_UNIT_0,
                         MEA_BM_SHAPER_FAST_PORT_CLUSTER_REG,
                         val,
                         MEA_MODULE_BM);
    }

     /* Update Shaper fast priQueue  */
    if ((!MEA_Globals_Entry_InitDone)                           ||
		(entry->shaper_fast_PriQueue != old_entry->shaper_fast_PriQueue) )
    {
       
        val = 0;
		val |= (entry->shaper_fast_PriQueue << 0 );
        
        
        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_BM_SHAPER_FAST_PRIQUEUE_REG,
                         val,
                         MEA_MODULE_BM);
    }


	/* Update Shaper slow */
    if ((!MEA_Globals_Entry_InitDone)                           ||
		(entry->shaper_slow_multiplexer_port != old_entry->shaper_slow_multiplexer_port) ||
        (entry->shaper_slow_multiplexer_cluster != old_entry->shaper_slow_multiplexer_cluster) ||
        (entry->shaper_slow_multiplexer_PriQueue != old_entry->shaper_slow_multiplexer_PriQueue))
    {
       	val=0;
        val |= entry->shaper_slow_multiplexer_port <<0;
        val |= entry->shaper_slow_multiplexer_cluster <<3;
        val |= entry->shaper_slow_multiplexer_PriQueue <<6;

        MEA_API_WriteReg(MEA_UNIT_0,
                         MEA_BM_SHAPER_SLOW_MULTIPLEXER_REG,
                         val,
                         MEA_MODULE_BM);
    }

    if ((!MEA_Globals_Entry_InitDone)                           ||
        (entry->ticks_for_afdx != old_entry->ticks_for_afdx))
    {
       	
        if(MEA_AFDX_SUPPORT){
            MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_AFDX_BAG_TICKS, entry->ticks_for_afdx, MEA_MODULE_BM);
        }

    }



	/* Update Mac Aging*/
    if ((!MEA_Globals_Entry_InitDone)                           ||
		(entry->macAging.if_mac_ageing != old_entry->macAging.if_mac_ageing)   )
    {
       	
        mea_memory_mode_e  save_globalMemoryMode;


    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,MEA_FWD_HW_UNIT);
        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_MAC_AGEING_REG,
                         entry->macAging.if_mac_ageing,
                         MEA_MODULE_IF);
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }



    if ((!MEA_Globals_Entry_InitDone)  || 
          (MEA_OS_maccmp(&(entry->Globals_Device.MAC_802_1ag.Mac_Address),
                         &(old_entry->Globals_Device.MAC_802_1ag.Mac_Address)) !=0 ) ||
          (entry->Globals_Device.MAC_802_1ag.command != old_entry->Globals_Device.MAC_802_1ag.command))
    {
        val=0;
        val  = entry->Globals_Device.MAC_802_1ag.Mac_Address.b[5]>>4;
        val |= entry->Globals_Device.MAC_802_1ag.Mac_Address.b[4]<<4;
        val |= entry->Globals_Device.MAC_802_1ag.Mac_Address.b[3]<<12;
        val |= entry->Globals_Device.MAC_802_1ag.Mac_Address.b[2]<<20;
        
        val |= (entry->Globals_Device.MAC_802_1ag.Mac_Address.b[1]& 0x0f) << (28);
            
        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_802_1AG_MAC_LO_REG,
                         val,
                         MEA_MODULE_IF);
        val=0;
        val = entry->Globals_Device.MAC_802_1ag.Mac_Address.b[1]>>4;
        val |= entry->Globals_Device.MAC_802_1ag.Mac_Address.b[0] <<4;

        
        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_802_1AG_MAC_HI_REG,
                         val,
                         MEA_MODULE_IF);
        

        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_802_1AG_COMMAND_REG,
                         entry->Globals_Device.MAC_802_1ag.command,
                         MEA_MODULE_IF);
         
    }
#if 0   
    if ((!MEA_Globals_Entry_InitDone) ||
        (MEA_OS_maccmp(&(entry->Globals_Device.MAC_ETH.Mac_Address),
                       &(old_entry->Globals_Device.MAC_ETH.Mac_Address))!=0) ||
        (entry->Globals_Device.MAC_ETH.ethertype != old_entry->Globals_Device.MAC_ETH.ethertype) || 
        (entry->Globals_Device.MAC_ETH.command != old_entry->Globals_Device.MAC_ETH.command) ||
        (entry->Globals_Device.MAC_ETH.location != old_entry->Globals_Device.MAC_ETH.location))
    {
        val=0;
        val=entry->Globals_Device.MAC_ETH.Mac_Address.b[5];
        val |= entry->Globals_Device.MAC_ETH.Mac_Address.b[4]<<8;
        val |= entry->Globals_Device.MAC_ETH.Mac_Address.b[3]<<16;
        val |= entry->Globals_Device.MAC_ETH.Mac_Address.b[2]<<24;

        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_GLOBAL_OAM_ETH_MAC_LO_REG,
                         val,
                         MEA_MODULE_IF);
        val=0;
        val= entry->Globals_Device.MAC_ETH.Mac_Address.b[1];
        val |= entry->Globals_Device.MAC_ETH.Mac_Address.b[0]<<8;
        val |= (entry->Globals_Device.MAC_ETH.ethertype <<16);
       

        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_GLOBAL_OAM_ETH_MAC_HI_REG,
                         val,
                         MEA_MODULE_IF);
       
        val=0;
        val = entry->Globals_Device.MAC_ETH.command;
        val |= entry->Globals_Device.MAC_ETH.location<<2;

        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_GLOBA_OAM_ETH_COMMAND_LOCACTION_REG,
                         val,
                         MEA_MODULE_IF);
    
    }
#endif

    if ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) &&
        ((!MEA_Globals_Entry_InitDone) ||
         (MEA_OS_maccmp(&(entry->L2TP_Tunnel_address),
                        &(old_entry->L2TP_Tunnel_address)) != 0))) {

        val =  (((MEA_Uint32)((unsigned char)(entry->L2TP_Tunnel_address.b[2])) << 24) |
                ((MEA_Uint32)((unsigned char)(entry->L2TP_Tunnel_address.b[3])) << 16) |
                ((MEA_Uint32)((unsigned char)(entry->L2TP_Tunnel_address.b[4])) <<  8) |
                ((MEA_Uint32)((unsigned char)(entry->L2TP_Tunnel_address.b[5])) <<  0) ) ;

        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_GLOBAL_L2TP_TUNNEL_MAC_LO,
                         val, 
                         MEA_MODULE_IF);

        val =  (((MEA_Uint32)((unsigned char)(entry->L2TP_Tunnel_address.b[0])) <<  8) |
                ((MEA_Uint32)((unsigned char)(entry->L2TP_Tunnel_address.b[1])) <<  0) ) ;

        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_GLOBAL_L2TP_TUNNEL_MAC_HI,
                         val,
                         MEA_MODULE_IF);
    }

    

        if ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) &&
            ((!MEA_Globals_Entry_InitDone) ||
            (MEA_OS_maccmp(&(entry->SA_My_Mac),
            &(old_entry->SA_My_Mac)) != 0))) {

            val = 0;
            val |= (((MEA_Uint32)((unsigned char)(entry->SA_My_Mac.b[2]))) << 24);
            val |= (((MEA_Uint32)((unsigned char)(entry->SA_My_Mac.b[3]))) << 16);
            val |= (((MEA_Uint32)((unsigned char)(entry->SA_My_Mac.b[4]))) << 8);
            val |= (((MEA_Uint32)((unsigned char)(entry->SA_My_Mac.b[5]))) << 0);
            MEA_API_WriteReg(MEA_UNIT_0,
                ENET_BM_GLOBAL_MY_MAC_LO,
                val,
                MEA_MODULE_BM);
            
            val = 0;
            val |= (((MEA_Uint32)((unsigned char)(entry->SA_My_Mac.b[0]))) << 8);
            val |= (((MEA_Uint32)((unsigned char)(entry->SA_My_Mac.b[1]))) << 0);
            MEA_API_WriteReg(MEA_UNIT_0,
                ENET_BM_GLOBAL_MY_MAC_HI,
                val,
                MEA_MODULE_BM);

        
        }

    if ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) &&
        ((!MEA_Globals_Entry_InitDone) ||
         (MEA_OS_maccmp(&(entry->CFM_OAM.UC_Mac_Address),
                        &(old_entry->CFM_OAM.UC_Mac_Address)) !=0))) {
        val=0;
        val |= (((MEA_Uint32)((unsigned char)(entry->CFM_OAM.UC_Mac_Address.b[2])))<<24);
        val |= (((MEA_Uint32)((unsigned char)(entry->CFM_OAM.UC_Mac_Address.b[3])))<<16);
        val |= (((MEA_Uint32)((unsigned char)(entry->CFM_OAM.UC_Mac_Address.b[4])))<< 8);
        val |= (((MEA_Uint32)((unsigned char)(entry->CFM_OAM.UC_Mac_Address.b[5])))<< 0);
#if 0
        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_GLOBAL_CFM_OAM_UC_MAC_LO,
                         val,
                         MEA_MODULE_IF);

        
        val = 0;
        val |= (((MEA_Uint32)((unsigned char)(entry->CFM_OAM.UC_Mac_Address.b[0])))<< 8);
        val |= (((MEA_Uint32)((unsigned char)(entry->CFM_OAM.UC_Mac_Address.b[1])))<< 0);
        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_GLOBAL_CFM_OAM_UC_MAC_HI,
                         val,
                         MEA_MODULE_IF);
    
#endif    
    }

    if ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) &&
        ((!MEA_Globals_Entry_InitDone) ||
        (MEA_OS_memcmp(&(entry->Local_Mac_Address),
        &(old_entry->Local_Mac_Address), sizeof(entry->Local_Mac_Address)) != 0))) {

        for (i = 0; i < MEA_MAX_LOCAL_MAC; i++){
            if (((!MEA_Globals_Entry_InitDone) ||
                (MEA_OS_maccmp(&(entry->Local_Mac_Address[i]),
                &(old_entry->Local_Mac_Address[i])) != 0))) 
            {
                /* write to table offset  */
                if (mea_Global_offsetMAC_UpdateEntry(unit_i,
                    MEA_GLOBAl_TBL_REG_LOCAL_MAC0 + i,
                    entry->Local_Mac_Address[i]) != MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
                        "%s - Local_Mac_Address offset %d  \n",
                        __FUNCTION__, MEA_GLOBAl_TBL_REG_LOCAL_MAC0 + i);
                
                }
                if (MEA_VXLAN_SUPPORT) {
                    if (mea_Global_offsetMAC_UpdateEntryBM(unit_i,
                        MEA_GLOBAl_TBL_REG_LOCAL_MAC0 + i,
                        entry->Local_Mac_Address[i]) != MEA_OK) {

                    }
                }


            }
        }
    }

    
    



    if ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) &&
        ((!MEA_Globals_Entry_InitDone) ||
        (MEA_OS_memcmp(&(entry->Local_IP_Address),
        &(old_entry->Local_IP_Address), sizeof(entry->Local_IP_Address)) != 0))) 
    {

        for (i = 0; i < MEA_MAX_LOCAL_IP; i++)
        {
            if (((!MEA_Globals_Entry_InitDone) ||
                (entry->Local_IP_Address[i]) != (old_entry->Local_IP_Address[i]))) {
                /* write to table offset  */
                mea_Global_TBL_offset_UpdateEntry(unit_i, MEA_GLOBAl_TBL_REG_LOCAL_IP0 + i, entry->Local_IP_Address[i]);
                if (MEA_VXLAN_SUPPORT) {
                    mea_Global_TBL_offset_UpdateEntryBM(unit_i, MEA_GLOBAl_TBL_REG_LOCAL_IP0 + i, entry->Local_IP_Address[i]);
                }
            }
        }
    }
 
    
    if (MEA_IPSec_SUPPORT) {

        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->Local_IP_my_Ipsec != old_entry->Local_IP_my_Ipsec))
        {
            mea_Global_TBL_offset_UpdateEntry(unit_i, MEA_GLOBAl_TBL_REG_LOCAL_IP7_MY_IpSec, entry->Local_IP_my_Ipsec);
        }
    }



    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->EthType_cam.reg[0] != old_entry->EthType_cam.reg[0]))
    {
        mea_Global_TBL_offset_UpdateEntry(unit_i, MEA_GLOBAl_TBL_REG_ETHETYPE_CAM, entry->EthType_cam.reg[0]);
    }

    if (MEA_BFD_SUPPORT) {
        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->MyBFD_Discriminator != old_entry->MyBFD_Discriminator))
        {

            mea_Global_TBL_offset_UpdateEntry(unit_i, MEA_GLOBAl_TBL_REG_BFD_MY_Discriminator, entry->MyBFD_Discriminator);
        }

        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->Local_BFD_SW_IP_Address != old_entry->Local_BFD_SW_IP_Address))
        {

            mea_Global_TBL_offset_UpdateEntry(unit_i, MEA_GLOBAl_TBL_REG_BFD_MY_SW_IPV4_ADDRES, entry->Local_BFD_SW_IP_Address);
        }
    }

    if (MEA_NVGRE_SUPPORT) {
        if ((!MEA_Globals_Entry_InitDone) || 
             (MEA_OS_memcmp(&entry->nvgre_info.Gre_src_mac, &old_entry->nvgre_info.Gre_src_mac,
                sizeof(MEA_MacAddr)) != 0)) 
        {
            mea_Global_offsetMAC_UpdateEntryBM(unit_i, MEA_GLOBAl_TBL_REG_SET_MY_GRE_MAC, entry->nvgre_info.Gre_src_mac);
        }

        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->nvgre_info.outer_vlanId != old_entry->nvgre_info.outer_vlanId))
        {
            mea_Global_TBL_offset_UpdateEntryBM(unit_i, MEA_GLOBAl_TBL_REG_SET_OUTER_TUNNEL_VID, entry->nvgre_info.outer_vlanId);
        }

        
        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->nvgre_info.Gre_ttl != old_entry->nvgre_info.Gre_ttl))
        {
            mea_Global_TBL_offset_UpdateEntryBM(unit_i, MEA_GLOBAl_TBL_REG_SET_IP_TTL_GRE, entry->nvgre_info.Gre_ttl);
        }

        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->nvgre_info.Gre_src_Ipv4 != old_entry->nvgre_info.Gre_src_Ipv4))
        {
            mea_Global_TBL_offset_UpdateEntryBM(unit_i, MEA_GLOBAl_TBL_REG_SET_SRC_IPV4_GRE, entry->nvgre_info.Gre_src_Ipv4);
        }
    
    }

    if (MEA_IPSec_SUPPORT)
    {
    
        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->Ipsec_info.ipsec_ttl != old_entry->Ipsec_info.ipsec_ttl))
        {
            mea_Global_TBL_offset_UpdateEntryBM(unit_i, MEA_GLOBAl_TBL_REG_SET_IP_TTL_IPSEC, entry->Ipsec_info.ipsec_ttl);
        }

        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->Ipsec_info.ipsec_src_Ipv4 != old_entry->Ipsec_info.ipsec_src_Ipv4))
        {
            mea_Global_TBL_offset_UpdateEntryBM(unit_i, MEA_GLOBAl_TBL_REG_SET_SRC_IPV4_IPSEC, entry->Ipsec_info.ipsec_src_Ipv4);
        }
    }


    if ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) &&
        ((!MEA_Globals_Entry_InitDone) ||
        (MEA_OS_memcmp(&(entry->acm_mode_configure),
        &(old_entry->acm_mode_configure),sizeof(entry->acm_mode_configure)) !=0))) {
          
            MEA_API_WriteReg(MEA_UNIT_0,
                ENET_BM_ACM_MODE_REG,
                entry->acm_mode_configure.reg[0],
                MEA_MODULE_BM);
    }


    if ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) &&
        ((!MEA_Globals_Entry_InitDone) ||
         (entry->CFM_OAM.EtherType != old_entry->CFM_OAM.EtherType) ||
         (entry->CFM_OAM.valid != old_entry->CFM_OAM.valid))) {
          val  = 0;
          val  = (MEA_Uint32)entry->CFM_OAM.EtherType;
          val |= (((MEA_Uint32) entry->CFM_OAM.valid<<16) & (0x00010000)); 
#if 0
        MEA_API_WriteReg(MEA_UNIT_0,
                         MEA_IF_GLOBAL_CFM_OAM_ETHER_TYPE_REG,
                         val,
                         MEA_MODULE_IF);
#endif

    }
    
    if ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) &&
        ((!MEA_Globals_Entry_InitDone) ||
        (MEA_OS_memcmp(&(entry->PacketAnalyzer.val) ,&(old_entry->PacketAnalyzer.val),sizeof(entry->PacketAnalyzer.val))!=0)) ) {
        
            if((entry->PacketAnalyzer.val.enable == MEA_TRUE) && (MEA_PACKET_ANALYZER_TYPE1_SUPPORT  == MEA_FALSE)){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -  can't set  on when we not support  \n",
                         __FUNCTION__);
                return MEA_ERROR;    
         }
        
        val  = (MEA_Uint32)entry->PacketAnalyzer.reg;
        MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_BM_ANALZER_ETERTYPE_REG,
                         val,
                         MEA_MODULE_BM);
    }
    
   
    //stp_mod
     if ((!MEA_Globals_Entry_InitDone)                           ||
         (MEA_OS_memcmp(&(entry->stp_mode.val) ,&(old_entry->stp_mode.val),sizeof(entry->stp_mode.val))!=0) ) 
     {
         
         if( mea_dev_PortState_set_Global_Mode(unit_i,
                                               (MEA_PortState_te) entry->stp_mode.val.mode,
                                               (MEA_PortState_te) globalSTP_Mode) !=MEA_OK)
         {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -  mea_dev_PortState_set_Global_Mode failed  \n",
                         __FUNCTION__);
                return MEA_ERROR; 
         }
         // we need to to save the stpMode in the global
         globalSTP_Mode=entry->stp_mode.val.mode;
         globalSTP_Default_State=entry->stp_mode.val.default_state;

     }

     
     if ((!MEA_Globals_Entry_InitDone)                           ||
         (MEA_OS_memcmp(&(entry->WallClock_Calibrate.val) ,&(old_entry->WallClock_Calibrate.val),sizeof(entry->WallClock_Calibrate.val))!=0) ) 
     {

         MEA_API_WriteReg(MEA_UNIT_0,
                     MEA_BM_ADD_SUB_GRN_WALL_CLOCK_REG,
                     entry->WallClock_Calibrate.reg[0],
                     MEA_MODULE_BM);

     }
     if ((!MEA_Globals_Entry_InitDone)                           ||
         (entry->bm_set_delay.reg[0] != old_entry->bm_set_delay.reg[0])   )
     {
         if(entry->bm_set_delay.reg[0] == 0){
             /*read from HW the default value */
             entry->bm_set_delay.reg[0] = MEA_API_ReadReg(MEA_UNIT_0,ENET_BM_DESCRIPTOR_DELAY_REG,MEA_MODULE_BM);

         }else{
             MEA_API_WriteReg(MEA_UNIT_0,
                 ENET_BM_DESCRIPTOR_DELAY_REG,
                 entry->bm_set_delay.reg[0],
                 MEA_MODULE_BM);
         }
     }


      if ((!MEA_Globals_Entry_InitDone)                           ||
          (MEA_OS_memcmp(&(entry->WallClock_1Sec.val) ,&(old_entry->WallClock_1Sec.val),sizeof(entry->WallClock_1Sec.val))!=0) ) 
     {

         MEA_API_WriteReg(MEA_UNIT_0,
                     ENET_BM_ADD_1SEC_WALL_CLOCK_THR_REG,
                     entry->WallClock_1Sec.reg[0],
                     MEA_MODULE_BM);

     }
      if ((!MEA_Globals_Entry_InitDone)                           ||
          (MEA_OS_memcmp(&(entry->pos_config.polling.val) ,
          &(old_entry->pos_config.polling.val),sizeof(entry->pos_config.polling.val))!=0) ) 
     {

         
         
         MEA_OS_memcpy(&pos_config.polling.val,
                         &entry->pos_config.polling.val
                         ,sizeof(pos_config.polling.val));

         pos_config.polling.val.Rx1--;
         pos_config.polling.val.Rx2--;
         pos_config.polling.val.Tx1--;
         pos_config.polling.val.Tx2--;
         pos_config.polling.val.Tx3--;
         pos_config.polling.val.Tx4--;

         
         MEA_API_WriteReg(MEA_UNIT_0,
                     ENET_IF_PORT_UTOPIA_POLLING_ADDR_REG,
                     pos_config.polling.reg[0],
                     MEA_MODULE_IF);

     }

      if ((!MEA_Globals_Entry_InitDone)                           ||
          ((MEA_OS_memcmp(&(entry->utopia_threshold_reg.val) ,
          &(old_entry->utopia_threshold_reg.val),
          sizeof(entry->utopia_threshold_reg.val) )    )!=0) ) 
      {
          if(MEA_UTOPIA_SUPPORT){
          MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_INTERNAL_POS_INGRESS_THS_REG,entry->utopia_threshold_reg.reg[0],MEA_MODULE_IF);
          }
      }




#if defined(MEA_OS_LINUX) && defined(MEA_PLAT_EVALUATION_BOARD)
#ifdef MEA_ENV_IPC
if ((!MEA_Globals_Entry_InitDone)                           ||
    (entry->Globals_eth0_Device.my_Ip != old_entry->Globals_eth0_Device.my_Ip)
    ){

//    IPCLOCK_Set_IP(entry->Globals_eth0_Device.my_Ip);
}
#endif
#endif
     
    if(MEA_SERDES_RESET_SUPPORT){
      
        MEA_API_WriteReg(MEA_UNIT_0,
            ENET_IF_SERDES_POWER_GROUP_REG,
            entry->power_saving.reg[0],
            MEA_MODULE_IF);

    }

    /* Update control reg only if change or in init process */
 

    
        if ((!MEA_Globals_Entry_InitDone) ||
            (entry->if_fwd_hash.reg[0] != old_entry->if_fwd_hash.reg[0]))
        {
            MEA_API_WriteReg(unit_i, ENET_IF_FWD_HASH_REG, entry->if_fwd_hash.reg[0], MEA_MODULE_IF);
        }



        if ((!MEA_Globals_Entry_InitDone)                           ||
                (entry->if_HDC_global.reg[0] != old_entry->if_HDC_global.reg[0])   )
        {
                MEA_API_WriteReg(unit_i,ENET_IF_HDC_GLOBAL0_REG,entry->if_HDC_global.reg[0],MEA_MODULE_IF);
        }


    if(MEA_HC_SUPPORT )
    {
        if ((!MEA_Globals_Entry_InitDone)                           ||
            (entry->if_HDC_EhterType.comp_eth != old_entry->if_HDC_EhterType.comp_eth) ||
            (entry->if_HDC_EhterType.learn_eth != old_entry->if_HDC_EhterType.learn_eth) )
        {
           val=0;
            val  = entry->if_HDC_EhterType.comp_eth;
            val |= (entry->if_HDC_EhterType.learn_eth<<16);

            
            MEA_API_WriteReg(unit_i,ENET_IF_HDC_LEARN_COMPRESS_ETHERTYPE,val,MEA_MODULE_IF);
        
        
        }

        if ((!MEA_Globals_Entry_InitDone)                           ||
            (entry->if_HDC_EhterType.Invalidate_eth != old_entry->if_HDC_EhterType.Invalidate_eth) ||
            (entry->if_HDC_EhterType.min_packetsize != old_entry->if_HDC_EhterType.min_packetsize))
        {
            val=0;
            val  = entry->if_HDC_EhterType.Invalidate_eth;
            val |= entry->if_HDC_EhterType.min_packetsize<<16;

                MEA_API_WriteReg(unit_i,ENET_IF_HDC_DEL_ETHERTYPE,val,MEA_MODULE_IF);
        }



    }
    if (MEA_DRV_TS_MEASUREMENT_SUPPORT) {
        MEA_Uint32 count_shift =0;

        if ((!MEA_Globals_Entry_InitDone) ||
            (MEA_OS_memcmp(&entry->ts_measurement_ingress, &old_entry->ts_measurement_ingress, sizeof(entry->ts_measurement_ingress)) != 0)
            )
        {
            arryOf_ts[0] = 0;
            arryOf_ts[1] = 0;
            count_shift = 0;

            MEA_OS_insert_value(0,16,
                (MEA_Uint32)entry->ts_measurement_ingress.L4_dst_port,
                 &arryOf_ts[0]);
            count_shift += 16;

            MEA_OS_insert_value(count_shift, 12,
                (MEA_Uint32)entry->ts_measurement_ingress.Net_Tag1,
                &arryOf_ts[0]);
            count_shift += 12;

            MEA_OS_insert_value(count_shift, 7,
                (MEA_Uint32)entry->ts_measurement_ingress.srcPort,
                &arryOf_ts[0]);
            count_shift += 7;

            MEA_OS_insert_value(count_shift, 5,
                (MEA_Uint32)entry->ts_measurement_ingress.L2_type,
                &arryOf_ts[0]);
            count_shift += 5;

            MEA_OS_insert_value(count_shift, 1,
                (MEA_Uint32)entry->ts_measurement_ingress.protocol_type,
                &arryOf_ts[0]);
            count_shift += 1;

            MEA_OS_insert_value(count_shift, 1,
                (MEA_Uint32)entry->ts_measurement_ingress.Ip_type,
                &arryOf_ts[0]);
            count_shift += 1;


#if 0
            arryOf_ts[0] = (MEA_Uint32)entry->ts_measurement_ingress.L4_dst_port;
            arryOf_ts[0] |= (MEA_Uint32)(entry->ts_measurement_ingress.Net_Tag1 & 0xfff) << 16;
            arryOf_ts[0] |= (MEA_Uint32)(entry->ts_measurement_ingress.srcPort & 0xf) << 28;
            arryOf_ts[1] = (MEA_Uint32)((entry->ts_measurement_ingress.srcPort >> 4) & 0x7);
            arryOf_ts[1] |=  (MEA_Uint32)(entry->ts_measurement_ingress.L2_type & 0x1f) << 3;
            arryOf_ts[1] |= (MEA_Uint32)(entry->ts_measurement_ingress.protocol_type & 0x1) << 4;
            arryOf_ts[1] |= (MEA_Uint32)(entry->ts_measurement_ingress.Ip_type & 0x1) << 5;
#endif           
            mea_Global_TBL_offset_UpdateEntry_pArray(unit_i, MEA_GLOBAl_TBL_REG_S_ing_ts_flow_def, 2, arryOf_ts);


        }

        if ((!MEA_Globals_Entry_InitDone) ||
            (MEA_OS_memcmp(&entry->ts_measurement_egress, &old_entry->ts_measurement_egress, sizeof(entry->ts_measurement_egress)) != 0)
            )
        {

            arryOf_ts[0] = 0;
            arryOf_ts[1] = 0;
			count_shift = 0;
            MEA_OS_insert_value(0, 16,
                (MEA_Uint32)entry->ts_measurement_egress.L4_dst_port,
                &arryOf_ts[0]);
            count_shift += 16;

            MEA_OS_insert_value(count_shift, 12,
                (MEA_Uint32)entry->ts_measurement_egress.Net_Tag1,
                &arryOf_ts[0]);
            count_shift += 12;

            MEA_OS_insert_value(count_shift, 7,
                (MEA_Uint32)entry->ts_measurement_egress.srcPort,
                &arryOf_ts[0]);
            count_shift += 7;

            MEA_OS_insert_value(count_shift, 5,
                (MEA_Uint32)entry->ts_measurement_egress.L2_type,
                &arryOf_ts[0]);
            count_shift += 5;

            MEA_OS_insert_value(count_shift, 1,
                (MEA_Uint32)entry->ts_measurement_egress.protocol_type,
                &arryOf_ts[0]);
            count_shift += 1;

            MEA_OS_insert_value(count_shift, 1,
                (MEA_Uint32)entry->ts_measurement_egress.Ip_type,
                &arryOf_ts[0]);
            count_shift += 1;




#if 0
            arryOf_ts[0] = (MEA_Uint32)entry->ts_measurement_egress.L4_dst_port;
            arryOf_ts[0] |= (MEA_Uint32)(entry->ts_measurement_egress.Net_Tag1 & 0xfff) << 16;
            arryOf_ts[0] |= (MEA_Uint32)(entry->ts_measurement_egress.srcPort & 0xf) << 28;
            arryOf_ts[1] = (MEA_Uint32)((entry->ts_measurement_egress.srcPort >> 4) & 0x7);
            arryOf_ts[1] |= (MEA_Uint32)(entry->ts_measurement_egress.L2_type & 0x1f) << 3;
            arryOf_ts[1] |= (MEA_Uint32)(entry->ts_measurement_egress.protocol_type & 0x1) << 4;
            arryOf_ts[1] |= (MEA_Uint32)(entry->ts_measurement_egress.Ip_type & 0x1) << 5;
#endif
            mea_Global_TBL_offset_UpdateEntryBM_pArry(unit_i, MEA_GLOBAl_TBL_REG_S_EGRESS_ts_flow_def, 2, arryOf_ts);

        }
    }
    if (MEA_HC_CLASSIFIER_SUPPORT){
        if ((!MEA_Globals_Entry_InitDone)                           ||
            (entry->Hc_classifier.reg[0] != old_entry->Hc_classifier.reg[0])   )
        {
            MEA_API_WriteReg(unit_i,ENET_IF_HDC_CLASS_CONF,entry->Hc_classifier.reg[0],MEA_MODULE_IF);
        }

    }

    if ((!MEA_Globals_Entry_InitDone) ||
        (entry->QueueTH_currentPacket != old_entry->QueueTH_currentPacket)||
        (entry->QueueTH_MQS           != old_entry->QueueTH_MQS)
        )
    {
        val = 0;
        val = (entry->QueueTH_currentPacket & 0xff);
        val |= (entry->QueueTH_MQS          & 0xff)<<8;

        if (mea_Global_TBL_offset_UpdateEntryBM_pArry(unit_i, MEA_GLOBAl_TBL_REG_SET_TH_MQS_MCMQS, 1, &val) != MEA_OK) {
            return MEA_ERROR;
        }
        
    }

    

    /* return to caller */
    return MEA_OK;


}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_802_1ag_mac_UpdateHwEntry>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#if 0
static MEA_Status  mea_802_1ag_mac_UpdateHwEntry(MEA_Globals_Entry_dbt* entry)
{
	MEA_Uint32	              val;
 
    val = 0;

    val  = entry->Globals_Device.MAC_802_1ag.command;

    MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0),val,MEA_MODULE_IF);

    return MEA_OK;
}
#endif
/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_Globals_Entry>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#define MEA_Collect_all_counters_enable ENET_Collect_all_counters_enable

MEA_Bool MEA_Collect_all_counters_enable;

#define MEA_portEgress_Link_update      ENET_portEgress_Link_update
MEA_Bool MEA_portEgress_Link_update;

#define MEA_portIngress_Link_update      ENET_portIngress_Link_update
MEA_Bool MEA_portIngress_Link_update;


#define MEA_Bonding_Link_update      ENET_Bonding_Link_update
MEA_Bool MEA_Bonding_Link_update;




MEA_Status mea_drv_Init_Globals_Entry(MEA_Unit_t unit) {


    MEA_Uint32  hwLxversion = 0;
    MEA_Globals_Entry_dbt entry;
    MEA_uint64 temp = 0, clock = 0;
    MEA_Clock_t entry_clock;
    MEA_Uint32 clocks_per_single_update = 8;
    MEA_Uint32 number_of_entities ;


    MEA_MacAddr network_default_mac = MEA_NETWORK_L2CP_BASE_ADDR_DEF_VAL;
    MEA_MacAddr L2TP_Tunnel_default_mac = MEA_GLOBAL_L2TP_TUNNEL_ADDRESS_DEF_VAL;
    MEA_MacAddr cfm_oam_UC_Mac = MEA_GLOBAL_CFM_OAM_UC_MAC_ADDRESS_DEF_VAL;
    MEA_MacAddr cfm_oam_MC_Mac = MEA_GLOBAL_CFM_OAM_MC_MAC_ADDRESS_DEF_VAL;

    /* Init MEA globals */
    if (b_bist_test) {
        return MEA_OK;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize Globals ...sizeof(%ld)\n", sizeof(entry));
    MEA_Globals_Entry_InitDone = MEA_FALSE;


    /* zeros the entry */
    MEA_OS_memset(&entry, 0, sizeof(entry));
    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Globals size %ld...\n",sizeof(entry));

    /* set defaults to all fields */
    if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
        entry.bm_config.val.bcst_drp_lvl = MEA_BC_Q_DROP_LEVEL_DEF_VAL_EPC;
    }
    else {
        if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
            entry.bm_config.val.bcst_drp_lvl = MEA_BC_Q_DROP_LEVEL_DEF_VAL_EPC / 2;
        }
        else
        {
            entry.bm_config.val.bcst_drp_lvl = MEA_BC_Q_DROP_LEVEL_DEF_VAL_128;
        }
    }

    entry.bm_config.val.Cluster_mode = MEA_GLOBAL_CLUSTER_MODE_DEF_VAL;
    entry.bm_config.val.pri_q_mode = MEA_GLOBAL_PRI_Q_MODE_DEF_VAL;
    entry.bm_config.val.wred_shift = MEA_GLOBAL_WRED_SHIFT_DEF_VAL;

    entry.bm_config.val.bm_enable = MEA_FALSE;
    entry.bm_config.val.policer_enable = MEA_GLOBAL_POLICER_ENABLE_DEF_VAL;
    entry.bm_config.val.shaper_port_enable = MEA_GLOBAL_SHAPER_PORT_ENABLE_DEF_VAL;
    entry.bm_config.val.shaper_cluster_enable = MEA_GLOBAL_SHAPER_CLUSTER_ENABLE_DEF_VAL;
    entry.bm_config.val.shaper_priQueue_enable = MEA_GLOBAL_SHAPER_PRIQUEUE_ENABLE_DEF_VAL;

    entry.bm_config.val.bm_config_bmqs = MEA_GLOBAL_BMQS_CONFIG_DEF_VAL;
    //WRED 
    entry.bm_config.val.bm_config_wred = (MEA_WRED_PROFILES_HW_SUPPORT == MEA_TRUE) ? MEA_GLOBAL_WRED_CONFIG_DEF_VAL : MEA_FALSE;
#ifdef   MEA_OS_ADVA_TDM 
    entry.bm_config.val.bm_config_mqs = MEA_TRUE;
#else
    entry.bm_config.val.bm_config_mqs = MEA_GLOBAL_MQS_CONFIG_DEF_VAL;
#endif

    entry.bm_config.val.mc_mqs_enable = MEA_GLOBAL_MC_MQS_CONFIG_DEF_VAL;
    entry.bm_config.val.bm_config_Pol_pri = MEA_GLOBAL_POL_PRI_CONFIG_DEF_VAL;
    /* set default value for CFM_OAM */
    entry.CFM_OAM.valid = MEA_GLOBAL_CFM_OAM_VLID_DEF_VAL;
    entry.CFM_OAM.EtherType = MEA_GLOBAL_CFM_OAM_ETHERTYPE_DEF_VAL;


    entry.Counters_Enable.val.Counters_CCMs = MEA_GLOBAL_COUNTERS_ANALYZER_CCM_DEF_VAL;
    entry.Counters_Enable.val.Counters_LMs = MEA_GLOBAL_COUNTERS_ANALYZER_LM_DEF_VAL;
    entry.Counters_Enable.val.Counters_ANALYZERs = MEA_GLOBAL_COUNTERS_ANALYZER_DEF_VAL;
    entry.Counters_Enable.val.Counters_RMONs = MEA_GLOBAL_COUNTERS_RMON_DEF_VAL;
    entry.Counters_Enable.val.Counters_IngressPorts = MEA_FALSE; //MEA_GLOBAL_COUNTERS_INGRESSPORT_DEF_VAL;
    entry.Counters_Enable.val.Counters_BMs = MEA_FALSE; //MEA_GLOBAL_COUNTERS_BMS_DEF_VAL;        
    entry.Counters_Enable.val.Counters_Queues = MEA_FALSE; //MEA_GLOBAL_COUNTERS_QUEUES_DEF_VAL;
    entry.Counters_Enable.val.Counters_PMs = MEA_GLOBAL_COUNTERS_PMS_DEF_VAL;
    entry.Counters_Enable.val.Counters_PMs_TDM = MEA_GLOBAL_COUNTERS_PMS_TDM_DEF_VAL;
    entry.Counters_Enable.val.Counters_RMONs_TDM = MEA_GLOBAL_COUNTERS_RMONS_TDM_DEF_VAL;
    entry.Counters_Enable.val.eventCes = MEA_GLOBAL_COUNTERS_UTOPIA_RMONS_DEF_VAL;
    entry.Counters_Enable.val.Counters_Bonding = MEA_GLOBAL_COUNTERS_BONDING_DEF_VAL;
    entry.Counters_Enable.val.Counters_forwarder = MEA_GLOBAL_COUNTERS_FORWARDER_DEF_VAL;
    entry.Counters_Enable.val.Counters_Ingress = MEA_FALSE ;//MEA_GLOBAL_COUNTERS_INGRESS_DEF_VAL;
    entry.Counters_Enable.val.Counters_HC_RMON = MEA_GLOBAL_COUNTERS_HC_RMON_DEF_VAL;
    entry.Counters_Enable.val.Counters_ACL = MEA_TRUE;

    entry.Counters_Enable.val.Counters_QUEUE_HANDLER = MEA_FALSE;

    entry.Counters_Enable.val.Counters_Virtual_Rmon = MEA_GLOBAL_COUNTERS_Virtual_Rmon_DEF_VAL;  
    MEA_OS_maccpy(&(entry.CFM_OAM.UC_Mac_Address),
        &(cfm_oam_UC_Mac));
    MEA_OS_maccpy(&(entry.CFM_OAM.MC_Mac_Address),
        &(cfm_oam_MC_Mac));




    entry.Periodic_enable = ENET_FALSE;
    ENET_Collect_all_counters_enable = ENET_FALSE;
    ENET_portEgress_Link_update = ENET_FALSE;
    ENET_portIngress_Link_update = ENET_FALSE;

    ENET_Bonding_Link_update = ENET_FALSE;

    entry.max_used_descriptors_high = MEA_HIGH_LEVEL_MAX_USED_DESCRIPTORS_LIMIT_DEF_VAL;
    entry.max_used_descriptors_low = MEA_LOW_LEVEL_MAX_USED_DESCRIPTORS_LIMIT_DEF_VAL;
    entry.max_used_buffers_high = MEA_HIGH_LEVEL_MAX_USED_BUFFERS_LIMIT_DEF_VAL;
    entry.max_used_buffers_low = MEA_LOW_LEVEL_MAX_USED_BUFFERS_LIMIT_DEF_VAL;

    entry.if_global0.reg = 0;
    entry.if_global0.val.ipg_gmii = MEA_GLOBAL_IPG_GMII_DEF_VAL;
    entry.if_global0.val.ipg_mii = MEA_GLOBAL_IPG_MII_DEF_VAL;
    entry.if_global0.val.srv_external_hash_function = MEA_GLOBAL_DEF_SRV_EXTENAL_HASH;

    entry.if_global0.val.h2dse_lmt_enable = MEA_TRUE;
    entry.if_global0.val.en_HPM = MEA_TRUE;

    if (!MEA_GLOBAL_CPE_BONDING_SUPPORT)
        entry.if_global0.val.semi_back_pressure = MEA_GLOBAL_SEMI_BACK_PRESSURE_DEF_VAL;
    else {
        entry.if_global0.val.semi_back_pressure = 0x1f;
    }
    if (MEA_TDM_SUPPORT) {
        entry.if_global0.val.semi_back_pressure = 0x0B;
    }











    if (MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE) <= 4095) {
        entry.if_global0.val.fwd_act_offset = 0;
    }
    if (MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE) >= 4096 && MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE) <= 8191) {
        entry.if_global0.val.fwd_act_offset = 1;
    }
    if (MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE) >= 8192 && MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE) <= 12287) {
        entry.if_global0.val.fwd_act_offset = 2;
    }
    if (MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE) >= 12288 && MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE) <= 12288 + 4095) {
        entry.if_global0.val.fwd_act_offset = 3;
    }



    entry.if_global0.val.pause_enable = MEA_GLOBAL_PAUSE_ENABLE_DEF_VAL;
    entry.if_global0.val.generator_enable = MEA_GLOBAL_GENERATOR_ENABLE_DEF_VAL;
#if  defined (HW_BOARD_IS_PCI)
    if ((mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_ACTION_TYPE) == MEA_TRUE)  ||
        (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_EDITING_TYPE) == MEA_TRUE) ||
        (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_PM_TYPE) == MEA_TRUE)      ||
        (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_SERVICE_EXTERNAL_TYPE) == MEA_TRUE)
        )
    {
        
        entry.if_global0.val.en_ddr_nic = MEA_TRUE;
    }

#endif


#ifdef MEA_VDSL_S1
    entry.if_global0.val.vdsl_conf     = MEA_TRUE;
#else
     entry.if_global0.val.vdsl_conf     = MEA_FALSE;
#endif
    entry.if_global1.reg = 0;
    entry.if_global1.val.Lxcp_Enable       = MEA_GLOBAL_LXCP_DEF_VAL;
    
    entry.if_global1.val.link_up_mask      = MEA_GLOBAL_LINK_UP_MASK_VAL;
    entry.if_global1.val.forwarder_enable  = MEA_GLOBAL_FORWARDER_ENABLE_DEF_VAL;
    entry.if_global1.val.mstp_enable       = MEA_GLOBAL_PARSER_MSTP_VAL;
    entry.if_global1.val.rstp_enable       = MEA_GLOBAL_PARSER_RSTP_VAL;

	entry.if_global1.val.Dse_Output_Algoritm  = MEA_GLOBAL_FORWARDER_OUTPORT_DEF_VAL; 
	entry.if_global1.val.if_Protection_bit    = MEA_GLOBAL_IF_PROTECT_BIT_DEF_VAL; 
    entry.if_global1.val.close_loop_utopia_master_tx_to_master_rx = MEA_GLOBAL_CLOSE_LOOP_UTOPIA_MASTER_TX_TO_MASTER_RX_DEF_VAL;
    entry.if_global1.val.close_loop_utopia_master_tx_to_slave_tx  = MEA_GLOBAL_CLOSE_LOOP_UTOPIA_MASTER_TX_TO_SLAVE_TX_DEF_VAL;
    entry.if_global1.val.drop_unmatch_pkts      = MEA_GLOBAL_DROP_UNMATCH_PKTS_DEF_VAL;
    entry.if_global1.val.learnPBB_info_Enable   = MEA_FALSE;
	entry.if_global1.val.hash1 = MEA_GLOBAL_HASH1_DEF_VAL;
	entry.if_global1.val.hash2 = MEA_GLOBAL_HASH2_DEF_VAL;
    entry.if_global1.val.Acl_hash = MEA_GLOBAL_ACL_HASH_VAL;
    entry.if_global1.val.wd_disable  = MEA_GLOBAL_WD_DISABLE_DEF_VAL;


    entry.if_global1.val.lag_enable  = MEA_GLOBAL_LAG_ENABLE_DEF_VAL;

    


   /* This parameters will be init later by mea_fwd_tbl.c via MEA_SE_Set_Aging API */
   entry.macAging.val.enable        =  0;
   entry.macAging.val.lastTable     =  0;
   entry.macAging.val.sweepInterval =  0;

   MEA_OS_memset(&(entry.OAM_data),0, sizeof(entry.OAM_data));



   entry.if_sssmii_rx_config.val.rx_memory_segment_size  = MEA_GLOBAL_SSSMII_RX_CONTROL_MEMORY_SEGMENT_SIZE_DEF_VAL;
   entry.if_sssmii_rx_config.val.rx_ready_threshold      = MEA_GLOBAL_SSSMII_RX_CONTROL_READY_THRESHOLD_DEF_VAL;
   entry.if_sssmii_rx_config.val.rx_select_control       = MEA_GLOBAL_SSSMII_RX_CONTROL_SELECT_CONTROL_DEF_VAL;

   entry.if_sssmii_tx_config0.val.speed    = MEA_GLOBAL_SSSMII_TX_CONTROL0_SPEED_DEF_VAL;
   entry.if_sssmii_tx_config0.val.duplex   = MEA_GLOBAL_SSSMII_TX_CONTROL0_DUPLEX_DEF_VAL;
   entry.if_sssmii_tx_config0.val.jabber   = MEA_GLOBAL_SSSMII_TX_CONTROL0_JABBER_DEF_VAL;
   entry.if_sssmii_tx_config0.val.ipg      = MEA_GLOBAL_SSSMII_TX_CONTROL0_IPG_DEF_VAL;
   entry.if_sssmii_tx_config0.val.preamble = MEA_GLOBAL_SSSMII_TX_CONTROL0_PREAMBLE_DEF_VAL;

   entry.if_sssmii_tx_config1.val.tx_link_up             = MEA_GLOBAL_SSSMII_TX_CONTROL1_LINK_UP_DEF_VAL;
   entry.if_sssmii_tx_config1.val.tx_memory_segment_size = MEA_GLOBAL_SSSMII_TX_CONTROL1_MEMORY_SEGMENT_SIZE_DEF_VAL;
   entry.if_sssmii_tx_config1.val.tx_ready_threshold     = MEA_GLOBAL_SSSMII_TX_CONTROL1_READY_THRESHOLD_DEF_VAL;
   entry.if_sssmii_tx_config1.val.tx_transmit_threshold  = MEA_GLOBAL_SSSMII_TX_CONTROL1_TRANSMIT_THRESHOLD_DEF_VAL;

   entry.bm_config0.val.pkt_short_limit = MEA_GLOBAL_PKT_SHORT_LIMIT_DEF_VAL;
   entry.bm_config0.val.pkt_high_limit  = MEA_GLOBAL_PKT_HIGH_LIMIT_DEF_VAL;
   entry.bm_config0.val.chunk_short_limit  = MEA_GLOBAL_CHUNK_SHORT_LIMIT_DEF_VAL;     
   entry.bm_config0.val.chunk_high_limit  = MEA_GLOBAL_CHUNK_HIGH_LIMIT_DEF_VAL;
#if 0 //HW is set th 
   entry.bm_config1.val.mode_best_eff_enable = MEA_GLOBAL_CONF1_BEST_EFFORT_DEF_VAL;
   entry.bm_config1.val.cnfg_bkpq_tresh      = MEA_GLOBAL_CONF1_BKPQ_TRESH_DEF_VAL ;
   entry.bm_config1.val.wfq_deficite_tresh   = MEA_GLOBAL_CONF1_WFQ_DEFICITE_TRESH_DEF_VAL ;
#endif
   entry.bm_config2.val.wred_offset   = MEA_GLOBAL_CONF2_WRED_OFFSET_DEF_VAL ;
   entry.bm_config2.val.target_MTU    = MEA_GLOBAL_CONF2_TARGET_MTU_VAL;
   entry.CCM_Bucket.val.wm    = 10000;
   entry.CCM_Bucket.val.xCir  = 4000;
   entry.reorder_frag_time_out = 1000;


   entry.WBRG_swap_Eth.val.lan_ethertype    =  MEA_GLOBAL_WBRG_LAN_ETH_DEF_VAL ;
   entry.WBRG_swap_Eth.val.access_ethertype =  MEA_GLOBAL_WBRG_ACCESS_ETH_DEF_VAL ;

   
   hwLxversion = MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_VER_LX_VERSION_H_REG,MEA_MODULE_IF);
   MEA_GLOBAL_SYS_Clock = MEA_Platform_GetDeviceSysClk();
   //entry.verInfo.val.sysClk = (MEA_Uint32)MEA_GLOBAL_SYS_Clock;
   {

   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "  DEVICE_SYS_CLK  %03d.%03d Mhz\n", (MEA_Uint32)(((MEA_Uint32)MEA_GLOBAL_SYS_Clock) / 1000000), (MEA_Uint32)(((MEA_Uint32)MEA_GLOBAL_SYS_Clock) % 1000000 )  );
   }
    entry.verInfo.val.dpDdr=
           ((hwLxversion &(MEA_IF_VER_LX_VERSION_H_DPDDR_MASK))
      >> MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_H_DPDDR_MASK));
    
#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION

		entry.verInfo.val.forwarder_Addr_Width = 21;

#else
    entry.verInfo.val.forwarder_Addr_Width=
           ((hwLxversion &(MEA_IF_VER_LX_VERSION_H_FORWARDER_ADDR_WIDTH_MASK))     
           >> MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_H_FORWARDER_ADDR_WIDTH_MASK));
#endif
    entry.verInfo.val.sysType=
           ((hwLxversion &(MEA_IF_VER_LX_VERSION_H_SYSTYPE_MASK))
      >> MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_H_SYSTYPE_MASK));

   
    MEA_OS_maccpy(&(entry.network_L2CP_BaseAddr),
                  &(network_default_mac));
    
    MEA_OS_maccpy(&(entry.L2TP_Tunnel_address),
                  &(L2TP_Tunnel_default_mac));

    clock= (MEA_Uint32)MEA_GLOBAL_SYS_Clock;
	{
		MEA_Uint32 numoftick;
		MEA_Uint32 i;

		numoftick = mea_drv_Get_DeviceInfo_NumOfPolicers(0, 0) * 6;
		temp = ((clock) / 8000); // Default 
		for (i = 0; i < 4; i++) {
			if ((temp) <= numoftick) {
			temp = temp * 2;
			}

		}
	}



     if(temp>MEA_TICKS_FOR_SLOW_SERVICE_MAX_VAL)
       temp=MEA_TICKS_FOR_SLOW_SERVICE_MAX_VAL;

 
     entry.ticks_for_slow_service = (MEA_Uint32)(temp );
    
        
        

   
    

    entry.ticks_for_fast_service = (MEA_Uint32)temp/4;
     
/*IngressFlow policer */    
    

        temp = ((clock) / 8000);
        //temp = 50000; /*Alexw */
    if (temp>MEA_ING_FLOW_TICKS_FOR_SLOW_MAX_VAL)
        temp = MEA_ING_FLOW_TICKS_FOR_SLOW_MAX_VAL;

      entry.ticks_for_slow_IngFlow = (MEA_Uint32)((temp));

    temp = ((clock) / 4000);
    


    if (temp>MEA_ING_FLOW_TICKS_FOR_FAST_MAX_VAL)
        temp = MEA_ING_FLOW_TICKS_FOR_FAST_MAX_VAL;

    entry.ticks_for_fast_IngFlow = (MEA_Uint32)temp;




 /************************************************/  
   
    

#if defined(NEW_SHAPER)	
     entry.shaper_slow_multiplexer_port    = MEA_SHAPER_SLOW_PORT_DEF_VAL;
     entry.shaper_slow_multiplexer_cluster = MEA_SHAPER_SLOW_CLUSTER_DEF_VAL;
     entry.shaper_slow_multiplexer_PriQueue = MEA_SHAPER_SLOW_PRI_QUEUE_DEF_VAL;

     temp = (  (clock) / MEA_SHAPER_CIR_EIR_FAST_GN_PORT);
     if (temp < 0xffff)
         entry.shaper_fast_port = (MEA_Uint32)temp;
     else
         entry.shaper_fast_port= 0xffff;
     
     

     temp = (  (clock) / MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER);
//      MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "-->>>>>  clock =%d \n", clock);
//      MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "-->>>>>  MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER =%d \n", MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER);
//      MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "-->>>>>  temp = %d \n", temp);

     if (temp < 0xffff)
        entry.shaper_fast_cluster = (MEA_Uint32)temp;
     else
         entry.shaper_fast_cluster = 0xffff;
     
     temp = (  (clock) / MEA_SHAPER_CIR_EIR_FAST_GN_PRIQUEUE);
     if (temp < 0xffff)
         entry.shaper_fast_PriQueue = (MEA_Uint32)temp;
     else
         entry.shaper_fast_PriQueue= 0xffff;



    
#else
    entry.shaper_slow_multiplexer_port      = MEA_SHAPER_SLOW_SERVICE_TICKS_DEF_VAL;
     entry.shaper_slow_multiplexer_cluster  = MEA_SHAPER_SLOW_SERVICE_TICKS_DEF_VAL;
     entry.shaper_slow_multiplexer_PriQueue = MEA_SHAPER_SLOW_SERVICE_TICKS_PRIQUEUE_DEF_VAL;

    
     if(!MEA_GLOBAL_SHAPER_XCIR_SUPPORT){
     temp= (8 * (clock) / MEA_SHAPER_CIR_EIR_FAST_GN_PORT);
     entry.shaper_fast_port             = (MEA_Uint32)temp;
     }
     else {
          clocks_per_single_update = 8; 
          number_of_entities = MEA_MAX_PORT_NUMBER_EGRESS+1;   // egress port
         temp = number_of_entities * clocks_per_single_update;
        entry.shaper_fast_port = (MEA_Uint32)temp;
     }


     if (!MEA_GLOBAL_SHAPER_XCIR_SUPPORT) {
     temp= (8 * (clock) / MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER);
     entry.shaper_fast_cluster             = (MEA_Uint32)temp;
     }
     else {
          clocks_per_single_update = 8;
          number_of_entities = (MEA_Platform_Get_MaxNumOfClusters());   // num of hw cluster;
         temp = number_of_entities * clocks_per_single_update;
         entry.shaper_fast_cluster = (MEA_Uint32)temp;

     }

     if (!MEA_GLOBAL_SHAPER_XCIR_SUPPORT) {
     temp= (8 * (clock) / MEA_SHAPER_CIR_EIR_FAST_GN_PRIQUEUE);
     entry.shaper_fast_PriQueue             = (MEA_Uint32)temp;
     }
     else {
         clocks_per_single_update = 8;
         number_of_entities = (MEA_Platform_Get_MaxNumOfClusters()) * 8;   // num of hw cluster;
         temp = number_of_entities * clocks_per_single_update;
         if (temp> 0xffff)
             temp=0xFFFF; //only 16bit in hw

         entry.shaper_fast_PriQueue = (MEA_Uint32)temp;
        }

#endif

     entry.PacketAnalyzer.val.enable         = MEA_GLOBAL_ANALYZER_ENABLE_DEF_VAL;
     entry.PacketAnalyzer.val.EtherType      = MEA_GLOBAL_ANALYZER_ETHERTYPE_DEF_VAL;
     entry.PacketAnalyzer.val.port           = MEA_GLOBAL_ANALYZER_PORT_DEF_VAL;
     /**/
     entry.stp_mode.val.mode                 = MEA_GLOBAL_STP_MODE_DEF_VAL;
     entry.stp_mode.val.default_state        = MEA_GLOBAL_STP_MODE_STATE_DEF_VAL;

     entry.WallClock_Calibrate.val.clock_Add_sub = 1;
     entry.WallClock_Calibrate.val.clock_val = 0;
     entry.WallClock_Calibrate.val.clock_grn = MEA_GLOBAL_WALLCLOCK_CALIBRATE_DEF_VAL ;

    
     entry.WallClock_1Sec.val.value = MEA_GLOBAL_WALLCLOCK_1SEC_DEF_VAL;

    entry.set_reorder_timeout.val.pck_reorder_timeout = 4276;   //  microSec
    entry.set_reorder_timeout.val.age_reorder_timeout = 384; //  microSec

    entry.ticks_for_afdx=(MEA_Uint32) (((clock )/1818)/1024);   //0.55msec (64 id number of hw ticks clock)


#if defined(MEA_VDSL_S1)
    entry.pos_config.polling.val.Rx1 = 12;
    entry.pos_config.polling.val.Rx2 = 12;
    entry.pos_config.polling.val.Tx1 = 12;
    entry.pos_config.polling.val.Tx2 = 12;
    entry.pos_config.polling.val.Tx3 = 1;
    entry.pos_config.polling.val.Tx4 = 1;
#else
   if(MEA_GLOBAL_DUAL_LATENCY_SUPPORT){
    entry.pos_config.polling.val.Rx1 = 56;
    entry.pos_config.polling.val.Rx2 = 56;
    entry.pos_config.polling.val.Tx1 = 12;
    entry.pos_config.polling.val.Tx2 = 12;
    entry.pos_config.polling.val.Tx3 = 12;
    entry.pos_config.polling.val.Tx4 = 12;
   } else {
     entry.pos_config.polling.val.Rx1 = 24;
     entry.pos_config.polling.val.Rx2 = 24;
     entry.pos_config.polling.val.Tx1 = 12;
     entry.pos_config.polling.val.Tx2 = 12;
     entry.pos_config.polling.val.Tx3 = 12;
     entry.pos_config.polling.val.Tx4 = 12;
   }
#endif

entry.utopia_threshold_reg.val.data_threshold=218;         
entry.utopia_threshold_reg.val.EOC_threshold=14;          
entry.utopia_threshold_reg.val.Ingress_FIFO_auto_flush=MEA_TRUE;
entry.utopia_threshold_reg.val.pad=0;                    
entry.utopia_threshold_reg.val.rx_parity_check=0;        
entry.utopia_threshold_reg.val.tx_parity_check=0;        



    entry.CCM_Tick.val.fastTick =(MEA_Uint32) (clock /10000); /*each 100 micro-*/
    
    entry.CCM_Tick.val.slowTick =  1200;


//    entry.fragment.val.chunk_size = MEA_GLOBAL_FRAGMENT_CHUNK_DEF_VAL;

    entry.if_configure_1.val.thresh_frag_art_eop = MEA_GLOBAL_FRAG_ART_EOP_SIZE_DEF_VAL;
    entry.Globals_eth0_Device.my_Ip = MEA_OS_inet_addr(MEA_GLOBAL_ETH0_IP_DEF_VAL);

    
   

 

    if(MEA_SERDES_RESET_SUPPORT){
      entry.power_saving.reg[0]=0;
      //This is Ethernity only
      entry.mea_eth24_sfp_reg=0;
      
    }

    entry.forwarder.disable_fwd_del_on_reinit = MEA_TRUE;
    
    entry.Hc_classifier.val.hash_func1 = 3 ;
    entry.Hc_classifier.val.hash_func2 = 0;
    entry.Hc_classifier.val.max_class_use_hcid = MEA_HC_CLASSIFIER_MAX_OF_HCID;
    entry.Hc_classifier.val.init_flush=0;

     entry.if_1p1_down_up_count.to_down = 0;
     entry.if_1p1_down_up_count.to_Up   = 0;

     entry.if_HDC_global.val.set1588ptp_val= 0xB;  /*Default as HW */
     entry.if_HDC_global.val.Comp_bypass = MEA_TRUE;
     entry.if_HDC_global.val.Comp_bypass_aligner   = MEA_TRUE;
     entry.if_HDC_global.val.Decomp_bypass         = MEA_TRUE;
     entry.if_HDC_global.val.Decomp_bypass_aligner  = MEA_TRUE;
     entry.HC_Offset_Mode = MEA_TRUE;
     /* need ti check if we have port with G999 and we support fragrant scheduler  */
     
     if ((MEA_HC_SUPPORT || MEA_PTP1588_SUPPORT) || MEA_FRAGMENT_ENABLE    ){
         entry.if_HDC_global.val.Comp_bypass_aligner = MEA_FALSE;

     }
   




     entry.if_fwd_hash.val.key0_hashO = 2;
     entry.if_fwd_hash.val.key0_hash1 = 3;
     entry.if_fwd_hash.val.key1_hashO = 2;
     entry.if_fwd_hash.val.key1_hash1 = 3;
     entry.if_fwd_hash.val.key2_hashO = 2;
     entry.if_fwd_hash.val.key2_hash1 = 3;
     entry.if_fwd_hash.val.key3_hashO = 0;
     entry.if_fwd_hash.val.key3_hash1 = 0;
     entry.if_fwd_hash.val.key4_hashO = 3;
     entry.if_fwd_hash.val.key4_hash1 = 3;
     entry.if_fwd_hash.val.key5_hashO = 1;
     entry.if_fwd_hash.val.key5_hash1 = 2;
     entry.if_fwd_hash.val.key6_hashO = 1;
     entry.if_fwd_hash.val.key6_hash1 = 2;
     entry.if_fwd_hash.val.key7_hashO = 1;
     entry.if_fwd_hash.val.key7_hash1 = 2;


    /*
    Learn e-type F000
    Compress e-type E000
    Invalidate e-type D000
    */
     
     entry.if_HDC_EhterType.comp_eth  =  0xf000;
     entry.if_HDC_EhterType.learn_eth = 0xe000;
     entry.if_HDC_EhterType.Invalidate_eth   = 0xd000;
     entry.if_HDC_EhterType.min_packetsize = 64;
     entry.bm_sw_global_parmamter.en_calc_rate = MEA_TRUE;
     entry.if_sw_global_parmamter.en_Rmon_calc_rate = MEA_TRUE;

     if (MEA_device_environment_info.MEA_DEVICE_ITHI_enable){
         entry.iTHi = MEA_device_environment_info.MEA_DEVICE_ITHI;
     }



     entry.if_External_srv.val.if_External_srv_mode = 1;
     entry.if_External_srv.val.if_External_srv_shaper_clk = 8;

     entry.Queue_reset_sec = 10;
     entry.QueueTH_MQS = 5;
     entry.QueueTH_currentPacket = 10;

     entry.bm_watchdog_parm.val.enable = MEA_TRUE;
     entry.bm_watchdog_parm.val.wd_threshold = MEA_BM_WD_THRESHOLDE_DEF_VAL_LOW;
     
     entry.check_srv_key = MEA_TRUE;
    /* update the entry (include the hardware) */
    if (MEA_API_Set_Globals_Entry (unit,&entry) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Set_Globals_Entry failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

   



    entry_clock.sec  = MEA_GLOBAL_WALLCLOCK_DATE_DEF_VAL;
    entry_clock.nsec = 0;

    MEA_API_Globals_WallCLock_Set(MEA_UNIT_0 ,entry_clock);

    if (MEA_device_environment_info.MEA_ROM_ENABLE) {
        MEA_DRV_ROM_VALUE 
    }
    
    /* init done */
    MEA_Globals_Entry_InitDone = MEA_TRUE;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize Globals ..InitDone \n");



    /* return to defaults */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_Globals_Entry>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_Globals_Entry(MEA_Unit_t unit_i) 
{

    MEA_Bool temp;
    MEA_Uint32 i;

   

    /* Disable BM */
    MEA_Globals_Entry.bm_config.val.bm_enable = MEA_FALSE;

    /* Update Hardware */
    temp = MEA_Globals_Entry_InitDone;
    MEA_Globals_Entry_InitDone = MEA_FALSE; /* Force write to hw */
    if (mea_Globals_UpdateHw(unit_i,&MEA_Globals_Entry, NULL) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_Globals_UpdateHw failed\n",
                         __FUNCTION__);
       MEA_Globals_Entry_InitDone = temp;
       return MEA_ERROR;
    }
    MEA_Globals_Entry_InitDone = temp;

    /* Update Global OAM CAM Table */
	for (i = 0; i < MEA_CAM_TBL_MAX_ENTRIES; i++)
	{
        if (!MEA_CAM_Table[i].valid) {
            continue;
        }

        if (mea_CAM_UpdateHw(i,
                             &(MEA_CAM_Table[i]),
                             NULL /* Old entry - force write to Hw */
                             ) != MEA_OK) {
    		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	          		          "%s - mea_CAM_UpdateHw failed (i=%d)\n",
						      __FUNCTION__,i);
			return MEA_ERROR;
		}
	}


    /* Update L2CP Base Address */
    if (mea_drv_Set_NetworkL2CP_BaseAddr (unit_i,
                                          &(MEA_Globals_Entry.network_L2CP_BaseAddr),
                                          NULL /* old_entry - force write to hw */
                                          ) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Set_NetworkL2CP_BaseAddr failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    } 
    if (MEA_device_environment_info.MEA_ROM_ENABLE) {
      MEA_DRV_ROM_VALUE  
    }

    return MEA_OK;
}

MEA_Uint32 mea_drv_get_if_global1(MEA_Unit_t  unit)
{

    return MEA_Globals_Entry.if_global1.reg;
}

MEA_Uint32 mea_drv_get_SRV_E_hash(MEA_Unit_t  unit)
{

    return MEA_Globals_Entry.if_global0.val.srv_external_hash_function;
}
MEA_Uint32 mea_drv_get_SRV_E_srv_mode(MEA_Unit_t  unit) 
{

    return MEA_Globals_Entry.if_External_srv.val.if_External_srv_mode;

}



MEA_Status mea_drv_set_if_global1(MEA_Unit_t  unit,MEA_Uint32 val)
{
   MEA_Globals_Entry.if_global1.reg= val;


    return MEA_OK;
}

MEA_Uint32 mea_grv_get_if_FWD_address(MEA_Unit_t  unit)
{

    return MEA_Globals_Entry.verInfo.val.forwarder_Addr_Width;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_Globals_Entry>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_API_Set_Globals_Entry (MEA_Unit_t               unit,
                                      MEA_Globals_Entry_dbt*   entry) 
{
	MEA_CAM_Entry_dbt cam_entry;
    MEA_Uint32 i, oam_id;
    MEA_MacAddr   CFM_OAM_MC_mac_default = MEA_GLOBAL_CFM_OAM_MC_MAC_ADDRESS_DEF_VAL;

	MEA_API_LOG
  if (!b_bist_test){
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid entry parameter */
    if (entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry==NULL \n",__FUNCTION__);
       return MEA_ERROR;
    }

   
//check parameter 
         if((entry->pos_config.polling.val.Rx1 == 0) ||
            (entry->pos_config.polling.val.Rx2 == 0) ||
            (entry->pos_config.polling.val.Tx1 == 0) ||
            (entry->pos_config.polling.val.Tx2 == 0) ||
            (entry->pos_config.polling.val.Tx3 == 0) ||
            (entry->pos_config.polling.val.Tx4 == 0) ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -On of pos_config_polling Tx/Rx is zero the minimum is 1 \n",
                         __FUNCTION__);
                //return MEA_ERROR; 
         
         
         }


        if (entry->if_sssmii_tx_config0.val.speed != 1)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - if_sssmii_tx_config0.val.speed can only be 1 (100Mbit)\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        if (entry->if_sssmii_tx_config0.val.duplex != 1)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - if_sssmii_tx_config0.val.duplex can only be 1 (Full)\n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry->bm_config0.val.pkt_high_limit > MEA_QUEUE_MTU_MAX_VALUE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - pkt_high_limit(%d) cannot be bigger than %d\n",
                __FUNCTION__, entry->bm_config0.val.pkt_high_limit, MEA_QUEUE_MTU_MAX_VALUE);
            return MEA_ERROR;
        }

        if (entry->bm_config0.val.pkt_high_limit < entry->bm_config0.val.pkt_short_limit)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - pkt_high_limit(%d) cannot be lower than pkt_short_limit %d\n",
                __FUNCTION__, entry->bm_config0.val.pkt_high_limit, entry->bm_config0.val.pkt_short_limit);
            return MEA_ERROR;
        }

        if (entry->bm_config0.val.pkt_high_limit < entry->bm_config0.val.pkt_short_limit)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - pkt_high_limit(%d) cannot be lower than pkt_short_limit %d\n",
                __FUNCTION__, entry->bm_config0.val.pkt_high_limit, entry->bm_config0.val.pkt_short_limit);
            return MEA_ERROR;
        }

        if (entry->bm_config0.val.chunk_high_limit < entry->bm_config0.val.chunk_short_limit)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - chunk_high_limit(%d) cannot be lower than chunk_short_limit %d\n",
                __FUNCTION__, entry->bm_config0.val.chunk_high_limit, entry->bm_config0.val.chunk_short_limit);
            return MEA_ERROR;
        }
        if ((entry->shaper_fast_port < MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MIN_VAL) /*||
             (entry->shaper_fast_port > MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MAX_VAL)*/)

        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Shaper PORT fast(%d) is out of range[%d..%d]\n",
                __FUNCTION__, entry->shaper_fast_port, MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MIN_VAL,
                MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MAX_VAL);
            return MEA_ERROR;
        }
        if ((entry->shaper_slow_multiplexer_port < MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MIN_VAL) ||
            (entry->shaper_slow_multiplexer_port > MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MAX_VAL))

        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Shaper slow(%d) is out of range[%d..%d]\n",
                __FUNCTION__, entry->shaper_slow_multiplexer_port, MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MIN_VAL,
                MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MAX_VAL);
            return MEA_ERROR;
        }

        if ((entry->shaper_fast_cluster < MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MIN_VAL) /*||
             (entry->shaper_fast_cluster > MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MAX_VAL)*/)

        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Shaper PORT fast(%d) is out of range[%d..%d]\n",
                __FUNCTION__, entry->shaper_fast_cluster, MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MIN_VAL,
                MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MAX_VAL);
            return MEA_ERROR;
        }
        if ((entry->shaper_slow_multiplexer_cluster < MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MIN_VAL) ||
            (entry->shaper_slow_multiplexer_cluster > MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MAX_VAL))

        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Shaper slow(%d) is out of range[%d..%d]\n",
                __FUNCTION__, entry->shaper_slow_multiplexer_cluster, MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MIN_VAL,
                MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MAX_VAL);
            return MEA_ERROR;
        }

        if ((entry->shaper_fast_PriQueue >(MEA_Uint32) MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MAX_VAL))

        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Shaper PORT fast(%d) is out of range[%d..%d]\n",
                __FUNCTION__, entry->shaper_fast_cluster, MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MIN_VAL,
                MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MAX_VAL);
            return MEA_ERROR;
        }
        if ((entry->shaper_slow_multiplexer_PriQueue < MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MIN_VAL) ||
            (entry->shaper_slow_multiplexer_PriQueue > MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MAX_VAL))

        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Shaper slow(%d) is out of range[%d..%d]\n",
                __FUNCTION__, entry->shaper_slow_multiplexer_PriQueue, MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MIN_VAL,
                MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MAX_VAL);
            return MEA_ERROR;
        }



        

        if ((MEA_Global_ClusterWfq_te)entry->bm_config.val.Cluster_mode != MEA_GLOBAL_CLUSTER_MODE_DEF_VAL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Not allowed to change cluster mode \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if ((MEA_Global_PriQWfq_te)entry->bm_config.val.pri_q_mode != MEA_GLOBAL_PRI_Q_MODE_DEF_VAL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Not allowed to change pri q mode \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

           

  

        if (MEA_OS_maccmp(&(entry->CFM_OAM.MC_Mac_Address),
            &(CFM_OAM_MC_mac_default)) != 0) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - not allowed to change CFM OAM Multicast mac address \n",
                __FUNCTION__);
            return MEA_ERROR;
        }


        if ((entry->if_global0.val.pause_enable) &&
            (!MEA_PLATFORM_IS_PAUSE_SUPPORT)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - pause is not support by the device \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if ((!MEA_PACKETGEN_SUPPORT) &&
            (entry->if_global0.val.generator_enable == MEA_TRUE)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - PacketGen is not support by the device \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        if ((!MEA_PACKET_ANALYZER_TYPE1_SUPPORT) &&
            (entry->PacketAnalyzer.val.enable == MEA_TRUE)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Packet Analyzer is not support by the device \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if ((!MEA_GLOBAL_SHAPER_PORT_ENABLE_DEF_VAL) &&
            (entry->bm_config.val.shaper_port_enable == MEA_TRUE)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Shaper per Port is not support by the device \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if ((!MEA_GLOBAL_SHAPER_CLUSTER_ENABLE_DEF_VAL) &&
            (entry->bm_config.val.shaper_cluster_enable == MEA_TRUE)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Shaper per Cluster is not support by the device \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        if ((!MEA_GLOBAL_SHAPER_PRIQUEUE_ENABLE_DEF_VAL) &&
            (entry->bm_config.val.shaper_priQueue_enable == MEA_TRUE)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Shaper per priQueue is not support by the device \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry->stp_mode.val.mode >= MEA_GLOBAL_STP_MODE_LAST) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  stp_mode.mod value (%d)failed \n",
                __FUNCTION__, entry->stp_mode);
            return MEA_ERROR;

        }
        if (entry->stp_mode.val.default_state >= MEA_PORT_STATE_LAST) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  stp_mode.state value (%d) failed \n",
                __FUNCTION__, entry->stp_mode.val.default_state);
            return MEA_ERROR;
        }

        if (entry->if_mirror_ces_port.val.enable == MEA_TRUE) {
            if (entry->if_mirror_ces_port.val.port_ces_type == 0) {
                //check if the port is valid
				
                if (MEA_API_Get_IsPortValid((MEA_Port_t)entry->if_mirror_ces_port.val.port_cesId, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE/*silent*/) == MEA_FALSE) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  mirror error port %d is not valid\n",
                        __FUNCTION__, entry->if_mirror_ces_port.val.port_cesId);
                    return MEA_ERROR;
                }
            }
            if (entry->if_mirror_ces_port.val.port_ces_type == 1) {
                //check if the cesid is valid
                if (entry->if_mirror_ces_port.val.port_ces_type == 1 && MEA_TDM_SUPPORT == MEA_FALSE) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  mirror error set cesId while TDM not support\n",
                        __FUNCTION__, entry->if_mirror_ces_port.val.port_cesId);
                    return MEA_ERROR;
                }
                if (entry->if_mirror_ces_port.val.port_ces_type == 1 && MEA_TDM_SUPPORT == MEA_TRUE && entry->if_mirror_ces_port.val.port_cesId > MEA_TDM_NUM_OF_CES) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  mirror error set cesId %d out of range\n",
                        __FUNCTION__, entry->if_mirror_ces_port.val.port_cesId);
                    return MEA_ERROR;

                }
				
                if (MEA_API_Get_IsPortValid((MEA_Port_t)entry->if_mirror_ces_port.val.dest_src_port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE/*silent*/) == MEA_FALSE) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  mirror to designate port %d is not valid\n",
                        __FUNCTION__, entry->if_mirror_ces_port.val.dest_src_port);
                    return MEA_ERROR;
                }

            }

        }

        if (entry->bm_config.val.bcst_drp_lvl >= MEA_MAX_MC_POINT_TWO_MULIPOINT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                " - entry->bcst_drp_lvl= %d > MAX %d \n", entry->bm_config.val.bcst_drp_lvl, MEA_MAX_MC_POINT_TWO_MULIPOINT);
            return MEA_ERROR;

        }

        


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
        }//if (b_bist_test)

        MEA_Collect_all_counters_enable = (MEA_Bool)entry->Periodic_enable;
        MEA_portEgress_Link_update = (MEA_Bool)entry->portEgress_Link_update;
        MEA_portIngress_Link_update = (MEA_Bool)entry->portIngress_Link_update;

        MEA_Bonding_Link_update = (MEA_Bool)entry->portBondingLink_update;

        
      

        mea_global_QueueFull_Handler_reset_sec=entry->Queue_reset_sec ;
        mea_global_QueueFull_Handler_TH_MQS=entry->QueueTH_MQS ;
        mea_global_QueueFull_Handler_TH_currentPacket=entry->QueueTH_currentPacket;


    if (mea_Globals_UpdateHw(unit,entry,&MEA_Globals_Entry) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_Globals_UpdateHw failed\n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

	/* clear current Global CAM entries */
	for (i = 0; i < MEA_CAM_TBL_MAX_ENTRIES; i++)
	{
		if ( MEA_Globals_Entry.OAM_data[i].valid )
		{
			if ( mea_drv_delete_CAM_entry(MEA_Globals_Entry.OAM_data[i].oam_id) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						         "%s - mea_drv_delete_CAM_entry failed\n",
								 __FUNCTION__);
				return MEA_ERROR;
			}
		}
	}

	/* Enter the new CAMs */
	for (i = 0; i < MEA_CAM_TBL_MAX_ENTRIES; i++)
	{
		if ( entry->OAM_data[i].valid )
		{
			MEA_OS_memset(&cam_entry,0,sizeof(MEA_CAM_Entry_dbt));
			cam_entry.valid = MEA_TRUE;
			cam_entry.ethertype = entry->OAM_data[i].ethertype;
			cam_entry.l2cp = entry->OAM_data[i].l2cp;
			if ( mea_drv_CAM_Add_Entry(&cam_entry,
				                       &oam_id) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						         "%s - mea_drv_CAM_Add_Entry failed\n",
								 __FUNCTION__);
				return MEA_ERROR;
			}
			entry->OAM_data[i].oam_id = oam_id;
		}
	}

    if (mea_drv_Set_NetworkL2CP_BaseAddr (unit,
                                          &(entry->network_L2CP_BaseAddr),
                                          &(MEA_Globals_Entry.network_L2CP_BaseAddr)) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Set_NetworkL2CP_BaseAddr failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    } 
    
   mea_PM_en_calc_rate = entry->bm_sw_global_parmamter.en_calc_rate;
 
   mea_RMON_en_calc_rate = entry->if_sw_global_parmamter.en_Rmon_calc_rate;


    /* update the SW local database  */
    MEA_OS_memcpy(&MEA_Globals_Entry,entry,sizeof(MEA_Globals_Entry));
   
    /* return to caller */
    return MEA_OK;


}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Globals_Entry>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Globals_Entry (MEA_Unit_t                  unit,
                                      MEA_Globals_Entry_dbt*      entry)
{

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid entry parameter */
    if (entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry==NULL \n",__FUNCTION__);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    MEA_Globals_Entry.Periodic_enable=MEA_Collect_all_counters_enable;
    MEA_Globals_Entry.portEgress_Link_update=MEA_portEgress_Link_update;
    
    /* copy the local entry SW shadow to the output parameter */
    MEA_OS_memcpy(entry,&MEA_Globals_Entry,sizeof(*entry));
   
    /* return to caller */
    return MEA_OK;
}



MEA_Bool MEA_API_Get_Globals_IS_best_eff_enable(MEA_Unit_t                  unit)
{
    MEA_API_LOG
        
       return (MEA_Globals_Entry.bm_config.val.mode_best_eff_enable == 1) ? MEA_TRUE : MEA_FALSE;
}

MEA_Bool MEA_API_Get_Globals_IS_NIC_DDR_enable(MEA_Unit_t                  unit)
{
    MEA_API_LOG

        return (MEA_Globals_Entry.if_global0.val.en_ddr_nic == 1) ? MEA_TRUE : MEA_FALSE;
}


MEA_Status mea_drv_global_set_if_protection_bit (MEA_Bool val) 
{
    MEA_Globals_Entry.if_global1.val.if_Protection_bit = val;
    MEA_API_WriteReg(MEA_UNIT_0,
        ENET_IF_GLOBAL1_REG,
                     MEA_Globals_Entry.if_global1.reg,
                     MEA_MODULE_IF);

    return MEA_OK;
}


MEA_Status MEA_API_Globals_WallCLock_Set(MEA_Unit_t    unit ,MEA_Clock_t entry)
{
    
    // write only
    MEA_API_WriteReg(MEA_UNIT_0,
                     MEA_BM_GLOBAL_WALL_CLOCK_HI_REG,
                     entry.sec,
                     MEA_MODULE_BM);

    MEA_API_WriteReg(MEA_UNIT_0,
                     MEA_BM_GLOBAL_WALL_CLOCK_LO_REG,
                     entry.nsec,
                     MEA_MODULE_BM);

return MEA_OK;
}

MEA_Status MEA_API_Globals_WallCLock_Get(MEA_Unit_t unit ,MEA_Clock_t *entry)
{
#if 0
   entry->nsec =MEA_API_ReadReg(MEA_UNIT_0,MEA_BM_GLOBAL_WALL_CLOCK_LO_REG,MEA_MODULE_BM);
   entry->sec  =MEA_API_ReadReg(MEA_UNIT_0,MEA_BM_GLOBAL_WALL_CLOCK_HI_REG,MEA_MODULE_BM);
#else
    //need to read other register
	MEA_API_WriteReg(MEA_UNIT_0,
                     MEA_BM_WALL_CLOCK_HI_READ_REG,
                     0x1,
                     MEA_MODULE_BM);
    
    entry->sec  =MEA_API_ReadReg(MEA_UNIT_0,MEA_BM_WALL_CLOCK_HI_READ_REG,MEA_MODULE_BM);
    entry->nsec =MEA_API_ReadReg(MEA_UNIT_0,MEA_BM_WALL_CLOCK_LO_READ_REG,MEA_MODULE_BM);
    

#endif
   
   return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status mea_Global_Interface_G999_1_UpdateHw(MEA_Unit_t unit_i,
                                       MEA_Global_Interface_G999_1_dbt *entry,
                                       MEA_Global_Interface_G999_1_dbt *old_entry)
{
    
    
    if(!MEA_FRAGMENT_9991_SUPPORT){
        return MEA_OK;
    }
    
        /* Update control reg only if change or in init process */
        if ((!MEA_Global_Interface_G999_1_Entry_InitDone)                           ||
            (entry->if_G999_1_th_config.reg != old_entry->if_G999_1_th_config.reg)   )
        {
            MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_G999_1_TH_REG ,entry->if_G999_1_th_config.reg,MEA_MODULE_IF);

        }


    
    /* return to caller */
    return MEA_OK;


}




MEA_Status mea_drv_Init_Global_Interface_G999_1_Entry(MEA_Unit_t unit)
{
    MEA_Global_Interface_G999_1_dbt entry;
    
    if(b_bist_test){
        return MEA_OK;
    }
    if(MEA_Global_Interface_G999_1_Entry_InitDone==MEA_TRUE){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"The  Interface_G999_1 All ready Initialize...\n");
      return MEA_ERROR;
    }




    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Interface_G999_1 ...\n");
    MEA_Global_Interface_G999_1_Entry_InitDone = MEA_FALSE;
  
    // clear the global
    MEA_OS_memset(&MEA_Global_G999_1_Entry,0,sizeof(entry)); 

    MEA_OS_memset(&entry,0,sizeof(entry));

   entry.if_G999_1_th_config.val.Fragment_999_pause_enable = MEA_GLOBAL_FRAGMANT_G999_1_PAUSE_ENABLE_DEF_VAL;
   entry.if_G999_1_th_config.val.Fragment_999_threshold    = MEA_GLOBAL_FRAGMANT_G999_1_DEF_TH_VAL;
   entry.if_G999_1_th_config.val.mng_channel = MEA_GLOBAL_FRAGMANT_G999_1_MNG_CHANNEL;
   entry.if_G999_1_th_config.val.packet_9991_threshold = 7;
   
   
    
   entry.if_G999_1_th_config.val.increment_enable = MEA_TRUE;

    
    /* update the entry (include the hardware) */
    if (MEA_API_Set_Global_Interface_G999_1_Entry (unit,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Set_Global_Interface_G999_1_Entry failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }




    /* init done */
    MEA_Global_Interface_G999_1_Entry_InitDone = MEA_TRUE;
    return MEA_OK;
}


MEA_Status mea_drv_ReInit_Global_Interface_G999_1_Entry(MEA_Unit_t unit) 
{

   MEA_Bool temp;
   MEA_Global_Interface_G999_1_dbt old_entry;
   
   MEA_OS_memset(&old_entry,0,sizeof(old_entry));

    /* Update Hardware */
    temp = MEA_Global_Interface_G999_1_Entry_InitDone;
    MEA_Global_Interface_G999_1_Entry_InitDone = MEA_FALSE; /* Force write to hw */
    if (mea_Global_Interface_G999_1_UpdateHw(unit,&MEA_Global_G999_1_Entry,&old_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_Global_Interface_G999_1_UpdateHw failed\n",
            __FUNCTION__);
        MEA_Global_Interface_G999_1_Entry_InitDone = temp;
        return MEA_ERROR;
    }
    MEA_Global_Interface_G999_1_Entry_InitDone = temp;
    
    
    
    
    
   
    
    return MEA_OK;
}





MEA_Status MEA_API_Set_Global_Interface_G999_1_Entry (MEA_Unit_t               unit,
                                                      MEA_Global_Interface_G999_1_dbt*   entry)
{

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check for valid entry parameter */
        if (entry == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry==NULL \n",__FUNCTION__);
            return MEA_ERROR;
        }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */   
    
    
    // check if we have change
    if(MEA_OS_memcmp(&MEA_Global_G999_1_Entry,entry,sizeof(MEA_Global_G999_1_Entry))== 0){
        return MEA_OK;
    }
    
    
    
    if (mea_Global_Interface_G999_1_UpdateHw(unit,entry,&MEA_Global_G999_1_Entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_Global_Interface_G999_1_UpdateHw failed\n",
            __FUNCTION__);
       
        return MEA_ERROR;
    }
    
    /* update the SW local database  */
    MEA_OS_memcpy(&MEA_Global_G999_1_Entry,entry,sizeof(MEA_Global_G999_1_Entry));

    /* return to caller */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Global_Interface_G999_1_Entry>                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Global_Interface_G999_1_Entry(MEA_Unit_t               unit,
                                                     MEA_Global_Interface_G999_1_dbt*   entry)
{

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check for valid entry parameter */
        if (entry == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry==NULL \n",__FUNCTION__);
            return MEA_ERROR;
        }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



        /* copy the local entry SW shadow to the output parameter */
        MEA_OS_memcpy(entry,&MEA_Global_G999_1_Entry,sizeof(*entry));

        /* return to caller */
        return MEA_OK;


}


/************************************************************************/
/*                                                                      */
/************************************************************************/
/* GW  */

static void mea_GW_BM_UpdateEntryHw_MAC(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)

{
    //     MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
    //     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
    //     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
    MEA_MacAddr             *MAC = (MEA_MacAddr*)arg4;

//    MEA_Uint32      count_shift;
      MEA_Uint32 val[2];

    val[0] = 0;
    val[1] = 0;

    val[0] = ((MEA_Uint32)(MAC->b[5]) << 0);
    val[0] |= ((MEA_Uint32)(MAC->b[4]) << 8);
    val[0] |= ((MEA_Uint32)(MAC->b[3]) << 16);
    val[0] |= ((MEA_Uint32)(MAC->b[2]) << 24);


    val[1] = ((MEA_Uint32)(MAC->b[1]) << 0);
    val[1] |= ((MEA_Uint32)(MAC->b[0]) << 8);



    /* write to hardware */
    MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA0, val[0], MEA_MODULE_BM);
    MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA1, val[1], MEA_MODULE_BM);

}

static void      mea_GW_UpdateEntryHw_MAC(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

//     MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
//     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
//     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
    MEA_MacAddr             *MAC = (MEA_MacAddr*)arg4;
    MEA_Uint32 i;

    MEA_Uint32     val[2];
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }

    val[0] = ((MEA_Uint32)(MAC->b[5]) << 0);
    val[0] |= ((MEA_Uint32)(MAC->b[4]) << 8);
    val[0] |= ((MEA_Uint32)(MAC->b[3]) << 16);
    val[0] |= ((MEA_Uint32)(MAC->b[2]) << 24);


    val[1] = ((MEA_Uint32)(MAC->b[1]) << 0);
    val[1] |= ((MEA_Uint32)(MAC->b[0]) << 8);


    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }


}

MEA_Status mea_Global_offsetMAC_UpdateEntry(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_MacAddr new_entry)
{


    MEA_ind_write_t ind_write;




    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_write.tableOffset = offset;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_GW_UpdateEntryHw_MAC;
    ind_write.funcParam1 = (MEA_funcParam)unit;
//     ind_write.funcParam2 = (MEA_Uint32)hwUnit;
//     ind_write.funcParam3 = (MEA_Uint32)hwId;
    ind_write.funcParam4 = (MEA_funcParam)(&new_entry);


    /* Write to the Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

 


    return MEA_OK;

}


MEA_Status mea_Global_offsetMAC_UpdateEntryBM(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_MacAddr new_entry)
{


    MEA_ind_write_t ind_write;




    /************************************************************************/
    /*                                                                      */
    /************************************************************************/

    /* set the ind_write parameters */
    ind_write.tableType = ENET_BM_TBL_TYP_GW_GLOBAL_REGS;
    ind_write.tableOffset = offset;
    ind_write.cmdReg = MEA_BM_IA_CMD;
    ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_write.statusReg = MEA_BM_IA_STAT;
    ind_write.statusMask = MEA_BM_IA_STAT_MASK;
    ind_write.tableAddrReg = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    //     ind_write.funcParam2 = (MEA_Uint32)hwUnit;
    //     ind_write.funcParam3 = (MEA_Uint32)0/*hwId*/;
    ind_write.funcParam4 = (MEA_funcParam)(&new_entry);
    ind_write.writeEntry = (MEA_FUNCPTR)mea_GW_BM_UpdateEntryHw_MAC;

    /* Write To HW Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) == MEA_ERROR) {
        return MEA_ERROR;
    }



    return MEA_OK;

}



static void      mea_GW_RootFilter_UpdateEntryHw_Reg32(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

//     MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
//     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
//     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
    MEA_Uint32    entry = (MEA_Uint32)((long)arg4);
    MEA_Uint32 i;

    MEA_Uint32     val[1];
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }

    val[0] = entry;


    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }


}



MEA_Status mea_Global_TBL_offset_UpdateEntry(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_Uint32 new_entry)
{


    MEA_ind_write_t ind_write;




    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_write.tableOffset = offset;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_GW_RootFilter_UpdateEntryHw_Reg32;
    ind_write.funcParam4 = (MEA_funcParam)((long)new_entry);


    /* Write to the Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }




    return MEA_OK;

}




static void mea_drv_write_UpdateEntryHw_ArryReg32(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)

{
    MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
    //     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
    //     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
    MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32       *data = (MEA_Uint32*)arg4;
    MEA_Uint32 i;




    for (i = 0; i < len; i++) {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            data[i],
            MEA_MODULE_IF);

        // printf("data[%d]= 0x08%x\n",i, data[i]);

    }


}

MEA_Status mea_drv_write_IF_TBL_X_offset_UpdateEntry_pArray(MEA_Unit_t  unit, MEA_Uint32 tableId, MEA_Uint32 offset, MEA_Uint32 numofRegs, MEA_Uint32 *new_entry)
{


    MEA_ind_write_t ind_write;




    /* build the ind_write value */
    ind_write.tableType = tableId;
    ind_write.tableOffset = offset;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_write_UpdateEntryHw_ArryReg32;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
    ind_write.funcParam4 = (MEA_funcParam)(new_entry);


    /* Write to the Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }




    return MEA_OK;

}


static void mea_GW_RootFilter_UpdateEntryHw_ArryReg32(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)

{
    MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
    //     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
    //     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
    MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32       *data = (MEA_Uint32*)arg4;
    MEA_Uint32 i;
    
   

   
    for (i = 0; i < len; i++) {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            data[i],
            MEA_MODULE_IF);

        // printf("data[%d]= 0x08%x\n",i, data[i]);

    }


}





static MEA_Status mea_Global_TBL_offset_UpdateEntry_pArray(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_Uint32 numofRegs, MEA_Uint32 *new_entry)
{


    MEA_ind_write_t ind_write;




    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_write.tableOffset = offset;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_GW_RootFilter_UpdateEntryHw_ArryReg32;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
    ind_write.funcParam4 = (MEA_funcParam)(new_entry);


    /* Write to the Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }




    return MEA_OK;

}



static void mea_GW_global_BM_UpdateEntryHw_Reg32(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)

{
    //     MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
    //     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
    //     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
    MEA_Uint32             entry = (MEA_Uint32)((long)arg4);

    //    MEA_Uint32      count_shift;
    MEA_Uint32 val[1];

    val[0] = entry;





    /* write to hardware */
    MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA0, val[0], MEA_MODULE_BM);


}


MEA_Status mea_Global_TBL_offset_UpdateEntryBM(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_Uint32 new_entry)
{


    MEA_ind_write_t ind_write;




    /* set the ind_write parameters */
    ind_write.tableType = ENET_BM_TBL_TYP_GW_GLOBAL_REGS;
    ind_write.tableOffset = offset;
    ind_write.cmdReg = MEA_BM_IA_CMD;
    ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_write.statusReg = MEA_BM_IA_STAT;
    ind_write.statusMask = MEA_BM_IA_STAT_MASK;
    ind_write.tableAddrReg = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    //     ind_write.funcParam2 = (MEA_Uint32)hwUnit;
    //     ind_write.funcParam3 = (MEA_Uint32)0/*hwId*/;
    ind_write.funcParam4 = (MEA_funcParam)((long)new_entry);
    ind_write.writeEntry = (MEA_FUNCPTR)mea_GW_global_BM_UpdateEntryHw_Reg32;

    /* Write To HW Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) == MEA_ERROR) {
        return MEA_ERROR;
    }




    return MEA_OK;

}



static void mea_GW_global_BM_UpdateEntryHw_arryReg32(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)

{
    MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
    //     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
    //     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
    MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32       *data = (MEA_Uint32*)arg4;
    MEA_Uint32 i;



    /* write to hardware */
    for (i = 0; i < len; i++) {
        MEA_API_WriteReg(unit_i, ENET_BM_IA_WRDATA(i), data[i], MEA_MODULE_BM);
    }

}

static MEA_Status mea_Global_TBL_offset_UpdateEntryBM_pArry(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_Uint32 numofRegs ,MEA_Uint32 *new_entry)
{


    MEA_ind_write_t ind_write;




    /* set the ind_write parameters */
    ind_write.tableType = ENET_BM_TBL_TYP_GW_GLOBAL_REGS;
    ind_write.tableOffset = offset;
    ind_write.cmdReg = MEA_BM_IA_CMD;
    ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_write.statusReg = MEA_BM_IA_STAT;
    ind_write.statusMask = MEA_BM_IA_STAT_MASK;
    ind_write.tableAddrReg = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    //     ind_write.funcParam2 = (MEA_Uint32)hwUnit;
    //     ind_write.funcParam3 = (MEA_Uint32)0/*hwId*/;
    ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
    ind_write.funcParam4 = (MEA_funcParam)(new_entry);
    ind_write.writeEntry = (MEA_FUNCPTR)mea_GW_global_BM_UpdateEntryHw_arryReg32;

    /* Write To HW Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) == MEA_ERROR) {
        return MEA_ERROR;
    }




    return MEA_OK;

}


static void      mea_GW_RootFilter_valid_UpdateEntryHw(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

//     MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
//     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
//     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
    MEA_Uint32    entry = (MEA_Uint32)((long)arg4);
    MEA_Uint32 i;

    MEA_Uint32     val[1];
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }

    val[0] = entry;


    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }


}
MEA_Status mea_GW_RootFilter_valid_UpdateEntry(MEA_Unit_t  unit, MEA_Uint32 new_entry)
{


    MEA_ind_write_t ind_write;

   


    /* build the ind_write value */
    ind_write.tableType = MEA_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_write.tableOffset = 31;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_GW_RootFilter_valid_UpdateEntryHw;
    ind_write.funcParam4 = (MEA_funcParam)((long)new_entry);


    /* Write to the Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }




    return MEA_OK;

}




MEA_Status mea_drv_GW_Update_Global(MEA_Unit_t  unit, MEA_Gw_Global_dbt  *new_entry, MEA_Gw_Global_dbt  *old_entry)
{
    MEA_Uint32 i;
    MEA_Uint32 write_TOIF_value;
    MEA_Uint32 write_value;

    
    MEA_Uint32  valArry[2];

    if ((old_entry) &&
        (MEA_OS_memcmp(new_entry,
        old_entry,
        sizeof(*new_entry)) == 0)) {
        return MEA_OK;
    }

    if ((!old_entry) ||
        (MEA_OS_memcmp(&new_entry->My_Host_MAC,
        &old_entry->My_Host_MAC,
        sizeof(MEA_MacAddr)) != 0)){
        if(MEA_GW_SUPPORT)
           mea_Global_offsetMAC_UpdateEntry(unit, MEA_GLOBAl_TBL_REG_My_Host_MAC, new_entry->My_Host_MAC);
        if (MEA_GW_SUPPORT || MEA_VXLAN_SUPPORT)
           mea_Global_offsetMAC_UpdateEntryBM(unit, MEA_GLOBAl_TBL_REG_My_Host_MAC, new_entry->My_Host_MAC);
    
    }
    if ((!old_entry) ||
        (MEA_OS_memcmp(&new_entry->My_PGW_MAC,
            &old_entry->My_PGW_MAC,
            sizeof(MEA_MacAddr)) != 0)) 
    {
        if (MEA_GW_SUPPORT) {
            for (i = 0; i < 5; i++) {
                mea_Global_offsetMAC_UpdateEntry(unit, MEA_GLOBAl_TBL_REG_My_PGW_MAC, new_entry->My_PGW_MAC);
            }
         }
    }
    
    


    if (MEA_GW_SUPPORT) {
        /*APN_vlanId APN_Ipv6_mask_Type*/
        for (i = 0; i < MEA_GW_MAX_OF_APN_VLAN; i++)
        {
           
            if ((!old_entry) ||
                ((new_entry->APN_vlanId[i] != old_entry->APN_vlanId[i]) ||
                 (new_entry->APN_Ipv6_mask_Type[i] != old_entry->APN_Ipv6_mask_Type[i]) )) {
              
                write_value = 0;
                write_value = new_entry->APN_vlanId[i];
                write_value |= new_entry->APN_Ipv6_mask_Type[i] << 12;

                /*Update HW */
                mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_APN_vlan + i), write_value);
            }
 
        }


        /*APN_Ipv6_mask_Type*/
        for (i = 0; i < MEA_GW_MAX_OF_APN_VLAN; i++)
        {
          
            if ((!old_entry) ||
                (new_entry->APN_IpV6[i].val != old_entry->APN_IpV6[i].val)
                ) {

                valArry[0] = 0;
                valArry[1] = 0;
                valArry[0] = new_entry->APN_IpV6[i].s.lsw;
                valArry[1] = new_entry->APN_IpV6[i].s.msw;
                if (i <= 3)
                    mea_Global_TBL_offset_UpdateEntry_pArray(unit, (MEA_GLOBAl_TBL_REG_EPC_IPV6_0 + i), 2, &valArry[0]);
                else
                    mea_Global_TBL_offset_UpdateEntry_pArray(unit, (MEA_GLOBAl_TBL_REG_EPC_IPV6_4 + (i - 4)), 2, &valArry[0]);


            }
         }

    } //if (MEA_GW_SUPPORT)

    {
        /*Infra_VLAN */
       
        if ((!old_entry) ||
            ((new_entry->Infra_VLAN != old_entry->Infra_VLAN) ||
            (new_entry->Infra_VLAN_Ipv6mask != old_entry->Infra_VLAN_Ipv6mask)))
        {
            
            write_TOIF_value = new_entry->Infra_VLAN & 0xffff;
            write_TOIF_value |= new_entry->Infra_VLAN_Ipv6mask << 12;

            write_value = new_entry->Infra_VLAN;
            /*Update HW */
            if (MEA_GW_SUPPORT)
                mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_Infra_VLAN), write_TOIF_value);
            if (MEA_GW_SUPPORT || MEA_VXLAN_SUPPORT)
                mea_Global_TBL_offset_UpdateEntryBM(unit, (MEA_GLOBAl_TBL_REG_Infra_VLAN), write_value);
                 
        }
 
    }
    
    for (i = 0; i < MEA_GW_MAX_OF_SGW_IP; i++)
    {
        
        if ((!old_entry) || (new_entry->S_GW_IP[i] != old_entry->S_GW_IP[i])){
            
            write_value = new_entry->S_GW_IP[i];
            /*Update HW */
            if (MEA_GW_SUPPORT)
                mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_S_GW_IP0 + i), write_value);
            if (MEA_GW_SUPPORT || MEA_VXLAN_SUPPORT)
                mea_Global_TBL_offset_UpdateEntryBM(unit, (MEA_GLOBAl_TBL_REG_S_GW_IP0 + i), write_value);
        }
    }
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    if (MEA_GW_SUPPORT) {
        for (i = 0; i < MEA_GW_MAX_OF_SGW_IP; i++)
        {
            
            if ((!old_entry) || (new_entry->PDN_Ipv4[i] != old_entry->PDN_Ipv4[i])) {

                write_value = new_entry->PDN_Ipv4[i];
                /*Update HW */
                mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_PDN_IPV4_INDEX_0 + i), write_value);
                mea_Global_TBL_offset_UpdateEntryBM(unit, (MEA_GLOBAl_TBL_REG_BM_PDN_IPV4_INDEX_0 + i), write_value);
            }
            

        }
    }
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/

    if (MEA_GW_SUPPORT) {
        /*P_GW_U_IP */
        
        if ((!old_entry) || (new_entry->P_GW_U_IP != old_entry->P_GW_U_IP)){

            write_value = new_entry->P_GW_U_IP;
            /*Update HW */
            mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_P_GW_U_IP), write_value);
            mea_Global_TBL_offset_UpdateEntryBM(unit, (MEA_GLOBAl_TBL_REG_P_GW_U_IP), write_value);
        }
        
    }

    if (MEA_GW_SUPPORT) {
        /*MGM_vlanId */
      
        if ((!old_entry) || (new_entry->MNG_vlanId != old_entry->MNG_vlanId)){
            
            write_value = new_entry->MNG_vlanId;
            /*Update HW */
            mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_MNG_vlanId), write_value);
        }
      

    }
    if (MEA_GW_SUPPORT) {
        /*Conf_D_IP */
        
        if ((!old_entry) || (new_entry->Conf_D_IP != old_entry->Conf_D_IP)){
            
            write_value = new_entry->Conf_D_IP;
                /*Update HW */
                mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_Conf_D_IP), write_value);
        }
    }

    if (MEA_GW_SUPPORT) {
        /*MGM_vlanId */
        
        if ((!old_entry) || (new_entry->MME_IP != old_entry->MME_IP)){
            
            write_value = new_entry->MME_IP;
			/*Update HW */
            mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_MME_IP), write_value);
        }
    }

    if (MEA_GW_SUPPORT) {
        /*default_sid_info */
        
        if ((!old_entry) || 
            (MEA_OS_memcmp(&new_entry->default_sid_info, &old_entry->default_sid_info, sizeof(MEA_Def_SID_dbt)) !=0) ){

            write_value = (new_entry->default_sid_info.action << 14 | new_entry->default_sid_info.def_sid);
            /*Update HW */
            mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_default_sid_info), write_value);
        }
    }

    if (MEA_GW_SUPPORT) {
        /*default_sid_info */

        if ((!old_entry) ||
            (new_entry->IPCS_default_sid_srcport != old_entry->IPCS_default_sid_srcport) ||
            (MEA_OS_memcmp(&new_entry->default_sid_info, &old_entry->default_sid_info, sizeof(MEA_Def_SID_dbt)) != 0) ) {

            write_value =(new_entry->default_sid_info.def_sid);
            write_value |= (new_entry->default_sid_info.action << 14 );
            write_value |= (new_entry->IPCS_default_sid_srcport << 16);
            /*Update HW */
            mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_IPCS_default_sid_info), write_value);
        }
    }


    if (MEA_GW_SUPPORT) {
        /*edit_IP_TTL */
        
        if ((!old_entry) || (new_entry->edit_IP_TTL != old_entry->edit_IP_TTL)){
            
            write_value = new_entry->edit_IP_TTL;
            /*Update HW */
            mea_Global_TBL_offset_UpdateEntryBM(unit, (MEA_GLOBAl_TBL_BM_REG_edit_IP_TTL), write_value);
        }
    }

    if (MEA_GW_SUPPORT) {
       
        
        if ((!old_entry) || (new_entry->InternalDscp != old_entry->InternalDscp)){
            
            write_value = new_entry->InternalDscp;
            /*Update HW */
            mea_Global_TBL_offset_UpdateEntryBM(unit, (MEA_GLOBAl_TBL_BM_REG_InternalDscp), write_value);
        }
    }

    if (MEA_GW_SUPPORT) {
         /*local_mng_ip */

         if ((!old_entry) || (new_entry->local_mng_ip != old_entry->local_mng_ip)){
             
             //write_value = new_entry->local_mng_ip;
             ///*Update HW */
             // mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_local_mng_ip), write_value);
         }
    
     }

    if (MEA_GW_SUPPORT) {
        /*ETHCS_default_sid_info */

        if ((!old_entry) || 
            
            (MEA_OS_memcmp(&new_entry->ETHCS_UL_default_sid_info,
                &old_entry->ETHCS_UL_default_sid_info,
                sizeof(MEA_Def_SID_dbt)) != 0)) {
            
            write_value  = (new_entry->ETHCS_UL_default_sid_info.def_sid);
            write_value |= (new_entry->ETHCS_UL_default_sid_info.action << 14);
            
            
            /*Update HW */
            mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_default_SID_ETHCS_UL), write_value);
        }

    }




    if (MEA_GW_SUPPORT || MEA_VXLAN_SUPPORT) {
         /*learn_enb_vpn */
         
         if ((!old_entry) || (new_entry->learn_enb_vpn != old_entry->learn_enb_vpn)){
             
             write_value = new_entry->learn_enb_vpn;
             /*Update HW */
             MEA_Uint32 arryRead[10];
             MEA_Uint32 count = 0;
             mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_enB_vpn), write_value);

             MEA_OS_memset(&arryRead, 0, sizeof(arryRead));

             MEA_API_Read_IFIndirect(unit, ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL, MEA_GLOBAl_TBL_REG_enB_vpn, 1, &arryRead[0]);
             while (arryRead[0] != write_value) {
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "learn_enb_vpn %d  !=  Read Val %d\n", write_value, arryRead[0]);
                 mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_enB_vpn), write_value);
                 MEA_API_Read_IFIndirect(unit, ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL, MEA_GLOBAl_TBL_REG_enB_vpn, 1, &arryRead[0]);
                 count++;
                 if (count == 5)
                     break;

             }
         }
     }






    
    if (MEA_VXLAN_SUPPORT) 
    {
        /*default_sid_info */
        
        if ((!old_entry) || 
            ((new_entry->vxlan_L4_dst_port != old_entry->vxlan_L4_dst_port) ||
            (new_entry->vxlan_upto_vlan != old_entry->vxlan_upto_vlan)))
        {
            write_value = new_entry->vxlan_L4_dst_port;
            write_value |= (new_entry->vxlan_upto_vlan & 0xfff) << 16;
            /*Update HW */
            mea_Global_TBL_offset_UpdateEntry(unit, (MEA_GLOBAl_TBL_REG_VXLAN_L4_DEST_port), write_value);

        }

    }



    return MEA_OK;
}



MEA_Status mea_drv_global_GW_init(MEA_Unit_t  unit)
{


    MEA_Uint32 size;
    MEA_Gw_Global_dbt                   entry;
//     if (!MEA_GW_SUPPORT){
//         return MEA_OK;
//     }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize GW  ...\n");
    /* Verify not already init */
    if (MEA_GW_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - already init \n", __FUNCTION__);
        return MEA_ERROR;
    }

    
    /* Allocate PacketGen Table */
    size =  sizeof(MEA_Gw_Global_dbt);
    MEA_OS_memset(&(MEA_GW_Global_Entry), 0, size);
    MEA_OS_memset(&(entry), 0, size);
   
    
    My_rootFilter = 0x0;



    MEA_GW_InitDone = MEA_TRUE;

    entry.edit_IP_TTL = 64;

    entry.learn_enb_vpn = 1;
    
    if (MEA_VXLAN_SUPPORT)
        entry.learn_enb_vpn = 0;



    if (mea_drv_GW_Update_Global(unit, &entry, NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_GW_Update_Global fail \n", __FUNCTION__);
        return MEA_ERROR;
    }

    /* update  database   */
    MEA_OS_memcpy(&MEA_GW_Global_Entry, &entry, sizeof(MEA_Gw_Global_dbt));

   
      


    return MEA_OK;
}


MEA_Status mea_drv_global_GW_Reinit(MEA_Unit_t  unit)
{

    if (b_bist_test)
        return MEA_OK;

   

    if (mea_drv_GW_Update_Global(unit, &MEA_GW_Global_Entry, NULL) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - imea_drv_GW_Update_Global \n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (!MEA_GW_SUPPORT) {
        return MEA_OK;
    }

    /**/
    if (mea_GW_RootFilter_valid_UpdateEntry(unit, My_rootFilter) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index is out of range \n", __FUNCTION__);
        return MEA_ERROR;
    }



    return MEA_OK;
}

MEA_Status mea_drv_global_GW_Conclude(MEA_Unit_t  unit)
{
    
    MEA_GW_InitDone = MEA_FALSE;
 


    return MEA_OK;
}






/* configure the GW  */



/************************************************************************/
/* GW API                                                               */
/************************************************************************/
MEA_Status MEA_API_Set_RootFilter_Entry(MEA_Unit_t                 unit,
    MEA_root_Filter_type_t     index,
    MEA_Bool                   enable)
{
    MEA_Uint32 value;
    

    if (!MEA_GW_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_GW_SUPPORT is not support on this HW\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (index >= MEA_ROOT_FILTER_TYPE_LAST){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index is out of range \n", __FUNCTION__);
        return MEA_ERROR;

    }

    value = My_rootFilter;
    (value) &= ~(0x0000001 << (index % 32));
    (value) |= (enable << (index % 32));

    


    /*Write to HW  */
    if (value != My_rootFilter){
        if (mea_GW_RootFilter_valid_UpdateEntry(unit, value) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - index is out of range \n", __FUNCTION__);
            return MEA_ERROR;
        }

        My_rootFilter = value;
    }
 
    return MEA_OK;
}

MEA_Status MEA_API_Get_RootFilter_Entry(MEA_Unit_t                  unit,
    MEA_root_Filter_type_t      index,
    MEA_Bool                    *enable)
{

    if (!MEA_GW_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_GW_SUPPORT is not support on this HW\n", __FUNCTION__);
        return MEA_ERROR;
    }
    if (enable == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - enable = NULL\n");
        return MEA_ERROR;

    }

    if (index >= MEA_ROOT_FILTER_TYPE_LAST){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index is out of range \n", __FUNCTION__);
        return MEA_ERROR;

    }


    *enable = ((My_rootFilter >> index ) & 0x1);




    return MEA_OK;
}





MEA_Status MEA_API_Set_GW_Global_Entry(MEA_Unit_t                 unit,

    MEA_Gw_Global_dbt                   *entry)

{

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }


   


    if(mea_drv_GW_Update_Global(unit,entry,  &MEA_GW_Global_Entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_GW_Update_Global fail \n", __FUNCTION__);
        return MEA_ERROR;
    }


    
    /* update  database   */
    MEA_OS_memcpy(&MEA_GW_Global_Entry, entry, sizeof(MEA_Gw_Global_dbt));
    
    return MEA_OK;
}

MEA_Status MEA_API_Get_GW_Global_Entry(MEA_Unit_t                 unit,

    MEA_Gw_Global_dbt                   *entry)
{

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry, &MEA_GW_Global_Entry, sizeof(MEA_Gw_Global_dbt));


    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_API_IF_InterrUp_Enable(MEA_Unit_t  unit,MEA_Bool Enable)
{

    MEA_Uint32 SetValue=0;

    SetValue = MEA_WD_TIMEOUT_DEF_VAL;
    if (Enable){
        SetValue |= 0x10000000; /*Enable*/
    }
    

    MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_WD_TA_TIMEOUT, SetValue, MEA_MODULE_IF);
    
    return MEA_OK;
}

MEA_Status MEA_API_IF_InterrUp_Set_MASK(MEA_Unit_t  unit,MEA_Uint32 SetValue){


    MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_INTERRUPT_MASK_REG, SetValue, MEA_MODULE_IF);

    return MEA_OK;
}


MEA_Status MEA_API_IF_InterrUp_Get_Status(MEA_Unit_t  unit,MEA_Uint32 *GetValue){


    *GetValue = MEA_API_ReadReg(MEA_UNIT_0, ENET_IF_INTERRUPT_REG, MEA_MODULE_IF);
    
    return MEA_OK;
}


/************************************************************************/
/*   #define ENET_BM_IMR               interrupt mask                   */
/*   #define ENET_BM_ENABLE            interrupt enable                 */
/*   #define ENET_BM_ISR               interrupt status                 */  
/************************************************************************/
MEA_Status MEA_API_BM_InterrUp_Enable(MEA_Unit_t  unit,MEA_Bool Enable)
{

    MEA_Uint32 SetValue = 0;

   
    if (Enable){
        SetValue = 0x00000001; /*Enable*/
    }


    MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_ENABLE, SetValue, MEA_MODULE_BM);

    return MEA_OK;
}

MEA_Status MEA_API_BM_InterrUp_Set_MASK(MEA_Unit_t  unit,MEA_Uint32 SetValue){


    MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_IMR, SetValue, MEA_MODULE_BM);

    return MEA_OK;
}


MEA_Status MEA_API_BM_InterrUp_Get_Status(MEA_Unit_t  unit,MEA_Uint32 *GetValue){


    *GetValue = MEA_API_ReadReg(MEA_UNIT_0, ENET_BM_ISR, MEA_MODULE_BM);

    return MEA_OK;
}
/************************************************************************/
/*          WallCLock                                                   */
/************************************************************************/

static void mea_drv_wallclock_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
   //  MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
  //    MEA_Policer_Prof_hw_dbt*hwEntry_pi = (MEA_Policer_Prof_hw_dbt*)arg3;
    MEA_Uint32  entry = (MEA_Uint32)(long)arg4;
    MEA_Uint32             val[1];
  

//    MEA_Uint32             count_shift;


    val[0] = 0;
    val[0] = entry;

  
 

    /**************************/


    
    MEA_API_WriteReg(unit_i, ENET_BM_IA_WRDATA0, val[0], MEA_MODULE_BM);
   
 

}

/************************************************************************/
/*         WallCLock_ Setting API                                       */
/************************************************************************/



/************************************************************************/
/*         WallCLock_Set_CTL0_REG                                       */
/************************************************************************/



MEA_Status MEA_API_WallCLock_CTL0_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_CTL0_REG *entry)
{
    MEA_ind_write_t ind_write;
    ENET_ind_read_t ind_read;
    ENET_Uint32 read_data[1];



    if (type_RW != MEA_WALLCLOCK_TYPE_READ && type_RW != MEA_WALLCLOCK_TYPE_WRITE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  The command is not correct \n");
        return MEA_ERROR;

    }

    ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL0_REG;
    ind_read.tableOffset  = ENET_BM_WALL_CLOCK_CTL0_REG;

    if (type_RW == MEA_WALLCLOCK_TYPE_WRITE){ /*write*/

        /* init the ind_write */
        //ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        ind_write.cmdReg = MEA_BM_IA_CMD;
        ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
        ind_write.statusReg = MEA_BM_IA_STAT;
        ind_write.statusMask = MEA_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_wallclock_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2 = (MEA_funcParam)hwUnit;
        // ind_write.funcParam3 = (MEA_funcParam)MEA_ind_write_t ind_write;;
        ind_write.funcParam4 = (MEA_funcParam)(long)entry->regs[0];

        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , mea_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }
    }
    else if (type_RW == MEA_WALLCLOCK_TYPE_READ) {

        //ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_read.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        /* build default ind_read structure */
        ind_read.cmdReg = ENET_BM_IA_CMD;
        ind_read.cmdMask = ENET_BM_IA_CMD_MASK;
        ind_read.statusReg = ENET_BM_IA_STAT;
        ind_read.statusMask = ENET_BM_IA_STAT_MASK;
        ind_read.tableAddrReg = ENET_BM_IA_ADDR;
        ind_read.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;

        ind_read.read_data = read_data;


        if (ENET_ReadIndirect(ENET_UNIT_0, &ind_read,
            ENET_NUM_OF_ELEMENTS(read_data),
            ENET_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Read from hw failed tblType=%d tblOffset=%d \n",
                __FUNCTION__,
                ind_read.tableType, ind_read.tableOffset);

            return ENET_ERROR;
        }
        entry->regs[0] = read_data[0];
    }





    return MEA_OK;
}

/************************************************************************/
/*         WallCLock_Set_CTL1_REG                                       */
/************************************************************************/
MEA_Status MEA_API_WallCLock_CTL1_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_CTL1_REG *entry)
{
    MEA_ind_write_t ind_write;
    ENET_ind_read_t ind_read;
    ENET_Uint32 read_data[1];

    

    if (type_RW != MEA_WALLCLOCK_TYPE_READ && type_RW != MEA_WALLCLOCK_TYPE_WRITE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  The command is not correct \n");
            return MEA_ERROR;
        
    }

    ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;
    ind_read.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

    if (type_RW == MEA_WALLCLOCK_TYPE_WRITE){ /*write*/

        /* init the ind_write */
        //ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;
        
        ind_write.cmdReg = MEA_BM_IA_CMD;
        ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
        ind_write.statusReg = MEA_BM_IA_STAT;
        ind_write.statusMask = MEA_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_wallclock_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2 = (MEA_funcParam)hwUnit;
        // ind_write.funcParam3 = (MEA_funcParam)MEA_ind_write_t ind_write;;
        ind_write.funcParam4 = (MEA_funcParam)(long)entry->regs[0];

        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , mea_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }
    }
    else if (type_RW == MEA_WALLCLOCK_TYPE_READ) {

        //ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_read.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        /* build default ind_read structure */
        ind_read.cmdReg = ENET_BM_IA_CMD;
        ind_read.cmdMask = ENET_BM_IA_CMD_MASK;
        ind_read.statusReg = ENET_BM_IA_STAT;
        ind_read.statusMask = ENET_BM_IA_STAT_MASK;
        ind_read.tableAddrReg = ENET_BM_IA_ADDR;
        ind_read.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;
        
        ind_read.read_data = read_data;
        
        
        if (ENET_ReadIndirect(ENET_UNIT_0, &ind_read,
            ENET_NUM_OF_ELEMENTS(read_data),
            ENET_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Read from hw failed tblType=%d tblOffset=%d \n",
                __FUNCTION__,
                ind_read.tableType, ind_read.tableOffset);

            return ENET_ERROR;
        }
        entry->regs[0] = read_data[0];
    }





    return MEA_OK;
}




MEA_Status MEA_API_WallCLock_SEC_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_SEC_REG *entry)
{

    MEA_ind_write_t ind_write;
    ENET_ind_read_t ind_read;
    ENET_Uint32 read_data[1];

    if (type_RW != MEA_WALLCLOCK_TYPE_READ && type_RW != MEA_WALLCLOCK_TYPE_WRITE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  The command is not correct \n");
        return MEA_ERROR;

    }

    ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;

    ind_write.tableOffset = ENET_BM_WALL_CLOCK_SET_SEC_REG;
    ind_read.tableOffset = ENET_BM_WALL_CLOCK_GET_SEC_REG;

    if (type_RW == MEA_WALLCLOCK_TYPE_WRITE){ /*write*/

        /* init the ind_write */
        //ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        ind_write.cmdReg = MEA_BM_IA_CMD;
        ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
        ind_write.statusReg = MEA_BM_IA_STAT;
        ind_write.statusMask = MEA_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_wallclock_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2 = (MEA_funcParam)hwUnit;
        // ind_write.funcParam3 = (MEA_funcParam)MEA_ind_write_t ind_write;;
        ind_write.funcParam4 = (MEA_funcParam)(long)entry->regs[0];

        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , mea_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }
    }
    else if (type_RW == MEA_WALLCLOCK_TYPE_READ) {

        //ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_read.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        /* build default ind_read structure */
        ind_read.cmdReg = ENET_BM_IA_CMD;
        ind_read.cmdMask = ENET_BM_IA_CMD_MASK;
        ind_read.statusReg = ENET_BM_IA_STAT;
        ind_read.statusMask = ENET_BM_IA_STAT_MASK;
        ind_read.tableAddrReg = ENET_BM_IA_ADDR;
        ind_read.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;

        ind_read.read_data = read_data;


        if (ENET_ReadIndirect(ENET_UNIT_0, &ind_read,
            ENET_NUM_OF_ELEMENTS(read_data),
            ENET_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Read from hw failed tblType=%d tblOffset=%d \n",
                __FUNCTION__,
                ind_read.tableType, ind_read.tableOffset);

            return ENET_ERROR;
        }
        entry->regs[0] = read_data[0];
    }





    return MEA_OK;

}



MEA_Status MEA_API_WallCLock_NS_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_NS_REG *entry)
{

    MEA_ind_write_t ind_write;
    ENET_ind_read_t ind_read;
    ENET_Uint32 read_data[1];

    if (type_RW != MEA_WALLCLOCK_TYPE_READ && type_RW != MEA_WALLCLOCK_TYPE_WRITE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  The command is not correct \n");
        return MEA_ERROR;

    }

    ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;

    ind_write.tableOffset = ENET_BM_WALL_CLOCK_SET_NS_REG;
    ind_read.tableOffset = ENET_BM_WALL_CLOCK_GET_NS_REG;

    if (type_RW == MEA_WALLCLOCK_TYPE_WRITE){ /*write*/

        /* init the ind_write */
        //ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        ind_write.cmdReg = MEA_BM_IA_CMD;
        ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
        ind_write.statusReg = MEA_BM_IA_STAT;
        ind_write.statusMask = MEA_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_wallclock_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2 = (MEA_Uint32)hwUnit;
        // ind_write.funcParam3 = (MEA_Uint32)MEA_ind_write_t ind_write;;
        ind_write.funcParam4 = (MEA_funcParam)(long)entry->regs[0];

        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , mea_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }
    }
    else if (type_RW == MEA_WALLCLOCK_TYPE_READ) {

        //ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_read.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        /* build default ind_read structure */
        ind_read.cmdReg = ENET_BM_IA_CMD;
        ind_read.cmdMask = ENET_BM_IA_CMD_MASK;
        ind_read.statusReg = ENET_BM_IA_STAT;
        ind_read.statusMask = ENET_BM_IA_STAT_MASK;
        ind_read.tableAddrReg = ENET_BM_IA_ADDR;
        ind_read.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;

        ind_read.read_data = read_data;


        if (ENET_ReadIndirect(ENET_UNIT_0, &ind_read,
            ENET_NUM_OF_ELEMENTS(read_data),
            ENET_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Read from hw failed tblType=%d tblOffset=%d \n",
                __FUNCTION__,
                ind_read.tableType, ind_read.tableOffset);

            return ENET_ERROR;
        }
        entry->regs[0] = read_data[0];
    }





    return MEA_OK;

    
}


/************************************************************************/
/*         WallCLock_COMP_REG                                           */
/************************************************************************/
MEA_Status MEA_API_WallCLock_COMP_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_COMP_REG *entry)
{

    MEA_ind_write_t ind_write;
    ENET_ind_read_t ind_read;
    ENET_Uint32 read_data[1];

    if (type_RW != MEA_WALLCLOCK_TYPE_READ && type_RW != MEA_WALLCLOCK_TYPE_WRITE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  The command is not correct \n");
        return MEA_ERROR;

    }

    ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_read.tableType  = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    
    ind_write.tableOffset = ENET_BM_WALL_CLOCK_COMP_REG;
    ind_read.tableOffset = ENET_BM_WALL_CLOCK_COMP_REG;

    if (type_RW == MEA_WALLCLOCK_TYPE_WRITE){ /*write*/

        /* init the ind_write */
        //ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        ind_write.cmdReg = MEA_BM_IA_CMD;
        ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
        ind_write.statusReg = MEA_BM_IA_STAT;
        ind_write.statusMask = MEA_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_wallclock_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2 = (MEA_funcParam)hwUnit;
        // ind_write.funcParam3 = (MEA_funcParam)MEA_ind_write_t ind_write;;
        ind_write.funcParam4 = (MEA_funcParam)(long)entry->regs[0];

        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , mea_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }
    }
    else if (type_RW == MEA_WALLCLOCK_TYPE_READ) {

        //ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_read.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        /* build default ind_read structure */
        ind_read.cmdReg = ENET_BM_IA_CMD;
        ind_read.cmdMask = ENET_BM_IA_CMD_MASK;
        ind_read.statusReg = ENET_BM_IA_STAT;
        ind_read.statusMask = ENET_BM_IA_STAT_MASK;
        ind_read.tableAddrReg = ENET_BM_IA_ADDR;
        ind_read.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;

        ind_read.read_data = read_data;


        if (ENET_ReadIndirect(ENET_UNIT_0, &ind_read,
            ENET_NUM_OF_ELEMENTS(read_data),
            ENET_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Read from hw failed tblType=%d tblOffset=%d \n",
                __FUNCTION__,
                ind_read.tableType, ind_read.tableOffset);

            return ENET_ERROR;
        }
        entry->regs[0] = read_data[0];
    }
    
    
    
    return MEA_OK;
}


/************************************************************************/
/*         WallCLock_COR_REG                                           */
/************************************************************************/
MEA_Status MEA_API_WallCLock_COR_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_COR_REG *entry)
{
    MEA_ind_write_t ind_write;
    ENET_ind_read_t ind_read;
    ENET_Uint32 read_data[1];

    if (type_RW != MEA_WALLCLOCK_TYPE_READ && type_RW != MEA_WALLCLOCK_TYPE_WRITE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  The command is not correct \n");
        return MEA_ERROR;

    }

    ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_read.tableType  = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;

    ind_write.tableOffset = ENET_BM_WALL_CLOCK_COR_REG;
    ind_read.tableOffset  = ENET_BM_WALL_CLOCK_COR_REG;

    if (type_RW == MEA_WALLCLOCK_TYPE_WRITE){ /*write*/

        /* init the ind_write */
        //ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        ind_write.cmdReg = MEA_BM_IA_CMD;
        ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
        ind_write.statusReg = MEA_BM_IA_STAT;
        ind_write.statusMask = MEA_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (MEA_funcParam)mea_drv_wallclock_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2 = (MEA_funcParam)hwUnit;
        // ind_write.funcParam3 = (MEA_funcParam)MEA_ind_write_t ind_write;;
        ind_write.funcParam4 = (MEA_funcParam)(long)entry->regs[0];

        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , mea_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }
    }
    else if (type_RW == MEA_WALLCLOCK_TYPE_READ) {

        //ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_read.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        /* build default ind_read structure */
        ind_read.cmdReg = ENET_BM_IA_CMD;
        ind_read.cmdMask = ENET_BM_IA_CMD_MASK;
        ind_read.statusReg = ENET_BM_IA_STAT;
        ind_read.statusMask = ENET_BM_IA_STAT_MASK;
        ind_read.tableAddrReg = ENET_BM_IA_ADDR;
        ind_read.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;

        ind_read.read_data = read_data;


        if (ENET_ReadIndirect(ENET_UNIT_0, &ind_read,
            ENET_NUM_OF_ELEMENTS(read_data),
            ENET_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Read from hw failed tblType=%d tblOffset=%d \n",
                __FUNCTION__,
                ind_read.tableType, ind_read.tableOffset);

            return ENET_ERROR;
        }
        entry->regs[0] = read_data[0];
    }



    return MEA_OK;
}





MEA_Status MEA_API_WallCLock_ACUM_CFG1_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_ACUM_CFG1_REG *entry)
{
    MEA_ind_write_t ind_write;
    ENET_ind_read_t ind_read;
    ENET_Uint32 read_data[1];

    if (type_RW != MEA_WALLCLOCK_TYPE_READ && type_RW != MEA_WALLCLOCK_TYPE_WRITE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  The command is not correct \n");
        return MEA_ERROR;

    }

    ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;

    ind_write.tableOffset = ENET_BM_WALL_CLOCK_ACUM_CFG1_REG;
    ind_read.tableOffset  = ENET_BM_WALL_CLOCK_ACUM_CFG1_REG;

    if (type_RW == MEA_WALLCLOCK_TYPE_WRITE){ /*write*/

        /* init the ind_write */
        //ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        ind_write.cmdReg = MEA_BM_IA_CMD;
        ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
        ind_write.statusReg = MEA_BM_IA_STAT;
        ind_write.statusMask = MEA_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (MEA_funcParam)mea_drv_wallclock_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2 = (MEA_funcParam)hwUnit;
        // ind_write.funcParam3 = (MEA_funcParam)MEA_ind_write_t ind_write;;
        ind_write.funcParam4 = (MEA_funcParam)(long)entry->regs[0];

        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , mea_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }
    }
    else if (type_RW == MEA_WALLCLOCK_TYPE_READ) {

        //ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_read.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        /* build default ind_read structure */
        ind_read.cmdReg = ENET_BM_IA_CMD;
        ind_read.cmdMask = ENET_BM_IA_CMD_MASK;
        ind_read.statusReg = ENET_BM_IA_STAT;
        ind_read.statusMask = ENET_BM_IA_STAT_MASK;
        ind_read.tableAddrReg = ENET_BM_IA_ADDR;
        ind_read.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;

        ind_read.read_data = read_data;


        if (ENET_ReadIndirect(ENET_UNIT_0, &ind_read,
            ENET_NUM_OF_ELEMENTS(read_data),
            ENET_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Read from hw failed tblType=%d tblOffset=%d \n",
                __FUNCTION__,
                ind_read.tableType, ind_read.tableOffset);

            return ENET_ERROR;
        }
        entry->regs[0] = read_data[0];
    }



    return MEA_OK;
}
MEA_Status MEA_API_WallCLock_ACUM_CFG2_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_ACUM_CFG2_REG *entry)
{
    MEA_ind_write_t ind_write;
    ENET_ind_read_t ind_read;
    ENET_Uint32 read_data[1];

    if (type_RW != MEA_WALLCLOCK_TYPE_READ && type_RW != MEA_WALLCLOCK_TYPE_WRITE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  The command is not correct \n");
        return MEA_ERROR;

    }

    ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
    ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;

    ind_write.tableOffset = ENET_BM_WALL_CLOCK_ACUM_CFG2_REG;
    ind_read.tableOffset = ENET_BM_WALL_CLOCK_ACUM_CFG2_REG;

    if (type_RW == MEA_WALLCLOCK_TYPE_WRITE){ /*write*/

        /* init the ind_write */
        //ind_write.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_write.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        ind_write.cmdReg = MEA_BM_IA_CMD;
        ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
        ind_write.statusReg = MEA_BM_IA_STAT;
        ind_write.statusMask = MEA_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (MEA_funcParam)mea_drv_wallclock_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2 = (MEA_funcParam)hwUnit;
        // ind_write.funcParam3 = (MEA_funcParam)MEA_ind_write_t ind_write;;
        ind_write.funcParam4 = (MEA_funcParam)(long)entry->regs[0];

        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , mea_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }
    }
    else if (type_RW == MEA_WALLCLOCK_TYPE_READ) {

        //ind_read.tableType = ENET_BM_TBL_TYP_WALL_CLOCK_REGS;
        //ind_read.tableOffset = ENET_BM_WALL_CLOCK_CTL1_REG;

        /* build default ind_read structure */
        ind_read.cmdReg = ENET_BM_IA_CMD;
        ind_read.cmdMask = ENET_BM_IA_CMD_MASK;
        ind_read.statusReg = ENET_BM_IA_STAT;
        ind_read.statusMask = ENET_BM_IA_STAT_MASK;
        ind_read.tableAddrReg = ENET_BM_IA_ADDR;
        ind_read.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;

        ind_read.read_data = read_data;


        if (ENET_ReadIndirect(ENET_UNIT_0, &ind_read,
            ENET_NUM_OF_ELEMENTS(read_data),
            ENET_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Read from hw failed tblType=%d tblOffset=%d \n",
                __FUNCTION__,
                ind_read.tableType, ind_read.tableOffset);

            return ENET_ERROR;
        }
        entry->regs[0] = read_data[0];
    }



    return MEA_OK;
}



MEA_Status MEA_API_WallCLock_Set_1588(MEA_Unit_t unit, MEA_Clock_t *entry, MEA_Bool SW_Enable)
{

    mea_WallCLock_TS_CTL1_REG CTL1_entry;
    mea_WallCLock_TS_SEC_REG  SEC_entry;
    mea_WallCLock_TS_NS_REG   NS_SEC_entry;

    MEA_OS_memset(&CTL1_entry, 0, sizeof(CTL1_entry));
    MEA_OS_memset(&SEC_entry, 0, sizeof(SEC_entry));
    MEA_OS_memset(&NS_SEC_entry, 0, sizeof(NS_SEC_entry));


    if ((entry != NULL) && (SW_Enable == MEA_TRUE)){
        SEC_entry.val.SEC = entry->sec;
        if (MEA_API_WallCLock_SEC_REG(unit, MEA_WALLCLOCK_TYPE_WRITE, &SEC_entry) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  MEA_API_WallCLock_SEC_REG failed\n ");
            return MEA_ERROR;

        }
        NS_SEC_entry.val.NS = entry->nsec;
        if (MEA_API_WallCLock_NS_REG(unit, MEA_WALLCLOCK_TYPE_WRITE, &NS_SEC_entry) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  MEA_API_WallCLock_NS_REG failed\n ");
            return MEA_ERROR;

        }
    }

   

    CTL1_entry.val.ENBL_FORCE_NS_ALL_ZERO = SW_Enable;

    if (MEA_API_WallCLock_CTL1_REG(unit, MEA_WALLCLOCK_TYPE_WRITE, &CTL1_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  MEA_API_WallCLock_CTL1_REG failed\n ");
        return MEA_ERROR;

    }
    CTL1_entry.val.TS_WR_LATCH = MEA_TRUE;
    if (MEA_API_WallCLock_CTL1_REG(unit, MEA_WALLCLOCK_TYPE_WRITE, &CTL1_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s  MEA_API_WallCLock_CTL1_REG failed\n ");
        return MEA_ERROR;

    }


    return MEA_OK;
}



MEA_Status MEA_API_Buffer_Search(MEA_Unit_t unit,
                                mea_BuffSearch_dbt *entry_i)
{
    MEA_Uint32 val;
    if (entry_i->val.Buff_Id > 0xffff){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Buffer_Search Id %d is out of range\n", entry_i->val.Buff_Id);
        return MEA_ERROR;
    }

    if ((entry_i->val.En_Search == MEA_TRUE) && (entry_i->val.Force_release == MEA_TRUE)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "set only the Search enable or release \n", entry_i->val.Buff_Id);
        return MEA_ERROR;
    }

    val = 0;

    val = (entry_i->val.Buff_Id) & 0xffff;
    val |= entry_i->val.En_Search << 16;
    val |= (entry_i->val.Force_release) << 17;

    if (entry_i->val.En_Search == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "buffer id=%d start search \n", entry_i->val.Buff_Id);
    }
    if (entry_i->val.Force_release == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "buffer id=%d start release  \n", entry_i->val.Buff_Id);
    }

        MEA_API_WriteReg(MEA_UNIT_0,
        ENET_BM_SEARCH_BUFFER,
        val,
        MEA_MODULE_BM);

    return MEA_OK;
}




MEA_Status MEA_API_Buffer_Status(MEA_Unit_t unit,
                                 mea_BuffSearch_dbt *entry_o)
{

    if (entry_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "entry_o = NULL \n");
        return MEA_ERROR;
    }

    entry_o->regs[0] = MEA_API_ReadReg(MEA_UNIT_0, ENET_BM_SEARCH_BUFFER, MEA_MODULE_BM);



    return MEA_OK;

}

MEA_Status MEA_API_Get_Ts_MEASUREMENT(MEA_Unit_t unit, MEA_Uint32 *ret_masurement)
{
    MEA_Uint32 IngTs[1];
    MEA_Uint32 egTs[1];
    MEA_ind_read_t                 ind_read;
    mea_memory_mode_e save_globalMemoryMode;

    if(ret_masurement == NULL)
    { 
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ret_masurement = NULL \n");
        return MEA_ERROR;
    }

    if (!MEA_DRV_TS_MEASUREMENT_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "TS_MEASUREMENT Not SUPPORT \n");
        return MEA_ERROR;
    }


    /*read ingress */
    save_globalMemoryMode = globalMemoryMode;
    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;



    /* Read offset 0 -  */
    ind_read.read_data = &(IngTs[0]);
    ind_read.tableOffset = MEA_GLOBAl_TBL_REG_S_ing_ts_value;
    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(IngTs),
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
       
        globalMemoryMode = save_globalMemoryMode;
        return MEA_ERROR;
    }





    /*read Egress */

    ind_read.tableType = ENET_BM_TBL_TYP_GW_GLOBAL_REGS; /*Table 7*/
    ind_read.cmdReg = MEA_BM_IA_CMD;
    ind_read.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_read.statusReg = MEA_BM_IA_STAT;
    ind_read.statusMask = MEA_BM_IA_STAT_MASK;
    ind_read.tableAddrReg = MEA_BM_IA_ADDR;
    ind_read.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_read.tableOffset = MEA_GLOBAl_TBL_REG_S_EGRESS_ts_value;
    ind_read.read_data = &egTs[0];

   

    /* read from HW */
    ind_read.read_data = &egTs[0];
   
    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(egTs),
        MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ReadIndirect from tableType %d / tableOffset %d module BM failed \n"
            ,
           __FUNCTION__, ind_read.tableType, ind_read.tableOffset);
           globalMemoryMode = save_globalMemoryMode;
        return MEA_ERROR;
    }





    if (egTs[0] > IngTs[0])
    {
        *ret_masurement = (egTs[0] - IngTs[0]);
    }
    else {
        *ret_masurement = 0xDEADDEAD;
    }


    return MEA_OK;
}



/************************************************************************/
/*    MEA_API_GPIO_Z70_RW                                               */
/*     read_write  0-read 1-write                                       */
/*     *value      return/ set value                                    */
/************************************************************************/

MEA_Status MEA_API_GPIO_Z70_RW(MEA_Unit_t unit, MEA_Uint8 read_write, MEA_Uint32 *value)
{
    if (value == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "value == NULL");
        return MEA_ERROR;
    }

    if (read_write == MEA_TRUE) {
        MEA_API_WriteReg(unit, ENET_IF_REG_Z70_GPIO, *value, MEA_MODULE_IF);
        return MEA_OK;
    }
    else {
        *value = MEA_API_ReadReg(MEA_UNIT_0, ENET_IF_REG_Z70_GPIO, MEA_MODULE_IF);
        return MEA_OK;
    }

    return MEA_OK;
}


/************************************************************************/
/* VxLAN snooping                                                       */
/************************************************************************/

static MEA_Status mea_drv_vxlan_read_info_Entry(MEA_Unit_t  unit, 
                                                MEA_Uint32 tableOffset, 
                                                MEA_Uint32 numOfReg, MEA_Uint32 *pArry)
{


    MEA_ind_read_t ind_read;




    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_SNOOP_VXLAN;
    ind_read.tableOffset = tableOffset;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = pArry;

    /* Write to the Indirect Table */
    if (MEA_LowLevel_ReadIndirect(unit, &ind_read,
        numOfReg,
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect MODULE_IF tbl=%3d (Offset=%3d) failed\n",
            __FUNCTION__, ind_read.tableType, tableOffset);

        return MEA_ERROR;
    }





    return MEA_OK;

}




MEA_Status MEA_API_Get_Vxlan_snooping_Info(MEA_Unit_t  unit, MEA_Snoop_info_dbt *infoEntry )
{
    MEA_Uint32 numOfReg = 5;
    MEA_Uint32  val[5];
    MEA_Uint32 count_shift;

    if (MEA_ignore_vxlan_info)
	    return MEA_OK;

    if (!MEA_VXLAN_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_VXLAN_SUPPORT not support\n");
        return MEA_ERROR;
    }

    if (infoEntry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "infoEntry == NULL\n");
        return MEA_ERROR;
    }

    if(mea_drv_vxlan_read_info_Entry(unit, 0, numOfReg, val) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"mea_drv_vxlan_read_info_Entry failed \n");

        return MEA_ERROR;
    }
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/

    count_shift = 0;

    infoEntry->number_words = MEA_OS_read_value(count_shift,10,val);
    count_shift += 10;
    count_shift += 6; /*reserve*/


    
    infoEntry->vxlan_snp_empty = MEA_OS_read_value(count_shift, 1, val);
    count_shift += 1;
    infoEntry->vxlan_snp_full = MEA_OS_read_value(count_shift, 1, val);
    count_shift += 1;
    infoEntry->vxlan_snp_overflow = MEA_OS_read_value(count_shift, 1, val);
    count_shift += 1;
    infoEntry->vxlan_snp_underflow = MEA_OS_read_value(count_shift, 1, val);
    count_shift += 1;



    return MEA_OK;
}


MEA_Status MEA_API_Get_Vxlan_snooping_Entry(MEA_Unit_t  unit, MEA_Snoop_Entry_dbt *Entry)
{
    MEA_Uint32 numOfReg = 6;
    MEA_Uint32  val[10];
    MEA_Uint32 count_shift;

    if (!MEA_VXLAN_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_VXLAN_SUPPORT not support\n");
        return MEA_ERROR;
    }

    if (Entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Entry == NULL\n");
        return MEA_ERROR;
    }
    MEA_OS_memset(&val, 0, sizeof(val));

    if (mea_drv_vxlan_read_info_Entry(unit, 1, numOfReg, val) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "mea_drv_vxlan_read_info_Entry failed \n");

        return MEA_ERROR;
    }
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    count_shift = 0;

    //MEA_OS_memcpy(&(Entry->sa_mac_ext), &val, sizeof(MEA_MacAddr));
    // count_shift += 48;
    Entry->sa_mac_ext.b[5]= MEA_OS_read_value(count_shift, 8, val);
    count_shift += 8;
    Entry->sa_mac_ext.b[4] = MEA_OS_read_value(count_shift, 8, val);
    count_shift += 8;
    Entry->sa_mac_ext.b[3] = MEA_OS_read_value(count_shift, 8, val);
    count_shift += 8;
    Entry->sa_mac_ext.b[2] = MEA_OS_read_value(count_shift, 8, val);
    count_shift += 8;
    Entry->sa_mac_ext.b[1] = MEA_OS_read_value(count_shift, 8, val);
    count_shift += 8;
    Entry->sa_mac_ext.b[0] = MEA_OS_read_value(count_shift, 8, val);
    count_shift += 8;





    
    
    

    Entry->src_ipv4_ext = MEA_OS_read_value(count_shift, 32, val);
    count_shift += 32;
    Entry->src_ipv4_int = MEA_OS_read_value(count_shift, 32, val);
    count_shift += 32;
    Entry->src_port = MEA_OS_read_value(count_shift, 7, val);
    count_shift += 7;
    Entry->L2_type = MEA_OS_read_value(count_shift, 5, val);
    count_shift += 5;
    Entry->L2_sub_type = MEA_OS_read_value(count_shift, 4, val);
    count_shift += 4;
    Entry->L3_type = MEA_OS_read_value(count_shift, 5, val);
    count_shift += 5;
    Entry->vni = MEA_OS_read_value(count_shift, 24, val);
    count_shift += 24;
    Entry->vlan_vxlan_int = MEA_OS_read_value(count_shift, 12, val);
    count_shift += 12;

    return MEA_OK;
}



