/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_bm_drv.h"
#include "mea_drv_common.h"
#include "mea_if_l2cp_drv.h"
#include "mea_port_drv.h"
#include "mea_fwd_tbl.h"
#include "mea_db_drv.h"
#include "mea_globals_drv.h"
#include "mea_deviceInfo_drv.h"




/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#if 0 
#define MEA_DEVICE_INFO_PM_BLOCK_SIZE(unit_i,hwUnit_i)     ( ENET_BM_TBL_TYP_CNT_PM_XXX_LENGTH((unit_i),(hwUnit_i))/2)  ////1024//128      
#else
#define MEA_DEVICE_INFO_PM_BLOCK_SIZE(unit_i,hwUnit_i)     ( ENET_BM_TBL_TYP_CNT_PM_XXX_LENGTH((unit_i),(hwUnit_i)))  ////1024//128      
#endif


/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
MEA_Uint32    MEA_InterfaceHwInfo[MEA_MAX_INERTNAL_INTERFACE_NUMBER];

#if 1 


MEA_drv_port2Interface_dbt *MEA_Ports2InterfaceInfo = NULL;    // [MEA_MAX_PORT_NUMBER + 1];
MEA_drv_Interface2Port_dbt *MEA_Interface2PortsInfo = NULL;   // [MEA_MAX_PORT_NUMBER + 1];

#else


extern MEA_Uint8 MEA_PortsTypeSetting[MEA_MAX_PORT_NUMBER+1];

MEA_drv_port2Interface_dbt MEA_Ports2InterfaceInfo[MEA_MAX_PORT_NUMBER+1];
MEA_drv_Interface2Port_dbt MEA_Interface2PortsInfo[MEA_MAX_PORT_NUMBER+1];
#endif

static MEA_DeviceInfo_dbt*     MEA_DeviceInfo_Table = NULL;
static char   MEA_chip_name [MEA_CHIP_NAME_STR_LENGTH];

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
static MEA_Bool mea_drv_Get_DeviceInfo_IsPacketAnalyzer_exist(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

static MEA_Status mea_drv_Get_DeviceInfo_GetChipName(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION 
static MEA_Status mea_drv_DeviceInfo_Set_info(MEA_Unit_t unit_i,
                                              MEA_db_HwUnit_t hwUnit_i,
                                              MEA_DeviceInfo_dbt*entry);

#endif

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_DeviceInfo_Conclude>                                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_DeviceInfo_Conclude(MEA_Unit_t unit)
{

   MEA_Platform_OS_free(MEA_DeviceInfo_Table);

  return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_DeviceInfo_Init>                                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_DeviceInfo_Init (MEA_Unit_t unit_i)
{
 
    MEA_DeviceInfo_dbt      entry;
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
    MEA_ind_read_t          ind_read;
    mea_memory_mode_e       save_globalMemoryMode;
    
#endif

    MEA_db_HwUnit_t         hwUnit;
    
    
    MEA_InterfaceHwInfo[31] = 127;
    MEA_InterfaceHwInfo[30] = 122; //0xFFFF;
    MEA_InterfaceHwInfo[29] =  126;
    MEA_InterfaceHwInfo[28] =  125;
    MEA_InterfaceHwInfo[27] =  119;
    MEA_InterfaceHwInfo[26] =  118;
    MEA_InterfaceHwInfo[25] =  111;
    MEA_InterfaceHwInfo[24] =  110;
    MEA_InterfaceHwInfo[23] =  109;
    MEA_InterfaceHwInfo[22] =  108;
    MEA_InterfaceHwInfo[21] =  107;
    MEA_InterfaceHwInfo[20] =  106;
    MEA_InterfaceHwInfo[19] =  105;
    MEA_InterfaceHwInfo[18] =  104;
    MEA_InterfaceHwInfo[17] =  103;
    MEA_InterfaceHwInfo[16] =  102;
    MEA_InterfaceHwInfo[15] =  101;
    MEA_InterfaceHwInfo[14] =  100;
    MEA_InterfaceHwInfo[13] =  99;
    MEA_InterfaceHwInfo[12] =  98;
    MEA_InterfaceHwInfo[11] =  97;
    MEA_InterfaceHwInfo[10] =  96;
    MEA_InterfaceHwInfo[9] =  95;
    MEA_InterfaceHwInfo[8] =  94;
    MEA_InterfaceHwInfo[7] =  93;
    MEA_InterfaceHwInfo[6] =  92;
    MEA_InterfaceHwInfo[5] =  72;
    MEA_InterfaceHwInfo[4] =  48;
    MEA_InterfaceHwInfo[3] =  36;
    MEA_InterfaceHwInfo[2] =  24;
    MEA_InterfaceHwInfo[1] =  12;
    MEA_InterfaceHwInfo[0] =  0;





    /* Allocate DeviceInfo Table */
    MEA_DeviceInfo_Table = (MEA_DeviceInfo_dbt*)MEA_OS_malloc
                    (mea_drv_num_of_hw_units*sizeof(MEA_DeviceInfo_dbt));

    if (MEA_DeviceInfo_Table == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_DeviceInfo_Table == NULL \n",__FUNCTION__);
       return MEA_ERROR;
    } 
    MEA_OS_memset(MEA_DeviceInfo_Table,
                  0,
                  mea_drv_num_of_hw_units*sizeof(MEA_DeviceInfo_dbt));
    if (b_bist_test){
        return MEA_OK;
    }
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize DeviceInfo...\n");

#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION 


    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        hwUnit=MEA_MODE_REGULAR_ENET3000;
        MEA_OS_memset(&entry,0,sizeof(MEA_DeviceInfo_dbt));
      
        if(mea_drv_DeviceInfo_Set_info(unit_i,0,&entry)!=MEA_OK){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Error on init DeviceInfo \n");
        }

      MEA_OS_memcpy(&MEA_DeviceInfo_Table[hwUnit],&entry,sizeof(MEA_DeviceInfo_Table[0]));
      
    
    }

#else /* MEA_OS_DEVICE_MEMORY_SIMULATION */


{
    /* Init the ind_read structure for fpga context table */
    ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_DEVICE_CONTEXT;
    ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
    ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_read.statusReg		= MEA_IF_STATUS_REG;   
    ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
    ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;

    /* For each hwUnit read all features from fpga context table */
    for(hwUnit=0; hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        
        /* Set global memory mode according to the hwUnit */
        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);

        /* Reset the entry */
        MEA_OS_memset(&entry,0,sizeof(MEA_DeviceInfo_dbt)); 


        /* Read index 0 - feature global */
        ind_read.read_data      = &(entry.global.index_0_regs[0] ); 
        ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_0;
        if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.global.index_0_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
        }
#if 1
//entry.global.val.flowCos_Mapping_exist     = MEA_TRUE;
//entry.global.val.flowMarking_Mapping_exist = MEA_TRUE;

        //entry.global.val.Interface_Id=0;

        
//        entry.global.val.HeaderCompress_support   = MEA_FALSE;
//        entry.global.val.HeaderDeCompress_support = MEA_FALSE;
//        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,">>>>>>>>>>>>>>>>>>>>>Alex Set tHeaderCompress_support HeaderDeCompress_support Disable <<<<<<<<<<<<<<<<<<<<<<<<\n");

        







//entry.global.val.L2CP_enable = MEA_TRUE; // this bit always need to be True
//entry.global.val.pre_sched_enable =1;

#endif
        /* Read index 1 - feature global */
        ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_1;
        ind_read.read_data      = &(entry.global1.index_1_regs[0]); 
        if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.global1.index_1_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
        }



        /* Read feature egress_port mask */
        ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_2;
        ind_read.read_data      = &(entry.hpm.index_2_regs[0]);
         if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.hpm.index_2_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
        }

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_3;
    ind_read.read_data      = &(entry.pre_parser.index_3_regs[0]); 
     if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.pre_parser.index_3_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
        }

     /* Until all old version of fpga will support this new bit */

     //entry.pre_parser.val.vsp_exist  = MEA_TRUE;


    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_4;
    ind_read.read_data      = &(entry.parser.index_4_regs[0]); 
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.parser.index_4_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
        }
    
   
    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_5;
    ind_read.read_data      = &(entry.service.index_5_regs[0]); 
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.service.index_5_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }

    
    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_6;
    ind_read.read_data      = &(entry.acl.index_6_regs[0]); 
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.acl.index_6_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }
    

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_7;
    ind_read.read_data      = &(entry.forwarder_lmt.index_7_regs[0]); 
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.forwarder_lmt.index_7_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }


    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_8;
    ind_read.read_data      = &(entry.forwarder.index_8_regs[0]); 
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.forwarder.index_8_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }
#if 1
    {

      
        if (MEA_device_environment_info.MEA_FWD_DISABLE_ebable) {
           
            entry.forwarder.val.exist= !((MEA_Uint32)(MEA_device_environment_info.MEA_FWD_DISABLE));
            if(entry.forwarder.val.exist== MEA_FALSE)
               entry.forwarder.val.show_all=0;

            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"User set the forwarder [%s]\n",MEA_STATUS_STR(entry.forwarder.val.exist));
        }
    
    //entry.forwarder.val.exist = MEA_FALSE;
    //entry.forwarder.val.show_all=0;
    }


#endif
    
    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_9;
    ind_read.read_data      = &(entry.shaper.index_9_regs[0]); 
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.shaper.index_9_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }
#if 0 

MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"SHAPER OFF\n");

entry.shaper.val.shaper_per_Priqueue_exist=MEA_FALSE;
entry.shaper.val.shaper_per_cluster_exist=MEA_FALSE;
entry.shaper.val.shaper_per_port_exist=MEA_FALSE;



#endif

#if defined(MEA_OS_SYAC)
//entry.shaper.val.shaper_per_Priqueue_exist=MEA_FALSE;
#endif


//entry.shaper.val.num_of_shaper_profile =256;
    
    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_10;
    ind_read.read_data      = &(entry.policer.index_10_regs[0]); 
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.policer.index_10_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }


#if 1 // T.B.D - Until the software will know to handle size of zero.
    if (globalMemoryMode == MEA_MODE_UPSTREEM_ONLY) {
        entry.policer.val.num_of_TMID = 2;
    }
//entry.policer.val.exist =MEA_FALSE;
//entry.policer.val.port_exist = MEA_FALSE; // bug 
#endif




    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_11;
    ind_read.read_data      = &(entry.wfq.index_11_regs[0]); 
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.wfq.index_11_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }
    

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_12;
    ind_read.read_data      = &(entry.action.index_12_regs[0]); 
    
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.action.index_12_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_13;
    ind_read.read_data      = &(entry.xpermission.index_13_regs[0]); 
    
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.xpermission.index_13_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }


    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_14;
    ind_read.read_data      = &(entry.sar_machine.index_14_regs[0]); 
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.sar_machine.index_14_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }
    
    
    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_15;
    ind_read.read_data      = &(entry.editor_BMF.index_15_regs[0]); 
    
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.editor_BMF.index_15_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }

#if 0
      printf(" ************* Patch  set num of LM is Zero  instated of %d \n",entry.editor_BMF.val.num_of_lm_count);
      entry.editor_BMF.val.num_of_lm_count = 0;
  
#endif

 if (MEA_Platform_EnhancePmCounterSupport()) {
    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_16;
    ind_read.read_data      = &(entry.pm.index_16_regs[0]); 
    
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.pm.index_16_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }

    //entry.pm.val.num0fBlocksForBytes=2;
    //entry.pm.val.num0fBlocksForPmPackets=2;
    
    } else {
         entry.pm.val.num0fBlocksForBytes=1;
         entry.pm.val.num0fBlocksForPmPackets=1;
         entry.pm.val.width_pm_id=9;     /* max is 512 */
        
    }
    
    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_17;
    ind_read.read_data      = &(entry.packetGen.index_17_regs[0]); 
    
    if (MEA_API_ReadIndirect(unit_i,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(entry.packetGen.index_17_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    return MEA_ERROR;
    }

     /* nvgr_tunnel*/

    ind_read.tableOffset = MEA_IF_CONTEXT_INDEX_18;
    ind_read.read_data = &(entry.nvgr_tunnel.index_18_regs[0]);

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.nvgr_tunnel.index_18_regs),
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);
        return MEA_ERROR;
    }




#if 0
    entry.packetGen.val.Analyzer_type2_num_of_streams=16;
    entry.packetGen.val.Analyzer_num_of_support_profile = 16;
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"---alex set Analyzer_type2_num_of_streams %d",entry.packetGen.val.Analyzer_type2_num_of_streams);
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"---alex set Analyzer_num_of_support_profile %d",entry.packetGen.val.Analyzer_num_of_support_profile);


#endif

     ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_21;
     ind_read.read_data      = &(entry.Rx_mac_fifo.index_21_regs[0]); 

     if (MEA_API_ReadIndirect(unit_i,
         &ind_read,
         MEA_NUM_OF_ELEMENTS(entry.Rx_mac_fifo.index_21_regs),
         MEA_MODULE_IF) != MEA_OK) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
             __FUNCTION__,
             ind_read.tableType,
             ind_read.tableOffset,
             MEA_MODULE_IF);
         return MEA_ERROR;
     }

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_22;
    ind_read.read_data      = &(entry.Tx_mac_fifo.index_22_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.Tx_mac_fifo.index_22_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_23;
    ind_read.read_data      = &(entry.fragment.index_23_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.fragment.index_23_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }
#if 1
entry.fragment.val.fragment_vsp_exist = MEA_TRUE;
#endif

/* TDM*/
ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_24;
ind_read.read_data      = &(entry.Interface_Tdm_A.index_24_regs[0]); 

if (MEA_API_ReadIndirect(unit_i,
    &ind_read,
    MEA_NUM_OF_ELEMENTS(entry.Interface_Tdm_A.index_24_regs),
    MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);
        return MEA_ERROR;
}



ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_25;
ind_read.read_data      = &(entry.Interface_Tdm_B.index_25_regs[0]); 

if (MEA_API_ReadIndirect(unit_i,
    &ind_read,
    MEA_NUM_OF_ELEMENTS(entry.Interface_Tdm_B.index_25_regs),
    MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);
        return MEA_ERROR;
}

#if 0 // pcm only
entry.Interface_Tdm_B.val.exist = MEA_TRUE;
entry.Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
entry.Interface_Tdm_B.val.start_ts  = 8; //96 
entry.Interface_Tdm_B.val.num0fspe  = 8;
#endif

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_26;
    ind_read.read_data      = &(entry.Interface_Tdm_C.index_26_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.Interface_Tdm_C.index_26_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }
    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_27;
    ind_read.read_data      = &(entry.global_Tdm.index_27_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.global_Tdm.index_27_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }
/************************************************************************/
/*                                                                      */
/************************************************************************/
    if((entry.Interface_Tdm_A.val.type == MEA_TDM_INTERFACE_PCM) &&
       (entry.Interface_Tdm_B.val.type == MEA_TDM_INTERFACE_PCM) &&
       (entry.global_Tdm.val.mode_satop_only == MEA_TRUE))
    {
        entry.global_Tdm.val.num_of_ces = 16;

    }
//     if(entry.global_Tdm.val.num_of_ces == 384){
//        entry.global_Tdm.val.num_of_ces=344;
//     }


/************************************************************************/
/*                                                                      */
/************************************************************************/

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_29;
    ind_read.read_data      = &(entry.mac_mii_support.index_29_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.mac_mii_support.index_29_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }

 

 

    





 

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_34;
    ind_read.read_data      = &(entry.Hc_Classifier.index_34_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.Hc_Classifier.index_34_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }

    ind_read.tableOffset	= MEA_IF_CONTEXT_INDEX_35;
    ind_read.read_data      = &(entry.Egress_Classifier.index_35_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.Egress_Classifier.index_35_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }


   ind_read.tableOffset = MEA_IF_CONTEXT_INDEX_40;
        ind_read.read_data      = &(entry.slice0.index_40_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.slice0.index_40_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }


    ind_read.tableOffset = MEA_IF_CONTEXT_INDEX_41;
    ind_read.read_data      = &(entry.slice1.index_41_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.slice1.index_41_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }

    ind_read.tableOffset = MEA_IF_CONTEXT_INDEX_42;
    ind_read.read_data      = &(entry.slice2.index_42_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.slice2.index_42_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }
    ind_read.tableOffset = MEA_IF_CONTEXT_INDEX_43;
    ind_read.read_data      = &(entry.slice3.index_43_regs[0]); 

    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.slice3.index_43_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }


        
        
       


    MEA_OS_memcpy(&MEA_DeviceInfo_Table[hwUnit],&entry,sizeof(MEA_DeviceInfo_dbt));
    
        

    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

   }//end for
}

#endif /* MEA_OS_DEVICE_MEMORY_SIMULATION */
    
//mea_deviceInfo_print(unit_i,0 );

   mea_drv_Get_DeviceInfo_GetChipName(unit_i, 0);
   if (entry.pm.val.num0fBlocksForBytes > entry.pm.val.num0fBlocksForPmPackets) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - pm num of blocks for bytes (%d) > pm num of blocks for packets %d \n",
                          __FUNCTION__,
                          entry.pm.val.num0fBlocksForBytes,
                          entry.pm.val.num0fBlocksForPmPackets);
        return MEA_ERROR;
    }

   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize GetPort2Interface...\n");
   mea_drv_Get_DeviceInfo_GetPort2Interface_Init(unit_i);

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_DeviceInfo>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_DeviceInfo(MEA_Unit_t          unit_i,
                                  MEA_Uint32          hwUnit_i,
                                  MEA_DeviceInfo_dbt  *entry_po)
{

   
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid hwUnit_i parameter */
    if (hwUnit_i >= mea_drv_num_of_hw_units) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - hwUnit_i(%d) >= max value\n",
                         __FUNCTION__,
                         hwUnit_i,
                         mea_drv_num_of_hw_units);
       return MEA_ERROR;
    }

    /* Check for valid entry parameter */
    if (entry_po == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry==NULL \n",__FUNCTION__);
       return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    
    

    MEA_OS_memcpy(entry_po,&MEA_DeviceInfo_Table[hwUnit_i],sizeof(*entry_po));

    return MEA_OK;
}


MEA_Bool mea_drv_Get_DeviceInfo_Egress_Filter_exist   (MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_filter_exist);
}

MEA_Bool            mea_drv_Get_DeviceInfo_Egress_classifier_exist    (MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_classifier_exist);
}

MEA_Bool mea_drv_Get_DeviceInfo_Egress_parser_exist(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_parser_exist);
}

MEA_Bool            mea_drv_Get_DeviceInfo_Egress_classifierRang_exist    (MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_classifierRang_exist);
}     

MEA_Uint16            mea_drv_Get_DeviceInfo_Egress_classifier_number_contex    (MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_classifier_number_contex);
}  

MEA_Uint16            mea_drv_Get_DeviceInfo_Egress_classifier_Key_width   (MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_classifier_Key_width);
}  
MEA_Uint16            mea_drv_Get_DeviceInfo_Egress_classifier_hash_groups   (MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_classifier_hash_groups);
}

MEA_Uint16            mea_drv_Get_DeviceInfo_Egress_classifier_num_of_rangPort   (MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_classifier_num_of_rangPort);
} 
MEA_Uint16            mea_drv_Get_DeviceInfo_Egress_classifier_of_ranges   (MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_classifier_of_ranges);
}    

MEA_Uint16            mea_drv_Get_DeviceInfo_Egress_classifier_number_hashflow   (MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].Egress_Classifier.val.Egress_classifier_number_hashflow);
}           
     


       
 




MEA_Bool            mea_drv_Get_DeviceInfo_Forwader_supportDSE    (MEA_Unit_t unit_i,
															       MEA_db_HwUnit_t hwUnit_i)
{
	return (MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.exist);
}

MEA_Bool mea_drv_Get_DeviceInfo_ADM_support(MEA_Unit_t unit_i,
                                            MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.adm_support);
}
MEA_Bool mea_drv_Get_DeviceInfo_segment_switch_support(MEA_Unit_t unit_i,
                                            MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.segment_switch_support);
}


MEA_Service_t mea_drv_Get_DeviceInfo_NumOfService(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

      
	    return (MEA_Service_t)(MEA_DeviceInfo_Table[hwUnit_i].service.val.table_size);
    
}

MEA_Service_t mea_drv_Get_DeviceInfo_NumOfExternalService(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    
    switch (MEA_DeviceInfo_Table[hwUnit_i].service.val.srv_externl_size){
    case   0:
        return 0;
        break;
    case   1:
    case   2:
        return (MEA_Service_t)(128 * MEA_DeviceInfo_Table[hwUnit_i].service.val.srv_externl_size) * 1024;
        break;
    case   3:
        return (MEA_Service_t)(512) * 1024;
    break;
    case   4:
        return (MEA_Service_t)  (1024*1024);
        break;
    default:
        return 0;
        break;
    
    }

    return 0;

}



MEA_Uint16 mea_drv_Get_DeviceInfo_ExternalService_mode(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    switch (MEA_DeviceInfo_Table[hwUnit_i].service.val.srv_externl_size){
    case   0:
        return 0;
        break;
    case   1:
        return 0; /*17Bit*/
        break;
    case   2:
        return 1; /*18Bit*/
        break;
    case   3:
        return 2;/*19bit*/
        break;
    case   4:
        return 3;/*20bit*/
        break;
    default:
        return 0;
        break;

    }


}









MEA_Uint16 mea_drv_Get_DeviceInfo_Service_NumOf_groupHash(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].service.val.num_service_hash;
}

MEA_Uint16 mea_drv_Get_DeviceInfo_Service_NumOf_groupHash_EVC(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].service.val.EVC_Classifier_num_hash_group;
}

MEA_Bool mea_drv_Get_DeviceInfo_Service_EVC_Classifier_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool)MEA_DeviceInfo_Table[hwUnit_i].service.val.EVC_Classifier_exist;
}
MEA_Uint16 mea_drv_Get_DeviceInfo_Service_EVC_classifier_key_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].service.val.EVC_classifier_key_width;
}



MEA_Action_t mea_drv_Get_DeviceInfo_NumOfForworwarAction(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Action_t)(1 << MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.fwd_action);
                           
} 

MEA_Bool mea_drv_Get_DeviceInfo_fwd_LPM_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool)MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.lpm_enable;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_fwd_LPM_Ipv4_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.lpm_ipv4;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_fwd_LPM_Ipv6_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    if(MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.lpm_ipv6 != 0 )
        return 1 << MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.lpm_ipv6;
    else
    return 0;
}

MEA_Bool mea_drv_Get_DeviceInfo_queueStatistics_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
	return (MEA_Bool)MEA_DeviceInfo_Table[hwUnit_i].global.val.QueueStat_en;
}

 
MEA_Uint16 mea_drv_Get_DeviceInfo_GetMaxLQ(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Uint16 MaxLQ = 1;

    switch (MEA_DeviceInfo_Table[hwUnit_i].global.val.num_of_port_Ingress)
    {
    case 0:
        MaxLQ = 1;
        break;
    case 1:
        MaxLQ = 2;
        break;
    case 2:
        MaxLQ = 4;
        break;
    case 3:
        MaxLQ = 8;
        break;
    }
    

    return MaxLQ;
}

MEA_Uint16 mea_drv_Get_DeviceInfo_GetMaxNumOf_EgressPort(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
	MEA_Uint16 NumOfPort = 127;

	switch (MEA_DeviceInfo_Table[hwUnit_i].global.val.num_of_Egress_port)
	{
	case 0:
		NumOfPort = 128 - 1;
		break;
	case 1:
		NumOfPort = 256 - 1;
		break;
	case 2:
		NumOfPort = 512 - 1;
		break;
	case 3:
		NumOfPort = 1024 - 1;
		break;



	}


	return NumOfPort;
}


MEA_Uint16 mea_drv_Get_DeviceInfo_GetMaxNumOfPort_INGRESS(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Uint16 NumOfPort = 127;

    switch (MEA_DeviceInfo_Table[hwUnit_i].global.val.num_of_port_Ingress)
    {
    case 0:
        NumOfPort = 128 - 1;
        break;
    case 1:
        NumOfPort = 256 - 1;
        break;
    case 2:
        NumOfPort = 512 - 1;
        break;
    case 3:
        NumOfPort = 1024 - 1;
        break;
	default:
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"NumOfPort Ingress %d\n", NumOfPort);
		break;


    }
	
	//MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "NumOfPort Ingress %d\n", NumOfPort);
	return NumOfPort;
}

MEA_Action_t mea_drv_Get_DeviceInfo_NumOfActions(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{


    return (MEA_Action_t)(MEA_DeviceInfo_Table[hwUnit_i].action.val.table_size == 0 ? 1 : MEA_DeviceInfo_Table[hwUnit_i].action.val.table_size);

}

MEA_Action_t mea_drv_Get_DeviceInfo_NumOfActions_noACL(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Action_t calc;

    if(MEA_DeviceInfo_Table[hwUnit_i].action.val.table_size == 0){
         calc= 1;
    } else {
    
        calc = MEA_DeviceInfo_Table[hwUnit_i].action.val.table_size;
        
        if (MEA_DeviceInfo_Table[hwUnit_i].acl.val.exist == MEA_TRUE) {
            calc -= MEA_DeviceInfo_Table[hwUnit_i].acl.val.table_size;
        }
        if (MEA_HPM_SUPPORT) {
            if(MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_numOfAct!=0)
            calc -= (1 << MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_numOfAct);
        }
        
        
    }

    return calc;

}

MEA_Filter_t mea_drv_Get_DeviceInfo_NumOfACL(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Filter_t)((MEA_DeviceInfo_Table[hwUnit_i].acl.val.exist == MEA_TRUE) ? (MEA_DeviceInfo_Table[hwUnit_i].acl.val.table_size): 0);


}
MEA_Bool mea_drv_Get_DeviceInfo_ACL_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].acl.val.exist;
}
MEA_Bool mea_drv_Get_DeviceInfo_ACL_hierarchical_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global1.val.hierarchical_en;
}

MEA_Bool mea_drv_Get_DeviceInfo_Vxlan_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global1.val.vxlan_support;
}


MEA_Uint16 mea_drv_Get_DeviceInfo_instance_fifo_1588(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global1.val.instanc_fifo_1588;
}


MEA_Uint32 mea_drv_Get_DeviceInfo_ActToBMbus(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    if (MEA_DeviceInfo_Table[hwUnit_i].global1.val.ActToBMbus == 0)
        return 1;
    else
        return MEA_DeviceInfo_Table[hwUnit_i].global1.val.ActToBMbus;

}

MEA_Uint32 mea_drv_Get_DeviceInfo_max_numof_descriptor(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Uint32 calculate=2;


    if (!MEA_ALLOC_ALLOCR_NEW_SUPPORT) {

       
            calculate = (MEA_DeviceInfo_Table[hwUnit_i].global1.val.max_numof_descriptor); //- 1;

       
        calculate = (1 << calculate) * 1024;
    }
    else {
        calculate = (MEA_DeviceInfo_Table[hwUnit_i].global1.val.max_numof_descriptor);
    }

        return (calculate);
}

MEA_Uint32 mea_drv_Get_DeviceInfo_max_numof_DataBuffer(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Uint32 calculate = 1;



     if (MEA_DeviceInfo_Table[hwUnit_i].global1.val.max_numof_DataBuffer != 0)
        calculate = (MEA_DeviceInfo_Table[hwUnit_i].global1.val.max_numof_DataBuffer) ;
  


    return (calculate);
}


MEA_Bool mea_drv_Get_DeviceInfo_ip_fragmentation_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global1.val.ip_reassembly;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_ip_reassembly_session_id_key_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global1.val.ip_reassembly_session_id_key_width;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_ip_reassembly_session_id_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global1.val.ip_session_id_width;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_ip_reassembly_numof_session_id(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return 1<<MEA_DeviceInfo_Table[hwUnit_i].global1.val.ip_session_id_width;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_NumOf_FWD_ENB_DA(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
	MEA_Uint32 reVal=0;
	
		if (MEA_GW_SUPPORT) {
			if (MEA_DeviceInfo_Table[hwUnit_i].global1.val.num_enb_mac == 0)
			{
				//reVal= (2 * 1024);
				reVal = 0;
			}
			else {

				reVal = (1 << MEA_DeviceInfo_Table[hwUnit_i].global1.val.num_enb_mac);
			}
		}

	return reVal;
}


MEA_Filter_t mea_drv_Get_DeviceInfo_NumOfACL_contex(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Filter_t numOfcontex;

    numOfcontex = (MEA_Filter_t)((MEA_DeviceInfo_Table[hwUnit_i].global1.val.hierarchical_en == MEA_TRUE) ? (MEA_DeviceInfo_Table[hwUnit_i].acl.val.acl_context) : (mea_drv_Get_DeviceInfo_NumOfACL(MEA_UNIT_0, ((hwUnit_i)))));
    return  numOfcontex;

}

/************************************************************************/
/* ACL5  Device                                                         */
/************************************************************************/
MEA_Bool mea_drv_Get_DeviceInfo_ACL5_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
   

    return  MEA_DeviceInfo_Table[hwUnit_i].acl.val.ACL5_exist;

}

MEA_Bool mea_drv_Get_DeviceInfo_ACL5_ExternalSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    return  MEA_DeviceInfo_Table[hwUnit_i].acl.val.ACL5_External;

}
MEA_Bool mea_drv_Get_DeviceInfo_ACL5_InternalSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    return  MEA_DeviceInfo_Table[hwUnit_i].acl.val.ACL5_Internal;

}

MEA_Uint32 mea_drv_Get_DeviceInfo_ACL5_Ipmask_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    return  (0x1 << (MEA_DeviceInfo_Table[hwUnit_i].acl.val.ACL5_Ipmask_size));

}

MEA_Uint32 mea_drv_Get_DeviceInfo_ACL5_keymask_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    return  (0x1 << (MEA_DeviceInfo_Table[hwUnit_i].acl.val.ACL5_keymask_size));

}

MEA_Uint32 mea_drv_Get_DeviceInfo_ACL5_rangeProf_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    return  (0x1 << (MEA_DeviceInfo_Table[hwUnit_i].acl.val.ACL5_rangeProf_size));

}


MEA_Uint32 mea_drv_Get_DeviceInfo_ACL5_rangeBlock_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    return  (0x1 << (MEA_DeviceInfo_Table[hwUnit_i].acl.val.ACL5_rangeBlock));

}





MEA_Uint32 mea_Get_PolicersGroup(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    if (MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_TMID >= 2048) {
        return 2048; /*TM Group*/
    }else
    {
        return MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_TMID;
    }

}

MEA_TmId_t mea_drv_Get_DeviceInfo_NumOfPolicers(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    
    
    return (MEA_TmId_t) (MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_TMID);

}

MEA_Uint32 mea_drv_Get_DeviceInfo_CIR_Token(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
	return (MEA_DeviceInfo_Table[hwUnit_i].policer.val.CIR_Token_bits);
}

MEA_Uint32 mea_drv_Get_DeviceInfo_CBS_bucket(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
	return (MEA_DeviceInfo_Table[hwUnit_i].policer.val.CBS_bucket_bits);
}

MEA_TmId_t mea_drv_Get_DeviceInfo_NumOfPolicersPtr(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    
    return (MEA_TmId_t) (MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_TMID);

}

MEA_Editing_t mea_drv_Get_DeviceInfo_EHPs_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Editing_t)(MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.num_of_edit_width);

}

MEA_Editing_t mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Editing_t Editing = MEA_EDITING_SW_X_MAX;


     if ((MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) && 
         (MEA_device_environment_info.MEA_DEVICE_DB_EDITING_enable == MEA_FALSE) )
     {
         if (MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.num_of_edit_width == MEA_EDITING_SW_X_MAX)
              return (MEA_Editing_t)(1 << MEA_EDITING_SW_X_USER);
     }
     if ((MEA_device_environment_info.MEA_DEVICE_DB_EDITING_enable == MEA_TRUE)) {
         Editing = MEA_device_environment_info.MEA_DEVICE_DB_EDITING;
         if (Editing == 0 || Editing >= MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.num_of_edit_width) {
             return (MEA_Editing_t)(1 << MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.num_of_edit_width);
         }
         return (MEA_Editing_t)(1 << Editing);
     }
//       if (MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.edit_force_6k == MEA_TRUE){
//  
//           return (MEA_Editing_t)(6*1024);
//       }



    return (MEA_Editing_t) (1<<MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.num_of_edit_width);

}
MEA_Bool  mea_drv_Get_DeviceInfo_MC_Editor_new_mode_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    return (MEA_Bool)(MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.MC_Edit_new_mode);

}
MEA_Bool  mea_drv_Get_DeviceInfo_Editor_new_dataBase_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    return (MEA_Bool)(MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.EDIT_DB_EN);

}

MEA_Uint32  mea_drv_Get_DeviceInfo_Editor_new_dataBase_numofBits(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{


    return (MEA_Uint32)(MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.EDIT_DB_num_of_bits);

}




MEA_Uint16 mea_drv_Get_DeviceInfo_NumOfMC_EHPs(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    if (!MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.MC_Edit_new_mode){
        return  8;
    }
    else{
        return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.num_of_MC_Edit_Per_Cluster);
    }

        
}

MEA_Uint16 mea_drv_Get_DeviceInfo_NumOf_AC_MTUs(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
       
    return (512);

}
MEA_Bool mea_drv_Get_DeviceInfo_bmf_stamp802_1p_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool)(MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.stamp802_1p_support);
}

MEA_Uint16 mea_drv_Get_DeviceInfo_NumOflm_count(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
return (MEA_Uint16) (MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.num_of_lm_count);
}
MEA_Bool mea_drv_Get_DeviceInfo_bmf_ccm_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool) (MEA_DeviceInfo_Table[hwUnit_i].editor_BMF.val.BMF_CCM_count_EN);
}

MEA_PmId_t mea_drv_Get_DeviceInfo_NumOfPmIds(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_PmId_t)(1<<MEA_DeviceInfo_Table[hwUnit_i].pm.val.width_pm_id);
}

MEA_Uint16 mea_drv_Get_DeviceInfo_PmWidth(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].pm.val.width_pm_id);
}


MEA_Uint32 mea_drv_Get_DeviceInfo_crypto_db_prof_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Uint32)(MEA_DeviceInfo_Table[hwUnit_i].nvgr_tunnel.val.crypto_db_prof_width);
}


MEA_Uint32 mea_drv_Get_DeviceInfo_crypto_db_numOf_profile(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Uint32)(1<<MEA_DeviceInfo_Table[hwUnit_i].nvgr_tunnel.val.crypto_db_prof_width);
}




ENET_xPermissionId_t mea_drv_Get_DeviceInfo_NumOfXpermission(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
        return (ENET_xPermissionId_t) (MEA_DeviceInfo_Table[hwUnit_i].xpermission.val.xper_table_size);
    
}
MEA_Bool mea_drv_Get_DeviceInfo_mstp_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Limiter_t) (MEA_DeviceInfo_Table[hwUnit_i].global.val.mstp_support);

}

MEA_Bool mea_drv_Get_DeviceInfo_ts_measuremant_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Bool)(MEA_DeviceInfo_Table[hwUnit_i].global.val.TS_MEASUREMENT_en);

}



MEA_Bool mea_drv_Get_DeviceInfo_rstp_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Limiter_t)(MEA_DeviceInfo_Table[hwUnit_i].global.val.rstp_support);

}

MEA_Bool mea_drv_Get_DeviceInfo_ECN_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Limiter_t) (MEA_DeviceInfo_Table[hwUnit_i].global.val.ECN_support);

}

MEA_Uint32 mea_drv_Get_DeviceInfo_num_ofMSTP(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    if (MEA_DeviceInfo_Table[hwUnit_i].xpermission.val.num_ofMSTP  == 0)
        return 0;
    if (MEA_DeviceInfo_Table[hwUnit_i].xpermission.val.num_ofMSTP == 1)
        return 1;
    else
     return   (1<< (MEA_DeviceInfo_Table[hwUnit_i].xpermission.val.num_ofMSTP));
}


MEA_Uint8 mea_drv_Get_DeviceInfo_Xpermission_num_clusterInGroup(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    
 return (MEA_Uint8) (MEA_DeviceInfo_Table[hwUnit_i].xpermission.val.num_clusterInGroup);

}

ENET_QueueId_t mea_drv_Get_DeviceInfo_NumOfClusters(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return (ENET_QueueId_t) (MEA_DeviceInfo_Table[hwUnit_i].wfq.val.num_of_cluster); // num of cluster we have
}


MEA_Bool mea_drv_Get_DeviceInfo_IsLimiterSupport(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
         
    return (MEA_Limiter_t) (MEA_DeviceInfo_Table[hwUnit_i].forwarder_lmt.val.exist);
    
}

MEA_Limiter_t mea_drv_Get_DeviceInfo_NumOfLimiters(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
         
    return (MEA_Limiter_t) (MEA_DeviceInfo_Table[hwUnit_i].forwarder_lmt.val.table_size);
    
}

MEA_LxcpAction_t mea_drv_Get_DeviceInfo_NumOfLxcpAction(MEA_db_HwUnit_t hwUnit_i)
{

	if(MEA_DeviceInfo_Table[hwUnit_i].acl.val.extended_lxcp == 0){
		if(MEA_DeviceInfo_Table[hwUnit_i].acl.val.lxcp_act_width == 0){
			return 8;
		} else {
			return ((0x00000001)<<(MEA_DeviceInfo_Table[hwUnit_i].acl.val.lxcp_act_width));
		}
	}else{
			return ((0x00000001)<<(MEA_DeviceInfo_Table[hwUnit_i].acl.val.new_lxcp_act_width));
	}

}




MEA_Uint16 mea_drv_Get_DeviceInfo_Lxcp_num_of_prof(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit_i)
{
	if(MEA_DeviceInfo_Table[hwUnit_i].acl.val.extended_lxcp == 0){   
		if(MEA_DeviceInfo_Table[hwUnit_i].acl.val.lxcp_prof_width == 0){
			return 16;
		} else {
			return ((0x00000001)<<(MEA_DeviceInfo_Table[hwUnit_i].acl.val.lxcp_prof_width));
		}
	}else{
		return ((0x00000001)<<(MEA_DeviceInfo_Table[hwUnit_i].acl.val.new_lxcp_prof_width));

	}
}


MEA_Uint32 mea_drv_Get_DeviceInfo_GetForwarderLearnActionIdWidth(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.width_learn_action_id;
    
}


MEA_Uint32 mea_drv_Get_DeviceInfo_GetForwarderLearnSrcPortValueWidth(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.width_learn_src_port;
}


MEA_Bool      mea_drv_Get_DeviceInfo_IsPortPolicerSupport(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_FALSE;

}


MEA_Bool  mea_drv_Get_DeviceInfo_Is_Ingress_flow_PolicerSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool)MEA_DeviceInfo_Table[hwUnit_i].policer.val.Ingress_flow_exist;
    //return MEA_FALSE;
}

MEA_Uint32  mea_drv_Get_DeviceInfo_Ingress_flow_Policer_profile(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Uint32 calculate;
    if (MEA_DeviceInfo_Table[hwUnit_i].policer.val.Ingress_flow_num_of_prof == 0){
        calculate = 256;
    }
    else{
        calculate = 1<<(MEA_DeviceInfo_Table[hwUnit_i].policer.val.Ingress_flow_num_of_prof);
    }

    return calculate;

    

}
MEA_Bool  mea_drv_Get_DeviceInfo_policer_slowslowSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].policer.val.policer_slowslow;
}


MEA_Uint32 mea_drv_Get_DeviceInfo_NumOFIngress_flowPolicers(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i) 
{
    return mea_drv_Get_DeviceInfo_Ingress_flow_Policer_profile(unit_i, hwUnit_i) * 4;  /*BC,MC,UN,Un-UC*/
}




MEA_Bool      mea_drv_Get_DeviceInfo_IsPolicerSupport(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool)MEA_DeviceInfo_Table[hwUnit_i].policer.val.exist;

}


MEA_Uint32 mea_drv_Get_DeviceInfo_systemCalc_CLK(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Uint64 reVal ;
   // MEA_Uint64  calc_clk;

    MEA_Uint32 M;
    MEA_Uint32 N;
    MEA_Uint32 pushClk = 40; /*  40 nanu --> 25 Mhz*/
    
    reVal.val = 100; //100;





    M = MEA_DeviceInfo_Table[hwUnit_i].global1.val.sys_M;
    N = MEA_DeviceInfo_Table[hwUnit_i].global1.val.sys_N;
	if (MEA_device_environment_info.MEA_NUM_OF_SYS_MN_enable == MEA_TRUE)
	{
		M = (MEA_device_environment_info.MEA_NUM_OF_SYS_MN >> 16) & 0xff;
		N = (MEA_device_environment_info.MEA_NUM_OF_SYS_MN ) & 0xff;
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "##################### USER Set M=%d,N=%d   #####################\n",M,N);

	}

    
    if (MEA_DeviceInfo_Table[hwUnit_i].global1.val.pushClk == 0){
        pushClk = 40;
    }
    else{
        pushClk = MEA_DeviceInfo_Table[hwUnit_i].global1.val.pushClk;
    }

    
        
    if (M == 0 || N == 0){
        
        if ( ! MEA_device_environment_info.MEA_DEVICE_SYS_CLK_enable)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "##################### M,N ==0  SET SYS Clock %d,M  M #####################\n", reVal.val);
    
    } else{

         

            
        reVal.val = (1000000000 );
        reVal.val = reVal.val / pushClk;  // 25M

        reVal.val = reVal.val * M;

        reVal.val = reVal.val / (N);
        
            //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "  DEVICE_SYS_CLK calculate %llu \n", reVal.val);
    }

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)

    if (MEA_device_environment_info.MEA_DEVICE_SYS_CLK_enable) {
        reVal.val = MEA_device_environment_info.MEA_DEVICE_SYS_CLK;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " User set DEVICE_SYS_CLK %llu \n", reVal.val);
    }

#endif /* MEA_OS_LINUX */


    return  (MEA_Uint32) reVal.val;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_Get_Max_point_2_multipoint(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return ((MEA_ShaperSupportType_te)(MEA_DeviceInfo_Table[hwUnit_i].global1.val.max_point_2_multipoint));
}

MEA_Bool      mea_drv_Get_DeviceInfo_GetShaperPriQueueType_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return ((MEA_ShaperSupportType_te)(MEA_DeviceInfo_Table[hwUnit_i].shaper.val.shaper_per_Priqueue_exist));
}

MEA_Bool      mea_drv_Get_DeviceInfo_GetShaperClusterType_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].shaper.val.shaper_per_cluster_exist);
}
MEA_Bool      mea_drv_Get_DeviceInfo_GetShaperPortType_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].shaper.val.shaper_per_port_exist);
}


MEA_Uint32 mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support()
{
    MEA_Unit_t unit_i = MEA_UNIT_0;
    MEA_db_HwUnit_t hwUnit_i;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit_i,return 0;)

    return 8; 

}

MEA_Uint16 mea_drv_Get_DeviceInfo_Get_numOf_Shaper_Profile(MEA_Unit_t unit_i,
                                                           MEA_db_HwUnit_t hwUnit_i)
{
    

        return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].shaper.val.num_of_shaper_profile);

}

MEA_Bool mea_drv_Get_DeviceInfo_GetShaper_xCIR_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].shaper.val.xCIR_en);
    //20bit
}




MEA_Bool      mea_drv_Get_DeviceInfo_IsIngressMtuSupport(MEA_Unit_t unit_i)
{
	if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        return MEA_TRUE;
     } else {
       return MEA_FALSE;
     }
    

}
MEA_QueueMtuSupportType_te mea_drv_Get_DeviceInfo_IsMtuPriQueueSupport(MEA_Unit_t unit_i)
{
	
        return MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE;
     
}
MEA_Bool mea_drv_Get_DeviceInfo_Isqueue_mc_mqs_Support(MEA_Unit_t unit_i,
                                                       MEA_db_HwUnit_t hwUnit_i)
{
	return (MEA_DeviceInfo_Table[hwUnit_i].global.val.mc_mqs_Support) ;
}

MEA_Bool mea_drv_Get_DeviceInfo_Isqueue_count_Support(MEA_Unit_t unit_i,
                                                      MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].wfq.val.queue_count_exist);
}

MEA_Bool mea_drv_Get_DeviceInfo_Isqueue_adminOn_Support(MEA_Unit_t unit_i,
                                                       MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].global.val.cluster_adminon_support) ;
}

MEA_Bool mea_drv_Get_DeviceInfo_IsLAG_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].global.val.lag_support);
}

MEA_Uint16 mea_drv_Get_DeviceInfo_NumLAG_Profile(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].xpermission.val.num_lag_prof);
}

MEA_Bool mea_drv_Get_DeviceInfo_extend_default_sid_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].global.val.extend_default_sid);
}


MEA_Uint16 mea_drv_Get_DeviceInfo_Num_of_Slice(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].global.val.num_of_slice);
}

MEA_Bool      mea_drv_Get_DeviceInfo_IsServiceRangeSupport(MEA_Unit_t unit_i)
{
     MEA_db_HwUnit_t  hwUnit; 

	 MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR); 
	
	return (MEA_DeviceInfo_Table[hwUnit].service.val.serviceRangeExist) ; 

}


MEA_Uint32      mea_drv_Get_DeviceInfo_ServiceRange_Num_of_Service_in_port(MEA_Unit_t unit_i)
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return MEA_ERROR);
	return (MEA_DeviceInfo_Table[hwUnit].service.val.ServiceRange_Num_of_range);
   
}

MEA_Uint32      mea_drv_Get_DeviceInfo_ServiceRange_numofRangeProfile(MEA_Unit_t unit_i)
{
    MEA_Uint32 retval;
    MEA_db_HwUnit_t  hwUnit;
    

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit, return MEA_ERROR); 
    // TBD need read from HW

    retval = MEA_DeviceInfo_Table[hwUnit].service.val.numofRangeProfile; 

   return retval;
 
}


MEA_Bool      mea_drv_Get_DeviceInfo_EnhancePmCounterSupport(MEA_Unit_t unit_i)
{
   if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        return MEA_TRUE;
     }

   return MEA_FALSE;
}


MEA_Status  mea_drv_Get_DeviceInfo_NumOfPmIdsPerType(MEA_Unit_t       unit_i,
                                                     MEA_db_HwUnit_t  hwUnit_i,
                                                     MEA_pm_type_te   pm_type_i,
                                                     MEA_PmId_t      *numOfPmIds_o) 
{

  MEA_Uint32 num0fBlocksForBytes;
  MEA_Uint32 num0fBlocksForPmPackets;


    /* Check hwUnit_i parameter */
    if (hwUnit_i > mea_drv_num_of_hw_units) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - hwUnit_i %d > max value %d \n",
                          __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
        return MEA_ERROR;
    }

    /* Check numOfPmIds_o parameter */
    if (numOfPmIds_o == NULL ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - numOfPmIds_o==NULL \n",__FUNCTION__);
        return MEA_ERROR;
    }
     
//////////////////////////////////////////////////////////////////////////

    num0fBlocksForBytes = 1; //MEA_DeviceInfo_Table[hwUnit_i].pm.val.num0fBlocksForBytes;

#if 0
    num0fBlocksForBytes = MEA_DeviceInfo_Table[hwUnit_i].pm.val.num0fBlocksForBytes;
    num0fBlocksForPmPackets = MEA_DeviceInfo_Table[hwUnit_i].pm.val.num0fBlocksForPmPackets;
#else
    num0fBlocksForBytes = 1; 
    num0fBlocksForPmPackets = 1;
#endif

    if (num0fBlocksForBytes!=0)
    {

        if(num0fBlocksForPmPackets>num0fBlocksForBytes) {   
            num0fBlocksForPmPackets=num0fBlocksForPmPackets/num0fBlocksForBytes;
            num0fBlocksForBytes=num0fBlocksForBytes/num0fBlocksForBytes;
        }else{
            if(num0fBlocksForPmPackets == num0fBlocksForBytes){
                num0fBlocksForPmPackets=0;
                //num0fBlocksForBytes=num0fBlocksForBytes;
            }
        }


    }



    /* Verify num of pm blocks for bytes <= num of pm block for packets */
    if (MEA_DeviceInfo_Table[hwUnit_i].pm.val.num0fBlocksForBytes > 
        MEA_DeviceInfo_Table[hwUnit_i].pm.val.num0fBlocksForPmPackets) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Software not support PM bytes %d >  PM packets %d \n",
                          __FUNCTION__,
                          MEA_DeviceInfo_Table[hwUnit_i].pm.val.num0fBlocksForBytes,
                          MEA_DeviceInfo_Table[hwUnit_i].pm.val.num0fBlocksForPmPackets);
        return MEA_ERROR;
    }


    /* Calculate the *numOfPmIds_o  */
    switch (pm_type_i) {
    case MEA_PM_TYPE_ALL:
        *numOfPmIds_o  = (MEA_PmId_t)num0fBlocksForBytes;
        *numOfPmIds_o *= (MEA_PmId_t)MEA_DEVICE_INFO_PM_BLOCK_SIZE(unit_i,hwUnit_i);
        break;
#if 0
    case MEA_PM_TYPE_PACKET_ONLY:
        
        if(num0fBlocksForPmPackets!=0){
        *numOfPmIds_o  = (MEA_PmId_t)num0fBlocksForPmPackets;
        *numOfPmIds_o -= (MEA_PmId_t)num0fBlocksForBytes;
        }else{
            *numOfPmIds_o=(MEA_PmId_t)num0fBlocksForBytes;
        }
#endif

        *numOfPmIds_o *= MEA_DEVICE_INFO_PM_BLOCK_SIZE(unit_i,hwUnit_i);
        break;

    case MEA_PM_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown pm_type %d \n",
                          __FUNCTION__,pm_type_i);
        return MEA_ERROR;
    }

    /* Return to caller */
    return MEA_OK;
}

MEA_Bool mea_drv_Get_DeviceInfo_WredProfiles_SUPPORT(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i) 
{
    if (MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_wred_prof > 0) {
        return MEA_TRUE;
    }
        return MEA_FALSE;

}



MEA_WredProfileId_t mea_drv_Get_DeviceInfo_GetMaxWredProfiles(MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_i)
{
  MEA_WredProfileId_t value;
     
  if(MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_wred_prof == 0){
    value=1;
  }else{
    value = (MEA_WredProfileId_t)MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_wred_prof;
  }
    
    
    return value; 

}

MEA_AcmMode_t mea_drv_Get_DeviceInfo_GetMaxWredACM(MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_i)
{
    MEA_AcmMode_t value;

    if(MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_wred_acm == 0){
        value=1;
    }else{
        value = (MEA_AcmMode_t)MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_wred_acm;
    }


    return value; 

}

MEA_Uint16 mea_drv_Get_DeviceInfo_Get_ParsernumofBit(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16) MEA_DeviceInfo_Table[hwUnit_i].parser.val.numofBit;
}

MEA_Bool  mea_drv_Get_DeviceInfo_Support_IPV6(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].parser.val.support_IPV6;

}

MEA_Bool            mea_drv_Get_DeviceInfo_ClusterWfqSupport (MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_i) 
{
    return (MEA_DeviceInfo_Table[hwUnit_i].wfq.val.cluster_exist);
}

MEA_Bool            mea_drv_Get_DeviceInfo_PriQWfqSupport    (MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].wfq.val.queue_exist);
}
MEA_Bool mea_drv_Get_DeviceInfo_clusterTo_multiport_Support(MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].wfq.val.clusterTo_multiport_exist);
}



MEA_Bool    mea_drv_Get_DeviceInfo_CFM_OAM_Support(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit_i)
{
   //TBD need bit from HW
    
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        return MEA_TRUE;
     }

   return MEA_FALSE;
}

MEA_Bool    mea_drv_Get_DeviceInfo_CFM_OAM_ME_LvL_FWD_Support(MEA_Unit_t unit_i)
{
   //TBD need bit from HW
    
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        return MEA_TRUE;
     }

   return MEA_FALSE;
}


MEA_Status mea_drv_Get_DeviceInfo_ServiceRange_PortSupport(MEA_Unit_t unit_i, 
                                                           MEA_Port_t port ,
                                                           MEA_Bool *support, 
                                                           MEA_Uint16 *indexStart)
{
    
    MEA_IngressPort_Entry_dbt  IngressPort_Entry;
    /* Check for valid port parameter */
	if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - port %d is invalid\n",
                         __FUNCTION__,port);
       return MEA_ERROR; 
    }
	

   
        *support =MEA_FALSE;
        
        if (MEA_API_Get_IngressPort_Entry(unit_i,
            port,
            &IngressPort_Entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_SetIngressPort_Entry for port %d failed\n",
                    __FUNCTION__,port);
                return MEA_ERROR;
        }

        if(IngressPort_Entry.parser_info.vlanrange_enable == MEA_TRUE){
            *support = MEA_TRUE;
            *indexStart= (MEA_Uint16 )IngressPort_Entry.parser_info.vlanrange_prof_Id;
            return MEA_OK;
        }


   

    return MEA_ERROR;



}


MEA_Bool  mea_drv_Get_DeviceInfo_IsActionMtuSupport(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_DeviceInfo_Table[hwUnit_i].global.val.ac_MTU_support) ;

}
MEA_Bool  mea_drv_Get_DeviceInfo_IsNewAllocSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].global.val.NewAlloc);
}
MEA_Bool  mea_drv_Get_DeviceInfo_dest_vl_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_FALSE;//(MEA_DeviceInfo_Table[hwUnit_i].global.val.dest_vl_support) ;

}
MEA_Bool  mea_drv_Get_DeviceInfo_tdm_act_disable(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_DeviceInfo_Table[hwUnit_i].global.val.tdm_act_disable) ;

}

MEA_Bool mea_drv_Get_DeviceInfo_wbrg_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.wbrg_support;
}




MEA_Bool mea_drv_Get_DeviceInfo_ingress_count(MEA_Unit_t unit_i,
                                                    MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.ingress_count;
}

MEA_Bool mea_drv_Get_DeviceInfo_HeaderCompress_support(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.HeaderCompress_support;
}

MEA_Bool mea_drv_Get_DeviceInfo_HeaderDeCompress_support(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.HeaderDeCompress_support;
}



MEA_Bool mea_drv_Get_DeviceInfo_Desc_external(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.desc_external;
}


MEA_Bool mea_drv_Get_DeviceInfo_Desc_my_mac_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.my_mac_support;
}




MEA_Bool mea_drv_Get_DeviceInfo_pre_sched_enable(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.pre_sched_enable;
}

MEA_Bool    mea_drv_Get_DeviceInfo_pause_support(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i)
{
  
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.pause_support;
}

MEA_Bool mea_drv_Get_DeviceInfo_rx_pause_support(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i){
return MEA_DeviceInfo_Table[hwUnit_i].global.val.rx_pause_support;
}


MEA_Bool mea_drv_Get_DeviceInfo_IsPacketGeneratorSupport(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.PacketGen_exist);
}

MEA_Bool mea_drv_Get_DeviceInfo_IsPacketGeneratorSupportType2(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.Pktgen_type2_num_of_streams ==0) ? MEA_FALSE : MEA_TRUE;
}

static MEA_Bool mea_drv_Get_DeviceInfo_IsPacketAnalyzer_exist(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.Analyzer_exist);
}

MEA_Bool mea_drv_Get_DeviceInfo_IsPacketAnalyzerSupportType1(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i)
{
  
    return mea_drv_Get_DeviceInfo_IsPacketAnalyzer_exist(unit_i, hwUnit_i);
}

MEA_Bool mea_drv_Get_DeviceInfo_IsPacketAnalyzerSupportType2(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i)
{
    return  mea_drv_Get_DeviceInfo_IsPacketAnalyzer_exist(unit_i, hwUnit_i);
   
}


MEA_PacketGen_t mea_drv_Get_DeviceInfo_PacketGen_num_of_streams(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i)
{
  
   
    return (MEA_PacketGen_t)(MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.Analyzer_type1_num_of_streams + 
             MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.Pktgen_type2_num_of_streams);

}

MEA_PacketGen_t mea_drv_Get_DeviceInfo_PacketGen_num_of_streams_Type1(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i)
{
  
   
    return (MEA_PacketGen_t)(MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.Pktgen_type1_num_of_streams);

}
MEA_PacketGen_t mea_drv_Get_DeviceInfo_PacketGen_num_of_streams_Type2(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i)
{
  
   
    return (MEA_PacketGen_t)(MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.Pktgen_type2_num_of_streams);

}



MEA_PacketGen_t mea_drv_Get_DeviceInfo_PacketGen_num_of_profile(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i)
{
    /* T.B.D - Get from fpga */


    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        return  ((MEA_PacketGen_t)MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.PacketGen_num_of_support_profile);  
     }

   return 0;

}

MEA_PacketGen_t mea_drv_Get_DeviceInfo_Analyzer_num_of_streamType1(MEA_Unit_t unit_i,
                                                               MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_PacketGen_t)(MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.Analyzer_type1_num_of_streams);;
}

MEA_PacketGen_t mea_drv_Get_DeviceInfo_Analyzer_num_of_streamType2(MEA_Unit_t unit_i,
                                                               MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_PacketGen_t)(MEA_DeviceInfo_Table[hwUnit_i].packetGen.val.Analyzer_type2_num_of_streams);;
}


MEA_Bool mea_drv_Get_DeviceInfo_IsCamATMExist(MEA_Unit_t unit_i,
                                           MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool)(MEA_DeviceInfo_Table[hwUnit_i].pre_parser.val.cam_exist);
}

MEA_Bool mea_drv_Get_DeviceInfo_IsVspExist(MEA_Unit_t unit_i,
                                           MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool)(MEA_DeviceInfo_Table[hwUnit_i].pre_parser.val.vsp_exist);
}

MEA_Uint32      mea_drv_Get_DeviceInfo_maxClusterPerPort(MEA_Unit_t unit_i)
{
    MEA_db_HwUnit_t  hwUnit; 

    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);


     return  MEA_DeviceInfo_Table[hwUnit].wfq.val.maxCluserperport; 
}



MEA_Bool  mea_drv_Get_DeviceInfo_flowCos_Mapping_Support(MEA_Unit_t unit_i,
                                                MEA_db_HwUnit_t hwUnit_i)
{

return  MEA_DeviceInfo_Table[hwUnit_i].global.val.flowCos_Mapping_exist; 
}

MEA_Bool  mea_drv_Get_DeviceInfo_flowMarking_Mapping_Support(MEA_Unit_t unit_i,
                                                MEA_db_HwUnit_t hwUnit_i)
{

return  MEA_DeviceInfo_Table[hwUnit_i].global.val.flowMarking_Mapping_exist; 
}

MEA_Bool  mea_drv_Get_DeviceInfo_Swap_machine_Support(MEA_Unit_t unit_i,
                                                MEA_db_HwUnit_t hwUnit_i)
{

return  MEA_DeviceInfo_Table[hwUnit_i].global.val.Swap_machine_enable; 
}


MEA_Bool  mea_drv_Get_DeviceInfo_ingress_port_count_support(MEA_Unit_t unit_i,
                                                            MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_FALSE; 
}

MEA_Bool  mea_drv_Get_DeviceInfo_ingress_extended_mac_fifo(MEA_Unit_t unit_i,MEA_Port_t port)
{
    
    MEA_Bool retVal;
     MEA_db_HwUnit_t  hwUnit;
//     MEA_Uint32 *pPortGrp;

    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return MEA_ERROR);

     retVal=MEA_FALSE;

//      pPortGrp=(MEA_Uint32 *)(&MEA_DeviceInfo_Table[hwUnit].port_mac_fifo.val.out_ports_0_31);
//      pPortGrp += (port/32);
// 
// 
//      if((*pPortGrp) & (0x00000001      <<(port%32))){
//      retVal=MEA_TRUE;
//      }
     
     

     
    return retVal; 

}

MEA_Bool  mea_drv_Get_DeviceInfo_bmf_atm_over_pos_support(MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_FALSE;
}


MEA_Uint8  mea_drv_Get_DeviceInfo_RX_mac_fifo(MEA_Unit_t unit_i, MEA_Interface_t InterfaceId){
    MEA_db_HwUnit_t  hwUnit;
    MEA_Uint8 retVal = 0xff;
    MEA_Uint8   interface[32];
    MEA_Uint16 i;
    MEA_Bool  found_interface = MEA_FALSE;
    MEA_Uint32 readBitOffset;

    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return MEA_ERROR);

    MEA_OS_memset(&interface[0], 0xff, sizeof(interface));

    interface[31] = 127;
    interface[30] = 126;
    interface[29] = 125;
    interface[28] = 122;
    interface[27] = 119;
    interface[26] = 118;
    interface[25] = 111;
    interface[24] = 110;
    interface[23] = 109;
    interface[22] = 108;
    interface[21] = 107;
    interface[20] = 106;
    interface[19] = 105;
    interface[18] = 104;
    interface[17] = 103;
    interface[16] = 102;
    interface[15] = 101;
    interface[14] = 100;
    interface[13] = 99;
    interface[12] = 98;
    interface[11] = 97;
    interface[10] = 96;
    interface[9] = 95;
    interface[8] = 94;
    interface[7] = 93;
    interface[6] = 92;
    interface[5] = 72;
    interface[4] = 48;
    interface[3] = 36;
    interface[2] = 24;
    interface[1] = 12;
    interface[0] = 0;

    for (i = 0; i < 32; i++){
        if (InterfaceId == interface[i]){
            found_interface = MEA_TRUE;
            break;
        }
    }

    if (found_interface == MEA_FALSE){
        return retVal;
    }

    readBitOffset = i * 2;

    return    (MEA_Uint8)(MEA_OS_read_value(readBitOffset, 2, &MEA_DeviceInfo_Table[hwUnit].Rx_mac_fifo.index_21_regs[0]));



}


MEA_Uint8  mea_drv_Get_DeviceInfo_TX_mac_fifo(MEA_Unit_t unit_i, MEA_Interface_t InterfaceId){
    MEA_db_HwUnit_t  hwUnit;
    MEA_Uint8 retVal = 0xff;
    MEA_Uint8   interface[32];
    MEA_Uint16 i;
    MEA_Bool  found_interface = MEA_FALSE;
    MEA_Uint32 readBitOffset;

    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return MEA_ERROR);

    MEA_OS_memset(&interface[0], 0xff, sizeof(interface));
    interface[31] = 127;
    interface[30] = 126;
    interface[29] = 125;
    interface[28] = 122;
    interface[27] = 119;
    interface[26] = 118;
    interface[25] = 111;
    interface[24] = 110;
    interface[23] = 109;
    interface[22] = 108;
    interface[21] = 107;
    interface[20] = 106;
    interface[19] = 105;
    interface[18] = 104;
    interface[17] = 103;
    interface[16] = 102;
    interface[15] = 101;
    interface[14] = 100;
    interface[13] = 99;
    interface[12] = 98;
    interface[11] = 97;
    interface[10] = 96;
    interface[9] = 95;
    interface[8] = 94;
    interface[7] = 93;
    interface[6] = 92;
    interface[5] = 72;
    interface[4] = 48;
    interface[3] = 36;
    interface[2] = 24;
    interface[1] = 12;
    interface[0] = 0;

    for (i = 0; i < 32; i++){
        if (InterfaceId == interface[i]){
            found_interface = MEA_TRUE;
            break;
        }
    }

    if (found_interface == MEA_FALSE){
        return retVal;
    }

    readBitOffset = i * 2;

    return    (MEA_Uint8)(MEA_OS_read_value(readBitOffset, 2, &MEA_DeviceInfo_Table[hwUnit].Tx_mac_fifo.index_22_regs[0]));



}

/************************************************************************/
/*          Fragment                                                    */
/************************************************************************/
MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_Edit_num_of_group(MEA_Unit_t unit_i,
                                                       MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].fragment.val.fragment_Edit_num_of_group;
}

MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_Edit_num_of_port(MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].fragment.val.fragment_Edit_num_of_port;
}

MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_Edit_num_of_session(MEA_Unit_t unit_i,
                                                             MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].fragment.val.fragment_Edit_num_of_session;
}
 

MEA_Bool  mea_drv_Get_DeviceInfo_fragment_Edit_Support(MEA_Unit_t unit_i,
                                              MEA_db_HwUnit_t hwUnit_i)
{
return MEA_DeviceInfo_Table[hwUnit_i].fragment.val.fragment_Edit_exist;
}


MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_vsp_num_of_group(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].fragment.val.fragment_vsp_num_of_group;
}
MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_vsp_num_of_port(MEA_Unit_t unit_i,
                                                             MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].fragment.val.fragment_vsp_num_of_port;
}  

MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_vsp_num_of_session(MEA_Unit_t unit_i,
                                                            MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].fragment.val.fragment_vsp_num_of_session;
}


MEA_Bool  mea_drv_Get_DeviceInfo_fragment_vsp_Support(MEA_Unit_t unit_i,
                                                       MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].fragment.val.fragment_vsp_exist; 
}

MEA_Bool  mea_drv_Get_DeviceInfo_fragment_Global_Support(MEA_Unit_t unit_i,
                                              MEA_db_HwUnit_t hwUnit_i)
{

    return  (MEA_DeviceInfo_Table[hwUnit_i].global.val.fragment_exist); 
}

MEA_Bool  mea_drv_Get_DeviceInfo_Dual_Latency_Support(MEA_Unit_t unit_i,
                                              MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].global.val.dual_latency_support; 
}




/*************End Fragment*******************************************************/


MEA_Bool  mea_drv_Get_DeviceInfo_Blocking_Class_Support(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i)
{

    return  1;
}


MEA_Bool  mea_drv_Get_DeviceInfo_IsAcmSupport(MEA_Unit_t unit_i,
                                              MEA_db_HwUnit_t hwUnit_i)
{

    return  (MEA_DeviceInfo_Table[hwUnit_i].global.val.ACM_exist); 
}


MEA_Uint32  mea_drv_Get_DeviceInfo_tx_chunk_size(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
 

    return  (1<<MEA_DeviceInfo_Table[hwUnit_i].global.val.tx_chunk_size);
}



MEA_Uint16  mea_drv_Get_DeviceInfo_NumOfAcmModes(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Uint16 val;
    if(MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_acm == 0){
        val = 1;
    } else {
        val = (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_acm );
    }

    return  val; 
}

MEA_Uint16  mea_drv_Get_DeviceInfo_NumOfAcmHiddenProfiles(MEA_Unit_t unit_i,
                                                          MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Uint16) (MEA_DeviceInfo_Table[hwUnit_i].policer.val.hidden_profile); 
}
MEA_Uint16  mea_drv_Get_DeviceInfo_NumOfPolicerProfiles(MEA_Unit_t unit_i,
                                                          MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Uint16) (MEA_DeviceInfo_Table[hwUnit_i].policer.val.num_of_profile); 
}
MEA_Bool mea_drv_Get_DeviceInfo_afdx_Support(MEA_Unit_t unit_i,
                                             MEA_db_HwUnit_t hwUnit_i)
{
  return  (MEA_DeviceInfo_Table[hwUnit_i].global.val.afdx_support); 
}

MEA_Bool  mea_drv_Get_DeviceInfo_Fragment_Bonding_Support(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i)
{

    return  (MEA_DeviceInfo_Table[hwUnit_i].global.val.fragment_bonding); 
}

MEA_Bool  mea_drv_Get_DeviceInfo_CPE_Bonding(MEA_Unit_t unit_i,
                                             MEA_db_HwUnit_t hwUnit_i)
{

    return  (MEA_DeviceInfo_Table[hwUnit_i].global.val.cpe_exist); 
}




MEA_Bool mea_drv_Get_DeviceInfo_pw_control_Edit_Support(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i)
{
    return  MEA_FALSE;  //MEA_DeviceInfo_Table[hwUnit_i].global.val.pw_enable;
}

MEA_Bool mea_drv_Get_DeviceInfo_forwarder_State_show(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].forwarder.val.show_all;
}

MEA_Bool mea_drv_Get_DeviceInfo_large_BM_ind_address(MEA_Unit_t unit_i,
                                                     MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.large_BM_ind_address;

}

MEA_Bool mea_drv_Get_DeviceInfo_new_parser_Support(MEA_Unit_t unit_i,
                                                     MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_TRUE;

}
MEA_Bool mea_drv_Get_DeviceInfo_new_parser_reduce(MEA_Unit_t unit_i,
                                            MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_TRUE; 

}


MEA_Bool mea_drv_Get_DeviceInfo_L2TP_enable(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.L2TP_enable;

}

MEA_Bool mea_drv_Get_DeviceInfo_GRE_enable(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.GRE_enable;

}




MEA_Bool mea_drv_Get_DeviceInfo_BFD_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.BFD_support;

}

MEA_Bool mea_drv_Get_DeviceInfo_cls_large_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.cls_large;

}
MEA_Bool mea_drv_Get_DeviceInfo_1588_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.Support_1588;
     
}
MEA_Bool mea_drv_Get_DeviceInfo_Protect_1p1_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.protect_1p1_suppport;

}
MEA_Uint16 mea_drv_Get_DeviceInfo_BM_bus_width(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].global.val.BM_bus_width);

}

MEA_Bool mea_drv_Get_DeviceInfo_ingressMacfifo_small(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool)(MEA_DeviceInfo_Table[hwUnit_i].global.val.ingressMacfifo_small);
}



MEA_Bool mea_drv_Get_DeviceInfo_ProtocolMapping_Support(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.protocolMapping;
}






MEA_Bool mea_drv_Get_DeviceInfo_AlloC_AllocR(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.AlloC_AllocR;
}


MEA_Bool mea_drv_Get_DeviceInfo_Ip_sec_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.ip_sec_support;
}

MEA_Bool mea_drv_Get_DeviceInfo_NVGRE_enable(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return MEA_DeviceInfo_Table[hwUnit_i].global.val.NVGRE_enable;
}

MEA_Bool mea_drv_Get_DeviceInfo_RmonRx_drop_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.rmonRx_drop_support;
}



MEA_Bool mea_drv_Get_DeviceInfo_RmonRx_IL_drop_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.rmonRx_IL_drop_support;
}


MEA_Bool mea_drv_Get_DeviceInfo_fwd_filter_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.forworder_filter_support;
}
MEA_Bool mea_drv_Get_DeviceInfo_autoneg_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.autoneg_support;
}

MEA_Bool mea_drv_Get_DeviceInfo_GATEWAY_EN_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.GATEWAY_EN;
}



MEA_Bool mea_drv_Get_DeviceInfo_MAC_LB_Support(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i){
 return MEA_DeviceInfo_Table[hwUnit_i].global.val.MAC_LB_support;

}
#if 0
MEA_Bool  mea_drv_Get_DeviceInfo_serdes_LB_Internal_PortSupport(MEA_Unit_t unit_i,MEA_Port_t port)
{

    MEA_Bool retVal;
    MEA_db_HwUnit_t  hwUnit;
    MEA_Uint32 *pPortGrp;

    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return MEA_ERROR);

    retVal=MEA_FALSE;

    pPortGrp=(MEA_Uint32 *)(&MEA_DeviceInfo_Table[hwUnit].serdes_lb.val.serdes_internal_ports_lb_0_31);
    pPortGrp += (port/32);


    if((*pPortGrp) & (0x00000001      <<(port%32))){
        retVal=MEA_TRUE;
    }




    return retVal; 

}

MEA_Bool  mea_drv_Get_DeviceInfo_serdes_LB_External_PortSupport(MEA_Unit_t unit_i,MEA_Port_t port)
{

    MEA_Bool retVal;
    MEA_db_HwUnit_t  hwUnit;
    MEA_Uint32 *pPortGrp;

    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return MEA_ERROR);

    retVal=MEA_FALSE;

    pPortGrp=(MEA_Uint32 *)(&MEA_DeviceInfo_Table[hwUnit].serdes_lb.val.serdes_external_ports_lb_0_31);
    pPortGrp += (port/32);


    if((*pPortGrp) & (0x00000001      <<(port%32))){
        retVal=MEA_TRUE;
    }




    return retVal; 

}
#endif


MEA_Bool  mea_drv_Get_DeviceInfo_MII_reduce_PortSupport(MEA_Unit_t unit_i,MEA_Port_t port)
{

    MEA_Bool retVal;
    MEA_db_HwUnit_t  hwUnit;
    MEA_Uint32 *pPortGrp;

    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return MEA_ERROR);

    retVal=MEA_FALSE;

    pPortGrp=(MEA_Uint32 *)(&MEA_DeviceInfo_Table[hwUnit].mac_mii_support.val.ports_mii_0_31);
    pPortGrp += (port/32);


    if((*pPortGrp) & (0x00000001      <<(port%32))){
        retVal=MEA_TRUE;
    }




    return retVal; 

}

MEA_Uint32 mea_drv_Get_DeviceInfo_numof_SER_TEID(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i)
{


    return MEA_DeviceInfo_Table[hwUnit_i].hpm.val.SER_TeId;

}



MEA_Bool mea_drv_Get_DeviceInfo_Utopia_Support(MEA_Unit_t unit_i,
                                               MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_FALSE; 
}
MEA_Bool mea_drv_Get_DeviceInfo_st_contx_mac_support(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return 1;
}
MEA_Bool mea_drv_Get_DeviceInfo_fragment_9991_Support(MEA_Unit_t unit_i,
											      MEA_db_HwUnit_t hwUnit_i)
{
	return MEA_DeviceInfo_Table[hwUnit_i].global.val.fragment_9991;
}



MEA_Bool mea_drv_Get_DeviceInfo_SerdesReset_Support(MEA_Unit_t unit_i,
                                                  MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.serdesReset_support;
}



MEA_Bool mea_drv_Get_DeviceInfo_standard_bonding_support(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.standard_bonding_support;
}

MEA_Uint16 mea_drv_Get_DeviceInfo_standard_bonding_num_of_groupTx(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i)
{
    if(mea_drv_Get_DeviceInfo_standard_bonding_support(unit_i, hwUnit_i) == MEA_FALSE)
        return 0;
    else
        return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].global.val.num_bonding_groupTx);
}

MEA_Uint16 mea_drv_Get_DeviceInfo_standard_bonding_num_of_groupRx(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i)
{
    if(mea_drv_Get_DeviceInfo_standard_bonding_support(unit_i, hwUnit_i) == MEA_FALSE)
        return 0;
    else
        return (MEA_Uint16)(MEA_DeviceInfo_Table[hwUnit_i].global.val.num_bonding_groupRx);
}



MEA_Uint32 mea_drv_Get_chip_number()
{
    MEA_Uint32 infoId;
    MEA_db_HwUnit_t hwUnit_i = 0;

    infoId = 0;
    infoId  = MEA_DeviceInfo_Table[hwUnit_i].global.val.chip_name   * 1000;
    infoId += MEA_DeviceInfo_Table[hwUnit_i].global.val.sub_family  * 100;
    infoId += MEA_DeviceInfo_Table[hwUnit_i].global.val.package     * 10;
    infoId += MEA_DeviceInfo_Table[hwUnit_i].global.val.speed_grade  * 1;


    return infoId;

}

static MEA_Status mea_drv_Get_DeviceInfo_GetChipName(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

  MEA_Uint32 infoId;
  MEA_Bool   flagfound=MEA_FALSE;
  

  if(unit_i != 0)  // currently we support on unit so the relevant is unit 0 
      return MEA_ERROR;

  if(hwUnit_i != 0)  // currently we support on unit so the relevant is hwUnit_i 0 
      return MEA_ERROR;
  



  infoId = mea_drv_Get_chip_number();
  MEA_OS_memset(&MEA_chip_name, 0, sizeof(MEA_chip_name));

    MEA_OS_strcat(MEA_chip_name,"Chip name:");
    if(infoId==0){
        MEA_OS_strcat(MEA_chip_name,"---");
    }
    else {

        if (infoId >= MEA_CHIP_NAME_Cyclone_II_ID && infoId < MEA_CHIP_NAME_Cyclone_II_ID + 1000) {
            flagfound = MEA_TRUE;
            MEA_OS_strcat(MEA_chip_name, "Cyclone-II-");
        }
        if (infoId >= MEA_CHIP_NAME_Stratix_II_ID && infoId < MEA_CHIP_NAME_Stratix_II_ID + 1000) {
            flagfound = MEA_TRUE;
            MEA_OS_strcat(MEA_chip_name, "Stratix-II-");
        }
        if (infoId >= MEA_CHIP_NAME_Cyclone_III_ID && infoId < MEA_CHIP_NAME_Cyclone_III_ID + 1000) {
            flagfound = MEA_TRUE;
            MEA_OS_strcat(MEA_chip_name, "Cyclone-III-");
            if (infoId == 3100)
                MEA_OS_strcat(MEA_chip_name, "80");
            if (infoId == 3100)
                MEA_OS_strcat(MEA_chip_name, "120");

        }

        if (infoId >= MEA_CHIP_NAME_Spartan6_ID && infoId < MEA_CHIP_NAME_Spartan6_ID + 1000) {
            flagfound = MEA_TRUE;
            MEA_OS_strcat(MEA_chip_name, "Spartan6-");
            if (infoId == 6211)
                MEA_OS_strcat(MEA_chip_name, "lx75t-fgg484-2");
            if (infoId == 6310)
                MEA_OS_strcat(MEA_chip_name, "lx100t-fgg484-0");
            if (infoId == 6311)
                MEA_OS_strcat(MEA_chip_name, "lx100t-fgg484-2");
            if (infoId == 6312)
                MEA_OS_strcat(MEA_chip_name, "lx100t-fgg484-3");
            /**/
            if (infoId == 6320)
                MEA_OS_strcat(MEA_chip_name, "lx100t-fgg676-0");
            if (infoId == 6321)
                MEA_OS_strcat(MEA_chip_name, "lx100t-fgg676-2");
            if (infoId == 6322)
                MEA_OS_strcat(MEA_chip_name, "lx100t-fgg676-3");

            if (infoId == 6410)
                MEA_OS_strcat(MEA_chip_name, "lx150t-fgg484-0");
            if (infoId == 6411)
                MEA_OS_strcat(MEA_chip_name, "lx150t-fgg484-2");
            if (infoId == 6412)
                MEA_OS_strcat(MEA_chip_name, "lx150t-fgg484-3");
            /**/
            if (infoId == 6420)
                MEA_OS_strcat(MEA_chip_name, "lx150t-fgg676-0");
            if (infoId == 6421)
                MEA_OS_strcat(MEA_chip_name, "lx150t-fgg676-2");
            if (infoId == 6422)
                MEA_OS_strcat(MEA_chip_name, "lx150t-fgg676-3");

            if (infoId == 6430)
                MEA_OS_strcat(MEA_chip_name, "lx150t-fgg900-0");
            if (infoId == 6431)
                MEA_OS_strcat(MEA_chip_name, "lx150t-fgg900-2");
            if (infoId == 6432)
                MEA_OS_strcat(MEA_chip_name, "lx150t-fgg900-3");

            if (infoId == 6510)
                MEA_OS_strcat(MEA_chip_name, "lx100t-csg484-0");
            if (infoId == 6511)
                MEA_OS_strcat(MEA_chip_name, "lx100t-csg484-2");
            if (infoId == 6512)
                MEA_OS_strcat(MEA_chip_name, "lx100t-csg484-3");

        }



        if (infoId >= MEA_CHIP_NAME_Kintex7_ID && infoId < MEA_CHIP_NAME_Kintex7_ID + 1000) {
            flagfound = MEA_TRUE;
            MEA_OS_strcat(MEA_chip_name, "Kintex7-");
            if ((infoId) >= 7100 && (infoId < 7200))
                MEA_OS_strcat(MEA_chip_name, "xc7k325-");
            if (infoId == 7111)
                MEA_OS_strcat(MEA_chip_name, "ffg900-2");
            if (infoId == 7121)
                MEA_OS_strcat(MEA_chip_name, "tffg676-2");
            
            if ((infoId) >= 7200 && (infoId < 7300)) 
            {
                MEA_OS_strcat(MEA_chip_name, "k410t-");
                if (infoId == 7211)
                    MEA_OS_strcat(MEA_chip_name, "ffg900-2");
                if (infoId == 7221)
                    MEA_OS_strcat(MEA_chip_name, "tffg676-2");
            }


        }
        if ((infoId >= MEA_CHIP_NAME_Zynq7_ID) && 
            (infoId < (MEA_CHIP_NAME_Zynq7035i_ID_UP))) 
        {
            flagfound = MEA_TRUE;
            MEA_OS_strcat(MEA_chip_name, "Zynq7-");
            if ((infoId >= MEA_CHIP_NAME_Zynq7045_ID) && 
                (infoId < MEA_CHIP_NAME_Zynq7045_ID_UP)) 
            {
                MEA_OS_strcat(MEA_chip_name, "xc7z045-");
                if (infoId >= 8110 && infoId <= 8119) {
                    MEA_OS_strcat(MEA_chip_name, "ffg900-");
                    if (infoId == 8111) {
                        MEA_OS_strcat(MEA_chip_name, "2");
                    }
                    if (infoId == 8112) {
                        MEA_OS_strcat(MEA_chip_name, "3");
                    }
                }
                if (infoId >= 8120 && infoId <= 8129) {
                    MEA_OS_strcat(MEA_chip_name, "ffg676-");
                    if (infoId == 8121) {
                        MEA_OS_strcat(MEA_chip_name, "2");
                    }
                    if (infoId == 8122) {
                        MEA_OS_strcat(MEA_chip_name, "-3");
                    }
                }

            }
            if ((infoId >= MEA_CHIP_NAME_Zynq7015i_ID) && 
                (infoId < MEA_CHIP_NAME_Zynq7015i_ID_UP)) 
            {
                MEA_OS_strcat(MEA_chip_name, "xc7z015i-");
                if (infoId >= 8210 && infoId <= 8219) {
                    MEA_OS_strcat(MEA_chip_name, "clg485-");
                }
                if (infoId == 8211) {
                    MEA_OS_strcat(MEA_chip_name, "1");
                }

            }
            if ((infoId >= MEA_CHIP_NAME_Zynq7030i_ID) && 
                (infoId < MEA_CHIP_NAME_Zynq7030i_ID_UP)) {
                MEA_OS_strcat(MEA_chip_name, "xc7z030i-");
                if (infoId >= 8310 && infoId <= 8319) {
                    MEA_OS_strcat(MEA_chip_name, "sbg485-");
                    if (infoId == 8311) {
                        MEA_OS_strcat(MEA_chip_name, "1");
                    }
                    if (infoId == 8312) {
                        MEA_OS_strcat(MEA_chip_name, "2");
                    }
                    if (infoId == 8313) {
                        MEA_OS_strcat(MEA_chip_name, "3");
                    }
                }
                if (infoId >= 8320 && infoId <= 8329) {
                    MEA_OS_strcat(MEA_chip_name, "ffg900-");
                    if (infoId == 8321) {
                        MEA_OS_strcat(MEA_chip_name, "1");
                    }
                    if (infoId == 8322) {
                        MEA_OS_strcat(MEA_chip_name, "2");
                    }
                    if (infoId == 8323) {
                        MEA_OS_strcat(MEA_chip_name, "3");
                    }
                }


            }
            if (infoId >= MEA_CHIP_NAME_Zynq7035i_ID &&
                infoId < MEA_CHIP_NAME_Zynq7035i_ID_UP)
            {
                MEA_OS_strcat(MEA_chip_name, " z7035-");
                if (infoId >= 8410 && infoId <= 8419) {
                    MEA_OS_strcat(MEA_chip_name, "ffg676-");
                    if (infoId == 8411) {
                        MEA_OS_strcat(MEA_chip_name, "2");
                    }
                    if (infoId == 8412) {
                        MEA_OS_strcat(MEA_chip_name, "3");
                    }
                    if (infoId == 8413) {
                        MEA_OS_strcat(MEA_chip_name, "4");
                    }
                }
                if (infoId >= 8420 && infoId <= 8429) {
                    MEA_OS_strcat(MEA_chip_name, "ffg900-");
                    if (infoId == 8421) {
                        MEA_OS_strcat(MEA_chip_name, "1");
                    }
                    if (infoId == 8422) {
                        MEA_OS_strcat(MEA_chip_name, "2");
                    }
                    if (infoId == 8423) {
                        MEA_OS_strcat(MEA_chip_name, "3");
                    }
                    if (infoId >= 8420 && infoId <= 8429) {
                        MEA_OS_strcat(MEA_chip_name, "ffg900-");
                        if (infoId == 8421) {
                            MEA_OS_strcat(MEA_chip_name, "1");
                        }
                        if (infoId == 8422) {
                            MEA_OS_strcat(MEA_chip_name, "2");
                        }
                        if (infoId == 8423) {
                            MEA_OS_strcat(MEA_chip_name, "3");
                        }
                    }


                }
                if (infoId >= 8440 && infoId <= 8449) {
                    MEA_OS_strcat(MEA_chip_name, "ffg676-");
                    if (infoId == 8441) {
                        MEA_OS_strcat(MEA_chip_name, "2");
                    }

                }
            }


        }
		
		if (infoId >= MEA_CHIP_NAME_ZynqUS9_ID && infoId < MEA_CHIP_NAME_ZynqUS9_ID + 1000) {
			MEA_OS_strcat(MEA_chip_name, "xczu9eg-");
			if ((infoId) >= 10111 && (infoId < 10119))
			{
				MEA_OS_strcat(MEA_chip_name, "ffg900-2");
				if (infoId == 10111)
					MEA_OS_strcat(MEA_chip_name, "-i");
				
			}
		}
    }
    if (flagfound == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "chip_name not found   %d\n", infoId);
    }





    return MEA_OK;
}

MEA_Status MEA_API_Get_ChipName(char*      buffer, MEA_Uint32 len) {

  if (buffer == NULL) {
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - buffer == NULL\n",__FUNCTION__);
     return MEA_ERROR;
  }

  if (len < sizeof(MEA_chip_name) ) {
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - len %d is too small of \n",__FUNCTION__,len);
     return MEA_ERROR;
  } 

  MEA_OS_strcpy(buffer,MEA_chip_name);

  return MEA_OK;
}



MEA_Uint32  mea_drv_Get_DeviceInfo_EPC_max_VPLS_FLOOD()
{

    if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
        return  MEA_VPLS_MAX_OF_FLOODING_Z45;
    }
    if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
        return MEA_VPLS_MAX_OF_FLOODING_Z15;
    }

    return  MEA_VPLS_MAX_OF_FLOODING_Z15;

}

MEA_Uint16  mea_drv_Get_DeviceInfo_EPC_max_VPLS_Instance()
{

    if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
        return  MEA_VPLS_MAX_OF_INSTANCE_Z45;
    }
    if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
        return MEA_VPLS_MAX_OF_INSTANCE_Z15;
    }

    return MEA_VPLS_MAX_OF_INSTANCE_Z15;
}




MEA_Uint16 mea_drv_Get_DeviceInfo_EPC_max_VPLS_OF_USER_ON_GROUP()
{
    MEA_Uint32 numOfcluster;
    numOfcluster = mea_drv_Get_DeviceInfo_maxClusterPerPort(MEA_UNIT_0);
    
    if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
        return  MEA_VPLS_MAX_OF_USER_ON_GROUP_Z45;
    }
    if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
        if (MEA_VPLS_MAX_OF_USER_ON_GROUP_Z15 > numOfcluster) {
            return numOfcluster;
        }
        else {
            return MEA_VPLS_MAX_OF_USER_ON_GROUP_Z15;
        }
    }

    return numOfcluster;

}
MEA_Uint16 mea_drv_Get_DeviceInfo_EPC_max_VPLS_OF_CY_GROUP()
{

    MEA_Uint32 numOfcluster;
    numOfcluster = mea_drv_Get_DeviceInfo_maxClusterPerPort(MEA_UNIT_0);

    if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
        return  MEA_VPLS_MAX_OF_CY_GROUP_Z45;
    }
    if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {

        if (MEA_VPLS_MAX_OF_CY_GROUP_Z15 > numOfcluster) {
            return (MEA_Uint16)numOfcluster;
        }
        else {
            return MEA_VPLS_MAX_OF_CY_GROUP_Z15;
        }

    }

    return MEA_VPLS_MAX_OF_CY_GROUP_Z15;

}



MEA_Uint16 mea_drv_Get_DeviceInfo_Interface_Id(MEA_Unit_t unit_i)
{
    /*not support */
    return 0; 
}


MEA_Bool mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(MEA_Unit_t unit_i,MEA_Uint8  type)
{

    MEA_db_HwUnit_t hwUnit_i;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit_i, return MEA_FALSE);

    switch (type)
    {
    case MEA_DB_ACTION_TYPE:
            return  MEA_DeviceInfo_Table[hwUnit_i].global1.val.ACTION_ON_DDR;
        break;
    case MEA_DB_EDITING_TYPE:
            return  MEA_DeviceInfo_Table[hwUnit_i].global1.val.EDITING_ON_DDR;
            break;
    case MEA_DB_PM_TYPE:
            return  MEA_DeviceInfo_Table[hwUnit_i].global1.val.PM_ON_DDR;
        break;
    case MEA_DB_SERVICE_EXTERNAL_TYPE:
        return  MEA_DeviceInfo_Table[hwUnit_i].global1.val.SERVICE_EXTERNAL;
        break;
        default:
            return MEA_FALSE;
            break;
    }
    
    return MEA_FALSE;
}

MEA_Bool mea_drv_Get_DeviceInfo_Get_type_DB_DDR(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{
    return  MEA_DeviceInfo_Table[hwUnit_i].global1.val.type_DB_DDR;
}



MEA_Bool mea_drv_Get_DeviceInfo_Get_Port_new_setting(MEA_Unit_t unit_i,
                                               MEA_db_HwUnit_t hwUnit_i)
{
    return  1;
}

MEA_Bool mea_drv_Get_DeviceInfo_Get_mtu_pri_queue_reduce(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{
    return  MEA_DeviceInfo_Table[hwUnit_i].global.val.mtu_pri_queue_reduce ;
}


/************************************************************************/
/* TDM                                                                  */
/************************************************************************/
MEA_Bool mea_drv_Get_DeviceInfo_TDM_Support(MEA_Unit_t unit_i,
                                            MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_DeviceInfo_Table[hwUnit_i].global.val.tdm_enable;
}
MEA_TdmCesId_t mea_drv_Get_DeviceInfo_TDM_num_of_CES(MEA_Unit_t unit_i,
                                                     MEA_db_HwUnit_t hwUnit_i)
{

	return (MEA_TdmCesId_t)MEA_DeviceInfo_Table[hwUnit_i].global_Tdm.val.num_of_ces;
}

MEA_Bool mea_drv_Get_DeviceInfo_TDM_Interface_X_Support(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i,MEA_Uint8 interfaceId)
{
    /*check the key*/
   
    if(interfaceId == 0 || interfaceId > 3){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - interfaceId %d is invalid\n",
            __FUNCTION__,interfaceId);
    return MEA_FALSE;
    }

    if(interfaceId == 1)
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_A.val.exist;
    if(interfaceId == 2)
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_B.val.exist;
    if(interfaceId == 3)
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_C.val.exist;
    
    return MEA_FALSE;

}



MEA_Uint16 mea_drv_Get_DeviceInfo_TDM_Interface_Type(MEA_Unit_t unit_i,
                                                     MEA_db_HwUnit_t hwUnit_i,MEA_Uint8 interfaceId)
{
    if(interfaceId == 0){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - interfaceId %d is invalid\n",
            __FUNCTION__,interfaceId);
     return 0;
    }

    if(interfaceId == 1)
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_A.val.type;
    if(interfaceId == 2)
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_B.val.type;
    if(interfaceId == 3)
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_C.val.type;
   return 0; //MEA_TDM_INTERFACE_NONE;
}

MEA_Uint16 mea_drv_Get_DeviceInfo_TDM_Interface_num0fspe(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i,MEA_Uint8 interfaceId)
{
    if(interfaceId == 0){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - interfaceId %d is invalid\n",
            __FUNCTION__,interfaceId);
       return 0;
    }

    if(interfaceId == 1)
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_A.val.num0fspe;
    if(interfaceId == 2)
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_B.val.num0fspe;
    if(interfaceId == 3)
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_C.val.num0fspe;
return 0;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_TDM_Interface_start_ts(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i,MEA_Uint8 interfaceId)
{
    MEA_Uint16 val=1;

    if(interfaceId == 0){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - interfaceId %d is invalid\n",
            __FUNCTION__,interfaceId);
    }
    if(mea_drv_Get_DeviceInfo_TDM_mode_Satop(unit_i,hwUnit_i) == MEA_TRUE){
    val=1;
    }else{
        val=32;
    }

    if(interfaceId == 1){
       return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_A.val.start_ts * val;
        
    }
    if(interfaceId == 2){
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_B.val.start_ts * val;
        
    }
    if(interfaceId == 3){
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Interface_Tdm_C.val.start_ts * val ;
        
    }
    else
       return 0;
}

MEA_Uint16 mea_drv_Get_DeviceInfo_TDM_num_of_Ces(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i)
{
return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].global_Tdm.val.num_of_ces ;
}

MEA_Bool mea_drv_Get_DeviceInfo_TDM_mode_Satop(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i)
{
    return (MEA_Bool)MEA_DeviceInfo_Table[hwUnit_i].global_Tdm.val.mode_satop_only ;
}

MEA_Uint16 mea_drv_Get_DeviceInfo_TDM_ts_size(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i)
{
    if(MEA_DeviceInfo_Table[hwUnit_i].global_Tdm.val.mode_satop_only == 0){
        return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].global_Tdm.val.ts_size *32;
    }else{
         return (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].global_Tdm.val.ts_size ;
    }
}
/******** End TDM***************************/

/*Hc_Classifier */

MEA_Bool mea_drv_Get_DeviceInfo_Hc_Classifier_exist(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

   return  MEA_DeviceInfo_Table[hwUnit_i].Hc_Classifier.val.Hc_classifier_exist;
}


MEA_Bool mea_drv_Get_DeviceInfo_Hc_Classifier_Rang_exist(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].Hc_Classifier.val.Hc_classifierRang_exist;
}
MEA_Bool mea_drv_Get_DeviceInfo_Hc_rmon_exist(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].Hc_Classifier.val.Hc_rmon_exist;
}


MEA_Uint16  mea_drv_Get_DeviceInfo_Hc_Classifier_Num_of_hcid(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return  (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Hc_Classifier.val.Num_of_hcid;
}
 
MEA_Uint16  mea_drv_Get_DeviceInfo_Hc_Classifier_Hc_number_of_hash_groups(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return  (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Hc_Classifier.val.Hc_number_of_hash_groups;
}

MEA_Uint16  mea_drv_Get_DeviceInfo_Hc_Classifier_Hc_classifier_key_width(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return  (MEA_Uint16)MEA_DeviceInfo_Table[hwUnit_i].Hc_Classifier.val.Hc_classifier_key_width;
}
/************************************************************************/
/* PTP1588                                                               */
/************************************************************************/      

MEA_Bool mea_drv_Get_DeviceInfo_PTP1588_exist(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].global.val.PTP1588_support;
}

MEA_Bool mea_drv_Get_DeviceInfo_Isvpls_access_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].global.val.vpls_access_enable;
}

MEA_Bool mea_drv_Get_DeviceInfo_Isvpls_chactristic_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].global.val.vpls_chactristic_enable;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION 
static MEA_Status mea_drv_DeviceInfo_Set_info(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i,MEA_DeviceInfo_dbt*entry)
{
MEA_Uint32 num_of_forwarder_Action;
MEA_Uint32 num_of_edit;
MEA_Uint32 NumOfPmIds;
MEA_Uint32 numof_hpmAction = 0;


MEA_Uint32 Type=0;
    // this function is for simulation 
    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry == NULL\n",
            __FUNCTION__);
    }

    if(hwUnit_i != 0){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not support hwUnit_i = %d\n",
            __FUNCTION__,hwUnit_i);
    }
#if defined(MEA_OS_DEVICE_SIMULATION_DEF)
    Type = ((MEA_Uint32)MEA_OS_DEVICE_SIMULATION_DEF);
#endif
    

    switch (Type)
    {

    case MEA_OS_DEVICE_SIMULATION_3700PW:
#if 1
        entry->global.val.encription_enable            = MEA_TRUE;        
        entry->global.val.protection_enable            = MEA_FALSE;
        entry->global.val.bmf_martini_support          = MEA_TRUE; 
        
        
        entry->global.val.forwarder_external           = MEA_TRUE;
        entry->global.val.desc_external                = MEA_FALSE;
        entry->global.val.desc_checker_exist           = MEA_FALSE;
        entry->global.val.host_reduce                  = MEA_TRUE;
        entry->global.val.pause_support                = MEA_FALSE;
        entry->global.val.pre_sched_enable             = MEA_TRUE;

        entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
        entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;


        entry->global.val.fragment_exist               = MEA_FALSE;

        entry->global.val.ACM_exist                    = MEA_TRUE;
        entry->global.val.dual_latency_support         = MEA_TRUE;
        entry->global.val.fragment_bonding             = MEA_FALSE;
        entry->global.val.cpe_exist                    = MEA_TRUE;
      
        entry->global.val.tdm_enable                   = MEA_TRUE;
        entry->global.val.large_BM_ind_address         = MEA_FALSE;
       
        entry->global.val.protocolMapping              = MEA_FALSE;
        entry->global.val.cls_large                    = MEA_TRUE;
        entry->global.val.Support_1588                 = MEA_FALSE;
        entry->global.val.BM_bus_width                 = 0;  /* 0-64bit 01-128bit 10-256 11 TBD */
        entry->global.val.mc_mqs_Support               = MEA_FALSE;
        entry->global.val.cluster_adminon_support      = MEA_FALSE;
        



        
       
        
        /*only for ATM mode*/
        entry->pre_parser.val.exist                    = MEA_FALSE;
        entry->pre_parser.val.table_size               = 0; //TBD
        entry->pre_parser.val.vsp_exist                = MEA_FALSE;

               entry->parser.val.exist                      = MEA_TRUE;
        entry->parser.val.parser_etype_cam           = MEA_FALSE; 
        entry->parser.val.table_size                 = 128; 

        entry->service.val.exist                     = MEA_TRUE;
        entry->service.val.table_size                = 1024;
        entry->service.val.serviceRangeExist         = MEA_FALSE;
        entry->service.val.num_service_hash          = 3;
        entry->service.val.ServiceRange_Num_of_range     = 0;

        entry->acl.val.exist                         = MEA_TRUE;  
        entry->acl.val.table_size                    = 256;

        entry->acl.val.lxcp_act_width                 = 0; // 2^8 is 256 Action
        entry->acl.val.lxcp_prof_width                = 0; // 2^4 is 16 profile


        entry->forwarder_lmt.val.exist               = MEA_TRUE;
        entry->forwarder_lmt.val.table_size          = 128;

       
        entry->forwarder.val.exist               = MEA_FALSE;
        entry->forwarder.val.addres_size         = 23;      
        entry->forwarder.val.adm_support         = MEA_TRUE;
        num_of_forwarder_Action                 =   512;
        entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
        
        entry->forwarder.val.width_learn_action_id  =     10;
        entry->forwarder.val.width_learn_src_port =        7;   
 

        entry->shaper.val.num_of_shaper_profile=64;  // for port and priQue and cluster
        entry->shaper.val.shaper_per_Priqueue_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_cluster_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_port_exist=MEA_TRUE;

       
        entry->policer.val.exist                 = MEA_FALSE; 

        entry->policer.val.reduce                = MEA_FALSE;

        entry->policer.val.num_of_TMID = 512;
        entry->policer.val.hidden_profile        = 0;//3;
        entry->policer.val.num_of_acm            = 0;//8;   // 1 when no ACM
        entry->policer.val.num_of_profile        = 64;
        entry->policer.val.num_of_wred_prof      = 0; //8;
        entry->policer.val.num_of_wred_acm       = 0;
        
        
        
        entry->wfq.val.cluster_exist             = MEA_FALSE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
        entry->wfq.val.queue_exist               = MEA_FALSE;
        entry->wfq.val.num_of_cluster                = 128;  // num of cluster
        entry->wfq.val.maxCluserperport          = 32;    //  32 if the HW support
        entry->wfq.val.clusterTo_multiport_exist  = MEA_TRUE; // only on bonding

        

        entry->action.val.exist                  = MEA_TRUE;
        entry->action.val.table_size             = 1024+256; 

        
        
        entry->xpermission.val.exist             = MEA_TRUE;
        entry->xpermission.val.xper_table_size = 512;
        
        
        
        entry->sar_machine.val.exist             = MEA_FALSE;
        entry->sar_machine.val.table_size        = 0;   
        
        
        entry->editor_BMF.val.atm_support           = MEA_FALSE;
        entry->editor_BMF.val.maritini_mac_support  = MEA_TRUE;
        
        entry->editor_BMF.val.router_support        = MEA_FALSE;
                

        num_of_edit                    = 512;
        entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);

        entry->editor_BMF.val.num_of_lm_count       = 16;
        
        
        NumOfPmIds     = 512;
        entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
        
        entry->pm.val.num0fBlocksForBytes      = 1;
        entry->pm.val.num0fBlocksForPmPackets  = 1;
       
        

       
        entry->packetGen.val.PacketGen_exist         = MEA_TRUE;
        entry->packetGen.val.Analyzer_exist          = MEA_TRUE;
        entry->packetGen.val.PacketGen_On_Port_num   = 120;
        entry->packetGen.val.Analyzer_On_Port_num    = 120;
        entry->packetGen.val.Analyzer_type1_num_of_streams  = 16;//16; /*lbm lbr*/
        entry->packetGen.val.Analyzer_type2_num_of_streams  = 16; /*ccm*/
        entry->packetGen.val.PacketGen_num_of_support_profile = 3;
        entry->packetGen.val.Pktgen_type1_num_of_streams = 0;
        entry->packetGen.val.Pktgen_type2_num_of_streams = 16;
        entry->packetGen.val.Analyzer_num_of_support_profile = 8;//buckets

        
        
        entry->fragment.val.fragment_Edit_exist           = MEA_FALSE;
        entry->fragment.val.fragment_Edit_num_of_session  = 512;
        entry->fragment.val.fragment_Edit_num_of_port     = 1;
        entry->fragment.val.fragment_Edit_num_of_group    = 1;

        entry->fragment.val.fragment_vsp_exist           = MEA_TRUE;
        entry->fragment.val.fragment_vsp_num_of_session  = 0;
        entry->fragment.val.fragment_vsp_num_of_port     = 0;
        entry->fragment.val.fragment_vsp_num_of_group    = 0;
#if 1
        entry->Interface_Tdm_A.val.exist = MEA_TRUE;
        entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_SBI;
        entry->Interface_Tdm_A.val.start_ts  = 0;
        entry->Interface_Tdm_A.val.num0fspe  = 12;

        entry->Interface_Tdm_B.val.exist = MEA_FALSE;
        entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
        entry->Interface_Tdm_B.val.start_ts  = 8; //96 
        entry->Interface_Tdm_B.val.num0fspe  = 8;

        entry->Interface_Tdm_C.val.exist = MEA_FALSE;
        entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
        entry->Interface_Tdm_C.val.start_ts  =0;
        entry->Interface_Tdm_C.val.num0fspe =0; //6;

        entry->global_Tdm.val.num_of_ces = 384; 
        entry->global_Tdm.val.ts_size    =  512;  // need to * 32
        entry->global_Tdm.val.mode_satop_only = MEA_TRUE;
#else
        entry->Interface_Tdm_A.val.exist = MEA_TRUE;
        entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_PCM ;//MEA_TDM_INTERFACE_SBI;
        entry->Interface_Tdm_A.val.start_ts  = 0;
        entry->Interface_Tdm_A.val.num0fspe  = 8;                  //12;

        entry->Interface_Tdm_B.val.exist = MEA_TRUE;
        entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
        entry->Interface_Tdm_B.val.start_ts  = 8; //96 
        entry->Interface_Tdm_B.val.num0fspe  = 8;

        entry->Interface_Tdm_C.val.exist = MEA_FALSE;
        entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
        entry->Interface_Tdm_C.val.start_ts  =0;
        entry->Interface_Tdm_C.val.num0fspe =0; //6;

        entry->global_Tdm.val.num_of_ces = 16; 
        entry->global_Tdm.val.ts_size    =  512;  // need to * 32
        entry->global_Tdm.val.mode_satop_only = MEA_TRUE;

#endif
#endif

    break;
    case MEA_OS_DEVICE_SIMULATION_3700PW_PCM_PCM:
#if 1 
        entry->global.val.encription_enable            = MEA_TRUE;        
        entry->global.val.protection_enable            = MEA_FALSE;
        entry->global.val.bmf_martini_support          = MEA_TRUE; 
        
        
        entry->global.val.forwarder_external           = MEA_TRUE;
        entry->global.val.desc_external                = MEA_FALSE;
        entry->global.val.desc_checker_exist           = MEA_FALSE;
        entry->global.val.host_reduce                  = MEA_TRUE;
        entry->global.val.pause_support                = MEA_FALSE;
        entry->global.val.pre_sched_enable             = MEA_TRUE;
        entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
        entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;


        entry->global.val.fragment_exist               = MEA_FALSE;
       
        entry->global.val.ACM_exist                    = MEA_TRUE;
        entry->global.val.dual_latency_support         = MEA_TRUE;
        entry->global.val.fragment_bonding             = MEA_FALSE;
        entry->global.val.cpe_exist                    = MEA_TRUE;
       
        entry->global.val.tdm_enable                   = MEA_TRUE;
        entry->global.val.large_BM_ind_address         = MEA_FALSE;
       
        entry->global.val.protocolMapping              = MEA_FALSE;
        entry->global.val.cls_large                    = MEA_TRUE;
        entry->global.val.Support_1588                 = MEA_FALSE;
        entry->global.val.BM_bus_width                 = 0;  // 0-64bit 01-128bit 10-256 11 TBD 
        entry->global.val.mc_mqs_Support               = MEA_FALSE;
        entry->global.val.cluster_adminon_support      = MEA_FALSE;
        



        
        /*only for ATM mode*/
        entry->pre_parser.val.exist                    = MEA_FALSE;
        entry->pre_parser.val.table_size               = 0; //TBD
        entry->pre_parser.val.vsp_exist                = MEA_FALSE;

        /*******************************************************/
        entry->parser.val.exist                      = MEA_TRUE;
        entry->parser.val.parser_etype_cam           = MEA_FALSE; 
        entry->parser.val.table_size                 = 128; 

        /*******************************************************/
        entry->service.val.exist                     = MEA_TRUE;
        entry->service.val.table_size                = 1024;
        entry->service.val.serviceRangeExist         = MEA_FALSE;
        entry->service.val.num_service_hash          = 3;
        entry->service.val.ServiceRange_Num_of_range     = 0;
        /*******************************************************/
        entry->acl.val.exist                         = MEA_TRUE;  
        entry->acl.val.table_size                    = 256;

        entry->acl.val.lxcp_act_width                 = 0; // 2^8 is 256 Action
        entry->acl.val.lxcp_prof_width                = 0; // 2^4 is 16 profile

        /*******************************************************/

        entry->forwarder_lmt.val.exist               = MEA_TRUE;
        entry->forwarder_lmt.val.table_size          = 128;

        /*******************************************************/
        entry->forwarder.val.exist               = MEA_FALSE;
        entry->forwarder.val.addres_size         = 23;      
        entry->forwarder.val.adm_support         = MEA_TRUE;
        num_of_forwarder_Action                 =   512;
        entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
        entry->forwarder.val.width_learn_action_id  =     10;
        entry->forwarder.val.width_learn_src_port =        7;   

        /*******************************************************/

        entry->shaper.val.num_of_shaper_profile=64;  // for port and priQue and cluster
        entry->shaper.val.shaper_per_Priqueue_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_cluster_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_port_exist=MEA_TRUE;

        /*******************************************************/
        entry->policer.val.exist                 = MEA_FALSE; 

        entry->policer.val.reduce                = MEA_FALSE;

        entry->policer.val.num_of_TMID = 512;
        entry->policer.val.hidden_profile        = 0;//3;
        entry->policer.val.num_of_acm            = 0;//8;   // 1 when no ACM
        entry->policer.val.num_of_profile        = 64;
        entry->policer.val.num_of_wred_prof      = 0; //8;
        entry->policer.val.num_of_wred_acm       = 0;

        /*******************************************************/

        entry->wfq.val.cluster_exist             = MEA_FALSE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
        entry->wfq.val.queue_exist               = MEA_FALSE;
        entry->wfq.val.num_of_cluster            = 128;  // num of cluster
        entry->wfq.val.maxCluserperport          = 32;    //  32 if the HW support
        entry->wfq.val.clusterTo_multiport_exist  = MEA_TRUE; // only on bonding

        /*******************************************************/

        entry->action.val.exist                  = MEA_TRUE;
        entry->action.val.table_size             = 1024+256; 

        /*******************************************************/

        entry->xpermission.val.exist             = MEA_TRUE;
        entry->xpermission.val.xper_table_size = 512;

        /*******************************************************/

        entry->sar_machine.val.exist             = MEA_FALSE;
        entry->sar_machine.val.table_size        = 0;   

        /*******************************************************/
        entry->editor_BMF.val.atm_support           = MEA_FALSE;
        entry->editor_BMF.val.maritini_mac_support  = MEA_TRUE;
        entry->editor_BMF.val.router_support        = MEA_FALSE;
        num_of_edit = 512;
        entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);

        entry->editor_BMF.val.num_of_lm_count       = 16;
        /*******************************************************/

        NumOfPmIds = 512;
        entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);

        entry->pm.val.num0fBlocksForBytes = 1;
        entry->pm.val.num0fBlocksForPmPackets = 1;


        /*******************************************************/
        entry->packetGen.val.PacketGen_exist         = MEA_TRUE;
        entry->packetGen.val.Analyzer_exist          = MEA_TRUE;
        entry->packetGen.val.PacketGen_On_Port_num   = 120;
        entry->packetGen.val.Analyzer_On_Port_num    = 120;
        entry->packetGen.val.Analyzer_type1_num_of_streams  = 16;//16; /*lbm lbr*/
        entry->packetGen.val.Analyzer_type2_num_of_streams  = 16; /*ccm*/
        entry->packetGen.val.PacketGen_num_of_support_profile = 3;
        entry->packetGen.val.Pktgen_type1_num_of_streams = 0;
        entry->packetGen.val.Pktgen_type2_num_of_streams = 16;
        entry->packetGen.val.Analyzer_num_of_support_profile = 8;//buckets

        /*******************************************************************************/

        entry->fragment.val.fragment_Edit_exist           = MEA_FALSE;
        entry->fragment.val.fragment_Edit_num_of_session  = 512;
        entry->fragment.val.fragment_Edit_num_of_port     = 1;
        entry->fragment.val.fragment_Edit_num_of_group    = 1;

        entry->fragment.val.fragment_vsp_exist           = MEA_TRUE;
        entry->fragment.val.fragment_vsp_num_of_session  = 0;
        entry->fragment.val.fragment_vsp_num_of_port     = 0;
        entry->fragment.val.fragment_vsp_num_of_group    = 0;
#if 0
        entry->Interface_Tdm_A.val.exist = MEA_TRUE;
        entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_SBI;
        entry->Interface_Tdm_A.val.start_ts  = 0;
        entry->Interface_Tdm_A.val.num0fspe  = 12;
        entry->Interface_Tdm_B.val.exist = MEA_FALSE;
        entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
        entry->Interface_Tdm_B.val.start_ts  = 8; //96 
        entry->Interface_Tdm_B.val.num0fspe  = 8;
        entry->Interface_Tdm_C.val.exist = MEA_FALSE;
        entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
        entry->Interface_Tdm_C.val.start_ts  =0;
        entry->Interface_Tdm_C.val.num0fspe =0; //6;
        entry->global_Tdm.val.num_of_ces = 384; 
        entry->global_Tdm.val.ts_size    =  512;  // need to * 32
        entry->global_Tdm.val.mode_satop_only = MEA_TRUE;
#else
        entry->Interface_Tdm_A.val.exist = MEA_TRUE;
        entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_PCM ;//MEA_TDM_INTERFACE_SBI;
        entry->Interface_Tdm_A.val.start_ts  = 0;
        entry->Interface_Tdm_A.val.num0fspe  = 8;                  //12;
        entry->Interface_Tdm_B.val.exist = MEA_TRUE;
        entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
        entry->Interface_Tdm_B.val.start_ts  = 8; //96 
        entry->Interface_Tdm_B.val.num0fspe  = 8;
        entry->Interface_Tdm_C.val.exist = MEA_FALSE;
        entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
        entry->Interface_Tdm_C.val.start_ts  =0;
        entry->Interface_Tdm_C.val.num0fspe =0; //6;
        entry->global_Tdm.val.num_of_ces = 16; 
        entry->global_Tdm.val.ts_size    =  512;  // need to * 32
        entry->global_Tdm.val.mode_satop_only = MEA_TRUE;
#endif
#endif
        break;
        case MEA_OS_DEVICE_SIMULATION_3875_AOP:
#if 1
            entry->global.val.encription_enable            = MEA_TRUE;        
            entry->global.val.protection_enable            = MEA_TRUE;
            entry->global.val.bmf_martini_support          = MEA_TRUE; 
            
            
            entry->global.val.forwarder_external           = MEA_TRUE;
            entry->global.val.desc_external                = MEA_TRUE;
            entry->global.val.desc_checker_exist           = MEA_TRUE;
            entry->global.val.host_reduce                  = MEA_TRUE;
            entry->global.val.pause_support                = MEA_TRUE;
            entry->global.val.pre_sched_enable             = MEA_TRUE;

            entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
            entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;

            entry->global.val.fragment_exist               = MEA_TRUE;
           
            entry->global.val.ACM_exist                    = MEA_TRUE;
            entry->global.val.dual_latency_support         = MEA_TRUE;
            entry->global.val.fragment_bonding             = MEA_FALSE;
            entry->global.val.cpe_exist                    = MEA_TRUE;
          
            entry->global.val.tdm_enable                   = MEA_FALSE;
            entry->global.val.large_BM_ind_address         = MEA_TRUE;
            
            entry->global.val.protocolMapping              = MEA_TRUE;
            entry->global.val.cls_large                    = MEA_TRUE;
            entry->global.val.Support_1588                 = MEA_TRUE;
            entry->global.val.BM_bus_width                 = 1;  // 0-64bit 01-128bit 10-256 11 TBD 
            entry->global.val.mc_mqs_Support               = MEA_TRUE;
            entry->global.val.cluster_adminon_support      = MEA_TRUE;
            
            
            entry->global.val.forworder_filter_support      = MEA_TRUE;
            entry->global.val.rx_pause_support              = MEA_TRUE;
            entry->global.val.autoneg_support               = MEA_TRUE;


            
            /*only for ATM mode*/
            entry->pre_parser.val.exist                    = MEA_FALSE;
            entry->pre_parser.val.table_size               = 8; //TBD
            entry->pre_parser.val.vsp_exist                = MEA_FALSE;

            /*******************************************************/
            entry->parser.val.exist                      = MEA_TRUE;
            entry->parser.val.parser_etype_cam           = MEA_FALSE; 
            entry->parser.val.table_size                 = 128; 
            /*******************************************************/

            entry->service.val.exist                     = MEA_TRUE;
            entry->service.val.table_size                = 8192;
            entry->service.val.serviceRangeExist         = MEA_TRUE;
            entry->service.val.num_service_hash          = 3;
            entry->service.val.ServiceRange_Num_of_range     =64;

            /*******************************************************/
            entry->acl.val.exist                         = MEA_TRUE;  
            entry->acl.val.table_size                    = 256;
            entry->acl.val.lxcp_act_width                 = 8; // 2^8 is 256 Action
            entry->acl.val.lxcp_prof_width                = 6; // 2^4 is 16 profile

            /*******************************************************/
            entry->forwarder_lmt.val.exist               = MEA_TRUE;
            entry->forwarder_lmt.val.table_size          = 8192;

            /*******************************************************/
            entry->forwarder.val.exist               = MEA_TRUE;
            entry->forwarder.val.addres_size         = 23;      
            entry->forwarder.val.adm_support         = MEA_TRUE;
            num_of_forwarder_Action                 =   2048;
            entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
            entry->forwarder.val.width_learn_action_id  =     12;
            entry->forwarder.val.width_learn_src_port =        7;   
            /*******************************************************/
            entry->shaper.val.num_of_shaper_profile=64;  // for port and priQue and cluster

            entry->shaper.val.shaper_per_Priqueue_exist=MEA_TRUE;
            entry->shaper.val.shaper_per_cluster_exist=MEA_TRUE;
            entry->shaper.val.shaper_per_port_exist=MEA_TRUE;

            /*******************************************************/
            entry->policer.val.exist                 = MEA_TRUE; 

            entry->policer.val.num_of_TMID = 2048;  // 1024
            entry->policer.val.reduce                = MEA_TRUE;
            entry->policer.val.hidden_profile        = 3;
            entry->policer.val.num_of_acm            = 1;   // 1 when no ACM
            entry->policer.val.num_of_profile        = 128; //64;
            entry->policer.val.num_of_wred_prof        = 8;
            entry->policer.val.num_of_wred_acm        = 1;

            /*******************************************************/
            entry->wfq.val.cluster_exist             = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
            entry->wfq.val.queue_exist               = MEA_TRUE;
            entry->wfq.val.num_of_cluster            = 256;  // num of cluster
            entry->wfq.val.maxCluserperport          = 32;    //  32 if the HW support
            entry->wfq.val.clusterTo_multiport_exist  = MEA_TRUE; // only on bonding


            /*******************************************************/
            entry->action.val.exist                  = MEA_TRUE;
            entry->action.val.table_size             = 16384+entry->acl.val.table_size; 
            /*******************************************************/
            entry->xpermission.val.exist             = MEA_TRUE;
            entry->xpermission.val.xper_table_size = 512;
            /*******************************************************/
            entry->sar_machine.val.exist             = MEA_TRUE;
            entry->sar_machine.val.table_size        = 0;   
            /*******************************************************/
            entry->editor_BMF.val.atm_support           = MEA_TRUE;
            entry->editor_BMF.val.maritini_mac_support  = MEA_TRUE;
            entry->editor_BMF.val.router_support        = MEA_TRUE;
           
            num_of_edit = 4096;
            entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);

            entry->editor_BMF.val.BMF_CCM_count_EN      = MEA_TRUE;
            entry->editor_BMF.val.BMF_CCM_count_stamp   = MEA_TRUE;
            entry->editor_BMF.val.num_of_lm_count       =  256;

            /*******************************************************/
            NumOfPmIds = 4096;
            entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);

            entry->pm.val.num0fBlocksForBytes = 1;
            entry->pm.val.num0fBlocksForPmPackets = 1;


           

            /*******************************************************/
            entry->packetGen.val.PacketGen_exist         = MEA_TRUE;
            entry->packetGen.val.Analyzer_exist          = MEA_TRUE;
            entry->packetGen.val.PacketGen_On_Port_num   = 120;
            entry->packetGen.val.Analyzer_On_Port_num    = 120;
            entry->packetGen.val.Analyzer_type1_num_of_streams  = 16;//16; /*lbm lbr*/
            entry->packetGen.val.Analyzer_type2_num_of_streams  = 16; /*ccm*/
            entry->packetGen.val.PacketGen_num_of_support_profile = 3;
            entry->packetGen.val.Pktgen_type1_num_of_streams = 16;
            entry->packetGen.val.Pktgen_type2_num_of_streams = 16;
            entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets
            /*******************************************************************************/
            entry->fragment.val.fragment_Edit_exist           = MEA_TRUE;
            entry->fragment.val.fragment_Edit_num_of_session  = 512;
            entry->fragment.val.fragment_Edit_num_of_port     = 1;
            entry->fragment.val.fragment_Edit_num_of_group    = 1;

            entry->fragment.val.fragment_vsp_exist           = MEA_TRUE;
            entry->fragment.val.fragment_vsp_num_of_session  = 512;
            entry->fragment.val.fragment_vsp_num_of_port     = 1;
            entry->fragment.val.fragment_vsp_num_of_group    = 1;

            entry->Interface_Tdm_A.val.exist = MEA_FALSE;
            entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_SBI;
            entry->Interface_Tdm_A.val.start_ts  = 0;
            entry->Interface_Tdm_A.val.num0fspe  = 12;

            entry->Interface_Tdm_B.val.exist = MEA_FALSE;
            entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
            entry->Interface_Tdm_B.val.start_ts  = 96;
            entry->Interface_Tdm_B.val.num0fspe  = 8;

            entry->Interface_Tdm_C.val.exist = MEA_FALSE;
            entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
            entry->Interface_Tdm_C.val.start_ts  =0;
            entry->Interface_Tdm_C.val.num0fspe =0; //6;

            entry->global_Tdm.val.num_of_ces = 384; 
            entry->global_Tdm.val.ts_size    =  512;  // need to * 32
#endif
        break;
    case MEA_OS_DEVICE_SIMULATION_4800:
#if 1 
        {
        entry->global.val.encription_enable            = MEA_TRUE;        
        entry->global.val.protection_enable            = MEA_TRUE;
        entry->global.val.bmf_martini_support          = MEA_TRUE; 
        
        
        entry->global.val.forwarder_external           = MEA_TRUE;
        entry->global.val.desc_external                = MEA_TRUE;
        entry->global.val.desc_checker_exist           = MEA_TRUE;
        entry->global.val.host_reduce                  = MEA_TRUE;
        entry->global.val.pause_support                = MEA_TRUE;

        entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
        entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;

        entry->global.val.fragment_exist               = MEA_TRUE;
        
        entry->global.val.ACM_exist                    = MEA_TRUE;
        entry->global.val.dual_latency_support         = MEA_TRUE;
        entry->global.val.fragment_bonding             = MEA_FALSE;
        entry->global.val.cpe_exist                    = MEA_TRUE;
     
        entry->global.val.tdm_enable                   = MEA_FALSE;
        entry->global.val.large_BM_ind_address         = MEA_TRUE;
     
        entry->global.val.protocolMapping              = MEA_TRUE;
        entry->global.val.cls_large                    = MEA_TRUE;
        entry->global.val.Support_1588                 = MEA_TRUE;
        entry->global.val.BM_bus_width                 = 2;  // 0-64bit 01-128bit 10-256 11 TBD 
        entry->global.val.mc_mqs_Support               = MEA_TRUE;
        entry->global.val.cluster_adminon_support      = MEA_TRUE;
        
       
       entry->global.val.forworder_filter_support      = MEA_TRUE;
       entry->global.val.rx_pause_support              = MEA_TRUE;
       entry->global.val.autoneg_support               = MEA_TRUE;


        
        /*********** only for ATM mode*****************************/
        
        entry->pre_parser.val.exist                    = MEA_FALSE;
        entry->pre_parser.val.table_size               = 8; //TBD
        entry->pre_parser.val.vsp_exist                = MEA_FALSE;

        /*******************************************************/
        entry->parser.val.exist                      = MEA_TRUE;
        entry->parser.val.parser_etype_cam           = MEA_FALSE; 
        entry->parser.val.table_size                 = 128; 
        /*******************************************************/

        entry->service.val.exist                     = MEA_TRUE;
        entry->service.val.table_size                = 8192;
        entry->service.val.serviceRangeExist         = MEA_TRUE;
        entry->service.val.num_service_hash          = 3;
        entry->service.val.ServiceRange_Num_of_range     =64;

        /*******************************************************/
        entry->acl.val.exist                         = MEA_TRUE ; //MEA_TRUE;  
        entry->acl.val.table_size                    = 256;
        entry->acl.val.lxcp_act_width                 = 8; // 2^8 is 256 Action
        entry->acl.val.lxcp_prof_width                = 6; // 2^4 is 16 profile

        /*******************************************************/
        entry->forwarder_lmt.val.exist               = MEA_TRUE;
        entry->forwarder_lmt.val.table_size          = 8192;

        /*******************************************************/
        entry->forwarder.val.exist               = MEA_TRUE;
        entry->forwarder.val.addres_size         = 23;      
        entry->forwarder.val.adm_support         = MEA_TRUE;
        num_of_forwarder_Action                 =   2048;
        entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
        entry->forwarder.val.width_learn_action_id  =     12;
        entry->forwarder.val.width_learn_src_port =        7;   
        /*******************************************************/
        entry->shaper.val.num_of_shaper_profile=64;  // for port and priQue and cluster

        entry->shaper.val.shaper_per_Priqueue_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_cluster_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_port_exist=MEA_TRUE;

        /*******************************************************/
        entry->policer.val.exist                 = MEA_TRUE; 

        entry->policer.val.num_of_TMID = 2048;  // 1024
        entry->policer.val.reduce                = MEA_TRUE;
        entry->policer.val.hidden_profile        = 3;
        entry->policer.val.num_of_acm            = 1;   // 1 when no ACM
        entry->policer.val.num_of_profile        = 128; //64;
        entry->policer.val.num_of_wred_prof        = 8;
        entry->policer.val.num_of_wred_acm        = 1;
        
        /*******************************************************/
        entry->wfq.val.cluster_exist             = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
        entry->wfq.val.queue_exist               = MEA_TRUE;
        entry->wfq.val.num_of_cluster            = 256;  // num of cluster
        entry->wfq.val.maxCluserperport          = 32;    //  32 if the HW support
        entry->wfq.val.clusterTo_multiport_exist  = MEA_TRUE; // only on bonding
        

        /*******************************************************/
        entry->action.val.exist                  = MEA_TRUE;
        entry->action.val.table_size             = 16384+entry->acl.val.table_size; 
        /*******************************************************/
        entry->xpermission.val.exist             = MEA_TRUE;
        entry->xpermission.val.xper_table_size = 1024;
        /*******************************************************/
        entry->sar_machine.val.exist             = MEA_TRUE;
        entry->sar_machine.val.table_size        = 0;   
        /*******************************************************/
        entry->editor_BMF.val.atm_support           = MEA_TRUE;
        entry->editor_BMF.val.maritini_mac_support  = MEA_TRUE;
        entry->editor_BMF.val.router_support        = MEA_TRUE;
        num_of_edit = 4096;
        entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);

        entry->editor_BMF.val.BMF_CCM_count_EN      = MEA_TRUE;
        entry->editor_BMF.val.BMF_CCM_count_stamp   = MEA_TRUE;
        entry->editor_BMF.val.num_of_lm_count       =  256;

        /*******************************************************/
        NumOfPmIds = 4096;
        entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
        entry->pm.val.num0fBlocksForBytes = 1;
        entry->pm.val.num0fBlocksForPmPackets = 1;

        /*******************************************************/
        entry->packetGen.val.PacketGen_exist         = MEA_TRUE;
        entry->packetGen.val.Analyzer_exist          = MEA_TRUE;
        entry->packetGen.val.PacketGen_On_Port_num   = 120;
        entry->packetGen.val.Analyzer_On_Port_num    = 120;
        entry->packetGen.val.Analyzer_type1_num_of_streams  = 16;//16; /*lbm lbr*/
        entry->packetGen.val.Analyzer_type2_num_of_streams  = 16; /*ccm*/
        entry->packetGen.val.PacketGen_num_of_support_profile = 3;
        entry->packetGen.val.Pktgen_type1_num_of_streams = 16;
        entry->packetGen.val.Pktgen_type2_num_of_streams = 16;
        entry->packetGen.val.Analyzer_num_of_support_profile = 16; 
        /*******************************************************************************/
        entry->fragment.val.fragment_Edit_exist           = MEA_TRUE;
        entry->fragment.val.fragment_Edit_num_of_session  = 512;
        entry->fragment.val.fragment_Edit_num_of_port     = 1;
        entry->fragment.val.fragment_Edit_num_of_group    = 1;

        entry->fragment.val.fragment_vsp_exist           = MEA_TRUE;
        entry->fragment.val.fragment_vsp_num_of_session  = 512;
        entry->fragment.val.fragment_vsp_num_of_port     = 1;
        entry->fragment.val.fragment_vsp_num_of_group    = 1;

        entry->Interface_Tdm_A.val.exist = MEA_FALSE;
        entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_SBI;
        entry->Interface_Tdm_A.val.start_ts  = 0;
        entry->Interface_Tdm_A.val.num0fspe  = 12;

        entry->Interface_Tdm_B.val.exist = MEA_FALSE;
        entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
        entry->Interface_Tdm_B.val.start_ts  = 96;
        entry->Interface_Tdm_B.val.num0fspe  = 8;

        entry->Interface_Tdm_C.val.exist = MEA_FALSE;
        entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
        entry->Interface_Tdm_C.val.start_ts  =0;
        entry->Interface_Tdm_C.val.num0fspe =0; //6;

        entry->global_Tdm.val.num_of_ces = 384; 
        entry->global_Tdm.val.ts_size    =  512;  // need to * 32
        }
#endif

        break;
case   MEA_OS_DEVICE_SIMULATION_4800_GPON:


#if 1
    entry->global.val.encription_enable            = MEA_TRUE; 
    entry->global.val.protection_enable            = MEA_TRUE;
    entry->global.val.bmf_martini_support          = MEA_TRUE; 
    
    
    entry->global.val.forwarder_external           = MEA_TRUE;
    entry->global.val.desc_external                = MEA_TRUE;
    entry->global.val.desc_checker_exist           = MEA_TRUE;
    entry->global.val.host_reduce                  = MEA_TRUE;
    entry->global.val.pause_support                = MEA_TRUE;

    entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
    entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;

    entry->global.val.fragment_exist               = MEA_TRUE;
    
    entry->global.val.ACM_exist                    = MEA_FALSE;
    entry->global.val.dual_latency_support         = MEA_FALSE;
    entry->global.val.fragment_bonding             = MEA_FALSE;
    entry->global.val.cpe_exist                    = MEA_TRUE;
   
    entry->global.val.tdm_enable                   = MEA_FALSE;
    entry->global.val.large_BM_ind_address         = MEA_TRUE;
   
    entry->global.val.protocolMapping              = MEA_TRUE;
    entry->global.val.cls_large                    = MEA_TRUE;
    entry->global.val.Support_1588                 = MEA_TRUE;
    entry->global.val.BM_bus_width                 = 1;  // 0-64bit 01-128bit 10-256 11 TBD 
    entry->global.val.mc_mqs_Support               = MEA_TRUE;
    entry->global.val.cluster_adminon_support      = MEA_TRUE;
    
    
    entry->global.val.forworder_filter_support      = MEA_TRUE;
    entry->global.val.rx_pause_support              = MEA_TRUE;


    /************************************************************/
    
    entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
    /*******************************************************/
    /*only for ATM mode*/
    entry->pre_parser.val.exist                    = MEA_FALSE;
    entry->pre_parser.val.table_size               = 8; //TBD
    entry->pre_parser.val.vsp_exist                = MEA_FALSE;

    /*******************************************************/
    entry->parser.val.exist                      = MEA_TRUE;
    entry->parser.val.parser_etype_cam           = MEA_FALSE; 
    entry->parser.val.table_size                 = 128; 
    /*******************************************************/

    entry->service.val.exist                     = MEA_TRUE;
    entry->service.val.table_size                = 8192;
    entry->service.val.serviceRangeExist         = MEA_TRUE;
    entry->service.val.num_service_hash          = 3;
    entry->service.val.ServiceRange_Num_of_range     =64;

    /*******************************************************/
    entry->acl.val.exist                         = MEA_TRUE;  
    entry->acl.val.table_size                    = 256;
    entry->acl.val.lxcp_act_width                 = 8; // 2^8 is 256 Action
    entry->acl.val.lxcp_prof_width                = 6; // 2^4 is 16 profile

    /*******************************************************/
    entry->forwarder_lmt.val.exist               = MEA_TRUE;
    entry->forwarder_lmt.val.table_size          = 8192;

    /*******************************************************/
    entry->forwarder.val.exist               = MEA_TRUE;
    entry->forwarder.val.addres_size         = 23;      
    entry->forwarder.val.adm_support         = MEA_TRUE;
    num_of_forwarder_Action                 =   2048;
    entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
    entry->forwarder.val.width_learn_action_id  =     12;
    entry->forwarder.val.width_learn_src_port =        7;   
    /*******************************************************/
    entry->shaper.val.num_of_shaper_profile=64;  // for port and priQue and cluster

    entry->shaper.val.shaper_per_Priqueue_exist=MEA_TRUE;
    entry->shaper.val.shaper_per_cluster_exist=MEA_TRUE;
    entry->shaper.val.shaper_per_port_exist=MEA_TRUE;

    /*******************************************************/
    entry->policer.val.exist                 = MEA_TRUE; 

    entry->policer.val.num_of_TMID = 2048;  // 1024
    entry->policer.val.reduce                = MEA_TRUE;
    entry->policer.val.hidden_profile        = 3;
    entry->policer.val.num_of_acm            = 1;   // 1 when no ACM
    entry->policer.val.num_of_profile        = 128; //64;
    entry->policer.val.num_of_wred_prof        = 8;
    entry->policer.val.num_of_wred_acm        = 1;

    /*******************************************************/
    entry->wfq.val.cluster_exist             = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
    entry->wfq.val.queue_exist               = MEA_TRUE;
    entry->wfq.val.num_of_cluster            = 256;  // num of cluster
    entry->wfq.val.maxCluserperport          = 32;    //  32 if the HW support
    entry->wfq.val.clusterTo_multiport_exist  = MEA_TRUE; // only on bonding


    /*******************************************************/
    entry->action.val.exist                  = MEA_TRUE;
    entry->action.val.table_size             = 16384+entry->acl.val.table_size; 
    /*******************************************************/
    entry->xpermission.val.exist             = MEA_TRUE;
    entry->xpermission.val.xper_table_size = 1024;
    /*******************************************************/
    entry->sar_machine.val.exist             = MEA_TRUE;
    entry->sar_machine.val.table_size        = 0;   
    /*******************************************************/
    entry->editor_BMF.val.atm_support           = MEA_TRUE;
    entry->editor_BMF.val.maritini_mac_support  = MEA_TRUE;
    entry->editor_BMF.val.router_support        = MEA_TRUE;
    num_of_edit = 4096;
    entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);

    entry->editor_BMF.val.BMF_CCM_count_EN      = MEA_TRUE;
    entry->editor_BMF.val.BMF_CCM_count_stamp   = MEA_TRUE;
    entry->editor_BMF.val.num_of_lm_count       =  256;

    /*******************************************************/

    NumOfPmIds = 4096;
    entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
    entry->pm.val.num0fBlocksForBytes = 1;
    entry->pm.val.num0fBlocksForPmPackets = 1;

    /*******************************************************/
    entry->packetGen.val.PacketGen_exist         = MEA_TRUE;
    entry->packetGen.val.Analyzer_exist          = MEA_TRUE;
    entry->packetGen.val.PacketGen_On_Port_num   = 120;
    entry->packetGen.val.Analyzer_On_Port_num    = 120;
    entry->packetGen.val.Analyzer_type1_num_of_streams  = 16;//16; /*lbm lbr*/
    entry->packetGen.val.Analyzer_type2_num_of_streams  = 16; /*ccm*/
    entry->packetGen.val.PacketGen_num_of_support_profile = 3;
    entry->packetGen.val.Pktgen_type1_num_of_streams = 16;
    entry->packetGen.val.Pktgen_type2_num_of_streams = 16;
    entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets
    /*******************************************************************************/
    entry->fragment.val.fragment_Edit_exist           = MEA_TRUE;
    entry->fragment.val.fragment_Edit_num_of_session  = 512;
    entry->fragment.val.fragment_Edit_num_of_port     = 1;
    entry->fragment.val.fragment_Edit_num_of_group    = 1;

    entry->fragment.val.fragment_vsp_exist           = MEA_TRUE;
    entry->fragment.val.fragment_vsp_num_of_session  = 512;
    entry->fragment.val.fragment_vsp_num_of_port     = 1;
    entry->fragment.val.fragment_vsp_num_of_group    = 1;

    entry->Interface_Tdm_A.val.exist = MEA_FALSE;
    entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_SBI;
    entry->Interface_Tdm_A.val.start_ts  = 0;
    entry->Interface_Tdm_A.val.num0fspe  = 12;

    entry->Interface_Tdm_B.val.exist = MEA_FALSE;
    entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
    entry->Interface_Tdm_B.val.start_ts  = 96;
    entry->Interface_Tdm_B.val.num0fspe  = 8;

    entry->Interface_Tdm_C.val.exist = MEA_FALSE;
    entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
    entry->Interface_Tdm_C.val.start_ts  =0;
    entry->Interface_Tdm_C.val.num0fspe =0; //6;

    entry->global_Tdm.val.num_of_ces = 384; 
    entry->global_Tdm.val.ts_size    =  512;  // need to * 32
#endif

break;
    case MEA_OS_DEVICE_SIMULATION_4800_GE:
#if 1
        {
            entry->global.val.encription_enable            = MEA_TRUE;        
            entry->global.val.protection_enable            = MEA_FALSE;
            entry->global.val.bmf_martini_support          = MEA_FALSE; 
            
            
            entry->global.val.forwarder_external           = MEA_TRUE;
            entry->global.val.desc_external                = MEA_FALSE;
            entry->global.val.desc_checker_exist           = MEA_FALSE;
            entry->global.val.host_reduce                  = MEA_TRUE;
            entry->global.val.pause_support                = MEA_TRUE;

            entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
            entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;


            entry->global.val.fragment_exist               = MEA_TRUE;

            
            entry->global.val.ACM_exist                    = MEA_TRUE;
            entry->global.val.dual_latency_support         = MEA_TRUE;
            entry->global.val.fragment_bonding             = MEA_FALSE;
            entry->global.val.cpe_exist                    = MEA_TRUE;
           
            entry->global.val.tdm_enable                   = MEA_FALSE;
            entry->global.val.large_BM_ind_address         = MEA_TRUE;
         
            entry->global.val.protocolMapping              = MEA_TRUE;
            entry->global.val.cls_large                    = MEA_TRUE;
            entry->global.val.Support_1588                 = MEA_FALSE;
            entry->global.val.BM_bus_width                 = 1;  // 0-64bit 01-128bit 10-TBD 11 TBD 
            entry->global.val.mc_mqs_Support               = MEA_TRUE;
            entry->global.val.cluster_adminon_support      = MEA_TRUE;
            
            entry->global.val.MAC_LB_support            = MEA_TRUE;



            /************************************************************/
            
            entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
            /*******************************************************/
            /*only for ATM mode*/
            entry->pre_parser.val.exist                    = MEA_FALSE;
            entry->pre_parser.val.table_size               = 0; //TBD
            entry->pre_parser.val.vsp_exist                = MEA_FALSE;

            /*******************************************************/
            entry->parser.val.exist                      = MEA_TRUE;
            entry->parser.val.parser_etype_cam           = MEA_FALSE; 
            entry->parser.val.table_size                 = 128; 
            /*******************************************************/

            entry->service.val.exist                     = MEA_TRUE;
            entry->service.val.table_size                = 2048;
            entry->service.val.serviceRangeExist         = MEA_TRUE;
            entry->service.val.num_service_hash          = 3;
            entry->service.val.ServiceRange_Num_of_range     = 32;
            /*******************************************************/
            entry->acl.val.exist                         = MEA_TRUE;  
            entry->acl.val.table_size                    = 256;

            entry->acl.val.lxcp_act_width                 = 8; // 2^8 is 256 Action
            entry->acl.val.lxcp_prof_width                = 6; // 2^4 is 16 profile

            /*******************************************************/

            entry->forwarder_lmt.val.exist               = MEA_TRUE;
            entry->forwarder_lmt.val.table_size          = 512;

            /*******************************************************/
            entry->forwarder.val.exist               = MEA_TRUE;
            entry->forwarder.val.adm_support         = MEA_TRUE;
            entry->forwarder.val.addres_size         = 23;      
            num_of_forwarder_Action                 =   2048;
            entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
            entry->forwarder.val.width_learn_action_id  =     12;
            entry->forwarder.val.width_learn_src_port =        9;   
            /*******************************************************/
            entry->shaper.val.num_of_shaper_profile=64;  // for port and priQue and cluster

            entry->shaper.val.shaper_per_Priqueue_exist = MEA_TRUE;
            entry->shaper.val.shaper_per_cluster_exist  = MEA_TRUE;
            entry->shaper.val.shaper_per_port_exist     = MEA_TRUE;

            /*******************************************************/
            entry->policer.val.exist                 = MEA_TRUE; 


            entry->policer.val.num_of_TMID = 4096;  // 1024
            entry->policer.val.reduce                = MEA_TRUE;
            if(MEA_TDM_SUPPORT ==MEA_TRUE){	   
                entry->policer.val.hidden_profile        = 0;//3;
                entry->policer.val.num_of_acm            = 0;//8;   // 1 when no ACM
                entry->policer.val.num_of_profile        = 64;

                entry->policer.val.num_of_wred_prof        = 1; //8;
                entry->policer.val.num_of_wred_acm        = 1;
            }else{
                entry->policer.val.hidden_profile        = 3;
                entry->policer.val.num_of_acm            = 8;   // 1 when no ACM
                entry->policer.val.num_of_profile        = 64;

                entry->policer.val.num_of_wred_prof        = 8;
                entry->policer.val.num_of_wred_acm        = 1;
            }
            /*******************************************************/
            entry->wfq.val.cluster_exist             = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
            entry->wfq.val.queue_exist               = MEA_TRUE;
            entry->wfq.val.num_of_cluster            = 256;  // num of cluster
            entry->wfq.val.maxCluserperport          = 32;    //  32 if the HW support
            entry->wfq.val.clusterTo_multiport_exist  = MEA_TRUE; // only on bonding

            /*******************************************************/
            entry->action.val.exist                  = MEA_TRUE;
            entry->action.val.table_size             = 16384+256; 
            /*******************************************************/
            entry->xpermission.val.exist             = MEA_TRUE;
            entry->xpermission.val.xper_table_size = 1024;  //512
            /*******************************************************/
            entry->sar_machine.val.exist             = MEA_TRUE;
            entry->sar_machine.val.table_size        = 0;   
            /*******************************************************/
            entry->editor_BMF.val.atm_support           = MEA_TRUE;
            entry->editor_BMF.val.maritini_mac_support  = MEA_TRUE;
            entry->editor_BMF.val.router_support        = MEA_TRUE;
            num_of_edit = 4096;
            entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
            /*******************************************************/
           
            NumOfPmIds = 8196;
            entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
            entry->pm.val.num0fBlocksForBytes = 1;
            entry->pm.val.num0fBlocksForPmPackets = 1;

            /*******************************************************/
            entry->packetGen.val.PacketGen_exist         = MEA_TRUE;
            entry->packetGen.val.Analyzer_exist          = MEA_TRUE;
            entry->packetGen.val.PacketGen_On_Port_num   = 120;
            entry->packetGen.val.Analyzer_On_Port_num    = 120;
            entry->packetGen.val.Analyzer_type1_num_of_streams  = 16;//16; /*lbm lbr*/
            entry->packetGen.val.Analyzer_type2_num_of_streams  = 16; /*ccm*/
            entry->packetGen.val.PacketGen_num_of_support_profile = 3;
            entry->packetGen.val.Pktgen_type1_num_of_streams = 0;
            entry->packetGen.val.Pktgen_type2_num_of_streams = 16;
            entry->packetGen.val.Analyzer_num_of_support_profile = 8;
            /*******************************************************************************/
            entry->fragment.val.fragment_Edit_exist           = MEA_TRUE;
            entry->fragment.val.fragment_Edit_num_of_session  = 512;
            entry->fragment.val.fragment_Edit_num_of_port     = 1;
            entry->fragment.val.fragment_Edit_num_of_group    = 1;

            entry->fragment.val.fragment_vsp_exist           = MEA_TRUE;
            entry->fragment.val.fragment_vsp_num_of_session  = 512;
            entry->fragment.val.fragment_vsp_num_of_port     = 1;
          }
#endif

        break;
    case MEA_OS_DEVICE_SIMULATION_3850_ADSL:
#if 1
        entry->global.val.encription_enable            = MEA_TRUE;        
        entry->global.val.protection_enable            = MEA_TRUE;
        entry->global.val.bmf_martini_support          = MEA_TRUE; 
        //entry->global.val.bmf_atm_support              = MEA_TRUE;
        
        entry->global.val.forwarder_external           = MEA_TRUE;
        entry->global.val.desc_external                = MEA_TRUE;
        entry->global.val.desc_checker_exist           = MEA_TRUE;
        entry->global.val.host_reduce                  = MEA_TRUE;
        entry->global.val.pause_support                = MEA_TRUE;

        entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
        entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;

        entry->global.val.fragment_exist               = MEA_TRUE;
       
        entry->global.val.ACM_exist                    = MEA_TRUE;
        entry->global.val.dual_latency_support         = MEA_TRUE;
        entry->global.val.fragment_bonding             = MEA_FALSE;
        entry->global.val.cpe_exist                    = MEA_TRUE;
        
        entry->global.val.tdm_enable                   = MEA_FALSE;
        entry->global.val.large_BM_ind_address         = MEA_TRUE;
        
        entry->global.val.protocolMapping              = MEA_TRUE;
        entry->global.val.cls_large                    = MEA_TRUE;
        entry->global.val.Support_1588                 = MEA_TRUE;
        entry->global.val.BM_bus_width                 =1;  // 0-64bit 01-128bit 10-TBD 11 TBD 
        entry->global.val.mc_mqs_Support               = MEA_TRUE;
        entry->global.val.cluster_adminon_support      = MEA_TRUE;
        
        


        /************************************************************/
       
        entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
        /*******************************************************/
        /*only for ATM mode*/
        entry->pre_parser.val.exist                    = MEA_FALSE;
        entry->pre_parser.val.table_size               = 8; //TBD
        entry->pre_parser.val.vsp_exist                = MEA_FALSE;
        entry->pre_parser.val.cam_exist                = MEA_TRUE;

        /*******************************************************/
        entry->parser.val.exist                      = MEA_TRUE;
        entry->parser.val.parser_etype_cam           = MEA_FALSE; 
        entry->parser.val.table_size                 = 128; 
        /*******************************************************/

        entry->service.val.exist                     = MEA_TRUE;
        entry->service.val.table_size                = 2048;
        entry->service.val.serviceRangeExist         = MEA_TRUE;
        entry->service.val.num_service_hash          = 1;
        entry->service.val.ServiceRange_Num_of_range     =32;

        /*******************************************************/
        entry->acl.val.exist                         = MEA_TRUE;  
        entry->acl.val.table_size                    = 256;
        entry->acl.val.lxcp_act_width                 = 8; // 2^8 is 256 Action
        entry->acl.val.lxcp_prof_width                = 6; // 2^4 is 16 profile

        /*******************************************************/
        entry->forwarder_lmt.val.exist               = MEA_TRUE;
        entry->forwarder_lmt.val.table_size          = 512;

        /*******************************************************/
        entry->forwarder.val.exist               = MEA_TRUE;
        entry->forwarder.val.addres_size         = 23;      
        entry->forwarder.val.adm_support         = MEA_TRUE;
        num_of_forwarder_Action                 =   2048;
        entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
        entry->forwarder.val.width_learn_action_id  =     12;
        entry->forwarder.val.width_learn_src_port =        7;   
        /*******************************************************/
        entry->shaper.val.num_of_shaper_profile=64;  // for port and priQue and cluster

        entry->shaper.val.shaper_per_Priqueue_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_cluster_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_port_exist=MEA_TRUE;

        /*******************************************************/
        entry->policer.val.exist                 = MEA_TRUE; 

        entry->policer.val.num_of_TMID = 2048;  // 1024
        entry->policer.val.reduce                = MEA_TRUE;
        entry->policer.val.hidden_profile        = 3;
        entry->policer.val.num_of_acm            = 8;   // 1 when no ACM
        entry->policer.val.num_of_profile        = 64;
        entry->policer.val.num_of_wred_prof        = 8;
        entry->policer.val.num_of_wred_acm        = 1;

        /*******************************************************/
        entry->wfq.val.cluster_exist             = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
        entry->wfq.val.queue_exist               = MEA_TRUE;
        entry->wfq.val.num_of_cluster                = 256;  // num of cluster
        entry->wfq.val.maxCluserperport          = 32;    //  32 if the HW support
        entry->wfq.val.clusterTo_multiport_exist  = MEA_FALSE; // only on bonding
        
        /*******************************************************/
        entry->action.val.exist                  = MEA_TRUE;
        entry->action.val.table_size             = 4352; 
        /*******************************************************/
        entry->xpermission.val.exist             = MEA_TRUE;
        entry->xpermission.val.xper_table_size = 512;
        entry->xpermission.val.num_clusterInGroup = 1;
        /*******************************************************/
        entry->sar_machine.val.exist             = MEA_TRUE;
        entry->sar_machine.val.table_size        = 0;   
        /*******************************************************/
        entry->editor_BMF.val.atm_support           = MEA_TRUE;
        entry->editor_BMF.val.maritini_mac_support  = MEA_TRUE;
        entry->editor_BMF.val.router_support        = MEA_TRUE;
        num_of_edit = 4096;
        entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
        /*******************************************************/

        NumOfPmIds = 4096;
        entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
        entry->pm.val.num0fBlocksForBytes = 1;
        entry->pm.val.num0fBlocksForPmPackets = 1;

        /*******************************************************/
        entry->packetGen.val.PacketGen_exist         = MEA_FALSE;
        entry->packetGen.val.Analyzer_exist          = MEA_FALSE;
        entry->packetGen.val.PacketGen_On_Port_num   = 120;
        entry->packetGen.val.Analyzer_On_Port_num    = 120;
        entry->packetGen.val.Analyzer_type1_num_of_streams  = 16;//16; /*lbm lbr*/
        entry->packetGen.val.Analyzer_type2_num_of_streams  = 16; /*ccm*/
        entry->packetGen.val.PacketGen_num_of_support_profile = 3;
        entry->packetGen.val.Pktgen_type1_num_of_streams = 16;
        entry->packetGen.val.Pktgen_type2_num_of_streams = 16;
        entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets
        /*******************************************************************************/
        entry->fragment.val.fragment_Edit_exist           = MEA_TRUE;
        entry->fragment.val.fragment_Edit_num_of_session  = 512;
        entry->fragment.val.fragment_Edit_num_of_port     = 1;
        entry->fragment.val.fragment_Edit_num_of_group    = 1;

        entry->fragment.val.fragment_vsp_exist           = MEA_TRUE;
        entry->fragment.val.fragment_vsp_num_of_session  = 512;
        entry->fragment.val.fragment_vsp_num_of_port     = 1;
        entry->fragment.val.fragment_vsp_num_of_group    = 1;

        entry->Interface_Tdm_A.val.exist = MEA_FALSE;
        entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_SBI;
        entry->Interface_Tdm_A.val.start_ts  = 0;
        entry->Interface_Tdm_A.val.num0fspe  = 12;

        entry->Interface_Tdm_B.val.exist = MEA_FALSE;
        entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
        entry->Interface_Tdm_B.val.start_ts  = 96;
        entry->Interface_Tdm_B.val.num0fspe  = 8;

        entry->Interface_Tdm_C.val.exist = MEA_FALSE;
        entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
        entry->Interface_Tdm_C.val.start_ts  =0;
        entry->Interface_Tdm_C.val.num0fspe =0; //6;

        entry->global_Tdm.val.num_of_ces = 384; 
        entry->global_Tdm.val.ts_size    =  512;  // need to * 32

#endif

        break;

    case MEA_OS_DEVICE_SIMULATION_3875_G999_1: // need to Update
#if 1
        entry->global.val.encription_enable            = MEA_TRUE;        
        entry->global.val.protection_enable            = MEA_TRUE;
        entry->global.val.bmf_martini_support          = MEA_TRUE; 
        
        
        entry->global.val.forwarder_external           = MEA_TRUE;
        entry->global.val.desc_external                = MEA_TRUE;
        entry->global.val.desc_checker_exist           = MEA_TRUE;
        entry->global.val.host_reduce                  = MEA_TRUE;
        entry->global.val.pause_support                = MEA_TRUE;
        entry->global.val.pre_sched_enable             = MEA_TRUE;

        entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
        entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;

        entry->global.val.fragment_exist               = MEA_TRUE;
       
        entry->global.val.ACM_exist                    = MEA_TRUE;
        entry->global.val.dual_latency_support         = MEA_TRUE;
        entry->global.val.fragment_bonding             = MEA_FALSE;
        entry->global.val.cpe_exist                    = MEA_TRUE;
        
        entry->global.val.tdm_enable                   = MEA_FALSE;
        entry->global.val.large_BM_ind_address         = MEA_TRUE;
     
        entry->global.val.protocolMapping              = MEA_TRUE;
        entry->global.val.cls_large                    = MEA_TRUE;
        entry->global.val.Support_1588                 = MEA_TRUE;
        entry->global.val.BM_bus_width                 =  1;  // 0-64bit 01-128bit 10-TBD 11 TBD 
        entry->global.val.mc_mqs_Support               = MEA_TRUE;
        entry->global.val.cluster_adminon_support      = MEA_TRUE;
        
        
        entry->global.val.fragment_9991                = MEA_TRUE;


        entry->global.val.fragment_exist                    = MEA_TRUE;
        entry->global.val.standard_bonding_support          = MEA_TRUE;
        entry->global.val.num_bonding_groupTx               = 16;
        entry->global.val.num_bonding_groupRx               = 1;
        


        
        /*only for ATM mode*/
        entry->pre_parser.val.exist                    = MEA_FALSE;
        entry->pre_parser.val.table_size               = 8; //TBD
        entry->pre_parser.val.vsp_exist                = MEA_FALSE;
        entry->pre_parser.val.cam_exist                = MEA_TRUE;

        /*******************************************************/
        entry->parser.val.exist                      = MEA_TRUE;
        entry->parser.val.parser_etype_cam           = MEA_FALSE; 
        entry->parser.val.table_size                 = 128; 
        /*******************************************************/

        entry->service.val.exist                     = MEA_TRUE;
        entry->service.val.table_size                = 1024;
        entry->service.val.serviceRangeExist         = MEA_TRUE;
        entry->service.val.num_service_hash          = 1;
        entry->service.val.ServiceRange_Num_of_range     =32;

        /*******************************************************/
        entry->acl.val.exist                         = MEA_TRUE;  
        entry->acl.val.table_size                    = 256;
        entry->acl.val.lxcp_act_width                 = 8; // 2^8 is 256 Action
        entry->acl.val.lxcp_prof_width                = 6; // 2^4 is 16 profile

        /*******************************************************/
        entry->forwarder_lmt.val.exist               = MEA_TRUE;
        entry->forwarder_lmt.val.table_size          = 512;

        /*******************************************************/
        entry->forwarder.val.exist               = MEA_TRUE;
        entry->forwarder.val.addres_size         = 23;      
        entry->forwarder.val.adm_support         = MEA_TRUE;
        num_of_forwarder_Action                 =   512;
        entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
        entry->forwarder.val.width_learn_action_id  =     12;
        entry->forwarder.val.width_learn_src_port =        7;   
        /*******************************************************/
        entry->shaper.val.num_of_shaper_profile=256; //64;  // for port and priQue and cluster

        entry->shaper.val.shaper_per_Priqueue_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_cluster_exist=MEA_TRUE;
        entry->shaper.val.shaper_per_port_exist=MEA_TRUE;

        /*******************************************************/
        entry->policer.val.exist                 = MEA_TRUE; 

        entry->policer.val.num_of_TMID = 512;  // 1024
        entry->policer.val.reduce                = MEA_TRUE;
        entry->policer.val.hidden_profile        = 3;
        entry->policer.val.num_of_acm            = 1;   // 1 when no ACM
        entry->policer.val.num_of_profile        = 64;
        entry->policer.val.num_of_wred_prof        = 8;
        entry->policer.val.num_of_wred_acm        = 1;

        /*******************************************************/
        entry->wfq.val.cluster_exist             = MEA_FALSE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
        entry->wfq.val.queue_exist               = MEA_FALSE;  /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
        entry->wfq.val.num_of_cluster                = 128;  // num of cluster
        entry->wfq.val.maxCluserperport              = 64;    //  32 if the HW support
        entry->wfq.val.clusterTo_multiport_exist  = MEA_FALSE; // only on bonding

        /*******************************************************/
        entry->action.val.exist                  = MEA_TRUE;
        entry->action.val.table_size             = 2304; 
        /*******************************************************/
        entry->xpermission.val.exist             = MEA_TRUE;
        entry->xpermission.val.xper_table_size = 512;
        entry->xpermission.val.num_clusterInGroup = 0; //1;
        /*******************************************************/
        entry->sar_machine.val.exist             = MEA_TRUE;
        entry->sar_machine.val.table_size        = 0;   
        /*******************************************************/
        entry->editor_BMF.val.atm_support           = MEA_TRUE;
        entry->editor_BMF.val.maritini_mac_support  = MEA_TRUE;
        entry->editor_BMF.val.router_support        = MEA_TRUE;
        num_of_edit = 4096;
        entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
        /*******************************************************/

        NumOfPmIds = 512;
        entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
        entry->pm.val.num0fBlocksForBytes = 1;
        entry->pm.val.num0fBlocksForPmPackets = 1;

        /*******************************************************/
        entry->packetGen.val.PacketGen_exist         = MEA_FALSE;
        entry->packetGen.val.Analyzer_exist          = MEA_FALSE;
        entry->packetGen.val.PacketGen_On_Port_num   = 120;
        entry->packetGen.val.Analyzer_On_Port_num    = 120;
        entry->packetGen.val.Analyzer_type1_num_of_streams  = 16;//16; /*lbm lbr*/
        entry->packetGen.val.Analyzer_type2_num_of_streams  = 16; /*ccm*/
        entry->packetGen.val.PacketGen_num_of_support_profile = 3;
        entry->packetGen.val.Pktgen_type1_num_of_streams = 16;
        entry->packetGen.val.Pktgen_type2_num_of_streams = 16;
        entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets
        /*******************************************************************************/
        entry->fragment.val.fragment_Edit_exist           = MEA_TRUE;
        entry->fragment.val.fragment_Edit_num_of_session  = 512;
        entry->fragment.val.fragment_Edit_num_of_port     = 1;
        entry->fragment.val.fragment_Edit_num_of_group    = 1;

        entry->fragment.val.fragment_vsp_exist           = MEA_TRUE;
        entry->fragment.val.fragment_vsp_num_of_session  = 512;
        entry->fragment.val.fragment_vsp_num_of_port     = 1;
        entry->fragment.val.fragment_vsp_num_of_group    = 1;

        entry->Interface_Tdm_A.val.exist = MEA_FALSE;
        entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_SBI;
        entry->Interface_Tdm_A.val.start_ts  = 0;
        entry->Interface_Tdm_A.val.num0fspe  = 12;

        entry->Interface_Tdm_B.val.exist = MEA_FALSE;
        entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
        entry->Interface_Tdm_B.val.start_ts  = 96;
        entry->Interface_Tdm_B.val.num0fspe  = 8;

        entry->Interface_Tdm_C.val.exist = MEA_FALSE;
        entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
        entry->Interface_Tdm_C.val.start_ts  =0;
        entry->Interface_Tdm_C.val.num0fspe =0; //6;

        entry->global_Tdm.val.num_of_ces = 384; 
        entry->global_Tdm.val.ts_size    =  512;  // need to * 32

#endif
        break;

        case MEA_OS_DEVICE_SIMULATION_Z7045_GW_AW:
#if 1
            entry->global.val.encription_enable = MEA_TRUE;
            entry->global.val.protection_enable = MEA_FALSE;
            entry->global.val.bmf_martini_support = MEA_FALSE;
            

            entry->global.val.forwarder_external = MEA_TRUE;
            entry->global.val.desc_external = MEA_TRUE;
            entry->global.val.desc_checker_exist = MEA_FALSE;
            entry->global.val.host_reduce = MEA_TRUE;
            entry->global.val.pause_support = MEA_TRUE;
            entry->global.val.pre_sched_enable = MEA_FALSE;

            entry->global.val.flowCos_Mapping_exist = MEA_TRUE;
            entry->global.val.flowMarking_Mapping_exist = MEA_TRUE;

            entry->global.val.fragment_exist = MEA_FALSE;
            entry->global.val.tx_chunk_size = 8;
           
            entry->global.val.ACM_exist = MEA_FALSE;
            entry->global.val.dual_latency_support = MEA_TRUE;
            entry->global.val.fragment_bonding = MEA_FALSE;
            entry->global.val.cpe_exist = MEA_FALSE;
            entry->global.val.dual_latency_support = MEA_FALSE;
            
           


            entry->global.val.tdm_enable = MEA_FALSE;
            entry->global.val.large_BM_ind_address = MEA_TRUE;
            
            entry->global.val.protocolMapping = MEA_FALSE;
            entry->global.val.cls_large = MEA_TRUE;
            entry->global.val.Support_1588 = MEA_FALSE;
            entry->global.val.BM_bus_width = 1;
            entry->global.val.rx_pause_support = MEA_FALSE;
            entry->global.val.lag_support = MEA_TRUE;
            entry->global.val.mstp_support = MEA_TRUE;
            entry->global.val.rstp_support = MEA_TRUE;

            entry->global.val.mc_mqs_Support = MEA_TRUE;
            entry->global.val.cluster_adminon_support = MEA_TRUE;


            entry->global.val.ingressMacfifo_small = MEA_TRUE;
            entry->global.val.AlloC_AllocR = MEA_FALSE;

            entry->global.val.MAC_LB_support = MEA_TRUE;
           
            entry->global.val.reduce_lq = MEA_FALSE;
            entry->global.val.reduce_parser_tdm = MEA_FALSE;
            entry->global.val.fragment_9991 = MEA_FALSE;
            entry->global.val.forworder_filter_support = MEA_FALSE;
            entry->global.val.serdesReset_support = MEA_FALSE;
            entry->global.val.autoneg_support = MEA_FALSE;

            entry->global.val.standard_bonding_support = MEA_TRUE;
            
            entry->global.val.rmonRx_drop_support = MEA_FALSE;
            entry->global.val.afdx_support = MEA_FALSE;
            entry->global.val.HeaderCompress_support = MEA_TRUE;
            entry->global.val.HeaderDeCompress_support = MEA_TRUE;
            entry->global.val.ac_MTU_support = MEA_TRUE;
            entry->global.val.PTP1588_support = MEA_TRUE;

            //-----------------------------------------------------
            entry->global.val.num_of_slice = 2;
            entry->global.val.num_bonding_groupTx = 16;
            entry->global.val.num_bonding_groupRx = 1;
            //-----------------------------------------------------
            entry->global.val.wbrg_support = MEA_TRUE;
            entry->global.val.ECN_support = MEA_TRUE;
            entry->global.val.Swap_machine_enable = MEA_TRUE;


            /************************************************************/
            
            entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
            entry->global.val.GATEWAY_EN = MEA_TRUE;
            entry->global.val.vpls_access_enable = MEA_TRUE;


            entry->global1.val.ACTION_ON_DDR = MEA_FALSE;
            entry->global1.val.EDITING_ON_DDR = MEA_FALSE;
            entry->global1.val.PM_ON_DDR = MEA_TRUE;
            entry->global1.val.SERVICE_EXTERNAL = MEA_TRUE;
            entry->global1.val.FWD_IS_256 = MEA_FALSE;
            entry->global1.val.hierarchical_en = MEA_TRUE;
          
       







            /*******************************************************/
            /*only for ATM mode*/
            entry->pre_parser.val.exist = MEA_FALSE;
            entry->pre_parser.val.table_size = 0; //TBD
            entry->pre_parser.val.vsp_exist = MEA_FALSE;

            /*******************************************************/
            entry->parser.val.exist = MEA_TRUE;
            entry->parser.val.parser_etype_cam = MEA_FALSE;
            entry->parser.val.table_size = 128;
            entry->parser.val.numofBit = 216;
            /*******************************************************/

            entry->service.val.exist = MEA_TRUE;
            entry->service.val.table_size = 512;
            entry->service.val.serviceRangeExist = MEA_TRUE;
            entry->service.val.num_service_hash = 0;
            entry->service.val.ServiceRange_Num_of_range = 8;
            entry->service.val.numofRangeProfile = 32;
            entry->service.val.EVC_Classifier_exist = MEA_TRUE;
            entry->service.val.EVC_classifier_key_width = 24;
            entry->service.val.EVC_Classifier_num_hash_group = 0;
            
            entry->service.val.srv_externl_size = 4;

            /*******************************************************/
            entry->acl.val.exist = MEA_TRUE;
            entry->acl.val.table_size = 256;
            entry->acl.val.lxcp_act_width = 8; // 2^8 is 256 Action
            entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile

            /*******************************************************/
            entry->forwarder_lmt.val.exist = MEA_TRUE;
            entry->forwarder_lmt.val.table_size = 512;

            /*******************************************************/
            entry->forwarder.val.exist = MEA_TRUE;
            entry->forwarder.val.addres_size = 23;
            entry->forwarder.val.adm_support = MEA_TRUE;
            num_of_forwarder_Action = 16384;//8192;
            entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
            entry->forwarder.val.width_learn_action_id = 15;
            entry->forwarder.val.width_learn_src_port = 9;
            entry->global.val.forworder_filter_support = MEA_TRUE;
            /*******************************************************/
            entry->shaper.val.shaper_per_port_exist = MEA_TRUE;
            entry->shaper.val.shaper_per_cluster_exist = MEA_TRUE;
            entry->shaper.val.shaper_per_Priqueue_exist = MEA_TRUE;
            entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster



            /*******************************************************/
            entry->policer.val.exist = MEA_TRUE;

            entry->policer.val.reduce = MEA_TRUE;
            entry->policer.val.num_of_TMID = 512;  // 1024

            entry->policer.val.hidden_profile = 1;
            entry->policer.val.num_of_acm = 1;   // 1 when no ACM
            entry->policer.val.num_of_profile = 128; //64;
            entry->policer.val.num_of_wred_prof = 1;
            entry->policer.val.num_of_wred_acm = 1;
            entry->policer.val.Ingress_flow_num_of_prof = 11;//lort 2048;

            /*******************************************************/
            entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
            entry->wfq.val.queue_exist = MEA_TRUE;
            entry->wfq.val.num_of_cluster = 256;  // num of cluster
            entry->wfq.val.maxCluserperport = 32;    //  32 if the HW support
            entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding


            /*******************************************************/
            entry->action.val.exist = MEA_TRUE;
            entry->action.val.table_size = 164096;

                //(entry->service.val.table_size + num_of_forwarder_Action + entry->acl.val.table_size);
            /*******************************************************/
            entry->xpermission.val.exist = MEA_TRUE;
            entry->xpermission.val.xper_table_size = 512; //1024;

            if (entry->global.val.mstp_support == MEA_TRUE)
                entry->xpermission.val.num_ofMSTP = 9;   /*512*/
            else{
                if (entry->global.val.rstp_support == MEA_TRUE)
                    entry->xpermission.val.num_ofMSTP = 1;   /*1 */
            }
            entry->xpermission.val.num_lag_prof = 8;


            /*******************************************************/
            entry->sar_machine.val.exist = MEA_FALSE;
            entry->sar_machine.val.table_size = 0;
            /*******************************************************/
            
            num_of_edit = 16384;
            entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
            entry->editor_BMF.val.atm_support = MEA_FALSE;
            entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
            entry->editor_BMF.val.router_support = MEA_FALSE;
            entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
            entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
            entry->editor_BMF.val.num_of_lm_count = 16;

            entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
            entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

            /*******************************************************/
            NumOfPmIds = 8196;
            entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
            entry->pm.val.num0fBlocksForBytes = 1;
            entry->pm.val.num0fBlocksForPmPackets = 1;

            /*******************************************************/
            entry->packetGen.val.PacketGen_exist = MEA_TRUE;
            entry->packetGen.val.Analyzer_exist = MEA_TRUE;
            entry->packetGen.val.PacketGen_On_Port_num = 120;
            entry->packetGen.val.Analyzer_On_Port_num = 120;


            entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
            entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
            entry->packetGen.val.PacketGen_num_of_support_profile = 8;
            entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
            entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
            entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets


            /***************************************************************/



            /**************************************************************/
            entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
            entry->fragment.val.fragment_Edit_num_of_session = 0;
            entry->fragment.val.fragment_Edit_num_of_port = 0;
            entry->fragment.val.fragment_Edit_num_of_group = 0;

            entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
            entry->fragment.val.fragment_vsp_num_of_session = 0;
            entry->fragment.val.fragment_vsp_num_of_port = 0;
            entry->fragment.val.fragment_vsp_num_of_group = 0;
            /*******************************************************************************/
            entry->Interface_Tdm_A.val.exist = MEA_FALSE;
            entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
            entry->Interface_Tdm_A.val.start_ts = 0;
            entry->Interface_Tdm_A.val.num0fspe = 12;

            entry->Interface_Tdm_B.val.exist = MEA_FALSE;
            entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
            entry->Interface_Tdm_B.val.start_ts = 96;
            entry->Interface_Tdm_B.val.num0fspe = 8;

            entry->Interface_Tdm_C.val.exist = MEA_FALSE;
            entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
            entry->Interface_Tdm_C.val.start_ts = 0;
            entry->Interface_Tdm_C.val.num0fspe = 0; //6;

            entry->global_Tdm.val.num_of_ces = 384;
            entry->global_Tdm.val.ts_size = 512;  // need to * 32
            /************************************************************************/


            entry->Hc_Classifier.val.Hc_classifier_exist = MEA_TRUE;
            entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

            entry->Hc_Classifier.val.Num_of_hcid = 32;
            entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
            entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
            entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

            entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

            entry->slice0.index_40_regs[0] = 0x00ffffff;
            entry->slice0.index_40_regs[1] = 0;
            entry->slice0.index_40_regs[2] = 0;
            entry->slice0.index_40_regs[3] = 0x0000f000;

            entry->slice1.index_41_regs[0] = 0;
            entry->slice1.index_41_regs[1] = 0;
            entry->slice1.index_41_regs[2] = 0;
            entry->slice1.index_41_regs[3] = 0x810003f0;







#endif
            break;
            case MEA_OS_DEVICE_SIMULATION_Z7015_2G_1Radio:
#if 1
                entry->global.val.encription_enable = MEA_TRUE;
                entry->global.val.protection_enable = MEA_FALSE;
                entry->global.val.bmf_martini_support = MEA_FALSE;
                
                entry->global.val.rstp_support = MEA_FALSE;
                entry->global.val.forwarder_external = MEA_TRUE;
                entry->global.val.desc_external = MEA_FALSE;
                entry->global.val.desc_checker_exist = MEA_FALSE;
                entry->global.val.host_reduce = MEA_TRUE;
                entry->global.val.pause_support = MEA_FALSE;
                entry->global.val.pre_sched_enable = MEA_FALSE;

                entry->global.val.flowCos_Mapping_exist = MEA_FALSE;
                entry->global.val.flowMarking_Mapping_exist = MEA_FALSE;

                entry->global.val.fragment_exist = MEA_FALSE;
                entry->global.val.tx_chunk_size = 7;
                
                entry->global.val.ACM_exist = MEA_FALSE;
                entry->global.val.dual_latency_support = MEA_FALSE;
                entry->global.val.fragment_bonding = MEA_FALSE;
                entry->global.val.cpe_exist = MEA_FALSE;
                entry->global.val.dual_latency_support = MEA_FALSE;
               


                entry->global.val.tdm_enable = MEA_FALSE;
                entry->global.val.large_BM_ind_address = MEA_TRUE;
                
                entry->global.val.protocolMapping = MEA_FALSE;
                entry->global.val.cls_large = MEA_TRUE;
                entry->global.val.Support_1588 = MEA_FALSE;
                entry->global.val.BM_bus_width = 0;
                entry->global.val.rx_pause_support = MEA_FALSE;
                entry->global.val.lag_support = MEA_TRUE;
                entry->global.val.mstp_support = MEA_TRUE;


                entry->global.val.mc_mqs_Support = MEA_TRUE;
                entry->global.val.cluster_adminon_support = MEA_TRUE;


                entry->global.val.ingressMacfifo_small = MEA_TRUE;
                entry->global.val.AlloC_AllocR = MEA_FALSE;

                entry->global.val.MAC_LB_support = MEA_TRUE;
                
                entry->global.val.reduce_lq = MEA_FALSE;
                entry->global.val.reduce_parser_tdm = MEA_FALSE;
                entry->global.val.fragment_9991 = MEA_FALSE;
                entry->global.val.forworder_filter_support = MEA_FALSE;
                entry->global.val.serdesReset_support = MEA_FALSE;
                entry->global.val.autoneg_support = MEA_FALSE;

                entry->global.val.standard_bonding_support = MEA_TRUE;
               
                entry->global.val.rmonRx_drop_support = MEA_FALSE;
                entry->global.val.afdx_support = MEA_FALSE;
                entry->global.val.HeaderCompress_support = MEA_TRUE;
                entry->global.val.HeaderDeCompress_support = MEA_TRUE;
                entry->global.val.ac_MTU_support = MEA_TRUE;
                entry->global.val.PTP1588_support = MEA_TRUE;

                //-----------------------------------------------------
                entry->global.val.num_of_slice = 1;
                entry->global.val.num_bonding_groupTx = 0;
                entry->global.val.num_bonding_groupRx = 0;
                //-----------------------------------------------------
                entry->global.val.wbrg_support = MEA_FALSE;
                entry->global.val.ECN_support = MEA_FALSE;
                entry->global.val.Swap_machine_enable = MEA_TRUE;
                entry->global.val.num_of_port_Ingress = 0; /*0-128 1-256 2-512 3-1024 */


                /************************************************************/
                
                entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
                entry->global.val.GATEWAY_EN = MEA_FALSE;
                entry->global.val.vpls_access_enable = MEA_TRUE;

                //entry->global1.val.sys_M         = 40;
                //entry->global1.val.sys_N         = 10;
                //entry->global1.val.pushClk       = 40;
                entry->global1.val.ACTION_ON_DDR = MEA_TRUE;
                entry->global1.val.EDITING_ON_DDR = MEA_FALSE;
                entry->global1.val.PM_ON_DDR = MEA_TRUE;
                entry->global1.val.SERVICE_EXTERNAL = MEA_TRUE;
                entry->global1.val.FWD_IS_256 = MEA_FALSE;
                entry->global1.val.hierarchical_en = MEA_FALSE;

                entry->global1.val.instanc_fifo_1588 = 2;
                entry->global1.val.max_numof_descriptor = 3;  /*for 4K     2^(3-1)x1024   */




                                                              /*only for ATM mode*/
                entry->pre_parser.val.exist = MEA_FALSE;
                entry->pre_parser.val.table_size = 0; //TBD
                entry->pre_parser.val.vsp_exist = MEA_FALSE;

                /*******************************************************/
                entry->parser.val.exist = MEA_TRUE;
                entry->parser.val.parser_etype_cam = MEA_FALSE;
                entry->parser.val.table_size = 128;
                entry->parser.val.numofBit = 180;
                /*******************************************************/

                entry->service.val.exist = MEA_TRUE;
                entry->service.val.table_size = 512;
                entry->service.val.serviceRangeExist = MEA_FALSE;
                entry->service.val.num_service_hash = 0;
                entry->service.val.ServiceRange_Num_of_range = 8;
                entry->service.val.numofRangeProfile = 32;
                entry->service.val.EVC_Classifier_exist = MEA_TRUE;
                entry->service.val.EVC_classifier_key_width = 24;
                entry->service.val.EVC_Classifier_num_hash_group = 0;

                entry->service.val.srv_externl_size = 4;

                /*******************************************************/
                entry->acl.val.exist = MEA_FALSE;
                entry->acl.val.table_size = 256;
                entry->acl.val.lxcp_act_width = 9; // 2^8 is 256 Action
                entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile

                                                    /*******************************************************/
                entry->forwarder_lmt.val.exist = MEA_FALSE;
                entry->forwarder_lmt.val.table_size = 512;

                /*******************************************************/
                entry->forwarder.val.exist = MEA_TRUE;
                entry->forwarder.val.addres_size = 23;
                entry->forwarder.val.adm_support = MEA_TRUE;
                num_of_forwarder_Action = 512;
                entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
                entry->forwarder.val.width_learn_action_id = 10;
                entry->forwarder.val.width_learn_src_port = 10;
                entry->global.val.forworder_filter_support = MEA_TRUE;
                /*******************************************************/
                entry->shaper.val.shaper_per_port_exist = MEA_FALSE;
                entry->shaper.val.shaper_per_cluster_exist = MEA_FALSE;
                entry->shaper.val.shaper_per_Priqueue_exist = MEA_FALSE;
                entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster



                                                                /*******************************************************/
                entry->policer.val.exist = MEA_TRUE;

                entry->policer.val.reduce = MEA_TRUE;
                entry->policer.val.num_of_TMID = 512;  // 1024

                entry->policer.val.hidden_profile = 1;
                entry->policer.val.num_of_acm = 1;   // 1 when no ACM
                entry->policer.val.num_of_profile = 128; //64;
                entry->policer.val.num_of_wred_prof = 1;
                entry->policer.val.num_of_wred_acm = 1;
                entry->policer.val.Ingress_flow_exist = MEA_TRUE;
                entry->policer.val.Ingress_flow_num_of_prof = 1;  //2^11 = 2048;

                                                                  /*******************************************************/
                entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
                entry->wfq.val.queue_exist = MEA_TRUE;
                entry->wfq.val.num_of_cluster = 64;  // num of cluster
                entry->wfq.val.maxCluserperport = 8;    //  32 if the HW support
                entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding


                                                                      /*******************************************************/
                entry->action.val.exist = MEA_TRUE;
                entry->action.val.table_size = (num_of_forwarder_Action * 2 + entry->acl.val.table_size);
                /*******************************************************/
                entry->xpermission.val.exist = MEA_TRUE;
                entry->xpermission.val.xper_table_size = 512; //1024;

                if (entry->global.val.mstp_support == MEA_TRUE)
                    entry->xpermission.val.num_ofMSTP = 9;   /*512*/
                else {
                    if (entry->global.val.rstp_support == MEA_TRUE)
                        entry->xpermission.val.num_ofMSTP = 1;   /*1 */
                }
                entry->xpermission.val.num_lag_prof = 0;


                /*******************************************************/
                entry->sar_machine.val.exist = MEA_FALSE;
                entry->sar_machine.val.table_size = 0;
                /*******************************************************/
                num_of_edit = 512;
                entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
                entry->editor_BMF.val.atm_support = MEA_FALSE;
                entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
                entry->editor_BMF.val.router_support = MEA_FALSE;
                entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
                entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
                entry->editor_BMF.val.num_of_lm_count = 0;

                entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
                entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

                /*******************************************************/
                NumOfPmIds = 512;
                entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
                entry->pm.val.num0fBlocksForBytes = 1;
                entry->pm.val.num0fBlocksForPmPackets = 1;
                /*******************************************************/
                entry->packetGen.val.PacketGen_exist = MEA_FALSE;
                entry->packetGen.val.Analyzer_exist = MEA_FALSE;
                entry->packetGen.val.PacketGen_On_Port_num = 120;
                entry->packetGen.val.Analyzer_On_Port_num = 120;


                entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
                entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
                entry->packetGen.val.PacketGen_num_of_support_profile = 8;
                entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
                entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
                entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets

                entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
                entry->fragment.val.fragment_Edit_num_of_session = 0;
                entry->fragment.val.fragment_Edit_num_of_port = 0;
                entry->fragment.val.fragment_Edit_num_of_group = 0;

                entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
                entry->fragment.val.fragment_vsp_num_of_session = 0;
                entry->fragment.val.fragment_vsp_num_of_port = 0;
                entry->fragment.val.fragment_vsp_num_of_group = 0;
                /*******************************************************************************/
                entry->Interface_Tdm_A.val.exist = MEA_FALSE;
                entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
                entry->Interface_Tdm_A.val.start_ts = 0;
                entry->Interface_Tdm_A.val.num0fspe = 12;

                entry->Interface_Tdm_B.val.exist = MEA_FALSE;
                entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
                entry->Interface_Tdm_B.val.start_ts = 96;
                entry->Interface_Tdm_B.val.num0fspe = 8;

                entry->Interface_Tdm_C.val.exist = MEA_FALSE;
                entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
                entry->Interface_Tdm_C.val.start_ts = 0;
                entry->Interface_Tdm_C.val.num0fspe = 0; //6;

                entry->global_Tdm.val.num_of_ces = 384;
                entry->global_Tdm.val.ts_size = 512;
                // need to * 32
                /************************************************************************/


                entry->Hc_Classifier.val.Hc_classifier_exist = MEA_TRUE;
                entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

                entry->Hc_Classifier.val.Num_of_hcid = 32;
                entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
                entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
                entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

                entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

                entry->slice0.index_40_regs[0] = 0;
                entry->slice0.index_40_regs[1] = 0;
                entry->slice0.index_40_regs[2] = 0;
                entry->slice0.index_40_regs[3] = 0;

                entry->slice1.index_41_regs[0] = 0;
                entry->slice1.index_41_regs[1] = 0;
                entry->slice1.index_41_regs[2] = 0;
                entry->slice1.index_41_regs[3] = 0;

                entry->slice2.index_42_regs[0] = 0;
                entry->slice2.index_42_regs[1] = 0;
                entry->slice2.index_42_regs[2] = 0;
                entry->slice2.index_42_regs[3] = 0;

#endif
                break;

   case MEA_OS_DEVICE_SIMULATION_Z7015_GFAST:
#if 1
       entry->global.val.encription_enable      = MEA_TRUE;
       entry->global.val.protection_enable      = MEA_FALSE;
       entry->global.val.bmf_martini_support    = MEA_FALSE;
       
       entry->global.val.rstp_support           = MEA_FALSE;
       entry->global.val.forwarder_external     = MEA_TRUE;
       entry->global.val.desc_external          = MEA_FALSE;
       entry->global.val.desc_checker_exist     = MEA_FALSE;
       entry->global.val.host_reduce            = MEA_TRUE;
       entry->global.val.pause_support          = MEA_FALSE;
       entry->global.val.pre_sched_enable       = MEA_FALSE;

       entry->global.val.flowCos_Mapping_exist     = MEA_FALSE;
       entry->global.val.flowMarking_Mapping_exist = MEA_FALSE;

       entry->global.val.fragment_exist             = MEA_FALSE;
       entry->global.val.tx_chunk_size              = 7;
    
       entry->global.val.ACM_exist                  = MEA_FALSE;
       entry->global.val.dual_latency_support       = MEA_FALSE;
       entry->global.val.fragment_bonding           = MEA_FALSE;
       entry->global.val.cpe_exist = MEA_FALSE;
       entry->global.val.dual_latency_support       = MEA_FALSE;
      


       entry->global.val.tdm_enable                 = MEA_FALSE;
       entry->global.val.large_BM_ind_address       = MEA_TRUE;
       
       entry->global.val.protocolMapping            = MEA_FALSE;
       entry->global.val.cls_large                  = MEA_TRUE;
       entry->global.val.Support_1588               = MEA_FALSE;
       entry->global.val.BM_bus_width               = 0;
       entry->global.val.rx_pause_support           = MEA_FALSE;
       entry->global.val.lag_support                = MEA_TRUE;
       entry->global.val.mstp_support               = MEA_TRUE;
       

       entry->global.val.mc_mqs_Support             = MEA_TRUE;
       entry->global.val.cluster_adminon_support    = MEA_TRUE;


       entry->global.val.ingressMacfifo_small       = MEA_TRUE;
       entry->global.val.AlloC_AllocR               = MEA_FALSE;

       entry->global.val.MAC_LB_support             = MEA_TRUE;
       
       entry->global.val.reduce_lq                  = MEA_FALSE;
       entry->global.val.reduce_parser_tdm          = MEA_FALSE;
       entry->global.val.fragment_9991              = MEA_FALSE;
       entry->global.val.forworder_filter_support   = MEA_FALSE;
       entry->global.val.serdesReset_support        = MEA_FALSE;
       entry->global.val.autoneg_support            = MEA_FALSE;

       entry->global.val.standard_bonding_support   = MEA_TRUE;
      
       entry->global.val.rmonRx_drop_support        = MEA_FALSE;
       entry->global.val.afdx_support               = MEA_FALSE;
       entry->global.val.HeaderCompress_support     = MEA_TRUE;
       entry->global.val.HeaderDeCompress_support   = MEA_TRUE;
       entry->global.val.ac_MTU_support             = MEA_TRUE;
       entry->global.val.PTP1588_support            = MEA_TRUE;

       //-----------------------------------------------------
       entry->global.val.num_of_slice = 1;
       entry->global.val.num_bonding_groupTx = 0;
       entry->global.val.num_bonding_groupRx = 0;
       //-----------------------------------------------------
       entry->global.val.wbrg_support = MEA_FALSE;
       entry->global.val.ECN_support = MEA_FALSE;
       entry->global.val.Swap_machine_enable = MEA_TRUE;
       entry->global.val.num_of_port_Ingress = 0; /*0-128 1-256 2-512 3-1024 */


       /************************************************************/
       
       entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
       entry->global.val.GATEWAY_EN = MEA_FALSE;
       entry->global.val.vpls_access_enable = MEA_TRUE;

       //entry->global1.val.sys_M         = 40;
       //entry->global1.val.sys_N         = 10;
       //entry->global1.val.pushClk       = 40;
       entry->global1.val.ACTION_ON_DDR = MEA_TRUE;
       entry->global1.val.EDITING_ON_DDR = MEA_FALSE;
       entry->global1.val.PM_ON_DDR = MEA_TRUE;
       entry->global1.val.SERVICE_EXTERNAL = MEA_TRUE;
       entry->global1.val.FWD_IS_256 = MEA_FALSE;
       entry->global1.val.hierarchical_en = MEA_FALSE;

       entry->global1.val.instanc_fifo_1588 = 2;
       entry->global1.val.max_numof_descriptor = 3;  /*for 4K     2^(3-1)x1024   */




                                                     /*only for ATM mode*/
       entry->pre_parser.val.exist = MEA_FALSE;
       entry->pre_parser.val.table_size = 0; //TBD
       entry->pre_parser.val.vsp_exist = MEA_FALSE;

       /*******************************************************/
       entry->parser.val.exist = MEA_TRUE;
       entry->parser.val.parser_etype_cam = MEA_FALSE;
       entry->parser.val.table_size = 128;
       entry->parser.val.numofBit = 180;
       /*******************************************************/

       entry->service.val.exist = MEA_TRUE;
       entry->service.val.table_size = 512;
       entry->service.val.serviceRangeExist = MEA_FALSE;
       entry->service.val.num_service_hash = 0;
       entry->service.val.ServiceRange_Num_of_range = 8;
       entry->service.val.numofRangeProfile = 32;
       entry->service.val.EVC_Classifier_exist = MEA_TRUE;
       entry->service.val.EVC_classifier_key_width = 24;
       entry->service.val.EVC_Classifier_num_hash_group = 0;

       entry->service.val.srv_externl_size = 4;

       /*******************************************************/
       entry->acl.val.exist = MEA_FALSE;
       entry->acl.val.table_size = 256;
       entry->acl.val.lxcp_act_width = 9; // 2^8 is 256 Action
       entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile

      /*******************************************************/
       entry->forwarder_lmt.val.exist = MEA_FALSE;
       entry->forwarder_lmt.val.table_size = 512;

       /*******************************************************/
       entry->forwarder.val.exist = MEA_TRUE;
       entry->forwarder.val.addres_size = 23;
       entry->forwarder.val.adm_support = MEA_TRUE;
       num_of_forwarder_Action = 512;
       entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
       entry->forwarder.val.width_learn_action_id = 10;
       entry->forwarder.val.width_learn_src_port = 10;
       entry->global.val.forworder_filter_support = MEA_TRUE;
       /*******************************************************/
       entry->shaper.val.shaper_per_port_exist = MEA_FALSE;
       entry->shaper.val.shaper_per_cluster_exist = MEA_FALSE;
       entry->shaper.val.shaper_per_Priqueue_exist = MEA_FALSE;
       entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster



       /*******************************************************/
       entry->policer.val.exist = MEA_TRUE;

       entry->policer.val.reduce = MEA_TRUE;
       entry->policer.val.num_of_TMID = 512;  // 1024

       entry->policer.val.hidden_profile = 1;
       entry->policer.val.num_of_acm = 1;   // 1 when no ACM
       entry->policer.val.num_of_profile = 128; //64;
       entry->policer.val.num_of_wred_prof = 1;
       entry->policer.val.num_of_wred_acm = 1;
       entry->policer.val.Ingress_flow_exist = MEA_TRUE;
       entry->policer.val.Ingress_flow_num_of_prof = 1;  //2^11 = 2048;

      /*******************************************************/
       entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
       entry->wfq.val.queue_exist = MEA_TRUE;
       entry->wfq.val.num_of_cluster = 64;  // num of cluster
       entry->wfq.val.maxCluserperport = 8;    //  32 if the HW support
       entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding


      /*******************************************************/
       entry->action.val.exist = MEA_TRUE;
       entry->action.val.table_size = (num_of_forwarder_Action * 2 + entry->acl.val.table_size);
       /*******************************************************/
       entry->xpermission.val.exist = MEA_TRUE;
       entry->xpermission.val.xper_table_size = 512; //1024;

       if (entry->global.val.mstp_support == MEA_TRUE)
           entry->xpermission.val.num_ofMSTP = 9;   /*512*/
       else {
           if (entry->global.val.rstp_support == MEA_TRUE)
               entry->xpermission.val.num_ofMSTP = 1;   /*1 */
       }
       entry->xpermission.val.num_lag_prof = 0;


       /*******************************************************/
       entry->sar_machine.val.exist = MEA_FALSE;
       entry->sar_machine.val.table_size = 0;
       /*******************************************************/
       num_of_edit = 512;
       entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
       entry->editor_BMF.val.atm_support = MEA_FALSE;
       entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
       entry->editor_BMF.val.router_support = MEA_FALSE;
       entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
       entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
       entry->editor_BMF.val.num_of_lm_count = 0;

       entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
       entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

       /*******************************************************/
       NumOfPmIds = 512;
       entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
       entry->pm.val.num0fBlocksForBytes = 1;
       entry->pm.val.num0fBlocksForPmPackets = 1;
       /*******************************************************/
       entry->packetGen.val.PacketGen_exist = MEA_FALSE;
       entry->packetGen.val.Analyzer_exist = MEA_FALSE;
       entry->packetGen.val.PacketGen_On_Port_num = 120;
       entry->packetGen.val.Analyzer_On_Port_num = 120;


       entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
       entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
       entry->packetGen.val.PacketGen_num_of_support_profile = 8;
       entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
       entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
       entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets

       entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
       entry->fragment.val.fragment_Edit_num_of_session = 0;
       entry->fragment.val.fragment_Edit_num_of_port = 0;
       entry->fragment.val.fragment_Edit_num_of_group = 0;

       entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
       entry->fragment.val.fragment_vsp_num_of_session = 0;
       entry->fragment.val.fragment_vsp_num_of_port = 0;
       entry->fragment.val.fragment_vsp_num_of_group = 0;
       /*******************************************************************************/
       entry->Interface_Tdm_A.val.exist = MEA_FALSE;
       entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
       entry->Interface_Tdm_A.val.start_ts = 0;
       entry->Interface_Tdm_A.val.num0fspe = 12;

       entry->Interface_Tdm_B.val.exist = MEA_FALSE;
       entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
       entry->Interface_Tdm_B.val.start_ts = 96;
       entry->Interface_Tdm_B.val.num0fspe = 8;

       entry->Interface_Tdm_C.val.exist = MEA_FALSE;
       entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
       entry->Interface_Tdm_C.val.start_ts = 0;
       entry->Interface_Tdm_C.val.num0fspe = 0; //6;

       entry->global_Tdm.val.num_of_ces = 384;
       entry->global_Tdm.val.ts_size = 512;  
       // need to * 32
       /************************************************************************/


       entry->Hc_Classifier.val.Hc_classifier_exist = MEA_TRUE;
       entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

       entry->Hc_Classifier.val.Num_of_hcid = 32;
       entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
       entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
       entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

       entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

       entry->slice0.index_40_regs[0] = 0;
       entry->slice0.index_40_regs[1] = 0;
       entry->slice0.index_40_regs[2] = 0;
       entry->slice0.index_40_regs[3] = 0;

       entry->slice1.index_41_regs[0] = 0;
       entry->slice1.index_41_regs[1] = 0;
       entry->slice1.index_41_regs[2] = 0;
       entry->slice1.index_41_regs[3] = 0;

       entry->slice2.index_42_regs[0] = 0;
       entry->slice2.index_42_regs[1] = 0;
       entry->slice2.index_42_regs[2] = 0;
       entry->slice2.index_42_regs[3] = 0;







#endif
       break;

       case MEA_OS_DEVICE_SIMULATION_Z7035_20G_24GFAST:
#if 1
           entry->global.val.encription_enable = MEA_TRUE;
           entry->global.val.protection_enable = MEA_FALSE;
           entry->global.val.bmf_martini_support = MEA_FALSE;

           entry->global.val.rstp_support = MEA_FALSE;
           entry->global.val.forwarder_external = MEA_TRUE;
           entry->global.val.desc_external = MEA_FALSE;
           entry->global.val.desc_checker_exist = MEA_FALSE;
           entry->global.val.host_reduce = MEA_TRUE;
           entry->global.val.pause_support = MEA_FALSE;
           entry->global.val.pre_sched_enable = MEA_FALSE;

           entry->global.val.flowCos_Mapping_exist = MEA_FALSE;
           entry->global.val.flowMarking_Mapping_exist = MEA_FALSE;

           entry->global.val.fragment_exist = MEA_FALSE;
           entry->global.val.tx_chunk_size = 7;

           entry->global.val.ACM_exist = MEA_FALSE;
           entry->global.val.dual_latency_support = MEA_FALSE;
           entry->global.val.fragment_bonding = MEA_FALSE;
           entry->global.val.cpe_exist = MEA_FALSE;
           entry->global.val.dual_latency_support = MEA_FALSE;
           


           entry->global.val.tdm_enable = MEA_FALSE;
           entry->global.val.large_BM_ind_address = MEA_TRUE;

           entry->global.val.protocolMapping = MEA_FALSE;
           entry->global.val.cls_large = MEA_TRUE;
           entry->global.val.Support_1588 = MEA_FALSE;
           entry->global.val.BM_bus_width = 0;
           entry->global.val.rx_pause_support = MEA_FALSE;
           entry->global.val.lag_support = MEA_TRUE;
           entry->global.val.mstp_support = MEA_TRUE;


           entry->global.val.mc_mqs_Support = MEA_TRUE;
           entry->global.val.cluster_adminon_support = MEA_TRUE;


           entry->global.val.ingressMacfifo_small = MEA_TRUE;
           entry->global.val.AlloC_AllocR = MEA_FALSE;

           entry->global.val.MAC_LB_support = MEA_TRUE;

           entry->global.val.reduce_lq = MEA_FALSE;
           entry->global.val.reduce_parser_tdm = MEA_FALSE;
           entry->global.val.fragment_9991 = MEA_FALSE;
           entry->global.val.forworder_filter_support = MEA_FALSE;
           entry->global.val.serdesReset_support = MEA_FALSE;
           entry->global.val.autoneg_support = MEA_FALSE;

           entry->global.val.standard_bonding_support = MEA_TRUE;

           entry->global.val.rmonRx_drop_support = MEA_FALSE;
           entry->global.val.afdx_support = MEA_FALSE;
           entry->global.val.HeaderCompress_support = MEA_TRUE;
           entry->global.val.HeaderDeCompress_support = MEA_TRUE;
           entry->global.val.ac_MTU_support = MEA_TRUE;
           entry->global.val.PTP1588_support = MEA_TRUE;

           //-----------------------------------------------------
           entry->global.val.num_of_slice = 5;
           entry->global.val.num_bonding_groupTx = 12;
           entry->global.val.num_bonding_groupRx = 12;
           //-----------------------------------------------------
           entry->global.val.wbrg_support = MEA_FALSE;
           entry->global.val.ECN_support = MEA_FALSE;
           entry->global.val.Swap_machine_enable = MEA_TRUE;
           entry->global.val.num_of_port_Ingress = 0; /*0-128 1-256 2-512 3-1024 */

           /************************************************************/

           entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
           entry->global.val.GATEWAY_EN = MEA_FALSE;
           entry->global.val.vpls_access_enable = MEA_TRUE;

           entry->global1.val.sys_M         = 34;
           entry->global1.val.sys_N         = 5;
           entry->global1.val.pushClk       = 40;
           entry->global1.val.ACTION_ON_DDR = MEA_FALSE;
           entry->global1.val.EDITING_ON_DDR = MEA_FALSE;
           entry->global1.val.PM_ON_DDR = MEA_TRUE;
           entry->global1.val.SERVICE_EXTERNAL = MEA_FALSE;
           entry->global1.val.FWD_IS_256 = MEA_FALSE;
           entry->global1.val.hierarchical_en = MEA_FALSE;

           entry->global1.val.instanc_fifo_1588 = 2;
           entry->global1.val.max_numof_descriptor = 5;  /*for 4K     2^(3-1)x1024   */
           entry->global1.val.max_numof_DataBuffer = 5;
           entry->global1.val.max_point_2_multipoint = 512;



                                                         /*only for ATM mode*/
           entry->pre_parser.val.exist = MEA_FALSE;
           entry->pre_parser.val.table_size = 0; //TBD
           entry->pre_parser.val.vsp_exist = MEA_FALSE;

           /*******************************************************/
           entry->parser.val.exist = MEA_TRUE;
           entry->parser.val.parser_etype_cam = MEA_FALSE;
           entry->parser.val.table_size = 128;
           entry->parser.val.numofBit = 180;
           /*******************************************************/

           entry->service.val.exist = MEA_TRUE;
           entry->service.val.table_size = 512;
           entry->service.val.serviceRangeExist = MEA_FALSE;
           entry->service.val.num_service_hash = 0;
           entry->service.val.ServiceRange_Num_of_range = 8;
           entry->service.val.numofRangeProfile = 32;
           entry->service.val.EVC_Classifier_exist = MEA_TRUE;
           entry->service.val.EVC_classifier_key_width = 24;
           entry->service.val.EVC_Classifier_num_hash_group = 0;

           entry->service.val.srv_externl_size = 4;

           /*******************************************************/
           entry->acl.val.exist = MEA_FALSE;
           entry->acl.val.table_size = 256;
           entry->acl.val.lxcp_act_width = 9; // 2^8 is 256 Action
           entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile

           /*******************************************************/
           entry->forwarder_lmt.val.exist = MEA_FALSE;
           entry->forwarder_lmt.val.table_size = 512;

           /*******************************************************/
           entry->forwarder.val.exist = MEA_TRUE;
           entry->forwarder.val.addres_size = 23;
           entry->forwarder.val.adm_support = MEA_TRUE;
           num_of_forwarder_Action = 512;
           entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
           entry->forwarder.val.width_learn_action_id = 10;
           entry->forwarder.val.width_learn_src_port = 10;
           entry->global.val.forworder_filter_support = MEA_TRUE;
           /*******************************************************/
           entry->shaper.val.shaper_per_port_exist = MEA_FALSE;
           entry->shaper.val.shaper_per_cluster_exist = MEA_FALSE;
           entry->shaper.val.shaper_per_Priqueue_exist = MEA_FALSE;
           entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster



                                                           /*******************************************************/
           entry->policer.val.exist = MEA_TRUE;

           entry->policer.val.reduce = MEA_TRUE;
           entry->policer.val.num_of_TMID = 512;  // 1024

           entry->policer.val.hidden_profile = 1;
           entry->policer.val.num_of_acm = 1;   // 1 when no ACM
           entry->policer.val.num_of_profile = 128; //64;
           entry->policer.val.num_of_wred_prof = 1;
           entry->policer.val.num_of_wred_acm = 1;
           entry->policer.val.Ingress_flow_exist = MEA_TRUE;
           entry->policer.val.Ingress_flow_num_of_prof = 1;  //2^11 = 2048;

                                                             /*******************************************************/
           entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
           entry->wfq.val.queue_exist = MEA_TRUE;
           entry->wfq.val.num_of_cluster = 64;  // num of cluster
           entry->wfq.val.maxCluserperport = 8;    //  32 if the HW support
           entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding


                                                                 /*******************************************************/
           entry->action.val.exist = MEA_TRUE;
           entry->action.val.table_size = (num_of_forwarder_Action * 2 + entry->acl.val.table_size);
           /*******************************************************/
           entry->xpermission.val.exist = MEA_TRUE;
           entry->xpermission.val.xper_table_size = 512; //1024;

           if (entry->global.val.mstp_support == MEA_TRUE)
               entry->xpermission.val.num_ofMSTP = 9;   /*512*/
           else {
               if (entry->global.val.rstp_support == MEA_TRUE)
                   entry->xpermission.val.num_ofMSTP = 1;   /*1 */
           }
           entry->xpermission.val.num_lag_prof = 0;


           /*******************************************************/
           entry->sar_machine.val.exist = MEA_FALSE;
           entry->sar_machine.val.table_size = 0;
           /*******************************************************/
           num_of_edit = 512;
           entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
           entry->editor_BMF.val.atm_support = MEA_FALSE;
           entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
           entry->editor_BMF.val.router_support = MEA_FALSE;
           entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
           entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
           entry->editor_BMF.val.num_of_lm_count = 0;

           entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
           entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

           /*******************************************************/
           NumOfPmIds = 512;
           entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
           entry->pm.val.num0fBlocksForBytes = 1;
           entry->pm.val.num0fBlocksForPmPackets = 1;
           /*******************************************************/
           entry->packetGen.val.PacketGen_exist = MEA_FALSE;
           entry->packetGen.val.Analyzer_exist = MEA_FALSE;
           entry->packetGen.val.PacketGen_On_Port_num = 120;
           entry->packetGen.val.Analyzer_On_Port_num = 120;


           entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
           entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
           entry->packetGen.val.PacketGen_num_of_support_profile = 8;
           entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
           entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
           entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets

           entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
           entry->fragment.val.fragment_Edit_num_of_session = 0;
           entry->fragment.val.fragment_Edit_num_of_port = 0;
           entry->fragment.val.fragment_Edit_num_of_group = 0;

           entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
           entry->fragment.val.fragment_vsp_num_of_session = 0;
           entry->fragment.val.fragment_vsp_num_of_port = 0;
           entry->fragment.val.fragment_vsp_num_of_group = 0;
           /*******************************************************************************/
           entry->Interface_Tdm_A.val.exist = MEA_FALSE;
           entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
           entry->Interface_Tdm_A.val.start_ts = 0;
           entry->Interface_Tdm_A.val.num0fspe = 12;

           entry->Interface_Tdm_B.val.exist = MEA_FALSE;
           entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
           entry->Interface_Tdm_B.val.start_ts = 96;
           entry->Interface_Tdm_B.val.num0fspe = 8;

           entry->Interface_Tdm_C.val.exist = MEA_FALSE;
           entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
           entry->Interface_Tdm_C.val.start_ts = 0;
           entry->Interface_Tdm_C.val.num0fspe = 0; //6;

           entry->global_Tdm.val.num_of_ces = 384;
           entry->global_Tdm.val.ts_size = 512;
           // need to * 32
           /************************************************************************/


           entry->Hc_Classifier.val.Hc_classifier_exist = MEA_TRUE;
           entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

           entry->Hc_Classifier.val.Num_of_hcid = 32;
           entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
           entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
           entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

           entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

           entry->slice0.index_40_regs[0] = 0;
           entry->slice0.index_40_regs[1] = 0;
           entry->slice0.index_40_regs[2] = 0;
           entry->slice0.index_40_regs[3] = 0;

           entry->slice1.index_41_regs[0] = 0;
           entry->slice1.index_41_regs[1] = 0;
           entry->slice1.index_41_regs[2] = 0;
           entry->slice1.index_41_regs[3] = 0;

           entry->slice2.index_42_regs[0] = 0;
           entry->slice2.index_42_regs[1] = 0;
           entry->slice2.index_42_regs[2] = 0;
           entry->slice2.index_42_regs[3] = 0;







#endif
           break;

        case MEA_OS_DEVICE_SIMULATION_Z7015_GW:
#if 1
            entry->global.val.chip_name = 8;
            entry->global.val.sub_family = 2;
            entry->global.val.package = 1;
            entry->global.val.speed_grade = 2;
            
            entry->global.val.encription_enable = MEA_TRUE;
            entry->global.val.protection_enable = MEA_FALSE;
            entry->global.val.bmf_martini_support = MEA_FALSE;
       

            entry->global.val.forwarder_external = MEA_TRUE;
            entry->global.val.desc_external = MEA_TRUE;
            entry->global.val.desc_checker_exist = MEA_FALSE;
            entry->global.val.host_reduce = MEA_TRUE;
            entry->global.val.pause_support = MEA_TRUE;
            entry->global.val.pre_sched_enable = MEA_FALSE;

            entry->global.val.flowCos_Mapping_exist = MEA_TRUE;
            entry->global.val.flowMarking_Mapping_exist = MEA_TRUE;

            entry->global.val.fragment_exist = MEA_FALSE;
            entry->global.val.tx_chunk_size = 8;
            
            entry->global.val.ACM_exist = MEA_FALSE;
            entry->global.val.dual_latency_support = MEA_TRUE;
            entry->global.val.fragment_bonding = MEA_FALSE;
            entry->global.val.cpe_exist = MEA_FALSE;
            entry->global.val.dual_latency_support = MEA_FALSE;
            


            entry->global.val.tdm_enable = MEA_FALSE;
            entry->global.val.large_BM_ind_address = MEA_TRUE;
            
            entry->global.val.protocolMapping = MEA_FALSE;
            entry->global.val.cls_large = MEA_TRUE;
            entry->global.val.Support_1588 = MEA_FALSE;
            entry->global.val.BM_bus_width = 1;
            entry->global.val.rx_pause_support = MEA_FALSE;
            entry->global.val.lag_support = MEA_TRUE;
            entry->global.val.mstp_support = MEA_TRUE;
            entry->global.val.rstp_support = MEA_TRUE;

            entry->global.val.mc_mqs_Support = MEA_TRUE;
            entry->global.val.cluster_adminon_support = MEA_TRUE;


            entry->global.val.ingressMacfifo_small = MEA_TRUE;
            entry->global.val.AlloC_AllocR = MEA_FALSE;

            entry->global.val.MAC_LB_support = MEA_TRUE;
            
            entry->global.val.reduce_lq = MEA_FALSE;
            entry->global.val.reduce_parser_tdm = MEA_FALSE;
            entry->global.val.fragment_9991 = MEA_FALSE;
            entry->global.val.forworder_filter_support = MEA_FALSE;
            entry->global.val.serdesReset_support = MEA_FALSE;
            entry->global.val.autoneg_support = MEA_FALSE;

            entry->global.val.standard_bonding_support = MEA_TRUE;
            
            entry->global.val.rmonRx_drop_support = MEA_FALSE;
            entry->global.val.afdx_support = MEA_FALSE;
            entry->global.val.HeaderCompress_support = MEA_TRUE;
            entry->global.val.HeaderDeCompress_support = MEA_TRUE;
            entry->global.val.ac_MTU_support = MEA_TRUE;
            entry->global.val.PTP1588_support = MEA_TRUE;

            //-----------------------------------------------------
            entry->global.val.num_of_slice = 2;
            entry->global.val.num_bonding_groupTx = 16;
            entry->global.val.num_bonding_groupRx = 1;
            //-----------------------------------------------------
            entry->global.val.wbrg_support = MEA_TRUE;
            entry->global.val.ECN_support = MEA_TRUE;
            entry->global.val.Swap_machine_enable = MEA_TRUE;


            /************************************************************/
            
            entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
            entry->global.val.GATEWAY_EN = MEA_FALSE;//
            entry->global.val.vpls_access_enable = MEA_TRUE;


            entry->global1.val.ACTION_ON_DDR = MEA_FALSE;
            entry->global1.val.EDITING_ON_DDR = MEA_FALSE;
            entry->global1.val.PM_ON_DDR = MEA_FALSE;
            entry->global1.val.SERVICE_EXTERNAL = MEA_FALSE;
            entry->global1.val.FWD_IS_256 = MEA_FALSE;
            entry->global1.val.hierarchical_en = MEA_TRUE;

            entry->global1.val.instanc_fifo_1588 = 2;
            entry->global1.val.max_numof_descriptor = 3;  /*for 4K     2^(3-1)x1024   */



            entry->hpm.val.hpm_valid = MEA_TRUE;
            entry->hpm.val.numof_UE_PDN = 8;
            entry->hpm.val.hpm_profile = 64;
            entry->hpm.val.hpm_numof_rules = 32;
            entry->hpm.val.hpm_Type = 1;
            entry->hpm.val.hpm_numOfAct = 11;

            entry->hpm.val.hpm_BASE = 4352;
            entry->hpm.val.hpm_Mask_profile = 9;


            /*******************************************************/
            /*only for ATM mode*/
            entry->pre_parser.val.exist = MEA_FALSE;
            entry->pre_parser.val.table_size = 0; //TBD
            entry->pre_parser.val.vsp_exist = MEA_FALSE;

            /*******************************************************/
            entry->parser.val.exist = MEA_TRUE;
            entry->parser.val.parser_etype_cam = MEA_FALSE;
            entry->parser.val.table_size = 128;
            entry->parser.val.numofBit = 216;
            /*******************************************************/

            entry->service.val.exist = MEA_TRUE;
            entry->service.val.table_size = 2048;///512;
            entry->service.val.serviceRangeExist = MEA_TRUE;
            entry->service.val.num_service_hash = 0;
            entry->service.val.ServiceRange_Num_of_range = 8;
            entry->service.val.numofRangeProfile = 32;
            entry->service.val.EVC_Classifier_exist = MEA_TRUE;
            entry->service.val.EVC_classifier_key_width = 24;
            entry->service.val.EVC_Classifier_num_hash_group = 0;

            entry->service.val.srv_externl_size = 4;

            /*******************************************************/
            entry->acl.val.exist = MEA_TRUE;
            entry->acl.val.table_size = 256;
            entry->acl.val.lxcp_act_width = 8; // 2^8 is 256 Action
            entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile

            /*******************************************************/
            entry->forwarder_lmt.val.exist = MEA_TRUE;
            entry->forwarder_lmt.val.table_size = 512;

            /*******************************************************/
            entry->forwarder.val.exist = MEA_TRUE;
            entry->forwarder.val.addres_size = 23;
            entry->forwarder.val.adm_support = MEA_TRUE;
            num_of_forwarder_Action = 2048;//16384;//8192;
            entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
            entry->forwarder.val.width_learn_action_id = 12;//15;
            entry->forwarder.val.width_learn_src_port = 9;
            entry->global.val.forworder_filter_support = MEA_TRUE;
            /*******************************************************/
            entry->shaper.val.shaper_per_port_exist = MEA_TRUE;
            entry->shaper.val.shaper_per_cluster_exist = MEA_TRUE;
            entry->shaper.val.shaper_per_Priqueue_exist = MEA_TRUE;
            entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster



           /*******************************************************/
            entry->policer.val.exist = MEA_TRUE;

            entry->policer.val.reduce = MEA_TRUE;
            entry->policer.val.num_of_TMID = 512;  // 1024

            entry->policer.val.hidden_profile = 1;
            entry->policer.val.num_of_acm = 1;   // 1 when no ACM
            entry->policer.val.num_of_profile = 128; //64;
            entry->policer.val.num_of_wred_prof = 1;
            entry->policer.val.num_of_wred_acm = 1;
            entry->policer.val.Ingress_flow_exist = MEA_TRUE;
            entry->policer.val.Ingress_flow_num_of_prof = 11;  //2^11 = 2048;

            /*******************************************************/
            entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
            entry->wfq.val.queue_exist = MEA_TRUE;
            entry->wfq.val.num_of_cluster = 256;  // num of cluster
            entry->wfq.val.maxCluserperport = 32;    //  32 if the HW support
            entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding
            entry->wfq.val.queue_count_exist = MEA_TRUE;

            /*******************************************************/
            entry->action.val.exist = MEA_TRUE;
            if(entry->hpm.val.hpm_valid)
                numof_hpmAction=(1 << entry->hpm.val.hpm_numOfAct);
            
             entry->action.val.table_size = (MEA_OS_MAX(entry->service.val.table_size,(num_of_forwarder_Action * 2)) + entry->acl.val.table_size + numof_hpmAction);
            /*max from service and fwd */
            /*******************************************************/
            entry->xpermission.val.exist = MEA_TRUE;
            entry->xpermission.val.xper_table_size = 512; //1024;

            if (entry->global.val.mstp_support == MEA_TRUE)
                entry->xpermission.val.num_ofMSTP = 9;   /*512*/
            else {
                if (entry->global.val.rstp_support == MEA_TRUE)
                    entry->xpermission.val.num_ofMSTP = 1;   /*1 */
            }
            entry->xpermission.val.num_lag_prof = 8;


            /*******************************************************/
            entry->sar_machine.val.exist = MEA_FALSE;
            entry->sar_machine.val.table_size = 0;
            /*******************************************************/
            
            num_of_edit = 16384;
            entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
            entry->editor_BMF.val.atm_support = MEA_FALSE;
            entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
            entry->editor_BMF.val.router_support = MEA_FALSE;
            entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
            entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
            entry->editor_BMF.val.num_of_lm_count = 16;

            entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
            entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

            /*******************************************************/
            
            NumOfPmIds = 16384;
            entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
            entry->pm.val.num0fBlocksForBytes = 1;
            entry->pm.val.num0fBlocksForPmPackets = 1;


            /*******************************************************/
            entry->packetGen.val.PacketGen_exist = MEA_TRUE;
            entry->packetGen.val.Analyzer_exist = MEA_TRUE;
            entry->packetGen.val.PacketGen_On_Port_num = 120;
            entry->packetGen.val.Analyzer_On_Port_num = 120;


            entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
            entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
            entry->packetGen.val.PacketGen_num_of_support_profile = 8;
            entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
            entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
            entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets


                                                                      /***************************************************************/



                                                                      /**************************************************************/
            entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
            entry->fragment.val.fragment_Edit_num_of_session = 0;
            entry->fragment.val.fragment_Edit_num_of_port = 0;
            entry->fragment.val.fragment_Edit_num_of_group = 0;

            entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
            entry->fragment.val.fragment_vsp_num_of_session = 0;
            entry->fragment.val.fragment_vsp_num_of_port = 0;
            entry->fragment.val.fragment_vsp_num_of_group = 0;
            /*******************************************************************************/
            entry->Interface_Tdm_A.val.exist = MEA_FALSE;
            entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
            entry->Interface_Tdm_A.val.start_ts = 0;
            entry->Interface_Tdm_A.val.num0fspe = 12;

            entry->Interface_Tdm_B.val.exist = MEA_FALSE;
            entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
            entry->Interface_Tdm_B.val.start_ts = 96;
            entry->Interface_Tdm_B.val.num0fspe = 8;

            entry->Interface_Tdm_C.val.exist = MEA_FALSE;
            entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
            entry->Interface_Tdm_C.val.start_ts = 0;
            entry->Interface_Tdm_C.val.num0fspe = 0; //6;

            entry->global_Tdm.val.num_of_ces = 384;
            entry->global_Tdm.val.ts_size = 512;  // need to * 32
           /************************************************************************/


            entry->Hc_Classifier.val.Hc_classifier_exist = MEA_TRUE;
            entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

            entry->Hc_Classifier.val.Num_of_hcid = 32;
            entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
            entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
            entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

            entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

            entry->slice0.index_40_regs[0] = 0x00ffffff;
            entry->slice0.index_40_regs[1] = 0;
            entry->slice0.index_40_regs[2] = 0;
            entry->slice0.index_40_regs[3] = 0x0000f000;

            entry->slice1.index_41_regs[0] = 0;
            entry->slice1.index_41_regs[1] = 0;
            entry->slice1.index_41_regs[2] = 0;
            entry->slice1.index_41_regs[3] = 0x810003f0;







#endif
            break;

         case MEA_OS_DEVICE_SIMULATION_NIC_40G:
#if 1
             entry->global.val.chip_name = 7;
             entry->global.val.sub_family = 2;
             entry->global.val.package = 1;
             entry->global.val.speed_grade = 2;

             entry->global.val.encription_enable = MEA_TRUE;
             entry->global.val.protection_enable = MEA_FALSE;
             entry->global.val.bmf_martini_support = MEA_FALSE;
           

             entry->global.val.forwarder_external = MEA_TRUE;
             entry->global.val.desc_external = MEA_TRUE;
             entry->global.val.desc_checker_exist = MEA_FALSE;
             entry->global.val.host_reduce = MEA_TRUE;
             entry->global.val.pause_support = MEA_TRUE;
             entry->global.val.pre_sched_enable = MEA_FALSE;

             entry->global.val.flowCos_Mapping_exist = MEA_TRUE;
             entry->global.val.flowMarking_Mapping_exist = MEA_TRUE;

             entry->global.val.fragment_exist = MEA_FALSE;
             entry->global.val.tx_chunk_size = 8;
            
             entry->global.val.ACM_exist = MEA_FALSE;
             entry->global.val.dual_latency_support = MEA_TRUE;
             entry->global.val.fragment_bonding = MEA_FALSE;
             entry->global.val.cpe_exist = MEA_FALSE;
             entry->global.val.dual_latency_support = MEA_FALSE;
             


             entry->global.val.tdm_enable = MEA_FALSE;
             entry->global.val.large_BM_ind_address = MEA_TRUE;
             
             entry->global.val.protocolMapping = MEA_FALSE;
             entry->global.val.cls_large = MEA_TRUE;
             entry->global.val.Support_1588 = MEA_FALSE;
             entry->global.val.BM_bus_width = 1;
             entry->global.val.rx_pause_support = MEA_FALSE;
             entry->global.val.lag_support = MEA_TRUE;
             entry->global.val.mstp_support = MEA_TRUE;
             entry->global.val.rstp_support = MEA_TRUE;

             entry->global.val.mc_mqs_Support = MEA_TRUE;
             entry->global.val.cluster_adminon_support = MEA_TRUE;


             entry->global.val.ingressMacfifo_small = MEA_TRUE;
             entry->global.val.AlloC_AllocR = MEA_FALSE;

             entry->global.val.MAC_LB_support = MEA_TRUE;
             
             entry->global.val.reduce_lq = MEA_FALSE;
             entry->global.val.reduce_parser_tdm = MEA_FALSE;
             entry->global.val.fragment_9991 = MEA_FALSE;
             entry->global.val.forworder_filter_support = MEA_FALSE;
             entry->global.val.serdesReset_support = MEA_FALSE;
             entry->global.val.autoneg_support = MEA_FALSE;

             entry->global.val.standard_bonding_support = MEA_TRUE;
             
             entry->global.val.rmonRx_drop_support = MEA_FALSE;
             entry->global.val.afdx_support = MEA_FALSE;
             entry->global.val.HeaderCompress_support = MEA_TRUE;
             entry->global.val.HeaderDeCompress_support = MEA_TRUE;
             entry->global.val.ac_MTU_support = MEA_TRUE;
             entry->global.val.PTP1588_support = MEA_TRUE;

             //-----------------------------------------------------
             entry->global.val.num_of_slice = 2;
             entry->global.val.num_bonding_groupTx = 16;
             entry->global.val.num_bonding_groupRx = 1;
             //-----------------------------------------------------
             entry->global.val.wbrg_support = MEA_TRUE;
             entry->global.val.ECN_support = MEA_TRUE;
             entry->global.val.Swap_machine_enable = MEA_TRUE;
             entry->global.val.NewAlloc = MEA_TRUE;
             entry->global.val.BFD_support = MEA_TRUE;
             entry->global.val.ip_sec_support = MEA_TRUE;
             entry->global.val.NVGRE_enable = MEA_TRUE;
             entry->global1.val.max_numof_descriptor = 8;
             
             entry->global1.val.max_numof_DataBuffer = 3;

             /************************************************************/
             
             entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
             entry->global.val.GATEWAY_EN = MEA_FALSE;
             entry->global.val.vpls_access_enable = MEA_TRUE;

             entry->global1.val.sys_M = 59;
             entry->global1.val.sys_N = 7;


             entry->global1.val.ACTION_ON_DDR  = MEA_TRUE;
             entry->global1.val.EDITING_ON_DDR = MEA_TRUE;
             entry->global1.val.PM_ON_DDR      = MEA_TRUE;
             entry->global1.val.SERVICE_EXTERNAL = MEA_FALSE;
             entry->global1.val.FWD_IS_256 = MEA_FALSE;
             entry->global1.val.type_DB_DDR = MEA_TRUE;
             entry->global1.val.hierarchical_en = MEA_TRUE;
             entry->global1.val.vxlan_support = MEA_TRUE;

             entry->global1.val.instanc_fifo_1588 = 2;
             entry->global1.val.max_numof_descriptor = 3;  /*for 4K     2^(3-1)x1024   */

             entry->hpm.val.hpm_valid = MEA_TRUE;
             entry->hpm.val.numof_UE_PDN = 8;
             entry->hpm.val.hpm_profile = 64;
             entry->hpm.val.hpm_numof_rules = 8;
             entry->hpm.val.hpm_Type = 0;
             entry->hpm.val.hpm_numOfAct = 11;

             entry->hpm.val.hpm_BASE = 262400;
             entry->hpm.val.hpm_Mask_profile = 9;


            /*******************************************************/
            /*only for ATM mode*/
             entry->pre_parser.val.exist = MEA_FALSE;
             entry->pre_parser.val.table_size = 0; //TBD
             entry->pre_parser.val.vsp_exist = MEA_FALSE;

             /*******************************************************/
             entry->parser.val.exist = MEA_TRUE;
             entry->parser.val.parser_etype_cam = MEA_FALSE;
             entry->parser.val.table_size = 128;
             entry->parser.val.numofBit = 216;
             /*******************************************************/

             entry->service.val.exist = MEA_TRUE;
             entry->service.val.table_size = 1024;
             entry->service.val.serviceRangeExist = MEA_TRUE;
             entry->service.val.num_service_hash = 0;
             entry->service.val.ServiceRange_Num_of_range = 8;
             entry->service.val.numofRangeProfile = 32;
             entry->service.val.EVC_Classifier_exist = MEA_TRUE;
             entry->service.val.EVC_classifier_key_width = 24;
             entry->service.val.EVC_Classifier_num_hash_group = 0;

             entry->service.val.srv_externl_size = 4;

             /*******************************************************/
             entry->acl.val.exist = MEA_TRUE;
             entry->acl.val.table_size = 256;
             entry->acl.val.lxcp_act_width = 8; // 2^8 is 256 Action
             entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile
             entry->acl.val.ACL5_exist      = MEA_TRUE;

             entry->acl.val.ACL5_Ipmask_size = 9;
             entry->acl.val.ACL5_rangeProf_size = 9;
             entry->acl.val.ACL5_keymask_size =  9;
             entry->acl.val.ACL5_rangeBlock  = 5 ;
             entry->acl.val.ACL5_External = MEA_TRUE;





             /*******************************************************/
             entry->forwarder_lmt.val.exist = MEA_TRUE;
             entry->forwarder_lmt.val.table_size = 256;

             /*******************************************************/
             entry->forwarder.val.exist = MEA_TRUE;
             entry->forwarder.val.addres_size = 23;
             entry->forwarder.val.adm_support = MEA_TRUE;
             num_of_forwarder_Action = 131072;// 1024;//8192;
             entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
             entry->forwarder.val.width_learn_action_id = 18;
             entry->forwarder.val.width_learn_src_port = 9;
             entry->global.val.forworder_filter_support = MEA_TRUE;
             entry->forwarder.val.lpm_enable=1;
             entry->forwarder.val.lpm_ipv4=120;
             entry->forwarder.val.lpm_ipv6 = 6;
             /*******************************************************/
             entry->shaper.val.shaper_per_port_exist = MEA_TRUE;
             entry->shaper.val.shaper_per_cluster_exist = MEA_TRUE;
             entry->shaper.val.shaper_per_Priqueue_exist = MEA_TRUE;
             entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster


            /*******************************************************/
             entry->policer.val.exist = MEA_TRUE;

             entry->policer.val.reduce = MEA_TRUE;
			 entry->policer.val.num_of_TMID = 4 * 1024;  //512;  // 1024

             entry->policer.val.hidden_profile = 1;
             entry->policer.val.num_of_acm = 1;   // 1 when no ACM
             entry->policer.val.num_of_profile = 128; //64;
             entry->policer.val.num_of_wred_prof = 1;
             entry->policer.val.num_of_wred_acm = 1;
             entry->policer.val.Ingress_flow_exist = MEA_TRUE;
             entry->policer.val.Ingress_flow_num_of_prof = 9;  //2^11 = 2048;
			 entry->policer.val.policer_slowslow = MEA_TRUE;
			 entry->policer.val.CBS_bucket_bits = 20;
			 entry->policer.val.CIR_Token_bits  = 20;

             /*******************************************************/
             entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
             entry->wfq.val.queue_exist = MEA_TRUE;
             entry->wfq.val.num_of_cluster = 256;  // num of cluster
             entry->wfq.val.maxCluserperport = 64;    //  32 if the HW support
             entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding
             entry->wfq.val.queue_count_exist = MEA_TRUE;

             /*******************************************************/
             entry->action.val.exist = MEA_TRUE;
             if (entry->hpm.val.hpm_valid)
                 numof_hpmAction = (1 << entry->hpm.val.hpm_numOfAct);

             entry->action.val.table_size = (MEA_OS_MAX(entry->service.val.table_size, (num_of_forwarder_Action * 2)) + entry->acl.val.table_size + numof_hpmAction);
             /*******************************************************/
             entry->xpermission.val.exist = MEA_TRUE;
             entry->xpermission.val.xper_table_size = 512; //1024;

             if (entry->global.val.mstp_support == MEA_TRUE)
                 entry->xpermission.val.num_ofMSTP = 9;   /*512*/
             else {
                 if (entry->global.val.rstp_support == MEA_TRUE)
                     entry->xpermission.val.num_ofMSTP = 1;   /*1 */
             }
             entry->xpermission.val.num_lag_prof = 8;
             entry->xpermission.val.sflow_support = MEA_TRUE;
             entry->xpermission.val.sflow_count_lg = 11;


             /*******************************************************/
             entry->sar_machine.val.exist = MEA_FALSE;
             entry->sar_machine.val.table_size = 0;
             /*******************************************************/

             num_of_edit = 262144;
             entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
             entry->editor_BMF.val.atm_support = MEA_FALSE;
             entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
             entry->editor_BMF.val.router_support = MEA_FALSE;
             entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
             entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
             entry->editor_BMF.val.num_of_lm_count = 16;

             entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
             entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

             /*******************************************************/

             NumOfPmIds = 262144;
             entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
             entry->pm.val.num0fBlocksForBytes = 1;
             entry->pm.val.num0fBlocksForPmPackets = 1;


             /*******************************************************/
             entry->packetGen.val.PacketGen_exist = MEA_TRUE;
             entry->packetGen.val.Analyzer_exist = MEA_TRUE;
             entry->packetGen.val.PacketGen_On_Port_num = 120;
             entry->packetGen.val.Analyzer_On_Port_num = 120;


             entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
             entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
             entry->packetGen.val.PacketGen_num_of_support_profile = 8;
             entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
             entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
             entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets


        /***************************************************************/

             entry->nvgr_tunnel.val.crypto_db_prof_width = 9; 

        /**************************************************************/
             entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
             entry->fragment.val.fragment_Edit_num_of_session = 0;
             entry->fragment.val.fragment_Edit_num_of_port = 0;
             entry->fragment.val.fragment_Edit_num_of_group = 0;

             entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
             entry->fragment.val.fragment_vsp_num_of_session = 0;
             entry->fragment.val.fragment_vsp_num_of_port = 0;
             entry->fragment.val.fragment_vsp_num_of_group = 0;
             /*******************************************************************************/
             entry->Interface_Tdm_A.val.exist = MEA_FALSE;
             entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
             entry->Interface_Tdm_A.val.start_ts = 0;
             entry->Interface_Tdm_A.val.num0fspe = 12;

             entry->Interface_Tdm_B.val.exist = MEA_FALSE;
             entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
             entry->Interface_Tdm_B.val.start_ts = 96;
             entry->Interface_Tdm_B.val.num0fspe = 8;

             entry->Interface_Tdm_C.val.exist = MEA_FALSE;
             entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
             entry->Interface_Tdm_C.val.start_ts = 0;
             entry->Interface_Tdm_C.val.num0fspe = 0; //6;

             entry->global_Tdm.val.num_of_ces = 384;
             entry->global_Tdm.val.ts_size = 512;  // need to * 32
            /************************************************************************/


             entry->Hc_Classifier.val.Hc_classifier_exist = MEA_TRUE;
             entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

             entry->Hc_Classifier.val.Num_of_hcid = 32;
             entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
             entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
             entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

             entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

             entry->slice0.index_40_regs[0] = 0x00ffffff;
             entry->slice0.index_40_regs[1] = 0;
             entry->slice0.index_40_regs[2] = 0;
             entry->slice0.index_40_regs[3] = 0x0000f000;

             entry->slice1.index_41_regs[0] = 0;
             entry->slice1.index_41_regs[1] = 0;
             entry->slice1.index_41_regs[2] = 0;
             entry->slice1.index_41_regs[3] = 0x810003f0;







#endif
          break;
        case MEA_OS_DEVICE_SIMULATION_Z7045_GW:
#if 1
            
            
            entry->global.val.chip_name = 8;
            entry->global.val.sub_family = 1;
            entry->global.val.package = 1;
            entry->global.val.speed_grade = 2;



            entry->global.val.encription_enable = MEA_TRUE;
            entry->global.val.protection_enable = MEA_FALSE;
            entry->global.val.bmf_martini_support = MEA_FALSE;
           

            entry->global.val.forwarder_external = MEA_TRUE;
            entry->global.val.desc_external = MEA_TRUE;
            entry->global.val.desc_checker_exist = MEA_FALSE;
            entry->global.val.host_reduce = MEA_TRUE;
            entry->global.val.pause_support = MEA_TRUE;
            entry->global.val.pre_sched_enable = MEA_FALSE;

            entry->global.val.flowCos_Mapping_exist = MEA_TRUE;
            entry->global.val.flowMarking_Mapping_exist = MEA_TRUE;

            entry->global.val.fragment_exist = MEA_FALSE;
            entry->global.val.tx_chunk_size = 8;
            
            entry->global.val.ACM_exist = MEA_FALSE;
            entry->global.val.dual_latency_support = MEA_TRUE;
            entry->global.val.fragment_bonding = MEA_FALSE;
            entry->global.val.cpe_exist = MEA_FALSE;
            entry->global.val.dual_latency_support = MEA_FALSE;
            


            entry->global.val.tdm_enable = MEA_FALSE;
            entry->global.val.large_BM_ind_address = MEA_TRUE;
            
            entry->global.val.protocolMapping = MEA_FALSE;
            entry->global.val.cls_large = MEA_TRUE;
            entry->global.val.Support_1588 = MEA_FALSE;
            entry->global.val.BM_bus_width = 1;
            entry->global.val.rx_pause_support = MEA_FALSE;
            entry->global.val.lag_support = MEA_TRUE;
            entry->global.val.mstp_support = MEA_TRUE;
            entry->global.val.rstp_support = MEA_TRUE;

            entry->global.val.mc_mqs_Support = MEA_TRUE;
            entry->global.val.cluster_adminon_support = MEA_TRUE;


            entry->global.val.ingressMacfifo_small = MEA_TRUE;
            entry->global.val.AlloC_AllocR = MEA_FALSE;

            entry->global.val.MAC_LB_support = MEA_TRUE;
            
            entry->global.val.reduce_lq = MEA_FALSE;
            entry->global.val.reduce_parser_tdm = MEA_FALSE;
            entry->global.val.fragment_9991 = MEA_FALSE;
            entry->global.val.forworder_filter_support = MEA_FALSE;
            entry->global.val.serdesReset_support = MEA_FALSE;
            entry->global.val.autoneg_support = MEA_FALSE;

            entry->global.val.standard_bonding_support = MEA_TRUE;
            
            entry->global.val.rmonRx_drop_support = MEA_FALSE;
            entry->global.val.afdx_support = MEA_FALSE;
            entry->global.val.HeaderCompress_support = MEA_TRUE;
            entry->global.val.HeaderDeCompress_support = MEA_TRUE;
            entry->global.val.ac_MTU_support = MEA_TRUE;
            entry->global.val.PTP1588_support = MEA_TRUE;

            //-----------------------------------------------------
            entry->global.val.num_of_slice = 2;
            entry->global.val.num_bonding_groupTx = 16;
            entry->global.val.num_bonding_groupRx = 1;
            //-----------------------------------------------------
            entry->global.val.wbrg_support = MEA_TRUE;
            entry->global.val.ECN_support = MEA_TRUE;
            entry->global.val.Swap_machine_enable = MEA_TRUE;


            /************************************************************/
            
            entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
            entry->global.val.GATEWAY_EN = MEA_TRUE;
            entry->global.val.vpls_access_enable = MEA_TRUE;
            entry->global.val.vpls_chactristic_enable = MEA_TRUE;

            entry->global1.val.sys_M         = 50; //40;
            entry->global1.val.sys_N         = 10;
            entry->global1.val.pushClk       = 40;

            entry->global1.val.ACTION_ON_DDR = MEA_TRUE;
            entry->global1.val.EDITING_ON_DDR = MEA_TRUE;
            entry->global1.val.PM_ON_DDR = MEA_TRUE;
            entry->global1.val.SERVICE_EXTERNAL = MEA_TRUE;
            entry->global1.val.FWD_IS_256 = MEA_FALSE;
            entry->global1.val.hierarchical_en = MEA_TRUE;
            entry->global1.val.vxlan_support       = MEA_TRUE; // 

            entry->global1.val.instanc_fifo_1588 = 2;
            entry->global1.val.max_numof_descriptor = 4;  /*for 4K     2^(3-1)x1024   */


            entry->hpm.val.hpm_valid = MEA_TRUE;
            entry->hpm.val.numof_UE_PDN = 14;
            entry->hpm.val.hpm_profile = 64;
            entry->hpm.val.hpm_numof_rules = 32;
            entry->hpm.val.hpm_Type = 1;
            entry->hpm.val.hpm_numOfAct = 18;

            entry->hpm.val.hpm_BASE = 262400;
            entry->hpm.val.hpm_Mask_profile = 9;
            entry->hpm.val.SER_TeId = 14;



            /*******************************************************/
            /*only for ATM mode*/
            entry->pre_parser.val.exist = MEA_FALSE;
            entry->pre_parser.val.table_size = 0; //TBD
            entry->pre_parser.val.vsp_exist = MEA_FALSE;

            /*******************************************************/
            entry->parser.val.exist            = MEA_TRUE;
            entry->parser.val.parser_etype_cam = MEA_FALSE;
            entry->parser.val.table_size       = 128;
            entry->parser.val.numofBit         = 216;
            entry->parser.val.support_IPV6     = MEA_TRUE;
            /*******************************************************/

            entry->service.val.exist = MEA_TRUE;
            entry->service.val.table_size = 512;
            entry->service.val.serviceRangeExist = MEA_TRUE;
            entry->service.val.num_service_hash = 0;
            entry->service.val.ServiceRange_Num_of_range = 8;
            entry->service.val.numofRangeProfile = 32;
            entry->service.val.EVC_Classifier_exist = MEA_TRUE;
            entry->service.val.EVC_classifier_key_width = 24;
            entry->service.val.EVC_Classifier_num_hash_group = 0;

            entry->service.val.srv_externl_size = 4;

            /*******************************************************/
            entry->acl.val.exist = MEA_TRUE;
            entry->acl.val.table_size = 256;
            entry->acl.val.lxcp_act_width = 8; // 2^8 is 256 Action
            entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile

            /*******************************************************/
            entry->forwarder_lmt.val.exist = MEA_TRUE;
            entry->forwarder_lmt.val.table_size = 512;

            /*******************************************************/
            entry->forwarder.val.exist = MEA_TRUE;
            entry->forwarder.val.addres_size = 23;
            entry->forwarder.val.adm_support = MEA_TRUE;
            entry->forwarder.val.segment_switch_support = MEA_TRUE; //
            num_of_forwarder_Action = 128 * 1024; // 16384;//8192;
            entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
            entry->forwarder.val.width_learn_action_id = 18;
            entry->forwarder.val.width_learn_src_port = 9;
            entry->global.val.forworder_filter_support = MEA_TRUE;
            /*******************************************************/
            entry->shaper.val.shaper_per_port_exist = MEA_TRUE;
            entry->shaper.val.shaper_per_cluster_exist = MEA_TRUE;
            entry->shaper.val.shaper_per_Priqueue_exist = MEA_TRUE;
            entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster



            /*******************************************************/
            entry->policer.val.exist = MEA_TRUE;

            entry->policer.val.reduce = MEA_TRUE;
            entry->policer.val.num_of_TMID = 8192;  //512;  // 1024

            entry->policer.val.hidden_profile = 1;
            entry->policer.val.num_of_acm = 1;   // 1 when no ACM
            entry->policer.val.num_of_profile = 128; //64;
            entry->policer.val.num_of_wred_prof = 1;
            entry->policer.val.num_of_wred_acm = 1;
            entry->policer.val.Ingress_flow_exist = MEA_TRUE;
            entry->policer.val.Ingress_flow_num_of_prof = 11;  //2^11 = 2048;

            /*******************************************************/
            entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
            entry->wfq.val.queue_exist = MEA_TRUE;
            entry->wfq.val.num_of_cluster = 128;  // num of cluster
            entry->wfq.val.maxCluserperport = 64;    //  32 if the HW support
            entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding


           /*******************************************************/
            entry->action.val.exist = MEA_TRUE;
            //entry->action.val.table_size = 524544; //164096;
            if (entry->hpm.val.hpm_valid)
                numof_hpmAction = (1 << entry->hpm.val.hpm_numOfAct);

            entry->action.val.table_size = (MEA_OS_MAX(entry->service.val.table_size, (num_of_forwarder_Action * 2)) + entry->acl.val.table_size + numof_hpmAction);

           
           /*******************************************************/
            entry->xpermission.val.exist = MEA_TRUE;
            entry->xpermission.val.xper_table_size = 2048; //512; //1024;

            if (entry->global.val.mstp_support == MEA_TRUE)
                entry->xpermission.val.num_ofMSTP = 9;   /*512*/
            else {
                if (entry->global.val.rstp_support == MEA_TRUE)
                    entry->xpermission.val.num_ofMSTP = 1;   /*1 */
            }
            entry->xpermission.val.num_lag_prof = 8;


            /*******************************************************/
            entry->sar_machine.val.exist = MEA_FALSE;
            entry->sar_machine.val.table_size = 0;
            /*******************************************************/
           
            num_of_edit = 256 * 1024; //16384;
            entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
            entry->editor_BMF.val.atm_support = MEA_FALSE;
            entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
            entry->editor_BMF.val.router_support = MEA_FALSE;
            entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
            entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
            entry->editor_BMF.val.num_of_lm_count = 16;

            entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
            entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

            /*******************************************************/
            NumOfPmIds = 256 * 1024; //16384;
            entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
            entry->pm.val.num0fBlocksForBytes = 1;
            entry->pm.val.num0fBlocksForPmPackets = 1;


            /*******************************************************/
            entry->packetGen.val.PacketGen_exist = MEA_TRUE;
            entry->packetGen.val.Analyzer_exist = MEA_TRUE;
            entry->packetGen.val.PacketGen_On_Port_num = 120;
            entry->packetGen.val.Analyzer_On_Port_num = 120;


            entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
            entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
            entry->packetGen.val.PacketGen_num_of_support_profile = 8;
            entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
            entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
            entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets


         /***************************************************************/



           /**************************************************************/
            entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
            entry->fragment.val.fragment_Edit_num_of_session = 0;
            entry->fragment.val.fragment_Edit_num_of_port = 0;
            entry->fragment.val.fragment_Edit_num_of_group = 0;

            entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
            entry->fragment.val.fragment_vsp_num_of_session = 0;
            entry->fragment.val.fragment_vsp_num_of_port = 0;
            entry->fragment.val.fragment_vsp_num_of_group = 0;
            /*******************************************************************************/
            entry->Interface_Tdm_A.val.exist = MEA_FALSE;
            entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
            entry->Interface_Tdm_A.val.start_ts = 0;
            entry->Interface_Tdm_A.val.num0fspe = 12;

            entry->Interface_Tdm_B.val.exist = MEA_FALSE;
            entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
            entry->Interface_Tdm_B.val.start_ts = 96;
            entry->Interface_Tdm_B.val.num0fspe = 8;

            entry->Interface_Tdm_C.val.exist = MEA_FALSE;
            entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
            entry->Interface_Tdm_C.val.start_ts = 0;
            entry->Interface_Tdm_C.val.num0fspe = 0; //6;

            entry->global_Tdm.val.num_of_ces = 384;
            entry->global_Tdm.val.ts_size = 512;  // need to * 32
           /************************************************************************/


            entry->Hc_Classifier.val.Hc_classifier_exist = MEA_TRUE;
            entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

            entry->Hc_Classifier.val.Num_of_hcid = 32;
            entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
            entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
            entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

            entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

            entry->slice0.index_40_regs[0] = 0x00ffffff;
            entry->slice0.index_40_regs[1] = 0;
            entry->slice0.index_40_regs[2] = 0;
            entry->slice0.index_40_regs[3] = 0x0000f000;

            entry->slice1.index_41_regs[0] = 0;
            entry->slice1.index_41_regs[1] = 0;
            entry->slice1.index_41_regs[2] = 0;
            entry->slice1.index_41_regs[3] = 0x810003f0;







#endif
            break;
            case    MEA_OS_DEVICE_SIMULATION_Z7045_GW_1KC:
#if 1


                entry->global.val.chip_name = 8;
                entry->global.val.sub_family = 1;
                entry->global.val.package = 1;
                entry->global.val.speed_grade = 2;



                entry->global.val.encription_enable = MEA_TRUE;
                entry->global.val.protection_enable = MEA_FALSE;
                entry->global.val.bmf_martini_support = MEA_FALSE;
               

                entry->global.val.forwarder_external = MEA_TRUE;
                entry->global.val.desc_external = MEA_TRUE;
                entry->global.val.desc_checker_exist = MEA_FALSE;
                entry->global.val.host_reduce = MEA_TRUE;
                entry->global.val.pause_support = MEA_TRUE;
                entry->global.val.pre_sched_enable = MEA_FALSE;

                entry->global.val.flowCos_Mapping_exist = MEA_TRUE;
                entry->global.val.flowMarking_Mapping_exist = MEA_TRUE;

                entry->global.val.fragment_exist = MEA_FALSE;
                entry->global.val.tx_chunk_size = 8;
                
                entry->global.val.ACM_exist = MEA_FALSE;
                entry->global.val.dual_latency_support = MEA_TRUE;
                entry->global.val.fragment_bonding = MEA_FALSE;
                entry->global.val.cpe_exist = MEA_FALSE;
                entry->global.val.dual_latency_support = MEA_FALSE;
                


                entry->global.val.tdm_enable = MEA_FALSE;
                entry->global.val.large_BM_ind_address = MEA_TRUE;
                
                entry->global.val.protocolMapping = MEA_FALSE;
                entry->global.val.cls_large = MEA_TRUE;
                entry->global.val.Support_1588 = MEA_FALSE;
                entry->global.val.BM_bus_width = 1;
                entry->global.val.rx_pause_support = MEA_FALSE;
                entry->global.val.lag_support = MEA_TRUE;
                entry->global.val.mstp_support = MEA_TRUE;
                entry->global.val.rstp_support = MEA_TRUE;

                entry->global.val.mc_mqs_Support = MEA_TRUE;
                entry->global.val.cluster_adminon_support = MEA_TRUE;


                entry->global.val.ingressMacfifo_small = MEA_TRUE;
                entry->global.val.AlloC_AllocR = MEA_FALSE;

                entry->global.val.MAC_LB_support = MEA_TRUE;
                
                entry->global.val.reduce_lq = MEA_FALSE;
                entry->global.val.reduce_parser_tdm = MEA_FALSE;
                entry->global.val.fragment_9991 = MEA_FALSE;
                entry->global.val.forworder_filter_support = MEA_FALSE;
                entry->global.val.serdesReset_support = MEA_FALSE;
                entry->global.val.autoneg_support = MEA_FALSE;

                entry->global.val.standard_bonding_support = MEA_TRUE;
                
                entry->global.val.rmonRx_drop_support = MEA_FALSE;
                entry->global.val.afdx_support = MEA_FALSE;
                entry->global.val.HeaderCompress_support = MEA_TRUE;
                entry->global.val.HeaderDeCompress_support = MEA_TRUE;
                entry->global.val.ac_MTU_support = MEA_TRUE;
                entry->global.val.PTP1588_support = MEA_TRUE;

                //-----------------------------------------------------
                entry->global.val.num_of_slice = 2;
                entry->global.val.num_bonding_groupTx = 16;
                entry->global.val.num_bonding_groupRx = 1;
                //-----------------------------------------------------
                entry->global.val.wbrg_support = MEA_TRUE;
                entry->global.val.ECN_support = MEA_TRUE;
                entry->global.val.Swap_machine_enable = MEA_TRUE;
				entry->global.val.num_of_Egress_port = 0;  /*only 128 Egress*/

				entry->global.val.num_of_port_Ingress = 3;  /* we set 1024 port */



                /************************************************************/
                
                entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
                entry->global.val.GATEWAY_EN = MEA_FALSE;
                entry->global.val.vpls_access_enable = MEA_TRUE;
                entry->global.val.vpls_chactristic_enable = MEA_TRUE;

                entry->global1.val.sys_M = 50; //40;
                entry->global1.val.sys_N = 10;
                entry->global1.val.pushClk = 40;

                entry->global1.val.ACTION_ON_DDR = MEA_TRUE;
                entry->global1.val.EDITING_ON_DDR = MEA_TRUE;
                entry->global1.val.PM_ON_DDR = MEA_TRUE;
                entry->global1.val.SERVICE_EXTERNAL = MEA_TRUE;
                entry->global1.val.FWD_IS_256 = MEA_FALSE;
                entry->global1.val.hierarchical_en = MEA_TRUE;

                entry->global1.val.instanc_fifo_1588 = 2;
                entry->global1.val.max_numof_descriptor = 4;  /*for 4K     2^(3-1)x1024   */


                entry->hpm.val.hpm_valid = MEA_TRUE;
                entry->hpm.val.numof_UE_PDN = 14;
                entry->hpm.val.hpm_profile = 64;
                entry->hpm.val.hpm_numof_rules = 32;
                entry->hpm.val.hpm_Type = 1;
                entry->hpm.val.hpm_numOfAct = 18;

                entry->hpm.val.hpm_BASE = 262400;
                entry->hpm.val.hpm_Mask_profile = 9;



                /*******************************************************/
                /*only for ATM mode*/
                entry->pre_parser.val.exist = MEA_FALSE;
                entry->pre_parser.val.table_size = 0; //TBD
                entry->pre_parser.val.vsp_exist = MEA_FALSE;

                /*******************************************************/
                entry->parser.val.exist = MEA_TRUE;
                entry->parser.val.parser_etype_cam = MEA_FALSE;
                entry->parser.val.table_size = 128;
                entry->parser.val.numofBit = 216;
                entry->parser.val.support_IPV6 = MEA_TRUE;
                /*******************************************************/

                entry->service.val.exist = MEA_TRUE;
                entry->service.val.table_size = 512;
                entry->service.val.serviceRangeExist = MEA_TRUE;
                entry->service.val.num_service_hash = 0;
                entry->service.val.ServiceRange_Num_of_range = 8;
                entry->service.val.numofRangeProfile = 32;
                entry->service.val.EVC_Classifier_exist = MEA_TRUE;
                entry->service.val.EVC_classifier_key_width = 24;
                entry->service.val.EVC_Classifier_num_hash_group = 0;

                entry->service.val.srv_externl_size = 4;

                /*******************************************************/
                entry->acl.val.exist = MEA_TRUE;
                entry->acl.val.table_size = 256;
                entry->acl.val.lxcp_act_width = 8; // 2^8 is 256 Action
                entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile

                /*******************************************************/
                entry->forwarder_lmt.val.exist = MEA_TRUE;
                entry->forwarder_lmt.val.table_size = 512;

                /*******************************************************/
                entry->forwarder.val.exist = MEA_TRUE;
                entry->forwarder.val.addres_size = 23;
                entry->forwarder.val.adm_support = MEA_TRUE;
                num_of_forwarder_Action = 128 * 1024; // 16384;//8192;
                entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
                entry->forwarder.val.width_learn_action_id = 18;
                entry->forwarder.val.width_learn_src_port = 9;
                entry->global.val.forworder_filter_support = MEA_TRUE;
                /*******************************************************/
                entry->shaper.val.shaper_per_port_exist = MEA_TRUE;
                entry->shaper.val.shaper_per_cluster_exist = MEA_TRUE;
                entry->shaper.val.shaper_per_Priqueue_exist = MEA_TRUE;
                entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster
                entry->shaper.val.xCIR_en = MEA_TRUE;



               /*******************************************************/
                entry->policer.val.exist = MEA_TRUE;

                entry->policer.val.reduce = MEA_TRUE;
                entry->policer.val.num_of_TMID = 8192;  //512;  // 1024

                entry->policer.val.hidden_profile = 1;
                entry->policer.val.num_of_acm = 1;   // 1 when no ACM
                entry->policer.val.num_of_profile = 128; //64;
                entry->policer.val.num_of_wred_prof = 1;
                entry->policer.val.num_of_wred_acm = 1;
                entry->policer.val.Ingress_flow_exist = MEA_TRUE;
                entry->policer.val.Ingress_flow_num_of_prof = 11;  //2^11 = 2048;

                                                                   /*******************************************************/
                entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
                entry->wfq.val.queue_exist = MEA_TRUE;
                entry->wfq.val.num_of_cluster = 1024;//128;  // num of cluster
                entry->wfq.val.maxCluserperport = 64;    //  32 if the HW support
                entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding


                                                                      /*******************************************************/
                entry->action.val.exist = MEA_TRUE;
                //entry->action.val.table_size = 524544; //164096;
                if (entry->hpm.val.hpm_valid)
                    numof_hpmAction = (1 << entry->hpm.val.hpm_numOfAct);

                entry->action.val.table_size = (MEA_OS_MAX(entry->service.val.table_size, (num_of_forwarder_Action * 2)) + entry->acl.val.table_size + numof_hpmAction);


                /*******************************************************/
                entry->xpermission.val.exist = MEA_TRUE;
				entry->xpermission.val.xper_table_size = 2048; //4096; //2048; //512; //1024;

                if (entry->global.val.mstp_support == MEA_TRUE)
                    entry->xpermission.val.num_ofMSTP = 9;   /*512*/
                else {
                    if (entry->global.val.rstp_support == MEA_TRUE)
                        entry->xpermission.val.num_ofMSTP = 1;   /*1 */
                }
                entry->xpermission.val.num_lag_prof = 8;


                /*******************************************************/
                entry->sar_machine.val.exist = MEA_FALSE;
                entry->sar_machine.val.table_size = 0;
                /*******************************************************/

                num_of_edit = 256 * 1024; //16384;
                entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
                entry->editor_BMF.val.atm_support = MEA_FALSE;
                entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
                entry->editor_BMF.val.router_support = MEA_FALSE;
                entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
                entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
                entry->editor_BMF.val.num_of_lm_count = 16;

                entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
                entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

                /*******************************************************/
                NumOfPmIds = 256 * 1024; //16384;
                entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
                entry->pm.val.num0fBlocksForBytes = 1;
                entry->pm.val.num0fBlocksForPmPackets = 1;


                /*******************************************************/
                entry->packetGen.val.PacketGen_exist = MEA_TRUE;
                entry->packetGen.val.Analyzer_exist = MEA_TRUE;
                entry->packetGen.val.PacketGen_On_Port_num = 120;
                entry->packetGen.val.Analyzer_On_Port_num = 120;


                entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
                entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
                entry->packetGen.val.PacketGen_num_of_support_profile = 8;
                entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
                entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
                entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets


                                                                          /***************************************************************/



                                                                          /**************************************************************/
                entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
                entry->fragment.val.fragment_Edit_num_of_session = 0;
                entry->fragment.val.fragment_Edit_num_of_port = 0;
                entry->fragment.val.fragment_Edit_num_of_group = 0;

                entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
                entry->fragment.val.fragment_vsp_num_of_session = 0;
                entry->fragment.val.fragment_vsp_num_of_port = 0;
                entry->fragment.val.fragment_vsp_num_of_group = 0;
                /*******************************************************************************/
                entry->Interface_Tdm_A.val.exist = MEA_FALSE;
                entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
                entry->Interface_Tdm_A.val.start_ts = 0;
                entry->Interface_Tdm_A.val.num0fspe = 12;

                entry->Interface_Tdm_B.val.exist = MEA_FALSE;
                entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
                entry->Interface_Tdm_B.val.start_ts = 96;
                entry->Interface_Tdm_B.val.num0fspe = 8;

                entry->Interface_Tdm_C.val.exist = MEA_FALSE;
                entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
                entry->Interface_Tdm_C.val.start_ts = 0;
                entry->Interface_Tdm_C.val.num0fspe = 0; //6;

                entry->global_Tdm.val.num_of_ces = 384;
                entry->global_Tdm.val.ts_size = 512;  // need to * 32
                                                      /************************************************************************/


                entry->Hc_Classifier.val.Hc_classifier_exist = MEA_TRUE;
                entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

                entry->Hc_Classifier.val.Num_of_hcid = 32;
                entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
                entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
                entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

                entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

                entry->slice0.index_40_regs[0] = 0x00ffffff;
                entry->slice0.index_40_regs[1] = 0;
                entry->slice0.index_40_regs[2] = 0;
                entry->slice0.index_40_regs[3] = 0x0000f000;

                entry->slice1.index_41_regs[0] = 0;
                entry->slice1.index_41_regs[1] = 0;
                entry->slice1.index_41_regs[2] = 0;
                entry->slice1.index_41_regs[3] = 0x810003f0;







#endif
                break;


     case MEA_OS_DEVICE_SIMULATION_ASIC1 :
#if 1
            entry->global.val.encription_enable            = MEA_TRUE;        
            entry->global.val.protection_enable            = MEA_FALSE;
            entry->global.val.bmf_martini_support          = MEA_FALSE; 
           
            
            entry->global.val.forwarder_external           = MEA_TRUE;
            entry->global.val.desc_external                = MEA_TRUE;
            entry->global.val.desc_checker_exist           = MEA_FALSE;
            entry->global.val.host_reduce                  = MEA_TRUE;
            entry->global.val.pause_support                = MEA_TRUE;
            entry->global.val.pre_sched_enable             = MEA_FALSE;

            entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
            entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;

            entry->global.val.fragment_exist               = MEA_FALSE;
            entry->global.val.tx_chunk_size                = 8;
            
            entry->global.val.ACM_exist                    = MEA_FALSE;
            entry->global.val.dual_latency_support         = MEA_TRUE;
            entry->global.val.fragment_bonding             = MEA_FALSE;
            entry->global.val.cpe_exist                    = MEA_FALSE;
            entry->global.val.dual_latency_support         = MEA_FALSE;
            


           entry->global.val.tdm_enable                    = MEA_FALSE;
           entry->global.val.large_BM_ind_address          = MEA_TRUE;      
                         
           entry->global.val.protocolMapping               = MEA_FALSE;
           entry->global.val.cls_large                     = MEA_TRUE;              
           entry->global.val.Support_1588                  = MEA_FALSE;             
           entry->global.val.BM_bus_width                  = 1 ;              
           entry->global.val.rx_pause_support              = MEA_FALSE;          
           entry->global.val.lag_support                   = MEA_TRUE;                
           entry->global.val.mstp_support                  = MEA_TRUE;
           entry->global.val.rstp_support                  = MEA_TRUE;

           entry->global.val.mc_mqs_Support                = MEA_TRUE;           
           entry->global.val.cluster_adminon_support       = MEA_TRUE;   
          
             
           entry->global.val.ingressMacfifo_small          = MEA_TRUE;      
           entry->global.val.AlloC_AllocR                  = MEA_FALSE;              
                             
           entry->global.val.MAC_LB_support                = MEA_TRUE;         
                   
           entry->global.val.reduce_lq                     = MEA_FALSE;                 
           entry->global.val.reduce_parser_tdm             = MEA_FALSE;
           entry->global.val.fragment_9991                 = MEA_FALSE;                 
           entry->global.val.forworder_filter_support      = MEA_FALSE;  
           entry->global.val.serdesReset_support           = MEA_FALSE;       
           entry->global.val.autoneg_support               = MEA_FALSE;
           
           entry->global.val.standard_bonding_support      = MEA_TRUE;
           
           entry->global.val.rmonRx_drop_support           = MEA_FALSE;
           entry->global.val.afdx_support                  = MEA_FALSE;
           entry->global.val.HeaderCompress_support        = MEA_TRUE;
           entry->global.val.HeaderDeCompress_support      = MEA_TRUE;
           entry->global.val.ac_MTU_support                = MEA_TRUE;
           entry->global.val.PTP1588_support                = MEA_TRUE;
           
//-----------------------------------------------------
           entry->global.val.num_of_slice                  = 2;
           entry->global.val.num_bonding_groupTx           = 16;
           entry->global.val.num_bonding_groupRx           = 1;
//-----------------------------------------------------
           entry->global.val.wbrg_support                  = MEA_TRUE;
           entry->global.val.ECN_support                   = MEA_TRUE;
           entry->global.val.Swap_machine_enable           = MEA_TRUE;


           
           entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;

            /*******************************************************/
            /*only for ATM mode*/
            entry->pre_parser.val.exist                    = MEA_FALSE;
            entry->pre_parser.val.table_size               = 0; //TBD
            entry->pre_parser.val.vsp_exist                = MEA_FALSE;

            /*******************************************************/
            entry->parser.val.exist                      = MEA_TRUE;
            entry->parser.val.parser_etype_cam           = MEA_FALSE; 
            entry->parser.val.table_size                 = 128;
            entry->parser.val.numofBit                   = 216;
            /*******************************************************/

            entry->service.val.exist                     = MEA_TRUE;
            entry->service.val.table_size                = 1024;
            entry->service.val.serviceRangeExist         = MEA_TRUE;
            entry->service.val.num_service_hash          = 0;
            entry->service.val.ServiceRange_Num_of_range = 4;
            entry->service.val.numofRangeProfile         = 64;
            entry->service.val.EVC_Classifier_exist = MEA_TRUE;
            entry->service.val.EVC_classifier_key_width      = 24;
            entry->service.val.EVC_Classifier_num_hash_group = 0;

            /*******************************************************/
            entry->acl.val.exist                         = MEA_TRUE;  
            entry->acl.val.table_size                    = 256;
            entry->acl.val.lxcp_act_width                 = 8; // 2^8 is 256 Action
            entry->acl.val.lxcp_prof_width                = 4; // 2^4 is 16 profile

            /*******************************************************/
            entry->forwarder_lmt.val.exist               = MEA_TRUE;
            entry->forwarder_lmt.val.table_size          = 512;

            /*******************************************************/
            entry->forwarder.val.exist               = MEA_TRUE;
            entry->forwarder.val.addres_size         = 23;      
            entry->forwarder.val.adm_support         = MEA_TRUE;
            num_of_forwarder_Action                 =   1024;
            entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
            entry->forwarder.val.width_learn_action_id  =     10;
            entry->forwarder.val.width_learn_src_port =        9;
            entry->global.val.forworder_filter_support      = MEA_TRUE;
            /*******************************************************/
            entry->shaper.val.shaper_per_port_exist=MEA_TRUE;
            entry->shaper.val.shaper_per_cluster_exist=MEA_TRUE;
            entry->shaper.val.shaper_per_Priqueue_exist=MEA_TRUE;
            entry->shaper.val.num_of_shaper_profile=256;  // for port and priQue and cluster

            

            /*******************************************************/
            entry->policer.val.exist                 = MEA_TRUE; 

            entry->policer.val.reduce                = MEA_TRUE;
            entry->policer.val.num_of_TMID = 512;  // 1024
            
            entry->policer.val.hidden_profile        = 3;
            entry->policer.val.num_of_acm            = 1;   // 1 when no ACM
            entry->policer.val.num_of_profile        = 128; //64;
            entry->policer.val.num_of_wred_prof      = 2;
            entry->policer.val.num_of_wred_acm       = 1;

            /*******************************************************/
            entry->wfq.val.cluster_exist             = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
            entry->wfq.val.queue_exist               = MEA_TRUE;
            entry->wfq.val.num_of_cluster            = 64;  // num of cluster
            entry->wfq.val.maxCluserperport          = 8;    //  32 if the HW support
            entry->wfq.val.clusterTo_multiport_exist  = MEA_FALSE; // only on bonding


            /*******************************************************/
            entry->action.val.exist                  = MEA_TRUE;
            entry->action.val.table_size         = (entry->service.val.table_size + 
                                                   num_of_forwarder_Action        + 
                                                   entry->acl.val.table_size);
            /*******************************************************/
            entry->xpermission.val.exist             = MEA_TRUE;
            entry->xpermission.val.xper_table_size = 1024;
            
            if(entry->global.val.mstp_support == MEA_TRUE)
               entry->xpermission.val.num_ofMSTP        =  9;   /*512*/
            else{
                if (entry->global.val.rstp_support == MEA_TRUE)
                    entry->xpermission.val.num_ofMSTP = 1;   /*1 */
            }
            entry->xpermission.val.num_lag_prof      =  8;


            /*******************************************************/
            entry->sar_machine.val.exist             = MEA_FALSE;
            entry->sar_machine.val.table_size        = 0;   
            /*******************************************************/
            num_of_edit = 512;
            entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
            entry->editor_BMF.val.atm_support           = MEA_FALSE;
            entry->editor_BMF.val.maritini_mac_support  = MEA_FALSE;
            entry->editor_BMF.val.router_support        = MEA_FALSE;

            entry->editor_BMF.val.BMF_CCM_count_EN      = MEA_TRUE;
            entry->editor_BMF.val.BMF_CCM_count_stamp   = MEA_TRUE;
            entry->editor_BMF.val.num_of_lm_count       =  16;

            entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
            entry->editor_BMF.val.MC_Edit_new_mode           = MEA_TRUE;

            /*******************************************************/
            NumOfPmIds = 512;
            entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
            entry->pm.val.num0fBlocksForBytes = 1;
            entry->pm.val.num0fBlocksForPmPackets = 1;
            

            /*******************************************************/
            entry->packetGen.val.PacketGen_exist         = MEA_TRUE;
            entry->packetGen.val.Analyzer_exist          = MEA_TRUE;
            entry->packetGen.val.PacketGen_On_Port_num   = 120;
            entry->packetGen.val.Analyzer_On_Port_num    = 120;
            
           
            entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
            entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
            entry->packetGen.val.PacketGen_num_of_support_profile = 8;
            entry->packetGen.val.Analyzer_type1_num_of_streams  = 32;//16; /*lbm lbr*/
            entry->packetGen.val.Analyzer_type2_num_of_streams  = 32; /*ccm*/
            entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets

             
             /***************************************************************/
 

             /**************************************************************/
             entry->fragment.val.fragment_Edit_exist           = MEA_FALSE;
             entry->fragment.val.fragment_Edit_num_of_session  = 0;
             entry->fragment.val.fragment_Edit_num_of_port     = 0;
             entry->fragment.val.fragment_Edit_num_of_group    = 0;

             entry->fragment.val.fragment_vsp_exist           = MEA_FALSE;
             entry->fragment.val.fragment_vsp_num_of_session  = 0;
             entry->fragment.val.fragment_vsp_num_of_port     = 0;
             entry->fragment.val.fragment_vsp_num_of_group    = 0;
   /*******************************************************************************/
            entry->Interface_Tdm_A.val.exist = MEA_FALSE;
            entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_SBI;
            entry->Interface_Tdm_A.val.start_ts  = 0;
            entry->Interface_Tdm_A.val.num0fspe  = 12;

            entry->Interface_Tdm_B.val.exist = MEA_FALSE;
            entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
            entry->Interface_Tdm_B.val.start_ts  = 96;
            entry->Interface_Tdm_B.val.num0fspe  = 8;

            entry->Interface_Tdm_C.val.exist = MEA_FALSE;
            entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
            entry->Interface_Tdm_C.val.start_ts  =0;
            entry->Interface_Tdm_C.val.num0fspe =0; //6;

            entry->global_Tdm.val.num_of_ces = 384; 
            entry->global_Tdm.val.ts_size    =  512;  // need to * 32
 /************************************************************************/


            entry->Hc_Classifier.val.Hc_classifier_exist      =MEA_TRUE;
            entry->Hc_Classifier.val.Hc_classifierRang_exist  =MEA_FALSE;
                         
            entry->Hc_Classifier.val.Num_of_hcid =             32;             
            entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
            entry->Hc_Classifier.val.Hc_classifier_key_width  = 512;
            entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

            entry->Egress_Classifier.val.Egress_classifier_exist=MEA_FALSE;

            entry->slice0.index_40_regs[0]= 0x00ffffff;
            entry->slice0.index_40_regs[1]= 0;
            entry->slice0.index_40_regs[2]= 0;
            entry->slice0.index_40_regs[3]=0x0000f000;

            entry->slice1.index_41_regs[0]= 0;
            entry->slice1.index_41_regs[1]= 0;
            entry->slice1.index_41_regs[2]= 0;
            entry->slice1.index_41_regs[3]= 0x810003f0;


            
      


          
#endif
            break;
    case MEA_OS_DEVICE_SIMULATION_VDSlK7:
#if 1
        entry->global.val.encription_enable = MEA_TRUE;
        entry->global.val.protection_enable = MEA_FALSE;
        entry->global.val.bmf_martini_support = MEA_FALSE;
        
        entry->global.val.forwarder_external = MEA_TRUE;
        entry->global.val.desc_external = MEA_TRUE;
        entry->global.val.desc_checker_exist = MEA_FALSE;
        entry->global.val.host_reduce = MEA_TRUE;
        entry->global.val.pause_support = MEA_TRUE;
        entry->global.val.pre_sched_enable = MEA_FALSE;

        entry->global.val.flowCos_Mapping_exist = MEA_TRUE;
        entry->global.val.flowMarking_Mapping_exist = MEA_TRUE;

        entry->global.val.fragment_exist = MEA_FALSE;
        entry->global.val.tx_chunk_size = 8;
        
        entry->global.val.ACM_exist = MEA_FALSE;
        entry->global.val.dual_latency_support = MEA_TRUE;
        entry->global.val.fragment_bonding = MEA_FALSE;
        entry->global.val.cpe_exist = MEA_FALSE;
        entry->global.val.dual_latency_support = MEA_FALSE;
        


        entry->global.val.tdm_enable = MEA_FALSE;
        entry->global.val.large_BM_ind_address = MEA_TRUE;
        
        entry->global.val.protocolMapping = MEA_FALSE;
        entry->global.val.cls_large = MEA_TRUE;
        entry->global.val.Support_1588 = MEA_FALSE;
        entry->global.val.BM_bus_width = 1;
        entry->global.val.rx_pause_support = MEA_FALSE;
        entry->global.val.lag_support = MEA_TRUE;
        entry->global.val.mstp_support = MEA_TRUE;
        entry->global.val.rstp_support = MEA_TRUE;

        entry->global.val.mc_mqs_Support = MEA_TRUE;
        entry->global.val.cluster_adminon_support = MEA_TRUE;


        entry->global.val.ingressMacfifo_small = MEA_TRUE;
        entry->global.val.AlloC_AllocR = MEA_FALSE;

        entry->global.val.MAC_LB_support = MEA_TRUE;
        
        entry->global.val.reduce_lq = MEA_FALSE;
        entry->global.val.reduce_parser_tdm = MEA_FALSE;
        entry->global.val.fragment_9991 = MEA_FALSE;
        entry->global.val.forworder_filter_support = MEA_FALSE;
        entry->global.val.serdesReset_support = MEA_FALSE;
        entry->global.val.autoneg_support = MEA_FALSE;
        
        entry->global.val.standard_bonding_support = MEA_TRUE;
        
        entry->global.val.rmonRx_drop_support = MEA_FALSE;
        entry->global.val.afdx_support = MEA_FALSE;
        entry->global.val.HeaderCompress_support = MEA_TRUE;
        entry->global.val.HeaderDeCompress_support = MEA_TRUE;
        entry->global.val.ac_MTU_support = MEA_TRUE;
        entry->global.val.PTP1588_support = MEA_TRUE;

        //-----------------------------------------------------
        entry->global.val.num_of_slice = 2;
        entry->global.val.num_bonding_groupTx = 16;
        entry->global.val.num_bonding_groupRx = 1;
        //-----------------------------------------------------
        entry->global.val.wbrg_support = MEA_TRUE;
        entry->global.val.ECN_support = MEA_TRUE;
        entry->global.val.Swap_machine_enable = MEA_TRUE;
        entry->global.val.GATEWAY_EN = MEA_TRUE;

        /************************************************************/
        
        entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
        /*******************************************************/
        /*only for ATM mode*/
        entry->pre_parser.val.exist = MEA_FALSE;
        entry->pre_parser.val.table_size = 0; //TBD
        entry->pre_parser.val.vsp_exist = MEA_FALSE;

        /*******************************************************/
        entry->parser.val.exist = MEA_TRUE;
        entry->parser.val.parser_etype_cam = MEA_FALSE;
        entry->parser.val.table_size = 128;
        entry->parser.val.numofBit = 216;
        /*******************************************************/

        entry->service.val.exist = MEA_TRUE;
        entry->service.val.table_size = 1024;
        entry->service.val.serviceRangeExist = MEA_TRUE;
        entry->service.val.num_service_hash = 0;
        entry->service.val.ServiceRange_Num_of_range = 4;
        entry->service.val.numofRangeProfile = 64;
        entry->service.val.EVC_Classifier_exist = MEA_TRUE;
        entry->service.val.EVC_classifier_key_width = 24;
        entry->service.val.EVC_Classifier_num_hash_group = 0;

        /*******************************************************/
        entry->acl.val.exist = MEA_TRUE;
        entry->acl.val.table_size = 256;
        entry->acl.val.lxcp_act_width = 8; // 2^8 is 256 Action
        entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile

        /*******************************************************/
        entry->forwarder_lmt.val.exist = MEA_TRUE;
        entry->forwarder_lmt.val.table_size = 512;

        /*******************************************************/
        entry->forwarder.val.exist = MEA_TRUE;
        entry->forwarder.val.addres_size = 23;
        entry->forwarder.val.adm_support = MEA_TRUE;
        num_of_forwarder_Action = 1024;
        entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
        entry->forwarder.val.width_learn_action_id = 10;
        entry->forwarder.val.width_learn_src_port = 9;
        entry->global.val.forworder_filter_support = MEA_TRUE;
        /*******************************************************/
        entry->shaper.val.shaper_per_port_exist = MEA_TRUE;
        entry->shaper.val.shaper_per_cluster_exist = MEA_TRUE;
        entry->shaper.val.shaper_per_Priqueue_exist = MEA_TRUE;
        entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster



        /*******************************************************/
        entry->policer.val.exist = MEA_TRUE;

        entry->policer.val.reduce = MEA_TRUE;
        entry->policer.val.num_of_TMID = 512;  // 1024

        entry->policer.val.hidden_profile = 3;
        entry->policer.val.num_of_acm = 1;   // 1 when no ACM
        entry->policer.val.num_of_profile = 128; //64;
        entry->policer.val.num_of_wred_prof = 2;
        entry->policer.val.num_of_wred_acm = 1;
        entry->policer.val.Ingress_flow_exist = MEA_TRUE;
        entry->policer.val.Ingress_flow_num_of_prof = 9;  //2^11 = 2048

        /*******************************************************/
        entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
        entry->wfq.val.queue_exist = MEA_TRUE;
        entry->wfq.val.num_of_cluster = 128;  // num of cluster
        entry->wfq.val.maxCluserperport = 8;    //  32 if the HW support
        entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding


        /*******************************************************/
        entry->action.val.exist = MEA_TRUE;
        entry->action.val.table_size = (entry->service.val.table_size +
            num_of_forwarder_Action +
            entry->acl.val.table_size);
        /*******************************************************/
        entry->xpermission.val.exist = MEA_TRUE;
        entry->xpermission.val.xper_table_size = 512;

        if (entry->global.val.mstp_support == MEA_TRUE)
            entry->xpermission.val.num_ofMSTP = 9;   /*512*/
        else{
            if (entry->global.val.rstp_support == MEA_TRUE)
                entry->xpermission.val.num_ofMSTP = 1;   /*1 */
        }
        entry->xpermission.val.num_lag_prof = 8;


        /*******************************************************/
        entry->sar_machine.val.exist = MEA_FALSE;
        entry->sar_machine.val.table_size = 0;
        /*******************************************************/
        
        num_of_edit = 512;
        entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
        entry->editor_BMF.val.atm_support = MEA_FALSE;
        entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
        entry->editor_BMF.val.router_support = MEA_FALSE;
        entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
        entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
        entry->editor_BMF.val.num_of_lm_count = 16;

        entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
        entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

        /*******************************************************/
        NumOfPmIds = 512;
        entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
        entry->pm.val.num0fBlocksForBytes = 1;
        entry->pm.val.num0fBlocksForPmPackets = 1;


        /*******************************************************/
        entry->packetGen.val.PacketGen_exist = MEA_TRUE;
        entry->packetGen.val.Analyzer_exist = MEA_TRUE;
        entry->packetGen.val.PacketGen_On_Port_num = 120;
        entry->packetGen.val.Analyzer_On_Port_num = 120;


        entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
        entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
        entry->packetGen.val.PacketGen_num_of_support_profile = 8;
        entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
        entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
        entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets


        /***************************************************************/


        /**************************************************************/
        entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
        entry->fragment.val.fragment_Edit_num_of_session = 0;
        entry->fragment.val.fragment_Edit_num_of_port = 0;
        entry->fragment.val.fragment_Edit_num_of_group = 0;

        entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
        entry->fragment.val.fragment_vsp_num_of_session = 0;
        entry->fragment.val.fragment_vsp_num_of_port = 0;
        entry->fragment.val.fragment_vsp_num_of_group = 0;
        /*******************************************************************************/
        entry->Interface_Tdm_A.val.exist = MEA_FALSE;
        entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
        entry->Interface_Tdm_A.val.start_ts = 0;
        entry->Interface_Tdm_A.val.num0fspe = 12;

        entry->Interface_Tdm_B.val.exist = MEA_FALSE;
        entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
        entry->Interface_Tdm_B.val.start_ts = 96;
        entry->Interface_Tdm_B.val.num0fspe = 8;

        entry->Interface_Tdm_C.val.exist = MEA_FALSE;
        entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
        entry->Interface_Tdm_C.val.start_ts = 0;
        entry->Interface_Tdm_C.val.num0fspe = 0; //6;

        entry->global_Tdm.val.num_of_ces = 384;
        entry->global_Tdm.val.ts_size = 512;  // need to * 32
        /************************************************************************/


        entry->Hc_Classifier.val.Hc_classifier_exist = MEA_TRUE;
        entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

        entry->Hc_Classifier.val.Num_of_hcid = 32;
        entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
        entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
        entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

        entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

        entry->slice0.index_40_regs[0] = 0x00ffffff;
        entry->slice0.index_40_regs[1] = 0;
        entry->slice0.index_40_regs[2] = 0;
        entry->slice0.index_40_regs[3] = 0x0000f000;

        entry->slice1.index_41_regs[0] = 0;
        entry->slice1.index_41_regs[1] = 0;
        entry->slice1.index_41_regs[2] = 0;
        entry->slice1.index_41_regs[3] = 0x810003f0;







#endif                
    break;
        case MEA_OS_DEVICE_SIMULATION_VDSlK7_196:
        case MEA_OS_DEVICE_SIMULATION_VDSlK7_196_2SFP:
#if 1
            entry->global.val.encription_enable = MEA_TRUE;
            entry->global.val.protection_enable = MEA_FALSE;
            entry->global.val.bmf_martini_support = MEA_FALSE;
            
            entry->global.val.forwarder_external = MEA_TRUE;
            entry->global.val.desc_external = MEA_TRUE;
            entry->global.val.desc_checker_exist = MEA_FALSE;
            entry->global.val.host_reduce = MEA_TRUE;
            entry->global.val.pause_support = MEA_TRUE;
            entry->global.val.pre_sched_enable = MEA_FALSE;

            entry->global.val.flowCos_Mapping_exist = MEA_TRUE;
            entry->global.val.flowMarking_Mapping_exist = MEA_TRUE;

            entry->global.val.fragment_exist = MEA_FALSE;
            entry->global.val.tx_chunk_size = 8;
          
            entry->global.val.ACM_exist = MEA_FALSE;
            entry->global.val.dual_latency_support = MEA_TRUE;
            entry->global.val.fragment_bonding = MEA_FALSE;
            entry->global.val.cpe_exist = MEA_FALSE;
            entry->global.val.dual_latency_support = MEA_FALSE;
           


            entry->global.val.tdm_enable = MEA_FALSE;
            entry->global.val.large_BM_ind_address = MEA_TRUE;
           
            entry->global.val.protocolMapping = MEA_FALSE;
            entry->global.val.cls_large = MEA_TRUE;
            entry->global.val.Support_1588 = MEA_FALSE;
            entry->global.val.BM_bus_width = 1;
            entry->global.val.rx_pause_support = MEA_FALSE;
            entry->global.val.lag_support = MEA_TRUE;
            entry->global.val.mstp_support = MEA_TRUE;
            entry->global.val.rstp_support = MEA_TRUE;

            entry->global.val.mc_mqs_Support = MEA_TRUE;
            entry->global.val.cluster_adminon_support = MEA_TRUE;


            entry->global.val.ingressMacfifo_small = MEA_TRUE;
            entry->global.val.AlloC_AllocR = MEA_FALSE;

            entry->global.val.MAC_LB_support = MEA_TRUE;
            
            entry->global.val.reduce_lq = MEA_FALSE;
            entry->global.val.reduce_parser_tdm = MEA_FALSE;
            entry->global.val.fragment_9991 = MEA_FALSE;
            entry->global.val.forworder_filter_support = MEA_FALSE;
            entry->global.val.serdesReset_support = MEA_FALSE;
            entry->global.val.autoneg_support = MEA_FALSE;

            entry->global.val.standard_bonding_support = MEA_TRUE;
            
            entry->global.val.rmonRx_drop_support = MEA_FALSE;
            entry->global.val.afdx_support = MEA_FALSE;
            entry->global.val.HeaderCompress_support = MEA_TRUE;
            entry->global.val.HeaderDeCompress_support = MEA_TRUE;
            entry->global.val.ac_MTU_support = MEA_TRUE;
            entry->global.val.PTP1588_support = MEA_TRUE;

            //-----------------------------------------------------
            entry->global.val.num_of_slice = 2;
            entry->global.val.num_bonding_groupTx = 16;
            entry->global.val.num_bonding_groupRx = 1;
            //-----------------------------------------------------
            entry->global.val.wbrg_support = MEA_TRUE;
            entry->global.val.ECN_support = MEA_TRUE;
            entry->global.val.Swap_machine_enable = MEA_TRUE;
            entry->global.val.GATEWAY_EN = MEA_TRUE;

            
            entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
            /*******************************************************/
            /*only for ATM mode*/
            entry->pre_parser.val.exist = MEA_FALSE;
            entry->pre_parser.val.table_size = 0; //TBD
            entry->pre_parser.val.vsp_exist = MEA_FALSE;

            /*******************************************************/
            entry->parser.val.exist = MEA_TRUE;
            entry->parser.val.parser_etype_cam = MEA_FALSE;
            entry->parser.val.table_size = 128;
            entry->parser.val.numofBit = 216;
            /*******************************************************/

            entry->service.val.exist = MEA_TRUE;
            entry->service.val.table_size = 2048; //1024;
            entry->service.val.serviceRangeExist = MEA_TRUE;
            entry->service.val.num_service_hash = 0;
            entry->service.val.ServiceRange_Num_of_range = 4;
            entry->service.val.numofRangeProfile = 64;
            entry->service.val.EVC_Classifier_exist = MEA_TRUE;
            entry->service.val.EVC_classifier_key_width = 24;
            entry->service.val.EVC_Classifier_num_hash_group = 0;

            /*******************************************************/
            entry->acl.val.exist = MEA_TRUE;
            entry->acl.val.table_size = 1024; //256;
            entry->acl.val.lxcp_act_width = 8; // 2^8 is 256 Action
            entry->acl.val.lxcp_prof_width = 4; // 2^4 is 16 profile

            /*******************************************************/
            entry->forwarder_lmt.val.exist = MEA_TRUE;
            entry->forwarder_lmt.val.table_size = 512;

            /*******************************************************/
            entry->forwarder.val.exist = MEA_TRUE;
            entry->forwarder.val.addres_size = 23;
            entry->forwarder.val.adm_support = MEA_TRUE;
            num_of_forwarder_Action = 2048;//1024;
            entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
            entry->forwarder.val.width_learn_action_id = 10;
            entry->forwarder.val.width_learn_src_port = 9;
            entry->global.val.forworder_filter_support = MEA_TRUE;
            /*******************************************************/
            entry->shaper.val.shaper_per_port_exist = MEA_TRUE;
            entry->shaper.val.shaper_per_cluster_exist = MEA_TRUE;
            entry->shaper.val.shaper_per_Priqueue_exist = MEA_TRUE;
            entry->shaper.val.num_of_shaper_profile = 256;  // for port and priQue and cluster



            /*******************************************************/
            entry->policer.val.exist = MEA_TRUE;

            entry->policer.val.reduce = MEA_TRUE;
            entry->policer.val.num_of_TMID = 2048;

            entry->policer.val.hidden_profile = 3;
            entry->policer.val.num_of_acm = 1;   // 1 when no ACM
            entry->policer.val.num_of_profile = 128; //64;
            entry->policer.val.num_of_wred_prof = 2;
            entry->policer.val.num_of_wred_acm = 1;

            /*******************************************************/
            entry->wfq.val.cluster_exist = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
            entry->wfq.val.queue_exist = MEA_TRUE;
            entry->wfq.val.num_of_cluster = 256;  // num of cluster
            entry->wfq.val.maxCluserperport = 8;    //  32 if the HW support
            entry->wfq.val.clusterTo_multiport_exist = MEA_FALSE; // only on bonding


            /*******************************************************/
            entry->action.val.exist = MEA_TRUE;
            entry->action.val.table_size = (entry->service.val.table_size +
                num_of_forwarder_Action +
                entry->acl.val.table_size);
            /*******************************************************/
            entry->xpermission.val.exist = MEA_TRUE;
            entry->xpermission.val.xper_table_size = 512;

            if (entry->global.val.mstp_support == MEA_TRUE)
                entry->xpermission.val.num_ofMSTP = 9;   /*512*/
            else{
                if (entry->global.val.rstp_support == MEA_TRUE)
                    entry->xpermission.val.num_ofMSTP = 1;   /*1 */
            }
            entry->xpermission.val.num_lag_prof = 8;


            /*******************************************************/
            entry->sar_machine.val.exist = MEA_FALSE;
            entry->sar_machine.val.table_size = 0;
            /*******************************************************/
           
            num_of_edit = 2048;
            entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
            entry->editor_BMF.val.atm_support = MEA_FALSE;
            entry->editor_BMF.val.maritini_mac_support = MEA_FALSE;
            entry->editor_BMF.val.router_support = MEA_FALSE;

            entry->editor_BMF.val.BMF_CCM_count_EN = MEA_TRUE;
            entry->editor_BMF.val.BMF_CCM_count_stamp = MEA_TRUE;
            entry->editor_BMF.val.num_of_lm_count = 16;

            entry->editor_BMF.val.num_of_MC_Edit_Per_Cluster = 64;
            entry->editor_BMF.val.MC_Edit_new_mode = MEA_TRUE;

            /*******************************************************/
            NumOfPmIds = 2048;
            entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
            entry->pm.val.num0fBlocksForBytes = 1;
            entry->pm.val.num0fBlocksForPmPackets = 1;


            /*******************************************************/
            entry->packetGen.val.PacketGen_exist = MEA_TRUE;
            entry->packetGen.val.Analyzer_exist = MEA_TRUE;
            entry->packetGen.val.PacketGen_On_Port_num = 120;
            entry->packetGen.val.Analyzer_On_Port_num = 120;


            entry->packetGen.val.Pktgen_type1_num_of_streams = 32;
            entry->packetGen.val.Pktgen_type2_num_of_streams = 32;
            entry->packetGen.val.PacketGen_num_of_support_profile = 8;
            entry->packetGen.val.Analyzer_type1_num_of_streams = 32;//16; /*lbm lbr*/
            entry->packetGen.val.Analyzer_type2_num_of_streams = 32; /*ccm*/
            entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets


            /***************************************************************/


            /**************************************************************/
            entry->fragment.val.fragment_Edit_exist = MEA_FALSE;
            entry->fragment.val.fragment_Edit_num_of_session = 0;
            entry->fragment.val.fragment_Edit_num_of_port = 0;
            entry->fragment.val.fragment_Edit_num_of_group = 0;

            entry->fragment.val.fragment_vsp_exist = MEA_FALSE;
            entry->fragment.val.fragment_vsp_num_of_session = 0;
            entry->fragment.val.fragment_vsp_num_of_port = 0;
            entry->fragment.val.fragment_vsp_num_of_group = 0;
            /*******************************************************************************/
            entry->Interface_Tdm_A.val.exist = MEA_FALSE;
            entry->Interface_Tdm_A.val.type = MEA_TDM_INTERFACE_SBI;
            entry->Interface_Tdm_A.val.start_ts = 0;
            entry->Interface_Tdm_A.val.num0fspe = 12;

            entry->Interface_Tdm_B.val.exist = MEA_FALSE;
            entry->Interface_Tdm_B.val.type = MEA_TDM_INTERFACE_PCM;
            entry->Interface_Tdm_B.val.start_ts = 96;
            entry->Interface_Tdm_B.val.num0fspe = 8;

            entry->Interface_Tdm_C.val.exist = MEA_FALSE;
            entry->Interface_Tdm_C.val.type = 0;//MEA_TDM_INTERFACE_PCM_MUX;
            entry->Interface_Tdm_C.val.start_ts = 0;
            entry->Interface_Tdm_C.val.num0fspe = 0; //6;

            entry->global_Tdm.val.num_of_ces = 384;
            entry->global_Tdm.val.ts_size = 512;  // need to * 32
            /************************************************************************/


            entry->Hc_Classifier.val.Hc_classifier_exist = MEA_FALSE;
            entry->Hc_Classifier.val.Hc_classifierRang_exist = MEA_FALSE;

            entry->Hc_Classifier.val.Num_of_hcid = 32;
            entry->Hc_Classifier.val.Hc_number_of_hash_groups = 0;
            entry->Hc_Classifier.val.Hc_classifier_key_width = 512;
            entry->Hc_Classifier.val.Hc_rmon_exist = MEA_TRUE;

            entry->Egress_Classifier.val.Egress_classifier_exist = MEA_FALSE;

            entry->slice0.index_40_regs[0] = 0x00ffffff;
            entry->slice0.index_40_regs[1] = 0;
            entry->slice0.index_40_regs[2] = 0;
            entry->slice0.index_40_regs[3] = 0x0000f000;

            entry->slice1.index_41_regs[0] = 0;
            entry->slice1.index_41_regs[1] = 0;
            entry->slice1.index_41_regs[2] = 0;
            entry->slice1.index_41_regs[3] = 0x810003f0;







#endif                
            break;
        case MEA_OS_DEVICE_SIMULATION_3875_8G:
#if 1
            entry->global.val.encription_enable            = MEA_TRUE;        
            entry->global.val.protection_enable            = MEA_FALSE;
            entry->global.val.bmf_martini_support          = MEA_FALSE; 
            
            
            entry->global.val.forwarder_external           = MEA_TRUE;
            entry->global.val.desc_external                = MEA_TRUE;
            entry->global.val.desc_checker_exist           = MEA_FALSE;
            entry->global.val.host_reduce                  = MEA_TRUE;
            entry->global.val.pause_support                = MEA_TRUE;
            entry->global.val.pre_sched_enable             = MEA_TRUE;

            entry->global.val.flowCos_Mapping_exist        = MEA_TRUE;
            entry->global.val.flowMarking_Mapping_exist    = MEA_TRUE;

            entry->global.val.fragment_exist               = MEA_FALSE;
            entry->global.val.tx_chunk_size                = 8;
        
            entry->global.val.ACM_exist                    = MEA_FALSE;
            entry->global.val.dual_latency_support         = MEA_TRUE;
            entry->global.val.fragment_bonding             = MEA_FALSE;
            entry->global.val.cpe_exist                    = MEA_FALSE;
            entry->global.val.dual_latency_support         = MEA_FALSE;
            


           entry->global.val.tdm_enable                    = MEA_FALSE;
           entry->global.val.large_BM_ind_address          = MEA_TRUE;      
                     
           entry->global.val.protocolMapping               = MEA_FALSE;
           entry->global.val.cls_large                     = MEA_TRUE;              
           entry->global.val.Support_1588                  = MEA_FALSE;             
           entry->global.val.BM_bus_width                  = 1 ;              
           entry->global.val.rx_pause_support              = MEA_FALSE;          
           entry->global.val.lag_support                   = MEA_FALSE;                
               
           entry->global.val.mc_mqs_Support                = MEA_TRUE;           
           entry->global.val.cluster_adminon_support       = MEA_TRUE;   
            
               
           entry->global.val.ingressMacfifo_small          = MEA_TRUE;      
           entry->global.val.AlloC_AllocR                  = MEA_FALSE;              
                             
           entry->global.val.MAC_LB_support             = MEA_FALSE;         
                  
           entry->global.val.reduce_lq                     = MEA_FALSE;                 
           entry->global.val.reduce_parser_tdm             = MEA_FALSE;
           entry->global.val.fragment_9991                 = MEA_FALSE;                 
           entry->global.val.forworder_filter_support      = MEA_FALSE;  
           entry->global.val.serdesReset_support           = MEA_FALSE;       
           entry->global.val.autoneg_support               = MEA_FALSE;
           
           entry->global.val.standard_bonding_support      = MEA_TRUE;
           
           entry->global.val.rmonRx_drop_support           = MEA_FALSE;
           entry->global.val.afdx_support                     =MEA_TRUE;


           entry->global.val.num_of_slice                  = 2;
           entry->global.val.num_bonding_groupTx           = 16;
           entry->global.val.num_bonding_groupRx           = 1;




           
           entry->global.val.mtu_pri_queue_reduce = MEA_FALSE;
            
            /*only for ATM mode*/
            entry->pre_parser.val.exist                    = MEA_FALSE;
            entry->pre_parser.val.table_size               = 0; //TBD
            entry->pre_parser.val.vsp_exist                = MEA_FALSE;

            
            entry->parser.val.exist                      = MEA_TRUE;
            entry->parser.val.parser_etype_cam           = MEA_FALSE; 
            entry->parser.val.table_size                 = 128; 
            

            entry->service.val.exist                     = MEA_TRUE;
            entry->service.val.table_size                = 512;
            entry->service.val.serviceRangeExist         = MEA_FALSE;
            entry->service.val.num_service_hash          = 0;
            entry->service.val.ServiceRange_Num_of_range =0;


            
            entry->acl.val.exist                         = MEA_TRUE;  
            entry->acl.val.table_size                    = 256;
            entry->acl.val.lxcp_act_width                 = 8; // 2^8 is 256 Action
            entry->acl.val.lxcp_prof_width                = 4; // 2^4 is 16 profile

           
            entry->forwarder_lmt.val.exist               = MEA_TRUE;
            entry->forwarder_lmt.val.table_size          = 512;

           
            entry->forwarder.val.exist               = MEA_TRUE;
            entry->forwarder.val.addres_size         = 23;      
            entry->forwarder.val.adm_support         = MEA_TRUE;
            num_of_forwarder_Action                 =   512;
            entry->forwarder.val.fwd_action = MEA_OS_calc_from_size_to_width(num_of_forwarder_Action);
            entry->forwarder.val.width_learn_action_id  =     10;
            entry->forwarder.val.width_learn_src_port =        9;   
           
            entry->shaper.val.shaper_per_port_exist=MEA_TRUE;
            entry->shaper.val.shaper_per_cluster_exist=MEA_TRUE;
            entry->shaper.val.shaper_per_Priqueue_exist=MEA_TRUE;
            entry->shaper.val.num_of_shaper_profile=256;  // for port and priQue and cluster

            

           
            entry->policer.val.exist                 = MEA_TRUE; 

            entry->policer.val.reduce                = MEA_TRUE;
            entry->policer.val.num_of_TMID = 512;  // 1024
            
            entry->policer.val.hidden_profile        = 3;
            entry->policer.val.num_of_acm            = 1;   // 1 when no ACM
            entry->policer.val.num_of_profile        = 128; //64;
            entry->policer.val.num_of_wred_prof      = 1;
            entry->policer.val.num_of_wred_acm       = 1;

           
            entry->wfq.val.cluster_exist             = MEA_TRUE; /* [MEA_TRUE WFQ and strict] [MEA_FALSE RR]*/
            entry->wfq.val.queue_exist               = MEA_TRUE;
            entry->wfq.val.num_of_cluster            = 64;  // num of cluster
            entry->wfq.val.maxCluserperport          = 8;    //  32 if the HW support
            entry->wfq.val.clusterTo_multiport_exist  = MEA_FALSE; // only on bonding


            /*******************************************************/
            entry->action.val.exist                  = MEA_TRUE;
            entry->action.val.table_size             = 1024+entry->acl.val.table_size; 
            /*******************************************************/
            entry->xpermission.val.exist             = MEA_TRUE;
            entry->xpermission.val.xper_table_size = 512;
            /*******************************************************/
            entry->sar_machine.val.exist             = MEA_FALSE;
            entry->sar_machine.val.table_size        = 0;   
            /*******************************************************/
            num_of_edit = 512;
            entry->editor_BMF.val.num_of_edit_width = MEA_OS_calc_from_size_to_width(num_of_edit);
            entry->editor_BMF.val.atm_support           = MEA_FALSE;
            entry->editor_BMF.val.maritini_mac_support  = MEA_FALSE;
            entry->editor_BMF.val.router_support        = MEA_FALSE;
            entry->editor_BMF.val.BMF_CCM_count_EN      = MEA_TRUE;
            entry->editor_BMF.val.BMF_CCM_count_stamp   = MEA_TRUE;
            entry->editor_BMF.val.num_of_lm_count       =  16;

            /*******************************************************/
            NumOfPmIds = 512;
            entry->pm.val.width_pm_id = MEA_OS_calc_from_size_to_width(NumOfPmIds);
            entry->pm.val.num0fBlocksForBytes = 1;
            entry->pm.val.num0fBlocksForPmPackets = 1;
            

            /*******************************************************/
            entry->packetGen.val.PacketGen_exist         = MEA_TRUE;
            entry->packetGen.val.Analyzer_exist          = MEA_TRUE;
            entry->packetGen.val.PacketGen_On_Port_num   = 120;
            entry->packetGen.val.Analyzer_On_Port_num    = 120;
            entry->packetGen.val.Analyzer_type1_num_of_streams  = 16;//16; /*lbm lbr*/
            entry->packetGen.val.Analyzer_type2_num_of_streams  = 16; /*ccm*/
            entry->packetGen.val.PacketGen_num_of_support_profile = 16;
            entry->packetGen.val.Pktgen_type1_num_of_streams = 16;
            entry->packetGen.val.Pktgen_type2_num_of_streams = 16;
            entry->packetGen.val.Analyzer_num_of_support_profile = 16;//buckets
            /*******************************************************************************/

             
             /***************************************************************/


             /**************************************************************/
             entry->fragment.val.fragment_Edit_exist           = MEA_FALSE;
             entry->fragment.val.fragment_Edit_num_of_session  = 0;
             entry->fragment.val.fragment_Edit_num_of_port     = 0;
             entry->fragment.val.fragment_Edit_num_of_group    = 0;

             entry->fragment.val.fragment_vsp_exist           = MEA_FALSE;
             entry->fragment.val.fragment_vsp_num_of_session  = 0;
             entry->fragment.val.fragment_vsp_num_of_port     = 0;
             entry->fragment.val.fragment_vsp_num_of_group    = 0;
   /*******************************************************************************/
            entry->Interface_Tdm_A.val.exist = MEA_FALSE;
            entry->Interface_Tdm_A.val.type  = MEA_TDM_INTERFACE_SBI;
            entry->Interface_Tdm_A.val.start_ts  = 0;
            entry->Interface_Tdm_A.val.num0fspe  = 12;

            entry->Interface_Tdm_B.val.exist = MEA_FALSE;
            entry->Interface_Tdm_B.val.type  = MEA_TDM_INTERFACE_PCM;
            entry->Interface_Tdm_B.val.start_ts  = 96;
            entry->Interface_Tdm_B.val.num0fspe  = 8;

            entry->Interface_Tdm_C.val.exist = MEA_FALSE;
            entry->Interface_Tdm_C.val.type  = 0;//MEA_TDM_INTERFACE_PCM_MUX;
            entry->Interface_Tdm_C.val.start_ts  =0;
            entry->Interface_Tdm_C.val.num0fspe =0; //6;

            entry->global_Tdm.val.num_of_ces = 384; 
            entry->global_Tdm.val.ts_size    =  512;  // need to * 32
 /************************************************************************/
            






#endif
            break;



    default : 
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The type % is not support for this Driver\n",
            __FUNCTION__,Type);

        break;
    }
    
    
    // mea_drv_Get_DeviceInfo_GetChipName(unit_i,hwUnit_i);
    
    return MEA_OK;
}






#endif
MEA_Status MEA_API_ALEX_DBG(MEA_Uint32 num)
{
 MEA_Uint32 size;
    

    alexDBG_t  *alex =NULL;
    

   
#ifdef ARCH64
    printf("DBG ALEX  IN  [%ld] \n",num);
     size=sizeof(alexDBG_t)*mea_drv_num_of_hw_units;
     printf("DBG ALEX  x1   size = %ld\n",size);
    alex = (alexDBG_t*)MEA_OS_malloc(size);
    printf("DBG ALEX  x2   size = %ld\n",size);
#else    
    printf("DBG ALEX  IN  [%d] \n",num);

     size=sizeof(alexDBG_t)*mea_drv_num_of_hw_units;
     printf("DBG ALEX  x1   size = %d\n",size);
    alex = (alexDBG_t*)MEA_OS_malloc(size);
    printf("DBG ALEX  x2   size = %d\n",size);
#endif    
    if(alex == NULL){
        printf("DBG ALEX   alex = NULL\n");
        return MEA_ERROR;
    }
    

   printf("DBG ALEX  x3   free\n");
   MEA_OS_free(alex);


return MEA_OK;
}



MEA_Status mea_drv_Get_DeviceInfo_Get_Port_Interface_str(mea_drv_portInterface_str_t type, MEA_Uint8 index, char *name_str)
{

    char portType[MEA_PORTTYPE_LAST+1][25];
    char InterfaceType[MEA_INTERFACETYPE_LAST+1][25];
    char Interface_Rate[MEA_INTERFACE_Rate_LAST + 1][25];


   if(name_str == NULL){
       return MEA_ERROR;
   }




    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_NONE][0]),                "NONE           ");           
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_GMII][0]),                "GMII           ");           
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_RGMII][0]),               "RGMII          ");          
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_MII][0]),                 "MII            ");            
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_SSSMII][0]),              "SSMII          ");         
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_UTOPIA][0]),              "UTOPIA         ");         
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_POS2][0]),                "POS2           ");           
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_SGMII][0]),               "SGMII          ");          
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_AURORA][0]),              "AURORA         ");         
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_INTERNAL_TLS][0]),        "INTERNAL_TLS   ");   
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_EXTERNAL_Radio][0]),      "EXTERNAL       "); 
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_TDM][0]),                 "TDM            ");            
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_XGMII][0]),               "XGMII          ");          
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_sXAUI][0]),               "sXAUI          ");          
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_RMII][0]),                "RMII           ");           
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_XAUI][0]),                "XAUI           ");           
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_Interlaken][0]),          "Interlaken     ");     
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_RXAUI][0]),               "RXAUI          ");          
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_SINGLE10G][0]),           "SFP+10G        ");
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_PCI_e][0]),               "PCI_e          ");          
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_QSGMII][0]),              "QSGMII         ");         
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_XLAUI][0]),               "XLAUI          ");          
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_CAUI][0]),                "CAUI           ");           
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_CAUI_4][0]),              "CAUI_4         "); 
    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_ENHANCED_TLS][0]),        "ENHANCED_TLS   ");

    MEA_OS_strcpy(&(InterfaceType[MEA_INTERFACETYPE_LAST][0]),                "LAST           ");





    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_NONE][0]),                "NONE                ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_E1_T1_T1_193B][0]),       "E1_T1_T1-193B       ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_AAL5_ATM_TC][0]),         "AAL5_ATM_TC         ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_GBE_MAC][0]),             "GBE_MAC             ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_GBE_1588][0]),            "GBE_1588            ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_10G_XMAC_ENET][0]),       "10G_XMAC_ENET       ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_10_XMAC_Xilinx][0]),      "10_XMAC_Xilinx      ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_G999_1MAC][0]),           "G999_1MAC           "); 
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_G999_1MAC_1588][0]),      "G999_1MAC_1588      "); 
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_AURORA_MAC][0]),          "AURORA_MAC          ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_Interlaken_MAC][0]),      "Interlaken_MAC      "); 
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_GENERATOR_ANLAZER][0]),   "GENERATOR_ANLAZER   ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_40GBE][0]),               "40GBE               ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_100GBE][0]),              "100GBE              "); 
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_VIRTUAL][0]),             "VIRTUAL             ");
    MEA_OS_strcpy(&(portType[MEA_PORTTYPE_LAST][0]),                "LAST                ");


    MEA_OS_strcpy(&(Interface_Rate[MEA_INTERFACE_Rate_NONE][0]), "NONE");
    MEA_OS_strcpy(&(Interface_Rate[MEA_INTERFACE_Rate_10Mbs][0]), "10Mb/s");
    MEA_OS_strcpy(&(Interface_Rate[MEA_INTERFACE_Rate_100Mbs][0]), "100Mb/s");
    MEA_OS_strcpy(&(Interface_Rate[MEA_INTERFACE_Rate_1Gbs][0]), "1Gb/s");
    MEA_OS_strcpy(&(Interface_Rate[MEA_INTERFACE_Rate_2_5Gbs][0]), "2.5Gb/s");
    MEA_OS_strcpy(&(Interface_Rate[MEA_INTERFACE_Rate_10Gbs][0]), "10Gb/s");
    MEA_OS_strcpy(&(Interface_Rate[MEA_INTERFACE_Rate_40Gbs][0]), "40Gb/s");
    MEA_OS_strcpy(&(Interface_Rate[MEA_INTERFACE_Rate_100Gbs][0]), "100Gb/s");
    MEA_OS_strcpy(&(Interface_Rate[MEA_INTERFACE_Rate_LAST][0]), "ERROR");



    switch (type)
    {
    case MEA_DRV_PORT_INT_STR_PORT_TYPE:
        if (index >= MEA_PORTTYPE_LAST) {
            MEA_OS_strcpy(name_str, &(portType[MEA_PORTTYPE_LAST][0]));
        }
        MEA_OS_strcpy(name_str, &(portType[index][0]));
        break;
    case MEA_DRV_PORT_INT_STR_INTFACE_TYPE:
        if (index >= MEA_INTERFACETYPE_LAST) {
            MEA_OS_strcpy(name_str, &(InterfaceType[MEA_INTERFACETYPE_LAST][0]));
        }
        MEA_OS_strcpy(name_str, &(InterfaceType[index][0]));
        break;
    case MEA_DRV_PORT_INT_STR_INTFACE_RATE:
        if (index >= MEA_INTERFACE_Rate_LAST) {
            MEA_OS_strcpy(name_str, &(Interface_Rate[MEA_INTERFACE_Rate_LAST][0]));

        }
        MEA_OS_strcpy(name_str, &(Interface_Rate[index][0]));
        break;
    default:
        MEA_OS_strcpy(name_str, "-ERROR-");
        return MEA_ERROR;
        break;
    }

    return MEA_OK;
}





MEA_Status mea_drv_Get_DeviceInfo_GetPort2Interface_Init(MEA_Unit_t unit_i)

{
#if (!defined(MEA_OS_DEVICE_MEMORY_SIMULATION))    
    MEA_ind_read_t          ind_read;
    MEA_drv_port2Interface_dbt entry;
#endif
    MEA_Uint16              port;
    MEA_Uint16              interface_index;
#if (defined(MEA_OS_DEVICE_MEMORY_SIMULATION))
    MEA_Uint32 interface_ID=0;
    MEA_Uint32 type;
#endif    
    
    MEA_Uint32 hw_interface_index;
   
    MEA_OutPorts_Entry_dbt outports;
	MEA_Uint32 size;
    
#if 1
	
	size = (MEA_MAX_PORT_NUMBER_INGRESS+1) * sizeof(MEA_drv_port2Interface_dbt);
	MEA_Ports2InterfaceInfo = (MEA_drv_port2Interface_dbt*)MEA_OS_malloc(size);
	if (MEA_Ports2InterfaceInfo == NULL) {
		MEA_OS_free(MEA_Ports2InterfaceInfo);
		MEA_Ports2InterfaceInfo = NULL;
		
		
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_Ports2InterfaceInfo, 0, size);


	size = (MEA_MAX_PORT_NUMBER_INGRESS+1) * sizeof(MEA_drv_Interface2Port_dbt);
	MEA_Interface2PortsInfo = (MEA_drv_Interface2Port_dbt*)MEA_OS_malloc(size);
	if (MEA_Interface2PortsInfo == NULL) {
		MEA_OS_free(MEA_Interface2PortsInfo);
		MEA_Interface2PortsInfo = NULL;


		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_Interface2PortsInfo, 0, size);
#else
   MEA_OS_memset(&MEA_Ports2InterfaceInfo[0],0,sizeof(MEA_Ports2InterfaceInfo));
   MEA_OS_memset(&MEA_Interface2PortsInfo[0],0,sizeof(MEA_Interface2PortsInfo));
#endif
#if (!defined(MEA_OS_DEVICE_MEMORY_SIMULATION))
    for (port=0;port<= MEA_MAX_PORT_NUMBER_INGRESS;port++)
    {
    MEA_OS_memset(&entry,0,sizeof(entry));

    ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_PORT_INTERFACE_INFO;
    ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
    ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_read.statusReg		= MEA_IF_STATUS_REG;   
    ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
    ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;

    ind_read.read_data      = &(entry.index_regs[0]); 
    ind_read.tableOffset	= port;
    if (MEA_API_ReadIndirect(unit_i,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry.index_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            return MEA_ERROR;
    }

       
            MEA_OS_memcpy(&MEA_Ports2InterfaceInfo[port],&entry,sizeof(MEA_Ports2InterfaceInfo[0])); 
           
       
    }

    /*update index interface */
   /*interface */
    for (interface_index=0;interface_index<=MEA_MAX_PORT_NUMBER;interface_index++)
    {
        if(MEA_Ports2InterfaceInfo[interface_index].val.Interface_type == 0)
            continue;

        if(MEA_Device_HW_interfaceIndex(unit_i,interface_index,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_index].val.Interface_Hw_index =hw_interface_index; 
        }
    
    }


#endif
     
#if (defined(MEA_OS_DEVICE_MEMORY_SIMULATION)) 
    type= (MEA_Uint32) MEA_OS_DEVICE_SIMULATION_DEF;



    switch (type){

    case    MEA_OS_DEVICE_SIMULATION_Z7015_GFAST:
#if 1      
        /*set the interface*/



        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID = 0;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 100;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        
        interface_ID = 127;
        MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
       

        /************************************************************************/
        /*                                                                      */
        /************************************************************************/

        for (port = 0; port <= 7; port++) { /*G999_1MAC to 8 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 0;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
        
        {/*mng g999*/
            port = 31;
            interface_ID = 0;
            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }


        port = 100;
        MEA_Ports2InterfaceInfo[port ].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[port ].val.interface_ID = 100;





        MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;
#endif
        break;

    case MEA_OS_DEVICE_SIMULATION_Z7035_20G_24GFAST:
#if 1      
        /*set the interface*/



        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID = 104;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 105;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 118;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 119;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }



        interface_ID = 127;
        MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_GMII;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        /************************************************************************/
        /*                                                                      */
        /************************************************************************/


        /************************************************************************/
        /*                                                                      */
        /************************************************************************/

        for (port = 0; port <= 23; port++) { /*G999_1MAC to 8 MEA_INTERFACETYPE_RGMII of */
            
            if (port >= 0 && port <= 7)
                interface_ID = 104;
            if (port >= 8 && port <= 15)
                interface_ID = 105;
            if (port >= 16 && port <= 23)
                interface_ID = 106;


            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }

        {/*mng g999*/
            port = 30;
            interface_ID = 104;
            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
        {/*mng g999*/
            port = 31;
            interface_ID = 104;
            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }

        {/*mng g999*/
            port = 62;
            interface_ID = 105;
            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }

        {/*mng g999*/
            port = 63;
            interface_ID = 105;
            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }

        {/*mng g999*/
            port = 94;
            interface_ID = 106;
            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
        {/*mng g999*/
            port = 95;
            interface_ID = 106;
            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }


        port = 118;
        MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_10G_XMAC_ENET;
        MEA_Ports2InterfaceInfo[port].val.interface_ID = 118;
        port = 119;
        MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_10G_XMAC_ENET;
        MEA_Ports2InterfaceInfo[port].val.interface_ID = 119;





        MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;
#endif
        break;


    case MEA_OS_DEVICE_SIMULATION_Z7015_2G_1Radio:
#if 1      
        /*set the interface*/



        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID = 12;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 100;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 101;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 127;
        MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        /************************************************************************/
        /*                                                                      */
        /************************************************************************/


        /************************************************************************/
        /*                                                                      */
        /************************************************************************/

        port = 12;
        MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[port].val.interface_ID = 12;

        port = 100;
        MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[port].val.interface_ID = 100;
        port = 101;
        MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[port].val.interface_ID = 100;






        MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;
#endif
        break;
    case MEA_OS_DEVICE_SIMULATION_Z7045_GW_AW:
#if 1      
        /*set the interface*/



        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID = 100;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 101;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 102;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 103;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }



        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID = 104;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 105;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 106;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 107;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }


        interface_ID = 108;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 109;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 110;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 111;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }


        interface_ID = 118;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 119;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }


        /************************************************************************/
        /*                                                                      */
        /************************************************************************/

        interface_ID = 126;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_INTERNAL_TLS;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 122;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_INTERNAL_TLS;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }


        interface_ID = 127;
        MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_GMII;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }












        for (port = 100; port < 104; port += 4){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
            MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = 100;

            MEA_Ports2InterfaceInfo[port + 1].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[port + 1].val.interface_ID = 101;

            MEA_Ports2InterfaceInfo[port + 2].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[port + 2].val.interface_ID = 102;

            MEA_Ports2InterfaceInfo[port + 3].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[port + 3].val.interface_ID = 103;
        }




        MEA_Ports2InterfaceInfo[104].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[104].val.interface_ID = 104;

        MEA_Ports2InterfaceInfo[105].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[105].val.interface_ID = 105;

        MEA_Ports2InterfaceInfo[118].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[118].val.interface_ID = 118;

        MEA_Ports2InterfaceInfo[119].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[119].val.interface_ID = 119;

        MEA_Ports2InterfaceInfo[126].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[126].val.interface_ID = 126;

        MEA_Ports2InterfaceInfo[126].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[126].val.interface_ID = 126;

        for (port = 24; port <= 27; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 122;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }


        MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;
#endif
        break;

    case MEA_OS_DEVICE_SIMULATION_Z7045_GW:
#if 1      
        /*set the interface*/


     
        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID = 96;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 97;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 99;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 100;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 101;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 102;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 103;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }



        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID = 104;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 105;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 106;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 107;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }


        interface_ID = 108;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 109;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 110;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 111;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }


        interface_ID = 118;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 119;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }


        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
/*       
        interface_ID = 126;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_INTERNAL_TLS;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
*/
        interface_ID = 122;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_ENHANCED_TLS;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }





        interface_ID = 127;
        MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_GMII;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }


       



        for (port = 96; port <= 98; port++) {
            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = port;
        }



        for (port = 100; port < 104; port += 4){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
            MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = 100;

            MEA_Ports2InterfaceInfo[port + 1].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[port + 1].val.interface_ID = 101;

            MEA_Ports2InterfaceInfo[port + 2].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[port + 2].val.interface_ID = 102;

            MEA_Ports2InterfaceInfo[port + 3].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[port + 3].val.interface_ID = 103;
        }

       





        MEA_Ports2InterfaceInfo[104].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[104].val.interface_ID = 104;

        MEA_Ports2InterfaceInfo[105].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[105].val.interface_ID = 105;

        MEA_Ports2InterfaceInfo[106].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[106].val.interface_ID = 106;

        MEA_Ports2InterfaceInfo[107].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[107].val.interface_ID = 107;

        MEA_Ports2InterfaceInfo[108].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[108].val.interface_ID = 108;

        MEA_Ports2InterfaceInfo[109].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[109].val.interface_ID = 109;

        MEA_Ports2InterfaceInfo[110].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[110].val.interface_ID = 110;

        MEA_Ports2InterfaceInfo[111].val.Port_type = MEA_PORTTYPE_GBE_1588;
        MEA_Ports2InterfaceInfo[111].val.interface_ID = 111;









        
        MEA_Ports2InterfaceInfo[118].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[118].val.interface_ID = 118;

        MEA_Ports2InterfaceInfo[119].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[119].val.interface_ID = 119;



//        MEA_Ports2InterfaceInfo[126].val.Port_type = MEA_PORTTYPE_GBE_MAC;
//        MEA_Ports2InterfaceInfo[126].val.interface_ID = 126;

        for (port = 24; port <= 27; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 122;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GBE_MAC; //MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }


        MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;
#endif
        break;
        case    MEA_OS_DEVICE_SIMULATION_Z7045_GW_1KC:
#if 1      
            /*set the interface*/



            /************************************************************************/
            /*                                                                      */
            /************************************************************************/
          

            interface_ID = 100;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 101;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 102;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 103;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }



            /************************************************************************/
            /*                                                                      */
            /************************************************************************/
            interface_ID = 104;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 105;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }

            interface_ID = 106;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 107;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }


            interface_ID = 108;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 109;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }

            interface_ID = 110;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 111;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }


            interface_ID = 118;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 119;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }


            interface_ID = 122;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_ENHANCED_TLS;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }





            interface_ID = 127;
            MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_GMII;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }






         



            for (port = 100; port < 104; port += 4) { /*port to interface  MEA_INTERFACETYPE_QSGMII */
                MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_GBE_1588;
                MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = 100;

                MEA_Ports2InterfaceInfo[port + 1].val.Port_type = MEA_PORTTYPE_GBE_1588;
                MEA_Ports2InterfaceInfo[port + 1].val.interface_ID = 101;

                MEA_Ports2InterfaceInfo[port + 2].val.Port_type = MEA_PORTTYPE_GBE_1588;
                MEA_Ports2InterfaceInfo[port + 2].val.interface_ID = 102;

                MEA_Ports2InterfaceInfo[port + 3].val.Port_type = MEA_PORTTYPE_GBE_1588;
                MEA_Ports2InterfaceInfo[port + 3].val.interface_ID = 103;
            }







            MEA_Ports2InterfaceInfo[104].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[104].val.interface_ID = 104;

            MEA_Ports2InterfaceInfo[105].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[105].val.interface_ID = 105;

            MEA_Ports2InterfaceInfo[106].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[106].val.interface_ID = 106;

            MEA_Ports2InterfaceInfo[107].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[107].val.interface_ID = 107;

            MEA_Ports2InterfaceInfo[108].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[108].val.interface_ID = 108;

            MEA_Ports2InterfaceInfo[109].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[109].val.interface_ID = 109;

            MEA_Ports2InterfaceInfo[110].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[110].val.interface_ID = 110;

            MEA_Ports2InterfaceInfo[111].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[111].val.interface_ID = 111;










            MEA_Ports2InterfaceInfo[118].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[118].val.interface_ID = 118;
            
            for (port = 80; port <= 86; port++) {
                MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GBE_MAC;
                MEA_Ports2InterfaceInfo[port].val.interface_ID = 118;
            }






            MEA_Ports2InterfaceInfo[119].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[119].val.interface_ID = 119;



            //        MEA_Ports2InterfaceInfo[126].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            //        MEA_Ports2InterfaceInfo[126].val.interface_ID = 126;

            for (port = 24; port <= 27; port++) { /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
                interface_ID = 122;

                MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GBE_MAC; //MEA_PORTTYPE_G999_1MAC;
                MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
                MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
            }


            MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;

			for (port = 128; port <= 639; port++) 
			{
				MEA_Ports2InterfaceInfo[port].val.Port_type    = MEA_PORTTYPE_VIRTUAL;
				MEA_Ports2InterfaceInfo[port].val.interface_ID = 122;

			}



#endif
            break;

        case MEA_OS_DEVICE_SIMULATION_Z7015_GW:
#if 1      
            /********************************************************/
            /*set the interface                                    */
           /********************************************************/
            interface_ID = 12;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }

            interface_ID = 100;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 101;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 102;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            



            interface_ID = 122;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_ENHANCED_TLS;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }

            interface_ID = 127;
            MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_GMII;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }


            /*************************************/


            MEA_Ports2InterfaceInfo[100].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[100].val.interface_ID = 100;

            MEA_Ports2InterfaceInfo[101].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[101].val.interface_ID = 101;

            MEA_Ports2InterfaceInfo[102].val.Port_type = MEA_PORTTYPE_GBE_1588;
            MEA_Ports2InterfaceInfo[102].val.interface_ID = 102;

           


            for (port = 24; port <= 27; port++) { /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
                interface_ID = 122;

                MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GBE_MAC; //MEA_PORTTYPE_G999_1MAC;
                MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
                MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
            }


            MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;
#endif

            break;
        case MEA_OS_DEVICE_SIMULATION_NIC_40G :
#if 1      
            /********************************************************/
            /*set the interface                                    */
            /********************************************************/
            interface_ID = 104;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }

            interface_ID = 105;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 106;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            interface_ID = 107;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }

            interface_ID = 122;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_ENHANCED_TLS;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }

            interface_ID = 127;
            MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_XLAUI;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE) {
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }


            /*************************************/


            MEA_Ports2InterfaceInfo[104].val.Port_type = MEA_PORTTYPE_10G_XMAC_ENET;
            MEA_Ports2InterfaceInfo[104].val.interface_ID = 104;

            MEA_Ports2InterfaceInfo[105].val.Port_type = MEA_PORTTYPE_10G_XMAC_ENET;
            MEA_Ports2InterfaceInfo[105].val.interface_ID = 105;

            MEA_Ports2InterfaceInfo[106].val.Port_type = MEA_PORTTYPE_10G_XMAC_ENET;
            MEA_Ports2InterfaceInfo[106].val.interface_ID = 106;
            MEA_Ports2InterfaceInfo[107].val.Port_type = MEA_PORTTYPE_10G_XMAC_ENET;
            MEA_Ports2InterfaceInfo[107].val.interface_ID = 107;




            for (port = 24; port <= 27; port++) { /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
                interface_ID = 122;

                MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GBE_MAC; //MEA_PORTTYPE_G999_1MAC;
                MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
                MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
            }


            MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_40GBE;
            MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;
#endif


            break;

        case MEA_OS_DEVICE_SIMULATION_ASIC1:
#if 1      
            /*set the interface*/
        interface_ID=0;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type   = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW     = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
          MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }

        interface_ID=12;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }
        
        interface_ID=24;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }

        interface_ID=36;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }

        interface_ID=48;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }

        interface_ID=72;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }
        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID=100;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_QSGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }
        interface_ID=101;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }
        interface_ID=102;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }
        interface_ID=103;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }



        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID=104;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }
        interface_ID=105;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
        if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
        }



       /************************************************************************/
       /*                                                                      */
       /************************************************************************/
       interface_ID=108;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_QSGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
       if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
       }
       interface_ID=109;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
       if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
       }
       interface_ID=110;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
       if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
       }
       interface_ID=111;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type  = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW    = 0;
       if(MEA_Device_HW_interfaceIndex(unit_i,interface_ID,&hw_interface_index)==MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index =hw_interface_index; 
       }

       interface_ID = 127;
       MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_RMII;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
           
            
 






    for(port=0;port<24;port+=4){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
        if(port<=19)
         interface_ID= (port/4)*12;
        else
            interface_ID = 72;

    MEA_Ports2InterfaceInfo[port+0].val.Port_type       = MEA_PORTTYPE_G999_1MAC; 
    
    MEA_Ports2InterfaceInfo[port+0].val.Interface_BW    = 0;
    MEA_Ports2InterfaceInfo[port+0].val.interface_ID    =  interface_ID;

    MEA_Ports2InterfaceInfo[port+1].val.Port_type       = MEA_PORTTYPE_G999_1MAC; 
    MEA_Ports2InterfaceInfo[port+1].val.Interface_type  = 0;
    MEA_Ports2InterfaceInfo[port+1].val.Interface_BW    = 0;
    MEA_Ports2InterfaceInfo[port+1].val.interface_ID    = interface_ID;

    MEA_Ports2InterfaceInfo[port+2].val.Port_type       = MEA_PORTTYPE_G999_1MAC; 
    MEA_Ports2InterfaceInfo[port+2].val.Interface_type  = 0;
    MEA_Ports2InterfaceInfo[port+2].val.Interface_BW    = 0;
    MEA_Ports2InterfaceInfo[port+2].val.interface_ID    = interface_ID; 

    MEA_Ports2InterfaceInfo[port+3].val.Port_type       = MEA_PORTTYPE_G999_1MAC; 
    MEA_Ports2InterfaceInfo[port+3].val.Interface_type  = 0;
    MEA_Ports2InterfaceInfo[port+3].val.Interface_BW    = 0;
    MEA_Ports2InterfaceInfo[port+3].val.interface_ID    = interface_ID;; 
    }


    for(port=100;port<104;port+=4){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
        MEA_Ports2InterfaceInfo[port+0].val.Port_type       = MEA_PORTTYPE_GBE_1588; 
        MEA_Ports2InterfaceInfo[port+0].val.interface_ID    =  100;

        MEA_Ports2InterfaceInfo[port+1].val.Port_type       = MEA_PORTTYPE_GBE_1588; 
        MEA_Ports2InterfaceInfo[port+1].val.interface_ID    = 101;

        MEA_Ports2InterfaceInfo[port+2].val.Port_type       = MEA_PORTTYPE_GBE_1588; 
        MEA_Ports2InterfaceInfo[port+2].val.interface_ID    = 102; 

        MEA_Ports2InterfaceInfo[port+3].val.Port_type       = MEA_PORTTYPE_GBE_1588; 
        MEA_Ports2InterfaceInfo[port+3].val.interface_ID    = 103; 
    }




    MEA_Ports2InterfaceInfo[104].val.Port_type       = MEA_PORTTYPE_GBE_1588; 
    MEA_Ports2InterfaceInfo[104].val.interface_ID    = 104;

    MEA_Ports2InterfaceInfo[105].val.Port_type       = MEA_PORTTYPE_GBE_1588; 
    MEA_Ports2InterfaceInfo[105].val.interface_ID    = 105;

    for(port=108;port<112;port+=4){ /* port to MEA_INTERFACETYPE_QSGMII */
        MEA_Ports2InterfaceInfo[port+0].val.Port_type       = MEA_PORTTYPE_GBE_MAC; 
        MEA_Ports2InterfaceInfo[port+0].val.interface_ID    =  108;

        MEA_Ports2InterfaceInfo[port+1].val.Port_type       = MEA_PORTTYPE_GBE_MAC; 
        MEA_Ports2InterfaceInfo[port+1].val.interface_ID    = 109;

        MEA_Ports2InterfaceInfo[port+2].val.Port_type       = MEA_PORTTYPE_GBE_MAC; 
               MEA_Ports2InterfaceInfo[port+2].val.interface_ID    = 110; 

        MEA_Ports2InterfaceInfo[port+3].val.Port_type       = MEA_PORTTYPE_GBE_MAC; 
        MEA_Ports2InterfaceInfo[port+3].val.interface_ID    = 111; 
    }


    

    MEA_Ports2InterfaceInfo[127].val.Port_type       = MEA_PORTTYPE_GBE_MAC; 
    MEA_Ports2InterfaceInfo[127].val.interface_ID    =  127;
#endif

    
    break;
    case MEA_OS_DEVICE_SIMULATION_VDSlK7:
#if 1      

             
             




        /*set the interface*/
        interface_ID = 0;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 12;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 24;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 36;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 48;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 72;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

       
        interface_ID = 96;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 97;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
           
            
                interface_ID = 98;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }
            
            interface_ID = 99;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
            if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
                MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
            }

        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
  
        interface_ID = 100;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 101;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 102;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 103;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }



        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        interface_ID = 104;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 105;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }



        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        
            
           
            
            
        interface_ID = 108;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 109;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 110;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 111;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 122;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_INTERNAL_TLS;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        
        interface_ID = 125;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }
        interface_ID = 126;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        interface_ID = 127;
        MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_RMII;
        MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
        if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
            MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
        }

        for (port = 0; port<=15; port ++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 0;

            MEA_Ports2InterfaceInfo[port ].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port ].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port ].val.interface_ID = interface_ID;


        }
        for (port = 16; port <= 31; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 12;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


        }
        for (port = 32; port <= 47; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 24;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


        }
        for (port = 48; port <= 63; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 36;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


        }
        for (port = 64; port <= 79; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 48;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


        }
        for (port = 80; port <= 95; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 72;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


        }
        for (port = 96; port <= 99; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 72;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


        }
        for (port = 100; port<104; port += 4){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
            MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = 100;

            MEA_Ports2InterfaceInfo[port + 1].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[port + 1].val.interface_ID = 101;
            
            MEA_Ports2InterfaceInfo[port + 2].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[port + 2].val.interface_ID = 102;
            
            MEA_Ports2InterfaceInfo[port + 3].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[port + 3].val.interface_ID = 103;
        }
        for (port = 104; port <= 105; port++){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
            MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = port;
        }
        for (port = 106; port <= 107; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 125;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


        }
        for (port = 108; port <= 111; port++){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
            MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_GBE_MAC;
            MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = port;
        }
        for (port = 112; port <= 113; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 0;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


        }
        for (port = 114; port <= 114; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 12;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


        }
        for (port = 115; port <= 116; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 24;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
        for (port = 117; port <= 117; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 36;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
        for (port = 118; port <= 119; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 48;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
         for (port = 120; port <= 120; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
             interface_ID = 120;
 
             MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_GENERATOR_ANLAZER;
             MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
             MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
         }
        for (port = 121; port <= 121; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 72;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
        for (port = 122; port <= 122; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 122;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
        for (port = 124; port <= 124; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 97;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }

        for (port = 125; port <= 125; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 99;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
        for (port = 126; port <= 126; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
            interface_ID = 126;

            MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
            MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
            MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
        }
            
        MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
        MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;
#endif    
        
        
        break;

   case MEA_OS_DEVICE_SIMULATION_VDSlK7_196:
#if 1      







       /*set the interface*/
       interface_ID = 0;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }

       interface_ID = 12;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }

       interface_ID = 24;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }

       interface_ID = 36;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }

       interface_ID = 48;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }

       interface_ID = 72;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }


       interface_ID = 96;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 97;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }


       interface_ID = 98;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }

       interface_ID = 99;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }

       interface_ID = 100;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_QSGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 101;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 102;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 103;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }



       /************************************************************************/
       /*                                                                      */
       /************************************************************************/
       interface_ID = 104;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 105;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }



       /************************************************************************/
       /*                                                                      */
       /************************************************************************/





       interface_ID = 108;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 109;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 110;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 111;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 122;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_INTERNAL_TLS;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }


       interface_ID = 125;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_RGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }
       interface_ID = 126;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SGMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }

       interface_ID = 127;
       MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_RMII;
       MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
       if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
       }

       /*****************************************************************************************************************/
       interface_ID = 0;
       for (port = 0; port <= 15; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           

           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       /**/
       for (port = 112; port <= 113; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */


           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       
       /**/

       interface_ID = 12;
       for (port = 16; port <= 31; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           

           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       for (port = 114; port <= 114; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */


           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       interface_ID = 24;
       for (port = 32; port <= 47; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           

           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       for (port = 115; port <= 116; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */


           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       interface_ID = 36;
       for (port = 48; port <= 63; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       for (port = 117; port <= 117; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       interface_ID = 48;
       for (port = 64; port <= 79; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }
       for (port = 118; port <= 119; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }

       interface_ID = 72;
       for (port = 80; port <= 95; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }
       for (port = 121; port <= 121; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }

       interface_ID = 96;
       for (port = 128; port <= 143; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       for (port = 96; port <= 97; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }

       interface_ID = 97;
       for (port = 144; port <= 159; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;


       }
       for (port = 124; port <= 124; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }

       interface_ID = 98;
       for (port = 160; port <= 175; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }
       for (port = 98; port <= 99; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }
       interface_ID = 99;
       for (port = 176; port <= 191; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }
       for (port = 125; port <= 125; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }
       interface_ID = 125;
       for (port = 192; port <= 207; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }
       for (port = 106; port <= 107; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }

       interface_ID = 126;
       for (port = 208; port <= 223; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }
       for (port = 126; port <= 126; port++){ /*G999_1MAC to 6 MEA_INTERFACETYPE_RGMII of */
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }

       for (port = 100; port<104; port += 4){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
           MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_GBE_MAC;
           MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = 100;

           MEA_Ports2InterfaceInfo[port + 1].val.Port_type = MEA_PORTTYPE_GBE_MAC;
           MEA_Ports2InterfaceInfo[port + 1].val.interface_ID = 101;

           MEA_Ports2InterfaceInfo[port + 2].val.Port_type = MEA_PORTTYPE_GBE_MAC;
           MEA_Ports2InterfaceInfo[port + 2].val.interface_ID = 102;

           MEA_Ports2InterfaceInfo[port + 3].val.Port_type = MEA_PORTTYPE_GBE_MAC;
           MEA_Ports2InterfaceInfo[port + 3].val.interface_ID = 103;
       }
       for (port = 104; port <= 105; port++){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
           MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_GBE_MAC;
           MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = port;
       }

       for (port = 108; port <= 111; port++){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
           MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_GBE_MAC;
           MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = port;
       }

       interface_ID = 122;
       for (port = 252; port <= 255; port++){
           MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
           MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
           MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
       }


       MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
       MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;

      


#endif    


       break;

       case MEA_OS_DEVICE_SIMULATION_VDSlK7_196_2SFP:

#if 1      


           /*set the interface*/

           /************************************************************************/
           /*                                                                      */
           /************************************************************************/
           interface_ID = 104;
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
           if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
               MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
           }
           interface_ID = 105;
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_SINGLE10G;
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
           if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
               MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
           }



           /************************************************************************/
           /*                                                                      */
           /************************************************************************/


           interface_ID = 122;
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_type = MEA_INTERFACETYPE_INTERNAL_TLS;
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
           if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
               MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
           }



           interface_ID = 127;
           MEA_Ports2InterfaceInfo[127].val.Interface_type = MEA_INTERFACETYPE_RMII;
           MEA_Ports2InterfaceInfo[interface_ID].val.Interface_BW = 0;
           if (MEA_Device_HW_interfaceIndex(unit_i, interface_ID, &hw_interface_index) == MEA_TRUE){
               MEA_Ports2InterfaceInfo[interface_ID].val.Interface_Hw_index = hw_interface_index;
           }

           /*****************************************************************************************************************/


           for (port = 104; port <= 105; port++){ /*port to interface  MEA_INTERFACETYPE_QSGMII */
               MEA_Ports2InterfaceInfo[port + 0].val.Port_type = MEA_PORTTYPE_10G_XMAC_ENET;
               MEA_Ports2InterfaceInfo[port + 0].val.interface_ID = port;
           }
          
           interface_ID = 122;
           for (port = 240; port <= 251; port++){
               MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
               MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
               MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
           }

           interface_ID = 122;
           for (port = 252; port <= 255; port++){
               MEA_Ports2InterfaceInfo[port].val.Port_type = MEA_PORTTYPE_G999_1MAC;
               MEA_Ports2InterfaceInfo[port].val.Interface_BW = 0;
               MEA_Ports2InterfaceInfo[port].val.interface_ID = interface_ID;
           }


           MEA_Ports2InterfaceInfo[127].val.Port_type = MEA_PORTTYPE_GBE_MAC;
           MEA_Ports2InterfaceInfo[127].val.interface_ID = 127;






#endif
    break;
    }

#endif

   /*init the interface to port */
     for(interface_index=0;interface_index<=MEA_MAX_PORT_NUMBER;interface_index++){
         
         if(MEA_Ports2InterfaceInfo[interface_index].val.Interface_type == 0)
           continue;

          MEA_Interface2PortsInfo[interface_index].valid = MEA_TRUE;
          MEA_Interface2PortsInfo[interface_index].InterfacType         = MEA_Ports2InterfaceInfo[interface_index].val.Interface_type;
          MEA_Interface2PortsInfo[interface_index].Interface_BW         = (MEA_Uint8)MEA_Ports2InterfaceInfo[interface_index].val.Interface_BW;
          MEA_Interface2PortsInfo[interface_index].Interface_Hw_index   = (MEA_Uint8)MEA_Ports2InterfaceInfo[interface_index].val.Interface_Hw_index;
          MEA_Interface2PortsInfo[interface_index].Packet_mode          = (MEA_Uint8)MEA_Ports2InterfaceInfo[interface_index].val.Interface_Packet_mode;
          

     }

    for(interface_index=0;interface_index<=MEA_MAX_PORT_NUMBER;interface_index++){
        MEA_OS_memset(&outports,0,sizeof(outports));

        if(MEA_Interface2PortsInfo[interface_index].valid == MEA_TRUE){
            for(port=0;port<=MEA_MAX_PORT_NUMBER;port++){
                if((MEA_Ports2InterfaceInfo[port].val.Port_type != 0) && (MEA_Ports2InterfaceInfo[port].val.interface_ID == interface_index)){
                    MEA_SET_OUTPORT(&outports,port);
                    if(MEA_Ports2InterfaceInfo[port].val.Port_type == MEA_PORTTYPE_G999_1MAC ||
                        MEA_Ports2InterfaceInfo[port].val.Port_type == MEA_PORTTYPE_G999_1MAC_1588 ){
                        MEA_Interface2PortsInfo[interface_index].ShaperEnable_On_interface = MEA_TRUE;
                    }


                }
                

            } //for
            MEA_OS_memcpy(&MEA_Interface2PortsInfo[interface_index].OutPorts,&outports,sizeof(MEA_OutPorts_Entry_dbt));
        } // valid
    }  //for interface




return MEA_OK;

}
 



MEA_Status mea_drv_Get_DeviceInfo_Show_Port_Interface(void){

MEA_Uint32              port,interfaceId;
    char portType_str[50];
    char interfaceType_str[50];
    char Rate_str[20];
    

    for (interfaceId =0; interfaceId <=MEA_MAX_PORT_NUMBER; interfaceId++)
    {
        if(MEA_Interface2PortsInfo[interfaceId].InterfacType == 0){
            continue;
        }
        mea_drv_Get_DeviceInfo_Get_Port_Interface_str(MEA_DRV_PORT_INT_STR_INTFACE_TYPE, (MEA_Uint8)(MEA_Interface2PortsInfo[interfaceId].InterfacType), &interfaceType_str[0]);
        mea_drv_Get_DeviceInfo_Get_Port_Interface_str(MEA_DRV_PORT_INT_STR_INTFACE_RATE, (MEA_Uint8)(MEA_Interface2PortsInfo[interfaceId].Interface_BW), &Rate_str[0]);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"    Interface[%3d] %s BW %12s index[%d]\n", interfaceId,interfaceType_str, Rate_str,MEA_Interface2PortsInfo[interfaceId].Interface_Hw_index  );
    }

    for (port=0;port<=MEA_MAX_PORT_NUMBER_INGRESS;port++)
    {
        if(MEA_Ports2InterfaceInfo[port].val.Port_type == 0  )
           continue;

        mea_drv_Get_DeviceInfo_Get_Port_Interface_str(MEA_DRV_PORT_INT_STR_PORT_TYPE, (MEA_Uint8)MEA_Ports2InterfaceInfo[port].val.Port_type, &portType_str[0]);
        if ((MEA_Ports2InterfaceInfo[port].val.Port_type != MEA_PORTTYPE_G999_1MAC) && 
            (MEA_Ports2InterfaceInfo[port].val.Port_type != MEA_PORTTYPE_G999_1MAC_1588) )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Port[%3d].portType  %s  %8s To Interface_Id %d \n", (port), portType_str, "        " ,(MEA_Ports2InterfaceInfo[port].val.interface_ID));
         } else {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Port[%3d].portType  %s %8s To Interface_Id %d  \n", (port), portType_str, 
                ((MEA_Interface2PortsInfo[MEA_Ports2InterfaceInfo[port].val.interface_ID].Packet_mode == 0) ? "Fragment" : "Packet  " ),
                (MEA_Ports2InterfaceInfo[port].val.interface_ID)  );
         }
    }

return MEA_OK;
}


MEA_Bool MEA_Device_HW_interfaceIndex(MEA_Unit_t unit, MEA_Uint32 interfaceId, MEA_Uint32 *Id_o)
{
    MEA_Uint8 i;
    MEA_Bool retVal=MEA_FALSE;
    
    
    *Id_o=0;

    for (i=0;i<MEA_MAX_INERTNAL_INTERFACE_NUMBER ; i++){
        if(MEA_InterfaceHwInfo[i]== interfaceId ){
          retVal=MEA_TRUE;
          *Id_o=i;
          break;
        }

    }
    

return retVal;

}

MEA_Bool mea_drv_Get_DeviceInfo_rmonVP_enable(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].global.val.rmonVP_enable;
}


/*******************************************************/
/* HPM Device info                                     */
/*******************************************************/
MEA_Bool mea_drv_Get_DeviceInfo_HPM_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_valid;
}
MEA_Bool mea_drv_Get_DeviceInfo_HPM_newSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_new;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_numof_UE_PDN(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  (1<<(MEA_DeviceInfo_Table[hwUnit_i].hpm.val.numof_UE_PDN));
}

MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_numof_Profile(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_profile ;
}
MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_numof_Mask_Profile(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i) 
{
    return  1<<(MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_Mask_profile);
}

MEA_Uint8 mea_drv_Get_DeviceInfo_HPM_numof_Rules(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_numof_rules;
}

MEA_Bool mea_drv_Get_DeviceInfo_HPM_Type_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_Type;
}

MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_numof_Act(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{
    MEA_Uint32 HPM = MEA_HPM_SW_X_MAX;

    if ((MEA_device_environment_info.MEA_DEVICE_DB_HPM_enable == MEA_TRUE)) {
        HPM = MEA_device_environment_info.MEA_DEVICE_DB_HPM;
        if (HPM == 0 || HPM >= MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_numOfAct) {
            return (MEA_Uint32)(1 << MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_numOfAct);
        }
        return (MEA_Uint32)(1 << HPM);
    }

    return  (1<<MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_numOfAct);
}

MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_hpm_BASE(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].hpm.val.hpm_BASE;
}
/************************************************************************/
/*      SFLOW                                                          */
/************************************************************************/
MEA_Bool mea_drv_Get_DeviceInfo_SFLOW_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  MEA_DeviceInfo_Table[hwUnit_i].xpermission.val.sflow_support;

}

MEA_Uint32 mea_drv_Get_DeviceInfo_SFLOW_count(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i)
{

    return  (1<<(MEA_DeviceInfo_Table[hwUnit_i].xpermission.val.sflow_count_lg));

}


MEA_Uint32 mea_drv_get_Ports2InterfaceInfo_Type(MEA_Port_t port, MEA_Uint16 Type) 
{
	if (port > MEA_MAX_PORT_NUMBER_INGRESS) {
		return 0xFFFF;  //Error
	}

	switch (Type)
	{
	
	case MEA_TYPE_Ports2Interf_Interface_Type :
		return MEA_Ports2InterfaceInfo[port].val.Interface_type;
		break;
	case 	MEA_TYPE_Ports2Interf_Interface_BW:	
		return MEA_Ports2InterfaceInfo[port].val.Interface_BW;
		break;

	case MEA_TYPE_Ports2Interf_Interface_Hw_index:	
		return MEA_Ports2InterfaceInfo[port].val.Interface_Hw_index;
		break;

	case MEA_TYPE_Ports2Interf_Interface_Packet_mode:
		return MEA_Ports2InterfaceInfo[port].val.Interface_Packet_mode;
		break;
	case MEA_TYPE_Ports2Interf_Interface_interface_ID:
		return MEA_Ports2InterfaceInfo[port].val.interface_ID;
		break;
	case MEA_TYPE_Ports2Interf_Interface_Port_type :
		return MEA_Ports2InterfaceInfo[port].val.Port_type;
		break;

	default:
		return 0xFFFF;  //Error
		break;

	}
		
	
	
	
	return 0xFFFF;  //Error	

}

MEA_Uint32 mea_drv_get_interfaceType_by_portId(MEA_Unit_t unit, MEA_Port_t port)
{

	if (port > MEA_MAX_PORT_NUMBER) {
		return 0;
	}



	return MEA_Interface2PortsInfo[MEA_Ports2InterfaceInfo[port].val.interface_ID].InterfacType;

}

MEA_Status mea_drv_Get_Interface_ToPortMapp(MEA_Unit_t       unit,
	MEA_Interface_t  Id,
	MEA_OutPorts_Entry_dbt  *OutPorts)
{

	if (OutPorts == NULL)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "%s entry = NULL", __FUNCTION__);
		return MEA_ERROR;
	}


	if (Id > MEA_MAX_PORT_NUMBER)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "%s  interface Id %d is out of range ", __FUNCTION__, Id);
		return MEA_ERROR;
	}

	




	MEA_OS_memcpy(OutPorts, &MEA_Interface2PortsInfo[Id].OutPorts, sizeof(*OutPorts));

	return MEA_OK;
}

MEA_Bool mea_drv_Get_Interface_ShaperIs_On_interface(MEA_Unit_t       unit,
	                                                  MEA_Interface_t  Id)
{

	return MEA_Interface2PortsInfo[Id].ShaperEnable_On_interface;
}
MEA_Uint32 mea_drv_Get_Interface_Hw_index(MEA_Unit_t       unit,
	MEA_Interface_t  Id)
{

	return MEA_Interface2PortsInfo[Id].Interface_Hw_index;
}

MEA_Bool mea_drv_Get_Interface2port_isValid(MEA_Unit_t       unit,
	MEA_Interface_t  Id)
{
	return MEA_Interface2PortsInfo[Id].valid;

}

MEA_Status mea_drv_Get_Interface2PortsInfo(MEA_Unit_t       unit,
	MEA_Interface_t  Id , MEA_drv_Interface2Port_dbt *entry)
{

	MEA_OS_memcpy(entry, &MEA_Interface2PortsInfo[Id], sizeof(MEA_drv_Interface2Port_dbt));
	return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_API_Get_DeviceInfo_Type(MEA_Unit_t  unit_i,
    MEA_Uint32          hwUnit_i,
	MEA_DeviceInfo_Type_t          type,
    MEA_Uint32  *retvalue)
{

    if (retvalue == NULL){
        return MEA_ERROR;
    }

    switch (type)
    {

    case     MEA_DEVICEINFO_NUM_OF_SERVICE:
        *retvalue = (MEA_Uint32)mea_drv_Get_DeviceInfo_NumOfService((unit_i), (hwUnit_i));
        break;
    case    MEA_DEVICEINFO_NUM_OF_ACTION :
        *retvalue = (MEA_Uint32)mea_drv_Get_DeviceInfo_NumOfActions_noACL(unit_i, hwUnit_i);
        break;
    case    MEA_DEVICEINFO_NUM_OF_ACL :
        *retvalue = (MEA_Uint32)mea_drv_Get_DeviceInfo_NumOfACL(unit_i, hwUnit_i);
        break;
    case    MEA_DEVICEINFO_NUM_OF_PM :
        *retvalue = (MEA_Uint32)mea_drv_Get_DeviceInfo_NumOfPmIds(unit_i, hwUnit_i);
        break;
    case    MEA_DEVICEINFO_NUM_OF_TM :
        *retvalue = (MEA_Uint32) mea_drv_Get_DeviceInfo_NumOfPolicers(unit_i, hwUnit_i);
        break;
    case MEA_DEVICEINFO_NUM_OF_ED:
        *retvalue = (MEA_Uint32)mea_drv_Get_DeviceInfo_NumOfEHPs((unit_i), (hwUnit_i));
        break;
    case    MEA_DEVICEINFO_NUM_OF_POLICER_PROF :
        *retvalue = (MEA_Uint32) mea_drv_Get_DeviceInfo_NumOfPolicerProfiles(unit_i, hwUnit_i);
        break;
    case     MEA_DEVICEINFO_NUM_OF_CLUSTERS :
        *retvalue = (MEA_Uint32) mea_drv_Get_DeviceInfo_NumOfClusters(unit_i, hwUnit_i);
        break;
    case MEA_DEVICEINFO_NUM_OF_CLUSTERS_ON_PORT:
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "not support this type %d \n", type);
        break;
    case MEA_DEVICEINFO_LAST:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "not support this type %d \n", type);
        return MEA_ERROR;
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "not support this type %d \n", type);
        return MEA_ERROR;
        break;
    }
     

    return MEA_OK;
}



int MEA_DRV_Get_ROM_INFO() {
    
    return 1;
}

