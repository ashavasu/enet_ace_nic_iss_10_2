/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*****************************************************************************/
/* 	Module Name:		 mea_tft_drv.c		   							     */ 
/*																		     */
/*  Module Description:	 MEA driver										     */
/*																			 */
/*  Date of Creation:	 													 */
/*																			 */
/*  Author Name:			 												 */
/*****************************************************************************/ 

/*-------------------------------- Includes ------------------------------------------*/

#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_drv_common.h"
#include "enet_queue_drv.h"
#include "mea_action_drv.h"
#include "mea_tft_drv.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                        */
/*----------------------------------------------------------------------------*/

typedef struct {
    MEA_Uint32 L4_Source_min    : 16;
    MEA_Uint32 L4_Source_max    : 16;
    MEA_Uint32 L4_Dest_min      : 16;
    MEA_Uint32 L4_Dest_max      : 16;

    MEA_Uint32 Source_IPv4;
    MEA_Uint32 Dest_IPv4;
    
    MEA_Uint32 Ipv6[4];            /*only64bitMSB*/   
    MEA_Uint32 Ipv6_mask        : 6;
    MEA_Uint32 Ipv6_flow_Label  : 20;

    MEA_Uint32 l2_pri           : 8;
    MEA_Uint32 IP_protocol      : 8;
    MEA_Uint32 DSCP_min         : 6;
    MEA_Uint32 DSCP_max         : 6;
    MEA_Uint32 Internal_Etype   : 3;
   
    MEA_Uint32 Dest_IP_mask     : 5;
    MEA_Uint32 Source_IP_mask   : 5;


    MEA_Uint32 Type             : 3;
    MEA_Uint32 Priority_Rule    : 5;
    MEA_Uint32 QoS_Index        : 3;
}mea_tft_lpm_hw_dbt;




typedef struct
{
    MEA_Bool valid;
    MEA_TFT_entry_data_dbt entry_data;

    MEA_TFT_data_dbt *data_Rules_info;
    MEA_Uint32 numOf_owners;
}mea_tft_profile_dbt;

typedef struct {
    MEA_Bool valid;
    MEA_TFT_mask_data_dbt data;
    MEA_Uint32 numOf_owners;
    


}mea_tft_prof_mask_dbt;

typedef struct {
    MEA_TFT_L3_EtheType_dbt data;

}mea_TFT_L3_Internal_EtherType_dbt;


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

mea_tft_profile_dbt  *mea_TFT_prof_TBL = NULL;


mea_tft_prof_mask_dbt  *mea_TFT_mask_prof_TBL = NULL;



mea_TFT_L3_Internal_EtherType_dbt mea_TFT_L3_Internal_EtherType_prof_TBL[MAT_MAX_OF_L3_Internal_EtherType];



/*-------------------------------- External Variables --------------------------------*/


/*-------------------------------- Local Variables -----------------------------------*/


/*-------------------------------- Global Variables ----------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
static MEA_Status mea_drv_TFT_Prof_Rinit(MEA_Unit_t unit_i);
static MEA_Status mea_drv_TFT_Prof_Mask_Rinit(MEA_Unit_t unit);


static MEA_Status mea_drv_TFT_Check_LPM_Paramters(MEA_Unit_t unit, MEA_TFT_t tft_profile_Id, MEA_Uint8 index, MEA_TFT_data_dbt *entry)
{

//     if (entry->TFT_info.Type != 0 &&
//         (mea_TFT_prof_TBL[tft_profile_Id].data_Rules_info[index].TFT_info.Type != 0) &&
//         (entry->TFT_info.Type != mea_TFT_prof_TBL[tft_profile_Id].data_Rules_info[index].TFT_info.Type)) {
//         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Delete the entry before changing type \n", __FUNCTION__);
//         return MEA_ERROR;
//     }

    if (entry->TFT_info.Type == MEA_TFT_TYPE_IPV4) {
        if (entry->TFT_info.Ipv6_valid == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error can't be Set Type to IPV4 with Dest_Ipv6_valid  \n", __FUNCTION__);
            return MEA_ERROR;
        }

    }

    if (entry->TFT_info.Type == MEA_TFT_TYPE_IPV6) {
        if (entry->TFT_info.Dest_IPv4_valid == MEA_TRUE || entry->TFT_info.Source_IPv4_valid == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error can't be Set Type to IPV6 with Dest_IPv4_valid or Source_IPv4_valid  \n", __FUNCTION__);
            return MEA_ERROR;
        }

    }

    if (mea_TFT_prof_TBL[tft_profile_Id].entry_data.en_rule_type == MEA_TRUE) {
         switch (mea_TFT_prof_TBL[tft_profile_Id].entry_data.rule_type)
         {
         case 0:
             if (index != 0 && index != 8 && index != 16 && index != 24)
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "The valid rule is 0,8,16,24\n");
             break;
         case 1:
             if (index != 0 && index != 1 && 
                 index != 8 && index != 9 && 
                 index != 16 && index != 17 &&
                 index != 24 && index != 25  )
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "The valid rule is 0,1,8,9,16,17,24,25\n");
             break;
         case 2:
             if (index != 0 && index != 1   && index != 2 &&
                 index != 8 && index != 9   && index != 10 &&
                 index != 16 && index != 17 && index != 18 &&
                 index != 24 && index != 25 && index != 26  )
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "The valid rule is 0:2,8:10 16:18,24:26\n");
             break;

         case 3:
             if (index != 0 && index != 1 && index != 2    && index != 3 &&
                 index != 8 && index != 9 && index != 10   && index != 11 &&
                 index != 16 && index != 17 && index != 18 && index != 19 &&
                 index != 24 && index != 25 && index != 26 && index != 27  )
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "The valid rule is 0:3,8:11 16:19,24:27\n");
             break;
         case 4:
             if (index != 0 && index != 1 && index != 2 && index != 3 && index != 4 &&
                 index != 8 && index != 9 && index != 10 && index != 11 && index != 12 &&
                 index != 16 && index != 17 && index != 18 && index != 19 && index != 20 &&
                 index != 24 && index != 25 && index != 26 && index != 27 && index != 28 )
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "The valid rule is 0:4,8:12 16:20,24:28\n");
             break;
         case 5:
             if (index != 0 && index != 1 && index != 2 && index != 3 && index != 4 && index != 5 &&
                 index != 8 && index != 9 && index != 10 && index != 11 && index != 12 && index != 13 &&
                 index != 16 && index != 17 && index != 18 && index != 19 && index != 20 && index != 21 &&
                 index != 24 && index != 25 && index != 26 && index != 27 && index != 28 && index != 29 )
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "The valid rule is 0:5,8:13 16:21,24:29\n");
             break;
         case 6:
             if (index != 0 && index != 1 && index != 2 && index != 3 && index != 4 && index != 5 && index != 6 &&
                 index != 8 && index != 9 && index != 10 && index != 11 && index != 12 && index != 13 && index != 14 &&
                 index != 16 && index != 17 && index != 18 && index != 19 && index != 20 && index != 21 && index != 22 &&
                 index != 24 && index != 25 && index != 26 && index != 27 && index != 28 && index != 29 && index != 30)
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "The valid rule is 0:6,8:14 16:22,24:30\n");
             break;



         }

    }


    return MEA_OK;

}


void mea_drv_TFT_prof_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32             *data = (MEA_Uint32*)arg4;

   // MEA_Uint32                 val[5];
    MEA_Uint32                 i = 0;

    /* Update Hw Entry */
    for (i = 0; i<len; i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            data[i],
            MEA_MODULE_IF);
    }

}
#define MEA_TFT_LPM_HW_LEN 6
static MEA_Status mea_drv_TFT_SetEntry(MEA_Unit_t unit, MEA_TFT_t tft_Id, MEA_Uint8 Index, MEA_TFT_data_dbt *entry)
{
    // MEA_db_HwUnit_t       hwUnit;

    MEA_ind_write_t      ind_write;
    MEA_Bool             support_hw = MEA_TRUE;

    MEA_Uint32                count_shift;
    MEA_Uint32                  Val[MEA_TFT_LPM_HW_LEN];
    mea_tft_lpm_hw_dbt        hw_entry;

    MEA_OS_memset(&Val[0], 0, sizeof(Val));
    MEA_OS_memset(&hw_entry, 0, sizeof(hw_entry));

    hw_entry.Type = entry->TFT_info.Type;

    if (entry->TFT_info.Type != MEA_TFT_TYPE_DISABLE) {
        hw_entry.Internal_Etype = entry->TFT_info.L3_EtherType;

        hw_entry.l2_pri = 0xff;

        hw_entry.DSCP_min = 0;
        hw_entry.DSCP_max = 0x3f;

        hw_entry.IP_protocol = 0xff;

        hw_entry.L4_Source_min = 0;
        hw_entry.L4_Source_max = 0xffff;
        hw_entry.L4_Dest_min = 0;
        hw_entry.L4_Dest_max = 0xffff;


        hw_entry.Priority_Rule = entry->TFT_info.Priority_Rule;
        hw_entry.QoS_Index = entry->TFT_info.QoS_Index;
    }

    if (entry->TFT_info.Type == MEA_TFT_TYPE_NO_IP)
    {
        hw_entry.Type = entry->TFT_info.Type;

        if (entry->TFT_info.L2_enable == MEA_TRUE) {
            hw_entry.l2_pri = entry->TFT_info.L2_Pri;
        }


    }
    if (entry->TFT_info.Type == MEA_TFT_TYPE_IPV4)
    {


        if (entry->TFT_info.L2_enable == MEA_TRUE) {
            hw_entry.l2_pri = entry->TFT_info.L2_Pri;
        }

        if (entry->TFT_info.IP_protocol_valid == MEA_TRUE)
        {
            hw_entry.IP_protocol = entry->TFT_info.IP_protocol;
        }

        if (entry->TFT_info.L3_DSCP_valid == MEA_TRUE) {
            hw_entry.DSCP_min = entry->TFT_info.L3_from_DSCP;
            hw_entry.DSCP_max = entry->TFT_info.L3_end_DSCP;
        }






        if (entry->TFT_info.Dest_IPv4_valid == MEA_TRUE) {
            hw_entry.Dest_IPv4 = entry->TFT_info.Dest_IPv4;
            hw_entry.Dest_IP_mask = entry->TFT_info.Dest_IPv4_mask;
        }
        else {
            hw_entry.Dest_IPv4 = 0;
            hw_entry.Dest_IP_mask = 0;
        }


        if (entry->TFT_info.Source_IPv4_valid == MEA_TRUE) {
            hw_entry.Source_IPv4 = entry->TFT_info.Source_IPv4;
            hw_entry.Source_IP_mask = entry->TFT_info.Source_IPv4_mask;
        }
        else {
            hw_entry.Source_IPv4 = 0;
            hw_entry.Source_IP_mask = 0;
        }



        if (entry->TFT_info.L4_Source_port_valid == MEA_TRUE) {
            hw_entry.L4_Source_min = entry->TFT_info.L4_from_Source_port;
            hw_entry.L4_Source_max = entry->TFT_info.L4_end_Source_port;
        }



        if (entry->TFT_info.L4_dest_valid == MEA_TRUE) {
            hw_entry.L4_Dest_min = entry->TFT_info.L4_from_Dest_port;
            hw_entry.L4_Dest_max = entry->TFT_info.L4_end_Dest_port;
        }



    }//end MEA_TFT_TYPE_IPV4

    if (!MEA_HPM_NEW_SUPPORT)
    {
        if (entry->TFT_info.Type == MEA_TFT_TYPE_IPV6) /**/
        {
            hw_entry.Internal_Etype = entry->TFT_info.L3_EtherType;

            hw_entry.L4_Source_min = 0;
            hw_entry.L4_Source_max = 0;
            hw_entry.L4_Dest_min = 0;
            hw_entry.L4_Dest_max = 0;
            hw_entry.Source_IP_mask = 0;
            hw_entry.Dest_IP_mask = 0;

            hw_entry.Dest_IP_mask =    entry->TFT_info.Ipv6_mask & 0x1f;
            hw_entry.Source_IP_mask = (entry->TFT_info.Ipv6_mask >> 5) & 0x7;

            hw_entry.L4_Source_min = entry->TFT_info.Ipv6[0] & 0xffff;
            hw_entry.L4_Source_max = (entry->TFT_info.Ipv6[0] >> 16) & 0xffff;
            hw_entry.L4_Dest_min = entry->TFT_info.Ipv6[1] & 0xffff;
            hw_entry.L4_Dest_max = (entry->TFT_info.Ipv6[1] >> 16) & 0xffff;
            hw_entry.Source_IPv4 = entry->TFT_info.Ipv6[2];
            hw_entry.Dest_IPv4 = entry->TFT_info.Ipv6[3];

            if (entry->TFT_info.L2_enable == MEA_TRUE) {
                hw_entry.l2_pri = entry->TFT_info.L2_Pri;
            }

            if (entry->TFT_info.IP_protocol_valid == MEA_TRUE)
            {
                hw_entry.IP_protocol = entry->TFT_info.IP_protocol;
            }

            if (entry->TFT_info.L3_DSCP_valid == MEA_TRUE) {
                hw_entry.DSCP_min = entry->TFT_info.L3_from_DSCP;
                hw_entry.DSCP_max = entry->TFT_info.L3_end_DSCP;
            }
        }//MEA_TFT_TYPE_IPV6
    }




    if (MEA_HPM_NEW_SUPPORT) 
    {
        hw_entry.Type = entry->TFT_info.Type;
        if ((entry->TFT_info.Type == MEA_TFT_TYPE_IPV6) || 
            (entry->TFT_info.Type == MEA_TFT_TYPE_IPV6_Source)) /**/
        {
            if (entry->TFT_info.L4_Source_port_valid == MEA_TRUE) {
                hw_entry.L4_Source_min = entry->TFT_info.L4_from_Source_port;
                hw_entry.L4_Source_max = entry->TFT_info.L4_end_Source_port;
            }



            if (entry->TFT_info.L4_dest_valid == MEA_TRUE) {
                hw_entry.L4_Dest_min = entry->TFT_info.L4_from_Dest_port;
                hw_entry.L4_Dest_max = entry->TFT_info.L4_end_Dest_port;
            }

            hw_entry.Ipv6[2] = entry->TFT_info.Ipv6[2];
            hw_entry.Ipv6[3] = entry->TFT_info.Ipv6[3];

            hw_entry.Ipv6_flow_Label = entry->TFT_info.Flow_Label;
            hw_entry.Ipv6_mask = entry->TFT_info.Ipv6_mask;



            if (entry->TFT_info.IP_protocol_valid == MEA_TRUE)
            {
                hw_entry.IP_protocol = entry->TFT_info.IP_protocol;
            }

            if (entry->TFT_info.L3_DSCP_valid == MEA_TRUE) {
                hw_entry.DSCP_min = entry->TFT_info.L3_from_DSCP;
                hw_entry.DSCP_max = entry->TFT_info.L3_end_DSCP;
            }
        }
    
    
    }
    

        

    

    count_shift = 0;

    if (!MEA_HPM_NEW_SUPPORT) {


       

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_L4_Source_min, hw_entry.L4_Source_min, &Val[0]);

        count_shift += MEA_LPM_REG_L4_Source_min;
        MEA_OS_insert_value(count_shift, MEA_LPM_REG_L4_Source_max, hw_entry.L4_Source_max, &Val[0]);
        count_shift += MEA_LPM_REG_L4_Source_max;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_L4_Dest_min, hw_entry.L4_Dest_min, &Val[0]);
        count_shift += MEA_LPM_REG_L4_Dest_min;
        MEA_OS_insert_value(count_shift, MEA_LPM_REG_L4_Dest_max, hw_entry.L4_Dest_max, &Val[0]);
        count_shift += MEA_LPM_REG_L4_Dest_max;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_Source_IPv4, hw_entry.Source_IPv4, &Val[0]);
        count_shift += MEA_LPM_REG_Source_IPv4;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_Dest_IPv4, hw_entry.Dest_IPv4, &Val[0]);
        count_shift += MEA_LPM_REG_Dest_IPv4;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_Dest_pri_p, hw_entry.l2_pri, &Val[0]);
        count_shift += MEA_LPM_REG_Dest_pri_p;


        MEA_OS_insert_value(count_shift, MEA_LPM_REG_IP_protocol, hw_entry.IP_protocol, &Val[0]);
        count_shift += MEA_LPM_REG_IP_protocol;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_DSCP_min, hw_entry.DSCP_min, &Val[0]);
        count_shift += MEA_LPM_REG_DSCP_min;
        MEA_OS_insert_value(count_shift, MEA_LPM_REG_DSCP_max, hw_entry.DSCP_max, &Val[0]);
        count_shift += MEA_LPM_REG_DSCP_max;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_Internal_Etype, hw_entry.Internal_Etype, &Val[0]);
        count_shift += MEA_LPM_REG_Internal_Etype;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_Type, hw_entry.Type, &Val[0]);
        count_shift += MEA_LPM_REG_Type;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_Dest_IP_mask, hw_entry.Dest_IP_mask, &Val[0]);
        count_shift += MEA_LPM_REG_Dest_IP_mask;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_Source_IP_mask, hw_entry.Source_IP_mask, &Val[0]);
        count_shift += MEA_LPM_REG_Source_IP_mask;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_Priority_Rule, hw_entry.Priority_Rule, &Val[0]);
        count_shift += MEA_LPM_REG_Priority_Rule;

        MEA_OS_insert_value(count_shift, MEA_LPM_REG_QoS_Index, hw_entry.QoS_Index, &Val[0]);
        count_shift += MEA_LPM_REG_QoS_Index;

    }
    
    if (MEA_HPM_NEW_SUPPORT) 
    {

      if ((entry->TFT_info.Type == MEA_TFT_TYPE_DISABLE) ||
          (entry->TFT_info.Type == MEA_TFT_TYPE_NO_IP)   ||
          (entry->TFT_info.Type == MEA_TFT_TYPE_IPV4)
          ) {

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_L4_Source_min, hw_entry.L4_Source_min, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_L4_Source_min;

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_L4_Source_max, hw_entry.L4_Source_max, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_L4_Source_max;

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_L4_Dest_min, hw_entry.L4_Dest_min, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_L4_Dest_min;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_L4_Dest_max, hw_entry.L4_Dest_max, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_L4_Dest_max;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_Source_IPv4, hw_entry.Source_IPv4, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_Source_IPv4;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_Dest_IPv4, hw_entry.Dest_IPv4, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_Dest_IPv4;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_IP_protocol, hw_entry.IP_protocol, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_IP_protocol;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_DSCP_min, hw_entry.DSCP_min, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_DSCP_min;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_DSCP_max, hw_entry.DSCP_max, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_DSCP_max;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_Dest_pri_p, hw_entry.l2_pri, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_Dest_pri_p;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_Internal_Etype, hw_entry.Internal_Etype, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_Internal_Etype;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_Dest_IP_mask, hw_entry.Dest_IP_mask, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_Dest_IP_mask;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_Source_IP_mask, hw_entry.Source_IP_mask, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_Source_IP_mask;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_PAD, 0, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_PAD;

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_Type, hw_entry.Type, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_Type;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_Priority_Rule, hw_entry.Priority_Rule, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_Priority_Rule;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_QoS_Index, hw_entry.QoS_Index, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_QoS_Index;








      }
      if ((entry->TFT_info.Type == MEA_TFT_TYPE_IPV6) ||
          (entry->TFT_info.Type == MEA_TFT_TYPE_IPV6_Source)
          ) {

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_L4_Source_min, hw_entry.L4_Source_min, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_L4_Source_min;

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_L4_Source_max, hw_entry.L4_Source_max, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_L4_Source_max;

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_L4_Dest_min, hw_entry.L4_Dest_min, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_L4_Dest_min;

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_L4_Dest_max, hw_entry.L4_Dest_max, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_L4_Dest_max;

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_IPV6_L, hw_entry.Ipv6[2], &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_IPV6_L;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_IPV6_H, hw_entry.Ipv6[3], &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_IPV6_H;

          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_IP_protocol, hw_entry.IP_protocol, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_IP_protocol;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_DSCP_min, hw_entry.DSCP_min, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_DSCP_min;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_DSCP_max, hw_entry.DSCP_max, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_DSCP_max;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_Flow_Label, hw_entry.Ipv6_flow_Label, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_Flow_Label;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_IPv6_mask, hw_entry.Ipv6_mask, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_IPv6_mask;
          
              
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_Type, hw_entry.Type, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_Type;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_Priority_Rule, hw_entry.Priority_Rule, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_Priority_Rule;
          MEA_OS_insert_value(count_shift, MEA_LPM_REG_NEW_KEY_IPV6_QoS_Index, hw_entry.QoS_Index, &Val[0]);
          count_shift += MEA_LPM_REG_NEW_KEY_IPV6_QoS_Index;
  
         }




    }



    /* Prepare ind_write structure */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TFT_LPM_PROF;
    ind_write.tableOffset = (tft_Id * MEA_TFT_L3L4_MAX_Rule) + Index;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_TFT_prof_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
    ind_write.funcParam3    = (MEA_funcParam)MEA_TFT_LPM_HW_LEN;
    ind_write.funcParam4 = (MEA_funcParam)&Val;


    if (support_hw){

        if (ENET_WriteIndirect(unit, &ind_write, ENET_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_IF);
return ENET_ERROR;
        }
    }

    return MEA_OK;
}



static MEA_Status mea_drv_TFT_Prof_Rinit(MEA_Unit_t unit_i)
{
    MEA_TFT_t tft_Id;
    MEA_Uint8 Index;

    for (tft_Id = 0; tft_Id < MEA_TFT_L3L4_MAX_PROFILE; tft_Id++)
    {
        if (!mea_TFT_prof_TBL[tft_Id].valid)
            continue;
        for (Index = 0; Index < MEA_TFT_L3L4_MAX_Rule; Index++) {
            if (mea_TFT_prof_TBL[tft_Id].data_Rules_info[Index].TFT_info.Type == MEA_TFT_TYPE_DISABLE)
                continue;

            mea_drv_TFT_SetEntry(unit_i, tft_Id, Index, &mea_TFT_prof_TBL[tft_Id].data_Rules_info[Index]);
        }
    }

    return MEA_OK;
}





/************************************************************************/
/* TFT profile                                                          */
/************************************************************************/

static MEA_Bool mea_drv_TFT_prof_find_free(MEA_Unit_t unit, MEA_TFT_t  *id_io)
{

    MEA_Uint16 i;


    for (i = 0; i < MEA_TFT_L3L4_MAX_PROFILE; i++)
    {
        if (mea_TFT_prof_TBL[i].valid == MEA_FALSE) {
            *id_io = i;
            return MEA_TRUE;
        }
    }

    return MEA_FALSE;


}

static MEA_Bool mea_drv_TFT_prof_IsRange(MEA_Unit_t unit, MEA_TFT_t tft_Id, MEA_Bool silent)
{

    if ((tft_Id) >= MEA_TFT_L3L4_MAX_PROFILE) {
        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - out range \n", __FUNCTION__);
        }
        return MEA_FALSE;
    }
    else {
        return MEA_TRUE;
    }

    return MEA_FALSE;

}

static MEA_Status mea_drv_TFT_prof_IsExist(MEA_Unit_t unit, MEA_TFT_t tft_Id, MEA_Bool silent, MEA_Bool *exist)
{
    if (exist == NULL) {
        if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
    *exist = MEA_FALSE;

    if (mea_drv_TFT_prof_IsRange(unit, tft_Id, silent) != MEA_TRUE) {
        if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  failed  for id=%d\n", __FUNCTION__, tft_Id);
        return MEA_ERROR;
    }

    if (mea_TFT_prof_TBL[tft_Id].valid == MEA_TRUE) {
        *exist = MEA_TRUE;
        return MEA_OK;
    }

    return MEA_OK;

}

MEA_Status mea_drv_TFT_prof_AddOwner(MEA_Unit_t unit, MEA_TFT_t tft_Id)
{
    
    MEA_Bool exist = MEA_FALSE;
    if(mea_drv_TFT_prof_IsExist(unit, tft_Id, MEA_TRUE, &exist) != MEA_OK){
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "The HPM profile %d not exist \n");
        return MEA_ERROR;
    }

    mea_TFT_prof_TBL[tft_Id].numOf_owners++;

    return MEA_OK;
}

MEA_Status mea_drv_TFT_prof_DeleteOwner(MEA_Unit_t unit, MEA_TFT_t tft_Id)
{
    
    MEA_Bool exist = MEA_FALSE;
    if (mea_drv_TFT_prof_IsExist(unit, tft_Id, MEA_TRUE, &exist) != MEA_OK) {
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "The HPM profile %d not exist \n", tft_Id);
        return MEA_ERROR;
    }
    if(mea_TFT_prof_TBL[tft_Id].numOf_owners - 1 > 1)
       mea_TFT_prof_TBL[tft_Id].numOf_owners--;
    else {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "can't delete HPM profile %d  used the \n",tft_Id);
        return MEA_ERROR;
    }
       
    return MEA_OK;

}



void mea_drv_TFT_L3_Internal_EtherType_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32             *data = (MEA_Uint32*)arg4;

    // MEA_Uint32                 val[5];
    MEA_Uint32                 i = 0;

    /* Update Hw Entry */
    for (i = 0; i < len; i++)
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            data[i],
            MEA_MODULE_IF);
    }

}

MEA_Status mea_drv_tft_Set_L3_Internal_EtherType_prof(MEA_Unit_t unit)
{
    MEA_ind_write_t      ind_write;
    MEA_Bool             support_hw = MEA_TRUE;

    MEA_Uint32                count_shift;
    MEA_Uint32                  Val[3];


    MEA_OS_memset(&Val[0], 0, sizeof(Val));

    /*write to HW */



    count_shift = 0;

    MEA_OS_insert_value(count_shift, 16, mea_TFT_L3_Internal_EtherType_prof_TBL[4].data.EtherType, &Val[0]);
    count_shift += 16;
    MEA_OS_insert_value(count_shift, 16, mea_TFT_L3_Internal_EtherType_prof_TBL[5].data.EtherType, &Val[0]);
    count_shift += 16;
    MEA_OS_insert_value(count_shift, 16, mea_TFT_L3_Internal_EtherType_prof_TBL[6].data.EtherType, &Val[0]);
    count_shift += 16;
    MEA_OS_insert_value(count_shift, 16, mea_TFT_L3_Internal_EtherType_prof_TBL[7].data.EtherType, &Val[0]);
    count_shift += 16;
    MEA_OS_insert_value(count_shift, 1, mea_TFT_L3_Internal_EtherType_prof_TBL[4].data.valid, &Val[0]);
    count_shift += 1;
    MEA_OS_insert_value(count_shift, 1, mea_TFT_L3_Internal_EtherType_prof_TBL[5].data.valid, &Val[0]);
    count_shift += 1;
    MEA_OS_insert_value(count_shift, 1, mea_TFT_L3_Internal_EtherType_prof_TBL[6].data.valid, &Val[0]);
    count_shift += 1;
    MEA_OS_insert_value(count_shift, 1, mea_TFT_L3_Internal_EtherType_prof_TBL[7].data.valid, &Val[0]);
    count_shift += 1;




    /* Prepare ind_write structure */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_write.tableOffset = MEA_GLOBAl_TBL_L3_Internal_ETHERTYPE;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_TFT_L3_Internal_EtherType_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
    ind_write.funcParam3 = (MEA_funcParam)3;
    ind_write.funcParam4 = (MEA_funcParam)&Val;


    if (support_hw) {

        if (ENET_WriteIndirect(unit, &ind_write, ENET_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_IF);
            return ENET_ERROR;
        }
    }



    return MEA_OK;
}

/*----------------------------------------------------------------------------*/
/*             APIs functions implementation                                  */
/*----------------------------------------------------------------------------*/


MEA_Status MEA_API_TFT_L3_Internal_EtherType_Set(MEA_Unit_t unit, MEA_Uint8 index, MEA_TFT_L3_EtheType_dbt *entry)
{

    MEA_TFT_L3_EtheType_dbt save_entry;

    if (!MEA_HPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_GW_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (index <= 3) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - the index %d can't be change use index > 3 \n", __FUNCTION__, index);
        return MEA_ERROR;
    }

    if (index >= MAT_MAX_OF_L3_Internal_EtherType) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - the index %d >= is out of range \n", __FUNCTION__, index, MAT_MAX_OF_L3_Internal_EtherType);
        return MEA_ERROR;
    }

    if (MEA_OS_memcmp(&mea_TFT_L3_Internal_EtherType_prof_TBL[index].data, entry, sizeof(MEA_TFT_L3_EtheType_dbt)) != 0) {
        MEA_OS_memcpy(&save_entry, &mea_TFT_L3_Internal_EtherType_prof_TBL[index].data, sizeof(MEA_TFT_L3_EtheType_dbt));
        MEA_OS_memcpy(&mea_TFT_L3_Internal_EtherType_prof_TBL[index].data, entry, sizeof(MEA_TFT_L3_EtheType_dbt));
        if(mea_drv_tft_Set_L3_Internal_EtherType_prof(unit) != MEA_OK)
        {

            MEA_OS_memcpy( &mea_TFT_L3_Internal_EtherType_prof_TBL[index].data, &save_entry,sizeof(MEA_TFT_L3_EtheType_dbt));
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_tft_Set_L3_Internal_EtherType_prof index %d  failed to set \n", __FUNCTION__, index, MAT_MAX_OF_L3_Internal_EtherType);
            return MEA_ERROR;
        }
   }


    
    return MEA_OK;
}

MEA_Status MEA_API_TFT_L3_Internal_EtherType_Get(MEA_Unit_t unit, MEA_Uint8 index, MEA_TFT_L3_EtheType_dbt *entry)
{

    if (!MEA_HPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_GW_SUPPORT not support \n",__FUNCTION__);
        return MEA_ERROR;
    }
   

    if (index >= MAT_MAX_OF_L3_Internal_EtherType) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - the index %d >= is out of range \n", __FUNCTION__, index, MAT_MAX_OF_L3_Internal_EtherType);
        return MEA_ERROR;
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }


    MEA_OS_memcpy(entry,&mea_TFT_L3_Internal_EtherType_prof_TBL[index], sizeof(MEA_TFT_L3_EtheType_dbt));
    return MEA_OK;
}



/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_API_TFT_Create_QoS_Profile(MEA_Unit_t unit, MEA_TFT_t  *tft_profile_Id, MEA_TFT_entry_data_dbt *entry)
{
    MEA_Bool exist;
    MEA_Bool silent = MEA_FALSE;

    if (!MEA_HPM_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_TFT_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if ((*tft_profile_Id) != MEA_PLAT_GENERATE_NEW_ID){

        if (mea_drv_TFT_prof_IsExist(unit, *tft_profile_Id, silent, &exist) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_TFT_prof_IsExist failed (*id_io=%d\n",
                __FUNCTION__, *tft_profile_Id);
            return MEA_ERROR;
        }
        if (exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - *Id_io %d is already exist\n", __FUNCTION__, *tft_profile_Id);
            return MEA_ERROR;
        }


    }
    else{
        /*create profile */
        if (mea_drv_TFT_prof_find_free(unit, tft_profile_Id) == MEA_FALSE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_TFT_prof_find_free failed\n", __FUNCTION__);
            return MEA_ERROR;

        }
    }

    if (entry->en_rule_type == MEA_TRUE) {

    }
    else {
        entry->rule_type = 7;
    }

        
        mea_TFT_prof_TBL[*tft_profile_Id].valid = MEA_TRUE;
        mea_TFT_prof_TBL[*tft_profile_Id].numOf_owners = 1;
        MEA_OS_memcpy(&mea_TFT_prof_TBL[*tft_profile_Id].entry_data, entry, sizeof(MEA_TFT_entry_data_dbt));

    return MEA_OK;
}

MEA_Status MEA_API_TFT_Delete_QoS_Profile(MEA_Unit_t unit, MEA_TFT_t  tft_profile_Id)
{
    MEA_Bool exist;
    MEA_Uint8 priority;
    MEA_Bool silent = MEA_FALSE;

    if (!MEA_HPM_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HPM_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_TFT_prof_IsExist(unit, tft_profile_Id, silent, &exist) != MEA_OK){
        return MEA_ERROR;
    }

    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - The tft %d profile not exist \n",
            __FUNCTION__, tft_profile_Id);
        return MEA_ERROR;
    }




if (mea_TFT_prof_TBL[tft_profile_Id].numOf_owners > 1) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - TFT Profile %d can't be delete numOf owner %d \n", __FUNCTION__,
        tft_profile_Id,
        mea_TFT_prof_TBL[tft_profile_Id].numOf_owners);
    return MEA_ERROR;
}
else {

    for (priority = 0; priority < MEA_TFT_L3L4_MAX_Rule; priority++) {
        if (mea_TFT_prof_TBL[tft_profile_Id].data_Rules_info[priority].TFT_info.Type != 0) {
            mea_TFT_prof_TBL[tft_profile_Id].data_Rules_info[priority].TFT_info.Type = 0;
            if (mea_drv_TFT_SetEntry(unit, tft_profile_Id, priority, &mea_TFT_prof_TBL[tft_profile_Id].data_Rules_info[priority]) != MEA_OK) {

            }
            MEA_OS_memset(&mea_TFT_prof_TBL[tft_profile_Id].data_Rules_info[priority].TFT_info, 0, sizeof(MEA_TFT_key_t));
            
            
            
        }
    }

    MEA_OS_memset(&mea_TFT_prof_TBL[tft_profile_Id].entry_data, 0, sizeof(mea_TFT_prof_TBL[tft_profile_Id].entry_data));
    mea_TFT_prof_TBL[tft_profile_Id].valid = MEA_FALSE;
    mea_TFT_prof_TBL[tft_profile_Id].numOf_owners = 0;
  
}




return MEA_OK;
}

MEA_Status MEA_API_TFT_QoS_Profile_IsExist(MEA_Unit_t unit, MEA_TFT_t  tft_profile_Id, MEA_Bool silent, MEA_Bool *exist)
{
    if (!MEA_HPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HPM_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (exist == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
    *exist = MEA_FALSE;




    return mea_drv_TFT_prof_IsExist(unit, tft_profile_Id, silent, exist);
}


MEA_Status MEA_API_TFT_Set_ClassificationRule(MEA_Unit_t unit, MEA_TFT_t tft_profile_Id, MEA_Uint8 index, MEA_TFT_data_dbt  *entry)
{
    MEA_Bool exist;
    MEA_Bool silent = MEA_FALSE;

    if (!MEA_HPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HPM_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  data = NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_TFT_prof_IsExist(unit, tft_profile_Id, silent, &exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_TFT_prof_IsExist \n", __FUNCTION__);
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "TFT_prof %d not exist\n", tft_profile_Id);
        return MEA_ERROR;
    }


    if (index >= MEA_TFT_L3L4_MAX_Rule) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - the priority rule is out of range \n", __FUNCTION__);
        return MEA_ERROR;
    }




    if (MEA_OS_memcmp(entry, &mea_TFT_prof_TBL[tft_profile_Id].data_Rules_info[index], sizeof(MEA_TFT_data_dbt)) != 0)
    {


        /******************************************/
        /* check the parameters                   */
        /******************************************/
        
        
        if(mea_drv_TFT_Check_LPM_Paramters(unit,tft_profile_Id, index, entry) != MEA_OK){
            return MEA_ERROR;
        }

            if (mea_drv_TFT_SetEntry(unit, tft_profile_Id, index, entry) != MEA_OK){
                return MEA_ERROR;
            }
            /*save info*/
            MEA_OS_memcpy(&mea_TFT_prof_TBL[tft_profile_Id].data_Rules_info[index], entry, sizeof(MEA_TFT_data_dbt));
        }
  


    return MEA_OK;
}
MEA_Status MEA_API_TFT_Get_ClassificationRule(MEA_Unit_t unit, MEA_TFT_t tft_profile_Id, MEA_Uint8 index, MEA_TFT_data_dbt  *entry)
{
    MEA_Bool exist;
    MEA_Bool silent = MEA_FALSE;
    
    if (!MEA_HPM_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HPM_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  data = NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_TFT_prof_IsExist(unit, tft_profile_Id, silent, &exist) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_TFT_prof_IsExist \n", __FUNCTION__);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_TFT_prof_IsExist  not valid\n", __FUNCTION__);
        return MEA_ERROR;

    }

        if (index >= MEA_TFT_L3L4_MAX_Rule){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - the priority rule is out of range \n", __FUNCTION__);
            return MEA_ERROR;
        }

        MEA_OS_memcpy(&entry->TFT_info, &mea_TFT_prof_TBL[tft_profile_Id].data_Rules_info[index], sizeof(MEA_TFT_key_t));

  


    return MEA_OK;
}


/************************************************************************/
/*                                                                      */
/************************************************************************/
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Bool mea_drv_TFT_Prof_Mask_find_free(MEA_Unit_t unit, MEA_TFT_t  *id_io)
{

    MEA_Uint16 i;


    for (i = 0; i < MEA_TFT_MASK_MAX_PROFILE; i++)
    {
        if (mea_TFT_mask_prof_TBL[i].valid == MEA_FALSE) {
            *id_io = i;
            return MEA_TRUE;
        }
    }

    return MEA_FALSE;


}

static MEA_Bool mea_drv_TFT_Prof_Mask_IsRange(MEA_Unit_t unit, MEA_TFT_t tft_Id, MEA_Bool silent)
{

    if ((tft_Id) >= MEA_TFT_MASK_MAX_PROFILE) {
        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - out range \n", __FUNCTION__);
        }
        return MEA_FALSE;
    }
    else {
        return MEA_TRUE;
    }

    return MEA_FALSE;

}

static MEA_Status mea_drv_TFT_prof_Mask_IsExist(MEA_Unit_t unit, MEA_TFT_t tft_Id, MEA_Bool silent, MEA_Bool *exist)
{
    if (exist == NULL) {
        if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
    *exist = MEA_FALSE;

    if (mea_drv_TFT_Prof_Mask_IsRange(unit, tft_Id, silent) != MEA_TRUE) {
        if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  failed  for id=%d\n", __FUNCTION__, tft_Id);
        return MEA_ERROR;
    }

    if (mea_TFT_mask_prof_TBL[tft_Id].valid == MEA_TRUE) {
        *exist = MEA_TRUE;
        return MEA_OK;
    }

    return MEA_OK;

}

void mea_drv_TFT_prof_MASK_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32             *data = (MEA_Uint32*)arg4;

    // MEA_Uint32                 val[5];
    MEA_Uint32                 i = 0;

    /* Update Hw Entry */
    for (i = 0; i < len; i++)
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            data[i],
            MEA_MODULE_IF);
    }

}
#define MEA_TFT_PROF_MASK_HW_LEN 2
static MEA_Status mea_drv_TFT_Prof_Mask_UpdateHw(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_TFT_mask_data_dbt *entry)
{

    MEA_ind_write_t      ind_write;
    MEA_Bool             support_hw = MEA_TRUE;

    
    MEA_Uint32                  Val[2];
    

    Val[0] = entry->mask;

    if (entry->en_rule_type == MEA_TRUE) {
        Val[1] = entry->rule_type;
    }
    else {
        Val[1] = 7;
    }

    /* Prepare ind_write structure */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TFT_MASK_PROF;
    ind_write.tableOffset = MaskId;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_TFT_prof_MASK_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
    ind_write.funcParam3 = (MEA_funcParam)MEA_TFT_PROF_MASK_HW_LEN;
    ind_write.funcParam4 = (MEA_funcParam)&Val;


    if (support_hw) {

        if (ENET_WriteIndirect(unit, &ind_write, ENET_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_IF);
            return ENET_ERROR;
        }
    }


    return MEA_OK;
}


static MEA_Status mea_drv_TFT_Prof_Mask_Rinit(MEA_Unit_t unit)
{
    MEA_Uint32 MaskId;

    for (MaskId = 0; MaskId < MEA_TFT_MASK_MAX_PROFILE; MaskId++)
    {
        if(!mea_TFT_mask_prof_TBL[MaskId].valid)
            continue;

        mea_drv_TFT_Prof_Mask_UpdateHw(unit, MaskId, &mea_TFT_mask_prof_TBL[MaskId].data);

    }


    return MEA_OK;
}

/************************************************************************/
/* TFT_Prof_Mask                                                        */
/************************************************************************/
MEA_Status MEA_API_TFT_Prof_Mask_Create(MEA_Unit_t unit, MEA_Uint16 *MaskId)
{
    MEA_Bool exist;
    MEA_Bool silent = MEA_FALSE;

    if (!MEA_HPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_TFT_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(MaskId == NULL)
        return MEA_ERROR;


    if ((*MaskId) != MEA_PLAT_GENERATE_NEW_ID) {

        if (mea_drv_TFT_Prof_Mask_IsRange(unit, *MaskId, silent) != MEA_TRUE) {
            if (!silent)
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  failed TFT Prof_Mask Is out of Range id=%d\n", __FUNCTION__, *MaskId);
            return MEA_ERROR;
        }

        if (mea_drv_TFT_prof_Mask_IsExist(unit, *MaskId, silent, &exist) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_TFT_prof_Mask_IsExist failed (*id_io=%d\n",
                __FUNCTION__, MaskId);
            return MEA_ERROR;
        }
        if (exist) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MaskId %d is already exist\n", __FUNCTION__, *MaskId);
            return MEA_ERROR;
        }


    }
    else {
        /*create profile */
        if (mea_drv_TFT_Prof_Mask_find_free(unit, MaskId) == MEA_FALSE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_TFT_Prof_Mask_find_free failed\n", __FUNCTION__);
            return MEA_ERROR;

        }
    }




    mea_TFT_mask_prof_TBL[*MaskId].valid = MEA_TRUE;
    mea_TFT_mask_prof_TBL[*MaskId].numOf_owners = 1;


    return MEA_OK;

}

MEA_Status MEA_API_TFT_Prof_Mask_Set(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_TFT_mask_data_dbt *entry)
{
    MEA_Bool silent = MEA_FALSE;
    MEA_Bool exist = MEA_FALSE;


    if (mea_drv_TFT_prof_Mask_IsExist(unit, MaskId, silent, &exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_TFT_prof_Mask_IsExist failed (*id_io=%d\n",
            __FUNCTION__, MaskId);
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MaskId %d is Not exist\n", __FUNCTION__, MaskId);
        return MEA_ERROR;
    }

    

    if(mea_drv_TFT_Prof_Mask_UpdateHw(unit, MaskId, entry) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_TFT_Prof_Mask_UpdateHw MaskId %d \n", __FUNCTION__, MaskId);
        return MEA_ERROR;
    }


    MEA_OS_memcpy(&mea_TFT_mask_prof_TBL[MaskId].data, entry, sizeof(MEA_TFT_mask_data_dbt));


    return MEA_OK;
}
MEA_Status MEA_API_TFT_Prof_Mask_Get(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_TFT_mask_data_dbt *entry)
{
    MEA_Bool silent = MEA_FALSE;
    MEA_Bool exist = MEA_FALSE;


    if (mea_drv_TFT_prof_Mask_IsExist(unit, MaskId, silent, &exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_TFT_prof_Mask_IsExist failed (*id_io=%d\n",
            __FUNCTION__, MaskId);
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MaskId %d is Not exist\n", __FUNCTION__, MaskId);
        return MEA_ERROR;
    }



    MEA_OS_memcpy(entry, &mea_TFT_mask_prof_TBL[MaskId].data, sizeof(MEA_TFT_mask_data_dbt));

    return MEA_OK;
}
MEA_Status MEA_API_TFT_Prof_Mask_Delete(MEA_Unit_t unit, MEA_Uint16 MaskId)
{
    MEA_Bool exist;
    
    MEA_Bool silent = MEA_FALSE;

    MEA_TFT_mask_data_dbt entry;

    if (!MEA_HPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HPM_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_TFT_prof_Mask_IsExist(unit, MaskId, silent, &exist) != MEA_OK) {
        return MEA_ERROR;
    }

    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - The MaskId %d profile not exist \n",
            __FUNCTION__, MaskId);
        return MEA_ERROR;
    }




    if (mea_TFT_mask_prof_TBL[MaskId].numOf_owners > 1) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - TFT MaskId %d  profile can't be delete numOf owner %d \n", __FUNCTION__,
            MaskId,
            mea_TFT_mask_prof_TBL[MaskId].numOf_owners);
        return MEA_ERROR;
    }
    else {
        MEA_OS_memset(&entry, 0, sizeof(entry));
        if (mea_drv_TFT_Prof_Mask_UpdateHw(unit, MaskId, &entry) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " %s\n mea_drv_TFT_Prof_Mask_UpdateHw %d\n", __FUNCTION__,
                MaskId);
            return MEA_ERROR;
        }

        MEA_OS_memset(&mea_TFT_mask_prof_TBL[MaskId], 0, sizeof(mea_TFT_mask_prof_TBL[0]));
        mea_TFT_mask_prof_TBL[MaskId].valid = MEA_FALSE;
        mea_TFT_mask_prof_TBL[MaskId].numOf_owners = 0;
    }




    return MEA_OK;
}



MEA_Status MEA_API_TFT_Prof_Mask_IsExist(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_Bool silent, MEA_Bool *exist) 
{

    if (!MEA_HPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HPM_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    return mea_drv_TFT_prof_Mask_IsExist(unit, MaskId, silent, exist);
}


MEA_Status mea_drv_TFT_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;
    MEA_Uint32 i;

    if (!MEA_HPM_SUPPORT) {
        return MEA_OK;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize HPM .\n"); 

    /* Allocate Table */
    size = MEA_TFT_L3L4_MAX_PROFILE * sizeof(mea_tft_profile_dbt);
    if (size != 0) {
        mea_TFT_prof_TBL = (mea_tft_profile_dbt   *)MEA_OS_malloc(size);
        if (mea_TFT_prof_TBL == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_TFT_prof_TBL    failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(mea_TFT_prof_TBL[0]), 0, size);
    }

    for (i = 0; i < MEA_TFT_L3L4_MAX_PROFILE; i++) {
        size = MEA_TFT_L3L4_MAX_Rule * sizeof(MEA_TFT_data_dbt);
        if (size != 0) {
            mea_TFT_prof_TBL[i].data_Rules_info = (MEA_TFT_data_dbt   *)MEA_OS_malloc(size);
            if (mea_TFT_prof_TBL[i].data_Rules_info == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Allocate mea_TFT_prof_TBL[i].data_Rules_info    failed (size=%d)\n",
                    __FUNCTION__, size);
                return MEA_ERROR;
            }
            MEA_OS_memset(&(mea_TFT_prof_TBL[i].data_Rules_info[0]), 0, size);
        }





    }





    size = MEA_TFT_MASK_MAX_PROFILE * sizeof(mea_tft_prof_mask_dbt);
    if (size != 0) {
        mea_TFT_mask_prof_TBL = (mea_tft_prof_mask_dbt *)MEA_OS_malloc(size);
        if (mea_TFT_mask_prof_TBL == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_TFT_mask_prof_TBL    failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(mea_TFT_mask_prof_TBL[0]), 0, size);
    }



    /* Allocate Table */
   

    mea_TFT_L3_Internal_EtherType_prof_TBL[0].data.EtherType = 0;
    mea_TFT_L3_Internal_EtherType_prof_TBL[0].data.valid = MEA_TRUE;

    mea_TFT_L3_Internal_EtherType_prof_TBL[1].data.EtherType = MEA_TFT_L3_Internal_EtherType_1;
    mea_TFT_L3_Internal_EtherType_prof_TBL[1].data.valid = MEA_TRUE;

    mea_TFT_L3_Internal_EtherType_prof_TBL[2].data.EtherType = MEA_TFT_L3_Internal_EtherType_2;
    mea_TFT_L3_Internal_EtherType_prof_TBL[2].data.valid = MEA_TRUE;

    mea_TFT_L3_Internal_EtherType_prof_TBL[3].data.EtherType = MEA_TFT_L3_Internal_EtherType_3;
    mea_TFT_L3_Internal_EtherType_prof_TBL[3].data.valid = MEA_TRUE;



    return MEA_OK;
}

MEA_Status mea_drv_TFT_RInit(MEA_Unit_t       unit_i)
{
    
    
    if (!MEA_HPM_SUPPORT) {
        return MEA_OK;
    }




    if (mea_drv_TFT_Prof_Rinit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "ERROR..mea_drv_TFT_Prof_Rinit\n");
    }

    if(mea_drv_TFT_Prof_Mask_Rinit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "ERROR..mea_drv_TFT_Prof_Mask_Rinit\n");

    }

    if (mea_drv_tft_Set_L3_Internal_EtherType_prof(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "ERROR..mea_drv_tft_Set_L3_Internal_EtherType_prof\n");

    }


    return MEA_OK;
}
MEA_Status mea_drv_TFT_Conclude(MEA_Unit_t       unit_i)
{

    if (!MEA_HPM_SUPPORT) {
        return MEA_OK;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude TFT ..\n");
    if (mea_TFT_prof_TBL) {
        MEA_OS_free(mea_TFT_prof_TBL);
        mea_TFT_prof_TBL = NULL;
    }


    if (mea_TFT_mask_prof_TBL) {
        MEA_OS_free(mea_TFT_mask_prof_TBL);
        mea_TFT_mask_prof_TBL = NULL;
    }



    return MEA_OK;
}


/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_API_UEPDN_TFT_TEID_ConfigAction_IsExist(MEA_Unit_t    unit_i,
                                                       MEA_Uint16    UEPDN_Id,
                                                       MEA_Uint8     index,
                                                       MEA_Bool      *exist)
{
    MEA_Action_t   ActionId;
    

    
    if (!MEA_HPM_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_GW_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
    *exist = MEA_FALSE;

    if (UEPDN_Id >= MEA_UEPDN_MAX_ID){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  UEPDN_id is out  \n", __FUNCTION__);
        return MEA_ERROR;
    }
    if (index >= MEA_TFT_MAX_TEID_Rule) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d is out of range \n",
            __FUNCTION__, index);
        return MEA_ERROR;

    }

    ActionId = MEA_Platform_Get_HPM_BASE() + (UEPDN_Id * 8) + index;


    return MEA_API_IsExist_Action(unit_i, ActionId, exist);

  
    
}




MEA_Status MEA_API_Create_UEPDN_TEID_ConfigAction(MEA_Unit_t unit, MEA_Uint16 UEPDN_Id, MEA_Uint8 index, mea_TFT_Action_rule_t *entry)
{
    
    MEA_Action_t ActionId;
    
    if (!MEA_HPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_GW_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (index >= MEA_TFT_MAX_TEID_Rule) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d is out of range \n",
            __FUNCTION__, index);
        return MEA_ERROR;

    }
    

    if (UEPDN_Id >= MEA_UEPDN_MAX_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  UEPDN_id is out  \n", __FUNCTION__);
        return MEA_ERROR;
    }

    ActionId = MEA_Platform_Get_HPM_BASE() + (UEPDN_Id * 8) + index;
    entry->action_params->Action_type = MEA_ACTION_TYPE_HPM;

     
    if (MEA_API_Create_Action(MEA_UNIT_0,
        entry->action_params,
        entry->OutPorts_Entry,
        entry->Policer_Entry,
        entry->EHP_Entry,
        &ActionId) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - HPM_ACTION Create failed   (UEPDN_Id %d) (Qos %d) ->>(Act %d) \n",__FUNCTION__, UEPDN_Id,index, ActionId);
        return MEA_ERROR;
    }



    return MEA_OK;

}

MEA_Status MEA_API_Set_UEPDN_TEID_ConfigAction(MEA_Unit_t unit, MEA_Uint16 UEPDN_Id, MEA_Uint8 index, mea_TFT_Action_rule_t *entry)
{
    MEA_Action_t ActionId;
    
    if (!MEA_HPM_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HPM_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (index >= MEA_TFT_MAX_TEID_Rule){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d is out of range \n",
            __FUNCTION__,index);
        return MEA_ERROR;

    }
   

    if (UEPDN_Id >= MEA_UEPDN_MAX_ID){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  UEPDN_id is out  \n", __FUNCTION__);
        return MEA_ERROR;
    }
    
    ActionId = MEA_Platform_Get_HPM_BASE() + (UEPDN_Id * 8) + index;
    entry->action_params->Action_type = MEA_ACTION_TYPE_HPM;


    if (MEA_API_Set_Action(MEA_UNIT_0,
        ActionId,
        entry->action_params,
        entry->OutPorts_Entry,
        entry->Policer_Entry,
        entry->EHP_Entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - HPM_ACTION Set failed   (UEPDN_Id %d) (Qos %d) ->>(Act %d) \n", __FUNCTION__, UEPDN_Id, index, ActionId);
        return MEA_ERROR;
    }

    




    return MEA_OK;
}


MEA_Status MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_Unit_t unit, MEA_Uint16 UEPDN_Id, MEA_Uint8 index, mea_TFT_Action_rule_t *entry)
{
    MEA_Action_t  ActionId;
    
    if (!MEA_HPM_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HPM_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (index >= MEA_TFT_MAX_TEID_Rule){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d is out of range \n",
            __FUNCTION__, index);
        return MEA_ERROR;

    }
    

    if (UEPDN_Id >= MEA_UEPDN_MAX_ID){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  UEPDN_Id is out  \n", __FUNCTION__);
        return MEA_ERROR;
    }

    ActionId = MEA_Platform_Get_HPM_BASE() + (UEPDN_Id * 8) + index;
    entry->action_params->Action_type = MEA_ACTION_TYPE_HPM;


    if (MEA_API_Get_Action(MEA_UNIT_0,
        ActionId,
        entry->action_params,
        entry->OutPorts_Entry,
        entry->Policer_Entry,
        entry->EHP_Entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - HPM_ACTION Set failed   (UEPDN_Id %d) (Qos %d) ->>(Act %d) \n", __FUNCTION__, UEPDN_Id, index, ActionId);
        return MEA_ERROR;
    }
    


    
    
    
    return MEA_OK;
}

MEA_Status MEA_API_Delete_UEPDN_TEID_ConfigAction(MEA_Unit_t unit, MEA_Uint16 UEPDN_Id, MEA_Uint8 index)
{

    MEA_Action_t ActionId;

    if (!MEA_HPM_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_GW_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (index >= MEA_TFT_MAX_TEID_Rule) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d is out of range \n",
            __FUNCTION__, index);
        return MEA_ERROR;

    }

    if (UEPDN_Id >= MEA_UEPDN_MAX_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  UEPDN_id is out  \n", __FUNCTION__);
        return MEA_ERROR;
    }

    ActionId = MEA_Platform_Get_HPM_BASE() + (UEPDN_Id * 8) + index;
   
    return MEA_API_Delete_Action_HPM(unit, ActionId, MEA_ACTION_TYPE_HPM);

     
}





