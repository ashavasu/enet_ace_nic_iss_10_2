/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_queueStatistics_drv.h"
#if defined(MEA_OS_LINUX)
#include <semaphore.h>
#endif

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

#define QUEUE_STATS_BUFF_SIZE 128 * 1024
#define QUEUE_STATS_QUEUE_DATA_SIZE_IN_BYTES 32
#define MEA_QueueStatistics_CLEAR_DONE_THRESHOLD 64
#define QUEUE_STATS_NUMBER_OF_DB 3
#define DEVICE_NAME_STR_LEN 32
#ifdef DEBUG
#define NUM_OF_BUFFER_DEBUG_PRINTS 8
#endif
#define ETHER 1

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/

typedef struct
{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
MEA_uint64 ingressBytes;
MEA_uint64 egressBytes;
MEA_Uint32 ingressPackets;
MEA_Uint32 egressPackets;
MEA_uint64 droppedPackets;
#else
MEA_uint64 droppedPackets;
MEA_Uint32 egressPackets;
MEA_Uint32 ingressPackets;
MEA_uint64 egressBytes;
MEA_uint64 ingressBytes;
#endif    
    
}MEA_QueueStatistics_hardwareData;

typedef enum DB_NUMBER
{
    DB0,
    DB1,
    DB2
}DB_NUMBER;

typedef struct
{
    /**
    * @brief A set of 3 databases. In all times 2 will be used for data accumulation, and 1 will be spare so a user can get it to read the last hardware reading.
    */
    MEA_Counters_QueueStatistics_qStats_dbt qStatsArr[QUEUE_STATS_NUMBER_OF_DB][MEA_QUEUESTATS_NUM_OF_QUEUES];
    DB_NUMBER m_lastUpdated; /* last updated database */
    DB_NUMBER m_userHas; /* The database that we gave the user */
    DB_NUMBER m_userHad; /* Used for the case when we finished update from one database to the other, but the user has already requested the updated database in the current Cycle - so we won't loose the database cycle count. */
    DB_NUMBER m_toUpdate; /* The database to update now */
#if defined(MEA_OS_LINUX)
    pthread_t       ENET_thread_queueStats_id;
    pthread_mutex_t dbLock;
    pthread_mutex_t goStateLock; // for controlling access to the variable: MEA_QueueStatistics_go
    pthread_cond_t clearDbCond;
    sem_t startTaskSem;
#endif
}MEA_QueueStatistics_DataBase;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

static MEA_Bool MEA_QueueStatistics_moduleIsInitialized = 0;
static MEA_Bool MEA_QueueStatistics_go = 0;
static MEA_QueueStatistics_DataBase MEA_QueueStatistics_qStatsDataBase;
static MEA_Bool MEA_QueueStatistics_isClearDBRequest = 0;
static MEA_Uint32 MEA_QueueStatistics_clearOperationCount = 0;
#ifdef ETHER
//static const char *deviceName = "/dev/ethernity0/stat";
#endif    

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
#ifdef ETHER
static void mea_drv_readQueueDataFromHW_QueueStatistics(MEA_Uint32 qIndex, MEA_Uint8 *hwDataBuff);
static void mea_drv_updateQueueStats_QueueStatistics(MEA_Uint32 qIndex);
static void *mea_drv_QueueStatsTask_QueueStatistics(void *arg);
static MEA_Status mea_drv_UpdateDataBase_QueueStatistics(MEA_Uint8 *hwDataBuff);
static MEA_Bool mea_drv_isUserRequestedDatabaseInThisCycle_QueueStatistics();
static void mea_drv_cycleDatabases_QueueStatistics();
#endif //ETHER
/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
#ifdef ETHER
static void mea_drv_readQueueDataFromHW_QueueStatistics(MEA_Uint32 qIndex, MEA_Uint8 *hwDataBuff)
{
	DB_NUMBER toUpdate = MEA_QueueStatistics_qStatsDataBase.m_toUpdate;
    DB_NUMBER lastUpdated = MEA_QueueStatistics_qStatsDataBase.m_lastUpdated;
    hwDataBuff += (qIndex * QUEUE_STATS_QUEUE_DATA_SIZE_IN_BYTES); // shift the pointer to the right place in the buffer
    MEA_QueueStatistics_qStatsDataBase.qStatsArr[toUpdate][qIndex].droppedPackets.val = (((MEA_QueueStatistics_hardwareData *)hwDataBuff)->droppedPackets) + MEA_QueueStatistics_qStatsDataBase.qStatsArr[lastUpdated][qIndex].droppedPackets.val;
    MEA_QueueStatistics_qStatsDataBase.qStatsArr[toUpdate][qIndex].egressPackets.val = (((MEA_QueueStatistics_hardwareData *)hwDataBuff)->egressPackets) + MEA_QueueStatistics_qStatsDataBase.qStatsArr[lastUpdated][qIndex].egressPackets.val;
    MEA_QueueStatistics_qStatsDataBase.qStatsArr[toUpdate][qIndex].ingressPackets.val = (((MEA_QueueStatistics_hardwareData *)hwDataBuff)->ingressPackets) + MEA_QueueStatistics_qStatsDataBase.qStatsArr[lastUpdated][qIndex].ingressPackets.val;
    MEA_QueueStatistics_qStatsDataBase.qStatsArr[toUpdate][qIndex].egressBytes.val = (((MEA_QueueStatistics_hardwareData *)hwDataBuff)->egressBytes) + MEA_QueueStatistics_qStatsDataBase.qStatsArr[lastUpdated][qIndex].egressBytes.val;
    MEA_QueueStatistics_qStatsDataBase.qStatsArr[toUpdate][qIndex].ingressBytes.val = (((MEA_QueueStatistics_hardwareData *)hwDataBuff)->ingressBytes) + MEA_QueueStatistics_qStatsDataBase.qStatsArr[lastUpdated][qIndex].ingressBytes.val;  
}

static void mea_drv_updateQueueStats_QueueStatistics(MEA_Uint32 qIndex)
{
    MEA_Counters_QueueStatistics_qStats_dbt *currentQStats = &(MEA_QueueStatistics_qStatsDataBase.qStatsArr[MEA_QueueStatistics_qStatsDataBase.m_toUpdate][qIndex]);
    currentQStats->deltaIngressPackets = (MEA_Int32)labs(((long)currentQStats->ingressPackets.val) - ((long)currentQStats->egressPackets.val));
    currentQStats->deltaIngressBytes = (MEA_Int32)labs(((long)currentQStats->ingressBytes.val) - ((long)currentQStats->egressBytes.val));
    currentQStats->curQueueSizePackets = currentQStats->ingressPackets.val - currentQStats->egressPackets.val - currentQStats->droppedPackets.val;
    currentQStats->curQueueSizeBytes = currentQStats->ingressBytes.val - currentQStats->egressBytes.val;
}

static MEA_Status mea_drv_UpdateDataBase_QueueStatistics(MEA_Uint8 *hwDataBuff)
{
    MEA_Uint32 qIndex = 0;
    if(MEA_QueueStatistics_isClearDBRequest == 1)
    {
        ++MEA_QueueStatistics_clearOperationCount;
        if(MEA_QueueStatistics_clearOperationCount >= MEA_QueueStatistics_CLEAR_DONE_THRESHOLD)
        {
#if defined(MEA_OS_LINUX)
    	    if(pthread_mutex_lock(&(MEA_QueueStatistics_qStatsDataBase.dbLock)) != MEA_OK)
		    {
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error locking mutex ", __FUNCTION__);
		        //return MEA_ERROR; So user is not stucked in function MEA_API_ClearCounters_QueueStatistics
		    }
            pthread_cond_signal(&(MEA_QueueStatistics_qStatsDataBase.clearDbCond));
            if(pthread_mutex_unlock(&(MEA_QueueStatistics_qStatsDataBase.dbLock)) != MEA_OK)
		    {
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error unlocking mutex ", __FUNCTION__);
		        return MEA_ERROR;
		    }
#endif
        }
    }
    else
    {
        for(qIndex = 0; qIndex < MEA_QUEUESTATS_NUM_OF_QUEUES; ++qIndex)
        {
            mea_drv_readQueueDataFromHW_QueueStatistics(qIndex, hwDataBuff);
            mea_drv_updateQueueStats_QueueStatistics(qIndex);
        }
    }
    return MEA_OK;
}

static void mea_drv_swapDatabases_QueueStatistics(DB_NUMBER* dbl, DB_NUMBER* dbr)
{
	DB_NUMBER tempDBnum = *dbl;
	*dbl = *dbr;
	*dbr = tempDBnum;
}

/* This function answers wether the user has requested a database in the current Cycle. */
static MEA_Bool mea_drv_isUserRequestedDatabaseInThisCycle_QueueStatistics()
{
	return(MEA_QueueStatistics_qStatsDataBase.m_userHas == MEA_QueueStatistics_qStatsDataBase.m_lastUpdated);
}

static void mea_drv_cycleDatabases_QueueStatistics()
{
#if defined(MEA_OS_LINUX)
    if(pthread_mutex_lock(&(MEA_QueueStatistics_qStatsDataBase.dbLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error locking mutex ", __FUNCTION__);
        return;
    }
	if(mea_drv_isUserRequestedDatabaseInThisCycle_QueueStatistics())
	{
		MEA_QueueStatistics_qStatsDataBase.m_lastUpdated = MEA_QueueStatistics_qStatsDataBase.m_toUpdate;
		MEA_QueueStatistics_qStatsDataBase.m_toUpdate = MEA_QueueStatistics_qStatsDataBase.m_userHad;
	}
	else
	{
		mea_drv_swapDatabases_QueueStatistics(&(MEA_QueueStatistics_qStatsDataBase.m_toUpdate), &(MEA_QueueStatistics_qStatsDataBase.m_lastUpdated));
	}
    if(pthread_mutex_unlock(&(MEA_QueueStatistics_qStatsDataBase.dbLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error unlocking mutex ", __FUNCTION__);
        return;
    }
#endif
}

static void *mea_drv_QueueStatsTask_QueueStatistics(void *arg)
{
    MEA_Status err = MEA_OK;
#ifdef ETHER
    MEA_Uint8 hwDataBuff[QUEUE_STATS_BUFF_SIZE];
#endif
#if defined(MEA_OS_LINUX)  
#ifdef ETHER
	char    devname[DEVICE_NAME_STR_LEN];
	char           *ENET_card_str = MEA_OS_getenv("MEA_CARD");
	int             ENET_card = ((ENET_card_str) ? atoi(ENET_card_str) : 0);
	if (!MEA_QUEUESTATISTICS_SUPPORT)
		return MEA_OK;

	sprintf(devname, "/dev/%s%d/%s", DEVICE_NAME_ENET, ENET_card, DEVICE_NODE_ENET_stat);
    MEA_Int32 fd_bar2 = open(devname, O_RDWR);
    if(fd_bar2 < 0) 
    {
        MEA_QueueStatistics_moduleIsInitialized = 0;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error cant open device %s \n", __FUNCTION__, devname);
        return (void *)0;
    }
    MEA_QueueStatistics_moduleIsInitialized = 1;
#endif
    while(MEA_QueueStatistics_moduleIsInitialized)
    {
        if(sem_wait(&(MEA_QueueStatistics_qStatsDataBase.startTaskSem)) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error waiting semaphore ", __FUNCTION__);
            return (void *)0;
        }
        while(MEA_QueueStatistics_go)
        {
#ifdef ETHER
            pread(fd_bar2, hwDataBuff, sizeof hwDataBuff, 0);
            err = mea_drv_UpdateDataBase_QueueStatistics(hwDataBuff);
#endif
			if(err != MEA_OK)
            {
                pthread_exit(NULL);
            }
            //Finished updating the database - now switch databases:
            mea_drv_cycleDatabases_QueueStatistics();
        }
    }
#endif
#ifdef ETHER
    close(fd_bar2);
#endif
    return (void *)1;
}


#endif //ETHER
/*----------------------------------------------------------------------------*/
/*             external functions implementation                              */
/*----------------------------------------------------------------------------*/

MEA_Status mea_drv_Init_QueueStatistics(MEA_Unit_t unit_i)
{
    if((!MEA_QUEUESTATISTICS_SUPPORT))
    {
        return MEA_OK;
    }
    if(MEA_QueueStatistics_moduleIsInitialized)
	   return MEA_OK;

#if defined(MEA_OS_LINUX) && defined(ETHER)
    if(pthread_mutex_init(&(MEA_QueueStatistics_qStatsDataBase.dbLock), NULL) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics mutex initialization failed\n");
        return MEA_ERROR;
    }
    if(pthread_cond_init(&(MEA_QueueStatistics_qStatsDataBase.clearDbCond), NULL) != MEA_OK)
    {
    	pthread_mutex_destroy(&(MEA_QueueStatistics_qStatsDataBase.dbLock));
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics CV initialization failed\n");
        return MEA_ERROR;
    }
    if(sem_init(&(MEA_QueueStatistics_qStatsDataBase.startTaskSem), 0, 0) != MEA_OK)
    {
        pthread_mutex_destroy(&(MEA_QueueStatistics_qStatsDataBase.dbLock));
        pthread_cond_destroy(&(MEA_QueueStatistics_qStatsDataBase.clearDbCond));
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics semaphore initialization failed\n");
        return MEA_ERROR;   
    }
    if(pthread_mutex_init(&(MEA_QueueStatistics_qStatsDataBase.goStateLock), NULL) != MEA_OK)
    {
        sem_destroy(&(MEA_QueueStatistics_qStatsDataBase.startTaskSem));
        pthread_mutex_destroy(&(MEA_QueueStatistics_qStatsDataBase.dbLock));
        pthread_cond_destroy(&(MEA_QueueStatistics_qStatsDataBase.clearDbCond));
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics mutex initialization failed\n");
        return MEA_ERROR;
    }
    if(pthread_create(&(MEA_QueueStatistics_qStatsDataBase.ENET_thread_queueStats_id), NULL, mea_drv_QueueStatsTask_QueueStatistics, NULL) != MEA_OK)
    {
        sem_destroy(&(MEA_QueueStatistics_qStatsDataBase.startTaskSem));
        pthread_mutex_destroy(&(MEA_QueueStatistics_qStatsDataBase.dbLock));
        pthread_mutex_destroy(&(MEA_QueueStatistics_qStatsDataBase.goStateLock));
        pthread_cond_destroy(&(MEA_QueueStatistics_qStatsDataBase.clearDbCond));
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics thread creation failed\n");
        return MEA_ERROR;
    }
    MEA_OS_memset(MEA_QueueStatistics_qStatsDataBase.qStatsArr[DB0], 0, sizeof(MEA_Counters_QueueStatistics_qStats_dbt) * MEA_QUEUESTATS_NUM_OF_QUEUES);
    MEA_OS_memset(MEA_QueueStatistics_qStatsDataBase.qStatsArr[DB1], 0, sizeof(MEA_Counters_QueueStatistics_qStats_dbt) * MEA_QUEUESTATS_NUM_OF_QUEUES);
    MEA_OS_memset(MEA_QueueStatistics_qStatsDataBase.qStatsArr[DB2], 0, sizeof(MEA_Counters_QueueStatistics_qStats_dbt) * MEA_QUEUESTATS_NUM_OF_QUEUES);
#endif

    //MEA_QueueStatistics_moduleIsInitialized = 1; // The thread will initialize that variable, if it is successful to open the device.
    MEA_QueueStatistics_qStatsDataBase.m_lastUpdated = DB0;
    MEA_QueueStatistics_qStatsDataBase.m_toUpdate = DB1;
    MEA_QueueStatistics_qStatsDataBase.m_userHas = DB2;
    MEA_QueueStatistics_qStatsDataBase.m_userHad = DB2; // at init it is not important

    return MEA_OK;
}

MEA_Status MEA_API_Start_QueueStatistics(MEA_Unit_t unit_i)
{
	if ((!MEA_QUEUESTATISTICS_SUPPORT)) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Queue Statistics not Support  ", __FUNCTION__);
		return MEA_ERROR;
	}
	
	if(!MEA_QueueStatistics_moduleIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Queue Statistics not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(MEA_QueueStatistics_go == 1)
    {
        return MEA_OK;
    }
    MEA_QueueStatistics_go = 1;
#if defined(MEA_OS_LINUX)
    if(sem_post(&(MEA_QueueStatistics_qStatsDataBase.startTaskSem)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error posting semaphore ", __FUNCTION__);
        return MEA_ERROR;
    }
#endif
    return MEA_OK;
}

MEA_Status MEA_API_Stop_QueueStatistics(MEA_Unit_t unit_i)
{
	if ((!MEA_QUEUESTATISTICS_SUPPORT)) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Queue Statistics not Support  ", __FUNCTION__);
		return MEA_ERROR;
	}
	
	if(!MEA_QueueStatistics_moduleIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Queue Statistics not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(pthread_mutex_lock(&(MEA_QueueStatistics_qStatsDataBase.goStateLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error locking mutex ", __FUNCTION__);
        return MEA_ERROR;
    }
    MEA_QueueStatistics_go = 0;
    if(pthread_mutex_unlock(&(MEA_QueueStatistics_qStatsDataBase.goStateLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error unlocking mutex ", __FUNCTION__);
        return MEA_ERROR;
    }
    return MEA_OK;
}

MEA_Status mea_drv_Reinit_QueueStatistics(MEA_Unit_t unit_i)
{
 	if(!MEA_QUEUESTATISTICS_SUPPORT)
 	{
        return MEA_OK;
    }
    
    if(!MEA_QueueStatistics_moduleIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Queue Statistics not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    return MEA_OK;
}

MEA_Status mea_drv_Conclude_QueueStatistics(MEA_Unit_t unit_i)
{
 	if (!MEA_QUEUESTATISTICS_SUPPORT) 
 	{
        return MEA_OK;
    }
    
    if(!MEA_QueueStatistics_moduleIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Queue Statistics not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    MEA_QueueStatistics_moduleIsInitialized = 0;
    MEA_QueueStatistics_go = 0;
#if defined(MEA_OS_LINUX)
    if(sem_post(&(MEA_QueueStatistics_qStatsDataBase.startTaskSem)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error posting semaphore ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(pthread_join((MEA_QueueStatistics_qStatsDataBase.ENET_thread_queueStats_id), NULL) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics join thread failed\n");
        return MEA_ERROR;
    }
    if(pthread_mutex_destroy(&(MEA_QueueStatistics_qStatsDataBase.dbLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics mutex destroy failed\n");
        return MEA_ERROR;
    }
    if(pthread_mutex_destroy(&(MEA_QueueStatistics_qStatsDataBase.goStateLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics mutex destroy failed\n");
        return MEA_ERROR;
    }
    if(pthread_cond_destroy(&(MEA_QueueStatistics_qStatsDataBase.clearDbCond)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics CV destroy failed\n");
        return MEA_ERROR;
    }
    if(sem_destroy(&(MEA_QueueStatistics_qStatsDataBase.startTaskSem)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Queue Statistics semaphore destroy failed\n");
        return MEA_ERROR;
    }
#endif
    return MEA_OK;
}

MEA_Status MEA_API_GetStatDataBase_QueueStatistics(MEA_Unit_t unit_i, const MEA_Counters_QueueStatistics_qStats_dbt **dbStats)
{
	if(!MEA_QueueStatistics_moduleIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Queue Statistics not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(dbStats == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - NULL pointer received ", __FUNCTION__);
        return MEA_ERROR;
    }
#if defined(MEA_OS_LINUX)
    if(pthread_mutex_lock(&(MEA_QueueStatistics_qStatsDataBase.dbLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error locking mutex ", __FUNCTION__);
        return MEA_ERROR;
    }
    // This is asked to avoid a case of entering this function twice, before a whole cycle is finished - so it woun't mess up the m_userHad field.
    // A whole cycle means that the function mea_drv_cycleDatabases_QueueStatistics was called.
    if(MEA_QueueStatistics_qStatsDataBase.m_userHas != MEA_QueueStatistics_qStatsDataBase.m_lastUpdated)
    {
	    MEA_QueueStatistics_qStatsDataBase.m_userHad = MEA_QueueStatistics_qStatsDataBase.m_userHas;
	    MEA_QueueStatistics_qStatsDataBase.m_userHas = MEA_QueueStatistics_qStatsDataBase.m_lastUpdated;
    }
    *dbStats = MEA_QueueStatistics_qStatsDataBase.qStatsArr[MEA_QueueStatistics_qStatsDataBase.m_userHas];   
    if(pthread_mutex_unlock(&(MEA_QueueStatistics_qStatsDataBase.dbLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error unlocking mutex ", __FUNCTION__);
        return MEA_ERROR;
    }
#endif
    return MEA_OK;
}

MEA_Status MEA_API_ClearCounters_QueueStatistics(MEA_Unit_t unit_i)
{
	if(!MEA_QueueStatistics_moduleIsInitialized)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Queue Statistics not initialized ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(pthread_mutex_lock(&(MEA_QueueStatistics_qStatsDataBase.goStateLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error locking mutex ", __FUNCTION__);
        return MEA_ERROR;
    }
    if(MEA_QueueStatistics_go == 1)
    {
        MEA_QueueStatistics_clearOperationCount = 0;
        MEA_QueueStatistics_isClearDBRequest = 1;
    #if defined(MEA_OS_LINUX)
        if(pthread_mutex_lock(&(MEA_QueueStatistics_qStatsDataBase.dbLock)) != MEA_OK)
        {
            pthread_mutex_unlock(&(MEA_QueueStatistics_qStatsDataBase.goStateLock));
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error locking mutex ", __FUNCTION__);
            return MEA_ERROR;
        }
        while(MEA_QueueStatistics_clearOperationCount < MEA_QueueStatistics_CLEAR_DONE_THRESHOLD)
        {
            pthread_cond_wait(&(MEA_QueueStatistics_qStatsDataBase.clearDbCond), &(MEA_QueueStatistics_qStatsDataBase.dbLock));
        }
    #endif
        MEA_OS_memset(MEA_QueueStatistics_qStatsDataBase.qStatsArr[DB0], 0, sizeof(MEA_Counters_QueueStatistics_qStats_dbt) * MEA_QUEUESTATS_NUM_OF_QUEUES);
        MEA_OS_memset(MEA_QueueStatistics_qStatsDataBase.qStatsArr[DB1], 0, sizeof(MEA_Counters_QueueStatistics_qStats_dbt) * MEA_QUEUESTATS_NUM_OF_QUEUES);
        MEA_OS_memset(MEA_QueueStatistics_qStatsDataBase.qStatsArr[DB2], 0, sizeof(MEA_Counters_QueueStatistics_qStats_dbt) * MEA_QUEUESTATS_NUM_OF_QUEUES);
        MEA_QueueStatistics_isClearDBRequest = 0;
    #if defined(MEA_OS_LINUX)
        if(pthread_mutex_unlock(&(MEA_QueueStatistics_qStatsDataBase.dbLock)) != MEA_OK)
        {
            pthread_mutex_unlock(&(MEA_QueueStatistics_qStatsDataBase.goStateLock));
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error unlocking mutex ", __FUNCTION__);
            return MEA_ERROR;
        }
    }
    if(pthread_mutex_unlock(&(MEA_QueueStatistics_qStatsDataBase.goStateLock)) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Error unlocking mutex ", __FUNCTION__);
        return MEA_ERROR;
    }
#endif
    return MEA_OK;
}
