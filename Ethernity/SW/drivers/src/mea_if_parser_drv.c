/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_port_drv.h"
#include "mea_drv_common.h"
#include "mea_db_drv.h"
#include "mea_init.h"
#include "MEA_platform_types.h"
#include "mea_policer_drv.h"
#include "mea_bm_counters_drv.h"
#include "mea_bm_ehp_drv.h"
#include "mea_limiter_drv.h"
#include "mea_if_parser_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_MAX_CONFIGURE_PARSER_PROTOCOL 18
#define MEA_NEW_PARSER_INIT 1
/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
static MEA_Parser_Entry_dbt *MEA_Parser_Table = NULL;  //[MEA_MAX_PORT_NUMBER+1]; 
static MEA_Bool             MEA_Parser_InitDone = MEA_FALSE;
					  
static MEA_GlobalParser_parsing_L2KEY_t MEA_GlobalParser_Table_parsing_L2KEY[MEA_PARSING_L2_KEY_LAST];

static MEA_Bool mea_GlobalParser_init_flag = MEA_FALSE; 
static MEA_Global_Parser_dbt *MEA_GlobalParser_CAM0_Table = NULL;
static MEA_Global_Parser_dbt *MEA_GlobalParser_CAM1_Table = NULL;
static MEA_Global_Parser_dbt *MEA_GlobalParser_CAM2_Table = NULL;
static MEA_Global_Parser_dbt *MEA_GlobalParser_CAM3_Table = NULL;
static MEA_Global_Parser_dbt *MEA_GlobalParser_CAM4_Table = NULL;
#if 0
static MEA_Global_Parser_dbt *MEA_GlobalParser_ConfigureProtocol_Table=NULL;
#endif

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Parser_UpdateHwEntry>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_Parser_UpdateHwEntry(MEA_Parser_Entry_dbt *entry) 
{

    MEA_Uint32 i;
    


 MEA_Uint32     val[10];
MEA_Uint32 numofregs;
 

	
    MEA_Uint32     count_shift;

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
    count_shift=0;



    MEA_OS_insert_value(count_shift,MEA_PARSET_pri_rule, (MEA_Uint32)entry->f.pri_rule, &val[0]);
    count_shift+=MEA_PARSET_pri_rule;
    MEA_OS_insert_value(count_shift,MEA_PARSET_DSA_Type_enable, (MEA_Uint32)entry->f.DSA_Type_enable, &val[0]);
    count_shift+=MEA_PARSET_DSA_Type_enable;
    MEA_OS_insert_value(count_shift,MEA_PARSET_L4_SRC_DST, (MEA_Uint32)entry->f.L4_SRC_DST, &val[0]);
    count_shift+=MEA_PARSET_L4_SRC_DST;
    MEA_OS_insert_value(count_shift,MEA_PARSET_default_sid, (MEA_Uint32)entry->f.default_sid, &val[0]);
    count_shift+=MEA_PARSET_default_sid;
    MEA_OS_insert_value(count_shift,MEA_PARSET_default_sid_act, (MEA_Uint32)entry->f.default_sid_act, &val[0]);
    count_shift+=MEA_PARSET_default_sid_act;
    MEA_OS_insert_value(count_shift,MEA_PARSET_logical_src_port, ((MEA_Uint32)entry->f.logical_src_port & 0x7f), &val[0]);
    count_shift+=MEA_PARSET_logical_src_port;
    MEA_OS_insert_value(count_shift,MEA_PARSET_logical_src_port_replace, (MEA_Uint32)entry->f.logical_src_port_replace, &val[0]);
    count_shift+=MEA_PARSET_logical_src_port_replace;
    MEA_OS_insert_value(count_shift,MEA_PARSET_mapping_profile, (MEA_Uint32)entry->f.mapping_profile, &val[0]);
    count_shift+=MEA_PARSET_mapping_profile;
    MEA_OS_insert_value(count_shift,MEA_PARSET_filter_key_interface_type, (MEA_Uint32)entry->f.filter_key_interface_type, &val[0]);
    count_shift+=MEA_PARSET_filter_key_interface_type;     
    MEA_OS_insert_value(count_shift,MEA_PARSET_color_valid, (MEA_Uint32)entry->f.color_valid, &val[0]);
    count_shift+=MEA_PARSET_color_valid; 
    MEA_OS_insert_value(count_shift,MEA_PARSET_da_fwrd_l2, (MEA_Uint32)entry->f.ENNI_enable, &val[0]);
    count_shift+=MEA_PARSET_da_fwrd_l2; 
    MEA_OS_insert_value(count_shift,MEA_PARSET_or_mask_net2_0_15, (MEA_Uint32)entry->f.or_mask_net2_0_15, &val[0]);
    count_shift+=MEA_PARSET_or_mask_net2_0_15;
    MEA_OS_insert_value(count_shift,MEA_PARSET_or_mask_net2_16_23, (MEA_Uint32)entry->f.or_mask_net2_16_23, &val[0]);
    count_shift+=MEA_PARSET_or_mask_net2_16_23;
    MEA_OS_insert_value(count_shift,MEA_PARSET_or_mask_net_tag_20_23, (MEA_Uint32)entry->f.or_mask_net_tag_20_23, &val[0]);
    count_shift+=MEA_PARSET_or_mask_net_tag_20_23;
    MEA_OS_insert_value(count_shift,MEA_PARSET_or_mask_pri_3_7, (MEA_Uint32)entry->f.or_mask_pri_3_7, &val[0]);
    count_shift+=MEA_PARSET_or_mask_pri_3_7;
    MEA_OS_insert_value(count_shift,MEA_PARSET_or_mask_L2_Type, (MEA_Uint32)entry->f.or_mask_L2_Type, &val[0]);
    count_shift+=MEA_PARSET_or_mask_L2_Type;
    MEA_OS_insert_value(count_shift,MEA_PARSET_transparent_mode, (MEA_Uint32)entry->f.transparent_mode, &val[0]);
    count_shift+=MEA_PARSET_transparent_mode;
    MEA_OS_insert_value(count_shift,MEA_PARSET_protocol2pri_mapping_enable, (MEA_Uint32)entry->f.protocol2pri_mapping_enable, &val[0]);
    count_shift+=MEA_PARSET_protocol2pri_mapping_enable;
    MEA_OS_insert_value(count_shift,MEA_PARSET_or_mask_pri, (MEA_Uint32)entry->f.or_mask_pri, &val[0]);
    count_shift+=MEA_PARSET_or_mask_pri;
    MEA_OS_insert_value(count_shift,MEA_PARSET_or_mask_src_port, (MEA_Uint32)(entry->f.or_mask_src_port & 0x7f), &val[0]);
    count_shift+=MEA_PARSET_or_mask_src_port;
    MEA_OS_insert_value(count_shift,MEA_PARSET_or_mask_net_tag_0_13, (MEA_Uint32)entry->f.or_mask_net_tag_0_13, &val[0]);
    count_shift+=MEA_PARSET_or_mask_net_tag_0_13;
    MEA_OS_insert_value(count_shift,MEA_PARSET_or_mask_net_tag_14_19, (MEA_Uint32)entry->f.or_mask_net_tag_14_19, &val[0]);
    count_shift+=MEA_PARSET_or_mask_net_tag_14_19;
    MEA_OS_insert_value(count_shift, MEA_PARSET_evc_external_internal, (MEA_Uint32)entry->f.evc_external_internal, &val[0]);
    count_shift += MEA_PARSET_evc_external_internal;
    MEA_OS_insert_value(count_shift,MEA_PARSET_default_net_tag_value, (MEA_Uint32)entry->f.default_net_tag_value, &val[0]);
    count_shift+=MEA_PARSET_default_net_tag_value;
    MEA_OS_insert_value(count_shift,MEA_PARSET_afdx_enable, (MEA_Uint32)entry->f.afdx_enable, &val[0]);
    count_shift+=MEA_PARSET_afdx_enable;
    MEA_OS_insert_value(count_shift,MEA_PARSET_default_pri, (MEA_Uint32)entry->f.default_pri, &val[0]);
    count_shift+=MEA_PARSET_default_pri;
    MEA_OS_insert_value(count_shift,MEA_PARSET_pri_Ip_type, (MEA_Uint32)entry->f.pri_Ip_type, &val[0]);
    count_shift+=MEA_PARSET_pri_Ip_type;
    if(MEA_GLOBAL_MY_MAC_SUPPORT){
        MEA_OS_insert_value(count_shift,MEA_PARSET_my_mac_5, (MEA_Uint32)entry->f.my_Mac.b[5], &val[0]);
        count_shift+=MEA_PARSET_my_mac_5;
        MEA_OS_insert_value(count_shift,MEA_PARSET_my_mac_4, (MEA_Uint32)entry->f.my_Mac.b[4], &val[0]);
        count_shift+=MEA_PARSET_my_mac_4;
        MEA_OS_insert_value(count_shift,MEA_PARSET_my_mac_3, (MEA_Uint32)entry->f.my_Mac.b[3], &val[0]);
        count_shift+=MEA_PARSET_my_mac_3;
        MEA_OS_insert_value(count_shift,MEA_PARSET_my_mac_2, (MEA_Uint32)entry->f.my_Mac.b[2], &val[0]);
        count_shift+=MEA_PARSET_my_mac_2;
        MEA_OS_insert_value(count_shift,MEA_PARSET_my_mac_1, (MEA_Uint32)entry->f.my_Mac.b[1], &val[0]);
        count_shift+=MEA_PARSET_my_mac_1;
        MEA_OS_insert_value(count_shift,MEA_PARSET_my_mac_0, (MEA_Uint32)entry->f.my_Mac.b[0], &val[0]);
        count_shift+=MEA_PARSET_my_mac_0;
       

    }

    MEA_OS_insert_value(count_shift,MEA_PARSET_vlanrange_Prof_Id, (MEA_Uint32)entry->f.vlanrange_prof_Id, &val[0]);
    count_shift+=MEA_PARSET_vlanrange_Prof_Id;
    MEA_OS_insert_value(count_shift,MEA_PARSET_vlanrange_Prof_Valid, (MEA_Uint32)entry->f.vlanrange_prof_valid, &val[0]);
    count_shift+=MEA_PARSET_vlanrange_Prof_Valid;
    MEA_OS_insert_value(count_shift,MEA_PARSET_LAG_dis_MASK, (MEA_Uint32)entry->f.lag_distribution_mask, &val[0]);
    count_shift+=MEA_PARSET_LAG_dis_MASK;
    MEA_OS_insert_value(count_shift, MEA_PARSET_def_sid_port_type, (MEA_Uint32)entry->f.def_sid_port_type, &val[0]);
    count_shift += MEA_PARSET_def_sid_port_type;
    MEA_OS_insert_value(count_shift, MEA_PARSET_l2type_consolidate, (MEA_Uint32)(entry->f.l2type_consolidate), &val[0]);
    count_shift += MEA_PARSET_l2type_consolidate;
    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        MEA_OS_insert_value(count_shift, MEA_PARSET_or_mask_src_port_3BIT, (MEA_Uint32)(entry->f.or_mask_src_port >> 7), &val[0]);
        count_shift += MEA_PARSET_or_mask_src_port_3BIT;
        MEA_OS_insert_value(count_shift, MEA_PARSET_logical_src_port_3BIT, (MEA_Uint32)(entry->f.logical_src_port >> 7), &val[0]);
        count_shift += MEA_PARSET_logical_src_port_3BIT;
    }
    /*4 bit globalMy*/
    if (MEA_GLOBAL_MY_MAC_SUPPORT){
        MEA_OS_insert_value(count_shift, MEA_PARSET_globalMyMac_enable, (MEA_Uint32)(entry->f.local_MAC_enable), &val[0]);
        count_shift += MEA_PARSET_globalMyMac_enable;
    }



    if(MEA_Platform_Get_ParsernumofBit() == 0){
        if(MEA_GLOBAL_MY_MAC_SUPPORT){
        numofregs=6;
        }else{
        numofregs=4;
        }
    }else{
        numofregs=MEA_Platform_Get_ParsernumofBit()/32;
        if(MEA_Platform_Get_ParsernumofBit() % 32 != 0){
        numofregs+=1;
        }

    }

    /* Write values */
    for(i=0;i<numofregs;i++) {
        MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    entry->reg[i]=val[i];
    }

    

   
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Parser_UpdateHw>                                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Parser_UpdateHw(MEA_Port_t            port,
                                      MEA_Parser_Entry_dbt *entry,
                                      MEA_Parser_Entry_dbt *old_entry)
{

	MEA_ind_write_t ind_write;
    mea_memory_mode_e save_globalMemoryMode;


    if ((MEA_Parser_InitDone == MEA_TRUE) &&
        (old_entry != NULL) && 
        (MEA_OS_memcmp(entry,old_entry,sizeof(*entry)) == 0)) {
       return MEA_OK;
    }

	save_globalMemoryMode = globalMemoryMode;


    /* build the ind_write value */
	ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_PARSER;
	ind_write.tableOffset	= port;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_Parser_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)entry;

	
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
		return MEA_ERROR;
    }



    /* return to caller */
	globalMemoryMode = save_globalMemoryMode;
	return MEA_OK;

}



/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_Parser_Table>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_Parser_Table(MEA_Unit_t unit_i)
{

    MEA_Port_t port;
    MEA_Parser_Entry_dbt Parser_Entry;
	MEA_Uint32 size;

    if(b_bist_test){
        return MEA_OK;
    }
    /* Init IF Parser Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Parser  table ...\n");
    
	 

	size = (MEA_MAX_PORT_NUMBER_INGRESS + 1) * sizeof(MEA_Parser_Entry_dbt);
	MEA_Parser_Table = (MEA_Parser_Entry_dbt*)MEA_OS_malloc(size);
	if (MEA_Parser_Table == NULL) {
		MEA_OS_free(MEA_Parser_Table);
		MEA_Parser_Table = NULL;

		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s \n- Allocate MEA_port_map_array failed (size=%d)\n",
			__FUNCTION__,
			size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_Parser_Table, 0, size);



    

    MEA_Parser_InitDone = MEA_FALSE;

    MEA_OS_memset (&Parser_Entry,0,sizeof(Parser_Entry));
    for (port=0;port<MEA_MAX_PORT_NUMBER_INGRESS;port++) {

	    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }
		

        if (MEA_API_Set_Parser_Entry(unit_i,
                                     port,
                                     &Parser_Entry) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_Parser_Entry for port %d failed\n",
                              __FUNCTION__,port);
            return MEA_ERROR;
        }

    }

    MEA_Parser_InitDone = MEA_TRUE;

    /* Return to caller */
    return MEA_OK; 

}


MEA_Status mea_drv_Conclude_Parser_Table(MEA_Unit_t unit_i)
{
	if (MEA_Parser_Table) {
		MEA_OS_free(MEA_Parser_Table);
	}
	return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_Parser_Table>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_Parser_Table(MEA_Unit_t unit_i)
{

    MEA_Port_t port;

    for (port=0;port<MEA_NUM_OF_ELEMENTS(MEA_Parser_Table);port++) {

	    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        if (mea_Parser_UpdateHw(port,
                                &(MEA_Parser_Table[port]),
                                NULL) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_Parser_UpdateHw failed (port=%d)\n",
                              __FUNCTION__,port);
            return MEA_ERROR;
        }

        /************************************************************************/
        /* ADM                                                                  */
        /************************************************************************/
        {
            MEA_Bool enable = MEA_Parser_Table[port].f.logical_src_port_replace;
            MEA_Port_t trunkId = MEA_Parser_Table[port].f.logical_src_port;
            mea_drv_ADM_port_ToTrunk(unit_i, port, trunkId, enable);
        }


    }


    /* Return to caller */
    return MEA_OK; 

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_Parser_Entry>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_Parser_Entry(MEA_Unit_t            unit,
                                    MEA_Port_t            port,
                                    MEA_Parser_Entry_dbt* entry) 
{

	MEA_Uint32 index;
	MEA_API_LOG
    

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS



    /* Check for valid port parameter */
	if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - port %d is invalid\n",
                         __FUNCTION__,port);
       return MEA_ERROR; 
    }

    /* Check for valid entry parameter */
	if (entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry == NULL (port=%d)\n",
                         __FUNCTION__,port);
       return MEA_ERROR; 
    }

    if ((globalMemoryMode != MEA_MODE_REGULAR_ENET3000) &&
        (entry->f.filter_key_interface_type != MEA_FILTER_KEY_INTERFACE_TYPE_SOURCE_PORT)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - filter key not support for (mode=%d)\n",
                         __FUNCTION__,globalMemoryMode);
       return MEA_ERROR; 
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	index = mea_get_port_mapArrayINGRESS(port);
    /* Update the Hardware */
    if (mea_Parser_UpdateHw(port,
                            entry,
                            &(MEA_Parser_Table[index])
                           ) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Ingress Port Update Hw for port %d failed \n",
                          __FUNCTION__,port);
		return MEA_ERROR;
	}



    /************************************************************************/
    /* ADM                                                                  */
    /************************************************************************/
    if ((entry->f.logical_src_port_replace != MEA_Parser_Table[mea_get_port_mapArrayINGRESS(port)].f.logical_src_port_replace) &&
        (entry->f.logical_src_port != MEA_Parser_Table[mea_get_port_mapArrayINGRESS(port)].f.logical_src_port)

        ){
        MEA_Bool enable    = entry->f.logical_src_port_replace;
        MEA_Port_t trunkId = entry->f.logical_src_port;
        mea_drv_ADM_port_ToTrunk(unit, port, trunkId, enable);
    }


    /* Update the Software shadow DB */
    MEA_OS_memcpy(&(MEA_Parser_Table[mea_get_port_mapArrayINGRESS(port)]),
                  entry,
                  sizeof(MEA_Parser_Table[0]));



    /* Return to caller */
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Parser_Entry>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Parser_Entry(MEA_Unit_t            unit,
                                    MEA_Port_t            port,
                                    MEA_Parser_Entry_dbt* entry) 
{

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS


    /* Check for valid port parameter */
	if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - port %d is invalid\n",
                         __FUNCTION__,port);
       return MEA_ERROR; 
    }

    /* Check for valid entry parameter */
    if (entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry==NULL (port=%d)\n",
                         __FUNCTION__,port);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* update output parameter */
    MEA_OS_memcpy(entry,
                  &(MEA_Parser_Table[mea_get_port_mapArrayINGRESS(port)]),
                  sizeof(*entry));

    /* return to caller */  
	return MEA_OK;
}






/************************************************************************/
/*   Global Parser                                                      */
/************************************************************************/




#define MEA_PARSER_MPLS_MULTICAST     0x8848
#define MEA_PARSER_MEF8_MULTICAST     0x88d8





MEA_Status mea_drv_Init_Global_parser(MEA_Unit_t            unit)
{
    MEA_Uint32 size;
    MEA_Global_Parser_dbt        entry;
#ifdef MEA_NEW_PARSER_INIT
    MEA_Uint16 i;
#endif
    


    /*valid ethertype*/ 
    MEA_Uint16 table_CAM0[ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12_LENGTH][2]={/*0*/ {1, 0x88A8}, 
                                                                                   /*1*/ {1, 0x8100},
                                                                                   /*2*/ {1, 0x8847},
                                                                                   /*3*/ {1, MEA_PARSER_MPLS_MULTICAST},
                                                                                   /*4*/ {1, 0x88e7},
                                                                                   /*5*/ {0, 0x0000},
                                                                                   /*6*/ {1, 0x8100},
                                                                                   /*7*/ {1, 0x8100}
    };
    MEA_Uint16 table_CAM1[ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16_LENGTH][2]={ /*0*/ {1, 0x88e7}, 
                                                                                    /*1*/ {1, 0x88a8},
                                                                                    /*2*/ {1, 0x8100},
                                                                                    /*3*/ {1, 0x8847},
                                                                                    /*4*/ {1, MEA_PARSER_MPLS_MULTICAST},
                                                                                    /*5*/ {0, 0x0000},
                                                                                    /*6*/ {0, 0x8100},
                                                                                    /*7*/ {1, 0x8100}
    };
    MEA_Uint16 table_CAM2[ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20_LENGTH][2]={ /*0*/ {1, 0x8100}, 
                                                                                    /*1*/ {1, 0x8847},
                                                                                    /*2*/ {1, MEA_PARSER_MPLS_MULTICAST},
                                                                                    /*3*/ {0, 0x0000},
                                                                                    /*4*/ {0, 0x0000}
        
    };
    MEA_Uint16 table_CAM3[ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34_LENGTH][2]={ /*0*/ {1, 0x88A8}, 
                                                                                    /*1*/ {1, 0x8100}
    };
    MEA_Uint16 table_CAM4[ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38_LENGTH][2]={ /*0*/ {1, 0x8100}, 
                                                                                    /*1*/ {0, 0x0000}
    };


 /* Check if support */
    if (!MEA_NEW_PARSER_SUPPORT){
        return MEA_OK;   
    }
 
    MEA_OS_memset(&MEA_GlobalParser_Table_parsing_L2KEY[0],0,sizeof(MEA_GlobalParser_Table_parsing_L2KEY));
    if(b_bist_test){
        return MEA_OK;
    }
    /* Init IF Parser Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Global CAMx Parser  table ...\n");
 /* Allocate PacketGen Table */
 size = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12_LENGTH * sizeof(MEA_Global_Parser_dbt);
 if (size != 0) {
     MEA_GlobalParser_CAM0_Table = (MEA_Global_Parser_dbt*)MEA_OS_malloc(size);
     if (MEA_GlobalParser_CAM0_Table == NULL) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - Allocate MEA_GlobalParser_CAM0_Table failed (size=%d)\n",
             __FUNCTION__,size);
         return MEA_ERROR;
     }
     MEA_OS_memset(&(MEA_GlobalParser_CAM0_Table[0]),0,size);
 }


 /* Allocate PacketGen Table */
 size = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16_LENGTH * sizeof(MEA_Global_Parser_dbt);
 if (size != 0) {
     MEA_GlobalParser_CAM1_Table = (MEA_Global_Parser_dbt*)MEA_OS_malloc(size);
     if (MEA_GlobalParser_CAM1_Table == NULL) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - Allocate MEA_GlobalParser_CAM1_Table failed (size=%d)\n",
             __FUNCTION__,size);
         return MEA_ERROR;
     }
     MEA_OS_memset(&(MEA_GlobalParser_CAM1_Table[0]),0,size);
 }


 /* Allocate PacketGen Table */
 size = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20_LENGTH * sizeof(MEA_Global_Parser_dbt);
 if (size != 0) {
     MEA_GlobalParser_CAM2_Table = (MEA_Global_Parser_dbt*)MEA_OS_malloc(size);
     if (MEA_GlobalParser_CAM2_Table == NULL) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - Allocate MEA_GlobalParser_CAM2_Table failed (size=%d)\n",
             __FUNCTION__,size);
         return MEA_ERROR;
     }
     MEA_OS_memset(&(MEA_GlobalParser_CAM2_Table[0]),0,size);
 }

 /* Allocate PacketGen Table */
 size = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34_LENGTH * sizeof(MEA_Global_Parser_dbt);
 if (size != 0) {
     MEA_GlobalParser_CAM3_Table = (MEA_Global_Parser_dbt*)MEA_OS_malloc(size);
     if (MEA_GlobalParser_CAM3_Table == NULL) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - Allocate MEA_GlobalParser_CAM3_Table failed (size=%d)\n",
             __FUNCTION__,size);
         return MEA_ERROR;
     }
     MEA_OS_memset(&(MEA_GlobalParser_CAM3_Table[0]),0,size);
 }

 /* Allocate PacketGen Table */
 size = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38_LENGTH * sizeof(MEA_Global_Parser_dbt);
 if (size != 0) {
     MEA_GlobalParser_CAM4_Table = (MEA_Global_Parser_dbt*)MEA_OS_malloc(size);
     if (MEA_GlobalParser_CAM4_Table == NULL) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - Allocate MEA_GlobalParser_CAM4_Table failed (size=%d)\n",
             __FUNCTION__,size);
         return MEA_ERROR;
     }
     MEA_OS_memset(&(MEA_GlobalParser_CAM4_Table[0]),0,size);
 }
/**************************************************/
 MEA_OS_memset(&entry,0,sizeof(entry));

 for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12_LENGTH;i++){
     MEA_OS_memset(&entry,0,sizeof(entry));
     entry.etherType = table_CAM0[i][1];
     entry.valid = table_CAM0[i][0];
     if(MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_12,&entry)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - MEA_API_Set_Global_ParserEntry failed PARSER_OFFSET_38 index %d\n",
             __FUNCTION__,i);
         return MEA_ERROR;
     }
 }

 for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16_LENGTH;i++){
     entry.etherType = table_CAM1[i][1];
     entry.valid = table_CAM1[i][0];
     if(MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_16,&entry)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - MEA_API_Set_Global_ParserEntry failed PARSER_OFFSET_38 index %d\n",
             __FUNCTION__,i);
         return MEA_ERROR;
     }
 }

 MEA_OS_memset(&entry,0,sizeof(entry));

 for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20_LENGTH;i++){
     entry.etherType = table_CAM2[i][1];
     entry.valid = table_CAM2[i][0];
     if(MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_20,&entry)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - MEA_API_Set_Global_ParserEntry failed PARSER_OFFSET_20 index %d\n",
             __FUNCTION__,i);
         return MEA_ERROR;
     }
 }

 MEA_OS_memset(&entry,0,sizeof(entry));
 for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34_LENGTH;i++){
     entry.etherType = table_CAM3[i][1];
     entry.valid = table_CAM3[i][0];
     if(MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_34,&entry)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - MEA_API_Set_Global_ParserEntry failed PARSER_OFFSET_34 index %d\n",
             __FUNCTION__,i);
         return MEA_ERROR;
     }
 }

 MEA_OS_memset(&entry,0,sizeof(entry));
 for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38_LENGTH;i++){
     entry.etherType = table_CAM4[i][1];
     entry.valid = table_CAM4[i][0];
     if(MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_38,&entry)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - MEA_API_Set_Global_ParserEntry failed PARSER_OFFSET_38 index %d\n",
             __FUNCTION__,i);
         return MEA_ERROR;
     }
 }

    return MEA_OK;
}

MEA_Status mea_drv_Renit_Global_parser(MEA_Unit_t            unit)
{
    MEA_Global_Parser_dbt        entry;
    MEA_Uint16 i;
    if(!MEA_NEW_PARSER_SUPPORT){
        return MEA_OK;
    }

    for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12_LENGTH;i++){
        MEA_OS_memcpy(&entry, &MEA_GlobalParser_CAM0_Table[i],sizeof(entry));
        
        if(MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_12,&entry) !=MEA_OK){
            
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  MEA_API_Set_Global_ParserEntry for PARSER_OFFSET_12 failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16_LENGTH;i++){
        MEA_OS_memcpy(&entry, &MEA_GlobalParser_CAM1_Table[i],sizeof(entry));
        
        if(MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_16,&entry) !=MEA_OK){
            
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  MEA_API_Set_Global_ParserEntry for PARSER_OFFSET_16 failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20_LENGTH;i++){
        MEA_OS_memcpy(&entry, &MEA_GlobalParser_CAM2_Table[i],sizeof(entry));
        
        if(MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_20,&entry) !=MEA_OK){
            
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  MEA_API_Set_Global_ParserEntry for PARSER_OFFSET_20 failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }



    for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34_LENGTH;i++){
        MEA_OS_memcpy(&entry, &MEA_GlobalParser_CAM3_Table[i],sizeof(entry));
        
        if(MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_34,&entry) !=MEA_OK){
            
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  MEA_API_Set_Global_ParserEntry for PARSER_OFFSET_34 failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }


    for(i=0; i<ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38_LENGTH;i++){
           MEA_OS_memcpy(&entry, &MEA_GlobalParser_CAM4_Table[i],sizeof(entry));
           
        if (MEA_API_Set_Global_ParserEntry(unit ,i,MEA_INGRESS_PARSER_OFFSET_38,&entry)!=MEA_OK){
           
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s -  MEA_API_Set_Global_ParserEntry for PARSER_OFFSET_38 failed\n",
               __FUNCTION__);
           return MEA_ERROR;
        }
    }

    return MEA_OK;
}

MEA_Status mea_drv_Conclude_Global_parser(MEA_Unit_t            unit)
{
    if(!MEA_NEW_PARSER_SUPPORT){
            return MEA_OK;
    }

    if(MEA_GlobalParser_CAM0_Table){
     MEA_OS_free(MEA_GlobalParser_CAM0_Table);
     MEA_GlobalParser_CAM0_Table=NULL;
    }

    if(MEA_GlobalParser_CAM1_Table){
        MEA_OS_free(MEA_GlobalParser_CAM1_Table);
        MEA_GlobalParser_CAM1_Table=NULL;
    }


    if(MEA_GlobalParser_CAM2_Table){
        MEA_OS_free(MEA_GlobalParser_CAM2_Table);
        MEA_GlobalParser_CAM2_Table=NULL;
    }

    if(MEA_GlobalParser_CAM3_Table){
        MEA_OS_free(MEA_GlobalParser_CAM3_Table);
        MEA_GlobalParser_CAM3_Table=NULL;
    }
    if(MEA_GlobalParser_CAM4_Table){
        MEA_OS_free(MEA_GlobalParser_CAM4_Table);
        MEA_GlobalParser_CAM4_Table=NULL;
    }

   



    return MEA_OK;
}


static void mea_drv_Parser_CAM_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Global_Parser_dbt * entry= (MEA_Global_Parser_dbt * )arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    count_shift=0;

    MEA_OS_insert_value(count_shift,
        16,
        entry->etherType ,
        &val[0]);
    count_shift+=16;

    MEA_OS_insert_value(count_shift,
        1,
        entry->valid ,
        &val[0]);
    count_shift+=1;



    /* Write to Hardware */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(unit_i,MEA_IF_WRITE_DATA_REG(i) , val[i] , MEA_MODULE_IF);
    }

}




MEA_Status MEA_API_Set_Global_ParserEntry(MEA_Unit_t                 unit,
                                          MEA_Uint16                 index,
                                          MEA_Global_Parser_type_t     type,
                                          MEA_Global_Parser_dbt        *entry)
{
    
        MEA_Uint32 max_offset;
        MEA_Uint32 table_num;
        MEA_ind_write_t ind_write;
        MEA_Global_Parser_dbt * pEntryTable;
        MEA_Uint32 size;

        if(!MEA_NEW_PARSER_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - The New parser not support ON FPGA \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        switch (type)
        {
        
        case MEA_INGRESS_PARSER_OFFSET_12 :
             max_offset = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12_LENGTH;
             table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12;
            break;
        case MEA_INGRESS_PARSER_OFFSET_16 :
             max_offset =ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16_LENGTH;
             table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16;
            break;
        case MEA_INGRESS_PARSER_OFFSET_20 :
            max_offset =ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20_LENGTH;
            table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20;
            break;
        case MEA_INGRESS_PARSER_OFFSET_34 :
            max_offset =ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34_LENGTH;
            table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34;
            break;
        case MEA_INGRESS_PARSER_OFFSET_38 :
             max_offset =ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38_LENGTH;
             table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38;
            break;


        case MEA_INGRESS_PARSER_OFFSET_LAST:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - global ParserEntry type not support\n",
                __FUNCTION__);
            return MEA_ERROR;

            break;
        }

        if(index >=max_offset){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - global ParserEntry  index is out of range type %d  index %d max index is %d \n",
                __FUNCTION__,type,index,max_offset);
            return MEA_ERROR;
        }


        switch (type)
        {

        case MEA_INGRESS_PARSER_OFFSET_12 :
          if((mea_GlobalParser_init_flag) && MEA_OS_memcmp(&MEA_GlobalParser_CAM0_Table[index],entry, sizeof(MEA_GlobalParser_CAM0_Table[0])) == 0){
                return MEA_OK; 
          }
          pEntryTable=&MEA_GlobalParser_CAM0_Table[index];
          size=sizeof(MEA_GlobalParser_CAM0_Table[0]);
            break;
        case MEA_INGRESS_PARSER_OFFSET_16 :
            if( (mea_GlobalParser_init_flag) && MEA_OS_memcmp(&MEA_GlobalParser_CAM1_Table[index],entry,sizeof(MEA_GlobalParser_CAM1_Table[0])) == 0){
                return MEA_OK; 
            }
            pEntryTable=&MEA_GlobalParser_CAM1_Table[index];
            size=sizeof(MEA_GlobalParser_CAM1_Table[0]);
            break;
        case MEA_INGRESS_PARSER_OFFSET_20 :
            
                if((mea_GlobalParser_init_flag) && MEA_OS_memcmp(&MEA_GlobalParser_CAM2_Table[index],entry,sizeof(MEA_GlobalParser_CAM2_Table[0])) == 0){
                    return MEA_OK; 
                }
               pEntryTable=&MEA_GlobalParser_CAM2_Table[index];
               size=sizeof(MEA_GlobalParser_CAM2_Table[0]);
            break;
        case MEA_INGRESS_PARSER_OFFSET_34 :
                if( (mea_GlobalParser_init_flag) && MEA_OS_memcmp(&MEA_GlobalParser_CAM3_Table[index],entry,sizeof(MEA_GlobalParser_CAM3_Table[0])) == 0){
                    return MEA_OK; 
                }
            pEntryTable=&MEA_GlobalParser_CAM3_Table[index];
            size=sizeof(MEA_GlobalParser_CAM3_Table[0]);
            break;
        case MEA_INGRESS_PARSER_OFFSET_38 :
                if( (mea_GlobalParser_init_flag) && MEA_OS_memcmp(&MEA_GlobalParser_CAM4_Table[index],entry,sizeof(MEA_GlobalParser_CAM4_Table[0])) == 0){
                    return MEA_OK; 
                }
                pEntryTable=&MEA_GlobalParser_CAM4_Table[index];
                size=sizeof(MEA_GlobalParser_CAM4_Table[0]);
            break;

        case MEA_INGRESS_PARSER_OFFSET_LAST:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - global ParserEntry type not support\n",
                __FUNCTION__);
            return MEA_ERROR;

            break;
        }







        /* build the ind_write value */
        ind_write.tableType		= table_num;
        ind_write.tableOffset	= index;
        ind_write.cmdReg		= MEA_IF_CMD_REG;      
        ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg		= MEA_IF_STATUS_REG;   
        ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_Parser_CAM_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
        //ind_write.funcParam3    = (MEA_Uint32);
        ind_write.funcParam4 = (MEA_funcParam)(entry);
        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }

//update db pEntryTable=&MEA_GlobalParser_CAM3_Table[index];
    MEA_OS_memcpy(pEntryTable,entry,size);

mea_drv_update_Parsing_L2_Key_update(unit);



    
    return MEA_OK;
}


MEA_Status MEA_API_Get_Global_ParserEntry(MEA_Unit_t                 unit,
                                          MEA_Uint16                 index,
                                          MEA_Global_Parser_type_t     type,
                                          MEA_Global_Parser_dbt        *entry)
{

    MEA_Uint32 max_offset;
    MEA_Uint32 table_num=0;

    MEA_Global_Parser_dbt * pEntryTable;
    MEA_Uint32 size=0;


    if(!MEA_NEW_PARSER_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - The New parser not support ON FPGA \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry  = NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    switch (type)
    {
    case MEA_INGRESS_PARSER_OFFSET_12 :
        max_offset = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12_LENGTH;
        table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12;
        break;
    case MEA_INGRESS_PARSER_OFFSET_16 :
        max_offset =ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16_LENGTH;
        table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16;
        break;
    case MEA_INGRESS_PARSER_OFFSET_20 :
        max_offset =ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20_LENGTH;
        table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20;
        break;
    case MEA_INGRESS_PARSER_OFFSET_34 :
        max_offset =ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34_LENGTH;
        table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34;
        break;
    case MEA_INGRESS_PARSER_OFFSET_38 :
        max_offset =ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38_LENGTH;
        table_num = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38;
        break;


    case MEA_INGRESS_PARSER_OFFSET_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - global ParserEntry type not support\n",
            __FUNCTION__);
        return MEA_ERROR;

        break;
    }

    if(index >=max_offset){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - global ParserEntry  index is out of range type %d  index %d max index is %d \n",
            __FUNCTION__,type,index,max_offset);
        return MEA_ERROR;
    }


    switch (type)
    {

    case MEA_INGRESS_PARSER_OFFSET_12 :
        pEntryTable=&MEA_GlobalParser_CAM0_Table[index];
        size=sizeof(MEA_GlobalParser_CAM0_Table[0]);
        break;
    case MEA_INGRESS_PARSER_OFFSET_16 :
        pEntryTable=&MEA_GlobalParser_CAM1_Table[index];
        size=sizeof(MEA_GlobalParser_CAM1_Table[0]);
        break;
    case MEA_INGRESS_PARSER_OFFSET_20 :
        pEntryTable=&MEA_GlobalParser_CAM2_Table[index];
        size=sizeof(MEA_GlobalParser_CAM2_Table[0]);
        break;
    case MEA_INGRESS_PARSER_OFFSET_34 :
        pEntryTable=&MEA_GlobalParser_CAM3_Table[index];
        size=sizeof(MEA_GlobalParser_CAM3_Table[0]);
        break;
    case MEA_INGRESS_PARSER_OFFSET_38 :
        pEntryTable=&MEA_GlobalParser_CAM4_Table[index];
        size=sizeof(MEA_GlobalParser_CAM4_Table[0]);
        break;

    case MEA_INGRESS_PARSER_OFFSET_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - global ParserEntry type not support\n",
            __FUNCTION__);
        return MEA_ERROR;

        break;
    }
    if (size == 0){
//        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"%s - size of the Table is %d  \n", __FUNCTION__, size);
    }
    if (table_num == 0 ){
        //        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"%s - table_num %d  \n", __FUNCTION__, table_num);
    }


    MEA_OS_memcpy(entry,pEntryTable,sizeof(*entry));



    return MEA_OK;
}








MEA_Bool mea_drv_Get_Parsing_L2_Key_Support(MEA_Unit_t unit,MEA_PARSING_L2_KEY_t        type)
{
    if(!(type<=MEA_PARSING_L2_KEY_LAST-1))
        return MEA_FALSE;

    return (MEA_GlobalParser_Table_parsing_L2KEY[type].valid);
}

MEA_Status mea_drv_update_Parsing_L2_Key_update(MEA_Unit_t unit){



MEA_PARSING_L2_KEY_t        i;



for(i=0;i<=MEA_PARSING_L2_KEY_LAST-1 ;i++){
    switch(i){
        case MEA_PARSING_L2_KEY_Untagged:
            MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            break;
        case MEA_PARSING_L2_KEY_Ctag:
            if(MEA_GlobalParser_CAM0_Table[1].valid) {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Ctag_Ctag:
            if(MEA_GlobalParser_CAM0_Table[1].valid && MEA_GlobalParser_CAM1_Table[2].valid) {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Stag:
            if(MEA_GlobalParser_CAM0_Table[0].valid) {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Stag_Ctag:
            if(MEA_GlobalParser_CAM0_Table[0].valid && MEA_GlobalParser_CAM1_Table[2].valid) {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Stag_Stag:
            if(MEA_GlobalParser_CAM0_Table[0].valid && MEA_GlobalParser_CAM1_Table[1].valid) {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Stag_Stag_Ctag:
            if(MEA_GlobalParser_CAM0_Table[0].valid && MEA_GlobalParser_CAM1_Table[1].valid && MEA_GlobalParser_CAM2_Table[0].valid) {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Btag_Itag:
            if(MEA_GlobalParser_CAM0_Table[0].valid && MEA_GlobalParser_CAM1_Table[0].valid ) {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Btag_Itag_Ctag:
            if(MEA_GlobalParser_CAM0_Table[0].valid && MEA_GlobalParser_CAM1_Table[0].valid && MEA_GlobalParser_CAM3_Table[1].valid) {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Btag_Itag_Stag:
            if(MEA_GlobalParser_CAM0_Table[0].valid && MEA_GlobalParser_CAM1_Table[0].valid && MEA_GlobalParser_CAM3_Table[0].valid) {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Btag_Itag_Stag_Ctag:
            if(MEA_GlobalParser_CAM0_Table[0].valid && 
                MEA_GlobalParser_CAM1_Table[0].valid && 
                MEA_GlobalParser_CAM3_Table[0].valid && 
                MEA_GlobalParser_CAM4_Table[0].valid) {
                    MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;

          
        case   MEA_PARSING_L2_KEY_DSA_TAG:
              MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
        break;
        case  MEA_PARSING_L2_KEY_GW :
            MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            break;
        
        case   MEA_PARSING_L2_KEY_RESERV13:
        case   MEA_PARSING_L2_KEY_RESERV14:
        case   MEA_PARSING_L2_KEY_RESERV15:


        #if 0
            if(MEA_GlobalParser_CAM0_Table[4].valid || MEA_GlobalParser_CAM0_Table[5].valid ||
                MEA_GlobalParser_CAM1_Table[6].valid || MEA_GlobalParser_CAM1_Table[6].valid ||
                MEA_GlobalParser_CAM2_Table[3].valid || MEA_GlobalParser_CAM2_Table[4].valid   ) {
                    MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
        #else
            MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
        #endif
            break;    
            /************************************************************************/
            /*   MPLS Protocols                                                     */
            /************************************************************************/            
        case MEA_PARSING_L2_KEY_MPLS_Single_label_no_tag:
        case MEA_PARSING_L2_KEY_MPLS_Two_label_no_tag:
        case MEA_PARSING_L2_KEY_MPLS_Three_label_no_tag:
        case MEA_PARSING_L2_KEY_MPLS_Four_labelno_tag:
        case MEA_PARSING_L2_KEY_MPLS_N_labels_no_tag:
            if(MEA_GlobalParser_CAM0_Table[2].valid || 
                MEA_GlobalParser_CAM0_Table[3].valid ) {
                    MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;

        case MEA_PARSING_L2_KEY_MPLS_Single_label_with_tag:
        case MEA_PARSING_L2_KEY_MPLS_Two_label_with_tag:
        case MEA_PARSING_L2_KEY_MPLS_Three_label_with_tag:
        case MEA_PARSING_L2_KEY_MPLS_N_labels_with_tag:
            if((MEA_GlobalParser_CAM1_Table[3].valid || 
                MEA_GlobalParser_CAM1_Table[4].valid) && MEA_GlobalParser_CAM0_Table[6].valid) {
                    MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;
        case MEA_PARSING_L2_KEY_Reserv24:
            MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            break;

        case MEA_PARSING_L2_KEY_MPLS_Single_label_with_two_tag:
        case MEA_PARSING_L2_KEY_MPLS_Two_label_with_two_tag:
        case MEA_PARSING_L2_KEY_MPLS_N_label_with_two_tag:
            if((MEA_GlobalParser_CAM2_Table[1].valid || 
                MEA_GlobalParser_CAM2_Table[2].valid) && MEA_GlobalParser_CAM0_Table[7].valid && MEA_GlobalParser_CAM1_Table[7].valid) {
                    MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }
            break;


        case MEA_PARSING_L2_KEY_TDM:
            MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            break;

        case MEA_PARSING_L2_KEY_MPLS_not_over_ethernet:
            if((MEA_GlobalParser_CAM2_Table[1].valid || 
                MEA_GlobalParser_CAM2_Table[2].valid) && MEA_GlobalParser_CAM0_Table[7].valid && MEA_GlobalParser_CAM1_Table[7].valid) {
                    MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            } else {
                MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            }

            break;  
        case MEA_PARSING_L2_KEY_Transparent:
            MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            break;
        case MEA_PARSING_L2_KEY_DEF_SID:
            MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_TRUE;
            break; 

        default:
            MEA_GlobalParser_Table_parsing_L2KEY[i].valid = MEA_FALSE;
            break;
    }
}

return MEA_OK;
}

/*----------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------*/

MEA_Status mea_get_Last_if_to_bm_Info(MEA_Unit_t unit, MEA_Uint32 *get_info, mea_if_to_bm_dbt *entry_o)
{

    MEA_Counters_PM_Type_dbt pm_type_entry;
    MEA_db_Hw_Entry_dbt hwEntry;
    MEA_db_HwUnit_t     hwUnit;
    MEA_db_dbt          db;


    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,
        hwUnit,
        return MEA_OK);

    entry_o->src_port = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_SOURCE_PORT_START,
        MEA_EVENT_IFLASTIFTOBM_SOURCE_PORT_WIDTH,
        get_info);
    entry_o->frag_schd_pri = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_FRAG_SCHD_PRI,
        MEA_EVENT_IFLASTIFTOBM_FRAG_SCHD_PRI_WIDTH,
        get_info);
    entry_o->edId_hwId = (MEA_db_HwId_t)MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_HW_EDITID_START,
        MEA_EVENT_IFLASTIFTOBM_HW_EDITID_WIDTH(hwUnit),
        get_info);

    entry_o->proto_type = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_EDIT_PROTOCOL_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_EDIT_PROTOCOL_WIDTH,
        get_info);

    entry_o->llc = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_EDIT_LLC_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_EDIT_LLC_WIDTH,
        get_info);
    entry_o->fragment_enable = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_FRAGMANT_START,
        MEA_EVENT_IFLASTIFTOBM_FRAGMANT_WIDTH,
        get_info);


    entry_o->pmId_hwId = (MEA_db_HwId_t)MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_HW_PM_ID_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_HW_PM_ID_WIDTH(hwUnit),
        get_info);
    entry_o->tmId_hwId = (MEA_db_HwId_t)MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_HW_TM_ID_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_HW_TM_ID_WIDTH(hwUnit),
        get_info);




    if (mea_drv_Get_EHP_ETH_db(MEA_UNIT_0, &db) != MEA_OK) {
       
        return MEA_ERROR;
    }
    if (mea_db_Get_Hw(MEA_UNIT_0,
        db,
        hwUnit,
        entry_o->edId_hwId,
        &hwEntry) != MEA_OK) {
       
        return MEA_ERROR;
    }
    if (hwEntry.valid) {
        entry_o->edId_swId = (MEA_Editing_t)(hwEntry.swId);
    }
    else {
        entry_o->edId_swId = (MEA_Editing_t)MEA_PLAT_GENERATE_NEW_ID;
    }

    if (mea_drv_Get_Policer_db(MEA_UNIT_0, &db) != MEA_OK) {
        
        return MEA_ERROR;
    }

    if (mea_drv_Get_DeviceInfo_NumOfPolicers(MEA_UNIT_0, hwUnit) <= 2) {
        entry_o->tmId_swId = MEA_PLAT_GENERATE_NEW_ID;
    }
    else {
        if (mea_db_Get_Hw(MEA_UNIT_0,
            db,
            hwUnit,
            entry_o->tmId_hwId,
            &hwEntry) != MEA_OK) {
           
            return MEA_ERROR;
        }
        if (hwEntry.valid) {
            entry_o->tmId_swId = (MEA_TmId_t)(hwEntry.swId);
        }
        else {
            entry_o->tmId_swId = MEA_PLAT_GENERATE_NEW_ID;
        }
    }


    for (entry_o->pm_type = 0; entry_o->pm_type<MEA_PM_TYPE_LAST; entry_o->pm_type++) {
        if (mea_drv_Get_Cunters_PM_Type_Entry(MEA_UNIT_0,
            hwUnit,
            entry_o->pm_type,
            &pm_type_entry) != MEA_OK) {
           
            return MEA_ERROR;
        }
        if ((entry_o->pmId_hwId >= pm_type_entry.base_index) &&
            (entry_o->pmId_hwId <  pm_type_entry.base_index + pm_type_entry.max_value)) {
            break;
        }
    }
    if (entry_o->pm_type == MEA_PM_TYPE_LAST) {
        entry_o->pmId_swId = MEA_PLAT_GENERATE_NEW_ID;
    }
    else {
        if (mea_drv_Get_pm_db_ByType(MEA_UNIT_0, &db, entry_o->pm_type) != MEA_OK) {
           
            return MEA_ERROR;
        }
        if (mea_db_Get_Hw(MEA_UNIT_0,
            db,
            hwUnit,
            (MEA_db_HwId_t)(entry_o->pmId_hwId - (MEA_db_HwId_t)pm_type_entry.base_index),
            &hwEntry) != MEA_OK) {
            
            return MEA_ERROR;
        }
        if (hwEntry.valid) {
            entry_o->pmId_swId = (MEA_PmId_t)(hwEntry.swId);
        }
        else {
            entry_o->pmId_swId = MEA_PLAT_GENERATE_NEW_ID;
        }
    }

    entry_o->frag_header = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_FRAG_HEDER_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_FRAG_HEDER_WIDTH(hwUnit),
        get_info);

    entry_o->dfrag = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_DEFRAG_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_DEFRAG_WIDTH(hwUnit),
        get_info);

 

    entry_o->cpu = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_PACKET_TO_CPU_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_PACKET_TO_CPU_WIDTH(hwUnit),
        get_info);

    entry_o->pos = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_POS_OR_ATM_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_POS_OR_ATM_WIDTH(hwUnit),
        get_info);
    entry_o->alr_fcos = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_ALR_FORCE_COS_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_ALR_FORCE_COS_WIDTH(hwUnit),
        get_info);
    entry_o->alr_cos = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_ALR_COS_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_ALR_COS_WIDTH(hwUnit),
        get_info);
    entry_o->psr_cos = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_PARSER_COS_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_PARSER_COS_WIDTH(hwUnit),
        get_info);
    entry_o->alr_fcolor = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_FORCE_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_FORCE_WIDTH(hwUnit),
        get_info);

    entry_o->alr_color = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_WIDTH(hwUnit),
        get_info);
    entry_o->psr_color_awr = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_AWARE_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_AWARE_WIDTH(hwUnit),
        get_info);

    entry_o->psr_color = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_WIDTH(hwUnit),
        get_info);

    entry_o->alr_fl2pri = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_VALID_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_VALID_WIDTH(hwUnit),
        get_info);

    entry_o->alr_l2pri = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_WIDTH(hwUnit),
        get_info);

    entry_o->psr_pri_vld = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_PRIORITY_VALID_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_PRIORITY_VALID_WIDTH(hwUnit),
        get_info);
    entry_o->psr_pri = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_PRIORITY_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_PRIORITY_WIDTH(hwUnit),
        get_info);

    entry_o->port_num = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_PORT_NUMBER_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_PORT_NUMBER_WIDTH(hwUnit),
        get_info);



    entry_o->dest_port = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_DEST_PORT_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_DEST_PORT_WIDTH(hwUnit),
        get_info);



    entry_o->ing_flow_pol = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_ING_FLOW_POLICER_PROF_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_ING_FLOW_POLICER_PROF_WIDTH(hwUnit),
        get_info);


    entry_o->ip_frag_outer            = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_outer_IP_fragmented_START(hwUnit),
                                               MEA_EVENT_IFLASTIFTOBM_outer_IP_fragmented_WIDTH(hwUnit),
                                               get_info);

    entry_o->ip_frag_inner            = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_inner_IP_fragmented_START(hwUnit),
                                               MEA_EVENT_IFLASTIFTOBM_inner_IP_fragmented_WIDTH(hwUnit),
                                               get_info);

    entry_o->pcacket_reassembl_en     = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_pcacket_reassembl_START(hwUnit),
                                                      MEA_EVENT_IFLASTIFTOBM_pcacket_reassembl_WIDTH(hwUnit),
                                                      get_info);

    entry_o->ip_reassembly_session_id  = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_reassembl_sessionID_START(hwUnit),
                                                          MEA_EVENT_IFLASTIFTOBM_reassembl_sessionID_WIDTH(hwUnit),
                                                            get_info);

  
    entry_o->ing_flow_mtu = MEA_OS_read_value(MEA_EVENT_IFLASTIFTOBM_ing_flow_mtu_START(hwUnit),
        MEA_EVENT_IFLASTIFTOBM_ing_flow_mtu_WIDTH(hwUnit),
        get_info);
    


    


















    return MEA_OK;
}




MEA_Status mea_get_Last_Ip_reassembly_last_error(MEA_Unit_t unit, MEA_Uint32 *get_info, mea_ipfrag_reassembly_dbt *entry_o)
{

    entry_o->byte_count = MEA_OS_read_value(0,
        14,
        get_info);

    entry_o->error_code_type = MEA_OS_read_value(14,
        4,
        get_info);

    entry_o->session_id = MEA_OS_read_value(18,
        8,
        get_info);

    entry_o->session_timeout = MEA_OS_read_value(26,
        15,
        get_info);

    entry_o->pm_id = MEA_OS_read_value(42,
        21,
        get_info);

    entry_o->valid_error = MEA_OS_read_value(63,
        1,
        get_info);



    return MEA_OK;
}




MEA_Status mea_get_Last_Reassembly_classifier_event(MEA_Unit_t unit, MEA_Uint8 type, mea_ip_reassembly_lastEvent_dbt *entry_o)
{

    MEA_Uint32          get_info[10];
    MEA_ind_read_t         ind_read;


   // MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Last_Reassembly_classifier_event NOT Support\n"  );
   // return MEA_OK; /*NOT support*/

    if (type == 0) {
        ind_read.cmdReg = MEA_IF_CMD_REG;
        ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
        ind_read.statusReg = MEA_IF_STATUS_REG;
        ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
        ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
        ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        
        
        
        
        ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
        ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_IP_Reassembly_EVENT;
        ind_read.read_data = &(get_info[0]);








        if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(get_info),
            MEA_MODULE_IF,
            globalMemoryMode,
            MEA_MEMORY_READ_IGNORE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
            return MEA_ERROR;
        }
    }
    if (type == 1) {
        ind_read.cmdReg = MEA_IF_CMD_REG;
        ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
        ind_read.statusReg = MEA_IF_STATUS_REG;
        ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
        ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
        ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        
        ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
        ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_IP_Reassembly_UNMACH_EVENT;
        ind_read.read_data = &(get_info[0]);
        if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(get_info),
            MEA_MODULE_IF,
            globalMemoryMode,
            MEA_MEMORY_READ_IGNORE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
            return MEA_ERROR;
        }
    
    }

    /*KEY */
    entry_o->Ip_ID          =  MEA_OS_read_value(0,16,&get_info);
    entry_o->IP_protocol    = MEA_OS_read_value(16, 8, &get_info);
    entry_o->Src_IPV4       = MEA_OS_read_value(16+8, 32, &get_info);
    entry_o->Dst_IPV4       = MEA_OS_read_value(16+8+32, 32, &get_info);

    /**/
   entry_o->session_id    = MEA_OS_read_value(MEA_SESSION_ID_KEY_WIDTH,MEA_NUM_OF_REASSEMBLY_SESSION_ID_WIDTH, get_info);
    entry_o->enet_Src_port = MEA_OS_read_value(MEA_SESSION_ID_KEY_WIDTH+ MEA_NUM_OF_REASSEMBLY_SESSION_ID_WIDTH, (7 + MEA_Platform_GetMaxNumOfPort_BIT()), get_info);


    return MEA_OK;
}