/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_if_regs.h"
#include "mea_action_drv.h"
#include "mea_lxcp_drv.h"
#include "enet_queue_drv.h"
#include "enet_xPermission_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct{
    MEA_LxCP_Protocol_data_dbt data;
    MEA_Uint16                 lxcpActionId;
}MEA_LxCP_Sw_Protocol_data_dbt;

typedef struct{
    MEA_Bool                        valid;
    MEA_Uint32                      num_of_owners;
    MEA_LxCP_Entry_dbt              data;
    MEA_LxCP_Sw_Protocol_data_dbt   protocol[MEA_LXCP_PROTOCOL_LAST];
} MEA_LxCP_dbt;

typedef struct{
    MEA_Bool                    valid;
    MEA_Uint32                  numOfOwners;
    MEA_LxCP_Protocol_Action_te action_type;
    MEA_db_HwUnit_t             hwUnit;
    MEA_Bool                    action_force;
    MEA_Action_t                action_id;
    MEA_Bool                    xPermission_force;
    ENET_xPermissionId_t        xPermission_id;
	MEA_Bool                    WinWhiteList;

} MEA_LxCP_Action_t;



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static MEA_Bool           MEA_LxCP_InitDone     = MEA_FALSE;
static MEA_Uint32         MEA_LxCP_Protocol_Support[MEA_LXCP_PROTOCOL_LAST/32];
static MEA_LxCP_dbt      *MEA_LxCP_Table        = NULL;
static MEA_LxCP_Action_t *MEA_LxCP_Action_Table = NULL;
static MEA_db_dbt         MEA_LxCP_db           = NULL;
static MEA_db_dbt         MEA_LxCP_Protocol_db  = NULL;
static MEA_db_dbt         MEA_LxCP_Action_db    = NULL;



/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
static MEA_Status mea_LxCP_drv_Protocol_set(MEA_Unit_t                          unit,
                                            MEA_LxCp_t                          LxCP_Id_i,
                                            MEA_LxCP_Protocol_key_dbt          *key_pi,
                                            MEA_LxCP_Protocol_data_dbt         *data_pi,
                                            MEA_LxCP_Sw_Protocol_data_dbt      *old_entry_pi);

static MEA_Status mea_Lxcp_drv_LxcpAction_FreePlace(MEA_LxCP_Protocol_Action_te       action_type,
                                                    MEA_Uint16                       *lxcpActionId_o);



static MEA_Status mea_Lxcp_Add_Support_protocol(MEA_LxCP_Protocol_te protocol){
     
        MEA_LxCP_Protocol_Support[protocol/32]|=1<<(protocol%32);

return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Bool mea_lxcp_drv_Profile_IsRange(MEA_LxCp_t id_i){
    
    if((id_i) >= MEA_LXCP_MAX_PROF){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}
static MEA_Status mea_lxcp_drv_Profile_IsExist(MEA_LxCp_t id_i,MEA_Bool *exist){
    
    
    if(exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    (*exist)=MEA_FALSE;
    
    if(id_i>MEA_LXCP_MAX_PROF){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
    return MEA_ERROR;
    }
    
    if(MEA_LxCP_Table[id_i].valid == MEA_TRUE){
    (*exist)=MEA_TRUE;
    }

    return MEA_OK;
}

static void mea_drv_Lxcp_Action_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
        
    MEA_Unit_t         unit_i      = (MEA_Unit_t        )arg1;
    MEA_db_HwUnit_t    hwUnit_i = (MEA_db_HwUnit_t)(long)arg2;
    MEA_db_HwId_t      hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_LxCP_Action_t *entry_pi    = (MEA_LxCP_Action_t*)arg4;
    MEA_Uint32         val[MEA_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION_NUM_OF_REGS(hwUnit)];
    MEA_Uint32         count_shift;
    MEA_Uint32         i;
    MEA_db_HwId_t      action_hwId;
	MEA_Action_t       set_action_id=0;


   for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
       val[i] = 0;
   }

   count_shift=0;
	
   if ((entry_pi->action_force) && 
       (entry_pi->action_id != MEA_ACTION_ID_TO_CPU) ) {

       MEA_DRV_GET_HW_ID(MEA_UNIT_0,
                         mea_drv_Get_Action_db,
                         entry_pi->action_id,
                         hwUnit_i,
                         action_hwId,
                         return ;)

#ifdef MEA_NEW_FWD_ROW_SET
	set_action_id = action_hwId ;
#else
             set_action_id = action_hwId - MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE);
#endif			 


    } else {
        set_action_id = 0;
    }

    MEA_OS_insert_value(count_shift,
		MEA_IF_LXCP_ACTION_H_REG_ACT_ID_WIDTH(unit_i, hwUnit_i),
                        set_action_id,
                        &val[0]);
	count_shift += MEA_IF_LXCP_ACTION_H_REG_ACT_ID_WIDTH(unit_i, hwUnit_i);

    MEA_OS_insert_value(count_shift,
                        MEA_IF_LXCP_ACTION_H_REG_ACT_FORCE_WIDTH,
                        entry_pi->action_force,
                        &val[0]);
    count_shift+=MEA_IF_LXCP_ACTION_H_REG_ACT_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
                        MEA_IF_LXCP_ACTION_H_REG_PER_ID_WIDTH(hwUnit_i),
                        entry_pi->xPermission_id,
                        &val[0]);
    count_shift+=MEA_IF_LXCP_ACTION_H_REG_PER_ID_WIDTH(hwUnit_i);

    MEA_OS_insert_value(count_shift,
                        MEA_IF_LXCP_ACTION_H_REG_PER_FORCE_WIDTH,
                        entry_pi->xPermission_force,
                        &val[0]);
    count_shift+=MEA_IF_LXCP_ACTION_H_REG_PER_FORCE_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_LXCP_WinWhiteList_H_REG_PER_WIDTH,
		entry_pi->WinWhiteList,
		&val[0]);
	count_shift += MEA_IF_LXCP_WinWhiteList_H_REG_PER_WIDTH;


  
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
                         MEA_IF_WRITE_DATA_REG(i),
                         val[i],
                         MEA_MODULE_IF);  
    }

    /* Update Hardware shadow */
    if (mea_db_Set_Hw_Regs(unit_i,
                           MEA_LxCP_Action_db,
                           hwUnit_i,
                           hwId_i,
                           MEA_NUM_OF_ELEMENTS(val),
                           val) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                          __FUNCTION__,
                          hwUnit_i,
                          hwId_i);
    }

    return;
}

static MEA_Status mea_drv_Lxcp_Action_UpdateHw(MEA_Unit_t         unit_i,
                                               MEA_Uint32         lxcpActionId_i,
                                               MEA_LxCP_Action_t *entry_pi,
                                               MEA_LxCP_Action_t *old_entry_pi) 
{

    ENET_ind_write_t ind_write;
    MEA_db_HwUnit_t  hwUnit;
    MEA_db_SwId_t    swId;
    MEA_db_HwId_t    hwId;

    if (old_entry_pi) {
        if (MEA_OS_memcmp(old_entry_pi,
                          entry_pi,
                          sizeof(*old_entry_pi)) == 0) {
            return MEA_OK;
        }
    }


    swId = (MEA_db_SwId_t)lxcpActionId_i;
    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
                                  mea_drv_Get_LxCP_Action_db,
                                  swId,
                                  hwUnit,
                                  hwId,
                                  return MEA_ERROR;)


    /* build the ind_write value */
    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION;
    ind_write.tableOffset   = (MEA_Uint32)hwId;
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg     = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_Lxcp_Action_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)ind_write.tableOffset);
    ind_write.funcParam4 = (MEA_funcParam)entry_pi;


    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - ENET_WriteIndirect tbl=%d mdl=%d failed \n",
                           __FUNCTION__,ind_write.tableType,ENET_MODULE_IF);
        return ENET_ERROR;
    }

    /* Return to caller */
    return MEA_OK;
}

static void mea_drv_Lxcp_Protocol_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
        
    MEA_Unit_t                     unit_i      = (MEA_Unit_t                    )arg1;
    MEA_db_HwUnit_t                hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t                  hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_LxCP_Sw_Protocol_data_dbt* entry_pi    = (MEA_LxCP_Sw_Protocol_data_dbt*)arg4;
    MEA_Uint32                     val[ENET_IF_CMD_PARAM_TBL_TYP_LXCP_NUM_OF_REGS(hwUnit_i)];
    MEA_Uint32                     i;
    MEA_Uint32                     count_shift;
    MEA_db_HwId_t                  lxcpActionId_hwId;
    
    /* Erase all hw entry */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
      val[i] = 0;
    }

    MEA_DRV_GET_HW_ID(unit_i,
                      mea_drv_Get_LxCP_Action_db,
                      entry_pi->lxcpActionId,
                      hwUnit_i,
                      lxcpActionId_hwId,
                      return;)


    /* Build the hw entry */
    count_shift = 0;
    MEA_OS_insert_value(count_shift,
                        MEA_IF_LXCP_PROF_WIDTH(hwUnit_i),
                        (MEA_Uint32)lxcpActionId_hwId, 
                        &val[0]);
    count_shift+=MEA_IF_LXCP_PROF_WIDTH(hwUnit_i);

    /* Write to Hardware */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(unit_i,MEA_IF_WRITE_DATA_REG(0) , val[i] , MEA_MODULE_IF);
    }

    /* Update Hardware shadow */
    if (mea_db_Set_Hw_Regs(unit_i,
                           MEA_LxCP_Protocol_db,
                           hwUnit_i,
                           hwId_i,
                           MEA_NUM_OF_ELEMENTS(val),
                           val) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                          __FUNCTION__,
                          hwUnit_i,
                          hwId_i);
    }

}

static MEA_Status mea_drv_Lxcp_protocol_UpdateHw(MEA_Unit_t                      unit_i,
                                                 MEA_LxCp_t                      LxCp_id_i,
                                                 MEA_LxCP_Protocol_key_dbt*      key_pi,
                                                 MEA_LxCP_Sw_Protocol_data_dbt*  entry_pi,
                                                 MEA_LxCP_Sw_Protocol_data_dbt*  old_entry_pi) 
{


    ENET_ind_write_t ind_write;
    MEA_db_HwUnit_t  hwUnit;
    MEA_db_SwId_t    swId;
    MEA_db_HwId_t    hwId;

    if (old_entry_pi) {
        if (MEA_OS_memcmp(old_entry_pi,
                          entry_pi,
                          sizeof(*old_entry_pi)) == 0) {
            return MEA_OK;
        }
    }


    swId = (MEA_db_SwId_t)((LxCp_id_i*MEA_LXCP_PROTOCOL_LAST)+(key_pi->protocol));
    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
                                  mea_drv_Get_LxCP_Protocol_db,
                                  swId,
                                  hwUnit,
                                  hwId,
                                  return MEA_ERROR;)


    /* build the ind_write value */
    ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_LXCP;
    ind_write.tableOffset	= (MEA_Uint32)hwId;
    ind_write.cmdReg		= MEA_IF_CMD_REG;      
    ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg		= MEA_IF_STATUS_REG;   
    ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_Lxcp_Protocol_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)ind_write.tableOffset);
    ind_write.funcParam4 = (MEA_funcParam)entry_pi;
    


    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - ENET_WriteIndirect tblTyp=%d tblOffset=%d,mdl=%d failed \n",
                           __FUNCTION__,
                           ind_write.tableType,
                           ind_write.tableOffset,
                           ENET_MODULE_IF);
        return ENET_ERROR;
    }

    /* Return to caller */
    return ENET_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_lxcp_drv_Add_owner>                                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_lxcp_drv_Add_owner(MEA_Unit_t  unit,
                                         MEA_LxCp_t  i_LxCPId)
{
    MEA_Bool exist;

    if(mea_lxcp_drv_Profile_IsExist(i_LxCPId,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Profile_IsExist failed  \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    MEA_LxCP_Table[i_LxCPId].num_of_owners++;
    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_lxcp_drv_delete_owner>                                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_lxcp_drv_delete_owner(MEA_Unit_t  unit,
                                            MEA_LxCp_t  i_LxCPId)
{
    
    MEA_Bool exist;
    

	if(i_LxCPId ==MEA_LXCP_PROFILE_ALL_TRANSPARENT) {
       return MEA_OK;
    }

    if(mea_lxcp_drv_Profile_IsExist(i_LxCPId,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Profile_IsExist failed  \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    if(exist==MEA_FALSE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id is net exsit  \n",__FUNCTION__); 
       return MEA_ERROR;
    }
  
    if(MEA_LxCP_Table[i_LxCPId].num_of_owners <= 1){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - invalid num_of_owners (%d) \n",__FUNCTION__,MEA_LxCP_Table[i_LxCPId].num_of_owners); 
       return MEA_ERROR;
    }

    /*decrease owner*/
    MEA_LxCP_Table[i_LxCPId].num_of_owners--;
    
    
    return MEA_OK;
}


MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_LxCP_num_of_sw_size_func,
                                         (MEA_LxCp_t)MEA_LXCP_MAX_PROF,
                                         0)

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_LxCP_num_of_hw_size_func,
                                         ENET_IF_CMD_PARAM_TBL_TYP_LXCP_NUM_OF_PROF(hwUnit_i),
                                         1/* dummy */)

MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_LxCP_db,
                             MEA_LxCP_db) 

MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_LxCP_Protocol_num_of_sw_size_func,
                                         (MEA_Uint16)(MEA_LXCP_MAX_PROF*MEA_LXCP_PROTOCOL_LAST),
                                         0)

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_LxCP_Protocol_num_of_hw_size_func,
                                         ENET_IF_CMD_PARAM_TBL_TYP_LXCP_LENGTH(hwUnit_i),
                                         ENET_IF_CMD_PARAM_TBL_TYP_LXCP_NUM_OF_REGS(hwUnit_i))

MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_LxCP_Protocol_db,
                             MEA_LxCP_Protocol_db) 

MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_LxCP_Action_num_of_sw_size_func,
                                         (MEA_Uint16)(MEA_LXCP_MAX_NUM_OF_LXCP_ACTION),
                                         0)

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_LxCP_Action_num_of_hw_size_func,
                                         ENET_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION_LENGTH(hwUnit_i),
                                         ENET_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION_NUM_OF_REGS(hwUnit_i))

MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_LxCP_Action_db,
                             MEA_LxCP_Action_db) 

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_lxcp_drv_Init>                                             */
/*                                                                          */
/*---------------------------------------------------------------------------*/
MEA_Status mea_lxcp_drv_Init(MEA_Unit_t  unit )
{
    MEA_Uint32 size;
    MEA_db_HwUnit_t    hwUnit;
    MEA_db_HwId_t      hwId;
    MEA_db_SwId_t      swId;
    mea_memory_mode_e  save_globalMemoryMode;
    MEA_LxCP_Protocol_key_dbt key;
    MEA_LxCP_Sw_Protocol_data_dbt lxcp_protocol_entry;
    MEA_db_SwId_t protocol_swId;
    MEA_db_HwId_t protocol_hwId;
    MEA_LxCP_Protocol_data_dbt        data;
 


    if (MEA_LxCP_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - already  init \n",__FUNCTION__);
        return MEA_ERROR;
    }
                         

    MEA_LxCP_InitDone = MEA_FALSE;
    
    MEA_OS_memset(&(MEA_LxCP_Protocol_Support[0]),0,sizeof(MEA_LxCP_Protocol_Support));
    
    /* insert the protocol we are support */
	for (key.protocol=0;key.protocol<=16;key.protocol++) {
         mea_Lxcp_Add_Support_protocol(key.protocol);
	}
    
       
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_BPDU_MMRP);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_BPDU_MVRP);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_BPDU_34_BPDU_47);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_R_APS);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_BFD_CONTROL);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_BFD_ECO_LOOP);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_BFD_ECO_ANALYZER);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_BFD_ECO_SW_MANGE);

    /*
        
    */
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_IS_IS);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_ARP_Replay);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_BGP);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_802_1X);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_PIM);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IN_OSPF);
    /*
    MEA_LXCP_PROTOCOL_LXCP_PROTOCOL_RESERV31
    MEA_LXCP_PROTOCOL_LXCP_PROTOCOL_RESERV32
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_ICMPv6_MLDv2_QUERY);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_ICMPv6_MLDv2_REPORT);
    */
    
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_ARP);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_PPPoE_DISCOVERY);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_PPPoE_ICP_LCP);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IGMPoV4);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_ICMPoV4);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IPV4);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IPV6);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_DHCP_SERVER);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_DHCP_CLIENT);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_DNSoIPv4);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_ICMPoV6);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_RIP);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_DHCP_SERVER_IPv6);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_VSTP);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_CDP_VTP);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_BPDU_TUNNEL);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_CFM_OAM);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IEEE1588_PTP_Event);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IEEE1588_PTP_General);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_PTP_Primary_over_IPv4);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_PTP_Primary_over_IPv6);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_PTP_Delay_over_IPv4);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_PTP_Delay_over_IPv6);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_DHCP_CLIENT_IPv6);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_PPPoE_SESSION);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_VDSL_Vectoring);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_IPSEC);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP);
       
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_L2TPoE);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_DDOS);
    mea_Lxcp_Add_Support_protocol(MEA_LXCP_PROTOCOL_OTHER);
        


    

    
    /***********************************************************************************
    MLD query  :  ETYPE=0x86DD &&  Next_header_L3 = 58  && ICMPv6_message_type =130 
    MLD report  :  ETYPE=0x86DD &&  Next_header_L3 = 58  && ICMPv6_message_type =131 
    MLD done  :  ETYPE=0x86DD &&  Next_header_L3 = 58  && ICMPv6_message_type =132 
    MLDv2 report :  ETYPE=0x86DD &&  Next_header_L3 = 58  && ICMPv6_message_type =143 
    ************************************************************************************/
    
    
    
    /* Init LxCP Action db - Hardware shadow */
    if (mea_db_Init(unit,
                    "LxCP Action",
                    mea_drv_Get_LxCP_Action_num_of_sw_size_func,
                    mea_drv_Get_LxCP_Action_num_of_hw_size_func,
                    &MEA_LxCP_Action_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init (LxCP Action) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }





    /* Init LxCP Protocol db - Hardware shadow */
    if (mea_db_Init(unit,
                    "LxCP Protocol",
                    mea_drv_Get_LxCP_Protocol_num_of_sw_size_func,
                    mea_drv_Get_LxCP_Protocol_num_of_hw_size_func,
                    &MEA_LxCP_Protocol_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init (LxCP Protocol) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Init LxCP (Profile) db */
    if (mea_db_Init(unit,
                    "LxCP",
                    mea_drv_Get_LxCP_num_of_sw_size_func,
                    mea_drv_Get_LxCP_num_of_hw_size_func,
                    &MEA_LxCP_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init (LxCP) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Allocate MEA_LxCP_Action_Table and set to defaults */
    size = MEA_LXCP_MAX_NUM_OF_LXCP_ACTION * sizeof(MEA_LxCP_Action_t);
    MEA_LxCP_Action_Table = (MEA_LxCP_Action_t*)MEA_OS_malloc(size);
    if (MEA_LxCP_Action_Table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate MEA_LxCP_Action_Table failed (size=%d)\n",
                          __FUNCTION__,size);
        return MEA_ERROR;
    }
    MEA_OS_memset((char*)MEA_LxCP_Action_Table,0,size);

    /* Allocate MEA_LxCP_Table and set to defaults */
    size = MEA_LXCP_MAX_PROF * sizeof(MEA_LxCP_dbt);
    MEA_LxCP_Table = (MEA_LxCP_dbt*)MEA_OS_malloc(size);
    if (MEA_LxCP_Table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate MEA_LxCP_Table failed (size=%d)\n",
                          __FUNCTION__,size);
        return MEA_ERROR;
    }
    MEA_OS_memset((char*)MEA_LxCP_Table,0,size);


    /* Add LxCP Action 0 as transparent */
    swId=MEA_LXCP_PROFILE_ALL_TRANSPARENT;
    MEA_OS_memset(&(MEA_LxCP_Action_Table[swId]),0,sizeof(MEA_LxCP_Action_Table[0]));
    MEA_LxCP_Action_Table[swId].valid = MEA_TRUE;
    MEA_LxCP_Action_Table[swId].action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
    MEA_LxCP_Action_Table[swId].numOfOwners = 2;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        MEA_LxCP_Action_Table[swId].hwUnit = hwUnit;
        hwId=0;
        if (mea_db_Create(unit,
                          MEA_LxCP_Action_db,
                          swId,
                          hwUnit,
                          &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Create failed (hwUnit=%d,swId=%d,hwId=%d)\n",
                              __FUNCTION__,
                              hwUnit,
                              swId,
                              hwId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (mea_drv_Lxcp_Action_UpdateHw(unit,
                                         swId,
                                         &(MEA_LxCP_Action_Table[0]),
                                         NULL
                                         ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Lxcp_Action_UpdateHw failed (hwUnit=%d,swId=%d)\n",
                              __FUNCTION__,
                              hwUnit,
                              swId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }


    /* Add LxCP profile 0 (SW/HW) as transparent */
    swId=MEA_LXCP_PROFILE_ALL_TRANSPARENT;

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        hwId=MEA_LXCP_PROFILE_ALL_TRANSPARENT;
        if (mea_db_Create(unit,
                          MEA_LxCP_db,
                          swId,
                          hwUnit,
                          &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Create failed (hwUnit=%d,swId=%d,hwId=%d)\n",
                              __FUNCTION__,
                              hwUnit,
                              swId,
                              hwId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        for(key.protocol=0;
            key.protocol<MEA_LXCP_PROTOCOL_LAST;
            key.protocol++){
            
            protocol_swId = (swId*MEA_LXCP_PROTOCOL_LAST)+key.protocol;
            protocol_hwId = (hwId*MEA_LXCP_PROTOCOL_LAST)+key.protocol;
            if (mea_db_Create(unit,
                              MEA_LxCP_Protocol_db,
                              protocol_swId,
                              hwUnit,
                              &protocol_hwId
                            ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_db_Create MEA_LxCP_Protocol_db failed (hwUnit=%d,swId=%d,hwId=%d)\n",
                                  __FUNCTION__,hwUnit,protocol_swId,protocol_hwId);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;   

            }
            MEA_OS_memset(&lxcp_protocol_entry,0,sizeof(lxcp_protocol_entry));
            lxcp_protocol_entry.data.action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
            lxcp_protocol_entry.lxcpActionId = MEA_LXCP_PROFILE_ALL_TRANSPARENT;
            if (mea_drv_Lxcp_protocol_UpdateHw(unit,
                                               swId,
                                               &key,
                                               &lxcp_protocol_entry,
                                               NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_drv_Lxcp_protocol_UpdateHw failed (swId=%d,protocol=%d)\n",
                                  __FUNCTION__,swId,key.protocol);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;   
            }


        }
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }

    /* Set InitDone */
    MEA_LxCP_InitDone = MEA_TRUE;

   /*******************************************/
   /*    Create  LXCP ALL discard except l2p  */
   /*    user can't delete is for MSTP        */
   /*******************************************/
    {
        MEA_LxCP_Entry_dbt entry;
	    MEA_LxCp_t  LxCPId;
            
        entry.direction=MEA_DIRECTION_GENERAL;
        
        LxCPId=         MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP;
        
        if (MEA_API_Create_LxCP (unit,&entry,&LxCPId)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s  MEA_API_Create_LxCP filed \n",
                                      __FUNCTION__);
            return MEA_ERROR;
        }
        mea_lxcp_drv_Add_owner(unit,LxCPId); 
        
        MEA_OS_memset(&data,0,sizeof(data));
        for(key.protocol=0;key.protocol<MEA_LXCP_PROTOCOL_LAST;key.protocol++){
            if(key.protocol<=32){
                data.action_type=MEA_LXCP_PROTOCOL_ACTION_CPU;
                   
            } else {
                data.action_type=MEA_LXCP_PROTOCOL_ACTION_DISCARD;
            }
            if(MEA_API_Set_LxCP_Protocol (unit,LxCPId,&key,&data)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s  MEA_API_Set_LxCP_Protocol filed \n",
                                      __FUNCTION__);
                return MEA_ERROR;
            }
        }
    
    }


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_lxcp_drv_ReInit>                                             */
/*                                                                          */
/*---------------------------------------------------------------------------*/
MEA_Status mea_lxcp_drv_ReInit(MEA_Unit_t  unit )
{

    /* ReInit LxCP Action table   */
    if (mea_db_ReInit(unit,
                      MEA_LxCP_Action_db,
                      ENET_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION,
                      MEA_MODULE_IF,
                      NULL,
                      MEA_FALSE) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit for LxCP Action failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

    /* ReInit LxCP Protocol table   */
    if (mea_db_ReInit(unit,
                      MEA_LxCP_Protocol_db,
                      ENET_IF_CMD_PARAM_TBL_TYP_LXCP,
                      MEA_MODULE_IF,
                      NULL,
                      MEA_FALSE) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit for LxCP Protocol failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        LxCP                                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_Create_LxCP>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_LxCP    (MEA_Unit_t                           unit,
                                   MEA_LxCP_Entry_dbt                  *pi_entry,
								   MEA_LxCp_t                          *io_LxCPId)
{
    MEA_LxCp_t i;
    MEA_LxCP_Protocol_data_dbt data;
    MEA_LxCP_Protocol_key_dbt  key;
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId,protocol_hwId;
    MEA_db_SwId_t   swId,protocol_swId;
    mea_memory_mode_e save_globalMemoryMode;

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    if(io_LxCPId== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - o_LxCPId == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
#endif    

    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        hwUnit = MEA_HW_UNIT_ID_GENERAL;
    } else {
        switch (pi_entry->direction) {
        case MEA_DIRECTION_GENERAL:
        case MEA_DIRECTION_UPSTREAM:
            hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_DIRECTION_DOWNSTREAM:
            hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        default: 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown direction %d \n",
                              __FUNCTION__,pi_entry->direction);
            return MEA_ERROR;
        }
    }
    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);

    if((*io_LxCPId) == MEA_PLAT_GENERATE_NEW_ID ){
        for (i=1;i<MEA_LXCP_MAX_PROF;i++){
            if(MEA_LxCP_Table[i].valid == MEA_FALSE){
               *io_LxCPId=i;
               break;
            }  
        }
        if (i>= MEA_LXCP_MAX_PROF) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - No available Id \n",__FUNCTION__);
           MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
           return MEA_ERROR;
        }
    } else {
        if(mea_lxcp_drv_Profile_IsRange((*io_LxCPId)) !=MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - No available Id == NULL\n",__FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }   
        if(MEA_LxCP_Table[*io_LxCPId].valid == MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id %d existing\n",__FUNCTION__,*io_LxCPId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;  
        } 
    }


    swId = (MEA_db_SwId_t)(*io_LxCPId);
    hwId = swId;/*MEA_PLAT_GENERATE_NEW_ID;*/
    if (mea_db_Create(unit,
                      MEA_LxCP_db,
                      swId,
                      hwUnit,
                      &hwId
                      ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Create MEA_LxCP_db failed (hwUnit=%d,swId=%d,hwId=%d)\n",
                          __FUNCTION__,hwUnit,swId,hwId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    for(key.protocol=0;
        key.protocol<MEA_LXCP_PROTOCOL_LAST;
        key.protocol++){
            
        protocol_swId = (swId*MEA_LXCP_PROTOCOL_LAST)+key.protocol;
        protocol_hwId = (hwId*MEA_LXCP_PROTOCOL_LAST)+key.protocol;
        if (mea_db_Create(unit,
                          MEA_LxCP_Protocol_db,
                          protocol_swId,
                          hwUnit,
                          &protocol_hwId
                        ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Create MEA_LxCP_Protocol_db failed (hwUnit=%d,swId=%d,hwId=%d)\n",
                              __FUNCTION__,hwUnit,swId,hwId);
            for(protocol_swId--,protocol_hwId--;
                protocol_swId<(swId*MEA_LXCP_PROTOCOL_LAST);
                protocol_swId--,protocol_hwId--) {
                    mea_db_Delete(unit,MEA_LxCP_Protocol_db,protocol_swId,hwUnit);
            }
            mea_db_Delete(unit,MEA_LxCP_db,swId,hwUnit);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }
    

    MEA_LxCP_Table[*io_LxCPId].num_of_owners=1;
    MEA_OS_memcpy(&(MEA_LxCP_Table[*io_LxCPId].data),
                  pi_entry,
                  sizeof(MEA_LxCP_Table[0].data));
    MEA_LxCP_Table[*io_LxCPId].valid=MEA_TRUE;
    
    MEA_OS_memset(&data,0,sizeof(data));

    /* update Protocols to default send to CPU  */
    for(key.protocol=0;
        key.protocol<MEA_LXCP_PROTOCOL_LAST;
        key.protocol++){

			/* The user will need to define each protocol that he want to trap */
			data.action_type=MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
            if(MEA_API_IsSupport_LxCP_Protocol(key.protocol)==MEA_TRUE)
            {
               if(mea_LxCP_drv_Protocol_set(unit,
                                            *io_LxCPId,
                                            &key,
                                            &data,
                                            NULL)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "%s - mea_LxCP_drv_Protocol_set failed \n",__FUNCTION__); 
                    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                  return MEA_ERROR;
               }

            }
    }
    
    

    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
 
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_Delete_LxCP>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_API_Delete_LxCP     (MEA_Unit_t                      unit,
                                     MEA_LxCp_t                      i_LxCPId)
{
    MEA_Bool exist;
    MEA_LxCP_Protocol_key_dbt  key;
    MEA_db_HwUnit_t hwUnit;
    MEA_db_SwId_t   swId,protocol_swId;
    mea_memory_mode_e save_globalMemoryMode;
   

	MEA_API_LOG

    if(i_LxCPId>=MEA_LXCP_MAX_PROF){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
    return MEA_ERROR;
    }

    if (mea_lxcp_drv_Profile_IsExist(i_LxCPId,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_lxcp_drv_Profile_IsExist Failed\n",__FUNCTION__);
        return MEA_ERROR;    
    }
    if(exist!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id %d is not exist \n",__FUNCTION__,i_LxCPId);
        return MEA_ERROR;
    }
    if(MEA_LxCP_Table[i_LxCPId].num_of_owners > 1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - canot delete lxcp Id %d someone used this profile \n",__FUNCTION__,i_LxCPId);
            return MEA_ERROR;
    } 

    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        hwUnit = MEA_HW_UNIT_ID_GENERAL;
    } else {
        switch (MEA_LxCP_Table[i_LxCPId].data.direction) {
        case MEA_DIRECTION_GENERAL:
        case MEA_DIRECTION_UPSTREAM:
            hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_DIRECTION_DOWNSTREAM:
            hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        default: 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown direction %d \n",
                              __FUNCTION__,MEA_LxCP_Table[i_LxCPId].data.direction);
            return MEA_ERROR;
        }
    }
    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);

    /* Set the transparent protocol - use by the delete */
    

    /* update Protocols to default  */
    for(key.protocol=0;
        key.protocol<MEA_LXCP_PROTOCOL_LAST;
        key.protocol++){
            if(MEA_API_IsSupport_LxCP_Protocol(key.protocol))
            {
                if(mea_LxCP_drv_Protocol_set(unit,
                                             i_LxCPId,
                                             &key,
                                             NULL, /* delete */
                                             &(MEA_LxCP_Table[i_LxCPId].protocol[key.protocol])
                                             )!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "%s - mea_LxCP_drv_Protocol_set failed \n",__FUNCTION__); 
                  MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                  return MEA_ERROR;
               }

            }/*protocol support*/
    }

    /* Update SW Shadow */
    MEA_LxCP_Table[i_LxCPId].num_of_owners=0;
    MEA_LxCP_Table[i_LxCPId].valid=MEA_FALSE;


    swId = (MEA_db_SwId_t)(i_LxCPId);
    if (mea_db_Delete(unit,
                      MEA_LxCP_db,
                      swId,
                      hwUnit
                      ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Delete MEA_LxCP_db failed (hwUnit=%d,swId=%d)\n",
                          __FUNCTION__,hwUnit,swId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    for(key.protocol=0;
        key.protocol<MEA_LXCP_PROTOCOL_LAST;
        key.protocol++){
            
        protocol_swId = (swId*MEA_LXCP_PROTOCOL_LAST)+key.protocol;
        if (mea_db_Delete(unit,
                          MEA_LxCP_Protocol_db,
                          protocol_swId,
                          hwUnit
                        ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Dreate MEA_LxCP_Protocol_db failed (hwUnit=%d,swId=%d)\n",
                              __FUNCTION__,hwUnit,swId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }

    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return  MEA_OK;
   
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_Get_LxCP>                                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_API_Get_LxCP     (MEA_Unit_t                      unit,
                                  MEA_LxCp_t                      i_LxCPId,
                                  MEA_LxCP_Entry_dbt            *po_entry)
{
    MEA_Bool exist;
   

	MEA_API_LOG

    if(i_LxCPId>=MEA_LXCP_MAX_PROF){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
    return MEA_ERROR;
    }

    if (mea_lxcp_drv_Profile_IsExist(i_LxCPId,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_lxcp_drv_Profile_IsExist Failed\n",__FUNCTION__);
        return MEA_ERROR;    
    }

    if(exist!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id %d is not exist \n",__FUNCTION__,i_LxCPId);
        return MEA_ERROR;
    }

    if (po_entry) {
        MEA_OS_memcpy(po_entry,
                      &(MEA_LxCP_Table[i_LxCPId].data),
                      sizeof(*po_entry));
    }

    return  MEA_OK;
   
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_GetFirst_LxCP>                                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_LxCP   (MEA_Unit_t          unit,
                                    MEA_LxCp_t         *o_LxCPId,
                                    MEA_LxCP_Entry_dbt *po_entry,
                                    MEA_Bool           *o_found)
{
    MEA_LxCp_t i;

    MEA_API_LOG
    
    for(i=2;
        ((i<MEA_LXCP_MAX_PROF) && (!MEA_LxCP_Table[i].valid)) ; 
        i++);
    
    if (i>=MEA_LXCP_MAX_PROF) {
        if (o_found) {
            *o_found = MEA_FALSE;
        }
        return MEA_OK;
    }

    if (o_found) {
        *o_found=MEA_TRUE;
    }
    if (o_LxCPId) {
        *o_LxCPId=i;
    }
    if (po_entry) {
        MEA_OS_memcpy(po_entry,
                      &(MEA_LxCP_Table[i].data),
                      sizeof(*po_entry));
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_GetNext_LxCP>                                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_LxCP     (MEA_Unit_t          unit,
                                     MEA_LxCp_t         *io_LxCPId , 
                                     MEA_LxCP_Entry_dbt *po_entry,
                                     MEA_Bool           *o_found)
{
    MEA_LxCp_t i;
    

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
     if(io_LxCPId == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - o_LxCPId == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
#endif    
    
    for(i=(*io_LxCPId)+1;
        ((i<MEA_LXCP_MAX_PROF) && (!MEA_LxCP_Table[i].valid)) ; 
        i++);
    
    if (i>=MEA_LXCP_MAX_PROF) {
        if (o_found != NULL) {
            *o_found = MEA_FALSE;
        }
        return MEA_OK;
    }

    if (o_found != NULL) {
        *o_found=MEA_TRUE;
    }
    
    *io_LxCPId=i;
    
    if (po_entry != NULL) {
        MEA_OS_memcpy(po_entry,
                      &(MEA_LxCP_Table[i].data),
                      sizeof(*po_entry));
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_IsExist_LxCP_ById>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_LxCP_ById     (MEA_Unit_t     unit,
                                          MEA_LxCp_t  i_LxCPId, 
                                          MEA_Bool      *exist)
{
	MEA_API_LOG

    return mea_lxcp_drv_Profile_IsExist(i_LxCPId,exist);
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_IsInRange_LxCP>                                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool MEA_API_IsInRange_LxCP(MEA_Unit_t     unit, 
                                MEA_LxCp_t  i_LxCPId)
{
	MEA_API_LOG

    return mea_lxcp_drv_Profile_IsRange(i_LxCPId);
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                        <mea_Lxcp_drv_LxcpAction_search>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Lxcp_drv_LxcpAction_search(MEA_Unit_t                   unit_i,
                                                 MEA_LxCP_Protocol_data_dbt * data_pi,
                                                 MEA_Bool                   * found_o,
                                                 MEA_Uint16                 * lxcpActionId_o)
{
    
    MEA_Uint16 i;
    MEA_db_HwUnit_t hwUnit;
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    if(found_o== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
     if(lxcpActionId_o== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - lxcpActionId_o == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
     
#endif
    
    if (data_pi->action_type == MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT) {
        (*found_o)=MEA_TRUE;
        (*lxcpActionId_o)=0;
        return MEA_OK;
    }

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    (*found_o)=MEA_FALSE;
    for (i=0;i<MEA_LXCP_MAX_NUM_OF_LXCP_ACTION ;i++){
        if ((MEA_LxCP_Action_Table[i].valid       == MEA_TRUE            ) && 
            (MEA_LxCP_Action_Table[i].action_type == data_pi->action_type) && 
            (MEA_LxCP_Action_Table[i].hwUnit      == hwUnit              )  ) {

            switch(data_pi->action_type) {
            case MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT:
            case MEA_LXCP_PROTOCOL_ACTION_CPU:
            case MEA_LXCP_PROTOCOL_ACTION_DISCARD:
                (*found_o)=MEA_TRUE;
                (*lxcpActionId_o)=i;
                return MEA_OK;
                break;

            case MEA_LXCP_PROTOCOL_ACTION_ACTION:

                if ((MEA_LxCP_Action_Table[i].action_force      == data_pi->ActionId_valid) &&
                    (MEA_LxCP_Action_Table[i].action_id         == data_pi->ActionId      ) &&
                    (MEA_LxCP_Action_Table[i].xPermission_force == data_pi->OutPorts_valid)  ) {
                    ENET_xPermission_dbt xPermission;
                    if (enet_Get_xPermission(unit_i,
                                             MEA_LxCP_Action_Table[i].xPermission_id,
                                             &xPermission) != ENET_OK) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s - enet_Get_xPermission %d failed \n",
                                          __FUNCTION__,
                                          MEA_LxCP_Action_Table[i].xPermission_id);
                        return MEA_ERROR;
                    }
                    if (MEA_OS_memcmp(&xPermission,
                                      &(data_pi->OutPorts),
                                      sizeof(xPermission)) == 0) {
                        (*found_o)=MEA_TRUE;
                        (*lxcpActionId_o)=i;
                        return MEA_OK;
                    }
                }
                break;

            case MEA_LXCP_PROTOCOL_ACTION_LAST:
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown action_type %d \n",
                                  __FUNCTION__,data_pi->action_type);
                return MEA_ERROR;
            }
                
        }
    }

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                        <mea_Lxcp_drv_LxcpAction_FreePlace>                */
/*                                                                           */
/*---------------------------------------------------------------------------*/

static MEA_Status mea_Lxcp_drv_LxcpAction_FreePlace(MEA_LxCP_Protocol_Action_te       action_type,
                                               MEA_Uint16                       *lxcpActionId_o)
{
    MEA_Uint16 i;
    
    
    for(i=1;i< MEA_LXCP_MAX_NUM_OF_LXCP_ACTION ;i++){
        if(MEA_LxCP_Action_Table[i].valid == MEA_FALSE){
            *lxcpActionId_o=i;
            return MEA_OK;
        }
    }
    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s lxcp Action occupied\n",__FUNCTION__);
    return MEA_ERROR;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                        <mea_Lxcp_drv_LxcpAction_AddOwner>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Lxcp_drv_LxcpAction_AddOwner(MEA_Unit_t       unit,
                                                   MEA_Uint16       lxcpActionId)
{
    
    if (lxcpActionId == 0) {
        return MEA_OK;
    }

    if(lxcpActionId > MEA_LXCP_MAX_NUM_OF_LXCP_ACTION){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s lxcp Action is out of range\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(MEA_LxCP_Action_Table[lxcpActionId].valid!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s lxcp Action profile %d is not valid\n",__FUNCTION__,lxcpActionId);
        return MEA_ERROR;
    }
    
    MEA_LxCP_Action_Table[lxcpActionId].numOfOwners++;

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                        <mea_Lxcp_drv_LxcpAction_DelOwner>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Lxcp_drv_LxcpAction_DelOwner(MEA_Unit_t           unit_i,
                                               MEA_Uint16               lxcpActionId)
{
    MEA_db_HwUnit_t hwUnit;

    if (lxcpActionId == 0) {
        return MEA_OK;
    }

    if(lxcpActionId > MEA_LXCP_MAX_NUM_OF_LXCP_ACTION){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s lxcp Action is out of range\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if(MEA_LxCP_Action_Table[lxcpActionId].valid !=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s lxcp Action is not valid\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_LxCP_Action_Table[lxcpActionId].numOfOwners > 1) {
        MEA_LxCP_Action_Table[lxcpActionId].numOfOwners--;
        return MEA_OK;
    }

    if (MEA_LxCP_Action_Table[lxcpActionId].xPermission_force) {
        if (enet_Delete_xPermission(unit_i,
                                    MEA_LxCP_Action_Table[lxcpActionId].xPermission_id)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_Delete_xPermission %d failed (lxcpActionId=%d)\n",
                              __FUNCTION__,
                              MEA_LxCP_Action_Table[lxcpActionId].xPermission_id);
            return MEA_ERROR;
        }
        MEA_LxCP_Action_Table[lxcpActionId].xPermission_force=MEA_FALSE;
    }

    if (MEA_LxCP_Action_Table[lxcpActionId].action_force) {
        if (MEA_API_DelOwner_Action(unit_i,MEA_LxCP_Action_Table[lxcpActionId].action_id)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_DelOwner_Action  %d failed (lxcpActionId=%d)\n",
                              __FUNCTION__,
                              MEA_LxCP_Action_Table[lxcpActionId].action_id,
                              lxcpActionId); 
            return MEA_ERROR;
        }
        MEA_LxCP_Action_Table[lxcpActionId].action_force=MEA_FALSE;
    }

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)
    if (mea_db_Delete(unit_i,
                      MEA_LxCP_Action_db,
                      (MEA_db_SwId_t)lxcpActionId,
                      hwUnit) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_DelOwner_Action  %d failed (lxcpActionId=%d)\n",
                          __FUNCTION__,
                          MEA_LxCP_Action_Table[lxcpActionId].action_id,
                          lxcpActionId); 
        return MEA_ERROR;
    }

    MEA_OS_memset(&MEA_LxCP_Action_Table[lxcpActionId],0,sizeof(MEA_LxCP_Action_Table[0]));  
    MEA_LxCP_Action_Table[lxcpActionId].valid=MEA_FALSE;
         
    return MEA_OK;
 }



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                        <mea_Lxcp_drv_LxcpAction_Create>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Lxcp_drv_LxcpAction_Create(MEA_Unit_t                          unit,
                                                 MEA_LxCP_Sw_Protocol_data_dbt    *data_pio)
{
    MEA_Uint16                  lxcpActionId;
    MEA_LxCP_Action_t           entry_LxcpAction;
    ENET_xPermission_dbt        xPermission;
    MEA_db_HwUnit_t             hwUnit;
    MEA_db_HwId_t               hwId;

    MEA_DRV_GET_HW_UNIT(unit,hwUnit,return MEA_ERROR;)

    /* find lxcp Action free space*/
        if( mea_Lxcp_drv_LxcpAction_FreePlace(data_pio->data.action_type,&lxcpActionId)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s no free lxcp Action\n",__FUNCTION__);
            return MEA_ERROR;
    }

    hwId = MEA_PLAT_GENERATE_NEW_ID;
    if (mea_db_Create(unit,
                      MEA_LxCP_Action_db,
                      (MEA_db_SwId_t)lxcpActionId,
                      hwUnit,
                      &hwId) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Create for LxCP_Action failed "
                          "(hwUnit=%d,swId=%d)\n",
                          __FUNCTION__,
                          hwUnit,
                          lxcpActionId);
        return MEA_ERROR;
    }

    /* Build entry_LxcpAction */
    MEA_OS_memset(&entry_LxcpAction,0,sizeof(entry_LxcpAction));
    entry_LxcpAction.valid       = MEA_TRUE;
    entry_LxcpAction.numOfOwners = 1;
    entry_LxcpAction.action_type = data_pio->data.action_type;
    entry_LxcpAction.hwUnit      = hwUnit;
	entry_LxcpAction.WinWhiteList = data_pio->data.WinWhiteList;


    switch (entry_LxcpAction.action_type) {
    case MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT:
        entry_LxcpAction.action_force      = MEA_FALSE;
        entry_LxcpAction.xPermission_force = MEA_FALSE;
        break;
    case MEA_LXCP_PROTOCOL_ACTION_CPU:
        if (globalMemoryMode == MEA_MODE_DOWNSTREEM_ONLY) {
            entry_LxcpAction.action_id         = (MEA_Action_t) MEA_ACTION_ID_TO_CPU_DOWNSTREAM;
        } else {
            entry_LxcpAction.action_id         = (MEA_Action_t) MEA_ACTION_ID_TO_CPU;
        }
        entry_LxcpAction.action_force      = MEA_TRUE;
        entry_LxcpAction.xPermission_force = MEA_TRUE;
		entry_LxcpAction.WinWhiteList = MEA_TRUE;
        MEA_OS_memset(&xPermission,0,sizeof(xPermission));
        MEA_SET_OUTPORT(&xPermission,MEA_CPU_PORT);
        break;
    case MEA_LXCP_PROTOCOL_ACTION_DISCARD:
        entry_LxcpAction.action_force      = MEA_FALSE;
        entry_LxcpAction.xPermission_force = MEA_TRUE;
        MEA_OS_memset(&xPermission,0,sizeof(xPermission));
        break;
    case MEA_LXCP_PROTOCOL_ACTION_ACTION:
        entry_LxcpAction.action_force      = data_pio->data.ActionId_valid;
        entry_LxcpAction.action_id         = data_pio->data.ActionId;
        entry_LxcpAction.xPermission_force = data_pio->data.OutPorts_valid;
        MEA_OS_memcpy(&xPermission,&(data_pio->data.OutPorts),sizeof(xPermission));
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown action type %d \n",
                          __FUNCTION__,
                          entry_LxcpAction.action_type);
        mea_db_Delete(unit,MEA_LxCP_Action_db,(MEA_db_SwId_t)lxcpActionId,hwUnit);
        return MEA_ERROR;
    }

    /* Add owner to the actionId - if needed */
    if (entry_LxcpAction.action_force) {
        if (MEA_API_AddOwner_Action(unit,entry_LxcpAction.action_id)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_AddOwner_Action failed for ActionId %d \n",
                              __FUNCTION__,
                              entry_LxcpAction.action_id);
            mea_db_Delete(unit,MEA_LxCP_Action_db,(MEA_db_SwId_t)lxcpActionId,hwUnit);
            return MEA_ERROR;
        }
    }

    /* Create the xPermission - if needed */
    if (entry_LxcpAction.xPermission_force) {
        if (! MEA_API_Get_Warm_Restart())
            entry_LxcpAction.xPermission_id=ENET_PLAT_GENERATE_NEW_ID;
        else
            entry_LxcpAction.xPermission_id = data_pio->data.save_hw_xPermision; 

        
        if (enet_Create_xPermission(MEA_UNIT_0,
                                    &xPermission,
                                    0,
                                    &(entry_LxcpAction.xPermission_id)
                                    ) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_Create_xPermission for DISCARD failed \n",
                              __FUNCTION__);
            if (entry_LxcpAction.action_force     ) {
                MEA_API_DelOwner_Action(unit,entry_LxcpAction.action_id);
            }
            mea_db_Delete(unit,MEA_LxCP_Action_db,(MEA_db_SwId_t)lxcpActionId,hwUnit);
            return MEA_ERROR;
        }
         data_pio->data.save_hw_xPermision = entry_LxcpAction.xPermission_id ; 
    }



    /* Update Hardware */
    if(mea_drv_Lxcp_Action_UpdateHw(unit, 
                                    lxcpActionId,
                                    &entry_LxcpAction,
                                    NULL)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_drv_Lxcp_Action_UpdateHw failed\n",
                                 __FUNCTION__);
        if (entry_LxcpAction.action_force     ) {
            MEA_API_DelOwner_Action(unit,entry_LxcpAction.action_id);
        }
        if (entry_LxcpAction.xPermission_force) {
            enet_Delete_xPermission(unit,entry_LxcpAction.xPermission_id);
            data_pio->data.save_hw_xPermision=0;
        }
        mea_db_Delete(unit,MEA_LxCP_Action_db,(MEA_db_SwId_t)lxcpActionId,hwUnit);
        return MEA_ERROR;
    }

    /* Update Software Shadow */
    MEA_OS_memcpy(&(MEA_LxCP_Action_Table[lxcpActionId]),
                  &entry_LxcpAction,
                  sizeof(MEA_LxCP_Action_Table[0]));

    /* Update output parameters */
    data_pio->lxcpActionId=lxcpActionId;

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                        <mea_Lxcp_drv_LxcpAction_Delete>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Lxcp_drv_LxcpAction_Delete(MEA_Unit_t                          unit,
                                                   MEA_LxCp_t                        LxCP_Id_i,
                                                   MEA_LxCP_Protocol_key_dbt        *key_pi)

{
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    
     if(key_pi== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key_pi == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
#endif

     if(MEA_LxCP_Table[LxCP_Id_i].valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_LxCP_Table %d is not valid to delete \n",__FUNCTION__,LxCP_Id_i);
        return MEA_ERROR;
     }
     
     
     if(mea_Lxcp_drv_LxcpAction_DelOwner(unit,
                                        (MEA_LxCP_Table[LxCP_Id_i].protocol[key_pi->protocol].lxcpActionId))!=
                                        MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_Lxcp_drv_LxcpAction_DelOwner %d failed \n",
             __FUNCTION__,LxCP_Id_i);
        return MEA_ERROR;
     }
    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        LxCP_Protocol's                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                        <mea_LxCP_drv_Protocol_set>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_LxCP_drv_Protocol_set(MEA_Unit_t                         unit,
                                           MEA_LxCp_t                          LxCP_Id_i,
                                           MEA_LxCP_Protocol_key_dbt          *key_pi,
                                           MEA_LxCP_Protocol_data_dbt         *data_pi,
                                           MEA_LxCP_Sw_Protocol_data_dbt      *old_entry_pi)
{
    
    
    MEA_Bool                              found_Lxcp_ActionId;
    MEA_LxCP_Sw_Protocol_data_dbt         entry,save_old_entry;
    

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    
     if(key_pi== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key_pi == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
#endif

    /* Check if need to delete the lxcp action */
    if (data_pi== NULL) {
        if(mea_Lxcp_drv_LxcpAction_Delete(unit,LxCP_Id_i,key_pi) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_Lxcp_drv_LxcpAction_Delete failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }
        return MEA_OK;
    }

    /* Build the entry */
    MEA_OS_memset(&entry,0,sizeof(entry));
    MEA_OS_memcpy(&entry.data,data_pi,sizeof(entry.data));
    if (mea_Lxcp_drv_LxcpAction_search(unit,&entry.data,&found_Lxcp_ActionId,&entry.lxcpActionId) !=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Lxcp_Action_search failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }


    if (old_entry_pi) {
        if (MEA_OS_memcmp(old_entry_pi,&entry,sizeof(*old_entry_pi)) == 0){
            return MEA_OK; /* Do Nothing */
        }
        MEA_OS_memcpy(&save_old_entry,old_entry_pi,sizeof(save_old_entry));
    }



    if (found_Lxcp_ActionId) {
        /* Add owner to the LxCP Action profile */
        if (mea_Lxcp_drv_LxcpAction_AddOwner(unit,entry.lxcpActionId)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_Lxcp_drv_LxcpAction_AddOwner failed (lxcpActionId=%d)\n",
                              __FUNCTION__,entry.lxcpActionId);
            return MEA_ERROR;
        }

        /* if there was old entry delete owner for the old entry LxCP Action Profile */
        if (old_entry_pi) {
            if (mea_Lxcp_drv_LxcpAction_DelOwner(unit,save_old_entry.lxcpActionId)!=  MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_Lxcp_drv_LxcpAction_DelOwner %d failed \n",
                                  __FUNCTION__,
                                  old_entry_pi->lxcpActionId);
                mea_Lxcp_drv_LxcpAction_DelOwner(unit,entry.lxcpActionId);
                return MEA_ERROR;
            }
        }
    } else {
        if (old_entry_pi) {
            if (mea_Lxcp_drv_LxcpAction_DelOwner(unit,old_entry_pi->lxcpActionId)!=  MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_Lxcp_drv_LxcpAction_DelOwner %d failed \n",
                                  __FUNCTION__,
                                  old_entry_pi->lxcpActionId);
                return MEA_ERROR;
            }
        }
        if (mea_Lxcp_drv_LxcpAction_Create(unit,&entry)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_Lxcp_drv_LxcpAction_Create failed\n",
                              __FUNCTION__);
            if (old_entry_pi) {
                if (MEA_LxCP_Action_Table[save_old_entry.lxcpActionId].valid) {
                    mea_Lxcp_drv_LxcpAction_AddOwner(unit,save_old_entry.lxcpActionId);
                } else {
                    mea_Lxcp_drv_LxcpAction_Create(unit,&save_old_entry);
                }
            }    
            return MEA_ERROR;   
        }
    }

    /* Update LxCP Protocol Hardware table */
    if (mea_drv_Lxcp_protocol_UpdateHw(unit,
                                       LxCP_Id_i,
                                       key_pi,
                                       &entry,
                                       old_entry_pi) != MEA_OK) {
        mea_Lxcp_drv_LxcpAction_DelOwner(unit,entry.lxcpActionId);
        if (old_entry_pi) {
            if (MEA_LxCP_Action_Table[save_old_entry.lxcpActionId].valid) {
                mea_Lxcp_drv_LxcpAction_AddOwner(unit,save_old_entry.lxcpActionId);
            } else {
                mea_Lxcp_drv_LxcpAction_Create(unit,&save_old_entry);
            }
        }    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_lxcp_protocol_UpdateHw Failed \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    

    /* Update Software shadow */
    MEA_OS_memcpy (&(MEA_LxCP_Table[LxCP_Id_i].protocol[key_pi->protocol]),
                   &entry,
                   sizeof(MEA_LxCP_Table[0].protocol[0])); ///


    /* Return to caller */
    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_Set_LxCP_Protocol>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_LxCP_Protocol (MEA_Unit_t                          unit,
 								      MEA_LxCp_t                          LxCP_Id_i,
								      MEA_LxCP_Protocol_key_dbt          *key_pi,
								      MEA_LxCP_Protocol_data_dbt         *data_pi)
{
    MEA_Bool exist;
    MEA_Uint32      ActionDirection;
    MEA_db_HwUnit_t hwUnit;
    mea_memory_mode_e save_globalMemoryMode;

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    if(data_pi== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - data_pi == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
     if(key_pi== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key_pi == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
    if((globalMemoryMode != MEA_MODE_REGULAR_ENET3000) && (MEA_subSysType == MEA_SUB_SYS_TYPE_ENET4000_QXGMII) &&
         (data_pi->action_type == MEA_LXCP_PROTOCOL_ACTION_ACTION)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - not support  MEA_LxCP_PROTOCOL_ACTION_ACTION \n",__FUNCTION__);
        return MEA_ERROR;
    }

#endif /*  MEA_SW_CHECK_INPUT_PARAMETERS  */
    
    if(mea_lxcp_drv_Profile_IsExist(LxCP_Id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Profile_IsExist failed  \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    if(!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id %d is not exist  \n",__FUNCTION__,LxCP_Id_i); 
        return MEA_ERROR;
    }

    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        hwUnit = MEA_HW_UNIT_ID_GENERAL;
    } else {
        switch (MEA_LxCP_Table[LxCP_Id_i].data.direction) {
        case MEA_DIRECTION_GENERAL:
        case MEA_DIRECTION_UPSTREAM:
            hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_DIRECTION_DOWNSTREAM:
            hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        default: 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown direction %d \n",
                              __FUNCTION__,MEA_LxCP_Table[LxCP_Id_i].data.direction);
            return MEA_ERROR;
        }
    }
    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
   

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 

    /* Check valid action_type and associate parameters */
    switch (data_pi->action_type) {
    case MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT:
    case MEA_LXCP_PROTOCOL_ACTION_CPU:
    case MEA_LXCP_PROTOCOL_ACTION_DISCARD:
        if (data_pi->ActionId_valid) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ActionId_valid not allowed for action_type %d "
                              "(LxCP_Id_i=%d,key_pi->protocol=%d)\n",
                              __FUNCTION__,
                              data_pi->action_type,
                              LxCP_Id_i,
                              key_pi->protocol);
            return MEA_ERROR;
        }
        if (data_pi->OutPorts_valid) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - OutPorts_valid not allowed for action_type %d "
                              "(LxCP_Id_i=%d,key_pi->protocol=%d)\n",
                              __FUNCTION__,
                              data_pi->action_type,
                              LxCP_Id_i,
                              key_pi->protocol);
            return MEA_ERROR;
        }
        break;
    case MEA_LXCP_PROTOCOL_ACTION_ACTION:
        if ((!(data_pi->ActionId_valid))  && (!(data_pi->OutPorts_valid))) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ActionId_valid== FALSE and OutPorts_valid==FALSE "
                              "not allowed for action_type %d "
                              "(LxCP_Id_i=%d,key_pi->protcol=%d)\n",
                              __FUNCTION__,
                              data_pi->action_type,
                              LxCP_Id_i,
                              key_pi->protocol);
            return MEA_ERROR;
        }
        if (data_pi->ActionId_valid) {
            if (mea_Action_GetDirection (unit,
                                         data_pi->ActionId,
                                         &ActionDirection) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_Action_GetDirection for actionId %d failed "
                                  "(LxCP_Id_i=%d,key_pi->protcol=%d)\n",
                                  __FUNCTION__,
                                  data_pi->ActionId,
                                  LxCP_Id_i,
                                  key_pi->protocol);
                return MEA_ERROR;
            }
            if (ActionDirection != MEA_LxCP_Table[LxCP_Id_i].data.direction) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - direction %d for actionId %d not match LxCP direction "
                                  "(LxCP_Id_i=%d,key_pi->protocol=%d)\n",
                                  __FUNCTION__,
                                  ActionDirection,
                                  data_pi->ActionId,
                                  LxCP_Id_i,
                                  key_pi->protocol);
                return MEA_ERROR;
            }
        }
        if (data_pi->OutPorts_valid) {
            MEA_Port_t  outport;

            MEA_OUTPORT_FOR_LOOP((&(data_pi->OutPorts)),outport) {
                if (ENET_IsValid_Queue(unit,outport,ENET_FALSE)==ENET_FALSE) { 
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - out port %d is invalid "
                                      "(LxCP_Id_i=%d,key_pi->protocol=%d)\n",
                                      __FUNCTION__,
                                      outport,
                                      LxCP_Id_i,
                                      key_pi->protocol);
                    return MEA_ERROR;
                }
            }
        }
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown action_type %d (LxCP_Id=%d,protocol=%d)\n",
                          __FUNCTION__,
                          data_pi->action_type,
                          LxCP_Id_i,
                          key_pi->protocol);
        return MEA_ERROR;
    } 

#endif /*  MEA_SW_CHECK_INPUT_PARAMETERS  */

    
    if (mea_LxCP_drv_Protocol_set(unit,
                                  LxCP_Id_i,
                                  key_pi,
                                  data_pi,
                                  &(MEA_LxCP_Table[LxCP_Id_i].protocol[key_pi->protocol])
                                 )!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_lxcp_protocol_UpdateHw Failed \n",__FUNCTION__); 
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_Get_LxCP_Protocol>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_API_Get_LxCP_Protocol (MEA_Unit_t                      unit,
                                       MEA_LxCp_t                      LxCP_Id_i,
								       MEA_LxCP_Protocol_key_dbt      *key_pi,
								       MEA_LxCP_Protocol_data_dbt     *data_po)
{
 MEA_Bool exist;
    
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    if(data_po== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - data_po == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
     if(key_pi== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key_pi == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
#endif 

    if(mea_lxcp_drv_Profile_IsExist(LxCP_Id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Profile_IsExist failed  \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    if(!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id %d is not exist  \n",__FUNCTION__,LxCP_Id_i); 
        return MEA_ERROR;
    }
   
    if(MEA_API_IsSupport_LxCP_Protocol(key_pi->protocol)==MEA_FALSE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - protocol %d is not Support  \n",__FUNCTION__,LxCP_Id_i,key_pi->protocol); 
        return MEA_ERROR;
    }  
     
    MEA_OS_memcpy(data_po,&MEA_LxCP_Table[LxCP_Id_i].protocol[key_pi->protocol].data ,sizeof(*data_po));
    
    
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_GetFirst_LxCP_Protocol>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_LxCP_Protocol (MEA_Unit_t                  unit_i,
                                           MEA_LxCp_t                  LxCP_id_i,
                                           MEA_LxCP_Protocol_key_dbt  *key_po,
                                           MEA_LxCP_Protocol_data_dbt *data_po,
                                           MEA_Bool                   *found_o)
{
 
    MEA_Bool exist;

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS   
   if(data_po== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - data_po == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
     if(key_po== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key_po == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    } 
     if(found_o== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    } 
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/   
    
    (*found_o)=MEA_FALSE;

    if(mea_lxcp_drv_Profile_IsExist(LxCP_id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Profile_IsExist failed  \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    if(!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id %d is not exist  \n",__FUNCTION__,LxCP_id_i); 
        return MEA_ERROR;
    }
     
     for(key_po->protocol=0;
        key_po->protocol<MEA_LXCP_PROTOCOL_LAST;
        key_po->protocol++){
            if(MEA_API_IsSupport_LxCP_Protocol(key_po->protocol)==MEA_TRUE)
            {
                (*found_o)=MEA_TRUE;
                return MEA_OK;
            }
    }
    
    (*found_o)=MEA_FALSE;
    return MEA_ERROR;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_GetNext_LxCP_Protocol>                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_LxCP_Protocol  (MEA_Unit_t                  unit_i,
                                           MEA_LxCp_t                  LxCP_Id_i,
                                           MEA_LxCP_Protocol_key_dbt  *key_po,
                                           MEA_LxCP_Protocol_data_dbt *data_po,
                                           MEA_Bool                   *found_o)
{
 
MEA_Bool exist;

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS   
   if(data_po== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - data_po == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
     if(key_po== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key_po == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    } 
     if(found_o== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    } 
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/   

    (*found_o)=MEA_FALSE;

    if(mea_lxcp_drv_Profile_IsExist(LxCP_Id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Profile_IsExist failed  \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    if(!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id %d is not exist  \n",__FUNCTION__,LxCP_Id_i); 
        return MEA_ERROR;
    }
     
     for(key_po->protocol++;
        key_po->protocol<MEA_LXCP_PROTOCOL_LAST;
        key_po->protocol++){
            if(MEA_API_IsSupport_LxCP_Protocol(key_po->protocol)==MEA_TRUE)
            {
                (*found_o)=MEA_TRUE;
                return MEA_OK;
            }
    }
    
    (*found_o)=MEA_FALSE;
    return MEA_ERROR;    
     
     
     
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <MEA_API_IsSupport_LxCP_Protocol>                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool  MEA_API_IsSupport_LxCP_Protocol (MEA_LxCP_Protocol_te protocol_i)
{

	MEA_API_LOG
    if(protocol_i >=MEA_LXCP_PROTOCOL_LAST){
      return MEA_FALSE;
    }
    if ((protocol_i/32) > MEA_NUM_OF_ELEMENTS(MEA_LxCP_Protocol_Support)) {
       return MEA_FALSE;
    }

    if (!(MEA_LxCP_Protocol_Support[protocol_i/32] & (0x00000001<<(protocol_i%32)))) {
        return MEA_FALSE;
    }
    return MEA_TRUE;
    
}

MEA_Status mea_lxcp_drv_Conclude(MEA_Unit_t unit)
{
   MEA_LxcpAction_t LxcpAction;
	

#if 1
    
        mea_lxcp_drv_delete_owner(unit, MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP);
        MEA_API_Delete_LxCP(unit, MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP);
  
#endif
	/* scan all Lxcp and delete one by one */
	 for (LxcpAction=0;LxcpAction<MEA_LXCP_MAX_NUM_OF_LXCP_ACTION;LxcpAction++) {
         if(!MEA_LxCP_Action_Table[LxcpAction].valid){
           continue; 
         }
         if (MEA_LxCP_Action_Table[LxcpAction].xPermission_force) {
             if (enet_Delete_xPermission(unit,MEA_LxCP_Action_Table[LxcpAction].xPermission_id)!=MEA_OK){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s - enet_Delete_xPermission  failed \n",
                                   __FUNCTION__,
                                   MEA_LxCP_Action_Table[LxcpAction].xPermission_id); 
                 return MEA_ERROR;
             }
         }
         if (MEA_LxCP_Action_Table[LxcpAction].action_force) {
            
             if(MEA_API_DelOwner_Action(unit,MEA_LxCP_Action_Table[LxcpAction].action_id)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,\
                                  "%s - MEA_API_DelOwner_Action  failed \n",
                                  __FUNCTION__,
                                  MEA_LxCP_Action_Table[LxcpAction].xPermission_id); 
                return MEA_ERROR;
             }
         }
    }

     if (MEA_LxCP_Table!= NULL) {
         MEA_OS_free(MEA_LxCP_Table);
         MEA_LxCP_Table = NULL;
     }

     if (MEA_LxCP_Action_Table!= NULL) {
         MEA_OS_free(MEA_LxCP_Action_Table);
         MEA_LxCP_Action_Table = NULL;
     }

    if (mea_db_Conclude(unit,&MEA_LxCP_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Conclude (LxCP) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_db_Conclude(unit,&MEA_LxCP_Protocol_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Conclude (LxCP Protocol) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_db_Conclude(unit,&MEA_LxCP_Action_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Conclude (LxCP Action) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    return ENET_OK; 
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Lxcp_drv_IsOutPortOf_LxCP_Protocol>                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool mea_Lxcp_drv_IsOutPortOf_LxCP_Protocol(MEA_Unit_t                  unit_i, 
                                           MEA_LxCp_t                  LxCP_Id_i,
                                           MEA_LxCP_Protocol_key_dbt  *key_pi,
                                           MEA_Port_t                  outPort_i)
{

    MEA_Uint32 *pPortGrp;
    MEA_Uint32  shiftWord= 0x00000001;
   

    MEA_API_LOG

    
    if(!ENET_IsValid_Queue(unit_i,outPort_i,ENET_FALSE)) return MEA_ERROR;


    pPortGrp=(MEA_Uint32 *)(&(MEA_LxCP_Table[LxCP_Id_i].protocol[key_pi->protocol].data.OutPorts.out_ports_0_31));
    pPortGrp +=(outPort_i/32);

    if(!(*pPortGrp & (shiftWord<<(outPort_i%32)))) {
       return MEA_FALSE;
    } else {
        return MEA_TRUE;
    }

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_API_Delete_all_LxCPs>                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_all_LxCPs(MEA_Unit_t                  unit) 
{

   MEA_LxCp_t id;
   MEA_LxCp_t temp_id;
   MEA_Bool   found;


   /* Get the first filter */
   if (MEA_API_GetFirst_LxCP(unit,&id, NULL,&found) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - failed to GetFirst action \n",
                          __FUNCTION__);
        return MEA_ERROR;
   }
 
   /* loop until no more lxcp profiles */
   while (found) {

       /* Save the id that need to be delete , before the getNext */
       temp_id = id;
       
            
       /* Get the next LxCP Profile */
       if (MEA_API_GetNext_LxCP(unit,
                                &id,
                                NULL,
                                &found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to GetNext LxCP %d\n",
                             __FUNCTION__,id);
            return MEA_ERROR;
       }

       /* Delete the save LxCP Profile */
       if (MEA_API_Delete_LxCP(unit,temp_id)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete LxCP %d\n",
                             __FUNCTION__,temp_id);
           return MEA_ERROR;
       }

   }
   {
       MEA_LxCP_Protocol_key_dbt          key;
       MEA_LxCP_Protocol_data_dbt        data;

       /*Disable action on Lxcp 1*/
       for (key.protocol = 0;
           key.protocol < MEA_LXCP_PROTOCOL_LAST;
           key.protocol++){
           if (MEA_API_IsSupport_LxCP_Protocol(key.protocol))
           {
               MEA_OS_memset(&data, 0, sizeof(data));
               data.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
               if (mea_LxCP_drv_Protocol_set(unit,
                   MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP,
                   &key,
                   &data, /* delete */
                   &(MEA_LxCP_Table[MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP].protocol[key.protocol])
                   ) != MEA_OK){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                       "%s - mea_LxCP_drv_Protocol_set failed \n", __FUNCTION__);
                 
                   return MEA_ERROR;
               }

           }/*protocol support*/
       }
   }


   /* Return to caller */
   return MEA_OK;

}



