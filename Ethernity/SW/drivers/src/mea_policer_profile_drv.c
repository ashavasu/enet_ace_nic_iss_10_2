/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/






/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_if_service_drv.h"
#include "mea_api.h"
#include "MEA_platform.h"
#include "mea_port_drv.h"
#include "mea_drv_common.h"
#include "mea_bm_regs.h"
#include "mea_policer_profile_drv.h"
#include "mea_policer_drv.h"
#include "mea_init.h"
#include "mea_action_drv.h"
#include "mea_deviceInfo_drv.h"




/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_POLICER_SLOWSLOW (MEA_IS_POLICE_SLOW_SUPPORT)
/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/


static MEA_Policer_AcmProf_dbt     *MEA_Policer_AcmProfile_Table      = NULL;
static MEA_Policer_Prof_hwUnit_dbt *MEA_Policer_Prof_hwUnit_table     = NULL;
static MEA_Policer_prof_t          *MEA_Policer_Profile_Disable_Table = NULL;

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_policer_Profile_UpdateHwEntry>                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_policer_Profile_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t              unit_i     = (MEA_Unit_t              )arg1;
    MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)(long)arg2;
    MEA_Policer_Prof_hw_dbt*hwEntry_pi = (MEA_Policer_Prof_hw_dbt*)arg3;
    MEA_Policer_Entry_dbt  *entry_pi   = (MEA_Policer_Entry_dbt  *)arg4;
    MEA_Uint32             val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32             i;
    MEA_Uint16             num_of_regs;
    
    MEA_Uint32             count_shift;
    

    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
                             MEA_Policer_Prof_hwUnit_table[hwUnit_i].num_of_regs);

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
    count_shift=0;
    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_PROF_CIR_WIDTH,
                            (MEA_Uint32)entry_pi->CIR,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_PROF_CIR_WIDTH;

   MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_PROF_CBS_WIDTH,
                            (MEA_Uint32)entry_pi->CBS ,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_PROF_CBS_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_PROF_EIR_WIDTH,
                            (MEA_Uint32)entry_pi->EIR ,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_PROF_EIR_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_PROF_EBS_WIDTH,
                            (MEA_Uint32)entry_pi->EBS ,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_PROF_EBS_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_PROF_COMP_WIDTH,
                            ((MEA_Uint32)entry_pi->comp / 2 ),
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_PROF_COMP_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_PROF_CF_WIDTH,
                            (MEA_Uint32)entry_pi->cup , /*  0- ++  1--   */
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_PROF_CF_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_PROF_CA_WIDTH,
                            (MEA_Uint32)entry_pi->color_aware ,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_PROF_CA_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_SRV_RATE_METER_PROF_TYPE_BAG_WIDTH,
        (MEA_Uint32)entry_pi->type_mode , // BAG
        &val[0]);
    count_shift+=MEA_SRV_RATE_METER_PROF_TYPE_BAG_WIDTH;
   if(MEA_POLICER_SLOWSLOW) {
    MEA_OS_insert_value(count_shift,
        MEA_SRV_RATE_METER_PROF_SLOWSLOW,
        (MEA_Uint32)entry_pi->slow_mode, 
        &val[0]);
    count_shift += MEA_SRV_RATE_METER_PROF_SLOWSLOW;
    }
    
    
    /**************************/
    

    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }

    for (i=0;i<num_of_regs;i++) {
        hwEntry_pi->regs[i] = val[i];
    }

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_policer_Profile_UpdateHw>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_policer_Profile_UpdateHw(MEA_Unit_t               unit_i,
                                                   MEA_AcmMode_t    ACM_Mode_i,
                                                   MEA_Policer_prof_t       id_i,
                                                   MEA_Policer_Entry_dbt   *entry_pi,
                                                   MEA_Policer_CBS_EBS_dbt *calculate_CBS_EBS)
{

    MEA_ind_write_t ind_write;     
    MEA_db_HwUnit_t hwUnit;
    MEA_Policer_Prof_hwUnit_dbt        *policer_prof_hwUnit;
    MEA_Policer_Prof_acm_hw_dbt        *policer_prof_acm_hw;
    MEA_Policer_Prof_hw_dbt            *policer_prof_hw;
    MEA_Uint16                          hiddenId;
    MEA_db_HwId_t                       hwProfId;

    
    MEA_Uint32             flag_fast_service;
    
    MEA_EirCir_t           round_cir,round_eir;
    MEA_EirCir_t           norm_eir,norm_cir;
    MEA_EbsCbs_t           round_cbs, round_ebs;
    MEA_EbsCbs_t           norm_ebs,norm_cbs;
    MEA_EbsCbs_t           xbs_gn = (entry_pi->gn_sign == 1) ? (1) : (0x00000001<<(entry_pi->gn_type)) ;
    MEA_uint64             temp; 
    MEA_Uint32             flag_SlowSlow_service;
    MEA_uint64 temp1;
    MEA_Policer_Entry_dbt       entry;
    MEA_Globals_Entry_dbt      Globals_Entry;
    MEA_Uint32 tick;
    MEA_Uint32 mtu_size;
    MEA_Uint32 maxVal;

  
    flag_SlowSlow_service= MEA_FALSE;
    MEA_OS_memset(&entry,0,sizeof(entry));
    MEA_OS_memset(&Globals_Entry, 0, sizeof(Globals_Entry));
     
    MEA_OS_memcpy(&entry,entry_pi,sizeof(entry));
    /* Get the hwUnit and hwId */
    
    hwUnit = MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[id_i].hwUnit;
    hwProfId = MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[id_i].hwProfId;
    if(entry_pi->type_mode == MEA_POLICER_MODE_MEF){
		if((MEA_SRV_RATE_METER_PROF_CIR_WIDTH !=0) && (MEA_SRV_RATE_METER_BUCKET_CIR_WIDTH !=0 ))
			temp = (MEA_uint64) 10E9; //MEA_POLICER_CIR_EIR_SLOW_MAX_VAL((entry_pi->gn_sign),(entry_pi->gn_type));
		else
			temp = MEA_POLICER_CIR_EIR_SLOW_MAX_VAL((entry_pi->gn_sign),(entry_pi->gn_type));

    /* get the relevant tick value according to the service type (normal/fast) */
    if(entry_pi->gn_type == 0){ 
        if((entry_pi->CIR > temp) || 
           (entry_pi->EIR > temp)  ){
            flag_fast_service=MEA_TRUE; 
            } else {
                flag_fast_service=MEA_FALSE;
                if (MEA_POLICER_SLOWSLOW) {
                    temp1 = MEA_POLICER_CIR_EIR_SLOW_SLOW_MAX_VAL((entry_pi->gn_sign), (entry_pi->gn_type));
                    if ( ((entry_pi->CIR < temp1) && (entry_pi->CIR >0)) || 
						 ((entry_pi->EIR < temp1) && (entry_pi->EIR >0))
					  ) {
                        flag_SlowSlow_service = MEA_TRUE;

                    }
                }

            }
    }else{
        flag_fast_service=MEA_FALSE;
        if (MEA_POLICER_SLOWSLOW) {
            temp1 = MEA_POLICER_CIR_EIR_SLOW_SLOW_MAX_VAL((entry_pi->gn_sign), (entry_pi->gn_type));
			if (((entry_pi->CIR < temp1) && (entry_pi->CIR > 0)) ||
				((entry_pi->EIR < temp1) && (entry_pi->EIR > 0))
				) {
            flag_SlowSlow_service = MEA_TRUE;
             
           }
         }
    }


    /* calculate normalize cir and eir */
    if(entry_pi->CIR != 0){
        if (flag_fast_service == MEA_TRUE)
		{
            temp=MEA_SRV_POLICER_CIR_EIR_FAST_GN((entry_pi->gn_sign),(entry_pi->gn_type));
            round_cir = entry_pi->CIR % temp;
            norm_cir  = entry_pi->CIR / temp;
			
				if ((round_cir >= temp / 2)) {
					round_cir = 1;
				}
				else {
					round_cir = 0;
				}
			
            if (round_cir +  norm_cir <= MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK){ 
                norm_cir += round_cir  ;
            }else{
                
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"fast norm_cir is %llu > %d \n",norm_cir,MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK );
                 if(entry_pi->gn_type!=0)
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING," enlarge the PGT  \n" );
                norm_cir=MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK;
            }
           
         } else {
             temp= MEA_SRV_POLICER_CIR_EIR_SLOW_GN((entry_pi->gn_sign),(entry_pi->gn_type));
             /**/
             if (MEA_POLICER_SLOWSLOW) {
                if(flag_SlowSlow_service ==MEA_TRUE)
                    temp = temp/4;
             }

             round_cir = entry_pi->CIR % temp;
             norm_cir  = entry_pi->CIR / temp;
			 
                if (round_cir >= temp/2 ) {
					if (flag_SlowSlow_service == MEA_FALSE) {
						if (entry_pi->CIR > 10 * 1024 * 1024) 
						{
							round_cir = 1;
						}
						else {
							round_cir = 0;
						}
					}
					else {
						round_cir = 0;
					}

               } else {
                  round_cir=0;
               }
               if (round_cir +  norm_cir <= MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK) {
                   norm_cir += round_cir  ;
               }else{
                   
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"slow norm_cir is %llu > %d \n",norm_cir,MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK );
                   if(entry_pi->gn_type!=0)
                       MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING," enlarge the PGT  \n" );
                   norm_cir=MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK;
               }
       }
    }else{
        norm_cir=0;
    } 
 
    if(entry_pi->EIR != 0){
    if (flag_fast_service == MEA_TRUE){
       temp= MEA_SRV_POLICER_CIR_EIR_FAST_GN((entry_pi->gn_sign),(entry_pi->gn_type));
       round_eir = entry_pi->EIR % temp;
       norm_eir  = entry_pi->EIR / temp;
       if ( round_eir >= temp/2 ){ 
          round_eir=1;
       } else {
          round_eir=0;
       }         
       if (round_eir + norm_eir <=MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK){
           norm_eir+=round_eir;
       }else{
           
            MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"fast norm_eir is %llu > %d \n",norm_cir,MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK );
            if(entry_pi->gn_type!=0)
                MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING," enlarge the PGT  \n" );
            norm_eir=MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK;
       }

    } else {
        temp = MEA_SRV_POLICER_CIR_EIR_SLOW_GN((entry_pi->gn_sign),(entry_pi->gn_type));
        if (MEA_POLICER_SLOWSLOW) {
            if (flag_SlowSlow_service == MEA_TRUE)
                temp = temp / 4;
        }

        
        round_eir = entry_pi->EIR % temp;
        norm_eir  = entry_pi->EIR / temp;
       if ( round_eir >= temp/2 ){ 
		   if (flag_SlowSlow_service == MEA_FALSE) {
			   if (entry_pi->CIR > 10 * 1024 * 1024) {
				   round_eir = 1;
			   }
			   else {
				   round_eir = 0;
			   }
		   }
		   else {
			   round_eir = 0;
		   }
       } else {
          round_eir=0;
       }         
       if (round_eir + norm_eir <=MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK){
           norm_eir+=round_eir; 
       }else{
           
           MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"slow norm_eir is %llu > %d \n",norm_cir,MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK );
           if(entry_pi->gn_type!=0)
               MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING," enlarge the PGT  \n" );
            norm_eir=MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK;
       }
     
    }
    } else {
        norm_eir=0;
    }

    if(entry_pi->CBS != 0){

       round_cbs = entry_pi->CBS % xbs_gn;
       norm_cbs  = entry_pi->CBS / xbs_gn;
       if ( round_cbs >= xbs_gn/2 ){ 
          round_cbs=1;
       } else {
          round_cbs=0;
       }         
       temp =MEA_POLICER_CBS_EBS_MAX_VAL((entry_pi->gn_sign),(entry_pi->gn_type)); 
       if (round_cbs + norm_cbs <= ((MEA_Uint32)(temp/xbs_gn))) {
          norm_cbs+=round_cbs;    
       }
       if (entry_pi->comp + norm_cbs <= (MEA_Uint32)(temp/xbs_gn)) {
          norm_cbs+=entry_pi->comp;    
       }

    } else {
        norm_cbs=0;
    }

    if(entry_pi->EBS != 0){

       round_ebs = entry_pi->EBS % xbs_gn;
       norm_ebs  = entry_pi->EBS / xbs_gn;
       if ( round_ebs >= xbs_gn/2 ){ 
          round_ebs=1;
       } else {
          round_ebs=0;
       }         
       temp = MEA_POLICER_CBS_EBS_MAX_VAL((entry_pi->gn_sign),(entry_pi->gn_type));
       if (round_ebs + norm_ebs <= temp/xbs_gn) {
          norm_ebs+=round_ebs;    
       }
       if (entry_pi->comp + norm_ebs <= temp/xbs_gn) {
          norm_ebs+=entry_pi->comp;    
       }

    } else {
        norm_ebs=0;
    }
    
   
    if(entry_pi->gn_sign==1){
        MEA_Uint32 granularity; 
        norm_cir=(norm_cir<<entry_pi->gn_type);
    
        norm_eir=(norm_cir<<entry_pi->gn_type);
        
       ////   if cir/32000 > cbs then cbs= xcir
        if(flag_fast_service == MEA_TRUE){
        granularity = MEA_POLICER_GLOBAL_GRANULARITY_FAST_VAL;
        } else {
            granularity = MEA_POLICER_GLOBAL_GRANULARITY_SLOW_VAL;
        }

        if(norm_cbs !=0){
            if(entry_pi->CIR/granularity >= norm_cbs){
               norm_cbs=(MEA_Uint32)entry_pi->CIR/granularity;//(xcir)
            }else {
                temp = MEA_POLICER_CBS_EBS_MAX_VAL((entry_pi->gn_sign),(entry_pi->gn_type));
                if(norm_cbs *2 <= temp ){
                  norm_cbs=norm_cbs *2;
                }else{
                    norm_cbs=(MEA_Uint32)temp;
                }
            }
        }
        if(norm_ebs !=0){
            if(entry_pi->EIR/granularity >= norm_ebs){
               norm_ebs=(MEA_Uint32)entry_pi->EIR/granularity;//(xcir)
            }else {
                temp = MEA_POLICER_CBS_EBS_MAX_VAL((entry_pi->gn_sign),(entry_pi->gn_type));
                if(norm_ebs *2 <= temp ){
                  norm_ebs=norm_ebs *2;
                }else{
                    norm_ebs=(MEA_Uint32)temp;
                }
            }
        }
    
    } //gn_sign ==1
#if 1
    /**/
        if (entry_pi->CIR != 0 && entry_pi->CBS != 0 /*&& entry_pi->gn_type == 0*/)
        {
           
                
                if (entry_pi->maxMtu == 0){
                    mtu_size = MEA_Platform_GetMaxMTU();
                }
                else{
                    mtu_size = entry_pi->maxMtu;
                }

                maxVal = MEA_OS_MAX(((MEA_Uint32)(norm_cir<<(entry_pi->gn_type)) + mtu_size), (mtu_size * 2));
               
                //norm_cbs = norm_cbs;
                if (maxVal > norm_cbs) //if (norm_cir > norm_cbs)
                {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " ****** The CBS is smaller than the recommended %d ****** \n", maxVal);
                    //return MEA_ERROR;
                }
            
        }
        if (entry_pi->EIR != 0 && entry_pi->EBS != 0 /*&& entry_pi->gn_type == 0*/)
        {
            
                if (entry_pi->maxMtu == 0){
                    mtu_size = MEA_Platform_GetMaxMTU();
                }
                else{
                    mtu_size = entry_pi->maxMtu;
                }
                

                maxVal = MEA_OS_MAX(((MEA_Uint32)(norm_eir<< (entry_pi->gn_type)) + mtu_size), (mtu_size * 2));
                
                //norm_ebs = norm_ebs;
                //return MEA_ERROR;
                if (maxVal > norm_ebs)
                {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "****** The EBS is smaller than the recommended %d ****** \n", maxVal);
                }
        }


    
    /**/
#endif
    }else{
        if (entry_pi->type_mode == MEA_POLICER_MODE_BAG)
        {
         flag_fast_service=MEA_FALSE;
         MEA_API_Get_Globals_Entry(unit_i, &Globals_Entry);
         if (entry_pi->CIR != 0){
             if (entry_pi->CIR == 1 || entry_pi->CIR == 2){
                 flag_fast_service = MEA_TRUE;
                 tick = Globals_Entry.ticks_for_fast_service;
             }
             else{
                 tick = Globals_Entry.ticks_for_slow_service;
             }
             temp= MEA_SRV_POLICER_CIR_EIR_SLOW_GN((entry_pi->gn_sign),(entry_pi->gn_type));
             // CIR [bps]= [(Lmax[Byte]+ Smin[Byte])/2] * 8000)/BAG[msec]
             // CIR= BAG [msec], EIR=Jitter [micsec] , 
             
             
             norm_cir = (1 + (0x78 * 16) / ((entry_pi->CIR * 1000 * (MEA_Uint32)MEA_GLOBAL_SYS_Clock/1000000) / (tick)));
            
         }else{
             norm_cir=0;
         }


         //CBS_policer =(Lmax + 20) *(1+ Jitter/BAG)(
         if( entry_pi->CIR != 0) {

        //     norm_cbs = (MEA_Uint32) ( (0x78 ) * ((10) + 10*(entry_pi->EIR /(entry_pi->CIR*1000) ) )/10);
             if (entry_pi->EIR != 0)
                 norm_cbs = (MEA_EbsCbs_t)(0x78 * 16) * (101 + (100 * (MEA_EbsCbs_t)entry_pi->EIR) / (1000 * (MEA_EbsCbs_t)entry_pi->CIR)) / 100;
             else
                 norm_cbs = (MEA_EbsCbs_t)(0x78 * 16) * 105 / 100;


         }else{
             norm_cbs = (MEA_EbsCbs_t)(0x78 * 16) * 110 / 100;
         }

         norm_eir =0;
         norm_ebs = 0 ;

    
         }//BAG
        
    }

    if (entry_pi->type_mode != MEA_POLICER_MODE_BAG){
        entry.CIR = norm_cir;
        entry.CBS = norm_cbs;

        entry.EIR = norm_eir;
        entry.EBS = norm_ebs;
    
        calculate_CBS_EBS->CBS = norm_cbs;
        calculate_CBS_EBS->EBS = norm_ebs;
    }
    if(entry_pi->type_mode == MEA_POLICER_MODE_BAG){
        entry.CIR = norm_cir;
        entry.CBS = norm_cbs;
		entry.EIR = norm_eir;
        entry.EBS = norm_ebs;
        calculate_CBS_EBS->CBS = norm_cbs;
        calculate_CBS_EBS->Lmax = 0x78; /*entry_pi->CBS*/;
    }
    
    if (MEA_POLICER_SLOWSLOW) {
        if(flag_SlowSlow_service == MEA_TRUE)
            entry.slow_mode =  MEA_TRUE;
    }

    /* calculate policer_prof_hw */
    policer_prof_hwUnit        = &(MEA_Policer_Prof_hwUnit_table[hwUnit]);
    policer_prof_acm_hw        = &(policer_prof_hwUnit->acmTable[ACM_Mode_i]);
    policer_prof_hw            = &(policer_prof_acm_hw->profTable[hwProfId]);

    /* init the ind_write */
    ind_write.tableType      = ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF ;    
    ind_write.tableOffset    = 0; /* Will be define inside the loop */
    ind_write.cmdReg         = MEA_BM_IA_CMD;      
    ind_write.cmdMask        = MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg      = MEA_BM_IA_STAT;   
    ind_write.statusMask     = MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg   = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask  = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry     = (MEA_FUNCPTR)mea_drv_policer_Profile_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)(policer_prof_hw);
    ind_write.funcParam4 = (MEA_funcParam)&entry;



    /* Write to hw for for all hiddenId */
    for(hiddenId=0;hiddenId<policer_prof_hwUnit->num_of_hiddenEntries;hiddenId++) {

        ind_write.tableOffset = (hiddenId                                * 
                                 policer_prof_hwUnit->num_of_acmEntries  * 
                                 policer_prof_hwUnit->num_of_profEntries ) +
                                (ACM_Mode_i *
                                 policer_prof_hwUnit->num_of_profEntries ) +
                                 hwProfId;
if (MEA_IS_POLICER_SUPPORT){
        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_WriteIndirect failed "
                              "TableType=%d , TableOffset=%d , mea_module=%d\n",
                              __FUNCTION__,
                              ind_write.tableType,
                              ind_write.tableOffset,
                              MEA_MODULE_BM);
            return MEA_ERROR;
        } 
}

    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_ConcludeHw>                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Policer_Profile_ConcludeHw(MEA_Unit_t unit_i)
{

    MEA_Policer_Prof_hwUnit_dbt        *policer_prof_hwUnit;
    MEA_Policer_Prof_acm_hw_dbt        *policer_prof_acm_hw;
    MEA_Policer_Prof_hw_dbt            *policer_prof_hw;
    MEA_db_HwUnit_t                     hwUnit;
    MEA_AcmMode_t               ACM_Mode;
    MEA_Policer_prof_t                  policerProfId;


    /* If not allocate then simple return */
    if (MEA_Policer_Prof_hwUnit_table == NULL) {
        return MEA_OK;
    }

    /* Loop on all hwUnit and init the MEA_Policer_prof_hwUnit_table */
    for (hwUnit=0,policer_prof_hwUnit=&(MEA_Policer_Prof_hwUnit_table[0]);
         hwUnit<mea_drv_num_of_hw_units;
         hwUnit++,policer_prof_hwUnit++) {

        if (policer_prof_hwUnit->acmTable == NULL) {
            continue;
        }

        /* Loop on all ACM_Mode in this hwUnit */
        for (ACM_Mode=0,policer_prof_acm_hw=&(policer_prof_hwUnit->acmTable[0]);
             ACM_Mode<policer_prof_hwUnit->num_of_acmEntries;
             ACM_Mode++,policer_prof_acm_hw++) {

//             if (policer_prof_acm_hw->used_profEntries != 0) {
//                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
//                                   "%s - Ttr to conclude while there is still used profiles "
//                                   "(hwUnit=%d,ACM_Mode=%d,used_profEntries=%d)\n",
//                                   __FUNCTION__,hwUnit,ACM_Mode,policer_prof_acm_hw->used_profEntries);
//                 return MEA_ERROR;
//             }

            if (policer_prof_acm_hw->profTable == NULL) {
                continue;
            }

            /* Loop on all profiles in this hidden profile */
            for (policerProfId=0,policer_prof_hw=&(policer_prof_acm_hw->profTable[0]);
                 policerProfId<policer_prof_hwUnit->num_of_profEntries;
                 policerProfId++,policer_prof_hw++) {

                if (policer_prof_hw->regs == NULL) {
                    continue;
                }

                MEA_OS_free (policer_prof_hw->regs);
                policer_prof_hw->regs=NULL;
            }

            MEA_OS_free (policer_prof_acm_hw->profTable);
        }

        MEA_OS_free(policer_prof_hwUnit->acmTable);
    }

    MEA_OS_free (MEA_Policer_Prof_hwUnit_table);
    MEA_Policer_Prof_hwUnit_table=NULL;

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_InitHw>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Policer_Profile_InitHw(MEA_Unit_t unit_i){

    MEA_Policer_Prof_hwUnit_dbt        *policer_prof_hwUnit;
    MEA_Policer_Prof_acm_hw_dbt        *policer_prof_acm_hw;
    MEA_Policer_Prof_hw_dbt            *policer_prof_hw;
    MEA_db_HwUnit_t                     hwUnit;
    MEA_AcmMode_t               ACM_Mode;
    MEA_Policer_prof_t                  hwProfId;
    MEA_Uint32                          size;


    /* Check that we not already init */
    if (MEA_Policer_Prof_hwUnit_table != NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Policer_Prof_hwUnit_table already allocated\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    /* Allocate hw database MEA_Policer_prof_hwUnit_table */
    size=sizeof(MEA_Policer_Prof_hwUnit_dbt)*mea_drv_num_of_hw_units;
    MEA_Policer_Prof_hwUnit_table = (MEA_Policer_Prof_hwUnit_dbt*)MEA_OS_malloc(size);
    if (MEA_Policer_Prof_hwUnit_table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate MEA_Policer_Prof_hwUnit_table failed (size=%d)\n",
                          __FUNCTION__,size);
        return MEA_ERROR;
    }
    MEA_OS_memset(MEA_Policer_Prof_hwUnit_table,0,size);


    /* Loop on all hwUnit and init the MEA_Policer_prof_hwUnit_table */
    for (hwUnit=0,policer_prof_hwUnit=&(MEA_Policer_Prof_hwUnit_table[0]);
         hwUnit<mea_drv_num_of_hw_units;
         hwUnit++,policer_prof_hwUnit++) {

        /* Set number of ACM Modes for this hwUnit */
        policer_prof_hwUnit->num_of_acmEntries = mea_drv_Get_DeviceInfo_NumOfAcmModes(unit_i,hwUnit);
        
        /* Set the number of hiddenProfiles + 1 for this hwUnit */
        policer_prof_hwUnit->num_of_hiddenEntries = 
                    mea_drv_Get_DeviceInfo_NumOfAcmHiddenProfiles(unit_i,hwUnit) + 1;

        /* Set the number of Profiles per this hwUnit */
        policer_prof_hwUnit->num_of_profEntries = ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_LENGTH(hwUnit);

        /* Init the Hw Profile entry num of regs */
        policer_prof_hwUnit->num_of_regs = ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_NUM_OF_REGS(hwUnit);

        /* Allocate the acmTable  */
        size=sizeof(MEA_Policer_Prof_acm_hw_dbt)*policer_prof_hwUnit->num_of_acmEntries;
        policer_prof_hwUnit->acmTable = (MEA_Policer_Prof_acm_hw_dbt*)MEA_OS_malloc(size); 
        if (policer_prof_hwUnit->acmTable == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s - Allocate policer_prof_hwUnit->acmTable failed (hwUnit=%d,size=%d)\n",
                                   __FUNCTION__,hwUnit,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(policer_prof_hwUnit->acmTable,0,size);

        /* Init the acmTable  */
        for (ACM_Mode=0,policer_prof_acm_hw=&(policer_prof_hwUnit->acmTable[0]);
             ACM_Mode<policer_prof_hwUnit->num_of_acmEntries;
             ACM_Mode++,policer_prof_acm_hw++) {

            /* Init the used_profEntries  */
            policer_prof_acm_hw->used_profEntries = 0;

            /* Allocate the profTable */
            size=sizeof(MEA_Policer_Prof_hw_dbt)*policer_prof_hwUnit->num_of_profEntries;
            policer_prof_acm_hw->profTable = (MEA_Policer_Prof_hw_dbt*)MEA_OS_malloc(size); 
            if (policer_prof_acm_hw->profTable == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Allocate policer_prof_acm_hw->profTable failed (hwUnit=%d,ACM_Mode=%d,size=%d)\n",
                                  __FUNCTION__,hwUnit,ACM_Mode,size);
                return MEA_ERROR;
            }
            MEA_OS_memset(policer_prof_acm_hw->profTable,0,size);

            /* Init the profTable  */
            for (hwProfId=0,policer_prof_hw=&(policer_prof_acm_hw->profTable[0]);
                 hwProfId<policer_prof_hwUnit->num_of_profEntries;
                 hwProfId++,policer_prof_hw++) {

                /* Allocate regs  */
                size=sizeof(MEA_Uint32)*policer_prof_hwUnit->num_of_regs;
                policer_prof_hw->regs = (MEA_Uint32*)MEA_OS_malloc(size); 
                if (policer_prof_hw->regs == NULL) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - Allocate policer_prof_hw->regs failed (hwUnit=%d,ACM_Mode=%d,hwProfId=%d,size=%d)\n",
                                      __FUNCTION__,hwUnit,ACM_Mode,hwProfId,size);
                    return MEA_ERROR;
                }
                MEA_OS_memset(policer_prof_hw->regs,0,size);
            }
        }
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_ReInitHw_writeHwEntry>                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_Policer_Profile_ReInitHw_writeHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t           unit_i   = (MEA_Unit_t   )arg1;
    MEA_module_te        module_i = (MEA_module_te)arg2;
    MEA_Uint32           count_i = (MEA_Uint32)((long)arg3);
    MEA_Uint32*          regs_i   = (MEA_Uint32*  )arg4;
    MEA_Uint32 i;

    for (i=0;i<count_i;i++) {
        MEA_API_WriteReg(unit_i,
                         (module_i==MEA_MODULE_BM) 
                            ? MEA_BM_IA_WRDATA(i)
                            : MEA_IF_WRITE_DATA_REG(i),
                         regs_i[i],
                         module_i);
    }

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_ReInitHw>                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Policer_Profile_ReInitHw(MEA_Unit_t unit_i)
{

    MEA_Policer_Prof_hwUnit_dbt        *policer_prof_hwUnit;
    MEA_Policer_Prof_acm_hw_dbt        *policer_prof_acm_hw;
    MEA_Policer_Prof_hw_dbt            *policer_prof_hw;
    MEA_db_HwUnit_t                     hwUnit;
    MEA_AcmMode_t               ACM_Mode;
    MEA_Uint16                          hiddenId;
    MEA_Policer_prof_t                  hwProfId;
    MEA_ind_write_t                     ind_write;     
    mea_memory_mode_e                   save_globalMemoryMode;


    /* Check that we allocate */
    if (MEA_Policer_Prof_hwUnit_table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Policer_Prof_hwUnit_table Not allocated\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    memset(&ind_write,0,sizeof(ind_write));
    ind_write.tableType      = ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF ;    
    ind_write.tableOffset    = 0; /* Will be define inside the loop  */
    ind_write.cmdReg         = MEA_BM_IA_CMD;      
    ind_write.cmdMask        = MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg      = MEA_BM_IA_STAT;   
    ind_write.statusMask     = MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg   = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask  = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry     = (MEA_FUNCPTR)mea_drv_Policer_Profile_ReInitHw_writeHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)MEA_MODULE_BM;
    ind_write.funcParam3 = (MEA_funcParam)0; /* Will be define inside the loop  */
    ind_write.funcParam4 = (MEA_funcParam)0; /* Will be define inside the loop  */

    /* Loop on all hwUnit and init the MEA_Policer_prof_hwUnit_table */
    for (hwUnit=0,policer_prof_hwUnit=&(MEA_Policer_Prof_hwUnit_table[0]);
         hwUnit<mea_drv_num_of_hw_units;
         hwUnit++,policer_prof_hwUnit++) {

        /* Set the HwUnit */
        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);


        /* Loop on all ACM_Mode  */
        for (ACM_Mode=0,policer_prof_acm_hw=&(policer_prof_hwUnit->acmTable[0]);
             ACM_Mode<policer_prof_hwUnit->num_of_acmEntries;
             ACM_Mode++,policer_prof_acm_hw++) {

            /* Loop on all profiles  */
            for (hwProfId=0,policer_prof_hw=&(policer_prof_acm_hw->profTable[0]);
                 hwProfId<policer_prof_hwUnit->num_of_profEntries;
                 hwProfId++,policer_prof_hw++) {
                    
                /* If not valid continue */
                if (!policer_prof_hw->valid) {
                    continue;
                }

                ind_write.funcParam3 = (MEA_funcParam)((long)policer_prof_hwUnit->num_of_regs);
                ind_write.funcParam4 = (MEA_funcParam)&(policer_prof_hw->regs[0]);

                /* Loop on all hidden profile */
                for (hiddenId=0;hiddenId<policer_prof_hwUnit->num_of_hiddenEntries;hiddenId++) {
                    
                    /* Write to hardware */
                    ind_write.tableOffset = (hiddenId                                * 
                                             policer_prof_hwUnit->num_of_acmEntries  * 
                                             policer_prof_hwUnit->num_of_profEntries ) +
                                            (ACM_Mode *
                                             policer_prof_hwUnit->num_of_profEntries ) +
                                            hwProfId;
if (MEA_IS_POLICER_SUPPORT){
                    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM) != MEA_OK) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s - MEA_API_WriteIndirect failed "
                                          "TableType=%d , TableOffset=%d , mea_module=%d\n",
                                          __FUNCTION__, 
                                          ind_write.tableType,
                                          ind_write.tableOffset,
                                          MEA_MODULE_BM);
                        /* restore the HwUnit */
                        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                        return MEA_ERROR;
                    }
}

                }
            }
        }

        /* restore the HwUnit */
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

    }

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_Check   >                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_Check(MEA_Unit_t             unit_i,
                                         MEA_Policer_Entry_dbt *entry_i, MEA_Bool *fast_slow)
{
    
    MEA_uint64 slow_max;
    MEA_Bool request_for_fast_tmId;
    MEA_Policer_Entry_dbt *Policer_Entry;
    MEA_uint64 gn;
    MEA_uint64 max;
    MEA_Uint32 comp;
    MEA_uint64 temp_calc;
    MEA_uint64 MyCIR;
    MEA_uint64 MyEIR;
    

    if(entry_i == NULL){
        return MEA_ERROR;
    }

    if(fast_slow== NULL){
     return MEA_ERROR;
    }
    
    Policer_Entry=entry_i;
	if (entry_i->type_mode == MEA_POLICER_MODE_MEF)
	{
		if ((MEA_SRV_RATE_METER_PROF_CIR_WIDTH != 0) && (MEA_SRV_RATE_METER_BUCKET_CIR_WIDTH != 0))
		{ 
			slow_max = (MEA_uint64) 10E9;
		} else {
			slow_max = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_MAX_VAL((entry_i->gn_sign), (entry_i->gn_type));
		}
		 if(entry_i->gn_type== 0){ 
			 /*  */

             if ((entry_i->CIR   > slow_max) ||
                 (entry_i->EIR   > slow_max)  ) {
                 *fast_slow= request_for_fast_tmId = MEA_TRUE;
				 if ((MEA_SRV_RATE_METER_PROF_CIR_WIDTH != 0) && (MEA_SRV_RATE_METER_BUCKET_CIR_WIDTH != 0))
				 {
					 gn = (MEA_uint64) 10E9;
					 max = (MEA_uint64) 10E9;
				 }
				 else {
					 gn = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_FAST_GN((entry_i->gn_sign), (entry_i->gn_type));
					 max = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_FAST_MAX_VAL((entry_i->gn_sign), (entry_i->gn_type));
				 }
				  MyCIR = entry_i->CIR;
                  MyEIR = entry_i->EIR;
                  if (MyCIR || MyEIR) {

                  }
             } else {
                  *fast_slow=request_for_fast_tmId = MEA_FALSE;
				  if ((MEA_SRV_RATE_METER_PROF_CIR_WIDTH != 0) && (MEA_SRV_RATE_METER_BUCKET_CIR_WIDTH != 0))
				  {
					  gn = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_GN((0), (0));
				      max = (MEA_uint64) 10E9;
				  }
				  else {
					  gn = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_GN((entry_i->gn_sign), (entry_i->gn_type));
					  max = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_MAX_VAL((entry_i->gn_sign), (entry_i->gn_type));

				  }
			}
         }else{
              *fast_slow=request_for_fast_tmId = MEA_FALSE;
              /*
              gn = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_GN((entry_i->gn_sign),(entry_i->gn_type));
              max = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_MAX_VAL((entry_i->gn_sign),(entry_i->gn_type));
              */
			  if ((MEA_SRV_RATE_METER_PROF_CIR_WIDTH != 0) && (MEA_SRV_RATE_METER_BUCKET_CIR_WIDTH != 0))
			  {
				  gn = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_FAST_GN((0), (0));
				  max = (MEA_uint64) 10E9;
			  }
			  else {
				  gn = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_FAST_GN((entry_i->gn_sign), (entry_i->gn_type));
				  max = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_FAST_MAX_VAL((entry_i->gn_sign), (entry_i->gn_type));
			  }
			  MyCIR = entry_i->CIR;
              MyEIR = entry_i->EIR;
         }

         if (Policer_Entry->CIR == 0 && Policer_Entry->CBS !=0) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - CBS (%d) need to be Zero when Cir is Zero \n",
                                  __FUNCTION__,Policer_Entry->CBS);
                
            return MEA_ERROR;
          }
          if (Policer_Entry->CIR != 0 && Policer_Entry->CBS == 0) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - CIR (%d) need to be Zero when CBS is Zero \n",
                                  __FUNCTION__,Policer_Entry->CIR);   
                
                return MEA_ERROR;
         }

          if (Policer_Entry->EIR == 0 && Policer_Entry->EBS !=0) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - EBS (%d) need to be Zero when EIR is Zero \n",
                                  __FUNCTION__,Policer_Entry->EBS);
                
                return MEA_ERROR;
         }
         if (Policer_Entry->EIR!= 0 && Policer_Entry->EBS == 0) {
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - EIR (%d) need to be Zero when EBS is Zero \n",
                                  __FUNCTION__,Policer_Entry->EIR);
                
                return MEA_ERROR;
         }
         if (Policer_Entry->EIR != 0 && Policer_Entry->EIR < gn) {

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - EIR value %ld  < min value %ld  \n",
                                  __FUNCTION__,Policer_Entry->EIR,gn);
                
                return MEA_ERROR;
         }
         if (Policer_Entry->EIR >  max    ) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - EIR value %llu  > max value %llu  \n",
                                  __FUNCTION__,
                                  Policer_Entry->EIR, max);
                
                return MEA_ERROR;
         }

         

         if (Policer_Entry->CIR != 0 &&  Policer_Entry->CIR < gn) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s -ERROR1 CIR value %llu  < min value %llu  \n",
                                  __FUNCTION__,Policer_Entry->CIR,gn);
                
                return MEA_ERROR;
         }
         
         if (Policer_Entry->CIR >  max    ) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s -ERROR2 CIR value %llu  > max value %llu  \n",
                                  __FUNCTION__,
                                  Policer_Entry->CIR,max);
                
                return MEA_ERROR;
         }

         comp =Policer_Entry->comp;
         if (MEA_policer_compensation(MEA_INGRESS_PORT_PROTO_VLAN,&comp) !=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - MEA_policer_compensation failed \n",
                               __FUNCTION__);
             return MEA_ERROR;
         }


         if (Policer_Entry->CBS + comp >  
             MEA_POLICER_CBS_EBS_MAX_VAL((Policer_Entry->gn_sign),(Policer_Entry->gn_type))) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - CBS value %ld + comp %ld > max value %ld (gn_type=%d) (gn_sign=%d)\n",
                               __FUNCTION__,
                               Policer_Entry->CBS,
                               comp,
                               MEA_POLICER_CBS_EBS_MAX_VAL((Policer_Entry->gn_sign),(Policer_Entry->gn_type)),
                               Policer_Entry->gn_type,
                               (Policer_Entry->gn_sign));
             return MEA_ERROR;
         }

         if (Policer_Entry->EBS + comp >  
             MEA_POLICER_CBS_EBS_MAX_VAL((Policer_Entry->gn_sign),(Policer_Entry->gn_type))) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - EBS value %ld + comp %ld > max value %ld (gn_type=%d) (gn_sign=%d)\n",
                               __FUNCTION__,
                               Policer_Entry->EBS,
                               comp,
                               MEA_POLICER_CBS_EBS_MAX_VAL((Policer_Entry->gn_sign),(Policer_Entry->gn_type)),
                               Policer_Entry->gn_type,
                               (Policer_Entry->gn_sign));
             return MEA_ERROR;
         }

         if((Policer_Entry->gn_sign)== 1){
             MEA_uint64 temp;
             if(request_for_fast_tmId== MEA_TRUE)
             {
                temp= 1000000000;
                 temp_calc= ((MEA_uint64)0x00000001<<((entry_i->gn_type)));
                 if ((Policer_Entry->CIR*(temp_calc)) >  temp){
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - CIR value  (0x%08x) * 2^pgt > max value %ld  \n ",
                                  __FUNCTION__, 
                                  Policer_Entry->CIR,temp);
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                                  "%s - The max CIR value  with  pgt %d = %ld  \n ",
                                  __FUNCTION__,
                                  (entry_i->gn_type),
                                  temp/(temp_calc));

                     return MEA_ERROR;
                 }
                 temp_calc= ((MEA_uint64)0x00000001<<((entry_i->gn_type)));
                 if (  (Policer_Entry->EIR*(temp_calc)) >  temp) {
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - EIR value %ld (0x%08x) * 2^pgt > max value %ld  \n",
                                  __FUNCTION__, 
                                  Policer_Entry->EIR,temp);
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                                  "%s - The max CIR value  with  pgt %d = %ld  \n ",
                                  __FUNCTION__,
                                  (entry_i->gn_type),
                                  temp/(temp_calc) );
                 return MEA_ERROR;
                 }
             } else {
                 //slow
				 if ((MEA_SRV_RATE_METER_PROF_CIR_WIDTH == 0) && (MEA_SRV_RATE_METER_BUCKET_CIR_WIDTH == 0))
				 {

					 temp = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_MAX_VAL((entry_i->gn_sign), (entry_i->gn_type));
					 temp_calc = ((MEA_uint64)0x00000001 << ((entry_i->gn_type)));
					 if ((Policer_Entry->CIR *(MEA_uint64)(temp_calc)) > temp) {
						 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - CIR value  (0x%08x) * 2^pgt > max value %ld  \n ",
							 __FUNCTION__,
							 Policer_Entry->CIR, temp);
						 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - The max CIR value  with  pgt %d = %ld  \n ",
							 __FUNCTION__,
							 (entry_i->gn_type),
							 temp / (temp_calc));

						 return MEA_ERROR;
					 }
					 temp_calc = ((MEA_uint64)0x00000001 << ((entry_i->gn_type)));
					 if ((Policer_Entry->EIR*(MEA_uint64)temp_calc) > temp) {
						 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - EIR value %ld (0x%08x) * 2^pgt > max value %ld  \n",
							 __FUNCTION__,
							 Policer_Entry->EIR, temp);
						 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - The max CIR value  with  pgt %d = %ld  \n ",
							 __FUNCTION__,
							 (entry_i->gn_type),
							 temp / (temp_calc));
						 return MEA_ERROR;
					 }
				 }
             
             }

         
         }
         
    
    }else{
        if(entry_i->type_mode == MEA_POLICER_MODE_BAG)
        {
            if(Policer_Entry->CIR > 128){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                    "%s - The max BAG value   128 \n ",
                    __FUNCTION__);
                return MEA_ERROR;
            }
            if(Policer_Entry->EIR > 128000){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                    "%s - The max BAG value  (10*8)   \n ",
                    __FUNCTION__);
                return MEA_ERROR;
            }
            if (Policer_Entry->CIR == 1 || Policer_Entry->CIR == 2)
                *fast_slow = request_for_fast_tmId = MEA_TRUE;
            else
                *fast_slow = request_for_fast_tmId = MEA_FALSE;

         
          
          
        }
    }
    
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_Create>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_Create(MEA_Unit_t                unit_i,
                                          MEA_IngressPort_Proto_t   port_proto_prof_i,                                
                                          MEA_Policer_Entry_dbt    *policer_entry_i,                            
                                          MEA_AcmMode_t     ACM_Mode_i,
                                          MEA_Policer_prof_t       *id_io)
{

    MEA_Policer_prof_t                  i;
    MEA_Bool                            found=MEA_FALSE;
    MEA_Policer_prof_t                  id=0;
    MEA_db_HwUnit_t                     hwUnit;
    MEA_db_HwId_t                       hwProfId;
    MEA_Uint32                          comp;
    MEA_Policer_prof_t                  max;
//    MEA_AcmMode_t               ACM_Mode;
    MEA_Policer_prof_t                  first_free;
    MEA_Policer_CBS_EBS_dbt             calculate_CBS_EBS;
    MEA_Policer_Prof_dbt               *entry;
    MEA_Policer_Prof_hwUnit_dbt        *policer_prof_hwUnit;
    MEA_Policer_Prof_acm_hw_dbt        *policer_prof_acm_hw;
    MEA_Policer_Prof_hw_dbt            *policer_prof_hw;
    MEA_Bool                           fast_slow;

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if(policer_entry_i== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - policer_entry_i == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if (ACM_Mode_i >= MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ACM_Mode_i %d >= max value %d (hwUnit=%d)\n",
                          __FUNCTION__,
                          ACM_Mode_i,
                          MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries,
                          hwUnit);
        return MEA_ERROR;
    }

    if (id_io== NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if ((*id_io != 0) && (*id_io != MEA_PLAT_GENERATE_NEW_ID)) {
        if (*id_io >= MEA_POLICER_MAX_PROF) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - *id_io %d >= max_value %d \n",
                              __FUNCTION__,*id_io,MEA_POLICER_MAX_PROF);
            return MEA_ERROR;
        }

        /* check same values */
        entry=&(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[*id_io]);
        if (entry->valid) {
            if (memcmp(&(entry->rateMeter),policer_entry_i,sizeof(entry->rateMeter))!=0) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Not same entry_i parameterizes for *id_io %d \n",
                                  __FUNCTION__,*id_io);
                return MEA_ERROR;
            }
        }
    } else {
        if (ACM_Mode_i != 0) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - To Generate Id you must use ACM_Mode_i==0\n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

    }

    

    /* check if the parameters is ok*/
    if(mea_drv_Policer_Profile_Check(unit_i,policer_entry_i,&fast_slow)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Policer_Profile_Check failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
    

    /* Recalculate policer_entry_i->comp */
    comp =policer_entry_i->comp;
    if (MEA_policer_compensation(port_proto_prof_i,&comp) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - MEA_policer_compensation failed \n",
                           __FUNCTION__);
        return MEA_ERROR;
    }
    policer_entry_i->comp=comp;

    if (*id_io == MEA_PLAT_GENERATE_NEW_ID) 
    {
        *id_io = 0;
    }

    if (*id_io != 0) {


        /* If already exist then just increase num_of_owners */
        entry=&(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[*id_io]);
        if (entry->valid) {
            entry->num_of_owners++;
            return MEA_OK;  
        }
#if 0 /*alex 04/04/2012*/

        /* search one of the ACM_Mode that already allocated to get the same hwProfId  */ 
        for(ACM_Mode=0;ACM_Mode<MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries;ACM_Mode++) {
            entry=&(MEA_Policer_AcmProfile_Table[ACM_Mode].policer_profile_table[*id_io]);
            if (entry->valid) {
                break;
            }
        }
        /* If no ACM_Mode for this *id_io then */
        if (ACM_Mode>=MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries) {

            /* Search for free hwProfId */
            policer_prof_hwUnit        = &(MEA_Policer_Prof_hwUnit_table[hwUnit]);
            policer_prof_acm_hw        = &(policer_prof_hwUnit->acmTable[ACM_Mode_i]);
            for (hwProfId=0,policer_prof_hw=&(policer_prof_acm_hw->profTable[0]);
                 hwProfId<policer_prof_hwUnit->num_of_profEntries;
                hwProfId++,policer_prof_hw++) {
                if (!policer_prof_hw->valid) {
                    break;
                }
            }
            if (hwProfId>=policer_prof_hwUnit->num_of_profEntries) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - No free hwId \n",
                                  __FUNCTION__);    
                return MEA_ERROR;
            } 
            
        } else {
            
            /* Check that we are in same hwUnit */
            if (entry->hwUnit != hwUnit) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - *id_io %d for ACM_Mode==0 is not in same hwUnit\n",
                                  __FUNCTION__,*id_io);
                return MEA_ERROR;
            }

            /* Take the hwProfId of the policer_prof of ACM_Mode zero */
            hwProfId = entry->hwProfId;
        }




#endif
        


        /* Calculate the entry for this ACM_Mode_i */
        entry=&(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[*id_io]);
		hwProfId=*id_io; //software is hw



        /* Calculate policer_prof_hw */
        policer_prof_hwUnit        = &(MEA_Policer_Prof_hwUnit_table[hwUnit]);
        policer_prof_acm_hw        = &(policer_prof_hwUnit->acmTable[ACM_Mode_i]);
        policer_prof_hw=&(policer_prof_acm_hw->profTable[hwProfId]);
        
        /* Init the hwEntry */
        policer_prof_acm_hw->used_profEntries++;
        policer_prof_hw->policer_profile_id = *id_io;
        MEA_OS_memset(policer_prof_hw->regs,0,sizeof(policer_prof_hw->regs[0])*policer_prof_hwUnit->num_of_regs);
        policer_prof_hw->valid              = MEA_TRUE;

        /* Set the id to *id_io */
        id = *id_io;

    } else {


        max = MEA_POLICER_MAX_PROF;
        first_free=max;
        found=0;
        for(i=1,entry=&(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[1]);
            i<max;
            i++,entry++){
            if (!entry->valid) {
                if (first_free==max) {
                    first_free=i;
                }
                continue;
            }
            if ((entry->hwUnit == hwUnit) &&
                (MEA_OS_memcmp(&(entry->rateMeter), 
                               policer_entry_i,
                               sizeof(entry->rateMeter)) == 0 )) {
                found = MEA_TRUE;
                id=(MEA_Policer_prof_t) i;
            }
        }
        /*if found then just increase the num_of_owners */
        if(found){
            /*need to increase owner*/
            entry=&(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[id]);
            entry->num_of_owners++;
            (*id_io)=id;
            return MEA_OK;  
        }

        if (first_free==max) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - No free id \n",
                              __FUNCTION__);    
            return MEA_ERROR;
        }

        /* Assign the first free entry */
        id=first_free;
        entry=&(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[id]);

        /* Search for free hwProfId */
        policer_prof_hwUnit    = &(MEA_Policer_Prof_hwUnit_table[hwUnit]);
        policer_prof_acm_hw    = &(policer_prof_hwUnit->acmTable[ACM_Mode_i]);
        
		for (hwProfId=1,policer_prof_hw=&(policer_prof_acm_hw->profTable[1]);
             hwProfId<policer_prof_hwUnit->num_of_profEntries;
             hwProfId++,policer_prof_hw++) {
            if (!policer_prof_hw->valid) {
                break;
            }
        }
        if (hwProfId>=policer_prof_hwUnit->num_of_profEntries) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - No free hwId \n",
                              __FUNCTION__);    
            return MEA_ERROR;
        } 
     
        /* Init the hwEntry */
        policer_prof_acm_hw->used_profEntries++;
        policer_prof_hw->policer_profile_id = id;
        MEA_OS_memset(policer_prof_hw->regs,0,sizeof(policer_prof_hw->regs[0])*policer_prof_hwUnit->num_of_regs);
        policer_prof_hw->valid              = MEA_TRUE;

    }
    

    /*update to HW*/
    entry->hwUnit        = hwUnit;
    entry->hwProfId      = hwProfId;
    MEA_OS_memset(&calculate_CBS_EBS,0,sizeof(calculate_CBS_EBS));
    if(mea_drv_policer_Profile_UpdateHw(unit_i,
                                        ACM_Mode_i,
                                        id,
                                        policer_entry_i,
                                        &calculate_CBS_EBS) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_policer_Profile_UpdateHw failed (ACM_Mode=%d,id=%d)\n",
                          __FUNCTION__,
                          ACM_Mode_i,
                          id);
        policer_prof_acm_hw->used_profEntries--;
        policer_prof_hw->valid  = MEA_FALSE;
        return MEA_ERROR;
    }

    /*update data*/
    MEA_OS_memset(entry,0,sizeof(*entry));
    MEA_OS_memcpy(&entry->rateMeter,policer_entry_i   ,sizeof(entry->rateMeter));
    MEA_OS_memcpy(&entry->Calculat ,&calculate_CBS_EBS,sizeof(entry->Calculat ));
    entry->fast_slow=fast_slow;
    entry->num_of_owners = 1;
    entry->hwUnit        = hwUnit;
    entry->hwProfId      = hwProfId;
    entry->valid         = MEA_TRUE;

    /* Update output variable */
    (*id_io)=id;



    //My_function_Checker(__FUNCTION__);
    return MEA_OK;
}

MEA_Status mea_drv_Policer_Profile_check_Before_Delete(MEA_Unit_t   unit_i,
                                                    MEA_AcmMode_t ACM_Mode_i,
                                                    MEA_Policer_prof_t    id_i)
{

    MEA_db_HwUnit_t                     hwUnit;
   
    MEA_Policer_Prof_dbt               *entry;

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return MEA_ERROR);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (ACM_Mode_i >= MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ACM_Mode_i %d >= max value %d (hwUnit=%d)\n",
            __FUNCTION__,
            ACM_Mode_i,
            MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries,
            hwUnit);
        return MEA_ERROR;
    }

    if (id_i >= MEA_POLICER_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id_i %d >= max_value %d \n",
            __FUNCTION__,id_i,MEA_POLICER_MAX_PROF);
        return MEA_ERROR;
    }


#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/


    entry=&(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[id_i]);
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check entry exist */
    if(entry->valid != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Policer profile id=%d not allocate (ACM_Mode_i=%d)\n",
            __FUNCTION__,id_i,ACM_Mode_i);    
        return MEA_ERROR;
    }

#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    /* Decrease num_of_owners , and if there is more owners simple return */
    
    if (entry->num_of_owners > 1) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - this profile %d have owners you need to delete the owners\n",
            __FUNCTION__,id_i);    
        return MEA_ERROR;
        
    }




    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_Delete>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_Delete(MEA_Unit_t            unit_i,
                                          MEA_AcmMode_t ACM_Mode_i,
                                          MEA_Policer_prof_t    id_i)
{
    MEA_db_HwUnit_t                     hwUnit;
    MEA_Policer_Entry_dbt               tmp_entry;
    MEA_Policer_CBS_EBS_dbt             tmp_calculate_CBS_EBS;
    MEA_Policer_Prof_dbt               *entry;
    MEA_Policer_Prof_hwUnit_dbt        *policer_prof_hwUnit;
    MEA_Policer_Prof_acm_hw_dbt        *policer_prof_acm_hw;
    MEA_Policer_Prof_hw_dbt            *policer_prof_hw;

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (ACM_Mode_i >= MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ACM_Mode_i %d >= max value %d (hwUnit=%d)\n",
                          __FUNCTION__,
                          ACM_Mode_i,
                          MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries,
                          hwUnit);
        return MEA_ERROR;
    }

    if (id_i >= MEA_POLICER_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i %d >= max_value %d \n",
                          __FUNCTION__,id_i,MEA_POLICER_MAX_PROF);
        return MEA_ERROR;
    }


#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
    

    entry=&(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[id_i]);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    /* Check entry exist */
    if(entry->valid != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Policer profile id=%d not allocate (ACM_Mode_i=%d)\n",
                          __FUNCTION__,id_i,ACM_Mode_i);    
        return MEA_ERROR;
    }

#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    /* Decrease num_of_owners , and if there is more owners simple return */
    entry->num_of_owners--;
    if (entry->num_of_owners > 0) {
        return MEA_OK;
    }

    /*write to profile index All zero*/
    MEA_OS_memset(&tmp_entry,0,sizeof(tmp_entry));
    MEA_OS_memset(&tmp_calculate_CBS_EBS,0,sizeof(tmp_calculate_CBS_EBS));
    if(mea_drv_policer_Profile_UpdateHw(unit_i,
                                        ACM_Mode_i,
                                        id_i,
                                        &tmp_entry,
                                        &tmp_calculate_CBS_EBS) != MEA_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Policer profile can't update %d/%d \n",__FUNCTION__,ACM_Mode_i,id_i);     
       entry->num_of_owners++;
       return MEA_ERROR;
    }

    /* Update hw database */
    policer_prof_hwUnit    = &(MEA_Policer_Prof_hwUnit_table[hwUnit]);
    policer_prof_acm_hw    = &(policer_prof_hwUnit->acmTable[ACM_Mode_i]);
    policer_prof_hw        = &(policer_prof_acm_hw->profTable[entry->hwProfId]);
    policer_prof_hw->valid = MEA_FALSE;
    policer_prof_acm_hw->used_profEntries--;

    /* Update Software database */
    MEA_OS_memset(entry,0,sizeof(*entry));
    

    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_Set>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_Set(MEA_Unit_t               unit_i,
                                       MEA_IngressPort_Proto_t  port_proto_prof_i,
                                       MEA_AcmMode_t    ACM_Mode_i,
                                       MEA_Policer_prof_t       id_i,
                                       MEA_Policer_Entry_dbt   *entry_i)
{

    MEA_Policer_CBS_EBS_dbt calculate_CBS_EBS;
    MEA_Policer_Prof_dbt* entry;
    MEA_db_HwUnit_t       hwUnit;
    MEA_Bool fast_slow;

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);
 
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (ACM_Mode_i >= MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ACM_Mode_i %d >= max value %d (hwUnit=%d)\n",
                          __FUNCTION__,
                          ACM_Mode_i,
                          MEA_Policer_Prof_hwUnit_table[hwUnit].num_of_acmEntries,
                          hwUnit);
        return MEA_ERROR;
    }

    if (id_i >= MEA_POLICER_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i %d >= max_value %d \n",
                          __FUNCTION__,id_i,MEA_POLICER_MAX_PROF);
        return MEA_ERROR;
    }

    if (entry_i== NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_i == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

                                           
    if (mea_drv_Policer_Profile_Check(unit_i,entry_i,&fast_slow) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Policer profile failed \n",__FUNCTION__);
        return MEA_ERROR; 
    }
    
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    entry = &(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[id_i]);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (entry->valid == MEA_FALSE){    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Policer profile id_io = %d is not exist (ACM_Mode_i=%d)\n",
                          __FUNCTION__,
                          id_i,
                          ACM_Mode_i);
        return MEA_ERROR;    
    }
    
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    /* Update Hardware */    
    MEA_OS_memset(&calculate_CBS_EBS,0,sizeof(calculate_CBS_EBS));
    if (mea_drv_policer_Profile_UpdateHw(unit_i,
                                         ACM_Mode_i,
                                         id_i,
                                         entry_i,
                                         &calculate_CBS_EBS)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Policer profile %d/%d set failed \n",
                          __FUNCTION__,ACM_Mode_i,id_i);
        return MEA_ERROR;
    }

    /* Update Software */    
    MEA_OS_memcpy (&entry->rateMeter,entry_i,sizeof(entry->rateMeter));
    MEA_OS_memcpy (&entry->Calculat,&calculate_CBS_EBS,sizeof(entry->Calculat));

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_Get>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_Get(MEA_Unit_t              unit_i,
                                       MEA_AcmMode_t   ACM_Mode_i,
                                       MEA_Policer_prof_t      id_i,
                                       MEA_Policer_Prof_dbt   *entry_o )
{
    MEA_Policer_Prof_dbt* entry;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (ACM_Mode_i >= MEA_MAX_NUM_OF_ACM_MODES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ACM_Mode_i %d >= max value %d \n",
                          __FUNCTION__,
                          ACM_Mode_i,
                          MEA_MAX_NUM_OF_ACM_MODES);
        return MEA_ERROR;
    }

    if (id_i >= MEA_POLICER_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i %d >= max_value %d \n",
                          __FUNCTION__,id_i,MEA_POLICER_MAX_PROF);
        return MEA_ERROR;
    }

#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
    
    entry = &(MEA_Policer_AcmProfile_Table[ACM_Mode_i].policer_profile_table[id_i]);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(entry->valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Policer profile id %d (ACM_Mode_i=%d) is not exist \n",
                          __FUNCTION__,id_i,ACM_Mode_i);
        return MEA_ERROR;
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    if (entry_o) {
        MEA_OS_memcpy(entry_o,entry,sizeof(*entry_o));
    }
 
    return MEA_OK;
}






/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_IsExist>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_IsExist(MEA_Unit_t             unit_i,
                                           MEA_AcmMode_t  ACM_Mode_i,
                                           MEA_Policer_prof_t     id_i,
                                           MEA_Bool              *exist_o){
    
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (ACM_Mode_i >= MEA_MAX_NUM_OF_ACM_MODES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ACM_Mode_i %d >= max value %d \n",
                          __FUNCTION__,
                          ACM_Mode_i,
                          MEA_MAX_NUM_OF_ACM_MODES);
        return MEA_ERROR;
    }

    if (id_i >= MEA_POLICER_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i %d >= max_value %d \n",
                          __FUNCTION__,id_i,MEA_POLICER_MAX_PROF);
        return MEA_ERROR;
    }


#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/



    /* Update output parameter */
    if (exist_o) {    
        MEA_Policer_AcmProf_dbt  *policer_acmProf;
        MEA_Policer_Prof_dbt     *entry;
        policer_acmProf = &(MEA_Policer_AcmProfile_Table[ACM_Mode_i]);
        entry = &(policer_acmProf->policer_profile_table[id_i]);
        (*exist_o) = entry->valid;
   }


    return MEA_OK;
}


MEA_Status mea_drv_Policer_Profile_Is_profile_exsit(MEA_Unit_t            unit_i,
                                                    MEA_db_HwUnit_t       hwUnit_i,
                                                    MEA_AcmMode_t  ACM_Mode,
                                                    MEA_Policer_prof_t    id_i ,
                                                    MEA_Bool *exsit)
{

    MEA_Policer_Prof_hwUnit_dbt        *policer_prof_hwUnit;
    
   
    policer_prof_hwUnit    = &(MEA_Policer_Prof_hwUnit_table[hwUnit_i]);


    if(exsit == NULL){
        return MEA_ERROR;
    }

    if(ACM_Mode > policer_prof_hwUnit->num_of_acmEntries){
        return MEA_ERROR;
    }

    if( id_i >= MEA_POLICER_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -*prof_id_io %d >= max_value %d \n",
            __FUNCTION__,id_i, MEA_POLICER_MAX_PROF);
        return MEA_ERROR;
    }

    

   *exsit = MEA_Policer_AcmProfile_Table[ACM_Mode].policer_profile_table[id_i].valid;

return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_GetHwProfId>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_GetHwProfId (MEA_Unit_t            unit_i,
                                                MEA_db_HwUnit_t       hwUnit_i,
                                                MEA_Policer_prof_t    id_i,
                                                MEA_db_HwId_t        *hwProfId_o)
{

    MEA_Policer_Prof_hwUnit_dbt        *policer_prof_hwUnit;
    MEA_db_HwId_t                       hwProfId;
    MEA_AcmMode_t               ACM_Mode;

    policer_prof_hwUnit    = &(MEA_Policer_Prof_hwUnit_table[hwUnit_i]);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS


    if (id_i >= MEA_POLICER_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i %d >= max_value %d \n",
                          __FUNCTION__,id_i,MEA_POLICER_MAX_PROF);
        return MEA_ERROR;
    }

#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    
    for(ACM_Mode=0;ACM_Mode<policer_prof_hwUnit->num_of_acmEntries;ACM_Mode++) {

        if (MEA_Policer_AcmProfile_Table[ACM_Mode].policer_profile_table[id_i].valid) {
            break;
        }
    }

    if (ACM_Mode>=policer_prof_hwUnit->num_of_acmEntries) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Profile %d is not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }

    if (hwUnit_i != MEA_Policer_AcmProfile_Table[ACM_Mode].policer_profile_table[id_i].hwUnit) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Profile %d/%d is not in same hwUnit  \n",
                          __FUNCTION__,ACM_Mode,id_i);
        return MEA_ERROR;
    }

    hwProfId = MEA_Policer_AcmProfile_Table[ACM_Mode].policer_profile_table[id_i].hwProfId;

    if (hwProfId_o) {
      *hwProfId_o = hwProfId;
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_GetDisableProfId>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Policer_prof_t  mea_drv_Policer_Profile_GetDisableProfId (MEA_db_HwUnit_t hwUnit_i)
{
    return MEA_Policer_Profile_Disable_Table[hwUnit_i];
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_GetHwDb>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Policer_Prof_hwUnit_dbt *mea_drv_Policer_Profile_GetHwDb()
{
    return MEA_Policer_Prof_hwUnit_table;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_Conclude>                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_Conclude(MEA_Unit_t unit_i)
{

    MEA_db_HwUnit_t           hwUnit;
    mea_memory_mode_e         save_globalMemoryMode;
    MEA_Policer_AcmProf_dbt  *acm_profile;
    MEA_AcmMode_t     ACM_Mode;


    if (MEA_Policer_Profile_Disable_Table) {
        for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
            MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
            for (ACM_Mode=0;
                 ACM_Mode<mea_drv_Get_DeviceInfo_NumOfAcmModes(unit_i,hwUnit);
                ACM_Mode++) {
                if (mea_drv_Policer_Profile_Delete(unit_i,
                                                   ACM_Mode,
                                                   MEA_Policer_Profile_Disable_Table[hwUnit]
                                                   ) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - mea_drv_Policer_Profile_Delete failed (ACM_Mode=%d,id=%d)\n",
                                     __FUNCTION__,ACM_Mode,MEA_Policer_Profile_Disable_Table[hwUnit]);
                    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                    return MEA_ERROR;
                }
            }
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        }
        MEA_OS_free(MEA_Policer_Profile_Disable_Table);
        MEA_Policer_Profile_Disable_Table = NULL;
    }
  
    for (ACM_Mode=0,acm_profile=MEA_Policer_AcmProfile_Table;
         ACM_Mode<MEA_MAX_NUM_OF_ACM_MODES;
         ACM_Mode++,acm_profile++) {

        if (acm_profile->policer_profile_table != NULL) {
            /* T.B.D - Go over all policer profile and clean them */
            MEA_OS_free(acm_profile->policer_profile_table);
            acm_profile->policer_profile_table=NULL;
        }

    }

    if (mea_drv_Policer_Profile_ConcludeHw(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Policer_Profile_ConcludeHw failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
    }


    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_Init>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_Init(MEA_Unit_t unit_i)
{
    
    MEA_Policer_Entry_dbt entry;
    MEA_Uint32            size;
    MEA_db_HwUnit_t       hwUnit;
    mea_memory_mode_e     save_globalMemoryMode;
    MEA_Policer_AcmProf_dbt* acm_profile;
    MEA_AcmMode_t ACM_Mode;


    if (mea_drv_Policer_Profile_InitHw(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Policer_Profile_InitHw failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
    }


    if (MEA_Policer_AcmProfile_Table == NULL) {
        size = MEA_MAX_NUM_OF_ACM_MODES * sizeof(MEA_Policer_AcmProf_dbt);
        MEA_Policer_AcmProfile_Table = (MEA_Policer_AcmProf_dbt*)MEA_OS_malloc(size);
        if (MEA_Policer_AcmProfile_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_Policer_AcmProfile_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        } 
        MEA_OS_memset((char*)MEA_Policer_AcmProfile_Table,0,size);
    }

    for (ACM_Mode=0,acm_profile=MEA_Policer_AcmProfile_Table;
         ACM_Mode<MEA_MAX_NUM_OF_ACM_MODES;
         ACM_Mode++,acm_profile++) {
        if (acm_profile->policer_profile_table == NULL) {
            size = MEA_POLICER_MAX_PROF * sizeof(MEA_Policer_Prof_dbt);
            acm_profile->policer_profile_table = (MEA_Policer_Prof_dbt*)MEA_OS_malloc(size);
            if (acm_profile->policer_profile_table == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Allocate acm_profile->policer_profile_table failed (ACM_mode=%dsize=%d)\n",
                                  __FUNCTION__,ACM_Mode,size);
                return MEA_ERROR;
            } 
            MEA_OS_memset((char*)acm_profile->policer_profile_table,0,size);
        }
    }

    if (MEA_Policer_Profile_Disable_Table == NULL) {
        size = mea_drv_num_of_hw_units * sizeof(MEA_Policer_prof_t);
        MEA_Policer_Profile_Disable_Table = (MEA_Policer_prof_t*)MEA_OS_malloc(size);
        if (MEA_Policer_Profile_Disable_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_Policer_Profile_Disable_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        } 
        MEA_OS_memset((char*)MEA_Policer_Profile_Disable_Table,0,size);

        for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
            MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
            for (ACM_Mode=0;
                 ACM_Mode<mea_drv_Get_DeviceInfo_NumOfAcmModes(unit_i,hwUnit);
                ACM_Mode++) {
                MEA_OS_memset((char*)&entry,0,sizeof(entry));
                if (ACM_Mode==0) {
                    MEA_Policer_Profile_Disable_Table[hwUnit] = 0;
                }
                if (mea_drv_Policer_Profile_Create(unit_i,
                                                   MEA_INGRESS_PORT_PROTO_TRANS,
                                                   &entry,
                                                   ACM_Mode,
                                                   &(MEA_Policer_Profile_Disable_Table[hwUnit])
                                                   ) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - mea_drv_Policer_Profile_Create failed (ACM_Mode=%d,hwUnit=%d)\n",
                                     __FUNCTION__,ACM_Mode,hwUnit);
                    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                    return MEA_ERROR;
                }
            }

            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        }
    }



    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Policer_Profile_ReInit>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Policer_Profile_ReInit(MEA_Unit_t unit_i)
{
    
    if (mea_drv_Policer_Profile_ReInitHw(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Policer_Profile_ReInitHw failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
    }

    return MEA_OK;
}






/************************************************************************/
/*   NEW API                                                            */
/************************************************************************/ 
MEA_Status MEA_API_Delete_Policer_ACM_Profile(MEA_Unit_t                unit,
                                              MEA_Uint16                    policer_id,
                                              MEA_AcmMode_t            ACM_Mode)
{
    if (policer_id >= MEA_POLICER_MAX_PROF) {
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - policer_id is out of range \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(ACM_Mode > MEA_MAX_NUM_OF_ACM_MODES-1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - policer_id is out of range \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Policer_Profile_check_Before_Delete(unit,ACM_Mode,policer_id)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - failed mea_drv_Policer_Profile_check_Before_Delete\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Policer_Profile_Delete(unit,ACM_Mode,policer_id)!=MEA_OK){
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Policer_Profile_Delete ACM_Mode  is out of range\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

return MEA_OK;
}

MEA_Status MEA_API_Create_Policer_ACM_Profile(MEA_Unit_t                    unit,
                                          MEA_Uint16                    *policer_id,
                                          MEA_AcmMode_t            ACM_Mode,
                                          MEA_IngressPort_Proto_t       port_proto_prof_i,
                                          MEA_Policer_Entry_dbt         *Entry_i )
{
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(Entry_i == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Entry_i  == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(policer_id == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - policer_id  == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
#endif

    if(mea_drv_Policer_Profile_Create(unit,port_proto_prof_i,Entry_i,ACM_Mode,policer_id)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Policer_Profile_Create Failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    return MEA_OK;
}
MEA_Status MEA_API_Set_Policer_ACM_Profile(MEA_Unit_t                       unit,
                                       MEA_Uint16                    policer_id,
                                       MEA_AcmMode_t            ACM_Mode,
                                       MEA_IngressPort_Proto_t       port_proto_prof_i,
                                       MEA_Policer_Entry_dbt         *Entry_i )
{
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(Entry_i == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Entry_i  == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
#endif

    if(mea_drv_Policer_Profile_Set(unit,port_proto_prof_i,ACM_Mode,policer_id,Entry_i)!=MEA_OK){
        // mea_drv_Policer_Profile_Set
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Policer_Profile_Set  failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    
   return MEA_OK;
}
MEA_Status MEA_API_Get_Policer_ACM_Profile(MEA_Unit_t                   unit,
                                       MEA_Uint16                       policer_id,
                                       MEA_AcmMode_t               ACM_Mode,
                                       MEA_Policer_Entry_dbt            *Entry_o )
{
MEA_Policer_Prof_dbt entry;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(Entry_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Entry_o  == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

#endif
MEA_OS_memset(&entry,0,sizeof(entry));
    if(mea_drv_Policer_Profile_Get(unit,ACM_Mode,policer_id,&entry )!=MEA_OK ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Policer_Profile_Get  failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

MEA_OS_memcpy(Entry_o,&entry.rateMeter,sizeof(*Entry_o));

    return MEA_OK;

}

MEA_Status MEA_API_GetFirst_Policer_ACM_Profile(MEA_Unit_t               unit,
                                                MEA_Uint16                   *policer_id,
                                                MEA_AcmMode_t           ACM_Mode,
                                                MEA_Policer_Entry_dbt       *Entry_o,
                                                MEA_Bool                    *found_o)
{
    MEA_Uint16  i,start;
    if(policer_id == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - policer_id == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(*policer_id == 0 || MEA_PLAT_GENERATE_NEW_ID){
      start=0;
    }else{
     start=*policer_id;
    }
  
    for(i=start; i< MEA_POLICER_MAX_PROF ;i++){
        if(mea_drv_Policer_Profile_IsExist(unit,ACM_Mode,i, found_o)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - pmea_drv_Policer_Profile_IsExist failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        if(*found_o== MEA_TRUE){
            *policer_id=i;
            break;
        }
    }
    
    if(Entry_o){
        if(MEA_API_Get_Policer_ACM_Profile(unit,*policer_id,ACM_Mode,Entry_o)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Get_Policer_ACM_Profile failed  \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

    }
return MEA_OK;
}

MEA_Status MEA_API_GetNext_Policer_ACM_Profile(MEA_Unit_t               unit,
                                               MEA_Uint16              *policer_id,
                                               MEA_AcmMode_t       ACM_Mode,
                                               MEA_Policer_Entry_dbt    *Entry_o,
                                               MEA_Bool                 *found_o)
{
    MEA_Uint16 id;
    if(policer_id==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - policer_id = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    id =(*policer_id) +1;

    return MEA_API_GetFirst_Policer_ACM_Profile(unit,&id,ACM_Mode,Entry_o,found_o);

}


