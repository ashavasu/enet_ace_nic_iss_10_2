/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"

#include "mea_if_regs.h"
#include "mea_policer_drv.h"
#include "mea_policer_profile_drv.h"
#include "mea_bm_ehp_drv.h"
#include "mea_bm_counters_drv.h"

#include "mea_filter_drv.h"
#include "mea_drv_common.h"
#include "mea_if_service_drv.h"
#include "mea_deviceInfo_drv.h"
#include "enet_queue_drv.h"
#include "mea_action_drv.h"
#include "mea_bm_wred_drv.h"

#include "mea_swe_ip_drv.h"


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_ACTION_PM_STATE 1
#define MEA_ACTION_TM_STATE 1
#define MEA_ACTION_ED_STATE 1

#define MEA_ACTION_AFDX_STATE 1

#define MEA_DRV_ACTION_AFDX_STATE_FUNCTION(func_name)                \
    MEA_Status func_name  ( MEA_Unit_t                   unit_i,       \
    MEA_Uint32                   state_i,      \
    MEA_Action_t                 action_id_i,  \
    MEA_Action_Entry_Data_dbt   *user_data_pi, \
    MEA_Action_Entry_Data_dbt   *db_data_pio,  \
    MEA_Bool                     create_i,     \
    MEA_Bool                     test_i) 

#define MEA_DRV_ACTION_PM_STATE_FUNCTION(func_name)                \
MEA_Status func_name  ( MEA_Unit_t                   unit_i,       \
                        MEA_Uint32                   state_i,      \
                        MEA_Action_t                 action_id_i,  \
                        MEA_Action_Entry_Data_dbt   *user_data_pi, \
                        MEA_Action_Entry_Data_dbt   *db_data_pio,  \
                        MEA_Bool                     create_i,     \
                        MEA_Bool                     test_i)    

#define MEA_DRV_ACTION_TM_STATE_FUNCTION(func_name)                \
MEA_Status func_name  ( MEA_Unit_t                   unit_i,       \
                        MEA_Uint32                   state_i,      \
                        MEA_Action_t                 action_id_i,  \
                        MEA_Action_Entry_Data_dbt   *user_data_pi, \
                        MEA_Action_Entry_Data_dbt   *db_data_pio,  \
                        MEA_Policer_Entry_dbt       *Action_policer_pi,\
                        MEA_Bool                     create_i)    

#define MEA_DRV_ACTION_ED_STATE_FUNCTION(func_name)                              \
MEA_Status func_name  ( MEA_Unit_t                            unit_i,            \
                        MEA_Uint32                            state_i,           \
                        MEA_Action_t                          action_id_i,       \
                        MEA_Action_Entry_Data_dbt            *user_data_pi,      \
                        mea_action_drv_dbt                   *db_pio,            \
                        MEA_EgressHeaderProc_Array_Entry_dbt *Action_editing_pi, \
                        MEA_Bool                              create_i         , \
                        MEA_Bool                              test_i          )    

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef MEA_DRV_ACTION_AFDX_STATE_FUNCTION((*mea_drv_action_afdx_state_func_t));

typedef MEA_DRV_ACTION_PM_STATE_FUNCTION((*mea_drv_action_pm_state_func_t));

typedef MEA_DRV_ACTION_TM_STATE_FUNCTION((*mea_drv_action_tm_state_func_t));

typedef MEA_DRV_ACTION_ED_STATE_FUNCTION((*mea_drv_action_ed_state_func_t));

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
/* MEA drv action afdx state functions */
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_notValid_user_notValid_auto);
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_notValid_user_notValid_specific);
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_notValid_user_valid_auto);
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_notValid_user_valid_specific);
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_Error_Internal);
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_valid_user_notValid_auto);
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_valid_user_notValid_specific);
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_valid_user_valid_auto);
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_valid_user_valid_specific);


/* MEA drv action pm state functions */
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_notValid_user_notValid_auto);
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_notValid_user_notValid_specific);
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_notValid_user_valid_auto);
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_notValid_user_valid_specific);
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_Error_Internal);
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_valid_user_notValid_auto);
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_valid_user_notValid_specific);
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_valid_user_valid_auto);
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_valid_user_valid_specific);

/* MEA drv action tm state functions */
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id0_data0);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id0_data1);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id1_data0);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id1_data1);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_valid_id0_data0);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_valid_id0_data1);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_valid_id1_data0);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_valid_id1_data1);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_Error_Internal);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_notValid_id0_data0);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_notValid_id0_data1);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_notValid_id1_data0);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_notValid_id1_data1);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_valid_id0_data0);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_valid_id0_data1);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_valid_id1_data0);
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_valid_id1_data1);


/* MEA drv action ed state functions */
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id0_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id0_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id1_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id1_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_valid_id0_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_valid_id0_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_valid_id1_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_valid_id1_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_Error_Internal);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_notValid_id0_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_notValid_id0_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_notValid_id1_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_notValid_id1_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_valid_id0_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_valid_id0_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_valid_id1_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_valid_id1_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_notValid_id0_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_notValid_id0_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_notValid_id1_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_notValid_id1_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_valid_id0_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_valid_id0_data1);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_valid_id1_data0);
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_valid_id1_data1);


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
extern MEA_editing_machin_dbt              MEA_EHP_ProtocolMachine[512];

MEA_Policer_prof_t MEA_ActionToCput_policer_profile_id;

/* driver action type that currently we are refer to it */
/* Note: This allowed us to use same APIs of action driver also 
         for service and filter , and not only for forwarder */
//mea_action_type_te     MEA_Action_drv_type = MEA_ACTION_TYPE_DEFAULT;

 MEA_Bool*   MEA_ACL5_Action_drv_Table = NULL;


/* Software Shadow Action Table */
mea_action_drv_dbt*     MEA_Action_drv_Table   = NULL;
//MEA_Uint32              myxxxTamp[10] = {0,0,0,0,0,0,0,0,0,0};
/* Hardware Shadow Action Table */ 
MEA_Action_HwEntry_dbt* MEA_Action_drv_HwTable = NULL;
MEA_Uint32              myxxxTamp1[10] = { 0,0,0,0,0,0,0,0,0,0 };
/* db */
MEA_db_dbt              MEA_Action_db = NULL;
//MEA_Uint32              myxxxTamp2[10] = { 0,0,0,0,0,0,0,0,0,0 };
/* MEA drv action afdx state table  */
mea_drv_action_afdx_state_func_t   MEA_drv_action_afdx_state_table[] = 
/* Index ,  db    , db   , user  , user    function  */ 
/*          valid , id   , valid   id                */
/*     0 ,  0     , 0    , 0     , 0   */{ &MEA_drv_action_afdx_state_db_notValid_user_notValid_auto,
/*     1 ,  0     , 0    , 0     , 1   */  &MEA_drv_action_afdx_state_db_notValid_user_notValid_specific,
/*     2 ,  0     , 0    , 1     , 0   */  &MEA_drv_action_afdx_state_db_notValid_user_valid_auto,
/*     3 ,  0     , 0    , 1     , 1   */  &MEA_drv_action_afdx_state_db_notValid_user_valid_specific,
/*     4 ,  0     , 1    , 0     , 0   */  &MEA_drv_action_afdx_state_Error_Internal,
/*     5 ,  0     , 1    , 0     , 1   */  &MEA_drv_action_afdx_state_Error_Internal,
/*     6 ,  0     , 1    , 1     , 0   */  &MEA_drv_action_afdx_state_Error_Internal,
/*     7 ,  0     , 1    , 1     , 1   */  &MEA_drv_action_afdx_state_Error_Internal,
/*     8 ,  1     , 0    , 0     , 0   */  &MEA_drv_action_afdx_state_Error_Internal,
/*     9 ,  1     , 0    , 0     , 1   */  &MEA_drv_action_afdx_state_Error_Internal,
/*    10 ,  1     , 0    , 1     , 0   */  &MEA_drv_action_afdx_state_Error_Internal,
/*    11 ,  1     , 0    , 1     , 1   */  &MEA_drv_action_afdx_state_Error_Internal,
/*    12 ,  1     , 1    , 0     , 0   */  &MEA_drv_action_afdx_state_db_valid_user_notValid_auto,
/*    13 ,  1     , 1    , 0     , 1   */  &MEA_drv_action_afdx_state_db_valid_user_notValid_specific,
/*    14 ,  1     , 1    , 1     , 0   */  &MEA_drv_action_afdx_state_db_valid_user_valid_auto,
/*    15 ,  1     , 1    , 1     , 1   */  &MEA_drv_action_afdx_state_db_valid_user_valid_specific};

 /* MEA drv action pm state table  */
mea_drv_action_pm_state_func_t   MEA_drv_action_pm_state_table[] = 
/* Index ,  db    , db   , user  , user    function  */ 
/*          valid , id   , valid   id                */
/*     0 ,  0     , 0    , 0     , 0   */{ &MEA_drv_action_pm_state_db_notValid_user_notValid_auto,
/*     1 ,  0     , 0    , 0     , 1   */  &MEA_drv_action_pm_state_db_notValid_user_notValid_specific,
/*     2 ,  0     , 0    , 1     , 0   */  &MEA_drv_action_pm_state_db_notValid_user_valid_auto,
/*     3 ,  0     , 0    , 1     , 1   */  &MEA_drv_action_pm_state_db_notValid_user_valid_specific,
/*     4 ,  0     , 1    , 0     , 0   */  &MEA_drv_action_pm_state_Error_Internal,
/*     5 ,  0     , 1    , 0     , 1   */  &MEA_drv_action_pm_state_Error_Internal,
/*     6 ,  0     , 1    , 1     , 0   */  &MEA_drv_action_pm_state_Error_Internal,
/*     7 ,  0     , 1    , 1     , 1   */  &MEA_drv_action_pm_state_Error_Internal,
/*     8 ,  1     , 0    , 0     , 0   */  &MEA_drv_action_pm_state_Error_Internal,
/*     9 ,  1     , 0    , 0     , 1   */  &MEA_drv_action_pm_state_Error_Internal,
/*    10 ,  1     , 0    , 1     , 0   */  &MEA_drv_action_pm_state_Error_Internal,
/*    11 ,  1     , 0    , 1     , 1   */  &MEA_drv_action_pm_state_Error_Internal,
/*    12 ,  1     , 1    , 0     , 0   */  &MEA_drv_action_pm_state_db_valid_user_notValid_auto,
/*    13 ,  1     , 1    , 0     , 1   */  &MEA_drv_action_pm_state_db_valid_user_notValid_specific,
/*    14 ,  1     , 1    , 1     , 0   */  &MEA_drv_action_pm_state_db_valid_user_valid_auto,
/*    15 ,  1     , 1    , 1     , 1   */  &MEA_drv_action_pm_state_db_valid_user_valid_specific};


/* MEA drv action tm state table  */
mea_drv_action_tm_state_func_t   MEA_drv_action_tm_state_table[] ={ 
/* Index ,  db    , db   , user  , user,  user ,   function  */ 
/*          valid , id   , valid , id  ,  data             */
/*     0 ,  0     , 0    , 0     , 0   ,  0   */ &MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id0_data0,
/*     1 ,  0     , 0    , 0     , 0   ,  1   */ &MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id0_data1,
/*     2 ,  0     , 0    , 0     , 1   ,  0   */ &MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id1_data0,
/*     3 ,  0     , 0    , 0     , 1   ,  1   */ &MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id1_data1,
/*     4 ,  0     , 0    , 1     , 0   ,  0   */ &MEA_drv_action_tm_state_db_notValid_id0_user_valid_id0_data0,
/*     5 ,  0     , 0    , 1     , 0   ,  1   */ &MEA_drv_action_tm_state_db_notValid_id0_user_valid_id0_data1,
/*     6 ,  0     , 0    , 1     , 1   ,  0   */ &MEA_drv_action_tm_state_db_notValid_id0_user_valid_id1_data0, /*create new*/
/*     7 ,  0     , 0    , 1     , 1   ,  1   */ &MEA_drv_action_tm_state_db_notValid_id0_user_valid_id1_data1, /* create */
/*     8 ,  0     , 1    , 0     , 0   ,  0   */ &MEA_drv_action_tm_state_Error_Internal,
/*     9 ,  0     , 1    , 0     , 0   ,  1   */ &MEA_drv_action_tm_state_Error_Internal,
/*    10 ,  0     , 1    , 0     , 1   ,  0   */ &MEA_drv_action_tm_state_Error_Internal,
/*    11 ,  0     , 1    , 0     , 1   ,  1   */ &MEA_drv_action_tm_state_Error_Internal,
/*    12 ,  0     , 1    , 1     , 0   ,  0   */ &MEA_drv_action_tm_state_Error_Internal,
/*    13 ,  0     , 1    , 1     , 0   ,  1   */ &MEA_drv_action_tm_state_Error_Internal,
/*    14 ,  0     , 1    , 1     , 1   ,  0   */ &MEA_drv_action_tm_state_Error_Internal,
/*    15 ,  0     , 1    , 1     , 1   ,  1   */ &MEA_drv_action_tm_state_Error_Internal,
/*    16 ,  1     , 0    , 0     , 0   ,  0   */ &MEA_drv_action_tm_state_Error_Internal,
/*    17 ,  1     , 0    , 0     , 0   ,  1   */ &MEA_drv_action_tm_state_Error_Internal,
/*    18 ,  1     , 0    , 0     , 1   ,  0   */ &MEA_drv_action_tm_state_Error_Internal,
/*    19 ,  1     , 0    , 0     , 1   ,  1   */ &MEA_drv_action_tm_state_Error_Internal,
/*    20 ,  1     , 0    , 1     , 0   ,  0   */ &MEA_drv_action_tm_state_Error_Internal,
/*    21 ,  1     , 0    , 1     , 0   ,  1   */ &MEA_drv_action_tm_state_Error_Internal,
/*    22 ,  1     , 0    , 1     , 1   ,  0   */ &MEA_drv_action_tm_state_Error_Internal,
/*    23 ,  1     , 0    , 1     , 1   ,  1   */ &MEA_drv_action_tm_state_Error_Internal,
/*    24 ,  1     , 1    , 0     , 0   ,  0   */ &MEA_drv_action_tm_state_db_valid_id1_user_notValid_id0_data0,/* Delete*/
/*    25 ,  1     , 1    , 0     , 0   ,  1   */ &MEA_drv_action_tm_state_db_valid_id1_user_notValid_id0_data1,/* Delete*/
/*    26 ,  1     , 1    , 0     , 1   ,  0   */ &MEA_drv_action_tm_state_db_valid_id1_user_notValid_id1_data0,/* Delete*/
/*    27 ,  1     , 1    , 0     , 1   ,  1   */ &MEA_drv_action_tm_state_db_valid_id1_user_notValid_id1_data1,/* Delete*/
/*    28 ,  1     , 1    , 1     , 0   ,  0   */ &MEA_drv_action_tm_state_db_valid_id1_user_valid_id0_data0,
/*    29 ,  1     , 1    , 1     , 0   ,  1   */ &MEA_drv_action_tm_state_db_valid_id1_user_valid_id0_data1,
/*    30 ,  0     , 1    , 1     , 1   ,  0   */ &MEA_drv_action_tm_state_db_valid_id1_user_valid_id1_data0,
/*    31 ,  0     , 1    , 1     , 1   ,  1   */ &MEA_drv_action_tm_state_db_valid_id1_user_valid_id1_data1};


/* MEA drv action ed state table  */
mea_drv_action_ed_state_func_t   MEA_drv_action_ed_state_table[] ={ 
/* Index ,  db    , db   , user  , user,  user ,   function  */ 
/*          valid , id   , valid , id  ,  data             */
/*     0 ,  0     , 0    , 0     , 0   ,  0   */ &MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id0_data0,
/*     1 ,  0     , 0    , 0     , 0   ,  1   */ &MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id0_data1,
/*     2 ,  0     , 0    , 0     , 1   ,  0   */ &MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id1_data0,
/*     3 ,  0     , 0    , 0     , 1   ,  1   */ &MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id1_data1,
/*     4 ,  0     , 0    , 1     , 0   ,  0   */ &MEA_drv_action_ed_state_db_notValid_id0_user_valid_id0_data0,
/*     5 ,  0     , 0    , 1     , 0   ,  1   */ &MEA_drv_action_ed_state_db_notValid_id0_user_valid_id0_data1,
/*     6 ,  0     , 0    , 1     , 1   ,  0   */ &MEA_drv_action_ed_state_db_notValid_id0_user_valid_id1_data0, /*create new*/
/*     7 ,  0     , 0    , 1     , 1   ,  1   */ &MEA_drv_action_ed_state_db_notValid_id0_user_valid_id1_data1, /* create */
/*     8 ,  0     , 1    , 0     , 0   ,  0   */ &MEA_drv_action_ed_state_Error_Internal,
/*     9 ,  0     , 1    , 0     , 0   ,  1   */ &MEA_drv_action_ed_state_Error_Internal,
/*    10 ,  0     , 1    , 0     , 1   ,  0   */ &MEA_drv_action_ed_state_Error_Internal,
/*    11 ,  0     , 1    , 0     , 1   ,  1   */ &MEA_drv_action_ed_state_Error_Internal,
/*    12 ,  0     , 1    , 1     , 0   ,  0   */ &MEA_drv_action_ed_state_Error_Internal,
/*    13 ,  0     , 1    , 1     , 0   ,  1   */ &MEA_drv_action_ed_state_Error_Internal,
/*    14 ,  0     , 1    , 1     , 1   ,  0   */ &MEA_drv_action_ed_state_Error_Internal,
/*    15 ,  0     , 1    , 1     , 1   ,  1   */ &MEA_drv_action_ed_state_Error_Internal,
/*    16 ,  1     , 0    , 0     , 0   ,  0   */ &MEA_drv_action_ed_state_db_valid_id0_user_notValid_id0_data0,
/*    17 ,  1     , 0    , 0     , 0   ,  1   */ &MEA_drv_action_ed_state_db_valid_id0_user_notValid_id0_data1,
/*    18 ,  1     , 0    , 0     , 1   ,  0   */ &MEA_drv_action_ed_state_db_valid_id0_user_notValid_id1_data0,
/*    19 ,  1     , 0    , 0     , 1   ,  1   */ &MEA_drv_action_ed_state_db_valid_id0_user_notValid_id1_data1,
/*    20 ,  1     , 0    , 1     , 0   ,  0   */ &MEA_drv_action_ed_state_db_valid_id0_user_valid_id0_data0,
/*    21 ,  1     , 0    , 1     , 0   ,  1   */ &MEA_drv_action_ed_state_db_valid_id0_user_valid_id0_data1,
/*    22 ,  1     , 0    , 1     , 1   ,  0   */ &MEA_drv_action_ed_state_db_valid_id0_user_valid_id1_data0,
/*    23 ,  1     , 0    , 1     , 1   ,  1   */ &MEA_drv_action_ed_state_db_valid_id0_user_valid_id1_data1,
/*    24 ,  1     , 1    , 0     , 0   ,  0   */ &MEA_drv_action_ed_state_db_valid_id1_user_notValid_id0_data0,
/*    25 ,  1     , 1    , 0     , 0   ,  1   */ &MEA_drv_action_ed_state_db_valid_id1_user_notValid_id0_data1,
/*    26 ,  1     , 1    , 0     , 1   ,  0   */ &MEA_drv_action_ed_state_db_valid_id1_user_notValid_id1_data0,
/*    27 ,  1     , 1    , 0     , 1   ,  1   */ &MEA_drv_action_ed_state_db_valid_id1_user_notValid_id1_data1,
/*    28 ,  1     , 1    , 1     , 0   ,  0   */ &MEA_drv_action_ed_state_db_valid_id1_user_valid_id0_data0,
/*    29 ,  1     , 1    , 1     , 0   ,  1   */ &MEA_drv_action_ed_state_db_valid_id1_user_valid_id0_data1,
/*    30 ,  0     , 1    , 1     , 1   ,  0   */ &MEA_drv_action_ed_state_db_valid_id1_user_valid_id1_data0,
/*    31 ,  0     , 1    , 1     , 1   ,  1   */ &MEA_drv_action_ed_state_db_valid_id1_user_valid_id1_data1};


/*----------------------------------------------------------------------------*/
/*             Implementation                                                 */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             Local functions                                                */
/*----------------------------------------------------------------------------*/
#if 1 /*afdx_state*/
/*---------------------------------------------------------------------------*/
/*            <MEA_drv_action_afdx_state_db_notValid_user_notValid_auto>       */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_notValid_user_notValid_auto)
{

    /* This state Do_nothing*/
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*            <MEA_drv_action_afdx_state_db_notValid_user_notValid_specific>  */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_notValid_user_notValid_specific)
{

    /* This state Do_nothing*/
    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*      2     <MEA_drv_action_pm_state_db_notValid_user_valid_auto>          */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_notValid_user_valid_auto)
{

   MEA_RxAfdxBag_dbt    afdx_entry;
   MEA_Uint16 afdx_Rx_sessionId=(MEA_Uint16)user_data_pi->afdx_Rx_sessionId;

    if(test_i == MEA_TRUE){
       

        return MEA_OK;
    }
    
    MEA_OS_memset(&afdx_entry,0,sizeof(afdx_entry));
    afdx_entry.Bag=user_data_pi->afdx_Bag_info;

    if(mea_drv_Rx_AfdxBag_Create_Ingress(unit_i,&afdx_entry,&afdx_Rx_sessionId)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -AfdxBag_Create %d failed\n",
            __FUNCTION__, user_data_pi->afdx_Rx_sessionId);

        return MEA_ERROR;
    }

    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*  pm_state 3    <MEA_drv_action_pm_state_db_notValid_user_valid_specific>  */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_notValid_user_valid_specific)
{

    
    MEA_RxAfdxBag_dbt    afdx_entry;
    MEA_Uint16 afdx_Rx_sessionId=(MEA_Uint16)user_data_pi->afdx_Rx_sessionId;

    if(test_i == MEA_TRUE){
        
        if(mea_drv_Rx_AfdxBag_Get_Ingress(unit_i,
            (MEA_Uint16)user_data_pi->afdx_Rx_sessionId,
            &afdx_entry)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- AfdxBag_Get, id %d\n",
                    __FUNCTION__,user_data_pi->afdx_Rx_sessionId);
                return MEA_ERROR;
        }
        if(user_data_pi->afdx_Bag_info != afdx_entry.Bag){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- afdx_entry->Bag diff %d != %d\n",
                __FUNCTION__,user_data_pi->afdx_Bag_info,afdx_entry.Bag);
            return MEA_ERROR;
        }


        return MEA_OK;
    }
    
    /*Add owner*/
    MEA_OS_memset(&afdx_entry,0,sizeof(afdx_entry));
    afdx_entry.Bag=user_data_pi->afdx_Bag_info;

    if(mea_drv_Rx_AfdxBag_Create_Ingress(unit_i,&afdx_entry,&afdx_Rx_sessionId)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -AfdxBag_Create %d failed\n",
            __FUNCTION__, user_data_pi->afdx_Rx_sessionId);

        return MEA_ERROR;
    }

    

    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*     pm_state 4 to 11    <MEA_drv_action_afdx_state_Error_Internal>          */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_Error_Internal)
{

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s \n- action afdx state error in state %d \n",
        __FUNCTION__,state_i);

    return MEA_ERROR;
}


/*---------------------------------------------------------------------------*/
/*     pm_state  12  <MEA_drv_action_afdx_state_db_valid_user_notValid_auto>   */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_valid_user_notValid_auto)
{

    if(test_i == MEA_TRUE){
       
         

        return MEA_OK;
    }


    if(mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i, (MEA_PmId_t)db_data_pio->afdx_Rx_sessionId))

    {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s \n- we can't delete the older afdx_Rx_sessionId  %d \n",
            __FUNCTION__,db_data_pio->afdx_Rx_sessionId);
        return MEA_ERROR; 
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*  pm_state 13   <MEA_drv_action_afdx_state_db_valid_user_notValid_specific>  */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_valid_user_notValid_specific)
{

    if(test_i == MEA_TRUE){
       
        return MEA_OK;
    }

    /* Delete the old PM */
    if(mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i, (MEA_PmId_t)db_data_pio->afdx_Rx_sessionId))

    {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s \n- we can't delete the older afdx_Rx_sessionId  %d \n",
            __FUNCTION__,db_data_pio->afdx_Rx_sessionId);
        return MEA_ERROR; 
    }

    
    
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*  pm_state 14  <MEA_drv_action_afdx_state_db_valid_user_valid_auto>          */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_valid_user_valid_auto)
{

    MEA_RxAfdxBag_dbt    afdx_entry;
    MEA_Uint16 afdx_Rx_sessionId=(MEA_Uint16)user_data_pi->afdx_Rx_sessionId;
   

    if(test_i == MEA_TRUE){
        

        return MEA_OK;
    }

    
    
    MEA_OS_memset(&afdx_entry,0,sizeof(afdx_entry));
    afdx_entry.Bag=user_data_pi->afdx_Bag_info;
    /* Create new afdx id */
    
    
    if(mea_drv_Rx_AfdxBag_Create_Ingress(unit_i,&afdx_entry,&afdx_Rx_sessionId)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -AfdxBag_Create %d failed\n",
            __FUNCTION__, user_data_pi->afdx_Rx_sessionId);

        return MEA_ERROR;
    }

    
    /* Delete the old afdx */
    if(mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i, (MEA_PmId_t)db_data_pio->afdx_Rx_sessionId))

    {
        mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i,(MEA_Uint16)user_data_pi->afdx_Rx_sessionId); /* Delete the new pm that we already created */
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s \n- we can't delete the older afdx_Rx_sessionId  %d \n",
            __FUNCTION__,db_data_pio->afdx_Rx_sessionId);
        return MEA_ERROR; 
    }

    /* Set the db to the new pm id */
   

    /* Return to caller */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*    pm_state 15  <MEA_drv_action_afdx_state_db_valid_user_valid_specific>    */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_AFDX_STATE_FUNCTION(MEA_drv_action_afdx_state_db_valid_user_valid_specific)
{

    
    MEA_RxAfdxBag_dbt    afdx_entry;
    MEA_Uint16 afdx_Rx_sessionId=(MEA_Uint16)user_data_pi->afdx_Rx_sessionId;


    if((user_data_pi->afdx_Rx_sessionId == db_data_pio->afdx_Rx_sessionId) &&
        (user_data_pi->afdx_Bag_info    == db_data_pio->afdx_Bag_info)){
       
        /*do nothing */
        return MEA_OK;
    }

    if(test_i == MEA_TRUE){
        
        if((user_data_pi->afdx_Rx_sessionId == db_data_pio->afdx_Rx_sessionId) &&
            (user_data_pi->afdx_Bag_info    != db_data_pio->afdx_Bag_info)){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - sessionId %d equal bat the afdx_Bag_info is different\n",
                    __FUNCTION__, user_data_pi->afdx_Rx_sessionId);
                
                return MEA_ERROR;
        }
        if((user_data_pi->afdx_Rx_sessionId != db_data_pio->afdx_Rx_sessionId)){
            // its ok we need need to change the id create and delete.
        }

        return MEA_OK;
    }

    

    MEA_OS_memset(&afdx_entry,0,sizeof(afdx_entry));
    afdx_entry.Bag=user_data_pi->afdx_Bag_info;
    /* Create new afdx id */


    if(mea_drv_Rx_AfdxBag_Create_Ingress(unit_i,&afdx_entry,&afdx_Rx_sessionId)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -AfdxBag_Create %d failed\n",
            __FUNCTION__, user_data_pi->afdx_Rx_sessionId);

        return MEA_ERROR;
    }

    




    /* Delete the old PM */
    if(mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i, (MEA_PmId_t)db_data_pio->afdx_Rx_sessionId))

   {
           mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i,(MEA_Uint16)user_data_pi->afdx_Rx_sessionId); /* Delete the new pm that we already created */
            MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                "%s \n- we can't delete the older afdx_Rx_sessionId  %d \n",
                __FUNCTION__,db_data_pio->afdx_Rx_sessionId);
            return MEA_ERROR; 
    }

    /* Set the db to the new pm id */
    

    /* Return to caller */
    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*            <MEA_drv_action_afdx_state_Setting>                              */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_drv_action_afdx_state_Setting
( MEA_Unit_t                   unit_i,                             
 MEA_Action_t                 action_id_i,
 MEA_Action_Entry_Data_dbt   *user_data_pi,
 MEA_Action_Entry_Data_dbt   *db_data_pio, 
 MEA_Bool                     create_i,
 MEA_Bool                     test_i)
{

    MEA_Uint32          state;
    MEA_Bool          afdx_exist=MEA_FALSE;
   if (!MEA_AFDX_SUPPORT)
   {
       return MEA_OK;
   }


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS    
    if (user_data_pi == NULL) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - user_data_pi = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }
    if (db_data_pio == NULL) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - db_data_pio = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

    if(mea_drv_Rx_AfdxBag_prof_IsRange(unit_i,(MEA_Uint16) user_data_pi->afdx_Rx_sessionId)!= MEA_TRUE){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,"%s - afdx_Rx_sessionId  out of range\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

#endif   
      if (user_data_pi->afdx_enable)
            mea_drv_Rx_AfdxBag_prof_IsExist(unit_i,(MEA_Uint16)(user_data_pi->afdx_Rx_sessionId),&afdx_exist);

    /* Build the state */
    state      = 0x00000000;
    if (afdx_exist  == MEA_TRUE) {
        state |= 0x00000001;
    }
    if (user_data_pi->afdx_enable) {
        state |= 0x00000002;
    }
    if(!create_i){
        afdx_exist=MEA_FALSE;
        if (db_data_pio->afdx_enable)
             mea_drv_Rx_AfdxBag_prof_IsExist(unit_i,(MEA_Uint16)(db_data_pio->afdx_Rx_sessionId),&afdx_exist);

        if (afdx_exist  == MEA_TRUE) {
            state |= 0x00000004;
        }
        if (db_data_pio->afdx_enable) {
            state |= 0x00000008;
        }
    }

    /* Call to the function according to the state */
    if (MEA_drv_action_afdx_state_table[state](unit_i,
        state,
        action_id_i,
        user_data_pi,
        db_data_pio,
        create_i,
        test_i) != MEA_OK) {
            MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                "%s \n- Error: MEA_drv_action_pm_state_table failed state %d \n",
                __FUNCTION__,
                state);
            return MEA_ERROR;
    } 

    /* Return to caller */      
    return MEA_OK;

}



#endif /*afdx_state*/

#if 1 /*pm_state*/
/*---------------------------------------------------------------------------*/
/*            <MEA_drv_action_pm_state_db_notValid_user_notValid_auto>       */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_notValid_user_notValid_auto)
{
    
     /* This state Do_nothing*/
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*            <MEA_drv_action_pm_state_db_notValid_user_notValid_specific>  */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_notValid_user_notValid_specific)
{
                                    
    /* This state Do_nothing*/
    return MEA_OK;
    
}


 
/*---------------------------------------------------------------------------*/
/*      2     <MEA_drv_action_pm_state_db_notValid_user_valid_auto>          */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_notValid_user_valid_auto)
{
    
   MEA_PmId_t    pm_id;

   if(test_i == MEA_TRUE){
       if ( !mea_drv_IsPmIdInRange((MEA_PmId_t)user_data_pi->pm_id) )
        {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- non valid id_io %d\n",
                           __FUNCTION__, user_data_pi->pm_id);
        return MEA_ERROR;    
        }
       /* check if we have place to Add new*/
       if(mea_drv_ispm_freeEntry(unit_i,user_data_pi->pm_type)==MEA_FALSE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- PM db is full \n",
                           __FUNCTION__);
        return MEA_ERROR;
       }
   
    return MEA_OK;
   }

   pm_id = (MEA_PmId_t)user_data_pi->pm_id;
   if ( mea_drv_Set_PM(unit_i, &pm_id,user_data_pi->pm_type, MEA_TRUE) != MEA_OK ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- mea_drv_Set_PM, id %d\n",
                          __FUNCTION__,pm_id);
        return MEA_ERROR;
   }
   db_data_pio->pm_id       = pm_id;
   db_data_pio->pm_id_valid = MEA_TRUE;

   return MEA_OK;
    
}

/*---------------------------------------------------------------------------*/
/*  pm_state 3    <MEA_drv_action_pm_state_db_notValid_user_valid_specific>  */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_notValid_user_valid_specific)
{

   MEA_PmId_t    pm_id;

   if(test_i == MEA_TRUE){
       if ( !mea_drv_IsPmIdInRange((MEA_PmId_t)user_data_pi->pm_id) )
        {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- non valid id_io %d\n",
                           __FUNCTION__, user_data_pi->pm_id);
        return MEA_ERROR;    
        }
       /* check if we have place to Add new*/
       if(user_data_pi->pm_id == 0 && mea_drv_ispm_freeEntry(unit_i,user_data_pi->pm_type)== MEA_FALSE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- PM db is full \n",
                           __FUNCTION__);
        return MEA_ERROR;
       }
       
    return MEA_OK;
   }
   
   
   pm_id = (MEA_PmId_t)user_data_pi->pm_id;
   if ( mea_drv_Set_PM(unit_i, &pm_id, user_data_pi->pm_type,MEA_TRUE) != MEA_OK ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- mea_drv_Set_PM, id %d\n",
                          __FUNCTION__,pm_id);
        return MEA_ERROR;
   }
   db_data_pio->pm_id       = pm_id;
   db_data_pio->pm_id_valid = MEA_TRUE;

   return MEA_OK;
     
}

/*---------------------------------------------------------------------------*/
/*     pm_state 4 to 11    <MEA_drv_action_pm_state_Error_Internal>          */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_Error_Internal)
{
    
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s \n- action pm state error in state %d \n",
                                  __FUNCTION__,state_i);
    
    return MEA_ERROR;
}


/*---------------------------------------------------------------------------*/
/*     pm_state  12  <MEA_drv_action_pm_state_db_valid_user_notValid_auto>   */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_valid_user_notValid_auto)
{

    if(test_i == MEA_TRUE){
       if ( !mea_drv_IsPmIdInRange((MEA_PmId_t)db_data_pio->pm_id) )
        {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- non valid id_io %d\n",
                           __FUNCTION__, db_data_pio->pm_id);
        return MEA_ERROR;    
        }
       if(mea_drv_IsPm_Exist(unit_i,(MEA_PmId_t) db_data_pio->pm_id)==MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- can't pm is not exist %d\n",
                           __FUNCTION__, db_data_pio->pm_id);
        return MEA_ERROR;
       }
   
    return MEA_OK;
   }
    
    
    if (mea_drv_Delete_PM(unit_i, 
                          (MEA_PmId_t)db_data_pio->pm_id) 
                             != MEA_OK ) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_Delete_PM failed (ActionId=%d , pmId=%d) \n",
                           __FUNCTION__,
                           action_id_i,
                           db_data_pio->pm_id);
        return MEA_ERROR; 
    }
    db_data_pio->pm_id_valid = MEA_FALSE;
    db_data_pio->pm_id=user_data_pi->pm_id;

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*  pm_state 13   <MEA_drv_action_pm_state_db_valid_user_notValid_specific>  */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_valid_user_notValid_specific)
{

   if(test_i == MEA_TRUE){
       if ( !mea_drv_IsPmIdInRange((MEA_PmId_t)db_data_pio->pm_id) )
        {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - non valid id_io %d\n",
                           __FUNCTION__, db_data_pio->pm_id);
        return MEA_ERROR;    
        }
       if(mea_drv_IsPm_Exist(unit_i,(MEA_PmId_t) db_data_pio->pm_id)==MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- pm is not exist can't delete %d\n",
                           __FUNCTION__, db_data_pio->pm_id);
        return MEA_ERROR;
       }
   
    return MEA_OK;
   }
    
    

    if (mea_drv_Delete_PM(unit_i,(MEA_PmId_t)db_data_pio->pm_id)!= MEA_OK ) 
    {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_Delete_PM failed (ActionId=%d , pmId=%d) \n",
                           __FUNCTION__,
                           action_id_i,
                           db_data_pio->pm_id);
        return MEA_ERROR; 
    }
    db_data_pio->pm_id_valid = MEA_FALSE;
    db_data_pio->pm_id=user_data_pi->pm_id;

    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*  pm_state 14  <MEA_drv_action_pm_state_db_valid_user_valid_auto>          */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_valid_user_valid_auto)
{

   MEA_PmId_t    pm_id;

   if(test_i == MEA_TRUE){
       if ( !mea_drv_IsPmIdInRange((MEA_PmId_t)user_data_pi->pm_id) )
        {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - non valid id_io %d\n",
                           __FUNCTION__, user_data_pi->pm_id);
        return MEA_ERROR;    
        }
       /* check if we have place to Add new*/
       if(mea_drv_ispm_freeEntry(unit_i,user_data_pi->pm_type)== MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- PM db is full \n",
                           __FUNCTION__);
        return MEA_ERROR;
       }
   
    return MEA_OK;
   }
   
   
   
   /* Create new pm_id */
    pm_id = (MEA_PmId_t)user_data_pi->pm_id;
    if ( mea_drv_Set_PM(unit_i, &pm_id, user_data_pi->pm_type,MEA_TRUE) != MEA_OK ) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- mea_drv_Set_PM, id %d\n",
                           __FUNCTION__,pm_id);
         return MEA_ERROR;
   }

   /* Delete the old PM */
   if (mea_drv_Delete_PM(unit_i, 
                           (MEA_PmId_t)db_data_pio->pm_id) 
                            != MEA_OK ) {
        mea_drv_Delete_PM(unit_i,pm_id); /* Delete the new pm that we already creted */
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_Delete_PM failed (ActionId=%d , pmId=%d) \n",
                           __FUNCTION__,
                           action_id_i,
                           db_data_pio->pm_id);
        return MEA_ERROR; 
    }

    /* Set the db to the new pm id */
    db_data_pio->pm_id       = pm_id;
    db_data_pio->pm_id_valid = MEA_TRUE;

    /* Return to caller */
    return MEA_OK;
    
}

/*---------------------------------------------------------------------------*/
/*    pm_state 15  <MEA_drv_action_pm_state_db_valid_user_valid_specific>    */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_PM_STATE_FUNCTION(MEA_drv_action_pm_state_db_valid_user_valid_specific)
{

    MEA_PmId_t    pm_id;
    
    if(user_data_pi->pm_id == db_data_pio->pm_id){
        if (user_data_pi->pm_type != db_data_pio->pm_type) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- non allowed to change pm_type to exist pm %d\n",
                           __FUNCTION__, user_data_pi->pm_id);
            return MEA_ERROR;
        }
        /*do nothing */
        return MEA_OK;
    }
    if(test_i == MEA_TRUE){
       if ( !mea_drv_IsPmIdInRange((MEA_PmId_t)user_data_pi->pm_id) )
        {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - non valid id_io %d\n",
                           __FUNCTION__, user_data_pi->pm_id);
        return MEA_ERROR;    
        }
       /* check if we have place to Add new*/
       if(mea_drv_IsPm_Exist(unit_i,(MEA_PmId_t) user_data_pi->pm_id)==MEA_FALSE){
        
            if(mea_drv_ispm_freeEntry(unit_i,user_data_pi->pm_type)== MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- PM db is full \n",
                           __FUNCTION__);
            return MEA_ERROR;
       }
       }
   
    return MEA_OK;
   }

    /* Create new pm_id with the request pm_id */
    pm_id = (MEA_PmId_t)user_data_pi->pm_id;
    if ( mea_drv_Set_PM(unit_i, &pm_id,user_data_pi->pm_type ,MEA_TRUE) != MEA_OK ) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- mea_drv_Set_PM, id %d\n",
                           __FUNCTION__,pm_id);
         return MEA_ERROR;
   }

   /* Delete the old PM */
   if (mea_drv_Delete_PM(unit_i, 
                           (MEA_PmId_t)db_data_pio->pm_id) 
                            != MEA_OK ) {
        mea_drv_Delete_PM(unit_i,pm_id); /* Delete the new pm that we already creted */
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_Delete_PM failed (ActionId=%d , pmId=%d) \n",
                           __FUNCTION__,
                           action_id_i,
                           db_data_pio->pm_id);
        return MEA_ERROR; 
    }

    /* Set the db to the new pm id */
    db_data_pio->pm_id       = pm_id;
    db_data_pio->pm_id_valid = MEA_TRUE;

    /* Return to caller */
    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*            <MEA_drv_action_pm_state_Setting>                              */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_drv_action_pm_state_Setting
                                      ( MEA_Unit_t                   unit_i,                             
                                        MEA_Action_t                 action_id_i,
                                        MEA_Action_Entry_Data_dbt   *user_data_pi,
                                        MEA_Action_Entry_Data_dbt   *db_data_pio, 
                                        MEA_Bool                     create_i,
                                        MEA_Bool                     test_i)
{

    MEA_Uint32          state;
     
    

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS    
    if (user_data_pi == NULL) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s - user_data_pi = NULL  \n",
                           __FUNCTION__);
        return MEA_ERROR; 
    }
    if (db_data_pio == NULL) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s - db_data_pio = NULL  \n",
                           __FUNCTION__);
        return MEA_ERROR; 
    }
#endif   
    

    /* Build the state */
    state      = 0x00000000;
    if (user_data_pi->pm_id != 0) {
        state |= 0x00000001;
    }
    if (user_data_pi->pm_id_valid) {
        state |= 0x00000002;
    }
    if(!create_i){
        if (db_data_pio->pm_id != 0) {
            state |= 0x00000004;
        }
        if (db_data_pio->pm_id_valid) {
            state |= 0x00000008;
        }
    }

    /* Call to the function according to the state */
    if (MEA_drv_action_pm_state_table[state](unit_i,
                                             state,
                                               action_id_i,
                                             user_data_pi,
                                             db_data_pio,
                                             create_i,
                                             test_i) != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- Error: MEA_drv_action_pm_state_table failed state %d \n",
                           __FUNCTION__,
                           state);
       return MEA_ERROR;
    } 
    
    /* Return to caller */      
    return MEA_OK;

}



#endif /*pm_state*/
/*---------------------------------------------------------------------------------------------*/
/* tm_state 0 <MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id0_data0>                */
/*---------------------------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id0_data0)
{
    /*Do nothing*/
    return MEA_OK;
    
}

/*---------------------------------------------------------------------------*/
/*    1 <MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id0_data1>    */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id0_data1)
{
    /*Do nothing*/
    return MEA_OK;
    
}

/*---------------------------------------------------------------------------*/
/*      2  <MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id0_data1>    */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id1_data0)
{
    /*Do nothing*/
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*    3 <MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id1_data1>    */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_notValid_id1_data1)
{
    /*Do nothing*/
    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*   4  <MEA_drv_action_tm_state_db_notValid_id0_user_valid_id0_data0>       */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_valid_id0_data0)
{
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                      "%s \n- Missing input data for policing (Action_policer_pi==NULL)\n",
                      __FUNCTION__,
                      state_i);
    return MEA_ERROR;
}

/*---------------------------------------------------------------------------*/
/*    5 <MEA_drv_action_tm_state_db_notValid_id0_user_valid_id0_data1>       */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_valid_id0_data1)
{
    MEA_TmId_t              tm_id;   
    MEA_Policer_prof_t      policer_prof_id;


    tm_id = (MEA_TmId_t)user_data_pi->tm_id;
    policer_prof_id = user_data_pi->policer_prof_id;
    if(mea_drv_CreateNew_Policer(unit_i,0,
                                 Action_policer_pi,
                                   MEA_FALSE,
                                 user_data_pi->tmId_disable/*MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL*/,  
                                 &policer_prof_id,
                                 &tm_id)!= MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- mea_drv_CreateNew_Policer failed in state\n",
                              __FUNCTION__,state_i);
            return MEA_ERROR;
     }
     db_data_pio->tm_id       = tm_id;    
     db_data_pio->tm_id_valid = MEA_TRUE;
     db_data_pio->policer_prof_id = policer_prof_id;
     db_data_pio->policer_prof_id_valid = MEA_TRUE;


     return MEA_OK;
    
}

/*---------------------------------------------------------------------------*/
/*    6  <MEA_drv_action_tm_state_db_notValid_id0_user_valid_id1_data0>      */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_valid_id1_data0)
{

   MEA_TmId_t              tm_id;   

   tm_id = (MEA_TmId_t)user_data_pi->tm_id;
   if(mea_drv_addOwner_Policer(unit_i,0,
                               Action_policer_pi,
                               MEA_FALSE ,
                               user_data_pi->tmId_disable/*MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL*/,  
                               &tm_id)!=MEA_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- mea_drv_addOwner_Policer failed in state %d\n",
                         __FUNCTION__,state_i);
       return MEA_ERROR;
    }
    db_data_pio->tm_id       = tm_id;    
    db_data_pio->tm_id_valid = MEA_TRUE;
    
    return MEA_OK;
    
}

/*---------------------------------------------------------------------------*/
/*   7  <MEA_drv_action_tm_state_db_notValid_id0_user_valid_id1_data1>       */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_notValid_id0_user_valid_id1_data1)
{
   MEA_TmId_t              tm_id;   
   MEA_Policer_prof_t      policer_prof_id;


   tm_id = (MEA_TmId_t) user_data_pi->tm_id;
   policer_prof_id = (user_data_pi->policer_prof_id_valid)?user_data_pi->policer_prof_id:0;
   if( mea_drv_CreateId_Policer(unit_i,0,
                                Action_policer_pi,
                                MEA_FALSE,
                                user_data_pi->tmId_disable/*MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL*/,  
                                &policer_prof_id,
                                &tm_id)!=MEA_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- mea_drv_CreateId_Policer failed in state\n",
                         __FUNCTION__,state_i);
       return MEA_ERROR;
    }
    db_data_pio->tm_id       = tm_id;    
    db_data_pio->tm_id_valid = MEA_TRUE;
    db_data_pio->policer_prof_id       = policer_prof_id;
    db_data_pio->policer_prof_id_valid = MEA_TRUE;
    
    return MEA_OK;
    
    
}

/*---------------------------------------------------------------------------*/
/*            <MEA_drv_action_tm_state_Error_Internal>                       */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_Error_Internal)
{

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                      "%s \n- Internal Error (state=%d)\n",
                      __FUNCTION__,
                      state_i);
    return MEA_ERROR;
    
}

/*---------------------------------------------------------------------------*/
/*   24  <MEA_drv_action_tm_state_db_valid_id1_user_notValid_id0_data0>      */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_notValid_id0_data0)
{

    if ( mea_drv_Delete_Policer(unit_i,(MEA_TmId_t)db_data_pio->tm_id) != MEA_OK )
     {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                    "%s \n- mea_drv_Delete_Policer failed (tm_id=%d) \n",
                                     __FUNCTION__,db_data_pio->tm_id);
                    return MEA_ERROR; 
    }
    db_data_pio->tm_id       = 0;
    db_data_pio->tm_id_valid = MEA_FALSE;
    
    
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*   25   <MEA_drv_action_tm_state_db_valid_id1_user_notValid_id0_data1>     */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_notValid_id0_data1)
{
    
    if ( mea_drv_Delete_Policer(unit_i,(MEA_TmId_t)db_data_pio->tm_id) != MEA_OK )
     {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                    "%s \n- mea_drv_Delete_Policer failed (tm_id=%d) \n",
                                     __FUNCTION__,db_data_pio->tm_id);
                    return MEA_ERROR; 
    }
    db_data_pio->tm_id       = 0;
    db_data_pio->tm_id_valid = MEA_FALSE;

    
    return MEA_OK;
    
    
}

/*---------------------------------------------------------------------------*/
/*   26  <MEA_drv_action_tm_state_db_valid_id1_user_notValid_id1_data0>      */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_notValid_id1_data0)
{
   
    if ( mea_drv_Delete_Policer(unit_i,(MEA_TmId_t)db_data_pio->tm_id) != MEA_OK )
     {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                    "%s \n- mea_drv_Delete_Policer failed (tm_id=%d) \n",
                                     __FUNCTION__,db_data_pio->tm_id);
                    return MEA_ERROR; 
    }
    db_data_pio->tm_id       = 0;
    db_data_pio->tm_id_valid = MEA_FALSE;

    return MEA_OK;
    
    
}

/*---------------------------------------------------------------------------*/
/*   27       <MEA_drv_action_tm_state_db_valid_id1_user_notValid_id1_data1> */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_notValid_id1_data1)
{

    if ( mea_drv_Delete_Policer(unit_i,(MEA_TmId_t)db_data_pio->tm_id) != MEA_OK )
     {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                    "%s \n- mea_drv_Delete_Policer failed (tm_id=%d) \n",
                                     __FUNCTION__,db_data_pio->tm_id);
                    return MEA_ERROR; 
    }
    db_data_pio->tm_id       = 0;
    db_data_pio->tm_id_valid = MEA_FALSE;
    
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*    28     <MEA_drv_action_tm_state_db_valid_id1_user_valid_id0_data0>     */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_valid_id0_data0)
{
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                      "%s \n- Missing input data for policing (Action_policer_pi==NULL)\n",
                      __FUNCTION__,
                      state_i);
    
    return MEA_ERROR;
}

/*---------------------------------------------------------------------------*/
/*   29  <MEA_drv_action_tm_state_db_valid_id1_user_valid_id0_data1>         */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_valid_id0_data1)
{
    MEA_TmId_t              tm_id;   
    MEA_Policer_prof_t      policer_prof_id;


    /* create new*/
    tm_id= (MEA_TmId_t) user_data_pi->tm_id;
    policer_prof_id = user_data_pi->policer_prof_id;
    if(mea_drv_CreateNew_Policer(unit_i,0,
                                 Action_policer_pi,
                                 MEA_FALSE,
                                 user_data_pi->tmId_disable/*MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL*/,  
                                 &policer_prof_id,
                                 &tm_id)!= MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- mea_drv_CreateNew_Policer failed in state\n",
                         __FUNCTION__,state_i);
       return MEA_ERROR;
    }
            
    /*delete old*/
    if (mea_drv_Delete_Policer(unit_i,(MEA_TmId_t)db_data_pio->tm_id) != MEA_OK ) {
        mea_drv_Delete_Policer(unit_i,tm_id);
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_Delete_Policer failed (tm_id=%d) \n",
                           __FUNCTION__,db_data_pio->tm_id);
        return MEA_ERROR; 
    }
    db_data_pio->tm_id       = tm_id;
    db_data_pio->tm_id_valid = MEA_TRUE;
    db_data_pio->policer_prof_id = policer_prof_id;
    db_data_pio->policer_prof_id_valid = MEA_TRUE;

    return MEA_OK;
    
    
}

/*---------------------------------------------------------------------------*/
/*   30  <MEA_drv_action_tm_state_db_valid_id1_user_valid_id1_data0>         */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_valid_id1_data0)
{
    MEA_TmId_t              tm_id;   
    
    
    /* if new id == old id 
        then do nothing
     else
       if new id not exist 
       then error
     else add owner to the new , and delOwner to the old
     */

    if(user_data_pi->tm_id == db_data_pio->tm_id){
        return MEA_OK;
    } 

    /* add */
    

    tm_id=  (MEA_TmId_t) user_data_pi->tm_id;
    if (mea_drv_addOwner_Policer(unit_i,0,
                                Action_policer_pi,
                                MEA_FALSE ,
                                user_data_pi->tmId_disable /*MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL*/,  
                                &tm_id)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- mea_drv_addOwner_Policer failed in state\n",
                          __FUNCTION__,state_i);
         return MEA_ERROR;
    }
   
    /*delete old*/
    if (mea_drv_Delete_Policer(unit_i,(MEA_TmId_t)db_data_pio->tm_id) != MEA_OK ) {
        mea_drv_Delete_Policer(unit_i,tm_id);
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s \n- mea_drv_Delete_Policer failed (tm_id=%d) \n",
                           __FUNCTION__,db_data_pio->tm_id);
        return MEA_ERROR; 
    } 

    db_data_pio->tm_id = tm_id;
    db_data_pio->tm_id_valid = MEA_TRUE;

    return MEA_OK;
    
    
}

/*---------------------------------------------------------------------------*/
/*   31  <MEA_drv_action_tm_state_db_valid_id1_user_valid_id1_data1>         */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_TM_STATE_FUNCTION(MEA_drv_action_tm_state_db_valid_id1_user_valid_id1_data1)
{

   MEA_TmId_t              tm_id;   
    MEA_Policer_prof_t     policer_prof_id;
    MEA_drv_policer_dbt entryInfo;
    MEA_Policer_Prof_dbt   entryProfInfo;
    MEA_Bool found;
    MEA_Uint32 numOfOwners;
    MEA_Policer_Entry_dbt   tmpPolicer_Entry;
    MEA_Uint32              comp;
    
   /* if new id == old id 
      then 
        if new data is not same as database data 
        then  
          if current owner is more then 1 
          then error
          else update data  
        else  do nothing 
      else
        if new id not exist 
        then create this id , with this data (new feature)
        else 
          if new data is not same as database data of the new id 
          then  
            if current owner is more then 1 
            then error
            else update data  , and delete old id
          else  addOwner , and delete old id
     */       

   tm_id= (MEA_TmId_t) user_data_pi->tm_id;
   policer_prof_id = (user_data_pi->policer_prof_id_valid)?user_data_pi->policer_prof_id:0;
    
    if (user_data_pi->tm_id == db_data_pio->tm_id) {
           
        /* compare the Data*/
        /*  if input data and the new id internal data are not same 
           then 
             if current owner is more then 1 
             then error
             else update data  
           else  do nothing */
        if (MEA_PolicerSW_Tmid_GetInfo(unit_i,
                                        tm_id,
                                        &entryInfo,
                                        &found)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s \n- MEA_PolicerSW_Tmid_GetInfo failed\n",
                              __FUNCTION__); 
             return MEA_ERROR;
        }

                
        if (mea_drv_Policer_Profile_Get(unit_i,
                                        0,
                                          entryInfo.prof_id,
                                          &entryProfInfo )!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s \n- mea_drv_Policer_Profile_Get failed\n",
                                __FUNCTION__); 
            return MEA_ERROR;
        }
          
        MEA_OS_memcpy(&tmpPolicer_Entry,Action_policer_pi,sizeof(tmpPolicer_Entry));
        comp =tmpPolicer_Entry.comp;
        if (MEA_policer_compensation(MEA_INGRESS_PORT_PROTO_TRANS,&comp) !=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- MEA_policer_compensation failed \n",
                        __FUNCTION__);
            return MEA_ERROR;
        }
        tmpPolicer_Entry.comp=comp;



        if (MEA_OS_memcmp(&entryProfInfo.rateMeter,
                            &tmpPolicer_Entry,
                            sizeof(entryProfInfo.rateMeter))==0 && entryInfo.prof_id == policer_prof_id){
              return MEA_OK; /* Do Nothing */
        }

        if (mea_drv_getOwner_Policer(unit_i,tm_id,&numOfOwners) != MEA_OK) {
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s \n- mea_drv_getOwner_Policer failed, index %d\n",
                                __FUNCTION__, tm_id);
              return MEA_ERROR; 
        }

        if (numOfOwners > 1) {
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s \n- numOfOwners (%d) > 1 - Not allowed to change implicit (tm_id=%d)\n",
                                __FUNCTION__, numOfOwners,tm_id);
              return MEA_ERROR; 
        }

        if ((user_data_pi->policer_prof_id == db_data_pio->policer_prof_id) && entryProfInfo.num_of_owners > 1) 
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- policer profile num_of_owners(%d) > 1 - Not allowed to change implicit (tm_id=%d,prof_id=%d)\n",
                              __FUNCTION__, entryProfInfo.num_of_owners,tm_id,entryInfo.prof_id);
            return MEA_ERROR; 
        }
        if ((user_data_pi->policer_prof_id == db_data_pio->policer_prof_id) ){
             //policer_prof_id=policer_prof_id;
        }else{
            /* Update Policer data - assume not pass between fast and slow */
            policer_prof_id = user_data_pi->policer_prof_id ; //0; /* generate new profile id */
        } 

        
        if(MEA_API_Set_TM_ID_Info(unit_i,
                                 tm_id,
                                 &policer_prof_id,
                                 Action_policer_pi )!=MEA_OK){

            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s \n- MEA_API_Set_TM_ID_Info failed  - can't change  fast/slow policer\n",
                                __FUNCTION__, numOfOwners,tm_id);
            return MEA_ERROR;  
         }
  
    } else {
      
      if( mea_drv_CreateId_Policer(unit_i,
                                   MEA_INGRESS_PORT_PROTO_TRANS,
                                   Action_policer_pi,
                                   MEA_TRUE,
                                   user_data_pi->tmId_disable /*MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL*/, 
                                   &policer_prof_id, 
                                   &tm_id)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- mea_drv_CreateId_Policer failed in state\n",
                              __FUNCTION__,state_i);
            return MEA_ERROR;
     }

     /* delete old*/
         if (mea_drv_Delete_Policer(unit_i,(MEA_TmId_t)db_data_pio->tm_id) != MEA_OK ) {
             mea_drv_Delete_Policer(unit_i,tm_id);
             MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                                  "%s \n- mea_drv_Delete_Policer failed (tm_id=%d) \n",
                                __FUNCTION__,db_data_pio->tm_id);
             return MEA_ERROR; 
         } 
   
    } 
     db_data_pio->tm_id = tm_id;
     db_data_pio->tm_id_valid = MEA_TRUE;
     db_data_pio->policer_prof_id = policer_prof_id;
     db_data_pio->policer_prof_id_valid = MEA_TRUE;

        
   
    
   return MEA_OK;
    
    
}





/*---------------------------------------------------------------------------*/
/*            <MEA_drv_action_tm_state_Setting>                              */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_drv_action_tm_state_Setting (MEA_Unit_t                  unit_i,                             
                                             MEA_Action_t                action_id_i,
                                             MEA_Action_Entry_Data_dbt  *user_data_pi, 
                                             MEA_Action_Entry_Data_dbt  *db_data_pio,
                                             MEA_Policer_Entry_dbt      *Policer_Entry,
                                             MEA_Bool                    create_i,
                                             MEA_Bool                    test_i)
    {
     MEA_Uint32          state;
     
    

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS    
    if(user_data_pi == NULL) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s - user_data_pi = NULL  \n",
                           __FUNCTION__);
        return MEA_ERROR; 
    }
    if(db_data_pio == NULL) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s - db_data_pio = NULL  \n",
                           __FUNCTION__);
        return MEA_ERROR; 
    }
#endif   
    
      
    
    /* Calculate the state */
    state      = 0x00000000;
    if(Policer_Entry != NULL){
       state |= 0x00000001;
    }
    if (user_data_pi->tm_id != 0) {
        state |= 0x00000002;
    }
    if (user_data_pi->tm_id_valid) {
        state |= 0x00000004;
    }
    if(!create_i) {
        if (db_data_pio->tm_id != 0) {
            state |= 0x00000008;
        }
        if (db_data_pio->tm_id_valid) {
            state |= 0x00000010;
        }
    }
    
    /* Call to the action tm state function according to the state */
    if (MEA_drv_action_tm_state_table[state](unit_i,
                                              state,
                                              action_id_i,
                                              user_data_pi,
                                              db_data_pio,
                                              Policer_Entry,
                                              create_i) != MEA_OK) {
       MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- Error: MEA_drv_action_tm_state_table faile state %d \n",
                           __FUNCTION__,state);
       return MEA_ERROR;
    } 
    
    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*  0   <MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id0_data0>     */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id0_data0)
{

    /* Do Nothing */


    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*    1 <MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id0_data1>     */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id0_data1)
{

    /* Do Nothing */

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   2  <MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id1_data0>     */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id1_data0)
{

    /* Do Nothing */

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   3  <MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id1_data1>     */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_notValid_id1_data1)
{


    /* Do Nothing */

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   4  <MEA_drv_action_ed_state_db_notValid_id0_user_valid_id0_data0>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_valid_id0_data0)
{

        
     
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                      "%s \n- Missing input data for editing (Action_editing_pi==NULL)\n",
                      __FUNCTION__,
                      state_i);
    
    
    return MEA_ERROR;
}


/*---------------------------------------------------------------------------*/
/*   5  <MEA_drv_action_ed_state_db_notValid_id0_user_valid_id0_data1>       */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_valid_id0_data1)
{

    mea_drv_ehp_id_dbt                   ehp_id;
    if(test_i == MEA_TRUE){
       
        /* TBD*/

        /* Return to caller after test*/
        return MEA_OK;
    } 
    /* Create new editing */ 
    MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
    ehp_id.id.uni_id.id = (MEA_Editing_t)user_data_pi->ed_id;
    if ( mea_drv_ehp_set(unit_i,Action_editing_pi, &ehp_id) != MEA_OK ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- mea_drv_ehp_set failed\n",
                        __FUNCTION__);
        return MEA_ERROR;            
    }

    /* save results in the db parameter */ 
    db_pio->EHP_MC     = ehp_id.is_mc;
    db_pio->MC_ID      = (db_pio->EHP_MC) ? (MEA_Uint8)ehp_id.id.mc_id.id : 0;
    db_pio->data.ed_id = (db_pio->EHP_MC) ? 0                             : ehp_id.id.uni_id.id;    
    db_pio->data.ed_id_valid = MEA_TRUE;

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   6  <MEA_drv_action_ed_state_db_notValid_id0_user_valid_id1_data0>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_valid_id1_data0)
{

    
    if (mea_drv_IsEditingIdInRange( (MEA_Editing_t) user_data_pi->ed_id) != MEA_TRUE) 
    {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,user_data_pi->ed_id);
        return MEA_ERROR;  
    }
    if (mea_drv_is_EHP_id_exist(unit_i,(MEA_Editing_t)  user_data_pi->ed_id) == MEA_TRUE)
    {
        if(test_i != MEA_TRUE)
        {
            if( mea_drv_ehp_add_owner( (MEA_Editing_t)  user_data_pi->ed_id ) != MEA_OK )
            {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- state=%d  editing id %d is not valid \n",__FUNCTION__,state_i,user_data_pi->ed_id);
            /* Return to caller */
            return MEA_ERROR;        
            }
         /* save results in the db parameter */ 
            db_pio->EHP_MC           = MEA_FALSE;
            db_pio->MC_ID            = 0;
            db_pio->data.ed_id       = user_data_pi->ed_id;
            db_pio->data.ed_id_valid = MEA_TRUE;
        }

    } else {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n - state=%d  editing id %d is not valid \n",__FUNCTION__,state_i,user_data_pi->ed_id);
        /* Return to caller */
        return MEA_ERROR;
    }
  
 return MEA_OK;   
}


/*---------------------------------------------------------------------------*/
/*    7 <MEA_drv_action_ed_state_db_notValid_id0_user_valid_id1_data1>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_notValid_id0_user_valid_id1_data1)
{

    MEA_Editing_t    ed_id;
    MEA_drv_ehp_dbt  entryDB_info;
    /*chack id the data is MC*/
    if(Action_editing_pi->num_of_entries>1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n-ERROR Action_editing_pi is MC\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    ed_id =(MEA_Editing_t) user_data_pi->ed_id;
    if (mea_drv_IsEditingIdInRange( ed_id ) != MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,ed_id);
        return MEA_ERROR;  
    }
       
    /* if id not exist need to create id */
    if(mea_drv_is_EHP_id_exist(unit_i,ed_id) == MEA_FALSE){
        if(test_i != MEA_TRUE){
            ed_id = (MEA_Editing_t) user_data_pi->ed_id;  
            if(mea_drv_ED_Set_EgressHeaderProc_Entry(unit_i,
                                            &Action_editing_pi->ehp_info->ehp_data,
                                             &ed_id,
                                              MEA_TRUE)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- mea_drv_ED_Set_EgressHeaderProc_Entry failed\n",__FUNCTION__);
                /* Return to caller */
                return MEA_ERROR;
           }
            /* save results in the db parameter */ 
            db_pio->EHP_MC           = MEA_FALSE;
            db_pio->MC_ID            = 0;
            db_pio->data.ed_id       = ed_id;
            db_pio->data.ed_id_valid = MEA_TRUE;  
        } 

    } else {
        ed_id = (MEA_Editing_t) user_data_pi->ed_id;
        if(mea_drv_ehp_get_info_db(unit_i, 
                                   ed_id,
                                  &entryDB_info)== MEA_OK){

                /*check that the data is the same */
            if ( MEA_OS_memcmp((char*)(&( entryDB_info.editing_info)), 
                               (char*)(&(  Action_editing_pi->ehp_info->ehp_data)) , 
                                sizeof(entryDB_info.editing_info)) == 0 ){
                    if(test_i != MEA_TRUE){
                        /*add owner*/
                        if( mea_drv_ehp_add_owner( ed_id ) != MEA_OK )
                        {
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s state=%d  editing id %d is not valid \n",
                             __FUNCTION__,
                             state_i,
                             user_data_pi->ed_id);
                            /* Return to caller */
                            return MEA_ERROR;        
                        }
                        /* save results in the db parameter */ 
                        db_pio->EHP_MC           = MEA_FALSE;
                        db_pio->MC_ID            = 0;
                        db_pio->data.ed_id       = ed_id;
                        db_pio->data.ed_id_valid = MEA_TRUE;  
                   }
                }else{
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- internal data are not same \n",
                         __FUNCTION__);
                    return MEA_ERROR; 
                }
        
        }else {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- mea_drv_ehp_get_info_db failed \n",
                         __FUNCTION__);
                    return MEA_ERROR; 

        }
        

    }
    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   8 <-> 15         <MEA_drv_action_ed_state_Error_Internal>                       */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_Error_Internal)
{

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                      "%s \n- Internal Error (state=%d)\n",
                      __FUNCTION__,
                      state_i);
    return MEA_ERROR;
    
}


/*---------------------------------------------------------------------------*/
/*  16   <MEA_drv_action_ed_state_db_valid_id0_user_notValid_id0_data0>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_notValid_id0_data0)
{

    mea_drv_ehp_id_dbt                   ehp_id;

    if(test_i == MEA_TRUE){  
        return MEA_OK;
    }


    MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
    ehp_id.is_mc = db_pio->EHP_MC;
    if (ehp_id.is_mc) {
         ehp_id.id.mc_id.id = db_pio->MC_ID;
    } else {
        ehp_id.id.uni_id.id = (MEA_Editing_t)(db_pio->data.ed_id);
    }
    if (mea_drv_ehp_delete(unit_i,
                             &ehp_id,
                           &(db_pio->out_ports)) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- mea_drv_ehp_mc_delete failed (id=%d %d) \n",
                         __FUNCTION__,
                         ehp_id.is_mc,
                         ehp_id.is_mc ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);
      return MEA_ERROR; 
    }
    db_pio->EHP_MC           = MEA_FALSE;
    db_pio->MC_ID            = 0;
    db_pio->data.ed_id       = user_data_pi->ed_id;
    db_pio->data.ed_id_valid = MEA_FALSE;


    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   17  <MEA_drv_action_ed_state_db_valid_id0_user_notValid_id0_data1>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_notValid_id0_data1)
{
    mea_drv_ehp_id_dbt                   ehp_id;

    if(test_i == MEA_TRUE){  
        return MEA_OK;
    }
    MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
    ehp_id.is_mc = db_pio->EHP_MC;
    if (ehp_id.is_mc) {
         ehp_id.id.mc_id.id = db_pio->MC_ID;
    } else {
        ehp_id.id.uni_id.id = (MEA_Editing_t)(db_pio->data.ed_id);
    }
    if (mea_drv_ehp_delete(unit_i,
                             &ehp_id,
                           &(db_pio->out_ports)) != MEA_OK) {
      MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                         "%s \n- mea_drv_ehp_mc_delete failed  (id=%d/%d) \n",
                         __FUNCTION__,
                         ehp_id.is_mc,
                         (ehp_id.is_mc) ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);
      return MEA_ERROR; 
    }
    db_pio->EHP_MC           = MEA_FALSE;
    db_pio->MC_ID            = 0;
    db_pio->data.ed_id       = user_data_pi->ed_id;
    db_pio->data.ed_id_valid = MEA_FALSE;

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   18  <MEA_drv_action_ed_state_db_valid_id0_user_notValid_id1_data0>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_notValid_id1_data0)
{
    mea_drv_ehp_id_dbt                   ehp_id;
    if(test_i == MEA_TRUE){  
        return MEA_OK;
    }
    MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
    ehp_id.is_mc = db_pio->EHP_MC;
    if (ehp_id.is_mc) {
         ehp_id.id.mc_id.id = db_pio->MC_ID;
    } else {
        ehp_id.id.uni_id.id = (MEA_Editing_t)(db_pio->data.ed_id);
    }
    if (mea_drv_ehp_delete(unit_i,
                             &ehp_id,
                           &(db_pio->out_ports)) != MEA_OK) {
      MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ehp_mc_delete failed (id=%d/%d) \n",
                         __FUNCTION__,
                         ehp_id.is_mc,
                         (ehp_id.is_mc) ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);
      return MEA_ERROR; 
    }
    db_pio->EHP_MC           = MEA_FALSE;
    db_pio->MC_ID            = 0;
    db_pio->data.ed_id       = 0;/*user_data_pi->ed_id;*/
    db_pio->data.ed_id_valid = user_data_pi->ed_id_valid;

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   19  <MEA_drv_action_ed_state_db_valid_id0_user_notValid_id1_data1>     */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_notValid_id1_data1)
{
    mea_drv_ehp_id_dbt                   ehp_id;
    if(test_i == MEA_TRUE){  
        return MEA_OK;
    }
    MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
    ehp_id.is_mc = db_pio->EHP_MC;
    if (ehp_id.is_mc) {
         ehp_id.id.mc_id.id = db_pio->MC_ID;
    } else {
        ehp_id.id.uni_id.id = (MEA_Editing_t)(db_pio->data.ed_id);
    }
    
    if (mea_drv_ehp_delete(unit_i,
                             &ehp_id,
                           &(db_pio->out_ports)) != MEA_OK) {
      MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                         "%s \n- mea_drv_ehp_mc_delete failed (id=%d/%d) \n",
                         __FUNCTION__,
                         ehp_id.is_mc,
                         (ehp_id.is_mc) ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);
      return MEA_ERROR; 
    }
    db_pio->EHP_MC           = MEA_FALSE;
    db_pio->MC_ID            = 0;
    db_pio->data.ed_id       = 0;/*user_data_pi->ed_id;*/
    db_pio->data.ed_id_valid = user_data_pi->ed_id_valid;

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   20  <MEA_drv_action_ed_state_db_valid_id0_user_valid_id0_data0>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_valid_id0_data0)
{

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                      "%s \n- Missing input data for Editing (Action_editing_pi==NULL)\n",
                      __FUNCTION__,
                      state_i);
    return MEA_ERROR;
}


/*---------------------------------------------------------------------------*/
/*   21  <MEA_drv_action_ed_state_db_valid_id0_user_valid_id0_data1>           */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_valid_id0_data1)
{

    mea_drv_ehp_id_dbt           ehp_id;
    MEA_Editing_t                ed_id;
    mea_drv_ehp_id_dbt           ehp_idnew;
  
    if(test_i == MEA_TRUE){  
         /*   TBD*/    
          return MEA_OK;
    }

    ed_id = (MEA_Editing_t) user_data_pi->ed_id; 
    if (ed_id){

    }

    MEA_OS_memset(&ehp_idnew,0,sizeof(ehp_idnew));

    ehp_idnew.id.uni_id.id = (MEA_Editing_t) user_data_pi->ed_id;
    if(mea_drv_ehp_set(unit_i,
                        Action_editing_pi,
                               &ehp_idnew) !=MEA_OK){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_ehp_set failed  \n",
                             __FUNCTION__); 

    /* Return to caller */
    return MEA_ERROR;
    }

    /*delete the DB*/
    MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
    ehp_id.is_mc = db_pio->EHP_MC;
    if (ehp_id.is_mc) {
         ehp_id.id.mc_id.id = db_pio->MC_ID;
    } else {
        ehp_id.id.uni_id.id = (MEA_Editing_t)(db_pio->data.ed_id);
    }

    if (mea_drv_ehp_delete(unit_i,
                             &ehp_id,
                           &(db_pio->out_ports)) != MEA_OK) {
      MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                         "%s \n- mea_drv_ehp_mc_delete failed (id=%d/%d) \n",
                         __FUNCTION__,
                         ehp_id.is_mc,
                         (ehp_id.is_mc) ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);

      /* need to delete the */

    /* Return to caller */
    return MEA_ERROR;
    }
    db_pio->EHP_MC           = MEA_FALSE;
    db_pio->MC_ID            = 0;
    db_pio->data.ed_id       = 0;
    db_pio->data.ed_id_valid = MEA_FALSE;

    /**/
    if ( ehp_idnew.is_mc )
    {

        db_pio->MC_ID  = (MEA_Uint8) ehp_idnew.id.mc_id.id;
        db_pio->EHP_MC = MEA_TRUE;
        db_pio->data.ed_id_valid = MEA_TRUE;
        db_pio->data.ed_id       =0;
    }
    else
    {
        db_pio->EHP_MC           = MEA_FALSE;
        db_pio->MC_ID            = 0;
        db_pio->data.ed_id = ehp_idnew.id.uni_id.id;
        db_pio->data.ed_id_valid = MEA_TRUE;
    }

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   22  <MEA_drv_action_ed_state_db_valid_id0_user_valid_id1_data0>           */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_valid_id1_data0)
{

    mea_drv_ehp_id_dbt                   ehp_id;
    MEA_Editing_t ed_id;

    
    if (mea_drv_IsEditingIdInRange( (MEA_Editing_t) user_data_pi->ed_id) != MEA_TRUE) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,user_data_pi->ed_id);
        return MEA_ERROR;
    }
    ed_id=(MEA_Editing_t) user_data_pi->ed_id;
    /* if id not exist need to create id */
    if(mea_drv_is_EHP_id_exist(unit_i,ed_id) == MEA_TRUE){
        if(test_i != MEA_TRUE){  
            if( mea_drv_ehp_add_owner( ed_id ) != MEA_OK )
            {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s \n - state=%d  editing id %d is not valid \n",
                 __FUNCTION__,
                 state_i,
                 user_data_pi->ed_id);
                /* Return to caller */
                return MEA_ERROR;        
            }
             /* Delete */
            MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
            ehp_id.is_mc = db_pio->EHP_MC;
            if (ehp_id.is_mc) {
                 ehp_id.id.mc_id.id = db_pio->MC_ID;
            } else {
                ehp_id.id.uni_id.id = (MEA_Editing_t)(db_pio->data.ed_id);
            }
            
            if (mea_drv_ehp_delete(unit_i,
                                     &ehp_id,
                                   &(db_pio->out_ports)) != MEA_OK) {
              MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                                 "%s \n- mea_drv_ehp_mc_delete failed (id=%d/%d) \n",
                                 __FUNCTION__,
                                 ehp_id.is_mc,
                                 (ehp_id.is_mc) ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);
              return MEA_ERROR; 
            }
            db_pio->EHP_MC           = MEA_FALSE;
            db_pio->MC_ID            = 0;
            db_pio->data.ed_id       = ed_id;
            db_pio->data.ed_id_valid = MEA_TRUE;
        }
    } else {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- id %d is not exist\n",
                         __FUNCTION__,ed_id);
            return MEA_ERROR;
   }
    
    
    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   23  <MEA_drv_action_ed_state_db_valid_id0_user_valid_id1_data1>           */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id0_user_valid_id1_data1)
{
    mea_drv_ehp_id_dbt                   ehp_id;
    MEA_Editing_t                        ed_id;
    MEA_drv_ehp_dbt                     entryDB_info;
    
    if(Action_editing_pi->num_of_entries > 1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n-ERROR Action_editing_pi is MC not allowed here\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_IsEditingIdInRange((MEA_Editing_t) user_data_pi->ed_id) != MEA_TRUE) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- Invalid id %d \n",
                         __FUNCTION__,user_data_pi->ed_id);
        return MEA_ERROR;  
    }
    ed_id=(MEA_Editing_t) user_data_pi->ed_id;
    if (mea_drv_is_EHP_id_exist(unit_i,ed_id)== MEA_FALSE){
        if(test_i != MEA_TRUE){
            if(mea_drv_ED_Set_EgressHeaderProc_Entry(unit_i,
                                                &Action_editing_pi->ehp_info->ehp_data,
                                                &ed_id,
                                                MEA_FALSE)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- mea_drv_ED_Set_EgressHeaderProc_Entry fail\n",
                __FUNCTION__);
            /* Return to caller */
            return MEA_ERROR;
            }
            /* Delete */
            MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
            ehp_id.is_mc = db_pio->EHP_MC;
            if (ehp_id.is_mc) {
                 ehp_id.id.mc_id.id = db_pio->MC_ID;
            } else {
                ehp_id.id.uni_id.id = (MEA_Editing_t)(db_pio->data.ed_id);
            }
    
            if (mea_drv_ehp_delete(unit_i,
                             &ehp_id,
                           &(db_pio->out_ports)) != MEA_OK) {
                MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                         "%s \n- mea_drv_ehp_mc_delete failed (id=%d/%d) \n",
                         __FUNCTION__,
                         ehp_id.is_mc,
                         (ehp_id.is_mc) ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);
            return MEA_ERROR; 
            }
            /**/
            db_pio->EHP_MC           = MEA_FALSE;
            db_pio->MC_ID            = 0;
            db_pio->data.ed_id       = ed_id;
            db_pio->data.ed_id_valid = user_data_pi->ed_id_valid;
        }
    } else {
        ed_id=(MEA_Editing_t) user_data_pi->ed_id;
        if(mea_drv_ehp_get_info_db(unit_i, 
                              ed_id,
                              &entryDB_info)!= MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s \n- mea_drv_ehp_get_info_db failed  %d \n",
                     __FUNCTION__,user_data_pi->ed_id);
            return MEA_ERROR;
        }
        /* check if the data is not te same */
        if ( MEA_OS_memcmp(     (char*)(&( entryDB_info.editing_info)), 
                               (char*)(&(  Action_editing_pi->ehp_info->ehp_data)) , 
                               sizeof(entryDB_info.editing_info)) != 0 )
       {
            if(entryDB_info.num_of_owners > 1){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- num_of_owners  %d > 1 can't update (edId = %d )\n",
                        __FUNCTION__,
                        entryDB_info.num_of_owners,
                        ed_id);
                return MEA_ERROR;
            }else {
                if(test_i != MEA_TRUE){
                /*update data*/
                    ed_id = (MEA_Editing_t) user_data_pi->ed_id;  
                    if(mea_drv_ED_Set_EgressHeaderProc_Entry(unit_i,
                                        &Action_editing_pi->ehp_info->ehp_data,
                                         &ed_id,
                                          MEA_FALSE)!=MEA_OK)
                    {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s \n- mea_drv_ED_Set_EgressHeaderProc_Entry failed\n",
                            __FUNCTION__);
                        /* Return to caller */
                        return MEA_ERROR;
                    }
                /* Delete */
                MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
                ehp_id.is_mc = db_pio->EHP_MC;
                if (ehp_id.is_mc) {
                     ehp_id.id.mc_id.id = db_pio->MC_ID;
                } else {
                    ehp_id.id.uni_id.id = (MEA_Editing_t)(db_pio->data.ed_id);
                }
        
                if (mea_drv_ehp_delete(unit_i,
                                 &ehp_id,
                               &(db_pio->out_ports)) != MEA_OK) {
                    MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_ehp_mc_delete failed (id=%d/%d) \n",
                             __FUNCTION__,
                             ehp_id.is_mc,
                             (ehp_id.is_mc) ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);
                return MEA_ERROR; 
                }
                db_pio->EHP_MC           = MEA_FALSE;
                db_pio->MC_ID            = 0;
                db_pio->data.ed_id       = user_data_pi->ed_id;
                db_pio->data.ed_id_valid = user_data_pi->ed_id_valid;
                }
            }
        }else {
             /*add owner*/
            if(test_i != MEA_TRUE){
                ed_id = (MEA_Editing_t) user_data_pi->ed_id;  
                if(mea_drv_ED_Set_EgressHeaderProc_Entry(unit_i,
                                    &Action_editing_pi->ehp_info->ehp_data,
                                    &ed_id,
                                    MEA_FALSE)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s \n- mea_drv_ED_Set_EgressHeaderProc_Entry failed\n",
                               __FUNCTION__);
                        /* Return to caller */
                    return MEA_ERROR;
                }
                /* Delete */
                MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
                ehp_id.is_mc = db_pio->EHP_MC;
                if (ehp_id.is_mc) {
                     ehp_id.id.mc_id.id = db_pio->MC_ID;
                } else {
                    ehp_id.id.uni_id.id = (MEA_Editing_t)(db_pio->data.ed_id);
                }
                if (mea_drv_ehp_delete(unit_i,
                                 &ehp_id,
                               &(db_pio->out_ports)) != MEA_OK) {
                    MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_ehp_mc_delete failed (id=%d/%d) \n",
                             __FUNCTION__,
                             ehp_id.is_mc,
                             (ehp_id.is_mc) ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);
                return MEA_ERROR; 
                }
                db_pio->EHP_MC           = MEA_FALSE;
                db_pio->MC_ID            = 0;
                db_pio->data.ed_id       = user_data_pi->ed_id;
                db_pio->data.ed_id_valid = user_data_pi->ed_id_valid;
            } 
        }
    }
    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   24  <MEA_drv_action_ed_state_db_valid_id1_user_notValid_id0_data0>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_notValid_id0_data0)
{

    if(test_i == MEA_TRUE){

    /* Return to caller after test*/
    return MEA_OK; 
    }
    
    
    
    
    if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
        (MEA_Editing_t) db_pio->data.ed_id) != MEA_OK )
        {
          MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
                             __FUNCTION__,db_pio->data.ed_id);
          return MEA_ERROR; 
        }
    
    
    db_pio->EHP_MC           = MEA_FALSE;
    db_pio->MC_ID            = 0;
    db_pio->data.ed_id       = user_data_pi->ed_id;
    db_pio->data.ed_id_valid = MEA_FALSE;
 
    
    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   25  <MEA_drv_action_ed_state_db_valid_id1_user_notValid_id0_data1>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_notValid_id0_data1)
{
    if(test_i == MEA_TRUE){

    /* Return to caller after test*/
    return MEA_OK; 
    }
  
    
    if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
        (MEA_Editing_t) db_pio->data.ed_id) != MEA_OK )
        {
          MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
                             __FUNCTION__,db_pio->data.ed_id);
          return MEA_ERROR; 
        }

    db_pio->EHP_MC           = MEA_FALSE;
    db_pio->MC_ID            = 0;
    db_pio->data.ed_id       = user_data_pi->ed_id;
    db_pio->data.ed_id_valid = MEA_FALSE;
    
    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*    26 <MEA_drv_action_ed_state_db_valid_id1_user_notValid_id1_data0>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_notValid_id1_data0)
{

    if(test_i == MEA_TRUE){

    /* Return to caller after test*/
    return MEA_OK; 
    }
  
  
    if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
        (MEA_Editing_t) db_pio->data.ed_id) != MEA_OK )
        {
          MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
                             __FUNCTION__,db_pio->data.ed_id);
          return MEA_ERROR; 
        }
    
    
    db_pio->EHP_MC           = MEA_FALSE;
    db_pio->MC_ID            = 0;
    db_pio->data.ed_id       = 0; /*user_data_pi->ed_id;*/
    db_pio->data.ed_id_valid = MEA_FALSE;
    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   27  <MEA_drv_action_ed_state_db_valid_id1_user_notValid_id1_data1>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_notValid_id1_data1)
{
    if(test_i == MEA_TRUE){

    /* Return to caller after test*/
    return MEA_OK; 
    }
  


    if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
        (MEA_Editing_t) db_pio->data.ed_id) != MEA_OK )
        {
          MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
                             __FUNCTION__,db_pio->data.ed_id);
          return MEA_ERROR; 
        }

    
    db_pio->EHP_MC           = MEA_FALSE;
    db_pio->MC_ID            = 0;
    db_pio->data.ed_id       = 0; /*user_data_pi->ed_id;*/
    db_pio->data.ed_id_valid = MEA_FALSE;

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*  28   <MEA_drv_action_ed_state_db_valid_id1_user_valid_id0_data0>        */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_valid_id0_data0)
{

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- Error No Input data \n",__FUNCTION__);
    /* Return to caller */
    return MEA_ERROR;
}


/*---------------------------------------------------------------------------*/
/*   29  <MEA_drv_action_ed_state_db_valid_id1_user_valid_id0_data1>           */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_valid_id0_data1)
{

    mea_drv_ehp_id_dbt ehp_idnew;
    /*create new*/
    if(test_i == MEA_TRUE){
      
    /* Return to caller after test*/
    return MEA_OK; 
    }
  
    MEA_OS_memset(&ehp_idnew,0,sizeof(ehp_idnew));

    ehp_idnew.id.uni_id.id = (MEA_Editing_t)user_data_pi->ed_id;

    if ( mea_drv_ehp_set(unit_i,Action_editing_pi, &ehp_idnew) != MEA_OK )
    {
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- mea_drv_ehp_set failed\n",
                        __FUNCTION__);
        return MEA_ERROR;            
    }

    
    /*delete old*/
    if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
        (MEA_Editing_t) db_pio->data.ed_id) != MEA_OK )
    {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                         "%s \n- MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
                             __FUNCTION__,db_pio->data.ed_id);
        return MEA_ERROR; 
    }

    /**/
    if ( ehp_idnew.is_mc )
    {
        
        db_pio->MC_ID  = (MEA_Uint8) ehp_idnew.id.mc_id.id;
        db_pio->EHP_MC = MEA_TRUE;
        db_pio->data.ed_id_valid = MEA_TRUE;
        db_pio->data.ed_id=0;
    }
    else
    {
        db_pio->EHP_MC           = MEA_FALSE;
        db_pio->MC_ID            = 0;
        db_pio->data.ed_id = ehp_idnew.id.uni_id.id;
        db_pio->data.ed_id_valid = MEA_TRUE;
    }
    
    

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*    30 <MEA_drv_action_ed_state_db_valid_id1_user_valid_id1_data0>           */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_valid_id1_data0)
{


    if(db_pio->data.ed_id == user_data_pi->ed_id){
        /* do nothing */
        return MEA_OK;
    }else{
        if (mea_drv_is_EHP_id_exist(unit_i,(MEA_Editing_t)  user_data_pi->ed_id) == MEA_TRUE){
            if(test_i != MEA_TRUE){
                if( mea_drv_ehp_add_owner( (MEA_Editing_t)  user_data_pi->ed_id ) != MEA_OK )
                {
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- state=%d  mea_drv_ehp_add_owner failed \n",__FUNCTION__,state_i);
                /* Return to caller */
                return MEA_ERROR;
                }
                /* delete old*/
                if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
                    (MEA_Editing_t) db_pio->data.ed_id) != MEA_OK )
                    {
                    MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
                             __FUNCTION__,db_pio->data.ed_id);
                    return MEA_ERROR; 
                }

                db_pio->data.ed_id = user_data_pi->ed_id;
                db_pio->data.ed_id_valid = MEA_TRUE;
            }

        } else {
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n - state=%d  editing id %d is not exist \n",
                  __FUNCTION__,state_i,user_data_pi->ed_id);
                /* Return to caller */
                return MEA_ERROR; 
        } 

    
    }
    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*   31  <MEA_drv_action_ed_state_db_valid_id1_user_valid_id1_data1>           */
/*---------------------------------------------------------------------------*/
MEA_DRV_ACTION_ED_STATE_FUNCTION(MEA_drv_action_ed_state_db_valid_id1_user_valid_id1_data1)
{
    MEA_Editing_t                        ed_id;
    MEA_drv_ehp_dbt                     entryDB_info;
    if(Action_editing_pi->num_of_entries>1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n-ERROR Action_editing_pi is MC\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    
    
    if (mea_drv_IsEditingIdInRange( (MEA_Editing_t) user_data_pi->ed_id) != MEA_TRUE) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s \n- Invalid id %d \n",
                     __FUNCTION__,user_data_pi->ed_id);
    return MEA_ERROR;  
    }
   
    /*if new id == old id*/
    if(user_data_pi->ed_id == db_pio->data.ed_id)
    {
        ed_id=(MEA_Editing_t) user_data_pi->ed_id;
        if(mea_drv_ehp_get_info_db(unit_i, 
                              ed_id,
                              &entryDB_info)!= MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s \n- mea_drv_ehp_get_info_db failed  %d \n",
                     __FUNCTION__,user_data_pi->ed_id);
        return MEA_ERROR;
        
        }
        /* check if the data is not the same */
        if ( MEA_OS_memcmp(     (char*)(&( entryDB_info.editing_info)), 
                               (char*)(&(  Action_editing_pi->ehp_info->ehp_data)) , 
                               sizeof(entryDB_info.editing_info)) != 0 )
       {
            if(entryDB_info.num_of_owners > 1){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s \n- num_of_owners  %d > 1 can't update (edId = %d )\n",
                     __FUNCTION__,entryDB_info.num_of_owners,ed_id);

             return MEA_ERROR;
            }else {
                if(test_i != MEA_TRUE){
                /*update data*/
                 ed_id = (MEA_Editing_t) user_data_pi->ed_id;  
                        if(mea_drv_ED_Set_EgressHeaderProc_Entry(unit_i,
                                        &Action_editing_pi->ehp_info->ehp_data,
                                         &ed_id,
                                          MEA_FALSE)!=MEA_OK)
                        {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s \n- mea_drv_ED_Set_EgressHeaderProc_Entry failed\n",__FUNCTION__);
                        /* Return to caller */
                        return MEA_ERROR;
                        } 
                   db_pio->EHP_MC           = MEA_FALSE;
                   db_pio->MC_ID            = 0;
                   db_pio->data.ed_id       = ed_id;
                   db_pio->data.ed_id_valid = MEA_TRUE;
                }
            }
       
        } else {
           /*Data is the same do nothing*/
        } 
    }else { /*end same ids*/
        if(mea_drv_is_EHP_id_exist(unit_i,(MEA_Editing_t) user_data_pi->ed_id) == MEA_FALSE){
            /* create this id , with this data and delete owner for id*/
            if(test_i != MEA_TRUE){
                /*update data*/
                ed_id = (MEA_Editing_t) user_data_pi->ed_id;  
                if(mea_drv_ED_Set_EgressHeaderProc_Entry(unit_i,
                                    &Action_editing_pi->ehp_info->ehp_data,
                                    &ed_id,
                                    MEA_FALSE)!=MEA_OK)
                {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- mea_drv_ED_Set_EgressHeaderProc_Entry failed\n",__FUNCTION__);
                        /* Return to caller */
                    return MEA_ERROR;
                 } 
                 if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
                        (MEA_Editing_t) db_pio->data.ed_id) != MEA_OK )
                    {
                        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
                         __FUNCTION__,db_pio->data.ed_id);
                    return MEA_ERROR; 
                    }   
                        
                    db_pio->EHP_MC           = MEA_FALSE;
                    db_pio->MC_ID            = 0;
                    db_pio->data.ed_id       = ed_id;
                    db_pio->data.ed_id_valid = MEA_TRUE;
                 }
        }else{
            ed_id=(MEA_Editing_t) user_data_pi->ed_id;
            if(mea_drv_ehp_get_info_db(unit_i, 
                              ed_id,
                              &entryDB_info)!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s \n- mea_drv_ehp_get_info_db failed  %d \n",
                     __FUNCTION__,user_data_pi->ed_id);
                return MEA_ERROR;
            }
            if ( MEA_OS_memcmp(
                               (char*)(&( entryDB_info.editing_info)), 
                               (char*)(&(  Action_editing_pi->ehp_info->ehp_data)) , 
                               sizeof(entryDB_info.editing_info)) != 0 )
            {
                if(entryDB_info.num_of_owners > 1){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s \n- num_of_owners  %d > 1 can't update (edId = %d )\n",
                     __FUNCTION__,entryDB_info.num_of_owners,ed_id);

                    return MEA_ERROR;
                }else{
                    /*update the database and delete old id*/
                    if(test_i != MEA_TRUE){
                    /*update data*/
                    ed_id = (MEA_Editing_t) user_data_pi->ed_id;  
                    if(mea_drv_ED_Set_EgressHeaderProc_Entry(unit_i,
                                    &Action_editing_pi->ehp_info->ehp_data,
                                    &ed_id,
                                    MEA_FALSE)!=MEA_OK)
                    {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- mea_drv_ED_Set_EgressHeaderProc_Entry failed\n",__FUNCTION__);
                        /* Return to caller */
                        return MEA_ERROR;
                    } 
                    if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
                        (MEA_Editing_t) db_pio->data.ed_id) != MEA_OK )
                    {
                        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                         "%s \n- MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
                         __FUNCTION__,db_pio->data.ed_id);
                        return MEA_ERROR; 
                    }   
                    db_pio->EHP_MC           = MEA_FALSE;
                    db_pio->MC_ID            = 0;
                    db_pio->data.ed_id       = ed_id;
                    db_pio->data.ed_id_valid = MEA_TRUE;          
                 }
                    
                }
            }else {
                /*add owner*/
                if(test_i != MEA_TRUE){
                    ed_id = (MEA_Editing_t) user_data_pi->ed_id;  
                    if(mea_drv_ED_Set_EgressHeaderProc_Entry(unit_i,
                                    &Action_editing_pi->ehp_info->ehp_data,
                                    &ed_id,
                                    MEA_FALSE)!=MEA_OK)
                    {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- mea_drv_ED_Set_EgressHeaderProc_Entry failed\n",__FUNCTION__);
                        /* Return to caller */
                        return MEA_ERROR;
                    } 
                    if ( MEA_API_Delete_EgressHeaderProc_Entry(unit_i,
                        (MEA_Editing_t) db_pio->data.ed_id) != MEA_OK )
                    {
                        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                         "%s \n- MEA_API_Delete_EgressHeaderProc_Entry failed (editId=%d) \n",
                         __FUNCTION__,db_pio->data.ed_id);
                        return MEA_ERROR; 
                    }
                    db_pio->EHP_MC           = MEA_FALSE;
                    db_pio->MC_ID            = 0;
                    db_pio->data.ed_id       = ed_id;
                    db_pio->data.ed_id_valid = MEA_TRUE;
                 }
             }
        }
    } 

    
        
        /* Return to caller after test*/
    return MEA_OK; 
}


/*---------------------------------------------------------------------------*/
/*            <MEA_drv_action_ed_state_Setting>                              */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_drv_action_ed_state_Setting
(MEA_Unit_t                            unit_i,
MEA_Action_t                          action_id_i,
MEA_Action_Entry_Data_dbt            *user_data_pi,
mea_action_drv_dbt                   *db_pio,
MEA_EgressHeaderProc_Array_Entry_dbt *Action_editing_pi,
MEA_Bool                              create_i,
MEA_Bool                              test_i)
{

    MEA_Uint32          state;
    MEA_Uint32          i;


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS    
    if (user_data_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - user_data_pi = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (db_pio == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - db_pio = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
#endif   

    if ((Action_editing_pi) &&
        (mea_drv_ehp_info_check(Action_editing_pi, NULL) != MEA_OK)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- mea_drv_ehp_info_check failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    /* Calculate the state */
    state = 0x00000000;
    if (Action_editing_pi != NULL){
        state |= 0x00000001;
    }
    if (user_data_pi->ed_id != 0) {
        state |= 0x00000002;
    }
    if (user_data_pi->ed_id_valid) {
        state |= 0x00000004;
    }
    if (!create_i) {
        if (db_pio->data.ed_id != 0) {
            state |= 0x00000008;
        }
        if (db_pio->data.ed_id_valid) {
            state |= 0x00000010;
        }
    }
    
    if (Action_editing_pi){
        if (Action_editing_pi->num_of_entries > 1)
        {
            for (i = 0; i < Action_editing_pi->num_of_entries; i++){
                if (MEA_IS_CLEAR_ALL_OUTPORT(&Action_editing_pi->ehp_info[i].output_info)){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - EHP is MC Editing but out cluster not set for ehp_info[%d]\n",
                        __FUNCTION__, i);
                    return MEA_ERROR;
                }
            }
        }

    }




    /* Call to the action tm state function according to the state */
    if (MEA_drv_action_ed_state_table[state](unit_i,
        state,
        action_id_i,
        user_data_pi,
        db_pio,
        Action_editing_pi,
        create_i,
        test_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- Error: MEA_drv_action_ed_state_table failed state %d \n",
            __FUNCTION__, state);
        return MEA_ERROR;
    }

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Calc_ProtocolAndLlc>                                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Calc_ProtocolAndLlc
                      (MEA_Unit_t              unit_i,
                       MEA_Port_t              i_src_port,
                       MEA_OutPorts_Entry_dbt* i_out_ports,
                       MEA_Uint32             *protocol_o,
                       MEA_Bool               *exist_llc_o) {

    MEA_Uint32*               ptr;
    MEA_Uint32                mask;
    MEA_Uint32                       i;
    MEA_Port_t                outport;
    MEA_IngressPort_Entry_dbt IngressPort_Entry;
    MEA_EgressPort_Entry_dbt  EgressPort_Entry;
    MEA_IngressPort_Proto_t   IngressPort_proto;
    MEA_EgressPort_Proto_t    EgressPort_proto;
    MEA_Bool                  found; 
    MEA_Bool                  IngressPort_utopia;
    MEA_Bool                  EgressPort_utopia;
    ENET_Queue_dbt              queue_Entry;

    if (protocol_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - protocol_o == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (exist_llc_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - exist_llc_o == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }    
    
    *protocol_o  = 1;
    *exist_llc_o = 0;

    /* Get the ingress port protocol profile */
    if (MEA_API_Get_IngressPort_Entry(unit_i,
                                      (MEA_Port_t)i_src_port,
                                      &IngressPort_Entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_IngressPort_Entry for port %d failed\n",
                           __FUNCTION__,i_src_port);
        return MEA_ERROR;
    }
    IngressPort_proto = IngressPort_Entry.parser_info.port_proto_prof;
    IngressPort_utopia = IngressPort_Entry.utopia;

    if ( IngressPort_utopia )
        *protocol_o = 0;
    else
        *protocol_o = 1;

    if (i_out_ports == NULL) {
       return MEA_OK;
    }

    for (i=0 , ptr = (MEA_Uint32*)i_out_ports , outport = 0 , found=MEA_FALSE ;
         i<(sizeof(*i_out_ports)/sizeof(*ptr));
         i++ , ptr++) {

        for (mask = 0x00000001 ;  (mask & 0xffffffff) != 0 ;  mask <<= 1,outport++) {

            if (!(*ptr & mask)) {
               continue;
            }

            found = MEA_TRUE;
            break;

         }
         if (found) {
            break;
         }
    }

    if (found) {
        MEA_OS_memset(&queue_Entry,0,sizeof(queue_Entry));
        /* Get Egress Port protocol */
         if (ENET_Get_Queue(unit_i,
                           (ENET_QueueId_t)outport,
                           &queue_Entry) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- ENET_Get_Queue for queue %d failed\n",
                              __FUNCTION__,outport);
            return MEA_ERROR;
        }
//         if(queue_Entry.port.type !=ENET_QUEUE_PORT_TYPE_PORT){
//             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
//                               "%s \n- only Queue type port is supported\n",
//                               __FUNCTION__);
//             return MEA_ERROR;
//         }
        if(queue_Entry.port.type == ENET_QUEUE_PORT_TYPE_PORT){
        if (MEA_API_Get_EgressPort_Entry(unit_i,
                                        /* (MEA_Port_t)(MEA_API_Get_IsPortValid(outport,MEA_TRUE) ? outport : */
                                         (MEA_Port_t)queue_Entry.port.id.port,
                                         &EgressPort_Entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- MEA_API_Get_EgressPort_Entry "
                              "for port %d failed\n",
                              __FUNCTION__,outport);
            return MEA_ERROR;
        }
        EgressPort_proto = EgressPort_Entry.proto;


        /* Get the ingress port protocol profile */
        if (MEA_API_Get_IngressPort_Entry(unit_i,
                                          (MEA_Port_t)queue_Entry.port.id.port,
                                          &IngressPort_Entry) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s \n- MEA_API_Get_IngressPort_Entry for port %d failed\n",
                             __FUNCTION__,outport);
           return MEA_ERROR;
        }
        EgressPort_utopia = IngressPort_Entry.utopia;


        /* check for ethernet only */
        if ((IngressPort_utopia == MEA_FALSE) &&
            (EgressPort_utopia  == MEA_FALSE)  ) {

        *exist_llc_o = MEA_FALSE;  /*  exist llc */

            if ((IngressPort_proto == MEA_INGRESS_PORT_PROTO_MARTINI) ||
                (EgressPort_proto  == MEA_EGRESS_PORT_PROTO_MARTINI )  )  {
                *protocol_o = 3;
            } else {
                *protocol_o = 1;
            }

        } else { 

            /* Check for PPPoEoLLCoA */
            if ((EgressPort_proto   == MEA_EGRESS_PORT_PROTO_PPPoEoLLCoATM )   ||
                (IngressPort_proto  == MEA_INGRESS_PORT_PROTO_PPPoEoLLCoATM)   ||
                (EgressPort_proto   == MEA_EGRESS_PORT_PROTO_PPPoEoVcMUXoATM )   ||
                (IngressPort_proto  == MEA_INGRESS_PORT_PROTO_PPPoEoVcMUXoATM)   ){
                *protocol_o = 2; /* PPPoEoA */
            } else if ((EgressPort_proto   == MEA_EGRESS_PORT_PROTO_PPPoLLCoATM   )   ||
                       (IngressPort_proto  == MEA_INGRESS_PORT_PROTO_PPPoLLCoATM  )   ||
                       (EgressPort_proto   == MEA_EGRESS_PORT_PROTO_PPPoVcMUXoATM  )   ||
                       (IngressPort_proto  == MEA_INGRESS_PORT_PROTO_PPPoVcMUXoATM  )   ){
                *protocol_o = 3; /* pppoa */
            } else if ((EgressPort_proto   == MEA_EGRESS_PORT_PROTO_IPoLLCoATM )   ||
                       (IngressPort_proto  == MEA_INGRESS_PORT_PROTO_IPoLLCoATM)   ||
                       (EgressPort_proto   == MEA_EGRESS_PORT_PROTO_IPoVcMUXoATM ) ||
                       (IngressPort_proto  == MEA_INGRESS_PORT_PROTO_IPoVcMUXoATM)   ){
                *protocol_o = 1; /* IPoa */
            } else {
                /* We assume for now transparent EoA */
                *protocol_o = 0; /* trans */
            }
            if ( (EgressPort_proto  == MEA_EGRESS_PORT_PROTO_EoLLCoATM)       ||
                 (EgressPort_proto  == MEA_EGRESS_PORT_PROTO_IPoEoLLCoATM)    ||
                 (EgressPort_proto  == MEA_EGRESS_PORT_PROTO_PPPoEoLLCoATM)   ||
                 (EgressPort_proto  == MEA_EGRESS_PORT_PROTO_IPoLLCoATM)      ||
                 (EgressPort_proto  == MEA_EGRESS_PORT_PROTO_PPPoLLCoATM  )  ||
                 (IngressPort_proto == MEA_INGRESS_PORT_PROTO_EoLLCoATM)      ||
                 (IngressPort_proto == MEA_INGRESS_PORT_PROTO_IPoEoLLCoATM)   ||
                 (IngressPort_proto == MEA_INGRESS_PORT_PROTO_PPPoEoLLCoATM)  ||
                 (IngressPort_proto == MEA_INGRESS_PORT_PROTO_IPoLLCoATM)   ||
                 (IngressPort_proto == MEA_INGRESS_PORT_PROTO_PPPoLLCoATM  )  ){
                /* LLC over ATM */
                *exist_llc_o = MEA_TRUE;  /*  exist llc */
            }else{
                /* vc mux over ATM */
                *exist_llc_o = MEA_FALSE;  /*  exist llc */

            }

        }
        } else {
            if(queue_Entry.port.type == ENET_QUEUE_PORT_TYPE_VIRTUAL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- need to set protocol an llc by force \n",
                __FUNCTION__,outport);
            return MEA_ERROR;
            } else {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s \n- we not supported  Queue type %d \n",
                                          __FUNCTION__,queue_Entry.port.type);
                        return MEA_ERROR;
            }
           
        }

    }//found

    return MEA_OK;

}

MEA_Uint32 mea_action_calc_numof_regs(MEA_Unit_t         unit, 
                                     MEA_db_HwUnit_t    hwUnit )
{

    MEA_Uint32                count=0;
    MEA_Uint32 numofRegs = 0;

    count = (
        MEA_IF_ACTION_MAKE0_EDIT_ID_WIDTH(unit, hwUnit) +
        MEA_IF_ACTION_MAKE0_PROTOCOL_WIDTH +
        MEA_IF_ACTION_MAKE0_LLC_EXIST_WIDTH +
        MEA_IF_ACTION_MAKE0_PM_ID_WIDTH(unit, hwUnit) +
        MEA_IF_ACTION_MAKE0_TM_ID_WIDTH(unit, hwUnit) +
        MEA_IF_ACTION_MAKE0_COS_WIDTH +
        MEA_IF_ACTION_MAKE1_COS_FORCE_WIDTH +
        MEA_IF_ACTION_MAKE1_COLOR_WIDTH +
        MEA_IF_ACTION_MAKE1_COLOR_FORCE_WIDTH +
        MEA_IF_ACTION_MAKE1_L2_PRI_WIDTH +
        MEA_IF_ACTION_MAKE1_L2_PRI_FORCE_WIDTH +
        MEA_IF_ACTION_MAKE1_MC_FID_SKIP_WIDTH +
        MEA_IF_ACTION_MAKE1_MC_FID_EDIT_WIDTH +
        MEA_IF_ACTION_MAKE1_ED_ID_FORCE_WIDTH +
        MEA_IF_ACTION_MAKE1_PM_ID_FORCE_WIDTH +
        MEA_IF_ACTION_MAKE1_TM_ID_FORCE_WIDTH +
        MEA_IF_ACTION_MAKE1_FORCE_MARKING_MAP_WIDTH +
        MEA_IF_ACTION_MAKE1_FORCE_COS_MAP_WIDTH +
        MEA_IF_ACTION_MAKE1_INGRESS_TS_TYPE_WIDTH +
        MEA_IF_ACTION_MAKE1_TDM_PACKET_TYPE_WIDTH +
        MEA_IF_ACTION_MAKE1_TDM_REMOVE_BYTE_WIDTH +
        MEA_IF_ACTION_MAKE1_TDM_REMOVE_LINE_WIDTH +
        MEA_IF_ACTION_MAKE1_TDM_CES_TYPE_WIDTH +
        MEA_IF_ACTION_MAKE1_TDM_USED_VLAN_WIDTH +
        MEA_IF_ACTION_MAKE1_TDM_USED_RTP_WIDTH +
        MEA_IF_ACTION_MAKE1_WRED_PROF_WIDTH +
        MEA_IF_ACTION_MAKE1_SET1588_WIDTH +
        MEA_IF_ACTION_MAKE1_MTU_ID_WIDTH +
        MEA_IF_ACTION_MAKE1_LAG_BYPASS_WIDTH + 
        MEA_IF_ACTION_MAKE1_FLOOD_ED_WIDTH +
        MEA_IF_ACTION_MAKE1_DROP_WIDTH +
        MEA_IF_ACTION_MAKE1_cluster_id(hwUnit) +
        MEA_IF_ACTION_MAKE1_cluster_id_valid + 
        MEA_IF_ACTION_MAKE1_FRAG_IP +
        MEA_IF_ACTION_MAKE1_IP_TUNNEL + 
        MEA_IF_ACTION_MAKE1_DEFRAG_EXT_IP +
        MEA_IF_ACTION_MAKE1_DEFRAG_INT_IP +
        MEA_IF_ACTION_MAKE1_DEFRAG_Reassembly_L2 +
        MEA_IF_ACTION_MAKE1_FRAG_IP_EXTERNAL_INTERNAL+
        MEA_IF_ACTION_MAKE1_FRAG_IP_TARGET_MTU +
        MEA_IF_ACTION_MAKE1_fwd_win +
        MEA_IF_ACTION_MAKE1_SFLOW_TYPE

  

        
        );

    numofRegs = count / 32;
    if (count % 32 != 0)
        numofRegs++;

if(numofRegs<=3)
	numofRegs=4;/*need 4*/


    return numofRegs;

}




MEA_Status mea_drv_action_hw_info_parsing(MEA_Unit_t unit_i,
                                          MEA_Action_HwEntry_dbt *action_info, void *retVal)
{

   
    MEA_Uint32 count_shift = 0;
    MEA_db_HwUnit_t hwUnit_i = 0;
    

        
    action_info->ed_id = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_EDIT_ID_WIDTH(unit_i, hwUnit_i), retVal);

    count_shift += MEA_IF_ACTION_MAKE0_EDIT_ID_WIDTH(unit_i, hwUnit_i);

    action_info->protocol = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_PROTOCOL_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE0_PROTOCOL_WIDTH;
    action_info->llc = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_LLC_EXIST_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE0_LLC_EXIST_WIDTH;
    action_info->fragment_enable = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_FRAGMENT_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE0_FRAGMENT_WIDTH;



    if (MEA_AFDX_SUPPORT == MEA_TRUE){

        action_info->afdx_A_B = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_AFDX_EXTRACT_SESSION_WIDTH, retVal);
        count_shift += MEA_IF_ACTION_MAKE0_AFDX_EXTRACT_SESSION_WIDTH;
        action_info->afdx_Rx_sessionId = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_AFDX_SESSION_ID_WIDTH, retVal);
        count_shift += MEA_IF_ACTION_MAKE0_AFDX_SESSION_ID_WIDTH;
        action_info->afdx_enable = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_AFDX_VALID_WIDTH, retVal);
        count_shift += MEA_IF_ACTION_MAKE0_AFDX_VALID_WIDTH;

    }


    action_info->pm_id = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_PM_ID_WIDTH(unit_i, hwUnit_i), retVal);
    count_shift += MEA_IF_ACTION_MAKE0_PM_ID_WIDTH(unit_i, hwUnit_i);

    action_info->tm_id = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_TM_ID_WIDTH(unit_i, hwUnit_i), retVal);
    count_shift += MEA_IF_ACTION_MAKE0_TM_ID_WIDTH(unit_i, hwUnit_i);

    /* Cos = which Queue is going*/


    //0 - disable ,1 - force value,2 – max value
    action_info->cos = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE0_COS_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE0_COS_WIDTH;

    action_info->cos_force = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_COS_FORCE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_COS_FORCE_WIDTH;

    action_info->color = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_COLOR_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_COLOR_WIDTH;
    action_info->color_force = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_COLOR_FORCE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_COLOR_FORCE_WIDTH;

    action_info->l2_pri = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_L2_PRI_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_L2_PRI_WIDTH;

    action_info->l2_pri_force = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_L2_PRI_FORCE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_L2_PRI_FORCE_WIDTH;


    action_info->mc_fid_skip = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_MC_FID_SKIP_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_MC_FID_SKIP_WIDTH;


    if (MEA_MC_EHP_NEW_MODE_SUPPORT == MEA_FALSE){
        
        action_info->mc_fid_edit = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_MC_FID_EDIT_WIDTH, retVal);
    }else{
        action_info->mc_fid_edit = action_info->ed_id;
    }
    /*reserve bit */
    count_shift += MEA_IF_ACTION_MAKE1_MC_FID_EDIT_WIDTH;


    action_info->edit_force = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_ED_ID_FORCE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_ED_ID_FORCE_WIDTH;

    action_info->pm_force = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_PM_ID_FORCE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_PM_ID_FORCE_WIDTH;

    action_info->tm_force = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_TM_ID_FORCE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TM_ID_FORCE_WIDTH;

    action_info->flowMarkingMappingProfile_force = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_FORCE_MARKING_MAP_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FORCE_MARKING_MAP_WIDTH;

    action_info->flowCoSMappingProfile_force = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_FORCE_COS_MAP_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FORCE_COS_MAP_WIDTH;

    action_info->fwd_ingress_TS_type = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_INGRESS_TS_TYPE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_INGRESS_TS_TYPE_WIDTH;

    action_info->l2_pri_prof_b5 = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_l2_pri_prof_b5, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_l2_pri_prof_b5;

#ifdef MEA_TDM_SW_SUPPORT
    action_info->tdm_packet_type = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_TDM_PACKET_TYPE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_PACKET_TYPE_WIDTH;
    action_info->tdm_remove_byte = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_TDM_REMOVE_BYTE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_REMOVE_BYTE_WIDTH;
    action_info->tdm_remove_line = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_TDM_REMOVE_LINE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_REMOVE_LINE_WIDTH;

    action_info->tdm_ces_type = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_TDM_CES_TYPE_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_CES_TYPE_WIDTH;
    action_info->tdm_used_vlan = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_TDM_USED_VLAN_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_USED_VLAN_WIDTH;
    action_info->tdm_used_rtp = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_TDM_USED_RTP_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_USED_RTP_WIDTH;
#endif
    action_info->wred_prof = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_WRED_PROF_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_WRED_PROF_WIDTH;

    action_info->set_1588 = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_SET1588_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_SET1588_WIDTH;

    action_info->mtu_id = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_MTU_ID_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_MTU_ID_WIDTH;

    action_info->lag_bypass = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_LAG_BYPASS_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_LAG_BYPASS_WIDTH;
    action_info->flood_ED = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_FLOOD_ED_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FLOOD_ED_WIDTH;
    action_info->Drop_en = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_DROP_WIDTH, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_DROP_WIDTH;

    action_info->overRule_vp_val = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_cluster_id(hwUnit_i), retVal);
    count_shift += MEA_IF_ACTION_MAKE1_cluster_id(hwUnit_i);
    action_info->overRule_vpValid = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_cluster_id_valid, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_cluster_id_valid;

    action_info->Fragment_IP = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_FRAG_IP, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FRAG_IP;
    action_info->IP_TUNNEL = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_IP_TUNNEL, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_IP_TUNNEL;
    action_info->Defrag_ext_IP = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_DEFRAG_EXT_IP, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_DEFRAG_EXT_IP;
    action_info->Defrag_int_IP = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_DEFRAG_INT_IP, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_DEFRAG_INT_IP;
    action_info->Defrag_Reassembly_L2_profile = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_DEFRAG_Reassembly_L2, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_DEFRAG_Reassembly_L2;
    action_info->fragment_ext_int = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_FRAG_IP_EXTERNAL_INTERNAL, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FRAG_IP_EXTERNAL_INTERNAL;
    action_info->fragment_target_mtu_prof = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_FRAG_IP_TARGET_MTU, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FRAG_IP_TARGET_MTU;
    action_info->fwd_win = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_fwd_win, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_fwd_win;
    action_info->sflow_Type = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_SFLOW_TYPE, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_SFLOW_TYPE;

    action_info->overRule_bfd_sp = MEA_OS_read_value(count_shift, MEA_IF_ACTION_MAKE1_overRule_bfd_sp, retVal);
    count_shift += MEA_IF_ACTION_MAKE1_overRule_bfd_sp;



    

    

    

    return MEA_OK;
}
/*--------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_UpdateHwEntryAction>                                      */
/*                                                                           */
/*                                                                           */
/*--------------------------------------------------------------------------*/
MEA_Status      mea_Action_buildHWData(MEA_Unit_t unit_i,
                                       MEA_db_HwUnit_t         hwUnit_i,
                                       MEA_db_HwId_t           hwId_i,
                                       MEA_Action_HwEntry_dbt *action_info, 
                                       MEA_Uint32 len,
                                       MEA_Uint32  *retVal)
{

   
   
    MEA_db_HwId_t pm_hwId;
    MEA_db_HwId_t ed_hwId;
    MEA_db_HwId_t tm_hwId;
    MEA_Uint32                count_shift;
    MEA_Uint32  numOfregs;


    numOfregs = mea_action_calc_numof_regs(unit_i, hwUnit_i);
        
    if (numOfregs > len){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "register on Action is not correct \n");
    }

    count_shift = 0;

    if (action_info->ed_id) {
        MEA_DRV_GET_HW_ID(unit_i,
            mea_drv_Get_EHP_ETH_db,
            (MEA_db_SwId_t)(action_info->ed_id),
            hwUnit_i,
            ed_hwId,
            return MEA_ERROR;);
    }
    else {
        ed_hwId = 0;
    }
    if (MEA_MC_EHP_NEW_MODE_SUPPORT){
        if (action_info->mc_fid_skip == MEA_FALSE){
            ed_hwId = action_info->mc_fid_edit; /*the MCedit */
        }
    }

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE0_EDIT_ID_WIDTH(unit_i, hwUnit_i),
        (MEA_Uint32)ed_hwId,
        retVal);

    count_shift += MEA_IF_ACTION_MAKE0_EDIT_ID_WIDTH(unit_i, hwUnit_i);


    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE0_PROTOCOL_WIDTH,
        (MEA_Uint32)action_info->protocol,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE0_PROTOCOL_WIDTH;



    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE0_LLC_EXIST_WIDTH,
        (((MEA_Uint32)(action_info->llc != MEA_FALSE)) ? 1 : 0),
        retVal);
    count_shift += MEA_IF_ACTION_MAKE0_LLC_EXIST_WIDTH;
  
    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE0_FRAGMENT_WIDTH,
        action_info->fragment_enable,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE0_FRAGMENT_WIDTH;

    if (MEA_AFDX_SUPPORT == MEA_TRUE){
        MEA_OS_insert_value(count_shift,
            MEA_IF_ACTION_MAKE0_AFDX_EXTRACT_SESSION_WIDTH,
            action_info->afdx_A_B,// TBD
            retVal);
        count_shift += MEA_IF_ACTION_MAKE0_AFDX_EXTRACT_SESSION_WIDTH;


        MEA_OS_insert_value(count_shift,
            MEA_IF_ACTION_MAKE0_AFDX_SESSION_ID_WIDTH,
            action_info->afdx_Rx_sessionId,// TBD
            retVal);
        count_shift += MEA_IF_ACTION_MAKE0_AFDX_SESSION_ID_WIDTH;
        MEA_OS_insert_value(count_shift,
            MEA_IF_ACTION_MAKE0_AFDX_VALID_WIDTH,
            action_info->afdx_enable,// TBD
            retVal);
        count_shift += MEA_IF_ACTION_MAKE0_AFDX_VALID_WIDTH;

    }



    if (action_info->pm_id) {
        mea_drv_Get_pm_hwId_Calculate(unit_i,
            action_info->pm_id,
            &pm_hwId);
    }
    else {
        pm_hwId = 0;
    }


    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE0_PM_ID_WIDTH(unit_i, hwUnit_i),
        (MEA_Uint32)pm_hwId,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE0_PM_ID_WIDTH(unit_i, hwUnit_i);


    if (action_info->tm_id) {
        MEA_DRV_GET_HW_ID(unit_i,
            mea_drv_Get_Policer_db,
            (MEA_db_SwId_t)(action_info->tm_id),
            hwUnit_i,
            tm_hwId,
            return MEA_ERROR;);
    }
    else {
        tm_hwId = 0;
    }


    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE0_TM_ID_WIDTH(unit_i, hwUnit_i),
        (MEA_Uint32)tm_hwId,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE0_TM_ID_WIDTH(unit_i, hwUnit_i);

    /* Cos = which Queue is going*/


    //0 - disable ,1 - force value,2 – max value


    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE0_COS_WIDTH,
        (MEA_Uint32)action_info->cos,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE0_COS_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_COS_FORCE_WIDTH,
        (MEA_Uint32)action_info->cos_force,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_COS_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_COLOR_WIDTH,
        (MEA_Uint32)action_info->color,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_COLOR_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_COLOR_FORCE_WIDTH,
        (MEA_Uint32)action_info->color_force,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_COLOR_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_L2_PRI_WIDTH,
        (MEA_Uint32)action_info->l2_pri,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_L2_PRI_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_L2_PRI_FORCE_WIDTH,
        (MEA_Uint32)action_info->l2_pri_force,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_L2_PRI_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_MC_FID_SKIP_WIDTH,
        (MEA_Uint32)action_info->mc_fid_skip,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_MC_FID_SKIP_WIDTH;

    if (MEA_MC_EHP_NEW_MODE_SUPPORT == MEA_FALSE){
        MEA_OS_insert_value(count_shift,
            MEA_IF_ACTION_MAKE1_MC_FID_EDIT_WIDTH,
            (MEA_Uint32)action_info->mc_fid_edit,
            retVal);
    }
    count_shift += MEA_IF_ACTION_MAKE1_MC_FID_EDIT_WIDTH;


    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_ED_ID_FORCE_WIDTH,
        ((action_info->ed_id != 0 || action_info->mc_fid_skip == 0) ? 1 : 0),
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_ED_ID_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_PM_ID_FORCE_WIDTH,
        (action_info->pm_id != 0 ? 1 : 0),
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_PM_ID_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_TM_ID_FORCE_WIDTH,
        (action_info->tm_id != 0 ? 1 : 0),
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TM_ID_FORCE_WIDTH;


    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_FORCE_MARKING_MAP_WIDTH,
        action_info->flowMarkingMappingProfile_force,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FORCE_MARKING_MAP_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_FORCE_COS_MAP_WIDTH,
        action_info->flowCoSMappingProfile_force,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FORCE_COS_MAP_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_INGRESS_TS_TYPE_WIDTH,
        action_info->fwd_ingress_TS_type,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_INGRESS_TS_TYPE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_l2_pri_prof_b5,
        action_info->l2_pri_prof_b5,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_l2_pri_prof_b5;





#ifdef MEA_TDM_SW_SUPPORT

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_TDM_PACKET_TYPE_WIDTH,
        action_info->tdm_packet_type,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_PACKET_TYPE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_TDM_REMOVE_BYTE_WIDTH,
        action_info->tdm_remove_byte,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_REMOVE_BYTE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_TDM_REMOVE_LINE_WIDTH,
        action_info->tdm_remove_line,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_REMOVE_LINE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_TDM_CES_TYPE_WIDTH,
        action_info->tdm_ces_type,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_CES_TYPE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_TDM_USED_VLAN_WIDTH,
        action_info->tdm_used_vlan,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_USED_VLAN_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_TDM_USED_RTP_WIDTH,
        action_info->tdm_used_rtp,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_TDM_USED_RTP_WIDTH;
#endif

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_WRED_PROF_WIDTH,
        action_info->wred_prof,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_WRED_PROF_WIDTH;


    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_SET1588_WIDTH,
        action_info->set_1588,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_SET1588_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_MTU_ID_WIDTH,
        action_info->mtu_id,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_MTU_ID_WIDTH;


    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_LAG_BYPASS_WIDTH,
        action_info->lag_bypass,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_LAG_BYPASS_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_FLOOD_ED_WIDTH,
        action_info->flood_ED,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FLOOD_ED_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_DROP_WIDTH,
        action_info->Drop_en,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_DROP_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_cluster_id(hwUnit_i),
        action_info->overRule_vp_val,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_cluster_id(hwUnit_i);

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_cluster_id_valid,
        action_info->overRule_vpValid,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_cluster_id_valid;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_FRAG_IP,
        action_info->Fragment_IP,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FRAG_IP;
    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_IP_TUNNEL,
        action_info->IP_TUNNEL,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_IP_TUNNEL;
    
    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_DEFRAG_EXT_IP,
        action_info->Defrag_ext_IP,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_DEFRAG_EXT_IP;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_DEFRAG_INT_IP,
        action_info->Defrag_int_IP,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_DEFRAG_INT_IP;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_DEFRAG_Reassembly_L2,
        action_info->Defrag_Reassembly_L2_profile,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_DEFRAG_Reassembly_L2;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_FRAG_IP_EXTERNAL_INTERNAL,
        action_info->fragment_ext_int,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FRAG_IP_EXTERNAL_INTERNAL;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_FRAG_IP_TARGET_MTU,
        action_info->fragment_target_mtu_prof,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_FRAG_IP_TARGET_MTU;
    
    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_fwd_win,
        action_info->fwd_win,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_fwd_win;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_SFLOW_TYPE,
        action_info->sflow_Type,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_SFLOW_TYPE;

    MEA_OS_insert_value(count_shift,
        MEA_IF_ACTION_MAKE1_overRule_bfd_sp,
        action_info->overRule_bfd_sp,
        retVal);
    count_shift += MEA_IF_ACTION_MAKE1_overRule_bfd_sp;

    
    
     
   

    

    

    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    
        if (mea_db_Set_Hw_Regs(unit_i,
            MEA_Action_db,
            hwUnit_i,
            hwId_i,
            MEA_ACTION_ADDRES_DDR_REGS, //len,
            retVal) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                __FUNCTION__,
                hwUnit_i,
                hwId_i);
        }


    

    
    return MEA_OK;
}


static void      mea_UpdateHwEntryAction(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    MEA_Unit_t              unit_i     = (MEA_Unit_t)arg1;
//    MEA_db_HwUnit_t         hwUnit_i   = (MEA_db_HwUnit_t)((long)arg2);
    MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32       *data = (MEA_Uint32*)arg4;
    MEA_Uint32 i;
 

    




    for (i=0;i<len;i++) {
        MEA_API_WriteReg(unit_i,
                         MEA_IF_WRITE_DATA_REG(i) , 
                         data[i],
                         MEA_MODULE_IF);

       // printf("data[%d]= 0x08%x\n",i, data[i]);

    }

   

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Action_UpdateHw>                                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Action_UpdateHw(MEA_Unit_t               unit_i,
                               MEA_Action_t             actionId_i,
                               MEA_Action_HwEntry_dbt  *action_info_pio)
{
    MEA_db_HwUnit_t       hwUnit;
    MEA_db_HwId_t         hwId;
    MEA_Uint32            retVal[MEA_ACTION_ADDRES_DDR_MAX_WIDTH];
    MEA_Uint32            addressDDR;
 //   MEA_Uint32           i;
    MEA_Uint32         numofRegs = 0;
    MEA_ind_write_t   ind_write;

    if (actionId_i >= MEA_MAX_NUM_OF_ACTIONS) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- ActionId_i %d is out of range \n",
                          __FUNCTION__,actionId_i);
        return MEA_ERROR;
    }

    if (action_info_pio == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - action_info_pi == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if(action_info_pio->valid == MEA_TRUE){
    /* convert cos to hw format */
        
    if (!action_info_pio->flowCoSMappingProfile_force) {
        switch (action_info_pio->cos_force) {
        case 0:
              action_info_pio->cos_force = 0;
              action_info_pio->cos       = 7;
            break;
        case 1:
            break;
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- Unknown action_info->cos_force %d\n",
                             __FUNCTION__,
                             action_info_pio->cos_force);
            return MEA_ERROR;
        }
    }

    /* convert l2_pri to hw format */
    if (!action_info_pio->flowMarkingMappingProfile_force) {
        switch (action_info_pio->l2_pri_force) {
        case 0:
              action_info_pio->l2_pri_force = 0;
              action_info_pio->l2_pri       = 7;
            break;
        case 1:
            break;
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- Unknown action_info->l2_pri_force %d\n",
                             __FUNCTION__,
                             action_info_pio->l2_pri_force);
            return MEA_ERROR;
        }
    }

    if (action_info_pio->flowCoSMappingProfile_force == MEA_FALSE &&  action_info_pio->flowMarkingMappingProfile_force == MEA_FALSE){
        /* convert color to hw format */
        switch (action_info_pio->color_force) {
        case 0:
            action_info_pio->color = 2;
            break;
        case 1:
            break;

        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- Unknown action_info->color_force %d\n",
                __FUNCTION__,
                action_info_pio->color_force);
            return MEA_ERROR;
        }
    }
}

    /* Get the hwUnit and hwId */
    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
                                  mea_drv_Get_Action_db,
                                  actionId_i,
                                  hwUnit,
                                  hwId,
                                  return MEA_ERROR);


    MEA_OS_memset(&retVal, 0, sizeof(retVal));


	if ((action_info_pio->Action_type == MEA_ACTION_TYPE_SERVICE_EXTERNAL) ||
        (action_info_pio->Action_type == MEA_ACTION_TYPE_ACL5)
    
    ){

        mea_Action_buildHWData(unit_i,
            hwUnit,
            hwId,
            action_info_pio,
            MEA_ACTION_BASE_ADDRES_DDR_WIDTH,/*256bit*/
            &retVal[0]);
    
    }
    else{


        if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit_i, MEA_DB_ACTION_TYPE) == MEA_TRUE)
        {

            mea_Action_buildHWData(unit_i,
                hwUnit,
                hwId,
                action_info_pio,
                MEA_ACTION_BASE_ADDRES_DDR_WIDTH,/*256bit*/
                &retVal[0]);

            /*write to ACTION DB on DDR */
            addressDDR = MEA_ACTION_BASE_ADDRES_DDR_START + ((hwId * MEA_ACTION_BASE_ADDRES_DDR_WIDTH * 4));

            MEA_DRV_WriteDDR_DATA_BLOCK(unit_i, hwId, addressDDR, &retVal[0], MEA_ACTION_BASE_ADDRES_DDR_WIDTH, DDR_Action);

        }
        else {

            numofRegs = mea_action_calc_numof_regs(unit_i, hwUnit);
            mea_Action_buildHWData(unit_i,
                hwUnit,
                hwId,
                action_info_pio,
                numofRegs,/* 3 regs*/
                &retVal[0]);


            /* build the ind_write value */
            ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_ACTION;
            ind_write.tableOffset = hwId;
            ind_write.cmdReg = MEA_IF_CMD_REG;
            ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
            ind_write.statusReg = MEA_IF_STATUS_REG;
            ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
            ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
            ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
            ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntryAction;
            ind_write.funcParam1 = (MEA_funcParam)unit_i;
            ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
            ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
            ind_write.funcParam4 = (MEA_funcParam)&retVal;


            if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }
        }
    }
   
        MEA_OS_memcpy(&(MEA_Action_drv_HwTable[actionId_i]),
            action_info_pio,
            sizeof(MEA_Action_drv_HwTable[0]));
    




     


    /* return to caller */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Action_GetHw>                                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Action_GetHw(MEA_Unit_t                   unit_i,
                            MEA_Action_t             actionId_i,
                               MEA_Action_HwEntry_dbt      *action_info_po)
{

    if (actionId_i >= MEA_MAX_NUM_OF_ACTIONS) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ActionId_i %s is out of range \n",
                          __FUNCTION__,actionId_i);
        return MEA_ERROR;
    }

    if (action_info_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - action_info_po == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(action_info_po,
                  &(MEA_Action_drv_HwTable[actionId_i]),
                  sizeof(*action_info_po));

    /* return to caller */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Action_Create_ActionToCpu>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Action_Create_ActionToCpu(MEA_Unit_t unit_i)
{

    MEA_Action_Entry_Data_dbt             Action_Data;
    MEA_OutPorts_Entry_dbt                Action_OutPorts;
    MEA_Policer_Entry_dbt                 Action_Policer;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_Editing;
    MEA_EHP_Info_dbt                      Action_Editing_Info;
    MEA_Action_t                          Action_Id;
    MEA_AcmMode_t                 ACM_Mode;
    MEA_Port_type_te porttype = MEA_PORTTYPE_GBE_MAC;
	
    if (MEA_API_Get_IsPortValid(127, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE/*silent*/) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
            " - No ACTION to CPU  \n");
        return MEA_OK;
    }

    if (MEA_API_Get_IngressPort_Type(unit_i, 127, &porttype) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " MEA_API_Get_IngressPort_Type failed port=%d\n", 127);
        return MEA_ERROR;
    }

    MEA_OS_memset(&Action_Data, 0, sizeof(Action_Data));
    Action_Data.ed_id_valid = MEA_TRUE;
    Action_Data.ed_id = 0; /* generate new */
    Action_Data.tm_id_valid = MEA_TRUE;
    Action_Data.tm_id = 0; /* generate new */
    Action_Data.pm_id_valid = MEA_TRUE;
    Action_Data.pm_id = 0; /* generate new */
    Action_Data.pm_type = MEA_PM_TYPE_ALL;
    Action_Data.proto_llc_valid = MEA_TRUE;
    Action_Data.output_ports_valid = MEA_TRUE;
    Action_Data.protocol_llc_force = MEA_TRUE;
    Action_Data.Protocol = 1;
    Action_Data.Llc = 0;
    Action_Data.force_color_valid = MEA_ACTION_FORCE_COMMAND_VALUE;
    Action_Data.COLOR = MEA_POLICER_COLOR_VAL_GREEN;

    MEA_OS_memset(&Action_OutPorts, 0, sizeof(Action_OutPorts));
    ((MEA_Uint32*)(&Action_OutPorts))[MEA_CPU_PORT / 32] |= (1 << (MEA_CPU_PORT % 32));

    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing));
    MEA_OS_memset(&Action_Editing_Info, 0, sizeof(Action_Editing_Info));
    Action_Editing.ehp_info = &Action_Editing_Info;
    Action_Editing.num_of_entries = 1;
    MEA_OS_memcpy(&(Action_Editing.ehp_info[0].output_info),
        &Action_OutPorts,
        sizeof(Action_Editing.ehp_info[0].output_info));

    if (MEA_ACTION_0_CPU_APPEND_QinQ == MEA_TRUE){
        Action_Editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_APPEND_APPEND;
        Action_Editing.ehp_info[0].ehp_data.atm_info.val.all = (MEA_ACTION_0_ETHERTYPE_OUTER<<16) | (MEA_ACTION_0_OUTER_VLANID); //vlan 4011 //// wiil check for vlan 4011
        Action_Editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
    } else {
        Action_Editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_APPEND;
    }
    Action_Editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
    Action_Editing.ehp_info[0].ehp_data.eth_info.val.all =0x81000000;  // the src port is the vlan
    Action_Editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = 1;
    Action_Editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
    Action_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
    Action_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
    Action_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
    Action_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
    Action_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
    Action_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

    MEA_OS_memset(&Action_Policer,0,sizeof(Action_Policer));
    if(mea_drv_Get_DeviceInfo_NumOfPolicerProfiles((MEA_UNIT_0),(0)) != 0){
    Action_Policer.CBS  = MEA_CPU_SERVICE_POLICER_CBS_DEF_VAL;
    Action_Policer.EBS  = MEA_CPU_SERVICE_POLICER_EBS_DEF_VAL;
    Action_Policer.EIR  = MEA_CPU_SERVICE_POLICER_EIR_DEF_VAL;
    Action_Policer.CIR  = MEA_CPU_SERVICE_POLICER_CIR_DEF_VAL;
    Action_Policer.cup  = MEA_CPU_SERVICE_POLICER_CUP_DEF_VAL;
    Action_Policer.gn_type=1;

    Action_Policer.comp = MEA_POLICER_COMP_ENFORCE;
    Action_Policer.color_aware=0;
    if(porttype != MEA_PORTTYPE_GBE_MAC)
        Action_Data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL;
}    
    for (ACM_Mode=0;
         ACM_Mode<mea_drv_Get_DeviceInfo_NumOfAcmModes(unit_i,0);
         ACM_Mode++) {
         if (ACM_Mode == 0) {
            MEA_ActionToCput_policer_profile_id= 0;
         }
         if (mea_drv_Policer_Profile_Create(unit_i,
                                            MEA_INGRESS_PORT_PROTO_TRANS,
                                            &Action_Policer,
                                            ACM_Mode,
                                            &MEA_ActionToCput_policer_profile_id) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Policer_Profile_Create failed (ACM_Mode=%d,id=%d)\n",
                              __FUNCTION__,ACM_Mode,MEA_ActionToCput_policer_profile_id);
            return MEA_ERROR;
        }
    }
         

    Action_Id = MEA_ACTION_ID_TO_CPU;
#if 1
    Action_Data.pm_type= MEA_PM_TYPE_ALL;
    Action_Data.pm_id_valid=MEA_TRUE;
    Action_Data.pm_id = 0;//(MEA_MAX_NUM_OF_PM_ID-1);
    Action_Data.policer_prof_id_valid = MEA_TRUE;
    Action_Data.policer_prof_id = 0;
    Action_Data.no_allow_del_Act = MEA_TRUE;
#endif
	Action_Data.Action_type = MEA_ACTION_TYPE_FWD; /* Internal */
    if (MEA_API_Create_Action (unit_i,
                               &Action_Data,
                               &Action_OutPorts,
                               &Action_Policer,
                               &Action_Editing,
                               &Action_Id) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- MEA_API_Create_Action failed (id=%d) \n",
                         __FUNCTION__,
                         Action_Id);
       return MEA_ERROR;
    }
    

    /* Add Owner to not allowed delete of this action */
    if (MEA_API_AddOwner_Action(unit_i,MEA_ACTION_ID_TO_CPU) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- MEA_API_AddOwner_Action failed (id=%d) \n",
                         __FUNCTION__,
                         MEA_ACTION_ID_TO_CPU);
       //mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
       return MEA_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Service ACTION to CPU  %d \n", Action_Id);
    /*Create FWD Action to CPU   */
    Action_Id = MEA_ACTION_ID_TO_FWD_CPU;
	Action_Data.Action_type = MEA_ACTION_TYPE_FWD;
    Action_Data.COS = 5;
    Action_Data.force_cos_valid = MEA_TRUE;

        if (MEA_API_Create_Action(unit_i,
            &Action_Data,
            &Action_OutPorts,
            &Action_Policer,
            &Action_Editing,
            &Action_Id) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_Create_Action failed (id=%d) \n",
                __FUNCTION__,
                Action_Id);
            return MEA_ERROR;
        }

        /* Add Owner to not allowed delete of this action */
        if (MEA_API_AddOwner_Action(unit_i, MEA_ACTION_ID_TO_FWD_CPU) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_AddOwner_Action failed (id=%d) \n",
                __FUNCTION__,
                MEA_ACTION_ID_TO_CPU);
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
            return MEA_ERROR;
        }


        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " FWD ACTION to CPU  %d \n", Action_Id);


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Action_Delete_ActionToCpu>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Action_Delete_ActionToCpu(MEA_Unit_t unit_i) 
{

    MEA_AcmMode_t                 ACM_Mode;
	
     //TBD
     if (MEA_API_Get_IsPortValid(127, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE/*silent*/)==MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                         " Action To CPU  \n");
       return MEA_OK;
    } 
    
     /* Del Owner to cpu Action to allowed the conclude to delete this action */
     if (MEA_API_DelOwner_Action(unit_i, MEA_ACTION_ID_TO_FWD_CPU) != MEA_OK) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s \n- MEA_API_DelOwner_Action failed (id=%d) \n",
             __FUNCTION__,
             MEA_ACTION_ID_TO_CPU);
         return MEA_ERROR;
     }

     /* Delete  Action to cpu */
	 if (MEA_DRV_Delete_Action(unit_i, MEA_ACTION_ID_TO_FWD_CPU, MEA_ACTION_TYPE_FWD) != MEA_OK) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s \n- MEA_DRV_Delete_Action failed (id=%d) \n",
             __FUNCTION__,
             MEA_ACTION_ID_TO_FWD_CPU);
         return MEA_ERROR;
     }


    /* Del Owner to cpu Action to allowed the conclude to delete this action */
	 if (MEA_API_DelOwner_Action(unit_i, MEA_ACTION_ID_TO_CPU) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- MEA_API_DelOwner_Action failed (id=%d) \n",
                         __FUNCTION__,
                         MEA_ACTION_ID_TO_CPU);
       return MEA_ERROR;
    }

    /* Delete  Action to cpu */
	 if (MEA_DRV_Delete_Action(unit_i, MEA_ACTION_ID_TO_CPU, MEA_ACTION_TYPE_FWD) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- MEA_DRV_Delete_Action failed (id=%d) \n",
                         __FUNCTION__,
                         MEA_ACTION_ID_TO_CPU);
       return MEA_ERROR;
    }

    for (ACM_Mode=0;
         ACM_Mode<mea_drv_Get_DeviceInfo_NumOfAcmModes(unit_i,0);
         ACM_Mode++) {
         if (mea_drv_Policer_Profile_Delete(unit_i,
                                            ACM_Mode,
                                            MEA_ActionToCput_policer_profile_id) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Policer_Profile_Delete failed (ACM_Mode=%d,id=%d)\n",
                              __FUNCTION__,ACM_Mode,MEA_ActionToCput_policer_profile_id);
            return MEA_ERROR;
        }
    }

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_Action_db>                               */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Action_db(MEA_Unit_t unit_i,
                                  MEA_db_dbt* db_po) {

    if (db_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - db_po == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    *db_po = MEA_Action_db;

    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_Action_num_of_hw_size_func>              */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Action_num_of_hw_size_func(MEA_Unit_t     unit_i,
                                                   MEA_db_HwUnit_t   hwUnit_i,
                                                   MEA_db_HwId_t *length_o,
                                                   MEA_Uint16    *num_of_regs_o) 
{

    if (hwUnit_i >= mea_drv_num_of_hw_units) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - hwUnit_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          hwUnit_i,
                          mea_drv_num_of_hw_units);
        return MEA_ERROR;
    }

    if (length_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - legnth_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (num_of_regs_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - num_of_regs_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    *length_o = MEA_MAX_NUM_OF_ACTIONS; // internal-->   MEA_IF_CMD_PARAM_TBL_TYP_ACTION_LENGTH(hwUnit_i);
    *num_of_regs_o = ENET_IF_CMD_PARAM_TBL_TYP_ACTION_NUM_OF_REGS(hwUnit_i);

    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_Action_num_of_sw_size_func>             */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Action_num_of_sw_size_func(MEA_Unit_t    unit_i,
                                                  MEA_db_SwId_t *length_o,
                                                  MEA_Uint32    *width_o) 
{
    if (length_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - legnth_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (width_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - width_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    *length_o = (MEA_db_SwId_t)MEA_MAX_NUM_OF_ACTIONS;
    *width_o  = 0;

    return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Action_Init>                                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Action_Init(MEA_Unit_t unit_i) 
{

    MEA_Uint32 size;
    MEA_Uint32 tempsize;
    if(b_bist_test){
      return MEA_OK;
    }

    /* Init Action object */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Action...\n");
    /* Init db - Hardware shadow */
    if (mea_db_Init(unit_i,
                    "Action ",
                    mea_drv_Get_Action_num_of_sw_size_func,
                    mea_drv_Get_Action_num_of_hw_size_func,
                    &MEA_Action_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- mea_db_Init failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Allocate MEA_Action_drv_Table */
    size = sizeof(mea_action_drv_dbt);
    tempsize = MEA_MAX_NUM_OF_ACTIONS;
    size = tempsize*size;
    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,  " MEA_Action_drv_Table size=%d\n",size);

    MEA_Action_drv_Table = (mea_action_drv_dbt*)MEA_OS_BigMalloc(size);
    if (MEA_Action_drv_Table == NULL) {
        mea_db_Conclude(unit_i,&MEA_Action_db);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- Allocate MEA_Action_drv_Table failed (size=%d)\n",
                          __FUNCTION__,
                          size);
        return MEA_ERROR;
    }
    MEA_OS_memset((char*)MEA_Action_drv_Table,0,size);

    /* Allocate MEA_Action_drv_HwTable */
    size = MEA_MAX_NUM_OF_ACTIONS*sizeof(MEA_Action_HwEntry_dbt);
    MEA_Action_drv_HwTable = (MEA_Action_HwEntry_dbt*)MEA_OS_BigMalloc(size);
    if (MEA_Action_drv_HwTable == NULL) {
        MEA_OS_free(MEA_Action_drv_Table);
        MEA_Action_drv_Table = NULL;
        mea_db_Conclude(unit_i,&MEA_Action_db);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- Allocate MEA_Action_drv_HwTable failed (size=%d)\n",
                          __FUNCTION__,
                          size);
        return MEA_ERROR;
    }
    MEA_OS_memset((char*)MEA_Action_drv_HwTable,0,size);



    size = MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW * sizeof(MEA_Bool);
    MEA_ACL5_Action_drv_Table = (MEA_Bool*)MEA_OS_BigMalloc(size);
    if (MEA_ACL5_Action_drv_Table == NULL) {
        MEA_OS_free(MEA_Action_drv_Table);
        MEA_Action_drv_Table = NULL;
        MEA_OS_free(MEA_Action_drv_HwTable);
        MEA_Action_drv_HwTable=NULL;
        mea_db_Conclude(unit_i, &MEA_Action_db);

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- Allocate MEA_ACL5_Action_drv_Table failed (size=%d)\n",
            __FUNCTION__,
            size);
        return MEA_ERROR;
    }
    MEA_OS_memset((char*)MEA_ACL5_Action_drv_Table, 0, size);

   

    


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Action_ReInit>                                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Action_ReInit(MEA_Unit_t unit_i) 
{
    MEA_Bool  ACTION_IS_DDR;
    
    if (b_bist_test) {
        return MEA_OK;
    }
    ACTION_IS_DDR = mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit_i, MEA_DB_ACTION_TYPE);

    /* ReInit Action table   */
    if (mea_db_ReInit(unit_i,
                      MEA_Action_db,
                      ENET_IF_CMD_PARAM_TBL_TYP_ACTION,
                      MEA_MODULE_IF,
                      NULL,
                      ACTION_IS_DDR) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- mea_db_ReInit for Action failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

    


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Action_Conclude>                                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Action_Conclude(MEA_Unit_t unit_i) 
{

    MEA_Action_t actionId;
    MEA_Bool found;


    if (MEA_API_GetFirst_Action(unit_i,
                                &actionId,
								&found, MEA_ACTION_TYPE_FWD) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s \n- MEA_API_GetFirst_Action failed \n",
                              __FUNCTION__);
        return MEA_ERROR;
    }
    while (found) {

        if (MEA_API_Delete_Action(unit_i,actionId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s \n- MEA_API_Delete_Action failed (id=%d)\n",
                              __FUNCTION__,actionId);
            return MEA_ERROR;
        }

		if (MEA_API_GetNext_Action(unit_i, &actionId, &found, MEA_ACTION_TYPE_FWD) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s \n- MEA_API_GetNext_Action failed (id=%d)\n",
                              __FUNCTION__,actionId);
            return MEA_ERROR;
       }
    }

    /* Free MEA_Action_drv_Table */
    MEA_OS_BigFree(MEA_Action_drv_Table);
    MEA_Action_drv_Table = NULL;

    /* Free MEA_Action_drv_HwTable */
    MEA_OS_BigFree(MEA_Action_drv_HwTable);
    MEA_Action_drv_HwTable = NULL;

    /* Conclude db */
    if (mea_db_Conclude(unit_i,&MEA_Action_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- mea_dn_Conclude failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    return MEA_OK;
}





/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <mea_action_drv_data_getFreeId>                                        */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status mea_action_drv_getFreeId(MEA_Unit_t unit_i, MEA_Action_t *ActionId_o, mea_action_type_te Action_type)
{

    MEA_Action_t ActionId,start_ActionId,end_ActionId;
    MEA_db_HwUnit_t           hwUnit;

    if (ActionId_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ActionId_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    
    
    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return 0;);

	switch (Action_type) {
    case MEA_ACTION_TYPE_SERVICE:
        start_ActionId = MEA_ACTION_TBL_SERVICE_START_INDEX+1;
        end_ActionId   = MEA_ACTION_TBL_SERVICE_START_INDEX +
                         MEA_MAX_NUM_OF_SERVICE_ACTIONS-1;
        break;
    case MEA_ACTION_TYPE_FWD: 
        start_ActionId = MEA_ACTION_TBL_FWD_START_INDEX; /*+1*/
        end_ActionId   = MEA_ACTION_TBL_FWD_START_INDEX +
                         MEA_MAX_NUM_OF_FWD_ACTIONS-1;
        break;
    case MEA_ACTION_TYPE_FILTER:
        start_ActionId = MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit);
        end_ActionId   = MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit) +
                         MEA_MAX_NUM_OF_FILTER_ACTIONS-1;
        break;
    case MEA_ACTION_TYPE_HPM:
        start_ActionId = MEA_ACTION_TBL_HPM_START_INDEX(hwUnit);
        end_ActionId   = MEA_ACTION_TBL_HPM_START_INDEX(hwUnit) + MEA_MAX_NUM_OF_HPM_ACTIONS - 1;

        break;
	case MEA_ACTION_TYPE_SERVICE_EXTERNAL:
		start_ActionId = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX;
		end_ActionId = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX + MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW -1;
		break;
    case MEA_ACTION_TYPE_ACL5:
        start_ActionId = MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX;
        end_ActionId = MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX + MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW - 1;
        break;

         
    case MEA_ACTION_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- Unknown action type (%d) \n",
						  __FUNCTION__, Action_type);
        return MEA_ERROR;
    }

    if (Action_type == MEA_ACTION_TYPE_HPM) {
        if (*ActionId_o == 0 || *ActionId_o == MEA_PLAT_GENERATE_NEW_ID) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_ACTION_TYPE_HPM (%d) the id need to be set\n",
                __FUNCTION__, *ActionId_o);
            return MEA_ERROR;
        }
    }

    for (ActionId = start_ActionId; 
         ActionId <= end_ActionId; 
         ActionId++) {

        //if (!MEA_API_IsValid_Action_Id(unit_i,ActionId)) {
        //    continue;
        //}

        if ( MEA_Action_drv_Table[ActionId].valid == MEA_FALSE) {
            *ActionId_o=ActionId;
            return MEA_OK;
        }
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s \n- no free action id \n",
                      __FUNCTION__);
    return MEA_ERROR;
}



/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <mea_action_drv_data_exist>                                            */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
static MEA_Bool mea_action_drv_data_exist(MEA_Unit_t unit_i,
                                          mea_action_drv_dbt *entry_i,
										  mea_action_type_te Action_type,
                                          MEA_Action_t         *id_o)
{
    MEA_Action_t index;
    MEA_Action_t start_ActionId,end_ActionId;
    MEA_db_HwUnit_t           hwUnit;
    
    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return 0;);


	switch (Action_type) {
    case MEA_ACTION_TYPE_SERVICE:
        start_ActionId = MEA_ACTION_TBL_SERVICE_START_INDEX+1;
        end_ActionId   = MEA_ACTION_TBL_SERVICE_START_INDEX +
                         MEA_MAX_NUM_OF_SERVICE_ACTIONS;
        break;
    case MEA_ACTION_TYPE_FWD: 
        start_ActionId = MEA_ACTION_TBL_FWD_START_INDEX;/*+1*/
        end_ActionId   = MEA_ACTION_TBL_FWD_START_INDEX +
                         MEA_MAX_NUM_OF_FWD_ACTIONS;
        break;
    case MEA_ACTION_TYPE_FILTER:
        start_ActionId = MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit);
        end_ActionId   = MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit) +
                         MEA_MAX_NUM_OF_FILTER_ACTIONS;
        break;
    case MEA_ACTION_TYPE_SERVICE_EXTERNAL:
        start_ActionId = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX;
        end_ActionId = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX +
            ENET_IF_CMD_PARAM_TBL_TYP_EXTERNAL_SERVICE_ACTION_LENGTH(hwUnit);
        break;
    case MEA_ACTION_TYPE_ACL5:
        start_ActionId = MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX;
        end_ActionId = MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX + MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW - 1;
        break;


    case MEA_ACTION_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown action type (%d) \n",
						  __FUNCTION__, Action_type);
        return MEA_ERROR;
    }

    

    for (index = start_ActionId; 
         index <= end_ActionId; 
         index++) 
    {
        if ( MEA_Action_drv_Table[index].valid )
        {
            if ( MEA_OS_memcmp(&(entry_i->data), 
                &(MEA_Action_drv_Table[index].data), 
                               sizeof(MEA_Action_Entry_Data_dbt)) == 0 )
            {
                if (!( MEA_OS_memcmp(&(entry_i->out_ports), 
                    &(MEA_Action_drv_Table[index].out_ports), 
                               sizeof(MEA_OutPorts_Entry_dbt)) == 0 ))
                {
                    return MEA_FALSE;
                }
                if (!((entry_i->EHP_MC == MEA_Action_drv_Table[index].EHP_MC) && 
                      (entry_i->MC_ID  == MEA_Action_drv_Table[index].MC_ID )  ))
                {
                    return MEA_FALSE;
                }
                *id_o = index;
                return MEA_TRUE;
            }
        }
    }

    return MEA_FALSE;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status mea_Action_GetOutport(MEA_Unit_t unit_i,
    MEA_Action_t actionId_i,
    MEA_OutPorts_Entry_dbt          *out_ports)
{

    MEA_Bool exist;

    if (MEA_API_IsExist_Action(unit_i, actionId_i, &exist) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n-MEA_API_IsExist_Action %d failed \n",
            __FUNCTION__, actionId_i);
        return MEA_ERROR;
    }
    if (exist == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- ActionId %d not exist \n",
            __FUNCTION__, actionId_i);
        return MEA_ERROR;
    }

    if (out_ports) {
        if(MEA_Action_drv_Table[actionId_i].data.output_ports_valid == MEA_TRUE)
          MEA_OS_memcpy(out_ports, &MEA_Action_drv_Table[actionId_i].out_ports, sizeof(MEA_OutPorts_Entry_dbt));
    }

    return MEA_OK;



}



MEA_Status mea_Action_GetDirection      (MEA_Unit_t              unit_i,
                                         MEA_Action_t            ActionId_i,
                                         MEA_Uint32             *direction_o)
{

    MEA_Bool exist;

    if(MEA_API_IsExist_Action(unit_i,ActionId_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n-MEA_API_IsExist_Action %d failed \n",
                         __FUNCTION__,ActionId_i);
        return MEA_ERROR;
    }
    if (exist == MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- ActionId %d not exist \n",
                         __FUNCTION__,ActionId_i);
       return MEA_ERROR; 
    }

    if (direction_o) {
        *direction_o = MEA_Action_drv_Table[ActionId_i].data.direction;
    }

    return MEA_OK;
}
/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <mea_Action_GetType>                                                   */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status mea_Action_GetType           (MEA_Unit_t              unit_i,
                                         MEA_Action_t            actionId_i,
                                         mea_action_type_te     *type_o)
{

    MEA_db_HwUnit_t           hwUnit;
    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return 0;);


    if (actionId_i >= MEA_MAX_NUM_OF_ACTIONS) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- actionId_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          actionId_i,
                          MEA_MAX_NUM_OF_ACTIONS);
       return MEA_ERROR;
    }

    if (type_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- type_o == NULL \n",
                          __FUNCTION__);
       return MEA_ERROR;
    }

    if (actionId_i == MEA_ACTION_ID_TO_CPU) {
        *type_o = MEA_ACTION_TYPE_DEFAULT;
    } else {
       if ((actionId_i >= MEA_ACTION_TBL_SERVICE_START_INDEX+1) &&
           (actionId_i <  MEA_ACTION_TBL_SERVICE_START_INDEX+
                          MEA_MAX_NUM_OF_SERVICE_ACTIONS      )  ) {
          *type_o = MEA_ACTION_TYPE_SERVICE;
       } else {
          if ((actionId_i >= MEA_ACTION_TBL_FWD_START_INDEX+1) &&
              (actionId_i <  MEA_ACTION_TBL_FWD_START_INDEX+
                             MEA_MAX_NUM_OF_FWD_ACTIONS      )  ) {
             *type_o = MEA_ACTION_TYPE_FWD;
          } else {
             if ((actionId_i >= MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit)+1) &&
                 (actionId_i <  MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit)+
                                MEA_MAX_NUM_OF_FILTER_ACTIONS      )  ) {
                *type_o = MEA_ACTION_TYPE_FILTER;
             } else {
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s - Unknown type (actionId_i=%d) \n",
                                   __FUNCTION__,
                                   actionId_i);
                 return MEA_ERROR;
             }
          }
       }
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <mea_Action_SetType>                                                   */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#if 0
MEA_Status mea_Action_SetType(MEA_Unit_t         unit_i,
                              mea_action_type_te type_i) 
{
    if (type_i >= MEA_ACTION_TYPE_LAST) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- type_i (%d) >= max value (%d) \n",
            __FUNCTION__,type_i,MEA_ACTION_TYPE_LAST);
        return MEA_ERROR;
    }

    MEA_Action_drv_type = type_i;

    return MEA_OK;
    

}
#endif

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_Create_Action>                                                */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_Action (MEA_Unit_t                              unit_i,
                                   MEA_Action_Entry_Data_dbt             *Action_data_pio,
                                   MEA_OutPorts_Entry_dbt                *Action_outPorts_pi,
                                   MEA_Policer_Entry_dbt                 *Action_policer_pi,
                                   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_editing_pi,
                                   MEA_Action_t                          *ActionId_io)
{


    
   // MEA_Uint32                           i;                                   
    MEA_Action_t                         ActionId;
   // MEA_Uint32*                          ptr;
   // MEA_Uint32                           mask;
   // MEA_Port_t                           outport;
    MEA_Action_HwEntry_dbt               hwEntry;
    mea_action_drv_dbt                   entry;
    mea_memory_mode_e                    save_globalMemoryMode;
    MEA_db_HwUnit_t                      hwUnit;
    MEA_db_HwId_t                        hwId;
    MEA_WRED_Profile_Key_dbt             wred_profile_key;
	char Action_type_str_vec[MEA_ACTION_TYPE_LAST+1][25];



	

	


    MEA_API_LOG

    MEA_OS_strcpy(&(Action_type_str_vec[MEA_ACTION_TYPE_FWD][0]), "TYPE_FWD");
	MEA_OS_strcpy(&(Action_type_str_vec[MEA_ACTION_TYPE_SERVICE][0]), "TYPE_SERVICE");
	MEA_OS_strcpy(&(Action_type_str_vec[MEA_ACTION_TYPE_FILTER][0]), "TYPE_FILTER");
	MEA_OS_strcpy(&(Action_type_str_vec[MEA_ACTION_TYPE_LXCP][0]), "LXCP");
    MEA_OS_strcpy(&(Action_type_str_vec[MEA_ACTION_TYPE_HPM][0]), "HPM");
	MEA_OS_strcpy(&(Action_type_str_vec[MEA_ACTION_TYPE_SERVICE_EXTERNAL][0]), "TYPE_SERVICE_EXTERNA");
    MEA_OS_strcpy(&(Action_type_str_vec[MEA_ACTION_TYPE_ACL5][0]), "TYPE_ACL5");
	MEA_OS_strcpy(&(Action_type_str_vec[MEA_ACTION_TYPE_LAST][0]), "TYPE_LAST");



#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (ActionId_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- ActionId_io == NULL\n",
                         __FUNCTION__);

       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Set Global Memory Mode */
    save_globalMemoryMode = globalMemoryMode;

     MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                        return MEA_ERROR);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
   /* Check for valid data parameter */
    if (Action_data_pio == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- Action_data_pio == NULL\n",
                         __FUNCTION__);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR; 
    }
	// check parameters for tdm
#ifdef MEA_TDM_SW_SUPPORT
	if (MEA_TDM_SUPPORT)
	{
		if((Action_data_pio->tdm_ces_type == MEA_TDM_CES_ENCP_TYPE_VLAN) && Action_data_pio->tdm_used_vlan == MEA_TRUE){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s \n- Tdm encapsulation vlan need to set the tdm_used_vlan to disable.\n",
				__FUNCTION__);
			MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR; 
		}
	}
#endif
	if (MEA_AFDX_SUPPORT)
	{
		if (Action_data_pio->afdx_enable == MEA_TRUE && 
			Action_data_pio->afdx_Rx_sessionId  >= MEA_AFDX_BAG_MAX_PROF) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s \n- data->afdx_Rx_sessionId (%d) >= max allowed (%d) \n",
					__FUNCTION__,
					Action_data_pio->afdx_Rx_sessionId,
					MEA_AFDX_BAG_MAX_PROF-1);
				MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
				return MEA_ERROR; 
		}
    

	}

   
    /* check valid data->pm_id  */
    if (Action_data_pio->pm_id_valid == MEA_TRUE && 
        Action_data_pio->pm_id  >= MEA_MAX_NUM_OF_PM_ID) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- data->pmId (%d) >= max allowed (%d) \n",
                         __FUNCTION__,
                         Action_data_pio->pm_id,
                         MEA_MAX_NUM_OF_PM_ID);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR; 
    }

    /* check valid data->pm_type  */
    if (Action_data_pio->pm_id_valid == MEA_TRUE) {
        switch (Action_data_pio->pm_type) {
        case MEA_PM_TYPE_ALL:
            break;
        
        default:
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s \n- unknown data->pm_type (%d) \n",
                             __FUNCTION__,
                             Action_data_pio->pm_type);
           MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
           return MEA_ERROR; 
        }
    }



 
   

    if (mea_drv_Get_DeviceInfo_NumOfPolicers(unit_i,hwUnit) <= 2) {
        if (Action_data_pio->tmId_disable != MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- No policer available in this direction\n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
        }
    }

    if (Action_data_pio->tm_id_valid == MEA_TRUE && 
        Action_data_pio->tm_id >= MEA_MAX_NUM_OF_TM_ID) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- data->tmId (%d) >= max allowed (%d) \n",
                         __FUNCTION__,
                         Action_data_pio->tm_id,
                         MEA_MAX_NUM_OF_TM_ID);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR; 
    }

    if (Action_data_pio->ed_id_valid == MEA_TRUE && 
        Action_data_pio->ed_id >= MEA_MAX_NUM_OF_EDIT_ID) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- data->editId (%d) >= max allowed (%d) \n",
                         __FUNCTION__,
                         Action_data_pio->ed_id,
                         MEA_MAX_NUM_OF_EDIT_ID);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR; 
    }

    if (Action_data_pio->flowCoSMappingProfile_force) {

        if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- flowCoSMappingProfile_force is not support \n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (Action_data_pio->force_cos_valid  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- flowCoSMappingProfile_force (%d) is not allowed "
                              " together with force_cos_valid (%d)\n",
                              __FUNCTION__,
                              Action_data_pio->flowCoSMappingProfile_force,
                              Action_data_pio->force_cos_valid);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (MEA_API_Get_FlowCoSMappingProfile_Entry
                                    (unit_i,
                                    (MEA_FlowCoSMappingProfile_Id_t)(Action_data_pio->flowCoSMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- Flow CoS Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              Action_data_pio->flowCoSMappingProfile_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }

    if (Action_data_pio->flowMarkingMappingProfile_force) {

        if (!MEA_FLOW_MARKING_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- flowMarkingMappingProfile_FORCE is not support \n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (Action_data_pio->force_l2_pri_valid  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- flowMarkingMappingProfile_FORCE (%d) is not allowed "
                              " together with force_l2_pri_valid (%d)\n",
                              __FUNCTION__,
                              Action_data_pio->flowMarkingMappingProfile_force,
                              Action_data_pio->force_l2_pri_valid);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }


        if (MEA_API_Get_FlowMarkingMappingProfile_Entry
                                   (unit_i,
                                    (MEA_FlowMarkingMappingProfile_Id_t)(Action_data_pio->flowMarkingMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- Flow Marking Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              Action_data_pio->flowMarkingMappingProfile_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }
    if(Action_data_pio->fwd_ingress_TS_type >= MEA_INGGRESS_TS_TYPE_LAST){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- ingress TS type (%d) is not support \n",
                              __FUNCTION__,Action_data_pio->fwd_ingress_TS_type);
    }
    
    if (Action_data_pio->overRule_vpValid == MEA_TRUE) {
        if (ENET_IsValid_Queue(unit_i,(ENET_QueueId_t)Action_data_pio->overRule_vp_val,MEA_FALSE) == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- overRule_vp_val  %d is not valid \n",
                __FUNCTION__, Action_data_pio->overRule_vp_val);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }


    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (*ActionId_io != MEA_PLAT_GENERATE_NEW_ID) {

        if (!MEA_API_IsValid_Action_Id(unit_i,*ActionId_io,Action_data_pio->Action_type) ) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s \n- Action Id %d is not valid for %s \n",
								   __FUNCTION__, *ActionId_io, Action_type_str_vec[Action_data_pio->Action_type] );
               MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
               return MEA_ERROR;
        }

        if (MEA_Action_drv_Table[*ActionId_io].valid) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s \n- Action Id %d already exist \n",
								   __FUNCTION__, *ActionId_io );
               MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
               return MEA_ERROR;
        }
        ActionId=*ActionId_io;
    } else {
		if (mea_action_drv_getFreeId(unit_i, &ActionId, Action_data_pio->Action_type) != MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s \n- mea_action_drv_getfreeId failed no free Action \n",
                               __FUNCTION__);          
              MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR;
       }
    }



    /* Create new empty entry locally */
    MEA_OS_memset(&entry,0,sizeof(entry));
    entry.num_of_owners = 1;
    entry.valid = MEA_TRUE;

    /* Copy the user data , but erase the tm/pm/ed that will be update later */
    MEA_OS_memcpy(&(entry.data),Action_data_pio,sizeof(entry.data));
    entry.data.pm_id_valid = MEA_FALSE;
    entry.data.pm_id       = 0;
    entry.data.tm_id_valid = MEA_FALSE;
    entry.data.tm_id       = 0;
    entry.data.ed_id_valid = MEA_FALSE;
    entry.data.ed_id       = 0;
    entry.data.policer_prof_id_valid = MEA_FALSE;
    entry.data.policer_prof_id = 0;
       
    

    if ((Action_data_pio->output_ports_valid) &&
        (Action_outPorts_pi != NULL)) {
       if(ENET_queue_check_OutPorts(unit_i,Action_outPorts_pi) !=ENET_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s \n- ENET_queue_check_OutPorts Fail \n",
                              __FUNCTION__);
             /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        
        }
        MEA_OS_memcpy(&(entry.out_ports),Action_outPorts_pi,sizeof(entry.out_ports));
    }   
    //check if the wred profile exist must before the create Action.
    {
        MEA_WRED_Profile_Key_dbt  key_i;
        key_i.acm_mode =0 ;
        key_i.profile_id = Action_data_pio->wred_profId ;


        if (MEA_API_IsValid_WRED_Profile   (unit_i,&key_i,MEA_TRUE)!=MEA_TRUE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsValid_WRED_Profile failed  wred profile %d not valid\n",
                __FUNCTION__,Action_data_pio->wred_profId);
            /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }



/*TEST check    */
    if(MEA_drv_action_pm_state_Setting(unit_i,
                                       ActionId,
                                       Action_data_pio,
                                       &entry.data,
                                       MEA_TRUE, /* Create */
                                       MEA_TRUE)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s \n- PM Test check failed mode create id %d\n",
                               __FUNCTION__,ActionId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
     if(MEA_drv_action_ed_state_Setting(unit_i,
                                       ActionId,
                                       Action_data_pio,
                                       &entry,
                                       Action_editing_pi,
                                       MEA_TRUE , /* Create */
                                       MEA_TRUE   /* Test */
                                      )!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s \n- ED Test check failed mode create id %d\n",
                               __FUNCTION__,
                               ActionId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
     if(MEA_drv_action_afdx_state_Setting(unit_i,
         ActionId,
         Action_data_pio,
         &entry.data,
         MEA_TRUE, /* Create */
         MEA_TRUE)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s \n- PM Test check failed mode create id %d\n",
                 __FUNCTION__,ActionId);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR;
     }


/* start to config*/    
    /* PM */ 
    if(MEA_drv_action_pm_state_Setting(unit_i,
                                       ActionId,
                                       Action_data_pio,
                                       &entry.data,
                                       MEA_TRUE, /* Create */
                                      MEA_FALSE)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s \n- MEA_drv_action_pm_state_Setting failed , action id %d\n",
                              __FUNCTION__,ActionId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* TM */ 
    if (mea_drv_Get_DeviceInfo_NumOfPolicers(unit_i,hwUnit) <= 2) {
        entry.data.tm_id_valid = MEA_FALSE;
        entry.data.tm_id       = 0;
    } else {
        if(MEA_drv_action_tm_state_Setting(unit_i,
                                           ActionId,
                                           Action_data_pio,
                                           &entry.data,
                                           Action_policer_pi,
                                           MEA_TRUE, /* Create */
                                           MEA_FALSE)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- MEA_drv_action_tm_state_Setting failed , action id %d\n",
                              __FUNCTION__,ActionId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }


    if (Action_editing_pi){
        if ((Action_editing_pi->ehp_info[0].ehp_data.Attribute_info.valid == MEA_FALSE) && 
            (Action_editing_pi->ehp_info[0].ehp_data.EditingType == 0)                       ){
            if (Action_data_pio->protocol_llc_force == MEA_TRUE){
                Action_editing_pi->ehp_info[0].ehp_data.Attribute_info.protocol = Action_data_pio->Protocol;
                Action_editing_pi->ehp_info[0].ehp_data.Attribute_info.llc = Action_data_pio->Llc;
                Action_editing_pi->ehp_info[0].ehp_data.Attribute_info.valid = MEA_TRUE;
            }
        }
    }



    /* ED */ 
    if(MEA_drv_action_ed_state_Setting(unit_i,
                                       ActionId,
                                       Action_data_pio,
                                       &entry,
                                       Action_editing_pi,
                                       MEA_TRUE , /* Create */
                                       MEA_FALSE /* no Test */
                                      )!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s \n- MEA_drv_action_ed_state_Setting failed , action id %d\n",
                              __FUNCTION__,ActionId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* afdx */ 
    if(MEA_drv_action_afdx_state_Setting(unit_i,
        ActionId,
        Action_data_pio,
        &entry.data,
        MEA_TRUE, /* Create */
        MEA_FALSE)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_drv_action_pm_state_Setting failed , action id %d\n",
                __FUNCTION__,ActionId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
    }



    /* Update WRED Owner */
    MEA_OS_memset(&wred_profile_key,0,sizeof(wred_profile_key));
    wred_profile_key.acm_mode   = 0;
    wred_profile_key.profile_id = entry.data.wred_profId;
    
    if (mea_drv_AddOwner_WRED_Profile(unit_i,&wred_profile_key) != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s \n- mea_drv_AddOwner_WRED_Profile failed (ActionId=%d , wred_profId=%d) \n",
                           __FUNCTION__,
                           ActionId,
                           entry.data.wred_profId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR; 
    }
   
    /*check if the  exist Action if yes we need to add owner if not we need to create new one */
    if ((*ActionId_io == MEA_PLAT_GENERATE_NEW_ID) &&
		(mea_action_drv_data_exist(unit_i, &entry, Action_data_pio->Action_type,&ActionId))) {
            
            if (entry.data.ed_id_valid) {
                mea_drv_ehp_id_dbt  ehp_id;
                MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
                ehp_id.is_mc = entry.EHP_MC;
                if (ehp_id.is_mc) {
                     ehp_id.id.mc_id.id  = entry.MC_ID;
                } else {
                    ehp_id.id.uni_id.id = entry.data.ed_id;
                }
                mea_drv_ehp_delete(unit_i,&ehp_id,&(entry.out_ports));
            }
            if (entry.data.tm_id_valid) {
                mea_drv_Delete_Policer(unit_i,entry.data.tm_id);
            }
            if (entry.data.pm_id_valid) {
                mea_drv_Delete_PM(unit_i,entry.data.pm_id);
            }
            if (MEA_AFDX_SUPPORT)
            {               
                if (entry.data.afdx_enable) {
                    mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i,(MEA_Uint16)entry.data.afdx_Rx_sessionId);
                }
            }
            mea_drv_DelOwner_WRED_Profile(unit_i,&wred_profile_key);
            
            if(MEA_API_AddOwner_Action(unit_i,ActionId) !=MEA_OK){
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s \n- MEA_API_AddOwner_Action failed (id=%d) \n",
                                 __FUNCTION__,ActionId);
                                  MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
               MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
               return MEA_ERROR;
            }
            
            /* Update Output parameters */
            Action_data_pio->tm_id_valid = entry.data.tm_id_valid;
            Action_data_pio->tm_id       = entry.data.tm_id;
            Action_data_pio->pm_id_valid = entry.data.pm_id_valid;
            Action_data_pio->pm_id       = entry.data.pm_id;
            Action_data_pio->ed_id_valid = entry.data.ed_id_valid;
            Action_data_pio->ed_id       = entry.data.ed_id;
            Action_data_pio->policer_prof_id_valid = entry.data.policer_prof_id_valid;
            Action_data_pio->policer_prof_id       = entry.data.policer_prof_id;
            *ActionId_io                 = ActionId;

            /* Return to caller */
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_OK;

    }


    /* Get the actionId */
    if (*ActionId_io == MEA_PLAT_GENERATE_NEW_ID) {
		if (mea_action_drv_getFreeId(unit_i, &ActionId, Action_data_pio->Action_type) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s \n- mea_action_drv_getfreeId failed no free Action \n",
                              __FUNCTION__);          
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
       }
    } else {
       ActionId = *ActionId_io;
    }

    /* Get HwId and HwUnit */
	switch (Action_data_pio->Action_type) {
    case MEA_ACTION_TYPE_SERVICE:
        MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
            mea_drv_Get_Service_Internal_db,
                                      ActionId,
                                      hwUnit,
                                      hwId,
                                      MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                                      return MEA_ERROR);
        break;
    case MEA_ACTION_TYPE_FILTER:
        hwId = (MEA_db_HwId_t)((ActionId));
        break;
    case MEA_ACTION_TYPE_FWD: 
        MEA_DRV_GET_HW_UNIT(unit_i,
                            hwUnit,
                            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                            return MEA_ERROR);
        if (ActionId ==    MEA_ACTION_ID_TO_CPU) {
            hwId = 0;
        } else {
            hwId = (MEA_db_HwId_t)((ActionId ));
        }    
        break;
    case MEA_ACTION_TYPE_HPM:
        hwId = (MEA_db_HwId_t)((ActionId));
        break;
    case MEA_ACTION_TYPE_SERVICE_EXTERNAL:
        hwId = (MEA_db_HwId_t)((ActionId));
        break;
    case MEA_ACTION_TYPE_ACL5:
        hwId = (MEA_db_HwId_t)((ActionId));
        break;

    case MEA_ACTION_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- Unknown action type (%d) \n",
						  __FUNCTION__, Action_data_pio->Action_type);
        if (entry.data.ed_id_valid) {
            mea_drv_ehp_id_dbt  ehp_id;
            MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
            ehp_id.is_mc = entry.EHP_MC;
            if (ehp_id.is_mc) {
                 ehp_id.id.mc_id.id  = entry.MC_ID;
            } else {
                ehp_id.id.uni_id.id = entry.data.ed_id;
            }
            mea_drv_ehp_delete(unit_i,&ehp_id,&(entry.out_ports));
        }
        if (entry.data.tm_id_valid) {
            mea_drv_Delete_Policer(unit_i,entry.data.tm_id);
        }
        if (entry.data.pm_id_valid) {
            mea_drv_Delete_PM(unit_i,entry.data.pm_id);
        }
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
        break;
    }



    /* Allocate HwId */
    if (mea_db_Create(unit_i,
                      MEA_Action_db,
                      (MEA_db_SwId_t)ActionId,
                      hwUnit,
                      &hwId) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- mea_db_Create failed (swId=%d,hwUnit=%d) \n",
                          __FUNCTION__,ActionId,hwUnit);
        if (entry.data.ed_id_valid) {
            mea_drv_ehp_id_dbt  ehp_id;
            MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
            ehp_id.is_mc = entry.EHP_MC;
            if (ehp_id.is_mc) {
                 ehp_id.id.mc_id.id  = entry.MC_ID;
            } else {
                ehp_id.id.uni_id.id = entry.data.ed_id;
            }
            mea_drv_ehp_delete(unit_i,&ehp_id,&(entry.out_ports));
        }
        if (entry.data.tm_id_valid) {
            mea_drv_Delete_Policer(unit_i,entry.data.tm_id);
        }
        if (entry.data.pm_id_valid) {
            mea_drv_Delete_PM(unit_i,entry.data.pm_id);
        }
        if (MEA_AFDX_SUPPORT)
        {               
            if (entry.data.afdx_enable) {
                mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i,(MEA_Uint16)entry.data.afdx_Rx_sessionId);
            }
        }
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }



    /*Write to HW */
    MEA_OS_memset(&hwEntry,0,sizeof(hwEntry));
    hwEntry.color        = Action_data_pio->COLOR;
    hwEntry.color_force  = Action_data_pio->force_color_valid;
    hwEntry.flowCoSMappingProfile_force = Action_data_pio->flowCoSMappingProfile_force;
    if (Action_data_pio->flowCoSMappingProfile_force) {
        hwEntry.cos          = Action_data_pio->flowCoSMappingProfile_id & 0x7;
        hwEntry.cos_force    = (Action_data_pio->flowCoSMappingProfile_id >> 3) & 0x1;
        hwEntry.color = (Action_data_pio->flowCoSMappingProfile_id >> 4) & 0x3;


    } else {
        hwEntry.cos          = Action_data_pio->COS;
        hwEntry.cos_force    = Action_data_pio->force_cos_valid;
    }
    hwEntry.flowMarkingMappingProfile_force = Action_data_pio->flowMarkingMappingProfile_force;
    if (Action_data_pio->flowMarkingMappingProfile_force) {
        hwEntry.l2_pri         = Action_data_pio->flowMarkingMappingProfile_id & 0x7;
        hwEntry.l2_pri_force   = (Action_data_pio->flowMarkingMappingProfile_id >> 3) & 0x1;
        hwEntry.color_force    = (Action_data_pio->flowMarkingMappingProfile_id >> 4) & 0x1;
        hwEntry.l2_pri_prof_b5 = (Action_data_pio->flowMarkingMappingProfile_id >> 5) & 0x1;

    } else {
        hwEntry.l2_pri       = Action_data_pio->L2_PRI;
        hwEntry.l2_pri_force = Action_data_pio->force_l2_pri_valid;
    }
    
    hwEntry.llc = Action_data_pio->Llc;
    hwEntry.protocol = Action_data_pio->Protocol;
    
    /*Update PROTOCOL LLC */
    if (Action_editing_pi != NULL && entry.data.ed_id_valid == MEA_TRUE){
        if (Action_editing_pi->ehp_info[0].ehp_data.EditingType !=0 ) {

            hwEntry.llc         = Action_data_pio->Llc         = mea_drv_ProtocolMachine_get_command((MEA_Uint16)Action_editing_pi->ehp_info[0].ehp_data.EditingType, MEA_MACHINE_PROTO_LLC);
            hwEntry.protocol    = Action_data_pio->Protocol    = mea_drv_ProtocolMachine_get_command((MEA_Uint16)Action_editing_pi->ehp_info[0].ehp_data.EditingType, MEA_MACHINE_PROTO_PROTO);
            entry.data.Llc      = Action_data_pio->Llc;
            entry.data.Protocol = Action_data_pio->Protocol;

            entry.data.proto_llc_valid = MEA_TRUE;
            entry.data.protocol_llc_force = MEA_TRUE;
            

        }
    }

    
    
    hwEntry.mc_fid_edit  = entry.EHP_MC ? entry.MC_ID : 0;
    hwEntry.mc_fid_skip  = !(entry.EHP_MC);

    hwEntry.ptmp         = entry.EHP_MC;
    hwEntry.ed_id        = (!entry.data.ed_id_valid) ? 0 : (MEA_Editing_t )entry.data.ed_id;
    
    hwEntry.pm_id        = (!entry.data.pm_id_valid) ? 0 : (MEA_PmId_t)    entry.data.pm_id;
    hwEntry.tm_id        = (!entry.data.tm_id_valid) ? 0 : (MEA_TmId_t)    entry.data.tm_id;
    hwEntry.fwd_ingress_TS_type = entry.data.fwd_ingress_TS_type;
    hwEntry.fragment_enable     = entry.data.fragment_enable;
#ifdef MEA_TDM_SW_SUPPORT
    hwEntry.tdm_packet_type     = entry.data.tdm_packet_type;
    hwEntry.tdm_remove_byte     = entry.data.tdm_remove_byte;
    hwEntry.tdm_remove_line     = entry.data.tdm_remove_line;
    hwEntry.tdm_ces_type        = entry.data.tdm_ces_type;   
    hwEntry.tdm_used_vlan       = entry.data.tdm_used_vlan;  
    hwEntry.tdm_used_rtp        = entry.data.tdm_used_rtp;
#endif
    hwEntry.wred_prof           = entry.data.wred_profId;
    hwEntry.set_1588            = entry.data.set_1588;
    hwEntry.afdx_enable         = entry.data.afdx_enable;
    hwEntry.afdx_Rx_sessionId       = entry.data.afdx_Rx_sessionId;
    hwEntry.afdx_A_B       = entry.data.afdx_A_B;
    hwEntry.mtu_id         = entry.data.mtu_Id;
    hwEntry.lag_bypass     = entry.data.lag_bypass;
    hwEntry.flood_ED       = entry.data.flood_ED;
    hwEntry.Drop_en        = entry.data.Drop_en;
	hwEntry.Action_type    = entry.data.Action_type;

    
    
    
    if (entry.data.overRule_vpValid == MEA_TRUE) {
        hwEntry.overRule_vpValid = entry.data.overRule_vpValid;
        hwEntry.overRule_vp_val = enet_cluster_external2internal((ENET_QueueId_t) entry.data.overRule_vp_val);
    }
    
   

    hwEntry.Fragment_IP = entry.data.Fragment_IP;
    hwEntry.IP_TUNNEL = entry.data.IP_TUNNEL;
    hwEntry.Defrag_ext_IP = entry.data.Defrag_ext_IP;
    hwEntry.Defrag_int_IP = entry.data.Defrag_int_IP;

    hwEntry.Defrag_Reassembly_L2_profile = entry.data.Defrag_Reassembly_L2_profile;
    hwEntry.fragment_ext_int             = entry.data.fragment_ext_int;
    hwEntry.fragment_target_mtu_prof     = entry.data.fragment_target_mtu_prof;
    hwEntry.fwd_win                      = entry.data.fwd_win;
    hwEntry.sflow_Type                   = entry.data.sflow_Type;
    hwEntry.overRule_bfd_sp              = entry.data.overRule_bfd_sp;

   
    




#if 1
    /************************************************************************/
    /* check the the MTU on flow and                                        */
    /************************************************************************/
    {
        MEA_AC_Mtu_dbt           mtu_entry_po;
        MEA_Bool                 mtu_exist = MEA_FALSE;
        MEA_Status               retVal = MEA_OK;
        MEA_Uint32               mtu_size;
        if (entry.data.tm_id_valid){
            /*get the MTU flow info */
            if (MEA_ACT_MTU_SUPPORT){
                retVal = MEA_API_Get_Flow_MTU_Prof(unit_i, entry.data.mtu_Id, &mtu_entry_po, &mtu_exist);
                if (retVal != MEA_OK)
                    return MEA_ERROR;
            }
            else{
                mtu_exist = MEA_Platform_GetMaxMTU();
            }

            if (mtu_exist && Action_policer_pi != NULL){

                if (Action_policer_pi->maxMtu == 0){
                    mtu_size = MEA_Platform_GetMaxMTU();
                }
                else{
                    mtu_size = Action_policer_pi->maxMtu;
                }

                if (mtu_size < mtu_entry_po.mtu_size){
                   // MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " WARNING! The Policer MTU %d < Flow MTU %d \r\n", Action_policer_pi->maxMtu, mtu_entry_po.mtu_size);
                }
            }

        }
    }
#endif

    hwEntry.valid        = MEA_TRUE;

    if ( mea_Action_UpdateHw(unit_i,ActionId, &hwEntry) != MEA_OK ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- mea_Action_UpdateHw failed \n",
                          __FUNCTION__);
        if (entry.data.ed_id_valid) {
            mea_drv_ehp_id_dbt  ehp_id;
            MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
            ehp_id.is_mc = entry.EHP_MC;
            if (ehp_id.is_mc) {
                 ehp_id.id.mc_id.id  = entry.MC_ID;
            } else {
                ehp_id.id.uni_id.id = entry.data.ed_id;
            }
            mea_drv_ehp_delete(unit_i,&ehp_id,&(entry.out_ports));
        }
        if (entry.data.tm_id_valid) {
            mea_drv_Delete_Policer(unit_i,entry.data.tm_id);
        }
        if (entry.data.pm_id_valid) {
            mea_drv_Delete_PM(unit_i,entry.data.pm_id);
        }
        /*need to delete the afdx*/
        if (MEA_AFDX_SUPPORT)
        {               
            if (entry.data.afdx_enable) {
                mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i,(MEA_Uint16)entry.data.afdx_Rx_sessionId);
            }
        }
        mea_db_Delete(unit_i,MEA_Action_db,(MEA_db_SwId_t)ActionId,hwUnit);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    /* Update SW Shadow */
    MEA_OS_memcpy(&MEA_Action_drv_Table[ActionId],
                  &entry,
                  sizeof(MEA_Action_drv_Table[0]));
     

    /* Update Output parameters */
    Action_data_pio->tm_id_valid = entry.data.tm_id_valid;
    Action_data_pio->tm_id       = entry.data.tm_id;
    Action_data_pio->pm_id_valid = entry.data.pm_id_valid;
    Action_data_pio->pm_id       = entry.data.pm_id;
    Action_data_pio->ed_id_valid = entry.data.ed_id_valid;
    Action_data_pio->ed_id       = entry.data.ed_id;
    Action_data_pio->policer_prof_id_valid = entry.data.policer_prof_id_valid;
    Action_data_pio->policer_prof_id       = entry.data.policer_prof_id;
    *ActionId_io                 = ActionId;


    /* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}










/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_DRV_Delete_Action>                                                */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status  MEA_DRV_Delete_Action(MEA_Unit_t    unit_i, MEA_Action_t  ActionId_i, mea_action_type_te Action_type)
{
    MEA_Bool exist;
    MEA_Action_HwEntry_dbt action_info;
    mea_memory_mode_e      save_globalMemoryMode;
    MEA_db_HwUnit_t        hwUnit;
    MEA_WRED_Profile_Key_dbt             wred_profile_key;


    MEA_API_LOG
 

    /* Set Global Memory Mode */
    save_globalMemoryMode = globalMemoryMode;


    if(MEA_API_IsExist_Action(unit_i,ActionId_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n-MEA_API_IsExist_Action %d failed \n",
                         __FUNCTION__,ActionId_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    if (exist == MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- ActionId %d not exist \n",
                         __FUNCTION__,ActionId_i);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR; 
    }
  
    /* Check if we just need to delete the owner */
    if ((MEA_Action_drv_Table[ActionId_i].num_of_owners > 1) &&
        (ActionId_i != MEA_ACTION_ID_TO_CPU) && 
        ActionId_i  != MEA_ACTION_ID_TO_FWD_CPU) {

        if (MEA_API_DelOwner_Action(unit_i,ActionId_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- MEA_API_DelOwner failed (actionId %d) \n",
                            __FUNCTION__,ActionId_i);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
           MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_OK;
    }

    /* Check if this is the last owner */
    if (MEA_Action_drv_Table[ActionId_i].num_of_owners > 1) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- Not allowed to delete actionId %d (num_of_owners=%d)\n",
            __FUNCTION__,ActionId_i,MEA_Action_drv_Table[ActionId_i].num_of_owners);
         MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    /* disconnect from the ED ID, TM ID, PM ID */
    if (MEA_Action_drv_Table[ActionId_i].data.ed_id_valid) {
        mea_drv_ehp_id_dbt                   ehp_id;
        MEA_OS_memset(&ehp_id,0,sizeof(ehp_id));
        ehp_id.is_mc = MEA_Action_drv_Table[ActionId_i].EHP_MC;
        if (ehp_id.is_mc) {
             ehp_id.id.mc_id.id = MEA_Action_drv_Table[ActionId_i].MC_ID;
        } else {
            ehp_id.id.uni_id.id = (MEA_Editing_t)MEA_Action_drv_Table[ActionId_i].data.ed_id;
        }
        if (mea_drv_ehp_delete(unit_i,
                                 &ehp_id,
                               &(MEA_Action_drv_Table[ActionId_i].out_ports)) != MEA_OK) {
          MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_ehp_delete failed (id=%d/%d) \n",
                             __FUNCTION__,
                             ehp_id.is_mc,
                             ehp_id.is_mc ? ehp_id.id.mc_id.id : ehp_id.id.uni_id.id);
          MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
          return MEA_ERROR; 
        }
        MEA_Action_drv_Table[ActionId_i].EHP_MC           = MEA_FALSE;
        MEA_Action_drv_Table[ActionId_i].MC_ID            = 0;
        MEA_Action_drv_Table[ActionId_i].data.ed_id       = 0;
        MEA_Action_drv_Table[ActionId_i].data.ed_id_valid = MEA_FALSE;
    }

    if ((MEA_Action_drv_Table[ActionId_i].data.tm_id_valid) &&
        (mea_drv_Delete_Policer(unit_i,(MEA_TmId_t)MEA_Action_drv_Table[ActionId_i].data.tm_id) 
                                 != MEA_OK )) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_Delete_Policer failed (ActionId=%d , tmId=%d) \n",
                           __FUNCTION__,
                             ActionId_i,
                           MEA_Action_drv_Table[ActionId_i].data.tm_id);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR; 
    }

    if ((MEA_Action_drv_Table[ActionId_i].data.pm_id_valid) &&
        ( mea_drv_Delete_PM(unit_i, 
                            (MEA_PmId_t)MEA_Action_drv_Table[ActionId_i].data.pm_id) 
                            != MEA_OK )) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                             "%s \n- mea_drv_Delete_PM failed (ActionId=%d , pmId=%d) \n",
                           __FUNCTION__,
                           ActionId_i,
                           MEA_Action_drv_Table[ActionId_i].data.pm_id);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR; 
    }

    if (MEA_AFDX_SUPPORT)
   {
       if ((MEA_Action_drv_Table[ActionId_i].data.afdx_enable) &&
           ( mea_drv_Rx_AfdxBag_prof_delete_owner(unit_i, 
           (MEA_Uint16)MEA_Action_drv_Table[ActionId_i].data.afdx_Rx_sessionId) 
           != MEA_OK )) {
               MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                   "%s \n- mea_drv_Rx_AfdxBag_prof_delete_owner failed (ActionId=%d , pmId=%d) \n",
                   __FUNCTION__,
                   ActionId_i,
                   MEA_Action_drv_Table[ActionId_i].data.afdx_Rx_sessionId);
               MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
               return MEA_ERROR; 
       }

  }




    /* Update WRED Owner */
    MEA_OS_memset(&wred_profile_key,0,sizeof(wred_profile_key));
    wred_profile_key.acm_mode   = 0;
    wred_profile_key.profile_id = MEA_Action_drv_Table[ActionId_i].data.wred_profId;
    
    if (mea_drv_DelOwner_WRED_Profile(unit_i,&wred_profile_key) != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s \n- mea_drv_DelOwner_WRED_Profile failed (ActionId=%d , wred_profId=%d) \n",
                           __FUNCTION__,
                           ActionId_i,
                           MEA_Action_drv_Table[ActionId_i].data.wred_profId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR; 
    }
    
    
    /* Update Hardware */
    MEA_OS_memset(&action_info,0,sizeof(action_info));
    action_info.Action_type = Action_type;
    action_info.Drop_en = MEA_TRUE; 
    if ( mea_Action_UpdateHw(unit_i,ActionId_i  , &action_info) != MEA_OK ) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- mea_Action_UpdateHw failed, id %d\n",
                           __FUNCTION__,ActionId_i);
         MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
         return MEA_ERROR;
    }
    
    /* Update SW Shadow */
    MEA_Action_drv_Table[ActionId_i].valid = MEA_FALSE;
        
    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                        return MEA_ERROR);

    /* Delete the HwId */
    if (mea_db_Delete(unit_i,
                      MEA_Action_db,
                      (MEA_db_SwId_t)ActionId_i,
                      hwUnit) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- mea_db_Delete failed (serviceId=%d,hwUnit=$d)\n",
                          __FUNCTION__,ActionId_i,hwUnit);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    /* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}



MEA_Status  MEA_API_Delete_Action_HPM(MEA_Unit_t    unit_i, MEA_Action_t  ActionId_i, mea_action_type_te Action_type)
{
    
    
   
    MEA_Action_t  Action_Id_start;
    MEA_Action_t  Action_Id_end;


    MEA_API_LOG

        if (Action_type == MEA_ACTION_TYPE_HPM)
        {
            if (ActionId_i == MEA_ACTION_ID_TO_CPU) {
                Action_Id_start = 0;
                Action_Id_end = 0;
            }
            else {
                Action_Id_start = MEA_Platform_Get_HPM_BASE();
                Action_Id_end = MEA_Platform_Get_HPM_BASE() + MEA_MAX_NUM_OF_HPM_ACTIONS;
            }
            if (ActionId_i < Action_Id_start || ActionId_i >= Action_Id_end) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n-  Not allowed Action_data_pio = NULL\n",
                    __FUNCTION__);
                return MEA_ERROR;

            }

            return MEA_DRV_Delete_Action(unit_i, ActionId_i, Action_type);

        }
        else {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n-  Not allowed Action_type %d \n",
                __FUNCTION__, Action_type);
            return MEA_ERROR;
        }


    

     
}



MEA_Status  MEA_API_Delete_Action(MEA_Unit_t    unit_i,
	                              MEA_Action_t  ActionId_i)
{

	mea_action_type_te Action_type = MEA_ACTION_TYPE_FWD;
	MEA_Action_t  Action_Id_start;
	MEA_Action_t  Action_Id_end;
    MEA_Bool exist=MEA_FALSE;
    MEA_Status ret = MEA_ERROR;




	if (Action_type == MEA_ACTION_TYPE_FWD)
	{
		if (ActionId_i == MEA_ACTION_ID_TO_CPU){
			Action_Id_start = 0;
		    Action_Id_end   = 0;
		}
		else{
			Action_Id_start = ENET_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_START(0);
			Action_Id_end = ENET_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_START(0) + ENET_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_LENGTH(0);
		}
		if (ActionId_i < Action_Id_start || ActionId_i >=Action_Id_end ){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s \n-  Not allowed Action_data_pio = NULL\n",
				__FUNCTION__);
			return MEA_ERROR;

		}
        if (MEA_API_IsExist_Action(unit_i, ActionId_i, &exist) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n-MEA_API_IsExist_Action %d failed \n",
                __FUNCTION__, ActionId_i);
          
            return MEA_ERROR;
        }
        if (exist == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- ActionId %d not exist \n",
                __FUNCTION__, ActionId_i);
            return MEA_ERROR;
        }

//         if (MEA_Action_drv_Table[ActionId_i].num_of_owners > 1) {
//             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
//                 "%s \n- Not allowed to delete actionId %d (num_of_owners=%d)\n",
//                 __FUNCTION__, ActionId_i, MEA_Action_drv_Table[ActionId_i].num_of_owners);
//             return MEA_ERROR;
//         }

        ret= MEA_DRV_Delete_Action(unit_i, ActionId_i, Action_type);

	}
   
   

    return ret;

}


/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_Set_Action>                                                   */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status  MEA_API_Set_Action    (MEA_Unit_t                             unit_i,
                                    MEA_Action_t                          Action_id_i,
                                    MEA_Action_Entry_Data_dbt            *Action_data_pio,
                                    MEA_OutPorts_Entry_dbt               *Action_outPorts_pi,
                                    MEA_Policer_Entry_dbt                *Action_policer_pi,
                                    MEA_EgressHeaderProc_Array_Entry_dbt *Action_editing_pi) {

    
    MEA_Action_HwEntry_dbt hwEntry;
    mea_action_drv_dbt     entry;
    MEA_Bool               exist;
    MEA_db_HwUnit_t        hwUnit;
    mea_memory_mode_e                    save_globalMemoryMode;
    MEA_WRED_Profile_Key_dbt wred_profile_key;

    MEA_API_LOG


    /* Set Global Memory Mode */
    save_globalMemoryMode = globalMemoryMode;


    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                        return MEA_ERROR);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (Action_data_pio == NULL){  
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
       "%s \n-  Not allowed Action_data_pio = NULL\n",
                         __FUNCTION__);    
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    // check parameters for tdm
#ifdef MEA_TDM_SW_SUPPORT
    if (MEA_TDM_SUPPORT)
    {
        if((Action_data_pio->tdm_ces_type == MEA_TDM_CES_ENCP_TYPE_VLAN) && Action_data_pio->tdm_used_vlan == MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- Tdm encapsulation vlan need to set the tdm_used_vlan to disable.\n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
        }
    }
#endif
    /* check valid data->pm_id */
    if ( (Action_data_pio->pm_id_valid == MEA_TRUE            ) && 
         (Action_data_pio->pm_id       >= MEA_MAX_NUM_OF_PM_ID)  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- data->pmId (%d) >= max allowed (%d) \n",
                              __FUNCTION__,
                              Action_data_pio->pm_id,
                              MEA_MAX_NUM_OF_PM_ID);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
    }

    if(Action_outPorts_pi != NULL) {
       if(ENET_queue_check_OutPorts(unit_i,Action_outPorts_pi) !=ENET_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s \n- ENET_queue_check_OutPorts Fail \n",
                              __FUNCTION__);
             /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        
        }
    }
    /* check valid data->pm_type  */
    if (Action_data_pio->pm_id_valid == MEA_TRUE) {
        switch (Action_data_pio->pm_type) {
        case MEA_PM_TYPE_ALL:
            break;
#if 0
        case MEA_PM_TYPE_PACKET_ONLY:
            if (!MEA_Platform_EnhancePmCounterSupport()) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s \n- data->pm_type (%d) not support \n",
                                  __FUNCTION__,
                                  Action_data_pio->pm_type);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
            break;
#endif        
        default:
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s \n- unknown data->pm_type (%d) \n",
                             __FUNCTION__,
                             Action_data_pio->pm_type);
           MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
           return MEA_ERROR; 
        }
    }

    if (mea_drv_Get_DeviceInfo_NumOfPolicers(unit_i,hwUnit) <= 2) {
        if (Action_data_pio->tmId_disable != MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- No policers available in this direction\n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
        }
    }

    /* check valid data->tm_id */
    if ((Action_data_pio->tm_id_valid == MEA_TRUE             ) && 
        (Action_data_pio->tm_id       >=  MEA_MAX_NUM_OF_TM_ID)  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- data->tmId (%d) >= max allowed (%d) \n",
                         __FUNCTION__,
                         Action_data_pio->tm_id,
                         MEA_MAX_NUM_OF_TM_ID);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR; 
    }

    if ((Action_data_pio->policer_prof_id_valid == MEA_TRUE             ) && 
        (Action_data_pio->policer_prof_id       >=  MEA_POLICER_MAX_PROF)  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- data->policer_prof_id (%d) >= max allowed (%d) \n",
                __FUNCTION__,
                Action_data_pio->policer_prof_id,
                MEA_POLICER_MAX_PROF);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
    }




    /* check valid data->ed_id */
    if ((Action_data_pio->ed_id_valid == MEA_TRUE               ) && 
        (Action_data_pio->ed_id       >=  MEA_MAX_NUM_OF_EDIT_ID)  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s \n- data->edId (%d) >= max allowed (%d) \n",
                         __FUNCTION__,
                         Action_data_pio->ed_id,
                         MEA_MAX_NUM_OF_EDIT_ID);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR; 
    }
    if (Action_data_pio->flowCoSMappingProfile_force) {

        if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- flowCoSMappingProfile_force is not support \n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (Action_data_pio->force_cos_valid  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- flowCoSMappingProfile_force (%d) is not allowed "
                              " together with force_cos_valid (%d)\n",
                              __FUNCTION__,
                              Action_data_pio->flowCoSMappingProfile_force,
                              Action_data_pio->force_cos_valid);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (MEA_API_Get_FlowCoSMappingProfile_Entry
                                    (unit_i,
                                    (MEA_FlowCoSMappingProfile_Id_t)(Action_data_pio->flowCoSMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- Flow CoS Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              Action_data_pio->flowCoSMappingProfile_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }

    if (Action_data_pio->flowMarkingMappingProfile_force) {

        if (!MEA_FLOW_MARKING_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- flowMarkingMappingProfile_FORCE is not support \n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (Action_data_pio->force_l2_pri_valid  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- flowMarkingMappingProfile_FORCE (%d) is not allowed "
                              " together with force_l2_pri_valid (%d)\n",
                              __FUNCTION__,
                              Action_data_pio->flowMarkingMappingProfile_force,
                              Action_data_pio->force_l2_pri_valid);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }


        if (MEA_API_Get_FlowMarkingMappingProfile_Entry
                                   (unit_i,
                                    (MEA_FlowMarkingMappingProfile_Id_t)(Action_data_pio->flowMarkingMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- Flow Marking Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              Action_data_pio->flowMarkingMappingProfile_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }
    if(Action_data_pio->fwd_ingress_TS_type >= MEA_INGGRESS_TS_TYPE_LAST){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s \n- ingress TS type (%d) is not support \n",
                              __FUNCTION__,Action_data_pio->fwd_ingress_TS_type);
    }

    if (Action_data_pio->overRule_vpValid == MEA_TRUE) {
        if (ENET_IsValid_Queue(unit_i,(ENET_QueueId_t)Action_data_pio->overRule_vp_val, MEA_FALSE) == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- overRule_vp_val  %d is not valid \n",
                __FUNCTION__, Action_data_pio->overRule_vp_val);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }


    }


#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
        

    /*Check if the Id is valid*/
    if (MEA_API_IsExist_Action(unit_i,Action_id_i,&exist)!=MEA_OK ){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - MEA_API_IsExist_Action failed, id %d\n",
                           __FUNCTION__,Action_id_i);
         MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
         return MEA_ERROR;
    }
    if(!exist){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - Action id %d not exist\n",
                           __FUNCTION__,Action_id_i);
         MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
         return MEA_ERROR;
    }

    /* Copy current to local variable , and work on it */
    MEA_OS_memcpy(&entry,
                   &MEA_Action_drv_Table[Action_id_i],
                  sizeof(entry));
    //check if the wred profile exist must before the create Action.
    {
        MEA_WRED_Profile_Key_dbt  key_i;
        key_i.acm_mode =0 ;
        key_i.profile_id = Action_data_pio->wred_profId ;


        if (MEA_API_IsValid_WRED_Profile   (unit_i,&key_i,MEA_TRUE)!=MEA_TRUE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsValid_WRED_Profile failed  wred profile %d not valid\n",
                __FUNCTION__,Action_data_pio->wred_profId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }


/*start TEST mode*/
     if(MEA_drv_action_pm_state_Setting(unit_i,
                                       Action_id_i,
                                       Action_data_pio,
                                       &entry.data,
                                       MEA_FALSE, /* Create(MEA_TRUE)/Set(MEA_FALSE) */
                                       MEA_TRUE)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               " - PM Test check failed mode set id %d\n",Action_id_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
#if 0
     if(MEA_drv_action_tm_state_Setting(unit_i,
                                       Action_id_i,
                                       Action_data_pio,
                                       &entry.data,
                                       Action_policer_pi,
                                       MEA_FALSE, /* Create(MEA_TRUE)/Set(MEA_FALSE) */
                                       MEA_TRUE)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "% - TM Test chack failed mode set id %d\n",Action_id_i);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR;
    }
#endif
    if(MEA_drv_action_ed_state_Setting(unit_i,
                                       Action_id_i,
                                       Action_data_pio,
                                       &entry,
                                       Action_editing_pi,
                                       MEA_FALSE, /* Create(MEA_TRUE)/Set(MEA_FALSE) */
                                       MEA_TRUE /* test (MEA_TRUE)/ no test(MEA_FALSE) */
                                      )!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               " ED Test check fail mode set id = %d \n",                               
                              Action_id_i);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR;
    }
    
    if(MEA_drv_action_afdx_state_Setting(unit_i,
        Action_id_i,
        Action_data_pio,
        &entry.data,
        MEA_FALSE, /* Create(MEA_TRUE)/Set(MEA_FALSE) */
        MEA_TRUE)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                " - afdx Test check failed mode set id %d\n",Action_id_i);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
    }
    

/* end test mode */
    /* Update PM */
    if(MEA_drv_action_pm_state_Setting(unit_i,
                                       Action_id_i,
                                       Action_data_pio,
                                       &entry.data,
                                       MEA_FALSE, /* Create(MEA_TRUE)/Set(MEA_FALSE) */
                                       MEA_FALSE)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - MEA_drv_Action_pm_state_Setting failed (id=%d)\n",
                              __FUNCTION__,
                              Action_id_i);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR;
    }

    /* Update TM */
    if (mea_drv_Get_DeviceInfo_NumOfPolicers(unit_i,hwUnit) <= 2) {
        entry.data.tm_id_valid = MEA_FALSE;
        entry.data.tm_id       = 0;
    } else {
        if(MEA_drv_action_tm_state_Setting(unit_i,
                                       Action_id_i,
                                       Action_data_pio,
                                       &entry.data,
                                       Action_policer_pi,
                                       MEA_FALSE ,/* Create(MEA_TRUE)/Set(MEA_FALSE) */
                                       MEA_FALSE)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s \n- MEA_drv_Action_tm_state_Setting failed (id=%d)\n",
                              __FUNCTION__,
                              Action_id_i);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR;
        }
    }


    /* Update ED */
    if(MEA_drv_action_ed_state_Setting(unit_i,
                                       Action_id_i,
                                       Action_data_pio,
                                       &entry,
                                       Action_editing_pi,
                                       MEA_FALSE, /* Create(MEA_TRUE)/Set(MEA_FALSE) */
                                       MEA_FALSE /* test (MEA_TRUE)/no test(MEA_FALSE) */                                       
                                      )!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s \n- MEA_drv_Action_ed_state_Setting failed (id=%d)\n",
                              __FUNCTION__,
                              Action_id_i);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR;
    }

    /* Update afdx */
    if(MEA_drv_action_afdx_state_Setting(unit_i,
        Action_id_i,
        Action_data_pio,
        &entry.data,
        MEA_FALSE, /* Create(MEA_TRUE)/Set(MEA_FALSE) */
        MEA_FALSE)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_drv_action_afdx_state_Setting failed (id=%d)\n",
                __FUNCTION__,
                Action_id_i);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
    }

    /* Update WRED Owner */
    MEA_OS_memset(&wred_profile_key,0,sizeof(wred_profile_key));
    wred_profile_key.acm_mode   = 0;
    wred_profile_key.profile_id = MEA_Action_drv_Table[Action_id_i].data.wred_profId;
    
    if (mea_drv_DelOwner_WRED_Profile(unit_i,&wred_profile_key) != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s \n- mea_drv_DelOwner_WRED_Profile failed (ActionId=%d , wred_profId=%d) \n",
                           __FUNCTION__,
                           Action_id_i,
                           MEA_Action_drv_Table[Action_id_i].data.wred_profId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR; 
    }
    
    MEA_OS_memset(&wred_profile_key,0,sizeof(wred_profile_key));
    wred_profile_key.acm_mode   = 0;
    wred_profile_key.profile_id = Action_data_pio->wred_profId;
    
    if (mea_drv_AddOwner_WRED_Profile(unit_i,&wred_profile_key) != MEA_OK) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s \n- mea_drv_AddOwner_WRED_Profile failed (ActionId=%d , wred_profId=%d) \n",
                           __FUNCTION__,
                           Action_id_i,
                           Action_data_pio->wred_profId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR; 
    }
   

    /*Write to HW */
    MEA_OS_memset(&hwEntry,0,sizeof(hwEntry));
    hwEntry.color        = Action_data_pio->COLOR;
    hwEntry.color_force  = Action_data_pio->force_color_valid;
    hwEntry.flowCoSMappingProfile_force = Action_data_pio->flowCoSMappingProfile_force;
    if (Action_data_pio->flowCoSMappingProfile_force) {
        hwEntry.cos = Action_data_pio->flowCoSMappingProfile_id & 0x7;
        hwEntry.cos_force = (Action_data_pio->flowCoSMappingProfile_id >> 3) & 0x1;
        hwEntry.color = (Action_data_pio->flowCoSMappingProfile_id >> 4) & 0x3;
    } else {
        hwEntry.cos          = Action_data_pio->COS;
        hwEntry.cos_force    = Action_data_pio->force_cos_valid;
    }
    hwEntry.flowMarkingMappingProfile_force = Action_data_pio->flowMarkingMappingProfile_force;
    if (Action_data_pio->flowMarkingMappingProfile_force) {
        hwEntry.l2_pri = Action_data_pio->flowMarkingMappingProfile_id & 0x7;
        hwEntry.l2_pri_force   = (Action_data_pio->flowMarkingMappingProfile_id >> 3)   & 0x1;
        hwEntry.color_force    = (Action_data_pio->flowMarkingMappingProfile_id >> 4)    & 0x1;
        hwEntry.l2_pri_prof_b5 = (Action_data_pio->flowMarkingMappingProfile_id >> 5) & 0x1;
    } else {
        hwEntry.l2_pri       = Action_data_pio->L2_PRI;
        hwEntry.l2_pri_force = Action_data_pio->force_l2_pri_valid;
    }
    /*Update PROTOCOL LLC */

    hwEntry.llc          = Action_data_pio->Llc;
    hwEntry.protocol     = Action_data_pio->Protocol;

    if (Action_editing_pi != NULL && entry.data.ed_id_valid == MEA_TRUE){
        if (Action_editing_pi->ehp_info[0].ehp_data.EditingType != 0 ) {

            hwEntry.llc = Action_data_pio->Llc = mea_drv_ProtocolMachine_get_command((MEA_Uint16)Action_editing_pi->ehp_info[0].ehp_data.EditingType, MEA_MACHINE_PROTO_LLC);
            hwEntry.protocol = Action_data_pio->Protocol = mea_drv_ProtocolMachine_get_command((MEA_Uint16)Action_editing_pi->ehp_info[0].ehp_data.EditingType, MEA_MACHINE_PROTO_PROTO);
            entry.data.Llc = Action_data_pio->Llc;
            entry.data.Protocol = Action_data_pio->Protocol;

            entry.data.proto_llc_valid = MEA_TRUE;
            entry.data.protocol_llc_force = MEA_TRUE;


        }
    }




    hwEntry.mc_fid_edit  = entry.EHP_MC ? entry.MC_ID : 0;
    hwEntry.mc_fid_skip  = !(entry.EHP_MC);
    hwEntry.ptmp         = entry.EHP_MC;
    hwEntry.ed_id        = (!entry.data.ed_id_valid) ? 0 : (MEA_Editing_t )entry.data.ed_id;
    hwEntry.pm_id        = (!entry.data.pm_id_valid) ? 0 : (MEA_PmId_t)    entry.data.pm_id;
    hwEntry.tm_id        = (!entry.data.tm_id_valid) ? 0 : (MEA_TmId_t)    entry.data.tm_id;





    hwEntry.fwd_ingress_TS_type = Action_data_pio->fwd_ingress_TS_type;
    hwEntry.fragment_enable     = Action_data_pio->fragment_enable;
#ifdef MEA_TDM_SW_SUPPORT
    hwEntry.tdm_packet_type     = Action_data_pio->tdm_packet_type;
    hwEntry.tdm_remove_byte     = Action_data_pio->tdm_remove_byte;
    hwEntry.tdm_remove_line     = Action_data_pio->tdm_remove_line;
    hwEntry.tdm_ces_type        = Action_data_pio->tdm_ces_type;   
    hwEntry.tdm_used_vlan       = Action_data_pio->tdm_used_vlan;  
    hwEntry.tdm_used_rtp        = Action_data_pio->tdm_used_rtp;
#endif
    hwEntry.wred_prof           = Action_data_pio->wred_profId;
    hwEntry.set_1588            = Action_data_pio->set_1588;

    hwEntry.afdx_enable         = Action_data_pio->afdx_enable;
    hwEntry.afdx_Rx_sessionId   = Action_data_pio->afdx_Rx_sessionId;
    hwEntry.afdx_A_B            = Action_data_pio->afdx_A_B;
    hwEntry.mtu_id              = Action_data_pio->mtu_Id;
    hwEntry.lag_bypass          = Action_data_pio->lag_bypass;
    hwEntry.flood_ED            = Action_data_pio->flood_ED;
    hwEntry.Drop_en             = Action_data_pio->Drop_en;
	hwEntry.Action_type         = Action_data_pio->Action_type;


    if (Action_data_pio->overRule_vpValid == MEA_TRUE) {
        hwEntry.overRule_vpValid = Action_data_pio->overRule_vpValid;
        hwEntry.overRule_vp_val = enet_cluster_external2internal((ENET_QueueId_t)Action_data_pio->overRule_vp_val);
    }
   
    

    hwEntry.Fragment_IP = Action_data_pio->Fragment_IP;
    hwEntry.IP_TUNNEL = Action_data_pio->IP_TUNNEL;
    hwEntry.Defrag_ext_IP = Action_data_pio->Defrag_ext_IP;
    hwEntry.Defrag_int_IP = Action_data_pio->Defrag_int_IP;

    hwEntry.Defrag_Reassembly_L2_profile = Action_data_pio->Defrag_Reassembly_L2_profile;

    hwEntry.fragment_ext_int            = Action_data_pio->fragment_ext_int;
    hwEntry.fragment_target_mtu_prof    = Action_data_pio->fragment_target_mtu_prof;
    hwEntry.fwd_win                     = entry.data.fwd_win;
    hwEntry.sflow_Type = entry.data.sflow_Type;

    hwEntry.valid        = MEA_TRUE;

    if ( mea_Action_UpdateHw(unit_i,Action_id_i, &hwEntry) != MEA_OK ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- mea_Action_UpdateHw failed \n",
                          __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* Update Output parameters */
    /* Note: This need to be done before copy the original other data attributes for this action */
    Action_data_pio->tm_id_valid = entry.data.tm_id_valid;
    Action_data_pio->tm_id       = entry.data.tm_id;
    Action_data_pio->pm_id_valid = entry.data.pm_id_valid;
    Action_data_pio->pm_id       = entry.data.pm_id;
    Action_data_pio->ed_id_valid = entry.data.ed_id_valid;
    Action_data_pio->ed_id       = entry.data.ed_id;
    Action_data_pio->policer_prof_id_valid = entry.data.policer_prof_id_valid;
    Action_data_pio->policer_prof_id       = entry.data.policer_prof_id;

    /* Copy the new user definition to the software shadow */
    MEA_OS_memcpy(&entry.data,
                  Action_data_pio,
                  sizeof(MEA_Action_Entry_Data_dbt));
    
    /* Save the output ports */
    if(Action_outPorts_pi){
        MEA_OS_memcpy(&entry.out_ports,
                  Action_outPorts_pi,
                  sizeof(entry.out_ports));     
    }
    
    /* Save the entry in the database */
    MEA_OS_memcpy(&MEA_Action_drv_Table[Action_id_i],
                  &entry,
                  sizeof(MEA_Action_drv_Table[0]));
     



    
    /* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_Get_Action>                                                   */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status  MEA_API_Get_Action    (MEA_Unit_t                              unit_i,
                                    MEA_Action_t                           Action_id_i,
                                    MEA_Action_Entry_Data_dbt             *Action_data_po,
                                    MEA_OutPorts_Entry_dbt                *Action_outPorts_po,
                                    MEA_Policer_Entry_dbt                 *Action_policer_po,
                                    MEA_EgressHeaderProc_Array_Entry_dbt  *Action_editing_po){

    MEA_Bool            exist;
    mea_action_drv_dbt* drv_action_p;
    mea_memory_mode_e                    save_globalMemoryMode;

    MEA_API_LOG


    /* Set Global Memory Mode */
    save_globalMemoryMode = globalMemoryMode;

     if(MEA_API_IsExist_Action(unit_i,Action_id_i,&exist) != MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- MEA_API_IsExist_Action failed \n",
                           __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    if(exist == MEA_FALSE){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- Action %d not exist \n",
                           __FUNCTION__,Action_id_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    
    drv_action_p = &(MEA_Action_drv_Table[Action_id_i]);

 
    /* Handle data */
    if (Action_data_po) {
          MEA_OS_memcpy(Action_data_po,
                      &drv_action_p->data,
                         sizeof(*Action_data_po));
    }
    
    /* Handle OutPorts */
    if(Action_outPorts_po){
        MEA_OS_memset(Action_outPorts_po,0,sizeof(*Action_outPorts_po));
        if (drv_action_p->data.output_ports_valid) {
              MEA_OS_memcpy(Action_outPorts_po,
                          &(drv_action_p->out_ports),
                          sizeof(*Action_outPorts_po));
        }
    }

    /* Handle Policer */
    if(Action_policer_po) {
        MEA_OS_memset(Action_policer_po,0,sizeof(*Action_policer_po));
        if (drv_action_p->data.tm_id_valid) {
             if (mea_drv_Get_Policer(unit_i,(MEA_TmId_t)(drv_action_p->data.tm_id),
                                     Action_policer_po)!= MEA_OK) {
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s \n- mea_policer_Get failed\n",
                                   __FUNCTION__);
                 MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                 return MEA_ERROR;
             }
        }
    }
    
    /* Get the egress header processor */
    if ((Action_editing_po) && (drv_action_p->data.ed_id_valid)) {
        if ( drv_action_p->EHP_MC == MEA_TRUE ) {
            if ( mea_drv_ehp_mc_get_info(unit_i,
                                         drv_action_p->MC_ID, 
                                         &(drv_action_p->out_ports),
                                         Action_editing_po) != MEA_OK ) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s \n- mea_drv_ehp_mc_get_info failed\n",
                                  __FUNCTION__);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
        } else {
            if (Action_editing_po->ehp_info == NULL) {
                Action_editing_po->num_of_entries = 1;
            } else {
                 if (MEA_API_Get_EgressHeaderProc_Entry(unit_i,
                                                        (MEA_Editing_t)drv_action_p->data.ed_id,
                                                        &(Action_editing_po->ehp_info[0].ehp_data) ) != MEA_OK ) {
                      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s \n- MEA_API_Get_EgressHeaderProc failed\n",
                                      __FUNCTION__);
                    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                    return MEA_ERROR;
                 }
                  MEA_OS_memcpy(&(Action_editing_po->ehp_info[0].output_info),
                               &(drv_action_p->out_ports),
                              sizeof(Action_editing_po->ehp_info[0].output_info));
            }
        }
    }

    
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}


/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_GetFirst_Action>                                              */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_Action (MEA_Unit_t     unit_i,
                                     MEA_Action_t *ActionId_o , 
                                     MEA_Bool     *found_o,
									 mea_action_type_te Action_type)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (ActionId_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- ActionId_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    if (found_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- found_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    *found_o = MEA_FALSE;

    for ( *ActionId_o = 0; 
          *ActionId_o < MEA_MAX_NUM_OF_ACTIONS; 
          (*ActionId_o)++ ) {
		if (MEA_Action_drv_Table[*ActionId_o].data.Action_type != Action_type)
			continue;
		
        if ( MEA_Action_drv_Table[*ActionId_o].valid == MEA_TRUE ) {
            *found_o = MEA_TRUE;
            break;
        }
    }

    return MEA_OK;

}

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_GetNext_Action>                                               */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_Action  (MEA_Unit_t     unit_i,
                                     MEA_Action_t *ActionId_io , 
                                     MEA_Bool     *found_o,
									 mea_action_type_te Action_type){
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (ActionId_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- ActionId_io == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for valid filterId parameter */
	if (MEA_API_IsValid_Action_Id(unit_i, *ActionId_io, Action_type) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- Invalid *ActionId_io %d \n",
                          __FUNCTION__,*ActionId_io);
        return MEA_ERROR;  
    }

    if (found_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- found_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    *found_o = MEA_FALSE;

    for ( (*ActionId_io)++;
          (*ActionId_io) < MEA_MAX_NUM_OF_ACTIONS; 
          (*ActionId_io)++ )
    {
        if (MEA_Action_drv_Table[(*ActionId_io)].data.Action_type != Action_type)
            continue;

        if((MEA_Action_drv_Table[(*ActionId_io)].valid == MEA_TRUE))
        {
            *found_o= MEA_TRUE;
            break;
        }
    }

    return MEA_OK;
                       
}

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_IsExist_Action>                                               */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_Action   (MEA_Unit_t     unit_i,
                                     MEA_Action_t   ActionId_i, 
                                     MEA_Bool      *exist_o){
    
    MEA_API_LOG


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS                                              
    if (exist_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- exist == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
#endif 

    *exist_o=MEA_FALSE;


	if (ActionId_i >= MEA_MAX_NUM_OF_ACTIONS) {
        *exist_o = MEA_FALSE;
        return MEA_ERROR;
	}

   if ( (MEA_Action_drv_Table[ActionId_i].valid ==MEA_TRUE)) {
      *exist_o=MEA_TRUE; 
   }

   return MEA_OK;
                                              
}



MEA_Status MEA_API_IsExist_Action_ByType(MEA_Unit_t     unit_i,
										MEA_Action_t   ActionId_i,
										mea_action_type_te Action_type,
										MEA_Bool      *exist_o){

	MEA_API_LOG


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS                                              
		if (exist_o == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s \n- exist == NULL\n", __FUNCTION__);
			return MEA_ERROR;
		}
#endif 

	*exist_o = MEA_FALSE;


	if (ActionId_i >= MEA_MAX_NUM_OF_ACTIONS) {
		return MEA_FALSE;
	}

	if ((MEA_API_IsValid_Action_Id(unit_i, ActionId_i, Action_type)) &&
		(MEA_Action_drv_Table[ActionId_i].valid == MEA_TRUE)) {
		*exist_o = MEA_TRUE;
	}

	return MEA_OK;

}


/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_IsValid_Action_id>                                            */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Bool MEA_API_IsValid_Action_Id(MEA_Unit_t     unit,
								   MEA_Action_t  ActionId,
								   mea_action_type_te Action_type)
{

    MEA_db_HwUnit_t           hwUnit;
    

    MEA_API_LOG

    if (ActionId >= MEA_MAX_NUM_OF_ACTIONS) {
        return MEA_FALSE;
    }

    if (ActionId == MEA_ACTION_ID_TO_CPU || 
        ActionId == MEA_ACTION_ID_TO_FWD_CPU) {
        return MEA_TRUE;
    }

    MEA_DRV_GET_HW_UNIT(unit,
        hwUnit,
        return 0;);




#if 1
	switch (Action_type) {
    case MEA_ACTION_TYPE_SERVICE: 
       if ((ActionId <  MEA_ACTION_TBL_SERVICE_START_INDEX+1) ||
           (ActionId >= MEA_ACTION_TBL_SERVICE_START_INDEX+
                        MEA_MAX_NUM_OF_SERVICE_ACTIONS   )  ) {
            return MEA_FALSE;
       }
       break;
    case MEA_ACTION_TYPE_FWD: 
       if ((ActionId <  MEA_ACTION_TBL_FWD_START_INDEX) ||
           (ActionId >= MEA_ACTION_TBL_FWD_START_INDEX+
                        MEA_MAX_NUM_OF_FWD_ACTIONS   )  ) {
            return MEA_FALSE;
       }
       break;
    case MEA_ACTION_TYPE_FILTER: 
       if ((ActionId <  MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit)) ||
           (ActionId >= MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit)+
                        MEA_MAX_NUM_OF_FILTER_ACTIONS   )  ) {
            return MEA_FALSE;
       }
       break;

    case MEA_ACTION_TYPE_HPM:
        if ((ActionId < MEA_ACTION_TBL_HPM_START_INDEX(hwUnit)) ||
            (ActionId >= MEA_ACTION_TBL_HPM_START_INDEX(hwUnit) +
                MEA_MAX_NUM_OF_HPM_ACTIONS)) {
            return MEA_FALSE;
        }
        break;


    case MEA_ACTION_TYPE_SERVICE_EXTERNAL:
        if ((ActionId < MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX) ||
            (ActionId >= MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX +
            MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW)) {
            return MEA_FALSE;
        }
        break;
    case MEA_ACTION_TYPE_ACL5:
        if ((ActionId < MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX) ||
            (ActionId >= MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX +
                MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW)) {
            return MEA_FALSE;
        }
        break;


    case MEA_ACTION_TYPE_LAST:
    default: 
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- MEA_Action_drv_type (%d) is unknown \n",
                          __FUNCTION__,
						  Action_type);
        return MEA_FALSE;
    }
#endif

   return MEA_TRUE;
}



MEA_Status MEA_API_Get_NumOf_Action(MEA_Unit_t unit_i, mea_action_type_te Action_type, MEA_Action_numInfo_dbt *Info)
{
    MEA_db_HwUnit_t           hwUnit;
    MEA_Uint32 CountAction=0;
    MEA_Action_t  ActionId,start_ActionId, end_ActionId;
    MEA_Uint32 TotalActionType;
    MEA_API_LOG

      
        if (Info == NULL) {
            return MEA_ERROR;
        }

   

    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return 0;);

    switch (Action_type) {
    case MEA_ACTION_TYPE_SERVICE:
        start_ActionId = (MEA_ACTION_TBL_SERVICE_START_INDEX + 1);
        end_ActionId   = (MEA_MAX_NUM_OF_SERVICES_INTERNAL); //(MEA_ACTION_TBL_SERVICE_START_INDEX + MEA_MAX_NUM_OF_SERVICE_ACTIONS);


       
        break;
    case MEA_ACTION_TYPE_FWD:
        start_ActionId = MEA_ACTION_TBL_FWD_START_INDEX;
        end_ActionId = MEA_ACTION_TBL_FWD_START_INDEX + MEA_MAX_NUM_OF_FWD_ACTIONS;

        break;
    case MEA_ACTION_TYPE_FILTER:
        start_ActionId = MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit);
        end_ActionId = MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit) + MEA_MAX_NUM_OF_FILTER_ACTIONS;
        break;

    case MEA_ACTION_TYPE_HPM:
        start_ActionId = MEA_ACTION_TBL_HPM_START_INDEX(hwUnit);
        end_ActionId   = MEA_ACTION_TBL_HPM_START_INDEX(hwUnit) + MEA_MAX_NUM_OF_HPM_ACTIONS;
        break;


    case MEA_ACTION_TYPE_SERVICE_EXTERNAL:
        start_ActionId = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX;
        end_ActionId = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX + MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW;
        break;
    case MEA_ACTION_TYPE_ACL5:
        start_ActionId = MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX; 
        end_ActionId = MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX + MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW;
        break;


    case MEA_ACTION_TYPE_LAST:
    default:
        
        return 0;
    break;
    }
    CountAction = 0;
    TotalActionType = end_ActionId - start_ActionId;

    for (ActionId = start_ActionId; ActionId <= end_ActionId; ActionId++) 
    {
        if(MEA_Action_drv_Table[ActionId].valid == MEA_FALSE)
            continue;
        if(MEA_Action_drv_Table[ActionId].valid == MEA_TRUE && MEA_Action_drv_Table[ActionId].data.Action_type == Action_type)
            CountAction++;
    }


    Info->usedAction = CountAction;
        Info->TotalActionType = TotalActionType;

    return MEA_OK;
}


/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_AddOwner_Action>                                              */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status MEA_API_AddOwner_Action(MEA_Unit_t   unit_i, 
                                   MEA_Action_t ActionId_i)
{
    
    MEA_Bool exist;

    MEA_API_LOG

    if (MEA_API_IsExist_Action(unit_i,ActionId_i,&exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- MEA_API_IsExist_Action failed (id=%d)\n",
                          __FUNCTION__,ActionId_i);
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- ActionId_i %d not exist \n",
                          __FUNCTION__,ActionId_i);
        return MEA_ERROR;
    }

    MEA_Action_drv_Table[ActionId_i].num_of_owners++;

    return MEA_OK;
}



/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_DelOwner_Action>                                              */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status MEA_API_DelOwner_Action(MEA_Unit_t   unit_i, 
                                   MEA_Action_t ActionId_i)
{

    MEA_Bool exist;

    MEA_API_LOG

    if (MEA_API_IsExist_Action(unit_i,ActionId_i,&exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- MEA_API_IsExist_Action failed (id=%d)\n",
                          __FUNCTION__,ActionId_i);
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- ActionId_i %d not exist \n",
                          __FUNCTION__,ActionId_i);
        return MEA_ERROR;
    }
// need to check
    if (MEA_Action_drv_Table[ActionId_i].num_of_owners == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- actionId %d num_of_owners (%d) <= 0 \n",
                          __FUNCTION__,
                          ActionId_i,
                          MEA_Action_drv_Table[ActionId_i].num_of_owners);
        return MEA_ERROR;
    }

    MEA_Action_drv_Table[ActionId_i].num_of_owners--;
    
    
    return MEA_OK;
}


/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_GetOwner_Action>                                              */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status MEA_API_GetOwner_Action(MEA_Unit_t   unit_i, 
                                   MEA_Action_t ActionId_i,
                                   MEA_Uint32  *numOfOwners_o)
{

    MEA_Bool exist;

      MEA_API_LOG

    if (MEA_API_IsExist_Action(unit_i,ActionId_i,&exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- MEA_API_IsExist_Action failed (id=%d)\n",
                          __FUNCTION__,ActionId_i);
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s \n- ActionId_i %d not exist \n",
                          __FUNCTION__,ActionId_i);
        return MEA_ERROR;
    }

    if (numOfOwners_o) {
        *numOfOwners_o = MEA_Action_drv_Table[ActionId_i].num_of_owners;
    }

    return MEA_OK;
}


MEA_Status MEA_API_DeleteAll_Action(MEA_Unit_t unit_i)
{

    MEA_Action_t  ActionId;
    MEA_Bool      exist;
    
	if (MEA_API_GetFirst_Action(unit_i, &ActionId, &exist, MEA_ACTION_TYPE_FWD) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- MEA_API_GetFirst_Action failed\n",__FUNCTION__);
        return MEA_ERROR;
    }
    while (exist) {
        if ((ActionId != MEA_ACTION_ID_TO_CPU) &&
            ActionId != MEA_ACTION_ID_TO_FWD_CPU   ) {

            /* Check if the actionId is use */
            /* If the action exist then delete it */
            while (exist) { 
                if (MEA_API_Delete_Action (MEA_UNIT_0,ActionId)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                       "%s - MEA_API_Delete_Action failed (id=%d) \n",
                                       __FUNCTION__,ActionId);
                    return MEA_ERROR;
                }
                if (MEA_API_IsExist_Action  (MEA_UNIT_0, ActionId , &exist)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s \n- MEA_API_IsExist_Action_ById failed (id=%d) \n",
                                      __FUNCTION__,ActionId);
                    return MEA_ERROR;
                }
            
            }
        }


        if (ActionId < (MEA_Action_t)ENET_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_START(MEA_UNIT_0))
            ActionId=ENET_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_START(MEA_UNIT_0);

		if (MEA_API_GetNext_Action(unit_i, &ActionId, &exist, MEA_ACTION_TYPE_FWD) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s \n- MEA_API_GetNext_Action failed\n", __FUNCTION__);
            return MEA_ERROR;
        }


    }

    return MEA_OK;
}



MEA_Status MEA_API_DeleteAll_Action_HPM(MEA_Unit_t unit_i)
{

    MEA_Action_t  ActionId;
    MEA_Bool      exist;

    if (MEA_API_GetFirst_Action(unit_i, &ActionId, &exist, MEA_ACTION_TYPE_HPM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s \n- MEA_API_GetFirst_Action failed\n", __FUNCTION__);
        return MEA_ERROR;
    }
    while (exist) {
        

            /* Check if the actionId is use */
            /* If the action exist then delete it */
            while (exist) {
                if (MEA_API_Delete_Action(MEA_UNIT_0, ActionId) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_Delete_Action failed (id=%d) \n",
                        __FUNCTION__, ActionId);
                    return MEA_ERROR;
                }
                if (MEA_API_IsExist_Action(MEA_UNIT_0, ActionId, &exist) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- MEA_API_IsExist_Action_ById failed (id=%d) \n",
                        __FUNCTION__, ActionId);
                    return MEA_ERROR;
                }

            }
           


        if (ActionId < (MEA_Action_t)MEA_Platform_Get_HPM_BASE())
            ActionId = MEA_Platform_Get_HPM_BASE();

if (MEA_API_GetNext_Action(unit_i, &ActionId, &exist, MEA_ACTION_TYPE_HPM) != MEA_OK) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s \n- MEA_API_GetNext_Action failed\n", __FUNCTION__);
    return MEA_ERROR;
}


    }

    return MEA_OK;
}


MEA_Status mea_action_Get_hw_info(MEA_Unit_t unit_i, MEA_Action_t actionId_i, MEA_Uint32 *Action_val)
{

mea_Action_buildHWData(unit_i,
    0,
    actionId_i,
    &MEA_Action_drv_HwTable[actionId_i],
    MEA_ACTION_BASE_ADDRES_DDR_WIDTH,/*256bit*/
    Action_val);

    return MEA_OK;
}



/************************************************************************/
/* ACTION FOR ACL5                                                      */
/************************************************************************/



MEA_Status mea_drv_actAcl5_get_freeIndex(MEA_Unit_t unit, MEA_Action_t *Id)
{
    MEA_Action_t index;
    MEA_Status   ret = MEA_ERROR;

    if (!MEA_ACL5_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ACL5_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (Id == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;

    }

    if (*Id >= MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ACL Action out of the range  \n",
            __FUNCTION__);
        return ret;
    }
    for (index = 1; index < MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW; index++)
    {
        if (index == MEA_PLAT_GENERATE_NEW_ID)
            continue;

        if (MEA_ACL5_Action_drv_Table[index] == MEA_FALSE) {
            *Id = index;
            ret = MEA_OK;

            break;
        }

    }

    return ret;
}

MEA_Status mea_drv_ACL5Action_IsRange(MEA_Unit_t unit, MEA_Action_t Id)
{
    if (Id >= MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW) {

        return MEA_ERROR;
    }

    return MEA_OK;
}
MEA_Status MEA_API_Create_ACL5Action(MEA_Unit_t unit, MEA_Action_t *Id_io, MEA_Action_rule_t entry)
{

    MEA_Action_t ActionId;

    if (!MEA_ACL5_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ACL5_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry.action_params == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -entry.action_params = NULL \n",
            __FUNCTION__);
        return MEA_ERROR;

    }

    if (entry.action_params->Action_type != MEA_ACTION_TYPE_ACL5) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -Error This function is for ACTION_TYPE_ACL5   \n",
            __FUNCTION__);
        return MEA_ERROR;

    }


    if (*Id_io != MEA_PLAT_GENERATE_NEW_ID) {
        if(mea_drv_ACL5Action_IsRange(unit, *Id_io) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- mea_drv_ACL5Action_IsRange Id %d out of range \n",
                __FUNCTION__, *Id_io);
            return MEA_ERROR;
        }
        if (MEA_ACL5_Action_drv_Table[*Id_io] == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- Action Id %d already exist \n",
                __FUNCTION__, *Id_io);
            return MEA_ERROR;
        }
    } else {

       if( mea_drv_actAcl5_get_freeIndex(unit, Id_io) != MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s - error actAcl5_get_freeIndex  \n",
               __FUNCTION__);
           return MEA_ERROR;
        }
    }


   

    ActionId = MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX + (*Id_io) ;
    entry.action_params->Action_type = MEA_ACTION_TYPE_ACL5;


    if (MEA_API_Create_Action(MEA_UNIT_0,
        entry.action_params,
        entry.OutPorts_Entry,
        entry.Policer_Entry,
        entry.EHP_Entry,
        &ActionId) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - ACL5_EXTERNAL Create failed (Act %d) \n", __FUNCTION__, (*Id_io));
        return MEA_ERROR;
    }


    MEA_ACL5_Action_drv_Table[*Id_io]= MEA_TRUE;


    return MEA_OK;

}

MEA_Status MEA_API_ACL5Action_isExist(MEA_Unit_t  unit, 
                                      MEA_Action_t Id, 
                                      MEA_Bool silent, 
                                      MEA_Bool *exist)
{
    if (exist == NULL) {
        if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);

        return MEA_ERROR;
    }
    *exist = MEA_FALSE;

    if (mea_drv_ACL5Action_IsRange(unit, Id) != MEA_OK) {
        if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5Action_IsRange failed  for id=%d  \n", __FUNCTION__, Id);

        return MEA_ERROR;
    }

    if (MEA_ACL5_Action_drv_Table[Id] == MEA_TRUE) {
        *exist = MEA_TRUE;
        return MEA_OK;
    }

    return MEA_OK;
}




MEA_Status MEA_API_Get_ACL5Action(MEA_Unit_t unit, MEA_Action_t Id, MEA_Action_rule_t entry)
{
    MEA_Action_t  ActionId;

    if (!MEA_ACL5_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ACL5_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
   


    if (Id >= MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ACL Action out of the range  \n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    ActionId = MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX + (Id);
    entry.action_params->Action_type = MEA_ACTION_TYPE_ACL5;


    if (MEA_API_Get_Action(MEA_UNIT_0,
        ActionId,
        entry.action_params,
        entry.OutPorts_Entry,
        entry.Policer_Entry,
        entry.EHP_Entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - ACL5_ACTION failed  Id=%d (Act %d) \n", __FUNCTION__, ActionId);
        return MEA_ERROR;
    }






    return MEA_OK;
}

MEA_Status MEA_API_Delete_ACL5Action(MEA_Unit_t unit, MEA_Action_t ActionId)
{

    MEA_Action_t Id;

    if (!MEA_ACL5_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ACL5_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (ActionId >= MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ActionId %d out of range  \n",
            __FUNCTION__, ActionId);
        return MEA_ERROR;
    }

    if (MEA_ACL5_Action_drv_Table[ActionId] == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ActionId %d not valid \n",
            __FUNCTION__, ActionId);
        return MEA_ERROR;
    }
   

    Id = MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX + (ActionId);

    if (MEA_DRV_Delete_Action(unit, Id, MEA_ACTION_TYPE_ACL5) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_DRV_Delete_Action %d\n",
            __FUNCTION__ , ActionId);
        return MEA_ERROR;
    }
    
    MEA_ACL5_Action_drv_Table[ActionId] = MEA_FALSE;
    return MEA_OK;

}



MEA_Status MEA_API_DeleteAll_Action_ACL5(MEA_Unit_t unit)
{

    MEA_Action_t ActionId;
    

   


    for(ActionId=0; ActionId<MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW ; ActionId++){
        if (MEA_ACL5_Action_drv_Table[ActionId] == MEA_FALSE)
            continue;
    
     

    if (MEA_API_Delete_ACL5Action(unit, (ActionId)) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Delete_ACL5Action %d\n",
            __FUNCTION__, ActionId);
        return MEA_ERROR;
    }
  }

    return MEA_OK;
}