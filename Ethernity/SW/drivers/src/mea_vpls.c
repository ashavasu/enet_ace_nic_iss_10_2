/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*************************************************************************************/
/* 	Module Name:		 mea_vpls.c		   									 */ 
/*																					 */
/*  Module Description:	 MEA driver										 */
/*																					 */
/*  Date of Creation:	 													         */
/*																					 */
/*  Author Name:			 													         */
/*************************************************************************************/ 

/*-------------------------------- Includes ------------------------------------------*/

#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_drv_common.h"
#include "enet_queue_drv.h"
#include "mea_policer_drv.h"
#include "mea_vpls.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#if 1
#define MEA_INTERNAL_PORT_DL_LEVEL1        24
#define MEA_INTERNAL_PORT_DL_LEVEL2        25
#define MEA_INTERNAL_PORT_DL_LEVEL3        26
#define MEA_INTERNAL_PORT_DL_LEVEL3_27     27
#else
#define MEA_INTERNAL_PORT_DL_LEVEL1    108
#define MEA_INTERNAL_PORT_DL_LEVEL2    109
#define MEA_INTERNAL_PORT_DL_LEVEL3    110
#define MEA_INTERNAL_PORT_DL_LEVEL3_27 111
#endif


#define MEA_INTERNAL_PORT_DL_TLS     126 /**/





#define MEA_VPLS_ED_SWAP 255 
#define ENET_QUEUE_MC_MQS_DEF_VPLS_VAL 32




#define MEA_vpls_VPLSi_SHAPER_CIR_LEVEL2_TYPE1  (32*1000*1000)
#define MEA_vpls_VPLSi_SHAPER_CIR_LEVEL2_TYPE2  (8*32*1000*1000)

#define MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_TYPE3   (950*1000*1000)
#define MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_TYPE4  (9500*1000*1000)


#define MEA_vpls_VPLSi_SHAPER_LEVEL2_ENABLE (MEA_FALSE)
#define MEA_vpls_VPLSi_SHAPER_CIR    ((MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) ? (MEA_vpls_VPLSi_SHAPER_CIR_LEVEL2_TYPE1) : (MEA_vpls_VPLSi_SHAPER_CIR_LEVEL2_TYPE2))   //128000 
#define MEA_vpls_VPLSi_SHAPER_CBS 64

#define MEA_vpls_VPLSi_SHAPER_LEVEL3_24_ENABLE  (MEA_TRUE)
#define MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_24     (MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_TYPE3)
#define MEA_vpls_VPLSi_SHAPER_CBS_LEVEL3_24     64


#define MEA_vpls_DL_LEVEL2_ENABLE (MEA_TRUE)
#define MEA_vpls_VPLSi_SHAPER_LEVEL3_25_ENABLE (MEA_vpls_DL_LEVEL2_ENABLE)
#define MEA_vpls_DL_LEVEL2_SHAPER_CIR_25 (MEA_vpls_VPLSi_SHAPER_CIR_LEVEL2_TYPE1)  
#define MEA_vpls_DL_LEVEL2_SHAPER_CBS_25 64


#define MEA_vpls_VPLSi_SHAPER_LEVEL3_26_ENABLE  (MEA_TRUE)
#define MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_26     (MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_TYPE3)
#define MEA_vpls_VPLSi_SHAPER_CBS_LEVEL3_26     64

#define MEA_vpls_VPLSi_SHAPER_LEVEL3_ENABLE  (MEA_TRUE)
#define MEA_vpls_VPLSi_SHAPER_LEVEL3_27_ENABLE MEA_vpls_VPLSi_SHAPER_LEVEL3_ENABLE
#define MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_27  (MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_TYPE3) 
#define MEA_vpls_VPLSi_SHAPER_CBS_LEVEL3_27  64


#define MEA_vpls_VPLSi_POLICER_CIR  (5*1024*1024) //(128 * 1024)
#define MEA_vpls_VPLSi_POLICER_CBS  (128000)

#define MEA_vpls_VPLSi_POLICER_CIR_leaf (10*1024*1024)
#define MEA_vpls_VPLSi_POLICER_CBS_leaf (128000)

#define MEA_vpls_VPLSi_MEETRING_DISABLE      MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL
#define MEA_vpls_VPLSi_POLICER_PGT   0

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/

MEA_Bool  mea_drv_vpls_tmId_valid = MEA_FALSE;
MEA_Bool  mea_drv_vpls_tmId_value = 0;


typedef struct{
    MEA_Bool enable;
    //MEA_Uint32     UE_PND_ID;
    MEA_VPLS_UE_Pdn_dl_dbt Ue_pdn_dl; /*data from user*/

    MEA_Service_t      DL_ServiceId;  /*create by Driver*/
    MEA_MacAddr        eNB_MAC;

    MEA_Uint16   vp;  /* the learn source port for enB*/

}mea_drv_vpls_ue_pdn_dl;


typedef struct{
    MEA_Bool       valid;
    MEA_VPLS_dbt   data_info;
    MEA_Service_t  Vlan_serviceId;     /* Service to 32*/
    MEA_Uint32     VPLS_tmId;
    
    MEA_Uint32     VPLS_Pmid;
    MEA_Service_t  *servicelevel2Group; //servicelevel2Group[MEA_VPLS_MAX_OF_CY_GROUP];        /* group to 32*/

    mea_drv_vpls_ue_pdn_dl  *data_info_ue_pdn_dl; //data_info_ue_pdn_dl[MEA_VPLS_MAX_OF_FLOODING];
    MEA_Uint32               numof_ue_pdn;
    MEA_OutPorts_Entry_dbt     OutPorts;

    MEA_Service_t             ServiceDownlink;
    MEA_Service_t             ServiceLocaSwitch;/*from port 24*/

}MEA_VPLS_entry_dbt;


typedef struct {
    MEA_Bool    valid;
    MEA_Port_t  vp;

}mea_vpls_out_vp_dbt;


typedef struct MyStruct
{
    MEA_Bool valid;
    mea_UEpdn_Handover_dbt data;
    mea_UEpdn_Handover_dbt data_internal;

    
}mea_UEpdn_Handover_data_dbt;
/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/



MEA_FlowCoSMappingProfile_Id_t VPLS_cosMappingId = 0;

MEA_Uint16                   vpls_ingressPolicerFlow = 0;
MEA_Uint32                   MEA_drv_VPLS_EditId[256];
MEA_VPLS_entry_dbt	         *MEA_drv_VPLS_Table = NULL;
mea_UEpdn_Handover_data_dbt  *MEA_drv_UEPDN_VPLS_Handover_Table = NULL;

mea_vpls_out_vp_dbt        *mea_vpls_groups_leval1=NULL;

mea_vpls_out_vp_dbt        *mea_vpls_groups_leval2=NULL;


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
#define MEA_VPLS_MODE_PRI_QUEUE_STRICT     ((MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) ? (1) : (1))
#define MEA_VPLS_MODE_PRI_QUEUE_STRICT_LV3 ((MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) ? (1) : (1)) 

MEA_Status mea_drv_UEPDN_Set_Handover_UpdateHW(MEA_Unit_t unit);
MEA_Status mea_vpls_init_internalclusterLEVEL(void)
{

    ENET_Queue_dbt entry;
    MEA_Uint32 maxGroup;
    
    ENET_QueueId_t clusterId;
    MEA_Uint32 priQ;
    MEA_Uint8 index;

    MEA_Globals_Entry_dbt globals_entry;
    MEA_EgressPort_Entry_dbt Egress_entry;
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "init VPLS clusterLEVEL\n");

    MEA_OS_memset(&globals_entry, 0, sizeof(globals_entry));
    MEA_OS_memset(&Egress_entry, 0, sizeof(Egress_entry));

    if (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) {
        if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_VDSL64) == 0))
        {
            for(index=0;index<64;index++)
                ENET_Delete_Queue(ENET_UNIT_0, index);
            

        }
    }



    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_API_Get_Globals_Entry failed\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if ((MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) || 
        (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE)
        )
    /*create cluster on port 127 id 20 */
    {
        index = 0;
        clusterId = 20;
        /* Init the entry */
        MEA_OS_memset(&entry, 0, sizeof(entry));
        MEA_OS_strcpy(entry.name, ENET_PLAT_GENERATE_NEW_NAME);
        entry.adminOn = MEA_TRUE;

        entry.port.type = ENET_QUEUE_PORT_TYPE_PORT;
        entry.groupId = 0;
        entry.port.id.port = (ENET_PortId_t)(127);

        if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
            entry.mode.type = ENET_QUEUE_MODE_TYPE_RR;
            entry.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL; // because fpga use this
        }
        else {
            entry.mode.type = ENET_QUEUE_MODE_TYPE_WFQ;
            entry.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
        }




        entry.shaper_enable = ENET_FALSE;
        entry.shaper_info.CIR = 1000000000;
        entry.shaper_info.CBS = 64;

        entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
        entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

        entry.shaper_info.overhead = 0;
        entry.shaper_info.Cell_Overhead = 0;




        for (priQ = 0; priQ < MEA_NUM_OF_ELEMENTS(entry.pri_queues); priQ++) {
            if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL_CPU;
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU_EPC(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_CPU_EPC(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_CPU_EPC(priQ);
            }
            if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL_CPU;
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU_EMB(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_CPU_EMB(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_CPU_EMB(priQ);
            }
           
           
            if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY) && (MEA_VPLS_MODE_PRI_QUEUE_STRICT == 1)) {
                entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_STRICT_DEF_VAL;
            }
            else {
                if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY)) {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
                else {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
            }

        }
        /* create the queue */

        if (ENET_Create_Queue(ENET_UNIT_0,
            &entry,
            &clusterId) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Create_Queue failed (port=%d) clusterId %d\n",
                __FUNCTION__, entry.port.id.port, clusterId);
            return MEA_ERROR;
        }


    }



    /************************************************************************/
    /* Create shaper on port 24    950M                                      */
    /************************************************************************/
    MEA_API_Get_EgressPort_Entry(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL1, &Egress_entry);
    Egress_entry.shaper_enable = MEA_FALSE;

    if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PORT_TYPE) == MEA_SHAPER_SUPPORT_PORT_TYPE) {
        Egress_entry.shaper_enable = MEA_vpls_VPLSi_SHAPER_LEVEL3_24_ENABLE;
    }else{
    	Egress_entry.shaper_enable = MEA_FALSE;
    }

    Egress_entry.shaper_info.CIR = MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_24;
    Egress_entry.shaper_info.CBS = MEA_vpls_VPLSi_SHAPER_CBS_LEVEL3_24;

    Egress_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
    Egress_entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

    Egress_entry.shaper_info.overhead = 0;
    Egress_entry.shaper_info.Cell_Overhead = 0;


    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL1, &Egress_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_API_Set_EgressPort_Entry for port %d failed\n", MEA_INTERNAL_PORT_DL_LEVEL1);
        return MEA_ERROR;
    }



    /************************************************************************/
    /* Create shaper on port 25    1M                                      */
    /************************************************************************/

    MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,MEA_INTERNAL_PORT_DL_LEVEL2,&Egress_entry);
    Egress_entry.shaper_enable = MEA_FALSE;

    if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PORT_TYPE) == MEA_SHAPER_SUPPORT_PORT_TYPE) {
        Egress_entry.shaper_enable = MEA_vpls_VPLSi_SHAPER_LEVEL3_25_ENABLE;
    }else{
    	Egress_entry.shaper_enable = MEA_FALSE;
    }

    Egress_entry.shaper_info.CIR = MEA_vpls_DL_LEVEL2_SHAPER_CIR_25;
    Egress_entry.shaper_info.CBS = MEA_vpls_DL_LEVEL2_SHAPER_CBS_25;

    Egress_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
    Egress_entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

    Egress_entry.shaper_info.overhead = 0;
    Egress_entry.shaper_info.Cell_Overhead = 0;


    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL2, &Egress_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_API_Set_EgressPort_Entry for port %d failed\n", MEA_INTERNAL_PORT_DL_LEVEL2);
        return MEA_ERROR;
    }

    /************************************************************************/
    /* Create shaper on port 26    950M                                      */
    /************************************************************************/
    MEA_API_Get_EgressPort_Entry(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL3, &Egress_entry);
    Egress_entry.shaper_enable = MEA_FALSE;

    if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PORT_TYPE) == MEA_SHAPER_SUPPORT_PORT_TYPE) {
        Egress_entry.shaper_enable = MEA_vpls_VPLSi_SHAPER_LEVEL3_26_ENABLE;
    }else{
    	Egress_entry.shaper_enable = MEA_FALSE;
    }

    
        

    Egress_entry.shaper_info.CIR = MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_26;
    Egress_entry.shaper_info.CBS = MEA_vpls_VPLSi_SHAPER_CBS_LEVEL3_26;

    Egress_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
    Egress_entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

    Egress_entry.shaper_info.overhead = 0;
    Egress_entry.shaper_info.Cell_Overhead = 0;


    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL3, &Egress_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_API_Set_EgressPort_Entry for port %d failed\n", MEA_INTERNAL_PORT_DL_LEVEL2);
        return MEA_ERROR;
    }



    /************************************************************************/
    /* Create shaper on port 27    30M                                      */
    /************************************************************************/
    MEA_API_Get_EgressPort_Entry(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL3_27, &Egress_entry);
    Egress_entry.shaper_enable = MEA_FALSE;

    if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PORT_TYPE) == MEA_SHAPER_SUPPORT_PORT_TYPE) {
        Egress_entry.shaper_enable = MEA_vpls_VPLSi_SHAPER_LEVEL3_27_ENABLE;
    }else{
    	Egress_entry.shaper_enable = MEA_FALSE;
    }

    Egress_entry.shaper_info.CIR = MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_27;
    Egress_entry.shaper_info.CBS = MEA_vpls_VPLSi_SHAPER_CBS_LEVEL3_27;

    Egress_entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
    Egress_entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

    Egress_entry.shaper_info.overhead = 0;
    Egress_entry.shaper_info.Cell_Overhead = 0;


    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL3_27, &Egress_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_API_Set_EgressPort_Entry for port %d failed\n", MEA_INTERNAL_PORT_DL_LEVEL2);
        return MEA_ERROR;
    }

     
        
            /*create cluster 126 on port 24 */
            {     
                index = 0;
                clusterId = MEA_INTERNAL_PORT_DL_TLS;
                /* Init the entry */
                MEA_OS_memset(&entry, 0, sizeof(entry));
                MEA_OS_strcpy(entry.name, ENET_PLAT_GENERATE_NEW_NAME);
                entry.adminOn = MEA_TRUE;

                entry.port.type = ENET_QUEUE_PORT_TYPE_PORT;
                entry.groupId = 0;
                entry.port.id.port = (ENET_PortId_t)(MEA_INTERNAL_PORT_DL_LEVEL1);

                if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
                    entry.mode.type = ENET_QUEUE_MODE_TYPE_RR;
                    entry.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL; // because fpga use this
                }
                else {
                    entry.mode.type = ENET_QUEUE_MODE_TYPE_WFQ;
                    entry.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
                }



                
                entry.shaper_enable = ENET_FALSE;
                entry.shaper_info.CIR = 1000000000;
                entry.shaper_info.CBS = 64;

                entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
                entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

                entry.shaper_info.overhead = 0;
                entry.shaper_info.Cell_Overhead = 0;
                



                for (priQ = 0; priQ < MEA_NUM_OF_ELEMENTS(entry.pri_queues); priQ++) {
                    if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
                        entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_126(priQ);
                        entry.pri_queues[priQ].max_q_size_Byte    = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_126(priQ);
                        entry.pri_queues[priQ].mc_MQS             = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_126(priQ);;
                        entry.pri_queues[priQ].mtu                = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
                    }
                    if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
                        entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_126_EMB(priQ);
                        entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_126_EMB(priQ);
                        entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_126_EMB(priQ);;
                        entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
                    
                    }
#if defined(HW_BOARD_IS_PCI)
                    entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G(priQ);
                    entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G(priQ);
                    entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G(priQ);;
                    entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;

#endif
                    if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY) && (MEA_VPLS_MODE_PRI_QUEUE_STRICT == 1) ) {
                        entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                        entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_STRICT_DEF_VAL;
                    }
                    else {
                        if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY)) {
                            entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
                            entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                        }
                        else {
                            entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                            entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                        }
                  }

                }
                /* create the queue */

                if (ENET_Create_Queue(ENET_UNIT_0,
                    &entry,
                    &clusterId) != ENET_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - ENET_Create_Queue failed (port=%d) clusterId %d\n",
                        __FUNCTION__, entry.port.id.port, clusterId);
                    return MEA_ERROR;
                }


            }


/************************************************************************/
/*                                                                      */
/************************************************************************/
            ENET_Delete_Queue(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL3); /*Delete the default queue on port*/

            {
                index = 0;
                clusterId = MEA_INTERNAL_PORT_DL_LEVEL3;
                /* Init the entry */
                MEA_OS_memset(&entry, 0, sizeof(entry));
                MEA_OS_strcpy(entry.name, ENET_PLAT_GENERATE_NEW_NAME);
                entry.adminOn = MEA_TRUE;

                entry.port.type = ENET_QUEUE_PORT_TYPE_PORT;
                entry.groupId = 0;
                entry.port.id.port = (ENET_PortId_t)(MEA_INTERNAL_PORT_DL_LEVEL3);
                if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
                    entry.groupId = 0;
                }

                

                if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
                    entry.mode.type = ENET_QUEUE_MODE_TYPE_RR;
                    entry.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL; // because fpga use this
                }
                else {
                    entry.mode.type = ENET_QUEUE_MODE_TYPE_WFQ;
                    entry.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
                }



                if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_CLUSTER_TYPE) == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) {
                    entry.shaper_enable = ENET_FALSE;
                }
                else {
                    entry.shaper_enable = ENET_FALSE;
                }

                entry.shaper_info.CIR = MEA_vpls_VPLSi_SHAPER_CIR;
                entry.shaper_info.CBS = MEA_vpls_VPLSi_SHAPER_CBS;

                entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
                entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

                entry.shaper_info.overhead = 0;
                entry.shaper_info.Cell_Overhead = 0;




                for (priQ = 0; priQ < MEA_NUM_OF_ELEMENTS(entry.pri_queues); priQ++) {
                    if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
                        entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_26(priQ);
                        entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_26(priQ);
                        entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_26(priQ);;
                        entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
                    }
                    if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
                        entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_26_EMB(priQ);
                        entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_26_EMB(priQ);
                        entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_26_EMB(priQ);;
                        entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
                    }
#if defined(HW_BOARD_IS_PCI)
                    entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G(priQ);
                    entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G(priQ);
                    entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G(priQ);;
                    entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;

#endif

                    if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY) ) {
                        entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                        entry.pri_queues[priQ].mode.value.strict_priority = ENET_CLUSTER_WFQ_DEF_VAL;
                    }
                    else {
                        if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY)) {
                            entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
                            entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                        }
                        else {
                            entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                            entry.pri_queues[priQ].mode.value.strict_priority = ENET_CLUSTER_WFQ_DEF_VAL;
                        }
                    }

                }
                /* create the queue */

                //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " port %d cluster id=%d\n", MEA_INTERNAL_PORT_DL_LEVEL2, clusterId);

                if (ENET_Create_Queue(ENET_UNIT_0,
                    &entry,
                    &clusterId) != ENET_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - ENET_Create_Queue failed (port=%d) clusterId %d\n",
                        __FUNCTION__, entry.port.id.port, clusterId);
                    return MEA_ERROR;
                }

                mea_vpls_groups_leval1[index].vp = clusterId;
                mea_vpls_groups_leval1[index].valid = MEA_TRUE;
            }

/************************************************************************/
/*                                                                      */
/************************************************************************/


    ENET_Delete_Queue(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL2); /*Delete the default queue on port*/

    {
        index = 0;
        clusterId = MEA_INTERNAL_PORT_DL_LEVEL2;
        /* Init the entry */
        MEA_OS_memset(&entry, 0, sizeof(entry));
        MEA_OS_strcpy(entry.name, ENET_PLAT_GENERATE_NEW_NAME);
        entry.adminOn = MEA_TRUE;

        entry.port.type = ENET_QUEUE_PORT_TYPE_PORT;
        entry.groupId = 1;
        if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
            entry.groupId = 0;
        }
        
        entry.port.id.port = (ENET_PortId_t)(MEA_INTERNAL_PORT_DL_LEVEL2);

        if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
            entry.mode.type = ENET_QUEUE_MODE_TYPE_RR;
            entry.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL; // because fpga use this
        }
        else{
            entry.mode.type = ENET_QUEUE_MODE_TYPE_WFQ;
            entry.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
        }

        
        
        if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_CLUSTER_TYPE) == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) {
            entry.shaper_enable = ENET_FALSE;
        }
        else {
            entry.shaper_enable = ENET_FALSE;
        }
        
        entry.shaper_info.CIR = MEA_vpls_VPLSi_SHAPER_CIR;
        entry.shaper_info.CBS = MEA_vpls_VPLSi_SHAPER_CBS;
        
        entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
        entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

        entry.shaper_info.overhead = 0;
        entry.shaper_info.Cell_Overhead = 0;




        for (priQ = 0; priQ < MEA_NUM_OF_ELEMENTS(entry.pri_queues); priQ++) {
            if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
			   if(priQ == 7){
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_26(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_26(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_25(priQ);;
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
               } else {
			   
			    entry.pri_queues[priQ].max_q_size_Packets   = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_25(priQ);
                entry.pri_queues[priQ].max_q_size_Byte      = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_25(priQ);
                entry.pri_queues[priQ].mc_MQS               = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_25(priQ);;
                entry.pri_queues[priQ].mtu                  = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
			   
			   }
			}
            if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
			    if(priQ == 7){
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_26(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_26(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_26(priQ);;
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
               } else {
                entry.pri_queues[priQ].max_q_size_Packets   = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_25_EMB(priQ);
                entry.pri_queues[priQ].max_q_size_Byte      = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_25_EMB(priQ);
                entry.pri_queues[priQ].mc_MQS               = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_25_EMB(priQ);;
                entry.pri_queues[priQ].mtu                  = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
              }
			}
#if defined(HW_BOARD_IS_PCI)
            entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G(priQ);
            entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G(priQ);
            entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G(priQ);;
            entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;

#endif
            if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY) && (MEA_VPLS_MODE_PRI_QUEUE_STRICT == 1)) {
                entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_STRICT_DEF_VAL;
            }
            else {
                if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY)) {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
                else {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
            }

        }
        /* create the queue */

        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " port %d cluster id=%d\n", MEA_INTERNAL_PORT_DL_LEVEL2, clusterId);

        if (ENET_Create_Queue(ENET_UNIT_0,
            &entry,
            &clusterId) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Create_Queue failed (port=%d) clusterId %d\n",
                __FUNCTION__, entry.port.id.port, clusterId);
            return MEA_ERROR;
        }

        mea_vpls_groups_leval1[index].vp = clusterId;
        mea_vpls_groups_leval1[index].valid = MEA_TRUE;
    }

    maxGroup = 0;
  
    //maxGroup = MEA_VPLS_MAX_OF_CY_GROUP - 1;
    maxGroup = (MEA_VPLS_MAX_OF_FLOODING / MEA_VPLS_MAX_OF_CY_GROUP) - 1;
  
    index = 1;
    for (clusterId = 30; clusterId < 30 + maxGroup; clusterId++, index++) {
        /* Init the entry */
        MEA_OS_memset(&entry, 0, sizeof(entry));
        MEA_OS_strcpy(entry.name, ENET_PLAT_GENERATE_NEW_NAME);
        entry.adminOn = MEA_TRUE;

        entry.port.type = ENET_QUEUE_PORT_TYPE_PORT;
        entry.groupId = 1;
        if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
            entry.groupId = 0;
        }

        entry.port.id.port = (ENET_PortId_t)(MEA_INTERNAL_PORT_DL_LEVEL2);
            

        if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
            entry.mode.type = ENET_QUEUE_MODE_TYPE_RR;
            entry.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL; // because fpga use this
        }
        else{
            entry.mode.type = ENET_QUEUE_MODE_TYPE_WFQ;
            entry.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
        }


        if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_CLUSTER_TYPE) == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) {
            entry.shaper_enable = ENET_FALSE;
        }
        else {
            entry.shaper_enable = ENET_FALSE;
        }
            entry.shaper_info.CIR = MEA_vpls_VPLSi_SHAPER_CIR;
            entry.shaper_info.CBS = MEA_vpls_VPLSi_SHAPER_CBS;

            entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
            entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

            entry.shaper_info.overhead = 0;
            entry.shaper_info.Cell_Overhead = 0;


       
        for (priQ = 0; priQ < MEA_NUM_OF_ELEMENTS(entry.pri_queues); priQ++) {
            if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_25(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_25(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_25(priQ);;
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
            }
            if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_25_EMB(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_25_EMB(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_25_EMB(priQ);;
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
            }
#if defined(HW_BOARD_IS_PCI)
            entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G(priQ);
            entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G(priQ);
            entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G(priQ);;
            entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;

#endif
            
            if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY) && (MEA_VPLS_MODE_PRI_QUEUE_STRICT == 1)) {
                entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_STRICT_DEF_VAL;
            }
            else {
                if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY)) {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
                else {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
            }

        }
        /* create the queue */
        
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " port %d cluster id=%d\n", MEA_INTERNAL_PORT_DL_LEVEL2, clusterId);

        if (ENET_Create_Queue(ENET_UNIT_0,
            &entry,
            &clusterId) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Create_Queue failed (port=%d) clusterId %d\n",
                __FUNCTION__, entry.port.id.port, clusterId);
              return MEA_ERROR; 
        }

        mea_vpls_groups_leval1[index].vp = clusterId;
        mea_vpls_groups_leval1[index].valid = MEA_TRUE;
    }


    /************************************************************************/
    /*                                                                      */
    /************************************************************************/

    ENET_Delete_Queue(MEA_UNIT_0, MEA_INTERNAL_PORT_DL_LEVEL3_27); /*Delete the default queue on port*/

    index = 0;
    {
        index = 0;
        clusterId = MEA_INTERNAL_PORT_DL_LEVEL3_27;

        /* Init the entry */
        MEA_OS_memset(&entry, 0, sizeof(entry));
        MEA_OS_strcpy(entry.name, ENET_PLAT_GENERATE_NEW_NAME);
        entry.adminOn = MEA_TRUE;
        entry.groupId = 1;
        entry.port.type = ENET_QUEUE_PORT_TYPE_PORT;
        entry.port.id.port = (ENET_PortId_t)(MEA_INTERNAL_PORT_DL_LEVEL3_27);



        if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
            entry.groupId = 0;
        }
       

        if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
            entry.mode.type = ENET_QUEUE_MODE_TYPE_RR;
            entry.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL; // because fpga use this
        }
        else{
            entry.mode.type = ENET_QUEUE_MODE_TYPE_WFQ;
            entry.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
        }


        if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_CLUSTER_TYPE) == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) {
            entry.shaper_enable = MEA_FALSE;
        }
        else {
            entry.shaper_enable = ENET_FALSE;
        }
#if 0
        entry.shaper_info.CIR = MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3_27;
        entry.shaper_info.CBS = MEA_vpls_VPLSi_SHAPER_CBS_LEVEL3_27;

        entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
        entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

        entry.shaper_info.overhead = 0;
        entry.shaper_info.Cell_Overhead = 0;
#endif


        for (priQ = 0; priQ < MEA_NUM_OF_ELEMENTS(entry.pri_queues); priQ++) {
            if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_27(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_27(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_27(priQ);;
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
            }
            if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_27_EMB(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_27_EMB(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_27_EMB(priQ);;
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
            }
#if defined(HW_BOARD_IS_PCI)
            entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G(priQ);
            entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G(priQ);
            entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G(priQ);;
            entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;

#endif
            if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY) && (MEA_VPLS_MODE_PRI_QUEUE_STRICT_LV3 == 1)) {
                entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_STRICT_DEF_VAL;
            }
            else {
                if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY)) {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
                else {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
            }

        }
        /* create the queue */

        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " port %d cluster id=%d\n", MEA_INTERNAL_PORT_DL_LEVEL2, clusterId);

        if (ENET_Create_Queue(ENET_UNIT_0,
            &entry,
            &clusterId) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Create_Queue failed (id=%d) port %d\n",
                __FUNCTION__, entry.port.id.port, clusterId);
            return MEA_ERROR;
        }
        mea_vpls_groups_leval2[index].vp = clusterId ;
        mea_vpls_groups_leval2[index].valid = MEA_TRUE;

    }// for
    
    
    maxGroup = MEA_VPLS_MAX_OF_CY_GROUP - 1;
    index = 1;
    for (clusterId = 64; clusterId < 64 + maxGroup; clusterId++, index++) {
        /* Init the entry */
        MEA_OS_memset(&entry, 0, sizeof(entry));
        MEA_OS_strcpy(entry.name, ENET_PLAT_GENERATE_NEW_NAME);
        entry.adminOn = MEA_TRUE;
        entry.groupId = 1;
        entry.port.type = ENET_QUEUE_PORT_TYPE_PORT;
        entry.port.id.port = (ENET_PortId_t)(MEA_INTERNAL_PORT_DL_LEVEL3_27);

        if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
            entry.groupId = 0;
        }
       
        
        

        if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
            entry.mode.type = ENET_QUEUE_MODE_TYPE_RR;
            entry.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL; // because fpga use this
        }
        else{
            entry.mode.type = ENET_QUEUE_MODE_TYPE_WFQ;
            entry.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
        }


        if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_CLUSTER_TYPE) == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) {
            entry.shaper_enable = MEA_FALSE;;
        }
        else {
            entry.shaper_enable = ENET_FALSE;
        }
#if 0
        entry.shaper_info.CIR = MEA_vpls_VPLSi_SHAPER_CIR_LEVEL3;
        entry.shaper_info.CBS = MEA_vpls_VPLSi_SHAPER_CBS_LEVEL3;

        entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
        entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;

        entry.shaper_info.overhead = 0;
        entry.shaper_info.Cell_Overhead = 0;
#endif


        for (priQ = 0; priQ < MEA_NUM_OF_ELEMENTS(entry.pri_queues); priQ++) {
            if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_27(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_27(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_27(priQ);;
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
            }
            if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
                entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_27_EMB(priQ);
                entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_27_EMB(priQ);
                entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_27_EMB(priQ);;
                entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
            }
#if defined(HW_BOARD_IS_PCI)
            entry.pri_queues[priQ].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G(priQ);
            entry.pri_queues[priQ].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G(priQ);
            entry.pri_queues[priQ].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G(priQ);;
            entry.pri_queues[priQ].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;

#endif

            if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY) && (MEA_VPLS_MODE_PRI_QUEUE_STRICT_LV3 == 1)) {
                entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
                entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_STRICT_DEF_VAL;
            }
            else {
                if ((MEA_GLOBAL_PRI_Q_MODE_DEF_VAL == MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY)) {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
                else {
                    entry.pri_queues[priQ].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ;
                    entry.pri_queues[priQ].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
                }
            }

        }
        /* create the queue */

        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " port %d cluster id=%d\n", MEA_INTERNAL_PORT_DL_LEVEL2, clusterId);

        if (ENET_Create_Queue(ENET_UNIT_0,
            &entry,
            &clusterId) != ENET_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - ENET_Create_Queue failed (id=%d) port %d\n",
                     __FUNCTION__, entry.port.id.port, clusterId);
            return MEA_ERROR;
        }
        mea_vpls_groups_leval2[index].vp = clusterId;
        mea_vpls_groups_leval2[index].valid = MEA_TRUE;

    }// for


    return MEA_OK;
}




MEA_Status mea_drv_VPLS_Init(MEA_Unit_t       unit_i)
{
    
    MEA_EgressHeaderProc_Entry_dbt entry_i;
   
    MEA_Editing_t                  EditId;
    MEA_Uint32                        i;

    MEA_Uint32 size;
    MEA_IngressPort_Entry_dbt entry;
    MEA_Parser_Entry_dbt parser_entry_val_def;


    if (!MEA_VPLS_SUPPORT)
        return MEA_OK;




    

    

    size = MEA_VPLS_MAX_OF_CY_GROUP * sizeof(mea_vpls_out_vp_dbt);
    if (size != 0) {
        mea_vpls_groups_leval1 = (mea_vpls_out_vp_dbt*)MEA_OS_malloc(size);
        if (mea_vpls_groups_leval1 == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_vpls_groups_leval1 failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(mea_vpls_groups_leval1[0]), 0, size);
    }

    size = MEA_VPLS_MAX_OF_CY_GROUP * sizeof(mea_vpls_out_vp_dbt);
    if (size != 0) {
        mea_vpls_groups_leval2 = (mea_vpls_out_vp_dbt*)MEA_OS_malloc(size);
        if (mea_vpls_groups_leval2 == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_vpls_groups_leval2 failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(mea_vpls_groups_leval2[0]), 0, size);
    }



   


    size = MEA_MAX_OF_VPLS_Handover * sizeof(mea_UEpdn_Handover_data_dbt);
    if (size != 0) {
        MEA_drv_UEPDN_VPLS_Handover_Table = (mea_UEpdn_Handover_data_dbt*)MEA_OS_malloc(size);
        if (MEA_drv_UEPDN_VPLS_Handover_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_drv_VPLS_Table failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_drv_UEPDN_VPLS_Handover_Table[0]), 0, size);
    }
    
    
    


    size = MEA_VPLS_MAX_OF_INSTANCE * sizeof(MEA_VPLS_entry_dbt);
    if (size != 0) {
        MEA_drv_VPLS_Table = (MEA_VPLS_entry_dbt*)MEA_OS_malloc(size);
        if (MEA_drv_VPLS_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_drv_VPLS_Table failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_drv_VPLS_Table[0]), 0, size);
    }



    if (MEA_device_environment_info.MEA_GW_VPLS_EN_ebable == MEA_TRUE){
        if (MEA_device_environment_info.MEA_GW_VPLS_EN == MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "User set Initialize VPLSi OFF \n");
            return MEA_OK;
        }
    }



    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize Update ..port.\n");
    {
        MEA_Port_t port;
        for (port = 0; port <= MEA_MAX_PORT_NUMBER; port++) {
            if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) == MEA_FALSE)
                continue;
            MEA_drv_Queue_UPDATE_ForPort(unit_i, port);
        }
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize VPLSi ..ON.\n");

/* Allocate PacketGen Table */

  
    mea_vpls_init_internalclusterLEVEL();
    /*Update  the port 24,25,26,27  to mask the internal Net2*/




    MEA_API_Get_IngressPort_Entry(unit_i,
        MEA_INTERNAL_PORT_DL_LEVEL2,
        &entry);

    entry.parser_info.net2_wildcard = 0xffffff;
    entry.parser_info.net2_wildcard_valid = MEA_TRUE;

    entry.parser_info.port_cos_aw = MEA_TRUE;
    entry.parser_info.port_ip_aw = MEA_TRUE;

    entry.parser_info.pri_wildcard_valid  = MEA_TRUE;
    entry.parser_info.pri_wildcard = 0xff;
    entry.parser_info.IP_pri_mask_type = 3;
    entry.parser_info.default_pri = 7;
    

    MEA_API_Set_IngressPort_Entry(unit_i,
        MEA_INTERNAL_PORT_DL_LEVEL2,
        &entry);

    if (MEA_API_Get_Parser_Entry(MEA_UNIT_0,
        MEA_INTERNAL_PORT_DL_LEVEL2,
        &parser_entry_val_def) != MEA_OK) {
        return MEA_ERROR;
    }

    parser_entry_val_def.f.pri_rule = 1;

    if (MEA_API_Set_Parser_Entry(MEA_UNIT_0,
        MEA_INTERNAL_PORT_DL_LEVEL2,
        &parser_entry_val_def) != MEA_OK) {
        return MEA_ERROR;
   }

/************************************************************************/
/*       MEA_INTERNAL_PORT_DL_LEVEL3                                                               */
/************************************************************************/

    MEA_API_Get_IngressPort_Entry(unit_i,
        MEA_INTERNAL_PORT_DL_LEVEL3,
        &entry);

    entry.parser_info.net2_wildcard = 0xffffff;
    entry.parser_info.net2_wildcard_valid = MEA_TRUE;

    entry.parser_info.pri_wildcard_valid = MEA_TRUE;
    entry.parser_info.pri_wildcard = 0xff;
    entry.parser_info.IP_pri_mask_type = 3;
    entry.parser_info.port_cos_aw = MEA_TRUE;
    entry.parser_info.port_ip_aw = MEA_TRUE;

    entry.parser_info.pri_wildcard_valid = MEA_TRUE;
    entry.parser_info.pri_wildcard = 0xff;
    entry.parser_info.IP_pri_mask_type = 3;

   MEA_API_Set_IngressPort_Entry(unit_i,
        MEA_INTERNAL_PORT_DL_LEVEL3,
        &entry);
   /************************************************************************/
   /*      MEA_INTERNAL_PORT_DL_LEVEL3_27                                  */
   /************************************************************************/ 

    MEA_API_Get_IngressPort_Entry(unit_i,
        MEA_INTERNAL_PORT_DL_LEVEL3_27,
        &entry);
    entry.parser_info.LAG_src_port_value = MEA_INTERNAL_PORT_DL_LEVEL3;
    entry.parser_info.LAG_src_port_valid = MEA_TRUE;


    entry.parser_info.net2_wildcard = 0xffffff;
    entry.parser_info.net2_wildcard_valid = MEA_TRUE;

    entry.parser_info.pri_wildcard_valid = MEA_TRUE;
    entry.parser_info.pri_wildcard = 0xff;
    entry.parser_info.IP_pri_mask_type = 3;
    entry.parser_info.port_cos_aw = MEA_TRUE;
    entry.parser_info.port_ip_aw = MEA_TRUE;

    entry.parser_info.pri_wildcard_valid = MEA_TRUE;
    entry.parser_info.pri_wildcard = 0xff;
    entry.parser_info.IP_pri_mask_type = 3;
    entry.parser_info.default_pri = 7;

    MEA_API_Set_IngressPort_Entry(unit_i,
        MEA_INTERNAL_PORT_DL_LEVEL3_27,
        &entry);

    if (MEA_API_Get_Parser_Entry(MEA_UNIT_0,
        MEA_INTERNAL_PORT_DL_LEVEL3_27,
        &parser_entry_val_def) != MEA_OK) {
        return MEA_ERROR;
    }

    parser_entry_val_def.f.pri_rule = 1;

    if (MEA_API_Set_Parser_Entry(MEA_UNIT_0,
        MEA_INTERNAL_PORT_DL_LEVEL3_27,
        &parser_entry_val_def) != MEA_OK) {
        return MEA_ERROR;
    }




    /////////////////////////////////////////////////////////////

    MEA_OS_memset(&entry_i, 0, sizeof(entry_i));





    for (i = 0; i < MEA_VPLS_MAX_OF_INSTANCE; i++){
        /*Vpls Instance*/

        entry_i.EditingType = MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_OFSET12;
        entry_i.eth_info.val.all = 0;
        entry_i.eth_info.val.all = (MEA_Uint32)(0x000001ff + ((MEA_Uint32)(i << MEA_VPLS_ED_ID_SHIFT)));
        EditId = 0;
        if (MEA_API_Set_EgressHeaderProc_Entry(MEA_UNIT_0,
            &entry_i,
            &EditId,
            MEA_FALSE) != MEA_OK){
            return MEA_ERROR;
        }
        MEA_drv_VPLS_EditId[i] = EditId;

    }


    entry_i.EditingType = MEA_EDITINGTYPE_MPLS_SWAP_MPLS_LABEL;
    entry_i.eth_info.val.all = (MEA_Uint32)(0x000001ff);

    EditId = 0;
    if (MEA_API_Set_EgressHeaderProc_Entry(MEA_UNIT_0,
        &entry_i,
        &EditId,
        MEA_FALSE) != MEA_OK){
        return MEA_ERROR;
    }
    MEA_drv_VPLS_EditId[MEA_VPLS_ED_SWAP] = EditId;
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Allocate Edit Id from  %d to %d for VPLSi\n", MEA_drv_VPLS_EditId[0], MEA_drv_VPLS_EditId[255]);

#if 0
/* Create ingress flow policer to set on the leaf Service */
    {
        mea_Ingress_Flow_policer_dbt Flow_policer_entry;
        vpls_ingressPolicerFlow = 0;
        if (MEA_IS_INGRESS_FLOW_POLICER_SUPPORT) {
            vpls_ingressPolicerFlow = MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE - 1;
            if (MEA_API_Get_Ingress_flow_policer(MEA_UNIT_0, vpls_ingressPolicerFlow, &Flow_policer_entry) != MEA_OK)
            {
                return MEA_ERROR;
            }

            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_MC].tmId = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_MC].tmId_enable =  MEA_PORT_RATE_MEETRING_ENABLE_DEF_VAL;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_MC].sla_params.CIR = 128000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_MC].sla_params.EIR = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_MC].sla_params.CBS = 32000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_MC].sla_params.EBS = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_MC].sla_params.cup = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_MC].sla_params.color_aware = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_MC].sla_params.comp = 0;

            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_BC].tmId = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_BC].tmId_enable = MEA_PORT_RATE_MEETRING_ENABLE_DEF_VAL;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_BC].sla_params.CIR = 128000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_BC].sla_params.EIR = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_BC].sla_params.CBS = 32000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_BC].sla_params.EBS = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_BC].sla_params.cup = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_BC].sla_params.color_aware = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_BC].sla_params.comp = 0;

            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST].tmId = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST].tmId_enable = MEA_PORT_RATE_MEETRING_DISABLE_DEF_VAL;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST].sla_params.CIR = 128000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST].sla_params.EIR = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST].sla_params.CBS = 32000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST].sla_params.EBS = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST].sla_params.cup = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST].sla_params.color_aware = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST].sla_params.comp = 0;

            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].tmId = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].tmId_enable = MEA_PORT_RATE_MEETRING_DISABLE_DEF_VAL; /**/
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.CIR = 128000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.EIR = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.CBS = 32000;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.EBS = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.cup = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.color_aware = 0;
            Flow_policer_entry.policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN].sla_params.comp = 0;

            if (MEA_API_Set_Ingress_flow_policer(MEA_UNIT_0, vpls_ingressPolicerFlow, &Flow_policer_entry) != MEA_OK) {
                
                return MEA_ERROR;
            }




        }
    }
#endif

    
    //Create profile Mapping 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Create FlowCoSMappingProfile \n");

        MEA_Uint16 Idx;
        MEA_FlowCoSMappingProfile_Entry_dbt 	entry_cos;
        MEA_FlowCoSMappingProfile_Id_t          cosMappingId; 

          
      
            /* Create a default Cos mapping profile in ENET*/
            cosMappingId = MEA_PLAT_GENERATE_NEW_ID;
            MEA_OS_memset(&entry_cos, 0, sizeof(MEA_FlowCoSMappingProfile_Entry_dbt));

            for (Idx = 0; Idx < MEA_FLOW_COS_MAPPING_MAX_NUM_OF_ITEMS; Idx++)
            {
                entry_cos.item_table[Idx].valid = 1;
                if (((Idx >> 3) & 0x7) == 0x7)
                    entry_cos.item_table[Idx].COS = 6;
                else
                   entry_cos.item_table[Idx].COS = (Idx >> 3) & 0x7;

                /* Color input to Policer 0-Green / 1-Yellow */
                entry_cos.item_table[Idx].COLOR = ((Idx & 0x01) == 0) ? 0 : 1;
            }
            entry_cos.type = MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_DSCP;
            if (MEA_API_Create_FlowCoSMappingProfile_Entry(MEA_UNIT_0, &entry_cos, &cosMappingId) != MEA_OK)
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "QoSHwInit ERROR: failed to call MEA_API_Create_FlowCoSMappingProfile_Entry\n");
                return MEA_ERROR;
            }

            VPLS_cosMappingId = cosMappingId;





    }







return MEA_OK;
}

MEA_Status mea_drv_VPLS_RInit(MEA_Unit_t       unit_i)
{

    if (!MEA_VPLS_SUPPORT)
        return MEA_OK;


    /*Update Handover*/
    if(mea_drv_UEPDN_Set_Handover_UpdateHW(unit_i) != MEA_OK){

    }
    
    
        
    
    
    return MEA_OK;
}
MEA_Status mea_drv_VPLS_Conclude(MEA_Unit_t       unit_i)
{
    
    
    if (b_bist_test){
        return MEA_OK;
    }

    if(!MEA_VPLS_SUPPORT)
        return MEA_OK;

  
    MEA_API_LOG

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude VPLS ..\n");


    if (mea_vpls_groups_leval2 != NULL) {
        MEA_OS_BigFree(mea_vpls_groups_leval2);
        mea_vpls_groups_leval2 = NULL;
        
    }

    if (mea_vpls_groups_leval1 != NULL) {
        MEA_OS_BigFree(mea_vpls_groups_leval1);
        mea_vpls_groups_leval1 = NULL;
        
    }

   

    /* Free the table */
    if (MEA_drv_VPLS_Table != NULL) {
        if (MEA_drv_VPLS_Table->servicelevel2Group != NULL) {
            MEA_OS_free(MEA_drv_VPLS_Table->servicelevel2Group);
            MEA_drv_VPLS_Table->servicelevel2Group = NULL;
                
        }
        if (MEA_drv_VPLS_Table->data_info_ue_pdn_dl != NULL) {
            MEA_OS_free(MEA_drv_VPLS_Table->data_info_ue_pdn_dl);
            MEA_drv_VPLS_Table->data_info_ue_pdn_dl = NULL;
            
        }



        MEA_OS_free(MEA_drv_VPLS_Table);
        MEA_drv_VPLS_Table = NULL;
    }

    

    if (MEA_drv_UEPDN_VPLS_Handover_Table != NULL) {
        MEA_OS_free(MEA_drv_UEPDN_VPLS_Handover_Table);
        MEA_drv_UEPDN_VPLS_Handover_Table = NULL;
    }

    



 

    return MEA_OK;
}




static MEA_Bool mea_vpls_drv_Profile_IsRange(MEA_Uint16 id_i){

    if ((id_i) >= MEA_VPLS_MAX_OF_INSTANCE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - out range \n", __FUNCTION__);
        return MEA_FALSE;
    }
    return MEA_TRUE;
}


/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status mea_drv_vplsi_Get_outport(MEA_Uint16 vpls_Ins_Id, MEA_OutPorts_Entry_dbt     *OutPorts)
{
    if (!MEA_VPLS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " MEA_VPLS_SUPPORT not support on this HW\n");
        return MEA_ERROR;
    }
    if (OutPorts == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " OutPorts == NULL\n");
        return MEA_ERROR;
    }
    MEA_OS_memcpy(OutPorts, &MEA_drv_VPLS_Table[vpls_Ins_Id].OutPorts, sizeof(MEA_OutPorts_Entry_dbt));
   

    return MEA_OK;
}

MEA_Status mea_drv_vpls_Service_vlan_Level1_GroupX(MEA_Uint16 vpls_Ins_Id, MEA_Uint16 group, MEA_Service_t *Service_id)
{


    MEA_Uint16  i;

    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;

    MEA_EHP_Info_dbt                      Service_editing_info[1]; //32
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;

    MEA_Service_Entry_Data_dbt first_Service_data;
    MEA_Policer_Entry_dbt       first_Policer_entry;



    /** Apply the obtained parameters to the specified Service */
    MEA_OS_memset(&Service_key, 0, sizeof(Service_key));
    MEA_OS_memset(&Service_data, 0, sizeof(Service_data));
    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    MEA_OS_memset(&Service_policer, 0, sizeof(Service_policer));
    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));
    MEA_OS_memset(&Service_editing_info, 0, sizeof(Service_editing_info));


    MEA_OS_memset(&first_Service_data, 0, sizeof(first_Service_data));
    MEA_OS_memset(&first_Policer_entry, 0, sizeof(first_Policer_entry));

    Service_editing.ehp_info = &Service_editing_info[0];



    Service_key.ClassifierKEY = 0;
    Service_key.src_port = MEA_INTERNAL_PORT_DL_LEVEL2;

    Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_MPLS_Single_label_no_tag;
   
    
    switch (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.SM_mode)
    {
 
   
    case MEA_CHARACTER_TYPE_TEID_MODE_Vlan_To_Vxlan:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM1:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM1a:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM2:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM3:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM4:

        Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_MPLS_OUTER_VLAN;
    	break;

    case MEA_CHARACTER_TYPE_TEID_MODE_SM5:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM2a:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM2b:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM3a:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM5a:
        Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_MPLS_OUTER_TWO_VLAN;
        break;
    default:
        Service_key.sub_protocol_type = 0; /*no Vlan*/
        break;

    }

       

    Service_key.net_tag = 0x00000 | ((vpls_Ins_Id << MEA_VPLS_ID_SHIFT) | (group & 0x1f) << MEA_VPLS_GROUP_SHIFT);


    Service_key.pri = 0x3f; //7; /* DC */
    Service_key.priType = 3;

    Service_key.inner_netTag_from_valid = MEA_TRUE;
    Service_key.inner_netTag_from_value = 0xffffff;



    /* Build the  attributes for the forwarder  */
    Service_data.vpn = 0;
    Service_data.DSE_forwarding_enable = MEA_FALSE;
    Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    Service_data.flood_ED = MEA_TRUE;

    
    if (group != 0){
        
       //Service_data.tmId = MEA_drv_VPLS_Table[vpls_Ins_Id].tmId;
        /* Set the upstream service data attribute for pm (counters) */
            Service_data.pmId = MEA_drv_VPLS_Table[vpls_Ins_Id].VPLS_Pmid;
            Service_data.tmId = MEA_drv_VPLS_Table[vpls_Ins_Id].VPLS_tmId;
    }else{
        Service_data.tmId = 0;
        /* Set the upstream service data attribute for pm (counters) */
        Service_data.pmId = 0;
    }

	Service_data.tmId_disable = MEA_vpls_VPLSi_MEETRING_DISABLE;
    
    Service_data.COS_FORCE = MEA_TRUE;
    Service_data.COS = 7;

    /* Set the upstream service data attribute for policing */
    Service_data.editId = 0; /* generate new id */

    MEA_OS_memset(&Service_policer, 0, sizeof(Service_policer));//
    
    MEA_OS_memset(&Service_policer, 0, sizeof(Service_policer));//

    Service_policer.CIR = MEA_vpls_VPLSi_POLICER_CIR;
    Service_policer.CBS = MEA_vpls_VPLSi_POLICER_CBS;
    Service_policer.EIR = 0;
    Service_policer.EBS = 0;
    Service_policer.comp = 0;
	Service_policer.gn_type = MEA_vpls_VPLSi_POLICER_PGT;

    /* Build the Upstream Service editing */
    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));

    Service_editing.ehp_info = Service_editing_info;


    Service_editing.num_of_entries = 1;
    Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_MPLS_SWAP_MPLS_LABEL;
    Service_editing.ehp_info[0].ehp_data.eth_info.val.all = (MEA_Uint32)(0x000001ff);

    Service_data.editId = MEA_drv_VPLS_EditId[MEA_VPLS_ED_SWAP];
   


    /* Build the upstream service outPorts */

    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    for (i = 0; i < MEA_VPLS_MAX_OF_CY_GROUP ; i++){
        MEA_SET_OUTPORT(&Service_outPorts, mea_vpls_groups_leval2[i].vp);
    }

    /* Create new service for the given . */
    if (MEA_API_Create_Service(MEA_UNIT_0,
        &Service_key,
        &Service_data,
        &Service_outPorts,
        &Service_policer,
        &Service_editing,
        Service_id) != MEA_OK){

        return MEA_ERROR;
    }


			
    if (group == 0){
        /*Save the info of TM and Pmid */
        MEA_drv_VPLS_Table[vpls_Ins_Id].VPLS_tmId = Service_data.tmId;
        Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL; /* No upstream policing */
        /* Set the upstream service data attribute for pm (counters) */
        MEA_drv_VPLS_Table[vpls_Ins_Id].VPLS_Pmid = Service_data.pmId;

    }


    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "VPLS port %d  sid %d  ed %d \n", Service_key.src_port, *Service_id, Service_data.editId);

    return MEA_OK;
}


MEA_Status MEA_API_VPLSi_Create_Entry(MEA_Unit_t  unit, MEA_Uint16 *vpls_Ins_Id, MEA_VPLS_dbt* entry)
{
    MEA_Uint16 i;
    MEA_Uint32 group;
    MEA_Service_t    Service_id = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Uint32 numof_group;
    MEA_Uint32 size;
    MEA_Uint32 maxGroup;

    if (!MEA_VPLS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_VPLS_SUPPORT not support on this HW\n");  
        return MEA_ERROR;
    }


    /*-----------------------------*/
    /*                             */
    /*-----------------------------*/
    if ((*vpls_Ins_Id) == MEA_PLAT_GENERATE_NEW_ID){
        for (i = 0; i < MEA_VPLS_MAX_OF_INSTANCE; i++){
            if (MEA_drv_VPLS_Table[i].valid == MEA_FALSE){
                *vpls_Ins_Id = i;
                break;
            }
        }
        if (i >= MEA_VPLS_MAX_OF_INSTANCE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - No available Id \n", __FUNCTION__);
           
            return MEA_ERROR;
        }
    } else {
        if (mea_vpls_drv_Profile_IsRange((*vpls_Ins_Id)) != MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - No available Id == NULL\n", __FUNCTION__);
           
            return MEA_ERROR;
        }
        if (MEA_drv_VPLS_Table[*vpls_Ins_Id].valid == MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id %d existing\n", __FUNCTION__, *vpls_Ins_Id);
            
            return MEA_ERROR;
        }
    }
    
    if (entry->max_of_fooding == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - max_of_fooding  can't be Zero \n", __FUNCTION__);

        return MEA_ERROR;
    }

    

    if (entry->max_of_fooding > MEA_VPLS_MAX_OF_FLOODING){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - max_of_fooding  have max limit %d\n", __FUNCTION__, MEA_VPLS_MAX_OF_FLOODING);

        return MEA_ERROR;
    }
    if (entry->SM_mode == MEA_CHARACTER_TYPE_TEID_MODE_NONE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mast to get SM mode  %d  cat be set \n", __FUNCTION__, entry->SM_mode);

        return MEA_ERROR;
    }
    if (entry->max_of_fooding % MEA_VPLS_MAX_OF_CY_GROUP != 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "- max_of_fooding should be divided into %d \n", MEA_VPLS_MAX_OF_CY_GROUP);
        return MEA_ERROR;
    }

    /************************************************************************/
    /*                                                                      */
    /************************************************************************/

    size = MEA_VPLS_MAX_OF_FLOODING*sizeof(mea_drv_vpls_ue_pdn_dl);
    MEA_drv_VPLS_Table[*vpls_Ins_Id].data_info_ue_pdn_dl = (mea_drv_vpls_ue_pdn_dl*)MEA_OS_malloc(size);
    if (MEA_drv_VPLS_Table[*vpls_Ins_Id].data_info_ue_pdn_dl == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_drv_VPLS_Table[i].data_info_ue_pdn_dl failed (size=%d)\n",
            __FUNCTION__, size);
        return MEA_ERROR;
    }
    MEA_OS_memset(&(MEA_drv_VPLS_Table[*vpls_Ins_Id].data_info_ue_pdn_dl[0]), 0, size);


    size = MEA_VPLS_MAX_OF_CY_GROUP*sizeof(MEA_Service_t);
    MEA_drv_VPLS_Table[*vpls_Ins_Id].servicelevel2Group = (MEA_Service_t*)MEA_OS_malloc(size);
    if (MEA_drv_VPLS_Table[*vpls_Ins_Id].servicelevel2Group == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_drv_VPLS_Table[%d].servicelevel2Group (size=%d)\n",
            __FUNCTION__, *vpls_Ins_Id, size);
        if (MEA_drv_VPLS_Table[*vpls_Ins_Id].data_info_ue_pdn_dl)
            MEA_OS_free(MEA_drv_VPLS_Table[*vpls_Ins_Id].data_info_ue_pdn_dl);

        return MEA_ERROR;
    }

    


    MEA_OS_memset(&(MEA_drv_VPLS_Table[*vpls_Ins_Id].servicelevel2Group[0]), 0, size);



    MEA_OS_memcpy(&MEA_drv_VPLS_Table[*vpls_Ins_Id].data_info, entry, sizeof(MEA_VPLS_dbt));

#if 0 
      numof_group = MEA_drv_VPLS_Table[*vpls_Ins_Id].data_info.max_of_fooding / MEA_VPLS_MAX_OF_CY_GROUP;
#else
    numof_group = MEA_VPLS_MAX_OF_FLOODING/MEA_VPLS_MAX_OF_CY_GROUP;

#endif

//       if (MEA_drv_VPLS_Table[*vpls_Ins_Id].data_info.max_of_fooding % MEA_VPLS_MAX_OF_CY_GROUP != 0)
//           numof_group++;

      for (group = 0; group <numof_group; group++)
      {
          Service_id = MEA_PLAT_GENERATE_NEW_ID;
          if (mea_drv_vpls_Service_vlan_Level1_GroupX(*vpls_Ins_Id, group, &Service_id) != MEA_OK){
              /* delete All the services one instance */
              MEA_drv_VPLS_Table[*vpls_Ins_Id].valid = MEA_TRUE; /*dummy */
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " one the VPLSi %d service group%d fail  all the  all service are delete\n", vpls_Ins_Id, group);
              MEA_API_VPLSi_Delete_Entry(unit, *vpls_Ins_Id);
              MEA_drv_VPLS_Table[*vpls_Ins_Id].valid = MEA_FALSE; /* */
              return MEA_ERROR;
          }
          
          
          MEA_drv_VPLS_Table[*vpls_Ins_Id].servicelevel2Group[group] = Service_id;
      }


      MEA_OS_memset(&MEA_drv_VPLS_Table[*vpls_Ins_Id].OutPorts, 0, sizeof(MEA_drv_VPLS_Table[*vpls_Ins_Id].OutPorts));
      

#if 0
      maxGroup = (MEA_VPLS_MAX_OF_FLOODING / MEA_VPLS_MAX_OF_CY_GROUP);
#else
      maxGroup = MEA_drv_VPLS_Table[*vpls_Ins_Id].data_info.max_of_fooding / MEA_VPLS_MAX_OF_CY_GROUP;
#endif
    for (group = 0; group < maxGroup; group++)
    {
        MEA_SET_OUTPORT(&MEA_drv_VPLS_Table[*vpls_Ins_Id].OutPorts, mea_vpls_groups_leval1[group].vp);
    }



    MEA_drv_VPLS_Table[*vpls_Ins_Id].valid = MEA_TRUE;



return MEA_OK;
}


MEA_Status MEA_drv_VPLS_Service(MEA_Uint8 vpls_Ins_Id, MEA_Service_t serviceId,MEA_Port_t src_port)
{
         


    if (src_port == 24)
        MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceLocaSwitch = serviceId;
    else
        MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceDownlink = serviceId;

    return MEA_OK;
}



MEA_Status MEA_API_VPLSi_Add_Group(MEA_Unit_t  unit, MEA_Uint16 vpls_Ins_Id)
{


    MEA_Uint32 group;
    //MEA_Service_t    Service_id = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Uint32 numof_group;

    MEA_Service_Entry_Data_dbt            service_data;
    MEA_Policer_Entry_dbt                 Policer_entry;
    MEA_OutPorts_Entry_dbt                OutPorts_Entry;
    MEA_EgressHeaderProc_Array_Entry_dbt  EHP_Entry;
    MEA_EHP_Info_dbt 					ehp_info[1];
    MEA_Bool      Service_exist = MEA_FALSE;


    if (MEA_drv_VPLS_Table[vpls_Ins_Id].valid == MEA_FALSE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " VPLSi %d is not Valid \n", vpls_Ins_Id);
        return MEA_ERROR;
    }
    if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding == MEA_VPLS_MAX_OF_FLOODING)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " VPLSi %d is have full of flooding \n", vpls_Ins_Id);
        return MEA_ERROR;
    }

#if 0
    numof_group = MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding / MEA_VPLS_MAX_OF_CY_GROUP;
    if ((MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding + MEA_VPLS_MAX_OF_CY_GROUP) >
        MEA_VPLS_MAX_OF_FLOODING)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " VPLSi %d can't add group \n", vpls_Ins_Id);
        return MEA_ERROR;
    }

    for (group = numof_group; group < numof_group + 1; group++)
    {
        Service_id = MEA_PLAT_GENERATE_NEW_ID;
        if (mea_drv_vpls_Service_vlan_Level1_GroupX(vpls_Ins_Id, group, &Service_id) != MEA_OK) {

            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " one the VPLSi %d service group%d fail service can't created\n", vpls_Ins_Id, group);
            return MEA_ERROR;
        }


        MEA_drv_VPLS_Table[vpls_Ins_Id].servicelevel2Group[group] = Service_id;
    }

#if 0
    MEA_OS_memset(&MEA_drv_VPLS_Table[vpls_Ins_Id].OutPorts, 0, sizeof(MEA_drv_VPLS_Table[vpls_Ins_Id].OutPorts));

    for (group = numof_group; group < numof_group + 1; group++)
    {
        MEA_SET_OUTPORT(&MEA_drv_VPLS_Table[vpls_Ins_Id].OutPorts, mea_vpls_groups_leval1[group].vp);
    }
#endif
#endif
    
    
    MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding += MEA_VPLS_MAX_OF_CY_GROUP;

#if 1
    numof_group = MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding / MEA_VPLS_MAX_OF_CY_GROUP;

    for (group = 0; group < numof_group; group++)
    {
        MEA_SET_OUTPORT(&MEA_drv_VPLS_Table[vpls_Ins_Id].OutPorts, mea_vpls_groups_leval1[group].vp);
    }
#endif


#if 1 
    
        MEA_OS_memset(&service_data, 0, sizeof(service_data));
        MEA_OS_memset(&OutPorts_Entry, 0, sizeof(OutPorts_Entry));
        MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));
        MEA_OS_memset(&ehp_info, 0, sizeof(MEA_EHP_Info_dbt)*1);

    if (MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceDownlink != 0) {
        /*check if the service is valid */
        MEA_API_IsExist_Service_ById(unit,
            MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceDownlink,
            &Service_exist);
        /*update the service */
        if (Service_exist == MEA_TRUE) {
          
            EHP_Entry.ehp_info = &ehp_info[0];
            EHP_Entry.num_of_entries = 1;

            if(MEA_API_Get_Service(unit,
                MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceDownlink,
                NULL,
                &service_data,
                &OutPorts_Entry,
                &Policer_entry,
                &EHP_Entry) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " VPLSi %d is MEA_API_Get_Service \n", vpls_Ins_Id);
                return MEA_ERROR;

            }

            MEA_OS_memset(&ehp_info, 0, sizeof(MEA_EHP_Info_dbt) * 1);
            EHP_Entry.ehp_info = &ehp_info[0];
            EHP_Entry.num_of_entries = 1;


            if(MEA_API_Get_Service(unit,
                MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceDownlink,
                NULL,
                &service_data,
                &OutPorts_Entry,
                &Policer_entry,
                &EHP_Entry) != MEA_OK){
           
                return MEA_ERROR;
               }

            MEA_OS_memset(&ehp_info, 0, sizeof(MEA_EHP_Info_dbt) * 1);
            EHP_Entry.ehp_info = &ehp_info[0];
            EHP_Entry.num_of_entries = 1;
            service_data.editId = 0;

            if (MEA_API_Set_Service(unit,
                MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceDownlink,
                &service_data,
                &OutPorts_Entry,
                &Policer_entry,
                &EHP_Entry) != MEA_OK) {
                           return MEA_ERROR;
               }
            

        }
        else {
            MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceDownlink = 0;
        }
    
    
    }



    
    Service_exist = MEA_FALSE;
    /*service from port 24*/
    if (MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceLocaSwitch != 0) {
        /*check if the service is valid */
        MEA_API_IsExist_Service_ById(unit,
            MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceLocaSwitch,
            &Service_exist);
          /*update the service */
        if (Service_exist == MEA_TRUE) {
            MEA_OS_memset(&service_data, 0, sizeof(service_data));
            MEA_OS_memset(&OutPorts_Entry, 0, sizeof(OutPorts_Entry));
            MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));
            MEA_OS_memset(&ehp_info, 0, sizeof(MEA_EHP_Info_dbt) * 1);

            MEA_OS_memset(&ehp_info, 0, sizeof(MEA_EHP_Info_dbt) * 1);
            EHP_Entry.ehp_info = &ehp_info[0];
            EHP_Entry.num_of_entries = 1;
            service_data.editId = 0;


            if (MEA_API_Get_Service(unit,
                MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceLocaSwitch,
                NULL,
                &service_data,
                &OutPorts_Entry,
                &Policer_entry,
                &EHP_Entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " VPLSi %d is MEA_API_Get_Service \n", vpls_Ins_Id);
                return MEA_ERROR;

            }


            MEA_OS_memset(&ehp_info, 0, sizeof(MEA_EHP_Info_dbt) * 1);
            EHP_Entry.ehp_info = &ehp_info[0];
            EHP_Entry.num_of_entries = 1;
            service_data.editId = 0;


            if (MEA_API_Get_Service(unit,
                MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceLocaSwitch,
                NULL,
                &service_data,
                &OutPorts_Entry,
                &Policer_entry,
                &EHP_Entry) != MEA_OK) {
                MEA_OS_free(EHP_Entry.ehp_info);
                return MEA_ERROR;
            }

            MEA_OS_memset(&ehp_info, 0, sizeof(MEA_EHP_Info_dbt) * 1);
            EHP_Entry.ehp_info = &ehp_info[0];
            EHP_Entry.num_of_entries = 1;
            service_data.editId = 0;

            service_data.editId = 0;

            if (MEA_API_Set_Service(unit,
                MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceLocaSwitch,
                &service_data,
                &OutPorts_Entry,
                &Policer_entry,
                &EHP_Entry) != MEA_OK) {
                return MEA_ERROR;
            }
        }
        else {
            MEA_drv_VPLS_Table[vpls_Ins_Id].ServiceLocaSwitch = 0;
        }
    
    
    }



#endif

        
    return MEA_OK;
}


MEA_Status MEA_API_VPLSi_Delete_Entry(MEA_Unit_t  unit, MEA_Uint16 vpls_Ins_Id)
{
    MEA_Uint16 i;
    MEA_Uint32 group, numof_group;
   

    

    if (!MEA_VPLS_SUPPORT){
              return MEA_OK;
    }

    if (MEA_drv_VPLS_Table[vpls_Ins_Id].valid != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id %d is not existing\n", __FUNCTION__, vpls_Ins_Id);
        return MEA_ERROR;
    }
   
    if (MEA_drv_VPLS_Table[vpls_Ins_Id].Vlan_serviceId != 0){
        if (MEA_API_Delete_Service(unit,MEA_drv_VPLS_Table[vpls_Ins_Id].Vlan_serviceId) != MEA_OK){

        }
    }
    
    numof_group = MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding / MEA_VPLS_MAX_OF_CY_GROUP;
//     if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding % 32 != 0)
//         numof_group++;

    for (group = 0; group < numof_group; group++)
    {
        if (MEA_drv_VPLS_Table[vpls_Ins_Id].servicelevel2Group[group] == 0)
            continue;

        if (MEA_API_Delete_Service(unit, MEA_drv_VPLS_Table[vpls_Ins_Id].servicelevel2Group[group]) != MEA_OK){
        }
        MEA_drv_VPLS_Table[vpls_Ins_Id].servicelevel2Group[group] = 0;

    }

    for (i = 0; i < MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding; i++)
    {
        if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[i].DL_ServiceId != 0){
            if (MEA_API_Delete_Service(unit, MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[i].DL_ServiceId) != MEA_OK){

            }
            MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[i].DL_ServiceId = 0;
            
            if (MEA_drv_VPLS_Table[vpls_Ins_Id].numof_ue_pdn == 0){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - numof_ue_pdn == Zero\n", __FUNCTION__);
            }
            MEA_drv_VPLS_Table[vpls_Ins_Id].numof_ue_pdn--;

        }
    }

    
   
    if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl != NULL)
        MEA_OS_free(MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl);

    if(MEA_drv_VPLS_Table[vpls_Ins_Id].servicelevel2Group!=NULL)
        MEA_OS_free(MEA_drv_VPLS_Table[vpls_Ins_Id].servicelevel2Group);

    MEA_OS_memset(&MEA_drv_VPLS_Table[vpls_Ins_Id], 0, sizeof(MEA_VPLS_entry_dbt));

    return MEA_OK;
}


MEA_Status MEA_API_VPLSi_isExist(MEA_Unit_t  unit, MEA_Uint16 vpls_Ins_Id,MEA_Bool *exist)
{
    if (!MEA_VPLS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " MEA_VPLS_SUPPORT not support \n");
        return MEA_ERROR;
    }
    if (exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " exist == NULL \n");
        return MEA_ERROR;
    }

    if (vpls_Ins_Id >= MEA_VPLS_MAX_OF_INSTANCE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " vpls_Ins_Id is out of range \n");
        return MEA_ERROR;
    }

    if (MEA_drv_VPLS_Table[vpls_Ins_Id].valid == MEA_TRUE){
        *exist = MEA_TRUE;
    }
    else{
        *exist = MEA_FALSE;
    }
    return MEA_OK;

}

MEA_Status MEA_API_VPLSi_Get_Entry(MEA_Unit_t         unit,
                                  MEA_Uint16         vpls_Ins_Id,
                                  MEA_VPLS_dbt*      entry)
{
    if(!MEA_VPLS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," MEA_VPLS_SUPPORT not support \n");  
        return MEA_ERROR;
    }
    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " entry = NULL \n");
        return MEA_ERROR;
    }
    if (vpls_Ins_Id >= MEA_VPLS_MAX_OF_INSTANCE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " vpls_Ins_Id %d is out of range  \n", vpls_Ins_Id);
        return MEA_ERROR;
    }
    if (MEA_drv_VPLS_Table[vpls_Ins_Id].valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " vpls_Ins_Id %d is not valid  \n", vpls_Ins_Id);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry ,&MEA_drv_VPLS_Table[vpls_Ins_Id].data_info, sizeof(MEA_VPLS_dbt));



    return MEA_OK;
}



MEA_Status mea_drv_Create_UE_PDN_dl(MEA_Uint16             vpls_Ins_Id, 
                                    MEA_Uint16             leafId,
                                    MEA_VPLS_UE_Pdn_dl_dbt *entry, 
                                    MEA_Service_t          *Service_id)
{

    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    
    MEA_EHP_Info_dbt                      Service_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Action_Entry_Data_dbt             Action_data_po;
    MEA_Policer_Entry_dbt                 Action_policer;
    MEA_Uint32 my_leaf;
    MEA_Uint32 my_group;


    if (vpls_Ins_Id >= MEA_VPLS_MAX_OF_INSTANCE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The VPLS Instance is out of range \n");
        return MEA_ERROR;
    }

    /** Apply the obtained parameters to the specified Service */
    MEA_OS_memset(&Service_key, 0, sizeof(Service_key));
    MEA_OS_memset(&Service_data, 0, sizeof(Service_data));
    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    MEA_OS_memset(&Service_policer, 0, sizeof(Service_policer));
    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));
    MEA_OS_memset(&Service_editing_info, 0, sizeof(Service_editing_info));
    MEA_OS_memset(&Action_data_po, 0, sizeof(Action_data_po));
    MEA_OS_memset(&Action_policer, 0, sizeof(Action_policer));

    
    Service_editing.num_of_entries = 1;
    Service_editing.ehp_info = &Service_editing_info[0];

    if (entry->ActionId != 0 && entry->en_service_TFT == MEA_TRUE) {
    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR ActionId != 0 && entry->en_service_TFT  only one can be set \n", entry->ActionId);
        return MEA_ERROR;
    }
    if (entry->Access_port_id == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR Access_port_id = 0  can't be set \n", entry->ActionId);
        return MEA_ERROR;
    }


    if (entry->ActionId != 0) {

        /*get info of the Action */
        if (MEA_API_Get_Action(MEA_UNIT_0,
            entry->ActionId,
            &Action_data_po,
            NULL,
            NULL,
            &Service_editing) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR MEA_API_Get_Action Id= %d Fail 1\n", entry->ActionId);
            return MEA_ERROR;
        }
        if (MEA_API_Get_Action(MEA_UNIT_0,
            entry->ActionId,
            &Action_data_po,
            NULL,
            &Action_policer,
            &Service_editing) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR MEA_API_Get_Action Id= %d Fail 1\n", entry->ActionId);
            return MEA_ERROR;
        }
    }
    else {
        if (entry->en_service_TFT == MEA_TRUE) {
            if (entry->Service_Entry_data == NULL ||
                entry->EHP_Entry == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR EHP_Entry Or Service_Entry is NULL \n");
                return MEA_ERROR;

            }
        }

    }
 

    Service_key.ClassifierKEY = 0;
    Service_key.src_port = MEA_INTERNAL_PORT_DL_LEVEL3;



    Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_MPLS_Single_label_no_tag;

    switch (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.SM_mode)
    {


    case MEA_CHARACTER_TYPE_TEID_MODE_Vlan_To_Vxlan:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM1:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM1a:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM2:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM3:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM4:

        Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_MPLS_OUTER_VLAN;
        break;

    case MEA_CHARACTER_TYPE_TEID_MODE_SM5:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM2a:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM2b:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM3a:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM5a:
        Service_key.sub_protocol_type = MEA_PARSING_L2_Sub_MPLS_OUTER_TWO_VLAN;
        break;
    default:
        Service_key.sub_protocol_type = 0; /*no Vlan*/
        break;

    }

    if (MEA_VPLS_MAX_OF_USER_ON_GROUP == MEA_VPLS_MAX_OF_USER_ON_GROUP_Z45) {
        Service_key.net_tag = 0x00000 | (vpls_Ins_Id << MEA_VPLS_ID_SHIFT) | leafId;
    }
    else {
        my_leaf = leafId & (MEA_VPLS_MAX_OF_USER_ON_GROUP_Z15 - 1);
        my_group = leafId / MEA_VPLS_MAX_OF_CY_GROUP_Z15;
        
     
       // Service_key.net_tag = 0x00000 | (vpls_Ins_Id << MEA_VPLS_ID_SHIFT) | leafId;
        Service_key.net_tag = 0x00000 | (vpls_Ins_Id << MEA_VPLS_ID_SHIFT) | (my_group <<MEA_VPLS_GROUP_SHIFT) | my_leaf;

    }


    Service_key.pri = 0x3f; //7; /* DC */
    Service_key.priType = 3;


    Service_key.inner_netTag_from_valid = MEA_TRUE;
    Service_key.inner_netTag_from_value = 0xffffff;
   

    
    /* Build the  attributes for the forwarder  */
    Service_data.FWD_DaMAC_valid = MEA_TRUE;
    if (entry->en_service_TFT == MEA_FALSE) {
        if(Service_editing.ehp_info[0].ehp_data.ETHCS_DL_info.valid == MEA_TRUE)
            MEA_OS_maccpy(&Service_data.FWD_DaMAC_value, &Service_editing.ehp_info[0].ehp_data.ETHCS_DL_info.Da_eNB);  /*-fwd_internal */
        if (Service_editing.ehp_info[0].ehp_data.VxLan_info.valid == MEA_TRUE)
            MEA_OS_maccpy(&Service_data.FWD_DaMAC_value, &Service_editing.ehp_info[0].ehp_data.VxLan_info.vxlan_Da);
    }
    else {

        Service_editing.num_of_entries = entry->EHP_Entry->num_of_entries;
        MEA_OS_memcpy(&Service_editing_info[0], entry->EHP_Entry->ehp_info, sizeof(Service_editing_info));


        if (Service_editing.ehp_info[0].ehp_data.ETHCS_DL_info.valid == MEA_TRUE)
            MEA_OS_maccpy(&Service_data.FWD_DaMAC_value, &Service_editing.ehp_info[0].ehp_data.ETHCS_DL_info.Da_eNB);  /*-fwd_internal */
        if (Service_editing.ehp_info[0].ehp_data.VxLan_info.valid == MEA_TRUE)
            MEA_OS_maccpy(&Service_data.FWD_DaMAC_value, &Service_editing.ehp_info[0].ehp_data.VxLan_info.vxlan_Da);
    }

        //Service_data.vpn = entry->vpn;   /* learn the enb MAC*/
    Service_data.vpn = MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.vpn;

    Service_data.DSE_forwarding_enable   = MEA_TRUE;
    Service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    
    
     Service_data.DSE_learning_enable   = MEA_TRUE;
     Service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
     
     Service_data.DSE_learning_update_allow_enable = MEA_FALSE;
     Service_data.do_sa_search_only = MEA_TRUE;
     Service_data.ADM_ENA           = MEA_TRUE;

    Service_data.Access_port_en     = MEA_TRUE;
    Service_data.Access_port_adm_en = MEA_FALSE;
    Service_data.Access_port_id     = entry->Access_port_id;
    Service_data.Access_local_sw_en = MEA_FALSE;

    if (Service_data.Access_port_id == 0)
        Service_data.Access_port_en = MEA_FALSE;


    
/************************************************************************/
/*                                                                      */
/************************************************************************/
/* Set the upstream service data attribute for policing */
    Service_data.tmId   = 0; /* generate new id */
    Service_data.editId = 0; /* generate new id */
    Service_data.pmId   = 0; /* generate new id */

    Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL;

    MEA_OS_memset(&Service_policer, 0, sizeof(Service_policer));//
    
    if (entry->Service_Entry_Policer == NULL) {
        Service_policer.CIR = MEA_vpls_VPLSi_POLICER_CIR_leaf;
        Service_policer.CBS = MEA_vpls_VPLSi_POLICER_CBS_leaf;
        Service_policer.EIR = 0;
        Service_policer.EBS = 0;
        Service_policer.comp = 0;
        Service_policer.gn_type = MEA_vpls_VPLSi_POLICER_PGT;
    }
    else {
        MEA_OS_memcpy(&Service_policer, entry->Service_Entry_Policer, sizeof(MEA_Policer_Entry_dbt));
		 Service_data.CM = Service_policer.color_aware;
    }


    if (entry->ActionId != 0) {
    
        /* Set the upstream service data attribute for pm (counters) */
        Service_data.pmId = Action_data_po.pm_id;
        if (Action_data_po.tm_id_valid ==MEA_TRUE ) {
            Service_data.tmId = Action_data_po.tm_id;
            Service_data.tmId_disable = Action_data_po.tmId_disable;
            MEA_OS_memcpy(&Service_policer, &Action_policer, sizeof(MEA_Policer_Entry_dbt));
            Service_data.CM = Service_policer.color_aware;
        }


    }

            
            /* Set the upstream service data attribute for policing */
            
       if(entry->en_service_TFT == MEA_TRUE) {
 
        if (entry->Service_Entry_Policer != NULL) {
            Service_data.tmId            = entry->Service_Entry_data->tmId;
            Service_data.policer_prof_id = entry->Service_Entry_data->policer_prof_id;
            
            MEA_OS_memcpy(&Service_policer, entry->Service_Entry_Policer, sizeof(Service_policer));
            Service_data.CM = Service_policer.color_aware;

         }

        Service_data.pmId        = entry->Service_Entry_data->pmId;
        Service_data.Uepdn_id    = entry->Service_Entry_data->Uepdn_id;
        Service_data.Uepdn_valid = entry->Service_Entry_data->Uepdn_valid;

        Service_data.HPM_valid = entry->Service_Entry_data->HPM_valid;
        Service_data.HPM_profileId = entry->Service_Entry_data->HPM_profileId;
        Service_data.HPM_prof_mask = entry->Service_Entry_data->HPM_prof_mask;
        if (MEA_IS_INGRESS_FLOW_POLICER_SUPPORT) {
            if (entry->Service_Entry_data->Ingress_flow_policer_ProfileId != 0)
                Service_data.Ingress_flow_policer_ProfileId = entry->Service_Entry_data->Ingress_flow_policer_ProfileId;
            else {
             //   Service_data.Ingress_flow_policer_ProfileId = vpls_ingressPolicerFlow;
            }
        }

    }
   

    /* Build the Upstream Service editing */
    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));
    Service_editing.num_of_entries = 1;
    Service_editing.ehp_info = Service_editing_info;
    /*get the info of packet type from the create*/


    switch (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.SM_mode)
    {

    case MEA_CHARACTER_TYPE_TEID_MODE_SM1:
    case MEA_CHARACTER_TYPE_TEID_MODE_SM2:
        //Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN;
        Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent;
        
        break;
    case MEA_CHARACTER_TYPE_TEID_MODE_SM2a:
        //Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN;
        Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent;
        break;

    case MEA_CHARACTER_TYPE_TEID_MODE_SM3:
        //Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN;
        Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN;
        break;

    case MEA_CHARACTER_TYPE_TEID_MODE_SM4:
        //Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN;
        Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent;
        break;

    case  MEA_CHARACTER_TYPE_TEID_MODE_SM5:
        //Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN;
        Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN;
        break;
     case MEA_CHARACTER_TYPE_TEID_MODE_SM5a:
         Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG_Swap_Internal_VLAN;
 
         break;
     case MEA_CHARACTER_TYPE_TEID_MODE_SM3a:
         //Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN;
         Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ;
         break;
     case MEA_CHARACTER_TYPE_TEID_MODE_Vlan_To_Vxlan:
             Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_VXLAN_header; /*need to extract MPLS*/
         break;
     case MEA_CHARACTER_TYPE_TEID_MODE_Vlan_To_Vxlan_noVlan:
         Service_editing.ehp_info->ehp_data.EditingType = MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_Extract_Inner_vlan_VXLAN_header; /*need to extract MPLS*/
         break;


    default:
        return MEA_ERROR;
        break;
    }





    MEA_OS_memset(&Service_editing.ehp_info[0].output_info, 0, sizeof(MEA_OutPorts_Entry_dbt));

    /* Build the upstream service outPorts */
    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    if (entry->en_output == MEA_TRUE) {
        if (entry->Service_Entry_OutPorts == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "ERROR Service_Entry_OutPorts is NULL \n");
            return MEA_ERROR;
        }
        MEA_OS_memcpy(&Service_outPorts, entry->Service_Entry_OutPorts, sizeof(MEA_OutPorts_Entry_dbt));
        
    }


    /* Create new service for the given . */
    if (MEA_API_Create_Service(MEA_UNIT_0,
        &Service_key,
        &Service_data,
        &Service_outPorts,
        &Service_policer,
        &Service_editing,

        Service_id) != MEA_OK){

        return MEA_ERROR;
    }

    if (entry->en_service_TFT == MEA_TRUE) {
        if (entry->Service_Entry_Policer != NULL) {
            entry->Service_Entry_data->tmId = Service_data.tmId;
            entry->Service_Entry_data->pmId = Service_data.pmId;
        }
    }


    return MEA_OK;
}

MEA_Status MEA_API_VPLSi_UE_PDN_DL_Create_Entry(MEA_Unit_t         unit,
                                                    MEA_Uint16                vpls_Ins_Id,
                                                    MEA_Uint32                leafId,
                                                    MEA_VPLS_UE_Pdn_dl_dbt    *entry)
{
    //MEA_Uint16 i;
    //MEA_Uint16 index;
    MEA_Service_t ServiceId = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Bool      exist_o=MEA_FALSE;


    //MEA_Bool fondfree = MEA_FALSE;

    if (!MEA_VPLS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " MEA_VPLS_SUPPORT not support \n");
        return MEA_ERROR;
    }
    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " entry = NULL \n");
        return MEA_ERROR;
    }
    if (vpls_Ins_Id >= MEA_VPLS_MAX_OF_INSTANCE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " vpls_Ins_Id %d is out of range  \n", vpls_Ins_Id);
        return MEA_ERROR;
    }

    if (MEA_drv_VPLS_Table[vpls_Ins_Id].valid != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The VPLS Instance is not created \n");
        return MEA_ERROR;
    }
    if (entry->ActionId != 0) {
        if (MEA_API_IsExist_Action_ByType(unit, entry->ActionId, MEA_ACTION_TYPE_FWD, &exist_o) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The Action (%d)is failed\n", entry->ActionId);
            return MEA_ERROR;
        }

        if (exist_o == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The Action (%d)is not exist\n", entry->ActionId);
            return MEA_ERROR;
        }
    }


#if 0
    /*Search that UE_PDN_ID is not on the list*/
    
    for (i = 0; i < MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding; i++)
    {
        if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[i].enable == MEA_TRUE &&
            MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[i].UE_PND_ID == UE_pdn_Id){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The UE_PND_ID already on created \n");
            return MEA_ERROR;
        }
    }

    for (i = 0; i < MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding; i++)
    {
        if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[i].enable == MEA_FALSE)
        {
            fondfree = MEA_TRUE;
            index = i;
            break;
        }

    }

    if (fondfree == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The All the flooding is occupied\n");
        return MEA_ERROR;
    }

#else
    if (entry->ActionId == 0 && entry->en_service_TFT == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " the ActionId == 0 and en_service_TFT == FALSE  one of them need to set\n");
        return MEA_ERROR;
    
    }
    if (leafId >= MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " leafId %d > max_of_fooding %d \n", leafId, MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding);
        return MEA_ERROR;
    }
    if (leafId >= MEA_VPLS_MAX_OF_FLOODING) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " leafId %d  is out bigger then %d  \n", leafId, MEA_VPLS_MAX_OF_FLOODING);
        return MEA_ERROR;

    }

    if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].enable == MEA_TRUE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The UE_PND_ID already on created \n");
        return MEA_ERROR;
    }

    


#endif

    if (mea_drv_Create_UE_PDN_dl(vpls_Ins_Id, leafId, entry, &ServiceId) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The service Create_UE_PDN_dl fail \n");
        return MEA_ERROR;
    }

    

    /*Save the parameter */
    MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].DL_ServiceId = ServiceId;
    entry->serviceId = ServiceId;
    MEA_drv_VPLS_Table[vpls_Ins_Id].numof_ue_pdn++;
    
    MEA_OS_memcpy(&MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].Ue_pdn_dl, entry, sizeof(MEA_VPLS_UE_Pdn_dl_dbt));
    MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].enable = MEA_TRUE;

    return MEA_OK;
}

MEA_Status MEA_API_VPLSi_UE_PDN_DL_Delete_Entry(MEA_Unit_t         unit,
    MEA_Uint16               vpls_Ins_Id,
    MEA_Uint32               leafId)
{
   
   // MEA_Uint16 i;
   // MEA_Bool found_Ue_Pdn = MEA_FALSE;
    MEA_Bool exist;
    MEA_Status retStatus;
    

    if (!MEA_VPLS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " MEA_VPLS_SUPPORT not support \n");
        return MEA_ERROR;
    }
   
    if (leafId >= MEA_VPLS_MAX_OF_FLOODING){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " vpls_Ins_Id %d is out of range  \n", vpls_Ins_Id);
        return MEA_ERROR;
    }

    if (MEA_drv_VPLS_Table[vpls_Ins_Id].valid != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The VPLS Instance is not created \n");
        return MEA_ERROR;
    }



    if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].enable == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The VPLS Instance leaf is not created \n");
        return MEA_ERROR;

    }
    

  
       

            /* Delete the Service */
            retStatus = MEA_API_IsExist_Service_ById(MEA_UNIT_0, MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].DL_ServiceId, &exist);
            if (exist){
                MEA_API_Delete_Service(MEA_UNIT_0, MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].DL_ServiceId);

                MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].DL_ServiceId = 0;
                MEA_OS_memset(&MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId], 0, sizeof(mea_drv_vpls_ue_pdn_dl));
                MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].enable = MEA_FALSE;

                MEA_drv_VPLS_Table[vpls_Ins_Id].numof_ue_pdn--;
            }
            else{
                return retStatus;
            }

        
        


    return MEA_OK;

}



MEA_Status MEA_API_VPLSi_UE_PDN_DL_Get_Entry(MEA_Unit_t               unit,
    MEA_Uint16              vpls_Ins_Id,
    MEA_Uint32              leafId,
    MEA_VPLS_UE_Pdn_dl_dbt  *entry)
{

   // MEA_Uint16 index;
  //  MEA_Uint16 i;
  //  MEA_Bool found_Ue_Pdn = MEA_FALSE;
   


    if (!MEA_VPLS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " MEA_VPLS_SUPPORT not support \n");
        return MEA_ERROR;
    }
    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " entry = NULL \n");
        return MEA_ERROR;
    }
    if (vpls_Ins_Id >= MEA_VPLS_MAX_OF_INSTANCE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " vpls_Ins_Id %d is out of range  \n", vpls_Ins_Id);
        return MEA_ERROR;
    }

    if (MEA_drv_VPLS_Table[vpls_Ins_Id].valid != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The VPLS Instance is not created \n");
        return MEA_ERROR;
    }
    if (leafId >= MEA_VPLS_MAX_OF_FLOODING) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The leafId is out of range  \n");
        return MEA_ERROR;
    }
     
    if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].enable == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The leafId is not create \n");
        return MEA_ERROR;
    }


    MEA_OS_memcpy(entry, &MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[leafId].Ue_pdn_dl,sizeof(MEA_VPLS_UE_Pdn_dl_dbt));
    
   

    return MEA_OK;
}

MEA_Status MEA_API_VPLSi_UE_PDN_DL_GetFirst_Entry(MEA_Unit_t               unit,
    MEA_Uint16              vpls_Ins_Id,
    MEA_Uint32              *leafId,
    MEA_Bool     *o_found)
{
    MEA_Uint32 i;

    if (!MEA_VPLS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " MEA_VPLS_SUPPORT not support \n");
        return MEA_ERROR;
    }
    if (o_found == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " o_found = NULL \n");
return MEA_ERROR;
    }
    if (vpls_Ins_Id >= MEA_VPLS_MAX_OF_INSTANCE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " vpls_Ins_Id %d is out of range  \n", vpls_Ins_Id);
        return MEA_ERROR;
    }

    if (MEA_drv_VPLS_Table[vpls_Ins_Id].valid != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The VPLS Instance is not created \n");
        return MEA_ERROR;
    }

    *o_found = MEA_FALSE;

    for (i = 0; i < MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding; i++)
    {
        if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[i].enable == MEA_TRUE){
            *leafId = i;
            *o_found = MEA_TRUE;
            return MEA_OK;
        }
    }


    return MEA_OK;
}

MEA_Status MEA_API_VPLSi_UE_PDN_DL_GetNext_Entry(MEA_Unit_t               unit,
    MEA_Uint16              vpls_Ins_Id,
    MEA_Uint32              *leafId,
    MEA_Bool     *o_found)
{

    MEA_Uint32 i, index = 0;
    //MEA_Bool   found = MEA_FALSE;

    if (!MEA_VPLS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " MEA_VPLS_SUPPORT not support \n");
        return MEA_ERROR;
    }
    if (o_found == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " o_found = NULL \n");
        return MEA_ERROR;
    }
    if (vpls_Ins_Id >= MEA_VPLS_MAX_OF_INSTANCE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " vpls_Ins_Id %d is out of range  \n", vpls_Ins_Id);
        return MEA_ERROR;
    }

    *o_found = MEA_FALSE;
    index = *leafId;
    if (index + 1 > MEA_VPLS_MAX_OF_FLOODING) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " leaf Id %d is out of range  \n", index + 1);
        return MEA_ERROR;
    }

        for (i = index + 1; i < MEA_drv_VPLS_Table[vpls_Ins_Id].data_info.max_of_fooding; i++)
        {
            if (MEA_drv_VPLS_Table[vpls_Ins_Id].data_info_ue_pdn_dl[i].enable == MEA_TRUE){
                *leafId = i;
                *o_found = MEA_TRUE;
                return MEA_OK;
            }
        }
 

    return MEA_OK;

}

/******************************************************************************************/

static void mea_drv_UEPDN_Set_Handover_UpdateHW_Entry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    //MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    // MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    MEA_Uint32           len = (MEA_Uint32)(long)arg3;
    MEA_Uint32 *data = (MEA_Uint32*)arg4;

    MEA_Uint32      i;


   
    for (i = 0; i < len; i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            data[i],
            MEA_MODULE_IF);
    }



}



MEA_Status mea_drv_UEPDN_Set_Handover_UpdateHW(MEA_Unit_t unit)
{

    MEA_ind_write_t   ind_write;
    MEA_Uint32 val[4];
    MEA_Uint32 len=2;
    MEA_Uint32 count_shift;


    if (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_FALSE || 
        (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE && 
        ( ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7015) == 0)) ||
          ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045) == 0)) 
        ) )
        )
    {
        len = 2;
        count_shift = 0;

        MEA_OS_insert_value(count_shift,
            8,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.clusterId,
            &val[0]);
        count_shift += 8;
        MEA_OS_insert_value(count_shift,
            1,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.en_vp,
            &val[0]);
        count_shift += 1;
        MEA_OS_insert_value(count_shift,
            10,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.access_port,
            &val[0]);
        count_shift += 10;
        MEA_OS_insert_value(count_shift,
            12,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.vpn,
            &val[0]);
        count_shift += 12;
        MEA_OS_insert_value(count_shift,
            1,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.force,
            &val[0]);
        count_shift += 1;
        /************************************************************************/
        /* handover 1                                                           */
        /************************************************************************/
        MEA_OS_insert_value(count_shift,
            8,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.clusterId,
            &val[0]);
        count_shift += 8;
        MEA_OS_insert_value(count_shift,
            1,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.en_vp,
            &val[0]);
        count_shift += 1;
        MEA_OS_insert_value(count_shift,
            10,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.access_port,
            &val[0]);
        count_shift += 10;
        MEA_OS_insert_value(count_shift,
            12,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.vpn,
            &val[0]);
        count_shift += 12;
        MEA_OS_insert_value(count_shift,
            1,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.force,
            &val[0]);
        count_shift += 1;

    }
    else {
        len = 4;


        count_shift = 0;

        MEA_OS_insert_value(count_shift,
            10,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.clusterId,
            &val[0]);
        count_shift += 10;
        MEA_OS_insert_value(count_shift,
            1,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.en_vp,
            &val[0]);
        count_shift += 1;
        MEA_OS_insert_value(count_shift,
            10,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.access_port,
            &val[0]);
        count_shift += 10;
        MEA_OS_insert_value(count_shift,
            12,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.vpn,
            &val[0]);
        count_shift += 12;
        
        MEA_OS_insert_value(count_shift,
            1,
            MEA_drv_UEPDN_VPLS_Handover_Table[0].data_internal.force,
            &val[0]);
        count_shift += 1;

        MEA_OS_insert_value(count_shift,
            30,
            0,
            &val[0]);
        count_shift += 30;
        


        /************************************************************************/
        /* handover 1                                                           */
        /************************************************************************/
        MEA_OS_insert_value(count_shift,
            8,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.clusterId,
            &val[0]);
        count_shift += 8;
        MEA_OS_insert_value(count_shift,
            1,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.en_vp,
            &val[0]);
        count_shift += 1;
        MEA_OS_insert_value(count_shift,
            10,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.access_port,
            &val[0]);
        count_shift += 10;
        MEA_OS_insert_value(count_shift,
            12,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.vpn,
            &val[0]);
        count_shift += 12;
        MEA_OS_insert_value(count_shift,
            11,
            MEA_drv_UEPDN_VPLS_Handover_Table[1].data_internal.force,
            &val[0]);
        count_shift += 1;





    }


  
 
    

    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
    ind_write.tableOffset = ENET_REGISTER_TBL100_Configure_Handover_0; //reg Id
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_UEPDN_Set_Handover_UpdateHW_Entry;


    ind_write.funcParam1 = (MEA_funcParam)unit;
    ind_write.funcParam2 = (MEA_funcParam)0;
    ind_write.funcParam3 = (MEA_funcParam)(long)len;/*len*/
    ind_write.funcParam4 = (MEA_funcParam)&val;

    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit, &ind_write, ENET_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_IF);
        return ENET_ERROR;
    }


    return MEA_OK;
}

MEA_Status MEA_API_UEPDN_Set_Handover(MEA_Unit_t unit, MEA_Uint8 index, mea_UEpdn_Handover_dbt *entry)
{
    if (index >= MEA_MAX_OF_VPLS_Handover){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " MEA_MAX_OF_VPLS_Handover out of range \n");
        return MEA_ERROR;
    }

    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " entry == NULL \n");
        return MEA_ERROR;
    }
    
    if (entry->force == MEA_FALSE){
        MEA_OS_memset(&MEA_drv_UEPDN_VPLS_Handover_Table[index].data, 0, sizeof(mea_UEpdn_Handover_dbt));
        MEA_OS_memset(&MEA_drv_UEPDN_VPLS_Handover_Table[index].data_internal, 0, sizeof(mea_UEpdn_Handover_dbt));
        MEA_drv_UEPDN_VPLS_Handover_Table[index].valid = MEA_FALSE;
    }
    else{
        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        if (entry->access_port == 0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " access_port  different from %d \n",entry->access_port);
            return MEA_ERROR;
        }
        /*check on the cluster is valid*/
        if (entry->en_vp == MEA_TRUE){
            
        
        /* check the cluster id */
            if (ENET_IsValid_Queue(unit, entry->clusterId, MEA_TRUE) == ENET_FALSE) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " The Cluster Id is not valid  \n");
                return MEA_ERROR;
            }

        }

        MEA_drv_UEPDN_VPLS_Handover_Table[index].valid = entry->force;
           
        MEA_OS_memcpy(&MEA_drv_UEPDN_VPLS_Handover_Table[index].data, entry, sizeof(mea_UEpdn_Handover_dbt));
        MEA_OS_memcpy(&MEA_drv_UEPDN_VPLS_Handover_Table[index].data_internal, entry, sizeof(mea_UEpdn_Handover_dbt));
        MEA_drv_UEPDN_VPLS_Handover_Table[index].data_internal.clusterId = enet_cluster_external2internal(entry->clusterId);
    }

    
    
    
    /************************************************************************/
    /* Write to HW                                                          */
    /************************************************************************/
    if(mea_drv_UEPDN_Set_Handover_UpdateHW(unit) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " entry == NULL \n");
    }


    return MEA_OK;
}
MEA_Status MEA_API_UEPDN_Get_Handover(MEA_Unit_t unit, MEA_Uint8 index, mea_UEpdn_Handover_dbt *entry)
{

   if (index >= MEA_MAX_OF_VPLS_Handover){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " MEA_MAX_OF_VPLS_Handover out of range \n");
        return MEA_ERROR;
    }

    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " entry == NULL \n");
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry, &MEA_drv_UEPDN_VPLS_Handover_Table[index].data, sizeof(mea_UEpdn_Handover_dbt));


    return MEA_OK;
}


