/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish 
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_filter_drv.h"
#include "mea_if_service_drv.h"
#include "mea_serviceEntry_drv.h"

#include "mea_port_drv.h"
#include "mea_drv_common.h"
#include "mea_policer_drv.h"
#include "mea_bm_drv.h"
#include "enet_xPermission_drv.h"
#include "mea_serviceEntry_drv.h"
#include "mea_bm_counters_drv.h"
#include "mea_bm_ehp_drv.h"
#include "mea_filter_drv.h"
#include "mea_action_drv.h"
#include "mea_lxcp_drv.h"
#include "mea_db_drv.h"
#include "mea_bm_ehp_drv.h"

#include "mea_action_drv.h"
#include "mea_lxcp_drv.h"
#include "mea_deviceInfo_drv.h"
#include "enet_queue_drv.h"
#include "mea_vpls.h"
#include "mea_if_vsp_drv.h"
#include "mea_globals_drv.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
//#define NOT_SUPPORT_INTERNAL 1



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
 


struct mea_service_node_s;
typedef struct mea_service_node_s {
    struct mea_service_node_s* next;
    struct mea_service_node_s* prev;
    MEA_Service_t              serviceId;
} mea_service_node_t;



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

static mea_service_node_t            *p_service_head = NULL;
static MEA_db_dbt                     MEA_Service_Internal_db = NULL;
static MEA_db_dbt                     MEA_Service_Internal_db_getHW = NULL;



static MEA_Uint32                    *MEA_Service_Internal_BIT = NULL;
static MEA_Uint32                    *MEA_Service_External_BIT = NULL;       /* software external */
static MEA_Uint32                    *MEA_Service_External_Hash_BIT = NULL;  /* hash address forDDR*/
//static MEA_Uint32                    *MEA_Service_SW_BIT = NULL;             /* total service*/



static MEA_db_dbt                     MEA_ServiceRange_db = NULL;
static MEA_SrvRange_Vlan_pri_dbt     *MEA_ServiceRange_Table = NULL;


static MEA_vlanRange_prof_dbt        *MEA_vlanRange_profile = NULL;

static MEA_srv_filter_prof_dbt        *MEA_srv_filter_prof_Table = NULL;
static MEA_Service_Entry_dbt           *MEA_Service_Table = NULL;


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
MEA_db_HwUnit_t mea_Service_HwGetUnit(MEA_Unit_t                  unit_i,
    MEA_Service_Entry_Key_dbt  *key_pi);


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/





static void MEA_Service_rollback(MEA_Unit_t unit_i, MEA_Service_t serviceId);
static void  MEA_service_setting_SRV_ON_External_DDR(MEA_Unit_t unit_i, MEA_Service_t serviceId, MEA_SETTING_TYPE_te   type);

static MEA_Status mea_Service_GetFreeServiceId(MEA_Service_t *serviceId, MEA_SRV_mode_t SRV_Mode);

static MEA_Status mea_Service_GetFree_Internal_ServiceId(MEA_Service_t *serviceId);
static MEA_Status mea_Service_GetFree_External_ServiceId(MEA_Service_t *serviceId);


static MEA_Status mea_drv_serviceRange_UpdateHw(MEA_Unit_t                  unit_i,
    MEA_Service_t               serviceId_i,
    MEA_Uint16                  rangeId_i,
    MEA_SrvRange_Vlan_pri_dbt*  new_entry_pi,
    MEA_SrvRange_Vlan_pri_dbt*  old_entry_pi);
static void mea_drv_serviceRange_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4);




static MEA_Status mea_drv_serviceRange_Add(MEA_Unit_t                 unit_i,
    MEA_Service_t              serviceId_i,
    MEA_Service_Entry_Key_dbt *key_i,
    MEA_Uint16                *rangeId_o);
static MEA_Status mea_drv_serviceRange_Delete(MEA_Uint16 rangeId_i);



static MEA_Status mea_drv_serviceRange_Check_Intersect(MEA_Unit_t                unit_i,
    MEA_Service_Entry_Key_dbt *key_i);

static MEA_Status mea_drv_Init_serviceRange_Table(MEA_Unit_t unit_i);
static MEA_Status mea_drv_ReInit_serviceRange_Table(MEA_Unit_t unit_i);
static MEA_Status mea_drv_Conclude_serviceRange_Table(MEA_Unit_t unit_i);

static MEA_Status mea_drv_ReInit_service_Internal_Hush(MEA_Unit_t unit_i);

/*----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Service_GetFreeServiceId>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

static MEA_Status mea_Service_GetFreeServiceId(MEA_Service_t *serviceId, MEA_SRV_mode_t SRV_Mode)
{

    MEA_Service_t from, to;
    MEA_Bool      forceId = MEA_FALSE;
    MEA_Service_t External_serviceId;

    if (SRV_Mode == MEA_SRV_MODE_NORMAL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Service Fail cant be  MEA_SRV_MODE_NORMAL\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (SRV_Mode == MEA_SRV_MODE_REGULAR)
    {
        if(mea_Service_GetFree_Internal_ServiceId(serviceId) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Internal Service Fail \n", __FUNCTION__);
            return MEA_ERROR;
        }

        MEA_Service_Table[*serviceId].en_Sid_Internal = MEA_TRUE;
        MEA_Service_Table[*serviceId].Sid_Internal = *serviceId;


    }
    if (SRV_Mode == MEA_SRV_MODE_EXTERNAL){
        if (*serviceId == ENET_PLAT_GENERATE_NEW_ID)
            External_serviceId = ENET_PLAT_GENERATE_NEW_ID;
        else{
            if (*serviceId <= MEA_MAX_NUM_OF_SERVICES_INTERNAL){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - EXTERNAL Service Fail  Sid  %d <= %d \n", __FUNCTION__, *serviceId, MEA_MAX_NUM_OF_SERVICES_INTERNAL);
                return MEA_ERROR;
            }

            External_serviceId = (*serviceId) - MEA_MAX_NUM_OF_SERVICES_INTERNAL;
        }
         
        if (mea_Service_GetFree_External_ServiceId(&External_serviceId) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - External Service Fail \n", __FUNCTION__);
            return MEA_ERROR;
        }
        if (*serviceId == ENET_PLAT_GENERATE_NEW_ID){
                *serviceId = External_serviceId + MEA_MAX_NUM_OF_SERVICES_INTERNAL;
        }
        else{
        
        }

            MEA_Service_Table[*serviceId].en_Sid_External = MEA_TRUE;
            MEA_Service_Table[*serviceId].Sid_External = External_serviceId;

    } // MEA_SRV_MODE_EXTERNAL


   
   
   if(*serviceId == ENET_PLAT_GENERATE_NEW_ID) { 
    /* Calculate from and to */
        from=1;
        to=MEA_MAX_NUM_OF_SERVICES-1;
    }else{
        forceId=MEA_TRUE;
        from=*serviceId;
        to=*serviceId;
        if (*serviceId > (MEA_MAX_NUM_OF_SERVICES - 1)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - ServiceId MAX of service %d \n", __FUNCTION__, MEA_MAX_NUM_OF_SERVICES-1);
            return MEA_ERROR;
        }
    }
 
    /* Search for free entry */
        for((*serviceId)=from;(*serviceId)<=to;(*serviceId)++){
            if(*serviceId == ENET_PLAT_GENERATE_NEW_ID)
                continue;

           if (MEA_Service_Table[(*serviceId)].data.valid == MEA_FALSE){
                             MEA_Service_Table[*serviceId].data.valid = MEA_TRUE;
                             return MEA_OK;
           }  
      
        }

     if(forceId == MEA_FALSE){
     
    /* No free entry */    
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - No free serviceId MAX of service %d \n",__FUNCTION__,MEA_MAX_NUM_OF_SERVICES);
         
    }else{
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  serviceId %d  is all ready created \n",__FUNCTION__,*serviceId);
     }

     
     return MEA_ERROR;

}



static MEA_Status mea_Service_GetFree_Internal_ServiceId(MEA_Service_t *serviceId)
{

    MEA_Service_t from, to;
    MEA_Bool      forceId = MEA_FALSE;

    if (*serviceId == ENET_PLAT_GENERATE_NEW_ID) {
        /* Calculate from and to */
        from = 1;
        to = MEA_MAX_NUM_OF_SERVICES_INTERNAL - 1;
    }
    else{
        forceId = MEA_TRUE;
        from = *serviceId;
        to = *serviceId;
        if (*serviceId > (MEA_MAX_NUM_OF_SERVICES_INTERNAL - 1)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - ServiceId MAX of Internal service %d \n", __FUNCTION__, MEA_MAX_NUM_OF_SERVICES_INTERNAL - 1);
            return MEA_ERROR;
        }
    }

    /* Search for free entry */
    for ((*serviceId) = from; (*serviceId) <= to; (*serviceId)++){
        if (*serviceId == ENET_PLAT_GENERATE_NEW_ID) /*not sid == ENET_PLAT_GENERATE_NEW_ID cant be set*/
            continue;

        if (MEA_IS_CLEAR_SID(MEA_Service_Internal_BIT, *serviceId) == MEA_TRUE){
            MEA_SET_SID(MEA_Service_Internal_BIT, *serviceId);
            return MEA_OK;
        }
    }

    if (forceId == MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - No free Internal serviceId  %d \n", __FUNCTION__, MEA_MAX_NUM_OF_SERVICES_INTERNAL);

    }
    else{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  serviceId %d  is all ready created \n", __FUNCTION__, *serviceId);
    }
    return MEA_ERROR;

}


static MEA_Status mea_Service_GetFree_External_ServiceId(MEA_Service_t *serviceId)
{

    MEA_Service_t from, to;
    MEA_Bool      forceId = MEA_FALSE;

    if (*serviceId == ENET_PLAT_GENERATE_NEW_ID || *serviceId == 0) {
        /* Calculate from and to */
        from = 1; /* start from 1 Zero cant be set */
        to = MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW - 1;
    }
    else{
        forceId = MEA_TRUE;
        from = *serviceId;
        to = *serviceId;
        if (*serviceId > (MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW - 1)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - ServiceId MAX of service %d \n", __FUNCTION__, MEA_MAX_NUM_OF_SERVICES - 1);
            return MEA_ERROR;
        }
    }

    /* Search for free entry */
    for ((*serviceId) = from; (*serviceId) <= to; (*serviceId)++){
        if (*serviceId == ENET_PLAT_GENERATE_NEW_ID) /*not sid == ENET_PLAT_GENERATE_NEW_ID cant be set*/
            continue;

        if (MEA_IS_CLEAR_SID(MEA_Service_External_BIT, *serviceId) == MEA_TRUE){
            MEA_SET_SID(MEA_Service_External_BIT, *serviceId);
            return MEA_OK;
        }
    }

    if (forceId == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  No free External serviceId  %d \n", __FUNCTION__, MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW-1);

    }
    else{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  serviceId %d  is all ready created \n", __FUNCTION__, *serviceId);
    }
    return MEA_ERROR;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Service_Add>                                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Service_Add(MEA_Service_t serviceId) 
{

    mea_service_node_t* p_mea_service_node;

    p_mea_service_node = (mea_service_node_t*)MEA_OS_malloc(sizeof(mea_service_node_t));
    if (p_mea_service_node == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_OS_malloc failed (serviceId=%d)\n",
                         __FUNCTION__,serviceId);
       return MEA_ERROR; 
    }

    p_mea_service_node->serviceId = serviceId;
    p_mea_service_node->next      = p_service_head;
    p_mea_service_node->prev      = NULL;
    if (p_mea_service_node->next != NULL) {
       (p_mea_service_node->next)->prev = p_mea_service_node;
    }
    p_service_head = p_mea_service_node;

    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Service_Delete>                                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Service_Delete(MEA_Service_t serviceId) 
{
    mea_service_node_t* p_mea_service_node;
    MEA_Bool found = MEA_FALSE;

    for (p_mea_service_node = p_service_head; 
         p_mea_service_node != NULL; 
         p_mea_service_node = p_mea_service_node->next) {
      
        if (p_mea_service_node->serviceId == serviceId) {

            if (p_mea_service_node->next != NULL) {
                (p_mea_service_node->next)->prev = p_mea_service_node->prev;
            }

            if (p_mea_service_node->prev != NULL) {
                (p_mea_service_node->prev)->next = p_mea_service_node->next;
            } else {
                p_service_head = p_mea_service_node->next;
            }

            MEA_OS_free(p_mea_service_node);
            found = MEA_TRUE;


            break;
        }
    }

    if (found == MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - delete service (%d) failed - not found in link list\n",
                         __FUNCTION__,serviceId);
       return MEA_ERROR;
    }

    return MEA_OK;
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_Service_GetHwUnit>                               */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_db_HwUnit_t mea_Service_GetHwUnit(MEA_Unit_t                  unit_i,
                                        MEA_Service_Entry_Key_dbt  *key_pi)
{


    if (globalMemoryMode==MEA_MODE_REGULAR_ENET3000) {
        return MEA_HW_UNIT_ID_GENERAL;
    } 
    


    return MEA_HW_UNIT_ID_UPSTREAM;

}

/*alex Hw */
MEA_Status mea_drv_Service_External_hash_clac_function(MEA_Unit_t  unit_i, MEA_Service_HW_SRV_Key HW_SRV_key, MEA_Uint32 mode, MEA_Uint32 *hash_addr)
{

#ifdef MEA_DEBUG_HASH    
    MEA_Uint32 i = 0;
#endif
    //MEA_Uint32 hash_addr = 0;
	MEA_Uint32 Hash_Val;
    MEA_Uint32  net_tag1;
    MEA_Uint32  net_tag2;
    MEA_Uint32  priority;


    MEA_Uint32 net_tag1_H = 0;
    MEA_Uint32 net_tag2_H = 0;
    MEA_Uint32 src_port_L = 0;
    MEA_Uint32 src_port_M = 0;
    MEA_Uint32 src_port_H = 0;


    MEA_Uint32 Temp_net_tag1 = 0;
    MEA_Uint32 Temp_net_tag2 = 0;
    MEA_Uint32 Temp_priority = 0;


    MEA_Uint32 temp[5];

    MEA_Uint32 val[1];
    
    
    MEA_Uint32  arrayValue[32];
    MEA_Uint32 i;




    MEA_Uint32                count_shift;

    *hash_addr = 0;

    MEA_OS_memset(&temp[0], 0, sizeof(temp));

    net_tag1 = HW_SRV_key.Hw_net_tag;
    net_tag1 |= HW_SRV_key.Hw_sub_protocol_type << 20;
    net_tag2 = HW_SRV_key.Hw_inner_netTag_from_value;

    priority = HW_SRV_key.Hw_priority;
    priority |= HW_SRV_key.Hw_priType << 6;



    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    MEA_OS_memset(&arrayValue, 0, sizeof(arrayValue));

    for (i = 0; i < 24; i++) {
        arrayValue[i] = ((net_tag1 >> i) & 0x1);
    }


    Temp_net_tag1 = 0;

    Temp_net_tag1 |= (arrayValue[22] & 0x1) << 23;
    Temp_net_tag1 |= (arrayValue[3] & 0x1) << 22;
    Temp_net_tag1 |= (arrayValue[20] & 0x1) << 21;
    Temp_net_tag1 |= (arrayValue[5] & 0x1) << 20;
    Temp_net_tag1 |= (arrayValue[19] & 0x1) << 19;
    Temp_net_tag1 |= (arrayValue[14] & 0x1) << 18;
    Temp_net_tag1 |= (arrayValue[1] & 0x1) << 17;
    Temp_net_tag1 |= (arrayValue[12] & 0x1) << 16;
    Temp_net_tag1 |= (arrayValue[7] & 0x1) << 15;
    Temp_net_tag1 |= (arrayValue[10] & 0x1) << 14;
    Temp_net_tag1 |= (arrayValue[16] & 0x1) << 13;
    Temp_net_tag1 |= (arrayValue[6] & 0x1) << 12;
    Temp_net_tag1 |= (arrayValue[4] & 0x1) << 11;
    Temp_net_tag1 |= (arrayValue[21] & 0x1) << 10;
    Temp_net_tag1 |= (arrayValue[13] & 0x1) << 9;
    Temp_net_tag1 |= (arrayValue[9] & 0x1) << 8;
    Temp_net_tag1 |= (arrayValue[15] & 0x1) << 7;
    Temp_net_tag1 |= (arrayValue[0] & 0x1) << 6;
    Temp_net_tag1 |= (arrayValue[17] & 0x1) << 5;
    Temp_net_tag1 |= (arrayValue[23] & 0x1) << 4;
    Temp_net_tag1 |= (arrayValue[11] & 0x1) << 3;
    Temp_net_tag1 |= (arrayValue[8] & 0x1) << 2;
    Temp_net_tag1 |= (arrayValue[18] & 0x1) << 1;
    Temp_net_tag1 |= (arrayValue[2] & 0x1) << 0;

    net_tag1 = Temp_net_tag1;
    /************************************************************************/

    MEA_OS_memset(&arrayValue, 0, sizeof(arrayValue));

    for (i = 0; i < 24; i++) {
        arrayValue[i] = ((net_tag2 >> i) & 0x1);
    }

    Temp_net_tag2 = 0;

    Temp_net_tag2 |= (arrayValue[9] & 0x1) << 23;
    Temp_net_tag2 |= (arrayValue[0] & 0x1) << 22;
    Temp_net_tag2 |= (arrayValue[12] & 0x1) << 21;
    Temp_net_tag2 |= (arrayValue[4] & 0x1) << 20;
    Temp_net_tag2 |= (arrayValue[23] & 0x1) << 19;
    Temp_net_tag2 |= (arrayValue[7] & 0x1) << 18;
    Temp_net_tag2 |= (arrayValue[18] & 0x1) << 17;
    Temp_net_tag2 |= (arrayValue[15] & 0x1) << 16;
    Temp_net_tag2 |= (arrayValue[2] & 0x1) << 15;
    Temp_net_tag2 |= (arrayValue[13] & 0x1) << 14;
    Temp_net_tag2 |= (arrayValue[5] & 0x1) << 13;
    Temp_net_tag2 |= (arrayValue[20] & 0x1) << 12;
    Temp_net_tag2 |= (arrayValue[17] & 0x1) << 11;
    Temp_net_tag2 |= (arrayValue[22] & 0x1) << 10;
    Temp_net_tag2 |= (arrayValue[11] & 0x1) << 9;
    Temp_net_tag2 |= (arrayValue[19] & 0x1) << 8;
    Temp_net_tag2 |= (arrayValue[3] & 0x1) << 7;
    Temp_net_tag2 |= (arrayValue[21] & 0x1) << 6;
    Temp_net_tag2 |= (arrayValue[10] & 0x1) << 5;
    Temp_net_tag2 |= (arrayValue[1] & 0x1) << 4;
    Temp_net_tag2 |= (arrayValue[16] & 0x1) << 3;
    Temp_net_tag2 |= (arrayValue[6] & 0x1) << 2;
    Temp_net_tag2 |= (arrayValue[14] & 0x1) << 1;
    Temp_net_tag2 |= (arrayValue[8] & 0x1) << 0;

    net_tag2 = Temp_net_tag2;
    /******************************************************************/
    MEA_OS_memset(&arrayValue, 0, sizeof(arrayValue));

    for (i = 0; i < 32; i++) {
        arrayValue[i] = ((priority >> i) & 0x1);
    }

    Temp_priority = 0;

    Temp_priority |= (arrayValue[6] & 0x1) << 7;
    Temp_priority |= (arrayValue[0] & 0x1) << 6;
    Temp_priority |= (arrayValue[4] & 0x1) << 5;
    Temp_priority |= (arrayValue[5] & 0x1) << 4;
    Temp_priority |= (arrayValue[2] & 0x1) << 3;
    Temp_priority |= (arrayValue[7] & 0x1) << 2;
    Temp_priority |= (arrayValue[1] & 0x1) << 1;
    Temp_priority |= (arrayValue[3] & 0x1) << 0;




    priority = Temp_priority;

    switch (mode)
    {
    case 0:  /*17bit*/
#if 1
             /* start width    &array */
        val[0] = net_tag2;
        temp[0] = MEA_OS_read_value(0, 16, &val[0]);
        val[0] = HW_SRV_key.Hw_src_port;
        temp[1] = (MEA_OS_read_value(7, 3, &val[0]) << 14);
        val[0] = net_tag1;
        temp[2] = MEA_OS_read_value(0, 16, &val[0]);
        /*---------------------------------------------*/
        val[0] = net_tag2;
        net_tag2_H = MEA_OS_read_value(17, 7, &val[0]);
        val[0] = HW_SRV_key.Hw_src_port;
        src_port_H = MEA_OS_read_value(4, 3, &val[0]);

        val[0] = net_tag1;
        net_tag1_H = MEA_OS_read_value(17, 7, &val[0]);

        count_shift = 0;
        MEA_OS_insert_value(count_shift,
            7,
            net_tag1_H,
            &temp[3]);
        count_shift += 7;
        MEA_OS_insert_value(count_shift,
            3,
            src_port_H,
            &temp[3]);
        count_shift += 3;
        MEA_OS_insert_value(count_shift,
            7,
            net_tag2_H,
            &temp[3]);




        /*---------------------------------------------*/
        val[0] = HW_SRV_key.Hw_src_port;

        src_port_L = MEA_OS_read_value(0, 4, &val[0]);

        count_shift = 0;
        MEA_OS_insert_value(count_shift,
            5,
            HW_SRV_key.Hw_L2_protocol_type,
            &temp[4]);
        count_shift += 5;

        MEA_OS_insert_value(count_shift,
            4,
            src_port_L,
            &temp[4]);
        count_shift += 4;
        MEA_OS_insert_value(count_shift,
            8,
            priority,
            &temp[4]);
        count_shift += 8;

        /*---------------------------------------------*/

        *hash_addr = temp[0] ^ temp[1] ^ temp[2] ^ temp[3] ^ temp[4];
#endif
        break;
    case 1:/*18 bit*/
#if 1

           /* start width    &array */
        val[0] = net_tag2;
        temp[0] = MEA_OS_read_value(0, 18, &val[0]);

        val[0] = net_tag1;
        temp[1] = MEA_OS_read_value(0, 18, &val[0]);

        val[0] = HW_SRV_key.Hw_src_port;
        src_port_L = (MEA_OS_read_value(5, 2, &val[0]));

        val[0] = HW_SRV_key.Hw_src_port;
        src_port_H = (MEA_OS_read_value(7, 3, &val[0]));

        val[0] = net_tag2;
        net_tag2_H = MEA_OS_read_value(18, 6, &val[0]);
        val[0] = net_tag1;
        net_tag1_H = MEA_OS_read_value(18, 6, &val[0]);

        count_shift = 0;
        MEA_OS_insert_value(count_shift,
            6,
            net_tag1_H,
            &temp[2]);
        count_shift += 6;

        MEA_OS_insert_value(count_shift,
            1,
            0,
            &temp[2]);
        count_shift += 1;

        MEA_OS_insert_value(count_shift,
            3,
            src_port_H,
            &temp[2]);
        count_shift += 3;

        MEA_OS_insert_value(count_shift,
            2,
            src_port_L,
            &temp[2]);
        count_shift += 2;
        MEA_OS_insert_value(count_shift,
            6,
            net_tag2_H,
            &temp[2]);
        count_shift += 6;






        /*---------------------------------------------*/

        src_port_L = MEA_OS_read_value(0, 5, &val[0]);

        count_shift = 0;
        MEA_OS_insert_value(count_shift,
            5,
            HW_SRV_key.Hw_L2_protocol_type,
            &temp[3]);
        count_shift += 5;

        MEA_OS_insert_value(count_shift,
            5,
            src_port_L,
            &temp[3]);
        count_shift += 5;
        MEA_OS_insert_value(count_shift,
            8,
            priority,
            &temp[3]);
        count_shift += 8;




        *hash_addr = temp[0] ^ temp[1] ^ temp[1] ^ temp[3];
#endif
        break;

    case 2: /*19bit address*/
#if 1

            /* start width    &array */
        val[0] = net_tag2;
        temp[0] = MEA_OS_read_value(0, 19, &val[0]);

        val[0] = net_tag1;
        temp[1] = MEA_OS_read_value(0, 19, &val[0]);
        /*---------------------------------------------*/
        val[0] = HW_SRV_key.Hw_src_port;
        src_port_M = (MEA_OS_read_value(6, 1, &val[0]));

        val[0] = HW_SRV_key.Hw_src_port;
        src_port_H = (MEA_OS_read_value(7, 3, &val[0]));

        val[0] = net_tag2;
        net_tag2_H = MEA_OS_read_value(19, 5, &val[0]);
        val[0] = net_tag1;
        net_tag1_H = MEA_OS_read_value(19, 5, &val[0]);

        temp[2] = 0;
        count_shift = 0;
        MEA_OS_insert_value(count_shift,
            5,
            net_tag1_H,
            &temp[2]);
        count_shift += 5;

        MEA_OS_insert_value(count_shift,
            5,
            0,
            &temp[2]);
        count_shift += 5;

        MEA_OS_insert_value(count_shift,
            3,
            src_port_H,
            &temp[2]);
        count_shift += 3;

        MEA_OS_insert_value(count_shift,
            1,
            src_port_M,
            &temp[2]);
        count_shift += 1;
        MEA_OS_insert_value(count_shift,
            5,
            net_tag2_H,
            &temp[2]);
        count_shift += 5;
        /*---------------------------------------------*/
        val[0] = HW_SRV_key.Hw_src_port;
        src_port_L = MEA_OS_read_value(0, 6, &val[0]);
        temp[3] = 0;
        count_shift = 0;
        MEA_OS_insert_value(count_shift,
            5,
            HW_SRV_key.Hw_L2_protocol_type,
            &temp[3]);
        count_shift += 5;

        MEA_OS_insert_value(count_shift,
            6,
            src_port_L,
            &temp[3]);
        count_shift += 6;
        MEA_OS_insert_value(count_shift,
            8,
            priority,
            &temp[3]);
        count_shift += 8;

		Hash_Val = temp[0] ^ temp[1] ^ temp[2] ^ temp[3];
		*hash_addr = Hash_Val;
#ifdef MEA_DEBUG_HASH
        printf("address19bit \n");
        for (i = 0; i < 4; i++) {
            printf("f[%d] = 0x%08x \n", i, temp[i]);
        }
        printf("hash_addr = 0x%08x  \n", Hash_Val);
        printf("hash_addr*2 = 0x%08x  \n", (Hash_Val) * 2);
        printf("hash_addr*2<<6 = 0x%08x  \n", ((Hash_Val) * 2) << 6);

        printf("baseDDR+hash_addr*2<<6 = 0x%08x  \n", MEA_SRV_BASE_ADDRES_DDR_START + (((Hash_Val) * 2) << 6));
#endif

#endif
        break;
    case 3: /*20bit address*/

#if 1

            /* start width    &array */
        val[0] = net_tag2;
        temp[0] = MEA_OS_read_value(0, 20, &val[0]);

        val[0] = net_tag1;
        temp[1] = MEA_OS_read_value(0, 20, &val[0]);
        /*---------------------------------------------*/

        val[0] = HW_SRV_key.Hw_src_port;
        src_port_H = (MEA_OS_read_value(7, 3, &val[0]));

        val[0] = net_tag2;
        net_tag2_H = MEA_OS_read_value(20, 4, &val[0]);
        val[0] = net_tag1;
        net_tag1_H = MEA_OS_read_value(20, 4, &val[0]);

        count_shift = 0;
        MEA_OS_insert_value(count_shift,
            4,
            net_tag1_H,
            &temp[2]);
        count_shift += 4;

        MEA_OS_insert_value(count_shift,
            9,
            0,
            &temp[2]);
        count_shift += 5;

        MEA_OS_insert_value(count_shift,
            3,
            src_port_H,
            &temp[2]);
        count_shift += 3;


        MEA_OS_insert_value(count_shift,
            4,
            net_tag2_H,
            &temp[2]);
        count_shift += 4;






        /*---------------------------------------------*/

        src_port_L = MEA_OS_read_value(0, 7, &val[0]);

        count_shift = 0;
        MEA_OS_insert_value(count_shift,
            5,
            HW_SRV_key.Hw_L2_protocol_type,
            &temp[3]);
        count_shift += 5;

        MEA_OS_insert_value(count_shift,
            7,
            src_port_L,
            &temp[3]);
        count_shift += 7;
        MEA_OS_insert_value(count_shift,
            8,
            priority,
            &temp[3]);
        count_shift += 8;

		Hash_Val = temp[0] ^ temp[1] ^ temp[2] ^ temp[3];
		*hash_addr = Hash_Val;

#ifdef MEA_DEBUG_HASH
        printf("address19bit \n");
        for (i = 0; i < 4; i++) {
            printf("f[%d] = 0x%08x \n", i, temp[i]);
        }
        printf("hash_addr = 0x%08x  \n", Hash_Val);
        printf("hash_addr*2 = 0x%08x  \n", Hash_Val * 2);
        printf("hash_addr*2<<6 = 0x%08x  \n", (Hash_Val * 2) << 6);

        printf("baseDDR+hash_addr*2<<6 = 0x%08x  \n", MEA_SRV_BASE_ADDRES_DDR_START + (((Hash_Val * 2) << 6)));
#endif

#endif
        break;
    }





    return MEA_OK;
}


MEA_Status mea_drv_Service_External_hash_clac_function0(MEA_Unit_t  unit_i, 
	                                                    MEA_Service_HW_SRV_Key HW_SRV_key, 
	                                                    MEA_Uint32 mode, MEA_Uint32 *hash_addr)
{
	MEA_Uint32 Hash_Val;
#ifdef MEA_DEBUG_HASH    
    MEA_Uint32 i = 0;

#endif
    //MEA_Uint32 hash_addr = 0;

    MEA_Uint32  net_tag1;
    MEA_Uint32  net_tag2;
    MEA_Uint32  priority;
    MEA_Uint32  src_port;
    MEA_Uint32  L2_type;




    MEA_Uint32 temp[5];



    MEA_Uint32  search_key_net_tag1[32];
    MEA_Uint32  search_key_net_tag2[32];
    MEA_Uint32  search_key_pri[8];
    MEA_Uint32  search_key_vsp[10];
    MEA_Uint32  ps2st_L2_type[5];


    MEA_Uint32 i;




    //MEA_Uint32                count_shift;

    *hash_addr = 0;

    MEA_OS_memset(&temp[0], 0, sizeof(temp));

    net_tag1 = HW_SRV_key.Hw_net_tag;
    net_tag1 |= HW_SRV_key.Hw_sub_protocol_type << 20;
    net_tag2 = HW_SRV_key.Hw_inner_netTag_from_value;

    priority = HW_SRV_key.Hw_priority;
    priority |= HW_SRV_key.Hw_priType << 6;
    src_port = HW_SRV_key.Hw_src_port;
    L2_type = HW_SRV_key.Hw_L2_protocol_type;




    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    MEA_OS_memset(&search_key_net_tag1, 0, sizeof(search_key_net_tag1));

    for (i = 0; i < 24; i++) {
        search_key_net_tag1[i] = ((net_tag1 >> i) & 0x1);
    }

    MEA_OS_memset(&search_key_net_tag2, 0, sizeof(search_key_net_tag2));

    for (i = 0; i < 24; i++) {
        search_key_net_tag2[i] = ((net_tag2 >> i) & 0x1);
    }

    MEA_OS_memset(&search_key_pri, 0, sizeof(search_key_pri));

    for (i = 0; i < 8; i++) {
        search_key_pri[i] = ((priority >> i) & 0x1);
    }

    MEA_OS_memset(&search_key_vsp, 0, sizeof(search_key_vsp));

    for (i = 0; i < 10; i++) {
        search_key_vsp[i] = ((src_port >> i) & 0x1);
    }

    MEA_OS_memset(&ps2st_L2_type, 0, sizeof(ps2st_L2_type));

    for (i = 0; i < 5; i++) {
        ps2st_L2_type[i] = ((L2_type >> i) & 0x1);
    }

    
 


    

    switch (mode)
    {
    case 0:  /*17bit*/
#if 1
             
        /* TBD */

        /*---------------------------------------------*/

        *hash_addr = temp[0] ^ temp[1] ^ temp[2] ^ temp[3] ^ temp[4];
#endif
        break;
    case 1:/*18 bit*/
#if 1
        /*TBD*/


        *hash_addr = temp[0] ^ temp[1] ^ temp[1] ^ temp[3];
#endif
        break;

    case 2: /*19bit address*/
#if 1


        temp[0] = 0;


        

            temp[0] |= (search_key_net_tag2[0] & 0x1) << 18;
            temp[0] |= (search_key_net_tag2[4] & 0x1) << 17;
            temp[0] |= (search_key_net_tag2[7] & 0x1) << 16;
            temp[0] |= (search_key_net_tag2[2] & 0x1) << 15; 
            temp[0] |= (search_key_net_tag2[5] & 0x1) << 14; 
            temp[0] |= (search_key_net_tag2[3] & 0x1) << 13; 
            temp[0] |= (search_key_net_tag2[1] & 0x1) << 12; 
            temp[0] |= (search_key_net_tag2[6] & 0x1) << 11; 
            temp[0] |= (search_key_net_tag2[8] & 0x1) << 10; 
            temp[0] |= (search_key_net_tag1[5] & 0x1) << 9; 
            temp[0] |= (search_key_vsp[3] & 0x1) << 8;
            temp[0] |= (search_key_net_tag1[3] & 0x1) << 7;
            temp[0] |= (search_key_net_tag1[7] & 0x1) << 6;
            temp[0] |= (search_key_net_tag1[6] & 0x1) << 5;
            temp[0] |= (search_key_net_tag1[4] & 0x1) << 4;
            temp[0] |= (search_key_net_tag1[1] & 0x1) << 3;
            temp[0] |= (search_key_net_tag1[0] & 0x1) << 2;
            temp[0] |= (search_key_net_tag1[8] & 0x1) << 1;
            temp[0] |= (search_key_net_tag1[2] & 0x1) << 0;
            /**********************************************/


            temp[1] |= (search_key_vsp[5]           & 0x1) << 18;
            temp[1] |= (search_key_net_tag2[9]      & 0x1) << 17;
            temp[1] |= (search_key_net_tag2[12]     & 0x1) << 16;
            temp[1] |= (search_key_net_tag2[16]     & 0x1) << 15;
            temp[1] |= (search_key_net_tag2[15]     & 0x1) << 14;
            temp[1] |= (search_key_net_tag2[13]     & 0x1) << 13;
            temp[1] |= (search_key_net_tag2[10]     & 0x1) << 12;
            temp[1] |= (search_key_net_tag2[17]     & 0x1) << 11;
            temp[1] |= (search_key_vsp[2]           & 0x1) << 10;
            temp[1] |= (search_key_net_tag1[10]     & 0x1) << 9;
            temp[1] |= (search_key_net_tag1[14]     & 0x1) << 8;
            temp[1] |= (search_key_vsp[1]           & 0x1) << 7;
            temp[1] |= (search_key_net_tag1[9]      & 0x1) << 6;
            temp[1] |= (search_key_net_tag1[11]     & 0x1) << 5;
            temp[1] |= (search_key_net_tag1[15]     & 0x1) << 4;
            temp[1] |= (search_key_net_tag1[12]     & 0x1) << 3;
            temp[1] |= (search_key_net_tag1[16]     & 0x1) << 2;
            temp[1] |= (search_key_vsp[6]           & 0x1) << 1;
            temp[1] |= (search_key_net_tag1[13]     & 0x1) << 0;

            /**********************************************/

            temp[2] |= (search_key_net_tag2[19]         & 0x1) << 18;
            temp[2] |= (search_key_net_tag1[17]         & 0x1) << 17;
            temp[2] |= (search_key_vsp[0]               & 0x1) << 16;
            temp[2] |= (search_key_net_tag1[18]         & 0x1) << 15;
            temp[2] |= (search_key_pri[0]               & 0x1) << 14;
            temp[2] |= (search_key_net_tag1[19]         & 0x1) << 13;
            temp[2] |= (search_key_vsp[4]               & 0x1) << 12;
            temp[2] |= (search_key_net_tag2[20]         & 0x1) << 11;
            temp[2] |= (search_key_net_tag1[20]         & 0x1) << 10;
            temp[2] |= (search_key_net_tag2[21]         & 0x1) << 9;
            temp[2] |= (search_key_pri[6]               & 0x1) << 8;
            temp[2] |= (search_key_pri[4]               & 0x1) << 7;
            temp[2] |= (search_key_pri[5]               & 0x1) << 6;
            temp[2] |= (search_key_pri[2]               & 0x1) << 5;
            temp[2] |= (search_key_pri[1]               & 0x1) << 4;
            temp[2] |= (search_key_pri[3]               & 0x1) << 3;
            temp[2] |= (search_key_net_tag1[22]         & 0x1) << 2;
            temp[2] |= (search_key_net_tag1[21]         & 0x1) << 1;
            temp[2] |= (search_key_net_tag2[22]         & 0x1) << 0;


            /**********************************************/

            temp[3] |= (search_key_net_tag2[11]         & 0x1) << 18;
            temp[3] |= (search_key_net_tag2[14]         & 0x1) << 17;
            temp[3] |= (search_key_net_tag2[18]         & 0x1) << 16;
            temp[3] |= (ps2st_L2_type[3]                & 0x1) << 15;
            temp[3] |= (ps2st_L2_type[1]                & 0x1) << 14;
            temp[3] |= (ps2st_L2_type[2]                & 0x1) << 13;
            temp[3] |= (ps2st_L2_type[4]                & 0x1) << 12;
            temp[3] |= (ps2st_L2_type[0]                & 0x1) << 11;
            temp[3] |= (search_key_pri[7]               & 0x1) << 10;
            temp[3] |= (search_key_net_tag1[23]         & 0x1) << 9;
            temp[3] |= (search_key_net_tag2[23]         & 0x1) << 8;
            temp[3] |= (search_key_net_tag2[19]         & 0x1) << 7;
            temp[3] |= (search_key_vsp[7]               & 0x1) << 6;
            temp[3] |= (search_key_net_tag2[0]          & 0x1) << 5;
            temp[3] |= (search_key_net_tag2[18]         & 0x1) << 4;
            temp[3] |= (search_key_vsp[8]               & 0x1) << 3;
            temp[3] |= (search_key_net_tag2[14]         & 0x1) << 2;
            temp[3] |= (search_key_vsp[9]               & 0x1) << 1;
            temp[3] |= (search_key_net_tag2[11]         & 0x1) << 0;
            






      

        
		Hash_Val = temp[0] ^ temp[1] ^ temp[2] ^ temp[3];
		*hash_addr = Hash_Val;
#ifdef MEA_DEBUG_HASH
        printf("address19bit \n");
        for (i = 0; i < 4; i++) {
            printf("f[%d] = 0x%08x \n", i, temp[i]);
        }
        printf("hash_addr = 0x%08x  \n", Hash_Val);
        printf("hash_addr*2 = 0x%08x  \n", Hash_Val * 2);
        printf("hash_addr*2<<6 = 0x%08x  \n", (Hash_Val * 2) << 6);

        printf("baseDDR+hash_addr*2<<6 = 0x%08x  \n", MEA_SRV_BASE_ADDRES_DDR_START + ((Hash_Val * 2) << 6));
#endif

#endif
        break;
    case 3: /*20bit address*/

#if 1

   

		Hash_Val = temp[0] ^ temp[1] ^ temp[2] ^ temp[3];
		*hash_addr = Hash_Val;

#ifdef MEA_DEBUG_HASH
        printf("address19bit \n");
        for (i = 0; i < 4; i++) {
            printf("f[%d] = 0x%08x \n", i, temp[i]);
        }
        printf("hash_addr = 0x%08x  \n", Hash_Val);
        printf("hash_addr*2 = 0x%08x  \n", Hash_Val * 2);
        printf("hash_addr*2<<6 = 0x%08x  \n", (Hash_Val * 2) << 6);

        printf("baseDDR+hash_addr*2<<6 = 0x%08x  \n", MEA_SRV_BASE_ADDRES_DDR_START + ((Hash_Val * 2) << 6));
#endif

#endif
        break;
    }





    return MEA_OK;
}

MEA_Status mea_drv_Service_External_hash_clac_function1(MEA_Unit_t  unit_i, MEA_Service_HW_SRV_Key HW_SRV_key, MEA_Uint32 mode, MEA_Uint32 *hash_addr)
{

#ifdef MEA_DEBUG_HASH    
    MEA_Uint32 i = 0;
#endif

    MEA_Uint32 SRV_E_hash;

    MEA_Uint32  net_tag1;
    MEA_Uint32  net_tag2;
    MEA_Uint32  priority;
    MEA_Uint32  src_port;
    MEA_Uint32  L2_type;
    MEA_Uint32  ipv6tag;

    MEA_Uint32 netTag_change;


    MEA_Uint32 temp[10];



    MEA_Uint32  search_key_net_tag1[32];
    MEA_Uint32  search_key_net_tag2[32];
    MEA_Uint32  search_key_pri[8];
    MEA_Uint32  search_key_vsp[10];
    MEA_Uint32  ps2st_L2_type[5];
    MEA_Uint32  search_key_ipv6tag[32];

    MEA_Uint32 Zero = 0;

    MEA_Uint32 i;

    MEA_Uint32  key1[32];  // net_tag1
    MEA_Uint32  key2[32];  // net_tag2
    MEA_Uint32  key3[32];  // src_port
    MEA_Uint32  key4[32];  // pri
    MEA_Uint32  key5[32];  // L2_type
    MEA_Uint32  key6[32];  // ipv6
    MEA_Uint32  key7[32];  // net_tag1
    MEA_Uint32  key8[32];  // net_tag2


    //MEA_Uint32                count_shift;

    *hash_addr = 0;

    MEA_OS_memset(&temp[0], 0, sizeof(temp));

    net_tag1 = HW_SRV_key.Hw_net_tag;
    net_tag1 |= HW_SRV_key.Hw_sub_protocol_type << 20;
    net_tag2 = HW_SRV_key.Hw_inner_netTag_from_value;

    priority = HW_SRV_key.Hw_priority;
    priority |= HW_SRV_key.Hw_priType << 6;
    src_port = HW_SRV_key.Hw_src_port;
    L2_type = HW_SRV_key.Hw_L2_protocol_type;

    ipv6tag = HW_SRV_key.HW_Ipv6msb_tag;

    SRV_E_hash = mea_drv_get_SRV_E_hash(unit_i);

    switch (SRV_E_hash)
    {
    case 0:
       
    case 1:

        break;
    case 2:
        //  left shift 13 of net_tag1 - Net_tag1 = { search_key_net_tag1_int[10:0],search_key_net_tag1_int[23:11] }
        //  left shift 5 of net_tag2 � Net_tag2 = { search_key_net_tag2_int[18:0],search_key_net_tag2_int[23:19] }

        netTag_change = 0;
        netTag_change = (net_tag1 >> 11) & 0x1fff;
        netTag_change |= ((net_tag1) & 0x7ff) << 13;
        net_tag1 = netTag_change;

        netTag_change = 0;
        netTag_change = (net_tag2 >> 19) & 0x1f;
        netTag_change |= ((net_tag2) & 0x7ffff) << 5;
        net_tag2 = netTag_change;


        break;
    case 3:
        //  left shift 17 of net_tag1 - Net_tag1 = { search_key_net_tag1_int[6:0],search_key_net_tag1_int[23:7] }
        //  left shift 7 of net_tag2 � Net_tag2 = { search_key_net_tag2_int[16:0],search_key_net_tag2_int[23:17] }

        netTag_change = 0;
        netTag_change = (net_tag1 >> 7) & 0x1ffff;
        netTag_change |= ((net_tag1) & 0x7f) << 17;
        net_tag1 = netTag_change;

        netTag_change = 0;
        netTag_change = (net_tag2 >> 17) & 0x7f;
        netTag_change |= ((net_tag2) & 0x1ffff) << 7;
        net_tag2 = netTag_change;


        break;

    default:
        break;
    }



    MEA_OS_memset(&key1, 0, sizeof(key1));
    MEA_OS_memset(&key2, 0, sizeof(key2));
    MEA_OS_memset(&key3, 0, sizeof(key3));
    MEA_OS_memset(&key4, 0, sizeof(key4));
    MEA_OS_memset(&key5, 0, sizeof(key5));
    MEA_OS_memset(&key6, 0, sizeof(key6));
    MEA_OS_memset(&key7, 0, sizeof(key7));
    MEA_OS_memset(&key8, 0, sizeof(key7));


    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    MEA_OS_memset(&search_key_net_tag1, 0, sizeof(search_key_net_tag1));


    for (i = 0; i < 24; i++) {
        search_key_net_tag1[i] = ((net_tag1 >> i) & 0x1);
        key1[i] = search_key_net_tag1[i];
        if (i <= 8)
            key7[i] = search_key_net_tag1[i];

    }

    MEA_OS_memset(&search_key_net_tag2, 0, sizeof(search_key_net_tag2));

    for (i = 0; i < 24; i++) {
        search_key_net_tag2[i] = ((net_tag2 >> i) & 0x1);
        key2[i] = search_key_net_tag2[i];
        if (i <= 8)
            key8[i] = key2[i];
    }

    MEA_OS_memset(&search_key_pri, 0, sizeof(search_key_pri));

    for (i = 0; i < 8; i++) {
        search_key_pri[i] = ((priority >> i) & 0x1);
        key4[i] = search_key_pri[i];
    }

    MEA_OS_memset(&search_key_vsp, 0, sizeof(search_key_vsp));

    for (i = 0; i < 10; i++) {
        search_key_vsp[i] = ((src_port >> i) & 0x1);
        key3[i] = search_key_vsp[i];
    }

    MEA_OS_memset(&ps2st_L2_type, 0, sizeof(ps2st_L2_type));

    for (i = 0; i < 5; i++) {
        ps2st_L2_type[i] = ((L2_type >> i) & 0x1);
        key5[i] = ps2st_L2_type[i];
    }

    MEA_OS_memset(&search_key_ipv6tag, 0, sizeof(search_key_ipv6tag));
    for (i = 0; i < 27; i++) {
      search_key_ipv6tag[i] = ((ipv6tag >> i) & 0x1);
      key6[i] = search_key_ipv6tag[i];
    }




    switch (mode)
    {
    case 0:  /*17bit*/
#if 1
             /*17bit address*/

    
            
        temp[0] = 0;
        temp[0] |= (search_key_vsp[9]       & 0x1) << 16;
        temp[0] |= (search_key_net_tag2[1]  & 0x1) << 15;
        temp[0] |= (search_key_net_tag2[7]  & 0x1) << 14;
        temp[0] |= (search_key_net_tag2[3]  & 0x1) << 13;
        temp[0] |= (search_key_net_tag2[6]  & 0x1) << 12;
        temp[0] |= (search_key_net_tag2[4]  & 0x1) << 11;
        temp[0] |= (search_key_net_tag2[0]  & 0x1) << 10;
        temp[0] |= (search_key_net_tag2[2]  & 0x1) << 9;
        temp[0] |= (search_key_net_tag2[5]  & 0x1) << 8;
        temp[0] |= (search_key_net_tag1[7]  & 0x1) << 7;
        temp[0] |= (search_key_net_tag1[0]  & 0x1) << 6;
        temp[0] |= (search_key_net_tag1[3]  & 0x1) << 5;
        temp[0] |= (search_key_net_tag1[1]  & 0x1) << 4;
        temp[0] |= (search_key_net_tag1[5]  & 0x1) << 3;
        temp[0] |= (search_key_net_tag1[6]  & 0x1) << 2;
        temp[0] |= (search_key_net_tag1[2]  & 0x1) << 1;
        temp[0] |= (search_key_net_tag1[4]  & 0x1) << 0;
        /**********************************************/

        temp[1] = 0;

        temp[1] |= (search_key_vsp[4]       & 0x1) << 16;
        temp[1] |= (search_key_net_tag1[9]  & 0x1) << 15;
        temp[1] |= (search_key_net_tag1[11] & 0x1) << 14;
        temp[1] |= (search_key_net_tag1[15] & 0x1) << 13;
        temp[1] |= (search_key_net_tag1[8]  & 0x1) << 12;
        temp[1] |= (search_key_net_tag1[14] & 0x1) << 11;
        temp[1] |= (search_key_net_tag1[12] & 0x1) << 10;
        temp[1] |= (search_key_net_tag1[10] & 0x1) << 9;
        temp[1] |= (search_key_net_tag1[13] & 0x1) << 8;
        temp[1] |= (search_key_net_tag2[8]  & 0x1) << 7;
        temp[1] |= (search_key_net_tag2[10] & 0x1) << 6;
        temp[1] |= (search_key_net_tag2[9]  & 0x1) << 5;
        temp[1] |= (search_key_net_tag2[14] & 0x1) << 4;
        temp[1] |= (search_key_net_tag2[11] & 0x1) << 3;
        temp[1] |= (search_key_net_tag2[13] & 0x1) << 2;
        temp[1] |= (search_key_net_tag2[12] & 0x1) << 1;
        temp[1] |= (search_key_net_tag2[15] & 0x1) << 0;

        /**********************************************/
        temp[2] = 0;

        temp[2] |= (search_key_net_tag1[19] & 0x1) << 16;
        temp[2] |= (search_key_net_tag1[22] & 0x1) << 15;
        temp[2] |= (search_key_net_tag1[16] & 0x1) << 14;
        temp[2] |= (search_key_net_tag1[20] & 0x1) << 13;
        temp[2] |= (search_key_net_tag1[17] & 0x1) << 12;
        temp[2] |= (search_key_net_tag1[21] & 0x1) << 11;
        temp[2] |= (search_key_net_tag1[23] & 0x1) << 10;
        temp[2] |= (search_key_net_tag1[18] & 0x1) << 9;
        temp[2] |= (search_key_net_tag2[21] & 0x1) << 8;
        temp[2] |= (search_key_net_tag2[18] & 0x1) << 7;
        temp[2] |= (search_key_net_tag2[23] & 0x1) << 6;
        temp[2] |= (search_key_net_tag2[16] & 0x1) << 5;
        temp[2] |= (search_key_net_tag2[20] & 0x1) << 4;
        temp[2] |= (search_key_net_tag2[17] & 0x1) << 3;
        temp[2] |= (search_key_net_tag2[22] & 0x1) << 2;
        temp[2] |= (search_key_net_tag2[19] & 0x1) << 1;
        temp[2] |= (search_key_vsp[2] & 0x1) << 0;


        /**********************************************/
        temp[3] = 0;

        temp[3] |= (search_key_pri[4] & 0x1) << 16;
        temp[3] |= (search_key_pri[3] & 0x1) << 15;
        temp[3] |= (search_key_pri[7] & 0x1) << 14;
        temp[3] |= (search_key_pri[5] & 0x1) << 13;
        temp[3] |= (search_key_pri[6] & 0x1) << 12;
        temp[3] |= (search_key_pri[0] & 0x1) << 11;
        temp[3] |= (search_key_pri[2] & 0x1) << 10;
        temp[3] |= (search_key_pri[1] & 0x1) << 9;
        temp[3] |= (search_key_vsp[8] & 0x1) << 8;
        temp[3] |= (search_key_vsp[5] & 0x1) << 7;
        temp[3] |= (search_key_vsp[3] & 0x1) << 6;
        temp[3] |= (search_key_vsp[0] & 0x1) << 5;
        temp[3] |= (ps2st_L2_type[2]  & 0x1) << 4;
        temp[3] |= (ps2st_L2_type[1]  & 0x1) << 3;
        temp[3] |= (ps2st_L2_type[4]  & 0x1) << 2;
        temp[3] |= (ps2st_L2_type[0]  & 0x1) << 1;
        temp[3] |= (ps2st_L2_type[3]  & 0x1) << 0;

        temp[4] = 0;

        temp[4] |= (Zero                & 0x1) << 16;
        temp[4] |= (Zero                & 0x1) << 15;
        temp[4] |= (search_key_vsp[7]   & 0x1) << 14;
        temp[4] |= (Zero                & 0x1) << 13;
        temp[4] |= (Zero                & 0x1) << 12;
        temp[4] |= (search_key_vsp[6]   & 0x1) << 11;
        temp[4] |= (Zero                & 0x1) << 10;
        temp[4] |= (Zero                & 0x1) << 9;
        temp[4] |= (search_key_vsp[1]   & 0x1) << 8;
        temp[4] |= (Zero                & 0x1) << 7;
        temp[4] |= (Zero                & 0x1) << 6;
        temp[4] |= (Zero                & 0x1) << 5;
        temp[4] |= (Zero                & 0x1) << 4;
        temp[4] |= (Zero                & 0x1) << 3;
        temp[4] |= (Zero                & 0x1) << 2;
        temp[4] |= (Zero                & 0x1) << 1;
        temp[4] |= (Zero                & 0x1) << 0;

        *hash_addr = temp[0] ^ temp[1] ^ temp[2] ^ temp[3] ^ temp[4];


#endif 
        break;
    case 1:/*18 bit*/
#if 1

           /*18bit address*/

        temp[0] = 0;

        temp[0] |= (search_key_vsp[7]       & 0x1) << 17;
        temp[0] |= (search_key_vsp[9]       & 0x1) << 16;
        temp[0] |= (search_key_net_tag2[1]  & 0x1) << 15;
        temp[0] |= (search_key_net_tag2[7]  & 0x1) << 14;
        temp[0] |= (search_key_net_tag2[3]  & 0x1) << 13;
        temp[0] |= (search_key_net_tag2[6]  & 0x1) << 12;
        temp[0] |= (search_key_net_tag2[4]  & 0x1) << 11;
        temp[0] |= (search_key_net_tag2[0]  & 0x1) << 10;
        temp[0] |= (search_key_net_tag2[2]  & 0x1) << 9;
        temp[0] |= (search_key_net_tag2[5]  & 0x1) << 8;
        temp[0] |= (search_key_net_tag1[7]  & 0x1) << 7;
        temp[0] |= (search_key_net_tag1[0]  & 0x1) << 6;
        temp[0] |= (search_key_net_tag1[3]  & 0x1) << 5;
        temp[0] |= (search_key_net_tag1[1]  & 0x1) << 4;
        temp[0] |= (search_key_net_tag1[5]  & 0x1) << 3;
        temp[0] |= (search_key_net_tag1[6]  & 0x1) << 2;
        temp[0] |= (search_key_net_tag1[2]  & 0x1) << 1;
        temp[0] |= (search_key_net_tag1[4]  & 0x1) << 0;
        /**********************************************/

        temp[1] = 0;

        temp[1] |= (search_key_vsp[6]           & 0x1) << 17;
        temp[1] |= (search_key_vsp[4]           & 0x1) << 16;
        temp[1] |= (search_key_net_tag1[9]      & 0x1) << 15;
        temp[1] |= (search_key_net_tag1[11]     & 0x1) << 14;
        temp[1] |= (search_key_net_tag1[15]     & 0x1) << 13;
        temp[1] |= (search_key_net_tag1[8]      & 0x1) << 12;
        temp[1] |= (search_key_net_tag1[14]     & 0x1) << 11;
        temp[1] |= (search_key_net_tag1[12]     & 0x1) << 10;
        temp[1] |= (search_key_net_tag1[10]     & 0x1) << 9;
        temp[1] |= (search_key_net_tag1[13]     & 0x1) << 8;
        temp[1] |= (search_key_net_tag2[8]      & 0x1) << 7;
        temp[1] |= (search_key_net_tag2[10]     & 0x1) << 6;
        temp[1] |= (search_key_net_tag2[9]      & 0x1) << 5;
        temp[1] |= (search_key_net_tag2[14]     & 0x1) << 4;
        temp[1] |= (search_key_net_tag2[11]     & 0x1) << 3;
        temp[1] |= (search_key_net_tag2[13]     & 0x1) << 2;
        temp[1] |= (search_key_net_tag2[12]     & 0x1) << 1;
        temp[1] |= (search_key_net_tag2[15]     & 0x1) << 0;

        /**********************************************/
        temp[2] = 0;

        temp[2] |= (search_key_net_tag1[19] & 0x1) << 17;
        temp[2] |= (search_key_net_tag1[22] & 0x1) << 16;
        temp[2] |= (search_key_net_tag1[16] & 0x1) << 15;
        temp[2] |= (search_key_net_tag1[20] & 0x1) << 14;
        temp[2] |= (search_key_net_tag1[17] & 0x1) << 13;
        temp[2] |= (search_key_net_tag1[21] & 0x1) << 12;
        temp[2] |= (search_key_net_tag1[23] & 0x1) << 11;
        temp[2] |= (search_key_net_tag1[18] & 0x1) << 10;
        temp[2] |= (search_key_net_tag2[21] & 0x1) << 9;
        temp[2] |= (search_key_net_tag2[18] & 0x1) << 8;
        temp[2] |= (search_key_net_tag2[23] & 0x1) << 7;
        temp[2] |= (search_key_net_tag2[16] & 0x1) << 6;
        temp[2] |= (search_key_net_tag2[20] & 0x1) << 5;
        temp[2] |= (search_key_net_tag2[17] & 0x1) << 4;
        temp[2] |= (search_key_net_tag2[22] & 0x1) << 3;
        temp[2] |= (search_key_net_tag2[19] & 0x1) << 2;
        temp[2] |= (search_key_vsp[2] & 0x1) << 1;
        temp[2] |= (search_key_vsp[1] & 0x1) << 0;


        /**********************************************/
        temp[3] = 0;

        temp[3] |= (search_key_pri[4] & 0x1) << 17;
        temp[3] |= (search_key_pri[3] & 0x1) << 16;
        temp[3] |= (search_key_pri[7] & 0x1) << 15;
        temp[3] |= (search_key_pri[5] & 0x1) << 14;
        temp[3] |= (search_key_pri[6] & 0x1) << 13;
        temp[3] |= (search_key_pri[0] & 0x1) << 12;
        temp[3] |= (search_key_pri[2] & 0x1) << 11;
        temp[3] |= (search_key_pri[1] & 0x1) << 10;
        temp[3] |= (search_key_vsp[8]  &0x1) << 9;
        temp[3] |= (search_key_vsp[5] & 0x1) << 8;
        temp[3] |= (search_key_vsp[3] & 0x1) << 7;
        temp[3] |= (search_key_vsp[0] & 0x1) << 6;
        temp[3] |= (ps2st_L2_type[2] & 0x1) << 5;
        temp[3] |= (ps2st_L2_type[1] & 0x1) << 4;
        temp[3] |= (ps2st_L2_type[4] & 0x1) << 3;
        temp[3] |= (ps2st_L2_type[0] & 0x1) << 2;
        temp[3] |= (ps2st_L2_type[3] & 0x1) << 1;
        temp[3] |= (Zero & 0x1) << 0;


        *hash_addr = temp[0] ^ temp[1] ^ temp[2] ^ temp[3];


#endif
        break;

    case 2: /*19bit address*/
#if 1
            /*19bit address*/
        if (SRV_E_hash == 1) {

            temp[0] = 0;
            temp[0] |= (key1[13] & 0x1) << 18;
            temp[0] |= (key1[07] & 0x1) << 17;
            temp[0] |= (key1[15] & 0x1) << 16;
            temp[0] |= (key1[17] & 0x1) << 15;
            temp[0] |= (key1[5]  & 0x1) << 14;
            temp[0] |= (key1[12] & 0x1) << 13;
            temp[0] |= (key1[18] & 0x1) << 12;
            temp[0] |= (key1[6]  & 0x1) << 11;
            temp[0] |= (key1[2]  & 0x1) << 10;
            temp[0] |= (key1[11] & 0x1) << 9;
            temp[0] |= (key1[14] & 0x1) << 8;
            temp[0] |= (key1[9]  & 0x1) << 7;
            temp[0] |= (key1[1]  & 0x1) << 6;
            temp[0] |= (key1[16] & 0x1) << 5;
            temp[0] |= (key1[0]  & 0x1) << 4;
            temp[0] |= (key1[3]  & 0x1) << 3;
            temp[0] |= (key1[8]  & 0x1) << 2;
            temp[0] |= (key1[10] & 0x1) << 1;
            temp[0] |= (key1[4]  & 0x1) << 0;
            /**********************************************/

            temp[1] = 0;
            temp[1] |= (key2[8]  & 0x1) << 18;
            temp[1] |= (key2[12] & 0x1) << 17;
            temp[1] |= (key2[4]  & 0x1) << 16;
            temp[1] |= (key2[5]  & 0x1) << 15;
            temp[1] |= (key2[15] & 0x1) << 14;
            temp[1] |= (key2[0]  & 0x1) << 13;
            temp[1] |= (key2[9]  & 0x1) << 12;
            temp[1] |= (key2[16] & 0x1) << 11;
            temp[1] |= (key2[17] & 0x1) << 10;
            temp[1] |= (key2[6]  & 0x1) << 9;
            temp[1] |= (key2[3]  & 0x1) << 8;
            temp[1] |= (key2[10] & 0x1) << 7;
            temp[1] |= (key2[13] & 0x1) << 6;
            temp[1] |= (key2[1]  & 0x1) << 5;
            temp[1] |= (key2[7]  & 0x1) << 4;
            temp[1] |= (key2[14] & 0x1) << 3;
            temp[1] |= (key2[18] & 0x1) << 2;
            temp[1] |= (key2[2]  & 0x1) << 1;
            temp[1] |= (key2[11] & 0x1) << 0;

            /**********************************************/

            temp[2] = 0;
            temp[2] |= (key5[2] & 0x1) << 18;
            temp[2] |= (key4[6] & 0x1) << 17;
            temp[2] |= (key4[7] & 0x1) << 16;
            temp[2] |= (key4[4] & 0x1) << 15;
            temp[2] |= (key4[2] & 0x1) << 14;
            temp[2] |= (key4[3] & 0x1) << 13;
            temp[2] |= (key4[0] & 0x1) << 12;
            temp[2] |= (key4[1] & 0x1) << 11;
            temp[2] |= (key4[5] & 0x1) << 10;
            temp[2] |= (key5[3] & 0x1) << 9;
            temp[2] |= (key5[4] & 0x1) << 8;
#ifdef KEY5_TYPE
             temp[2] |= (key5[0] & 0x1) << 7;
 #else
            temp[2] |= (key5[5] & 0x1) << 7;
#endif 
            temp[2] |= (key3[8] & 0x1) << 6;
            temp[2] |= (key5[1] & 0x1) << 5;
            temp[2] |= (key5[3] & 0x1) << 4;
            temp[2] |= (key5[4] & 0x1) << 3;
            temp[2] |= (key5[1] & 0x1) << 2;
            temp[2] |= (key5[2] & 0x1) << 1;
            temp[2] |= (key5[0] & 0x1) << 0;


            /**********************************************/
            temp[3] = 0;
            temp[3] |= (key1[23] & 0x1) << 18;
            temp[3] |= (key1[11] & 0x1) << 17;
            temp[3] |= (key1[20] & 0x1) << 16;
            temp[3] |= (key1[21] & 0x1) << 15;
            temp[3] |= (key1[12] & 0x1) << 14;
            temp[3] |= (key1[9]  & 0x1) << 13;
            temp[3] |= (key1[22] & 0x1) << 12;
            temp[3] |= (key1[10] & 0x1) << 11;
            temp[3] |= (key1[13] & 0x1) << 10;
            temp[3] |= (key1[14] & 0x1) << 9;
            temp[3] |= (key4[4]  & 0x1) << 8;
            temp[3] |= (key4[7]  & 0x1) << 7;
            temp[3] |= (key4[3]  & 0x1) << 6;
            temp[3] |= (key4[5]  & 0x1) << 5;
            temp[3] |= (key4[1]  & 0x1) << 4;
            temp[3] |= (key4[2]  & 0x1) << 3;
            temp[3] |= (key4[6]  & 0x1) << 2;
            temp[3] |= (key4[0]  & 0x1) << 1;
            temp[3] |= (key1[15] & 0x1) << 0;
            /**********************************************/
            temp[4] = 0;
            temp[4] |= (key3[9]  & 0x1) << 18;
            temp[4] |= (key3[5]  & 0x1) << 17;
            temp[4] |= (key3[2]  & 0x1) << 16;
            temp[4] |= (key3[3]  & 0x1) << 15;
            temp[4] |= (key3[1]  & 0x1) << 14;
            temp[4] |= (key3[0]  & 0x1) << 13;
            temp[4] |= (key3[4]  & 0x1) << 12;
            temp[4] |= (key3[6]  & 0x1) << 11;
            temp[4] |= (key3[7]  & 0x1) << 10;
            temp[4] |= (key2[22] & 0x1) << 9;
            temp[4] |= (key2[21] & 0x1) << 8;
            temp[4] |= (key2[20] & 0x1) << 7;
            temp[4] |= (key2[23] & 0x1) << 6;
            temp[4] |= (key2[19] & 0x1) << 5;
            temp[4] |= (key1[21] & 0x1) << 4;
            temp[4] |= (key1[23] & 0x1) << 3;
            temp[4] |= (key1[19] & 0x1) << 2;
            temp[4] |= (key1[22] & 0x1) << 1;
            temp[4] |= (key1[20] & 0x1) << 0;
            /**********************************************/

            temp[5] = 0;
            temp[5] |= (key7[0] & 0x1) << 18;
            temp[5] |= (key8[5] & 0x1) << 17;
            temp[5] |= (key7[3] & 0x1) << 16;
            temp[5] |= (key7[7] & 0x1) << 15;
            temp[5] |= (key8[4] & 0x1) << 14;
            temp[5] |= (key7[1] & 0x1) << 13;
            temp[5] |= (key7[6] & 0x1) << 12;
            temp[5] |= (key8[6] & 0x1) << 11;
            temp[5] |= (key8[3] & 0x1) << 10;
            temp[5] |= (key7[2] & 0x1) << 9;
            temp[5] |= (key7[4] & 0x1) << 8;
            temp[5] |= (key8[1] & 0x1) << 7;
            temp[5] |= (key8[8] & 0x1) << 6;
            temp[5] |= (key7[5] & 0x1) << 5;
            temp[5] |= (Zero    & 0x1) << 4;
            temp[5] |= (key8[2] & 0x1) << 3;
            temp[5] |= (key8[0] & 0x1) << 2;
            temp[5] |= (key7[8] & 0x1) << 1;
            temp[5] |= (key8[7] & 0x1) << 0;
            /**********************************************/
            temp[6] = 0;
            temp[6] |= (key6[3]  & 0x1) << 18;
            temp[6] |= (key6[13] & 0x1) << 17;
            temp[6] |= (key6[11] & 0x1) << 16;
            temp[6] |= (key6[8]  & 0x1) << 15;
            temp[6] |= (key6[0]  & 0x1) << 14;
            temp[6] |= (key6[6]  & 0x1) << 13;
            temp[6] |= (key6[14] & 0x1) << 12;
            temp[6] |= (key6[2]  & 0x1) << 11;
            temp[6] |= (key6[9]  & 0x1) << 10;
            temp[6] |= (key6[4]  & 0x1) << 9;
            temp[6] |= (key6[17] & 0x1) << 8;
            temp[6] |= (key6[15] & 0x1) << 7;
            temp[6] |= (key6[1]  & 0x1) << 6;
            temp[6] |= (key6[18] & 0x1) << 5;
            temp[6] |= (key6[16] & 0x1) << 4;
            temp[6] |= (key6[10] & 0x1) << 3;
            temp[6] |= (key6[5]  & 0x1) << 2;
            temp[6] |= (key6[12] & 0x1) << 1;
            temp[6] |= (key6[7]  & 0x1) << 0;
            /**********************************************/
            temp[7] = 0;
            temp[7] |= (Zero     & 0x1) << 18;
            temp[7] |= (Zero     & 0x1) << 17;
            temp[7] |= (key6[21] & 0x1) << 16;
            temp[7] |= (Zero     & 0x1) << 15;
            temp[7] |= (Zero     & 0x1) << 14;
            temp[7] |= (Zero     & 0x1) << 13;
            temp[7] |= (key6[23] & 0x1) << 12;
            temp[7] |= (Zero     & 0x1) << 11;
            temp[7] |= (key6[20] & 0x1) << 10;
            temp[7] |= (key6[25] & 0x1) << 9;
            temp[7] |= (Zero     & 0x1) << 8;
            temp[7] |= (key6[22] & 0x1) << 7;
            temp[7] |= (Zero     & 0x1) << 6;
            temp[7] |= (key6[26] & 0x1) << 5;
            temp[7] |= (Zero     & 0x1) << 4;
            temp[7] |= (Zero     & 0x1) << 3;
            temp[7] |= (key6[19] & 0x1) << 2;
            temp[7] |= (Zero     & 0x1) << 1;
            temp[7] |= (key6[24] & 0x1) << 0;







            if (MEA_PARSER_SUPPORT_IPV6 == MEA_FALSE)
                *hash_addr = temp[0] ^ temp[1] ^ temp[2] ^ temp[3] ^ temp[4] ^ temp[5];
            else {
                *hash_addr = temp[0] ^ temp[1] ^ temp[2] ^ temp[3] ^ temp[4] ^ temp[5] ^ temp[6] ^ temp[7];

            }




        }
        else { /*hush 0 */
            temp[0] = 0;
            temp[0] |= (search_key_vsp[8] & 0x1) << 18;
            temp[0] |= (search_key_vsp[7] & 0x1) << 17;
            temp[0] |= (search_key_vsp[9] & 0x1) << 16;
            temp[0] |= (search_key_net_tag2[1] & 0x1) << 15;
            temp[0] |= (search_key_net_tag2[7] & 0x1) << 14;
            temp[0] |= (search_key_net_tag2[3] & 0x1) << 13;
            temp[0] |= (search_key_net_tag2[6] & 0x1) << 12;
            temp[0] |= (search_key_net_tag2[4] & 0x1) << 11;
            temp[0] |= (search_key_net_tag2[0] & 0x1) << 10;
            temp[0] |= (search_key_net_tag2[2] & 0x1) << 9;
            temp[0] |= (search_key_net_tag2[5] & 0x1) << 8;
            temp[0] |= (search_key_net_tag1[7] & 0x1) << 7;
            temp[0] |= (search_key_net_tag1[0] & 0x1) << 6;
            temp[0] |= (search_key_net_tag1[3] & 0x1) << 5;
            temp[0] |= (search_key_net_tag1[1] & 0x1) << 4;
            temp[0] |= (search_key_net_tag1[5] & 0x1) << 3;
            temp[0] |= (search_key_net_tag1[6] & 0x1) << 2;
            temp[0] |= (search_key_net_tag1[2] & 0x1) << 1;
            temp[0] |= (search_key_net_tag1[4] & 0x1) << 0;
            /**********************************************/

            temp[1] = 0;
            temp[1] |= (search_key_vsp[5] & 0x1) << 18;
            temp[1] |= (search_key_vsp[6] & 0x1) << 17;
            temp[1] |= (search_key_vsp[4] & 0x1) << 16;
            temp[1] |= (search_key_net_tag1[9] & 0x1) << 15;
            temp[1] |= (search_key_net_tag1[11] & 0x1) << 14;
            temp[1] |= (search_key_net_tag1[15] & 0x1) << 13;
            temp[1] |= (search_key_net_tag1[8] & 0x1) << 12;
            temp[1] |= (search_key_net_tag1[14] & 0x1) << 11;
            temp[1] |= (search_key_net_tag1[12] & 0x1) << 10;
            temp[1] |= (search_key_net_tag1[10] & 0x1) << 9;
            temp[1] |= (search_key_net_tag1[13] & 0x1) << 8;
            temp[1] |= (search_key_net_tag2[8] & 0x1) << 7;
            temp[1] |= (search_key_net_tag2[10] & 0x1) << 6;
            temp[1] |= (search_key_net_tag2[9] & 0x1) << 5;
            temp[1] |= (search_key_net_tag2[14] & 0x1) << 4;
            temp[1] |= (search_key_net_tag2[11] & 0x1) << 3;
            temp[1] |= (search_key_net_tag2[13] & 0x1) << 2;
            temp[1] |= (search_key_net_tag2[12] & 0x1) << 1;
            temp[1] |= (search_key_net_tag2[15] & 0x1) << 0;

            /**********************************************/

            temp[2] = 0;
            temp[2] |= ((search_key_net_tag1[19] & 0x1) ^ (search_key_net_tag2[3] & 0x1)) << 18;
            temp[2] |= (search_key_net_tag1[22] & 0x1) << 17;
            temp[2] |= ((search_key_net_tag1[16] & 0x1) ^ (search_key_net_tag2[7] & 0x1)) << 16;
            temp[2] |= (search_key_net_tag1[20] & 0x1) << 15;
            temp[2] |= ((search_key_net_tag1[17] & 0x1) ^ (search_key_net_tag2[4] & 0x1)) << 14;
            temp[2] |= (search_key_net_tag1[21] & 0x1) << 13;
            temp[2] |= (search_key_net_tag1[23] & 0x1) << 12;
            temp[2] |= ((search_key_net_tag1[18] & 0x1) ^ (search_key_net_tag2[6] & 0x1)) << 11;
            temp[2] |= (search_key_net_tag2[21] & 0x1) << 10;
            temp[2] |= (search_key_net_tag2[18] & 0x1) << 9;
            temp[2] |= (search_key_net_tag2[23] & 0x1) << 8;
            temp[2] |= ((search_key_net_tag2[16] & 0x1) ^ (search_key_net_tag1[6] & 0x1)) << 7;
            temp[2] |= (search_key_net_tag2[20] & 0x1) << 6;
            temp[2] |= ((search_key_net_tag2[17] & 0x1) ^ (search_key_net_tag1[4] & 0x1)) << 5;
            temp[2] |= ((search_key_net_tag2[22] & 0x1) ^ (search_key_net_tag1[8] & 0x1)) << 4;
            temp[2] |= (search_key_net_tag2[19] & 0x1) << 3;
            temp[2] |= (search_key_vsp[2] & 0x1) << 2;
            temp[2] |= (search_key_vsp[1] & 0x1) << 1;
            temp[2] |= (search_key_vsp[3] & 0x1) << 0;


            /**********************************************/
            temp[3] = 0;
            temp[3] |= (search_key_pri[4] & 0x1) << 18;
            temp[3] |= (search_key_pri[3] & 0x1) << 17;
            temp[3] |= (search_key_pri[7] & 0x1) << 16;
            temp[3] |= (search_key_pri[5] & 0x1) << 15;
            temp[3] |= (search_key_pri[6] & 0x1) << 14;
            temp[3] |= (search_key_pri[0] & 0x1) << 13;
            temp[3] |= (search_key_pri[2] & 0x1) << 12;
            temp[3] |= (search_key_pri[1] & 0x1) << 11;
            temp[3] |= (Zero & 0x1) << 10;
            temp[3] |= (Zero & 0x1) << 9;
            temp[3] |= (search_key_vsp[0] & 0x1) << 8;
            temp[3] |= (ps2st_L2_type[2] & 0x1) << 7;
            temp[3] |= (ps2st_L2_type[1] & 0x1) << 6;
            temp[3] |= (ps2st_L2_type[4] & 0x1) << 5;
            temp[3] |= (ps2st_L2_type[0] & 0x1) << 4;
            temp[3] |= (ps2st_L2_type[3] & 0x1) << 3;
            temp[3] |= (Zero & 0x1) << 2;
            temp[3] |= (Zero & 0x1) << 1;
            temp[3] |= (Zero & 0x1) << 0;

            temp[4] = 0;
            temp[4] |= (search_key_ipv6tag[0] & 0x1) << 18;
            temp[4] |= (search_key_ipv6tag[6] & 0x1) << 17;
            temp[4] |= (search_key_ipv6tag[18] & 0x1) << 16;
            temp[4] |= (search_key_ipv6tag[4] & 0x1) << 15;
            temp[4] |= (search_key_ipv6tag[16] & 0x1) << 14;
            temp[4] |= (search_key_ipv6tag[10] & 0x1) << 13;
            temp[4] |= (search_key_ipv6tag[2] & 0x1) << 12;
            temp[4] |= (search_key_ipv6tag[8] & 0x1) << 11;
            temp[4] |= (search_key_ipv6tag[12] & 0x1) << 10;
            temp[4] |= (search_key_ipv6tag[14] & 0x1) << 9;
            temp[4] |= (search_key_ipv6tag[7] & 0x1) << 8;
            temp[4] |= (search_key_ipv6tag[17] & 0x1) << 7;
            temp[4] |= (search_key_ipv6tag[1] & 0x1) << 6;
            temp[4] |= (search_key_ipv6tag[11] & 0x1) << 5;
            temp[4] |= (search_key_ipv6tag[9] & 0x1) << 4;
            temp[4] |= (search_key_ipv6tag[5] & 0x1) << 3;
            temp[4] |= (search_key_ipv6tag[13] & 0x1) << 2;
            temp[4] |= (search_key_ipv6tag[15] & 0x1) << 1;
            temp[4] |= (search_key_ipv6tag[3] & 0x1) << 0;


            temp[5] = 0;
            temp[5] |= (Zero & 0x1) << 18;
            temp[5] |= (Zero & 0x1) << 17;
            temp[5] |= (Zero & 0x1) << 16;
            temp[5] |= (Zero & 0x1) << 15;
            temp[5] |= (search_key_ipv6tag[19] & 0x1) << 14;
            temp[5] |= (Zero & 0x1) << 13;
            temp[5] |= (search_key_ipv6tag[25] & 0x1) << 12;
            temp[5] |= (search_key_ipv6tag[23] & 0x1) << 11;
            temp[5] |= (search_key_ipv6tag[21] & 0x1) << 10;
            temp[5] |= (search_key_ipv6tag[26] & 0x1) << 9;
            temp[5] |= (Zero & 0x1) << 8;
            temp[5] |= (Zero & 0x1) << 7;
            temp[5] |= (Zero & 0x1) << 6;
            temp[5] |= (Zero & 0x1) << 5;
            temp[5] |= (Zero & 0x1) << 4;
            temp[5] |= (search_key_ipv6tag[24] & 0x1) << 3;
            temp[5] |= (search_key_ipv6tag[20] & 0x1) << 2;
            temp[5] |= (Zero & 0x1) << 1;
            temp[5] |= (search_key_ipv6tag[22] & 0x1) << 0;

            if (MEA_PARSER_SUPPORT_IPV6 == MEA_FALSE)
                *hash_addr = temp[0] ^ temp[1] ^ temp[2] ^ temp[3];
            else {
                *hash_addr = temp[0] ^ temp[1] ^ temp[2] ^ temp[3] ^ temp[4] ^ temp[5];

            }



        }
        



#endif
        break;
    case 3: /*20bit address*/
#if 1
        /*20bit address*/


        temp[0] = 0;
        temp[0] |= (search_key_pri[4]           & 0x1) << 19;
        temp[0] |= (search_key_vsp[8]           & 0x1) << 18;
        temp[0] |= (search_key_vsp[7]           & 0x1) << 17;
        temp[0] |= (search_key_vsp[9]           & 0x1) << 16;
        temp[0] |= (search_key_net_tag2[1]      & 0x1) << 15;
        temp[0] |= (search_key_net_tag2[7]      & 0x1) << 14;
        temp[0] |= (search_key_net_tag2[3]      & 0x1) << 13;
        temp[0] |= (search_key_net_tag2[6]      & 0x1) << 12;
        temp[0] |= (search_key_net_tag2[4]      & 0x1) << 11;
        temp[0] |= (search_key_net_tag2[0]      & 0x1) << 10;
        temp[0] |= (search_key_net_tag2[2]      & 0x1) << 9;
        temp[0] |= (search_key_net_tag2[5]      & 0x1) << 8;
        temp[0] |= (search_key_net_tag1[7]      & 0x1) << 7;
        temp[0] |= (search_key_net_tag1[0]      & 0x1) << 6;
        temp[0] |= (search_key_net_tag1[3]      & 0x1) << 5;
        temp[0] |= (search_key_net_tag1[1]      & 0x1) << 4;
        temp[0] |= (search_key_net_tag1[5]      & 0x1) << 3;
        temp[0] |= (search_key_net_tag1[6]      & 0x1) << 2;
        temp[0] |= (search_key_net_tag1[2]      & 0x1) << 1;
        temp[0] |= (search_key_net_tag1[4]      & 0x1) << 0;
        /**********************************************/

        temp[1] = 0;
        temp[1] |= (search_key_pri[3]           & 0x1) << 19;
        temp[1] |= (search_key_vsp[5]           & 0x1) << 18;
        temp[1] |= (search_key_vsp[6]           & 0x1) << 17;
        temp[1] |= (search_key_vsp[4]           & 0x1) << 16;
        temp[1] |= (search_key_net_tag1[9]      & 0x1) << 15;
        temp[1] |= (search_key_net_tag1[11]     & 0x1) << 14;
        temp[1] |= (search_key_net_tag1[15]     & 0x1) << 13;
        temp[1] |= (search_key_net_tag1[8]      & 0x1) << 12;
        temp[1] |= (search_key_net_tag1[14]     & 0x1) << 11;
        temp[1] |= (search_key_net_tag1[12]     & 0x1) << 10;
        temp[1] |= (search_key_net_tag1[10]     & 0x1) << 9;
        temp[1] |= (search_key_net_tag1[13]     & 0x1) << 8;
        temp[1] |= (search_key_net_tag2[8]      & 0x1) << 7;
        temp[1] |= (search_key_net_tag2[10]     & 0x1) << 6;
        temp[1] |= (search_key_net_tag2[9]      & 0x1) << 5;
        temp[1] |= (search_key_net_tag2[14]     & 0x1) << 4;
        temp[1] |= (search_key_net_tag2[11]     & 0x1) << 3;
        temp[1] |= (search_key_net_tag2[13]     & 0x1) << 2;
        temp[1] |= (search_key_net_tag2[12]     & 0x1) << 1;
        temp[1] |= (search_key_net_tag2[15]     & 0x1) << 0;

        /**********************************************/
        temp[2] = 0;
        temp[2] |= (search_key_pri[7]           & 0x1) << 19;
        temp[2] |= (search_key_net_tag1[19]     & 0x1) << 18;
        temp[2] |= (search_key_net_tag1[22]     & 0x1) << 17;
        temp[2] |= (search_key_net_tag1[16]     & 0x1) << 16;
        temp[2] |= (search_key_net_tag1[20]     & 0x1) << 15;
        temp[2] |= (search_key_net_tag1[17]     & 0x1) << 14;
        temp[2] |= (search_key_net_tag1[21]     & 0x1) << 13;
        temp[2] |= (search_key_net_tag1[23]     & 0x1) << 12;
        temp[2] |= (search_key_net_tag1[18]     & 0x1) << 11;
        temp[2] |= (search_key_net_tag2[21]     & 0x1) << 10;
        temp[2] |= (search_key_net_tag2[18]     & 0x1) << 9;
        temp[2] |= (search_key_net_tag2[23]     & 0x1) << 8;
        temp[2] |= (search_key_net_tag2[16]     & 0x1) << 7;
        temp[2] |= (search_key_net_tag2[20]     & 0x1) << 6;
        temp[2] |= (search_key_net_tag2[17]     & 0x1) << 5;
        temp[2] |= (search_key_net_tag2[22]     & 0x1) << 4;
        temp[2] |= (search_key_net_tag2[19]     & 0x1) << 3;
        temp[2] |= (search_key_vsp[2]           & 0x1) << 2;
        temp[2] |= (search_key_vsp[1]           & 0x1) << 1;
        temp[2] |= (search_key_vsp[3]           & 0x1) << 0;


        /**********************************************/
        temp[3] = 0;
        temp[3] |= (Zero                    & 0x1) << 19;
        temp[3] |= (Zero                    & 0x1) << 18;
        temp[3] |= (Zero                    & 0x1) << 17;
        temp[3] |= (Zero                    & 0x1) << 16;
        temp[3] |= (search_key_pri[5]       & 0x1) << 15;
        temp[3] |= (search_key_pri[6]       & 0x1) << 14;
        temp[3] |= (search_key_pri[0]       & 0x1) << 13;
        temp[3] |= (search_key_pri[2]       & 0x1) << 12;
        temp[3] |= (search_key_pri[1]       & 0x1) << 11;
        temp[3] |= (Zero                    & 0x1) << 10;
        temp[3] |= (Zero                    & 0x1) << 9;
        temp[3] |= (search_key_vsp[0]       & 0x1) << 8;
        temp[3] |= (ps2st_L2_type[2]        & 0x1) << 7;
        temp[3] |= (ps2st_L2_type[1]        & 0x1) << 6;
        temp[3] |= (ps2st_L2_type[4]        & 0x1) << 5;
        temp[3] |= (ps2st_L2_type[0]        & 0x1) << 4;
        temp[3] |= (ps2st_L2_type[3]        & 0x1) << 3;
        temp[3] |= (Zero                    & 0x1) << 2;
        temp[3] |= (Zero                    & 0x1) << 1;
        temp[3] |= (Zero                    & 0x1) << 0;


        *hash_addr = temp[0] ^ temp[1] ^ temp[2] ^ temp[3];


#endif 


        break;
    }





    return MEA_OK;
}


MEA_Uint32 mea_drv_Service_External_addres_F_Hash(MEA_Unit_t  unit_i, MEA_Service_HW_SRV_Key HW_SRV_key)
{

    MEA_Uint32 hash_addr = 0;
    
    MEA_Uint32            mode;
    
    MEA_Uint32 srv_mode;
    
   




   


    srv_mode = mea_drv_get_SRV_E_srv_mode(unit_i);
    



    /************************************************************************/
    /*Service size and the address hash                                     */
    /************************************************************************/
    mode = mea_drv_Get_DeviceInfo_ExternalService_mode(unit_i, 0);

    /************************************************************************/
    /* get info if we work dual mode or single                             */
    /************************************************************************/

    if (srv_mode == 1){
        
        if (mode!=0){
            mode -= 1;
        }

    }


   
    mea_drv_Service_External_hash_clac_function1(unit_i, HW_SRV_key, mode, &hash_addr);
 
    if (srv_mode == 1){
        return hash_addr*2;
    }


    return hash_addr;
}





static void mea_Service_Bulid_Hash_info(MEA_Unit_t  unit_i,
                                        MEA_Service_Entry_Key_dbt   *key,
                                        MEA_Service_HW_SRV_Key      *HW_SRV_key)
{



    if (key->ClassifierKEY == 0){

        if ( ((key->L2_protocol_type == MEA_PARSING_L2_KEY_Ctag) || 
            (key->L2_protocol_type == MEA_PARSING_L2_KEY_Untagged)) &&
            (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_IPSEC_NVGRE_ENCRYPT) ) 
        {
            key->ClassifierKEY = MEA_ClassifierKEY_IPSEC_NVGRE_ENCRYPT;
        }

       if ((key->L2_protocol_type == MEA_PARSING_L2_KEY_GW) &&
           (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_IPinIP_CS_DL_IPv4))
        {
                key->ClassifierKEY = MEA_ClassifierKEY_IPinIP_CS_DL_IPv4;
        }

        if ((key->L2_protocol_type == MEA_PARSING_L2_KEY_GW || key->L2_protocol_type == MEA_PARSING_L2_KEY_Ctag) &&
            (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_IP_CS_DL))
        {
            key->ClassifierKEY = MEA_ClassifierKEY_IP_CS_DL;
        }
        if ((key->L2_protocol_type == MEA_PARSING_L2_KEY_GW) &&
            (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_IP_CS_UL))
        {
            key->ClassifierKEY = MEA_ClassifierKEY_IP_CS_UL;
        }
        if ((key->L2_protocol_type == MEA_PARSING_L2_KEY_GW) &&
            (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_UL_VPWS))
        {

            key->ClassifierKEY = MEA_ClassifierKEY_Eth_CS_Tag_UL;
        }
        if ((key->L2_protocol_type == MEA_PARSING_L2_KEY_GW) &&
            (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_UL_VPWS) &&
            (key->inner_netTag_from_value == 0xffffff))
        {

            key->ClassifierKEY = MEA_ClassifierKEY_Eth_CS_UnTag_UL;
        }

        if ((key->L2_protocol_type == MEA_PARSING_L2_KEY_GW) && 
            (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_IPCS_IPV6_DL))
        {
            key->ClassifierKEY = MEA_ClassifierKEY_DL_IPCS_IPV6;
        }
        if ((key->L2_protocol_type == MEA_PARSING_L2_KEY_GW) &&
            (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_DL_VPWS_IPV6))
        {
            key->ClassifierKEY = MEA_ClassifierKEY_DL_VPWS_IPV6;
        }

        

        if ((key->L2_protocol_type == MEA_PARSING_L2_KEY_GW) && 
            (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_IPCS_IPV4_IPV6_UL))
        {
            key->ClassifierKEY = MEA_ClassifierKEY_UL_IPCS_IPV4_IPV6;
        }


        
        if ((key->L2_protocol_type == MEA_PARSING_L2_KEY_GW) &&
            (key->sub_protocol_type == MEA_PARSING_L2_Sub_GW_protocol_IP_CS_UL_SGW))
        {
           key->ClassifierKEY = MEA_ClassifierKEY_IP_CS_UL_SGW;
        }



    }

    switch (key->ClassifierKEY)
    {
    case MEA_ClassifierKEY_DEFUALT:
#if 1
        HW_SRV_key->Hw_priority = key->pri;
        HW_SRV_key->Hw_priType = key->priType;
        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = key->net_tag;









        if (key->L2_protocol_type == MEA_PARSING_L2_KEY_DSA_TAG) {
            HW_SRV_key->Hw_sub_protocol_type = (key->sub_protocol_type);
        }
        else {
            HW_SRV_key->Hw_sub_protocol_type = ((key->sub_protocol_type == 0) ? (0xf) : (key->sub_protocol_type));
        }

        if (key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_L3) {
            HW_SRV_key->Hw_sub_protocol_type = 0; // L3 routing Service 
        }




        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;

        if (!key->inner_netTag_from_valid)
            HW_SRV_key->Hw_inner_netTag_from_value = 0xffffff;
        else
            HW_SRV_key->Hw_inner_netTag_from_value = key->inner_netTag_from_value;


        if ((key->sub_protocol_type == 0) ||
            key->sub_protocol_type == MEA_PARSING_L2_Sub_Ctag_protocol_L3 ) {
            switch (key->L2_protocol_type)
            {
            case MEA_PARSING_L2_KEY_Ctag:
            case MEA_PARSING_L2_KEY_Stag:
                HW_SRV_key->Hw_net_tag = 0xff000 | (key->net_tag & 0xfff);
                if (!key->inner_netTag_from_valid)
                    HW_SRV_key->Hw_inner_netTag_from_value = 0xffffff;
                else
                    HW_SRV_key->Hw_inner_netTag_from_value = (0xfff000 | (key->inner_netTag_from_value & 0xfff));



                break;
            case MEA_PARSING_L2_KEY_Ctag_Ctag:
            case MEA_PARSING_L2_KEY_Stag_Ctag:
            case MEA_PARSING_L2_KEY_Stag_Stag:

                HW_SRV_key->Hw_net_tag = 0xff000 | (key->net_tag & 0xfff);
                if (!key->inner_netTag_from_valid)
                    HW_SRV_key->Hw_inner_netTag_from_value = 0xffffff;
                else
                    HW_SRV_key->Hw_inner_netTag_from_value = (0xfff000 | (key->inner_netTag_from_value & 0xfff));
                break;


            default:
                break;
            }
        
        }
      

#endif
        break;
  
    case MEA_ClassifierKEY_IPSEC_NVGRE_ENCRYPT:


        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type = 9;

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = (key->net_tag & 0xfffff); /*IPSRC_IP*/

        HW_SRV_key->Hw_inner_netTag_from_value = key->inner_netTag_from_value & 0xffffff;  /*SPI */


        HW_SRV_key->Hw_priority = (key->inner_netTag_from_value >> 24) & 0x3f;
        HW_SRV_key->Hw_priType = ((key->inner_netTag_from_value >> 30) & 0x3);


        break;



    case MEA_ClassifierKEY_IPinIP_CS_DL_IPv4:


        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type = (key->sub_protocol_type);

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = (0xff000 | (key->net_tag & 0xfff)); /*Vlanid APN*/
        
       HW_SRV_key->Hw_inner_netTag_from_value = key->inner_netTag_from_value & 0xffffff;  /*Ext_Dest_IP*/


        HW_SRV_key->Hw_priority = (key->inner_netTag_from_value >> 24) & 0x3f;
        HW_SRV_key->Hw_priType = ((key->inner_netTag_from_value >> 30) & 0x3);


        break;

    case MEA_ClassifierKEY_IP_CS_DL:
#if 1




        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type = (key->sub_protocol_type);

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = (0xff000 | (key->net_tag & 0xfff)); /*Vlanid*/


        if (!key->inner_netTag_from_valid)
            HW_SRV_key->Hw_inner_netTag_from_value = 0xffffff;
        else
            HW_SRV_key->Hw_inner_netTag_from_value = key->inner_netTag_from_value & 0xffffff;  /*Ext_Dest_IP*/


        HW_SRV_key->Hw_priority   = (key->inner_netTag_from_value >> 24) & 0x3f;
        HW_SRV_key->Hw_priType    = ((key->inner_netTag_from_value >> 30) & 0x3);




#endif
        break;

    case MEA_ClassifierKEY_IP_CS_UL:
#if 1

 


        HW_SRV_key->Hw_L2_protocol_type  = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type =  (key->sub_protocol_type);

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = 0xf0000 | key->net_tag; /*SGW_TE_ID*/


        if (!key->inner_netTag_from_valid)
            HW_SRV_key->Hw_inner_netTag_from_value = 0xffffff;
        else
            HW_SRV_key->Hw_inner_netTag_from_value = key->inner_netTag_from_value & 0xffffff;  /*Internal_src_IP */


        HW_SRV_key->Hw_priority = (key->inner_netTag_from_value >> 24) & 0x3f;
        HW_SRV_key->Hw_priType = ((key->inner_netTag_from_value >> 30) & 0x3);


#endif
        break;
    case MEA_ClassifierKEY_Eth_CS_Tag_UL:
#if 1




        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type =  (key->sub_protocol_type);

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = 0xC0000 | key->net_tag; /*SGW_TE_ID*/


        if (!key->inner_netTag_from_valid)
            HW_SRV_key->Hw_inner_netTag_from_value = 0xffffff;
        else
            HW_SRV_key->Hw_inner_netTag_from_value = key->inner_netTag_from_value | 0xfff000;  /*Internal_vlan */


        HW_SRV_key->Hw_priority = 0x3f;
        HW_SRV_key->Hw_priType  = 0x3;



#endif
        break;
    case MEA_ClassifierKEY_Eth_CS_UnTag_UL:
#if 1

 

        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type = (key->sub_protocol_type);

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = 0xC0000 | key->net_tag; /*SGW_TE_ID*/


        if (!key->inner_netTag_from_valid)
            HW_SRV_key->Hw_inner_netTag_from_value = 0xffffff;
        else
            HW_SRV_key->Hw_inner_netTag_from_value = 0xffffff;  /*Internal_vlan */


        HW_SRV_key->Hw_priority = 0x3f;
        HW_SRV_key->Hw_priType  = 0x3;

#endif

        break;
    case    MEA_ClassifierKEY_DL_VPWS_IPV6:
        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type = (key->sub_protocol_type);

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = (0xf000 | (key->net_tag & 0xfff)); /*Vlanid */
#ifdef MEA_IPV6_CLASS_SUPPORT
        if (key->IPV6_msb_valid) {
            HW_SRV_key->Hw_inner_netTag_from_value = key->IPV6_msb.s.lsw & 0xffffff;
            HW_SRV_key->Hw_priority = ((key->IPV6_msb.s.lsw >> 24) & 0x3f);
            HW_SRV_key->Hw_priType = ((key->IPV6_msb.s.lsw >> 30) & 0x3);
            HW_SRV_key->Hw_net_tag |= (key->IPV6_msb.s.msw & 0xF) << 16;
            HW_SRV_key->Hw_sub_protocol_type |= ((key->IPV6_msb.s.msw >> 4) & 0x1) << 3;
            HW_SRV_key->HW_Ipv6msb_tag = ((key->IPV6_msb.s.msw >> 5) & 0x7ffffff);

        }
#endif
    break;
    case MEA_ClassifierKEY_DL_IPCS_IPV6:



        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type =  (key->sub_protocol_type);

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag =  (0xf000 | (key->net_tag & 0xfff)) ; /*Vlanid */
#ifdef MEA_IPV6_CLASS_SUPPORT
        if (key->IPV6_msb_valid) {
            HW_SRV_key->Hw_inner_netTag_from_value = key->IPV6_msb.s.lsw & 0xffffff;
            HW_SRV_key->Hw_priority = ((key->IPV6_msb.s.lsw >> 24) & 0x3f);
            HW_SRV_key->Hw_priType  = ((key->IPV6_msb.s.lsw >> 30) & 0x3);
            HW_SRV_key->Hw_net_tag |= (key->IPV6_msb.s.msw & 0xF) << 16;
            HW_SRV_key->Hw_sub_protocol_type |= ((key->IPV6_msb.s.msw >> 4) & 0x1) << 3;
            HW_SRV_key->HW_Ipv6msb_tag = ((key->IPV6_msb.s.msw >> 5) & 0x7ffffff);
            
        }
#endif       


        break;
    case MEA_ClassifierKEY_UL_IPCS_IPV4_IPV6:


        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type = (key->sub_protocol_type);

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = key->net_tag; /* TEId */
#ifdef MEA_IPV6_CLASS_SUPPORT
        if (key->IPV6_msb_valid) {
            HW_SRV_key->Hw_inner_netTag_from_value = key->IPV6_msb.s.lsw & 0xffffff;
            HW_SRV_key->Hw_priority = ((key->IPV6_msb.s.lsw >> 24) & 0x3f);
            HW_SRV_key->Hw_priType = ((key->IPV6_msb.s.lsw >> 30) & 0x3);
            HW_SRV_key->Hw_net_tag |= (key->IPV6_msb.s.msw & 0xF) << 16;
            HW_SRV_key->Hw_sub_protocol_type |= ((key->IPV6_msb.s.msw >> 4) & 0x1) << 3;
            HW_SRV_key->HW_Ipv6msb_tag = ((key->IPV6_msb.s.msw >> 5) & 0x7ffffff);
            
        }
#endif

        break;

    case  MEA_ClassifierKEY_IP_CS_UL_SGW:
#if 1
        HW_SRV_key->Hw_L2_protocol_type = key->L2_protocol_type;
        HW_SRV_key->Hw_sub_protocol_type = (key->sub_protocol_type);

        HW_SRV_key->Hw_src_port = key->src_port;
        HW_SRV_key->Hw_net_tag = 0xc0000 | key->net_tag; /*SGW_TE_ID*/


        if (!key->inner_netTag_from_valid)
            HW_SRV_key->Hw_inner_netTag_from_value = 0xffffff;
        else
            HW_SRV_key->Hw_inner_netTag_from_value = key->inner_netTag_from_value & 0xffffff;  /*Internal_src_IP */


        HW_SRV_key->Hw_priority = 0x3f;
        HW_SRV_key->Hw_priType = 0x3;
#endif

        break;



    default:
        break;
    }



 




}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Service_UpdateHwEntry>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/* Note - All parameters should be in size of 4 bytes */
static void      mea_Service_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
        
    MEA_Unit_t                  unit_i   = (MEA_Unit_t            )arg1;
    MEA_db_HwUnit_t             hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t               hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_Service_Entry_dbt*      entry_pi = (MEA_Service_Entry_dbt*)arg4;
    MEA_Uint32                  val[10];
    MEA_Service_HW_SRV_Key      HW_SRV_key;

    MEA_Uint32                  i;
   
    MEA_Uint32  hwSid=0;

    MEA_Uint32                  count_shift;
    MEA_Uint16 num_of_regs= MEA_IF_SRV_INTERNAL_NUMOF_REGS;



    MEA_OS_memset(&val, 0, sizeof(val));

    MEA_OS_memset(&HW_SRV_key, 0, sizeof(MEA_Service_HW_SRV_Key));

    mea_Service_Bulid_Hash_info(unit_i,
        &entry_pi->key,
        &HW_SRV_key);



    hwSid = (entry_pi->data.valid) ? hwId_i : 0;

    count_shift = 0;
    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_PRI_WIDTH,
        HW_SRV_key.Hw_priority,
        &val[0]);
    count_shift += MEA_IF_SRV_PRI_WIDTH;


    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_PRI_TYPE_WIDTH,
        HW_SRV_key.Hw_priType ,
        &val[0]);
    count_shift += MEA_IF_SRV_PRI_TYPE_WIDTH;
    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_SRC_PORT_WIDTH,
            HW_SRV_key.Hw_src_port,
            &val[0]);
        count_shift += MEA_IF_SRV_SRC_PORT_WIDTH;
    } else {
        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_SRC_PORT_WIDTH,
            HW_SRV_key.Hw_src_port & 0x7f,
            &val[0]);
        count_shift += MEA_IF_SRV_SRC_PORT_WIDTH;
    }
    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_NET_TAG_WIDTH,
        HW_SRV_key.Hw_net_tag & 0xfffff, /*20bit*/
        &val[0]);
    count_shift += MEA_IF_SRV_NET_TAG_WIDTH;


    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_SUB_TYPE_WIDTH,
        (HW_SRV_key.Hw_sub_protocol_type),
        &val[0]);
    count_shift += MEA_IF_SRV_SUB_TYPE_WIDTH;



    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_L2_TYPE_WIDTH,
        HW_SRV_key.Hw_L2_protocol_type,
        &val[0]);
    count_shift += MEA_IF_SRV_L2_TYPE_WIDTH;

 

    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_NET1_TAG_WIDTH,
        (HW_SRV_key.Hw_inner_netTag_from_value & 0xffffff),
        &val[0]);

    count_shift += (MEA_IF_SRV_NET1_TAG_WIDTH);

    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_IPV6_TAG_WIDTH,
        (HW_SRV_key.HW_Ipv6msb_tag & 0x3ffffff),
        &val[0]);

    count_shift += (MEA_IF_SRV_IPV6_TAG_WIDTH);



    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_CID_WIDTH(unit_i, hwUnit_i),
        hwSid,
        &val[0]);
    count_shift += MEA_IF_SRV_CID_WIDTH(unit_i, hwUnit_i);



    
    
    for (i=0;i< num_of_regs/*MEA_NUM_OF_ELEMENTS(val)*/;i++) {
        MEA_API_WriteReg(unit_i,MEA_IF_SRV_MAKE0_REG , val[i] , MEA_MODULE_IF);
    }
    

    if (mea_db_Set_Hw_Regs(unit_i,
                           MEA_Service_Internal_db,
                           hwUnit_i,
                           hwId_i,
                           num_of_regs /*MEA_NUM_OF_ELEMENTS(val)*/,
                           val) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                          __FUNCTION__,
                          hwUnit_i,
                          hwId_i);
    }



}


static void      mea_Service_UpdateHwEntry_EVC(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    MEA_Unit_t                  unit_i = (MEA_Unit_t)arg1;
    MEA_db_HwUnit_t             hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t               hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_Service_Entry_dbt*      entry_pi = (MEA_Service_Entry_dbt*)arg4;
    MEA_Uint32                  val[2];
    MEA_Uint32                  i;
   
    MEA_Uint32                  count_shift;
    



    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }

    count_shift = 0;

    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_L2_TYPE_WIDTH,
        entry_pi->key.L2_protocol_type,
        &val[0]);
    count_shift += MEA_IF_SRV_L2_TYPE_WIDTH;


    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_SRC_PORT_WIDTH,
        entry_pi->key.src_port,
        &val[0]);
    count_shift += MEA_IF_SRV_SRC_PORT_WIDTH;



    if (entry_pi->key.external_internal == MEA_FALSE){ /*External */
        MEA_OS_insert_value(count_shift,
            12, //MEA_IF_SRV_NET_TAG_WIDTH,
            (entry_pi->key.net_tag & 0x00fff),
            &val[0]);
        
    }
    else{
        MEA_OS_insert_value(count_shift,
            12, //MEA_IF_SRV_NET_TAG_WIDTH,
            (entry_pi->key.inner_netTag_from_value & 0x00fff),
            &val[0]);
    }
    count_shift += 12; //MEA_IF_SRV_NET_TAG_WIDTH;



    MEA_OS_insert_value(count_shift,
        MEA_IF_SRV_CID_WIDTH(unit_i, hwUnit_i),
        (entry_pi->data.valid) ? hwId_i : 0,
        &val[0]);
    count_shift += MEA_IF_SRV_CID_WIDTH(unit_i, hwUnit_i);




    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(unit_i, MEA_IF_SRV_MAKE0_REG, val[i], MEA_MODULE_IF);
    }


    /*db  TBD*/



}


static MEA_Status mea_drv_service_hash_key_set_EVC(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit,
                                                   MEA_db_HwId_t hwId,
                                                   MEA_Service_Entry_dbt *new_entry_pi,
                                                   MEA_Bool silence)
{
    MEA_ind_write_t ind_write;
    MEA_Uint32 count, group;
    MEA_Status status;
    MEA_Uint32 collision_evt;
    //int j = 0;
    //int i;

    /* build the ind_write value */
    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_99_HASH;/* not relevant - service hash table */
    ind_write.tableOffset   = 0; /* not relevant - service hash table */
    ind_write.cmdReg        = MEA_IF_CMD_REG;
    ind_write.cmdMask       = MEA_IF_CMD_SRVEVC_CLASS_MAKE_REG_MASK;      //Alex (group bit (9,8))
    ind_write.statusReg     = MEA_IF_STATUS_REG;
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK; //MEA_IF_STATUS_EVC_CLASS_BUSY_MASK;
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG_IGNORE; /* service hash table */
    ind_write.tableAddrMask = 0;

    ind_write.writeEntry = (MEA_FUNCPTR)mea_Service_UpdateHwEntry_EVC;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)hwId);
    ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;




    count = 0;
    if (new_entry_pi->data.valid == MEA_TRUE) {
        for (group = 0; group <= MEA_SERVICES_NUM_OF_GROUP_HASH_EVC; group++) {
            MEA_API_WriteReg(unit_i, ENET_IF_HASH_EVENT_REG, 0, MEA_MODULE_IF); /*ClearEvent*/
            ind_write.cmdMask = MEA_IF_CMD_SRVEVC_CLASS_MAKE_REG_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(group);      //Alex (group bit (9,8)) 

            new_entry_pi->hashGroup = (MEA_Uint8)group;
            status = MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF);
            if (status == MEA_ERROR) {
                if (!silence) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (port=%d)\n",
                        __FUNCTION__, ind_write.tableType, MEA_MODULE_IF, new_entry_pi->key.src_port);
                }
                return MEA_ERROR;
            }

            collision_evt = MEA_API_ReadReg(unit_i, ENET_IF_HASH_EVENT_REG, MEA_MODULE_IF);


            collision_evt = (collision_evt & MEA_IF_COLL_HASH_EVENT_EVC_MASK); /*MASK the relevant group*/
            if ((collision_evt & (1 << (MEA_IF_COLL_HASH_EVENT_EVC_GROUP + group)))) {
                status = MEA_COLLISION;
            }



            if (status == MEA_COLLISION) {
                if (mea_hash_group_flag_show)   MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "service EVC hash group %d COLLISION count %d \n", group, count);
                count++;

            }
            if (status == MEA_OK) {
                if (mea_hash_group_flag_show)
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "service EVC hash group %d Write succeed \n", group);

                return MEA_OK;
                break;
            }
        }
        if (count > (MEA_SERVICES_NUM_OF_GROUP_HASH)) {
            if (!silence) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - We have Collision for All group EVC Hash service id=%d src_port %d  net_tag 0x%08x\n",
                    __FUNCTION__, hwId, new_entry_pi->key.src_port, new_entry_pi->key.net_tag);
            }
            return MEA_ERROR;
        }
    }
    else {
        ind_write.cmdMask = MEA_IF_CMD_SRVEVC_CLASS_MAKE_REG_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(new_entry_pi->hashGroup);   //Alex (group bit (9,8)) 
                                                                                                                                        //THIS READ IS dummy 
                                                                                                                                        //for(i=0;i<10;i++)
                                                                                                                                        //    status = MEA_API_ReadReg(unit_i, MEA_IF_STATUS_REG, MEA_MODULE_IF);

        status = MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF);
        if (status == MEA_ERROR) {
            if (!silence) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF, new_entry_pi->key.src_port);
            }
            return MEA_ERROR;
        }
        
    }

    return MEA_OK;

}
static MEA_Status mea_drv_service_Set_EVC_classifcation(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit, MEA_db_HwId_t hwId, MEA_Service_Entry_dbt *new_entry_pi, MEA_Bool silence, MEA_SETTING_TYPE_te type)
{

     
   

    if ((type == MEA_SETTING_TYPE_CREATE || type == MEA_SETTING_TYPE_DELETE) && new_entry_pi->key.def_sid_valid == MEA_FALSE)
    {

    }
    else{
        return MEA_OK;
    }




    /* Write to the hash table as Indirect Table without the table type and entry # */
    if (new_entry_pi->data.valid == MEA_TRUE){

        if (mea_drv_service_hash_key_set_EVC(unit_i, hwUnit, hwId, new_entry_pi, MEA_FALSE) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  mea_drv_service_hush_set (port=%d) \n",
                __FUNCTION__, new_entry_pi->key.src_port);
            return MEA_ERROR;
        }
        // need to check if succeed 
        // Add function for check if the entry  existing on the Hush



    }
    else{
        //not valid // need to delete
        /************************************************************************/
        /*                                                                      */
        /************************************************************************/

        if (mea_drv_service_hash_key_set_EVC(unit_i, hwUnit, hwId,  new_entry_pi,MEA_FALSE) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  mea_drv_service_hush_set (port=%d) \n",
                __FUNCTION__, new_entry_pi->key.src_port);
            return MEA_ERROR;
        }


    }






    return MEA_OK;
}



static MEA_Status mea_drv_service_intrenal_read_result_hash(MEA_Unit_t unit,MEA_db_HwUnit_t hwUnit, MEA_db_HwId_t hwId)
{
    MEA_ind_read_t         ind_read;
    MEA_Uint32 val[5];

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_INFO_INTRNAL_HASH_RESULT;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(val[0]);


    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(val),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }




    if (mea_db_Set_Hw_Regs(unit,
        MEA_Service_Internal_db_getHW,
        hwUnit,
        hwId,
        MEA_IF_SRV_INTERNAL_NUMOF_REGS,
        val) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
            __FUNCTION__,
            hwUnit,
            hwId);
    }

    return MEA_OK;
}

MEA_Status mea_drv_service_intrenal_Get_result_info(MEA_Unit_t unit, MEA_db_HwUnit_t hwUnit, MEA_db_HwId_t hwId, MEA_Uint32 *val)
{
    MEA_db_Hw_Entry_dbt Hw_Entry;

    if (mea_db_Get_Hw(unit,
        MEA_Service_Internal_db_getHW,
        hwUnit,
        hwId,
       
        &Hw_Entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
            __FUNCTION__,
            hwUnit,
            hwId);
    }

    MEA_OS_memcpy(val, Hw_Entry.regs, sizeof(MEA_Uint32)*MEA_IF_SRV_INTERNAL_NUMOF_REGS);

    return MEA_OK;
}

MEA_Status mea_drv_service_intrenal_result_info_parsing(MEA_Unit_t unit_i, MEA_Uint32 *val, mea_event_hash_key_parsing_Internal_dbt *entry)
{
   
    MEA_Uint32 count_shift = 0;
    MEA_Uint32 temp;
    
    
    entry->subType =  MEA_OS_read_value(MEA_EVENT_LAST_CLS_SubType_START, MEA_EVENT_LAST_CLS_SubType_WIDTH,&val[0]);
    entry->L2Type  =  MEA_OS_read_value(MEA_EVENT_LAST_CLS_L2TYPE_START, MEA_EVENT_LAST_CLS_L2TYPE_WIDTH, &val[0]);
    if ((entry->L2Type == 1) &&
        ((entry->subType == 4) || (entry->subType == 5))) {
        /*Ipv6 mode */
        entry->sig[0] = MEA_OS_read_value(MEA_EVENT_LAST_CLS_SIG0_START, MEA_EVENT_LAST_CLS_SIG0_WIDTH, &val[0]);
        entry->srcport = MEA_OS_read_value(MEA_EVENT_LAST_CLS_SRC_PORT_START, MEA_EVENT_LAST_CLS_SRC_PORT_WIDTH, &val[0]);
        entry->net1    = MEA_OS_read_value(MEA_EVENT_LAST_CLS_NET1_START, MEA_EVENT_LAST_CLS_NET1_WIDTH, &val[0]);
            
        
        temp = MEA_OS_read_value(MEA_EVENT_LAST_CLS_SIG1_3B_START, MEA_EVENT_LAST_CLS_SIG1_3B_WIDTH, &val[0]);
        temp |= MEA_OS_read_value(MEA_EVENT_LAST_CLS_SIG1_6B_START, MEA_EVENT_LAST_CLS_SIG1_6B_WIDTH, &val[0])<<3;
        entry->sig[1] = temp;
        entry->sig[2] = MEA_OS_read_value(MEA_EVENT_LAST_CLS_SIG2_START, MEA_EVENT_LAST_CLS_SIG2_WIDTH, &val[0]);
        entry->sig[3] = MEA_OS_read_value(MEA_EVENT_LAST_CLS_SIG3_START, MEA_EVENT_LAST_CLS_SIG3_WIDTH, &val[0]);


        count_shift = MEA_SRV_PARSING_START_OFFSET_HASH_ADD_Start;
        entry->hash_address = MEA_OS_read_value(count_shift, MEA_SRV_PARSING_START_OFFSET_HASH_ADD_WIDTH, &val[0]);
        count_shift += MEA_SRV_PARSING_START_OFFSET_HASH_ADD_WIDTH;
        entry->hash_type   = MEA_OS_read_value(count_shift, MEA_SRV_PARSING_START_OFFSET_MEM_GROUP, &val[0]);
        count_shift       += MEA_SRV_PARSING_START_OFFSET_MEM_GROUP;
        entry->hash_group = MEA_OS_read_value(count_shift, MEA_SRV_PARSING_START_OFFSET_HASH_GROUP, &val[0]);
        count_shift += MEA_SRV_PARSING_START_OFFSET_HASH_GROUP;
    
    
    }
    else {
         /*regular mode */
        count_shift = 0;
        

        entry->pri     = MEA_OS_read_value(count_shift, MEA_IF_SRV_PRI_WIDTH, &val[0]);
        count_shift += MEA_IF_SRV_PRI_WIDTH;
        entry->priType = MEA_OS_read_value(count_shift, MEA_IF_SRV_PRI_TYPE_WIDTH, &val[0]);
        count_shift += MEA_IF_SRV_PRI_TYPE_WIDTH;
        entry->srcport = MEA_OS_read_value(count_shift, MEA_IF_SRV_SRC_PORT_WIDTH, &val[0]);
        count_shift += MEA_IF_SRV_SRC_PORT_WIDTH;

            
        
          

        
            entry->net1 = MEA_OS_read_value(MEA_IF_SRV_NET_TAG_START, MEA_IF_SRV_NET_TAG_WIDTH, &val[0]);

            entry->net2 = MEA_OS_read_value(MEA_IF_SRV_NET1_TAG_START, MEA_IF_SRV_NET1_TAG_WIDTH, &val[0]);
               
            
            
            




        count_shift = MEA_SRV_PARSING_START_OFFSET_HASH_ADD_Start;
        entry->hash_address = MEA_OS_read_value(count_shift, MEA_SRV_PARSING_START_OFFSET_HASH_ADD_WIDTH, &val[0]);
        count_shift += MEA_SRV_PARSING_START_OFFSET_HASH_ADD_WIDTH;
        entry->hash_type = MEA_OS_read_value(count_shift, MEA_SRV_PARSING_START_OFFSET_MEM_GROUP, &val[0]);
        count_shift += MEA_SRV_PARSING_START_OFFSET_MEM_GROUP;
        entry->hash_group = MEA_OS_read_value(count_shift, MEA_SRV_PARSING_START_OFFSET_HASH_GROUP, &val[0]);
        count_shift += MEA_SRV_PARSING_START_OFFSET_HASH_GROUP;

        
    }




    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_service_hash_key_set>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_service_hash_key_set(MEA_Unit_t               unit_i,
    MEA_db_HwUnit_t          hwUnit,
    MEA_db_HwId_t            hwId,
    MEA_Service_Entry_dbt    *new_entry_pi,
    MEA_Bool                 silence)
{
    MEA_ind_write_t ind_write;
    MEA_Uint32 count, group;
    MEA_Status status;
    MEA_Uint32 collision_evt;
    //int j = 0;
    //int i;


    /* build the ind_write value */
    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_99_HASH;/* not relevant - service hash table */
    ind_write.tableOffset   = 0; /* not relevant - service hash table */
    ind_write.cmdReg        = MEA_IF_CMD_REG;
    ind_write.cmdMask       = MEA_IF_CMD_MAKE_REG_MASK;      //Alex (group bit (9,8))
    ind_write.statusReg     = MEA_IF_STATUS_REG;
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK; //MEA_IF_STATUS_COLLISION_MASK;
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG_IGNORE; /* service hash table */
    ind_write.tableAddrMask = 0;

    ind_write.writeEntry = (MEA_FUNCPTR)mea_Service_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)hwId);
    ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;


    collision_evt = 0;
    count = 0;
    if (new_entry_pi->data.valid == MEA_TRUE) {
        for (group = 0; group <= MEA_SERVICES_NUM_OF_GROUP_HASH; group++) {
            MEA_API_WriteReg(unit_i, ENET_IF_HASH_EVENT_REG, 0, MEA_MODULE_IF); /*ClearEvent*/
            ind_write.cmdMask = MEA_IF_CMD_MAKE_REG_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(group);      //Alex (group bit (9,8)) 
            //THIS READ IS dummy 
            //for(i=0;i<10;i++)
            //    status = MEA_API_ReadReg(unit_i, MEA_IF_STATUS_REG, MEA_MODULE_IF);
            new_entry_pi->hashGroup = group;
            if (mea_hash_group_flag_show)
                MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " hash group %d\n", group);

            status = MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF);
            


            if (status == MEA_ERROR) {
                if (!silence) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (port=%d)\n",
                        __FUNCTION__, ind_write.tableType, MEA_MODULE_IF, new_entry_pi->key.src_port);
                }
                return MEA_ERROR;
            }

            /*check the collision EVNT MASK*/
            /* ENET_IF_HASH_EVENT_REG */
           collision_evt=MEA_API_ReadReg(unit_i, ENET_IF_HASH_EVENT_REG, MEA_MODULE_IF);
          

           collision_evt = (collision_evt & MEA_IF_COLL_HASH_EVENT_ST_MASK); /*MASK the relevant group*/
           if ((collision_evt & (1 << (MEA_IF_COLL_HASH_EVENT_ST_GROUP + group))) ) {
               status = MEA_COLLISION;
           }

           


            if (status == MEA_COLLISION) {
                if (mea_hash_group_flag_show)   MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "COLLISION service hash group %d Event 0x%08x count %d \n", group, collision_evt,count);
                count++;
            }
            if (status == MEA_OK) {
                if (mea_hash_group_flag_show)
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "service hash group %d Write succeed \n", group);

                /*Get Info  */

                mea_drv_service_intrenal_read_result_hash(unit_i, hwUnit, hwId);

                return MEA_OK;
                break;
            }
            if (mea_hash_group_flag_show)
                MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " end hash group %d\n", group);
    }

        if (count > (MEA_SERVICES_NUM_OF_GROUP_HASH)) {
            if (!silence) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - We have Collision for All group Hash service id=%d src_port %d  net_tag 0x%08x\n",
                    __FUNCTION__, hwId, new_entry_pi->key.src_port, new_entry_pi->key.net_tag);
            }
            return MEA_ERROR;
        }
    }
    else {
        ind_write.cmdMask = MEA_IF_CMD_MAKE_REG_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(new_entry_pi->hashGroup);      //Alex (group bit (9,8)) 
        MEA_API_WriteReg(unit_i, ENET_IF_HASH_EVENT_REG, 0, MEA_MODULE_IF); /*ClearEvent*/
        if (mea_hash_group_flag_show)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " Delete hash group %d\n", new_entry_pi->hashGroup);

        status = MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF);
        if (status == MEA_ERROR) {
            if (!silence) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF, new_entry_pi->key.src_port);
            }
            return MEA_ERROR;
        }

    }
    return MEA_OK;

}

static MEA_Status mea_drv_service_Set_regular_classifcation(MEA_Unit_t unit_i,
                                                            MEA_db_HwUnit_t hwUnit,
                                                            MEA_db_HwId_t hwId,
                                                            MEA_Service_Entry_dbt *new_entry_pi, 
                                                            MEA_Bool silence,
                                                            MEA_SETTING_TYPE_te         type)
{

    
#ifdef PRI_GROUP     
    MEA_Service_Entry_dbt  copy_entry;

	MEA_Uint32 pri;
    MEA_Uint32 count;

    MEA_Status status;
    MEA_pri_mask_dbt copy_pri_group_mask;
    MEA_pri_mask_dbt pri_group_mask;
    MEA_Bool flag_failed = MEA_FALSE;
	MEA_Uin32 numOfShift = 0;
#endif
  

   if((type == MEA_SETTING_TYPE_CREATE || type == MEA_SETTING_TYPE_DELETE) && new_entry_pi->key.def_sid_valid == MEA_FALSE)
   {

   }else{
	   return MEA_OK;
   }

#ifdef PRI_GROUP 
	// Alex  
    if(new_entry_pi->key.pri_group_mask_enable == MEA_FALSE){ 
#endif
        /* Write to the hash table as Indirect Table without the table type and entry # */
        if(new_entry_pi->data.valid==MEA_TRUE){

            if(mea_drv_service_hash_key_set(unit_i,hwUnit,hwId,new_entry_pi,MEA_FALSE)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  mea_drv_service_hush_set (port=%d) \n",
                    __FUNCTION__,new_entry_pi->key.src_port);
                return MEA_ERROR;
            }
            // need to check if succeed 
            // Add function for check if the entry  existing on the Hush



        } else{
            //not valid // need to delete
            /************************************************************************/
            /*                                                                      */
            /************************************************************************/

            if(mea_drv_service_hash_key_set(unit_i,hwUnit,hwId,new_entry_pi,MEA_FALSE)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  mea_drv_service_hush_set (port=%d) \n",
                    __FUNCTION__,new_entry_pi->key.src_port);
                return MEA_ERROR;
            }


        }

#ifdef PRI_GROUP 
    }else { // pri group set
        /*count the number of pri mask existing port*/
        if(new_entry_pi->data.valid==MEA_TRUE){
            MEA_OS_memcpy(&copy_pri_group_mask,&new_entry_pi->key.pri_group_mask,sizeof(copy_pri_group_mask));
            MEA_OS_memcpy(&copy_entry,new_entry_pi,sizeof(copy_entry));
            MEA_OS_memset(&pri_group_mask,0,sizeof(pri_group_mask));
            count=0;
            for (pri = 0;pri < 64; pri++)
            {
				numOfShift = (((MEA_Uint32)(pri % 32)));
                if (((ENET_Uint32*)(&(copy_pri_group_mask.pri_0_31)))[pri/32]   & ((ENET_Uint32)(1<<((numOfShift))))) 
				{

                    copy_entry.key.pri = pri;
                    status = mea_drv_service_hash_key_set(unit_i,hwUnit,hwId,&copy_entry,MEA_TRUE);
                    if(status != MEA_OK){
                        flag_failed=MEA_TRUE;
                    }
                    if(status == MEA_OK){
                        ((ENET_Uint32*)(&(pri_group_mask.pri_0_31)))[pri/32] |= (1 << (pri%32));
                        count++;
                    }
                }
            }
            if(flag_failed==MEA_TRUE){
                //need to delete because we not  succeed to insert all the select pri.
                for (pri = 0;pri < 64; pri++)
                {
                    if (((ENET_Uint32*)(&(pri_group_mask.pri_0_31)))[pri/32]   & (1 << (pri%32))) {
                        copy_entry.key.pri = pri;
                        copy_entry.data.valid=MEA_FALSE;
                        if(mea_drv_service_hash_key_set(unit_i,hwUnit,hwId,&copy_entry,MEA_FALSE)!=MEA_OK){
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s -  mea_drv_service_hush_set Delete group of PRI (pri=%d) \n",
                                __FUNCTION__,pri);
                        }

                    }
                }
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  The pri group failed to set (only the pri  pri_msb =0x%08x pri_lsb =0x%08x can be setting) look on the pri_group_mask\n",
                    __FUNCTION__,pri_group_mask.pri_32_63,pri_group_mask.pri_0_31);
                MEA_OS_memcpy(&new_entry_pi->key.pri_group_mask,&pri_group_mask,sizeof(new_entry_pi->key.pri_group_mask));
                return MEA_ERROR;
            }

        } else{
            /************************************************************************/
            /*           Delete group of PRI                                        */
            /************************************************************************/
            MEA_OS_memcpy(&copy_pri_group_mask,&new_entry_pi->key.pri_group_mask,sizeof(copy_pri_group_mask));
            MEA_OS_memcpy(&copy_entry,new_entry_pi,sizeof(copy_entry));
            count=0;
            for (pri = 0;pri < 64; pri++)
            {
				numOfShift = (((MEA_Uint32)(pri % 32)));
				if (((ENET_Uint32*)(&(copy_pri_group_mask.pri_0_31)))[pri/32]   & ((ENET_Uint32)(1<<(numOfShift)))) 
				{
                    copy_entry.key.pri = pri;
                    status= mea_drv_service_hash_key_set(unit_i,hwUnit,hwId,&copy_entry,MEA_TRUE);
                    if(status!=MEA_OK){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s -  mea_drv_service_hush_set Delete group of PRI (pri=%d) \n",
                            __FUNCTION__,pri);
                        return MEA_ERROR; 
                    }
                    count++;
                }
            }
        }

    } // pri group set 
#endif


    return MEA_OK; 
}


static MEA_Status mea_service_srcport_lean(MEA_Unit_t                  unit_i,
    MEA_Service_t               serviceId_i,
    MEA_Service_Entry_dbt*      new_entry_pi,
    MEA_Service_Entry_dbt*      old_entry_pi,
    MEA_SETTING_TYPE_te         type) 
{

    /* check if we have any changes */
    if ((old_entry_pi != NULL) &&
        (MEA_OS_memcmp((char*)new_entry_pi,
        (char*)old_entry_pi,
        sizeof(*new_entry_pi)) == 0)) {
        return MEA_OK;
    }

    if ((new_entry_pi->data.DSE_learning_srcPort_force != MEA_FALSE) ||
        (new_entry_pi->data.DSE_learning_enable))
    {
        ENET_xPermissionId_t xPermission_id;
        MEA_db_HwUnit_t      learning_xPermission_id_hwUnit;
        mea_memory_mode_e save_globalMemoryMode;
        if (new_entry_pi->data.DSE_learning_srcPort_force) {
            xPermission_id = (ENET_xPermissionId_t)new_entry_pi->data.DSE_learning_srcPort;
        }
        else {
            xPermission_id = (ENET_xPermissionId_t)new_entry_pi->key.src_port;
        }
        if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000)  {
            learning_xPermission_id_hwUnit = MEA_HW_UNIT_ID_GENERAL;
        }
        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode, learning_xPermission_id_hwUnit);
        if (new_entry_pi->data.valid){
            if (enet_IsValid_xPermission(unit_i,
                xPermission_id,
                ENET_TRUE
                ) != ENET_TRUE)
            {
                ENET_xPermission_dbt xPermission_entry;
                MEA_OS_memset(&xPermission_entry, 0, sizeof(xPermission_entry));
                ((MEA_Uint32 *)(&xPermission_entry.out_ports_0_31))
                    [xPermission_id / 32] |=
                    (0x00000001 << (xPermission_id % 32));
                if (enet_Create_xPermission(unit_i,
                    &xPermission_entry,
                    0,
                    &xPermission_id) != ENET_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - enet_Create_xPermission %d\n",
                        __FUNCTION__, xPermission_id);
                    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                    return MEA_ERROR;
                }
            }
            else{
                enet_xPermission_Add_owner(unit_i, xPermission_id);
            }
        }
        else{/* end valid */
            if (enet_IsValid_xPermission(unit_i,
                xPermission_id,
                ENET_TRUE
                ) == ENET_TRUE){

                enet_xPermission_delete_owner(unit_i, xPermission_id);
            }
        }



        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }



    /* return to caller */
    return MEA_OK;

}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Service_UpdateHw>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Service_UpdateHw(MEA_Unit_t                  unit_i,
                                       MEA_Service_t               serviceId_i,
                                       MEA_Service_Entry_dbt*      new_entry_pi,
                                       MEA_Service_Entry_dbt*      old_entry_pi,
									   MEA_SETTING_TYPE_te         type)
{

    //MEA_ind_write_t ind_write;
    MEA_db_HwUnit_t           hwUnit;
    MEA_db_HwId_t             hwId;
 


    if (new_entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - new_entry_pi == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    /* check if we have any changes */
    if ((old_entry_pi                                                != NULL    ) &&
        (MEA_OS_memcmp((char*)new_entry_pi,
                       (char*)old_entry_pi,
                       sizeof(*new_entry_pi)) == 0)) {
       return MEA_OK;
    }



    

        /* Get the hwUnit and hwId */
        MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
            mea_drv_Get_Service_Internal_db,
            MEA_Service_Table[serviceId_i].Sid_Internal,
            hwUnit,
            hwId,
            return MEA_ERROR);






        if (new_entry_pi->service_Type == MEA_SERVICE_CLASSIFCATION_REGULAR && type == MEA_SETTING_TYPE_DELETE) {

            if (mea_drv_service_Set_regular_classifcation(unit_i, hwUnit, hwId, new_entry_pi,MEA_FALSE, type) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_service_Set_regular_classifcation failed \n", __FUNCTION__);
                return MEA_ERROR;
            }


        }
        if (new_entry_pi->service_Type == MEA_SERVICE_CLASSIFCATION_EVC && type == MEA_SETTING_TYPE_DELETE) {

            if (mea_drv_service_Set_EVC_classifcation(unit_i, hwUnit, hwId, new_entry_pi, MEA_FALSE, type) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_service_Set_EVC_classifcation failed \n", __FUNCTION__);
                return MEA_ERROR;
            }


        }
#ifdef MEA_TDM_SW_SUPPORT    
    if (new_entry_pi->data.tdm_packet_type == 1)
        MEA_OS_sleep(0, 10 * 1000 * 1000);
#endif

 	if(type != MEA_SETTING_TYPE_DELETE){
    /* Update ServiceEntry */
    
    if (mea_ServiceEntry_UpdateHw(unit_i,
        serviceId_i,
        hwUnit,
        hwId,
        new_entry_pi,
        old_entry_pi) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_ServiceEntry_UpdateHw failed \n", __FUNCTION__);
        return MEA_ERROR;
    }
	}
    

    
//Alex change
	if(new_entry_pi->service_Type == MEA_SERVICE_CLASSIFCATION_REGULAR && type == MEA_SETTING_TYPE_CREATE) {

		if (mea_drv_service_Set_regular_classifcation(unit_i,hwUnit,hwId,new_entry_pi,MEA_FALSE,type) != MEA_OK){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_service_Set_regular_classifcation failed \n",__FUNCTION__);
			return MEA_ERROR;
		}

	}

    if (new_entry_pi->service_Type == MEA_SERVICE_CLASSIFCATION_EVC && type == MEA_SETTING_TYPE_CREATE) {

        if (mea_drv_service_Set_EVC_classifcation(unit_i, hwUnit, hwId, new_entry_pi, MEA_FALSE, type) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_service_Set_EVC_classifcation failed \n", __FUNCTION__);
            return MEA_ERROR;
        }

    }



    /* return to caller */
    return MEA_OK;

}





/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/

MEA_Status mea_drv_Flush_HW_Service_Table(MEA_Unit_t unit_i)
{


    MEA_Globals_Entry_dbt entry;
    MEA_Uint32 status;

    status = MEA_API_ReadReg(unit_i, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    
    if( (status & MEA_IF_STATUS_BUSY_MASK) == 1 )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - status command busy is set\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_API_Get_Globals_Entry (unit_i,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_Globals failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
 
    entry.if_global1.val.service_table_flush = 1;

    if (MEA_API_Set_Globals_Entry (unit_i,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_Globals (1) failed  \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    status = MEA_API_ReadReg(unit_i, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    while ( (status & MEA_IF_STATUS_BUSY_MASK) )
    {
         MEA_OS_sleep(0, 1000 ); /* sleep for 1 microsecond */

         status = MEA_API_ReadReg(unit_i, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    }


    entry.if_global1.val.service_table_flush = 0;

    if (MEA_API_Set_Globals_Entry (unit_i,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_Globals (0) failed  \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }



    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Init_Cpu_Service>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Init_Cpu_Service(MEA_Unit_t unit_i)
{

    MEA_Service_Entry_dbt entry;
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId;

    MEA_OS_memset(&entry,0,sizeof(entry));
    entry.data.ADM_ENA = MEA_TRUE;
    entry.data.valid = MEA_TRUE;
    entry.xPermissionId = MEA_CPU_PORT;

    /* Create CPU service */
    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        hwId = (MEA_db_HwId_t)MEA_CPU_SERVICE_ID;
        if (mea_db_Create(unit_i,
                          MEA_Service_Internal_db,
                          (MEA_db_SwId_t)MEA_CPU_SERVICE_ID,
                          hwUnit,
                          &hwId) != MEA_OK) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - mea_db_Create failed (hwUnit=%d,hwId=%d)\n",
                            __FUNCTION__,
                            hwUnit,
                            hwId);
          return MEA_ERROR;
        }
        if (mea_db_Create(unit_i,
            MEA_Service_Internal_db_getHW,
            (MEA_db_SwId_t)MEA_CPU_SERVICE_ID,
            hwUnit,
            &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Create failed (hwUnit=%d,hwId=%d)\n",
                __FUNCTION__,
                hwUnit,
                hwId);
            return MEA_ERROR;
        }


        

        /* Update ServiceEntry */
        if (mea_ServiceEntry_UpdateHw(unit_i,
                                      MEA_CPU_SERVICE_ID,
                                      hwUnit,
                                        hwId,
                                      &entry,
                                      NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_ServiceEntry_UpdateHw failed \n",__FUNCTION__);
           return MEA_ERROR;
       }

    }

    /* Note: The action of the CPU Service create by mea_Action_CreateCpuAction that call 
             from the init process */
    

    
    /* Return to caller */
    return MEA_OK; 

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Conclude_Cpu_Service>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_Conclude_Cpu_Service(MEA_Unit_t unit_i)
{

    MEA_Service_Entry_dbt entry;
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId;



    MEA_OS_memset(&entry,0,sizeof(entry));
    entry.data.ADM_ENA = MEA_TRUE;
    entry.data.valid = MEA_FALSE;
    entry.xPermissionId = ENET_PLAT_GENERATE_NEW_ID;;

    /* Delete CPU service */
    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        hwId = (MEA_db_HwId_t)MEA_CPU_SERVICE_ID;

        if (mea_ServiceEntry_UpdateHw(unit_i,
                                      MEA_CPU_SERVICE_ID,
                                      hwUnit,
                                      hwId,
                                      &entry,
                                      NULL) /* old_entry */
                                  != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_ServiceEntry_UpdateHw failed \n",
                                __FUNCTION__);
            return MEA_ERROR;
        }

        if (mea_db_Delete(unit_i,
                          MEA_Service_Internal_db,
                          (MEA_db_SwId_t)MEA_CPU_SERVICE_ID,
                          hwUnit) != MEA_OK) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - mea_db_Delete failed (hwUnit=%d)\n",
                            __FUNCTION__,
                            hwUnit);
          return MEA_ERROR;
        }
        if (mea_db_Delete(unit_i,
            MEA_Service_Internal_db_getHW,
            (MEA_db_SwId_t)MEA_CPU_SERVICE_ID,
            hwUnit) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Delete failed (hwUnit=%d)\n",
                __FUNCTION__,
                hwUnit);
            return MEA_ERROR;
        }


        


    }
    
    /* Return to caller */
    return MEA_OK; 

}

MEA_Status mea_drv_Conclude_Service_Table(MEA_Unit_t unit_i)
{

    MEA_Service_t serviceId , next_serviceId;
    MEA_ULong_t cookie;

    /* Get the first service */
    if (MEA_API_GetFirst_Service(unit_i,&serviceId,&cookie) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_GetFirst_Service failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    while (cookie != 0) { 
   
        /* get the next service Id */
        next_serviceId = serviceId;
        if (MEA_API_GetNext_Service(unit_i,&next_serviceId,&cookie) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_GetNext_Service failed\n",
                              __FUNCTION__);
            return MEA_ERROR; 
       }

		if (MEA_DRV_Delete_Service(unit_i, serviceId,MEA_TRUE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Delete_Service failed\n",
                              __FUNCTION__);
            return MEA_ERROR; 
        }

		
        serviceId = next_serviceId;
    }
    mea_drv_Conclude_ServiceEntry_Table(unit_i);

    mea_drv_Conclude_serviceRange_Table(unit_i);

    if (MEA_Service_Internal_BIT){
        MEA_OS_free(MEA_Service_Internal_BIT);
    }

    if (MEA_Service_External_BIT){
        MEA_OS_free(MEA_Service_External_BIT);
    }
    if (MEA_Service_External_Hash_BIT){
        MEA_OS_free(MEA_Service_External_Hash_BIT);
    }
//     if (MEA_Service_SW_BIT){
//         MEA_OS_free(MEA_Service_SW_BIT);
//     }

    
    if (MEA_srv_filter_prof_Table) {
        MEA_OS_free(MEA_srv_filter_prof_Table);
        MEA_srv_filter_prof_Table = NULL;
    }

    if (MEA_Service_Table) {
        MEA_OS_BigFree(MEA_Service_Table);
        MEA_Service_Table=NULL;
    }

    if (mea_db_Conclude(unit_i,&MEA_Service_Internal_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_dn_Conclude failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_db_Conclude(unit_i, &MEA_Service_Internal_db_getHW) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_dn_Conclude failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    


    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/




 MEA_Status MEA_DRV_Create_Service(MEA_Unit_t                              unit_i,
    MEA_Service_Entry_Key_dbt              *key,
    MEA_Service_Entry_Data_dbt             *data,
    MEA_OutPorts_Entry_dbt                 *OutPorts_Entry,
    MEA_Policer_Entry_dbt                  *Policer_Entry,
    MEA_EgressHeaderProc_Array_Entry_dbt   *EHP_Entry,
    MEA_Service_t                          *o_serviceId)


{

    MEA_Service_t             serviceId;
//    MEA_Service_t             Internal_serviceId = 0;
//    MEA_Service_t             External_serviceId = 0;


    MEA_Service_Entry_dbt     entry;
    MEA_Service_Entry_Key_dbt tempkey;
    MEA_IngressPort_Entry_dbt IngressPort_Entry;
    MEA_Bool                  exist;
    MEA_Bool                  existActionId;
    MEA_Bool                  exist_lxcp;
    ENET_xPermission_dbt      xPermission;
    ENET_xPermissionId_t      xPermissionId;

    ENET_xPermission_dbt      LearnxPermission;
    ENET_xPermissionId_t      LearnxPermissionId;


    MEA_FilterMask_t                        filter_mask_id;
    MEA_EgressHeaderProc_Array_Entry_dbt*   Action_Editing;
    MEA_Action_Entry_Data_dbt               Action_Data;
    MEA_Action_t                            Action_Id;
    mea_memory_mode_e                       save_globalMemoryMode;
    MEA_db_HwUnit_t                         hwUnit;
    MEA_db_HwId_t                           hwId;
    MEA_Service_classification_t            service_Type;
    MEA_Uint16 rangeId = 0;
    MEA_Bool isCpu;
    MEA_Uint32 hashAddres; /*for external*/
    MEA_Service_HW_SRV_Key      HW_SRV_key;
    MEA_Globals_Entry_dbt Global_entry;
    MEA_Bool collision_ddr = MEA_FALSE;
    MEA_Bool  exsit;




#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool      exist_flag;
    MEA_Service_t exist_serviceId;
#endif 

    MEA_API_LOG



        /* Set default output parameters */
        if (o_serviceId == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "-o_serviceId  NULL\n");
            return MEA_ERROR;
            //       *o_serviceId = 0;
        }

    serviceId = *o_serviceId;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key parameter */
    if (key == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
	
    /* Check for valid key->src_port parameter */
    if ((key->virtual_port == MEA_FALSE)
        && MEA_API_Get_IsPortValid((MEA_Port_t)(key->src_port), MEA_PORTS_TYPE_INGRESS_TYPE,MEA_FALSE) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - port %d is invalid\n",
            __FUNCTION__, key->src_port);
        return MEA_ERROR;
    }


    /* Check for valid data parameter */
    if (data == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data == NULL\n",
            __FUNCTION__);

        return MEA_ERROR;
    }




 



#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,
        mea_Service_GetHwUnit(unit_i, key));


    if (MEA_API_Get_Globals_Entry(unit_i, &Global_entry) != MEA_OK) {

        return MEA_ERROR;
    }







    /* Get the ingress port protocol profile */
    if ((key->virtual_port == MEA_FALSE) &&
        MEA_API_Get_IngressPort_Entry(unit_i,
        (MEA_Port_t)(key->src_port),
        &IngressPort_Entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Get_IngressPort_Entry for port %d failed\n",
            __FUNCTION__, key->src_port);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ((MEA_CLS_LARGE_SUPPORT == MEA_FALSE) && (key->pri_to_valid == MEA_FALSE) &&
        (key->pri > 7)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Currently  the priority can't be bigger then  7 for exact match service\n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (((key->inner_netTag_to_valid == MEA_TRUE) ||
        (key->inner_netTag_from_valid == MEA_TRUE)) && (MEA_CLS_LARGE_SUPPORT == MEA_FALSE)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - currently inner netTag not support \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }



    if ((key->virtual_port == MEA_FALSE) &&
        (MEA_CLS_LARGE_SUPPORT == MEA_TRUE) &&
        (key->L2_protocol_type == MEA_PARSING_L2_KEY_Transparent) &&
        (IngressPort_Entry.parser_info.port_proto_prof != MEA_INGRESS_PORT_PROTO_TRANS)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- L2_protocol_type %d only if the port is transparent mode \n",
            __FUNCTION__, key->L2_protocol_type);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* Set service_Type (range/regular) */
    if ((key->net_tag_to_valid == MEA_TRUE) ||
        (key->pri_to_valid == MEA_TRUE) ||
        ((key->inner_netTag_from_valid == MEA_TRUE) && (key->inner_netTag_to_valid == MEA_TRUE))
        ) {
        service_Type = MEA_SERVICE_CLASSIFCATION_RANGE;
    }
    else {
        if (key->evc_enable == MEA_TRUE){
            service_Type = MEA_SERVICE_CLASSIFCATION_EVC;
            if (MEA_SERVICES_EVC_CLASSIFER_SUPPORT == MEA_FALSE){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " EVC classifier not Support \n");
                return MEA_ERROR;
            }
        }
        else{
            if (key->L2_protocol_type == MEA_PARSING_L2_KEY_DEF_SID)
                service_Type = MEA_SERVICE_CLASSIFCATION_DEFUALT_SID;
            else
                service_Type = MEA_SERVICE_CLASSIFCATION_REGULAR;
        }
    }
#ifdef PRI_GROUP 
    if ((service_Type == MEA_SERVICE_CLASSIFCATION_RANGE) && (key->pri_group_mask_enable == MEA_TRUE)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - The  pri_group_mask_enable can't be set while you configure Service range \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if ((key->pri_group_mask_enable == MEA_TRUE) &&
        (key->pri_group_mask.pri_0_31 == 0) &&
        (key->pri_group_mask.pri_32_63 == 0)){

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - The  pri_group_mask_enable set while the  pri_group_mask is zero  failed\n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if ((key->pri_group_mask_enable == MEA_FALSE) &&
        (key->pri_group_mask.pri_0_31 != 0) &&
        (key->pri_group_mask.pri_32_63 != 0)){

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - The  pri_group_mask_enable disable while the  pri_group_mask must be zero  failed\n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (key->evc_enable == MEA_TRUE && key->pri_group_mask_enable == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - The  pri_group_mask_enable enable while and evc_enable enable \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
#endif

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if ((MEA_SERVICE_RANGE_SUPPORT == MEA_FALSE) &&
        ((key->net_tag_to_valid == MEA_TRUE) || (key->pri_to_valid == MEA_TRUE))){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Not support vlan range and pri range  \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;

    }



    /*check if the net_tag range is correct */
    if ((key->net_tag != key->net_tag_to_value) &&
        (key->net_tag_to_valid == MEA_FALSE) &&
        (key->net_tag_to_value != 0)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key net tag is range and the valid of range no set \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /*check if the pri range is correct */
    if ((key->pri != key->pri_to_value) &&
        (key->pri_to_valid == MEA_FALSE) &&
        (key->pri_to_value != 0)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key: Range for pri but pri range valid not SET \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* Check range is valid , and values are not same */
    if ((key->net_tag == key->net_tag_to_value) &&
        (key->net_tag_to_valid == MEA_TRUE) &&
        (key->pri == key->pri_to_value) &&
        (key->pri_to_valid == MEA_TRUE)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key: Range valid for netTag and priority , but the values are same \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE) {
        if ((key->net_tag_to_valid == MEA_TRUE) &&
            ((key->net_tag & 0x00ffffff) > (key->net_tag_to_value & 0x00ffffff))) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - key: Range netTag from %d bigger than %d \n",
                __FUNCTION__,
                (key->net_tag & 0x00ffffff),
                (key->net_tag_to_value & 0x00ffffff));
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if ((key->pri_to_valid == MEA_TRUE) &&
            (key->pri > key->pri_to_value)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - key: Range Pri from %d can't bigger than %d \n",
                __FUNCTION__,
                (key->pri),
                (key->pri_to_value));
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;

        }
        if ((key->inner_netTag_from_valid == MEA_TRUE) && (key->inner_netTag_to_valid == MEA_TRUE) &&
            ((key->inner_netTag_from_value & 0x00ffffff) > (key->inner_netTag_to_value & 0x00ffffff))) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - key: Range inner_netTag from %d bigger than %d \n",
                __FUNCTION__,
                (key->inner_netTag_from_value & 0x00ffffff),
                (key->inner_netTag_to_value & 0x00ffffff));
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }


    if ((key->virtual_port == MEA_FALSE) &&
        (key->pri_to_valid != MEA_TRUE)) {
        if (IngressPort_Entry.parser_info.pri_wildcard_valid && IngressPort_Entry.parser_info.pri_wildcard != 255) {
            if (IngressPort_Entry.parser_info.pri_wildcard != 7) { //Alex T.B.D 
                if ((IngressPort_Entry.parser_info.pri_wildcard & key->pri) !=
                    IngressPort_Entry.parser_info.pri_wildcard) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "Wild-card and pri as no matching wild-card = %d pri= %d \n  Failed on create service \n",
                        IngressPort_Entry.parser_info.pri_wildcard, key->pri);
                    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                    return MEA_ERROR;
                }
                /*???*/ /* T.B.D - Check also the key->pri_to_value */
            }
        }/*IngressPort_Entry.parser_info.pri_wildcard_valid*/
    }
    if ((key->virtual_port == MEA_FALSE) && IngressPort_Entry.parser_info.net_wildcard_valid && IngressPort_Entry.parser_info.evc_external_internal == MEA_FALSE) {
        if (((MEA_Uint32)IngressPort_Entry.parser_info.net_wildcard & (MEA_Uint32)key->net_tag) != (MEA_Uint32)IngressPort_Entry.parser_info.net_wildcard) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "Wild-card and netTag as no matching wild-card = 0x%x netTag= 0x%x \n  Failed on create service \n",
                IngressPort_Entry.parser_info.net_wildcard, key->net_tag);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        /*??? *//* T.B.D - Check also the key->net_tag_to_value */

    }/*IngressPort_Entry.parser_info.net_wildcard_valid*/

    if (MEA_ACL_SUPPORT == MEA_FALSE && data->filter_enable == MEA_TRUE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  ACL NOT SUPPORT you to allowed to set the filter_enable\n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (MEA_ACL5_SUPPORT) {
        if (data->Acl5_mask_prof != 0) {
            ///check 
            if(MEA_API_ACL5_Mask_Grouping_Profiles_isExist(MEA_UNIT_0, data->Acl5_mask_prof, MEA_TRUE, &exsit) != MEA_OK){
                return MEA_ERROR;
            }
            if(exsit == MEA_FALSE){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  MEA_API_ACL5_Mask_Grouping_Profiles_isExist is not Exist\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }
    }




#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */




     if ( ((data->SRV_mode == MEA_SRV_MODE_REGULAR) || 
          (data->SRV_mode == MEA_SRV_MODE_EXTERNAL)) && 
           Global_entry.check_srv_key == MEA_TRUE)
     { /*only on internal check */
        /* Check if such service is already exists with same key */
         //need to build the key before the compare           
         
         MEA_OS_memset(&HW_SRV_key, 0, sizeof(HW_SRV_key));
         MEA_OS_memset(&tempkey, 0, sizeof(tempkey));
         MEA_OS_memcpy(&tempkey, key, sizeof(tempkey));
         

         mea_Service_Bulid_Hash_info(unit_i, &tempkey, &HW_SRV_key);


         if (MEA_API_IsExist_Service_ByKey(unit_i,
             &tempkey, //key,
            &exist_flag,
            &exist_serviceId) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsExist_Service_ByKey failed \n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        if (exist_flag == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Service Id %d is already exists with same key\n",
                __FUNCTION__, exist_serviceId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS


    

    if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
        /* check if we have service range intersection */
        if (mea_drv_serviceRange_Check_Intersect(unit_i, key) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  Check range intersect failed \n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }







    /* check valid data->pmId  */
    if (data->pmId >= MEA_MAX_NUM_OF_PM_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data->pmId (%d) >= max allowed (%d) \n",
            __FUNCTION__, data->pmId, MEA_MAX_NUM_OF_PM_ID);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* check valid data->pm_type  */
    switch (data->pm_type) {
    case MEA_PM_TYPE_ALL:
        break;

    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - unknown data->pm_type (%d) \n",
            __FUNCTION__,
            data->pm_type);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    if (data->tmId >= MEA_MAX_NUM_OF_TM_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data->tmId (%d) >= max allowed (%d) \n",
            __FUNCTION__, data->tmId, MEA_MAX_NUM_OF_PM_ID);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (data->editId >= MEA_MAX_NUM_OF_EDIT_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data->editId (%d) >= max allowed (%d) \n",
            __FUNCTION__, data->editId, MEA_MAX_NUM_OF_PM_ID);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (data->stp == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data->stp  stp no support used the MSTP_Info.state \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;

    }

    /* Check CFM_OAM_ME_Level_Threashold_valid support by the device */
    if ((globalMemoryMode != MEA_MODE_REGULAR_ENET3000) &&
        (data->CFM_OAM_ME_Level_Threshold_valid)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - CFM_OAM_ME_Level_valid == 1 is not support by the device \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* Forwarder enable is not allowed in upstream only */
    if ((globalMemoryMode == MEA_MODE_UPSTREEM_ONLY) &&
        (data->DSE_forwarding_enable)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - DSE_forwarding_enable==TRUE not allowed in upstream \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* Learning enable is not allowed in downstream only */
    if ((globalMemoryMode == MEA_MODE_DOWNSTREEM_ONLY) &&
        (data->DSE_learning_enable)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - DSE_learning_enable==TRUE not allowed in downstream \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* check data->DSE_forwarding_key_type */
    switch ((MEA_SE_Forwarding_key_type_te)(data->DSE_forwarding_key_type)) {
    case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN:
    case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
#ifdef PPP_MAC 
    case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
#else
	case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
	
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN:
    case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
    case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Invalid value DSE_forwarding_key_type (%d) \n",
            __FUNCTION__, (int)(data->DSE_forwarding_key_type));
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* check data->DSE_learning_key_type */
    switch ((MEA_SE_Learning_key_type_te)(data->DSE_learning_key_type)) {
    case MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN:
    case MEA_SE_LEARNING_KEY_TYPE_MPLS_VPN:
    case MEA_SE_LEARNING_KEY_TYPE_DA_PLUS_PPPOE_SESSION_ID:
    case MEA_SE_LEARNING_KEY_TYPE_VLAN_VPN:
    case MEA_SE_LEARNING_KEY_TYPE_OAM_VPN:
    case MEA_SE_LEARNING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
    case MEA_SE_LEARNING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
    case MEA_SE_LEARNING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        break;
    case MEA_SE_LEARNING_KEY_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Invalid value DSE_learning_key_type (%d) \n",
            __FUNCTION__, (int)(data->DSE_learning_key_type));
        return MEA_ERROR;
    }


    if (Policer_Entry == NULL && data->tmId == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Policer_Entry == NULL (Policer is mandatory for service)\n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    if (data->limiter_enable){
        if (MEA_API_IsExist_Limiter_ById(unit_i, data->limiter_id, &exist) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "ERROR in MEA_API_IsExist_Limiter_ById  (%d) \n",
                data->limiter_id);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - UnDefined limiter Id (%d) \n",
                __FUNCTION__, data->limiter_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }

    /* check learning Action*/
    if (data->DSE_learning_actionId_valid /*&& data->DSE_learning_enable == MEA_TRUE*/){
		if (MEA_API_IsExist_Action_ByType(unit_i, data->DSE_learning_actionId, MEA_ACTION_TYPE_FWD, &existActionId) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsExist_Action Failed (id=%d)\n",
                __FUNCTION__,
                data->DSE_learning_actionId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        if (!existActionId){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - DSE_learning_actionId %d not exist\n",
                __FUNCTION__,
                data->DSE_learning_actionId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        if (globalMemoryMode == MEA_MODE_DOWNSTREEM_ONLY) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - learning action Id  is not allowed in downstream \n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }
    if ((data->DSE_learning_srcPort_force == MEA_TRUE) && (data->DSE_learning_enable == MEA_FALSE)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
            "%s - DSE_learning_srcPort (%d) need also learning enable\n",
            __FUNCTION__,
            data->DSE_learning_srcPort);
    }
    /* check learning srcPort*/
    if (data->DSE_learning_srcPort_force &&
        data->DSE_learning_enable == MEA_TRUE){
        if (!ENET_IsValid_Queue(unit_i,
            (ENET_QueueId_t)(data->DSE_learning_srcPort),
            MEA_TRUE)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - DSE_learning_srcPort (%d) is not valid output cluster\n",
                __FUNCTION__,
                data->DSE_learning_srcPort);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }
    if (data->DSE_learning_srcPort_force == MEA_TRUE &&
        data->DSE_learning_Vp_force == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - \r\nDSE_learning_srcPort_force == MEA_TRUE  and  DSE_learning_Vp_force Only one of them can be TRUE\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (data->DSE_learning_Vp_force == MEA_TRUE &&
        data->DSE_learning_actionId_valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - \r\nDSE_learning_Vp_force TRUE work only with Learn Action \n",
            __FUNCTION__);
        return MEA_ERROR;

    }

    if (data->LxCp_enable == MEA_TRUE){
        // check if the id is exist
        if (MEA_API_IsExist_LxCP_ById(unit_i,
            data->LxCp_Id, &exist_lxcp) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsExist_LxCP_ById \n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;

        }
        if (!exist_lxcp){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - UnDefined Lxcp Id (%d) \n",
                __FUNCTION__,
                data->LxCp_Id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }

    if (data->flowCoSMappingProfile_force) {

        if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - flowCoSMappingProfile_force is not support \n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (data->COS_FORCE != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - flowCoSMappingProfile_force (%d) is not allowed "
                " together with COS_FORCE (%d)\n",
                __FUNCTION__,
                data->flowCoSMappingProfile_force,
                data->COS_FORCE);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (MEA_API_Get_FlowCoSMappingProfile_Entry
            (unit_i,
            (MEA_FlowCoSMappingProfile_Id_t)(data->flowCoSMappingProfile_id),
            NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Flow CoS Mapping Profile %d get failed \n",
                __FUNCTION__,
                data->flowCoSMappingProfile_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }

    if (data->flowMarkingMappingProfile_force) {

        if (!MEA_FLOW_MARKING_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - flowMarkingMappingProfile_FORCE is not support \n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (data->L2_PRI_FORCE != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - flowMarkingMappingProfile_FORCE (%d) is not allowed "
                " together with L2_PRI_FORCE (%d)\n",
                __FUNCTION__,
                data->flowMarkingMappingProfile_force,
                data->L2_PRI_FORCE);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }


        if (MEA_API_Get_FlowMarkingMappingProfile_Entry
            (unit_i,
            (MEA_FlowMarkingMappingProfile_Id_t)(data->flowMarkingMappingProfile_id),
            NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Flow Marking Mapping Profile %d get failed \n",
                __FUNCTION__,
                data->flowMarkingMappingProfile_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */




    /* Get free serviceId */
    if (mea_Service_GetFreeServiceId(&serviceId, data->SRV_mode) != MEA_OK) {
        MEA_Service_rollback(unit_i, serviceId);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_Service_GetFreeServiceId failed \n", __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    /************************************************************************/
    /*  get Internal Id                                                    */
    /************************************************************************/
    if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
#if 0
        Internal_serviceId = MEA_PLAT_GENERATE_NEW_ID;

        if (mea_Service_GetFree_Internal_ServiceId(&Internal_serviceId) != MEA_OK)
        {
            MEA_Service_rollback(unit_i, serviceId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        MEA_Service_Table[serviceId].en_Sid_Internal = MEA_TRUE;
        MEA_Service_Table[serviceId].Sid_Internal = Internal_serviceId;
#endif
    }


    if (data->SRV_mode == MEA_SRV_MODE_EXTERNAL){ /*   external */

        /************************************************************************/
        /*    Compute_Hash  Hash                                                */
        /************************************************************************/

        MEA_OS_memset(&HW_SRV_key,0,sizeof(HW_SRV_key));
        mea_Service_Bulid_Hash_info(unit_i, key, &HW_SRV_key);

		hashAddres = mea_drv_Service_External_addres_F_Hash(unit_i, HW_SRV_key);
        if (hashAddres >= MEA_MAX_NUM_OF_SERVICES_EXTERNAL_HW) {
            MEA_Service_rollback(unit_i, serviceId);
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n - hashAddres >= MEA_MAX_NUM_OF_SERVICES_EXTERNAL_HW failed hashAddres %d \n", __FUNCTION__,hashAddres);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

		/************************************************************************/
		/* check if exist  hashAddres                                           */
		/************************************************************************/
		/*TBD need to read from DDR if exist */


        /************************************************************************/
        /* if we mode of service 2 Address                                      */
        /************************************************************************/
        switch (Global_entry.if_External_srv.val.if_External_srv_mode){
        case 0:
            /*check if the address hash exist*/
            if (MEA_IS_CLEAR_SID(MEA_Service_External_Hash_BIT, hashAddres) == MEA_TRUE){
                MEA_SET_SID(MEA_Service_External_Hash_BIT, hashAddres);
                MEA_Service_Table[serviceId].hashaddress_External = hashAddres;

            }
            else{
                collision_ddr = MEA_TRUE;
            }
            break;
        case 1:
            /*check if the address hash address 0 exist*/
            /*if not take the address+1 */

            /*check if the address hash address 1 exist*/
            if (MEA_IS_CLEAR_SID(MEA_Service_External_Hash_BIT, hashAddres) == MEA_TRUE){
                MEA_SET_SID(MEA_Service_External_Hash_BIT, hashAddres);
                MEA_Service_Table[serviceId].hashaddress_External = hashAddres;
            }
            else{
                hashAddres += 1;
                if (MEA_IS_CLEAR_SID(MEA_Service_External_Hash_BIT, (hashAddres)) == MEA_TRUE){
                   
                    MEA_SET_SID(MEA_Service_External_Hash_BIT, hashAddres );
                    MEA_Service_Table[serviceId].hashaddress_External = hashAddres;
                }
                else{
                    //collision on the DDR
                    collision_ddr = MEA_TRUE;
                   }

            }



            break;


        }

        if (collision_ddr == MEA_TRUE){
            
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"*** MEA_COLLISION DDR hashAddres = 0x%x and  0x%x \n", (hashAddres - 1), (hashAddres));
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"key: net1 = 0x%08x net2 = 0x%08x\n", key->net_tag, key->inner_netTag_from_value);
            /*fine the service of the collision */
            {
                MEA_Uint32  sId;
                MEA_Uint32 find_two_Sid;
                MEA_Uint32 save_sevice[2];
                save_sevice[0]=0;
                save_sevice[1] = 0;

                find_two_Sid = 0;
                for (sId = 1; sId < MEA_MAX_NUM_OF_SERVICES; sId++) {
                        if (MEA_Service_Table[sId].data.valid == MEA_TRUE) {
                            if (MEA_Service_Table[sId].hashaddress_External == (hashAddres - 1)) {
                                /*save Sid*/
                                save_sevice[find_two_Sid] = sId;
                                
                                find_two_Sid++;
                            }
                            if (MEA_Service_Table[sId].hashaddress_External == (hashAddres)) {
                                /*save Sid*/
                                save_sevice[find_two_Sid] = sId;
                                find_two_Sid++;
                            }
                            if (find_two_Sid == 2) {
                                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "The Services with COLLISION are SID1 %d SID2 %d \n", save_sevice[0], save_sevice[1]);
                                break;
                            }

                        }
                    }
            }
            
            MEA_Service_rollback(unit_i, serviceId);
            return MEA_COLLISION;
        }


    } // end SRV








    /* Add the service Range */
	if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE && data->SRV_mode == MEA_SRV_MODE_REGULAR){
        if (mea_drv_serviceRange_Add(unit_i,
            serviceId,
            key,
            &rangeId) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - add serviceRange failed \n", __FUNCTION__);
            MEA_Service_rollback(unit_i, serviceId);/*rollback*/

            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }

    /* Get Current HwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i, hwUnit, return MEA_ERROR;)


        /* Allocate new HwId
         Note: we create the entry in the service hash even if it is range
         because other objects (like action - depend on this) */
         if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
             if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE){
                 hwId = (MEA_db_HwId_t)(MEA_ServiceRange_Table[rangeId].ptr_context);
                 if (hwId == 0){
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "HWID can't be zero  rangeId %d \n", rangeId);

                 }


             }
             else {
                 if (serviceId == MEA_DB_GENERATE_NEW_ID)
                     hwId = (MEA_db_HwId_t)MEA_DB_GENERATE_NEW_ID;
                 else
                     hwId = MEA_Service_Table[serviceId].Sid_Internal; //serviceId;
             }
         }

    if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
        if (mea_db_Create(unit_i,
            MEA_Service_Internal_db,
            (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal,
            hwUnit,
            &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Create failed (swId=%d,hwUnit=%d) \n",
                __FUNCTION__, serviceId, hwUnit);


            if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                mea_drv_serviceRange_Delete(rangeId);

            MEA_Service_rollback(unit_i, serviceId);

            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (mea_db_Create(unit_i,
            MEA_Service_Internal_db_getHW,
            (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal,
            hwUnit,
            &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Create failed (swId=%d,hwUnit=%d) \n",
                __FUNCTION__, serviceId, hwUnit);


            if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                mea_drv_serviceRange_Delete(rangeId);

            MEA_Service_rollback(unit_i, serviceId);

            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }


        



    }

    /************************************************************************/
    /* VPLSi DL Service                                                     */
    /************************************************************************/

    if (MEA_VPLS_SUPPORT)
    {
        MEA_Bool vpls_exist = MEA_FALSE;
        MEA_OutPorts_Entry_dbt     vpls_OutPorts;

        if (data->en_vpls_Ins == MEA_TRUE){
            data->COS_FORCE = MEA_TRUE;
            data->COS = 7;
            if (OutPorts_Entry == NULL || EHP_Entry == NULL || EHP_Entry->ehp_info == NULL){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    " those parmeter suould not set it NULL  (OutPorts_Entry == NULL OR EHP_Entry == NULL) \n");
				if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                    mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                    mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                    
                    if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                        mea_drv_serviceRange_Delete(rangeId);
                }

                MEA_Service_rollback(unit_i, serviceId); /*rollback*/

                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);


                return MEA_ERROR;
            }


            if (MEA_API_VPLSi_isExist(unit_i, (MEA_Uint16)data->vpls_Ins_Id, &vpls_exist) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    " VPLSi must creat before the Dl Service \n");
                if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                    mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                    mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                    if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                        mea_drv_serviceRange_Delete(rangeId);
                }

                MEA_Service_rollback(unit_i, serviceId); /*rollback*/
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
            if (vpls_exist == MEA_FALSE){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " fail the vpls_Ins_Id %d is not exist\n", data->vpls_Ins_Id);
                if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                    mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)serviceId, hwUnit);
                    mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);

                    if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                        mea_drv_serviceRange_Delete(rangeId);
                }

                MEA_Service_rollback(unit_i, serviceId); /*rollback*/
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

                return MEA_ERROR;
            }
            else {
                if (mea_drv_vplsi_Get_outport((MEA_Uint16)data->vpls_Ins_Id, &vpls_OutPorts) != MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        " mea_drv_vplsi_Get_outport fail \n");
                    return MEA_ERROR;
                }
                MEA_OS_memcpy(OutPorts_Entry, &vpls_OutPorts, sizeof(MEA_OutPorts_Entry_dbt));
                data->editId = 0;
                data->flood_ED = MEA_TRUE;
                MEA_OS_memset(&EHP_Entry->ehp_info[0], 0, sizeof(*EHP_Entry->ehp_info));
                EHP_Entry->num_of_entries = 1;
                EHP_Entry->ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_OFSET12;
                EHP_Entry->ehp_info[0].ehp_data.eth_info.val.all = (MEA_Uint32)(0x000001ff + ((MEA_Uint32)(data->vpls_Ins_Id << MEA_VPLS_ED_ID_SHIFT)));

            }
        }
    }

    if (OutPorts_Entry != NULL) {
        if (ENET_queue_check_OutPorts(unit_i, OutPorts_Entry) != ENET_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_queue_check_OutPorts Fail \n",
                __FUNCTION__);
            if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);

                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                    mea_drv_serviceRange_Delete(rangeId);
            }

            MEA_Service_rollback(unit_i, serviceId); /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;

        }
    }




    if (data->DSE_learning_actionId_valid) {
        if (MEA_API_AddOwner_Action(unit_i,
            data->DSE_learning_actionId) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_AddOwner_Action failed (id=%d)\n",
                __FUNCTION__,
                data->DSE_learning_actionId);

            if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                    mea_drv_serviceRange_Delete(rangeId);
            }

            MEA_Service_rollback(unit_i, serviceId); /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

            return MEA_ERROR;
        }
    }

    /* Add owner to LxCP */
    if (data->LxCp_enable) {
        if (mea_lxcp_drv_Add_owner(unit_i, data->LxCp_Id) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "ERROR: mea_lxcp_drv_Add_owner(%d) \n",
                data->LxCp_Id);
            if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);

                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                    mea_drv_serviceRange_Delete(rangeId);
            }

            MEA_Service_rollback(unit_i, serviceId); /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }




    /* Add the service to the service link list */
    if (mea_Service_Add(serviceId) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_Service_Add failed \n", __FUNCTION__);
        /* rollback */
        if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
            mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                mea_drv_serviceRange_Delete(rangeId);
        }

        MEA_Service_rollback(unit_i, serviceId); /*rollback*/
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /*color_aware */
    /*data->CM = Policer_Entry->color_aware;*/
    if (data->CM == 1) {
        if (data->COLOR_FORSE == MEA_ACTION_FORCE_COMMAND_DISABLE){
            data->COLOR_FORSE = MEA_ACTION_FORCE_COMMAND_DISABLE;
            data->COLOR = MEA_POLICER_COLOR_VAL_YELLOW;
        }

    }
    else {
        if (data->COLOR_FORSE == MEA_ACTION_FORCE_COMMAND_DISABLE){
            data->COLOR_FORSE = MEA_ACTION_FORCE_COMMAND_VALUE;
            data->COLOR = MEA_POLICER_COLOR_VAL_GREEN;
        }
    }

    /* set the caller data->valid to MEA_TRUE (not free )  */
    data->valid = MEA_Service_Table[serviceId].data.valid;

    if (MEA_ACL_SUPPORT == MEA_FALSE && data->filter_enable == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  ACL NOT SUPPORT you to allowed to set the filter_enable\n",
            __FUNCTION__);
        if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
            mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);

            if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                mea_drv_serviceRange_Delete(rangeId);
        }

        MEA_Service_rollback(unit_i, serviceId); /*rollback*/
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    /* handle filtering if needed */

    if (data->filter_enable){

        

        if((mea_drv_set_Filter_Mask_Prof(unit_i,
        &(data->ACL_filter_info),
        &filter_mask_id) != MEA_OK)) {
        data->valid = MEA_FALSE;

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_set_Filter_Mask_Prof failed\n",
            __FUNCTION__);
        mea_Service_Delete(serviceId);
        if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
            mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                mea_drv_serviceRange_Delete(rangeId);
        }


        MEA_Service_rollback(unit_i, serviceId); /*rollback*/
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
        }
        /***************************/
        /* Set Filter prof         */
        /***************************/
        if (data->ACL_Prof_valid == MEA_TRUE) {
            if (data->ACL_Prof >= MEA_ACL_SRV_PROFILE_END) {
                MEA_Service_rollback(unit_i, serviceId); /*rollback*/
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ACL_Prof %d  >= Max %d \n",
                    __FUNCTION__, data->ACL_Prof, MEA_ACL_SRV_PROFILE_END-1);
                return MEA_ERROR;
            }
        }




    }

    if (data->ACL_filter_info.filter_Whitelist_enable == MEA_TRUE && data->filter_enable == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "%s - filter_Whitelist_enable work only with filter enable\n", __FUNCTION__);
    }
    
    /* Handle the xPermission */
    if (OutPorts_Entry != NULL) {
        if ((data->MSTP_Info.state != MEA_MSTP_TYPE_STATE_BLOCKING_SRV) &&
            (data->MSTP_Info.state != MEA_MSTP_TYPE_STATE_BLOCKING_SRV_VPN0))
            MEA_OS_memcpy((char*)(&xPermission), (char*)OutPorts_Entry, sizeof(xPermission));
        else {
            MEA_OS_memset((char*)(&xPermission), 0, sizeof(xPermission));
        }

        
    } else {
        MEA_OS_memset((char*)(&xPermission),0,sizeof(xPermission));
    }

    MEA_OS_memset(&LearnxPermission, 0, sizeof(LearnxPermission));

    if((data->DSE_learning_Vp_force == MEA_TRUE) && data->DSE_learning_actionId_valid) {
        
        if (mea_Action_GetOutport(unit_i, data->DSE_learning_actionId, &LearnxPermission) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "%s - mea_Action_GetOutport fail \n", __FUNCTION__);
            mea_Service_Delete(serviceId);
            if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                
                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                    mea_drv_serviceRange_Delete(rangeId);
            }

            MEA_Service_rollback(unit_i, serviceId); /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        
    }



 

    if (! MEA_API_Get_Warm_Restart() )  
      xPermissionId = ENET_PLAT_GENERATE_NEW_ID;
    else
      xPermissionId = data->save_hw_xPermision;

    
        if (!MEA_API_Get_Warm_Restart())
            LearnxPermissionId = ENET_PLAT_GENERATE_NEW_ID;
        else
            LearnxPermissionId = data->save_hw_LearnxPermision;
  


    if(key->src_port == 127){
    isCpu =MEA_TRUE;
    }else {
    isCpu = MEA_FALSE;
    }

    if (enet_Create_xPermission(unit_i,
                                &xPermission,
                                isCpu,
                                &xPermissionId) != ENET_OK) {
        MEA_Service_Table[serviceId].data.valid = MEA_FALSE;
        data->valid = MEA_FALSE;
        mea_Service_Delete(serviceId);
        if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
            mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
           if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                mea_drv_serviceRange_Delete(rangeId);
        }

        MEA_Service_rollback(unit_i, serviceId); /*rollback*/
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    
    data->save_hw_xPermision = xPermissionId;

    
        if (enet_Create_xPermission(unit_i,
            &LearnxPermission,
            isCpu,
            &LearnxPermissionId) != ENET_OK) {
            MEA_Service_Table[serviceId].data.valid = MEA_FALSE;
            data->valid = MEA_FALSE;
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Create_xPermission LearnxPermissionfailed\n",
                __FUNCTION__);
            enet_Delete_xPermission(unit_i, xPermissionId);
            mea_Service_Delete(serviceId);
            if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                
                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                    mea_drv_serviceRange_Delete(rangeId);
            }

            MEA_Service_rollback(unit_i, serviceId); /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        data->save_hw_LearnxPermision = LearnxPermissionId;
   




    /* Create the Action */
    MEA_OS_memset((char*)&Action_Data,0,sizeof(Action_Data));
    Action_Data.flowMarkingMappingProfile_force = (MEA_Uint16)data->flowMarkingMappingProfile_force;
    Action_Data.flowMarkingMappingProfile_id    = (MEA_Uint16)data->flowMarkingMappingProfile_id;
    Action_Data.flowCoSMappingProfile_force     = (MEA_Uint16)data->flowCoSMappingProfile_force;
    Action_Data.flowCoSMappingProfile_id        = (MEA_Uint16)data->flowCoSMappingProfile_id;
    Action_Data.force_l2_pri_valid = data->L2_PRI_FORCE;
    Action_Data.L2_PRI             = data->L2_PRI;
    Action_Data.force_cos_valid    = data->COS_FORCE;
    Action_Data.COS                = data->COS;
    Action_Data.force_color_valid  = data->COLOR_FORSE;
    Action_Data.COLOR              = data->COLOR;
    Action_Data.output_ports_valid = MEA_TRUE;
    Action_Data.proto_llc_valid    = MEA_TRUE;
    Action_Data.protocol_llc_force = data->protocol_llc_force;
    Action_Data.Llc                = data->Llc;
    Action_Data.Protocol           = data->Protocol;
    Action_Data.ed_id              = data->editId;
    Action_Data.ed_id_valid        = MEA_TRUE;
    Action_Data.pm_id              = data->pmId;
    Action_Data.pm_id_valid        = MEA_TRUE;
    Action_Data.pm_type            = data->pm_type;
    Action_Data.tm_id              = data->tmId;
    Action_Data.tm_id_valid        = MEA_TRUE;
    Action_Data.tmId_disable       = data->tmId_disable;
    Action_Data.fragment_enable    = (MEA_Uint16)data->fragment_enable;
    Action_Data.fwd_ingress_TS_type = data->ingress_TS_type;
#ifdef MEA_TDM_SW_SUPPORT 
    Action_Data.tdm_packet_type = data->tdm_packet_type;
    Action_Data.tdm_remove_byte = data->tdm_remove_byte;
    Action_Data.tdm_remove_line = data->tdm_remove_line;
    Action_Data.tdm_ces_type    = data->tdm_ces_type;   
    Action_Data.tdm_used_vlan   = data->tdm_used_vlan ; 
    Action_Data.tdm_used_rtp    = data->tdm_used_rtp; 
    
#endif
    
    Action_Data.afdx_enable        = data->afdx_enable;
    Action_Data.afdx_Rx_sessionId  = data->afdx_Rx_sessionId;
    Action_Data.afdx_A_B           = data->afdx_A_B;
    Action_Data.afdx_Bag_info      = data->afdx_Bag_info;
    Action_Data.set_1588           = data->set_1588;
    Action_Data.mtu_Id             = data->mtu_Id;
    Action_Data.lag_bypass         = data->lag_bypass;
    Action_Data.flood_ED           = data->flood_ED;
    Action_Data.Drop_en            = data->Drop_en;
    

    Action_Data.overRule_vpValid = data->overRule_vpValid;
    Action_Data.overRule_vp_val  = data->overRule_vp_val;

    Action_Data.Fragment_IP = data->Fragment_IP;
    Action_Data.IP_TUNNEL = data->IP_TUNNEL;
    Action_Data.Defrag_ext_IP  = data->Defrag_ext_IP;
    Action_Data.Defrag_int_IP = data->Defrag_int_IP;
    Action_Data.Defrag_Reassembly_L2_profile = data->Defrag_Reassembly_L2_profile;
    
    Action_Data.fragment_ext_int         = data->fragment_ext_int;
    Action_Data.fragment_target_mtu_prof = data->fragment_target_mtu_prof;
    Action_Data.fwd_win                  = data->fwd_win;
    Action_Data.sflow_Type               = data->sflow_Type;
    Action_Data.overRule_bfd_sp          = data->overRule_bfd_sp;
	
    Action_Data.policer_prof_id=data->policer_prof_id;
	Action_Data.policer_prof_id_valid=MEA_TRUE;
	
		





    if (!data->protocol_llc_force) {
        MEA_Uint32 protocol;
        MEA_Bool   llc;
           if ((key->virtual_port == MEA_FALSE) &&
               mea_Calc_ProtocolAndLlc(unit_i,
                                    (MEA_Port_t)(key->src_port),
                                    OutPorts_Entry,
                                    &protocol,
                                     &llc ) != MEA_OK ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_Calc_ProtocolAndLlc failed \n",
                                __FUNCTION__);
            enet_Delete_xPermission(unit_i,xPermissionId);
            if (data->DSE_learning_Vp_force == MEA_TRUE){
                enet_Delete_xPermission(unit_i, LearnxPermissionId);
            }
            
            data->valid = MEA_FALSE;
           
            mea_Service_Delete(serviceId);
            if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            
                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                    mea_drv_serviceRange_Delete(rangeId);
            }

            MEA_Service_rollback(unit_i, serviceId); /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        Action_Data.proto_llc_valid    = MEA_TRUE;
        Action_Data.protocol_llc_force = MEA_TRUE;
        Action_Data.Protocol           = protocol;
        Action_Data.Llc                = llc;
    } 

     

    if ((EHP_Entry) || (data->editId != 0)) {
        Action_Editing = EHP_Entry;
    } else {
        static MEA_Bool Action_def_Editing_first_time = MEA_TRUE;
        static MEA_EgressHeaderProc_Array_Entry_dbt    Action_def_Editing;
        static MEA_EHP_Info_dbt                        Action_def_Editing_Info;
        if (Action_def_Editing_first_time) {
            MEA_OS_memset(&Action_def_Editing     ,0,sizeof(Action_def_Editing));
            MEA_OS_memset(&Action_def_Editing_Info,0,sizeof(Action_def_Editing_Info));
            Action_def_Editing.num_of_entries = 1;
            Action_def_Editing.ehp_info = &Action_def_Editing_Info;
            Action_def_Editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = 1;
            Action_def_Editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
            Action_def_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid = MEA_TRUE;
            Action_def_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc = 13;
            Action_def_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid = MEA_TRUE;
            Action_def_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc = 14;
            Action_def_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid = MEA_TRUE;
            Action_def_Editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc = 15;
            Action_def_Editing_first_time = MEA_FALSE;
        }
        Action_Editing = &Action_def_Editing;
    }

    Action_Data.wred_profId = data->wred_profId;
    //check if the wred profile exist must before the create Action.
    {
        MEA_WRED_Profile_Key_dbt  key_i;
        key_i.acm_mode = 0;
        key_i.profile_id = data->wred_profId;


        if (MEA_API_IsValid_WRED_Profile(unit_i, &key_i, MEA_TRUE) != MEA_TRUE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsValid_WRED_Profile failed  wred profile %d not valid\n",
                __FUNCTION__, data->wred_profId);
            enet_Delete_xPermission(unit_i, xPermissionId);
            if (data->DSE_learning_Vp_force == MEA_TRUE){
                enet_Delete_xPermission(unit_i, LearnxPermissionId);
            }
            MEA_Service_Table[serviceId].data.valid = MEA_FALSE;
            data->valid = MEA_FALSE;
            mea_Service_Delete(serviceId);
            if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);

                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                    mea_drv_serviceRange_Delete(rangeId);
            }

            MEA_Service_rollback(unit_i, serviceId); /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }

    if (MEA_Service_Table[serviceId].en_Sid_Internal == MEA_TRUE){
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_SERVICE);
		Action_Data.Action_type = MEA_ACTION_TYPE_SERVICE;

        Action_Id = (MEA_Action_t)MEA_Service_Table[serviceId].Sid_Internal;
    }
    else{
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_SERVICE_EXTERNAL);
        Action_Id = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX + MEA_Service_Table[serviceId].Sid_External;
		Action_Data.Action_type = MEA_ACTION_TYPE_SERVICE_EXTERNAL;

    }



    if (Action_Editing){
        if (Action_Editing->ehp_info[0].ehp_data.Attribute_info.valid == MEA_FALSE && Action_Editing->ehp_info[0].ehp_data.EditingType == 0){
                if (Action_Data.protocol_llc_force == MEA_TRUE){
                    Action_Editing->ehp_info[0].ehp_data.Attribute_info.protocol = Action_Data.Protocol;
                    Action_Editing->ehp_info[0].ehp_data.Attribute_info.llc = Action_Data.Llc;
                    Action_Editing->ehp_info[0].ehp_data.Attribute_info.valid = MEA_TRUE;
            }
        }
    }

    if (MEA_API_Create_Action(unit_i,
        &Action_Data,
        OutPorts_Entry,
        Policer_Entry,
        Action_Editing,
        &Action_Id) != MEA_OK) {
        enet_Delete_xPermission(unit_i, xPermissionId);
        if (data->DSE_learning_Vp_force == MEA_TRUE){
            enet_Delete_xPermission(unit_i, LearnxPermissionId);
        }
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Create_Action failed \n",
            __FUNCTION__);

        data->valid = MEA_FALSE;
        mea_Service_Delete(serviceId);
        if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
            mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);

            if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                mea_drv_serviceRange_Delete(rangeId);
        }

        MEA_Service_rollback(unit_i, serviceId); /*rollback*/
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);

        return MEA_ERROR;
    }
    //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);

    if (data->FWD_DaMAC_valid == MEA_TRUE)
    {
        MEA_Uint16 id_FWD_enb = 0;

        if (mea_drv_Create_FWD_ENB_DA_Prof(unit_i, &id_FWD_enb , data->FWD_DaMAC_value) != MEA_OK)
        {

            enet_Delete_xPermission(unit_i, xPermissionId);
            if (data->DSE_learning_Vp_force == MEA_TRUE){
                enet_Delete_xPermission(unit_i, LearnxPermissionId);
            }
            //mea_Action_SetType(unit_i, ((data->SRV_mode == MEA_SRV_MODE_REGULAR) ? (MEA_ACTION_TYPE_SERVICE) : (MEA_ACTION_TYPE_SERVICE_EXTERNAL)));
			MEA_DRV_Delete_Action(unit_i, Action_Id, ((data->SRV_mode == MEA_SRV_MODE_REGULAR) ? (MEA_ACTION_TYPE_SERVICE) : (MEA_ACTION_TYPE_SERVICE_EXTERNAL)));
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);

            data->valid = MEA_FALSE;
            mea_Service_Delete(serviceId);
            if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                    mea_drv_serviceRange_Delete(rangeId);
            }

            MEA_Service_rollback(unit_i, serviceId); /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);



            return MEA_ERROR;
        }
        data->FWD_DaMAC_ID = id_FWD_enb;
    }
 


    /* Build the new entry */
    MEA_OS_memset((char*)(&entry),0,sizeof(entry));
    entry.en_Sid_Internal      = MEA_Service_Table[serviceId].en_Sid_Internal;
    entry.Sid_Internal         = MEA_Service_Table[serviceId].Sid_Internal;
    entry.en_Sid_External      = MEA_Service_Table[serviceId].en_Sid_External;
    entry.Sid_External         = MEA_Service_Table[serviceId].Sid_External;
    entry.hashaddress_External = MEA_Service_Table[serviceId].hashaddress_External;

    MEA_OS_memcpy((char*)(&(entry.key)),
                  key,
                  sizeof(entry.key));
    MEA_OS_memcpy((char*)(&(entry.data)),
                  data,
                  sizeof(entry.data));

    entry.data.editId  = Action_Data.ed_id;
    entry.data.pmId    = Action_Data.pm_id;
    entry.data.tmId    = Action_Data.tm_id;
	entry.data.policer_prof_id   = Action_Data.policer_prof_id;
    /* update protocol llc */
    entry.data.protocol_llc_force = Action_Data.protocol_llc_force;
    entry.data.Protocol = Action_Data.Protocol;
    entry.data.Llc      = Action_Data.Llc;

    if (OutPorts_Entry != NULL) {
        MEA_OS_memcpy((char*)(&(entry.out_ports)),
                      OutPorts_Entry,
                      sizeof(entry.out_ports));
    }


   


    MEA_OS_memcpy((char*)(&(entry.Learn_OutPorts)),
        &LearnxPermission,
        sizeof(MEA_OutPorts_Entry_dbt));
    

 

    entry.xPermissionId = xPermissionId;
    entry.LearnxPermissionId = LearnxPermissionId;

    entry.filter_mask_id = filter_mask_id;
    entry.service_Type = service_Type;
    entry.rangeId      = rangeId;

    /*--------FWD ENB DA--------------------*/
   
    





    /* Update the hardware */
    if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
        if (mea_Service_UpdateHw(unit_i,
            serviceId,
            &entry,
            &(MEA_Service_Table[serviceId]),
            MEA_SETTING_TYPE_CREATE) != MEA_OK) {
            enet_Delete_xPermission(unit_i, xPermissionId);
            if (data->DSE_learning_Vp_force == MEA_TRUE){
                enet_Delete_xPermission(unit_i, LearnxPermissionId);
            }
//            mea_Action_SetType(unit_i, ((data->SRV_mode == MEA_SRV_MODE_REGULAR) ? (MEA_ACTION_TYPE_SERVICE) : (MEA_ACTION_TYPE_SERVICE_EXTERNAL)));
			MEA_DRV_Delete_Action(unit_i, Action_Id, ((data->SRV_mode == MEA_SRV_MODE_REGULAR) ? (MEA_ACTION_TYPE_SERVICE) : (MEA_ACTION_TYPE_SERVICE_EXTERNAL)));
//            mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);


            if (data->FWD_DaMAC_valid == MEA_TRUE){
                mea_drv_Delete_FWD_ENB_DA_Prof(unit_i, data->FWD_DaMAC_ID);
            }




            data->valid = MEA_FALSE;
            mea_Service_Delete(serviceId);
            if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                    mea_drv_serviceRange_Delete(rangeId);
            }

            MEA_Service_rollback(unit_i, serviceId); /*rollback*/
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

            return MEA_ERROR;
        }

        if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE) {
            if (mea_drv_serviceRange_UpdateHw(unit_i,
                serviceId,
                rangeId,
                &(MEA_ServiceRange_Table[rangeId]),
                NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_ServiceRange_UpdateHw failed \n", __FUNCTION__);
                enet_Delete_xPermission(unit_i, xPermissionId);
                if (data->DSE_learning_Vp_force == MEA_TRUE){
                    enet_Delete_xPermission(unit_i, LearnxPermissionId);
                }
//                mea_Action_SetType(unit_i, (MEA_ACTION_TYPE_SERVICE));
				MEA_DRV_Delete_Action(unit_i, Action_Id, MEA_ACTION_TYPE_SERVICE);
//                mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);

                data->valid = MEA_FALSE;
                mea_Service_Delete(serviceId);
                mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
                mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);

                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE){ /* delete service range*/
                    mea_drv_serviceRange_Delete(MEA_Service_Table[serviceId].rangeId);
                }
                if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE) mea_drv_serviceRange_Delete(rangeId);


                MEA_Service_rollback(unit_i, serviceId); /*rollback*/
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
        }
    }
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    if(mea_service_srcport_lean(unit_i,
        serviceId,
        &entry,
        &(MEA_Service_Table[serviceId]),
        MEA_SETTING_TYPE_CREATE) != MEA_OK) {
        enet_Delete_xPermission(unit_i, xPermissionId);
        if (data->DSE_learning_Vp_force == MEA_TRUE){
            enet_Delete_xPermission(unit_i, LearnxPermissionId);
        }
//        mea_Action_SetType(unit_i, ((data->SRV_mode == MEA_SRV_MODE_REGULAR) ? (MEA_ACTION_TYPE_SERVICE) : (MEA_ACTION_TYPE_SERVICE_EXTERNAL)));
		MEA_DRV_Delete_Action(unit_i, Action_Id, ((data->SRV_mode == MEA_SRV_MODE_REGULAR) ? (MEA_ACTION_TYPE_SERVICE) : (MEA_ACTION_TYPE_SERVICE_EXTERNAL)));
//        mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);

        data->valid = MEA_FALSE;
        mea_Service_Delete(serviceId);
        if (data->SRV_mode == MEA_SRV_MODE_REGULAR){
            mea_db_Delete(unit_i, MEA_Service_Internal_db, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId].Sid_Internal, hwUnit);
            if (service_Type == MEA_SERVICE_CLASSIFCATION_RANGE)
                mea_drv_serviceRange_Delete(rangeId);
        }

        MEA_Service_rollback(unit_i, serviceId); /*rollback*/
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

        return MEA_ERROR;
        }
    
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/


    /* Update the Software shadow */
    MEA_OS_memcpy((char*)(&(MEA_Service_Table[serviceId])),
                  &entry,
                  sizeof(MEA_Service_Table[0]));


    /* Update output parameters */
    data->editId			= Action_Data.ed_id;
    data->pmId				= Action_Data.pm_id;
    data->tmId				= Action_Data.tm_id;
	data->policer_prof_id   = Action_Data.policer_prof_id;
         
	*o_serviceId = serviceId;
    if (data->SRV_mode == MEA_SRV_MODE_EXTERNAL){
       
        MEA_service_setting_SRV_ON_External_DDR(unit_i, serviceId, MEA_SETTING_TYPE_CREATE);

    }

    if (MEA_VPLS_SUPPORT)
    {
        /*Add to database VPLS the service*/
        if (data->en_vpls_Ins == MEA_TRUE) {
            MEA_drv_VPLS_Service(data->vpls_Ins_Id,serviceId, key->src_port);
        }
    }
  

    /* return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}

void mea_drv_ServiceGetInternal_group(MEA_Service_t serviceId, MEA_Uint8 *hashGroup)
{
    *hashGroup = MEA_Service_Table[serviceId].hashGroup;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Create_Service>                                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_Service(MEA_Unit_t                              unit_i,
								MEA_Service_Entry_Key_dbt              *key,
								MEA_Service_Entry_Data_dbt             *data,
								MEA_OutPorts_Entry_dbt                 *OutPorts_Entry,
								MEA_Policer_Entry_dbt                  *Policer_Entry,
								MEA_EgressHeaderProc_Array_Entry_dbt   *EHP_Entry,
								MEA_Service_t                          *o_serviceId)
{

    MEA_Service_t save_o_serviceId;
    MEA_Status retVal;
    MEA_SRV_mode_t SRV_Mode;

    /* Check for valid key parameter */
    if (key == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
	
    /* Check for valid key->src_port parameter */
    if ((key->virtual_port == MEA_FALSE)
        && (MEA_API_Get_IsPortValid((MEA_Port_t)(key->src_port), MEA_PORTS_TYPE_INGRESS_TYPE, MEA_FALSE) == MEA_FALSE
		)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - port %d is invalid\n",
            __FUNCTION__, key->src_port);
        return MEA_ERROR;
    }


    /* Check for valid data parameter */
    if (data == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data == NULL\n",
            __FUNCTION__);

        return MEA_ERROR;
    }

    if (data->SRV_mode == MEA_SRV_MODE_EXTERNAL && MEA_SERVICES_EXTERNAL_SUPPORT == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - External service not support\n",
            __FUNCTION__);

        return MEA_ERROR;
    }


    /************************************************************************/
    /*   decision Of Create Service                                         */
    /* EVC Service Internal                                                 */
    /* Default service must be on Internal                                 */
    /************************************************************************/

    if ((key->evc_enable == MEA_TRUE) || (key->L2_protocol_type == MEA_PARSING_L2_KEY_DEF_SID)){
        data->SRV_mode = MEA_SRV_MODE_REGULAR; /* internal */
    }



    SRV_Mode = data->SRV_mode;

    switch (SRV_Mode)
    {
    case MEA_SRV_MODE_NORMAL:
        data->SRV_mode = MEA_SERVICES_DEFUALT_SRV_MODE(data->SRV_mode);
        save_o_serviceId = *o_serviceId;
        retVal = MEA_DRV_Create_Service(unit_i, key, data, OutPorts_Entry, Policer_Entry, EHP_Entry, o_serviceId);
        if (retVal == MEA_COLLISION){
            data->SRV_mode = MEA_SRV_MODE_REGULAR;
            *o_serviceId = save_o_serviceId;
#ifdef NOT_SUPPORT_INTERNAL           
            if (SRV_Mode == MEA_SRV_MODE_REGULAR) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Internal service not support on This HW\n", __FUNCTION__);
                return MEA_COLLISION;

            }
#endif

            retVal = MEA_DRV_Create_Service(unit_i, key, data, OutPorts_Entry, Policer_Entry, EHP_Entry, o_serviceId);
        }
        break;
    case MEA_SRV_MODE_REGULAR:
    case MEA_SRV_MODE_EXTERNAL:
    default:
#ifdef NOT_SUPPORT_INTERNAL 
        if (SRV_Mode == MEA_SRV_MODE_REGULAR) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Internal service not support on This HW\n", __FUNCTION__);
            return MEA_ERROR;

        }
#endif

        retVal = MEA_DRV_Create_Service(unit_i, key, data, OutPorts_Entry, Policer_Entry, EHP_Entry, o_serviceId);
        break;
    }


    return retVal;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_Service>                                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_API_Set_Service    (MEA_Unit_t                      unit_i,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Policer_Entry_dbt          *Policer_Entry,
                                    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry)

{
    
    MEA_Service_Entry_dbt  entry;
    MEA_Bool existActionId;
    MEA_Bool exist_lxcp; 
    ENET_xPermission_dbt  xPermission;
    ENET_xPermissionId_t  xPermissionId;

    ENET_xPermission_dbt      LearnxPermission;
    ENET_xPermissionId_t      LearnxPermissionId;

    MEA_FilterMask_t      filter_mask_id;
    MEA_Action_Entry_Data_dbt Action_Data;
    MEA_Service_Entry_Key_dbt           key;
    MEA_Bool                            isCpu;
    MEA_Action_t                        Action_Id;
    MEA_Bool                            exsit;

    
  
//     MEA_EgressHeaderProc_Array_Entry_dbt ehp_array;
//     MEA_EHP_Info_dbt                ehp_info;
//     MEA_Uint32                      NumOfMulticastEditor;



    mea_memory_mode_e save_globalMemoryMode;
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool exist; 
#endif

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Verify that this is not Reserved CPU ServiceId */
    if (serviceId == MEA_CPU_SERVICE_ID) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -You are not allowed to modify Reserved CPU ServiceId %d \n",
                         __FUNCTION__,serviceId);
       return MEA_ERROR; 
    }

    /* Check for valid key->src_port parameter */
    if (MEA_API_IsExist_Service_ById(unit_i,
                                     serviceId,
                                     &exist) != MEA_OK)  {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -MEA_API_IsExist_Service_ById %d failed \n",
                         __FUNCTION__,serviceId);
       return MEA_ERROR; 
    }
    if (exist == MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - serviceId %d not exist \n",
                         __FUNCTION__,serviceId);
       return MEA_ERROR; 
    }
	if (data == NULL) 
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "(data == NULL \n");
		return MEA_ERROR;
	}
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    
    /* Set the globalMemoryMode */
    MEA_DRV_SET_GLOBAL_MEMORY_MODE
        (save_globalMemoryMode,
           mea_Service_GetHwUnit(unit_i,
                               &(MEA_Service_Table[serviceId].key)));


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

      if(MEA_API_Get_Service    (unit_i,serviceId,
                                    &key,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL) !=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_Service (%d) filed to get only the key ",
                              __FUNCTION__,serviceId);
      
      }


 

        if (data->SRV_mode != MEA_Service_Table[serviceId].data.SRV_mode){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - not allow to modify SRV_mode  from %d to %d  \n ",
                __FUNCTION__, MEA_Service_Table[serviceId].data.SRV_mode ,data->SRV_mode);
        
        }


        /* Check that data->valid is set on */
        if (data->valid == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - set serviceId %d data valid indication to MEA_FALSE "
                              "is not allowed (Use MEA_API_Delete_Service)\n",
                              __FUNCTION__,serviceId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
        }

        if (MEA_Service_Table[serviceId].data.SRV_mode != data->SRV_mode){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - data->SRV_mode (%d) != Database (%d)  Not alow to change SRV_mode\n",
                __FUNCTION__, data->SRV_mode, MEA_Service_Table[serviceId].data.SRV_mode);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        /* check valid data->pmId  */
        if (data->pmId >= MEA_MAX_NUM_OF_PM_ID) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - data->pmId (%d) >= max allowed (%d) \n",
                              __FUNCTION__,data->pmId,MEA_MAX_NUM_OF_PM_ID);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
        }

        /* check valid data->pm_type  */
        switch (data->pm_type) {
        case MEA_PM_TYPE_ALL:
            break;
#if 0
        case MEA_PM_TYPE_PACKET_ONLY:
            if (!MEA_Platform_EnhancePmCounterSupport()) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - data->pm_type (%d) not support \n",
                                  __FUNCTION__,
                                  data->pm_type);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
             }
             break;
#endif        
        default:
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - unknown data->pm_type (%d) \n",
                               __FUNCTION__,
                               data->pm_type);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR; 
        }

        if (data->tmId >=  MEA_MAX_NUM_OF_TM_ID) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data->tmId (%d) >= max allowed (%d) \n",
                         __FUNCTION__,data->tmId,MEA_MAX_NUM_OF_TM_ID);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR; 
        }

        /* Check CFM_OAM_ME_Level_Threashold_valid support by the device */
        if ((globalMemoryMode != MEA_MODE_REGULAR_ENET3000) &&
            (data->CFM_OAM_ME_Level_Threshold_valid)  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - CFM_OAM_ME_Level_valid == 1 is not support by the device \n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
        }

        /* Forwarder enable is not allowed in upstream only */
        if ((globalMemoryMode == MEA_MODE_UPSTREEM_ONLY) &&
            (data->DSE_forwarding_enable               )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - DSE_forwarding_enable==TRUE not allowed in upstream \n",
                             __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
        }

        /* Learning enable is not allowed in downstream only */
        if ((globalMemoryMode == MEA_MODE_DOWNSTREEM_ONLY) &&
            (data->DSE_learning_enable                   )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - DSE_learning_enable==TRUE not allowed in downstream \n",
                             __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR; 
        }

        /* check data->DSE_forwarding_key_type */
        switch ((MEA_SE_Forwarding_key_type_te)(data->DSE_forwarding_key_type)) {
        case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
        case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
#ifdef PPP_MAC 
		case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
#else
		case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:

#endif
        case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN:
        case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :
        case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
        case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
        case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN :
        case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
            
           break;
        case MEA_SE_FORWARDING_KEY_TYPE_LAST:
        default:
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Invalid value DSE_forwarding_key_type (%d) \n",
                              __FUNCTION__,(int)(data->DSE_forwarding_key_type));
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
           return MEA_ERROR;
        }

        /* check data->DSE_learning_key_type */
        switch ((MEA_SE_Learning_key_type_te)(data->DSE_learning_key_type)) {
        case MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN :
        case MEA_SE_LEARNING_KEY_TYPE_MPLS_VPN:
        case MEA_SE_LEARNING_KEY_TYPE_DA_PLUS_PPPOE_SESSION_ID:
        case MEA_SE_LEARNING_KEY_TYPE_VLAN_VPN:
        case MEA_SE_LEARNING_KEY_TYPE_OAM_VPN:
        case MEA_SE_LEARNING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
        case MEA_SE_LEARNING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
        case MEA_SE_LEARNING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN    :
            break;
        case MEA_SE_LEARNING_KEY_TYPE_LAST:
        default:
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Invalid value DSE_learning_key_type (%d) \n",
                              __FUNCTION__,(int)(data->DSE_learning_key_type));
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
           return MEA_ERROR;
        }

        /* check learning Action*/
        if(data->DSE_learning_actionId_valid /*&& data->DSE_learning_enable == MEA_TRUE*/){
			if (MEA_API_IsExist_Action_ByType(unit_i, data->DSE_learning_actionId, MEA_ACTION_TYPE_FWD, &existActionId) != MEA_OK){
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,  
                                 "%s - MEA_API_IsExist_Action Failed (id=%d)\n",
                                 __FUNCTION__ ,
                                data->DSE_learning_actionId);
               MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
               return MEA_ERROR;
            }
            if (!existActionId){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,  
                                  "%s - DSE_learning_actionId %d not exist\n",
                                  __FUNCTION__ ,
                                  data->DSE_learning_actionId);
                 MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
               return MEA_ERROR;
            }
            if (globalMemoryMode == MEA_MODE_DOWNSTREEM_ONLY) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - learning action Id  is not allowed in downstream \n",
                                     __FUNCTION__);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
        }
        if((data->DSE_learning_srcPort_force == MEA_TRUE) && (data->DSE_learning_enable == MEA_FALSE)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
                              "%s - DSE_learning_srcPort (%d) need also learning enable\n",
                              __FUNCTION__,
                              data->DSE_learning_srcPort);
        }
        /* check learning srcPort*/
        if(data->DSE_learning_srcPort_force && 
              data->DSE_learning_enable == MEA_TRUE){
            if (!ENET_IsValid_Queue(unit_i,
                                    (ENET_QueueId_t)(data->DSE_learning_srcPort),
                                      MEA_TRUE)) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - DSE_learning_srcPort (%d) is not valid output cluster\n",
                                  __FUNCTION__,
                                     data->DSE_learning_srcPort);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }

        }
        if (data->stp == MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - data->stp  stp no support used the MSTP_Info.state \n",
                __FUNCTION__ );
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;

        }
        if(data->DSE_learning_srcPort_force == MEA_TRUE &&
            data->DSE_learning_Vp_force == MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - \r\nDSE_learning_srcPort_force == MEA_TRUE  and  DSE_learning_Vp_force Only one of them can be TRUE\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        if (data->DSE_learning_Vp_force == MEA_TRUE &&
            data->DSE_learning_actionId_valid == MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - \r\nDSE_learning_Vp_force TRUE work only with Learn Action \n",
                __FUNCTION__);
            return MEA_ERROR;

        }
        

        if(data->LxCp_enable == MEA_TRUE){
            if(MEA_API_IsExist_LxCP_ById(unit_i,
                                         data->LxCp_Id,&exist_lxcp)!=MEA_OK){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - MEA_API_IsExist_LxCP_ById \n",__FUNCTION__);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
              return MEA_ERROR;
        
            }
            if(!exist_lxcp){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - UnDefined Lxcp Id (%d) \n",
                        __FUNCTION__,
                        data->LxCp_Id);
              MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
              return MEA_ERROR;
            }

        }

        
            

    if (data->flowCoSMappingProfile_force) {

        if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowCoSMappingProfile_force is not support \n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (data->COS_FORCE  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowCoSMappingProfile_force (%d) is not allowed "
                              " together with COS_FORCE (%d)\n",
                              __FUNCTION__,
                              data->flowCoSMappingProfile_force,
                              data->COS_FORCE);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (MEA_API_Get_FlowCoSMappingProfile_Entry
                                    (unit_i,
                                    (MEA_FlowCoSMappingProfile_Id_t)(data->flowCoSMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Flow CoS Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              data->flowCoSMappingProfile_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }

    if (data->flowMarkingMappingProfile_force) {

        if (!MEA_FLOW_MARKING_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowMarkingMappingProfile_FORCE is not support \n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (data->L2_PRI_FORCE  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowMarkingMappingProfile_FORCE (%d) is not allowed "
                              " together with L2_PRI_FORCE (%d)\n",
                              __FUNCTION__,
                              data->flowMarkingMappingProfile_force,
                              data->L2_PRI_FORCE);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }


        if (MEA_API_Get_FlowMarkingMappingProfile_Entry
                                   (unit_i,
                                    (MEA_FlowMarkingMappingProfile_Id_t)(data->flowMarkingMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Flow Marking Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              data->flowMarkingMappingProfile_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }

    if (MEA_ACL5_SUPPORT) {
        if (data->Acl5_mask_prof != 0) {
            ///check 
            if (MEA_API_ACL5_Mask_Grouping_Profiles_isExist(MEA_UNIT_0, data->Acl5_mask_prof, MEA_TRUE, &exsit) != MEA_OK) {
                return MEA_ERROR;
            }
            if (exsit == MEA_FALSE) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  MEA_API_ACL5_Mask_Grouping_Profiles_isExist is not Exist\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



    /* Handle the learning Action id */
  
    if (data->DSE_learning_actionId_valid) {
        /* Add owner to the new Action Id  */
        if (MEA_API_AddOwner_Action(unit_i,
                                        data->DSE_learning_actionId)!=MEA_OK) {
            MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                                    "%s - MEA_API_AddOwner_Action failed (serviceId=%d, ActionId=%d) \n",
                                __FUNCTION__,
                                serviceId,
                                data->DSE_learning_actionId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }

    /* Delete owner to the Action Id 
        Note: In case no change in ActionId we simple add_owner and del_owner (I.E. no change) ,
                but important to do the Add before the delete */
    if (MEA_Service_Table[serviceId].data.DSE_learning_actionId_valid) {
        if (MEA_API_DelOwner_Action(unit_i,
                                        MEA_Service_Table[serviceId].data.DSE_learning_actionId)!=MEA_OK) {
            if (data->DSE_learning_actionId_valid) {
                MEA_API_DelOwner_Action(unit_i,data->DSE_learning_actionId);
            }
            MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                                    "%s - MEA_API_DelOwner_Action failed (serviceId=%d, ActionId=%d) \n",
                                __FUNCTION__,
                                serviceId,
                                MEA_Service_Table[serviceId].data.DSE_learning_actionId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        MEA_Service_Table[serviceId].data.DSE_learning_actionId_valid = MEA_FALSE;
        MEA_Service_Table[serviceId].data.DSE_learning_actionId       = 0;
    }
    


    /* handle filtering if needed */
    filter_mask_id = MEA_Service_Table[serviceId].filter_mask_id;
    {
        if(MEA_ACL_SUPPORT == MEA_FALSE && data->filter_enable == MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  ACL NOT SUPPORT you to allowed to set the filter_enable\n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        /* Check if need to Delete the old filter_mask_id */
        if (MEA_Service_Table[serviceId].data.filter_enable) {
            if ((!(data->filter_enable)) ||
                ((data->filter_enable                                                 ) &&
                (MEA_OS_memcmp(&(MEA_Service_Table[serviceId].data.ACL_filter_info),
                               &(data->ACL_filter_info),
                               sizeof(data->ACL_filter_info)) != 0))) {
                /* Delete the old filter mask id */
                filter_mask_id = MEA_Service_Table[serviceId].filter_mask_id;
                if ( mea_drv_filter_mask_prof_delete_owner(unit_i,filter_mask_id) != MEA_OK ) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - mea_drv_filter_mask_prof_delete_owner failed\n",
                                      __FUNCTION__);
                      MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                    return MEA_ERROR;  
                }

               

                filter_mask_id = 0;
                MEA_Service_Table[serviceId].filter_mask_id = filter_mask_id;
                MEA_Service_Table[serviceId].data.filter_enable = MEA_FALSE;

            }
        }

        /* Check if need to create new filter_mask_id */
        if (data->filter_enable) {
            if ((!(MEA_Service_Table[serviceId].data.filter_enable)) ||
                ((MEA_Service_Table[serviceId].data.filter_enable                     ) &&
                (MEA_OS_memcmp(&(MEA_Service_Table[serviceId].data.ACL_filter_info),
                &(data->ACL_filter_info),
                sizeof(data->ACL_filter_info)) != 0))) {
                /* Create new filter mask id */
                if (mea_drv_set_Filter_Mask_Prof(unit_i,
                    &(data->ACL_filter_info),
                                                     &filter_mask_id) != MEA_OK ) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - mea_drv_set_Filter_Mask_Prof failed\n",
                                      __FUNCTION__);
                      MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                    return MEA_ERROR;
               }
                /************************************************************************/
                /* profile FiterId                                                      */
                /************************************************************************/
                if (data->ACL_Prof_valid == MEA_TRUE) {
                    if (data->ACL_Prof >= MEA_ACL_SRV_PROFILE_END) {
                        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                        return MEA_ERROR;
                    }
                }


            }
        }
    }

    if (data->ACL_filter_info.filter_Whitelist_enable == MEA_TRUE && data->filter_enable == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"%s - filter_Whitelist_enable work only with filter enable\n",__FUNCTION__);
    }

    /* Handle LxCP */

        if (data->LxCp_enable == MEA_TRUE){
            if(mea_lxcp_drv_Add_owner(unit_i,data->LxCp_Id)!=MEA_OK){
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - mea_lxcp_drv_Add_owner (%d) failed\n",
                            __FUNCTION__,
                            data->LxCp_Id);
                    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                  return MEA_ERROR; 
             }
        }

        if(MEA_Service_Table[serviceId].data.LxCp_enable == MEA_TRUE){ 
            if(mea_lxcp_drv_delete_owner(unit_i,MEA_Service_Table[serviceId].data.LxCp_Id)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_lxcp_drv_delete_owner (%d) failed\n",
                                  __FUNCTION__,MEA_Service_Table[serviceId].data.LxCp_Id);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
            MEA_Service_Table[serviceId].data.LxCp_enable = MEA_FALSE;
            MEA_Service_Table[serviceId].data.LxCp_Id = 0;
        }


    if (MEA_VPLS_SUPPORT)
    {
        MEA_Bool vpls_exist = MEA_FALSE;
        MEA_OutPorts_Entry_dbt     vpls_OutPorts;

        if (data->en_vpls_Ins == MEA_TRUE){
            if (OutPorts_Entry == NULL || EHP_Entry == NULL){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    " those parmeter suould not set it NULL  (OutPorts_Entry == NULL OR EHP_Entry == NULL) \n");
                return MEA_ERROR;
            }


            if (MEA_API_VPLSi_isExist(unit_i, (MEA_Uint16)data->vpls_Ins_Id, &vpls_exist) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    " VPLSi must creat before the Dl Service \n");
                return MEA_ERROR;
            }
            if (vpls_exist == MEA_TRUE) {
                data->COS_FORCE = MEA_TRUE;
                data->COS = 7;

                if (mea_drv_vplsi_Get_outport((MEA_Uint16)data->vpls_Ins_Id, &vpls_OutPorts) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        " mea_drv_vplsi_Get_outport fail \n");
                    return MEA_ERROR;
                }

                    if (EHP_Entry) {
                        MEA_OS_memcpy(OutPorts_Entry, &vpls_OutPorts, sizeof(*OutPorts_Entry));
                        data->editId = 0;
                        data->flood_ED = MEA_TRUE;
                        EHP_Entry->ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_OFSET12;
                        EHP_Entry->ehp_info[0].ehp_data.eth_info.val.all = (MEA_Uint32)(0x000001ff + ((MEA_Uint32)(data->vpls_Ins_Id << MEA_VPLS_ED_ID_SHIFT)));
                    }
                    else {

                    }
               

            }//vpls_exist
        
            
        }
    }






    /* handle the xPermission */
    if ((OutPorts_Entry != NULL) && (
            (MEA_OS_memcmp((char*)OutPorts_Entry,
            (char*)(&(MEA_Service_Table[serviceId].out_ports)),
            sizeof(*OutPorts_Entry)) != 0) || 
            (data->MSTP_Info.state != MEA_Service_Table[serviceId].data.MSTP_Info.state))
        ) {

        if (OutPorts_Entry != NULL) {
            if (ENET_queue_check_OutPorts(unit_i, OutPorts_Entry) != ENET_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_queue_check_OutPorts Fail \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }


        MEA_OS_memset(&xPermission, 0, sizeof(xPermission));
        MEA_OS_memcpy((char*)(&xPermission), (char*)OutPorts_Entry, sizeof(xPermission));



  

        if (!MEA_API_Get_Warm_Restart())
            xPermissionId = ENET_PLAT_GENERATE_NEW_ID;
        else
            xPermissionId = data->save_hw_xPermision;

        if (key.src_port == 127){
            isCpu = MEA_TRUE;
        }
        else {
            isCpu = MEA_FALSE;
        }

        if (data->MSTP_Info.state == MEA_MSTP_TYPE_STATE_BLOCKING_SRV ||
            data->MSTP_Info.state == MEA_MSTP_TYPE_STATE_BLOCKING_SRV_VPN0)
            MEA_OS_memset((char*)(&xPermission), 0, sizeof(xPermission));
        

        if (enet_Create_xPermission(unit_i,
            &xPermission,
            isCpu,
            &xPermissionId) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Create_xPermission failed\n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        data->save_hw_xPermision = xPermissionId;

        if (MEA_Service_Table[serviceId].xPermissionId != ENET_PLAT_GENERATE_NEW_ID) {
            if (enet_Delete_xPermission(unit_i,
                MEA_Service_Table[serviceId].xPermissionId) != MEA_OK) {
                enet_Delete_xPermission(unit_i, xPermissionId);
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - enet_Delete_xPermission failed\n",
                    __FUNCTION__);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
            MEA_Service_Table[serviceId].xPermissionId = ENET_PLAT_GENERATE_NEW_ID;
        }
    }
    else {
        xPermissionId = MEA_Service_Table[serviceId].xPermissionId;
    }
    /************************************************************************/
    /*Start Learn OutPort                                                   */
    /************************************************************************/
    /* handle the xPermission */
    MEA_OS_memset(&LearnxPermission, 0, sizeof(LearnxPermission));
    if ((data->DSE_learning_Vp_force == MEA_TRUE) && data->DSE_learning_actionId_valid) {

        if (mea_Action_GetOutport(unit_i, data->DSE_learning_actionId, &LearnxPermission) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "%s - mea_Action_GetOutport fail \n", __FUNCTION__);
            return MEA_ERROR;
        }


    }

    if ((MEA_OS_memcmp(&LearnxPermission,
        (&(MEA_Service_Table[serviceId].Learn_OutPorts)),
        sizeof(MEA_OutPorts_Entry_dbt)) != 0)) {
        if (ENET_queue_check_OutPorts(unit_i, &LearnxPermission) != ENET_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_queue_check_OutPorts LearnxPermission Fail \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        if (!MEA_API_Get_Warm_Restart())
            LearnxPermissionId = ENET_PLAT_GENERATE_NEW_ID;
        else
            LearnxPermissionId = data->save_hw_LearnxPermision;


        if (key.src_port == 127){
            isCpu = MEA_TRUE;
        }
        else {
            isCpu = MEA_FALSE;
        }
        if (enet_Create_xPermission(unit_i,
            &LearnxPermission,
            isCpu,
            &LearnxPermissionId) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Create_xPermission LearnxPermissionId failed\n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        data->save_hw_LearnxPermision = LearnxPermissionId;

        if (MEA_Service_Table[serviceId].LearnxPermissionId != ENET_PLAT_GENERATE_NEW_ID) {
            if (enet_Delete_xPermission(unit_i,
                MEA_Service_Table[serviceId].LearnxPermissionId) != MEA_OK) {
                enet_Delete_xPermission(unit_i, LearnxPermissionId);
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - enet_Delete_xPermission LearnxPermissionId failed\n",
                    __FUNCTION__);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
            MEA_Service_Table[serviceId].LearnxPermissionId = ENET_PLAT_GENERATE_NEW_ID;
        }
    }
    else {
        LearnxPermissionId = MEA_Service_Table[serviceId].LearnxPermissionId;
    }




    /************************************************************************/
    /*End Learn OutPort                                                   */
    /************************************************************************/


    /* Update the Action */
    

    if(data->CM == 1) {
        if(data->COLOR_FORSE == MEA_ACTION_FORCE_COMMAND_DISABLE){
            data->COLOR_FORSE= MEA_ACTION_FORCE_COMMAND_DISABLE;
            data->COLOR =  MEA_POLICER_COLOR_VAL_YELLOW;
        } 
        
    } else {
        if(data->COLOR_FORSE == MEA_ACTION_FORCE_COMMAND_DISABLE){
            data->COLOR_FORSE= MEA_ACTION_FORCE_COMMAND_VALUE;
            data->COLOR= MEA_POLICER_COLOR_VAL_GREEN;   
        } 
    }

    MEA_OS_memset((char*)&Action_Data,0,sizeof(Action_Data));
    Action_Data.flowMarkingMappingProfile_force = (MEA_Uint16)data->flowMarkingMappingProfile_force;
    Action_Data.flowMarkingMappingProfile_id    = (MEA_Uint16)data->flowMarkingMappingProfile_id;
    Action_Data.flowCoSMappingProfile_force     = (MEA_Uint16)data->flowCoSMappingProfile_force;
    Action_Data.flowCoSMappingProfile_id        = (MEA_Uint16)data->flowCoSMappingProfile_id;
    Action_Data.force_l2_pri_valid = data->L2_PRI_FORCE;
    Action_Data.L2_PRI             = data->L2_PRI;
    Action_Data.force_cos_valid    = data->COS_FORCE;
    Action_Data.COS                = data->COS;
    Action_Data.force_color_valid  = data->COLOR_FORSE;
    Action_Data.COLOR              = data->COLOR;
    Action_Data.output_ports_valid = MEA_TRUE;
    Action_Data.proto_llc_valid    = MEA_TRUE;
    Action_Data.protocol_llc_force = data->protocol_llc_force;
    Action_Data.Llc                = data->Llc;
    Action_Data.Protocol           = data->Protocol;
    Action_Data.ed_id              = data->editId;
    Action_Data.ed_id_valid        = MEA_TRUE;
    Action_Data.pm_id              = data->pmId;
    Action_Data.pm_id_valid        = MEA_TRUE;
    Action_Data.pm_type            = data->pm_type;
    Action_Data.tm_id              = data->tmId;
    Action_Data.tm_id_valid        = MEA_TRUE;
    Action_Data.tmId_disable      = data->tmId_disable;
    Action_Data.fragment_enable   = (MEA_Uint16)data->fragment_enable;
    Action_Data.fwd_ingress_TS_type = data->ingress_TS_type;
#ifdef MEA_TDM_SW_SUPPORT         
    Action_Data.tdm_packet_type = data->tdm_packet_type;
    Action_Data.tdm_remove_byte = data->tdm_remove_byte;
    Action_Data.tdm_remove_line = data->tdm_remove_line;
    Action_Data.tdm_ces_type    = data->tdm_ces_type;   
    Action_Data.tdm_used_vlan   = data->tdm_used_vlan ; 
    Action_Data.tdm_used_rtp    = data->tdm_used_rtp;
#endif        
	Action_Data.afdx_enable        = data->afdx_enable;
	Action_Data.afdx_Rx_sessionId  = data->afdx_Rx_sessionId;
	Action_Data.afdx_A_B           = data->afdx_A_B;
	Action_Data.afdx_Bag_info      = data->afdx_Bag_info;
    Action_Data.set_1588           = data->set_1588;
    Action_Data.mtu_Id             = data->mtu_Id;
    Action_Data.lag_bypass         = data->lag_bypass;
    Action_Data.flood_ED           = data->flood_ED;
    Action_Data.Drop_en            = data->Drop_en;
        

    Action_Data.overRule_vpValid   = data->overRule_vpValid;
    Action_Data.overRule_vp_val    = data->overRule_vp_val;

    Action_Data.Fragment_IP         = data->Fragment_IP;
    Action_Data.IP_TUNNEL           = data->IP_TUNNEL;
    Action_Data.Defrag_ext_IP       = data->Defrag_ext_IP;
    Action_Data.Defrag_int_IP       = data->Defrag_int_IP;
    Action_Data.Defrag_Reassembly_L2_profile = data->Defrag_Reassembly_L2_profile;
    Action_Data.fragment_ext_int = data->fragment_ext_int;
    Action_Data.fragment_target_mtu_prof = data->fragment_target_mtu_prof;
    Action_Data.fwd_win                  = data->fwd_win;
    Action_Data.sflow_Type               = data->sflow_Type;
    Action_Data.overRule_bfd_sp          = data->overRule_bfd_sp;

	if (data->SRV_mode == MEA_SRV_MODE_EXTERNAL){
		Action_Data.Action_type = MEA_ACTION_TYPE_SERVICE_EXTERNAL;
	}
	else{
		Action_Data.Action_type = MEA_ACTION_TYPE_SERVICE;
	}

	Action_Data.policer_prof_id=data->policer_prof_id;
	Action_Data.policer_prof_id_valid=MEA_TRUE;


    Action_Data.wred_profId     = data->wred_profId;
    //check if the wred profile exist must before the create Action.
    {
        MEA_WRED_Profile_Key_dbt  key_i;
        key_i.acm_mode =0 ;
        key_i.profile_id = data->wred_profId ;


        if (MEA_API_IsValid_WRED_Profile   (unit_i,&key_i,MEA_TRUE)!=MEA_TRUE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsValid_WRED_Profile failed  wred profile %d not valid\n",
                __FUNCTION__,data->wred_profId);
            return MEA_ERROR;
        }
    }


    if (!data->protocol_llc_force) {
        MEA_Uint32 protocol;
        MEA_Bool   llc;
            if ((key.virtual_port == MEA_FALSE) &&
                mea_Calc_ProtocolAndLlc(unit_i,
                                    (MEA_Port_t)(MEA_Service_Table[serviceId].key.src_port),
                                        OutPorts_Entry,
                                    &protocol,
                                        &llc ) != MEA_OK ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - mea_Calc_ProtocolAndLlc failed \n",
                                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        Action_Data.proto_llc_valid    = MEA_TRUE;
        Action_Data.protocol_llc_force = MEA_TRUE;
        Action_Data.Protocol           = protocol;
        Action_Data.Llc                = llc;
    }
    if (MEA_Service_Table[serviceId].en_Sid_Internal == MEA_TRUE){
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_SERVICE);
		Action_Data.Action_type = MEA_ACTION_TYPE_SERVICE;


        Action_Id = (MEA_Action_t)MEA_Service_Table[serviceId].Sid_Internal;
    }
    else{
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_SERVICE_EXTERNAL);
        Action_Id = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX + MEA_Service_Table[serviceId].Sid_External;
		Action_Data.Action_type = MEA_ACTION_TYPE_SERVICE;

    }


    if (EHP_Entry){
        if (EHP_Entry->ehp_info[0].ehp_data.Attribute_info.valid == MEA_FALSE && EHP_Entry->ehp_info[0].ehp_data.EditingType == 0){
            if (Action_Data.protocol_llc_force == MEA_TRUE){
                EHP_Entry->ehp_info[0].ehp_data.Attribute_info.protocol = Action_Data.Protocol;
                EHP_Entry->ehp_info[0].ehp_data.Attribute_info.llc = Action_Data.Llc;
                EHP_Entry->ehp_info[0].ehp_data.Attribute_info.valid = MEA_TRUE;
            }
        }
    }


        
    if (MEA_API_Set_Action(unit_i,
                            Action_Id,
                            &Action_Data,
                            OutPorts_Entry,
                            Policer_Entry,
                            EHP_Entry) != MEA_OK) {
        // mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - MEA_API_Set_Action failed (serviceId=%d)\n",
                            __FUNCTION__,serviceId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		// mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
        return MEA_ERROR;
    }
    //mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
    



    if (data->FWD_DaMAC_valid == MEA_TRUE  )
    {
        MEA_Uint16 id_FWD_enb = 0;

        if (mea_drv_Create_FWD_ENB_DA_Prof(unit_i, &id_FWD_enb, data->FWD_DaMAC_value) != MEA_OK)
        {
            return MEA_ERROR;
        }
        data->FWD_DaMAC_ID = id_FWD_enb;
    } else{
        if (data->FWD_DaMAC_valid == MEA_FALSE && data->FWD_DaMAC_ID != 0)
            data->FWD_DaMAC_ID = 0;
    }



    /* Build the new entry */
    MEA_OS_memcpy((char*)(&entry),
                  (char*)(&(MEA_Service_Table[serviceId])),
                  sizeof(entry));

	MEA_OS_memcpy((char*)(&(entry.data)),
		(char*)data,
		sizeof(entry.data));
        entry.data.editId  = Action_Data.ed_id;
        entry.data.pmId    = Action_Data.pm_id;
        entry.data.tmId    = Action_Data.tm_id;
		entry.data.policer_prof_id    = Action_Data.policer_prof_id;
        /* update protocol llc */
        entry.data.protocol_llc_force = Action_Data.protocol_llc_force;
        entry.data.Protocol           = Action_Data.Protocol;
        entry.data.Llc                = Action_Data.Llc;

    
    if (OutPorts_Entry != NULL) {
        MEA_OS_memcpy((char*)(&(entry.out_ports)),
                      OutPorts_Entry,
                      sizeof(entry.out_ports));
    }

    MEA_OS_memcpy((char*)(&(entry.Learn_OutPorts)),
        &LearnxPermission,
        sizeof(entry.Learn_OutPorts));

    entry.xPermissionId = xPermissionId;
    entry.filter_mask_id = filter_mask_id;
    entry.LearnxPermissionId = LearnxPermissionId;
    if (entry.data.SRV_mode == MEA_SRV_MODE_REGULAR){
        /* Update the hardware */
        if (mea_Service_UpdateHw(unit_i,
            serviceId,
            &entry,
            &(MEA_Service_Table[serviceId]),
            MEA_SETTING_TYPE_UPDATE) != MEA_OK) {
            
            
            if (data->FWD_DaMAC_valid == MEA_TRUE)
            {
                mea_drv_Delete_FWD_ENB_DA_Prof(unit_i, data->FWD_DaMAC_ID);
            }



            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_Service_UpdateHw failed \n", __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }

    if(mea_service_srcport_lean(unit_i,
        serviceId,
        &entry,
        &(MEA_Service_Table[serviceId]),
        MEA_SETTING_TYPE_UPDATE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_Service_UpdateHw failed \n", __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    

    if (MEA_Service_Table[serviceId].data.FWD_DaMAC_valid == MEA_TRUE){
        mea_drv_Delete_FWD_ENB_DA_Prof(unit_i, MEA_Service_Table[serviceId].data.FWD_DaMAC_ID);
    }


    /* Update the Software shadow */
    MEA_OS_memcpy(&(MEA_Service_Table[serviceId]),
                  &entry,
                  sizeof(MEA_Service_Table[0]));
       
        

    /* Update output variable */
    
    data->editId        = Action_Data.ed_id;
    data->pmId          = Action_Data.pm_id;
    data->tmId          = Action_Data.tm_id;
	entry.data.policer_prof_id    = Action_Data.policer_prof_id;
    
    if (data->SRV_mode == MEA_SRV_MODE_EXTERNAL){

        MEA_service_setting_SRV_ON_External_DDR(unit_i, serviceId, MEA_SETTING_TYPE_UPDATE);

    }
    if (MEA_VPLS_SUPPORT)
    {
        if ((MEA_Service_Table[serviceId].data.en_vpls_Ins == MEA_TRUE) &&
            (data->en_vpls_Ins == MEA_FALSE)) 
        {
            //remove service from VPLS
            MEA_drv_VPLS_Service(data->vpls_Ins_Id, 0, MEA_Service_Table[serviceId].key.src_port);
        }
        if((MEA_Service_Table[serviceId].data.en_vpls_Ins == MEA_FALSE) &&
           (data->en_vpls_Ins == MEA_TRUE)
          ){
                MEA_drv_VPLS_Service(data->vpls_Ins_Id, serviceId, MEA_Service_Table[serviceId].key.src_port);
           }
        
    }

    /* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}


MEA_Status  MEA_API_Get_Service_InfoDb(MEA_Unit_t               unit_i,
                                       MEA_Service_t            serviceId,
                                       MEA_Service_Info_dbt      *entry)
{


    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (entry == NULL){
            return MEA_ERROR;
        }


        /* Check for valid key->src_port parmeter */
    if (MEA_API_IsServiceIdInRange(unit_i, serviceId) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Invalid serviceId %d \n",
            __FUNCTION__, serviceId);
        return MEA_ERROR;
    }



#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    entry->SRV_mode = MEA_Service_Table[serviceId].data.SRV_mode;
    
    entry->eb_Sid_Internal = MEA_Service_Table[serviceId].en_Sid_Internal;
    entry->Sid_Internal = MEA_Service_Table[serviceId].Sid_Internal;

    entry->eb_Sid_External = MEA_Service_Table[serviceId].en_Sid_External;
    entry->Sid_External = MEA_Service_Table[serviceId].Sid_External;
    entry->hashAdress = MEA_Service_Table[serviceId].hashaddress_External;

   


    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Service>                                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_API_Get_Service    (MEA_Unit_t                      unit_i,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Key_dbt      *key,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Policer_Entry_dbt          *Policer_Entry,
                                    MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry)

{
    MEA_Action_t Action_Id;
    mea_memory_mode_e save_globalMemoryMode;
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool exist;
#endif

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key->src_port parmeter */
    if (MEA_API_IsExist_Service_ById(unit_i,
                                     serviceId,
                                     &exist) != MEA_OK)  {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -MEA_API_IsExist_Service_ById %d failed \n",
                         __FUNCTION__,serviceId);
       return MEA_ERROR; 
    }
    if (exist == MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - serviceId %d not exist \n",
                         __FUNCTION__,serviceId);
       return MEA_ERROR; 
    }



#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Set the globalMemoryMode */
    MEA_DRV_SET_GLOBAL_MEMORY_MODE
        (save_globalMemoryMode,
           mea_Service_GetHwUnit(unit_i,
                               &(MEA_Service_Table[serviceId].key)));


    /* Get the key of this serviceId */
    if (key != NULL) {
        MEA_OS_memcpy(key,&(MEA_Service_Table[serviceId].key),sizeof(*key));
    }

    /* Get the data of this serviceId */
    if (data != NULL) {
        MEA_OS_memcpy(data,&(MEA_Service_Table[serviceId].data),sizeof(*data));
    }


    /* Get the outports table - if supply */
    if (OutPorts_Entry) {    
        MEA_OS_memcpy(OutPorts_Entry,
                      &(MEA_Service_Table[serviceId].out_ports),
                      sizeof(*OutPorts_Entry));
    }


   

    if (MEA_Service_Table[serviceId].en_Sid_Internal == MEA_TRUE){
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_SERVICE);


        Action_Id = (MEA_Action_t)MEA_Service_Table[serviceId].Sid_Internal;
    }
    else{
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_SERVICE_EXTERNAL);
        Action_Id = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX + MEA_Service_Table[serviceId].Sid_External;


    }



    /* Get Policer / Editing from the Action */
    if ((Policer_Entry != NULL) ||
        (EHP_Entry     != NULL)  ) {
       
        if (MEA_API_Get_Action(unit_i,
            (MEA_Action_t)Action_Id,
                               NULL, /* data */
                               NULL, /* OutPorts */
                               Policer_Entry,
                               EHP_Entry) != MEA_OK) {
            //mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_Action failed (serviceId=%d)\n",
                              __FUNCTION__,
                              serviceId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        //mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
    }



   
    /* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;


}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Delete_Service>                                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_API_Delete_Service(MEA_Unit_t       unit_i,
	MEA_Service_t    serviceId_i){

	MEA_Bool force= MEA_FALSE;

	return MEA_DRV_Delete_Service(unit_i, serviceId_i, force);

}



MEA_Status  MEA_DRV_Delete_Service(MEA_Unit_t       unit_i, MEA_Service_t    serviceId_i,MEA_Bool force)
{
    MEA_Action_t Action_Id;
    MEA_Port_t outport;
    mea_memory_mode_e save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit;
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool exist;
#endif

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        /* Verify that this is not Reserved CPU ServiceId */
        if (serviceId_i == MEA_CPU_SERVICE_ID) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -You are not allowed to delete Reserved CPU ServiceId %d \n",
                __FUNCTION__, serviceId_i);
            return MEA_ERROR;
        }

    /* Check for valid key->src_port parameter */
    if (MEA_API_IsExist_Service_ById(unit_i,
        serviceId_i,
        &exist) != MEA_OK)  {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -MEA_API_IsExist_Service_ById %d failed \n",
            __FUNCTION__, serviceId_i);
        return MEA_ERROR;
    }
    if (exist == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - serviceId %d not exist \n",
            __FUNCTION__, serviceId_i);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Set the globalMemoryMode */
    MEA_DRV_SET_GLOBAL_MEMORY_MODE
        (save_globalMemoryMode,
        mea_Service_GetHwUnit(unit_i,
        &(MEA_Service_Table[serviceId_i].key)));


    /* mark this service number not valid in DB */
	if (MEA_Service_Table[serviceId_i].data.no_allow_del == MEA_TRUE){ 
		if (force == MEA_FALSE){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "You are no_allow_del This ServiceId %d \n",  serviceId_i);
			return MEA_OK;
		}
	}


    MEA_Service_Table[serviceId_i].data.valid = MEA_FALSE;

    if (MEA_Service_Table[serviceId_i].data.SRV_mode == MEA_SRV_MODE_EXTERNAL){

        MEA_service_setting_SRV_ON_External_DDR(unit_i, serviceId_i, MEA_SETTING_TYPE_DELETE);

    }


    /* delete from Hardware */
    if (MEA_Service_Table[serviceId_i].data.SRV_mode == MEA_SRV_MODE_REGULAR){
        if (mea_Service_UpdateHw(unit_i,
            serviceId_i,
            &(MEA_Service_Table[serviceId_i]),
            NULL /* old_entry */,
            MEA_SETTING_TYPE_DELETE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_Service_UpdateHw failed (serviceId=%d) \n",
                __FUNCTION__, serviceId_i);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }


    //First we need to invalidate the service at 
    //egress header processor ports if exists


    for (outport = 0; outport <= MEA_MAX_PORT_NUMBER; outport++) {
        if (ENET_IsValid_Queue(unit_i, outport, ENET_TRUE) == ENET_FALSE) {
            continue;
        }

        if (MEA_API_IsOutPortOf_Service(unit_i, serviceId_i, outport) == MEA_FALSE){
            continue;
        }


    }

    if (/*(MEA_Service_Table[serviceId_i].data.DSE_learning_enable) && */
        (MEA_Service_Table[serviceId_i].data.DSE_learning_actionId_valid)) {
        MEA_Bool exist_actionId = MEA_FALSE;
        MEA_API_IsExist_Action(unit_i, MEA_Service_Table[serviceId_i].data.DSE_learning_actionId, &exist_actionId);
            if (exist_actionId) {
                if (MEA_API_DelOwner_Action(unit_i,
                    MEA_Service_Table[serviceId_i].data.DSE_learning_actionId) != MEA_OK)
                {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_DelOwner_Action failed (serviceId=%d, ActionId=%d) \n",
                        __FUNCTION__,
                        serviceId_i,
                        MEA_Service_Table[serviceId_i].data.DSE_learning_actionId);
                    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                    return MEA_ERROR;
                }
            }
            else {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
                    "%s - LearnAction not Exist (serviceId=%d, ActionId=%d) \n",__FUNCTION__,
                    serviceId_i,MEA_Service_Table[serviceId_i].data.DSE_learning_actionId);
            }
    }


    // delete Lxcp
    if (MEA_Service_Table[serviceId_i].data.LxCp_enable == MEA_TRUE){
        if (mea_lxcp_drv_delete_owner(unit_i, MEA_Service_Table[serviceId_i].data.LxCp_Id) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_lxcp_drv_delete_owner (serviceId=%d, LxCp_Id=%d) Failed\n",
                __FUNCTION__, serviceId_i, MEA_Service_Table[serviceId_i].data.LxCp_Id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }

    /* Delete the filter mask id */
    if (MEA_Service_Table[serviceId_i].data.filter_enable)
    {
        if (mea_drv_filter_mask_prof_delete_owner(unit_i, MEA_Service_Table[serviceId_i].filter_mask_id) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_filter_mask_prof_delete_owner failed\n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        
    
    }

    if (MEA_VPLS_SUPPORT)
    {
        if (MEA_Service_Table[serviceId_i].data.en_vpls_Ins == MEA_TRUE)
        {
            //remove service from VPLS
            MEA_drv_VPLS_Service(MEA_Service_Table[serviceId_i].data.vpls_Ins_Id, 0, MEA_Service_Table[serviceId_i].key.src_port);
        }
    }


    /* Delete the xPermissionId */
    if (MEA_Service_Table[serviceId_i].xPermissionId != ENET_PLAT_GENERATE_NEW_ID) {
        if (enet_Delete_xPermission(unit_i,
            MEA_Service_Table[serviceId_i].xPermissionId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Delete_xPermission failed\n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        MEA_Service_Table[serviceId_i].xPermissionId = ENET_PLAT_GENERATE_NEW_ID;
    }

    if (MEA_Service_Table[serviceId_i].LearnxPermissionId != ENET_PLAT_GENERATE_NEW_ID) {
        if (enet_Delete_xPermission(unit_i,
            MEA_Service_Table[serviceId_i].LearnxPermissionId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Delete_xPermission LearnxPermissionId failed\n",
                __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        MEA_Service_Table[serviceId_i].LearnxPermissionId = ENET_PLAT_GENERATE_NEW_ID;
    }

    if (MEA_Service_Table[serviceId_i].en_Sid_Internal == MEA_TRUE){
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_SERVICE);


        Action_Id = (MEA_Action_t)MEA_Service_Table[serviceId_i].Sid_Internal;
    }
    else{
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_SERVICE_EXTERNAL);
        Action_Id = MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX + MEA_Service_Table[serviceId_i].Sid_External;


    }


    /* Delete the action of the Service */
	if (MEA_DRV_Delete_Action(unit_i, Action_Id, ((MEA_Service_Table[serviceId_i].data.SRV_mode == MEA_SRV_MODE_REGULAR) ? (MEA_ACTION_TYPE_SERVICE) : (MEA_ACTION_TYPE_SERVICE_EXTERNAL)) )  != MEA_OK ) 
	{
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Delete_Action failed (serviceId=%d)\n",
            __FUNCTION__, serviceId_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);

    if(MEA_Service_Table[serviceId_i].data.FWD_DaMAC_valid == MEA_TRUE){
        mea_drv_Delete_FWD_ENB_DA_Prof(unit_i, MEA_Service_Table[serviceId_i].data.FWD_DaMAC_ID);
        MEA_Service_Table[serviceId_i].data.FWD_DaMAC_valid = MEA_FALSE;
        
        MEA_OS_memset(&MEA_Service_Table[serviceId_i].data.FWD_DaMAC_value, 0, sizeof(MEA_MacAddr));
        MEA_Service_Table[serviceId_i].data.FWD_DaMAC_ID = 0;
    }



#if 1   
    if (MEA_Service_Table[serviceId_i].data.SRV_mode == MEA_SRV_MODE_REGULAR)
    {


        MEA_db_HwUnit_t           hwUnit;
        MEA_db_HwId_t          hwId;
        /* Get the hwUnit and hwId */
        MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
            mea_drv_Get_Service_Internal_db,
            MEA_Service_Table[serviceId_i].Sid_Internal,
            hwUnit,
            hwId,
            return MEA_ERROR);
#ifdef MEA_TDM_SW_SUPPORT 
        if (!MEA_Service_Table[serviceId_i].data.tdm_packet_type == 1){
#endif
            /* Update ServiceEntry */
            if (mea_ServiceEntry_UpdateHw(unit_i,
                serviceId_i,
                hwUnit,
                hwId,
                &MEA_Service_Table[serviceId_i],
                NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_ServiceEntry_UpdateHw failed \n", __FUNCTION__);
                return MEA_ERROR;
            }
#ifdef MEA_TDM_SW_SUPPORT 
    }
#endif

    }
#endif

    //Delete the service from the valid service linked list
    if (mea_Service_Delete(serviceId_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_Service_Delete failed (serviceId=%d) \n",
            __FUNCTION__, serviceId_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }



    /* set that all outports is zero */
    MEA_OS_memset(&(MEA_Service_Table[serviceId_i].out_ports),
        0,
        sizeof(MEA_Service_Table[0].out_ports));

    MEA_OS_memset(&(MEA_Service_Table[serviceId_i].Learn_OutPorts),
        0,
        sizeof(MEA_Service_Table[0].Learn_OutPorts));



    if (MEA_Service_Table[serviceId_i].data.SRV_mode == MEA_SRV_MODE_REGULAR){
        if (MEA_Service_Table[serviceId_i].service_Type == MEA_SERVICE_CLASSIFCATION_RANGE) {
            MEA_ServiceRange_Table[MEA_Service_Table[serviceId_i].rangeId].valid = MEA_FALSE;
            if (mea_drv_serviceRange_UpdateHw(unit_i,
                serviceId_i,
                MEA_Service_Table[serviceId_i].rangeId,
                &(MEA_ServiceRange_Table[MEA_Service_Table[serviceId_i].rangeId]),
                NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_ServiceRange_UpdateHw failed \n", __FUNCTION__);
                MEA_ServiceRange_Table[MEA_Service_Table[serviceId_i].rangeId].valid = MEA_TRUE;
                return MEA_ERROR;
            }
            MEA_ServiceRange_Table[MEA_Service_Table[serviceId_i].rangeId].valid = MEA_TRUE;
        }

        /* Delete the serviceRange */
        if (MEA_Service_Table[serviceId_i].service_Type == MEA_SERVICE_CLASSIFCATION_RANGE){ /* delete service range*/
            mea_drv_serviceRange_Delete(MEA_Service_Table[serviceId_i].rangeId);
        }

        /* Get Current HwUnit */
        MEA_DRV_GET_HW_UNIT(unit_i,
            hwUnit,
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR);

        /* Delete the HwId */
        if (mea_db_Delete(unit_i,
            MEA_Service_Internal_db,
            (MEA_db_SwId_t)MEA_Service_Table[serviceId_i].Sid_Internal,
            hwUnit) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Delete failed (serviceId=%d,hwUnit=$d)\n",
                __FUNCTION__, serviceId_i, hwUnit);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        mea_db_Delete(unit_i, MEA_Service_Internal_db_getHW, (MEA_db_SwId_t)MEA_Service_Table[serviceId_i].Sid_Internal, hwUnit);
    }
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/




    MEA_Service_rollback(unit_i, serviceId_i);


   MEA_OS_memset(&(MEA_Service_Table[serviceId_i]),0,sizeof(MEA_Service_Table[0]));
   MEA_Service_Table[serviceId_i].xPermissionId      = ENET_PLAT_GENERATE_NEW_ID;
   MEA_Service_Table[serviceId_i].LearnxPermissionId = ENET_PLAT_GENERATE_NEW_ID;



    

    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;

}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_GetFirst_Service>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_Service(MEA_Unit_t     unit,
                                    MEA_Service_t *o_serviceId,
                                    MEA_ULong_t    *o_cookie)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (o_serviceId == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - o_serviceId == NULL\n",
                          __FUNCTION__);
       return MEA_ERROR; 
    }

    if (o_cookie == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - o_cookie == NULL\n",
                          __FUNCTION__);
       return MEA_ERROR; 
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    *o_cookie     = (long)p_service_head;

    if (p_service_head == NULL) {
       *o_serviceId    = MEA_MAX_NUM_OF_SERVICES;
    } else {
       *o_serviceId = p_service_head->serviceId;
    }

    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_GetNext_Service>                                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_Service(MEA_Unit_t     unit,
                                   MEA_Service_t *o_serviceId,
                                   MEA_ULong_t    *io_cookie)
{

    mea_service_node_t* p_mea_service_node;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (o_serviceId == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - o_serviceId == NULL\n",
                          __FUNCTION__);
       return MEA_ERROR; 
    }

    if (io_cookie == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - io_cookie == NULL\n",
                          __FUNCTION__);
       return MEA_ERROR; 
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    p_mea_service_node = (mea_service_node_t*)(*io_cookie);

    if (p_mea_service_node == NULL) {
       *o_serviceId  = MEA_MAX_NUM_OF_SERVICES;
    } else {
       p_mea_service_node = p_mea_service_node->next;
       if (p_mea_service_node == NULL) {
          *o_serviceId  = MEA_MAX_NUM_OF_SERVICES;
       } else {
          *o_serviceId  = p_mea_service_node->serviceId;
       }
    }
    *io_cookie = (MEA_ULong_t)p_mea_service_node;
    
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_IsExist_Service_ById>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_Service_ById(MEA_Unit_t     unit_i,
                                        MEA_Service_t  serviceId_i, 
                                        MEA_Bool      *exist_o) 
{
    mea_service_node_t* p_mea_service_node;

    MEA_API_LOG

    if (MEA_Service_Table == NULL) {
        if (exist_o) {
            *exist_o=MEA_FALSE;
        }
        return MEA_OK;
    }
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid serviceId parameter */
    if (MEA_API_IsServiceIdInRange(unit_i,serviceId_i) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid serviceId %d \n",
                          __FUNCTION__,serviceId_i);
        return MEA_ERROR;  
    }
 
    /* Check for valid exist parameter */
    if (exist_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    *exist_o = MEA_FALSE;


    for (p_mea_service_node = p_service_head; 
         p_mea_service_node != NULL; 
         p_mea_service_node = p_mea_service_node->next) {
      
        if (p_mea_service_node->serviceId == serviceId_i) {
            if (MEA_Service_Table[serviceId_i].data.valid) {

                *exist_o = MEA_TRUE;
                break;
            }
        }
    }

    if (*exist_o != (MEA_Bool)(MEA_Service_Table[serviceId_i].data.valid)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_serviceTable[%d].data.valid(%d) != "
                         "service link list search result (%d) \n",
                         __FUNCTION__,
                         serviceId_i,
                         MEA_Service_Table[serviceId_i].data.valid,
                         *exist_o);
       return MEA_ERROR;
    }

    return MEA_OK;
}

MEA_Status MEA_API_IsExist_Service_BySRV_TYPE(MEA_Unit_t     unit_i,
                                              MEA_Service_t  serviceId_i,
                                              MEA_SRV_mode_t SRV_mode,
                                              MEA_Bool      *exist_o)
{
   

    MEA_API_LOG

        if (MEA_Service_Table == NULL) {
            if (exist_o) {
                *exist_o = MEA_FALSE;
            }
            return MEA_OK;
        }
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid serviceId parameter */
    if (MEA_API_IsServiceIdInRange(unit_i, serviceId_i) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Invalid serviceId %d \n",
            __FUNCTION__, serviceId_i);
        return MEA_ERROR;
    }

    /* Check for valid exist parameter */
    if (exist_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - exist_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    *exist_o = MEA_FALSE;

    if (!MEA_Service_Table[serviceId_i].data.valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_serviceTable[%d].data.valid(%d) != "
            "service link list search result (%d) \n",
            __FUNCTION__,
            serviceId_i,
            MEA_Service_Table[serviceId_i].data.valid,
            *exist_o);
        return MEA_ERROR;
    }

    if((MEA_Service_Table[serviceId_i].data.valid == MEA_TRUE) && MEA_Service_Table[serviceId_i].data.SRV_mode == SRV_mode) {
        *exist_o = MEA_TRUE;


    }

 

    return MEA_OK;
}


MEA_Status mea_drv_Get_Service_ExtrnalHash(MEA_Unit_t     unit_i,
    MEA_Service_t  serviceId_i,
    MEA_SRV_mode_t SRV_mode,
    MEA_Uint32     *hashaddress,
    MEA_Bool      *exist_o)
{


    MEA_API_LOG

        if (MEA_Service_Table == NULL) {
            if (exist_o) {
                *exist_o = MEA_FALSE;
            }
            return MEA_OK;
        }
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid serviceId parameter */
    if (MEA_API_IsServiceIdInRange(unit_i, serviceId_i) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Invalid serviceId %d \n",
            __FUNCTION__, serviceId_i);
        return MEA_ERROR;
    }

    /* Check for valid exist parameter */
    if (exist_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - exist_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }
    if (hashaddress == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - hashaddress == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    *exist_o = MEA_FALSE;

    if (!MEA_Service_Table[serviceId_i].data.valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_serviceTable[%d].data.valid(%d) != "
            "service link list search result (%d) \n",
            __FUNCTION__,
            serviceId_i,
            MEA_Service_Table[serviceId_i].data.valid,
            *exist_o);
        return MEA_ERROR;
    }

    if (MEA_Service_Table[serviceId_i].data.SRV_mode == SRV_mode) {
        *exist_o = MEA_TRUE;
        *hashaddress =MEA_Service_Table[serviceId_i].hashaddress_External;


    }



    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_IsExist_Service_ByKey>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_Service_ByKey(MEA_Unit_t                 unit_i,
                                         MEA_Service_Entry_Key_dbt *key_pi,
                                         MEA_Bool                  *exist_o,
                                         MEA_Service_t             *serviceId_o) {

    MEA_Service_t db_serviceId;
    MEA_ULong_t    db_cookie;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key_pi parameter */
    if (key_pi == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key_pi == NULL \n",__FUNCTION__);
       return MEA_ERROR; 
    }

    /* Check for valid exist_o parameter */
    if (exist_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist_o == NULL \n",__FUNCTION__);
       return MEA_ERROR; 
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* set default output */
    *exist_o = MEA_FALSE;
    if (serviceId_o) {
        *serviceId_o = 0;
    }
 
    /* Get the first service */
    if (MEA_API_GetFirst_Service(unit_i,&db_serviceId,&db_cookie) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_GetFirst_Service failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    while (db_cookie != 0) { 
   
        /* compare the key of this serviceId */
        if (MEA_OS_memcmp(key_pi,
                          &(MEA_Service_Table[db_serviceId].key),
                          sizeof(*key_pi)) == 0) {
            *exist_o = MEA_TRUE;
            if (serviceId_o) {
               *serviceId_o = db_serviceId;
            }
            return MEA_OK; 
        }

        /* get the next service Id */
        if (MEA_API_GetNext_Service(unit_i,&db_serviceId,&db_cookie) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_GetNext_Service failed\n",
                              __FUNCTION__);
            return MEA_ERROR; 
       }
    }
    return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_IsServiceIdInRange>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool MEA_API_IsServiceIdInRange(MEA_Unit_t unit, 
                                    MEA_Service_t serviceId)
{

    MEA_API_LOG

   if(serviceId<MEA_MAX_NUM_OF_SERVICES)
       return MEA_TRUE;
   else
    return MEA_FALSE;        

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Service_Set_MSTP_State>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_API_Service_Set_MSTP_State(MEA_Unit_t unit, MEA_Service_t serviceId, MEA_MSTP_State_t mstp_state)
{
    MEA_Bool      exist = MEA_FALSE;


    MEA_Service_Entry_Data_dbt Service_Entry_Data;

    MEA_Policer_Entry_dbt                sla_params_entry;
    MEA_OutPorts_Entry_dbt               out_ports_tbl_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;


    if (MEA_API_IsExist_Service_ById(unit, serviceId, &exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: service %d failed\n", serviceId);
        return MEA_ERROR;
    }



    if (exist)
    {


        if (MEA_Service_Table[serviceId].data.MSTP_Info.state == mstp_state) {
            return MEA_OK;
        }



        /* Get service information */
        MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));
        if (MEA_API_Get_Service(MEA_UNIT_0,
            serviceId,
            NULL,
            &Service_Entry_Data,
            &out_ports_tbl_entry, /* outPorts */
            &sla_params_entry, /* policer  */
            &EHP_Entry
        ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: MEA_API_Get_Service  for service %d failed\n", serviceId);
            return MEA_ERROR;
        }



        EHP_Entry.ehp_info = (MEA_EHP_Info_dbt*)MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt)*(EHP_Entry.num_of_entries));
        if (MEA_API_Get_Service(MEA_UNIT_0,
            serviceId,
            NULL,
            &Service_Entry_Data,
            &out_ports_tbl_entry, /* outPorts */
            &sla_params_entry, /* policer  */
            &EHP_Entry
        ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: MEA_API_Get_Service  for service %d failed\n", serviceId);
            MEA_OS_free(EHP_Entry.ehp_info);
            return MEA_ERROR;
        }

        Service_Entry_Data.MSTP_Info.state = mstp_state;

        if (MEA_API_Set_Service(MEA_UNIT_0,
            serviceId,
            &Service_Entry_Data,
            &out_ports_tbl_entry,
            &sla_params_entry,
            &EHP_Entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: MEA_API_Set_Service  for service %d failed\n", serviceId);
            MEA_OS_free(EHP_Entry.ehp_info);
            return MEA_ERROR;
        }
        MEA_OS_free(EHP_Entry.ehp_info);

        return MEA_OK;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: service %d not exist\n", serviceId);
    return MEA_ERROR;

}



MEA_Status MEA_API_Service_Set_LXCP(MEA_Unit_t unit, MEA_Service_t serviceId, MEA_Bool enable, MEA_Uint16 LxCp_Id)
{

    MEA_Bool      exist = MEA_FALSE;


    MEA_Service_Entry_Data_dbt Service_Entry_Data;

    MEA_Policer_Entry_dbt                sla_params_entry;
    MEA_OutPorts_Entry_dbt               out_ports_tbl_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;


    if (MEA_API_IsExist_Service_ById(unit, serviceId, &exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: service %d failed\n", serviceId);
        return MEA_ERROR;
    }



    if (exist)
    {


        if ((MEA_Service_Table[serviceId].data.LxCp_Id == LxCp_Id) && 
            (MEA_Service_Table[serviceId].data.LxCp_enable == enable)) 
        {
            return MEA_OK;
        }



        /* Get service information */
        MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));
        if (MEA_API_Get_Service(MEA_UNIT_0,
            serviceId,
            NULL,
            &Service_Entry_Data,
            &out_ports_tbl_entry, /* outPorts */
            &sla_params_entry, /* policer  */
            &EHP_Entry
        ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: MEA_API_Get_Service  for service %d failed\n", serviceId);
            return MEA_ERROR;
        }



        EHP_Entry.ehp_info = (MEA_EHP_Info_dbt*)MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt)*(EHP_Entry.num_of_entries));
        if (MEA_API_Get_Service(MEA_UNIT_0,
            serviceId,
            NULL,
            &Service_Entry_Data,
            &out_ports_tbl_entry, /* outPorts */
            &sla_params_entry, /* policer  */
            &EHP_Entry
        ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: MEA_API_Get_Service  for service %d failed\n", serviceId);
            MEA_OS_free(EHP_Entry.ehp_info);
            return MEA_ERROR;
        }

        Service_Entry_Data.LxCp_enable = enable;
        Service_Entry_Data.LxCp_Id = LxCp_Id;

        if (MEA_API_Set_Service(MEA_UNIT_0,
            serviceId,
            &Service_Entry_Data,
            &out_ports_tbl_entry,
            &sla_params_entry,
            &EHP_Entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: MEA_API_Set_Service  for service %d failed\n", serviceId);
            MEA_OS_free(EHP_Entry.ehp_info);
            return MEA_ERROR;
        }
        MEA_OS_free(EHP_Entry.ehp_info);

        return MEA_OK;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error: service %d not exist\n", serviceId);
    return MEA_ERROR;




    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_IsOutPortOf_Service>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool MEA_API_IsOutPortOf_Service(MEA_Unit_t     unit_i, 
                                     MEA_Service_t  serviceId_i,
                                     MEA_Port_t     outPort_i)
{

    MEA_Uint32 *pPortGrp;
    MEA_Uint32  shiftWord= 0x00000001;
   

    MEA_API_LOG

    //Preform some sanity check
    MEA_CHECK_SERVICE_IN_RANGE(serviceId_i)
    if(!ENET_IsValid_Queue(unit_i,outPort_i,ENET_FALSE)) return MEA_ERROR;


    pPortGrp=(MEA_Uint32 *)(&(MEA_Service_Table[serviceId_i].out_ports.out_ports_0_31));
    pPortGrp +=(outPort_i/32);

    if(!(*pPortGrp & (shiftWord<<(outPort_i%32)))) {
       return MEA_FALSE;
    } else {
        return MEA_TRUE;
    }

      
    



}



MEA_Uint16 MEA_API_Get_Num_OutPorts(MEA_Unit_t     unit_i, MEA_Service_t  serviceId_i)
{
    MEA_Bool exist = MEA_FALSE;
    MEA_Port_t       outport;
    MEA_Uint16               count;

    if (MEA_API_IsExist_Service_ById(unit_i,
        serviceId_i,
        &exist) != MEA_OK) {
        return 0; 
    }
    if (exist == MEA_FALSE) {
        return 0;
    }

    
    count = 0;
    for (outport = 0; outport < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; outport++)
    {
        if (ENET_IsValid_Queue(ENET_UNIT_0, outport, ENET_TRUE) == ENET_FALSE)
        {
            continue;
        }
        if (MEA_API_IsOutPortOf_Service(MEA_UNIT_0, serviceId_i, outport) == MEA_TRUE)
        {
            count++;
        }
    }

        return count;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_Service_num_of_hw_size_func>             */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Service_num_of_hw_Internal_size_func(MEA_Unit_t     unit_i,
                                                   MEA_db_HwUnit_t   hwUnit_i,
                                                   MEA_db_HwId_t *length_o,
                                                   MEA_Uint16    *num_of_regs_o) 
{

    if (hwUnit_i >= mea_drv_num_of_hw_units) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - hwUnit_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          hwUnit_i,
                          mea_drv_num_of_hw_units);
        return MEA_ERROR;
    }

    if (length_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - legnth_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (num_of_regs_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - num_of_regs_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    *length_o = MEA_MAX_NUM_OF_SERVICES_INTERNAL;
    *num_of_regs_o = MEA_IF_SRV_INTERNAL_NUMOF_REGS;

    
    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_Service_num_of_sw_size_func>             */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Service_num_of_sw_Internal_size_func(MEA_Unit_t    unit_i,
                                                   MEA_db_SwId_t *length_o,
                                                   MEA_Uint32    *width_o) 
{
    if (length_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - legnth_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (width_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - width_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    *length_o = MEA_MAX_NUM_OF_SERVICES_INTERNAL;
    *width_o  = 0;

    return MEA_OK;
}


/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status mea_drv_service_Clear_DDR_external(MEA_Unit_t unit_i)
{
    MEA_Uint32   value[16];

    if (MEA_SERVICES_EXTERNAL_SUPPORT) {
        if (!MEA_API_Get_Warm_Restart()) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Start clear the SRV\n");
            //MEA_OS_memset(&srvExdata[0], 0, sizeof(srvExdata));


            MEA_Uint32 addressDDR;
            //MEA_Uint32 i;
            MEA_Uint32 j;
            MEA_DDR_Lock();
            MEA_DB_DDR_HW_ACCESS_ENABLE

            MEA_OS_memset(&value, 0, sizeof(value));

            addressDDR = MEA_SRV_BASE_ADDRES_DDR_START;

            if (MEA_OS_Device_DATA_DDR_RW(MEA_MODULE_DATA_DDR,//MEA_ULong_t  mea_module,
                1,/*0 read 1- write */
                addressDDR,
                (MEA_SRV_BASE_ADDRES_DDR_WIDTH * 4),
                &value[0]
            ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR MODULE_DATA_DDR  Write to DDR \n");
                MEA_DB_DDR_HW_ACCESS_DISABLE
                    MEA_DDR_Unlock();
                return MEA_ERROR;
            }


            for (j =1; j < (((MEA_Uint32)MEA_SRV_BASE_ADDRES_NUM_OF_SRV) ); j++)
            {

                /*write to SERVICE DB on DDR */
                addressDDR = MEA_SRV_BASE_ADDRES_DDR_START + j*(64) + 0x3C; // (j*(MEA_SRV_BASE_ADDRES_DDR_WIDTH * 4));    ///+ (8);
                //MEA_API_WriteReg(unit_i, addressDDR, 0, MEA_MODULE_DATA_DDR);
               // if ((j % 100000) == 0 ) {
               //     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Write to DDR 0x%08x\n",addressDDR);
               // }

                if (MEA_OS_Device_DATA_DDR_RW(MEA_MODULE_DATA_DDR,//MEA_ULong_t  mea_module,
                    1,/*0 read 1- write */
                    addressDDR ,
                    4,
                    &value[0]
                ) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR MODULE_DATA_DDR  Write to DDR \n");
                    MEA_DB_DDR_HW_ACCESS_DISABLE
                        MEA_DDR_Unlock();
                    return MEA_ERROR;
                }


            }
            MEA_DB_DDR_HW_ACCESS_DISABLE
                MEA_DDR_Unlock();
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " End clear the SRV\n");


        }
    }
    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Init_Service_Table>                          */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_Service_Table(MEA_Unit_t unit_i)
{

    MEA_Service_t serviceId;
    MEA_Uint32    size;
    MEA_Uint32   temp;
    
    //MEA_Uint32   srvExdata[MEA_SRV_BASE_ADDRES_DDR_WIDTH];

    if(b_bist_test){
        return MEA_OK;
    }
    /* Init IF Service Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Service table ...\n");

    /* Init db - Hardware shadow for Internal*/
    if (mea_db_Init(unit_i,
                    "Service_Int ",
                    mea_drv_Get_Service_num_of_sw_Internal_size_func,
                    mea_drv_Get_Service_num_of_hw_Internal_size_func,
                    &MEA_Service_Internal_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_db_Init(unit_i,
        "Service_Int_info",
        mea_drv_Get_Service_num_of_sw_Internal_size_func,
        mea_drv_Get_Service_num_of_hw_Internal_size_func,
        &MEA_Service_Internal_db_getHW) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_db_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    


    /* Flush Service table in the Device */
    if (mea_drv_Flush_HW_Service_Table(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Flush_HW_Service_Table failed \n",
                         __FUNCTION__);
       
        return MEA_ERROR;
    }


    
    size = sizeof(MEA_Uint32);
    size = ((MEA_MAX_NUM_OF_SERVICES_INTERNAL/32) * sizeof(MEA_Uint32));
    MEA_Service_Internal_BIT = (MEA_Uint32*)MEA_OS_malloc(size);
    if (MEA_Service_Internal_BIT == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_Service_Internal_Table failed (size=%d)\n",
            __FUNCTION__, size);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db_getHW);
        
        return MEA_ERROR;
    }
    MEA_OS_memset(&MEA_Service_Internal_BIT[0], 0, size);

    size = sizeof(MEA_Uint32);
    size = ((MEA_MAX_NUM_OF_SERVICES_EXTERNAL_HW / 32) * sizeof(MEA_Uint32));
    MEA_Service_External_BIT = (MEA_Uint32*)MEA_OS_malloc(size);
    if (MEA_Service_External_BIT == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_Service_Internal_Table failed (size=%d)\n",
            __FUNCTION__, size);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db_getHW);
        return MEA_ERROR;
    }
    MEA_OS_memset(&MEA_Service_External_BIT[0], 0, size);



    



    if (MEA_MAX_NUM_OF_SERVICES_EXTERNAL_HW == 0){
        temp = 1;
    }
    else{
        temp = (MEA_MAX_NUM_OF_SERVICES_EXTERNAL_HW / 32);
    }

    size = ((temp) * sizeof(MEA_Uint32));
    MEA_Service_External_Hash_BIT = (MEA_Uint32*)MEA_OS_malloc(size);
    if (MEA_Service_External_Hash_BIT == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_Service_Internal_Table failed (size=%d)\n",
            __FUNCTION__, size);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db_getHW);
        return MEA_ERROR;
    }

    MEA_OS_memset(&MEA_Service_External_Hash_BIT[0], 0, size);



     




    /* Init Software shadow  - MEA_Service_Table Internal */
    size = sizeof(MEA_Service_Entry_dbt);
    size = (MEA_MAX_NUM_OF_SERVICES * sizeof(MEA_Service_Entry_dbt));
    MEA_Service_Table = (MEA_Service_Entry_dbt*)MEA_OS_BigMalloc(size);
    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG," MEA_Service_Table Service ==> size=%d \n",size);
    if (MEA_Service_Table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate MEA_Service_Table failed (size=%d)\n",
                          __FUNCTION__,size);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db_getHW);
        return MEA_ERROR;
    }


    MEA_OS_memset((char*)MEA_Service_Table,0,size);

    for(serviceId=0;
        serviceId<MEA_MAX_NUM_OF_SERVICES;
        serviceId++) {
        MEA_Service_Table[serviceId].xPermissionId = ENET_PLAT_GENERATE_NEW_ID;
    }

    /* Init the Service Entry */
    mea_drv_Init_ServiceEntry_Table(unit_i);
    
   
    /* Init the ServiceRange */ 
    if (mea_drv_Init_serviceRange_Table(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate mea_serviceRange_Init failed (size=%d)\n",
                          __FUNCTION__,size);
        return MEA_ERROR;
    }
   
    mea_drv_ClassifierKEY_Character_Init(unit_i);


    size = ((MEA_MAX_OF_ACL_SRV_TOTAL)* sizeof(MEA_srv_filter_prof_dbt));
    MEA_srv_filter_prof_Table = (MEA_srv_filter_prof_dbt*)MEA_OS_malloc(size);
    if (MEA_srv_filter_prof_Table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_Service_Internal_Table failed (size=%d)\n",
            __FUNCTION__, size);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db);
        mea_db_Conclude(unit_i, &MEA_Service_Internal_db_getHW);
        return MEA_ERROR;
    }
	MEA_OS_memset(&MEA_srv_filter_prof_Table[0], 0, size);

#if 1
    if (MEA_device_environment_info.MEA_CLEAR_DDR_Enable) 
    {
        if (MEA_device_environment_info.MEA_CLEAR_DDR == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " environment_info.MEA_CLEAR_DDR CLEAR DB_DDR  Enable \n");
            mea_drv_service_Clear_DDR_external(unit_i);
        }
        else {
            /*Not clear DDR*/
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " environment_info.MEA_CLEAR_DDR CLEAR DB_DDR Disable \n");
        }


    }else{
        if (glClearDDR_servie) {
             mea_drv_service_Clear_DDR_external(unit_i);
        }
        else {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Not-clear DDR SRV\n");
        }
	}
        
    
    

#endif   
    /* Return to caller */
    return MEA_OK; 

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_Service_callback>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_Service_callback(MEA_Unit_t unit_i,
                                           MEA_ind_write_t* ind_write_pio)
{

    ind_write_pio->tableType  = 99;/* not relevant - service hash table */
    ind_write_pio->tableOffset     = 0; /* not relevant - service hash table */
    ind_write_pio->cmdMask         = MEA_IF_CMD_MAKE_REG_MASK;      
    ind_write_pio->tableAddrReg     = MEA_IF_CMD_PARAM_REG_IGNORE; /* service hash table */
    ind_write_pio->tableAddrMask = 0;


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_ReInit_Service_Table>                          */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_Service_Table(MEA_Unit_t unit_i)
{
    if (b_bist_test) {
        return MEA_OK;
    }

    if (mea_drv_ReInit_service_Internal_Hush(unit_i) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_service_Internal_Hush failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
   

    /* ReInit the Service Entry */
    if (mea_drv_ReInit_ServiceEntry_Table(unit_i) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_drv_ReInit_ServiceEntry_Table failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }
    /* ReInit the Service Entry */
    if (mea_drv_ReInit_serviceRange_Table(unit_i) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_drv_ReInit_ServiceRange_Table failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    } 
    

    if(mea_drv_ClassifierKEY_Character_ReInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_ServiceRange_Table failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    /* Return to caller */
    return MEA_OK; 

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_Service_db>                              */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Service_Internal_db(MEA_Unit_t unit_i,
                                  MEA_db_dbt* db_po) {

    if (db_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - db_po == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    *db_po = MEA_Service_Internal_db;

    return MEA_OK;

}

MEA_Status mea_drv_Get_Service_Internal_db_infoHw(MEA_Unit_t unit_i,
    MEA_db_dbt* db_po) {

    if (db_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - db_po == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

    *db_po = MEA_Service_Internal_db_getHW;

    return MEA_OK;

}



MEA_Status mea_drv_Get_xPermissionIdbyIndex(MEA_Uint32 index, ENET_xPermissionId_t *xPermissionId)
{
    *xPermissionId=MEA_Service_Table[index].xPermissionId; 
    
   return MEA_OK;
}
/*********************************************************************************/
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_serviceRange_UpdateHwEntry>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

static void mea_drv_serviceRange_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    MEA_Unit_t                  unit_i   = (MEA_Unit_t            )arg1;
    MEA_db_HwUnit_t             hwUnit_i = (MEA_db_HwUnit_t       )((long)arg2);
    MEA_db_HwId_t               hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_SrvRange_Vlan_pri_dbt*  entry_pi = (MEA_SrvRange_Vlan_pri_dbt*)arg4;
    MEA_Uint32                  val[MEA_IF_CMD_PARAM_TBL_TYP_SERVICE_VLAN_RANGE_NUM_OF_REGS];
    MEA_Uint32                  i;
    MEA_Uint32                  count_shift;
    MEA_Uint32 pri;
    
    MEA_Uint32 nettag;
    MEA_Uint32 sub_protocol_type;

    
    
    for (i=0;i< MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
    
    count_shift=0;

    

    if(entry_pi->L2_protocol_type == MEA_PARSING_L2_KEY_DSA_TAG){
        sub_protocol_type=(entry_pi->sub_protocol_type);
    }else{
        sub_protocol_type = ((entry_pi->sub_protocol_type==0)? (0xf) : (entry_pi->sub_protocol_type));
    }



    nettag= entry_pi->net_tag & 0xfffff;
    nettag |= sub_protocol_type<<20;

        MEA_OS_insert_value(count_shift,
                            MEA_IF_SRV_VLAN_RANGE_NETTAG_WIDTH,
                            nettag ,
                            &val[0]);
        count_shift+=MEA_IF_SRV_VLAN_RANGE_NETTAG_WIDTH;
     
        nettag= entry_pi->net_tag_to_value & 0xfffff;
        nettag |= sub_protocol_type<<20;

        MEA_OS_insert_value(count_shift,
                            MEA_IF_SRV_VLAN_RANGE_NETTAG_TO_WIDTH,
                            nettag ,
                            &val[0]);
        count_shift+=MEA_IF_SRV_VLAN_RANGE_NETTAG_TO_WIDTH;

        MEA_OS_insert_value(count_shift,
                            MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_FROM_WIDTH,
                            entry_pi->inner_netTag_from ,
                            &val[0]);
        count_shift+=MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_FROM_WIDTH;

        MEA_OS_insert_value(count_shift,
                            MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_TO_WIDTH,
                            entry_pi->inner_netTag_to ,
                            &val[0]);
        count_shift+=MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_TO_WIDTH;
        pri=0;
        pri=entry_pi->priType<<6;
        pri|= entry_pi->pri;
        MEA_OS_insert_value(count_shift,
                            MEA_IF_SRV_VLAN_RANGE_PRI_WIDTH,
                            pri ,
                            &val[0]);
        count_shift+=MEA_IF_SRV_VLAN_RANGE_PRI_WIDTH;
    
        pri=0;
        pri=entry_pi->priType<<6;
        pri|=entry_pi->pri_to_value;
        

        MEA_OS_insert_value(count_shift,
                            MEA_IF_SRV_VLAN_RANGE_PRI_TO_WIDTH,
                            pri,
                            &val[0]);
        count_shift+=MEA_IF_SRV_VLAN_RANGE_PRI_TO_WIDTH;

        MEA_OS_insert_value(count_shift,
                            MEA_IF_SRV_VLAN_RANGE_L2TYPE_WIDTH,
                            entry_pi->L2_protocol_type, 
                            &val[0]);
        count_shift+=MEA_IF_SRV_VLAN_RANGE_L2TYPE_WIDTH;


        MEA_OS_insert_value(count_shift,
                            MEA_IF_SRV_VLAN_RANGE_PTR_CONTEX_WIDTH(unit_i,hwUnit_i),
                            entry_pi->ptr_context,
                            &val[0]);
        count_shift+=MEA_IF_SRV_VLAN_RANGE_PTR_CONTEX_WIDTH(unit_i,hwUnit_i);    

    MEA_OS_insert_value(count_shift,
                            MEA_IF_SRV_VLAN_RANGE_VALID_WIDTH,
                            entry_pi->valid,
                            &val[0]);
        count_shift+=MEA_IF_SRV_VLAN_RANGE_VALID_WIDTH;

    
       

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(unit_i,MEA_IF_SRV_MAKE0_REG , val[i] , MEA_MODULE_IF);
    }



    /* Update Hardware shadow */
    if (mea_db_Set_Hw_Regs(unit_i,
                           MEA_ServiceRange_db,
                           hwUnit_i,
                           hwId_i,
                           MEA_NUM_OF_ELEMENTS(val),
                           val) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                          __FUNCTION__,
                          hwUnit_i,
                          hwId_i);
    }



}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_serviceRange_UpdateHw>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_serviceRange_UpdateHw(MEA_Unit_t                  unit_i,
                                                MEA_Service_t               serviceId_i,
                                                MEA_Uint16                  rangeId_i,
                                                MEA_SrvRange_Vlan_pri_dbt*  new_entry_pi,
                                                MEA_SrvRange_Vlan_pri_dbt*  old_entry_pi) 
{

    MEA_ind_write_t             ind_write;
    MEA_db_HwUnit_t             hwUnit;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    /* Check if we have change */
    if ((old_entry_pi != NULL) &&
        (MEA_OS_memcmp(old_entry_pi,new_entry_pi,sizeof(*old_entry_pi)) == 0)) {
        return MEA_OK;
    }


     
    
    /* build the ind_write value */
    ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_SERVICE_VLAN_RANGE;
    ind_write.tableOffset    = new_entry_pi->hwId; 
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask        = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg    = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;  
    
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_serviceRange_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)new_entry_pi->hwId);
    ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

    if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                          __FUNCTION__,
                          ind_write.tableType,
                          ind_write.tableOffset,
                          MEA_MODULE_IF);
        return MEA_ERROR;
    }

    /* return to caller */
    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_serviceRange_Check_Intersect>                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_serviceRange_Check_Intersect(MEA_Unit_t                  unit_i,                                 
                                                       MEA_Service_Entry_Key_dbt  *key_i) 
{
    MEA_Service_Entry_Key_dbt key;
    MEA_Uint32                i;
    MEA_Bool                  NET_Intersect;
    MEA_Bool                  PRI_Intersect;
    MEA_Parser_Entry_dbt      parser_entry;
    MEA_Uint32                or_mask_net_tag;
    


    /* If not range simple return back */
    if ((key_i->net_tag_to_valid == MEA_FALSE) &&
        (key_i->pri_to_valid     == MEA_FALSE)   ) {
        return MEA_OK;
    }   


    if (MEA_API_Get_Parser_Entry(unit_i,
                                 (MEA_Port_t)(key_i->src_port),
                                 &parser_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_Parser_Entry for port %d failed\n",
                          __FUNCTION__,key_i->src_port);
        return MEA_ERROR;
    }
 
    or_mask_net_tag =  ((((MEA_Uint32)(parser_entry.f.or_mask_net_tag_14_19)) << 14) |
                        (((MEA_Uint32)(parser_entry.f.or_mask_net_tag_0_13 ))      )  );
    

    /* Check valid wildcard for net_tag */
    if ((key_i->net_tag_to_valid == MEA_TRUE) &&
        (or_mask_net_tag         != MEA_WILDCARD_ALL_NTAGS) &&
        ((or_mask_net_tag         & 0x00000fff) != 0)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Service range not allowed with this wildcard for the ingress port "
                          "(port=%d)\n",
                          __FUNCTION__,key_i->src_port);
        return MEA_ERROR;
    }

    /* Check valid wildcard for pri */
    if ((key_i->pri_to_valid == MEA_TRUE                   ) &&
        (parser_entry.f.or_mask_pri != MEA_WILDCARD_ALL_PRI) &&
        (parser_entry.f.or_mask_pri != 0                   )  ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Service range not allowed with this wildcard for the ingress port "
                          "(port=%d)\n",
                          __FUNCTION__,key_i->src_port);
        return MEA_ERROR;
    }


    

    /* Build key from key_i , and build the correct range that require by the user */
    MEA_OS_memcpy(&key,key_i,sizeof(key));
    if ((or_mask_net_tag      == MEA_WILDCARD_ALL_NTAGS) && 
        (key.net_tag_to_valid == MEA_FALSE             ) &&
        (key.net_tag          == MEA_WILDCARD_ALL_NTAGS) && 
        (key.net_tag_to_value == 0                     )  ) {
        key.net_tag          = 1;          /* start from vlan 1*/ 
        key.net_tag_to_value = 4095;     /* end of vlan 4095*/
    }
    if ((parser_entry.f.or_mask_pri == MEA_WILDCARD_ALL_PRI) && 
        (key.pri_to_valid           == MEA_FALSE           ) &&     
        (key.pri                    == MEA_WILDCARD_ALL_PRI) && 
        (key.pri_to_value           == 0                   )  ) {
        key.pri          = 0;
        key.pri_to_value = 0x3f;
    }

    if (key.inner_netTag_from_valid == MEA_FALSE) {
        key.inner_netTag_from_value = 0;
    }
    if (key.inner_netTag_to_valid == MEA_FALSE) {
        key.inner_netTag_to_value = 4095;
    }





    for(i=0; i< ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_VLAN_RANGE_LENGTH;i++){

        if (MEA_ServiceRange_Table[i].valid == MEA_FALSE) {
            continue;
        }   

        if (MEA_ServiceRange_Table[i].src_port != key.src_port) {
            continue;
        }

        /* Calculate NET_Intersect */
        NET_Intersect = MEA_FALSE;
        if (((key.net_tag          & 0x0000fff) >= MEA_ServiceRange_Table[i].net_tag         ) &&  
            ((key.net_tag          & 0x0000fff) <= MEA_ServiceRange_Table[i].net_tag_to_value)  ) { 
            NET_Intersect = MEA_TRUE;  
        }
        if (((key.net_tag_to_value & 0x0000fff) >= MEA_ServiceRange_Table[i].net_tag         ) &&  
            ((key.net_tag_to_value & 0x0000fff) <= MEA_ServiceRange_Table[i].net_tag_to_value)  ) { 
           NET_Intersect = MEA_TRUE;  
       }

        /* Calculate PRI_Intersect */
        PRI_Intersect = MEA_FALSE;
        if ((key.pri          >= MEA_ServiceRange_Table[i].pri         ) &&  
            (key.pri          <= MEA_ServiceRange_Table[i].pri_to_value)  ) { 
            PRI_Intersect = MEA_TRUE;  
        }
        if ((key.pri_to_value >= MEA_ServiceRange_Table[i].pri         ) &&  
            (key.pri_to_value <= MEA_ServiceRange_Table[i].pri_to_value)  ) { 
            PRI_Intersect = MEA_TRUE;  
        }

        /* If we have intersection on PRI or NET then fail the check */
        if ((PRI_Intersect == MEA_TRUE) && 
            (NET_Intersect == MEA_TRUE)  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - fount other service (%d) range who as intersect with the new service\n",
                              __FUNCTION__,
                              MEA_ServiceRange_Table[i].ptr_context );
            return MEA_ERROR;
       }
         
      
 
    }//for
    
    /* Return to caller */
    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_serviceRange_Delete>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_serviceRange_Delete(MEA_Uint16 rangeId_i){

    MEA_SrvRange_Vlan_pri_dbt* range_entry;
    MEA_db_HwUnit_t hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_ERROR;)
    
    /* Get the range_entry */ 
    range_entry = &(MEA_ServiceRange_Table[rangeId_i]);

    /* Verify the entry is valid */
    if(range_entry->valid== MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Service range already delete (ID=%d)\n",
                          __FUNCTION__,rangeId_i);   
        return MEA_ERROR;
    }
    
    /* Delete the entry from DB */
    if (mea_db_Delete(MEA_UNIT_0,
                      MEA_ServiceRange_db,
                      (MEA_db_SwId_t)(range_entry->ptr_context),
                      hwUnit) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_db_Delete failed \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Delete the entry */
    MEA_OS_memset(range_entry,0,sizeof(*range_entry));
    range_entry->valid = MEA_FALSE;


    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_serviceRange_Add>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_serviceRange_Add (MEA_Unit_t                 unit_i,
                                            MEA_Service_t              serviceId_i,
                                            MEA_Service_Entry_Key_dbt *key_i,
                                            MEA_Uint16                *rangeId_o){
   
    MEA_Parser_Entry_dbt       parser_entry;
    MEA_Uint32                 or_mask_net_tag; 
    MEA_SrvRange_Vlan_pri_dbt* range_entry=NULL;
    MEA_db_HwUnit_t            hwUnit;
    MEA_db_HwId_t              hwId;
    MEA_Bool                   supportPort_ServiceRange=MEA_FALSE;
    MEA_Uint16                 index, indexInport;
    MEA_Uint32                 i,iend ;
    MEA_Bool                   foundPlace;
	MEA_Uint32                 val_shift;


    /* if not service Range ignore and simple return */
    if ((key_i->net_tag_to_valid == MEA_FALSE) &&
        (key_i->pri_to_valid     == MEA_FALSE)  ) {
        return MEA_OK;
    }

    /* Get current hwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

    /* Calculate rangeId */
    if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - calculate rangeId/ptr_context for non ENET3000 is not support yet\n",
                          __FUNCTION__,*rangeId_o,serviceId_i);
        return MEA_ERROR;
    }
   
    if(mea_drv_Get_DeviceInfo_ServiceRange_PortSupport(unit_i, 
                                                               (MEA_Port_t)key_i->src_port ,
                                                               &supportPort_ServiceRange, 
                                                               &index)!=MEA_OK){

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - The port % not support service Range \n",
                          __FUNCTION__,key_i->src_port);
        return MEA_ERROR;
    }
    if(supportPort_ServiceRange != MEA_TRUE){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - The port % not support service Range \n",
                          __FUNCTION__,key_i->src_port);
        return MEA_ERROR;
    }
    foundPlace=MEA_FALSE;
    
    iend=((index * MEA_SERVICE_RANGE_MAX_NUM_SERVICES_PER_PORT) + (MEA_SERVICE_RANGE_MAX_NUM_SERVICES_PER_PORT));
   
    for(i=(index * MEA_SERVICE_RANGE_MAX_NUM_SERVICES_PER_PORT),indexInport=0 ; i < iend  ; i++  ){
           if (MEA_ServiceRange_Table[i].valid == MEA_TRUE){
               indexInport++;
               continue;
           } else {
               /*found place to Add*/
               foundPlace=MEA_TRUE;
               *rangeId_o=(MEA_Uint16)i ;
               range_entry = &(MEA_ServiceRange_Table[i]);
               break;
           }
    }

    if(foundPlace != MEA_TRUE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Service Range for port % is full\n",
                          __FUNCTION__,key_i->src_port);
        return MEA_ERROR;
    }
    
  
    
    /* Get parser entry for or_mask_net_tag and or_mask_pri */
    if (MEA_API_Get_Parser_Entry(unit_i,
                                 (MEA_Port_t) (key_i->src_port),
                                 &parser_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_Parser_Entry for port %d failed\n",
                          __FUNCTION__,key_i->src_port);
        return MEA_ERROR;
    }
    or_mask_net_tag =  ((((MEA_Uint32)(parser_entry.f.or_mask_net_tag_20_23)) << 20) |
                        (((MEA_Uint32)(parser_entry.f.or_mask_net_tag_14_19)) << 14) |
                        (((MEA_Uint32)(parser_entry.f.or_mask_net_tag_0_13 ))      )  );

    if (or_mask_net_tag == 0){
      //  MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " or_mask_net_tag id Zero\n");
    }

    /* Build the range_entry */
	val_shift=MEA_OS_calc_from_size_to_width(MEA_SERVICE_RANGE_MAX_NUM_SERVICES_PER_PORT);
    MEA_OS_memset(range_entry,0,sizeof(*range_entry));
    range_entry->hwId              = (index<<val_shift) + ((indexInport)) ;
    range_entry->valid             = MEA_TRUE;
    range_entry->src_port          = key_i->src_port;
    range_entry->L2_protocol_type  = key_i->L2_protocol_type;
    range_entry->sub_protocol_type = key_i->sub_protocol_type;
    range_entry->net_tag           = key_i->net_tag          ; 
    range_entry->net_tag_to_value  = key_i->net_tag_to_value ; 
    range_entry->priType           = key_i->priType;

    range_entry->pri               = key_i->pri;

    range_entry->pri_to_value      = key_i->pri_to_value;
    range_entry->ptr_context       = serviceId_i ;
    range_entry->inner_netTag_from = ((key_i->inner_netTag_from_valid) ? (key_i->inner_netTag_from_value ): MEA_WILDCARD_ALL_NTAGS);
    range_entry->inner_netTag_to   = ((key_i->inner_netTag_to_valid  ) ? (key_i->inner_netTag_to_value   ): MEA_WILDCARD_ALL_NTAGS);
//    range_entry->inner_pri_from    = (!(key_i->inner_pri_from_valid)) ?  0 : key_i->inner_pri_from_value ;
//    range_entry->inner_pri_to      = (!(key_i->inner_pri_to_valid  )) ? 63 : key_i->inner_pri_to_value ;

    /* Handle case of net_tag range is not valid */
    if ((key_i->net_tag_to_valid == MEA_FALSE) &&
        (key_i->net_tag_to_value == 0        )  ) {
        
            range_entry->net_tag_to_value = range_entry->net_tag;
            
    }   

    /* Handle case of pri range is not valid */
    if ((key_i->pri_to_valid == MEA_FALSE) &&
        (key_i->pri_to_value == 0        )  ) {
            range_entry->pri_to_value  = range_entry->pri;
    }   

    /* Add to db */   
    hwId = (MEA_db_HwId_t)(range_entry->hwId);
    if (mea_db_Create(unit_i,
                      MEA_ServiceRange_db,
                      (MEA_db_SwId_t)serviceId_i,
                      hwUnit,
                      &hwId) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_db_Create failed \n",__FUNCTION__);
       range_entry->valid = MEA_FALSE;
       return MEA_ERROR;
    } 
    range_entry->hwId = hwId;


    /* return to caller */
    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Get_service_rangeEntry>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_serviceRange_Get_Entry(MEA_Uint16 index,
                                                 MEA_SrvRange_Vlan_pri_dbt  *entry_o)
{
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
   if(entry_o== NULL){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - entry_o == NULL \n",
                              __FUNCTION__);
    return MEA_ERROR;  
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
    
    if(MEA_ServiceRange_Table == NULL)
		return MEA_ERROR;

    if(MEA_ServiceRange_Table[index].valid == MEA_FALSE)
    {
     /* do not print error */
     //MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
     //                          "%s - service rangeEntry %d is empty\n",
     //                          __FUNCTION__ ,index);
    return MEA_ERROR;
    }

 
    MEA_OS_memcpy(entry_o,&MEA_ServiceRange_Table[index],sizeof(*entry_o));

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_Get_ServiceRange_num_of_hw_size_func>             */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_ServiceRange_num_of_hw_size_func(MEA_Unit_t     unit_i,
                                                           MEA_db_HwUnit_t   hwUnit_i,
                                                        MEA_db_HwId_t *length_o,
                                                        MEA_Uint16    *num_of_regs_o) 
{
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (hwUnit_i >= mea_drv_num_of_hw_units) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - hwUnit_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          hwUnit_i,
                          mea_drv_num_of_hw_units);
        return MEA_ERROR;
    }

    if (length_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - legnth_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (num_of_regs_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - num_of_regs_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
    *length_o = (MEA_db_HwId_t)ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_VLAN_RANGE_LENGTH; 
    *num_of_regs_o  = ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_VLAN_RANGE_NUM_OF_REGS;

    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <mea_drv_Get_ServiceRange_num_of_sw_size_func>              */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_ServiceRange_num_of_sw_size_func(MEA_Unit_t    unit_i,
                                                         MEA_db_SwId_t *length_o,
                                                        MEA_Uint32    *width_o) 
{
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS    
    if (length_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - legnth_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (width_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - width_o == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    *length_o = MEA_MAX_NUM_OF_SERVICES ;/* WE USED ONLY THE 32*/
    *width_o  = 0;

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_serviceRange_Table>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_HW_serviceRange_Table(MEA_Unit_t unit_i)
{
MEA_Uint32 value;

        if (MEA_SERVICE_RANGE_SUPPORT == MEA_TRUE) {
            /* enable the mode in Hw and reset to zero all the data*/
            value = 3; /* enable and reset all hw db*/
            MEA_IF_INIT_HW_ACCESS_ENABLE
            MEA_API_WriteReg(unit_i,
                             ENET_IF_SERVICRANGE_REG,
                             value,
                              MEA_MODULE_IF);
            MEA_IF_INIT_HW_ACCESS_DISABLE 
        }


return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_serviceRange_Table>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Init_serviceRange_Table(MEA_Unit_t unit_i)
{
    
    

    MEA_db_HwUnit_t   hwUnit;
    mea_memory_mode_e save_globalMemoryMode;
    MEA_Uint32 size;
   
    
    
    
    /* Allocate MEA_Counters_PM_Type_Table */
    size = ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_VLAN_RANGE_LENGTH * sizeof(MEA_SrvRange_Vlan_pri_dbt);
    if (size == 0) {
        MEA_ServiceRange_Table = NULL;
    } else {
        MEA_ServiceRange_Table = (MEA_SrvRange_Vlan_pri_dbt*)MEA_OS_malloc(size);
        if (MEA_ServiceRange_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_ServiceRange_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_ServiceRange_Table[0]),0,size);
    }

       
       
    /* Init db - Hardware shadow */
    if (mea_db_Init(unit_i,
                    "ServiceRange ",
                    mea_drv_Get_ServiceRange_num_of_sw_size_func,
                    mea_drv_Get_ServiceRange_num_of_hw_size_func,
                    &MEA_ServiceRange_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {

        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        if(mea_drv_Init_HW_serviceRange_Table(unit_i) !=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - flush_serviceRange failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
        }

        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

    }
    
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_serviceRange_Table>                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_ReInit_serviceRange_Table(MEA_Unit_t unit_i)
{

    MEA_db_HwUnit_t   hwUnit;
    mea_memory_mode_e save_globalMemoryMode;
   

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {

        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);

        if (MEA_SERVICE_RANGE_SUPPORT == MEA_TRUE) {
            /* enable the mode in Hw and reset to zero all the data*/
           if(mea_drv_Init_HW_serviceRange_Table(unit_i) !=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - flush_serviceRange failed\n",
                          __FUNCTION__);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
            }
        }

        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

    }

    /* ReInit ServiceEntry table   */
    if (mea_db_ReInit(unit_i,
                      MEA_ServiceRange_db,
                      MEA_IF_CMD_PARAM_TBL_SERVICE_VLAN_RANGE,
                      MEA_MODULE_IF,
                      NULL,
                      MEA_FALSE) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit for ServiceRange failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }


    /* Return to Caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Conclude_ServiceRange_Table>                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Conclude_serviceRange_Table(MEA_Unit_t unit_i)
{
    /* phase2 - need to confirm that all entry are not in use by service */

    if (mea_db_Conclude(unit_i,&MEA_ServiceRange_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_dn_Conclude failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_ServiceRange_Table != NULL) {
        MEA_OS_free(MEA_ServiceRange_Table);
        MEA_ServiceRange_Table = NULL;
    }


    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_ServiceRange_db>                         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_ServiceRange_db(MEA_Unit_t unit_i,
                                       MEA_db_dbt* db_po) {

    if (db_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - db_po == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    *db_po = MEA_ServiceRange_db;

    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*              <mea_drv_Service_ReUpdateByPortAndVlan>                      */ 
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status mea_drv_Service_ReUpdateByPortAndVlan(MEA_Unit_t    unit_i,
                                                 MEA_Port_t    port_i,
                                                 MEA_Uint16    vlan_i,
                                                 MEA_PortState_te state_i)
{

    
    mea_service_node_t* ptr;
//    MEA_Uint32* ptr_outport;
    MEA_Service_t serviceId;
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId;
    MEA_Service_Entry_dbt* entry;
    
//    MEA_Uint32 i,j,mask;
//    ENET_Queue_dbt         entry_queue;


    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    for(ptr=p_service_head;ptr;ptr=ptr->next) {
        serviceId=ptr->serviceId;
        entry = &(MEA_Service_Table[serviceId]);
  
        //not match our port vlan
        // continue;
        if (entry->data.stp==MEA_FALSE) {
            continue;
        }
        if ((MEA_Port_t)(entry->key.src_port)             != port_i){
            continue;
        }
        if(vlan_i == MEA_PORT_STATE_VLAN_DONT_CARE){
          
        } else {
            if((MEA_Uint32)(entry->key.net_tag & 0x00000FFF) != (MEA_Uint32)vlan_i ){
              continue; // // no match
            }
        }
        


        MEA_DRV_GET_HW_ID(unit_i,
            mea_drv_Get_Service_Internal_db,
            (MEA_db_SwId_t)serviceId,
            hwUnit,
            hwId,
            return MEA_ERROR;)
        if (mea_ServiceEntry_UpdateHw(  unit_i,
                                        serviceId,
                                        hwUnit,
                                        hwId,
                                        entry,
                                        NULL /*port state*/) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - mea_ServiceEntry_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }
    }
    
    return MEA_OK;
}






/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_API_delete_all_services>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_all_services(MEA_Unit_t                      unit){

	return MEA_DRV_Delete_all_services(unit, MEA_FALSE);
}

MEA_Status MEA_DRV_Delete_all_services(MEA_Unit_t    unit,MEA_Bool force) 
{

   MEA_Service_t db_serviceId;
   MEA_Service_t temp_db_serviceId;
   MEA_ULong_t    db_cookie;

   /* Get first service */
   if (MEA_API_GetFirst_Service(unit,&db_serviceId,&db_cookie) != MEA_OK)  {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_GetFirst_Service failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
   }

   /* loop until no more services */
   while (db_cookie != 0) { 


       /* Save the current db_serviceId */
       temp_db_serviceId = db_serviceId;

       /* Make next before delete the object */ 
       if (MEA_API_GetNext_Service(unit,&db_serviceId,&db_cookie) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetNext_Service failed (db_serviceId=%d)\n",
                             __FUNCTION__,db_serviceId);
           return MEA_ERROR;
       }

       /* Delete the save db_serviceId */
       if (MEA_DRV_Delete_Service(unit,temp_db_serviceId,force)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete service %d\n",
                             __FUNCTION__,temp_db_serviceId);
           return MEA_ERROR;
       }
        
   }
 

   /* Return to caller */
   return MEA_OK;
}


static MEA_Status mea_drv_ReInit_service_Internal_Hush(MEA_Unit_t unit_i)
{


    MEA_Service_t           serviceId;
    MEA_Service_Entry_dbt   entry;
    MEA_db_HwUnit_t         hwUnit;
    MEA_db_HwId_t           hwId;
    
    MEA_Service_t from=0;
    MEA_Service_t to=MEA_MAX_NUM_OF_SERVICES-1;


    /* Search for free entry */
    for((serviceId)=from;(serviceId)<=to;(serviceId)++){
        if (MEA_Service_Table[(serviceId)].data.valid == MEA_FALSE){
        continue;
        }
        if (MEA_Service_Table[(serviceId)].data.SRV_mode != MEA_SRV_MODE_REGULAR)
            continue;

        MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)
        
            MEA_DRV_GET_HW_ID(unit_i,
            mea_drv_Get_Service_Internal_db,
                (MEA_db_SwId_t)serviceId,
                hwUnit,
                hwId,
                return MEA_ERROR;)

                MEA_OS_memcpy(&entry,&MEA_Service_Table[(serviceId)],sizeof(MEA_Service_Table[0]));
            
                MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
                    mea_drv_Get_Service_Internal_db,
                    MEA_Service_Table[serviceId].Sid_Internal,
                hwUnit,
                hwId,
                return MEA_ERROR);
                if(MEA_Service_Table[(serviceId)].service_Type == MEA_SERVICE_CLASSIFCATION_REGULAR){
                    // MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Reinit CLASSIFCATION_REGULAR serviceId %d \n",serviceId );
                    if (mea_drv_service_Set_regular_classifcation(unit_i,hwUnit,hwId,&entry,MEA_FALSE,MEA_SETTING_TYPE_CREATE) != MEA_OK){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - mea_drv_service_Set_regular_classifcation failed \n",__FUNCTION__);
                        return MEA_ERROR;
                    }
                    

                }

                if (MEA_Service_Table[(serviceId)].service_Type == MEA_SERVICE_CLASSIFCATION_EVC){
                    // MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Reinit CLASSIFCATION_REGULAR serviceId %d \n",serviceId );
                    if (mea_drv_service_Set_EVC_classifcation(unit_i, hwUnit, hwId, &entry, MEA_FALSE, MEA_SETTING_TYPE_CREATE) != MEA_OK){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - mea_drv_service_Set_EVC_classifcation failed \n", __FUNCTION__);
                        return MEA_ERROR;
                    }


                }
        
                 
        
    
    }



    /* Return to Caller */
    return MEA_OK;
}



/************************************************************************/
/*      Vlan Range  profile                                             */
/************************************************************************/



/************************************************************************/
/* Init                                                                  */
/************************************************************************/
MEA_Status mea_drv_Init_vlanrange_profile(MEA_Unit_t unit_i)
{
    MEA_Uint32 size;

    if (!MEA_SERVICE_RANGE_SUPPORT)
        return MEA_OK;
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize vlan Range ...\n");


   
    /* Allocate PacketGen Table */
    size = MEA_SERVICE_RANGE_NUM_OF_PROFILE * sizeof(MEA_vlanRange_prof_dbt);
    if (size != 0) {
        MEA_vlanRange_profile = (MEA_vlanRange_prof_dbt*)MEA_OS_malloc(size);
        if (MEA_vlanRange_profile == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_vlanRange_profile failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_vlanRange_profile[0]),0,size);
    }




    


    return MEA_OK;
}


/************************************************************************/
/* Reinit                                                                */
/************************************************************************/
 MEA_Bool mea_drv_RInit_vlanrange_profile(MEA_Unit_t       unit_i)
{
    MEA_Uint16 id;

    if (b_bist_test) {
        return MEA_OK;
    }
     if (!MEA_SERVICE_RANGE_SUPPORT)
         return MEA_OK;

    for (id=0;id<MEA_SERVICE_RANGE_NUM_OF_PROFILE;id++) {
        if (MEA_vlanRange_profile[id].valid){
            /*need to check if need to set the ingress port*/


        }
    }


    return MEA_OK;
}


MEA_Bool mea_drv_vlanrange_profile_Conclude(MEA_Unit_t       unit_i)
{

    if (!MEA_SERVICE_RANGE_SUPPORT)
        return MEA_OK;

    /* Free the table */
    if (MEA_vlanRange_profile != NULL) {
        MEA_OS_free(MEA_vlanRange_profile);
        MEA_vlanRange_profile = NULL;
    }

    return MEA_OK;
}

static MEA_Bool mea_drv_vlanrange_profile_find_free(MEA_Unit_t unit_i,MEA_Uint16 *id_io)
{
     MEA_Uint16 id;

    
        for (id=0;id<MEA_SERVICE_RANGE_NUM_OF_PROFILE;id++) {
        if (MEA_vlanRange_profile[id].valid == MEA_FALSE){
           *id_io=id;
           return MEA_TRUE;


        }
    }


    
    return MEA_FALSE;
}

/************************************************************************/
/* create profile                                                       */
/************************************************************************/

MEA_Status mea_drv_Create_vlanrange_profile(MEA_Unit_t unit_i,
                                            MEA_Port_t port,
                                            MEA_Bool   *valid,
                                            MEA_Uint16   *id_io )
{
    

    MEA_API_LOG

        /* Check if support */
        if (!MEA_SERVICE_RANGE_SUPPORT){
           *valid= MEA_FALSE;
           *id_io=0;
            return MEA_OK;   
        }
        /* check parameter */
        


        
            /*find new place*/
            if (mea_drv_vlanrange_profile_find_free(unit_i,id_io)!=MEA_TRUE){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "- vlan range profile failed for port %d \n",port);
                *valid=MEA_FALSE;
                return MEA_ERROR;
            }
        

       /*set the ingress port*/
        *valid=MEA_TRUE;
        MEA_vlanRange_profile[*id_io].valid         = MEA_TRUE;
        MEA_vlanRange_profile[*id_io].num_of_owners = 1;
        MEA_vlanRange_profile[*id_io].portId = port;
        



        return MEA_OK;

}


MEA_Status mea_drv_vlanRange_Mapping_profile(MEA_Unit_t unit_i,
                                             MEA_Port_t port,
                                             MEA_Bool   *valid,
                                             MEA_Uint16   *id_io )
{


  return   mea_drv_Create_vlanrange_profile(unit_i,port,valid,id_io);


    return MEA_OK;
}


static void  MEA_service_setting_SRV_ON_External_DDR(MEA_Unit_t unit_i, 
                                                     MEA_Service_t serviceId, 
                                                     MEA_SETTING_TYPE_te   type)
{
    MEA_Uint32 i;
    MEA_Uint32 addressDDR;
    MEA_Uint32 count_shift = 0;
    MEA_Service_HW_SRV_Key      HW_SRV_key;
    MEA_Uint32                  retVal[16];
    MEA_Uint32                  count = 0;
    MEA_Uint32 ServiceEntry_val[10];
    MEA_Uint32 Action_val[MEA_ACTION_ADDRES_DDR_MAX_WIDTH];

    MEA_Uint32 val[MEA_SRV_BASE_ADDRES_DDR_MAX_WIDTH];

 //   MEA_Action_HwEntry_dbt hw_action_info;


    MEA_OS_memset(&val, 0, sizeof(val));
    MEA_OS_memset(&Action_val, 0, sizeof(Action_val));
    MEA_OS_memset(&HW_SRV_key, 0, sizeof(HW_SRV_key));



    if (MEA_Service_Table[serviceId].data.valid == MEA_TRUE){
            mea_Service_Bulid_Hash_info(unit_i, &MEA_Service_Table[serviceId].key, &HW_SRV_key);

        count_shift = 0;
        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_PRI_WIDTH,
            HW_SRV_key.Hw_priority,
            &val[0]);
        count_shift += MEA_IF_SRV_PRI_WIDTH;


        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_PRI_TYPE_WIDTH,
            HW_SRV_key.Hw_priType,
            &val[0]);
        count_shift += MEA_IF_SRV_PRI_TYPE_WIDTH;
        if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_SRC_PORT_WIDTH,
                HW_SRV_key.Hw_src_port,
                &val[0]);
            count_shift += MEA_IF_SRV_SRC_PORT_WIDTH;
        }
        else {
            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_SRC_PORT_WIDTH,
                HW_SRV_key.Hw_src_port & 0x7f,
                &val[0]);
            count_shift += MEA_IF_SRV_SRC_PORT_WIDTH;
        }
        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_NET_TAG_WIDTH,
            HW_SRV_key.Hw_net_tag & 0xfffff, /*20bit*/
            &val[0]);
        count_shift += MEA_IF_SRV_NET_TAG_WIDTH;


        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_SUB_TYPE_WIDTH,
            (HW_SRV_key.Hw_sub_protocol_type),
            &val[0]);
        count_shift += MEA_IF_SRV_SUB_TYPE_WIDTH;



        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_L2_TYPE_WIDTH,
            HW_SRV_key.Hw_L2_protocol_type,
            &val[0]);
        count_shift += MEA_IF_SRV_L2_TYPE_WIDTH;



        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_NET1_TAG_WIDTH,
            (HW_SRV_key.Hw_inner_netTag_from_value & 0xffffff),
            &val[0]);

        count_shift += (MEA_IF_SRV_NET1_TAG_WIDTH);


        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_IPV6_TAG_WIDTH,
            (HW_SRV_key.HW_Ipv6msb_tag & 0x7ffffff),
            &val[0]);

        count_shift += (MEA_IF_SRV_IPV6_TAG_WIDTH);





        MEA_OS_insert_value(95,
            1,
            1,
            &val[0]);
        count_shift += 95 + 1;



        /* Key context, action  */
        MEA_OS_memset(&ServiceEntry_val, 0, sizeof(ServiceEntry_val));

        mea_ServiceEntry_buildHWData(unit_i,
            0,
            0,
            &MEA_Service_Table[serviceId],
            mea_ServiceEntry_calc_numof_regs(0, 0), /*num of reg*/
            &ServiceEntry_val[0]);

        for (i = 0; i <=6 ; i++){              /* 0 :  7 */
            val[3 + i] = ServiceEntry_val[i];
        }

        mea_action_Get_hw_info(unit_i, MEA_ACTION_TBL_SERVICE_ACTIONS_EXTERNAL_START_INDEX + MEA_Service_Table[serviceId].Sid_External, &Action_val[0]);

        for (i = 0; i <=5; i++){              /* only 6regs*/
            val[10 + i] = Action_val[i];       /*  offset 8 -> 10*/
        }
	

    }

    /*write to SERVICE DB on DDR */
    addressDDR = MEA_SRV_BASE_ADDRES_DDR_START + (MEA_Service_Table[serviceId].hashaddress_External << 6);
    MEA_DRV_WriteDDR_DATA_BLOCK(unit_i, serviceId, addressDDR, &val[0], MEA_SRV_BASE_ADDRES_DDR_WIDTH, DDR_Service_EXT);
    if (type == MEA_SETTING_TYPE_DELETE) {
        //Read from DDR see if we get
        MEA_OS_memset(&retVal[0], 0, sizeof(retVal));
        
        MEA_DDR_Lock();
        MEA_DB_DDR_HW_ACCESS_ENABLE
            if (MEA_OS_Device_DATA_DDR_RW(MEA_MODULE_DATA_DDR, 0,/*0 read 1- write */
                addressDDR,
                MEA_ADDRES_DDR_BYTE*MEA_SRV_BASE_ADDRES_DDR_WIDTH,
                &retVal[0]
            ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR MEA_OS_Device_DATA_DDR_RW  Read from DDR \n");
                MEA_DB_DDR_HW_ACCESS_DISABLE
                    MEA_DDR_Unlock();
                return ;
            }
        MEA_DB_DDR_HW_ACCESS_DISABLE
            MEA_DDR_Unlock();
        count = 0;
        for(i=0;i < (MEA_Uint32)MEA_SRV_BASE_ADDRES_DDR_WIDTH; i++)
            { 
                if (retVal[i] == 0) {
                    count++;
                }
            }
        if (count != MEA_SRV_BASE_ADDRES_DDR_WIDTH) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "DATA_DDR_R is not clear addressDDR 0x%08x \n", addressDDR);
            for (i = 0; i < (MEA_Uint32)MEA_SRV_BASE_ADDRES_DDR_WIDTH; i++)
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Data[%d] = 0x%08X\n", retVal[i]);
            }

        }

    }


}


static void MEA_Service_rollback(MEA_Unit_t unit_i, MEA_Service_t serviceId)
{

    if (MEA_Service_Table[serviceId].en_Sid_Internal){
        MEA_CLEAR_SID(MEA_Service_Internal_BIT, MEA_Service_Table[serviceId].Sid_Internal);/*rollback*/
        MEA_Service_Table[serviceId].en_Sid_Internal = MEA_FALSE;
    }
    
    if (MEA_SERVICES_EXTERNAL_SUPPORT){
        if (MEA_Service_Table[serviceId].en_Sid_External){
            MEA_CLEAR_SID(MEA_Service_External_BIT, MEA_Service_Table[serviceId].Sid_External);

            if (MEA_IS_SET_SID(MEA_Service_External_Hash_BIT, MEA_Service_Table[serviceId].hashaddress_External) == MEA_TRUE)
                 MEA_CLEAR_SID(MEA_Service_External_Hash_BIT, MEA_Service_Table[serviceId].hashaddress_External);
        }
    }

   

    MEA_Service_Table[serviceId].data.valid = MEA_FALSE;/*rollback*/
    MEA_OS_memset(&(MEA_Service_Table[serviceId]), 0, sizeof(MEA_Service_Table[serviceId]));


}


/************************************************************************/
/*                                                                      */
/************************************************************************/

MEA_Uint32 MEA_API_Get_NumOf_Service(MEA_Unit_t unit_i, MEA_Uint8 serviceType)
{
    
    MEA_Uint32 Sid;
    MEA_Uint32 count=0;
    MEA_Bool   exist;
    if (serviceType == MEA_SRV_MODE_REGULAR){
        for (Sid = 1; Sid < MEA_MAX_NUM_OF_SERVICES_INTERNAL; Sid++)
        {
            MEA_API_IsExist_Service_ById(MEA_UNIT_0, Sid, &exist);
            if (exist == MEA_FALSE)
                continue;
            count++;
        }
    }
    else {
        for (Sid = MEA_MAX_NUM_OF_SERVICES_INTERNAL; Sid < MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW; Sid++)
        {
            MEA_API_IsExist_Service_ById(MEA_UNIT_0, Sid, &exist);
            if (exist == MEA_FALSE)
                continue;
            count++;
        }


    }

    return count;
}
