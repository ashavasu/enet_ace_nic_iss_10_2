/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_drv_common.h"

#include "mea_if_service_drv.h"
#include "mea_PortState_drv.h"
#include "enet_xPermission_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_PORT_STATE_HASH_TABLE_SIZE (1024) 
#define MEA_PORT_STATE_HASH_FUNC(M_port,M_vlan) \
    (((M_vlan) & 0x03ff) ^ \
     (((M_vlan) >> 3) | (M_port)))

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
#define MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(func_name)           \
MEA_Status func_name  ( MEA_Unit_t                   unit_i,       \
                        MEA_Global_STP_mode_te                   new_mode_i,  \
				        MEA_Global_STP_mode_te                   old_mode_i)



typedef MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION((*mea_drv_PortState_Global_mode_state_func_t));

typedef struct {
	MEA_Port_t port;
	MEA_Uint16 vlanId;
} MEA_PortState_Key_dbt;

typedef struct {
	MEA_PortState_te state;
} MEA_PortState_Data_dbt;



typedef struct mea_PortState_HashEntry_dbs {
	MEA_PortState_Key_dbt        key;
	MEA_PortState_Data_dbt       data;
	struct mea_PortState_HashEntry_dbs *next;
} mea_PortState_HashEntry_dbt , *mea_PortState_HashEntry_dbp;






/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
mea_PortState_HashEntry_dbp MEA_PortState_HashTable[MEA_PORT_STATE_HASH_TABLE_SIZE];



/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Do_NONE);
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Disable_To_Stp);
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Stp_to_Disable);
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Disable_To_MStp);
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_MStp_to_Disable);
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Stp_to_MStp);
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_MStp_to_Stp);



mea_drv_PortState_Global_mode_state_func_t   MEA_drv_PortState_Global_mode_state_table[] = 
/* Index ,                         function  */ 
/*          MSTP ,STP     Disable                */
/*     0 ,  0    , 0     , 0   */{ &MEA_drv_PortState_Global_state_Do_NONE,
/*     1 ,  0    , 0     , 1   */  &MEA_drv_PortState_Global_state_Do_NONE,
/*     2 ,  0    , 1     , 0   */  &MEA_drv_PortState_Global_state_Disable_To_Stp,
/*     3 ,  0    , 1     , 1   */  &MEA_drv_PortState_Global_state_Stp_to_Disable,
/*     4 ,  1    , 0     , 0   */  &MEA_drv_PortState_Global_state_Disable_To_MStp,
/*     5 ,  1    , 0     , 1   */  &MEA_drv_PortState_Global_state_MStp_to_Disable,
/*     6 ,  1    , 1     , 0   */  &MEA_drv_PortState_Global_state_Stp_to_MStp,
/*     7 ,  1    , 1     , 1   */  &MEA_drv_PortState_Global_state_MStp_to_Stp};


MEA_Status mea_drv_Init_PortState(MEA_Unit_t unit)
{
   MEA_OS_memset(&(MEA_PortState_HashTable[0]),0,sizeof(MEA_PortState_HashTable));
   
   return MEA_OK;


}

MEA_Status mea_drv_Conclude_PortState(MEA_Unit_t unit)
{
    mea_PortState_HashEntry_dbt *hashEntry;
    MEA_Uint32   hashIndex;

    for (hashIndex=0;
         hashIndex<MEA_NUM_OF_ELEMENTS(MEA_PortState_HashTable);
         hashIndex++) {
        while(MEA_PortState_HashTable[hashIndex] != NULL) {
              hashEntry=MEA_PortState_HashTable[hashIndex];
            MEA_PortState_HashTable[hashIndex] = hashEntry->next;
            MEA_OS_free(hashEntry);
        }
    }
    return MEA_OK;
}



MEA_Status MEA_API_Set_PortState(MEA_Unit_t unit_i,
                                 MEA_Port_t port_i,
                                 MEA_Uint16 vlan_i, 
                                 MEA_PortState_te state_i,
                                 ENET_QueueId_t  QueueId_i)
{
    MEA_Uint16 vlan;
    MEA_Uint32 hashIndex;
    mea_PortState_HashEntry_dbt *hashEntry;
    MEA_PortState_Key_dbt key;
    MEA_PortState_te previous_state=0;

    
    if(globalSTP_Mode == MEA_GLOBAL_STP_MODE_DISABLE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - The global mode type of stp is DISABLE \n",
                          __FUNCTION__,port_i);
        return MEA_ERROR;
    }
	
    if (!MEA_API_Get_IsPortValid(port_i, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown port_i %d \n",
                          __FUNCTION__,port_i);
        return MEA_ERROR;
    }

    if ((vlan_i > MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID) && 
        (vlan_i != MEA_PORT_STATE_VLAN_DONT_CARE)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown vlan_i %d 0..%d or 0x%04x for don't care\n",
                          __FUNCTION__,vlan_i,
                          MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID,
                          MEA_PORT_STATE_VLAN_DONT_CARE);
        return MEA_ERROR;
    }
     if(ENET_IsValid_Queue(unit_i,
 						  QueueId_i,
						  ENET_FALSE)==MEA_FALSE){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - ENET_IsValid_Queue =   \n",
						   __FUNCTION__,QueueId_i);
		return MEA_ERROR; 
	}

    switch (state_i) {
    case MEA_PORT_STATE_DISCARD:
    case MEA_PORT_STATE_LEARN:
    case MEA_PORT_STATE_FORWARD:
        break;
    case MEA_PORT_STATE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown state_i %d \n",
                          __FUNCTION__,state_i);
        return MEA_ERROR;
    }

    if (vlan_i == MEA_PORT_STATE_VLAN_DONT_CARE) {
        vlan = MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID + 1;
    } else {
        vlan = vlan_i;
    }


    hashIndex = MEA_PORT_STATE_HASH_FUNC(port_i,vlan);
    hashEntry=MEA_PortState_HashTable[hashIndex];
    MEA_OS_memset(&key,0,sizeof(key));
    key.port = port_i;
    key.vlanId =vlan;
    while(hashEntry) {
        if (MEA_OS_memcmp(&key,
                          &(hashEntry->key),
						  sizeof(key)) == 0) {
		    break;
		}
        hashEntry = hashEntry->next;
    }
    if (hashEntry != NULL) {
       previous_state=hashEntry->data.state;
        
		hashEntry->data.state = state_i;
	} else {
	   hashEntry = MEA_OS_malloc(sizeof(*hashEntry));
	   if (hashEntry == NULL) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Allocate hash entry failed \n",__FUNCTION__);
	       return MEA_ERROR;
	   }
	   MEA_OS_memset(hashEntry,0,sizeof(*hashEntry));
	   MEA_OS_memcpy(&(hashEntry->key),&key,sizeof(hashEntry->key));
	   hashEntry->data.state = state_i;
       hashEntry->next = MEA_PortState_HashTable[hashIndex];
       MEA_PortState_HashTable[hashIndex] = hashEntry;
	}
       /*** start egress************************************************/
       // TBD need to check if the cluster id is part of egress port.
       //------------------------------------------------------------
     if (enet_xPermission_UpdateByPortState(unit_i,
									        QueueId_i,
											state_i) != MEA_OK) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_xPermission_UpdateByPortState failed  \n",
                          __FUNCTION__);
         return MEA_ERROR;
     }
     /***start ingress***********************************************/
     if(previous_state != state_i){
         if(mea_drv_Service_ReUpdateByPortAndVlan(unit_i,port_i,vlan_i,state_i) !=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Service_ReUpdateByPortAndVlan failed  \n",
                              __FUNCTION__);
             return MEA_ERROR;
         }
     }
#if 0
     if(MEA_API_Delete_CAM_ByPortAndVlan(unit_i,port_i,vlan_i) !=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Delete_CAM_ByPortAndVlan failed  \n",
                          __FUNCTION__);
         return MEA_ERROR;
     }

#endif
   


     


     
     return MEA_OK;
}

MEA_Status MEA_API_Get_PortState(MEA_Unit_t unit_i,
                                 MEA_Port_t port_i,
                                 MEA_Uint16 vlan_i, 
                                 MEA_PortState_te*  state_o)
{
    MEA_Uint32 hashIndex;
    mea_PortState_HashEntry_dbt *hashEntry;
    MEA_PortState_Key_dbt key;

    if(globalSTP_Mode == MEA_GLOBAL_STP_MODE_DISABLE){
        
        return MEA_OK;
    }
	
    if (!MEA_API_Get_IsPortValid(port_i, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown port_i %d \n",
                          __FUNCTION__,port_i);
        return MEA_ERROR;
    }

    if ((vlan_i > MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID) && 
        (vlan_i != MEA_PORT_STATE_VLAN_DONT_CARE)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown vlan_i %d 0..%d or 0x%04x for don't care\n",
                          __FUNCTION__,vlan_i,
                          MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID,
                          MEA_PORT_STATE_VLAN_DONT_CARE);
        return MEA_ERROR;
    }

    if (state_o) {
        MEA_Uint16 vlan;
        if (vlan_i == MEA_PORT_STATE_VLAN_DONT_CARE) {
            vlan = MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID + 1;
        } else {
            vlan = vlan_i;
        }

		hashIndex = MEA_PORT_STATE_HASH_FUNC(port_i,vlan);
        hashEntry=MEA_PortState_HashTable[hashIndex];
        MEA_OS_memset(&key,0,sizeof(key));
        key.port = port_i;
        key.vlanId =vlan;
        while(hashEntry) {
            if (MEA_OS_memcmp(&key,
                              &(hashEntry->key),
				     		  sizeof(key)) == 0) {
		        break;
		    }
            hashEntry = hashEntry->next;
        }
	    if (hashEntry != NULL) {
		    *state_o = hashEntry->data.state;
		} else {
            *state_o = MEA_PORT_STATE_FORWARD; /* T.B.D according to globalState */
		}
    }

    return MEA_OK;
}


MEA_Status MEA_API_Delete_CAM_ByPortAndVlan(MEA_Unit_t unit_i, MEA_Port_t port_i, MEA_Uint16 vlanId_i){


    MEA_Uint32 seconds;
    MEA_Bool   admin_status;
    MEA_Uint32 seconds_save;
    MEA_Bool   admin_status_save;
    
    if(MEA_API_Get_SE_Aging  (unit_i,&seconds_save,&admin_status_save)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," %s - MEA_API_Get_SE_Aging failed \n",__FUNCTION__);
        return MEA_ERROR;
    }

    seconds=0;
    admin_status=MEA_TRUE;
    if(MEA_API_Set_SE_Aging(unit_i,seconds,admin_status)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_Set_SE_Aging failed (for aging 0)\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(MEA_API_Set_SE_Aging(unit_i,seconds_save,admin_status_save)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_Set_SE_Aging failed return back\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    
    
    
    return MEA_OK;
}




MEA_Status mea_dev_PortState_set_Global_Mode(MEA_Unit_t unit_i,MEA_Global_STP_mode_te  New_mode_i,MEA_Global_STP_mode_te  old_mode_i)
{
    MEA_Uint32 state_index=0;

    if(old_mode_i == MEA_GLOBAL_STP_MODE_DISABLE){
        if(New_mode_i == MEA_GLOBAL_STP_MODE_DISABLE){
            state_index=1;
        }
        if(New_mode_i == MEA_GLOBAL_STP_MODE_STP){
            state_index=2;
        }
        if(New_mode_i == MEA_GLOBAL_STP_MODE_MSTP){
            state_index=4;
        }
    
    }
    if(old_mode_i == MEA_GLOBAL_STP_MODE_STP){
       if(New_mode_i == MEA_GLOBAL_STP_MODE_DISABLE){
            state_index=3;
       }
       if(New_mode_i == MEA_GLOBAL_STP_MODE_MSTP){
            state_index=6;
        }

    }

    if(old_mode_i == MEA_GLOBAL_STP_MODE_MSTP){
        if(New_mode_i == MEA_GLOBAL_STP_MODE_DISABLE){
            state_index=5;
        }
        if(New_mode_i == MEA_GLOBAL_STP_MODE_STP){
            state_index=7;
        }
    }

    
    /* Call to the function according to the state */
    if (MEA_drv_PortState_Global_mode_state_table[state_index](unit_i,
                                             New_mode_i,
  			                                 old_mode_i) != MEA_OK) {
	    MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - Error: MEA_drv_PortState_Global_mode_state_table failed index %d \n",
						   __FUNCTION__,
						   state_index);
	   return MEA_ERROR;
	}
    
    
    
    return MEA_OK;
}


/*1,0*/
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Do_NONE)
{
return MEA_OK;
}
/*2*/
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Disable_To_Stp)
{
return MEA_OK;
}

/*3*/
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Stp_to_Disable)
{
return MEA_OK;
}
/*4*/
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Disable_To_MStp)
{
MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - failed currently we not support move from mode DISABLE (%d) to mode MSTP(%d)\n",
						   __FUNCTION__,
						   new_mode_i,old_mode_i);
    return MEA_OK;
}
/*5*/
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_MStp_to_Disable)
{
    return MEA_OK;
    
}
/*6*/
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_Stp_to_MStp)
{
MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - failed currently we not support move from mode STP (%d) to mode MSTP(%d)\n",
						   __FUNCTION__,
						   new_mode_i,old_mode_i);
    return MEA_ERROR;
}
/*7*/
MEA_DRV_PORTSTP_GLOBAL_STATE_FUNCTION(MEA_drv_PortState_Global_state_MStp_to_Stp)
{
   MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - failed currently we not support move from mode MSTP(%d) to mode STP(%d)\n",
						   __FUNCTION__,
						   new_mode_i,old_mode_i);
    return MEA_ERROR;
}




