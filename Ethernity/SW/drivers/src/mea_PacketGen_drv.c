/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/






/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_if_regs.h"
#include "mea_swe_ip_drv.h"
#include "mea_globals_drv.h"
#include "mea_PacketGen_drv.h"
/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
/*#define MEA_ANALYZER_SW_CALC_AVG*/	


#define MEA_PACKET_GEN_MAX_PACKET_LEN_SUPPORT  (16384) //2046
#define MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET (8)
#define MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET_TYPE2 (16)


#define MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1  8  

#define MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE2 16

#define MEA_PACKET_GEN_MAX_LOOP              (31)

#define MEA_ANALYZER_MAX_NUM_OF_COUNTERS 16
#define MEA_ANALYZER_SUPPORT_NUM_OF_COUNTERS 11


#define MEA_PACKETGEN_SEQ_TYPE_1_LOCATION_OFFSET 18
#define MEA_PACKETGEN_SEQ_TYPE_2_LOCATION_OFFSET 22
#define MEA_PACKETGEN_SEQ_TYPE_3_LOCATION_OFFSET 26


typedef enum{
      MEA_DATATYPE_NO_CRC_0000  = 0,
      MEA_DATATYPE_WITH_CRC_0000= 1,
      MEA_DATATYPE_NO_CRC_PRBS  = 2, 
      MEA_DATATYPE_WITH_CRC_PRBS= 3, /*not support*/
      MEA_DATATYPE_NO_CRC_FFFF  = 4,
      MEA_DATATYPE_WITH_CRC_FFFF= 5,
      MEA_DATATYPE_NO_CRC_AA55  = 6,
      MEA_DATATYPE_WITH_CRC_AA55= 7

}MEA_DATATYPE_PacketGen_t;



#define MEA_PACKET_GEN_PROF_HEADER_LENGTH_WIDTH    8
#define MEA_PACKET_GEN_PROF_SEG_STAM_ENABLE_WIDTH  1
#define MEA_PACKET_GEN_PROF_SEG_STAM_VALUE_WIDTH   2
#define MEA_PACKET_GEN_PROF_FORCE_ERROR_PRBS_WIDTH 1
#define MEA_PACKET_GEN_PROF_STREAM_TYPE_WIDTH       3






/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct {
 
    MEA_Uint8  header [MEA_PACKET_GEN_MAX_HEADER_LEN];
    MEA_Uint32 nextTime;  /* offset 7 bit 31 0*/
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 gepTime   : 30;
    MEA_Uint32 pad       :  2;
#else
    MEA_Uint32 pad       :  2;
    MEA_Uint32 gepTime   : 30;
#endif

    union {
        struct{
            /* index 0*/
 
            /* index 0*/
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint8     active                : 1;
            MEA_Uint8     prof_ptr              : 4; 
            MEA_Uint8     datatype              : 3;
#else
            MEA_Uint8     datatype              : 3;
            MEA_Uint8     prof_ptr              : 4; 
            MEA_Uint8     active                : 1;
#endif

            /* index 1*/
            MEA_Uint8      packesToSendMSB      ;

            /* index 2*/
            MEA_Uint8      packesToSendLSB      ;
            /* index 3*/
            MEA_Uint8      packesLen_Start_MSB   ;

            /* index 4*/
            MEA_Uint8      packesLen_Start_LSB   ;
    
            /* index 5*/
            MEA_Uint8      packesLen_End_MSB    ;
            
            /* index 6*/
            MEA_Uint8      packesLen_End_LSB    ;
    
            /* index 7*/
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint8     nextStream            : 4;
            MEA_Uint8     packet_Error          : 1;
            MEA_Uint8     seq_mode              : 1;
            MEA_Uint8     packetLen             : 2;
#else
            MEA_Uint8     packetLen             : 2;
            MEA_Uint8     seq_mode              : 1;
            MEA_Uint8     packet_Error          : 1;
            MEA_Uint8     nextStream            : 4;
#endif 


        }val;
        MEA_Uint8 reg[MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1];
    }ctl;

} mea_PacketGen_drv_hwEntry_type1_dbt;

typedef struct {
 
    MEA_Uint8  header [MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2];
    MEA_Uint32 nextTime;  /* offset 15 bit 31 0*/
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 gepTime   : 30;
    MEA_Uint32 pad       :  2;
#else
    MEA_Uint32 pad       :  2;
    MEA_Uint32 gepTime   : 30;
#endif

    union {
        struct{
            /* index 0*/
 
            /* index 0*/
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint8     active                : 1;
            MEA_Uint8     prof_ptr              : 4; 
            MEA_Uint8     datatype              : 3;
#else
            MEA_Uint8     datatype              : 3;
            MEA_Uint8     prof_ptr              : 4; 
            MEA_Uint8     active                : 1;
#endif

            /* index 1*/
            MEA_Uint8      packesToSendMSB      ;

            /* index 2*/
            MEA_Uint8      packesToSendLSB      ;
            /* index 3*/
            MEA_Uint8      packesLen_Start_MSB   ;

            /* index 4*/
            MEA_Uint8      packesLen_Start_LSB   ;
    
            /* index 5*/
            MEA_Uint8      packesLen_End_MSB    ;
            
            /* index 6*/
            MEA_Uint8      packesLen_End_LSB    ;
    
            /* index 7*/
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint8     nextStream            : 4;
            MEA_Uint8     packet_Error          : 1;
            MEA_Uint8     seq_mode              : 1;
            MEA_Uint8     packetLen             : 2;
#else
            MEA_Uint8     packetLen             : 2;
            MEA_Uint8     seq_mode              : 1;
            MEA_Uint8     packet_Error          : 1;
            MEA_Uint8     nextStream            : 4;
#endif 
           /* index 8*/
           MEA_Uint8 temp8;
           /* index 9*/
           MEA_Uint8 temp9;
           /* index 10*/
           MEA_Uint8 temp10;
           /* index 11*/
           MEA_Uint8 temp11;
           /* index 12*/
           MEA_Uint8 temp12;
           /* index 13*/
           MEA_Uint8 temp13;
           /* index 14*/
           MEA_Uint8 temp14;
           /* index 15*/
           MEA_Uint8 temp15;

        }val;
        MEA_Uint8 reg[MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE2];
    }ctl;

} mea_PacketGen_drv_hwEntry_type2_dbt;



typedef struct { 
    struct {
        MEA_Bool valid;
        
    } dbt_private;
    MEA_PacketGen_Entry_dbt dbt_public;
} mea_PacketGen_drv_entry_dbt;

typedef struct {
    MEA_Bool valid;
    MEA_Uint32 num_of_owners;
    MEA_PacketGen_drv_Profile_info_entry_dbt data;
}   mea_PacketGen_drv_Prof_info_entry_dbt;

/*----------------------------------------------------------------------------*/
/*             Typedefs  PRBS                                                     */
/*----------------------------------------------------------------------------*/


typedef struct {
    MEA_Bool                    valid;
    MEA_PRBS_Gen_configure_dbt  data;
}mea_drv_prbs_Gen_configure_dbt;

typedef struct {
    MEA_Bool                             valid;
    MEA_PRBS_analyzer_event_Counters_dbt data;

}mea_drv_prbs_event_Counters_dbt;




/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static MEA_Bool                     MEA_PacketGen_InitDone = MEA_FALSE;
static mea_PacketGen_drv_entry_dbt *MEA_PacketGen_Table    = NULL;
static MEA_db_dbt                   MEA_PacketGen_type1_db       = NULL;
static MEA_db_dbt                   MEA_PacketGen_type2_db       = NULL;
static MEA_Uint32                   MEA_PacketGen_Control_Reg = 0 ;
static mea_PacketGen_drv_Prof_info_entry_dbt *MEA_PacketGen_prof_info_Table    = NULL;



/************************************************************************/
/*  PRBS                                                                    */
/************************************************************************/
static MEA_Uint32 mea_Gen_configure[MEA_PRBS_MAX_OF_STREAMS];
static mea_drv_prbs_Gen_configure_dbt MEA_drv_prbs_Gen_info_Table[MEA_PRBS_MAX_OF_STREAMS];
static mea_drv_prbs_event_Counters_dbt MEA_drv_prbs_Counters_Table[MEA_PRBS_MAX_OF_STREAMS];


/*----------------------------------------------------------------------------*/
/*             Global variables for Analyzer                                  */
/*----------------------------------------------------------------------------*/

static MEA_Bool                    MEA_Analyzer_InitDone =MEA_FALSE;
static MEA_Counters_Analyzer_dbt   *MEA_Counters_ANALYZER_Table = NULL ;
static MEA_Analyzer_configure_dbt   *MEA_Configure_ANALYZER_Table = NULL ;


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

#if 1  /* profile_info */

static MEA_Bool mea_PacketGen_prof_info_drv_find_free(MEA_Unit_t       unit_i, 
                                                       MEA_PacketGen_profile_info_t       *id_io)
 {
    MEA_PacketGen_t i;

    for(i=0;i< MEA_PACKETGEN_MAX_PROFILE; i++) {
        if (MEA_PacketGen_prof_info_Table[i].valid == MEA_FALSE){
            *id_io=i; 
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;
 
 }


 
static MEA_Status mea_PacketGen_prof_info_drv_UpdateHwEntry(MEA_funcParam arg1,
                                                            MEA_funcParam arg2,
                                                            MEA_funcParam arg3,
                                                            MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
	//MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )((long)arg2);
	//MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )((long)arg3);
    MEA_PacketGen_drv_Profile_info_entry_dbt * entry= (MEA_PacketGen_drv_Profile_info_entry_dbt * )arg4;
    
    
    

    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    count_shift=0;

    MEA_OS_insert_value(count_shift,
                            MEA_PACKET_GEN_PROF_HEADER_LENGTH_WIDTH,
                            entry->header_length ,
                            &val[0]);
    count_shift+=MEA_PACKET_GEN_PROF_HEADER_LENGTH_WIDTH;
    
    MEA_OS_insert_value(count_shift,
                            MEA_PACKET_GEN_PROF_SEG_STAM_ENABLE_WIDTH,
                            entry->seq_stamping_enable ,
                            &val[0]);
    count_shift+=MEA_PACKET_GEN_PROF_SEG_STAM_ENABLE_WIDTH;

     MEA_OS_insert_value(count_shift,
                            MEA_PACKET_GEN_PROF_SEG_STAM_VALUE_WIDTH,
                            entry->seq_offset_type ,
                            &val[0]);
    count_shift+=MEA_PACKET_GEN_PROF_SEG_STAM_VALUE_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_PACKET_GEN_PROF_FORCE_ERROR_PRBS_WIDTH,
                            entry->forceErrorPrbs ,
                            &val[0]);
    count_shift+=MEA_PACKET_GEN_PROF_FORCE_ERROR_PRBS_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_PACKET_GEN_PROF_STREAM_TYPE_WIDTH,
        entry->stream_type,
        &val[0]);
    count_shift+=MEA_PACKET_GEN_PROF_STREAM_TYPE_WIDTH;
  

    

    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
       MEA_API_WriteReg(unit_i,
                        MEA_IF_WRITE_DATA_REG(i),
                        val[i],
                        MEA_MODULE_IF);  
    }
   
   
    return MEA_OK;
}


static MEA_Status mea_PacketGen_prof_info_drv_UpdateHw(MEA_Unit_t                  unit_i,
                                            MEA_PacketGen_profile_info_t             id_i,
                                            MEA_PacketGen_drv_Profile_info_entry_dbt    *new_entry_pi,
                                            MEA_PacketGen_drv_Profile_info_entry_dbt    *old_entry_pi)
{
    
    MEA_db_HwUnit_t       hwUnit;
    
    ENET_ind_write_t      ind_write;
    
    
    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    /* check if we have any changes */
    if ((old_entry_pi) &&
        (MEA_OS_memcmp(new_entry_pi,
                       old_entry_pi,
                       sizeof(*new_entry_pi))== 0) ) { 
       return MEA_OK;
    }
    

    
    /* Prepare ind_write structure */
    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_PROF_INFO;
    ind_write.tableOffset   = id_i; 
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg     = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_PacketGen_prof_info_drv_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)id_i);
    ind_write.funcParam4 = (MEA_funcParam)(new_entry_pi);


    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                              __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
    }






    /* Return to caller */
    return MEA_OK;
}

 
 static MEA_Bool mea_PacketGen_prof_info_drv_IsRange(MEA_Unit_t     unit_i,
                                                   MEA_PacketGen_t id_i){
    
    if ((id_i) >= MEA_PACKETGEN_MAX_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

static MEA_Status mea_PacketGen_prof_info_drv_IsExist(MEA_Unit_t     unit_i,
                                                   MEA_PacketGen_profile_info_t id_i,
                                                   MEA_Bool *exist){
    
    
    if(exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    (*exist)=MEA_FALSE;
    
    if (mea_PacketGen_prof_info_drv_IsRange(unit_i,id_i) == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    
    if(MEA_PacketGen_prof_info_Table[id_i].valid == MEA_TRUE){
        (*exist)=MEA_TRUE;
    }

    return MEA_OK;
}

static MEA_Status mea_PacketGen_prof_info_drv_check_parameters(MEA_Unit_t       unit_i,
                                                       MEA_PacketGen_drv_Profile_info_entry_dbt      *entry_pi)
 {

     if(entry_pi->header_length > MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The parameter header_length is out of range\n",__FUNCTION__); 
        return MEA_ERROR;
     }
     if((entry_pi->seq_stamping_enable == MEA_TRUE) &&
         ((entry_pi->seq_offset_type  == MEA_PACKETGEN_SEQ_TYPE_NONE)      ) ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The parameter seq_offset_value support only (18,22,26)\n",__FUNCTION__); 
        return MEA_ERROR;
     }
#if 0
     if(entry_pi->PRBS_Type >= MEA_PACKET_GEN_PRBS_TYPE_LAST){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The parameter PRBS_Type is out of range\n",__FUNCTION__); 
        return MEA_ERROR;
     }
#endif
     if(entry_pi->header_length < 20){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The minimum header_length is 20\n",__FUNCTION__); 
        return MEA_ERROR;
     }
     if(entry_pi->stream_type == MEA_PACKETGEN_STREAM_TYPE_LAST){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The parameter stream_type is not support\n",__FUNCTION__); 
        return MEA_ERROR;
     }
     if((entry_pi->seq_stamping_enable == MEA_TRUE)){
         switch (entry_pi->seq_offset_type){
             case MEA_PACKETGEN_SEQ_TYPE_1:
                 if(((entry_pi->header_length )  < (MEA_PACKETGEN_SEQ_TYPE_1_LOCATION_OFFSET +4-1))){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The header_length %d + 4 byte from the seq offset %d  \n",__FUNCTION__,
                     entry_pi->header_length,MEA_PACKETGEN_SEQ_TYPE_1_LOCATION_OFFSET); 
                    return MEA_ERROR;
                 }
                 break;
             case MEA_PACKETGEN_SEQ_TYPE_2:
                 if(((entry_pi->header_length )  < (MEA_PACKETGEN_SEQ_TYPE_2_LOCATION_OFFSET +4 -1))){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The header_length %d + 4 byte from the seq offset %d  \n",__FUNCTION__,
                     entry_pi->header_length,MEA_PACKETGEN_SEQ_TYPE_2_LOCATION_OFFSET); 
                    return MEA_ERROR;
                 }
                 break;
             case MEA_PACKETGEN_SEQ_TYPE_3:
                 if(((entry_pi->header_length )  < (MEA_PACKETGEN_SEQ_TYPE_3_LOCATION_OFFSET +4 -1))){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The header_length %d + 4 byte from the seq offset %d  \n",__FUNCTION__,
                     entry_pi->header_length,MEA_PACKETGEN_SEQ_TYPE_3_LOCATION_OFFSET); 
                    return MEA_ERROR;
                 }
                 break;
             default:
                 
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The header_length (18,22,26)\n",__FUNCTION__); 
            return MEA_ERROR;
            break;
         }


     }
    

    return MEA_OK;
 }




static MEA_Status mea_PacketGen_prof_info_drv_add_owner(MEA_Unit_t       unit_i ,
                                                        MEA_PacketGen_profile_info_t       id_i)
{
    MEA_Bool exist;

    if(mea_PacketGen_prof_info_drv_IsExist(unit_i,id_i, &exist)!=MEA_OK){
    return MEA_ERROR;
    }
    
    MEA_PacketGen_prof_info_Table[id_i].num_of_owners++;

    return MEA_OK;
}
static MEA_Status mea_PacketGen_prof_info_drv_delete_owner(MEA_Unit_t       unit_i ,
                                                        MEA_PacketGen_profile_info_t       id_i)
{
    MEA_Bool exist;

    if(mea_PacketGen_prof_info_drv_IsExist(unit_i,id_i, &exist)!=MEA_OK){
    return MEA_ERROR;
    }
    
    if(MEA_PacketGen_prof_info_Table[id_i].num_of_owners==1){
        MEA_PacketGen_prof_info_Table[id_i].valid=MEA_FALSE;
        MEA_PacketGen_prof_info_Table[id_i].num_of_owners=0;
        return MEA_OK;

    }
    if(MEA_PacketGen_prof_info_Table[id_i].num_of_owners < 1){
        MEA_PacketGen_prof_info_Table[id_i].valid=MEA_FALSE;

    }
    
    MEA_PacketGen_prof_info_Table[id_i].num_of_owners--;

    return MEA_OK;
}
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API  PacketGen Profile_info >                         */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Create_PacketGen_Profile_info>                    */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_PacketGen_Profile_info(MEA_Unit_t              unit_i,
                          MEA_PacketGen_drv_Profile_info_entry_dbt      *entry_pi,
                          MEA_PacketGen_profile_info_t                  *id_io)
 {
 
    MEA_Bool exist;

     /* check parameter */
     if(mea_PacketGen_prof_info_drv_check_parameters(unit_i,entry_pi) !=MEA_OK){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_PacketGen_drv_Profile_info_check_parameters failed \n",__FUNCTION__); 
        return MEA_ERROR;
    }

        /* Look for stream id */
    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID){
        /*check if the id exist*/
        if (mea_PacketGen_prof_info_drv_IsExist(unit_i,*id_io,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_PacketGen_drv_prof_info_IsExist failed (*id_io=%d\n",
                              __FUNCTION__,*id_io);
            return MEA_ERROR;
        }
        if (exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - *Id_io %d is already exist\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
        }

    }else {
        /*find new place*/
        if (mea_PacketGen_prof_info_drv_find_free(unit_i,id_io)!=MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - mea_PacketGen_prof_info_drv_find_free failed\n",__FUNCTION__);
            return MEA_ERROR;
        }
    }

   if( mea_PacketGen_prof_info_drv_UpdateHw(unit_i,
                                        *id_io,
                                         entry_pi,
                                         NULL)!= MEA_OK){
   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - mea_PacketGen_Prof_info_UpdateHw failed for id %d\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
   }

   MEA_PacketGen_prof_info_Table[*id_io].valid         = MEA_TRUE;
   MEA_PacketGen_prof_info_Table[*id_io].num_of_owners = 1;
   MEA_OS_memcpy(&(MEA_PacketGen_prof_info_Table[*id_io].data),entry_pi,sizeof(MEA_PacketGen_prof_info_Table[*id_io].data));


     
     return MEA_OK;
 }

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <MEA_API_Set_PacketGen_Profile_info_Entry>                        */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_PacketGen_Profile_info_Entry(MEA_Unit_t          unit_i,
                           MEA_PacketGen_profile_info_t                 id_i,
                           MEA_PacketGen_drv_Profile_info_entry_dbt     *entry_pi)
{

    
    
    if(id_i >= MEA_PACKETGEN_MAX_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The  Profile info id = %d out of range\n",__FUNCTION__,id_i); 
        return MEA_ERROR;
    }
    if(MEA_PacketGen_prof_info_Table[id_i].valid !=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The  Profile info id =%d is not valid\n",__FUNCTION__,id_i); 
        return MEA_ERROR;
    }
    
    /* check parameters*/
    if(mea_PacketGen_prof_info_drv_check_parameters(unit_i,entry_pi)!= MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_PacketGen_drv_Profile_info_check_parameters filed \n",__FUNCTION__); 
        return MEA_ERROR;
    }

    if( mea_PacketGen_prof_info_drv_UpdateHw(unit_i,
                                        id_i,
                                         entry_pi,
                                         &MEA_PacketGen_prof_info_Table[id_i].data)!= MEA_OK){
   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - mea_PacketGen_Prof_info_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
            return MEA_ERROR;
   }

    MEA_OS_memcpy(&(MEA_PacketGen_prof_info_Table[id_i].data),entry_pi,sizeof(MEA_PacketGen_prof_info_Table[id_i].data));

    
    
    return MEA_OK;
}





/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Delete_PacketGen_Profile_info_Entry>                           */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Delete_PacketGen_Profile_info_Entry(MEA_Unit_t                    unit_i,
                                                       MEA_PacketGen_profile_info_t  id_i)
{
    
    MEA_Bool                    exist;
   
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check if support */
    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }

    /*check if the id exist*/
    if (mea_PacketGen_prof_info_drv_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_PacketGen_drv_stream_IsExist failed (id_i=%d\n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - stream %d not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
    
    if(MEA_PacketGen_prof_info_Table[id_i].num_of_owners >1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - need to delete all the reference to this profile\n",__FUNCTION__,id_i);
    return MEA_ERROR;
    }

    /*Write to Hw All zero*/

    MEA_OS_memset(&MEA_PacketGen_prof_info_Table[id_i].data,0,sizeof(MEA_PacketGen_prof_info_Table[0].data));

    MEA_PacketGen_prof_info_Table[id_i].valid = MEA_FALSE;
    MEA_PacketGen_prof_info_Table[id_i].num_of_owners=0;
    
    /* Return to caller */
    return MEA_OK;
}

MEA_Status MEA_API_Get_PacketGen_Profile_info_Entry(MEA_Unit_t           unit_i,
                               MEA_PacketGen_profile_info_t              id_i,
                               MEA_PacketGen_drv_Profile_info_entry_dbt  *entry_po)
{

    MEA_Bool    exist; 
     
    MEA_API_LOG
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check if support */
    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }

    /*check if the id exist*/
    if (mea_PacketGen_prof_info_drv_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_PacketGen_drv_stream_IsExist failed (id_i=%d\n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - stream %d not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if(entry_po==NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_po is null\n",__FUNCTION__);
        return MEA_ERROR;
    
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    /* Copy to caller structure */
    MEA_OS_memset(entry_po,0,sizeof(*entry_po));
    MEA_OS_memcpy(entry_po ,
        &(MEA_PacketGen_prof_info_Table[id_i].data),
                  sizeof(*entry_po));
    
    /* Return to caller */
    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_GetFirst_PacketGen_Profile_info_Entry>            */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_GetFirst_PacketGen_Profile_info_Entry(MEA_Unit_t                  unit_i,
                                            MEA_PacketGen_profile_info_t            *id_o,
                                            MEA_PacketGen_drv_Profile_info_entry_dbt    *entry_po, 
                                            MEA_Bool                   *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
     if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    
    if (id_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ( (*id_o)=0;
          (*id_o) < MEA_PACKETGEN_MAX_PROFILE; 
          (*id_o)++ ) {
        if (MEA_PacketGen_prof_info_Table[(*id_o)].valid == MEA_TRUE) {
            if (found_o) {
                *found_o= MEA_TRUE;
            }

            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                              &MEA_PacketGen_prof_info_Table[(*id_o)].data,
                              sizeof(*entry_po));
            }
            break;
        }
    }

    
    return MEA_OK;
}

 
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_GetNext_PacketGen_Profile_info_Entry>             */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_GetNext_PacketGen_Profile_info_Entry(MEA_Unit_t                      unit_i,
                                           MEA_PacketGen_profile_info_t                 *id_io,
                                           MEA_PacketGen_drv_Profile_info_entry_dbt     *entry_po, 
                                           MEA_Bool                                     *found_o)
{

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    if (id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ( (*id_io)++;
          (*id_io) < MEA_PACKETGEN_MAX_PROFILE; 
          (*id_io)++ ) {
        if(MEA_PacketGen_prof_info_Table[(*id_io)].valid == MEA_TRUE) {
            if (found_o) {
                *found_o= MEA_TRUE;
            }
            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                    &MEA_PacketGen_prof_info_Table[(*id_io)].data,
                              sizeof(*entry_po));
            }
            break;
        }
    }
   
    
    return MEA_OK;
}

 

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_IsExist_PacketGen_Profile_info>                   */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_PacketGen_Profile_info(MEA_Unit_t                    unit_i,
                                           MEA_PacketGen_profile_info_t              Id_i,
                                           MEA_Bool                     *exist_o)
{
    MEA_API_LOG
    
    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    
    
    return mea_PacketGen_prof_info_drv_IsExist(unit_i,Id_i,exist_o);
}

/*-----------------------End----PacketGen_Profile_info-----------------------*/
#endif

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_PacketGen_drv_stream_IsRange>                         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Bool mea_PacketGen_drv_stream_IsRange(MEA_Unit_t     unit_i,
                                                  MEA_PacketGen_t id_i){
    
    if ((id_i) >= MEA_PACKETGEN_MAX_STREAMS){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_PacketGen_drv_stream_IsExist>                         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_PacketGen_drv_stream_IsExist(MEA_Unit_t     unit_i,
                                                   MEA_PacketGen_t id_i,
                                                   MEA_Bool *exist){
    
    
    if(exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    (*exist)=MEA_FALSE;
    
    if (mea_PacketGen_drv_stream_IsRange(unit_i,id_i) == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    
    if(MEA_PacketGen_Table[id_i].dbt_private.valid == MEA_TRUE){
        (*exist)=MEA_TRUE;
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_PacketGen_drv_find_free_Stream>                       */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Bool mea_PacketGen_drv_find_free_Stream(MEA_PacketGen_t *id_io ,MEA_Uint16 type)
{
    MEA_PacketGen_t i;

    
    if(type==0){
        for(i=0;i< MEA_PACKETGEN_MAX_STREAMS_TYPE_1; i++) {
            if (MEA_PacketGen_Table[i].dbt_private.valid == MEA_FALSE){
                *id_io=i; 
                return MEA_TRUE;
            }
        }
    } else {
        if(MEA_PACKETGEN_SUPPORT_TYPE_2){
        for(i=MEA_PACKETGEN_MAX_STREAMS_TYPE_1;i< MEA_PACKETGEN_MAX_STREAMS; i++) {
            if (MEA_PacketGen_Table[i].dbt_private.valid == MEA_FALSE){
                *id_io=i; 
                return MEA_TRUE;
            }
        }
        }

    }
    return MEA_FALSE;
}


/*--------------------------------------------------------------------------*/
/*                                                                          */
/*            <mea_PacketGen_BuildHwEntry>                                  */
/*                                                                          */
/*--------------------------------------------------------------------------*/
MEA_Status mea_PacketGen_BuildHwEntryType1(MEA_Unit_t                  unit_i,
                                      MEA_PacketGen_t             id_i,
                                      MEA_PacketGen_Entry_dbt    *entry_pi,
                                      mea_PacketGen_drv_hwEntry_type1_dbt  *hwEntry_po)
{

    //MEA_Uint32 i;
    MEA_uint64 clock, sysClk, gepTime;
    MEA_Uint16 Startvalue;
    MEA_PacketGen_drv_Profile_info_entry_dbt  entry_profile;
    MEA_Uint32 seq_offset_value=MEA_PACKETGEN_SEQ_TYPE_1_LOCATION_OFFSET;

    /* Reset hwEntry */
    MEA_OS_memset(hwEntry_po,0,sizeof(*hwEntry_po));
    MEA_OS_memset(&entry_profile,0,sizeof(entry_profile));

    /* Build HwEntry header information */
    MEA_OS_memcpy (&hwEntry_po->header,
                    entry_pi->header.data,
                    sizeof(hwEntry_po->header));
    
#if 0   // the hw will add the PATTERN 
    if ((entry_pi->data.pattern_type == MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE) && 
        (entry_pi->header.length      < MEA_PACKET_GEN_MAX_HEADER_LEN         )  ) {
        
        MEA_Uint16 val;
        i=entry_pi->header.length;
        val= MEA_OS_htons(entry_pi->data.pattern_value.s);
        if (i%2 !=0) {
              hwEntry_po->header[i] = val & 0x00ff;
              i++;
          }
        while(i< MEA_PACKET_GEN_MAX_HEADER_LEN){
          
          hwEntry_po->header[i  ] = (val & 0xFF00)>> 8;    //entry_pi->data.pattern_value.b[0];
          hwEntry_po->header[i+1] = (val & 0x00FF) ;       //entry_pi->data.pattern_value.b[1];

          i += 2;
        }
        
        
    }
#endif
   
    if(entry_pi->profile_info.valid){
    if(MEA_API_Get_PacketGen_Profile_info_Entry(unit_i,
        entry_pi->profile_info.id,
                               &entry_profile)!=MEA_OK){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Get_PacketGen_Profile_info_Entry failed\n",
                                  __FUNCTION__);
         return MEA_ERROR;
    
    }
    }
    if(entry_profile.seq_stamping_enable == MEA_TRUE ){
   //         hwEntry_po->ctl.val.Seq_ID_stamp = MEA_TRUE;
        if(entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_1){
            seq_offset_value = MEA_PACKETGEN_SEQ_TYPE_1_LOCATION_OFFSET;
        }
        if(entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_2){
            seq_offset_value = MEA_PACKETGEN_SEQ_TYPE_2_LOCATION_OFFSET;
        }
        if(entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_3){
            seq_offset_value = MEA_PACKETGEN_SEQ_TYPE_3_LOCATION_OFFSET;
        }
        hwEntry_po->header[seq_offset_value   ] = (MEA_Uint8)((entry_pi->seqIdStamp.start_value & 0xff000000)>>24);
        hwEntry_po->header[seq_offset_value +1] = (MEA_Uint8)((entry_pi->seqIdStamp.start_value & 0x00ff0000)>>16);
        hwEntry_po->header[seq_offset_value +2] = (MEA_Uint8)((entry_pi->seqIdStamp.start_value & 0x0000ff00)>>8);
        hwEntry_po->header[seq_offset_value +3] = (MEA_Uint8)((entry_pi->seqIdStamp.start_value & 0x000000ff)>>0);
            
            //hwEntry_po->header[26  ] = (MEA_Uint8)((entry_pi->seqIdStamp.end_Value & 0xff000000)>>24);
            //hwEntry_po->header[27  ] = (MEA_Uint8)((entry_pi->seqIdStamp.end_Value & 0x00ff0000)>>16);
            //hwEntry_po->header[28  ] = (MEA_Uint8)((entry_pi->seqIdStamp.end_Value & 0x0000ff00)>>8);
            //hwEntry_po->header[29  ] = (MEA_Uint8)((entry_pi->seqIdStamp.end_Value & 0x000000ff)>>0);
    }
    

    clock=sysClk = MEA_GLOBAL_SYS_Clock;
    //clock=MEA_systemClock_calc(unit_i,sysClk);

    if(entry_pi->shaper.shaper_enable== MEA_TRUE){
    if(entry_pi->shaper.shaper_info.CIR== 0){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - CIR (pps) cet be zero\n",
                                  __FUNCTION__,entry_pi->data.pattern_value,id_i);
         return MEA_ERROR;
    }
     gepTime = clock /(entry_pi->shaper.shaper_info.CIR);
     if(gepTime > 0x3fffffff){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - CIR (pps) is out range for gepTime\n",
                                  __FUNCTION__,entry_pi->data.pattern_value,id_i);
         return MEA_ERROR;
     }

     hwEntry_po->gepTime = (MEA_Uint32) gepTime ;//& 0x3fffffff;
    } else{
      /* set the gepTime to default*/
      hwEntry_po->gepTime=(MEA_Uint32) (clock /100);
    }
    /* Build HwEntry control information */
//    hwEntry_po->ctl.val.header_length=(MEA_Uint8)((entry_pi->header.length - 20)/2);          /* currently  not support the len*/
    if(entry_pi->data.pattern_type == MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE){
   
        switch(entry_pi->data.pattern_value.s){
           case (MEA_PACKET_GEN_DATA_PATTERN_VALUE_0000):
                hwEntry_po->ctl.val.datatype = MEA_DATATYPE_NO_CRC_0000 + entry_pi->data.withCRC;
                break;
            case (MEA_PACKET_GEN_DATA_PATTERN_VALUE_FFFF):
                hwEntry_po->ctl.val.datatype = MEA_DATATYPE_NO_CRC_FFFF + entry_pi->data.withCRC;
                break;
            case (MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55):
                hwEntry_po->ctl.val.datatype = MEA_DATATYPE_NO_CRC_AA55 + entry_pi->data.withCRC;
                break;
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown pattern_value (0x%04x) (id_i=%d)\n",
                                  __FUNCTION__,entry_pi->data.pattern_value,id_i);
                return MEA_ERROR;
        }
    } else {
        if(entry_pi->data.pattern_type == MEA_PACKET_GEN_DATA_PATTERN_TYPE_PRBS){
           hwEntry_po->ctl.val.datatype = MEA_DATATYPE_NO_CRC_PRBS + entry_pi->data.withCRC;
        }else {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown pattern_type  = %d)\n",
                                  __FUNCTION__,entry_pi->data.pattern_type);
                return MEA_ERROR;
        }

    }
    hwEntry_po->ctl.val.packesToSendMSB= (MEA_Uint8)((entry_pi->control.numOf_packets &0xff00)>>8);
    hwEntry_po->ctl.val.packesToSendLSB= (MEA_Uint8)((entry_pi->control.numOf_packets &0x00ff)>>0);
    switch (entry_pi->length.type) {
    case MEA_PACKET_GEN_LENGTH_TYPE_DECREMENT:
    case MEA_PACKET_GEN_LENGTH_TYPE_RANDOM:
    case MEA_PACKET_GEN_LENGTH_TYPE_INCREMANT:
    case MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE:
         hwEntry_po->ctl.val.packesLen_Start_MSB = (MEA_Uint8)((entry_pi->length.start & 0xff00)>>8);
         hwEntry_po->ctl.val.packesLen_Start_LSB = (MEA_Uint8)((entry_pi->length.start & 0x00ff)>>0);
         hwEntry_po->ctl.val.packesLen_End_MSB  = (MEA_Uint8)((entry_pi->length.end & 0xff00)>>8);
         hwEntry_po->ctl.val.packesLen_End_LSB  = (MEA_Uint8)((entry_pi->length.end & 0x00ff)>>0);
         break;
    
    case MEA_PACKET_GEN_LENGTH_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown length.type %d (id_i=%d)\n",
                          __FUNCTION__,entry_pi->length.type,id_i);
        return MEA_ERROR;
    }
    
    if(((entry_pi->length.type == MEA_PACKET_GEN_LENGTH_TYPE_DECREMENT) || 
        (entry_pi->length.type == MEA_PACKET_GEN_LENGTH_TYPE_INCREMANT)   )  ){
        
        Startvalue= (hwEntry_po->ctl.val.packesLen_Start_MSB<<8) | 
                    (hwEntry_po->ctl.val.packesLen_Start_LSB);
        
        if(Startvalue > 2048){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Startvalue can't be > 2047 at mode increment decremant \n",
                          __FUNCTION__,entry_pi->length.type,id_i);
        return MEA_ERROR;
        }
        
        hwEntry_po->ctl.val.packesToSendMSB  = (hwEntry_po->ctl.val.packesLen_Start_MSB & 0x07);
        hwEntry_po->ctl.val.packesToSendMSB |= (entry_pi->control.loop<<3);

        hwEntry_po->ctl.val.packesToSendLSB= hwEntry_po->ctl.val.packesLen_Start_LSB;
        
    }
    
    
    hwEntry_po->ctl.val.packetLen              = entry_pi->length.type;
    hwEntry_po->ctl.val.seq_mode               = entry_pi->control.next_packetGen_valid;
    hwEntry_po->ctl.val.packet_Error           = entry_pi->control.Packet_error;
    hwEntry_po->ctl.val.nextStream             = (MEA_Uint8)entry_pi->control.next_packetGen_id;
    hwEntry_po->ctl.val.prof_ptr               =  (MEA_Uint8)(entry_pi->profile_info.id & 0x0f);
    hwEntry_po->ctl.val.active                 =  (MEA_Uint8)(entry_pi->control.active & 0x01);


    /* Return to caller */
    return MEA_OK;
}


MEA_Status mea_PacketGen_BuildHwEntryType2(MEA_Unit_t                  unit_i,
                                      MEA_PacketGen_t             id_i,
                                      MEA_PacketGen_Entry_dbt    *entry_pi,
                                      mea_PacketGen_drv_hwEntry_type2_dbt  *hwEntry_po)
{

    //MEA_Uint32 i;
    MEA_uint64 clock, sysClk, gepTime;
    MEA_Uint16 Startvalue;
    MEA_PacketGen_drv_Profile_info_entry_dbt  entry_profile;
    MEA_Uint32 seq_offset_value=MEA_PACKETGEN_SEQ_TYPE_1_LOCATION_OFFSET;

    /* Reset hwEntry */
    MEA_OS_memset(hwEntry_po,0,sizeof(*hwEntry_po));
    MEA_OS_memset(&entry_profile,0,sizeof(entry_profile));
  
    /* Build HwEntry header information */
    MEA_OS_memcpy (&hwEntry_po->header,
                    entry_pi->header.data,
                    sizeof(hwEntry_po->header));

   

    
#if 0   // the hw will add the PATTERN 
    if ((entry_pi->data.pattern_type == MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE) && 
        (entry_pi->header.length      < MEA_PACKET_GEN_MAX_HEADER_LEN         )  ) {
        
        MEA_Uint16 val;
        i=entry_pi->header.length;
        val= MEA_OS_htons(entry_pi->data.pattern_value.s);
        if (i%2 !=0) {
              hwEntry_po->header[i] = val & 0x00ff;
              i++;
          }
        while(i< MEA_PACKET_GEN_MAX_HEADER_LEN){
          
          hwEntry_po->header[i  ] = (val & 0xFF00)>> 8;    //entry_pi->data.pattern_value.b[0];
          hwEntry_po->header[i+1] = (val & 0x00FF) ;       //entry_pi->data.pattern_value.b[1];

          i += 2;
        }
        
        
    }
#endif
    if(entry_pi->profile_info.valid){
    if(MEA_API_Get_PacketGen_Profile_info_Entry(unit_i,
        entry_pi->profile_info.id,
                               &entry_profile)!=MEA_OK){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_Get_PacketGen_Profile_info_Entry failed\n",
                                  __FUNCTION__);
         return MEA_ERROR;
    
    }
    }
    if(entry_profile.seq_stamping_enable == MEA_TRUE ){
        if(entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_1){
            seq_offset_value = MEA_PACKETGEN_SEQ_TYPE_1_LOCATION_OFFSET;
        }
        if(entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_2){
            seq_offset_value = MEA_PACKETGEN_SEQ_TYPE_2_LOCATION_OFFSET;
        }
        if(entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_3){
            seq_offset_value = MEA_PACKETGEN_SEQ_TYPE_3_LOCATION_OFFSET;
        }
        hwEntry_po->header[seq_offset_value   ] = (MEA_Uint8)((entry_pi->seqIdStamp.start_value & 0xff000000)>>24);
        hwEntry_po->header[seq_offset_value +1] = (MEA_Uint8)((entry_pi->seqIdStamp.start_value & 0x00ff0000)>>16);
        hwEntry_po->header[seq_offset_value +2] = (MEA_Uint8)((entry_pi->seqIdStamp.start_value & 0x0000ff00)>>8);
        hwEntry_po->header[seq_offset_value +3] = (MEA_Uint8)((entry_pi->seqIdStamp.start_value & 0x000000ff)>>0);
         
        
            
            //hwEntry_po->header[26  ] = (MEA_Uint8)((entry_pi->seqIdStamp.end_Value & 0xff000000)>>24);
            //hwEntry_po->header[27  ] = (MEA_Uint8)((entry_pi->seqIdStamp.end_Value & 0x00ff0000)>>16);
            //hwEntry_po->header[28  ] = (MEA_Uint8)((entry_pi->seqIdStamp.end_Value & 0x0000ff00)>>8);
            //hwEntry_po->header[29  ] = (MEA_Uint8)((entry_pi->seqIdStamp.end_Value & 0x000000ff)>>0);
    }
    

    clock=sysClk = MEA_GLOBAL_SYS_Clock;
    //clock=MEA_systemClock_calc(unit_i,sysClk);

    if(entry_pi->shaper.shaper_enable== MEA_TRUE){
    if(entry_pi->shaper.shaper_info.CIR== 0){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - CIR (pps) cet be zero\n",
                                  __FUNCTION__,entry_pi->data.pattern_value,id_i);
         return MEA_ERROR;
    }
     gepTime = clock /(entry_pi->shaper.shaper_info.CIR);
     if(gepTime > 0x3fffffff){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - CIR (pps) is out range for gepTime\n",
                                  __FUNCTION__,entry_pi->data.pattern_value,id_i);
         return MEA_ERROR;
     }

     hwEntry_po->gepTime = (MEA_Uint32)gepTime ;//& 0x3fffffff;
    } else{
      /* set the gepTime to default*/
      hwEntry_po->gepTime=(MEA_Uint32)clock /100;
    }
    /* Build HwEntry control information */
//    hwEntry_po->ctl.val.header_length=(MEA_Uint8)((entry_pi->header.length - 20)/2);          /* currently  not support the len*/
    if(entry_pi->data.pattern_type == MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE){
   
        switch(entry_pi->data.pattern_value.s){
           case (MEA_PACKET_GEN_DATA_PATTERN_VALUE_0000):
                hwEntry_po->ctl.val.datatype = MEA_DATATYPE_NO_CRC_0000 + entry_pi->data.withCRC;
                break;
            case (MEA_PACKET_GEN_DATA_PATTERN_VALUE_FFFF):
                hwEntry_po->ctl.val.datatype = MEA_DATATYPE_NO_CRC_FFFF + entry_pi->data.withCRC;
                break;
            case (MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55):
                hwEntry_po->ctl.val.datatype = MEA_DATATYPE_NO_CRC_AA55 + entry_pi->data.withCRC;
                break;
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown pattern_value (0x%04x) (id_i=%d)\n",
                                  __FUNCTION__,entry_pi->data.pattern_value,id_i);
                return MEA_ERROR;
        }
    } else {
        if(entry_pi->data.pattern_type == MEA_PACKET_GEN_DATA_PATTERN_TYPE_PRBS){
           hwEntry_po->ctl.val.datatype = MEA_DATATYPE_NO_CRC_PRBS + entry_pi->data.withCRC;
        }else {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown pattern_type  = %d)\n",
                                  __FUNCTION__,entry_pi->data.pattern_type);
                return MEA_ERROR;
        }

    }
    hwEntry_po->ctl.val.packesToSendMSB= (MEA_Uint8)((entry_pi->control.numOf_packets &0xff00)>>8);
    hwEntry_po->ctl.val.packesToSendLSB= (MEA_Uint8)((entry_pi->control.numOf_packets &0x00ff)>>0);
    switch (entry_pi->length.type) {
    case MEA_PACKET_GEN_LENGTH_TYPE_DECREMENT:
    case MEA_PACKET_GEN_LENGTH_TYPE_RANDOM:
    case MEA_PACKET_GEN_LENGTH_TYPE_INCREMANT:
    case MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE:
         hwEntry_po->ctl.val.packesLen_Start_MSB = (MEA_Uint8)((entry_pi->length.start & 0xff00)>>8);
         hwEntry_po->ctl.val.packesLen_Start_LSB = (MEA_Uint8)((entry_pi->length.start & 0x00ff)>>0);
         hwEntry_po->ctl.val.packesLen_End_MSB  = (MEA_Uint8)((entry_pi->length.end & 0xff00)>>8);
         hwEntry_po->ctl.val.packesLen_End_LSB  = (MEA_Uint8)((entry_pi->length.end & 0x00ff)>>0);
         break;
    
    case MEA_PACKET_GEN_LENGTH_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown length.type %d (id_i=%d)\n",
                          __FUNCTION__,entry_pi->length.type,id_i);
        return MEA_ERROR;
    }
    
    if(((entry_pi->length.type == MEA_PACKET_GEN_LENGTH_TYPE_DECREMENT) || 
        (entry_pi->length.type == MEA_PACKET_GEN_LENGTH_TYPE_INCREMANT)   )  ){
        
        Startvalue= (hwEntry_po->ctl.val.packesLen_Start_MSB<<8) | 
                    (hwEntry_po->ctl.val.packesLen_Start_LSB);
        
        if(Startvalue > 2048){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Startvalue can't be > 2047 at mode increment decremant \n",
                          __FUNCTION__,entry_pi->length.type,id_i);
        return MEA_ERROR;
        }
        
        hwEntry_po->ctl.val.packesToSendMSB  = (hwEntry_po->ctl.val.packesLen_Start_MSB & 0x07);
        hwEntry_po->ctl.val.packesToSendMSB |= (entry_pi->control.loop<<3);

        hwEntry_po->ctl.val.packesToSendLSB= hwEntry_po->ctl.val.packesLen_Start_LSB;
        
    }
    
    
    hwEntry_po->ctl.val.packetLen              = entry_pi->length.type;
    hwEntry_po->ctl.val.seq_mode               = entry_pi->control.next_packetGen_valid;
    hwEntry_po->ctl.val.packet_Error           = entry_pi->control.Packet_error;
    hwEntry_po->ctl.val.nextStream             = (MEA_Uint8)entry_pi->control.next_packetGen_id;
    hwEntry_po->ctl.val.prof_ptr               = (MEA_Uint8)(entry_pi->profile_info.id & 0x0f);
    hwEntry_po->ctl.val.active                 = (MEA_Uint8)(entry_pi->control.active & 0x01);

    /* Return to caller */
    return MEA_OK;
}



/*--------------------------------------------------------------------------*/
/*                                                                          */
/*            <mea_PacketGen_UpdateHwEntryType1>                            */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static MEA_Status mea_PacketGen_UpdateHwEntryType1(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_db_HwUnit_t            hwUnit_i = (MEA_db_HwUnit_t)((long)arg1);
    MEA_db_HwId_t              hwId_i = (MEA_db_HwId_t)((long)arg2);
    MEA_Uint32                 offset_i = (MEA_Uint32)((long)arg3);
    mea_PacketGen_drv_hwEntry_type1_dbt *hwEntry_pi = (mea_PacketGen_drv_hwEntry_type1_dbt*)arg4;
    MEA_Uint32                 val[ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_NUM_OF_REGS];
    MEA_Uint32                 i=0;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }

    /* Init val */
    
    i = (offset_i * MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET);
    if (offset_i <= 6) {
        val[1] |=  (((MEA_Uint32)((hwEntry_pi->header[i  ]))) << 24);
        val[1] |=  (((MEA_Uint32)((hwEntry_pi->header[i+1]))) << 16);
        val[1] |=  (((MEA_Uint32)((hwEntry_pi->header[i+2]))) <<  8);
        val[1] |=  (((MEA_Uint32)((hwEntry_pi->header[i+3]))) <<  0);
    
        val[0] |=  (((MEA_Uint32)((hwEntry_pi->header[i+4]))) << 24);
        val[0] |=  (((MEA_Uint32)((hwEntry_pi->header[i+5]))) << 16);
        val[0] |=  (((MEA_Uint32)((hwEntry_pi->header[i+6]))) <<  8);
        val[0] |=  (((MEA_Uint32)((hwEntry_pi->header[i+7]))) <<  0);
    } 
    if(offset_i == 7){
      val[0] = 0; /*nextTime NXT_TIME [31:0]*/
      val[1] = hwEntry_pi->gepTime;
    }

    val[2] = hwEntry_pi->ctl.reg     [offset_i];
#if 0  /*debug*/   
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"DBG:ALEX.index %d .val[2]=0x%08x val[1]=0x%08x. val[0]=0x%08x\n",
                                                       offset_i,val[2],val[1],val[0]); 
#endif
    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
       MEA_API_WriteReg(MEA_UNIT_0,
                        MEA_IF_WRITE_DATA_REG(i),
                        val[i],
                        MEA_MODULE_IF);  
    }
   
    /* Update db */
    if (mea_db_Set_Hw_Regs(MEA_UNIT_0,
                           MEA_PacketGen_type1_db,
                           hwUnit_i,
                           hwId_i,
                           MEA_NUM_OF_ELEMENTS(val),
                           val) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Set_Hw_Regs failed (hwUnit=%d,hwId=%d)\n",
                          __FUNCTION__,
                          hwUnit_i,
                          hwId_i);
        return MEA_ERROR;
   }
   
    return MEA_OK;
}



/*--------------------------------------------------------------------------*/
/*                                                                          */
/*            <mea_PacketGen_UpdateHwEntryType2>                            */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static MEA_Status mea_PacketGen_UpdateHwEntryType2(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_db_HwUnit_t            hwUnit_i = (MEA_db_HwUnit_t)((long)arg1);
    MEA_db_HwId_t              hwId_i = (MEA_db_HwId_t)((long)arg2);
    MEA_Uint32                 offset_i = (MEA_Uint32)((long)arg3);
    mea_PacketGen_drv_hwEntry_type2_dbt *hwEntry_pi = (mea_PacketGen_drv_hwEntry_type2_dbt*)arg4;
    MEA_Uint32                 val[ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_NUM_OF_REGS];
    MEA_Uint32                 i=0;

    /* Zero laze the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }

    /* Init val */
    
    i = (offset_i * MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET);
    if (offset_i <= 14) {
        val[1] |=  (((MEA_Uint32)((hwEntry_pi->header[i  ]))) << 24);
        val[1] |=  (((MEA_Uint32)((hwEntry_pi->header[i+1]))) << 16);
        val[1] |=  (((MEA_Uint32)((hwEntry_pi->header[i+2]))) <<  8);
        val[1] |=  (((MEA_Uint32)((hwEntry_pi->header[i+3]))) <<  0);
    
        val[0] |=  (((MEA_Uint32)((hwEntry_pi->header[i+4]))) << 24);
        val[0] |=  (((MEA_Uint32)((hwEntry_pi->header[i+5]))) << 16);
        val[0] |=  (((MEA_Uint32)((hwEntry_pi->header[i+6]))) <<  8);
        val[0] |=  (((MEA_Uint32)((hwEntry_pi->header[i+7]))) <<  0);
    } 
    if(offset_i == 15){
      val[0] = 0; /*nextTime NXT_TIME [31:0]*/
      val[1] = hwEntry_pi->gepTime;
    }

    val[2] = hwEntry_pi->ctl.reg     [offset_i];
#if 0  /*debug*/   
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"DBG:ALEX.index %d .val[2]=0x%08x val[1]=0x%08x. val[0]=0x%08x\n",
                                                       offset_i,val[2],val[1],val[0]); 
#endif
    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
       MEA_API_WriteReg(MEA_UNIT_0,
                        MEA_IF_WRITE_DATA_REG(i),
                        val[i],
                        MEA_MODULE_IF);  
    }
     /* Update db */
    if (mea_db_Set_Hw_Regs(MEA_UNIT_0,
                           MEA_PacketGen_type2_db,
                           hwUnit_i,
                           hwId_i,
                           MEA_NUM_OF_ELEMENTS(val),
                           val) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Set_Hw_Regs failed (hwUnit=%d,hwId=%d)\n",
                          __FUNCTION__,
                          hwUnit_i,
                          hwId_i);
        return MEA_ERROR;
   }
  
   
    return MEA_OK;
}




MEA_Status mea_PacketGen_UpdateHw_Type1_Active(MEA_Unit_t unit_i,
                                               MEA_Uint16 type,
                                                MEA_PacketGen_t id_i,
                                                mea_PacketGen_drv_hwEntry_type1_dbt *hwEntry)
{
    
    ENET_ind_write_t      ind_write;
    MEA_Uint32            offset;
    MEA_db_HwUnit_t       hwUnit;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    /* Prepare ind_write structure */
    ind_write.tableType        = MEA_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_STREAM;
    ind_write.tableOffset      = 0; /* will be update in the loop */
    ind_write.cmdReg           = MEA_IF_CMD_REG;      
    ind_write.cmdMask          = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask       = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg     = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry       = (MEA_FUNCPTR)mea_PacketGen_UpdateHwEntryType1;
    ind_write.funcParam1 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam2       = 0; /* will be update in the loop */
    ind_write.funcParam3       = 0; /* will be update in the loop */
    ind_write.funcParam4 = (MEA_funcParam)(hwEntry);


    /* Update the Hardware for offset 0 only because the active bit is there */
    offset=0 ;
        ind_write.tableOffset = (id_i * MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1)+offset;
        ind_write.funcParam2 = (MEA_funcParam)((long)ind_write.tableOffset);
        ind_write.funcParam3 = (MEA_funcParam)((long)offset);

        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }
   
    
    
    return MEA_OK;
}



MEA_Status mea_PacketGen_UpdateHw_Type2_Active(MEA_Unit_t unit_i,
                                               MEA_Uint16 type,
                                               MEA_PacketGen_t id_i,
                                               mea_PacketGen_drv_hwEntry_type2_dbt *hwEntry2)
{

    ENET_ind_write_t      ind_write;
    MEA_Uint32            offset;
    MEA_db_HwUnit_t       hwUnit;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

        /* Prepare ind_write structure */
    ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_STREAM_TYPE2;
    ind_write.tableOffset      = 0; /* will be update in the loop */
    ind_write.cmdReg           = MEA_IF_CMD_REG;      
    ind_write.cmdMask          = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask       = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg     = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry       = (MEA_FUNCPTR)mea_PacketGen_UpdateHwEntryType2;
    ind_write.funcParam1 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam2       = 0; /* will be update in the loop */
    ind_write.funcParam3       = 0; /* will be update in the loop */
    ind_write.funcParam4 = (MEA_funcParam)(hwEntry2);


    /* Update the Hardware for offset 0 only  */
    offset=0;
        
        ind_write.tableOffset = ((id_i-MEA_PACKETGEN_MAX_STREAMS_TYPE_1) * MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE2)+offset;
        ind_write.funcParam2 = (MEA_funcParam)((long)ind_write.tableOffset);
        ind_write.funcParam3 = (MEA_funcParam)((long)offset);

        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }
    



    return MEA_OK;
}


/*--------------------------------------------------------------------------*/
/*                                                                          */
/*            <mea_PacketGen_UpdateHw>                                      */
/*                                                                          */
/*--------------------------------------------------------------------------*/
MEA_Status mea_PacketGen_UpdateHw(MEA_Unit_t                  unit_i,
                                  MEA_PacketGen_t             id_i,
                                  MEA_Uint16                  type,
                                  MEA_PacketGen_Entry_dbt    *new_entry_pi,
                                  MEA_PacketGen_Entry_dbt    *old_entry_pi)
{
    
    MEA_db_HwUnit_t       hwUnit;
    mea_PacketGen_drv_hwEntry_type1_dbt    hwEntry;
    mea_PacketGen_drv_hwEntry_type2_dbt    hwEntry2;
    MEA_Uint32  value;

    ENET_ind_write_t      ind_write;
    MEA_Uint32            offset;
    
    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    if ((old_entry_pi) &&
        (new_entry_pi->profile_info.headerType != old_entry_pi->profile_info.headerType)) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - headerType can't be change \n",
                          __FUNCTION__,id_i);
       return MEA_ERROR;
    }

    /* check if we have any changes */
    if ((old_entry_pi) &&
        (MEA_OS_memcmp(new_entry_pi,
                       old_entry_pi,
                       sizeof(*new_entry_pi))== 0) ) { 
       return MEA_OK;
    }
    

    if(type == 0){ 
    /* Build hwEntry */
    if (mea_PacketGen_BuildHwEntryType1(unit_i,
                                   id_i,
                                   new_entry_pi,
                                   &hwEntry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_PacketGen_BuildHwEntry failed (id_i=%d)\n",
                          __FUNCTION__,id_i);
        return ENET_ERROR;
    }

     
      hwEntry.ctl.val.active = MEA_FALSE;
    if(mea_PacketGen_UpdateHw_Type1_Active(unit_i,type,id_i,&hwEntry)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_PacketGen_UpdateHw_Type1_Active failed  for id=%d \n",
            __FUNCTION__,id_i);
        return ENET_ERROR;
    }

    if(new_entry_pi->setting.resetPrbs == MEA_TRUE){
#if 0
        value= ((MEA_Uint32)id_i & 0x000003ff);
        value |=1<<10; // mode 0 regular 
        MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_GLOBAL_PACKETGEN_RESRET_PRBS_REG,value,MEA_MODULE_IF);
#else
        value= ((MEA_Uint32)id_i & 0x000003ff);
        value |=0<<10; // mode 0 regular 
        value |=1<<11; //reset
        MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_GLOBAL_PACKETGEN_RESRET_PRBS_REG,value,MEA_MODULE_IF);
#endif
        new_entry_pi->setting.resetPrbs=MEA_FALSE;
    }

    
    /* Prepare ind_write structure */
    ind_write.tableType        = MEA_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_STREAM;
    ind_write.tableOffset      = 0; /* will be update in the loop */
    ind_write.cmdReg           = MEA_IF_CMD_REG;      
    ind_write.cmdMask          = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask       = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg     = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry       = (MEA_FUNCPTR)mea_PacketGen_UpdateHwEntryType1;
    ind_write.funcParam1 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam2       = 0; /* will be update in the loop */
    ind_write.funcParam3       = 0; /* will be update in the loop */
    ind_write.funcParam4 = (MEA_funcParam)(&hwEntry);


    /* Update the Hardware for all offsets of this stream */
    for (offset=0;offset<MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1;offset++) { 
        ind_write.tableOffset = (id_i * MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1)+offset;
        ind_write.funcParam2 = (MEA_funcParam)((long)ind_write.tableOffset);
        ind_write.funcParam3 = (MEA_funcParam)((long)offset);

        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                              __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }
    }




        /* Start the stream after the configuration */
        if (new_entry_pi->control.active) {

            hwEntry.ctl.val.active = MEA_TRUE;
            if(mea_PacketGen_UpdateHw_Type1_Active(unit_i,type,id_i,&hwEntry)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_PacketGen_UpdateHw_Type1_Active failed  for id=%d \n",
                    __FUNCTION__,id_i);
                return ENET_ERROR;
            }

        }
    } else {
/************************************************    
            type 2
**************************************************/
     /* Build hwEntry */
    if (mea_PacketGen_BuildHwEntryType2(unit_i,
                                   id_i,
                                   new_entry_pi,
                                   &hwEntry2) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_PacketGen_BuildHwEntry failed (id_i=%d)\n",
                          __FUNCTION__,id_i);
        return ENET_ERROR;
    }

    hwEntry2.ctl.val.active = MEA_FALSE;
    if( mea_PacketGen_UpdateHw_Type2_Active(unit_i,type,id_i,&hwEntry2) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_PacketGen_UpdateHw_Type2_Active failed  for id=%d \n",
            __FUNCTION__,id_i);
        return ENET_ERROR;
    }

    if(new_entry_pi->setting.resetPrbs == MEA_TRUE){

#if 0
        value= ((MEA_Uint32)(id_i - MEA_PACKETGEN_MAX_STREAMS_TYPE_1)& 0x000003ff);
        value|=1<<10; // mode

        MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_GLOBAL_PACKETGEN_RESRET_PRBS_REG,value,MEA_MODULE_IF);

#else
        value= ((MEA_Uint32)(id_i - MEA_PACKETGEN_MAX_STREAMS_TYPE_1)& 0x000003ff);
        value|=1<<10; // mode
        value|=1<<11; // reset
        MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_GLOBAL_PACKETGEN_RESRET_PRBS_REG,value,MEA_MODULE_IF);
#endif
 
        new_entry_pi->setting.resetPrbs=MEA_FALSE;
    }
    /* Prepare ind_write structure */
    ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_STREAM_TYPE2;
    ind_write.tableOffset      = 0; /* will be update in the loop */
    ind_write.cmdReg           = MEA_IF_CMD_REG;      
    ind_write.cmdMask          = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask       = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg     = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry       = (MEA_FUNCPTR)mea_PacketGen_UpdateHwEntryType2;
    ind_write.funcParam1 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam2       = 0; /* will be update in the loop */
    ind_write.funcParam3       = 0; /* will be update in the loop */
    ind_write.funcParam4 = (MEA_funcParam)(&hwEntry2);


    /* Update the Hardware for all offsets of this stream */
    for (offset=0;offset<MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE2;offset++) { 
                                 //TBD
        ind_write.tableOffset = ((id_i-MEA_PACKETGEN_MAX_STREAMS_TYPE_1) * MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE2)+offset;
        ind_write.funcParam2 = (MEA_funcParam)((long)ind_write.tableOffset);
        ind_write.funcParam3 = (MEA_funcParam)((long)offset);

        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                              __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }
    }



    /* Start the stream after the configuration */
        if (new_entry_pi->control.active) {

            hwEntry2.ctl.val.active = MEA_TRUE;
            if( mea_PacketGen_UpdateHw_Type2_Active(unit_i,type,id_i,&hwEntry2) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_PacketGen_UpdateHw_Type2_Active failed  for id=%d \n",
                    __FUNCTION__,id_i);
                return ENET_ERROR;
            }



        }
    }
    /* Return to caller */
    return MEA_OK;
}


static MEA_Status mea_PacketGen_drv_check_parameters(MEA_Unit_t                 unit_i ,
                                                     MEA_PacketGen_Entry_dbt   *entry_pi)
{
    MEA_Mtu_t maxJumbo;
    MEA_Bool exist;
    MEA_PacketGen_profile_info_t prof_id;
    MEA_PacketGen_drv_Profile_info_entry_dbt  entry_pofile;

    if (!entry_pi) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_pi == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    if(entry_pi->profile_info.valid != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - profile_info.valid must be true\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    if(entry_pi->profile_info.id > MEA_PACKETGEN_MAX_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - used_Profile_Id %d is out of reange %\n",
                          __FUNCTION__ ,entry_pi->profile_info.id,MEA_PACKETGEN_MAX_PROFILE);
        return MEA_ERROR;
    }
    /*
     Check if the profile is is valid 
    */
    prof_id = (MEA_PacketGen_profile_info_t)entry_pi->profile_info.id;
    if(MEA_API_IsExist_PacketGen_Profile_info(unit_i,
                    prof_id ,
                     &exist) != MEA_OK){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_IsExist_PacketGen_Profile_info filed Profile_Id %d\n",
                          __FUNCTION__ ,entry_pi->profile_info.id);
        return MEA_ERROR;
    }
    if(!exist){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Profile_info id %d not exist\n",
                          __FUNCTION__ ,entry_pi->profile_info.id);
        return MEA_ERROR;
    }

    if(MEA_API_Get_PacketGen_Profile_info_Entry(unit_i,
                                (MEA_PacketGen_profile_info_t)  (entry_pi->profile_info.id),
                                       &entry_pofile)!=MEA_OK){
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  MEA_API_Get_PacketGen_Profile_info_Entry failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    
    switch(entry_pi->profile_info.headerType){
        case MEA_PACKET_GEN_HEADER_TYPE_1:
            if(!MEA_PACKETGEN_SUPPORT_TYPE_1){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  headerType MEA_PACKET_GEN_HEADER_TYPE_1 not support\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
            if(entry_pofile.header_length >=16 && entry_pofile.header_length<=56){
            }else{
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  headerType MEA_PACKET_GEN_HEADER_TYPE_1 file because header_length\n",
                          __FUNCTION__);
                return MEA_ERROR;
            }
            break;
       case MEA_PACKET_GEN_HEADER_TYPE_2:
           if(!MEA_PACKETGEN_SUPPORT_TYPE_2){
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  headerType MEA_PACKET_GEN_HEADER_TYPE_2 not support\n",
                   __FUNCTION__);
               return MEA_ERROR;
           }
            if(entry_pofile.header_length >=16 && entry_pofile.header_length<=120){
            }else{
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  headerType MEA_PACKET_GEN_HEADER_TYPE_2 file because header_length\n",
                          __FUNCTION__);
                return MEA_ERROR;
            }
            break;
       case MEA_PACKET_GEN_HEADER_TYPE_LAST:
       default:
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  headerType not support \n",
                          __FUNCTION__);
                return MEA_ERROR;
           break;

    }
    
        


    
    if(entry_pi->timeStamp.type     != MEA_PACKET_GEN_TIME_STAMP_TYPE_NONE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - currently we not support timeStamp \n",__FUNCTION__);
        return MEA_ERROR;
    }
#if 0
    if (entry_pi->header.length < 20){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - header length need to be bigger then %d \n",
                        __FUNCTION__,
                        entry_pi->header.length,
                        20);
        return MEA_ERROR;
    }
    if(entry_pi->header.length > sizeof(entry_pi->header.data)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - header length %d > max value %d \n",
                        __FUNCTION__,
                        entry_pi->header.length,
                        sizeof(entry_pi->header.data));
        return MEA_ERROR;
    }
#endif
    if(entry_pi->data.withCRC == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  currently we not support data.withCRC can't be TRUE \n",__FUNCTION__);
        return MEA_ERROR;
    }

    if( ( entry_pi->data.pattern_type == MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE) && 
        ( entry_pi->data.pattern_value.s != MEA_PACKET_GEN_DATA_PATTERN_VALUE_0000)                &&
        ( entry_pi->data.pattern_value.s != MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55)                && 
        ( entry_pi->data.pattern_value.s != MEA_PACKET_GEN_DATA_PATTERN_VALUE_FFFF)                   ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  DATA_PATTERN wrong  0x%x\n",__FUNCTION__,entry_pi->data.pattern_value);
        return MEA_ERROR;
    
    }
    if(entry_pi->control.Burst_size != 1){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - currently control.Burst size   %d can't be different then 1\n",__FUNCTION__,entry_pi->control.Burst_size);
        return MEA_ERROR;
    }


    if((entry_pi->length.start < 64) || (entry_pi->length.end < 64)){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  start/end  can't be less 64\n",__FUNCTION__);
        return MEA_ERROR;
    }
    maxJumbo=MEA_Platform_GetMaxMTU();
    if((entry_pi->length.start > maxJumbo) || 
        (entry_pi->length.end > maxJumbo)){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  start/end  can't be oversized maxJumbo %d\n",__FUNCTION__,maxJumbo);
        return MEA_ERROR;
    }
    if((entry_pi->length.start > MEA_PACKET_GEN_MAX_PACKET_LEN_SUPPORT) || 
        (entry_pi->length.end > MEA_PACKET_GEN_MAX_PACKET_LEN_SUPPORT)){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - currently packet length  start/end  can't be oversized  %d\n",__FUNCTION__,MEA_PACKET_GEN_MAX_PACKET_LEN_SUPPORT);
        return MEA_ERROR;
    }

    if(entry_pofile.header_length > entry_pi->length.start){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  packet length start %d can't be smallest  profile header length %d \n",__FUNCTION__,
             entry_pi->length.start,entry_pofile.header_length);
        return MEA_ERROR;
    }
    if(entry_pofile.header_length > entry_pi->length.end){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  packet length end %d can't be smallest  profile header length %d \n",__FUNCTION__,
             entry_pi->length.end,entry_pofile.header_length);
        return MEA_ERROR;
    }


    switch(entry_pi->length.type){
        case MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE:
            if(entry_pi->length.start != entry_pi->length.end){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  TYPE_FIX_VALUE start %d != end %d \n",__FUNCTION__,entry_pi->length.start,entry_pi->length.end);
                return MEA_ERROR;
            }
            if(entry_pofile.header_length > entry_pi->length.start){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  %d is less then %d header_length\n",__FUNCTION__,entry_pi->length.start,
                        entry_pofile.header_length);
                return MEA_ERROR;
            }

        break;
        
        case MEA_PACKET_GEN_LENGTH_TYPE_INCREMANT:
             if(entry_pi->length.start >=entry_pi->length.end){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  %s start %d >= end %d \n",__FUNCTION__,
                        (entry_pi->length.type == MEA_PACKET_GEN_LENGTH_TYPE_INCREMANT) ? "TYPE_INCREMANT" : "TYPE_RANDOM",
                                                                         entry_pi->length.start,
                                                                         entry_pi->length.end);
                return MEA_ERROR;
            }
             if(entry_pofile.header_length > entry_pi->length.start){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  %d is less then %d header_length\n",__FUNCTION__,entry_pi->length.start,
                        entry_pofile.header_length);
                return MEA_ERROR;
            }
         break;
        case MEA_PACKET_GEN_LENGTH_TYPE_DECREMENT:
            if(entry_pi->length.start <= entry_pi->length.end){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  TYPE_DECREMENT start %d <= end %d \n",__FUNCTION__,
                                                                                   entry_pi->length.start,
                                                                                   entry_pi->length.end);
                return MEA_ERROR;
            }
            if((entry_pofile.header_length > entry_pi->length.start) || 
                (entry_pofile.header_length > entry_pi->length.end)){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  %d is less then %d header_length \n",__FUNCTION__,entry_pi->length.start,
                        entry_pofile.header_length);
                return MEA_ERROR;
            }
        break;
        case MEA_PACKET_GEN_LENGTH_TYPE_RANDOM:
               if(entry_pi->length.start > entry_pi->length.end){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  %s start %d >= end %d \n",__FUNCTION__,
                        (entry_pi->length.type == MEA_PACKET_GEN_LENGTH_TYPE_INCREMANT) ? "TYPE_INCREMANT" : "TYPE_RANDOM",
                                                                         entry_pi->length.start,
                                                                         entry_pi->length.end);
                return MEA_ERROR;
            }
             if(entry_pofile.header_length > entry_pi->length.start){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  packet length  %d is less then %d header_length\n",__FUNCTION__,entry_pi->length.start,
                        entry_pofile.header_length);
                return MEA_ERROR;
            }
           
        break;
        case MEA_PACKET_GEN_LENGTH_TYPE_LAST:
        default: 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  Length type  %d is out of range\n",__FUNCTION__,entry_pi->length.type);
            return MEA_ERROR;
        break;
    }

    if(entry_pi->control.numOf_packets > MEA_PACKET_GEN_INFINITE_NUMBER_PACKET_TO_SEND){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - control num Of packets can't be %d > %d\n",__FUNCTION__
                                                                  ,entry_pi->control.numOf_packets
                                                                  , MEA_PACKET_GEN_INFINITE_NUMBER_PACKET_TO_SEND);
        return MEA_ERROR;
    }

 

    /* check  loop */
    if((entry_pi->length.type == MEA_PACKET_GEN_LENGTH_TYPE_DECREMENT) || 
        (entry_pi->length.type == MEA_PACKET_GEN_LENGTH_TYPE_INCREMANT)   ){ 
        
         if(entry_pi->control.numOf_packets != 0 ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - at mode length type increment or decrement the numOf_packets need to be zero used the loop field \n",__FUNCTION__);
            return MEA_ERROR;
         }
         if(entry_pi->control.loop > MEA_PACKET_GEN_MAX_LOOP){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - at mode length type increment or decrement the LOOP=%d > max(%d) \n",__FUNCTION__,entry_pi->control.loop,MEA_PACKET_GEN_MAX_LOOP);
            return MEA_ERROR;
         }
    }else{
         if(entry_pi->control.loop != 0){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - at mode length type different (increment or decrement) the LOOP need to be zero\n",__FUNCTION__);
            return MEA_ERROR;
         }

    }
    
    /* check the shaper */
    if(entry_pi->shaper.shaper_enable){
        if(entry_pi->shaper.Shaper_compensation != ENET_SHAPER_COMPENSATION_TYPE_PPACKET){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - Shaper_compensation failed  only COMPENSATION_TYPE_PPACKET mode support \n",__FUNCTION__);
            return MEA_ERROR;
        }
        if(entry_pi->shaper.shaper_Id != 0){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - shaper_Id failed  need to be zero \n",__FUNCTION__);
            return MEA_ERROR;
        }

        if(entry_pi->shaper.shaper_info.CBS != 0){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - CBS failed  need to be zero \n",__FUNCTION__);
            return MEA_ERROR;
        }
        if(entry_pi->shaper.shaper_info.Cell_Overhead != 0){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - Cell_Overhead failed  need to be zero \n",__FUNCTION__);
            return MEA_ERROR;
        }
        if(entry_pi->shaper.shaper_info.overhead != 0){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - overhead failed  need to be zero \n",__FUNCTION__);
            return MEA_ERROR;
        }
        if(entry_pi->shaper.shaper_info.resolution_type != ENET_SHAPER_RESOLUTION_TYPE_PKT){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - CBS failed  need to be zero \n",__FUNCTION__);
            return MEA_ERROR;
        }

        if(entry_pi->shaper.shaper_info.CIR == 0 ){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - CIR failed  can't be zero when shaper enable\n",__FUNCTION__);
            return MEA_ERROR;
        }
    }
    return MEA_OK;
}




MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_PacketGen_num_of_sw_size_func,
                                         ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_LENGTH,
                                         0)

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_PacketGen_num_of_hw_size_func,
                                         ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_LENGTH,
                                         ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_NUM_OF_REGS)

MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_PacketGen_db,
                             MEA_PacketGen_type1_db)

  

MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_PacketGen_type2_num_of_sw_size_func,
                                         ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_LENGTH_TYPE2,
                                         ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_TYPE2_NUM_OF_REGS)


MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_PacketGen_type2_num_of_hw_size_func,
                                         ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_LENGTH_TYPE2,
                                         ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_TYPE2_NUM_OF_REGS)

MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_PacketGen_type2_db,
                             MEA_PacketGen_type2_db)

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Conclude_ServiceRange_Table>                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Conclude_PacketGen(MEA_Unit_t unit_i)
{

    MEA_PacketGen_t id;

    /* If exist any stream then remove it */

    if (MEA_PacketGen_Table != NULL) {
        for (id=0;id<MEA_PACKETGEN_MAX_STREAMS;id++) {
            if(MEA_PacketGen_Table[id].dbt_private.valid){
                if (MEA_API_Delete_PacketGen_Entry(unit_i,id) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - MEA_API_Delete_PacketGen_Entry failed \n",
                                      __FUNCTION__);
                    return MEA_OK;
                }
            }
        }
    }


    
    /* Delete the db */
    if (mea_db_Conclude(unit_i,&MEA_PacketGen_type1_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_dn_Conclude failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    /* Delete the db */
    if (mea_db_Conclude(unit_i,&MEA_PacketGen_type2_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_dn_Conclude failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Free the table */
    if (MEA_PacketGen_Table != NULL) {
        MEA_OS_free(MEA_PacketGen_Table);
        MEA_PacketGen_Table = NULL;
    }

    /* Reset the control reg */
    MEA_PacketGen_Control_Reg =0;

        if (MEA_PacketGen_Table != NULL) {
        for (id=0;id<MEA_PACKETGEN_MAX_PROFILE;id++) {
            if(MEA_PacketGen_prof_info_Table[id].valid){
                if (MEA_API_Delete_PacketGen_Profile_info_Entry(unit_i,id) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - MEA_API_Delete_PacketGen_Profile_info_Entry failed \n",
                                      __FUNCTION__);
                    return MEA_OK;
                }
            }
        }
    }
    /* Reset the init done */
    MEA_PacketGen_InitDone = MEA_FALSE;

    /* Return to the caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Init_PacketGen>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_PacketGen(MEA_Unit_t  unit_i )
{
     
    
    MEA_Uint32 size;
    MEA_PacketGen_t id;
    mea_PacketGen_drv_entry_dbt entry;
    MEA_PacketGen_drv_Profile_info_entry_dbt entry_prof_info;

    if(b_bist_test){
        return MEA_OK;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF PacketGen tables ...\n");
    /* Verify not already init */
    if (MEA_PacketGen_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Already init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    MEA_PacketGen_InitDone = MEA_FALSE;
/********************************************************/
 MEA_OS_memset(&entry_prof_info,0,sizeof(entry_prof_info));
    
 /* Allocate PacketGen Table */
    size = MEA_PACKETGEN_MAX_PROFILE * sizeof(mea_PacketGen_drv_Prof_info_entry_dbt);
    if (size != 0) {
        MEA_PacketGen_prof_info_Table = (mea_PacketGen_drv_Prof_info_entry_dbt*)MEA_OS_malloc(size);
        if (MEA_PacketGen_prof_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_PacketGen_prof_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_PacketGen_prof_info_Table[0]),0,size);
    }
    /* Init db - Hardware shadow */
 
    /* Reset the Hardware to defaults - only if support */
    if (MEA_PACKETGEN_SUPPORT && MEA_PACKETGEN_MAX_PROFILE != 0){
        for (id=0;id<MEA_PACKETGEN_MAX_PROFILE;id++) {
            if (mea_PacketGen_prof_info_drv_UpdateHw(unit_i,
                                       id,
                                       &(entry_prof_info),
                                       NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s -  mea_PacketGen_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                return MEA_ERROR;
            }
           
        }
      

    }





/*****************************************************************/
    /* Allocate PacketGen Table */
    size = MEA_PACKETGEN_MAX_STREAMS * sizeof(mea_PacketGen_drv_entry_dbt);
    if (size != 0) {
        MEA_PacketGen_Table = (mea_PacketGen_drv_entry_dbt*)MEA_OS_malloc(size);
        if (MEA_PacketGen_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_Packetgen_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_PacketGen_Table[0]),0,size);
    }

       
       
    /* Init db - Hardware shadow */
    if (mea_db_Init(unit_i,
                    "PacketGen",
                    mea_drv_Get_PacketGen_num_of_sw_size_func,
                    mea_drv_Get_PacketGen_num_of_hw_size_func,
                    &MEA_PacketGen_type1_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
       if (mea_db_Init(unit_i,
                    "PacketGen_type2",
                    mea_drv_Get_PacketGen_type2_num_of_sw_size_func,
                    mea_drv_Get_PacketGen_type2_num_of_hw_size_func,
                    &MEA_PacketGen_type2_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    
    MEA_OS_memset(&entry,0,sizeof(entry));
    MEA_PacketGen_Control_Reg =0;

    /* Reset the Hardware to defaults - only if support */
    if (MEA_PACKETGEN_SUPPORT){
#if 0 // no need to clear the HW 
        for (id=0;id<MEA_PACKETGEN_MAX_STREAMS_TYPE_1;id++) {
            if (mea_PacketGen_UpdateHw(unit_i,
                                       id,
                                       0,
                                       &( entry.dbt_public ),
                                       NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s -  mea_PacketGen_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                return MEA_ERROR;
            }
           
        }
        if(MEA_PACKETGEN_SUPPORT_TYPE_2){
       for (id=MEA_PACKETGEN_MAX_STREAMS_TYPE_1;
            id<(MEA_PACKETGEN_MAX_STREAMS_TYPE_1+MEA_PACKETGEN_MAX_STREAMS_TYPE_2)
           ;id++) {
            if (mea_PacketGen_UpdateHw(unit_i,
                                       id,
                                       1,
                                       &( entry.dbt_public ),
                                       NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s -  mea_PacketGen_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                return MEA_ERROR;
            }
           
        }
        }
#endif
    }
//     if(mea_IngressPort_Write_Mtu(120, 2048)!= MEA_OK){
//         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
//                               "%s -  mea_IngressPort_Write_Mtu failed for port 120\n",__FUNCTION__);
//                 return MEA_ERROR;
//     }



    


    MEA_PacketGen_InitDone = MEA_TRUE;

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_PacketGen>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_PacketGen(MEA_Unit_t  unit_i)
{

    MEA_PacketGen_t id;

    if (!MEA_PACKETGEN_SUPPORT){
        return MEA_OK;
    }

    for (id=0;id<MEA_PACKETGEN_MAX_PROFILE;id++) {
        if (MEA_PacketGen_prof_info_Table[id].valid){
            if(mea_PacketGen_prof_info_drv_UpdateHw(unit_i,
                                            id,
                                            &(MEA_PacketGen_prof_info_Table[id].data),
                                            NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s -  mea_PacketGen_prof_info_drv_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                return MEA_ERROR;
            }
        }
    }
    /* We reinit via updateHw and not via db , because updateHw make more process then simple shaode
       for example stop/start the stream , and confDone */
    for (id=0;id<MEA_PACKETGEN_MAX_STREAMS_TYPE_1;id++) {
        if (MEA_PacketGen_Table[id].dbt_private.valid) {
            if (mea_PacketGen_UpdateHw(unit_i,
                                       id,
                                       0,
                                       &( MEA_PacketGen_Table[id].dbt_public ),
                                       NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s -  mea_PacketGen_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                return MEA_ERROR;
            }
        }
    }
    for (id=MEA_PACKETGEN_MAX_STREAMS_TYPE_1;id<MEA_PACKETGEN_MAX_STREAMS;id++) {
        if (MEA_PacketGen_Table[id].dbt_private.valid) {
            if (mea_PacketGen_UpdateHw(unit_i,
                                       id,
                                       1,
                                       &( MEA_PacketGen_Table[id].dbt_public ),
                                       NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s -  mea_PacketGen_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                return MEA_ERROR;
            }
        }
    }



    /* Return to Caller */
    return MEA_OK;
}



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA PacketGen APIs                                         */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

 
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Create_PacketGen_Entry>                           */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Create_PacketGen_Entry(MEA_Unit_t                    unit_i,
                                          MEA_PacketGen_Entry_dbt      *entry_pi,
                                          MEA_PacketGen_t              *id_io)
{
    MEA_Bool exist;
    MEA_db_HwUnit_t  hwUnit;
    MEA_db_HwId_t   hwId;
    MEA_Uint16      j;
    
    
    
   
    MEA_API_LOG 

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }

    if (id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_i == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_PacketGen_drv_check_parameters(unit_i ,entry_pi)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_PacketGen_drv_check_parameters failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

#endif    

    

    /* Look for stream id */
    if (*id_io != MEA_PLAT_GENERATE_NEW_ID){
        /*check if the id exist*/
        if (mea_PacketGen_drv_stream_IsExist(unit_i,*id_io,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_PacketGen_drv_stream_IsExist failed (*id_io=%d\n",
                              __FUNCTION__,*id_io);
            return MEA_ERROR;
        }
        if (exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - *Id_io %d is already exist\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
        }
        if( (*id_io< MEA_PACKETGEN_MAX_STREAMS_TYPE_1) && 
                 (entry_pi->profile_info.headerType==MEA_PACKET_GEN_HEADER_TYPE_2)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - for HEADER_TYPE_2 Id %d need to be big then \n",__FUNCTION__,*id_io,MEA_PACKETGEN_MAX_STREAMS_TYPE_1);
            return MEA_ERROR;
        }

    }else {
        /*find new place*/
        if (mea_PacketGen_drv_find_free_Stream(id_io,(MEA_Uint16)entry_pi->profile_info.headerType)!=MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - mea_PacketGen_drv_find_free_Stream failed\n",__FUNCTION__);
            return MEA_ERROR;
        }
    }
 
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);


    

    if(entry_pi->profile_info.headerType == MEA_PACKET_GEN_HEADER_TYPE_1){
    /* Create db entries - same as in the fpga  */
        for (j=0;j<MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1;j++){
            hwId = (((*id_io) * MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1) +j);
            if (mea_db_Create(unit_i,
                          MEA_PacketGen_type1_db,
                          (MEA_db_SwId_t)hwId,
                          hwUnit,
                          &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Create failed (swId/hwId=%d,hwUnit=%d) \n",
                              __FUNCTION__,hwId,hwUnit);
            return MEA_ERROR;
            }
        }

        /* Update Hardware */
        if (mea_PacketGen_UpdateHw(unit_i,
                              *id_io,
                              (MEA_Uint16)entry_pi->profile_info.headerType,
                              entry_pi,
                              NULL) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_PacketGen_UpdateHw id %failed \n",__FUNCTION__,*id_io);
            for(j=0;j<MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1;j++){ 
                hwId = (((*id_io) * MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1) +j);
                mea_db_Delete(unit_i,
                          MEA_PacketGen_type1_db,
                          (MEA_db_SwId_t)hwId,
                          hwUnit);
            }
            return MEA_ERROR;
        }
    } else {
         for (j=0;j<MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET_TYPE2;j++){
            hwId = (((*id_io-MEA_PACKETGEN_MAX_STREAMS_TYPE_1) * MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET_TYPE2) +j);
            if (mea_db_Create(unit_i,
                          MEA_PacketGen_type2_db,
                          (MEA_db_SwId_t)hwId,
                          hwUnit,
                          &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Create failed (swId/hwId=%d,hwUnit=%d) \n",
                              __FUNCTION__,hwId,hwUnit);
            return MEA_ERROR;
            }
        }
      
     if (mea_PacketGen_UpdateHw(unit_i,
                              *id_io,
                              (MEA_Uint16)entry_pi->profile_info.headerType,
                              entry_pi,
                              NULL) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_PacketGen_UpdateHw id %failed \n",__FUNCTION__,*id_io);
            for(j=0;j<MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET_TYPE2;j++){ 
                hwId = (((*id_io -MEA_PACKETGEN_MAX_STREAMS_TYPE_1) * MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET_TYPE2) +j);
                mea_db_Delete(unit_i,
                          MEA_PacketGen_type2_db,
                          (MEA_db_SwId_t)hwId,
                          hwUnit);
            }
        
        
        return MEA_ERROR;
    }

    }
    
    /* Update Software shadow */
    MEA_OS_memcpy(&MEA_PacketGen_Table[*id_io].dbt_public,
                  entry_pi,
                  sizeof(MEA_PacketGen_Table[*id_io].dbt_public));
    MEA_OS_memset(&MEA_PacketGen_Table[*id_io].dbt_private,
                  0,
                  sizeof(MEA_PacketGen_Table[*id_io].dbt_private));
    MEA_PacketGen_Table[*id_io].dbt_private.valid=MEA_TRUE;
    
    if( mea_PacketGen_prof_info_drv_add_owner(unit_i , entry_pi->profile_info.id)!=MEA_OK){
    return MEA_ERROR;
    }


    /* Return to caller */
    return MEA_OK;
}
 

 
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Set_PacketGen_Entry>                              */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_PacketGen_Entry(MEA_Unit_t                       unit_i,
                                       MEA_PacketGen_t                  id_i,
                                       MEA_PacketGen_Entry_dbt         *entry_pi)
{

    MEA_Bool                 exist=MEA_FALSE;
   
    
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check if support */
    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }

    /*check if the id exist*/
    if (mea_PacketGen_drv_stream_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_PacketGen_drv_stream_IsExist failed (id_i=%d\n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - stream %d not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    /* Check parameters */
    if(mea_PacketGen_drv_check_parameters(unit_i ,entry_pi)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  mea_PacketGen_drv_check_parameters failed \n",__FUNCTION__);
        return MEA_ERROR;
    }



    

    /* Update Hardware */
    if (mea_PacketGen_UpdateHw(unit_i,
                              id_i,
                              (MEA_Uint16)entry_pi->profile_info.headerType,
                              entry_pi,
                              &(MEA_PacketGen_Table[id_i].dbt_public)) != MEA_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -  mea_PacketGen_UpdateHw id %failed \n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;
    }

    if(entry_pi->profile_info.id != MEA_PacketGen_Table[id_i].dbt_public.profile_info.id){
     
        if(mea_PacketGen_prof_info_drv_delete_owner(unit_i ,MEA_PacketGen_Table[id_i].dbt_public.profile_info.id)!=MEA_OK){
        }
                                                     
        if(mea_PacketGen_prof_info_drv_add_owner(unit_i , entry_pi->profile_info.id)!=MEA_OK){
    
        }
    }
    /* Update software shadow */
    MEA_OS_memcpy(&MEA_PacketGen_Table[id_i].dbt_public,
                  entry_pi,
                  sizeof(MEA_PacketGen_Table[id_i].dbt_public));

    /* Return to caller */
    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Delete_PacketGen_Entry>                           */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Delete_PacketGen_Entry(MEA_Unit_t                    unit_i,
                                          MEA_PacketGen_t               id_i)
{
    MEA_PacketGen_Entry_dbt     entry;
    MEA_Bool                    exist;
    MEA_db_HwUnit_t             hwUnit;
    MEA_db_HwId_t               hwId;
    MEA_Uint16                  j;
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check if support */
    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }

    /*check if the id exist*/
    if (mea_PacketGen_drv_stream_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_PacketGen_drv_stream_IsExist failed (id_i=%d\n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - stream %d not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
    
    /* Delete from device */
    MEA_OS_memset(&entry,0,sizeof(entry));
    entry.profile_info.headerType= MEA_PacketGen_Table[id_i].dbt_public.profile_info.headerType;
    if (mea_PacketGen_UpdateHw(unit_i,
                              id_i,
                              (MEA_Uint16)MEA_PacketGen_Table[id_i].dbt_public.profile_info.headerType,
                              &entry,
                              &(MEA_PacketGen_Table[id_i].dbt_public)) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_PacketGen_UpdateHw id %failed \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }
        MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

    if(MEA_PacketGen_Table[id_i].dbt_public.profile_info.headerType == 0){
    /* Delete from db */
        for (j=0;j<MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1;j++){ 
            hwId = (((id_i) * MEA_PACKET_GEN_MUM_OF_ENTRIES_PER_STREAM_TYPE1) +j);
            mea_db_Delete(unit_i,
                          MEA_PacketGen_type1_db,
                          (MEA_db_SwId_t)hwId,
                          hwUnit);
        }
    }else{
        for (j=0;j< MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET_TYPE2;j++){ 
        hwId = (((id_i-MEA_PACKETGEN_MAX_STREAMS_TYPE_1) * MEA_PACKET_GEN_HEADER_LEN_PER_OFFSET_TYPE2) +j);
        mea_db_Delete(unit_i,
                      MEA_PacketGen_type2_db,
                      (MEA_db_SwId_t)hwId,
                      hwUnit);
        }
    }

    mea_PacketGen_prof_info_drv_delete_owner(unit_i ,MEA_PacketGen_Table[id_i].dbt_public.profile_info.id);
    /* Delete the entry from driver shadow */
    MEA_OS_memset(&(MEA_PacketGen_Table[id_i]),
                  0,
                  sizeof(MEA_PacketGen_Table[id_i]));
    MEA_PacketGen_Table[id_i].dbt_private.valid = MEA_FALSE;

    
    /* Return to caller */
    return MEA_OK;
}


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Set_PacketGen_Entry>                              */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_PacketGen_Entry(MEA_Unit_t                       unit_i,
                                       MEA_PacketGen_t                  id_i,
                                       MEA_PacketGen_Entry_dbt         *entry_po)
{

    MEA_Bool    exist; 
     
    MEA_API_LOG
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check if support */
    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }

    /*check if the id exist*/
    if (mea_PacketGen_drv_stream_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_PacketGen_drv_stream_IsExist failed (id_i=%d\n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - stream %d not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    /* Copy to caller structure */
    MEA_OS_memcpy(entry_po ,
                  &(MEA_PacketGen_Table[id_i].dbt_public),
                  sizeof(*entry_po));
    
    /* Return to caller */
    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_GetFirst_PacketGen_Entry>                         */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_GetFirst_PacketGen_Entry(MEA_Unit_t                  unit_i,
                                            MEA_PacketGen_t            *id_o,
                                            MEA_PacketGen_Entry_dbt    *entry_po, 
                                            MEA_Bool                   *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
     if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    
    if (id_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ( (*id_o)=0;
          (*id_o) < MEA_PACKETGEN_MAX_STREAMS; 
          (*id_o)++ ) {
        if (MEA_PacketGen_Table[(*id_o)].dbt_private.valid == MEA_TRUE) {
            if (found_o) {
                *found_o= MEA_TRUE;
            }

            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                              &MEA_PacketGen_Table[(*id_o)].dbt_public,
                              sizeof(*entry_po));
            }
            break;
        }
    }

    
    return MEA_OK;
}

 
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_GetNext_PacketGen_Entry>                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_GetNext_PacketGen_Entry(MEA_Unit_t                   unit_i,
                                           MEA_PacketGen_t             *id_io,
                                           MEA_PacketGen_Entry_dbt     *entry_po, 
                                           MEA_Bool                    *found_o)
{

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    if (id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ( (*id_io)++;
          (*id_io) < MEA_PACKETGEN_MAX_STREAMS; 
          (*id_io)++ ) {
        if(MEA_PacketGen_Table[(*id_io)].dbt_private.valid == MEA_TRUE) {
            if (found_o) {
                *found_o= MEA_TRUE;
            }
            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                              &MEA_PacketGen_Table[(*id_io)].dbt_public,
                              sizeof(*entry_po));
            }
            break;
        }
    }
   
    
    return MEA_OK;
}

 

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_IsExist_PacketGen>                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_PacketGen(MEA_Unit_t                    unit_i,
                                           MEA_PacketGen_t              Id_i,
                                           MEA_Bool                     *exist_o)
{
    MEA_API_LOG
    
    if (!MEA_PACKETGEN_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Packet Generator not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    
    
    return mea_PacketGen_drv_stream_IsExist(unit_i,Id_i,exist_o);
}



MEA_Status MEA_API_Delete_PacketGen_All_Entry(MEA_Unit_t                    unit_i)
{
  MEA_Bool found;
  MEA_PacketGen_t            id;

    if(MEA_API_GetFirst_PacketGen_Entry(unit_i,
                                        &id,
                                          NULL, 
                                          &found) !=MEA_OK){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_GetFirst_PacketGen_Entry failed\n",__FUNCTION__);
        return MEA_ERROR;

    }
    while (found) {
        if(MEA_API_Delete_PacketGen_Entry(unit_i,id) !=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_Delete_PacketGen_Entry failed\n",__FUNCTION__);
            return MEA_ERROR;
        }
        if(MEA_API_GetNext_PacketGen_Entry(unit_i,
            &id,
            NULL, 
            &found) !=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_GetNext_PacketGen_Entry failed\n",__FUNCTION__);
                return MEA_ERROR;
    
        }
    }

    return MEA_OK;
}

/************************** ANALYZER **************************/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Conclude_Analyzer>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Conclude_Analyzer(MEA_Unit_t  unit_i )
{

    if(MEA_Counters_ANALYZER_Table !=NULL){
        MEA_OS_free(MEA_Counters_ANALYZER_Table);
        MEA_Counters_ANALYZER_Table = NULL;
    }
    if(MEA_Configure_ANALYZER_Table != NULL) {
        MEA_OS_free(MEA_Configure_ANALYZER_Table);
        MEA_Configure_ANALYZER_Table=NULL;
    }

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Init_Analyzer>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_Analyzer(MEA_Unit_t  unit_i )
{
    MEA_Uint32 size;

    /* Verify not already init */
    if (MEA_Analyzer_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - already init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    MEA_Analyzer_InitDone = MEA_FALSE;

    /* Allocate PacketGen Table */
    size = MEA_ANALYZER_MAX_STREAMS_TYPE1  * sizeof(MEA_Counters_Analyzer_dbt);
    if (size != 0) {
        MEA_Counters_ANALYZER_Table = (MEA_Counters_Analyzer_dbt*)MEA_OS_malloc(size);
        if (MEA_Counters_ANALYZER_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_Counters_ANALYZER_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_Counters_ANALYZER_Table[0]),0,size);
    }

    /* Allocate PacketGen Table */
    size = MEA_ANALYZER_MAX_STREAMS_TYPE1  * sizeof(MEA_Analyzer_configure_dbt);
    if (size != 0) {
        MEA_Configure_ANALYZER_Table = (MEA_Analyzer_configure_dbt*)MEA_OS_malloc(size);
        if (MEA_Configure_ANALYZER_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_Configure_ANALYZER_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_Configure_ANALYZER_Table[0]),0,size);
    }

    

    if(MEA_PACKET_ANALYZER_TYPE1_SUPPORT){
   /* Reset Counters RMON */
        if (MEA_API_Clear_Counters_ANALYZERs(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Clear_Counters_ANALYZERs failed\n",
                              __FUNCTION__);
            return MEA_ERROR;
        }
    }
    
    MEA_Analyzer_InitDone = MEA_TRUE;

    return MEA_OK;
}


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <mea_drv_ReInit_Counters_ANALYZE>                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status mea_drv_ReInit_Counters_ANALYZE(MEA_Unit_t unit_i)
{
    MEA_Analyzer_t id;
    MEA_Counters_Analyzer_dbt  save_entry_db;
    MEA_Counters_Analyzer_dbt* entry_db_p;

    if(!MEA_PACKET_ANALYZER_TYPE1_SUPPORT){
        return MEA_OK;
    }
    /* Scan all stream and read the ANALZER counters to clear from hardware ,
       without update the software shadow */
    for (id=0;id< MEA_ANALYZER_MAX_STREAMS_TYPE1 ;id++) {

        entry_db_p = &(MEA_Counters_ANALYZER_Table[id]);
        MEA_OS_memcpy(&save_entry_db,entry_db_p,sizeof(save_entry_db));
	    if (MEA_API_Collect_Counters_ANALYZER(unit_i,id,NULL)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect ANALYZER Counters for id=%d failed\n",
                              __FUNCTION__,id);
            MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));
            return MEA_ERROR;
        }
        MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));

    }

    /* Return to Callers */
    return MEA_OK;
    }

static MEA_Status mea_anlyzerReset_Seq_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                  unit_i       = (MEA_Unit_t           )arg1;
    MEA_Uint32                  value = (MEA_Uint32)((long)arg3);
    

    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    MEA_Uint32 count_shift;
    /* Zero initialization the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }

    count_shift=0;

    MEA_OS_insert_value(count_shift,
        32,
        value ,
        &val[0]);
    count_shift+=32;


    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            ENET_BM_IA_WRDATA(i),
            val[i],
            MEA_MODULE_BM);  
    }


    return MEA_OK;
}

static MEA_Status mea_anlyzerReset_Seq_UpdateHw(MEA_Unit_t                  unit_i,
                                                MEA_CcmId_t                 id_i,
                                                MEA_Uint32                  value)
{


    ENET_ind_write_t      ind_write;
   

    //  MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    /* check if we have any changes */

    /* Prepare ind_write structure */
    ind_write.tableType        = ENET_BM_TBL_ANALZER_CNT;

    ind_write.cmdReg            = ENET_BM_IA_CMD;      
    ind_write.cmdMask           = ENET_BM_IA_CMD_MASK;      
    ind_write.statusReg         = ENET_BM_IA_STAT;   
    ind_write.statusMask        = ENET_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg      = ENET_BM_IA_ADDR;
    ind_write.tableAddrMask     = ENET_BM_IA_ADDR_OFFSET_MASK;

    ind_write.writeEntry       = (MEA_FUNCPTR)mea_anlyzerReset_Seq_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2       = 0; 
    ind_write.funcParam3 = (MEA_funcParam)((long)value);
    ind_write.funcParam4       = 0;


        ind_write.tableOffset      = (id_i* MEA_ANALYZER_MAX_NUM_OF_COUNTERS) +  0 ;
        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_BM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }

    /* Return to caller */

    return MEA_OK;
}




MEA_Status MEA_API_Set_Analyzer(MEA_Unit_t                    unit_i,
                                MEA_Analyzer_t                id_i,
                                MEA_Analyzer_configure_dbt    *entry_pi)
{

MEA_Uint32 val;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_PACKET_ANALYZER_TYPE1_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Analyzer not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    if (entry_pi == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_pi = NULL \n",
            __FUNCTION__);
        return MEA_ERROR;   
    } 

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    if(id_i >= MEA_ANALYZER_MAX_STREAMS_TYPE1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Analyzer id=%d is out of range \n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
    
    
    if((entry_pi->activate == MEA_TRUE) && (MEA_Configure_ANALYZER_Table[id_i].activate == MEA_FALSE)){
        //clear the counter.
        if(MEA_API_Clear_Counters_ANALYZER(unit_i,id_i) !=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Analyzer id=%d is out of range \n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
    
    }
    if(entry_pi->activate == MEA_TRUE){

        if(entry_pi->resetPrbs == MEA_TRUE){
         
          val=0;
           val = (id_i & 0x000003ff);
           val |= (1<<10);
           MEA_API_WriteReg(MEA_UNIT_0,ENET_BM_RESET_PRBS_STREAM_REG,val,MEA_MODULE_BM);
           val=0;
           MEA_API_WriteReg(MEA_UNIT_0,ENET_BM_RESET_PRBS_STREAM_REG,val,MEA_MODULE_BM);
            entry_pi->resetPrbs=MEA_FALSE;
        }
        if(entry_pi->enable_clearSeq == MEA_TRUE){
            val=entry_pi->value_nextSeq;
            if(mea_anlyzerReset_Seq_UpdateHw(unit_i,id_i,val)!=MEA_OK){
                return MEA_ERROR;
            }
            entry_pi->enable_clearSeq=MEA_FALSE;
        }
    
    }
    
    MEA_OS_memcpy(&MEA_Configure_ANALYZER_Table[id_i],entry_pi,sizeof(MEA_Configure_ANALYZER_Table[id_i]));
    
    return MEA_OK;
}






MEA_Status MEA_API_Get_Analyzer(MEA_Unit_t                    unit_i,
                                MEA_Analyzer_t                id_i,
                                MEA_Analyzer_configure_dbt    *entry_po)
{
    MEA_Bool exist;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_PACKET_ANALYZER_TYPE1_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Analyzer not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    if (entry_po == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_po = NULL \n",
            __FUNCTION__);
        return MEA_ERROR;   
    } 

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    if(id_i >= MEA_ANALYZER_MAX_STREAMS_TYPE1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Analyzer id=%d is out of range \n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    if(MEA_API_IsExist_Analyzer(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_IsExist_Analyzer id=%d  \n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    if(!exist){
       entry_po->activate=MEA_FALSE;
    }



    MEA_OS_memcpy(entry_po,&MEA_Configure_ANALYZER_Table[id_i],sizeof(*entry_po));
    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_IsExist_Analyzer>                                 */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_Analyzer(MEA_Unit_t                    unit_i,
                                    MEA_Analyzer_t               id_i,
                                    MEA_Bool                      *exist_o)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_PACKET_ANALYZER_TYPE1_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Analyzer not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    if (exist_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - exist_o = NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    } 

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   
    if(id_i >= MEA_ANALYZER_MAX_STREAMS_TYPE1){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Analyzer id=%d is out of range \n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
    *exist_o=MEA_FALSE;
    if(MEA_Configure_ANALYZER_Table[id_i].activate == MEA_TRUE){
    *exist_o=MEA_TRUE;
    }
    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Collect_Counters_ANALYZER>                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_ANALYZER(MEA_Unit_t             unit,
                                             MEA_Analyzer_t             id_i,
                                             MEA_Counters_Analyzer_dbt* entry)
{
    
    
    MEA_ind_read_t ind_read;
	MEA_Uint32     read_data[MEA_ANALYZER_SUPPORT_NUM_OF_COUNTERS];
    MEA_Bool       exist;
    MEA_Uint32     index;
    MEA_Uint32 AVG_latency;
    MEA_Uint32 MAX_jitter; 
    MEA_Uint32 MIN_jitter; 

    

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_PACKET_ANALYZER_TYPE1_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PACKET ANALYZER are not support \n",
        __FUNCTION__);
        return ENET_ERROR;
    }
     if(MEA_API_IsExist_Analyzer(unit,id_i,&exist)==MEA_ERROR){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Analyzer stream id=%d is not Exist\n",
        __FUNCTION__,id_i);
        return ENET_ERROR;
     }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
      
    MEA_OS_memset(&read_data[0],0,sizeof(read_data));

    ind_read.tableType      =   ENET_BM_TBL_ANALZER_CNT;
    ind_read.cmdReg         =   MEA_BM_IA_CMD;      
    ind_read.cmdMask        =   MEA_BM_IA_CMD_MASK;      
    ind_read.statusReg      =   MEA_BM_IA_STAT;   
    ind_read.statusMask     =   MEA_BM_IA_STAT_MASK;   
    ind_read.tableAddrReg   =   MEA_BM_IA_ADDR;
    ind_read.tableAddrMask  =   MEA_BM_IA_ADDR_OFFSET_MASK;
    
    for(index=0 ;index < MEA_NUM_OF_ELEMENTS(read_data); index++){
        ind_read.tableOffset    =   (id_i * MEA_ANALYZER_MAX_NUM_OF_COUNTERS)+ index;
        ind_read.read_data      =   &(read_data[index]);

        if (MEA_API_ReadIndirect(unit,
                                 &ind_read,
                                 1,
                                 MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - ReadIndirect from tableType %x module BM failed "
                               "(id=%d)\n",
                                __FUNCTION__,ind_read.tableType,id_i);

            return MEA_ERROR;
        }
        /* Update */
     
        
    }
   
//    for(index=0 ;index < MEA_NUM_OF_ELEMENTS(read_data); index++){
//        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"read_data[%d]= %ld \n",index,read_data[index]);
//    }

    MEA_Counters_ANALYZER_Table[id_i].lastseqID=(read_data[0]); // This is not counter
    MEA_OS_UPDATE_COUNTER(MEA_Counters_ANALYZER_Table[id_i].num_Of_Bits_Error.val,(read_data[1]));
//    if((read_data[2])>4)
//     read_data[2]= read_data[2] -4;

    MEA_OS_UPDATE_COUNTER(MEA_Counters_ANALYZER_Table[id_i].Bytes.val            ,(read_data[2]));
    MEA_OS_UPDATE_COUNTER(MEA_Counters_ANALYZER_Table[id_i].Pkts.val             ,(read_data[3]));
    MEA_OS_UPDATE_COUNTER(MEA_Counters_ANALYZER_Table[id_i].seq_ID_err.val       ,(read_data[4]));
    MEA_OS_UPDATE_COUNTER(MEA_Counters_ANALYZER_Table[id_i].seq_ID_reorder.val   ,(read_data[5]));
    MEA_OS_UPDATE_COUNTER(MEA_Counters_ANALYZER_Table[id_i].drop.val             ,(read_data[6]));
    //MEA_OS_UPDATE_COUNTER(MEA_Counters_ANALYZER_Table[id_i].reserve          ,(read_data[7]));

AVG_latency=0;
MAX_jitter=0;
MIN_jitter=0xffffffff;

AVG_latency =(read_data[8]);
MAX_jitter =(read_data[9]);
MIN_jitter =(read_data[10]);

       
 
        
        if(MAX_jitter){
        if( MEA_Counters_ANALYZER_Table[id_i].MAX_jitter < MAX_jitter )
                MEA_Counters_ANALYZER_Table[id_i].MAX_jitter = MAX_jitter;
        }
        if(MIN_jitter ){
            if (MEA_Counters_ANALYZER_Table[id_i].MIN_jitter == 0)
                MEA_Counters_ANALYZER_Table[id_i].MIN_jitter = 0xffffffff;


         if(MEA_Counters_ANALYZER_Table[id_i].MIN_jitter >= MIN_jitter  && (MIN_jitter <= MAX_jitter) )
                MEA_Counters_ANALYZER_Table[id_i].MIN_jitter = MIN_jitter;
        }
		
		if(AVG_latency){
            if ((AVG_latency >= MIN_jitter) && (AVG_latency <= MAX_jitter) && MIN_jitter )
#ifdef MEA_ANALYZER_SW_CALC_AVG		  
                MEA_Counters_ANALYZER_Table[id_i].AVG_latency = (MEA_Counters_ANALYZER_Table[id_i].MIN_jitter + MEA_Counters_ANALYZER_Table[id_i].MAX_jitter )/2;
#else
          MEA_Counters_ANALYZER_Table[id_i].AVG_latency =AVG_latency;
#endif				
        }
        
         

    

    

    /* Update the output Parameter if Supply */
    if ( entry != NULL) {
        MEA_OS_memcpy(entry,
                      &(MEA_Counters_ANALYZER_Table[id_i]),
                      sizeof(*entry));
    }


    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Collect_Counters_ANALYZER>                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_ANALYZERs (MEA_Unit_t unit)
{

    MEA_Analyzer_t id;
    MEA_Bool       exist;
    /* Scan all stream and read the ANALZER counters to clear from hardware ,
       without update the software shadow */
    for (id=0;id< MEA_ANALYZER_MAX_STREAMS_TYPE1;id++) {
        if(MEA_API_IsExist_Analyzer(unit,id,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsExist_Analyzer id=%d failed\n",
                __FUNCTION__,id);
            return MEA_ERROR;
        }
        if(exist==MEA_FALSE){
            continue;
        }
        if (MEA_API_Collect_Counters_ANALYZER(unit,id,NULL)==MEA_ERROR){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect ANALYZER Counters for id=%d failed\n",
                              __FUNCTION__,id);
            return MEA_ERROR;
        }
    }
    
    
    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Get_Counters_ANALYZER>                            */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_ANALYZER(MEA_Unit_t                  unit,
                                         MEA_Analyzer_t             id_i,
                                         MEA_Counters_Analyzer_dbt  *entry)
{

    MEA_Bool     exist;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_PACKET_ANALYZER_TYPE1_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PACKET ANALYZER are not support \n",
        __FUNCTION__);
        return ENET_ERROR;
    }
    if(MEA_API_IsExist_Analyzer(unit,id_i,&exist)==MEA_ERROR){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Analyzer stream id=%d is not Exist\n",
        __FUNCTION__,id_i);
        return ENET_ERROR;
    }
    if(!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Analyzer stream id=%d is not Exist\n",
            __FUNCTION__,id_i);
        return ENET_ERROR;
    }
    if(entry == NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Analyzer stream id=%d is not Exist\n",
        __FUNCTION__,id_i);
        return ENET_ERROR;

    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

     MEA_OS_memcpy(entry ,&MEA_Counters_ANALYZER_Table[id_i] ,sizeof(*entry));
    
    return MEA_OK;
}
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Clear_Counters_ANALYZER>                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_ANALYZER(MEA_Unit_t    unit,
                                           MEA_Analyzer_t             id_i)
{
    MEA_Bool exist;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_PACKET_ANALYZER_TYPE1_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PACKET ANALYZER are not support \n",
        __FUNCTION__);
        return ENET_ERROR;
    }
    if(MEA_API_IsExist_Analyzer(unit,id_i,&exist)== MEA_ERROR){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Analyzer stream id=%d is not Exist\n",
        __FUNCTION__,id_i);
        return ENET_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    
    if(MEA_API_Collect_Counters_ANALYZER(unit,id_i,NULL) !=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Collect_Counters_ANALYZER id=%d failed  \n",
        __FUNCTION__,id_i);
        return ENET_ERROR;
    }

    MEA_OS_memset(&MEA_Counters_ANALYZER_Table[id_i],0,sizeof(MEA_Counters_ANALYZER_Table[0]));
   MEA_Counters_ANALYZER_Table[id_i].MIN_jitter = 0xffffffff;
    return MEA_OK;
}
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Clear_Counters_ANALYZERs>                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_ANALYZERs(MEA_Unit_t                unit)
{
    MEA_Analyzer_t             id;
    MEA_Bool                   exist;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_PACKET_ANALYZER_TYPE1_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PACKET ANALYZER are not support \n",
        __FUNCTION__);
        return ENET_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    for (id=0;id< MEA_ANALYZER_MAX_STREAMS_TYPE1 ;id++) {
        if(MEA_API_IsExist_Analyzer(unit,id,&exist)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsExist_Analyzer id=%d failed\n",
                __FUNCTION__,id);
            return MEA_ERROR;
        }
        if(exist==MEA_FALSE){
            continue;
        }
        if(MEA_API_Clear_Counters_ANALYZER(unit,id) !=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_ANALYZER id=%d faild  \n",
            __FUNCTION__,id);
            return ENET_ERROR;
        }
    }
    
    return MEA_OK;
}


/************************************************************************/
/* The function below foe entity   PRBS Gen and ANALAzer               */
/************************************************************************/



 

MEA_Status mea_drv_PRBS_Init(MEA_Unit_t  unit)
{
int i;    
    if(!b_bist_test){
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," BRBS Init Off\n");
        return MEA_OK;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize BRBS On \n");

    MEA_OS_memset(&MEA_drv_prbs_Gen_info_Table[0],0,sizeof(MEA_drv_prbs_Gen_info_Table));

    MEA_OS_memset(&MEA_drv_prbs_Counters_Table[0],0,sizeof(MEA_drv_prbs_Counters_Table));
    MEA_OS_memset(&mea_Gen_configure[0],0,sizeof(mea_Gen_configure));
    

    for(i=0;i< MEA_PRBS_MAX_OF_STREAMS;i++){
        MEA_drv_prbs_Gen_info_Table[i].valid = MEA_TRUE;
        MEA_drv_prbs_Counters_Table[i].valid = MEA_TRUE;
    }

    return MEA_OK;
}

MEA_Status mea_drv_PRBS_ReInit(MEA_Unit_t  unit)
{
     MEA_Uint32 value,i;

    if(!b_bist_test){
       return MEA_OK;
    }
   
    value=0;
    for(i=0;i<MEA_PRBS_MAX_OF_STREAMS; i++)
        value |=(mea_Gen_configure[i]<<(i*8));



    MEA_IF_HW_ACCESS_ENABLE
        MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_GENRATOR ,value,MEA_MODULE_IF);
    MEA_IF_HW_ACCESS_DISABLE  

    return MEA_OK;
}

MEA_Status mea_drv_PRBS_Conclude(MEA_Unit_t  unit)
{
    

    if(!b_bist_test){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," BRBS Init OFF\n");
        return MEA_OK;
    }
     


    return MEA_OK;
}


MEA_Status MEA_API_PRBS_SET_Generator(MEA_Unit_t  unit,MEA_Uint16 Id_i,MEA_PRBS_Gen_configure_dbt *entry)
{

    MEA_Uint32 value,i;

    if(!b_bist_test){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," PRBS work only on Bist Mode\n");
        return MEA_ERROR;
    }
    
    if( Id_i >=MEA_PRBS_MAX_OF_STREAMS){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," Id(%d) is out of range\n",Id_i);
        return MEA_ERROR;
    }

    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry = NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    //check parameter 
    

    mea_Gen_configure[Id_i]=0;
    mea_Gen_configure[Id_i]= entry->v.reg[0];
   
    value=0;
    for(i=0;i<MEA_PRBS_MAX_OF_STREAMS; i++)
        value |=(mea_Gen_configure[i]<<(i*8));

   MEA_drv_prbs_Gen_info_Table[Id_i].data.v.reg[0]=entry->v.reg[0];

MEA_IF_HW_ACCESS_ENABLE
   MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_GENRATOR ,value,MEA_MODULE_IF);
MEA_IF_HW_ACCESS_DISABLE    
    
    return MEA_OK;
}

MEA_Status MEA_API_PRBS_Get_Generator(MEA_Unit_t  unit,MEA_Uint16 Id_i,MEA_PRBS_Gen_configure_dbt *entry_o)
{

    MEA_Uint32 value=0;
	MEA_Uint32 get_value = 0;
	MEA_Uint32 numShifitBit = 0;

    if(!b_bist_test){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," PRBS work only on Bist Mode\n");
        return MEA_ERROR;
    }
       
    if( Id_i >=MEA_PRBS_MAX_OF_STREAMS){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," Id(%d) is out of range\n",Id_i);
        return MEA_ERROR;
    } 

    if(entry_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry_o = NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
MEA_IF_HW_ACCESS_ENABLE
    
    get_value = MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_GENRATOR,MEA_MODULE_IF);
MEA_IF_HW_ACCESS_DISABLE
      numShifitBit = 8 * Id_i;
     value= (MEA_Uint32)  (((MEA_Uint32)get_value)>>(numShifitBit) ) & ((MEA_Uint32)(0x000000ff) ) ;

     entry_o->v.reg[0]=value;


    return MEA_OK;
}

MEA_Status mea_drv_prbs_counter_read(MEA_Unit_t  unit ,MEA_Uint16 Id_i)
{
    MEA_Uint32 val_ber=0;
    MEA_Uint32 val_good=0;
    MEA_Uint32 val_event=0;

    switch (Id_i){
     case 0:
         MEA_IF_HW_ACCESS_ENABLE


            MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA0_COUNT_BER ,0,MEA_MODULE_IF);
            MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA0_COUNT_GOOD ,0,MEA_MODULE_IF);
            MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA0_EVNT_LOCK ,0,MEA_MODULE_IF);

            val_ber=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA0_COUNT_BER,MEA_MODULE_IF);
            val_good=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA0_COUNT_GOOD,MEA_MODULE_IF);
            
            val_event=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA0_EVNT_LOCK,MEA_MODULE_IF);

         MEA_IF_HW_ACCESS_DISABLE

         break;
     case 1:

         MEA_IF_HW_ACCESS_ENABLE


         MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA1_COUNT_BER ,0,MEA_MODULE_IF);
         MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA1_COUNT_GOOD ,0,MEA_MODULE_IF);
         MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA1_EVNT_LOCK ,0,MEA_MODULE_IF);

         val_ber=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA1_COUNT_BER,MEA_MODULE_IF);
         val_good=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA1_COUNT_GOOD,MEA_MODULE_IF);
        
         val_event=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA1_EVNT_LOCK,MEA_MODULE_IF);

         MEA_IF_HW_ACCESS_DISABLE
         break;
     case 2:
         MEA_IF_HW_ACCESS_ENABLE
         MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA2_COUNT_BER ,0,MEA_MODULE_IF);
         MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA2_COUNT_GOOD ,0,MEA_MODULE_IF);
         MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA2_EVNT_LOCK ,0,MEA_MODULE_IF);

         val_ber=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA2_COUNT_BER,MEA_MODULE_IF);
         val_good=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA2_COUNT_GOOD,MEA_MODULE_IF);
         
         val_event=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA2_EVNT_LOCK,MEA_MODULE_IF);
         
         
         MEA_IF_HW_ACCESS_DISABLE
         break;
     case 3:
         MEA_IF_HW_ACCESS_ENABLE
         MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA3_COUNT_BER ,0,MEA_MODULE_IF);
         MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA3_COUNT_GOOD ,0,MEA_MODULE_IF);
         MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA3_EVNT_LOCK ,0,MEA_MODULE_IF);

         val_ber=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA3_COUNT_BER,MEA_MODULE_IF);
         val_good=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA3_COUNT_GOOD,MEA_MODULE_IF);
        
         val_event=MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_BIST_PRBS_ANA3_EVNT_LOCK,MEA_MODULE_IF);
         MEA_IF_HW_ACCESS_DISABLE



         break;
    default:
        break;
    }

    MEA_OS_UPDATE_COUNTER(MEA_drv_prbs_Counters_Table[Id_i].data.error_ber.val,val_ber);
    MEA_OS_UPDATE_COUNTER(MEA_drv_prbs_Counters_Table[Id_i].data.good_bit.val,val_good);
    MEA_drv_prbs_Counters_Table[Id_i].data.event_Lock=(MEA_Bool)val_event;



    
    return MEA_OK;
}

/************************************************************************/
/*   PRBS analyzer counters                                             */
/************************************************************************/
MEA_Status MEA_API_Collect_Counters_PRBSs(MEA_Unit_t  unit)
{
  MEA_Uint16 i;
   
  if(!b_bist_test){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," PRBS work only on Bist Mode\n");
      return MEA_ERROR;
  }

  
    for(i=0;i<MEA_PRBS_MAX_OF_STREAMS;i++)
        mea_drv_prbs_counter_read(unit ,i);



    return MEA_OK;
}

MEA_Status MEA_API_Collect_Counters_PRBS(MEA_Unit_t  unit,MEA_Uint16 Id_i)
{
    if(!b_bist_test){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," PRBS work only on Bist Mode\n");
        return MEA_ERROR;
    }

    if( Id_i >=MEA_PRBS_MAX_OF_STREAMS){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," Id(%d) is out of range\n",Id_i);
        return MEA_ERROR;
    } 

   mea_drv_prbs_counter_read(unit ,Id_i);


    return MEA_OK;
}



MEA_Status MEA_API_Clear_Counters_PRBSs(MEA_Unit_t  unit)
{
MEA_Uint16 i;
    
    if(!b_bist_test){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," PRBS work only on Bist Mode\n");
        return MEA_ERROR;
    }

    for(i=0;i<MEA_PRBS_MAX_OF_STREAMS;i++){
            mea_drv_prbs_counter_read(unit ,i);
        MEA_OS_memset(&MEA_drv_prbs_Counters_Table[i].data,0,sizeof(MEA_drv_prbs_Counters_Table[i].data));
    }


    return MEA_OK;
}

MEA_Status MEA_API_Clear_Counters_PRBS(MEA_Unit_t  unit,MEA_Uint16 Id_i)
{

    if(!b_bist_test){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," PRBS work only on Bist Mode\n");
        return MEA_ERROR;
    }

    if( Id_i >=MEA_PRBS_MAX_OF_STREAMS){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," Id(%d) is out of range\n",Id_i);
        return MEA_ERROR;
    } 

    mea_drv_prbs_counter_read(unit ,Id_i);
    MEA_OS_memset(&MEA_drv_prbs_Counters_Table[Id_i].data,0,sizeof(MEA_drv_prbs_Counters_Table[0].data));

    return MEA_OK;
}

MEA_Status MEA_API_Get_Counters_PRBS(MEA_Unit_t  unit,MEA_Uint16 Id_i,MEA_PRBS_analyzer_event_Counters_dbt *entry)
{

    if(!b_bist_test){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," PRBS work only on Bist Mode\n");
        return MEA_ERROR;
    }

    if( Id_i >=MEA_PRBS_MAX_OF_STREAMS){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," Id(%d) is out of range\n",Id_i);
        return MEA_ERROR;
    } 
    
    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry = NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

   MEA_OS_memcpy(entry,&MEA_drv_prbs_Counters_Table[Id_i].data,sizeof(*entry));




    return MEA_OK;
}

