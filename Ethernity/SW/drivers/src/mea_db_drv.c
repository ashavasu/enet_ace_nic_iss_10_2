/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_action_drv.h"
#include "mea_db_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct {
	const char               *name;
	MEA_db_SwId_t             num_of_sw_ids;
	MEA_db_SwHw_Entry_dbt    *SwHw_Table;
    MEA_db_HwUnit_Entry_dbt  *HwUnit_Table;
} MEA_db_Entry_dbt;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             external functions implementation                              */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_Conclude>                                     */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_Conclude(MEA_Unit_t         unit_i,
 				           MEA_db_dbt*        db_io)
{

	MEA_db_Entry_dbt *db;
    MEA_db_HwUnit_t      hwUnit;
    MEA_db_HwId_t      hwId;


	/* Check valid db_i parameter */
	if (db_io == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_io == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Verify that there is db allocated */
	if (*db_io == NULL) {
		return MEA_OK;
	}
	db = (MEA_db_Entry_dbt*)(*db_io);


	/* Free SwHw_Table */
	if (db->SwHw_Table) {
		MEA_OS_free(db->SwHw_Table);
	}
	db->SwHw_Table = NULL;

	
	/* Free Hw_Table */
	if (db->HwUnit_Table) {
  	    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
			if (db->HwUnit_Table[hwUnit].table) {
				for (hwId=0;hwId<db->HwUnit_Table[hwUnit].num_of_entries;hwId++) {
					if (db->HwUnit_Table[hwUnit].table[hwId].regs) {
						MEA_OS_free(db->HwUnit_Table[hwUnit].table[hwId].regs);
						db->HwUnit_Table[hwUnit].table[hwId].regs = NULL;
					}
				}
				MEA_OS_free(db->HwUnit_Table[hwUnit].table);
				db->HwUnit_Table[hwUnit].table=NULL;
			}
		}
		MEA_OS_free(db->HwUnit_Table);
	}
	db->HwUnit_Table = NULL;


	/* Set output parameter */
	*db_io = NULL;

	/* Return to caller */
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_Init>                                         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_Init(MEA_Unit_t                    unit_i,
					   const char                   *name_i,
					   MEA_db_num_of_sw_size_func_t  num_of_sw_size_func_i,
					   MEA_db_num_of_hw_size_func_t  num_of_hw_size_func_i,
 					   MEA_db_dbt*                   db_o)
{

	MEA_Uint32        size;
	MEA_db_HwUnit_t      hwUnit;
	MEA_db_HwId_t     hwId;
	MEA_db_Entry_dbt *db;
	MEA_Uint32        tmp;


	/* Check parameter name_i */
	if (name_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - name_i==NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check parameter db_o */
	if (db_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_o==NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Set default value for output parameter */
	*db_o = NULL;

	/* allocate new db */
	db = (MEA_db_Entry_dbt*)MEA_OS_malloc(sizeof(*db));
	if (db == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Allocate MEA_db_Entry_dbt failed (name='%s')\n",
                          __FUNCTION__,(name_i==NULL)?"NULL":name_i);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)db,0,sizeof(*db));

	/* Set the name for this db */
	db->name = name_i;

	/* Get db->num_of_sw_ids */
	if (num_of_sw_size_func_i(unit_i,
		                      &(db->num_of_sw_ids),
							  &tmp) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - num_of_sw_size_func_i failed",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Allocate SwHw_Table */
	size = db->num_of_sw_ids * 
		   mea_drv_num_of_hw_units *
		   sizeof(MEA_db_SwHw_Entry_dbt);
	db->SwHw_Table = (MEA_db_SwHw_Entry_dbt*)MEA_OS_malloc(size);
	if (db->SwHw_Table == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Allocate SwHw_Table failed (size=%d)\n",
						  __FUNCTION__,size);
		mea_db_Conclude(unit_i,(MEA_db_dbt)(&db));
		return MEA_ERROR;
	}
	MEA_OS_memset(db->SwHw_Table,0,size);

	/* Allocate HwUnit_Table */
	size = mea_drv_num_of_hw_units *
		   sizeof(MEA_db_HwUnit_Entry_dbt);
	db->HwUnit_Table = (MEA_db_HwUnit_Entry_dbt*)MEA_OS_malloc(size);
	if (db->HwUnit_Table == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Allocate HwUnit_Table failed (size=%d)\n",
						  __FUNCTION__,size);
		mea_db_Conclude(unit_i,(MEA_db_dbt)(&db));
		return MEA_ERROR;
	}
	MEA_OS_memset(db->HwUnit_Table,0,size);
	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		if (num_of_hw_size_func_i(unit_i,
			                      hwUnit,
								  &(db->HwUnit_Table[hwUnit].num_of_entries),
								  &(db->HwUnit_Table[hwUnit].num_of_regs   )
								  ) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - num_of_hw_size_func_i failed\n",
							  __FUNCTION__);
			mea_db_Conclude(unit_i,(MEA_db_dbt)(&db));
		}
		db->HwUnit_Table[hwUnit].used_entries   = 0;
		db->HwUnit_Table[hwUnit].table          = NULL;
		size = db->HwUnit_Table[hwUnit].num_of_entries * sizeof(MEA_db_Hw_Entry_dbt);
		if (size > 0) {
			db->HwUnit_Table[hwUnit].table = (MEA_db_Hw_Entry_dbt*)MEA_OS_malloc(size);
			if (db->HwUnit_Table[hwUnit].table == NULL) {
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                      "%s - Allocate Hw_Table failed (hwUnit=%d,size=%d)\n",
						          __FUNCTION__,hwUnit,size);
  		        mea_db_Conclude(unit_i,(MEA_db_dbt)(&db));
		        return MEA_ERROR;
			}
			MEA_OS_memset(db->HwUnit_Table[hwUnit].table,0,size);
			for (hwId=0;hwId<db->HwUnit_Table[hwUnit].num_of_entries;hwId++) {
				size = sizeof(MEA_Uint32)*db->HwUnit_Table[hwUnit].num_of_regs;
				if (size > 0) {
                    db->HwUnit_Table[hwUnit].table[hwId].regs = (long*)MEA_OS_malloc(size);
					if (db->HwUnit_Table[hwUnit].table[hwId].regs == NULL) {
				        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                      "%s - Allocate HwShadow failed (hwUnit=%d,size=%d)\n",
								          __FUNCTION__,hwUnit,size);

  						mea_db_Conclude(unit_i,(MEA_db_dbt)(&db));
						return MEA_ERROR;
					}
					MEA_OS_memset((char*)(db->HwUnit_Table[hwUnit].table[hwId].regs),0,size);
				} else {
                    db->HwUnit_Table[hwUnit].table[hwId].regs = NULL;
				}

			}
		}

	}

	/* Set output parameter */
	*db_o = (MEA_db_dbt)db;

	return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_Create>                                       */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_Create(MEA_Unit_t                  unit_i,
					     MEA_db_dbt                  db_i,
						 MEA_db_SwId_t               swId_i,
					     MEA_db_HwUnit_t                hwUnit_i,
						 MEA_db_HwId_t              *hwId_io) 
{

	MEA_db_Entry_dbt    *db = (MEA_db_Entry_dbt*)db_i;
	MEA_db_Hw_Entry_dbt *hw_entry;
	MEA_db_HwId_t        hwId;
	MEA_Uint32           index;
	long                 *save_regs;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid hwId_i parameter */
	if (swId_i >= db->num_of_sw_ids) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - swId_i (%d) >= max value (%d) \n",
						  __FUNCTION__,swId_i,db->num_of_sw_ids);
		return MEA_ERROR;
	}

	/* Check valid hwUnit_i parameter */
	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) > max value(%d) \n",
						  __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}
	
	/* Check valid hwId_io parameter */
	if (hwId_io == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwId_io == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check that is allocated */
	index = (swId_i*mea_drv_num_of_hw_units)+hwUnit_i;
	if (db->SwHw_Table[index].valid) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - swId %d is already allocated (hwUnit=%d)\n",
						  __FUNCTION__,
						  swId_i,
						  hwUnit_i);
		return MEA_ERROR;
	}
	
	if (*hwId_io != MEA_DB_GENERATE_NEW_ID) {
		if (*hwId_io >= db->HwUnit_Table[hwUnit_i].num_of_entries) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - *hwId_io (%d) >= max value (%d) (hwUnit=%d)\n",
							  __FUNCTION__,
							  *hwId_io,
							  db->HwUnit_Table[hwUnit_i].num_of_entries,
							  hwUnit_i);
			return MEA_ERROR;
		}
		if (db->HwUnit_Table[hwUnit_i].table[*hwId_io].valid) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - HwId %d already allocate for HwUnit %d \n",
							  __FUNCTION__,*hwId_io,hwUnit_i);
			return MEA_ERROR;
		}

	} else {
		/* Check if no more free space */
		if (db->HwUnit_Table[hwUnit_i].used_entries == 
			db->HwUnit_Table[hwUnit_i].num_of_entries) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - no free HW entry \n",
						          __FUNCTION__);
  		        return MEA_ERROR;
		}
		
		/* Search for free index */
		for (hwId=1;hwId<db->HwUnit_Table[hwUnit_i].num_of_entries;hwId++) {
		    hw_entry = &(db->HwUnit_Table[hwUnit_i].table[hwId]);
			if (!(hw_entry->valid)) {
				break;
			}
		}
		if (hwId==db->HwUnit_Table[hwUnit_i].num_of_entries) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - no free HW entry (2)\n",
						          __FUNCTION__);
  		        return MEA_ERROR;
		}

		/* Update the output parameter */
	    *hwId_io = hwId;
		
	}

	/* Update the hwUnit global information */
	db->HwUnit_Table[hwUnit_i].used_entries++;

	/* Allocate the new entry */
	save_regs = db->HwUnit_Table[hwUnit_i].table[*hwId_io].regs;
	MEA_OS_memset((char*)(&(db->HwUnit_Table[hwUnit_i].table[*hwId_io])),
		          0,
				  sizeof(MEA_db_Hw_Entry_dbt));
	
	db->HwUnit_Table[hwUnit_i].table[*hwId_io].regs = save_regs;

	MEA_OS_memset((char*)(db->HwUnit_Table[hwUnit_i].table[*hwId_io].regs),
		          0,
				  sizeof(MEA_Uint32)*db->HwUnit_Table[hwUnit_i].num_of_regs);
	db->HwUnit_Table[hwUnit_i].table[*hwId_io].valid = MEA_TRUE;
    db->HwUnit_Table[hwUnit_i].table[*hwId_io].swId  = swId_i;


	/* Update the SwHw_Table */
	index = (swId_i*mea_drv_num_of_hw_units)+hwUnit_i;
	MEA_OS_memset((char*)(&(db->SwHw_Table[index])),
		          0,
				  sizeof(MEA_db_SwHw_Entry_dbt));
	db->SwHw_Table[index].valid             = MEA_TRUE;
	db->SwHw_Table[index].hwId              
		= (*hwId_io);

	/* Return to caller */
	return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_Delete>                                       */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_Delete(MEA_Unit_t                  unit_i,
				         MEA_db_dbt                  db_i,
						 MEA_db_SwId_t               swId_i,
					     MEA_db_HwUnit_t                hwUnit_i)
{


	MEA_db_Entry_dbt    *db = (MEA_db_Entry_dbt*)db_i;
	MEA_db_HwId_t        hwId;
	MEA_Uint32           index;
	long                *save_regs;


	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid hwId_i parameter */
	if (swId_i >= db->num_of_sw_ids) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - swId_i (%d) >= max value (%d) \n",
						  __FUNCTION__,swId_i,db->num_of_sw_ids);
		return MEA_ERROR;
	}

	/* Check valid hwUnit_i parameter */
	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) > max value(%d) \n",
						  __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	/* Check that is allocated */
	index = (swId_i*mea_drv_num_of_hw_units)+hwUnit_i;
	if (!(db->SwHw_Table[index].valid)) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		  	              "%s - HwUnit %d for SwId %d not allocated \n",
			              __FUNCTION__,hwUnit_i,swId_i);
		return MEA_ERROR;
	}


	/* Get the hw_id and verify it in valid range */
	index = (swId_i*mea_drv_num_of_hw_units)+hwUnit_i;
	hwId = db->SwHw_Table[index].hwId;
	if (hwId >= db->HwUnit_Table[hwUnit_i].num_of_entries) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		  	              "%s - HwId (%d) >= max value (%d) (SwId=%d,hwUnit=%d)\n",
			              __FUNCTION__,
						  hwId,
						  db->HwUnit_Table[hwUnit_i].num_of_entries,
						  swId_i,
						  hwUnit_i);
		return MEA_ERROR;
	}

	/* verify that the hwId is real allocated for this hwUnit */
	if (!(db->HwUnit_Table[hwUnit_i].table[hwId].valid)) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	              "%s - hwId %d not allocated (swId=%d,hwUnit=%d)\n",
			              __FUNCTION__,
						  hwId,
						  swId_i,
						  hwUnit_i);
		return MEA_ERROR;
	}

	/* Free the hwId from the HwUnit_Table */
	save_regs = db->HwUnit_Table[hwUnit_i].table[hwId].regs;
	MEA_OS_memset((char*)(&(db->HwUnit_Table[hwUnit_i].table[hwId])),
		          0,
				  sizeof(MEA_db_Hw_Entry_dbt));
	
	db->HwUnit_Table[hwUnit_i].table[hwId].regs = save_regs;
	MEA_OS_memset((char*)(db->HwUnit_Table[hwUnit_i].table[hwId].regs),
		          0,
				  sizeof(MEA_Uint32)*db->HwUnit_Table[hwUnit_i].num_of_regs);
	db->HwUnit_Table[hwUnit_i].used_entries--;


	/* Free the swId from the SwUnit_Table */
	index = (swId_i*mea_drv_num_of_hw_units)+hwUnit_i;
	MEA_OS_memset((char*)&(db->SwHw_Table[index]),
		          0,
				  sizeof(MEA_db_SwHw_Entry_dbt));

	/* Return to Caller */
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_Get_SwHw>                                     */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_Get_SwHw(MEA_Unit_t                  unit_i,
					       MEA_db_dbt                  db_i,
						   MEA_db_SwId_t               swId_i,
                           MEA_db_HwUnit_t                hwUnit_i,
						   MEA_db_SwHw_Entry_dbt*      entry_po) 
{
	MEA_db_Entry_dbt    *db = (MEA_db_Entry_dbt*)db_i;
	MEA_Uint32           index;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid hwId_i parameter */
	if (swId_i >= db->num_of_sw_ids) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - swId_i (%d) >= max value (%d) \n",
						  __FUNCTION__,swId_i,db->num_of_sw_ids);
		return MEA_ERROR;
	}

	/* Check valid hwUnit_i parameter */
	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) > max value(%d) \n",
						  __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	/* Check valid emtru_po parameter */
	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Update the output parameter */
	index = (swId_i*mea_drv_num_of_hw_units)+hwUnit_i;
	MEA_OS_memcpy((char*)entry_po,
		          &(db->SwHw_Table[index]),
				  sizeof(*entry_po));

	/* Return to caller */
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_GetFirst_SwHw>                                */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_GetFirst_SwHw(MEA_Unit_t                  unit_i,
					            MEA_db_dbt                  db_i,
					            MEA_db_SwId_t              *swId_o,
                                MEA_db_HwUnit_t               *hwUnit_o,
						        MEA_db_SwHw_Entry_dbt*      entry_po,
							    MEA_Bool                   *found_o) 
{

	MEA_db_Entry_dbt      *db = (MEA_db_Entry_dbt*)db_i;
	MEA_db_SwHw_Entry_dbt *entry;
	MEA_db_SwId_t          swId;
	MEA_db_HwUnit_t           hwUnit;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid swId_o parameter */
	if (swId_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - swId_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Check valid hwUnit_o parameter */
	if (hwUnit_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid emtru_po parameter */
	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid found_o parameter */
	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - found_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Search for the first entry */
    *found_o = MEA_FALSE;
	entry = &(db->SwHw_Table[0]);
	for (swId=0;swId<db->num_of_sw_ids;swId++) {
		for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++,entry++) {
			if (entry->valid) {
		        *found_o   = MEA_TRUE;
		        *swId_o    = swId;
		        *hwUnit_o  = hwUnit;
		        MEA_OS_memcpy((char*)entry_po,
			                  entry,
					          sizeof(*entry_po));
				return MEA_OK;
			}
		}
	}

	/* Return to caller */
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_GetNext_SwHw>                                 */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_GetNext_SwHw(MEA_Unit_t                  unit_i,
					           MEA_db_dbt                  db_i,
					           MEA_db_SwId_t              *swId_io,
                               MEA_db_HwUnit_t               *hwUnit_io,
						       MEA_db_SwHw_Entry_dbt*      entry_po,
							   MEA_Bool                   *found_o) 
{

	MEA_db_Entry_dbt      *db = (MEA_db_Entry_dbt*)db_i;
	MEA_db_SwHw_Entry_dbt *entry;
	MEA_db_SwId_t          swId;
	MEA_db_HwUnit_t           hwUnit;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid swId_io parameter */
	if (swId_io == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - swId_io == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Check valid hwUnit_io parameter */
	if (hwUnit_io == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_io == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid emtru_po parameter */
	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid found_o parameter */
	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - found_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Search for the next entry */
	*found_o = MEA_FALSE;
	entry = &(db->SwHw_Table[((*swId_io)*mea_drv_num_of_hw_units)+(*hwUnit_io)]);
	entry++;
	swId=(*swId_io);
    hwUnit=(*hwUnit_io)+1;
	for (;swId < db->num_of_sw_ids;swId++) {
		for (;hwUnit < mea_drv_num_of_hw_units;hwUnit++,entry++) {
			if (entry->valid) {
		        *found_o   = MEA_TRUE;
		        *swId_io   = swId;
		        *hwUnit_io = hwUnit;
		        MEA_OS_memcpy((char*)entry_po,
			                  entry,
					          sizeof(*entry_po));
				return MEA_OK;
			}
		}
		hwUnit=0;
	}

	/* Return to caller */
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_Get_HwUnit>                                   */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_Get_HwUnit(MEA_Unit_t                  unit_i,
					         MEA_db_dbt                  db_i,
                             MEA_db_HwUnit_t                hwUnit_i,
						     MEA_db_HwUnit_Entry_dbt    *entry_po) 
{
	MEA_db_Entry_dbt    *db = (MEA_db_Entry_dbt*)db_i;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Check valid hwUnit_i parameter */
	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) > max value(%d) \n",
						  __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	/* Check valid emtru_po parameter */
	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Update the output parameter */
	MEA_OS_memcpy((char*)entry_po,
		          &(db->HwUnit_Table[hwUnit_i]),
				  sizeof(*entry_po));

	/* Return to caller */
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_GetFirst_HwUnit>                              */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_GetFirst_HwUnit(MEA_Unit_t                  unit_i,
					              MEA_db_dbt                  db_i,
                                  MEA_db_HwUnit_t               *hwUnit_o,
						          MEA_db_HwUnit_Entry_dbt    *entry_po,
							      MEA_Bool                   *found_o) 
{

	MEA_db_Entry_dbt      *db = (MEA_db_Entry_dbt*)db_i;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Check valid hwUnit_o parameter */
	if (hwUnit_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid emtru_po parameter */
	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid found_o parameter */
	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - found_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Check if we have hardware units */
	if (mea_drv_num_of_hw_units == 0) {
		*found_o = MEA_FALSE;
	}

	/* Update output parameters */
	*found_o   = MEA_TRUE;
	*hwUnit_o  = 0;
	MEA_OS_memcpy((char*)entry_po,
		          &(db->HwUnit_Table[(*hwUnit_o)]),
				  sizeof(*entry_po));


	/* Return to caller */
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_GetNext_HwUnit>                               */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_GetNext_HwUnit(MEA_Unit_t                  unit_i,
					             MEA_db_dbt                  db_i,
                                 MEA_db_HwUnit_t               *hwUnit_io,
						         MEA_db_HwUnit_Entry_dbt    *entry_po,
							     MEA_Bool                   *found_o) 
{

	MEA_db_Entry_dbt      *db = (MEA_db_Entry_dbt*)db_i;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid hwUnit_io parameter */
	if (hwUnit_io == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_io == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid emtru_po parameter */
	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid found_o parameter */
	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - found_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check if we have hardware units */
	if (*hwUnit_io + 1 >= mea_drv_num_of_hw_units) {
		*found_o = MEA_FALSE;
		return MEA_OK;
	}

	/* Update output parameters */
	*found_o   = MEA_TRUE;
	(*hwUnit_io)++;
	MEA_OS_memcpy((char*)entry_po,
		          &(db->HwUnit_Table[(*hwUnit_io)]),
				  sizeof(*entry_po));

	/* Return to caller */
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_Get_Hw>                                       */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_Get_Hw(MEA_Unit_t                  unit_i,
					     MEA_db_dbt                  db_i,
                         MEA_db_HwUnit_t                hwUnit_i,
					     MEA_db_HwId_t               hwId_i,
						 MEA_db_Hw_Entry_dbt*        entry_po) 
{
	MEA_db_Entry_dbt    *db = (MEA_db_Entry_dbt*)db_i;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Check valid hwUnit_i parameter */
	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) > max value(%d) \n",
						  __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	/* Check valid hwId_i parameter */
	if (hwId_i >= db->HwUnit_Table[hwUnit_i].num_of_entries) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwId_i (%d) >= max value (%d) \n",
						  __FUNCTION__,hwId_i,db->HwUnit_Table[hwUnit_i].num_of_entries);
		return MEA_ERROR;
	}

	/* Check valid emtru_po parameter */
	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Update the output parameter */
	MEA_OS_memcpy((char*)entry_po,
		          &(db->HwUnit_Table[hwUnit_i].table[hwId_i]),
				  sizeof(*entry_po));

	/* Return to caller */
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_GetFirst_Hw>                                  */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_GetFirst_Hw(MEA_Unit_t                  unit_i,
					          MEA_db_dbt                  db_i,
                              MEA_db_HwUnit_t                hwUnit_i,
					          MEA_db_HwId_t              *hwId_o,
						      MEA_db_Hw_Entry_dbt        *entry_po,
							  MEA_Bool                   *found_o) 
{

	MEA_db_Entry_dbt      *db = (MEA_db_Entry_dbt*)db_i;
	MEA_db_Hw_Entry_dbt   *entry;
	MEA_db_HwId_t          hwId;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid hwUnit_i parameter */
	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) > max value(%d) \n",
						  __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	/* Check valid hwId_o parameter */
	if (hwId_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwId_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid entry_po parameter */
	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}
	
	/* Check valid found_o parameter */
	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - found_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Search for the first valid entry */
	for (hwId=0 , entry = &(db->HwUnit_Table[hwUnit_i].table[hwId]); 
		 hwId < db->HwUnit_Table[hwUnit_i].num_of_entries && (!(entry->valid));
		 hwId++,entry++);

	/* Check if the entry found */
	if (hwId   < db->HwUnit_Table[hwUnit_i].num_of_entries) {
		*found_o   = MEA_TRUE;
		*hwId_o    = hwId;
		MEA_OS_memcpy((char*)entry_po,
			          entry,
					  sizeof(*entry_po));
	} else {
		*found_o = MEA_FALSE;
	}


	/* Return to caller */
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_GetNext_Hw>                                   */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_GetNext_Hw(MEA_Unit_t                  unit_i,
					         MEA_db_dbt                  db_i,
                             MEA_db_HwUnit_t                hwUnit_i,
					         MEA_db_HwId_t              *hwId_io,
						     MEA_db_Hw_Entry_dbt        *entry_po,
							 MEA_Bool                   *found_o) 
{

	MEA_db_Entry_dbt      *db = (MEA_db_Entry_dbt*)db_i;
	MEA_db_Hw_Entry_dbt   *entry;
	MEA_db_HwId_t          hwId;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid hwUnit_i parameter */
	if (hwUnit_i >= mea_drv_num_of_hw_units) {

		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) > max value(%d) \n",
						  __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	/* Check valid hwId_io parameter */
	if (hwId_io == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwId_io == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Check valid emtru_po parameter */
	if (entry_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry_po == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Check valid found_o parameter */
	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - found_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Search for the first valid entry */
	for (hwId=(*hwId_io)+1 , entry = &(db->HwUnit_Table[hwUnit_i].table[hwId]); 
		 hwId < db->HwUnit_Table[hwUnit_i].num_of_entries && (!(entry->valid));
		 hwId++,entry++);

	/* Check if the entry found */
	if (hwId   < db->HwUnit_Table[hwUnit_i].num_of_entries) {
		*found_o   = MEA_TRUE;
		*hwId_io   = hwId;
		MEA_OS_memcpy((char*)entry_po,
			          entry,
					  sizeof(*entry_po));
	} else {
		*found_o = MEA_FALSE;
	}

	/* Return to caller */
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_Set_Hw_Regs>                                  */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_Set_Hw_Regs(MEA_Unit_t                  unit_i,
					          MEA_db_dbt                  db_i,
                              MEA_db_HwUnit_t                hwUnit_i,
					          MEA_db_HwId_t               hwId_i,
						      MEA_Uint16                  num_of_regs_i,
						      MEA_Uint32*                 regs_pi) 
{
	MEA_db_Entry_dbt    *db = (MEA_db_Entry_dbt*)db_i;

	/* Check valid db_i parameter */
	if (db_i == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - db_i == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Check valid hwUnit_i parameter */
	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) > max value(%d) \n",
						  __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	/* Check valid hwId_i parameter */
	if (hwId_i >= db->HwUnit_Table[hwUnit_i].num_of_entries) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwId_i (%d) >= max value (%d) \n",
						  __FUNCTION__,hwId_i,db->HwUnit_Table[hwUnit_i].num_of_entries);
		return MEA_ERROR;
	}

	/* num_of_regs not equal to the basic width config */
    if (num_of_regs_i > db->HwUnit_Table[hwUnit_i].num_of_regs) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - num_of_regs_i (%d) > width (%d) \n",
						  __FUNCTION__,
						  num_of_regs_i,
						  db->HwUnit_Table[hwUnit_i].num_of_regs);
		return MEA_ERROR;
	}

	/* Check valid emtru_po parameter */
	if (regs_pi == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - regs_pi == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Update the output parameter */
	MEA_OS_memcpy((char*)(db->HwUnit_Table[hwUnit_i].table[hwId_i].regs),
		          (char*)regs_pi,
				  num_of_regs_i*sizeof(MEA_Uint32));

	/* Return to caller */
	return MEA_OK;
}

static void mea_db_writeHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t           unit_i   = (MEA_Unit_t   )arg1;
    MEA_module_te        module_i = (MEA_module_te)arg2;
	MEA_Uint32           count_i  = (MEA_Uint32   )((long)arg3);
    MEA_Uint32*          regs_i   = (MEA_Uint32*  )arg4;
    MEA_Uint32 i;

    for (i=0;i<count_i;i++) {
        MEA_API_WriteReg(unit_i,
                         (module_i==MEA_MODULE_BM) 
                            ? MEA_BM_IA_WRDATA(i)
                            : MEA_IF_WRITE_DATA_REG(i),
				         regs_i[i],
                         module_i);
    }

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_db_ReInit>                                       */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_db_ReInit(MEA_Unit_t       unit_i,
    MEA_db_dbt       db_i,
    MEA_Uint32       tableType_i,
    MEA_module_te    module_i,
    MEA_db_ReInit_callback_func_t callback_i,
    MEA_Bool   Is_DDR)
{
    MEA_db_Entry_dbt        *db = (MEA_db_Entry_dbt*)db_i;
    MEA_db_HwUnit_t          hwUnit=0;
    MEA_db_HwId_t            hwId;
    MEA_db_HwUnit_Entry_dbt *entry;
    MEA_ind_write_t          ind_write;
    mea_memory_mode_e       save_globalMemoryMode;
    MEA_Uint32              addressDDR;
    MEA_Uint32              value[MEA_ACTION_ADDRES_DDR_MAX_WIDTH];
    //MEA_Uint32  group = 0;

    /* Check valid db_i parameter */
    if (db_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - db_i == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (Is_DDR == MEA_TRUE){
        
        
        if (tableType_i == ENET_IF_CMD_PARAM_TBL_TYP_ACTION){
            for (hwUnit = 0; hwUnit < mea_drv_num_of_hw_units; hwUnit++) {
                /* Set entry pointer */
                entry = &(db->HwUnit_Table[hwUnit]);
                //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "start TYP_ACTION \n");
                if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit_i, MEA_DB_ACTION_TYPE) == MEA_TRUE){
                    /*write to ACTION DB on DDR */
                    for (hwId = 0; hwId <= 2; hwId++)
                    { /* because we see after reset the 0 : 0x5c */
                        MEA_OS_memset(&value, 0, sizeof(value));

                        addressDDR = MEA_ACTION_BASE_ADDRES_DDR_START + ((hwId * (MEA_ACTION_BASE_ADDRES_DDR_WIDTH) * 4));
                        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "TYP_ACTION address 0x%x\n", addressDDR);
                        MEA_OS_memcpy(&value[0], &entry->table[hwId].regs[0], MEA_ADDRES_DDR_BYTE * MEA_ACTION_ADDRES_DDR_REGS);
                        MEA_DDR_Lock();
                            MEA_DB_DDR_HW_ACCESS_ENABLE 
                            if (MEA_OS_Device_DATA_DDR_RW(MEA_MODULE_DATA_DDR,
                                1,/*0 read 1- write */
                                addressDDR,
                                MEA_ADDRES_DDR_BYTE * MEA_ACTION_BASE_ADDRES_DDR_WIDTH,
                                &value[0]
                                ) != MEA_OK) {
                                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR MODULE_DATA_DDR  Write to DDR \n");
                                MEA_DB_DDR_HW_ACCESS_DISABLE
                                MEA_DDR_Unlock();
                                return MEA_ERROR;
                            }
                            MEA_DB_DDR_HW_ACCESS_DISABLE
                            MEA_DDR_Unlock();
                    }//for
                }
            }
            //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "end TYP_ACTION \n");
        }

       
    
    }
    else{

        MEA_OS_memset(&ind_write, 0, sizeof(ind_write));
        ind_write.tableType = tableType_i;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_db_writeHwEntry;
        if (module_i == MEA_MODULE_BM) {
            ind_write.cmdReg = MEA_BM_IA_CMD;
            ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
            ind_write.statusReg = MEA_BM_IA_STAT;
            ind_write.statusMask = MEA_BM_IA_STAT_MASK;
            ind_write.tableAddrReg = MEA_BM_IA_ADDR;
            ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        }
        else {
            ind_write.cmdReg = MEA_IF_CMD_REG;
            ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
            ind_write.statusReg = MEA_IF_STATUS_REG;
            ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
            ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
            ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        }



        for (hwUnit = 0; hwUnit < mea_drv_num_of_hw_units; hwUnit++) {
            MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode, hwUnit);
            for (hwId = 0;
                hwId < db->HwUnit_Table[hwUnit].num_of_entries; 
                hwId++) {

                /* Set entry pointer */
                entry = &(db->HwUnit_Table[hwUnit]);

                /* Check if entry is valid */
                if (!entry->table[hwId].valid) {
                    continue;
                }

                /* Write to Hardware */
                ind_write.tableType = tableType_i;
                ind_write.tableOffset = (MEA_Uint32)hwId;
                ind_write.funcParam1 = (MEA_funcParam)unit_i;
                ind_write.funcParam2 = (MEA_funcParam)module_i;
                ind_write.funcParam3 = (MEA_funcParam)((long)entry->num_of_regs);
                ind_write.funcParam4 = (MEA_funcParam)(entry->table[hwId].regs);
                if (tableType_i == ENET_IF_CMD_PARAM_TBL_TYP_ACTION){
                    ind_write.funcParam3 = (MEA_funcParam)(long)(ENET_IF_CMD_PARAM_TBL_TYP_ACTION_NUM_OF_REGS_Internal(hwUnit));
                }

                if (callback_i != NULL) {

                    if (callback_i(unit_i, &ind_write) != MEA_OK) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - callback_i failed "
                            "TableType=%d , TableOffset=%d , mea_module=%d\n",
                            __FUNCTION__,
                            ind_write.tableType,
                            ind_write.tableOffset,
                            module_i);
                        return MEA_ERROR;
                    }
                }
                if (MEA_API_WriteIndirect(unit_i,
                    &ind_write,
                    module_i) == MEA_ERROR) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_WriteIndirect failed "
                        "TableType=%d , TableOffset=%d , mea_module=%d\n",
                        __FUNCTION__,
                        ind_write.tableType,
                        ind_write.tableOffset,
                        module_i);
                    return MEA_ERROR;
                }
            }
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        }
    }

    /* Return to caller */
    return MEA_OK;
}


