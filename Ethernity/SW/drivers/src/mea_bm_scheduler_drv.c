/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*-------------------------------- Includes ------------------------------------------*/

#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_bm_scheduler_drv.h"
#include "mea_drv_common.h"
#include "mea_db_drv.h"

/*-------------------------------- Definitions ---------------------------------------*/

		       

/*-------------------------------- External Functions --------------------------------*/


/*-------------------------------- External Variables --------------------------------*/




/*-------------------------------- Forward Declarations ------------------------------*/

/*-------------------------------- Local Variables -----------------------------------*/

/*-------------------------------- Global Variables ----------------------------------*/
static MEA_Uint32              *MEA_Scheduler_Length = NULL;
static MEA_Scheduler_Entry_dbt *MEA_Scheduler_Table = NULL;
static MEA_db_dbt               MEA_Scheduler_db = NULL;

MEA_Bool mea_reinit_Scheduler_length =MEA_FALSE;

MEA_Status mea_drv_Scheduler_Length_UpdateHw(MEA_Unit_t unit_i,
                                             MEA_Uint32* new_value,
                                             MEA_Uint32* old_value) 
{

    MEA_Uint32 val;

    if ((!mea_reinit_Scheduler_length)&&
        (old_value != NULL) &&
        (*old_value == *new_value)) {
        return MEA_OK;
    }

    val = MEA_API_ReadReg(MEA_UNIT_0,MEA_BM_PRE_SCH_CY_LEN , MEA_MODULE_BM);

    val &= ~MEA_BM_PRE_SCH_CY_LEN_MASK;
    val |= ((*new_value) <<
            MEA_OS_calc_shift_from_mask(MEA_BM_PRE_SCH_CY_LEN_MASK));

    MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_PRE_SCH_CY_LEN,val,MEA_MODULE_BM);

    return MEA_OK;
}



MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Scheduler_num_of_sw_size_func,
                                         MEA_OS_MAX(ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(0),
                                                    ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(1)),
                                         0)

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Scheduler_num_of_hw_size_func,
                                         ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(hwUnit_i),
                                         ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_NUM_OF_REGS(hwUnit_i))

MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_Scheduler_db,
                             MEA_Scheduler_db) 

#if 0
MEA_Status mea_drv_Init_Scheduler_Table(MEA_Unit_t unit_i) 
{

    MEA_Uint32 size;

    if (MEA_Scheduler_Table != NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Already init \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Init db - Hardware shadow */
    if (mea_db_Init(unit_i,
                    "Scheduler ",
                    mea_drv_Get_Scheduler_num_of_sw_size_func,
                    mea_drv_Get_Scheduler_num_of_hw_size_func,
                    &MEA_Scheduler_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Allocate main MEA_Scheduler_Table - array of arrays (one per each hwUnit) */
    size = MEA_MAX_NUM_OF_PRE_SCHEDULER_ENTRIES*sizeof(MEA_Scheduler_Entry_dbt);
    MEA_Scheduler_Table = (MEA_Scheduler_Entry_dbt*)MEA_OS_malloc(size);
    if (MEA_Scheduler_Table==NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate MEA_Scheduler_Table failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    MEA_OS_memset(MEA_Scheduler_Table,0,size);

    size = mea_drv_num_of_hw_units*sizeof(MEA_Uint32);
    MEA_Scheduler_Length = (MEA_Uint32*)MEA_OS_malloc(size);
    if (MEA_Scheduler_Length == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Scheduler_Length allocate failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    MEA_OS_memset(MEA_Scheduler_Length,0,size);

    return MEA_OK;

}


MEA_Status mea_drv_Conclude_Scheduler_Table(MEA_Unit_t unit_i) 
{

    if (MEA_Scheduler_Length != NULL) {
        MEA_OS_free(MEA_Scheduler_Length);
        MEA_Scheduler_Length=NULL;
    }

    if (MEA_Scheduler_Table != NULL) {
        MEA_OS_free(MEA_Scheduler_Table);
        MEA_Scheduler_Table=NULL;
    }

    if (MEA_Scheduler_db != NULL) {
        if (mea_db_Conclude(unit_i,&MEA_Scheduler_db) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_dn_Conclude  failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }
    }


    return MEA_OK;

}

MEA_Status mea_drv_ReInit_Scheduler_Table(MEA_Unit_t unit_i) 
{

    mea_memory_mode_e save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit;

    
    if (mea_db_ReInit(unit_i,
                      MEA_Scheduler_db,
                      MEA_BM_TBL_TYP_SCHEDULER_PRIORITY,
                      MEA_MODULE_BM,
                      NULL,
                      MEA_FALSE) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        if (mea_drv_Scheduler_Length_UpdateHw(unit_i,
                                              &(MEA_Scheduler_Length[hwUnit]),
                                              NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Scheduler_Length_UpdateHw failed \n",
                              __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }

    return MEA_OK;

}
#endif




static void mea_drv_Scheduler_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    MEA_Unit_t                   unit_i   = (MEA_Unit_t              )arg1;
    MEA_db_HwUnit_t              hwUnit_i = (MEA_db_HwUnit_t         )((long) arg2);
    MEA_db_HwId_t                hwId_i   = (MEA_db_HwId_t           )((long)arg3);
    MEA_Scheduler_Entry_dbt     *entry_pi = (MEA_Scheduler_Entry_dbt*)arg4;
    MEA_Uint32                   val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32                   i;
    MEA_Uint16                   num_of_regs;

    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),  
                             ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_NUM_OF_REGS(hwUnit_i));
    for (i=0;i<num_of_regs;i++) {
        val[i] = 0;
    }

    /* build word 1 */
    if (entry_pi->valid) {
        val[0] = entry_pi->port;
    }

    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }

    if (mea_db_Set_Hw_Regs(unit_i,
                           MEA_Scheduler_db,
                           hwUnit_i,
                           hwId_i,
                           num_of_regs,
                           val) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                          __FUNCTION__,
                          hwUnit_i,
                          hwId_i);
    }


}

static MEA_Status mea_drv_Scheduler_UpdateHw(MEA_Unit_t               unit_i,
                                             MEA_Uint32               slot_i,
                                             MEA_db_HwUnit_t          hwUnit_i,
                                             MEA_db_HwId_t            hwId_i,
                                             MEA_Scheduler_Entry_dbt* new_entry_pi,
                                             MEA_Scheduler_Entry_dbt* old_entry_pi)
{

    MEA_ind_write_t ind_write;

    if (mea_reinit_Scheduler_length == MEA_FALSE){
    if ((old_entry_pi!= NULL) &&
        (MEA_OS_memcmp(new_entry_pi,
                       old_entry_pi,
                       sizeof(*new_entry_pi))==0)) {
        return MEA_OK;
    }
}

    ind_write.tableType		= ENET_BM_TBL_TYP_SCHEDULER_PRIORITY;    
    ind_write.tableOffset   = hwId_i;
    ind_write.cmdReg		= MEA_BM_IA_CMD;      
    ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg		= MEA_BM_IA_STAT;   
    ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
    ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_Scheduler_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit_i);
    ind_write.funcParam3 = (MEA_funcParam)((long) hwId_i);
    ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect failed "
                          "TableType=%d , TableOffset=%d , mea_module=%d\n",
                          __FUNCTION__,
                          ind_write.tableType,
                          ind_write.tableOffset,
                          MEA_MODULE_BM);
 	    return MEA_ERROR;
    }

    return MEA_OK;


}



MEA_Status MEA_API_Get_Scheduler_Entry (MEA_Unit_t               unit_i,
                                        MEA_Uint32               direction_i,
                                        MEA_Uint32               slot_i,
                                        MEA_Scheduler_Entry_dbt *entry_po) 
{

    mea_memory_mode_e save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit,tmp_hwUnit;
    MEA_Uint32 index;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (MEA_Scheduler_Table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Not init \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry_po == NULL (slot_i=%d)  \n",__FUNCTION__,slot_i);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    switch(direction_i) {
    case MEA_DIRECTION_GENERAL:
        switch(globalMemoryMode) {
        case MEA_MODE_REGULAR_ENET3000:
            hwUnit= MEA_HW_UNIT_ID_GENERAL;
            break;
        case MEA_MODE_UPSTREEM_ONLY:
            hwUnit= MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_MODE_DOWNSTREEM_ONLY:
            hwUnit= MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        case MEA_MODE_REGULAR_ENET4000:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
            break;
        }
        break;
    case MEA_DIRECTION_UPSTREAM:
        if ((globalMemoryMode != MEA_MODE_REGULAR_ENET4000) &&
            (globalMemoryMode != MEA_MODE_UPSTREEM_ONLY   )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
        }
        hwUnit= MEA_HW_UNIT_ID_UPSTREAM;
        break;
    case MEA_DIRECTION_DOWNSTREAM:
        if ((globalMemoryMode != MEA_MODE_REGULAR_ENET4000) &&
            (globalMemoryMode != MEA_MODE_DOWNSTREEM_ONLY   )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
        }
        hwUnit= MEA_HW_UNIT_ID_DOWNSTREAM;
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown direction %d \n",
                              __FUNCTION__,direction_i);
        return MEA_ERROR;
    }

    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (slot_i >= (MEA_Uint32)ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(hwUnit)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Invalid slot_i %d  \n",__FUNCTION__,slot_i);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    for (tmp_hwUnit=0,index=0;
         tmp_hwUnit<hwUnit;
         index+=ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(tmp_hwUnit),tmp_hwUnit++);
    index += slot_i;




    MEA_OS_memcpy (entry_po,
                   &(MEA_Scheduler_Table[index]),
                   sizeof(*entry_po));

    
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;

}

MEA_Status MEA_API_Set_Scheduler_Entry (MEA_Unit_t               unit_i,
                                        MEA_Uint32               direction_i,
                                        MEA_Uint32               slot_i,
                                        MEA_Scheduler_Entry_dbt *entry_pi) 
{

    mea_memory_mode_e save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit,tmp_hwUnit;
    MEA_db_HwId_t   hwId;
    MEA_Uint32 index;
    MEA_Scheduler_Entry_dbt *old_entry;


    if(MEA_GLOBAL_ISPRESCHED_ENABLE == MEA_FALSE){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry_pi == NULL (slot_i=%d)  \n",__FUNCTION__,slot_i);
        return MEA_ERROR;
    }



#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (MEA_Scheduler_Table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Not init \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry_pi == NULL (slot_i=%d)  \n",__FUNCTION__,slot_i);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    switch(direction_i) {
    case MEA_DIRECTION_GENERAL:
        switch(globalMemoryMode) {
        case MEA_MODE_REGULAR_ENET3000:
            hwUnit= MEA_HW_UNIT_ID_GENERAL;
            break;
        case MEA_MODE_UPSTREEM_ONLY:
            hwUnit= MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_MODE_DOWNSTREEM_ONLY:
            hwUnit= MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        case MEA_MODE_REGULAR_ENET4000:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
            break;
        }
        break;
    case MEA_DIRECTION_UPSTREAM:
        if ((globalMemoryMode != MEA_MODE_REGULAR_ENET4000) &&
            (globalMemoryMode != MEA_MODE_UPSTREEM_ONLY   )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
        }
        hwUnit= MEA_HW_UNIT_ID_UPSTREAM;
        break;
    case MEA_DIRECTION_DOWNSTREAM:
        if ((globalMemoryMode != MEA_MODE_REGULAR_ENET4000) &&
            (globalMemoryMode != MEA_MODE_DOWNSTREEM_ONLY   )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
        }
        hwUnit= MEA_HW_UNIT_ID_DOWNSTREAM;
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown direction %d \n",
                              __FUNCTION__,direction_i);
        return MEA_ERROR;
    }

    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (slot_i >= (MEA_Uint32)ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(hwUnit)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Invalid slot_i %d  \n",__FUNCTION__,slot_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    for (tmp_hwUnit=0,index=0;
         tmp_hwUnit<hwUnit;
         index+=ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(tmp_hwUnit),tmp_hwUnit++);
    index += slot_i;
	

    if (entry_pi->valid) {
        if(entry_pi->port<=MEA_MAX_PORT_NUMBER && entry_pi->type == 0 ){
        if (!MEA_API_Get_IsPortValid(entry_pi->port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - entry_pi->port %d is not valid \n",
                              __FUNCTION__,
                              entry_pi->port);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        }

        if (globalMemoryMode == MEA_MODE_UPSTREEM_ONLY) {
            if ((entry_pi->port != 118) && 
                (entry_pi->port !=  24) && 
                (entry_pi->port != 127)  ) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - entry->port %d is not valid for Upstream (direction_i=%d,slot_i=%d)\n",
                                  __FUNCTION__,entry_pi->port,direction_i,slot_i);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
        }

        if (globalMemoryMode == MEA_MODE_DOWNSTREEM_ONLY) {
            if (entry_pi->port == 118) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - entry->port %d is not valid for Downstream (direction_i=%d,slot_i=%d)\n",
                                  __FUNCTION__,entry_pi->port,direction_i,slot_i);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
        }
    }

    old_entry = &(MEA_Scheduler_Table[index]);
    hwId = (MEA_db_HwId_t)slot_i;

    if ((entry_pi->valid  == MEA_TRUE ) &&
        (old_entry->valid == MEA_FALSE)  ) {
        if (mea_db_Create(unit_i,
                          MEA_Scheduler_db,
                          (MEA_db_SwId_t)slot_i,
                          hwUnit,
                          &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Create failed \n",__FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }


    if (mea_drv_Scheduler_UpdateHw(unit_i,
                                   slot_i,
                                   hwUnit,
                                   hwId,
                                   entry_pi,
                                   old_entry
                                  ) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Scheduler_UpdateHw failed (hwUnit=%d,slot=%d)\n",
                         __FUNCTION__,hwUnit,slot_i);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR;
    }

    if ((entry_pi->valid  == MEA_FALSE) &&
        (old_entry->valid == MEA_TRUE )  ) {
        if (mea_db_Delete(unit_i,
                          MEA_Scheduler_db,
                          (MEA_db_SwId_t)slot_i,
                          hwUnit) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Delete failed \n",__FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }

    MEA_OS_memcpy (old_entry,entry_pi,sizeof(*old_entry));
    
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;

}

MEA_Status MEA_API_Get_Scheduler_Length(MEA_Unit_t  unit_i,
                                        MEA_Uint32  direction_i,
                                        MEA_Uint32 *length_o) 
{

    mea_memory_mode_e save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (MEA_Scheduler_Length == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Not init \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    switch(direction_i) {
    case MEA_DIRECTION_GENERAL:
        switch(globalMemoryMode) {
        case MEA_MODE_REGULAR_ENET3000:
            hwUnit= MEA_HW_UNIT_ID_GENERAL;
            break;
        case MEA_MODE_UPSTREEM_ONLY:
            hwUnit= MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_MODE_DOWNSTREEM_ONLY:
            hwUnit= MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        case MEA_MODE_REGULAR_ENET4000:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
            break;
        }
        break;
    case MEA_DIRECTION_UPSTREAM:
        if ((globalMemoryMode != MEA_MODE_REGULAR_ENET4000) &&
            (globalMemoryMode != MEA_MODE_UPSTREEM_ONLY   )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
        }
        hwUnit= MEA_HW_UNIT_ID_UPSTREAM;
        break;
    case MEA_DIRECTION_DOWNSTREAM:
        if ((globalMemoryMode != MEA_MODE_REGULAR_ENET4000) &&
            (globalMemoryMode != MEA_MODE_DOWNSTREEM_ONLY   )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
        }
        hwUnit= MEA_HW_UNIT_ID_DOWNSTREAM;
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown direction %d \n",
                              __FUNCTION__,direction_i);
        return MEA_ERROR;
    }

    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (length_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - length_o == NULL  \n",__FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



    *length_o = MEA_Scheduler_Length[hwUnit];
    
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;

}


MEA_Status MEA_API_Set_Scheduler_Length(MEA_Unit_t unit_i,
                                        MEA_Uint32 direction_i,
                                        MEA_Uint32 length_i) 
{

    mea_memory_mode_e save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (MEA_Scheduler_Length == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Not init \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    switch(direction_i) {
    case MEA_DIRECTION_GENERAL:
        switch(globalMemoryMode) {
        case MEA_MODE_REGULAR_ENET3000:
            hwUnit= MEA_HW_UNIT_ID_GENERAL;
            break;
        case MEA_MODE_UPSTREEM_ONLY:
            hwUnit= MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_MODE_DOWNSTREEM_ONLY:
            hwUnit= MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        case MEA_MODE_REGULAR_ENET4000:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
            break;
        }
        break;
    case MEA_DIRECTION_UPSTREAM:
        if ((globalMemoryMode != MEA_MODE_REGULAR_ENET4000) &&
            (globalMemoryMode != MEA_MODE_UPSTREEM_ONLY   )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
        }
        hwUnit= MEA_HW_UNIT_ID_UPSTREAM;
        break;
    case MEA_DIRECTION_DOWNSTREAM:
        if ((globalMemoryMode != MEA_MODE_REGULAR_ENET4000) &&
            (globalMemoryMode != MEA_MODE_DOWNSTREEM_ONLY   )  ) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - direction %d not support for this platform (globalMemoryMode=%d)\n",
                              __FUNCTION__,direction_i,globalMemoryMode);
            return MEA_ERROR;
        }
        hwUnit= MEA_HW_UNIT_ID_DOWNSTREAM;
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown direction %d \n",
                              __FUNCTION__,direction_i);
        return MEA_ERROR;
    }

    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
if (length_i > (MEA_Uint32)ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(hwUnit))
{
    length_i= (MEA_Uint32)ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(hwUnit);
}

    if (length_i >= (MEA_Uint32)ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(hwUnit)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Invalid length_i %d  \n",__FUNCTION__,length_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



    if (mea_drv_Scheduler_Length_UpdateHw(unit_i,
                                          &length_i,
                                          &(MEA_Scheduler_Length[hwUnit])
                                         ) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Scheduler_Length_UpdateHw failed (hwUnit=%d)\n",
                         __FUNCTION__,hwUnit);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR;
    }

    MEA_Scheduler_Length[hwUnit] = length_i;
    
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;


}

