/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/






/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_if_regs.h"

#include "mea_hc_decomp_drv.h"






/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct{
    MEA_Bool valid;
    MEA_Uint32 num_of_owners;
    MEA_HDC_Data_dbt data;
    MEA_Uint32   byteCount;
}MEA_HC_Decomp_prof_entry_dbt;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/


static MEA_HC_Decomp_prof_entry_dbt  *MEA_HC_Decomp_prof_info_Table=NULL;

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/



static void mea_drv_HC_Decomp_Bitmap_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;
   
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry;




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}


static MEA_Status mea_drv_HC_Decomp_Bitmap_prof_UpdateHw(MEA_Unit_t                  unit_i,
                                                               MEA_Uint16             id_i,
                                                               MEA_HDC_Data_dbt    *new_entry_pi,
                                                               MEA_HDC_Data_dbt    *old_entry_pi)
{

    MEA_db_HwUnit_t       hwUnit;
   MEA_Uint32              value,index;
   MEA_ind_write_t      ind_write;
    

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

        /* check if we have any changes */
        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi))== 0) ) { 
                return MEA_OK;
        }

        
        
        /* Prepare ind_write structure */
        ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_HC_DECOMPRESS_BIT_PROF;
        ind_write.tableOffset   = id_i; 
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_HC_Decomp_Bitmap_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
        //ind_write.funcParam3    = (MEA_Uint32);

       
        
        for (index=0;index< MEA_HD_NM_WORD;index++) 
        {
        
            ind_write.tableOffset   = (id_i*MEA_HD_NM_WORD)+index ;
            value=new_entry_pi->bit_Of_byte_comp[index];
            ind_write.funcParam4 = (MEA_funcParam)((long)value);

            /* Write to the Indirect Table */
            if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                    __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
                return ENET_ERROR;
            }
        }


        /* Return to caller */
        return MEA_OK;
}


static void mea_drv_HC_Decomp_ByteCnt_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry;
    




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}
static MEA_Status mea_drv_HC_Decomp_ByteCnt_prof_UpdateHw(MEA_Unit_t                  unit_i,
                                                          MEA_Uint16             id_i,
                                                          MEA_Uint32             setByteCnt)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32              value;
    MEA_ind_write_t      ind_write;


    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    



        /* Prepare ind_write structure */
        ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_HC_PROF_DECOMPRESS_BYTE_COUNT;
        ind_write.tableOffset   = id_i; 
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_HC_Decomp_ByteCnt_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
        //ind_write.funcParam3    = (MEA_funcParam);



       

            ind_write.tableOffset   = (id_i) ;
            value=setByteCnt;
            ind_write.funcParam4 = (MEA_funcParam)((long)value);

            /* Write to the Indirect Table */
            if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                    __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
                return ENET_ERROR;
            }
        


        /* Return to caller */
        return MEA_OK;
}



static void mea_drv_HC_Decomp_ByteMap_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  *entry         = (MEA_Uint32  *)arg4;




    MEA_Uint32                 val[2];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry[0];
    val[1]=entry[1];




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}

static MEA_Status mea_drv_HC_Decomp_ByteMap_prof_UpdateHw(MEA_Unit_t                  unit_i,
                                                          MEA_Uint16             id_i,
                                                          MEA_Uint8             *aryByte)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32            value[2];
    MEA_ind_write_t      ind_write;
    MEA_Uint32           index,startFrom;
    

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)





        /* Prepare ind_write structure */
    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_HC_DECOMPRESS_BYTE_MAP_PROF;
    ind_write.tableOffset   = id_i; 
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg     = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_HC_Decomp_ByteMap_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
    //ind_write.funcParam3    = (MEA_funcParam);


    startFrom=0;
    for (index=0;index< MEA_HD_DEC_COMPRESS_INDEX;index++) 
    {
    
    ind_write.tableOffset   = (id_i*MEA_HD_DEC_COMPRESS_INDEX)+index;
    value[0]=0;
    value[0] |= aryByte[startFrom + 0];
    value[0] |= aryByte[startFrom + 1]<<8;
    value[0] |= aryByte[startFrom + 2]<<16;
    value[0] |= aryByte[startFrom + 3]<<24;
    value[1] =0;
    value[1] |= aryByte[startFrom + 4];
    value[1] |= aryByte[startFrom + 5]<<8;
    value[1] |= aryByte[startFrom + 6]<<16;
    value[1] |= aryByte[startFrom + 7]<<24;
//    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"startFrom %d offset=%d 0x%08x 0x%08x\n",startFrom,index,value[0],value[1] );
    startFrom+=8;
    
    ind_write.funcParam4 = (MEA_funcParam)(&value);
    
    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
        return ENET_ERROR;
    }

    }

    /* Return to caller */
    return MEA_OK;
}


static MEA_Bool mea_drv_HC_Decomp_prof_find_free(MEA_Unit_t       unit_i, 
                                                              MEA_Uint16       *id_io)
{
    MEA_Uint16 i;

    for(i=0;i< MEA_HC_DECOMP_MAX_PROF; i++) {
        if (MEA_HC_Decomp_prof_info_Table[i].valid == MEA_FALSE){
            *id_io=i; 
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;

}



static MEA_Bool mea_drv_HC_Decomp_prof_IsRange(MEA_Unit_t     unit_i,
                                                            MEA_Uint16 id_i)
{

    if ((id_i) >= MEA_HC_DECOMP_MAX_PROF){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

static MEA_Status mea_drv_HC_Decomp_prof_IsExist(MEA_Unit_t     unit_i,
                                                              MEA_Uint16 id_i,
                                                              MEA_Bool *exist)
{
    if(exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  exist = NULL \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    *exist=MEA_FALSE;

    if(mea_drv_HC_Decomp_prof_IsRange(unit_i,id_i)!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed  for id=%d\n",__FUNCTION__,id_i); 
        return MEA_ERROR;
    }

    if( MEA_HC_Decomp_prof_info_Table[id_i].valid){
        *exist=MEA_TRUE;
        return MEA_OK;
    }

    return MEA_OK;
}

static MEA_Status mea_drv_HC_Decomp_prof_check_parameters(MEA_Unit_t       unit_i,
                                                         MEA_HDC_Data_dbt      *entry_pi)
{
    MEA_Globals_Entry_dbt Globalentry;
    MEA_Uint32 reg;

    MEA_OS_memset(&Globalentry,0,sizeof(Globalentry));
    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&Globalentry) !=MEA_OK) {

        return MEA_ERROR;
    }
    if(Globalentry.HC_Offset_Mode == MEA_FALSE){
        if(entry_pi->start_offset !=0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed start_offset  need to be zero \n",__FUNCTION__); 
            return MEA_ERROR;
        }
    }
    

    if(entry_pi->start_offset >= 64){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed start_offset  >= 64 \n",__FUNCTION__,entry_pi->start_offset); 
        return MEA_ERROR;
    }
    if((entry_pi->bit_Of_byte_comp[0] == 0) && (entry_pi->bit_Of_byte_comp[1] == 0)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed no configure of the compress \n",__FUNCTION__,entry_pi->start_offset); 
        return MEA_ERROR;
    }

    reg = (entry_pi->bit_Of_byte_comp[1]) >> 29;

    if (reg > 0){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - should have ZERO byte 62:64 \n", __FUNCTION__);
        return MEA_ERROR;
    }

   if((entry_pi->start_offset % 8) !=0 ){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  start_offset %d  need to be multiple of 8 \n",__FUNCTION__,entry_pi->start_offset); 
       return MEA_ERROR;
   
   
   }   


    return MEA_OK;
}
#if 0
static MEA_Status mea_drv_HC_Decomp_prof_add_owner(MEA_Unit_t       unit_i ,
                                                                MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_HC_Decomp_prof_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }

    MEA_HC_Decomp_prof_info_Table[id_i].num_of_owners++;

    return MEA_OK;
}
static MEA_Status mea_drv_HC_Decomp_prof_delete_owner(MEA_Unit_t       unit_i ,
                                                                   MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_HC_Decomp_prof_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }

    if(MEA_HC_Decomp_prof_info_Table[id_i].num_of_owners==1){
        MEA_HC_Decomp_prof_info_Table[id_i].valid=MEA_FALSE;
        MEA_HC_Decomp_prof_info_Table[id_i].num_of_owners=0;
        return MEA_OK;

    }


    MEA_HC_Decomp_prof_info_Table[id_i].num_of_owners--;

    return MEA_OK;
}
#endif


MEA_Status mea_drv_HC_Decomp_prof_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;
    
    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_HDEC_SUPPORT){
            return MEA_OK;   
        }

    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize HC Decompression ...\n");
    
    
    /* Allocate PacketGen Table */
    size = MEA_HC_DECOMP_MAX_PROF * sizeof(MEA_HC_Decomp_prof_entry_dbt);
    if (size != 0) {
        MEA_HC_Decomp_prof_info_Table = (MEA_HC_Decomp_prof_entry_dbt*)MEA_OS_malloc(size);
        if (MEA_HC_Decomp_prof_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_HC_Decomp_prof_info_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_HC_Decomp_prof_info_Table[0]),0,size);
    }
    /**/
    // Hw set zero on the init



    return MEA_OK;
}
MEA_Status mea_drv_HC_Decomp_prof_RInit(MEA_Unit_t       unit_i)
{
    
    
    MEA_Uint16 id;

    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_HDEC_SUPPORT){
            return MEA_OK;   
        }



    for (id=0;id<MEA_HC_DECOMP_MAX_PROF;id++) {
        if (MEA_HC_Decomp_prof_info_Table[id].valid){
            
            if(  MEA_API_HC_Decomp_Set_Profile(unit_i,
                                          id,
                                          &(MEA_HC_Decomp_prof_info_Table[id].data),
                                        MEA_TRUE) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  MEA_API_HC_Decomp_Set_Profile failed (id=%d)\n",__FUNCTION__,id);
                    return MEA_ERROR;
            }
        }
    }


    return MEA_OK;
}
MEA_Status mea_drv_HC_Decomp_prof_Conclude(MEA_Unit_t       unit_i)
{
    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_HDEC_SUPPORT){
            return MEA_OK;   
        }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude HC Decompression..\n");

    /* Free the table */
    if (MEA_HC_Decomp_prof_info_Table != NULL) {
        MEA_OS_free(MEA_HC_Decomp_prof_info_Table);
        MEA_HC_Decomp_prof_info_Table = NULL;
    }

    return MEA_OK;
}



/************************************************************************/
/*  MEA_API_HC_Decomp profile                                            */
/************************************************************************/


MEA_Status MEA_API_HC_Decomp_Profile_IsExist(MEA_Unit_t                      unit_i,
                                             MEA_Uint16                       id_i,
                                             MEA_Bool                        *exist)
{
    MEA_API_LOG

        /* Check if support */
        if (!MEA_HDEC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HDEC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

    return mea_drv_HC_Decomp_prof_IsExist(unit_i,id_i,exist);
}


MEA_Status MEA_API_HC_Decomp_Create_Profile(MEA_Unit_t                unit_i,
                                            MEA_Uint16                *id_io,
                                            MEA_HDC_Data_dbt          *entry_pi)
{
    MEA_Bool    exist;
    MEA_Uint32  ByteCnt=0,i,index;
    
    MEA_Uint8   startCount=0;
    MEA_Uint8   count=0;
    MEA_Uint32  value;
    MEA_Uint8   byteIndex[MEA_HD_NM_WORD*32];
    MEA_Uint32 setValue;
    MEA_Globals_Entry_dbt Globalentry;
    
    
    
    MEA_API_LOG

      

        /* Check if support */
        if (!MEA_HDEC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HDEC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        /* check parameter */
        if(mea_drv_HC_Decomp_prof_check_parameters(unit_i,entry_pi) !=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_HC_Decomp_prof_check_parameters failed \n",__FUNCTION__); 
            return MEA_ERROR;
        }
        MEA_OS_memset(&Globalentry,0,sizeof(Globalentry));
        MEA_OS_memset(&byteIndex[0],0,sizeof(byteIndex));

        if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&Globalentry) !=MEA_OK) {

            return MEA_ERROR;
        }
        


        /* Look for  id */
        if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID){
            /*check if the id exist*/
            if (mea_drv_HC_Decomp_prof_IsExist(unit_i,*id_io,&exist)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_HC_Decomp_prof_IsExist failed (*id_io=%d\n",
                    __FUNCTION__,*id_io);
                return MEA_ERROR;
            }
            if (exist){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - *Id_io %d is already exist\n",__FUNCTION__,*id_io);
                return MEA_ERROR;
            }

        }else {
            /*find new place*/
            if (mea_drv_HC_Decomp_prof_find_free(unit_i,id_io)!=MEA_TRUE){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_HC_Decomp_prof_find_free failed\n",__FUNCTION__);
                return MEA_ERROR;
            }
        }

        
        for (i=0;i<MEA_HD_NM_WORD;i++)
        {
           ByteCnt+= MEA_OS_NumOfu32bitSet(entry_pi->bit_Of_byte_comp[i]);
        }
        
        startCount=0;
        for(i=0; i<MEA_HD_NM_WORD; i++)
        {
            value=entry_pi->bit_Of_byte_comp[i];

            for(index=0;index<=31;index++){
                
                if(((value>>(index)) & 0x01)== 0){
                    count=(MEA_Uint8)((i*32)+index);
                    if(Globalentry.HC_Offset_Mode == MEA_TRUE)
                        byteIndex[startCount++]=(count+(MEA_Uint8)entry_pi->start_offset+8);
                    else
                        byteIndex[startCount++]=(count+(MEA_Uint8)entry_pi->start_offset+0);

                }


            }
        
        
        }
        
           

        while(startCount<=(MEA_HD_NM_WORD*32)-1){
                     
        if(Globalentry.HC_Offset_Mode == MEA_TRUE) 
                    byteIndex[startCount++]=++count+(MEA_Uint8)entry_pi->start_offset+8;
        else
                    byteIndex[startCount++]=++count+(MEA_Uint8)entry_pi->start_offset+0;

        }
        
        if( mea_drv_HC_Decomp_Bitmap_prof_UpdateHw(unit_i,
            *id_io,
            entry_pi,
            NULL)!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_HC_Decomp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,*id_io);
                return MEA_ERROR;
        }

        if(mea_drv_HC_Decomp_ByteMap_prof_UpdateHw(unit_i,*id_io,&byteIndex[0])!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Decomp_ByteMap_prof_UpdateHw failed for id %d\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
        }

       setValue=0;
       setValue = ByteCnt;
       setValue |= entry_pi->start_offset<<8;


        if(mea_drv_HC_Decomp_ByteCnt_prof_UpdateHw(unit_i,*id_io,setValue )!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_HC_Decomp_ByteCnt_prof_UpdateHw failed for id %d\n",__FUNCTION__,*id_io);
                return MEA_ERROR;
        }




        MEA_HC_Decomp_prof_info_Table[*id_io].valid         = MEA_TRUE;
        MEA_HC_Decomp_prof_info_Table[*id_io].num_of_owners = 1;
        MEA_HC_Decomp_prof_info_Table[*id_io].byteCount      = ByteCnt;

        MEA_OS_memcpy(&(MEA_HC_Decomp_prof_info_Table[*id_io].data),entry_pi,sizeof(MEA_HDC_Data_dbt));



        return MEA_OK;

}


MEA_Status MEA_API_HC_Decomp_Set_Profile(MEA_Unit_t                     unit_i,
                                         MEA_Uint16                      id_i,
                                         MEA_HDC_Data_dbt               *entry_pi,
                                         MEA_Bool                        force)
{
    MEA_Bool    exist;
    MEA_Uint32  ByteCnt=0,i,index;
    
    MEA_Uint8   startCount=0;
    MEA_Uint8   count=0;
    MEA_Uint32  value;
    MEA_Uint8   byteIndex[MEA_HD_NM_WORD*32];
    MEA_Uint32 setValue;
   MEA_Globals_Entry_dbt Globalentry;


    MEA_API_LOG

        /* Check if support */
        if (!MEA_HDEC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HDEC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }



        /*check if the id exist*/
        if (mea_drv_HC_Decomp_prof_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Decomp_prof_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - stream %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }

        /* check parameter*/
        if(mea_drv_HC_Decomp_prof_check_parameters(unit_i,entry_pi)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_HC_Decomp_prof_check_parameters filed \n",__FUNCTION__); 
            return MEA_ERROR;
        }
        

        if(force == MEA_FALSE){
            if ((MEA_OS_memcmp(entry_pi,
                &MEA_HC_Decomp_prof_info_Table[id_i].data,
                sizeof(*entry_pi))== 0) ) { 
                    return MEA_OK;
            }
        }

        MEA_OS_memset(&Globalentry,0,sizeof(Globalentry));
        MEA_OS_memset(&byteIndex[0],0,sizeof(byteIndex));
        
        if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&Globalentry) !=MEA_OK) {

            return MEA_ERROR;
        }

        for (i=0;i<MEA_HD_NM_WORD;i++)
        {
            ByteCnt+= MEA_OS_NumOfu32bitSet(entry_pi->bit_Of_byte_comp[i]);
        }

        startCount=0;
        for(i=0; i<MEA_HD_NM_WORD; i++)
        {
            value=entry_pi->bit_Of_byte_comp[i];

            for(index=0;index<=31;index++){

                if(((value>>(index)) & 0x01)== 0){
                    count=(MEA_Uint8)((i*32)+index);
                    if(Globalentry.HC_Offset_Mode == MEA_TRUE)
                        byteIndex[startCount++]=(count+(MEA_Uint8)entry_pi->start_offset+8);
                    else
                        byteIndex[startCount++]=(count+(MEA_Uint8)entry_pi->start_offset+0);
                }


            }


        }



        while(startCount<=(MEA_HD_NM_WORD*32)-1){
            if(Globalentry.HC_Offset_Mode == MEA_TRUE) 
                byteIndex[startCount++]=++count+(MEA_Uint8)entry_pi->start_offset+8;
            else
                byteIndex[startCount++]=++count+(MEA_Uint8)entry_pi->start_offset+0;
        }


        
        
        if( mea_drv_HC_Decomp_Bitmap_prof_UpdateHw(unit_i,
            id_i,
            entry_pi,
           (force==MEA_TRUE) ? (NULL) : (&MEA_HC_Decomp_prof_info_Table[id_i].data))!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_HC_Decomp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
                return MEA_ERROR;
        }
        if(mea_drv_HC_Decomp_ByteMap_prof_UpdateHw(unit_i,id_i,&byteIndex[0])!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Decomp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
        
        setValue=0;
        setValue = ByteCnt;
        setValue |= entry_pi->start_offset<<8;

        if(mea_drv_HC_Decomp_ByteCnt_prof_UpdateHw(unit_i,id_i,setValue)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Decomp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }







        MEA_OS_memcpy(&(MEA_HC_Decomp_prof_info_Table[id_i].data),
            entry_pi,
            sizeof(MEA_HDC_Data_dbt));
            
        MEA_HC_Decomp_prof_info_Table[id_i].byteCount=ByteCnt;



        return MEA_OK;
}


MEA_Status MEA_API_HC_Decomp_Delete_Profile(MEA_Unit_t                      unit_i,
                                            MEA_Uint16                       id_i)
{
    MEA_Bool                    exist;
    MEA_HDC_Data_dbt            entry_pi;
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_HDEC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HDEC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        /*check if the id exist*/
        if (mea_drv_HC_Decomp_prof_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Decomp_prof_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - stream %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        if(MEA_HC_Decomp_prof_info_Table[id_i].num_of_owners >1){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - need to delete all the reference to this profile\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }

        /*Write to Hw All zero*/

       MEA_OS_memset(&entry_pi,0,sizeof(entry_pi));

        if( mea_drv_HC_Decomp_Bitmap_prof_UpdateHw(unit_i,
            id_i,
            &entry_pi,
            NULL)!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_HC_Decomp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
                return MEA_ERROR;
        }





        MEA_OS_memset(&MEA_HC_Decomp_prof_info_Table[id_i].data,0,sizeof(MEA_HC_Decomp_prof_info_Table[0].data));

        MEA_HC_Decomp_prof_info_Table[id_i].valid = MEA_FALSE;
        MEA_HC_Decomp_prof_info_Table[id_i].num_of_owners=0;
        MEA_HC_Decomp_prof_info_Table[id_i].byteCount = 0;

        /* Return to caller */
        return MEA_OK;
}

MEA_Status MEA_API_HC_Decomp_Get_Profile(MEA_Unit_t                          unit_i,
                                         MEA_Uint16                           id_i,
                                         MEA_HDC_Data_dbt                   *entry_po)
{
    MEA_Bool    exist; 

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_HDEC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HDEC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        if(entry_po==NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry_po is null\n",__FUNCTION__,id_i);
            return MEA_ERROR;

        }
       
        
        /*check if the id exist*/
        if (mea_drv_HC_Decomp_prof_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Decomp_prof_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - id %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
       
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        /* Copy to caller structure */
        MEA_OS_memset(entry_po,0,sizeof(*entry_po));
        MEA_OS_memcpy(entry_po ,
            &(MEA_HC_Decomp_prof_info_Table[id_i].data),
            sizeof(*entry_po));

        /* Return to caller */
        return MEA_OK;

}

MEA_Status MEA_API_HC_Decomp_GetFirst_Profile(MEA_Unit_t                         unit_i,
                                              MEA_Uint16                        *id_o,
                                              MEA_HDC_Data_dbt                  *entry_po, /* Can't be NULL */
                                              MEA_Bool                          *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_HDEC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - TDM not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        if (id_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_o)=0;
            (*id_o) < MEA_HC_DECOMP_MAX_PROF; 
            (*id_o)++ ) {
                if (MEA_HC_Decomp_prof_info_Table[(*id_o)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }

                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_HC_Decomp_prof_info_Table[(*id_o)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;

}

MEA_Status MEA_API_HC_Decomp_GetNext_Profile(MEA_Unit_t                unit_i,
                                             MEA_Uint16                        *id_io,
                                             MEA_HDC_Data_dbt                  *entry_po, /* Can't be NULL */
                                             MEA_Bool                          *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_HDEC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HDEC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        if (id_io == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_io)++;
            (*id_io) < MEA_HC_DECOMP_MAX_PROF; 
            (*id_io)++ ) {
                if(MEA_HC_Decomp_prof_info_Table[(*id_io)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }
                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_HC_Decomp_prof_info_Table[(*id_io)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;
}





