/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/******************************************************************************                
*     Module Name:         enet_shaper_profile_drv.c                                                        
*                                                                                             
*   Module Description:                                                                         
*                                                                                             
*  Date of Creation:     14/09/06                                                             
*                                                                                             
*  Autor Name:             Alex Weis.                                                             
*******************************************************************************/            
                                                                                            
                                                                                            
                                                                                            
/*----------------------------------------------------------------------------*/            
/*             Includes                                                       */            
/*----------------------------------------------------------------------------*/            
#include "ENET_platform_types.h"
#include "mea_api.h"     
#include "MEA_platform_os_memory.h"
#include "MEA_platform_os_log.h"
#include "enet_general_drv.h"
#include "enet_shaper_profile_drv.h"
                                                                                            

#ifndef ENET_SW_CHECK_INPUT_PARAMETERS
#define ENET_SW_CHECK_INPUT_PARAMETERS 1
#endif


/*-------------------------------------------------------------------------------*/
/*             Defines                                                           */
/*-------------------------------------------------------------------------------*/
/* SHAPER PROFILER       indirect table entry format */
#define ENET_BM_SHAPER_PROFILE_CIR_WIDTH            16
    
#define ENET_BM_SHAPER_PROFILE_CBS_WIDTH             20
#define ENET_BM_SHAPER_PROFILE_OVERHEAD_WIDTH        8
#define ENET_BM_SHAPER_PROFILE_CELL_OVERHEAD_WIDTH   8
#define ENET_BM_SHAPER_PROFILE_OVERHEAD_SIGN_WIDTH   1
#define ENET_BM_SHAPER_PROFILE_MULTIPLE_SCAN_WIDTH   1
#define ENET_BM_SHAPER_PROFILE_TYPE_WIDTH            1
#define ENET_BM_SHAPER_PROFILE_MODE_BAG_WIDTH        1
#define ENET_BM_SHAPER_PROFILE_CIR_WIDTH_EXTD         4  



#define SHAPER_FAST_THRESHOLD_FOR_PACKETS_VAL (SHAPER_FAST_THRESHOLD_FOR_BIT_VAL/1500)   // 134M / max packet size

#define SHAPER_PROFILE_MULTYPLESCAN_NORMAL_VAL   0
#define SHAPER_PROFILE_MULTYPLESCAN_FAST_VAL   1

/* TBD the resolution should be calculated as a function of the TIC reg*/
#define ENET_SHAPER_FAST_RESOLUTION(type)     ( (type == MEA_SHAPER_SUPPORT_PORT_TYPE)   ?\
                                                         MEA_SHAPER_CIR_EIR_FAST_GN_PORT :\
                                                (type == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) ?\
                                                         MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER :\
                                                (type == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) ?\
                                                         MEA_SHAPER_CIR_EIR_FAST_GN_PRIQUEUE: MEA_SHAPER_SUPPORT_PORT_TYPE)

#define ENET_SHAPER_SLOW_RESOLUTION(type)     ( (type == MEA_SHAPER_SUPPORT_PORT_TYPE)   ?\
                                                         MEA_SHAPER_CIR_EIR_SLOW_GN_PORT :\
                                                (type == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) ?\
                                                         MEA_SHAPER_CIR_EIR_SLOW_GN_CLUSTER :\
                                                (type == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) ?\
                                                         MEA_SHAPER_CIR_EIR_SLOW_GN_PRIQUEUE: MEA_SHAPER_SUPPORT_PORT_TYPE)


#define ENET_SHAPER_SLOW_RESOLUTION_TICKs(type)     ( (type == MEA_SHAPER_SUPPORT_PORT_TYPE)   ?\
                                                         MEA_SHAPER_CIR_EIR_SLOW_GN_PORT_TICKs :\
                                                    (type == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) ?\
                                                         MEA_SHAPER_CIR_EIR_SLOW_GN_CLUSTER_TICKs :\
                                                    (type == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) ?\
                                                         MEA_SHAPER_CIR_EIR_SLOW_GN_PRIQUEUE_TICKs: MEA_SHAPER_CIR_EIR_SLOW_GN_PORT_TICKs)





#define ENET_SHAPER_FAST_RESOLUTION_TICKs(type)     ( (type == MEA_SHAPER_SUPPORT_PORT_TYPE)   ?\
                                                         MEA_SHAPER_CIR_EIR_FAST_GN_PORT_TICKs :\
                                                    (type == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) ?\
                                                         MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER_TICKs :\
                                                    (type == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) ?\
                                                         MEA_SHAPER_CIR_EIR_FAST_GN_PRIQUEUE_TICKs: MEA_SHAPER_CIR_EIR_FAST_GN_PORT_TICKs)


#define ENET_SHAPER_SLOW_MULTYPLESCAN(type)            ( (type == MEA_SHAPER_SUPPORT_PORT_TYPE)   ?\
                                                             MEA_SHAPER_SLOW_MULTYPLESCAN_PORT :\
                                                        (type == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) ?\
                                                             MEA_SHAPER_SLOW_MULTYPLESCAN_CLUSTER :\
                                                        (type == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) ?\
                                                             MEA_SHAPER_SLOW_MULTYPLESCAN_PRIQUEUE: MEA_SHAPER_SLOW_MULTYPLESCAN_PORT)









#define ENET_SHAPER_OVERHEAD_SIGN_VALUE  0

#if 0
#define  ENET_SHAPER_CIR_BPS_MAX_VALUE \
    ((MEA_Uint32) \
      ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) \
       ? 0x3B9ACA00 /* 1.0G */ \
       : 0x9502F900 /* 2.5G */ \
       ))
#else
#define  ENET_SHAPER_CIR_BPS_MAX_VALUE      (1E10) //(0x9502F900) /* 2.5G */ 
#endif

#define  ENET_SHAPER_CIR_BPS_MIN_VALUE 0
#define  ENET_SHAPER_CBS_BPS_MAX_VALUE \
    ((ENET_Uint32)((0x00000001 << ENET_BM_SHAPER_PROFILE_CBS_WIDTH)-1))
#define  ENET_SHAPER_CBS_BPS_MIN_VALUE 0
#define  ENET_SHAPER_CIR_PPS_MAX_VALUE 1500000  
#define  ENET_SHAPER_CIR_PPS_MIN_VALUE 0
#define  ENET_SHAPER_CBS_PPS_MAX_VALUE \
    ((ENET_Uint32)((0x00000001 << ENET_BM_SHAPER_PROFILE_CBS_WIDTH)-1))
#define  ENET_SHAPER_CBS_PPS_MIN_VALUE 0

/*-------------------------------------------------------------------------------*/            
/*             Typedefs                                                          */            
/*-------------------------------------------------------------------------------*/            
typedef struct{
    ENET_Uint32     numberOfOwners;            /* this is used to count the number of owner for this shaper */
    MEA_db_HwUnit_t hwUnit;
}enet_Shaper_Private_dbt;

typedef struct{
    enet_Shaper_Private_dbt dbt_private;
    ENET_Shaper_Profile_dbt dbt_public;
}enet_Shaper_Profile_GroupEntry_dbt;

                                      
typedef struct {
    ENET_GroupId_t group;
    ENET_Uint16    num_of_profiles_per_acm;
    ENET_Uint16    max_num_of_acm;
    ENET_Bool      support;
} enet_Shaper_Profile_TypeEntry_dbt;
                                                      
/*-------------------------------------------------------------------------------*/            
/*             Global variables                                                  */            
/*-------------------------------------------------------------------------------*/            
static enet_Shaper_Profile_TypeEntry_dbt enet_ShaperProfile_type_table[MEA_SHAPER_SUPPORT_LAST_TYPE];
static ENET_Bool                         enet_ShaperProfile_InitDone;

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                      Local Function implementation                         */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                <enet_InitDevice_ShaperProfile>                             */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status  enet_InitDevice_ShaperProfile(MEA_Unit_t               unit_i,
                                           MEA_ShaperSupportType_te type_i)

{
    ENET_Uint32 val[2];
    ENET_Uint32 i;
    ENET_Uint32 count_shift;
    MEA_db_HwUnit_t hwUnit;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return ENET_ERROR;)

    /* Reset the values */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }

    /* Build the or mask shaper */
    count_shift = 0;
    count_shift+=ENET_BM_SHAPER_PROFILE_CIR_WIDTH;
    MEA_OS_insert_value(count_shift,
                        ENET_BM_SHAPER_PROFILE_CBS_WIDTH,
                        (MEA_Uint32)0x1f4,  /* Add CBC=500 bytes (OR mask) */
                        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_CBS_WIDTH;
    count_shift+=ENET_BM_SHAPER_PROFILE_OVERHEAD_WIDTH;
    count_shift+=ENET_BM_SHAPER_PROFILE_CELL_OVERHEAD_WIDTH;
    count_shift+=ENET_BM_SHAPER_PROFILE_OVERHEAD_SIGN_WIDTH;
    count_shift+=ENET_BM_SHAPER_PROFILE_MULTIPLE_SCAN_WIDTH;
    count_shift+=ENET_BM_SHAPER_PROFILE_TYPE_WIDTH;

    switch (type_i) {
    case MEA_SHAPER_SUPPORT_PORT_TYPE:
        enet_ShaperProfile_type_table[type_i].support = mea_drv_Get_DeviceInfo_GetShaperPortType_support(unit_i,hwUnit);
            //ENET_BM_TBL_SHAPER_PROFILE_PORT_IS_ACM_SUPPORT(unit_i,hwUnit);
        enet_ShaperProfile_type_table[type_i].max_num_of_acm = 
            ENET_BM_TBL_SHAPER_PROFILE_PORT_NUM_OF_ACM_MODES(unit_i,hwUnit);
        enet_ShaperProfile_type_table[type_i].num_of_profiles_per_acm = 
            ENET_BM_TBL_SHAPER_PROFILE_PORT_LENGTH_PER_ACM(unit_i,hwUnit);
        if (mea_drv_Get_DeviceInfo_GetShaperPortType_support(unit_i,hwUnit)==MEA_TRUE){
            ENET_WriteReg(ENET_UNIT_0,
                          ENET_BM_SHAPER_PROFILE_OR_MASK_LSB_REG_PORT,
                          val[0],
                          ENET_MODULE_BM);
            ENET_WriteReg(ENET_UNIT_0,
                          ENET_BM_SHAPER_PROFILE_OR_MASK_MSB_REG_PORT,
                          val[1],
                          ENET_MODULE_BM);
        }
        break;
    case MEA_SHAPER_SUPPORT_CLUSTER_TYPE:
        enet_ShaperProfile_type_table[type_i].support = mea_drv_Get_DeviceInfo_GetShaperClusterType_support(unit_i,hwUnit);
            //ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_IS_ACM_SUPPORT(unit_i,hwUnit);
        enet_ShaperProfile_type_table[type_i].max_num_of_acm = 
            ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_NUM_OF_ACM_MODES(unit_i,hwUnit);
        enet_ShaperProfile_type_table[type_i].num_of_profiles_per_acm = 
            ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_LENGTH_PER_ACM(unit_i,hwUnit);
        if(mea_drv_Get_DeviceInfo_GetShaperClusterType_support(unit_i,hwUnit)==MEA_TRUE){
            ENET_WriteReg(ENET_UNIT_0,
                          ENET_BM_SHAPER_PROFILE_OR_MASK_LSB_REG_CLUSTER,
                          val[0],
                          ENET_MODULE_BM);
            ENET_WriteReg(ENET_UNIT_0,
                          ENET_BM_SHAPER_PROFILE_OR_MASK_MSB_REG_CLUSTER,
                          val[1],
                          ENET_MODULE_BM);
        }
        break;
    case MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE:
        enet_ShaperProfile_type_table[type_i].support = mea_drv_Get_DeviceInfo_GetShaperPriQueueType_support(unit_i,hwUnit);
            //ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_IS_ACM_SUPPORT(unit_i,hwUnit);
        enet_ShaperProfile_type_table[type_i].max_num_of_acm = 
            ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_NUM_OF_ACM_MODES(unit_i,hwUnit);
        enet_ShaperProfile_type_table[type_i].num_of_profiles_per_acm = 
            ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_LENGTH_PER_ACM(unit_i,hwUnit);
        if(mea_drv_Get_DeviceInfo_GetShaperPriQueueType_support(unit_i,hwUnit)==MEA_TRUE){
            ENET_WriteReg(ENET_UNIT_0,
                          ENET_BM_SHAPER_PROFILE_OR_MASK_LSB_REG_PRIQUEUE,
                          val[0],
                          ENET_MODULE_BM);
            ENET_WriteReg(ENET_UNIT_0,
                          ENET_BM_SHAPER_PROFILE_OR_MASK_MSB_REG_PRIQUEUE,
                          val[1],
                          ENET_MODULE_BM);
        }
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown type_i %d \n",
                          __FUNCTION__,type_i);
        return ENET_ERROR;

    }


    return ENET_OK;
}


static MEA_Uint32 enet_GetShaperTICK(MEA_ShaperSupportType_te type)
{
    MEA_Globals_Entry_dbt entry;
    MEA_Uint32 retVal = 128;
    MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry);

    switch (type)
    {
        case MEA_SHAPER_SUPPORT_PORT_TYPE:
        retVal = entry.shaper_fast_port;
    	    break;
        case  MEA_SHAPER_SUPPORT_CLUSTER_TYPE:
            retVal =  entry.shaper_fast_cluster;
            break;
        case  MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE:
            retVal =  entry.shaper_fast_PriQueue;
            break;
        default:
            break;
    }
 

 return  retVal;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_UpdateDeviceEntry_ShaperProfile>                  */
/*                                                                            */
/*----------------------------------------------------------------------------*/
void enet_UpdateDeviceEntry_ShaperProfile(MEA_funcParam    arg1_i,
    MEA_funcParam    arg2_i,
    MEA_funcParam    arg3_i,
    MEA_funcParam    arg4_i,
    MEA_funcParam module_i)
{
   // MEA_db_HwUnit_t           hwUnit_i = (MEA_db_HwUnit_t         )arg1_i;
    MEA_ShaperSupportType_te  type_i   = (MEA_ShaperSupportType_te)arg2_i;
   // ENET_ShaperId_t           id_i     = (ENET_ShaperId_t         )arg3_i;
    ENET_Shaper_Profile_dbt  *entry_i  = (ENET_Shaper_Profile_dbt*)arg4_i;
    ENET_Uint32               val[2];
    ENET_Uint32               i;
    ENET_Uint32               count_shift;
    ENET_Uint32               multipleScan;
    MEA_uint64               cir=0;
    ENET_Uint32               cbs=0;
    ENET_Uint32               CirResolution;
    ENET_Uint32 numoftick;
    ENET_Uint32 number_of_update_ops;
    MEA_uint64 sysclock;


#if NEW_SHAPER	
    ENET_Uint32 SlowTick =1;
#endif

#if 0    
    ENET_Uint32 BM_SHP_SLOW_Adr=0;
#endif    




/* select Fast or Normal ? */
    if ( entry_i->mode == MEA_SHAPER_TYPE_MEF)
    {
    
    
        if ( entry_i->type == SHAPER_PROFILE_TYPE_BIT_VAL )  
        {
        
            if (entry_i->CIR > SHAPER_FAST_THRESHOLD_FOR_BIT_VAL){
                multipleScan = SHAPER_PROFILE_MULTYPLESCAN_FAST_VAL;
#if NEW_SHAPER	
                SlowTick = ENET_SHAPER_SLOW_MULTYPLESCAN(type_i);
                CirResolution= ENET_SHAPER_FAST_RESOLUTION_TICKs(type_i);
#else

            CirResolution= ENET_SHAPER_FAST_RESOLUTION(type_i);
#endif				
            }else{
                multipleScan = SHAPER_PROFILE_MULTYPLESCAN_NORMAL_VAL;
                
#if NEW_SHAPER
                SlowTick = ENET_SHAPER_SLOW_MULTYPLESCAN(type_i);
                CirResolution=ENET_SHAPER_SLOW_RESOLUTION_TICKs(type_i);
#else
                CirResolution = ENET_SHAPER_SLOW_RESOLUTION(type_i);
#endif
            }
        }
        else
        {

            if (entry_i->CIR > SHAPER_FAST_THRESHOLD_FOR_PACKETS_VAL){
                multipleScan = SHAPER_PROFILE_MULTYPLESCAN_FAST_VAL;
#if NEW_SHAPER
                SlowTick = ENET_SHAPER_SLOW_MULTYPLESCAN(type_i);
                CirResolution= ENET_SHAPER_FAST_RESOLUTION_TICKs(type_i);
#else
            CirResolution= ENET_SHAPER_FAST_RESOLUTION(type_i);
#endif				
            }else{
                multipleScan = SHAPER_PROFILE_MULTYPLESCAN_NORMAL_VAL;
#if NEW_SHAPER
                SlowTick = ENET_SHAPER_SLOW_MULTYPLESCAN(type_i);
                CirResolution = ENET_SHAPER_SLOW_RESOLUTION_TICKs(type_i);
#else
                CirResolution = ENET_SHAPER_SLOW_RESOLUTION(type_i);
#endif
            }
        }


#if NEW_SHAPER		
        cir = (MEA_uint64)(entry_i->CIR)/8 *(SlowTick) ; 
#else
	   cir = (MEA_uint64)(entry_i->CIR) ;
#endif	
        if(MEA_GLOBAL_SHAPER_XCIR_SUPPORT)
        {
            cir = (MEA_uint64)(entry_i->CIR);
            if (cir > 0)
            {
                multipleScan = SHAPER_PROFILE_MULTYPLESCAN_FAST_VAL;

                numoftick = enet_GetShaperTICK(type_i);
                sysclock = MEA_GLOBAL_SYS_Clock;
                //sysclock = MEA_systemClock_calc(MEA_UNIT_0, sysclock);

                number_of_update_ops = (MEA_Uint32)(sysclock / numoftick);
            
                cir=cir/8;  // 
                if((cir % number_of_update_ops) >= (number_of_update_ops/2) )
                {
                cir/= number_of_update_ops;
                cir++;
                } else {
                    cir /= number_of_update_ops;
                }
            }
         
       } else {
            if ((cir % CirResolution) >= (CirResolution/2))  { 
                cir /= CirResolution;
                cir++;
            } else {
                cir /= CirResolution;
            }
        }
        cbs = entry_i->CBS ;

    }else{
        if(entry_i->mode == MEA_SHAPER_TYPE_BAG){
            multipleScan = SHAPER_PROFILE_MULTYPLESCAN_FAST_VAL;
            CirResolution= ENET_SHAPER_FAST_RESOLUTION(type_i); 
            // CIR [bps]= (Lmax[Byte]+ 20) * 8000)/BAG[msec]
            if(entry_i->CIR >= 512 ){
                multipleScan = SHAPER_PROFILE_MULTYPLESCAN_NORMAL_VAL;
                CirResolution= ENET_SHAPER_SLOW_RESOLUTION(type_i); 
            }

            if((entry_i->CIR)!=0)
            {
                cir = (MEA_uint64)((entry_i->CBS +20)*64000/(entry_i->CIR));
            if ((cir % CirResolution) >= (CirResolution/2))  { 
                cir /= CirResolution;
                cir++;
            } else {
                cir /= CirResolution;
            }
             cbs=64;//

            }else{
                 cir =0;
                 cbs = 0;

            }//BAG
        }
    }//

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }

    /* Build register 0 */
    if ((type_i == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE)  && 
        (entry_i->mode == MEA_SHAPER_TYPE_MEF)  )
    {
        if (mea_drv_Get_DeviceInfo_NumOfClusters(MEA_UNIT_0, 0) <= 512) {
            //cir = cir;
        }
        else {
            if (!MEA_GLOBAL_SHAPER_XCIR_SUPPORT){
                if (mea_drv_Get_DeviceInfo_NumOfClusters(MEA_UNIT_0, 0) <= 1024) {
                    cir = cir*2;
                }
            }
        }
    
    }

    if ((type_i == MEA_SHAPER_SUPPORT_CLUSTER_TYPE) &&
        (entry_i->mode == MEA_SHAPER_TYPE_MEF))
    {
        if (mea_drv_Get_DeviceInfo_NumOfClusters(MEA_UNIT_0, 0) <= 256) {
            //cir = cir;
        }
        else {
            if (!MEA_GLOBAL_SHAPER_XCIR_SUPPORT){
                if (mea_drv_Get_DeviceInfo_NumOfClusters(MEA_UNIT_0, 0) <= 1024) {
                    cir = cir * 2;
                }
            }
        }
    }
    

    count_shift = 0;

    MEA_OS_insert_value(count_shift,
                        ENET_BM_SHAPER_PROFILE_CIR_WIDTH,
                        (MEA_Uint32)cir,
                        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_CIR_WIDTH;

    MEA_OS_insert_value(count_shift,
                        ENET_BM_SHAPER_PROFILE_CBS_WIDTH,
                        (MEA_Uint32)cbs,
                        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_CBS_WIDTH;

    MEA_OS_insert_value(count_shift,
                        ENET_BM_SHAPER_PROFILE_OVERHEAD_WIDTH,
                        (MEA_Uint32)(entry_i->overhead),
                        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_OVERHEAD_WIDTH;

if(entry_i->mode != MEA_SHAPER_TYPE_BAG){
    MEA_OS_insert_value(count_shift,
                        ENET_BM_SHAPER_PROFILE_CELL_OVERHEAD_WIDTH,
                        (MEA_Uint32)(entry_i->Cell_Overhead)   ,
                        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_CELL_OVERHEAD_WIDTH;
}else{
    MEA_OS_insert_value(count_shift,
        ENET_BM_SHAPER_PROFILE_CELL_OVERHEAD_WIDTH,
        0x20 /*(MEA_Uint32)(entry_i->CBS>>4) */  ,
        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_CELL_OVERHEAD_WIDTH;
}

    MEA_OS_insert_value(count_shift,
                        ENET_BM_SHAPER_PROFILE_OVERHEAD_SIGN_WIDTH,
                        (MEA_Uint32)(ENET_SHAPER_OVERHEAD_SIGN_VALUE),
                        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_OVERHEAD_SIGN_WIDTH;

    MEA_OS_insert_value(count_shift,
                        ENET_BM_SHAPER_PROFILE_MULTIPLE_SCAN_WIDTH,
                        (MEA_Uint32)multipleScan,
                        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_MULTIPLE_SCAN_WIDTH;

    MEA_OS_insert_value(count_shift,
                        ENET_BM_SHAPER_PROFILE_TYPE_WIDTH,
                        (MEA_Uint32)(entry_i->type),
                        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_TYPE_WIDTH;

    MEA_OS_insert_value(count_shift,
        ENET_BM_SHAPER_PROFILE_MODE_BAG_WIDTH,
        (MEA_Uint32)(entry_i->mode),
        &(val[0]));
    count_shift+=ENET_BM_SHAPER_PROFILE_MODE_BAG_WIDTH;
    if (MEA_GLOBAL_SHAPER_XCIR_SUPPORT) {

        MEA_OS_insert_value(count_shift,
            ENET_BM_SHAPER_PROFILE_CIR_WIDTH_EXTD,
            (MEA_Uint32)(((cir)>> ENET_BM_SHAPER_PROFILE_CIR_WIDTH) & 0xf),
            &(val[0]));
        count_shift += ENET_BM_SHAPER_PROFILE_CIR_WIDTH_EXTD;

    }



    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        ENET_WriteReg(ENET_UNIT_0,ENET_BM_IA_WRDATA(i),val[i],ENET_MODULE_BM);
    }

}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_UpdateDevice_ShaperProfile>                            */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_UpdateDevice_ShaperProfile (ENET_Unit_t               unit_i,
                                             MEA_ShaperSupportType_te  type_i,
                                             ENET_ShaperId_t           id_i,
                                             ENET_Shaper_Profile_dbt  *new_entry_i,
                                             ENET_Shaper_Profile_dbt  *old_entry_i) 
{
    MEA_db_HwUnit_t hwUnit;
    ENET_ind_write_t ind_write; 
#ifdef MEA_ENV_256PORT
    MEA_Uint32 i=0;
#endif

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS
    ENET_ASSERT_NULL(new_entry_i,"new_entry_i");
#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */
     

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return ENET_ERROR;)

    if ((old_entry_i != NULL) &&
        (MEA_OS_memcmp(new_entry_i,
                       old_entry_i,
                       sizeof(*new_entry_i)) == 0)) {
        return ENET_OK;
    }

    /* Build the ind_write structure */
    MEA_OS_memset(&ind_write,0,sizeof(ind_write));
    switch(type_i) {
    case MEA_SHAPER_SUPPORT_PORT_TYPE:
        ind_write.tableType     = ENET_BM_TBL_SHAPER_PROFILE_PORT;
        break;
    case MEA_SHAPER_SUPPORT_CLUSTER_TYPE:
        ind_write.tableType     = ENET_BM_TBL_SHAPER_PROFILE_CLUSTER;
        break;
    case MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE:
        ind_write.tableType     = ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE;
        break;

    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown type_i %d\n",
                          __FUNCTION__,type_i);
        return ENET_ERROR;
    }
#ifdef MEA_ENV_256PORT
    for (i = 0; i < 2; i++){  /*write the profile x  to LQ 0 ,LQ 1*/

        MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, (i), MEA_MODULE_BM);
#endif
        ind_write.tableOffset = id_i;
        ind_write.cmdReg = ENET_BM_IA_CMD;
        ind_write.cmdMask = ENET_BM_IA_CMD_MASK;
        ind_write.statusReg = ENET_BM_IA_STAT;
        ind_write.statusMask = ENET_BM_IA_STAT_MASK;
        ind_write.tableAddrReg = ENET_BM_IA_ADDR;
        ind_write.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry = (ENET_FUNCPTR)enet_UpdateDeviceEntry_ShaperProfile;
        ind_write.funcParam1 = (MEA_funcParam)((long) hwUnit);
        ind_write.funcParam2 = (MEA_funcParam)type_i;
        ind_write.funcParam3 = (MEA_funcParam)((long)id_i);
        ind_write.funcParam4 = (MEA_funcParam)new_entry_i;

        /* Write to device */
        if (ENET_WriteIndirect(ENET_UNIT_0, &ind_write, ENET_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , ENET_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                ENET_MODULE_BM);
            return ENET_ERROR;
#ifdef MEA_ENV_256PORT			
        }
#endif		
    }

    // Return to caller 
    return ENET_OK;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <enet_ConvertToKey_Shaper_Profile>                */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status enet_ConvertToKey_Shaper_Profile (ENET_Unit_t                  unit_i,
                                              MEA_ShaperSupportType_te     type_i,
                                              ENET_ShaperId_t              id_i,
                                              ENET_Shaper_Profile_key_dbt *key_o,
                                              ENET_Bool                    silent_i)
{

    if (key_o == NULL) {
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - key_o == NULL\n",
                              __FUNCTION__);
        }
        return ENET_ERROR;
    }


    switch (type_i) {
    case MEA_SHAPER_SUPPORT_PORT_TYPE:
    case MEA_SHAPER_SUPPORT_CLUSTER_TYPE:
    case MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE:
        MEA_OS_memset(key_o,0,sizeof(*key_o));
        key_o->entity_type = type_i;
        key_o->acm_mode    = id_i / enet_ShaperProfile_type_table[type_i].num_of_profiles_per_acm;
        key_o->id          = id_i % enet_ShaperProfile_type_table[type_i].num_of_profiles_per_acm;
        break;
    default:
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown type_i %d \n",
                              __FUNCTION__,type_i);
        }
        return ENET_ERROR;
    }

    
    
    return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <enet_ConvertToId_Shaper_Profile>                */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status enet_ConvertToId_Shaper_Profile (ENET_Unit_t                  unit_i,
                                             ENET_Shaper_Profile_key_dbt *key_i,
                                             ENET_ShaperId_t             *id_o,
                                             ENET_Bool                    silent_i)
{

    if (key_i == NULL) {
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - key_i == NULL\n",
                              __FUNCTION__);
        }
        return ENET_ERROR;
    }

    if (id_o == NULL) {
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - id_o == NULL\n",
                              __FUNCTION__);
            return ENET_ERROR;
        }
    }


    switch (key_i->entity_type) {
    case MEA_SHAPER_SUPPORT_PORT_TYPE:
    case MEA_SHAPER_SUPPORT_CLUSTER_TYPE:
    case MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE:
        if (!enet_ShaperProfile_type_table[key_i->entity_type].support) {
            if (!silent_i) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Shaper profile not support for type %d \n",
                                  __FUNCTION__,
                                  key_i->entity_type);
            }
            return ENET_ERROR;
        }
        if (key_i->acm_mode >= enet_ShaperProfile_type_table[key_i->entity_type].max_num_of_acm) {
           if (!silent_i) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - key_i->acm_mode %d >= max value %d (type=%d)\n",
                                  __FUNCTION__,
                                  key_i->acm_mode,
                                  enet_ShaperProfile_type_table[key_i->entity_type].max_num_of_acm,
                                  key_i->entity_type);
            }
            return ENET_ERROR;
        }
        if (key_i->id >= enet_ShaperProfile_type_table[key_i->entity_type].num_of_profiles_per_acm) {
            if (!silent_i) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - key_i->id %d >= max value %d (type=%d)\n",
                                  __FUNCTION__,
                                  key_i->id,
                                  enet_ShaperProfile_type_table[key_i->entity_type].num_of_profiles_per_acm,
                                  key_i->entity_type);
            }
            return ENET_ERROR;
        }
        *id_o  = key_i->acm_mode * enet_ShaperProfile_type_table[key_i->entity_type].num_of_profiles_per_acm;
        *id_o += key_i->id;        
        break;
    default:
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown key_i->entity_type %d \n",
                              __FUNCTION__,key_i->entity_type);
        }
        return ENET_ERROR;
    }

    
    
    return ENET_OK;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <enet_Find_Shaper_Profile>                        */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status enet_Find_Shaper_Profile (ENET_Unit_t                         unit_i, 
                                      ENET_Shaper_Profile_dbt            *entry_i,
                                      ENET_Shaper_Profile_key_dbt        *key_io,
                                      ENET_ShaperId_t                    *id_o,
                                      enet_Shaper_Profile_GroupEntry_dbt *groupEntry_o,
                                      ENET_Bool                          *found_o)
{

    ENET_Uint32 len = sizeof(*groupEntry_o);
    MEA_db_HwUnit_t hwUnit;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check entry_i parameter */
    ENET_ASSERT_NULL(entry_i,"entry_i");

    /* check key_io parameter */
    ENET_ASSERT_NULL(key_io,"key_io");

    /* Verify that the entity_type is valid */
    if (key_io->entity_type > MEA_SHAPER_SUPPORT_LAST_TYPE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Unknown key_io->entity_type %d \n",
                         __FUNCTION__,key_io->entity_type);
       return ENET_ERROR;                                                                            
    }

    /* Verify that the entity_type is valid */
    if (!enet_ShaperProfile_type_table[key_io->entity_type].support) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_io->entity_type %d  is not support\n",
                         __FUNCTION__,key_io->entity_type);
       return ENET_ERROR;                                                                            
    }

    /* Verify the acm_mode is zero */
    if (key_io->acm_mode != 0) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_io->acm_mode %d != 0 \n",
                         __FUNCTION__,key_io->acm_mode);
       return ENET_ERROR;                                                                            
    }

    /* Verify the id is ENET_PLAT_GENERATE_NEW_ID */
    if (key_io->id != ENET_PLAT_GENERATE_NEW_ID) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_io->id %d != ENET_PLAT_GENERATE_NEW_ID \n",
                         __FUNCTION__,key_io->id);
       return ENET_ERROR;                                                                            
    }

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    if (ENET_GetFirst_GroupElement(unit_i,
                                   enet_ShaperProfile_type_table[key_io->entity_type].group,
                                   id_o,
                                   groupEntry_o,
                                   &len,
                                   found_o) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ENET_GetFirst_GroupElement failed \n",
                          __FUNCTION__);
       return ENET_ERROR;
   }

   while (*found_o) {

       if ((*id_o) >= enet_ShaperProfile_type_table[key_io->entity_type].num_of_profiles_per_acm) {
           break;
       }

       if ((groupEntry_o->dbt_private.hwUnit       == hwUnit      ) &&
           (MEA_OS_memcmp(&groupEntry_o->dbt_public,entry_i,sizeof(*entry_i))==0)) {
            key_io->id = *id_o;
            *found_o = ENET_TRUE;
            return ENET_OK;
       }
       
       if (ENET_GetNext_GroupElement(unit_i,
                                     enet_ShaperProfile_type_table[key_io->entity_type].group,
                                     id_o,
                                     groupEntry_o,
                                     &len,
                                     found_o) != ENET_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - ENET_GetFirst_GroupElement failed \n",
                             __FUNCTION__);
           return ENET_ERROR;
       }
   }

   *id_o     = ENET_PLAT_GENERATE_NEW_ID;
   key_io->id = *id_o;
   *found_o = ENET_FALSE;


    /* Return to caller */
    return ENET_OK;
}     


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                     Private APIs implementation                            */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <enet_Init_Shaper_Profile>                        */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status enet_Init_Shaper_Profile    (ENET_Unit_t unit_i)
{

    ENET_Group_dbt           group;
    MEA_ShaperSupportType_te type;
    MEA_db_HwUnit_t          hwUnit;
    mea_memory_mode_e        save_globalMemoryMode;
    ENET_ShaperId_t          id;
    ENET_Shaper_Profile_dbt  entry;


    /* Check that we not already init */
    if (enet_ShaperProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Already Init \n",
                          __FUNCTION__);
        return ENET_ERROR;
    }


    /* Init enet_ShaperProfile_type_table */
    for (type=0;type<MEA_SHAPER_SUPPORT_LAST_TYPE;type++) {

        enet_ShaperProfile_type_table[type].group = ENET_PLAT_GENERATE_NEW_ID;
        MEA_OS_memset(&group,0,sizeof(group));
        MEA_OS_sprintf(group.name,"ShaperProfile group for type %d",type);
        group.max_number_of_elements = ENET_MAX_NUM_SHAPER_PROFILES(unit_i,type);
        if (ENET_Create_Group(unit_i,
                              &group,
                              &(enet_ShaperProfile_type_table[type].group)) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_Create_Group failed (type=%d)\n",
                              __FUNCTION__,type);
            return ENET_ERROR;
        }

        /* set default to Device */
        for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
            MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        
            if (enet_InitDevice_ShaperProfile(unit_i,type) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - enet_InitDevice_ShaperProfile failed (type=%d)\n",
                                  __FUNCTION__,type);
               return ENET_ERROR;
            }

            for (id=0;id<group.max_number_of_elements;id++) {
                MEA_OS_memset(&entry,0,sizeof(entry));
                if (enet_UpdateDevice_ShaperProfile(unit_i,type,id,&entry,NULL)!=ENET_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - enet_UpdateDevice_ShaperProfile failed (type=%d,id=%d)\n",
                                      __FUNCTION__,type,id);
                    return ENET_ERROR;
                }
            }

            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        }
    }

    // Mark InitDone 
    enet_ShaperProfile_InitDone = ENET_TRUE;

    return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <enet_ReInit_Shaper_Profile>                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status enet_ReInit_Shaper_Profile  (ENET_Unit_t unit_i)
{
     
    ENET_ShaperId_t                    id;                
    enet_Shaper_Profile_GroupEntry_dbt entry;
    ENET_Uint32                        len;
    ENET_Bool                          found;                             
    MEA_ShaperSupportType_te           type;
    MEA_db_HwUnit_t                    hwUnit;
    mea_memory_mode_e                  save_globalMemoryMode;

    /* Check that we already init */
    if (!enet_ShaperProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Not Init \n",
                          __FUNCTION__);
        return ENET_ERROR;
    }

    /* For each type */
    for (type=0;type<MEA_SHAPER_SUPPORT_LAST_TYPE;type++) {

        /* set default to Device */
        for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
            MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        
            if (enet_InitDevice_ShaperProfile(unit_i,type) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - enet_InitDevice_ShaperProfile failed (type=%d)\n",
                                  __FUNCTION__,type);
               return ENET_ERROR;
            }
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        }

        /* ReInit all shaper profiles of this type */
        len=sizeof(entry);
        if (ENET_GetFirst_GroupElement(unit_i,
                                       enet_ShaperProfile_type_table[type].group,
                                       &id,
                                       &entry,
                                       &len,
                                       &found) !=ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_GetFirst_GroupElemenet failed \n",
                              __FUNCTION__);
            return ENET_ERROR;
        }
        while (found) {
            MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,entry.dbt_private.hwUnit);
            if (enet_UpdateDevice_ShaperProfile(unit_i,
                                                type,
                                                id,
                                                &entry.dbt_public,
                                                NULL) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - enet_UpdateDevice_ShaperProfile failed (id=%d) \n",
                                  __FUNCTION__,id);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return ENET_ERROR;
            }
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

            if (ENET_GetNext_GroupElement(unit_i,
                                          enet_ShaperProfile_type_table[type].group,
                                          &id,
                                          &entry,
                                          &len,
                                          &found) !=ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - ENET_GetNext_GroupElemenet failed \n",
                                  __FUNCTION__);
                return ENET_ERROR;
            }
        }
    }

    /* Return to Caller */
    return ENET_OK;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <enet_AddOwner_Shaper_Profile>                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status enet_AddOwner_Shaper_Profile (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_io,
                                          ENET_Shaper_Profile_dbt     *entry_i)
{

    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    ENET_Bool                          found;
//    ENET_Uint32                        len = sizeof(groupEntry);
    ENET_GroupElementId_t              id;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check key_io parameter */
    ENET_ASSERT_NULL(key_io,"key_io");

    /* check entry_i parameter */
    ENET_ASSERT_NULL(entry_i,"entry_i");

    /* Verify that the entity_type is valid */
    if (key_io->entity_type > MEA_SHAPER_SUPPORT_LAST_TYPE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Unknown key_io->entity_type %d \n",
                         __FUNCTION__,key_io->entity_type);
       return ENET_ERROR;                                                                            
    }

    /* Verify that the entity_type is valid */
    if (!enet_ShaperProfile_type_table[key_io->entity_type].support) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_io->entity_type %d  is not support\n",
                         __FUNCTION__,key_io->entity_type);
       return ENET_ERROR;                                                                            
    }

    /* Verify the acm_mode is zero */
    if (key_io->acm_mode != 0) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_io->acm_mode %d != 0 \n",
                         __FUNCTION__,key_io->acm_mode);
       return ENET_ERROR;                                                                            
    }

    /* Verify the id is ENET_PLAT_GENERATE_NEW_ID */
    if (key_io->id != ENET_PLAT_GENERATE_NEW_ID) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_io->id %d != ENET_PLAT_GENERATE_NEW_ID \n",
                         __FUNCTION__,key_io->id);
       return ENET_ERROR;                                                                            
    }

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    if (enet_Find_Shaper_Profile(unit_i,
                                 entry_i,
                                 key_io,
                                 &id,
                                 &groupEntry,
                                 &found
                                ) != ENET_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - error in find shaper element\n",
                         __FUNCTION__);
       return ENET_ERROR;                                                                            
    }

    if (found) { /* this shaper profile already exists, increase owner and return */
        groupEntry.dbt_private.numberOfOwners++;
        if (ENET_Set_GroupElement(unit_i,
                                  enet_ShaperProfile_type_table[key_io->entity_type].group,
                                  id,
                                  &groupEntry,
                                  sizeof(groupEntry))!=ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - can not set shaper element %d\n",
                              __FUNCTION__,id);
            return ENET_ERROR;                                                                            
        }        
        return ENET_OK;
    }

    /* Create Entry */
    if (ENET_Create_Shaper_Profile(unit_i,key_io,entry_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ENET_Create_Shaper_Profile failed \n",
                          __FUNCTION__);
        return ENET_ERROR;
    }

    /* Return to caller */
    return ENET_OK;

}




/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                    Public  APIs implementation                             */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <ENET_Create_Shaper_Profile>                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status ENET_Create_Shaper_Profile   (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_io,
                                          ENET_Shaper_Profile_dbt     *entry_i)
{
    
    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    MEA_db_HwUnit_t                    hwUnit;
    ENET_GroupElementId_t              id;
    ENET_GroupElementId_t              start;
    ENET_GroupElementId_t              end;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    ENET_ASSERT_UNIT_VALID(unit_i);

    ENET_ASSERT_NULL(key_io,"key_io");

    /* Verify that the entity_type is valid */
    if (key_io->entity_type > MEA_SHAPER_SUPPORT_LAST_TYPE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Unknown key_io->entity_type %d \n",
                         __FUNCTION__,key_io->entity_type);
       return ENET_ERROR;                                                                            
    }

    /* Verify that the entity_type is valid */
    if (!enet_ShaperProfile_type_table[key_io->entity_type].support) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_io->entity_type %d  is not support\n",
                         __FUNCTION__,key_io->entity_type);
       return ENET_ERROR;                                                                            
    }

    /* Verify the acm_mode */
    if (key_io->acm_mode >= enet_ShaperProfile_type_table[key_io->entity_type].max_num_of_acm) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_io->acm_mode %d >= max value \n",
                         __FUNCTION__,key_io->acm_mode,
                         enet_ShaperProfile_type_table[key_io->entity_type].max_num_of_acm);
       return ENET_ERROR;                                                                            
    }

    /* If not request to generate id */
    if ( key_io->id != ENET_PLAT_GENERATE_NEW_ID) {

        /* Verify valid id */
        if (key_io->id >= enet_ShaperProfile_type_table[key_io->entity_type].num_of_profiles_per_acm) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - key_io->id %d >= max value \n",
                             __FUNCTION__,key_io->acm_mode,
                             enet_ShaperProfile_type_table[key_io->entity_type].num_of_profiles_per_acm);
            return ENET_ERROR;                                                                            
        }

        /* Verify the id is not already exist */
        if (ENET_IsValid_Shaper_Profile(unit_i,key_io,ENET_TRUE)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Entry already exist \n",
                              __FUNCTION__);
            return ENET_ERROR;                                                                            
        }
    }

    /* Verify entry_i */
    ENET_ASSERT_NULL(entry_i,"entry_i");


    /* Check that name length not too long */
    ENET_ASSERT_VALID_NAME_LEN(entry_i->name,entry_i->name);
#if 0
    /* if no name specify then generate default name */
    if (MEA_OS_strcmp(ENET_PLAT_GENERATE_NEW_NAME,entry_i->name) == 0) {  
       MEA_OS_sprintf(entry_i->name,
                      "%-8s %d/%d",
                      (key_io->entity_type == MEA_SHAPER_SUPPORT_PORT_TYPE    ) ? "Port" : 
                      (key_io->entity_type == MEA_SHAPER_SUPPORT_CLUSTER_TYPE ) ? "Cluster" : 
                      (key_io->entity_type == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) ? "PriQueue" : 
                      "???",
                      key_io->acm_mode,
                      key_io->id);
    }
#endif
    /* Check for Shaper parameter */
    if(entry_i->mode==MEA_SHAPER_TYPE_MEF){
        if (entry_i->type == ENET_SHAPER_RESOLUTION_TYPE_BIT) {
            if ((entry_i->CIR > (MEA_uint64)ENET_SHAPER_CIR_BPS_MAX_VALUE) || 
                //(entry_i->CIR < ENET_SHAPER_CIR_BPS_MIN_VALUE) ||
                (entry_i->CBS > ENET_SHAPER_CBS_BPS_MAX_VALUE) 
                //(entry_i->CBS < ENET_SHAPER_CBS_BPS_MIN_VALUE)
                )  {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - CIR=%llu CBR=%d is invalid \n",
                                 __FUNCTION__,(int)entry_i->CIR,entry_i->CBS);
               return ENET_ERROR;
            }
        } else {
            if ((entry_i->CIR > ENET_SHAPER_CIR_PPS_MAX_VALUE) || 
                //(entry_i->CIR < ENET_SHAPER_CIR_PPS_MIN_VALUE) ||
                (entry_i->CBS > ENET_SHAPER_CBS_PPS_MAX_VALUE) 
                //(entry_i->CBS < ENET_SHAPER_CBS_PPS_MIN_VALUE)
                )  {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - CIR=%llu CBR=%d is invalid \n",
                                 __FUNCTION__,(int)entry_i->CIR,entry_i->CBS);
               return ENET_ERROR;
            }
        }

        if (entry_i->CIR == 0 && entry_i->CBS !=0) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - CBS (%d) need to be Zero when Cir is Zero \n",
                                 __FUNCTION__,entry_i->CBS);
               
           return MEA_ERROR;
        }
        if (entry_i->CIR != 0 && entry_i->CBS == 0) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - CIR (%llu) need to be Zero when CBS is Zero \n",
                                 __FUNCTION__,entry_i->CIR);   
               
            return MEA_ERROR;
        } 
        
    }else{
        if (entry_i->mode == MEA_SHAPER_TYPE_BAG)
        {
            if(entry_i->CIR > 128*8){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - CIR (%d) need to be 1 to 128 msec on mode TYPE_BAG\n",
                    __FUNCTION__,entry_i->CIR);   

                return MEA_ERROR;
            }
        }
        
    }


#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */



    /* Convert key to id */
    if (key_io->id == ENET_PLAT_GENERATE_NEW_ID) {
        id = ENET_PLAT_GENERATE_NEW_ID;
    } else {
        if (enet_ConvertToId_Shaper_Profile(unit_i,key_io,&id,ENET_FALSE) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_ConvertToId_Shaper_Profile failed \n",
                              __FUNCTION__);
            return ENET_FALSE;
        }
    }
    
    /* Calculate the range */
    start = key_io->acm_mode * enet_ShaperProfile_type_table[key_io->entity_type].num_of_profiles_per_acm;
    end   = start            + enet_ShaperProfile_type_table[key_io->entity_type].num_of_profiles_per_acm - 1;

    /* Init the groupEntry */
    MEA_OS_memset(&groupEntry,0,sizeof(groupEntry));
    groupEntry.dbt_private.numberOfOwners = 1;
    groupEntry.dbt_private.hwUnit         = hwUnit;
    MEA_OS_memcpy(&groupEntry.dbt_public,entry_i,sizeof(groupEntry.dbt_public));
    if (ENET_Create_GroupElement_withInRange(unit_i,
                                             enet_ShaperProfile_type_table[key_io->entity_type].group,
                                             &groupEntry,
                                             sizeof(groupEntry),
                                             start,
                                             end,
                                             &id
                                             )!=ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - can not create element %d\n",
                          __FUNCTION__,id);
        return ENET_ERROR;
    }


    /* Update the Hardware */
    if (enet_UpdateDevice_ShaperProfile(unit_i,
                                        key_io->entity_type,
                                        id,
                                        entry_i,
                                        NULL) == ENET_ERROR) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - enet_UpdateDevice_ShaperProfile fail\n",
                          __FUNCTION__);
        return ENET_ERROR;                                                                            
    }

    /* Convert id to key */
    if (key_io->id == ENET_PLAT_GENERATE_NEW_ID) {
        if (enet_ConvertToKey_Shaper_Profile(unit_i,key_io->entity_type,id,key_io,ENET_FALSE) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_ConvertToKey_Shaper_Profile failed \n",
                              __FUNCTION__);
            return ENET_FALSE;
        }
    }
    
    /* Return to caller */
    return ENET_OK;                                                                            
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <ENET_Set_Shaper_Profile>                         */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status ENET_Set_Shaper_Profile      (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_i,
                                          ENET_Shaper_Profile_dbt     *entry_i)
{
    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    ENET_Uint32                        len = sizeof(groupEntry);
    ENET_GroupElementId_t              id;
    MEA_db_HwUnit_t                    hwUnit;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check key_i parameter */
    ENET_ASSERT_NULL(key_i,"key_i");

    /* Verify that the entity_type is valid */
    if (key_i->entity_type > MEA_SHAPER_SUPPORT_LAST_TYPE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Unknown key_i->entity_type %d \n",
                         __FUNCTION__,key_i->entity_type);
       return ENET_ERROR;                                                                            
    }

    /* Verify that the entity_type is valid */
    if (!enet_ShaperProfile_type_table[key_i->entity_type].support) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->entity_type %d  is not support\n",
                         __FUNCTION__,key_i->entity_type);
       return ENET_ERROR;                                                                            
    }

    /* Verify the acm_mode */
    if (key_i->acm_mode >= enet_ShaperProfile_type_table[key_i->entity_type].max_num_of_acm) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->acm_mode %d >= max value \n",
                         __FUNCTION__,key_i->acm_mode,
                         enet_ShaperProfile_type_table[key_i->entity_type].max_num_of_acm);
       return ENET_ERROR;                                                                            
    }

    /* Check valid id */
    if (key_i->id >= enet_ShaperProfile_type_table[key_i->entity_type].num_of_profiles_per_acm) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->id %d >= max value \n",
                          __FUNCTION__,key_i->acm_mode,
                          enet_ShaperProfile_type_table[key_i->entity_type].num_of_profiles_per_acm);
        return ENET_ERROR;                                                                            
    }
    
    /* Check if entry exist */
    if (!ENET_IsValid_Shaper_Profile(unit_i,key_i,ENET_TRUE)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Entry not exist \n",
                          __FUNCTION__);
        return ENET_ERROR;                                                                            
    }

    /* check entry_i parameter */
    ENET_ASSERT_NULL(key_i,"entry_i");

    /* Check that name length not too long */
    ENET_ASSERT_VALID_NAME_LEN(entry_i->name,entry_i->name);
#if 0
    /* if no name specify then generate default name */
    if (MEA_OS_strcmp(ENET_PLAT_GENERATE_NEW_NAME,entry_i->name) == 0) {  
       MEA_OS_sprintf(entry_i->name,
                      "%-8s %d/%d",
                      (key_i->entity_type == MEA_SHAPER_SUPPORT_PORT_TYPE    ) ? "Port" : 
                      (key_i->entity_type == MEA_SHAPER_SUPPORT_CLUSTER_TYPE ) ? "Cluster" : 
                      (key_i->entity_type == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) ? "PriQueue" : 
                      "???",
                      key_i->acm_mode,
                      key_i->id);
    }
#endif
    /* Check for Shaper parameter */
    if (entry_i->type == ENET_SHAPER_RESOLUTION_TYPE_BIT) {
        if ((entry_i->CIR > (MEA_uint64)ENET_SHAPER_CIR_BPS_MAX_VALUE) || 
            //(entry_i->CIR < ENET_SHAPER_CIR_BPS_MIN_VALUE) ||
            (entry_i->CBS > ENET_SHAPER_CBS_BPS_MAX_VALUE) 
            //(entry_i->CBS < ENET_SHAPER_CBS_BPS_MIN_VALUE)
            )  {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - CIR=%llu CBR=%d is invalid \n",
                             __FUNCTION__,(int)entry_i->CIR,entry_i->CBS);
           return ENET_ERROR;
        }
    } else {
        if ((entry_i->CIR > ENET_SHAPER_CIR_PPS_MAX_VALUE) || 
            //(entry_i->CIR < ENET_SHAPER_CIR_PPS_MIN_VALUE) ||
            (entry_i->CBS > ENET_SHAPER_CBS_PPS_MAX_VALUE) 
            //(entry_i->CBS < ENET_SHAPER_CBS_PPS_MIN_VALUE)
            )  {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - CIR=%llu CBR=%d is invalid \n",
                             __FUNCTION__,(int)entry_i->CIR,entry_i->CBS);
           return ENET_ERROR;
        }
    }

    if (entry_i->CIR == 0 && entry_i->CBS !=0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - CBS (%d) need to be Zero when Cir is Zero \n",
                             __FUNCTION__,entry_i->CBS);
           
       return MEA_ERROR;
    }
    if (entry_i->CIR != 0 && entry_i->CBS == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - CIR (%llu) need to be Zero when CBS is Zero \n",
                             __FUNCTION__,entry_i->CIR);   
           
        return MEA_ERROR;
    }

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    /* Convert key to id */
    if (enet_ConvertToId_Shaper_Profile(unit_i,key_i,&id,ENET_FALSE) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_ConvertToId_Shaper_Profile failed \n",
                          __FUNCTION__);
        return ENET_FALSE;
    }

    /* Get Element */
    if (ENET_Get_GroupElement(unit_i,
                              enet_ShaperProfile_type_table[key_i->entity_type].group,
                              id,
                              &groupEntry,
                              &len) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - ENET_Get_GroupElement failed \n",
                         __FUNCTION__);
       return ENET_ERROR;
    }


    /* Check that we are in same hw_unit running */
    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)
    if (groupEntry.dbt_private.hwUnit != hwUnit) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - We are not in same hwUnit \n",
                         __FUNCTION__);
       return ENET_ERROR;
    }


    /* Update device */
    if (enet_UpdateDevice_ShaperProfile(unit_i,key_i->entity_type,id,entry_i,&groupEntry.dbt_public)!=ENET_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_UpdateDevice_ShaperProfile failed (type=%d,id=%d)\n",
                          __FUNCTION__,key_i->entity_type,id);
        return ENET_ERROR;
    }

    /* Update the entry */
    MEA_OS_memcpy (&groupEntry.dbt_public,entry_i,sizeof(groupEntry.dbt_public));

    /* Set Element */
    if (ENET_Set_GroupElement(unit_i,
                              enet_ShaperProfile_type_table[key_i->entity_type].group,
                              id,
                              &groupEntry,
                              len) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - ENET_Set_GroupElement failed \n",
                         __FUNCTION__);
       return ENET_ERROR;
    }

    /* Return to Caller */
    return ENET_OK;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <ENET_Delete_Shaper_Profile>                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status ENET_Delete_Shaper_Profile   (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_i)
{

    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    ENET_Uint32                        len = sizeof(groupEntry);
    ENET_GroupElementId_t              id;
    MEA_db_HwUnit_t                    hwUnit;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check key_i parameter */
    ENET_ASSERT_NULL(key_i,"key_i");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    /* Convert key to id */
    if (enet_ConvertToId_Shaper_Profile(unit_i,key_i,&id,ENET_FALSE) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_ConvertToId_Shaper_Profile failed \n",
                          __FUNCTION__);
        return ENET_FALSE;
    }

    /* Get Element */
    if (ENET_Get_GroupElement(unit_i,
                              enet_ShaperProfile_type_table[key_i->entity_type].group,
                              id,
                              &groupEntry,
                              &len) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - ENET_Get_GroupElement failed \n",
                         __FUNCTION__);
       return ENET_ERROR;
    }


    /* Check that we are in same hw_unit running */
    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)
    if (groupEntry.dbt_private.hwUnit != hwUnit) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - We are not in same hwUnit \n",
                         __FUNCTION__);
       return ENET_ERROR;
    }


    /* Check that number of Owners is 1 */
    if (groupEntry.dbt_private.numberOfOwners > 1) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Can not delete numberOfOwneres %d > 1 \n",
                         __FUNCTION__,groupEntry.dbt_private.numberOfOwners);
       return ENET_ERROR;
    }


    /* Update device */
    MEA_OS_memset(&groupEntry.dbt_public,0,sizeof(groupEntry.dbt_public));
    if (enet_UpdateDevice_ShaperProfile(unit_i,key_i->entity_type,id,&groupEntry.dbt_public,NULL)!=ENET_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_UpdateDevice_ShaperProfile failed (type=%d,id=%d)\n",
                          __FUNCTION__,key_i->entity_type,id);
        return ENET_ERROR;
    }

    /* Delete Element */
    if (ENET_Delete_GroupElement(unit_i,
                                 enet_ShaperProfile_type_table[key_i->entity_type].group,
                                 id) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - ENET_Delete_GroupElement failed \n",
                         __FUNCTION__);
       return ENET_ERROR;
    }

    /* Return to caller */
    return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <ENET_Get_Shaper_Profile>                         */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status ENET_Get_Shaper_Profile      (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_i,
                                          ENET_Shaper_Profile_dbt     *entry_o)
{
    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    ENET_Uint32                        len = sizeof(groupEntry);
    ENET_GroupElementId_t              id;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check key_i parameter */
    ENET_ASSERT_NULL(key_i,"key_i");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    if (enet_ConvertToId_Shaper_Profile(unit_i,key_i,&id,ENET_FALSE) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_ConvertToId_Shaper_Profile failed \n",
                          __FUNCTION__);
        return ENET_FALSE;
    }

    if (ENET_Get_GroupElement(unit_i,
                              enet_ShaperProfile_type_table[key_i->entity_type].group,
                              id,
                              &groupEntry,
                              &len) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - ENET_Get_GroupElement failed \n",
                         __FUNCTION__);
       return ENET_ERROR;
    }

    if (entry_o) {
        MEA_OS_memcpy(entry_o,&groupEntry.dbt_public,sizeof(*entry_o));
    }

    return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <ENET_GetFirst_Shaper_Profile>                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status ENET_GetFirst_Shaper_Profile (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_o,
                                          ENET_Shaper_Profile_dbt     *entry_o,
                                          ENET_Bool                   *found_o)
{
    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    ENET_Uint32                        len = sizeof(groupEntry);
    ENET_GroupElementId_t              id;
    MEA_ShaperSupportType_te           type;
    ENET_Bool                          found;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    /* For each type */
    for (type=0,found=ENET_FALSE;
         type<MEA_SHAPER_SUPPORT_LAST_TYPE;
         type++) {

        if (ENET_GetFirst_GroupElement(unit_i,
                                       enet_ShaperProfile_type_table[type].group,
                                       &id,
                                       &groupEntry,
                                       &len,
                                       &found) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_GetFirst_GroupElement failed \n",
                              __FUNCTION__);
            return ENET_ERROR;
        }
        if (found) {
            if (key_o) {
                if (enet_ConvertToKey_Shaper_Profile(unit_i,type,id,key_o,ENET_FALSE) != ENET_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - enet_ConvertToKey_Shaper_Profile failed \n",
                                      __FUNCTION__);
                    return ENET_ERROR;
                }
            }
            if (entry_o) {
                MEA_OS_memcpy(entry_o,&groupEntry.dbt_public,sizeof(*entry_o));
            }
            if (found_o) {
               *found_o = found;
            }    
            return ENET_OK;
       }
       
    }

    if (found_o) {
        *found_o = ENET_FALSE;
    }

    return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <ENET_GetNext_Shaper_Profile>                     */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status ENET_GetNext_Shaper_Profile (ENET_Unit_t                  unit_i,
                                         ENET_Shaper_Profile_key_dbt *key_io,
                                         ENET_Shaper_Profile_dbt     *entry_o,
                                         ENET_Bool                   *found_o)
{

    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    ENET_Uint32                        len = sizeof(groupEntry);
    ENET_GroupElementId_t              id;
    ENET_Bool                          found;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check key_io parameter */
    ENET_ASSERT_NULL(key_io,"key_io");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    /* Check for last type */
    if (key_io->entity_type >= MEA_SHAPER_SUPPORT_LAST_TYPE) {
        if (found_o) {
            *found_o = ENET_FALSE;
        }
        return ENET_OK;
    }

    /* Convert current key to id */
    if (enet_ConvertToId_Shaper_Profile(unit_i,key_io,&id,ENET_FALSE) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_ConvertToId_Shaper_Profile failed \n",
                          __FUNCTION__);
        return ENET_ERROR;
    }

    /* Get next id in this type */
    if (ENET_GetNext_GroupElement(unit_i,
                                  enet_ShaperProfile_type_table[key_io->entity_type].group,
                                  &id,
                                  &groupEntry,
                                  &len,
                                  &found) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ENET_GetFirst_GroupElement failed \n",
                          __FUNCTION__);
        return ENET_ERROR;
    }


    /* If found next entry in this type then return it */
    if (found) {
        if (enet_ConvertToKey_Shaper_Profile(unit_i,key_io->entity_type,id,key_io,ENET_FALSE) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_ConvertToKey_Shaper_Profile failed \n",
                              __FUNCTION__);
            return ENET_ERROR;
        }
        if (entry_o) {
            MEA_OS_memcpy(entry_o,&groupEntry.dbt_public,sizeof(*entry_o));
        }
        if (found_o) {
            *found_o = found;
        }    
        return ENET_OK;
    }


    /* For each type */
    for (key_io->entity_type++;
         key_io->entity_type<MEA_SHAPER_SUPPORT_LAST_TYPE;
         key_io->entity_type++) {


        if (ENET_GetFirst_GroupElement(unit_i,
                                       enet_ShaperProfile_type_table[key_io->entity_type].group,
                                       &id,
                                       &groupEntry,
                                       &len,
                                       &found) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_GetFirst_GroupElement failed \n",
                              __FUNCTION__);
            return ENET_ERROR;
        }
        if (found) {
            if (enet_ConvertToKey_Shaper_Profile(unit_i,key_io->entity_type,id,key_io,ENET_FALSE) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - enet_ConvertToKey_Shaper_Profile failed \n",
                                  __FUNCTION__);
                return ENET_ERROR;
            }
            if (entry_o) {
                MEA_OS_memcpy(entry_o,&groupEntry.dbt_public,sizeof(*entry_o));
            }
            if (found_o) {
               *found_o = found;
            }    
            return ENET_OK;
       }
       
    }

    if (found_o) {
        *found_o = ENET_FALSE;
    }

    return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <ENET_GetIDByName_Shaper_Profile>                 */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Status ENET_GetIDByName_Shaper_Profile (ENET_Unit_t                  unit_i,
                                             char                        *name_i,
                                             ENET_Shaper_Profile_key_dbt *key_o,
                                             ENET_Bool                   *found_o)
{

    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    ENET_Uint32                        len = sizeof(groupEntry);
    ENET_GroupElementId_t              id;
    MEA_ShaperSupportType_te           type;
    ENET_Bool                          found;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check name_i parameter */
    ENET_ASSERT_NULL(name_i,"name_i");
    
    ENET_ASSERT_VALID_NAME_LEN(name_i,groupEntry.dbt_public.name);
    

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


    /* For each type */
    for (type=0;type<MEA_SHAPER_SUPPORT_LAST_TYPE;type++) {

        if (ENET_GetFirst_GroupElement(unit_i,
                                       enet_ShaperProfile_type_table[type].group,
                                       &id,
                                       &groupEntry,
                                       &len,
                                       &found) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_GetFirst_GroupElement failed \n",
                              __FUNCTION__);
            return ENET_ERROR;
        }

        while (found_o) {

            if (MEA_OS_strcmp(groupEntry.dbt_public.name,name_i) == 0) {
                if (key_o) {
                    if (enet_ConvertToKey_Shaper_Profile(unit_i,type,id,key_o,ENET_FALSE) != ENET_OK) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s - enet_ConvertToKey_Shaper_Profile failed \n",
                                          __FUNCTION__);
                        return ENET_ERROR;
                    }
                }
                if (found_o) {
                    *found_o = ENET_TRUE;
                }
                return ENET_OK;
            }
        
            if (ENET_GetNext_GroupElement(unit_i,
                                          enet_ShaperProfile_type_table[type].group,
                                          &id,
                                          &groupEntry,
                                          &len,
                                          &found) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - ENET_GetFirst_GroupElement failed \n",
                                  __FUNCTION__);
                return ENET_ERROR;
            }
        }
    }

    if (found_o) {
        *found_o = ENET_FALSE;
    }

    /* Return to caller */
    return ENET_OK;
}     

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <ENET_IsValid_Shaper_Profile>                     */
/*                                                                            */
/*----------------------------------------------------------------------------*/ 
ENET_Bool  ENET_IsValid_Shaper_Profile(ENET_Unit_t                    unit_i,
                                       ENET_Shaper_Profile_key_dbt   *key_i,
                                       ENET_Bool                      silent_i)
{
    ENET_GroupElementId_t              id;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check key_i parameter */
    ENET_ASSERT_NULL(key_i,"key_i");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    if (enet_ConvertToId_Shaper_Profile(unit_i,key_i,&id,silent_i) != ENET_OK) {
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_ConvertToId_Shaper_Profile failed \n",
                              __FUNCTION__);
        }
        return ENET_FALSE;
    }

    return ENET_IsValid_GroupElement(unit_i,
                                     enet_ShaperProfile_type_table[key_i->entity_type].group,
                                     id,
                                     silent_i);

}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <enet_DelOwner_Shaper_Profile>                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_DelOwner_Shaper_Profile(ENET_Unit_t                  unit_i,
    ENET_Shaper_Profile_key_dbt *key_i)
{

    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    ENET_Uint32                        len = sizeof(groupEntry);
    ENET_GroupElementId_t              id;
    MEA_db_HwUnit_t                    hwUnit;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check key_i parameter */
    ENET_ASSERT_NULL(key_i, "key_i");

    /* Verify that the entity_type is valid */
    if (key_i->entity_type > MEA_SHAPER_SUPPORT_LAST_TYPE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Unknown key_i->entity_type %d \n",
            __FUNCTION__, key_i->entity_type);
        return ENET_ERROR;
    }

    /* Verify that the entity_type is valid */
    if (!enet_ShaperProfile_type_table[key_i->entity_type].support) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key_i->entity_type %d  is not support\n",
            __FUNCTION__, key_i->entity_type);
        return ENET_ERROR;
    }

    /* Verify the acm_mode is zero */
    if (key_i->acm_mode != 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key_i->acm_mode %d != 0 \n",
            __FUNCTION__, key_i->acm_mode);
        return ENET_ERROR;
    }

    /* Verify the id is not ENET_PLAT_GENERATE_NEW_ID */
    if (key_i->id == ENET_PLAT_GENERATE_NEW_ID) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key_i->id %d == ENET_PLAT_GENERATE_NEW_ID \n",
            __FUNCTION__, key_i->id);
        return ENET_ERROR;
    }

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    /* Convert key to id */
    if (enet_ConvertToId_Shaper_Profile(unit_i, key_i, &id, ENET_FALSE) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - enet_ConvertToId_Shaper_Profile failed \n",
            __FUNCTION__);
        return ENET_FALSE;
    }

    /* Get Element */
    if (ENET_Get_GroupElement(unit_i,
        enet_ShaperProfile_type_table[key_i->entity_type].group,
        id,
        &groupEntry,
        &len) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_Get_GroupElement failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }


    /* Check that we are in same hw_unit running */
    MEA_DRV_GET_HW_UNIT(unit_i, hwUnit, return MEA_ERROR;)
        if (groupEntry.dbt_private.hwUnit != hwUnit) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - We are not in same hwUnit \n",
                __FUNCTION__);
            return ENET_ERROR;
        }


    /* In case number of owners 1 then simple delete the entry */
    if (groupEntry.dbt_private.numberOfOwners == 1) {
        if (ENET_Delete_Shaper_Profile(unit_i, key_i) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Delete_Shaper_Profile failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }
        return ENET_OK;
    }

    /* Decrement number of owners */
    groupEntry.dbt_private.numberOfOwners--;

    /* Set Element */
    if (ENET_Set_GroupElement(unit_i,
        enet_ShaperProfile_type_table[key_i->entity_type].group,
        id,
        &groupEntry,
        len) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_Set_GroupElement failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    /* Return to caller */
    return ENET_OK;
}
/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                          <enet_Conclude_Shaper_Profile>                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Conclude_Shaper_Profile(ENET_Unit_t unit_i)
{

    enet_Shaper_Profile_GroupEntry_dbt groupEntry;
    ENET_Uint32                        len = sizeof(groupEntry);
    ENET_ShaperId_t                    id;
    ENET_Bool                          found;
    MEA_ShaperSupportType_te           type;
    ENET_Shaper_Profile_key_dbt        key;
    mea_memory_mode_e                  save_globalMemoryMode;


    /* Check that we already init */
    if (!enet_ShaperProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Not Init \n",
            __FUNCTION__);
        return ENET_ERROR;
    }



    /* For each type */
    for (type = 0; type < MEA_SHAPER_SUPPORT_LAST_TYPE; type++) {

        /* Get the first Shaper_Profile */
        if (ENET_GetFirst_GroupElement(unit_i,
            enet_ShaperProfile_type_table[type].group,
            &id,
            &groupEntry,
            &len,
            &found) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_GetFirst_GroupElement failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }

        /* scan all shaperProfile and delete one by one */
        while (found) {

            if (enet_ConvertToKey_Shaper_Profile(unit_i, type, id, &key, ENET_FALSE) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_Delete_shaperProfile failed \n",
                    __FUNCTION__);
                return ENET_ERROR;
            }
            MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode, groupEntry.dbt_private.hwUnit);
            if (ENET_Delete_Shaper_Profile(unit_i, &key) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_Delete_shaperProfile failed \n",
                    __FUNCTION__);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return ENET_ERROR;
            }
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

            if (ENET_GetFirst_GroupElement(unit_i,
                enet_ShaperProfile_type_table[type].group,
                &id,
                &groupEntry,
                &len,
                &found) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_GetNext_GroupElement failed \n",
                    __FUNCTION__);
                return ENET_ERROR;
            }

        }

        /* Delete the Group  */
        if (ENET_Delete_Group(unit_i, enet_ShaperProfile_type_table[type].group, MEA_TRUE) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Delete_Group failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }
        enet_ShaperProfile_type_table[type].group = ENET_PLAT_GENERATE_NEW_ID;
    }

    /* Indicate InitDone to false for this unit */
    enet_ShaperProfile_InitDone = ENET_FALSE;

    /* Return to caller */
    return ENET_OK;
}




