/*
This file/directory and the information contained in it are
proprietary and confidential to Ethernity Networks Ltd.
No person is allowed to copy, reprint, reproduce or publish
any part of this document, nor disclose its contents to others,
nor make any use of it, nor allow or assist others to make any
use of it - unless by prior written express
authorization of Neralink  Networks Ltd and then only to the
extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/

/*****************************************************************************/
/* 	Module Name:		 mea_ACL5_profiles_drv.c    							     */
/*																		     */
/*  Module Description:	 MEA driver										     */
/*																			 */
/*  Date of Creation:	 													 */
/*																			 */
/*  Author Name: Alex Weis  29/01/2017  									 */
/*****************************************************************************/

/*-------------------------------- Includes ------------------------------------------*/
#include "mea_api.h"
#include "MEA_platform.h"
#include "mea_drv_common.h"
#include "enet_queue_drv.h"
#include "mea_action_drv.h"
#include "mea_ACL5_profiles_drv.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

#define MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV4_MASK_WIDTH	6
#define MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV6_MASK_WIDTH	8

#define MEA_IF_ACL5_PROF_Range_MASK_MAKE0_L4_WIDTH		16
#define MEA_IF_ACL5_PROF_Range_MASK_MAKE0_dscp_WIDTH	6
#define MEA_IF_ACL5_PROF_Range_MASK_MAKE0_WIDTH			5

#define MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH		1
#define MEA_IF_ACL5_PROF_Key_MASK_MAKE0_PED_WIDTH		15

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                        */
/*----------------------------------------------------------------------------*/

typedef struct {

	MEA_Bool valid;
	MEA_ACL5_Ipmask_dbt data;

}MEA_ACL5_Ipmask_profile_dbt;

typedef struct {
	
	MEA_ACL5_Range_Mask_data_dbt data;
	
}MEA_ACL5_Range_Mask_dataEntry_dbt;


typedef struct {
	MEA_Bool valid;
	MEA_ACL5_Range_Mask_dataEntry_dbt *inf;
	MEA_Uint32 numofowners;
}MEA_ACL5_Range_Mask_profile_dbt;

typedef struct {

	MEA_Bool valid;
	MEA_ACL5_KeyMask_dbt data;

}MEA_ACL5_KeyMask_profile_dbt;

typedef struct {

	MEA_Bool valid;
	MEA_ACL5_Key_dbt data;

}MEA_ACL5_KeyProf_dbt;


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
/*The first row in the SW table is not valid, howeve it valid in the HW and it row 1 in SW.*/

MEA_ACL5_Ipmask_profile_dbt  *mea_acl5_prof_IP_mask_info_TBL = NULL;
MEA_ACL5_Range_Mask_profile_dbt *mea_acl5_prof_Range_mask_info_TBL = NULL;
MEA_ACL5_KeyMask_profile_dbt *mea_acl5_prof_Key_mask_info_TBL = NULL;
//MEA_ACL5_KeyProf_dbt *mea_acl5_prof_info_TBL = NULL;
MEA_ACL5_Mask_Grouping_Profiles *MEA_ACL5_Mask_Grouping_Profiles_TLB = NULL;

static MEA_Bool  MEA_ACL5_InitDone = MEA_FALSE;

/*-------------------------------- External Variables --------------------------------*/



/*-------------------------------- static function   ----------------------------------*/

/*------------------ACL5 IP Mask  profile--------------------*/


static MEA_Bool mea_drv_ACL5_prof_IP_mask_prof_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i);
static MEA_Status mea_drv_ACL5_prof_IP_mask_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist);
static MEA_Bool mea_drv_ACL5_Prof_IP_mask_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io);
static void mea_drv_ACL5_prof_IPmask_UpdateHwEntry(MEA_funcParam arg1,
	MEA_funcParam arg2,
	MEA_funcParam arg3,
	MEA_funcParam arg4);
static MEA_Status mea_drv_ACL5_prof_IP_mask_UpdateHw(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t                        id_i,
	MEA_ACL5_Ipmask_dbt    *new_entry_pi,
	MEA_ACL5_Ipmask_dbt    *old_entry_pi);


/*------------------ACL5 Range Mask  profile--------------------*/

static MEA_Status mea_drv_ACL5_prof_Range_mask_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist);

static MEA_Status mea_drv_ACL5_prof_Range_mask_IsExistIndex(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_ACL5Index_t index,
	MEA_Bool silent,
	MEA_Bool *exist);

static MEA_Bool mea_drv_ACL5_prof_Range_mask_prof_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i);

static MEA_Bool mea_drv_ACL5_prof_Range_mask_prof_IsRangeIndex(MEA_Unit_t     unit_i,
	MEA_ACL5Index_t index);

static MEA_Bool mea_drv_ACL5_Prof_Range_mask_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io);

static void mea_drv_ACL5_prof_RangeMask_UpdateHwEntry(MEA_funcParam arg1,
	MEA_funcParam arg2,
	MEA_funcParam arg3,
	MEA_funcParam arg4);

static MEA_Status mea_drv_ACL5_prof_Range_mask_UpdateHw(MEA_Unit_t    unit_i,
	MEA_ACL5Id_t                        id_i,
	MEA_ACL5Index_t					index,
	MEA_ACL5_Range_Mask_data_dbt    *new_entry_pi,
	MEA_ACL5_Range_Mask_data_dbt    *old_entry_pi);


/*------------------ACL5 Key Mask  profile--------------------*/

static MEA_Status mea_drv_ACL5_prof_Key_mask_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist);

static MEA_Bool mea_drv_ACL5_prof_Key_mask_prof_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i);

static MEA_Bool mea_drv_ACL5_Prof_Key_mask_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io);

static void mea_drv_ACL5_prof_KeyMask_UpdateHwEntry(MEA_funcParam arg1,
	MEA_funcParam arg2,
	MEA_funcParam arg3,
	MEA_funcParam arg4);

static MEA_Status mea_drv_ACL5_prof_Key_mask_UpdateHw(MEA_Unit_t   unit_i,
	MEA_ACL5Id_t                        id_i,
	MEA_ACL5_KeyMask_dbt    *new_entry_pi,
	MEA_ACL5_KeyMask_dbt    *old_entry_pi);


/*------------------ACL5_Block_Profiles_Table--------------------*/


static MEA_Bool mea_drv_ACL5_Mask_Grouping_Profiles_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io);

static MEA_Bool mea_drv_ACL5_Mask_Grouping_Profiles_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i);

static MEA_Status mea_drv_ACL5_Mask_Grouping_Profiles_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist);

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

/*------------------ACL5 IP Mask  profile--------------------*/



static MEA_Bool mea_drv_ACL5_prof_IP_mask_prof_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i)
{

	if ((id_i) >= MEA_ACL5_IPMASK_MAX_PROF) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "out range id_i %d > \n", id_i, MEA_ACL5_IPMASK_MAX_PROF);
		return MEA_FALSE;
	}
	return MEA_TRUE;
}

static MEA_Status mea_drv_ACL5_prof_IP_mask_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist)
{

	if (exist == NULL) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);

		return MEA_ERROR;
	}
	*exist = MEA_FALSE;

	if (mea_drv_ACL5_prof_IP_mask_prof_IsRange(unit_i, id_i) != MEA_TRUE) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5_prof_IP_mask_prof_IsRange failed  for id=%d  \n", __FUNCTION__, id_i);

		return MEA_ERROR;
	}

	if (mea_acl5_prof_IP_mask_info_TBL[id_i].valid == MEA_TRUE) {
		*exist = MEA_TRUE;
		return MEA_OK;
	}

	return MEA_OK;
}

static MEA_Bool mea_drv_ACL5_Prof_IP_mask_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io)
{
	MEA_Uint32 i;

	for (i = 1; i <= MEA_ACL5_IPMASK_MAX_PROF; i++) {
		if (mea_acl5_prof_IP_mask_info_TBL[i].valid == MEA_FALSE) {
			*id_io = i;
			return MEA_TRUE;
		}
	}
	return MEA_FALSE;

}

static void mea_drv_ACL5_prof_IPmask_UpdateHwEntry(MEA_funcParam arg1,
	MEA_funcParam arg2,
	MEA_funcParam arg3,
	MEA_funcParam arg4)
{
	MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
	//MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
	//MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
	MEA_ACL5_Ipmask_dbt  *entry = (MEA_ACL5_Ipmask_dbt  *)arg4;




	MEA_Uint32                 val[1];
	MEA_Uint32                 i = 0;
	MEA_Uint32                  count_shift;

	/* Zero  the val */
	for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
		val[i] = 0;
	}
	count_shift = 0;


	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV4_MASK_WIDTH,
		((MEA_Uint32)entry->Ipv4_source_mask),
		&val);
	count_shift += MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV4_MASK_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV4_MASK_WIDTH,
		((MEA_Uint32)entry->Ipv4_dest_mask),
		&val); 
	count_shift += MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV4_MASK_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV6_MASK_WIDTH,
		(MEA_Uint32)entry->Ipv6_source_mask,
		&val);
	count_shift += MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV6_MASK_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV6_MASK_WIDTH,
		(MEA_Uint32)entry->Ipv6_dest_mask,
		&val); 
	count_shift += MEA_IF_ACL5_PROF_IP_MASK_MAKE0_IPV6_MASK_WIDTH;


	/* Update Hw Entry */
	for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
	{
		MEA_API_WriteReg(unit_i,
			MEA_IF_WRITE_DATA_REG(i),
			val[i],
			MEA_MODULE_IF);
	}



}

static MEA_Status mea_drv_ACL5_prof_IP_mask_UpdateHw(MEA_Unit_t                        unit_i,
	MEA_ACL5Id_t                        id_i,
	MEA_ACL5_Ipmask_dbt    *new_entry_pi,
	MEA_ACL5_Ipmask_dbt    *old_entry_pi)
{

	MEA_ind_write_t   ind_write;



	/* check if we have any changes */
	if ((old_entry_pi) &&
		(MEA_OS_memcmp(new_entry_pi,
			old_entry_pi,
			sizeof(*new_entry_pi)) == 0)) {
		return MEA_OK;
	}


	
		/* build the ind_write value */
		ind_write.tableType = ENET_IF_CMD_PARAM_TBL_IPMASK_ACL5;
		ind_write.tableOffset = id_i;
		ind_write.cmdReg = MEA_IF_CMD_REG;
		ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
		ind_write.statusReg = MEA_IF_STATUS_REG;
		ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
		ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
		ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
		ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_ACL5_prof_IPmask_UpdateHwEntry;
		ind_write.funcParam1 = (MEA_funcParam)unit_i;
		//ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
		//ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
		ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

		if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
				__FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
			return MEA_ERROR;
		}
	


	/* Return to caller */
	return MEA_OK;
}


/*------------------ACL5 Range Mask  profile--------------------*/

static MEA_Status mea_drv_ACL5_prof_Range_mask_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist)
{

	if (exist == NULL) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);

		return MEA_ERROR;
	}
	*exist = MEA_FALSE;

	if (mea_drv_ACL5_prof_Range_mask_prof_IsRange(unit_i,id_i) != MEA_TRUE) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5_prof_Range_mask_prof_IsRange failed  for id=%d  \n", __FUNCTION__, id_i);

		return MEA_ERROR;
	}

	if (mea_acl5_prof_Range_mask_info_TBL[id_i].valid == MEA_TRUE) {
		*exist = MEA_TRUE;
		return MEA_OK;
	}

	return MEA_OK;
}

static MEA_Status mea_drv_ACL5_prof_Range_mask_IsExistIndex(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_ACL5Index_t index,
	MEA_Bool silent,
	MEA_Bool *exist)
{

	if (exist == NULL) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);

		return MEA_ERROR;
	}
	*exist = MEA_FALSE;

	if (mea_drv_ACL5_prof_Range_mask_prof_IsRange(unit_i, id_i) != MEA_TRUE) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5_prof_Range_mask_prof_IsRange failed  for id=%d  \n", __FUNCTION__, id_i);

		return MEA_ERROR;
	}

	if (mea_drv_ACL5_prof_Range_mask_prof_IsRangeIndex(unit_i, index) != MEA_TRUE) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5_prof_Range_mask_prof_IsRangeIndex failed  for index=%d  \n", __FUNCTION__, index);

		return MEA_ERROR;
	}

	if (mea_acl5_prof_Range_mask_info_TBL[id_i].inf[index].data.valid == MEA_TRUE) {
		*exist = MEA_TRUE;
		return MEA_OK;
	}

	return MEA_OK;
}

static MEA_Bool mea_drv_ACL5_prof_Range_mask_prof_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i)
{

	if ((id_i) >= MEA_ACL5_RANGEMASK_MAX_PROF) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "out range id_i %d > \n", id_i, MEA_ACL5_RANGEMASK_MAX_PROF);
		return MEA_FALSE;
		}
	
	return MEA_TRUE;
}

static MEA_Bool mea_drv_ACL5_prof_Range_mask_prof_IsRangeIndex(MEA_Unit_t     unit_i,
	MEA_ACL5Index_t index)
{

	if ((index) >= MEA_ACL5_RANGEMASK_MAX_BOLOCK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "out range index %d >= \n", index, MEA_ACL5_RANGEMASK_MAX_BOLOCK);
		return MEA_FALSE;
	}

	return MEA_TRUE;
}

static MEA_Bool mea_drv_ACL5_Prof_Range_mask_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io)
{
	MEA_Uint32 i;

	for (i = 1; i <= MEA_ACL5_RANGEMASK_MAX_PROF; i++) {
		if (mea_acl5_prof_Range_mask_info_TBL[i].valid == MEA_FALSE) {
			*id_io = i;
			return MEA_TRUE;
		}
	}
	return MEA_FALSE;

}
#if 0
static MEA_Bool mea_drv_ACL5_Prof_Range_mask_find_free_index(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io,
	MEA_ACL5Index_t    *index)
{
	MEA_Uint32 i;

	for (i = 0; i < MEA_ACL5_RANGEMASK_MAX_BOLOCK; i++) {
		if (mea_acl5_prof_Range_mask_info_TBL[*id_io].inf[i].data.valid == MEA_FALSE) {
			*index = i;
			return MEA_TRUE;
		}
	}
	return MEA_FALSE;

}
#endif
static void mea_drv_ACL5_prof_RangeMask_UpdateHwEntry(MEA_funcParam arg1,
	MEA_funcParam arg2,
	MEA_funcParam arg3,
	MEA_funcParam arg4)
{
	MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
	//MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
	//MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
	MEA_ACL5_Range_Mask_data_dbt  *entry = (MEA_ACL5_Range_Mask_data_dbt  *)arg4;




	MEA_Uint32                 val[3];
	MEA_Uint32                 i = 0;
	MEA_Uint32                  count_shift;

	/* Zero  the val */
	for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
		val[i] = 0;
	}
	count_shift = 0;


	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Range_MASK_MAKE0_L4_WIDTH,
		((MEA_Uint32)entry->L4src_min),
		&val);
	count_shift += MEA_IF_ACL5_PROF_Range_MASK_MAKE0_L4_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Range_MASK_MAKE0_L4_WIDTH,
		((MEA_Uint32)entry->L4src_max),
		&val);
	count_shift += MEA_IF_ACL5_PROF_Range_MASK_MAKE0_L4_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Range_MASK_MAKE0_L4_WIDTH,
		(MEA_Uint32)entry->L4dest_min,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Range_MASK_MAKE0_L4_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Range_MASK_MAKE0_L4_WIDTH,
		(MEA_Uint32)entry->L4dest_max,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Range_MASK_MAKE0_L4_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Range_MASK_MAKE0_dscp_WIDTH,
		(MEA_Uint32)entry->dscp_min,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Range_MASK_MAKE0_dscp_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Range_MASK_MAKE0_dscp_WIDTH,
		(MEA_Uint32)entry->dscp_max,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Range_MASK_MAKE0_dscp_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Range_MASK_MAKE0_WIDTH,
		(MEA_Uint32)entry->priValue,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Range_MASK_MAKE0_WIDTH;
	
	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Range_MASK_MAKE0_WIDTH,
		(MEA_Uint32)entry->Id,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Range_MASK_MAKE0_WIDTH;
	
	/* Update Hw Entry */
	for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
	{
		MEA_API_WriteReg(unit_i,
			MEA_IF_WRITE_DATA_REG(i),
			val[i],
			MEA_MODULE_IF);
	}



}

static MEA_Status mea_drv_ACL5_prof_Range_mask_UpdateHw(MEA_Unit_t   unit_i,
	MEA_ACL5Id_t                        id_i,
	MEA_ACL5Index_t					index,
	MEA_ACL5_Range_Mask_data_dbt    *new_entry_pi,
	MEA_ACL5_Range_Mask_data_dbt    *old_entry_pi)
{

	MEA_ind_write_t   ind_write;



	/* check if we have any changes */
	if ((old_entry_pi) &&
		(MEA_OS_memcmp(new_entry_pi,
			old_entry_pi,
			sizeof(*new_entry_pi)) == 0)) {
		return MEA_OK;
	}


		/* build the ind_write value */
		ind_write.tableType = ENET_IF_CMD_PARAM_TBL_RANGE_ACL5;
		ind_write.tableOffset =  (id_i*MEA_ACL5_RANGEMASK_MAX_BOLOCK) + index;
		ind_write.cmdReg = MEA_IF_CMD_REG;
		ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
		ind_write.statusReg = MEA_IF_STATUS_REG;
		ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
		ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
		ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
		ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_ACL5_prof_RangeMask_UpdateHwEntry;
		ind_write.funcParam1 = (MEA_funcParam)unit_i;
		//ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
		//ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
		ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

		if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
				__FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
			return MEA_ERROR;
		}
	


	/* Return to caller */
	return MEA_OK;
}


/*------------------ACL5 Key Mask  profile--------------------*/

static MEA_Status mea_drv_ACL5_prof_Key_mask_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist)
{

	if (exist == NULL) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);

		return MEA_ERROR;
	}
	*exist = MEA_FALSE;

	if (mea_drv_ACL5_prof_Key_mask_prof_IsRange(unit_i, id_i) != MEA_TRUE) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5_prof_Key_mask_prof_IsRange failed  for id=%d  \n", __FUNCTION__, id_i);

		return MEA_ERROR;
	}

	if (mea_acl5_prof_Key_mask_info_TBL[id_i].valid == MEA_TRUE) {
		*exist = MEA_TRUE;
		return MEA_OK;
	}

	return MEA_OK;
}

static MEA_Bool mea_drv_ACL5_prof_Key_mask_prof_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i)
{

	if ((id_i) >= MEA_ACL5_KEYMASK_MAX_PROF) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "out range id_i %d >= \n", id_i, MEA_ACL5_KEYMASK_MAX_PROF);
		return MEA_FALSE;
	}
	return MEA_TRUE;
}

static MEA_Bool mea_drv_ACL5_Prof_Key_mask_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io)
{
	MEA_Uint32 i;

	for (i = 1; i <= MEA_ACL5_KEYMASK_MAX_PROF; i++) {
		if (mea_acl5_prof_Key_mask_info_TBL[i].valid == MEA_FALSE) {
			*id_io = i;
			return MEA_TRUE;
		}
	}
	return MEA_FALSE;

}

static void mea_drv_ACL5_prof_KeyMask_UpdateHwEntry(MEA_funcParam arg1,
	MEA_funcParam arg2,
	MEA_funcParam arg3,
	MEA_funcParam arg4)
{
	MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
	//MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
	//MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
	MEA_ACL5_KeyMask_dbt  *entry = (MEA_ACL5_KeyMask_dbt  *)arg4;




	MEA_Uint32                 val[1];
	MEA_Uint32                 i = 0;
	MEA_Uint32                  count_shift;

	/* Zero  the val */
	for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
		val[i] = 0;
	}
	count_shift = 0;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->L3_EtherType,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->Ip_Protocol_NH,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->L4src,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->L4dst,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->TCP_Flags,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->DSCP_TC,
		&val);
	count_shift += (MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH +1);

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->IPv4_Flags,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->TAG1,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->TAG2,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->TAG3,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;


	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->TAG1_p,
		&val);
	count_shift += (MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH +2);

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->IPv6FlowLabel,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH,
		(MEA_Uint32)entry->SID,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_FLAG_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_IF_ACL5_PROF_Key_MASK_MAKE0_PED_WIDTH,
		(MEA_Uint32)entry->pad,
		&val);
	count_shift += MEA_IF_ACL5_PROF_Key_MASK_MAKE0_PED_WIDTH;

	/* Update Hw Entry */
	for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
	{
		MEA_API_WriteReg(unit_i,
			MEA_IF_WRITE_DATA_REG(i),
			val[i],
			MEA_MODULE_IF);
	}

}

static MEA_Status mea_drv_ACL5_prof_Key_mask_UpdateHw(MEA_Unit_t   unit_i,
	MEA_ACL5Id_t                        id_i,
	MEA_ACL5_KeyMask_dbt    *new_entry_pi,
	MEA_ACL5_KeyMask_dbt    *old_entry_pi)
{

	MEA_ind_write_t   ind_write;



	/* check if we have any changes */
	if ((old_entry_pi) &&
		(MEA_OS_memcmp(new_entry_pi,
			old_entry_pi,
			sizeof(*new_entry_pi)) == 0)) {
		return MEA_OK;
	}


		/* build the ind_write value */
		ind_write.tableType = ENET_IF_CMD_PARAM_TBL_KEYMASK_ACL5;
		ind_write.tableOffset =  id_i;
		ind_write.cmdReg = MEA_IF_CMD_REG;
		ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
		ind_write.statusReg = MEA_IF_STATUS_REG;
		ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
		ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
		ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
		ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_ACL5_prof_KeyMask_UpdateHwEntry;
		ind_write.funcParam1 = (MEA_funcParam)unit_i;
		//ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
		//ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
		ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

		if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
				__FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
			return MEA_ERROR;
		}
	


	/* Return to caller */
	return MEA_OK;
}


/*------------------ACL5_Block_Profiles_Table--------------------*/


static MEA_Bool mea_drv_ACL5_Mask_Grouping_Profiles_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io)
{
	MEA_Uint32 i;

	for (i = 1; i < MEA_ACL5_MASK_GROUPING_PROFILES_MAX_PROF; i++) {
		if (MEA_ACL5_Mask_Grouping_Profiles_TLB[i].valid == MEA_FALSE) {
			*id_io = i;
			return MEA_TRUE;
		}
	}
	return MEA_FALSE;

}

static MEA_Bool mea_drv_ACL5_Mask_Grouping_Profiles_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i)
{

	if ((id_i) >= MEA_ACL5_MASK_GROUPING_PROFILES_MAX_PROF) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "out range id_i %d >= \n", id_i, MEA_ACL5_MASK_GROUPING_PROFILES_MAX_PROF);
		return MEA_FALSE;
	}
	return MEA_TRUE;
}

static MEA_Status mea_drv_ACL5_Mask_Grouping_Profiles_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist)
{

	if (exist == NULL) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);

		return MEA_ERROR;
	}
	*exist = MEA_FALSE;

	if (mea_drv_ACL5_Mask_Grouping_Profiles_IsRange(unit_i, id_i) != MEA_TRUE) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5_Mask_Grouping_Profiles_IsRange failed  for id=%d  \n", __FUNCTION__, id_i);

		return MEA_ERROR;
	}

	if (MEA_ACL5_Mask_Grouping_Profiles_TLB[id_i].valid == MEA_TRUE) {
		*exist = MEA_TRUE;
		return MEA_OK;
	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_ACL5_drv_Add_owner>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_ACL5_drv_Add_owner(MEA_Unit_t  unit,
	MEA_ACL5Id_t  Id)
{
	MEA_Bool exist;

	if (mea_drv_ACL5_prof_Range_mask_IsExist(unit, Id, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5_prof_Range_mask_IsExist failed  \n", __FUNCTION__);
		return MEA_ERROR;
	}
	mea_acl5_prof_Range_mask_info_TBL[Id].numofowners++;
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_ACL5_drv_delete_owner>                                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_ACL5_drv_delete_owner(MEA_Unit_t  unit,
	MEA_ACL5Id_t  Id)
{

	MEA_Bool exist;


	if (Id == 0) {
		return MEA_OK;
	}

	if (mea_drv_ACL5_prof_Range_mask_IsExist(unit, Id, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5_prof_Range_mask_IsExist failed  \n", __FUNCTION__);
		return MEA_ERROR;
	}
	if (exist == MEA_FALSE) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id is not exsit  \n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (mea_acl5_prof_Range_mask_info_TBL[Id].numofowners <= 1) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - invalid num_of_owners (%d) \n", __FUNCTION__, mea_acl5_prof_Range_mask_info_TBL[Id].numofowners);
		return MEA_ERROR;
	}

	/*decrease owner*/
	mea_acl5_prof_Range_mask_info_TBL[Id].numofowners--;


	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Init_ACL5_Table>							             */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status mea_drv_Init_ACL5_Profiles(MEA_Unit_t unit_i)
{
	MEA_Uint32				size;
	MEA_Uint32				j;
	MEA_Uint32				i;

	if (!MEA_ACL5_SUPPORT) {
		
		return MEA_OK;
	}


	if (MEA_ACL5_InitDone) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Already Init \n", __FUNCTION__);
		return MEA_ERROR;
	}

	MEA_ACL5_InitDone = MEA_FALSE;
	
	/* Init IF ACL5 Table */
	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize IF ACL5 table ...\n");
	/* Init db - Hardware shadow */

	size = (MEA_ACL5_IPMASK_MAX_PROF) * sizeof(MEA_ACL5_Ipmask_profile_dbt);
	mea_acl5_prof_IP_mask_info_TBL = (MEA_ACL5_Ipmask_profile_dbt*)MEA_OS_malloc(size);
	if (mea_acl5_prof_IP_mask_info_TBL == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Allocate mea_acl5_prof_IP_mask_info_TBL failed (size=%d)\n",
			__FUNCTION__, size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)mea_acl5_prof_IP_mask_info_TBL, 0, size);



	size = (MEA_ACL5_RANGEMASK_MAX_PROF) * sizeof(MEA_ACL5_Range_Mask_profile_dbt);
	mea_acl5_prof_Range_mask_info_TBL = (MEA_ACL5_Range_Mask_profile_dbt*)MEA_OS_malloc(size);
	if (mea_acl5_prof_Range_mask_info_TBL == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Allocate mea_acl5_prof_Range_mask_info_TBL failed (size=%d)\n",
			__FUNCTION__, size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)mea_acl5_prof_Range_mask_info_TBL, 0, size);

	for (i=0; i<MEA_ACL5_RANGEMASK_MAX_PROF;i++)
	{
		mea_acl5_prof_Range_mask_info_TBL[i].inf= (MEA_ACL5_Range_Mask_dataEntry_dbt*)MEA_OS_malloc(MEA_ACL5_RANGEMASK_MAX_BOLOCK*MEA_ACL5_RANGEMASK_MAX_BOLOCK);
		if (mea_acl5_prof_Range_mask_info_TBL[i].inf == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - Allocate mea_acl5_prof_Range_mask_info_TBL[%d] failed (size=%d)\n",
				__FUNCTION__, i, MEA_ACL5_RANGEMASK_MAX_BOLOCK);
			return MEA_ERROR;
		}
		mea_acl5_prof_Range_mask_info_TBL[i].valid = MEA_FALSE;
		mea_acl5_prof_Range_mask_info_TBL[i].numofowners = 0;
			//init hw
			for (j = 0; j < MEA_ACL5_RANGEMASK_MAX_BOLOCK; j++)
			{
				MEA_OS_memset( (char*) &mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data ,0,sizeof(MEA_ACL5_Range_Mask_data_dbt));
				mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data.valid = MEA_FALSE;
				mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data.L4src_min = 1;
				mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data.L4src_max = 0;
				mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data.L4dest_min = 1;
				mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data.L4dest_max = 0;
				mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data.dscp_min = 1;
				mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data.dscp_max = 0;
				mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data.priValue = 0;
				mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data.Id = 0;
			
				
				mea_drv_ACL5_prof_Range_mask_UpdateHw(unit_i,
					i,
					j,
					&(mea_acl5_prof_Range_mask_info_TBL[i].inf[j].data),
					NULL);
			}
	}


	size = (MEA_ACL5_KEYMASK_MAX_PROF) * sizeof(MEA_ACL5_KeyMask_profile_dbt);
	mea_acl5_prof_Key_mask_info_TBL = (MEA_ACL5_KeyMask_profile_dbt*)MEA_OS_malloc(size);
	if (mea_acl5_prof_Key_mask_info_TBL == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Allocate mea_acl5_prof_Key_mask_info_TBL failed (size=%d)\n",
			__FUNCTION__, size);
		return MEA_ERROR;
	}
	
	MEA_OS_memset((char*)mea_acl5_prof_Key_mask_info_TBL, 0, size);

	size = (MEA_ACL5_MASK_GROUPING_PROFILES_MAX_PROF) * sizeof(MEA_ACL5_Mask_Grouping_Profiles);
	MEA_ACL5_Mask_Grouping_Profiles_TLB = (MEA_ACL5_Mask_Grouping_Profiles*)MEA_OS_malloc(size);
	if (MEA_ACL5_Mask_Grouping_Profiles_TLB == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Allocate MEA_ACL5_Mask_Grouping_Profiles_TLB failed (size=%d)\n",
			__FUNCTION__, size);
		return MEA_ERROR;
	}

	MEA_OS_memset((char*)MEA_ACL5_Mask_Grouping_Profiles_TLB, 0, size);
	

	

	MEA_ACL5_InitDone = MEA_TRUE;
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/  
/*                                                                           */
/*            <mea_drv_ReInit_ACL5_Table>							         */
/*                                                                           */
/*---------------------------------------------------------------------------*/


MEA_Status mea_drv_ReInit_ACL5_Table(MEA_Unit_t unit_i)
{
	MEA_ACL5Id_t id;
	MEA_ACL5Index_t index;

	/* Check if support */
	if (!MEA_ACL5_SUPPORT) {

		return MEA_OK;
	}

	/* ReInit ACL5 Mask table   */
	for (id = 1; id < MEA_ACL5_IPMASK_MAX_PROF; id++) {
		if (mea_acl5_prof_IP_mask_info_TBL[id].valid) {
			if (mea_drv_ACL5_prof_IP_mask_UpdateHw(unit_i,
				id,
				&(mea_acl5_prof_IP_mask_info_TBL[id].data),
				NULL) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s -  mea_drv_acl5_prof_IP_mask_UpdateHw failed (id=%d)\n", __FUNCTION__, id);
				return MEA_ERROR;
			}
		}
	}

	for (id = 1; id < MEA_ACL5_RANGEMASK_MAX_PROF; id++) {
		if(mea_acl5_prof_Range_mask_info_TBL[id].valid)
		{
			for (index = 0; index < MEA_ACL5_RANGEMASK_MAX_BOLOCK; index++) {
				if (mea_acl5_prof_Range_mask_info_TBL[id].inf[index].data.valid) {
					if (mea_drv_ACL5_prof_Range_mask_UpdateHw(unit_i,
						id,
						index,
						&(mea_acl5_prof_Range_mask_info_TBL[id].inf[index].data),
						NULL) != MEA_OK) {
						MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							"%s -  mea_drv_acl5_prof_Range_mask_UpdateHw failed (id=%d, index=%d)\n", __FUNCTION__, id, index);
						return MEA_ERROR;

					}
				}
			}
		}
	}

	for (id = 1; id < MEA_ACL5_KEYMASK_MAX_PROF; id++) {
		if (mea_acl5_prof_Key_mask_info_TBL[id].valid) {
			if (mea_drv_ACL5_prof_Key_mask_UpdateHw(unit_i,
				id,
				&(mea_acl5_prof_Key_mask_info_TBL[id].data),
				NULL) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s -  mea_drv_acl5_prof_Key_mask_UpdateHw failed (id=%d)\n", __FUNCTION__, id);
				return MEA_ERROR;
			}
		}
	}


	

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/ 
/*                                                                           */
/*            <mea_drv_Conclude_ACL5_Table>							         */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status mea_drv_ACL5_Conclude(MEA_Unit_t  unit_i)
{
    MEA_Uint32 i;
    /* Check if support */
	if (!MEA_ACL5_SUPPORT) {

		return MEA_OK;
	}

	/* Free the table */
	if (mea_acl5_prof_IP_mask_info_TBL != NULL) {
		MEA_OS_free(mea_acl5_prof_IP_mask_info_TBL);
		mea_acl5_prof_IP_mask_info_TBL = NULL;
	}

	for (i = 0; i < MEA_ACL5_RANGEMASK_MAX_PROF; i++)
	{
		if (mea_acl5_prof_Range_mask_info_TBL[i].inf != NULL) {
			MEA_OS_free(mea_acl5_prof_Range_mask_info_TBL[i].inf);
			mea_acl5_prof_Range_mask_info_TBL[i].inf = NULL;
		}
	}

	if (mea_acl5_prof_Range_mask_info_TBL != NULL) {
		MEA_OS_free(mea_acl5_prof_Range_mask_info_TBL);
		mea_acl5_prof_Range_mask_info_TBL = NULL;
	}

	if (mea_acl5_prof_Key_mask_info_TBL != NULL) {
		MEA_OS_free(mea_acl5_prof_Key_mask_info_TBL);
		mea_acl5_prof_Key_mask_info_TBL = NULL;
	}

	if (MEA_ACL5_Mask_Grouping_Profiles_TLB != NULL) {
		MEA_OS_free(MEA_ACL5_Mask_Grouping_Profiles_TLB);
		MEA_ACL5_Mask_Grouping_Profiles_TLB = NULL;
	}

	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_ACL5_ACL5_Block_Profiles_Table>							 */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_API_ACL5_Mask_Grouping_Profiles_isExist(MEA_Unit_t  unit, MEA_ACL5Id_t Id_o, MEA_Bool silent, MEA_Bool *exist)
{
    return mea_drv_ACL5_Mask_Grouping_Profiles_IsExist(unit, Id_o, silent, exist);
}


MEA_Status MEA_API_ACL5_Mask_Grouping_Profiles_Create(MEA_Unit_t  unit_i, MEA_ACL5_Mask_Grouping_Profiles* entry_prof, MEA_ACL5Id_t *Id_io)
{
	MEA_Bool    exist;

	/* Check if support */
	if (!MEA_ACL5_SUPPORT) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - MEA_ACL5_SUPPORT not support \n",
			__FUNCTION__);
		return MEA_ERROR;
	}

	if (entry_prof == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - entry is null\n", __FUNCTION__);
		return MEA_ERROR;
	}
    if (Id_io == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id_io is null\n", __FUNCTION__);
    }



	/*check if the ip_mask_id exist*/
	if (mea_drv_ACL5_prof_IP_mask_IsExist(unit_i, entry_prof->ip_mask_id, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_IP_mask_IsExist failed (ip_mask_id=%d\n",
			__FUNCTION__, entry_prof->ip_mask_id);
		return MEA_ERROR;
	}
	if ((!exist) && (entry_prof->ip_mask_id != 0)) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - ip_mask_id %d not exist\n", __FUNCTION__, entry_prof->ip_mask_id);
		return MEA_ERROR;
	}

	/*check if the range_mask_id exist*/
	if (mea_drv_ACL5_prof_Range_mask_IsExist(unit_i, entry_prof->range_mask_id, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Range_mask_IsExist failed (range_mask_id=%d\n",
			__FUNCTION__, entry_prof->range_mask_id);
		return MEA_ERROR;
	}
	if ((!exist) && (entry_prof->range_mask_id != 0)) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - range_mask_id %d not exist\n", __FUNCTION__, entry_prof->range_mask_id);
		return MEA_ERROR;
	}

	/*check if the key_mask_id exist*/
	if (mea_drv_ACL5_prof_Key_mask_IsExist(unit_i, entry_prof->key_mask_id, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Key_mask_IsExist failed (key_mask_id=%d\n",
			__FUNCTION__, entry_prof->key_mask_id);
		return MEA_ERROR;
	}
	if ((!exist) && (entry_prof->key_mask_id != 0)) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - key_mask_id %d not exist\n", __FUNCTION__, entry_prof->key_mask_id);
		return MEA_ERROR;
	}


    if ((*Id_io) != MEA_PLAT_GENERATE_NEW_ID) {
        if(mea_drv_ACL5_Mask_Grouping_Profiles_IsRange(unit_i, *Id_io) == MEA_FALSE){
            return MEA_ERROR;
        }
        if(mea_drv_ACL5_Mask_Grouping_Profiles_IsExist(unit_i, *Id_io, MEA_TRUE, &exist) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_ACL5_Mask_Grouping_Profiles_IsExist failed (*id_io=%d\n",
                __FUNCTION__, *Id_io);
            return MEA_ERROR;
        }
        if (exist) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - *Id_io %d is already exist\n", __FUNCTION__, *Id_io);
            return MEA_ERROR;
        }
    
	}
	else {
		/*find new place*/
		if (mea_drv_ACL5_Mask_Grouping_Profiles_find_free(unit_i, Id_io) != MEA_TRUE) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_Mask_Grouping_Profiles_find_free failed\n", __FUNCTION__);
			return MEA_ERROR;
		}
	}
	    MEA_OS_memcpy(&(MEA_ACL5_Mask_Grouping_Profiles_TLB[*Id_io]), entry_prof, sizeof(MEA_ACL5_Mask_Grouping_Profiles_TLB[*Id_io]));
		MEA_ACL5_Mask_Grouping_Profiles_TLB[*Id_io].valid = MEA_TRUE;
    	

	return MEA_OK;
}

MEA_Status MEA_API_ACL5_Mask_Grouping_Profiles_Get(MEA_Unit_t  unit_i, MEA_ACL5Id_t Id_i, MEA_ACL5_Mask_Grouping_Profiles* entry_prof)
{
	MEA_Bool    exist;

	/* Check if support */
	if (!MEA_ACL5_SUPPORT) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - MEA_ACL5_SUPPORT not support \n",
			__FUNCTION__);
		return MEA_ERROR;
	}

	
	/*check if the id exist*/
	if (MEA_API_ACL5_Mask_Grouping_Profiles_isExist(unit_i, Id_i, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - MEA_API_ACL5_Mask_Grouping_Profiles_isExist failed (id_i=%d\n",
			__FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - id %d not exist\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (entry_prof == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - entry is null\n", __FUNCTION__, Id_i);
		return MEA_ERROR;

	}
	

	/* Copy to caller structure */
	MEA_OS_memset(entry_prof, 0, sizeof(*entry_prof));
	if (MEA_ACL5_Mask_Grouping_Profiles_TLB[Id_i].valid == MEA_TRUE)
		MEA_OS_memcpy(entry_prof,
			&(MEA_ACL5_Mask_Grouping_Profiles_TLB[Id_i]),
			sizeof(*entry_prof));

	/* Return to caller */
	return MEA_OK;

}



MEA_Status MEA_API_ACL5_Mask_Grouping_Profiles_Delete(MEA_Unit_t  unit_i, MEA_ACL5Id_t Id_i)
{
	MEA_Bool exist;
	MEA_ACL5_Mask_Grouping_Profiles entry;

	/* Check if support */
	if (!MEA_ACL5_SUPPORT) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - MEA_ACL5_SUPPORT not support \n",
			__FUNCTION__);
		return MEA_ERROR;
	}

	/*check if the id exist*/
	if (mea_drv_ACL5_Mask_Grouping_Profiles_IsExist(unit_i, Id_i, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_Mask_Grouping_Profiles_IsExist failed (id_i=%d\n",
			__FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - ACL5_Mask_Grouping id %d not exist\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	MEA_OS_memset(&entry, 0, sizeof(entry));
	MEA_OS_memcpy(&(MEA_ACL5_Mask_Grouping_Profiles_TLB[Id_i]),
		&entry,
		sizeof(MEA_ACL5_Mask_Grouping_Profiles_TLB[Id_i]));
	MEA_ACL5_Mask_Grouping_Profiles_TLB[Id_i].valid = MEA_FALSE;

	return MEA_OK;
}

/************************************************************************/
/*		MEA_API  profile                                                */
/************************************************************************/

/*----------------------------------------------------------------------*/
/*				ACL5 IP Mask  profile			                        */
/*----------------------------------------------------------------------*/

MEA_Status MEA_API_ACL5_IP_Mask_Prof_Create(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_io, MEA_ACL5_Ipmask_dbt* entry)
{
	MEA_Bool exist;

	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}


	/* Look for stream id */
	if ((*Id_io) != MEA_PLAT_GENERATE_NEW_ID) {
		/*check if the id exist*/
		if (mea_drv_ACL5_prof_IP_mask_IsExist(unit, *Id_io, MEA_TRUE, &exist) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_prof_IP_mask_IsExist failed (*id_io=%d\n",
				__FUNCTION__, *Id_io);
			return MEA_ERROR;
		}
		if (exist) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - *Id_io %d is already exist\n", __FUNCTION__, *Id_io);
			return MEA_ERROR;
		}

	}
	else {
		/*find new place*/
		if (mea_drv_ACL5_Prof_IP_mask_find_free(unit, Id_io) != MEA_TRUE) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_Prof_IP_mask_find_free failed\n", __FUNCTION__);
			return MEA_ERROR;
		}
	}

	if (mea_drv_ACL5_prof_IP_mask_UpdateHw(unit,
		*Id_io,
		entry,
		NULL) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_IP_mask_UpdateHw failed for id %d\n", __FUNCTION__, *Id_io);
		return MEA_ERROR;
	}

	mea_acl5_prof_IP_mask_info_TBL[*Id_io].valid = MEA_TRUE;
	MEA_OS_memcpy(&(mea_acl5_prof_IP_mask_info_TBL[*Id_io].data), entry, sizeof(mea_acl5_prof_IP_mask_info_TBL[*Id_io].data));
	


	return MEA_OK;



}

MEA_Status MEA_API_ACL5_IP_Mask_Prof_Set(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5_Ipmask_dbt* entry)
{
	
	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}

	if (Id_i > MEA_ACL5_IPMASK_MAX_PROF)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
			"%s - MEA_API_ACL5_IP_Mask_Prof_Set index out of range %d\n", __FUNCTION__, Id_i);
		return MEA_ERROR;

	}


	if (mea_drv_ACL5_prof_IP_mask_UpdateHw(unit,
                Id_i,
		entry,
		NULL) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
			"%s - MEA_API_ACL5_IP_Mask_Prof_Set cannot modify for id %d\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(&(mea_acl5_prof_IP_mask_info_TBL[Id_i].data), entry, sizeof(mea_acl5_prof_IP_mask_info_TBL[Id_i].data));

	return MEA_OK;
	
}

MEA_Status MEA_API_ACL5_IP_Mask_Prof_Delete(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i)
{
	MEA_Bool exist;
	MEA_ACL5_Ipmask_dbt entry;
	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}

	/*check if the id exist*/
	if (mea_drv_ACL5_prof_IP_mask_IsExist(unit, Id_i, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_IP_mask_prof_IsExist failed (id_i=%d\n",
			__FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - ACL5_prof_IP_mask %d not exist\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

	MEA_OS_memset(&entry, 0, sizeof(entry));

	if (mea_drv_ACL5_prof_IP_mask_UpdateHw(unit,
		Id_i,
		&entry,
		NULL) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_IP_mask_UpdateHw failed for id %d\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(&(mea_acl5_prof_IP_mask_info_TBL[Id_i].data),
		&entry,
		sizeof(mea_acl5_prof_IP_mask_info_TBL[Id_i].data));
	mea_acl5_prof_IP_mask_info_TBL[Id_i].valid = MEA_FALSE;

	return MEA_OK;
}

MEA_Status MEA_API_ACL5_IP_Mask_Prof_Get(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5_Ipmask_dbt* entry)
{

	MEA_Bool    exist;

	MEA_API_LOG


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}

	/*check if the id exist*/
	if (mea_drv_ACL5_prof_IP_mask_IsExist(unit, Id_i, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_IP_mask_IsExist failed (id_i=%d\n",
			__FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - id %d not exist\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (entry == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - entry is null\n", __FUNCTION__, Id_i);
		return MEA_ERROR;

	}
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

	/* Copy to caller structure */
	MEA_OS_memset(entry, 0, sizeof(*entry));
	if (mea_acl5_prof_IP_mask_info_TBL[Id_i].valid== MEA_TRUE)
		MEA_OS_memcpy(entry,
			&(mea_acl5_prof_IP_mask_info_TBL[Id_i].data),
			sizeof(*entry));

	/* Return to caller */
	return MEA_OK;
}

MEA_Status MEA_API_ACL5_IP_Mask_Prof_isExist(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_Bool silent, MEA_Bool *exist)
{
	return mea_drv_ACL5_prof_IP_mask_IsExist(unit, Id_i, silent, exist);
}

MEA_Status MEA_API_ACL5_IP_Mask_Prof_GetFirst(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5_Ipmask_dbt* entry, MEA_Bool *found_o)
{
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}

	if (Id_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	if (found_o) {
		*found_o = MEA_FALSE;
	}

	for ((*Id_o) = 0;
		(*Id_o) < MEA_ACL5_IPMASK_MAX_PROF;
		(*Id_o)++) {
		if (mea_acl5_prof_IP_mask_info_TBL[(*Id_o)].valid == MEA_TRUE) {
			if (found_o) {
				*found_o = MEA_TRUE;
			}

			if (entry) {
				MEA_OS_memcpy(entry,
					&mea_acl5_prof_IP_mask_info_TBL[(*Id_o)].data,
					sizeof(*entry));
			}
			break;
		}
	}


	return MEA_OK;

}

MEA_Status MEA_API_ACL5_IP_Mask_Prof_GetNext(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5_Ipmask_dbt* entry, MEA_Bool *found_o)
{
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}
	if (Id_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	if (found_o) {
		*found_o = MEA_FALSE;
	}

	for ((*Id_o)++;
		(*Id_o) < MEA_ACL5_IPMASK_MAX_PROF;
		(*Id_o)++) {
		if (mea_acl5_prof_IP_mask_info_TBL[(*Id_o)].valid == MEA_TRUE) {
			if (found_o) {
				*found_o = MEA_TRUE;
			}
			if (entry) {
				MEA_OS_memcpy(entry,
					&mea_acl5_prof_IP_mask_info_TBL[(*Id_o)].data,
					sizeof(*entry));
			}
			break;
		}
	}

	return MEA_OK;
}


/*----------------------------------------------------------------------*/
/*				ACL5 Range Mask  profile		                        */
/*----------------------------------------------------------------------*/


MEA_Status MEA_API_ACL5_Range_Mask_Prof_Create(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_io)
{
	MEA_Bool exist= MEA_FALSE;

	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}

	
	/* Look for stream id */
	if ((*Id_io) != MEA_PLAT_GENERATE_NEW_ID  ) {
		/*check if the id exist*/
		if (mea_drv_ACL5_prof_Range_mask_IsExist(unit, *Id_io, MEA_TRUE, &exist) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_prof_Range_mask_IsExist failed (id_io=%d\n",
				__FUNCTION__, *Id_io);
			return MEA_ERROR;
		}
		if (exist) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - Id_o %d is already exist\n", __FUNCTION__, *Id_io);
			return MEA_ERROR;
		}

	}
	else {
		/*find new place*/
		if (mea_drv_ACL5_Prof_Range_mask_find_free(unit, Id_io) != MEA_TRUE) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_Prof_Range_mask_find_free failed\n", __FUNCTION__);
			return MEA_ERROR;
		}
	}


	mea_acl5_prof_Range_mask_info_TBL[*Id_io].valid = MEA_TRUE;
	mea_acl5_prof_Range_mask_info_TBL[*Id_io].numofowners = 1;



	return MEA_OK;



}

MEA_Status MEA_API_ACL5_Range_Mask_Prof_Set(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5Index_t index , MEA_ACL5_Range_Mask_data_dbt* entry)
{
	MEA_Bool exist;
	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}




	if (mea_acl5_prof_Range_mask_info_TBL[Id_i].numofowners > 1)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
			"%s - MEA_API_ACL5_Range_Mask_Prof_Set cannot modify number of owners is %d \n", __FUNCTION__, 
            mea_acl5_prof_Range_mask_info_TBL[Id_i].numofowners);
		return MEA_ERROR;

	}

	if (mea_acl5_prof_Range_mask_info_TBL[Id_i].valid != MEA_TRUE)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
			"%s - MEA_API_ACL5_Range_Mask_Prof_Set id is not exist %d \n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

	if ((index) != MEA_PLAT_GENERATE_NEW_ID) {
		/*check if the id exist*/
		if (mea_drv_ACL5_prof_Range_mask_IsExistIndex(unit, Id_i, index, MEA_TRUE, &exist) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_prof_Range_mask_IsExistIndex failed (Id_i=%d\n",
				__FUNCTION__, Id_i);
			return MEA_ERROR;
		}
		if (exist) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - index %d is already exist\n", __FUNCTION__, index);
			return MEA_ERROR;
		}

	}
	


	if (mea_drv_ACL5_prof_Range_mask_UpdateHw(unit,
        Id_i,
		index,
		entry,
		NULL) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
			"%s - MEA_API_ACL5_Range_Mask_Prof_Set cannot modify for id %d\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

	
	MEA_OS_memcpy(&(mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].data), entry, sizeof(mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].data));
	mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].data.valid = MEA_TRUE;
		
	
	

	return MEA_OK;

}

MEA_Status MEA_API_ACL5_Range_Mask_Prof_Delete(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i)
{

	MEA_Bool exist;
	MEA_ACL5_Range_Mask_data_dbt entry;
	//MEA_Uint32 i;
	MEA_ACL5Index_t index;
	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}

	/*check if the id exist*/
	if (mea_drv_ACL5_prof_Range_mask_IsExist(unit, Id_i, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Range_mask_prof_IsExist failed (id_i=%d\n",
			__FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - ACL5_prof_Range_mask %d not exist\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

// 	if (mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].numofowners > 1)
// 	{
// 		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
// 			"%s - MEA_API_ACL5_Range_Mask_Prof_Delete cannot delete numofowners is %d \n", __FUNCTION__, mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].numofowners);
// 		return MEA_ERROR;
// 
// 	}

	//MEA_OS_memset(&entry, 0, sizeof(entry));
	entry.valid = MEA_FALSE;
	entry.L4src_min = 1;
	entry.L4src_max = 0;
	entry.L4dest_min = 1;
	entry.L4dest_max = 0;
	entry.dscp_min = 1;
	entry.dscp_max = 0;
	entry.priValue = 0;
	entry.Id = 0;
	for (index = 0; index < MEA_ACL5_RANGEMASK_MAX_BOLOCK; index++) {
		if (mea_drv_ACL5_prof_Range_mask_UpdateHw(unit,
            Id_i,
			index,
			&entry,
			NULL) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_prof_Range_mask_UpdateHw failed for id %d, index %d\n", __FUNCTION__, Id_i,index);
			return MEA_ERROR;
		}
		mea_acl5_prof_Range_mask_info_TBL[Id_i].valid = MEA_FALSE;
		MEA_OS_memcpy(&(mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].data),
			&entry,
			sizeof(mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].data));
		mea_acl5_prof_Range_mask_info_TBL[Id_i].numofowners = 0;
	}
	
	
	




	return MEA_OK;
}

MEA_Status MEA_API_ACL5_Range_Mask_Index_Delete(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5Index_t index)
{
	MEA_Bool exist;
	MEA_ACL5_Range_Mask_data_dbt entry;

	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}

	if (mea_drv_ACL5_prof_Range_mask_IsExistIndex(unit, Id_i,index, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Range_mask_IsExistIndex failed index %d does not exist\n",
			__FUNCTION__, index);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - ACL5_prof_index_mask index %d in profile %d does not exist\n", __FUNCTION__, index, Id_i);
		return MEA_ERROR;
	}

	if (mea_acl5_prof_Range_mask_info_TBL[Id_i].numofowners > 1)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
			"%s - MEA_API_ACL5_Range_Mask_Index_Delete cannot delete numofowners is %d \n", __FUNCTION__, mea_acl5_prof_Range_mask_info_TBL[Id_i].numofowners);
		return MEA_ERROR;

	}

	MEA_OS_memset(&entry, 0, sizeof(entry));

	if (mea_drv_ACL5_prof_Range_mask_UpdateHw(unit,
        Id_i,
		index,
		&entry,
		NULL) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_IP_mask_UpdateHw failed for index %d\n", __FUNCTION__, index);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(&(mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].data),
		&entry,
		sizeof(mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].data));
	mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].data.valid = MEA_FALSE;

	return MEA_OK;
}

MEA_Status MEA_API_ACL5_Range_Mask_Prof_Get(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5Index_t index, MEA_ACL5_Range_Mask_data_dbt* entry)
{

	MEA_Bool    exist;

	MEA_API_LOG


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}

	/*check if the id exist*/
	if (mea_drv_ACL5_prof_Range_mask_IsExist(unit, Id_i, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Range_mask_IsExist failed (id_i=%d\n",
			__FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - id %d not exist\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

	if (mea_drv_ACL5_prof_Range_mask_IsExistIndex(unit, Id_i,index, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Range_mask_IsExistIndex failed (id_i=%d, index=%d)\n",
			__FUNCTION__, Id_i,index);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - id %d with index %d not exist\n", __FUNCTION__, Id_i,index);
		return MEA_ERROR;
	}

	if (entry == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - entry is null\n", __FUNCTION__, Id_i);
		return MEA_ERROR;

	}
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

	/* Copy to caller structure */
	MEA_OS_memset(entry, 0, sizeof(*entry));
	MEA_OS_memcpy(entry,
		&(mea_acl5_prof_Range_mask_info_TBL[Id_i].inf[index].data),
		sizeof(*entry));

	/* Return to caller */
	return MEA_OK;
}

MEA_Status MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_Unit_t  unit, MEA_ACL5Id_t  Id_i, MEA_Bool silent, MEA_Bool *exist)
{
	return mea_drv_ACL5_prof_Range_mask_IsExist(unit, Id_i, silent, exist);
}

MEA_Status MEA_API_ACL5_Range_Mask_Prof_isExistIndex(MEA_Unit_t  unit, MEA_ACL5Id_t  Id_i, MEA_ACL5Index_t index, MEA_Bool silent, MEA_Bool *exist)
{
	return mea_drv_ACL5_prof_Range_mask_IsExistIndex(unit, Id_i,index, silent, exist);
}

MEA_Status MEA_API_ACL5_Range_Mask_Prof_GetFirst(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5Index_t *index, MEA_Bool *found_o, MEA_ACL5_Range_Mask_data_dbt* entry)
{
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}

	if (Id_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (index == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - index == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	if (found_o) {
		*found_o = MEA_FALSE;
	}

	for ((*Id_o) = 0; (*Id_o) < MEA_ACL5_IPMASK_MAX_PROF; (*Id_o)++) {
		for ((*index) = 0; (*index) < MEA_ACL5_IPMASK_MAX_PROF; (index)++) {
			if (mea_acl5_prof_Range_mask_info_TBL[*Id_o].valid == MEA_TRUE) {
				if (found_o) {
					*found_o = MEA_TRUE;
				}

				if (entry) {
					MEA_OS_memcpy(entry,
						&mea_acl5_prof_Range_mask_info_TBL[*Id_o].inf[*index].data,
						sizeof(*entry));
				}
				break;
			}
		}
	}

	return MEA_OK;
}

MEA_Status MEA_API_ACL5_Range_Mask_Prof_GetNext(MEA_Unit_t  unit, 
												MEA_ACL5Id_t *Id_o,
												MEA_ACL5Index_t *index,
                                                MEA_Bool *found_o, 
                                                MEA_ACL5_Range_Mask_data_dbt* entry)
{

	MEA_Bool exist;

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}
	if (Id_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (index == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - index == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}
    

	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	if (found_o) {
		*found_o = MEA_FALSE;
	}
	
	(*index)++;
	exist = MEA_FALSE;
	for ((*Id_o)= (*Id_o)+1; (*Id_o) < MEA_ACL5_RANGEMASK_MAX_PROF; (*Id_o)++) {
		if (exist == MEA_TRUE)
		{
			(*Id_o)--;
			break;
		}
		for ((*index); (*index) < MEA_ACL5_RANGEMASK_MAX_BOLOCK; (*index)++) {
			if (mea_acl5_prof_Range_mask_info_TBL[*Id_o].inf[*index].data.valid == MEA_TRUE) {
				if (found_o) {
					*found_o = MEA_TRUE;
				}
				if (entry) {
					MEA_OS_memcpy(entry,
						&mea_acl5_prof_Range_mask_info_TBL[*Id_o].inf[*index].data,
						sizeof(*entry));
					exist = MEA_TRUE;
				}
				break;
			}
		}
		if (exist != MEA_TRUE)
			*index = 0;
	
	}
	return MEA_OK;
}

MEA_Status MEA_API_ACL5_Range_Mask_Prof_GetmaxOf_Index(MEA_Unit_t  unit, MEA_ACL5Index_t *index)
{
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}
	

	if (index == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - index == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	

	MEA_ACL5Id_t Id_o;
	MEA_ACL5Index_t max_index = 0;

	for ((Id_o) = MEA_ACL5_IPMASK_MAX_PROF - 1; (Id_o) >= 0; (Id_o)--) {
		for ((*index) = MEA_ACL5_RANGEMASK_MAX_BOLOCK - 1; (*index) >= 0; (*index)--) {
			if (mea_acl5_prof_Range_mask_info_TBL[Id_o].valid == MEA_TRUE) {
				if (max_index < *index)
					*index = max_index;
				else
					break;
				
			}
		}
	}
	return MEA_OK;
}

/*----------------------------------------------------------------------*/
/*				ACL5 Key Mask  profile		                        */
/*----------------------------------------------------------------------*/


MEA_Status MEA_API_ACL5_Key_Mask_Prof_Create(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_io, MEA_ACL5_KeyMask_dbt* entry)
{
	MEA_Bool exist;

	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}


	/* Look for stream id */
	if ((*Id_io) != MEA_PLAT_GENERATE_NEW_ID) {
		/*check if the id exist*/
		if (mea_drv_ACL5_prof_Key_mask_IsExist(unit, *Id_io, MEA_TRUE, &exist) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_prof_Key_mask_IsExist failed (*id_io=%d\n",
				__FUNCTION__, *Id_io);
			return MEA_ERROR;
		}
		if (exist) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - *Id_io %d is already exist\n", __FUNCTION__, *Id_io);
			return MEA_ERROR;
		}

	}
	else {
		/*find new place*/
		if (mea_drv_ACL5_Prof_Key_mask_find_free(unit, Id_io) != MEA_TRUE) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_LAG_find_free failed\n", __FUNCTION__);
			return MEA_ERROR;
		}
	}

	if (mea_drv_ACL5_prof_Key_mask_UpdateHw(unit,
		*Id_io,
		entry,
		NULL) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Key_mask_UpdateHw failed for id %d\n", __FUNCTION__, *Id_io);
		return MEA_ERROR;
	}

	mea_acl5_prof_Key_mask_info_TBL[*Id_io].valid = MEA_TRUE;
	MEA_OS_memcpy(&(mea_acl5_prof_Key_mask_info_TBL[*Id_io].data), entry, sizeof(mea_acl5_prof_Key_mask_info_TBL[*Id_io-1].data));
	mea_acl5_prof_Key_mask_info_TBL[*Id_io].valid = MEA_TRUE;



	return MEA_OK;



}

MEA_Status MEA_API_ACL5_Key_Mask_Prof_Set(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5_KeyMask_dbt* entry)
{
	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}

	if (Id_i > MEA_ACL5_KEYMASK_MAX_PROF)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
			"%s - MEA_API_ACL5_Key_Mask_Prof_Set index out of range %d\n", __FUNCTION__, Id_i);
		return MEA_ERROR;

	}


	if (mea_drv_ACL5_prof_Key_mask_UpdateHw(unit,
		Id_i,
		entry,
		NULL) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
			"%s - MEA_API_ACL5_Key_Mask_Prof_Set cannot modify for id %d\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

	mea_acl5_prof_Key_mask_info_TBL[Id_i].valid = MEA_TRUE;
	MEA_OS_memcpy(&(mea_acl5_prof_Key_mask_info_TBL[Id_i].data), entry, sizeof(mea_acl5_prof_Key_mask_info_TBL[Id_i].data));




	return MEA_OK;

}

MEA_Status MEA_API_ACL5_Key_Mask_Prof_Delete(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i)
{
	MEA_Bool exist;
	MEA_ACL5_KeyMask_dbt entry;
	MEA_API_LOG

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_ACL5_SUPPORT not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}

	/*check if the id exist*/
	if (mea_drv_ACL5_prof_Key_mask_IsExist(unit, Id_i, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Key_mask_prof_IsExist failed (id_i=%d\n",
			__FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - ACL5_prof_Key_mask %d not exist\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

	MEA_OS_memset(&entry, 0, sizeof(entry));

	if (mea_drv_ACL5_prof_Key_mask_UpdateHw(unit,
        Id_i,
		&entry,
		NULL) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Key_mask_UpdateHw failed for id %d\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}

	MEA_OS_memcpy(&(mea_acl5_prof_Key_mask_info_TBL[Id_i].data),
		&entry,
		sizeof(mea_acl5_prof_Key_mask_info_TBL[Id_i].data));
	mea_acl5_prof_Key_mask_info_TBL[Id_i].valid = MEA_FALSE;

	return MEA_OK;
}

MEA_Status MEA_API_ACL5_Key_Mask_Prof_Get(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5_KeyMask_dbt* entry)
{
	MEA_Bool    exist;

	MEA_API_LOG


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}

	/*check if the id exist*/
	if (mea_drv_ACL5_prof_Key_mask_IsExist(unit, Id_i, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_prof_Key_mask_IsExist failed (id_i=%d\n",
			__FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - id %d not exist\n", __FUNCTION__, Id_i);
		return MEA_ERROR;
	}
	if (entry == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - entry is null\n", __FUNCTION__, Id_i);
		return MEA_ERROR;

	}
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

	/* Copy to caller structure */
	MEA_OS_memset(entry, 0, sizeof(*entry));
	MEA_OS_memcpy(entry,
		&(mea_acl5_prof_Key_mask_info_TBL[Id_i].data),
		sizeof(*entry));

	/* Return to caller */
	return MEA_OK;
}

MEA_Status MEA_API_ACL5_Key_Mask_Prof_isExist(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_Bool silent, MEA_Bool *exist)
{
	return mea_drv_ACL5_prof_Key_mask_IsExist(unit, Id_i, silent, exist);

}

MEA_Status MEA_API_ACL5_Key_Mask_Prof_GetFirst(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5_KeyMask_dbt* entry, MEA_Bool *found_o)
{
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}

	if (Id_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	if (found_o) {
		*found_o = MEA_FALSE;
	}

	for ((*Id_o) = 0;
		(*Id_o) < MEA_ACL5_KEYMASK_MAX_PROF;
		(*Id_o)++) {
		if (mea_acl5_prof_Key_mask_info_TBL[(*Id_o)].valid == MEA_TRUE) {
			if (found_o) {
				*found_o = MEA_TRUE;
			}

			if (entry) {
				MEA_OS_memcpy(entry,
					&mea_acl5_prof_Key_mask_info_TBL[(*Id_o)].data,
					sizeof(*entry));
			}
			break;
		}
	}


	return MEA_OK;
}

MEA_Status MEA_API_ACL5_Key_Mask_Prof_GetNext(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5_KeyMask_dbt* entry, MEA_Bool *found_o)
{
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
		/* Check if support */
		if (!MEA_ACL5_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
			return MEA_ERROR;
		}
	if (Id_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (found_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	if (found_o) {
		*found_o = MEA_FALSE;
	}

	for ((*Id_o)++;
		(*Id_o) < MEA_ACL5_KEYMASK_MAX_PROF;
		(*Id_o)++) {
		if (mea_acl5_prof_Key_mask_info_TBL[(*Id_o)].valid == MEA_TRUE) {
			if (found_o) {
				*found_o = MEA_TRUE;
			}
			if (entry) {
				MEA_OS_memcpy(entry,
					&mea_acl5_prof_Key_mask_info_TBL[(*Id_o)].data,
					sizeof(*entry));
			}
			break;
		}
	}

	return MEA_OK;
}


