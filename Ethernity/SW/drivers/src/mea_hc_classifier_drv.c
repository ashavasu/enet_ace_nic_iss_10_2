/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/






/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_if_regs.h"
#include "mea_hc_classifier_drv.h"


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_HC_CLASS_MAX_REG 17
#define MEA_HC_CLASS_DEFAULT_LRN 5

#define MEA_HC_CLASS_COMPRESS_DATA 61

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct{
    MEA_Uint32 data[MEA_HC_CLASS_MAX_REG];
}MEA_HC_classifierData_dbt;


typedef struct {
        union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 compress_enable :1;
        MEA_Uint32 counterlrn      :7;
        MEA_Uint32 InvalidateCount :8;
        MEA_Uint32 pad             :16;
#else
        MEA_Uint32 pad             :16;
        MEA_Uint32 InvalidateCount :8;
        MEA_Uint32 counterlrn      :7;
        MEA_Uint32 compress_enable :1;
#endif

    } val;
    MEA_Uint32 regs[1]; 
} classifieContexHw;

}MEA_HC_classifier_ContexHw_dbt;



typedef struct {
    MEA_Bool valid;
    MEA_HDC_control_dbt data;

}MEA_HC_classifier_Contex_entry_dbt;
/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

static MEA_HC_classifier_Contex_entry_dbt  *MEA_HC_classifier_Contexinfo_Table=NULL;
/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


static void mea_drv_HC_classifier_Contex_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_HC_classifier_ContexHw_dbt  *entry= (MEA_HC_classifier_ContexHw_dbt* )arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry->classifieContexHw.regs[0];


    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}


static MEA_Status mea_drv_HC_classifier_Contex_UpdateHw(MEA_Unit_t             unit_i, 
                                                        MEA_Uint16             id_i,
                                                        MEA_HDC_control_dbt      *new_entry_pi)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_ind_write_t      ind_write;
    MEA_HC_classifier_ContexHw_dbt ContexHw;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)


     ContexHw.classifieContexHw.val.compress_enable  =      new_entry_pi->type;
     if(new_entry_pi->LearnEnable){
        ContexHw.classifieContexHw.val.counterlrn = new_entry_pi->LearnCount;
         ContexHw.classifieContexHw.val.InvalidateCount = new_entry_pi->InvalidateCount;;
     }else{
       ContexHw.classifieContexHw.val.counterlrn = 0;
        ContexHw.classifieContexHw.val.InvalidateCount = 0;
     
     }


        /* Prepare ind_write structure */
        ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_HC_CLASS_CONTEX;
        ind_write.tableOffset   = id_i; 
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_HC_classifier_Contex_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
        //ind_write.funcParam3    = (MEA_funcParam);
        ind_write.funcParam4 = (MEA_funcParam)(&ContexHw);

            /* Write to the Indirect Table */
            if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                    __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
                return ENET_ERROR;
            }
       


        /* Return to caller */
        return MEA_OK;
}


MEA_Status mea_drv_HC_classifier_Init      (MEA_Unit_t     unit_i)
{
    MEA_Globals_Entry_dbt entry;
    MEA_Status  status;
    MEA_Uint32 size;
   MEA_Uint16  hcid;
    
    
    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG


    /* Check if support */
    if (!MEA_HC_CLASSIFIER_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF HC_classifier ...OFF\n");
        return MEA_OK;   
    }

      
   

    /* Init IF Service Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF HC_classifier ...\n");

    if (MEA_API_Get_Globals_Entry (unit_i,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Get_Globals failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    entry.Hc_classifier.val.init_flush = MEA_TRUE;
    if (MEA_API_Set_Globals_Entry (unit_i,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Set_Globals (1) failed  \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    status = MEA_API_ReadReg(unit_i, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    while ( (status & MEA_IF_STATUS_BUSY_MASK) )
    {
        MEA_OS_sleep(0, 1000 ); /* sleep for 1 microsecond */

        status = MEA_API_ReadReg(unit_i, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    }

    entry.Hc_classifier.val.init_flush = MEA_FALSE;
    if (MEA_API_Set_Globals_Entry (unit_i,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Set_Globals (1) failed  \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    /************************************************************************/
    /* init the context                                                     */
    /************************************************************************/
   
    size = MEA_HC_CLASSIFIER_MAX_OF_HCID * sizeof(MEA_HC_classifier_Contex_entry_dbt);
    if (size != 0) {
        MEA_HC_classifier_Contexinfo_Table = (MEA_HC_classifier_Contex_entry_dbt*)MEA_OS_malloc(size);
        if (MEA_HC_classifier_Contexinfo_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_HC_classifier_Contexinfo_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_HC_classifier_Contexinfo_Table[0]),0,size);
        /*write to hw the the default  value*/
       MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_REG_HDC_RELESE_HC,0x1,MEA_MODULE_IF);
#if 1 /*no need */
       
       for (hcid=0;hcid<MEA_HC_CLASSIFIER_MAX_OF_HCID;hcid++)
       {
           
           MEA_HC_classifier_Contexinfo_Table[hcid].valid=MEA_TRUE;
           MEA_HC_classifier_Contexinfo_Table[hcid].data.type = MEA_HDC_TYPE_COMPRESS_ENABLE;
           
           MEA_HC_classifier_Contexinfo_Table[hcid].data.LearnEnable= MEA_TRUE;
           MEA_HC_classifier_Contexinfo_Table[hcid].data.LearnCount= MEA_HC_CLASS_DEFAULT_LRN;
           mea_drv_HC_classifier_Contex_UpdateHw(unit_i,hcid,&MEA_HC_classifier_Contexinfo_Table[hcid].data );
       }
#endif       
       
       
    }




    
    return MEA_OK;
}
MEA_Status mea_drv_HC_classifier_RInit     (MEA_Unit_t     unit_i)
{
    MEA_Uint16 hcid;
    MEA_Globals_Entry_dbt entry;
    MEA_Status  status;

    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_HC_CLASSIFIER_SUPPORT){
            return MEA_OK;   
        }

        if (MEA_API_Get_Globals_Entry (unit_i,&entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Get_Globals failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        entry.Hc_classifier.val.init_flush = MEA_TRUE;
        if (MEA_API_Set_Globals_Entry (unit_i,&entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Set_Globals (1) failed  \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        status = MEA_API_ReadReg(unit_i, MEA_IF_STATUS_REG, MEA_MODULE_IF);
        while ( (status & MEA_IF_STATUS_BUSY_MASK) )
        {
            MEA_OS_sleep(0, 1000 ); /* sleep for 1 microsecond */

            status = MEA_API_ReadReg(unit_i, MEA_IF_STATUS_REG, MEA_MODULE_IF);
        }

        entry.Hc_classifier.val.init_flush = MEA_FALSE;
        if (MEA_API_Set_Globals_Entry (unit_i,&entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Set_Globals (1) failed  \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_REG_HDC_RELESE_HC,0x1,MEA_MODULE_IF);


        for (hcid=0;hcid<MEA_HC_CLASSIFIER_MAX_OF_HCID;hcid++)
        {
            if(MEA_HC_classifier_Contexinfo_Table[hcid].valid == MEA_TRUE){

                mea_drv_HC_classifier_Contex_UpdateHw(unit_i,hcid,&MEA_HC_classifier_Contexinfo_Table[hcid].data);
            }
        }

   return MEA_OK;
}
MEA_Status mea_drv_HC_classifier_Conclude  (MEA_Unit_t     unit_i)
{
    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

    /* Check if support */
    if (!MEA_HC_CLASSIFIER_SUPPORT){
        return MEA_OK;   
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude HC_classifier..\n");

    /* Free the table */
    if (MEA_HC_classifier_Contexinfo_Table != NULL) {
        MEA_OS_free(MEA_HC_classifier_Contexinfo_Table);
        MEA_HC_classifier_Contexinfo_Table = NULL;
    }
    
    
    return MEA_OK;
}

#if 0
static void mea_drv_HC_classifier_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry= (MEA_Uint32  )arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry;




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}
 

static MEA_Status mea_drv_HC_classifier_UpdateHw(MEA_Unit_t    unit_i, MEA_HC_classifierData_dbt    *entry_pi )
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32              value,index;
    MEA_ind_write_t      ind_write;


    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

      



        /* Prepare ind_write structure */
        ind_write.tableType     = ENET_IF_CMD_TBL_HC_CLASS;
       
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_HC_classifier_UpdateHwEntry;
        ind_write.funcParam1    = (MEA_Uint32)unit_i;
        //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
        //ind_write.funcParam3    = (MEA_Uint32);



        for (index=0;index<=MEA_HC_CLASS_MAX_REG;index++)
        {

            ind_write.tableOffset   = index ;
            value=entry_pi->data[index];
            ind_write.funcParam4    = (MEA_Uint32)(value);

            /* Write to the Indirect Table */
            if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                    __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
                return ENET_ERROR;
            }
        }


        /* Return to caller */
        return MEA_OK;
}
#endif



static MEA_Status mea_drv_HC_classifier_Read_Info(MEA_Unit_t    unit_i, MEA_HC_classifierData_dbt    *entry_po )
{

    
    MEA_Uint32     val[1];
    MEA_ind_read_t ind_read;
    MEA_Uint32 index;


    ind_read.tableType		= ENET_IF_CMD_TBL_HC_CLASS;
    
    ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
    ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_read.statusReg		= MEA_IF_STATUS_REG;   
    ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
    ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data      = &(val[0]);


    for (index=0;index<= (MEA_Uint32)(MEA_HC_CLASSIFIER_MAX_KEY_WIDTH/32); index++)
    {
        ind_read.tableOffset	= index;

        /* Write to the Indirect Table */
        if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(val),
            MEA_MODULE_IF,
            globalMemoryMode,
            MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
                return MEA_ERROR;
        }
    
        entry_po->data[index]=val[0];
    
    }







    return MEA_OK;

}
#if 0
static void     mea_drv_HC_classifier_hash_key_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry= (MEA_Uint32  )arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry;




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}
#endif

#if 0
static MEA_Status mea_drv_HC_classifier_hash_key_set(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit, MEA_db_HwId_t hwId,MEA_Bool silence)
{
    MEA_ind_write_t ind_write;
    MEA_Uint32 count,group;
    MEA_Uint32    valueToset=0;
    MEA_Status status;
    int j=0;
    //int i;

    /* build the ind_write value */
    ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_99_HASH;;/* not relevant - HC hash table */
    ind_write.tableOffset      = 0; /* not relevant - HC hash table */
    ind_write.cmdReg           = MEA_IF_CMD_REG;      
    ind_write.cmdMask          = MEA_IF_CMD_HC_MAKE_REG_MASK;      //Alex (group bit (9,8))
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask       = MEA_IF_STATUS_HC_BUSY_MASK;   
    ind_write.tableAddrReg     = MEA_IF_CMD_PARAM_REG_IGNORE; /* service hash table */
    ind_write.tableAddrMask    = 0;

    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_HC_classifier_hash_key_UpdateHwEntry;
    ind_write.funcParam1    = (MEA_Uint32)unit_i;
    ind_write.funcParam2    = (MEA_Uint32)hwUnit;
    ind_write.funcParam3    = (MEA_Uint32)hwId;
    ind_write.funcParam4    = (MEA_Uint32)valueToset;



    count=0;
    for(group=0;group<=MEA_HC_CLASSIFIER_MAX_HASH_GROUPS;group++){
        ind_write.cmdMask        = MEA_IF_CMD_HC_MAKE_REG_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(group) ;      //Alex (group bit (9,8)) 
       
        
        status=MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF);
        if (status == MEA_ERROR) {
            if(!silence){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                    __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
            }
            return MEA_ERROR;
        }
        if (status == MEA_COLLISION) {
            if(mea_hash_group_flag_show)   MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"HC hash group %d COLLISION count %d \n",group,count);

            j=0;
            while(j != 0){
               
                status=MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF);
                if (status == MEA_COLLISION){
                    if(mea_hash_group_flag_show)
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,"HC hash group %d COLLISION count %d \n",group,j);


                }
                if(status== MEA_OK){
                    if(mea_hash_group_flag_show)
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"HC hash group %d Write count %d \n",group,j);
                    return MEA_OK;
                    break;
                }

                j++;
            }
            count++;

        }
        if(status== MEA_OK){
            if(mea_hash_group_flag_show)
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"HC hash group %d Write succeed \n",group);

            return MEA_OK;
            break;
        }
    }
    if(count > (MEA_HC_CLASSIFIER_MAX_HASH_GROUPS)){
        if(!silence){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - We have Collision for All group HC_Hash  id=%d \n",
                __FUNCTION__);
        }
        return MEA_ERROR;
    }

    return MEA_OK;

}
#endif


#if 0
static MEA_Status mea_drv_HC_classifier_hashRead_All(int argc, char *argv[])
{


    
    MEA_ind_read_t ind_read;
   
    MEA_Uint32 read_data[3];
   
    
    MEA_Uint32 group;
    
   
    MEA_Uint32 count;

   
    MEA_db_HwUnit_t  hwUnit=0;
    mea_memory_mode_e       save_globalMemoryMode=0;

    count=0;
  

    
  MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);

     
    
    for(group=0;group<=MEA_HC_CLASSIFIER_MAX_HASH_GROUPS;group++){
 

    /* build the ind_write value */
    ind_read.tableType        = 99; /* not relevant - service hash table */
    ind_read.tableOffset      = 0; /* not relevant - service hash table */
    ind_read.cmdReg           = MEA_IF_CMD_REG;      
    ind_read.cmdMask          = MEA_IF_CMD_HC_RST_NEXT_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(group);      
    ind_read.statusReg        = MEA_IF_STATUS_REG;   
    ind_read.statusMask       = MEA_IF_STATUS_BUSY_MASK;   
    ind_read.tableAddrReg     = MEA_IF_CMD_PARAM_REG_IGNORE; /* service hash table */
    ind_read.tableAddrMask    = 0;
    ind_read.read_data    =   NULL;
     
    
    

    /* Read to the hash table as Indirect Table without the table type and entry # */
    if (MEA_API_ReadIndirect(MEA_UNIT_0,&ind_read,
                             0,
                             MEA_MODULE_IF)!=MEA_OK) {
                                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                     "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                                     __FUNCTION__,
                                     ind_read.tableType,
                                     ind_read.tableOffset,
                                     MEA_MODULE_IF);

        return MEA_ERROR;
    }

 

    while(MEA_TRUE) {

          ind_read.cmdMask        = MEA_IF_CMD_HC_GET_NEXT_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(group) ;      
        ind_read.read_data        =   read_data;

        /* Read from the hash table as Indirect Table without the table type and entry # */
          if (MEA_API_ReadIndirect(MEA_UNIT_0,&ind_read,
                                 MEA_NUM_OF_ELEMENTS(read_data),
                                 MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                         "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                                         __FUNCTION__,
                                         ind_read.tableType,
                                         ind_read.tableOffset,
                                         MEA_MODULE_IF);
              return MEA_ERROR;
        }

       

    

      
    }/*MEA_TRUE*/

   

        } //group


    if(count==0){
        
    }
    
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);






   return MEA_OK;

}
#endif


void mea_Hc_class_tbl_Search_write_cmd(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry;




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}


static MEA_Status mea_drv_HC_classifier_hash_Search(MEA_Unit_t   unit, MEA_Uint16 Hcid, int group, MEA_Bool *exist)
{



    MEA_ind_read_t ind_read;
    MEA_ind_write_t	            ind_write;
    MEA_Uint32 read_data[1];
    MEA_Status status;



    //MEA_Uint32 count;

    



    /************************************************************************/
    /* Write the Hcid to search                                             */
    /************************************************************************/

    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG;
    ind_write.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_HDC_CLASS_EVENT;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_Hc_class_tbl_Search_write_cmd;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
    //ind_write.funcParam3    = (MEA_Uint32)hwId;
    ind_write.funcParam4 = (MEA_funcParam)((long)Hcid);

    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect failed "
            "(module=%d , tableType=%d , tableOffset=%d)\n",
            __FUNCTION__,
            MEA_MODULE_IF,
            ind_write.tableType,
            ind_write.tableOffset);

        return MEA_ERROR;
    }

#if 1

    /* build the ind_write value */
    ind_read.tableType     = ENET_IF_CMD_PARAM_TBL_99_HASH; /* not relevant - service hash table */
    ind_read.tableOffset   = 0; /* not relevant - service hash table */
    ind_read.cmdReg        = MEA_IF_CMD_REG;
    ind_read.cmdMask       = MEA_IF_CMD_HC_RST_NEXT_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(group);
    ind_read.statusReg     = MEA_IF_STATUS_REG;
    ind_read.statusMask    = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg  = MEA_IF_CMD_PARAM_REG_IGNORE; /* service hash table */
    ind_read.tableAddrMask = 0;
    ind_read.read_data     = NULL;

    /* Read to the hash table as Indirect Table without the table type and entry # */
    if (MEA_API_ReadIndirect(MEA_UNIT_0, &ind_read,
        0,
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);

        return MEA_ERROR;
    }
#endif        

    ind_read.tableType     = ENET_IF_CMD_PARAM_TBL_99_HASH; /* not relevant - service hash table */
    ind_read.tableOffset   = 0; /* not relevant - service hash table */
    ind_read.cmdReg        = MEA_IF_CMD_REG;
    ind_read.statusReg     = MEA_IF_STATUS_REG;
    ind_read.statusMask    = MEA_IF_STATUS_HC_BUSY_MASK;
    ind_read.tableAddrReg  = MEA_IF_CMD_PARAM_REG_IGNORE; /* hash table */
    ind_read.tableAddrMask = 0;


    ind_read.cmdMask = MEA_IF_CMD_HC_GET_NEXT_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(group);
    ind_read.read_data = read_data;

    /* Read from the hash table as Indirect Table without the table type and entry # */
    if (MEA_API_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(read_data),
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);
        return MEA_ERROR;
    }




    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    while ((status & MEA_IF_STATUS_HC_BUSY_MASK))
    {
        MEA_OS_sleep(0, 1000); /* sleep for 1 microsecond */

        status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    }


    *exist = MEA_FALSE;
    if (MEA_CLASSIFIER_BLOCK_SUPPORT == MEA_FALSE){
        if ((read_data[0] & (MEA_HC_CLASSIFIER_MAX_OF_HCID - 1)) == (MEA_Uint32)Hcid){
            *exist = MEA_TRUE;
        }
    }
    else{
        if ((read_data[0] & ((MEA_HC_CLASSIFIER_MAX_OF_HCID*2)-1)) == (MEA_Uint32)Hcid){
            *exist = MEA_TRUE;
        }

    }


    return MEA_OK;

}



MEA_Status MEA_API_HC_Set_Compress_Config(MEA_Unit_t   unit,  MEA_HDC_t hcId,MEA_HDC_control_dbt    *entry)
{

    
    
    
    MEA_API_LOG

        /* Check if support */
        if (!MEA_HC_CLASSIFIER_SUPPORT){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"not support \n" );
            return MEA_ERROR;   
        }



        if(entry == NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"entry = NULL\n" );
            return MEA_ERROR;
        }

        if(hcId >=MEA_HC_CLASSIFIER_MAX_OF_HCID){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"hcid %d out of range\n", hcId);
            return MEA_ERROR;
        }
    
      switch(entry->type){
        case MEA_HDC_TYPE_COMPRESS_DISABLE :
        case MEA_HDC_TYPE_COMPRESS_ENABLE:
        break;
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"The type %d not support yet \n",entry->type);
            return MEA_ERROR;
            break;

      }


        if(mea_drv_HC_classifier_Contex_UpdateHw(unit,hcId,entry)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"mea_drv_HC_classifier_Contex_UpdateHw %d \n",hcId);
            return MEA_ERROR;
        }

        MEA_HC_classifier_Contexinfo_Table[hcId].valid=MEA_TRUE;

        MEA_OS_memcpy(&MEA_HC_classifier_Contexinfo_Table[hcId].data,
            entry,
            sizeof(MEA_HC_classifier_Contexinfo_Table[0].data));
    
    
    return MEA_OK;
}




MEA_Status MEA_API_HC_Get_Compress_Config_Info(MEA_Unit_t                unit,
    MEA_HDC_t                  hcId,
    MEA_hdc_Get_info_dbt      *entry)
{


    MEA_Bool exist;
    MEA_HC_classifierData_dbt    Hc_Key;
    MEA_Uint16 i,j;

    if (!MEA_HC_CLASSIFIER_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"not support \n" );
        return MEA_ERROR;   
    }

    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"entry = NULL\n" );
        return MEA_ERROR;
    }

    if(hcId >=MEA_HC_CLASSIFIER_MAX_OF_HCID){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"hcid %d out of range\n", hcId);
        return MEA_ERROR;
    }

    switch(entry->type){
    case MEA_HDC_GET_CONTROL:
    case MEA_HDC_GET_INFO:
        break;

    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Not support this type =%d this Hw\n", entry->type);
        return MEA_ERROR;
        break;

    }
    
    if(MEA_HC_classifier_Contexinfo_Table[hcId].valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"This hcid %d is not setting   \n", hcId);
        return MEA_ERROR;
    }

    if(MEA_HC_classifier_Contexinfo_Table[hcId].valid == MEA_TRUE && entry->type == MEA_HDC_GET_CONTROL){
       
        MEA_OS_memcpy(&entry->status ,&MEA_HC_classifier_Contexinfo_Table[hcId].data,sizeof(entry->status));
    }

    if(MEA_HC_classifier_Contexinfo_Table[hcId].valid == MEA_TRUE && entry->type == MEA_HDC_GET_INFO){

        if(entry->header_data.header == NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"entry.header_data->header = NULL   \n", hcId);
            return MEA_ERROR;

        }
        exist=MEA_FALSE;
        mea_drv_HC_classifier_hash_Search(unit,hcId, 0, &exist);
        entry->header_valid = exist;
        if(exist == MEA_TRUE){
            
            MEA_OS_memset( &Hc_Key,0,sizeof(Hc_Key));
            mea_drv_HC_classifier_Read_Info(unit, &Hc_Key );
            j=0;
            for(i=0;i<= (MEA_HC_CLASSIFIER_MAX_KEY_WIDTH/32)*4; i++,j++ ){
                if(j == (MEA_HC_CLASS_MAX_REG-1) ){
                
                entry->header_data.L3Type = (MEA_Uint8)(Hc_Key.data[j] & 0xff);
                entry->header_data.L2Type = (MEA_Uint8)((Hc_Key.data[j]>>8) & 0xff);
                entry->header_data.vp     = (MEA_Uint8)((Hc_Key.data[j]>>16) & 0xff);

                entry->header_data.header[i]=(MEA_Uint8)(Hc_Key.data[j]>>24);
                
                }else{

                    entry->header_data.header[i++]   = (MEA_Uint8)((Hc_Key.data[j]>>24) & 0xff);
                    entry->header_data.header[i++] = (MEA_Uint8)((Hc_Key.data[j]>>16) & 0xff);
                    entry->header_data.header[i++] = (MEA_Uint8)((Hc_Key.data[j]>>8)  & 0xff);
                    entry->header_data.header[i] = (MEA_Uint8)((Hc_Key.data[j]>>0)  & 0xff);
                    
                }
            }

 
        }else{

        }
        
        
        
        
        
    }//info



return MEA_OK;

}

