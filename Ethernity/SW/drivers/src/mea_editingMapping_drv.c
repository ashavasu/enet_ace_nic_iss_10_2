/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_editingMapping_drv.h"
#include "mea_if_regs.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct {
    MEA_Uint32 valid         :  1;
    MEA_Uint32 num_of_owners : 10;
    MEA_Uint32 pad           : 21;
    MEA_EditingMappingProfile_Entry_dbt public_entry;  
} mea_drv_EditingMappingProfile_Entry_dbt,*mea_drv_EditingMappingProfile_Entry_dbp;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static mea_drv_EditingMappingProfile_Entry_dbt *MEA_EditingMappingProfile_Table    = NULL;
static MEA_Bool                             MEA_EditingMappingProfile_InitDone = MEA_FALSE;
static MEA_db_dbt                           MEA_EditingMappingProfile_db       = NULL;
static MEA_db_dbt                           MEA_EditingMapping_db              = NULL;


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_EditingMappingProfile_db>                                 */
/* Note: This is software entity only to allocate profiles                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EditingMappingProfile_num_of_sw_size_func,
                                         MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES,
                                         0)
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EditingMappingProfile_num_of_hw_size_func,
                                         MEA_BM_TBL_TYP_EDITING_MAPPING_NUM_OF_PROFILES(hwUnit_i),
                                         1)
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EditingMappingProfile_db,
                             MEA_EditingMappingProfile_db) 
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_EditingMapping_db>                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EditingMapping_num_of_sw_size_func,
                                         (MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES*
                                          MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE),
                                         0)
MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_EditingMapping_num_of_hw_size_func,
                                         MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH(hwUnit_i),
                                         MEA_BM_TBL_TYP_EDITING_MAPPING_NUM_OF_REGS(hwUnit_i))
MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_EditingMapping_db,
                             MEA_EditingMapping_db) 


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_EditingMapping_UpdateHwEntry>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_EditingMapping_UpdateHwEntry(MEA_funcParam arg1,
MEA_funcParam arg2,
MEA_funcParam arg3,
MEA_funcParam arg4)
{
    MEA_Unit_t       unit_i     = (MEA_Unit_t     )arg1;
    MEA_db_HwUnit_t  hwUnit_i   = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t    hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_EditingMappingProfile_ColorItem_Entry_dbt* entry_pi 
                                = (MEA_EditingMappingProfile_ColorItem_Entry_dbt*)arg4;
    MEA_Uint32                 val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32                 i;
    MEA_Uint16                 num_of_regs;
    MEA_Uint32                 count_shift;

    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
                             MEA_BM_TBL_TYP_EDITING_MAPPING_NUM_OF_REGS(hwUnit_i));
    for (i=0;i<num_of_regs;i++) {
        val[i] = 0;
    }


    count_shift=0;

    MEA_OS_insert_value(count_shift,
                        MEA_BM_TBL_TYP_EDITING_MAPPING_STAMP_COLOR_WIDTH,
                        (entry_pi->stamp_color_value == 0)? 1 : 0 ,
                        &val[0]);
    count_shift+=MEA_BM_TBL_TYP_EDITING_MAPPING_STAMP_COLOR_WIDTH;
 
    MEA_OS_insert_value(count_shift,
                        MEA_BM_TBL_TYP_EDITING_MAPPING_STAMP_PRIORITY_WIDTH,
                        entry_pi->stamp_priority_value,
                        &val[0]);
    count_shift+=MEA_BM_TBL_TYP_EDITING_MAPPING_STAMP_PRIORITY_WIDTH;


    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,
                         MEA_BM_IA_WRDATA(i),
                         val[i],
                         MEA_MODULE_BM);  
    }

    if (mea_db_Set_Hw_Regs(unit_i,
                           MEA_EditingMapping_db,
                           hwUnit_i,
                           hwId_i,
                           num_of_regs,
                           &(val[0])) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                          __FUNCTION__,
                          hwUnit_i,
                          hwId_i);
    }


    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_mea_drv_EditingMappingProfile_UpdateHw>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_EditingMappingProfile_UpdateHw
                        (MEA_Unit_t unit_i,
                         MEA_EditingMappingProfile_Id_t id_i,
                         MEA_EditingMappingProfile_Entry_dbt* new_entry_pi,
                         MEA_EditingMappingProfile_Entry_dbt* old_entry_pi)
{



    MEA_ind_write_t      ind_write;
    MEA_db_HwUnit_t      hwUnit;
    MEA_db_HwId_t        profile_hwId;
    MEA_Uint32           priority,color;
    MEA_Uint32 index;


    /* Check if support */
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check new_entry_pi parameter */
    if (new_entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - new_entry_pi == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_i */
    if (id_i >= MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES);
        return MEA_ERROR;
    }


    /* Get the hwUnit and hwId */
    MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
                                  mea_drv_Get_EditingMappingProfile_db,
                                  id_i,
                                  hwUnit,
                                  profile_hwId,
                                  return MEA_ERROR;)

    /* Update the HW */
    ind_write.tableType     = MEA_BM_TBL_TYP_EDITING_MAPPING;
    ind_write.tableOffset   = (profile_hwId*MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE);
    ind_write.cmdReg        = MEA_BM_IA_CMD;      
    ind_write.cmdMask       = MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg     = MEA_BM_IA_STAT;   
    ind_write.statusMask    = MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg  = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_EditingMapping_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)ind_write.tableOffset);

    index = ind_write.tableOffset;
    /* Loop on all items in this profile */
    for (priority=0;
         priority<MEA_NUM_OF_ELEMENTS(new_entry_pi->item_table);
         priority++) {
        for (color=0;
             color<MEA_NUM_OF_ELEMENTS(new_entry_pi->item_table[0].colorItem_table);
             color++,ind_write.tableOffset++,index++ /*ind_write.funcParam3++*/) {
            ind_write.funcParam3 = (MEA_funcParam)((long)index);
            /* Check for no change */
            if ((MEA_EditingMappingProfile_InitDone) &&
                (old_entry_pi != NULL) &&
                (MEA_OS_memcmp(&(new_entry_pi->item_table[priority].colorItem_table[color]),
                               &(old_entry_pi->item_table[priority].colorItem_table[color]),
                               sizeof(new_entry_pi->item_table[priority].colorItem_table[color])) == 0)) { 
                continue;
            }

            ind_write.funcParam4 = (MEA_funcParam)&(new_entry_pi->item_table[priority].colorItem_table[color]);

            /* Write to the Indirect Table */
            if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                                  __FUNCTION__,
                                  ind_write.tableType,
                                  ind_write.tableOffset,
                                  MEA_MODULE_BM);
                return ENET_ERROR;
            }
        }

    }

    /* Return to Caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Init_EditingMappingProfile_Table>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_EditingMappingProfile_Table(MEA_Unit_t unit_i)
{
    MEA_Uint32                     size;

    if (MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Already Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (MEA_EDITING_MAPPING_PROFILE_SUPPORT) {

        /* Init db - software allocation for profile hwId  */
        if (mea_db_Init(unit_i,
                        "EditingMappingProfile ",
                        mea_drv_Get_EditingMappingProfile_num_of_sw_size_func,
                        mea_drv_Get_EditingMappingProfile_num_of_hw_size_func,
                        &MEA_EditingMappingProfile_db) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Init (EditingMappingProfile) failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        /* Init db - Hardware shadow */
        if (mea_db_Init(unit_i,
                        "EditingMapping ",
                        mea_drv_Get_EditingMapping_num_of_sw_size_func,
                        mea_drv_Get_EditingMapping_num_of_hw_size_func,
                        &MEA_EditingMapping_db) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Init (EditingMapping) failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        /* Allocate the MEA_EditingMappingProfile_Table */
        size = MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES * 
               sizeof(mea_drv_EditingMappingProfile_Entry_dbt);
        MEA_EditingMappingProfile_Table = 
            (mea_drv_EditingMappingProfile_Entry_dbt*)MEA_OS_malloc (size);
        if (MEA_EditingMappingProfile_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_EditingMappingProfile_Table == NULL\n",
                              __FUNCTION__);
            return MEA_ERROR;
        }
        MEA_OS_memset(MEA_EditingMappingProfile_Table,0,size);

    }


    /* Set Init Done */
    MEA_EditingMappingProfile_InitDone = MEA_TRUE;     

    /* Return to Caller */
    return MEA_OK; 
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_EditingMappingProfile_Table>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_EditingMappingProfile_Table(MEA_Unit_t unit_i)
{

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    MEA_EditingMappingProfile_InitDone = MEA_FALSE;

    if (MEA_EDITING_MAPPING_PROFILE_SUPPORT) {

        if (mea_db_ReInit(unit_i,
                          MEA_EditingMapping_db,
                          MEA_BM_TBL_TYP_EDITING_MAPPING,
                          MEA_MODULE_BM,
                          NULL,
                          MEA_FALSE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_ReInit for EditingMapping failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

    }

    MEA_EditingMappingProfile_InitDone = MEA_TRUE;

    /* Return to Caller */
    return MEA_OK; 
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Conclude_EditingMappingProfile_Table>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Conclude_EditingMappingProfile_Table(MEA_Unit_t unit_i)
{

    MEA_EditingMappingProfile_Id_t id;

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (MEA_EDITING_MAPPING_PROFILE_SUPPORT) {

        if (MEA_EditingMappingProfile_Table) {
            for (id=0;
                 id<(MEA_Uint32)MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES;
                 id++) {
                if (!MEA_EditingMappingProfile_Table[id].valid) {
                    continue;
                }
                if (MEA_API_Delete_EditingMappingProfile_Entry(unit_i,id) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - MEA_API_Delete_EditingMappingProfile_Entry failed (id=%d)\n",
                                      __FUNCTION__,id);
                    return MEA_ERROR;
                }
                if (MEA_EditingMappingProfile_Table[id].valid) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - EditingMappingProfile %d is already exist \n",
                                      __FUNCTION__,id);
                    return MEA_ERROR;
                }
            }

            MEA_OS_free(MEA_EditingMappingProfile_Table);
            MEA_EditingMappingProfile_Table = NULL;
        }
        
        if (mea_db_Conclude(unit_i,&MEA_EditingMappingProfile_db) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_dn_Conclude (EditingMappingProfile) failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if (mea_db_Conclude(unit_i,&MEA_EditingMapping_db) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_dn_Conclude (EditingMapping) failed \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

    }

    /* Reset Init Done */
    MEA_EditingMappingProfile_InitDone = MEA_FALSE;     

    /* Return to Caller */
    return MEA_OK; 
}


/*----------------------------------------------------------------------------*/
/*             API functions implementation                                   */
/*----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Create_EditingMappingProfile_Entry>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Entry_dbt *entry_pi,
                                     MEA_EditingMappingProfile_Id_t      *id_io)
{

    MEA_EditingMappingProfile_Entry_dbt *entry;
    MEA_EditingMappingProfile_Id_t       id;
    MEA_db_SwId_t                        profile_swId;
    MEA_db_HwId_t                        profile_hwId;
    MEA_db_Hw_Entry_dbt                  profile_hwEntry;
    MEA_db_SwId_t                        swId;
    MEA_db_HwId_t                        hwId;
    MEA_db_HwUnit_t                      hwUnit;
    MEA_Uint32                           i,j;
    mea_memory_mode_e                    save_globalMemoryMode;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check entry_pi parameter*/
    if (entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_pi == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_io parameter*/
    if (id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_io == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_io parameter value */
    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID) {
        if ((*id_io) >= MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - id_i (%d) >= max value (%d) \n",
                              __FUNCTION__,
                              *id_io,
                              MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES);
            return MEA_ERROR;
        }
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID) {
        if (MEA_EditingMappingProfile_Table[(*id_io)].valid) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - entry %d already exist \n",
                              __FUNCTION__,(*id_io));
            return MEA_ERROR;
        }
        id = (*id_io);
    } else {
        for (id=0;
             id<(MEA_Uint32)MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES;
             id++) {
             if (!MEA_EditingMappingProfile_Table[id].valid) {
                 break;
             }
        }
        if (id>=(MEA_Uint32)MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - No free entries \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

    }


    /* Decide in which hwUnit we are working */
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        hwUnit = MEA_HW_UNIT_ID_GENERAL;
    } else {
        switch(entry_pi->direction) {
        case MEA_DIRECTION_UPSTREAM:
            hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_DIRECTION_DOWNSTREAM:
            hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        case MEA_DIRECTION_GENERAL:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown direction %d for this platform \n",
                              __FUNCTION__,entry_pi->direction);
            return MEA_ERROR;
        }
    }

    /* Change globalMemoryMode according to hwUnit */
    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);

    /* Allocate profile hwId */
    profile_swId = (MEA_db_SwId_t)id;
    if (mea_db_Get_Hw(unit_i,
                      MEA_EditingMappingProfile_db,
                      hwUnit,
                      (MEA_db_HwId_t)0,
                      &profile_hwEntry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Get_Hw (MEA_EditingMappingProfile) failed (hwUnit=%d,hwId=0)\n"
                          ,__FUNCTION__ ,hwUnit);

        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    if (profile_hwEntry.valid) {
        profile_hwId = MEA_DB_GENERATE_NEW_ID;
    } else {
        profile_hwId = 0;
    }
    if (mea_db_Create(unit_i,
                      MEA_EditingMappingProfile_db,
                      profile_swId,
                      hwUnit,
                      &profile_hwId) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Create failed (EditingMappingProfile) (swId=%d,hwUnit=%d) \n",
                          __FUNCTION__,profile_swId,hwUnit);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    for (i=0,
         hwId=profile_hwId*MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE,
         swId=profile_swId*MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE;
         i<MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE;
         i++,hwId++,swId++) {
        if (mea_db_Create(unit_i,
                          MEA_EditingMapping_db,
                          swId,
                          hwUnit,
                          &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Create failed (EditingMapping) (swId=%d,hwUnit=%d) \n",
                              __FUNCTION__,swId,hwUnit);
            for (j=0,swId=profile_swId*MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE;
                 j<i;
                 j++,swId++) {
                mea_db_Delete(unit_i,MEA_EditingMapping_db,swId,hwUnit);
            }
            mea_db_Delete(unit_i,MEA_EditingMappingProfile_db,profile_swId,hwUnit);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    }



    /* Allocate the entry */
    entry  = MEA_OS_malloc(sizeof(MEA_EditingMappingProfile_Entry_dbt));
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate entry failed \n",
                          __FUNCTION__);
        for (i=0,swId=profile_swId*MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE;
             i<MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE;
             i++,swId++) {
            mea_db_Delete(unit_i,MEA_EditingMapping_db,swId,hwUnit);
        }
        mea_db_Delete(unit_i,MEA_EditingMappingProfile_db,profile_swId,hwUnit);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* Copy input to entry */
    MEA_OS_memcpy(entry,entry_pi,sizeof(*entry));

    /* Update Hardware */
    if (mea_drv_EditingMappingProfile_UpdateHw(unit_i,
                                               id,
                                               entry,
                                               NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_EditingMappingProfile_UpdateHw failed (id=%d) \n",
                          __FUNCTION__,
                          id);
        MEA_OS_free(entry);
        for (i=0,swId=profile_swId*MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE;
             i<MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE;
             i++,swId++) {
            mea_db_Delete(unit_i,MEA_EditingMapping_db,swId,hwUnit);
        }
        mea_db_Delete(unit_i,MEA_EditingMappingProfile_db,profile_swId,hwUnit);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    /* Add to the table */
    MEA_OS_memset(&(MEA_EditingMappingProfile_Table[id]),0,sizeof(MEA_EditingMappingProfile_Table[0]));
    MEA_EditingMappingProfile_Table[id].valid = MEA_TRUE;
    MEA_EditingMappingProfile_Table[id].num_of_owners = 1;
    MEA_OS_memcpy(&(MEA_EditingMappingProfile_Table[id].public_entry),
                  entry,
                  sizeof(MEA_EditingMappingProfile_Table[0].public_entry));

    /* Update output parameter */
    (*id_io) = id;

    /* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Delete_EditingMappingProfile_Entry>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t       id_i)
{
    MEA_EditingMappingProfile_Entry_dbt entry;
    MEA_db_SwId_t                        profile_swId;
    MEA_db_SwId_t                        swId;
    MEA_db_HwUnit_t                      hwUnit;
    MEA_Uint32                           i;
    mea_memory_mode_e                    save_globalMemoryMode;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_i paramter value */
    if (id_i >= MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES);
        return MEA_ERROR;
    }


    /* Check if exist */
    if (!MEA_EditingMappingProfile_Table[id_i].valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }

    if (MEA_EditingMappingProfile_Table[id_i].num_of_owners > 1) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d has num_owners %d > 1 \n",
                          __FUNCTION__,id_i,
                          MEA_EditingMappingProfile_Table[id_i].num_of_owners);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Decide in which hwUnit we are working */
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        hwUnit = MEA_HW_UNIT_ID_GENERAL;
    } else {
        switch(MEA_EditingMappingProfile_Table[id_i].public_entry.direction) {
        case MEA_DIRECTION_UPSTREAM:
            hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_DIRECTION_DOWNSTREAM:
            hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        case MEA_DIRECTION_GENERAL:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown direction %d for this platform \n",
                              __FUNCTION__,MEA_EditingMappingProfile_Table[id_i].public_entry.direction);
            return MEA_ERROR;
        }
    }

    /* Change globalMemoryMode according to hwUnit */
    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);

    /* zero entry */
    MEA_OS_memset(&entry,0,sizeof(entry));

    /* Update Hardware */
    if (mea_drv_EditingMappingProfile_UpdateHw(unit_i,
                                               id_i,
                                               &entry,
                                               &(MEA_EditingMappingProfile_Table[id_i].public_entry)) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_EditingMappingProfile_UpdateHw failed (id=%d) \n",
                          __FUNCTION__,
                          id_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    /* Delete from DB */
    profile_swId = id_i;
    for (i=0,swId=profile_swId*MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE;
         i<MEA_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE;
         i++,swId++) {
         if (mea_db_Delete(unit_i,MEA_EditingMapping_db,swId,hwUnit) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - mea_db_Delete (MEA_EditingMapping_db) failed (swId=%d,hwUnit=%d)\n",
                               __FUNCTION__,
                               swId,
                               hwUnit);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR;
         }
    }
    if (mea_db_Delete(unit_i,MEA_EditingMappingProfile_db,profile_swId,hwUnit) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Delete (MEA_EditingMappingProfile_db) failed (swId=%d,hwUnit=%d)\n",
                          __FUNCTION__,
                          profile_swId,
                          hwUnit);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    
    /* Delete from table */
    MEA_OS_memset(&(MEA_EditingMappingProfile_Table[id_i]),0,sizeof(MEA_EditingMappingProfile_Table[0]));
    MEA_EditingMappingProfile_Table[id_i].valid = MEA_FALSE;


    /* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Set_EditingMappingProfile_Entry>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t       id_i,
                                     MEA_EditingMappingProfile_Entry_dbt *entry_pi)
{

    MEA_db_HwUnit_t                      hwUnit;
    mea_memory_mode_e                    save_globalMemoryMode;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check entry_pi parameter*/
    if (entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_pi == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_i parameter value */
    if (id_i >= MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES);
        return MEA_ERROR;
    }

    /* Check if exist */
    if (!MEA_EditingMappingProfile_Table[id_i].valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }

    if (entry_pi->direction != MEA_EditingMappingProfile_Table[id_i].public_entry.direction) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_pi->direction %d != current direction %d \n",
                          __FUNCTION__,
                          entry_pi->direction,
                          MEA_EditingMappingProfile_Table[id_i].public_entry.direction);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Decide in which hwUnit we are working */
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        hwUnit = MEA_HW_UNIT_ID_GENERAL;
    } else {
        switch(MEA_EditingMappingProfile_Table[id_i].public_entry.direction) {
        case MEA_DIRECTION_UPSTREAM:
            hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
            break;
        case MEA_DIRECTION_DOWNSTREAM:
            hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
            break;
        case MEA_DIRECTION_GENERAL:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown direction %d for this platform \n",
                              __FUNCTION__,entry_pi->direction);
            return MEA_ERROR;
        }
    }

    /* Change globalMemoryMode according to hwUnit */
    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);


    /* Update Hardware */
    if (mea_drv_EditingMappingProfile_UpdateHw(unit_i,
                                               id_i,
                                               entry_pi,
                                               &(MEA_EditingMappingProfile_Table[id_i].public_entry)) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_EditingMappingProfile_UpdateHw failed (id=%d) \n",
                          __FUNCTION__,
                          id_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    /* Update shadow */
    MEA_OS_memcpy(&(MEA_EditingMappingProfile_Table[id_i].public_entry),
                  entry_pi,
                  sizeof(MEA_EditingMappingProfile_Table[0].public_entry));

    /* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Get_EditingMappingProfile_Entry>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t       id_i,
                                     MEA_EditingMappingProfile_Entry_dbt *entry_po)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_i parameter value */
    if (id_i >= MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES);
        return MEA_ERROR;
    }


    /* Check if exist */
    if (!MEA_EditingMappingProfile_Table[id_i].valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Update output variable */
    if (entry_po != NULL) {
        MEA_OS_memcpy(entry_po,
                      &(MEA_EditingMappingProfile_Table[id_i].public_entry),
                      sizeof(*entry_po));
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_GetFirst_EditingMappingProfile_Entry>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t      *id_o,
                                     MEA_EditingMappingProfile_Entry_dbt *entry_po,
                                     MEA_Bool                            *found_o)
{


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }


    if (id_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_o == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    for ((*id_o)=0;(*id_o)<MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES;(*id_o)++) {
        if (!MEA_EditingMappingProfile_Table[(*id_o)].valid) {
            continue;
        }
        if (found_o) {
            *found_o = MEA_TRUE;
        }
        if (entry_po != NULL) {
            MEA_OS_memcpy(entry_po,
                          &(MEA_EditingMappingProfile_Table[(*id_o)].public_entry),
                          sizeof(*entry_po));
        }
        return MEA_OK;
    }

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_GetNext_EditingMappingProfile_Entry>                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t      *id_io,
                                     MEA_EditingMappingProfile_Entry_dbt *entry_po,
                                     MEA_Bool                            *found_o)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }


    if (id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_io == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    for ((*id_io)++;(*id_io)<MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES;(*id_io)++) {
        if (!MEA_EditingMappingProfile_Table[(*id_io)].valid) {
            continue;
        }
        if (found_o) {
            *found_o = MEA_TRUE;
        }
        if (entry_po != NULL) {
            MEA_OS_memcpy(entry_po,
                          &(MEA_EditingMappingProfile_Table[(*id_io)].public_entry),
                          sizeof(*entry_po));
        }
        return MEA_OK;
    }

    if (found_o) {
        *found_o = MEA_FALSE;
    }
   
    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_AddOwner_EditingMappingProfile_Entry>                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_AddOwner_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t       id_i)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check valid id_i parameter */
    if (id_i >= MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES);
        return MEA_ERROR;
    }


    /* Check if exist */
    if (!MEA_EditingMappingProfile_Table[id_i].valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Increment the num_of_owner */
    MEA_EditingMappingProfile_Table[id_i].num_of_owners++;

    /* Return to caller */
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_DelOwner_EditingMappingProfile_Entry>                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_DelOwner_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t       id_i)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check valid id_i parameter */
    if (id_i >= MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES);
        return MEA_ERROR;
    }


    /* Check if exist */
    if (!MEA_EditingMappingProfile_Table[id_i].valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }

    /* Verify we have extra owner on this entity */
    if (MEA_EditingMappingProfile_Table[id_i].num_of_owners == 1) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d has num_of_owners 1 (Use delete API) \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* Increment the num_of_owner */
    MEA_EditingMappingProfile_Table[id_i].num_of_owners--;

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_GetOwner_EditingMappingProfile_Entry>                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetOwner_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t       id_i,
                                     MEA_Uint32                          *num_of_owners_o)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_EditingMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_EDITING_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check valid id_i parameter */
    if (id_i >= MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_MAX_NUM_OF_EDITING_MAPPING_PROFILES);
        return MEA_ERROR;
    }


    /* Check if exist */
    if (!MEA_EditingMappingProfile_Table[id_i].valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* Update output variable  */
    if (num_of_owners_o) {
        *num_of_owners_o = MEA_EditingMappingProfile_Table[id_i].num_of_owners;
    }

    /* Return to caller */
    return MEA_OK;
}



// 

MEA_Status MEA_Delete_all_editing_mapping(MEA_Unit_t                           unit_i)
{
    MEA_EditingMappingProfile_Id_t       id;
    MEA_Bool found;
    
    if (MEA_API_GetFirst_EditingMappingProfile_Entry(unit_i,
        &id,
        NULL,
        &found) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_GetFirst_EditingMappingProfile_Entry \n",
                __FUNCTION__,id);
            return MEA_ERROR;
    }
    while (found) {
        if (MEA_API_Delete_EditingMappingProfile_Entry(unit_i,id) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Delete_EditingMappingProfile_Entry \n",
                __FUNCTION__,id);
            return MEA_ERROR;
        }
        if (MEA_API_GetNext_EditingMappingProfile_Entry(unit_i,
            &id,
            NULL,
            &found) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_GetNext_EditingMappingProfile_Entry \n",
                    __FUNCTION__,id);
                return MEA_ERROR;
        }
    }

 return MEA_OK;
}

