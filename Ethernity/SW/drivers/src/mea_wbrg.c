/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*************************************************************************************/
/* 	Module Name:		 mea_wbrg.c		   									 */ 
/*																					 */
/*  Module Description:	 MEA driver										 */
/*																					 */
/*  Date of Creation:	 													         */
/*																					 */
/*  Author Name:			 													         */
/*************************************************************************************/ 

/*-------------------------------- Includes ------------------------------------------*/

#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_wbrg.h"
#include "mea_init.h"


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_WBRG_NUM_OF_TABLE_SIZE  256 

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
MEA_BRG_PVID_dbt	        *MEA_drv_BRG_PVID_Table =NULL;    


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/




static void mea_drv_Edit_TRANSFORM_UpdateHwEntry_WBRG(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t              unit_i     = (MEA_Unit_t              )arg1;
    //MEA_db_HwUnit_t         hwUnit_i   = (MEA_db_HwUnit_t         )arg2;
    //MEA_Policer_Prof_hw_dbt*hwEntry_pi = (MEA_Policer_Prof_hw_dbt*)arg3;
    MEA_Uint32        entry_value   = (MEA_Uint32  )  ( (long) arg4);
    MEA_Uint32             val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32             i;
    MEA_Uint16             num_of_regs;

 //   MEA_Uint32             count_shift;


    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),1 );

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
 //   count_shift=0;
    val[0]= entry_value;


    /**************************/


    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }

    
	 
}

MEA_Status mea_drv_WBRG_Init_tranform_table(MEA_Unit_t       unit_i)
{
    MEA_ind_write_t ind_write;
    MEA_Uint32 index;
    MEA_Uint32 valueSet=0;
    MEA_Uint8 MEA_edit_trans_table[MEA_WBRG_NUM_OF_TABLE_SIZE] = {MEA_WBRG_EDIT_TRANSFORM_DATA};



    ind_write.tableType      = ENET_BM_TBL_TYP_TRANSFORM_EDIT ;
    ind_write.tableOffset    = 0; /* Will be define inside the loop */
    ind_write.cmdReg         = MEA_BM_IA_CMD;      
    ind_write.cmdMask        = MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg      = MEA_BM_IA_STAT;   
    ind_write.statusMask     = MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg   = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask  = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry     = (MEA_FUNCPTR)mea_drv_Edit_TRANSFORM_UpdateHwEntry_WBRG;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2     = (MEA_Uint32)hwUnit;
    //ind_write.funcParam3     = (MEA_Uint32) 0;
    ind_write.funcParam4 = (MEA_funcParam)(long)valueSet;


    if(MEA_WBRG_SUPPORT == MEA_TRUE){
        for(index=0 ; index < MEA_WBRG_NUM_OF_TABLE_SIZE ; index++ ){
            ind_write.tableOffset	 = index;
            valueSet                 = (MEA_Uint32) MEA_edit_trans_table[index];
            ind_write.funcParam4 = (MEA_funcParam)((long)valueSet);

            if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
                    __FUNCTION__,
                    ind_write.tableType,
                    ind_write.tableOffset,
                    MEA_MODULE_BM);
                return MEA_ERROR;
            }   
        } // for
    }

return  MEA_OK;
}


static void mea_drv_BRG_port_Attrib_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t              unit_i     = (MEA_Unit_t              )arg1;
    //MEA_db_HwUnit_t         hwUnit_i   = (MEA_db_HwUnit_t         )arg2;
    //MEA_Policer_Prof_hw_dbt*hwEntry_pi = (MEA_Policer_Prof_hw_dbt*)arg3;
    MEA_Uint32        entry_value = (MEA_Uint32)((long)arg4);
    MEA_Uint32             val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32             i;
    MEA_Uint16             num_of_regs;

    //MEA_Uint32             count_shift;


    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),1 );

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
   // count_shift=0;
    val[0]= entry_value;


    /**************************/


    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }



}




static MEA_Status mea_BRG_Port_Attrib_UpdateHw(MEA_Unit_t     unit,
    MEA_Port_t     port,
    MEA_BRG_PVID_dbt     *entry )
{
    MEA_ind_write_t             ind_write;
    MEA_Uint32                   valueSet;


    if(!MEA_WBRG_SUPPORT){
        return MEA_OK;
    }

    /* Check if we have changes */
    if ((MEA_OS_memcmp(&entry->egress,&MEA_drv_BRG_PVID_Table[port].egress,sizeof(entry->egress)) != 0)) {

        ind_write.tableType      = ENET_BM_TBL_TYP_DEST_WBRG_PORT_ATTRIBUTE ;
        ind_write.tableOffset    = port; 
        ind_write.cmdReg         = MEA_BM_IA_CMD;      
        ind_write.cmdMask        = MEA_BM_IA_CMD_MASK;      
        ind_write.statusReg      = MEA_BM_IA_STAT;   
        ind_write.statusMask     = MEA_BM_IA_STAT_MASK;   
        ind_write.tableAddrReg   = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask  = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry     = (MEA_FUNCPTR)mea_drv_BRG_port_Attrib_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2     = (MEA_Uint32)hwUnit;
        //ind_write.funcParam3     = (MEA_Uint32) 0;

        valueSet                 = 0;
        valueSet  = entry->egress.Pvid;
        valueSet |= entry->egress.egress_type<<12;
        valueSet |= entry->egress.encap_swap_mode<<14;
        valueSet |= entry->egress.external_select<<16;
        valueSet |= entry->egress.internal_select<<17;

        
        ind_write.funcParam4 = (MEA_funcParam)((long)valueSet);

        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }   


    }

    if ((MEA_OS_memcmp(&entry->ingress,&MEA_drv_BRG_PVID_Table[port].ingress,sizeof(entry->ingress)) != 0)) {

        ind_write.tableType      = ENET_BM_TBL_TYP_DEST_WBRG_PORT_ATTRIBUTE ;
        ind_write.tableOffset    = 128 + port; 
        ind_write.cmdReg         = MEA_BM_IA_CMD;      
        ind_write.cmdMask        = MEA_BM_IA_CMD_MASK;      
        ind_write.statusReg      = MEA_BM_IA_STAT;   
        ind_write.statusMask     = MEA_BM_IA_STAT_MASK;   
        ind_write.tableAddrReg   = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask  = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry     = (MEA_FUNCPTR)mea_drv_BRG_port_Attrib_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        //ind_write.funcParam2     = (MEA_Uint32)hwUnit;
        //ind_write.funcParam3     = (MEA_Uint32) 0;

        valueSet                 = 0;
        valueSet  = entry->ingress.Pvid;



        ind_write.funcParam4 = (MEA_funcParam)((long)valueSet);

        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }   



    }




    return MEA_OK;
}


MEA_Status mea_drv_WBRG_Init(MEA_Unit_t       unit_i)
{

#ifdef MEA_NEED_PVID
    MEA_Uint32 size;
#endif    


    if(MEA_WBRG_SUPPORT == MEA_FALSE){
        return MEA_OK;
     }
    
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize WBRG configuration  ...\n");
     if( mea_drv_WBRG_Init_tranform_table(unit_i) !=MEA_OK){
            return MEA_ERROR;
      }
    

    
#ifdef MEA_NEED_PVID

 
   
        /* Allocate Table */
        size = (MEA_MAX_PORT_NUMBER+1) * sizeof(MEA_BRG_PVID_dbt   );
    if (size != 0) {
        MEA_drv_BRG_PVID_Table    = (MEA_BRG_PVID_dbt   *)MEA_OS_malloc(size);
        if (MEA_drv_BRG_PVID_Table    == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_drv_BRG_PVID_Table    failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_drv_BRG_PVID_Table   [0]),0,size);
    }
#endif
   /* because all zero no need to set the HW*/


    
    return MEA_OK;
}
MEA_Status mea_drv_WBRG_RInit(MEA_Unit_t       unit_i)
{
#ifdef MEA_NEED_PVID
    MEA_Port_t port;
#endif
    if(!MEA_WBRG_SUPPORT)
        return MEA_OK;

       if( mea_drv_WBRG_Init_tranform_table(unit_i) !=MEA_OK){
        return MEA_ERROR;
       }
#ifdef MEA_NEED_PVID
       for(port=0;port<=MEA_MAX_PORT_NUMBER ;port++){
           if (MEA_API_Get_IsPortValid(port,MEA_TRUE)==MEA_FALSE) 
               continue ;
           if(mea_BRG_Port_Attrib_UpdateHw(unit_i, port,&MEA_drv_BRG_PVID_Table[port] )!=MEA_OK){
                  return MEA_ERROR;
           }
       }
#endif    
    
    
    
    return MEA_OK;
}
MEA_Status mea_drv_WBRG_Conclude(MEA_Unit_t       unit_i)
{
    if(!MEA_WBRG_SUPPORT)
        return MEA_OK;
#ifdef MEA_NEED_PVID
    if(MEA_drv_BRG_PVID_Table){
        MEA_OS_free(MEA_drv_BRG_PVID_Table);
        MEA_drv_BRG_PVID_Table=NULL;
    }
#endif

    return MEA_OK;
}







/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_API_Set_BRG_PVID_Entry(MEA_Unit_t    unit,MEA_Port_t   port,
    MEA_BRG_PVID_dbt* entry)
{
    if(!MEA_WBRG_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," PVID BRG not support on this HW\n");  
        return MEA_ERROR;
    }
#ifndef MEA_NEED_PVID
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," PVID BRG work flow base\n");  
    return MEA_ERROR;

#endif 


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(port > MEA_MAX_PORT_NUMBER ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," port %d is out of range \n",port);  
        return MEA_ERROR;
    }

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE)
        return MEA_ERROR;
    /* Check for valid entry parameter */
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry == NULL (port=%d)\n",
            __FUNCTION__,port);
        return MEA_ERROR; 
    }
#endif
        /* Check if we have changes */
        if ((MEA_OS_memcmp(entry,&MEA_drv_BRG_PVID_Table[port],sizeof(*entry)) == 0)) {
                return MEA_OK;
        }

        if(mea_BRG_Port_Attrib_UpdateHw(unit,port,entry)!=MEA_OK){
            return MEA_ERROR;
        }

       /* Update the Software shadow DB */
        MEA_OS_memcpy(&(MEA_drv_BRG_PVID_Table[port]),
            entry,
            sizeof(MEA_drv_BRG_PVID_Table[0]));

    
    return MEA_OK;
}

MEA_Status MEA_API_Get_BRG_PVID_Entry(MEA_Unit_t         unit,
                                      MEA_Port_t         port,
                                      MEA_BRG_PVID_dbt* entry)
{
    if(!MEA_WBRG_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," PVID BRG not support on this HW\n");  
        return MEA_ERROR;
    }

#ifndef MEA_NEED_PVID
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," PVID BRG work flow base\n");  
    return MEA_ERROR;

#endif 




#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(port > MEA_MAX_PORT_NUMBER ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," port %d is out of range \n",port);  
        return MEA_ERROR;
    }

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE)
        return MEA_ERROR;
    /* Check for valid entry parameter */
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry == NULL (port=%d)\n",
            __FUNCTION__,port);
        return MEA_ERROR; 
    }
#endif

    MEA_OS_memcpy(entry,&(MEA_drv_BRG_PVID_Table[port]),sizeof(*entry));
    
    return MEA_OK;
}

