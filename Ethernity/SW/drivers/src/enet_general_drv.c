/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/******************************************************************************
*     Module Name:         enet_general_drv.c                                 
*                                                                             
*   Module Description:         
*                            
*                         
                        
*                                                                              
*  Date of Creation:     14/09/06                                              
*                                                                             
*  Author Name:             Alex Weis.                                         
*******************************************************************************/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "MEA_platform_types.h"
#include "mea_api.h"
#include "mea_drv_common.h"
#include "enet_group_drv.h"
#include "MEA_platform_os_utils.h"
#include "mea_port_drv.h"
#include "enet_queue_drv.h"
#include "enet_shaper_profile_drv.h"
#include "enet_xPermission_drv.h"
#include "mea_init.h"
#include "mea_if_regs.h"
#include "mea_if_counters_drv.h"
#include "mea_if_service_drv.h"
#include "mea_limiter_drv.h"
#include "mea_lxcp_drv.h"
#include "mea_flowCoSMapping_drv.h"
#include "mea_flowMarkingMapping_drv.h"
#include "mea_action_drv.h"
#include "mea_lxcp_drv.h"
#include "mea_filter_drv.h"
#include "mea_bm_drv.h"
#include "mea_globals_drv.h"
#include "mea_deviceInfo_drv.h"
#include "mea_bm_wred_drv.h"
#include "mea_PacketGen_drv.h"
#include "mea_PortState_drv.h"

#include "mea_ccm_drv.h"
#include "mea_bfd_drv.h"
#include "mea_if_vsp_drv.h"
#include "mea_if_parser_drv.h"
#include "mea_if_l2cp_drv.h"
#include "mea_if_parser_drv.h"
#include "mea_swe_ip_drv.h"
#include "mea_mapping_drv.h"
#include "mea_preATM_parser_drv.h"
#include "mea_Interface_drv.h"

#include "mea_hc_decomp_drv.h"
#include "mea_hc_comp_drv.h"
#include "mea_hc_classifier_drv.h"
#include "mea_vpls.h"
#include "mea_tft_drv.h"
#include "mea_sflow_drv.h"
#include "enet_general_drv.h"
#include "mea_IPSec_drv.h"
#include "mea_ACL5_profiles_drv.h"
#include "mea_if_Acl5_drv.h"
#include "mea_lpm_drv.h"
#include "mea_queueStatistics_drv.h"





/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define ENET_PLAT_DEVICE_TOPOLOGY_GROUP_MAX_COUNT 12
#define ENET_PLAT_DEVICE_RESERV_REG_SIZE 4
#define ENET_PLAT_DEVICE_MAX_GIGA_DISTANCE 5
#define ENET_PLAT_DEVICE_MAX_10GIGA_DISTANCE 8

#define ENET_PLAT_DEVICE_MAX_10GIGA_DISTANCE_WITH_TLS 5
#define ENET_PLAT_DEVICE_MAX_10GIGA_LEN_TLS_UP   13


#define MEA_SCDUER_MAX_PROF 5
/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef enum {
    ENET_PLAT_DEVICE_TYPE_UNKNOWN,
    ENET_PLAT_DEVICE_TYPE_MEA_OS_Z1_OLD_BOARD,
    ENET_PLAT_DEVICE_TYPE_ENET3000,
    ENET_PLAT_DEVICE_TYPE_ENET4000,
    ENET_PLAT_DEVICE_TYPE_LAST
} ENET_PLAT_DeviceType_te;

/* example first port = 0 , count = 32 , cluster_resolution = 4 */
/* we will create 32 ports[0..31] and 8 clusters[0,4,8,...,28]  */
typedef struct {
    ENET_PortId_t      first_port;
    ENET_PortId_t      count;
    ENET_QueueId_t     first_cluster;
    ENET_Uint8         cluster_resolution;  
    MEA_Port_type_te  port_type;
} ENET_PLAT_DeviceTopology_group_dbt;



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
/* VER_XXX*/

const char*   ENET_sw_ver = "0";

ENET_Bool    ENET_InitDone = ENET_FALSE;
extern ENET_Bool    mea_reinit_Scheduler_length;
extern MEA_Bool Init_QUEUE ;
/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
typedef enum{
MEA_PRE_SCDULER_TYPE_NON=0,
MEA_PRE_SCDULER_TYPE_100=1,
MEA_PRE_SCDULER_TYPE_1G=2,
MEA_PRE_SCDULER_TYPE_10G=3,
MEA_PRE_SCDULER_LAST
}MEA_Scheduler_relation_t;

typedef struct {
      MEA_Bool                   valid;
      MEA_Uint16                 port;
      MEA_Scheduler_relation_t   relation;

}MEA_PreScheduler_dbt;

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/



//MEA_PreScheduler_dbt  MEA_Scheduler_ProFile[MEA_SCDUER_MAX_PROF][MEA_MAX_PORT_NUMBER+1];
                                                         
 

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*        <enet_Init_Scheduler_Table>                                         */
/*                                                                            */
/*   The algorithm: let n-number of fust ports,                                */
/*                     m=max(5-n,1)- num of slow port at a time               */
/*                                                                            */
/*     put n fust port and than m slow ports , repeat until all slow ports    */
/*         are handled                                                        */
/*----------------------------------------------------------------------------*/





/*----------------------------------------------------------------------------*/
/*                                                                            */
/*        <enet_Init_MEA_driver>                                              */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Init_MEA_driver(ENET_Unit_t                        unit_i)
{
    MEA_Globals_Entry_dbt globals_entry;



    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize AdvanceInit...\n");
    /* Init Advance MEA driver */
    if (mea_drv_AdvanceInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_AdvanceInit failed\n", __FUNCTION__);
        return MEA_ERROR;
    }

#ifdef MEA_PLATFORM_TDM_SUPPORT

    if (MEA_TDM_SUPPORT){
        {
            MEA_drv_TDM_global_dbt entry;

            if (MEA_API_TDM_Get_Global(unit_i, &entry) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_TDM_Set_Global failed\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }

            entry.etu_mode_Enable = MEA_TRUE;
            entry.e1_t1_mode = MEA_FALSE; // E1 mode
            entry.filter_current_jb = MEA_TRUE;

            if (MEA_API_TDM_Set_Global(unit_i, &entry) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_TDM_Set_Global failed\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }
    }
#endif


if (!b_bist_test)
{

    /* Enable BM and IF */
    MEA_API_Get_Globals_Entry(MEA_UNIT_0,&globals_entry);
    globals_entry.bm_config.val.bm_enable = MEA_TRUE;
    
    MEA_API_Set_Globals_Entry(MEA_UNIT_0,&globals_entry);
}
    

    /* Return to Caller */
    return ENET_OK;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*        <enet_Create_Ports>                                                 */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Create_Ports(ENET_Unit_t                        unit_i) {


    ENET_PortId_t port_id;

    MEA_PortsMapping_t  portMap;



    /* build the mea_portGroup_vec and mea_portGroup_count from device_topology */
    for (port_id=0;port_id<=MEA_MAX_PORT_NUMBER;port_id++) {
       if (MEA_Low_Get_Port_Mapping_By_key(unit_i,
                                           MEA_PORTS_TYPE_EGRESS_TYPE,
                                           (MEA_Uint16)port_id,
                                           &portMap)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n",port_id);
       }
       if (!portMap.valid){
           /* check if ingress port exist */
           if (MEA_Low_Get_Port_Mapping_By_key(unit_i,
                                               MEA_PORTS_TYPE_INGRESS_TYPE,
                                               (MEA_Uint16)port_id,
                                               &portMap)!=MEA_OK){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n",port_id);
           }
           if (!portMap.valid){
               /* this port number is not used as physical or logical */
               continue;
           }
       }

    } /* for */

    /* Return to Caller */
    return ENET_OK;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*        <enet_Create_Clusters>                                              */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Create_Clusters(ENET_Unit_t                        unit_i) {


    ENET_QueueId_t queue_id;
    MEA_PortsMapping_t portMap;
    ENET_QueueId_t start, Id;
    MEA_Uint32 NumOfCluster;
    MEA_Bool   ClusterSet = MEA_FALSE;
    MEA_Bool  cluster127= MEA_FALSE;
    

//default first
// Create The CPU cluster 
#if 0
    for (queue_id = 0; queue_id <= MEA_MAX_PORT_NUMBER_EGRESS; queue_id++)
    {
        
        if ((MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) && 
            ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_1K) == 0) || 
            (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_RN7035_1K) == 0) )) {
            if (queue_id != 127)
                continue;
        }
        else{
            if (queue_id != 127 && queue_id != 110 && queue_id != 111 && queue_id != 118 && queue_id != 119 && queue_id != 125 && queue_id != 126)
                continue;
        }
        if (MEA_Low_Get_Port_Mapping_By_key(unit_i,MEA_PORTS_TYPE_EGRESS_TYPE,(MEA_Uint16)queue_id, &portMap)!=MEA_OK)
        {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on queue_id %d \n",queue_id);
        }
        if (portMap.valid){
            /* Create default Queue for port */
           
                 //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"queue_id %d \n",queue_id);
                enet_InitDefaultQueueForPort(ENET_UNIT_0, (ENET_PortId_t)portMap.physicalport, queue_id);
            }
        

    }

#endif

    queue_id = 127;
    if (MEA_Low_Get_Port_Mapping_By_key(unit_i, MEA_PORTS_TYPE_EGRESS_TYPE, (MEA_Uint16)queue_id, &portMap) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_Low_Get_Port_Mapping_By_key failed on queue_id %d \n", queue_id);
    }
    if (portMap.valid) {
        /* Create default Queue for port */

        //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"queue_id %d \n",queue_id);
        enet_InitDefaultQueueForPort(ENET_UNIT_0, (ENET_PortId_t)portMap.physicalport, queue_id);
        cluster127=MEA_TRUE;
        
    }

    


    
    
    for (queue_id=0; queue_id<= MEA_MAX_PORT_NUMBER; queue_id++) {
        
        ClusterSet = MEA_FALSE;
        if ((MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) && 
            ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_1K) == 0) ||
            (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_RN7035_1K) == 0  )  
                )) {
            if (queue_id == 127)
                continue;
        }
#if 0
        else {
            if (queue_id == 127 || queue_id == 110 || queue_id == 111 || queue_id == 118 || queue_id == 119 || queue_id == 125 || queue_id == 126)
                continue;
        }
#endif
        if (MEA_Low_Get_Port_Mapping_By_key(unit_i,
			                               MEA_PORTS_TYPE_EGRESS_TYPE,
                                           (MEA_Uint16)queue_id,
                                           &portMap)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on queue_id %d \n",queue_id);
        }
        if (portMap.valid) {

            if ((MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) && 
                ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_1K) == 0) ||
                (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_RN7035_1K) == 0)))
            {
                if (queue_id == 101 || ( (queue_id >= 80) && (queue_id <= 86) )) {
                    if (queue_id == 101 ) { //port
                        start = 128;
                        NumOfCluster = 64;
                    }
                    if (queue_id >= 80 && queue_id <= 86) {
                        start = 192 + 64 * (queue_id - 80);
                        NumOfCluster = 64;
                    }


                    for (Id = start; Id < start + NumOfCluster; Id++) {
                        /* Create default Queue for port */
                        ClusterSet = MEA_TRUE;
                        enet_InitDefaultQueueForPort(ENET_UNIT_0, (ENET_PortId_t)portMap.physicalport, Id);
                    }
                }
                else {
                    ClusterSet = MEA_TRUE;
                    enet_InitDefaultQueueForPort(ENET_UNIT_0,(ENET_PortId_t)portMap.physicalport,queue_id);
                }


             
            }
            if ((MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) &&
                  (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_NIC40G) == 0)) 
            {

                if (queue_id == 127) 
                {
                    start = 81;
                    NumOfCluster = 4;

                    for (Id = start; Id < start + NumOfCluster; Id++) {
                        /* Create default Queue for port */
                        ClusterSet = MEA_TRUE;
                        enet_InitDefaultQueueForPort(ENET_UNIT_0, (ENET_PortId_t)portMap.physicalport, Id);
                    }
                }else{
                    ClusterSet = MEA_TRUE;
                    enet_InitDefaultQueueForPort(ENET_UNIT_0, (ENET_PortId_t)portMap.physicalport, queue_id);
            
                
                }


            }
            else{
                 if((queue_id == 127) && cluster127 == MEA_TRUE)
                      continue;

                /* Create default Queue for port */
                if(ClusterSet == MEA_TRUE){
                    continue;
                }
                enet_InitDefaultQueueForPort(ENET_UNIT_0,
                (ENET_PortId_t)portMap.physicalport,
                    queue_id);
            }
        }



    }
    
    Init_QUEUE = MEA_TRUE;
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Init_QUEUE\n");
    if (enet_ReInit_Queue(unit_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - enet_ReInit_Queue failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    /* Return to Caller */
    return ENET_OK;

}


/*----------------------------------------------------------------------------*/
/*             APIs functions implementation                                  */
/*----------------------------------------------------------------------------*/
MEA_Status MEA_API_Device_environment_Init(mea_global_environment_dbt *entry) {

    
    if (entry == NULL) {
        
        /*Get from Appinit */
        MEA_device_environment_init();
        return MEA_OK;
    }
    else {
        if (entry->valid == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "User Set Environment \n");
 
        }
        else {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "User Not Set MEA_API_ENV_SET  \n");
            return MEA_ERROR;

        }

        

    }

    return MEA_OK;
}

void MEA_PCI_Init_bdf(void)
{

#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)
    char buf[500];
    char *up_ptr;
    char PCIADDR[50];
#endif

	MEA_OS_memset(&mea_pcie_device, 0, sizeof(mea_pcie_device));

	


#if defined(MEA_OS_LINUX) && defined(HW_BOARD_IS_PCI)

	if (MEA_device_environment_info.MEA_PCIE_slot_enable == MEA_TRUE) {

		MEA_OS_strcpy(buf, MEA_device_environment_info.MEA_PCIE_slot);
		if ((up_ptr = strchr(buf, ':')) != 0) {
			(*up_ptr) = 0;
			up_ptr++;
			mea_pcie_device[0].bus = MEA_OS_atohex(buf);  /*bus*/
			MEA_OS_strcpy(buf, up_ptr);
			if ((up_ptr = strchr(buf, '.')) != 0) {
				(*up_ptr) = 0;
				up_ptr++;
				mea_pcie_device[0].slot = MEA_OS_atohex(buf); /*slot*/
				mea_pcie_device[0].func = MEA_OS_atohex(up_ptr); /*func*/

			}
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "User: GET PCIe bus = 0x%x slot = 0x%x func = 0x%x\n",
					mea_pcie_device[0].bus, mea_pcie_device[0].slot, mea_pcie_device[0].func);
		}

	} else {


		FILE *fp = popen("lspci | grep Xilinx | awk '{print $1 }'", "r");
		if (fp != NULL) {
			if (fgets(PCIADDR, 20, fp) > 0) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Read  PCIe Xilinx %s  \n", PCIADDR);
			}
			pclose(fp);
		}
		MEA_OS_strcpy(buf, PCIADDR);
		if ((up_ptr = strchr(buf, ':')) != 0) {
			(*up_ptr) = 0;
			up_ptr++;
			mea_pcie_device[0].bus = MEA_OS_atohex(buf);  /*bus*/
			MEA_OS_strcpy(buf, up_ptr);
			if ((up_ptr = strchr(buf, '.')) != 0) {
				(*up_ptr) = 0;
				up_ptr++;
				mea_pcie_device[0].slot = MEA_OS_atoi(buf); /*slot*/
				mea_pcie_device[0].func = MEA_OS_atoi(up_ptr); /*func*/

			}
			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " PCIe Xilinx  bus = 0x%x slot = 0x%x func = 0x%x\n",
					mea_pcie_device[0].bus, mea_pcie_device[0].slot, mea_pcie_device[0].func);

		}

	}
#endif
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*        <MEA_API_Init>                                                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/
MEA_Status MEA_API_Init(MEA_Unit_t                   unit_i,
    MEA_ULong_t*                module_base_addr_vec_i)
{

    MEA_Events_Entry_dbt entry;
    char enet_sw_ver[120];
	char enet_Build[80];
    char enet_chip_ver[MEA_CHIP_NAME_STR_LENGTH];
    MEA_HwVersion_info_t hw_version;


    MEA_InitDone = MEA_FALSE;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
        "***** MEA driver init start ******\n");
    /* init All getenv to one Place */


    MEA_device_environment_init();


#if (defined(MEA_OS_DEVICE_MEMORY_SIMULATION))


#if defined(MEA_OS_DEVICE_SIMULATION_DEF)
     MEA_Uint32 Type;

    Type = (MEA_Uint32)MEA_OS_DEVICE_SIMULATION_DEF;

    if (Type == MEA_OS_DEVICE_SIMULATION_Z7045_GW_1KC){
        MEA_device_environment_info.MEA_PRODUCT_NAME_enable = MEA_TRUE;
        MEA_OS_strcpy(&MEA_device_environment_info.PRODUCT_NAME[0], (MEA_BOARD_TYPE_EPC_7045_1K));
        MEA_device_environment_info.MEA_GW_VPLS_EN_ebable = MEA_TRUE;
        MEA_device_environment_info.MEA_GW_VPLS_EN = MEA_FALSE;
    }
    if(Type == MEA_OS_DEVICE_SIMULATION_NIC_40G){
        MEA_device_environment_info.MEA_PRODUCT_NAME_enable = MEA_TRUE;
        MEA_OS_strcpy(&MEA_device_environment_info.PRODUCT_NAME[0], (MEA_BOARD_TYPE_NIC40G));
    }


#endif
#endif  


    if (MEA_device_environment_info.MEA_PRODUCT_NAME_enable) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Board Name >> %s\n", MEA_device_environment_info.PRODUCT_NAME);
    }
    else {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Board Name >> NULL\n");
    }


    /* Init ENET Group object */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Groups...\n");
    if (enet_Init_Group(unit_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - enet_Init_Group failed \n",
                              __FUNCTION__);
        return ENET_ERROR;
    }
    
   

      

    /* Basic init of MEA driver */
    if (mea_drv_BasicInit(unit_i,
                          module_base_addr_vec_i) != MEA_OK) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_BasicInit failed\n",__FUNCTION__);
        return ENET_ERROR;
    }
    

    /* Init old MEA driver */
    if (enet_Init_MEA_driver(unit_i) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_Init_MEA_driver failed\n",
                          __FUNCTION__);
       return ENET_ERROR;
    }

    if (!b_bist_test){


        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize Enhance driver Started...\n");

        /* Init ENET Queue object */
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize Queues...\n");
        if (enet_Init_Queue(unit_i) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Init_Queue failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }


        /* Init ENET ShaperProfile object */
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize ShaperProfile...\n");
        if (enet_Init_Shaper_Profile(unit_i) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Init_Shaper_Profile failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }


        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Create     Queue Clusters...\n");
        if (enet_Create_Clusters(unit_i) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Create_Clusters  failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }

        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Create     Action To CPU ...\n");
        if (mea_Action_Create_ActionToCpu(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_Action_Create_ActionToCpu  failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }

        /* Init IF LxCP Table */
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize IF LxCp     table ..\n");
        if (mea_lxcp_drv_Init(unit_i) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_lxcp_drv_Init  failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if (MEA_SW_VPLS_SUPPORT){
            if (mea_drv_VPLS_Init(unit_i) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_VPLS_Init  failed \n",
                    __FUNCTION__);
            }
            

        }

        if (mea_drv_TFT_Init(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_TFT_Init  failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        if(mea_drv_sflow_Init(unit_i) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_sflow_Init  failed \n",
            __FUNCTION__);
           return MEA_ERROR;
        }

		if (mea_drv_Init_ACL5_Profiles(unit_i) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_Init_ACL5_Profiles  failed \n",
				__FUNCTION__);
			return MEA_ERROR;
		}

		if (mea_drv_Init_IF_ACL5(unit_i) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_Init_IF_ACL5  failed \n",
				__FUNCTION__);
			return MEA_ERROR;
		}

        if (mea_drv_Init_Lpm(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_Lpm  failed \n",
            __FUNCTION__);
        return MEA_ERROR;
        }

        if (mea_drv_Init_QueueStatistics(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_QueueStatistics  failed \n",
            __FUNCTION__);
        return MEA_ERROR;
        }
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize Enhance driver Finished...\n");


        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize default objects...\n");
        if (mea_Init_Cpu_Service(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_Init_Cpu_Service  failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }

        /* read the Events of the FPGA*/
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Clear      Events ...\n");
        if (MEA_API_Get_Events_Entry(MEA_UNIT_0,
            &entry) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Get_Events failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }


        /* Get the Software Version */
        if (MEA_API_Get_SoftwareVersion(enet_sw_ver, sizeof(enet_sw_ver)) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_Get_SoftwareVersion failed\n",
                __FUNCTION__);
            return ENET_ERROR;
        }
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
            "Software version: %s\n", enet_sw_ver);


        if (MEA_API_Get_HwVersion(&hw_version) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_API_Get_HwVersion failed\n");
            return MEA_ERROR;
        }


        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "    Hardware Version: %x.%x.%02x.%x   (Date %02x/%02x/%x: hour %02x:00 )\n",
            (hw_version.if_reg_HWversion.val.product_number),
            hw_version.if_reg_HWversion.val.phase_version,
            hw_version.if_reg_HWversion.val.release_number,
            hw_version.if_reg_HWversion.val.vlsi_int_version,

            hw_version.if_reg_HWdate.val.day,
            hw_version.if_reg_HWdate.val.month,
            hw_version.if_reg_HWdate.val.year,
            hw_version.if_reg_HWdate.val.hour);




        if (MEA_API_Get_ChipName(enet_chip_ver, sizeof(enet_chip_ver)) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Get_ChipName failed\n",
                __FUNCTION__);
            return ENET_ERROR;
        }

        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " %s\n", enet_chip_ver);
        
		if (MEA_API_Get_SoftwareBuildTag(enet_Build, sizeof(enet_Build)) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_Get_SoftwareBuildTag failed\n",
				__FUNCTION__);
			return ENET_ERROR;
		}
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
			"Tag: %s\n", enet_Build);


		{
            MEA_LAG_dbt           Lag_entry;
            MEA_Uint16 id = MEA_PLAT_GENERATE_NEW_ID;
            MEA_OS_memset(&entry, 0, sizeof(Lag_entry));
            if ((MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) &&
            (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_NIC40G) == 0)) {
              //Create Lag for port 127 
                Lag_entry.numof_clusters = 4;
                Lag_entry.clustermask =0xf;
                Lag_entry.cluster[0] = 81;
                Lag_entry.cluster[1] = 82;
                Lag_entry.cluster[2] = 83;
                Lag_entry.cluster[3] = 84;

                if (MEA_API_LAG_Create_Entry(MEA_UNIT_0, &Lag_entry, &id) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"MEA_API_LAG_Create_Entry failed\n");
                    return MEA_OK;
                }
        
        
            }
        }

        /* end of ENET Driver init */
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
            "***** ENET Device driver init finish  ******\n");
#if 0
#ifdef MEA_LOCAL_MNG      
        MEA_Device_LOCAL_MNG_127_100(unit_i);
#endif
#endif

        

    ENET_InitDone = MEA_TRUE;
  

   

    { //start
         


#if defined(MEA_OS_LINUX) && defined(MEA_ETHERNITY) && !defined(__KERNEL__)
          { 
              MEA_Globals_Entry_dbt entryGlobals;

              
              

              if (MEA_device_environment_info.MEA_WD_DISABLE_ENV_enable) {
                  if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entryGlobals) != MEA_OK){

                  }
                  entryGlobals.if_global1.val.wd_disable = MEA_device_environment_info.MEA_WD_DISABLE_ENV;
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"user set WD set to Disable = %s\n",MEA_STATUS_STR(entryGlobals.if_global1.val.wd_disable));
                  if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entryGlobals) != MEA_OK){

                  }
              }
          }
#endif
       

  


    }//end






  }//!bist


    /* Return to caller */
    return ENET_OK;
}



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*        <MEA_API_Conclude>                                                     */
/*                                                                            */
/*----------------------------------------------------------------------------*/

MEA_Status MEA_API_Conclude (MEA_Unit_t       unit_i)
{

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Started \n");

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude default objects...\n");
#ifdef MEA_LOCAL_MNG
    {
        MEA_Uint32 Globals1;
        Globals1 = mea_drv_get_if_global1(MEA_UNIT_0);

        Globals1 &= 0xfffffffe<< 3;
        MEA_API_WriteReg(MEA_UNIT_0, MEA_IF_GLOBAL_REG(1), Globals1, MEA_MODULE_IF);
    }
#endif	



    {
        MEA_Port_t port;
        MEA_EgressPort_Entry_dbt entry;
        MEA_IngressPort_Entry_dbt ing_entry;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude ingress port Admin Down\n");
        for (port = 0; port <= MEA_MAX_PORT_NUMBER_INGRESS; port++) {
            if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) == MEA_FALSE) {
                continue;
            }
			
            if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0, port, &ing_entry)
                != MEA_OK) {
                    return MEA_ERROR;
            }

            ing_entry.rx_enable= MEA_FALSE;
            if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0,port,&ing_entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s  Conclude portShaper  %d failed\n",port);
                return MEA_ERROR;
            }

        }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Egress port Admin Down\n");

    for (port = 0; port <= MEA_MAX_PORT_NUMBER; port++) {
		
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE) == MEA_FALSE) {
            continue;
        }
        MEA_OS_memset(&entry,0,sizeof(entry));

        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0, port, &entry)
            != MEA_OK) {
                return MEA_ERROR;
        }
        entry.shaper_enable = ENET_FALSE;
        entry.tx_enable     = MEA_FALSE;
        if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s  Conclude portShaper  %d failed\n",port);
            return MEA_ERROR;
        }

    }
    }

    //power off 
#if defined(MEA_OS_SYAC)
    {

    MEA_RXAUI_Entry_dbt   entry_RXAUI;
    
    MEA_API_Get_Interface_RXAUI_Entry(unit_i,1,&entry_RXAUI);
  
    entry_RXAUI.val.power_tx_down=MEA_TRUE;

    MEA_API_Set_Interface_RXAUI_Entry(unit_i,1,&entry_RXAUI);

    MEA_API_Get_Interface_RXAUI_Entry(unit_i,2,&entry_RXAUI);

    entry_RXAUI.val.power_tx_down=MEA_TRUE;
    MEA_API_Set_Interface_RXAUI_Entry(unit_i,2,&entry_RXAUI);
    
    }


#endif


    if (mea_Conclude_Cpu_Service(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - mea_Conclude_Cpu_Service  failed \n",
                              __FUNCTION__);
        return ENET_ERROR;
    }
    

    if (MEA_API_Conclude_Services(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Conclude_Services failed \n",
                          __FUNCTION__);
       return ENET_ERROR;
    }

    
    if (mea_drv_Conclude_FlowCoSMappingProfile_Table(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Conclude_FlowCoSMappingProfile_Table failed \n",
                          __FUNCTION__);
        return ENET_ERROR;
    }

    if (mea_drv_Conclude_FlowMarkingMappingProfile_Table(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Conclude_FlowMarkingMappingProfile_Table failed \n",
                          __FUNCTION__);
        return ENET_ERROR;
    }
    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude VSP_Fragment...\n");
    if( mea_drv_VSP_Fragment_Conclude(unit_i)!= MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_VSP_Fragment_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }


        if (mea_drv_VPLS_Conclude(unit_i) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_VPLS_Init  failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }
        if (mea_drv_TFT_Conclude(unit_i) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_TFT_Init  failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }
        if (mea_drv_sflow_Conclude(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_sflow_Conclude  failed \n",
                __FUNCTION__);
            return ENET_ERROR;
        }

		if (mea_drv_ACL5_Conclude(unit_i) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_Conclude  failed \n",
				__FUNCTION__);
			return ENET_ERROR;
		}

		if (mea_drv_Conclude_IF_ACL5(unit_i) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_Conclude_IF_ACL5  failed \n",
				__FUNCTION__);
			return ENET_ERROR;
		}

        if(mea_drv_Conclude_Lpm(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_Lpm  failed \n",
            __FUNCTION__);
        return ENET_ERROR;
        }

        if(mea_drv_Conclude_QueueStatistics(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ACL5_Conclude  failed \n",
            __FUNCTION__);
        return ENET_ERROR;
        }
 
    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Limiter...\n");
    mea_Conclude_limiter(ENET_UNIT_0);

    mea_drv_ADM_Conclude(ENET_UNIT_0);
    mea_drv_ISOLATE_Conclude(ENET_UNIT_0);

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Lxcp...\n");
    mea_lxcp_drv_Conclude(ENET_UNIT_0);
    

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Filter...\n");
    mea_drv_Conclude_Filter_Table(unit_i);
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude PortState...\n");
    mea_drv_Conclude_PortState(unit_i);

    if (mea_Action_Delete_ActionToCpu(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_Action_Delete_ActionToCpu \n",
                          __FUNCTION__);
       return ENET_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Action...\n");
    mea_Action_Conclude(unit_i);

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Queues...\n");
    if (enet_Conclude_Queue(unit_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - enet_Conclude_Queue failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude xPermission...\n");
    enet_Conclude_xPermission(ENET_UNIT_0);
    
	if(mea_drv_Conclude_Parser_Table(unit_i) != MEA_OK) {
	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		"%s - mea_drv_Conclude_Parser_Table failed \n",
		__FUNCTION__);
	return ENET_ERROR;
	}

	if(mea_drv_Conclude_IF_L2CP(unit_i) != MEA_OK) {
	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		"%s - mea_drv_Conclude_IF_L2CP failed \n",
		__FUNCTION__);
	return ENET_ERROR;
	}

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Global_parser...\n");
    if(mea_drv_Conclude_Global_parser(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_Global_parser failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Protocol_To_Priority_Mapping...\n");
    if(mea_drv_Protocol_To_Priority_Mapping_Conclude(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Protocol_To_Priority_Mapping_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }


    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude BM  block ...\n");
    mea_drv_Conclude_BM(unit_i);




    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude WRED...\n");
    if(mea_drv_Conclude_WRED_Table(unit_i)!=ENET_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Conclude_WRED_Table failed \n",
                          __FUNCTION__);
       return ENET_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude PacketGen...\n");
    if (mea_drv_Conclude_PacketGen(unit_i)!=ENET_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Conclude_PacketGen failed \n",
                          __FUNCTION__);
       return ENET_ERROR;
    }
#if MEA_CCM_ENABLE    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude CCM...\n");
    if(mea_drv_Conclude_CCM(unit_i)!=ENET_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Conclude_CCM failed \n",
                          __FUNCTION__);
       return ENET_ERROR;
    }
#endif

   
    if (mea_drv_Conclude_BFD(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_BFD failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    if(mea_drv_Conclude_BFD_SRC_PORT(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_BFD_SRC_PORT failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    if(mea_drv_Conclude_IPSec(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_IPSec failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    
    if(mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_Conclude(unit_i)!=ENET_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    
    if(mea_drv_Conclude_Utopia_RMON_Counters(unit_i)!=ENET_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_Utopia_RMON_Counters failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    if(mea_drv_Conclude_Virtual_Rmon_Counters(unit_i) != ENET_OK){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_Conclude_Virtual_Rmon_Counters failed \n",
        __FUNCTION__);
    return ENET_ERROR;
    }

#ifdef MEA_PLATFORM_TDM_SUPPORT
    
    if(mea_drv_Conclude_TDM_Interface(unit_i)!=ENET_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_TDM_Interface failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    
    if(mea_drv_Conclude_TDM_Ces(unit_i)!=ENET_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_TDM_Ces failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
#endif

    if(mea_drv_PreATM_parser_Conclude(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_PreATM_parser_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    if(mea_drv_Interface_Conclude(unit_i) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Interface_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude ShaperProfile...\n");
    if (enet_Conclude_Shaper_Profile(unit_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - enet_Conclude_ShaperProfile failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }


    if(mea_drv_Conclude_IF_Counters_HC_RMON(unit_i) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_IF_Counters_HC_RMON failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
 
    if(mea_drv_HC_Decomp_prof_Conclude(unit_i) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_Decomp_prof_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    if(mea_drv_HC_Comp_Conclude(unit_i) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_Comp_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
   

    if(mea_drv_HC_classifier_Conclude(unit_i) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_classifier_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }


   
    if(mea_drv_VP_MSTP_Conclude(unit_i) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_VP_MSTP_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    if(mea_drv_FWD_ENB_DA_prof_Conclude(unit_i) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_FWD_ENB_DA_prof_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

	if(mea_drv_Counclude_IngressPort_Table(unit_i) != MEA_OK){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		"%s - mea_drv_FWD_ENB_DA_prof_Conclude failed \n",
		__FUNCTION__);
		return ENET_ERROR;
	}

/******************************************************************/
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude DeviceInfo...\n");
    if (mea_DeviceInfo_Conclude(unit_i)!= ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_DeviceInfo_Conclude failed \n",
                          __FUNCTION__);
       return ENET_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Groups...\n");
    if (enet_Conclude_Group(unit_i) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_Conclude_Group failed \n",
                          __FUNCTION__);
       return ENET_ERROR;
    }

    if(mea_drv_Conclude_MemoryMap()!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_Conclude_memory failed \n",
                          __FUNCTION__);
       return ENET_ERROR;
    }
   

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Finish \n");

    return ENET_OK;
}                                 

                      

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*        <ENET_Get_DeviceInternalVersion>                                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Get_DeviceInternalVersion   (ENET_Unit_t  unit,
                                              ENET_Uint32* if_version_o,
                                              ENET_Uint32* bm_version_o)
{

    return MEA_API_Get_HwInternalVersion(if_version_o,bm_version_o);
}                                 

   


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*        <ENET_IsValid_Unit>                                                 */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Bool ENET_IsValid_Unit(ENET_Unit_t unit_i,
                            ENET_Bool   silent_i) 
{

    if (unit_i != ENET_UNIT_0) {
        if (!silent_i) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
                             "%s - unit (%d) is not valid\n", 
                             __FUNCTION__,unit_i); 
        }
        return ENET_FALSE;
    }


    return ENET_TRUE;
}






/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_API_delete_all_entities>                                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_all_entities(MEA_Unit_t unit) 
{

     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Delete all entities \n");
    /* Delete all services */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"     delete all services \n");
    if (MEA_DRV_Delete_all_services(unit,MEA_TRUE) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_DRV_delete_all_services failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "     delete all LxCPs \n");
    /* Delete all LxCP */
    if (MEA_API_Delete_all_LxCPs(unit) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_delete_all_LxCPs failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"     delete all filters \n");
    /* Delete all filters */
    if (MEA_API_Delete_all_filters(unit) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_delete_all_filters failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"     delete all forwarder \n");
    /* Delete all forwarder entries */
    if (MEA_API_DeleteAll_SE(unit) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Delete_all_forwarder_entries\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
    
    
   

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"     delete all Action \n");
    
    /* Delete all actions */
    if (MEA_API_DeleteAll_Action(unit) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_DeleteAll_Action failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"     delete all limiters \n");
    /* Delete all limiters */
    if (MEA_API_Delete_all_limiters(unit) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_delete_all_limiters failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
    }



   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Done\n");
    /* Return to caller */
    return MEA_OK;

}







#if 0
#ifdef MEA_LOCAL_MNG
MEA_Status MEA_Device_LOCAL_MNG_127_100(MEA_Unit_t unit_i)
{

    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;

    MEA_EHP_Info_dbt                      Service_editing_info[1];
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
    MEA_Service_t Service_id;

    MEA_IngressPort_Entry_dbt  IngressPort_Entry;
    MEA_EgressPort_Entry_dbt   EgressPort_Entry;

#ifndef MEA_OS_EPC_ZYNQ7045
    return MEA_OK;
#endif // DEBUG


    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "  CPU to Local MNG on port %d \n", MEA_OUTPORT_MNG_100);

    MEA_OS_memset(&Service_key, 0, sizeof(Service_key));
    MEA_OS_memset(&Service_data, 0, sizeof(Service_data));
    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    MEA_OS_memset(&Service_policer, 0, sizeof(Service_policer));
    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));
    MEA_OS_memset(&Service_editing_info, 0, sizeof(Service_editing_info));


    /*Create service from 127 to 107*/
    Service_key.ClassifierKEY = 0;
    Service_key.src_port = MEA_OUTPORT_MNG_127;


    Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;

    Service_key.sub_protocol_type = 0;
    Service_key.net_tag = 0xff000;
    Service_key.pri = 7; /* DC */
    Service_key.inner_netTag_from_valid = MEA_TRUE;
    Service_key.inner_netTag_from_value = 0xffffff;


    Service_data.tmId_disable = MEA_TRUE;

    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));

    Service_editing.ehp_info = Service_editing_info;


    Service_editing.num_of_entries = 1;



    if (MEA_ACTION_CPU_TO_100_2EX == MEA_TRUE){
        Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT_EXTRACT;
        Service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0x81000000;
        Service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0x81000000;
    }
    else{
        Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
        Service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0x81000000;
    }

    //Service_data.SRV_mode = MEA_SRV_MODE_EXTERNAL;
    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    MEA_SET_OUTPORT(&Service_outPorts, MEA_OUTPORT_MNG_100);
    Service_id = MEA_PLAT_GENERATE_NEW_ID;
    /* Create new service for the given . */
    if (MEA_API_Create_Service(MEA_UNIT_0,
        &Service_key,
        &Service_data,
        &Service_outPorts,
        &Service_policer,
        &Service_editing,
        &Service_id) != MEA_OK){


    }


    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    MEA_OS_memset(&Service_key, 0, sizeof(Service_key));
    MEA_OS_memset(&Service_data, 0, sizeof(Service_data));
    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    MEA_OS_memset(&Service_policer, 0, sizeof(Service_policer));
    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));
    MEA_OS_memset(&Service_editing_info, 0, sizeof(Service_editing_info));


    /*Create service from 127 to 107*/
    Service_key.ClassifierKEY = 0;
    Service_key.src_port = MEA_OUTPORT_MNG_100;



    Service_key.L2_protocol_type = MEA_PARSING_L2_KEY_Untagged;

    Service_key.sub_protocol_type = 0;
    Service_key.net_tag = 0xfff000;
    Service_key.pri = 7; /* DC */
    Service_key.inner_netTag_from_valid = MEA_TRUE;
    Service_key.inner_netTag_from_value = 0xffffff;

    Service_data.tmId_disable = MEA_TRUE;

    MEA_OS_memset(&Service_editing, 0, sizeof(Service_editing));

    Service_editing.ehp_info = Service_editing_info;


    Service_editing.num_of_entries = 1;



    if (MEA_ACTION_CPU_TO_127_2EX == MEA_TRUE){
        Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT_EXTRACT;
        Service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0x81000000;
        Service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0x81000000;
    }
    else{
        Service_editing.ehp_info[0].ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
        Service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0x81000000;
    }
    Service_data.SRV_mode = MEA_SRV_MODE_EXTERNAL;

    MEA_OS_memset(&Service_outPorts, 0, sizeof(Service_outPorts));
    MEA_SET_OUTPORT(&Service_outPorts, MEA_OUTPORT_MNG_127);
    Service_id = MEA_PLAT_GENERATE_NEW_ID;
    /* Create new service for the given . */
    if (MEA_API_Create_Service(MEA_UNIT_0,
        &Service_key,
        &Service_data,
        &Service_outPorts,
        &Service_policer,
        &Service_editing,
        &Service_id) != MEA_OK){


    }



    {
        MEA_Uint32 Globals1;
        Globals1 = mea_drv_get_if_global1(MEA_UNIT_0);

        Globals1 |= 1 << 3;
        MEA_API_WriteReg(MEA_UNIT_0, MEA_IF_GLOBAL_REG(1), Globals1, MEA_MODULE_IF);
    }
    /************************************************************************/
    /* Open port                                                            */
    /************************************************************************/

    MEA_API_Get_EgressPort_Entry(unit_i, MEA_OUTPORT_MNG_100, &EgressPort_Entry);
    EgressPort_Entry.tx_enable = MEA_TRUE;
    MEA_API_Set_EgressPort_Entry(unit_i, MEA_OUTPORT_MNG_100, &EgressPort_Entry);

    MEA_API_Get_EgressPort_Entry(unit_i, MEA_OUTPORT_MNG_127, &EgressPort_Entry);
    EgressPort_Entry.tx_enable = MEA_TRUE;
    MEA_API_Set_EgressPort_Entry(unit_i, MEA_OUTPORT_MNG_127, &EgressPort_Entry);


    MEA_API_Get_IngressPort_Entry(unit_i,
        MEA_OUTPORT_MNG_127,
        &IngressPort_Entry);
    IngressPort_Entry.rx_enable = MEA_TRUE;

    MEA_API_Set_IngressPort_Entry(unit_i,
        MEA_OUTPORT_MNG_127,
        &IngressPort_Entry);


    MEA_API_Get_IngressPort_Entry(unit_i,
        MEA_OUTPORT_MNG_100,
        &IngressPort_Entry);
    IngressPort_Entry.rx_enable = MEA_TRUE;

    MEA_API_Set_IngressPort_Entry(unit_i,
        MEA_OUTPORT_MNG_100,
        &IngressPort_Entry);




    return MEA_OK;

}
#endif
#endif



