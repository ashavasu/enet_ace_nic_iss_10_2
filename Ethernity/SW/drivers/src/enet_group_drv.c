/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/******************************************************************************
* 	Module Name:		 enet_group_drv.c	   									     
*																					 
*   Module Description:	     
*                            
*                         
*	                     
*                        
*                        
*																					 
*  Date of Creation:	 14/09/06													 
*																					 
*  Author Name:			 Alex Weis.													 
*******************************************************************************/



/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "ENET_platform_types.h"
#include "ENET_platform.h"
#include "enet_group_drv.h"
#include "enet_general_drv.h"
#include "mea_api.h"


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef ENET_Group_dbt enet_Group_Public_dbt;


typedef enum {
	ENET_GROUP_TYPE_LIST,
	ENET_GROUP_TYPE_LAST
} enet_Group_type_te;

typedef struct enet_GroupElement_list_node_dbs {
    struct enet_GroupElement_list_node_dbs *next;
    struct enet_GroupElement_list_node_dbs *prev;
    ENET_GroupElementId_t            id;
    ENET_Uint32                      data_length;
    void*                            data;
} enet_GroupElement_list_node_dbt;


typedef struct {
	  ENET_GroupId_t                       id;
	  enet_Group_type_te                   type;
	  ENET_Uint32                          num_of_elements;
	  union {
		  enet_GroupElement_list_node_dbt *list_head;
	  } elements;
} enet_Group_Private_dbt;

typedef struct {
  enet_Group_Public_dbt  public;
  enet_Group_Private_dbt private;
} enet_Group_dbt;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

static enet_Group_dbt* enet_group_container
                                   [ENET_PLAT_MAX_UNIT_ID+1]
								   [ENET_PLAT_MAX_GROUP_ID+1];

static ENET_Bool              enet_group_InitDone [ENET_PLAT_MAX_UNIT_ID+1];

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/





/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Check_Group>                                           */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Check_group (ENET_Unit_t        unit_i,
                              ENET_GroupId_t     id_i,
                              ENET_Group_dbt    *entry_i) 
{


	/* Check that name length not too long */
	if (MEA_OS_strlen(entry_i->name) >= sizeof(entry_i->name)) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - group name is too long \n",
			      		   __FUNCTION__);
	    return ENET_ERROR;
	}

	/* if no name specify then generate default name */
	if ((id_i != ENET_PLAT_GENERATE_NEW_ID) &&
        (MEA_OS_strcmp(ENET_PLAT_GENERATE_NEW_NAME,entry_i->name) == 0)) {  
#ifdef ARCH64
       MEA_OS_sprintf(entry_i->name,"Group %ld",id_i);
#else
       MEA_OS_sprintf(entry_i->name,"Group %d",id_i);
#endif	   
	}

    /* check if the name change , if it already use by another instance */ 
    if ((id_i == ENET_PLAT_GENERATE_NEW_ID) || 
        (enet_group_container[unit_i][id_i] == NULL) ||
        (MEA_OS_strcmp(entry_i->name,
	                   enet_group_container[unit_i][id_i]->public.name) != 0)) {

       ENET_Bool      found;
       ENET_GroupId_t id;

	   if (ENET_GetIDByName_Group (unit_i,entry_i->name,&id,&found) != ENET_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                      "%s - ENET_GetIDByName_group failed (name='%s')\n",
		        		   __FUNCTION__,entry_i->name);
	       return ENET_ERROR;
	   } 
       if ((found) && (id!=id_i)) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                     "%s - The group name ('%s') already use by group id %d (id=%d)\n",
			       		     __FUNCTION__,entry_i->name,id,id_i);
	      return ENET_ERROR;
	   }
	}
	

	
	/* Return to caller */
	return ENET_OK;
}



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_IsAllowedToDelete_Group>                               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_IsAllowedToDelete_Group (ENET_Unit_t    unit_i,
    ENET_GroupId_t id_i, ENET_Bool Force)
{

    if (Force == MEA_FALSE && enet_group_container[unit_i][id_i]->private.num_of_elements != 0) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - Not allowed to delete group with elements (id=%d)\n",
						   __FUNCTION__,id_i);
        /* we print the name in the log fmt parameter , 
           and not as argument to avoid problem in delay print of log 
           that will use pointer that is not valid any more */
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           enet_group_container[unit_i][id_i]->public.name);
		return ENET_ERROR;
	} 

	/* Return to caller */
	return ENET_OK;
}




/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Init_Group>                                            */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Init_Group(ENET_Unit_t unit_i)
{

    enet_group_InitDone[unit_i] = ENET_FALSE;

    /* Zeros the group Container  */
    MEA_OS_memset (&(enet_group_container[unit_i]),0,sizeof(enet_group_container[unit_i]));

    enet_group_InitDone[unit_i] = ENET_TRUE;

    /* Return to caller */
    return ENET_OK; 

}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Conclude_Group>                                        */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Conclude_Group(ENET_Unit_t unit_i)
{

	ENET_GroupId_t group_id , next_group_id;
	ENET_Bool     found; 
	

	/* Get the first group */
	if (ENET_GetFirst_Group  (unit_i,
		                      &group_id,
							  NULL, /* entry_o*/
							  &found) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_GetFirst_group failed \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}

	/* scan all groups and delete one by one */
	while(found) {

		next_group_id = group_id;
        if (ENET_GetNext_Group   (unit_i,
		                          &next_group_id,
		  					      NULL, /* entry_o */
							      &found) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - ENET_GetNext_group failed \n",
		 		  		       __FUNCTION__);
		    return ENET_ERROR;
		}

        if (ENET_Delete_Group   (unit_i,group_id,MEA_FALSE) != ENET_OK) { 
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - ENET_Delete_group failed \n",
		 		  		       __FUNCTION__);
		    return ENET_ERROR;
		}


		group_id = next_group_id;
	}

    /* Zeros the group Container  */
    MEA_OS_memset (&(enet_group_container[unit_i]),0,sizeof(enet_group_container[unit_i]));

	/* Indicate InitDone to false for this unit */
    enet_group_InitDone[unit_i] = ENET_FALSE;

    /* Return to caller */
    return ENET_OK; 

}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Conclude_GroupElement>                                 */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Conclude_GroupElement(ENET_Unit_t    unit_i,
								       ENET_GroupId_t group_id_i) 
{

	ENET_GroupElementId_t groupElement_id , next_groupElement_id;
	ENET_Bool     found;
	ENET_Uint32   len; 
	

	/* Get the first group */
	len=0;
	if (ENET_GetFirst_GroupElement( unit_i,
		                            group_id_i,
									&groupElement_id,
							        NULL, /* entry_o*/
									&len,
							        &found) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_GetFirst_groupElement failed \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}

	/* scan all groups and delete one by one */
	while(found) {

		next_groupElement_id = groupElement_id;
        len=0;
        if (ENET_GetNext_GroupElement   (unit_i,
			                             group_id_i,
		                                 &next_groupElement_id,
		  					             NULL, /* entry_o */
										 &len,
							             &found) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - ENET_GetNext_groupElement failed \n",
		 		  		       __FUNCTION__);
		    return ENET_ERROR;
		}

        if (ENET_Delete_GroupElement   (unit_i,group_id_i,groupElement_id) != ENET_OK) { 
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - ENET_Delete_groupElement failed \n",
		 		  		       __FUNCTION__);
		    return ENET_ERROR;
		}


		groupElement_id = next_groupElement_id;
	}


    /* Return to caller */
    return ENET_OK; 

}

/*----------------------------------------------------------------------------*/
/*             API functions implementation                                   */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Create_Group>                                          */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Create_Group    (ENET_Unit_t       unit_i,
                                  ENET_Group_dbt     *entry_i,
                     		      ENET_GroupId_t     *id_io) 
                                 
{


#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);


	/* check entry_i parameter */
	ENET_ASSERT_NULL(entry_i,"entry_i");

	/* check id_io    parameter */
	ENET_ASSERT_NULL(id_io,"id_io");
	if ((*id_io) != ENET_PLAT_GENERATE_NEW_ID) {
		
		if ((*id_io) >= ENET_NUM_OF_ELEMENTS(enet_group_container[0])) {
  		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                  "%s - group Id %d  for create is invalid\n",
							  __FUNCTION__,(*id_io));
		   return ENET_ERROR;
		}
        if (enet_group_container[unit_i][(*id_io)] != NULL)  {
  		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                  "%s - group Id %d  already exist\n",
							  __FUNCTION__,(*id_io));
		   return ENET_ERROR;
		}
    }

	/* check for entity attributes  */
	if (enet_Check_group(unit_i,
                        (*id_io),
  				        entry_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_Check_group failed (group id=%d)\n",
						   __FUNCTION__,*id_io);
		return ENET_ERROR;
	} 



#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */
	

	if ((*id_io) == ENET_PLAT_GENERATE_NEW_ID) {

		/* scan the container for the first id */
	    for ((*id_io) = 0;
	         (((*id_io) < ENET_NUM_OF_ELEMENTS(enet_group_container[0])) &&
		      (enet_group_container[unit_i][(*id_io)] != NULL          )  );
		     (*id_io)++);

	    /* check if we found id valid */
	    if ((*id_io) >= ENET_NUM_OF_ELEMENTS(enet_group_container[0])) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		  	                  "%s - Can not create new group - no more entries\n",
						   __FUNCTION__);
		   return ENET_ERROR;
		}
	}
	

	/* create the new instance of group */
	if ((enet_group_container[unit_i][(*id_io)] = 
		      (enet_Group_dbt*)
                      MEA_OS_malloc(sizeof(enet_Group_dbt))) == NULL) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		  	                  "%s - Can not create new group - no more memory space\n",
						   __FUNCTION__);
		   return ENET_ERROR;
	}
	MEA_OS_memset(enet_group_container[unit_i][*id_io],
		          0,
				  sizeof(enet_Group_dbt));

	/* set default to private attributes */
	enet_group_container[unit_i][(*id_io)]->private.id                 = *id_io;
	enet_group_container[unit_i][(*id_io)]->private.type               = ENET_GROUP_TYPE_LIST;
	enet_group_container[unit_i][(*id_io)]->private.num_of_elements    = 0;
	enet_group_container[unit_i][(*id_io)]->private.elements.list_head = NULL;
	
	/* update the group instance */
    if (ENET_Set_Group (unit_i,
                        (*id_io),
		 		        entry_i) != ENET_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		  	                  "%s - ENET_Set_group failed (group id = %d) \n",
						   __FUNCTION__,(*id_io));
	       MEA_OS_free(enet_group_container[unit_i][(*id_io)]);
	       enet_group_container[unit_i][(*id_io)] = NULL;
		   return ENET_ERROR;
	} 

	
	/* return to caller */
    return ENET_OK;
}
                                 

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Set_Group>                                             */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Set_Group       (ENET_Unit_t     unit_i,
								  ENET_GroupId_t  id_i,
                                  ENET_Group_dbt *entry_i)
                                 
{

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_GROUP_VALID(unit_i,id_i);

	/* check entry_i parameter */
	ENET_ASSERT_NULL(entry_i,"entry_i");

	/* check for entity attributes  */
	if (enet_Check_group(unit_i,
		                        id_i,
						        entry_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_Check_group failed (group id=%d)\n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	} 


#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


	/* set entity information */
	MEA_OS_memcpy(&(enet_group_container[unit_i][id_i]->public),
		           entry_i,
				   sizeof(*entry_i));

	/* Return to caller */
    return ENET_OK;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Get_Group>                                             */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Get_Group (ENET_Unit_t       unit_i,
                            ENET_GroupId_t     id_i,
                            ENET_Group_dbt     *entry_o)
{
 
#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_GROUP_VALID(unit_i,id_i);

	/* check entry_o parameter */
	ENET_ASSERT_NULL(entry_o,"entry_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

	/* get entity information */
	MEA_OS_memcpy(entry_o,
		           &(enet_group_container[unit_i][id_i]->public),
				   sizeof(*entry_o));

	/* Return to caller */
    return ENET_OK;
}                                 
                                 

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Delete_Group>                                          */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Delete_Group    (ENET_Unit_t    unit_i,
                                  ENET_GroupId_t id_i,ENET_Bool Force   )
{

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_GROUP_VALID(unit_i,id_i);


	/* check for entity attributes  */
	if (enet_IsAllowedToDelete_Group(unit_i,
        id_i, Force) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_IsAllowedToDelete_group failed (group id=%d)\n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	} 


#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


	/* delete the entity */
    MEA_OS_free(enet_group_container[unit_i][id_i]);
    enet_group_container[unit_i][id_i] = NULL;

	/* Return to caller */
    return ENET_OK;
}
	
                                 



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_GetFirst_Group>                                        */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_GetFirst_Group  (ENET_Unit_t     unit_i,
								  ENET_GroupId_t *id_o,
                                  ENET_Group_dbt *entry_o,
                                  ENET_Bool      *found_o)
{

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_io parameter */
	ENET_ASSERT_NULL(id_o,"id_o");

	/* check found_o parameter */
	ENET_ASSERT_NULL(found_o,"found_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    /* scan the container for the first id */
	for ((*id_o) = 0;
	     (((*id_o) < ENET_NUM_OF_ELEMENTS(enet_group_container[0])) &&
		  (enet_group_container[unit_i][(*id_o)] == NULL          )  );
		 (*id_o)++);

	/* check if we found id valid */
	if ((*id_o) < ENET_NUM_OF_ELEMENTS(enet_group_container[0])) {
		if (entry_o) {
			MEA_OS_memcpy (entry_o,
				           &(enet_group_container[unit_i][(*id_o)]->public),
						   sizeof(*entry_o));
		}
		*found_o = ENET_TRUE;
	} else {
		*found_o = ENET_FALSE;
	}

	/* Return to caller */
    return ENET_OK;
}                                 
                                 
/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_GetNext_Group>                                         */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_GetNext_Group   (ENET_Unit_t     unit_i,
                                  ENET_GroupId_t *id_io,
                                  ENET_Group_dbt *entry_o,
                                  ENET_Bool      *found_o)
{

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_io parameter */
	ENET_ASSERT_NULL(id_io,"id_io");

	/* check found_o parameter */
	ENET_ASSERT_NULL(found_o,"found_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    /* scan the container for the next id */
	for ((*id_io)++;
	     (((*id_io) < ENET_NUM_OF_ELEMENTS(enet_group_container[0])) &&
		  (enet_group_container[unit_i][(*id_io)]   == NULL        )  );
		 (*id_io)++);

	/* check if we found id valid */
	if ((*id_io) < ENET_NUM_OF_ELEMENTS(enet_group_container[0])) {
		if (entry_o) {
			MEA_OS_memcpy (entry_o,
				           &(enet_group_container[unit_i][(*id_io)]->public),
						   sizeof(*entry_o));
		}
		*found_o = ENET_TRUE;
	} else {
		*found_o = ENET_FALSE;
	}

	/* Return to caller */
    return ENET_OK;

}                                 
                                 
                                                             
/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_GetIDByName_Group>                                     */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_GetIDByName_Group (ENET_Unit_t     unit_i,
								    char           *name_i,								    ENET_GroupId_t *id_o,
                                    ENET_Bool      *found_o)
{

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check name_i parameter */
	ENET_ASSERT_NULL(name_i,"name_i");
	if (MEA_OS_strlen(name_i) >= sizeof(enet_group_container[0][0]->public.name)) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - name_i ('%s') length (%d) is too long \n",
						   __FUNCTION__,name_i,MEA_OS_strlen(name_i));
		return ENET_ERROR;
	}

	/* check id_o parameter */
	ENET_ASSERT_NULL(id_o,"id_o");

	/* check found_o parameter */
	ENET_ASSERT_NULL(found_o,"found_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    /* scan the container for the id with this name */
	for ((*id_o)=0 ;
	     (*id_o) < ENET_NUM_OF_ELEMENTS(enet_group_container[0]);
         (*id_o)++) {

	    if ((enet_group_container[unit_i][(*id_o)] != NULL) && 
		    (MEA_OS_strcmp(enet_group_container[unit_i][(*id_o)]->public.name,
		 	                name_i) == 0)) {
			*found_o = ENET_TRUE;
			return ENET_OK;
		}
	}

    *found_o = ENET_FALSE;

	/* Return to caller */
    return ENET_OK;
}     


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_IsValid_Group>                                         */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Bool  ENET_IsValid_Group(ENET_Unit_t    unit_i,
 							  ENET_GroupId_t id_i,
							  ENET_Bool      silent_i)
{

    if ((unit_i >= ENET_NUM_OF_ELEMENTS(enet_group_container   )) ||
		(id_i   >= ENET_NUM_OF_ELEMENTS(enet_group_container[0])) ||
        (enet_group_container[unit_i][id_i] == NULL             )  ) {

  	  if (!silent_i) {
  		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - Unit/group %d/%d is invalid\n",
							 __FUNCTION__,unit_i,id_i);
		  
      }

  	  return ENET_FALSE;  
    }
    
    return ENET_TRUE;
}



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Create_GroupElement>                                   */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Create_GroupElement (ENET_Unit_t            unit_i,
					 		          ENET_GroupId_t         groupId_i,
                                      void                  *entry_i,
							          ENET_Uint32            entry_len_i,
									  ENET_GroupElementId_t *id_io) {


    enet_Group_dbt* group;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check groupId_i parameter */
    ENET_ASSERT_GROUP_VALID(unit_i,groupId_i);

	/* check if entry_I is NULL and entry_len_i is not zero */
	if ((entry_i == NULL) && (entry_len_i != 0)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - entry_I == NULL , but entry_len_i (%d) != 0 (groupId=%d)\n",
						  __FUNCTION__,entry_len_i,groupId_i);
       return ENET_ERROR;
	}

	/* check if entry_i is not NULL and entry_len_i is zero */
	if ((entry_i != NULL) && (entry_len_i == 0)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - entry_i (0x%08x) != NULL , but entry_len_i == 0 (groupId=%d)\n",
						  __FUNCTION__,entry_i,groupId_i);
       return ENET_ERROR;
	}

	/* check id_io    parameter */
	ENET_ASSERT_NULL(id_io,"id_io");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

	
	group = enet_group_container[unit_i][groupId_i];
	if ((group->public.max_number_of_elements !=0) && 
		(group->public.max_number_of_elements <=group->private.num_of_elements)){
		 	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			 	                  "%s - max number of object already allocated (current=%d,max=%d) \n",
							      __FUNCTION__,group->private.num_of_elements,group->public.max_number_of_elements);
		 	   return ENET_ERROR;
	}

	

	switch (group->private.type) {
    case ENET_GROUP_TYPE_LIST :

	    if ((*id_io) != ENET_PLAT_GENERATE_NEW_ID) {

		
		   enet_GroupElement_list_node_dbt* ptr,*last_ptr;

		   ptr = group->private.elements.list_head;
		   last_ptr = NULL;

		   while ((ptr) && (ptr->id < (*id_io))) {
		  	   last_ptr = ptr;
			   ptr = ptr->next;
		   }
		   if (ptr) {

			   if (ptr->id == (*id_io)) {
			 	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					               "%s - GroupElement id %d already exist in group id %d \n",
								   __FUNCTION__,(*id_io),groupId_i);
				   return ENET_ERROR;
			   }
		   }


		   ptr = (enet_GroupElement_list_node_dbt*)
                         MEA_OS_malloc(sizeof(*ptr));
		   if (ptr == NULL) {
		 	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			 	                  "%s - Alloc of new Group Element failed \n",
							      __FUNCTION__);
		 	   return ENET_ERROR;
		   }
		   ptr->id = *id_io;
		   if (entry_len_i==0) {
		 	   ptr->data = NULL;
               ptr->data_length = 0;
		   } else {
			   ptr->data = (void*)MEA_OS_malloc(entry_len_i);
		       if (ptr->data == NULL) {
			      ptr->data_length = 0;
			      MEA_OS_free(ptr);
			      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			  	                     "%s - Alloc of new Group Element Data failed \n",
					  		         __FUNCTION__);
			      return ENET_ERROR;
			   }		
			   ptr->data_length = entry_len_i;
			   MEA_OS_memcpy(ptr->data,entry_i,ptr->data_length);
		   }

		   /* Add the element to the list */
		   if (last_ptr) { /* need to add the element in the middle or at end */
		      ptr->prev = last_ptr;
		      ptr->next = last_ptr->next;
		      if (ptr->next) {
			     (ptr->next)->prev = ptr;
			  }
		      last_ptr->next = ptr;
		   } else { /* need to add the element in the begin of the list */
		      ptr->prev = NULL;
              ptr->next = group->private.elements.list_head;
		      if (ptr->next) {
			     (ptr->next)->prev = ptr;
			  }
              group->private.elements.list_head = ptr;
		   }
           group->private.num_of_elements++;

		} else { /* (*id_io) == ENET_PLAT_GENERATE_NEW_ID) */

		   enet_GroupElement_list_node_dbt* ptr,*last_ptr;
		   ptr = group->private.elements.list_head;
		   last_ptr = NULL;
		   while (ptr)  {
			   if (  ((last_ptr!=NULL)&& (last_ptr->id +1 != ptr->id)) ||
					 ((last_ptr==NULL)&& (ptr->id != group->public.first_groupElementId))  )
				   break;				   
			   last_ptr = ptr;
			   ptr = ptr->next;
		   }

		   /* TBD check max number of elements */

		   ptr = (enet_GroupElement_list_node_dbt*)
                         MEA_OS_malloc(sizeof(*ptr));
		   if (ptr == NULL) {
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			  	                  "%s - Alloc of new Group Element failed \n",
							      __FUNCTION__);
			   return ENET_ERROR;
		   }
		   if (last_ptr) {
			   (*id_io) = last_ptr->id + 1;
		   } else {
			   (*id_io) = group->public.first_groupElementId; 
		   }
		   ptr->id = *id_io;
		   if (entry_len_i==0) {
		 	   ptr->data = NULL;
               ptr->data_length = 0;
		   } else {
			   ptr->data = (void*)MEA_OS_malloc(entry_len_i);
		       if (ptr->data == NULL) {
			       ptr->data_length = 0;
			       MEA_OS_free(ptr);
			       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			  	                       "%s - Alloc of new Group Element Data failed \n",
						  	           __FUNCTION__);
			       return ENET_ERROR;
			   }		
			   ptr->data_length = entry_len_i;
			   MEA_OS_memcpy(ptr->data,entry_i,ptr->data_length);
		   }

		   /* Add the element to the list */
		   if (last_ptr) { /* need to add the element at end or midle of list */
		      ptr->prev = last_ptr;
		      ptr->next = last_ptr->next;
		      if (ptr->next) {
			     (ptr->next)->prev = ptr;
			  }
		      last_ptr->next = ptr;
		   } else { /* need to add the element in the begin of the list */
		      ptr->prev = NULL;
              ptr->next = group->private.elements.list_head;
		      if (ptr->next) {
			     (ptr->next)->prev = ptr;
			  }
              group->private.elements.list_head = ptr;
		   }
           group->private.num_of_elements++;
		}
		return ENET_OK;
	    break;

	case ENET_GROUP_TYPE_LAST:
        MEA_OS_LOG_logMsg(ENET_ERROR,
			               "%s - Invalid group type (%d) (groupId_i=%d)\n",
						   __FUNCTION__,group->private.type,groupId_i);
		return ENET_ERROR;
		break;
	}


	MEA_OS_LOG_logMsg(ENET_ERROR,
		               "%s - Invalid end of this function \n",__FUNCTION__);
	return ENET_ERROR;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Create_GroupElement_withInRange>                       */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Create_GroupElement_withInRange (ENET_Unit_t            unit_i,
                                                  ENET_GroupId_t         groupId_i,
                                                  void                  *entry_i,
                                                  ENET_Uint32            entry_len_i,
                                                  ENET_GroupElementId_t  startRangeId_i,
                                                  ENET_GroupElementId_t  endRangeId_i,
                                                  ENET_GroupElementId_t *id_io) {


    enet_Group_dbt* group;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

    /* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

    /* check groupId_i parameter */
    ENET_ASSERT_GROUP_VALID(unit_i,groupId_i);

    /* check id_io    parameter */
    ENET_ASSERT_NULL(id_io,"id_io");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


    group = enet_group_container[unit_i][groupId_i];

    if (group->private.type == ENET_GROUP_TYPE_LIST) {

        if ((*id_io) != ENET_PLAT_GENERATE_NEW_ID) {

            if (((*id_io) < startRangeId_i) ||
                ((*id_io) > endRangeId_i  )  ) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - GroupElementId %d not in the valid range %d..%d \n",
                                  __FUNCTION__,(*id_io),startRangeId_i,endRangeId_i);
                return MEA_ERROR;
            }


        } else { /* (*id_io) == ENET_PLAT_GENERATE_NEW_ID) */
            
            enet_GroupElement_list_node_dbt* ptr,*last_ptr;
            ENET_GroupElementId_t  startRangeId;

            if (startRangeId_i > group->public.first_groupElementId) {
                startRangeId = startRangeId_i;
            } else {
                startRangeId = group->public.first_groupElementId;
            }

            ptr = group->private.elements.list_head;
            if ((ptr != NULL) && 
                (ptr->id > startRangeId)) {
                (*id_io) = startRangeId;
            } else {
                if (ptr == NULL) {
                   (*id_io) = startRangeId;
                } else {
                    last_ptr = ptr;
                    ptr = ptr->next;
                    while (ptr)  {
                        if (last_ptr->id >= endRangeId_i) {
                            break;
                        }
                        if ((ptr->id > startRangeId    ) &&
                            (ptr->id > last_ptr->id + 1)  ) {
                           break;
                        }
                        last_ptr = ptr;
                        ptr = ptr->next;
                    }

                    if (last_ptr->id >= endRangeId_i) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s - No free entries in this group \n",
                                          __FUNCTION__);
                        return MEA_ERROR;
                    } else {
                        if (last_ptr->id < startRangeId) {
                            (*id_io) = startRangeId;
                        } else {
                            (*id_io) = last_ptr->id + 1;
                        }
                    }
                }
            }
        }
    }

    return ENET_Create_GroupElement(unit_i,
                                    groupId_i,
                                    entry_i,
                                    entry_len_i,
                                    id_io);
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Delete_GroupElement>                                   */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Delete_GroupElement (ENET_Unit_t           unit_i,
							          ENET_GroupId_t        groupId_i,
									  ENET_GroupElementId_t id_i) {


    enet_Group_dbt* group;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check groupId_i parameter */
    ENET_ASSERT_GROUP_VALID(unit_i,groupId_i);


#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


	group = enet_group_container[unit_i][groupId_i];

	
	switch (group->private.type) {
	case ENET_GROUP_TYPE_LIST:
        {
			enet_GroupElement_list_node_dbt* ptr;
		    ptr = group->private.elements.list_head;
			while ((ptr) && (ptr->id != id_i)) {
				ptr = ptr->next;
			}
			if (ptr == NULL) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                              "%s - Unit/group/groupElement %d/%d/%d is invalid\n",
			           	   		  __FUNCTION__,unit_i,groupId_i,id_i);
			   return ENET_ERROR;
			}
			if (ptr->data  != NULL) {
				MEA_OS_free(ptr->data);
				ptr->data=NULL;
				ptr->data_length=0;
			}
			if (ptr->next) {
				(ptr->next)->prev = ptr->prev;
			}
			if (ptr->prev) {
				(ptr->prev)->next = ptr->next;
			} else {
                group->private.elements.list_head = ptr->next;
			}
			MEA_OS_free(ptr);
			group->private.num_of_elements--;
			return ENET_OK;
		}
		break;
	case ENET_GROUP_TYPE_LAST:
        MEA_OS_LOG_logMsg(ENET_ERROR,
			               "%s - Invalid group type (%d) (groupId_i=%d)\n",
						   __FUNCTION__,group->private.type,groupId_i);
		return ENET_ERROR;
		break;
	}

	MEA_OS_LOG_logMsg(ENET_ERROR,
		               "%s - Invalid end of this function \n",__FUNCTION__);
	return ENET_ERROR;
}



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Set_GroupElement>                                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Set_GroupElement (ENET_Unit_t	          unit_i,
							       ENET_GroupId_t         groupId_i,
							       ENET_GroupElementId_t  id_i,
							       void                  *entry_i,
								   ENET_Uint32            entry_len_i) {


    enet_Group_dbt* group;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check groupId_i parameter */
    ENET_ASSERT_GROUP_VALID(unit_i,groupId_i);

	/* check entry_i parameter */
	ENET_ASSERT_NULL(entry_i,"entry_i");

	/* check entry_length_i parameter */
	if (entry_len_i == 0) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - entry_len_i is zero (groupId=%d,id=%d)\n",
						  __FUNCTION__,groupId_i,id_i);
       return ENET_ERROR;
	}



#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


	group = enet_group_container[unit_i][groupId_i];

	
	switch (group->private.type) {
	case ENET_GROUP_TYPE_LIST:
        {
			enet_GroupElement_list_node_dbt* ptr;
		    ptr = group->private.elements.list_head;
			while ((ptr) && (ptr->id != id_i)) {
				ptr = ptr->next;
			}
			if (ptr == NULL) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                              "%s - Unit/group/groupElement %d/%d/%d is invalid\n",
			           	   		  __FUNCTION__,unit_i,groupId_i,id_i);
			   return ENET_ERROR;
			}
			if (ptr->data_length != entry_len_i) {
                MEA_OS_LOG_logMsg(ENET_ERROR,
			                       "%s - groupElelement %d length (%d) != entry_len (%d) "
								   "(groupId_i=%d)\n",
						           __FUNCTION__,
								   ptr->id,
								   ptr->data_length,
								   entry_len_i,
								   groupId_i);
			    return ENET_ERROR;
			}
			MEA_OS_memcpy(ptr->data,entry_i,entry_len_i);
			return ENET_OK;
		}
		break;
	case ENET_GROUP_TYPE_LAST:
        MEA_OS_LOG_logMsg(ENET_ERROR,
			               "%s - Invalid group type (%d) (groupId_i=%d)\n",
						   __FUNCTION__,group->private.type,groupId_i);
		return ENET_ERROR;
		break;
	}

	MEA_OS_LOG_logMsg(ENET_ERROR,
		               "%s - Invalid end of this function \n",__FUNCTION__);
	return ENET_ERROR;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Get_GroupElement>                                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Get_GroupElement (ENET_Unit_t	          unit_i,
							       ENET_GroupId_t         groupId_i,
							       ENET_GroupElementId_t  id_i,
							       void                  *entry_o,
								   ENET_Uint32            *entry_len_io) {



    enet_Group_dbt* group;
    

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check groupId_i parameter */
    ENET_ASSERT_GROUP_VALID(unit_i,groupId_i);

	/* check entry_o parameter */
	ENET_ASSERT_NULL(entry_o,"entry_o");

	/* check entry_length_i parameter */
    ENET_ASSERT_NULL(entry_len_io, "entry_len_io");

	if ((*entry_len_io) == 0) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - entry_len_i is zero (groupId=%d,id=%d)\n",
						  __FUNCTION__,groupId_i,id_i);
       return ENET_ERROR;
	}


#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


	group = enet_group_container[unit_i][groupId_i];

	
	switch (group->private.type) {
	case ENET_GROUP_TYPE_LIST:
        {
			enet_GroupElement_list_node_dbt* ptr;
		    ptr = group->private.elements.list_head;
			while ((ptr) && (ptr->id != id_i)) {
				ptr = ptr->next;
			}
			if (ptr == NULL) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                              "%s - Unit/group/groupElement %d/%d/%d is invalid\n",
			           	   		  __FUNCTION__,unit_i,groupId_i,id_i);
			   return ENET_ERROR;
			}
			/*-------------*/
			if (ptr->data_length > (*entry_len_io)) {
                MEA_OS_LOG_logMsg(ENET_ERROR,
			                       "%s - groupElelement %d length (%d) > entry_len (%d) "
								   "(groupId_i=%d)\n",
						           __FUNCTION__,
								   ptr->id,
								   ptr->data_length,
								   (*entry_len_io),
								   groupId_i);
			    return ENET_ERROR;
			}
			(*entry_len_io)=ptr->data_length;
			MEA_OS_memcpy(entry_o,ptr->data,(*entry_len_io));
			return ENET_OK;
		}
		break;
	case ENET_GROUP_TYPE_LAST:
        MEA_OS_LOG_logMsg(ENET_ERROR,
			               "%s - Invalid group type (%d) (groupId_i=%d)\n",
						   __FUNCTION__,group->private.type,groupId_i);
		return ENET_ERROR;
		break;
	}

	MEA_OS_LOG_logMsg(ENET_ERROR,
		               "%s - Invalid end of this function \n",__FUNCTION__);
	return ENET_ERROR;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_GetFirst_GroupElement>                                 */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_GetFirst_GroupElement (ENET_Unit_t	         unit_i,
						                ENET_GroupId_t          groupId_i,
							            ENET_GroupElementId_t  *id_o,
							            void                   *entry_o,
						                ENET_Uint32             *entry_len_io,
									    ENET_Bool		        *found_o) {

    enet_Group_dbt* group;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check groupId_i parameter */
    ENET_ASSERT_GROUP_VALID(unit_i,groupId_i);

	/* check id_o parameter */
	ENET_ASSERT_NULL(id_o,"id_o");

    ENET_ASSERT_NULL(entry_len_io, "entry_len_io");


	/* check if entry_o is NULL and entry_len_i is not zero */
	if ((entry_o == NULL) && ((*entry_len_io) != 0)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - entry_o == NULL , but entry_len_i (%d) != 0 (groupId=%d)\n",
						  __FUNCTION__,(*entry_len_io),groupId_i);
       return ENET_ERROR;
	}

	/* check if entry_o is not NULL and entry_len_i is zero */
	if ((entry_o != NULL) && ((*entry_len_io) == 0)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - entry_o (0x%08x) != NULL , but entry_len_i == 0 (groupId=%d)\n",
						  __FUNCTION__,entry_o,groupId_i);
       return ENET_ERROR;
	}

	/* check found_o parameter */
	ENET_ASSERT_NULL(found_o,"found_o");



#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


	group = enet_group_container[unit_i][groupId_i];

	
	switch (group->private.type) {
	case ENET_GROUP_TYPE_LIST:
        {
			enet_GroupElement_list_node_dbt* ptr;
		    ptr = group->private.elements.list_head;
                                              
                             if (ptr == NULL) {
                               *found_o = ENET_FALSE;
                              return ENET_OK;
                             }
			if (entry_o != NULL)  {
				/**/
				if (ptr->data_length > (*entry_len_io)) {
                    MEA_OS_LOG_logMsg(ENET_ERROR,
			                       "%s - groupElelement %d length (%d) > entry_len (%d) "
								   "(groupId_i=%d)\n",
						           __FUNCTION__,
								   ptr->id,
								   ptr->data_length,
								   (*entry_len_io),
								   groupId_i);
				    return ENET_ERROR;
				}
				(*entry_len_io)=ptr->data_length;
				MEA_OS_memcpy(entry_o,ptr->data,(*entry_len_io));
			} 
		    *id_o = ptr->id;
			*found_o = ENET_TRUE;
			return ENET_OK;
		}
		break;
	case ENET_GROUP_TYPE_LAST:
        MEA_OS_LOG_logMsg(ENET_ERROR,
			               "%s - Invalid group type (%d) (groupId_i=%d)\n",
						   __FUNCTION__,group->private.type,groupId_i);
		return ENET_ERROR;
		break;
	}

	*found_o = ENET_FALSE;
	return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_GetNext_GroupElement>                                  */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_GetNext_GroupElement  (ENET_Unit_t	            unit_i,
						                ENET_GroupId_t          groupId_i,
							            ENET_GroupElementId_t  *id_io,
							            void                   *entry_o,
						                ENET_Uint32             *entry_len_io,
										ENET_Bool		       *found_o) {


	enet_Group_dbt* group;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check groupId_i parameter */
    ENET_ASSERT_GROUP_VALID(unit_i,groupId_i);

	/* check id_o parameter */
	ENET_ASSERT_NULL(id_io,"id_io");

    ENET_ASSERT_NULL(entry_len_io, "entry_len_io");

	/* check if entry_o is NULL and entry_len_i is not zero */
	if ((entry_o == NULL) && ((*entry_len_io) != 0)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - entry_o == NULL , but entry_len_i (%d) != 0 (groupId=%d)\n",
						  __FUNCTION__,(*entry_len_io),groupId_i);
       return ENET_ERROR;
	}

	/* check if entry_o is not NULL and entry_len_i is zero */
	if ((entry_o != NULL) && ((*entry_len_io) == 0)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - entry_o (0x%08x) != NULL , but entry_len_i == 0 (groupId=%d)\n",
						  __FUNCTION__,entry_o,groupId_i);
       return ENET_ERROR;
	}

	/* check found_o parameter */
	ENET_ASSERT_NULL(found_o,"found_o");



#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


	group = enet_group_container[unit_i][groupId_i];

	
	switch (group->private.type) {
	case ENET_GROUP_TYPE_LIST:
        {
			enet_GroupElement_list_node_dbt* ptr;
		    ptr = group->private.elements.list_head;
			while ((ptr) && (ptr->id <= (*id_io))) {
				ptr = ptr->next;
			}
			if (ptr == NULL) {
				*found_o = ENET_FALSE;
				return ENET_OK;
			}

			if (entry_o != NULL)  {
				/**/
				if (ptr->data_length > (*entry_len_io)) {
                    MEA_OS_LOG_logMsg(ENET_ERROR,
			                       "%s - groupElelement %d length (%d) > entry_len (%d) "
								   "(groupId_i=%d)\n",
						           __FUNCTION__,
								   ptr->id,
								   ptr->data_length,
								   (*entry_len_io),
								   groupId_i);
				    return ENET_ERROR;
				}
				(*entry_len_io)=ptr->data_length;
				MEA_OS_memcpy(entry_o,ptr->data,(*entry_len_io));
			} 
		    *id_io = ptr->id;
			*found_o = ENET_TRUE;
			return ENET_OK;
		}
		break;
	case ENET_GROUP_TYPE_LAST:
        MEA_OS_LOG_logMsg(ENET_ERROR,
			               "%s - Invalid group type (%d) (groupId_i=%d)\n",
						   __FUNCTION__,group->private.type,groupId_i);
		return ENET_ERROR;
		break;
	}

	*found_o = ENET_FALSE;
	return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_IsValid_GroupElement>                                  */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Bool  ENET_IsValid_GroupElement(ENET_Unit_t           unit_i,
							         ENET_GroupId_t        groupId_i,
							         ENET_GroupElementId_t id_i,
									 ENET_Bool             silent_i) {

    enet_Group_dbt* group;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check groupId_i parameter */
    ENET_ASSERT_GROUP_VALID(unit_i,groupId_i);


#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


	group = enet_group_container[unit_i][groupId_i];

	
	switch (group->private.type) {
	case ENET_GROUP_TYPE_LIST:
        {
			enet_GroupElement_list_node_dbt* ptr;
		    ptr = group->private.elements.list_head;
		    while (ptr) {
               if (ptr->id == id_i) {
			  	   return ENET_TRUE;
			   }
			   ptr = ptr->next;
			} 
		}
		break;
	case ENET_GROUP_TYPE_LAST:
		if (!silent_i) {
            MEA_OS_LOG_logMsg(ENET_ERROR,
			                   "%s - Invalid group type (%d) (groupId_i=%d)\n",
				  		       __FUNCTION__,group->private.type,groupId_i);
		}
		return ENET_FALSE;
		break;
	}
    if (!silent_i) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - Unit/group/groupElement %d/%d/%d is invalid\n",
				   		   __FUNCTION__,unit_i,groupId_i,id_i);
    }
	return ENET_FALSE;
}

