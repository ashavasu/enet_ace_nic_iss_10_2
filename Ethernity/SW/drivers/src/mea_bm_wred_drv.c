/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_bm_drv.h"
#include "mea_drv_common.h"
#include "mea_init.h"
#include "mea_bm_wred_drv.h"


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct {
    //MEA_Uint32                  pad; // don't remove this
    MEA_Uint32                  valid;
    MEA_Uint32                  number_of_owners;
    MEA_WRED_Profile_Data_dbt   data;
    //MEA_Uint32                  pad1; // don't remove this
} mea_drv_wred_profile_dbt;

typedef struct {
    mea_drv_wred_profile_dbt  *profileTable;
} mea_drv_wred_acm_dbt;

typedef struct {
    MEA_Uint32                number_of_acms;
    MEA_Uint32                number_of_profiles;
    mea_drv_wred_acm_dbt     *acmTable;
} mea_drv_wred_info_dbt;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
mea_drv_wred_info_dbt mea_drv_wred_info;

static MEA_Uint32  mea_drv_ECN_prof3_0;

static MEA_Uint32  mea_drv_ECN_prof7_4;

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_set_WRED_UpdateHwEntry>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_set_WRED_UpdateHwEntry(MEA_WRED_Entry_Data_dbt *data_i) 
{

 	MEA_API_WriteReg(MEA_UNIT_0,
                     MEA_BM_IA_WRDATA0,
                     data_i->drop_probability,
                     MEA_MODULE_BM);
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_set_WRED_UpdateHw>                                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_set_WRED_UpdateHw(MEA_WRED_Entry_Key_dbt  *key_i,
                                       MEA_WRED_Entry_Data_dbt  *data_i)
{

	MEA_ind_write_t ind_write;
    MEA_Uint32 index;

    if (!MEA_WRED_PROFILES_HW_SUPPORT)
        return MEA_OK;

#if 0 // TODO LIOR 
    2^15 x 8 = 2 ^ 18 = 2 ^ 8 x 1K = 256K 
    2 - acm_mode
    3 - profile 
    3 - priority 
    7 - normalize 
#endif

    /* Update output parameter */
    index = 0;
    index <<= 2;
    index += (key_i->profile_key.acm_mode   & 0x00000003);
    index <<= 3;
    index += (key_i->profile_key.profile_id & 0x00000007);
    index <<= 3;
    index += (key_i->priority               & 0x00000007);
    index <<= 7;
    index += (key_i->normalize_aqs          & 0x0000007f);

    /* build the ind_write structure */
	ind_write.tableType		= ENET_BM_TBL_TYP_PRI_DROP_PROP;
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.tableOffset   = index;
    ind_write.funcParam1 = (MEA_funcParam)data_i;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_set_WRED_UpdateHwEntry;



    /* Write to Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect for tableType=%d offset=%d failed"
                          " (profile_key=(acm_mode=%d,profile_id=%d),priority=%d,normalize_aqs=%d)\n",
                          __FUNCTION__,
                          ind_write.tableType,
                          ind_write.tableOffset,
                          key_i->profile_key.acm_mode,
                          key_i->profile_key.profile_id,
                          key_i->priority,
                          key_i->normalize_aqs);
        return MEA_ERROR;
	}

	/* Return to caller */
	return MEA_OK;
}




/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


MEA_Status mea_drv_Conclude_WRED_Table(MEA_Unit_t unit_i)
{
    MEA_WRED_Profile_Key_dbt key;
    mea_drv_wred_acm_dbt     *wred_acm;
//    mea_drv_wred_profile_dbt *wred_profile;

    if(mea_drv_wred_info.acmTable){

        MEA_OS_memset(&key,0,sizeof(key));
        for (key.acm_mode=0,wred_acm=&(mea_drv_wred_info.acmTable[0]);
             key.acm_mode<mea_drv_wred_info.number_of_acms;
             key.acm_mode++,wred_acm++) {
            if(wred_acm->profileTable) {
#if 0
                for(key.profile_id=0,wred_profile=&(wred_acm->profileTable[0]);
                    key.profile_id<mea_drv_wred_info.number_of_profiles;
                    key.profile_id++,wred_profile++){

                    if ((wred_profile->valid                ) && 
                        (key.acm_mode                   == 0) && 
                        (key.profile_id                 == 0) &&
                        (wred_profile->number_of_owners == 2)  ) {
                        wred_profile->valid=MEA_FALSE;
                    }
                    if (wred_profile->valid) {
                        if (wred_profile->number_of_owners > 1) {
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                              "%s - profile %d/%d is still has owners %d\n",
                                              __FUNCTION__,
                                              key.acm_mode,key.profile_id,
                                              wred_profile->number_of_owners);
                            return MEA_ERROR;
                        }
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s - profile %d/%d is still valid \n",
                                          __FUNCTION__,
                                          key.acm_mode,key.profile_id);
                        return MEA_ERROR;
                    }
                }
#endif
                MEA_OS_free(wred_acm->profileTable);
                wred_acm->profileTable=NULL;
            }
        }
        MEA_OS_free(mea_drv_wred_info.acmTable);
        mea_drv_wred_info.acmTable=NULL;
    }
    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_WRED_Table>                                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_WRED_Table(MEA_Unit_t unit_i)
{
    MEA_AcmMode_t             acm_mode;
    mea_drv_wred_acm_dbt     *wred_acm;
    mea_drv_wred_profile_dbt *wred_profile;
    MEA_Uint32                size;
    MEA_WRED_Entry_Key_dbt    entry_key;
    MEA_WRED_Entry_Key_dbt    entry_key_from;
    MEA_WRED_Entry_Key_dbt    entry_key_to;

#ifdef MEA_WRED_GRAPH_SUPPORT
    MEA_NormalizeAqs_t        normalize_aqs;
#else  /* MEA_WRED_GRAPH_SUPPORT */
    MEA_WRED_Entry_Data_dbt   entry_data;
#endif /* MEA_WRED_GRAPH_SUPPORT */



    /* Init mea_drv_wred_info */
    MEA_OS_memset(&mea_drv_wred_info,0,sizeof(mea_drv_wred_info));
    mea_drv_wred_info.number_of_acms = MEA_WRED_NUM_OF_ACM_MODE;
    mea_drv_wred_info.number_of_profiles = MEA_WRED_NUM_OF_PROFILES;

    /* Init mea_drv_wred_info.acmTable */
    size = (mea_drv_wred_info.number_of_acms * sizeof(mea_drv_wred_acm_dbt));
    mea_drv_wred_info.acmTable = (mea_drv_wred_acm_dbt*)MEA_OS_malloc(size);
    if (mea_drv_wred_info.acmTable == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate mea_drv_wred_info.acmTable failed (size=%d)\n",
                          __FUNCTION__,size);
        return MEA_ERROR;
    }
    MEA_OS_memset(&mea_drv_wred_info.acmTable[0],0,size); 


    /* for each acm_mode init mea_drv_wred_info.acmTable[acm_mode] */
    for (acm_mode=0,wred_acm=&(mea_drv_wred_info.acmTable[0]);
         acm_mode<mea_drv_wred_info.number_of_acms;
         acm_mode++,wred_acm++) {
        size = (mea_drv_wred_info.number_of_profiles * sizeof(mea_drv_wred_profile_dbt));
        wred_acm->profileTable = (mea_drv_wred_profile_dbt*)MEA_OS_malloc(size);
        if (wred_acm->profileTable == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate wred_acm->profileTable failed (acm_mode=%d,size=%d)\n",
                              __FUNCTION__,acm_mode,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&wred_acm->profileTable[0],0,size); 
    }


    /* Create default profile */
    MEA_OS_memset(&entry_key,0,sizeof(entry_key));
    entry_key.profile_key.acm_mode   = 0;
    entry_key.profile_key.profile_id = 0;
    wred_acm=&(mea_drv_wred_info.acmTable[entry_key.profile_key.acm_mode]);
    wred_profile=&(wred_acm->profileTable[entry_key.profile_key.profile_id]);
    MEA_OS_memset(wred_profile,0,sizeof(mea_drv_wred_profile_dbt));

    wred_profile->valid            = MEA_TRUE;
    wred_profile->data.ECN_data.ECN_enable = MEA_FALSE;
    wred_profile->number_of_owners = 1; 
    wred_profile->number_of_owners++; /* To not allowed to delete acm=0,priority=0 */
    MEA_OS_memset(&entry_data,0,sizeof(entry_data));

#ifdef MEA_WRED_GRAPH_SUPPORT

#warning Need to init the WRED table HW , but now it take too much time 
#if 0 /* T.B.D - Need to init the HW WRED table - now it take too much time */
    
    for(profileId=0;profileId<MEA_WRED_NUM_OF_PROFILES;profileId++){
  
       for(normalize_aqs=0;
           normalize_aqs<=MEA_WRED_NORMALIZE_AQS_MAX_VALUE;
           normalize_aqs++) {
           if (mea_set_WRED_UpdateHw(profileId,
                                     (MEA_NormalizeAQS_t)normalize_aqs,
                                     &(MEA_WRED_Table[profileId][normalize_aqs])
                                     ) != MEA_OK) {
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - Update HW failed (profileId=%d , normalize_aqs=%d)\n",
                                __FUNCTION__,profileId,normalize_aqs);
              return MEA_ERROR;
           }
                
        }

    }
#endif

#else /* MEA_WRED_GRAPH_SUPPORT */


    
    entry_data.drop_probability = MEA_DROP_PROB_TBL_DEF_VAL;
#if 0
	for(profileId=0;profileId<MEA_WRED_NUM_OF_PROFILES;profileId++){

                
        entry.drop_probability = MEA_DROP_PROB_TBL_DEF_VAL;
        
        if ((entry.drop_probability != 0) &&
		    (MEA_API_Set_WRED_EntryRange(unit_i,
                                        profileId,
                                        0,
                                        MEA_WRED_NORMALIZE_AQS_MAX_VALUE-1,
                                        &entry) != MEA_OK)) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_WRED_EntryRange (%d..%d) for profileId=%d  failed\n",
                              __FUNCTION__,
                              0,
                              MEA_WRED_NORMALIZE_AQS_MAX_VALUE-1,
                              profileId);
			return MEA_ERROR; 
		}

        entry.drop_probability = MEA_WRED_DROP_PROBABILITY_MAX_VALUE;

		if (MEA_API_Set_WRED_EntryRange(unit_i,
                                        profileId,
                                        MEA_WRED_NORMALIZE_AQS_MAX_VALUE,
                                        MEA_WRED_NORMALIZE_AQS_MAX_VALUE,
                                        &entry) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_WRED_EntryRange (%d..%d) for profileId=%d  failed\n",
                              __FUNCTION__,
                              MEA_WRED_NORMALIZE_AQS_MAX_VALUE,
                              MEA_WRED_NORMALIZE_AQS_MAX_VALUE,
                              profileId);
			return MEA_ERROR; 
		}

	}
#endif
    
    /*----------------------*/
    /*  from 0 to 60 add zero*/
    /*  from 61 t0 127 add MAX*/
    /*----------------------*/
    entry_data.drop_probability = 0;
    
 

    for (entry_key.priority=0;entry_key.priority<MEA_WRED_NUM_OF_PRI;entry_key.priority++) {

        MEA_OS_memcpy(&entry_key_from,&entry_key,sizeof(entry_key_from));
        MEA_OS_memcpy(&entry_key_to  ,&entry_key,sizeof(entry_key_to  ));
        entry_key_from.normalize_aqs = 0;
        entry_key_to.normalize_aqs   = 60;
        MEA_OS_memset(&entry_data,0,sizeof(entry_data));
        entry_data.drop_probability = 0;
        if (MEA_API_Set_WRED_EntryRange(unit_i,
                                        &entry_key_from,
                                        &entry_key_to,
                                        &entry_data) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_WRED_EntryRange (%d..%d) failed (acm_mode=%d,profile_id=%d,priority=%d)\n",
                              __FUNCTION__,
                              entry_key_from.normalize_aqs,
                              entry_key_to.normalize_aqs,
                              entry_key_from.profile_key.acm_mode,
                              entry_key_from.profile_key.profile_id,
                              entry_key.priority);
            return MEA_ERROR; 
        }


        entry_key_from.normalize_aqs = 61;
        entry_key_to.normalize_aqs   = MEA_WRED_NORMALIZE_AQS_MAX_VALUE;
        MEA_OS_memset(&entry_data,0,sizeof(entry_data));
        entry_data.drop_probability = MEA_WRED_DROP_PROBABILITY_MAX_VALUE;

        if (MEA_API_Set_WRED_EntryRange(unit_i,
                                        &entry_key_from,
                                        &entry_key_to,
                                        &entry_data) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_WRED_EntryRange (%d..%d) failed (acm_mode=%d,profile_id=%d,priority=%d)\n",
                              __FUNCTION__,
                              entry_key_from.normalize_aqs,
                              entry_key_to.normalize_aqs,
                              entry_key_from.profile_key.acm_mode,
                              entry_key_from.profile_key.profile_id,
                              entry_key.priority);
            return MEA_ERROR; 
        }
    }
#endif  /* MEA_WRED_GRAPH_SUPPORT */
     
   

    mea_drv_ECN_prof3_0=0;
    mea_drv_ECN_prof7_4 =0;

    if(MEA_ECN_SUPPORT){
        MEA_API_WriteReg(unit_i,ENET_BM_ECN_3_TO_0_PROF,mea_drv_ECN_prof3_0,MEA_MODULE_BM);
        MEA_API_WriteReg(unit_i,ENET_BM_ECN_7_TO_4_PROF,mea_drv_ECN_prof7_4,MEA_MODULE_BM);
    }




    /* return to caller */
	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_WRED_Table>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_WRED_Table(MEA_Unit_t unit_i)
{

    mea_drv_wred_acm_dbt     *wred_acm;
    mea_drv_wred_profile_dbt *wred_profile;
    MEA_WRED_Entry_Key_dbt    entry_key;
    MEA_WRED_Entry_Data_dbt   *entry_data;

    MEA_OS_memset(&entry_key,0,sizeof(entry_key));

    for (entry_key.profile_key.acm_mode=0,wred_acm=&(mea_drv_wred_info.acmTable[0]); 
         entry_key.profile_key.acm_mode<mea_drv_wred_info.number_of_acms;
         entry_key.profile_key.acm_mode++,wred_acm++) {
        
        for (entry_key.profile_key.profile_id=0,wred_profile=&(wred_acm->profileTable[0]);
             entry_key.profile_key.profile_id<mea_drv_wred_info.number_of_profiles;
             entry_key.profile_key.profile_id++,wred_profile++) {

            if (!wred_profile->valid) {
                continue;
            }

            for (entry_key.priority = 0; entry_key.priority<MEA_WRED_NUM_OF_PRI;entry_key.priority++) {
                 for(entry_key.normalize_aqs=0;
                     entry_key.normalize_aqs<=MEA_WRED_NORMALIZE_AQS_MAX_VALUE;
                     entry_key.normalize_aqs++) {
                   entry_data=&(wred_profile->data.priorityTable[entry_key.priority].wredTable[entry_key.normalize_aqs]);
                   if (mea_set_WRED_UpdateHw(&entry_key,
                                             entry_data) != MEA_OK) {
                      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                        "%s - Update HW failed \n",
                                        __FUNCTION__);
                      return MEA_ERROR;
                   }
                        
                }//normalize_aqs
            }//priority
        }// profile_id
    }// acm 
    
    if(MEA_ECN_SUPPORT){
        MEA_API_WriteReg(unit_i,ENET_BM_ECN_3_TO_0_PROF,mea_drv_ECN_prof3_0,MEA_MODULE_BM);
   
        MEA_API_WriteReg(unit_i,ENET_BM_ECN_7_TO_4_PROF,mea_drv_ECN_prof7_4,MEA_MODULE_BM);
    }


    /* return to caller */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_AddOwner_WRED_Profile>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_AddOwner_WRED_Profile(MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i)
{

    mea_drv_wred_acm_dbt     *wred_acm;
    mea_drv_wred_profile_dbt *wred_profile;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for key_i */
    if (key_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check for key_i->acm_mode */
    if (key_i->acm_mode >= mea_drv_wred_info.number_of_acms) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->acm_mode %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          mea_drv_wred_info.number_of_acms);
        return MEA_ERROR;
    }

    /* Check for key_i->profile_id */
    if (key_i->profile_id >= mea_drv_wred_info.number_of_profiles) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->profile_id %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->profile_id,
                          mea_drv_wred_info.number_of_profiles);
        return MEA_ERROR;
    }

#endif

    /* Locate on the profile */
    wred_acm     = &(mea_drv_wred_info.acmTable[key_i->acm_mode]);
    wred_profile = &(wred_acm->profileTable[key_i->profile_id]);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    /* Check if profile exist */
    if (!wred_profile->valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - profile %d/%d not exist \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          key_i->profile_id);
       return MEA_ERROR;
    }

#endif

    /* Update database */
    wred_profile->number_of_owners++;

    /* return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_DelOwner_WRED_Profile>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_DelOwner_WRED_Profile(MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i)
{

    mea_drv_wred_acm_dbt     *wred_acm;
    mea_drv_wred_profile_dbt *wred_profile;
    MEA_Uint32                limit;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for key_i */
    if (key_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check for key_i->acm_mode */
    if (key_i->acm_mode >= mea_drv_wred_info.number_of_acms) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->acm_mode %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          mea_drv_wred_info.number_of_acms);
        return MEA_ERROR;
    }

    /* Check for key_i->profile_id */
    if (key_i->profile_id >= mea_drv_wred_info.number_of_profiles) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->profile_id %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->profile_id,
                          mea_drv_wred_info.number_of_profiles);
        return MEA_ERROR;
    }

#endif

    /* Locate on the profile */
    wred_acm     = &(mea_drv_wred_info.acmTable[key_i->acm_mode]);
    wred_profile = &(wred_acm->profileTable[key_i->profile_id]);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    /* Check if profile exist */
    if (!wred_profile->valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - profile %d/%d not exist \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          key_i->profile_id);
       return MEA_ERROR;
    }

    /* Check if we have owners */
    limit=1;
    if ((key_i->acm_mode==0) && (key_i->profile_id==0)) {
        limit++;
    }
    if (wred_profile->number_of_owners <= limit) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - wred_profile->number_of_owners %d <= %d (profile %d/%d)\n",
                          __FUNCTION__,
                          wred_profile->number_of_owners,
                          limit,
                          key_i->acm_mode,
                          key_i->profile_id);
       return MEA_ERROR;
    }

#endif

    /* Update database */
    wred_profile->number_of_owners--;

    /* return to caller */
    return MEA_OK;
}


MEA_Status mea_set_ECN_UpdateHw(MEA_Unit_t    unit_i, MEA_Uint32 Id ,MEA_ECN_Entry_Data_dbt *ECN_data )
{
MEA_Uint32 ConfigValue;


         ConfigValue=0;
         
         if(ECN_data->ECN_enable == MEA_TRUE){
           ConfigValue=0x80;
         
             
             if (ECN_data->ECN_maxTH >=64){
                ConfigValue |=  (((ECN_data->ECN_maxTH-64)/4) & 0x7);
             }
             if (ECN_data->ECN_minTH != 0){
                 ConfigValue |=  (((ECN_data->ECN_minTH)/4) & 0xf)<<3;
             }
         }else {
             ConfigValue=0;
         }


    if(Id <=3){
        mea_drv_ECN_prof3_0 &= ~(0xff<<((Id)*8));
        mea_drv_ECN_prof3_0 |= ConfigValue<<((Id)*8); 
        MEA_API_WriteReg(unit_i,ENET_BM_ECN_3_TO_0_PROF,mea_drv_ECN_prof3_0,MEA_MODULE_BM);
    }
    if(Id>=4 && Id <=7 ){
        mea_drv_ECN_prof7_4 &= ~(0xff<<((Id-4)*8));
        mea_drv_ECN_prof7_4 |= ConfigValue<<((Id-4)*8);
       MEA_API_WriteReg(unit_i,ENET_BM_ECN_7_TO_4_PROF,mea_drv_ECN_prof7_4,MEA_MODULE_BM);
    }




    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Create_WRED_Profile>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_WRED_Profile  (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i,
                                         MEA_WRED_Profile_Data_dbt *data_i)
{
    mea_drv_wred_acm_dbt     *wred_acm;
    mea_drv_wred_profile_dbt *wred_profile;
    MEA_WRED_Entry_Key_dbt    entry_key;
    MEA_WRED_Entry_Data_dbt  *entry_data;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for key_i */
    if (key_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check for key_i->acm_mode */
    if (key_i->acm_mode >= mea_drv_wred_info.number_of_acms) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->acm_mode %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          mea_drv_wred_info.number_of_acms);
        return MEA_ERROR;
    }

    /* Check for key_i->profile_id */
    if (key_i->profile_id >= mea_drv_wred_info.number_of_profiles) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->profile_id %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->profile_id,
                          mea_drv_wred_info.number_of_profiles);
        return MEA_ERROR;
    }

#endif

    /* Locate on the profile */
    wred_acm     = &(mea_drv_wred_info.acmTable[key_i->acm_mode]);
    wred_profile = &(wred_acm->profileTable[key_i->profile_id]);

    

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    /* Check if profile already exist */
    if (wred_profile->valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - profile %d/%d already exist \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          key_i->profile_id);
       return MEA_ERROR;
    }

    if (MEA_ECN_SUPPORT){
        if (data_i){
            if (data_i->ECN_data.ECN_enable == MEA_TRUE){
                if (!(data_i->ECN_data.ECN_maxTH >= 64 && data_i->ECN_data.ECN_maxTH <= 92)){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - THE ECN  maxTH need to set between 64..92  \n",
                        __FUNCTION__);
                    return MEA_ERROR;

                }
                if (!(data_i->ECN_data.ECN_minTH < 60)){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - THE ECN  minTH need e between 64..92  \n",
                        __FUNCTION__);
                    return MEA_ERROR;

                }
            }
        }
    }

    if (data_i) {
        /* Check for data_i->drop_prbability */
        MEA_OS_memset(&entry_key,0,sizeof(entry_key));
        MEA_OS_memcpy(&entry_key.profile_key,key_i,sizeof(entry_key.profile_key)); 
        for (entry_key.priority=0;
             entry_key.priority<MEA_WRED_NUM_OF_PRI;
             entry_key.priority++) {
            for (entry_key.normalize_aqs=0,entry_data=&(data_i->priorityTable[entry_key.priority].wredTable[0]);
                 entry_key.normalize_aqs< MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1;
                 entry_key.normalize_aqs++,entry_data++) {
                if (entry_data->drop_probability > MEA_WRED_DROP_PROBABILITY_MAX_VALUE) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - data_i->drop_probability %d > max value %d "
                                      "(acm_mode=%d,profile_id=%d,priority=%d,normalize_aqs=%d\n",
                                      __FUNCTION__,
                                      entry_data->drop_probability,
                                      MEA_WRED_DROP_PROBABILITY_MAX_VALUE,
                                      entry_key.profile_key.acm_mode,
                                      entry_key.profile_key.profile_id,
                                      entry_key.priority,
                                      entry_key.normalize_aqs);
                    return MEA_ERROR;
                }
            }
        }
    }

#endif

    /* Update database */
    MEA_OS_memset(wred_profile,0,sizeof(*wred_profile));
    if (data_i) {
        MEA_OS_memcpy(&(wred_profile->data),data_i,sizeof(wred_profile->data));
    }
    wred_profile->number_of_owners = 1;
    wred_profile->valid = 1;

    /* Update the HW */
    MEA_OS_memcpy(&entry_key.profile_key,key_i,sizeof(entry_key.profile_key)); 
    for (entry_key.priority=0;
         entry_key.priority<MEA_WRED_NUM_OF_PRI;
         entry_key.priority++) {
        for (entry_key.normalize_aqs=0,entry_data=&(wred_profile->data.priorityTable[entry_key.priority].wredTable[0]);
             entry_key.normalize_aqs< MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1;
             entry_key.normalize_aqs++,entry_data++) {
            if (!data_i) {
               if (entry_key.normalize_aqs >= 61) {
                 entry_data->drop_probability = MEA_WRED_DROP_PROBABILITY_MAX_VALUE;
               }
            }
            if (mea_set_WRED_UpdateHw(&entry_key,entry_data) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Update HW failed (acm_mode=%d,profile_id=%d , priority=%d,normalize_aqs=%d)\n",
                                  __FUNCTION__,
                                  entry_key.profile_key.acm_mode,
                                  entry_key.profile_key.profile_id,
                                  entry_key.priority,
                                  entry_key.normalize_aqs);
                wred_profile->valid = 0;
                // T.B.D Rollback 
                return MEA_ERROR;
            }
        }
    }
    if(MEA_ECN_SUPPORT){
        if (data_i){
            if (mea_set_ECN_UpdateHw(unit_i, entry_key.profile_key.profile_id, &data_i->ECN_data) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "set_ECN_UpdateHw id %d ", entry_key.profile_key.profile_id);

                return MEA_ERROR;
            }
        }
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Delete_WRED_Profile>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_WRED_Profile  (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i)
{
    mea_drv_wred_acm_dbt     *wred_acm;
    mea_drv_wred_profile_dbt *wred_profile;
    MEA_WRED_Entry_Key_dbt    entry_key;
    MEA_WRED_Entry_Data_dbt   entry_data;
    MEA_ECN_Entry_Data_dbt   ECN_data;
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for key_i */
    if (key_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check for key_i->acm_mode */
    if (key_i->acm_mode >= mea_drv_wred_info.number_of_acms) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->acm_mode %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          mea_drv_wred_info.number_of_acms);
        return MEA_ERROR;
    }

    /* Check for key_i->profile_id */
    if (key_i->profile_id >= mea_drv_wred_info.number_of_profiles) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->profile_id %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->profile_id,
                          mea_drv_wred_info.number_of_profiles);
        return MEA_ERROR;
    }

#endif

    /* Locate on the profile */
    wred_acm     = &(mea_drv_wred_info.acmTable[key_i->acm_mode]);
    wred_profile = &(wred_acm->profileTable[key_i->profile_id]);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    /* Check if profile exist */
    if (!wred_profile->valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - profile %d/%d not exist \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          key_i->profile_id);
       return MEA_ERROR;
    }

    /* Check if profile exist */
    if (wred_profile->number_of_owners != 1) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - wred_profile->number_of_owners %d != 1 (profile %d/%d)\n",
                          __FUNCTION__,
                          wred_profile->number_of_owners,
                          key_i->acm_mode,
                          key_i->profile_id);
       return MEA_ERROR;
    }



#endif

    /* Update the HW */
    MEA_OS_memset(&entry_data,0,sizeof(entry_data));
    MEA_OS_memset(&entry_key,0,sizeof(entry_key));
    MEA_OS_memcpy(&entry_key.profile_key,key_i,sizeof(entry_key.profile_key)); 
    for (entry_key.priority=0;
         entry_key.priority<MEA_WRED_NUM_OF_PRI;
         entry_key.priority++) {
        for (entry_key.normalize_aqs=0;
             entry_key.normalize_aqs< MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1;
             entry_key.normalize_aqs++) {
            if (mea_set_WRED_UpdateHw(&entry_key,&entry_data) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Update HW failed (acm_mode=%d,profile_id=%d , priority=%d,normalize_aqs=%d)\n",
                                  __FUNCTION__,
                                  entry_key.profile_key.acm_mode,
                                  entry_key.profile_key.profile_id,
                                  entry_key.priority,
                                  entry_key.normalize_aqs);
                // T.B.D Rollback 
                return MEA_ERROR;
            }
        }
    }
    
    if(MEA_ECN_SUPPORT){
            MEA_OS_memset(&ECN_data,0,sizeof(ECN_data));
        if (mea_set_ECN_UpdateHw(unit_i, entry_key.profile_key.profile_id,&ECN_data) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"set_ECN_UpdateHw id %d ",entry_key.profile_key.profile_id);

            return MEA_ERROR;
        }
    }

    /* Update database */
    MEA_OS_memset(wred_profile,0,sizeof(*wred_profile));

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_WRED_Profile>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_WRED_Profile     (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i,
                                         MEA_WRED_Profile_Data_dbt *data_i)
{
    mea_drv_wred_acm_dbt     *wred_acm;
    mea_drv_wred_profile_dbt *wred_profile;
    MEA_WRED_Entry_Key_dbt    entry_key;
    MEA_WRED_Entry_Data_dbt  *entry_data;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for key_i */
    if (key_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check for key_i->acm_mode */
    if (key_i->acm_mode >= mea_drv_wred_info.number_of_acms) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->acm_mode %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          mea_drv_wred_info.number_of_acms);
        return MEA_ERROR;
    }

    /* Check for key_i->profile_id */
    if (key_i->profile_id >= mea_drv_wred_info.number_of_profiles) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->profile_id %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->profile_id,
                          mea_drv_wred_info.number_of_profiles);
        return MEA_ERROR;
    }


#endif

    /* Locate on the profile */
    wred_acm     = &(mea_drv_wred_info.acmTable[key_i->acm_mode]);
    wred_profile = &(wred_acm->profileTable[key_i->profile_id]);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check if profile exist */
    if (!wred_profile->valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - profile %d/%d not exist \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          key_i->profile_id);
       return MEA_ERROR;
    }

    /* Check for data_i */
    if (data_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - data_i == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check for data_i->drop_prbability */
    MEA_OS_memset(&entry_key,0,sizeof(entry_key));
    MEA_OS_memcpy(&entry_key.profile_key,key_i,sizeof(entry_key.profile_key)); 
    for (entry_key.priority=0;
         entry_key.priority<MEA_WRED_NUM_OF_PRI;
         entry_key.priority++) {
        for (entry_key.normalize_aqs=0,entry_data=&(data_i->priorityTable[entry_key.priority].wredTable[0]);
             entry_key.normalize_aqs< MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1;
             entry_key.normalize_aqs++,entry_data++) {
            if (entry_data->drop_probability > MEA_WRED_DROP_PROBABILITY_MAX_VALUE) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - data_i->drop_probability %d > max value %d "
                                  "(acm_mode=%d,profile_id=%d,priority=%d,normalize_aqs=%d\n",
                                  __FUNCTION__,
                                  entry_data->drop_probability,
                                  MEA_WRED_DROP_PROBABILITY_MAX_VALUE,
                                  entry_key.profile_key.acm_mode,
                                  entry_key.profile_key.profile_id,
                                  entry_key.priority,
                                  entry_key.normalize_aqs);
                return MEA_ERROR;
            }
        }
    }

#endif

#if 0
    if(!MEA_ECN_SUPPORT){
        if(data_i->ECN_data.ECN_enable == MEA_TRUE) 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - THE ECN  not support on this version \n",
            __FUNCTION__ );
        return MEA_ERROR;
    }
#endif
    if(MEA_ECN_SUPPORT){
        if(data_i->ECN_data.ECN_enable == MEA_TRUE){
            if(!(data_i->ECN_data.ECN_maxTH >=64 && data_i->ECN_data.ECN_maxTH <=92)){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - THE ECN  maxTH need to set between 64..92  \n",
                    __FUNCTION__ );
                return MEA_ERROR;

            }
            if(!(data_i->ECN_data.ECN_minTH  <= 60 )){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - THE ECN  minTH need e between 64..92  \n",
                    __FUNCTION__ );
                return MEA_ERROR;

            }
        }

    }





    /* Update the HW */
    MEA_OS_memset(&entry_key,0,sizeof(entry_key));
    MEA_OS_memcpy(&entry_key.profile_key,key_i,sizeof(entry_key.profile_key)); 
    for (entry_key.priority=0;
         entry_key.priority<MEA_WRED_NUM_OF_PRI;
         entry_key.priority++) {
        for (entry_key.normalize_aqs=0,entry_data=&(data_i->priorityTable[entry_key.priority].wredTable[0]);
             entry_key.normalize_aqs< MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1;
             entry_key.normalize_aqs++,entry_data++) {
            
            if(wred_profile->data.priorityTable[entry_key.priority].wredTable[entry_key.normalize_aqs].drop_probability != entry_data->drop_probability){


            if (mea_set_WRED_UpdateHw(&entry_key,entry_data) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Update HW failed (acm_mode=%d,profile_id=%d , priority=%d,normalize_aqs=%d)\n",
                                  __FUNCTION__,
                                  entry_key.profile_key.acm_mode,
                                  entry_key.profile_key.profile_id,
                                  entry_key.priority,
                                  entry_key.normalize_aqs);
                // T.B.D Rollback 
                return MEA_ERROR;
            }
            }
        }
    }
    if(MEA_ECN_SUPPORT){
        if( MEA_OS_memcmp(&wred_profile->data.ECN_data ,&data_i->ECN_data,sizeof(wred_profile->data.ECN_data))!= 0 ){
            if (mea_set_ECN_UpdateHw(unit_i ,entry_key.profile_key.profile_id,&data_i->ECN_data) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"set_ECN_UpdateHw id %d ",entry_key.profile_key.profile_id);

                return MEA_ERROR;
            }
        }
    }


    /* Update database */
    MEA_OS_memcpy(&(wred_profile->data),data_i,sizeof(wred_profile->data));

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_WRED_Profile>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_WRED_Profile     (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i,
                                         MEA_WRED_Profile_Data_dbt *data_o)
{
    mea_drv_wred_acm_dbt    * wred_acm;
    mea_drv_wred_profile_dbt* wred_profile;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for key_i */
    if (key_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check for key_i->acm_mode */
    if (key_i->acm_mode >= mea_drv_wred_info.number_of_acms) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->acm_mode %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          mea_drv_wred_info.number_of_acms);
        return MEA_ERROR;
    }

    /* Check for key_i->profile_id */
    if (key_i->profile_id >= mea_drv_wred_info.number_of_profiles) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i->profile_id %d >= max value %d \n",
                          __FUNCTION__,
                          key_i->profile_id,
                          mea_drv_wred_info.number_of_profiles);
        return MEA_ERROR;
    }

#endif

    /* Locate on the profile */
    wred_acm     = &(mea_drv_wred_info.acmTable[key_i->acm_mode]);
    wred_profile = &(wred_acm->profileTable[key_i->profile_id]);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check if profile exist */
    if (!wred_profile->valid) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - profile %d/%d not exist \n",
                          __FUNCTION__,
                          key_i->acm_mode,
                          key_i->profile_id);
       return MEA_ERROR;
    }
#endif

    /* Update output paramter */
    if (data_o) {
        MEA_OS_memcpy(data_o,&(wred_profile->data),sizeof(*data_o));
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_GetFirst_WRED_Profile>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_WRED_Profile(MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_o,
                                         MEA_WRED_Profile_Data_dbt *data_o,
                                         MEA_Bool                  *found_o)
{
    mea_drv_wred_acm_dbt    * wred_acm;
    mea_drv_wred_profile_dbt* wred_profile=NULL;
    MEA_WRED_Profile_Key_dbt  key;
    MEA_Bool                  found;

    /* Search for the first valid profile */
    for (found=MEA_FALSE,key.acm_mode=0,wred_acm=&(mea_drv_wred_info.acmTable[0]); 
         key.acm_mode<mea_drv_wred_info.number_of_acms;
         key.acm_mode++,wred_acm++) {
        
        for (key.profile_id=0,wred_profile=&(wred_acm->profileTable[0]);
             key.profile_id<mea_drv_wred_info.number_of_profiles;
             key.profile_id++,wred_profile++) {
            
            if (wred_profile->valid) {
                found=MEA_TRUE;
                break;
            }
        }
        if (found) {
            break;
        }
    }

    /* Update output paramters */
    if (found_o) {
        *found_o = found;
    }
    if (found) {
        if (key_o) {
            MEA_OS_memcpy(key_o,&key,sizeof(*key_o));
        }
        if (data_o) {
            MEA_OS_memcpy(data_o,&(wred_profile->data),sizeof(*data_o));    
        }
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_GetNext_WRED_Profile>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_WRED_Profile (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_io,
                                         MEA_WRED_Profile_Data_dbt *data_o,
                                         MEA_Bool                  *found_o)
{
    mea_drv_wred_acm_dbt    * wred_acm;
    mea_drv_wred_profile_dbt* wred_profile=NULL;
    MEA_WRED_Profile_Key_dbt  key;
    MEA_Bool                  found;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for key_io */
    if (key_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_io == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

#endif


    /* Search for the next valid profile */
    MEA_OS_memcpy(&key,key_io,sizeof(key));
    key.profile_id++;
    for (found=MEA_FALSE,wred_acm=&(mea_drv_wred_info.acmTable[key.acm_mode]); 
         key.acm_mode<mea_drv_wred_info.number_of_acms;
         key.acm_mode++,wred_acm++) {
        
        for (wred_profile=&(wred_acm->profileTable[key.profile_id]);
             key.profile_id<mea_drv_wred_info.number_of_profiles;
             key.profile_id++,wred_profile++) {
            
            if (wred_profile->valid) {
                found=MEA_TRUE;
                break;
            }
        }
        if (found) {
            break;
        }
        key.profile_id = 0;
    }

    /* Update output paramters */
    if (found_o) {
        *found_o = found;
    }
    if (found) {
        MEA_OS_memcpy(key_io,&key,sizeof(*key_io));
        if (data_o) {
            MEA_OS_memcpy(data_o,&(wred_profile->data),sizeof(*data_o));    
        }
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_IsValid_WRED_Profile>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool MEA_API_IsValid_WRED_Profile (MEA_Unit_t                 unit_i,
                                       MEA_WRED_Profile_Key_dbt  *key_i,
                                       MEA_Bool                   silent_i)
{
    mea_drv_wred_acm_dbt    * wred_acm;
    mea_drv_wred_profile_dbt* wred_profile;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (key_i == NULL) {
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - key_i == NULL \n",
                              __FUNCTION__);
        }
        return MEA_FALSE;
    }

    /* Check for key_i->acm_mode */
    if (key_i->acm_mode >= mea_drv_wred_info.number_of_acms) {
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - key_i->acm_mode %d >= max value %d \n",
                              __FUNCTION__,
                              key_i->acm_mode,
                              mea_drv_wred_info.number_of_acms);
        }
        return MEA_FALSE;
    }

    /* Check for key_i->profile_id */
    if (key_i->profile_id >= mea_drv_wred_info.number_of_profiles) {
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - key_i->profile_id %d >= max value %d \n",
                              __FUNCTION__,
                              key_i->profile_id,
                              mea_drv_wred_info.number_of_profiles);
        }
        return MEA_FALSE;
    }

#endif

    /* Locate on the profile */
    wred_acm     = &(mea_drv_wred_info.acmTable[key_i->acm_mode]);
    wred_profile = &(wred_acm->profileTable[key_i->profile_id]);

    /* Check if profile exist */
    if (!wred_profile->valid) {
        if (!silent_i) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - profile %d/%d not exist \n",
                              __FUNCTION__,
                              key_i->acm_mode,
                              key_i->profile_id);
        }
        return MEA_FALSE;
    }

    /* Return to caller */
    return MEA_TRUE;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_WRED_Entry>                                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_WRED_Entry(MEA_Unit_t                     unit,
                                  MEA_WRED_Entry_Key_dbt        *key_i,
                                  MEA_WRED_Entry_Data_dbt       *data_o)
{

    mea_drv_wred_acm_dbt    * wred_acm;
    mea_drv_wred_profile_dbt* wred_profile;
    MEA_WRED_Entry_Data_dbt* entry_data;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (key_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for key_i->profile_key.acm_mode */
    if (key_i->profile_key.acm_mode >= mea_drv_wred_info.number_of_acms) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->profile_key.acm_mode %d >= max value %d \n",
                         __FUNCTION__,
                         key_i->profile_key.acm_mode,
                         mea_drv_wred_info.number_of_acms);
       return MEA_ERROR;
    }

    /* Check for key_i->profile_key.profile_id */
    if (key_i->profile_key.profile_id >= mea_drv_wred_info.number_of_profiles) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->profile_key.profile_id %d >= max value %d \n",
                         __FUNCTION__,
                         key_i->profile_key.profile_id,
                         mea_drv_wred_info.number_of_profiles);
       return MEA_ERROR;
    }

    /* Check for key_i->normalize_aqs */
    if (key_i->priority >= MEA_WRED_NUM_OF_PRI) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->priority %d >= max value %d \n",
                         __FUNCTION__,
                         key_i->priority,
                         MEA_WRED_NUM_OF_PRI);
       return MEA_ERROR;
    }

    /* Check for key_i->normalize_aqs */
    if (key_i->normalize_aqs >= MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->normalize_aqs %d >= max value %d \n",
                         __FUNCTION__,
                         key_i->normalize_aqs,
                         MEA_WRED_NORMALIZE_AQS_MAX_VALUE);
       return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Locate on the entry */
    wred_acm     = &(mea_drv_wred_info.acmTable[key_i->profile_key.acm_mode]);
    wred_profile = &(wred_acm->profileTable[key_i->profile_key.profile_id]);
    entry_data   = &(wred_profile->data.priorityTable[key_i->priority].wredTable[key_i->normalize_aqs]);
    
    /* Update output parameter */
    if (data_o) {
        MEA_OS_memcpy(data_o,        
                      entry_data,
                      sizeof(*data_o));
    }

    /* Return to Caller */
	return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_WRED_Entry>                                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_WRED_Entry(MEA_Unit_t                     unit,
                                  MEA_WRED_Entry_Key_dbt        *key_i,
                                  MEA_WRED_Entry_Data_dbt       *data_i)
{


    mea_drv_wred_acm_dbt    * wred_acm;
    mea_drv_wred_profile_dbt* wred_profile;
    MEA_WRED_Entry_Data_dbt* entry_data;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (key_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for key_i->profile_key.acm_mode */
    if (key_i->profile_key.acm_mode >= mea_drv_wred_info.number_of_acms) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->profile_key.acm_mode %d >= max value %d \n",
                         __FUNCTION__,
                         key_i->profile_key.acm_mode,
                         mea_drv_wred_info.number_of_acms);
       return MEA_ERROR;
    }

    /* Check for key_i->profile_key.profile_id */
    if (key_i->profile_key.profile_id >= mea_drv_wred_info.number_of_profiles) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->profile_key.profile_id %d >= max value %d \n",
                         __FUNCTION__,
                         key_i->profile_key.profile_id,
                         mea_drv_wred_info.number_of_profiles);
       return MEA_ERROR;
    }

    /* Check for key_i->normalize_aqs */
    if (key_i->priority >= MEA_WRED_NUM_OF_PRI) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->priority %d >= max value %d \n",
                         __FUNCTION__,
                         key_i->priority,
                         MEA_WRED_NUM_OF_PRI);
       return MEA_ERROR;
    }

    /* Check for key_i->normalize_aqs */
    if (key_i->normalize_aqs >= MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i->normalize_aqs %d >= max value %d \n",
                         __FUNCTION__,
                         key_i->normalize_aqs,
                         MEA_WRED_NORMALIZE_AQS_MAX_VALUE);
       return MEA_ERROR;
    }

    /* Check for data_i */
    if (data_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for data_i->drop_prbability */
    if (data_i->drop_probability > MEA_WRED_DROP_PROBABILITY_MAX_VALUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_i->drop_probability %d > max value %d \n",
                         __FUNCTION__,
                         data_i->drop_probability,
                         MEA_WRED_DROP_PROBABILITY_MAX_VALUE);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

/************************************************************************/
/*        Check if the key is valid                                     */
/************************************************************************/
    if(MEA_API_IsValid_WRED_Profile (unit,&key_i->profile_key,MEA_FALSE)!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - the wred (acm=%d profile= %d) not valid \n",
            __FUNCTION__,
            key_i->profile_key.acm_mode,
            key_i->profile_key.profile_id);
        return MEA_ERROR;
    }

    /* Update the HW */
    if (mea_set_WRED_UpdateHw(key_i,
                              data_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Update HW failed (acm_mode=%d,profile_id=%d , priority=%d,normalize_aqs=%d)\n",
                         __FUNCTION__,
                         key_i->profile_key.acm_mode,
                         key_i->profile_key.profile_id,
                         key_i->priority,
                         key_i->normalize_aqs);
       return MEA_ERROR;
    }

    /* Locate on the entry */
    wred_acm     = &(mea_drv_wred_info.acmTable[key_i->profile_key.acm_mode]);
    wred_profile = &(wred_acm->profileTable[key_i->profile_key.profile_id]);
    entry_data   = &(wred_profile->data.priorityTable[key_i->priority].wredTable[key_i->normalize_aqs]);
    
    /* Update output parameter */
    MEA_OS_memcpy(entry_data,
                  data_i,        
                  sizeof(*entry_data));

    /* Return to Caller */
    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_WRED_EntryRange>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_WRED_EntryRange(MEA_Unit_t                 unit,
                                       MEA_WRED_Entry_Key_dbt    *key_from_i,
                                       MEA_WRED_Entry_Key_dbt    *key_to_i,
                                       MEA_WRED_Entry_Data_dbt   *data_i)
{

    MEA_WRED_Entry_Key_dbt   entry_key;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (key_from_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for key_from_i->profile_key.acm_mode */
    if (key_from_i->profile_key.acm_mode >= mea_drv_wred_info.number_of_acms) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->profile_key.acm_mode %d >= max value %d \n",
                         __FUNCTION__,
                         key_from_i->profile_key.acm_mode,
                         mea_drv_wred_info.number_of_acms);
       return MEA_ERROR;
    }

    /* Check for key_from_i->profile_key.profile_id */
    if (key_from_i->profile_key.profile_id >= mea_drv_wred_info.number_of_profiles) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->profile_key.profile_id %d >= max value %d \n",
                         __FUNCTION__,
                         key_from_i->profile_key.profile_id,
                         mea_drv_wred_info.number_of_profiles);
       return MEA_ERROR;
    }

    /* Check for key_from_i->normalize_aqs */
    if (key_from_i->priority >= MEA_WRED_NUM_OF_PRI) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->priority %d >= max value %d \n",
                         __FUNCTION__,
                         key_from_i->priority,
                         MEA_WRED_NUM_OF_PRI);
       return MEA_ERROR;
    }

    /* Check for key_from_i->normalize_aqs */
    if (key_from_i->normalize_aqs >= MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->normalize_aqs %d >= max value %d \n",
                         __FUNCTION__,
                         key_from_i->normalize_aqs,
                         MEA_WRED_NORMALIZE_AQS_MAX_VALUE);
       return MEA_ERROR;
    }

    if (key_to_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for key_to_i->profile_key.acm_mode */
    if (key_to_i->profile_key.acm_mode >= mea_drv_wred_info.number_of_acms) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i->profile_key.acm_mode %d >= max value %d \n",
                         __FUNCTION__,
                         key_to_i->profile_key.acm_mode,
                         mea_drv_wred_info.number_of_acms);
       return MEA_ERROR;
    }

    /* Check for key_to_i->profile_key.profile_id */
    if (key_to_i->profile_key.profile_id >= mea_drv_wred_info.number_of_profiles) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i->profile_key.profile_id %d >= max value %d \n",
                         __FUNCTION__,
                         key_to_i->profile_key.profile_id,
                         mea_drv_wred_info.number_of_profiles);
       return MEA_ERROR;
    }

    /* Check for key_to_i->normalize_aqs */
    if (key_to_i->priority >= MEA_WRED_NUM_OF_PRI) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->priority %d >= max value %d \n",
                         __FUNCTION__,
                         key_to_i->priority,
                         MEA_WRED_NUM_OF_PRI);
       return MEA_ERROR;
    }

    /* Check for key_to_i->normalize_aqs */
    if (key_to_i->normalize_aqs >= MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i->normalize_aqs %d >= max value %d \n",
                         __FUNCTION__,
                         key_to_i->normalize_aqs,
                         MEA_WRED_NORMALIZE_AQS_MAX_VALUE);
       return MEA_ERROR;
    }

    /* Check for key_from_i->profile_key.acm_mode == key_to_i->profile_key.acm_mode */
    if (key_from_i->profile_key.acm_mode != key_to_i->profile_key.acm_mode) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->profile_key.acm_mode %d != key_to_i->profile_key.acm_mode %d \n",
                         __FUNCTION__,
                         key_from_i->profile_key.acm_mode,
                         key_to_i->profile_key.acm_mode);
       return MEA_ERROR;
    }

    /* Check for key_from_i->profile_key.profile_id == key_to_i->profile_key.profile_id */
    if (key_from_i->profile_key.profile_id != key_to_i->profile_key.profile_id) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->profile_key.profile_id %d != key_to_i->profile_key.profile_id %d \n",
                         __FUNCTION__,
                         key_from_i->profile_key.profile_id,
                         key_to_i->profile_key.profile_id);
       return MEA_ERROR;
    }

    /* Check for key_from_i->priority == key_to_i->priority */
    if (key_from_i->priority != key_to_i->priority) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->priority %d != key_to_i->priority %d \n",
                         __FUNCTION__,
                         key_from_i->priority,
                         key_to_i->priority);
       return MEA_ERROR;
    }

    /* Check for key_to_i->normalize_aqs >= key_from_i->normalize_aqs */
    if (key_to_i->normalize_aqs < key_from_i->normalize_aqs) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i->normalize_aqs %d < key_from_i->normalize_aqs %d \n",
                         __FUNCTION__,
                         key_to_i->normalize_aqs,
                         key_from_i->normalize_aqs);
       return MEA_ERROR;
    }



    if (data_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for data_i->drop_prbability */
    if (data_i->drop_probability > MEA_WRED_DROP_PROBABILITY_MAX_VALUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_i->drop_probability %d > max value %d \n",
                         __FUNCTION__,
                         data_i->drop_probability,
                         MEA_WRED_DROP_PROBABILITY_MAX_VALUE);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* for each normalize_aqs between from and to update the entry */
    MEA_OS_memcpy(&entry_key,key_from_i,sizeof(entry_key));
    for (;
         entry_key.normalize_aqs<=key_to_i->normalize_aqs;
         entry_key.normalize_aqs++){ 

        if (MEA_API_Set_WRED_Entry(MEA_UNIT_0,
                                   &entry_key,
                                   data_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_WRED_Entry failed "
                              "(acm_mode=%d,profile_id=%d , priority=%d,normalize_aqs=%d)\n",
                              __FUNCTION__,
                              entry_key.profile_key.acm_mode,
                              entry_key.profile_key.profile_id,
                              entry_key.priority,
                              entry_key.normalize_aqs);
            return MEA_ERROR;
        }
    }

    /* Return to Caller */
	return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_WRED_EntryCurve>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_WRED_EntryCurve  (MEA_Unit_t               unit,
                                         MEA_WRED_Entry_Key_dbt    *key_from_i,
                                         MEA_WRED_Entry_Key_dbt    *key_to_i,
                                         MEA_WRED_Entry_Data_dbt   *data_from_i,
                                         MEA_WRED_Entry_Data_dbt   *data_to_i)
{

    MEA_WRED_Entry_Key_dbt   entry_key;
    MEA_WRED_Entry_Data_dbt  entry_data;
    MEA_Uint32               slope;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (key_from_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for key_from_i->profile_key.acm_mode */
    if (key_from_i->profile_key.acm_mode >= mea_drv_wred_info.number_of_acms) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->profile_key.acm_mode %d >= max value %d \n",
                         __FUNCTION__,
                         key_from_i->profile_key.acm_mode,
                         mea_drv_wred_info.number_of_acms);
       return MEA_ERROR;
    }

    /* Check for key_from_i->profile_key.profile_id */
    if (key_from_i->profile_key.profile_id >= mea_drv_wred_info.number_of_profiles) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->profile_key.profile_id %d >= max value %d \n",
                         __FUNCTION__,
                         key_from_i->profile_key.profile_id,
                         mea_drv_wred_info.number_of_profiles);
       return MEA_ERROR;
    }

    /* Check for key_from_i->normalize_aqs */
    if (key_from_i->priority >= MEA_WRED_NUM_OF_PRI) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->priority %d >= max value %d \n",
                         __FUNCTION__,
                         key_from_i->priority,
                         MEA_WRED_NUM_OF_PRI);
       return MEA_ERROR;
    }

    /* Check for key_from_i->normalize_aqs */
    if (key_from_i->normalize_aqs >= MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->normalize_aqs %d >= max value %d \n",
                         __FUNCTION__,
                         key_from_i->normalize_aqs,
                         MEA_WRED_NORMALIZE_AQS_MAX_VALUE);
       return MEA_ERROR;
    }

    if (key_to_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for key_to_i->profile_key.acm_mode */
    if (key_to_i->profile_key.acm_mode >= mea_drv_wred_info.number_of_acms) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i->profile_key.acm_mode %d >= max value %d \n",
                         __FUNCTION__,
                         key_to_i->profile_key.acm_mode,
                         mea_drv_wred_info.number_of_acms);
       return MEA_ERROR;
    }

    /* Check for key_to_i->profile_key.profile_id */
    if (key_to_i->profile_key.profile_id >= mea_drv_wred_info.number_of_profiles) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i->profile_key.profile_id %d >= max value %d \n",
                         __FUNCTION__,
                         key_to_i->profile_key.profile_id,
                         mea_drv_wred_info.number_of_profiles);
       return MEA_ERROR;
    }

    /* Check for key_to_i->normalize_aqs */
    if (key_to_i->priority >= MEA_WRED_NUM_OF_PRI) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->priority %d >= max value %d \n",
                         __FUNCTION__,
                         key_to_i->priority,
                         MEA_WRED_NUM_OF_PRI);
       return MEA_ERROR;
    }

    /* Check for key_to_i->normalize_aqs */
    if (key_to_i->normalize_aqs >= MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i->normalize_aqs %d >= max value %d \n",
                         __FUNCTION__,
                         key_to_i->normalize_aqs,
                         MEA_WRED_NORMALIZE_AQS_MAX_VALUE);
       return MEA_ERROR;
    }

    /* Check for key_from_i->profile_key.acm_mode == key_to_i->profile_key.acm_mode */
    if (key_from_i->profile_key.acm_mode != key_to_i->profile_key.acm_mode) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->profile_key.acm_mode %d != key_to_i->profile_key.acm_mode %d \n",
                         __FUNCTION__,
                         key_from_i->profile_key.acm_mode,
                         key_to_i->profile_key.acm_mode);
       return MEA_ERROR;
    }

    /* Check for key_from_i->profile_key.profile_id == key_to_i->profile_key.profile_id */
    if (key_from_i->profile_key.profile_id != key_to_i->profile_key.profile_id) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->profile_key.profile_id %d != key_to_i->profile_key.profile_id %d \n",
                         __FUNCTION__,
                         key_from_i->profile_key.profile_id,
                         key_to_i->profile_key.profile_id);
       return MEA_ERROR;
    }

    /* Check for key_from_i->priority == key_to_i->priority */
    if (key_from_i->priority != key_to_i->priority) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_from_i->priority %d != key_to_i->priority %d \n",
                         __FUNCTION__,
                         key_from_i->priority,
                         key_to_i->priority);
       return MEA_ERROR;
    }

    /* Check for key_to_i->normalize_aqs >= key_from_i->normalize_aqs */
    if (key_to_i->normalize_aqs < key_from_i->normalize_aqs) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_to_i->normalize_aqs %d < key_from_i->normalize_aqs %d \n",
                         __FUNCTION__,
                         key_to_i->normalize_aqs,
                         key_from_i->normalize_aqs);
       return MEA_ERROR;
    }


    if (data_from_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_from_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for data_from_i->drop_prbability */
    if (data_from_i->drop_probability > MEA_WRED_DROP_PROBABILITY_MAX_VALUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_from_i->drop_probability %d > max value %d \n",
                         __FUNCTION__,
                         data_from_i->drop_probability,
                         MEA_WRED_DROP_PROBABILITY_MAX_VALUE);
       return MEA_ERROR;
    }

    if (data_to_i == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_to_i == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for data_to_i->drop_prbability */
    if (data_to_i->drop_probability > MEA_WRED_DROP_PROBABILITY_MAX_VALUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_to_i->drop_probability %d > max value %d \n",
                         __FUNCTION__,
                         data_to_i->drop_probability,
                         MEA_WRED_DROP_PROBABILITY_MAX_VALUE);
       return MEA_ERROR;
    }

    /* Check for valid from_normalize_aqs <= to_normalize_aqs parameter */
    if (data_from_i->drop_probability > data_to_i->drop_probability) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid "
                         "data_from_i->drop_probability(%d) > "
                         "data_to_i->drop_probability(%d) \n",
                         __FUNCTION__,
                         data_from_i->drop_probability,
                         data_to_i->drop_probability);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* slope = 10*DY/DX */
    slope = (((MEA_Uint32)data_to_i   -> drop_probability -
              (MEA_Uint32)data_from_i -> drop_probability +1)  * 10) / 
            (key_to_i->normalize_aqs-key_from_i->normalize_aqs+1); 

    /* for each normalize_aqs between from and to update the entry */
    MEA_OS_memcpy(&entry_key,key_from_i,sizeof(entry_key));
    for (;
         entry_key.normalize_aqs<=key_to_i->normalize_aqs;
         entry_key.normalize_aqs++){ 

        MEA_OS_memset(&entry_data,0,sizeof(entry_data));
        entry_data.drop_probability = (MEA_DropProbability_t)((entry_key.normalize_aqs - key_from_i->normalize_aqs)*slope);

        if (entry_data.drop_probability % 10 > 5) {
           entry_data.drop_probability /= 10;
           entry_data.drop_probability ++;
        } else {
           entry_data.drop_probability /= 10;
        }

        entry_data.drop_probability += data_from_i->drop_probability; 

        if (MEA_API_Set_WRED_Entry(MEA_UNIT_0,
                                   &entry_key,
                                   &entry_data) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Set_WRED_Entry failed "
                              "(acm_mode=%d,profile_id=%d , priority=%d,normalize_aqs=%d)\n",
                              __FUNCTION__,
                              entry_key.profile_key.acm_mode,
                              entry_key.profile_key.profile_id,
                              entry_key.priority,
                              entry_key.normalize_aqs);
            return MEA_ERROR;
        }
    }

    /* Return to Caller */
    return MEA_OK;

}






