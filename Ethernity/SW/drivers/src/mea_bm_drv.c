/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*************************************************************************************/
/* 	Module Name:		 mea_bm_drv.c											 	 */
/*																					 */
/*  Module Description:	 MEA buffer manager driver									 */
/*																					 */
/*  Date of Creation:	 20/01/06		   											 */
/*																					 */
/*  Author Name: Alex Weis.						    					             */
/*************************************************************************************/ 



/*-------------------------------- Includes ------------------------------------------*/


#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_port_drv.h"
#include "mea_bm_drv.h"
#include "mea_bm_scheduler_drv.h"
#include "mea_bm_ehp_drv.h"
#include "mea_bm_ep_drv.h"
#include "mea_bm_wred_drv.h"
#include "mea_bm_counters_drv.h"
#include "mea_drv_common.h"
#include "mea_port_drv.h"
#include "mea_init.h"
#include "mea_policer_drv.h"
#include "mea_PacketGen_drv.h"
#include "mea_editingMapping_drv.h"
#include "mea_ccm_drv.h"
#include "mea_bfd_drv.h"
#include "mea_bonding_drv.h"
#include "mea_swe_ip_drv.h"
#include "mea_if_vsp_drv.h"
#include "mea_wbrg.h"


/*-------------------------------- Definitions ---------------------------------------*/
#define Target_MTU 1
  

/*-------------------------------- External Functions --------------------------------*/

/*-------------------------------- External Variables --------------------------------*/

/*-------------------------------- Forward Declarations ------------------------------*/

/*-------------------------------- Local Variables -----------------------------------*/

/*-------------------------------- Global Variables ----------------------------------*/


MEA_Status  mea_drv_Init_BM(MEA_Unit_t unit_i) 
{

    if(b_bist_test){
        return MEA_OK;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize BM  block ...\n");
#if 0
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Scheduler configuration  ...\n");
    if (mea_drv_Init_Scheduler_Table(unit_i) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_Scheduler_Table failed\n",
                          __FUNCTION__);
		return MEA_ERROR;
	}
#endif

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Editing Mapping configuration  ...\n");
    if (mea_drv_Init_EditingMappingProfile_Table(unit_i)==MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Init_EditingMappingProfile_Table failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize EHP configuration  ...\n");
	if(mea_drv_Init_EHP(unit_i)==MEA_ERROR){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Init_EHP failed\n",__FUNCTION__);
		return MEA_ERROR;
	}

    
        
    if(mea_drv_WBRG_Init(unit_i)==MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_WBRG_Init failed\n",__FUNCTION__);
        return MEA_ERROR;
    }
	
    if (mea_drv_Global_Egress_SA_MAC_Init(unit_i) == MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_Global_Egress_SA_MAC_Init failed\n", __FUNCTION__);
        return MEA_ERROR;
    }

     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Egress Port configuration  ...\n");
	if(mea_drv_Init_EgressPort_Table(unit_i)==MEA_ERROR){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Init_EgressPort_Table failed\n",__FUNCTION__);
		return MEA_ERROR;
	}

	//Init WRED drop probability
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize WRED probability  ...\n");
    if (mea_drv_Init_WRED_Table(unit_i) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_WRED_Table failed\n",__FUNCTION__);
		return MEA_ERROR;
	}
        
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize BM Policer  ...\n");

    if (mea_drv_Init_Policer(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_Policer failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 

    if(mea_drv_Ingress_flow_policer_Init(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_Ingress_flow_policer_Init failed\n",
        __FUNCTION__);
        return MEA_ERROR;
    }


    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize BM Counters   ...\n");
    mea_drv_Init_BM_Counters(unit_i);

   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Analyzer Counters  ...\n");
   if(mea_drv_Init_Analyzer(unit_i)!=MEA_OK){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_Analyzer failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
   }
#if MEA_CCM_ENABLE   
   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize CCM configure  ...\n");
   if(mea_drv_Init_CCM(unit_i) !=MEA_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_CCM failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
   }
#endif
  
   
   if (mea_drv_Init_BFD(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - mea_drv_Init_CCM failed\n",
           __FUNCTION__);
       return MEA_ERROR;
   }
  
   
  
    
    if(mea_drv_Init_BondingTx(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_BondingTx failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    if(mea_drv_Init_Bonding_Counters(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_Bonding_Counters failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
   
    if(mea_drv_Init_Rx_AfdxBag_prof(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_Rx_AfdxBag_prof failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_AC_MTU_prof_Init(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_AC_MTU_prof_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

	

#ifdef Target_MTU
    if(mea_drv_Target_MTU_prof_Init(unit_i) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_Target_MTU_prof_Init failed \n",
        __FUNCTION__);
        return MEA_ERROR;
        }
#endif
    return MEA_OK;
}
MEA_Status  mea_drv_ReInit_BM(MEA_Unit_t unit_i) 
{
    if (b_bist_test)  return MEA_OK;

    if (mea_drv_ReInit_EditingMappingProfile_Table(unit_i)==MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_EditingMappingProfile_Table failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

	if(mea_drv_ReInit_EHP(unit_i)==MEA_ERROR){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_EHP failed\n",
                          __FUNCTION__);
		return MEA_ERROR;
	}

    if(mea_drv_WBRG_RInit(unit_i)==MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_EHP failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
	
    if(mea_drv_Global_Egress_SA_MAC_ReInit(unit_i) == MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ReInit_EgressPort_Table failed\n", __FUNCTION__);
        return MEA_ERROR;
    }

	if(mea_drv_ReInit_EgressPort_Table(unit_i)==MEA_ERROR){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_ReInit_EgressPort_Table failed\n",__FUNCTION__);
		return MEA_ERROR;
	}

    if (mea_drv_ReInit_WRED_Table(unit_i) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_WRED_Table failed\n",__FUNCTION__);
		return MEA_ERROR;
	}


    if (mea_drv_ReInit_Policer(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_Policer failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 
    if(mea_drv_Ingress_flow_policer_Reinit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Ingress_flow_policer_Reinit failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_ReInit_BM_Counters(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_BM_Counters failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    
    if(mea_drv_ReInit_Counters_ANALYZE(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_Counters_ANALYZE failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
#if MEA_CCM_ENABLE       
    if(mea_drv_Reinit_CCM(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_CCM failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
#endif
  

    if (mea_drv_Reinit_BFD(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Reinit_BFD failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
  
  
    if(mea_drv_RInit_BondingTx(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_RInit_BondingTx failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    
    if(mea_drv_ReInit_Bonding_Counters(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_Bonding_Counters failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_RInit_Rx_AfdxBag_prof(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_RInit_Rx_AfdxBag_prof failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_AC_MTU_prof_RInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_AC_MTU_prof_RInit failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
#ifdef Target_MTU
    if(mea_drv_Target_MTU_prof_RInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Target_MTU_prof_RInit failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
#endif
    return MEA_OK;
}




MEA_Status  mea_drv_Conclude_BM(MEA_Unit_t unit_i) 
{


    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude BM Counters ...\n");
	if (mea_drv_Conclude_BM_Counters(unit_i) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_drv_Conclude_BM_Counters failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

    if (mea_drv_Conclude_Policer(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Conclude_Policer failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 

    if(mea_drv_Ingress_flow_policer_Conclude(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Ingress_flow_policer_Conclude failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude EHP configuration ...\n");
	if(mea_drv_Conclude_EHP(unit_i)==MEA_ERROR){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Conclude_EHP failed\n",__FUNCTION__);
		return MEA_ERROR;
	}
    if(mea_drv_WBRG_Conclude(unit_i)==MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_WBRG_Conclude failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Analyzer ...\n");
    if(mea_drv_Conclude_Analyzer(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Conclude Analyzer failed\n",__FUNCTION__);
		return MEA_ERROR;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude EditingMapping Profile ...\n");
    if (mea_drv_Conclude_EditingMappingProfile_Table(unit_i)==MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Conclude_EditingMappingProfile_table failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Conclude_BondingTx(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Conclude BondingTx failed\n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_Conclude_Bonding_Counters(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Conclude Bonding Count failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Conclude_Rx_AfdxBag_prof(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Conclude AfdxBag failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_AC_MTU_prof_Conclude(unit_i) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_AC_MTU_prof_Conclude failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
#ifdef Target_MTU
    if(mea_drv_Target_MTU_prof_Conclude(unit_i) != MEA_OK){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_Target_MTU_prof_Conclude failed \n",
        __FUNCTION__);
    return ENET_ERROR;
    }
#endif    

    return MEA_OK;

}

