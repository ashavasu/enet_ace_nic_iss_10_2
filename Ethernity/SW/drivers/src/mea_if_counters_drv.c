/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#if 1
#ifdef MEA_OS_LINUX
#ifdef MEA_OS_OC
#include <linux/unistd.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#endif
#else
#include <time.h>
#endif
#endif

#include "mea_if_counters_drv.h"
#include "mea_api.h"
#include "mea_port_drv.h"
#include "mea_drv_common.h"
#include "mea_init.h"
#include "mea_preATM_parser_drv.h"
#include "mea_Interface_drv.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/









/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/

typedef struct {
    MEA_Bool            			valid;
    MEA_Uint32                 		num_of_owners ;
    MEA_Counters_Utopia_Rmon_dbt	data[MEA_PRE_ATM_PARSER_MAX_VSP_TYPE+1];
    time_t							t1[MEA_PRE_ATM_PARSER_MAX_VSP_TYPE+1];
    
} MEA_drv_Utopia_Rmon_dbt;



typedef struct  
{
    MEA_Bool   valid;
    MEA_Uint16 offset;
}mea_rmon_translate_dbt;



typedef struct{ 

    union {
        struct{
             MEA_Uint32 RX_UC_Pkts;
             MEA_Uint32 TX_UC_Pkts;
             MEA_Uint32 RX_Bytes;
             MEA_Uint32 TX_Bytes;
         }val;
         MEA_Uint32 index_0_regs[4];
     }offset_0;
        
     
     union {
        struct{
             MEA_Uint32 RX_MC_Pkts;
             MEA_Uint32 TX_MC_Pkts;
             MEA_Uint32 RX_drop_bytes;
             MEA_Uint32 TX_drop_bytes;
        }val;
         MEA_Uint32 index_1_regs[4];
     }offset_1;
    union {
        struct{
             
             MEA_Uint32 RX_BC_Pkts;
             MEA_Uint32 TX_BC_Pkts;
#if __BYTE_ORDER == __LITTLE_ENDIAN 
             MEA_Uint32 RX_Min : 8;
             MEA_Uint32 pad    : 24;

#else
             MEA_Uint32 pad    : 24;
             MEA_Uint32 RX_Min : 8;
#endif 
            MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_2_regs[4];
     }offset_2;
#if 1    
    union {
        struct{
             
             MEA_Uint32 NA_0;
             MEA_Uint32 Tx_L2CP_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_3_regs[4];
     }offset_3;
#endif    
     union {
        struct{
             
             MEA_Uint32 Phy_Error_Pkts;
             MEA_Uint32 Tx_Underrun_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_4_regs[4];
     }offset_4;
      union {
        struct{
             
             MEA_Uint32 Rx_fragmant_Pkts; /*MTU*//*fragmant*/
             MEA_Uint32 Tx_Oversize_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_5_regs[4];
     }offset_5;

      union {
        struct{
             
             MEA_Uint32 Rx_CRC_Err; 
             MEA_Uint32 Tx_CRC_Err;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_6_regs[4];
     }offset_6;
     union {
        struct{
             
             MEA_Uint32 NA_1;
             MEA_Uint32 Tx_Drop_Pkts_egrs_decision_rule; //Tx_L2CP_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_7_regs[4];
     }offset_7;

    union {
        struct{
             MEA_Uint32 Rx_64Octets_Pkts; 
             MEA_Uint32 Tx_64Octets_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_8_regs[4];
     }offset_8;
    union {
        struct{
             MEA_Uint32 Rx_65to127Octets_Pkts; 
             MEA_Uint32 Tx_65to127Octets_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_9_regs[4];
     }offset_9;
     union {
        struct{
             MEA_Uint32 Rx_128to255Octets_Pkts; 
             MEA_Uint32 Tx_128to255Octets_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_10_regs[4];
     }offset_10;
     union {
        struct{
             MEA_Uint32 Rx_256to511Octets_Pkts; 
             MEA_Uint32 Tx_256to511Octets_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_11_regs[4];
     }offset_11;
     union {
        struct{
             MEA_Uint32 Rx_512to1023Octets_Pkts; 
             MEA_Uint32 Tx_512to1023Octets_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_12_regs[4];
     }offset_12;
     union {
        struct{
             MEA_Uint32 Rx_1024to1518Octets_Pkts; 
             MEA_Uint32 Tx_1024to1518Octets_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_13_regs[4];
     }offset_13;
     union {
        struct{
             MEA_Uint32 Rx_1519to2047Octets_Pkts; 
             MEA_Uint32 Tx_1519to2047Octets_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_14_regs[4];
     }offset_14;
     union {
        struct{
             MEA_Uint32 Rx_2048toMaxOctets_Pkts; 
             MEA_Uint32 Tx_2048toMaxOctets_Pkts;
             MEA_Uint32 NA_2; 
             MEA_Uint32 NA_3;
        }val;
         MEA_Uint32 index_15_regs[4];
     }offset_15;

} MEA_Counter_RMON_driver_dbt;


typedef enum {
    MEA_IF_COUNTER_RMON_INDEX_0 =0,
    MEA_IF_COUNTER_RMON_INDEX_1,
    MEA_IF_COUNTER_RMON_INDEX_2,
    MEA_IF_COUNTER_RMON_INDEX_3, 
    MEA_IF_COUNTER_RMON_INDEX_4,
    MEA_IF_COUNTER_RMON_INDEX_5, /* MTU drop*/
    MEA_IF_COUNTER_RMON_INDEX_6,
    MEA_IF_COUNTER_RMON_INDEX_7,
    MEA_IF_COUNTER_RMON_INDEX_8,
    MEA_IF_COUNTER_RMON_INDEX_9,
    MEA_IF_COUNTER_RMON_INDEX_10,
    MEA_IF_COUNTER_RMON_INDEX_11,
    MEA_IF_COUNTER_RMON_INDEX_12,
    MEA_IF_COUNTER_RMON_INDEX_13,
    MEA_IF_COUNTER_RMON_INDEX_14,
    MEA_IF_COUNTER_RMON_INDEX_15,
    MEA_IF_COUNTER_RMON_INDEX_LAST  

}MEA_if_Counter_Rmon_index;

typedef struct {
    MEA_Bool  valid;

    MEA_Counters_Virtual_Rmon_dbt data;
}MEA_Counters_VRmon_dbt;
/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static MEA_Counters_IngressPort_dbt MEA_Counters_IngressPort_Table[MEA_MAX_PORT_NUMBER+1];

static MEA_Counters_RMON_dbt        MEA_Counters_RMON_Table       [MEA_MAX_PORT_NUMBER+1];
static MEA_Bool                     MEA_Counters_RMON_Enable      [MEA_MAX_PORT_NUMBER + 1];


static  MEA_Counters_VRmon_dbt     *MEA_Counters_Virtual_Rmon_Table = NULL;

static MEA_drv_Utopia_Rmon_dbt     *MEA_Counters_Utopia_RMON_Table = NULL;

static MEA_Counters_dbg_ing_dbt     MEA_Counters_Ingress_Table;

MEA_Bool mea_RMON_en_calc_rate;

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


static MEA_Status mea_drv_rmon_get_1p1(MEA_Unit_t  unit, MEA_Port_t   port);

/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_Counters_IngressPort>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_IF_Counters_IngressPort(MEA_Unit_t unit_i)
{

    if(b_bist_test) {
      return MEA_OK;
	  }
    /* Init IF Counters Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Counters table ...\n");
    /* Reset Counters IngressPort */
    if (MEA_API_Clear_Counters_IngressPorts(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Clear_Counters_IngressPorts failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* return to caller */
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_Counters_IngressPort>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_IF_Counters_IngressPort(MEA_Unit_t unit_i)
{

    MEA_Port_t port;
    MEA_Counters_IngressPort_dbt  save_entry_db;
    MEA_Counters_IngressPort_dbt*      entry_db_p;

    return MEA_OK;
    /* Scan all ports and read the IngressPort counters to clear from hardware ,
       without update the software shadow */
    for (port=0;port<=MEA_MAX_PORT_NUMBER;port++) {

	    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        entry_db_p = &(MEA_Counters_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)]);
        MEA_OS_memcpy(&save_entry_db,entry_db_p,sizeof(save_entry_db));
	    if (MEA_API_Collect_Counters_IngressPort(unit_i,port,NULL)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect Ingress Port Counters for port=%d failed\n",
                              __FUNCTION__,port);
            MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));
            return MEA_ERROR;
        }
        MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));

    }

    /* Return to Callers */
    return MEA_OK;

}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_Counters_RMON>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_IF_Counters_RMON(MEA_Unit_t unit_i)
{
    MEA_Port_t  port;

    if (b_bist_test) {
        return MEA_OK;
    }



    /* Reset Counters RMON */
    if (MEA_API_Clear_Counters_RMONs(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Clear_Counters_RMONs failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    for (port = 0; port <= MEA_MAX_PORT_NUMBER; port++)
    {
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) == MEA_FALSE) {
            continue;
        }
        MEA_Counters_RMON_Enable[port] = MEA_TRUE;
    }
    /* return to caller */
	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_Counters_RMON>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_IF_Counters_RMON(MEA_Unit_t unit_i)
{
    MEA_Port_t port;
    MEA_Counters_RMON_dbt  save_entry_db;
    MEA_Counters_RMON_dbt* entry_db_p;

    /* Scan all ports and read the RMON counters to clear from hardware ,
       without update the software shadow */
    for (port=0;port<=MEA_MAX_PORT_NUMBER;port++) {

        if(MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE){
            continue;
        }

        /* Check for valid port parameter */
        if (MEA_API_Get_IsRmonPortValid(port,
                                    MEA_TRUE /* silent */ 
                                   )==MEA_FALSE) { 
                continue;
            }

        if(MEA_Counters_RMON_Enable[port] == MEA_FALSE)
            continue;

        entry_db_p = &(MEA_Counters_RMON_Table[port]);
        MEA_OS_memcpy(&save_entry_db,entry_db_p,sizeof(save_entry_db));
	    if (MEA_API_Collect_Counters_RMON(unit_i,port,NULL)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect RMON Counters for port=%d failed\n",
                              __FUNCTION__,port);
            MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));
            return MEA_ERROR;
        }
        MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));

    }

    /* Return to Callers */
    return MEA_OK;
    }

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_IngressPorts>                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_IngressPorts (MEA_Unit_t unit) {
   
#if 0    
    MEA_Port_t port;

	MEA_API_LOG
    
    if(MEA_reinit_start)
            return MEA_OK;

    for (port=0;port<=MEA_MAX_PORT_NUMBER;port++) {

	    if (MEA_API_Get_IsPortValid(port,
                                    MEA_TRUE /* silent */ 
                                   )==MEA_FALSE) { 
            continue;
        }

        /* Collect the Port Counters */
	    if (MEA_API_Collect_Counters_IngressPort(MEA_UNIT_0,port,NULL)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect Ingress Port Counters for port=%d failed\n",
                              __FUNCTION__,port);
            return MEA_ERROR;
        }

    }
#endif
    /* Return to Callers */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_IngressPort>                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_IngressPort  (MEA_Unit_t             unit,
                                                  MEA_Port_t             port,
                                                  MEA_Counters_IngressPort_dbt* entry) 
{
#if 0  /*Not support*/
    MEA_Counters_IngressPort_dbt* entry_db;
	MEA_Uint32                    parser_rd[2];
    MEA_Uint32                    value; 
	MEA_ind_read_t ind_read;
	mea_memory_mode_e save_globalMemoryMode;


	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid port parameter */
    if (MEA_API_Get_IsPortValid(port,MEA_FALSE)==MEA_FALSE) { 
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid port %d \n",
                          __FUNCTION__,port);
        return MEA_ERROR;  
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


	save_globalMemoryMode = globalMemoryMode;
	if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
		if ((port == 118) || (port == 127) ||(port == 24)) {
			globalMemoryMode = MEA_MODE_DOWNSTREEM_ONLY;
		} else {
			globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
		}
	}

    if (MEA_GLOBAL_INGRESS_PORT_COUNT_SUPPORT ==MEA_TRUE){

	ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_COUNTERS;
	ind_read.tableOffset	= port;
	ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
	ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_read.statusReg		= MEA_IF_STATUS_REG;   
	ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
	ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_read.read_data      = &(parser_rd[0]);

    /* Write to the Indirect Table */
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
                             MEA_NUM_OF_ELEMENTS(parser_rd),
                             MEA_MODULE_IF,
							 globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
    	globalMemoryMode = save_globalMemoryMode;
		return MEA_ERROR;
    }
    
    }
    globalMemoryMode = save_globalMemoryMode;
    
    



    entry_db = &(MEA_Counters_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)]);

     if (MEA_GLOBAL_INGRESS_PORT_COUNT_SUPPORT == MEA_TRUE){
    /* Update the L2CP_Discard counter */
    value = (parser_rd[0] & MEA_IF_PARSER_COUNTERS_MEA_L2CP_ACTION_DISCARD_MASK) 
              >> MEA_OS_calc_shift_from_mask(MEA_IF_PARSER_COUNTERS_MEA_L2CP_ACTION_DISCARD_MASK);
    MEA_OS_UPDATE_COUNTER(entry_db->L2CP_Discard.val,value);


    /* Update the OAM_Discard counter */
    value = (parser_rd[0] & MEA_IF_PARSER_COUNTERS_OAM_DISCARD_MASK) 
              >> MEA_OS_calc_shift_from_mask(MEA_IF_PARSER_COUNTERS_OAM_DISCARD_MASK);
    MEA_OS_UPDATE_COUNTER(entry_db->OAM_Discard.val,value);

    /* Update the Mismatch_Discard counter */
    value = (parser_rd[0] & MEA_IF_PARSER_COUNTERS_MISS_DISCARD_MASK) 
              >> MEA_OS_calc_shift_from_mask(MEA_IF_PARSER_COUNTERS_MISS_DISCARD_MASK);
    MEA_OS_UPDATE_COUNTER(entry_db->Mismatch_Discard.val,value);

    /* Update the Rx_Fifo_Full_Discard counter */
    
    value = (parser_rd[1] & MEA_IF_PARSER_COUNTERS_RX_FIFO_FULL_DISCARD_MASK ) 
                  >> MEA_OS_calc_shift_from_mask(MEA_IF_PARSER_COUNTERS_RX_FIFO_FULL_DISCARD_MASK);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Fifo_Full_Discard.val,value);
     }else{
         /*the counter not support*/
        entry_db->L2CP_Discard.val             = 0xDEAD0001;
        entry_db->OAM_Discard.val              = 0xDEAD0002;
        entry_db->Mismatch_Discard.val         = 0xDEAD0003;
        entry_db->Rx_Fifo_Full_Discard.val     = 0xDEAD0004;
     }

    /* update output parameter */
    if (entry != NULL) {
        MEA_OS_memcpy(entry,entry_db,sizeof(*entry));
    }

#endif
    /* Return to Callers */
    return MEA_OK;   

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Counters_IngressPort>                             */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_IngressPort      (MEA_Unit_t             unit,
                                                  MEA_Port_t             port,
                                                  MEA_Counters_IngressPort_dbt* entry) {
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid port parameter */
    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid port %d \n",
                          __FUNCTION__,port);
        return MEA_ERROR;  
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry==NULL (port=%d)\n",
                          __FUNCTION__,port);
        return MEA_ERROR;  
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   /* Update output parameters */
   MEA_OS_memcpy(entry,
                 &(MEA_Counters_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)]),
                 sizeof(*entry));

   /* Return to Callers */
   return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Clear_Counters_ports>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_IngressPorts   (MEA_Unit_t                unit) {


    MEA_Port_t port;

	MEA_API_LOG

    for (port=0;port<=MEA_MAX_PORT_NUMBER;port++) {

	    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        /* Collect the Port Counters */
	    if (MEA_API_Clear_Counters_IngressPort(MEA_UNIT_0,port)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Clear Port Counters for port=%d failed\n",
                              __FUNCTION__,port);
        }

    }


    /* Zeors the SW ports counters db - for all table */
    MEA_OS_memset(&(MEA_Counters_IngressPort_Table[0]),
                  0,
                  sizeof(MEA_Counters_IngressPort_Table));

    /* Return to Callers */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Clear_Counters_IngressPort>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_IngressPort(MEA_Unit_t    unit,
    MEA_Port_t port) {

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check for valid port parameter */
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE) == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Invalid port %d \n",
                __FUNCTION__, port);
            return MEA_ERROR;
        }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* Collect Port Counters from HW to cause HW counters zeros */
    if (MEA_API_Collect_Counters_IngressPort(MEA_UNIT_0, port, NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Collect Port counters failed (port=%d)\n",
            __FUNCTION__, port);
        return MEA_ERROR;

    }

    /* Zeors the SW port counters db */
    MEA_OS_memset(&(MEA_Counters_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)]),
        0,
        sizeof(MEA_Counters_IngressPort_Table[0]));


    /* Return to Callers */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Get_IsRmonPortValid>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool  MEA_API_Get_IsRmonPortValid(MEA_Port_t port,
    MEA_Bool   silent)
{
    MEA_Port_type_te       porttype;
    MEA_Bool exist;

    if ((MEA_PACKET_ANALYZER_TYPE1_SUPPORT || MEA_PACKETGEN_SUPPORT) &&
        (port == MEA_PACKET_GEN_RMON_PORT)) {
        exist = MEA_TRUE;
        return MEA_TRUE;
    }



    exist = MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, silent);

    if (exist) {
        if (MEA_API_Get_IngressPort_Type(MEA_UNIT_0, port, &porttype) != MEA_OK) {
            return MEA_FALSE;
        }
        if (porttype == MEA_PORTTYPE_AAL5_ATM_TC) {
            return MEA_FALSE;
        }
    }


    return exist;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Collect_RMON_Enable>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_RMON_Enable(MEA_Unit_t unit_i,
                                       MEA_Port_t port_i,
                                       MEA_Bool   enable)
{
    if (port_i > MEA_MAX_PORT_NUMBER)
        return MEA_ERROR;
    if (MEA_API_Get_IsRmonPortValid(port_i, MEA_FALSE) == MEA_FALSE) {
        return MEA_ERROR;
    }

    if (MEA_Counters_RMON_Enable[port_i] == MEA_FALSE && enable == MEA_TRUE) 
    {
        /*clear RMON*/
        if(MEA_API_Clear_Counters_RMON(unit_i, port_i) != MEA_OK)
        {
            return MEA_ERROR;
        }
    }

    MEA_Counters_RMON_Enable[port_i] = enable;
    

    return MEA_OK;

}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Collect_RMON_Enable>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool MEA_API_Collect_RMON_IsEnable(MEA_Unit_t                          unit_i,
    MEA_Port_t                          port_i)
{
    if (port_i > MEA_MAX_PORT_NUMBER)
        return MEA_FALSE;

    return MEA_Counters_RMON_Enable[port_i];
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_RMONs>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_RMONs (MEA_Unit_t unit) {
   
    
    MEA_Port_t port;

 	MEA_API_LOG
    
    if(MEA_reinit_start)
            return MEA_OK;


    //check the link_status of the serdes interface
    if (mea_DRV_Serdes_Link_check(unit)==MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_DRV_Serdes_Link_check\n",
            __FUNCTION__);
        return MEA_ERROR;
    }



    for (port=0;port<=MEA_MAX_PORT_NUMBER;port++) { 

	    if (MEA_API_Get_IsRmonPortValid(port,
                                    MEA_TRUE /* silent */ 
                                   )==MEA_FALSE) { 
            continue;
        }
        if (MEA_Counters_RMON_Enable[port] == MEA_FALSE)
            continue;

        /* Collect the Port Counters */
	    if (MEA_API_Collect_Counters_RMON(MEA_UNIT_0,port,NULL)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect RMON Counters for port=%d failed\n",
                              __FUNCTION__,port);
            return MEA_ERROR;
        }

    }


    







    /* Return to Callers */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_RMON>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_RMON  (MEA_Unit_t             unit,
                                           MEA_Port_t             port,
                                           MEA_Counters_RMON_dbt* entry) 
{

    MEA_Port_type_te          port_type;
    MEA_Counters_RMON_dbt* entry_db;
//    MEA_Uint32             rmon_rx_rd[1];
//    MEA_Uint32             rmon_tx_rd[1];    
//    MEA_Uint32             rmon_rx_crc_err_rd[1];
//    MEA_Uint32             value;
	MEA_ind_read_t         ind_read;
    MEA_Uint16             addres_offset;
    MEA_Counter_RMON_driver_dbt entry_drv; 
    mea_memory_mode_e save_globalMemoryMode;
    MEA_Uint32             Rx_Mac_Drop_Pkts[4];
    MEA_Uint32             Rx_IL_Mac_Drop_Pkts[1];
    MEA_db_HwUnit_t hwUnit;
    MEA_Counters_RMON_dbt rmon_counter_entry;
    time_t t1,t2;
    MEA_Uint32 i;

	
    MEA_API_LOG

        

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check for valid port parameter */
    if (MEA_API_Get_IsRmonPortValid(port,MEA_FALSE)==MEA_FALSE) { 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Invalid port %d \n",
                              __FUNCTION__,port);
            return MEA_ERROR;  
        }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

      

	save_globalMemoryMode = globalMemoryMode;
	if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000) {
		if ((port == 118) || (port == 127) || (port == 24)) {
			globalMemoryMode = MEA_MODE_DOWNSTREEM_ONLY;
		} else {
			globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
		}
	}


    MEA_API_Get_IngressPort_Type(unit,port ,&port_type);

    /* set the entry_db pointer */
    entry_db = &(MEA_Counters_RMON_Table[port]);

    t1=MEA_Counters_RMON_Table[port].t1;
    

    MEA_OS_memset(&rmon_counter_entry,0,sizeof(rmon_counter_entry));
    MEA_OS_memcpy(&rmon_counter_entry,&MEA_Counters_RMON_Table[port],sizeof(rmon_counter_entry));


if(globalMemoryMode == MEA_MODE_REGULAR_ENET3000){
    MEA_OS_memset(&entry_drv,0,sizeof(MEA_Counter_RMON_driver_dbt));
    
     addres_offset=0;
     addres_offset=(port<<4);
   

    
    /*  RMON  */
	ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_RMON_RX;
	ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
	ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_read.statusReg		= MEA_IF_STATUS_REG;   
	ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
	ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	
    
    
/* Read offset 0 -  */
    ind_read.read_data      = &(entry_drv.offset_0.index_0_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_0;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_0.index_0_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }

    

/* Read offset 1 -  */
    ind_read.read_data      = &(entry_drv.offset_1.index_1_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_1;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_1.index_1_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    

/* Read offset 2 -  */
    ind_read.read_data      = &(entry_drv.offset_2.index_2_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_2;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_2.index_2_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }

    /* Read offset 3 -  */
        ind_read.read_data      = &(entry_drv.offset_3.index_3_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_3;
    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry_drv.offset_3.index_3_regs),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);

            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
            globalMemoryMode = save_globalMemoryMode;

            return MEA_ERROR;
    }

    

/* Read offset 4 -  */
    ind_read.read_data      = &(entry_drv.offset_4.index_4_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_4;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_4.index_4_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    
 
  /* Read offset 5 -  */
    ind_read.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_RMON_RX;

    ind_read.read_data      = &(entry_drv.offset_5.index_5_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_5;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_5.index_5_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    
  

/* Read offset 6 -  */
    ind_read.read_data      = &(entry_drv.offset_6.index_6_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_6;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_6.index_6_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    

/* Read offset 7 -  */
    ind_read.read_data      = &(entry_drv.offset_7.index_7_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_7;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_7.index_7_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    
    
/* Read offset 8 -  */
    ind_read.read_data      = &(entry_drv.offset_8.index_8_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_8;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_8.index_8_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    
    
/* Read offset 9 -  */
    ind_read.read_data      = &(entry_drv.offset_9.index_9_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_9;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_9.index_9_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    

/* Read offset 10 -  */
    ind_read.read_data      = &(entry_drv.offset_10.index_10_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_10;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_10.index_10_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    


/* Read offset 11 -  */
    ind_read.read_data      = &(entry_drv.offset_11.index_11_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_11;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_11.index_11_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    

/* Read offset 12 -  */
    ind_read.read_data      = &(entry_drv.offset_12.index_12_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_12;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_12.index_12_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    

/* Read offset 13 -  */
    ind_read.read_data      = &(entry_drv.offset_13.index_13_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_13;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_13.index_13_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    
/* Read offset 14 -  */
    ind_read.read_data      = &(entry_drv.offset_14.index_14_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_14;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_14.index_14_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
		globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }
    

/* Read offset 15 -  */
    ind_read.read_data      = &(entry_drv.offset_15.index_15_regs[0] ); 
    ind_read.tableOffset	= addres_offset  + MEA_IF_COUNTER_RMON_INDEX_15;
    if (MEA_API_ReadIndirect(unit,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(entry_drv.offset_15.index_15_regs),
                                 MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                              __FUNCTION__,
                              ind_read.tableType,
                              ind_read.tableOffset,
                              MEA_MODULE_IF);
		    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
        globalMemoryMode = save_globalMemoryMode;
            
            return MEA_ERROR;
    }

       MEA_DRV_GET_HW_UNIT(unit,hwUnit,return MEA_FALSE);
        Rx_Mac_Drop_Pkts[0] =0;
        Rx_Mac_Drop_Pkts[1] = 0;
        Rx_Mac_Drop_Pkts[2] = 0;
        Rx_Mac_Drop_Pkts[3] = 0;
        
        
        if(mea_drv_Get_DeviceInfo_RmonRx_drop_support(unit,hwUnit)==MEA_TRUE)
        {
            ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_RX_MAC_DROP_PACKET;
            ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
            ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
            ind_read.statusReg		= MEA_IF_STATUS_REG;   
            ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
            ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
            ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;

            ind_read.read_data      = &(Rx_Mac_Drop_Pkts[0] ); 
   
            
 
            for (i = 0; i <= 1; i++){
                Rx_Mac_Drop_Pkts[i] = 0;
                ind_read.read_data = &(Rx_Mac_Drop_Pkts[i]);
                ind_read.tableOffset = (port*4)+i;
                if (MEA_API_ReadIndirect(unit,
                    &ind_read,
                    1,
                    MEA_MODULE_IF) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                        __FUNCTION__,
                        ind_read.tableType,
                        ind_read.tableOffset,
                        MEA_MODULE_IF);

                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                        __FUNCTION__, ind_read.tableType, MEA_MODULE_IF, port);
                    globalMemoryMode = save_globalMemoryMode;

                    return MEA_ERROR;
                }
            }//for
    }
    Rx_IL_Mac_Drop_Pkts[0]=0;
    if(mea_drv_Get_DeviceInfo_RmonRx_IL_drop_support(unit,hwUnit)==MEA_TRUE){
        if(port_type == MEA_PORTTYPE_Interlaken_MAC){

            ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_IL_DROP_PACKET;
            ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
            ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
            ind_read.statusReg		= MEA_IF_STATUS_REG;   
            ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
            ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
            ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;

            ind_read.read_data      = &(Rx_IL_Mac_Drop_Pkts[0] ); 
            ind_read.tableOffset	= port;

            if (MEA_API_ReadIndirect(unit,
                &ind_read,
                MEA_NUM_OF_ELEMENTS(Rx_IL_Mac_Drop_Pkts),
                MEA_MODULE_IF) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                        __FUNCTION__,
                        ind_read.tableType,
                        ind_read.tableOffset,
                        MEA_MODULE_IF);

                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                        __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port);
                    globalMemoryMode = save_globalMemoryMode;

                    return MEA_ERROR;
            }

        }
    }



    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Bytes.val ,entry_drv.offset_0.val.TX_Bytes);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Bytes.val ,entry_drv.offset_0.val.RX_Bytes);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_UC_Pkts.val ,entry_drv.offset_0.val.TX_UC_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_UC_Pkts.val ,entry_drv.offset_0.val.RX_UC_Pkts);
    
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Drop_Bytes.val ,entry_drv.offset_1.val.TX_drop_bytes);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Drop_Bytes.val ,entry_drv.offset_1.val.RX_drop_bytes);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_MC_Pkts.val ,entry_drv.offset_1.val.TX_MC_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_MC_Pkts.val ,entry_drv.offset_1.val.RX_MC_Pkts);
    /* need to Calculate  also the drop*/
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Bytes.val ,entry_drv.offset_1.val.RX_drop_bytes);

    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Mac_Drop_Pkts.val ,Rx_Mac_Drop_Pkts[0]);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Mac_DropSmallfragment_Pkts.val, Rx_Mac_Drop_Pkts[1]);

    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Mac_Drop_Pkts.val ,Rx_IL_Mac_Drop_Pkts[0]);

    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Error_Pkts.val ,entry_drv.offset_2.val.RX_Min);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_BC_Pkts.val ,entry_drv.offset_2.val.RX_BC_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_BC_Pkts.val ,entry_drv.offset_2.val.TX_BC_Pkts);

    MEA_OS_UPDATE_COUNTER(entry_db->Tx_L2CP_Pkts.val ,entry_drv.offset_3.val.Tx_L2CP_Pkts);

    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Underrun_Pkts.val ,entry_drv.offset_4.val.Tx_Underrun_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Error_Pkts.val ,entry_drv.offset_4.val.Phy_Error_Pkts);
    
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_fragmant_Pkts.val ,entry_drv.offset_5.val.Rx_fragmant_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Oversize_Pkts.val ,entry_drv.offset_5.val.Tx_Oversize_Pkts);
    
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_CRC_Err.val ,entry_drv.offset_6.val.Tx_CRC_Err);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_CRC_Err.val ,entry_drv.offset_6.val.Rx_CRC_Err);
    
    //MEA_OS_UPDATE_COUNTER(entry_db->Tx_Overrun_Pkts.val ,entry_drv.offset_7.val.Tx_Overrun_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Drop_Pkts_egr_rule.val ,entry_drv.offset_7.val.Tx_Drop_Pkts_egrs_decision_rule);
    
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_64Octets_Pkts.val ,entry_drv.offset_8.val.Tx_64Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_64Octets_Pkts.val ,entry_drv.offset_8.val.Rx_64Octets_Pkts);

    MEA_OS_UPDATE_COUNTER(entry_db->Tx_65to127Octets_Pkts.val ,entry_drv.offset_9.val.Tx_65to127Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_65to127Octets_Pkts.val ,entry_drv.offset_9.val.Rx_65to127Octets_Pkts);

    MEA_OS_UPDATE_COUNTER(entry_db->Tx_128to255Octets_Pkts.val ,entry_drv.offset_10.val.Tx_128to255Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_128to255Octets_Pkts.val ,entry_drv.offset_10.val.Rx_128to255Octets_Pkts);

    MEA_OS_UPDATE_COUNTER(entry_db->Tx_256to511Octets_Pkts.val ,entry_drv.offset_11.val.Tx_256to511Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_256to511Octets_Pkts.val ,entry_drv.offset_11.val.Rx_256to511Octets_Pkts);

    MEA_OS_UPDATE_COUNTER(entry_db->Tx_512to1023Octets_Pkts.val ,entry_drv.offset_12.val.Tx_512to1023Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_512to1023Octets_Pkts.val ,entry_drv.offset_12.val.Rx_512to1023Octets_Pkts);

    MEA_OS_UPDATE_COUNTER(entry_db->Tx_1024to1518Octets_Pkts.val ,entry_drv.offset_13.val.Tx_1024to1518Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_1024to1518Octets_Pkts.val ,entry_drv.offset_13.val.Rx_1024to1518Octets_Pkts);

    MEA_OS_UPDATE_COUNTER(entry_db->Tx_1519to2047Octets_Pkts.val ,entry_drv.offset_14.val.Tx_1519to2047Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_1519to2047Octets_Pkts.val ,entry_drv.offset_14.val.Rx_1519to2047Octets_Pkts);




    MEA_OS_UPDATE_COUNTER(entry_db->Tx_2048toMaxOctets_Pkts.val ,entry_drv.offset_15.val.Tx_2048toMaxOctets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_2048toMaxOctets_Pkts.val ,entry_drv.offset_15.val.Rx_2048toMaxOctets_Pkts);

   /* calculate  total RX packets*/
    MEA_OS_UPDATE_COUNTER( entry_db->Rx_Pkts.val ,entry_drv.offset_5.val.Rx_fragmant_Pkts);
    MEA_OS_UPDATE_COUNTER( entry_db->Rx_Pkts.val ,entry_drv.offset_8.val.Rx_64Octets_Pkts);
    MEA_OS_UPDATE_COUNTER( entry_db->Rx_Pkts.val ,entry_drv.offset_9.val.Rx_65to127Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_10.val.Rx_128to255Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_11.val.Rx_256to511Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_12.val.Rx_512to1023Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_13.val.Rx_1024to1518Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_14.val.Rx_1519to2047Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_15.val.Rx_2048toMaxOctets_Pkts);
    
    
    /* calculate  total TX packets*/ 
    MEA_OS_UPDATE_COUNTER( entry_db->Tx_Pkts.val ,entry_drv.offset_5.val.Tx_Oversize_Pkts);
    MEA_OS_UPDATE_COUNTER( entry_db->Tx_Pkts.val ,entry_drv.offset_8.val.Tx_64Octets_Pkts);
    MEA_OS_UPDATE_COUNTER( entry_db->Tx_Pkts.val ,entry_drv.offset_9.val.Tx_65to127Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_10.val.Tx_128to255Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_11.val.Tx_256to511Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_12.val.Tx_512to1023Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_13.val.Tx_1024to1518Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_14.val.Tx_1519to2047Octets_Pkts);
    MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_15.val.Tx_2048toMaxOctets_Pkts);

    
    if((mea_RMON_en_calc_rate))
      t2=time(&MEA_Counters_RMON_Table[port].t1);
    
    if(t2>t1 && t1 !=0 && (mea_RMON_en_calc_rate)){
		if (MEA_Counters_RMON_Table[port].Rx_Bytes.val != 0) {
			MEA_Uint64 tmp;
#if defined(__KERNEL__)
			MEA_Uint32 tdiff;
			MEA_uint64 rate;
			tmp.val = MEA_Counters_RMON_Table[port].Rx_Bytes.val -
				rmon_counter_entry.Rx_Bytes.val;

			tdiff = (MEA_Uint32) t2-t1;
			do_div (tmp.val, tdiff);
			MEA_Counters_RMON_Table[port].Rx_rate = tmp.val;
#else
			tmp.val = MEA_Counters_RMON_Table[port].Rx_Bytes.val -
				rmon_counter_entry.Rx_Bytes.val;
			MEA_Counters_RMON_Table[port].Rx_rate.val = (tmp.val /(t2-t1));
#endif
		}
        if (MEA_Counters_RMON_Table[port].Rx_Pkts.val != 0) {
            MEA_Uint64 tmp;
#if defined(__KERNEL__)
            MEA_Uint32 tdiff;
            MEA_uint64 rate;
            tmp.val = MEA_Counters_RMON_Table[port].Rx_Pkts.val -
                rmon_counter_entry.Rx_Pkts.val;

            tdiff = (MEA_Uint32) t2-t1;
            do_div (tmp.val, tdiff);
            MEA_Counters_RMON_Table[port].Rx_Pkts = tmp.val;
#else
            tmp.val = MEA_Counters_RMON_Table[port].Rx_Pkts.val -
                rmon_counter_entry.Rx_Pkts.val;
            MEA_Counters_RMON_Table[port].Rx_ratePacket.val = (tmp.val /(t2-t1));
#endif
        }



		if (MEA_Counters_RMON_Table[port].Tx_Bytes.val != 0) {
			MEA_Uint64 tmp;
#if defined(__KERNEL__)
			MEA_Uint32 tdiff;
			MEA_Uint64 rate;
			tmp.val = MEA_Counters_RMON_Table[port].Tx_Bytes.val -
				rmon_counter_entry.Tx_Bytes.val;

			tdiff = (MEA_Uint32) t2-t1;
			do_div (tmp.val, tdiff);
			MEA_Counters_RMON_Table[port].Tx_rate = tmp.val; 
#else
			tmp.val = MEA_Counters_RMON_Table[port].Tx_Bytes.val -
				rmon_counter_entry.Tx_Bytes.val;
            MEA_Counters_RMON_Table[port].Tx_rate.val= (tmp.val / (t2-t1));
#endif
		}
        if (MEA_Counters_RMON_Table[port].Tx_Pkts.val != 0) {
            MEA_Uint64 tmp;
#if defined(__KERNEL__)
            MEA_Uint32 tdiff;
            MEA_Uint64 rate;
            tmp.val = MEA_Counters_RMON_Table[port].Tx_Pkts.val -
                rmon_counter_entry.Tx_Pkts.val;

            tdiff = (MEA_Uint32) t2-t1;
            do_div (tmp.val, tdiff);
            MEA_Counters_RMON_Table[port].Tx_rate = tmp.val; 
#else
            tmp.val = MEA_Counters_RMON_Table[port].Tx_Pkts.val -
                rmon_counter_entry.Tx_Pkts.val;
            MEA_Counters_RMON_Table[port].Tx_ratePacket.val= (tmp.val / (t2-t1));
#endif
        }


    }


}

    
    /* update output parameter */
    if (entry != NULL) {
        MEA_OS_memcpy(entry,entry_db,sizeof(*entry));
    }




    /* Return to Callers */
    return MEA_OK;   

}

MEA_Status mea_drv_rmon_get_1p1(MEA_Unit_t             unit,
                                MEA_Port_t             port)
{
    MEA_IngressPort_Entry_dbt IngressPort_Entry;
    MEA_Port_t take_port;
    MEA_Bool  valid_1p1=MEA_TRUE;

        
    /* Get the current IngressPort Entry */
    if (MEA_API_Get_IngressPort_Entry(unit,
        port,
        &IngressPort_Entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_GetIngressPort_Entry for port %d failed\n",
                __FUNCTION__,port);
            return MEA_ERROR;
    }
    
 
   
   
    
    if(  IngressPort_Entry.ProtectType == MEA_INGRESS_PORT_PROTECT_1_PLUS_1 &&
        IngressPort_Entry.Protect_1p1_info.enable == MEA_TRUE &&
        IngressPort_Entry.Protect_1p1_info.master_secondary == MEA_INGRESS_PORT_PROTECT_1P1_SECONDARY ){
        
            switch (port)
            {
            case 12:
                take_port=0;
                break;
            case 36:
                take_port=24;
                break;

            case 72:
                take_port=48;
                break;
            
            case 126:
                take_port=125;
                break;
            
            case 111:
                take_port=110;
                break;

            case 119:
                take_port=118;
                break;
            default:
                valid_1p1=MEA_FALSE;
                break;
            }
            
            if(valid_1p1 == MEA_TRUE){

        // only the Tx will copy
                    MEA_Counters_RMON_Table[port].Tx_Pkts                      =  MEA_Counters_RMON_Table[take_port].Tx_Pkts                   ;
                    MEA_Counters_RMON_Table[port].Tx_BC_Pkts                   =  MEA_Counters_RMON_Table[take_port].Tx_BC_Pkts                 ;
                    MEA_Counters_RMON_Table[port].Tx_MC_Pkts                   =  MEA_Counters_RMON_Table[take_port].Tx_MC_Pkts                 ;
                    MEA_Counters_RMON_Table[port].Tx_UC_Pkts                   =  MEA_Counters_RMON_Table[take_port].Tx_UC_Pkts                 ;
                    MEA_Counters_RMON_Table[port].Tx_64Octets_Pkts             =  MEA_Counters_RMON_Table[take_port].Tx_64Octets_Pkts           ;
                    MEA_Counters_RMON_Table[port].Tx_65to127Octets_Pkts        =  MEA_Counters_RMON_Table[take_port].Tx_65to127Octets_Pkts      ;
                    MEA_Counters_RMON_Table[port].Tx_128to255Octets_Pkts       =  MEA_Counters_RMON_Table[take_port].Tx_128to255Octets_Pkts     ;
                    MEA_Counters_RMON_Table[port].Tx_256to511Octets_Pkts       =  MEA_Counters_RMON_Table[take_port].Tx_256to511Octets_Pkts     ;
                    MEA_Counters_RMON_Table[port].Tx_512to1023Octets_Pkts      =  MEA_Counters_RMON_Table[take_port].Tx_512to1023Octets_Pkts    ;
                    MEA_Counters_RMON_Table[port].Tx_1024to1518Octets_Pkts     =  MEA_Counters_RMON_Table[take_port].Tx_1024to1518Octets_Pkts   ;
                    MEA_Counters_RMON_Table[port].Tx_1519to2047Octets_Pkts     =  MEA_Counters_RMON_Table[take_port].Tx_1519to2047Octets_Pkts   ;
                    MEA_Counters_RMON_Table[port].Tx_2048toMaxOctets_Pkts      =  MEA_Counters_RMON_Table[take_port].Tx_2048toMaxOctets_Pkts    ;
                    MEA_Counters_RMON_Table[port].Tx_Underrun_Pkts             =  MEA_Counters_RMON_Table[take_port].Tx_Underrun_Pkts           ;
                    MEA_Counters_RMON_Table[port].Tx_Overrun_Pkts              =  MEA_Counters_RMON_Table[take_port].Tx_Overrun_Pkts            ;
                    MEA_Counters_RMON_Table[port].Tx_Oversize_Pkts             =  MEA_Counters_RMON_Table[take_port].Tx_Oversize_Pkts           ;
                    MEA_Counters_RMON_Table[port].Tx_Oversize_Bytes            =  MEA_Counters_RMON_Table[take_port].Tx_Oversize_Bytes          ;
                    MEA_Counters_RMON_Table[port].Tx_CRC_Err                   =  MEA_Counters_RMON_Table[take_port].Tx_CRC_Err                 ;
                    MEA_Counters_RMON_Table[port].Tx_Bytes                     =  MEA_Counters_RMON_Table[take_port].Tx_Bytes                   ;
                    MEA_Counters_RMON_Table[port].Tx_Drop_Bytes                =  MEA_Counters_RMON_Table[take_port].Tx_Drop_Bytes              ;
                    MEA_Counters_RMON_Table[port].Tx_ratePacket                =  MEA_Counters_RMON_Table[take_port].Tx_ratePacket              ;
                    MEA_Counters_RMON_Table[port].Tx_rate                      =  MEA_Counters_RMON_Table[take_port].Tx_rate                    ;
            }
    }

    return MEA_OK;

};


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Counters_RMON>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_RMON      (MEA_Unit_t             unit,
                                           MEA_Port_t             port,
                                           MEA_Counters_RMON_dbt* entry) {


MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid port parameter */
    if (MEA_API_Get_IsRmonPortValid(port,MEA_FALSE)==MEA_FALSE) { 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Invalid port %d \n",
                              __FUNCTION__,port);
            return MEA_ERROR;  
        }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry==NULL (port=%d)\n",
                          __FUNCTION__,port);
        return MEA_ERROR;  
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
   
    if(MEA_PROTECT_1PLUS1_SUPPORT){
      mea_drv_rmon_get_1p1(unit,port);
    }


   /* Update output parameters */
   MEA_OS_memcpy(entry,
                 &(MEA_Counters_RMON_Table[port]),
                 sizeof(*entry));

   /* Return to Callers */
   return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Clear_Counters_RMONs>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_RMONs   (MEA_Unit_t                unit) {


    MEA_Port_t port;

	MEA_API_LOG

    for (port=0;port<=MEA_MAX_PORT_NUMBER;port++) { 

    /* Check for valid port parameter */
        if (MEA_API_Get_IsRmonPortValid(port,
                                    MEA_TRUE /* silent */ 
                                   )==MEA_FALSE) { 
                continue;
            }


        /* Collect the Port Counters */
	    if (MEA_API_Clear_Counters_RMON(MEA_UNIT_0,port)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Clear RMON Counters for port=%d failed\n",
                              __FUNCTION__,port);
        }

    }


    /* Zeors the SW ports counters db - for all table */
    MEA_OS_memset(&(MEA_Counters_RMON_Table[0]),
                  0,
                  sizeof(MEA_Counters_RMON_Table));

    /* Return to Callers */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Clear_Counters_RMON>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_RMON    (MEA_Unit_t    unit,
                                           MEA_Port_t port) {

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid port parameter */
    if (MEA_API_Get_IsRmonPortValid(port,MEA_FALSE)==MEA_FALSE) { 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Invalid port %d \n",
                              __FUNCTION__,port);
            return MEA_ERROR;  
        }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   
    /* Collect Port Counters from HW to cause HW counters zeros */
    if (MEA_API_Collect_Counters_RMON(MEA_UNIT_0,port,NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Collect RMON counters failed (port=%d)\n",
                          __FUNCTION__,port);
        return MEA_ERROR;
      
    }
    /* Clear the SW port counters db */
    MEA_OS_memset(&(MEA_Counters_RMON_Table[port]),
                  0,
                  sizeof(MEA_Counters_RMON_Table[0]));



    /* Return to Callers */
    return MEA_OK;

}



/************************************************************************/
/*                                                                      */
/************************************************************************/




/*---------------------------------------------------------------------------*/
/************************************************************************/
/*  API Counters  Utopia RMON                                              */
/************************************************************************/
/*---------------------------------------------------------------------------*/
MEA_Bool mea_drv_Is_Id_InRange_Utopia_Rmon_Counter(MEA_Port_t port)
{

    if(port<MEA_NUM_OF_UTOPIA_PORTS)
        return MEA_TRUE;
    else
        return MEA_FALSE;		

}
MEA_Bool mea_drv_Is_exist_Utopia_Rmon_Counter(MEA_Unit_t unit_i,MEA_Port_t port)
{
    if(mea_drv_Is_Id_InRange_Utopia_Rmon_Counter(port) != MEA_TRUE){
        return MEA_FALSE;
    } else {
        if(MEA_Counters_Utopia_RMON_Table[port].valid == MEA_TRUE)
            return MEA_TRUE;
        else 
            return MEA_FALSE;
    }
    return MEA_FALSE;
}
MEA_Bool mea_drv_is_freeEntry_Utopia_Rmon_Counter(MEA_Unit_t unit_i)
{
    MEA_Port_t port;

    for(port=0; port<MEA_NUM_OF_UTOPIA_PORTS;port++){
        if(MEA_Counters_Utopia_RMON_Table[port].valid == MEA_FALSE){
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;
}
MEA_Status mea_drv_get_first_Utopia_Rmon_Counter(MEA_Bool *found_o, MEA_Port_t *port)
{
    MEA_Port_t i;
    *found_o = MEA_FALSE;

    for ( i=0; i < MEA_NUM_OF_UTOPIA_PORTS; i++ )
    {
        if (MEA_Counters_Utopia_RMON_Table[i].valid)
        {
            *port = i;
            *found_o = MEA_TRUE;
            return MEA_OK;
        }
    }

    return MEA_OK;
}
MEA_Status mea_drv_get_next_Utopia_Rmon_Counter(MEA_Bool *found_o, MEA_Port_t *port)
{
    MEA_Port_t i;
    *found_o = MEA_FALSE;

    if (mea_drv_Is_Id_InRange_Utopia_Rmon_Counter(*port) != MEA_TRUE) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Invalid port %d \n",
            __FUNCTION__,*port);
        return MEA_ERROR;  
    }

    for ( i=*port+1; i < MEA_NUM_OF_UTOPIA_PORTS; i++ )
    {
        if (MEA_Counters_Utopia_RMON_Table[i].valid)
        {
            *port = i;
            *found_o = MEA_TRUE;
            return MEA_OK;
        }
    }

    return MEA_OK;
}



MEA_Status mea_drv_Collect_Counters_Utopia_Rmon(MEA_Unit_t              unit,
                                                MEA_Port_t              port)
{


    MEA_ind_read_t                 ind_read;
    MEA_Uint8   vsp;
    time_t t1,t2;
    



        //    MEA_db_HwUnit_t          hwUnit;
        //    MEA_db_HwId_t            hwId;
    mea_memory_mode_e              save_globalMemoryMode;
    MEA_Counters_Utopia_Rmon_dbt  *entry_db;
    MEA_Counters_Utopia_Rmon_dbt  entry_db_old;
    MEA_Counters_Utopia_Rmon_Vsp_dbt entry_vsp;

    MEA_Uint32 val[3];
    MEA_Uint32 ret_val_tx[4];
    MEA_Uint32 ret_val_drop[1];
    MEA_Uint32 VspId;
   


    save_globalMemoryMode = globalMemoryMode;
    // read from the hw
    if (!MEA_UTOPIA_SUPPORT){
        globalMemoryMode = save_globalMemoryMode;
        return MEA_ERROR;;
    }
   


    /* Set Global Memory Mode */

    if(port > MEA_NUM_OF_UTOPIA_PORTS ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - counter is out of range\n", __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memset(&entry_vsp,0,sizeof(entry_vsp));

    for(vsp=0;vsp < MEA_PRE_ATM_PARSER_MAX_VSP_TYPE;vsp++){
       
		MEA_OS_memset(&entry_db_old,0,sizeof(entry_db_old));
		MEA_OS_memcpy(&entry_db_old,&MEA_Counters_Utopia_RMON_Table[port].data[vsp],sizeof(entry_db_old));

        entry_db=&MEA_Counters_Utopia_RMON_Table[port].data[vsp];
		t1=MEA_Counters_Utopia_RMON_Table[port].t1[vsp];
		

        
        if(mea_drv_PreATM_parser_get_internal_vsp_number(unit,port,vsp,&VspId) == MEA_TRUE){

        /*  RMON  ATM vsp*/
        ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_RMON_UTOPIA_RX;
        ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
        ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
        ind_read.statusReg		= MEA_IF_STATUS_REG;   
        ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
        ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
        ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;


        ind_read.read_data      = &(val[0]) ; 
        ind_read.tableOffset	=  VspId;/// vsp number
        
        
            if (MEA_API_ReadIndirect(unit,
                &ind_read,
                MEA_NUM_OF_ELEMENTS(val),
                MEA_MODULE_IF) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                        __FUNCTION__,
                        ind_read.tableType,
                        ind_read.tableOffset,
                        MEA_MODULE_IF);

                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)(vsp %d)\n",
                        __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port,vsp);
                    globalMemoryMode = save_globalMemoryMode;

                    return MEA_ERROR;
            }
        





        MEA_OS_UPDATE_COUNTER(entry_db->rx_crc.val,val[0]);
        MEA_OS_UPDATE_COUNTER(entry_db->rx_pkt.val,val[1]);
        MEA_OS_UPDATE_COUNTER(entry_db->rx_byte.val,val[2]);
        
        MEA_OS_UPDATE_COUNTER(entry_vsp.rx_crc.val,val[0]);
        MEA_OS_UPDATE_COUNTER(entry_vsp.rx_pkt.val,val[1]);
        MEA_OS_UPDATE_COUNTER(entry_vsp.rx_byte.val,val[2]);
        
       
        

		
        t2=time(&MEA_Counters_Utopia_RMON_Table[port].t1[vsp]);
		if(t2>t1 && t1 !=0){

			if (entry_db->rx_byte.val != 0) {

#if defined(__KERNEL__)
					MEA_Uint32 tdiff;
#endif
					MEA_Uint64 rate;
					rate.val = entry_db->rx_byte.val - entry_db_old.rx_byte.val;
						
#if defined(__KERNEL__)
					tdiff = (MEA_Uint32) t2-t1;
					do_div (rate.val, tdiff);
#else

					rate.val = rate.val/(t2-t1);
#endif
					entry_db->rate_rx.val = rate.val;
		 }


		}

       } //true



    }//VSP
     entry_db=&MEA_Counters_Utopia_RMON_Table[port].data[MEA_PRE_ATM_PARSER_MAX_VSP_TYPE];
     MEA_OS_memcpy(&entry_db_old,&MEA_Counters_Utopia_RMON_Table[port].data[MEA_PRE_ATM_PARSER_MAX_VSP_TYPE],sizeof(entry_db_old));
     t1=MEA_Counters_Utopia_RMON_Table[port].t1[MEA_PRE_ATM_PARSER_MAX_VSP_TYPE];
     

#if 0 /*we not support*/
    /*  RMON  ATM port drop*/
    ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_RMON_UTOPIA_RX_CAM_DROP_CELL;
    ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
    ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_read.statusReg		= MEA_IF_STATUS_REG;   
    ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
    ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data      = &(ret_val_drop[0]) ; 
    ind_read.tableOffset	=  port;/// vsp number
    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(ret_val_drop),
        MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                __FUNCTION__,
                ind_read.tableType,
                ind_read.tableOffset,
                MEA_MODULE_IF);
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)(vsp %d)\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port,vsp);
            globalMemoryMode = save_globalMemoryMode;
            return MEA_ERROR;
    }
   
    MEA_OS_UPDATE_COUNTER(entry_db->rx_unmatched_L2Type.val,ret_val_drop[0]);
#endif

   /*  RMON  ATM port drop*/
   ind_read.tableType       = ENET_IF_CMD_PARAM_TBL_RMON_UTOPIA_RX_CAM_DROP_CELL;
   ind_read.cmdReg          = MEA_IF_CMD_REG;      
   ind_read.cmdMask         = MEA_IF_CMD_PARSER_MASK;      
   ind_read.statusReg       = MEA_IF_STATUS_REG;   
   ind_read.statusMask      = MEA_IF_STATUS_BUSY_MASK;   
   ind_read.tableAddrReg    = MEA_IF_CMD_PARAM_REG;
   ind_read.tableAddrMask   = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
   ind_read.read_data      = &(ret_val_drop[0]) ; 
   ind_read.tableOffset	=  128 + port;/// vsp number
   if (MEA_API_ReadIndirect(unit,
       &ind_read,
       MEA_NUM_OF_ELEMENTS(ret_val_drop),
       MEA_MODULE_IF) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
               __FUNCTION__,
               ind_read.tableType,
               ind_read.tableOffset,
               MEA_MODULE_IF);
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)(vsp %d)\n",
               __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port,vsp);
           globalMemoryMode = save_globalMemoryMode;
           return MEA_ERROR;
   }
   MEA_OS_UPDATE_COUNTER(entry_db->rx_unmatched_VP_VC.val,ret_val_drop[0]);

//  read tx utopia 


   /*  RMON  ATM port drop*/
   ind_read.tableType       = ENET_IF_CMD_PARAM_TBL_UTOPIA_RMON_COUNTER_TX;
   ind_read.cmdReg          = MEA_IF_CMD_REG;      
   ind_read.cmdMask         = MEA_IF_CMD_PARSER_MASK;      
   ind_read.statusReg       = MEA_IF_STATUS_REG;   
   ind_read.statusMask      = MEA_IF_STATUS_BUSY_MASK;   
   ind_read.tableAddrReg    = MEA_IF_CMD_PARAM_REG;
   ind_read.tableAddrMask   = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
   ind_read.read_data      = &(ret_val_tx[0]) ; 
   ind_read.tableOffset	=  port;/// 
   if (MEA_API_ReadIndirect(unit,
       &ind_read,
       MEA_NUM_OF_ELEMENTS(ret_val_tx),
       MEA_MODULE_IF) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
               __FUNCTION__,
               ind_read.tableType,
               ind_read.tableOffset,
               MEA_MODULE_IF);
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)(vsp %d)\n",
               __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,port,vsp);
           globalMemoryMode = save_globalMemoryMode;
           return MEA_ERROR;
   }
   MEA_OS_UPDATE_COUNTER(entry_db->tx_pkt.val,ret_val_tx[2]);
   MEA_OS_UPDATE_COUNTER(entry_db->tx_byte.val,ret_val_tx[3]);
   MEA_OS_SUM_COUNTER(entry_db->rx_crc,entry_vsp.rx_crc);
   MEA_OS_SUM_COUNTER(entry_db->rx_pkt,entry_vsp.rx_pkt);
   MEA_OS_SUM_COUNTER(entry_db->rx_byte,entry_vsp.rx_byte);
   
   t2=time(&MEA_Counters_Utopia_RMON_Table[port].t1[MEA_PRE_ATM_PARSER_MAX_VSP_TYPE]);
   if(t2>t1 && t1 !=0){
       if (entry_db->rx_byte.val != 0) {
#if defined(__KERNEL__)
           MEA_Uint32 tdiff;
#endif
           MEA_Uint64 rate;
           rate.val = entry_db->rx_byte.val - entry_db_old.rx_byte.val;
#if defined(__KERNEL__)
           tdiff = (MEA_Uint32) t2-t1;
           do_div (rate.val, tdiff);
#else
           rate.val = rate.val/(t2-t1);
#endif
           entry_db->rate_rx.val = rate.val;
       }
			if (entry_db->tx_byte.val != 0) {

#if defined(__KERNEL__)
					MEA_Uint32 tdiff;
#endif				 
					MEA_Uint64 rate;
					rate.val = entry_db->tx_byte.val - entry_db_old.tx_byte.val;
#if defined(__KERNEL__)
					tdiff = (MEA_Uint32) t2-t1;
					do_div (rate.val, tdiff);
#else
					rate.val = rate.val/(t2-t1);
#endif
					entry_db->rate_tx.val = rate.val;
		 }
		}







    return MEA_OK;
}


MEA_Status mea_drv_Init_Utopia_RMON_Counters(MEA_Unit_t unit_i)
{
    MEA_Uint32      size;
    MEA_Port_t port;
	MEA_PortsMapping_t  portMap;

    if(b_bist_test){
        return MEA_OK;
    }
    
    if (!MEA_UTOPIA_SUPPORT){
        return MEA_OK;   
    }
    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Utopia RMON \n");

    /* Allocate MEA_Counters_Utopia_RMON_Table */
    if (MEA_Counters_Utopia_RMON_Table == NULL) {

        size = sizeof(MEA_drv_Utopia_Rmon_dbt)*MEA_NUM_OF_UTOPIA_PORTS;
        MEA_Counters_Utopia_RMON_Table = (MEA_drv_Utopia_Rmon_dbt*)MEA_OS_malloc(size);
        if (MEA_Counters_Utopia_RMON_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_Counters_Utopia_RMON_Table failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        MEA_OS_memset((char*)MEA_Counters_Utopia_RMON_Table,0,size);
    }
    
    for(port=0;port<MEA_NUM_OF_UTOPIA_PORTS;port++){
		if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
			continue;
		}
		
		if (MEA_Low_Get_Port_Mapping_By_key(unit_i,
									MEA_PORTS_TYPE_PHYISCAL_TYPE,
									port,
									&portMap)!=MEA_OK){
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s - MEA_Low_Get_Port_Mapping_By_key failed forUtopia\n",
					__FUNCTION__);
				return ENET_ERROR;
		}
		if(portMap.portType == MEA_PORTTYPE_AAL5_ATM_TC  ){
			MEA_Counters_Utopia_RMON_Table[port].valid = MEA_TRUE;
			MEA_Counters_Utopia_RMON_Table[port].num_of_owners=1;
		}
    }


    return MEA_OK;
}
MEA_Status mea_drv_ReInit_Utopia_RMON_Counters(MEA_Unit_t unit_i)
{

    MEA_Bool              found;
    MEA_Port_t            port=0;
    if (!MEA_UTOPIA_SUPPORT){
        return MEA_OK;   
    }

    if ( mea_drv_get_first_Utopia_Rmon_Counter(&found, &port) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_get_first_Utopia_Rmon_Counter failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    while (found) {
        if (mea_drv_Collect_Counters_Utopia_Rmon(unit_i,port) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Collect_Counters_Utopia_Rmon failed "
                "(tdmId=%d)\n",
                __FUNCTION__,port);
            return MEA_ERROR;
        }
        if ( mea_drv_get_next_Utopia_Rmon_Counter(&found, &port) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_get_next_tdmid_Rmon_Tdm_Counter failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }




    return MEA_OK;

}
MEA_Status mea_drv_Conclude_Utopia_RMON_Counters(MEA_Unit_t unit_i)
{
    if (!MEA_UTOPIA_SUPPORT){
     return MEA_OK;
    }
    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Utopia_RMON \n");

    if(MEA_Counters_Utopia_RMON_Table){
        MEA_OS_free(MEA_Counters_Utopia_RMON_Table);
        MEA_Counters_Utopia_RMON_Table = NULL;
    }
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*            <MEA_API_IsExist_Utopia_RMON_Counter>                          */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_Utopia_RMON_Counter(MEA_Unit_t              unit,
                                               MEA_Port_t              port,
                                               MEA_Bool                *exist) 
{


    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_UTOPIA_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - UTOPIA not Support\n",
                __FUNCTION__);
            return MEA_ERROR;   
        }


        /* Check for valid exist parameter */
        if (exist == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist == NULL \n",__FUNCTION__);
            return MEA_ERROR; 
        }

        if(port > MEA_NUM_OF_UTOPIA_PORTS ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - counter is out of range\n", __FUNCTION__);
            return MEA_ERROR;
        }
        



#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


        /* set default output */
        *exist = MEA_FALSE;


        if ( !MEA_Counters_Utopia_RMON_Table[port].valid )
        {
            return MEA_OK;
        }



        *exist = MEA_TRUE;

        return MEA_OK;

}

/*----------------------------------------------------------------------------------*/
/*            <MEA_API_Collect_Counters_Utopia_RMON>                                */
/*---------------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_Utopia_RMON(MEA_Unit_t                    unit,
                                                MEA_Port_t              	  port,
                                                MEA_Counters_Utopia_Rmon_dbt*    entry)
{

    
    


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool exist = MEA_FALSE;
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_UTOPIA_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - UTOPIA not Support\n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        exist=mea_drv_Is_exist_Utopia_Rmon_Counter(unit,port);

        if (exist == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Utopia_Rmon_Counter port %d  define\n",
                __FUNCTION__,port);
            return MEA_ERROR;  
        }
        

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        

       

        if (mea_drv_Collect_Counters_Utopia_Rmon(unit,port) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Collect_Counters_Utopia_Rmon failed "
                "(port=%d)\n",
                __FUNCTION__,port);
            return MEA_ERROR;

        }
        


        if ( entry != NULL) {
            MEA_OS_memcpy(entry,
                &(MEA_Counters_Utopia_RMON_Table[port].data[MEA_PRE_ATM_PARSER_MAX_VSP_TYPE]),
                sizeof(*entry));
        }



        /* Return to Callers */
        return MEA_OK;


}

/*---------------------------------------------------------------------------*/
/*            <MEA_API_Collect_Counters_Utopia_RMONs>                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_Utopia_RMONs(MEA_Unit_t                unit)
{

    MEA_Bool found;
    MEA_Port_t port;
    MEA_Uint16 vsp;

    MEA_API_LOG
    
    if(MEA_reinit_start)
            return MEA_OK;

    if (!MEA_UTOPIA_SUPPORT){
        return MEA_OK;   
    }

    if ( mea_drv_get_first_Utopia_Rmon_Counter(&found, &port) != MEA_OK )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_get_first_Utopia_Rmon_Counter failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if ( !found )
    {
        return MEA_OK;
    }

    for(vsp=0;vsp < MEA_NUM_OF_UTOPIA_VSP;vsp++){
        if (MEA_API_Collect_Counters_Utopia_RMON(unit,port,
            NULL)==MEA_ERROR){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Collect Utopia_RMON Counters for port=%d failed\n",
                    __FUNCTION__,port);
                return MEA_ERROR;
        }
    }

    while ( found )
    {
        if ( mea_drv_get_next_Utopia_Rmon_Counter(&found, &port) != MEA_OK )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_get_next_Utopia_Rmon_Counter failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if ( found )
        {
            for(vsp=0;vsp < MEA_NUM_OF_UTOPIA_VSP;vsp++){
                if (MEA_API_Collect_Counters_Utopia_RMON(unit,
                    port,
                    NULL)==MEA_ERROR){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - MEA_API_Collect_Counters_Utopia_RMON port=%d failed\n",
                            __FUNCTION__,port);
                        return MEA_ERROR;
                }
            }
        }
    }

    /* Return to Callers */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*            <MEA_API_Get_Counters_Utopia_RMON>                                */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_Utopia_RMON(MEA_Unit_t                    unit,
                                            MEA_Port_t                       port,
                                            MEA_Counters_Utopia_Rmon_dbt*    entry)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool exist = MEA_FALSE;
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_UTOPIA_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - UTOPIA not Support\n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        exist=mea_drv_Is_exist_Utopia_Rmon_Counter(unit,port);

        if (exist == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Utopia_Rmon_Counter port %d not define\n",
                __FUNCTION__,port);
            return MEA_ERROR;  
        }

        /* Check valid parameter :  entry */
        if (entry == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry==NULL \n",
                __FUNCTION__);
            return MEA_ERROR;  
        }
        


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


        /* Update output parameters */
        MEA_OS_memcpy(entry,
            &(MEA_Counters_Utopia_RMON_Table[port].data[MEA_PRE_ATM_PARSER_MAX_VSP_TYPE]),
            sizeof(*entry));

        return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*            <MEA_API_Clear_Counters_Utopia_RMONs>                             */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_Utopia_RMONs(MEA_Unit_t                    unit)
{
    MEA_Bool found;
    MEA_Port_t port;
   

    MEA_API_LOG
        if (!MEA_UTOPIA_SUPPORT){
            return MEA_OK;   
        }

        if ( mea_drv_get_first_Utopia_Rmon_Counter(&found, &port) != MEA_OK )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_get_first_tdmId_Rmon_Tdm_Counter failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if ( !found )
        {
            return MEA_OK;
        }

        //for(vsp=0;vsp < MEA_NUM_OF_UTOPIA_VSP;vsp++){

            /* Clear the Utopia Rmon_ID  Counters */
            if (MEA_API_Clear_Counters_Utopia_RMON(unit,
                port) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - Clear TDM MEA_API_Clear_Counters_Utopia_RMON for port=%d failed\n",
                        __FUNCTION__,port);
                    return MEA_ERROR;
            }
        //}



        while ( found )
        {
            if ( mea_drv_get_next_Utopia_Rmon_Counter(&found, &port) != MEA_OK )
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_get_next_tdmid_Rmon_Tdm_Counter failed\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }

            if ( found )
            {
                //for(vsp=0;vsp < MEA_NUM_OF_UTOPIA_VSP;vsp++){
                    if (MEA_API_Clear_Counters_Utopia_RMON(unit,
                        port) != MEA_OK) {
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - Clear  MEA_API_Clear_Counters_Utopia_RMON port=%d failed\n",
                                __FUNCTION__,port);
                            return MEA_ERROR;
                    }
                //}
            }
        }

        /* Return to Callers */
        return MEA_OK;    
}
/*---------------------------------------------------------------------------*/
/*            <MEA_API_Clear_Counters_Utopia_RMON>                             */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_Utopia_RMON(MEA_Unit_t                    unit,
                                              MEA_Port_t                    port)
{
    MEA_Uint16 vsp;
    MEA_Bool exist;  
    
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_UTOPIA_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - UTOPIA not Support\n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        exist=mea_drv_Is_exist_Utopia_Rmon_Counter(unit,port);

        if (exist == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Utopia port %d not define\n",
                __FUNCTION__,port);
            return MEA_ERROR;  
        }

        




#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


        /* Collect Counters from HW to cause HW counters zeros */
        if (mea_drv_Collect_Counters_Utopia_Rmon(unit,port) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Collect_Counters_Utopia_Rmon failed "
                "(port=%d)\n",
                __FUNCTION__,port);
            return MEA_ERROR;

        }


        /* Zeros  counters database */
        for(vsp=0;vsp<=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE;vsp++)
        MEA_OS_memset(&(MEA_Counters_Utopia_RMON_Table[port].data[vsp]),
            0,
            sizeof(MEA_Counters_Utopia_RMON_Table[0].data[vsp]));


        /* Return to Callers */
        return MEA_OK;
}

/***********************************************************************/
/*  Ingress counting packets                                           */
/***********************************************************************/
MEA_Status MEA_API_Collect_Counters_IngressS    (MEA_Unit_t                    unit)
{
    MEA_API_LOG

    if (!MEA_COUNT_INGRESS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_COUNT_INGRESS_SUPPORT not Support\n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    

    

        return MEA_API_Collect_Counters_Ingress  (unit,NULL);

    return MEA_OK;
}
MEA_Status mea_drv_Collect_Counters_IngressHW(MEA_Unit_t                    unit)
{

    MEA_Uint32 readcount[3];

    if (!MEA_COUNT_INGRESS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_COUNT_INGRESS_SUPPORT not Support\n",
            __FUNCTION__);
        return MEA_ERROR;   
    }

    MEA_OS_memset(&readcount[0],0,sizeof(readcount));


    readcount[0]= MEA_API_ReadReg(unit,ENET_IF_DBG_COUNT_CTRL_FIFO,MEA_MODULE_IF);
    readcount[1]= MEA_API_ReadReg(unit,ENET_BM_DBG_COUNT_RX_REG,MEA_MODULE_BM);
    readcount[2]= MEA_API_ReadReg(unit,ENET_BM_DBG_COUNT_FIFO_REG,MEA_MODULE_BM);

    MEA_OS_UPDATE_COUNTER(MEA_Counters_Ingress_Table.NP_TX.val,readcount[0]);
    MEA_OS_UPDATE_COUNTER(MEA_Counters_Ingress_Table.TM_RX.val,     readcount[1]);
    MEA_OS_UPDATE_COUNTER(MEA_Counters_Ingress_Table.ddr_RX.val,   readcount[2]);

    return MEA_OK;
}

MEA_Status MEA_API_Collect_Counters_Ingress    (MEA_Unit_t                    unit,
                                                MEA_Counters_dbg_ing_dbt*         entry)
{
    

    if (!MEA_COUNT_INGRESS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_COUNT_INGRESS_SUPPORT not Support\n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    
   


   if(mea_drv_Collect_Counters_IngressHW( unit)!=MEA_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - mea_drv_Collect_Counters_IngressHW fail\n",
           __FUNCTION__);
       return MEA_ERROR;
   }
   
    /* Update the output Parameter if Supply */
    if ( entry != NULL) {
        MEA_OS_memcpy(entry,
            &(MEA_Counters_Ingress_Table),
            sizeof(*entry));
    }

    
    return MEA_OK;
}


MEA_Status MEA_API_Clear_Counters_IngressS    (MEA_Unit_t                    unit)
{

    if (!MEA_COUNT_INGRESS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_COUNT_INGRESS_SUPPORT not Support\n",
            __FUNCTION__);
        return MEA_ERROR;   
    }

    /*get*/
    if(mea_drv_Collect_Counters_IngressHW( unit)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Collect_Counters_IngressHW fail\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
        MEA_OS_memset(&(MEA_Counters_Ingress_Table),
        0,
        sizeof(MEA_Counters_Ingress_Table));

    return MEA_OK;
}


MEA_Status MEA_API_Get_Counters_Ingress    (MEA_Unit_t                    unit,
                                            MEA_Counters_dbg_ing_dbt*         entry)
{
    if (!MEA_COUNT_INGRESS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_COUNT_INGRESS_SUPPORT not Support\n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry==NULL \n",
            __FUNCTION__);
        return MEA_ERROR;  
    }

    MEA_OS_memcpy(entry,
        &(MEA_Counters_Ingress_Table),
        sizeof(*entry));


    return MEA_OK;

}


/**-------------------------------------------------------------
 * 
 ---------------------------------------------------------------*/
MEA_Status mea_drv_Init_Virtual_Rmon_Counters(MEA_Unit_t unit_i)
{
	MEA_Uint32      size;
	MEA_Port_t port;
	MEA_PortsMapping_t  portMap;

	if (b_bist_test) {
		return MEA_OK;
	}

    if (!MEA_Virtual_Rmon_SUPPORT) {
        return MEA_OK;
    }

	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize Virtual_Rmon \n");
         
	/* Allocate Table */
	if (MEA_Counters_Virtual_Rmon_Table == NULL) {

		size = sizeof(MEA_Counters_VRmon_dbt)*(MEA_MAX_PORT_NUMBER_INGRESS+1);
        MEA_Counters_Virtual_Rmon_Table = (MEA_Counters_VRmon_dbt*)MEA_OS_malloc(size);
		if (MEA_Counters_Virtual_Rmon_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - Allocate MEA_Counters_Virtual_Rmon_Table failed \n",
				__FUNCTION__);
			return MEA_ERROR;
		}
		MEA_OS_memset((char*)MEA_Counters_Virtual_Rmon_Table, 0, size);
	}

	for (port = 0; port < MEA_MAX_PORT_NUMBER_INGRESS; port++) {
		if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) == MEA_FALSE) {
			continue;
		}

		if (MEA_Low_Get_Port_Mapping_By_key(unit_i,
            MEA_PORTS_TYPE_INGRESS_TYPE,
			port,
			&portMap) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_Low_Get_Port_Mapping_By_key failed forUtopia\n",
				__FUNCTION__);
			return ENET_ERROR;
		}
		if (portMap.portType == MEA_PORTTYPE_VIRTUAL) {
            MEA_Counters_Virtual_Rmon_Table[port].valid = MEA_TRUE;
			
		}
	}


	return MEA_OK;
}
MEA_Status mea_drv_ReInit_Virtual_Rmon_Counters(MEA_Unit_t unit_i)
{

	//MEA_Bool              found;
	//MEA_Port_t            port = 0;
    
    if (!MEA_Virtual_Rmon_SUPPORT) {
       
        return MEA_OK;
    }





	return MEA_OK;

}
MEA_Status mea_drv_Conclude_Virtual_Rmon_Counters(MEA_Unit_t unit_i)
{
    if (!MEA_Virtual_Rmon_SUPPORT) {
       
        return MEA_OK;
    }

	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude Virtual_Rmon \n");

	if (MEA_Counters_Virtual_Rmon_Table) {
		MEA_OS_free(MEA_Counters_Virtual_Rmon_Table);
        MEA_Counters_Virtual_Rmon_Table = NULL;
	}
	return MEA_OK;
}

MEA_Status MEA_API_IsExist_Virtual_Rmon(MEA_Unit_t                    unit_i,
	MEA_Virtual_Rmon_t               id_i,
	MEA_Bool                      *exist_o)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	if (!MEA_Virtual_Rmon_SUPPORT) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Virtual_Rmon not support \n",
			__FUNCTION__);
		return MEA_ERROR;
	}
	if (exist_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - exist_o = NULL \n",
			__FUNCTION__);
		return MEA_ERROR;
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	
	if (id_i <= MEA_PORT_VIRUAL_INGRESS_START &&
		id_i> MEA_PORT_VIRUAL_INGRESS_END) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Virtual_Rmon id=%d is out of range \n", __FUNCTION__, id_i);
		return MEA_ERROR;
	}
	*exist_o = MEA_FALSE;
	if (MEA_Counters_Virtual_Rmon_Table[id_i].valid == MEA_TRUE) {
		*exist_o = MEA_TRUE;
	}
	return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Collect_Counters_Virtual_Rmon>                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_Virtual_Rmon(MEA_Unit_t             unit,
	MEA_Virtual_Rmon_t             id_i,
	MEA_Counters_Virtual_Rmon_dbt* entry)
{


	MEA_ind_read_t ind_read;
	MEA_Uint32     read_data[2];
	MEA_Bool       exist;
	//MEA_Uint32     index;




	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

		if (!MEA_Virtual_Rmon_SUPPORT) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - Virtual_Rmon not support \n",
				__FUNCTION__);
			return MEA_ERROR;
		}
	if (MEA_API_IsExist_Virtual_Rmon(unit, id_i, &exist) == MEA_ERROR) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Rmon id=%d is not Exist\n",
			__FUNCTION__, id_i);
		return ENET_ERROR;
	}


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	MEA_OS_memset(&read_data[0], 0, sizeof(read_data));

	ind_read.tableType = ENET_IF_CMD_PARAM_TBL_101_VRMON;
	ind_read.cmdReg = MEA_IF_CMD_REG;
	ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
	ind_read.statusReg = MEA_IF_STATUS_REG;
	ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
	ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
	ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;


	ind_read.read_data = &(read_data[0]);


	ind_read.tableOffset = id_i;
	if (MEA_API_ReadIndirect(unit,
		&ind_read,
		MEA_NUM_OF_ELEMENTS(read_data),
		MEA_MODULE_IF) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
			__FUNCTION__,
			ind_read.tableType,
			ind_read.tableOffset,
			MEA_MODULE_IF);
		return MEA_ERROR;
	}
	/* Update */






	MEA_OS_UPDATE_COUNTER(MEA_Counters_Virtual_Rmon_Table[id_i].data.Rx_byte.val, (read_data[0]));
	MEA_OS_UPDATE_COUNTER(MEA_Counters_Virtual_Rmon_Table[id_i].data.Rx_pkt.val, (read_data[1]));




	/* Update the output Parameter if Supply */
	if (entry != NULL) {
		MEA_OS_memcpy(entry,
			&(MEA_Counters_Virtual_Rmon_Table[id_i].data),
			sizeof(*entry));
	}


	return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Collect_Counters_Virtual_Rmon>                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_Virtual_Rmons(MEA_Unit_t unit)
{

	MEA_Virtual_Rmon_t id;
	MEA_Bool       exist;

	if (!MEA_Virtual_Rmon_SUPPORT) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Virtual_Rmon not support \n",
			__FUNCTION__);
		return MEA_ERROR;
	}
	


	for (id = MEA_PORT_VIRUAL_INGRESS_START; id <=MEA_PORT_VIRUAL_INGRESS_END; id++) {
		if (MEA_API_IsExist_Virtual_Rmon(unit, id, &exist) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_IsExist_Virtual_Rmon id=%d failed\n",
				__FUNCTION__, id);
			return MEA_ERROR;
		}
		if (exist == MEA_FALSE) {
			continue;
		}
		if (MEA_API_Collect_Counters_Virtual_Rmon(unit, id, NULL) == MEA_ERROR) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - Collect Virtual_Rmon Counters for id=%d failed\n",
				__FUNCTION__, id);
			return MEA_ERROR;
		}
	}


	return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Get_Counters_Virtual_Rmon>                            */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_Virtual_Rmon(MEA_Unit_t                  unit,
	MEA_Virtual_Rmon_t             id_i,
	MEA_Counters_Virtual_Rmon_dbt  *entry)
{

	MEA_Bool     exist;

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

	if (!MEA_Virtual_Rmon_SUPPORT) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Virtual_Rmon not support \n",
			__FUNCTION__);
		return MEA_ERROR;
	}
    if (MEA_API_IsExist_Virtual_Rmon(unit, id_i, &exist) == MEA_ERROR) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Rmon id=%d is not Exist\n",
			__FUNCTION__, id_i);
		return ENET_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Rmon id=%d is not Exist\n",
			__FUNCTION__, id_i);
		return ENET_ERROR;
	}
	if (entry == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Rmon id=%d is not Exist\n",
			__FUNCTION__, id_i);
		return ENET_ERROR;

	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	MEA_OS_memcpy(entry, &MEA_Counters_Virtual_Rmon_Table[id_i].data, sizeof(*entry));

	return MEA_OK;
}
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Clear_Counters_Virtual_Rmon>                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_Virtual_Rmon(MEA_Unit_t    unit,
	MEA_Virtual_Rmon_t             id_i)
{
	MEA_Bool exist;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

	if (!MEA_Virtual_Rmon_SUPPORT) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Virtual_Rmon not support \n",
			__FUNCTION__);
		return MEA_ERROR;
	}
	if (MEA_API_IsExist_Virtual_Rmon(unit, id_i, &exist) == MEA_ERROR) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Rmon id=%d is not Exist\n",
			__FUNCTION__, id_i);
		return ENET_ERROR;
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	if (MEA_API_Collect_Counters_Virtual_Rmon(unit, id_i, NULL) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - MEA_API_Collect_Counters_Virtual_Rmon id=%d failed  \n",
			__FUNCTION__, id_i);
		return ENET_ERROR;
	}

	MEA_OS_memset(&MEA_Counters_Virtual_Rmon_Table[id_i].data, 0, sizeof(MEA_Counters_Virtual_Rmon_Table[id_i].data));
	
	return MEA_OK;
}
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Clear_Counters_Virtual_Rmons>                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_Virtual_Rmons(MEA_Unit_t                unit)
{
	MEA_Virtual_Rmon_t             id;
	MEA_Bool                   exist;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

	if (!MEA_Virtual_Rmon_SUPPORT) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Virtual_Rmon not support \n",
			__FUNCTION__);
		return MEA_ERROR;
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


	for (id = MEA_PORT_VIRUAL_INGRESS_START; id < MEA_PORT_VIRUAL_INGRESS_END; id++) {
		if (MEA_API_IsExist_Virtual_Rmon(unit, id, &exist) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_IsExist_Virtual_Rmon id=%d failed\n",
				__FUNCTION__, id);
			return MEA_ERROR;
		}
		if (exist == MEA_FALSE) {
			continue;
		}
		if (MEA_API_Clear_Counters_Virtual_Rmon(unit, id) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_Clear_Counters_Virtual_Rmon id=%d Failed  \n",
				__FUNCTION__, id);
			return ENET_ERROR;
		}
	}

	return MEA_OK;
}



