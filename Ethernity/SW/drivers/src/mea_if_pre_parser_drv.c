/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include "mea_api.h"


#include "mea_if_regs.h"


/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct{
	MEA_PreParser_Key_dbt  key;
	MEA_PreParser_Data_dbt data;
    MEA_Uint32              valid :1;
    MEA_Uint32              pad0  :31;
} MEA_PreParser_Entry_dbt;  






static MEA_Status mea_PreParser_UpdateHw(MEA_Uint32 PreParser_index,
                                         MEA_PreParser_Entry_dbt* entry,
                                         MEA_PreParser_Entry_dbt* old_entry);
static MEA_Status  mea_PreParser_UpdateHwEntry(MEA_PreParser_Entry_dbt* entry);


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static   MEA_PreParser_Entry_dbt MEA_PreParser_Table[MEA_PRE_PARSER_MAX_ENTRIES];

MEA_Bool MEA_PreParser_InitDone = MEA_FALSE;


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_PreParser_Table>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_PreParser_Table(MEA_Unit_t unit_i)
{
    MEA_PreParser_InitDone = MEA_FALSE;
    
    if(b_bist_test){
        return MEA_OK;
    }
    /* Init IF Pre Parser Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Pre Parser  table ...\n");

    MEA_OS_memset(&(MEA_PreParser_Table[0]),0,sizeof(MEA_PreParser_Table));

    MEA_PreParser_InitDone = MEA_TRUE;     

    return MEA_OK; 
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_PreParser_Table>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_PreParser_Table(MEA_Unit_t unit_i)
{
    MEA_Uint32 i;

    for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_PreParser_Table);i++) {
        if (!MEA_PreParser_Table[i].valid) {
            continue;
        }
        if (mea_PreParser_UpdateHw(i,&(MEA_PreParser_Table[i]),NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_PreParser_UpdateHw failed (i=%d)\n",
                              __FUNCTION__,i);
            return MEA_ERROR;
        }
    }

    return MEA_OK; 
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_PreParser_GetFreeEntryIndex>                              */
/*                                                                           */
/* Pay attention that the function sets the Valid flag                       */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_PreParser_GetFreeEntryIndex(MEA_Uint32* index_o)
{
    if ( index_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - index_o == NULL\n",
                             __FUNCTION__);
       return MEA_ERROR;         
    }

    for ( *index_o = 0; *index_o < MEA_PRE_PARSER_MAX_ENTRIES; (*index_o)++ )    
    {
        if ( MEA_PreParser_Table[*index_o].valid == MEA_FALSE )
        {
            MEA_PreParser_Table[*index_o].valid = MEA_TRUE;
            return MEA_OK;
        }
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - no free Pre Parser table entry index\n",
                             __FUNCTION__);
    return MEA_ERROR;         
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_PreParser_GetIndexByKey>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* static */ MEA_Status mea_PreParser_GetIndexByKey(MEA_PreParser_Key_dbt*  key_i,
                                              MEA_Bool*               found_o,
                                              MEA_Uint32*             index_o )
{
    if ( key_i == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i == NULL\n",
                             __FUNCTION__);
       return MEA_ERROR;         
    }

    if ( found_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i == NULL\n",
                             __FUNCTION__);
       return MEA_ERROR;         
    }

    if ( index_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_i == NULL\n",
                             __FUNCTION__);
       return MEA_ERROR;         
    }    

    *found_o = MEA_FALSE;

    for ( *index_o = 0; *index_o < MEA_PRE_PARSER_MAX_ENTRIES; (*index_o)++ )
    {
        if ((MEA_PreParser_Table[*index_o].valid == MEA_TRUE) && 
            (MEA_OS_memcmp((char*)key_i, 
                          &(MEA_PreParser_Table[*index_o].key), 
                          sizeof(*key_i)) == 0))           
        {
            *found_o = MEA_TRUE;            
		     return MEA_OK; 
	    }
    }       
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_PreParser_UpdateHwEntry>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status  mea_PreParser_UpdateHwEntry(MEA_PreParser_Entry_dbt* entry)
{
	MEA_Uint32	              val0, val1;
 
    val0 = 0;
    val1 = 0;

	val0  = ((entry->key.raw.net_tag
              << MEA_OS_calc_shift_from_mask(MEA_IF_PRE_PARSER_TABLE_REG0_NET_TAG_MASK)
             ) & MEA_IF_PRE_PARSER_TABLE_REG0_NET_TAG_MASK
            ); 

	val0 |= (((entry->key.raw.net_tag_mask & 0x000000ff)
              << MEA_OS_calc_shift_from_mask(MEA_IF_PRE_PARSER_TABLE_REG0_NET_TAG_MASK_MASK)
             ) & MEA_IF_PRE_PARSER_TABLE_REG0_NET_TAG_MASK_MASK
             );

	val1 |= ((((entry->key.raw.net_tag_mask & 0x00ffff00) >> 8)
              << MEA_OS_calc_shift_from_mask(MEA_IF_PRE_PARSER_TABLE_REG1_NET_TAG_MASK_MASK)
             ) & MEA_IF_PRE_PARSER_TABLE_REG1_NET_TAG_MASK_MASK
            ); 

	val1 |= ((entry->valid
              << MEA_OS_calc_shift_from_mask(MEA_IF_PRE_PARSER_TABLE_REG1_VALID_MASK)
             ) & MEA_IF_PRE_PARSER_TABLE_REG1_VALID_MASK
             );

	val1 |= ((entry->data.vsp_type
              << MEA_OS_calc_shift_from_mask(MEA_IF_PRE_PARSER_TABLE_REG1_VSP_TYPE_MASK)
             ) & MEA_IF_PRE_PARSER_TABLE_REG1_VSP_TYPE_MASK
             );

    MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0),val0,MEA_MODULE_IF);
    MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(1),val1,MEA_MODULE_IF);


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_PreParser_UpdateHw>                                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_PreParser_UpdateHw(MEA_Uint32 PreParser_index,
                                         MEA_PreParser_Entry_dbt* entry,
                                         MEA_PreParser_Entry_dbt* old_entry) 
{



	MEA_ind_write_t ind_write;
   

    if ( entry == NULL )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - NULL entry input\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }   

    /* Check if we have changes */
    if (MEA_PreParser_InitDone && 
        old_entry != NULL      &&
       (MEA_OS_memcmp(entry,old_entry,sizeof(*entry)) == 0)) {
       return MEA_OK;
    }
 

    /* build the ind_write value */
	ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_PRE_PARSER;
	ind_write.tableOffset	= PreParser_index;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_PreParser_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)entry;

	
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }

	
	return MEA_OK;
	
   	 
}










/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_PreParser_Entry_Create>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_PreParser_Entry_Create ( MEA_Unit_t              unit_i,
                                                MEA_PreParser_Key_dbt*  key_i,
                                                MEA_PreParser_Data_dbt* data_i,
                                                MEA_Uint32*             index_o )
{

    MEA_Bool   already_exist;
    MEA_PreParser_Data_dbt tmp_data;
    MEA_PreParser_Entry_dbt entry;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key parameter */
    if (key_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
        
    MEA_CHECK_PRE_PARSER_NET_TAG_IN_RANGE(key_i->raw.net_tag)

    MEA_CHECK_PRE_PARSER_NET_TAG_MASK_IN_RANGE(key_i->raw.net_tag_mask)


    /* Check for valid data parameter */
    if (data_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    MEA_CHECK_PRE_PARSER_VSP_TYPE_IN_RANGE(data_i->vsp_type) 

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	/* Check if an entry with such key already exists */
     if ( MEA_API_Get_PreParser_Entry_ByKey ( unit_i,
                                              key_i,
                                              &tmp_data,
                                              &already_exist ) != MEA_OK )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_PreParser_Entry_ByKey failed\n",__FUNCTION__);
        return MEA_ERROR;
    }    

    if ( already_exist == MEA_TRUE )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - there is an existing pre parser table entry with the input key\n",__FUNCTION__);
        return MEA_ERROR;
    }   

    /* Get free pre parser entry index */
	if (mea_PreParser_GetFreeEntryIndex(index_o) != MEA_OK) 
    {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_PreParser_GetFreeEntryIndex failed \n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy( &(entry.key),
                   key_i,
                   sizeof(MEA_PreParser_Table[0].key) );
    MEA_OS_memcpy( &(entry.data),
                   data_i,
                   sizeof(MEA_PreParser_Table[0].data) );
    entry.valid = MEA_TRUE;
    


    /* Update the HW */
    if ( mea_PreParser_UpdateHw(*index_o,
                                &entry,
                                &(MEA_PreParser_Table[*index_o]) ) != MEA_OK )
    {
        /* free the new taken entry */
        MEA_PreParser_Table[*index_o].valid = MEA_FALSE;
    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_PreParser_UpdateHw failed \n",__FUNCTION__);
        return MEA_ERROR;
    }                          

    /* Update the Software shadow */
	MEA_OS_memcpy(&(MEA_PreParser_Table[*index_o].key ) , 
                  key_i, 
                  sizeof(MEA_PreParser_Table[0].key));
	MEA_OS_memcpy(&(MEA_PreParser_Table[*index_o].data ) , 
                  data_i, 
                  sizeof(MEA_PreParser_Table[0].data));
   

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_PreParser_Entry_ByKey>                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_PreParser_Entry_ByKey ( MEA_Unit_t              unit_i,
                                               MEA_PreParser_Key_dbt*  key_i,
                                               MEA_PreParser_Data_dbt* data_o,
                                               MEA_Bool*               found_o )
{
    MEA_Uint32 index;
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key parmeter */
    if (key_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Check for valid data parameter */
    if (data_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Check for valid found_o parameter */
     if (found_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }   



#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
   
	if ( mea_PreParser_GetIndexByKey(key_i, found_o, &index ) != MEA_OK )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_PreParser_GetIndexByKey failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if ( *found_o )
    {                            
        MEA_OS_memcpy(data_o,
                      &(MEA_PreParser_Table[index].data ) , 
                      sizeof(MEA_PreParser_Table[0].data));            
    }

    return MEA_OK;
} 




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_PreParser_Entry_ByIndex>                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_PreParser_Entry_ByIndex ( MEA_Unit_t              unit_i,
                                                 MEA_Uint32              index_i,
                                                 MEA_PreParser_Key_dbt*  key_o,
                                                 MEA_PreParser_Data_dbt* data_o,
                                                 MEA_Bool*               found_o )
{
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key parmeter */
    if (key_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }


    /* Check for valid index parameter */
    MEA_CHECK_PRE_PARSER_INDEX_IN_RANGE(index_i)
 
    /* Check for valid data parameter */
    if (data_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Check for valid found_o parameter */
     if (found_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }   



#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    *found_o = MEA_FALSE;
    
    if ( MEA_PreParser_Table[index_i].valid == MEA_TRUE )
    {
        MEA_OS_memcpy(data_o,
                      &(MEA_PreParser_Table[index_i].data ) , 
                      sizeof(MEA_PreParser_Table[0].data));
        MEA_OS_memcpy(key_o,
                      &(MEA_PreParser_Table[index_i].key ) , 
                      sizeof(MEA_PreParser_Table[0].key)); 
    
        *found_o = MEA_TRUE;
    }
  
	return MEA_OK;
} 





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_PreParser_Entry_ByIndex>                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_PreParser_Entry_ByIndex ( MEA_Unit_t              unit_i,
                                                 MEA_Uint32              index_i,
                                                 MEA_PreParser_Data_dbt* data_i )
{
    MEA_PreParser_Entry_dbt new_entry;
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key parmeter */
    if (data_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Check for valid index parmeter */
    MEA_CHECK_PRE_PARSER_INDEX_IN_RANGE(index_i)

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

                     
    
    if ( MEA_PreParser_Table[index_i].valid == MEA_TRUE )
    {
        /* Update the HW */
        MEA_OS_memcpy( &(new_entry.key),
                       &(MEA_PreParser_Table[index_i].key),
                       sizeof(MEA_PreParser_Table[0].key) );
        MEA_OS_memcpy( &(new_entry.data),
                       data_i,
                       sizeof(MEA_PreParser_Table[0].data) );
        new_entry.valid = MEA_TRUE;
     
        if ( mea_PreParser_UpdateHw(index_i,
                                    &new_entry,
                                    &(MEA_PreParser_Table[index_i]) ) != MEA_OK )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_PreParser_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }       

        /* Update the SW shadow */
        MEA_OS_memcpy(&(MEA_PreParser_Table[index_i].data ), 
                      data_i,
                      sizeof(MEA_PreParser_Table[0].data));
    }
    else
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - No Pre Parser entry on index %d\n",
                         __FUNCTION__, index_i);
       return MEA_ERROR; 
    }    
 
    return MEA_OK;
} 





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_PreParser_Entry_ByKey>                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_PreParser_Entry_ByKey ( MEA_Unit_t              unit_i,
                                               MEA_PreParser_Key_dbt*  key_i,
                                               MEA_PreParser_Data_dbt* data_i )
{
    MEA_Uint32 index;
    MEA_Bool   found;
    MEA_PreParser_Entry_dbt new_entry;
        
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key parmeter */
    if (key_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Check for valid data parmeter */
    if (data_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    if ( mea_PreParser_GetIndexByKey(key_i, &found, &index ) != MEA_OK )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_PreParser_GetIndexByKey failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if ( found )
    {                            
        /* Update the HW */
        MEA_OS_memcpy( &(new_entry.key),
                       &(MEA_PreParser_Table[index].key),
                       sizeof(MEA_PreParser_Table[0].key) );
        MEA_OS_memcpy( &(new_entry.data),
                       data_i,
                       sizeof(MEA_PreParser_Table[0].data) );
        new_entry.valid = MEA_TRUE;
     
        if ( mea_PreParser_UpdateHw(index,
                                    &new_entry,
                                    &(MEA_PreParser_Table[index]) ) != MEA_OK )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_PreParser_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }       



       /* Update the SW shadow */
       MEA_OS_memcpy(&(MEA_PreParser_Table[index].data ) ,
                    data_i,       
                    sizeof(MEA_PreParser_Table[0].data));  
    }
    else
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - No Pre Parser entry with such key\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }		

    return MEA_OK;
} 





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_PreParser_Entry_DeleteByKey>                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_PreParser_Entry_DeleteByKey ( MEA_Unit_t              unit_i,
                                                     MEA_PreParser_Key_dbt*  key_i  )
{
    MEA_Bool                found;
    MEA_Uint32              index;
    MEA_PreParser_Entry_dbt entry;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key parmeter */
    if (key_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if ( mea_PreParser_GetIndexByKey(key_i, &found, &index ) != MEA_OK )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_PreParser_GetIndexByKey failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
         

    if ( found )
    {           
        /* Delete from SW shadow */
        MEA_OS_memset(&(MEA_PreParser_Table[index]),0,sizeof(MEA_PreParser_Entry_dbt));
            
        /* Delete from HW */
        MEA_OS_memset( &entry,
                       0,
                       sizeof(MEA_PreParser_Entry_dbt) );
     
        if ( mea_PreParser_UpdateHw(index,
                                    &entry,
                                    0         )   != MEA_OK )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_PreParser_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }       
    }
    else
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - No Pre Parser entry with such key\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }	
		
    
   
    return MEA_OK;
} 




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_PreParser_Entry_DeleteByIndex>                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_PreParser_Entry_DeleteByIndex ( MEA_Unit_t     unit_i,
                                                       MEA_Uint32     index_i )
{
    MEA_PreParser_Entry_dbt entry;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

 
    /* Check for valid index parmeter */
    MEA_CHECK_PRE_PARSER_INDEX_IN_RANGE(index_i)

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    
    if ( MEA_PreParser_Table[index_i].valid == MEA_TRUE )
    {
        /* Delete from the SW shadow */
        MEA_OS_memset(&(MEA_PreParser_Table[index_i]),0,sizeof(MEA_PreParser_Entry_dbt));

        /* Delete from HW */
        MEA_OS_memset( &entry,
                       0,
                       sizeof(MEA_PreParser_Entry_dbt) );
     
        if ( mea_PreParser_UpdateHw(index_i,
                                    &entry,
                                    0         )   != MEA_OK )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_PreParser_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }       
    }
    else
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - No Pre Parser entry on index %d\n",
                         __FUNCTION__, index_i);
       return MEA_ERROR; 
    }    
 
    return MEA_OK;
} 





MEA_Bool MEA_API_IsPreParserIndexInRange(MEA_Unit_t unit, 
                                         MEA_Uint32 index)
{

   if( index < MEA_PRE_PARSER_MAX_ENTRIES )
   	return MEA_TRUE;
   else
    return MEA_FALSE;		

}

MEA_Bool MEA_API_IsPreParserNetTagInRange(MEA_Unit_t unit, 
                                          MEA_Uint32 net_tag)
{

   if( net_tag <= MEA_PRE_PARSER_LAST_NET_TAG )
   	return MEA_TRUE;
   else
    return MEA_FALSE;		

}


MEA_Bool MEA_API_IsPreParserVspTypeInRange(MEA_Unit_t unit, 
                                           MEA_Uint32 vsp_type)
{

   if( vsp_type <= MEA_PRE_PARSER_LAST_VSP_TYPE )
   	return MEA_TRUE;
   else
    return MEA_FALSE;		

}

MEA_Bool MEA_API_IsPreParserNetTagMaskInRange(MEA_Unit_t unit, 
                                           MEA_Uint32 net_tag_mask)
{

   if( net_tag_mask <= MEA_PRE_PARSER_LAST_NET_TAG_MASK )
   	return MEA_TRUE;
   else
    return MEA_FALSE;		

}






