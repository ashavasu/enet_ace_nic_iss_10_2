/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_flowCoSMapping_drv.h"
#include "mea_if_regs.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef union {
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 valid          :  1;  /*  0.. 0 */
        MEA_Uint32 Color_Policer1 :  1;  /*  1.. 1 */
        MEA_Uint32 Color_Policer0 :  1;  /*  2.. 2 */
        MEA_Uint32 CoS1           :  3;  /*  3.. 5 */
        MEA_Uint32 CoS0           :  3;  /*  6.. 8 */
        MEA_Uint32 pad            : 23;  /*  9..31 */
#else
        MEA_Uint32 pad            : 23;  /*  9..31 */
        MEA_Uint32 CoS0           :  3;  /*  6.. 8 */
        MEA_Uint32 CoS1           :  3;  /*  3.. 5 */
        MEA_Uint32 Color_Policer0 :  1;  /*  2.. 2 */
        MEA_Uint32 Color_Policer1 :  1;  /*  1.. 1 */
        MEA_Uint32 valid          :  1;  /*  0.. 0 */
#endif
    } val;
    MEA_Uint32 reg;
} mea_drv_FlowCoSMappingProfile_hwEntry_dbt;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static MEA_FlowCoSMappingProfile_Entry_dbp *MEA_FlowCoSMappingProfile_Table    = NULL;
static MEA_Bool                             MEA_FlowCoSMappingProfile_InitDone = MEA_FALSE;


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_mea_drv_FlowCoSMappingProfile_UpdateHwEntry>              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_FlowCoSMappingProfile_UpdateHwEntry(
    MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                                 unit_i     = (MEA_Unit_t                                )arg1;
    //MEA_db_HwUnit_t                            hwUnit_i   = (MEA_db_HwUnit_t                           )arg2;
    //MEA_Uint32                                 hwId_i     = (MEA_Uint32                                )arg3;
    mea_drv_FlowCoSMappingProfile_hwEntry_dbt* hwEntry_pi = (mea_drv_FlowCoSMappingProfile_hwEntry_dbt*)arg4;

    MEA_API_WriteReg(unit_i,
                     MEA_IF_WRITE_DATA_REG(0),
                     hwEntry_pi->reg,
                     MEA_MODULE_IF);  


    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_mea_drv_FlowCoSMappingProfile_UpdateHw>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_FlowCoSMappingProfile_UpdateHw
                        (MEA_Unit_t unit_i,
                         MEA_FlowCoSMappingProfile_Id_t id_i,
                         MEA_FlowCoSMappingProfile_Entry_dbt* new_entry_pi,
                         MEA_FlowCoSMappingProfile_Entry_dbt* old_entry_pi)
{

    MEA_ind_write_t      ind_write;
    mea_drv_FlowCoSMappingProfile_hwEntry_dbt    hwEntry;
    MEA_db_HwUnit_t hwUnit;
    MEA_Uint32 i;
    MEA_Uint32 numofparameters;
    MEA_Uint32 index;
    MEA_Uint32 j;

    /* Get hwUnit */
    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    /* Check if support */
    if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check new_entry_pi parameter */
    if (new_entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - new_entry_pi == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_i */
    if (id_i >= MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES);
        return MEA_ERROR;
    }

    if(mea_drv_Get_DeviceInfo_flowCos_Mapping_Support(unit_i,hwUnit) == MEA_FALSE){
        return MEA_OK;
    }
    /*
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_ETHERTYPE=0,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_INNER_VLAN,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_PRECEDENCE,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_DSCP,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_TOS,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_LAST
*/

    /* Prepare ind_write structure */
    ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING;

    switch(new_entry_pi->type){
        case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_ETHERTYPE:    /* Untag*/
                ind_write.tableOffset      = (id_i * ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS(hwUnit) + 0 /*+ 0*/); // 0
                numofparameters =MEA_FLOW_COS_MAPPING_NUM_OF_ITEMS(MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_ETHERTYPE)/2;
            break;
        case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_INNER_VLAN:
                ind_write.tableOffset      = (id_i * ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS(hwUnit) + 0 /*+ 8*/);    // 8
                numofparameters =MEA_FLOW_COS_MAPPING_NUM_OF_ITEMS(MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_INNER_VLAN)/2;
            break;
        case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN:
            ind_write.tableOffset      = (id_i * ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS(hwUnit) + 0 /*+ 4*/);      // 4
            numofparameters =MEA_FLOW_COS_MAPPING_NUM_OF_ITEMS(MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN)/2;
            break;
        case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_DSCP:
            ind_write.tableOffset      = (id_i * ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS(hwUnit));        // 
                numofparameters =MEA_FLOW_COS_MAPPING_NUM_OF_ITEMS(MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_DSCP)/2;
            break;
        case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_PRECEDENCE:
            ind_write.tableOffset      = (id_i * ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS(hwUnit) + 0 /*+12*/);    // 12
                numofparameters =MEA_FLOW_COS_MAPPING_NUM_OF_ITEMS(MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_PRECEDENCE)/2;
            break;
        case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_TOS:
                ind_write.tableOffset      = (id_i * ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS(hwUnit) + 0 /*+16*/); // 16
                numofparameters =MEA_FLOW_COS_MAPPING_NUM_OF_ITEMS(MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_TOS)/2;
            break;
        default:
             ind_write.tableOffset      = (id_i * ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS(hwUnit) );
             numofparameters =ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS(hwUnit);
            break;
    }
    
    ind_write.cmdReg           = MEA_IF_CMD_REG;      
    ind_write.cmdMask          = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask       = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg     = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry       = (MEA_FUNCPTR)mea_drv_FlowCoSMappingProfile_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)ind_write.tableOffset);
    ind_write.funcParam4 = (MEA_funcParam)(&hwEntry);

    index = ind_write.tableOffset;
    /* Loop on all items in this profile */
    for (i=0;
         i< numofparameters ;
         i++,ind_write.tableOffset++,index++ /*ind_write.funcParam3++*/ ) {
        ind_write.funcParam3 = (MEA_funcParam)((long)index);
         
         j = i*2;

         /* Check for no change */
         if ((MEA_FlowCoSMappingProfile_InitDone) &&
             (old_entry_pi != NULL) &&
             (MEA_OS_memcmp(&(new_entry_pi->item_table[j]),
                            &(old_entry_pi->item_table[j]),
                            sizeof(new_entry_pi->item_table[j])) == 0) &&
             (MEA_OS_memcmp(&(new_entry_pi->item_table[j+1]),
                            &(old_entry_pi->item_table[j+1]),
                            sizeof(new_entry_pi->item_table[j+1])) == 0)) {
                continue;
         }

         /* Build hwEntry */
         MEA_OS_memset(&hwEntry,0,sizeof(hwEntry));
         hwEntry.val.valid          = new_entry_pi->item_table[j  ].valid;
         hwEntry.val.Color_Policer0 = new_entry_pi->item_table[j  ].COLOR;
         hwEntry.val.CoS0           = new_entry_pi->item_table[j  ].COS;
         hwEntry.val.Color_Policer1 = new_entry_pi->item_table[j+1].COLOR;
         hwEntry.val.CoS1           = new_entry_pi->item_table[j+1].COS;

        /* Write to the Indirect Table */
        if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                              __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }

    }

    /* Return to Caller */
    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Init_FlowCoSMappingProfile_Table>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_FlowCoSMappingProfile_Table(MEA_Unit_t unit_i)
{
    MEA_Uint32                     size;
    MEA_FlowCoSMappingProfile_Id_t id;
    MEA_FlowCoSMappingProfile_Entry_dbt entry;

    if(b_bist_test){
        return MEA_OK;
    }
    /* Init Flow CoS Mapping Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Flow CoS Mapping  table ...\n");
    
    if (MEA_FlowCoSMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Already Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {

        /* Allocate the MEA_FlowCoSMappingProfile_Table */
        size = MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES * 
               sizeof(MEA_FlowCoSMappingProfile_Entry_dbp);
        MEA_FlowCoSMappingProfile_Table = 
            (MEA_FlowCoSMappingProfile_Entry_dbp*)MEA_OS_malloc (size);
        if (MEA_FlowCoSMappingProfile_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_FlowCoSMappingProfile_Table == NULL\n",
                              __FUNCTION__);
            return MEA_ERROR;
        }
        MEA_OS_memset(MEA_FlowCoSMappingProfile_Table,0,size);


        /* Update Hardware */
        MEA_OS_memset(&entry,0,sizeof(entry));
        entry.type=MEA_FLOW_COS_MAPPING_PROFILE_TYPE_LAST;
        for (id=0;
             id<(MEA_Uint32)MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES;
             id++) {
            if (mea_drv_FlowCoSMappingProfile_UpdateHw(unit_i,
                                                       id,
                                                       &entry,
                                                       NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_drv_FlowCoSMappingProfile_UpdateHw failed "
                                  "(id=%d)\n",
                                  __FUNCTION__,id);
                return MEA_ERROR;
            }
        }
    }


    /* Set Init Done */
    MEA_FlowCoSMappingProfile_InitDone = MEA_TRUE;     

    /* Return to Caller */
    return MEA_OK; 
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_FlowCoSMappingProfile_Table>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_FlowCoSMappingProfile_Table(MEA_Unit_t unit_i)
{
    MEA_FlowCoSMappingProfile_Id_t id;

    if (b_bist_test) {
        return MEA_OK;
    }

    if (!MEA_FlowCoSMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    MEA_FlowCoSMappingProfile_InitDone = MEA_FALSE;

    if (MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {

        /* Update Hardware */
        for (id=0;
             id<(MEA_Uint32)MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES;
             id++) {

            if (MEA_FlowCoSMappingProfile_Table[id] == NULL) {
                continue;
            }

            if (mea_drv_FlowCoSMappingProfile_UpdateHw(unit_i,
                                                       id,
                                                       MEA_FlowCoSMappingProfile_Table[id],
                                                       NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_drv_FlowCoSMappingProfile_UpdateHw failed "
                                  "(id=%d)\n",
                                  __FUNCTION__,id);
                return MEA_ERROR;
            }
        }
    }

    MEA_FlowCoSMappingProfile_InitDone = MEA_TRUE;

    /* Return to Caller */
    return MEA_OK; 
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Conclude_FlowCoSMappingProfile_Table>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Conclude_FlowCoSMappingProfile_Table(MEA_Unit_t unit_i)
{

    MEA_FlowCoSMappingProfile_Id_t id;

    if (!MEA_FlowCoSMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {

        if (MEA_FlowCoSMappingProfile_Table) {
            for (id=0;
                 id<(MEA_Uint32)MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES;
                 id++) {
                if (MEA_FlowCoSMappingProfile_Table[id] == NULL) {
                    continue;
                }
                MEA_OS_free(MEA_FlowCoSMappingProfile_Table[id]);
                MEA_FlowCoSMappingProfile_Table[id] = NULL;

            }

            MEA_OS_free(MEA_FlowCoSMappingProfile_Table);
            MEA_FlowCoSMappingProfile_Table = NULL;
        }
    }

    /* Reset Init Done */
    MEA_FlowCoSMappingProfile_InitDone = MEA_FALSE;     

    /* Return to Caller */
    return MEA_OK; 
}


/*----------------------------------------------------------------------------*/
/*             API functions implementation                                   */
/*----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Create_FlowCoSMappingProfile_Entry>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_pi,
                                     MEA_FlowCoSMappingProfile_Id_t      *id_io)
{

    MEA_FlowCoSMappingProfile_Entry_dbt *entry;
    MEA_FlowCoSMappingProfile_Id_t       id;
    MEA_Uint32                           item;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_FlowCoSMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check entry_pi paramter*/
    if (entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_pi == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check the type */
    switch (entry_pi->type) {
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_ETHERTYPE:
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_INNER_VLAN:
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN:
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_PRECEDENCE:  
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_DSCP:
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_TOS:
        break;
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown type %d \n",
                          __FUNCTION__,entry_pi->type);
        return MEA_ERROR;
    }

    /* Check that each pair items has same valid value */
    for (item=0;item<MEA_NUM_OF_ELEMENTS(entry_pi->item_table);item+=2) {
        if (entry_pi->item_table[item].valid != entry_pi->item_table[item+1].valid) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - item_table[%d].valid (%d) != item_table[%d].valid (%d) \n",
                              __FUNCTION__,
                              item,
                              entry_pi->item_table[item].valid,
                              item+1,
                              entry_pi->item_table[item+1].valid);
            return MEA_ERROR;
        }
    }

    /* Check id_io parameter*/
    if (id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_io == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_io parameter value */
    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID) {
        if ((*id_io) >= MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - id_i (%d) >= max value (%d) \n",
                              __FUNCTION__,
                              *id_io,
                              MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES);
            return MEA_ERROR;
        }
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID) {
        if (MEA_FlowCoSMappingProfile_Table[(*id_io)] != NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - entry %d already exist \n",
                              __FUNCTION__,(*id_io));
            return MEA_ERROR;
        }
        id = (*id_io);
    } else {
        for (id=0;
             id<(MEA_Uint32)MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES;
             id++) {
             if (MEA_FlowCoSMappingProfile_Table[id] == NULL) {
                 break;
             }
        }
        if (id>=(MEA_Uint32)MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - No free entries \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

    }

    /* Allocate the entry */
    entry  = MEA_OS_malloc(sizeof(MEA_FlowCoSMappingProfile_Entry_dbt));
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate entry failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Copy input to entry */
    MEA_OS_memcpy(entry,entry_pi,sizeof(*entry));

    /* Update Hardware */
    if (mea_drv_FlowCoSMappingProfile_UpdateHw(unit_i,
                                               id,
                                               entry,
                                               NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_FlowCoSMappingProfile_UpdateHw failed (id=%d) \n",
                          __FUNCTION__,
                          id);
        return MEA_ERROR;
    }


    /* Add to the table */
    MEA_FlowCoSMappingProfile_Table[id] = entry;

    /* Update output parameter */
    (*id_io) = id;

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Delete_FlowCoSMappingProfile_Entry>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t       id_i)
{
    MEA_FlowCoSMappingProfile_Entry_dbt entry;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_FlowCoSMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_i paramter value */
    if (id_i >= MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES);
        return MEA_ERROR;
    }


    /* Check if exist */
    if (MEA_FlowCoSMappingProfile_Table[id_i] == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* zero entry */
    MEA_OS_memset(&entry,0,sizeof(entry));
    entry.type=MEA_FLOW_COS_MAPPING_PROFILE_TYPE_LAST;
    /* Update Hardware */
    if (mea_drv_FlowCoSMappingProfile_UpdateHw(unit_i,
                                               id_i,
                                               &entry,
                                               MEA_FlowCoSMappingProfile_Table[id_i]) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_FlowCoSMappingProfile_UpdateHw failed (id=%d) \n",
                          __FUNCTION__,
                          id_i);
        return MEA_ERROR;
    }



    /* Delete from table */
    MEA_OS_free(MEA_FlowCoSMappingProfile_Table[id_i]);
    MEA_FlowCoSMappingProfile_Table[id_i] = NULL;


    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Set_FlowCoSMappingProfile_Entry>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t       id_i,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_pi)
{

    MEA_Uint32 item;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_FlowCoSMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check entry_pi paramter*/
    if (entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_pi == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_i paramter value */
    if (id_i >= MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES);
        return MEA_ERROR;
    }

    /* Check if exist */
    if (MEA_FlowCoSMappingProfile_Table[id_i] == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }

    /* Check the type */
    switch (entry_pi->type) {
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_ETHERTYPE:
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_INNER_VLAN:
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN:
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_PRECEDENCE:  
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_DSCP:
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_TOS:
        break;
    case MEA_FLOW_COS_MAPPING_PROFILE_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown type %d \n",
                          __FUNCTION__,entry_pi->type);
        return MEA_ERROR;
    }

    /* Check that each pair items has same valid value */
    for (item=0;item<MEA_NUM_OF_ELEMENTS(entry_pi->item_table);item+=2) {
        if (entry_pi->item_table[item].valid != entry_pi->item_table[item+1].valid) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - item_table[%d].valid (%d) != item_table[%d].valid (%d) \n",
                              __FUNCTION__,
                              item,
                              entry_pi->item_table[item].valid,
                              item+1,
                              entry_pi->item_table[item+1].valid);
            return MEA_ERROR;
        }
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* Update Hardware */
    if (mea_drv_FlowCoSMappingProfile_UpdateHw(unit_i,
                                               id_i,
                                               entry_pi,
                                               MEA_FlowCoSMappingProfile_Table[id_i]) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_FlowCoSMappingProfile_UpdateHw failed (id=%d) \n",
                          __FUNCTION__,
                          id_i);
        return MEA_ERROR;
    }


    /* Update shadow */
    MEA_OS_memcpy(MEA_FlowCoSMappingProfile_Table[id_i],
                  entry_pi,
                  sizeof(*entry_pi));

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Get_FlowCoSMappingProfile_Entry>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t       id_i,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_po)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_FlowCoSMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }

    /* Check id_i paramter value */
    if (id_i >= MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_i (%d) >= max value (%d) \n",
                          __FUNCTION__,
                          id_i,
                          MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES);
        return MEA_ERROR;
    }


    /* Check if exist */
    if (MEA_FlowCoSMappingProfile_Table[id_i] == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry %d not exist \n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Update output variable */
    if (entry_po != NULL) {
        MEA_OS_memcpy(entry_po,
                      MEA_FlowCoSMappingProfile_Table[id_i],
                      sizeof(*entry_po));
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_GetFirst_FlowCoSMappingProfile_Entry>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t      *id_o,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_po,
                                     MEA_Bool                            *found_o)
{


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_FlowCoSMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }


    if (id_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_o == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    for ((*id_o)=0;(*id_o)<MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES;(*id_o)++) {
        if (MEA_FlowCoSMappingProfile_Table[(*id_o)] == NULL) {
            continue;
        }
        if (found_o) {
            *found_o = MEA_TRUE;
        }
        if (entry_po != NULL) {
            MEA_OS_memcpy(entry_po,
                          MEA_FlowCoSMappingProfile_Table[(*id_o)],
                          sizeof(*entry_po));
        }
        return MEA_OK;
    }

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    /* Return to caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_GetNext_FlowCoSMappingProfile_Entry>                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t      *id_io,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_po,
                                     MEA_Bool                            *found_o)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_FlowCoSMappingProfile_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not Support \n",__FUNCTION__);
        return MEA_ERROR;
    }


    if (id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - id_io == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    for ((*id_io)++;(*id_io)<MEA_FLOW_COS_MAPPING_PROFILE_MAX_ENTRIES;(*id_io)++) {
        if (MEA_FlowCoSMappingProfile_Table[(*id_io)] == NULL) {
            continue;
        }
        if (found_o) {
            *found_o = MEA_TRUE;
        }
        if (entry_po != NULL) {
            MEA_OS_memcpy(entry_po,
                          MEA_FlowCoSMappingProfile_Table[(*id_io)],
                          sizeof(*entry_po));
        }
        return MEA_OK;
    }

    if (found_o) {
        *found_o = MEA_FALSE;
    }
   
    /* Return to caller */
    return MEA_OK;
}

