/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_drv_common.h"

#include "MEA_platform_os_memory.h"
#include "enet_xPermission_drv.h"
#include "mea_action_drv.h"
#include "mea_if_Acl5_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

#define MEA_ACL5_MAX_ENTRY_HW   ((16*1024*1024)/64)     //16M Byte   etch entry is 64 Byte
#define MEA_ACL5_IPv6_SIG_NUMBER_OF_HASH 32
#define MEA_ACL5_NUMBER_OF_HASH 17
#define MEA_ACL5_NUMBER_OF_REGISTERS 12
#define MEA_ACL5_NUMBER_OF_IPV6_REGISTERS 4
#define MEA_ACL5_NUMBER_OF_ACTION_REGISTERS 4



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/



typedef struct {
    MEA_Bool Valid;
    MEA_ACL5_Key_dbt key;
    MEA_ACL5_Entry_Data_dbt Data;
    // privet
    MEA_Uint32 HashAddress; 
	ENET_xPermissionId_t xPermissionId;
	MEA_Uint32 key_data[MEA_ACL5_NUMBER_OF_REGISTERS];
	MEA_Uint32 action_data[MEA_ACL5_NUMBER_OF_ACTION_REGISTERS];
	/*** Save the hw info for warm reset only ****/
	MEA_Uint16 save_hw_xPermissionId;

} MEA_ACL5_Entry_info_dbt;
	



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
MEA_Bool                    MEA_ACL5InitDone = MEA_FALSE;
MEA_ACL5_Entry_info_dbt	    *MEA_ACL5_Table = NULL;

MEA_Uint32					*acl5_hash_to_ids = NULL;

static MEA_Uint32           *MEA_ACL5_External_BIT = NULL;       /* software external */
static MEA_Uint32           *MEA_ACL5_External_Hash_BIT = NULL;  /* hash address forDDR*/


static MEA_Uint32	MEA_HASH_ACL5_FUNC1[][MEA_ACL5_NUMBER_OF_REGISTERS]=   {{ 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00fff000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000fff }, // 16
																			{ 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x0000000f, 0xff000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00fff000 }, // 15
																			{ 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x0000fff0, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x0000000f, 0xff000000 },
																			{ 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x0fff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x0000fff0, 0x00000000 },
	/*In this array:*/														{ 0x00000000, 0x00000000, 0x00000000, 0x000000ff, 0xf0000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x0fff0000, 0x00000000 },
	/*row 0 is hash function number 16*/									{ 0x00000000, 0x00000000, 0x00000000, 0x000fff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x000000ff, 0xf0000000, 0x00000000 },
	/*row 16 is hash function number 0*/									{ 0x00000000, 0x00000000, 0x00000000, 0xfff00000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x000fff00, 0x00000000, 0x00000000 }, // 10
	/*column 0 is MSB register */											{ 0x00000000, 0x00000000, 0x00000fff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xfff00000, 0x00000000, 0x00000000 },
	/*column 16 is LSB register*/											{ 0x00000000, 0x00000000, 0x00fff000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000fff, 0x00000000, 0x00000000, 0x00000000 },
																			{ 0x00000000, 0x0000000f, 0xff000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00fff000, 0x00000000, 0x00000000, 0x00000000 },
																			{ 0x00000000, 0x0000fff0, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x0000000f, 0xff000000, 0x00000000, 0x00000000, 0x00000000 },
																			{ 0x00000000, 0x0fff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x0000fff0, 0x00000000, 0x00000000, 0x00000000, 0x00000000 }, // 5
																			{ 0x000000ff, 0xf0000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x0fff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000 },
																			{ 0x000fff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x000000ff, 0xf0000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000 },
																			{ 0xfff00000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x000fff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000 },
																			{ 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xfff00000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000 },
																			{ 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000fff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000 }}; // 0

static MEA_Uint32	MEA_HASH_ACL5_IPV6_FUNC1[][MEA_ACL5_NUMBER_OF_IPV6_REGISTERS] = { { 0x00000000, 0x00000000, 0x00000000, 0xff000000 },
																					  { 0x00000000, 0x00000000, 0x000000ff, 0x00000000},
																					  { 0x00000000, 0x00000000, 0x0000ff00, 0x00000000},
																					  { 0x00000000, 0x00000000, 0x00ff0000, 0x00000000},
																					  { 0x00000000, 0x00000000, 0xff000000, 0x00000000},
																					  { 0x00000000, 0x000000ff, 0x00000000, 0x00000000},
																					  { 0x00000000, 0x0000ff00, 0x00000000, 0x00000000},
																					  { 0x00000000, 0x00ff0000, 0x00000000, 0x00000000},
																					  { 0x00000000, 0xff000000, 0x00000000, 0x00000000},
																					  { 0x000000ff, 0x00000000, 0x00000000, 0x00000000},
																					  { 0x0000ff00, 0x00000000, 0x00000000, 0x00000000},
																					  { 0x00ff0000, 0x00000000, 0x00000000, 0x00000000},
																					  { 0xff000000, 0x00000000, 0x00000000, 0x00000000},
																					  { 0x00000000, 0x00000000, 0x00000000, 0x000000ff},
																					  { 0x00000000, 0x00000000, 0x00000000, 0x0000ff00},
																					  { 0x00000000, 0x00000000, 0x00000000, 0x00ff0000},
																					  { 0x00000000, 0x00000000, 0x00000000, 0xff000000},
																					  { 0x00000000, 0x00000000, 0x000000ff, 0x00000000},
																					  { 0x00000000, 0x00000000, 0x0000ff00, 0x00000000},
																					  { 0x00000000, 0x00000000, 0x00ff0000, 0x00000000},
																					  { 0x00000000, 0x00000000, 0xff000000, 0x00000000},
																					  { 0x00000000, 0x000000ff, 0x00000000, 0x00000000},
																					  { 0x00000000, 0x0000ff00, 0x00000000, 0x00000000},
																					  { 0x00000000, 0x00ff0000, 0x00000000, 0x00000000},
																					  { 0x00000000, 0xff000000, 0x00000000, 0x00000000},
																					  { 0x000000ff, 0x00000000, 0x00000000, 0x00000000},
																					  { 0x0000ff00, 0x00000000, 0x00000000, 0x00000000},
																					  { 0x00ff0000, 0x00000000, 0x00000000, 0x00000000},
																					  { 0xff000000, 0x00000000, 0x00000000, 0x00000000},
																					  { 0x00000000, 0x00000000, 0x00000000, 0x00000fff},
																					  { 0x00000000, 0x00000000, 0x00000000, 0x00fff000},
																					  { 0x00000000, 0x00000000, 0x0000000f, 0xff000000} };

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/

/*-------------------------------- static function   ----------------------------------*/

static MEA_Bool mea_drv_ACL5_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i);

static MEA_Status mea_drv_ACL5_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist);

static MEA_Status mea_drv_ACL5_Update_TLB_Key(MEA_Unit_t       unit_i,
	MEA_Uint32				*data,
	MEA_Uint32				*hash_addressEntry, 
	MEA_ACL5Id_t				Id_io);

static MEA_Bool mea_drv_ACL5_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t				*Id_io);

static MEA_Status mea_drv_ACL5_UpdateHwEntry(MEA_Unit_t unit_i,
	MEA_ACL5Id_t Id,
	MEA_Uint32 *reg_key,
	MEA_SETTING_TYPE_te   type);

static MEA_Uint32 MEA_if_ACL5_Ipv6Sig_Hash(MEA_Uint32 *ipv6_address, MEA_Uint32	HASH_ACL5_IPV6_FUNC[][MEA_ACL5_NUMBER_OF_IPV6_REGISTERS]);
	
static MEA_Uint32 MEA_if_ACL5_Hash(MEA_Uint32 *key_data, MEA_Uint32	HASH_ACL5_FUNC[][MEA_ACL5_NUMBER_OF_REGISTERS]);

static MEA_Status MEA_If_ACL5_Init_Key(MEA_ACL5_Key_dbt  *key_entry, MEA_Uint32 *reg);

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

static MEA_Bool mea_drv_ACL5_IsRange(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i)
{

	if ((id_i) >= MEA_ACL5_MAX_ENTRY_SW) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "out range id_i %d >= \n", id_i, MEA_ACL5_MAX_ENTRY_SW);
		return MEA_FALSE;
	}
	return MEA_TRUE;
}

static MEA_Status mea_drv_ACL5_Update_TLB_Key(MEA_Unit_t       unit_i,
	MEA_Uint32				*data,
	MEA_Uint32				*hash_addressEntry,
	MEA_ACL5Id_t				Id_i)
{
	MEA_Uint32 hash_address;

	hash_address = *hash_addressEntry * 2;

	if (acl5_hash_to_ids[hash_address] != 0 && acl5_hash_to_ids[hash_address + 1] == 0)
	{
		if (!MEA_OS_memcmp(MEA_ACL5_Table[acl5_hash_to_ids[hash_address]].key_data, &data, MEA_ACL5_NUMBER_OF_REGISTERS))
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR mea_drv_ACL5_Update_TLB_Key  key is alredy exist in address 0x%05x\n", hash_address);
			return MEA_ERROR;
		}
		else
			hash_address += 1;
	}
	else
		if (acl5_hash_to_ids[hash_address] == 0 && acl5_hash_to_ids[hash_address + 1] != 0)
		{
			if (!MEA_OS_memcmp(MEA_ACL5_Table[acl5_hash_to_ids[hash_address+1]].key_data, &data, MEA_ACL5_NUMBER_OF_REGISTERS))
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR mea_drv_ACL5_Update_TLB_Key  key is alredy exist in address 0x%05x\n", hash_address);
				return MEA_ERROR;
			}
			
		}
		else
		{
			if (acl5_hash_to_ids[hash_address] != 0 && acl5_hash_to_ids[hash_address + 1] != 0)
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR mea_drv_ACL5_Update_TLB_Key  failed - collision  \n");
				return MEA_ERROR;
			}
		}

	acl5_hash_to_ids[hash_address] = Id_i;
	MEA_OS_memcpy(&MEA_ACL5_Table[Id_i].key_data, data, sizeof(MEA_ACL5_Table[Id_i].key_data));
	*hash_addressEntry = hash_address;

	
	return MEA_OK;
}


static MEA_Bool mea_drv_ACL5_find_free(MEA_Unit_t       unit_i,
	MEA_ACL5Id_t       *id_io)
{
	MEA_Uint32 i;

	for (i = 0; i < MEA_ACL5_MAX_ENTRY_SW; i++) {
		if (MEA_ACL5_Table[i].Valid == MEA_FALSE) {
			*id_io = i;
			return MEA_TRUE;
		}
	}
	return MEA_FALSE;

}

static MEA_Status mea_drv_ACL5_IsExist(MEA_Unit_t     unit_i,
	MEA_ACL5Id_t id_i,
	MEA_Bool silent,
	MEA_Bool *exist)
{

	if (exist == NULL) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);

		return MEA_ERROR;
	}
	*exist = MEA_FALSE;

	if (mea_drv_ACL5_IsRange(unit_i, id_i) != MEA_TRUE) {
		if (!silent)
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ACL5_IsRange failed  for id=%d  \n", __FUNCTION__, id_i);

		return MEA_ERROR;
	}

	if (MEA_ACL5_Table[id_i].Valid == MEA_TRUE) {
		*exist = MEA_TRUE;
		return MEA_OK;
	}

	return MEA_OK;
}


#if 0
static MEA_Status mea_ACL5_GetFree(MEA_ACL5Id_t *Id)
{

	MEA_Uint32 from, to;
	MEA_Bool      forceId = MEA_FALSE;

	if (*Id == ENET_PLAT_GENERATE_NEW_ID || *Id == 0) {
		/* Calculate from and to */
		from = 1; /* start from 1 Zero cant be set */
		to = MEA_ACL5_MAX_ENTRY_SW - 1;
	}
	else {
		forceId = MEA_TRUE;
		from = *Id;
		to = *Id;
		if (*Id > (MEA_ACL5_MAX_ENTRY_SW - 1)) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -ACL5 MAX of entries %d \n", __FUNCTION__, MEA_ACL5_MAX_ENTRY_SW - 1);
			return MEA_ERROR;
		}
	}

	/* Search for free entry */
	for ((*Id) = from; (*Id) <= to; (*Id)++) {
		if (*Id == ENET_PLAT_GENERATE_NEW_ID) /*not sid == ENET_PLAT_GENERATE_NEW_ID cant be set*/
			continue;

		if (MEA_IS_CLEAR_SID(MEA_ACL5_Table, *Id) == MEA_TRUE) {
			MEA_SET_SID(MEA_ACL5_Table, *Id);
			return MEA_OK;
		}
	}

	if (forceId == MEA_FALSE) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  No free External ACL5 id  %d \n", __FUNCTION__, MEA_ACL5_MAX_ENTRY_SW - 1);

	}
	else {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  ACL5_id %d  is all ready created \n", __FUNCTION__, *Id);
	}
	return MEA_ERROR;

}

#endif


static MEA_Uint32 MEA_if_ACL5_Ipv6Sig_Hash(MEA_Uint32 *ipv6_address, MEA_Uint32	HASH_ACL5_IPV6_FUNC[][MEA_ACL5_NUMBER_OF_IPV6_REGISTERS])
{
	MEA_Uint32 reg_hash = 0;
	MEA_Uint32 i;
	MEA_Uint32 j;
	MEA_Uint32 xor_result = 0;
	MEA_Uint32 HashAddress = 0;



	for (i = 0; i < MEA_ACL5_IPv6_SIG_NUMBER_OF_HASH; i++)
	{
		for (j = 0; j < MEA_ACL5_NUMBER_OF_IPV6_REGISTERS; j++)
		{
			reg_hash = ipv6_address[j] & HASH_ACL5_IPV6_FUNC[31 - i][3 - j];
			xor_result ^= reg_hash;
			reg_hash = 0;
		}

		xor_result = ((MEA_OS_calc_Xor_32_bits(xor_result)) & 0x1);
		xor_result = xor_result << i;
		HashAddress |= xor_result;
		xor_result = 0;
	}
	return HashAddress;
}

static MEA_Uint32 MEA_if_ACL5_Hash(MEA_Uint32 *key_data, MEA_Uint32	HASH_ACL5_FUNC[][MEA_ACL5_NUMBER_OF_REGISTERS])
{
	
	MEA_Uint32 reg_hash=  0 ;
	MEA_Uint32 i;
	MEA_Uint32 j;
	MEA_Uint32 xor_result=0;
	MEA_Uint32 HashAddress=0;

	

	for (i = 0; i < MEA_ACL5_NUMBER_OF_HASH; i++)
	{
		for (j = 0; j < MEA_ACL5_NUMBER_OF_REGISTERS; j++)
		{
			reg_hash = key_data[j] & HASH_ACL5_FUNC[16-i][11 - j];
			xor_result ^= reg_hash;
			reg_hash = 0;
		}

		xor_result= ((MEA_OS_calc_Xor_32_bits(xor_result)) & 0x1 );
		xor_result = xor_result << i;
		HashAddress |= xor_result;
		xor_result = 0;
	}
	return HashAddress;
}

static MEA_Status MEA_If_ACL5_Init_Key(MEA_ACL5_Key_dbt  *key_entry, MEA_Uint32 *reg)
{
	MEA_Uint32                 val[MEA_ACL5_NUMBER_OF_REGISTERS];
	MEA_Uint32                 i = 0;
	MEA_Uint32                 count_shift = 0;
	MEA_Uint32				   tag3;


	for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
		val[i] = 0;
	}

	switch (key_entry->keyType)
	{
	case MEA_ACL5_KeyType_NONE:
		break;
	case MEA_ACL5_KeyType_L2:
	{
		count_shift = 100;
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_UMB_WIDTH,
			((MEA_Uint32)key_entry->layer2.Mac.MAC_UMB),
			&val);
		count_shift += MEA_IF_ACL5_UMB_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_SID_WIDTH,
			((MEA_Uint32)key_entry->ACLprof),
			&val);
		count_shift += MEA_IF_ACL5_SID_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L3_EtherType),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.Ip_Protocol_NH),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4dst),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4src),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.DSCP_TC),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)(key_entry->Acl5_mask_prof.range_mask_id >> 7)),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH +1;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv4_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.Inner_Frame),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_KEY_TYPE_WIDTH,
			((MEA_Uint32)key_entry->keyType),
			&val);
		count_shift += MEA_IF_ACL5_KEY_TYPE_WIDTH;

		for (i = 0; i < 6; i++)
		{
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->layer2.Mac.MacDA.b[5-i]),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
		}

		for (i = 0; i < 6; i++)
		{
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->layer2.Mac.MacSA.b[5-i]),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
		}
		
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG2_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag2),
			&val);
		count_shift += MEA_IF_ACL5_TAG2_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag3),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_P_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1_p),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_P_WIDTH + 73;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG2),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG3),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1_p),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv6FlowLabel),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
	}
		break;
	case MEA_ACL5_KeyType_IPV4:
	{
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.ethertype3),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer3.ip_protocol),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.dst_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.src_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer4.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_DSCP_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_dscp_value.ipv4_dscp.dscp),
			&val);
		count_shift += MEA_IF_ACL5_DSCP_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_PRIORITY_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_pri_type),
			&val);
		count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.range_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH + 1;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_RNG_RSLT_WIDTH,
			((MEA_Uint32)key_entry->rangkProf_rule),
			&val);
		count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.ip_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_UMB_WIDTH,
			((MEA_Uint32)key_entry->layer2.Mac.MAC_UMB),
			&val);
		count_shift += MEA_IF_ACL5_UMB_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_SID_WIDTH,
			((MEA_Uint32)key_entry->ACLprof),
			&val);
		count_shift += MEA_IF_ACL5_SID_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L3_EtherType),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.Ip_Protocol_NH),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4dst),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4src),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.DSCP_TC),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)(key_entry->Acl5_mask_prof.range_mask_id >> 7)),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv4_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.Inner_Frame),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_KEY_TYPE_WIDTH,
			((MEA_Uint32)key_entry->keyType),
			&val);
		count_shift += MEA_IF_ACL5_KEY_TYPE_WIDTH;

		for (i = 0; i < 6; i++)
		{
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->layer2.Mac.MacDA.b[5 - i]),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
		}

		for (i = 0; i < 6; i++)
		{
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->layer2.Mac.MacSA.b[5 - i]),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
		}

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG2_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag2),
			&val);
		count_shift += MEA_IF_ACL5_TAG2_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag3),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_P_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1_p),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_P_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_IP_FLAGS_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv4_Flags),
			&val);
		count_shift += MEA_IF_ACL5_IP_FLAGS_WIDTH + 7;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv4.src_ip),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv4.dst_ip),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG2),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG3),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1_p),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv6FlowLabel),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
	}
		break;
	case MEA_ACL5_KeyType_IPV6_FULL:
	{
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.ethertype3),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer3.ip_protocol),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.dst_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.src_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer4.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_DSCP_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_dscp_value.ipv4_dscp.dscp),
			&val);
		count_shift += MEA_IF_ACL5_DSCP_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_PRIORITY_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_pri_type),
			&val);
		count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.range_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH + 1;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_RNG_RSLT_WIDTH,
			((MEA_Uint32)key_entry->rangkProf_rule),
			&val);
		count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.ip_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_UMB_WIDTH,
			((MEA_Uint32)key_entry->layer2.Mac.MAC_UMB),
			&val);
		count_shift += MEA_IF_ACL5_UMB_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_SID_WIDTH,
			((MEA_Uint32)key_entry->ACLprof),
			&val);
		count_shift += MEA_IF_ACL5_SID_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L3_EtherType),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.Ip_Protocol_NH),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4dst),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4src),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.DSCP_TC),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)(key_entry->Acl5_mask_prof.range_mask_id >> 7)),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv4_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.Inner_Frame),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_KEY_TYPE_WIDTH,
			((MEA_Uint32)key_entry->keyType),
			&val);
		count_shift += MEA_IF_ACL5_KEY_TYPE_WIDTH;

		for (i = 0; i < 4; i++)
		{
			MEA_OS_insert_value(count_shift,
				(MEA_IF_ACL5_FOUR_BYTES_WIDTH),
				((MEA_Uint32)key_entry->layer3.IPv6.Dst_Ipv6[i]),
				&val);
			count_shift += (MEA_IF_ACL5_FOUR_BYTES_WIDTH);
		}
		for (i = 0; i < 4; i++)
		{
			MEA_OS_insert_value(count_shift,
				(MEA_IF_ACL5_FOUR_BYTES_WIDTH),
				((MEA_Uint32)key_entry->layer3.IPv6.Src_Ipv6[i]),
				&val);
			count_shift += (MEA_IF_ACL5_FOUR_BYTES_WIDTH);
		}
	}
		break;
	case MEA_ACL5_KeyType_IPV6_DST:
	{
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.ethertype3),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer3.ip_protocol),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.dst_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.src_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer4.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_DSCP_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_dscp_value.ipv4_dscp.dscp),
			&val);
		count_shift += MEA_IF_ACL5_DSCP_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_PRIORITY_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_pri_type),
			&val);
		count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.range_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FLOW_LABEL_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv6.IPv6_Flow_Label),
			&val);
		count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_UMB_WIDTH,
			((MEA_Uint32)key_entry->layer2.Mac.MAC_UMB),
			&val);
		count_shift += MEA_IF_ACL5_UMB_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_SID_WIDTH,
			((MEA_Uint32)key_entry->ACLprof),
			&val);
		count_shift += MEA_IF_ACL5_SID_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L3_EtherType),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.Ip_Protocol_NH),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4dst),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4src),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.DSCP_TC),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)(key_entry->Acl5_mask_prof.range_mask_id >> 7)),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH + 1);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv4_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.Inner_Frame),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_KEY_TYPE_WIDTH,
			((MEA_Uint32)key_entry->keyType),
			&val);
		count_shift += MEA_IF_ACL5_KEY_TYPE_WIDTH;

		for (i = 0; i < 4; i++)
		{
			MEA_OS_insert_value(count_shift,
				(MEA_IF_ACL5_FOUR_BYTES_WIDTH),
				((MEA_Uint32)key_entry->layer3.IPv6.Dst_Ipv6[i]),
				&val);
			count_shift += (MEA_IF_ACL5_FOUR_BYTES_WIDTH);
		}
		for (i = 0; i < 6; i++)
		{
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->layer2.Mac.MacDA.b[5 - i]),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
		}

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_RNG_RSLT_WIDTH,
			((MEA_Uint32)key_entry->rangkProf_rule),
			&val);
		count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.ip_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG3_29_BITS_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG3),
			&val);
		count_shift += MEA_IF_ACL5_TAG3_29_BITS_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_P_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1_p),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_P_WIDTH;

		tag3 = key_entry->layer2.Tag3 >> 28;
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)tag3),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		tag3 = key_entry->layer2.Tag3 >> 29;
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)tag3),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG3),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1_p),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv6FlowLabel),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		tag3 = key_entry->layer2.Tag3 >> 30;
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)tag3),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
	}
		break;
	case MEA_ACL5_KeyType_IPV6_SRC:
		{
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				((MEA_Uint32)key_entry->layer3.ethertype3),
				&val);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->layer3.ip_protocol),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				((MEA_Uint32)key_entry->layer4.dst_port),
				&val);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				((MEA_Uint32)key_entry->layer4.src_port),
				&val);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->layer4.TCP_Flags),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				((MEA_Uint32)key_entry->layer3.l3_dscp_value.ipv4_dscp.dscp),
				&val);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				((MEA_Uint32)key_entry->layer3.l3_pri_type),
				&val);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->Acl5_mask_prof.range_mask_id),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_FLOW_LABEL_WIDTH,
				((MEA_Uint32)key_entry->layer3.IPv6.IPv6_Flow_Label),
				&val);
			count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				((MEA_Uint32)key_entry->layer2.Mac.MAC_UMB),
				&val);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				((MEA_Uint32)key_entry->ACLprof),
				&val);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.L3_EtherType),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.Ip_Protocol_NH),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4dst),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4src),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.TCP_Flags),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.DSCP_TC),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)(key_entry->Acl5_mask_prof.range_mask_id >> 7)),
				&val);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv4_Flags),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->Acl5_mask_prof.Inner_Frame),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_KEY_TYPE_WIDTH,
				((MEA_Uint32)key_entry->keyType),
				&val);
			count_shift += MEA_IF_ACL5_KEY_TYPE_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				((MEA_Uint32)key_entry->rangkProf_rule),
				&val);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				((MEA_Uint32)key_entry->Acl5_mask_prof.ip_mask_id),
				&val);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_TAG3_29_BITS_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG3),
				&val);
			count_shift += MEA_IF_ACL5_TAG3_29_BITS_WIDTH;

			for (i = 0; i < 6; i++)
			{
				MEA_OS_insert_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,
					((MEA_Uint32)key_entry->layer2.Mac.MacSA.b[5 - i]),
					&val);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				((MEA_Uint32)key_entry->layer2.Tag1),
				&val);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				((MEA_Uint32)key_entry->layer2.Tag1_p),
				&val);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH;

			tag3 = key_entry->layer2.Tag3 >> 28;
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)tag3),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			tag3 = key_entry->layer2.Tag3 >> 29;
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)tag3),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG3),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1_p),
				&val);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv6FlowLabel),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			tag3 = key_entry->layer2.Tag3 >> 30;
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				((MEA_Uint32)tag3),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			for (i = 0; i < 4; i++)
			{
				MEA_OS_insert_value(count_shift,
					(MEA_IF_ACL5_FOUR_BYTES_WIDTH),
					((MEA_Uint32)key_entry->layer3.IPv6.Src_Ipv6[i]),
					&val);
				count_shift += (MEA_IF_ACL5_FOUR_BYTES_WIDTH);
			}
		}
		break;
	case MEA_ACL5_KeyType_IPV6_SIG_Dest:
	{
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.ethertype3),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer3.ip_protocol),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.dst_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.src_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer4.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_DSCP_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_dscp_value.ipv4_dscp.dscp),
			&val);
		count_shift += MEA_IF_ACL5_DSCP_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_PRIORITY_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_pri_type),
			&val);
		count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.range_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FLOW_LABEL_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv6.IPv6_Flow_Label),
			&val);
		count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_UMB_WIDTH,
			((MEA_Uint32)key_entry->layer2.Mac.MAC_UMB),
			&val);
		count_shift += MEA_IF_ACL5_UMB_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_SID_WIDTH,
			((MEA_Uint32)key_entry->ACLprof),
			&val);
		count_shift += MEA_IF_ACL5_SID_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L3_EtherType),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.Ip_Protocol_NH),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4dst),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4src),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.DSCP_TC),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)(key_entry->Acl5_mask_prof.range_mask_id >> 7)),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv4_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.Inner_Frame),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_KEY_TYPE_WIDTH,
			((MEA_Uint32)key_entry->keyType),
			&val);
		count_shift += MEA_IF_ACL5_KEY_TYPE_WIDTH;

		for (i = 0; i < 6; i++)
		{
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->layer2.Mac.MacDA.b[5 - i]),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
		}

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_RNG_RSLT_WIDTH,
			((MEA_Uint32)key_entry->rangkProf_rule),
			&val);
		count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.ip_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH +29;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG2_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag2),
			&val);
		count_shift += MEA_IF_ACL5_TAG2_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag3),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_P_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1_p),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_P_WIDTH +9;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv4.src_ip),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv4.dst_ip),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG2),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG3),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1_p),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv6FlowLabel),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
	}
		break;
	case MEA_ACL5_KeyType_IPV6_SIG_SRC:
	{
		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.ethertype3),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer3.ip_protocol),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.dst_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TWO_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer4.src_port),
			&val);
		count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->layer4.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_DSCP_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_dscp_value.ipv4_dscp.dscp),
			&val);
		count_shift += MEA_IF_ACL5_DSCP_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_PRIORITY_WIDTH,
			((MEA_Uint32)key_entry->layer3.l3_pri_type),
			&val);
		count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BYTE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.range_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FLOW_LABEL_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv6.IPv6_Flow_Label),
			&val);
		count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_UMB_WIDTH,
			((MEA_Uint32)key_entry->layer2.Mac.MAC_UMB),
			&val);
		count_shift += MEA_IF_ACL5_UMB_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_SID_WIDTH,
			((MEA_Uint32)key_entry->ACLprof),
			&val);
		count_shift += MEA_IF_ACL5_SID_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L3_EtherType),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.Ip_Protocol_NH),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4dst),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.L4src),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TCP_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.DSCP_TC),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)(key_entry->Acl5_mask_prof.range_mask_id >> 7)),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv4_Flags),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.Inner_Frame),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_KEY_TYPE_WIDTH,
			((MEA_Uint32)key_entry->keyType),
			&val);
		count_shift += MEA_IF_ACL5_KEY_TYPE_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_RNG_RSLT_WIDTH,
			((MEA_Uint32)key_entry->rangkProf_rule),
			&val);
		count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
			((MEA_Uint32)key_entry->Acl5_mask_prof.ip_mask_id),
			&val);
		count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH + 29;

		for (i = 0; i < 6; i++)
		{
			MEA_OS_insert_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				((MEA_Uint32)key_entry->layer2.Mac.MacSA.b[5 - i]),
				&val);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
		}

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG2_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag2),
			&val);
		count_shift += MEA_IF_ACL5_TAG2_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag3),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_TAG1_P_WIDTH,
			((MEA_Uint32)key_entry->layer2.Tag1_p),
			&val);
		count_shift += MEA_IF_ACL5_TAG1_P_WIDTH + 9;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv4.src_ip),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_FOUR_BYTES_WIDTH,
			((MEA_Uint32)key_entry->layer3.IPv4.dst_ip),
			&val);
		count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG2),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG3),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.TAG1_p),
			&val);
		count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH + 2);

		MEA_OS_insert_value(count_shift,
			MEA_IF_ACL5_ONE_BIT_WIDTH,
			((MEA_Uint32)key_entry->ACL5_key_mask_prof.IPv6FlowLabel),
			&val);
		count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
	}
		break;
	default:
		return MEA_ERROR;
		break;
	}

	for (i = 0; i < MEA_ACL5_NUMBER_OF_REGISTERS; i++)
		reg[i] = val[i];

	return MEA_OK;

}

static MEA_Status mea_drv_ACL5_UpdateHwEntry(MEA_Unit_t unit_i,
											 MEA_ACL5Id_t Id,
											 MEA_Uint32 *reg_key,
											 MEA_SETTING_TYPE_te   type)
{
	
	MEA_Uint32 addressDDR;
	MEA_Uint32                  retVal[16];


    MEA_OS_memset(&retVal[0], 0, sizeof(retVal));


	/*write to SERVICE DB on DDR */
	addressDDR = MEA_ACL5_BASE_ADDRES_DDR_START + (MEA_ACL5_Table[Id].HashAddress << 6);
	MEA_DRV_WriteDDR_DATA_BLOCK(unit_i, Id, addressDDR, &reg_key[0], MEA_ACL5_BASE_ADDRES_DDR_WIDTH, DDR_ACL5);
	if (type == MEA_SETTING_TYPE_DELETE) {
		//Read from DDR see if we get
        MEA_OS_memset(&retVal[0], 0, sizeof(retVal));
		MEA_DDR_Lock();
		MEA_DB_DDR_HW_ACCESS_ENABLE
			if (MEA_OS_Device_DATA_DDR_RW(MEA_MODULE_DATA_DDR, 0,/*0 read 1- write */
				addressDDR,
				MEA_ADDRES_DDR_BYTE*MEA_ACL5_BASE_ADDRES_DDR_WIDTH,
				&retVal[0]
			) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR MEA_OS_Device_DATA_DDR_RW  Read from DDR \n");
				MEA_DB_DDR_HW_ACCESS_DISABLE
					MEA_DDR_Unlock();
				return MEA_ERROR;
			}
		MEA_DB_DDR_HW_ACCESS_DISABLE
			MEA_DDR_Unlock();
		

	}

	return MEA_OK;

}



MEA_Uint32 mea_drv_acl5_get_hashAdress(MEA_ACL5Id_t Id_i)
{
	
	if (Id_i >= MEA_ACL5_MAX_ENTRY_HW)
		return 0;

	return MEA_ACL5_Table[Id_i].HashAddress;
}








/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_IF_L2CP>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_IF_ACL5(MEA_Unit_t unit_i)
{
MEA_Uint32 size;

    if(b_bist_test){
        return MEA_OK;
    }
    /* Init IF ACL5 Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize ACL5     table ..\n");      

    if (MEA_ACL5InitDone == MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "ERROR : The ACL5 all ready Initialize\n");
            return MEA_ERROR;
    }
    
    MEA_ACL5InitDone = MEA_FALSE;

	
		size = (MEA_ACL5_MAX_ENTRY_HW ) * sizeof(MEA_ACL5_Entry_info_dbt);
        MEA_ACL5_Table = (MEA_ACL5_Entry_info_dbt*)MEA_OS_malloc(size);
	if (MEA_ACL5_Table == NULL) {
		MEA_OS_free(MEA_ACL5_Table);
        MEA_ACL5_Table = NULL;

		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s \n- Allocate MEA_ACL5_Table failed (size=%d)\n",
			__FUNCTION__,
			size);
		return MEA_ERROR;
	}
	    MEA_OS_memset((char*)MEA_ACL5_Table, 0, size);

    size = ((MEA_ACL5_MAX_ENTRY_HW / 32) * sizeof(MEA_Uint32));
    MEA_ACL5_External_BIT = (MEA_Uint32*)MEA_OS_malloc(size);
    if (MEA_ACL5_External_BIT == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_ACL5_External_BIT failed (size=%d)\n",
            __FUNCTION__, size);
        return MEA_ERROR;
    }
    MEA_OS_memset(&MEA_ACL5_External_BIT[0], 0, size);

	size = MEA_ACL5_MAX_ENTRY_SW * sizeof(MEA_Uint32);
	acl5_hash_to_ids = (MEA_Uint32*)MEA_OS_malloc(size);
	if (acl5_hash_to_ids == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Allocate acl5_ids failed (size=%d)\n",
			__FUNCTION__, size);
		return MEA_ERROR;
	}
	MEA_OS_memset(&acl5_hash_to_ids[0], 0, size);

    size = sizeof(MEA_Uint32);
    size = ((MEA_ACL5_MAX_ENTRY_HW / 32) * sizeof(MEA_Uint32));
    MEA_ACL5_External_Hash_BIT = (MEA_Uint32*)MEA_OS_malloc(size);
    if (MEA_ACL5_External_Hash_BIT == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_ACL5_External_Hash_BIT failed (size=%d)\n",
            __FUNCTION__, size);
        return MEA_ERROR;
    }
    MEA_OS_memset(&MEA_ACL5_External_Hash_BIT[0], 0, size);

	
	

	//Internal TBD 
    
	

    MEA_ACL5InitDone = MEA_TRUE;



    /* Return to caller */
    return MEA_OK; 

}

MEA_Status mea_drv_Conclude_IF_ACL5(MEA_Unit_t unit_i)
{
	if (MEA_ACL5_Table) {
		MEA_OS_free(MEA_ACL5_Table);
		MEA_ACL5_Table = NULL;
	}

	if (MEA_ACL5_External_BIT) {
		MEA_OS_free(MEA_ACL5_External_BIT);
		MEA_ACL5_External_BIT = NULL;
	}

	if (acl5_hash_to_ids) {
		MEA_OS_free(acl5_hash_to_ids);
		acl5_hash_to_ids = NULL;
	}

	if (MEA_ACL5_External_Hash_BIT) {
		MEA_OS_free(MEA_ACL5_External_Hash_BIT);
		MEA_ACL5_External_Hash_BIT = NULL;
	}
	
	return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_IF_ACL5>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_ACL5(MEA_Unit_t unit_i)
{
	MEA_ACL5Id_t id;
	MEA_Uint32 DataAcl5[16];

	/* ReInit ACL5 Mask table   */
	for (id = 0; id < MEA_ACL5_IPMASK_MAX_PROF; id++) {
		if (MEA_ACL5_Table[id].Valid) {
			MEA_OS_memcpy(&DataAcl5[0], &MEA_ACL5_Table[id].key_data[0], (sizeof(MEA_Uint32)*MEA_ACL5_NUMBER_OF_REGISTERS));
			MEA_OS_memcpy(&DataAcl5[MEA_ACL5_NUMBER_OF_REGISTERS], &MEA_ACL5_Table[id].action_data[0], (sizeof(MEA_Uint32) * 4));

			if (mea_drv_ACL5_UpdateHwEntry(unit_i,
				id,
				DataAcl5,
				DDR_ACL5) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s -  mea_drv_ACL5_UpdateHwEntry failed (id=%d)\n", __FUNCTION__, id);
				return MEA_ERROR;
			}
		}
	}


    /* Return to caller */
    return MEA_OK; 

}







/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs ACL5 implementation                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             <MEA_API_ACL5_Create_Entry>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_API_ACL5_Create_Entry(MEA_Unit_t              unit,
                                     MEA_ACL5_Key_dbt        *key,
                                     MEA_ACL5_Entry_Data_dbt *data,
									 MEA_ACL5Id_t              *Id_io)
{

	MEA_Bool exist;
	MEA_Uint32 key_reg[MEA_ACL5_NUMBER_OF_REGISTERS];
    MEA_Uint32 DataAcl5[16];
	ENET_xPermissionId_t xPermissionId;
    MEA_Uint32 Action_val[8];

    if (!MEA_ACL5_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ACL5_SUPPORT are not support \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    if (Id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id_io == NULL \n",
            __FUNCTION__);

        return MEA_ERROR;
    }

    if (key == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key = NULL \n",
            __FUNCTION__);

        return MEA_ERROR;
    }

    if (data == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data = NULL \n",
            __FUNCTION__);

        return MEA_ERROR;
    }

    if (data->Action_Valid  == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Should set valid Action \n",
            __FUNCTION__);

        return MEA_ERROR;
    }
    
    
    
    
    
    MEA_OS_memset(&key_reg, 0, sizeof(key_reg));
    MEA_OS_memset(&Action_val, 0, sizeof(Action_val));
    MEA_OS_memset(&DataAcl5, 0, sizeof(DataAcl5));
    
	
	/* Look for stream id */
	if ((*Id_io) != MEA_PLAT_GENERATE_NEW_ID) {
		/*check if the id exist*/
		if (mea_drv_ACL5_IsExist(unit, *Id_io, MEA_TRUE, &exist) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_IsExist failed (*id_io=%d\n",
				__FUNCTION__, *Id_io);
			return MEA_ERROR;
		}
		if (exist) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - *Id_io %d is already exist\n", __FUNCTION__, *Id_io);
			return MEA_ERROR;
		}
	}
	else {
		/*find new place*/

		if (mea_drv_ACL5_find_free(unit, Id_io) != MEA_TRUE) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_find_free failed\n", __FUNCTION__);
			return MEA_ERROR;
		}
	}

	
	if (key->keyType == MEA_ACL5_KeyType_IPV6_SIG_Dest || key->keyType == MEA_ACL5_KeyType_IPV6_SIG_SRC)
	{
		if (key->layer3.IPv6.Dst_Ipv6_valid == 1)
		{
			key->layer3.IPv4.dst_ip_valid = 1;
			key->layer3.IPv4.dst_ip = MEA_if_ACL5_Ipv6Sig_Hash(key->layer3.IPv6.Dst_Ipv6, MEA_HASH_ACL5_IPV6_FUNC1);
		}
		if (key->layer3.IPv6.Src_Ipv6_valid == 1)
		{
			key->layer3.IPv4.src_ip_valid = 1;
			key->layer3.IPv4.src_ip = MEA_if_ACL5_Ipv6Sig_Hash(key->layer3.IPv6.Src_Ipv6, MEA_HASH_ACL5_IPV6_FUNC1);
		}
	}
		
	if (MEA_If_ACL5_Init_Key(key, key_reg) != MEA_OK)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - MEA_If_ACL5_Init_Key failed\n", __FUNCTION__);
		return MEA_ERROR;
	}
		MEA_ACL5_Table[*Id_io].HashAddress = MEA_if_ACL5_Hash(key_reg, MEA_HASH_ACL5_FUNC1);
		
		if (mea_drv_ACL5_Update_TLB_Key(unit, key_reg, &MEA_ACL5_Table[*Id_io].HashAddress, *Id_io) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_find_key failed\n", __FUNCTION__);
			return MEA_ERROR;
		}

        if(data->OutPorts_valid){
		/* Get xPermissionId */
		if (!MEA_API_Get_Warm_Restart()) {
			xPermissionId = ENET_PLAT_GENERATE_NEW_ID;
		}
		else {
			xPermissionId = MEA_ACL5_Table[*Id_io].save_hw_xPermissionId;
		}

		if (enet_Create_xPermission(unit,
			&(data->OutPorts),
			0,/*no cpu*/
			&xPermissionId) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - enet_Create_xPermission failed \n", __FUNCTION__);
			return MEA_ERROR;
		}
		MEA_ACL5_Table[*Id_io].save_hw_xPermissionId = xPermissionId;
        }
/*
  Get Action 
 */
       if(data->Action_Valid == MEA_TRUE){
            mea_action_Get_hw_info(unit, 
                MEA_ACTION_TBL_ACL5_ACTIONS_EXTERNAL_START_INDEX +
                data->Action_Id, &Action_val[0]);
       }



       MEA_OS_memcpy(&DataAcl5[0], &key_reg[0],(sizeof(MEA_Uint32)*MEA_ACL5_NUMBER_OF_REGISTERS));
       MEA_OS_memcpy(&DataAcl5[MEA_ACL5_NUMBER_OF_REGISTERS], &Action_val[0], (sizeof(MEA_Uint32) * 4));
       DataAcl5[15] = DataAcl5[15] & 0x0fff;  
       
       if (data->OutPorts_valid) {
		   DataAcl5[15] |= xPermissionId << 12;
           DataAcl5[15] |= data->OutPorts_valid << 22;
       
       }
       DataAcl5[15] |= data->lxcp_win << 23;
	   DataAcl5[15] |= data->fwd_Act_en << 24;
	   DataAcl5[15] |= data->fwd_int_mac << 25;
	   DataAcl5[15] |= data->fwd_key << 26;
	   DataAcl5[15] |= data->fwd_force << 30;


		if (mea_drv_ACL5_UpdateHwEntry(unit,
			*Id_io,
            DataAcl5,
			DDR_ACL5) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - mea_drv_ACL5_UpdateHwEntry failed for id %d\n", __FUNCTION__, *Id_io);
			return MEA_ERROR;
		}
		
		MEA_OS_memcpy(&(MEA_ACL5_Table[*Id_io].key), key, sizeof(MEA_ACL5_Table[*Id_io].key));
		MEA_OS_memcpy(&(MEA_ACL5_Table[*Id_io].Data), data, sizeof(MEA_ACL5_Table[*Id_io].Data));
		MEA_OS_memcpy(&(MEA_ACL5_Table[*Id_io].action_data), &DataAcl5[MEA_ACL5_NUMBER_OF_REGISTERS], (sizeof(MEA_Uint32) * 4));
		MEA_ACL5_Table[*Id_io].Valid = MEA_TRUE;


    /*check the parameter*/


    /*build the hush */



    /*check if the we have we can insert this  if we can't  we have collision */
    /* if we have collisions avoidance  not support yet */

    /* write to DDR all the data */


    
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             <MEA_API_ACL5_Get_Entry>                                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


MEA_Status MEA_API_ACL5_Get_Entry(MEA_Unit_t              unit,
								  MEA_ACL5Id_t              Id_io,
                                  MEA_ACL5_Key_dbt        *key,
                                  MEA_ACL5_Entry_Data_dbt *data)
{
	MEA_Bool    exist;


	/* Check if support */
	if (!MEA_ACL5_SUPPORT) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"MEA_ACL5_SUPPORT %s \n", MEA_STATUS_STR(MEA_ACL5_SUPPORT));
		return MEA_ERROR;
	}

	/*check if the id exist*/
	if (mea_drv_ACL5_IsExist(unit, Id_io, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_IsExist failed (id_i=%d\n",
			__FUNCTION__, Id_io);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - id %d not exist\n", __FUNCTION__, Id_io);
		return MEA_ERROR;
	}
	if (key == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - key is null\n", __FUNCTION__, Id_io);
		return MEA_ERROR;
	}

	if (data == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - data action is null\n", __FUNCTION__, Id_io);
		return MEA_ERROR;
	}

	/* Copy to caller structure */
	MEA_OS_memset(key, 0, sizeof(*key));
	if (MEA_ACL5_Table[Id_io].Valid == MEA_TRUE)
		MEA_OS_memcpy(key,
			&(MEA_ACL5_Table[Id_io].key),
			sizeof(*key));

	MEA_OS_memset(data, 0, sizeof(*data));
	if (MEA_ACL5_Table[Id_io].Valid == MEA_TRUE)
		MEA_OS_memcpy(data,
			&(MEA_ACL5_Table[Id_io].Data),
			sizeof(*data));

	/* Return to caller */
	return MEA_OK;
}


MEA_Status MEA_API_ACL5_isExist(MEA_Unit_t  unit, MEA_ACL5Id_t Id_o, MEA_Bool silent, MEA_Bool *exist)
{
	return mea_drv_ACL5_IsExist(unit, Id_o, silent, exist);
}


MEA_Status MEA_API_ACL5_Delete_Entry(MEA_Unit_t              unit,
									 MEA_ACL5Id_t              Id_io)
{
	MEA_Bool exist;
	MEA_Uint32 key;

    if (!MEA_ACL5_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ACL5_SUPPORT are not support \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

	if (mea_drv_ACL5_IsExist(unit, Id_io, MEA_TRUE, &exist) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_IsExist failed (*id_io=%d\n",
			__FUNCTION__, Id_io);
		return MEA_ERROR;
	}
	if (!exist) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - *Id_io %d is not exist\n", __FUNCTION__, Id_io);
		return MEA_ERROR;
	}

	MEA_OS_memset(&key, 0, sizeof(key));


	if (mea_drv_ACL5_UpdateHwEntry(unit,
		Id_io,
		&key,
		MEA_SETTING_TYPE_DELETE) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ACL5_UpdateHwEntry failed for id %d\n", __FUNCTION__, Id_io);
		return MEA_ERROR;
	}

	
	
	acl5_hash_to_ids[MEA_ACL5_Table[Id_io].HashAddress] = 0;
	MEA_OS_memset(&MEA_ACL5_Table[Id_io], 0, sizeof(MEA_ACL5_Table[Id_io]));
	MEA_ACL5_Table[Id_io].Valid = MEA_FALSE;

    return MEA_OK;
}



