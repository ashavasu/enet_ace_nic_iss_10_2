/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*************************************************************************************/
/*     Module Name:         mea_port_drv.c                                           */
/*                                                                                   */
/*  Module Description:     MEA port configuration driver                            */
/*                                                                                   */
/*  Date of Creation:     20/01/04                                                   */
/*                                                                                   */
/*  Author Name:                                                                     */
/*************************************************************************************/



/*-------------------------------- Includes ------------------------------------------*/
#ifdef BL_PLATFORM
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#endif

#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_port_drv.h"
#include "enet_shaper_profile_drv.h"
#include "mea_Interface_drv.h" 


#if defined(MEA_ETHERNITY) //&& defined(MEA_ENV_S1_K7)
#include "k7_sfp_interface.h"
#endif


/*-------------------------------- Definitions ---------------------------------------*/
#define MEA_PARITIY_MAX_READ 16






/*-------------------------------- External Functions --------------------------------*/


/*-------------------------------- External Variables --------------------------------*/
#if 1
#else
extern MEA_drv_port2Interface_dbt MEA_Ports2InterfaceInfo[MEA_MAX_PORT_NUMBER+1];
extern MEA_drv_Interface2Port_dbt MEA_Interface2PortsInfo[MEA_MAX_PORT_NUMBER+1];

#endif
extern MEA_Uint32    MEA_InterfaceHwInfo[MEA_MAX_INERTNAL_INTERFACE_NUMBER];

static MEA_OutPorts_Entry_dbt MEA_Interface_Table_Tx_Disable;


/*-------------------------------- Forward Declarations ------------------------------*/

typedef struct{
    MEA_Bool                 valid;
    MEA_InterfaceType_t      InterfaceType;

    MEA_Interlaken_Entry_dbt Interlaken_data;
    MEA_RXAUI_Entry_dbt      RXAUI_data;
    MEA_SFP_PLUS_Entry_dbt   SFP_PLUS_data;
    MEA_XLAUI_Entry_dbt      XLAUI_data;
    MEA_SGMII_Entry_dbt      SGMII_data;
    MEA_QSGMII_Entry_dbt     QSGMII_data;

}mea_Interface_Data_Type_dbt;

typedef struct{
    MEA_Uint32 Id;
    MEA_SFP_PLUS_Entry_dbt   SFP_PLUS_data;
}mea_SFP_PLUS_hw_dbt;

typedef struct{
    MEA_Uint32 Id;
    MEA_SGMII_Entry_dbt   SGMII_data;
}mea_SGMII_hw_dbt;

typedef struct{
    MEA_Uint32 Id;
    MEA_QSGMII_GT_Entry_dbt   QSGMII_GT_data;
}mea_QGMII_Gt_hw_dbt;



typedef struct{
    MEA_Bool                valid;
    
    /* reset state */
    MEA_Uint8      parityRead[MEA_PARITIY_MAX_READ];
    MEA_Uint8      stateLink;       //01 Start 02-preSync 03- reset 04 sync
    
    MEA_Bool  sw_enable_reset;
}MEA_Interface_Data_Entry_dbt;

/*-------------------------------------------------------------------------------------*/
typedef struct{
    MEA_Bool             valid;

    MEA_Interface_Entry_dbt data;

}mea_Interface_Configure_dbt;



static MEA_Status mea_drv_SFP_PLUS_UpdateHw_SetID(MEA_Unit_t  unit_i, MEA_Interface_t  Id);

/*-------------------------------- Local Variables -----------------------------------*/
mea_Interface_Data_Type_dbt       *MEA_Interface_DataType_Info_Table= NULL;

MEA_Interface_Data_Entry_dbt      *MEA_Interface_data_Configure_info_Table=NULL; /* for Interlaken and rxAuvi*/

mea_Interface_Configure_dbt      MEA_Interface_Configure_Table[MEA_MAX_PORT_NUMBER+1];




//static  MEA_Uint32   Interface_info;

static MEA_Uint32              MEA_LinkStatusPrevious;
static MEA_OutPorts_Entry_dbt  MEA_LinkInterfaceStatusPrevious;

static MEA_OutPorts_Entry_dbt  MEA_My_LinkInterfaceStatusPrevious;

/*-------------------------------- Global Variables ----------------------------------*/
#if defined(MEA_ETHERNITY) && defined(MEA_OS_LINUX) &&  defined(MEA_OS_EPC_ZYNQ7045)
int mea_epc_interface_phy[MEA_MAX_PORT_NUMBER+1];
#endif

MEA_Uint32 mea_rxaui_interface_conf[3];

/*----------------------------------------------------------------------------*/
/*             Static declaration                                            */
/*----------------------------------------------------------------------------*/


static pGetIntExtLinkSttCallBack linkStatusCallback=NULL;

static void mea_Interface_UpdateHwEntryShaperCmd(MEA_Interface_Entry_dbt *entry /*,MEA_Uint32      shaperId*/)
{
    MEA_Uint32 val,temp;
    //MEA_Uint32  shaper_Id = shaperId;


    /* Build register 0 */
    val   = 0;
    if (entry->shaper_enable == MEA_TRUE){
        temp  = ENET_SHAPER_BYPASS_TYPE_ENABLE;
        temp &= (MEA_BM_SHAPER_BYPASS_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PORT)); 
        temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PORT);
        val  |= temp;

        temp  = entry->shaper_info.CBS;    // the initial value for the port CIR credit is the profile max ( = profile CBS)
        temp &= (MEA_BM_SHAPER_CIR_BACKET_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_CIR_BACKET_MASK_PORT)); 
        temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_CIR_BACKET_MASK_PORT);
        val  |= temp;

        temp = entry->shaper_Id;
        temp &= (MEA_BM_SHAPER_PROF_NUMBER_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_PROF_NUMBER_MASK_PORT)); 
        temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_PROF_NUMBER_MASK_PORT);
        val  |= temp;

        temp  = entry->Shaper_compensation;
        temp &= (MEA_BM_SHAPER_COMP_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PORT)); 
        temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PORT);
        val  |= temp;
    }else{
        temp  = ENET_SHAPER_BYPASS_TYPE_DISABLE;
        temp &= (MEA_BM_SHAPER_BYPASS_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PORT)); 
        temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PORT);
        val  |= temp;
        temp  = entry->Shaper_compensation;
        temp &= (MEA_BM_SHAPER_COMP_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PORT)); 
        temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PORT);
        val  |= temp;
    }
    MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,val,MEA_MODULE_BM);
}

static MEA_Status mea_Interface_UpdateHwShaperCmd (MEA_Interface_t  id, MEA_Interface_Entry_dbt *entry, MEA_Interface_Entry_dbt *old_entry) 
{  

    MEA_ind_write_t ind_write;
    MEA_db_HwUnit_t hwUnit;
#if defined(MEA_ENV_256PORT)
    MEA_Uint32       Lq=0; 
#endif


    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_ERROR;)

        if(
            (old_entry) &&
            (entry->shaper_Id           == old_entry->shaper_Id          ) && 
            (entry->Shaper_compensation == old_entry->Shaper_compensation) &&
            (entry->shaper_enable       == old_entry->shaper_enable      ) &&
            (MEA_OS_memcmp(&(entry->shaper_info),
            &(old_entry->shaper_info),
            sizeof(entry->shaper_info)) == 0              )  ) {
                return MEA_OK;
        } 
    
        /* interface to LQ*/
 
        
#if defined(MEA_ENV_256PORT)
        for (Lq = 0; Lq <= 1; Lq++){
            MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, Lq, MEA_MODULE_BM);
#endif
            /* build the ind_write value */
            ind_write.tableType = ENET_BM_TBL_SHAPER_BUCKET_PORT;
            ind_write.tableOffset = id;
            ind_write.cmdReg = MEA_BM_IA_CMD;
            ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
            ind_write.statusReg = MEA_BM_IA_STAT;
            ind_write.statusMask = MEA_BM_IA_STAT_MASK;
            ind_write.tableAddrReg = MEA_BM_IA_ADDR;
            ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
            ind_write.funcParam1 = (MEA_funcParam)entry;
           //ind_write.funcParam2 = (MEA_funcParam)entry->shaper_Id;
            ind_write.writeEntry = (MEA_FUNCPTR)mea_Interface_UpdateHwEntryShaperCmd;

            /* Write to the Indirect Table */
            if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_BM);
                return MEA_ERROR;
#if defined(MEA_ENV_256PORT)			
            }
#endif			
        }

        /* return to caller */   
        return MEA_OK;
}




static MEA_Status mea_Interface_UpdateHw_Mac_SerdesLB_Cmd(MEA_Interface_t      id, MEA_Interface_Entry_dbt *entry,MEA_Interface_Entry_dbt *old_entry){

    MEA_Uint32 val;

    if (
        (old_entry != NULL) &&
        (MEA_OS_memcmp (&entry->loopback,&old_entry->loopback,sizeof(entry->loopback) ) == 0)   
        ) {
            return MEA_OK;
    }


    val=0;
    val = id & 0x7f;
    val |= entry->loopback.lb_type<<7;  


    MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_MAC_SERDES_LB, val ,MEA_MODULE_IF);

    return MEA_OK;

}	



MEA_Status MEA_ConfigureInterface_Speed(MEA_Interface_t      id, MEA_Interface_Entry_dbt *entry, MEA_Interface_Entry_dbt *old_entry,int userConfig)
{
    MEA_Uint32 speed10_100;

    int adv=0;

    eSfpSpeedCfg speed=CONFIGURE_SPEED_NONE;

    if (adv){

    }


    if (
        (old_entry != NULL) &&
        ((entry->speed  == old_entry->speed) &&
        (entry->autoneg == old_entry->autoneg) )
         )
    {
//        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " No changed entry->autoneg %d entry->speed %d \n", entry->autoneg, entry->speed);
        return MEA_OK;
    }
//    MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " user_config:%d entry->autoneg %d entry->speed %d \n", userConfig, entry->autoneg, entry->speed);

    //           IF register 0x1F8.
    //            Bit 0: decimation valid, if set to 0, then we are in 1G mode, else 10M or 100M.
    //            Bit 1 : 10M = '0', 100M = '1'.
    //            Bit 2 : auto-neg 0-disable 1-enable.
    //            Bits[11:4] : port number.

    //            Bit 11 : if set to 1, then we are in read mode of the configuration of a port.

    

    speed10_100 = 0;
    speed10_100 = (entry->autoneg << 2);
    
        if (entry->autoneg)
        {
            speed = CONFIGURE_AUTONEG;
        }


        if (entry->speed == MEA_INTEFACE_SPEED_GIGA)
        {
            // if auto negotiation open then only edv with 1000M
            adv = (1 << CONFIGURE_1G_FULL_DUPLEX);

            if (speed != CONFIGURE_AUTONEG)// force to 1000M
            {
                speed = CONFIGURE_1G_FULL_DUPLEX;
            }
        }
        else
        {
            adv = (1 << CONFIGURE_10M_FULL_DUPLEX);

            if (speed != CONFIGURE_AUTONEG)// force to 10M
            {
                speed = CONFIGURE_10M_FULL_DUPLEX;
            }
            speed10_100 |= (1 << 0);


            if (entry->speed == MEA_INTEFACE_SPEED_100M)
            {
                adv = (1 << CONFIGURE_100M_FULL_DUPLEX);
                speed10_100 |= (1 << 1);
                if (speed != CONFIGURE_AUTONEG)// force to 100M
                {
                    speed = CONFIGURE_100M_FULL_DUPLEX;
                }
            }
            else
            {
                /*SPEED 10M*/

            }
        }
        speed10_100 |= (id << 4);
        speed10_100 |= (1 << 12);  /* Write mode */
        speed10_100 |= ((entry->mdio_Disable) << 13);
    
   

            

#if defined(MEA_ETHERNITY) && (defined(MEA_ENV_S1_K7) || defined(MEA_PLAT_S1_VDSL_K7))
            /*configure the SFP */
        if (MEA_device_environment_info.MEA_SFP_SUPPORT_enable == MEA_TRUE ){

            if ((MEA_Interface2PortsInfo[id].InterfacType == MEA_INTERFACETYPE_SGMII) ||
                MEA_Interface2PortsInfo[id].InterfacType == MEA_INTERFACETYPE_QSGMII ||
                MEA_Interface2PortsInfo[id].InterfacType == MEA_INTERFACETYPE_RGMII){

                if(userConfig == 1 ) 
                {
                    mea_sfp_change_speed(id,speed,adv);
                }

            }
        
        }

#endif



        	MEA_API_WriteReg(MEA_UNIT_0, (ENET_IF_SPEED_PORT_REG), (speed10_100), MEA_MODULE_IF);
            //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "IF_SPEED  0x1f8 =0x%08x \n", speed10_100);




    return MEA_OK;

}





MEA_Status mea_Interface_UpdateHwTx_Disable(MEA_Interface_t id, MEA_Interface_Entry_dbt *entry, MEA_Interface_Entry_dbt *old_entry)
{
    MEA_Uint32 *pPortGrp;
    MEA_Uint32 numofRegs = 4;
    MEA_Uint32 i;
    MEA_Uint32 val[4] ;

    if (
        (old_entry != NULL) &&
        (entry->Tx_Disable == old_entry->Tx_Disable)        
        )
    {
        return MEA_OK;
    }

    

    pPortGrp = (MEA_Uint32 *)(&(MEA_Interface_Table_Tx_Disable.out_ports_0_31));
    pPortGrp += (id / 32);

    (*pPortGrp) &= ~(0x0000001 << (id % 32));
    (*pPortGrp) |= (entry->Tx_Disable << (id % 32));


    pPortGrp = (MEA_Uint32 *)(&(MEA_Interface_Table_Tx_Disable.out_ports_0_31));
    for (i = 0; i < numofRegs; i++) {
        val[i]=0;
        val[i] = (*(pPortGrp));
        pPortGrp++;
    }
    
    if (mea_drv_write_IF_TBL_X_offset_UpdateEntry_pArray(MEA_UNIT_0, ENET_IF_CMD_PARAM_TBL_100_REGISTER,
        ENET_REGISTER_TBL100_Configure_INTERFACE_TX_Disable,
            numofRegs,
           val) != MEA_OK) {
        return MEA_ERROR;
    }


    return MEA_OK;
}

static MEA_Status mea_Interface_Conf_UpdateHw(MEA_Unit_t Unit,
                                          MEA_Interface_t      id,
                                          MEA_Interface_Entry_dbt *entry,
                                          MEA_Interface_Entry_dbt *old_entry)
{

	

    /* Update Shaper Interface Port info */
    if(mea_drv_Get_Interface_ShaperIs_On_interface(Unit,id) == MEA_TRUE){
            if ((MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PORT_TYPE) == MEA_SHAPER_SUPPORT_PORT_TYPE) &&
                (mea_Interface_UpdateHwShaperCmd(id,entry,old_entry) != MEA_OK))
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_EgressPort_UpdateHwShaperCmd failed\n",
                    __FUNCTION__);
                
                return MEA_ERROR;
            }
    } 
    if (entry->interfaceTyps != MEA_INTERFACETYPE_XLAUI) {
        if (mea_Interface_UpdateHw_Mac_SerdesLB_Cmd(id, entry, old_entry) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_Interface_UpdateHw_Mac_SerdesLB_Cmd failed\n",
                __FUNCTION__);

            return MEA_ERROR;
        }
    }

#if defined(MEA_ETHERNITY)
    if (MEA_ConfigureInterface_Speed(id, entry, old_entry,1) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ConfigureInterface_Speed failed\n",
            __FUNCTION__);

        return MEA_ERROR;
    
    }
#endif
   
    if (entry->interfaceTyps == MEA_INTERFACETYPE_XLAUI) 
    {
        MEA_XLAUI_Entry_dbt  XLAUI_entry;
        if (entry->loopback.lb_type == MEA_SERDES_LB_MAC) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MAC Loop back not support for XLAUI \n",
                __FUNCTION__);

            return MEA_ERROR;
        }

        if (MEA_API_Get_Interface_XLAUI_Entry(Unit, id, &XLAUI_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Get_Interface_XLAUI_Entry failed\n",
                __FUNCTION__);

            return MEA_ERROR;
        }

        XLAUI_entry.XLAUI.val.XLAUI_loopback = (MEA_Uint32)((MEA_Uint32)(entry->loopback.lb_type));

        if (MEA_API_Set_Interface_XLAUI_Entry(Unit, id, &XLAUI_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Set_Interface_XLAUI_Entry failed\n",
                __FUNCTION__);

            return MEA_ERROR;
        }
       
        
    
    }


    if(mea_Interface_UpdateHwTx_Disable(id, entry, old_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_Interface_UpdateHwTx_Disable failed\n",
            __FUNCTION__);

        return MEA_ERROR;

    }
    


    return MEA_OK;

}


static void  mea_UpdateHwEntry_Interlaken(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    //MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
   // MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
   // MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    MEA_Uint32  write_value = (MEA_Uint32)((long)arg4);

    MEA_Uint32     val[1],i;


    val[0] = write_value;
    
    
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i) , 
            val[i] , 
            MEA_MODULE_IF);
    }




    

}
static MEA_Status mea_drv_Interface_DataType_Interlaken_UpdateHw(MEA_Unit_t         unit_i,
                                                      MEA_Interface_t               Id,
                                                      mea_Interface_Data_Type_dbt   *entry,
                                                     mea_Interface_Data_Type_dbt   *old_entry)
{

    MEA_ind_write_t   ind_write;
    MEA_Uint32    value_to_write;
    MEA_Uint32    count_shift=0;
    MEA_Uint32    val[2],i;

    if(Id==0){
            // Table 70
            

                /* build the ind_write value */
            ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL;
            ind_write.tableOffset	= MEA_INTERLAKEN_CONTROL_REG_0_TX_RX_RESET ; //reg Id
            ind_write.cmdReg		= MEA_IF_CMD_REG;      
            ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
            ind_write.statusReg		= MEA_IF_STATUS_REG;   
            ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
            ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
            ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
            ind_write.writeEntry    = (MEA_FUNCPTR)	mea_UpdateHwEntry_Interlaken;
            
            if(old_entry == NULL ||
             ((entry->Interlaken_data.ctl_tx_reset_n != old_entry->Interlaken_data.ctl_tx_reset_n ) ||
             (entry->Interlaken_data.ctl_rx_reset_n != old_entry->Interlaken_data.ctl_rx_reset_n ))){

                value_to_write = 0;
                if(old_entry==NULL){
                    value_to_write = 1;
                    value_to_write |= 1<<16;
                }else{
                    value_to_write |= entry->Interlaken_data.ctl_rx_reset_n;
                    value_to_write |= entry->Interlaken_data.ctl_tx_reset_n<<16;

                    
                }


                ind_write.funcParam1 = (MEA_funcParam)unit_i;
                ind_write.funcParam2 = (MEA_funcParam)0;
                ind_write.funcParam3 = (MEA_funcParam)0;
                ind_write.funcParam4 = (MEA_funcParam)((long)value_to_write);
                
                if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                        __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
                    //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                    return MEA_ERROR;
                }
          }

            


                if(old_entry == NULL ||
                    ((entry->Interlaken_data.lbus           != old_entry->Interlaken_data.lbus )          ||
                    (entry->Interlaken_data.loopback_type   != old_entry->Interlaken_data.loopback_type )    
                   
                    )    ){

                        ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL;
                        ind_write.tableOffset	= MEA_INTERLAKEN_CONTROL_REG_1_LOOP ; //reg Id
                        value_to_write = 0;
                        count_shift=0;
                        for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
                            val[i] = 0;
                        }

                        MEA_OS_insert_value(count_shift,
                            3,
                            (MEA_Uint32)entry->Interlaken_data.loopback_type,
                            &val[0]);
                        count_shift+=3;

                        MEA_OS_insert_value(count_shift,
                            1,
                            (MEA_Uint32)entry->Interlaken_data.lbus,
                            &val[0]);

                        count_shift+=1;



                        value_to_write=val[0];

                        ind_write.writeEntry    = (MEA_FUNCPTR)	mea_UpdateHwEntry_Interlaken;
                        ind_write.funcParam1 = (MEA_funcParam)unit_i;
                        //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
                        //ind_write.funcParam3    = (MEA_funcParam)hwId;
                        ind_write.funcParam4 = (MEA_funcParam)((long)value_to_write);
                        if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                                __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
                            //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                            return MEA_ERROR;
                        }

                }




    if(old_entry == NULL ||
        ((entry->Interlaken_data.ctl_tx_enable != old_entry->Interlaken_data.ctl_tx_enable )          ||
         (entry->Interlaken_data.ctl_tx_rlim_enable != old_entry->Interlaken_data.ctl_tx_rlim_enable ) ||   
         (entry->Interlaken_data.ctl_tx_mubits != old_entry->Interlaken_data.ctl_tx_mubits )           ||
         (entry->Interlaken_data.ctl_tx_burstmax != old_entry->Interlaken_data.ctl_tx_burstmax )       ||
         (entry->Interlaken_data.ctl_tx_burstshort != old_entry->Interlaken_data.ctl_tx_burstshort )   ||
		 (entry->Interlaken_data.ctl_tx_fc_callen != old_entry->Interlaken_data.ctl_tx_fc_callen )     ||
		 (entry->Interlaken_data.ctl_fc_enable   != old_entry->Interlaken_data.ctl_fc_enable)          ||
         (entry->Interlaken_data.ctl_tx_fc_enable   != old_entry->Interlaken_data.ctl_tx_fc_enable)       
		)    ){

            ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL;
            ind_write.tableOffset	= MEA_INTERLAKEN_CONTROL_REG_4_CTL_TX ; //reg Id
           value_to_write = 0;
           count_shift=0;
           for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
               val[i] = 0;
           }

           MEA_OS_insert_value(count_shift,
               1,
               (MEA_Uint32)entry->Interlaken_data.ctl_tx_enable,
               &val[0]);
           count_shift+=1;

           MEA_OS_insert_value(count_shift,
               1,
               (MEA_Uint32)entry->Interlaken_data.ctl_tx_rlim_enable,
               &val[0]);

           count_shift+=1;

           MEA_OS_insert_value(count_shift,
               6,
               (MEA_Uint32)0,
               &val[0]);

           count_shift+=6;

           MEA_OS_insert_value(count_shift,
               8,
               (MEA_Uint32)entry->Interlaken_data.ctl_tx_mubits,
               &val[0]);
           count_shift+=8;

           MEA_OS_insert_value(count_shift,
               2,
               (MEA_Uint32)entry->Interlaken_data.ctl_tx_burstmax,
               &val[0]);
           count_shift+=2;

           MEA_OS_insert_value(count_shift,
               3,
               (MEA_Uint32)entry->Interlaken_data.ctl_tx_burstshort,
               &val[0]);
           count_shift+=3;

           MEA_OS_insert_value(count_shift,
               3,
               (MEA_Uint32)0,
               &val[0]);
           count_shift+=3;

           MEA_OS_insert_value(count_shift,
               4,
               (MEA_Uint32)entry->Interlaken_data.ctl_tx_fc_callen,
               &val[0]);
		    count_shift+=4;
           MEA_OS_insert_value(count_shift,
               1,
               (MEA_Uint32)entry->Interlaken_data.ctl_fc_enable,
               &val[0]);
            count_shift+=1;
            MEA_OS_insert_value(count_shift,
                1,
                (MEA_Uint32)entry->Interlaken_data.ctl_tx_fc_enable,
                &val[0]);

               
         value_to_write=val[0];

           ind_write.writeEntry    = (MEA_FUNCPTR)	mea_UpdateHwEntry_Interlaken;
           ind_write.funcParam1 = (MEA_funcParam)unit_i;
            //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
            //ind_write.funcParam3    = (MEA_funcParam)hwId;
           ind_write.funcParam4 = (MEA_funcParam)((long)value_to_write);
            if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }

    }

    if(old_entry == NULL ||
       ( (entry->Interlaken_data.ctl_tx_rdyout_thresh != old_entry->Interlaken_data.ctl_tx_rdyout_thresh ) ||
        (entry->Interlaken_data.ctl_tx_disabel_skipword != old_entry->Interlaken_data.ctl_tx_disabel_skipword ) ||
        (entry->Interlaken_data.ctl_tx_mframelen_minus1 != old_entry->Interlaken_data.ctl_tx_mframelen_minus1 )      )){
            ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL;
            ind_write.tableOffset	= MEA_INTERLAKEN_CONTROL_REG_8_CTL_TX_READY ; //reg Id
            value_to_write = 0;
            

            value_to_write  = entry->Interlaken_data.ctl_tx_mframelen_minus1;
            value_to_write |= entry->Interlaken_data.ctl_tx_disabel_skipword<<16;
            value_to_write |= entry->Interlaken_data.ctl_tx_rdyout_thresh<<24;   
          
            ind_write.writeEntry    = (MEA_FUNCPTR)	mea_UpdateHwEntry_Interlaken;
            ind_write.funcParam1 = (MEA_funcParam)unit_i;
            //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
            //ind_write.funcParam3    = (MEA_funcParam)hwId;
            ind_write.funcParam4 = (MEA_funcParam)((long)value_to_write);
            if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }

    }

    if(old_entry == NULL ||
        ( (entry->Interlaken_data.ctl_tx_rlim_max != old_entry->Interlaken_data.ctl_tx_rlim_max ) ||
        (entry->Interlaken_data.ctl_tx_rlim_delta != old_entry->Interlaken_data.ctl_tx_rlim_delta ) ||
        (entry->Interlaken_data.ctl_tx_rlim_intv != old_entry->Interlaken_data.ctl_tx_rlim_intv )
        )){


            ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL;
            ind_write.tableOffset	= MEA_INTERLAKEN_CONTROL_REG_12_CTL_TX_RATE_LLIMITER ; //reg Id
            value_to_write = 0;
            
            value_to_write =  entry->Interlaken_data.ctl_tx_rlim_max ; 
            value_to_write |= entry->Interlaken_data.ctl_tx_rlim_delta<<16;
            value_to_write |= entry->Interlaken_data.ctl_tx_rlim_intv<<24 ;

            ind_write.writeEntry    = (MEA_FUNCPTR)	mea_UpdateHwEntry_Interlaken;
            ind_write.funcParam1 = (MEA_funcParam)unit_i;
            //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
            //ind_write.funcParam3    = (MEA_funcParam)hwId;
            ind_write.funcParam4 = (MEA_funcParam)((long)value_to_write);
            if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }

    }

    if(old_entry == NULL ||
        ( (entry->Interlaken_data.ctl_tx_diagword_lanestat != old_entry->Interlaken_data.ctl_tx_diagword_lanestat ) ||
        (entry->Interlaken_data.ctl_tx_diagword_intfstat != old_entry->Interlaken_data.ctl_tx_diagword_intfstat ) 
        
        )){


            ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL;
            ind_write.tableOffset	= MEA_INTERLAKEN_CONTROL_REG_16_CTL_TX_DIAGNOSTIC ; //reg Id
            value_to_write = 0;

            value_to_write  =  entry->Interlaken_data.ctl_tx_diagword_lanestat;
            value_to_write |=  entry->Interlaken_data.ctl_tx_diagword_intfstat<<16;
                

                ind_write.writeEntry    = (MEA_FUNCPTR)	mea_UpdateHwEntry_Interlaken;
                ind_write.funcParam1 = (MEA_funcParam)unit_i;
            //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
            //ind_write.funcParam3    = (MEA_funcParam)hwId;
                ind_write.funcParam4 = (MEA_funcParam)((long)value_to_write);
            if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }

    }

    if(old_entry == NULL ||
        ( (entry->Interlaken_data.ctl_rx_mframelen_minus1 != old_entry->Interlaken_data.ctl_rx_mframelen_minus1 ) ||
        (entry->Interlaken_data.ctl_rx_force_resync != old_entry->Interlaken_data.ctl_rx_force_resync ) ||
        (entry->Interlaken_data.ctl_rx_packet_mode != old_entry->Interlaken_data.ctl_rx_packet_mode )  ||
        (entry->Interlaken_data.ctl_rx_burstmax != old_entry->Interlaken_data.ctl_rx_burstmax ) 

        )){


            ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL;
            ind_write.tableOffset	= MEA_INTERLAKEN_CONTROL_REG_20_CTL_RX_CONF ; //reg Id
            value_to_write = 0;

            value_to_write  =  entry->Interlaken_data.ctl_rx_mframelen_minus1;
            value_to_write |=  entry->Interlaken_data.ctl_rx_force_resync<<16;
            value_to_write |=  entry->Interlaken_data.ctl_rx_packet_mode<<17;
            value_to_write |=  entry->Interlaken_data.ctl_rx_burstmax<<18;


                ind_write.writeEntry    = (MEA_FUNCPTR)	mea_UpdateHwEntry_Interlaken;
                ind_write.funcParam1 = (MEA_funcParam)unit_i;
            //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
            //ind_write.funcParam3    = (MEA_funcParam)hwId;
                ind_write.funcParam4 = (MEA_funcParam)((long)value_to_write);
            if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }

    }

    ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL;
    ind_write.tableOffset	= MEA_INTERLAKEN_CONTROL_REG_0_TX_RX_RESET ; //reg Id
    ind_write.writeEntry    = (MEA_FUNCPTR)	mea_UpdateHwEntry_Interlaken;
    value_to_write = 0;
    if(old_entry==NULL){
        value_to_write = 0;
        value_to_write |= 0<<16;
         
        ind_write.funcParam4 = (MEA_funcParam)((long)value_to_write);
        if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
            //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
    
    }



    }else{
        return MEA_ERROR;
    }

    
    
    return MEA_OK;
}


static void  mea_UpdateHwEntry_SGMII(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    //MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    // MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    // MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    mea_SGMII_hw_dbt *entry = (mea_SGMII_hw_dbt*)arg4;

    MEA_Uint32     val[2], i;


    val[0] = entry->Id;
    val[1] = entry->SGMII_data.reg[0];
   


    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }






}



static MEA_Status mea_drv_SGMII_UpdateHw_info(MEA_Unit_t         unit_i,
                                             MEA_Interface_t               Id,
                                             mea_SGMII_hw_dbt              *SGMII_hw_info)
{

    MEA_ind_write_t   ind_write;

    
    

    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
    ind_write.tableOffset = ENET_REGISTER_TBL100_SGMII_config; //reg Id
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_SGMII;


    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)0;
    ind_write.funcParam3 = (MEA_funcParam)0;
    ind_write.funcParam4 = (MEA_funcParam)SGMII_hw_info;



    if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }


    return MEA_OK;
}



static MEA_Status mea_drv_SGMII_UpdateHw(MEA_Unit_t         unit_i,
                                         MEA_Interface_t               Id,
                                         mea_Interface_Data_Type_dbt   *entry)
{

    mea_SGMII_hw_dbt     SGMII_hw_info;
    MEA_Status  retval = MEA_FALSE;
 

    /*build the hw info */
    MEA_OS_memset(&SGMII_hw_info, 0, sizeof(mea_SGMII_hw_dbt));

    SGMII_hw_info.Id = Id;
    MEA_OS_memcpy(&SGMII_hw_info.SGMII_data, &entry->SGMII_data, sizeof(SGMII_hw_info.SGMII_data));
#if 0
    if ((SGMII_hw_info.SGMII_data.value.Unidirectional_Enable == MEA_TRUE) ||
        (SGMII_hw_info.SGMII_data.value.Powerdown             == MEA_TRUE) ||
        (SGMII_hw_info.SGMII_data.value.electrically_Isolate  == MEA_TRUE)
        ) 
    {
        if(SGMII_hw_info.SGMII_data.value.assert_en == MEA_FALSE)
            SGMII_hw_info.SGMII_data.value.assert_en = MEA_TRUE;
    }



    if (SGMII_hw_info.SGMII_data.value.AN_reset == MEA_TRUE)
        SGMII_hw_info.SGMII_data.value.assert_en = MEA_TRUE;


    retval = mea_drv_SGMII_UpdateHw_info(unit_i, Id, &SGMII_hw_info);
    /*return back to value */
    if (entry->SGMII_data.value.assert_en == MEA_TRUE) {
        entry->SGMII_data.value.assert_en = MEA_FALSE;
    }
    entry->SGMII_data.value.RX_PMA_reset = MEA_FALSE;
    entry->SGMII_data.value.TX_PMA_reset = MEA_FALSE;
#else

 retval = mea_drv_SGMII_UpdateHw_info(unit_i, Id, &SGMII_hw_info);

#endif

    return retval;
}



static void mea_UpdateHwEntry_QSGMII_GT(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    //MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    // MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    // MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    mea_QGMII_Gt_hw_dbt *entry = (mea_QGMII_Gt_hw_dbt*)arg4;

    MEA_Uint32     val[3], i;


    val[0] = entry->Id;
    val[1] = entry->QSGMII_GT_data.regs[0];
   



    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }



}

MEA_Status mea_drv_QSGMII_UpdateHw(MEA_Unit_t         unit_i,
    MEA_Interface_t               Id,
    mea_Interface_Data_Type_dbt   *entry,
    mea_Interface_Data_Type_dbt   *old_entry)
{
    mea_SGMII_hw_dbt     SGMII_hw_info;
    mea_QGMII_Gt_hw_dbt     QGMII_hw_gt_info;
    MEA_ind_write_t   ind_write;
    MEA_Bool  SGMII_change = MEA_TRUE;
    MEA_Bool  GT_QSGMII_change = MEA_TRUE;

    /*build the hw info */
    MEA_OS_memset(&SGMII_hw_info, 0, sizeof(mea_SGMII_hw_dbt));
    MEA_OS_memset(&QGMII_hw_gt_info, 0, sizeof(mea_QGMII_Gt_hw_dbt));


    if (old_entry != NULL
        &&
        (MEA_OS_memcmp(&entry->QSGMII_data.Sgmii_info, &old_entry->QSGMII_data.Sgmii_info, sizeof(old_entry->QSGMII_data.Sgmii_info)) == 0)){
        SGMII_change = MEA_FALSE;
    }

    if (SGMII_change){
        MEA_OS_memset(&SGMII_hw_info, 0, sizeof(mea_SGMII_hw_dbt));
        SGMII_hw_info.Id = Id;
        MEA_OS_memcpy(&SGMII_hw_info.SGMII_data, &entry->QSGMII_data.Sgmii_info, sizeof(SGMII_hw_info.SGMII_data));

        mea_drv_SGMII_UpdateHw_info(unit_i, Id, &SGMII_hw_info);
    }


    /************************************************************************/
    /*                                                                      */
    /************************************************************************/

    if (old_entry != NULL
        &&
        (MEA_OS_memcmp(&entry->QSGMII_data.GT, &old_entry->QSGMII_data.GT, sizeof(old_entry->QSGMII_data.GT)) == 0))
    {
        GT_QSGMII_change = MEA_FALSE;
    }

    if (GT_QSGMII_change)
    {
        MEA_OS_memset(&QGMII_hw_gt_info, 0, sizeof(mea_QGMII_Gt_hw_dbt));
        QGMII_hw_gt_info.Id = Id;
        MEA_OS_memcpy(&QGMII_hw_gt_info.QSGMII_GT_data, &entry->QSGMII_data.GT, sizeof(entry->QSGMII_data.GT));




        /* build the ind_write value */
        ind_write.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
        ind_write.tableOffset = ENET_REGISTER_TBL100_QSGMII_config_GT; //reg Id
        ind_write.cmdReg = MEA_IF_CMD_REG;
        ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
        ind_write.statusReg = MEA_IF_STATUS_REG;
        ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
        ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_QSGMII_GT;


        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)0;
        ind_write.funcParam3 = (MEA_funcParam)0;
        ind_write.funcParam4 = (MEA_funcParam)&QGMII_hw_gt_info;



        if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
            //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }


    return MEA_OK;

}



static void  mea_UpdateHwEntry_SFP_PLUS(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    //MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    // MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    // MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    mea_SFP_PLUS_hw_dbt *entry = (mea_SFP_PLUS_hw_dbt*) arg4;

    MEA_Uint32     val[3], i;


    val[0] = entry->Id;
    val[1] = entry->SFP_PLUS_data.SFP_PLUS.regs[0];
    val[2] = entry->SFP_PLUS_data.SFP_PLUS.regs[1];


    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }






}





static MEA_Status mea_drv_SFP_PLUS_UpdateHw(MEA_Unit_t         unit_i,
    MEA_Interface_t               Id,
    mea_Interface_Data_Type_dbt   *entry)
{

    mea_SFP_PLUS_hw_dbt     SFP_PLUS_hw_info;
    MEA_ind_write_t   ind_write;

    /*build the hw info */
    MEA_OS_memset(&SFP_PLUS_hw_info, 0, sizeof(SFP_PLUS_hw_info));

    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
    ind_write.tableOffset = ENET_REGISTER_TBL100_XMAC_TX_CONF; //reg Id
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_SFP_PLUS;


    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)0;
    ind_write.funcParam3 = (MEA_funcParam)0;
    ind_write.funcParam4 = (MEA_funcParam)&SFP_PLUS_hw_info;


    SFP_PLUS_hw_info.Id = Id;
    MEA_OS_memcpy(&SFP_PLUS_hw_info.SFP_PLUS_data, &entry->SFP_PLUS_data, sizeof(SFP_PLUS_hw_info.SFP_PLUS_data));

    if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (entry->SFP_PLUS_data.SFP_PLUS.val.xmac_pcs_reset == 1 || 
        entry->SFP_PLUS_data.SFP_PLUS.val.xmac_pma_reset == 1){
        
        entry->SFP_PLUS_data.SFP_PLUS.val.xmac_pcs_reset = 0;
        entry->SFP_PLUS_data.SFP_PLUS.val.xmac_pma_reset = 0;
        SFP_PLUS_hw_info.Id = Id;
        MEA_OS_memcpy(&SFP_PLUS_hw_info.SFP_PLUS_data, &entry->SFP_PLUS_data, sizeof(SFP_PLUS_hw_info.SFP_PLUS_data));
        if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
            //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }





    return MEA_OK;
}




static void mea_UpdateHwEntry_XLAUI(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    //MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    // MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    // MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    MEA_Uint32 entry = (MEA_Uint32)(long)arg4;

    MEA_Uint32     val[1], i;

    val[0] = 0;
    val[0] = entry;


    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }






}

static MEA_Status mea_drv_XLAUI_UpdateHw(MEA_Unit_t         unit_i,
    MEA_Interface_t               Id,
    mea_Interface_Data_Type_dbt   *entry,
    mea_Interface_Data_Type_dbt   *old_entry)
{

    
    MEA_ind_write_t   ind_write;
    MEA_Uint32        value;

    /*build the hw info */
    


    if (Id == 127){
        value = 3;
        /* build the ind_write value */
        ind_write.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
        ind_write.tableOffset = 0;
        ind_write.cmdReg = MEA_IF_CMD_REG;
        ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
        ind_write.statusReg = MEA_IF_STATUS_REG;
        ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
        ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_XLAUI;


        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)0;
        ind_write.funcParam3 = (MEA_funcParam)0;
        ind_write.funcParam4 = (MEA_funcParam)((long)value);

        if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
            //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        

        if ((old_entry == NULL) || (entry->XLAUI_data.XLAUI.regs[0] != MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.regs[0])) {
            /*  --------  */
            value = entry->XLAUI_data.XLAUI.regs[0];
            ind_write.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
            ind_write.tableOffset = 1;
            ind_write.cmdReg = MEA_IF_CMD_REG;
            ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
            ind_write.statusReg = MEA_IF_STATUS_REG;
            ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
            ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
            ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
            ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_XLAUI;


            ind_write.funcParam1 = (MEA_funcParam)unit_i;
            ind_write.funcParam2 = (MEA_funcParam)0;
            ind_write.funcParam3 = (MEA_funcParam)0;
            ind_write.funcParam4 = (MEA_funcParam)((long)value);

            if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
        }

        if ((old_entry == NULL) || (entry->XLAUI_data.XLAUI.regs[1] != MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.regs[1])) {
            value = entry->XLAUI_data.XLAUI.regs[1];
            ind_write.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
            ind_write.tableOffset = 4;
            ind_write.cmdReg = MEA_IF_CMD_REG;
            ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
            ind_write.statusReg = MEA_IF_STATUS_REG;
            ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
            ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
            ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
            ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_XLAUI;


            ind_write.funcParam1 = (MEA_funcParam)unit_i;
            ind_write.funcParam2 = (MEA_funcParam)0;
            ind_write.funcParam3 = (MEA_funcParam)0;
            ind_write.funcParam4 = (MEA_funcParam)((long)value);

            if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
            entry->XLAUI_data.XLAUI.val.diag_rx_frsnc = 0;/* return 0*/
        }
        


        if ((old_entry == NULL) || entry->XLAUI_data.XLAUI.regs[2] != MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.regs[2]) {
            value = entry->XLAUI_data.XLAUI.regs[2];
            ind_write.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
            ind_write.tableOffset = 5;
            ind_write.cmdReg = MEA_IF_CMD_REG;
            ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
            ind_write.statusReg = MEA_IF_STATUS_REG;
            ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
            ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
            ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
            ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_XLAUI;


            ind_write.funcParam1 = (MEA_funcParam)unit_i;
            ind_write.funcParam2 = (MEA_funcParam)0;
            ind_write.funcParam3 = (MEA_funcParam)0;
            ind_write.funcParam4 = (MEA_funcParam)((long)value);

            if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
        }

        if ((old_entry == NULL) || entry->XLAUI_data.XLAUI.regs[3] != MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.regs[3])
        {
            value = entry->XLAUI_data.XLAUI.regs[3];
            ind_write.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
            ind_write.tableOffset = 33;
            ind_write.cmdReg = MEA_IF_CMD_REG;
            ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
            ind_write.statusReg = MEA_IF_STATUS_REG;
            ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
            ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
            ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
            ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_XLAUI;


            ind_write.funcParam1 = (MEA_funcParam)unit_i;
            ind_write.funcParam2 = (MEA_funcParam)0;
            ind_write.funcParam3 = (MEA_funcParam)0;
            ind_write.funcParam4 = (MEA_funcParam)((long)value);

            if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
        }

		if ((old_entry == NULL) || entry->XLAUI_data.XLAUI.regs[4] != MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.regs[4])
		{
			value = entry->XLAUI_data.XLAUI.regs[4];
			ind_write.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
			ind_write.tableOffset = 36;
			ind_write.cmdReg = MEA_IF_CMD_REG;
			ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
			ind_write.statusReg = MEA_IF_STATUS_REG;
			ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
			ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
			ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
			ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_XLAUI;


			ind_write.funcParam1 = (MEA_funcParam)unit_i;
			ind_write.funcParam2 = (MEA_funcParam)0;
			ind_write.funcParam3 = (MEA_funcParam)0;
			ind_write.funcParam4 = (MEA_funcParam)((long)value);

			if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
					__FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
				//MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
				return MEA_ERROR;
			}
		}


        /* ---------- */
        value = 0;
        /* build the ind_write value */
        ind_write.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
        ind_write.tableOffset = 0;
        ind_write.cmdReg = MEA_IF_CMD_REG;
        ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
        ind_write.statusReg = MEA_IF_STATUS_REG;
        ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
        ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_XLAUI;


        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)0;
        ind_write.funcParam3 = (MEA_funcParam)0;
        ind_write.funcParam4 = (MEA_funcParam)((long)value);

        if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
            //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

    }
    else{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "This interface %d not support \n", Id);
        return MEA_OK;
    }



    







    return MEA_OK;
}




static void mea_UpdateHwEntry_RXAUI(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    //MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    // MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    // MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    MEA_Uint32 entry = (MEA_Uint32)(long)arg4;

    MEA_Uint32     val[1], i;

    val[0] = 0;
    val[0] = entry;


    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }






}

static MEA_Status mea_drv_RXAUI_UpdateHw(MEA_Unit_t         unit_i,
    MEA_Interface_t               Id,
    mea_Interface_Data_Type_dbt   *entry,
    mea_Interface_Data_Type_dbt   *old_entry)
{


    MEA_ind_write_t   ind_write;
    MEA_Uint32        value;
    MEA_Bool          SetResret=MEA_FALSE;

    /*build the hw info */


    if(entry->RXAUI_data.RXAUI_reg.val.rxaui_Reset_RX_Link_Status == 1 || 
       entry->RXAUI_data.RXAUI_reg.val.rxaui_Reset_Local_Fault    == 1)
    {
        SetResret= MEA_TRUE;

    }

    entry->RXAUI_data.RXAUI_reg.val.rxaui_Id = Id; // Interface ID
    value = entry->RXAUI_data.RXAUI_reg.regs[3];

        /* build the ind_write value */
        ind_write.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
        ind_write.tableOffset = ENET_REGISTER_TBL100_Configure_RXAUI;
        ind_write.cmdReg = MEA_IF_CMD_REG;
        ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
        ind_write.statusReg = MEA_IF_STATUS_REG;
        ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
        ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_RXAUI;


        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)0;
        ind_write.funcParam3 = (MEA_funcParam)0;
        ind_write.funcParam4 = (MEA_funcParam)((long)value);

        if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
            //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if(SetResret == MEA_TRUE)
        {

            entry->RXAUI_data.RXAUI_reg.val.rxaui_Reset_RX_Link_Status =0; 
            entry->RXAUI_data.RXAUI_reg.val.rxaui_Reset_Local_Fault=0;
        
            value = entry->RXAUI_data.RXAUI_reg.regs[3];

            if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
        
        
        }








    return MEA_OK;
}







/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Interface_DataType_UpdateHw>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Interface_DataType_UpdateHw(MEA_Unit_t               unit,
                                             MEA_Interface_t               Id,
                                             mea_Interface_Data_Type_dbt  *entry,
                                             mea_Interface_Data_Type_dbt *old_entry)
{
    
#if 0
    MEA_RXAUI_Entry_dbt rxauiData;
    MEA_Uint32 val[3];
    MEA_Uint32 count_shift;
    MEA_Uint32 save_rxaui_interface_conf[3];
#endif

    if(old_entry != NULL 
        && 
        (MEA_OS_memcmp(&entry,&old_entry,sizeof(old_entry))== 0) ){
            return MEA_OK; //no update
    }


    if(entry->InterfaceType == MEA_INTERFACETYPE_Interlaken){
        //* update HW
         //currently the interface id is 0 for Interlaken one interface
         
         
        if(mea_drv_Interface_DataType_Interlaken_UpdateHw(unit,Id,entry,old_entry)!=MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n - mea_drv_Interface_DataType_Interlaken_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }

        return MEA_OK;


    }

    if(entry->InterfaceType == MEA_INTERFACETYPE_RXAUI){

        return mea_drv_RXAUI_UpdateHw(unit, Id, entry, old_entry);
           

    }
    if (entry->InterfaceType == MEA_INTERFACETYPE_XLAUI) /*40G*/
    {

        return mea_drv_XLAUI_UpdateHw(unit, Id, entry , old_entry);

    }

    if (entry->InterfaceType == MEA_INTERFACETYPE_SINGLE10G)
    {

        return mea_drv_SFP_PLUS_UpdateHw(unit, Id, entry);

    }

    if(entry->InterfaceType == MEA_INTERFACETYPE_RGMII)
    {

    }

    if (entry->InterfaceType == MEA_INTERFACETYPE_SGMII ) 
    {
        /*to not configure the powerUpDwon*/
       
        return  mea_drv_SGMII_UpdateHw(unit, Id, entry);
    }


    if (entry->InterfaceType == MEA_INTERFACETYPE_QSGMII)
    {
        

        /*configure    */
        return   mea_drv_QSGMII_UpdateHw(unit, Id, entry, old_entry);
    
    }
    
    if (entry->InterfaceType == MEA_INTERFACETYPE_INTERNAL_TLS){
    
    
    
    }


    return MEA_OK;
}



static MEA_Status mea_drv_Interface_Get_Enter(MEA_Unit_t Unit,MEA_Interface_t Id,mea_Interface_Data_Type_dbt *entry)
{


    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry is NULL \n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry,&MEA_Interface_DataType_Info_Table[Id],sizeof(*entry));


    return MEA_OK;
}


MEA_Status mea_drv_Interface_Conclude(MEA_Unit_t Unit)
{

    MEA_Interface_Entry_dbt  entry;
    MEA_Interface_t Id;


    for (Id = 0; Id <= MEA_MAX_PORT_NUMBER; Id++) {

        if (MEA_API_Interface_Valid(MEA_UNIT_0, Id, MEA_TRUE) != MEA_TRUE)
            continue;
        if (MEA_API_Get_Interface_ConfigureEntry(Unit, Id, &entry) != MEA_OK){
        }
        if (entry.shaper_enable == MEA_TRUE){
            entry.shaper_enable = MEA_FALSE;
            if (MEA_API_Set_Interface_ConfigureEntry(Unit, Id, &entry) != MEA_OK){
            }
        }
    }

    
    if(MEA_Interface_DataType_Info_Table){
        MEA_OS_free(MEA_Interface_DataType_Info_Table);
        MEA_Interface_DataType_Info_Table = NULL;

    }

    if(MEA_Interface_data_Configure_info_Table){
        MEA_OS_free(MEA_Interface_data_Configure_info_Table);
        MEA_Interface_data_Configure_info_Table = NULL;

    }
    


    return MEA_OK;
}


MEA_Status mea_drv_Interface_Init(MEA_Unit_t Unit)
{

    MEA_Uint32 size;
    MEA_Interface_t Id;
	MEA_drv_Interface2Port_dbt InterfaceEntry;
    //MEA_Uint16 Interface_info;

    MEA_OS_memset(&MEA_Interface_Table_Tx_Disable,0,sizeof(MEA_OutPorts_Entry_dbt));


    mea_rxaui_interface_conf[0]=0;
    mea_rxaui_interface_conf[1]=0;





    //check if the interface Support
    if(b_bist_test){
        return MEA_OK;
    }

#if defined(MEA_ETHERNITY) && (defined(MEA_ENV_S1_K7) || defined(MEA_PLAT_S1_VDSL_K7))

    mea_init_sfp_configuration();

#endif
 
	MEA_OS_memset(&MEA_LinkStatusPrevious,0,sizeof(MEA_LinkStatusPrevious));
	MEA_OS_memset(&MEA_LinkInterfaceStatusPrevious,0,sizeof(MEA_LinkInterfaceStatusPrevious));

	MEA_OS_memset(&MEA_My_LinkInterfaceStatusPrevious,0,sizeof(MEA_My_LinkInterfaceStatusPrevious));

	MEA_OS_memset(&MEA_Interface_Configure_Table,0,sizeof(MEA_Interface_Configure_Table));


    /* Allocate PacketGen Table */
    size = (MEA_INTERFACE_MAX_ID) * sizeof(mea_Interface_Data_Type_dbt);
    if (size != 0) {
        MEA_Interface_DataType_Info_Table = (mea_Interface_Data_Type_dbt*)MEA_OS_malloc(size);
        if (MEA_Interface_DataType_Info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_Interface_DataType_Info_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_Interface_DataType_Info_Table[0]),0,size);
    }

    size = (MEA_INTERFACE_MAX_ID) * sizeof(MEA_Interface_Data_Entry_dbt);
    if (size != 0) {
        MEA_Interface_data_Configure_info_Table = (MEA_Interface_Data_Entry_dbt*)MEA_OS_malloc(size);
        if (MEA_Interface_data_Configure_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_Interface_DataType_Info_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_Interface_data_Configure_info_Table[0]),0,size);
    }



    for (Id=0;Id<=MEA_MAX_PORT_NUMBER;Id++)
    {
 
	
	   mea_drv_Get_Interface2PortsInfo(Unit,Id, &InterfaceEntry);


       if (InterfaceEntry.valid == MEA_TRUE )
       {
           MEA_Interface_Configure_Table[Id].valid = MEA_TRUE;
       }
       


        if((InterfaceEntry.valid == MEA_TRUE)  && 
		   (InterfaceEntry.InterfacType == MEA_INTERFACETYPE_Interlaken)){
            
            MEA_Interface_data_Configure_info_Table[Id].valid =MEA_TRUE;
            MEA_Interface_data_Configure_info_Table[Id].sw_enable_reset = MEA_TRUE;
            MEA_Interface_DataType_Info_Table[Id].valid = MEA_TRUE;

            MEA_Interface_DataType_Info_Table[Id].InterfaceType = MEA_INTERFACETYPE_Interlaken;
            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_tx_reset_n = MEA_FALSE;
            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_tx_burstmax = 0x3; // 256 byte
            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_tx_burstshort = 0; // 32 byte

            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_tx_mframelen_minus1 = 0x07ff;
            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_tx_rdyout_thresh    = 8;

            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_fc_enable= MEA_TRUE;

            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_tx_fc_callen       = 0x7;
            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_tx_enable=MEA_TRUE;

            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_rx_mframelen_minus1 = 0x07ff;
            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_rx_packet_mode=1;
            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_rx_burstmax = 3;
            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_rx_reset_n = MEA_FALSE;

            MEA_Interface_DataType_Info_Table[Id].Interlaken_data.ctl_tx_fc_enable= MEA_TRUE;
        
        }

        if(InterfaceEntry.valid == MEA_TRUE  && InterfaceEntry.InterfacType == MEA_INTERFACETYPE_RXAUI){
            MEA_Interface_data_Configure_info_Table[Id].valid =MEA_TRUE;
            MEA_Interface_data_Configure_info_Table[Id].sw_enable_reset = MEA_TRUE;

            MEA_Interface_DataType_Info_Table[Id].valid=MEA_TRUE; 
            MEA_Interface_DataType_Info_Table[Id].InterfaceType = MEA_INTERFACETYPE_RXAUI;

            MEA_Interface_DataType_Info_Table[Id].RXAUI_data.RXAUI_reg.val.rxaui_Reset_RX_Link_Status  = 1;
            MEA_Interface_DataType_Info_Table[Id].RXAUI_data.RXAUI_reg.val.rxaui_Reset_Local_Fault = 1;
        
        
        }
        if (InterfaceEntry.valid == MEA_TRUE  && InterfaceEntry.InterfacType == MEA_INTERFACETYPE_SINGLE10G){
            MEA_Interface_data_Configure_info_Table[Id].valid = MEA_TRUE;
            MEA_Interface_data_Configure_info_Table[Id].sw_enable_reset = MEA_FALSE;

            
            MEA_Interface_DataType_Info_Table[Id].InterfaceType = MEA_INTERFACETYPE_SINGLE10G;
           /**/ 
                
                MEA_Interface_DataType_Info_Table[Id].SFP_PLUS_data.SFP_PLUS.val.xmac_IFG_mode = 0;
                MEA_Interface_DataType_Info_Table[Id].SFP_PLUS_data.SFP_PLUS.val.xmac_DIC = 0;
#if HW_BOARD_IS_PCI
                MEA_Interface_DataType_Info_Table[Id].SFP_PLUS_data.SFP_PLUS.val.xmac_DIC = 2;
#endif
                
                
                MEA_Interface_DataType_Info_Table[Id].SFP_PLUS_data.SFP_PLUS.val.XmacRX_block_th = 300; //256  for 2048Jumbo packet
                MEA_Interface_DataType_Info_Table[Id].valid = MEA_TRUE;

         

        }


        if(InterfaceEntry.valid == MEA_TRUE  && InterfaceEntry.InterfacType == MEA_INTERFACETYPE_RGMII){
            MEA_Interface_DataType_Info_Table[Id].valid=MEA_TRUE; 
            MEA_Interface_DataType_Info_Table[Id].InterfaceType = MEA_INTERFACETYPE_RGMII;
        
        }

        if(InterfaceEntry.valid == MEA_TRUE  && InterfaceEntry.InterfacType == MEA_INTERFACETYPE_SGMII){
            MEA_Interface_DataType_Info_Table[Id].valid=MEA_TRUE; 
            MEA_Interface_DataType_Info_Table[Id].InterfaceType = MEA_INTERFACETYPE_SGMII;

            MEA_Interface_DataType_Info_Table[Id].SGMII_data.value.Powerdown = MEA_FALSE;


        }
        if (InterfaceEntry.valid == MEA_TRUE  && InterfaceEntry.InterfacType == MEA_INTERFACETYPE_GMII) {
            MEA_Interface_DataType_Info_Table[Id].valid = MEA_TRUE;
            MEA_Interface_DataType_Info_Table[Id].InterfaceType = MEA_INTERFACETYPE_GMII;

            MEA_Interface_DataType_Info_Table[Id].SGMII_data.value.Powerdown = MEA_FALSE;


        }

        if(InterfaceEntry.valid == MEA_TRUE  && InterfaceEntry.InterfacType == MEA_INTERFACETYPE_QSGMII){
            MEA_Interface_DataType_Info_Table[Id].valid=MEA_TRUE; 
            MEA_Interface_DataType_Info_Table[Id].InterfaceType = MEA_INTERFACETYPE_QSGMII;

            MEA_Interface_DataType_Info_Table[Id].QSGMII_data.Sgmii_info.value.Powerdown = MEA_FALSE;
            //MEA_Interface_DataType_Info_Table[Id].QSGMII_data.GT.value.GT_RX_reset = 0;

        }
        if (InterfaceEntry.valid == MEA_TRUE  && InterfaceEntry.InterfacType == MEA_INTERFACETYPE_RMII){
            MEA_Interface_DataType_Info_Table[Id].valid = MEA_TRUE;
            MEA_Interface_DataType_Info_Table[Id].InterfaceType = MEA_INTERFACETYPE_RMII;

        }
        if (InterfaceEntry.valid == MEA_TRUE  && InterfaceEntry.InterfacType == MEA_INTERFACETYPE_XLAUI){
            MEA_Interface_DataType_Info_Table[Id].valid = MEA_TRUE;
            MEA_Interface_DataType_Info_Table[Id].InterfaceType         = MEA_INTERFACETYPE_XLAUI;
            MEA_Interface_data_Configure_info_Table[Id].sw_enable_reset = MEA_FALSE;


            
            /**/
            MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.tx_enable = MEA_TRUE;
            MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.rx_enable = MEA_TRUE;
            MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.rx_igncrc = MEA_TRUE;
            
            MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.max_packetLen = 0x3fff;
            MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.min_packetLen = 0x40;
            MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.tx_inscrc = MEA_TRUE;

          
            MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.RX_ENABLE_GCP      = MEA_TRUE;
            MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.RX_ENABLE_GPP      = MEA_TRUE;
            MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.RX_PAUSE_GLBL_EN   = MEA_TRUE;
			MEA_Interface_DataType_Info_Table[Id].XLAUI_data.XLAUI.val.TXpause = 0xffffffff;


        }

        if (InterfaceEntry.valid == MEA_TRUE  && InterfaceEntry.InterfacType == MEA_INTERFACETYPE_INTERNAL_TLS){
            MEA_Interface_DataType_Info_Table[Id].valid = MEA_TRUE;
            MEA_Interface_DataType_Info_Table[Id].InterfaceType = MEA_INTERFACETYPE_INTERNAL_TLS;

        }

    }//end 
    

    
    for(Id=0;Id<MEA_INTERFACE_MAX_ID;Id++){
		
		mea_drv_Get_Interface2PortsInfo(Unit, Id, &InterfaceEntry);
        if(MEA_Interface_DataType_Info_Table[Id].valid == MEA_TRUE){

            if(mea_drv_Interface_DataType_UpdateHw(Unit,Id,&MEA_Interface_DataType_Info_Table[Id],NULL) !=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Interface_DataType_UpdateHw failed (id=%d)\n",
                    __FUNCTION__,Id);
                return MEA_ERROR;
            }

        }
    
        if (MEA_Interface_Configure_Table[Id].valid == MEA_TRUE){
            if (Id == MEA_CPU_PORT && (InterfaceEntry.InterfacType == MEA_INTERFACETYPE_RMII)){
                MEA_Interface_Configure_Table[Id].data.autoneg = MEA_FALSE;
                MEA_Interface_Configure_Table[Id].data.speed = MEA_INGRESSPORT_GIGA_DISABLE_100M;
            }
            
            if ( (InterfaceEntry.InterfacType == MEA_INTERFACETYPE_SGMII) ||
                (InterfaceEntry.InterfacType == MEA_INTERFACETYPE_QSGMII) ||
                (InterfaceEntry.InterfacType == MEA_INTERFACETYPE_RGMII)){
                
                
                  MEA_Interface_Configure_Table[Id].data.mdio_Disable = MEA_TRUE;

                if (MEA_device_environment_info.MEA_SFP_SUPPORT_enable == MEA_TRUE ){
                    if (MEA_device_environment_info.MEA_SFP_SUPPORT == 1)
                        MEA_Interface_Configure_Table[Id].data.autoneg = MEA_TRUE;
                    else
                        MEA_Interface_Configure_Table[Id].data.autoneg = MEA_FALSE;

                }
                else
                {
                    MEA_Interface_Configure_Table[Id].data.autoneg = MEA_FALSE;
                }

#if defined(MEA_ETHERNITY) && (defined(MEA_ENV_S1_K7) || defined(MEA_PLAT_S1_VDSL_K7))
                if (MEA_device_environment_info.MEA_SFP_SUPPORT_enable == MEA_TRUE ){
                     
                        mea_setting_sfp_interface(Id, MEA_Interface2PortsInfo[Id].InterfacType);
                    
                }
                
#endif
        }

            if (mea_Interface_Conf_UpdateHw(Unit, Id,
                &(MEA_Interface_Configure_Table[Id].data),
                NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_Interface_Conf_UpdateHw %d failed \n",
                    __FUNCTION__, Id);
                return MEA_ERROR;
            }
        }



    }


    return MEA_OK;
}


MEA_Status mea_drv_Interface_Reinit(MEA_Unit_t Unit)
{


    
    MEA_Interface_t Id;

    

   

    for(Id=0;Id<MEA_INTERFACE_MAX_ID;Id++){
        
            
            if(MEA_Interface_DataType_Info_Table[Id].valid == MEA_FALSE)
                continue;
            
         if(mea_drv_Interface_DataType_UpdateHw(Unit,Id,&MEA_Interface_DataType_Info_Table[Id],NULL) !=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"mea_drv_Interface_DataType_UpdateHw %d is invalid\n",Id);
                    return MEA_ERROR;
          }
            
        
        
        if(MEA_Interface_Configure_Table[Id].valid ==MEA_TRUE){
            if (mea_Interface_Conf_UpdateHw(Unit,Id,
                &(MEA_Interface_Configure_Table[Id].data),
                NULL) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_Interface_Conf_UpdateHw %d failed \n",
                        __FUNCTION__,Id);
                    return MEA_ERROR;
            }
        }
        
    }
    


    
    return MEA_OK;

}



MEA_Bool mea_drv_Interface_Valid(MEA_Unit_t Unit,MEA_Interface_t Id,MEA_Bool silent)
{

    if(!MEA_Interface_DataType_Info_Table[Id].valid){
   
        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"interfacePort %d is invalid\n",Id);
        }
        return MEA_FALSE;
    }
    return MEA_TRUE;
}






MEA_Status mea_drv_Interface_DataType_Set_Entry(MEA_Unit_t Unit,MEA_Interface_t Id,mea_Interface_Data_Type_dbt *entry)
{
    mea_Interface_Data_Type_dbt entry_old;

  
 /*
 * check if the port and type is correct
 */

    
    if(mea_drv_Interface_Get_Enter(Unit,Id,&entry_old)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"mea_drv_Interface_Get_Enter id %d is invalid\n",Id);
         return MEA_ERROR;
    }

    
     if(mea_drv_Interface_DataType_UpdateHw(Unit,Id,entry,&entry_old) !=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"mea_drv_Interface_DataType_UpdateHw id %d is invalid\n",Id);
         return MEA_ERROR;
     }
    
    //save the entry 
     
     MEA_OS_memcpy(&MEA_Interface_DataType_Info_Table[Id], entry, sizeof(MEA_Interface_DataType_Info_Table[0]));
    
    return MEA_OK;
}








/************************************************************************/
/* API Interface_Interlaken                                             */
/************************************************************************/



MEA_Status MEA_API_Set_Interface_Interlaken_Entry(MEA_Unit_t         unit_i,
                                                  MEA_Interface_t         Id,
                                                  MEA_Interlaken_Entry_dbt        *entry)
{

    mea_Interface_Data_Type_dbt data_entry;

    //
    
    // check the the port is valid and type is correct,

    if(MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s Interface not support port %d  \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }
  

    if(mea_drv_Interface_Valid_TypeCorrect(unit_i,Id,MEA_INTERFACETYPE_Interlaken ,MEA_TRUE) != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_drv_Interface_Valid_TypeCorrect port %d is not Correct \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }

    
    if(mea_drv_Interface_Get_Enter(unit_i,Id,&data_entry)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_drv_Interface_Get_Enter port %d failed \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }

    //
    //check parameters
    // 


    MEA_OS_memcpy(&data_entry.Interlaken_data,entry,sizeof(data_entry.Interlaken_data));
    
    if(mea_drv_Interface_DataType_Set_Entry(unit_i,Id,&data_entry)!=MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_drv_Interface_DataType_Set_Entry port %d failed \n",__FUNCTION__,Id); 
        return MEA_ERROR;

    }



    return MEA_OK;
}

MEA_Status MEA_API_Get_Interface_Interlaken_Entry(MEA_Unit_t        unit_i,
                                                  MEA_Interface_t        Id,
                                    MEA_Interlaken_Entry_dbt        *entry)
{
    mea_Interface_Data_Type_dbt data_entry;


    if(MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s Interface not support Id %d  \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }
    
    if(mea_drv_Interface_Valid_TypeCorrect(unit_i,Id,MEA_INTERFACETYPE_Interlaken ,MEA_TRUE) != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," mea_drv_Interface_Valid_TypeCorrect Id %d is not Correct \n",Id); 
        return MEA_ERROR;
    }
    MEA_OS_memset(&data_entry,0,sizeof(data_entry));
    if(mea_drv_Interface_Get_Enter(unit_i,Id,&data_entry)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," mea_drv_Interface_Get_Enter port %d failed \n",Id); 
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry,&data_entry.Interlaken_data,sizeof(*entry));

    return MEA_OK;
}


MEA_Status mea_drv_get_Interlaken_Status(MEA_Unit_t     unit_i,
                                         MEA_Interface_t     Id,
                                         MEA_Interlaken_Status_Entry_dbt    *entry)
{

    MEA_ind_read_t ind_read;
    MEA_Uint32  rd_value_reg1[1];
    MEA_Uint32  rd_value_reg2[1];
    MEA_Uint32  rd_value_reg3[1];
    MEA_Uint32  rd_value_reg4[1];
    mea_memory_mode_e save_globalMemoryMode;
    
    
    save_globalMemoryMode = globalMemoryMode;



    if(Id==0){
       
    ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL;
    ind_read.tableOffset	= MEA_INTERLAKEN_REG_0_RX_STATUS1; 
    ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
    ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_read.statusReg		= MEA_IF_STATUS_REG;   
    ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
    ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data      = &(rd_value_reg1[0]);

    /* Write to the Indirect Table */
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(rd_value_reg1),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (index=%d)\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,MEA_INTERLAKEN_REG_0_RX_STATUS1);
            globalMemoryMode = save_globalMemoryMode;
            return MEA_ERROR;
    }
    


    ind_read.tableOffset	= MEA_INTERLAKEN_REG_4_RX_STATUS2; 
    ind_read.read_data      = &(rd_value_reg2[0]);

    /* Write to the Indirect Table */
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(rd_value_reg2),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (index=%d)\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,ind_read.tableOffset);
            globalMemoryMode = save_globalMemoryMode;
            return MEA_ERROR;
    }

    ind_read.tableOffset	= MEA_INTERLAKEN_REG_8_RX_STATUS2; 
    ind_read.read_data      = &(rd_value_reg3[0]);

    /* Write to the Indirect Table */
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(rd_value_reg3),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (index=%d)\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,ind_read.tableOffset);
            globalMemoryMode = save_globalMemoryMode;
            return MEA_ERROR;
    }
    
    ind_read.tableOffset	= MEA_INTERLAKEN_REG_12_TX_STATUS2; 
    ind_read.read_data      = &(rd_value_reg4[0]);

    /* Write to the Indirect Table */
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(rd_value_reg4),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (index=%d)\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,ind_read.tableOffset);
            globalMemoryMode = save_globalMemoryMode;
            return MEA_ERROR;
    }

    // start to add 

    entry->stat_lanes[0].regs[0] =     rd_value_reg1[0] & 0xff;
    entry->stat_lanes[1].regs[0] =    (rd_value_reg1[0]>>8) & 0xff;
    entry->stat_lanes[2].regs[0] =    (rd_value_reg1[0]>>16) & 0xff;
    entry->stat_lanes[3].regs[0] =    (rd_value_reg1[0]>>24) & 0xff;
    
    entry->stat_lanes[4].regs[0]=    rd_value_reg2[0]       & 0xff;
        
    entry->stat_rx_burst_err =   ((rd_value_reg3[0]>>0) & 0x1); 
   
    
     entry->stat_rx_meop_err =   ((rd_value_reg3[0]>>1)  & 0x1);
     entry->stat_rx_msop_err =   ((rd_value_reg3[0]>>2) & 0x1);
     entry->stat_rx_crc32_err =   ((rd_value_reg3[0]>>3) & 0x1);
     entry->stat_rx_aligned_err =   ((rd_value_reg3[0]>>4) & 0x1);
     entry->stat_rx_aligned =   ((rd_value_reg3[0]>>5) & 0x1);
         
     entry->stat_tx_overflow_err =   ((rd_value_reg4[0]>>0) & 0x1);
     entry->stat_tx_burst_err =    ((rd_value_reg4[0]>>1) & 0x1);
     entry->stat_tx_underflow_err =    ((rd_value_reg4[0]>>2) & 0x1);


    
    
    }//0


    return MEA_OK;
}


MEA_Status MEA_API_Get_Interface_Interlaken_Status_Entry(MEA_Unit_t     unit_i,
                                                         MEA_Interface_t     Id,
                                    MEA_Interlaken_Status_Entry_dbt    *entry)
{

    if(MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s Interface not support for id=%d  \n",Id); 
        return MEA_ERROR;
    }
    if(mea_drv_Interface_Valid_TypeCorrect(unit_i,Id,MEA_INTERFACETYPE_Interlaken ,MEA_TRUE) != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," mea_drv_Interface_Valid_TypeCorrect port %d is not Correct \n",Id); 
        return MEA_ERROR;
    }
    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," entry = NULL \n"); 
        return MEA_ERROR;
    }


    //read the from HW
	if( mea_drv_get_Interlaken_Status(unit_i,Id,entry) !=MEA_OK){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," mea_drv_get_Interlaken_Status failed \n"); 
		return MEA_ERROR;
	}




    return MEA_OK;
}



/************************************************************************/
/* API Interface RXAUI                                                   */
/************************************************************************/


MEA_Status MEA_API_Set_Interface_RXAUI_Entry(MEA_Unit_t             unit_i,
                                             MEA_Interface_t             Id,
                                             MEA_RXAUI_Entry_dbt   *entry)
{
    
    mea_Interface_Data_Type_dbt data_entry;

    if(MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s Interface not support Id %d  \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }

    if(mea_drv_Interface_Valid_TypeCorrect(unit_i,Id,MEA_INTERFACETYPE_RXAUI ,MEA_TRUE) != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_drv_Interface_Valid_TypeCorrect port %d is not Correct \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }
    
    
    if(mea_drv_Interface_Get_Enter(unit_i,Id,&data_entry)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_drv_Interface_Get_Enter port %d failed \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }

    //check parameters for RXAUI
    //

    MEA_OS_memcpy(&data_entry.RXAUI_data,entry,sizeof(data_entry.RXAUI_data));

    if(mea_drv_Interface_DataType_Set_Entry(unit_i,Id,&data_entry)!=MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_drv_Interface_DataType_Set_Entry port %d failed \n",__FUNCTION__,Id); 
        return MEA_ERROR;

    }
    
    return MEA_OK;
}


MEA_Status MEA_API_Get_Interface_RXAUI_Entry(MEA_Unit_t             unit_i,
                                             MEA_Interface_t             Id,
                                    MEA_RXAUI_Entry_dbt             *entry)
{
    mea_Interface_Data_Type_dbt data_entry;


    if(MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s Interface not support port %d  \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }

    if(mea_drv_Interface_Valid_TypeCorrect(unit_i,Id,MEA_INTERFACETYPE_RXAUI ,MEA_TRUE) != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_drv_Interface_Valid_TypeCorrect port %d is not Correct \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }
    MEA_OS_memset(&data_entry,0,sizeof(data_entry));
    if(mea_drv_Interface_Get_Enter(unit_i,Id,&data_entry)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s mea_drv_Interface_Get_Enter port %d failed \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry,&data_entry.RXAUI_data,sizeof(*entry));
  
    
    return MEA_OK;
}


MEA_Status MEA_API_Get_Interface_RXAUI_Status_Entry(MEA_Unit_t     unit_i,
                                                    MEA_Interface_t     Id,
                                                    MEA_RXAUI_Status_Entry_dbt   *entry)
{

#if 0
    MEA_ind_read_t         ind_read;
    MEA_Uint32 rxuaui_event[4];
    MEA_Uint32 value;

    if(MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s Interface not support port %d  \n",__FUNCTION__,Id); 
        return MEA_ERROR;
    }
    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"entry = = NULL \n"); 
        return MEA_ERROR;
    }


    // need to read from HW

    ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset    = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_RXAUI_SERDES_STATUS;
    ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
    ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_read.statusReg		= MEA_IF_STATUS_REG;   
    ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
    ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;



     
    ind_read.read_data   = &(rxuaui_event[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(rxuaui_event),
        MEA_MODULE_IF,
        globalMemoryMode,
        MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }

    if(Id==118) // ID=1 
    {//port 118
    value=(rxuaui_event[0] &0xffff);
     MEA_OS_memcpy(entry,&value,sizeof(entry));
        
    }
    if(Id==119) // ID=2 
    {//port 119
     value=((rxuaui_event[1]>>0) & 0xffff);
     MEA_OS_memcpy(entry,&value,sizeof(entry));
    }
    if(Id==110) // ID=3 
    {//port 110
        value=((rxuaui_event[2]>>0) & 0xffff);
        MEA_OS_memcpy(entry,&value,sizeof(entry));

    }
    if(Id==111) // ID=4 
    {//port 111
        value=((rxuaui_event[3]>>0) & 0xffff);
        MEA_OS_memcpy(entry,&value,sizeof(entry));
    }
#endif 


    return MEA_OK;
}




MEA_Uint32 mea_RxAui_serdes_check_stable(MEA_Unit_t unit,MEA_Interface_t interfaceId)
{
    MEA_Uint32 counLinkNo_fult,i;
    MEA_Uint32 count_togling;
    MEA_RXAUI_Status_Entry_dbt   rxaui_status;
   



    MEA_OS_memset(&MEA_Interface_data_Configure_info_Table[interfaceId].parityRead[0],0,sizeof(MEA_Interface_data_Configure_info_Table[interfaceId].parityRead));
    counLinkNo_fult=0;
   
    for(i=0;i<4;i++){
        if(MEA_API_Get_Interface_RXAUI_Status_Entry(unit,interfaceId,&rxaui_status)!=MEA_OK){
        }

        MEA_Interface_data_Configure_info_Table[interfaceId].parityRead[rxaui_status.Parity]++;
        if(rxaui_status.rx_local_fault == MEA_FALSE){
            counLinkNo_fult++;

        }

    }
    count_togling=0;
    for(i=0 ;i< MEA_PARITIY_MAX_READ; i++){
        if(MEA_Interface_data_Configure_info_Table[interfaceId].parityRead[i]==1)
            count_togling++;

    }//MEA_PARITIY_MAX_READ
    if(count_togling >= 3 && counLinkNo_fult == 4){
        return 0; // ok
    }
    if(count_togling >= 3 && counLinkNo_fult < 4){
        return 1;// reset

    }
    if(count_togling < 3)
        return 2;// need to state

return MEA_FALSE;
}



MEA_Uint32 mea_IL_serdes_check_stable(MEA_Unit_t unit)
{

    MEA_Interface_t interfaceId;
    MEA_Interlaken_Status_Entry_dbt IL_entry;
    //MEA_Interlaken_Entry_dbt        IL_entry_info;
    MEA_Uint32 lanes,Sync,no_sync,rx_aligned_err,i;

    interfaceId=0;


    Sync=0;
    no_sync=0;
    rx_aligned_err=0;

    MEA_OS_memset(&MEA_Interface_data_Configure_info_Table[interfaceId].parityRead[0],0,sizeof(MEA_Interface_data_Configure_info_Table[interfaceId].parityRead));
    //read IL Status
    for(i=0;i<3;i++){
        if( mea_drv_get_Interlaken_Status(unit,interfaceId,&IL_entry) !=MEA_OK){

        }
        if(IL_entry.stat_rx_aligned_err == MEA_TRUE && IL_entry.stat_rx_aligned==MEA_FALSE){
            rx_aligned_err++;
        }
        for(lanes=0;lanes<MEA_INTERLAKEN_MAX_OF_LANES;lanes++){
            if(IL_entry.stat_lanes[lanes].val.stat_rx_synced == MEA_TRUE){
                MEA_Interface_data_Configure_info_Table[interfaceId].parityRead[lanes]++;
            }
        }//
    }// 3 time


    for(lanes=0;lanes<MEA_INTERLAKEN_MAX_OF_LANES;lanes++){
        if(MEA_Interface_data_Configure_info_Table[interfaceId].parityRead[lanes] == 3 )
            Sync++;
        else {
            if(MEA_Interface_data_Configure_info_Table[interfaceId].parityRead[lanes] < 3){
                no_sync++;
            }
        }
    }
    if(no_sync == 3 && rx_aligned_err != 0){
        return 1; //reset


    }else{
        if(Sync == 3  && no_sync == 0  && rx_aligned_err == 0){
            return 0;
            //MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=4;
        }else{
            if(Sync >= 1  && no_sync != 0  && rx_aligned_err != 0){
                return 1;
            } 
        }



    }




    return 0;


}


MEA_Status  mea_DRV_Serdes_Link_check(MEA_Unit_t unit)
{

#if 0 

    MEA_Interface_t interfaceId;
    MEA_Port_t port;
   MEA_RXAUI_Status_Entry_dbt   rxaui_status;
   MEA_RXAUI_Entry_dbt          rxaui_entry;
   MEA_Uint32   i;
   MEA_Uint32 re_preSync;

   
 

    if(Interface_info == MEA_OS_DEVICE_SIMULATION_4200_SYAC){
#if 1 
        {

        
        MEA_Interlaken_Status_Entry_dbt IL_entry;
        MEA_Interlaken_Entry_dbt        IL_entry_info;
       // MEA_Uint32 lanes,Sync,no_sync;

        interfaceId=0;

        



        if(MEA_Interface_data_Configure_info_Table[0].sw_enable_reset == MEA_TRUE && 
           MEA_Interface_DataType_Info_Table[0].InterfaceType == MEA_INTERFACETYPE_Interlaken  )
           {
           if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 0){
             MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=1;
           }

           if(MEA_API_Get_Interface_Interlaken_Entry(unit,interfaceId,&IL_entry_info)!=MEA_OK){
             
           }
               
           if( mea_drv_get_Interlaken_Status(unit,interfaceId,&IL_entry) !=MEA_OK){
              
           }

           if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink ==1){

               if(IL_entry.stat_rx_aligned_err == MEA_TRUE && IL_entry.stat_rx_aligned==MEA_FALSE){
                   MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=2;
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  start to PreSync\n",interfaceId);
                   MEA_OS_fflush(stdout);
               }else{
                   MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=4;
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  start to SYNC\n",interfaceId);
                   MEA_OS_fflush(stdout);
               }
           
           }


           if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 2){
           
             re_preSync=mea_IL_serdes_check_stable(unit);
             if(re_preSync==0){
                 MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=4;
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  PreSync to SYNC\n",interfaceId);
                 MEA_OS_fflush(stdout);

             }else{
                 MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=3;
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  PreSync to RESET\n",interfaceId);
                 MEA_OS_fflush(stdout);

             }

           
           
           }

           if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 3 ){ // makeReset
               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d IL Reset   \n",interfaceId);
               MEA_OS_fflush(stdout);
               //IL_entry_info.ctl_rx_force_resync=1;
               IL_entry_info.ctl_rx_reset_n=1;
               IL_entry_info.ctl_tx_reset_n=1;
               if(MEA_API_Set_Interface_Interlaken_Entry(unit,interfaceId,&IL_entry_info)!=MEA_OK){
               }
               //IL_entry_info.ctl_rx_force_resync=0;
               MEA_OS_sleep(0, 5*1000);
               IL_entry_info.ctl_rx_reset_n=0;
               IL_entry_info.ctl_tx_reset_n=0;
               if(MEA_API_Set_Interface_Interlaken_Entry(unit,interfaceId,&IL_entry_info)!=MEA_OK){
               }
               MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=1;
           
           }

           if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 4){ // sync
               
               if( mea_drv_get_Interlaken_Status(unit,interfaceId,&IL_entry) !=MEA_OK){
                   MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=1;
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d IL Sync To Start error\n",interfaceId);
                   
               }
               
               
              
               if(IL_entry.stat_rx_aligned_err== MEA_TRUE){
                   MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=1;
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d IL Sync To Start \n",interfaceId);
               }


           }

           
        }
        }
           
#endif







      


       for(interfaceId=1; interfaceId<= 4;interfaceId++){
    //    while(1){ interfaceId=1;

            if(interfaceId==1)
                port=118;
            if(interfaceId==2)
                port=119;
            if(interfaceId==3)
                port=110;
            if(interfaceId==4)
                port=111;

            if(MEA_Interface_data_Configure_info_Table[interfaceId].sw_enable_reset == MEA_TRUE && 
            MEA_Interface_DataType_Info_Table[interfaceId].InterfaceType == MEA_INTERFACETYPE_RXAUI  ) 
            {
                if( (MEA_API_Set_IngressPort_Is_AdminUp(unit,port) == MEA_FALSE) && (MEA_API_Set_EgressPort_Is_AdminUp(unit,port) == MEA_FALSE)){
                        MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=0;
                    continue;
                }

                if(MEA_API_Get_Interface_RXAUI_Status_Entry(unit,interfaceId,&rxaui_status)!=MEA_OK){
                    MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=0;
                    continue;
                }

                if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 0){
                    MEA_OS_memset(&MEA_Interface_data_Configure_info_Table[interfaceId].parityRead[0],0,sizeof(MEA_Interface_data_Configure_info_Table[interfaceId].parityRead));
                    MEA_Interface_data_Configure_info_Table[interfaceId].stateLink= 1;
                }
                
                if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 5){//postReset

                    re_preSync=mea_RxAui_serdes_check_stable(unit,interfaceId);
                    if(re_preSync==0){
                     
                    //set ingressPort AdminDown
                        MEA_API_Set_EgressPort_TxEnable(unit,port,MEA_TRUE);
                        MEA_OS_sleep(0, 5*1000);
                        MEA_API_Set_IngressPort_RxEnable(unit,port,MEA_TRUE);

                        MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=4;
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  postReset To  SYNC \n",interfaceId);
                        MEA_OS_fflush(stdout);
                    }

                    if(re_preSync == 2 ){
                        MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=3;
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  postReset To  Reset \n",interfaceId);
                        MEA_OS_fflush(stdout);
                    }
                        

            } // postReset
                /************************************************************************/
                /*                  // makeReset                                       */
                /************************************************************************/
                if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 3 ){ // makeReset
                    if( (MEA_API_Set_IngressPort_Is_AdminUp(unit,port) == MEA_TRUE) && (MEA_API_Set_EgressPort_Is_AdminUp(unit,port)==MEA_TRUE) ){
                        //set ingressPort AdminDown
                        // MEA_API_Set_IngressPort_RxEnable(unit,port,MEA_FALSE);
                        // MEA_API_Set_EgressPort_TxEnable(unit,port,MEA_FALSE);
                       
                        //set reset the interface
                        MEA_API_Get_Interface_RXAUI_Entry(unit,interfaceId,&rxaui_entry);
                        for (i=0; i<3;i++)
                        {
                            rxaui_entry.RXAUI_reg.val.reset_serdes_type =0x3;//0x3;
                            MEA_API_Set_Interface_RXAUI_Entry(unit,interfaceId,&rxaui_entry);
                            MEA_OS_sleep(0, 5*1000);
                            rxaui_entry.RXAUI_reg.val.reset_serdes_type =0x0;
                            MEA_API_Set_Interface_RXAUI_Entry(unit,interfaceId,&rxaui_entry);
                            MEA_OS_sleep(0, 5*1000);

                        }

                        MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=5;//postReset enable port
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  RESET --> postReset\n",interfaceId);
                        MEA_OS_fflush(stdout);
                        
                    }

                } // reset



                
                if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 1){ // start
                    if((MEA_API_Set_IngressPort_Is_AdminUp(unit,port) == MEA_FALSE) && (MEA_API_Set_EgressPort_Is_AdminUp(unit,port)==MEA_FALSE)){
                        MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=0;
                        continue;
                    }
                        if(rxaui_status.rx_local_fault == MEA_TRUE){
                            MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=2;
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  start to PreSync\n",interfaceId);
                            MEA_OS_fflush(stdout);
                        }else{
                            MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=4;
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d start To Sync \n",interfaceId);
                            MEA_OS_fflush(stdout);
                        }
                
                
                }

                if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 2){ // preSync
                     re_preSync=mea_RxAui_serdes_check_stable(unit,interfaceId);
                    if(re_preSync == 1){
                            MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=3; //move to reset;
                            if( MEA_API_Set_IngressPort_Is_AdminUp(unit,port) == MEA_TRUE && MEA_API_Set_EgressPort_Is_AdminUp(unit,port)==MEA_TRUE ){
                                MEA_API_Set_IngressPort_RxEnable(unit,port,MEA_FALSE);
                                MEA_API_Set_EgressPort_TxEnable(unit,port,MEA_FALSE);
                                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  preSync To Reset\n",interfaceId);
                                MEA_OS_fflush(stdout);
                                
                            }
                        }else{
                            if(re_preSync == 0){
                                MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=4;
                             MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  preSync To SYNC\n",interfaceId);
                             MEA_OS_fflush(stdout);
                            }
                     }
                }//end preSync


                if(MEA_Interface_data_Configure_info_Table[interfaceId].stateLink == 4){ // sync
                    if(MEA_API_Get_Interface_RXAUI_Status_Entry(unit,interfaceId,&rxaui_status)!=MEA_OK){
                        MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=1;
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  Sync To Start error\n",interfaceId);
                        continue;
                    }
                    if(rxaui_status.rx_local_fault == MEA_TRUE){
                        MEA_Interface_data_Configure_info_Table[interfaceId].stateLink=1;
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"interfaceId=%d  Sync To Start \n",interfaceId);
                    }


                }




            }else{


            }




        }
    
        
    
    
    
    
    }
    
#endif



    return MEA_OK;

}


/************************************************************************/
/*  Interface    XLAUI API            MEA_INTERFACETYPE_XLAUI           */
/************************************************************************/

/****************************************************************************************************************************/




/************************************************************************/
/*  Interface SFP_PLUS   API                                             */
/************************************************************************/

MEA_Status MEA_API_Set_Interface_SFP_PLUS_Entry(MEA_Unit_t                  unit,
                                                MEA_Interface_t             Id,
                                                MEA_SFP_PLUS_Entry_dbt*     entry)
{ 
    mea_Interface_Data_Type_dbt data_entry;

    if (MEA_API_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_SINGLE10G, MEA_TRUE/*silent*/) == MEA_FALSE){
        return MEA_ERROR;
    }

    if (mea_drv_Interface_Get_Enter(unit, Id, &data_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Get_Enter port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&data_entry.SFP_PLUS_data, entry, sizeof(data_entry.SFP_PLUS_data));

    if (mea_drv_Interface_DataType_Set_Entry(unit, Id, &data_entry) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_DataType_Set_Entry port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;

    }



    return MEA_OK;
}

MEA_Status MEA_API_Get_Interface_SFP_PLUS_Entry(MEA_Unit_t               unit,
                                                MEA_Interface_t          Id,
                                                MEA_SFP_PLUS_Entry_dbt* entry)
{

    mea_Interface_Data_Type_dbt data_entry;


    if (MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s Interface not support port %d  \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    if (mea_drv_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_SINGLE10G, MEA_TRUE) != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Valid_TypeCorrect port %d is not Correct \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }
    MEA_OS_memset(&data_entry, 0, sizeof(data_entry));
    if (mea_drv_Interface_Get_Enter(unit, Id, &data_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Get_Enter port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry, &data_entry.SFP_PLUS_data, sizeof(*entry));

    return MEA_OK;
}




static void  mea_UpdateHwEntry_SFP_PLUS_SetID(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    //MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    // MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    // MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    MEA_Uint32  write_value = (MEA_Uint32)((long)arg4);

    MEA_Uint32     val[1], i;


    val[0] = write_value;


    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }






}




static MEA_Status mea_drv_SFP_PLUS_UpdateHw_SetID(MEA_Unit_t  unit_i, MEA_Interface_t  Id)
{


    MEA_ind_write_t   ind_write;
    MEA_Uint32        interface_id = Id;
    /*build the hw info */


    /* build the ind_write value */
    ind_write.tableType   = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
    ind_write.tableOffset = ENET_REGISTER_TBL100_XMAC_STATUS; //reg Id
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_SFP_PLUS_SetID;


    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)0;
    ind_write.funcParam3 = (MEA_funcParam)0;
    ind_write.funcParam4 = (MEA_funcParam)((long)interface_id);

    if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }





    return MEA_OK;
}




static MEA_Status mea_drv_QSGMII_UpdateHw_SetID(MEA_Unit_t  unit_i, MEA_Interface_t  Id)
{


    MEA_ind_write_t   ind_write;
    MEA_Uint32        interface_id = Id;
    /*build the hw info */


    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
    ind_write.tableOffset = ENET_REGISTER_TBL100_QSGMII_Status_GT; //reg Id
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_SFP_PLUS_SetID;


    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)0;
    ind_write.funcParam3 = (MEA_funcParam)0;
    ind_write.funcParam4 = (MEA_funcParam)((long)interface_id);

    if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        //MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }





    return MEA_OK;
}


MEA_Status MEA_API_Get_Interface_SFP_PLUS_Status_Entry(MEA_Unit_t    unit,
    MEA_Interface_t     Id,
    MEA_SFP_PLUS_Status_Entry_dbt*   entry)
{
    MEA_ind_read_t         ind_read;

    MEA_API_LOG
    
    if (MEA_API_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_SINGLE10G, MEA_TRUE/*silent*/) == MEA_FALSE){
        return MEA_ERROR;
    }

    if(mea_drv_SFP_PLUS_UpdateHw_SetID(unit, Id) != MEA_OK){
    
    }

    

    ind_read.tableType      = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
    ind_read.tableOffset    = ENET_REGISTER_TBL100_XMAC_STATUS; 
    ind_read.cmdReg         = MEA_IF_CMD_REG;
    ind_read.cmdMask        = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg      = MEA_IF_STATUS_REG;
    ind_read.statusMask     = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg   = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask  = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data      = &(entry->regs[0]);

    MEA_OS_memset(entry, 0, sizeof(*entry));

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }





    return MEA_OK;
}

MEA_Status mea_drv_SFP_PLUS_Flash_UpdateHw_SetID(MEA_Unit_t         unit_i, MEA_Interface_t               Id)
{
    return mea_drv_SFP_PLUS_UpdateHw_SetID(unit_i, Id);
}

/************************************************************************/
/*          Interface_XLAUI                                             */
/************************************************************************/
MEA_Status MEA_API_Set_Interface_XLAUI_Entry(MEA_Unit_t    unit,
                                             MEA_Interface_t    Id,
                                             MEA_XLAUI_Entry_dbt*   entry)
{
    mea_Interface_Data_Type_dbt data_entry;

    if (MEA_API_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_XLAUI, MEA_TRUE/*silent*/) == MEA_FALSE){
        return MEA_ERROR;
    }
    if (Id != 127){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "XLAUI interface only on for ID 127\n");
        return MEA_ERROR;
    }

    if (mea_drv_Interface_Get_Enter(unit, Id, &data_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Get_Enter port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    MEA_OS_memset(&data_entry.XLAUI_data, 0, sizeof(data_entry.XLAUI_data));
    MEA_OS_memcpy(&data_entry.XLAUI_data, entry, sizeof(data_entry.XLAUI_data));

    if (mea_drv_Interface_DataType_Set_Entry(unit, Id, &data_entry) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_DataType_Set_Entry port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;

    }



    return MEA_OK;
}


MEA_Status MEA_API_Get_Interface_XLAUI_Entry(MEA_Unit_t             unit,
                                             MEA_Interface_t        Id,
                                             MEA_XLAUI_Entry_dbt*  entry)
{
    mea_Interface_Data_Type_dbt data_entry;


    if (MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s Interface not support port %d  \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    if (mea_drv_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_XLAUI, MEA_TRUE) != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Valid_TypeCorrect port %d is not Correct \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }
    MEA_OS_memset(&data_entry, 0, sizeof(data_entry));
    if (mea_drv_Interface_Get_Enter(unit, Id, &data_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Get_Enter port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry, &data_entry.XLAUI_data, sizeof(*entry));

    return MEA_OK;
}


MEA_Status MEA_API_Get_Interface_XLAUI_Status_Entry(MEA_Unit_t    unit,
    MEA_Interface_t     Id,
    MEA_XLAUI_Status_Entry_dbt*   entry)
{
    MEA_ind_read_t         ind_read;

    MEA_API_LOG

        if (MEA_API_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_XLAUI, MEA_TRUE/*silent*/) == MEA_FALSE){
            return MEA_ERROR;
        }

    
    if (Id != 127){
        return MEA_ERROR;
    }
    /**/



    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
    ind_read.tableOffset = 2;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(entry->regs[0]);

    MEA_OS_memset(entry, 0, sizeof(*entry));

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        1,//MEA_NUM_OF_ELEMENTS(entry->regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
    ind_read.tableOffset = 3;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(entry->regs[1]);

    MEA_OS_memset(entry, 0, sizeof(*entry));

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        1,//MEA_NUM_OF_ELEMENTS(entry->regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }









    return MEA_OK;
}


/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status mea_drv_Interface_set_Sgmii(MEA_Unit_t unit, MEA_Interface_t Id, MEA_Bool AN_Reset, MEA_InterfaceType_t InterfacType)
{
    MEA_SGMII_Entry_dbt Sgmii_entry;
    MEA_QSGMII_Entry_dbt Qgmii_entry;

    if (InterfacType == MEA_INTERFACETYPE_SGMII) {
        MEA_API_Get_Interface_SGMII_Entry(unit, Id, &Sgmii_entry);
        Sgmii_entry.value.AN_reset = AN_Reset;

        MEA_API_Set_Interface_SGMII_Entry(unit, Id, &Sgmii_entry);
        
    }
    if (InterfacType == MEA_INTERFACETYPE_QSGMII) {
        MEA_API_Get_Interface_QSGMII_Entry(unit, Id, &Qgmii_entry);
        Qgmii_entry.Sgmii_info.value.AN_reset = AN_Reset;

        MEA_API_Set_Interface_QSGMII_Entry(unit, Id, &Qgmii_entry);
    
    }




    return MEA_OK;

}

/************************************************************************/
/* SGMII                                                                */
/************************************************************************/

MEA_Status MEA_API_Set_Interface_SGMII_Entry(MEA_Unit_t    unit,
    MEA_Interface_t    Id,
    MEA_SGMII_Entry_dbt*   entry)
{

    mea_Interface_Data_Type_dbt data_entry;

    if (MEA_API_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_SGMII, MEA_TRUE/*silent*/) == MEA_FALSE && 
        MEA_API_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_QSGMII, MEA_TRUE/*silent*/) == MEA_FALSE
        
        ){
        return MEA_ERROR;
    }
    

    if (mea_drv_Interface_Get_Enter(unit, Id, &data_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Get_Enter port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&data_entry.SGMII_data, entry, sizeof(data_entry.SGMII_data));

    if (mea_drv_Interface_DataType_Set_Entry(unit, Id, &data_entry) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_DataType_Set_Entry port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;

    }

    return MEA_OK;
}

MEA_Status MEA_API_Get_Interface_SGMII_Entry(MEA_Unit_t    unit,
                                             MEA_Interface_t    Id,
                                             MEA_SGMII_Entry_dbt*   entry)
{

    mea_Interface_Data_Type_dbt data_entry;


    if (MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s Interface not support port %d  \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    if (mea_drv_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_SGMII, MEA_TRUE) == MEA_FALSE /*&&
        mea_drv_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_QSGMII, MEA_TRUE) == MEA_FALSE*/    ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Valid_TypeCorrect port %d is not Correct \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }
    MEA_OS_memset(&data_entry, 0, sizeof(data_entry));
    if (mea_drv_Interface_Get_Enter(unit, Id, &data_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Get_Enter port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry, &data_entry.SGMII_data, sizeof(*entry));

    return MEA_OK;


}







/************************************************************************/
/* RGMII                                                                */
/************************************************************************/

MEA_Status MEA_API_Get_Interface_RGMII_Status_Entry(MEA_Unit_t     unit_i,
                                                    MEA_Interface_t     Id,
                                                    MEA_RGMII_Status_Entry_dbt   *entry)

{
   MEA_Uint32 retread;

    if(entry == NULL){
        return MEA_ERROR;
    }

    if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,Id,MEA_INTERFACETYPE_RGMII,MEA_TRUE/*silent*/)==MEA_FALSE){
        return MEA_ERROR;
    }


    MEA_API_WriteReg(unit_i,ENET_IF_CTR_GMII_SPEED,Id,MEA_MODULE_IF);

      retread= MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_CTR_GMII_SPEED,MEA_MODULE_IF);
     entry->reg[0]= retread;

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Alex dbg id %d  0x%08x 0x%08x\n",Id,entry->reg[0],retread);

    return MEA_OK;
} 

/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_API_Get_Interface_SGMII_Status_Entry(MEA_Unit_t unit_i, 
                                                    MEA_Interface_t Id,
                                                    MEA_SGMII_Status_Entry_dbt   *entry)
{
    MEA_Uint32 speed[5];
    if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,Id,MEA_INTERFACETYPE_SGMII,MEA_TRUE/*silent*/)==MEA_FALSE){
        return MEA_ERROR;
    }
    speed[0] = 2;
    speed[1] = 1;
    speed[2] = 0;
    speed[3] = 3;
    speed[4] = 4;

    MEA_API_WriteReg(unit_i,ENET_IF_CTR_GMII_SPEED,Id,MEA_MODULE_IF);

    entry->reg[0]= MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_CTR_GMII_SPEED,MEA_MODULE_IF);
    if (MEA_Interface_Configure_Table[Id].data.autoneg == MEA_FALSE){
        entry->value.duplex = MEA_TRUE;
        entry->value.speed = speed[MEA_Interface_Configure_Table[Id].data.speed];
    }
    
    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/


MEA_Status MEA_API_Set_Interface_QSGMII_Entry(MEA_Unit_t    unit,
    MEA_Interface_t    Id,
    MEA_QSGMII_Entry_dbt*   entry)
{

    mea_Interface_Data_Type_dbt data_entry;

    if ( MEA_API_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_QSGMII, MEA_TRUE/*silent*/) == MEA_FALSE

        ){
        return MEA_ERROR;
    }


    if (mea_drv_Interface_Get_Enter(unit, Id, &data_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Get_Enter port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&data_entry.QSGMII_data, entry, sizeof(data_entry.QSGMII_data));

    if (mea_drv_Interface_DataType_Set_Entry(unit, Id, &data_entry) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_DataType_Set_Entry port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;

    }


    return MEA_OK;
}

MEA_Status MEA_API_Get_Interface_QSGMII_Entry(MEA_Unit_t    unit,
    MEA_Interface_t    Id,
    MEA_QSGMII_Entry_dbt*   entry) {

    mea_Interface_Data_Type_dbt data_entry;


    if (MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s Interface not support port %d  \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    if (mea_drv_Interface_Valid_TypeCorrect(unit, Id, MEA_INTERFACETYPE_QSGMII, MEA_TRUE) == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Valid_TypeCorrect port %d is not Correct \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }
    MEA_OS_memset(&data_entry, 0, sizeof(data_entry));
    if (mea_drv_Interface_Get_Enter(unit, Id, &data_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Get_Enter port %d failed \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry, &data_entry.QSGMII_data, sizeof(*entry));

    return MEA_OK;

}





MEA_Status MEA_API_Get_Interface_QSGMII_Status_Entry(MEA_Unit_t unit_i, 
                                                    MEA_Interface_t Id,
                                                    MEA_QSGMII_Status_Entry_dbt   *entry)
{
    MEA_Uint32 speed[5];


    if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,Id,MEA_INTERFACETYPE_QSGMII,MEA_TRUE/*silent*/)==MEA_FALSE){
        return MEA_ERROR;
    }
    
    
    speed[0] = 2;
    speed[1] = 1;
    speed[2] = 0;
    speed[3] = 3;
    speed[4] = 4;

    MEA_API_WriteReg(unit_i,ENET_IF_CTR_GMII_SPEED,Id,MEA_MODULE_IF);
    entry->qsgmii[0].reg[0] = MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_CTR_GMII_SPEED,MEA_MODULE_IF);

    if (MEA_Interface_Configure_Table[Id].data.autoneg == MEA_FALSE){
        entry->qsgmii[0].value.duplex = MEA_TRUE;
        entry->qsgmii[0].value.speed = speed[MEA_Interface_Configure_Table[Id].data.speed];
    }

    MEA_API_WriteReg(unit_i,ENET_IF_CTR_GMII_SPEED,Id+1,MEA_MODULE_IF);
    entry->qsgmii[1].reg[0] = MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_CTR_GMII_SPEED,MEA_MODULE_IF);
    
    if (MEA_Interface_Configure_Table[Id+1].data.autoneg == MEA_FALSE){
        entry->qsgmii[1].value.duplex = MEA_TRUE;
        entry->qsgmii[1].value.speed = speed[MEA_Interface_Configure_Table[Id+1].data.speed];
    }


    MEA_API_WriteReg(unit_i,ENET_IF_CTR_GMII_SPEED,Id+2,MEA_MODULE_IF);
    entry->qsgmii[2].reg[0] = MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_CTR_GMII_SPEED,MEA_MODULE_IF);
    
    if (MEA_Interface_Configure_Table[Id + 2].data.autoneg == MEA_FALSE){
        entry->qsgmii[2].value.duplex = MEA_TRUE;
        entry->qsgmii[2].value.speed = speed[MEA_Interface_Configure_Table[Id+2].data.speed];
    }

    MEA_API_WriteReg(unit_i,ENET_IF_CTR_GMII_SPEED,Id+3,MEA_MODULE_IF);
    entry->qsgmii[3].reg[0] = MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_CTR_GMII_SPEED,MEA_MODULE_IF);

    if (MEA_Interface_Configure_Table[Id + 3].data.autoneg == MEA_FALSE){
        entry->qsgmii[3].value.duplex = MEA_TRUE;
        entry->qsgmii[3].value.speed = speed[MEA_Interface_Configure_Table[Id + 3].data.speed];
    }


    return MEA_OK;
}



MEA_Status MEA_API_Get_Interface_QSGMII_GT_Status_Entry(MEA_Unit_t unit_i,
    MEA_Interface_t Id,
    MEA_QSGMII_GT_Status_Entry_dbt   *entry)
{
    MEA_ind_read_t         ind_read;

    MEA_API_LOG

    if (MEA_Interface_DataType_Info_Table == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s Interface not support port %d  \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    if (mea_drv_Interface_Valid_TypeCorrect(unit_i, Id, MEA_INTERFACETYPE_QSGMII, MEA_TRUE) == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_Interface_Valid_TypeCorrect port %d is not Correct \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }


    /************************************************************************/
    /* Write the interface ID                                               */
    /************************************************************************/
    if (mea_drv_QSGMII_UpdateHw_SetID(unit_i, Id) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s mea_drv_QSGMII_UpdateHw_SetID port %d is not Correct \n", __FUNCTION__, Id);
        return MEA_ERROR;
    }

    /************************************************************************/
    /* read the Status GT                                                   */
    /************************************************************************/


    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
    ind_read.tableOffset = ENET_REGISTER_TBL100_QSGMII_Status_GT;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(entry->regs[0]);

    MEA_OS_memset(entry, 0, sizeof(*entry));

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

    return MEA_OK;
}




MEA_Bool MEA_API_Interface_Valid_TypeCorrect(MEA_Unit_t Unit,MEA_Interface_t Id,MEA_InterfaceType_t InterfaceType ,MEA_Bool silent)
{

    return mea_drv_Interface_Valid_TypeCorrect(Unit,Id,InterfaceType ,silent);
}

MEA_Bool mea_drv_Interface_Valid_TypeCorrect(MEA_Unit_t Unit,MEA_Interface_t Id,MEA_InterfaceType_t InterfaceType ,MEA_Bool silent)
{
	MEA_drv_Interface2Port_dbt InterfaceEntry;
	

    if(Id >= MEA_INTERFACE_MAX_ID){
        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"interfacePort %d is invalid\n",Id);
        }
        return MEA_FALSE;
    }

    if(MEA_Interface_DataType_Info_Table==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"interfacePort not support\n",Id);
        return MEA_FALSE;
    }
	
	mea_drv_Get_Interface2PortsInfo(Unit, Id, &InterfaceEntry);
    if (InterfaceEntry.valid == MEA_FALSE) {
        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "interfacePort %d is invalid\n", Id);
        }
        return MEA_FALSE;
    }

   

    if (InterfaceEntry.InterfacType == InterfaceType)
    {
        return MEA_TRUE;
    }

    if(InterfaceEntry.InterfacType != InterfaceType){

        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"interfacePort %d is invalid\n",Id);
        }
        return MEA_FALSE;
    }


   
    


    return MEA_FALSE;
}



MEA_Bool MEA_API_Interface_Valid(MEA_Unit_t Unit,MEA_Interface_t Id,MEA_Bool silent)
{
	MEA_drv_Interface2Port_dbt InterfaceEntry;
	

    if(Id > MEA_INTERFACE_MAX_ID){
        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"interfacePort %d is invalid\n",Id);
        }
        return MEA_FALSE;
    }
	
	mea_drv_Get_Interface2PortsInfo(Unit, Id, &InterfaceEntry);
    if(!InterfaceEntry.valid){

        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"interfacePort %d is invalid\n",Id);
        }
        return MEA_FALSE;
    }

    if(MEA_Interface_DataType_Info_Table[Id].valid)
    {
        return MEA_TRUE;
    }
    
    return MEA_FALSE;
}

/***********************************************************************************************************/

#ifdef BL_PLATFORM
#define SMI_GET_RESULT  0x00
#define SMI_READ_REG    0x01
#define SMI_WRITE_REG   0x02
#define SMI_I2C_DEV     "/dev/i2c-8"
#define PHY_MAX_PORT_NUM    6
typedef struct {
  MEA_Uint8 port;
  MEA_Uint8 phy_num;
} addr_map_t;

static const addr_map_t addr_map[PHY_MAX_PORT_NUM] = {
  {100, 11},
  {101, 10},
  {102, 9},
  {103, 8},
  {104, 5},
  {105 ,7}
};

static pthread_mutex_t phy_bus_lock;

static
int iic_smbus_read_block_data(int bus, uint8_t command, uint8_t *ptr)
{
        union i2c_smbus_data msg;
        struct i2c_smbus_ioctl_data msg_ctl;

        msg_ctl.read_write = I2C_SMBUS_READ;
        msg_ctl.command = command;
        msg_ctl.size = I2C_SMBUS_BLOCK_DATA;
        msg_ctl.data = &msg;
        if (ioctl(bus, I2C_SMBUS, &msg_ctl) < 0) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "SMBUS read block transaction failed: %s\n", strerror(errno));
                return -1;
        }
        memcpy(ptr, msg.block + 1, msg.block[0]);
        return msg.block[0];
}

static
int iic_smbus_write_block_data(int bus, uint8_t command, uint8_t len, uint8_t *data)
{
        union i2c_smbus_data msg;
        struct i2c_smbus_ioctl_data msg_ctl;

        msg.block[0] = len;
        memcpy(msg.block + 1, data, len);
        msg_ctl.read_write = I2C_SMBUS_WRITE;
        msg_ctl.command = command;
        msg_ctl.size = I2C_SMBUS_BLOCK_DATA;
        msg_ctl.data = &msg;
        if (ioctl(bus, I2C_SMBUS, &msg_ctl) < 0)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "SMBUS write block transaction failed: %s\n", strerror(errno));
            return -1;
        }
        return 0;
}

static
uint16_t smi_read_reg(int bus, uint8_t phy_addr, uint8_t reg_addr)
{
    uint8_t msg[4];

    if (ioctl(bus, I2C_SLAVE, 0x25) < 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Failed to set I2C address: %s\n", strerror(errno));
        return -1;
    }
    msg[0] = phy_addr;
    msg[1] = reg_addr;
    if (iic_smbus_write_block_data(bus, SMI_READ_REG, 2, msg) == -1)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error writing to phy: PHY#%u, REG#%u=%#06x\n", msg[0], msg[1], (msg[3] << 8) | msg[2]);
        return -1;
    }
    if (iic_smbus_read_block_data(bus, SMI_GET_RESULT, msg) == -1)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error reading from phy: PHY#%u, REG#%u=%#06x\n", msg[0], msg[1], (msg[3] << 8) | msg[2]);
        return -1;
    }
    if (msg[0] == phy_addr && msg[1] == reg_addr)
        return (msg[3] << 8) | msg[2];
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Got unexpected result: PHY#%u, REG#%u=%#06x\n", msg[0], msg[1], (msg[3] << 8) | msg[2]);
    return -1;
}


static
int smi_write_reg(int bus, uint8_t phy_addr, uint8_t reg_addr, uint16_t value)
{
    uint8_t msg[4];

    if (ioctl(bus, I2C_SLAVE, 0x25) < 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Failed to set I2C address: %s\n", strerror(errno));
        return -1;
    }
    msg[0] = phy_addr;
    msg[1] = reg_addr;
    msg[2] = (uint8_t)value;
    msg[3] = (uint8_t)(value >> 8);
    return iic_smbus_write_block_data(bus, SMI_WRITE_REG, 4, msg);
}

// Get link status from PHY.
// 1 - link up
// 0 - link down
static
MEA_Status MEA_API_phy_get_link_status(int reg, MEA_Uint8 *state)
{
    MEA_Uint16 id;
    MEA_Status result = MEA_OK;
    MEA_LOCK(&phy_bus_lock);
    int bus = open(SMI_I2C_DEV, O_RDWR);
    *state = 0;

    if (bus < 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error opening I2C bus: %s\n", strerror(errno));
        MEA_UNLOCK(&phy_bus_lock);
        return MEA_ERROR;
    }

    if (smi_write_reg(bus, reg, 22, 0) == -1) //select page 0
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error writing to phy\n");
        result = MEA_ERROR;
        goto out;
    }

    id = smi_read_reg(bus, reg, 1);

    if (id == -1)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error reading from phy\n");
        result = MEA_ERROR;
        goto out;
    }

    if ((id&4) >> 2)
    {
        *state = 1;
        result = MEA_OK;
        goto out;
    }
    if (smi_write_reg(bus, reg, 22, 1) == -1) // select page 1
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error writing to phy\n");
        result = MEA_ERROR;
        goto out;
    }

    id = smi_read_reg(bus, reg, 1);

    if (id == -1)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error reading from phy\n");
        result = MEA_ERROR;
        goto out;
    }

    *state = (id&4) >> 2;
    result = MEA_OK;
out:
  /* all done, close bus and exit */
    close(bus);
    MEA_UNLOCK(&phy_bus_lock);
    return result;
}
#endif


MEA_Status mea_drv_Get_Interface_Link_Status(MEA_Unit_t unit ,MEA_OutPorts_Entry_dbt *interfaceStatus)
{

   MEA_ind_read_t         ind_read;
   MEA_Uint32             retVal[1];
   MEA_Uint32 i;
#ifdef BL_PLATFORM
   MEA_Uint8 state = 0;
   int j;
#endif

   if(interfaceStatus==NULL){
        return MEA_ERROR;
   }

    ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_LINK_UP_INTERFACE_TBL_EVENT;
    ind_read.cmdReg	   	    = MEA_IF_CMD_REG;
    ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg		= MEA_IF_STATUS_REG;
    ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data      = &(retVal[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(retVal),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }


#ifdef BL_PLATFORM
// Fill retval according to BL config
    for (i = 0; i < PHY_MAX_PORT_NUM; i++)
    {
        if (MEA_API_phy_get_link_status(addr_map[i].phy_num, &state) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s call failed, port = %d\n",
                __FUNCTION__, addr_map[i].port);
            return MEA_ERROR;

        }

        for (j = 0; j < MEA_MAX_INERTNAL_INTERFACE_NUMBER - 2; j++)
        {
            if (MEA_InterfaceHwInfo[j] != addr_map[i].port)
               continue;

            retVal[0] = (retVal[0] & ~(1 << j)) | (state << j);
            break;
        }
    }
#endif

    if(MEA_LinkStatusPrevious != retVal[0]){
        //MEA_CLEAR_ALL_OUTPORT(interfaceStatus);

        for (i=0;i<MEA_MAX_INERTNAL_INTERFACE_NUMBER-2 ; i++){
            if(((retVal[0]>>i) & 0x00000001) == 1 ){
              MEA_SET_OUTPORT(interfaceStatus,   MEA_InterfaceHwInfo[i]);
           }
        }

      
     MEA_OS_memcpy(&MEA_LinkInterfaceStatusPrevious,interfaceStatus,sizeof(MEA_OutPorts_Entry_dbt));
     MEA_LinkStatusPrevious=retVal[0];

    }else{
     /*no change */
      MEA_OS_memcpy(interfaceStatus,&MEA_LinkInterfaceStatusPrevious,sizeof(MEA_OutPorts_Entry_dbt));
    }

    return MEA_OK;
}


MEA_Status MEA_API_Get_Interface_CurrentLinkStatus(MEA_Unit_t unit,MEA_Uint16 interfaceId, MEA_Bool *state ,MEA_Bool silence)
{
	MEA_drv_Interface2Port_dbt InterfaceEntry;
#ifdef BL_PLATFORM
    int i;
    MEA_Uint8 phy_state;
#endif

    if(state==NULL){
        return MEA_ERROR;
    }

#ifdef BL_PLATFORM
    for (i = 0; i < PHY_MAX_PORT_NUM; i++)
    {
        if (addr_map[i].port != interfaceId)
           continue;

        if (MEA_API_phy_get_link_status(addr_map[i].phy_num, &phy_state) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s call failed, ifIndex = %d\n",
                __FUNCTION__, interfaceId);
            return MEA_ERROR;

        }

        *state = (phy_state == 1) ? MEA_TRUE : MEA_FALSE;
        if(!silence)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Interface[%d] is %s \n",
                interfaceId, (*state == MEA_TRUE) ? "UP" : "Down");
        return MEA_OK;
    }
#endif

	mea_drv_Get_Interface2PortsInfo(unit, interfaceId, &InterfaceEntry);

    if(InterfaceEntry.valid){
        if(MEA_IS_SET_OUTPORT(&MEA_My_LinkInterfaceStatusPrevious,interfaceId)){
            if(!silence)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Interface[%d] is %s \n", interfaceId,  (MEA_IS_SET_OUTPORT(&MEA_My_LinkInterfaceStatusPrevious,interfaceId)) ? "UP" : "Down"     );

            *state=MEA_TRUE;
        }else{
            if(!silence)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Interface[%d] is %s \n", interfaceId,  (MEA_IS_SET_OUTPORT(&MEA_My_LinkInterfaceStatusPrevious,interfaceId)) ? "UP" : "Down"     );
            *state=MEA_FALSE;
        }
    }else{
       if(!silence)
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"interface %d is not valid\n",interfaceId);
       return MEA_ERROR;
    }



    return MEA_OK;
}

MEA_Status MEA_API_set_Interface_ext_Link_callBack(MEA_Unit_t unit,pGetIntExtLinkSttCallBack callBack)
{
	linkStatusCallback = callBack;
	return MEA_OK;
}

MEA_Status MEA_API_get_Interface_ext_Link_Status(MEA_Unit_t unit, MEA_Port_t interfaceId, MEA_Bool *status)
{
	MEA_Status ret=MEA_OK;
	if(linkStatusCallback != NULL)
	{
		ret = linkStatusCallback(unit,interfaceId,status);
	}
	return ret;
}
MEA_Status MEA_API_get_Interface_oper_Link_Status(MEA_Unit_t unit, MEA_Port_t interfaceId, MEA_Bool *status)
{
	MEA_drv_Interface2Port_dbt InterfaceEntry;

	mea_drv_Get_Interface2PortsInfo(unit, interfaceId, &InterfaceEntry);

	if(InterfaceEntry.valid == MEA_TRUE)
	{
		(*status) = MEA_TRUE;
		if(MEA_IS_SET_OUTPORT(&MEA_LinkInterfaceStatusPrevious, interfaceId) == 0)
		{
			(*status) = MEA_FALSE;
		}
		else if(linkStatusCallback != NULL)
		{
			linkStatusCallback(unit,interfaceId,status);
		}
		return MEA_OK;
	}
	return MEA_ERROR;
	
}

MEA_Status MEA_API_Check_Interface_Link_Status(MEA_Unit_t unit, MEA_Bool current)
{
    MEA_Port_t interfaceId;
    MEA_OutPorts_Entry_dbt interfaceStatus;
    MEA_OutPorts_Entry_dbt linkchange;
    MEA_Uint16 lagId;



    MEA_OS_memset(&interfaceStatus, 0, sizeof(interfaceStatus));
    MEA_OS_memset(&linkchange, 0, sizeof(linkchange));

    mea_drv_Get_Interface_Link_Status(unit, &interfaceStatus);


    if ((MEA_OS_memcmp(&MEA_My_LinkInterfaceStatusPrevious,
        &(interfaceStatus),
        sizeof(interfaceStatus)) == 0)) {
        return MEA_OK;
    }
    if (interfaceStatus.out_ports_0_31 != MEA_My_LinkInterfaceStatusPrevious.out_ports_0_31) {
        linkchange.out_ports_0_31 = interfaceStatus.out_ports_0_31 ^ MEA_My_LinkInterfaceStatusPrevious.out_ports_0_31;
    }
    if (interfaceStatus.out_ports_32_63 != MEA_My_LinkInterfaceStatusPrevious.out_ports_32_63) {
        linkchange.out_ports_32_63 = interfaceStatus.out_ports_32_63 ^ MEA_My_LinkInterfaceStatusPrevious.out_ports_32_63;
    }
    if (interfaceStatus.out_ports_64_95 != MEA_My_LinkInterfaceStatusPrevious.out_ports_64_95) {
        linkchange.out_ports_64_95 = interfaceStatus.out_ports_64_95 ^ MEA_My_LinkInterfaceStatusPrevious.out_ports_64_95;
    }
    if (interfaceStatus.out_ports_96_127 != MEA_My_LinkInterfaceStatusPrevious.out_ports_96_127) {
        linkchange.out_ports_96_127 = interfaceStatus.out_ports_96_127 ^ MEA_My_LinkInterfaceStatusPrevious.out_ports_96_127;
    }

    for (interfaceId = 0; interfaceId < MEA_MAX_PORT_NUMBER - 1; interfaceId++) {
        if (current == MEA_FALSE) {
            if (MEA_IS_SET_OUTPORT(&linkchange, interfaceId) && mea_drv_Get_Interface2port_isValid(unit, interfaceId) == MEA_TRUE) {
                MEA_Bool status = (MEA_IS_SET_OUTPORT(&interfaceStatus, interfaceId)) ? MEA_TRUE : MEA_FALSE;

                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Interface[%d] is %s \n", interfaceId, (MEA_IS_SET_OUTPORT(&interfaceStatus, interfaceId)) ? "UP" : "Down");
                MEA_API_get_Interface_ext_Link_Status(unit, interfaceId, &status);

            }
        }
        else {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " Current Interface[%d] Status is %s \n", interfaceId, (MEA_IS_SET_OUTPORT(&interfaceStatus, interfaceId)) ? "UP" : "Down");

        }


        if (mea_drv_LAG_find_by_cluster(MEA_UNIT_0, interfaceId, &lagId) == MEA_TRUE)
        {
            if (MEA_IS_SET_OUTPORT(&linkchange, interfaceId))
            {
                if (MEA_IS_SET_OUTPORT(&interfaceStatus, interfaceId))
                {
                    if (MEA_API_LAG_Set_ClusterMask(MEA_UNIT_0, lagId, interfaceId, MEA_TRUE) != MEA_OK)
                    {
                        return MEA_ERROR;
                    }
                }
                else
                {
                    if (MEA_API_LAG_Set_ClusterMask(MEA_UNIT_0, lagId, interfaceId, MEA_FALSE) != MEA_OK)
                    {
                        return MEA_ERROR;
                    }
                }
            }
        }
    }

    /* set only on interface that was change */
    MEA_OS_memcpy(&MEA_My_LinkInterfaceStatusPrevious, &interfaceStatus, sizeof(MEA_OutPorts_Entry_dbt));



    return MEA_OK;
}

/***********************************************************************************************************/




MEA_Status MEA_API_IS_Interface_Valid(MEA_Unit_t       unit, MEA_Interface_t  id, MEA_Bool *Valid, MEA_Bool silence){

    if (Valid == NULL){
        return MEA_ERROR;
    }

    *Valid = MEA_FALSE;

    if (id > MEA_MAX_PORT_NUMBER){
        if (!silence)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "%s  interface Id %d is out of range ", __FUNCTION__, id);
        
        return MEA_ERROR;
    }
    if (MEA_Interface_Configure_Table[id].valid == MEA_TRUE){
        *Valid = MEA_TRUE;
        return MEA_OK;
    } 
    
    if (!silence)
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "%s  interface Id %d is not valid  ", __FUNCTION__, id);
    
    return MEA_ERROR;
}

MEA_Status MEA_API_Set_Interface_ConfigureEntry(MEA_Unit_t       unit,
                                                MEA_Interface_t  id,
                                                MEA_Interface_Entry_dbt*     entry)
{

    
    ENET_Shaper_Profile_key_dbt  shaperKeyDbt;
   
    
     ENET_Shaper_Profile_dbt      shaperDbt;
	 MEA_drv_Interface2Port_dbt InterfaceEntry;
	 

    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s entry = NULL",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if(id>MEA_MAX_PORT_NUMBER){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s  interface Id %d is out of range ",__FUNCTION__,id);
          return MEA_ERROR;
    } 
	
	mea_drv_Get_Interface2PortsInfo(unit, id, &InterfaceEntry);


    if(MEA_Interface_Configure_Table[id].valid ==  MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s  interface Id %d is not valid  ",__FUNCTION__,id);
        return MEA_ERROR;
    } 

   if(mea_drv_Get_Interface_ShaperIs_On_interface(unit,id) == MEA_FALSE &&
       entry->shaper_enable == MEA_TRUE){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s  interface Id %d is not allowed to enable shaper ",__FUNCTION__,id);
           return MEA_ERROR;
   }


   if(MEA_MAC_LB_SUPPORT == MEA_FALSE &&  entry->loopback.lb_type != 0){



       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - LB_SUPPORT InterfaceId %d not support LB \n",
           __FUNCTION__,id);
       return MEA_ERROR; 


   }

   if ((InterfaceEntry.InterfacType == MEA_INTERFACETYPE_SGMII) ||
	   InterfaceEntry.InterfacType == MEA_INTERFACETYPE_QSGMII){
#if 0
       if (entry->autoneg != MEA_Interface_Configure_Table[id].data.autoneg){
           if (entry->autoneg == MEA_TRUE) {
               /*Set to SGMII AN_Reset */
               mea_drv_Interface_set_Sgmii(unit, id, MEA_TRUE /*AN_reset*/, MEA_Interface2PortsInfo[id].InterfacType);

           }
           else {

           }
       }
#endif
   }

   
   /* configure the shaper on interface */
    if(InterfaceEntry.ShaperEnable_On_interface == MEA_TRUE){
        
       
        /*create profile shaper*/
        if(entry->shaper_enable == MEA_TRUE){

            MEA_OS_memset(&shaperDbt,0,sizeof(shaperDbt));
            shaperDbt.CIR  = entry->shaper_info.CIR;
            shaperDbt.CBS  = entry->shaper_info.CBS;
            shaperDbt.type = entry->shaper_info.resolution_type;
            shaperDbt.overhead = entry->shaper_info.overhead;
            shaperDbt.Cell_Overhead = entry->shaper_info.Cell_Overhead;
            shaperDbt.mode = entry->shaper_info.mode;
            MEA_OS_strcpy(shaperDbt.name,"Interface");
            MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
            shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PORT_TYPE;
            shaperKeyDbt.acm_mode    = 0;
            shaperKeyDbt.id          = MEA_PLAT_GENERATE_NEW_ID;
            if (enet_AddOwner_Shaper_Profile(unit,&shaperKeyDbt,&shaperDbt) == ENET_ERROR) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - enet_AddOwner_Shaper_Profile \n can not create shaper profile for Interface %d\n",
                    __FUNCTION__,id);
                return MEA_ERROR;
            }
            entry->shaper_Id = shaperKeyDbt.id;
        }
    
    }

    if(entry->interfaceTyps != InterfaceEntry.InterfacType)
       entry->interfaceTyps = InterfaceEntry.InterfacType;

    /* Update the Hardware */
    if (mea_Interface_Conf_UpdateHw(unit,id,
        entry,
        &(MEA_Interface_Configure_Table[id].data)
        ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_Interface_Conf_UpdateHw id %d failed \n",
                __FUNCTION__,id);
            return MEA_ERROR;
    }



    /* Only */
    if(InterfaceEntry.ShaperEnable_On_interface == MEA_TRUE){
    
        /* delete old shaper profile */
        if (MEA_Interface_Configure_Table[id].data.shaper_enable == ENET_TRUE) { //enable
            MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
            shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PORT_TYPE;
            shaperKeyDbt.acm_mode    = 0;
            shaperKeyDbt.id          = MEA_Interface_Configure_Table[id].data.shaper_Id;
            if (enet_DelOwner_Shaper_Profile(unit,&shaperKeyDbt) == ENET_ERROR) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - enet_DelOwner_Shaper_Profile \n can not delete shaper profile for interface %d\n",
                    __FUNCTION__,id);
                return MEA_ERROR;
            }

        }
    }


   /* Update */
    MEA_OS_memcpy(&(MEA_Interface_Configure_Table[id].data),
        entry,
        sizeof(MEA_Interface_Configure_Table[0].data));



    
    return MEA_OK;

}
#if defined(MEA_ETHERNITY) && (defined(MEA_ENV_S1_K7) || defined(MEA_PLAT_S1_VDSL_K7))
void mea_update_interface_config_from_sfp(MEA_Interface_t  Id,MEA_Interface_Entry_dbt *pEntry)
{
	if(Id > MEA_MAX_PORT_NUMBER)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_update_interface_config_from_sfp %d failed \n",__FUNCTION__, Id);
		return;
	}
	//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "%s - mea_update_interface_config_from_sfp id=%d speed=%d autoneg=%d \n",__FUNCTION__, Id,pEntry->speed,pEntry->autoneg);
	MEA_Interface_Configure_Table[Id].data.speed = pEntry->speed;
	MEA_Interface_Configure_Table[Id].data.autoneg = pEntry->autoneg;
}
#endif



MEA_Status MEA_API_Get_Interface_ConfigureEntry(MEA_Unit_t         unit,
                                                MEA_Interface_t     Id,
                                                MEA_Interface_Entry_dbt* entry)
{

    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s entry = NULL",__FUNCTION__);
        return MEA_ERROR;
    }

    if(Id>MEA_MAX_PORT_NUMBER){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s  interface Id %d is out of range ",__FUNCTION__,Id);
        return MEA_ERROR;
    } 

    if(MEA_Interface_Configure_Table[Id].valid ==  MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s  interface Id %d is not valid  ",__FUNCTION__,Id);
        return MEA_ERROR;
    } 



/* update the caller*/
    MEA_OS_memcpy(entry
        ,&(MEA_Interface_Configure_Table[Id].data),
        sizeof(*entry));




return MEA_OK;

}


MEA_Status MEA_API_Get_Interface_ToPortMapp(MEA_Unit_t       unit,
	MEA_Interface_t  Id,
	MEA_OutPorts_Entry_dbt  *OutPorts)
{
	if (OutPorts == NULL)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "%s entry = NULL", __FUNCTION__);
		return MEA_ERROR;
	}


	if (Id > MEA_MAX_PORT_NUMBER)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "%s  interface Id %d is out of range ", __FUNCTION__, Id);
		return MEA_ERROR;
	}
	if (MEA_Interface_Configure_Table[Id].valid == MEA_FALSE)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "%s  interface Id %d is not valid  ", __FUNCTION__, Id);
		return MEA_ERROR;
	}

	return mea_drv_Get_Interface_ToPortMapp(unit, Id, OutPorts);


}





