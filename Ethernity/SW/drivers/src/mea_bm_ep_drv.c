/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "MEA_platform.h"

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_bm_drv.h"
#include "mea_drv_common.h"
#include "mea_init.h"
#include "mea_port_drv.h"
#include "enet_shaper_profile_drv.h"
#include "enet_queue_drv.h"
#include "mea_if_service_drv.h"
#include "mea_globals_drv.h"
#include "mea_bm_ep_drv.h"
#include "mea_swe_ip_drv.h"


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/









/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/

				  

/*----------------------------------------------------------------------------*/
/*             Global variables                                       */
/*----------------------------------------------------------------------------*/
extern MEA_Uint32    MEA_InterfaceHwInfo[MEA_MAX_INERTNAL_INTERFACE_NUMBER];

static MEA_EgressPort_Entry_dbt MEA_EgressPort_Table[MEA_MAX_PORT_NUMBER+1];

//static MEA_Uint32               MEA_EgressPort_MartiniCmd_Table = 0;

static MEA_Bool                 MEA_EgressPort_InitDone = MEA_FALSE;
static MEA_OutPorts_Entry_dbt   MEA_EgressPort_Table_tx_enable;
static MEA_OutPorts_Entry_dbt   MEA_EgressPort_Table_tx_enable_save_for_ReInit;

static MEA_OutPorts_Entry_dbt   MEA_EgressPort_Table_halt;

static MEA_OutPorts_Entry_dbt   MEA_EgressPort_Table_flush;
static MEA_OutPorts_Entry_dbt   MEA_EgressPort_Table_flush_save_for_ReInit;

static MEA_OutPorts_Entry_dbt   MEA_EgressPort_Table_pause_enable;
static MEA_OutPorts_Entry_dbt   MEA_EgressPort_Table_pause_enable_save_for_ReInit;


static MEA_OutPorts_Entry_dbt   MEA_EgressPort_Table_calc_crc;

static MEA_MacAddr  MEA_EgressPort_MacAddrTable[MEA_MAX_PORT_NUMBER+1];

static MEA_Uint32  MEA_EgressPort_Protect_1p1_reg[2];

static MEA_Uint32  MEA_EgressPort_Protect_1p1_Enable[1];

static MEA_Uint32 MEA_EgressPort_Table_Frag_Sch[2];

extern MEA_Uint32 mea_global_1p1_down_up_count_And_ports_enable;

MEA_global_Egress_sa_mac_dbt  MEA_Global_MASK_EGRESS_MAC[MEA_MAX_GOLBL_MASK_EGRESS_MAC];

MEA_Uint32     mea_Global_SA_MASK_BY_Interface;

MEA_Bool  MEA_EgressPort_save_Admin[MEA_MAX_PORT_NUMBER + 1];

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/



MEA_Status mea_EgressPort_Update_Dependency(MEA_Unit_t                unit,
                                 MEA_Port_t                port,
                                 MEA_EgressPort_Entry_dbt* entry);

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
static MEA_Status mea_EgressPort_UpdateHwPortProtect(MEA_Port_t                port,
                                                     MEA_EgressPort_Entry_dbt *entry,
                                                     MEA_EgressPort_Entry_dbt *old_entry);




static void mea_drv_EgressPort_Class_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_EgressPort_Entry_dbt  *entry    = (MEA_EgressPort_Entry_dbt  *)arg4;
    



    MEA_Uint32                 val[3];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    MEA_Uint32   setnet2net1;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;
    

    count_shift=0;
    
    MEA_OS_insert_value(count_shift,
                3,
        (MEA_Uint32)entry->egressFilter.egress_port_state,
        &val[0]);
    count_shift+=3;

    MEA_OS_insert_value(count_shift,
        1,
        (MEA_Uint32)entry->egressFilter.compress_enable,
        &val[0]);
    count_shift+=1;

    for(i=0;i<32;i++){
       setnet2net1 = 0;
      setnet2net1 = (MEA_Uint32)(entry->egressFilter.L2Type_Mask[i].net1_enableMask & 0x1);
      setnet2net1 |= (MEA_Uint32)(entry->egressFilter.L2Type_Mask[i].net2_enableMask<<1);
      
      MEA_OS_insert_value(count_shift,
          2,
          (MEA_Uint32)setnet2net1,
          &val[0]);
         count_shift+=2;

    }
    MEA_OS_insert_value(count_shift,
        3,
        (MEA_Uint32)0,
        &val[0]);
    count_shift += 3;

    MEA_OS_insert_value(count_shift,
        1,
        (MEA_Uint32)entry->enable_te_id_stamping,
        &val[0]);
    count_shift += 1;



    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}

 static MEA_Status mea_EgressPort_Class_Rule(MEA_Port_t  port,MEA_EgressPort_Entry_dbt *entry, MEA_EgressPort_Entry_dbt  *old_entry)
{
    
   
    MEA_ind_write_t      ind_write;





    if(!MEA_EGRESS_PARSER_SUPPORT)
        return MEA_OK;

    /* check for NO change */
    if ((MEA_EgressPort_InitDone                    ) &&
        (old_entry != NULL                          ) &&            
        (MEA_OS_memcmp(&entry->egressFilter,
        &old_entry->egressFilter,
        sizeof(entry->egressFilter))==0 )            && 
        (entry->enable_te_id_stamping == old_entry->enable_te_id_stamping)
        ) {
            return MEA_OK;
    }

    

    /* Prepare ind_write structure */
    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_EGRESS_DEC_PARSER;
    ind_write.tableOffset   = port; 
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg     = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_EgressPort_Class_UpdateHwEntry;
    //ind_write.funcParam1    = (MEA_Uint32)unit_i;
    //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
    //ind_write.funcParam3    = (MEA_Uint32);
  
    ind_write.funcParam4 = (MEA_funcParam)(entry);

    /* Write to the Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
        return ENET_ERROR;
    }


    
    return MEA_OK;
}



 MEA_Status mea_EgressPort_Debug_UpdateHw_PauseEnable(MEA_Port_t port, MEA_Bool Enable)
 {

     MEA_Uint32 *pPortGrp;

     MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
     MEA_Uint32 offset;
	 MEA_Uint32 NumOFShift;

     pPortGrp = (MEA_Uint32 *)(&(MEA_EgressPort_Table_pause_enable.out_ports_0_31));
     pPortGrp += (port / 32);

	 NumOFShift = (port % 32);
     (*pPortGrp) &= ~(0x0000001 << NumOFShift);
     (*pPortGrp) |= (Enable << (NumOFShift));

     if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {

         MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

         if (port < 256){ // (port <= 255) 
             offset = ENET_REGISTER_TBL100_Pause_TX_MAC_0_255;
             MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_pause_enable.out_ports_0_31, sizeof(BlockOutPort));

         }
#ifdef MEA_OUT_PORT_256_511
         if (port >= 256 && port < 512){
             offset = ENET_REGISTER_TBL100_Pause_TX_MAC_256_511;
             MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_pause_enable.out_ports_256_287, sizeof(BlockOutPort));

         }
#endif

         /*config ENET_REGISTER_TBL100_Admin_RX */
         MEA_Configure_BlockOF256PORTS_TABLE100(MEA_UNIT_0, &BlockOutPort, offset);


     }
     else{
         /* Not support */
            //          MEA_API_WriteReg(MEA_UNIT_0,
            //              ENET_IF_PORT_TX_ENABLE_31_0_REG + ((port / 32) * 4),
            //              (*pPortGrp),
            //              MEA_MODULE_IF);
     }
    
     

     return MEA_OK;
 }

 MEA_Status mea_EgressPort_Debug_UpdateHwTxEnable(MEA_Port_t                port,
                                                    MEA_Bool Enable)
 {

    MEA_Uint32 *pPortGrp;
	MEA_Uint32 NumOFShift;
    
        MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
        MEA_Uint32 offset;
    
    pPortGrp=(MEA_Uint32 *)(&(MEA_EgressPort_Table_tx_enable.out_ports_0_31));
    pPortGrp += (port/32);

	NumOFShift = (port % 32);
    (*pPortGrp) &= ~(0x0000001       <<(NumOFShift));
    (*pPortGrp) |=  (Enable<<(NumOFShift));

    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {

        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

        if (port < 256){
            offset = ENET_REGISTER_TBL100_Admin_TX_MAC_0_255;
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_0_31, sizeof(BlockOutPort));

        }
#ifdef MEA_OUT_PORT_256_511
        if (port >= 256 && port < 512){
            offset = ENET_REGISTER_TBL100_Admin_TX_MAC_256_511;
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_256_287, sizeof(BlockOutPort));

        }
#endif

        /*config ENET_REGISTER_TBL100_Admin_RX */
        MEA_Configure_BlockOF256PORTS_TABLE100(MEA_UNIT_0, &BlockOutPort, offset);


    }
    else{

        return MEA_ERROR;

    }
    MEA_EgressPort_Table[mea_get_port_mapArray(port)].tx_enable=Enable;

         return MEA_OK;
 }

 MEA_Status mea_EgressPort_Debug_UpdateHwTxFlush(MEA_Port_t  port,MEA_Bool Enable)
 {

     MEA_Uint32 *pPortGrp;
    
         MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
         MEA_Uint32 offset;
		 MEA_Uint32 NumOFShift;

     pPortGrp = (MEA_Uint32 *)(&(MEA_EgressPort_Table_flush.out_ports_0_31));
     pPortGrp += (port / 32);

	 NumOFShift = (port % 32);
     (*pPortGrp) &= ~(0x0000001 << (NumOFShift));
     (*pPortGrp) |= (Enable << (NumOFShift));

     if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {

         MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

         if (port < 256){
             offset = ENET_REGISTER_TBL100_FLUSH_0_255;
             MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_flush.out_ports_0_31, sizeof(BlockOutPort));

         }
#ifdef MEA_OUT_PORT_256_511
         if (port >= 256 && port < 512){
             offset = ENET_REGISTER_TBL100_FLUSH_256_511;
             MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_flush.out_ports_256_287, sizeof(BlockOutPort));

         }
#endif


         /*config ENET_REGISTER_TBL100_Admin_RX */
         MEA_Configure_BlockOF256PORTS_TABLE100(MEA_UNIT_0, &BlockOutPort, offset);


     }
     else{
         return MEA_ERROR;

     }
     MEA_EgressPort_Table[mea_get_port_mapArray(port)].flush = Enable;

     return MEA_OK;
 }


 MEA_Status mea_EgressPort_Debug_PauseStatus_info(MEA_Unit_t unit, MEA_OutPorts_Entry_dbt *out_entry)
 {


     MEA_ind_read_t ind_read;
     MEA_Internal_Block_256OutPorts_dbt BlockOutPort;

     MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
     ind_read.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;

     ind_read.tableOffset = ENET_REGISTER_TBL100_PauseStatus_0_255;
     ind_read.cmdReg = MEA_IF_CMD_REG;
     ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
     ind_read.statusReg = MEA_IF_STATUS_REG;
     ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
     ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
     ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
     ind_read.read_data = &(BlockOutPort.regs[0]);

     /* Write to the Indirect Table */
     if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
         MEA_NUM_OF_ELEMENTS(BlockOutPort.regs),
         MEA_MODULE_IF,
         globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (index=%d)\n",
             __FUNCTION__, ind_read.tableType, MEA_MODULE_IF, MEA_INTERLAKEN_REG_0_RX_STATUS1);

         return MEA_ERROR;
     }

     MEA_OS_memcpy(&out_entry->out_ports_0_31, &BlockOutPort, sizeof(BlockOutPort));


     if (MEA_MAX_PORT_NUMBER >= 256) {
         
         MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

         ind_read.tableOffset = ENET_REGISTER_TBL100_PauseStatus_256_511;
         ind_read.cmdReg = MEA_IF_CMD_REG;
         ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
         ind_read.statusReg = MEA_IF_STATUS_REG;
         ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
         ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
         ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
         ind_read.read_data = &(BlockOutPort.regs[0]);

         /* Write to the Indirect Table */
         if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
             MEA_NUM_OF_ELEMENTS(BlockOutPort.regs),
             MEA_MODULE_IF,
             globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (index=%d)\n",
                 __FUNCTION__, ind_read.tableType, MEA_MODULE_IF, MEA_INTERLAKEN_REG_0_RX_STATUS1);

             return MEA_ERROR;
         }

#ifdef MEA_OUT_PORT_256_511
         MEA_OS_memcpy(&out_entry->out_ports_256_287, &BlockOutPort, sizeof(BlockOutPort));
#endif     
     }







     return MEA_OK;
 }

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwTxEnable>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_EgressPort_UpdateHwTxEnable(MEA_Port_t                port,
                                                  MEA_EgressPort_Entry_dbt *entry,
                                                  MEA_EgressPort_Entry_dbt *old_entry)
{


	MEA_Uint32 *pPortGrp;
    MEA_Uint32 *pPortGrp_flush;
    MEA_Bool   setflush=0;
	MEA_Uint32 NumOFShift;

    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;

    /* check for NO change */
    if ((MEA_EgressPort_InitDone                    ) &&
        (old_entry != NULL                          ) &&            
        (entry->tx_enable    == old_entry->tx_enable)  ) {
       return MEA_OK;
    }

	pPortGrp=(MEA_Uint32 *)(&(MEA_EgressPort_Table_tx_enable.out_ports_0_31));
	pPortGrp += (port/32);

    pPortGrp_flush=(MEA_Uint32 *)(&(MEA_EgressPort_Table_flush.out_ports_0_31));
    pPortGrp_flush += (port/32);

   if(MEA_STANDART_BONDING_SUPPORT){
       //check if the port is bonding
       if(entry->tx_enable== 1)
         setflush= 0;
       else
        setflush= 0;

	   NumOFShift = (port % 32);
       (*pPortGrp_flush) &= ~(0x0000001       <<(NumOFShift));
      (*pPortGrp_flush) |=  ((setflush)<<(NumOFShift));


   
   }else{
		if(entry->tx_enable== 1)
           setflush= 0;
		else
           setflush= 1;

		NumOFShift = (port % 32);
		(*pPortGrp_flush) &= ~(0x0000001       <<(NumOFShift));
		(*pPortGrp_flush) |=  ((setflush)<<(NumOFShift));

   }

	NumOFShift = (port % 32);
	(*pPortGrp) &= ~(0x0000001       <<(NumOFShift));
	(*pPortGrp) |=  (entry->tx_enable<<(NumOFShift));

    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {

        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));


        if (port < 256){

            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_0_31, sizeof(BlockOutPort));
            MEA_Configure_BlockOF256PORTS_TABLE100(MEA_UNIT_0, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_0_255);

            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_flush.out_ports_0_31, sizeof(BlockOutPort));
            MEA_Configure_BlockOF256PORTS_TABLE100(MEA_UNIT_0, &BlockOutPort, ENET_REGISTER_TBL100_FLUSH_0_255);

        }
        if (port >= 256 && port < 512){
#ifdef MEA_OUT_PORT_256_511
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_256_287, sizeof(BlockOutPort));
            MEA_Configure_BlockOF256PORTS_TABLE100(MEA_UNIT_0, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_256_511);

            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_flush.out_ports_256_287, sizeof(BlockOutPort));
            MEA_Configure_BlockOF256PORTS_TABLE100(MEA_UNIT_0, &BlockOutPort, ENET_REGISTER_TBL100_FLUSH_256_511);
#endif
        }




    }
    else{
        return MEA_ERROR;
 
    }

	
     entry->flush=setflush;

	
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwCalcCrc>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_EgressPort_UpdateHwCalcCrc(MEA_Port_t                port,
                                                 MEA_EgressPort_Entry_dbt *entry,
                                                 MEA_EgressPort_Entry_dbt *old_entry)
{



	MEA_Uint32 *pPortGrp;
	MEA_Uint32 NumOFShift;
    MEA_Uint32 offset = 0;
    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;

    /* check for NO change */
    if ((MEA_EgressPort_InitDone                     ) &&
        (old_entry != NULL                           ) &&            
        (entry->calc_crc    == old_entry->calc_crc)  ) {
       return MEA_OK;
    }

	pPortGrp=(MEA_Uint32 *)(&(MEA_EgressPort_Table_calc_crc.out_ports_0_31));
	pPortGrp += (port/32);

	NumOFShift = (port % 32);
	(*pPortGrp) &= ~(0x00000001      <<(NumOFShift));
	(*pPortGrp) |=  (entry->calc_crc <<(NumOFShift));

    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

        /*Write to TBL 100 RX */
        if (port < 256){
            offset = ENET_REGISTER_TBL100_TX_CRC_CALC_MAC_0_255;
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_calc_crc.out_ports_0_31, sizeof(BlockOutPort));

        }

        else if (port >= 256 && port < 512){
#ifdef MEA_OUT_PORT_256_511
            offset = ENET_REGISTER_TBL100_TX_CRC_CALC_MAC_256_511;
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_calc_crc.out_ports_256_287, sizeof(BlockOutPort));
#endif
        }

        /*config ENET_REGISTER_TBL100_Admin_RX */
        MEA_Configure_BlockOF256PORTS_TABLE100(MEA_UNIT_0, &BlockOutPort, offset);



    }
    else {


        return MEA_ERROR;


    }


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwCfg>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_EgressPort_UpdateHwCfg(MEA_Port_t                port,
                                             MEA_EgressPort_Entry_dbt *entry,
                                             MEA_EgressPort_Entry_dbt *old_entry)
{


#if 0
    /* check for NO change */
    if ((MEA_EgressPort_InitDone != MEA_TRUE  ) ||
        ((old_entry != NULL) &&
         (entry->halt              != old_entry->halt))  ) {

	    MEA_Uint32 *pPortGrp;
	    
        pPortGrp=(MEA_Uint32 *)(&(MEA_EgressPort_Table_halt.out_ports_0_31));
	    pPortGrp += (port/32);

   
	    (*pPortGrp) &= ~(0x0000001  <<(port%32));
	    (*pPortGrp) |=  (entry->halt<<(port%32));

        MEA_API_WriteReg(MEA_UNIT_0,
                         MEA_IF_PORT_HALT_31_0_REG+((port/32)*4),
	                     (*pPortGrp),
                         MEA_MODULE_IF);
    }
#endif
#if 0
    /* check for NO change */
    if ((MEA_EgressPort_InitDone != MEA_TRUE        ) ||
        ((old_entry != NULL) &&
         (entry->flush            != old_entry->flush)  )) {

	    MEA_Uint32 *pPortGrp;
	    
        pPortGrp=(MEA_Uint32 *)(&(MEA_EgressPort_Table_flush.out_ports_0_31));
	    pPortGrp += (port/32);

   
	    (*pPortGrp) &= ~(0x0000001   <<(port%32));
	    (*pPortGrp) |=  (entry->flush<<(port%32));

        MEA_API_WriteReg(MEA_UNIT_0,
                         MEA_IF_GLOBAL_REG(0x17+(port/32)),
	                     (*pPortGrp),
                         MEA_MODULE_IF);
    }
#endif




    /* return to caller */
	return MEA_OK;


}


#if 0
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwEntryPortProtect>                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/  
static void mea_EgressPort_UpdateHwEntryPortProtect(MEA_EgressPort_Entry_dbt *entry)
{

    MEA_Uint32 entryVal;

	entryVal  = (entry->phy_port <<
                 MEA_OS_calc_shift_from_mask(MEA_BM_TBL_PORT_PROTECT_MASK));

 	MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,entryVal,MEA_MODULE_BM);

}



#endif
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwPortProtect>                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/  
static MEA_Status mea_EgressPort_UpdateHwPortProtect(MEA_Port_t                port,
                                                     MEA_EgressPort_Entry_dbt *entry,
                                                     MEA_EgressPort_Entry_dbt *old_entry) 
{
#if 0
	MEA_ind_write_t           ind_write;
#endif
	MEA_Globals_Entry_dbt     global;



    /* check for NO change */
    if ((MEA_EgressPort_InitDone     ) &&
        (old_entry != NULL) &&
        (entry->phy_port == old_entry->phy_port)  ) {
       return MEA_OK;
    }

	/* for switching port 125 to 126 there use the patch for port protect */
	if ( (port == 125) &&
		 ( (entry->phy_port == 126) || (entry->phy_port == 125)))
	{
		if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&global) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - Error while get API_Get_Globals_Entry\n",
                 __FUNCTION__);
			return MEA_ERROR;
		}
		global.if_global1.val.if_Protection_bit = (entry->phy_port != 125);
		if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&global) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - Error while get API_set_Globals_Entry\n",
                 __FUNCTION__);
			return MEA_ERROR;
		}

	}

#if 0
    /* build the ind_write value */
	ind_write.tableType		= MEA_BM_TBL_PORT_PROTECT;
	ind_write.tableOffset	= port;
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_write.funcParam1	= (MEA_Uint32)entry;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_EgressPort_UpdateHwEntryPortProtect;
	
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_BM,port);
		return MEA_ERROR;
    }
#endif

    /* return to caller */
	return MEA_OK;


}

#if 0
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwEntryMtu>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/  
static void mea_EgressPort_UpdateHwEntryMtu(MEA_EgressPort_Entry_dbt *entry)
{

    MEA_Uint32 entryVal;

	entryVal  = (entry->MTU <<
                 MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MTU_SIZE_MASK));

 	MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,entryVal,MEA_MODULE_BM);

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwMtu>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/  
static MEA_Status mea_EgressPort_UpdateHwMtu(MEA_Port_t                port,
                                             MEA_EgressPort_Entry_dbt *entry,
                                             MEA_EgressPort_Entry_dbt *old_entry) 
{
	MEA_ind_write_t           ind_write;



    /* check for NO change */
    if ((MEA_EgressPort_InitDone     ) &&
        (old_entry != NULL) &&
        (entry->MTU == old_entry->MTU)  ) {
       return MEA_OK;
    }

    /* build the ind_write value */
	ind_write.tableType		= MEA_BM_TBL_TYP_EGRESS_PORT_MTU_SIZE;
	ind_write.tableOffset	= port;
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_write.funcParam1	= (MEA_Uint32)entry;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_EgressPort_UpdateHwEntryMtu;
	
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_BM,port);
		return MEA_ERROR;
    }

    /* return to caller */
	return MEA_OK;


}
#endif


MEA_Uint32 MEA_CALC_MAC_PASUE(MEA_MacAddr My_Mac,MEA_Bool type)
{
 MEA_Uint8 packtPause[64];

MEA_OS_memset(&packtPause,0,sizeof(packtPause));

packtPause[0]=0x01;
packtPause[1]=0x80;
packtPause[2]=0xc2;
packtPause[3]=0x00;
packtPause[4]=0x00;
packtPause[5]=0x01;

//SA
packtPause[6]=My_Mac.b[0];
packtPause[7]=My_Mac.b[1];
packtPause[8]=My_Mac.b[2];
packtPause[9]=My_Mac.b[3];
packtPause[10]=My_Mac.b[4];
packtPause[11]=My_Mac.b[5];


//
packtPause[12]=0x88;
packtPause[13]=0x08;

packtPause[14]=0x00;
packtPause[15]=0x01;


if(type == 0){
    packtPause[16]=0x00;
    packtPause[17]=0x00;
}else{

    packtPause[16]=0xff;
    packtPause[17]=0xff;
}


return (MEA_crc32_ccitt(&packtPause[0],60));


}

void mea_EgressPort_UpdateHwEntryMAC(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    MEA_Unit_t                  unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t             hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t               hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_EgressPort_Entry_dbt*  entry = (MEA_EgressPort_Entry_dbt*)arg4;
    MEA_Uint32                  val[5]; 
    MEA_Uint32                  i;
    MEA_Uint32                  count_shift;

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
    count_shift=0;
    
    MEA_OS_insert_value(count_shift,
        32,
        entry->pause_send.CRC_XON ,
        &val[0]);
    count_shift+=32;
    MEA_OS_insert_value(count_shift,
        32,
        entry->pause_send.CRC_XOFF ,
        &val[0]);
    count_shift+=32;

    val[2] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[2])))<<24);
    val[2] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[3])))<<16);
    val[2] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[4])))<< 8);
    val[2] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[5])))<< 0);

    val[3] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[0])))<< 8);
    val[3] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[1])))<< 0);
    count_shift+=48;
    
    MEA_OS_insert_value(count_shift,
        10,
        entry->pause_send.Transmit_threshold,
        &val[0]);
    count_shift+=10;
    MEA_OS_insert_value(count_shift,
        10,
        entry->pause_send.AF_threshold ,
        &val[0]);
    count_shift+=10;

    MEA_OS_insert_value(count_shift,
        1,
        entry->pause_send.Working_mode ,
        &val[0]);
    count_shift+=1;

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i) , 
            val[i] , 
            MEA_MODULE_IF);
    }

}


static MEA_Status mea_EgressPort_UpdateHwMAC(MEA_Port_t                port,
                                             MEA_EgressPort_Entry_dbt *entry,
                                             MEA_EgressPort_Entry_dbt *old_entry)
{
  
  MEA_ind_write_t             ind_write;
  
  /* check for NO change */
 
            if(!MEA_PLATFORM_IS_RX_PAUSE_SUPPORT)
           return MEA_OK;

  if ((MEA_EgressPort_InitDone     ) &&
      (old_entry != NULL) &&
      (MEA_OS_maccmp(&(entry->My_Mac),&(old_entry->My_Mac))==0 ) &&
      (MEA_OS_memcmp(&(entry->pause_send),
                     &(old_entry->pause_send),
                        sizeof(MEA_EgressPort_pause_send_dbt) )== 0)   
    ) {
          return MEA_OK;
    }


  //col function to calculate the CRC for pause 
  
 entry->pause_send.CRC_XON  = MEA_CALC_MAC_PASUE(entry->My_Mac,0);
 entry->pause_send.CRC_XOFF = MEA_CALC_MAC_PASUE(entry->My_Mac,1);




     /* build the ind_write value */
     ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_TYPE_INGRESS_PAUSE;
     ind_write.tableOffset    = port; 
     ind_write.cmdReg        = MEA_IF_CMD_REG;      
     ind_write.cmdMask        = MEA_IF_CMD_PARSER_MASK;      
     ind_write.statusReg        = MEA_IF_STATUS_REG;   
     ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
     ind_write.tableAddrReg    = MEA_IF_CMD_PARAM_REG;
     ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;  

     ind_write.writeEntry    = (MEA_FUNCPTR)mea_EgressPort_UpdateHwEntryMAC;
     ind_write.funcParam1    = (MEA_Uint32)0;
     ind_write.funcParam2    = (MEA_Uint32)0;
     ind_write.funcParam3    = 0;
     ind_write.funcParam4 = (MEA_funcParam)entry;
     if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - MEA_API_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
             __FUNCTION__,
             ind_write.tableType,
             ind_write.tableOffset,
             MEA_MODULE_IF);
         return MEA_ERROR;
     }

//     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,",entry->pause_send.Transmit_threshold %d  \n",entry->pause_send.Transmit_threshold);
//     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,",entry->pause_send.AF_threshold %d  \n",entry->pause_send.AF_threshold);


  



     /* return to caller */
     return MEA_OK;








}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwEntryMartiniEtherType>                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#if 0
static void mea_EgressPort_UpdateHwEntryMartiniCmd
                                            (MEA_EgressPort_Entry_dbt *entry )
{

 	MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0, MEA_EgressPort_MartiniCmd_Table ,MEA_MODULE_BM);
}
#endif




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwEntryShaperEtherType>                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_EgressPort_UpdateHwEntryShaperCmd(MEA_EgressPort_Entry_dbt *p_entry /*,MEA_Uint32      shaperId*/)
{
	MEA_Uint32 val,temp;
  //  MEA_Uint32  shaper_Id = shaperId;
    MEA_EgressPort_Entry_dbt *entry = (MEA_EgressPort_Entry_dbt *)p_entry;

    /* Build register 0 */
    val   = 0;
	if (entry->shaper_enable == MEA_TRUE){
		temp  = ENET_SHAPER_BYPASS_TYPE_ENABLE;
		temp &= (MEA_BM_SHAPER_BYPASS_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PORT)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PORT);
		val  |= temp;

		temp  = entry->shaper_info.CBS;    // the initial value for the port CIR credit is the profile max ( = profile CBS)
		temp &= (MEA_BM_SHAPER_CIR_BACKET_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_CIR_BACKET_MASK_PORT)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_CIR_BACKET_MASK_PORT);
		val  |= temp;
	        
        temp = entry->shaper_Id;
		temp &= (MEA_BM_SHAPER_PROF_NUMBER_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_PROF_NUMBER_MASK_PORT)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_PROF_NUMBER_MASK_PORT);
		val  |= temp;

		temp  = entry->Shaper_compensation;
		temp &= (MEA_BM_SHAPER_COMP_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PORT)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PORT);
		val  |= temp;
	}else{
		temp  = ENET_SHAPER_BYPASS_TYPE_DISABLE;
		temp &= (MEA_BM_SHAPER_BYPASS_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PORT)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PORT);
		val  |= temp;
        temp  = entry->Shaper_compensation;
		temp &= (MEA_BM_SHAPER_COMP_MASK_PORT >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PORT)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PORT);
		val  |= temp;
	}
	MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,val,MEA_MODULE_BM);
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHwShaperCmd>                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_EgressPort_UpdateHwShaperCmd (MEA_EgressPort_Entry_dbt *entry, MEA_EgressPort_Entry_dbt *old_entry) 
{  

	MEA_ind_write_t ind_write;
     MEA_db_HwUnit_t hwUnit;

    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_ERROR;)

    if((MEA_EgressPort_InitDone     ) && 
        (old_entry) &&
        (entry->shaper_Id           == old_entry->shaper_Id          ) && 
        (entry->Shaper_compensation == old_entry->Shaper_compensation) &&
        (entry->shaper_enable       == old_entry->shaper_enable      ) &&
        (MEA_OS_memcmp(&(entry->shaper_info),
                       &(old_entry->shaper_info),
                       sizeof(entry->shaper_info)) == 0              )  ) {
        return MEA_OK;
    } 
#ifdef MEA_ENV_256PORT	
    MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, (entry->phy_port / 128), MEA_MODULE_BM);
	ind_write.tableOffset = (entry->phy_port< 128) ? (entry->phy_port) : (entry->phy_port - 128);
#else
ind_write.tableOffset	= entry->phy_port;

#endif
    /* build the ind_write value */
	ind_write.tableType		= ENET_BM_TBL_SHAPER_BUCKET_PORT;
	
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)entry;
    /*ind_write.funcParam2 = (MEA_funcParam)entry->shaper_Id;*/
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_EgressPort_UpdateHwEntryShaperCmd;
	
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_BM);
		return MEA_ERROR;
    }

    /* return to caller */   
    return MEA_OK;
}


MEA_Status mea_EgressPort_UpdateHw_protect_1p1(MEA_Port_t port, MEA_EgressPort_Entry_dbt *entry,MEA_EgressPort_Entry_dbt *old_entry)
{

    MEA_Bool isPortSupport_1p1=MEA_TRUE;
    MEA_Uint32 offset4bitPerPort=0;
    MEA_Uint32 portToval=0;
    MEA_Uint32 port_map=0;
    MEA_Uint32 write_valu;

    
    if(!MEA_PROTECT_1PLUS1_SUPPORT){
        return MEA_OK;
    }
    
    if ((MEA_EgressPort_InitDone     ) &&
        (old_entry != NULL) &&
        (MEA_OS_memcmp (&entry->protect_1p1_info,&old_entry->protect_1p1_info,sizeof(entry->protect_1p1_info) ) == 0)     ) 
    {
            return MEA_OK;
    }
    
   

    switch (port)
    {
        case 0  : port_map=offset4bitPerPort=0 ;   break;
        case 12 : port_map=offset4bitPerPort=1 ;  break;
        case 24 : port_map=offset4bitPerPort=2 ;  break;
        case 36 : port_map=offset4bitPerPort=3 ;  break;
        case 48 : port_map=offset4bitPerPort=4 ;  break;
        case 72 : port_map=offset4bitPerPort=5 ;  break;
        case 100: port_map=offset4bitPerPort=6 ;  break;
        case 101: port_map=offset4bitPerPort=7 ;  break;
        case 102: port_map=offset4bitPerPort=8 ;  break;
        case 103: port_map=offset4bitPerPort=9 ;  break;
        case 104: port_map=offset4bitPerPort=10;  break;
        case 105: port_map=offset4bitPerPort=11;  break;
        case 108: port_map=offset4bitPerPort=12;  break;
        case 109: port_map=offset4bitPerPort=13;  break;
        case 110: port_map=offset4bitPerPort=14;  break;
        case 111: port_map=offset4bitPerPort=15;  break;

        default:
            isPortSupport_1p1=MEA_FALSE;
            
        break;

   }
    
    if(isPortSupport_1p1==MEA_TRUE){


        switch (entry->protect_1p1_info.outPortId_1p1)
        {
        case 0  : portToval=0 ;  break;
        case 12 : portToval=1 ;  break;
        case 24 : portToval=2 ;  break;
        case 36 : portToval=3 ;  break;
        case 48 : portToval=4 ;  break;
        case 72 : portToval=5 ;  break;
        case 100: portToval=6 ;  break;
        case 101: portToval=7 ;  break;
        case 102: portToval=8 ;  break;
        case 103: portToval=9 ;  break;
        case 104: portToval=10;  break;
        case 105: portToval=11;  break;
        case 108: portToval=12;  break;
        case 109: portToval=13;  break;
        case 110: portToval=14;  break;
        case 111: portToval=15;  break;
        
        }
   
     if(entry->protect_1p1_info.enable_1P1 == MEA_FALSE){
       portToval=0;
     }
     
     MEA_OS_insert_value((4*offset4bitPerPort), 4,(MEA_Uint32)portToval,&MEA_EgressPort_Protect_1p1_reg[0]);

    if(portToval<=7)    
       MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_EG_1P1_P0_REG, MEA_EgressPort_Protect_1p1_reg[0], MEA_MODULE_IF);
    else
       MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_EG_1P1_P1_REG, MEA_EgressPort_Protect_1p1_reg[1], MEA_MODULE_IF);


    // enable port
   if(entry->protect_1p1_info.enable_1P1 == MEA_TRUE){
          MEA_EgressPort_Protect_1p1_Enable[(port_map)/32] |= ( (0x00000001 << ((port_map)%32)));
   }else{
        MEA_EgressPort_Protect_1p1_Enable[(port_map)/32] &= (~(0x00000001 << ((port_map)%32)));
   }

   

    write_valu=0;
    write_valu   = mea_global_1p1_down_up_count_And_ports_enable & 0xffff0000;
    write_valu |= MEA_EgressPort_Protect_1p1_Enable[0];

    MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_EG_1P1_ENABLE_REG, write_valu, MEA_MODULE_IF);
    
    mea_global_1p1_down_up_count_And_ports_enable = write_valu;
    

    }

 
   
   
    
    
    
    return MEA_OK;
}


MEA_Status mea_EgressPort_UpdateHw_g9991_Fragmant_scheduler(MEA_Port_t port, MEA_EgressPort_Entry_dbt *entry,MEA_EgressPort_Entry_dbt *old_entry)
{

    if(!MEA_FRAGMENT_9991_SUPPORT)
        return MEA_OK;


    
    
    if ((MEA_EgressPort_InitDone     ) &&
        (old_entry != NULL) &&
        (entry->g9991_frg_scheduler == old_entry->g9991_frg_scheduler)     ) 
    {
        return MEA_OK;
    }

    if(port >= 63)  // we support only the 0:63
      return MEA_OK;

 

    // enable port
    if(entry->g9991_frg_scheduler == MEA_TRUE){
        MEA_EgressPort_Table_Frag_Sch[(port)/32] |= ( (0x00000001 << ((port)%32)));
    }else{
        MEA_EgressPort_Table_Frag_Sch[(port)/32] &= (~(0x00000001 << ((port)%32)));
    }


    MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_FRAG_SCH_PORT31_0_REG + (((port)/32) *4), MEA_EgressPort_Table_Frag_Sch[(port)/32], MEA_MODULE_BM);



    return MEA_OK;
}






/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_UpdateHw>                                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_EgressPort_UpdateHw(MEA_Port_t                port,
                                          MEA_EgressPort_Entry_dbt *entry,
                                          MEA_EgressPort_Entry_dbt *old_entry)
{

MEA_PortsMapping_t  portMap;

if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
    MEA_PORTS_TYPE_PHYISCAL_TYPE,
    port,
    &portMap)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n",port);

        return ENET_ERROR;
}

    /* Update HW Egress Port Tx Enable parameter */
    if (mea_EgressPort_UpdateHwTxEnable(port,entry,old_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_EgressPort_UpdateHwTxEnable for port %d failed\n",
                          __FUNCTION__,port);
		return MEA_ERROR;
    }


    /* Update HW Egress Port Calc Crc parameter */
    if (mea_EgressPort_UpdateHwCalcCrc(port,entry,old_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_EgressPort_UpdateHwCalcCrc for port %d failed\n",
                          __FUNCTION__,port);
		return MEA_ERROR;
    }




    /* Update HW Egress Port Configuration parameters */
    if (mea_EgressPort_UpdateHwCfg(port,entry,old_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_EgressPort_UpdateHwCfg for port %d failed\n",
                          __FUNCTION__,port);
		return MEA_ERROR;
    }

    /* Update HW Egress Port Protect parameter */
    if (mea_EgressPort_UpdateHwPortProtect(port,entry,old_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_EgressPort_UpdateHwPortProtect for port %d failed\n",
                          __FUNCTION__,port);
		return MEA_ERROR;
    }



    /* Update HW Egress Port MAC  */
    if (mea_EgressPort_UpdateHwMAC(port,entry,old_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_EgressPort_UpdateHwMAC for port %d failed\n",
            __FUNCTION__,port);
        return MEA_ERROR;
    }

    /* Update Shaper Egress Port info */
    {
        if((
            ((portMap.portType !=  MEA_PORTTYPE_G999_1MAC) &&
            (portMap.portType !=  MEA_PORTTYPE_G999_1MAC_1588)) ||

           ( (MEA_STANDART_BONDING_SUPPORT == MEA_TRUE) && 
            ((portMap.portType == MEA_PORTTYPE_G999_1MAC ||
             portMap.portType == MEA_PORTTYPE_G999_1MAC_1588)))
            
            
            )
            )
        {
            MEA_db_HwUnit_t hwUnit;
		    mea_memory_mode_e save_globalMemoryMode;
            if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
                hwUnit = MEA_HW_UNIT_ID_GENERAL;
            } 
		    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
            if ((MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PORT_TYPE) == MEA_SHAPER_SUPPORT_PORT_TYPE) &&
	            (mea_EgressPort_UpdateHwShaperCmd(entry,old_entry) != MEA_OK))
	        {
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			    			      "%s - mea_EgressPort_UpdateHwShaperCmd failed\n",
				    		      __FUNCTION__);
			    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
	        }
		    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        }
    }


   

    //protect_1p1
    
    if (mea_EgressPort_UpdateHw_protect_1p1(port,entry,old_entry) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_EgressPort_UpdateHw_protect_1p1 failed\n",
                __FUNCTION__);
            return MEA_ERROR;
    }

   if (mea_EgressPort_UpdateHw_g9991_Fragmant_scheduler(port,entry,old_entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_EgressPort_UpdateHw_Fragmant_scheduler failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }


   
       if (mea_EgressPort_Class_Rule(port,entry,old_entry) != MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s - mea_EgressPort_Class_Rule failed\n",
               __FUNCTION__);
           return MEA_ERROR;
       }




    /* return to caller */
	return MEA_OK;


}

/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_EgressPort_Table>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_EgressPort_Table(MEA_Unit_t unit_i)
{

    MEA_Port_t                   port;
    MEA_PortsMapping_t           portMap;
    MEA_EgressPort_Entry_dbt     entry;
    MEA_EgressPort_InitDone = MEA_FALSE;
    




    /* zeros the entry */
	MEA_OS_memset(&entry,0,sizeof(entry));
    
    MEA_OS_memset(&MEA_EgressPort_Protect_1p1_reg,0,sizeof(MEA_EgressPort_Protect_1p1_reg));

    MEA_EgressPort_Protect_1p1_Enable[0]=0;



	/* Reset the MEA_EgressPort_Table to zeros */
	MEA_OS_memset(&(MEA_EgressPort_Table[0]),0,sizeof(MEA_EgressPort_Table));
    MEA_OS_memset(&(MEA_EgressPort_Table_tx_enable),0,sizeof(MEA_EgressPort_Table_tx_enable));

    MEA_OS_memset(&(MEA_EgressPort_Table_halt     ),0,sizeof(MEA_EgressPort_Table_halt));
if(!MEA_STANDART_BONDING_SUPPORT){
    MEA_OS_memset(&(MEA_EgressPort_Table_flush    ),0xff,sizeof(MEA_EgressPort_Table_flush));
}else{
 MEA_OS_memset(&(MEA_EgressPort_Table_flush    ),0x00,sizeof(MEA_EgressPort_Table_flush));
}
    MEA_OS_memset(&(MEA_EgressPort_Table_calc_crc),0,sizeof(MEA_EgressPort_Table_calc_crc));
    MEA_OS_memset(&MEA_EgressPort_MacAddrTable,0,sizeof(MEA_EgressPort_MacAddrTable));
    /* Init Each Entry to defaults */
	for(port=0;port<=MEA_MAX_PORT_NUMBER;port++){

        /* Check if this port is valid port */
		if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
           continue;
        }
        MEA_OS_memset(&portMap,0,sizeof(portMap));
        if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
            MEA_PORTS_TYPE_EGRESS_TYPE,
            (MEA_Uint16)port,
            &portMap)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n",port);
        }


        /* set default value for egress port halt / flush  */
        entry.halt      = MEA_EGRESS_PORT_HALT_DEF_VAL(port);
        entry.flush     = MEA_EGRESS_PORT_FLUSH_DEF_VAL(port);

        /* set default value for egress port MTU */
        entry.MTU       = MEA_EGRESS_PORT_MTU_DEF_VAL(port);        

        /* set default value for egress port tx_enable */
        entry.tx_enable = MEA_EGRESS_PORT_TX_ENABLE_DEF_VAL(port);

        /* set default value for egress port calc_crc */
        entry.calc_crc = MEA_EGRESS_PORT_CALC_CRC_DEF_VAL(port);

          /* set default value for egress port phy_port */
        entry.phy_port  = MEA_EGRESS_PORT_PHY_PORT_DEF_VAL(port);

        if(portMap.portType == MEA_PORTTYPE_AAL5_ATM_TC){
            entry.proto    = MEA_EGRESS_PORT_PROTO_EoLLCoATM;
        }else {
        /* set default value for egress port protocol */
        entry.proto    = MEA_EGRESS_PORT_PROTO_DEF_VAL(port);
        }
        entry.shaper_enable =MEA_FALSE;
        entry.Shaper_compensation=ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
        MEA_OS_memset(&entry.shaper_info,0,sizeof(entry.shaper_info));
        
       
        MEA_EgressPort_MacAddrTable[port].b[5]      = (MEA_Uint8)port;
        MEA_OS_maccpy(&(entry.My_Mac),&(MEA_EgressPort_MacAddrTable[port]));
        
        if(mea_drv_Get_DeviceInfo_ingress_extended_mac_fifo(unit_i,port)== MEA_TRUE){
            if(MEA_DRV_BM_BUS_WIDTH == 0){//64bit
                if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                    entry.pause_send.AF_threshold =  232;
                }else{
                    entry.pause_send.AF_threshold =  1000;
                }
            }
            if(MEA_DRV_BM_BUS_WIDTH == 1 || MEA_DRV_BM_BUS_WIDTH == 2){//128bit
                if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                    entry.pause_send.AF_threshold =  108;
                }else{
                    entry.pause_send.AF_threshold =  492;
                }
            }
            
            
        }else {
            if(MEA_DRV_BM_BUS_WIDTH == 0){//64bit
                if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                    entry.pause_send.AF_threshold =  40;
                }else{
                    entry.pause_send.AF_threshold =  232;
                }
            }
            if(MEA_DRV_BM_BUS_WIDTH == 1 || MEA_DRV_BM_BUS_WIDTH == 2){//128bit
                if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                    entry.pause_send.AF_threshold =  0x31 ; //49; //44
                }else{
                    entry.pause_send.AF_threshold =  236;
                }
            }
            
        }
        
        entry.pause_send.Transmit_threshold= 0x30;//49;

        /* Set the Egress Port Entry */
 		if(MEA_API_Set_EgressPort_Entry(unit_i,
                                        port,      
                                        &entry) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_SetEgressPortProtocolType(%d,...) failed\n",
                              __FUNCTION__,port);
			return MEA_ERROR; 
		}
		
	}

    MEA_EgressPort_InitDone = MEA_TRUE;
#if MEA_PLAT_S1_VDSL_K7
    
    for(port=0;port<=MEA_MAX_PORT_NUMBER;port++){

        /* Check if this port is valid port */
        if (MEA_API_Get_IsPortValid(port,
            MEA_TRUE /* silent */
            ) == MEA_FALSE) {
            continue;
        }

        switch (port){
        case 96:
        case 97:
        case 98:
        case 99:
        case 106:
        case 107:
        case 112:
        case 113:
        case 114:
        case 115:
        case 116:
        case 117:
        case 118:
        case 119:
        case 121:
        case 124:
        case 125:
        case 126:

            entry.calc_crc = MEA_FALSE;
        break;
        default:
            continue;
            break;
        }
        entry.calc_crc = MEA_FALSE;

        /* Set the Egress Port Entry */
        if(MEA_API_Set_EgressPort_Entry(unit_i,
            port,      
            &entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_SetEgressPortProtocolType(%d,...) failed\n",
                __FUNCTION__, port);
            return MEA_ERROR;
        }

    }

#endif


    /* return to caller */
	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_EgressPort_Table>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_EgressPort_Table(MEA_Unit_t unit_i)
{

    MEA_Port_t                   port;
    MEA_Bool                     save_InitDone;





    /* Reset the InitDone to force hw update */
    save_InitDone = MEA_EgressPort_InitDone;
    MEA_EgressPort_InitDone = MEA_FALSE;

    /* Save the current tx enable */
    MEA_OS_memcpy(&MEA_EgressPort_Table_tx_enable_save_for_ReInit,
                  &MEA_EgressPort_Table_tx_enable,
                  sizeof(MEA_EgressPort_Table_tx_enable_save_for_ReInit));
    
    MEA_OS_memcpy(&MEA_EgressPort_Table_flush_save_for_ReInit,
        &MEA_EgressPort_Table_flush,
        sizeof(MEA_EgressPort_Table_flush_save_for_ReInit));

    MEA_OS_memcpy(&MEA_EgressPort_Table_pause_enable_save_for_ReInit,
        &MEA_EgressPort_Table_pause_enable,
        sizeof(MEA_EgressPort_Table_pause_enable_save_for_ReInit));

    
    

    MEA_OS_memset(&MEA_EgressPort_Table_tx_enable,
                  0,
                  sizeof(MEA_EgressPort_Table_tx_enable));
    
    MEA_OS_memset(&MEA_EgressPort_Table_flush,
        0xff,
        sizeof(MEA_EgressPort_Table_flush));

    /* ReInit all ports */
	for(port=0;port<=MEA_MAX_PORT_NUMBER;port++){

        /* Check if this port is valid port */
		if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
           continue;
        }
 
        /* Update the Hardware */
        MEA_EgressPort_Table[mea_get_port_mapArray(port)].tx_enable = MEA_FALSE;
        MEA_EgressPort_Table[mea_get_port_mapArray(port)].flush     = MEA_TRUE;
        if (mea_EgressPort_UpdateHw(port,
                                    &(MEA_EgressPort_Table[mea_get_port_mapArray(port)]),
                                    NULL /* old entry - force hwUpdate */
                                    ) != MEA_OK) {
    		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Egress Port Update Hw for port %d failed \n",
                              __FUNCTION__,port);
            MEA_EgressPort_InitDone = save_InitDone;
            return MEA_ERROR;
	    }
    }


    /* Restore the InitDone */
    MEA_EgressPort_InitDone = save_InitDone;


    /* return to caller */
	return MEA_OK;

}

MEA_Status mea_drv_ReInit_TxDisable(MEA_Unit_t unit)
{
    

    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
    MEA_Internal_Block_256OutPorts_dbt BlockOutPortFlUSH;

//    MEA_Uint32  i = 0;

    /* Restore the tx enable */


    if(b_bist_test){
        return MEA_OK;
    }

    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
        MEA_OS_memset(&BlockOutPortFlUSH, 0xff, sizeof(BlockOutPortFlUSH));

        MEA_Configure_BlockOF256PORTS_TABLE100(unit, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_0_255);
        MEA_Configure_BlockOF256PORTS_TABLE100(unit, &BlockOutPortFlUSH, ENET_REGISTER_TBL100_FLUSH_0_255);

        if (MEA_MAX_PORT_NUMBER >= 256){
            MEA_Configure_BlockOF256PORTS_TABLE100(unit, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_256_511);
            MEA_Configure_BlockOF256PORTS_TABLE100(unit, &BlockOutPortFlUSH, ENET_REGISTER_TBL100_FLUSH_256_511);
        }


    }else {
        return MEA_ERROR;

    }
    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_TxEnable>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_TxEnable(MEA_Unit_t unit_i)
{



    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
    MEA_Port_t  port;


if(b_bist_test)
        return MEA_OK;

    /* Restore the tx enable */
    MEA_OS_memcpy(&MEA_EgressPort_Table_tx_enable,
                  &MEA_EgressPort_Table_tx_enable_save_for_ReInit,
                  sizeof(MEA_EgressPort_Table_tx_enable));
    
    MEA_OS_memcpy(&MEA_EgressPort_Table_flush,
        &MEA_EgressPort_Table_flush_save_for_ReInit,
        sizeof(MEA_EgressPort_Table_flush));

    MEA_OS_memcpy(&MEA_EgressPort_Table_pause_enable,
        &MEA_EgressPort_Table_pause_enable_save_for_ReInit,
        sizeof(MEA_EgressPort_Table_pause_enable));


    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {

        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

        MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_0_31, sizeof(BlockOutPort));
        MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_0_255);
        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
        MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_flush.out_ports_0_31, sizeof(BlockOutPort));
        MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_FLUSH_0_255);

        if (MEA_MAX_PORT_NUMBER >= 256){
#ifdef MEA_OUT_PORT_256_511
            MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_256_287, sizeof(BlockOutPort));
            MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_256_511);
            MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_flush.out_ports_256_287, sizeof(BlockOutPort));
            MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_FLUSH_256_511);
#endif        
        }



    }
    else {

        return MEA_ERROR;

    }
    /* Update the software shadow */
    for (port=0;port<MEA_NUM_OF_ELEMENTS(MEA_EgressPort_Table);port++) {

	    //if (MEA_API_Get_IsPortValid(port,MEA_TRUE)==MEA_FALSE) { 
        //    continue;
        //}
        
        if (MEA_IS_SET_OUTPORT(&MEA_EgressPort_Table_tx_enable,port)) {
            MEA_EgressPort_Table[mea_get_port_mapArray(port)].tx_enable = MEA_TRUE;
            MEA_EgressPort_Table[mea_get_port_mapArray(port)].flush = MEA_FALSE;
        }
        if (MEA_IS_SET_OUTPORT(&MEA_EgressPort_Table_flush,port)) {
            MEA_EgressPort_Table[mea_get_port_mapArray(port)].flush = MEA_TRUE;
        }


    }

    /* Return to caller */
    return MEA_OK; 

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_EgressPort_phyPort>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_EgressPort_phyPort(MEA_Unit_t                unit,
                                          MEA_Port_t                port,
                                          MEA_Port_t                phy_port) 
{


	MEA_API_LOG 

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (port!= 125) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Currently the port must be 125 \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if ((phy_port != 125) && (phy_port != 126)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Currently the phy_port must be 125 or 126 \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Update Hardware */
    if (mea_drv_global_set_if_protection_bit ((phy_port != 125)) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_global_set_if_protection_bit failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    /* Update software shadow */
    MEA_EgressPort_Table[mea_get_port_mapArray(port)].phy_port = phy_port;

    /* Return to caller */
    return MEA_OK;

}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_EgressPort_Entry>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_EgressPort_Entry(MEA_Unit_t                unit,
                                        MEA_Port_t                port,
                                        MEA_EgressPort_Entry_dbt* entry) 
{
   
    ENET_Shaper_Profile_dbt      shaperDbt;
    ENET_Shaper_Profile_key_dbt  shaperKeyDbt;
    MEA_PortsMapping_t  portMap;
    MEA_Bool AdminChange=MEA_FALSE;



	MEA_API_LOG 

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check for valid port parameter */
	if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE) ==MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - port %d is invalid\n",
                         __FUNCTION__,(int)port);
       return MEA_ERROR; 
    }


    /* Check for valid entry parameter */
	if (entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry == NULL (port=%d)\n",
                         __FUNCTION__,port);
       return MEA_ERROR; 
    }
	
    /* Check for valid entry->phy_port parameter */
	if (MEA_API_Get_IsPortValid((MEA_Port_t)(entry->phy_port), MEA_PORTS_TYPE_EGRESS_TYPE,
		                         MEA_FALSE)==MEA_FALSE) { 
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - phy_port %d is invalid (port=%d)\n",
                         __FUNCTION__,entry->phy_port,port);
       return MEA_ERROR; 
    }


    /* Check for valid halt parameter */
    if ((entry->halt != MEA_FALSE) && (entry->halt != MEA_TRUE)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - halt %d is invalid (port=%d)\n",
                         __FUNCTION__,entry->halt,port);
       return MEA_ERROR;
    }
#if 1
	if (entry->halt != MEA_EgressPort_Table[mea_get_port_mapArray(port)].halt) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Change halt is not support currently by the device  \n",
                         __FUNCTION__);
       return MEA_ERROR;
    	}
#endif

    /* Check for valid flush parameter */
    if ((entry->flush != MEA_FALSE) && (entry->flush != MEA_TRUE)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - flash %d is invalid (port=%d)\n",
                         __FUNCTION__,entry->flush,port);
       return MEA_ERROR;
    }

#if 0
    /* Check for valid MTU parameter */
    if (entry->MTU > MEA_EGRESS_PORT_MTU_MAX_VALUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MTU 0x%08x is invalid (port=%d)\n",
                         __FUNCTION__,(int)entry->MTU,port);
       return MEA_ERROR;
    }


    if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
                                          port,
                                          &IngressPort_Entry) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetIngressPort_Entry failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
    }
#endif
    if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
        MEA_PORTS_TYPE_PHYISCAL_TYPE,
        port,
        &portMap)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n",port);
            
            return ENET_ERROR;
    }



    /* Check for valid proto parameter */
	if(entry->proto >= MEA_EGRESS_PORT_PROTO_LAST) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid protocol %d (port=%d)\n",
                          __FUNCTION__,entry->proto,port);
		return MEA_ERROR;
	}

#if 1 /* the utopia is marked in the physical and not in the logical port */
    if (entry->proto == MEA_EGRESS_PORT_PROTO_EoLLCoATM       || 
        entry->proto == MEA_EGRESS_PORT_PROTO_IPoEoLLCoATM    || 
        entry->proto == MEA_EGRESS_PORT_PROTO_PPPoEoLLCoATM   ||
        entry->proto == MEA_EGRESS_PORT_PROTO_PPPoEoVcMUXoATM   ||
        entry->proto == MEA_EGRESS_PORT_PROTO_EoVcMUXoATM     ||
        entry->proto == MEA_EGRESS_PORT_PROTO_IPoLLCoATM      ||
        entry->proto == MEA_EGRESS_PORT_PROTO_IPoVcMUXoATM    ||
        entry->proto == MEA_EGRESS_PORT_PROTO_PPPoVcMUXoATM   ||
        entry->proto == MEA_EGRESS_PORT_PROTO_PPPoLLCoATM    )
	{
        if(portMap.portType !=  MEA_PORTTYPE_AAL5_ATM_TC){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Invalid protocol %d for non ATM port (port=%d)\n",
                __FUNCTION__,entry->proto,port);
            return MEA_ERROR; 
        }
        
        
    } 

#endif

   


    /* Check for valid MQS_vec parameter */
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /*if the port is G999 the shaper not allowed*/
    if((portMap.portType ==  MEA_PORTTYPE_G999_1MAC ||
        portMap.portType ==  MEA_PORTTYPE_G999_1MAC_1588)  &&  
        ((entry->shaper_enable == ENET_TRUE) &&
        (MEA_STANDART_BONDING_SUPPORT    == MEA_FALSE))
        
        )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - shaper on g999 port not allowed used interface shaper \n",
            __FUNCTION__,port);
        return MEA_ERROR; 
    }
    
    
    /* Create new shaper profile */
	if (entry->shaper_enable == ENET_TRUE) { //enable
        MEA_db_HwUnit_t hwUnit;
		mea_memory_mode_e save_globalMemoryMode;
        if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
            hwUnit = MEA_HW_UNIT_ID_GENERAL;
        } 
		MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PORT_TYPE) != MEA_SHAPER_SUPPORT_PORT_TYPE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - The shaper is not available for \" port mode \" ",
							  __FUNCTION__,port);
			MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
        }
		MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        MEA_OS_memset(&shaperDbt,0,sizeof(shaperDbt));
		shaperDbt.CIR  = entry->shaper_info.CIR;
		shaperDbt.CBS  = entry->shaper_info.CBS;
		shaperDbt.type = entry->shaper_info.resolution_type;
		shaperDbt.overhead = entry->shaper_info.overhead;
		shaperDbt.Cell_Overhead = entry->shaper_info.Cell_Overhead;
        shaperDbt.mode = entry->shaper_info.mode;
		MEA_OS_strcpy(shaperDbt.name,"");
        MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
        shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PORT_TYPE;
        shaperKeyDbt.acm_mode    = 0;
        shaperKeyDbt.id          = MEA_PLAT_GENERATE_NEW_ID;
		if (enet_AddOwner_Shaper_Profile(unit,&shaperKeyDbt,&shaperDbt) == ENET_ERROR) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - can not create shaper profile for port %d\n",
							  __FUNCTION__,port);
			return MEA_ERROR;
		}
		entry->shaper_Id = shaperKeyDbt.id;
	} else  // disable
		entry->shaper_enable = ENET_FALSE;

    if(port >=64 && entry->g9991_frg_scheduler == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - frg_scheduler not support this port %d\n",
            __FUNCTION__,port);
        return MEA_ERROR;
    }
    






    /* Update the Hardware */
    if (mea_EgressPort_UpdateHw(port,
                                entry,
                                &(MEA_EgressPort_Table[mea_get_port_mapArray(port)])
                               ) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Egress Port Update Hw for port %d failed \n",
                          __FUNCTION__,port);
		return MEA_ERROR;
	}

    

    /* delete old shaper profile */
    if (MEA_EgressPort_Table[mea_get_port_mapArray(port)].shaper_enable == ENET_TRUE) { //enable
        MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
        shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PORT_TYPE;
        shaperKeyDbt.acm_mode    = 0;
        shaperKeyDbt.id          = MEA_EgressPort_Table[mea_get_port_mapArray(port)].shaper_Id;
	    if (enet_DelOwner_Shaper_Profile(unit,&shaperKeyDbt) == ENET_ERROR) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						      "%s - can not delete shaper profile for port %d\n",
						      __FUNCTION__,port);
		    return MEA_ERROR;
	    }

    }

    if(entry->tx_enable != MEA_EgressPort_Table[mea_get_port_mapArray(port)].tx_enable){
        AdminChange=MEA_TRUE;
    }



    /* Update the Software shadow DB */
    MEA_OS_memcpy(&(MEA_EgressPort_Table[mea_get_port_mapArray(port)]),
        entry,
        sizeof(MEA_EgressPort_Table[0]));

    MEA_OS_maccpy(&(MEA_EgressPort_Table[mea_get_port_mapArray(port)].My_Mac),&(entry->My_Mac)) ;
    
	if(AdminChange){
    /* Update dependency */
		if (mea_EgressPort_Update_Dependency(unit,
										 port,
											 entry) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_EgressPort_Update_Dependency failed (port=%d)\n",
								  __FUNCTION__,port);
				return MEA_ERROR;
		}
	}

	
    
	return MEA_OK;
}




MEA_Status MEA_API_Set_EgressPort_TxEnable(MEA_Unit_t     unit,  MEA_Port_t   port,MEA_Bool   tx_enable)
{
    MEA_Uint32 *pPortGrp;
    MEA_Uint32 NumOFShift;

    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
    MEA_Uint32 offset;
    MEA_TxEnable_stateWrite =MEA_TRUE;



    pPortGrp=(MEA_Uint32 *)(&(MEA_EgressPort_Table_tx_enable.out_ports_0_31));
    pPortGrp += (port/32);



	NumOFShift = (port % 32);
    (*pPortGrp) &= ~(0x0000001       <<(NumOFShift));
    (*pPortGrp) |=  (tx_enable<<(NumOFShift));


    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

        /*Write to TBL 100 RX */
        if (port < 256){
            offset = ENET_REGISTER_TBL100_Admin_TX_MAC_0_255;
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_0_31, sizeof(BlockOutPort));

        }
        if (port >= 256 && port < 512){
#ifdef MEA_OUT_PORT_256_511
            offset = ENET_REGISTER_TBL100_Admin_TX_MAC_256_511;
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_256_287, sizeof(BlockOutPort));
#endif
        }

        /*config ENET_REGISTER_TBL100_Admin_RX */
        MEA_Configure_BlockOF256PORTS_TABLE100(unit, &BlockOutPort, offset);

    } else {
        return MEA_ERROR;

    }


    MEA_TxEnable_stateWrite =MEA_FALSE;



    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_EgressPort_Update_Dependency>                             */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status mea_EgressPort_Update_Dependency(MEA_Unit_t                unit,
                                 MEA_Port_t                port,
                                 MEA_EgressPort_Entry_dbt* entry)
{
	return MEA_Queue_port_status_changed(port,isPortEnable(entry));
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_EgressPort_Entry>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_EgressPort_Entry(MEA_Unit_t                unit,
                                        MEA_Port_t                port,
                                        MEA_EgressPort_Entry_dbt* entry) 
{

	MEA_API_LOG 

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid port parameter */
	if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - port %d is invalid\n",
                         __FUNCTION__,port);
       return MEA_ERROR; 
    }

    /* Check for valid entry parameter */
    if (entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry==NULL (port=%d)\n",
                         __FUNCTION__,port);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* update output parameter */
    MEA_OS_memcpy(entry,
                  &(MEA_EgressPort_Table[mea_get_port_mapArray(port)]),
                  sizeof(*entry));

    /* return to caller */  
	return MEA_OK;
}


MEA_Bool MEA_API_Set_EgressPort_Is_AdminUp(MEA_Unit_t unit, MEA_Port_t port)
{
    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        return MEA_FALSE; 
    }
    
    return MEA_EgressPort_Table[mea_get_port_mapArray(port)].tx_enable;

}



MEA_Status MEA_API_PortEgress_Event_AdminStatus(MEA_Unit_t               unit_i,
                                                MEA_OutPorts_Entry_dbt  *eventPorts,  //1-port have event
                                                MEA_Bool                *exist)
{

    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
    MEA_OutPorts_Entry_dbt   entryOut;
   
 //   MEA_Uint32 read_reg[4];

    if(eventPorts == NULL){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - eventPorts = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }
    if(exist == NULL){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - exist = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

    MEA_OS_memset(eventPorts,0,sizeof(*eventPorts));
    *exist=MEA_FALSE;

    if( MEA_reinit_start){//Reinit is On 
        //MEA_OS_LOG_logMsg (MEA_OS_LOG_EVENT," user start to change the port Admin/or reinit start\n");
        return MEA_OK;
    }


    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        /*Read to TBL 100 TX */
        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

        MEA_Get_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_0_255);
        //MEA_OS_memcpy(&eventPorts->out_ports_0_31, &BlockOutPort, sizeof(BlockOutPort));
        eventPorts->out_ports_0_31 = BlockOutPort.regs[0] ^ MEA_EgressPort_Table_tx_enable.out_ports_0_31;
        eventPorts->out_ports_32_63 = BlockOutPort.regs[1] ^ MEA_EgressPort_Table_tx_enable.out_ports_32_63;
        eventPorts->out_ports_64_95 = BlockOutPort.regs[2] ^ MEA_EgressPort_Table_tx_enable.out_ports_64_95;
        eventPorts->out_ports_96_127 = BlockOutPort.regs[3] ^ MEA_EgressPort_Table_tx_enable.out_ports_96_127;
#ifdef MEA_OUT_PORT_128_255
        eventPorts->out_ports_128_159 = BlockOutPort.regs[4] ^ MEA_EgressPort_Table_tx_enable.out_ports_128_159;
        eventPorts->out_ports_160_191 = BlockOutPort.regs[5] ^ MEA_EgressPort_Table_tx_enable.out_ports_160_191;
        eventPorts->out_ports_192_223 = BlockOutPort.regs[6] ^ MEA_EgressPort_Table_tx_enable.out_ports_192_223;
        eventPorts->out_ports_224_255 = BlockOutPort.regs[7] ^ MEA_EgressPort_Table_tx_enable.out_ports_224_255;
#endif

        if (MEA_MAX_PORT_NUMBER >= 256){
            MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
#ifdef MEA_OUT_PORT_256_511
            MEA_Get_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_256_511);
            eventPorts->out_ports_256_287 = BlockOutPort.regs[0] ^ MEA_EgressPort_Table_tx_enable.out_ports_256_287;
            eventPorts->out_ports_288_319 = BlockOutPort.regs[1] ^ MEA_EgressPort_Table_tx_enable.out_ports_288_319;
            eventPorts->out_ports_320_351 = BlockOutPort.regs[2] ^ MEA_EgressPort_Table_tx_enable.out_ports_320_351;
            eventPorts->out_ports_352_383 = BlockOutPort.regs[3] ^ MEA_EgressPort_Table_tx_enable.out_ports_352_383;
            eventPorts->out_ports_384_415 = BlockOutPort.regs[4] ^ MEA_EgressPort_Table_tx_enable.out_ports_384_415;
            eventPorts->out_ports_416_447 = BlockOutPort.regs[5] ^ MEA_EgressPort_Table_tx_enable.out_ports_416_447;
            eventPorts->out_ports_448_479 = BlockOutPort.regs[6] ^ MEA_EgressPort_Table_tx_enable.out_ports_448_479;
            eventPorts->out_ports_480_511 = BlockOutPort.regs[7] ^ MEA_EgressPort_Table_tx_enable.out_ports_480_511;
#endif
        }

        MEA_OS_memset(&entryOut, 0, sizeof(entryOut));

        if (MEA_OS_memcmp(eventPorts, &entryOut, sizeof(MEA_OutPorts_Entry_dbt)) != 0){
            *exist = MEA_TRUE;
        }




    }
    else {

        return MEA_ERROR;
    }

    return MEA_OK;
}




MEA_Status MEA_API_PortEgress_UpdateAdminUp(MEA_Unit_t               unit_i)
{

    MEA_Bool                exist = MEA_FALSE;
    MEA_OutPorts_Entry_dbt  eventPorts;
    MEA_OutPorts_Entry_dbt     portEntry;

    


    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
    MEA_OutPorts_Entry_dbt    ReadOutPort;
   
    
    
//    MEA_Uint32 i,value;
//    MEA_Uint32 read_value;

//    MEA_Uint32             *pLinkPortGrp;
//    MEA_Uint32             *pPortGrpEgress;

    return MEA_OK; /*NOT Support On HW*/

    if(MEA_TDM_SUPPORT)
        return MEA_OK;

    if(MEA_API_PortEgress_Event_AdminStatus(unit_i ,&eventPorts,&exist)!=MEA_OK){
        return MEA_ERROR;
    }

    if(!exist){
        return MEA_OK;
    }


    if(MEA_API_port_Get_Link_protection_Status(unit_i,&portEntry) !=MEA_OK){
        return MEA_ERROR;
    }

    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        /*Read to TBL 100 TX */
        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
        MEA_OS_memset(&ReadOutPort, 0, sizeof(ReadOutPort));


        MEA_Get_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_0_255);
        MEA_OS_memcpy(&ReadOutPort.out_ports_0_31, &BlockOutPort, sizeof(BlockOutPort));

        if (MEA_OS_memcmp(&ReadOutPort.out_ports_0_31, &MEA_EgressPort_Table_tx_enable.out_ports_0_31, sizeof(MEA_Internal_Block_256OutPorts_dbt)) != 0){
            MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_0_31, sizeof(BlockOutPort));
            /*config ENET_REGISTER_TBL100_Admin_TX */
            MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_0_255);
        }


        if (MEA_MAX_PORT_NUMBER >= 256){
            MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
#ifdef MEA_OUT_PORT_256_511
            MEA_Get_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_256_511);
            MEA_OS_memcpy(&ReadOutPort.out_ports_256_287, &BlockOutPort, sizeof(BlockOutPort));
            if (MEA_OS_memcmp(&ReadOutPort.out_ports_256_287, &MEA_EgressPort_Table_tx_enable.out_ports_256_287, sizeof(MEA_Internal_Block_256OutPorts_dbt)) != 0){
                MEA_OS_memcpy(&BlockOutPort, &MEA_EgressPort_Table_tx_enable.out_ports_256_287, sizeof(BlockOutPort));
                /*config ENET_REGISTER_TBL100_Admin_TX */
                MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_TX_MAC_256_511);
            }
#endif
        }







    }
    else {

        return MEA_ERROR;

    }
    return MEA_OK;

}


/************************************************************************/
/*  EGRESS  ACL                                                         */
/************************************************************************/




MEA_Status MEA_API_EgressACL_Create(MEA_Unit_t               unit_i,
                                    MEA_EgressACL_Entry_dbt  *entry,
                                    MEA_Uint16                *indexId_o)
{



 return MEA_OK;
}


MEA_Status MEA_API_EgressACL_Set(MEA_Unit_t                unit_i,
                                 MEA_Uint16                indexId_i,
                                 MEA_EgressACL_Entry_dbt   *entry)
{

    return MEA_OK;
}

MEA_Status MEA_API_EgressACL_Delete(MEA_Unit_t               unit_i,
                                    MEA_Uint16                indexId_i)
{
    
    return MEA_OK;
}

MEA_Status MEA_API_EgressACL_Get(MEA_Unit_t               unit_i,
                                 MEA_Uint16                indexId_i,
                                 MEA_EgressACL_Entry_dbt  *entry)
{

    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

void mea_global_Egress_sa_mac_UpdateHwEntryMAC(MEA_Uint32 arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    MEA_Unit_t                  unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t             hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t               hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_global_Egress_sa_mac_dbt*  entry = (MEA_global_Egress_sa_mac_dbt*)arg4;
    MEA_Uint32                  val[2];
    MEA_Uint32                  i;
    //MEA_Uint32                  count_shift;

    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }


    val[0] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[2]))) << 24);
    val[0] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[3]))) << 16);
    val[0] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[4]))) << 8);
    val[0] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[5]))) << 0);

    val[1] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[0]))) << 8);
    val[1] |= (((MEA_Uint32)((unsigned char)(entry->My_Mac.b[1]))) << 0);


    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }

}

static MEA_Status mea_global_Egress_sa_mac_UpdateHwMAC(MEA_Unit_t     unit ,
                                                       MEA_Uint16     index,
                                                       MEA_global_Egress_sa_mac_dbt *entry)
{

    MEA_ind_write_t             ind_write;

    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYPE_EGRESS_MAC_MASK;
    ind_write.tableOffset = index;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;

    ind_write.writeEntry = (MEA_FUNCPTR)mea_global_Egress_sa_mac_UpdateHwEntryMAC;
    ind_write.funcParam1 = (MEA_funcParam)(long)unit;
    ind_write.funcParam2 = (MEA_funcParam)(long)0;
    ind_write.funcParam3 = (MEA_funcParam)(long)0;
    ind_write.funcParam4 = (MEA_funcParam)entry;
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            MEA_MODULE_IF);
        return MEA_ERROR;
    }

    /* return to caller */
    return MEA_OK;

}

/************************************************************************/
/*  init/reinit                                                         */
/************************************************************************/
MEA_Status mea_drv_Global_Egress_SA_MAC_Init(MEA_Unit_t unit)
{

    MEA_Uint16 index;

    mea_Global_SA_MASK_BY_Interface = 0;

    MEA_OS_memset(&MEA_Global_MASK_EGRESS_MAC[0], 0, sizeof(MEA_Global_MASK_EGRESS_MAC));

    for (index = 0; index < MEA_MAX_GOLBL_MASK_EGRESS_MAC; index++)
    {
        if (mea_global_Egress_sa_mac_UpdateHwMAC(unit, index, &(MEA_Global_MASK_EGRESS_MAC[index])) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_global_Egress_sa_mac_UpdateHwMAC fail \n",
                __FUNCTION__);
            return MEA_ERROR;

        }
    }


    MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_INTERFACE_SA_MASK_BRAS, mea_Global_SA_MASK_BY_Interface, MEA_MODULE_IF);

    



    return MEA_OK;
}

MEA_Status mea_drv_Global_Egress_SA_MAC_ReInit(MEA_Unit_t unit)
{
    MEA_Uint16 index;

    for (index = 0; index < MEA_MAX_GOLBL_MASK_EGRESS_MAC;index++)
    {
        if (mea_global_Egress_sa_mac_UpdateHwMAC(unit, index, &(MEA_Global_MASK_EGRESS_MAC[index]) )!= MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_global_Egress_sa_mac_UpdateHwMAC fail \n",
                __FUNCTION__);
            return MEA_ERROR;

        }
    }
    
    MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_INTERFACE_SA_MASK_BRAS, mea_Global_SA_MASK_BY_Interface, MEA_MODULE_IF);

    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

MEA_Status MEA_API_Set_BRAS_SA_MAC_MASK(MEA_Unit_t     unit,
    MEA_Uint16     index,
    MEA_global_Egress_sa_mac_dbt* entry)
{
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry==NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (index >= MEA_MAX_GOLBL_MASK_EGRESS_MAC)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - out of range  max  \n",
            __FUNCTION__, MEA_MAX_GOLBL_MASK_EGRESS_MAC - 1);
        return MEA_ERROR;

    }

    if ((MEA_OS_maccmp(&(entry->My_Mac), &(MEA_Global_MASK_EGRESS_MAC[index].My_Mac)) == 0)){

        return MEA_OK;
    }

    if (mea_global_Egress_sa_mac_UpdateHwMAC(unit, index, entry) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_global_Egress_sa_mac_UpdateHwMAC fail \n",
            __FUNCTION__);
        return MEA_ERROR;

    }


    MEA_OS_maccpy(&(MEA_Global_MASK_EGRESS_MAC[index].My_Mac), &(entry->My_Mac));

    return MEA_OK;
}
MEA_Status MEA_API_Get_BRAS_SA_MAC_MASK(MEA_Unit_t     unit,
                                            MEA_Uint16     index,
                                            MEA_global_Egress_sa_mac_dbt* entry)
{
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry==NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (index >= MEA_MAX_GOLBL_MASK_EGRESS_MAC)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - out of range  max  \n",
            __FUNCTION__, MEA_MAX_GOLBL_MASK_EGRESS_MAC-1);
        return MEA_ERROR;
    
    }


    MEA_OS_memcpy(&entry->My_Mac, &MEA_Global_MASK_EGRESS_MAC[index].My_Mac, sizeof(MEA_MacAddr));
    
    
    return MEA_OK;
}




MEA_Status MEA_API_Set_BRAS_SA_MAC_BY_Interface(MEA_Unit_t     unit, MEA_Interface_t id, MEA_Bool Enable)
{
    MEA_Bool Valid = MEA_FALSE;
    MEA_Uint32  BitId;
    MEA_Uint32 value, write_value;

    if (MEA_API_IS_Interface_Valid(unit, id, &Valid,MEA_FALSE) != MEA_OK){
        return MEA_ERROR;
    }

    if (Valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - interface is not valid \n",
            __FUNCTION__);
          return MEA_ERROR;
    }



    
    if (MEA_Device_HW_interfaceIndex(unit, id, &BitId) != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_Device_HW_interfaceIndex Error \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    value = 0;
    value=(1 << BitId);

    write_value = mea_Global_SA_MASK_BY_Interface;

    write_value = write_value & (~value);
    value = ((Enable)<< BitId);
    
    write_value = write_value | value;


    
    MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_INTERFACE_SA_MASK_BRAS, write_value, MEA_MODULE_IF);

    mea_Global_SA_MASK_BY_Interface = write_value;


    return MEA_OK;
}


MEA_Status MEA_API_Get_BRAS_SA_MAC_BY_Interface(MEA_Unit_t     unit, MEA_OutPorts_Entry_dbt *interfaceInfo)
{
    MEA_Uint32 i;

    if (interfaceInfo == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry==NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    MEA_OS_memset(interfaceInfo, 0, sizeof(MEA_OutPorts_Entry_dbt));

    for (i = 0; i < MEA_MAX_INERTNAL_INTERFACE_NUMBER; i++){
        if (((mea_Global_SA_MASK_BY_Interface >> i) & 0x00000001) == 1){
            MEA_SET_OUTPORT(interfaceInfo, MEA_InterfaceHwInfo[i]);
        }
    }



    return MEA_OK;
}

MEA_Status MEA_API_Get_BRAS_Event_SA_MAC_InterfaceInfo(MEA_Unit_t     unit, MEA_OutPorts_Entry_dbt *interfaceInfo)
{
    MEA_Uint32 i;
    MEA_ind_read_t         ind_read;
    MEA_Uint32             retVal[1];
    

    if (interfaceInfo == NULL){
        return MEA_ERROR;
    }

    ind_read.tableType      = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset    = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_BRAS_EVENT_LAST_INTERFACE;
    ind_read.cmdReg         = MEA_IF_CMD_REG;
    ind_read.cmdMask        = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg      = MEA_IF_STATUS_REG;
    ind_read.statusMask     = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg   = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask  = MEA_IF_CMD_PARAM_TBL_TYP_MASK;

    ind_read.read_data = &(retVal[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(retVal),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }



    MEA_OS_memset(interfaceInfo, 0, sizeof(MEA_OutPorts_Entry_dbt));


    for (i = 0; i < MEA_MAX_INERTNAL_INTERFACE_NUMBER; i++){
        if (((retVal[0] >> i) & 0x00000001) == 1){
            MEA_SET_OUTPORT(interfaceInfo, MEA_InterfaceHwInfo[i]);
        }
    }



    return MEA_OK;
}





