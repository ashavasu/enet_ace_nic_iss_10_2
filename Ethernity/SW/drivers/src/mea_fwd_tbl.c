/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*---------------------------------------------------------------------------*/
/*                                 Includes                                  */
/*---------------------------------------------------------------------------*/
#ifdef MEA_OS_LINUX
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#else
#include <time.h>
#endif




#include "mea_api.h"
#include "mea_if_regs.h"
#include "mea_drv_common.h"
#include "mea_fwd_tbl.h"
#include "mea_init.h"
#include "enet_queue_drv.h"
#include "enet_xPermission_drv.h"
#include "mea_policer_drv.h"
#include "mea_bm_counters_drv.h"
#include "mea_action_drv.h"
#include "mea_limiter_drv.h"
#include "mea_action_drv.h"
#include "mea_globals_drv.h"


 

/*---------------------------------------------------------------------------*/
/*                                 Defines                                   */
/*---------------------------------------------------------------------------*/
#define MEA_NoForwarder 1

#ifdef MEA_OS_DEVICE_MEMORY_SIMULATION
#define MEA_FORWARDER_DEVICE_SIMULATION
#endif

#ifdef MEA_FORWARDER_DEVICE_SIMULATION
#define MEA_FORWARDER_DEVICE_NUM_OF_ENTRIES (4096*8*6)
#endif /* MEA_FORWARDER_DEVICE_SIMULATION */



/*---------------------------------------------------------------------------*/
/*                                 Typedefs                                  */
/*---------------------------------------------------------------------------*/

typedef struct {
   

    MEA_Forwarder_Data_dbt fields_data;
    MEA_Forwarder_Key_dbt  fields_key;

    MEA_Forwarder_Data_dbt mask_data;
    MEA_Forwarder_Key_dbt  mask_key;

}MEA_FWD_hw_filter;

typedef struct {
    MEA_Bool  valid;
    MEA_SE_filter_Entry_dbt entry;
    MEA_FWD_hw_filter       hw;
}MEA_FWD_filter_dbt;


#ifdef MEA_FORWARDER_DEVICE_SIMULATION
typedef struct {
	MEA_Bool               used;
	MEA_Forwarder_Key_dbt  key;
	MEA_Forwarder_Data_dbt data;
} MEA_Forwarder_Entry_dbt;
#endif /* MEA_FORWARDER_DEVICE_SIMULATION */

/************************************************************************/
/* FWD IPV6 DBT                                                        */
/************************************************************************/

#define MEA_IPV6_MAX_VPN 10
#define MEA_IPV6_MAX_FWD 1000


typedef struct {
    MEA_Bool  valid;
    MEA_FWD_VPN_dbt Vpn_data;

    mea_fwd_vpn_IPv6_info_dbt ipv6_data[MEA_IPV6_MAX_FWD+1];


}mea_fwd_IPv6_VPN_dbt;






mea_fwd_IPv6_VPN_dbt MEA_ForwarderIPV6_Table[MEA_IPV6_MAX_VPN+1];

MEA_Uint32   mea_firstVpnId = 0;
MEA_Uint32   mea_firstVpnIdDB = 0;



#define MEA_FORWARDE_MAX_COUNT_STATUS 500000

/*---------------------------------------------------------------------------*/
/*                                 Globals                                   */
/*---------------------------------------------------------------------------*/
#ifndef MEA_FORWARDER_DEVICE_SIMULATION
static MEA_Uint32                 MEA_drv_fwd_count = 0;
static MEA_Bool                   MEA_drv_fwd_last  = MEA_FALSE;
#endif




static MEA_FWD_filter_dbt MEA_drv_fwd_filter_tble[MEA_FORWARDER_FILTER_PROFILE];



static MEA_Fwd_Tbl_HashType_t     MEA_drv_fwd_last_scan_hash_type;





static MEA_Counters_Forwarder_dbt MEA_Counters_Forwarder;

static MEA_Counter_t meaGlobal_static_MAC;
static MEA_Counter_t meaGlobal_multicast_static_MAC;

#ifdef MEA_FORWARDER_DEVICE_SIMULATION
MEA_Forwarder_Entry_dbt MEA_Forwarder_Table[MEA_FORWARDER_DEVICE_NUM_OF_ENTRIES+1];
MEA_Uint32              MEA_Forwarder_GetNext_lastIndex;
#endif /* MEA_FORWARDER_DEVICE_SIMULATION */

/*---------------------------------------------------------------------------*/
/*                        Implementation                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                      Internal  functions                                  */
/*---------------------------------------------------------------------------*/

MEA_Bool   mea_drv_fwd_vpnIpV6_free_place(MEA_Uint32 vpn);
MEA_Uint32 mea_fwd_SignatureIpv6(MEA_Uint32 *Ipv6);
MEA_Bool   mea_drv_fwd_vpnIpV6_check_Ipv6key(MEA_Uint32 vpn, MEA_Uint32 *Ipv6);

MEA_Status mea_drv_fwd_vpnIpV6_find_Ipv6key(MEA_Uint32 vpn,MEA_Uint32 Signature, MEA_Uint32 *Ipv6);

MEA_Status mea_drv_fwd_vpnIpV6_Add_Ipv6_DB(MEA_SE_Entry_dbt  *entry_pi);
MEA_Status mea_drv_fwd_vpnIpV6_Delete_Ipv6_DB(MEA_SE_Entry_dbt  *entry_pi);
MEA_Status mea_drv_fwd_vpnIpV6_Update_Ipv6_DB(MEA_SE_Entry_dbt  *entry_pi);



MEA_Status MEA_fwd_tbl_GetNext  ( MEA_Fwd_Tbl_HashType_t      *hash_type_io,
                                  MEA_Forwarder_Signature_dbt *signature_o,
                                  MEA_Forwarder_Result_dbt    *result_o ,
                                  MEA_Bool                    *found_o);



MEA_Status mea_fwd_write_filterSearch(MEA_Unit_t unit_i,MEA_Uint8 index);

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <mea_fwd_tbl_sleep>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_fwd_tbl_sleep(void) {
         

#if defined(HW_BOARD_IS_PCI)
    MEA_OS_sleep(0, 5*1000); /* sleep for 10 microsecond */
#else
     MEA_OS_sleep(0, 1000 ); /* sleep for 1 microsecond */
#endif

} 



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <mea_fwd_tbl_write_cmd>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_fwd_tbl_write_cmd(MEA_Forwarder_cmd_dbt *entry_pi) {
    MEA_Uint32 i;
    for (i=0;i<MEA_NUM_OF_ELEMENTS(entry_pi->cmd.regs);i++){
       MEA_API_WriteReg(MEA_UNIT_0,
		                MEA_IF_WRITE_DATA_REG(i) , 
						entry_pi->cmd.regs[i], 
						MEA_MODULE_IF); 
    }
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_ReadByIndex>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_readByIndex( MEA_Fwd_Tbl_HashType_t       hash_type,
                                    MEA_Uint32                   entry_number,
                                    MEA_Forwarder_Signature_dbt *signature_o,
                                    MEA_Forwarder_Result_dbt    *result_o )
{
    MEA_Forwarder_cmd_dbt       fwd_cmd;
	MEA_ind_write_t	            ind_write;

	MEA_ind_read_t              ind_read;
    MEA_Forwarder_Signature_dbt fwd_signature;
    MEA_Forwarder_Result_dbt    fwd_result;
    MEA_Uint32                  status;
//    MEA_Globals_Entry_dbt entry;
    MEA_Uint32 val,getFWD_adderss;
    mea_memory_mode_e originalMode;
    MEA_Uint32 count_Status;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ( signature_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - signature_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

#if 0
    if ( result_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - result_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif
#if 0
     if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Error while get API_Get_Globals_Entry\n");
      return MEA_OK;
     }
#endif

     getFWD_adderss= mea_grv_get_if_FWD_address(MEA_UNIT_0);

    val= (1 << (getFWD_adderss));
    if ( entry_number > val)
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_number(%d) is bigger than max value(%d)\n",
                         __FUNCTION__, entry_number, (1 << (getFWD_adderss)));
       return MEA_ERROR; 
    }        
       
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    

    MEA_OS_memset(&fwd_cmd,       0 , sizeof(MEA_Forwarder_cmd_dbt));
    MEA_OS_memset(&fwd_signature, 0 , sizeof(MEA_Forwarder_Signature_dbt));
    MEA_OS_memset(&fwd_result,    0 , sizeof(MEA_Forwarder_Result_dbt));
    

    fwd_cmd.cmd.val.command = MEA_FWD_TABLE_READ_CMD;
    fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_READ_TBL_ID(fwd_signature.val.key.raw.key_type);
    fwd_cmd.cmd.val.priority = MEA_FWD_TABLE_READ_PRI;
    fwd_cmd.cmd.val.key.read_key.cam_mem = MEA_FWD_TABLE_READ_CAM_MEM;
    fwd_cmd.cmd.val.key.read_key.result_type = MEA_FWD_TBL_RESULT_TYPE_SIGNATURE;
    fwd_cmd.cmd.val.key.read_key.hash_type = hash_type;
    fwd_cmd.cmd.val.key.read_key.address = entry_number;
    
    originalMode =globalMemoryMode; 
	if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000){
		globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
    }
	
    ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_REG;
	ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry   	 =  (MEA_FUNCPTR)mea_fwd_tbl_write_cmd;
    ind_write.funcParam1 = (MEA_funcParam)&(fwd_cmd);

   

	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_WriteIndirect failed "
                         "(module=%d , tableType=%d , tableOffset=%d)\n",
                         __FUNCTION__,
                         MEA_MODULE_IF,
                         ind_write.tableType,
                         ind_write.tableOffset);
       globalMemoryMode = originalMode;
       return MEA_ERROR;
    }


    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status=0;
    while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) )
    {
         mea_fwd_tbl_sleep();
         status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
         count_Status++;
         if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                 __FUNCTION__);
             globalMemoryMode = originalMode;
             return MEA_ERROR;

         }
    }



	ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_REG;
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_READ;
	ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
	ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_read.statusReg		= MEA_IF_STATUS_REG;   
	ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
	ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_read.read_data      = fwd_signature.regs;

	if (MEA_API_ReadIndirect(MEA_UNIT_0,&ind_read,
                             MEA_NUM_OF_ELEMENTS(fwd_signature.regs),
                             MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		globalMemoryMode = originalMode;
        return MEA_ERROR;
    }


    if ( result_o != NULL ) {





    fwd_cmd.cmd.val.command = MEA_FWD_TABLE_READ_CMD;
    fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_READ_TBL_ID(signature_o->val.key.raw.key_type);
    fwd_cmd.cmd.val.priority = MEA_FWD_TABLE_READ_PRI;
    fwd_cmd.cmd.val.key.read_key.cam_mem = MEA_FWD_TABLE_READ_CAM_MEM;
    fwd_cmd.cmd.val.key.read_key.result_type = MEA_FWD_TBL_RESULT_TYPE_RESULT;
    fwd_cmd.cmd.val.key.read_key.hash_type = hash_type;
    fwd_cmd.cmd.val.key.read_key.address = entry_number;
    
	ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry   	 =  (MEA_FUNCPTR)mea_fwd_tbl_write_cmd;
    ind_write.funcParam1 = (MEA_funcParam)&(fwd_cmd);



	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_WriteIndirect failed "
                         "(module=%d , tableType=%d , tableOffset=%d)\n",
                         __FUNCTION__,
                         MEA_MODULE_IF,
                         ind_write.tableType,
                         ind_write.tableOffset);
       globalMemoryMode = originalMode;
       return MEA_ERROR;
    }


    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status=0;
    while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) )
    {
         mea_fwd_tbl_sleep();
         status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
         count_Status++;
         if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                 __FUNCTION__);
             globalMemoryMode = originalMode;
             return MEA_ERROR;

         }
    }


	ind_read.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_READ;
	ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
	ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_read.statusReg		= MEA_IF_STATUS_REG;   
	ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
	ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_read.read_data      = fwd_result.regs;

	    if (MEA_API_ReadIndirect(MEA_UNIT_0,&ind_read,
                                 MEA_NUM_OF_ELEMENTS(fwd_result.regs),
                                 MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                              __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		    globalMemoryMode = originalMode;
            return MEA_ERROR;
        }

    }



    MEA_OS_memcpy( signature_o, &fwd_signature, sizeof(MEA_Forwarder_Signature_dbt));
    if ( result_o != NULL ) {
    MEA_OS_memcpy( result_o, &fwd_result, sizeof(MEA_Forwarder_Result_dbt));
    }



globalMemoryMode = originalMode;
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_WriteByIndex>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_WriteByIndex( MEA_Fwd_Tbl_HashType_t       hash_type,
                                     MEA_Uint32                   entry_number,
                                     MEA_Forwarder_Signature_dbt *signature_i,
                                     MEA_Forwarder_Result_dbt    *result_i )
{
    MEA_Forwarder_cmd_dbt       fwd_cmd;
	MEA_ind_write_t	            ind_write;
    MEA_Uint32                  val,getFWD_adderss;
    MEA_Uint32                  status;
//    MEA_Globals_Entry_dbt entry;
    mea_memory_mode_e originalMode;
    MEA_Uint32 count_Status;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ( signature_i == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - signature_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if ( result_i == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - result_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#if 0
    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Error while get API_Get_Globals_Entry\n");
      return MEA_ERROR;
    }
#endif
      getFWD_adderss= mea_grv_get_if_FWD_address(MEA_UNIT_0);

    val = (1 << (getFWD_adderss));
    if ( entry_number > val )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_number(%d) is bigger than max value(%d)\n",
                         __FUNCTION__, entry_number, (1 << (getFWD_adderss)));
       return MEA_ERROR; 
    }        

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    
    MEA_OS_memset(&fwd_cmd, 0 , sizeof(MEA_Forwarder_cmd_dbt));

    originalMode =globalMemoryMode; 
	if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000){
		globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
    }

    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    if( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) == MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - status command busy is set\n",
                          __FUNCTION__);
       globalMemoryMode = originalMode;
        return MEA_ERROR;
    }
   
    fwd_cmd.cmd.val.command                    = MEA_FWD_TABLE_WRITE_CMD;
    fwd_cmd.cmd.val.table_id                   = MEA_FWD_TABLE_WRITE_TBL_ID(signature_i->val.key.raw.key_type);
    fwd_cmd.cmd.val.priority                   = MEA_FWD_TABLE_WRITE_PRI;
    fwd_cmd.cmd.val.key.write_key.cam_mem      = MEA_FWD_TABLE_WRITE_CAM_MEM;
    fwd_cmd.cmd.val.key.write_key.result_type  = MEA_FWD_TBL_RESULT_TYPE_RESULT;
    fwd_cmd.cmd.val.key.write_key.hash_type    = hash_type;
    fwd_cmd.cmd.val.key.write_key.address      = entry_number;
	MEA_OS_memcpy(&(fwd_cmd.cmd.val.key.write_key.key),
		          &(signature_i->val.key),
		          sizeof(fwd_cmd.cmd.val.key.write_key.key));
    

	ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry   	 =  (MEA_FUNCPTR)mea_fwd_tbl_write_cmd;
    ind_write.funcParam1 = (MEA_funcParam)&(fwd_cmd);
  
    

	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_WriteIndirect failed "
                         "(module=%d , tableType=%d , tableOffset=%d)\n",
                         __FUNCTION__,
                         MEA_MODULE_IF,
                         ind_write.tableType,
                         ind_write.tableOffset);
       globalMemoryMode = originalMode;
       return MEA_ERROR;
    }


    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status=0;
    while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) )
    {
         mea_fwd_tbl_sleep();
         status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
         count_Status++;
         if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                 __FUNCTION__);
             globalMemoryMode = originalMode;
             return MEA_ERROR;

         }
    }


globalMemoryMode = originalMode;
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_ReadByKey>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_readByKey( MEA_Forwarder_Key_dbt     *key_pi,
                                  MEA_Forwarder_Result_dbt  *result_po )
{
    
#ifdef MEA_FORWARDER_DEVICE_SIMULATION
	MEA_Uint32 i;
	MEA_Uint32 idx = MEA_FORWARDER_DEVICE_NUM_OF_ENTRIES; //0xFFFFFFFF;
    MEA_Forwarder_Key_dbt     key;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ( key_pi == NULL ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_pi == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
    if ( result_po == NULL ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - result_po == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



    MEA_OS_memcpy(&key,key_pi,sizeof(key));

    /* if vlan range then ignore it */
    if (key.vlan_vpn.key_type       == 3) {
        //key.vlan_vpn.vlan_to_enable = 0x00000001;
        //key.vlan_vpn.vlan_to_val    = 0x00000fff;
    }

	for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_Forwarder_Table);i++) {
		if (MEA_Forwarder_Table[i].used) { 
			if (MEA_OS_memcmp(&key,
                              &(MEA_Forwarder_Table[i].key         ),
                              sizeof(key)) == 0) {
				idx=i;
				break;
			}
		}
	}

	MEA_OS_memset(result_po,0,sizeof(*result_po));

	if (i>=MEA_NUM_OF_ELEMENTS(MEA_Forwarder_Table)) {
		return MEA_OK;
	}

	result_po->val.info.match = 1;
	MEA_OS_memcpy(&(result_po->val.data),
		          &(MEA_Forwarder_Table[idx].data          ),
				  sizeof(result_po->val.data));


#else  /* MEA_FORWARDER_DEVICE_SIMULATION */
    MEA_Forwarder_cmd_dbt       fwd_cmd;
	MEA_ind_write_t	            ind_write;
	MEA_ind_read_t              ind_read;
    MEA_Uint32                  status;
    mea_memory_mode_e originalMode;
    MEA_Uint32 count_Status=0;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ( key_pi == NULL ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_pi == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
    if ( result_po == NULL ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - result_po == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    

	/* Reset cmd structure */
    MEA_OS_memset(&fwd_cmd,       0 , sizeof(MEA_Forwarder_cmd_dbt));
    
    /* Copy the key to the fwd_cmd */
	MEA_OS_memcpy(&(fwd_cmd.cmd.val.key.search_key.key),
                  key_pi,
				  sizeof(fwd_cmd.cmd.val.key.search_key.key));
	
    /* Set the Command to Search (ReadByKey */
    fwd_cmd.cmd.val.command  = MEA_FWD_TABLE_SEARCH_CMD;
    fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_SEARCH_TBL_ID(key_pi->raw.key_type);
    fwd_cmd.cmd.val.priority = MEA_FWD_TABLE_SEARCH_PRI;



    /* Forwarder write only to the UPSTREAM */
    originalMode = globalMemoryMode; 
	if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000){
		globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
    }

    /* if vlan range then ignore it */
    if (key_pi->vlan_vpn.key_type       == 3) {
       //fwd_cmd.cmd.val.key.search_key.key.vlan_vpn.vlan_to_enable = 0x00000001;
       //fwd_cmd.cmd.val.key.search_key.key.vlan_vpn.vlan_to_val    = 0x00000fff;
    }

	/* Write the command */
	ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_fwd_tbl_write_cmd;
    ind_write.funcParam1    = (MEA_funcParam)&(fwd_cmd);
 	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_WriteIndirect failed "
                         "(module=%d , tableType=%d , tableOffset=%d)\n",
                         __FUNCTION__,
                         MEA_MODULE_IF,
                         ind_write.tableType,
                         ind_write.tableOffset);
       globalMemoryMode = originalMode;
       return MEA_ERROR;
    }


	/* Wait on the status until stable */
    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status++;
    while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) ) {
         mea_fwd_tbl_sleep();
         status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
         count_Status++;
         if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                 __FUNCTION__);
             globalMemoryMode = originalMode;
             return MEA_ERROR;

         }
    }



    /* Read the results */
	ind_read.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_READ;
	ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
	ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_read.statusReg		= MEA_IF_STATUS_REG;   
	ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
	ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_read.read_data      = &(result_po->regs[0]);
	if (MEA_API_ReadIndirect(MEA_UNIT_0,&ind_read,
                             MEA_NUM_OF_ELEMENTS(result_po->regs),
                             MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		globalMemoryMode = originalMode;
        return MEA_ERROR;
    }


    /* Restore the global memory mode */
    globalMemoryMode = originalMode;


#endif /* MEA_FORWARDER_DEVICE_SIMULATION */
    /* Return to caller */ 
    return MEA_OK;
}
 

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_WriteByKey>                             */
/*                                                                           */
/*  Note: This function use for add/update and the fwd_cmd_pi is prepare     */
/*        by the caller.                                                     */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_WriteByKey( MEA_Forwarder_cmd_dbt *fwd_cmd_pi )
{
#ifdef MEA_FORWARDER_DEVICE_SIMULATION
	MEA_Uint32 i;
	MEA_Uint32 idx = MEA_FORWARDER_DEVICE_NUM_OF_ENTRIES;  // 0xFFFFFFFF;
#endif //#else
#if 1
    MEA_ind_write_t	            ind_write;
    MEA_Uint32                  status;
    MEA_Uint32                  count_Status=0;
#endif


    mea_memory_mode_e originalMode;
    MEA_Uint32                  vlan,vlan_from,vlan_to;
    MEA_Bool                    vlan_range_enable;
  //  MEA_Uint32                  keyType;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ( fwd_cmd_pi == NULL ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - fwd_cmd_pi == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    
 /* Write Forwarder to UpStream only */
    originalMode =globalMemoryMode; 
	if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000){
		globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
    }
    

  //  keyType = fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.key_type;


#ifdef MEA_FORWARDER_DEVICE_SIMULATION




	if ((fwd_cmd_pi->cmd.val.command != MEA_FWD_TABLE_UPDATE_CMD)  && 
		(fwd_cmd_pi->cmd.val.command != MEA_FWD_TABLE_ADD_CMD   )   ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Unknown fwd command (%d) \n",
						  __FUNCTION__,fwd_cmd_pi->cmd.val.command);
        globalMemoryMode = originalMode;
		return MEA_ERROR;
	}

    /* Check if we are in vlan range mode */
    if (fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.key_type == 3) {
       if (fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_enable) {
           vlan_from = fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan;
           vlan_to   = fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val;
           vlan_range_enable = MEA_TRUE;
       } else {
           vlan_from = 1;
           vlan_to   = 1;
           vlan_range_enable = MEA_FALSE;
       }
       //fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_enable = 0x00000001;
       //fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val    = 0x00000fff;
    } else {
       vlan_from = 1;
       vlan_to   = 1;
       vlan_range_enable = MEA_FALSE;
    }

    for (vlan=vlan_from;vlan<=vlan_to;vlan++) {

        if (vlan_range_enable) {
            fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan = vlan;
        }

        for (i=0,idx= MEA_FORWARDER_DEVICE_NUM_OF_ENTRIES;i<MEA_NUM_OF_ELEMENTS(MEA_Forwarder_Table);i++) {
		if (!MEA_Forwarder_Table[i].used) { 
			if (idx == MEA_FORWARDER_DEVICE_NUM_OF_ENTRIES) {
				idx=i;
			}
		} else {
			if (MEA_OS_memcmp(&(MEA_Forwarder_Table[i].key         ),
				              &(fwd_cmd_pi->cmd.val.key.add_key.key),
							   sizeof(MEA_Forwarder_Table[i].key    )) == 0)   {
				if (fwd_cmd_pi->cmd.val.command == MEA_FWD_TABLE_UPDATE_CMD) {
					idx=i;
					break;
				} else {
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						              "%s - forwarder entry already exist \n",
									  __FUNCTION__);
                        if (fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.key_type == 3) {
                            if (vlan_range_enable) {
                                fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan        = vlan_from;
                                fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val = vlan_to;
                            } else {
                                fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_enable = 0;
                                fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val    = 0;
                            }
                        }
                        globalMemoryMode = originalMode;
					return MEA_ERROR;
				}
			}
		}
	}

	if (fwd_cmd_pi->cmd.val.command == MEA_FWD_TABLE_UPDATE_CMD) {
		if (i>=MEA_NUM_OF_ELEMENTS(MEA_Forwarder_Table)) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					          "%s - Entry not found\n",
							  __FUNCTION__);
                if (fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.key_type == 3) {
                    if (vlan_range_enable) {
                        fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan        = vlan_from;
                        fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val = vlan_to;
                    } else {
                        fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_enable = 0;
                        fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val    = 0;
                    }
                }
                globalMemoryMode = originalMode;
			return MEA_ERROR;
		}
	} else {
		if (idx == MEA_FORWARDER_DEVICE_NUM_OF_ENTRIES) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					          "%s - No free entry \n",
							  __FUNCTION__);
                if (fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.key_type == 3) {
                    if (vlan_range_enable) {
                        fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan        = vlan_from;
                        fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val = vlan_to;
                    } else {
                        fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_enable = 0;
                        fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val    = 0;
                    }
                }
                globalMemoryMode = originalMode;
			return MEA_ERROR;
		}
	}

	/* Update the entry */
	MEA_Forwarder_Table[idx].used = MEA_TRUE;
	MEA_OS_memcpy(&(MEA_Forwarder_Table[idx].key          ),
	              &(fwd_cmd_pi->cmd.val.key.add_key.key),
	  		      sizeof(MEA_Forwarder_Table[idx].key    ));
	MEA_OS_memcpy(&(MEA_Forwarder_Table[idx].data          ),
				  &(fwd_cmd_pi->cmd.val.data),
				  sizeof(MEA_Forwarder_Table[idx].data    ));
    }

    if (fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.key_type == 3) {
        if (vlan_range_enable) {
            fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan        = vlan_from;
            fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val = vlan_to;
        } else {
            fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_enable = 0;
            fwd_cmd_pi->cmd.val.key.add_key.key.vlan_vpn.vlan_to_val    = 0;
        }
    }

#endif //#else  /* MEA_FORWARDER_DEVICE_SIMULATION */
#if 1	

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ( fwd_cmd_pi == NULL ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - fwd_cmd_pi == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

  

	/* Check the status is allowed to start the operation */
    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    if( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) == 1 ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - status command busy is set\n",
                          __FUNCTION__);
       globalMemoryMode = originalMode;
        return MEA_ERROR;
    }
  
	/* Write the command */
	ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry   	 =  (MEA_FUNCPTR)mea_fwd_tbl_write_cmd;
    ind_write.funcParam1	 =	(MEA_funcParam)fwd_cmd_pi;

    /* Check if we are in vlan range mode */
    if (fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.key_type == 3) {
       if (fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_enable) {
           vlan_from = fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan;
           vlan_to   = fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_val;
           vlan_range_enable = MEA_TRUE;
       } else {
           vlan_from = 1;
           vlan_to   = 1;
           vlan_range_enable = MEA_FALSE;
       }
       //fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_enable = 0x00000001;
       //fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_val    = 0x00000fff;
    } else {
       vlan_from = 1;
       vlan_to   = 1;
       vlan_range_enable = MEA_FALSE;
    }

    for (vlan=vlan_from;vlan<=vlan_to;vlan++) {


        if (vlan_range_enable) {
            fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan = vlan;
        }


	    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_WriteIndirect failed "
                              "(module=%d , tableType=%d , tableOffset=%d)\n",
                              __FUNCTION__,
                              MEA_MODULE_IF,
                              ind_write.tableType,
                              ind_write.tableOffset);
            if (fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.key_type == 3) {
                if (vlan_range_enable) {
                    fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan        = vlan_from;
                    fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_val = vlan_to;
                } else {
                    fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_enable = 0;
                    fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_val    = 0;
                }
            }
            globalMemoryMode = originalMode;
            return MEA_ERROR;
        }


        /* Check the Status */
        status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
        count_Status=0;
        while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) ) {
             mea_fwd_tbl_sleep();
            status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
            count_Status++;
            if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                    __FUNCTION__);
                globalMemoryMode = originalMode;
                return MEA_ERROR;
            }
        }
	    if (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_COLLISION_MASK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_WriteIndirect failed - collision "
                              "(status=0x%08x module=%d , tableType=%d , tableOffset=%d)\n",
                              __FUNCTION__,
                              status,
                              MEA_MODULE_IF,
                              ind_write.tableType,
                              ind_write.tableOffset);
            if (fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.key_type == 3) {
                if (vlan_range_enable) {
                    fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan        = vlan_from;
                    fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_val = vlan_to;
                } else {
                    fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_enable = 0;
                    fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_val    = 0;
                }
            }
            globalMemoryMode = originalMode;
            return MEA_ERROR;
	    }

    }

    if (fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.key_type == 3) {
        if (vlan_range_enable) {
            fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan        = vlan_from;
            fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_val = vlan_to;
        } else {
            fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_enable = 0;
            fwd_cmd_pi->cmd.val.key.write_key.key.vlan_vpn.vlan_to_val    = 0;
        }
    }

#endif /* MEA_FORWARDER_DEVICE_SIMULATION */
    
	/* Restore Global Memory Mode */
	globalMemoryMode = originalMode;

	/* Return to Caller */
	return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_DeleteByKey>                            */
/*                                                                           */
/*  Note: This function use for delete and the fwd_cmd_pi is prepare         */
/*        by the caller.                                                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_DeleteByKey( MEA_Forwarder_cmd_dbt* fwd_cmd_pi) 
{
    MEA_Uint32                  vlan,vlan_from,vlan_to;
    MEA_Bool                    vlan_range_enable;
#ifdef MEA_FORWARDER_DEVICE_SIMULATION
	MEA_Uint32 i;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ( fwd_cmd_pi == NULL ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - fwd_cmd_pi == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    /* Check if we are in vlan range mode */
    if (fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.key_type == 3) {
       if (fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_enable) {
           vlan_from = fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan;
           vlan_to   = fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val;
           vlan_range_enable = MEA_TRUE;
       } else {
           vlan_from = 1;
           vlan_to   = 1;
           vlan_range_enable = MEA_FALSE;
       }
       //fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_enable = 0x00000001;
       //fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val    = 0x00000fff;
    } else {
       vlan_from = 1;
       vlan_to   = 1;
       vlan_range_enable = MEA_FALSE;
    }

    for (vlan=vlan_from;vlan<=vlan_to;vlan++) {

        if (vlan_range_enable) {
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan = vlan;
        }

	for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_Forwarder_Table);i++) {
		if ((MEA_Forwarder_Table[i].used) &&
		    (MEA_OS_memcmp(&(MEA_Forwarder_Table[i].key         ),
			               &(fwd_cmd_pi->cmd.val.key.add_key.key),
			 			   sizeof(MEA_Forwarder_Table[i].key    )) == 0))   {
		   MEA_OS_memset(&(MEA_Forwarder_Table[i]),
						 0,
						 sizeof(MEA_Forwarder_Table[i]));
		        break;
		}
	}
        if (i>=MEA_NUM_OF_ELEMENTS(MEA_Forwarder_Table)) {
	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			          "%s - Entry not found\n",
				      __FUNCTION__);
            if (fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.key_type == 3) {
                if (vlan_range_enable) {
                    fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan        = vlan_from;
                    fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val = vlan_to;
                } else {
                    fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_enable = 0;
                    fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val    = 0;
                }
            }
	return MEA_ERROR;
        }
    }

    if (fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.key_type == 3) {
        if (vlan_range_enable) {
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan        = vlan_from;
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val = vlan_to;
        } else {
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_enable = 0;
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val    = 0;
        }
    }

#else /* MEA_FORWARDER_DEVICE_SIMULATION */
	MEA_ind_write_t	            ind_write;
    MEA_Uint32                  status;
    mea_memory_mode_e originalMode;
    MEA_Uint32 count_Status;

  
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ( fwd_cmd_pi == NULL ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - fwd_cmd_pi == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	/* Write Forwarder to UpStream only */
    originalMode =globalMemoryMode; 
	if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000){
		globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
    }

	/* Check the status is allowed to start the operation */
    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    if( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) == 1 ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - status command busy is set\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
  

	/* Write the command */
	ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE; //Delete
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry   	 =  (MEA_FUNCPTR)mea_fwd_tbl_write_cmd;
    ind_write.funcParam1	 =	(MEA_funcParam)fwd_cmd_pi;

    /* Check if we are in vlan range mode */
    if (fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.key_type == 3) {
       if (fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_enable) {
           vlan_from = fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan;
           vlan_to   = fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val;
           vlan_range_enable = MEA_TRUE;
       } else {
           vlan_from = 1;
           vlan_to   = 1;
           vlan_range_enable = MEA_FALSE;
       }
      // fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_enable = 0x00000001;
       //fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val    = 0x00000fff;
    } else {
       vlan_from = 1;
       vlan_to   = 1;
       vlan_range_enable = MEA_FALSE;
    }

    for (vlan=vlan_from;vlan<=vlan_to;vlan++) {

        if (vlan_range_enable) {
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan = vlan;
        }
    
        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_WriteIndirect failed "
                              "(module=%d , tableType=%d , tableOffset=%d)\n",
                              __FUNCTION__,
                              MEA_MODULE_IF,
                              ind_write.tableType,
                              ind_write.tableOffset);
            globalMemoryMode = originalMode;
            if (fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.key_type == 3) {
                if (vlan_range_enable) {
                    fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan        = vlan_from;
                    fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val = vlan_to;
                } else {
                    fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_enable = 0;
                    fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val    = 0;
                }
            }
            return MEA_ERROR;
        }

        /* Check Status */
        status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
        count_Status=0;
        while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) )
        {
             mea_fwd_tbl_sleep();
            status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
            count_Status++;
            if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                    __FUNCTION__);
                globalMemoryMode = originalMode;
                return MEA_ERROR;

            }
        }

    }

    if (fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.key_type == 3) {
        if (vlan_range_enable) {
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan        = vlan_from;
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val = vlan_to;
        } else {
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_enable = 0;
            fwd_cmd_pi->cmd.val.key.delete_key.key.vlan_vpn.vlan_to_val    = 0;
        }
    }

    /* Restore Global Memory Mode */
    globalMemoryMode = originalMode;

#endif /* MEA_FORWARDER_DEVICE_SIMULATION */

	/* Return to Caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_DeleteAll>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_DeleteAll(void)
{


#ifdef MEA_FORWARDER_DEVICE_SIMULATION

    MEA_OS_memset(&(MEA_Forwarder_Table[0]),
  				  0,
				  sizeof(MEA_Forwarder_Table));

#else /* MEA_FORWARDER_DEVICE_SIMULATION */
    //MEA_Globals_Entry_dbt entry;
    MEA_Uint32  Globals1;
    MEA_Uint32 status;
    mea_memory_mode_e originalMode;
    MEA_Uint32 count_Status;

	originalMode =globalMemoryMode;
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000){
		globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
    }


    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    
    if( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) == 1 )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - status command busy is set\n",
                          __FUNCTION__);
       globalMemoryMode = originalMode;
        return MEA_ERROR;
    }
#if 1 //MEA_FWD_DEL
       Globals1= mea_drv_get_if_global1(MEA_UNIT_0);
       
       Globals1 |=  1<<13;
       
       MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_GLOBAL_REG(1),Globals1,MEA_MODULE_IF);

       mea_drv_set_if_global1(MEA_UNIT_0,Globals1);
#else
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Disable delete FWD\n");
#endif



    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status=0;
    while ( (status & MEA_IF_STATUS_FWD_TBL_INIT_BUSY_MASK) )
    {

         mea_fwd_tbl_sleep();
         status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
         count_Status++;
         if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - 1 MEA_IF_STATUS_FWD_TBL_INIT_BUSY_MASK for long time (%d)\n",
                 __FUNCTION__,count_Status);
             globalMemoryMode = originalMode;
             return MEA_ERROR;

         }
    }


    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status=0;
    while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) )
    {
         mea_fwd_tbl_sleep();
         status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
         count_Status++;
         if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                 __FUNCTION__);
             globalMemoryMode = originalMode;
             return MEA_ERROR;

         }
    }


    
    Globals1= mea_drv_get_if_global1(MEA_UNIT_0);

    Globals1 &=  0xffffdfff; 

    MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_GLOBAL_REG(1),Globals1,MEA_MODULE_IF);

    mea_drv_set_if_global1(MEA_UNIT_0,Globals1);





   globalMemoryMode = originalMode;

#endif /* MEA_FORWARDER_DEVICE_SIMULATION */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_GetFirst>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_GetFirst ( MEA_Fwd_Tbl_HashType_t      *hash_type_io,
                                  MEA_Forwarder_Signature_dbt *signature_o,
                                  MEA_Forwarder_Result_dbt    *result_o ,
                                  MEA_Bool                    *found_o) 
{

#ifdef MEA_FORWARDER_DEVICE_SIMULATION
	MEA_Uint32 i;
#else  /* MEA_FORWARDER_DEVICE_SIMULATION */
	MEA_ind_write_t	            ind_write;
    MEA_Forwarder_cmd_dbt       fwd_cmd;
    MEA_Uint32                  status;
    //MEA_Globals_Entry_dbt entry;
    MEA_Uint32 getFWD_adderss;
    mea_memory_mode_e originalMode;
    MEA_Uint32 count_Status;
#endif  /* MEA_FORWARDER_DEVICE_SIMULATION */
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if ( hash_type_io == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - hash_type_io == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if ( (*hash_type_io != 0) && (*hash_type_io != 1) )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - invalid *hash_type_io (%d)\n",
                         __FUNCTION__,*hash_type_io);
       return MEA_ERROR; 
    }

    if ( signature_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - signature_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if ( result_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - result_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if ( found_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    
#ifdef MEA_FORWARDER_DEVICE_SIMULATION

    MEA_OS_memset(signature_o,0,sizeof(*signature_o));
    MEA_OS_memset(result_o,0,sizeof(*result_o));
	*found_o = MEA_FALSE;

	for (i=0;i<MEA_NUM_OF_ELEMENTS(MEA_Forwarder_Table);i++) {
		if (MEA_Forwarder_Table[i].used) {
		   signature_o->val.info.match = 1;
		   MEA_OS_memcpy(&(signature_o->val.key),
			             &(MEA_Forwarder_Table[i].key),
						 sizeof(signature_o->val.key));
		   result_o->val.info.match = 1;
		   MEA_OS_memcpy(&(result_o->val.data),
			             &(MEA_Forwarder_Table[i].data),
						 sizeof(result_o->val.data));
		   *found_o = MEA_TRUE;
		   MEA_Forwarder_GetNext_lastIndex=i;
		   return MEA_OK;
		}
	}



#else  /* MEA_FORWARDER_DEVICE_SIMULATION */
    MEA_OS_memset(&fwd_cmd       , 0 , sizeof(MEA_Forwarder_cmd_dbt));

    originalMode =globalMemoryMode;
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000){
		globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
    }

    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    
    if( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) == 1 )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - status command busy is set\n",
                          __FUNCTION__);
       globalMemoryMode = originalMode;
        return MEA_ERROR;
    }
#if 0
	if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Error while get API_Get_Globals_Entry\n");
      globalMemoryMode = originalMode;
      return MEA_ERROR;
     }
#endif

    fwd_cmd.cmd.val.command                  = MEA_FWD_TABLE_RST_NEXT_CMD;
    if(*hash_type_io==0)
        fwd_cmd.cmd.val.table_id                 = MEA_FWD_TABLE_RST_NEXT_CMD_TBL_ID;
    else{
        fwd_cmd.cmd.val.table_id                 = ((MEA_GLOBAL_ISDESC_EXTERNAL == MEA_TRUE) || (MEA_GLOBAL_FORWARDER_STATE_SHOW == MEA_TRUE)) ? (5) : (MEA_FWD_TABLE_RST_NEXT_CMD_TBL_ID);
        //fwd_cmd.cmd.val.table_id = (5);
    }
     fwd_cmd.cmd.val.priority                 = MEA_FWD_TABLE_RST_NEXT_PRI;
    fwd_cmd.cmd.val.key.read_key.cam_mem     = MEA_FWD_TABLE_READ_CAM_MEM;
    fwd_cmd.cmd.val.key.read_key.result_type = MEA_FWD_TBL_RESULT_TYPE_SIGNATURE;
    fwd_cmd.cmd.val.key.read_key.hash_type   = *hash_type_io;
#if 0

    fwd_cmd.cmd.val.key.read_key.address     = ((1 << (entry.verInfo.val.forwarder_Addr_Width)) +1 );
#else
      getFWD_adderss= mea_grv_get_if_FWD_address(MEA_UNIT_0);

      fwd_cmd.cmd.val.key.read_key.address = ((1 << (getFWD_adderss)) +1 ); 

#endif
	ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry   	= (MEA_FUNCPTR)mea_fwd_tbl_write_cmd;
    ind_write.funcParam1	= (MEA_funcParam)&(fwd_cmd);


	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_WriteIndirect failed "
                         "(module=%d , tableType=%d , tableOffset=%d)\n",
                         __FUNCTION__,
                         MEA_MODULE_IF,
                         ind_write.tableType,
                         ind_write.tableOffset);
       globalMemoryMode = originalMode;
       return MEA_ERROR;
    }


    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status=0;
    while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) )
    {
         mea_fwd_tbl_sleep();
         status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
         count_Status++;
         if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                 __FUNCTION__);
             globalMemoryMode = originalMode;
             return MEA_ERROR;

         }
    }
    
    if (*hash_type_io == 0) {
       MEA_drv_fwd_count = 0;
    }
    globalMemoryMode = originalMode;
    MEA_drv_fwd_last  = MEA_FALSE;

    if (MEA_fwd_tbl_GetNext  ( hash_type_io,
                               signature_o,
                               result_o ,
                               found_o) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_fwd_tbl_GetNext failed \n",
                         __FUNCTION__);
       
       return MEA_ERROR;

    }
    
#endif /* MEA_FORWARDER_DEVICE_SIMULATION */
    return MEA_OK;

}

#if defined(MEA_ENV_S1) && defined(MEA_ENV_S1_DEBUG)
#define TIME_SYSLOG(str) \
{\
   struct timeval now;\
 gettimeofday( &now, NULL );\
   syslog( LOG_INFO, "xyz - %s - %d,%03d", str, now.tv_sec, now.tv_usec/1000 );\
}; 

#define TIME_DELTA(init) \
{\
   struct timeval now;\
   int delta_sec,delta_usec;\
 gettimeofday( &now, NULL );\
 delta_sec = now.tv_sec-init.tv_sec;\
 delta_usec = now.tv_usec-init.tv_usec;\
   syslog( LOG_INFO, "DELTA %d,%03d", delta_sec, delta_usec/1000 );\
}; 

#define DARIO_RT_SYSLOG(str,t,v) \
   {\
       unsigned int delta;\
       struct timeval prev, now;\
          gettimeofday(&now, NULL);\
          memcpy(&prev,(struct timeval *) t,sizeof(struct timeval));\
          if ((now.tv_usec <= prev.tv_usec) || (now.tv_sec > prev.tv_sec))\
          {\
                delta = 1000000 - prev.tv_usec + now.tv_usec;\
          }\
          else\
          {\
                delta = now.tv_usec - prev.tv_usec;\
          }\
          if (delta > 20000)\
          {\
                syslog(LOG_INFO,"[%-32s] : %d usec *** HIGH ***", str, delta);\
          }\
          else\
          {\
                if (v)\
                   syslog(LOG_INFO,"[%-32s] : %d usec", str, delta);\
          }\
     };

#endif
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_GetNext>                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_GetNext  ( MEA_Fwd_Tbl_HashType_t      *hash_type_io,
                                  MEA_Forwarder_Signature_dbt *signature_o,
                                  MEA_Forwarder_Result_dbt    *result_o ,
                                  MEA_Bool                    *found_o) 
{
#if defined(MEA_ENV_S1) && defined(MEA_ENV_S1_DEBUG)
   struct timeval init;
#endif

#ifdef MEA_FORWARDER_DEVICE_SIMULATION
	MEA_Uint32 i;
#else /*  MEA_FORWARDER_DEVICE_SIMULATION */
    //MEA_Globals_Entry_dbt globals_entry;
    MEA_ind_write_t	  ind_write;
    MEA_ind_read_t        ind_read;
    MEA_Uint32            status;
    MEA_Forwarder_cmd_dbt fwd_cmd;
    mea_memory_mode_e originalMode;
    MEA_Uint32 count_Status;
 
#endif /*  MEA_FORWARDER_DEVICE_SIMULATION */


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if ( hash_type_io == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - hash_type_io == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if ( (*hash_type_io != 0) && (*hash_type_io != 1) )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - invalid *hash_type_io (%d)\n",
                         __FUNCTION__,*hash_type_io);
       return MEA_ERROR; 
    }


    if ( signature_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - signature_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if ( result_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - result_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if ( found_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
#ifdef MEA_FORWARDER_DEVICE_SIMULATION

    MEA_OS_memset(signature_o,0,sizeof(*signature_o));
    MEA_OS_memset(result_o,0,sizeof(*result_o));
	*found_o = MEA_FALSE;

	for (i=MEA_Forwarder_GetNext_lastIndex+1;i<MEA_NUM_OF_ELEMENTS(MEA_Forwarder_Table);i++) {
		if (MEA_Forwarder_Table[i].used) {
		   signature_o->val.info.match = 1;
		   MEA_OS_memcpy(&(signature_o->val.key),
			             &(MEA_Forwarder_Table[i].key),
						 sizeof(signature_o->val.key));
		   result_o->val.info.match = 1;
		   MEA_OS_memcpy(&(result_o->val.data),
			             &(MEA_Forwarder_Table[i].data),
						 sizeof(result_o->val.data));
		   *found_o = MEA_TRUE;
		   MEA_Forwarder_GetNext_lastIndex=i;
		   return MEA_OK;
		}
	}



#else  /* MEA_FORWARDER_DEVICE_SIMULATION */

	if (MEA_drv_fwd_last == MEA_TRUE) {
       if (*hash_type_io == 1) {
          *found_o = MEA_FALSE;
          return MEA_OK;
       }
       (*hash_type_io)++;

       if (MEA_fwd_tbl_GetFirst ( hash_type_io,
                                  signature_o,
                                  result_o ,
                                  found_o) != MEA_OK) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_fwd_tbl_GetFirst failed \n",
                             __FUNCTION__);
          return MEA_ERROR;
       }

       return MEA_OK;

    }

    MEA_OS_memset(&fwd_cmd       , 0 , sizeof(MEA_Forwarder_cmd_dbt));

    originalMode =globalMemoryMode;
    if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000){
		globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
    }

    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    if( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) == 1 )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - status command busy is set\n",
                          __FUNCTION__);
        globalMemoryMode = originalMode;
        return MEA_ERROR;
    }
#if 0   
    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_Globals failed \n",
                          __FUNCTION__);
        globalMemoryMode = originalMode;
        return MEA_ERROR;
    }
 
    
    globals_entry.if_global1.val.forwarder_next_cmd = 1 - globals_entry.if_global1.val.forwarder_next_cmd;
    

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_Globals (1) failed  \n",
                          __FUNCTION__);
        globalMemoryMode = originalMode;
        return MEA_ERROR;
    }
#else

    {
        MEA_Uint32 valSet;
        MEA_Uint32 forwarder_next_cmd;

        valSet= mea_drv_get_if_global1(MEA_UNIT_0);
       forwarder_next_cmd=0;
       forwarder_next_cmd=((valSet>>7) & 0x00000001);

       forwarder_next_cmd = 1- forwarder_next_cmd;
      
       valSet &= ~(0x00000080);
       valSet |= forwarder_next_cmd<<7;

    
            MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_GLOBAL_REG(1),valSet,MEA_MODULE_IF);
 
            mea_drv_set_if_global1(MEA_UNIT_0,valSet);
    }
#endif


#if defined(MEA_ENV_S1) && defined(MEA_ENV_S1_DEBUG)
gettimeofday( &init, NULL );
#endif
    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status=0;
    while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) )
    {
         mea_fwd_tbl_sleep();
         status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
         count_Status++;
         if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                 __FUNCTION__);
             globalMemoryMode = originalMode;
             return MEA_ERROR;

         }
    }
#if defined(MEA_ENV_S1)&& defined(MEA_ENV_S1_DEBUG)
syslog( LOG_INFO,"count_Status %d",count_Status);
DARIO_RT_SYSLOG("singleGetNext",&init,1);
#endif
	ind_read.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_READ;
	ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
	ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_read.statusReg		= MEA_IF_STATUS_REG;   
	ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
	ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_read.read_data      = signature_o->regs;

	if (MEA_API_ReadIndirect(MEA_UNIT_0,&ind_read,
                             MEA_NUM_OF_ELEMENTS(signature_o->regs),
                             MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		globalMemoryMode = originalMode;
        return MEA_ERROR;
    }

	MEA_drv_fwd_last  = signature_o->val.info.last;
			   
    if (signature_o->val.info.match == MEA_FALSE) {

       if (MEA_drv_fwd_last == MEA_TRUE) {
          globalMemoryMode = originalMode; 
          return MEA_fwd_tbl_GetNext  ( hash_type_io,
                                        signature_o,
                                        result_o ,
                                        found_o);
       }
       *found_o = MEA_FALSE;
       globalMemoryMode = originalMode;
       return MEA_OK;
	}

#if 0
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                      "%s - %d match=%d , valid=%d last=%d \n",
                      __FUNCTION__,
                      ++MEA_drv_fwd_count,
                      signature_o->signature.val.match,
                      signature_o->signature.val.valid,
                      signature_o->signature.val.last);
#endif

    *found_o = MEA_TRUE;

       fwd_cmd.cmd.val.command  = MEA_FWD_TABLE_SEARCH_CMD;
       fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_SEARCH_TBL_ID(signature_o->val.key.raw.key_type);
       fwd_cmd.cmd.val.priority = MEA_FWD_TABLE_SEARCH_PRI;
	   MEA_OS_memcpy(&(fwd_cmd.cmd.val.key.search_key.key),
		             &(signature_o->val.key),
					 sizeof(fwd_cmd.cmd.val.key.search_key.key));
 
       
	   ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
  	   ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE;
 	   ind_write.cmdReg		= MEA_IF_CMD_REG;      
	   ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	   ind_write.statusReg		= MEA_IF_STATUS_REG;   
	   ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	   ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	   ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	   ind_write.writeEntry   	 =  (MEA_FUNCPTR)mea_fwd_tbl_write_cmd;
       ind_write.funcParam1	 =	(MEA_funcParam)&(fwd_cmd);



	   if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF) != MEA_OK) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_WriteIndirect failed "
                             "(module=%d , tableType=%d , tableOffset=%d)\n",
                             __FUNCTION__,
                             MEA_MODULE_IF,
                             ind_write.tableType,
                             ind_write.tableOffset);
          globalMemoryMode = originalMode;
          return MEA_ERROR;
       }

       status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
       count_Status=0;
       while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) )
       {
           mea_fwd_tbl_sleep();
           status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
           count_Status++;
           if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                   "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
                   __FUNCTION__);
               globalMemoryMode = originalMode;
               return MEA_ERROR;

           }
       }


	   ind_read.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	   ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_READ;
       ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
	   ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	   ind_read.statusReg		= MEA_IF_STATUS_REG;   
	   ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
	   ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	   ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	   ind_read.read_data      = result_o->regs;

	   if (MEA_API_ReadIndirect(MEA_UNIT_0,&ind_read,
                                MEA_NUM_OF_ELEMENTS(result_o->regs),
                                MEA_MODULE_IF)!=MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                             __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		   globalMemoryMode = originalMode;
           return MEA_ERROR;
       }
    globalMemoryMode = originalMode;
#endif  /* MEA_FORWARDER_DEVICE_SIMULATION */
   return MEA_OK;

}






/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <mea_drv_Init_Forwarder>                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_Forwarder(MEA_Unit_t unit_i)
{
    if(b_bist_test){
      return MEA_OK;
    }

    MEA_OS_memset(&MEA_ForwarderIPV6_Table, 0, sizeof(MEA_ForwarderIPV6_Table));
    meaGlobal_static_MAC.val = 0;
    meaGlobal_multicast_static_MAC.val=0;


    if ((MEA_Uint32) MEA_FWD_DATA_WIDTH > (MEA_Uint32)(sizeof(MEA_Forwarder_Data_dbt)*8)) {
#ifdef MEA_NEW_FWD_ROW_SET        
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_STATIC_WIDTH				:%d\n",MEA_FWD_DATA_STATIC_WIDTH );
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_AGE_WIDTH					:%d\n",MEA_FWD_DATA_AGE_WIDTH );
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_XPERMISSION_ID_WIDTH		:%d\n",MEA_FWD_DATA_XPERMISSION_ID_WIDTH );
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_FWD_DATA_RESERV_WIDTH				:%d\n", MEA_FWD_DATA_RESERV_WIDTH);
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_FWD_DATA_SA_DISCARD_WIDTH			:%d\n", MEA_FWD_DATA_SA_DISCARD_WIDTH);
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_FWD_DATA_ACCESS_PORT_WIDTH			:%d\n", MEA_FWD_DATA_ACCESS_PORT_WIDTH);
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_ACTION_ID_VALID_WIDTH		:%d\n",MEA_FWD_DATA_ACTION_ID_VALID_WIDTH );
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_ACTION_ID_WIDTH			:%d\n",MEA_FWD_DATA_ACTION_ID_WIDTH );
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_LIMITER_ID_WIDTH			:%d\n",MEA_FWD_DATA_LIMITER_ID_WIDTH );
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "MEA_FWD_DATA_RESERV1_WIDTH			    :%d\n", MEA_FWD_DATA_RESERV1_WIDTH);
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_PARITY_WIDTH              :%d\n",MEA_FWD_DATA_PARITY_WIDTH );

#else
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_STATIC_WIDTH          %d\n",MEA_FWD_DATA_STATIC_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_AGE_WIDTH             %d\n",MEA_FWD_DATA_AGE_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_XPERMISSION_ID_WIDTH  %d\n",MEA_FWD_DATA_XPERMISSION_ID_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH    %d\n",MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH );

MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " MEA_FWD_DATA_ACCESS_PORT_BIT0_2      %d\n", MEA_FWD_DATA_ACCESS_PORT_BIT0_2);

MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_SA_DISCARD_WIDTH      %d\n",MEA_FWD_DATA_SA_DISCARD_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_COS_FORCE_WIDTH       %d\n",MEA_FWD_DATA_COS_FORCE_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_COS_WIDTH             %d\n",MEA_FWD_DATA_COS_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_PRI_FORCE_WIDTH       %d\n",MEA_FWD_DATA_PRI_FORCE_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_PRI_WIDTH             %d\n",MEA_FWD_DATA_PRI_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_COL_FORCE_WIDTH       %d\n",MEA_FWD_DATA_COL_FORCE_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_COL_WIDTH             %d\n",MEA_FWD_DATA_COL_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_ACTION_ID_VALID_WIDTH %d\n",MEA_FWD_DATA_ACTION_ID_VALID_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_ACTION_ID_WIDTH       %d\n",MEA_FWD_DATA_ACTION_ID_WIDTH );
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " MEA_FWD_DATA_ACCESS_PORT_BIT3_7 %d\n", MEA_FWD_DATA_ACCESS_PORT_BIT3_7);
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_LIMITER_ID_WIDTH      %d\n",MEA_FWD_DATA_LIMITER_ID_WIDTH );

MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " MEA_FWD_DATA_ACCESS_PORT_BIT_8    %d\n", MEA_FWD_DATA_ACCESS_PORT_BIT_8);

MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," MEA_FWD_DATA_PARITY_WIDTH          %d\n",MEA_FWD_DATA_PARITY_WIDTH );















        
        
        
        
        
        
        
        
        
        
#endif// MEA_NEW_FWD_ROW_SET       
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_FWD_DATA_WIDTH (%d) != sizeof(MEA_Forwarder_Data_dbt) (%d) * 8\n",
                          __FUNCTION__,MEA_FWD_DATA_WIDTH,sizeof(MEA_Forwarder_Data_dbt));

        return MEA_ERROR;
    }





    /* Check if support */
    if (!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize MEA fwd not Support \n");
        return MEA_OK;   
    }



#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
	/* Check if support */
	if (!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
		return MEA_OK;   
	}
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                         "Initialize MEA fwd tbl DeleteAll \n");

    if (MEA_fwd_tbl_DeleteAll() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_fwd_tbl_DeleteAll failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
#endif

	
	
	

    /* Reset Counters Forwarder */
    if (MEA_API_Clear_Counters_Forwarder(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Clear_Counters_Forwader failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Set Aging defaults */
    if (MEA_API_Set_SE_Aging (unit_i,
	                          MEA_GLOBAL_MAC_AGING_INTERVAL_DEF_VAL,
			   			      MEA_GLOBAL_MAC_AGING_ADMIN_DEF_VAL) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Set_SE_Aging failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
 MEA_OS_memset(&MEA_drv_fwd_filter_tble[0],0,sizeof(MEA_drv_fwd_filter_tble));

    if(MEA_GLOBALE_FORWADER_FILTER_SUPPORT){
        MEA_drv_fwd_filter_tble[0].valid = MEA_TRUE;
        MEA_drv_fwd_filter_tble[0].entry.fwd_reg_enable=MEA_TRUE;
        MEA_drv_fwd_filter_tble[0].hw.mask_key.raw.valid  = MEA_TRUE;
        MEA_drv_fwd_filter_tble[0].hw.fields_key.raw.valid= MEA_TRUE;
        /*Make one more profile that is create on the default*/
        MEA_drv_fwd_filter_tble[1].valid = MEA_TRUE;
        
    }

	return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <mea_drv_ReInit_Forwarder>                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_Forwarder(MEA_Unit_t unit_i)
{

    MEA_Counters_Forwarder_dbt save_counters_forwarder;
    MEA_Globals_Entry_dbt globals_entry;
    
    



     if (MEA_API_Get_Globals_Entry(MEA_UNIT_0,&globals_entry) != MEA_OK) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Get_Globals_Entry failed \n",
                         __FUNCTION__);

        return MEA_OK;
    }


#if MEA_NoForwarder
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
    if(!globals_entry.forwarder.disable_fwd_del_on_reinit){
        if (MEA_fwd_tbl_DeleteAll() != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_fwd_tbl_DeleteAll failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }
     }
#endif
#endif

	/* Check if support */
	if (!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
		return MEA_OK;   
	}

    /* Read Forwarder counter from to device to clear it without update the software shadow */
    MEA_OS_memcpy(&save_counters_forwarder,&MEA_Counters_Forwarder,sizeof(save_counters_forwarder));
    if (MEA_API_Collect_Counters_Forwarder(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Collect_Counters_Forwader failed\n",
                          __FUNCTION__);
        MEA_OS_memcpy(&MEA_Counters_Forwarder,&save_counters_forwarder,sizeof(MEA_Counters_Forwarder));
        return MEA_ERROR;
    }
    MEA_OS_memcpy(&MEA_Counters_Forwarder,&save_counters_forwarder,sizeof(MEA_Counters_Forwarder));

   
    

    

	return MEA_OK;

}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <APIs implementation>                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_fwd_tbl_verify_key_IPV6(MEA_SE_Entry_key_dbt  *key_pi, MEA_SE_Entry_data_dbt * data)
{
    MEA_FWD_VPN_dbt VPNentry;
    MEA_Uint32 Ipv6[4];

    if (key_pi == NULL) {
        return MEA_ERROR;
    }
    if (data == NULL) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&VPNentry, 0, sizeof(VPNentry));

    switch (key_pi->type)
    {
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN :
        VPNentry.fwd_vpn = key_pi->SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN;
        VPNentry.fwd_KeyType = key_pi->type;
        MEA_OS_memcpy(&Ipv6[0], &key_pi->SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0], 16);
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        VPNentry.fwd_vpn = key_pi->SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN;
        VPNentry.fwd_KeyType = key_pi->type;
        MEA_OS_memcpy(&Ipv6[0], &key_pi->SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0], 16);
        break;
    default:
        return MEA_OK;
        break;
    }

    
        if (data->static_flag != MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - the entry should be static \n", __FUNCTION__);
            return MEA_ERROR;
        }
       

        if(MEA_API_Exist_FWD_VPN_Ipv6(MEA_UNIT_0, &VPNentry) == MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - the Vpn is not valid \n", __FUNCTION__);
            return MEA_ERROR;
        }
        //check free place for vpn
        if (mea_drv_fwd_vpnIpV6_free_place(VPNentry.fwd_vpn) == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -NO Free pace to Add the Ipv6 \n", __FUNCTION__);
            return MEA_ERROR;
        }
        // check if the Ipv6 Signature is exist on FWD Table.
        if (mea_drv_fwd_vpnIpV6_check_Ipv6key(VPNentry.fwd_vpn, &Ipv6[0]) == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -Ipv6 key is exist \n", __FUNCTION__);
            return MEA_ERROR;
        }

    

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_build_key>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_build_key(MEA_SE_Entry_key_dbt  *key_pi,
     				  		     MEA_Forwarder_Key_dbt *key_po)
{
 
	
    MEA_SE_Forwarding_key_type_te key_type;
    MEA_MacAddr                    mac_addr;
    MEA_Uint16                     vpn;
#ifdef PPP_MAC
    MEA_Uint16                     PPPoE_sessionId;
#endif    
	MEA_Uint16                     vlan;
    MEA_Uint32                     Signature;
	if (key_pi == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - key_pi == NULL \n",__FUNCTION__);
		return MEA_ERROR;
	}

	if (key_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - key_po == NULL \n",__FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memset(key_po,0,sizeof(*key_po));

//    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," before switch  Key type %d \n",key_pi->mac.type);

    key_type = key_pi->type;
	
    switch (key_type) {
    case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
		MEA_OS_memcpy(&(mac_addr.b[0]),
		              &(key_pi->mac_plus_vpn.MAC.b[0]),
					  sizeof(mac_addr.b));
		vpn = (MEA_Uint16)(key_pi->mac_plus_vpn.VPN);

		key_po->mac_plus_vpn.mac_32_47 = 
            ((((MEA_Uint32)((unsigned char)(mac_addr.b[1]))) <<  0) |  
             (((MEA_Uint32)((unsigned char)(mac_addr.b[0]))) <<  8) );

		key_po->mac_plus_vpn.mac_0_31 = 
          ((((MEA_Uint32)((unsigned char)(mac_addr.b[5]))) <<  0) |  
           (((MEA_Uint32)((unsigned char)(mac_addr.b[4]))) <<  8) |
           (((MEA_Uint32)((unsigned char)(mac_addr.b[3]))) << 16) |  
           (((MEA_Uint32)((unsigned char)(mac_addr.b[2]))) << 24) );

		key_po->mac_plus_vpn.vpn = vpn;
        key_po->mac_plus_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		key_po->mac_plus_vpn.valid = 1;

		break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
    case MEA_SE_FORWARDING_KEY_TYPE_DA :
		MEA_OS_memcpy(&(mac_addr.b[0]),
			          &(key_pi->mac.MAC.b[0]),
					  sizeof(mac_addr.b));

        key_po->mac.mac_32_47 = 
            ((((MEA_Uint32)((unsigned char)(mac_addr.b[1]))) <<  0) |  
             (((MEA_Uint32)((unsigned char)(mac_addr.b[0]))) <<  8) );

		key_po->mac.mac_0_31 = 
          ((((MEA_Uint32)((unsigned char)(mac_addr.b[5]))) <<  0) |  
           (((MEA_Uint32)((unsigned char)(mac_addr.b[4]))) <<  8) |
           (((MEA_Uint32)((unsigned char)(mac_addr.b[3]))) << 16) |  
           (((MEA_Uint32)((unsigned char)(mac_addr.b[2]))) << 24) );

		key_po->mac.dont_care1 = 0xfff; // fix bug FWD MAC only

        key_po->mac.key_type = MEA_SE_FORWARDING_KEY_TYPE_DA;
		key_po->mac.valid = 1;

		break;
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :
        key_po->mpls_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN;
        key_po->mpls_vpn.label_1      = (key_pi->mpls_vpn.label_1);
        key_po->mpls_vpn.label_2_lsb  = (key_pi->mpls_vpn.label_2) & 0xfff;
        key_po->mpls_vpn.label_2_msb  = (key_pi->mpls_vpn.label_2>>12) & 0xff;
        key_po->mpls_vpn.vpn          = (key_pi->mpls_vpn.VPN);
        key_po->mpls_vpn.mask_type    = (key_pi->mpls_vpn.mask_type);

        key_po->mpls_vpn.dont_care1   = 0x0;
        
        key_po->vlan_vpn.valid        = 1;

        break;
#ifdef PPP_MAC 
    case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID :
		MEA_OS_memcpy(&(mac_addr.b[0]),
			          &(key_pi->mac_plus_pppoe_session_id.MAC.b[0]),
					  sizeof(mac_addr.b));
		PPPoE_sessionId = (MEA_Uint16)(key_pi->mac_plus_pppoe_session_id.PPPoE_session_id);
          
	    key_po->mac_plus_pppoe_session_id.mac_32_47 = 
            ((((MEA_Uint32)((unsigned char)(mac_addr.b[1]))) <<  0) |  
             (((MEA_Uint32)((unsigned char)(mac_addr.b[0]))) <<  8) );

		key_po->mac_plus_pppoe_session_id.mac_0_31 = 
          ((((MEA_Uint32)((unsigned char)(mac_addr.b[5]))) <<  0) |  
           (((MEA_Uint32)((unsigned char)(mac_addr.b[4]))) <<  8) |
           (((MEA_Uint32)((unsigned char)(mac_addr.b[3]))) << 16) |  
           (((MEA_Uint32)((unsigned char)(mac_addr.b[2]))) << 24) );

		key_po->mac_plus_pppoe_session_id.pppoe_session_id_lsb = PPPoE_sessionId;

        key_po->mac_plus_pppoe_session_id.key_type = MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID;
		key_po->mac_plus_pppoe_session_id.valid = 1;

		break;
#else
	case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
	
		

		key_po->pppoe_session_id_vpn.pppoe_session_id = key_pi->PPPOE_plus_vpn.PPPoE_session_id;
		key_po->pppoe_session_id_vpn.vpn = key_pi->PPPOE_plus_vpn.VPN;
		key_po->pppoe_session_id_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN;
		key_po->pppoe_session_id_vpn.valid = 1;
		break;
#endif

    case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :
	    vlan                        = (MEA_Uint16)(key_pi->vlan_vpn.VLAN);
		vpn                         = (MEA_Uint16)(key_pi->vlan_vpn.VPN);
        key_po->vlan_vpn.vlan_to_enable = key_pi->vlan_vpn.VLAN_to_enable;
        key_po->vlan_vpn.vlan_to_val    = key_pi->vlan_vpn.VLAN_to_val;
		//key_po->vlan_vpn.dont_care0 = 0x0007FFFF;
		key_po->vlan_vpn.dont_care1 = 0;
		key_po->vlan_vpn.vlan       = vlan;
		key_po->vlan_vpn.vpn        = vpn;
        key_po->vlan_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN;
		key_po->vlan_vpn.valid      = 1;

		break;
        case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :
            key_po->oam_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
         key_po->oam_vpn.reserverd_1   = 0;
         
         key_po->oam_vpn.label_1       = key_pi->oam_vpn.label_1;
         key_po->oam_vpn.vpn           = key_pi->oam_vpn.VPN;
         key_po->oam_vpn.MD_level   = key_pi->oam_vpn.MD_level;
         key_po->oam_vpn.op_code    = key_pi->oam_vpn.op_code;
         key_po->oam_vpn.packet_is_untag_mpls  =  key_pi->oam_vpn.packet_is_untag_mpls; 
         key_po->oam_vpn.Src_port              = key_pi->oam_vpn.Src_port;
         key_po->oam_vpn.mask_type             = key_pi->oam_vpn.mask_type;
         key_po->oam_vpn.valid      = 1;
       break;
		
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
       
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4_23_0  = (key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 & 0x00ffffff);
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4_23_8  =((key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4>>8) & 0x0000ffff);
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4_7_0   =((key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4)& 0x000000ff);
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.vpn           = key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.VPN;
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.key_type      = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.valid      = 1;

        break;
        case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
            key_po->DstIPv4_plus_L4DstPort_plus_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
           key_po->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_0_15  = key_pi->DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x0000ffff;
           key_po->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_16_32 = (key_pi->DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 >> 16) & 0x0000ffff;
           key_po->DstIPv4_plus_L4DstPort_plus_vpn.L4Port         = key_pi->DstIPv4_plus_L4_DstPort_plus_vpn.L4port;
           key_po->DstIPv4_plus_L4DstPort_plus_vpn.vpn            = key_pi->DstIPv4_plus_L4_DstPort_plus_vpn.VPN;
           key_po->DstIPv4_plus_L4DstPort_plus_vpn.mask_type      = key_pi->DstIPv4_plus_L4_DstPort_plus_vpn.mask_type;

           key_po->DstIPv4_plus_L4DstPort_plus_vpn.valid          = 1;
            break;
        case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_0_15  = key_pi->SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4 & 0x0000ffff;;
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_16_32 = (key_pi->SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4 >> 16) &  0x0000ffff ;
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.L4Port         = key_pi->SrcIPv4_plus_L4_srcPort_plus_vpn.L4port;
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.vpn            = key_pi->SrcIPv4_plus_L4_srcPort_plus_vpn.VPN;
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.mask_type      = key_pi->SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type;
            
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.valid          = 1;
            break;
        case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
            key_po->DstIPv4_plus_L4DstPort_plus_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
            Signature = mea_fwd_SignatureIpv6(&key_pi->SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0]);

            key_po->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_0_15 = Signature & 0x0000ffff;
            key_po->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_16_32 = (Signature >> 16) & 0x0000ffff;
            
            key_po->DstIPv4_plus_L4DstPort_plus_vpn.L4Port = key_pi->SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port;
            key_po->DstIPv4_plus_L4DstPort_plus_vpn.vpn = key_pi->SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN;
            key_po->DstIPv4_plus_L4DstPort_plus_vpn.mask_type = key_pi->SE_DstIPv6_plus_L4DstPort_plus_vpn.mask_type;

            key_po->DstIPv4_plus_L4DstPort_plus_vpn.valid = 1;
            break;
        case  MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
            
            
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
            Signature = mea_fwd_SignatureIpv6(&key_pi->SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0]);
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_0_15 = Signature & 0x0000ffff;;
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_16_32 = (Signature >> 16) & 0x0000ffff;
            
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.L4Port = key_pi->SE_SrcIPv6_plus_L4_srcPort_plus_vpn.L4port;
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.vpn = key_pi->SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN;
            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.mask_type = key_pi->SE_SrcIPv6_plus_L4_srcPort_plus_vpn.mask_type;

            key_po->SrcIPv4_plus_L4srcPort_plus_vpn.valid = 1;


            break;

	case MEA_SE_FORWARDING_KEY_TYPE_LAST :
    default: 
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - error1 Unknown key type (%d)\n", __FUNCTION__,key_type);
		return MEA_ERROR;
    }
#if 0
	if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000) {
		key_po->vlan_vpn.key_type <<= 1;
	}
#endif
	/* Return to caller */
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_build_data>                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_build_data(MEA_Unit_t                unit_i,
								  MEA_SE_Entry_data_dbt    *data_pi,
     				  		      MEA_Forwarder_Data_dbt   *data_po)
{

	ENET_xPermissionId_t      xPermissionId;
    MEA_db_HwId_t          action_hwId;
    MEA_db_HwUnit_t        action_hwUnit;
    MEA_Action_HwEntry_dbt  action_info;
	MEA_Uint32 count_shift;

	if (data_pi == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - data_pi == NULL \n",__FUNCTION__);
		return MEA_ERROR;
	}

	if (data_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - data_po == NULL \n",__FUNCTION__);
		return MEA_ERROR;
	}

	MEA_OS_memset((char*)data_po,0,sizeof(*data_po));
    
    if(ENET_queue_check_OutPorts(unit_i,&data_pi->OutPorts) !=ENET_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
         "%s - ENET_queue_check_OutPorts Fail \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    



    /* Get xPermissionId */ 
    if (! MEA_API_Get_Warm_Restart()){
	    xPermissionId = ENET_PLAT_GENERATE_NEW_ID;
    }else{
        xPermissionId=  data_pi->save_hw_xPermissionId;
    }

    if (enet_Create_xPermission(unit_i,
		                        &(data_pi->OutPorts),
                                0,/*no cpu*/
								&xPermissionId) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - enet_Create_xPermission failed \n", __FUNCTION__);
		return MEA_ERROR;
	}
      data_pi->save_hw_xPermissionId = xPermissionId;

	if (data_pi->actionId_valid) {

		if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000)  {
			action_hwUnit = MEA_HW_UNIT_ID_GENERAL;
		} else {
            MEA_Uint32 direction;
            if (mea_Action_GetDirection (unit_i,
                                         data_pi->actionId,
                                         &direction) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_Action_GetDirection failed (actionId=%d)\n",
                                  __FUNCTION__,
                                  data_pi->actionId);
                return MEA_ERROR;
            }
            switch (direction) {
            case MEA_DIRECTION_GENERAL:
            case MEA_DIRECTION_DOWNSTREAM:
			action_hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
                break;
            case MEA_DIRECTION_UPSTREAM:
                action_hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
                break;
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - unknown direction %d (action_id)\n",
                                  __FUNCTION__,
                                  direction,
                                  data_pi->actionId);
                return MEA_ERROR;
            }
		}
		MEA_DRV_GET_HW_ID(unit_i,
			              mea_drv_Get_Action_db,
						  data_pi->actionId,
						  action_hwUnit,
						  action_hwId,
					      return MEA_ERROR;); 

	} else {
		action_hwId = 0;
	}

#ifdef MEA_NEW_FWD_ROW_SET 
		if (data_pi->actionId_valid) {
			if (mea_Action_GetHw(unit_i,
								 data_pi->actionId,
								 &action_info) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - mea_Action_GetHw failed\n", 
								  __FUNCTION__);
				return MEA_ERROR;
			}
		} else {
			MEA_OS_memset(&action_info,0,sizeof(action_info));

		}

		count_shift = 0;

  	    MEA_OS_insert_value(count_shift, MEA_FWD_DATA_STATIC_WIDTH, (MEA_Uint32)(data_pi->static_flag), &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_STATIC_WIDTH;

  	    MEA_OS_insert_value(count_shift,MEA_FWD_DATA_AGE_WIDTH,(MEA_Uint32)(data_pi->aging),&(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_AGE_WIDTH;

  	    MEA_OS_insert_value(count_shift,MEA_FWD_DATA_XPERMISSION_ID_WIDTH,(MEA_Uint32)(xPermissionId),&(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;

		MEA_OS_insert_value(count_shift, MEA_FWD_DATA_RESERV_WIDTH, (MEA_Uint32)(0), &(data_po->val[0]));
		count_shift += MEA_FWD_DATA_RESERV_WIDTH;

		MEA_OS_insert_value(count_shift, MEA_FWD_DATA_SA_DISCARD_WIDTH, (MEA_Uint32)(data_pi->sa_discard), &(data_po->val[0]));
		count_shift += MEA_FWD_DATA_SA_DISCARD_WIDTH;
  	

        MEA_OS_insert_value(count_shift, MEA_FWD_DATA_ACCESS_PORT_WIDTH,(MEA_Uint32)(data_pi->access_port ),&(data_po->val[0]));
		count_shift += MEA_FWD_DATA_ACCESS_PORT_WIDTH;


		MEA_OS_insert_value(count_shift,MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,(MEA_Uint32)(data_pi->actionId_valid),&(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;

		MEA_OS_insert_value(count_shift,MEA_FWD_DATA_ACTION_ID_WIDTH,(MEA_Uint32)(action_hwId), &(data_po->val[0]));
        count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;



		MEA_OS_insert_value(count_shift,MEA_FWD_DATA_LIMITER_ID_WIDTH,(MEA_Uint32)((data_pi->limiterId_valid)  ? data_pi->limiterId : MEA_LIMITER_DONT_CARE), &(data_po->val[0]));
        count_shift+= MEA_FWD_DATA_LIMITER_ID_WIDTH;

		MEA_OS_insert_value(count_shift,MEA_FWD_DATA_RESERV1_WIDTH, (MEA_Uint32)(0), &(data_po->val[0]));
		count_shift += MEA_FWD_DATA_RESERV1_WIDTH;



       
        MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_PARITY_WIDTH,
                            (MEA_Uint32)(0),
                            &(data_po->val[0]));
        count_shift+= MEA_FWD_DATA_PARITY_WIDTH;
#else

		if (data_pi->actionId_valid) {
			if (mea_Action_GetHw(unit_i,
								 data_pi->actionId,
								 &action_info) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					              "%s - mea_Action_GetHw failed\n", 
								  __FUNCTION__);
				return MEA_ERROR;
			}
		} else {
			MEA_OS_memset(&action_info,0,sizeof(action_info));
			action_info.cos    = 7;
			action_info.l2_pri = 7;
			action_info.color  = 2;
			action_info.mc_fid_skip = MEA_TRUE;
		}

		count_shift = 0;

  	    MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_STATIC_WIDTH,
                            (MEA_Uint32)(data_pi->static_flag),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_STATIC_WIDTH;

  	    MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_AGE_WIDTH,
                            (MEA_Uint32)(data_pi->aging),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_AGE_WIDTH;

  	    MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_XPERMISSION_ID_WIDTH,
                            (MEA_Uint32)(xPermissionId),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;

  	    MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH,
							(MEA_Uint32)(action_info.mc_fid_skip), 
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH;
#if 0
  	    MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_MC_EDIT_ID_WIDTH,
							(MEA_Uint32)(action_info.mc_fid_edit), 
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_MC_EDIT_ID_WIDTH;
#else

        MEA_OS_insert_value(count_shift,
            MEA_FWD_DATA_ACCESS_PORT_BIT0_2,
            (MEA_Uint32)(data_pi->access_port & 0x7),
            &(data_po->val[0]));
        count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT0_2;

#endif

  	    MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_SA_DISCARD_WIDTH,
                            (MEA_Uint32)(data_pi->sa_discard),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_SA_DISCARD_WIDTH;

		MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_COS_FORCE_WIDTH,
                            (MEA_Uint32)(action_info.cos_force),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_COS_FORCE_WIDTH;

		MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_COS_WIDTH,
                            (MEA_Uint32)(action_info.cos),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_COS_WIDTH;

		MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_PRI_FORCE_WIDTH,
                            (MEA_Uint32)(action_info.l2_pri_force),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_PRI_FORCE_WIDTH;

		MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_PRI_WIDTH,
                            (MEA_Uint32)(action_info.l2_pri),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_PRI_WIDTH;

		MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_COL_FORCE_WIDTH,
                            (MEA_Uint32)(action_info.color_force),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_COL_FORCE_WIDTH; 

		MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_COL_WIDTH,
                            (MEA_Uint32)(action_info.color),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_COL_WIDTH;

		MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,
                            (MEA_Uint32)(data_pi->actionId_valid),
                            &(data_po->val[0]));
        count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;

		MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_ACTION_ID_WIDTH,
                            (MEA_Uint32)(action_hwId),
                            &(data_po->val[0]));
        count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;
#if 0
        MEA_OS_insert_value(count_shift,
                                MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH,
                                (MEA_Uint32)(data_pi->cfm_oam_me_level_valid),
                                &(data_po->val[0]));
        count_shift+= MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH;

        MEA_OS_insert_value(count_shift,
                                MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_WIDTH,
                                (MEA_Uint32)(data_pi->cfm_oam_me_level_value),
                                &(data_po->val[0]));
        count_shift+= MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_WIDTH;
#else
        MEA_OS_insert_value(count_shift,
            MEA_FWD_DATA_ACCESS_PORT_BIT3_7,
            (MEA_Uint32)(((data_pi->access_port) >> 3) & 0x1f),  /**/
            &(data_po->val[0]));
        count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT3_7;


#endif


		MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_LIMITER_ID_WIDTH,
                            (MEA_Uint32)((data_pi->limiterId_valid) 
							             ? data_pi->limiterId : MEA_LIMITER_DONT_CARE),
                            &(data_po->val[0]));
        count_shift+= MEA_FWD_DATA_LIMITER_ID_WIDTH;
#if 0
       MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_RESERVE_BITS_WIDTH,
                            (MEA_Uint32)(0),
                            &(data_po->val[0]));
        count_shift+= MEA_FWD_DATA_RESERVE_BITS_WIDTH;
#else
        MEA_OS_insert_value(count_shift,
            MEA_FWD_DATA_ACCESS_PORT_BIT_8,
            (MEA_Uint32)((data_pi->access_port>>8) & 0x1),
            &(data_po->val[0]));
        count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT_8;
#endif


       
        MEA_OS_insert_value(count_shift,
                            MEA_FWD_DATA_PARITY_WIDTH,
                            (MEA_Uint32)(0),
                            &(data_po->val[0]));
        count_shift+= MEA_FWD_DATA_PARITY_WIDTH;
#endif //MEA_NEW_FWD_ROW_SET 



	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <mea_fwd_convert_to_SE_Entry>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_convert_to_SE_Entry(MEA_Unit_t               unit_i,
										   MEA_Forwarder_Key_dbt   *key_pi,
                                           MEA_Forwarder_Data_dbt  *data_pi,
										   MEA_SE_Entry_dbt	       *entry_po,
										   MEA_SETTING_TYPE_te type) 
{

   MEA_db_HwUnit_t action_hwUnit;
   MEA_db_HwId_t   action_hwId;
   MEA_Action_t    actionId;
   MEA_Bool        action_exist;
   MEA_Limiter_t   limiterId;


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
   if (key_pi == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - key_pi == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
#if 0
   if (data_pi == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - data_pi == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
#endif
   if (entry_po == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - entry_po == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   /* Erase the entry output */
   MEA_OS_memset(entry_po,0,sizeof(*entry_po));
#if 0
   if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000) {
	   key_pi->raw.key_type >>= 1;
   }
#endif

   
   /* Build the entry key */
   entry_po->key.type = key_pi->raw.key_type;
   switch (key_pi->raw.key_type) {
    case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
		entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		entry_po->key.mac_plus_vpn.VPN      =            key_pi->mac_plus_vpn.vpn;
		entry_po->key.mac_plus_vpn.MAC.b[0] = ((MEA_Uint8)((key_pi->mac_plus_vpn.mac_32_47 & 0x0000FF00) >>  8));
		entry_po->key.mac_plus_vpn.MAC.b[1] = ((MEA_Uint8)((key_pi->mac_plus_vpn.mac_32_47 & 0x000000FF) >>  0));
		entry_po->key.mac_plus_vpn.MAC.b[2] = ((MEA_Uint8)((key_pi->mac_plus_vpn.mac_0_31  & 0xFF000000) >> 24));
		entry_po->key.mac_plus_vpn.MAC.b[3] = ((MEA_Uint8)((key_pi->mac_plus_vpn.mac_0_31  & 0x00FF0000) >> 16));
		entry_po->key.mac_plus_vpn.MAC.b[4] = ((MEA_Uint8)((key_pi->mac_plus_vpn.mac_0_31  & 0x0000FF00) >>  8));
		entry_po->key.mac_plus_vpn.MAC.b[5] = ((MEA_Uint8)((key_pi->mac_plus_vpn.mac_0_31  & 0x000000FF) >>  0));
		break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
   case MEA_SE_FORWARDING_KEY_TYPE_DA :
		entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_DA;
		entry_po->key.mac.MAC.b[0] = ((MEA_Uint8)((key_pi->mac.mac_32_47 & 0x0000FF00) >>  8));
		entry_po->key.mac.MAC.b[1] = ((MEA_Uint8)((key_pi->mac.mac_32_47 & 0x000000FF) >>  0));
		entry_po->key.mac.MAC.b[2] = ((MEA_Uint8)((key_pi->mac.mac_0_31  & 0xFF000000) >> 24));
		entry_po->key.mac.MAC.b[3] = ((MEA_Uint8)((key_pi->mac.mac_0_31  & 0x00FF0000) >> 16));
		entry_po->key.mac.MAC.b[4] = ((MEA_Uint8)((key_pi->mac.mac_0_31  & 0x0000FF00) >>  8));
		entry_po->key.mac.MAC.b[5] = ((MEA_Uint8)((key_pi->mac.mac_0_31  & 0x000000FF) >>  0));
		break;
#endif
   case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :		   
       entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN;
       entry_po->key.mpls_vpn.label_1 = key_pi->mpls_vpn.label_1;
       entry_po->key.mpls_vpn.label_2 = key_pi->mpls_vpn.label_2_lsb;
       entry_po->key.mpls_vpn.label_2 |= ((key_pi->mpls_vpn.label_2_msb & 0xFF)<<12);
       entry_po->key.mpls_vpn.VPN     = key_pi->mpls_vpn.vpn;
       entry_po->key.mpls_vpn.mask_type = key_pi->mpls_vpn.mask_type;

       break;
#ifdef PPP_MAC 
   case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID :
	    entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID;
		entry_po->key.mac_plus_pppoe_session_id.PPPoE_session_id  =   key_pi->mac_plus_pppoe_session_id.pppoe_session_id_lsb;
		entry_po->key.mac_plus_pppoe_session_id.MAC.b[0] = ((MEA_Uint8)((key_pi->mac_plus_pppoe_session_id.mac_32_47 & 0x0000FF00) >>  8));
		entry_po->key.mac_plus_pppoe_session_id.MAC.b[1] = ((MEA_Uint8)((key_pi->mac_plus_pppoe_session_id.mac_32_47 & 0x000000FF) >>  0));
		entry_po->key.mac_plus_pppoe_session_id.MAC.b[2] = ((MEA_Uint8)((key_pi->mac_plus_pppoe_session_id.mac_0_31  & 0xFF000000) >> 24));
		entry_po->key.mac_plus_pppoe_session_id.MAC.b[3] = ((MEA_Uint8)((key_pi->mac_plus_pppoe_session_id.mac_0_31  & 0x00FF0000) >> 16));
		entry_po->key.mac_plus_pppoe_session_id.MAC.b[4] = ((MEA_Uint8)((key_pi->mac_plus_pppoe_session_id.mac_0_31  & 0x0000FF00) >>  8));
		entry_po->key.mac_plus_pppoe_session_id.MAC.b[5] = ((MEA_Uint8)((key_pi->mac_plus_pppoe_session_id.mac_0_31  & 0x000000FF) >>  0));
  	    break;
#else
   case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
	   entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN;
	   entry_po->key.PPPOE_plus_vpn.PPPoE_session_id = key_pi->pppoe_session_id_vpn.pppoe_session_id;
	   entry_po->key.PPPOE_plus_vpn.VPN = key_pi->pppoe_session_id_vpn.vpn;
   
	   break;
#endif
	case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :		   
		entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN;
		entry_po->key.vlan_vpn.VLAN = key_pi->vlan_vpn.vlan;
		entry_po->key.vlan_vpn.VPN  = key_pi->vlan_vpn.vpn;
  	    break;

	case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :		   
		entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
		entry_po->key.oam_vpn.label_1 = key_pi->oam_vpn.label_1;
		entry_po->key.oam_vpn.VPN  = key_pi->oam_vpn.vpn;
        entry_po->key.oam_vpn.MD_level  = key_pi->oam_vpn.MD_level;
        entry_po->key.oam_vpn.op_code  = key_pi->oam_vpn.op_code;
        entry_po->key.oam_vpn.packet_is_untag_mpls  = key_pi->oam_vpn.packet_is_untag_mpls;
        entry_po->key.oam_vpn.Src_port = key_pi->oam_vpn.Src_port;
        entry_po->key.oam_vpn.mask_type = key_pi->oam_vpn.mask_type;

        entry_po->key.oam_vpn.reserverd_1  = 0;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
        entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
        entry_po->key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 = key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4_23_0;
        entry_po->key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4 = (key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4_23_8<<8) | (key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4_7_0 & 0xff);
        entry_po->key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN  = key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.vpn;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
    {
        MEA_FWD_VPN_dbt vpn_entry;
        //check if the VPN IS for IPV6
        MEA_OS_memset(&vpn_entry, 0, sizeof(vpn_entry));

        vpn_entry.fwd_vpn = key_pi->DstIPv4_plus_L4DstPort_plus_vpn.vpn;
        vpn_entry.fwd_KeyType = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
        if (MEA_API_Exist_FWD_VPN_Ipv6(unit_i, &vpn_entry) == MEA_TRUE) {
            //  sig to IPV6
            if (mea_drv_fwd_vpnIpV6_find_Ipv6key(key_pi->DstIPv4_plus_L4DstPort_plus_vpn.vpn,
                ((key_pi->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_16_32 << 16) | key_pi->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_0_15),
                &entry_po->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0]) != MEA_OK) {
                // Ipv6 not on Database
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - IPV6 not on database  sig  0x%08x \n",
                    __FUNCTION__ ,
                    ((key_pi->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_16_32 << 16) | key_pi->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_0_15) );
                                
            }
            entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
            entry_po->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port = key_pi->DstIPv4_plus_L4DstPort_plus_vpn.L4Port;
            entry_po->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN    = key_pi->DstIPv4_plus_L4DstPort_plus_vpn.vpn;
            entry_po->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.mask_type = key_pi->DstIPv4_plus_L4DstPort_plus_vpn.mask_type;




        }
        else {
           
            //IPV4
            entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
            entry_po->key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 = key_pi->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_0_15;
            entry_po->key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 |= (key_pi->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_16_32 << 16);
            entry_po->key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port = key_pi->DstIPv4_plus_L4DstPort_plus_vpn.L4Port;
            entry_po->key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = key_pi->DstIPv4_plus_L4DstPort_plus_vpn.vpn;
            entry_po->key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type = key_pi->DstIPv4_plus_L4DstPort_plus_vpn.mask_type;
        }
     }
        
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
    {
        MEA_FWD_VPN_dbt vpn_entry;
        MEA_OS_memset(&vpn_entry, 0, sizeof(vpn_entry));

        vpn_entry.fwd_vpn = key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.vpn;
        vpn_entry.fwd_KeyType = MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
        if (MEA_API_Exist_FWD_VPN_Ipv6(unit_i, &vpn_entry) == MEA_TRUE) {
            //  sig to IPV6
            if (mea_drv_fwd_vpnIpV6_find_Ipv6key(vpn_entry.fwd_vpn,
                ((key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_16_32 << 16) | key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_0_15),
                &entry_po->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0]) != MEA_OK) {
                // Ipv6 not on Database
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - IPV6 not on database  sig  0x%08x \n",
                    __FUNCTION__,
                    ((key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_16_32 << 16) | key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_0_15));

            }
            entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
            entry_po->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.L4port = key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.L4Port;
            entry_po->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN = key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.vpn;
            entry_po->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.mask_type = key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.mask_type;




        }
        else {
            //IPv4
            entry_po->key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
            entry_po->key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4 = key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_0_15;
            entry_po->key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4 |= (key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_16_32 << 16);
            entry_po->key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port = key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.L4Port;
            entry_po->key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN = key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.vpn;
            entry_po->key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type = key_pi->SrcIPv4_plus_L4srcPort_plus_vpn.mask_type;
        }
    }
        break;
    
    
     //case MEA_SE_FORWARDING_KEY_TYPE_LAST :
	 default: 
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - error2 unknown key type (%d) \n",
                          __FUNCTION__,
						  key_pi->raw.key_type);
	    return MEA_ERROR;
		break;
	}

#ifdef MEA_NEW_FWD_ROW_SET     
 
   if(data_pi != NULL){
	/* build the entry data */
    entry_po->data.actionId_valid = MEA_OS_read_value(MEA_FWD_DATA_ACTION_ID_VALID_START, MEA_FWD_DATA_ACTION_ID_VALID_WIDTH, &(data_pi->val[0]));
    if (entry_po->data.actionId_valid) {
		if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000)  {
			action_hwUnit = MEA_HW_UNIT_ID_GENERAL;
		} else {
			action_hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
		}
        action_hwId = (MEA_db_HwId_t)(MEA_OS_read_value(MEA_FWD_DATA_ACTION_ID_START,MEA_FWD_DATA_ACTION_ID_WIDTH,&(data_pi->val[0])));

        //this is patch when the action is bigger then 4K
        if (entry_po->data.actionId_valid)
        {
#ifdef MEA_NEW_FWD_ROW_SET
			// DBG
			//action_hwId = action_hwId; //+ MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE);
#else
			action_hwId = action_hwId + MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE);
#endif

        }


        MEA_DRV_GET_SW_ID_WITH_EXIST(unit_i,
                                     mea_drv_Get_Action_db,
                                     action_hwUnit,
                                     action_hwId,
                                     actionId,
                                     action_exist,
                                     return MEA_ERROR);
       
        if (!action_exist) {
			if( type != MEA_SETTING_TYPE_GET){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - not found SW id for action hwUnit (%d) / hwId (%d) \n",
                              __FUNCTION__,
                              action_hwUnit,
                              action_hwId);
            return MEA_ERROR;
			}else{
				entry_po->data.actionId_valid=MEA_FALSE;
                actionId = action_hwId;
                entry_po->data.actionId_err = MEA_TRUE;
                
			}
        }
   } else {
	   actionId = 0;
   }
	
	entry_po->data.valid = MEA_TRUE;
	entry_po->data.actionId       = actionId;

	entry_po->data.static_flag = MEA_OS_read_value(MEA_FWD_DATA_STATIC_START, MEA_FWD_DATA_STATIC_WIDTH, &(data_pi->val[0]));
	entry_po->data.aging          = MEA_OS_read_value(MEA_FWD_DATA_AGE_START, MEA_FWD_DATA_AGE_WIDTH,&(data_pi->val[0]));
	entry_po->data.xPermissionId  = (ENET_xPermissionId_t)(MEA_OS_read_value(MEA_FWD_DATA_XPERMISSION_ID_START,MEA_FWD_DATA_XPERMISSION_ID_WIDTH,&(data_pi->val[0])));
	
	entry_po->data.sa_discard     = MEA_OS_read_value(MEA_FWD_DATA_SA_DISCARD_START,MEA_FWD_DATA_SA_DISCARD_WIDTH,&(data_pi->val[0]));
    entry_po->data.access_port    = MEA_OS_read_value(MEA_FWD_DATA_ACCESS_PORT_START,MEA_FWD_DATA_ACCESS_PORT_WIDTH,&(data_pi->val[0]));

  



	
	limiterId = (MEA_Limiter_t)MEA_OS_read_value((MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_START,
											     (MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_WIDTH,
												 &(data_pi->val[0]));

	if (limiterId == MEA_LIMITER_DONT_CARE) {
	    entry_po->data.limiterId_valid = 0;
	    entry_po->data.limiterId       = 0;
	} else {
	    entry_po->data.limiterId_valid = MEA_TRUE;
		entry_po->data.limiterId       = limiterId;
		
	}


    
   
    
   //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"FWD xpermisionId =%d\n",entry_po->data.xPermissionId);
   /* Get the entry output ports */
    if (enet_IsValid_xPermission(unit_i, entry_po->data.xPermissionId, MEA_TRUE) == MEA_TRUE) {
        if (enet_Get_xPermission(ENET_UNIT_0,
            entry_po->data.xPermissionId,
            &(entry_po->data.OutPorts)) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Get_xPermission failed (id=%d) \n",
                __FUNCTION__,
                entry_po->data.xPermissionId);
            return MEA_ERROR;
        }
    }
    else {
        entry_po->data.xPermissionId_err = MEA_TRUE;
    }
#else
   if(data_pi != NULL){
	/* build the entry data */
    entry_po->data.actionId_valid = 
	   MEA_OS_read_value(MEA_FWD_DATA_ACTION_ID_VALID_START,
                         MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,
						 &(data_pi->val[0]));
    if (entry_po->data.actionId_valid) {
		if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000)  {
			action_hwUnit = MEA_HW_UNIT_ID_GENERAL;
		} else {
			action_hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
		}
        action_hwId = (MEA_db_HwId_t)(MEA_OS_read_value(MEA_FWD_DATA_ACTION_ID_START,
														MEA_FWD_DATA_ACTION_ID_WIDTH,
														&(data_pi->val[0])));
		//if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
		//	action_hwId &= 0x000001ff;
		//}

#ifdef MEA_ENET4000_FWD_OFFSET_ENABLE // T.B.D The FPGA add always the base by itself for ENET4000
		if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000)  {
   		    action_hwId += MEA_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_START(action_hwUnit);
        }
#endif
        //this is patch when the action is bigger then 4K
        if (entry_po->data.actionId_valid)
        {

            action_hwId = action_hwId + MEA_Platform_GetMaxNumOfActionsByType(MEA_ACTION_TYPE_SERVICE);
        }


        MEA_DRV_GET_SW_ID_WITH_EXIST(unit_i,
                                     mea_drv_Get_Action_db,
                                     action_hwUnit,
                                     action_hwId,
                                     actionId,
                                     action_exist,
                                     return MEA_ERROR);
       
        if (!action_exist) {
			if( type != MEA_SETTING_TYPE_GET){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - not found SW id for action hwUnit (%d) / hwId (%d) \n",
                              __FUNCTION__,
                              action_hwUnit,
                              action_hwId);
            return MEA_ERROR;
			}else{
				entry_po->data.actionId_valid=MEA_FALSE;
                
			}
        }
   } else {
	   actionId = 0;
   }
	

	entry_po->data.actionId       = actionId;
	entry_po->data.aging          = 
		   MEA_OS_read_value(MEA_FWD_DATA_AGE_START,
                         MEA_FWD_DATA_AGE_WIDTH,
						 &(data_pi->val[0]));
	entry_po->data.static_flag    = 
		   MEA_OS_read_value(MEA_FWD_DATA_STATIC_START,
                         MEA_FWD_DATA_STATIC_WIDTH,
						 &(data_pi->val[0]));
	entry_po->data.sa_discard     = 
		   MEA_OS_read_value(MEA_FWD_DATA_SA_DISCARD_START,
                         MEA_FWD_DATA_SA_DISCARD_WIDTH,
						 &(data_pi->val[0]));

#if 0
    entry_po->data.cfm_oam_me_level_valid     = 
	       MEA_OS_read_value(MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_START,
                         MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH,
					     &(data_pi->val[0]));
    entry_po->data.cfm_oam_me_level_value     = 
	       MEA_OS_read_value(MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_START,
                         MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_WIDTH,
					     &(data_pi->val[0]));
#endif
    /* read accses_port*/
    
    entry_po->data.access_port = MEA_OS_read_value(MEA_FWD_DATA_ACCESS_PORT_BIT0_2_START,
            MEA_FWD_DATA_ACCESS_PORT_BIT0_2,
            &(data_pi->val[0]));

    entry_po->data.access_port |= (MEA_OS_read_value(MEA_FWD_DATA_ACCESS_PORT_BIT3_7_START,
        MEA_FWD_DATA_ACCESS_PORT_BIT3_7,
        &(data_pi->val[0])) )<<3   ;
    entry_po->data.access_port |= (MEA_OS_read_value(MEA_FWD_DATA_ACCESS_PORT_BIT_8_START,
        MEA_FWD_DATA_ACCESS_PORT_BIT_8,
        &(data_pi->val[0]))) << 8;



	entry_po->data.valid          = MEA_TRUE;
	limiterId = (MEA_Limiter_t)MEA_OS_read_value((MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_START,
											     (MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_WIDTH,
												 &(data_pi->val[0]));

	if (limiterId == MEA_LIMITER_DONT_CARE) {
	    entry_po->data.limiterId_valid = 0;
	    entry_po->data.limiterId       = 0;
	} else {
	    entry_po->data.limiterId_valid = MEA_TRUE;
		entry_po->data.limiterId       = limiterId;
		
	}


    
   entry_po->data.xPermissionId =(ENET_xPermissionId_t)(MEA_OS_read_value(MEA_FWD_DATA_XPERMISSION_ID_START,
        MEA_FWD_DATA_XPERMISSION_ID_WIDTH,
        &(data_pi->val[0])));
    
   //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"FWD xpermisionId =%d\n",entry_po->data.xPermissionId);
   /* Get the entry output ports */
    if (enet_Get_xPermission (ENET_UNIT_0,
	                          entry_po->data.xPermissionId,
	      					  &(entry_po->data.OutPorts)) != ENET_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	 	                 "%s - enet_Get_xPermission failed (id=%d) \n",
		                 __FUNCTION__,
						  entry_po->data.xPermissionId);
	   return MEA_ERROR;
    }
#endif //MEA_NEW_FWD_ROW_SET 
}










	/* Return to caller */
	return MEA_OK;
}






MEA_Status MEA_fwd_key_convert_to_SE_key_Entry(MEA_Uint64              info_key,
                                               MEA_SE_Entry_key_dbt    *key_po)
{
MEA_SE_Entry_dbt entry_po;

MEA_Forwarder_Signature_dbt   Signature_key;

Signature_key.regs[0]= info_key.v[0];
Signature_key.regs[1]= info_key.v[1];
Signature_key.regs[2]= 0;


    if( MEA_fwd_tbl_convert_to_SE_Entry(MEA_UNIT_0,
                                    &Signature_key.val.key,
                                    NULL,
                                    &entry_po,
                                    MEA_SETTING_TYPE_GET)!= MEA_OK){

    }

  MEA_OS_memcpy(key_po,&entry_po.key,sizeof(*key_po));


return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Create_SE_Entry>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_SE_Entry     (MEA_Unit_t		   unit_i,
                                        MEA_SE_Entry_dbt  *entry_pi)
{

    MEA_Forwarder_cmd_dbt          fwd_cmd;
    MEA_Bool                       exist;
 
   MEA_API_LOG
   

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s \n- Forwarder not support  \n",
               __FUNCTION__);
           return MEA_ERROR;
    }
    /* Check for valid entry parameter */
    if (entry_pi == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

	/* Check valid ActionId */
	if (entry_pi->data.actionId_valid) {
	if (MEA_API_IsExist_Action(unit_i,
		                       entry_pi->data.actionId,
                               &exist) == MEA_ERROR){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_IsExist_Action failed (id=%d)\n",
						 __FUNCTION__,entry_pi->data.actionId);
	   return MEA_ERROR;
	}
	if(!exist){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - ActionId %d does not exist  \n",
						 __FUNCTION__,entry_pi->data.actionId);
	   return MEA_ERROR;

	}
	}

 

	/* Check valid LimiterId */
	if (entry_pi->data.limiterId_valid) {
	   if (MEA_API_IsExist_Limiter_ById  (unit_i,
                                          entry_pi->data.limiterId, 
                                          &exist) == MEA_ERROR){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - MEA_API_IsExist_Limiter_ById failed (id=%d) \n",
			    			 __FUNCTION__,entry_pi->data.limiterId);
	      return MEA_ERROR;
	   }
	   if(!exist){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - LimiterId %d does not exist  \n",
                            __FUNCTION__,entry_pi->data.limiterId);
	      return MEA_ERROR;
	   }
  	}

    /***************************
     *	if the Key is MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN
     *  verify if VPN exist
     *  verify if we have place
     *  check if the function sign IPv6 give exist  
     */
    if (MEA_fwd_tbl_verify_key_IPV6(&(entry_pi->key), &entry_pi->data) != MEA_OK) {
        return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


	/* Erase the fwd_cmd structure */
    MEA_OS_memset(&fwd_cmd,       0 , sizeof(MEA_Forwarder_cmd_dbt));
#if 0
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," before entry_pi->key.type                 type %d \n",entry_pi->key.type);
     
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " before MEA_fwd_tbl_build_key DstIPv4_plus_L4_DstPort_plus_vpn mask_type  %d \n", 
         entry_pi->key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type);


#endif
	/* Build signature key */
	if (MEA_fwd_tbl_build_key(&(entry_pi->key),
		                      &(fwd_cmd.cmd.val.key.add_key.key)) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - error1 MEA_fwd_tbl_build_key failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

#if 0
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " after fwd_cmd.cmd.val.key.add_key.key.DstIPv4_plus_L4DstPort_plus_vpn.mask_type  %d\n",
        fwd_cmd.cmd.val.key.add_key.key.DstIPv4_plus_L4DstPort_plus_vpn.mask_type);

#endif


    /* Build the data for the new update entry */
	if (MEA_fwd_tbl_build_data(unit_i,
		                       &(entry_pi->data),
		                       &(fwd_cmd.cmd.val.data)) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - MEA_fwd_tbl_build_data \n",
							  __FUNCTION__);
			return MEA_ERROR;
	}

    /* Add the entry */
    fwd_cmd.cmd.val.command            = MEA_FWD_TABLE_ADD_CMD;
    
    if (entry_pi->key.type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN || 
        entry_pi->key.type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN)
        fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_UPDATE_TBL_ID(entry_pi->key.type - 10); //
    else
        fwd_cmd.cmd.val.table_id           = MEA_FWD_TABLE_ADD_TBL_ID(entry_pi->key.type);
    
    fwd_cmd.cmd.val.priority           = MEA_FWD_TABLE_ADD_PRI;
	fwd_cmd.cmd.val.key.add_key.addCmd = 1;
	if (MEA_fwd_tbl_WriteByKey(&fwd_cmd) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_WriteByKey failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
    }
    
    if ((entry_pi->key.type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN) ||
        (entry_pi->key.type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN) ) 
    {
        
        // save to DataBase FWDIpv6
        if(mea_drv_fwd_vpnIpV6_Add_Ipv6_DB(entry_pi) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_fwd_vpnIpV6_Add_Ipv6_DB failed \n",
                __FUNCTION__);
        }
    
    }

    if (entry_pi->key.type == MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN)
    {
        if(entry_pi->data.static_flag == MEA_TRUE)
        {
            if ((entry_pi->key.mac_plus_vpn.MAC.b[0] & 0x1) == 0)
               meaGlobal_static_MAC.val++;

            if((entry_pi->key.mac_plus_vpn.MAC.b[0] & 0x1) == 1)
                meaGlobal_multicast_static_MAC.val++;

        }

        


    }
    
    
    return MEA_OK;
									     
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Delete_SE_Entry>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_SE_Entry    (MEA_Unit_t            unit_i,
                                       MEA_SE_Entry_key_dbt *key_pi)
{								
   MEA_Forwarder_cmd_dbt    fwd_cmd;
   MEA_Forwarder_Result_dbt result;
   MEA_Uint32               count_shift;
   MEA_SE_Entry_dbt              get_entry;

   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
       
       if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s \n- Forwarder not support  \n",
               __FUNCTION__);
           return MEA_ERROR;
           }
    /* Check for valid entry parameter */
    if (key_pi == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_pi == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	/* Erase the fwd_cmd structure */
    MEA_OS_memset(&fwd_cmd,       0 , sizeof(MEA_Forwarder_cmd_dbt));
   

	/* Build signature key */
	if (MEA_fwd_tbl_build_key(key_pi,
		                      &(fwd_cmd.cmd.val.key.delete_key.key)) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_build_key failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Get the current entry */
    if ( MEA_fwd_tbl_readByKey( &(fwd_cmd.cmd.val.key.delete_key.key),
                                &result) != MEA_OK )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_readByKey failed \n", __FUNCTION__);
        return MEA_ERROR;
    }

	/* Check if we found the entry */
    if (!result.val.info.match) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Entry not found\n", __FUNCTION__);
        return MEA_ERROR;
    }

/**/
    MEA_OS_memset(&get_entry,0,sizeof(get_entry));
    MEA_OS_memcpy(&get_entry.key,key_pi,sizeof(get_entry.key));
    if(MEA_API_Get_SE_Entry        (unit_i,&get_entry) != MEA_OK){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                  "%s - MEA_API_Get_SE_Entry failed \n", __FUNCTION__);
        return MEA_ERROR;
    }

/**/

	/* Set the delete command */
    fwd_cmd.cmd.val.command  = MEA_FWD_TABLE_DEL_CMD;
    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," key_pi->type  %d \r\n", key_pi->type);


    if(key_pi->type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN ||
        key_pi->type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN)
        fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_UPDATE_TBL_ID(key_pi->type - 10); //
    else
        fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_DEL_TBL_ID(key_pi->type);
    
    fwd_cmd.cmd.val.priority = MEA_FWD_TABLE_DEL_PRI;
#ifdef MEA_NEW_FWD_ROW_SET
    /* Disable the aging , and set the limiterId */
	count_shift = 0;
    count_shift+=MEA_FWD_DATA_STATIC_WIDTH;
    MEA_OS_insert_value(count_shift, MEA_FWD_DATA_AGE_WIDTH,(MEA_Uint32)(0), /* make the delete */  &(fwd_cmd.cmd.val.data.val[0]));
    count_shift+=MEA_FWD_DATA_AGE_WIDTH;
    count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;
    count_shift+= MEA_FWD_DATA_RESERV_WIDTH;
	count_shift += MEA_FWD_DATA_SA_DISCARD_WIDTH;
    count_shift += MEA_FWD_DATA_ACCESS_PORT_WIDTH;
    count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;
    count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;

	MEA_OS_insert_value(count_shift, MEA_FWD_DATA_LIMITER_ID_WIDTH,
						MEA_OS_read_value((MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_START,
										  (MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_WIDTH,
											&(result.val.data.val[0]) ),
                        &(fwd_cmd.cmd.val.data.val[0]));

    count_shift+= MEA_FWD_DATA_LIMITER_ID_WIDTH;
    count_shift += MEA_FWD_DATA_RESERV1_WIDTH;
    count_shift+= MEA_FWD_DATA_PARITY_WIDTH;
	
    /* Delete the entry */
	if (MEA_fwd_tbl_DeleteByKey(&fwd_cmd) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_DeleteByKey failed \n", __FUNCTION__);
	
        return MEA_ERROR;
    }
    if(get_entry.data.static_flag){
        if (enet_IsValid_xPermission(unit_i, get_entry.data.xPermissionId,MEA_TRUE) == MEA_TRUE) {
            if (enet_Delete_xPermission(unit_i, get_entry.data.xPermissionId) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - enet_Delete_xPermission failed   xpreID = %d\n", __FUNCTION__, get_entry.data.xPermissionId);
                return MEA_ERROR;
            }
        }
        else {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s xpreID = %d notValid on database\n", __FUNCTION__, get_entry.data.xPermissionId);
        }
    }
#else
	count_shift = 0;
    count_shift+=MEA_FWD_DATA_STATIC_WIDTH;
    MEA_OS_insert_value(count_shift,
                        MEA_FWD_DATA_AGE_WIDTH,
                        (MEA_Uint32)(0), /* make the delete */
                        &(fwd_cmd.cmd.val.data.val[0]));
    count_shift+=MEA_FWD_DATA_AGE_WIDTH;
    count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;
    count_shift+=MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH;
#if 0
    count_shift+=MEA_FWD_DATA_MC_EDIT_ID_WIDTH;
#else
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT0_2;
#endif
    count_shift+=MEA_FWD_DATA_SA_DISCARD_WIDTH;
    count_shift+=MEA_FWD_DATA_COS_FORCE_WIDTH;
    count_shift+=MEA_FWD_DATA_COS_WIDTH;
    count_shift+=MEA_FWD_DATA_PRI_FORCE_WIDTH;
    count_shift+=MEA_FWD_DATA_PRI_WIDTH;
    count_shift+=MEA_FWD_DATA_COL_FORCE_WIDTH;
    count_shift+=MEA_FWD_DATA_COL_WIDTH;
    count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;
    count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;
#if 0
    count_shift+= MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH;
    count_shift+= MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_WIDTH;
#else
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT3_7;
#endif
	MEA_OS_insert_value(count_shift,
                        MEA_FWD_DATA_LIMITER_ID_WIDTH,
						MEA_OS_read_value((MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_START,
										  (MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_WIDTH,
											&(result.val.data.val[0])
										 ),
                        &(fwd_cmd.cmd.val.data.val[0]));
    count_shift+= MEA_FWD_DATA_LIMITER_ID_WIDTH;
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT_8;
    count_shift+= MEA_FWD_DATA_PARITY_WIDTH;
	
    /* Delete the entry */
	if (MEA_fwd_tbl_DeleteByKey(&fwd_cmd) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_DeleteByKey failed \n", __FUNCTION__);
	
        return MEA_ERROR;
    }
    if(get_entry.data.static_flag){
        if (enet_Delete_xPermission    (unit_i,  get_entry.data.xPermissionId) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - enet_Delete_xPermission failed   xpreID = %d\n", __FUNCTION__,get_entry.data.xPermissionId);
            return MEA_ERROR;
        }
    }
#endif	
	
    if (key_pi->type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN || 
        key_pi->type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN) {
       // remove from DataBase
        MEA_SE_Entry_dbt  entry_pi;
        MEA_OS_memset(&entry_pi, 0, sizeof(MEA_SE_Entry_dbt));
        MEA_OS_memcpy(&entry_pi.key, key_pi, sizeof(MEA_SE_Entry_key_dbt));
        if(mea_drv_fwd_vpnIpV6_Delete_Ipv6_DB(&entry_pi) != MEA_OK){
        
        }
    
    
    }


    if (key_pi->type == MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN)
    {
        if (get_entry.data.static_flag == MEA_TRUE)
        {
            
            if ((key_pi->mac_plus_vpn.MAC.b[0] & 0x1) == 0){
                if (meaGlobal_static_MAC.val > 0)
                    meaGlobal_static_MAC.val--;
            }

            if ((key_pi->mac_plus_vpn.MAC.b[0] & 0x1) == 1){
               if(meaGlobal_multicast_static_MAC.val> 0)
                meaGlobal_multicast_static_MAC.val--;
            }

            
        }
    }

    
	return MEA_OK;
									     
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_DeleteAll_SE>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_DeleteAll_SE (MEA_Unit_t unit)
{
#ifndef MEA_FORWARDER_DEVICE_SIMULATION
    MEA_Globals_Entry_dbt entry;
    MEA_Uint32 status, aging_stat, fwd_stat;
    MEA_Uint32 learning_disable;
    mea_memory_mode_e  save_globalMemoryMode;
    MEA_Uint32 count_Status;
#endif

    MEA_API_LOG
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s \n- Forwarder not support  \n",
        __FUNCTION__);
    return MEA_ERROR;
    }
   
#ifdef MEA_FORWARDER_DEVICE_SIMULATION

   

    MEA_OS_memset(&(MEA_Forwarder_Table[0]),
  				  0,
				  sizeof(MEA_Forwarder_Table));
    

#else /* MEA_FORWARDER_DEVICE_SIMULATION */
   
   

    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,MEA_FWD_HW_UNIT);

    status = MEA_API_ReadReg(unit, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    
    if( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) == 1 )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - status command busy is set\n",
                          __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (MEA_API_Get_Globals_Entry (unit,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_Globals failed \n",
                          __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
 

    entry.if_global1.val.forwarder_flush = 1;
    learning_disable = entry.if_global1.val.learning_disable;
	fwd_stat = entry.if_global1.val.forwarder_enable;
    entry.if_global1.val.learning_disable = MEA_TRUE;
    aging_stat = entry.macAging.val.enable;
	entry.if_global1.val.forwarder_enable = 0;
	entry.macAging.val.enable = 0;



    if (MEA_API_Set_Globals_Entry (unit,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_Globals (1) failed  \n",
                          __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    MEA_OS_sleep(2, 0);

    status = MEA_API_ReadReg(unit, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status=0;
    while ( (status & MEA_IF_STATUS_FWD_TBL_INIT_BUSY_MASK) )
    {
         mea_fwd_tbl_sleep();
         status = MEA_API_ReadReg(unit, MEA_IF_STATUS_REG, MEA_MODULE_IF);
         count_Status++;
         if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - 2 MEA_IF_STATUS_FWD_TBL_INIT_BUSY_MASK for long time\n",
                 __FUNCTION__);
             MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
             return MEA_ERROR;

         }
    }


    status = MEA_API_ReadReg(unit, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    count_Status=0;
    while ( (status & MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK) )
    {
      
      mea_fwd_tbl_sleep();
      status = MEA_API_ReadReg(unit, MEA_IF_STATUS_REG, MEA_MODULE_IF);
      count_Status++;
      if(count_Status==MEA_FORWARDE_MAX_COUNT_STATUS){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
              "%s - MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK for long time\n",
              __FUNCTION__);
          MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
          return MEA_ERROR;

      }
    }


	if ( mea_drv_limiter_clear_counters(unit) != MEA_OK )
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_limiter_clear_counters failed  \n",
                          __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
	}





    entry.if_global1.val.forwarder_flush = 0;
	entry.if_global1.val.forwarder_enable = fwd_stat;
	entry.macAging.val.enable = aging_stat;
    entry.if_global1.val.learning_disable = learning_disable;

    if (MEA_API_Set_Globals_Entry (unit,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_Globals (0) failed  \n",
                          __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /* T.B.D - How we remove the owner to the Limiters and Action */
    /* TBD and the xper */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
#endif /* MEA_FORWARDER_DEVICE_SIMULATION */
	
    // clear Database of IPV6
    MEA_OS_memset(&MEA_ForwarderIPV6_Table, 0, sizeof(MEA_ForwarderIPV6_Table));

    meaGlobal_static_MAC.val = 0;
    meaGlobal_multicast_static_MAC.val = 0;
    
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Set_SE_Entry>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_SE_Entry(MEA_Unit_t         unit_i,
                                MEA_SE_Entry_dbt  *entry_pi)
{


    MEA_Forwarder_cmd_dbt          fwd_cmd;

    ENET_xPermission_dbt           portMap;
    MEA_Action_HwEntry_dbt         action_info;
    //   MEA_Uint32                     fwd_action;
    MEA_Forwarder_Result_dbt       result;
    MEA_Bool                       exist;

    MEA_SE_Entry_dbt              get_entry;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_GLOBAL_FORWARDER_SUPPORT_DSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- Forwarder not support  \n",
                __FUNCTION__);

            return MEA_ERROR;
        }
    /* Check for valid entry parameter */
    if (entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_pi == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    
    if (entry_pi->key.type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN || 
        entry_pi->key.type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN )
     
    {
        //find if the Key on DB.

        if (entry_pi->data.static_flag != MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - no Allow to change the static must enable \n",
                __FUNCTION__);
            return MEA_ERROR;
        }


    }


    /* Check valid ActionId */
    if (entry_pi->data.actionId_valid) {
        if (MEA_API_IsExist_Action(unit_i,
            entry_pi->data.actionId,
            &exist) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsExist_Action failed (id=%d)\n",
                __FUNCTION__, entry_pi->data.actionId);
            return MEA_ERROR;
        }
        if (!exist) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ActionId %d does not exist  \n",
                __FUNCTION__, entry_pi->data.actionId);
            return MEA_ERROR;

        }
    }

    /* Check valid LimiterId */
    if (entry_pi->data.limiterId_valid) {
        if (MEA_API_IsExist_Limiter_ById(unit_i,
            entry_pi->data.limiterId,
            &exist) == MEA_ERROR) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_IsExist_Limiter_ById failed (id=%d) \n",
                __FUNCTION__, entry_pi->data.limiterId);
            return MEA_ERROR;
        }
        if (!exist) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - LimiterId %d does not exist  \n",
                __FUNCTION__, entry_pi->data.limiterId);
            return MEA_ERROR;

        }
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    MEA_OS_memset(&fwd_cmd, 0, sizeof(MEA_Forwarder_cmd_dbt));
    MEA_OS_memset(&portMap, 0, sizeof(portMap));
    MEA_OS_memset(&action_info, 0, sizeof(action_info));

    //	fwd_action = 0;



        /* Build signature */
    if (MEA_fwd_tbl_build_key(&(entry_pi->key),
        &(fwd_cmd.cmd.val.key.add_key.key)) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_fwd_tbl_build_key failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    /* Get the current entry */
    if (MEA_fwd_tbl_readByKey(&(fwd_cmd.cmd.val.key.add_key.key),
        &result) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_fwd_tbl_readByKey failed \n", __FUNCTION__);
        return MEA_ERROR;
    }

    /* Check if we found the entry */
    if (!result.val.info.match) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Entry not found\n", __FUNCTION__);
        return MEA_ERROR;
    }
    /**/
    MEA_OS_memset(&get_entry, 0, sizeof(get_entry));
    MEA_OS_memcpy(&get_entry, entry_pi, sizeof(get_entry));
    if (MEA_API_Get_SE_Entry(unit_i, &get_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Get_SE_Entry failed \n", __FUNCTION__);
        return MEA_ERROR;
    }

    /**/

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (entry_pi->data.limiterId !=
        MEA_OS_read_value((MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_START,
        (MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_WIDTH,
            &(result.val.data.val[0])
        )) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - can not update LimiterId of exist Entry \n", __FUNCTION__);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



    /* Build the data for the new update entry */
    if (MEA_fwd_tbl_build_data(unit_i,
        &(entry_pi->data),
        &(fwd_cmd.cmd.val.data)) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_fwd_tbl_build_data \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    /* Update the entry */
    fwd_cmd.cmd.val.command = MEA_FWD_TABLE_UPDATE_CMD;
    
    //    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " entry_pi->key.type  %d \r\n", entry_pi->key.type);

    if (entry_pi->key.type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN ||
        entry_pi->key.type == MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN)
        fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_UPDATE_TBL_ID(entry_pi->key.type - 10); //
    else
        fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_UPDATE_TBL_ID(entry_pi->key.type);

    
    
    fwd_cmd.cmd.val.priority = MEA_FWD_TABLE_UPDATE_PRI;
    if (MEA_fwd_tbl_WriteByKey(&fwd_cmd) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_fwd_tbl_WriteByKey failed \n", __FUNCTION__);
        return MEA_ERROR;
    }
    // delete the old xper
    if (get_entry.data.static_flag) {
        if (enet_Delete_xPermission(unit_i, get_entry.data.xPermissionId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Delete_xPermission failed   xpreID = %d\n", __FUNCTION__, get_entry.data.xPermissionId);
            return MEA_ERROR;
        }
    }



	return MEA_OK;
									     
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Get_SE_Entry>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_SE_Entry        (MEA_Unit_t						unit_i,
										MEA_SE_Entry_dbt		       *entry_pio) {


   MEA_Forwarder_Result_dbt       result;
   MEA_Forwarder_Signature_dbt    signature;

   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS


    
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s \n- Forwarder not support  \n",
           __FUNCTION__);

       return MEA_ERROR;
       }
    /* Check for valid entry_pio parameter */
    if (entry_pio == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_pio == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    MEA_OS_memset(&result,0,sizeof(result));
    MEA_OS_memset(&signature,0,sizeof(signature));


	/* Build signature */
	if (MEA_fwd_tbl_build_key(&(entry_pio->key),
		                      &(signature.val.key)) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_build_key failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Read from HW */
	if (MEA_fwd_tbl_readByKey(&signature.val.key,
                              &result) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_readByKey failed\n", __FUNCTION__);
		return MEA_ERROR;
    }

	/* Check if we found the entry */
    if (!result.val.info.match) {
#if 0
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Entry not found\n", __FUNCTION__);
#endif
		return MEA_ERROR;
    }
#ifdef MEA_FWD_DEBUG_MODE
	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
		"%s:%d -  signature.regs=( regs[0]=0x%08x regs[1]=0x%08x regs[2]=0x%08x) \n",
		__FUNCTION__,__LINE__,
		signature.regs[0],
		signature.regs[1],
		signature.regs[2]);

	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
		"%s:%d -  result.regs=( regs[0]=0x%08x regs[1]=0x%08x regs[2]=0x%08x) \n",
		__FUNCTION__,__LINE__,
		result.regs[0],
		result.regs[1],
		result.regs[2]);
#endif
	/* Convert result to SE_Entry */
	if (MEA_fwd_tbl_convert_to_SE_Entry(unit_i,
		                                &signature.val.key,
		                                &result.val.data,
							   	 	    entry_pio,
										MEA_SETTING_TYPE_GET) != MEA_OK) {
			
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_convert_to_SE_Entry failed\n", 
						  __FUNCTION__);
		return MEA_ERROR;
	}

	return MEA_OK;								     


											 
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_GetFirst_SE_Entry>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_SE_Entry    (MEA_Unit_t                      unit_i,
                                         MEA_SE_Entry_dbt               *entry_po,
                                         MEA_Bool*                       found_o)
{

   MEA_Forwarder_Signature_dbt  fwd_entry_signature;
   MEA_Forwarder_Result_dbt     fwd_entry_result;

   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- Forwarder not support  \n",
            __FUNCTION__);

        return MEA_ERROR;
     }

   if (entry_po == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - entry_po == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
   if (found_o == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - found_o == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
   if(MEA_GLOBALE_FORWADER_FILTER_SUPPORT == MEA_FALSE &&  entry_po->filterEnable == MEA_TRUE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - The fwd filter not support on this version set  filterEnable to false\n",
           __FUNCTION__);
       return MEA_ERROR;
   }

   if(entry_po->filterEnable == MEA_TRUE && entry_po->filterId >= MEA_FORWARDER_FILTER_PROFILE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - The fwd filter index=%d out of range\n",
           __FUNCTION__,entry_po->filterId);
       return MEA_ERROR;

   }




#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
 
   MEA_drv_fwd_last_scan_hash_type = 0;
   MEA_OS_memset(&fwd_entry_signature,0,sizeof(fwd_entry_signature));
   MEA_OS_memset(&fwd_entry_result   ,0,sizeof(fwd_entry_result   ));
   if(MEA_GLOBALE_FORWADER_FILTER_SUPPORT){
       if(entry_po->filterEnable){
       // write the fwd command 
           //check if the ID is exist
           if(MEA_API_SE_Entry_IsExistFilter(unit_i,entry_po->filterId)!=MEA_TRUE){
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                   "%s - MEA_API_SE_Entry_IsExistFilter index=%d notExist\n",
                   __FUNCTION__,entry_po->filterId);
               return MEA_ERROR;
           }

           mea_fwd_write_filterSearch(unit_i,entry_po->filterId);

       }else{
            mea_fwd_write_filterSearch(unit_i,0);

       }
    }


   if (MEA_fwd_tbl_GetFirst (&MEA_drv_fwd_last_scan_hash_type,
                             &fwd_entry_signature,
                             &fwd_entry_result   ,
                             found_o ) != MEA_OK ) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                "%s - MEA_fwd_tbl_GetFirst failed \n",
						__FUNCTION__);
 	  return MEA_ERROR;
   }

   if ((*found_o) == MEA_FALSE) {
	   return MEA_OK;
   }
   MEA_OS_memset(entry_po,0,sizeof(*entry_po));

   if (MEA_fwd_tbl_convert_to_SE_Entry(unit_i,
									   &fwd_entry_signature.val.key,
	                                   &fwd_entry_result.val.data,
									   entry_po,
									   MEA_SETTING_TYPE_GET) != MEA_OK) {
			
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_convert_to_SE_Entry failed\n", 
						  __FUNCTION__);
		return MEA_ERROR;
	}

    return MEA_OK;
									     
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_GetNext_SE_Entry>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_SE_Entry   (MEA_Unit_t                      unit_i,
                                       MEA_SE_Entry_dbt               *entry_po,
                                       MEA_Bool                       *found_o)
{

   MEA_Forwarder_Signature_dbt  fwd_entry_signature;
   MEA_Forwarder_Result_dbt     fwd_entry_result;

   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s \n- Forwarder not support  \n",
        __FUNCTION__);

        return MEA_ERROR;
    }
    
   if (entry_po == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - entry_po == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
   if (found_o == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - found_o == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   MEA_OS_memset(&fwd_entry_signature,0,sizeof(fwd_entry_signature));
   MEA_OS_memset(&fwd_entry_result   ,0,sizeof(fwd_entry_result   ));

   if (MEA_fwd_tbl_GetNext (&MEA_drv_fwd_last_scan_hash_type,
                             &fwd_entry_signature,
                             &fwd_entry_result   ,
                             found_o ) != MEA_OK ) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                "%s - MEA_fwd_tbl_GetFirst failed \n",
						__FUNCTION__);
 	  return MEA_ERROR;
   }

   if ((*found_o) == MEA_FALSE) {
	   return MEA_OK;
   }
   
    MEA_OS_memset(entry_po,0,sizeof(*entry_po));

   if (MEA_fwd_tbl_convert_to_SE_Entry(unit_i,
	                                   &fwd_entry_signature.val.key,
	                                   &fwd_entry_result.val.data,
									   entry_po,
									   MEA_SETTING_TYPE_GET) != MEA_OK) {
			
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_convert_to_SE_Entry failed\n", 
						  __FUNCTION__);
		return MEA_ERROR;
	}

	return MEA_OK;
									     
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Set_SE_Aging>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_SE_Aging (MEA_Unit_t unit,
                                 MEA_Uint32 seconds,
                                 MEA_Bool   admin_status)
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 sweep_interval;
    MEA_Uint32 clock; 
    MEA_Uint32 Myseconds = seconds;
   MEA_API_LOG

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - MEA_API_Get_Globals_Entry failed\n",
					     __FUNCTION__);
        
       return MEA_ERROR;
    }
    clock= (MEA_Uint32)MEA_GLOBAL_SYS_Clock;
	
    

    if (Myseconds <=3){
        Myseconds = 3;
    }

    sweep_interval = clock;
	sweep_interval /= (1 * (1<< entry.verInfo.val.forwarder_Addr_Width));
    sweep_interval *= 2 * Myseconds*MEA_DSE_AGING_GAN1 / MEA_DSE_AGING_GAN;

	entry.macAging.val.enable        = admin_status;
	entry.macAging.val.lastTable     = 0;
    entry.macAging.val.sweepInterval = sweep_interval;

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - MEA_API_Set_Globals_Entry failed\n",
						 __FUNCTION__);
        
       return MEA_ERROR;
    }

	/* Save the value */
    if (!MEA_reinit_start){
        MEA_SE_Aging_interval = Myseconds;
        MEA_SE_Aging_enable = admin_status;
    }

//     printf( "Reinit %d Aging setting  Aging_enable %d Aging_interval %d \n", MEA_reinit_start, 
//         MEA_SE_Aging_enable, 
//         MEA_SE_Aging_interval);
        

	return MEA_OK;
									     
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Get_SE_Aging>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_SE_Aging  (MEA_Unit_t unit,
                                   MEA_Uint32 *seconds,
								   MEA_Bool   *admin_status) 
{


   MEA_API_LOG
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s \n- Forwarder not support  \n",
           __FUNCTION__);
       
       return MEA_ERROR;
    }

	if (seconds) {
		*seconds = MEA_SE_Aging_interval;
	}

	if (admin_status) {
		*admin_status = MEA_SE_Aging_enable;
	}


	return MEA_OK;
									     
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Collect_Counters_Forwarder>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_Forwarder(MEA_Unit_t unit_i) {
   
    

	MEA_Uint32        value[4];
	//MEA_Uint32        val;
	MEA_ind_read_t    ind_read;
    mea_memory_mode_e originalMode;

    MEA_API_LOG
    MEA_OS_memset(&value,0,sizeof(value));

    if(MEA_reinit_start)
            return MEA_OK;

        if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- Forwarder not support  \n",
                __FUNCTION__);
            return MEA_ERROR;
            }

	ind_read.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_COUNTERS;
	ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
	ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_read.statusReg		= MEA_IF_STATUS_REG;   
	ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
	ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_read.read_data      = value;

    originalMode =globalMemoryMode; 

    
    /* Read the counters from device */
	if (MEA_API_ReadIndirect(unit_i,
		                     &ind_read,
                             MEA_NUM_OF_ELEMENTS(value),
                             MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed \n",
                          __FUNCTION__,
						  ind_read.tableType,
						  ind_read.tableOffset,
						  MEA_MODULE_IF);
		globalMemoryMode=originalMode;
        return MEA_ERROR;
    }
    globalMemoryMode=originalMode;

#if 1   
    MEA_OS_UPDATE_COUNTER(MEA_Counters_Forwarder.learn_failed.val, value[0]);

    MEA_OS_UPDATE_COUNTER(MEA_Counters_Forwarder.learn_key_not_match.val, value[1]);

    MEA_OS_UPDATE_COUNTER(MEA_Counters_Forwarder.fwd_key_match.val, value[2]);
    MEA_Counters_Forwarder.learn_dynamic.val  = value[3]; //Status 
    MEA_Counters_Forwarder.unicast_static_MAC.val =  meaGlobal_static_MAC.val;
    MEA_Counters_Forwarder.multicast_static_MAC.val = meaGlobal_multicast_static_MAC.val;

#endif

     globalMemoryMode=originalMode;
    /* Return to Callers */
    return MEA_OK;   

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Get_Coun ters_Forwarder>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_Forwarder        (MEA_Unit_t             unit,
                                                  MEA_Counters_Forwarder_dbt* entry) {

   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	   if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			   "%s \n- Forwarder not support  \n",
			   __FUNCTION__);
		   return MEA_ERROR;
	   }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry==NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;  
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   /* Update output parameters */
   MEA_OS_memcpy(entry,
                 &(MEA_Counters_Forwarder),
                 sizeof(*entry));

   /* Return to Callers */
   return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Clear_Counters_Forwarder>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_Forwarder  (MEA_Unit_t unit_i) {


	MEA_API_LOG

        if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- Forwarder not support  \n",
                __FUNCTION__);
            return MEA_ERROR;
            }
   /* Collect counters from hardware - clear them */
   if (MEA_API_Collect_Counters_Forwarder(unit_i)==MEA_ERROR){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Clear Counters failed\n",
                         __FUNCTION__);
	   return MEA_ERROR;
   }



    /* Zeors the SW counters db */
    MEA_OS_memset(&(MEA_Counters_Forwarder),
                  0,
                  sizeof(MEA_Counters_Forwarder));

    /* Return to Callers */
    return MEA_OK;

}

#if MEA_OLD_DSE_SUPPORT

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <old APIs>                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_fwd_tbl_convert_to_DSE_Entry>                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_fwd_tbl_convert_to_DSE_Entry(MEA_Unit_t                      unit_i,
											MEA_Forwarder_Key_dbt          *key_pi,
                                            MEA_Forwarder_Data_dbt         *data_pi,
                              	            MEA_DSE_Entry_dbt	           *entry_po,
   										    MEA_Policer_Entry_dbt          *policer_po,
				    			            MEA_EgressHeaderProc_Entry_dbt *editing_po )
{


   MEA_SE_Entry_dbt se_entry;




#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
   if (key_pi == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - key_pi == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
   if (data_pi == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - data_pi == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
   if (entry_po == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - entry_po == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
 
   /* Xerolize the output entry */
   MEA_OS_memset(entry_po,0,sizeof(*entry_po));



   
   /* Convert to SE Entry */
   if (MEA_fwd_tbl_convert_to_SE_Entry(unit_i,
	                                   key_pi,
	                                   data_pi,
									   &se_entry,
									   MEA_SETTING_TYPE_GET) != MEA_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - MEA_fwd_tbl_convert_to_SE_Entry_po == NULL \n",
						 __FUNCTION__);

   }


   
   /* Build the entry key */
   MEA_OS_memcpy(&(entry_po->key),&(se_entry.key),sizeof(entry_po->key));




   /* Build the rest of entry data fields (except the action) */
   MEA_OS_memcpy(&(entry_po->data.OutPorts),
   	             &(se_entry.data.OutPorts),
		         sizeof(entry_po->data.OutPorts));
    entry_po->data.static_flag = se_entry.data.static_flag;
    entry_po->data.aging       = se_entry.data.aging;
    entry_po->data.valid       = se_entry.data.valid;
    entry_po->data.sa_discard  = se_entry.data.sa_discard;
        entry_po->data.access_port = se_entry.data.access_port;



    entry_po->data.LimiterId   = (se_entry.data.limiterId_valid) ? se_entry.data.limiterId : 0;
   

   /* Build the entry data action and policer_po / editing_po */
   if(se_entry.data.actionId_valid) { 

       MEA_Action_t                         Action_id;
       MEA_Action_Entry_Data_dbt            Action_data;
       MEA_EgressHeaderProc_Array_Entry_dbt Action_editing;
	   MEA_EHP_Info_dbt                     Action_editing_info;

	   MEA_OS_memset(&Action_editing,0,sizeof(Action_editing));

       Action_id = se_entry.data.actionId;
       
  	   /* Get Action Info */
	   if (MEA_API_Get_Action(MEA_UNIT_0,
	  	                      Action_id,
						      &Action_data,
						      NULL, /* &Action_outPorts */
						      policer_po, 
						      &Action_editing) 
						     != MEA_OK) {
	       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                     "%s - MEA_API_Get_Action failed (id=%d) \n",
		                     __FUNCTION__,
		                     Action_id);
	       return MEA_ERROR;
	   }

	   entry_po->data.action.action_force = MEA_TRUE;
	   entry_po->data.action.editId      = (Action_data.ed_id_valid    ) ? Action_data.ed_id              : 0;
       entry_po->data.action.pmId        = (Action_data.pm_id_valid    ) ? Action_data.pm_id              : 0;
	   entry_po->data.action.tmId        = (Action_data.tm_id_valid    ) ? Action_data.tm_id              : 0;
	   entry_po->data.Llc                = (Action_data.proto_llc_valid) ? Action_data.Llc                : 0;
	   entry_po->data.Protocol           = (Action_data.proto_llc_valid) ? Action_data.Protocol           : 0;
	   entry_po->data.protocol_llc_force = (Action_data.proto_llc_valid) ? Action_data.protocol_llc_force : 0;
	   entry_po->data.cos_force          = Action_data.force_cos_valid;
	   entry_po->data.cos                = Action_data.COS;
	   entry_po->data.l2pri_force        = Action_data.force_l2_pri_valid;
	   entry_po->data.l2pri              = Action_data.L2_PRI;
	   entry_po->data.color_force        = Action_data.force_color_valid;
	   entry_po->data.color              = Action_data.COLOR;
       entry_po->data.flowCoSMappingProfile_force     = (MEA_Uint32)Action_data.flowCoSMappingProfile_force;
       entry_po->data.flowCoSMappingProfile_id        = (MEA_Uint32)Action_data.flowCoSMappingProfile_id;
       entry_po->data.flowMarkingMappingProfile_force = (MEA_Uint32)Action_data.flowMarkingMappingProfile_force;
       entry_po->data.flowMarkingMappingProfile_id    = (MEA_Uint32)Action_data.flowMarkingMappingProfile_id;


	   if (editing_po && 
		   Action_data.ed_id_valid) {

	       if (Action_editing.num_of_entries != 1) {
		       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				                 "%s - Action_editing.num_of_entries (%d) != 1\n",
								 __FUNCTION__,
								 Action_editing.num_of_entries);
			   return MEA_ERROR;
		   }
		   Action_editing.ehp_info = &Action_editing_info;
	       if (MEA_API_Get_Action(MEA_UNIT_0,
	  	                          Action_id,
			   		  	          &Action_data,
						          NULL, /* &Action_outPorts */
						          policer_po, 
						          &Action_editing) 
						         != MEA_OK) {
	           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                         "%s - MEA_API_Get_Action failed (id=%d) \n",
		                         __FUNCTION__,
		                         Action_id);
	           return MEA_ERROR;
	       } 
		   MEA_OS_memcpy(editing_po,
			             &(Action_editing.ehp_info[0].ehp_data),
						 sizeof(*editing_po));
	   }
   }

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Create_DSE_Entry>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_DSE_Entry    (MEA_Unit_t						unit,
                                        MEA_DSE_Entry_dbt			   *entry_pio,
									    MEA_Policer_Entry_dbt          *policer_pi,
				    			        MEA_EgressHeaderProc_Entry_dbt *editing_pi)
{

   MEA_Bool                              exist;
   MEA_Action_Entry_Data_dbt             Action_Data;
   MEA_OutPorts_Entry_dbt                Action_OutPorts;
   MEA_EgressHeaderProc_Array_Entry_dbt  Action_Editing;
   MEA_EHP_Info_dbt                      Action_Editing_Info;
   MEA_Action_t                          Action_Id;
   MEA_SE_Entry_dbt                      se_entry;
   
   MEA_API_LOG
 
  
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
       if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s \n- Forwarder not support  \n",
               __FUNCTION__);
           return MEA_ERROR;
           }
    /* Check for valid entry parameter */
    if (entry_pio == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_pio == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
    /* Check CFM OAM  support*/
 
    
    if (MEA_API_IsExist_Limiter_ById  (unit,
                                       (MEA_Limiter_t)(entry_pio->data.LimiterId), 
                                       &exist) == MEA_ERROR){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_IsExist_Limiter_ById failed \n",
                         __FUNCTION__);

	}
	if(!exist){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - LimiterId(%d) does not exist  \n",
                         __FUNCTION__,entry_pio->data.LimiterId);

	}
 
    if (entry_pio->data.flowCoSMappingProfile_force) {

        if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowCoSMappingProfile_force is not support \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry_pio->data.cos_force  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowCoSMappingProfile_force (%d) is not allowed "
                              " together with cos_force (%d)\n",
                              __FUNCTION__,
                              entry_pio->data.flowCoSMappingProfile_force,
                              entry_pio->data.cos_force);
            return MEA_ERROR;
        }

        if (MEA_API_Get_FlowCoSMappingProfile_Entry
                                    (unit,
                                    (MEA_FlowCoSMappingProfile_Id_t)(entry_pio->data.flowCoSMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Flow CoS Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              entry_pio->data.flowCoSMappingProfile_id);
            return MEA_ERROR;
        }

    }

    if (entry_pio->data.flowMarkingMappingProfile_force) {

        if (!MEA_FLOW_MARKING_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowMarkingMappingProfile_FORCE is not support \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry_pio->data.l2pri_force  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowMarkingMappingProfile_FORCE (%d) is not allowed "
                              " together with l2pri_force (%d)\n",
                              __FUNCTION__,
                              entry_pio->data.flowMarkingMappingProfile_force,
                              entry_pio->data.l2pri_force);
            return MEA_ERROR;
        }


        if (MEA_API_Get_FlowMarkingMappingProfile_Entry
                                   (unit,
                                    (MEA_FlowMarkingMappingProfile_Id_t)(entry_pio->data.flowMarkingMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Flow Marking Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              entry_pio->data.flowMarkingMappingProfile_id);
            return MEA_ERROR;
        }

    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


#if 0
  {
    MEA_Forwarder_cmd_dbt                 fwd_cmd;
    MEA_Forwarder_Result_dbt              result;

	/* Erase the fwd_cmd structure */
    MEA_OS_memset(&fwd_cmd,       0 , sizeof(MEA_Forwarder_cmd_dbt));


	/* Build signature key */
	if (MEA_fwd_tbl_build_key(&(entry_pio->key),
		                      &(fwd_cmd.cmd.val.key.add_key.key)) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_build_key failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Get the current entry */
    if ( MEA_fwd_tbl_readByKey( &(fwd_cmd.cmd.val.key.add_key.key),
                                &result) != MEA_OK ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_readByKey failed \n", __FUNCTION__);
		return MEA_ERROR;
    }

	/* Check if we found the entry */
    if (result.val.info.match) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Entry already exist\n", __FUNCTION__);
		return MEA_ERROR;
    }
  }
#endif


   /* If the user entered all info NULL, and ED ID, PM ID, TM ID all 0 --> do not open action, just mark fid as false.*/
	if ( entry_pio->data.action.editId == 0 &&
		 entry_pio->data.action.tmId   == 0 &&
		 entry_pio->data.action.pmId   == 0 &&
		 entry_pio->data.color_force   == 0 &&
		 entry_pio->data.l2pri_force   == 0 &&
		 entry_pio->data.cos_force     == 0 &&
		 policer_pi == NULL          &&
		 editing_pi == NULL               ) {
		Action_Id = 0;
		entry_pio->data.action.action_force = MEA_FALSE;
	} else {

		/* Build the Action Data */
		MEA_OS_memset(&Action_Data         , 0 , sizeof(Action_Data        ));
		Action_Data.tm_id              = entry_pio->data.action.tmId;
		Action_Data.tm_id_valid        = (policer_pi==NULL && entry_pio->data.action.tmId==0) ? MEA_FALSE : MEA_TRUE;
        Action_Data.pm_type            = entry_pio->data.action.pm_type;
		Action_Data.pm_id              = entry_pio->data.action.pmId;
		Action_Data.pm_id_valid        = MEA_TRUE;
		Action_Data.ed_id              = entry_pio->data.action.editId;
		Action_Data.ed_id_valid        = (editing_pi==NULL && entry_pio->data.action.editId==0) ? MEA_FALSE : MEA_TRUE;
		Action_Data.Llc                = (entry_pio->data.protocol_llc_force) ? entry_pio->data.Llc : 0;
		Action_Data.proto_llc_valid    = MEA_TRUE;
		Action_Data.protocol_llc_force = MEA_TRUE;
		Action_Data.Protocol           = (entry_pio->data.protocol_llc_force) ? entry_pio->data.Protocol : 1;
		Action_Data.force_color_valid  = entry_pio->data.color_force;
		Action_Data.COLOR              = entry_pio->data.color;
		Action_Data.force_cos_valid    = entry_pio->data.cos_force;
		Action_Data.COS                = entry_pio->data.cos;
		Action_Data.force_l2_pri_valid = entry_pio->data.l2pri_force;
		Action_Data.L2_PRI             = entry_pio->data.l2pri;
        Action_Data.flowCoSMappingProfile_force     = (MEA_Uint16)entry_pio->data.flowCoSMappingProfile_force;
        Action_Data.flowCoSMappingProfile_id        = (MEA_Uint16)entry_pio->data.flowCoSMappingProfile_id;
        Action_Data.flowMarkingMappingProfile_force = (MEA_Uint16)entry_pio->data.flowMarkingMappingProfile_force;
        Action_Data.flowMarkingMappingProfile_id    = (MEA_Uint16)entry_pio->data.flowMarkingMappingProfile_id;


		/* Build Action OutPorts */
		MEA_OS_memset(&Action_OutPorts     , 0 , sizeof(Action_OutPorts     ));
		

		/* Build Action editing */
		MEA_OS_memset(&Action_Editing      , 0 , sizeof(Action_Editing     ));
		MEA_OS_memset(&Action_Editing_Info , 0 , sizeof(Action_Editing_Info));
		Action_Editing.num_of_entries = 1;
		Action_Editing.ehp_info = &Action_Editing_Info;
		if (editing_pi) {
			MEA_OS_memcpy(&(Action_Editing.ehp_info[0].ehp_data),
				          editing_pi,
						  sizeof(Action_Editing.ehp_info[0].ehp_data));
		}


		/* Create the Action  */
		Action_Id = MEA_PLAT_GENERATE_NEW_ID;
		if (MEA_API_Create_Action (MEA_UNIT_0,
			                       &Action_Data,
								   &Action_OutPorts,
								   policer_pi,
								   &Action_Editing,
								   &Action_Id) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - MEA_API_Create_Action failed\n",
							  __FUNCTION__);
			return MEA_ERROR;
		}
		entry_pio->data.action.action_force = MEA_TRUE;
		entry_pio->data.action.editId       = Action_Data.ed_id;
		entry_pio->data.action.tmId         = Action_Data.tm_id;
		entry_pio->data.action.pmId         = Action_Data.pm_id;

	}

	/* Build the se_entry */
	MEA_OS_memset(&se_entry,0,sizeof(se_entry));
	MEA_OS_memcpy(&(se_entry.key),
		          &(entry_pio->key),
				  sizeof(se_entry.key));
	se_entry.data.actionId_valid = entry_pio->data.action.action_force;
	se_entry.data.actionId  = Action_Id;
	se_entry.data.aging = entry_pio->data.aging;
	se_entry.data.limiterId = entry_pio->data.LimiterId;
	se_entry.data.limiterId_valid = (entry_pio->data.LimiterId != MEA_LIMITER_DONT_CARE);
	MEA_OS_memcpy(&(se_entry.data.OutPorts),
		          &(entry_pio->data.OutPorts),
				  sizeof(se_entry.data.OutPorts));
	se_entry.data.sa_discard = entry_pio->data.sa_discard;
	se_entry.data.static_flag = entry_pio->data.static_flag;
	se_entry.data.valid       = entry_pio->data.valid;
    
    
        se_entry.data.access_port = entry_pio->data.access_port;
      

	if (MEA_API_Create_SE_Entry(unit,&se_entry) != MEA_OK) {
		if (se_entry.data.actionId_valid) {
			MEA_API_Delete_Action(unit,Action_Id);
		}
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_API_Create_SE_Entry failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


    
    
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Delete_DSE_Entry>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_DSE_Entry    (MEA_Unit_t						unit_i,
										MEA_DSE_Entry_key_dbt		   *key_pi) 
{

   MEA_Forwarder_cmd_dbt    fwd_cmd;
   MEA_Forwarder_Result_dbt result;
   MEA_Action_t             Action_Id;
   MEA_Bool                 action_exist;
   MEA_Bool                 exist;
   MEA_Uint32               Action_NumOfOwners;
   MEA_db_HwUnit_t                       action_hwUnit;
   MEA_db_HwId_t                         action_hwId=0;
   MEA_Uint32 count_shift;

   MEA_API_LOG


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s \n- Forwarder not support  \n",
               __FUNCTION__);
           return MEA_ERROR;
    }
    /* Check for valid entry parameter */
    if (key_pi == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - key_pi == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	/* Erase the fwd_cmd structure */
    MEA_OS_memset(&fwd_cmd,       0 , sizeof(MEA_Forwarder_cmd_dbt));


	/* Build signature key */
	if (MEA_fwd_tbl_build_key(key_pi,
		                      &(fwd_cmd.cmd.val.key.delete_key.key)) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_build_key failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Get the current entry */
    if ( MEA_fwd_tbl_readByKey( &(fwd_cmd.cmd.val.key.delete_key.key),
                                &result) != MEA_OK )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_readByKey failed \n", __FUNCTION__);
		return MEA_ERROR;
    }

	/* Check if we found the entry */
    if (!result.val.info.match) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Entry not found\n", __FUNCTION__);
		return MEA_ERROR;
    }

	/* Set the delete command */
    fwd_cmd.cmd.val.command  = MEA_FWD_TABLE_DEL_CMD;
    fwd_cmd.cmd.val.table_id = MEA_FWD_TABLE_DEL_TBL_ID(key_pi->type);
    fwd_cmd.cmd.val.priority = MEA_FWD_TABLE_DEL_PRI;

    /* Disable the aging , and set the limiterId */
	count_shift = 0;
    count_shift+=MEA_FWD_DATA_STATIC_WIDTH;
    MEA_OS_insert_value(count_shift,
                        MEA_FWD_DATA_AGE_WIDTH,
                        (MEA_Uint32)(0), /* make the delete */
                        &(fwd_cmd.cmd.val.data.val[0]));
    count_shift+=MEA_FWD_DATA_AGE_WIDTH;
	
#ifdef MEA_NEW_FWD_ROW_SET	
    count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;
	count_shift += MEA_FWD_DATA_SA_DISCARD_WIDTH;
    count_shift += MEA_FWD_DATA_ACCESS_PORT_WIDTH;

    count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;
    count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;
#else
    count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;
    count_shift+=MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH;
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT0_2;

    count_shift+=MEA_FWD_DATA_SA_DISCARD_WIDTH;
    count_shift+=MEA_FWD_DATA_COS_FORCE_WIDTH;
    count_shift+=MEA_FWD_DATA_COS_WIDTH;
    count_shift+=MEA_FWD_DATA_PRI_FORCE_WIDTH;
    count_shift+=MEA_FWD_DATA_PRI_WIDTH;
    count_shift+=MEA_FWD_DATA_COL_FORCE_WIDTH;
    count_shift+=MEA_FWD_DATA_COL_WIDTH;
    count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;
    count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;


#endif //MEA_NEW_FWD_ROW_SET

	MEA_OS_insert_value(count_shift,
                        MEA_FWD_DATA_LIMITER_ID_WIDTH,
						MEA_OS_read_value((MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_START,
										  (MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_WIDTH,
											&(result.val.data.val[0])
										 ),
                        &(fwd_cmd.cmd.val.data.val[0]));
    count_shift+= MEA_FWD_DATA_LIMITER_ID_WIDTH;

	/* Delete the entry */
	if (MEA_fwd_tbl_DeleteByKey(&fwd_cmd) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_DeleteByKey failed \n", __FUNCTION__);
		return MEA_ERROR;
    }

	if (MEA_OS_read_value(MEA_FWD_DATA_STATIC_START,
                          MEA_FWD_DATA_STATIC_WIDTH,
						  &(result.val.data.val[0]))) {
        if ( MEA_OS_read_value(MEA_FWD_DATA_ACTION_ID_VALID_START,
							   MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,
							   &(result.val.data.val[0]))) {
 	    	if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000)  {
	    		action_hwUnit = MEA_HW_UNIT_ID_GENERAL;
		    } else {
			    action_hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
		    }
		    action_hwId = (MEA_Action_t)MEA_OS_read_value(MEA_FWD_DATA_ACTION_ID_START,
														  MEA_FWD_DATA_ACTION_ID_WIDTH,
														  &(result.val.data.val[0]));

            MEA_DRV_GET_SW_ID_WITH_EXIST(unit_i,
                                         mea_drv_Get_Action_db,
                                         action_hwUnit,
                                         action_hwId,
                                         Action_Id,
                                         action_exist,
                                         return MEA_ERROR);
            if (!action_exist) {
                if (action_hwUnit == MEA_HW_UNIT_ID_DOWNSTREAM) {
                    action_hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
                    MEA_DRV_GET_SW_ID_WITH_EXIST(unit_i,
                                                 mea_drv_Get_Action_db,
                                                 action_hwUnit,
                                                 action_hwId,
                                                 Action_Id,
                                                 action_exist,
                                                 return MEA_ERROR);
                }
            }
            if (!action_exist) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - not found SW id for action hwUnit (%d) / hwId (%d) \n",
                                  __FUNCTION__,
                                  action_hwUnit,
                                  action_hwId);
                return MEA_ERROR;
            }


		    if (MEA_API_IsExist_Action(MEA_UNIT_0,Action_Id,&exist) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_IsExist_Action failed (id=%d) \n",
                                 __FUNCTION__,Action_Id);
                return MEA_ERROR;  
		    }
		    if (!exist) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Current Action Id %d not exist \n",
                                 __FUNCTION__,Action_Id);
                return MEA_ERROR;  
            }        

		    if (MEA_API_GetOwner_Action(MEA_UNIT_0,
			                            Action_Id,
				   					    &Action_NumOfOwners) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - MEA_API_GetOwner_Action failed (id=%d) \n",
                                  __FUNCTION__,Action_Id);
                return MEA_ERROR;  
		    }

			if (Action_NumOfOwners == 1) {
			   if (MEA_API_Delete_Action (MEA_UNIT_0,
				                          Action_Id) != MEA_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                 "%s - MEA_API_Delete_Action failed (id=%d)\n",
					                 __FUNCTION__,Action_Id);
				   return MEA_ERROR;
			   }
		   } else {
			   if (MEA_API_DelOwner_Action (MEA_UNIT_0,
				                            Action_Id) != MEA_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                 "%s - MEA_API_DelOwner_Action failed (id=%d)\n",
					                 __FUNCTION__,Action_Id);
				   return MEA_ERROR;
			   }
		   }

		}
	}
	

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_DeleteAll_DSE>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_DeleteAll_DSE (MEA_Unit_t unit)
{
    MEA_Action_t  Action_Id;
	MEA_Action_t  Action_Id_start;
	MEA_Action_t  Action_Id_end;
    MEA_Bool exist;
	MEA_Status    status=MEA_OK;

    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- Forwarder not support  \n",
            __FUNCTION__);
        return MEA_ERROR;
        }
	/* Delete all forwarder entries */
	if (MEA_API_DeleteAll_SE(unit) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - MEA_API_DeleteAll_SE failed\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Delete all forwarder entries */
    Action_Id_start = (MEA_Action_t)MEA_ACTION_TBL_FWD_START_INDEX+1;
    Action_Id_end   = (MEA_Action_t)(Action_Id_start + MEA_MAX_NUM_OF_FWD_ACTIONS - 1);
    for (Action_Id=Action_Id_start; Action_Id<= Action_Id_end; Action_Id++){  
		if (MEA_API_IsExist_Action(unit,Action_Id,&exist) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				               "%s - MEA_API_IsExist_Action failed (id=%d)\n",
							   __FUNCTION__,Action_Id);
             status=MEA_ERROR;
			 continue;
		}
		if (!exist) {
			continue;
		}
    
        if (MEA_API_Delete_Action (unit,Action_Id)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - MEA_API_Delete_Action failed (Id=%d\n",
							  __FUNCTION__,Action_Id);
            status=MEA_ERROR;
  		    continue;
        }
    }

	return status ;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Set_DSE_Entry>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_DSE_Entry  (MEA_Unit_t                      unit,
                                   MEA_DSE_Entry_dbt              *entry_pio,
					  		       MEA_Policer_Entry_dbt          *policer_pi,
				    	           MEA_EgressHeaderProc_Entry_dbt *editing_pi)
{


   MEA_Forwarder_cmd_dbt                 fwd_cmd;
   MEA_Forwarder_Result_dbt              result;
   MEA_Bool                              exist;
   MEA_Action_Entry_Data_dbt             Action_Data;
   MEA_OutPorts_Entry_dbt                Action_OutPorts;
   MEA_EgressHeaderProc_Array_Entry_dbt  Action_Editing;
   MEA_EHP_Info_dbt                      Action_Editing_Info;
   MEA_Action_t                          Action_Id=0;
   MEA_Uint32                            Action_NumOfOwners=0;
   MEA_db_HwUnit_t                       action_hwUnit;
   MEA_db_HwId_t                         action_hwId=0;
   MEA_Bool                              current_action_force;
   MEA_SE_Entry_dbt                      se_entry;
   MEA_Bool action_exist;

   MEA_API_LOG



#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- Forwarder not support  \n",
            __FUNCTION__);

        return MEA_ERROR;
    }
    
    /* Check for valid entry parameter */
    if (entry_pio == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_pio == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* Check CFM OAM  support*/


    if (entry_pio->data.flowCoSMappingProfile_force) {

        if (!MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowCoSMappingProfile_force is not support \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry_pio->data.cos_force  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowCoSMappingProfile_force (%d) is not allowed "
                              " together with cos_force (%d)\n",
                              __FUNCTION__,
                              entry_pio->data.flowCoSMappingProfile_force,
                              entry_pio->data.cos_force);
            return MEA_ERROR;
        }

        if (MEA_API_Get_FlowCoSMappingProfile_Entry
                                    (unit,
                                    (MEA_FlowCoSMappingProfile_Id_t)(entry_pio->data.flowCoSMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Flow CoS Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              entry_pio->data.flowCoSMappingProfile_id);
            return MEA_ERROR;
        }

    }

    if (entry_pio->data.flowMarkingMappingProfile_force) {

        if (!MEA_FLOW_MARKING_MAPPING_PROFILE_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowMarkingMappingProfile_FORCE is not support \n",
                              __FUNCTION__);
            return MEA_ERROR;
        }

        if (entry_pio->data.l2pri_force  != MEA_ACTION_FORCE_COMMAND_DISABLE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - flowMarkingMappingProfile_FORCE (%d) is not allowed "
                              " together with l2pri_force (%d)\n",
                              __FUNCTION__,
                              entry_pio->data.flowMarkingMappingProfile_force,
                              entry_pio->data.l2pri_force);
            return MEA_ERROR;
        }


        if (MEA_API_Get_FlowMarkingMappingProfile_Entry
                                   (unit,
                                    (MEA_FlowMarkingMappingProfile_Id_t)(entry_pio->data.flowMarkingMappingProfile_id),
                                    NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Flow Marking Mapping Profile %d get failed \n",
                              __FUNCTION__,
                              entry_pio->data.flowMarkingMappingProfile_id);
            return MEA_ERROR;
        }

    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    MEA_OS_memset(&fwd_cmd, 0, sizeof(MEA_Forwarder_cmd_dbt));



	/* Erase the fwd_cmd structure */
    MEA_OS_memset(&fwd_cmd,       0 , sizeof(MEA_Forwarder_cmd_dbt));


	/* Build signature key */
	if (MEA_fwd_tbl_build_key(&(entry_pio->key),
		                      &(fwd_cmd.cmd.val.key.add_key.key)) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_build_key failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* Get the current entry */
    if ( MEA_fwd_tbl_readByKey( &(fwd_cmd.cmd.val.key.add_key.key),
                                &result) != MEA_OK ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_readByKey failed \n", __FUNCTION__);
		return MEA_ERROR;
    }

	/* Check if we found the entry */
    if (!result.val.info.match) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Entry not exist\n", __FUNCTION__);
		return MEA_ERROR;
    }






 #ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	if (entry_pio->data.LimiterId != 
		MEA_OS_read_value((MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_START,
						  (MEA_Uint32)MEA_FWD_DATA_LIMITER_ID_WIDTH,
						  &(result.val.data.val[0])
						  )) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - can not update LimiterId of exis Entry \n", __FUNCTION__);
		return MEA_ERROR;
	}
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   current_action_force = 
		    MEA_OS_read_value(MEA_FWD_DATA_ACTION_ID_VALID_START,
		  	   	              MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,
							  &(result.val.data.val[0]));


   if ( current_action_force) {
		if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000)  {
			action_hwUnit = MEA_HW_UNIT_ID_GENERAL;
		} else {
			action_hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
		}
		action_hwId = (MEA_db_HwId_t)MEA_OS_read_value(MEA_FWD_DATA_ACTION_ID_START,
													   MEA_FWD_DATA_ACTION_ID_WIDTH,
													   &(result.val.data.val[0]));
#ifdef MEA_ENET4000_FWD_OFFSET_ENABLE // T.B.D The FPGA add always the base by itself for ENET4000
		if (globalMemoryMode != MEA_MODE_REGULAR_ENET3000)  {
   		    action_hwId += MEA_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_START(action_hwUnit);
        }
#endif
        MEA_DRV_GET_SW_ID_WITH_EXIST(unit,
                                     mea_drv_Get_Action_db,
                                     action_hwUnit,
                                     action_hwId,
                                     Action_Id,
                                     action_exist,
                                     return MEA_ERROR);
        if (!action_exist) {
            if (action_hwUnit == MEA_HW_UNIT_ID_DOWNSTREAM) {
                action_hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
                MEA_DRV_GET_SW_ID_WITH_EXIST(unit,
                                             mea_drv_Get_Action_db,
                                             action_hwUnit,
                                             action_hwId,
                                             Action_Id,
                                             action_exist,
                                             return MEA_ERROR);
            }
        }
        if (!action_exist) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - not found SW id for action hwUnit (%d) / hwId (%d) \n",
                              __FUNCTION__,
                              action_hwUnit,
                              action_hwId);
            return MEA_ERROR;
        }



		if (MEA_API_IsExist_Action(MEA_UNIT_0,Action_Id,&exist) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_IsExist_Action failed (id=%d) \n",
                             __FUNCTION__,Action_Id);
           return MEA_ERROR;  
		}
		if (!exist) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - Current Action Id %d not exist \n",
                             __FUNCTION__,Action_Id);
           return MEA_ERROR;  
        }        

		if (MEA_API_GetOwner_Action(MEA_UNIT_0,
			                        Action_Id,
									&Action_NumOfOwners) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetOwner_Action failed (id=%d) \n",
                             __FUNCTION__,Action_Id);
           return MEA_ERROR;  
		}
								   
	}


   /* If the user entered all info NULL, and ED ID, PM ID, TM ID all 0 --> do not open action, just mark fid as false - 
      and disconnect from action 
   */
	if ( entry_pio->data.action.editId == 0 &&
		 entry_pio->data.action.tmId   == 0 &&
		 entry_pio->data.action.pmId   == 0 &&
		 entry_pio->data.color_force   == 0 &&
		 entry_pio->data.cos_force     == 0 &&
		 entry_pio->data.l2pri_force   == 0 &&
		 policer_pi == NULL          &&
		 editing_pi == NULL               ) {
	  
	   entry_pio->data.action.action_force = MEA_FALSE;
		

		if ( MEA_OS_read_value(MEA_FWD_DATA_ACTION_ID_VALID_START,
							   MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,
							   &(result.val.data.val[0]))) {
	   
		   if (Action_NumOfOwners == 1) {
			   if (MEA_API_Delete_Action (MEA_UNIT_0,
				                          Action_Id) != MEA_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                 "%s - MEA_API_Delete_Action failed (id=%d)\n",
					                 __FUNCTION__,Action_Id);
				   return MEA_ERROR;
			   }
		   } else {
			   if (MEA_API_DelOwner_Action (MEA_UNIT_0,
				                            Action_Id) != MEA_OK) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					                 "%s - MEA_API_DelOwner_Action failed (id=%d)\n",
					                 __FUNCTION__,Action_Id);
				   return MEA_ERROR;
			   }
		   }
	   }

	} else {

		if ((current_action_force      ) &&
		   (Action_NumOfOwners > 1     )  ) {
		   if (MEA_API_DelOwner_Action (MEA_UNIT_0,
			                            Action_Id) != MEA_OK) {
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				                 "%s - MEA_API_DelOwner_Action failed (id=%d)\n",
				                 __FUNCTION__,Action_Id);
			   return MEA_ERROR;
		   }
	   }

	   /* Build the Action Data */
	   MEA_OS_memset(&Action_Data         , 0 , sizeof(Action_Data        ));
	   Action_Data.tm_id              = entry_pio->data.action.tmId;
	   Action_Data.tm_id_valid        = (policer_pi==NULL && entry_pio->data.action.tmId==0) ? MEA_FALSE : MEA_TRUE;
       Action_Data.pm_type            = entry_pio->data.action.pm_type;
	   Action_Data.pm_id              = entry_pio->data.action.pmId;
	   Action_Data.pm_id_valid        = MEA_TRUE;
	   Action_Data.ed_id              = entry_pio->data.action.editId;
	   Action_Data.ed_id_valid        = (editing_pi==NULL && entry_pio->data.action.editId==0) ? MEA_FALSE : MEA_TRUE;
	   Action_Data.Llc                = (entry_pio->data.protocol_llc_force) ? entry_pio->data.Llc : 0;
	   Action_Data.proto_llc_valid    = MEA_TRUE;
	   Action_Data.protocol_llc_force = MEA_TRUE;
	   Action_Data.Protocol           = (entry_pio->data.protocol_llc_force) ? entry_pio->data.Protocol : 1;
	   Action_Data.force_color_valid  = entry_pio->data.color_force;
	   Action_Data.COLOR              = entry_pio->data.color;
	   Action_Data.force_cos_valid    = entry_pio->data.cos_force;
	   Action_Data.COS                = entry_pio->data.cos;
	   Action_Data.force_l2_pri_valid = entry_pio->data.l2pri_force;
	   Action_Data.L2_PRI             = entry_pio->data.l2pri;
       Action_Data.flowCoSMappingProfile_force     = (MEA_Uint16)entry_pio->data.flowCoSMappingProfile_force;
       Action_Data.flowCoSMappingProfile_id        = (MEA_Uint16)entry_pio->data.flowCoSMappingProfile_id;
       Action_Data.flowMarkingMappingProfile_force = (MEA_Uint16)entry_pio->data.flowMarkingMappingProfile_force;
       Action_Data.flowMarkingMappingProfile_id    = (MEA_Uint16)entry_pio->data.flowMarkingMappingProfile_id;

	   /* Build Action OutPorts */
	   MEA_OS_memset(&Action_OutPorts     , 0 , sizeof(Action_OutPorts     ));
		
	   /* Build Action editing */
	   MEA_OS_memset(&Action_Editing      , 0 , sizeof(Action_Editing     ));
	   MEA_OS_memset(&Action_Editing_Info , 0 , sizeof(Action_Editing_Info));
	   Action_Editing.num_of_entries = 1;
	   Action_Editing.ehp_info = &Action_Editing_Info;
	   if (editing_pi) {
	   	   MEA_OS_memcpy(&(Action_Editing.ehp_info[0].ehp_data),
			             editing_pi,
					     sizeof(Action_Editing.ehp_info[0].ehp_data));
	   }



		if ((current_action_force) &&
		    (Action_NumOfOwners == 1    )  ) {
   	       /* Update the Action  */
	       if (MEA_API_Set_Action (MEA_UNIT_0,
			                       Action_Id,
	  	                           &Action_Data,
			   				       &Action_OutPorts,
							       policer_pi,
								   (editing_pi) ? &Action_Editing : NULL) != MEA_OK) {
		       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                     "%s - MEA_API_Set_Action failed\n",
					   	         __FUNCTION__);
		       return MEA_ERROR;
		   }
	   } else {
		   MEA_Policer_Entry_dbt  Action_Policer;
		   MEA_Action_Entry_Data_dbt Action_Data_old;
		   if ((current_action_force  ) &&
		       ((editing_pi == NULL) || 
			    (policer_pi == NULL)  )  ) {

			   if (MEA_API_Get_Action(MEA_UNIT_0,
				                     Action_Id,
									 &Action_Data_old, /* data */
									 NULL, /* OutPorts */
									 &Action_Policer,
									 &Action_Editing) != MEA_OK) {

					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							          "%s - MEA_API_Set_Action failed\n",
					   			     __FUNCTION__);
					return MEA_ERROR;
			   }

			   if (editing_pi) {
	   			   MEA_OS_memcpy(&(Action_Editing.ehp_info[0].ehp_data),
							     editing_pi,
								 sizeof(Action_Editing.ehp_info[0].ehp_data));
			   }
		   }

   	       /* Create the Action  */
	       Action_Id = MEA_PLAT_GENERATE_NEW_ID;
	       if (MEA_API_Create_Action (MEA_UNIT_0,
	  	                              &Action_Data,
			   				          &Action_OutPorts,
									  ((current_action_force       ) && 
									   (policer_pi==NULL           ) &&
									   (Action_Data_old.tm_id_valid) && 
									   (Action_Data.tm_id_valid    ) &&
									   (Action_Data_old.tm_id == Action_Data.tm_id)) 
									   ? &Action_Policer : policer_pi,
							          ((current_action_force) || (editing_pi)) ? &Action_Editing : NULL,
							          &Action_Id) != MEA_OK) {
		       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                     "%s - MEA_API_Create_Action failed\n",
					   	         __FUNCTION__);
		       return MEA_ERROR;
		   }
	   }
	   entry_pio->data.action.action_force = MEA_TRUE;
	   entry_pio->data.action.editId       = Action_Data.ed_id;
	   entry_pio->data.action.tmId         = Action_Data.tm_id;
	   entry_pio->data.action.pmId         = Action_Data.pm_id;
   }

	/* Build the se_entry */
	MEA_OS_memset(&se_entry,0,sizeof(se_entry));
	MEA_OS_memcpy(&(se_entry.key),
		          &(entry_pio->key),
				  sizeof(se_entry.key));
	se_entry.data.actionId_valid = entry_pio->data.action.action_force;
	se_entry.data.actionId       = Action_Id;
	se_entry.data.aging = entry_pio->data.aging;
	se_entry.data.limiterId = entry_pio->data.LimiterId;
	se_entry.data.limiterId_valid = (entry_pio->data.LimiterId != MEA_LIMITER_DONT_CARE);
	MEA_OS_memcpy(&(se_entry.data.OutPorts),
		          &(entry_pio->data.OutPorts),
				  sizeof(se_entry.data.OutPorts));
	se_entry.data.sa_discard                   = entry_pio->data.sa_discard;
	se_entry.data.static_flag                  = entry_pio->data.static_flag;
	se_entry.data.valid                        = entry_pio->data.valid;
        se_entry.data.access_port       = entry_pio->data.access_port;
     
    if (MEA_API_Set_SE_Entry(unit,&se_entry) != MEA_OK) {
		// T.B.D Rollback to the Action changes
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_API_Create_SE_Entry failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}



	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_Get_DSE_Entry>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_DSE_Entry       (MEA_Unit_t						unit_i,
                                        MEA_DSE_Entry_dbt		       *entry_pio,
										MEA_Policer_Entry_dbt          *policer_po,
				    			        MEA_EgressHeaderProc_Entry_dbt *editing_po)
{

   MEA_Forwarder_Result_dbt       result;
   MEA_Forwarder_Signature_dbt    signature;

   MEA_API_LOG


 #ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check for valid entry_pio parameter */
    if (entry_pio == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_pio == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



	/* Build signature */
	if (MEA_fwd_tbl_build_key(&(entry_pio->key),
		                      &(signature.val.key)) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_build_key failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	/* Read from HW */
	if (MEA_fwd_tbl_readByKey(&signature.val.key,
                              &result) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_readByKey failed\n", __FUNCTION__);
		return MEA_ERROR;
    }

	/* Check if we found the entry */
    if (!result.val.info.match) {
#if 0
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Entry not found\n", __FUNCTION__);
#endif
		return MEA_ERROR;
    }


	if (MEA_fwd_tbl_convert_to_DSE_Entry(unit_i,
										 &(signature.val.key),
		                                 &(result.val.data),
									 	 entry_pio,
										 policer_po,
										 editing_po) != MEA_OK) {
			
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_convert_to_DSE_Entry failed\n", 
						  __FUNCTION__);
		return MEA_ERROR;
	}



	return MEA_OK;								     
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_GetFirst_DSE_Entry>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetFirst_DSE_Entry   (MEA_Unit_t                      unit_i,
                                         MEA_DSE_Entry_dbt              *entry_po,
							   		     MEA_Policer_Entry_dbt          *policer_po, /* Can be NULL */
				    			         MEA_EgressHeaderProc_Entry_dbt *editing_po, /* Can be NULL */
                                         MEA_Bool*                       found_o)
{

   MEA_Forwarder_Signature_dbt  fwd_entry_signature;
   MEA_Forwarder_Result_dbt     fwd_entry_result;

   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
   if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s \n- Forwarder not support  \n",
           __FUNCTION__);

       return MEA_ERROR;
    }
   if (entry_po == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - entry_po == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
   if (found_o == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - found_o == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
 
   MEA_drv_fwd_last_scan_hash_type = 0;
   MEA_OS_memset(&fwd_entry_signature,0,sizeof(fwd_entry_signature));
   MEA_OS_memset(&fwd_entry_result   ,0,sizeof(fwd_entry_result   ));

   if (MEA_fwd_tbl_GetFirst (&MEA_drv_fwd_last_scan_hash_type,
                             &fwd_entry_signature,
                             &fwd_entry_result   ,
                             found_o ) != MEA_OK ) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                "%s - MEA_fwd_tbl_GetFirst failed \n",
						__FUNCTION__);
 	  return MEA_ERROR;
   }

   if ((*found_o) == MEA_FALSE) {
	   return MEA_OK;
   }

   if (MEA_fwd_tbl_convert_to_DSE_Entry(unit_i,
	                                    &fwd_entry_signature.val.key,
	                                    &fwd_entry_result.val.data,
 							 	        entry_po,
										policer_po,
										editing_po) != MEA_OK) {
			
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_convert_to_DSE_Entry failed\n", 
						  __FUNCTION__);
		return MEA_ERROR;
	}

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      <MEA_API_GetNext_DSE_Entry>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_GetNext_DSE_Entry   (MEA_Unit_t                      unit_i,
                                        MEA_DSE_Entry_dbt              *entry_po,
										MEA_Policer_Entry_dbt          *policer_po, /* Can be NULL */
				    			        MEA_EgressHeaderProc_Entry_dbt *editing_po, /* Can be NULL */
                                        MEA_Bool                       *found_o)
{

   MEA_Forwarder_Signature_dbt  fwd_entry_signature;
   MEA_Forwarder_Result_dbt     fwd_entry_result;

   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s \n- Forwarder not support  \n",
        __FUNCTION__);

        return MEA_ERROR;
    }
    
   if (entry_po == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - entry_po == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
   if (found_o == NULL) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                 "%s - found_o == NULL \n",
						 __FUNCTION__);
	   return MEA_ERROR;
   }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   MEA_OS_memset(&fwd_entry_signature,0,sizeof(fwd_entry_signature));
   MEA_OS_memset(&fwd_entry_result   ,0,sizeof(fwd_entry_result   ));

   if (MEA_fwd_tbl_GetNext (&MEA_drv_fwd_last_scan_hash_type,
                             &fwd_entry_signature,
                             &fwd_entry_result   ,
                             found_o ) != MEA_OK ) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                "%s - MEA_fwd_tbl_GetFirst failed \n",
						__FUNCTION__);
 	  return MEA_ERROR;
   }

   if ((*found_o) == MEA_FALSE) {
	   return MEA_OK;
   }

   if (MEA_fwd_tbl_convert_to_DSE_Entry(unit_i,
	                                    &fwd_entry_signature.val.key,
	                                    &fwd_entry_result.val.data,
 							 	        entry_po,
										policer_po,
										editing_po) != MEA_OK) {
			
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - MEA_fwd_tbl_convert_to_DSE_Entry failed\n", 
						  __FUNCTION__);
		return MEA_ERROR;
	}

	return MEA_OK;
}
MEA_Status MEA_API_Set_DSE_Aging (MEA_Unit_t unit,
                                 MEA_Uint32 seconds,
                                 MEA_Bool   admin_status) 
{

    MEA_API_LOG
    
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s \n- Forwarder not support  \n",
        __FUNCTION__);

    return MEA_ERROR;
    }
    
    return MEA_API_Set_SE_Aging(unit,seconds,admin_status);

}
MEA_Status MEA_API_Get_DSE_Aging  (MEA_Unit_t unit,
                                   MEA_Uint32 *seconds,
                                   MEA_Bool   *admin_status)
{
    MEA_API_LOG
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s \n- Forwarder not support  \n",
                           __FUNCTION__);
        return MEA_ERROR;
    }
		
		
	return MEA_API_Get_SE_Aging(unit,seconds,admin_status);
	
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_API_delete_all_forwarder_entries>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_all_forwarder_entries(MEA_Unit_t unit) 
{
    if(!MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
    return MEA_OK;
    }

   if (MEA_API_DeleteAll_DSE(unit) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_DeleteAll_DSE failed \n",
                         __FUNCTION__);
      return MEA_ERROR;
   }

   /* Return to caller */
   return MEA_OK;

}


#endif
//////////////////////FWD Filter ///////////////////////////////////////////////


MEA_Status MEA_fwd_tbl_build_data_filter_MASK(MEA_Unit_t                unit_i,
                                      MEA_SE_Entry_data_dbt    *data_pi,
                                      MEA_Forwarder_Data_dbt   *data_po)
{

    //ENET_xPermissionId_t      xPermissionId;
//     MEA_db_HwId_t          action_hwId;
//     MEA_db_HwUnit_t        action_hwUnit;
//    MEA_Action_HwEntry_dbt  action_info;
    MEA_Uint32 count_shift;

    if (data_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - data_pi == NULL \n",__FUNCTION__);
        return MEA_ERROR;
    }

    if (data_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - data_po == NULL \n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memset((char*)data_po,0,sizeof(*data_po));


#ifdef MEA_NEW_FWD_ROW_SET

    count_shift = 0;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_STATIC_WIDTH,
        (MEA_Uint32)(data_pi->static_flag),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_STATIC_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_AGE_WIDTH,
        (MEA_Uint32)(data_pi->aging),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_AGE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_XPERMISSION_ID_WIDTH,
        data_pi->xPermissionId, //(MEA_Uint32)(xPermissionId),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_FWD_DATA_RESERV_WIDTH,
		0, 
		&(data_po->val[0]));
	count_shift += MEA_FWD_DATA_RESERV_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_FWD_DATA_SA_DISCARD_WIDTH,
		(MEA_Uint32)(data_pi->sa_discard),
		&(data_po->val[0]));
	count_shift += MEA_FWD_DATA_SA_DISCARD_WIDTH;


    MEA_OS_insert_value(count_shift,MEA_FWD_DATA_ACCESS_PORT_WIDTH, (MEA_Uint32)(data_pi->access_port  ),
        &(data_po->val[0]));
	count_shift += MEA_FWD_DATA_ACCESS_PORT_WIDTH;

    

  

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,
        (MEA_Uint32)(data_pi->actionId_valid),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACTION_ID_WIDTH,
        data_pi->actionId,//(MEA_Uint32)(action_hwId),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;



    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_LIMITER_ID_WIDTH,
        (MEA_Uint32)((data_pi->limiterId_valid) 
        ? data_pi->limiterId : MEA_LIMITER_DONT_CARE),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_LIMITER_ID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_RESERV1_WIDTH,
        (MEA_Uint32)(0),
        &(data_po->val[0]));
	count_shift += MEA_FWD_DATA_RESERV1_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_PARITY_WIDTH,
        (MEA_Uint32)(0),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_PARITY_WIDTH;
#else
    count_shift = 0;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_STATIC_WIDTH,
        (MEA_Uint32)(data_pi->static_flag),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_STATIC_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_AGE_WIDTH,
        (MEA_Uint32)(data_pi->aging),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_AGE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_XPERMISSION_ID_WIDTH,
        data_pi->xPermissionId, //(MEA_Uint32)(xPermissionId),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH,
        0,//(MEA_Uint32)(action_info.mc_fid_skip), 
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACCESS_PORT_BIT0_2,
        (MEA_Uint32)(data_pi->access_port & 0x7 ),
        &(data_po->val[0]));
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT0_2;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_SA_DISCARD_WIDTH,
        (MEA_Uint32)(data_pi->sa_discard),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_SA_DISCARD_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_COS_FORCE_WIDTH,
        0,//(MEA_Uint32)(action_info.cos_force),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_COS_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_COS_WIDTH,
        0,//(MEA_Uint32)(action_info.cos),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_COS_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_PRI_FORCE_WIDTH,
        0,//(MEA_Uint32)(action_info.l2_pri_force),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_PRI_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_PRI_WIDTH,
        0,//(MEA_Uint32)(action_info.l2_pri),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_PRI_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_COL_FORCE_WIDTH,
        0,//(MEA_Uint32)(action_info.color_force),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_COL_FORCE_WIDTH; 

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_COL_WIDTH,
        0,//(MEA_Uint32)(action_info.color),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_COL_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,
        (MEA_Uint32)(data_pi->actionId_valid),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACTION_ID_WIDTH,
        data_pi->actionId,//(MEA_Uint32)(action_hwId),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;
#if 0
    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH,
        (MEA_Uint32)(data_pi->cfm_oam_me_level_valid),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_WIDTH,
        (MEA_Uint32)(data_pi->cfm_oam_me_level_value),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_WIDTH;
#else
    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACCESS_PORT_BIT3_7,
        (MEA_Uint32)((data_pi->access_port >>3 ) & 0x1f),
        &(data_po->val[0]));
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT3_7;

#endif


    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_LIMITER_ID_WIDTH,
        (MEA_Uint32)((data_pi->limiterId_valid) 
        ? data_pi->limiterId : MEA_LIMITER_DONT_CARE),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_LIMITER_ID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACCESS_PORT_BIT_8,
        (MEA_Uint32)(data_pi->access_port >>8),
        &(data_po->val[0]));
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT_8;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_PARITY_WIDTH,
        (MEA_Uint32)(0),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_PARITY_WIDTH;

#endif //MEA_NEW_FWD_ROW_SET
    return MEA_OK;
}


MEA_Status MEA_fwd_tbl_build_data_filter_fields(MEA_Unit_t                unit_i,
                                      MEA_SE_Entry_data_dbt    *data_pi,
                                      MEA_Forwarder_Data_dbt   *data_po)
{

    
    //MEA_db_HwId_t          action_hwId;
//    MEA_db_HwUnit_t        action_hwUnit;
//    MEA_Action_HwEntry_dbt  action_info;
     ENET_xPermissionId_t xPermissionId=0;
    MEA_Uint32 count_shift;
    ENET_Bool found_o;

    if (data_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - data_pi == NULL \n",__FUNCTION__);
        return MEA_ERROR;
    }

    if (data_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - data_po == NULL \n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memset((char*)data_po,0,sizeof(*data_po));

    if(data_pi->xPermissionId_valid == MEA_FALSE && data_pi->OutPorts_valid == MEA_TRUE){
         if(enet_GetIDByEntry_xPermission(unit_i,
             &data_pi->OutPorts,
              0,
             &xPermissionId,
             &found_o)!=MEA_OK){

         }
         if(found_o==MEA_FALSE){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The out cluster not found \n",__FUNCTION__);
             return MEA_ERROR;
         }
          data_pi->xPermissionId=xPermissionId;
    }

#ifdef MEA_NEW_FWD_ROW_SET
    count_shift = 0;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_STATIC_WIDTH,
        (MEA_Uint32)(data_pi->static_flag),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_STATIC_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_AGE_WIDTH,
        (MEA_Uint32)(data_pi->aging),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_AGE_WIDTH;

    if(data_pi->xPermissionId_valid == MEA_TRUE && data_pi->OutPorts_valid == MEA_FALSE){

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_XPERMISSION_ID_WIDTH,
        data_pi->xPermissionId, //(MEA_Uint32)(xPermissionId),
        &(data_po->val[0]));
    }else{
        MEA_OS_insert_value(count_shift,
            MEA_FWD_DATA_XPERMISSION_ID_WIDTH,
            xPermissionId, //(MEA_Uint32)(xPermissionId),
            &(data_po->val[0]));
       
    }
    count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_RESERV_WIDTH,
        0,//(MEA_Uint32)(action_info.mc_fid_skip), 
        &(data_po->val[0]));
	count_shift += MEA_FWD_DATA_RESERV_WIDTH;

	MEA_OS_insert_value(count_shift,
		MEA_FWD_DATA_SA_DISCARD_WIDTH,
		(MEA_Uint32)(data_pi->sa_discard),
		&(data_po->val[0]));
	count_shift += MEA_FWD_DATA_SA_DISCARD_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACCESS_PORT_WIDTH,
        (MEA_Uint32)(data_pi->access_port ),
        &(data_po->val[0]));
	count_shift += MEA_FWD_DATA_ACCESS_PORT_WIDTH;

  

  

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,
        (MEA_Uint32)(data_pi->actionId_valid),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACTION_ID_WIDTH,
        data_pi->actionId,//(MEA_Uint32)(action_hwId),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;



    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_LIMITER_ID_WIDTH,
        (MEA_Uint32)((data_pi->limiterId_valid) 
        ? data_pi->limiterId : MEA_LIMITER_DONT_CARE),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_LIMITER_ID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_RESERV1_WIDTH,
        (MEA_Uint32)(0),
        &(data_po->val[0]));
	count_shift += MEA_FWD_DATA_RESERV1_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_PARITY_WIDTH,
        (MEA_Uint32)(0),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_PARITY_WIDTH;
#else

    count_shift = 0;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_STATIC_WIDTH,
        (MEA_Uint32)(data_pi->static_flag),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_STATIC_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_AGE_WIDTH,
        (MEA_Uint32)(data_pi->aging),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_AGE_WIDTH;

    if(data_pi->xPermissionId_valid == MEA_TRUE && data_pi->OutPorts_valid == MEA_FALSE){

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_XPERMISSION_ID_WIDTH,
        data_pi->xPermissionId, //(MEA_Uint32)(xPermissionId),
        &(data_po->val[0]));
    }else{
        MEA_OS_insert_value(count_shift,
            MEA_FWD_DATA_XPERMISSION_ID_WIDTH,
            xPermissionId, //(MEA_Uint32)(xPermissionId),
            &(data_po->val[0]));
       
    }
    count_shift+=MEA_FWD_DATA_XPERMISSION_ID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH,
        0,//(MEA_Uint32)(action_info.mc_fid_skip), 
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACCESS_PORT_BIT0_2,
        (MEA_Uint32)(data_pi->access_port & 0x7),
        &(data_po->val[0]));
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT0_2;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_SA_DISCARD_WIDTH,
        (MEA_Uint32)(data_pi->sa_discard),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_SA_DISCARD_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_COS_FORCE_WIDTH,
        0,//(MEA_Uint32)(action_info.cos_force),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_COS_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_COS_WIDTH,
        0,//(MEA_Uint32)(action_info.cos),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_COS_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_PRI_FORCE_WIDTH,
        0,//(MEA_Uint32)(action_info.l2_pri_force),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_PRI_FORCE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_PRI_WIDTH,
        0,//(MEA_Uint32)(action_info.l2_pri),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_PRI_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_COL_FORCE_WIDTH,
        0,//(MEA_Uint32)(action_info.color_force),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_COL_FORCE_WIDTH; 

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_COL_WIDTH,
        0,//(MEA_Uint32)(action_info.color),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_COL_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACTION_ID_VALID_WIDTH,
        (MEA_Uint32)(data_pi->actionId_valid),
        &(data_po->val[0]));
    count_shift+=MEA_FWD_DATA_ACTION_ID_VALID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACTION_ID_WIDTH,
        data_pi->actionId,//(MEA_Uint32)(action_hwId),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_ACTION_ID_WIDTH;
#if 0
    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH,
        (MEA_Uint32)(data_pi->cfm_oam_me_level_valid),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_WIDTH,
        (MEA_Uint32)(data_pi->cfm_oam_me_level_value),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_WIDTH;
#else
    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACCESS_PORT_BIT3_7,
        (MEA_Uint32)((data_pi->access_port>>3) & 0x7),
        &(data_po->val[0]));
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT3_7;
#endif


    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_LIMITER_ID_WIDTH,
        (MEA_Uint32)((data_pi->limiterId_valid) 
        ? data_pi->limiterId : MEA_LIMITER_DONT_CARE),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_LIMITER_ID_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_ACCESS_PORT_BIT_8,
        (MEA_Uint32)(0),
        &(data_po->val[0]));
    count_shift += MEA_FWD_DATA_ACCESS_PORT_BIT_8;

    MEA_OS_insert_value(count_shift,
        MEA_FWD_DATA_PARITY_WIDTH,
        (MEA_Uint32)(0),
        &(data_po->val[0]));
    count_shift+= MEA_FWD_DATA_PARITY_WIDTH;
#endif //MEA_NEW_FWD_ROW_SET



    return MEA_OK;
}




MEA_Status MEA_fwd_tbl_build_keyMASK(MEA_SE_Entry_key_dbt  *key_pi,
                                     MEA_Forwarder_Key_dbt *key_po)
{


    MEA_SE_Forwarding_key_type_te key_type;
    MEA_MacAddr                    mac_addr;
    MEA_Uint16                     vpn;
#ifdef PPP_MAC
    MEA_Uint16                     PPPoE_sessionId;
#endif    
	MEA_Uint16                     vlan;

    if (key_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - key_pi == NULL \n",__FUNCTION__);
        return MEA_ERROR;
    }

    if (key_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	"%s - key_po == NULL \n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memset(key_po,0,sizeof(*key_po));

    key_type = key_pi->type;

    switch (key_type) {
    case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
        MEA_OS_memcpy(&(mac_addr.b[0]),
            &(key_pi->mac_plus_vpn.MAC.b[0]),
            sizeof(mac_addr.b));
        vpn = (MEA_Uint16)(key_pi->mac_plus_vpn.VPN);

        key_po->mac_plus_vpn.mac_32_47 = 
            ((((MEA_Uint32)((unsigned char)(mac_addr.b[1]))) <<  0) |  
            (((MEA_Uint32)((unsigned char)(mac_addr.b[0]))) <<  8) );

        key_po->mac_plus_vpn.mac_0_31 = 
            ((((MEA_Uint32)((unsigned char)(mac_addr.b[5]))) <<  0) |  
            (((MEA_Uint32)((unsigned char)(mac_addr.b[4]))) <<  8) |
            (((MEA_Uint32)((unsigned char)(mac_addr.b[3]))) << 16) |  
            (((MEA_Uint32)((unsigned char)(mac_addr.b[2]))) << 24) );

        key_po->mac_plus_vpn.vpn = vpn;
        key_po->mac_plus_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        key_po->mac_plus_vpn.valid = 1;

        break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
    case MEA_SE_FORWARDING_KEY_TYPE_DA :
        MEA_OS_memcpy(&(mac_addr.b[0]),
            &(key_pi->mac.MAC.b[0]),
            sizeof(mac_addr.b));

        key_po->mac.mac_32_47 = 
            ((((MEA_Uint32)((unsigned char)(mac_addr.b[1]))) <<  0) |  
            (((MEA_Uint32)((unsigned char)(mac_addr.b[0]))) <<  8) );

        key_po->mac.mac_0_31 = 
            ((((MEA_Uint32)((unsigned char)(mac_addr.b[5]))) <<  0) |  
            (((MEA_Uint32)((unsigned char)(mac_addr.b[4]))) <<  8) |
            (((MEA_Uint32)((unsigned char)(mac_addr.b[3]))) << 16) |  
            (((MEA_Uint32)((unsigned char)(mac_addr.b[2]))) << 24) );

        //key_po->mac.dont_care1 = 0xfff; // fix bug FWD MAC only

        key_po->mac.key_type = 1;
        key_po->mac.valid = 1;

        break;
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :
        key_po->mpls_vpn.label_1      = (key_pi->mpls_vpn.label_1);
        key_po->mpls_vpn.label_2_lsb  = (key_pi->mpls_vpn.label_2) & 0xfff;
        key_po->mpls_vpn.label_2_msb  = (key_pi->mpls_vpn.label_2>>12) & 0xff;
        key_po->mpls_vpn.vpn          = (key_pi->mpls_vpn.VPN);
        key_po->mpls_vpn.dont_care1   = 0x0;
        key_po->mpls_vpn.key_type     = MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN;
        key_po->vlan_vpn.valid        = 1;

        break;
#ifdef PPP_MAC 
    case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID :
        MEA_OS_memcpy(&(mac_addr.b[0]),
            &(key_pi->mac_plus_pppoe_session_id.MAC.b[0]),
            sizeof(mac_addr.b));
        PPPoE_sessionId = (MEA_Uint16)(key_pi->mac_plus_pppoe_session_id.PPPoE_session_id);

        key_po->mac_plus_pppoe_session_id.mac_32_47 = 
            ((((MEA_Uint32)((unsigned char)(mac_addr.b[1]))) <<  0) |  
            (((MEA_Uint32)((unsigned char)(mac_addr.b[0]))) <<  8) );

        key_po->mac_plus_pppoe_session_id.mac_0_31 = 
            ((((MEA_Uint32)((unsigned char)(mac_addr.b[5]))) <<  0) |  
            (((MEA_Uint32)((unsigned char)(mac_addr.b[4]))) <<  8) |
            (((MEA_Uint32)((unsigned char)(mac_addr.b[3]))) << 16) |  
            (((MEA_Uint32)((unsigned char)(mac_addr.b[2]))) << 24) );

        key_po->mac_plus_pppoe_session_id.pppoe_session_id_lsb = PPPoE_sessionId;

        key_po->mac_plus_pppoe_session_id.key_type = 2;
        key_po->mac_plus_pppoe_session_id.valid = 1;

        break;
#else
	case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
	
		
		key_po->pppoe_session_id_vpn.pppoe_session_id = key_pi->PPPOE_plus_vpn.PPPoE_session_id;

		key_po->pppoe_session_id_vpn.key_type = MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN;
		key_po->pppoe_session_id_vpn.valid = 1;

		break;

#endif

    case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :
        vlan                        = (MEA_Uint16)(key_pi->vlan_vpn.VLAN);
        vpn                         = (MEA_Uint16)(key_pi->vlan_vpn.VPN);
        key_po->vlan_vpn.vlan_to_enable = key_pi->vlan_vpn.VLAN_to_enable;
        key_po->vlan_vpn.vlan_to_val    = key_pi->vlan_vpn.VLAN_to_val;
        //key_po->vlan_vpn.dont_care0 = 0x0007FFFF;
        key_po->vlan_vpn.dont_care1 = 0;
        key_po->vlan_vpn.vlan       = vlan;
        key_po->vlan_vpn.vpn        = vpn;
        key_po->vlan_vpn.key_type   = 3;
        key_po->vlan_vpn.valid      = 1;

        break;
    case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :
        if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {

            key_po->oam_vpn.reserverd_1   = 0;
            key_po->oam_vpn.label_1       = key_pi->oam_vpn.label_1;
            key_po->oam_vpn.vpn           = key_pi->oam_vpn.VPN;
            
            key_po->oam_vpn.MD_level                = key_pi->oam_vpn.MD_level;
            key_po->oam_vpn.op_code                 = key_pi->oam_vpn.op_code;
            
            key_po->oam_vpn.packet_is_untag_mpls   = key_pi->oam_vpn.packet_is_untag_mpls;
            key_po->oam_vpn.Src_port               = key_pi->oam_vpn.Src_port;
            key_po->oam_vpn.key_type   = 4;
            key_po->oam_vpn.valid      = 1;
            break;
        }
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:

        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4_23_0  = (key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 & 0x00ffffff);
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4_23_8  =((key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4>>8) & 0x0000ffff);
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4_7_0   =((key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4)& 0x000000ff);
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.vpn           = key_pi->DstIPv4_plus_SrcIPv4_plus_vpn.VPN;
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.key_type      = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
        key_po->DstIPv4_plus_SrcIPv4_plus_vpn.valid      = 1;

        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
        key_po->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_0_15  = key_pi->DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 & 0x0000ffff;
        key_po->DstIPv4_plus_L4DstPort_plus_vpn.IPv4_bit_16_32 = (key_pi->DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 >> 16) & 0x0000ffff;
        key_po->DstIPv4_plus_L4DstPort_plus_vpn.L4Port         = key_pi->DstIPv4_plus_L4_DstPort_plus_vpn.L4port;
        key_po->DstIPv4_plus_L4DstPort_plus_vpn.vpn            = key_pi->DstIPv4_plus_L4_DstPort_plus_vpn.VPN;
        key_po->DstIPv4_plus_L4DstPort_plus_vpn.key_type       = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
        key_po->DstIPv4_plus_L4DstPort_plus_vpn.valid          = 1;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        key_po->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_0_15  = key_pi->SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4 & 0x0000ffff;;
        key_po->SrcIPv4_plus_L4srcPort_plus_vpn.IPv4_bit_16_32 = (key_pi->SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4 >> 16) &  0x0000ffff ;
        key_po->SrcIPv4_plus_L4srcPort_plus_vpn.L4Port        = key_pi->SrcIPv4_plus_L4_srcPort_plus_vpn.L4port;
        key_po->SrcIPv4_plus_L4srcPort_plus_vpn.vpn            = key_pi->SrcIPv4_plus_L4_srcPort_plus_vpn.VPN;
        key_po->SrcIPv4_plus_L4srcPort_plus_vpn.key_type       = MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
        key_po->SrcIPv4_plus_L4srcPort_plus_vpn.valid          = 1;
        break;


    case MEA_SE_FORWARDING_KEY_TYPE_LAST :
    default: 
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - error3 Unknown key type (%d)\n", __FUNCTION__,key_type);
        return MEA_ERROR;
    }

    /* Return to caller */
    return MEA_OK;
}

static void mea_drv_fwd_filterSearch_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_FWD_hw_filter * entry= (MEA_FWD_hw_filter * )arg4;
/* Update Hw Entry */
   
    MEA_Uint32                 val[8];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;
    
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }


    val[0] = entry->fields_data.val[0];
    val[1] = entry->fields_data.val[1];
    val[2] = entry->fields_key.raw.signature0;
    val[3] = entry->fields_key.raw.signature1;
    val[3] |=entry->fields_key.raw.key_type<<28;
    val[3] |=entry->fields_key.raw.valid<<31;
    
    
    
    val[4] = entry->mask_data.val[0];
    val[5] = entry->mask_data.val[1];
    val[6] = entry->mask_key.raw.signature0;
    val[7] = entry->mask_key.raw.signature1;
    val[7] |=entry->mask_key.raw.key_type<<28;
    val[7] |=entry->mask_key.raw.valid<<31;

    



    
    
    
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
        MEA_IF_WRITE_DATA_REG(i),
        val[i],
        MEA_MODULE_IF);  
    }

}


MEA_Status mea_fwd_write_filterSearch(MEA_Unit_t unit_i,MEA_Uint8 index)
{

    MEA_ind_write_t	            ind_write;
    MEA_FWD_hw_filter           fwd_filter_HW;



    MEA_OS_memcpy(&fwd_filter_HW,&MEA_drv_fwd_filter_tble[index].hw,sizeof(fwd_filter_HW));


    ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_REG;
    ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_MASK_RESULT_WRITE;
    ind_write.cmdReg		= MEA_IF_CMD_REG;      
    ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg		= MEA_IF_STATUS_REG;   
    ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry   	= (MEA_FUNCPTR)mea_drv_fwd_filterSearch_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)(unit_i);
    
    ind_write.funcParam4 = (MEA_funcParam)(&fwd_filter_HW);


    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect failed "
            "(module=%d , tableType=%d , tableOffset=%d)\n",
            __FUNCTION__,
            MEA_MODULE_IF,
            ind_write.tableType,
            ind_write.tableOffset);
        
        return MEA_ERROR;
    }


return MEA_OK;

}




















MEA_Status MEA_API_SE_Entry_ClearFilter (MEA_Unit_t         unit_i,
                                         MEA_Uint8          index)
{
    
    
    if(index == 0 || index >=MEA_FORWARDER_FILTER_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d not available \n",
            __FUNCTION__,index);
        return MEA_ERROR; 
    }




    return MEA_OK;
}

MEA_Status MEA_API_SE_Entry_GetFilter (MEA_Unit_t               unit_i,
                                       MEA_Uint16                index,
                                       MEA_SE_filter_Entry_dbt  *entry_o)
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid data parameter */
    if (entry_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_o == NULL\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

#endif
    if(index == 0 || index >=MEA_FORWARDER_FILTER_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d not available \n",
            __FUNCTION__,index);
        return MEA_ERROR; 
    }

    MEA_OS_memcpy(entry_o,&MEA_drv_fwd_filter_tble[index].entry,sizeof(*entry_o));

    
    
    return MEA_OK;
}

MEA_Status MEA_API_SE_Entry_CreateFilter (MEA_Unit_t               unit_i,
                                          MEA_Uint16                index)
{

    if(index <= 1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d on allowed to change\n",__FUNCTION__,index);
        return MEA_ERROR; 

    }

    if(index >= MEA_FORWARDER_FILTER_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d out of range\n",__FUNCTION__,index);
        return MEA_ERROR; 

    }

    if(MEA_drv_fwd_filter_tble[index].valid == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - profile %d already exists \n",__FUNCTION__,index);
        return MEA_ERROR;
    }
    
    MEA_drv_fwd_filter_tble[index].valid=MEA_TRUE;
    
    return MEA_OK;
}

MEA_Status MEA_API_SE_Entry_DeleteFilter (MEA_Unit_t               unit_i,
                                          MEA_Uint16                index)
{

    
    if(!MEA_GLOBALE_FORWADER_FILTER_SUPPORT)
        return MEA_ERROR;
    
    if(index <= 1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d on allowed to delete\n",__FUNCTION__,index);
        return MEA_ERROR; 

    }

    if(index >= MEA_FORWARDER_FILTER_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d out of range\n",__FUNCTION__,index);
        return MEA_ERROR; 

    }

    if(MEA_drv_fwd_filter_tble[index].valid != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - profile %d is not create \n",__FUNCTION__,index);
        return MEA_ERROR;
    }

    MEA_drv_fwd_filter_tble[index].valid=MEA_FALSE;

    MEA_OS_memset(&MEA_drv_fwd_filter_tble[index],0,sizeof(MEA_drv_fwd_filter_tble[index]));


    return MEA_OK;
}





MEA_Status MEA_API_SE_Entry_SetFilter (MEA_Unit_t               unit_i,
                                       MEA_Uint16                index,
                                       MEA_SE_filter_Entry_dbt  *entry_pi)
{
    MEA_Forwarder_Data_dbt   mask_data,fields_data;
    MEA_Forwarder_Key_dbt mask_key,fields_key;
    ENET_xPermissionId_t xPermissionId;
    MEA_Bool found_o;
    //MEA_Bool exist;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(!MEA_GLOBALE_FORWADER_FILTER_SUPPORT)
        return MEA_ERROR;

/* Check for valid data parameter */
    if (entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_pi == NULL\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }


#endif

    MEA_OS_memset(&mask_key,0,sizeof(mask_key));
    MEA_OS_memset(&fields_key,0,sizeof(fields_key));


    if(index == 0){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d on allowed to change\n",__FUNCTION__,index);
        return MEA_ERROR; 

    }

    if(index >= MEA_FORWARDER_FILTER_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d out of range\n",__FUNCTION__,index);
        return MEA_ERROR; 

    }

    if(MEA_drv_fwd_filter_tble[index].valid != MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - index %d not create \n",__FUNCTION__,index);
        return MEA_ERROR;
    }

    if(entry_pi->fwd_fields_data.OutPorts_valid == MEA_TRUE &&
        entry_pi->fwd_fields_data.xPermissionId_valid == MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - index %d not allowed to set out with xPermissionId  \n",__FUNCTION__,index);
            return MEA_ERROR;
    }


    if(entry_pi->fwd_mask_data.xPermissionId_valid == MEA_TRUE && entry_pi->fwd_mask_data.OutPorts_valid == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The xPermissionId_valid and OutPorts_valid can't be set together\n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(entry_pi->fwd_mask_data.xPermissionId_valid == MEA_FALSE && entry_pi->fwd_mask_data.OutPorts_valid == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The OutPorts_valid can't be set on mask\n",__FUNCTION__);
        return MEA_ERROR;
    }


    //*******************Check the cluster to xPermision ******************************************
    if(entry_pi->fwd_fields_data.xPermissionId_valid == MEA_TRUE && entry_pi->fwd_fields_data.OutPorts_valid == MEA_TRUE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The xPermissionId_valid and OutPorts_valid can't be set together\n",__FUNCTION__);
       return MEA_ERROR;
    }

    if(entry_pi->fwd_fields_data.xPermissionId_valid == MEA_FALSE && entry_pi->fwd_fields_data.OutPorts_valid == MEA_TRUE){
        if(enet_GetIDByEntry_xPermission(unit_i,
            &entry_pi->fwd_fields_data.OutPorts,
            0,
            &xPermissionId,
            &found_o)!=MEA_OK){

        }
        if(found_o==MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - xPermission not found for out cluster  \n",__FUNCTION__);
            return MEA_ERROR;
        }
        entry_pi->fwd_fields_data.xPermissionId=xPermissionId;
    }
    
    
#if 0
    /* Check valid ActionId */
    if (entry_pi->fwd_fields_data.actionId_valid) {
        if (MEA_API_IsExist_Action(unit_i,
            entry_pi->fwd_fields_data.actionId,
            &exist) == MEA_ERROR){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_IsExist_Action failed (id=%d)\n",
                    __FUNCTION__,entry_pi->fwd_fields_data.actionId);
                return MEA_ERROR;
        }
        if(!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ActionId %d does not exist  \n",
                __FUNCTION__,entry_pi->fwd_fields_data.actionId);
            return MEA_ERROR;

        }
    }
#endif


    
    
    MEA_OS_memcpy(&MEA_drv_fwd_filter_tble[index].entry,entry_pi,sizeof(MEA_drv_fwd_filter_tble[0].entry));

 
    if(entry_pi->fwd_reg_enable)
    {
        MEA_OS_memcpy(&MEA_drv_fwd_filter_tble[index].hw.fields_data,&entry_pi->fwd_fields_data_reg,sizeof(MEA_Forwarder_Data_dbt));
        MEA_OS_memcpy(&MEA_drv_fwd_filter_tble[index].hw.fields_key,&entry_pi->fwd_fields_key_reg,sizeof(MEA_Forwarder_Key_dbt));

         MEA_OS_memcpy(&MEA_drv_fwd_filter_tble[index].hw.mask_data,&entry_pi->fwd_mask_data_reg,sizeof(MEA_Forwarder_Data_dbt));
         MEA_OS_memcpy(&MEA_drv_fwd_filter_tble[index].hw.mask_key,&entry_pi->fwd_mask_key_reg,sizeof(MEA_Forwarder_Key_dbt));

        
    }else{
    // need to build
     
        if(MEA_fwd_tbl_build_data_filter_MASK(unit_i,
                                          &entry_pi->fwd_mask_data,
                                          &mask_data)!=MEA_OK){
        return MEA_ERROR;
        }
        
        
        if(MEA_fwd_tbl_build_keyMASK(&entry_pi->fwd_mask_key,
                                     &mask_key)!=MEA_OK){
                                     return MEA_ERROR;
        }
        if(entry_pi->fwd_mask_key_enable)
            mask_key.raw.key_type=7; // force 

        if(MEA_fwd_tbl_build_data_filter_fields(unit_i,
            &entry_pi->fwd_fields_data,
            &fields_data)    !=MEA_OK){
                return MEA_ERROR;
        }
        if(MEA_fwd_tbl_build_keyMASK(&entry_pi->fwd_fields_key,
                                     &fields_key)!=MEA_OK){
                return MEA_ERROR;
        }

        MEA_OS_memcpy(&MEA_drv_fwd_filter_tble[index].hw.fields_data,&fields_data,sizeof(MEA_Forwarder_Data_dbt));
        MEA_OS_memcpy(&MEA_drv_fwd_filter_tble[index].hw.fields_key ,&fields_key,sizeof(MEA_Forwarder_Key_dbt));

        MEA_OS_memcpy(&MEA_drv_fwd_filter_tble[index].hw.mask_data,&mask_data,sizeof(MEA_Forwarder_Data_dbt));
        MEA_OS_memcpy(&MEA_drv_fwd_filter_tble[index].hw.mask_key,&mask_key,sizeof(MEA_Forwarder_Key_dbt));

        


    }




    return MEA_OK;
}








MEA_Bool MEA_API_SE_Entry_IsExistFilter (MEA_Unit_t               unit_i,
                                       MEA_Uint16                index)
{

    if(index == 0){
        return MEA_FALSE; 

    }

    if(index >= MEA_FORWARDER_FILTER_PROFILE){
        return MEA_FALSE; 
    }

   return MEA_drv_fwd_filter_tble[index].valid;

}





MEA_Status MEA_API_FWD_BrgFlushPortFid (MEA_Port_t EnetPortId , MEA_Uint16 VPN, MEA_SE_Forwarding_key_type_te keyType)
{
    MEA_Uint8                   index; 
    MEA_SE_Entry_dbt              entry;
    MEA_SE_filter_Entry_dbt         filter_entry;
	MEA_Uint32  count=0;
    MEA_Bool                     found;
    MEA_Uint32                            PortsCount = 0,PortsIndex;
	MEA_Uint32  Delete_count = 0;


    /* First, disable learning status for all services of this port*/
    MEA_OS_memset(&filter_entry,0,sizeof(filter_entry));

    /* Now, create and set the filter */
    index = 1; /*This profile create on init*/

    printf(" PortId:%d\n",EnetPortId);
    if(keyType == MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN)
    {
        filter_entry.fwd_fields_key.type =  keyType;

        filter_entry.fwd_fields_key.mac_plus_vpn.VPN = VPN;
        filter_entry.fwd_fields_data.static_flag = 0;
        filter_entry.fwd_mask_key_enable = MEA_TRUE;
        filter_entry.fwd_mask_key.type = 7; //MEA_SE_FORWARDING_KEY_TYPE_DA;
		
		filter_entry.fwd_mask_key.mac_plus_vpn.VPN = 0x3ff;
        filter_entry.fwd_mask_data.static_flag = 1;        
    }
    else
    {
        MEA_OS_printf("Unsupported key type: %d \n",keyType); 
        return MEA_ERROR;    
    }

    if(MEA_API_SE_Entry_SetFilter (MEA_UNIT_0, index, &filter_entry)!=MEA_OK)
    {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Error on MEA_API_SE_Entry_CreateFilter index %d\n",index); 
        MEA_API_SE_Entry_DeleteFilter (MEA_UNIT_0, index);
        return MEA_ERROR;
    }    

    ENET_FWD_Lock();
    entry.filterEnable     = MEA_TRUE;
    entry.filterId         = index;

    if (MEA_API_GetFirst_SE_Entry(MEA_UNIT_0, &entry, &found) != MEA_OK) 
    {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," Error on MEA_API_GetFirst_SE_Entry\n");                                      
        ENET_FWD_Unlock();
        MEA_API_SE_Entry_DeleteFilter (MEA_UNIT_0, index);
        return MEA_ERROR;
    }

    while (found == MEA_TRUE)
    {
		if (count++ == 16000) {  
			/*break if the fwd to ended */
			break;
		}
		
        /* delete mac which is dynamic and learn in portIdx */
		if (entry.key.mac_plus_vpn.VPN == VPN) {
			if (!entry.data.static_flag)
			{
				PortsCount = 0;
				if (EnetPortId != 0xFFFF) {
					if (MEA_IS_SET_OUTPORT(&(entry.data.OutPorts), EnetPortId))
					{
						PortsCount = 0;
						MEA_OUTPORT_FOR_LOOP(&entry.data.OutPorts, PortsIndex)
						{
							PortsCount++;
						}
						//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "DBG PortsCount: %d\n", PortsCount);
					}
				}
				else {
					PortsCount = 1;
				}

				if (PortsCount == 1)   /* only if we have one port/cluster out  we delete */
				{
					if (entry.key.mac_plus_vpn.VPN == VPN)
					{
						if (MEA_API_Delete_SE_Entry(MEA_UNIT_0, &(entry.key)) != MEA_OK)
						{
							//MEA_OS_printf("Error on MEA_API_Delete_SE_Entry\n");
							ENET_FWD_Unlock();
							//MEA_API_SE_Entry_DeleteFilter (MEA_UNIT_0, index);
							return MEA_ERROR;
						}
						//MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " port:%d is deleted!\n", EnetPortId);
						Delete_count++;
					}
					else {
						MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, "  GBG  get wrong VPNId %d \n", entry.key.mac_plus_vpn.VPN);
					}
				}
			}
		}

		found = MEA_FALSE;
		MEA_OS_memset(&filter_entry, 0, sizeof(filter_entry));
		entry.filterEnable = MEA_TRUE;
		entry.filterId = index;
        if (MEA_API_GetNext_SE_Entry(MEA_UNIT_0, &entry, &found) != MEA_OK) 
        {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_API_GetNext_SE_Entry\n"); 
            ENET_FWD_Unlock();
            //MEA_API_SE_Entry_DeleteFilter (MEA_UNIT_0, index);
            return MEA_ERROR;
        }

		

    }

    ENET_FWD_Unlock();
#if 0
    /*the profile 1 not need to delete*/ 
    if(MEA_API_SE_Entry_DeleteFilter (MEA_UNIT_0, index)!=MEA_OK)
    {
        printf("Error on MEA_API_SE_Entry_DeleteFilter index %d\n",index); 
        return MEA_ERROR;
    }
#endif
    /* After deleting of all entries, enable learning status for all services of this port*/

	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "count %d delete %d", count, Delete_count);

    return MEA_OK;
}



MEA_Status MEA_API_PBB_GetLearnInfo(MEA_Unit_t  unit, MEA_PBB_Lean_dbt  *entry)
{
    MEA_Uint32        value[7];
    //MEA_Uint32        val;
    MEA_ind_read_t    ind_read;
    mea_memory_mode_e originalMode;

    MEA_API_LOG

        if (MEA_reinit_start)
            return MEA_OK;

    if (entry == NULL){
        return MEA_OK;
    }
    MEA_OS_memset(&value, 0, sizeof(value));

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG;
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_FIFO_PBB_EVENT;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = value;

    originalMode = globalMemoryMode;

    /* Read the counters from device */
    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(value),
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);
        globalMemoryMode = originalMode;
        return MEA_ERROR;
    }
    
    globalMemoryMode = originalMode;


    if (((value[6]>>(16+7)) & 0x1) == MEA_TRUE){
        //entry->valid = MEA_TRUE;
        (entry->B_Sa_mac.b[2]) = (unsigned char)(value[0] >> 24) & 0xff;
        (entry->B_Sa_mac.b[3]) = (unsigned char)(value[0] >> 16) & 0xff;
        (entry->B_Sa_mac.b[4]) = (unsigned char)(value[0] >> 8) & 0xff;
        (entry->B_Sa_mac.b[5]) = (unsigned char)(value[0] >> 0) & 0xff;

        (entry->B_Sa_mac.b[0]) = (unsigned char)(value[1] >> 8) & 0xff;
        (entry->B_Sa_mac.b[1]) = (unsigned char)(value[1] >> 0) & 0xff;
        /************************************************************************/
        (entry->inner_Sa_mac.b[2]) = (unsigned char)(value[2] >> 24) & 0xff;
        (entry->inner_Sa_mac.b[3]) = (unsigned char)(value[2] >> 16) & 0xff;
        (entry->inner_Sa_mac.b[4]) = (unsigned char)(value[2] >> 8) & 0xff;
        (entry->inner_Sa_mac.b[5]) = (unsigned char)(value[2] >> 0) & 0xff;

        (entry->inner_Sa_mac.b[0]) = (unsigned char)(value[3] >> 8) & 0xff;
        (entry->inner_Sa_mac.b[1]) = (unsigned char)(value[3] >> 0) & 0xff;





        /************************************************************************/
        entry->vpn = value[3] >> 16;
        entry->Itag = value[4];

        
        (entry->inner_Da_mac.b[2]) = (unsigned char)(value[5] >> 24) & 0xff;
        (entry->inner_Da_mac.b[3]) = (unsigned char)(value[5] >> 16) & 0xff;
        (entry->inner_Da_mac.b[4]) = (unsigned char)(value[5] >> 8) & 0xff;
        (entry->inner_Da_mac.b[5]) = (unsigned char)(value[5] >> 0) & 0xff;

        (entry->inner_Da_mac.b[0]) = (unsigned char)(value[6] >> 8) & 0xff;
        (entry->inner_Da_mac.b[1]) = (unsigned char)(value[6] >> 0) & 0xff;



        entry->src_port = ((value[6]>>16) & 0x7f) ;
        entry->valid = MEA_TRUE;

    }

    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/



MEA_Bool   mea_drv_fwd_vpnIpV6_free_place(MEA_Uint32 vpn)
{
    MEA_Uint32 idx;
    MEA_Uint32 i;
    MEA_Bool   found = MEA_FALSE;

    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn == vpn) {
                found = MEA_TRUE;
                break;
            }
        }
    }

    if (found == MEA_TRUE) 
    {
        for (i = 0; i < MEA_IPV6_MAX_FWD; i++)
        {
            if (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid == MEA_FALSE)
            {
                return MEA_TRUE;
            }
        }
    }

    return MEA_FALSE;
}
MEA_Uint32 mea_fwd_SignatureIpv6(MEA_Uint32 *Ipv6)
{
    MEA_Uint32 Signature = 0;
    
    if (Ipv6 == NULL) {
        return Signature;
    }

    Signature = (Ipv6[0] ^ Ipv6[1] ^ Ipv6[2] ^ Ipv6[3]);
    
    return Signature;
}
MEA_Bool   mea_drv_fwd_vpnIpV6_check_Ipv6key (MEA_Uint32 vpn,MEA_Uint32 *Ipv6)
{
    MEA_Uint32 idx;
    MEA_Uint32 i;
    MEA_Uint32 Signature = 0;

    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn == vpn) {
                break;
            }
        }
    }

    Signature = mea_fwd_SignatureIpv6(Ipv6);
    

    for (i = 0; i < MEA_IPV6_MAX_FWD; i++)
    {
        if (MEA_ForwarderIPV6_Table[idx].ipv6_data->valid == MEA_TRUE){
            //check the Ipv6
            if ((
                (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[0] == Ipv6[0]) &&
                (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[1] == Ipv6[2]) &&
                (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[2] == Ipv6[3]) &&
                (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[3] == Ipv6[3])
                ) || (
                (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].IPv6_Signature == Signature)
                    )
                ) {
                
                return MEA_TRUE;
            }

        }
    }//for
    

    return MEA_FALSE;
}


MEA_Status mea_drv_fwd_vpnIpV6_find_Ipv6key(MEA_Uint32 vpn, 
                                            MEA_Uint32 Signature, 
                                            MEA_Uint32 *Ipv6)
{
    MEA_Uint32 idx;
    MEA_Uint32 i;

    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn == vpn) {
                break;
            }
        }
    }

    for (i = 0; i < MEA_IPV6_MAX_FWD; i++)
    {
        if (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid == MEA_FALSE)
            continue;
        
        if (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].IPv6_Signature == Signature) {
            MEA_OS_memcpy(&Ipv6[0], 
                         &MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[0],
                         16);   //4 *4
            return MEA_OK;
        }
        
    }

    return MEA_ERROR;

}


MEA_Status mea_drv_fwd_vpnIpV6_Add_Ipv6_DB(MEA_SE_Entry_dbt  *entry_pi)
{

    MEA_Uint32 idx;
    MEA_Uint32 i;
    MEA_Bool found = MEA_FALSE;
    //MEA_Uint32 Ipv6[4];
    MEA_Uint32  vpn;


    switch (entry_pi->key.type)
    {
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
        vpn = entry_pi->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN;
       
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        vpn = entry_pi->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN;
        
        break;
    default:
        break;
    }

    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn == vpn) {
                found = MEA_TRUE;
                break;
            }
        }
    }
    if (found) {
        for (i = 0; i < MEA_IPV6_MAX_FWD; i++)
        {
            if (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid == MEA_FALSE)
            {

                
                    switch (entry_pi->key.type)
                    {
                    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
                        MEA_OS_memcpy(&MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[0],
                            &entry_pi->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0], 16);
                    break;
                    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
                        MEA_OS_memcpy(&MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[0],
                            &entry_pi->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0], 16);
                        break;
                    default:
                        break;
                    }

                
                
                MEA_ForwarderIPV6_Table[idx].ipv6_data[i].IPv6_Signature = mea_fwd_SignatureIpv6(&MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[0]);
                
                
                MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid = MEA_TRUE;
                return MEA_OK;
            }
        }
    }

    return MEA_ERROR;

}
MEA_Status mea_drv_fwd_vpnIpV6_Update_Ipv6_DB(MEA_SE_Entry_dbt  *entry_pi)
{

    MEA_Uint32 idx;
    MEA_Uint32 i;

    MEA_Uint32  vpn;


    switch (entry_pi->key.type)
    {
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
        vpn = entry_pi->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN;

        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        vpn = entry_pi->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN;

        break;
    default:
        break;
    }

    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn == vpn) {
                break;
            }
        }
    }

    for (i = 0; i < MEA_IPV6_MAX_FWD; i++)
    {
        if (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid == MEA_TRUE)
        {
               
             return MEA_OK;
        }
    }

    return MEA_ERROR;

}

MEA_Status mea_drv_fwd_vpnIpV6_Delete_Ipv6_DB(MEA_SE_Entry_dbt  *entry_pi)
{

    MEA_Uint32 idx;
    MEA_Uint32 i;
    MEA_Bool found = MEA_FALSE;
    MEA_Uint32 IPV6[4];
    MEA_Uint32  vpn;


    
    switch (entry_pi->key.type)
    {
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
        MEA_OS_memcpy(&IPV6[0],
            &entry_pi->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0], 16);
        vpn = entry_pi->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        MEA_OS_memcpy(&IPV6[0],
            &entry_pi->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0], 16);
        vpn = entry_pi->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN;
        break;
    default:
        break;
    }




    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn == vpn) {
                found = MEA_TRUE;
                break;
            }
        }
    }
    if (found) {
        for (i = 0; i < MEA_IPV6_MAX_FWD; i++)
        {
            if (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid == MEA_TRUE)
            {
                
                

                
                
                if (MEA_OS_memcmp(&MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[0],
                    &IPV6[0], 16) == 0)
                {
                    // is is same IPV6 remove from DB
                    MEA_OS_memset(&MEA_ForwarderIPV6_Table[idx].ipv6_data[i].DestIPv6[0], 0, 16);
                    MEA_ForwarderIPV6_Table[idx].ipv6_data[i].IPv6_Signature = 0;
                    MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid = MEA_FALSE;

                }
                return MEA_OK;
            }
        }
    }

    return MEA_OK;

}






MEA_Bool MEA_API_Exist_FWD_VPN_Ipv6(MEA_Unit_t    unit, MEA_FWD_VPN_dbt *entry)
{
    MEA_Uint32 idx;
    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (MEA_OS_memcmp(&MEA_ForwarderIPV6_Table[idx].Vpn_data,entry,sizeof(MEA_FWD_VPN_dbt)) == 0 ) {
                return MEA_TRUE;
            }
        }
    }
    return MEA_FALSE;
}

MEA_Status MEA_API_Create_FWD_VPN_Ipv6(MEA_Unit_t    unit, MEA_FWD_VPN_dbt *entry)
{
    MEA_Uint32 idx;
    
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "entry == NULL  \n");
        return MEA_ERROR;
    }

    if (entry->fwd_vpn >= 1024) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " fwd_vpn up to 1024 \n");
        return MEA_ERROR;
    }
    
   
    switch (entry->fwd_KeyType)
    {
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " the Key %d is not support  \n", entry->fwd_KeyType);
        return MEA_ERROR;
        break;
    }

    

    
    /************************************************************************/
    /* check if the VPN is exist                                            */
    /************************************************************************/
    if (MEA_API_Exist_FWD_VPN_Ipv6(unit, entry) == MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The FWD_VPN %d For IPv6 is Exist on Database  \n",__FUNCTION__, entry->fwd_vpn);
        return MEA_ERROR;
    }
    
    /************************************************************************/
    /*               find free place                                        */
    /************************************************************************/
    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_FALSE) {
            MEA_OS_memcpy(&MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn, entry, sizeof(MEA_FWD_VPN_dbt));
            MEA_ForwarderIPV6_Table[idx].valid = MEA_TRUE;
                return MEA_OK;
        }
    }
    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The FWD_VPN Database is full \n", __FUNCTION__, entry->fwd_vpn);
    return MEA_ERROR;
}

MEA_Status MEA_API_Delete_FWD_VPN_Ipv6(MEA_Unit_t unit, MEA_FWD_VPN_dbt *entry)
{
    MEA_Uint32 idx;
    MEA_Uint32 i;
    /************************************************************************/
    /* check if the VPN is exist                                            */
    /************************************************************************/
    if (MEA_API_Exist_FWD_VPN_Ipv6(unit, entry) == MEA_FALSE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The FWD_VPN %d For IPv6 doesn't Exist on Database  \n", __FUNCTION__, entry->fwd_vpn);
        return MEA_ERROR;
    }
    
    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn == entry->fwd_vpn) {
                for (i = 0; i < MEA_IPV6_MAX_FWD; i++) 
                {
                    if (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid ==MEA_TRUE) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - VPN %d delete all Ipv6 from fwd  \n", __FUNCTION__, entry->fwd_vpn);
                        return MEA_ERROR;
                    }
                }
                //Database is not have any Ipv6 so we can set it to disable 
                MEA_OS_memset(&MEA_ForwarderIPV6_Table[idx].Vpn_data, 0, sizeof(MEA_FWD_VPN_dbt));
                MEA_ForwarderIPV6_Table[idx].valid = MEA_FALSE;
                
                return MEA_OK;
            }
        }
    }

    
    
    return MEA_OK;
}

MEA_Status MEA_API_Get_FWD_VPN_Ipv6(MEA_Unit_t unit, MEA_FWD_VPN_dbt *entry) 
{

    MEA_Uint32 idx;


    
   
    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_FALSE)
            continue;

        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (MEA_OS_memcmp(entry, &MEA_ForwarderIPV6_Table[idx].Vpn_data, sizeof(MEA_FWD_VPN_dbt)) == 0) {
                return MEA_OK;
            }
            
        }

    }


    return MEA_ERROR; 
}

MEA_Status MEA_API_GetFirst_FWD_VPN_Ipv6(MEA_Unit_t unit, MEA_FWD_VPN_dbt *entry, MEA_Bool*  found)
{
    MEA_Uint32 idx;
    
    
    if (found == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found == NULL  \n", __FUNCTION__);
        return MEA_ERROR;
    }
    mea_firstVpnId = 0;
    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_FALSE) 
            continue;

        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
            if (entry != NULL) {
                MEA_OS_memcpy(entry, &MEA_ForwarderIPV6_Table[idx].Vpn_data, sizeof(*entry));
                //entry->fwd_vpn = MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn;
            }
            *found = MEA_TRUE;
            mea_firstVpnId = idx;
            return MEA_OK;
            }
        
    }

    *found = MEA_FALSE;
    return MEA_ERROR;
}
MEA_Status MEA_API_GetNext_FWD_VPN_Ipv6(MEA_Unit_t unit, MEA_FWD_VPN_dbt *entry, MEA_Bool*  found)
{
    MEA_Uint32 idx;
    //MEA_Bool myfount =MEA_FALSE;
    

    if (found == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found == NULL  \n", __FUNCTION__);

    }

    
    
        for (idx = mea_firstVpnId+1; idx < MEA_IPV6_MAX_VPN; idx++)
        {
            if (MEA_ForwarderIPV6_Table[idx].valid == MEA_FALSE)
                continue;

            if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE) {
                if (entry != NULL) {
                    MEA_OS_memcpy(entry, &MEA_ForwarderIPV6_Table[idx].Vpn_data, sizeof(*entry));
                    //entry->fwd_vpn = MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn;
                }
                
                *found = MEA_TRUE;
                mea_firstVpnId = idx;
                return MEA_OK;
            }

        }

    
    

     /*Not fount */
    *found = MEA_FALSE;
    return MEA_OK;
}



MEA_Status MEA_API_GetFirst_FWD_VPN_Ipv6DB(MEA_Unit_t unit, MEA_Uint32 vpn, mea_fwd_vpn_IPv6_info_dbt *IPv6_info,MEA_Bool*  found)
{
    MEA_Uint32 idx,i;
    MEA_Bool myfound = MEA_TRUE;

    if (found == NULL || IPv6_info==NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found == NULL Or Ipv6 == NULL  \n", __FUNCTION__);
        return MEA_ERROR;
    }
    mea_firstVpnIdDB = 0;
    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_FALSE)
            continue;
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE && MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn == vpn) {
            myfound = MEA_TRUE;
            break;
        }
    }

    if(myfound){
        for (i = 0; i < MEA_IPV6_MAX_FWD; i++) {
            if (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid == MEA_TRUE) {


                if (IPv6_info != NULL) {
                    MEA_OS_memcpy(IPv6_info, &MEA_ForwarderIPV6_Table[idx].ipv6_data[i], sizeof(*IPv6_info));
                    
                }
                *found = MEA_TRUE;
                mea_firstVpnIdDB = i;
                return MEA_OK;
            }
        }


    }

    *found = MEA_FALSE;
    return MEA_ERROR;
}
MEA_Status MEA_API_GetNext_FWD_VPN_Ipv6DB(MEA_Unit_t unit, MEA_Uint32 vpn, mea_fwd_vpn_IPv6_info_dbt *IPv6_info, MEA_Bool*  found)
{
    MEA_Uint32 idx, i;
    MEA_Bool myfound = MEA_TRUE;

    if (found == NULL || IPv6_info == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found == NULL Or Ipv6 == NULL  \n", __FUNCTION__);
        return MEA_ERROR;
    }
    
    for (idx = 0; idx < MEA_IPV6_MAX_VPN; idx++)
    {
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_FALSE)
            continue;
        if (MEA_ForwarderIPV6_Table[idx].valid == MEA_TRUE && MEA_ForwarderIPV6_Table[idx].Vpn_data.fwd_vpn == vpn) {
            myfound = MEA_TRUE;
            break;
        }
    }

    if (myfound) {
        for (i = mea_firstVpnIdDB+1; i < MEA_IPV6_MAX_FWD; i++) {
            if (MEA_ForwarderIPV6_Table[idx].ipv6_data[i].valid == MEA_TRUE) {
                if (IPv6_info != NULL) {
                    MEA_OS_memcpy(IPv6_info, &MEA_ForwarderIPV6_Table[idx].ipv6_data[i], sizeof(*IPv6_info));

                }
                *found = MEA_TRUE;
                mea_firstVpnIdDB = i;
                return MEA_OK;
            }
        }


    }

    *found = MEA_FALSE;
    return MEA_ERROR;
}
