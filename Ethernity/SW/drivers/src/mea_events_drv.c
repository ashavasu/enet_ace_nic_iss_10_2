/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_if_regs.h"
#include "mea_bm_regs.h"
#include "mea_port_drv.h"
#include "enet_queue_drv.h"
#include "mea_if_parser_drv.h"
#include "mea_events_drv.h"
#include "mea_if_Acl5_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
#if    defined(MEA_ETHERNITY) && defined(MEA_ETHERNITY_DUBUG)
static MEA_debugLogerAxiEntry_dbt MEA_loggerAXI_Global_read_write[(ENET_BM_TBL_LOG_DESC_AXI_LENGTH * 2)];
#endif



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
extern MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_Link_protection_rx_Status;
extern MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_Link_protection_rx_Mask;

MEA_Uint32 mea_global_WRITE_ENGINE_STATE=0;
/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/




/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_API_Get_EgressMACPortReady(MEA_egressMacReady_t *entry)
{

    MEA_ind_read_t         ind_read;

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3

    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;


    ind_read.read_data = &(entry->if2bm_egr.v[0]);
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_POS_PARITY_EVENT;
    
#if 1    
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if2bm_egr.v),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }
#else

    if (MEA_API_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if2bm_egr.v),
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }


#endif





    ind_read.read_data = &(entry->if2bm_egr_mask.v[0]);
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF2BM_EGR_PRT_RDY_MSK;
    
#if 0   
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if2bm_egr_mask.v),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }
#else
    if (MEA_API_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if2bm_egr_mask.v),
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

#endif


    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Events_Entry>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Events_Entry(MEA_Unit_t             unit,
    MEA_Events_Entry_dbt*  entry)
{
    MEA_Uint32             if_events0_rd[10];
    MEA_Uint32             if_events_rd[1];
    MEA_Uint32             if_service_event[10];
    MEA_Uint32             if_L3_L4[7];
    MEA_Uint32             i;
    MEA_Uint32             numOfelements;
	MEA_Uint32             if_lpm_rd[5];
    MEA_Uint32 val[4];
    MEA_Uint32 count_shift = 0;
    MEA_Uint32 if_XAUI_xmac[3];
    MEA_Uint32 save_count_shift;


    MEA_ind_read_t         ind_read;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (entry == NULL)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Null entry input\n",
                __FUNCTION__);
            return MEA_ERROR;
        }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    MEA_OS_memset(entry, 0, sizeof(*entry));

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_IF_EVENTS;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(entry->if_events.regs[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_events.regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }
    ind_read.read_data = &(if_events0_rd[0]);
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_IF_LAST_SEARCH_KEY;
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(if_events0_rd),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }
    if (!MEA_CLS_LARGE_SUPPORT) {
        entry->if_last_classifier_search_key[0] = if_events0_rd[0] & 0x3fffffff;

    }
    else {
        entry->if_last_classifier_search_key[0] = if_events0_rd[0];
        entry->if_last_classifier_search_key[1] = if_events0_rd[1];
        entry->if_last_classifier_search_key[2] = if_events0_rd[2] & 0x000000ff;



        entry->if_EVC_info.val.EVC_search_key = if_events0_rd[6] >> 8;
        entry->if_EVC_info.val.EVC_search_key |= (if_events0_rd[7] & 0xff);

        entry->if_EVC_info.val.EVC_unmatch_key = (if_events0_rd[7] >> 8);
        entry->if_EVC_info.val.EVC_unmatch_key |= (if_events0_rd[8] & 0xff) << 8;
        entry->if_EVC_info.val.EVC_info = (if_events0_rd[8] >> 8) & 0x3; /*only bit */
    }



    ind_read.read_data = &(entry->if_events_FWD.reg[0]);
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE;
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_events_FWD.reg),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }




    MEA_OS_memset(&if_service_event[0], 0, sizeof(if_service_event));

    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_EVENT_EXTERNAL_SERVICE;
    ind_read.read_data = &(if_service_event[0]);
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(if_service_event),
        MEA_MODULE_IF,
        globalMemoryMode,
        MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }





    count_shift = 0;

    entry->if_SRV_EXT_info.last_search_key.search_key_pri = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_pri_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_pri_WIDTH;
    entry->if_SRV_EXT_info.last_search_key.search_key_priType = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_priType_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_priType_WIDTH;

    entry->if_SRV_EXT_info.last_search_key.search_key_src_port = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_src_port_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_src_port_WIDTH;

    entry->if_SRV_EXT_info.last_search_key.search_key_net_tag = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag_WIDTH;

    entry->if_SRV_EXT_info.last_search_key.search_key_subType = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_subType_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_subType_WIDTH;

    entry->if_SRV_EXT_info.last_search_key.search_key_l2Type = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_l2type_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_l2type_WIDTH;

    entry->if_SRV_EXT_info.last_search_key.search_key_net_tag1 = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag1_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag1_WIDTH;

    entry->if_SRV_EXT_info.last_search_key.search_key_net2 = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag2_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag2_WIDTH;

    entry->if_SRV_EXT_info.last_search_key.search_key_Sid = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_sid, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_sid;

   


		entry->if_SRV_EXT_info.last_unmatch_key.search_key_pri = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_pri_WIDTH, &if_service_event[0]);
		count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_pri_WIDTH;

        entry->if_SRV_EXT_info.last_unmatch_key.search_key_priType = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_priType_WIDTH, &if_service_event[0]);
        count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_priType_WIDTH;



		entry->if_SRV_EXT_info.last_unmatch_key.search_key_src_port = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_src_port_WIDTH, &if_service_event[0]);
		count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_src_port_WIDTH;

		entry->if_SRV_EXT_info.last_unmatch_key.search_key_net_tag = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag_WIDTH, &if_service_event[0]);
		count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag_WIDTH;

        entry->if_SRV_EXT_info.last_unmatch_key.search_key_subType = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_subType_WIDTH, &if_service_event[0]);
        count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_subType_WIDTH;



		entry->if_SRV_EXT_info.last_unmatch_key.search_key_l2Type = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_l2type_WIDTH, &if_service_event[0]);
		count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_l2type_WIDTH;

		entry->if_SRV_EXT_info.last_unmatch_key.search_key_net_tag1 = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag1_WIDTH, &if_service_event[0]);
		count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag1_WIDTH;
        
        entry->if_SRV_EXT_info.last_unmatch_key.search_key_net2 = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag2_WIDTH, &if_service_event[0]);
        count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag2_WIDTH;



		entry->if_SRV_EXT_info.last_unmatch_key.search_key_Sid = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_EXTNAL_service_last_sid, &if_service_event[0]);
		count_shift += MEA_IF_SRV_EVENT_EXTNAL_service_last_sid;
		





    ind_read.read_data      = &(entry->if_last_preparser_search_key.reg[0]);    
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PREPRASER_LAST_SEARCH_KEY;
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
                             MEA_NUM_OF_ELEMENTS(entry->if_last_preparser_search_key.reg),
                             MEA_MODULE_IF,globalMemoryMode,MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }
	


   

    
	




	ind_read.read_data      = &(entry->if_last_forwarder_learn_key.v[0]);
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_LAST_LEARN_KEY;
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
	                         MEA_NUM_OF_ELEMENTS(entry->if_last_forwarder_learn_key.v),
                             MEA_MODULE_IF,
							 globalMemoryMode,MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }
/**/
	ind_read.read_data      = &(entry->if_last_forwarder_Search_key.v[0]);
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_LAST_FWD_KEY;
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
	                         MEA_NUM_OF_ELEMENTS(entry->if_last_forwarder_Search_key.v),
                             MEA_MODULE_IF,
							 globalMemoryMode,MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }

    ind_read.read_data = &(entry->if_last_if_to_bm[0].v[0]);
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_0;
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_last_if_to_bm[0].v),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }
    if (MEA_GLOBAL_NUM_OF_SLICE >= 2){
        ind_read.read_data = &(entry->if_last_if_to_bm[1].v[0]);
        ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_1;
        if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
            MEA_NUM_OF_ELEMENTS(entry->if_last_if_to_bm[1].v),
            MEA_MODULE_IF,
            globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
            return MEA_ERROR;
        }
    }
    if (MEA_GLOBAL_NUM_OF_SLICE >= 3){
        ind_read.read_data = &(entry->if_last_if_to_bm[2].v[0]);
        ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_2;
        if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
            MEA_NUM_OF_ELEMENTS(entry->if_last_if_to_bm[2].v),
            MEA_MODULE_IF,
            globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
            return MEA_ERROR;
        }
    }
    if (MEA_GLOBAL_NUM_OF_SLICE >= 4){
        ind_read.read_data = &(entry->if_last_if_to_bm[3].v[0]);
        ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_3;
        if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
            MEA_NUM_OF_ELEMENTS(entry->if_last_if_to_bm[3].v),
            MEA_MODULE_IF,
            globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
            return MEA_ERROR;
        }
    }
    if (MEA_GLOBAL_NUM_OF_SLICE >= 5) {
        ind_read.read_data = &(entry->if_last_if_to_bm[4].v[0]);
        ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_4;
        if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
            MEA_NUM_OF_ELEMENTS(entry->if_last_if_to_bm[4].v),
            MEA_MODULE_IF,
            globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
            return MEA_ERROR;
        }
    }

    ind_read.read_data      = &(entry->if2bm_egr.v[0]);
    ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_POS_PARITY_EVENT;
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if2bm_egr.v),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }

    ind_read.read_data = &(entry->if2bm_egr_mask.v[0]);
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF2BM_EGR_PRT_RDY_MSK;
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if2bm_egr_mask.v),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

    //if_L3_L4
	ind_read.read_data      = &(if_L3_L4[0]);
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_L3L4; 
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
	                         (MEA_NUM_OF_ELEMENTS(if_L3_L4)),
                             MEA_MODULE_IF,
							 globalMemoryMode,MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }


    count_shift=0;


  











    entry->if_L3_L4.ip_header_len       = MEA_OS_read_value(count_shift,4,if_L3_L4);    count_shift+=4;
    entry->if_L3_L4.ipv4_ipv6_protocol  = MEA_OS_read_value(count_shift,8,if_L3_L4);    count_shift+=8;
    entry->if_L3_L4.ip_protocol_tcp     = MEA_OS_read_value(count_shift,1,if_L3_L4);    count_shift+=1;
    entry->if_L3_L4.ip_protocol_udp     = MEA_OS_read_value(count_shift,1,if_L3_L4);    count_shift+=1;
    entry->if_L3_L4.src_ipv4            = MEA_OS_read_value(count_shift,32,if_L3_L4);    count_shift+=32;
    entry->if_L3_L4.dst_ipv4            = MEA_OS_read_value(count_shift,32,if_L3_L4);    count_shift+=32;
    entry->if_L3_L4.IPv4_l4_sport       = MEA_OS_read_value(count_shift,16,if_L3_L4);    count_shift+=16;
    entry->if_L3_L4.IPv4_l4_dport       = MEA_OS_read_value(count_shift,16,if_L3_L4);    count_shift+=16;
    entry->if_L3_L4.src_ipv6_xor        = MEA_OS_read_value(count_shift,32,if_L3_L4);    count_shift+=32;
    entry->if_L3_L4.dst_ipv6_xor        = MEA_OS_read_value(count_shift,32,if_L3_L4);    count_shift+=32;
    entry->if_L3_L4.IPv6_l4_sport       = MEA_OS_read_value(count_shift,16,if_L3_L4);    count_shift+=16;
    entry->if_L3_L4.IPv6_l4_dport       = MEA_OS_read_value(count_shift,16,if_L3_L4);    count_shift+=16;
    entry->if_L3_L4.pkt_is_IPv4         = MEA_OS_read_value(count_shift,1,if_L3_L4);    count_shift+=1;
    entry->if_L3_L4.pkt_is_IPv6         = MEA_OS_read_value(count_shift,1,if_L3_L4);    count_shift+=1;
    entry->if_L3_L4.pkt_is_my_mac_ppp   = MEA_OS_read_value(count_shift,1,if_L3_L4);    count_shift+=1;
    entry->if_L3_L4.pkt_is_my_mac       = MEA_OS_read_value(count_shift,1,if_L3_L4);    count_shift+=1;
    entry->if_L3_L4.pkt_is_cfm          = MEA_OS_read_value(count_shift, 1, if_L3_L4);    count_shift += 1;
    entry->if_L3_L4.pkt_is_l2cp         = MEA_OS_read_value(count_shift, 1, if_L3_L4);    count_shift += 1;
    entry->if_L3_L4.pkt_is_lacp         = MEA_OS_read_value(count_shift, 1, if_L3_L4);    count_shift += 1;
    entry->if_L3_L4.pkt_is_mpls         = MEA_OS_read_value(count_shift, 1, if_L3_L4);    count_shift += 1;
    entry->if_L3_L4.pkt_is_mpls_cfm     = MEA_OS_read_value(count_shift, 1, if_L3_L4);    count_shift += 1;
    entry->if_L3_L4.pkt_is_IP_mc        = MEA_OS_read_value(count_shift, 1, if_L3_L4);    count_shift += 1;
       
        
        
        



	ind_read.read_data      = &(entry->if_last_ppp.v[0]);
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_PPP;
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
	                         MEA_NUM_OF_ELEMENTS(entry->if_last_ppp.v),
                             MEA_MODULE_IF,
							 globalMemoryMode,MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }

    

    ind_read.read_data      = &(if_events_rd[0]);    
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_PAUSE;
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
		                          &ind_read,
                                  1,
                                  MEA_MODULE_IF,
								  globalMemoryMode,
								  MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }
    entry->if_last_pause.reg = if_events_rd[0];

    
    
      ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_CFM; 
       ind_read.read_data   = &(entry->if_cfm_oam_melevel.v[0]);
       if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
		                              &ind_read,
                                      MEA_NUM_OF_ELEMENTS(entry->if_cfm_oam_melevel.v),
                                      MEA_MODULE_IF,
								      globalMemoryMode,
								      MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                              __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		    return MEA_ERROR;
        }
    

          ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_PROTOCOL_EVENT;
           ind_read.read_data = &(entry->if_psrser_protocol.reg[0]);
       if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
           &ind_read,
           MEA_NUM_OF_ELEMENTS(entry->if_psrser_protocol.reg),
           MEA_MODULE_IF,
           globalMemoryMode,
           MEA_MEMORY_READ_IGNORE) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
               __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
           return MEA_ERROR;
       }


    ind_read.read_data      = &(entry->if_events_state_machine.reg[0]);    
    ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_IF2BM_STATE_MACHINE;
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_events_state_machine.reg),
        MEA_MODULE_IF,globalMemoryMode,MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }


 

 

    if (!MEA_HPM_NEW_SUPPORT) {
    
        MEA_OS_memset(&if_events0_rd[0], 0, sizeof(if_events0_rd));
        ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_HPM_UNMATCH_EVENTS;
        ind_read.read_data = &(if_events0_rd[0]);
        if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(if_events0_rd),
            MEA_MODULE_IF,
            globalMemoryMode,
            MEA_MEMORY_READ_IGNORE) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
            return MEA_ERROR;
        }


        
            entry->unmatch_hpm.type_IPv4_6 = MEA_OS_read_value(148, 2, &if_events0_rd[0]);

            

            if (entry->unmatch_hpm.type_IPv4_6 != 0) 
            {
                if ((entry->unmatch_hpm.type_IPv4_6 == MEA_TFT_TYPE_NO_IP) || (entry->unmatch_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV4)) {
                    entry->unmatch_hpm.L4_Source   = MEA_OS_read_value(0, 16, &if_events0_rd[0]);
                    entry->unmatch_hpm.L4_Dest     = MEA_OS_read_value(16, 16, &if_events0_rd[0]);
                    entry->unmatch_hpm.Source_IPv4 = MEA_OS_read_value(32, 32, &if_events0_rd[0]);
                    entry->unmatch_hpm.Dest_IPv4   = MEA_OS_read_value(64, 32, &if_events0_rd[0]);
                    entry->unmatch_hpm.L2_pri      = MEA_OS_read_value(128, 3, &if_events0_rd[0]);
                    entry->unmatch_hpm.IP_protocol = MEA_OS_read_value(131, 8, &if_events0_rd[0]);
                    entry->unmatch_hpm.DSCP        = MEA_OS_read_value(139, 6, &if_events0_rd[0]);
                    entry->unmatch_hpm.Internal_ethertype = MEA_OS_read_value(145, 3, &if_events0_rd[0]);
                    entry->unmatch_hpm.prof_rule = MEA_OS_read_value(150, 6, &if_events0_rd[0]);
                    entry->unmatch_hpm.UE_id = MEA_OS_read_value(156, 14, &if_events0_rd[0]);
                }
                if (entry->unmatch_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV6) {
                    entry->unmatch_hpm.IPV6[0] = MEA_OS_read_value(0, 32, &if_events0_rd[0]);
                    entry->unmatch_hpm.IPV6[1] = MEA_OS_read_value(32, 32, &if_events0_rd[0]);
                    entry->unmatch_hpm.IPV6[2] = MEA_OS_read_value(64, 32, &if_events0_rd[0]);
                    entry->unmatch_hpm.IPV6[3] = MEA_OS_read_value(96, 32, &if_events0_rd[0]);
                
                    entry->unmatch_hpm.L2_pri = MEA_OS_read_value(128, 3, &if_events0_rd[0]);
                    entry->unmatch_hpm.IP_protocol = MEA_OS_read_value(131, 8, &if_events0_rd[0]);
                    entry->unmatch_hpm.DSCP = MEA_OS_read_value(139, 6, &if_events0_rd[0]);
                    entry->unmatch_hpm.prof_rule = MEA_OS_read_value(150, 6, &if_events0_rd[0]);
                    entry->unmatch_hpm.UE_id = MEA_OS_read_value(156, 14, &if_events0_rd[0]);

                }
            
            
            }

            MEA_OS_memset(&if_events0_rd[0], 0, sizeof(if_events0_rd));
            ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_HPM_MATCH_EVENTS;
            ind_read.read_data = &(if_events0_rd[0]);
            if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
                &ind_read,
                MEA_NUM_OF_ELEMENTS(if_events0_rd),
                MEA_MODULE_IF,
                globalMemoryMode,
                MEA_MEMORY_READ_IGNORE) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }



            /************************************************************************/
            /*                                                                      */
            /************************************************************************/
            entry->match_hpm.type_IPv4_6 = MEA_OS_read_value(148, 2, &if_events0_rd[0]);

            if (entry->match_hpm.type_IPv4_6 != 0)
            {
                if ((entry->match_hpm.type_IPv4_6 == MEA_TFT_TYPE_NO_IP) ||
                    (entry->match_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV4)    ) {

                    entry->match_hpm.L4_Source = MEA_OS_read_value(0, 16, &if_events0_rd[0]);
                    entry->match_hpm.L4_Dest = MEA_OS_read_value(16, 16, &if_events0_rd[0]);
                    entry->match_hpm.Source_IPv4 = MEA_OS_read_value(32, 32, &if_events0_rd[0]);
                    entry->match_hpm.Dest_IPv4 = MEA_OS_read_value(64, 32, &if_events0_rd[0]);
                    entry->match_hpm.L2_pri = MEA_OS_read_value(128, 3, &if_events0_rd[0]);
                    entry->match_hpm.IP_protocol = MEA_OS_read_value(131, 8, &if_events0_rd[0]);
                    entry->match_hpm.DSCP = MEA_OS_read_value(139, 6, &if_events0_rd[0]);
                    entry->match_hpm.Internal_ethertype = MEA_OS_read_value(145, 3, &if_events0_rd[0]);
                    entry->match_hpm.prof_rule = MEA_OS_read_value(150, 6, &if_events0_rd[0]);
                    entry->match_hpm.UE_id = MEA_OS_read_value(156, 14, &if_events0_rd[0]);
                   
                    entry->match_hpm.Qos_win = MEA_OS_read_value(171, 3, &if_events0_rd[0]);
                    entry->match_hpm.pri_rule_win = MEA_OS_read_value(174, 5, &if_events0_rd[0]);


                }
                if (entry->match_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV6) {
                    entry->match_hpm.IPV6[0] = MEA_OS_read_value(0, 32, &if_events0_rd[0]);
                    entry->match_hpm.IPV6[1] = MEA_OS_read_value(32, 32, &if_events0_rd[0]);
                    entry->match_hpm.IPV6[2] = MEA_OS_read_value(64, 32, &if_events0_rd[0]);
                    entry->match_hpm.IPV6[3] = MEA_OS_read_value(96, 32, &if_events0_rd[0]);

                    entry->match_hpm.L2_pri = MEA_OS_read_value(128, 3, &if_events0_rd[0]);
                    entry->match_hpm.IP_protocol = MEA_OS_read_value(131, 8, &if_events0_rd[0]);
                    entry->match_hpm.DSCP = MEA_OS_read_value(139, 6, &if_events0_rd[0]);
                    entry->match_hpm.Internal_ethertype = MEA_OS_read_value(145, 3, &if_events0_rd[0]);
                    entry->match_hpm.prof_rule = MEA_OS_read_value(150, 6, &if_events0_rd[0]);
                    entry->match_hpm.UE_id = MEA_OS_read_value(156, 14, &if_events0_rd[0]);

                    entry->match_hpm.Qos_win = MEA_OS_read_value(171, 3, &if_events0_rd[0]);
                    entry->match_hpm.pri_rule_win = MEA_OS_read_value(174, 5, &if_events0_rd[0]);




                }


            }



                

        } // 
    if (MEA_HPM_NEW_SUPPORT) 
        {

            MEA_OS_memset(&if_events0_rd[0], 0, sizeof(if_events0_rd));
            ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_HPM_UNMATCH_EVENTS;
            ind_read.read_data = &(if_events0_rd[0]);
            if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
                &ind_read,
                MEA_NUM_OF_ELEMENTS(if_events0_rd),
                MEA_MODULE_IF,
                globalMemoryMode,
                MEA_MEMORY_READ_IGNORE) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }
           



            entry->unmatch_hpm.type_IPv4_6 = MEA_OS_read_value(130, 3, &if_events0_rd[0]);



            if (entry->unmatch_hpm.type_IPv4_6 != 0)
            {
                if ((entry->unmatch_hpm.type_IPv4_6 == MEA_TFT_TYPE_NO_IP) ||
                    (entry->unmatch_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV4)) {
                    entry->unmatch_hpm.L4_Source         = MEA_OS_read_value(0, 16, &if_events0_rd[0]);
                    entry->unmatch_hpm.L4_Dest           = MEA_OS_read_value(16, 16, &if_events0_rd[0]);
                    entry->unmatch_hpm.Source_IPv4       = MEA_OS_read_value(32, 32, &if_events0_rd[0]);
                    entry->unmatch_hpm.Dest_IPv4         = MEA_OS_read_value(64, 32, &if_events0_rd[0]);
                    entry->unmatch_hpm.IP_protocol       = MEA_OS_read_value(96, 8, &if_events0_rd[0]);
                    entry->unmatch_hpm.DSCP              = MEA_OS_read_value(104, 6, &if_events0_rd[0]);
                    entry->unmatch_hpm.L2_pri            = MEA_OS_read_value(110, 3, &if_events0_rd[0]);
                    
                    
                    entry->unmatch_hpm.Internal_ethertype = MEA_OS_read_value(113, 3, &if_events0_rd[0]);

                    entry->unmatch_hpm.prof_rule = MEA_OS_read_value(133, 6, &if_events0_rd[0]);
                    entry->unmatch_hpm.UE_id = MEA_OS_read_value(139, 14, &if_events0_rd[0]);
                }
                if ((entry->unmatch_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV6) ||
                    (entry->unmatch_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV6_Source)   ) {

                    entry->unmatch_hpm.L4_Source = MEA_OS_read_value(0, 16, &if_events0_rd[0]);
                    entry->unmatch_hpm.L4_Dest   = MEA_OS_read_value(16, 16, &if_events0_rd[0]);
                    entry->unmatch_hpm.IPV6[0] = 0;
                    entry->unmatch_hpm.IPV6[1] = 0;
                    entry->unmatch_hpm.IPV6[2] = MEA_OS_read_value(32, 32, &if_events0_rd[0]);
                    entry->unmatch_hpm.IPV6[3] = MEA_OS_read_value(64, 32, &if_events0_rd[0]);
                    entry->unmatch_hpm.next_heder = MEA_OS_read_value(96, 8, &if_events0_rd[0]);
                    entry->unmatch_hpm.DSCP = MEA_OS_read_value(104, 6, &if_events0_rd[0]);
                    entry->unmatch_hpm.Flow_label = MEA_OS_read_value(110, 20, &if_events0_rd[0]);

                    
                    
                    entry->unmatch_hpm.prof_rule = MEA_OS_read_value(133, 6, &if_events0_rd[0]);
                    entry->unmatch_hpm.UE_id = MEA_OS_read_value(139, 14, &if_events0_rd[0]);

                }


            }

            MEA_OS_memset(&if_events0_rd[0], 0, sizeof(if_events0_rd));
            ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_HPM_MATCH_EVENTS;
            ind_read.read_data = &(if_events0_rd[0]);
            if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
                &ind_read,
                MEA_NUM_OF_ELEMENTS(if_events0_rd),
                MEA_MODULE_IF,
                globalMemoryMode,
                MEA_MEMORY_READ_IGNORE) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }



            /************************************************************************/
            /*                                                                      */
            /************************************************************************/



            entry->match_hpm.type_IPv4_6 = MEA_OS_read_value(130, 3, &if_events0_rd[0]);

            if (entry->match_hpm.type_IPv4_6 != 0)
            {
                if ((entry->match_hpm.type_IPv4_6 == MEA_TFT_TYPE_NO_IP) ||
                    (entry->match_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV4)) {

                   

                    entry->match_hpm.L4_Source = MEA_OS_read_value(0, 16, &if_events0_rd[0]);
                    entry->match_hpm.L4_Dest = MEA_OS_read_value(16, 16, &if_events0_rd[0]);
                    entry->match_hpm.Source_IPv4 = MEA_OS_read_value(32, 32, &if_events0_rd[0]);
                    entry->match_hpm.Dest_IPv4 = MEA_OS_read_value(64, 32, &if_events0_rd[0]);
                    entry->match_hpm.IP_protocol = MEA_OS_read_value(96, 8, &if_events0_rd[0]);
                    entry->match_hpm.DSCP = MEA_OS_read_value(104, 6, &if_events0_rd[0]);
                    entry->match_hpm.L2_pri = MEA_OS_read_value(110, 3, &if_events0_rd[0]);


                    entry->match_hpm.Internal_ethertype = MEA_OS_read_value(113, 3, &if_events0_rd[0]);

                    entry->match_hpm.prof_rule = MEA_OS_read_value(133, 6, &if_events0_rd[0]);
                    entry->match_hpm.UE_id = MEA_OS_read_value(139, 14, &if_events0_rd[0]);

                    entry->match_hpm.Qos_win = MEA_OS_read_value(154, 3, &if_events0_rd[0]);
                    entry->match_hpm.pri_rule_win = MEA_OS_read_value(157, 5, &if_events0_rd[0]);





                }
                if ((entry->match_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV6)||
                    (entry->match_hpm.type_IPv4_6 == MEA_TFT_TYPE_IPV6_Source)){
                    entry->match_hpm.L4_Source = MEA_OS_read_value(0, 16, &if_events0_rd[0]);
                    entry->match_hpm.L4_Dest = MEA_OS_read_value(16, 16, &if_events0_rd[0]);
                    entry->match_hpm.IPV6[0] = 0;
                    entry->match_hpm.IPV6[1] = 0;
                    entry->match_hpm.IPV6[2] = MEA_OS_read_value(32, 32, &if_events0_rd[0]);
                    entry->match_hpm.IPV6[3] = MEA_OS_read_value(64, 32, &if_events0_rd[0]);
                    entry->match_hpm.next_heder = MEA_OS_read_value(96, 8, &if_events0_rd[0]);
                    entry->match_hpm.DSCP = MEA_OS_read_value(104, 6, &if_events0_rd[0]);
                    entry->match_hpm.Flow_label = MEA_OS_read_value(110, 20, &if_events0_rd[0]);



                    entry->match_hpm.prof_rule = MEA_OS_read_value(133, 6, &if_events0_rd[0]);
                    entry->match_hpm.UE_id = MEA_OS_read_value(139, 14, &if_events0_rd[0]);

                    entry->match_hpm.Qos_win = MEA_OS_read_value(154, 3, &if_events0_rd[0]);
                    entry->match_hpm.pri_rule_win = MEA_OS_read_value(157, 5, &if_events0_rd[0]);





                }


            }





        } // 









    
   MEA_OS_memset(&if_service_event[0], 0, sizeof(if_service_event));
   
	ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_SERVICE_EVENT; 
	ind_read.read_data   = &(if_service_event[0]);
    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(if_service_event),
                                  MEA_MODULE_IF,
							      globalMemoryMode,
							      MEA_MEMORY_READ_IGNORE)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
	    return MEA_ERROR;
    }







    count_shift = 0;

    entry->if_service_event.errors_exactMatch_fifo = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_errors_exactMatch_fifo_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_errors_exactMatch_fifo_WIDTH;

    entry->if_service_event.errors_range_fifo = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_errors_range_fifo_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_errors_range_fifo_WIDTH;

    entry->if_service_event.exactMatchService_hit  = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_exactMatchService_hit_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_exactMatchService_hit_WIDTH ;

    entry->if_service_event.rangeMatchService_hit = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_rangeMatchService_hit_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_rangeMatchService_hit_WIDTH;
    
    entry->if_service_event.last_service_ctx_id = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_last_service_ctx_id_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_last_service_ctx_id_WIDTH;
    
    entry->if_service_event.range_service_last_key_net_tag1 = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_range_service_last_key_net_tag1_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_range_service_last_key_net_tag1_WIDTH;
    
    entry->if_service_event.range_service_last_key_net_tag2 = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_range_service_last_key_net_tag2_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_range_service_last_key_net_tag2_WIDTH;
        
    entry->if_service_event.range_service_last_key_pri = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_range_service_last_key_pri_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_range_service_last_key_pri_WIDTH;

    
    entry->if_service_event.range_service_last_key_l2type = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_range_service_last_key_l2type_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_range_service_last_key_l2type_WIDTH;
        
    entry->if_service_event.range_service_Reserv_WIDTH = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_range_service_Reserv_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_range_service_Reserv_WIDTH;
	

		
    save_count_shift = count_shift;
        
    entry->if_service_event.unmatch_pri = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_unmatch_pri_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_unmatch_pri_WIDTH;

    entry->if_service_event.unmatch_src_port = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_unmatch_src_port_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_unmatch_src_port_WIDTH;

    entry->if_service_event.unmatch_net_tag1 = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_unmatch_net_tag1_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_unmatch_net_tag1_WIDTH;
    entry->if_service_event.unmatch_subType = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_unmatch_SubType, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_unmatch_SubType;
        
    entry->if_service_event.unmatch_L2_type = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_unmatch_L2_type_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_unmatch_L2_type_WIDTH;

    entry->if_service_event.unmatch_net_tag2 = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_unmatch_net_tag2_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_unmatch_net_tag2_WIDTH;

    

    if ((entry->if_service_event.unmatch_L2_type == 1) &&
        ((entry->if_service_event.unmatch_subType == 4) || (entry->if_service_event.unmatch_subType == 5))) 
    {
        /************************************************************************/
        /* need to read                                                         */
        /************************************************************************/
        entry->if_service_event.unmatch_ISv6 = MEA_TRUE;
//         for (i = 0; i < 4; i++)
//         {
//             MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "DBG %s%d  0x%08x   \n", "sig", i, entry->if_service_event.unmatch_sig[i]);
//         }

        entry->if_service_event.unmatch_sig[0] = 0;
        entry->if_service_event.unmatch_sig[0] = MEA_OS_read_value(save_count_shift, MEA_EVENT_LAST_CLS_SIG0_WIDTH, &if_service_event[0]);
        save_count_shift += MEA_EVENT_LAST_CLS_SIG0_WIDTH;
        entry->if_service_event.unmatch_sig[1] = 0;
        entry->if_service_event.unmatch_sig[1] = MEA_OS_read_value(save_count_shift, MEA_EVENT_LAST_CLS_SIG1_3B_WIDTH, &if_service_event[0]);
        save_count_shift += MEA_EVENT_LAST_CLS_SIG1_3B_WIDTH;
        entry->if_service_event.unmatch_src_port = MEA_OS_read_value(save_count_shift , MEA_EVENT_LAST_CLS_SRC_PORT_WIDTH, &if_service_event[0]);
        save_count_shift += MEA_EVENT_LAST_CLS_SRC_PORT_WIDTH;
        entry->if_service_event.unmatch_net_tag1 = MEA_OS_read_value(save_count_shift, MEA_EVENT_LAST_CLS_NET1_WIDTH, &if_service_event[0]);
        save_count_shift += MEA_EVENT_LAST_CLS_NET1_WIDTH;

        entry->if_service_event.unmatch_subType = MEA_OS_read_value(save_count_shift, MEA_EVENT_LAST_CLS_SubType_WIDTH, &if_service_event[0]);
        save_count_shift += MEA_EVENT_LAST_CLS_SubType_WIDTH;
        entry->if_service_event.unmatch_L2_type = MEA_OS_read_value(save_count_shift, MEA_EVENT_LAST_CLS_L2TYPE_WIDTH, &if_service_event[0]);
        save_count_shift += MEA_EVENT_LAST_CLS_L2TYPE_WIDTH;

        entry->if_service_event.unmatch_sig[1] |= (MEA_OS_read_value(save_count_shift , MEA_EVENT_LAST_CLS_SIG1_6B_WIDTH, &if_service_event[0]))<<3;
        save_count_shift += MEA_EVENT_LAST_CLS_SIG1_6B_START;

        entry->if_service_event.unmatch_sig[2] = 0;
        entry->if_service_event.unmatch_sig[2] = MEA_OS_read_value(save_count_shift, MEA_EVENT_LAST_CLS_SIG2_WIDTH, &if_service_event[0]);
        save_count_shift += MEA_EVENT_LAST_CLS_SIG2_WIDTH;
        entry->if_service_event.unmatch_sig[3] = 0;
        entry->if_service_event.unmatch_sig[3] = MEA_OS_read_value(save_count_shift , MEA_EVENT_LAST_CLS_SIG3_WIDTH, &if_service_event[0]);
        
//         for (i = 0; i < 4; i++)
//         {
//             MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "DBG %s%d  0x%08x   \n", "sig", i, entry->if_service_event.unmatch_sig[i]);
//         }

    }


    entry->if_service_event.unmatch_out_src_port = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_unmatch_out_src_port_WIDTH, &if_service_event[0]);
    count_shift += MEA_IF_SRV_EVENT_unmatch_out_src_port_WIDTH;
	entry->if_service_event.unmatch_last_valid = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_unmatch_last_unmatch_VALID, &if_service_event[0]);
	count_shift += MEA_IF_SRV_EVENT_unmatch_last_unmatch_VALID;
#ifdef MEA_NEW_FWD_ROW_SET 
	entry->if_service_event.No_match_all_entry = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_No_match_all_entry, &if_service_event[0]);
	count_shift += MEA_IF_SRV_EVENT_No_match_all_entry;

	entry->if_service_event.match_External_srv = MEA_OS_read_value(count_shift, MEA_IF_SRV_EVENT_match_External_srv, &if_service_event[0]);
	count_shift += MEA_IF_SRV_EVENT_match_External_srv;
#endif



    
	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_IF_INDICATIONS_EVENT;
    ind_read.read_data      = &(entry->if_port_indication.regs[0]);    

	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
                             MEA_NUM_OF_ELEMENTS(entry->if_port_indication.regs),
                             MEA_MODULE_IF,
							 globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }

  
 
     ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_XMAC_EVENT;
     ind_read.read_data      = &(if_XAUI_xmac[0]);    

     if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
         MEA_NUM_OF_ELEMENTS(if_XAUI_xmac),
         MEA_MODULE_IF,
         globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                 __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
             return MEA_ERROR;
     }


entry->if_XAUI_xmac.xmac0_cnt           = MEA_OS_read_value( 0,32,&if_XAUI_xmac[0]);
entry->if_XAUI_xmac.xmac0_ipg_too_short = MEA_OS_read_value( 32,1,&if_XAUI_xmac[0]);

entry->if_XAUI_xmac.xmac1_cnt           = MEA_OS_read_value( 48,32,&if_XAUI_xmac[0]);
entry->if_XAUI_xmac.xmac1_ipg_too_short = MEA_OS_read_value( 80,1,&if_XAUI_xmac[0]);


	ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_TX_MAC_OVER_FLOW_EVENT;
	ind_read.read_data      = &(entry->if_tx_mac_overflow.regs[0]);    

	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
		MEA_NUM_OF_ELEMENTS(entry->if_tx_mac_overflow.regs),
		MEA_MODULE_IF,
		globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
				__FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
			return MEA_ERROR;
	}

    ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_DBG1_EVENT;
    ind_read.read_data      = &(entry->if_parser_debug1_event.regs[0]);    

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_parser_debug1_event.regs),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }
    ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_DBG2_EVENT;
    ind_read.read_data      = &(entry->if_parser_debug2_event.regs[0]);    

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_parser_debug2_event.regs),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }
    ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_DBG3_EVENT;
    ind_read.read_data      = &(entry->if_parser_debug3_event.regs[0]);    

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_parser_debug3_event.regs),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }
    ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_DBG4_EVENT;
    ind_read.read_data      = &(entry->if_parser_debug4_event.regs[0]);    

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_parser_debug4_event.regs),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }
    

    mea_get_Last_Reassembly_classifier_event(unit, 0 /*MACH*/,   &entry->match_ip_reassembly_entry);
    mea_get_Last_Reassembly_classifier_event(unit, 1 /*UnMACH*/, &entry->unmatch_ip_reassembly_entry);

    
    ind_read.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_DUMMY_ACK_TO_IF_TBL; 
    ind_read.read_data      = &(entry->if_check_tbl_event.regs[0]);  

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_check_tbl_event.regs),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }


    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_1588_EVENT;
    ind_read.read_data = &(entry->if_1588_event.regs[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_1588_event.regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }


    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_EVENT_ACTION;
    ind_read.read_data = &(entry->if_external_Act_event.regs[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_1588_event.regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }


	

   

  /*############################*/
  /*   LPM Event */
		ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LPM_EVENT;
	ind_read.read_data = &(if_lpm_rd[0]);

	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
		MEA_NUM_OF_ELEMENTS(if_lpm_rd),
		MEA_MODULE_IF,
		globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
			__FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
		return MEA_ERROR;
	}
	
	MEA_OS_memcpy(&entry->if_Lpm_event.regs, &if_lpm_rd, sizeof(if_lpm_rd));

	
	entry->if_Lpm_event.lastlpm_key_type_ip = MEA_OS_read_value(64, 1, if_lpm_rd);
	if (entry->if_Lpm_event.lastlpm_key_type_ip == 0)
	{ /* Ipv4 */
		entry->if_Lpm_event.lastlpm_searchKey.v[0] = MEA_OS_read_value(0, 32, if_lpm_rd);
		entry->if_Lpm_event.last_vrf = MEA_OS_read_value(32, 3, if_lpm_rd);
		entry->if_Lpm_event.last_vrf = MEA_OS_read_value(35, 2, if_lpm_rd);
		
	}
	else {
		entry->if_Lpm_event.lastlpm_searchKey.v[0] = MEA_OS_read_value(0, 32, if_lpm_rd);
		entry->if_Lpm_event.lastlpm_searchKey.v[1] = MEA_OS_read_value(32, 32, if_lpm_rd);
	
	}
	
		



    entry->if_Lpm_event.lastlpm_action_Valid     = ((if_lpm_rd[4] >> 28) & 0x1);
	entry->if_Lpm_event.lastlpm_action           = ((if_lpm_rd[4] >> 10) & 0x3ffff); //18bitAction
	entry->if_Lpm_event.lastlpm_match            = ((if_lpm_rd[4] >>2) & 0xff);
	entry->if_Lpm_event.lastlpm_key_unmatch_type = ((if_lpm_rd[4] >> 1) & 0x1);
	

	entry->if_Lpm_event.last_searchKey_unmatch.v[0] = (if_lpm_rd[2]>>1 );

	entry->if_Lpm_event.last_searchKey_unmatch.v[0] |= ((if_lpm_rd[3] & 0x1) << 31);
	entry->if_Lpm_event.last_searchKey_unmatch.v[1] = (if_lpm_rd[3] >> 1);
	entry->if_Lpm_event.last_searchKey_unmatch.v[1] |= ((if_lpm_rd[4] & 0x1) << 31);

	
	
	




        
    entry->IP_defrag_session_available = MEA_API_ReadReg(MEA_UNIT_0, ENET_IF_IP_DFRAG_STATUS_AVALBEL_SESSION, MEA_MODULE_IF);

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL;
    ind_read.tableOffset = 34;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(entry->if_XLAUI_L4checksum_event.regs[0]);

    

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->if_XLAUI_L4checksum_event.regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }


	


/*****************************************************************************/
    val[0] = 0;
    val[0] = MEA_API_ReadReg(MEA_UNIT_0,MEA_BM_EHP_EVENTS_REG, MEA_MODULE_BM);
    entry->bm_ehp_events.reg=val[0];
    val[0] = MEA_LowLevel_ReadReg(MEA_UNIT_0,MEA_BM_EVENT1 , MEA_MODULE_BM,globalMemoryMode,MEA_MEMORY_READ_SUM);
 	entry->bm_events1.reg=val[0];
    val[1] = MEA_LowLevel_ReadReg(MEA_UNIT_0,MEA_BM_EVENT2 , MEA_MODULE_BM,globalMemoryMode,MEA_MEMORY_READ_SUM);
 	entry->bm_events2.reg=val[1];

    val[2] = MEA_LowLevel_ReadReg(MEA_UNIT_0,MEA_BM_EVENT3 , MEA_MODULE_BM,globalMemoryMode,MEA_MEMORY_READ_SUM);
 	entry->bm_events3.reg=val[2];

    val[3] = MEA_LowLevel_ReadReg(MEA_UNIT_0,MEA_BM_EVENT4 , MEA_MODULE_BM,globalMemoryMode,MEA_MEMORY_READ_SUM);
	entry->bm_events4.reg=val[3];

    if (mea_drv_Get_WRITE_ENGINE_STATE(unit) != MEA_OK){
        return MEA_ERROR;
    }
    entry->bm_events5.reg = mea_global_WRITE_ENGINE_STATE;
    
    entry->bm_events6.reg = MEA_API_ReadReg(MEA_UNIT_0,ENET_BM_EVNT_ST_WRONG_TBL_REG , MEA_MODULE_BM);


     /*system events */
	entry->system_events.reg[0] =MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_CPLD_VER , MEA_MODULE_IF);

    entry->system_status = (MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_GLOBAL1_REG,MEA_MODULE_IF) & 0x00000002)>>1;// systemPLL

    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {

        ind_read.tableType		=   ENET_BM_TBL_EVENT_STATE;
	    ind_read.cmdReg			=	MEA_BM_IA_CMD;      
	    ind_read.cmdMask		=	MEA_BM_IA_CMD_MASK;      
	    ind_read.statusReg		=	MEA_BM_IA_STAT;   
	    ind_read.statusMask		=	MEA_BM_IA_STAT_MASK;   
	    ind_read.tableAddrReg	=	MEA_BM_IA_ADDR;
	    ind_read.tableAddrMask	=	MEA_BM_IA_ADDR_OFFSET_MASK;
        for(i= ENET_TBL_BM_EVENT60_OFFSET_START; i <= ENET_TBL_BM_EVENT60_OFFSET_END; i++ ){
            ind_read.tableOffset	=   i;
    	    switch (i){
            case 0:
                ind_read.read_data	    =   &(entry->bm_events60_0.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_0.reg);
                break;
            case 1:
                ind_read.read_data	    =   &(entry->bm_events60_1.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_1.reg);
                break;
            case 2:
                ind_read.read_data	    =   &(entry->bm_events60_2.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_2.reg);
                break;
            case 3:
                ind_read.read_data	    =   &(entry->bm_events60_3.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_3.reg);
                break;
            case 4:
                ind_read.read_data	    =   &(entry->bm_events60_4.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_4.reg);
                break;
            case 5:
                ind_read.read_data	    =   &(entry->bm_events60_5.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_5.reg);
                break;
            case 6:
                ind_read.read_data	    =   &(entry->bm_events60_6.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_6.reg);
                break;
            case 7:
                ind_read.read_data	    =   &(entry->bm_events60_7.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_7.reg);
                break;
            case 8:
                ind_read.read_data	    =   &(entry->bm_events60_8.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_8.reg);
                break;
            case 9:
                ind_read.read_data	    =   &(entry->bm_events60_9.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_9.reg);
                break;
            case 10:
                ind_read.read_data	    =   &(entry->bm_events60_10.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_10.reg);
                break;
            case 11:
                ind_read.read_data	    =   &(entry->bm_events60_11.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_11.reg);
                break;
            case 12:
                ind_read.read_data	    =   &(entry->bm_events60_12.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_12.reg);
                break;
            case 13:
                ind_read.read_data	    =   &(entry->bm_events60_13.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_13.reg);
                break;
            case 14:
                ind_read.read_data	    =   &(entry->bm_events60_14.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_14.reg);
                break;
            case 15:
                ind_read.read_data	    =   &(entry->bm_events60_15.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_15.reg);
                break;
            case 16:
                ind_read.read_data	    =   &(entry->bm_events60_16.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_16.reg);
                break;
            case 17:
                ind_read.read_data	    =   &(entry->bm_events60_17.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_17.reg);
                break;
            case 18:
                ind_read.read_data	    =   &(entry->bm_events60_18.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_18.reg);
                break;
            
            case 21:
                ind_read.read_data	    =   &(entry->bm_events60_21.reg[0]);
                numOfelements          =   MEA_NUM_OF_ELEMENTS(entry->bm_events60_21.reg);
                break;
            case 22:
                ind_read.read_data = &(entry->bm_events60_22.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_22.reg);
                break;
            case 23:
                ind_read.read_data = &(entry->bm_events60_23.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_23.reg);
                break;
            case 24:
                ind_read.read_data = &(entry->bm_events60_24.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_24.reg);
                break;

            case 25:
                ind_read.read_data = &(entry->bm_events60_25.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_25.reg);
                break;

            case 26:
                ind_read.read_data = &(entry->bm_events60_26.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_26.reg);
                break;
            case 27:
                ind_read.read_data = &(entry->bm_events60_27.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_27.reg);
                break;
            case 28:
                ind_read.read_data = &(entry->bm_events60_28.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_28.reg);
                break;
            case 29:
                ind_read.read_data = &(entry->bm_events60_29.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_29.reg);
                break;
            case 30:
                ind_read.read_data = &(entry->bm_events60_30.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_30.reg);
                break;
            case 31:
                ind_read.read_data = &(entry->bm_events60_31.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_31.reg);
                break;
            case 32:
                ind_read.read_data = &(entry->bm_events60_32.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_32.reg);
                break;
            case 33:
                ind_read.read_data = &(entry->bm_events60_33.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_33.reg);
                break;
            case 34:
                ind_read.read_data = &(entry->bm_events60_34.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_34.reg);
                break;
            case 35:
                ind_read.read_data = &(entry->bm_events60_35.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_35.reg);
                break;
            case 36:
                ind_read.read_data = &(entry->bm_events60_36.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_36.reg);
                break;
            case 37:
                ind_read.read_data = &(entry->bm_events60_37.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_37.reg);
                break;
            case 38:
                ind_read.read_data = &(entry->bm_events60_38.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_38.reg);
                break;
            case 39:
                ind_read.read_data = &(entry->bm_events60_39.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_39.reg);
                break;
            case 40:
                ind_read.read_data = &(entry->bm_events60_40.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_40.reg);
                break;
            case 41:
                ind_read.read_data = &(entry->bm_events60_41.reg[0]);
                numOfelements = MEA_NUM_OF_ELEMENTS(entry->bm_events60_40.reg);
                break;


            default:
                  continue;
                break;
            }
        
        
            if (MEA_API_ReadIndirect(unit,
                                     &ind_read,
                                     numOfelements,
                                     MEA_MODULE_BM) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - ReadIndirect from tableType %x module BM failed "
                                  "(offset=%d)\n",
                                  __FUNCTION__,ind_read.tableType,ind_read.tableOffset);
    		    return MEA_ERROR;
            }
        }
    }	



    entry->BM_descriptor_buffer_status = MEA_API_ReadReg(MEA_UNIT_0,ENET_BM_CURRENT_USED_BUFFERS_AND_DESCRIPTOR_REG,MEA_MODULE_BM);

    entry->BM_descriptor_count_status = MEA_API_ReadReg(MEA_UNIT_0, ENET_BM_DESCRIPTOR_COUNT_STATUS_REG, MEA_MODULE_BM);

    entry->BM_buffer_count_status     = MEA_API_ReadReg(MEA_UNIT_0, ENET_BM_BUFFER_COUNT_STATUS_REG, MEA_MODULE_BM);

    

    MEA_API_BM_INFO_ACK(MEA_UNIT_0, &entry->ack_info);
        

  

       
   

    /* Return to Callers */
    return MEA_OK;   
}



MEA_Status MEA_API_BM_INFO_ACK(MEA_Unit_t    unit , MEA_BM_ack_info *entry)
{

    MEA_ind_read_t         ind_read;

    MEA_API_LOG

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(entry->info.reg[0]);
    ind_read.tableOffset = MEA_GLOBAl_TBL_REG_IF_BM_ACK_INFO;
    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(entry->info.reg),
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);


        return MEA_ERROR;
    }


    return MEA_OK;
}



MEA_Status MEA_API_Get_ACL_Events(MEA_Unit_t   unit,  MEA_Events_ACL_dbt *entry)
{

    MEA_ind_read_t         ind_read;
    MEA_Uint32             if_rd[4];
    MEA_Uint32             if_rd1[5];

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_ACL_0_1;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(if_rd[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(if_rd),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_ACL_2_3;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(if_rd1[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(if_rd1),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

    entry->Last.acl_key[0].v[0] = if_rd[0];
    entry->Last.acl_key[0].v[1] = if_rd[1];
    entry->Last.acl_key[1].v[0] = if_rd[2];
    entry->Last.acl_key[1].v[1] = if_rd[3];

    entry->Last.acl_key[2].v[0] = if_rd1[0];
    entry->Last.acl_key[2].v[1] = if_rd1[1];
    entry->Last.acl_key[3].v[0] = if_rd1[2];
    entry->Last.acl_key[3].v[1] = if_rd1[3];
    entry->Last.result[0] = ((if_rd1[4]>>0) & 0x1) ;
    entry->Last.result[1] = ((if_rd1[4] >> 1) & 0x1);
    entry->Last.result[2] = ((if_rd1[4] >> 2) & 0x1);
    entry->Last.result[3] = ((if_rd1[4] >> 3) & 0x1);
    entry->Last.doAcl = (if_rd1[4] >> 4) & 0xf;
    entry->Last.command = (if_rd1[4] >> 8) & 0x3;
    entry->Last.last_Sid = (if_rd1[4] >> 10) & 0xfff;

   


    /************************************************************************/
    /*               UNMATCH                                               */
    /************************************************************************/
    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_UNMATCH_ACL_0_1;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(if_rd[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(if_rd),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_UNMATCH_ACL_2_3;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(if_rd1[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(if_rd1),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

    entry->unmatch.acl_key[0].v[0] = if_rd[0];
    entry->unmatch.acl_key[0].v[1] = if_rd[1];
    entry->unmatch.acl_key[1].v[0] = if_rd[2];
    entry->unmatch.acl_key[1].v[1] = if_rd[3];

    entry->unmatch.acl_key[2].v[0] = if_rd1[0];
    entry->unmatch.acl_key[2].v[1] = if_rd1[1];
    entry->unmatch.acl_key[3].v[0] = if_rd1[2];
    entry->unmatch.acl_key[3].v[1] = if_rd1[3];
    entry->unmatch.result[0] = ((if_rd1[4] >> 0) & 0x1);
    entry->unmatch.result[1] = ((if_rd1[4] >> 1) & 0x1);
    entry->unmatch.result[2] = ((if_rd1[4] >> 2) & 0x1);
    entry->unmatch.result[3] = ((if_rd1[4] >> 3) & 0x1);
    entry->unmatch.doAcl = (if_rd1[4] >> 4) & 0xf;
    entry->unmatch.command  = (if_rd1[4] >> 8) & 0x3;
    entry->unmatch.last_Sid = (if_rd1[4] >> 10) & 0xfff;


    return MEA_OK;
}


MEA_Status MEA_API_Get_ACL5_Events(MEA_Unit_t   unit, MEA_Events_ACL5_dbt *entry, MEA_ACL5_PAC_MODE ACL5_parser_mode, MEA_Bool *valid)
{

	MEA_ind_read_t         ind_read;
	MEA_Uint32			   data_reg[12] = { 0 };
	MEA_Uint32             if_rd_LSB[10];
	MEA_Uint32             if_rd_MSB[10];
	MEA_Uint32			   i;
	MEA_Uint32			   count_shift=0;
	MEA_Uint32			   temp = 0;



	if (ACL5_parser_mode == MEA_ACL5_Parser_MATCH)
	{
		ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
		ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL5_EVNT1;
		ind_read.cmdReg = MEA_IF_CMD_REG;
		ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
		ind_read.statusReg = MEA_IF_STATUS_REG;
		ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
		ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
		ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
		ind_read.read_data = &(if_rd_LSB[0]);

		if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
			MEA_NUM_OF_ELEMENTS(if_rd_LSB),
			MEA_MODULE_IF,
			globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
				__FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
			return MEA_ERROR;
		}


		ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL5_EVNT2;
		ind_read.read_data = &(if_rd_MSB[0]);

		if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
			MEA_NUM_OF_ELEMENTS(if_rd_MSB),
			MEA_MODULE_IF,
			globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
				__FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
			return MEA_ERROR;
		}

		if ((if_rd_MSB[9] >> 31) == 0x1)
			entry->flag_match = MEA_ACL5_Parser_MATCH;
		else
		{
			entry->flag_match = MEA_ACL5_Parser_UNMATCH;
			*valid = MEA_FALSE;
			return MEA_OK;
		}

		for (i = 0; i < 10; i++)
			data_reg[i] = if_rd_LSB[i];

		data_reg[10] = if_rd_MSB[0];
		data_reg[11] = if_rd_MSB[1];

		entry->Last.keyType = MEA_OS_read_value(MEA_IF_ACL5_KEY_TYPE_START,
			MEA_IF_ACL5_KEY_TYPE_WIDTH,
			data_reg);

		count_shift = 0;

		switch (entry->Last.keyType)
		{
		case  MEA_ACL5_KeyType_NONE:
			break;
		case  MEA_ACL5_KeyType_L2:
		{
			count_shift = 100;

			entry->Last.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->Last.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->Last.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->Last.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);
			temp = 0;

			entry->Last.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 6; i++)
			{
				entry->Last.layer2.Mac.MacDA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_DA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			for (i = 0; i < 6; i++)
			{
				entry->Last.layer2.Mac.MacSA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_SA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			entry->Last.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->Last.layer2.Tag2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG2_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG2_WIDTH;

			entry->Last.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH + 73;

			entry->Last.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+2);

			entry->Last.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
		}
			break;
		case  MEA_ACL5_KeyType_IPV4:
		{
			entry->Last.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->Last.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->Last.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH + 1;

			entry->Last.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->Last.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

			entry->Last.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->Last.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->Last.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->Last.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+1);
			temp = 0;

			entry->Last.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 6; i++)
			{
				entry->Last.layer2.Mac.MacDA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_DA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			for (i = 0; i < 6; i++)
			{
				entry->Last.layer2.Mac.MacSA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_SA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			entry->Last.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->Last.layer2.Tag2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG2_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG2_WIDTH;

			entry->Last.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH;

			entry->Last.layer3.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_FLAGS_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_FLAGS_WIDTH + 7;

			entry->Last.layer3.IPv4.src_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.layer3.IPv4.dst_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+2);

			entry->Last.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
		}
			break;
		case MEA_ACL5_KeyType_IPV6_FULL:
		{
			entry->Last.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->Last.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->Last.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH + 1;

			entry->Last.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->Last.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

			entry->Last.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->Last.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->Last.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->Last.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+1);
			temp = 0;

			entry->Last.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 4; i++)
			{
				entry->Last.layer3.IPv6.Dst_Ipv6[i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;
			}
			for (i = 0; i < 4; i++)
			{
				entry->Last.layer3.IPv6.Src_Ipv6[i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;
			}
		}
			break;
		case MEA_ACL5_KeyType_IPV6_DST:
		{
			entry->Last.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->Last.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->Last.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.IPv6.IPv6_Flow_Label = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FLOW_LABEL_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

			entry->Last.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->Last.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->Last.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->Last.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);
			temp = 0;

			entry->Last.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 4; i++)
			{
				entry->Last.layer3.IPv6.Dst_Ipv6[i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;
			}
			for (i = 0; i < 6; i++)
			{
				entry->Last.layer2.Mac.MacDA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_DA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}
			entry->Last.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->Last.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

			entry->Last.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG3_29_BITS_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG3_29_BITS_WIDTH;

			entry->Last.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->Last.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH;

			entry->Last.layer2.Tag3 = 0;
			 temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->Last.layer2.Tag3 |= (temp << 28);

			entry->Last.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->Last.layer2.Tag3 |= (temp << 29);

			entry->Last.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

			entry->Last.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->Last.layer2.Tag3 |= (temp << 30);
		}
			break;
		case MEA_ACL5_KeyType_IPV6_SRC:
		{
			entry->Last.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->Last.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->Last.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.IPv6.IPv6_Flow_Label = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FLOW_LABEL_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

			entry->Last.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->Last.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->Last.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->Last.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+1);
			temp = 0;

			entry->Last.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			entry->Last.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->Last.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

			entry->Last.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG3_29_BITS_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG3_29_BITS_WIDTH;

			for (i = 0; i < 6; i++)
			{
				entry->Last.layer2.Mac.MacSA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_SA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			entry->Last.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->Last.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH;

			entry->Last.layer2.Tag3 = 0;
			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->Last.layer2.Tag3 |= (temp << 28);

			entry->Last.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->Last.layer2.Tag3 |= (temp << 29);

			entry->Last.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+2);

			entry->Last.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->Last.layer2.Tag3 |= (temp << 30);

			for (i = 0; i < 4; i++)
			{
				entry->Last.layer3.IPv6.Src_Ipv6[i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;
			}
		}
				break;
		case MEA_ACL5_KeyType_IPV6_SIG_Dest:
		{
			entry->Last.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->Last.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->Last.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.IPv6.IPv6_Flow_Label = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FLOW_LABEL_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

			entry->Last.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->Last.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->Last.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->Last.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+1);
			temp = 0;

			entry->Last.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 6; i++)
			{
				entry->Last.layer2.Mac.MacDA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_DA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}
			entry->Last.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->Last.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH + 29;

			entry->Last.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->Last.layer2.Tag2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG2_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG2_WIDTH;

			entry->Last.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH + 9;

			entry->Last.layer3.IPv4.src_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.layer3.IPv4.dst_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+2);

			entry->Last.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
		}
			break;
		case MEA_ACL5_KeyType_IPV6_SIG_SRC:
		{
			entry->Last.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->Last.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->Last.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->Last.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->Last.layer3.IPv6.IPv6_Flow_Label = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FLOW_LABEL_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

			entry->Last.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->Last.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->Last.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->Last.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+1);
			temp = 0;

			entry->Last.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			entry->Last.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->Last.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH + 29;

			for (i = 0; i < 6; i++)
			{
				entry->Last.layer2.Mac.MacSA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_SA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			entry->Last.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->Last.layer2.Tag2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG2_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG2_WIDTH;

			entry->Last.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH + 9;

			entry->Last.layer3.IPv4.src_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.layer3.IPv4.dst_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->Last.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH + 2);

			entry->Last.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
		}
			break;
		default:
			break;
		}

	}
	/************************************************************************/
	/*               UNMATCH                                               */
	/************************************************************************/
	if (ACL5_parser_mode == MEA_ACL5_Parser_UNMATCH)
	{
		ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
		ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL5_EVNT3;
		ind_read.cmdReg = MEA_IF_CMD_REG;
		ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
		ind_read.statusReg = MEA_IF_STATUS_REG;
		ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
		ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
		ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
		ind_read.read_data = &(if_rd_LSB[0]);

		if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
			MEA_NUM_OF_ELEMENTS(if_rd_LSB),
			MEA_MODULE_IF,
			globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
				__FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
			return MEA_ERROR;
		}

		ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL5_EVNT4;
		ind_read.read_data = &(if_rd_MSB[0]);

		if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
			MEA_NUM_OF_ELEMENTS(if_rd_MSB),
			MEA_MODULE_IF,
			globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
				__FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
			return MEA_ERROR;
		}

		if (((if_rd_MSB[9] >> 31) & 0x1) == MEA_ACL5_Parser_MATCH)
			entry->flag_unmatch = MEA_ACL5_Parser_MATCH;
		else
		{
			entry->flag_unmatch = MEA_ACL5_Parser_UNMATCH;
			*valid = MEA_FALSE;
			return MEA_OK;
		}



		for (i = 0; i < 10; i++)
			data_reg[i] = if_rd_LSB[i];

		data_reg[10] = if_rd_MSB[0];
		data_reg[11] = if_rd_MSB[1];




		//count_shift = 120;
		entry->unmatch.keyType = MEA_OS_read_value(MEA_IF_ACL5_KEY_TYPE_START,
			MEA_IF_ACL5_KEY_TYPE_WIDTH,
			data_reg);
		count_shift += MEA_IF_ACL5_KEY_TYPE_WIDTH;

		count_shift = 0;

		switch (entry->unmatch.keyType)
		{
		case  MEA_ACL5_KeyType_NONE:
			break;
		case  MEA_ACL5_KeyType_L2:
		{
			count_shift = 100;

			entry->unmatch.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->unmatch.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->unmatch.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);
			temp = 0;

			entry->unmatch.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 6; i++)
			{
				entry->unmatch.layer2.Mac.MacDA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_DA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			for (i = 0; i < 6; i++)
			{
				entry->unmatch.layer2.Mac.MacSA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_SA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			entry->unmatch.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->unmatch.layer2.Tag2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG2_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG2_WIDTH;

			entry->unmatch.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->unmatch.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH + 73;

			entry->unmatch.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

			entry->unmatch.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
		}
		break;
		case  MEA_ACL5_KeyType_IPV4:
		{
			entry->unmatch.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->unmatch.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->unmatch.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH + 1;

			entry->unmatch.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->unmatch.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

			entry->unmatch.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->unmatch.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->unmatch.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH + 1);
			temp = 0;

			entry->unmatch.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 6; i++)
			{
				entry->unmatch.layer2.Mac.MacDA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_DA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			for (i = 0; i < 6; i++)
			{
				entry->unmatch.layer2.Mac.MacSA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_SA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			entry->unmatch.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->unmatch.layer2.Tag2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG2_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG2_WIDTH;

			entry->unmatch.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->unmatch.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH;

			entry->unmatch.layer3.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_FLAGS_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_FLAGS_WIDTH + 7;

			entry->unmatch.layer3.IPv4.src_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->unmatch.layer3.IPv4.dst_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

			entry->unmatch.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
		}
		break;
		case MEA_ACL5_KeyType_IPV6_FULL:
		{
			entry->unmatch.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->unmatch.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->unmatch.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH + 1;

			entry->unmatch.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->unmatch.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

			entry->unmatch.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->unmatch.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->unmatch.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH+1);
			temp = 0;

			entry->unmatch.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 4; i++)
			{
				entry->unmatch.layer3.IPv6.Dst_Ipv6[i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;
			}
			for (i = 0; i < 4; i++)
			{
				entry->unmatch.layer3.IPv6.Src_Ipv6[i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;
			}
		}
		break;
		case MEA_ACL5_KeyType_IPV6_DST:
		{
			entry->unmatch.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->unmatch.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->unmatch.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.IPv6.IPv6_Flow_Label = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FLOW_LABEL_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

			entry->unmatch.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->unmatch.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->unmatch.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);
			temp = 0;

			entry->unmatch.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 4; i++)
			{
				entry->unmatch.layer3.IPv6.Dst_Ipv6[i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;
			}
			for (i = 0; i < 6; i++)
			{
				entry->unmatch.layer2.Mac.MacDA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_DA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}
			entry->unmatch.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->unmatch.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

			entry->unmatch.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG3_29_BITS_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG3_29_BITS_WIDTH;

			entry->unmatch.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->unmatch.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH;

			entry->unmatch.layer2.Tag3 = 0;
			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->unmatch.layer2.Tag3 |= (temp << 28);

			entry->unmatch.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->unmatch.layer2.Tag3 |= (temp << 29);

			entry->unmatch.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

			entry->unmatch.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->unmatch.layer2.Tag3 |= (temp << 30);
		}
		break;
		case MEA_ACL5_KeyType_IPV6_SRC:
		{
			entry->unmatch.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->unmatch.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->unmatch.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.IPv6.IPv6_Flow_Label = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FLOW_LABEL_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

			entry->unmatch.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->unmatch.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->unmatch.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);
			temp = 0;

			entry->unmatch.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			entry->unmatch.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->unmatch.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH;

			entry->unmatch.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG3_29_BITS_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG3_29_BITS_WIDTH;

			for (i = 0; i < 6; i++)
			{
				entry->unmatch.layer2.Mac.MacSA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_SA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			entry->unmatch.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->unmatch.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH;

			entry->unmatch.layer2.Tag3 = 0;
			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->unmatch.layer2.Tag3 |= (temp << 28);

			entry->unmatch.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->unmatch.layer2.Tag3 |= (temp << 29);

			entry->unmatch.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

			entry->unmatch.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
			entry->unmatch.layer2.Tag3 |= (temp << 30);

			for (i = 0; i < 4; i++)
			{
				entry->unmatch.layer3.IPv6.Src_Ipv6[i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;
			}
		}
		break;
		case MEA_ACL5_KeyType_IPV6_SIG_Dest:
		{
			entry->unmatch.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->unmatch.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->unmatch.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.IPv6.IPv6_Flow_Label = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FLOW_LABEL_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

			entry->unmatch.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->unmatch.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->unmatch.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);
			temp = 0;

			entry->unmatch.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			for (i = 0; i < 6; i++)
			{
				entry->unmatch.layer2.Mac.MacDA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_DA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}
				entry->unmatch.rangkProf_rule = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_RNG_RSLT_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

				entry->unmatch.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH +29;

				entry->unmatch.layer2.Tag1 = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_TAG1_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_TAG1_WIDTH;

				entry->unmatch.layer2.Tag2 = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_TAG2_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_TAG2_WIDTH;

				entry->unmatch.layer2.Tag3 = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

				entry->unmatch.layer2.Tag1_p = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_TAG1_P_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_TAG1_P_WIDTH +9;

				entry->unmatch.layer3.IPv4.src_ip = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

				entry->unmatch.layer3.IPv4.dst_ip = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_FOUR_BYTES_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

				entry->unmatch.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BIT_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

				entry->unmatch.ACL5_key_mask_prof.TAG2 = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BIT_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

				entry->unmatch.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BIT_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

				entry->unmatch.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BIT_WIDTH,
					data_reg);
				count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);


				entry->unmatch.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BIT_WIDTH,
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
		}
			break;
		case MEA_ACL5_KeyType_IPV6_SIG_SRC:
		{
			entry->unmatch.layer3.ethertype3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer3.ip_protocol = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer4.dst_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.src_port = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TWO_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TWO_BYTES_WIDTH;

			entry->unmatch.layer4.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_DSCP_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_DSCP_WIDTH;

			entry->unmatch.layer3.l3_pri_type = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_PRIORITY_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_PRIORITY_WIDTH;

			entry->unmatch.Acl5_mask_prof.range_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BYTE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;

			entry->unmatch.layer3.IPv6.IPv6_Flow_Label = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FLOW_LABEL_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FLOW_LABEL_WIDTH;

			entry->unmatch.layer2.Mac.MAC_UMB = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_UMB_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_UMB_WIDTH;

			entry->unmatch.ACLprof = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_SID_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_SID_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L3_EtherType = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.Ip_Protocol_NH = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4dst = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.L4src = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TCP_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.DSCP_TC = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			temp = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			temp = temp << 7;
			entry->unmatch.Acl5_mask_prof.range_mask_id |= temp;
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +1);
			temp = 0;

			entry->unmatch.ACL5_key_mask_prof.IPv4_Flags = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.Acl5_mask_prof.Inner_Frame = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH + 4;

			entry->unmatch.rangkProf_rule = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_RNG_RSLT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_RNG_RSLT_WIDTH + 4;

			entry->unmatch.Acl5_mask_prof.ip_mask_id = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_IP_MSK_PFILE_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_IP_MSK_PFILE_WIDTH + 29;

			for (i = 0; i < 6; i++)
			{
				entry->unmatch.layer2.Mac.MacSA.b[5 - i] = MEA_OS_read_value(count_shift,
					MEA_IF_ACL5_ONE_BYTE_WIDTH,				//MAC_SA
					data_reg);
				count_shift += MEA_IF_ACL5_ONE_BYTE_WIDTH;
			}

			entry->unmatch.layer2.Tag1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_WIDTH;

			entry->unmatch.layer2.Tag2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG2_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG2_WIDTH;

			entry->unmatch.layer2.Tag3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->unmatch.layer2.Tag1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_TAG1_P_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_TAG1_P_WIDTH + 9;

			entry->unmatch.layer3.IPv4.src_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->unmatch.layer3.IPv4.dst_ip = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_FOUR_BYTES_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_FOUR_BYTES_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG1 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG2 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG3 = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;

			entry->unmatch.ACL5_key_mask_prof.TAG1_p = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += (MEA_IF_ACL5_ONE_BIT_WIDTH +2);

			entry->unmatch.ACL5_key_mask_prof.IPv6FlowLabel = MEA_OS_read_value(count_shift,
				MEA_IF_ACL5_ONE_BIT_WIDTH,
				data_reg);
			count_shift += MEA_IF_ACL5_ONE_BIT_WIDTH;
		}
			break;
		default:
			return MEA_ERROR;
			break;
		}

	}
	return MEA_OK;

}

 MEA_Status mea_drv_Get_WRITE_ENGINE_STATE(MEA_Unit_t   unit)
{

    mea_global_WRITE_ENGINE_STATE = MEA_LowLevel_ReadReg(MEA_UNIT_0, ENET_BM_WRITE_ENGINE_STATE_REG, MEA_MODULE_BM, globalMemoryMode, MEA_MEMORY_READ_SUM);
    /*save the state   */
    return MEA_OK;
}

MEA_Status MEA_API_Get_WRITE_ENGINE_STATE(MEA_Unit_t   unit, MEA_Uint32 *state){
    if (mea_drv_Get_WRITE_ENGINE_STATE(unit) != MEA_OK){
        return MEA_ERROR;
    }
    *state = mea_global_WRITE_ENGINE_STATE;
    return MEA_OK;
}
MEA_Status MEA_API_Clear_WRITE_ENGINE_STATE(MEA_Unit_t   unit){
    mea_global_WRITE_ENGINE_STATE = 0;
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Events_System_Entry>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Events_System_Entry  (MEA_Unit_t             unit,
                                             MEA_Events_System_Entry_dbt*  entry) 
{

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if ( entry == NULL )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Null entry input\n",
                          __FUNCTION__);
        return MEA_ERROR;  
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	/*system events */
	entry->reg[0] =MEA_API_ReadReg(MEA_UNIT_0,ENET_IF_CPLD_VER , MEA_MODULE_IF);

    /* Return to Callers */
    return MEA_OK;   
}



MEA_Status MEA_API_Get_Events_Drop_MTU_Priq(MEA_Unit_t      unit_i,
											ENET_QueueId_t  QueueId_i,
                                            MEA_Uint8      *drop_MTU_priq)
{
    return mea_Get_queue_Drop_MTU_Priq(unit_i, QueueId_i,drop_MTU_priq);
}

/*-------------------------------------------------------*/
/**********************************************************/
/* read the port event  1,sec                             */
/*---------------------------------- ----------------------*/
#define MEA_PORT_LINK_FILTER_COUNT 10

MEA_Uint32 mea_read_gbe_val[10];
MEA_Uint32 mea_read_qxaui_val[2];
MEA_Uint32 mea_read_utopia_val[2];
MEA_Uint32 mea_read_xaui_val[2];
MEA_Uint8  mea_link_port_counter[MEA_MAX_PORT_NUMBER];

MEA_Status mea_drv_Port_Read_GBE_Status(MEA_Unit_t      unit_i)
{
   



    return MEA_OK;
}






MEA_Status mea_drv_Port_Read_QXGMII_Status(MEA_Unit_t      unit_i)
{
    

    return MEA_OK;
}



MEA_Status mea_drv_Port_Read_XAUI_Status(MEA_Unit_t      unit_i)
{
   

    return MEA_OK;
}

MEA_Status mea_drv_Port_Read_UTOPIA_Status(MEA_Unit_t      unit_i)
{
    

    
    return MEA_OK;
}









MEA_Status mea_drv_Port_READ_All_STATUS(MEA_Unit_t      unit_i)
{

      mea_drv_Port_Read_GBE_Status(unit_i);
      mea_drv_Port_Read_QXGMII_Status(unit_i);
      mea_drv_Port_Read_XAUI_Status(unit_i);
      mea_drv_Port_Read_UTOPIA_Status(unit_i);

    return MEA_OK;
}








MEA_Status MEA_API_Port_Status(MEA_Unit_t      unit_i,
                               MEA_Port_t       port,
                               MEA_Port_Status_Event_t *eventStatus)
{
    //MEA_ind_read_t         ind_read;
    MEA_Uint32 gbe_val[10];
	MEA_Uint32 utopia_val[2];
    MEA_Uint32 xaui_val[2];
    MEA_Uint32 qxaui_val[2];
    MEA_Port_type_te porttype;
    MEA_Uint32 val;
    MEA_PortsMapping_t portMap;




    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE) ==MEA_FALSE) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s port %d  is invalid \n",__FUNCTION__,port);
        return MEA_ERROR;
    } 
    if(eventStatus == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," eventStatus = NULL\n");
        return MEA_ERROR;
    }
    if(MEA_API_Get_IngressPort_Type(unit_i,port ,&porttype)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," MEA_API_Get_IngressPort_Type failed port=%d\n",port);
        return MEA_ERROR;
    }
    if (MEA_Low_Get_Port_Mapping_By_key(unit_i,MEA_PORTS_TYPE_INGRESS_TYPE, (MEA_Uint16)port,&portMap)!=MEA_OK){
        return MEA_ERROR ;
    }

    if((
       (porttype != MEA_PORTTYPE_10G_XMAC_ENET)  &&
	   (porttype != MEA_PORTTYPE_GBE_MAC)    &&   
	   (porttype != MEA_PORTTYPE_GBE_1588)      )){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," The API support Only port-type  MEA_PORT_TYPE_GMII/ QXGMII / XGMII / UTOPIA port=%d\n",port);
           return MEA_ERROR;
    }

    if((porttype == MEA_PORTTYPE_GBE_MAC || porttype == MEA_PORTTYPE_GBE_1588) &&
        (portMap.PortTo_interfaceId_Type   ==  MEA_INTERFACETYPE_QSGMII ||
        portMap.PortTo_interfaceId_Type   ==  MEA_INTERFACETYPE_SGMII ) 
        ){

        eventStatus->InterfaceType=(MEA_Uint8)portMap.PortTo_interfaceId_Type;

    MEA_OS_memcpy(&gbe_val[0],&mea_read_gbe_val[0],sizeof(gbe_val));

    switch (port)
    {
        case 0:
          val = (MEA_OS_read_value(0,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 12:
          val = (MEA_OS_read_value(8,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 24:
          val =   (MEA_OS_read_value(16,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 36:
          val =  (MEA_OS_read_value(24,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 48:
          val  =  (MEA_OS_read_value(32,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 72:
          val  =  (MEA_OS_read_value(40,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 100:
          val  =  (MEA_OS_read_value(48,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 101:
          val  = (MEA_OS_read_value(56,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 102:
          val  =  (MEA_OS_read_value(64,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 103:
          val  =  (MEA_OS_read_value(72,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 104:
          val  =  (MEA_OS_read_value(80,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 105:
          val  =  (MEA_OS_read_value(88,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 106:
          val  =  (MEA_OS_read_value(96,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 107:
          val  =  (MEA_OS_read_value(104,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 108:
          val  =  (MEA_OS_read_value(112,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 109:
          val  =  (MEA_OS_read_value(120,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 110:
          val  = (MEA_OS_read_value(128,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 111:
          val  =  (MEA_OS_read_value(136,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 118:
          val  =  (MEA_OS_read_value(144,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 119:
          val  =  (MEA_OS_read_value(152,8,gbe_val));
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 125:
          val  =  (MEA_OS_read_value(160,8,gbe_val));
          //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Status_Event port 125  0x%x\n",val);
		  MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 126:
          val  =  (MEA_OS_read_value(168,8,gbe_val));
          //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Status_Event port 126  0x%x\n",val);
		  MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          break;
        case 92:
            val  =  (MEA_OS_read_value(176,8,gbe_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
            break;
        case 93:
            val  =  (MEA_OS_read_value(184,8,gbe_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
            break;


        default:
            val  = 0;
          MEA_OS_memcpy(&eventStatus->Status_Event.mea_Gbe ,&val  ,sizeof(eventStatus->Status_Event.mea_Gbe));
          return MEA_ERROR;
          break;
    }
    return MEA_OK;
    }

    if((porttype == MEA_PORTTYPE_GBE_MAC || porttype == MEA_PORTTYPE_GBE_1588) &&  /*look on line rate type 2 */
        (portMap.PortTo_interfaceId_Type   ==  MEA_INTERFACETYPE_GMII)

        ){

    
        eventStatus->InterfaceType=MEA_INTERFACETYPE_GMII;

       MEA_OS_memcpy(&qxaui_val[0],&mea_read_qxaui_val[0],sizeof(qxaui_val));

        switch (port)
        {
        case 0:
            val = (MEA_OS_read_value(0,8,qxaui_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Qxaui ,&qxaui_val  ,sizeof(eventStatus->Status_Event.mea_Qxaui));
            break;
        case 12:
            val = (MEA_OS_read_value(8,8,qxaui_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Qxaui ,&qxaui_val  ,sizeof(eventStatus->Status_Event.mea_Qxaui));
            break;
        case 24:
            val = (MEA_OS_read_value(16,8,qxaui_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Qxaui ,&qxaui_val  ,sizeof(eventStatus->Status_Event.mea_Qxaui));
            break;
        case 36:
            val = (MEA_OS_read_value(24,8,qxaui_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Qxaui ,&qxaui_val  ,sizeof(eventStatus->Status_Event.mea_Qxaui));
            break;

        case 48:
            val = (MEA_OS_read_value(32,8,qxaui_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Qxaui ,&qxaui_val  ,sizeof(eventStatus->Status_Event.mea_Qxaui));
            break;
        case 72:
            val = (MEA_OS_read_value(40,8,qxaui_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Qxaui ,&qxaui_val  ,sizeof(eventStatus->Status_Event.mea_Qxaui));
            break;
        case 125:
            val = (MEA_OS_read_value(48,8,qxaui_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Qxaui ,&qxaui_val  ,sizeof(eventStatus->Status_Event.mea_Qxaui));
            break;
        case 126:
            val = (MEA_OS_read_value(56,8,qxaui_val));
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Qxaui ,&qxaui_val  ,sizeof(eventStatus->Status_Event.mea_Qxaui));
            break;
        default:
            val  = 0;
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_Qxaui ,&qxaui_val  ,sizeof(eventStatus->Status_Event.mea_Qxaui));
            return MEA_ERROR;
            break;
        }
        return MEA_OK;
    }
    
    
    if((porttype == MEA_PORTTYPE_10G_XMAC_ENET) &&  /*look on line rate type 2 */
        (portMap.PortTo_interfaceId_Type   ==  MEA_INTERFACETYPE_XAUI)

        ){
   
        eventStatus->InterfaceType=MEA_INTERFACETYPE_XAUI;

         MEA_OS_memcpy(&xaui_val[0],&mea_read_xaui_val[0],sizeof(xaui_val));
        switch (port)
        {
        case 118:
              MEA_OS_memcpy(&eventStatus->Status_Event.mea_XAUI ,&xaui_val[0]  ,sizeof(eventStatus->Status_Event.mea_XAUI));
            break;
        case 119:
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_XAUI ,&xaui_val[1]  ,sizeof(eventStatus->Status_Event.mea_XAUI));
            break;
        }
        return MEA_OK;
    }

    
    if((porttype == MEA_PORTTYPE_10G_XMAC_ENET) &&  /*look on line rate type 2 */
        (portMap.PortTo_interfaceId_Type   ==  MEA_INTERFACETYPE_RXAUI)

        ){
        eventStatus->InterfaceType=MEA_INTERFACETYPE_RXAUI;

        MEA_OS_memcpy(&xaui_val[0],&mea_read_xaui_val[0],sizeof(xaui_val));
        switch (port)
        {
        case 118:
            val=(xaui_val[0] & 0xFF);
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_rxauvi ,&(val)  ,sizeof(eventStatus->Status_Event.mea_XAUI));
            break;
        case 119:
            val=((xaui_val[0]>>8) & 0xFF);
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_rxauvi ,&(val)  ,sizeof(eventStatus->Status_Event.mea_XAUI));
            break;
        case 110:
            val=((xaui_val[0]>>16) & 0xFF);
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_rxauvi ,&(val)  ,sizeof(eventStatus->Status_Event.mea_XAUI));
            break;
        case 111:
            val=((xaui_val[0]>>24) & 0xFF);
            MEA_OS_memcpy(&eventStatus->Status_Event.mea_rxauvi ,&(val)  ,sizeof(eventStatus->Status_Event.mea_XAUI));
            break;



        }
        return MEA_OK;
    }


    if((porttype == MEA_PORTTYPE_AAL5_ATM_TC) &&  
        (portMap.PortTo_interfaceId_Type   ==  MEA_INTERFACETYPE_UTOPIA)

        ){
		eventStatus->InterfaceType=MEA_INTERFACETYPE_UTOPIA;

      MEA_OS_memcpy(&utopia_val[0],&mea_read_utopia_val[0],sizeof(utopia_val));

		if(port>0 && port <=31){
			//utopia_val[0]=gbe_val[7];
			val= (( ((utopia_val[0])>>(port) ) & 0x000000001)== 1 ? 0 : 1 );  /// 1 the port is not sync in the HW 
            
		
			MEA_OS_memcpy(&eventStatus->Status_Event.mea_Utopia ,&val  ,sizeof(eventStatus->Status_Event.mea_Utopia));
            return MEA_OK;
		}else{//64--95
			  if(64>=0 && port <=95){
              //utopia_val[1]=gbe_val[8];
              val= (( ((utopia_val[1])>>(port-64) ) & 0x000000001)== 1 ? 0 : 1 );  /// 1 the port is not sync in the HW 
              MEA_OS_memcpy(&eventStatus->Status_Event.mea_Utopia ,&val  ,sizeof(eventStatus->Status_Event.mea_Utopia));
			  }else{
				  val= 0; 
				  MEA_OS_memcpy(&eventStatus->Status_Event.mea_Utopia ,&val  ,sizeof(eventStatus->Status_Event.mea_Utopia));
			  }
		}


		
		

		}// utpoia


    return MEA_OK;

}

/*------------------------------------------------------------------*/
/*                                                                  */
/*          <MEA_API_Port_Read_Link_protection_Status>              */
/*                                                                  */
/*------------------------------------------------------------------*/ 

MEA_Status MEA_API_Port_Read_Link_protection_Status(MEA_Unit_t      unit_i)
{

  

    return MEA_OK;
}




MEA_Status MEA_API_XADC_FPGA_Status(MEA_Unit_t      unit_i, MEA_XADC_Info_dbt *entry)
{
   
#if defined(MEA_XADC_NEW)
    int i;
    MEA_Uint32 read_data[5];
    MEA_Uint32 count_shift = 0;
    
    MEA_Uint32 temp[13];
   


    MEA_ind_read_t         ind_read;
#else
    MEA_Uint32 Val[2];
    MEA_Uint32 ttt;

#endif


   
    if(entry == NULL){

        return MEA_ERROR;
    }


    MEA_OS_memset(entry, 0, sizeof(*entry));

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_XADC_POWER;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = (read_data);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(read_data),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

   
    count_shift = 0;
    for (i = 0; i < 12;i++)
    {
        temp[i] = MEA_OS_read_value(count_shift, 12, read_data);
        count_shift += 12;
    }


    entry->temperature[0] = (temp[0] * 504 / 4096) - 273;
    entry->temperature[1] = (temp[1] * 504 / 4096) - 273;
    entry->temperature[2] = (temp[2] * 504 / 4096) - 273;

    /*0 vccint 1 vccaux 2 vbram*/
    entry->volt[0] = (temp[3] * 3 * 1000 )/ 4096;
    entry->volt[1] = (temp[6] * 3 * 1000 )/ 4096;
    entry->volt[2] = (temp[9] * 3 * 1000 )/ 4096;

    entry->vccint_volt[0] = (temp[4] * 3 * 1000 )/ 4096;
    entry->vccint_volt[1] = (temp[5] * 3 * 1000 )/ 4096;

    entry->vccaux_volt[0] = (temp[7] * 3 * 1000 )/ 4096;
    entry->vccaux_volt[1] = (temp[8] * 3 * 1000 )/ 4096;

    entry->vbram_volt[0] = (temp[10] * 3 * 1000 )/ 4096;
    entry->vbram_volt[1] = (temp[11] * 3 * 1000) / 4096;


//     User_temp_alarm[145]
//         Over_temp_alarm[144]
//         Vccint_alarm[146]
//         Vccaux_alarm[147]
//         Vbram_alarm(V)[14
    entry->user_temp_alarm = (MEA_OS_read_value(count_shift, 1, read_data) == 1) ? (MEA_TRUE): MEA_FALSE ;
    count_shift += 1;
    entry->over_temp_alarm = (MEA_OS_read_value(count_shift, 1, read_data) == 1) ? (MEA_TRUE) : MEA_FALSE;
    count_shift += 1;
    entry->vccint_alarm = (MEA_OS_read_value(count_shift, 1, read_data) == 1) ? (MEA_TRUE) : MEA_FALSE;
    count_shift += 1;
    entry->vccaux_alarm = (MEA_OS_read_value(count_shift, 1, read_data) == 1) ? (MEA_TRUE) : MEA_FALSE;
    count_shift += 1;
    entry->vbram_alarm = (MEA_OS_read_value(count_shift, 1, read_data) == 1) ? (MEA_TRUE) : MEA_FALSE;
    count_shift += 1;
    







  return MEA_OK;


}



MEA_Status MEA_API_GW_Get_EVENT(MEA_Unit_t unit, MEA_GW_evetEntry_dbt   *EventEntry)

{
    MEA_Uint32 regs[8];
    MEA_Uint32 access_regs[2];
    MEA_Uint32        count_shift;
    MEA_Uint32 valRead;

    MEA_ind_read_t         ind_read;
 
    if (!MEA_GW_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_GW_SUPPORT is not support on this HW\n", __FUNCTION__);
        return MEA_ERROR;
    }
    
    
    if (EventEntry == NULL){
        return MEA_ERROR;
    }

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_GW_EVENT;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(regs[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }


    
    EventEntry->root_filterId =(MEA_Uint16)((regs[7] >> 24) & 0x1f);
      EventEntry->GTPv1_flag = (MEA_Uint16)((regs[7] >> 16) & 0xff);
//    EventEntry->DA; // = regs[6] and 7;
      EventEntry->DA.b[0] = ((MEA_Uint8)((regs[7] & 0x0000FF00) >> 8));
      EventEntry->DA.b[1] = ((MEA_Uint8)((regs[7] & 0x000000FF) >> 0));
      EventEntry->DA.b[2] = ((MEA_Uint8)((regs[6] & 0xFF000000) >> 24));
      EventEntry->DA.b[3] = ((MEA_Uint8)((regs[6] & 0x00FF0000) >> 16));
      EventEntry->DA.b[4] = ((MEA_Uint8)((regs[6] & 0x0000FF00) >> 8));
      EventEntry->DA.b[5] = ((MEA_Uint8)((regs[6] & 0x000000FF) >> 0));

    EventEntry->GTP_message_type = (MEA_Uint16)((regs[5] >> 24) & 0xff);
    EventEntry->ip_protocol_int = (MEA_Uint16)((regs[5] >> 16) & 0xff);
//    EventEntry->SA;   // = regs[4] and 5;
    EventEntry->SA.b[0] = ((MEA_Uint8)((regs[5] & 0x0000FF00) >> 8));
    EventEntry->SA.b[1] = ((MEA_Uint8)((regs[5] & 0x000000FF) >> 0));
    EventEntry->SA.b[2] = ((MEA_Uint8)((regs[4] & 0xFF000000) >> 24));
    EventEntry->SA.b[3] = ((MEA_Uint8)((regs[4] & 0x00FF0000) >> 16));
    EventEntry->SA.b[4] = ((MEA_Uint8)((regs[4] & 0x0000FF00) >> 8));
    EventEntry->SA.b[5] = ((MEA_Uint8)((regs[4] & 0x000000FF) >> 0));


   


    EventEntry->sgw_teid        = regs[3];
    EventEntry->src_ipv4_int    = regs[2];
    EventEntry->dst_ipv4_int    = regs[1];
    EventEntry->ethertype_int   = (MEA_Uint16) ((regs[0]>>16) & 0xffff);
    EventEntry->vlan_int        = (MEA_Uint16)   (regs[0] & 0xffff);



    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_EVENT_VPLS_FWD_ACCESS_PORT;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(access_regs[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(access_regs),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }


    count_shift = 0;

    valRead = MEA_OS_read_value(count_shift,
        10,
        &access_regs[0]);
    count_shift += 10;

        EventEntry->event_vpls_access.st_cntx_ac_port = valRead;

        valRead = MEA_OS_read_value(count_shift,
            10,
            &access_regs[0]);
        count_shift += 10;
        EventEntry->event_vpls_access.lrn_ac_port = valRead;

        valRead = MEA_OS_read_value(count_shift,
            10,
            &access_regs[0]);
        count_shift += 10;
        EventEntry->event_vpls_access.fwd_ac_port = valRead;

        valRead = MEA_OS_read_value(count_shift,
            3,
            &access_regs[0]);
        count_shift += 3;
        EventEntry->event_vpls_access.info = valRead;
        
        
        






    return MEA_OK;


}



/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_API_Get_logDescInfo(MEA_Unit_t unit, MEA_Uint32 index, MEA_logDesEntry_dbt   *logEntry)
{
    MEA_ind_read_t ind_read;
    MEA_Uint32 read_data[5];
    MEA_Uint32 count_shift;
    //MEA_Uint32 val;
 

    ind_read.tableType = ENET_BM_TBL_TYP_LOG_DESC_AXI;
    ind_read.tableOffset = index;
    
   

    ind_read.cmdReg = MEA_BM_IA_CMD;
    ind_read.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_read.statusReg = MEA_BM_IA_STAT;
    ind_read.statusMask = MEA_BM_IA_STAT_MASK;
    ind_read.tableAddrReg = MEA_BM_IA_ADDR;
    ind_read.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_read.read_data = read_data;


    if (MEA_API_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(read_data),
        MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ReadIndirect from tableType %d / tableOffset %d module BM failed "
            "(index=%d)\n",
            __FUNCTION__, ind_read.tableType, ind_read.tableOffset, index);
        return MEA_ERROR;
    }



    count_shift = 0;

    logEntry->Des_write_NEXT_POINTER_BANK = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_write_NEXT_POINTER_BANK, read_data);
    count_shift += MEA_TBL_LOG_Des_write_NEXT_POINTER_BANK;
    logEntry->Des_write_NEXT_POINTER = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_write_NEXT_POINTER, read_data);
    
    count_shift += MEA_TBL_LOG_Des_write_NEXT_POINTER;
    logEntry->Des_write_Trigger = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_write_Trigger, read_data);
    count_shift += MEA_TBL_LOG_Des_write_Trigger;

    logEntry->Des_write_AXI_BANK_ADDR = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_write_AXI_BANK_ADDR, read_data);
    count_shift += MEA_TBL_LOG_Des_write_AXI_BANK_ADDR;

    logEntry->Des_write_AXI_ADDRESS = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_write_AXI_ADDRESS, read_data);
    count_shift += MEA_TBL_LOG_Des_write_AXI_ADDRESS;

    logEntry->Des_write_AXI_HoldTime = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_write_AXI_HoldTime, read_data);
    count_shift += MEA_TBL_LOG_Des_write_AXI_HoldTime;
    logEntry->Des_write_AXI_Packet_length  = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_write_AXI_Packet_length, read_data);
    count_shift += MEA_TBL_LOG_Des_write_AXI_Packet_length;

    logEntry->Des_write_AXI_TimeStamp = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_write_AXI_TimeStamp, read_data);
    count_shift += MEA_TBL_LOG_Des_write_AXI_TimeStamp;

    logEntry->Des_read_NEXT_POINTER_BANK = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_read_NEXT_POINTER_BANK, read_data);
    count_shift += MEA_TBL_LOG_Des_read_NEXT_POINTER_BANK;

    logEntry->Des_read_NEXT_POINTER = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_read_NEXT_POINTER, read_data);
    count_shift += MEA_TBL_LOG_Des_read_NEXT_POINTER;
    logEntry->Des_read_Trigger = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_read_Trigger, read_data);
    count_shift += MEA_TBL_LOG_Des_read_Trigger;

    logEntry->Des_read_AXI_BANK_ADDR = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_read_AXI_BANK_ADDR, read_data);
    count_shift += MEA_TBL_LOG_Des_read_AXI_BANK_ADDR;

    logEntry->Des_read_AXI_ADDRESS = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_read_AXI_ADDRESS, read_data);
    count_shift += MEA_TBL_LOG_Des_read_AXI_ADDRESS;
    logEntry->Des_read_AXI_HoldTime = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_read_AXI_HoldTime, read_data);
    count_shift += MEA_TBL_LOG_Des_read_AXI_HoldTime;
    
    logEntry->Des_read_AXI_Packet_length = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_read_AXI_Packet_length, read_data);
    count_shift += MEA_TBL_LOG_Des_read_AXI_Packet_length;

    logEntry->Des_read_AXI_TimeStamp = MEA_OS_read_value(count_shift, MEA_TBL_LOG_Des_read_AXI_TimeStamp, read_data);
    count_shift += MEA_TBL_LOG_Des_read_AXI_TimeStamp;







    return MEA_OK;

}

static void mea_logDescInfo_HwEntry(MEA_funcParam  arg1,
                                                MEA_funcParam arg2,
                                                MEA_funcParam arg3,
                                                MEA_funcParam arg4)

{
    //     MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
    //     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
    //     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
    //MEA_MacAddr             *MAC = (MEA_MacAddr*)arg4;

    //    MEA_Uint32      count_shift;
    MEA_Uint32 i;
    MEA_Uint32 val[5];

    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }



    /* write to hardware */
    MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA0, val[0], MEA_MODULE_BM);
    MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA1, val[1], MEA_MODULE_BM);
    MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA2, val[2], MEA_MODULE_BM);
    MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA3, val[3], MEA_MODULE_BM);
    MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA4, val[4], MEA_MODULE_BM);
   
   



}

MEA_Status MEA_API_Clear_logDescInfo(MEA_Unit_t unit, MEA_Uint32 index)
{

    MEA_ind_write_t ind_write;




    /************************************************************************/
    /*                                                                      */
    /************************************************************************/

    /* set the ind_write parameters */
    ind_write.tableType = ENET_BM_TBL_TYP_LOG_DESC_AXI;
    ind_write.tableOffset = index;
    ind_write.cmdReg = MEA_BM_IA_CMD;
    ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_write.statusReg = MEA_BM_IA_STAT;
    ind_write.statusMask = MEA_BM_IA_STAT_MASK;
    ind_write.tableAddrReg = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    //     ind_write.funcParam2 = (MEA_Uint32)hwUnit;
    //     ind_write.funcParam3 = (MEA_Uint32)0/*hwId*/;
    //ind_write.funcParam4 = (MEA_funcParam)(&new_entry);
    ind_write.writeEntry = (MEA_FUNCPTR)mea_logDescInfo_HwEntry;

    /* Write To HW Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) == MEA_ERROR) {
        return MEA_ERROR;
    }



    return MEA_OK;
}



MEA_Status MEA_API_collectDesAXI_logDescInfo(MEA_Unit_t unit)
{
#if    defined(MEA_ETHERNITY) && defined(MEA_ETHERNITY_DUBUG)
    MEA_Uint32 index,i,x,y;
    MEA_logDesEntry_dbt   logEntry;
    MEA_debugLogerAxiEntry_dbt temp;


    
    /*clear the Array */
    MEA_OS_memset(&MEA_loggerAXI_Global_read_write[0], 0, sizeof(MEA_loggerAXI_Global_read_write));
    
    

    /* read from HW */
    for (index = 0; index < ENET_BM_TBL_LOG_DESC_AXI_LENGTH; index++)
    {
        i = index * 2;
    MEA_API_Get_logDescInfo(unit, index, &logEntry);
   
    MEA_loggerAXI_Global_read_write[i].NEXT_POINTER_BANK = logEntry.Des_write_NEXT_POINTER_BANK;
    MEA_loggerAXI_Global_read_write[i].NEXT_POINTER      = logEntry.Des_write_NEXT_POINTER;
    MEA_loggerAXI_Global_read_write[i].Trigger           = logEntry.Des_write_Trigger;
    
    MEA_loggerAXI_Global_read_write[i].AXI_BANK_ADDR     = logEntry.Des_write_AXI_BANK_ADDR;
    MEA_loggerAXI_Global_read_write[i].AXI_ADDRESS       = logEntry.Des_write_AXI_ADDRESS;
    MEA_loggerAXI_Global_read_write[i].AXI_HoldTime      = logEntry.Des_write_AXI_HoldTime;
    MEA_loggerAXI_Global_read_write[i].Packet_length     = logEntry.Des_write_AXI_Packet_length;
    MEA_loggerAXI_Global_read_write[i].AXI_TimeStamp     = logEntry.Des_write_AXI_TimeStamp;
    
    MEA_loggerAXI_Global_read_write[i].type  = 0;
    MEA_loggerAXI_Global_read_write[i].index = index;

    MEA_loggerAXI_Global_read_write[i+1].NEXT_POINTER_BANK    = logEntry.Des_read_NEXT_POINTER_BANK;
    MEA_loggerAXI_Global_read_write[i+1].NEXT_POINTER         = logEntry.Des_read_NEXT_POINTER;
    MEA_loggerAXI_Global_read_write[i+1].Trigger              = logEntry.Des_read_Trigger;

    MEA_loggerAXI_Global_read_write[i+1].AXI_BANK_ADDR        = logEntry.Des_read_AXI_BANK_ADDR;
    MEA_loggerAXI_Global_read_write[i+1].AXI_ADDRESS          = logEntry.Des_read_AXI_ADDRESS;
    MEA_loggerAXI_Global_read_write[i+1].AXI_HoldTime         = logEntry.Des_read_AXI_HoldTime;
    MEA_loggerAXI_Global_read_write[i + 1].Packet_length      = logEntry.Des_read_AXI_Packet_length;
    MEA_loggerAXI_Global_read_write[i+1].AXI_TimeStamp        = logEntry.Des_read_AXI_TimeStamp;
    MEA_loggerAXI_Global_read_write[i+1].type = 1;
    MEA_loggerAXI_Global_read_write[i+1].index = index;
    }

//  //for debug
//     for (index = 0; index < ENET_BM_TBL_LOG_DESC_AXI_LENGTH; index++){
//         MEA_loggerAXI_Global_read_write[index].AXI_TimeStamp = 100 - index;
//     }



        /*make the sort of timest*/

    for ( x = 0; x < ENET_BM_TBL_LOG_DESC_AXI_LENGTH*2-1; x++)

    {

        for ( y = 0; y<ENET_BM_TBL_LOG_DESC_AXI_LENGTH*2-1 ; y++)

        {

            if (MEA_loggerAXI_Global_read_write[y].AXI_TimeStamp > MEA_loggerAXI_Global_read_write[y + 1].AXI_TimeStamp)

            {
                MEA_OS_memset(&temp, 0, sizeof(MEA_debugLogerAxiEntry_dbt));
                //int temp = array[y + 1];
                MEA_OS_memcpy(&temp, &MEA_loggerAXI_Global_read_write[y + 1], sizeof(MEA_debugLogerAxiEntry_dbt));


                //array[y + 1] = array[y];
                MEA_OS_memcpy(&MEA_loggerAXI_Global_read_write[y + 1], &MEA_loggerAXI_Global_read_write[y ],sizeof(MEA_debugLogerAxiEntry_dbt));
                //array[y] = temp;
                MEA_OS_memcpy(&MEA_loggerAXI_Global_read_write[y ], &temp,  sizeof(MEA_debugLogerAxiEntry_dbt));


            }

        }

    }



    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "The array is Bubble sort\n");
#else
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " %s the API used for debug only \n", __FUNCTION__);
#endif

        return MEA_OK;
}


MEA_Status MEA_API_Get_loggerAXI_DescInfo(MEA_Unit_t unit, MEA_Uint32 index, MEA_debugLogerAxiEntry_dbt   *logEntry)
{

#if    defined(MEA_ETHERNITY) && defined(MEA_ETHERNITY_DUBUG)
    MEA_OS_memcpy(logEntry, &MEA_loggerAXI_Global_read_write[index], sizeof(MEA_debugLogerAxiEntry_dbt));
#else
    return MEA_ERROR;
#endif
    return MEA_OK;
}



