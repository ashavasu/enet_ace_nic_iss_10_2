/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"

#include "MEA_platform_types.h"
#include "ENET_platform_types.h"
#include "mea_globals_drv.h"
#include "mea_limiter_drv.h"
#include "mea_if_regs.h"
#include "mea_drv_common.h"
#include "enet_queue_drv.h"


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MAX_NUM_OF_LIMITER_PROFILE MEA_Platform_GetMaxNumOfLimiters()

#define MAX_LIMITER_VALUE 65535



#define MEA_ADM_SW_SUPPORT            1
#define MEA_ADM_CLUSTERS_IN_GROUP     2   /* 2 X 32 64bit*/

#define MEA_ADM_TOTAL_GROUP_CLUSTER   MEA_OS_MAX( 2 , ((MEA_Platform_Get_MaxNumOfClusters() ) / (MEA_DRV_XPER_NUM_CLUSTER_GROUP_val  )) )
//#define MEA_ADM_TOTAL_GROUP_CLUSTER 2





/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct{
	MEA_Uint32            numberOfOwners;	
	MEA_Uint32            numberOfStaticEntries;	
}mea_limiter_Private_dbt;

typedef struct{
	MEA_Limiter_Entry_dbt  public_dbt;
	mea_limiter_Private_dbt private_dbt;
}mea_limiter_dbt;



typedef struct {


    MEA_Uint32  cluster     [MEA_ADM_CLUSTERS_IN_GROUP];  /*64 bit*/
    MEA_Uint32  trunkcluster[MEA_ADM_CLUSTERS_IN_GROUP]; /*64 bit*/


}MEA_ADM_Cluster_Group_dbt;

typedef struct {
    MEA_Bool    valid;
    MEA_Uint16  numofCluster;
    MEA_Uint8   state;   /* 0 - regular 1- TrunkPort */
    MEA_Port_t  trunkPort;

    //MEA_ADM_Cluster_Group_dbt Groups[MEA_ADM_TOTAL_GROUP_CLUSTER];
    MEA_ADM_Cluster_Group_dbt *Groups;


}MEA_ADM_entry_dbt;




typedef struct {
    MEA_Bool   valid;
    MEA_ISOLATE_dbt data;
    MEA_Uint32 num_of_owners;

    struct
    {
        MEA_ISOLATE_dbt internal_Queue; /*internal */
    }privet_data;



}MEA_ISOLATE_Entry_dbt;



static MEA_ISOLATE_Entry_dbt           *MEA_ISOLATE_info_Table = NULL;




/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static ENET_GroupId_t mea_limiter_group_id[1];
static MEA_Bool       mea_limiter_InitDone[1];

MEA_ADM_entry_dbt    *MEA_ADM_Table = NULL;
/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
MEA_Bool isAlowedToDeleteLimiter(MEA_Unit_t     unit,MEA_Limiter_t  limiterId);
static MEA_Status mea_limiter_UpdateHwLimiter(MEA_Limiter_t              limitId,
											  mea_limiter_dbt            *entry,
                                              mea_limiter_dbt            *old_entry);



/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
MEA_Status mea_drv_limiter_clear_counters(MEA_Unit_t unit)
{
	mea_limiter_dbt groupEntry;
	ENET_Uint32     len;
	MEA_Limiter_t   id;
	MEA_Bool found = MEA_FALSE;

    if(!MEA_GLOBAL_LIMITER_SUPPORT){
        return MEA_OK;     
    }

	if (MEA_API_GetFirst_Limiter (unit,
 		  				 	      &id,
                                  &found) != MEA_OK) 
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - MEA_API_GetFirst_Limiter failed \n",
			     		  __FUNCTION__);
        return MEA_ERROR;
	}


	if (!found){
		return MEA_OK;
	}

   while (found) 
   {
 		len = sizeof(mea_limiter_dbt);
		if (ENET_Get_GroupElement(unit,
								  mea_limiter_group_id[unit],
								  id,
								  &groupEntry,
								  &len) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - ENET_Get_GroupElement failed (limiter id=%d)\n",
							   __FUNCTION__,id);
			return MEA_ERROR;
		}

		if (mea_limiter_UpdateHwLimiter(id,
										&groupEntry,
										NULL)!=MEA_OK){
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - mea_limiter_UpdateHwLimiter failed\n",
							 __FUNCTION__);
		   return MEA_ERROR; 
		}		


       if (MEA_API_GetNext_Limiter  (MEA_UNIT_0,
	 	  	 					      &id,
                                      &found) != ENET_OK) {
	       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                      "%s - MEA_API_GetNext_Limiter failed \n",
			    			  __FUNCTION__);
	       return MEA_ERROR;
	   }	
   }

   return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Create_Limiter                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_Limiter (MEA_Unit_t                           unit,
								   MEA_Limiter_Entry_dbt               *data,
                                   MEA_Limiter_t                       *io_limiterId)
{
	mea_limiter_dbt groupEntry;

	MEA_API_LOG

	MEA_OS_memset(&(groupEntry.private_dbt),0,sizeof(groupEntry.private_dbt));
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	if (data==NULL){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	MEA_OS_memcpy(&(groupEntry.public_dbt),data,sizeof(groupEntry.public_dbt));
	if(io_limiterId == NULL){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - io_limiterId == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (data->action_type==MEA_LIMIT_ACTION_TO_ACTION){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_LIMIT_ACTION_TO_ACTION is not supported in this version\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (data->action_type>=MEA_LIMIT_ACTION_LAST){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - invalid action\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if(data->limit > MAX_LIMITER_VALUE){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - limit out of supported range\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if ((*io_limiterId != MEA_LIMITER_DONT_CARE) &&
        (!MEA_GLOBAL_LIMITER_SUPPORT)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Device is not support limiter \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

	groupEntry.private_dbt.numberOfOwners = 1;
	groupEntry.private_dbt.numberOfStaticEntries = 0;
	if (*io_limiterId == MEA_PLAT_GENERATE_NEW_ID)
	  *io_limiterId=ENET_PLAT_GENERATE_NEW_ID;
    else {
        if (MEA_API_IsLimiterIdInRange(unit,*io_limiterId) == MEA_FALSE) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_IsLimiterIdInRange failed \n",__FUNCTION__);
         return MEA_ERROR;
        } 

    }
	if (ENET_Create_GroupElement((ENET_Unit_t)unit,
		                         mea_limiter_group_id[unit],
		                         &groupEntry,
								 sizeof(groupEntry),
								 io_limiterId)!=ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - can not set Limiter element %d\n",
				 __FUNCTION__,*io_limiterId);
		return MEA_ERROR;																			
	}
	/* update HW */
	if (mea_limiter_UpdateHwLimiter(*io_limiterId,
									&groupEntry,
                                    NULL)!=MEA_OK){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_limiter_UpdateHwLimiter failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Set_Limiter                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_Limiter    (MEA_Unit_t                          unit,
								   MEA_Limiter_Entry_dbt               *data,
                                   MEA_Limiter_t                       i_limiterId)
{
	mea_limiter_dbt groupEntry,oldGroupEntry;
	MEA_Bool exist;
	ENET_Uint32 len;

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    if(!MEA_GLOBAL_LIMITER_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - limiter not support on this version\n",
            __FUNCTION__);
        return MEA_ERROR;     
    }

	if (data==NULL){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}

	if(MEA_API_IsExist_Limiter_ById(unit,i_limiterId,&exist)!=MEA_OK){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_IsExist_Limiter_ById failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (!exist){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -  limiterId does not exist\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (data->action_type==MEA_LIMIT_ACTION_TO_ACTION){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_LIMIT_ACTION_TO_ACTION is not supported in this version\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (data->action_type>=MEA_LIMIT_ACTION_LAST){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - invalid action\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if(data->limit > MAX_LIMITER_VALUE){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - limit out of supported range\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
    if (MEA_API_IsLimiterIdInRange(unit,i_limiterId) == MEA_FALSE) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_IsLimiterIdInRange failed id  %d \n",
                         __FUNCTION__,
                         i_limiterId);
         return MEA_ERROR;
        } 
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	len = sizeof(groupEntry);
	if(ENET_Get_GroupElement((ENET_Unit_t)unit,
						     mea_limiter_group_id[unit],
							 i_limiterId,
							 &groupEntry,
							 &len)!=ENET_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - ENET_Get_GroupElement failed for Limiter %d\n",
                         __FUNCTION__,i_limiterId);
       return MEA_ERROR; 
	}
	MEA_OS_memcpy(&oldGroupEntry,&groupEntry,sizeof(oldGroupEntry));
	MEA_OS_memcpy(&(groupEntry.public_dbt),data,sizeof(groupEntry.public_dbt));
	if (ENET_Set_GroupElement((ENET_Unit_t)unit,
		                         mea_limiter_group_id[unit],
								 i_limiterId,
		                         &groupEntry,
								 sizeof(groupEntry))!=ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - can not set Limiter element %d\n",
				 __FUNCTION__,i_limiterId);
		return MEA_ERROR;																			
	}
	/* update HW */
	if (mea_limiter_UpdateHwLimiter(i_limiterId,
									&groupEntry,
                                    &oldGroupEntry)!=MEA_OK){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_limiter_UpdateHwLimiter failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	return MEA_OK;
}
                                
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Get_Limiter                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                              
MEA_Status  MEA_API_Get_Limiter    (MEA_Unit_t                      unit,
                                    MEA_Limiter_t                   limiterId,
									MEA_Limiter_Entry_dbt           *data_o)
{
	mea_limiter_dbt groupEntry;
	MEA_Bool found = MEA_FALSE;
	ENET_Uint32 len;

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	if (data_o==NULL){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (MEA_API_IsExist_Limiter_ById(unit,limiterId,&found)!=MEA_OK){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - error calling MEA_API_IsExist_Limiter_ById\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (found == MEA_FALSE){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - limiterId (%d) doesnt exist\n",
                         __FUNCTION__,limiterId);
       return MEA_ERROR; 
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
	len = sizeof(mea_limiter_dbt);
	if (ENET_Get_GroupElement(unit,
		                      mea_limiter_group_id[unit],
							  limiterId,
		                      &groupEntry,
							  &len) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_Get_GroupElement failed (limiter id=%d)\n",
						   __FUNCTION__,limiterId);
		return MEA_ERROR;
	}
	MEA_OS_memcpy(data_o,(void *)&(groupEntry.public_dbt),sizeof(*data_o));
    return MEA_OK;
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Get_Limiter_Status                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                              
MEA_Status  MEA_API_Get_Limiter_Status    (MEA_Unit_t              unit,
                                    MEA_Limiter_t                  limiterId,
									MEA_Limiter_status             *data_o)
{

	mea_limiter_dbt groupEntry;
	MEA_Bool found = MEA_FALSE;
	ENET_Uint32 len;
	MEA_ind_read_t         ind_read;
	MEA_Uint32     read_val[2];

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	if (data_o==NULL){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (MEA_API_IsExist_Limiter_ById(unit,limiterId,&found)!=MEA_OK){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - error calling MEA_API_IsExist_Limiter_ById\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (found == MEA_FALSE){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - limiterId (%d) doesnt exist\n",
                         __FUNCTION__,limiterId);
       return MEA_ERROR; 
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
	/* get static entries number */
	len = sizeof(mea_limiter_dbt);
	if (ENET_Get_GroupElement(unit,
		                      mea_limiter_group_id[unit],
							  limiterId,
		                      &groupEntry,
							  &len) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_Get_GroupElement failed (limiter id=%d)\n",
						   __FUNCTION__,limiterId);
		return MEA_ERROR;
	}


	/* get used entries number */
	ind_read.tableType		= ENET_IF_CMD_LIMITER;
	ind_read.tableOffset	= limiterId;
	ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
	ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_read.statusReg		= MEA_IF_STATUS_REG;   
	ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
	ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_read.read_data      = read_val;
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
                             MEA_NUM_OF_ELEMENTS(read_val),
                             MEA_MODULE_IF,
							 MEA_MODE_UPSTREEM_ONLY,
							 MEA_MEMORY_READ_SUM) !=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_LowLevel_ReadIndirect tbl=%d mdl=%d failed (limiterId=%d)\n",
                          __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,limiterId);
		return MEA_ERROR;
    }
    data_o->number_of_used_entries = 0;
	data_o->number_of_used_entries =(read_val[0] &MEA_IF_LIMITER_CURRENT_LIMIT_MASK1);
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Delete_Limiter                             */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
MEA_Status  MEA_API_Delete_Limiter (MEA_Unit_t                      unit,
                                    MEA_Limiter_t                   limiterId)
{
	mea_limiter_dbt groupEntry;
	MEA_Limiter_status stat;
	ENET_Uint32 len;
	MEA_Bool found = MEA_FALSE;
	mea_limiter_dbt       internalData;

	MEA_API_LOG

 #ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(!MEA_GLOBAL_LIMITER_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Limiter is not support this version\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
	if (MEA_API_IsExist_Limiter_ById(unit,limiterId,&found)!=MEA_OK){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - error calling MEA_API_IsExist_Limiter_ById\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}
	if (found == MEA_FALSE){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - limiterId (%d) doesnt exist\n",
                         __FUNCTION__,limiterId);
       return MEA_ERROR; 
	}
	if (!isAlowedToDeleteLimiter(unit,limiterId)){
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - limiterId (%d) not allow to delete\n",
                         __FUNCTION__,limiterId);
       return MEA_ERROR; 
	}
	if ((limiterId == MEA_LIMITER_DONT_CARE)&&
		(mea_limiter_InitDone[unit] != MEA_FALSE))
	{
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                   "%s - can not delete static limiter (%d) \n",
                   __FUNCTION__,limiterId);

		return MEA_ERROR;
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
	len = sizeof(mea_limiter_dbt);
	if (ENET_Get_GroupElement(unit,
		                      mea_limiter_group_id[unit],
							  limiterId,
		                      &groupEntry,
							  &len) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_Get_GroupElement failed (limiter id=%d)\n",
						   __FUNCTION__,limiterId);
		return MEA_ERROR;
	}
	if(groupEntry.private_dbt.numberOfOwners>1){
		groupEntry.private_dbt.numberOfOwners=groupEntry.private_dbt.numberOfOwners-1;
		if (ENET_Set_GroupElement(unit,
								  mea_limiter_group_id[unit],
								  limiterId,
								  &groupEntry,
								  len) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - ENET_Set_GroupElement failed (limiter id=%d)\n",
							   __FUNCTION__,limiterId);
			return MEA_ERROR;
		}
	}else{ 
		if (MEA_API_Get_Limiter_Status(unit,
									   limiterId,
									   &stat)!=MEA_OK){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - MEA_API_Get_Limiter_Status failed\n",
							   __FUNCTION__);
			return MEA_ERROR;
		}

		if ((stat.number_of_used_entries!=0) && 
		    (mea_limiter_InitDone[unit] != MEA_FALSE)){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - Limiter has  entries (num of  entries =%d )\n",
							   __FUNCTION__,stat.number_of_used_entries);
			return MEA_ERROR;
		}
		/* delete from HW */
		MEA_OS_memset(&internalData,0,sizeof(internalData));
		if (mea_limiter_UpdateHwLimiter(limiterId,
									&internalData,
                                    NULL)!=MEA_OK){
	         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_limiter_UpdateHwLimiter failed\n",
                         __FUNCTION__);
             return MEA_ERROR; 
	    }
		/* delete from SW */
		if (ENET_Delete_GroupElement(unit,
								  mea_limiter_group_id[unit],
								  limiterId) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - ENET_Delete_GroupElement failed (limiter id=%d)\n",
							   __FUNCTION__,limiterId);
			return MEA_ERROR;
		}
	}
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_GetFirst_Limiter                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
MEA_Status MEA_API_GetFirst_Limiter (MEA_Unit_t     unit,
                                    MEA_Limiter_t   *o_limiterId,
									MEA_Bool        *o_found)
{
	mea_limiter_dbt groupEntry;
	ENET_Uint32 len;

	MEA_API_LOG
    if(!MEA_GLOBAL_LIMITER_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Limiter is not support this version\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

	len=sizeof(groupEntry);
	if ( ENET_GetFirst_GroupElement   (unit,
	                                   mea_limiter_group_id[unit],
									   o_limiterId,
                                       &groupEntry,
									   &len,
                                       o_found) !=ENET_OK)	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_GetFirst_GroupElement failed \n",
						   __FUNCTION__);
		return MEA_ERROR;
	}
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_GetNext_Limiter                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
MEA_Status MEA_API_GetNext_Limiter  (MEA_Unit_t     unit,
                                     MEA_Limiter_t *o_limiterId , 
                                     MEA_Bool      *o_found)
{
	mea_limiter_dbt groupEntry;
	ENET_Uint32 len;

	MEA_API_LOG
    if(!MEA_GLOBAL_LIMITER_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Limiter is not support this version\n",
            __FUNCTION__);
        return MEA_ERROR;
    }


	len=sizeof(groupEntry);
    if (  ENET_GetNext_GroupElement   (unit,
	                                   mea_limiter_group_id[unit],
									   o_limiterId,
                                       &groupEntry,
									   &len,
                                       o_found)!=ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_GetNext_GroupElement failed \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_IsExist_Limiter_ById                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
MEA_Status MEA_API_IsExist_Limiter_ById  (MEA_Unit_t     unit,
                                          MEA_Limiter_t  limiterId, 
                                          MEA_Bool      *exist)
{

	MEA_API_LOG
 
	*exist=MEA_TRUE;
	if (ENET_IsValid_GroupElement(unit,
		                          mea_limiter_group_id[unit],
								  limiterId,
								  ENET_TRUE) != ENET_TRUE) {


  	  *exist=MEA_FALSE;  
    }
    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_IsLimiterIdInRange                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/ 
MEA_Bool MEA_API_IsLimiterIdInRange(MEA_Unit_t     unit, 
                                    MEA_Limiter_t  limiterId)
{
	MEA_Bool retVal = MEA_FALSE;

	if (limiterId<MAX_NUM_OF_LIMITER_PROFILE)
		retVal = MEA_TRUE;

	return (retVal);
}


/********************* internal function implementation **********************/

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <mea_Init_limiter>                                           */
/*                                                                            */
/*----------------------------------------------------------------------------*/
MEA_Status mea_Init_limiter(MEA_Unit_t unit_i)
{

	ENET_Group_dbt group;
	MEA_Limiter_t limiterId;
    static MEA_Bool first = MEA_TRUE;
	MEA_Limiter_Entry_dbt data;
	mea_limiter_dbt       internalData;
	
    
    if(b_bist_test){
        return MEA_OK;
    }
    /* Init limiter Table */
   


    
    if(!MEA_GLOBAL_LIMITER_SUPPORT)
        return MEA_OK;
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Limiter table ...\n");

    if (first) {
       MEA_Uint32 i;
       for (i=0;i<MEA_NUM_OF_ELEMENTS(mea_limiter_group_id);i++) {
          mea_limiter_group_id[unit_i] = ENET_PLAT_GENERATE_NEW_ID;
          mea_limiter_InitDone[unit_i] = MEA_FALSE;
       } 
       first = MEA_FALSE;
    }

    if (mea_limiter_InitDone[unit_i] != MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - unit %d is already init\n",
                          __FUNCTION__,unit_i);
       return MEA_ERROR;
    }

	mea_limiter_InitDone[unit_i] = MEA_FALSE;

    /* Create the limiter group  */
	MEA_OS_memset(&group,0,sizeof(group));
	MEA_OS_sprintf(group.name,"limiter group");
	group.max_number_of_elements = MAX_NUM_OF_LIMITER_PROFILE;
	mea_limiter_group_id[unit_i] = ENET_PLAT_GENERATE_NEW_ID;
    if (ENET_Create_Group(unit_i,
		                  &group,
						  &(mea_limiter_group_id[unit_i])) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - ENET_Create_Group failed\n",
						  __FUNCTION__);
	   return MEA_ERROR;
	}

    /* set default to Device */
	MEA_OS_memset(&internalData,0,sizeof(internalData));
	for (limiterId=0;
		 limiterId<MAX_NUM_OF_LIMITER_PROFILE;
		 limiterId++) {
		if (mea_limiter_UpdateHwLimiter(limiterId,
									&internalData,
                                    NULL)!=MEA_OK){
	         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_limiter_UpdateHwLimiter failed\n",
                         __FUNCTION__);
             return MEA_ERROR; 
	    }
	}
    mea_limiter_InitDone[unit_i] = MEA_TRUE;
	/* create static "D.C" entry */
	data.action_type = MEA_LIMIT_ACTION_DISABLE;
	data.limit = 0;
	limiterId =MEA_LIMITER_DONT_CARE;
	if (MEA_API_Create_Limiter (unit_i,
								&data,
                                &limiterId)== MEA_ERROR) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - can not create static (D.C) limiter \n",
						   __FUNCTION__);
       return MEA_ERROR;
    }
    return MEA_OK; 
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <mea_ReInit_limiter>                                           */
/*                                                                            */
/*----------------------------------------------------------------------------*/
MEA_Status mea_ReInit_limiter(MEA_Unit_t unit_i)
{


	mea_limiter_dbt limiter_entry;
	MEA_Uint32      limiter_entry_len=sizeof(limiter_entry);
	MEA_Limiter_t   limiterId;
    MEA_Bool        found;

	if(!MEA_GLOBAL_LIMITER_SUPPORT)
		return MEA_OK;


	if ( ENET_GetFirst_GroupElement(unit_i,
	                                mea_limiter_group_id[unit_i],
									&limiterId,
                                    &limiter_entry,
									&limiter_entry_len,
                                    &found) !=ENET_OK)	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_GetFirst_GroupElement failed \n",
						   __FUNCTION__);
		return MEA_ERROR;
	}
    while (found) {

		if (mea_limiter_UpdateHwLimiter(limiterId,
									    &limiter_entry,
                                        NULL)!=MEA_OK){
	         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_limiter_UpdateHwLimiter failed\n",
                         __FUNCTION__);
             return MEA_ERROR; 
	    }

        if ( ENET_GetNext_GroupElement(unit_i,
	                                   mea_limiter_group_id[unit_i],
									   &limiterId,
                                       &limiter_entry,
									   &limiter_entry_len,
                                       &found) !=ENET_OK)	{
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                   "%s - ENET_GetNext_GroupElement failed \n",
				    		   __FUNCTION__);
		    return MEA_ERROR;
	    }
    }


    return MEA_OK; 
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <mea_Conclude_limiter>                                       */
/*                                                                            */
/*----------------------------------------------------------------------------*/
MEA_Status mea_Conclude_limiter(MEA_Unit_t unit_i)
{

	MEA_Limiter_t  limiterId;
	MEA_Limiter_t  next_limiterId;
	MEA_Bool       found;
	
    if(!MEA_GLOBAL_LIMITER_SUPPORT){
        return MEA_OK;
    }

	if (mea_limiter_InitDone[unit_i] == MEA_FALSE) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - Conclude call without Init before \n",
						   __FUNCTION__);
       return MEA_OK;
    }

	/* Get the first limiter */
	if (MEA_API_GetFirst_Limiter  (unit_i,
		                       &limiterId,
							   &found) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - MEA_API_GetFirst_Limiter failed \n",
						   __FUNCTION__);
		return MEA_ERROR;
	}
	/* Indicate InitDone to false for this unit so static entries will also be seleter */
    mea_limiter_InitDone[unit_i] = MEA_FALSE;

	/* scan all limiter and delete one by one */
	while(found) {

		next_limiterId = limiterId;
        if (MEA_API_Delete_Limiter    (unit_i,
		                               limiterId) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - MEA_API_Delete_Limiter failed \n",
		 		  		       __FUNCTION__);
		    return MEA_ERROR;
		}
        if (MEA_API_GetNext_Limiter   (unit_i,
		                                &next_limiterId,
							            &found) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - MEA_API_GetNext_Limiter failed \n",
		 		  		       __FUNCTION__);
		    return MEA_ERROR;
		}
		limiterId = next_limiterId;
	}



    /* Delete the limiter Group  */
	if (ENET_Delete_Group(unit_i,mea_limiter_group_id[unit_i],MEA_TRUE) != ENET_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	 	                   "%s - ENET_Delete_Group failed \n",
	 		  		       __FUNCTION__);
	    return MEA_ERROR;
	}
    mea_limiter_group_id[unit_i] = ENET_PLAT_GENERATE_NEW_ID;


    /* Return to caller */
    return MEA_OK; 

}

MEA_Bool isAlowedToDeleteLimiter(MEA_Unit_t     unit,MEA_Limiter_t  limiterId)
{
    MEA_Service_t db_serviceId;
    MEA_ULong_t    db_cookie;
    MEA_Uint32    countEntry=0;
	MEA_Service_Entry_Data_dbt service_data;
	MEA_Limiter_status stat;
 
    if (MEA_API_GetFirst_Service(unit,&db_serviceId,&db_cookie) != MEA_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_GetServiceFirst failed\n",
                          __FUNCTION__);
		return MEA_FALSE; 
    }

    if (db_cookie == 0) {
        /*Service table is empty*/
        return MEA_TRUE;
    }

    while (db_cookie != 0) { 

		if(MEA_API_Get_Service(unit,db_serviceId,NULL,&service_data,NULL,NULL,NULL)!=MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_Service(%d) Failed\n",
                              __FUNCTION__,db_serviceId);
			return MEA_TRUE;
		}
		if ( (service_data.limiter_enable) &&
			 (service_data.limiter_id == limiterId))
		{
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - limiterId (%d) is used by service\n",__FUNCTION__,limiterId);
			return MEA_FALSE;
		}

        if (MEA_API_GetNext_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetNext_Service failed (db_serviceId=%d)\n",
                             __FUNCTION__,db_serviceId);
           return MEA_TRUE;
        }
		countEntry++;
		
    }

	/* check that there are no mac's on this limiter */
	
    if (MEA_API_Get_Limiter_Status(MEA_UNIT_0,limiterId,&stat) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Get_Limiter_Status failed (limiter=%d)\n",
                         __FUNCTION__,limiterId);
       return MEA_TRUE;
    }
	if (stat.number_of_used_entries !=0)
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - limiter=%d has DSE entries , can not be deleted\n",
                         __FUNCTION__,limiterId);
       return MEA_FALSE;
    }


	return MEA_TRUE;

}




MEA_Status mea_Limiter_static_entry(MEA_Unit_t unit_i,MEA_Limiter_t  limiterId,MEA_Bool isAdd)
{

	mea_limiter_dbt        groupEntry;
	ENET_Uint32 len;

	len = sizeof(mea_limiter_dbt);
	if (ENET_Get_GroupElement(unit_i,
		                      mea_limiter_group_id[unit_i],
							  limiterId,
		                      &groupEntry,
							  &len) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_Get_GroupElement failed (limiter id=%d)\n",
						   __FUNCTION__,limiterId);
		return MEA_ERROR;
	}
	if(isAdd){
		groupEntry.private_dbt.numberOfStaticEntries++;
	}else{
		groupEntry.private_dbt.numberOfStaticEntries--;
	}

	if (ENET_Set_GroupElement(unit_i,
						  mea_limiter_group_id[unit_i],
						  limiterId,
						  &groupEntry,
						  len) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					   "%s - ENET_Set_GroupElement failed (limiter id=%d)\n",
					   __FUNCTION__,limiterId);
		return MEA_ERROR;
	}
    /* Return to caller */
    return MEA_OK; 
}


static void      mea_Limiter_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3)
{
	
    MEA_Uint32             limitValue   = (MEA_Uint32)((long) arg1);
    MEA_Bool               clearCurrent = (MEA_Bool  )((long) arg2);
    MEA_Uint32             action       = (MEA_Uint32)((long) arg3);

    
    MEA_Uint32                val0;
	MEA_Uint32	              val1;
    val0 = 0;
    val1 = 0;

	val0 |= ((limitValue 
              << MEA_OS_calc_shift_from_mask(MEA_IF_LIMITER_CONFIG_LIMIT_MASK1)
             ) & MEA_IF_LIMITER_CONFIG_LIMIT_MASK1
            );
	
	val1 |= ((action 
              << MEA_OS_calc_shift_from_mask(MEA_IF_LIMITER_ACTION_MASK2)
             ) & MEA_IF_LIMITER_ACTION_MASK2
            );

	if (clearCurrent){
		    val1  |= ((1 << MEA_OS_calc_shift_from_mask(MEA_IF_LIMITER_UPDATE_CURRENT_MASK2)
			 ) & MEA_IF_LIMITER_UPDATE_CURRENT_MASK2);
	}

    MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0) , val0 , MEA_MODULE_IF);
    MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(1) , val1 , MEA_MODULE_IF);
}

/*---------------------------------------------------------------------------*/
/*            <mea_limiter_UpdateHwLimiter>                                  */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_limiter_UpdateHwLimiter(MEA_Limiter_t              limitId,
											  mea_limiter_dbt            *entry,
                                              mea_limiter_dbt            *old_entry)
{
	MEA_ind_write_t  ind_write;
	MEA_Uint32 clearCurrent = 1;    /* on create */
	if (old_entry!=NULL)
		clearCurrent = 0; /* on set */
#if MEA_LIMETER_DISABLE_HW
    return MEA_OK;
#endif	
    
    if ((!mea_limiter_InitDone[MEA_UNIT_0])  ||
		(old_entry == NULL)                  ||
		(entry->public_dbt.limit !=old_entry->public_dbt.limit) ||
		(entry->public_dbt.action_type !=old_entry->public_dbt.action_type)) {
		/* build the ind_write value */
		ind_write.tableType		= ENET_IF_CMD_LIMITER;
		ind_write.tableOffset	= limitId;
		ind_write.cmdReg		= MEA_IF_CMD_REG;      
		ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
		ind_write.statusReg		= MEA_IF_STATUS_REG;   
		ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
		ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
		ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
		ind_write.writeEntry    = (MEA_FUNCPTR)mea_Limiter_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)((long)entry->public_dbt.limit);
        ind_write.funcParam2 = (MEA_funcParam)((long)clearCurrent);
        ind_write.funcParam3 = (MEA_funcParam)((long)entry->public_dbt.action_type);

		if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (LimitId=%d)\n",
							  __FUNCTION__,ind_write.tableType,MEA_MODULE_IF,limitId);
			return MEA_ERROR;
		}
	}
    /* return to caller */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_API_Delete_all_limiters>                                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_all_limiters(MEA_Unit_t unit) 
{

   MEA_Limiter_t      db_limiterId;
   MEA_Limiter_t      temp_db_limiterId;
   MEA_Bool           found;

    if(!MEA_GLOBAL_LIMITER_SUPPORT)
        return MEA_OK;

 //   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"alex DBG   MEA_GLOBAL_LIMITER_SUPPORT %d ",MEA_GLOBAL_LIMITER_SUPPORT );

   /* Get the first limiter */
   if (MEA_API_GetFirst_Limiter(unit,&db_limiterId,&found) != MEA_OK)  {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_GetFirst_Limiter failed\n",
                          __FUNCTION__);
        return MEA_ERROR; 
   }

   /* loop until no more limiter */
   while (found) { 


       /* Save the current db_limiterId */
       temp_db_limiterId = db_limiterId;

       /* Make next before delete the object */
       if (MEA_API_GetNext_Limiter(unit,&db_limiterId,&found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_GetNext_Limiter failed (db_limiterId=%d)\n",
                             __FUNCTION__,db_limiterId);
           return MEA_ERROR;
       }

       /* Ignore the special limiter */
       if (temp_db_limiterId == MEA_LIMITER_DONT_CARE) {
            continue;
       }
       
       /* Delete the save db_limiterId */
       if (MEA_API_Delete_Limiter(unit,temp_db_limiterId)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete limiter %d\n",
                             __FUNCTION__,temp_db_limiterId);
           return MEA_ERROR;
       }
   }

   /* Return to caller */
   return MEA_OK;

}





static MEA_Status mea_drv_ADM_UpDate_Table(MEA_Unit_t unit, MEA_Port_t port);

/**--------------------------------------------------------------------**/
/* ADM DRV                                                              */
/**--------------------------------------------------------------------**/

/************************************************************************/
/* ADM INT                                                              */
/************************************************************************/
MEA_Status mea_drv_ADM_Init(MEA_Unit_t unit)
{
    MEA_Uint32 size;
    MEA_Uint32 port;
    MEA_Uint32 i;

    if (!MEA_ADM_SUPPORT)
        return MEA_OK;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize IF ADM table ...\n");
    size = (MEA_MAX_PORT_NUMBER+1) * sizeof(MEA_ADM_entry_dbt);
    if (size != 0) {
        MEA_ADM_Table = (MEA_ADM_entry_dbt*)MEA_OS_malloc(size);
        if (MEA_ADM_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_ADM_Table failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_ADM_Table[0]), 0, size);
    }
    
    size = sizeof(MEA_ADM_Cluster_Group_dbt);
    size = size* (MEA_ADM_TOTAL_GROUP_CLUSTER);
     for (port = 0; port <=MEA_MAX_PORT_NUMBER; port++)
     {
         MEA_ADM_Table[port].Groups = (MEA_ADM_Cluster_Group_dbt*)MEA_OS_malloc(size);
 
      }
     for (port = 0; port <=MEA_MAX_PORT_NUMBER; port++)
     {
         for (i = 0; i < MEA_ADM_TOTAL_GROUP_CLUSTER; i++) {
             MEA_ADM_Table[port].Groups[i].cluster[0] = 0;
             MEA_ADM_Table[port].Groups[i].cluster[1] = 0;
             MEA_ADM_Table[port].Groups[i].trunkcluster[0] = 0;
             MEA_ADM_Table[port].Groups[i].trunkcluster[1] = 0;

         }
     }




    return MEA_OK;
}
/************************************************************************/
/* ADM Reinit                                                          */
/************************************************************************/
MEA_Status mea_drv_ADM_ReInit(MEA_Unit_t unit)
{
    MEA_Port_t port;

    if (!MEA_ADM_SUPPORT)
        return MEA_OK;


    for (port = 0; port <=MEA_MAX_PORT_NUMBER; port++)
    {
        if (!MEA_ADM_Table[port].valid)
            continue;
        mea_drv_ADM_UpDate_Table(unit, port);

    }

    return MEA_OK;
}

/************************************************************************/
/*  ADM conclude                                                         */
/************************************************************************/
MEA_Status mea_drv_ADM_Conclude(MEA_Unit_t unit)
{
    //MEA_Uint32 i;

    if (!MEA_ADM_SUPPORT)
        return MEA_OK;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude ADM table ...\n");

//     for (i = 0; i < MEA_MAX_PORT_NUMBER; i++)
//     {
//         MEA_OS_free(MEA_ADM_Table[i].Groups);
//     }

    if (MEA_ADM_Table) {
        MEA_OS_free(MEA_ADM_Table);
    }


    return MEA_OK;
}


static void      mea_UpdateHwEntry_ADM(MEA_funcParam arg1,
                                       MEA_funcParam arg2,
                                       MEA_funcParam arg3,
                                       MEA_funcParam arg4)
{

    MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
    //    MEA_db_HwUnit_t         hwUnit_i   = (MEA_db_HwUnit_t)((long)arg2);
    //MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32              data = (MEA_Uint32)((long)arg4);
    
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            data,
            MEA_MODULE_IF);

}


static MEA_Status mea_drv_ADM_UpDate_Table(MEA_Unit_t unit, MEA_Port_t port)
{
    MEA_ind_write_t   ind_write;
    MEA_Uint32        retVal[32]; 
    MEA_Uint32        index,i;
    MEA_Uint32       numofRegs = 1;

    MEA_OS_memset(&retVal, 0, sizeof(retVal));

    index = 0;
    for (i = 0; i < MEA_ADM_TOTAL_GROUP_CLUSTER; i++) {
        if (MEA_ADM_Table[port].state == 0) {
            retVal[index++] = MEA_ADM_Table[port].Groups[i].cluster[0];
            retVal[index++] = MEA_ADM_Table[port].Groups[i].cluster[1];
          
        }
        else {
            /*trunk mode*/
            retVal[index++] = MEA_ADM_Table[port].Groups[i].trunkcluster[0];
            retVal[index++] = MEA_ADM_Table[port].Groups[i].trunkcluster[1];
            
        }

    }
   
    
    
    /************************************************************************/
    /* group 0,1                                                            */
    /************************************************************************/
    
        /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_ADM_PORT;
    
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_UpdateHwEntry_ADM;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    ind_write.funcParam2 = (MEA_funcParam)((long)0); //hwUnit
    
    for (index = 0; index < MEA_ADM_TOTAL_GROUP_CLUSTER*2; index++) {
        ind_write.tableOffset = (port * MEA_ADM_TOTAL_GROUP_CLUSTER)*2 + index;
        ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
        ind_write.funcParam4 = (MEA_funcParam)((long)retVal[index]);


        if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
            return MEA_ERROR;
        }
    }

  


    return MEA_OK;
}


MEA_Status mea_drv_ADM_Cluster_To_port(MEA_Unit_t unit,
    MEA_Port_t port,
    MEA_Uint8 group,
    MEA_Uint16 clusterId, /* Internal */
    MEA_Bool enable)
{
    MEA_Uint32 index;
    MEA_Uint32 shiptBit;
    
    //MEA_Uint32 val;

    if (!MEA_ADM_SUPPORT)
        return MEA_OK;

    if (clusterId == UNASSIGNED_QUEUE_VAL)
        return MEA_OK;
    /*Get the internal cluster and the groupId*/

    /*Add to table */

    /* Update The Hw if the port is on regular*/

    if (clusterId <= 31)
        index = 0;
    if (clusterId >= 32 && clusterId <= 63)
        index = 1;


    shiptBit = (clusterId) % 32;

    if (enable) {
       

        //val=MEA_ADM_Table[port].Groups[group].cluster[index];

        if (!( (MEA_ADM_Table[port].Groups[group].cluster[index]) & ((0x00000001 << (shiptBit))) )
           ) {
            (MEA_ADM_Table[port].Groups[group].cluster[index]) |= (0x00000001 << (shiptBit));
            MEA_ADM_Table[port].numofCluster++;
        }
        else {
            return MEA_OK;
        }
    }
    else {
        if (
            (MEA_ADM_Table[port].Groups[group].cluster[index]) & ((0x00000001 << (shiptBit)))
           ) {
            (MEA_ADM_Table[port].Groups[group].cluster[index]) &= (~(0x00000001 << (shiptBit)));
            if (MEA_ADM_Table[port].numofCluster >= 1)
                MEA_ADM_Table[port].numofCluster--;
        }
        else {
            return MEA_OK;
        }
    }
    
    if (MEA_ADM_Table[port].numofCluster) {
        MEA_ADM_Table[port].valid = MEA_TRUE;
    }
   

    if (mea_drv_ADM_UpDate_Table(unit, port) != MEA_OK)
    {
        return MEA_ERROR;
    }

    if (MEA_ADM_Table[port].numofCluster == 0) {
        MEA_ADM_Table[port].valid = MEA_FALSE;
    }



    return MEA_OK;
}

MEA_Status mea_drv_ADM_port_ToTrunk(MEA_Unit_t  unit, 
                                    MEA_Port_t  port, 
                                    MEA_Port_t  trunkId, 
                                    MEA_Bool    enable )
{
    MEA_Uint32 i;

    if (!MEA_ADM_SUPPORT)
        return MEA_OK;


    if (trunkId >= (MEA_MAX_PORT_NUMBER + 1)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "can't set trunkId %d\n", trunkId);
        return MEA_ERROR;
    }

    if ((MEA_ADM_Table[port].state == enable) &&
        (MEA_ADM_Table[port].trunkPort == trunkId))
    {
        /*-----------*/
        return MEA_OK;
    }

    MEA_ADM_Table[port].trunkPort = trunkId; /*need to be port*/
    if (enable == MEA_TRUE) {
        MEA_ADM_Table[port].state = enable;
        if (port == trunkId) {
            for (i = 0; i < MEA_ADM_TOTAL_GROUP_CLUSTER; i++) {
                MEA_OS_memcpy(&MEA_ADM_Table[port].Groups[i].trunkcluster, &MEA_ADM_Table[port].Groups[i].cluster, sizeof(MEA_ADM_Table[port].Groups[0].trunkcluster));
            }
        }
        else {
            for (i = 0; i < MEA_ADM_TOTAL_GROUP_CLUSTER; i++) {
                MEA_OS_memcpy(&MEA_ADM_Table[port].Groups[i].trunkcluster, &MEA_ADM_Table[trunkId].Groups[i].cluster, sizeof(MEA_ADM_Table[port].Groups[0].trunkcluster));
            }
        }
    }
    else {
        MEA_ADM_Table[port].state = enable; /*regular*/
        for (i = 0; i < MEA_ADM_TOTAL_GROUP_CLUSTER; i++) {
            MEA_OS_memset(&MEA_ADM_Table[port].Groups[i].trunkcluster, 0, sizeof(MEA_ADM_Table[port].Groups[0].trunkcluster));
        }
    }

    if(mea_drv_ADM_UpDate_Table(unit, port) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "mea_drv_ADM_UpDate_Table failed  port %d  trunk %d \n", port, trunkId, enable);
        return MEA_ERROR;
    }
        





    return MEA_OK;
}




/************************************************************************/
/* Segment switch disable port/Cluster                                  */
/************************************************************************/

/************************************************************************/
/*   ISOLATE profiles                                                    */
/************************************************************************/






static MEA_Status mea_drv_ISOLATE_UpdateHw(MEA_Unit_t unit_i,
                                           MEA_Uint16 id_i,
                                           MEA_ISOLATE_dbt    *new_entry_pi,
                                           MEA_ISOLATE_dbt    *old_entry_pi)
{
    MEA_Uint32  offsetOnTable;
    MEA_Uint32  numofRegs = 4;

    if (id_i== 0 || id_i >= MEA_ISOLATE_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - out range \n", __FUNCTION__, id_i);
        return MEA_OK;
    }
        
    if ((MEA_Platform_Get_MaxNumOfClusters() / 32) > numofRegs)
    {
        numofRegs = (MEA_Platform_Get_MaxNumOfClusters() / 32);
    }
    /************************************************************************/
    /* check if there changing                                              */
    /************************************************************************/
    if ((old_entry_pi) && 
        (MEA_OS_memcmp(&new_entry_pi,
        &old_entry_pi,
        sizeof(MEA_OutPorts_Entry_dbt)) == 0)) {
        return MEA_OK;
    }



    offsetOnTable = MEA_GLOBAl_TBL_REG_SEGMANT_ISOLATE_PROF0 + (id_i -1);  /*prof 1 is offset 72*/
   
    if (mea_drv_write_IF_TBL_X_offset_UpdateEntry_pArray(unit_i, ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL,
        offsetOnTable,
        numofRegs,
        &new_entry_pi->outQueue.out_ports_0_31) != MEA_OK) {
        return MEA_ERROR;
    }



    /* Return to caller */
    return MEA_OK;
}


static MEA_Bool mea_drv_ISOLATE_find_free(MEA_Unit_t       unit_i,
    MEA_Uint16       *id_io)
{
    MEA_Uint16 i;

    for (i = 1; i < MEA_ISOLATE_MAX_PROF; i++) {
        if (MEA_ISOLATE_info_Table[i].valid == MEA_FALSE) {
            *id_io = i;
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;

}







static MEA_Bool mea_drv_ISOLATE_IsRange(MEA_Unit_t unit_i, MEA_Uint16 id_i, MEA_Bool silent)
{

    if ((id_i) == 0 || ((id_i) >= MEA_ISOLATE_MAX_PROF)) {
        if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id=%d out range \n", __FUNCTION__, id_i);
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

static MEA_Status mea_drv_ISOLATE_IsExist(MEA_Unit_t     unit_i,
                                         MEA_Uint16 id_i,
                                        MEA_Bool silent,
                                         MEA_Bool *exist)
{
    

    if (exist == NULL) {
        if(!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);

        return MEA_ERROR;
    }
    *exist = MEA_FALSE;

    if (mea_drv_ISOLATE_IsRange(unit_i, id_i, silent) != MEA_TRUE) {
        if(!silent)
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  failed  for id=%d\n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }

    if (MEA_ISOLATE_info_Table[id_i].valid) {
        *exist = MEA_TRUE;
        return MEA_OK;
    }

    return MEA_OK;
}

#if 0 
static MEA_Status mea_drv_ISOLATE_check_parameters(MEA_Unit_t       unit_i,
                                                  MEA_ISOLATE_dbt      *entry_pi)
{



    return MEA_OK;
}

static MEA_Status mea_drv_ISOLATE_add_owner(MEA_Unit_t unit_i,MEA_Uint16       id_i)
{
    MEA_Bool exist=MEA_FALSE;

    if (mea_drv_ISOLATE_IsExist(unit_i, id_i,MEA_FALSE, &exist) != MEA_OK) {
        return MEA_ERROR;
    }
	
   if(!exist)
	return MEA_ERROR;
	
	
    MEA_ISOLATE_info_Table[id_i].num_of_owners++;

    return MEA_OK;
}
static MEA_Status mea_drv_ISOLATE_delete_owner(MEA_Unit_t   unit_i,                                              MEA_Uint16       id_i)
{
    MEA_Bool exist=MEA_FALSE;

    if (mea_drv_ISOLATE_IsExist(unit_i, id_i, MEA_FALSE,&exist) != MEA_OK) {
        return MEA_ERROR;
    }
	if(!exist)
	  return MEA_ERROR;

    if (MEA_ISOLATE_info_Table[id_i].num_of_owners > 1) {
        MEA_ISOLATE_info_Table[id_i].num_of_owners--;
        return MEA_OK;

    }


    

    return MEA_OK;
}
#endif
 MEA_Bool mea_drv_ISOLATE_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;
    if (!MEA_SEGMENT_SWITCH_SUPPORT) {
        return MEA_OK;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Initialize ISOLATE \n", __FUNCTION__);

    

    size = MEA_ISOLATE_MAX_PROF * sizeof(MEA_ISOLATE_Entry_dbt);
    if (size != 0) {
        MEA_ISOLATE_info_Table = (MEA_ISOLATE_Entry_dbt*)MEA_OS_malloc(size);
        if (MEA_ISOLATE_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_ISOLATE_info_Table failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_ISOLATE_info_Table[0]), 0, size);
    }
    /**/
    // TBD do i need to clear the memory in the FPGA



    return MEA_OK;
}
 MEA_Bool mea_drv_ISOLATE_RInit(MEA_Unit_t      unit_i)
{
    MEA_Uint16 id;
    if (!MEA_SEGMENT_SWITCH_SUPPORT) {
         return MEA_OK;
    }

    for (id = 1; id < MEA_ISOLATE_MAX_PROF; id++) {
        if (MEA_ISOLATE_info_Table[id].valid) {
            if (mea_drv_ISOLATE_UpdateHw(unit_i,
                id,
                &(MEA_ISOLATE_info_Table[id].privet_data.internal_Queue),
                NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  mea_drv_ISOLATE_UpdateHw failed (id=%d)\n", __FUNCTION__, id);
                return MEA_ERROR;
            }
        }
    }


    return MEA_OK;
}
 MEA_Bool mea_drv_ISOLATE_Conclude(MEA_Unit_t       unit_i)
{
    if (!MEA_SEGMENT_SWITCH_SUPPORT) {
         return MEA_OK;
    }


    /* Free the table */
    if (MEA_ISOLATE_info_Table != NULL) {
        MEA_OS_free(MEA_ISOLATE_info_Table);
    }

    return MEA_OK;
}

/************************************************************************/
/* ISOLATE API                                                          */
/************************************************************************/
MEA_Status MEA_API_ISOLATE_IsExist(MEA_Unit_t unit_i, MEA_Uint16 id_i, MEA_Bool silent, MEA_Bool *exist)
{
    if (!MEA_SEGMENT_SWITCH_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  SEGMENT_SWITCH_SUPPORT %s \n",
            __FUNCTION__, MEA_STATUS_STR(MEA_SEGMENT_SWITCH_SUPPORT));
        return MEA_ERROR;
    }


    return mea_drv_ISOLATE_IsExist(unit_i, id_i, silent, exist);
}
MEA_Status MEA_API_ISOLATE_Create_Entry(MEA_Unit_t   unit_i, MEA_Uint16     *id_io)
{
    MEA_Bool exist;
    MEA_Bool silent = MEA_FALSE;
//     ENET_QueueId_t ClusterEx_Id, ClusterInternal_Id;
//     MEA_Uint32 *pPortGrp;
//     MEA_Uint32 *pPortGrpInternal;
//     MEA_Uint32  shiftWord = 0x00000001;
    MEA_ISOLATE_dbt  entryInt;


    if (!MEA_SEGMENT_SWITCH_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  SEGMENT_SWITCH_SUPPORT %s \n",
            __FUNCTION__,MEA_STATUS_STR(MEA_SEGMENT_SWITCH_SUPPORT));
        return MEA_ERROR;
    }

    if (id_io == NULL)
        return MEA_ERROR;

    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID) {

        

        if (mea_drv_ISOLATE_IsRange(unit_i, *id_io, silent) != MEA_TRUE) {
            if (!silent)
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  failed mea_drv_ISOLATE_IsRange Is out of Range id=%d\n", __FUNCTION__, *id_io);
            return MEA_ERROR;
        }

        if (mea_drv_ISOLATE_IsExist(unit_i, *id_io, silent, &exist) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_ISOLATE_IsExist failed (*id_io=%d\n",
                __FUNCTION__, *id_io);
            return MEA_ERROR;
        }
        if (exist) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - id_io %d is already exist\n", __FUNCTION__, *id_io);
            return MEA_ERROR;
        }


    }
    else {
        /*create profile */
        if (mea_drv_ISOLATE_find_free(unit_i, id_io) == MEA_FALSE)
        {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ISOLATE_find_free failed\n", __FUNCTION__);
        return MEA_ERROR;

        }
    }

/************************************************************************/
/*  set all cluster to Enable                                           */
/************************************************************************/
#if 0
    pPortGrp = (MEA_Uint32*)(&(MEA_ISOLATE_info_Table[*id_io].data.outQueue.out_ports_0_31));
    pPortGrpInternal = (MEA_Uint32*)(&(MEA_ISOLATE_info_Table[*id_io].privet_data.internal_Queue.outQueue.out_ports_0_31));
    for (ClusterEx_Id = 0; ClusterEx_Id < MEA_Platform_Get_MaxNumOfClusters(); ClusterEx_Id++)
    {
        if (ENET_IsValid_Queue(unit_i,
            ClusterEx_Id,/*external*/
            MEA_TRUE) == MEA_TRUE) {
            *(pPortGrp + (ClusterEx_Id / 32)) |= (shiftWord << (ClusterEx_Id % 32));

            ClusterInternal_Id = enet_cluster_external2internal(ClusterEx_Id);
            *(pPortGrpInternal + (ClusterInternal_Id / 32)) |= (shiftWord << (ClusterInternal_Id % 32));

        }

    }
#endif


/************************************************************************/
/* Write to HW                                                          */
/************************************************************************/
    MEA_OS_memset(&entryInt, 0, sizeof(MEA_ISOLATE_dbt));
    if (mea_drv_ISOLATE_UpdateHw(unit_i, *id_io,
        &entryInt,
        NULL) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ISOLATE_UpdateHw \n", __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&MEA_ISOLATE_info_Table[*id_io].privet_data.internal_Queue.outQueue,
        &entryInt.outQueue,
        sizeof(MEA_OutPorts_Entry_dbt));

    MEA_OS_memcpy(&MEA_ISOLATE_info_Table[*id_io].data.outQueue,
        &entryInt.outQueue,  // because all zero 
        sizeof(MEA_OutPorts_Entry_dbt));

    MEA_ISOLATE_info_Table[*id_io].valid = MEA_TRUE;
    MEA_ISOLATE_info_Table[*id_io].num_of_owners = 1;


return MEA_OK;
}

MEA_Status MEA_API_ISOLATE_Set_Entry(MEA_Unit_t unit_i, MEA_Uint16 id_i, MEA_ISOLATE_dbt  *entry_pi)
{
    ENET_QueueId_t ClusterEx_Id, ClusterInternal_Id;
    MEA_Uint32 *pPortGrp;
    MEA_Uint32 *pPortGrpEx;
    MEA_Uint32 *pPortGrpInternal;
    MEA_Uint32  shiftWord = 0x00000001;
    MEA_ISOLATE_dbt  entryEx;
    MEA_ISOLATE_dbt  entryInt;
   



    if (!MEA_SEGMENT_SWITCH_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  SEGMENT_SWITCH_SUPPORT %s \n",
            __FUNCTION__, MEA_STATUS_STR(MEA_SEGMENT_SWITCH_SUPPORT));
        return MEA_ERROR;
    }

    if (entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s entry_pi is NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (((id_i) == 0) || (id_i) >= MEA_ISOLATE_MAX_PROF) {

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - out range (1: %d)\n", __FUNCTION__, MEA_ISOLATE_MAX_PROF - 1);
        return MEA_ERROR;
    }
    

    MEA_OS_memset(&entryEx, 0, sizeof(MEA_ISOLATE_dbt));
    MEA_OS_memset(&entryInt, 0, sizeof(MEA_ISOLATE_dbt));

    pPortGrp = (MEA_Uint32*)(&entry_pi->outQueue.out_ports_0_31);
    pPortGrpEx = (MEA_Uint32*)(&(entryEx.outQueue.out_ports_0_31));
    pPortGrpInternal = (MEA_Uint32*)(&(entryInt.outQueue.out_ports_0_31));
    for (ClusterEx_Id = 0; ClusterEx_Id < MEA_Platform_Get_MaxNumOfClusters(); ClusterEx_Id++)
    {
        if ((*(pPortGrp + (ClusterEx_Id / 32)) >> (ClusterEx_Id % 32) ) & 0x00000001) {
            if (ENET_IsValid_Queue(unit_i,
                ClusterEx_Id,/*external*/
                MEA_TRUE) == MEA_FALSE) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - ENET_IsValid_Queue is not Valid \n", __FUNCTION__);
                return MEA_ERROR;
            }
            else {
                *(pPortGrpEx + (ClusterEx_Id / 32)) |= (shiftWord << (ClusterEx_Id % 32));
                ClusterInternal_Id = enet_cluster_external2internal(ClusterEx_Id);
                *(pPortGrpInternal + (ClusterInternal_Id / 32)) |= (shiftWord << (ClusterInternal_Id % 32));
            }

        }



    }

   
    
    /************************************************************************/
    /* Write to HW                                                          */
    /************************************************************************/
    if(mea_drv_ISOLATE_UpdateHw(unit_i, id_i, 
                                &entryInt, 
                                &MEA_ISOLATE_info_Table[id_i].privet_data.internal_Queue) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ISOLATE_UpdateHw \n", __FUNCTION__);
        return MEA_ERROR;
    }



    /************************************************************************/
    /* UPdate                                                               */
    /************************************************************************/
    MEA_OS_memcpy(&MEA_ISOLATE_info_Table[id_i].privet_data.internal_Queue.outQueue,
        &entryInt.outQueue,
        sizeof(MEA_OutPorts_Entry_dbt));

    MEA_OS_memcpy(&MEA_ISOLATE_info_Table[id_i].data.outQueue,
        &entry_pi->outQueue,
        sizeof(MEA_OutPorts_Entry_dbt));

    return MEA_OK;
}

MEA_Status MEA_API_ISOLATE_Delete_Entry(MEA_Unit_t   unit_i, MEA_Uint16     id_i)
{
    MEA_Bool exist = MEA_FALSE;
    MEA_ISOLATE_dbt  entryEx;

    if (!MEA_SEGMENT_SWITCH_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  SEGMENT_SWITCH_SUPPORT %s \n",
            __FUNCTION__, MEA_STATUS_STR(MEA_SEGMENT_SWITCH_SUPPORT));
        return MEA_ERROR;
    }
    if (((id_i) == 0 ) || (id_i) >=  MEA_ISOLATE_MAX_PROF) {

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - out range (1: %d)\n", __FUNCTION__ , MEA_ISOLATE_MAX_PROF-1);
        return MEA_ERROR;
    }
   

    if(MEA_API_ISOLATE_IsExist(unit_i, id_i, MEA_FALSE, &exist) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_API_ISOLATE_IsExist failed  (id_i=%d) \n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }
    if (exist == MEA_FALSE) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - can't not Delete (id_i=%d)  because Is not Exist \n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }


    if (MEA_ISOLATE_info_Table[id_i].num_of_owners > 1) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - can't not Delete (id_i=%d)  because need to delete all owners \n", __FUNCTION__, id_i);
        return MEA_ERROR;
        

    } 
    
    /*set to Zero */
    MEA_OS_memset(&entryEx, 0, sizeof(MEA_ISOLATE_dbt));
   /************************************************************************/
   /* Write to HW                                                          */
   /************************************************************************/
    if (MEA_API_ISOLATE_Set_Entry(unit_i, id_i, &entryEx) != MEA_OK) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_API_ISOLATE_Set_Entry (id_i=%d) failed \n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/

    MEA_OS_memset(&MEA_ISOLATE_info_Table[id_i], 0, sizeof(MEA_ISOLATE_Entry_dbt));
    MEA_ISOLATE_info_Table[id_i].valid = MEA_FALSE;
    MEA_ISOLATE_info_Table[id_i].num_of_owners=0;




    return MEA_OK;
}
MEA_Status MEA_API_ISOLATE_Get_Entry(MEA_Unit_t   unit_i, MEA_Uint16     id_i, MEA_ISOLATE_dbt  *entry_pi)
{
    if (!MEA_SEGMENT_SWITCH_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  SEGMENT_SWITCH_SUPPORT %s \n",
            __FUNCTION__, MEA_STATUS_STR(MEA_SEGMENT_SWITCH_SUPPORT));
        return MEA_ERROR;
    }
    if ((id_i)== 0 || (id_i) >= MEA_ISOLATE_MAX_PROF) {

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - out range \n", __FUNCTION__);
        return MEA_FALSE;
    }
    if (entry_pi == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - entry_pi == NULL \n", __FUNCTION__);
        return MEA_FALSE;
    }

    MEA_OS_memcpy(entry_pi ,&MEA_ISOLATE_info_Table[id_i].data.outQueue,
                   sizeof(MEA_OutPorts_Entry_dbt));

    return MEA_OK;
}



