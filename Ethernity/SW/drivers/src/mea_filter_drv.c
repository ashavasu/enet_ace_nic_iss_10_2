/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_db_drv.h"
#include "mea_filter_drv.h"
#include "mea_if_regs.h"
#include "enet_xPermission_drv.h"
#include "mea_action_drv.h"
#include "mea_bm_ehp_drv.h"
#include "mea_bm_counters_drv.h"
#include "mea_policer_drv.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct {
	MEA_Uint32 reference_count;
	char       da[6];
} MEA_drv_Filter_Globals_dbt;




/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static MEA_Service_t MEA_drv_Filter_Global_Sid_count;
static MEA_Service_t MEA_drv_Filter_default_Contex;

static MEA_Counters_ACL_dbt MEA_Counters_ACL_TBL;

static MEA_drv_filter_mask_profile_dbt *MEA_Filter_Mask_Prof_Table = NULL;
static MEA_drv_Filter_Globals_dbt      *MEA_drv_Filter_Globals     = NULL;

static MEA_Filter_Entry_dbt            *MEA_drv_Filter_Tbl         = NULL;
static MEA_Filter_Context_dbt         *MEA_drv_Filter_ContextTbl  = NULL;
static MEA_Filter_Action_dbt           *MEA_drv_Filter_ActionTbl   = NULL;
static MEA_Uint32                       MEA_drv_Filter_temp[10];






// static MEA_db_dbt                       MEA_Filter_Mask_db         = NULL;
//  static MEA_db_dbt                       MEA_Filter_db              = NULL;
//  static MEA_db_dbt                       MEA_Filter_Ctx_db          = NULL;
// 
//  static MEA_db_dbt                       MEA_Filter_Action_db = NULL;


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
 static MEA_Status mea_drv_Filter_Mask_Prof_UpdateHw(MEA_Unit_t   unit_i,
                                    MEA_FilterMask_t              id_i,
                                    //MEA_drv_filter_mask_profile_dbt *entry_pi
                                    MEA_ACL_prof_Key_mask_dbt *entry_pi);
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_Service_GetHwUnit>                               */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_db_HwUnit_t mea_Filter_GetHwUnit(MEA_Unit_t          unit_i,
  							         MEA_Filter_Key_dbt* key_pi) 
{


    if (globalMemoryMode==MEA_MODE_REGULAR_ENET3000) {
		return MEA_HW_UNIT_ID_GENERAL;
	} 
	

	return MEA_HW_UNIT_ID_UPSTREAM;

}


static void      mea_FilterCtx_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
	//MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
   // MEA_db_HwId_t          hwId_i = (MEA_db_HwId_t)((long)    arg3);
    MEA_Filter_Entry_dbt  *entry_pi = (MEA_Filter_Entry_dbt *)arg4;
	MEA_Uint32             val[4];
	MEA_Uint32             i;
	MEA_Uint16             num_of_regs;
	MEA_Uint32             output_valid=0;
    MEA_Uint32             count_shift;

	num_of_regs = MEA_NUM_OF_ELEMENTS(val);
	for (i=0;i<num_of_regs;i++) {
		val[i] = 0;
	}
 
count_shift=0;
	if( entry_pi->valid )
	{
		if ( entry_pi->data.action_params.output_ports_valid ||
			 entry_pi->data.action_type == MEA_FILTER_ACTION_SEND_TO_CPU || 
			 entry_pi->data.action_type == MEA_FILTER_ACTION_DISCARD )
		{
			
            MEA_OS_insert_value(count_shift,
                MEA_IF_FILTER_WORD_0_XPERMISSION_ID_WIDTH(hwUnit_i),
                (entry_pi->xpermission_id),
                &val[0]);
		}

        

       
        count_shift+=MEA_IF_FILTER_WORD_0_XPERMISSION_ID_WIDTH(hwUnit_i);




		output_valid = entry_pi->data.action_params.output_ports_valid;

		if ( entry_pi->data.action_type == MEA_FILTER_ACTION_SEND_TO_CPU || 
			 entry_pi->data.action_type == MEA_FILTER_ACTION_DISCARD )
		{
			output_valid = 1;
		}
			

        MEA_OS_insert_value(count_shift,
            MEA_IF_FILTER_WORD_0_XPERMISSION_VALID_WIDTH,
            (output_valid),
            &val[0]);
        count_shift+=MEA_IF_FILTER_WORD_0_XPERMISSION_VALID_WIDTH;
        
        MEA_OS_insert_value(count_shift,
            MEA_IF_FILTER_WORD_0_AID_VALID_WIDTH,
            !(entry_pi->data.fwd_Act_en),  /*was always (1) */
            &val[0]); /*force Action */
            count_shift+= MEA_IF_FILTER_WORD_0_AID_VALID_WIDTH;
            MEA_OS_insert_value(count_shift,
                MEA_IF_FILTER_WORD_0_LX_WIN_VALID_WIDTH,
                (entry_pi->data.lxcp_win),
                &val[0]);
            count_shift+= MEA_IF_FILTER_WORD_0_LX_WIN_VALID_WIDTH;


            MEA_OS_insert_value(count_shift,
                MEA_IF_FILTER_WORD_0_HPM_COS_WIDTH,
                (entry_pi->data.hpm_cos),
                &val[0]);
            count_shift += MEA_IF_FILTER_WORD_0_HPM_COS_WIDTH;
            MEA_OS_insert_value(count_shift,
                MEA_IF_FILTER_WORD_0_HPM_COS_VALID_WIDTH,
                (entry_pi->data.hpm_cos_valid),
                &val[0]);
            count_shift += MEA_IF_FILTER_WORD_0_HPM_COS_VALID_WIDTH;
            
            


            /*********************************************/
            MEA_OS_insert_value(count_shift,
                MEA_IF_FILTER_WORD_0_ACTION_ID,
                (entry_pi->data.Action_Id),
                &val[0]);
            count_shift += MEA_IF_FILTER_WORD_0_ACTION_ID;
            
            MEA_OS_insert_value(count_shift,
                MEA_IF_FILTER_WORD_0_FWD_EN,
                (entry_pi->data.fwd_enable),
                &val[0]);
            count_shift += MEA_IF_FILTER_WORD_0_FWD_EN;

            MEA_OS_insert_value(count_shift,
                MEA_IF_FILTER_WORD_0_FWD_EN_INTERNAL_MAC,
                (entry_pi->data.fwd_enable_Internal_mac),
                &val[0]);
            count_shift += MEA_IF_FILTER_WORD_0_FWD_EN_INTERNAL_MAC;

            

            MEA_OS_insert_value(count_shift,
                MEA_IF_FILTER_WORD_0_FWD_KEY,
                (entry_pi->data.fwd_Key),
                &val[0]);
            count_shift += MEA_IF_FILTER_WORD_0_FWD_KEY;

            MEA_OS_insert_value(count_shift,
                MEA_IF_FILTER_WORD_0_FWD_FORCE,
                (entry_pi->data.force_fwd_rule),
                &val[0]);
            count_shift += MEA_IF_FILTER_WORD_0_FWD_FORCE;
            
            
                MEA_OS_insert_value(count_shift,
                    MEA_IF_FILTER_WORD_0_FWD_DSE_MASK_FILED_TYPE,
                (entry_pi->data.DSE_mask_field_type),
                &val[0]);
                count_shift += MEA_IF_FILTER_WORD_0_FWD_DSE_MASK_FILED_TYPE;
   

		

	}
#if 0
	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_SRV_MAKE0_REG , val[0] , MEA_MODULE_IF);
	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_SRV_MAKE1_REG , val[1] , MEA_MODULE_IF);
	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_SRV_MAKE2_REG , val[2] , MEA_MODULE_IF);
	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_SRV_MAKE3_REG , val[3] , MEA_MODULE_IF);
#else
	for (i=0;i<num_of_regs;i++) {
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Filter Contex index [%d] = 0x%08x \n", i,val[i] );
		MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(i) , val[i] , MEA_MODULE_IF);

	}
#endif

}

MEA_Filter_Key_Type_te mea_filter_get_key_type(MEA_Filter_Key_dbt *Filter_Key)
{
	MEA_Uint32 reg = 0;

	reg |= (MEA_Uint32)(Filter_Key->layer2.ethernet.DA_valid) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_DA);

	reg |= (MEA_Uint32)(Filter_Key->layer2.ethernet.SA_valid) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_SA);

	reg |= (MEA_Uint32)(Filter_Key->layer2.outer_vlanTag.ethertype_valid) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_OUTER_ETHERTYPE);

	reg |= (MEA_Uint32)(Filter_Key->layer2.outer_vlanTag.priority_valid) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_OUTER_PRI);

    reg |= (MEA_Uint32)(Filter_Key->layer2.outer_vlanTag.cf_valid) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_OUTER_CFI);

	reg |= (MEA_Uint32)(Filter_Key->layer2.outer_vlanTag.vlan_valid) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_OUTER_VID);

	reg |= (MEA_Uint32)(Filter_Key->layer2.inner_vlanTag.ethertype_valid)
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_INNER_ETHERTYPE);

	reg |= (MEA_Uint32)(Filter_Key->layer2.inner_vlanTag.priority_valid) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_INNER_PRI);

	reg |= (MEA_Uint32)(Filter_Key->layer2.inner_vlanTag.cf_valid) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_INNER_CFI);

	reg |= (MEA_Uint32)(Filter_Key->layer2.inner_vlanTag.vlan_valid)
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_INNER_VID);

	reg |= (MEA_Uint32)(Filter_Key->layer2.mpls.mpls_label_valid)
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_MPLS_LABEL);

	reg |= (MEA_Uint32)(Filter_Key->layer2.ppp.pppoe_session_id_valid)
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_PPPOE_SESSION_ID);

	reg |= (MEA_Uint32)(Filter_Key->layer3.ethertype3_valid ) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_APPL_ETHERTYPE);

	reg |= (MEA_Uint32)(Filter_Key->layer3.ip_version_valid ) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_IP_VERSION);

    reg |= (MEA_Uint32)(Filter_Key->layer3.ip_protocol_valid )
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_IP_PROTOCOL);

	reg |= (MEA_Uint32)(Filter_Key->layer3.l3_pri_valid ) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_L3_PRI);

	reg |= (MEA_Uint32)(Filter_Key->layer3.IPv4.dst_ip_valid ) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_DST_IPV4);

	reg |= (MEA_Uint32)(Filter_Key->layer3.IPv4.src_ip_valid ) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_SRC_IPV4);

	reg |= (MEA_Uint32)(Filter_Key->layer4.dst_port_valid ) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_DST_PORT_L4);

	reg |= (MEA_Uint32)(Filter_Key->layer4.src_port_valid ) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_SRC_PORT_L4);

	reg |= (MEA_Uint32)(Filter_Key->layer2.ppp.ppp_protocol_valid ) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_PPP_PROTOCOL);

	reg |= (MEA_Uint32)(Filter_Key->layer3.IPv6.dst_ipv6_valid)
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_DST_IPV6);

    reg |= (MEA_Uint32)(Filter_Key->layer3.IPv6.src_ipv6_valid)
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_SRC_IPV6);

    reg |= (MEA_Uint32)(0) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_RESERVED3);

    reg |= (MEA_Uint32)(0) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_RESERVED4);
    reg |= (MEA_Uint32)(0) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_RESERVED5);
    reg |= (MEA_Uint32)(0) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_RESERVED6);
    reg |= (MEA_Uint32)(0) 
                       << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_RESERVED7);

   
    /* l3_pri_valid is TRUE from bit 24 to bit 27 */
    if(Filter_Key->layer3.l3_pri_valid ){
        switch(Filter_Key->layer3.l3_pri_type){
            case MEA_FILTER_L3_TYPE_IPV4_TOS:
                reg |= 0x00000001 << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_TOS);
                break;
            case MEA_FILTER_L3_TYPE_IPV4_PRECEDENCE:
                reg |= 0x00000001 << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_PRECEDENCE);
                break; 
            case MEA_FILTER_L3_TYPE_IPV4_DSCP:
                reg |= 0x00000001 << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_DSCP);
                break;
            case MEA_FILTER_L3_TYPE_IPV6_TC:
                reg |= 0x00000001 << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_TC);
                break;
            case MEA_FILTER_L3_TYPE_LAST:
            default:
                reg |= 0x00000001 << MEA_OS_calc_shift_from_mask(MEA_FILTER_VAL_PRECEDENCE);
                break;
        }
    }
    


	
    
    switch (reg) 
	{
	case MEA_FILTER_KEY_TYPE_VAL_DA_MAC:
		return MEA_FILTER_KEY_TYPE_DA_MAC;
	case MEA_FILTER_KEY_TYPE_VAL_SA_MAC:
		return MEA_FILTER_KEY_TYPE_SA_MAC;
	case MEA_FILTER_KEY_TYPE_VAL_DST_IPV4_DST_PORT:
		return MEA_FILTER_KEY_TYPE_DST_IPV4_DST_PORT;
	case MEA_FILTER_KEY_TYPE_VAL_SRC_IPV4_DST_PORT:
		return MEA_FILTER_KEY_TYPE_SRC_IPV4_DST_PORT;
	case MEA_FILTER_KEY_TYPE_VAL_DST_IPV4_SRC_PORT: 
		return MEA_FILTER_KEY_TYPE_DST_IPV4_SRC_PORT;
	case MEA_FILTER_KEY_TYPE_VAL_SRC_IPV4_SRC_PORT:
		return MEA_FILTER_KEY_TYPE_SRC_IPV4_SRC_PORT;
	case MEA_FILTER_KEY_TYPE_VAL_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG:
		return MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG;
	case MEA_FILTER_KEY_TYPE_VAL_SRC_IPV4_APPL_ETHERTYPE:
		return MEA_FILTER_KEY_TYPE_SRC_IPV4_APPL_ETHERTYPE;
	
   
    case MEA_FILTER_KEY_TYPE_VAL_PRI_INNER_VLAN_IP_PROTO_APPL_ETHERTYPE_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_OUTER_VLAN_IP_PROTO_APPL_ETHERTYPE_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_TOS_IP_PROTO_APPL_ETHERTYPE_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_PREC_IP_PROTO_APPL_ETHERTYPE_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_DSCP_IP_PROTO_APPL_ETHERTYPE_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_TC_IP_PROTO_APPL_ETHERTYPE_DST_PORT: 
		return MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT;
	
    
    case MEA_FILTER_KEY_TYPE_VAL_PRI_OUTER_VLAN_IP_PROTO_SRC_PORT_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_INNER_VLAN_IP_PROTO_SRC_PORT_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_TOS_IP_PROTO_SRC_PORT_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_PREC_IP_PROTO_SRC_PORT_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_DSCP_IP_PROTO_SRC_PORT_DST_PORT:
    case MEA_FILTER_KEY_TYPE_VAL_PRI_TC_IP_PROTO_SRC_PORT_DST_PORT:
        return MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_SRC_PORT_DST_PORT;

	case MEA_FILTER_KEY_TYPE_VAL_OUTER_VLAN_TAG_INNER_VLAN_TAG_OUTER_ETHERTYPE:
		return MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_INNER_VLAN_TAG_OUTER_ETHERTYPE;
	case MEA_FILTER_KEY_TYPE_VAL_OUTER_VLAN_TAG_DST_PORT_SRC_PORT:
		return MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_DST_PORT_SRC_PORT;
	case MEA_FILTER_KEY_TYPE_VAL_INNER_VLAN_TAG_DST_PORT_SRC_PORT:
		return MEA_FILTER_KEY_TYPE_INNER_VLAN_TAG_DST_PORT_SRC_PORT;
	case MEA_FILTER_KEY_TYPE_VAL_PPPOE_SESSION_ID_PPP_PROTO_SA_MAC_LSB: 
		return MEA_FILTER_KEY_TYPE_PPPOE_SESSION_ID_PPP_PROTO_SA_MAC_LSB;
    case MEA_FILTER_KEY_TYPE_VAL_OUTER_ETERTYPE_APPL_ETHERTYPE_OUTER_VLAN_TAG:
        return MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_APPL_ETHERTYPE_OUTER_VLAN_TAG; /* TBD */
    case MEA_FILTER_KEY_TYPE_VAL_DA_MAC_OUTER_VLAN_TAG_DST_IPV4:
    case MEA_FILTER_KEY_TYPE_VAL_DA_MAC_NOT_VALID_OUTER_VLAN_TAG_DST_IPV4:
		return MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4;
        break;

    case MEA_FILTER_KEY_TYPE_VAL_DST_IPV6_DST_PORT:
        return MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT;
        break;
    case MEA_FILTER_KEY_TYPE_VAL_SRC_IPV6_DST_PORT:
        return MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT;
        break;

    case MEA_FILTER_KEY_TYPE_VAL_DST_IPV6_SRC_PORT:
        return MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT;
        break;
    case MEA_FILTER_KEY_TYPE_VAL_SRC_IPV6_SRC_PORT:
        return MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT;
        break;
    case  MEA_FILTER_KEY_TYPE_VAL_SRC_IPV6_APPL_ETHERTYPE:
        return MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE;
        break;



	default:
		return MEA_FILTER_KEY_TYPE_LAST;
	}
}


MEA_Status MEA_API_Get_Filter_KeyType(MEA_Unit_t unit_i,
                                      MEA_Filter_Key_dbt *key_i,
                                      MEA_Filter_Key_Type_te *keyType_o)

{
    
    *keyType_o=  mea_filter_get_key_type(key_i);
    if(*keyType_o== MEA_FILTER_KEY_TYPE_LAST){
                         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - filter get raw key  failed \n",
                                 __FUNCTION__);
        return MEA_ERROR;
    }
    return MEA_OK;
}


MEA_Status mea_drv_filter_modify_data(MEA_Filter_Data_dbt *data, MEA_Filter_t filterId)
{
	if ( data->action_params.ed_id_valid )
	{
		MEA_drv_Filter_Tbl[filterId].data.action_params.ed_id_valid = MEA_TRUE;
		MEA_drv_Filter_Tbl[filterId].data.action_params.ed_id = data->action_params.ed_id;
	}
	if ( data->action_params.pm_id_valid )
	{
		MEA_drv_Filter_Tbl[filterId].data.action_params.pm_id_valid = MEA_TRUE;
		MEA_drv_Filter_Tbl[filterId].data.action_params.pm_id = data->action_params.pm_id;
	}
	if ( data->action_params.tm_id_valid )
	{
		MEA_drv_Filter_Tbl[filterId].data.action_params.tm_id_valid = MEA_TRUE;
		MEA_drv_Filter_Tbl[filterId].data.action_params.tm_id = data->action_params.tm_id;
	}
	if ( data->action_params.force_color_valid )
	{
		MEA_drv_Filter_Tbl[filterId].data.action_params.force_color_valid = MEA_TRUE;
		MEA_drv_Filter_Tbl[filterId].data.action_params.COLOR = data->action_params.COLOR;
	}
	if ( data->action_params.force_cos_valid )
	{
		MEA_drv_Filter_Tbl[filterId].data.action_params.force_cos_valid = MEA_TRUE;
		MEA_drv_Filter_Tbl[filterId].data.action_params.COS = data->action_params.COS;
	}
	if ( data->action_params.force_l2_pri_valid )
	{
		MEA_drv_Filter_Tbl[filterId].data.action_params.force_l2_pri_valid = MEA_TRUE;
		MEA_drv_Filter_Tbl[filterId].data.action_params.L2_PRI = data->action_params.L2_PRI;
	}
	if ( data->action_params.output_ports_valid )
	{
		MEA_drv_Filter_Tbl[filterId].data.action_params.output_ports_valid = MEA_TRUE;
	}
	if ( data->action_params.proto_llc_valid )
	{
		MEA_drv_Filter_Tbl[filterId].data.action_params.proto_llc_valid = MEA_TRUE;
		MEA_drv_Filter_Tbl[filterId].data.action_params.protocol_llc_force = data->action_params.protocol_llc_force;
		MEA_drv_Filter_Tbl[filterId].data.action_params.Protocol = data->action_params.Protocol;
		MEA_drv_Filter_Tbl[filterId].data.action_params.Llc = data->action_params.Llc;
	}

	return MEA_OK;
}


static MEA_Status mea_drv_filter_ipv6_sigature_function(MEA_Uint32 *ipv6_val, MEA_Uint32 *sigature)
{
    MEA_Uint32 ipv6_xor_tmp;
   // MEA_Uint16 temp[3];
    if (sigature == NULL) {
		return MEA_ERROR;
    }

    ipv6_xor_tmp  = ipv6_val[0] ^ ipv6_val[1] ^ ipv6_val[2] ^ ipv6_val[3];
#if 0 // xor bit16
    temp[0] = (MEA_Uint16)((ipv6_xor_tmp) & 0xffff);
    temp[1] = (MEA_Uint16)((ipv6_xor_tmp>>16) & 0xffff);

    temp[2] = temp[0] ^ temp[1];
     *sigature = 0;
     *sigature = (MEA_Uint32)(((MEA_Uint16)temp[2]) & 0x0000ffff);
#else
    *sigature = ipv6_xor_tmp;
#endif
    return MEA_OK;
}


static MEA_Status  mea_filter_get_raw_key(	MEA_Filter_Key_dbt *key,
										    MEA_Uint16		   *reg1, 
											MEA_Uint16		   *reg2, 
											MEA_Uint16		   *reg3,
											MEA_Filter_Key_Type_te *key_type)
{
	*key_type = mea_filter_get_key_type(key);
	*reg1 = 0;
	*reg2 = 0;
	*reg3 = 0;
    MEA_Uint32 sigature = 0;

	switch( *key_type )
	{
	case MEA_FILTER_KEY_TYPE_DA_MAC:
		*reg1 = (((MEA_Uint16)(key->layer2.ethernet.DA.b[4]) << 8) |
                 ((MEA_Uint16)(key->layer2.ethernet.DA.b[5]) << 0) );
		*reg2 = (((MEA_Uint16)(key->layer2.ethernet.DA.b[2]) << 8) |
                 ((MEA_Uint16)(key->layer2.ethernet.DA.b[3]) << 0) );
		*reg3 = (((MEA_Uint16)(key->layer2.ethernet.DA.b[0]) << 8) |
                 ((MEA_Uint16)(key->layer2.ethernet.DA.b[1]) << 0) );
		break;
	case MEA_FILTER_KEY_TYPE_SA_MAC:
		*reg1 = (((MEA_Uint16)(key->layer2.ethernet.SA.b[4]) << 8) |
                 ((MEA_Uint16)(key->layer2.ethernet.SA.b[5]) << 0) );
		*reg2 = (((MEA_Uint16)(key->layer2.ethernet.SA.b[2]) << 8) |
                 ((MEA_Uint16)(key->layer2.ethernet.SA.b[3]) << 0) );
		*reg3 = (((MEA_Uint16)(key->layer2.ethernet.SA.b[0]) << 8) |
                 ((MEA_Uint16)(key->layer2.ethernet.SA.b[1]) << 0) );
		break;
	case MEA_FILTER_KEY_TYPE_DST_IPV4_DST_PORT:
		*reg1 = (MEA_Uint16)(key->layer4.dst_port);
		*reg2 = (MEA_Uint16)(key->layer3.IPv4.dst_ip & 0x0000ffff);
		*reg3 = (MEA_Uint16)((key->layer3.IPv4.dst_ip >> 16) & 0x0000ffff);
		break;
	case MEA_FILTER_KEY_TYPE_SRC_IPV4_DST_PORT:
		*reg1 = (MEA_Uint16)(key->layer4.dst_port);
		*reg2 = (MEA_Uint16)(key->layer3.IPv4.src_ip & 0x0000ffff);
		*reg3 = (MEA_Uint16)((key->layer3.IPv4.src_ip >> 16) & 0x0000ffff);
		break;
	case MEA_FILTER_KEY_TYPE_DST_IPV4_SRC_PORT:
		*reg1 = (MEA_Uint16)(key->layer4.src_port);
		*reg2 = (MEA_Uint16)(key->layer3.IPv4.dst_ip & 0x0000ffff);
		*reg3 = (MEA_Uint16)((key->layer3.IPv4.dst_ip >> 16) & 0x0000ffff);
		break;
	case MEA_FILTER_KEY_TYPE_SRC_IPV4_SRC_PORT:	
		*reg1 = (MEA_Uint16)(key->layer4.src_port);
		*reg2 = (MEA_Uint16)(key->layer3.IPv4.src_ip & 0x0000ffff);
		*reg3 = (MEA_Uint16)((key->layer3.IPv4.src_ip >> 16) & 0x0000ffff);
		break;
	case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG:
		*reg1  = (MEA_Uint16)(key->layer2.outer_vlanTag.vlan);
		*reg1 |= (MEA_Uint16)(key->layer2.outer_vlanTag.cf) << 12;
		*reg1 |= (MEA_Uint16)(key->layer2.outer_vlanTag.priority) << 13;
		*reg2  = (MEA_Uint16)(key->layer2.inner_vlanTag.ethertype);
		*reg3  = (MEA_Uint16)(key->layer2.outer_vlanTag.ethertype);
		break;
	case MEA_FILTER_KEY_TYPE_SRC_IPV4_APPL_ETHERTYPE:
		*reg1 = (MEA_Uint16)(key->layer3.ethertype3);

		*reg2 = (MEA_Uint16)(key->layer3.IPv4.src_ip & 0x0000ffff);
		*reg3 = (MEA_Uint16)((key->layer3.IPv4.src_ip >> 16) & 0x0000ffff);
		break;
	case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT:
		*reg1 = (MEA_Uint16)(key->layer4.dst_port);
		*reg2 = (MEA_Uint16)(key->layer3.ethertype3);
		
		    *reg3 = (MEA_Uint16)(key->layer3.ip_protocol);
            if(key->layer2.outer_vlanTag.priority_valid == MEA_TRUE){
                *reg3|= (MEA_Uint16)(key->layer2.outer_vlanTag.priority)<<8;
                *reg3|= MEA_FILTER_PRI_TYPE_MASK_L2_PRI_OUTER;
            }else if(key->layer2.inner_vlanTag.priority_valid == MEA_TRUE){
                *reg3|= (MEA_Uint16)(key->layer2.inner_vlanTag.priority)<<8;
                *reg3|= MEA_FILTER_PRI_TYPE_MASK_L2_PRI_INNER;
            } else if(key->layer3.l3_pri_valid){
                switch(key->layer3.l3_pri_type){
                    case MEA_FILTER_L3_TYPE_IPV4_TOS :
                        *reg3|= (MEA_Uint16)(key->layer3.l3_pri_value.ipv4_tos.tos)<<8;
                        *reg3|= MEA_FILTER_PRI_TYPE_MASK_L3_TOS;
                        break;
                    case MEA_FILTER_L3_TYPE_IPV4_PRECEDENCE:
                        *reg3|= (MEA_Uint16)(key->layer3.l3_pri_value.ipv4_precedence.precedence)<<8;
                        *reg3|= MEA_FILTER_PRI_TYPE_MASK_L3_PRECEDENCE; 
                        break;
                    case MEA_FILTER_L3_TYPE_IPV4_DSCP:
                        *reg3|= (MEA_Uint16)(key->layer3.l3_pri_value.ipv4_dscp.dscp)<<8;
                        *reg3|= MEA_FILTER_PRI_TYPE_MASK_L3_DSCP;
                        break;
                   case MEA_FILTER_L3_TYPE_IPV6_TC:
                    case MEA_FILTER_L3_TYPE_LAST:
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - filter get raw key  failed\n",
                                 __FUNCTION__);
                        return MEA_ERROR;
                        break;
                    default:
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - filter get raw key  failed\n",
                                 __FUNCTION__);
                        return MEA_ERROR;
                        break;
                }
            }

      break;
	case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_SRC_PORT_DST_PORT:
		*reg1 = (MEA_Uint16)(key->layer4.dst_port);
		*reg2 = (MEA_Uint16)(key->layer4.src_port);
		*reg3 = (MEA_Uint16)(key->layer3.ip_protocol);
        if(key->layer2.outer_vlanTag.priority_valid == MEA_TRUE){
                *reg3|= (MEA_Uint16)(key->layer2.outer_vlanTag.priority)<<8;
                *reg3|= MEA_FILTER_PRI_TYPE_MASK_L2_PRI_OUTER;
            }else if(key->layer2.inner_vlanTag.priority_valid == MEA_TRUE){
                *reg3|= (MEA_Uint16)(key->layer2.inner_vlanTag.priority)<<8;
                *reg3|= MEA_FILTER_PRI_TYPE_MASK_L2_PRI_INNER;
            } else if(key->layer3.l3_pri_valid){
                switch(key->layer3.l3_pri_type){
                    case MEA_FILTER_L3_TYPE_IPV4_TOS :
                        *reg3|= (MEA_Uint16)(key->layer3.l3_pri_value.ipv4_tos.tos)<<8;
                        *reg3|= MEA_FILTER_PRI_TYPE_MASK_L3_TOS;
                        break;
                    case MEA_FILTER_L3_TYPE_IPV4_PRECEDENCE:
                        *reg3|= (MEA_Uint16)(key->layer3.l3_pri_value.ipv4_precedence.precedence)<<8;
                        *reg3|= MEA_FILTER_PRI_TYPE_MASK_L3_PRECEDENCE; 
                        break;
                    case MEA_FILTER_L3_TYPE_IPV4_DSCP:
                        *reg3|= (MEA_Uint16)(key->layer3.l3_pri_value.ipv4_dscp.dscp)<<8;
                        *reg3|= MEA_FILTER_PRI_TYPE_MASK_L3_DSCP;
                        break;
                   case MEA_FILTER_L3_TYPE_IPV6_TC:
                    case MEA_FILTER_L3_TYPE_LAST:
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - filter get raw key  failed\n",
                                 __FUNCTION__);
                        return MEA_ERROR;
                        break;
                    default:
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - filter get raw key  failed\n",
                                 __FUNCTION__);
                        return MEA_ERROR;
                        break;
                }
            }
		break;
	case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_INNER_VLAN_TAG_OUTER_ETHERTYPE:
		*reg1  = (MEA_Uint16)(key->layer2.outer_vlanTag.ethertype);
        *reg2  = (MEA_Uint16)(key->layer2.inner_vlanTag.vlan);
		*reg2 |= (MEA_Uint16)(key->layer2.inner_vlanTag.cf) << 12;
		*reg2 |= (MEA_Uint16)(key->layer2.inner_vlanTag.priority) << 13;

        *reg3  = (MEA_Uint16)(key->layer2.outer_vlanTag.vlan);
		*reg3 |= (MEA_Uint16)(key->layer2.outer_vlanTag.cf) << 12;
		*reg3 |= (MEA_Uint16)(key->layer2.outer_vlanTag.priority) << 13;

		break;
	case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_DST_PORT_SRC_PORT:
		*reg1 = (MEA_Uint16)(key->layer4.src_port);
		*reg2 = (MEA_Uint16)(key->layer4.dst_port);

		*reg3 = (MEA_Uint16)(key->layer2.outer_vlanTag.vlan);
		*reg3 |= (MEA_Uint16)(key->layer2.outer_vlanTag.cf) << 12;
		*reg3 |= (MEA_Uint16)(key->layer2.outer_vlanTag.priority) << 13;
		break;
	case MEA_FILTER_KEY_TYPE_INNER_VLAN_TAG_DST_PORT_SRC_PORT:
		*reg1 = (MEA_Uint16)(key->layer4.src_port);
		*reg2 = (MEA_Uint16)(key->layer4.dst_port);

		*reg3 = (MEA_Uint16)(key->layer2.inner_vlanTag.vlan);
		*reg3 |= (MEA_Uint16)(key->layer2.inner_vlanTag.cf) << 12;
		*reg3 |= (MEA_Uint16)(key->layer2.inner_vlanTag.priority) << 13;
		break;
	case MEA_FILTER_KEY_TYPE_PPPOE_SESSION_ID_PPP_PROTO_SA_MAC_LSB:
#if 0		
        *reg1 = (MEA_Uint16)(key->layer2.ppp.pppoe_session_id_from);
		*reg2 = (MEA_Uint16)(key->layer2.ppp.ppp_protocol);
		*reg3 = key->layer2.ethernet.SA.a.lss;
#else
        *reg1 |= key->layer2.ethernet.SA.b[5];
        *reg1 |= key->layer2.ethernet.SA.b[4]<<8;
        *reg2 = (MEA_Uint16)(key->layer2.ppp.ppp_protocol);
        *reg3 = (MEA_Uint16)(key->layer2.ppp.pppoe_session_id_from);

#endif
		break;
    case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_APPL_ETHERTYPE_OUTER_VLAN_TAG:
        *reg1 = (MEA_Uint16)(key->layer2.outer_vlanTag.vlan);
		*reg1 |= (MEA_Uint16)(key->layer2.outer_vlanTag.cf) << 12;
		*reg1 |= (MEA_Uint16)(key->layer2.outer_vlanTag.priority) << 13;

		*reg2 = (MEA_Uint16)(key->layer3.ethertype3);
        
        *reg3 = (MEA_Uint16)(key->layer2.outer_vlanTag.ethertype);

       break;

	case MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4:
		*reg1 = (MEA_Uint16)(key->layer2.outer_vlanTag.vlan);
		*reg1 |= (MEA_Uint16)(key->layer2.outer_vlanTag.cf) << 12;
		*reg1 |= (MEA_Uint16)(key->layer2.outer_vlanTag.priority) << 13;
		*reg2 = (MEA_Uint16)(key->layer3.IPv4.dst_ip & 0x0000ffff);
		*reg3 = (MEA_Uint16)((key->layer3.IPv4.dst_ip >> 16) & 0x0000ffff);
		break;

    case MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT:
        sigature = 0;
        mea_drv_filter_ipv6_sigature_function(&key->layer3.IPv6.dst_ipv6[0], &sigature);

        *reg1 = (MEA_Uint16)(key->layer4.dst_port);
        *reg2 = (MEA_Uint16)(sigature & 0x0000ffff);
        *reg3 = (MEA_Uint16)((sigature >> 16) & 0x0000ffff);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT:
        sigature = 0;
        mea_drv_filter_ipv6_sigature_function(&key->layer3.IPv6.src_ipv6[0], &sigature);
        *reg1 = (MEA_Uint16)(key->layer4.dst_port);
        *reg2 = (MEA_Uint16)(sigature & 0x0000ffff);
        *reg3 = (MEA_Uint16)((sigature >> 16) & 0x0000ffff);
        break;
    case MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT:
        sigature = 0;
        mea_drv_filter_ipv6_sigature_function(&key->layer3.IPv6.dst_ipv6[0], &sigature);
        *reg1 = (MEA_Uint16)(key->layer4.src_port);
        *reg2 = (MEA_Uint16)(sigature & 0x0000ffff);
        *reg3 = (MEA_Uint16)((sigature >> 16) & 0x0000ffff);
        break;

    case MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT:
        sigature = 0;
        mea_drv_filter_ipv6_sigature_function(&key->layer3.IPv6.src_ipv6[0], &sigature);
        *reg1 = (MEA_Uint16)(key->layer4.src_port);
        *reg2 = (MEA_Uint16)(sigature & 0x0000ffff);
        *reg3 = (MEA_Uint16)((sigature >> 16) & 0x0000ffff);

        


        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE:
        sigature = 0;
        mea_drv_filter_ipv6_sigature_function(&key->layer3.IPv6.src_ipv6[0], &sigature);
        *reg1 = (MEA_Uint16)(key->layer3.ethertype3);

        *reg2 = (MEA_Uint16)(sigature & 0x0000ffff);
        *reg3 = (MEA_Uint16)((sigature >> 16) & 0x0000ffff);
        break;



	default:
		break;
	}

	return MEA_OK;
}




static void mea_FilterHash_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
	//MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t          hwId_i = (MEA_db_HwId_t)((long)arg3);      /* contextId*/
    MEA_Filter_Entry_dbt  *entry_pi = (MEA_Filter_Entry_dbt *)arg4;
	MEA_Uint32             val[3];
	MEA_Uint32             i;
	MEA_Uint16             num_of_regs;
	MEA_Uint16 field[3];
	MEA_Filter_Key_Type_te key_type;
    MEA_Uint32 setKey_type;

	num_of_regs = MEA_NUM_OF_ELEMENTS(val);
	for (i=0;i<num_of_regs;i++) {
		val[i] = 0;
	}


	if ( mea_filter_get_raw_key( &(entry_pi->key),
								 &(field[0]), 
								 &(field[1]), 
								 &(field[2]),
								 &key_type ) != MEA_OK )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_filter_get_raw_key failed, id=%d\n",
                          __FUNCTION__, hwId_i);
	}

	val[0]  = (((MEA_Uint32)field[0]
              << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_WORD_0_FIELD0_MASK)
             ) & MEA_IF_FILTER_WORD_0_FIELD0_MASK
            ); 

	val[0]  |= (((MEA_Uint32)field[1]
              << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_WORD_0_FIELD1_MASK)
             ) & MEA_IF_FILTER_WORD_0_FIELD1_MASK
            ); 

	val[1]  = (((MEA_Uint32)field[2]
              << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_WORD_1_FIELD2_MASK)
             ) & MEA_IF_FILTER_WORD_1_FIELD2_MASK
            ); 

    switch (key_type)
    {

    case  MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT :
    case  MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT:
    case  MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT:
    case  MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT:
    case  MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE:
        setKey_type = key_type - 20;
    	break;

    default:
        setKey_type = key_type;
        break;
    }

	val[1]  |= (((MEA_Uint32)setKey_type  /*key_type*/
              << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_WORD_1_KEY_TYPE_MASK)
             ) & MEA_IF_FILTER_WORD_1_KEY_TYPE_MASK
            ); 


    if(entry_pi->key.interface_key.src_port_valid){
	val[1]  |= (((MEA_Uint32)entry_pi->key.interface_key.src_port
              << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_WORD_1_SRC_PORT_SID_MASK)
             ) & MEA_IF_FILTER_WORD_1_SRC_PORT_SID_MASK ); 
    }else {
        if(entry_pi->key.interface_key.sid_valid){
            val[1]  |= (((MEA_Uint32)entry_pi->key.interface_key.acl_prof
              << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_WORD_1_SRC_PORT_SID_MASK)
             ) & MEA_IF_FILTER_WORD_1_SRC_PORT_SID_MASK);
        }
    }

	if (key_type == MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4) 
    {
#if 0 /* no need to set this bit */
		if (MEA_IF_FILTER_WORD_1_DA_MASK != 0) {

            val[1] |= (((MEA_Uint32)entry_pi->key.layer2.ethernet.DA_valid
                << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_WORD_1_DA_MASK)
                ) & MEA_IF_FILTER_WORD_1_DA_MASK
                );



		}
#endif

	}

#ifdef MEA_ACL_IPV6_SUPPORT
#endif

	if ( entry_pi->valid )
	{
		
		
		if (MEA_IF_FILTER_WORD_2_FILTER_ID_MASK != 0) {
			val[2]   = (((MEA_Uint32)hwId_i
					  << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_WORD_2_FILTER_ID_MASK)
					 ) & MEA_IF_FILTER_WORD_2_FILTER_ID_MASK
					); 
		}
	}
	else
	{
		
		val[2] = 0;
	}






	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_SRV_MAKE0_REG , val[0] , MEA_MODULE_IF);
	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_SRV_MAKE1_REG , val[1] , MEA_MODULE_IF);
	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_SRV_MAKE2_REG , val[2] , MEA_MODULE_IF);

// 	if (mea_db_Set_Hw_Regs(unit_i,
// 		                   MEA_Filter_db,
//         				   hwUnit_i,
// 						   hwId_i,
// 						   num_of_regs,
// 						   val) != MEA_OK) {
// 		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
// 			              "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
// 						  __FUNCTION__,
// 						  hwUnit_i,
// 						  hwId_i);
//    }
}



static MEA_Status mea_drv_Filter_UpdateHw(MEA_Unit_t unit_i,
										  MEA_Filter_Entry_dbt *filter_entry,
										  MEA_Filter_t          id)
{

	MEA_ind_write_t ind_write;
	MEA_db_HwUnit_t hwUnit;
//	MEA_db_HwId_t   hwId;
    MEA_Status      status;
    
    MEA_Uint32 collision_evt;

#if MEA_FILTER_DISABLE_HW 
    return MEA_OK;  
#endif

    MEA_DRV_GET_HW_UNIT(unit_i, hwUnit, return MEA_FALSE);

	/* Write to HW Filter Context Table */
	ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_FILTER_CTX;
    ind_write.tableOffset = filter_entry->filter_CtxId;

	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_FilterCtx_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)filter_entry->filter_CtxId);
    ind_write.funcParam4 = (MEA_funcParam)filter_entry;

	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed, id %d\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF, id);
		return MEA_ERROR;
    }


// 	MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,
// 		                          mea_drv_Get_Filter_db,
// 								  id,
// 								  hwUnit,
// 								  hwId,
// 								  return MEA_ERROR);


    /* build the ind_write value */
	ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_99_HASH;/* not relevant - filter hash table */
	ind_write.tableOffset	= 0; /* not relevant - filter hash table */
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
    ind_write.cmdMask       = MEA_IF_CMD_FILTER_MAKE_REG_MASK | MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(filter_entry->key.Acl_hierarchical_id);
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK; //MEA_IF_STATUS_FILTER_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG_IGNORE; /* filter hash table */
	ind_write.tableAddrMask	= 0;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_FilterHash_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)filter_entry->filter_CtxId); /*context */
    ind_write.funcParam4 = (MEA_funcParam)filter_entry;

    MEA_API_WriteReg(unit_i, ENET_IF_HASH_EVENT_REG, 0, MEA_MODULE_IF); /*ClearEvent*/
	
    /* Write to the hash table as Indirect Table without the table type and entry # */
	status =MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF);
    
    collision_evt = MEA_API_ReadReg(unit_i, ENET_IF_HASH_EVENT_REG, MEA_MODULE_IF);


    collision_evt = (collision_evt & MEA_IF_COLL_HASH_EVENT_FILTER_MASK); /*MASK the relevant group*/
    if ((collision_evt & (1 << (MEA_IF_COLL_HASH_EVENT_FILTER_GROUP + filter_entry->key.Acl_hierarchical_id)))) {
        status = MEA_COLLISION;
    }

    if(status != MEA_OK || status == MEA_COLLISION) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (id=%d)\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF,id);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - status indicate collision , "
            "can not insert this filter \n",
            __FUNCTION__);

		return MEA_ERROR;
    }

    if (mea_filter_get_key_type(&(filter_entry->key)) ==
		MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4 && filter_entry->key.layer2.ethernet.DA_valid == MEA_TRUE) {
		MEA_Uint32 val;
  	    val  = ((MEA_Uint32)(filter_entry->key.layer2.ethernet.DA.s[2])      );
		val |= ((MEA_Uint32)(filter_entry->key.layer2.ethernet.DA.s[1]) << 16);
		MEA_API_WriteReg(MEA_UNIT_0,
						 MEA_IF_GLOBAL_FILTER_DA_MAC_31_0_REG,
			             val,
						 MEA_MODULE_IF);
  	    val  = ((MEA_Uint32)(filter_entry->key.layer2.ethernet.DA.s[0])      );
		MEA_API_WriteReg(MEA_UNIT_0,
			             MEA_IF_GLOBAL_FILTER_DA_MAC_47_32_REG,
						 val,
						 MEA_MODULE_IF);
	}
#ifdef MEA_ACL_IPV6_SUPPORT

#endif    
    return MEA_OK;
}


MEA_Status mea_drv_Flush_HW_Filter_Table(void)
{


    MEA_Globals_Entry_dbt entry;
    MEA_Uint32 status;

    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    
    if( (status & MEA_IF_STATUS_FILTER_BUSY_MASK) == MEA_IF_STATUS_FILTER_BUSY_MASK )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - status command busy is set\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_Globals failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
 
	entry.if_global1.val.acl_table_flush = 1;

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_Globals (1) failed  \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }


    status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    while ( (status & MEA_IF_STATUS_BUSY_MASK) )
    {
         MEA_OS_sleep(0, 1000 ); /* sleep for 1 microsecond */

         status = MEA_API_ReadReg(MEA_UNIT_0, MEA_IF_STATUS_REG, MEA_MODULE_IF);
    }


    entry.if_global1.val.acl_table_flush = 0;

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Set_Globals (0) failed  \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }



    return MEA_OK;

}

MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Filter_num_of_sw_size_func,
										 (MEA_Filter_t)MEA_MAX_NUM_OF_FILTER_ENTRIES,
										 0)

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Filter_num_of_hw_size_func,
										 ENET_IF_CMD_PARAM_TBL_TYP_FILTER_CTX_LENGTH(hwUnit_i),
                                         3)



MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Filter_Ctx_num_of_sw_size_func,
                                        MEA_MAX_NUM_OF_FILTER_CONTEXT, //(MEA_Filter_t)MEA_MAX_NUM_OF_FILTER_ENTRIES,
										 0)

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Filter_Ctx_num_of_hw_size_func,
                                         ENET_IF_CMD_PARAM_TBL_TYP_FILTER_CTX_LENGTH(hwUnit_i),
										 ENET_IF_CMD_PARAM_TBL_TYP_FILTER_CTX_NUM_OF_REGS(hwUnit_i))








MEA_Status mea_drv_Conclude_Filter_Table(MEA_Unit_t unit_i) {

	

	if (MEA_Filter_Mask_Prof_Table) {
		MEA_OS_free(MEA_Filter_Mask_Prof_Table);
	    MEA_Filter_Mask_Prof_Table=NULL;
	}



	if (MEA_drv_Filter_Globals) {
		MEA_OS_free(MEA_drv_Filter_Globals);
		MEA_drv_Filter_Globals=NULL;
	}

	if (MEA_drv_Filter_Tbl) {
		MEA_OS_free(MEA_drv_Filter_Tbl);
		MEA_drv_Filter_Tbl=NULL;
	}

    if (MEA_drv_Filter_ContextTbl) {
        MEA_OS_free(MEA_drv_Filter_ContextTbl);
        MEA_drv_Filter_ContextTbl = NULL;
    }

    if (MEA_drv_Filter_ActionTbl) {
        MEA_OS_free(MEA_drv_Filter_ActionTbl);
        MEA_drv_Filter_ActionTbl = NULL;
    }



	return MEA_OK;
}


MEA_Status mea_drv_Init_Filter_Table(MEA_Unit_t unit_i)
{

	MEA_Uint32 size;
    if(b_bist_test){
        return MEA_OK;
    }
    if(MEA_ACL_SUPPORT == MEA_FALSE){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF (ACL) Filter Not support ...\n");
     return MEA_OK;
    }
    /* Init IF Filter Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Filter table ...\n");
	/* Init db - Hardware shadow */





	size = mea_drv_num_of_hw_units * sizeof(MEA_drv_Filter_Globals_dbt);
	MEA_drv_Filter_Globals = (MEA_drv_Filter_Globals_dbt*)MEA_OS_malloc(size);
	if (MEA_drv_Filter_Globals == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - Allocate MEA_drv_Filter_Globals failed (size=%d)\n",
						  __FUNCTION__,size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_drv_Filter_Globals,0,size);


	size = MEA_MAX_NUM_OF_FILTER_ENTRIES * sizeof(MEA_Filter_Entry_dbt);
	MEA_drv_Filter_Tbl = (MEA_Filter_Entry_dbt*)MEA_OS_malloc(size);
	if (MEA_drv_Filter_Tbl == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - Allocate MEA_drv_Filter_Tbl failed (size=%d)\n",
						  __FUNCTION__,size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_drv_Filter_Tbl,0,size);


    size = MEA_MAX_NUM_OF_FILTER_CONTEXT * sizeof(MEA_Filter_Context_dbt);
    MEA_drv_Filter_ContextTbl = (MEA_Filter_Context_dbt*)MEA_OS_malloc(size);
    if (MEA_drv_Filter_ContextTbl == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_drv_Filter_ContextTbl failed (size=%d)\n",
            __FUNCTION__, size);
        return MEA_ERROR;
    }
    MEA_OS_memset((char*)MEA_drv_Filter_ContextTbl, 0, size);



    size = MEA_MAX_NUM_OF_FILTER_ACTION_ENTRIES * sizeof(MEA_Filter_Action_dbt);
    MEA_drv_Filter_ActionTbl = (MEA_Filter_Action_dbt*)MEA_OS_malloc(size);
    if (MEA_drv_Filter_ActionTbl == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_drv_Filter_ActionTbl failed (size=%d)\n",
            __FUNCTION__, size);
        return MEA_ERROR;
    }
    MEA_OS_memset((char*)MEA_drv_Filter_ActionTbl, 0, size);

    MEA_OS_memset(&MEA_drv_Filter_temp[0], 0, sizeof(MEA_drv_Filter_temp));

    



    if (mea_drv_Flush_HW_Filter_Table() != MEA_OK) {
		MEA_OS_free(MEA_drv_Filter_Tbl);
		MEA_drv_Filter_Tbl=NULL;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Flush_HW_Filter_Table failed 1\n",
                         __FUNCTION__);
	    return MEA_ERROR;
     }



    /* Init Filter Mask Software shadow table */
    size = MEA_MAX_NUM_OF_FILTER_MASK_ID * sizeof(MEA_drv_filter_mask_profile_dbt);
    MEA_Filter_Mask_Prof_Table = (MEA_drv_filter_mask_profile_dbt*)MEA_OS_malloc(size);
	if (MEA_Filter_Mask_Prof_Table == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - Allocate MEA_Filter_Mask_Prof_Table failed (size=%d)\n",
						  __FUNCTION__,size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_Filter_Mask_Prof_Table,0,size);

    MEA_drv_Filter_Global_Sid_count = 0;

    MEA_OS_memset(&MEA_Counters_ACL_TBL, 0, sizeof(MEA_Counters_ACL_TBL));

    MEA_drv_Filter_default_Contex = 0;

    return MEA_OK; 
}



MEA_Status mea_drv_ReInit_Filter_Mask(MEA_Unit_t unit_i)
{
    MEA_FilterMask_t id_i;

    for (id_i = 0; id_i < MEA_MAX_NUM_OF_FILTER_MASK_ID; id_i++){
          if(MEA_Filter_Mask_Prof_Table[id_i].valid ==MEA_FALSE)
              continue;


          if (mea_drv_Filter_Mask_Prof_UpdateHw(unit_i, id_i, &MEA_Filter_Mask_Prof_Table[id_i].data) != MEA_OK) {

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Filter_mask_prof_UpdateHw failed\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
    }



    return MEA_OK;
}


MEA_Status mea_drv_ReInit_Filter_Table(MEA_Unit_t unit_i)
{

    MEA_db_HwUnit_t hwUnit;
	mea_memory_mode_e       save_globalMemoryMode;
	MEA_Uint32 val;
    MEA_Uint8* mac;
    MEA_Filter_t FilterId;

    if(MEA_ACL_SUPPORT == MEA_FALSE){
        return MEA_OK;
    }

    /* Flush the Filter table in the Hardware */
    if (mea_drv_Flush_HW_Filter_Table() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Flush_HW_Filter_Table failed 2\n",
                         __FUNCTION__);
	    return MEA_ERROR;
     }

    /* ReInit Filter Hash table   */
     /* ReInit Filter Ctx table   */
    for (FilterId = 1; FilterId < MEA_MAX_NUM_OF_FILTER_ENTRIES; FilterId++){
        if(MEA_drv_Filter_Tbl[FilterId].valid == MEA_FALSE)
            continue;
        mea_drv_Filter_UpdateHw(unit_i,&MEA_drv_Filter_Tbl[FilterId], FilterId);
    }



    /* ReInit Filter Mask table   */
     if (mea_drv_ReInit_Filter_Mask(unit_i) != MEA_OK)
    {

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_Filter_Mask failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }





    /* ReInit Global Filter DA Mac */
	for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
		MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        mac =(MEA_Uint8 *) &(MEA_drv_Filter_Globals[hwUnit].da[0]);
        val = (((MEA_Uint32)(mac[2]) << 24) |
               ((MEA_Uint32)(mac[3]) << 16) |
               ((MEA_Uint32)(mac[4]) <<  8) |
               ((MEA_Uint32)(mac[5]) <<  0) );
		MEA_API_WriteReg(unit_i,
						 MEA_IF_GLOBAL_FILTER_DA_MAC_31_0_REG,
			             val,
						 MEA_MODULE_IF);
        val = 0;
        val = (((MEA_Uint32)(mac[0]) <<  8) |
               ((MEA_Uint32)(mac[1]) <<  0) );
		MEA_API_WriteReg(MEA_UNIT_0,
			             MEA_IF_GLOBAL_FILTER_DA_MAC_47_32_REG,
						 val,
						 MEA_MODULE_IF);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }


    /* Return to Caller */
    return MEA_OK; 
}

static MEA_Status mea_drv_filter_GetFreeFilterId(MEA_Unit_t unit_i,MEA_Filter_t *filterId)
{
	for ( *filterId = 1; *filterId < MEA_MAX_NUM_OF_FILTER_ENTRIES; (*filterId)++ )
	{
		if ( MEA_drv_Filter_Tbl[*filterId].valid == MEA_FALSE )
		{
			MEA_drv_Filter_Tbl[*filterId].valid = MEA_TRUE;
			return MEA_OK;
		}
	}

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - no free Filter entry\n",
                      __FUNCTION__);
	return MEA_ERROR;
}

MEA_Status mea_drv_filter_GetFreeFilter_CtxId(MEA_Unit_t unit_i,MEA_Uint8 hieracId, MEA_Filter_t *filterId)
{
    MEA_Filter_t fromId, toId;

    if (hieracId >= MEA_ACL_HIERARC_NUM_OF_FLOW){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - no free Filter entry\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_ACL_HIERARCHICAL_SUPPORT == MEA_FALSE){
        fromId = 1;
        toId = MEA_MAX_NUM_OF_FILTER_ENTRIES;
    
    }
    else{
        fromId = (hieracId* MEA_MAX_NUM_OF_FILTER_ENTRIES) + 1; /*the 0 is not valid */
        toId = (hieracId* MEA_MAX_NUM_OF_FILTER_ENTRIES) + MEA_MAX_NUM_OF_FILTER_ENTRIES;
    }


    for (*filterId = fromId; *filterId < toId; (*filterId)++)
    {
        if (MEA_drv_Filter_ContextTbl[*filterId].valid == MEA_FALSE)
        {
            MEA_drv_Filter_ContextTbl[*filterId].valid = MEA_TRUE;
            return MEA_OK;
        }
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - no free Filter entry\n",
        __FUNCTION__);

    return MEA_ERROR;
}

static MEA_Status mea_drv_filter_GetFreeFilter_ActionId(MEA_Unit_t unit_i,MEA_Filter_t *Action_filterId)
{
    for (*Action_filterId = 1; *Action_filterId < MEA_MAX_NUM_OF_FILTER_ENTRIES; (*Action_filterId)++)
    {
        if (MEA_drv_Filter_ActionTbl[*Action_filterId].valid == MEA_FALSE)
        {
            MEA_drv_Filter_ActionTbl[*Action_filterId].valid = MEA_TRUE;
            return MEA_OK;
        }
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - no free Filter entry\n",
        __FUNCTION__);
    return MEA_ERROR;
}




static MEA_Status mea_drv_filter_AddOwnerFilter_ActionId(MEA_Unit_t unit_i,MEA_Filter_t Action_filterId)
{
    if (Action_filterId >= MEA_MAX_NUM_OF_FILTER_ENTRIES || Action_filterId < 1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Filter Action is out of range\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_drv_Filter_ActionTbl[Action_filterId].num_of_owners++;

    return MEA_OK;
}

static MEA_Status mea_drv_filter_DelOwnerFilter_ActionId(MEA_Unit_t unit_i, MEA_Filter_t Action_filterId)
{
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0, hwUnit, return MEA_FALSE);
    
    if (Action_filterId >= MEA_MAX_NUM_OF_FILTER_ENTRIES || Action_filterId < 1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Filter Action is out of range\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (MEA_drv_Filter_ActionTbl[Action_filterId].num_of_owners >= 1 && 
        MEA_drv_Filter_ActionTbl[Action_filterId].valid == MEA_TRUE)
    {
        
        MEA_drv_Filter_ActionTbl[Action_filterId].num_of_owners--;
        if (MEA_drv_Filter_ActionTbl[Action_filterId].num_of_owners == 0){

            MEA_drv_Filter_ActionTbl[Action_filterId].valid = MEA_FALSE;
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_FILTER);
			MEA_DRV_Delete_Action(unit_i, Action_filterId + MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit), MEA_ACTION_TYPE_FILTER);
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
        }
        
        return MEA_OK;
    }


    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - Filter ActionId %d is not valid\n",
        __FUNCTION__, Action_filterId);
    return MEA_ERROR;
}





MEA_Status mea_drv_filter_create_procedure(MEA_Unit_t                           unit_i,
										   MEA_Filter_t                         filter_id,
										   MEA_Filter_Key_dbt                   *key,
										   MEA_Filter_Data_dbt                  *data,
										   MEA_OutPorts_Entry_dbt               *OutPorts_Entry)
{
    ENET_xPermissionId_t           xpermission_id;
    MEA_Port_t                     port;
    MEA_Uint32 i,j,mask;
    MEA_Uint32* ptr1;
	MEA_OutPorts_Entry_dbt output;
    MEA_Filter_t filter_CtxId;


	MEA_drv_Filter_Tbl[filter_id].valid = MEA_TRUE;

	/* If action is TO_ACTION - handle action:
	      1. Set the ED,TM, PM. 
		  2. Create ACL action - ID similar to Filter Entry ID.*/



	xpermission_id = ENET_PLAT_GENERATE_NEW_ID;
   /* Create X-permission if needed.*/
	if ( data->action_params.output_ports_valid )
	{
		if ( OutPorts_Entry == NULL )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - output_ports_valid marked but OutPorts_Entry is NULL\n", __FUNCTION__);
			return MEA_ERROR;
		}

		for ( port = 0, i = 0 , 
			  ptr1 = &(OutPorts_Entry->out_ports_0_31);
			  i < sizeof(*OutPorts_Entry)/sizeof(OutPorts_Entry->out_ports_0_31);
			  i++,ptr1++ )
		{
			
			for (j=0,mask=1;j<32;j++,mask<<=1,port++) 
			{
				if ((*ptr1) & mask)
				{
				   if (ENET_IsValid_Queue(ENET_UNIT_0,port,ENET_TRUE) != ENET_TRUE) 
				   {
					   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
										 "%s - Port id %d is not define \n",
										 __FUNCTION__,port);
					   return MEA_ERROR;
				   }
				}
			}
			  }
        if (! MEA_API_Get_Warm_Restart()) 
		xpermission_id = ENET_PLAT_GENERATE_NEW_ID;
        else
          xpermission_id=  data->save_hw_xPermissionId;

		if (enet_Create_xPermission(ENET_UNIT_0,
									OutPorts_Entry,
                                    0,/*no cpu */
									&xpermission_id) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - enet_Create_xPermission failed \n", __FUNCTION__);
			return MEA_ERROR;
		}
         data->save_hw_xPermissionId = xpermission_id;
	}

	/* if action is DISCARD - create 0 xpermission. 
	   If action is CPU - create CPU xpermission.
    */
	if ( data->action_type == MEA_FILTER_ACTION_SEND_TO_CPU ||
		 data->action_type == MEA_FILTER_ACTION_DISCARD )
	{
		MEA_OS_memset(&output,0,sizeof(output));

		if ( data->action_type == MEA_FILTER_ACTION_SEND_TO_CPU )
		{
	        ((MEA_Uint32*)(&(output.out_ports_0_31)))[MEA_CPU_DEFAULT_CLUSTER/32] |=  
				(1 << (MEA_CPU_DEFAULT_CLUSTER%32));
		}
         if (! MEA_API_Get_Warm_Restart())
		    xpermission_id = ENET_PLAT_GENERATE_NEW_ID;
         else
            xpermission_id=  data->save_hw_xPermissionId;

		if (enet_Create_xPermission(ENET_UNIT_0,
									&output,
                                    1,/*to cpu */
									&xpermission_id) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - enet_Create_xPermission failed \n", __FUNCTION__);
			return MEA_ERROR;
		}
	      data->save_hw_xPermissionId=xpermission_id;
          //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "xpermission_id %d ", xpermission_id);
	}
	

	/* Set the SW image Filter Table */
	MEA_OS_memcpy(&(MEA_drv_Filter_Tbl[filter_id].data),
		          data,
				  sizeof(MEA_drv_Filter_Tbl[filter_id].data));
	MEA_OS_memcpy(&(MEA_drv_Filter_Tbl[filter_id].key),
		          key,
				  sizeof(MEA_drv_Filter_Tbl[filter_id].key));

    MEA_drv_Filter_Tbl[filter_id].xpermission_id = xpermission_id;
    
    filter_CtxId = MEA_drv_Filter_Tbl[filter_id].filter_CtxId;
    
    MEA_drv_Filter_ContextTbl[filter_CtxId].HIe_id = key->Acl_hierarchical_id;

    MEA_drv_Filter_ContextTbl[filter_CtxId].Xpermission_id      = xpermission_id;
    MEA_drv_Filter_ContextTbl[filter_CtxId].force_Xpermission   = data->action_params.output_ports_valid;
    MEA_drv_Filter_ContextTbl[filter_CtxId].force_action_en     = data->force_Action_en;
    MEA_drv_Filter_ContextTbl[filter_CtxId].lxcp_win            = data->lxcp_win;
    MEA_drv_Filter_ContextTbl[filter_CtxId].action_id           = data->Action_Id;
    MEA_drv_Filter_ContextTbl[filter_CtxId].DSE_mask_field_type = data->DSE_mask_field_type;
    MEA_drv_Filter_ContextTbl[filter_CtxId].fwd_Act_en          = data->fwd_Act_en;




	if ( data->action_params.output_ports_valid )
	{
		MEA_OS_memcpy(&(MEA_drv_Filter_Tbl[filter_id].out_ports), 
			          OutPorts_Entry, 
					  sizeof(MEA_drv_Filter_Tbl[filter_id].out_ports));
	}

	/* Set the Filter HW */
    if (mea_drv_Filter_UpdateHw(unit_i,&(MEA_drv_Filter_Tbl[filter_id]),filter_id) != MEA_OK )
	{
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s - mea_drv_Filter_UpdateHw failed (filter_id=%d) \n",
                           __FUNCTION__,filter_id);
        return MEA_ERROR;
    }

	return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Create_Filter                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_Filter(MEA_Unit_t                           unit_i,
    MEA_Filter_Key_dbt                   *key_from,
    MEA_Filter_Key_dbt                   *key_to,
    MEA_Filter_Data_dbt                  *data,
    MEA_OutPorts_Entry_dbt               *OutPorts_Entry,
    MEA_Policer_Entry_dbt                *Policer_Entry,
    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry,
    MEA_Filter_t                         *o_filterId)
{
    MEA_Bool exist;
    MEA_Filter_t filterId;
    MEA_Filter_t filter_CtxId = 0;
    MEA_Filter_t filter_actionId = 0;
    MEA_Bool     New_Action = MEA_FALSE;
    MEA_Bool Act_exist = MEA_FALSE;

    MEA_Action_Entry_Data_dbt Action_Data;
    MEA_Action_t              Action_Id;
    mea_memory_mode_e         save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit;
//    MEA_db_HwId_t   hwId;
   

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (key_from == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - key_from == NULL \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

    if (key_to != NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key_to must be NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if ((key_from->interface_key.src_port_valid == MEA_FALSE) &&
        (key_from->interface_key.sid_valid == MEA_FALSE))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - filter key one of source port or sid valid  must be valid\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if ((key_from->interface_key.src_port_valid == MEA_TRUE) &&
        (key_from->interface_key.sid_valid == MEA_TRUE))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - filter key only one of source port or sid valid must be set\n",
            __FUNCTION__);
        return MEA_ERROR;
    }






#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (MEA_ACL_HIERARCHICAL_SUPPORT == MEA_FALSE){
        if (key_from->Acl_hierarchical_id != 0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - interface_key only hierarchical Zero support\n",
                __FUNCTION__, globalMemoryMode);
            return MEA_ERROR;
        }
    }

    


    hwUnit = mea_Filter_GetHwUnit(unit_i, key_from);
    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode, hwUnit);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (data == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data == NULL \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (o_filterId == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - o_filterId == NULL \n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
#if 0	
    if ( data->action_params.output_ports_valid && 
        !(data->action_type == MEA_FILTER_ACTION_TO_ACTION) )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data action output valid is set but action is not TO_ACTION\n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
#endif

    // 	if ( data->action_type == MEA_FILTER_ACTION_TO_ACTION &&
    // 		 data->action_params.output_ports_valid && 
    // 		 OutPorts_Entry == NULL )
    // 	{
    //        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
    //                          "%s - data action output valid is set but OutPorts_Entry is NULL\n",
    //                          __FUNCTION__);
    //        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    //        return MEA_ERROR;
    //     }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (MEA_API_IsExist_Filter_ByKey(unit_i,
        key_from,
        key_to,
        &exist,
        &filterId) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_IsExist_Filter_ByKey failed\n",
            __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    if (exist)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Filter with that key exist with ID %d\n",
            __FUNCTION__, filterId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }



    if (data->force_Action_en == MEA_TRUE){
        /*check if the Action Id is create */
        if (data->Action_Id < 1 && data->Action_Id < MEA_MAX_NUM_OF_FILTER_ENTRIES){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "actionId=%d  is not on range up to %d\n",
                data->Action_Id, MEA_MAX_NUM_OF_FILTER_ENTRIES);
            return MEA_ERROR;

        }

        Action_Id = data->Action_Id + MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit);
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_FILTER);
        if (MEA_API_IsExist_Action(unit_i, Action_Id, &Act_exist) != MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "MEA_API_IsExist_Action  actionId=%d  is not on range\n",
                data->Action_Id);
            return MEA_ERROR;
        }
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
        if (Act_exist == MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "MEA_API_IsExist_Action  actionId=%d  is not valid\n",
                data->Action_Id);
            return MEA_ERROR;
        }
        filter_actionId = data->Action_Id;

    }







    if (mea_drv_filter_GetFreeFilterId(unit_i,&filterId) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_filter_GetFreeFilterId failed \n", __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    /*return real id*/
    if (mea_drv_filter_GetFreeFilter_CtxId(unit_i,key_from->Acl_hierarchical_id, &filter_CtxId) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_filter_GetFreeFilterId failed \n", __FUNCTION__);
        MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]), 0, sizeof(MEA_drv_Filter_Tbl[filterId]));
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }



    if (data->force_Action_en == MEA_FALSE){
        New_Action = MEA_TRUE;
        if (mea_drv_filter_GetFreeFilter_ActionId(unit_i,&filter_actionId) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_filter_GetFreeFilter_ActionId failed \n", __FUNCTION__);
            MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]), 0, sizeof(MEA_drv_Filter_Tbl[filterId]));
            MEA_OS_memset(&(MEA_drv_Filter_ContextTbl[filter_CtxId]), 0, sizeof(MEA_drv_Filter_ContextTbl[filter_CtxId]));
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;


        }
        data->Action_Id = filter_actionId;
    }




 






    if ((data->force_Action_en == MEA_FALSE) ){

        /* Create the Action to CPU */
        if (data->action_type == MEA_FILTER_ACTION_SEND_TO_CPU) {
            if (MEA_API_Get_Action(unit_i,
                MEA_ACTION_ID_TO_CPU,
                &Action_Data,
                NULL, /* OutPorts */
                NULL, /* Policer */
                NULL  /* Editing */
                ) != MEA_OK) {

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_Get_Action failed (actionId=%d)\n",
                    __FUNCTION__, MEA_ACTION_ID_TO_CPU);
                MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]), 0, sizeof(MEA_drv_Filter_Tbl[filterId]));
                MEA_OS_memset(&(MEA_drv_Filter_ContextTbl[filter_CtxId]), 0, sizeof(MEA_drv_Filter_ContextTbl[filter_CtxId]));
                
                if (New_Action)
                    MEA_OS_memset(&(MEA_drv_Filter_ActionTbl[filter_actionId]), 0, sizeof(MEA_drv_Filter_ActionTbl[filter_actionId]));
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }



            Action_Id = (MEA_Action_t)filter_actionId + MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit);
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_FILTER);
			Action_Data.Action_type = MEA_ACTION_TYPE_FILTER;
            if (MEA_API_Create_Action(unit_i,
                &Action_Data,
                NULL,
                NULL,
                NULL,
                &Action_Id) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s:%d - MEA_API_Create_Action failed (actionId=%d)\n",
                    __FUNCTION__, __LINE__, Action_Id);
                //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
                MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]), 0, sizeof(MEA_drv_Filter_Tbl[filterId]));
                MEA_OS_memset(&(MEA_drv_Filter_ContextTbl[filter_CtxId]), 0, sizeof(MEA_drv_Filter_ContextTbl[filter_CtxId]));
               
                if (New_Action)
                    MEA_OS_memset(&(MEA_drv_Filter_ActionTbl[filter_actionId]), 0, sizeof(MEA_drv_Filter_ActionTbl[filter_actionId]));

                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
        }


        /* Create the Action Discard */
        if (data->action_type == MEA_FILTER_ACTION_DISCARD) {
            MEA_OS_memset((char*)(&Action_Data), 0, sizeof(Action_Data));
            Action_Id = (MEA_Action_t)filter_actionId + MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit);
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_FILTER);
			Action_Data.Action_type = MEA_ACTION_TYPE_FILTER;
            if (MEA_API_Create_Action(unit_i,
                &Action_Data,
                NULL,
                NULL,
                NULL,
                &Action_Id) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s:%d - MEA_API_Create_Action failed (actionId=%d)\n",
                    __FUNCTION__, __LINE__, Action_Id);
                MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]), 0, sizeof(MEA_drv_Filter_Tbl[filterId]));
                MEA_OS_memset(&(MEA_drv_Filter_ContextTbl[filter_CtxId]), 0, sizeof(MEA_drv_Filter_ContextTbl[filter_CtxId]));
                
                if (New_Action)
                    MEA_OS_memset(&(MEA_drv_Filter_ActionTbl[filter_actionId]), 0, sizeof(MEA_drv_Filter_ActionTbl[filter_actionId]));
                //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);

                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
        }


        /* Create Action to action */
        if (data->action_type == MEA_FILTER_ACTION_TO_ACTION) {
            MEA_Bool   llc;
            MEA_Uint32 protocol;

            llc = data->action_params.Llc;
            protocol = data->action_params.Protocol;

            if ((data->action_params.proto_llc_valid == MEA_FALSE) &&
                (data->action_params.protocol_llc_force == MEA_FALSE)) {
                if ((data->action_params.ed_id_valid) &&
                    (EHP_Entry != NULL) &&
                    (EHP_Entry->num_of_entries == 1) &&
                    (EHP_Entry->ehp_info[0].ehp_data.eth_info.command ==
                    MEA_EGRESS_HEADER_PROC_CMD_SWAP_MAC_AND_IP_ANS_SET_TTL)) {
                    llc = MEA_TRUE; /* SWAP MAC + IP */
                    protocol = MEA_ACTION_PROTOCOL_ETH_TO_ETH_VLAN;
                }
                else {
                    if (key_from->interface_key.sid_valid != MEA_TRUE)
                    {
                        if (mea_Calc_ProtocolAndLlc(unit_i,
                            key_from->interface_key.src_port,
                            OutPorts_Entry,
                            &protocol,
                            &llc) != MEA_OK) 
                        {
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - mea_Calc_ProtocolAndLlc failed \n",
                                __FUNCTION__);
                            MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]), 0, sizeof(MEA_drv_Filter_Tbl[filterId]));
                            MEA_OS_memset(&(MEA_drv_Filter_ContextTbl[filter_CtxId]), 0, sizeof(MEA_drv_Filter_ContextTbl[filter_CtxId]));
                            
                            if (New_Action)
                                MEA_OS_memset(&(MEA_drv_Filter_ActionTbl[filter_actionId]), 0, sizeof(MEA_drv_Filter_ActionTbl[filter_actionId]));

                            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                            return MEA_ERROR;
                        }
                    }
                    else{
                        protocol = 1;
                        llc = 0;
                    }
                }
            }

            MEA_OS_memcpy((char*)&Action_Data,(char*)&(data->action_params),sizeof(Action_Data));

            //Action_Data.Protocol = protocol;
            //Action_Data.Llc = llc;

            Action_Id = (MEA_Action_t)filter_actionId + MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit);
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_FILTER);
			Action_Data.Action_type = MEA_ACTION_TYPE_FILTER;
            if (MEA_API_Create_Action(unit_i,
                &Action_Data,
                OutPorts_Entry,
                Policer_Entry,
                EHP_Entry,
                &Action_Id) != MEA_OK) {
                //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
                MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]), 0, sizeof(MEA_drv_Filter_Tbl[filterId]));
                MEA_OS_memset(&(MEA_drv_Filter_ContextTbl[filter_CtxId]), 0, sizeof(MEA_drv_Filter_ContextTbl[filter_CtxId]));
               
                if (New_Action)
                    MEA_OS_memset(&(MEA_drv_Filter_ActionTbl[filter_actionId]), 0, sizeof(MEA_drv_Filter_ActionTbl[filter_actionId]));

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s:%d - MEA_API_Create_Action failed (actionId=%d)\n",
                    __FUNCTION__, __LINE__, Action_Id);
                MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
                return MEA_ERROR;
            }
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
            MEA_OS_memcpy((char*)&(data->action_params),
                (char*)&Action_Data,
                sizeof(data->action_params));
        }
    }


    /* Create the filter */
    MEA_drv_Filter_Tbl[filterId].filter_CtxId = filter_CtxId;
    MEA_drv_Filter_Tbl[filterId].filter_action = filter_actionId;

    if (New_Action){
        MEA_drv_Filter_ActionTbl[filter_actionId].valid = MEA_TRUE;
        MEA_OS_memcpy(&MEA_drv_Filter_ActionTbl[filter_actionId].action_params,
            &(data->action_params),
            sizeof(MEA_drv_Filter_ActionTbl[filter_actionId].action_params));
        MEA_drv_Filter_ActionTbl[filter_actionId].num_of_owners = 1;
    }
    else{
        MEA_OS_memcpy(&(data->action_params),
            &MEA_drv_Filter_ActionTbl[filter_actionId].action_params,
            sizeof(MEA_Action_Entry_Data_dbt));
        mea_drv_filter_AddOwnerFilter_ActionId(unit_i, filter_actionId);
    }



    /* Update the global da for filter key type 15 reference count */
    if (mea_filter_get_key_type(key_from) == MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4) {

        if ((MEA_drv_Filter_Globals[hwUnit].reference_count > 0) &&
            (MEA_OS_memcmp(MEA_drv_Filter_Globals[hwUnit].da,
            key_from->layer2.ethernet.DA.b,
            sizeof(MEA_drv_Filter_Globals[hwUnit].da)) != 0)) {
            mea_drv_filter_DelOwnerFilter_ActionId(unit_i, filter_actionId);
            MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]), 0, sizeof(MEA_drv_Filter_Tbl[filterId]));
            MEA_OS_memset(&(MEA_drv_Filter_ContextTbl[filter_CtxId]), 0, sizeof(MEA_drv_Filter_ContextTbl[filter_CtxId]));

            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Filter key type %d can use only one da value \n",
                __FUNCTION__,
                MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4);

            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }

        if (key_from->layer2.ethernet.DA_valid == MEA_TRUE){
            if (MEA_drv_Filter_Globals[hwUnit].reference_count == 0) {
                MEA_OS_memcpy(MEA_drv_Filter_Globals[hwUnit].da,
                    key_from->layer2.ethernet.DA.b,
                    sizeof(MEA_drv_Filter_Globals[hwUnit].da));
            }
            MEA_drv_Filter_Globals[hwUnit].reference_count++;
        }
    }// MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4
#ifdef MEA_ACL_IPV6_SUPPORT


#endif

	if ( mea_drv_filter_create_procedure( unit_i,
										  filterId,
										  key_from,
										  data,
										  OutPorts_Entry) != MEA_OK) 
	{

		
		if (mea_filter_get_key_type(key_from) == MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4 &&
            key_from->layer2.ethernet.DA_valid == MEA_TRUE) {
            

			MEA_drv_Filter_Globals[hwUnit].reference_count--;
		}
		MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]),0,sizeof(MEA_drv_Filter_Tbl[filterId]));
        MEA_OS_memset(&(MEA_drv_Filter_ContextTbl[filter_CtxId]), 0, sizeof(MEA_drv_Filter_ContextTbl[filter_CtxId]));
        mea_drv_filter_DelOwnerFilter_ActionId(unit_i,filter_actionId);
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - mea_drv_filter_create_procedure failed \n",__FUNCTION__);

        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		return MEA_ERROR;
	}

    


	MEA_OS_memcpy(&(MEA_drv_Filter_Tbl[filterId].key),
		          key_from,
				  sizeof(MEA_drv_Filter_Tbl[filterId].key));

	if (o_filterId) {
		*o_filterId = filterId;
	}

    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
	return MEA_OK;
}


                                 
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Set_Filter                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_API_Set_Filter    (MEA_Unit_t                      unit_i,
                                   MEA_Filter_t                    filterId,
				                   MEA_Filter_Data_dbt            *data,
						           MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
							       MEA_Policer_Entry_dbt          *Policer_Entry,
							       MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry)
{
    MEA_OutPorts_Entry_dbt    output;
	MEA_Bool                  changed_output;
    MEA_Bool                  exist=MEA_FALSE;
    MEA_Port_t                port;
    MEA_Uint32                i;
	MEA_Uint32                j;
	MEA_Uint32                mask;
    MEA_Uint32               *ptr1;
    ENET_xPermissionId_t      xpermission_id=ENET_PLAT_GENERATE_NEW_ID;
	MEA_Action_Entry_Data_dbt Action_Data;
    mea_memory_mode_e         save_globalMemoryMode;
	MEA_db_HwUnit_t           hwUnit;
	
     MEA_Action_t              Action_Id;
//     MEA_Filter_t filter_actionId = 0;
//     MEA_Bool     New_Action = MEA_FALSE;
     MEA_Bool Act_exist = MEA_FALSE;


    MEA_API_LOG
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -You are not allowed to modify filterId %d  delete and crate again the ACL\n", __FUNCTION__, filterId);
        return MEA_ERROR;
     
    MEA_OS_memset(&output,0,sizeof(output));

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (filterId == MEA_FILTER_ID_RESERVED) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -You are not allowed to modify Reserved filterId %d \n",
                         __FUNCTION__,filterId);
       return MEA_ERROR; 
    }

	if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0,
                                     filterId,
                                     &exist) != MEA_OK)  {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -MEA_API_IsExist_Filter_ById %d failed \n",
                         __FUNCTION__,filterId);
       return MEA_ERROR; 
    }
    if (exist == MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - filterId %d not exist \n",
                         __FUNCTION__,filterId);
       return MEA_ERROR; 
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
	hwUnit = mea_Filter_GetHwUnit(unit_i,&(MEA_drv_Filter_Tbl[filterId].key));
	MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (data == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - NULL data input\n",
                         __FUNCTION__);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR; 
    }
#if 0
	if ( data->action_params.output_ports_valid && 
		 !(data->action_type == MEA_FILTER_ACTION_TO_ACTION) )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data action output valid is set but action is not TO_ACTION\n",
                         __FUNCTION__);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR;
    }
#endif


    if (data->force_Action_en == MEA_TRUE){
        /*check if the Action Id is create */
        if (data->Action_Id > 1 && data->Action_Id < MEA_MAX_NUM_OF_FILTER_ENTRIES){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "actionId=%d  is not on range up to %d\n",
                data->Action_Id, MEA_MAX_NUM_OF_FILTER_ENTRIES);
            return MEA_ERROR;

        }

        Action_Id = data->Action_Id + MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit);
        if (MEA_API_IsExist_Action(unit_i, Action_Id, &Act_exist) != MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "MEA_API_IsExist_Action  actionId=%d  is not on range\n",
                data->Action_Id);
            return MEA_ERROR;
        }
        if (Act_exist == MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "MEA_API_IsExist_Action  actionId=%d  is not valid\n",
                data->Action_Id);
            return MEA_ERROR;
        }

//        filter_actionId = data->Action_Id;

    }

   



	if ( data->action_type == MEA_FILTER_ACTION_TO_ACTION &&
		 data->action_params.output_ports_valid && 
		 OutPorts_Entry == NULL )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - data action output valid is set but OutPorts_Entry is NULL\n",
                         __FUNCTION__);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR;
    }
#if 0
    if (EHP_Entry && OutPorts_Entry == NULL ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - EHP_Entry is not NULL but OutPorts_Entry is NULL\n",
                         __FUNCTION__);
       MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR;
    }
#endif

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    


	/* If action stays TO_CPU or DISCARD - nothing to be done */
	if ( ( data->action_type == MEA_FILTER_ACTION_SEND_TO_CPU &&
		   MEA_drv_Filter_Tbl[filterId].data.action_type == MEA_FILTER_ACTION_SEND_TO_CPU ) ||
		 ( data->action_type == MEA_FILTER_ACTION_DISCARD &&
		   MEA_drv_Filter_Tbl[filterId].data.action_type == MEA_FILTER_ACTION_DISCARD ) )
	{
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		return MEA_OK;
	}

	/* Create the Action to CPU */
	if (data->action_type == MEA_FILTER_ACTION_SEND_TO_CPU) {
		if (MEA_API_Get_Action(unit_i,
			                   MEA_ACTION_ID_TO_CPU,
							   &Action_Data,
							   NULL, /* OutPorts */
							   NULL, /* Policer */
							   NULL  /* Editing */
							   ) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - MEA_API_Get_Action failed (actionId=%d)\n",
							  __FUNCTION__,MEA_ACTION_ID_TO_CPU);
			return MEA_ERROR;
		}
	    //mea_Action_SetType(unit_i,MEA_ACTION_TYPE_FILTER);
		Action_Data.Action_type = MEA_ACTION_TYPE_FILTER;
	    if (MEA_API_Set_Action(unit_i,
							   (MEA_Action_t)(filterId+MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit)),
		                       &Action_Data,
							   NULL,
							   NULL,
							   NULL) != MEA_OK) {
			//mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					          "%s - MEA_API_Set_Action failed \n",
							 __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}
		//mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
	}

	/* Create the Action Discard */
	if (data->action_type == MEA_FILTER_ACTION_DISCARD) {
		MEA_OS_memset((char*)(&Action_Data),0,sizeof(Action_Data));
	   // mea_Action_SetType(unit_i,MEA_ACTION_TYPE_FILTER);
		Action_Data.Action_type = MEA_ACTION_TYPE_FILTER;
	    if (MEA_API_Set_Action(unit_i,
			                   (MEA_Action_t)(filterId+MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit)),
		                       &Action_Data,
						       NULL,
						       NULL,
							   NULL) != MEA_OK) {
			//mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					          "%s - MEA_API_Set_Action failed \n",
							 __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}
		//mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
	}

	/* Create Action to action */
	if (data->action_type == MEA_FILTER_ACTION_TO_ACTION) {
	    MEA_Bool   llc;
		MEA_Uint32 protocol;
		MEA_EgressHeaderProc_Array_Entry_dbt*   Action_Editing;

		llc       = data->action_params.Llc;
		protocol  = data->action_params.Protocol;

		if ((data->action_params.proto_llc_valid    == MEA_FALSE) &&
			(data->action_params.protocol_llc_force == MEA_FALSE)) {
			if ((data->action_params.ed_id_valid ) &&
				(EHP_Entry != NULL ) &&
				(EHP_Entry->num_of_entries == 1) &&
				(EHP_Entry->ehp_info[0].ehp_data.eth_info.command == 
				MEA_EGRESS_HEADER_PROC_CMD_SWAP_MAC_AND_IP_ANS_SET_TTL)) {
				llc      = MEA_TRUE; /* SWAP MAC + IP */
				protocol = MEA_ACTION_PROTOCOL_ETH_TO_ETH_VLAN;
			} else {
				if (mea_Calc_ProtocolAndLlc(unit_i,
											MEA_drv_Filter_Tbl[filterId].key.interface_key.src_port, 
										   OutPorts_Entry,
										   &protocol,
										   &llc ) != MEA_OK ) {
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			  					    "%s - mea_Calc_ProtocolAndLlc failed \n",
									__FUNCTION__);
		            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
					return MEA_ERROR;  
				}
			}
		}

		MEA_OS_memcpy((char*)&Action_Data,
			          (char*)&(data->action_params),
		              sizeof(Action_Data));
		Action_Data.Protocol = protocol;
		Action_Data.Llc      = llc;
		if (EHP_Entry) {
			Action_Editing = EHP_Entry;
		} else {
			static MEA_Bool Action_def_Editing_first_time = MEA_TRUE;
			static MEA_EgressHeaderProc_Array_Entry_dbt    Action_def_Editing;
			static MEA_EHP_Info_dbt                        Action_def_Editing_Info;
			if (Action_def_Editing_first_time) {
				MEA_OS_memset(&Action_def_Editing     ,0,sizeof(Action_def_Editing));
				MEA_OS_memset(&Action_def_Editing_Info,0,sizeof(Action_def_Editing_Info));
				Action_def_Editing.num_of_entries = 1;
				Action_def_Editing.ehp_info = &Action_def_Editing_Info;
				Action_def_Editing_first_time = MEA_FALSE;
			}
			Action_Editing = &Action_def_Editing;
		}
		//mea_Action_SetType(unit_i,MEA_ACTION_TYPE_FILTER);
		Action_Data.Action_type = MEA_ACTION_TYPE_FILTER;
		if (MEA_API_Set_Action(unit_i,
			                   (MEA_Action_t)(filterId+MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit)),
			                   &Action_Data,
					  	       OutPorts_Entry,
							   Policer_Entry,
							   Action_Editing) != MEA_OK) {
			//mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					          "%s - MEA_API_Set_Action failed \n",
							 __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}
		//mea_Action_SetType(unit_i,MEA_ACTION_TYPE_DEFAULT);
		MEA_OS_memcpy((char*)&(data->action_params),
			          (char*)&Action_Data,
		              sizeof(data->action_params));
	}



	changed_output = MEA_FALSE;
	if ( OutPorts_Entry )
	{
		if ( MEA_OS_memcmp(&(MEA_drv_Filter_Tbl[filterId].out_ports),
			               OutPorts_Entry,
						   sizeof(MEA_drv_Filter_Tbl[filterId].out_ports)) != 0 )
		{
			changed_output = MEA_TRUE;
		}
	}



	/* If old action is CPU, and new is not: delete all CPU stuff*/
	if ( data->action_type != MEA_FILTER_ACTION_SEND_TO_CPU &&
		MEA_drv_Filter_Tbl[filterId].data.action_type == MEA_FILTER_ACTION_SEND_TO_CPU)
	{
		if (MEA_drv_Filter_Tbl[filterId].xpermission_id != ENET_PLAT_GENERATE_NEW_ID) {
		if (enet_Delete_xPermission(ENET_UNIT_0,
									MEA_drv_Filter_Tbl[filterId].xpermission_id
		                           ) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s = enete_Delete_xPermission failed \n",
			                  __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}
		}
		MEA_drv_Filter_Tbl[filterId].xpermission_id = ENET_PLAT_GENERATE_NEW_ID;

        
	}

	/* If old action is DISCARD, and new is not: delete all discard stuff*/
	if ( data->action_type != MEA_FILTER_ACTION_DISCARD &&
		MEA_drv_Filter_Tbl[filterId].data.action_type == MEA_FILTER_ACTION_DISCARD)
	{
		if (MEA_drv_Filter_Tbl[filterId].xpermission_id != ENET_PLAT_GENERATE_NEW_ID) {
		if (enet_Delete_xPermission(ENET_UNIT_0,
									MEA_drv_Filter_Tbl[filterId].xpermission_id
		                           ) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s = enete_Delete_xPermission failed \n",
			                  __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}
		}
		MEA_drv_Filter_Tbl[filterId].xpermission_id = ENET_PLAT_GENERATE_NEW_ID;

        
	}
    /* If old action is ACTION_TO_ACTION, and new is not: delete all discard stuff*/
	if ( data->action_type != MEA_FILTER_ACTION_DISCARD &&
		MEA_drv_Filter_Tbl[filterId].data.action_type == MEA_FILTER_ACTION_TO_ACTION)
	{
		if ((MEA_drv_Filter_Tbl[filterId].xpermission_id != ENET_PLAT_GENERATE_NEW_ID) && changed_output) {
		if (enet_Delete_xPermission(ENET_UNIT_0,
									MEA_drv_Filter_Tbl[filterId].xpermission_id
		                           ) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s = enete_Delete_xPermission failed \n",
			                  __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}
            MEA_drv_Filter_Tbl[filterId].xpermission_id = ENET_PLAT_GENERATE_NEW_ID;
		}
		

        
	}

	/* If old action was not TO_ACTION, and new is TO_ACTION - all previous 
	   stuff was deleted above, now we only need to create new stuff */
	if ( data->action_type == MEA_FILTER_ACTION_TO_ACTION &&
		MEA_drv_Filter_Tbl[filterId].data.action_type != MEA_FILTER_ACTION_TO_ACTION)
	{

		if ( mea_drv_filter_create_procedure( unit_i,
											  filterId,
											  &(MEA_drv_Filter_Tbl[filterId].key),
											  data,
											  OutPorts_Entry) != MEA_OK) 
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_filter_create_procedure failed \n",__FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}

        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		return MEA_OK;
	}




	


	if ( changed_output )
	{
		/* delete xpermission */
		if (MEA_drv_Filter_Tbl[filterId].xpermission_id != ENET_PLAT_GENERATE_NEW_ID) 
		{
			if (MEA_drv_Filter_Tbl[filterId].xpermission_id != ENET_PLAT_GENERATE_NEW_ID) {
			if (enet_Delete_xPermission(ENET_UNIT_0,
										MEA_drv_Filter_Tbl[filterId].xpermission_id
									   ) != ENET_OK) 
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s = enete_Delete_xPermission failed \n",
								  __FUNCTION__);
	            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
				return MEA_ERROR;
			}
			}
			MEA_drv_Filter_Tbl[filterId].xpermission_id = ENET_PLAT_GENERATE_NEW_ID;
		}
	

		if ( OutPorts_Entry == NULL )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - output_ports_valid marked but OutPorts_Entry is NULL\n", __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}

		for ( port = 0, i = 0 , 
			  ptr1 = &(OutPorts_Entry->out_ports_0_31);
			  i < sizeof(*OutPorts_Entry)/sizeof(OutPorts_Entry->out_ports_0_31);
			  i++,ptr1++ )
		{
			for (j=0,mask=1;j<32;j++,mask<<=1,port++) 
			{
				if ((*ptr1) & mask)
				{
				   if (ENET_IsValid_Queue(ENET_UNIT_0,port,ENET_TRUE) != ENET_TRUE) 
				   {
					   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
										 "%s - Port id %d is not define \n",
										 __FUNCTION__,port);
			           MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
					   return MEA_ERROR;
				   }
				}
			}
		}
        if (! MEA_API_Get_Warm_Restart())
		    xpermission_id = ENET_PLAT_GENERATE_NEW_ID;
        else
           xpermission_id=  data->save_hw_xPermissionId;

		if (enet_Create_xPermission(ENET_UNIT_0,
									OutPorts_Entry,
                                    0,/* no cpu*/
									&xpermission_id) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - enet_Create_xPermission failed \n", __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}
         data->save_hw_xPermissionId = xpermission_id;
	}



	/* if action is DISCARD - create 0 xpermission. 
	   If action is CPU - create CPU xpermission.
    */
	if ( data->action_type == MEA_FILTER_ACTION_SEND_TO_CPU ||
		 data->action_type == MEA_FILTER_ACTION_DISCARD )
	{
		MEA_OS_memset(&output,0,sizeof(output));

		if ( data->action_type == MEA_FILTER_ACTION_SEND_TO_CPU )
		{
	        ((MEA_Uint32*)(&(output.out_ports_0_31)))[MEA_CPU_DEFAULT_CLUSTER/32] |=  
				(1 << (MEA_CPU_DEFAULT_CLUSTER%32));
		}
		if (! MEA_API_Get_Warm_Restart())
            xpermission_id = ENET_PLAT_GENERATE_NEW_ID;
        else
            xpermission_id=  data->save_hw_xPermissionId;

		if (enet_Create_xPermission(ENET_UNIT_0,
									&output,
                                    1,/*to cpu*/
									&xpermission_id) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - enet_Create_xPermission failed \n", __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}

	     data->save_hw_xPermissionId = xpermission_id;
        
	}
	

	/* Set the SW image Filter Table */
	if ( mea_drv_filter_modify_data(data, filterId) != MEA_OK )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_filter_modify_data failed\n", __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		return MEA_ERROR;	
	}



	if ( data->action_params.output_ports_valid )
	{
		MEA_OS_memcpy(&(MEA_drv_Filter_Tbl[filterId].out_ports), 
			          OutPorts_Entry, 
					  sizeof(MEA_drv_Filter_Tbl[filterId].out_ports));
	}
    
    if (xpermission_id != ENET_PLAT_GENERATE_NEW_ID) {
        MEA_drv_Filter_Tbl[filterId].xpermission_id = xpermission_id;
    }

	/* Set the Filter HW */
    if (mea_drv_Filter_UpdateHw(unit_i,&(MEA_drv_Filter_Tbl[filterId]),filterId) != MEA_OK )
	{
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s - mea_drv_Filter_UpdateHw failed (filterId=%d) \n",
                           __FUNCTION__,filterId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }




    /* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
	}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Get_Filter                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                              
MEA_Status  MEA_API_Get_Filter     (MEA_Unit_t                      unit_i,
                                    MEA_Filter_t                    filterId,
								    MEA_Filter_Key_dbt             *key_from,
								    MEA_Filter_Key_dbt             *key_to,
				                    MEA_Filter_Data_dbt			   *data,
							        MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
							        MEA_Policer_Entry_dbt          *Policer_Entry,
							        MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry)
{
//MEA_Tmid_pri_dbt entrySwTmid;
    mea_memory_mode_e         save_globalMemoryMode;
	MEA_db_HwUnit_t           hwUnit;


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool exist;
#endif

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check for valid key->src_port parameter */
	if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0,
                                    filterId,
                                    &exist) != MEA_OK)  {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -MEA_API_IsExist_Filter_ById %d failed \n",
                         __FUNCTION__,filterId);
       return MEA_ERROR; 
    }
    if (exist == MEA_FALSE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - filterId %d not exist \n",
                         __FUNCTION__,filterId);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	hwUnit =  mea_Filter_GetHwUnit(unit_i,key_from);

	MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	if ( key_to != NULL )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - key_to must be NULL\n",
                         __FUNCTION__);
	   MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
       return MEA_ERROR; 
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (key_from != NULL) {
        MEA_OS_memcpy(key_from,&(MEA_drv_Filter_Tbl[filterId].key),sizeof(*key_from));
    }

    if (data != NULL) {
        MEA_OS_memcpy(data,&(MEA_drv_Filter_Tbl[filterId].data),sizeof(*data));
    }


/* Get the outports table - if supply */
if (OutPorts_Entry)
{
    MEA_OS_memcpy(OutPorts_Entry,
        &(MEA_drv_Filter_Tbl[filterId].out_ports),
        sizeof(*OutPorts_Entry));
}



    if ((Policer_Entry != NULL) ||
        (EHP_Entry != NULL)) {
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_FILTER);
        if (MEA_API_Get_Action(unit_i,
            (MEA_Action_t)(MEA_drv_Filter_Tbl[filterId].filter_action + MEA_ACTION_TBL_FILTER_START_INDEX(hwUnit)),
            NULL, /* data */
            NULL, /* OutPorts */
            Policer_Entry,
            EHP_Entry) != MEA_OK) {
            //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Get_Action failed (filterId=%d)\n",
                __FUNCTION__,
                filterId);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        //mea_Action_SetType(unit_i, MEA_ACTION_TYPE_DEFAULT);
    }



/* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Delete_Filter                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_API_Delete_Filter(MEA_Unit_t                      unit_i,
    MEA_Filter_t                    filterId)
{

    mea_memory_mode_e         save_globalMemoryMode;
    MEA_db_HwUnit_t           hwUnit;
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool exist;
#endif

    MEA_API_LOG

        //MEA_Tmid_pri_dbt entryTmid;
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (filterId == MEA_FILTER_ID_RESERVED) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -You are not allowed to delete Reserved filterId %d \n",
                __FUNCTION__, filterId);
            return MEA_ERROR;
        }

    /* Check for valid filterId parameter */
    if (MEA_API_IsFilterIdInRange(MEA_UNIT_0, filterId) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Invalid filterId %d \n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }

    if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0,
        filterId,
        &exist) != MEA_OK)  {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -MEA_API_IsExist_Filter_ById %d failed \n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }
    if (exist == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - filterId %d not exist \n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    hwUnit = mea_Filter_GetHwUnit(unit_i, &(MEA_drv_Filter_Tbl[filterId].key));
    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode, hwUnit);

    /* Delete the action of the Service */
    if(mea_drv_filter_DelOwnerFilter_ActionId(unit_i, MEA_drv_Filter_Tbl[filterId].filter_action) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_filter_DelOwnerFilter_ActionId %d not exist \n",
            __FUNCTION__, MEA_drv_Filter_Tbl[filterId].filter_action);
        return MEA_ERROR;
    }

  



  if (MEA_drv_Filter_Tbl[filterId].xpermission_id != ENET_PLAT_GENERATE_NEW_ID) {
  if (enet_Delete_xPermission(ENET_UNIT_0,
  							  MEA_drv_Filter_Tbl[filterId].xpermission_id
		                     ) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s = enete_Delete_xPermission failed \n",
			                  __FUNCTION__);
	        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
  }
  }
  MEA_drv_Filter_Tbl[filterId].xpermission_id = ENET_PLAT_GENERATE_NEW_ID;

  


	
    if (mea_filter_get_key_type(&(MEA_drv_Filter_Tbl[filterId].key)) 
		 == MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4 &&
         MEA_drv_Filter_Tbl[filterId].key.layer2.ethernet.DA_valid == MEA_TRUE ) {
			 MEA_drv_Filter_Globals[hwUnit].reference_count--;
	}

#ifdef MEA_ACL_IPV6_SUPPORT

#endif

	MEA_drv_Filter_Tbl[filterId].valid = MEA_FALSE;

    /* delete from Hardware */
    if (mea_drv_Filter_UpdateHw(unit_i,&(MEA_drv_Filter_Tbl[filterId]),filterId) != MEA_OK )
	{
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                           "%s - mea_drv_Filter_UpdateHw failed (filterId=%d) \n",
                           __FUNCTION__,filterId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

    MEA_drv_Filter_ContextTbl[MEA_drv_Filter_Tbl[filterId].filter_CtxId].valid = MEA_FALSE;
    MEA_OS_memset(&(MEA_drv_Filter_ContextTbl[MEA_drv_Filter_Tbl[filterId].filter_CtxId]), 0, sizeof(MEA_drv_Filter_ContextTbl[0]));

   MEA_OS_memset(&(MEA_drv_Filter_Tbl[filterId]),
                   0,
                   sizeof(MEA_drv_Filter_Tbl[0]));

		                
 
    


	MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_GetFirst_Filter                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
MEA_Status MEA_API_GetFirst_Filter (MEA_Unit_t     unit,
                                    MEA_Filter_t *o_filterId,
									MEA_Bool     *o_found)
{
	
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (o_filterId == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - o_filterId == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    if (o_found == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - o_found == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	*o_found = MEA_FALSE;

	for ( *o_filterId = 1; *o_filterId < MEA_MAX_NUM_OF_FILTER_ENTRIES; (*o_filterId)++ )
	{
		if ( MEA_drv_Filter_Tbl[*o_filterId].valid == MEA_TRUE )
		{
			*o_found = MEA_TRUE;
			return MEA_OK;
		}
	}

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_GetNext_Filter                             */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
MEA_Status MEA_API_GetNext_Filter  (MEA_Unit_t     unit,
                                    MEA_Filter_t *io_filterId,
									MEA_Bool     *o_found)
{
	MEA_Uint32 i;

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (io_filterId == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - io_filterId == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    /* Check for valid filterId parameter */
    if (MEA_API_IsFilterIdInRange(MEA_UNIT_0,*io_filterId) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid filterId %d \n",
                          __FUNCTION__,*io_filterId);
        return MEA_ERROR;  
    }

    if (o_found == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - o_found == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	*o_found = MEA_FALSE;

	for ( i = *io_filterId+1; i < MEA_MAX_NUM_OF_FILTER_ENTRIES; i++ )
	{
		if ( MEA_drv_Filter_Tbl[i].valid == MEA_TRUE )
		{
			*o_found = MEA_TRUE;
			*io_filterId = (MEA_Filter_t)i;
			return MEA_OK;
		}
	}

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_IsExist_Filter_ById                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
MEA_Status MEA_API_IsExist_Filter_ById  (MEA_Unit_t     unit,
                                          MEA_Filter_t  filterId, 
                                          MEA_Bool      *exist)
{
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid filterId parameter */
    if (MEA_API_IsFilterIdInRange(MEA_UNIT_0,filterId) == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid filterId %d \n",
                          __FUNCTION__,filterId);
        return MEA_ERROR;  
    }

    if (exist == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    *exist = MEA_FALSE;

	if (MEA_drv_Filter_Tbl[filterId].valid)
	{
		*exist = MEA_TRUE;
	}
   

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_IsExist_Filter_ByKey                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/                                 
/* Note: only key and exist are mandatory */
MEA_Status MEA_API_IsExist_Filter_ByKey (MEA_Unit_t                 unit,
                                          MEA_Filter_Key_dbt       *key_from,
                                          MEA_Filter_Key_dbt       *key_to,
                                          MEA_Bool                  *exist,
                                          MEA_Filter_t             *filterId)
{
	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (key_from == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key_from == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    if (key_to != NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key_to != NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    if (exist == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    if (filterId == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - filterId == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	*exist = MEA_FALSE;

	for ( *filterId = 1; *filterId < MEA_MAX_NUM_OF_FILTER_ENTRIES; (*filterId)++ )
	{
		if ( MEA_drv_Filter_Tbl[*filterId].valid == MEA_TRUE )
		{
			if ( MEA_OS_memcmp(key_from, 
				               &(MEA_drv_Filter_Tbl[*filterId].key), 
							   sizeof(*key_from)) == 0 )
			{
				*exist = MEA_TRUE;
				return MEA_OK;
			}
		}
	}

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_IsFilterIdInRange                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/ 
MEA_Bool MEA_API_IsFilterIdInRange(MEA_Unit_t     unit, 
                                    MEA_Filter_t  filterId)
{
   if(/*filterId>=0 &&*/  filterId<MEA_MAX_NUM_OF_FILTER_ENTRIES)
   	return MEA_TRUE;
   else
    return MEA_FALSE;		
}


/****************************************************************************/
/*                     MASK PROFILE Implementation                          */
/****************************************************************************/
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Filter_Mask_Prof_UpdateHwEntry>                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_drv_Filter_Mask_Prof_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    //MEA_Unit_t                       unit_i   = (MEA_Unit_t                       )arg1;
    //MEA_db_HwUnit_t                  hwUnit_i = (MEA_db_HwUnit_t                  )((long)arg2);
    MEA_db_HwId_t                    hwId_i   = (MEA_db_HwId_t                    )((long)arg3);
    MEA_ACL_mask_dbt                *entry_pi = (MEA_ACL_mask_dbt *)arg4;
	MEA_Uint32                       val[2];
	MEA_Uint32                       i;
	MEA_Uint16                       num_of_regs;
     
	

	num_of_regs = MEA_NUM_OF_ELEMENTS(val);
	for (i=0;i<num_of_regs;i++) {
		val[i] = 0;
	}

	if ( (hwId_i%2) == 0 )
	{
        
            val[0] = entry_pi->mask.mask_0_31;


            val[1] |= (((1)
                << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_MASK_PROF_WORD_1_FIELD0_VALID_MASK)
                ) & MEA_IF_FILTER_MASK_PROF_WORD_1_FIELD0_VALID_MASK
                );

            val[1] |= (((1)
                << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_MASK_PROF_WORD_1_FIELD1_VALID_MASK)
                ) & MEA_IF_FILTER_MASK_PROF_WORD_1_FIELD1_VALID_MASK
                );
        

	} else {
        
            val[0] = entry_pi->mask.mask_32_63;

            val[1] |= (((1)
                << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_MASK_PROF_WORD_3_FIELD2_VALID_MASK)
                ) & MEA_IF_FILTER_MASK_PROF_WORD_3_FIELD2_VALID_MASK
                );

            val[1] |= (((1)
                << MEA_OS_calc_shift_from_mask(MEA_IF_FILTER_MASK_PROF_WORD_3_SRC_PORT_VALID_MASK)
                ) & MEA_IF_FILTER_MASK_PROF_WORD_3_SRC_PORT_VALID_MASK
                );
        

	}

	for (i=0;i<num_of_regs;i++) {
		MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(i) , val[i] , MEA_MODULE_IF);
	}



}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Filter_Mask_Prof_UpdateHw>                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Filter_Mask_Prof_UpdateHw
                       (MEA_Unit_t                       unit_i,
					    MEA_FilterMask_t                 id_i,
						//MEA_drv_filter_mask_profile_dbt *entry_pi
                        MEA_ACL_prof_Key_mask_dbt *entry_pi)
{

	MEA_ind_write_t ind_write;     
	
//	MEA_db_HwUnit_t hwUnit;
    MEA_ACL_mask_dbt  entry_mask;
    MEA_FilterMask_t  my_index;


   
    MEA_OS_memset(&entry_mask, 0, sizeof(entry_mask));


	/* important!! - on HW, each entry is spread over 2 entries */
	ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_FILTER_MASK_PROF;    
    ind_write.tableOffset = (id_i * 2);
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_Filter_Mask_Prof_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);

    my_index = id_i * 2;
    ind_write.funcParam3 = (MEA_funcParam)((long)(my_index));
    ind_write.funcParam4 = (MEA_funcParam)&entry_mask;

    MEA_OS_memcpy(&entry_mask, &entry_pi->data_info[0], sizeof(entry_mask));
    

	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }

	ind_write.tableOffset++;
	/*ind_write.funcParam3++;*/
    ind_write.funcParam3 = (MEA_funcParam)((long)((my_index)+1));


	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }

    if (MEA_ACL_HIERARCHICAL_SUPPORT == MEA_TRUE){
    
        
        /************************************************************************/
        /*            HIERARC 1                                                  */
        /************************************************************************/

            my_index = 128 + (id_i * 2);
            ind_write.tableOffset = (my_index);
            ind_write.funcParam3 = (MEA_funcParam)((long)(my_index));
            ind_write.funcParam4 = (MEA_funcParam)&entry_mask;

            MEA_OS_memcpy(&entry_mask, &entry_pi->data_info[1], sizeof(entry_mask));


            if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }

            ind_write.tableOffset++;
            /*ind_write.funcParam3++;*/
            ind_write.funcParam3 = (MEA_funcParam)((long)((my_index)+1));


            if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }


            /************************************************************************/
            /*         HIERARC 2                                                             */
            /************************************************************************/

            my_index = 256 + (id_i * 2);
            ind_write.tableOffset = (my_index);
            ind_write.funcParam3 = (MEA_funcParam)((long)(my_index));
            ind_write.funcParam4 = (MEA_funcParam)&entry_mask;

            MEA_OS_memcpy(&entry_mask, &entry_pi->data_info[2], sizeof(entry_mask));


            if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }

            ind_write.tableOffset++;
            /*ind_write.funcParam3++;*/
            ind_write.funcParam3 = (MEA_funcParam)((long)((my_index)+1));


            if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }
       
            /************************************************************************/
            /*         HIERARC 3                                                             */
            /************************************************************************/

            my_index = 384 + (id_i * 2);
            ind_write.tableOffset = (my_index);
            ind_write.funcParam3 = (MEA_funcParam)((long)(my_index));
            ind_write.funcParam4 = (MEA_funcParam)&entry_mask;

            MEA_OS_memcpy(&entry_mask, &entry_pi->data_info[3], sizeof(entry_mask));


            if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }

            ind_write.tableOffset++;
            /*ind_write.funcParam3++;*/
            ind_write.funcParam3 = (MEA_funcParam)((long)((my_index)+1));


            if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
                return MEA_ERROR;
            }
    
    
    
    
    
    
    
    }




    return MEA_OK;


}

static MEA_Bool mea_drv_IsFilterMaskProfIdInRange(MEA_Unit_t       unit_i,
												  MEA_FilterMask_t id_i)
{

   if(id_i<MEA_MAX_NUM_OF_FILTER_MASK_ID)
   	return MEA_TRUE;
   else
    return MEA_FALSE;		

}


static MEA_Bool mea_drv_is_filter_mask_prof_exist(MEA_Unit_t           unit_i,
                                                  MEA_ACL_prof_Key_mask_dbt *entry_pi,
												  MEA_FilterMask_t    *id_o)
{
	MEA_FilterMask_t index;
	MEA_db_HwUnit_t  hwUnit;

	MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_FALSE);

	for (index = 0; index < MEA_MAX_NUM_OF_FILTER_MASK_ID; index++)
	{
		if (( MEA_Filter_Mask_Prof_Table[index].valid ) &&
			( MEA_Filter_Mask_Prof_Table[index].hwUnit == hwUnit) &&
			( MEA_OS_memcmp(entry_pi, 
						    &(MEA_Filter_Mask_Prof_Table[index].data), 
							sizeof(*entry_pi)) == 0 )) {
			{
				*id_o = index;
				return MEA_TRUE;
			}
		}
	}

	return MEA_FALSE;
}

static MEA_Status mea_drv_filter_mask_prof_add_owner(MEA_Unit_t unit_i,
													 MEA_FilterMask_t id_i)
{
    if (mea_drv_IsFilterMaskProfIdInRange(unit_i,id_i) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }

	if ( MEA_Filter_Mask_Prof_Table[id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - none existing filter mask entry with ID %d\n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }

	MEA_Filter_Mask_Prof_Table[id_i].num_of_owners++;

	return MEA_OK;
}


MEA_Status mea_drv_filter_mask_prof_delete_owner(MEA_Unit_t       unit_i,
												 MEA_FilterMask_t id_i)
{
    if (mea_drv_IsFilterMaskProfIdInRange(unit_i,id_i) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }

	if ( MEA_Filter_Mask_Prof_Table[id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - none existing filter mask entry with ID %d\n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }

	MEA_Filter_Mask_Prof_Table[id_i].num_of_owners--;

	if ( MEA_Filter_Mask_Prof_Table[id_i].num_of_owners == 0 )
	{

		//MEA_drv_filter_mask_profile_dbt entry;
        MEA_ACL_prof_Key_mask_dbt entry;
		//MEA_db_HwUnit_t hwUnit; 
		MEA_OS_memset((char*)&entry,0,sizeof(entry));
		//hwUnit = MEA_Filter_Mask_Prof_Table[id_i].hwUnit;
		//entry.hwUnit = hwUnit;

		if (mea_drv_Filter_Mask_Prof_UpdateHw( unit_i,id_i,&entry) != MEA_OK) {

			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_Filter_mask_prof_UpdateHw failed\n",
							  __FUNCTION__);
			return MEA_ERROR;
		}



		MEA_OS_memset(&(MEA_Filter_Mask_Prof_Table[id_i]), 0, sizeof(MEA_Filter_Mask_Prof_Table[0]));
        MEA_Filter_Mask_Prof_Table[id_i].valid = MEA_FALSE;
	}


	return MEA_OK;
}



static MEA_Status mea_drv_get_free_filter_mask_prof_id(MEA_Unit_t        unit_i,
													   MEA_FilterMask_t *id_o)
{
 
    for ( *id_o = 1; *id_o <= MEA_MAX_NUM_OF_FILTER_MASK_ID; (*id_o)++ )
    {
	    if ( MEA_Filter_Mask_Prof_Table[*id_o].valid == MEA_FALSE )
	    {
		    return MEA_OK;
	    }
	}
	
    
   

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - no free filter mask entry\n",
                      __FUNCTION__);
	return MEA_ERROR;
}


static MEA_Status mea_drv_create_filter_mask_prof_entry(MEA_Unit_t           unit_i,
                                                        MEA_ACL_prof_Key_mask_dbt *entry_pi,
										                MEA_FilterMask_t    *id_o)
{
	MEA_db_HwUnit_t hwUnit;
	//MEA_db_HwId_t   hwId;
	//MEA_drv_filter_mask_profile_dbt entry;
    
    
	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
						return MEA_ERROR);

    if ( mea_drv_get_free_filter_mask_prof_id(unit_i,id_o) != MEA_OK )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_get_free_filter_mask_prof_id failed\n",
                          __FUNCTION__);
		return MEA_ERROR;
	}

	


//    MEA_OS_memset(&entry, 0, sizeof(MEA_drv_filter_mask_profile_dbt));

//     entry.valid         = MEA_TRUE;
//     entry.hwUnit        = hwUnit;
// 	entry.num_of_owners = 1;
//     MEA_OS_memcpy(&(entry.data), entry_pi, sizeof(entry));

	if (mea_drv_Filter_Mask_Prof_UpdateHw(unit_i,
		                                  *id_o,
                                          entry_pi) != MEA_OK) {

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Filter_Mask_Prof_UpdateHw failed\n",
                          __FUNCTION__);
//		mea_db_Delete(unit_i,MEA_Filter_Mask_db,(MEA_db_SwId_t)(*id_o),hwUnit);
        return MEA_ERROR;
    }

   

	MEA_OS_memcpy(&(MEA_Filter_Mask_Prof_Table[*id_o].data),
		          entry_pi,
                  sizeof(MEA_ACL_prof_Key_mask_dbt));

    MEA_Filter_Mask_Prof_Table[*id_o].valid = MEA_TRUE;
    MEA_Filter_Mask_Prof_Table[*id_o].hwUnit = hwUnit;
    MEA_Filter_Mask_Prof_Table[*id_o].num_of_owners = 1;


	return MEA_OK;
}


MEA_Status mea_drv_set_Filter_Mask_Prof(MEA_Unit_t           unit_i,
                                        MEA_ACL_prof_Key_mask_dbt *entry_pi,
										MEA_FilterMask_t    *id_o)
{
	MEA_FilterMask_t index=0;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check for valid entry parameter */
    if (id_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - id_o == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    if (entry_pi == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_pi == NULL \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


	if ( mea_drv_is_filter_mask_prof_exist(unit_i,entry_pi, &index) )
	{
		if ( mea_drv_filter_mask_prof_add_owner(unit_i,index) != MEA_OK )
		{
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_filter_mask_prof_add_owner failed, index %d\n",
							  __FUNCTION__, index);
			return MEA_ERROR; 
		}
	}
	else
	{
		if ( mea_drv_create_filter_mask_prof_entry(unit_i,entry_pi,&index) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_create_filter_mask_prof_entry failed\n",
							  __FUNCTION__);
			return MEA_ERROR; 
		}
	}

	*id_o = index;

	return MEA_OK;

}

MEA_Status mea_drv_dbg_filter_mask_prof_get(MEA_Unit_t unit_i,
											MEA_FilterMask_t id,
											MEA_Bool   *found,
	                                        MEA_drv_filter_mask_profile_dbt *mask_prof_o)
{
	*found = MEA_FALSE;

    if (mea_drv_IsFilterMaskProfIdInRange(unit_i,id) != MEA_TRUE) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    if ( MEA_Filter_Mask_Prof_Table[id].valid == MEA_TRUE )
    {
        MEA_OS_memcpy(mask_prof_o,&(MEA_Filter_Mask_Prof_Table[id]),sizeof(*mask_prof_o));
        *found = MEA_TRUE;
    }

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        <MEA_API_Delete_all_filters>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_all_filters(MEA_Unit_t unit) 
{

   MEA_Filter_t      filter_id;
   MEA_Filter_t      temp_filter_id;
   MEA_Bool          found;


   /* Get the first filter */
   if (MEA_API_GetFirst_Filter(unit,&filter_id, &found) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - failed to GetFirst filter \n",
                          __FUNCTION__);
        return MEA_ERROR;
   }
 
   /* loop until no more filters */
   while (found) {

       /* Save the filter id that need to be delete , before the getNext */
       temp_filter_id = filter_id;
       
            
       /* Get the next filter */
       if (MEA_API_GetNext_Filter(unit,
                                   &filter_id,
                                   &found) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to GetNext filter %d\n",
                             __FUNCTION__,filter_id);
            return MEA_ERROR;
       }

       /* Delete the save filter */
       if (MEA_API_Delete_Filter(unit,temp_filter_id)!=MEA_OK) { 
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - failed to delete filter %d\n",
                             __FUNCTION__,temp_filter_id);
           return MEA_ERROR;
       }

   }

    
   /* Return to caller */
   return MEA_OK;

}


MEA_Int8* mea_drv_acl_Get_Filter_Key_Type_STR(MEA_Filter_Key_Type_te key_type)
{
    switch (key_type)
    {
    case MEA_FILTER_KEY_TYPE_DA_MAC:
        return "DA MAC";
    case MEA_FILTER_KEY_TYPE_SA_MAC:
        return "SA MAC";
    case MEA_FILTER_KEY_TYPE_DST_IPV4_DST_PORT:
        return "DST IPv4 + DST Port";
    case MEA_FILTER_KEY_TYPE_SRC_IPV4_DST_PORT:
        return "SRC IPv4 + DST Port";
    case MEA_FILTER_KEY_TYPE_DST_IPV4_SRC_PORT:
        return "DST IPv4 + SRC Port";
    case MEA_FILTER_KEY_TYPE_SRC_IPV4_SRC_PORT:
        return "SRC IPv4 + SRC Port";
    case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG:
        return "Outer EthType + Inner EthType + Outer vlan-Tag";
    case MEA_FILTER_KEY_TYPE_SRC_IPV4_APPL_ETHERTYPE:
        return "SRC IPv4 + Appl EthType";
    case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT:
        return "Pri + IP Protocol + Appl EthType + DST Port ";
    case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_SRC_PORT_DST_PORT:
        return "Pri + IP Protocol + SRC Port + DST Port ";
    case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_INNER_VLAN_TAG_OUTER_ETHERTYPE:
        return "Outer vlan-Tag + Inner vlan-Tag + Outer EthType";
    case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_DST_PORT_SRC_PORT:
        return "Outer vlan-Tag TAG + DST Port + SRC Port";
    case MEA_FILTER_KEY_TYPE_INNER_VLAN_TAG_DST_PORT_SRC_PORT:
        return "Inner vlan-Tag + DST Port + SRC Port";
    case MEA_FILTER_KEY_TYPE_PPPOE_SESSION_ID_PPP_PROTO_SA_MAC_LSB:
        return "PPPoE Session ID + ppp protocol + SA MAC LSB";
    
    case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_APPL_ETHERTYPE_OUTER_VLAN_TAG:
        return "Outer EtherType + Appl EtherType + Outer vlan-Tag";

    case MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4:
        return "DA MAC + Outer vlan-Tag + Dst IPv4";
    case MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT:
        return "DST IPv6 + DST Port";
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT:
        return "SRC IPv6 + DST Port";
    case MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT:
        return "DST IPv6 + SRC Port";
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT:
        return "SRC IPv6 + SRC Port";
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE:
        return "SRC IPv6 + Appl EthType";


    default:
        return "--";
    break;
    }
}

MEA_Int8* MEA_drv_acl_Get_Filter_Action_STR(MEA_Filter_Action_te action)
{
    switch (action)
    {
    case MEA_FILTER_ACTION_DISCARD:
        return "DIS";
    case MEA_FILTER_ACTION_SEND_TO_CPU:
        return "CPU";
    case MEA_FILTER_ACTION_TO_ACTION:
        return "ACT";
    default:
        return "   ";
    }
}



MEA_Status MEA_API_ACL_Set_CountersBySID(MEA_Unit_t unit, MEA_Service_t Sid)
{
    MEA_Uint32 Val;

    MEA_drv_Filter_Global_Sid_count = (0x0000ffff & Sid);



    Val = 0;
    Val |= MEA_drv_Filter_Global_Sid_count;
    Val |= MEA_drv_Filter_default_Contex << 16;

    MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_ACL_COUNT_BY_SID_REG, (MEA_Uint32)Sid, MEA_MODULE_IF);
    MEA_API_Clear_Counters_ACLs(unit);


    return MEA_OK;
}

/* un-much on withList*/
MEA_Status MEA_API_ACL_Set_Default_rule(MEA_Unit_t unit, MEA_Uint32 filterId)
{
    MEA_Uint32 Val;

    MEA_drv_Filter_default_Contex = filterId;

    Val = 0;
    Val |= MEA_drv_Filter_Global_Sid_count;
    Val |= MEA_drv_Filter_default_Contex << 16;

    MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_ACL_COUNT_BY_SID_REG, (MEA_Uint32)Val, MEA_MODULE_IF);
    MEA_API_Clear_Counters_ACLs(unit);


    return MEA_OK;
}


MEA_Service_t MEA_API_ACL_Get_CountersBySID(MEA_Unit_t unit)
{

    return MEA_drv_Filter_Global_Sid_count;
}

MEA_Status MEA_API_Collect_Counters_ACLs(MEA_Unit_t unit)
{

   


    MEA_ind_read_t         ind_read;
    MEA_Uint32             if_rd[4];
    

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_TYP_REG; // 3
    ind_read.tableOffset = MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL_COUNT;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data = &(if_rd[0]);

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0, &ind_read,
        MEA_NUM_OF_ELEMENTS(if_rd),
        MEA_MODULE_IF,
        globalMemoryMode, MEA_MEMORY_READ_IGNORE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
            __FUNCTION__, ind_read.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }

    MEA_OS_UPDATE_COUNTER(MEA_Counters_ACL_TBL.ACLDrop[0].val, if_rd[0]);
    MEA_OS_UPDATE_COUNTER(MEA_Counters_ACL_TBL.ACLDrop[1].val, if_rd[1]);
    MEA_OS_UPDATE_COUNTER(MEA_Counters_ACL_TBL.ACLDrop[2].val, if_rd[2]);
    MEA_OS_UPDATE_COUNTER(MEA_Counters_ACL_TBL.ACLDrop[3].val, if_rd[3]);






    return MEA_OK;
}
MEA_Status MEA_API_Clear_Counters_ACLs(MEA_Unit_t unit)
{

    MEA_API_Collect_Counters_ACLs(unit);
    MEA_OS_memset(&MEA_Counters_ACL_TBL, 0, sizeof(MEA_Counters_ACL_TBL));

    return MEA_OK;
}
MEA_Status MEA_API_Get_Counters_ACL(MEA_Unit_t unit, MEA_Counters_ACL_dbt *entry)
{
    if (entry == NULL){
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry,
        &(MEA_Counters_ACL_TBL),
        sizeof(*entry));

    return MEA_OK;
}



/*****************************************************************************************************/

