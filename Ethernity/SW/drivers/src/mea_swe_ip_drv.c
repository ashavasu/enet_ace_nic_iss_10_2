/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_port_drv.h"
#include "mea_drv_common.h"
#include "MEA_platform_os_utils.h"
#include "mea_if_l2cp_drv.h"

#include "mea_if_service_drv.h"
#include "mea_mapping_drv.h"
#include "mea_port_drv.h"
#include "mea_limiter_drv.h"
#include "mea_globals_drv.h"
#include "mea_swe_ip_drv.h"




/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32  L3_offset           :6;
    MEA_Uint32  Enable_Checksum     :1;
    MEA_Uint32  Enable_TS           :1;
    MEA_Uint32  pad                 :24;
#else
    MEA_Uint32  pad                 :24;
    MEA_Uint32  Enable_TS           :1;
    MEA_Uint32  Enable_Checksum     :1;
    MEA_Uint32  L3_offset           :6;
#endif
}MEA_L2_To_L3_1588_Offset_t;

typedef struct{

    MEA_Bool           valid;
    MEA_RxAfdxBag_dbt  data;
    MEA_Uint32 num_of_owners;

}MEA_afdxBag_Rx_dbt;

typedef struct{

    MEA_Def_SID_dbt  data[MEA_INGRESS_L2TYPE_LAST]; /* 0:6 7 not support*/

}MEA_IngressPort_Default_L2Type_dbt;

typedef struct{

    MEA_Def_SID_dbt  data[MEA_INGRESS_PDUsTYPE_LAST]; /* */

}MEA_IngressPort_Default_PDUsType_dbt;


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

static MEA_Port_Ptp1588_dbt MEA_Port_delay_ptp1588_Table[MEA_MAX_PORT_NUMBER+1];

static MEA_IngressPort_Entry_dbt *MEA_IngressPort_Table = NULL;  // [MEA_MAX_PORT_NUMBER + 1];


static MEA_Bool                  MEA_IngressPort_InitDone = MEA_FALSE;
static MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_rx_enable;
static MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_rx_enable_save_for_ReInit;
#if 0
static MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_utopia;
#endif

static MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_GigaDisable;
static MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_Autoneg;

static  MEA_IngressPort_Default_L2Type_dbt MEA_IngressPort_Default_L2Type_Table[MEA_MAX_PORT_NUMBER+1];
static  MEA_IngressPort_Default_PDUsType_dbt MEA_IngressPort_Default_PDUsType_Table[MEA_MAX_PORT_NUMBER+1];

static MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_crc_check;

 MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_Link_protection_rx_Status;
 MEA_OutPorts_Entry_dbt    MEA_IngressPort_Table_Link_protection_rx_Mask;


static MEA_L2_To_L3_1588_Offset_t *MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table=NULL;

static      MEA_afdxBag_Rx_dbt     *MEA_AfdxBag_info_Table=NULL;



static MEA_Uint32                MEA_IngressPort_fragmant_bonding_port_group[4]; //

static MEA_Uint32  MEA_IngressPort_Protect_1p1_reg[2];

static MEA_Uint32  MEA_IngressPort_Protect_1p1_Enable[1];


MEA_Bool  MEA_IngressPort_save_Admin[MEA_MAX_PORT_NUMBER + 1];

static MEA_OutPorts_Entry_dbt     MEA_IngressPort_Vxlan_cpe;

static MEA_OutPorts_Entry_dbt    MEA_IngressPort_BondingRX_enable;

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                    */
/*----------------------------------------------------------------------------*/
   

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

MEA_Status mea_drv_parser_MAC_filter(MEA_Unit_t unit,MEA_Port_t port,MEA_MacAddr DA);

                                                
MEA_Status mea_drv_Set_IngressPort_ParserInfo(MEA_Unit_t                      unit,
                                              MEA_Port_t                      port,
                                              MEA_IngressPort_ParserInfo_dbt* new_entry,
                                              MEA_IngressPort_ParserInfo_dbt* old_entry)
{


	MEA_Parser_Entry_dbt parser_entry_val_def;
    MEA_Globals_Entry_dbt global_entry;
    MEA_Status retval=MEA_ERROR;


    if ((MEA_IngressPort_InitDone == MEA_TRUE) &&
        (MEA_OS_memcmp(new_entry,old_entry,sizeof(*new_entry)) == 0)) {
        return MEA_OK;
    }

    MEA_OS_memset(&global_entry,0,sizeof(global_entry));
    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&global_entry) !=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	 
            "%s /n- MEA_API_Get_Globals_Entry failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    /* build the parser entry */
	MEA_OS_memset(&parser_entry_val_def,0,sizeof(parser_entry_val_def));
    parser_entry_val_def.f.default_net_tag_value = new_entry->default_net_tag_value;
    
    parser_entry_val_def.f.vlanrange_prof_valid = new_entry->vlanrange_enable;
    parser_entry_val_def.f.vlanrange_prof_Id = new_entry->vlanrange_prof_Id;
    parser_entry_val_def.f.lag_distribution_mask = new_entry->lag_distribution_mask;

    


    if(MEA_AFDX_SUPPORT != MEA_TRUE && new_entry->afdx_enable == MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	 
                "%s /n- AFDX is not support \n",
                __FUNCTION__);
            return MEA_ERROR;
    }else{
        parser_entry_val_def.f.afdx_enable = new_entry->afdx_enable;
    }
    parser_entry_val_def.f.L4_SRC_DST = new_entry->L4_SRC_DST;
    parser_entry_val_def.f.DSA_Type_enable = new_entry->DSA_Type_enable;

    parser_entry_val_def.f.default_pri = new_entry->default_pri;

	
	parser_entry_val_def.f.L4_SRC_DST      =  0; 
	parser_entry_val_def.f.DSA_Type_enable = MEA_FALSE;

    if (MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT == MEA_FALSE && new_entry->mapping_enable == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	 
            "%s - Protocol to pri mapping not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    if(new_entry->mapping_enable == MEA_TRUE){
        // check that the profile exist
        
        if( mea_drv_Protocol_To_Priority_Mapping_Addowner(unit,(MEA_Uint16)new_entry->mapping_profile) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	 
                "%s - Protocol to pri mapping id % d failed\n",
                __FUNCTION__,new_entry->mapping_profile);
            return MEA_ERROR;
        }
        
    }
    
    if(old_entry->mapping_enable == MEA_TRUE){
        // check that the profile exist

        if( mea_drv_Protocol_To_Priority_Mapping_Deleteowner(unit,(MEA_Uint16)old_entry->mapping_profile) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,	 
                "%s - Protocol to pri mapping id % d failed\n",
                __FUNCTION__,old_entry->mapping_profile);
            return MEA_ERROR;
        }

    }


	if ( new_entry->mapping_enable )
		parser_entry_val_def.f.mapping_profile = new_entry->mapping_profile ;
	else
		parser_entry_val_def.f.mapping_profile = 0;


    parser_entry_val_def.f.logical_src_port         = new_entry->LAG_src_port_value;
    parser_entry_val_def.f.logical_src_port_replace = new_entry->LAG_src_port_valid;
    parser_entry_val_def.f.ENNI_enable              = new_entry->ENNI_enable;
    parser_entry_val_def.f.l2type_consolidate = new_entry->l2type_consolidate;
    parser_entry_val_def.f.local_MAC_enable = new_entry->local_MAC_enable;  /*set the global mac enable*/

    switch (new_entry->port_proto_prof) {

    case MEA_INGRESS_PORT_PROTO_TRANS:


    parser_entry_val_def.f.transparent_mode = MEA_TRUE;
    parser_entry_val_def.f.or_mask_pri_3_7 =0x1f;
    parser_entry_val_def.f.or_mask_pri=0x7;


        break;
	
    case MEA_INGRESS_PORT_PROTO_VLAN:
    case MEA_INGRESS_PORT_PROTO_TDM:
return MEA_ERROR;
        break;
	
	case MEA_INGRESS_PORT_PROTO_QTAG:
	case MEA_INGRESS_PORT_PROTO_QTAG_OUTER:
return MEA_ERROR;
              break;
	

    case MEA_INGRESS_PORT_PROTO_EoLLCoATM:
return MEA_ERROR;
        break;





    case MEA_INGRESS_PORT_PROTO_IPoEoLLCoATM:
return MEA_ERROR;
        break;


    case MEA_INGRESS_PORT_PROTO_IPoVLAN:
        return MEA_ERROR;
        break;


    case MEA_INGRESS_PORT_PROTO_IP:
return MEA_ERROR;

        break;

    case MEA_INGRESS_PORT_PROTO_PPPoEoVcMUXoATM:
return MEA_ERROR;
        break;

    case MEA_INGRESS_PORT_PROTO_PPPoEoLLCoATM:
return MEA_ERROR;
        break;


    case MEA_INGRESS_PORT_PROTO_EoVcMUXoATM:
 return MEA_ERROR;
        break;

    case MEA_INGRESS_PORT_PROTO_IPoLLCoATM:
return MEA_ERROR;
        break;

    case MEA_INGRESS_PORT_PROTO_IPoVcMUXoATM:
return MEA_ERROR;
        break;


    case MEA_INGRESS_PORT_PROTO_PPPoLLCoATM  :
return MEA_ERROR;
        break;

	
    case MEA_INGRESS_PORT_PROTO_PPPoVcMUXoATM:
          return MEA_ERROR;
        break;

    case MEA_INGRESS_PORT_PROTO_ETHER_TYPE:
        return MEA_ERROR;
        break;
    case MEA_INGRESS_PORT_PROTO_MPLSoE:
         return MEA_ERROR;
        break;
    
    case MEA_INGRESS_PORT_PROTO_DC:

     parser_entry_val_def.f.transparent_mode = MEA_FALSE;
/* set color valid/offset/mask fields if col_aw is on */
if (new_entry->port_col_aw){
    parser_entry_val_def.f.color_valid          = MEA_TRUE;
    
}else{
     parser_entry_val_def.f.color_valid          = MEA_FALSE;
}

/* set the mask_net_field field */
if (new_entry->net_wildcard_valid==MEA_TRUE){
    parser_entry_val_def.f.or_mask_net_tag_0_13   = 
        ((new_entry->net_wildcard             & 0x00003fff) >>  0);
    parser_entry_val_def.f.or_mask_net_tag_14_19  = 
        ((new_entry->net_wildcard             & 0x000fc000) >> 14);
    parser_entry_val_def.f.or_mask_net_tag_20_23  = 
        ((new_entry->net_wildcard             & 0x00f00000) >> 20);
} else {
    parser_entry_val_def.f.or_mask_net_tag_0_13   = 
        ((MEA_OR_NET_TAG_MASK_LABEL_DEF_VAL & 0x00003fff) >>  0);
    parser_entry_val_def.f.or_mask_net_tag_14_19  = 
        ((MEA_OR_NET_TAG_MASK_LABEL_DEF_VAL & 0x000fc000) >> 14);
    parser_entry_val_def.f.or_mask_net_tag_20_23  = 
        ((MEA_OR_NET_TAG_MASK_LABEL_DEF_VAL & 0x00f00000) >> 14);

}


/* set the mask_pri field */
if (new_entry->pri_wildcard_valid==MEA_TRUE){
    parser_entry_val_def.f.or_mask_pri=(new_entry->pri_wildcard & 0x07);
    parser_entry_val_def.f.or_mask_pri_3_7=((new_entry->pri_wildcard )>>3)& 0x1F;
} else {
    if (new_entry->port_cos_aw){
        parser_entry_val_def.f.or_mask_pri  = 0;  
    } else {
        parser_entry_val_def.f.or_mask_pri  = MEA_WILDCARD_ALL_PRI;
    }
}



/* set the mask_src_port field */
if (new_entry->sp_wildcard_valid==MEA_TRUE){
    parser_entry_val_def.f.or_mask_src_port=new_entry->sp_wildcard;
} else {
    parser_entry_val_def.f.or_mask_src_port=0;
}

/************************************************************************/
if (new_entry->net2_wildcard_valid==MEA_TRUE){
    parser_entry_val_def.f.or_mask_net2_0_15 = ((new_entry->net2_wildcard          & 0x0000ffff) >>  0);
    parser_entry_val_def.f.or_mask_net2_16_23 = ((new_entry->net2_wildcard         & 0x00ff0000) >>  16);
}else{
    parser_entry_val_def.f.or_mask_net2_0_15  = 0;
    parser_entry_val_def.f.or_mask_net2_16_23 = 0;
}
if (new_entry->l2_protocol_valid==MEA_TRUE){
    parser_entry_val_def.f.or_mask_L2_Type = ((new_entry->l2_protocol_wildcard         & 0x0000001f) >>  0);
} else {
    parser_entry_val_def.f.or_mask_L2_Type=0;
}
/* check cos_aw and ip_aw  on */
if ((new_entry->port_cos_aw) && (new_entry->port_ip_aw)) {

    /* set pri_rule field for ip */
    parser_entry_val_def.f.pri_rule = MEA_PARSER_PRI_RULE_IP_VLAN_NTAG;
}else{
     parser_entry_val_def.f.pri_rule = MEA_PARSER_PRI_RULE_NTAG_VLAN_IP;
}

    MEA_OS_maccpy (&parser_entry_val_def.f.my_Mac,&new_entry->my_Mac);         


/************************************************************************/




        break;
    
    default: 
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid port protocol profile %d (port=%d)\n",
                          __FUNCTION__,new_entry->port_proto_prof,port);
        return MEA_ERROR; 
    }


   
   

    if ((!MEA_IngressPort_InitDone)  ||
        (MEA_OS_memcmp(&new_entry->fwd_only_DA_value,&old_entry->fwd_only_DA_value,sizeof(MEA_MacAddr)) != 0)) {
        
             if(mea_drv_parser_MAC_filter(unit,port,(new_entry->fwd_only_DA_value))!=MEA_OK){
 
             }        
    
    }
    
    
    /*handle filter interface key*/
    parser_entry_val_def.f.filter_key_interface_type = new_entry->filter_key_interface_type;

    
	parser_entry_val_def.f.DSA_Type_enable = new_entry->DSA_Type_enable;

	parser_entry_val_def.f.L4_SRC_DST            = new_entry->L4_SRC_DST;
    parser_entry_val_def.f.evc_external_internal = new_entry->evc_external_internal;
    parser_entry_val_def.f.def_sid_port_type = new_entry->def_sid_port_type;
    
    if (parser_entry_val_def.f.def_sid_port_type >= MEA_DEF_PORT_TYPE_SID_LAST){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  (port=%d) not support this port_type %d,\n",
            __FUNCTION__, port, parser_entry_val_def.f.def_sid_port_type);
        return MEA_ERROR;
    }



    /* handle IP_pri_mask_type */
    parser_entry_val_def.f.pri_Ip_type = new_entry->IP_pri_mask_type;







    /* Set network L2CP action valid/Table */
    //parser_entry_val_def.f.da_fwrd_l2 = new_entry->network_L2CP_action_valid;
    if (MEA_IngressPort_InitDone == MEA_TRUE){

        if(MEA_GLOBAL_L2CP_SUPPORT){
        /* Set L2CP entry */
            if (MEA_API_Set_NetworkL2CP_Entry(MEA_UNIT_0,
                                              port,
                                              &new_entry->network_L2CP_action_table) != MEA_OK) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - Set Network L2CP  failed (port=%d)\n",
                                __FUNCTION__,port);
               return MEA_ERROR;
            }
        } else {
            {
             // if we not support the user cant change from the default
                if((new_entry->network_L2CP_action_table.reg[0] != 0) ||
                   (new_entry->network_L2CP_action_table.reg[1] != 0) ||
                   (new_entry->network_L2CP_action_table.reg[2] != 0) ||
                   (new_entry->network_L2CP_action_table.reg[3] != 0)  ){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s \n - Set Network L2CP  failed (port=%d) the L2CP is not Support  set all L2cp to PEER\n",
                                __FUNCTION__,port);
                        return MEA_ERROR;
                }
                if(MEA_NEW_PARSER_REDUSE_ENABLE == MEA_FALSE){
                    if((new_entry->default_sid_info.def_sid != 0) ||
                        (new_entry->default_sid_info.action != 0) ) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                     "%s \n- not support to Set port=%d for default service id  \n",
                                    __FUNCTION__,port);
                            return MEA_ERROR;
                    }
                }
            }
        }
    }

    if(MEA_GLOBAL_L2CP_SUPPORT == MEA_FALSE && MEA_NEW_PARSER_REDUSE_ENABLE == MEA_TRUE) // new parser 11/07/2012
    {
        if (new_entry->default_sid_info.def_sid > MEA_MAX_NUM_OF_SERVICES_INTERNAL && new_entry->default_sid_info.action == MEA_DEF_SID_ACTION_DEF_SID){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s \n- DEF_SID is only for Internal service  \n",
                __FUNCTION__, port);
            return MEA_ERROR;
        
        }

    parser_entry_val_def.f.default_sid    = new_entry->default_sid_info.def_sid;
    parser_entry_val_def.f.default_sid_act = new_entry->default_sid_info.action;
     


    }
    
    
    


    retval=MEA_API_Set_Parser_Entry(unit,port,&parser_entry_val_def);
    /* set the parser */

    


	return retval;


}

static void mea_IngressPort_UpdateHw_MAC_fifoEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                  unit_i   = (MEA_Unit_t            )arg1;
  //  MEA_db_HwUnit_t             hwUnit_i = (MEA_db_HwUnit_t       )arg2;
  //  MEA_db_HwId_t               hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_IngressPort_Entry_dbt*  entry    = (MEA_IngressPort_Entry_dbt*)arg4;
    MEA_Uint32                  val[2];
    MEA_Uint32                  i;
   

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
    val[0]= entry->ingress_fifo.regs[0];
    val[1]= entry->ingress_fifo.regs[1];


    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(unit_i,MEA_IF_SRV_MAKE0_REG , val[i] , MEA_MODULE_IF);
    }

}
static void mea_IngressPort_UpdateHw_1588_CPU_OffsetEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                  unit_i   = (MEA_Unit_t            )arg1;
    //  MEA_db_HwUnit_t             hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //  MEA_db_HwId_t               hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_IngressPort_Entry_dbt*  entry    = (MEA_IngressPort_Entry_dbt*)arg4;
    MEA_Uint32                  val[2];
    MEA_Uint32                  i;


    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
   
    val[0] =  entry->ts_cpu_offset.time_stampeL;//32bit
    val[1] =  entry->ts_cpu_offset.time_stampeM;//16bit
    val[1] |= entry->ts_cpu_offset.add_sub<<16;


    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(unit_i,MEA_IF_SRV_MAKE0_REG , val[i] , MEA_MODULE_IF);
    }

}
static MEA_Status mea_IngressPort_UpdateHw_1588_CPU_Offset(MEA_Unit_t                 unit,
                                                    MEA_Port_t                 port,
                                                    MEA_IngressPort_Entry_dbt *entry,
                                                    MEA_IngressPort_Entry_dbt *old_entry)
{
   MEA_ind_write_t             ind_write;
   
   MEA_PortsMapping_t portMap;
   
   
   
   //return MEA_OK;

    if(!MEA_1588_SUPPORT){
      return MEA_OK;
    }
    



    /* 1. convert egress to ingress */
    if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
        MEA_PORTS_TYPE_INGRESS_TYPE,
        (MEA_Uint16)port,
        &portMap)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on egress port= %d \n",port);
            return MEA_ERROR;
    }
    
    if(port == 127){
        return MEA_OK; // not support
    }



    if ((MEA_IngressPort_InitDone != MEA_TRUE            ) ||
        ((old_entry != NULL            ) &&
        (MEA_OS_memcmp(&entry->ts_cpu_offset,&old_entry->ts_cpu_offset,sizeof(entry->ts_cpu_offset))==0 )) ) {
            return MEA_OK;
    }

    /* build the ind_write value */
    ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_1588_CPU_OFFSET;
    ind_write.tableOffset    = port; 
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask        = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg    = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;  

    ind_write.writeEntry    = (MEA_FUNCPTR)mea_IngressPort_UpdateHw_1588_CPU_OffsetEntry;
    ind_write.funcParam1    = 0;
    ind_write.funcParam2    = 0;
    ind_write.funcParam3    = 0;
    ind_write.funcParam4 = (MEA_funcParam)entry;
    if (MEA_API_WriteIndirect(unit,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            MEA_MODULE_IF);
        return MEA_ERROR;
    }





    return MEA_OK;
}



static MEA_Status mea_IngressPort_UpdateHw_protect_1p1(MEA_Unit_t                 unit,
                                                       MEA_Port_t                 port,
                                                       MEA_IngressPort_Entry_dbt *entry,
                                                       MEA_IngressPort_Entry_dbt *old_entry)
{
    MEA_Bool isPortSupport_1p1=MEA_TRUE;
    MEA_Uint32 offset4bitPerPort=0;
    
    MEA_Uint32 port_map=0;
    MEA_Uint32 valToset=0;
    
    if(!MEA_PROTECT_1PLUS1_SUPPORT){
        return MEA_OK;
    }
    
    if ((MEA_IngressPort_InitDone       ) &&
        (old_entry != NULL                               ) &&
        (MEA_OS_memcmp(&entry->Protect_1p1_info,&old_entry->Protect_1p1_info,sizeof(entry->Protect_1p1_info))==0  ) && 
        (entry->ProtectType == old_entry->ProtectType )           ) {
        return MEA_OK;
    }
    

    switch (port)
    {
    case 0  : port_map=offset4bitPerPort=0 ;   break;
    case 12 : port_map=offset4bitPerPort=1 ;  break;
    case 24 : port_map=offset4bitPerPort=2 ;  break;
    case 36 : port_map=offset4bitPerPort=3 ;  break;
    case 48 : port_map=offset4bitPerPort=4 ;  break;
    case 72 : port_map=offset4bitPerPort=5 ;  break;
    case 125: port_map=offset4bitPerPort=6 ;  break;
    case 126: port_map=offset4bitPerPort=7 ;  break;
    case 110: port_map=offset4bitPerPort=8 ;  break;
    case 111: port_map=offset4bitPerPort=9 ;  break;
    case 118: port_map=offset4bitPerPort=10;  break;
    case 119: port_map=offset4bitPerPort=11;  break;

    default:
        isPortSupport_1p1=MEA_FALSE;
        port_map=offset4bitPerPort=15; //not support
        break;

    }


    if(isPortSupport_1p1 == MEA_TRUE){

    

    if(entry->Protect_1p1_info.enable == MEA_TRUE){
        MEA_IngressPort_Protect_1p1_Enable[(port_map)/32] |= ( (0x00000001 << ((port_map)%32)));
    }else{
        MEA_IngressPort_Protect_1p1_Enable[(port_map)/32] &= (~(0x00000001 << ((port_map)%32)));
    }

    
    
   if(entry->Protect_1p1_info.master_secondary == MEA_INGRESS_PORT_PROTECT_1P1_MASTER){
         valToset=0xc; //1100    
    }else{
         valToset=0x3;//0011     
    }
    if(entry->Protect_1p1_info.enable==MEA_FALSE)
      valToset=0;
    

   

    MEA_OS_insert_value((4*offset4bitPerPort), 4,(MEA_Uint32)valToset,&MEA_IngressPort_Protect_1p1_reg[0]);






    if(port_map<=7)    
        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_IG_1P1_P0_REG, MEA_IngressPort_Protect_1p1_reg[0], MEA_MODULE_IF);
    else
        MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_IG_1P1_P1_REG, MEA_IngressPort_Protect_1p1_reg[1], MEA_MODULE_IF);
   
     MEA_API_WriteReg(MEA_UNIT_0, ENET_IF_IG_1P1_ENABLE_REG, MEA_IngressPort_Protect_1p1_Enable[0], MEA_MODULE_IF);
    }

    return MEA_OK;
}
static MEA_Status mea_IngressPort_UpdateHw_MAC_fifo(MEA_Unit_t                 unit,
                                                    MEA_Port_t                 port,
                                                    MEA_IngressPort_Entry_dbt *entry,
                                                    MEA_IngressPort_Entry_dbt *old_entry)
{

    MEA_ind_write_t             ind_write;
    MEA_db_HwUnit_t             hwUnit;
    MEA_Bool rx_enable;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_ERROR;)

// RX_PAUSE
        if (!((MEA_PLATFORM_IS_PAUSE_SUPPORT) || (MEA_FRAGMENT_ENABLE))){
           return MEA_OK;
        } 


    if ((MEA_IngressPort_InitDone != MEA_TRUE            ) ||
        (old_entry == NULL                               ) ||
        (entry->rx_enable != old_entry->rx_enable )) {

            if((old_entry == NULL) || (MEA_OS_memcmp(&entry->ingress_fifo.regs[0],&old_entry->ingress_fifo.regs[0],sizeof(entry->ingress_fifo.regs))!=0  ) ){

            rx_enable=entry->rx_enable;
            MEA_API_Set_IngressPort_RxEnable(unit,port,((rx_enable == MEA_TRUE)? MEA_FALSE : MEA_FALSE));
            MEA_OS_sleep(0,500);

   
   




        /* build the ind_write value */
        ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_FIFO_WATERMARK;
        ind_write.tableOffset    = port; 
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask        = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg        = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg    = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;  

        ind_write.writeEntry    = (MEA_FUNCPTR)mea_IngressPort_UpdateHw_MAC_fifoEntry;
        ind_write.funcParam1    = 0;
        ind_write.funcParam2    = 0;
        ind_write.funcParam3    = 0;
        ind_write.funcParam4 = (MEA_funcParam)entry;
        if (MEA_API_WriteIndirect(unit,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_IF);
            MEA_API_Set_IngressPort_RxEnable(unit,port,rx_enable );

            return MEA_ERROR;
        }

        /* return to caller */
        MEA_API_Set_IngressPort_RxEnable(unit,port,rx_enable );

        return MEA_OK;
       }
    
    
    
    }

    
    // write to table

    /* return to caller */
    return MEA_OK;
}
#if 0
static void mea_UpdateHwEntryBondingRx(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    //MEA_Unit_t              unit_i        = (MEA_Unit_t             )arg1;
    //MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    //MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    MEA_Uint32             data_value     = (MEA_Uint32)arg4;

    MEA_Uint32     val[1];
    MEA_Uint32 i;
    
    MEA_Uint32  count_shift;

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
    count_shift=0;

    MEA_OS_insert_value(count_shift,
        4,
        data_value,
        &val[0]);
    count_shift+=4;

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i) , 
            val[i] , 
            MEA_MODULE_IF);
    }


}
#endif

static void mea_drv_UpdateHwEntry_256PORTS(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Internal_Block_256OutPorts_dbt *entry = (MEA_Internal_Block_256OutPorts_dbt*) arg4;




    MEA_Uint32                 val[8];
    MEA_Uint32                 i = 0;
    MEA_Uint32 num_of_reg  = 8;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i<MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    //count_shift=0;

    MEA_OS_memcpy(&val[0], entry, sizeof(val));
        

    if (MEA_Platform_GetMaxNumOfPort() == 127){
        num_of_reg = 4;
    }

    /* Update Hw Entry */
    for (i = 0; i < num_of_reg ; i++)
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }



}

MEA_Status MEA_Configure_BlockOF256PORTS_TABLE100(MEA_Unit_t unit, MEA_Internal_Block_256OutPorts_dbt *BlockOutPort, MEA_Uint32 offset)
{
    //MEA_db_HwUnit_t       hwUnit;
    
    MEA_ind_write_t      ind_write;

    /* Prepare ind_write structure */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
    ind_write.tableOffset = offset;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_UpdateHwEntry_256PORTS;
    ind_write.funcParam1 = (MEA_funcParam)unit;
    //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
    //ind_write.funcParam3    = (MEA_Uint32);
    ind_write.funcParam4 = (MEA_funcParam)(BlockOutPort);

        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit, &ind_write, ENET_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_IF);
            return ENET_ERROR;
        }
  





    return MEA_OK;
}

MEA_Status MEA_Get_BlockOF256PORTS_TABLE100(MEA_Unit_t unit, MEA_Internal_Block_256OutPorts_dbt *BlockOutPort, MEA_Uint32 offset)
{
    MEA_ind_read_t          ind_read;
    MEA_Uint32              regs[8];
    MEA_Uint32              read_num_of_reg=8;

    ind_read.tableType = ENET_IF_CMD_PARAM_TBL_100_REGISTER;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;

    if (MEA_Platform_GetMaxNumOfPort() == 127){
        read_num_of_reg = 4;
    }
    else{
        read_num_of_reg = MEA_NUM_OF_ELEMENTS(regs);
    }

    ind_read.read_data = (&regs[0]);
    ind_read.tableOffset = offset;
    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        read_num_of_reg,
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&BlockOutPort->regs[0], &regs[0], sizeof(MEA_Internal_Block_256OutPorts_dbt));

    return MEA_OK;
}





static MEA_Status mea_IngressPort_UpdateHwBondingEnable(MEA_Port_t    port,
                                                        MEA_IngressPort_Entry_dbt *entry,
                                                        MEA_IngressPort_Entry_dbt *old_entry)
{

    MEA_Uint32 *pPortGrp;
    MEA_Bool   setVal = 0;
	MEA_Uint32 NumOFShift;

    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;

    if (!MEA_STANDART_BONDING_SUPPORT)
        return MEA_OK;

    /* check for NO change */
    if ((MEA_IngressPort_InitDone != MEA_TRUE) ||
        (old_entry == NULL) ||
        (entry->bonding_group.val.valid != old_entry->bonding_group.val.valid)) 
    {

        pPortGrp = (MEA_Uint32 *)(&(MEA_IngressPort_BondingRX_enable.out_ports_0_31));
        pPortGrp += (port / 32);

        if (entry->bonding_group.val.valid == 1)
            setVal = 1;
        else
            setVal = 0;

		NumOFShift = (port % 32);
        (*pPortGrp) &= ~(0x0000001 << (NumOFShift));
        (*pPortGrp) |= ((setVal) << (NumOFShift));


        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

        if (port < 256) {

            MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_BondingRX_enable.out_ports_0_31, sizeof(BlockOutPort));
            MEA_Configure_BlockOF256PORTS_TABLE100(MEA_UNIT_0, &BlockOutPort, ENET_REGISTER_TBL100_Configure_RX_Bonding_enable);

        }
       
    }
    return MEA_OK;
}

static void mea_UpdateHwEntryBondingRx(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    //count_shift=0;

    val[0] = entry;




    /* Update Hw Entry */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }



}

static MEA_Status mea_swe_ip_drv_UpdateHw_port_fragment_bonding_group(MEA_Unit_t                 unit,
                                                                      MEA_Port_t                 port,
                                                                      MEA_IngressPort_Entry_dbt *entry,
                                                                      MEA_IngressPort_Entry_dbt *old_entry)
{
    MEA_Uint32 val;
    MEA_Uint32 reg;
    MEA_Uint32 seting_val;
    MEA_Uint32 index;
    MEA_Uint32 location;
    


   
    
    if(!MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT && !MEA_STANDART_BONDING_SUPPORT){
        return MEA_OK;
    }
    
    
    if ((MEA_IngressPort_InitDone     ) &&
        (old_entry != NULL            ) &&
        (entry->bonding_group.regs[0] == old_entry->bonding_group.regs[0])  ) {
            return MEA_OK;
    }
  

    if(MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT){


    if(entry->bonding_group.val.valid == MEA_TRUE){

        if (!((port<=31) || (port== 125) ||(port== 126 )|| (port >=100 && port<=107) ))
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  the port %d not support for fragment bonding\n",
                __FUNCTION__,port);
            return MEA_ERROR;

        }

#if 0
        if(entry->bonding_group.val.groupId == 0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -   port %d fragment bonding groupId = 0 not allow\n",
                __FUNCTION__,port);
        return MEA_ERROR;
        }
#endif
        if (entry->bonding_group.val.groupId >= 8 && MEA_GLOBAL_CPE_BONDING_SUPPORT == MEA_TRUE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -   port %d fragment bonding groupId =%d >= 8 not allow\n",
                __FUNCTION__,port,entry->bonding_group.val.groupId);
            return MEA_ERROR;
        }
        if (entry->bonding_group.val.groupId >= 8 && MEA_GLOBAL_CPE_BONDING_SUPPORT == MEA_FALSE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -   port %d fragment bonding groupId =%d >= 8 not allow\n",
                __FUNCTION__,port,entry->bonding_group.val.groupId);
            return MEA_ERROR;
        }
        
                    
    } else{
        if (!((port<=23) || (port== 125) ||(port== 126 )))
        {
            
            return MEA_OK;

        }
    }
    
    seting_val= (entry->bonding_group.val.groupId & 0xf);
    if(entry->bonding_group.val.valid == 0){
    seting_val=0;
    }

        if ((port<=23))
        {
            index=(port/8);
            
            reg=MEA_IF_PORT_RX_TO_GROUP_P7_P0_REG+(index*4);
            val=MEA_IngressPort_fragmant_bonding_port_group[index];
            location= (port -(index*8))*4; 
            
          val  &=  ~(0xf <<location);
          val  |=  seting_val<<(location);
        MEA_IngressPort_fragmant_bonding_port_group[index]=val;
        }else {
            reg=MEA_IF_PORT_RX_TO_GROUP_P127_P125_REG;
            val=MEA_IngressPort_fragmant_bonding_port_group[3];
            if (port == 125)
            {
                val = val & 0xfffffff0;
                val |= seting_val<<0;
             MEA_IngressPort_fragmant_bonding_port_group[3]=val;
            }
            if (port == 126)
            {
                val = val & 0xffffff0f;
                val |= (seting_val)<<4;
                MEA_IngressPort_fragmant_bonding_port_group[3]=val;
            } 
            

        }
        
        
    
    MEA_API_WriteReg(MEA_UNIT_0,reg,
        (val),
        MEA_MODULE_IF);

        /* return to caller */
        return MEA_OK;
    }

    if(MEA_STANDART_BONDING_SUPPORT){
        
        MEA_Uint32             groupId=0;

        MEA_ind_write_t   ind_write;

        if(mea_IngressPort_UpdateHwBondingEnable(port,
            entry,
            old_entry) != MEA_OK) {

            return MEA_ERROR;
        }



        //check the value of the 
        if(entry->bonding_group.val.valid == MEA_TRUE){
            groupId= entry->bonding_group.val.groupId;
            if (groupId  >= MEA_STANDART_BONDING_NUM_OF_GROUP_RX)
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -   port %d RX bonding groupId =%d >= %d not allow\n",
                    __FUNCTION__, port, groupId, MEA_STANDART_BONDING_NUM_OF_GROUP_RX);
                return MEA_ERROR;

            }
            

        }
       
//         /* build the ind_write value */
         ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_BONDING_PORT_TO_GROUT;
         ind_write.tableOffset	= port;
         ind_write.cmdReg = MEA_IF_CMD_REG;
         ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
         ind_write.statusReg = MEA_IF_STATUS_REG;
         ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
         ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
         ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
         ind_write.writeEntry    = (MEA_FUNCPTR) mea_UpdateHwEntryBondingRx;
         ind_write.funcParam1 = (MEA_funcParam)unit;
//         ind_write.funcParam2    = (MEA_funcParam)hwUnit;
//         ind_write.funcParam3    = (MEA_funcParam);
         ind_write.funcParam4 = (MEA_funcParam)((long)groupId);
         if (ENET_WriteIndirect(unit, &ind_write, ENET_MODULE_IF) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                 __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_IF);
             return ENET_ERROR;
         }


    }


return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_IngressPort_UpdateHwMtu>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/  
#if 0
static MEA_Status mea_IngressPort_UpdateHwMtu(MEA_Port_t                port,
                                              MEA_IngressPort_Entry_dbt *entry,
                                              MEA_IngressPort_Entry_dbt *old_entry) 
{
	
#if 0    
    /* check for NO change */
    if ((MEA_IngressPort_InitDone     ) &&
        (old_entry != NULL            ) &&
        (entry->MTU == old_entry->MTU)  ) {
       return MEA_OK;
    }
    
    return mea_IngressPort_Write_Mtu(port,(MEA_Uint32)entry->MTU);
#else
    return MEA_OK;
#endif
}
#endif
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_set_port_ingress_vxlan_cpe  >                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status  mea_drv_set_port_ingress_vxlan_cpe(MEA_Unit_t unit,
    MEA_Port_t port,
    MEA_IngressPort_Entry_dbt *entry,
    MEA_IngressPort_Entry_dbt *old_entry) {


    MEA_Uint32 *pPortGrp;

    //MEA_Uint32 numOfPort;
    MEA_Uint32 numofRegs=4;
    MEA_Uint32 i;
    MEA_Uint32 val[4];

    if (!MEA_VXLAN_SUPPORT){
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_VXLAN_SUPPORT not support\n");
        return MEA_OK;
    }

//     numOfPort = (MEA_Uint32)mea_drv_Get_DeviceInfo_GetMaxNumOfPort(unit, 0) + 1;
//     numofRegs = numOfPort / 32;

    if ((MEA_IngressPort_InitDone != MEA_TRUE) ||
        (old_entry == NULL) ||
        (entry->vxlan_cpe != old_entry->vxlan_cpe)) {


        pPortGrp = (MEA_Uint32 *)(&(MEA_IngressPort_Vxlan_cpe));
        pPortGrp += (port / 32);


        (*pPortGrp) &= ~(0x0000001 << (port % 32));
        (*pPortGrp) |= ((entry->vxlan_cpe) << (port % 32));

        pPortGrp = (MEA_Uint32 *)(&(MEA_IngressPort_Vxlan_cpe));

        for (i = 0; i < numofRegs; i++) {
            val[i] = (*(pPortGrp));
            pPortGrp++;
        }


        if ( mea_drv_write_IF_TBL_X_offset_UpdateEntry_pArray(unit, ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL,
            MEA_GLOBAl_TBL_REG_VXLAN_CPE_port,
            numofRegs,
            val) != MEA_OK) {
            return MEA_ERROR;
        }


    }






    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_IngressPort_UpdateHw>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_IngressPort_UpdateHw(MEA_Unit_t                 unit,
                                           MEA_Port_t                 port,
                                           MEA_IngressPort_Entry_dbt *entry,
                                           MEA_IngressPort_Entry_dbt *old_entry)
{


	MEA_Uint32 *pPortGrp;
    MEA_Uint32 val;
   MEA_Uint32 NumOFShift;
	
    MEA_Uint32 offset = 0;
    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;

    if ((MEA_IngressPort_InitDone != MEA_TRUE            ) ||
        (old_entry == NULL                               ) ||
        (entry->rx_enable         != old_entry->rx_enable)  ) {
MEA_RxEnable_stateWrite =MEA_TRUE;

	   pPortGrp=(MEA_Uint32 *)(&(MEA_IngressPort_Table_rx_enable.out_ports_0_31));
	   pPortGrp += (port/32);

	   NumOFShift = (port % 32);
	   (*pPortGrp) &= ~(0x0000001       <<(NumOFShift));
	   (*pPortGrp) |=  (entry->rx_enable<<(NumOFShift));
       if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
           MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

           /*Write to TBL 100 RX */
           if (port < 256){
               offset = ENET_REGISTER_TBL100_Admin_RX_MAC_0_255;
               MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_0_31, sizeof(BlockOutPort));

           }
		   if (MEA_MAX_PORT_NUMBER_INGRESS >= 256) {
				if (port >= 256 && port < 512) {
			   offset = ENET_REGISTER_TBL100_Admin_RX_MAC_256_511;
#if   MEA_OUT_PORT_256_511    
               MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_256_287, sizeof(BlockOutPort));
#endif
				}
		   }
		   if (MEA_MAX_PORT_NUMBER_INGRESS >= 512) {
				if (port >= 512 && port < 768) {
				   offset = ENET_REGISTER_TBL100_Admin_RX_MAC_512_767;
#if   MEA_OUT_PORT_512_1023   
                   MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_512_543, sizeof(BlockOutPort));
#endif
                }
		   }
		   if (MEA_MAX_PORT_NUMBER_INGRESS >= 768) {
			   if (port >= 768 && port < 1024) {
#if   MEA_OUT_PORT_512_1023   
				   offset = ENET_REGISTER_TBL100_Admin_RX_MAC_768_1023;
				   MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_768_799, sizeof(BlockOutPort));
#endif
               }
		   }

           /*config ENET_REGISTER_TBL100_Admin_RX */
           MEA_Configure_BlockOF256PORTS_TABLE100(unit, &BlockOutPort, offset);



       } else {

           return MEA_ERROR;
       }


MEA_RxEnable_stateWrite = MEA_FALSE;

	}    


    if ((MEA_IngressPort_InitDone != MEA_TRUE            ) ||
        (old_entry == NULL                               ) ||
        (entry->crc_check         != old_entry->crc_check)  ) {


	   pPortGrp=(MEA_Uint32 *)(&(MEA_IngressPort_Table_crc_check.out_ports_0_31));
	   pPortGrp += (port/32);

	   NumOFShift = (port % 32);
	   (*pPortGrp) &= ~(0x0000001       <<(NumOFShift));
	   (*pPortGrp) |=  (entry->crc_check<<(NumOFShift));
       if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
           MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

           /*Write to TBL 100 RX */
           if (port < 256){
               offset = ENET_REGISTER_TBL100_RX_CRC_CHECK_MAC_0_255;
               MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_crc_check.out_ports_0_31, sizeof(BlockOutPort));

           }
           else if (port >= 256 && port < 512){
#ifdef MEA_OUT_PORT_256_511
               offset = ENET_REGISTER_TBL100_RX_CRC_CHECK_MAC_256_511;
               MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_crc_check.out_ports_256_287, sizeof(BlockOutPort));
#endif
           }
#if 0
           else if (port >= 512 && port < 768){
               offset = ENET_REGISTER_TBL100_RX_CRC_CHECK_MAC_512_768;

           }
           else if (port >= 768 && port < 1024){
               offset = ENET_REGISTER_TBL100_RX_CRC_CHECK_MAC_768_1024;

           }
#endif

           /* Config ENET_REGISTER_TBL100_RX_CRC_CHECK_MAC */
           MEA_Configure_BlockOF256PORTS_TABLE100(unit, &BlockOutPort, offset);


       } else {
           MEA_API_WriteReg(MEA_UNIT_0,
               ENET_IF_PORT_RX_CRC_CHECK_31_0_REG + ((port / 32) * 4),
               (*pPortGrp),
               MEA_MODULE_IF);
       }


    }    

#if 0 /*need to remove to Table register */
    if (((MEA_IngressPort_InitDone != MEA_TRUE         ) ||
         (old_entry == NULL                            ) ||
         (entry->utopia            != old_entry->utopia)  ) &&
	    (port <= 95)) {

	   pPortGrp=(MEA_Uint32 *)(&(MEA_IngressPort_Table_utopia.out_ports_0_31));
	   pPortGrp += (port/32);

   
	   (*pPortGrp) &= ~(0x0000001    <<(port%32));
	   (*pPortGrp) |=  (entry->utopia<<(port%32));


       MEA_API_WriteReg(MEA_UNIT_0,
                        ENET_IF_PORT_UTOPIA_POS_31_0_REG +((port/32)*4)	,
	                    (*pPortGrp),
                        MEA_MODULE_IF);

    }    
#endif
#if 0   /*Move to interface*/
	if ((MEA_IngressPort_InitDone != MEA_TRUE         ) ||
        (old_entry == NULL                            ) ||
        (entry->autoneg_enable != old_entry->autoneg_enable) ||
		(entry->GigaDisable    != old_entry->GigaDisable)  ) {

	   pPortGrp=(MEA_Uint32 *)(&(MEA_IngressPort_Table_GigaDisable.out_ports_0_31));
	   pPortGrp += (port/32);

        val= (entry->GigaDisable == MEA_INGRESSPORT_GIGA_DISABLE_GIGA   || 
			  entry->GigaDisable == MEA_INGRESSPORT_GIGA_DISABLE_10G    ||
			  entry->GigaDisable == MEA_INGRESSPORT_GIGA_DISABLE_2dot5G  ) ?  0 : 1 ;
	   
       (*pPortGrp) &= ~(0x0000001    <<(port%32));
       (*pPortGrp) |=  ( val <<(port%32));
 
       MEA_API_WriteReg(MEA_UNIT_0,
                        (ENET_IF_VER_GBE_FAST31_0_REG+(port/32)*4),
	                    (*pPortGrp),
                        MEA_MODULE_IF);
   
//           IF register 0x1F8.
//            Bit 0: decimation valid, if set to 0, then we are in 1G mode, else 10M or 100M.
//            Bit 1 : 10M = '0', 100M = '1'.
//            Bit 2 : auto-neg 0-disable 1-enable.
//            Bits[11:4] : port number.

//            Bit 11 : if set to 1, then we are in read mode of the configuration of a port.

       speed10_100 = 0;
       speed10_100 = (entry->autoneg_enable << 2);

       if (entry->GigaDisable == MEA_INGRESSPORT_GIGA_DISABLE_GIGA){
           speed10_100 = 0;
       }
       else
       {
           speed10_100 = 1;; 
           if (entry->GigaDisable == MEA_INGRESSPORT_GIGA_DISABLE_100M){
               speed10_100 |= (1 << 1);
           }
           else {
               /*SPEED 10M*/
               
           }


       }


        
         
        speed10_100 |= (port<<4);
        speed10_100 |= (1<<12);  /* Write mode */
#if 0 /*Move to interface*/
       MEA_API_WriteReg(MEA_UNIT_0,(ENET_IF_SPEED_PORT_REG), (speed10_100), MEA_MODULE_IF);

     //  MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "Ingress port %d 0x1f8 =0x%08x \n", port, speed10_100);
#endif        
        

        
    }
#endif   

        
    if (mea_IngressPort_UpdateHw_MAC_fifo(unit,port,entry,old_entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_IngressPort_UpdateHw_MAC_fifo failed (port=%d)\n",
                    __FUNCTION__,port);
                return MEA_ERROR;
    }
       
        // check if the bonding port support
    if(mea_swe_ip_drv_UpdateHw_port_fragment_bonding_group(unit,port,entry,old_entry)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - fragment_bonding failed \n",
                __FUNCTION__);
            return MEA_ERROR;
    }
      // 1588_support
    if(mea_IngressPort_UpdateHw_1588_CPU_Offset(unit,port,entry,old_entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_IngressPort_UpdateHw_1588_CPU_Offset failed (port=%d)\n",
                __FUNCTION__,port);
            return MEA_ERROR;
        }

/* auto-neg enable */
      if(MEA_AUTONEG_SUPPORT){
          if ((MEA_IngressPort_InitDone != MEA_TRUE         ) ||
              (old_entry == NULL                            ) ||
              (entry->autoneg_enable    != old_entry->autoneg_enable)  ) {

                  val=entry->autoneg_enable;

                  pPortGrp=(MEA_Uint32 *)(&(MEA_IngressPort_Table_Autoneg.out_ports_0_31));
                  pPortGrp += (port/32);

                 
                  (*pPortGrp) &= ~(0x0000001    <<(port%32));
                  (*pPortGrp) |=  ( val <<(port%32));

                  MEA_API_WriteReg(MEA_UNIT_0,
                      (ENET_IF_AUTONEGOTIATE_ENABLE_31_0_REG+(port/32)*4),
                      (*pPortGrp),
                      MEA_MODULE_IF);
          }
      }

#if 1
      if(mea_IngressPort_UpdateHw_protect_1p1(unit,port,entry,old_entry) != MEA_OK) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
              "%s - mea_IngressPort_UpdateHw_protect_1p1 failed (port=%d)\n",
              __FUNCTION__,port);
          return MEA_ERROR;
      }

#endif



    /* return to caller */
	return MEA_OK;


}



/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_IngressPort_Table>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_IngressPort_Table(MEA_Unit_t unit_i)
{
    
    MEA_Port_t port;
    MEA_IngressPort_Entry_dbt IngressPort_Entry;
    MEA_Uint32 i;
	MEA_Port_type_te port_type;
    MEA_PortsMapping_t  portMap;
    MEA_Uint16 ingress_mac_thersh=0;
	MEA_Uint32 size;

    if(b_bist_test){
        return MEA_OK;
    }

MEA_IngressPort_Protect_1p1_reg[0]=0;
MEA_IngressPort_Protect_1p1_reg[1]=0;
MEA_IngressPort_Protect_1p1_Enable[0]=0;


    /* Port oriented settings */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize ingress port tables ...\n");

    /* Indicate we start init process (I.E. Update always the hardware) */
    MEA_IngressPort_InitDone = MEA_FALSE;

    /*vxlan cpe */
    
     
    


    MEA_OS_memset(&(MEA_IngressPort_Vxlan_cpe), 0, sizeof(MEA_IngressPort_Vxlan_cpe));

	

	size = (MEA_MAX_PORT_NUMBER_INGRESS + 1) * sizeof(MEA_IngressPort_Entry_dbt);
	MEA_IngressPort_Table = (MEA_IngressPort_Entry_dbt*)MEA_OS_malloc(size);
	if (MEA_IngressPort_Table == NULL) {
		MEA_OS_free(MEA_IngressPort_Table);
		MEA_IngressPort_Table = NULL;

		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s \n- Allocate MEA_IngressPort_Table failed (size=%d)\n",
			__FUNCTION__,
			size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_IngressPort_Table, 0, size);
    
    /* Zeros the Ingress Port Table SW shadow */
   
    MEA_OS_memset(&(MEA_IngressPort_Table_rx_enable),0,sizeof(MEA_IngressPort_Table_rx_enable));
    MEA_OS_memset(&(MEA_IngressPort_Table_GigaDisable),0,sizeof(MEA_IngressPort_Table_GigaDisable));
    MEA_OS_memset(&MEA_IngressPort_fragmant_bonding_port_group[0],0,sizeof(MEA_IngressPort_fragmant_bonding_port_group));

    MEA_OS_memset(&MEA_IngressPort_BondingRX_enable,0, sizeof(MEA_IngressPort_BondingRX_enable));

    
    for(i=MEA_INGRESS_PORT_POLICER_TYPE_MC;i< MEA_INGRESS_PORT_POLICER_TYPE_LAST ;i++){
    IngressPort_Entry.policer_vec[i].tmId_enable=MEA_PORT_RATE_MEETRING_DISABLE_DEF_VAL;
    IngressPort_Entry.policer_vec[i].tmId=0;
    MEA_OS_memset(&IngressPort_Entry.policer_vec[i].sla_params,0,sizeof(MEA_Policer_Entry_dbt));
    IngressPort_Entry.policer_vec[i].sla_params.CIR=100000000;
    IngressPort_Entry.policer_vec[i].sla_params.CBS=32000; 
    IngressPort_Entry.policer_vec[i].sla_params.comp=20; 
    } 



    for (port=0;port<=MEA_MAX_PORT_NUMBER_INGRESS;port++) {

	    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }
		if (port == 128) {
			port = 128;
		}
        /* Get the current IngressPort Entry */
        if (MEA_API_Get_IngressPort_Entry(unit_i,
                                          port,
                                          &IngressPort_Entry) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_GetIngressPort_Entry for port %d failed\n",
                              __FUNCTION__,port);
            return MEA_ERROR;
        }


        IngressPort_Entry.rx_enable = MEA_INGRESS_PORT_RX_ENABLE_DEF_VAL(port);
        IngressPort_Entry.utopia    = MEA_INGRESS_PORT_UTOPIA_ENABLE_DEF_VAL(port);
        IngressPort_Entry.crc_check = MEA_INGRESS_PORT_CRC_CHECK_DEF_VAL(port);
        IngressPort_Entry.MTU       = MEA_INGRESS_PORT_MTU_DEF_VAL(port);


		if (MEA_API_Get_IngressPort_Type(unit_i,port,&port_type) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_Get_IngressPort_Type failed (port=%d)\n",
				              __FUNCTION__,port);
			return MEA_ERROR;
		}

        /* check the interface type */
        
		IngressPort_Entry.GigaDisable = MEA_INGRESS_PORT_GIGA_DEF_VAL(port,port_type);
        if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
			MEA_PORTS_TYPE_INGRESS_TYPE,
            port,
            &portMap)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -   \n",
                    __FUNCTION__);
                return ENET_ERROR;
        }
        if(portMap.PortTo_interfaceId_Type == MEA_INTERFACETYPE_RMII ||
           portMap.PortTo_interfaceId_Type == MEA_INTERFACETYPE_MII){
             IngressPort_Entry.GigaDisable= MEA_INGRESSPORT_GIGA_DISABLE_100M;
        }
        
        
        IngressPort_Entry.autoneg_enable = MEA_INGRESS_PORT_AUTONEG_DEF_VAL(port,port_type);





#if 0
        /* set default port protocol */
        if(!IngressPort_Entry.utopia ){
            if(port_type != MEA_PORTTYPE_E1_T1_T1_193B)
                IngressPort_Entry.parser_info.port_proto_prof = MEA_INGRESS_PORT_PROTO_DEF_VAL(port);
            else{
                IngressPort_Entry.parser_info.port_proto_prof = MEA_PORTTYPE_E1_T1_T1_193B;
            }
        }else{
        IngressPort_Entry.parser_info.port_proto_prof= MEA_INGRESS_PORT_PROTO_EoLLCoATM;
        }
#else
		if (IngressPort_Entry.utopia == MEA_TRUE) {
			if (!MEA_UTOPIA_SUPPORT) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s - \n",
					__FUNCTION__);
				return ENET_ERROR;
				
			}
		}
		IngressPort_Entry.parser_info.port_proto_prof = MEA_INGRESS_PORT_PROTO_DEF_VAL(port);
#endif

        IngressPort_Entry.parser_info.port_cos_aw = MEA_INGRESS_PORT_COS_AW_DEF_VAL(port);
        IngressPort_Entry.parser_info.port_ip_aw = MEA_INGRESS_PORT_IP_AW_DEF_VAL(port);
        IngressPort_Entry.parser_info.port_col_aw = MEA_INGRESS_PORT_COL_AW_DEF_VAL(port);


        /* set default tag / untag allowed */
		IngressPort_Entry.parser_info.allowed_tagged = 
		          MEA_PARSER_ALLOWED_TAG_DEFAULT_VAL;
	    IngressPort_Entry.parser_info.allowed_untagged = 
		          MEA_PARSER_ALLOWED_UNTAG_DEFAULT_VAL;

        /* set default user/network_L2CP_action_table default values */

		/* set default sid default values */
		IngressPort_Entry.parser_info.default_sid_info.action = MEA_DEF_SID_ACTION_DISCARD;
		IngressPort_Entry.parser_info.default_sid_info.def_sid = 0;

		/* set default mapping status */
        
		IngressPort_Entry.parser_info.mapping_enable = MEA_INGRESS_PORT_MAPPING_ENABLE_DEF_VAL(port);
		IngressPort_Entry.parser_info.mapping_profile = MEA_INGRESS_PORT_MAPPING_PROF_DEF_VAL(port);
		/* set default IPv6 enable */
		IngressPort_Entry.parser_info.IP_pri_mask_IPv6 = MEA_INGRESS_PORT_IPv5_ENABLE_DEF_VAL;
	
		
          MEA_OS_memset(&IngressPort_Entry.parser_info.network_L2CP_action_table.reg, 
			            MEA_NETWORK_L2CP_ACTION_8_SUFFIXES_DEF_VAL(port),
						sizeof(IngressPort_Entry.parser_info.network_L2CP_action_table)); /* set all port entry to default */

		/* set default CFM_OAM_enable */
        IngressPort_Entry.parser_info.cfm_oam_qtag = MEA_INGRESS_PORT_CFM_OAM_QTAG_DEF_VAL(port);
        IngressPort_Entry.parser_info.cfm_oam_tag = MEA_INGRESS_PORT_CFM_OAM_TAG_DEF_VAL(port);
        IngressPort_Entry.parser_info.cfm_oam_untag = MEA_INGRESS_PORT_CFM_OAM_UNTAG_DEF_VAL(port);
 
//////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////
        
        {

        
        MEA_Uint32 WH=35; 
        MEA_Uint32 WL=25;
        
        if(MEA_DRV_BM_BUS_WIDTH == 0){//64bit
            if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                WH = 768;
                WL = 256;
            }else{
                WH = 2304;
                WL = 1792;
            }
        
        }else{
         if(MEA_DRV_BM_BUS_WIDTH == 1){ //128bit
             if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                 WH = 384;
                 WL = 128;
             }else{
                 WH = 1152;
                 WL = 896;
             }
         }
        }
        

        IngressPort_Entry.ingress_fifo.val.Water_mark_high_thresh = WH;
                //MEA_INGRESS_FIFO_MAC_WATHER_HI_DEF_VAL(mea_drv_Get_DeviceInfo_ingress_extended_mac_fifo(unit_i,port)); 
        IngressPort_Entry.ingress_fifo.val.Water_mark_low_thresh =WL;
                //MEA_INGRESS_FIFO_MAC_WATHER_LO_DEF_VAL(mea_drv_Get_DeviceInfo_ingress_extended_mac_fifo(unit_i,port)); 
     
        
        }
        
        if(mea_drv_Get_DeviceInfo_ingress_extended_mac_fifo(unit_i,port)== MEA_TRUE){
            if(MEA_DRV_BM_BUS_WIDTH == 0){//64bit
                if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                   ingress_mac_thersh =  990;
                }else{
                    ingress_mac_thersh =  4066;
                }
            }
            if(MEA_DRV_BM_BUS_WIDTH == 1 || MEA_DRV_BM_BUS_WIDTH == 2){//128bit
                if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                    ingress_mac_thersh =  482;
                }else{
                    ingress_mac_thersh =  2018;
                }
            }


        }else {
            if(MEA_DRV_BM_BUS_WIDTH == 0){//64bit
                if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                    ingress_mac_thersh =  290;
                }else{
                    ingress_mac_thersh=  990;
                }
            }
            if(MEA_DRV_BM_BUS_WIDTH == 1 || MEA_DRV_BM_BUS_WIDTH == 2){//128bit
                if(MEA_DRV_INGRESS_MAC_FIFO_SMALL == MEA_TRUE){
                    ingress_mac_thersh =  226;
                }else{
                    ingress_mac_thersh =  482;
                }
            }

        }
        
        IngressPort_Entry.ingress_fifo.val.ingress_mac_thersh = (ingress_mac_thersh/8);


          
    
        IngressPort_Entry.ingress_fifo.val.fragment     = MEA_INGRESS_FIFO_MAC_FRAGMENT_DEF_VAL(port);
        IngressPort_Entry.ingress_fifo.val.pause_enable = MEA_INGRESS_FIFO_MAC_PAUSE_DEF_VAL(port);
        IngressPort_Entry.ingress_fifo.val.working_mode = MEA_INGRESS_FIFO_MAC_WORKNIG_MODE_DEF_VAL(port);

        
        {

            MEA_Bool    vlanrange_valid=MEA_FALSE;
            MEA_Uint16  vlanrange_id=0;
            if ((port_type != MEA_PORTTYPE_G999_1MAC) && 
                (port_type != MEA_PORTTYPE_G999_1MAC_1588) && 
				(port_type != MEA_PORTTYPE_VIRTUAL)
				)
            {
                if (mea_drv_vlanRange_Mapping_profile(unit_i, port, &vlanrange_valid, &vlanrange_id) == MEA_OK){
                    IngressPort_Entry.parser_info.vlanrange_enable = vlanrange_valid;
                    IngressPort_Entry.parser_info.vlanrange_prof_Id = vlanrange_id;
                }
            }
            
        }

        IngressPort_Entry.parser_info.filter_key_interface_type = MEA_INGRESS_PORT_FILTER_SRC_SID_DEF_VAL(port);
        IngressPort_Entry.parser_info.lag_distribution_mask     = 0xf;
		if (port == 128) {
			port = 128;
		}
		/* Set the Ingress Port Entry */
        if (MEA_API_Set_IngressPort_Entry(unit_i,
                                          port,
                                          &IngressPort_Entry) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_SetIngressPort_Entry for port %d failed\n",
                              __FUNCTION__,port);
            return MEA_ERROR;
        }

        
       
    
    }//



    /* Indicate we finish init process (I.E. Update hardware only if change) */
    MEA_IngressPort_InitDone = MEA_TRUE;

    /* Return to caller */
    return MEA_OK; 

}

MEA_Status mea_drv_Counclude_IngressPort_Table(MEA_Unit_t unit_i) {

	
	if (MEA_IngressPort_Table != NULL) 
	{
		MEA_OS_free(MEA_IngressPort_Table);
	}
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_IngressPort_Table>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_IngressPort_Table(MEA_Unit_t unit_i)
{

    MEA_Port_t port;


    /* Save the current rx enable */
    MEA_OS_memcpy(&MEA_IngressPort_Table_rx_enable_save_for_ReInit,
                  &MEA_IngressPort_Table_rx_enable,
                  sizeof(MEA_IngressPort_Table_rx_enable_save_for_ReInit));
    MEA_OS_memset(&MEA_IngressPort_Table_rx_enable,
                  0,
                  sizeof(MEA_IngressPort_Table_rx_enable));

    /* ReInit all ingress ports */
    for (port=0;port<MEA_NUM_OF_ELEMENTS(MEA_IngressPort_Table);port++) {

	    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)].rx_enable = MEA_FALSE;

        if (mea_IngressPort_UpdateHw(unit_i,
                                    port,
                                     &(MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)]),
                                     NULL) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_IngressPort_UpdateHw failed (port=%d)\n",
                              __FUNCTION__,port);
            return MEA_ERROR;
        }
        if(mea_drv_Get_DeviceInfo_IsPortPolicerSupport(unit_i,0)){
            if (mea_drv_set_port_ingress_policer(unit_i,port,
		                                         &(MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)]),
                                                 NULL) != MEA_OK){
        	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Ingress Port Update policer for port %d failed \n",
                                  __FUNCTION__,port);
		        return MEA_ERROR;
            }
        }

    }

    if (mea_drv_set_port_ingress_vxlan_cpe(unit_i, port,
        &(MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)]),
        NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Ingress Port Update vxlan_cpe for port %d failed \n",
            __FUNCTION__, port);
        return MEA_ERROR;
    }


    /* Return to caller */
    return MEA_OK; 

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_RxDisable>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status mea_drv_ReInit_RxDisable(MEA_Unit_t unit_i)
{


   
    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
//    MEA_Uint32  i;
    /* Restore the rx enable */
    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

        MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_0_255);

        if (MEA_MAX_PORT_NUMBER_INGRESS >= 256  ){
            MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_256_511);
        }

		if (MEA_MAX_PORT_NUMBER_INGRESS >= 512 ) {
			
			MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_512_767);

		}

		if (MEA_MAX_PORT_NUMBER_INGRESS >= 768 ) {
			MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_768_1023);
		}




    } else {
        

        /* Restore the rx enable */
        return MEA_ERROR;

    }



    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_RxEnable>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_RxEnable(MEA_Unit_t unit_i)
{


    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
    
  
    MEA_Port_t  port;


/************************************************************************/
/* Restore link mask protection                                         */
/************************************************************************/


	
/************************************************************************/
/*                                                                      */
/************************************************************************/	
	/* Restore the rx enable */
    MEA_OS_memcpy(&MEA_IngressPort_Table_rx_enable,
                  &MEA_IngressPort_Table_rx_enable_save_for_ReInit,
                  sizeof(MEA_IngressPort_Table_rx_enable));
    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        /*Write to TBL 100 RX */
        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

        MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_0_31, sizeof(BlockOutPort));
        MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_0_255);

        if (MEA_MAX_PORT_NUMBER >= 256){
            MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
#ifdef MEA_OUT_PORT_256_511
            MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_256_287, sizeof(BlockOutPort));
            MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_256_511);
#endif        
        }
#ifdef MEA_OUT_PORT_512_1023
		if (MEA_MAX_PORT_NUMBER_INGRESS >= 512) {
			MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_512_543, sizeof(BlockOutPort));
			MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_512_767);

		}

		if (MEA_MAX_PORT_NUMBER_INGRESS >= 768) {
			MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_768_799, sizeof(BlockOutPort));

			MEA_Configure_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_768_1023);
		}
#endif


    } else {
        return MEA_ERROR;

    }
    /* Update the software shadow */
    for (port=0;port<MEA_NUM_OF_ELEMENTS(MEA_IngressPort_Table);port++) {

	    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        if (MEA_IS_SET_OUTPORT(&MEA_IngressPort_Table_rx_enable,port)) {
            MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)].rx_enable = MEA_TRUE;
        }

    }


    /* Return to caller */
    return MEA_OK; 

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Bool MEA_API_Set_IngressPort_Is_AdminUp(MEA_Unit_t unit, MEA_Port_t port)
{
    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        return MEA_FALSE; 
    }

    return MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)].rx_enable;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_IngressPort_Entry>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_IngressPort_Entry(MEA_Unit_t                 unit,
                                         MEA_Port_t                 port,
                                         MEA_IngressPort_Entry_dbt* entry) 
{



#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    MEA_Port_type_te port_type;

#endif

	MEA_API_LOG 

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check for valid port parameter */
	if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_FALSE)==MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - port %d is invalid\n",
                         __FUNCTION__,port);
       return MEA_ERROR; 
    }


    /* Check for valid entry parameter */
	if (entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry == NULL (port=%d)\n",
                         __FUNCTION__,port);
       return MEA_ERROR; 
    }

    /* Check for change speed */
    if ((MEA_IngressPort_InitDone) &&
        (entry->GigaDisable != MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)].GigaDisable)) {
        
        if (MEA_API_Get_IngressPort_Type(unit,port,&port_type) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_IngressPort_Type failed (port=%d)\n",
                              __FUNCTION__,port);
            return MEA_ERROR;
        }
        switch(port_type) {
       
        case MEA_PORTTYPE_G999_1MAC:
        case MEA_PORTTYPE_G999_1MAC_1588:
        case MEA_PORTTYPE_GBE_MAC:
        case MEA_PORTTYPE_GBE_1588:
        case MEA_PORTTYPE_E1_T1_T1_193B:
            switch (entry->GigaDisable) {
            case MEA_INGRESSPORT_GIGA_DISABLE_GIGA:
                 break;
            case MEA_INGRESSPORT_GIGA_DISABLE_100M:
            case MEA_INGRESSPORT_GIGA_DISABLE_10M:

                break;
            case MEA_PORTTYPE_10G_XMAC_ENET:
            
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Unknown1 speed %d when check if allowed to change speed (port=%d,type=%d)\n",
                                  __FUNCTION__,entry->GigaDisable,port,port_type);
                return MEA_ERROR;
                break;
            }
            break;
         
       
        case MEA_PORTTYPE_AAL5_ATM_TC:
       
        case MEA_PORTTYPE_AURORA_MAC:
        case MEA_PORTTYPE_10G_XMAC_ENET:
        
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Speed not allowed to change with this port type (port=%d,type=%d)\n",
                              __FUNCTION__,port,port_type);
            return MEA_ERROR;
            break;
        case MEA_PORTTYPE_LAST:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown2 port_type %d when check if allowed to change speed (port=%d)\n",
                              __FUNCTION__,port_type,port);
            return MEA_ERROR;
        }
    }


    /* Check for change utopia (eth/atm) */
    if ((MEA_IngressPort_InitDone) &&
        (entry->utopia != MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)].utopia)) {
        MEA_Port_type_te port_type;
        if (MEA_API_Get_IngressPort_Type(unit,port,&port_type) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_IngressPort_Type failed (port=%d)\n",
                              __FUNCTION__,port);
            return MEA_ERROR;
        }
        switch(port_type) {
        case MEA_PORTTYPE_AAL5_ATM_TC:
            if (entry->utopia != 1) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - port type %d allowed only cell mode , utopia==1 (port=%d)\n",
                                  __FUNCTION__,port_type,port);
                return MEA_ERROR;
            }
            break;
        
        
        case MEA_PORTTYPE_GBE_MAC:
        case MEA_PORTTYPE_GBE_1588:
        
        case MEA_PORTTYPE_AURORA_MAC:
        case MEA_PORTTYPE_10G_XMAC_ENET:
        
            if (entry->utopia != 0) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - port type %d allowed only packet mode , utopia==0 (port=%d)\n",
                                  __FUNCTION__,port_type,port);
                return MEA_ERROR;
            }
            break;
        case MEA_PORTTYPE_LAST:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Unknown port_type %d when check if allowed to change utopia (packet/cell mode) (port=%d)\n",
                              __FUNCTION__,port_type,port);
            return MEA_ERROR;
        }
    }

  if ((entry->parser_info.port_cos_aw == MEA_FALSE) && 
	  (entry->parser_info.port_ip_aw ==MEA_FALSE)) 
  {
	
	 if (entry->parser_info.pri_wildcard_valid == MEA_TRUE)
		{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  pri Wild card must be disable at port = %d \n because cos_aw and ip_aw are disable\n",__FUNCTION__,port); 
                          
        return MEA_ERROR;
		
		}
	 }

    /* check valid protocol and port for the protocol */
    switch (entry->parser_info.port_proto_prof) {

    case MEA_INGRESS_PORT_PROTO_TRANS:
    case MEA_INGRESS_PORT_PROTO_VLAN:
    case MEA_INGRESS_PORT_PROTO_IPoVLAN:
    case MEA_INGRESS_PORT_PROTO_IP:
    case MEA_INGRESS_PORT_PROTO_QTAG:
	case MEA_INGRESS_PORT_PROTO_QTAG_OUTER:
    case MEA_INGRESS_PORT_PROTO_MARTINI:
	case MEA_INGRESS_PORT_PROTO_ETHER_TYPE:
    case MEA_INGRESS_PORT_PROTO_MPLS:
    case MEA_INGRESS_PORT_PROTO_MPLSoE:
    case MEA_INGRESS_PORT_PROTO_DC:
    case MEA_INGRESS_PORT_PROTO_TDM:
#if 0
        if (entry->utopia == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - protocol %d not allowed for non ETH port \n",
                              __FUNCTION__,entry->parser_info.port_proto_prof);
            return MEA_ERROR;
        }
#endif
        break;


    case MEA_INGRESS_PORT_PROTO_EoLLCoATM:
    case MEA_INGRESS_PORT_PROTO_EoVcMUXoATM:
    case MEA_INGRESS_PORT_PROTO_IPoLLCoATM:
    case MEA_INGRESS_PORT_PROTO_IPoVcMUXoATM:
    case MEA_INGRESS_PORT_PROTO_PPPoLLCoATM  :
	case MEA_INGRESS_PORT_PROTO_PPPoVcMUXoATM:
		if (entry->parser_info.LAG_src_port_valid) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - LAG source port replace not valid for ATM Protocol (%d) \n",
                              __FUNCTION__,entry->parser_info.port_proto_prof);
            return MEA_ERROR;
		}
    case MEA_INGRESS_PORT_PROTO_IPoEoLLCoATM:
    case MEA_INGRESS_PORT_PROTO_PPPoEoLLCoATM:
    case MEA_INGRESS_PORT_PROTO_PPPoEoVcMUXoATM:

#if 0
        if (entry->utopia != MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - protocol %d not allowed for non ATM port \n",
                              __FUNCTION__,entry->parser_info.port_proto_prof);
            return MEA_ERROR;
        }
#endif
        break;



    default: 
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid port protocol profile %d (port=%d)\n",
                          __FUNCTION__,entry->parser_info.port_proto_prof,port);
        return MEA_ERROR; 
    }


    /* verify that if IP, IPoVLAN, IPoEoLLCoATM, PPPoEoLLCoATM ,
	               IPoLLCoATM, IPoVcMUXoATM,PPPoLLCoATM  , PPPoVcMUXoATM
				   were configured, ip_aw is ON */
    if ( (entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_IP             ||
          entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_IPoVLAN        || 
          entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_IPoEoLLCoATM   ||
        //entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_PPPoEoLLCoATM  ||
        //entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_PPPoEoVcMUXoATM  ||
          entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_IPoLLCoATM     ||
          entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_IPoVcMUXoATM   )
        //entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_PPPoLLCoATM   ||
        //entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_PPPoVcMUXoATM )
        && (entry->parser_info.port_ip_aw == 0) )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - In case of protocol %d, ip_aw should be ON\n",
                          __FUNCTION__,entry->parser_info.port_proto_prof);
        return MEA_ERROR; 
    }        

    /* verify that if ip_aw on also cos_aw is on */    
    if ((entry->parser_info.port_ip_aw) && (!entry->parser_info.port_cos_aw)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ip_aw on is allowed only if cos_aw on (port=%d)\n",
                          __FUNCTION__,port);
    }

	/* verify that if ether_type only tag or untag ( but not both ) */
	if (entry->parser_info.port_proto_prof == MEA_INGRESS_PORT_PROTO_ETHER_TYPE) {
		if ((entry->parser_info.allowed_tagged) &&
			(entry->parser_info.allowed_untagged)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Tag+UnTag is not supported in EtherType protocol\n",
                          __FUNCTION__);
		        return MEA_ERROR; 

		}

	}


    /* Check for maximum MTU parameter */
    if(MEA_INGRESS_PORT_MTU_SUPPORT == MEA_TRUE){
        /* Verify granularity */       
       if(entry->MTU % MEA_INGRESS_PORT_MTU_GRANULARITY_VALUE != 0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Port ingress %d mtu %d need to be in granularity of %d \n",
                           __FUNCTION__,port,(entry->MTU),MEA_INGRESS_PORT_MTU_GRANULARITY_VALUE);
         return MEA_ERROR; 
        } 

        if (entry->MTU > MEA_INGRESS_PORT_MTU_MAX_VALUE(port)) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - Ingress MTU %d > Max value (%d)  (port=%d)\n",
                             __FUNCTION__,
                             (int)entry->MTU,
                             MEA_INGRESS_PORT_MTU_MAX_VALUE(port),
                             port);
           return MEA_ERROR;
        }
    }else{
       /* Check for MTU not change for Enet4000*/ 
       if (entry->MTU != MEA_INGRESS_PORT_MTU_DEF_VAL(port)) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - Ingress MTU not support yet\n",
                             __FUNCTION__);
           return MEA_ERROR;
        }
    } 

	{
        MEA_db_HwUnit_t hwUnit = MEA_HW_UNIT_ID_GENERAL;
        if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
            hwUnit = MEA_HW_UNIT_ID_GENERAL;
        } 
        if (!mea_drv_Get_DeviceInfo_IsPortPolicerSupport(unit,hwUnit)) {
		    MEA_Uint32 i;
		    for (i=0;i<MEA_INGRESS_PORT_POLICER_TYPE_LAST;i++) {
	            if (entry->policer_vec[i].tmId_enable != MEA_FALSE){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - Currently port policer not support\n",
                                      __FUNCTION__);
                    return MEA_ERROR;
         	    }
            }
		}
	}

	if(entry->policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_TOTAL].tmId_enable != MEA_FALSE){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - total port policer is not supported\n",
                         __FUNCTION__);
       return MEA_ERROR;

	}
	
    
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    if(MEA_AUTONEG_SUPPORT){
        if ((MEA_IngressPort_InitDone) &&
            (entry->autoneg_enable != MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)].autoneg_enable)) {

            if(entry->autoneg_enable == MEA_TRUE ){
                if (MEA_API_Get_IngressPort_Type(unit,port,&port_type) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_Get_IngressPort_Type failed (port=%d)\n",
                        __FUNCTION__,port);
                    return MEA_ERROR;
                }
                if(port_type != MEA_PORTTYPE_GBE_MAC &&
                   port_type != MEA_PORTTYPE_GBE_1588     ){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - the autoneg_enable for (port=%d) can't be setting\n",
                        __FUNCTION__,port);
                    return MEA_ERROR;
                }

            }

        }


    }



    /* Update the Hardware */
    if (mea_IngressPort_UpdateHw(unit,
                                    port,
                                 entry,
                                 &(MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)])
                               ) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Ingress Port Update Hw for port %d failed \n",
                          __FUNCTION__,port);
		return MEA_ERROR;
	}

    if((entry->parser_info.port_proto_prof != MEA_INGRESS_PORT_PROTO_TRANS) && (entry->parser_info.port_proto_prof != MEA_INGRESS_PORT_PROTO_DC)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -port %d  INGRESS_PORT_PROTO  not support port protocol %d \n",
            __FUNCTION__,port,entry->parser_info.port_proto_prof);
        return MEA_ERROR;
    }

    if (entry->userParser == MEA_FALSE){ 
    /* Update the parser */
        
        if (mea_drv_Set_IngressPort_ParserInfo(unit,port,
                    &(entry->parser_info),
                    &(MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)].parser_info)
                   ) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Ingress Port Update Parser for port %d failed \n",
                              __FUNCTION__,port);
		    return MEA_ERROR;
        }
    } 
    /* ingress policer set*/



    if (mea_drv_set_port_ingress_policer(unit,port,
		                                 entry,
										 &(MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)])) != MEA_OK){
        	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Ingress Port Update policer for port %d failed \n",
                          __FUNCTION__,port);
		return MEA_ERROR;
    }

    /* Update the l2cp + default SID manager */
    if (mea_drv_l2cp_def_sid_hw_manager_set(port) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_l2cp_def_sid_hw_manager_set failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_set_port_ingress_vxlan_cpe(unit, port,
        entry,
        &(MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)])) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Ingress Port Update vxlan_cpe for port %d failed \n",
            __FUNCTION__, port);
        return MEA_ERROR;
    }


    /* Update the Software shadow DB */
    MEA_OS_memcpy(&(MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)]),
                  entry,
                  sizeof(MEA_IngressPort_Table[0]));



   




    /* Return to caller */
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_IngressPort_Entry>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_IngressPort_Entry(MEA_Unit_t                 unit,
                                         MEA_Port_t                 port,
                                         MEA_IngressPort_Entry_dbt* entry) 
{

   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS


    /* Check for valid port parameter */
	if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_FALSE)==MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - port %d is invalid\n",
                         __FUNCTION__,port);
       return MEA_ERROR; 
    }

    /* Check for valid entry parameter */
    if (entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry==NULL (port=%d)\n",
                         __FUNCTION__,port);
       return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* update output parameter */
    MEA_OS_memcpy(entry,
                  &(MEA_IngressPort_Table[mea_get_port_mapArrayINGRESS(port)]),
                  sizeof(*entry));

    /* return to caller */  
	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_IngressPort_RxEnable>                             */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_IngressPort_RxEnable(MEA_Unit_t                 unit,
                                            MEA_Port_t                 port,
                                            MEA_Bool                   rx_enable)
{
    MEA_Uint32 *pPortGrp;

    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
    MEA_Uint32 offset=0;
	MEA_Uint32 NumOFShift;

MEA_RxEnable_stateWrite =MEA_TRUE;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
#if 0
    /* Check for valid port parameter */
    if (MEA_API_Get_IsPortValid(port,MEA_FALSE)==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - port %d is invalid\n",
                          __FUNCTION__,port);
        return MEA_ERROR;
    }
#endif
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    pPortGrp=(MEA_Uint32 *)(&(MEA_IngressPort_Table_rx_enable.out_ports_0_31));
    pPortGrp += (port/32);

	NumOFShift = (port % 32);
    (*pPortGrp) &= ~(0x0000001       <<(NumOFShift));
    (*pPortGrp) |=  (rx_enable<<(NumOFShift));

    if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
        MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

        /*Write to TBL 100 RX */
        if (port < 256){
            offset = ENET_REGISTER_TBL100_Admin_RX_MAC_0_255;
            MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_0_31, sizeof(BlockOutPort));

        }
#ifdef MEA_OUT_PORT_256_511
		if (port >= 256 && port < 512) {
			offset = ENET_REGISTER_TBL100_Admin_RX_MAC_256_511; 
			MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_256_287, sizeof(BlockOutPort));

		}
#endif

#ifdef MEA_OUT_PORT_512_1023
		if (port >= 512 && port < 768) {
			offset = ENET_REGISTER_TBL100_Admin_RX_MAC_512_767;
			MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_512_543, sizeof(BlockOutPort));

		}
		
		if (port >= 768 && port < 1024) {
			offset = ENET_REGISTER_TBL100_Admin_RX_MAC_768_1023;
			MEA_OS_memcpy(&BlockOutPort, &MEA_IngressPort_Table_rx_enable.out_ports_768_799, sizeof(BlockOutPort));

		}


#endif
        /*config ENET_REGISTER_TBL100_Admin_RX */
        MEA_Configure_BlockOF256PORTS_TABLE100(unit, &BlockOutPort, offset);



    }
    else {
        return MEA_ERROR;

    }
    
MEA_RxEnable_stateWrite =MEA_FALSE;
    return MEA_OK;
}




MEA_Status MEA_API_PortIngress_Event_AdminStatus(MEA_Unit_t               unit_i,
                                            MEA_OutPorts_Entry_dbt  *eventPorts,  //1-port have event
                                            MEA_Bool                *exist)
{

    MEA_Internal_Block_256OutPorts_dbt BlockOutPort;
 
 //  MEA_Uint32 read_reg[4];
    if(eventPorts == NULL){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - eventPorts = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }
    if(exist == NULL){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - exist = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

    MEA_OS_memset(eventPorts,0,sizeof(*eventPorts));
    *exist=MEA_FALSE;

     if( MEA_reinit_start){//Reinit is On 
         //MEA_OS_LOG_logMsg (MEA_OS_LOG_EVENT," user start to change the port Admin/or reinit start\n");
         return MEA_OK;
     }

     if (MEA_GLOBAL_NEW_PORT_SETTING == MEA_TRUE) {
         /*Read to TBL 100 RX */
         MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));

         MEA_Get_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_0_255);
         MEA_OS_memcpy(&eventPorts->out_ports_0_31, &BlockOutPort, sizeof(BlockOutPort));

         if (MEA_MAX_PORT_NUMBER >= 256){
             MEA_OS_memset(&BlockOutPort, 0, sizeof(BlockOutPort));
#ifdef MEA_OUT_PORT_256_511
             MEA_Get_BlockOF256PORTS_TABLE100(unit_i, &BlockOutPort, ENET_REGISTER_TBL100_Admin_RX_MAC_256_511);
             MEA_OS_memcpy(&eventPorts->out_ports_256_287, &BlockOutPort, sizeof(BlockOutPort));
#endif         
         }


     }
     else {
         return MEA_ERROR;
     }
     
    return MEA_OK;
}





MEA_Status MEA_API_PortIngress_UpdateAdminUp(MEA_Unit_t               unit_i)
{

 

    return MEA_OK;
}

static void mea_IngressPort_UpdateHw_L2_To_1588_OffsetEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_L2_To_L3_1588_Offset_t * entry= (MEA_L2_To_L3_1588_Offset_t * )arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    count_shift=0;

    MEA_OS_insert_value(count_shift,
        6,
        entry->L3_offset ,
        &val[0]);
    count_shift+=6;

    MEA_OS_insert_value(count_shift,
        1,
        entry->Enable_Checksum ,
        &val[0]);
    count_shift+=1;

    MEA_OS_insert_value(count_shift,
        1,
        entry->Enable_TS ,
        &val[0]);
    count_shift+=1;



    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}


static MEA_Status mea_IngressGlobal_UpdateHw_L2_TO_L3_1588_Offset(MEA_Unit_t                 unit,
                                                                MEA_Uint16                 index,
                                                                MEA_L2_To_L3_1588_Offset_t  *entry)
{
    MEA_ind_write_t             ind_write;

   


    if(!MEA_1588_SUPPORT){
        return MEA_OK;
    }



    /* build the ind_write value */
    ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_L2_TO_L3_1588_OFFSET;
    ind_write.tableOffset    = index; 
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask        = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg    = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;  

    ind_write.writeEntry    = (MEA_FUNCPTR)mea_IngressPort_UpdateHw_L2_To_1588_OffsetEntry;
    ind_write.funcParam1    = 0;
    ind_write.funcParam2    = 0;
    ind_write.funcParam3    = 0;
    ind_write.funcParam4 = (MEA_funcParam)entry;
    if (MEA_API_WriteIndirect(unit,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            MEA_MODULE_IF);
        return MEA_ERROR;
    }





    return MEA_OK;
}

MEA_Status mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_Init(MEA_Unit_t  unit_i )
{
    MEA_Uint16 index;
    MEA_Uint32 size;

    if(!MEA_1588_SUPPORT){
        return MEA_OK;
    }
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF  L2 TO L3 for 1588 Offset Init ...\n");
    size = ENET_IF_CMD_PARAM_TBL_L2_TO_L3_1588_LENGT  * sizeof(MEA_L2_To_L3_1588_Offset_t);
    if (size != 0) {
        MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table = (MEA_L2_To_L3_1588_Offset_t*)MEA_OS_malloc(size);
        if (MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[0]),0,size);
    }




    
    index=0;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 12;

    index=1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 16;
    index=2;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 20;
    index=3;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 16;
    index=4;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 20;
    index=5;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 20;
    index=6;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 24;
    index=7;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 34;
    index=8;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 38;
    index=9;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 38;
    index=10;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 42;
   
    index=16;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 18;
    index=17;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 22;
    index=18;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset= 26;
    index=19;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 30;
    

    index=21;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 22;
    index=22;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 26;
    index=23;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 30;
   


    index=26;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset =26;
    index=27;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_TS = 1; 
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].Enable_Checksum = 1;
    MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index].L3_offset = 30;
    
    for(index=0;index<ENET_IF_CMD_PARAM_TBL_L2_TO_L3_1588_LENGT ;index++){
    
        if(mea_IngressGlobal_UpdateHw_L2_TO_L3_1588_Offset(unit_i,index,&MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index])!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_IngressGlobal_UpdateHw_L2_TO_L3_1588_Offset failed index =%d\n", __FUNCTION__,index);
        }
    
    }






    return MEA_OK;
}

MEA_Status mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_RInit(MEA_Unit_t  unit_i )
{
    MEA_Uint16 index;
    

    if(!MEA_1588_SUPPORT){
        return MEA_OK;
    }

    for(index=0;index<ENET_IF_CMD_PARAM_TBL_L2_TO_L3_1588_LENGT ;index++){

        if(mea_IngressGlobal_UpdateHw_L2_TO_L3_1588_Offset(unit_i,index,&MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table[index])!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_IngressGlobal_UpdateHw_L2_TO_L3_1588_Offset failed index =%d\n", __FUNCTION__,index);
        }

    }

 return MEA_OK;
}

MEA_Status mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_Conclude(MEA_Unit_t  unit_i )
{
    if(!MEA_1588_SUPPORT){
        return MEA_OK;
    }

   if (MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table)
   {
       MEA_OS_free(MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table);
       MEA_IngressGlobal_L2_TO_L3_1588_Offset_Table = NULL;
   }
   


    return MEA_OK;
}

void enet_drv_Pos_ingressShaper_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32 entry = (MEA_Uint32)((long)arg4);


    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    val[0]=entry;


    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }


}

MEA_Status MEA_API_Set_IngressPort_SHAPER_POS_RX(ENET_Unit_t unit_i,MEA_Port_t port,MEA_Uint8 Rx_num, MEA_Uint32 value)
{

    MEA_db_HwUnit_t       hwUnit;

    ENET_ind_write_t      ind_write;

    if(!MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  Pos shaper not Support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(port >32){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - we support only 0:32 Pos port \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(Rx_num != 1 && Rx_num != 2){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - we support only Rx 1 or 2  \n",
            __FUNCTION__);
        return MEA_ERROR;

    }
   
    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)


    if(Rx_num == 1){
     
        /* Prepare ind_write structure */
     ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_POS_RX1_INGRESS_SHAPER;
    ind_write.tableOffset   = port; 
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg     = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)enet_drv_Pos_ingressShaper_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)port);
    ind_write.funcParam4 = (MEA_funcParam)((long)value);


    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
        return ENET_ERROR;
    }


    }else {
         
        ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_POS_RX2_INGRESS_SHAPER;
        ind_write.tableOffset   = port; 
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)enet_drv_Pos_ingressShaper_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
        ind_write.funcParam3 = (MEA_funcParam)((long)port);
        ind_write.funcParam4 = (MEA_funcParam)((long)value);


        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }

    }
    return MEA_OK;
}







void mea_drv_parser_MAC_filter_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_MacAddr *mac_addr= (MEA_MacAddr  *)arg4;


    MEA_Uint32                 val[2];
    MEA_Uint32                 i=0;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
   

    val[1] = 
        ((((MEA_Uint32)((unsigned char)(mac_addr->b[1]))) <<  0) |  
        (((MEA_Uint32)((unsigned char)(mac_addr->b[0]))) <<  8) );

    val[0] = 
        ((((MEA_Uint32)((unsigned char)(mac_addr->b[5]))) <<  0) |  
        (((MEA_Uint32)((unsigned char)(mac_addr->b[4]))) <<  8) |
        (((MEA_Uint32)((unsigned char)(mac_addr->b[3]))) << 16) |  
        (((MEA_Uint32)((unsigned char)(mac_addr->b[2]))) << 24) );





    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }


}



MEA_Status mea_drv_parser_MAC_filter(MEA_Unit_t unit,MEA_Port_t port,MEA_MacAddr DA)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32 index=0;
    MEA_ind_write_t      ind_write;

    MEA_DRV_GET_HW_UNIT(unit,hwUnit,return MEA_ERROR;)

    if(!MEA_TDM_SUPPORT){
        return MEA_OK;
    }

    

    if(port != 48 &&  port != 72 && port != 125 && port != 126){
        return MEA_OK; 
    }
    
    
    
    
    if(port == 48)  index=0;
       
    if(port == 72)  index=1;
       
    if(port == 125) index=2;
        
    if(port == 126) index=3;
        

        ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_DA_MAC_FILTER;
        ind_write.tableOffset   = index;   // port index
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_parser_MAC_filter_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
        ind_write.funcParam3 = (MEA_funcParam)((long)port);
        ind_write.funcParam4 = (MEA_funcParam)(&DA);


        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }




    
    
    



    return MEA_OK;
}





MEA_Uint32 mea_drv_eth24_led_Get_Index(MEA_Unit_t unit,MEA_Port_t port)
{
MEA_Uint32 retval=31;
#if (defined(MEA_OS_TDN1) || defined(MEA_OS_ETH_PROJ_1))
	MEA_Uint32 index[MEA_MAX_PORT_NUMBER];

	


	MEA_OS_memset(&index[0],31,sizeof(index));
	//port bit num
	index[12]=0; // led 1
	index[24]=1; // led 2
	index[36]=2; // led 3
	index[109]=3; // led 4
	index[126]=4; // led 5
	index[101]=5; // led 6
	index[102]=6; // led 7
	index[104]=7; // led 8
	index[105]=8; // led 9
	index[93]=9;  // led 10
	index[106]=10;// led 11
	index[107]=11;// led 12
	index[0]=12;// led 13
	index[108]=13;// led 14
	index[48]=14;// led 15
	index[72]=15;// led 16
	index[125]=16;// led 17
	index[100]=17;// led 18
	index[92]=18;// led 19
	index[103]=19; // led 20


	retval=index[port];

#endif
return retval;
}







MEA_Status MEA_API_port_Get_Link_protection_Status(MEA_Unit_t unit_i,MEA_OutPorts_Entry_dbt *portEntry)
{
	if(portEntry== NULL){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," portEntry = NULL\n");
		return MEA_ERROR;
	}
     

	MEA_OS_memcpy(portEntry,&MEA_IngressPort_Table_Link_protection_rx_Status,sizeof(*portEntry));



	return MEA_OK;
}


MEA_Status MEA_API_port_Set_Link_protection_Status_mask(MEA_Unit_t  unit_i,MEA_Port_t port,MEA_Bool enable  )
{

  // MEA_Uint32             *pPortGrp;
    
   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s This API not Support on HW \n", __FUNCTION__);
   return MEA_ERROR;
  
   
   if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s port %d  is invalid \n",__FUNCTION__,port);
	   return MEA_ERROR;
   } 
   
    if(enable){
		MEA_SET_OUTPORT(&MEA_IngressPort_Table_Link_protection_rx_Mask,port);
	}else{
		MEA_CLEAR_OUTPORT(&MEA_IngressPort_Table_Link_protection_rx_Mask,port);
	}


#if 0 /*not Support */
	pPortGrp=(MEA_Uint32 *)(&(MEA_IngressPort_Table_Link_protection_rx_Mask.out_ports_0_31));
	pPortGrp += (port/32);
    if (port <= 127){
        MEA_API_WriteReg(MEA_UNIT_0,
            ENET_IF_LINK_STATUS_ADMIN31_0_REG + ((port / 32) * 4),
            (*pPortGrp),
            MEA_MODULE_IF);
    }
#endif	
	
	return MEA_OK;

}

MEA_Status MEA_API_port_Get_Link_protection_Status_mask(MEA_Unit_t      unit_i,MEA_OutPorts_Entry_dbt     *portEntry)
{
	if(portEntry==NULL){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," portEntry = NULL\n");
		return MEA_ERROR;
	}


	MEA_OS_memcpy(portEntry,&MEA_IngressPort_Table_Link_protection_rx_Mask,sizeof(*portEntry));



	return MEA_OK;
}




/************************************************************************/
/*   rx afdx BAG config                                                      */
/************************************************************************/
void mea_drv_Rx_AfdxBag_prof_UpdateHwEntry(
    MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t               unit_i   = (MEA_Unit_t            	)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       	)arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         	)arg3;
    //MEA_RxAfdxBag_dbt   *entry    = (MEA_RxAfdxBag_dbt *)arg4;

    MEA_Uint32   write_value = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
 //   MEA_Uint32                  count_shift;
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
  //  count_shift=0;
     


    //val[0]=entry->Bag;
    
    val[0]=write_value;

 
    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            ENET_BM_IA_WRDATA0 +(i*4),
            val[i],
            MEA_MODULE_BM);  
    }

}




static MEA_Status mea_drv_Rx_AfdxBag_prof_UpdateHw(MEA_Unit_t                        unit_i,
                                                           MEA_Uint16            id_i,
                                                           MEA_RxAfdxBag_dbt    *new_entry_pi,
                                                           MEA_RxAfdxBag_dbt    *old_entry_pi)
{
    
   
    MEA_ind_write_t ind_write;
    MEA_Uint32 value=0;

    /* check if we have any changes */
    if ((old_entry_pi) &&
        (MEA_OS_memcmp(new_entry_pi,old_entry_pi,sizeof(*new_entry_pi))== 0) ) { 
            return MEA_OK;
    }
    
        value=new_entry_pi->Bag;
    

    ind_write.tableType         = ENET_BM_TBL_TYP_AFDX_REDUNDANCY_BAG_CONFIGURE;
    ind_write.tableOffset       = id_i ;
    ind_write.cmdReg            = ENET_BM_IA_CMD;      
    ind_write.cmdMask           = ENET_BM_IA_CMD_MASK;      
    ind_write.statusReg         = ENET_BM_IA_STAT;   
    ind_write.statusMask        = ENET_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg      = ENET_BM_IA_ADDR;
    ind_write.tableAddrMask     = ENET_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry        =  (MEA_FUNCPTR)mea_drv_Rx_AfdxBag_prof_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
    ind_write.funcParam3 = (MEA_funcParam)((long)id_i);
    ind_write.funcParam4 = (MEA_funcParam)((long)value);

    if (MEA_API_WriteIndirect(ENET_UNIT_0,&ind_write,MEA_MODULE_BM) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_API_WriteIndirect failed "
            "TableType=%d , TableOffset=%d , ENET_module=%d\n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            ENET_MODULE_BM);
        return ENET_ERROR;
    } 


    /* Return to caller */
    return MEA_OK;
}


static MEA_Bool mea_drv_Rx_AfdxBag_prof_find_free(MEA_Unit_t       unit_i, 
                                                          MEA_Uint16       *id_io)
{
    MEA_Uint16 i;

    for(i=0;i< MEA_AFDX_BAG_MAX_PROF; i++) {
        if (MEA_AfdxBag_info_Table[i].valid == MEA_FALSE){
            *id_io=i; 
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;

}



MEA_Bool mea_drv_Rx_AfdxBag_prof_IsRange(MEA_Unit_t     unit_i,
                                                        MEA_Uint16 id_i)
{

    if ((id_i) >= MEA_AFDX_BAG_MAX_PROF){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

MEA_Status mea_drv_Rx_AfdxBag_prof_IsExist(MEA_Unit_t     unit_i,
                                                          MEA_Uint16 id_i,
                                                          MEA_Bool *exist)
{

    if(exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  exist = NULL \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    *exist=MEA_FALSE;

    if(mea_drv_Rx_AfdxBag_prof_IsRange(unit_i,id_i)!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed  for id=%d\n",__FUNCTION__,id_i); 
        return MEA_ERROR;
    }

    if( MEA_AfdxBag_info_Table[id_i].valid){
        *exist=MEA_TRUE;
        return MEA_OK;
    }

    return MEA_OK;
}

static MEA_Status mea_drv_Rx_AfdxBag_prof_check_parameters(MEA_Unit_t          unit_i,
                                                           MEA_RxAfdxBag_dbt   *entry_pi)
{

//    if((entry_pi->Bag != 0) && 
//       (entry_pi->Bag != 1) &&
//       (entry_pi->Bag != 2) &&
//       (entry_pi->Bag != 4) &&
//       (entry_pi->Bag != 8) &&
//       (entry_pi->Bag != 16) &&
//       (entry_pi->Bag != 32) &&
//       (entry_pi->Bag != 64) &&
//       (entry_pi->Bag != 128)   ){
//        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed  BAG value=%d is wrong\n",__FUNCTION__,entry_pi->Bag); 
//        return MEA_ERROR;
//    }
    if(entry_pi->Bag>15){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed  BAG value=%d is wrong\n",__FUNCTION__,entry_pi->Bag); 
        return MEA_ERROR;
    }



    return MEA_OK;
}



MEA_Status mea_drv_Rx_AfdxBag_prof_add_owner(MEA_Unit_t       unit_i ,
                                                     MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_Rx_AfdxBag_prof_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }

    MEA_AfdxBag_info_Table[id_i].num_of_owners++;

    return MEA_OK;
}
MEA_Status mea_drv_Rx_AfdxBag_prof_delete_owner(MEA_Unit_t       unit_i ,
                                                               MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_Rx_AfdxBag_prof_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }
    
    if ( MEA_AfdxBag_info_Table[id_i].valid == MEA_FALSE )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - none existing with ID %d\n",
            __FUNCTION__,id_i);
        return MEA_ERROR;  
    }

    MEA_AfdxBag_info_Table[id_i].num_of_owners--;

    if(MEA_AfdxBag_info_Table[id_i].num_of_owners==0){
        MEA_AfdxBag_info_Table[id_i].valid=MEA_FALSE;
        MEA_AfdxBag_info_Table[id_i].num_of_owners=0;
        return MEA_OK;

    }


    

    return MEA_OK;
}

MEA_Bool mea_drv_Init_Rx_AfdxBag_prof(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;
 //   MEA_Uint16 id;
    if (!MEA_AFDX_SUPPORT){
        return MEA_OK;   
    }
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize AFDX BAG \n");
    /* Allocate PacketGen Table */
    size = MEA_AFDX_BAG_MAX_PROF * sizeof(MEA_afdxBag_Rx_dbt);
    if (size != 0) {
        MEA_AfdxBag_info_Table = (MEA_afdxBag_Rx_dbt*)MEA_OS_malloc(size);
        if (MEA_AfdxBag_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_AfdxBag_info_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_AfdxBag_info_Table[0]),0,size);
    }
    /**/
//     for (id=0;id<MEA_AFDX_BAG_MAX_PROF;id++) {
//         MEA_AfdxBag_info_Table[id].valid =MEA_TRUE;
//         MEA_AfdxBag_info_Table[id].data.Bag  = 0;/*0.5msec*/
//         MEA_AfdxBag_info_Table[id].num_of_owners=1;
//         /*No need to write to fpga*/
//         
//     }



    return MEA_OK;
}
MEA_Bool mea_drv_RInit_Rx_AfdxBag_prof(MEA_Unit_t       unit_i)
{
    MEA_Uint16 id;

    if (!MEA_AFDX_SUPPORT){
        return MEA_OK;   
    }

    for (id=0;id<MEA_AFDX_BAG_MAX_PROF;id++) {
        if (MEA_AfdxBag_info_Table[id].valid){
            if(mea_drv_Rx_AfdxBag_prof_UpdateHw(unit_i,
                id,
                &(MEA_AfdxBag_info_Table[id].data),
                NULL) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  mea_drv_TDM_interface_prof_Idle_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                    return MEA_ERROR;
            }
        }
    }


    return MEA_OK;
}

MEA_Bool mea_drv_Conclude_Rx_AfdxBag_prof(MEA_Unit_t       unit_i)
{
    if (!MEA_AFDX_SUPPORT){
        return MEA_OK;
    }

    /* Free the table */
    if (MEA_AfdxBag_info_Table != NULL) {
        MEA_OS_free(MEA_AfdxBag_info_Table);
        MEA_AfdxBag_info_Table = NULL;
    }

    return MEA_OK;
}

/************************************************************************/
/*  MEA_API AfdxBag_Ingress                                                */
/************************************************************************/

MEA_Status mea_drv_Rx_AfdxBag_Create_Ingress(MEA_Unit_t                      unit_i,
                                        MEA_RxAfdxBag_dbt           *entry_pi,
                                        MEA_Uint16                       *id_io)
{
    MEA_Bool exist;

    MEA_API_LOG

        /* Check if support */
        if (!MEA_AFDX_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - TDM not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        /* check parameter */
        if(mea_drv_Rx_AfdxBag_prof_check_parameters(unit_i,entry_pi) !=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Rx_AfdxBag_prof_check_parameters failed \n",__FUNCTION__); 
            return MEA_ERROR;
        }

        /* Look for stream id */
        if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID){
            /*check if the id exist*/
            if (mea_drv_Rx_AfdxBag_prof_IsExist(unit_i,*id_io,&exist)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Rx_AfdxBag_prof_IsExist failed (*id_io=%d\n",
                    __FUNCTION__,*id_io);
                return MEA_ERROR;
            }
            if (exist){
//                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
//                     "%s - *Id_io %d is already exist\n",__FUNCTION__,*id_io);
//                 return MEA_ERROR;
              mea_drv_Rx_AfdxBag_prof_add_owner(unit_i,*id_io);
              return MEA_OK;
            }

        }else {
            /*find new place*/
            if (mea_drv_Rx_AfdxBag_prof_find_free(unit_i,id_io)!=MEA_TRUE){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Rx_AfdxBag_prof_find_free failed\n",__FUNCTION__);
                return MEA_ERROR;
            }
        }

        if( mea_drv_Rx_AfdxBag_prof_UpdateHw(unit_i,
            *id_io,
            entry_pi,
            NULL)!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Rx_AfdxBag_prof_UpdateHw failed for id %d\n",__FUNCTION__,*id_io);
                return MEA_ERROR;
        }

        MEA_AfdxBag_info_Table[*id_io].valid         = MEA_TRUE;
        MEA_AfdxBag_info_Table[*id_io].num_of_owners = 1;
        MEA_OS_memcpy(&(MEA_AfdxBag_info_Table[*id_io].data),entry_pi,sizeof(MEA_AfdxBag_info_Table[*id_io].data));



        return MEA_OK;

}


MEA_Status mea_drv_Rx_AfdxBag_Set_Ingress(MEA_Unit_t                     unit_i,
                                     MEA_Uint16                      id_i,
                                     MEA_RxAfdxBag_dbt *entry_pi)
{
    MEA_Bool exist;

    MEA_API_LOG

        /* Check if support */
        if (!MEA_AFDX_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - TDM not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        /*check if the id exist*/
        if (mea_drv_Rx_AfdxBag_prof_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Rx_AfdxBag_prof_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - stream %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }

        /* check parameter*/
        if(mea_drv_Rx_AfdxBag_prof_check_parameters(unit_i,entry_pi)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Rx_AfdxBag_prof_check_parameters filed \n",__FUNCTION__); 
            return MEA_ERROR;
        }

        if( mea_drv_Rx_AfdxBag_prof_UpdateHw(unit_i,
            id_i,
            entry_pi,
            &MEA_AfdxBag_info_Table[id_i].data)!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_TDM_interface_prof_Idle_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
                return MEA_ERROR;
        }

        MEA_OS_memcpy(&(MEA_AfdxBag_info_Table[id_i].data),
            entry_pi,
            sizeof(MEA_AfdxBag_info_Table[id_i].data));



        return MEA_OK;
}


MEA_Status mea_drv_Rx_AfdxBag_Delete_Ingress(MEA_Unit_t                  unit_i,
                                        MEA_Uint16                   id_i)
{

    MEA_Bool                    exist;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_AFDX_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - AFDX not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        /*check if the id exist*/
        if (mea_drv_Rx_AfdxBag_prof_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Rx_AfdxBag_prof_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - stream %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        if(MEA_AfdxBag_info_Table[id_i].num_of_owners >1){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - need to delete all the reference to this profile\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }

        /*Write to Hw All zero*/

        MEA_OS_memset(&MEA_AfdxBag_info_Table[id_i].data,0,sizeof(MEA_AfdxBag_info_Table[0].data));

        MEA_AfdxBag_info_Table[id_i].valid = MEA_FALSE;
        MEA_AfdxBag_info_Table[id_i].num_of_owners=0;

        /* Return to caller */
        return MEA_OK;
}

MEA_Status mea_drv_Rx_AfdxBag_Get_Ingress(MEA_Unit_t          unit_i,
                                     MEA_Uint16            id_i,
                                     MEA_RxAfdxBag_dbt    *entry_po)
{
    MEA_Bool    exist; 

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_AFDX_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - AFDX not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        /*check if the id exist*/
        if (mea_drv_Rx_AfdxBag_prof_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Rx_AfdxBag_prof_IsExist failed (id_i=%d)\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - id %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if(entry_po==NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry_po is null\n",__FUNCTION__,id_i);
            return MEA_ERROR;

        }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        /* Copy to caller structure */
        MEA_OS_memset(entry_po,0,sizeof(*entry_po));
        MEA_OS_memcpy(entry_po ,
            &(MEA_AfdxBag_info_Table[id_i].data),
            sizeof(*entry_po));

        /* Return to caller */
        return MEA_OK;


}

MEA_Status mea_drv_Rx_AfdxBag_GetFirst_Ingress(MEA_Unit_t               unit_i,
                                          MEA_Uint16                *id_o,
                                          MEA_RxAfdxBag_dbt    *entry_po, /* Can't be NULL */
                                          MEA_Bool                 *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_AFDX_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - AFDX not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        if (id_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_o)=0;
            (*id_o) < MEA_AFDX_BAG_MAX_PROF; 
            (*id_o)++ ) {
                if (MEA_AfdxBag_info_Table[(*id_o)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }

                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_AfdxBag_info_Table[(*id_o)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;

}

MEA_Status mea_drv_Rx_AfdxBag_GetNext_Ingress(MEA_Unit_t                  unit_i,
                                         MEA_Uint16                   *id_io,
                                         MEA_RxAfdxBag_dbt       *entry_po, /* Can't be NULL */
                                         MEA_Bool                    *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_AFDX_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - TDM not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        if (id_io == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_io)++;
            (*id_io) < MEA_AFDX_BAG_MAX_PROF; 
            (*id_io)++ ) {
                if(MEA_AfdxBag_info_Table[(*id_io)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }
                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_AfdxBag_info_Table[(*id_io)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;
}

MEA_Status MEA_API_portIngress_1P1_Status(MEA_Unit_t unit_i,MEA_Port_t port ,MEA_PortIngress_status_1p1_dbt *state_o)
{
    MEA_Uint32 look_bit=0;
    MEA_Bool   valid_1p1=MEA_TRUE;
    MEA_Uint32 read_val=0;
    MEA_Uint32 state_calc;
    MEA_Uint32 offset;
    MEA_Port_type_te porttype;


    if(state_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - state_o is null\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," MEA_API_Get_IsPortValid failed port=%d\n",port);
        return MEA_ERROR;
    }

    if(MEA_API_Get_IngressPort_Type(MEA_UNIT_0,port ,&porttype)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," MEA_API_Get_IngressPort_Type failed port=%d\n",port);
        return MEA_ERROR;
    }

    if((porttype != MEA_PORTTYPE_GBE_MAC)  &&  (porttype != MEA_PORTTYPE_10G_XMAC_ENET)  &&  (porttype != MEA_PORTTYPE_GBE_1588) ){

           return MEA_ERROR;
    }


    if(MEA_PROTECT_1PLUS1_SUPPORT){
        switch (port)
        {
        case 0:
        case 12:
            offset=0;
            if(port==0)
                look_bit=0x1;
            if(port==12)
                look_bit=0x2;

            break;
        case 24:
        case 36:
            offset=1;
            if(port==24)
                look_bit=0x1;
            if(port==36)
                look_bit=0x2;
            break;
        case 48:
        case 72:
            offset=2;
            if(port==48)
                look_bit=0x1;
            if(port==72)
                look_bit=0x2;


            break;
        case 125:
        case 126:
            offset=3;
            if(port==125)
                look_bit=0x1;
            if(port==126)
                look_bit=0x2;
            break;
        case 110:
        case 111:
            offset=4;
            if(port==110)
                look_bit=0x1;
            if(port==111)
                look_bit=0x1;
            break;
        case 118:
        case 119:
            offset=5;
            if(port==118)
                look_bit=0x1;
            if(port==119)
                look_bit=0x2;
            break;
        default:
               valid_1p1=MEA_FALSE;
            break;

        }
    
        if(valid_1p1){
            read_val=MEA_API_ReadReg(unit_i,ENET_IF_1P1_STATE_PROTECT,MEA_MODULE_IF);

            state_calc= (read_val>>(4*offset)) & 0xf;
            if(state_calc!=0){
                if(state_calc == 1)
                    *state_o=MEA_PORT_INGRESS_1P1_STATE_IDLE;
                else {
                    if (((state_calc>>look_bit) & 0x1) == MEA_TRUE)
                        *state_o=MEA_PORT_INGRESS_1P1_STATE_WORKING;
                    else 
                        *state_o=MEA_PORT_INGRESS_1P1_STATE_PROTECT;
                }

            }else{
                *state_o=MEA_PORT_INGRESS_1P1_STATE_OFF;
            }
        }else{
            *state_o=MEA_PORT_INGRESS_1P1_STATE_OFF;
        }

    }else{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_PROTECT_1PLUS1_SUPPORT Not support\n",__FUNCTION__);
        return MEA_ERROR;
    }




    return MEA_OK;

   
}
#define MEA_1p1_SWITCH_OVER_SUPPORT 1

MEA_Status MEA_API_port_1P1_SwitchOver(MEA_Unit_t unit,MEA_Port_t port)
{

    MEA_Bool   valid_1p1=MEA_TRUE;
#if MEA_1p1_SWITCH_OVER_SUPPORT
    MEA_Uint32 val=0;
#endif    
    MEA_Uint32 offset;
    MEA_Port_type_te porttype;
	MEA_PortIngress_status_1p1_dbt state;

    if(!MEA_PROTECT_1PLUS1_SUPPORT){
        return MEA_ERROR;
    }

    

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," MEA_API_Get_IsPortValid failed port=%d\n",port);
        return MEA_ERROR;
    }

    if(MEA_API_Get_IngressPort_Type(MEA_UNIT_0,port ,&porttype)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," MEA_API_Get_IngressPort_Type failed port=%d\n",port);
        return MEA_ERROR;
    }

    if((porttype != MEA_PORTTYPE_10G_XMAC_ENET)  &&  (porttype != MEA_PORTTYPE_GBE_1588)  &&  (porttype != MEA_PORTTYPE_GBE_MAC) ){

        return MEA_ERROR;
    }
    
    
    
    
    switch (port)
    {
    case 0:
    case 12:
        offset=0;
        break;
    case 24:
    case 36:
        offset=1;
        break;
    case 48:
    case 72:
        offset=2;
        break;
    case 100:
    case 101:
        offset=3;
        break;
    case 102:
    case 103:
        offset=4;
        break;
    case 104:
    case 105:
        offset=5;
        break;
    case 108:
    case 109:
        offset=6;
        break;
    case 110:
    case 111:
        offset=6;
        break;


    default:
        valid_1p1=MEA_FALSE;
        break;

    }

    if(!valid_1p1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_PROTECT_1PLUS1_SUPPORT Not support on this port % d\n",__FUNCTION__,port);
        return MEA_ERROR;
    }
    
    
    
    
    if(MEA_PROTECT_1PLUS1_SUPPORT){

		/************************************************************************/
		/* if the port is working do not switch only if protect                         */
		/************************************************************************/
		if (MEA_API_portIngress_1P1_Status(MEA_UNIT_0,port ,&state)!=MEA_OK){

			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s MEA_API_portIngress_1P1_Status \n",__FUNCTION__);
			return MEA_OK;
		}

		if(state!= MEA_PORT_INGRESS_1P1_STATE_PROTECT){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," 1P1_Status port=%d is not protect \n",port);
			return MEA_ERROR;
		}


#if MEA_1p1_SWITCH_OVER_SUPPORT		
       val= 0x1<<offset;
        MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_SWITC_1PLUS1_OVER,val,MEA_MODULE_IF);
		val= 0;
        MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_SWITC_1PLUS1_OVER,val,MEA_MODULE_IF);
		
	    
#endif        
		
		
    
    }



    return MEA_OK;

}


/************************************************************************/
/* PTP 1588 port delay                                                   */
/************************************************************************/

void mea_drv_port_PTP1588_Delay_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    //MEA_Unit_t               unit_i   = (MEA_Unit_t            	)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       	)arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         	)arg3;
    MEA_ptp1588_delay_dbt   *entry    = (MEA_ptp1588_delay_dbt *)arg4;

    




    MEA_Uint32                 val[2];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }

    count_shift=0;

    MEA_OS_insert_value(count_shift,
        8,
        (MEA_Uint32)entry->frac_nano,
        &val[0]);
    count_shift +=8;
    MEA_OS_insert_value(count_shift,
        30,
        (MEA_Uint32)entry->nSec,
        &val[0]);
    count_shift +=30;

    MEA_OS_insert_value(count_shift,
        2,
        (MEA_Uint32)entry->sec,
        &val[0]);
    count_shift +=2;

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i) , 
            val[i] , 
            MEA_MODULE_IF);
    }



}

static void mea_drv_Port_UpdateHw_PTP1588_mode_Entry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                  unit_i   = (MEA_Unit_t            )arg1;
    //  MEA_db_HwUnit_t             hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //  MEA_db_HwId_t               hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Port_Ptp1588_dbt*  entry    = (MEA_Port_Ptp1588_dbt*)arg4;
    MEA_Uint32                  val[1];
    MEA_Uint32                  i;


    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
    
    val[0]= entry->ptp1588_Mode;
   



    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(unit_i,MEA_IF_SRV_MAKE0_REG , val[i] , MEA_MODULE_IF);
    }

}

    
MEA_Status mea_drv_port_PTP1588_Delay_UpdateHw(MEA_Unit_t unit_i,MEA_Port_t port,
                                               MEA_Port_Ptp1588_dbt *new_entry_pi,
                                               MEA_Port_Ptp1588_dbt *old_entry_pi)

{
    
    MEA_ind_write_t ind_write;
   
    MEA_ptp1588_delay_dbt entryHw;

    /* check if we have any changes */
   

   
    /************************************************************************/
    /*  set port type                                                        */
    /************************************************************************/

    
    if ( ((old_entry_pi == NULL) ||
         (new_entry_pi->ptp1588_Mode != old_entry_pi->ptp1588_Mode))){
        
             /* build the ind_write value */
             ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_1588_MODE_TYPE;
             ind_write.tableOffset      = port; 
             ind_write.cmdReg           = MEA_IF_CMD_REG;      
             ind_write.cmdMask          = MEA_IF_CMD_PARSER_MASK;      
             ind_write.statusReg        = MEA_IF_STATUS_REG;   
             ind_write.statusMask       = MEA_IF_STATUS_BUSY_MASK;   
             ind_write.tableAddrReg     = MEA_IF_CMD_PARAM_REG;
             ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;  

             ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_Port_UpdateHw_PTP1588_mode_Entry;
             ind_write.funcParam1    = 0;
             ind_write.funcParam2    = 0;
             ind_write.funcParam3    = 0;
             ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

             if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - MEA_API_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                     __FUNCTION__,
                     ind_write.tableType,
                     ind_write.tableOffset,
                     MEA_MODULE_IF);


                 return MEA_ERROR;
             }
    
    
    
    
    }


   

    //asymmetry
    if (((old_entry_pi==NULL) ||
        (MEA_OS_memcmp(&new_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY],
                       &old_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY],
                       sizeof(new_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY])) != 0)) ){
    
        /* build the ind_write value */
        ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_1588_DELAY_TYPE;
        ind_write.tableOffset	= port;
        ind_write.cmdReg		= MEA_IF_CMD_REG;      
        ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg		= MEA_IF_STATUS_REG;   
        ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_port_PTP1588_Delay_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)0;
        ind_write.funcParam2 = (MEA_funcParam)0;
        ind_write.funcParam3 = (MEA_funcParam)0;
        ind_write.funcParam4 = (MEA_funcParam)&entryHw;



    MEA_OS_memcpy(&entryHw,&new_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY],sizeof(entryHw));



        /* Write to the Indirect Table */
        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
        }
    }
    
    
        
        if((old_entry_pi == NULL ) ||
        (MEA_OS_memcmp(&new_entry_pi->delay[MEA_1588_DELAY_PEER2PEER],&old_entry_pi->delay[MEA_1588_DELAY_PEER2PEER],sizeof(new_entry_pi->delay[MEA_1588_DELAY_PEER2PEER])) != 0) ||
        (MEA_OS_memcmp(&new_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY],&old_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY],sizeof(new_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY])) != 0) )
        {
            //sum
            MEA_OS_memset(&entryHw,0,sizeof(entryHw));
            
            ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_1588_DELAY_TYPE;
            ind_write.tableOffset	= port + 128;
            ind_write.cmdReg		= MEA_IF_CMD_REG;      
            ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
            ind_write.statusReg		= MEA_IF_STATUS_REG;   
            ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
            ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
            ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
            ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_port_PTP1588_Delay_UpdateHwEntry;
            ind_write.funcParam1 = (MEA_funcParam)0;
            ind_write.funcParam2 = (MEA_funcParam)0;
            ind_write.funcParam3 = (MEA_funcParam)0;
            ind_write.funcParam4 = (MEA_funcParam)&entryHw;




        entryHw.frac_nano = new_entry_pi->delay[MEA_1588_DELAY_PEER2PEER].frac_nano + new_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY].frac_nano;
        if((new_entry_pi->delay[MEA_1588_DELAY_PEER2PEER].frac_nano + new_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY].frac_nano)>255)
          entryHw.nSec=1;

        entryHw.nSec      += new_entry_pi->delay[MEA_1588_DELAY_PEER2PEER].nSec      + new_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY].nSec;
        if(entryHw.nSec > 0x3fffffff){
            entryHw.nSec = (entryHw.nSec & 0x3fffffff);
            entryHw.sec=1;
        }

        entryHw.sec       += new_entry_pi->delay[MEA_1588_DELAY_PEER2PEER].sec        + new_entry_pi->delay[MEA_1588_DELAY_ASYMMETRY].sec;
        
        entryHw.sec = entryHw.sec & 0x3;

        /* Write to the Indirect Table */
        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
        }

        
        }

  
    //delay egress
    if (!((old_entry_pi) &&
        (MEA_OS_memcmp(&new_entry_pi->delay[MEA_1588_DELAY_EGRESS],&old_entry_pi->delay[MEA_1588_DELAY_EGRESS],sizeof(new_entry_pi->delay[MEA_1588_DELAY_EGRESS]))== 0) ))
    {
         MEA_OS_memset(&entryHw,0,sizeof(entryHw));

        ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_1588_DELAY_TYPE;
        ind_write.tableOffset	= port + 256;
        ind_write.cmdReg		= MEA_IF_CMD_REG;      
        ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg		= MEA_IF_STATUS_REG;   
        ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_port_PTP1588_Delay_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)0;
        ind_write.funcParam2 = (MEA_funcParam)0;
        ind_write.funcParam3 = (MEA_funcParam)0;
        ind_write.funcParam4 = (MEA_funcParam)&entryHw;


   
        MEA_OS_memcpy(&entryHw,&new_entry_pi->delay[MEA_1588_DELAY_EGRESS],sizeof(entryHw));

    /* Write to the Indirect Table */
        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
        }

    }





    /* Return to caller */
    return MEA_OK;
 
}

    
MEA_Status mea_drv_Init_port_PTP1588_Delay(MEA_Unit_t unit_i)
{
    MEA_Port_t port;
    if(!MEA_PTP1588_SUPPORT)
        return MEA_OK;
    /* Port oriented settings */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF PTP1588 port tables ...\n");
    
    MEA_OS_memset(&MEA_Port_delay_ptp1588_Table[0],0,sizeof(MEA_Port_delay_ptp1588_Table));

    for (port=0;port<MEA_NUM_OF_ELEMENTS(MEA_IngressPort_Table);port++) 
    {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        if(mea_drv_port_PTP1588_Delay_UpdateHw(unit_i,port,
            &MEA_Port_delay_ptp1588_Table[port],
            NULL)!=MEA_OK){

              
        }
    }

    
    return MEA_OK;
}

MEA_Status mea_drv_Rinit_port_PTP1588_Delay(MEA_Unit_t unit_i)
{
    MEA_Port_t port;


    if (b_bist_test) {
        return MEA_OK;
    }
    if(!MEA_PTP1588_SUPPORT)
        return MEA_OK;

    for (port=0;port<MEA_NUM_OF_ELEMENTS(MEA_IngressPort_Table);port++) 
    {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        if(mea_drv_port_PTP1588_Delay_UpdateHw(unit_i,port,
                                                &MEA_Port_delay_ptp1588_Table[port],
                                                NULL)!=MEA_OK){
                
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_port_PTP1588_Delay_UpdateHw fail port %d\n",__FUNCTION__,port);
            return MEA_ERROR;
        }

       
        

    }
    
    return MEA_OK;
}


MEA_Status MEA_API_Set_port_PTP1588(MEA_Unit_t unit_i,MEA_Port_t port ,MEA_Port_Ptp1588_dbt *entry)
{

    if(!MEA_PTP1588_SUPPORT){
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_PTP1588_SUPPORT is not support\n",__FUNCTION__,port);
        return MEA_ERROR;
    }

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the port is not valid\n",__FUNCTION__,port);
        return MEA_ERROR;
    }






    /************************************************************************/
    /* check the value for nSec                                             */
    /************************************************************************/
    if(entry->delay[MEA_1588_DELAY_ASYMMETRY].nSec > 0x3ffffff){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the value for DELAY_ASYMMETRY.nSec is  out of range  \n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(entry->delay[MEA_1588_DELAY_PEER2PEER].nSec > 0x3ffffff){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the value for DELAY_PER2PER.nSec is  out of range  \n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(entry->delay[MEA_1588_DELAY_EGRESS].nSec > 0x3ffffff){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the value for DELAY_EGRESS.nSec is  out of range  \n",__FUNCTION__);
        return MEA_ERROR;
    }
    /************************************************************************/
    /* check the value for sec                                               */
    /************************************************************************/
    if(entry->delay[MEA_1588_DELAY_ASYMMETRY].sec > 0x3){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the value for DELAY_ASYMMETRY.sec is  out of range  \n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(entry->delay[MEA_1588_DELAY_PEER2PEER].sec > 0x3){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the value for DELAY_PER2PER.sec is  out of range  \n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(entry->delay[MEA_1588_DELAY_EGRESS].sec > 0x3){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the value for DELAY_EGRESS.sec is  out of range  \n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(entry->ptp1588_Mode >=MEA_PTP1588_MODE_LAST ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - ptp1588 mode type is not support  \n",__FUNCTION__);
        return MEA_ERROR;
    }


    
    if(mea_drv_port_PTP1588_Delay_UpdateHw(unit_i,port,
        entry,
        &MEA_Port_delay_ptp1588_Table[port])!=MEA_OK){

            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_port_PTP1588_Delay_UpdateHw fail port %d\n",__FUNCTION__,port);
            return MEA_ERROR;
    }


    /*save the database*/
    MEA_OS_memcpy(&MEA_Port_delay_ptp1588_Table[port],entry,sizeof(MEA_Port_delay_ptp1588_Table[port]));
    

    return MEA_OK;
}
MEA_Status MEA_API_Get_port_PTP1588(MEA_Unit_t unit_i,MEA_Port_t port ,MEA_Port_Ptp1588_dbt *entry)
{
    
    
    if(!MEA_PTP1588_SUPPORT){

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_PTP1588_SUPPORT is not support\n",__FUNCTION__,port);
        return MEA_ERROR;
    }

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - the port is not valid\n",__FUNCTION__,port);
        return MEA_ERROR;
    }
    
    MEA_OS_memcpy(entry,&MEA_Port_delay_ptp1588_Table[port],sizeof(*entry));



    return MEA_OK;

}



MEA_Status MEA_API_Get_PTP1588_Info_statusFifo(MEA_Unit_t unit_i, MEA_Uint16 instance, MEA_Ptp1588_statusTx_dbt *entry)
{
    MEA_ind_read_t         ind_read;
    MEA_Uint16 instanc_index = 0;
    
    if(!MEA_PTP1588_SUPPORT){

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_PTP1588_SUPPORT is not support\n",__FUNCTION__);
        return MEA_ERROR;
    }


    /************************************************************************/
    /* check that instance is on the range                                   */
    /************************************************************************/

    switch (MEA_INSTANCE_FIFO1588)
    {
    case 1:
        if (instance > 1){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_INSTANCE_FIFO1588 support only 1 \n", __FUNCTION__);
        }

        break;
    case 2:
        if (instance > 2){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_INSTANCE_FIFO1588 support only 2 \n", __FUNCTION__);
        }
        break;
    case 3:
        if (instance > 3){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_INSTANCE_FIFO1588 support only 3 \n", __FUNCTION__);
        }
        break;

    case 0:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_INSTANCE_FIFO1588 not support\n", __FUNCTION__);
        return MEA_ERROR;
        break;
    }
   



    MEA_OS_memset(entry,0,sizeof(*entry));

    
    instanc_index = instance * 0x10;
    


    ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_1588_MESSAGE_INFO; 
    ind_read.tableOffset    = instanc_index;
    ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
    ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_read.statusReg		= MEA_IF_STATUS_REG;   
    ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
    ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data      = (&entry->regs[0]);    

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
       (MEA_NUM_OF_ELEMENTS(entry->regs)),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }

    


    
    return MEA_OK;
}
MEA_Status MEA_API_Get_PTP1588_Info_ReadFifo(MEA_Unit_t unit_i, MEA_Uint16 instance,MEA_Ptp1588_info_fifo_dbt *entry)
{
    MEA_ind_read_t         ind_read;
     MEA_Uint32          val[3];
     MEA_Uint16 instanc_index = 0;


    if(!MEA_PTP1588_SUPPORT){

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_PTP1588_SUPPORT is not support\n",__FUNCTION__);
        return MEA_ERROR;
    }


    switch (MEA_INSTANCE_FIFO1588)
    {
    case 1:
        if (instance > 1){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_INSTANCE_FIFO1588 support only 1 \n", __FUNCTION__);
        }

        break;
    case 2:
        if (instance > 2){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_INSTANCE_FIFO1588 support only 2 \n", __FUNCTION__);
        }
        break;
    case 3:
        if (instance > 3){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_INSTANCE_FIFO1588 support only 3 \n", __FUNCTION__);
        }
        break;

    case 0:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_INSTANCE_FIFO1588 not support\n", __FUNCTION__);
        return MEA_ERROR;
        break;
    }




    MEA_OS_memset(entry, 0, sizeof(*entry));


    instanc_index = (instance * 0x10) +4;




    ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_1588_MESSAGE_INFO; 
    ind_read.tableOffset = instanc_index;
    ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
    ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_read.statusReg		= MEA_IF_STATUS_REG;   
    ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
    ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_read.read_data      = (&val[0]);    

    if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,&ind_read,
        (MEA_NUM_OF_ELEMENTS(val)),
        MEA_MODULE_IF,
        globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_read.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
    }
     
    
    //entry->timestemp[0] = val[0];
    //entry->timestemp[1] = (val[1] & 0x00ffffff);
     
    entry->timestemp.frac_nano = (MEA_Uint8)(val[0] & 0xff);
    entry->timestemp.nSec      = val[0]>>8;
    entry->timestemp.nSec     |= ((val[1] & 0xff)<<24);
    entry->timestemp.sec       = (MEA_Uint16)((val[1]>>8) & 0xffff);

    entry->seq_number   =   (val[1]>>24);
    entry->seq_number  |= ((val[2] & 0xff)<<8);
    entry->portId       = (MEA_Uint8)((val[2]>>8) & 0x7f);
    
    
    return MEA_OK;
}

/************************************************************************/
/* Extend default Sid with L2Type                                       */
/************************************************************************/

static void mea_UpdateHwEntryDefault_L2Type_Sid(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

   // MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    //MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    //MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    MEA_Def_SID_dbt *entry = (MEA_Def_SID_dbt*)arg4;

    MEA_Uint32     val[1];
    MEA_Uint32     i;
    MEA_Uint32     count_shift;

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
    count_shift=0;

    MEA_OS_insert_value(count_shift,
        14,
        (MEA_Uint32)entry->def_sid,
        &val[0]);
    count_shift+=14;

    MEA_OS_insert_value(count_shift,
        2,
        (MEA_Uint32)entry->action,
        &val[0]);
    count_shift+=2;
    MEA_OS_insert_value(count_shift,
        1,
        (MEA_Uint32)entry->valid,
        &val[0]);
    count_shift+=1;


    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i) , 
            val[i] , 
            MEA_MODULE_IF);
    }

}

MEA_Status mea_drv_set_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Uint8 L2Type, MEA_Def_SID_dbt *entry )
{

    MEA_Uint32 index;
    MEA_ind_write_t   ind_write;

    if(L2Type >=MEA_INGRESS_L2TYPE_DEFAULT_MAX){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s MEA_API_Set_IngressPort_Default_L2Type_Sid  fail the l2Type %d not support\n",__FUNCTION__,L2Type);
        return MEA_ERROR;
    }
    if( entry == NULL ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

    index = port<<3;
    index |=  (L2Type & 0x7);

    /* build the ind_write value */
    ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_L2TYPE_DEFUALT_SID;
    ind_write.tableOffset      = index;
    ind_write.cmdReg           = MEA_IF_CMD_REG;      
    ind_write.cmdMask          = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask       = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg     = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry       = (MEA_FUNCPTR)    mea_UpdateHwEntryDefault_L2Type_Sid;
    //ind_write.funcParam1    = (MEA_funcParam)unit_i;
    //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
    //ind_write.funcParam3    = (MEA_funcParam)hwId;
    ind_write.funcParam4 = (MEA_funcParam)entry;
    if(MEA_INGRESS_EXTEND_DEFAULT_SUPPORT == MEA_TRUE){
        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
        }
    }
    
    
    return MEA_OK;

}
MEA_Status mea_drv_Init_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit)
{
 
    MEA_OS_memset(&MEA_IngressPort_Default_L2Type_Table[0],0,sizeof(MEA_IngressPort_Default_L2Type_Table));
    /*No need to init the HW*/


    return MEA_OK;
}

MEA_Status mea_drv_Reinit_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit)
{

    MEA_Port_t port;
    MEA_Uint8 L2Type;

    for (port=0;port<=MEA_MAX_PORT_NUMBER;port++)
    {
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
                continue;
        }
        for(L2Type=0;L2Type<MEA_INGRESS_L2TYPE_DEFAULT_MAX ;L2Type++){
            if(MEA_IngressPort_Default_L2Type_Table[port].data[L2Type].valid == MEA_FALSE)
                continue;
            
            if(mea_drv_set_IngressPort_Default_L2Type_Sid(unit,
                                                       port,
                                                       L2Type, 
                                                       &MEA_IngressPort_Default_L2Type_Table[port].data[L2Type])!=MEA_OK){
            }
        }
    }
    
    
    
    
    return MEA_OK;
}


static MEA_Bool mea_drv_IngressPort_Default_L2Type_Sid_index(MEA_Unit_t unit, 
                                                       MEA_Port_t port, 
                                                       MEA_Uint8 L2Type, 
                                                       MEA_Uint8 *offset,
                                                       MEA_Bool silent){
    MEA_Uint8 index=0;
    MEA_Uint8 port_type;
    MEA_IngressPort_Entry_dbt  Ingressentry;

    if (L2Type >= MEA_PARSING_L2_KEY_LAST){
        if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s MEA_API_Set_IngressPort_Default_L2Type_Sid  fail the l2Type %d out of range \n", __FUNCTION__, L2Type);
        return MEA_FALSE;
    }
    MEA_OS_memset(&Ingressentry, 0, sizeof(Ingressentry));
    
    if (MEA_API_Get_IngressPort_Entry(unit, port, &Ingressentry) != MEA_OK){
        return MEA_ERROR;
    }


    switch (L2Type)
    {
     case MEA_PARSING_L2_KEY_Untagged:
        index = 0;
        /* support also port_type 1 */
        if (Ingressentry.parser_info.def_sid_port_type == 0 ||
            Ingressentry.parser_info.def_sid_port_type == 1){
            port_type = (MEA_Uint8)Ingressentry.parser_info.def_sid_port_type;
        }
        else{
            if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s fail the l2Type %d is not support on port mode  \n", __FUNCTION__, L2Type);
            return MEA_FALSE;
        }


        break;
    case MEA_PARSING_L2_KEY_Ctag:
        index = 1;
        /* support also port_type 1 */
        if (Ingressentry.parser_info.def_sid_port_type == 0 ||
            Ingressentry.parser_info.def_sid_port_type == 1){
            port_type = (MEA_Uint8)Ingressentry.parser_info.def_sid_port_type;
        }
        else{
            if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s fail the l2Type %d is not support on port_type %d  \n", __FUNCTION__, L2Type, Ingressentry.parser_info.def_sid_port_type);
            return MEA_FALSE;

        }
        break;
    case   MEA_PARSING_L2_KEY_Ctag_Ctag:
        index = 2;
        /* support also port_type 1 */
        if (Ingressentry.parser_info.def_sid_port_type == 0 ||
            Ingressentry.parser_info.def_sid_port_type == 1){
            port_type = (MEA_Uint8)Ingressentry.parser_info.def_sid_port_type;
        }
        else{
            if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s fail the l2Type %d is not support on port_type %d  \n", __FUNCTION__, L2Type, Ingressentry.parser_info.def_sid_port_type);
            return MEA_FALSE;
        }
        break;
    case    MEA_PARSING_L2_KEY_Stag:
        index = 3;
        /* support also port_type 1 */
        if (Ingressentry.parser_info.def_sid_port_type == 0 ){
            port_type = (MEA_Uint8)Ingressentry.parser_info.def_sid_port_type;
        }
        else{
            if (!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s fail the l2Type %d is not support on port_type %d  \n", __FUNCTION__, L2Type, Ingressentry.parser_info.def_sid_port_type);
            return MEA_FALSE;
        }
        break;
    case    MEA_PARSING_L2_KEY_Stag_Ctag:
        index = 4;
        port_type = 0;
        break;
    case    MEA_PARSING_L2_KEY_Stag_Stag:
        index = 5;
        port_type = 0;
        break;
    case    MEA_PARSING_L2_KEY_Stag_Stag_Ctag:
        index = 6;
        port_type = 0;
        break;

        /*7-10*/
    case MEA_PARSING_L2_KEY_Btag_Itag:
        index = 3;
        port_type = 1;
        break;
    case MEA_PARSING_L2_KEY_Btag_Itag_Ctag:
        index = 4;
        port_type = 1;
        break;
    case MEA_PARSING_L2_KEY_Btag_Itag_Stag:
        index = 5;
        port_type = 1;
        break;
    case       MEA_PARSING_L2_KEY_Btag_Itag_Stag_Ctag:
        index = 6;
        port_type = 1;
        break;

        /*16-18 21-21 26-27*/
    case MEA_PARSING_L2_KEY_MPLS_Single_label_no_tag:
        index = 0;
        port_type = 2;
        break;
    case MEA_PARSING_L2_KEY_MPLS_Two_label_no_tag:
        index = 1;
        port_type = 2;
        break;
    case MEA_PARSING_L2_KEY_MPLS_Three_label_no_tag:
        index = 2;
        port_type = 2;
        break;
    
    
    case MEA_PARSING_L2_KEY_MPLS_Single_label_with_tag:
        index = 3;
        port_type = 2;
        break;
    case MEA_PARSING_L2_KEY_MPLS_Two_label_with_tag:
        index = 4;
        port_type = 2;
        break;

    case   MEA_PARSING_L2_KEY_MPLS_Single_label_with_two_tag:
        index = 5;
        port_type = 2;
        break;
    case  MEA_PARSING_L2_KEY_MPLS_Two_label_with_two_tag:
        index = 6;
        port_type = 2;
        break;

   


    default:
        if (!silent)
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s MEA_API_Set_IngressPort_Default_L2Type_Sid  fail the l2Type %d out of range \n", __FUNCTION__, L2Type);
        return MEA_FALSE;
        break;
    }

    /* support also port_type 1 */
    if (Ingressentry.parser_info.def_sid_port_type != port_type){
        if (!silent)
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s fail the l2Type %d is not support on port_type %d  \n", __FUNCTION__, L2Type, Ingressentry.parser_info.def_sid_port_type);
        
        return MEA_FALSE;

    }

    *offset = index;
    return MEA_TRUE;

}






MEA_Status MEA_API_Set_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Uint8 L2Type, MEA_Def_SID_dbt *entry )
{

    MEA_Uint8 index;
    
    
    if (!MEA_INGRESS_EXTEND_DEFAULT_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s MEA_INGRESS_EXTEND_DEFAULT_SUPPORT == %s\n",
            __FUNCTION__, MEA_STATUS_STR(MEA_INGRESS_EXTEND_DEFAULT_SUPPORT));
        return MEA_ERROR;
    }
    
    
    if( entry == NULL ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }
    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        return MEA_ERROR;
    }
    
    if (mea_drv_IngressPort_Default_L2Type_Sid_index(unit,
        port,
        L2Type,
        &index,
        MEA_FALSE) == MEA_FALSE){
              return MEA_ERROR;
    }
        
    if (MEA_OS_memcmp(&MEA_IngressPort_Default_L2Type_Table[port].data[index], entry, sizeof(MEA_IngressPort_Default_L2Type_Table[port].data[L2Type])) == 0)
    {
        return MEA_OK;
    }




    
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    
    if(mea_drv_set_IngressPort_Default_L2Type_Sid(unit,port,index,entry)!=MEA_OK){
        
        return MEA_ERROR;
    }
    

    
    /*save to data base*/
    MEA_OS_memcpy(&MEA_IngressPort_Default_L2Type_Table[port].data[index],entry,sizeof(MEA_IngressPort_Default_L2Type_Table[port].data[L2Type]));
    
    
    return MEA_OK;
}


MEA_Status MEA_API_IngressPort_Default_L2Type_Sid_GetFirst(MEA_Unit_t unit, MEA_Port_t port, MEA_Uint8 *L2Type,MEA_Bool *found ,MEA_Def_SID_dbt *entry)
{

    MEA_Uint8 index = 0;
    
    MEA_IngressPort_Entry_dbt  Ingressentry;

    if (!MEA_INGRESS_EXTEND_DEFAULT_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s MEA_INGRESS_EXTEND_DEFAULT_SUPPORT == %s\n",
            __FUNCTION__, MEA_STATUS_STR(MEA_INGRESS_EXTEND_DEFAULT_SUPPORT));
        return MEA_ERROR;
    }

    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (found == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == found\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    if (L2Type == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == L2Type\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE) == MEA_FALSE) {
        return MEA_ERROR;
    }
    
    *found = MEA_TRUE;
    
    if (MEA_API_Get_IngressPort_Entry(unit, port, &Ingressentry) != MEA_OK){
        return MEA_ERROR;
    }

    if (Ingressentry.parser_info.def_sid_port_type == 0)
        *L2Type = 0;
    if (Ingressentry.parser_info.def_sid_port_type == 1)
        *L2Type = 0;
    if (Ingressentry.parser_info.def_sid_port_type == 2)
        *L2Type = 16;
     
    
    if (mea_drv_IngressPort_Default_L2Type_Sid_index(unit,
        port,
        *L2Type,
        &index,
        MEA_TRUE) == MEA_FALSE){
        return MEA_ERROR;
    }


    if (entry != NULL)
        MEA_OS_memcpy(entry, &MEA_IngressPort_Default_L2Type_Table[port].data[index], sizeof(*entry));

    return MEA_OK;
}

MEA_Status MEA_API_IngressPort_Default_L2Type_Sid_GetNext(MEA_Unit_t unit, MEA_Port_t port, MEA_Uint8 *L2Type, MEA_Bool *found, MEA_Def_SID_dbt *entry)
{

    MEA_Uint8 index = 0;
    
    MEA_IngressPort_Entry_dbt  Ingressentry;
    if (!MEA_INGRESS_EXTEND_DEFAULT_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s MEA_INGRESS_EXTEND_DEFAULT_SUPPORT == %s\n",
            __FUNCTION__, MEA_STATUS_STR(MEA_INGRESS_EXTEND_DEFAULT_SUPPORT));
        return MEA_ERROR;
    }

    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (found == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == found\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (L2Type == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == L2Type\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE) == MEA_FALSE) {
        return MEA_ERROR;
    }



    if (MEA_API_Get_IngressPort_Entry(unit, port, &Ingressentry) != MEA_OK){
        return MEA_ERROR;
    }

    if (Ingressentry.parser_info.def_sid_port_type == 0){
        *L2Type = *L2Type+1;
        if (*L2Type >= 7){
            *found = MEA_FALSE;
            return MEA_OK;
        }
        else
            *found = MEA_TRUE;

    }
    if (Ingressentry.parser_info.def_sid_port_type == 1) {
        *L2Type = *L2Type + 1;
        if (*L2Type == 3)
            *L2Type = 7;

        if (*L2Type > 10){
            *found = MEA_FALSE;
            return MEA_OK;
        }
        else
            *found = MEA_TRUE;
        
    }

    if (Ingressentry.parser_info.def_sid_port_type == 2)
        *L2Type = *L2Type + 1;
    if (*L2Type == 19)
        *L2Type = 21;
    
    if (*L2Type == 23)
        *L2Type = 26;

    if (*L2Type > 27){
        *found = MEA_FALSE;
        return MEA_OK;
    }
    else
        *found = MEA_TRUE;

    if (mea_drv_IngressPort_Default_L2Type_Sid_index(unit,
        port,
        *L2Type,
        &index,
        MEA_TRUE) == MEA_FALSE){
        return MEA_ERROR;
    }


    if (entry != NULL)
        MEA_OS_memcpy(entry, &MEA_IngressPort_Default_L2Type_Table[port].data[index], sizeof(*entry));

    return MEA_OK;
}


MEA_Status MEA_API_Get_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Uint8 L2Type, MEA_Def_SID_dbt *entry )
{

    MEA_Uint8 index;
    if (!MEA_INGRESS_EXTEND_DEFAULT_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s MEA_INGRESS_EXTEND_DEFAULT_SUPPORT == %s\n",
            __FUNCTION__, MEA_STATUS_STR(MEA_INGRESS_EXTEND_DEFAULT_SUPPORT));
        return MEA_ERROR;
    }

    if( entry == NULL ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }
    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE )==MEA_FALSE) {
        return MEA_ERROR;
    }
    if (L2Type >= MEA_PARSING_L2_KEY_LAST){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s MEA_API_Set_IngressPort_Default_L2Type_Sid  fail the l2Type %d out of range \n", __FUNCTION__, L2Type);
        return MEA_ERROR;
    }
    if (mea_drv_IngressPort_Default_L2Type_Sid_index(unit,
        port,
        L2Type,
        &index,
        MEA_FALSE) == MEA_FALSE){
        return MEA_ERROR;
    }

    
    MEA_OS_memcpy(entry,&MEA_IngressPort_Default_L2Type_Table[port].data[index],sizeof(*entry));

    return MEA_OK;
}


/************************************************************************/
/* Extend default Sid with PDUs                                       */
/************************************************************************/

static void mea_UpdateHwEntryDefault_PDUs_Sid(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    // MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    //MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    //MEA_db_HwId_t           hwId_i      = (MEA_db_HwId_t          )arg3;
    MEA_Def_SID_dbt *entry = (MEA_Def_SID_dbt*)arg4;

    MEA_Uint32     val[1];
    MEA_Uint32     i;
    MEA_Uint32     count_shift;

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i] = 0;
    }
    count_shift=0;

    MEA_OS_insert_value(count_shift,
        14,
        (MEA_Uint32)entry->def_sid,
        &val[0]);
    count_shift+=14;

    MEA_OS_insert_value(count_shift,
        2,
        (MEA_Uint32)entry->action,
        &val[0]);
    count_shift+=2;
    MEA_OS_insert_value(count_shift,
        1,
        (MEA_Uint32)entry->valid,
        &val[0]);
    count_shift+=1;


    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i) , 
            val[i] , 
            MEA_MODULE_IF);
    }

}

MEA_Status mea_drv_set_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Def_Sid_PDUsType PDUsType, MEA_Def_SID_dbt *entry )
{

    MEA_Uint32 index;
    MEA_ind_write_t   ind_write;

    if(PDUsType >=MEA_INGRESS_PDUsTYPE_LAST){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s MEA_API_Set_IngressPort_Default_PDUs_Sid  fail the PDUsType %d not support\n",__FUNCTION__,PDUsType);
        return MEA_ERROR;
    }
    if( entry == NULL ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

    index  = port<<2;
    index |=  ((PDUsType)& 0x3);

    /* build the ind_write value */
    ind_write.tableType        = ENET_IF_CMD_PARAM_TBL_LACP_L2CP_DEFUALT_SID;
    ind_write.tableOffset    = index;
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask        = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg        = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg    = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask    = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)    mea_UpdateHwEntryDefault_PDUs_Sid;
    //ind_write.funcParam1    = (MEA_Uint32)unit_i;
    //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
    //ind_write.funcParam3    = (MEA_Uint32)hwId;
    ind_write.funcParam4 = (MEA_funcParam)entry;
    if(MEA_INGRESS_EXTEND_DEFAULT_SUPPORT == MEA_TRUE){
        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
            return MEA_ERROR;
        }
    }


    return MEA_OK;

}
MEA_Status mea_drv_Init_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit)
{

    MEA_OS_memset(&MEA_IngressPort_Default_PDUsType_Table[0],0,sizeof(MEA_IngressPort_Default_PDUsType_Table));
    /*No need to init the HW*/


    return MEA_OK;
}

MEA_Status mea_drv_Reinit_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit)
{

    MEA_Port_t port;
    MEA_Def_Sid_PDUsType PDUsType;

    for (port=0;port<=MEA_MAX_PORT_NUMBER;port++)
    {
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }
        for(PDUsType=0;PDUsType<MEA_INGRESS_PDUsTYPE_DEFAULT_MAX ;PDUsType++){
            if(MEA_IngressPort_Default_PDUsType_Table[port].data[PDUsType].valid == MEA_FALSE)
                continue;

            if(mea_drv_set_IngressPort_Default_PDUs_Sid(unit,
                port,
                PDUsType, 
                &MEA_IngressPort_Default_PDUsType_Table[port].data[PDUsType])!=MEA_OK){
            }
        }
    }




    return MEA_OK;
}

MEA_Status MEA_API_Set_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Def_Sid_PDUsType PDUsType, MEA_Def_SID_dbt *entry )
{

    if(PDUsType >= MEA_INGRESS_PDUsTYPE_DEFAULT_MAX){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s MEA_API_Set_IngressPort_Default_PDUs_Sid  fail the PDUsType %d not support\n",__FUNCTION__,PDUsType);
        return MEA_ERROR;
    }
    if( entry == NULL ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

    if( MEA_OS_memcmp(&MEA_IngressPort_Default_PDUsType_Table[port].data[PDUsType],entry,sizeof(MEA_IngressPort_Default_PDUsType_Table[port].data[0])) == 0)
    {
        return MEA_OK;
    }


    /************************************************************************/
    /*                                                                      */
    /************************************************************************/

    if(mea_drv_set_IngressPort_Default_PDUs_Sid(unit,port,PDUsType,entry)!=MEA_OK){

        return MEA_ERROR;
    }



    /*save to data base*/
    MEA_OS_memcpy(&MEA_IngressPort_Default_PDUsType_Table[port].data[PDUsType],entry,sizeof(MEA_IngressPort_Default_PDUsType_Table[port].data[0]));


    return MEA_OK;
}

MEA_Status MEA_API_Get_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Def_Sid_PDUsType PDUsType, MEA_Def_SID_dbt *entry )
{

    if(PDUsType >=MEA_INGRESS_PDUsTYPE_DEFAULT_MAX){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s MEA_API_Set_IngressPort_Default_PDUsType_Sid  fail the PDUsType %d not support\n",__FUNCTION__,PDUsType);
        return MEA_ERROR;
    }
    if( entry == NULL ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }
    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_FALSE )==MEA_FALSE) {
        return MEA_ERROR;
    }


    MEA_OS_memcpy(entry,&MEA_IngressPort_Default_PDUsType_Table[port].data[PDUsType],sizeof(*entry));

    return MEA_OK;
}




