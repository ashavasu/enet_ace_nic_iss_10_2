/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_port_drv.h"
#include "mea_if_counters_drv.h"
#include "mea_if_parser_drv.h"
#include "mea_if_service_drv.h"
#include "mea_if_l2cp_drv.h"
#include "mea_if_vsp_drv.h"
#include "mea_swe_ip_drv.h"
#include "mea_bm_drv.h"
#include "mea_globals_drv.h"
#include "mea_deviceInfo_drv.h"
#include "mea_flowCoSMapping_drv.h"
#include "mea_flowMarkingMapping_drv.h"

#include "mea_if_pre_parser_drv.h"
#include "mea_fwd_tbl.h"
#include "mea_cpld.h"
#include "enet_xPermission_drv.h"
#include "mea_mapping_drv.h"
#include "mea_globals_drv.h"
#include "mea_filter_drv.h"
#include "mea_limiter_drv.h"
#include "mea_lxcp_drv.h"
#include "mea_bm_scheduler_drv.h"
#include "enet_queue_drv.h"
#include "enet_shaper_profile_drv.h"
#include "mea_bm_ep_drv.h"
#include "mea_PacketGen_drv.h"
#include "mea_PortState_drv.h"

#include "mea_preATM_parser_drv.h"
#include "mea_Interface_drv.h"
#include "mea_bonding_drv.h"
#include "mea_if_vsp_drv.h" 
#include "mea_tft_drv.h"
#include "mea_sflow_drv.h"
#include "mea_IPSec_drv.h"
#include "mea_ACL5_profiles_drv.h"
#include "mea_if_Acl5_drv.h"
#include "mea_lpm_drv.h"

#include "mea_hc_classifier_drv.h"
#include "mea_hc_decomp_drv.h"
#include "mea_hc_comp_drv.h"
#include "mea_init.h"


#if defined(MEA_ETHERNITY) && (defined(MEA_ENV_S1_K7) || defined(MEA_PLAT_S1_VDSL_K7))
#include "k7_sfp_interface.h"
#endif

/*-------------------------------- Includes ------------------------------------------*/



/*-------------------------------- Definitions ---------------------------------------*/
extern MEA_Bool   MEA_Counters_Type_Enable[MEA_COUNTERS_LAST_TYPE];

MEA_Bool MEA_during_warm_restart = MEA_FALSE; 


 
/*-------------------------------- External Functions --------------------------------*/

extern MEA_Status MEA_drv_DebugVlsiRecordStart(void);


extern MEA_Status MEA_API_Set_Warm_Restart(void);
extern MEA_Status MEA_Write_Disable(MEA_Bool pDisable); 

/*-------------------------------- External Variables --------------------------------*/


/*-------------------------------- Forward Declarations ------------------------------*/
MEA_Status mea_drv_Init_MemoryMap(MEA_ULong_t*   module_base_addr_vec);


/*-------------------------------- Local Variables -----------------------------------*/
/* VER_XXX*/

 





 

/*-------------------------------- Global Variables ----------------------------------*/

MEA_Bool MEA_InitDone = MEA_FALSE;

extern MEA_Uint32 mea_reinit_count;






MEA_Status mea_drv_BasicTest_IF(void) {


    if(b_bist_test){
        return MEA_OK;
    }
    /* Test that Module IF respond after the reset  */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Test       IF module\n");



    MEA_IF_INIT_HW_ACCESS_ENABLE
	MEA_API_WriteReg(MEA_UNIT_0,
                     ENET_IF_SCRATCH_REG,
                     MEA_SCRATCH_TST_PATTERN,
                     MEA_MODULE_IF);

    if (MEA_API_ReadReg(MEA_UNIT_0,
                        ENET_IF_SCRATCH_REG,
                        MEA_MODULE_IF) != MEA_SCRATCH_TST_PATTERN) {
    MEA_IF_INIT_HW_ACCESS_DISABLE 
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - IF scratch register (0x%08x) test failed\n",
                         __FUNCTION__,ENET_IF_SCRATCH_REG);
           if ( !MEA_IF_hw_regs_access_enable ) {
                return MEA_OK;
           } else {
                return MEA_ERROR;
           }
    }

    MEA_IF_INIT_HW_ACCESS_DISABLE



    return MEA_OK;
}

MEA_Status mea_drv_configure_register_types(MEA_Unit_t unit_i)
{
    MEA_Globals_Entry_dbt entry;
    //MEA_Uint32 val;

        //update the global
        MEA_OS_memset(&entry, 0, sizeof(entry));
        MEA_API_Get_Globals_Entry(unit_i, &entry);



        if (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE){
            if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7015) == 0)) {
            
            }

            if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045) == 0)        ||
                (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_VDSL64) == 0) ||
                (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, "MEA_OS_ZYNQ7000") == 0)) 
            {
                /************************************************************************/

                

                /************************************************************************/

            }




        }




 
        

    


        return MEA_API_Set_Globals_Entry(unit_i, &entry);;
}


MEA_Status mea_drv_Init_IF(MEA_Unit_t unit_i) {
   

    
  

    if (!b_bist_test){   
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF module \n");

            if(mea_drv_configure_register_types(unit_i) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Init_IF mea_drv_configure_register_types failed \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }

    }
    

   

	    if (enet_Init_xPermission(unit_i) != ENET_OK) {
	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                           "%s - enet_Init_xPermission failed \n",
			   			       __FUNCTION__);
	        return ENET_ERROR;
	    }


    
	if (mea_Action_Init(unit_i) != ENET_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - mea_Action_Init failed \n",
			   			   __FUNCTION__);
	    return ENET_ERROR;
	}
	
	
	if (mea_drv_Init_IF_Counters_IngressPort(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_IF_Counters_IngressPort failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
	if (mea_drv_Init_IF_Counters_RMON(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_IF_Counters_RMON failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    if(mea_drv_Init_Utopia_RMON_Counters(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_Utopia_RMON_Counters failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Init_Virtual_Rmon_Counters(unit_i) != MEA_OK) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_Init_Utopia_RMON_Counters failed \n",
        __FUNCTION__);
    return MEA_ERROR;
    }





	/* Init IF Forwarder  */
	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Forwarder ...%s\n",(MEA_GLOBAL_FORWARDER_SUPPORT_DSE == MEA_TRUE) ? "Support":" NO Support");
	if(MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
	if (mea_drv_Init_Forwarder(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,

                         "%s - mea_drv_Init_Forwarder failed \n",

                         __FUNCTION__);

       return MEA_ERROR;

    }
	}
    
    if (mea_drv_Init_IF_L2CP(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_IF_L2CP failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    if (mea_drv_Init_IF_VSP(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_IF_vsp failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
    if(MEA_FRAGMENT_VSP_SESSION_SUPPORT){
        
        if(mea_drv_VSP_Fragment_init(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_VSP_Fragment_init failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
   
    if (mea_drv_Init_FlowCoSMappingProfile_Table(unit_i) != MEA_OK) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Init_FlowCoSMappingProfile_Table failed\n",
                      __FUNCTION__);
    return MEA_ERROR;
    } 
    
    if (mea_drv_Init_FlowMarkingMappingProfile_Table(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Init_FlowMarkingMappingProfile_Table failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 

    
    if (mea_drv_Init_Service_Table(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_dev_Init_Service_Table failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 
 	if (mea_Init_limiter(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_Init_limiter failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 

    if(mea_drv_ADM_Init(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ADM_Init failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_ISOLATE_Init(unit_i) != MEA_OK) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ISOLATE_Init failed\n",
        __FUNCTION__);
    return MEA_ERROR;
    }

    if (mea_drv_Init_PreParser_Table(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_PreParserTable failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    
    if (mea_drv_Init_CAM_Tbl() != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_CAM_Table failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    if(mea_drv_Protocol_To_Priority_Mapping_Init(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Protocol_To_Priority_Mapping_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }


#if 0 
    /* Init IF Mapping Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Mapping table ...\n");
    if (mea_drv_Init_Mapping_Table(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_MappingTable failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
    if(MEA_API_Create_Mapping_Profile(unit_i,&mappProfId_1)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Create_Mapping_Profile failed Id_1\n",
                         __FUNCTION__);
        return MEA_ERROR;
    }
    if(MEA_API_Create_Mapping_Profile(unit_i,&mappProfId_2)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Create_Mapping_Profile failed Id_2\n",
                         __FUNCTION__);
        return MEA_ERROR;
    }

    if(MEA_API_Delete_Mapping_Profile(MEA_UNIT_0,mappProfId_2)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Delete_Mapping_Profile failed Id_2\n",
                         __FUNCTION__);
        return MEA_ERROR;
    } 
#endif

    if (mea_drv_Init_Filter_Table(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_Filter_Table failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

   
    if (mea_drv_Init_Parser_Table(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_ParserTable failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Global Parser  table ...\n");
    if(mea_drv_Init_Global_parser(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_Global_parser failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Init_vlanrange_profile(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_vlanrange_profile failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_Init_IngressPort_Table(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Init_IngressPortTable failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
    if(mea_drv_Init_port_PTP1588_Delay(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_port_PTP1588_Delay failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_Init_PacketGen(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_PacketGen failed \n",
                         __FUNCTION__);
        return MEA_ERROR;
    }
    
    if (mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_Init(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }



#ifdef MEA_PLATFORM_TDM_SUPPORT
    
    if(mea_drv_Init_TDM_Interface(unit_i)!= MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_TDM_Interface failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_Init_TDM_Ces(unit_i)!= MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_TDM_Ces failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
#endif
    if(mea_drv_LAG_Init(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_LAG_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_PreATM_parser_Init(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_LAG_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_PRBS_Init(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_LAG_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_HC_classifier_Init(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_classifier_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_HC_Decomp_prof_Init(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_Decomp_prof_Initunit_i failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_Init_IF_Counters_HC_RMON(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_Comp_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_HC_Comp_Init(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_Comp_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    
   
    if(mea_drv_VP_MSTP_Init(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_VP_MSTP_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_FWD_ENB_DA_prof_Init(unit_i) != MEA_OK){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_FWD_ENB_DA_prof_Init failed \n",
        __FUNCTION__);
    return MEA_ERROR;
    }

    if(mea_drv_Init_BFD_SRC_PORT(unit_i) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_Init_BFD_SRC_PORT failed \n",
        __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_Port_Remove_NVGRE_Header_Init(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Port_Remove_NVGRE_Header_Init failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Init_IPSec(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_Init_IPSec failed \n",
        __FUNCTION__);
    return MEA_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF module passed !\n");
	
    return MEA_OK;

}


MEA_Status mea_drv_ReInit_IF(MEA_Unit_t unit_i) 
{
    
if (b_bist_test) {
return MEA_OK;
}
        //update the global
        
       
        if (mea_drv_configure_register_types(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_configure_register_types failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }






    if(mea_drv_RInit_vlanrange_profile(unit_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_RInit_vlanrange_profile failed \n",
            __FUNCTION__);
        return ENET_ERROR;
    }


       
    /* ReInit xPermition object */
	if (enet_ReInit_xPermission(unit_i) != ENET_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - enet_ReInit_xPermission failed \n",
			   			   __FUNCTION__);
	    return ENET_ERROR;
	}

    /* ReInit Action object */
	if (mea_Action_ReInit(unit_i) != ENET_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - mea_Action_ReInit failed \n",
			   			   __FUNCTION__);
	    return ENET_ERROR;
	}


	/* ReInit IF Counters Table */
	if (mea_drv_ReInit_IF_Counters_IngressPort(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ReInit_IF_Counters_IngressPort failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* ReInit IF Counters RMON Table */
	if (mea_drv_ReInit_IF_Counters_RMON(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ReInit_IF_Counters_RMON failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
    
    if(mea_drv_ReInit_Utopia_RMON_Counters(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_IF_Counters_RMON failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_ReInit_Virtual_Rmon_Counters(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_Virtual_Rmon_Counters failed \n",
            __FUNCTION__);
    return MEA_ERROR;
    }



    /* ReInit IF L2CP Table */
    if (mea_drv_ReInit_IF_L2CP(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ReInit_IF_L2CP failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

   
   
    /* ReInit IF vsp Table */
    if (mea_drv_ReInit_IF_VSP(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ReInit_IF_vsp failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
    
   

    /* ReInit Flow CoS Mapping Profile Table  */
    if (mea_drv_ReInit_FlowCoSMappingProfile_Table(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_FlowCoSMappingProfle_Table failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 

    /* ReInit Flow Marking Mapping Profile Table  */
    if (mea_drv_ReInit_FlowMarkingMappingProfile_Table(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_FlowMarkingMappingProfle_Table failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 

    /* ReInit IF Service Table */
    if (mea_drv_ReInit_Service_Table(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_dev_ReInit_Service_Table failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 

    /* ReInit limiter Table */
	if (mea_ReInit_limiter(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_ReInit_limiter failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 

    if(mea_drv_ADM_ReInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ADM_ReInit failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_ISOLATE_RInit(unit_i) != MEA_OK) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_ISOLATE_RInit failed\n",
        __FUNCTION__);
    return MEA_ERROR;
    }
    /* ReInit IF Pre Parser Table */
    if (mea_drv_ReInit_PreParser_Table(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ReInit_PreParserTable failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

    /* ReInit IF CAM Table - Already done by ReInit globals */


    if(mea_drv_Protocol_To_Priority_Mapping_RInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Protocol_To_Priority_Mapping_RInit failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    /* ReInit IF Filter Table */
    if (mea_drv_ReInit_Filter_Table(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ReInit_Filter_Table failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }

	/* ReInit IF Parser Table */
    if (mea_drv_ReInit_Parser_Table(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ReInit_ParserTable failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
    /* ReInit IF GlobalParser Table */
    if(mea_drv_Renit_Global_parser(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Renit_Global_parser failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

	/* ReInit IngressPort  */
    if (mea_drv_ReInit_IngressPort_Table(unit_i) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_ReInit_IngressPortTable failed \n",
                         __FUNCTION__);
       return MEA_ERROR;
    }
    if(mea_drv_Rinit_port_PTP1588_Delay(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_IngressPortTable failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    /* ReInit PacketGen  */
    if (mea_drv_ReInit_PacketGen(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_ReInit_PacketGen failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    /* ReInit L2_TO_L3_1588_Offset_RInit  */
    if (mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_RInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_RInit failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
#ifdef MEA_PLATFORM_TDM_SUPPORT
    if(mea_drv_ReInit_TDM_Interface(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_TDM_Interface failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_ReInit_TDM_Ces(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_TDM_Ces failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

#endif

    if (mea_drv_Reinit_IngressPort_Default_L2Type_Sid(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Reinit_IngressPort_Default_L2Type_Sid failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_Reinit_IngressPort_Default_PDUs_Sid(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Reinit_IngressPort_Default_PDUs_Sid failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_LAG_ReInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_TDM_Ces failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_PreATM_parser_RInit(unit_i)!= MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_PreATM_parser_RInit failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    if(mea_drv_PRBS_ReInit(unit_i)!= MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_PRBS_ReInit failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    /* ReInit IF Forwarder  */
    if (MEA_GLOBAL_FORWARDER_SUPPORT_DSE){
        if (mea_drv_ReInit_Forwarder(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_ReInit_Forwarder failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }


    if(mea_drv_FWD_ENB_DA_prof_RInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_Forwarder failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_RInit_BFD_SRC_PORT(unit_i) != MEA_OK) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_RInit_BFD_SRC_PORT failed \n",
        __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_Port_Remove_NVGRE_Header_Renit(unit_i) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Port_Remove_NVGRE_Header_Renit failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_RInit_IPSec(unit_i) != MEA_OK)
    {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_RInit_IPSec failed \n",
        __FUNCTION__);
    return MEA_ERROR;
    }

    /* Return to caller */
    return MEA_OK;

}


MEA_Status mea_drv_BasicTest_BM(MEA_Bool silent_i) {

    MEA_Uint32 val;
    MEA_JumboType_te JumboType;
    MEA_Uint32 bm_jumboType;
    MEA_Uint32 desc_val;
    MEA_Uint32 data_buffer;
    MEA_Uint32 Bm_status_read;
    MEA_Uint32 write_val=0;
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
    int i;
#endif

    if (b_bist_test)  
    {
     return MEA_OK;
    }
/* Test that Module BM respond after the reset  */
MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Test       BM module\n");
   
     GlobalBm_busy = 0;

MEA_BM_INIT_HW_ACCESS_ENABLE
    write_val = 0xabcd1234;
    MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_SCRATCH_REG, write_val, MEA_MODULE_BM);

#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
  




    
    Bm_status_read = MEA_API_ReadReg(MEA_UNIT_0, ENET_BM_SCRATCH_REG, MEA_MODULE_BM);
    
    if (Bm_status_read != write_val) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"BM read/write on BM_SCRATCH not work Read 0x%8x \n", Bm_status_read, write_val);
    }

#endif





    
#if 1  

    /* Get Jumbo Type */
    JumboType = MEA_Platform_GetJumboType();
    switch(JumboType){
        case MEA_JUMBO_TYPE_2K:
            bm_jumboType = MEA_BM_JUMBO_2K;
        break;
        case MEA_JUMBO_TYPE_4K:
            bm_jumboType = MEA_BM_JUMBO_4K;
        break;
        case MEA_JUMBO_TYPE_8K:
            bm_jumboType = MEA_BM_JUMBO_8K;
		    break;
	    case MEA_JUMBO_TYPE_16K:
            bm_jumboType = MEA_BM_JUMBO_16K;
			break;
        case MEA_JUMBO_TYPE_LAST:
        default:
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," Not support Jumbo type = %d.\n",JumboType); 
           return MEA_ERROR;
           break;
    } 


    val=0;
    val=(64<<16) | (64<<0); 
     MEA_API_WriteReg(MEA_UNIT_0,
	                0x4e0/*ENET_BM_BUFFERS_AND_DESCRIPTOR_THRESHOLD_REG*/,
				     val,
                     MEA_MODULE_BM);



    /* Get number of descriptors */
    if (MEA_Platform_GetNumOfDescriptors(&desc_val, &data_buffer) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_Platform_GetNumOfDescriptors failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    //data_buffer = MEA_MAX_OF_FPGA_DATABUFFER;

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Alex debug jumbo_val %d desc_val %d\n",jumbo_val,desc_val); 
    /*Default with 2k jumbo*/
    val=0;
    if (!MEA_ALLOC_ALLOCR_NEW_SUPPORT) {
        val = (desc_val)& MEA_BM_DISCRIPTOR_BUFFER_MASK;
        val |= (0x3d0 << MEA_OS_calc_shift_from_mask(MEA_BM_DISCRIPTOR_TRHESHOLD_MASK))& MEA_BM_DISCRIPTOR_TRHESHOLD_MASK;
        val |= (bm_jumboType << MEA_OS_calc_shift_from_mask(MEA_BM_DISCRIPTOR_JUMBO_MASK)) & MEA_BM_DISCRIPTOR_JUMBO_MASK;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "BM_DISCRIPTOR_CONF_REG 0x4d4 = 0x%08x\n", val);
    }
    else {
        val  = (desc_val) & MEA_BM_DISCRIPTOR_BUFFER_MASK;
        val |= (data_buffer << MEA_OS_calc_shift_from_mask(MEA_BM_DISCRIPTOR_DATA_BUFFER_MASK)) & MEA_BM_DISCRIPTOR_DATA_BUFFER_MASK;
        val |= (0x3d0 << MEA_OS_calc_shift_from_mask(MEA_BM_DISCRIPTOR_TRHESHOLD_MASK))& MEA_BM_DISCRIPTOR_TRHESHOLD_MASK;
        val |= (bm_jumboType << MEA_OS_calc_shift_from_mask(MEA_BM_DISCRIPTOR_JUMBO_MASK)) & MEA_BM_DISCRIPTOR_JUMBO_MASK;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"BM_DISCRIPTOR_CONF_REG 0x4d4 = 0x%08x\n", val);
    }
    
    
        /*0x4d4*/
        MEA_API_WriteReg(MEA_UNIT_0,ENET_BM_DISCRIPTOR_CONF_REG,
						 val,
						 MEA_MODULE_BM);
	
    
    MEA_API_WriteReg(MEA_UNIT_0,0x4d8,
				     0x00000001,
                     MEA_MODULE_BM);
    /* wait 1 milli */
        MEA_OS_sleep(0,10*1000*1000);                    


 
    if (!silent_i) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                      "support Jumbo  %s \n",
                       (JumboType == MEA_JUMBO_TYPE_2K) ? "JUMBO_2K"  : 
                       (((JumboType == MEA_JUMBO_TYPE_4K) ? "JUMBO_4K" : 
                         (JumboType == MEA_JUMBO_TYPE_8K) ? "JUMBO_8K" :
                         (JumboType == MEA_JUMBO_TYPE_16K) ? "JUMBO_16K" :
                       "Not-support")));

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                      "BM descriptors init value = %d \n",desc_val);
    }


#endif  
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"sleep xx Sec \n", desc_val);
    MEA_OS_sleep(1, 0);
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
    for (i = 1; i <= 10; i++){
       
        MEA_BM_INIT_HW_ACCESS_ENABLE
            Bm_status_read = MEA_API_ReadReg(MEA_UNIT_0, MEA_BM_STATUS, MEA_MODULE_BM);
        if (!(Bm_status_read & MEA_BM_INIT_STATUS_READY_MASK)) {
            MEA_BM_INIT_HW_ACCESS_DISABLE
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " after %d  :: Bm_status=0x%08x \n", i,Bm_status_read);
        }
        else{
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "BM_STATUS after %d Sec\n",i);
            break;
        }
        MEA_OS_sleep(1, 0);
    }
#endif
   

	Bm_status_read= MEA_API_ReadReg(MEA_UNIT_0,ENET_BM_STATUS,MEA_MODULE_BM);
   
	if (!(Bm_status_read & MEA_BM_INIT_STATUS_READY_MASK)) { 

        MEA_BM_INIT_HW_ACCESS_DISABLE

        /* wait 1 milli */
        MEA_OS_sleep(0,10*1000*1000);
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION

        MEA_BM_INIT_HW_ACCESS_ENABLE
			Bm_status_read= MEA_API_ReadReg(MEA_UNIT_0,ENET_BM_STATUS,MEA_MODULE_BM);
        if (!(Bm_status_read & MEA_BM_INIT_STATUS_READY_MASK)) { 
                  MEA_BM_INIT_HW_ACCESS_DISABLE
 		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_BM_MEA_Status reg indicate that BM is not ready  0x08%x\n",__FUNCTION__,Bm_status_read);
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"BM_InitDone_reng                  = %s \n",!(Bm_status_read & MEA_BM_InitDone_reng_MASK) ? "ERROR" : "OK" );
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"BM_InitDone_aloc                  = %s \n",!(Bm_status_read & MEA_BM_InitDone_aloc_MASK) ? "ERROR" : "OK" );
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"BM_InitDone_alocR                 = %s \n",!(Bm_status_read & MEA_BM_InitDone_alocR_MASK) ? "ERROR" : "OK" );
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"BM_InitDone_Counters              = %s \n",!(Bm_status_read & MEA_BM_InitDone_Counters_MASK) ? "ERROR" : "OK" );
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"BM_InitDone_Counts                = %s \n",!(Bm_status_read & MEA_BM_InitDone_Counts_MASK) ? "ERROR" : "OK" );
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"BM_MRT_RAMInit                    = %s \n",!(Bm_status_read & MEA_BM_MRT_RAMInit_MASK) ? "ERROR" : "OK" );
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"BM_InitDone_DDR34_cntrl           = %s \n",!(Bm_status_read & MEA_BM_InitDone_DDR_calib_done_desc_MASK) ? "ERROR" : "OK" );
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"BM_InitDoneDDR_calib_done_dse     = %s \n",!(Bm_status_read & MEA_BM_InitDone_DDR_calib_done_dse_MASK) ? "ERROR" : "OK" );
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"BM_InitDone_DDR_calib_done_data   = %s \n",!(Bm_status_read & MEA_BM_InitDone_DDR_calib_done_data_MASK) ? "ERROR" : "OK" );

        
#if 1   //alex
                if ( !MEA_BM_hw_regs_access_enable ) {
                     return MEA_OK;
                } else {
                     return MEA_ERROR;
                }
#else
          MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," ************************** Skip  the error *******************************\n" );
#endif
        } 
#endif

    }


    
    

    MEA_BM_INIT_HW_ACCESS_DISABLE

    return MEA_OK;

}


MEA_Status mea_drv_BasicInit(MEA_Unit_t                 unit,
                            MEA_ULong_t             *   module_base_addr_vec)
{
    MEA_HwVersion_info_t hw_version;

    //MEA_Uint32 hw_if_version=0 , hw_bm_version=0;
   
    MEA_InitDone = MEA_FALSE;

   
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Initialize Start ...\n");

	
    if (mea_drv_Init_MemoryMap(module_base_addr_vec) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_MemoryMap failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Initialize Init_MemoryMap ...\n");

	/* Reset the Device via the CPLD interface */
    if (mea_drv_CPLD_Init() != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_CPLD_Init failed\n",__FUNCTION__);
        return MEA_ERROR;
    }



    if ( !MEA_IF_hw_regs_access_enable )
        		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "IF HW Access Disabled\n");

    if ( !MEA_BM_hw_regs_access_enable )
        		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "BM HW Access Disabled\n");

    if ( !MEA_CPLD_hw_regs_access_enable )
        		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "CPLD HW Access Disabled\n");
    
    if (mea_drv_BasicTest_IF() != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_BasicTest_IF failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

#if 1

    if (MEA_during_warm_restart) {
        MEA_Write_Disable(MEA_FALSE);
    }
    //Note: do not alter the initialization sequence

    if (mea_DeviceInfo_Init(unit) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_DeviceInfo_Init \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    if (MEA_during_warm_restart) {
        MEA_Write_Disable(MEA_TRUE);
    }
#endif


    
    if (mea_drv_BasicTest_BM(MEA_FALSE) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  1 mea_drv_BasicTest_BM failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

	if ( MEA_API_Get_HwVersion(&hw_version) != MEA_OK )
    {

       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,

                         "%s - MEA_API_Get_HwVersion failed\n",__FUNCTION__);

       return MEA_ERROR; 

    }        



    return MEA_OK;
} 


MEA_Status mea_drv_AdvanceInit(MEA_Unit_t unit_i)
{
 

    if ( MEA_during_warm_restart ) { 
        MEA_Write_Disable(MEA_FALSE);
    }
	//Note: do not alter the initialization sequence

    if (mea_DeviceInfo_Init(unit_i)!= ENET_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - mea_DeviceInfo_Init \n",
			   			   __FUNCTION__);
	    return ENET_ERROR;
	}

    if ( MEA_during_warm_restart ) { 
        MEA_Write_Disable(MEA_TRUE);
    }

     
#if 1 
     
     if (MEA_Low_Port_Mapping_Init(MEA_UNIT_0) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_Low_Port_Mapping_Init failed \n",
                         __FUNCTION__);
     
    } 

#endif
#if defined(MEA_ETHERNITY) && (defined(MEA_ENV_S1_K7) || defined(MEA_PLAT_S1_VDSL_K7))

        if(mea_sfp_open_connection() != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"failed to open device driver connection");
        }


#endif
	
    if (mea_drv_Init_PortMap(unit_i) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_PortMap failed\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
	MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize PortState ...\n");
    if (mea_drv_Init_PortState(unit_i) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_PortState failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

    
    if (mea_drv_Init_Globals_Entry(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_Globals_Entry failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_global_GW_init(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_global_GW_init failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Init_Global_Interface_G999_1_Entry(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Init_Globals_Entry failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Interface_Init(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Interface_Init failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    
    
    
    if (mea_drv_Init_BM(unit_i) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_BM failed\n",
                          __FUNCTION__);
		return MEA_ERROR;
	}


    if (mea_drv_Init_IF(unit_i)==MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Init_IF failed\n",
                          __FUNCTION__);
        return MEA_ERROR;		
    }







 
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize MEA_InitDone = MEA_TRUE; ...\n");
    MEA_InitDone = MEA_TRUE;


	return MEA_OK;
}


MEA_Status MEA_API_Conclude_Services(MEA_Unit_t unit_i) 
{
    mea_drv_Conclude_Service_Table(unit_i);
	return MEA_OK;

}


MEA_Status MEA_API_Get_HwInternalVersion(MEA_Uint32* if_version,
                                         MEA_Uint32* bm_version) {
 
                                             
    if(b_bist_test){
     return MEA_OK;
    }

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

   if (if_version == NULL) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - if_version == NULL\n",
                        __FUNCTION__);
      return MEA_ERROR;
   }

   if (bm_version == NULL) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - bm_version == NULL\n",
                        __FUNCTION__);
      return MEA_ERROR;
   }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    *if_version = 0; /* T.B.D - Wait for memory map definition */

    *bm_version = 0;

    return MEA_OK;
}
MEA_Status MEA_API_Clear_Counters_All(MEA_Unit_t unit) {

    MEA_Events_Entry_dbt entry;
    
    if(MEA_reinit_start)
        return MEA_OK;



    if (MEA_API_Get_Events_Entry(MEA_UNIT_0,&entry) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_API_Get_Events failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TYPE]){          

        if (MEA_API_Clear_Counters_PMs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_PMs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if(MEA_Counters_Type_Enable[MEA_COUNTERS_QUEUE_TYPE]){        

        if (MEA_API_Clear_Counters_Queues(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_Queues failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
    if(MEA_Counters_Type_Enable[MEA_COUNTERS_BMS_TYPE]){          

        if (MEA_API_Clear_Counters_BM(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_BM failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if(MEA_Counters_Type_Enable[MEA_COUNTERS_INGRESSPORTS_TYPE] ){

        if (MEA_API_Clear_Counters_IngressPorts(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_IngressPorts failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
    if(MEA_Counters_Type_Enable[MEA_COUNTERS_INGRESS_TYPE] ){

        if (MEA_API_Clear_Counters_IngressS(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_IngressS failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
    if(MEA_Counters_Type_Enable[MEA_COUNTERS_RMONS_TYPE]){        

        if (MEA_API_Clear_Counters_RMONs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_RMONs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
    if(MEA_Counters_Type_Enable[MEA_COUNTERS_ANALYZERS_TYPE] && (MEA_PACKET_ANALYZER_TYPE1_SUPPORT)){
        /* Collect all ANALYZERs counters */
        if (MEA_API_Clear_Counters_ANALYZERs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_ANALYZERs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if( (MEA_STANDART_BONDING_SUPPORT) && MEA_Counters_Type_Enable[MEA_COUNTERS_BONDING_TYPE] ){
        if(MEA_API_Clear_Counters_Bonding_All(MEA_UNIT_0)!= MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_Bonding_All failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if (MEA_Counters_Type_Enable[MEA_COUNTERS_Virtual_Rmon_TYPE] ) {
        /* Collect all ANALYZERs counters */
        if (MEA_API_Clear_Counters_Virtual_Rmons(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_Virtual_Rmons failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

#if MEA_CCM_ENABLE
    if(MEA_Counters_Type_Enable[MEA_COUNTERS_ANALYZERS_LM_TYPE] && (MEA_PACKET_ANALYZER_TYPE2_SUPPORT)){
        if(MEA_API_Clear_Counters_LMs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_LMs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if(MEA_Counters_Type_Enable[MEA_COUNTERS_ANALYZERS_CCM_TYPE] && (MEA_PACKET_ANALYZER_TYPE2_SUPPORT)){
        if(MEA_API_Clear_Counters_CCMs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_CCMs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
#endif

    if (MEA_UTOPIA_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_UTOPIA_RMONS_TYPE]){
        if(MEA_API_Clear_Counters_Utopia_RMONs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_Utopia_RMONs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }




#ifdef MEA_PLATFORM_TDM_SUPPORT
    if(MEA_TDM_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_RMONS_TDM_TYPE]){
        /* Collect all provisioned PM counters */
        if (MEA_API_Clear_Counters_RMON_TDMs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_RMON_TDMs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if(MEA_TDM_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TDM_TYPE]){

        if (MEA_API_Clear_Counters_TDMs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_TDMs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if (MEA_TDM_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_TDM_EGRESS_FIFO_TYPE]){

        if (MEA_API_Clear_Counters_TDM_EgressFifoS(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_TDM_EgressFifoS failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

#endif
    if(MEA_GLOBAL_FORWARDER_SUPPORT_DSE  &&  MEA_Counters_Type_Enable[MEA_COUNTERS_FORWARDER_TYPE]){

        if (MEA_API_Clear_Counters_Forwarder(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_Forwarder failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

 
    

        if (MEA_HC_SUPPORT  &&  MEA_Counters_Type_Enable[MEA_COUNTERS_HC_RMON_TYPE]) {

            if (MEA_API_Clear_Counters_HC_RMONs(MEA_UNIT_0) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_Clear_Counters_HC_RMONs failed \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }








    return MEA_OK;

}
MEA_Status MEA_API_Collect_Counters_All(MEA_Unit_t unit) {

    if(MEA_reinit_start)
        return MEA_OK;

    if(MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TYPE]){          
    /* Collect all provisioned PM counters */
        if (MEA_API_Collect_Counters_PMs(MEA_UNIT_0) != MEA_OK) {
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - MEA_API_Collect_Counters_PMs failed \n",
                                 __FUNCTION__);
               return MEA_ERROR;
        }
    }

    


   

    if(MEA_Counters_Type_Enable[MEA_COUNTERS_BMS_TYPE]){          
    /* Collect BM global counters  */
        if (MEA_API_Collect_Counters_BMs(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_BMs failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }
    }

    if(MEA_Counters_Type_Enable[MEA_COUNTERS_INGRESSPORTS_TYPE] ){
    /* Collect IngressPorts counters (parser counters) */		 
        if (MEA_API_Collect_Counters_IngressPorts(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_IngressPorts failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }
    }
    if(MEA_Counters_Type_Enable[MEA_COUNTERS_INGRESS_TYPE] ){
        /* Collect Ingress counters (mac to bm) */		 
        if (MEA_API_Collect_Counters_IngressS(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Collect_Counters_IngressS failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
    

    if(MEA_Counters_Type_Enable[MEA_COUNTERS_RMONS_TYPE]){        
    /* Collect all RMONs counters */
        if (MEA_API_Collect_Counters_RMONs(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_RMONs failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }
    }
    if(MEA_Counters_Type_Enable[MEA_COUNTERS_ANALYZERS_TYPE] && (MEA_PACKET_ANALYZER_TYPE1_SUPPORT)){
    /* Collect all ANALYZERs counters */
        if (MEA_API_Collect_Counters_ANALYZERs(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_ANALYZERs failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }
    }


    if (MEA_Counters_Type_Enable[MEA_COUNTERS_ACL_TYPE]){
        if (MEA_API_Collect_Counters_ACLs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Collect_Counters_HC_RMONs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }


    if( (MEA_STANDART_BONDING_SUPPORT) && MEA_Counters_Type_Enable[MEA_COUNTERS_BONDING_TYPE] ){
        if(MEA_API_Collect_Counters_Bonding_ALL(MEA_UNIT_0)!= MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Collect_Counters_Bonding_ALL failed \n",
                __FUNCTION__);
            return MEA_ERROR;

        }
    }

#if MEA_CCM_ENABLE
    if(MEA_Counters_Type_Enable[MEA_COUNTERS_ANALYZERS_LM_TYPE] && (MEA_PACKET_ANALYZER_TYPE2_SUPPORT)){
        if(MEA_API_Collect_Counters_LMs(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_LMs failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }
    }

    if(MEA_Counters_Type_Enable[MEA_COUNTERS_ANALYZERS_CCM_TYPE] && (MEA_PACKET_ANALYZER_TYPE2_SUPPORT)){
        if(MEA_API_Collect_Counters_CCMs(MEA_UNIT_0) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_API_Collect_Counters_CCMs failed \n",
                             __FUNCTION__);
           return MEA_ERROR;
        }
    }
#endif

    if (MEA_UTOPIA_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_UTOPIA_RMONS_TYPE]){
        if(MEA_API_Collect_Counters_Utopia_RMONs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Collect_Counters_Utopia_RMONs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
   
    if (MEA_Virtual_Rmon_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_Virtual_Rmon_TYPE]) 
    {
        if (MEA_API_Collect_Counters_Virtual_Rmons(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Collect_Counters_Virtual_Rmons failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    
    }


#ifdef MEA_PLATFORM_TDM_SUPPORT
    if(MEA_TDM_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_RMONS_TDM_TYPE]){
        /* Collect all provisioned PM counters */
        if (MEA_API_Collect_Counters_RMON_TDMs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Collect_Counters_TDMs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if(MEA_TDM_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TDM_TYPE]){
        /* Collect all provisioned PM counters */
        if (MEA_API_Collect_Counters_TDMs(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Collect_Counters_TDMs failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
#endif
    if(MEA_GLOBAL_FORWARDER_SUPPORT_DSE  &&  MEA_Counters_Type_Enable[MEA_COUNTERS_FORWARDER_TYPE]){
        /* Collect all provisioned PM counters */
        if (MEA_API_Collect_Counters_Forwarder(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Collect_Counters_Forwarder failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    
   
        if(MEA_HC_RMON_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_HC_RMON_TYPE]){
            /* Collect all provisioned PM counters */
            if (MEA_API_Collect_Counters_HC_RMONs(MEA_UNIT_0) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_Collect_Counters_HC_RMONs failed \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }
    
#ifdef MEA_PLATFORM_TDM_SUPPORT
    if(MEA_TDM_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_TDM_EGRESS_FIFO_TYPE]){
        /* Collect all provisioned PM counters */
        if (MEA_API_Collect_Counters_TDM_EgressFifoS(MEA_UNIT_0) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Collect_Counters_TDM_EgressFifoS failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
    
    
        if(MEA_TDM_SUPPORT && MEA_Counters_Type_Enable[MEA_COUNTERS_CES_EVENT_TDM_TYPE]){
            /* check the ces event */
            {//start
             MEA_TDM_Event_group_dbt entry_group;
             MEA_Bool exist_group=MEA_FALSE;
             MEA_Uint32 groupId;
             MEA_TDM_Event_Subgroup_dbt entry_subgroup;
             MEA_Uint32 start,end,i;
             MEA_TdmCesId_t TdmCesId;
             MEA_TDM_CES_Event_dbt my_entry;

             MEA_OS_memset(&entry_group,0,sizeof(entry_group));
            MEA_API_Get_TDM_CES_EventGroup(MEA_UNIT_0, &entry_group ,&exist_group);
            if(exist_group && entry_group.Group0 !=0){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"group exist Group31_0= 0x%08x\n",entry_group.Group0);
              
              start=0;
              end=   (((MEA_TDM_NUM_OF_CES/32) > 1) ?  (MEA_TDM_NUM_OF_CES/32)-1 : 0);
              for(groupId=start; groupId<=end ;groupId++){
                MEA_API_Get_TDM_CES_Event_SubGroup(MEA_UNIT_0,groupId,&entry_subgroup);
                for(i=0;i<MEA_TDM_MAX_SUB_GROUP_EVENT;i++){
                TdmCesId = (MEA_TdmCesId_t)(groupId*32 + i);
                    if(MEA_API_Get_IsCESValid(MEA_UNIT_0,TdmCesId,MEA_TRUE)== MEA_TRUE){
                      MEA_API_Get_TDM_CES_Event(MEA_UNIT_0,TdmCesId,&my_entry);
                      MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"subgroup %d event  index =%d value %d\n",groupId,i, entry_subgroup.SubGruopEvent[i]);
                      MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"TdmCesId %d event Lbit=%3s R=%3s Loss=%3s\n",TdmCesId,MEA_STATUS_STR(my_entry.Lbit),
                          MEA_STATUS_STR(my_entry.Remote_packet_loss),
                          MEA_STATUS_STR(my_entry.LOSS));
                      

                    }
                }//for

              }    
              
              }

            }


            }//end
#endif


        if (MEA_Counters_Type_Enable[MEA_COUNTERS_QUEUE_TYPE]) {
            /* Collect MTU and MQS dropped packets for all ports  */
            if (MEA_API_Collect_Counters_Queues(MEA_UNIT_0) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_Collect_Counters_Queues failed \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }

        if (MEA_Counters_Type_Enable[MEA_COUNTERS_QUEUE_HANDLER_TYPE])
        {
            /*  Check the Queue */
            {
                MEA_Bool enableRinit = MEA_FALSE;
                if (MEA_QueueFull_Handler(MEA_UNIT_0, &enableRinit) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_COUNTERS_QUEUE_HANDLER_TYPE failed \n",
                        __FUNCTION__);
                    return MEA_ERROR;
                }
                if (enableRinit == MEA_TRUE) {
                    if (MEA_API_ReInit(MEA_UNIT_0) != MEA_OK)
                    {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_API_ReInit Fail \n");
                    }
                    return MEA_OK;
                }
              }



          }




        return MEA_OK;

}


MEA_Status MEA_API_ReInit (MEA_Unit_t   			  unit_i)
{

    MEA_Events_Entry_dbt events_entry;
    MEA_Globals_Entry_dbt globals_entry;

    MEA_Uint32 systemPLL,count;



    /* Verify Driver Already Init */
    if (!MEA_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Driver not init yet\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* -------- */
    

    MEA_Platform_ResetDevice(unit_i);

	if (b_bist_test) { 
		return   MEA_OK;
	}


    count = 0;
    //////////////////////////////////////////////////////////////////////////
    systemPLL = 0;
    while ((count != 1000) && systemPLL != 1)
    {
        MEA_OS_sleep(0, 1000 *1000);
        systemPLL = 0;
        systemPLL = ((MEA_API_ReadReg(MEA_UNIT_0, ENET_IF_GLOBAL1_REG, MEA_MODULE_IF) & 0x00000002) >> 1) & 0x01;// systemPLL
        if(systemPLL == 1)
            break;
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "systemPLL %d \n", systemPLL);
        count++;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "count= %d systemPLL %s\n", systemPLL, (systemPLL == 1) ? "Lock" : "unLock" );

    //////////////////////////////////////////////////////////////////////////
    mea_reinit_count++;
    /* Test that Module IF respond after the reset  */
    if (mea_drv_BasicTest_IF() != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_BasicTest_IF failed\n", __FUNCTION__);
        return MEA_ERROR;
    }

    /* Test that Module BM respond after the reset  */
    if (mea_drv_BasicTest_BM(MEA_TRUE) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - 2 mea_drv_BasicTest_BM failed\n", __FUNCTION__);
        return MEA_ERROR;
    }


    


    /* ReInit Globals (include Disable BM) */
    if (mea_drv_ReInit_Globals_Entry(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_Globals_Entry failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_global_GW_Reinit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_Globals_Entry failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    /* ReInit BM */
    if (mea_drv_ReInit_BM(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_BM failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    /* ReInit IF */
    if (mea_drv_ReInit_IF(unit_i) == MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_IF failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    if (mea_drv_ReInit_Global_Interface_G999_1_Entry(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_Globals_Entry failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_Interface_Reinit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_interface_Reinit failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    
    /* Clear the events */
    if (MEA_API_Get_Events_Entry(unit_i,&events_entry)!= MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_Events failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    if (MEA_API_Clear_WRITE_ENGINE_STATE(unit_i) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Clear_WRITE_ENGINE_STATE failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

 

    /* ReInit ENET Queue object */
	if (enet_ReInit_Queue(unit_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - enet_ReInit_Queue failed \n",
			   			   __FUNCTION__);
	    return ENET_ERROR;
	}

    /* ReInit Shaper Profile */
	if (enet_ReInit_Shaper_Profile(unit_i) != ENET_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - enet_ReInit_Shaper_Profile failed \n",
			   			   __FUNCTION__);
	    return ENET_ERROR;
	}

    /* ReInit IF LxCP Table */
    if (mea_lxcp_drv_ReInit(unit_i)!=MEA_OK){
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_lxcp_drv_ReInit  failed \n",
			   			   __FUNCTION__);
       return MEA_ERROR;
	}




    if(mea_drv_HC_Decomp_prof_RInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_Decomp_prof_RInit failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    if(mea_drv_HC_Comp_RInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_Comp_RInit failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_ReInit_IF_Counters_HC_RMON(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_IF_Counters_HC_RMON failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    // on Hc Compress need to clear the hash.


    if(mea_drv_HC_classifier_RInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_classifier_RInit failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
   

    if(mea_drv_TFT_RInit(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_TFT_RInit failed\n",
        __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_sflow_RInit(unit_i) != MEA_OK) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - mea_drv_sflow_RInit failed\n",
        __FUNCTION__);
    return MEA_ERROR;
    }

	if (mea_drv_ReInit_ACL5_Table(unit_i) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ReInit_ACL5_Table failed\n",
			__FUNCTION__);
		return MEA_ERROR;
	}

	if (mea_drv_ReInit_ACL5(unit_i) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - mea_drv_ReInit_ACL5 failed\n",
			__FUNCTION__);
		return MEA_ERROR;
	}

    if(mea_drv_Reinit_Lpm(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Reinit_Lpm failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    if (!b_bist_test)
    {

        if (mea_drv_VP_MSTP_RInit(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_VP_MSTP_RInit failed\n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        /* Open Tx */
        if (mea_drv_ReInit_TxEnable(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - drv_ReInit_TxEnable  failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }






        /* Enable BM  */
        MEA_API_Get_Globals_Entry(unit_i, &globals_entry);
        globals_entry.bm_config.val.bm_enable = MEA_TRUE;
        MEA_API_Set_Globals_Entry(unit_i, &globals_entry);

        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Sleep 1 sec before bm_enable  \n");
        MEA_OS_sleep(1, 0);
#ifdef MEA_LOCAL_MNG
        {

            MEA_Uint32 Globals1;
            Globals1 = mea_drv_get_if_global1(MEA_UNIT_0);

            Globals1 |= 1 << 3;
            MEA_API_WriteReg(MEA_UNIT_0, MEA_IF_GLOBAL_REG(1), Globals1, MEA_MODULE_IF);
            /************************************/
            /* disable default service on CPU   */
            /***********************************/
        }
#endif	
#if 1
        if (!b_bist_test)
        {


            if (MEA_GLOBAL_FORWARDER_SUPPORT_DSE) {

                MEA_Status retStatus = MEA_OK;



                retStatus |= MEA_API_Set_SE_Aging(MEA_UNIT_0, 1, MEA_TRUE);

                if (retStatus != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "setting reinit aging  failed \n");
                }

                MEA_OS_sleep(2, 500 * 1000 * 1000);

            }
            else {
                MEA_OS_sleep(0, 500 * 1000 * 1000);
            }
        }
#endif



        
        if (MEA_GLOBAL_FORWARDER_SUPPORT_DSE) {
#if 1
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "setting aging back  \n");

            if (MEA_API_Set_SE_Aging(unit_i,
                Global_Aging_interval,
                Global_Aging_enable) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_Set_SE_Aging failed \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
#endif
    }

        /* Open Rx */
        if (mea_drv_ReInit_RxEnable(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - drv_ReInit_RxEnable  failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "....... \r\n");
    MEA_reinit_start=MEA_FALSE;




    


    /* Return to Caller */
    return MEA_OK;
}


MEA_Status MEA_API_CheckDeviceHealth(MEA_Unit_t unit_i,
                                     MEA_DeviceHealthInfo_dbt *info_po)
{

    if (info_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - info_po == NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;
    } 

    info_po->raw = 0;
// 
//     /* Check queue stuck */
//     if (MEA_LowLevel_ReadReg(unit_i,
//                              0x4f8, 
//                              MEA_MODULE_BM,
//                              (globalMemoryMode==MEA_MODE_REGULAR_ENET3000) 
//                              ? globalMemoryMode : MEA_MODE_DOWNSTREEM_ONLY,
//                              MEA_MEMORY_READ_SUM)  == 0x00000204) {
//         info_po->val.queue_stuck = 1;
//     }
 
    return MEA_OK;


}

//  Calling this would turn on the warm restart flag.
// during the init and re provisioning process, if this flag is set then fpga writes 
// would be disabled during most of the this process 
MEA_Status MEA_API_Set_Warm_Restart(void) 
{
    MEA_during_warm_restart = MEA_TRUE;
    MEA_Write_Disable(MEA_TRUE);

    return MEA_OK;
}

MEA_Status MEA_API_Clear_Warm_Restart(void)
{
    MEA_during_warm_restart = MEA_FALSE;
    MEA_Write_Disable(MEA_FALSE);

    return MEA_OK;
}

MEA_Bool MEA_API_Get_Warm_Restart(void)
{
    return MEA_during_warm_restart;

}




