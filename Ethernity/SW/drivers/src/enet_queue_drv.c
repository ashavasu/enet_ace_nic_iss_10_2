/*--------------------------------------------------------------------------*/
/*   This file/directory and the information contained in it are            */
/*   proprietary and confidential to Ethernity Networks Ltd.  				*/
/*   No person is allowed to copy, reprint, reproduce or publish			*/
/*   any part of this document, nor disclose its contents to others, 		*/
/*   nor make any use of it, nor allow or assist others to make any			*/
/*   use of it - unless by prior written express 							*/
/*   authorization of Neralink  Networks Ltd and then only to the			*/
/*   extent authorized (c) copyright Ethernity Networks Ltd. 2002 			*/
/*--------------------------------------------------------------------------*/

/****************************************************************************/
/* 	Module Name:		 enet_queue_drv.c	   								*/	     
/*																			*/		 
/*   Module Description:	     											*/
/*                            												*/
/*  Date of Creation:	 14/09/06											*/		 
/*  Author Name:			 Alex Weis.										*/			 
/****************************************************************************/



/*--------------------------------------------------------------------------*/
/*             Includes                                                     */
/*--------------------------------------------------------------------------*/

#include "ENET_platform_types.h"
#include "mea_api.h" 
#include "mea_drv_common.h"
#include "enet_xPermission_drv.h"
#include "mea_bm_ep_drv.h"
#include "mea_port_drv.h"
#include "enet_shaper_profile_drv.h"
#include "enet_queue_drv.h"
#include "mea_Interface_drv.h"
#include "mea_limiter_drv.h"


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

#define WORK_PAD_LOCATION_INDEX   ((ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT==64) ? 81 : (ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT == 128) ?  200 : (ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT == 1024) ? (640+128) : (300) )    /* this is used to move ports in the location array */
#define WORK_PAD_LOCATION_INDEX_LQ1 (1023 - MAX_QUE_PER_PORT)

#define MAX_QUE_PER_PORT          (mea_drv_Get_DeviceInfo_maxClusterPerPort(MEA_UNIT_0)== 0 ?  32 : mea_drv_Get_DeviceInfo_maxClusterPerPort(MEA_UNIT_0))
#define QUEUE_SIZE_IN_BYTES_GRANULARITY 64
#define QUEUE_SIZE_IN_BYTES_MAX_VAL (2*(1<<20))
#define QUEUE_SIZE_IN_PACKET_MAX_VAL    (MEA_MQS_MAX_ALLOW)   //((4*1024))   

#define QUEUE_SIZE_IN_MC_MQS_MAX_VAL 511 //32
#define QUE_PER_PORT(port) ( mea_drv_Get_DeviceInfo_maxClusterPerPort(MEA_UNIT_0)== 0 ? (8) : MAX_QUE_PER_PORT )

#define MEA_MAX_OF_CLUSTER_STRICT_LEVEL (mea_drv_Get_DeviceInfo_maxClusterPerPort(MEA_UNIT_0)== 0 ? 8 : MAX_QUE_PER_PORT )

#define DISABLED_FIRST_CLUSTER 64
#define DISABLED_FIRST_PRI 512

#define MEA_MAX_PORT_FOR_GROUP 8 //old bonding
//#define DEBUG_QUEUE  printf



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef ENET_Queue_dbt enet_Queue_Public_dbt;

typedef struct {
	  ENET_QueueId_t            external_id; 
	  ENET_Bool                 is_fatherActive;
      MEA_Egress_protectType_t  is_father1p1;
} enet_Queue_Private_dbt;

typedef struct {
  enet_Queue_Public_dbt  dbt_public;
  enet_Queue_Private_dbt dbt_private;
} enet_Queue_dbt;




typedef struct {
  ENET_QueueId_t  Qid;
  ENET_Bool  isValid;
} enet_ExternalQidDbt;

typedef struct {
    ENET_Bool  isValid;
    MEA_PortState_te portState;
}enet_ExternalQid_state_Dbt;

typedef struct {
    MEA_Uint32  group :4; 
    MEA_Uint32  valid   :1;
    MEA_Uint32  pad    :27;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  ports[2]; 
}MEA_Queue_portsGroupe_t;



typedef struct {
    MEA_Uint32  valid : 1;
    MEA_Uint32   queueST_count[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];
    


}MEA_ClusterQueue_state_t;



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

//static enet_Queue_dbt* enet_queue_container
//                               [ENET_PLAT_MAX_UNIT_ID+1]
//      						 [ENET_PLAT_MAX_QUEUE_ID+1];

static enet_ExternalQid_state_Dbt      *enet_externalQid_state_Table=NULL;

static ENET_GroupId_t             enet_Queue_group_id[ENET_PLAT_MAX_UNIT_ID+1];
static ENET_Bool                  enet_Queue_InitDone [ENET_PLAT_MAX_UNIT_ID+1];
static enet_ExternalQidDbt        *enet_externalQidTable=NULL;
enet_ReverseMappingIndx_dbt              enet_Hw_portReverseMappingIndx[REVERSE_MAPPING_PORT_INDX_SIZE];  /* this is table 20*/
enet_Queue_Cluster2Port_dbt              enet_Hw_Cluster2Port[REVERSE_MAPPING_TABLE_SIZE];
enet_ReverseMappingData_dbt              enet_Hw_portReverseMappingData[REVERSE_MAPPING_TABLE_SIZE];  /* this is table 20*/
enet_HwMirror_ClusterPortInformation_dbt enet_HwMirror_ClusterPortInformation[REVERSE_MAPPING_PORT_INDX_SIZE];
enet_HwMirror_ClusterToPort_dbt          enet_HwMirror_ClusterToPort         [REVERSE_MAPPING_TABLE_SIZE];



static MEA_ClusterQueue_state_t         *MEA_ClusterQueue_State = NULL;


MEA_Uint32 mea_global_QueueFull_Handler_reset_sec;
MEA_Uint32 mea_global_QueueFull_Handler_TH_MQS;
MEA_Uint32 mea_global_QueueFull_Handler_TH_currentPacket;

 MEA_Bool Init_QUEUE = MEA_FALSE;

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_UpdatetDevice_Queue (ENET_Unit_t       unit_i);
static ENET_Status enet_ClusterPortInfo_UpdateHw(ENET_PortId_t portId_i);
static ENET_Status enet_Cluster2Port_UpdateHw   (ENET_Uint32   index_i,ENET_Bool clusterMode);
static ENET_Status enet_delete_Queue (ENET_Unit_t    unit_i,ENET_QueueId_t id_i); /*external */

static ENET_Status ENET_drv_Set_Queue       (ENET_Unit_t     unit_i,
                                             ENET_QueueId_t  id_i, /* external */
                                             ENET_Queue_dbt *entry_i);

static ENET_Status enet_Cluster_Set_Update_admin(ENET_Unit_t           unit_i,
                                                ENET_QueueId_t            queue_internal,
                                                ENET_Bool                 adminOn,
                                                MEA_Bool                  is_fatherActive);

ENET_Status ENET_GetFirst_Queue  (ENET_Unit_t     unit_i,
								  ENET_QueueId_t *id_o,
                                  ENET_Queue_dbt *entry_o,
                                  ENET_Bool      *found_o);
ENET_Status ENET_GetNext_Queue   (ENET_Unit_t     unit_i,
                                  ENET_QueueId_t *id_io,
                                  ENET_Queue_dbt *entry_o,
                                  ENET_Bool      *found_o);
ENET_Status ENET_Delete_Queue    (ENET_Unit_t    unit_i,
                                  ENET_QueueId_t id_i);
static ENET_Status enet_queue_UpdateHwMQS(ENET_Unit_t    unit_i,
                                          ENET_QueueId_t      queue,
                                          enet_Queue_dbt      *entry,
                                          enet_Queue_dbt      *old_entry);
static ENET_Status enet_queue_UpdateHwPriStrictLevel(ENET_Unit_t    unit_i,
                                                     ENET_QueueId_t            queue,
                                                     enet_Queue_dbt            *entry,
                                                     enet_Queue_dbt            *old_entry);

static ENET_Status enet_queue_UpdateHwPriWfq(ENET_Unit_t               unit_i,
                                             ENET_QueueId_t            queue,
                                             enet_Queue_dbt            *entry,
                                             enet_Queue_dbt            *old_entry);

static ENET_Status enet_queue_UpdateHwClusterWFQ(ENET_Unit_t               unit_i,
                                                 ENET_QueueId_t            queue,
                                                 enet_Queue_dbt            *entry,
                                                 enet_Queue_dbt            *old_entry);
static void enet_queue_UpdateHwEntryClusterWFQ(ENET_Queue_dbt            *entry,
                                               ENET_QueueId_t            queue);
#if 0
static ENET_Status enet_Cluster_Update_admin(ENET_Unit_t                unit_i,
                                            ENET_QueueId_t            queue,
                                            enet_Queue_dbt            *entry,
                                            enet_Queue_dbt            *old_entry);
#endif
static ENET_Status enet_queue_UpdateHw_mc_MQS(ENET_Unit_t                unit_i,
                                              ENET_QueueId_t            queue,
                                              enet_Queue_dbt            *entry,
                                              enet_Queue_dbt            *old_entry);
static ENET_Status enet_queue_UpdateHwMTU(ENET_Unit_t               unit_i,
                                          ENET_QueueId_t            queue,
                                          enet_Queue_dbt            *entry,
                                          enet_Queue_dbt            *old_entry);

static ENET_Status enet_queue_MultiPortsUpdateHw(ENET_Unit_t               unit_i,
                                          ENET_QueueId_t            queue,
                                          enet_Queue_dbt            *entry,
                                          enet_Queue_dbt            *old_entry);


static MEA_Status mea_Queue_UpdateHwShaperCmd (ENET_Unit_t       unit_i,
                                               ENET_QueueId_t    queue_Id,
                                               enet_Queue_dbt   *entry_i,
                                               enet_Queue_dbt   *old_entry_i);

static MEA_Status mea_PriQueue_UpdateHwShaperCmd (ENET_Unit_t                  unit_i,
                                               ENET_QueueId_t                  queue_Id,
                                               ENET_Uint32                     pri,
                                               ENET_Queue_priority_queue_dbt   *entry_i,
                                               ENET_Queue_priority_queue_dbt   *old_entry_i);


ENET_Status enet_UpdatetDevice_NumPriInCluster(ENET_Unit_t unit_i);
void SetDefaultClusterToPort(ENET_PortId_t port,ENET_QueueId_t cluster);
static ENET_Status enet_queue_SetDefaultMQS(ENET_Unit_t unit_i,
                                            ENET_QueueId_t qid);
static ENET_Uint32 enet_getNumOfClusterInPort(ENET_PortId_t port);

static void enet_queue_MultiPortsUpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4);
static MEA_Status mea_queue_drv_config_potr_to_groupHw(ENET_Unit_t unit_i,
                                                       MEA_Port_t port,
                                                       MEA_Uint32 value);

void mea_get_queue_group_ID_for_port(MEA_Uint32 cluster, MEA_Uint32 *groupId);

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
void mea_get_queue_group_ID_for_port(MEA_Uint32 cluster, MEA_Uint32 *groupId)
{
    if (groupId == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s  groupId = NULL \n",
            __FUNCTION__);
        return;
    }

    if (((cluster >= 0) && (cluster <= 47))     ||

        (cluster == 127) || (cluster == 120)    ||
        ((cluster >= 100) && (cluster <= 103))  ||
        ((cluster >= 104) && (cluster <= 107))  ||
        ((cluster >= 124) && (cluster <= 126))  ||
        ((cluster >= 118) && (cluster <= 119)) 
        
        ){
        *groupId = 0;
    }
    if ((cluster == 122)||
        ((cluster >= 48) && (cluster <= 95))    ||
        ((cluster >= 96) && (cluster <= 99))    ||
        ((cluster >= 108) && (cluster <= 111))  ||
        ((cluster >= 112) && (cluster <= 115))  ||
        ((cluster >= 116) && (cluster <= 117))  
       
        
        ){
        *groupId = 1;
    }
    if (
        ((cluster >= 128) && (cluster <= 191)) 
        ){
        *groupId = 2;
    }

    if (
         (cluster >= 192) && (cluster <= 255)

        ){
        *groupId = 3;
    }

    

   

    return;


}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_InitDevice_Queue>                                      */
/*               <enet_ReInitDevice_Queue>                                    */
/*               <enet_update_queue_cluster_hw>                               */
/*               <enet_Cluster2Port_UpdateHwEntry>                            */
/*               <enet_Cluster2Port_UpdateHw>                                 */
/*               <enet_ClusterPortInfo_UpdateHwEntry>                         */
/*               <enet_ClusterPortInfo_UpdateHw>                              */
/*                                                                            */
/*   These function are use for HW access .using HW table + mirror           */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_InitDevice_Queue (ENET_Unit_t  unit_i)
{
	ENET_Uint16		location;
	ENET_Uint16		cluster;
    ENET_PortId_t   portId;
    ENET_Uint16     count;
    MEA_Uint32      size;
    ENET_Uint32     PriQueue;     
    ENET_Uint32     numOf_blocks_PriQue_support=0;
    ENET_Queue_priority_queue_dbt PriQueue_Entry;

    /* Init enet_HwMirror_ClusterToPort */
    count=MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort);
    MEA_OS_memset(&enet_HwMirror_ClusterToPort[0],
                  0,
                  count*sizeof(enet_HwMirror_ClusterToPort[0]));
    for (location=0;location<count;location++) {
        enet_HwMirror_ClusterToPort[location].PortNumber =UNASSIGNED_QUEUE_VAL-1; /*force HW update*/
	}

    /* Init enet_HwMirror_ClusterPortInformation */
    count=MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterPortInformation);
    MEA_OS_memset(&enet_HwMirror_ClusterPortInformation[0],
                  0,
                  count*sizeof(enet_HwMirror_ClusterPortInformation[0]));
    for (portId=0;portId<count;portId++) {
		enet_HwMirror_ClusterPortInformation[portId].location=UNASSIGNED_QUEUE_VAL-1; /*force HW update*/
	}


    /* Init enet_Hw_Cluster2Port */
    count=MEA_NUM_OF_ELEMENTS(enet_Hw_Cluster2Port);
    MEA_OS_memset(&enet_Hw_Cluster2Port[0],
                  0,
                  count*sizeof(enet_Hw_Cluster2Port[0]));
    for (cluster=0;cluster<count;cluster++) {
		enet_Hw_Cluster2Port[cluster].PortNumber = UNASSIGNED_QUEUE_VAL;
	}

    /* Init enet_Hw_Cluster2Port */
    count=MEA_NUM_OF_ELEMENTS(enet_Hw_portReverseMappingData);
    MEA_OS_memset(&enet_Hw_portReverseMappingData[0],
                  0,
                  count*sizeof(enet_Hw_portReverseMappingData[0]));
    for (location=0;location<count;location++) {
		enet_Hw_portReverseMappingData[location]=UNASSIGNED_QUEUE_VAL;
	}


    /* Init enet_Hw_portReverseMappingIndx */
    count=MEA_NUM_OF_ELEMENTS(enet_Hw_portReverseMappingIndx);
    MEA_OS_memset(&enet_Hw_portReverseMappingIndx[0],
                  0,
                  count*sizeof(enet_Hw_portReverseMappingIndx[0]));
    for (portId=0;portId<count;portId++) {
        enet_Hw_portReverseMappingIndx[portId].location=UNASSIGNED_QUEUE_VAL;
	}
	
    
    
    
    size = ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT;
    size = size * sizeof(MEA_ClusterQueue_state_t);
    MEA_ClusterQueue_State = (MEA_ClusterQueue_state_t*)MEA_OS_malloc(size);
    if (MEA_ClusterQueue_State == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_ClusterQueue_State failed (size=%d)\n",
            __FUNCTION__, size);
        return MEA_ERROR;
    }
    MEA_OS_memset(MEA_ClusterQueue_State, 0, size);



    
    /* Init enet_externalQid_state_Table*/
     size = ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT;
     size = size * sizeof(enet_ExternalQid_state_Dbt);
    
     enet_externalQid_state_Table = (enet_ExternalQid_state_Dbt*)MEA_OS_malloc(size);
    if (enet_externalQid_state_Table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocat enet_externalQidTable failed (size=%d)\n",
                          __FUNCTION__,size);
        return MEA_ERROR;
    }
    MEA_OS_memset(enet_externalQid_state_Table,0, size);

    /* Init enet_externalQidTable */
     size = ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT;
     size = size * sizeof(enet_ExternalQidDbt);
    enet_externalQidTable = (enet_ExternalQidDbt*)MEA_OS_malloc(size);
    if (enet_externalQidTable == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate enet_externalQidTable failed (size=%d)\n",
                          __FUNCTION__,size);
        return MEA_ERROR;
    }
    
    MEA_OS_memset(enet_externalQidTable,0, size);
    MEA_OS_memset(&PriQueue_Entry,0,sizeof(PriQueue_Entry));
	for (cluster=0 ; cluster<ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; cluster++)
	{
		enet_externalQidTable[cluster].isValid=ENET_FALSE;
		enet_externalQidTable[cluster].Qid =UNASSIGNED_QUEUE_VAL ;
	
        if( MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE){	
            numOf_blocks_PriQue_support=mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support();
            for(PriQueue=0;PriQueue<8;PriQueue++){
                if( PriQueue >= (8-numOf_blocks_PriQue_support)){
                    if(mea_PriQueue_UpdateHwShaperCmd(unit_i,cluster,PriQueue,
                                                 &PriQueue_Entry,
                                                       NULL )!=MEA_OK){
                                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                       "%s - mea_PriQueue_UpdateHwShaperCmd failed\n",
                                       __FUNCTION__);
                     }
                 }
            }
        }
    
    }

    /* Update Hardware */
	if (enet_UpdatetDevice_Queue(unit_i) !=ENET_OK)
	{
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				   "HW update error\n",
				   __FUNCTION__);

		return ENET_ERROR;
	}

    /* Update Hardware with Num Pri In Clusters */
	if (enet_UpdatetDevice_NumPriInCluster(unit_i) !=ENET_OK)
	{
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				   "HW update error\n",
				   __FUNCTION__);

		return ENET_ERROR;
	}


    /* Set default MQS */
	if (enet_queue_SetDefaultMQS(unit_i,UNASSIGNED_QUEUE_VAL) !=ENET_OK)
	{
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				   "enet_queue_SetDefaultMQS error\n",
				   __FUNCTION__);

		return ENET_ERROR;
	}



   
    

	return ENET_OK;
}

ENET_Status enet_ReInitDevice_Queue (ENET_Unit_t  unit_i)
{
	ENET_Uint16		location;
	ENET_QueueId_t	cluster;
    ENET_PortId_t   portId;
    ENET_Bool       save_InitDone;
 	enet_Queue_dbt  entry;
	ENET_Uint32     len;
    ENET_Uint32     PriQueue;
    ENET_Uint32     numOf_blocks_PriQue_support;


    /* Reset the InitDone to force Update Hardware */
    save_InitDone = enet_Queue_InitDone[unit_i];
    enet_Queue_InitDone[unit_i] = ENET_FALSE;


    /* ReInit the Hardware PortInfo and Cluster2Port */
    for (portId=0;portId<MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterPortInformation);portId++) {

        if (portId == 128){ /**/
            portId = 128;
        }

		

	    if (MEA_API_Get_IsPortValid(portId, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE)==MEA_FALSE) {
            // need to check if if Vport for bonding 
            if (MEA_STANDART_BONDING_SUPPORT) {
                if(MEA_API_Get_Is_VirtualPort_Valid(portId, MEA_TRUE) == MEA_FALSE)
                    continue;
            }
            else {
                continue;
            }
        }

        location=enet_HwMirror_ClusterPortInformation[portId].location;
        if (location == UNASSIGNED_QUEUE_VAL) {
            continue;
        }

        for (; location < MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort); location++) {

            cluster = enet_HwMirror_ClusterToPort[location].queueId;
            if (cluster == UNASSIGNED_QUEUE_VAL) {
                break;
            }
#ifdef MEA_ENV_256PORT			
            if (cluster >= 128 && cluster <= 255){
                if (enet_HwMirror_ClusterToPort[512 + (cluster-128)].PortNumber != portId) {
                    break;
                }
            }
            else{

                if (enet_HwMirror_ClusterToPort[cluster].PortNumber != portId) {
                    break;
                }
#else
            if (enet_HwMirror_ClusterToPort[cluster].PortNumber != portId) {
                break;
#endif				
            }

            /* Update the HW Cluster2Port by Index */
            if (enet_Cluster2Port_UpdateHw(location,ENET_FALSE) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - enet_Cluster2Port_UpdateHw(%d,FALSE) failed\n",
                                  __FUNCTION__,location);
                return ENET_ERROR;
            }

            /* Update the HW Cluster2Port by Cluster */
            if (enet_Cluster2Port_UpdateHw(cluster,ENET_TRUE) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - enet_Cluster2Port_UpdateHw(%d,TRUE) failed\n",
                                  __FUNCTION__,cluster);
                return ENET_ERROR;
            }


            /* Get the Queue group entry */
        	len=sizeof(entry);
	        if (ENET_Get_GroupElement(unit_i,
		                              enet_Queue_group_id[unit_i],
					        		  cluster,
		                              &entry,
							          &len) != ENET_OK) {
	            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				        		  "%s - ENET_Get_Queue failed (id=%d)\n",
						          __FUNCTION__,cluster);
                return ENET_ERROR;
            }

            /* Update the Cluster WFQ */
            if (entry.dbt_public.mode.type == ENET_QUEUE_MODE_TYPE_WFQ ){
                if (enet_queue_UpdateHwClusterWFQ(unit_i,cluster,&entry,NULL) != ENET_OK) {
    	            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	    			        		  "%s - enet_queue_UpdateHwClusterWFQ failed (id=%d)\n",
		    				          __FUNCTION__,cluster);
                    return ENET_ERROR;
                }
	        }
			
            /* Update the Cluster MTU */
			if (enet_queue_UpdateHwMTU(unit_i,cluster,&entry,NULL) != ENET_OK) {
        		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			          	      "%s - enet_queue_UpdateHwMTU failed (id=%d)\n",
    				          __FUNCTION__,cluster);
				return ENET_ERROR;
			}
            /* Update the Cluster  */
            if (enet_queue_MultiPortsUpdateHw(unit_i,cluster,&entry,NULL) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- enet_queue_MultiPortsUpdateHw failed (id=%d)\n",
                    __FUNCTION__,cluster);
                return ENET_ERROR;
            }
            if(enet_queue_UpdateHw_mc_MQS(unit_i,cluster,&entry,NULL) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - enet_queue_UpdateHw_mc_MQS failed (id=%d)\n",
                    __FUNCTION__,cluster);
                return ENET_ERROR;
            }

			/* update MQS priq queue*/
            if (enet_queue_UpdateHwMQS(unit_i,cluster,&entry,NULL) != ENET_OK) {
            	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				          	      "%s - enet_queue_UpdateHwMQS failed (id=%d)\n",
        				          __FUNCTION__,cluster);
                return ENET_ERROR;
	        }

			/* update PriQ Strict Level */
            if (enet_queue_UpdateHwPriStrictLevel(unit_i,cluster,&entry,NULL) != ENET_OK) {
            	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				          	      "%s - enet_queue_UpdateHwPriStrictLevel failed (id=%d)\n",
        				          __FUNCTION__,cluster);
                return ENET_ERROR;
	        }

			/* update PriQ Wfq */
            if (enet_queue_UpdateHwPriWfq(unit_i,cluster,&entry,NULL) != ENET_OK) {
            	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				          	      "%s - enet_queue_UpdateHwPriWfq failed (id=%d)\n",
        				          __FUNCTION__,cluster);
                return ENET_ERROR;
            }

            /* Update Shaper */
            if( MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_CLUSTER_TYPE) == MEA_SHAPER_SUPPORT_CLUSTER_TYPE){	
                if(mea_Queue_UpdateHwShaperCmd(unit_i,cluster,&entry,NULL)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			            		      "%s - mea_Queue_UpdateHwShaperCmd failed\n",
        				            __FUNCTION__);
                    return ENET_ERROR;
                }
            }
            /* Update Shaper PriQueue*/
            if( MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE){	
               numOf_blocks_PriQue_support=mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support();
                for(PriQueue=0;PriQueue<8;PriQueue++){
                    if( PriQueue >= (8-numOf_blocks_PriQue_support)){
                        if(mea_PriQueue_UpdateHwShaperCmd(unit_i,cluster,PriQueue,
                                                 &entry.dbt_public.pri_queues[PriQueue],
                                                       NULL )!=MEA_OK){
                                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                       "%s - mea_PriQueue_UpdateHwShaperCmd failed\n",
                                       __FUNCTION__);
                        }
                    }
                }
            }
        }


        /* Update Hw Port Information */
        if (enet_ClusterPortInfo_UpdateHw(portId) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_ClusterPortInfo_UpdateHw(%d) failed\n",
                              __FUNCTION__,portId);
           return ENET_ERROR;
        } 


    }

    /* Update Hardware with Num Pri In Clusters */
	if (enet_UpdatetDevice_NumPriInCluster(unit_i) !=ENET_OK) {
    	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				   "HW update error\n",
				   __FUNCTION__);
		return ENET_ERROR;
	}

    /* Restore InitDone */
    enet_Queue_InitDone[unit_i] = save_InitDone;

    /* Return to Caller */
	return ENET_OK;
}

ENET_Status enet_InitDefaultQueueForPort(ENET_Unit_t  unit_i,
    ENET_PortId_t port,
    ENET_QueueId_t Qid) /*External*/
{
    ENET_Queue_dbt  entry;
    ENET_Uint16		i;
    MEA_Globals_Entry_dbt globals_entry;
    MEA_PortsMapping_t  portMap;
    MEA_Status ret = MEA_ERROR;
    MEA_Bool TLS_Mode   = MEA_FALSE;
    MEA_Bool PortIs_10G = MEA_FALSE;


    //ENET_Uint16 groupId = 0;


    MEA_OS_memset(&entry, 0, sizeof(entry));

//     if (groupId){
//         groupId = entry.groupId;
//     }

    MEA_OS_strcpy(entry.name, ENET_PLAT_GENERATE_NEW_NAME);
    entry.port.type = ENET_QUEUE_PORT_TYPE_PORT;
    entry.port.id.port = port;
    entry.adminOn = MEA_TRUE;
    if (MEA_API_Get_Globals_Entry(unit_i, &globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_API_Get_Globals_Entry failed\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_Low_Get_Port_Mapping_By_key(unit_i,
        MEA_PORTS_TYPE_PHYISCAL_TYPE,
        port,
        &portMap) != MEA_OK){
        return ENET_ERROR;
    }


    if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
        entry.mode.type = ENET_QUEUE_MODE_TYPE_RR;
        entry.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL; // because fpga use this
    }
    else{
        entry.mode.type = ENET_QUEUE_MODE_TYPE_WFQ;
        entry.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
    }


    entry.MTU = MEA_QUEUE_CLUSTER_MTU_DEF_VAL;
    entry.shaper_enable = MEA_FALSE;
    entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
    MEA_OS_memset(&entry.shaper_info, 0, sizeof(entry.shaper_info));
    
    if (mea_drv_get_interfaceType_by_portId(unit_i, port) == MEA_INTERFACETYPE_ENHANCED_TLS) {
        TLS_Mode = MEA_TRUE;
    }
    if ((mea_drv_get_interfaceType_by_portId(unit_i, port) == MEA_INTERFACETYPE_SINGLE10G) ||
        (mea_drv_get_interfaceType_by_portId(unit_i, port) == MEA_INTERFACETYPE_XAUI)      ||
        (mea_drv_get_interfaceType_by_portId(unit_i, port) == MEA_INTERFACETYPE_XLAUI)         ) {
        PortIs_10G = MEA_TRUE;
    }

    for (i = 0; i < ENET_MAX_NUMBER_OF_PRI_QUEUE; i++)
    {


        if (MEA_Platform_Get_Calculate_descriptor() <= 2048) {
            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF0_VAL(i);
            entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF0_VAL(i);
            entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF0_VAL(i);
        }
        else {
            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL(i);
            entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL(i);
            entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL(i);
        }

        if (TLS_Mode == MEA_TRUE) { /*The Size of BMQS and MQS need to change */
            if (MEA_Platform_Get_Calculate_descriptor() > (16*1024)){
            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_10G(i);
            entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_10G(i);
            entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_10G(i);
            }else{
            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS(i);
            entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL(i);
            entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS(i);
            }

        }
        if (PortIs_10G == MEA_TRUE) {

            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_10G(i);
            entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_10G(i);
            entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_10G(i);
        }

        entry.pri_queues[i].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;

        if (port == 127 && PortIs_10G == MEA_FALSE) { /*CPU port*/

            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU(i);
            entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_CPU(i);
            entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_CPU(i);
        }
        if (port == 127){
            entry.pri_queues[i].mtu = 1536;
        }

        if (MEA_GLOBAL_PRI_Q_MODE_DEF_VAL != MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY){
            entry.pri_queues[i].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
            entry.pri_queues[i].mode.value.strict_priority = ENET_PQ_STRICT_DEF_VAL;
        }
        else{
            entry.pri_queues[i].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
            entry.pri_queues[i].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
        }
        
    }
#if !defined(MEA_OS_DEVICE_MEMORY_SIMULATION) 
#if defined(MEA_OS_SYAC) || defined(MEA_PLAT_S1_VDSL_K7) || defined(MEA_PLAT_VDSL_64)

#if defined(MEA_ENV_256PORT)
    entry.groupId = 0;
    mea_get_queue_group_ID_for_port((MEA_Uint32)Qid, &entry.groupId);
    //entry.groupId= (ENET_Uint32)groupId;
    
#else
    if((ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT == 64)){
        entry.groupId=0;
    }else{
        if(entry.port.id.port <=63 && portMap.portType == MEA_PORTTYPE_G999_1MAC){
            entry.groupId=1;
        }else{
            if(portMap.portType == MEA_PORTTYPE_G999_1MAC){
                entry.groupId=0;
            }

        }
    }
#endif

#endif
#else
    {
        MEA_Uint32 Type = 0;
#if defined(MEA_OS_DEVICE_SIMULATION_DEF)
        Type = (MEA_Uint32)MEA_OS_DEVICE_SIMULATION_DEF;
#endif
        if (Type == MEA_OS_DEVICE_SIMULATION_VDSlK7){
            if (entry.port.id.port <= 63 && portMap.portType == MEA_PORTTYPE_G999_1MAC){
                entry.groupId = 1;
            }
            else{
                if (portMap.portType == MEA_PORTTYPE_G999_1MAC){
                    entry.groupId = 0;
                }

            }
        }
        if (Type == MEA_OS_DEVICE_SIMULATION_VDSlK7_196 || Type == MEA_OS_DEVICE_SIMULATION_VDSlK7_196_2SFP)
        {

            //entry.groupId = 0;
            mea_get_queue_group_ID_for_port(Qid, &entry.groupId);
            //entry.groupId = (ENET_Uint32)groupId;
            
        
        
        }

    }
#endif

    if (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) {
        if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_VDSL64) == 0)) {
            if (entry.port.id.port <= 63 && portMap.portType == MEA_PORTTYPE_G999_1MAC) {
                entry.groupId = 1;
            }
            else {
                if (portMap.portType == MEA_PORTTYPE_G999_1MAC) {
                    entry.groupId = 0;
                }

            }
        }
    }


    if (MEA_device_environment_info.MEA_PRODUCT_NAME_enable == MEA_TRUE) {
        if ((MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_EPC_7045_1K) == 0)|| 
            (MEA_OS_strcmp(MEA_device_environment_info.PRODUCT_NAME, MEA_BOARD_TYPE_RN7035_1K) == 0)
            ) {
            
			if (entry.port.id.port == 101 ) {
                entry.groupId = 2;
            }
			
			
            if (entry.port.id.port >= 80 && entry.port.id.port <=86) {
                entry.groupId = 3 + entry.port.id.port - 80;
            }
        }
    }



    ret = ENET_Create_Queue(ENET_UNIT_0, &entry, &Qid);
    if (ret == MEA_OK){
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " QidExternal = %d groupId=%d ,port=%d \n", Qid, entry.groupId,entry.port.id);
    }
    return ret;
}

/*----------------------------------------------------------------------------*/
/*               <enet_UpdatetDevice_Queue>                               */
/*----------------------------------------------------------------------------*/
ENET_Status enet_UpdatetDevice_Queue (ENET_Unit_t       unit_i)
{
	ENET_Bool retVal = ENET_OK;


	retVal =  enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort),MEA_FALSE);
    if (retVal) {
	    retVal =  enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort),MEA_TRUE);
    if (retVal == ENET_OK) {
		    retVal =  enet_ClusterPortInfo_UpdateHw(REVERSE_MAPPING_PORT_INDX_SIZE);
        }
    }
	return (retVal);
}

/*----------------------------------------------------------------------------*/
/*               <enet_UpdatetDevice_NumPriInCluster>                         */
/*----------------------------------------------------------------------------*/
ENET_Status enet_UpdatetDevice_NumPriInCluster(ENET_Unit_t unit_i){

	  ENET_WriteReg(ENET_UNIT_0,ENET_BM_CLUSTER_SINGLE_FIRST_CLUSTER,DISABLED_FIRST_CLUSTER,ENET_MODULE_BM);
	  ENET_WriteReg(ENET_UNIT_0,ENET_BM_CLUSTER_SINGLE_FIRST_PRI    ,DISABLED_FIRST_PRI,ENET_MODULE_BM);
      ENET_WriteReg(ENET_UNIT_0,ENET_BM_CLUSTER_DOUBLE_FIRST_CLUSTER,DISABLED_FIRST_CLUSTER,ENET_MODULE_BM);
      ENET_WriteReg(ENET_UNIT_0,ENET_BM_CLUSTER_DOUBLE_FIRST_PRI    ,DISABLED_FIRST_PRI,ENET_MODULE_BM);
	  ENET_WriteReg(ENET_UNIT_0,ENET_BM_CLUSTER_QUAD_FIRST_CLUSTER  ,DISABLED_FIRST_CLUSTER,ENET_MODULE_BM);
	  ENET_WriteReg(ENET_UNIT_0,ENET_BM_CLUSTER_QUAD_FIRST_PRI      ,DISABLED_FIRST_PRI,ENET_MODULE_BM);
      ENET_WriteReg(ENET_UNIT_0,ENET_BM_CLUSTER_OCTAL_FIRST_CLUSTER ,0,ENET_MODULE_BM);
      ENET_WriteReg(ENET_UNIT_0,ENET_BM_CLUSTER_OCTAL_FIRST_PRI     ,0,ENET_MODULE_BM);
	  return ENET_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_PriQueue_UpdateHwEntryShaperCmd>                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_PriQueue_UpdateHwEntryShaperCmd(ENET_Queue_priority_queue_dbt *entry/*,  MEA_Uint32 shaperId*/)
{
	MEA_Uint32 val,temp;
    //MEA_Uint32   shaper_Id=shaperId;
    
    /* Build register 0 */
    val   = 0;
    if (entry->shaperPri_enable == MEA_TRUE){
		temp  = ENET_SHAPER_BYPASS_TYPE_ENABLE;
		temp &= (MEA_BM_SHAPER_BYPASS_MASK_PRIQUEUE >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PRIQUEUE)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PRIQUEUE);
		val  |= temp;

        temp  = entry->shaper_info.CBS;    // the initial values for the port CIR credit is the profile max ( = profile CBS)
		temp &= (MEA_BM_SHAPER_CIR_BACKET_MASK_PRIQUEUE >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_CIR_BACKET_MASK_PRIQUEUE)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_CIR_BACKET_MASK_PRIQUEUE);
		val  |= temp;
	        
        temp = entry->shaper_Id;
		temp &= (MEA_BM_SHAPER_PROF_NUMBER_MASK_PRIQUEUE >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_PROF_NUMBER_MASK_PRIQUEUE)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_PROF_NUMBER_MASK_PRIQUEUE);
		val  |= temp;

		temp  = entry->Shaper_compensation;
		temp &= (MEA_BM_SHAPER_COMP_MASK_PRIQUEUE >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PRIQUEUE)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PRIQUEUE);
		val  |= temp;
	}else{
		temp  = ENET_SHAPER_BYPASS_TYPE_DISABLE;
		temp &= (MEA_BM_SHAPER_BYPASS_MASK_PRIQUEUE >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PRIQUEUE)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_PRIQUEUE);
		val  |= temp;
        temp  = entry->Shaper_compensation;
		temp &= (MEA_BM_SHAPER_COMP_MASK_PRIQUEUE >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PRIQUEUE)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_PRIQUEUE);
		val  |= temp;
	}
	MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,val,MEA_MODULE_BM);
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_PriQueue_UpdateHwShaperCmd>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_PriQueue_UpdateHwShaperCmd (ENET_Unit_t                  unit_i,
                                               ENET_QueueId_t                  queue_Id,
                                               ENET_Uint32                     pri,
                                               ENET_Queue_priority_queue_dbt   *entry_i,
                                               ENET_Queue_priority_queue_dbt   *old_entry_i) 
{  

	MEA_ind_write_t ind_write;

    MEA_db_HwUnit_t hwUnit;
    MEA_Uint32 index;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_ERROR;)
    
    if (Init_QUEUE == MEA_FALSE) {
        return MEA_OK;
    }

    if( (old_entry_i) &&
        (entry_i->shaper_Id           == old_entry_i->shaper_Id          ) && 
        (entry_i->Shaper_compensation == old_entry_i->Shaper_compensation) &&
        (entry_i->shaperPri_enable       == old_entry_i->shaperPri_enable     ) &&
        (MEA_OS_memcmp(&(entry_i->shaper_info),
                       &(old_entry_i->shaper_info),
                       sizeof(entry_i->shaper_info)) == 0              )  ) {
        return MEA_OK;
    } 
    
    index = queue_Id * mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support() + pri;
#ifdef MEA_ENV_256PORT
    MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, (index / 1024), MEA_MODULE_BM);
#endif
    /* build the ind_write value */
	ind_write.tableType		= ENET_BM_TBL_SHAPER_BUCKET_PRIQUEUE;
#ifdef MEA_ENV_256PORT	
    ind_write.tableOffset = (index < 1024) ? (index) : (index-1024);    //queue_Id * mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support() + pri ;   // ((queueId) * maxpri +pri)
#else
    ind_write.tableOffset = index;
#endif
	ind_write.cmdReg		= ENET_BM_IA_CMD;      
	ind_write.cmdMask		= ENET_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)entry_i;
    //ind_write.funcParam2 = (MEA_funcParam)entry_i->shaper_Id;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_PriQueue_UpdateHwEntryShaperCmd;
#if 0
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "%s - MEA_API_WriteIndirect tbl=%d offset=%d \n",
                          __FUNCTION__,ind_write.tableType,ind_write.tableOffset);
#endif

 
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_BM);
		return MEA_ERROR;
    }

    /* return to caller */   
    return MEA_OK;
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Queue_UpdateHwEntryShaperEtherType>                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_Queue_UpdateHwEntryShaperCmd(enet_Queue_dbt *entry/*, MEA_Uint32 shaperId*/)
{
	MEA_Uint32 val,temp;
    
    //MEA_Uint32  shaper_Id =shaperId; 
    
    /* Build register 0 */
    val   = 0;
	if (entry->dbt_public.shaper_enable == MEA_TRUE){
		temp  = ENET_SHAPER_BYPASS_TYPE_ENABLE;
		temp &= (MEA_BM_SHAPER_BYPASS_MASK_CLUSTER >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_CLUSTER)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_CLUSTER);
		val  |= temp;

		temp  = entry->dbt_public.shaper_info.CBS;    // the initial values for the port CIR credit is the profile max ( = profile CBS)
		temp &= (MEA_BM_SHAPER_CIR_BACKET_MASK_CLUSTER >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_CIR_BACKET_MASK_CLUSTER)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_CIR_BACKET_MASK_CLUSTER);
		val  |= temp;
	        
        temp = entry->dbt_public.shaper_Id;
		temp &= (MEA_BM_SHAPER_PROF_NUMBER_MASK_CLUSTER >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_PROF_NUMBER_MASK_CLUSTER)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_PROF_NUMBER_MASK_CLUSTER);
		val  |= temp;

		temp  = entry->dbt_public.Shaper_compensation;
		temp &= (MEA_BM_SHAPER_COMP_MASK_CLUSTER >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_CLUSTER)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_CLUSTER);
		val  |= temp;
	}else{
		temp  = ENET_SHAPER_BYPASS_TYPE_DISABLE;
		temp &= (MEA_BM_SHAPER_BYPASS_MASK_CLUSTER >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_CLUSTER)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_BYPASS_MASK_CLUSTER);
		val  |= temp;
        temp  = entry->dbt_public.Shaper_compensation;
		temp &= (MEA_BM_SHAPER_COMP_MASK_CLUSTER >> MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_CLUSTER)); 
		temp <<= MEA_OS_calc_shift_from_mask(MEA_BM_SHAPER_COMP_MASK_CLUSTER);
		val  |= temp;
	}
	MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,val,MEA_MODULE_BM);
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Queue_UpdateHwShaperCmd>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Queue_UpdateHwShaperCmd (ENET_Unit_t       unit_i,
                                               ENET_QueueId_t    queue_Id,
                                               enet_Queue_dbt   *entry_i,
                                               enet_Queue_dbt   *old_entry_i) 
{  

	MEA_ind_write_t ind_write;
    MEA_db_HwUnit_t hwUnit;
    MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return 0;)
    


    if( (old_entry_i) &&
        (entry_i->dbt_public.shaper_Id           == old_entry_i->dbt_public.shaper_Id          ) && 
        (entry_i->dbt_public.Shaper_compensation == old_entry_i->dbt_public.Shaper_compensation) &&
        (entry_i->dbt_public.shaper_enable       == old_entry_i->dbt_public.shaper_enable      ) &&
        (MEA_OS_memcmp(&(entry_i->dbt_public.shaper_info),
                       &(old_entry_i->dbt_public.shaper_info),
                       sizeof(entry_i->dbt_public.shaper_info)) == 0              )  ) {
        return MEA_OK;
    } 
    /* build the ind_write value */
#ifdef MEA_ENV_256PORT
    MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, (queue_Id / 128), MEA_MODULE_BM);
#endif

	ind_write.tableType		= ENET_BM_TBL_SHAPER_BUCKET_CLUSTER;
#ifdef MEA_ENV_256PORT	
    ind_write.tableOffset = (queue_Id < 128) ? (queue_Id) : (queue_Id-128);
#else
	ind_write.tableOffset	= queue_Id;
#endif
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)entry_i;
    //ind_write.funcParam2 = (MEA_funcParam)entry_i->dbt_public.shaper_Id;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_Queue_UpdateHwEntryShaperCmd;
	
    /* Write to the Indirect Table */
    if (Init_QUEUE)
	if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_BM);
		return MEA_ERROR;
    }

    /* return to caller */   
    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*            <enet_Cluster2Port_UpdateHwEntry>                              */
/*---------------------------------------------------------------------------*/
static ENET_Status  enet_Cluster2Port_UpdateHwEntry(MEA_funcParam arg1)
{
    enet_HwMirror_ClusterToPort_dbt* entry = (enet_HwMirror_ClusterToPort_dbt*)arg1;
	ENET_Uint32	              val;
 
    val = 0;
 
    val  |= (((entry->PortNumber)<< MEA_OS_calc_shift_from_mask(MEA_CLUSTER_2_PORT_PORT_MASK)
             ) & MEA_CLUSTER_2_PORT_PORT_MASK
            ); 

    val |= (((entry->ClusterLocation)<< MEA_OS_calc_shift_from_mask(MEA_CLUSTER_2_PORT_LOCATION_MASK)
             ) & MEA_CLUSTER_2_PORT_LOCATION_MASK
            ); 

    val |= (((entry->queueId)<< MEA_OS_calc_shift_from_mask(MEA_CLUSTER_2_PORT_CLUSTER_NUMBER_MASK)
             ) & MEA_CLUSTER_2_PORT_CLUSTER_NUMBER_MASK
            ); 

 	ENET_WriteReg(ENET_UNIT_0,ENET_BM_IA_WRDATA0,val,ENET_MODULE_BM);

    return ENET_OK;
}

/*---------------------------------------------------------------------------*/
/*            <enet_Cluster2Port_UpdateHw>                                         */
/*---------------------------------------------------------------------------*/
static ENET_Status enet_Cluster2Port_UpdateHw(ENET_Uint32 index_i,
                                              ENET_Bool clusterMode_i) 
{

	ENET_ind_write_t ind_write;
    ENET_Uint32 index,index_start,index_end;
    enet_HwMirror_ClusterToPort_dbt entry;
    ENET_Uint32 MovToindex  ;

    /* Check parameter */
    if (index_i == MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort)) {
        index_start=0;
        index_end = index_i-1;
    } else {
		index_start=index_i;
        index_end = index_start;
    }

  	/* build the ind_write value , except tableOffset */
    MEA_OS_memset(&ind_write,0,sizeof(ind_write));
            ind_write.tableType      = ENET_BM_TBL_TYP_CLUSTER_TO_PORT; /* 19 */
			ind_write.cmdReg		= ENET_BM_IA_CMD;      
			ind_write.cmdMask		= ENET_BM_IA_CMD_MASK;      
			ind_write.statusReg		= ENET_BM_IA_STAT;   
			ind_write.statusMask	= ENET_BM_IA_STAT_MASK;   
			ind_write.tableAddrReg	= ENET_BM_IA_ADDR;
			ind_write.tableAddrMask	= ENET_BM_IA_ADDR_OFFSET_MASK;
            ind_write.funcParam1 = (MEA_funcParam)&entry;
			ind_write.writeEntry    = (ENET_FUNCPTR)enet_Cluster2Port_UpdateHwEntry;


    /* Loop on all requested indexes */
    for (index=index_start;index<=index_end;index++) {
        if (index == 512 || index == 128){
            //DBG
			//index = index;
        }
        /* Build the entry */
        MEA_OS_memcpy(&entry,
                      &(enet_HwMirror_ClusterToPort[index]),
                      sizeof(entry));
        if (clusterMode_i) {
            MovToindex = index;
            if (enet_Hw_Cluster2Port[index].PortNumber == 128){
                // DBG
				//enet_Hw_Cluster2Port[index].PortNumber = enet_Hw_Cluster2Port[index].PortNumber;
            }

            entry.PortNumber      = enet_Hw_Cluster2Port[index].PortNumber;
            entry.ClusterLocation = enet_Hw_Cluster2Port[index].ClusterLocation;
#ifdef MEA_ENV_256PORT
            if (index >= 128 && index <= 255){
                MovToindex = 512 + index % 128;
                entry.queueId = (enet_Hw_portReverseMappingData[MovToindex] < 128) ? (enet_Hw_portReverseMappingData[MovToindex]) : (enet_Hw_portReverseMappingData[MovToindex] % 128);
            }
#endif			

        } else {
            if (index == 512){
                // DBG
				//index = index;
            }
#ifdef MEA_ENV_256PORT			
            entry.queueId = (enet_Hw_portReverseMappingData[index] < 128) ? (enet_Hw_portReverseMappingData[index]) : ((enet_Hw_portReverseMappingData[index] != UNASSIGNED_QUEUE_VAL) ? (enet_Hw_portReverseMappingData[index] % 128) : enet_Hw_portReverseMappingData[index]);
#else
            entry.queueId         = enet_Hw_portReverseMappingData[index];
#endif            
			MovToindex = index;
        }



        /* Check for no change */
        if ((enet_Queue_InitDone[ENET_UNIT_0]) &&
            (MEA_OS_memcmp(&entry,
            &(enet_HwMirror_ClusterToPort[MovToindex]),
                           sizeof(entry)) == 0)) {
                continue;
        }

		/* Write to HW */
#ifdef MEA_ENV_256PORT		
        MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, (MovToindex / 512), MEA_MODULE_BM);
        


        ind_write.tableOffset = (MovToindex <512) ? (MovToindex) : (MovToindex % 512);
#else
        ind_write.tableOffset = MovToindex;

#endif	
        if (Init_QUEUE)
			if (ENET_WriteIndirect(ENET_UNIT_0,&ind_write,ENET_MODULE_BM)!=ENET_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - ENET_API_WriteIndirect tbl=%d mdl=%d failed (index=%d)\n",
							  __FUNCTION__,ind_write.tableType,ENET_MODULE_BM,index);
				return ENET_ERROR;
			}

#ifdef MEA_ENV_256PORT
            if (clusterMode_i) {
                if (index >= 128 && index <= 255){
                    
                    entry.queueId = enet_Hw_portReverseMappingData[MovToindex];
                }
            }
            else {
                entry.queueId = enet_Hw_portReverseMappingData[MovToindex];
            }
#endif

        /* Update HW mirror */
            MEA_OS_memcpy(&(enet_HwMirror_ClusterToPort[MovToindex]),
                      &entry,
                      sizeof(enet_HwMirror_ClusterToPort[0]));


	    
    
    
		}


	return ENET_OK;   	 
}

/*---------------------------------------------------------------------------*/
/*            <enet_ClusterPortInfo_UpdateHwEntry>                           */
/*---------------------------------------------------------------------------*/
static ENET_Status  enet_ClusterPortInfo_UpdateHwEntry(MEA_funcParam arg1) 
{
    enet_HwMirror_ClusterPortInformation_dbt* entry = (enet_HwMirror_ClusterPortInformation_dbt*)arg1;
	ENET_Uint32	              val;
 
    val=0;

    val  |= (((entry->location)<< MEA_OS_calc_shift_from_mask(MEA_CLUSTER_PORT_INFO_LOCATION_MASK)
             ) & MEA_CLUSTER_PORT_INFO_LOCATION_MASK
            ); 

    val  |= (((entry->strict_level) << MEA_OS_calc_shift_from_mask(MEA_CLUSTER_PORT_INFO_STRICT_LEVEL)
             ) & MEA_CLUSTER_PORT_INFO_STRICT_LEVEL
            );
      
    
 	ENET_WriteReg(ENET_UNIT_0,ENET_BM_IA_WRDATA0,val,ENET_MODULE_BM);
    return ENET_OK;
}

/*---------------------------------------------------------------------------*/
/*            <enet_ClusterPortInfo_UpdateHw>                                    */
/*---------------------------------------------------------------------------*/
static ENET_Status enet_ClusterPortInfo_UpdateHw(ENET_PortId_t portId_i) 
{
	ENET_ind_write_t ind_write;
	ENET_PortId_t    portId,portId_start,portId_end; 
    enet_HwMirror_ClusterPortInformation_dbt entry;

    /* Check parameter */
    if (portId_i == MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterPortInformation)) {
        portId_start=0;
        portId_end = portId_i-1;
    } else {
		portId_start=portId_i;
        portId_end = portId_start;
    }

  	/* build the ind_write value , except tableOffset */
    MEA_OS_memset(&ind_write,0,sizeof(ind_write));
    ind_write.tableType = ENET_BM_TBL_TYP_CLUSTER_PORT_INFORMATION; /*20*/
			ind_write.cmdReg		= ENET_BM_IA_CMD;      
			ind_write.cmdMask		= ENET_BM_IA_CMD_MASK;      
			ind_write.statusReg		= ENET_BM_IA_STAT;   
			ind_write.statusMask	= ENET_BM_IA_STAT_MASK;   
			ind_write.tableAddrReg	= ENET_BM_IA_ADDR;
			ind_write.tableAddrMask	= ENET_BM_IA_ADDR_OFFSET_MASK;
            ind_write.funcParam1 = (MEA_funcParam)&entry;
			ind_write.writeEntry    = (ENET_FUNCPTR)enet_ClusterPortInfo_UpdateHwEntry;		


    /* Loop on all requested ports */
    for (portId=portId_start;portId<=portId_end;portId++) {

        /* Build the entry */
        MEA_OS_memset(&entry,0,sizeof(entry));
        entry.location     = enet_Hw_portReverseMappingIndx[portId].location;
        entry.strict_level = enet_Hw_portReverseMappingIndx[portId].numOf_strict;
        if (entry.strict_level > MEA_MAX_OF_CLUSTER_STRICT_LEVEL-1) {
            entry.strict_level = (MEA_Uint16)MEA_MAX_OF_CLUSTER_STRICT_LEVEL-1;
        }

        /* Check for no change */
        if ((enet_Queue_InitDone[ENET_UNIT_0]) &&
            (MEA_OS_memcmp(&entry,
                           &(enet_HwMirror_ClusterPortInformation[portId]),
                           sizeof(entry)) == 0)) {
                continue;
        }
#ifdef MEA_ENV_256PORT		
        MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, (portId / 128), MEA_MODULE_BM);
#endif
		/* Write to HW */
    	ind_write.tableOffset	= (MEA_Uint32)portId;
        if (Init_QUEUE)
			if (ENET_WriteIndirect(ENET_UNIT_0,&ind_write,ENET_MODULE_BM)!=ENET_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - ENET_API_WriteIndirect tbl=%d mdl=%d failed (index=%d)\n",
                              __FUNCTION__,ind_write.tableType,ENET_MODULE_BM,ind_write.tableOffset);
				return ENET_ERROR;
			}

        /* Update HW mirror */
        MEA_OS_memcpy(&(enet_HwMirror_ClusterPortInformation[portId]),
                      &entry,
                      sizeof(enet_HwMirror_ClusterPortInformation[0]));


		}

	return ENET_OK;   	 
}


/*----------------------------------------------------------------------------*/
/*               <enet_removeClusterFromPort>                                 */
/*               <enet_addClusterToPort>                                      */
/*               <enet_UpdateDevice_Queue>                                    */
/*               <enet_DeleteDevice_Queue>                                    */
/*  These function are called by the API function,                            */
/*----------------------------------------------------------------------------*/

ENET_Status enet_removeClusterFromPort (enet_Queue_dbt *entry_pi,
                                        ENET_QueueId_t queueId_i)
{
	
	ENET_QueueId_t nextCluster = UNASSIGNED_QUEUE_VAL;
    ENET_PortId_t            portId_i    = entry_pi->dbt_public.port.id.port;
    ENET_Queue_mode_type_te  mode_type_i = entry_pi->dbt_public.mode.type;
    ENET_Uint16              indx        = enet_Hw_portReverseMappingIndx[portId_i].location; 
    ENET_PortId_t            port;
	ENET_Uint16 NumOfClusterInPort;
#ifdef MEA_ENV_256PORT
    MEA_Uint32 index_location;
#endif    
	//DEBUG_QUEUE("QueueLog: START - Remove Cluster %d from port %d\n",QueueId,portId); 

	if(enet_Hw_Cluster2Port[queueId_i].ClusterLocation == 0){
			SetDefaultClusterToPort(portId_i,UNASSIGNED_QUEUE_VAL);
	}

 	indx+=enet_Hw_Cluster2Port[queueId_i].ClusterLocation;
	//DEBUG_QUEUE("QueueLog: * Start shifting cluster in the port to lower location\n",nextCluster,indx,indx+1);/**/
	NumOfClusterInPort = enet_Hw_Cluster2Port[queueId_i].ClusterLocation;
	while(nextCluster!=queueId_i)
	{
		if (indx>=REVERSE_MAPPING_TABLE_SIZE-1)
			break; /* we get to the end of the table */
		nextCluster = enet_Hw_portReverseMappingData[indx+1];
		if (nextCluster == UNASSIGNED_QUEUE_VAL)
			break; 
		if (enet_Hw_Cluster2Port[nextCluster].PortNumber != portId_i)
			break; /* we get to the end of this port in the table */
		//DEBUG_QUEUE("QueueLog: ** push cluster %d from index %d to index %d\n",nextCluster,indx+1,indx);/**/
		NumOfClusterInPort++;
		MEA_OS_memcpy(&enet_Hw_portReverseMappingData[indx],
                      &enet_Hw_portReverseMappingData[indx+1],
                      sizeof(enet_Hw_portReverseMappingData[0]));
        

		enet_Hw_Cluster2Port[nextCluster].ClusterLocation--;
		if(enet_Hw_Cluster2Port[nextCluster].ClusterLocation == 0){
			SetDefaultClusterToPort(portId_i,nextCluster);
	    }

        enet_Cluster2Port_UpdateHw(indx,MEA_FALSE);
        enet_Cluster2Port_UpdateHw(nextCluster,MEA_TRUE);
		indx++;
	}

    /* Delete the space was free at end of the cluster list for this port */
	MEA_OS_memset(&enet_Hw_portReverseMappingData[indx],
                  0,
                  sizeof(enet_Hw_portReverseMappingData[0]));
	enet_Hw_portReverseMappingData[indx] = UNASSIGNED_QUEUE_VAL;
    enet_Cluster2Port_UpdateHw(indx,MEA_FALSE);

    /* Check if we need to update the enet_Hw_portReverseMappingIndx[portId] */
	if (NumOfClusterInPort==0)
	{
		//DEBUG_QUEUE("QueueLog: * No Cluster are left for port %d\n",portId);/**/
        MEA_OS_memset(&enet_Hw_portReverseMappingIndx[portId_i],
                      0,
                      sizeof(enet_Hw_portReverseMappingIndx[0]));
        enet_Hw_portReverseMappingIndx[portId_i].location=UNASSIGNED_QUEUE_VAL;
        enet_ClusterPortInfo_UpdateHw(portId_i);
    } else {
        /*update num numOf_strict*/
        if (mode_type_i == ENET_QUEUE_MODE_TYPE_STRICT) {
            if (enet_Hw_portReverseMappingIndx[portId_i].numOf_strict == 0) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
                                  "%s - remove strict cluster %d for port %d , but num of strict clusters is zero \n",
                                  __FUNCTION__,
                                  queueId_i,portId_i);
            } else {
                enet_Hw_portReverseMappingIndx[portId_i].numOf_strict--;   
                enet_ClusterPortInfo_UpdateHw(portId_i);
            }
        }
	}



	//DEBUG_QUEUE("QueueLog: * Start pushing cluster to lower index\n",nextCluster,indx,indx+1);/**/
	/* push all cluster in the location table to 0 , indx point to the free location*/
	while (indx<REVERSE_MAPPING_TABLE_SIZE-1)
	{
		nextCluster = enet_Hw_portReverseMappingData[indx+1];
		if (nextCluster == UNASSIGNED_QUEUE_VAL)
		{
			MEA_OS_memset(&enet_Hw_portReverseMappingData[indx],0,sizeof(enet_Hw_portReverseMappingData[0]));
			enet_Hw_portReverseMappingData[indx] = UNASSIGNED_QUEUE_VAL;
			break; 
		}
		port = enet_Hw_Cluster2Port[nextCluster].PortNumber;
		NumOfClusterInPort = 1;
		/* fine number of cluster in port */
		while (NumOfClusterInPort<MAX_QUE_PER_PORT)
		{
			nextCluster = enet_Hw_portReverseMappingData[indx+NumOfClusterInPort+1];
			if(nextCluster == UNASSIGNED_QUEUE_VAL)
				break;
			if(port!=enet_Hw_Cluster2Port[nextCluster].PortNumber)
				break ; /* this is another port */
			NumOfClusterInPort++;
		}
		//DEBUG_QUEUE("QueueLog: **move port %d(with %d clusters) from index %d to index %d \n",port,NumOfClusterInPort,indx+1,indx);/**/
#if !defined(MEA_ENV_256PORT)
            MEA_OS_memcpy(&enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX],
                &enet_Hw_portReverseMappingData[indx + 1],
                NumOfClusterInPort * sizeof(enet_Hw_portReverseMappingData[0]));
            enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort), MEA_FALSE);
            enet_Hw_portReverseMappingIndx[port].location = WORK_PAD_LOCATION_INDEX;
            enet_ClusterPortInfo_UpdateHw(port);


            MEA_OS_memcpy(&enet_Hw_portReverseMappingData[indx],
                &enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX],
                NumOfClusterInPort * sizeof(enet_Hw_portReverseMappingData[0]));
            enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort), MEA_FALSE);
            enet_Hw_portReverseMappingIndx[port].location = indx;
            enet_ClusterPortInfo_UpdateHw(port);
            indx += NumOfClusterInPort;
#else
        if (queueId_i < 128){
            index_location = WORK_PAD_LOCATION_INDEX;
        }
        else{
            index_location = WORK_PAD_LOCATION_INDEX_LQ1;
        }

        MEA_OS_memcpy(&enet_Hw_portReverseMappingData[index_location],
            &enet_Hw_portReverseMappingData[indx + 1],
            NumOfClusterInPort * sizeof(enet_Hw_portReverseMappingData[0]));
        enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort), MEA_FALSE);
        enet_Hw_portReverseMappingIndx[port].location = (ENET_Uint16)index_location;
        enet_ClusterPortInfo_UpdateHw(port);


        MEA_OS_memcpy(&enet_Hw_portReverseMappingData[indx],
            &enet_Hw_portReverseMappingData[index_location],
            NumOfClusterInPort * sizeof(enet_Hw_portReverseMappingData[0]));
        enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort), MEA_FALSE);
        enet_Hw_portReverseMappingIndx[port].location = indx;
        enet_ClusterPortInfo_UpdateHw(port);
        indx += NumOfClusterInPort;





#endif
        

	}

#if !defined(MEA_ENV_256PORT)
        for (indx = WORK_PAD_LOCATION_INDEX;
            indx < WORK_PAD_LOCATION_INDEX + MAX_QUE_PER_PORT;
            indx++){
            MEA_OS_memset(&enet_Hw_portReverseMappingData[indx], 0, sizeof(enet_Hw_portReverseMappingData[0]));
            enet_Hw_portReverseMappingData[indx] = UNASSIGNED_QUEUE_VAL;
            enet_Cluster2Port_UpdateHw(indx, MEA_FALSE);
        }
    
#else
    if (queueId_i < 128){
        for (indx = WORK_PAD_LOCATION_INDEX;
            indx < WORK_PAD_LOCATION_INDEX + MAX_QUE_PER_PORT;
            indx++){
            MEA_OS_memset(&enet_Hw_portReverseMappingData[indx], 0, sizeof(enet_Hw_portReverseMappingData[0]));
            enet_Hw_portReverseMappingData[indx] = UNASSIGNED_QUEUE_VAL;
            enet_Cluster2Port_UpdateHw(indx, MEA_FALSE);
        }
    }
    else{
        for (indx = (ENET_Uint16)WORK_PAD_LOCATION_INDEX_LQ1;
            indx < (ENET_Uint16)(WORK_PAD_LOCATION_INDEX_LQ1 + MAX_QUE_PER_PORT);
            indx++){
            MEA_OS_memset(&enet_Hw_portReverseMappingData[indx], 0, sizeof(enet_Hw_portReverseMappingData[0]));
            enet_Hw_portReverseMappingData[indx] = UNASSIGNED_QUEUE_VAL;
            enet_Cluster2Port_UpdateHw(indx, MEA_FALSE);
        }

    }
#endif
    enet_Hw_Cluster2Port[queueId_i].PortNumber = UNASSIGNED_QUEUE_VAL;
	enet_Cluster2Port_UpdateHw(queueId_i,MEA_TRUE);


	//DEBUG_QUEUE("QueueLog: END - Remove Cluster %d from port %d\n",QueueId,portId);/**/
	return ENET_OK;
}


ENET_Status enet_addClusterToPort (enet_Queue_dbt *new_entry_pi,
                                   ENET_QueueId_t  queueId_i)
{
	ENET_PortId_t           portId_i    = new_entry_pi->dbt_public.port.id.port;
	ENET_QueueId_t nextCluster ;
	ENET_PortId_t  port ;
	ENET_Uint16 location;
	ENET_Uint16 index;
	ENET_Uint16 lastIndex,startIndex;
    ENET_Uint32 i;
	ENET_Uint16 NumOfClusterInPort ;
	ENET_Uint16 old_index ;
	ENET_Uint16 groupOfQluster = 0;
    ENET_Uint16 LastIndexId=0;
    
    
    //DEBUG_QUEUE("QueueLog: START - add Cluster %d to port %d\n", queueId_i, portId_i);/**/
#ifdef MEA_ENV_256PORT   
    if (portId_i < 128){
        groupOfQluster = 0;
        /* insert space in location array */
        // index = MEA_NUM_OF_ELEMENTS(enet_Hw_portReverseMappingData) - 1;
        
        LastIndexId = 0;
        index = 511; //MEA_NUM_OF_ELEMENTS(enet_Hw_portReverseMappingData) - 1;
    }else{
        groupOfQluster = 1;
        LastIndexId = 512;
        index = 1023;
    }
#else
	/* insert space in location array */
	index = MEA_NUM_OF_ELEMENTS(enet_Hw_portReverseMappingData)-1;
#endif
    while ((enet_Hw_portReverseMappingData[index] == UNASSIGNED_QUEUE_VAL) &&
        (index > LastIndexId)) {
		index--; /* indx = upper (last) cluster in port*/
    }

    nextCluster = enet_Hw_portReverseMappingData[index];
    if (enet_Hw_portReverseMappingIndx[portId_i].location!=UNASSIGNED_QUEUE_VAL)
	{
		//DEBUG_QUEUE("QueueLog: * push port downward to make room for new cluster\n");/**/
		while (enet_Hw_Cluster2Port[nextCluster].PortNumber !=portId_i)
		{
			port = enet_Hw_Cluster2Port[nextCluster].PortNumber;
			NumOfClusterInPort = index- enet_Hw_portReverseMappingIndx[port].location +1;
            old_index = enet_Hw_portReverseMappingIndx[port].location;

            if (groupOfQluster == 0){
                //DEBUG_QUEUE("QueueLog: ** push port %d (with %d clusters)from %d to %d\n",port,NumOfClusterInPort,old_index,old_index+1);/**/
                MEA_OS_memcpy(&enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX],
                    &enet_Hw_portReverseMappingData[old_index],
                    NumOfClusterInPort * sizeof(enet_Hw_portReverseMappingData[0]));
            

            enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort),MEA_FALSE);   
			enet_Hw_portReverseMappingIndx[port].location=WORK_PAD_LOCATION_INDEX;
			enet_ClusterPortInfo_UpdateHw(port);

			MEA_OS_memcpy (&enet_Hw_portReverseMappingData[old_index+1],
					&enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX],
					NumOfClusterInPort * sizeof(enet_Hw_portReverseMappingData[0]));
			enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort),MEA_FALSE);
			enet_Hw_portReverseMappingIndx[port].location=old_index+1;
			enet_ClusterPortInfo_UpdateHw(port);

			index -= NumOfClusterInPort;
            nextCluster = enet_Hw_portReverseMappingData[index];
            }
#ifdef MEA_ENV_256PORT			
            else{
               
                    MEA_OS_memcpy(&enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX_LQ1],
                    &enet_Hw_portReverseMappingData[old_index],
                    NumOfClusterInPort * sizeof(enet_Hw_portReverseMappingData[0]));

                enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort), MEA_FALSE);
                enet_Hw_portReverseMappingIndx[port].location = (ENET_Uint16)WORK_PAD_LOCATION_INDEX_LQ1;
                enet_ClusterPortInfo_UpdateHw(port);

                MEA_OS_memcpy(&enet_Hw_portReverseMappingData[old_index + 1],
                    &enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX_LQ1],
                    NumOfClusterInPort * sizeof(enet_Hw_portReverseMappingData[0]));
                enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort), MEA_FALSE);
                enet_Hw_portReverseMappingIndx[port].location = old_index + 1;
                enet_ClusterPortInfo_UpdateHw(port);

                index -= NumOfClusterInPort;
                nextCluster = enet_Hw_portReverseMappingData[index];



            }
#endif			
		}
		index=index+1;
	}
	else
	{
        if (nextCluster!=UNASSIGNED_QUEUE_VAL) {
			index = index+1;
        }
        MEA_OS_memset(&(enet_Hw_portReverseMappingIndx[portId_i]),
                      0,
                      sizeof(enet_Hw_portReverseMappingIndx[0]));
        enet_Hw_portReverseMappingIndx[portId_i].location = index;
        enet_Hw_portReverseMappingIndx[portId_i].numOf_strict = 0;

	}
	

	/* Set startIndex and lastIndex for this port , 
       when the lastIndex is the free entry to add into it */
    lastIndex=index;
    startIndex=enet_Hw_portReverseMappingIndx[portId_i].location;

    /* If we are strict cluster we need to check if we want to add the new 
       cluster in the middle , and not in the lastIndex , according to the 
       strict priority value */
    if(new_entry_pi->dbt_public.mode.type == ENET_QUEUE_MODE_TYPE_STRICT){
       /*check where we need to add*/  
        for (index=startIndex;index<=lastIndex-1;index++) {
            ENET_QueueId_t current_cluster;
            current_cluster=enet_Hw_portReverseMappingData[index];
            if(enet_Hw_Cluster2Port[current_cluster].mode.type != ENET_QUEUE_MODE_TYPE_STRICT){
                break;
            }
            if(enet_Hw_Cluster2Port[current_cluster].mode.value.strict_priority < 
               new_entry_pi->dbt_public.mode.value.strict_priority) {
               break;
            } 
        }//end while

        if (index < lastIndex) {
            /* Copy all current clusters of this port to temporary area and 
                change the port pointer to allowed Hardware work while we are update */
		    //DEBUG_QUEUE("QueueLog: ** push port %d (with %d clusters)from %d to %d\n",port,NumOfClusterInPort,old_index,old_index+1);/**/
            if (groupOfQluster == 0){
                MEA_OS_memcpy(&enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX],
                    &enet_Hw_portReverseMappingData[startIndex],
                    ((lastIndex - 1) - startIndex + 1) * sizeof(enet_Hw_portReverseMappingData[0]));
                enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort), MEA_FALSE);
                enet_Hw_portReverseMappingIndx[portId_i].location = WORK_PAD_LOCATION_INDEX;
                enet_ClusterPortInfo_UpdateHw(portId_i);

                /* copy from temp area to index+1..lastindex , and free the index to the new area */
                MEA_OS_memcpy(&enet_Hw_portReverseMappingData[index + 1],
                    &enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX + (index - startIndex)],
                    (lastIndex - (index + 1) + 1) * sizeof(enet_Hw_portReverseMappingData[0]));
            }
#ifdef MEA_ENV_256PORT			
            else{
                MEA_OS_memcpy(&enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX_LQ1],
                    &enet_Hw_portReverseMappingData[startIndex],
                    ((lastIndex - 1) - startIndex + 1) * sizeof(enet_Hw_portReverseMappingData[0]));
                enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort), MEA_FALSE);
                enet_Hw_portReverseMappingIndx[portId_i].location = (ENET_Uint16)WORK_PAD_LOCATION_INDEX_LQ1;
                enet_ClusterPortInfo_UpdateHw(portId_i);

                /* copy from temp area to index+1..lastindex , and free the index to the new area */
                MEA_OS_memcpy(&enet_Hw_portReverseMappingData[index + 1],
                    &enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX_LQ1 + (index - startIndex)],
                    (lastIndex - (index + 1) + 1) * sizeof(enet_Hw_portReverseMappingData[0]));
            }
#endif			

            for(i=index+1; i<= lastIndex;i++ ){
                enet_Cluster2Port_UpdateHw(i,MEA_FALSE);
                enet_Hw_Cluster2Port[enet_Hw_portReverseMappingData[i]].ClusterLocation++;
		        enet_Cluster2Port_UpdateHw(enet_Hw_portReverseMappingData[i],MEA_TRUE );
            }
        }

    }

    /* Now index is the free entry that is free to fill it */
    //DEBUG_QUEUE("QueueLog: * put new cluster %d at index %d\n", queueId_i, index);/**/
    enet_Hw_portReverseMappingData[index]           = queueId_i;
    MEA_OS_memset(&(enet_Hw_Cluster2Port[queueId_i]),
                  0,
                  sizeof(enet_Hw_Cluster2Port[0]));
    enet_Hw_Cluster2Port[queueId_i].PortNumber      = portId_i;
    enet_Hw_Cluster2Port[queueId_i].ClusterLocation = index - startIndex;
    MEA_OS_memcpy(&(enet_Hw_Cluster2Port[queueId_i].mode),
                  &(new_entry_pi->dbt_public.mode),
                  sizeof(enet_Hw_Cluster2Port[0].mode));
    
        
    if(enet_Hw_Cluster2Port[queueId_i].ClusterLocation == 0){
        SetDefaultClusterToPort(portId_i,queueId_i);
	}
    enet_Cluster2Port_UpdateHw(index,MEA_FALSE);
    enet_Cluster2Port_UpdateHw(queueId_i,MEA_TRUE);
    
   

    /* Update num of strict*/    
    if(new_entry_pi->dbt_public.mode.type == ENET_QUEUE_MODE_TYPE_STRICT) {
       enet_Hw_portReverseMappingIndx[portId_i].numOf_strict++;
    }
        
    /* Update Hardware PortInfo */
    enet_Hw_portReverseMappingIndx[portId_i].location=startIndex;
	enet_ClusterPortInfo_UpdateHw(portId_i); 
        

	//DEBUG_QUEUE("QueueLog: END - add Cluster %d to port %d\n",QueueId,portId);/**/
    if (groupOfQluster == 0){
        for (location = WORK_PAD_LOCATION_INDEX;
            location < WORK_PAD_LOCATION_INDEX + MAX_QUE_PER_PORT;
            location++){
            enet_Hw_portReverseMappingData[location] = UNASSIGNED_QUEUE_VAL;
            enet_Cluster2Port_UpdateHw(location, MEA_FALSE);
        }
    }
#ifdef MEA_ENV_256PORT	
    else{
        for (location = (ENET_Uint16)WORK_PAD_LOCATION_INDEX_LQ1;
            location < (ENET_Uint16)(WORK_PAD_LOCATION_INDEX_LQ1 + MAX_QUE_PER_PORT);
            location++){
            enet_Hw_portReverseMappingData[location] = UNASSIGNED_QUEUE_VAL;
            enet_Cluster2Port_UpdateHw(location, MEA_FALSE);
        }
	
    }
#endif	

	return ENET_OK;
}

ENET_Status enet_updaterClusterInPort (enet_Queue_dbt *new_entry_pi,
                                       ENET_QueueId_t  queueId_i)
{
    
    ENET_PortId_t       port;
    ENET_Queue_mode_dbt old_mode;
    ENET_Uint16         old_offset;
    ENET_Uint16         new_offset;
    ENET_Uint16         offset;
    ENET_Uint16         index;
    ENET_Uint16         startIndex;
    ENET_Uint16         endIndex;
    ENET_QueueId_t      cluster;
    ENET_Uint16         numOfClustersInPort;
#ifdef MEA_ENV_256PORT
    MEA_Uint32 working_Pad;
#endif    

    /* Calculate the next parameters :
       - port                : The current port we are working on it 
       - old_offset          : old offset of the queue in this port 
       - old_mode            : old mode   of the queue in this port
       - startIndex          : Index of the first cluster in this port 
       - endIndex            : Index of the last  cluster in this port
       - NumOfClustersInPort : Num of clusters in this port 
       */
    port       = enet_Hw_Cluster2Port[queueId_i].PortNumber;
    old_offset = enet_Hw_Cluster2Port[queueId_i].ClusterLocation;
    MEA_OS_memcpy(&old_mode,&(enet_Hw_Cluster2Port[queueId_i].mode),sizeof(old_mode));
    startIndex = enet_Hw_portReverseMappingIndx[port].location;
    for(index=startIndex+1;index<MEA_NUM_OF_ELEMENTS(enet_Hw_portReverseMappingData);index++) {
        cluster = enet_Hw_portReverseMappingData[index];
        if (cluster == UNASSIGNED_QUEUE_VAL) {
            break;
        }
        if (enet_Hw_Cluster2Port[cluster].PortNumber != port) {
            break;
        }
    }
    endIndex=index-1;
    numOfClustersInPort = endIndex - startIndex + 1;


    /* Copy the current clusters of this port to temp area and let the 
       device work with the temp area , while we edit the current clusters */
#if !defined(MEA_ENV_256PORT)
	MEA_OS_memcpy (&enet_Hw_portReverseMappingData[WORK_PAD_LOCATION_INDEX],
				   &enet_Hw_portReverseMappingData[startIndex],
				   numOfClustersInPort * sizeof(enet_Hw_portReverseMappingData[0]));
    enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort),MEA_FALSE);   
    enet_Hw_portReverseMappingIndx[port].location=WORK_PAD_LOCATION_INDEX;
    enet_ClusterPortInfo_UpdateHw(port);
#else
    if (queueId_i < 128){
        working_Pad = WORK_PAD_LOCATION_INDEX;
    }
    else{
        working_Pad = WORK_PAD_LOCATION_INDEX_LQ1;
    }
    MEA_OS_memcpy(&enet_Hw_portReverseMappingData[working_Pad],
            &enet_Hw_portReverseMappingData[startIndex],
            numOfClustersInPort * sizeof(enet_Hw_portReverseMappingData[0]));
        enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort), MEA_FALSE);
        enet_Hw_portReverseMappingIndx[port].location = (ENET_Uint16)working_Pad;
        enet_ClusterPortInfo_UpdateHw(port);
    


#endif

    /* check what should be the new offset */  
    if(new_entry_pi->dbt_public.mode.type != ENET_QUEUE_MODE_TYPE_STRICT){
        new_offset  = endIndex - startIndex;
    } else {
        for (index=startIndex;index<=endIndex;index++) {
            /* Get Cluster */
            cluster=enet_Hw_portReverseMappingData[index];

            /* Check if we end scan all strict clusters */
            if(enet_Hw_Cluster2Port[cluster].mode.type != ENET_QUEUE_MODE_TYPE_STRICT){
                if (old_mode.type == ENET_QUEUE_MODE_TYPE_STRICT) {
                    index--;
                }
                break;
            }
            
            /* Check if we are need to enter before this strict cluster */
            if(enet_Hw_Cluster2Port[cluster].mode.value.strict_priority < 
                new_entry_pi->dbt_public.mode.value.strict_priority) {
                if (index-startIndex==old_offset+1) {
                    index--;
               }
               break;
            } 
        }
 //Alex fix at 16/05/2009`
        if (index < startIndex) {
           new_offset = 0; 
        } else {
           if (index > endIndex) {
               new_offset = endIndex - startIndex;
           } else {
               new_offset = index - startIndex; 
           }
        }

    }


    /* Move the other clusters according to the new_offset */
    if (new_offset != old_offset) {
        if (new_offset > old_offset) {
            for(offset=old_offset+1; offset<= new_offset;offset++ ){
                cluster=enet_Hw_portReverseMappingData[startIndex+offset];
                enet_Hw_portReverseMappingData[startIndex+offset-1] = cluster;
                enet_Cluster2Port_UpdateHw(startIndex+offset-1,MEA_FALSE);
                enet_Hw_Cluster2Port[cluster].ClusterLocation--;
            }
        } else {
            for(offset=old_offset; offset>=new_offset+1;offset--){
                cluster=enet_Hw_portReverseMappingData[startIndex+offset-1];
                enet_Hw_portReverseMappingData[startIndex+offset] = cluster;
                enet_Cluster2Port_UpdateHw(startIndex+offset,MEA_FALSE);
                enet_Hw_Cluster2Port[cluster].ClusterLocation++;
            }
        }
    }


    /* Update the new_offset with the new_entry */
    enet_Hw_portReverseMappingData[startIndex+new_offset] = queueId_i;
    enet_Cluster2Port_UpdateHw(startIndex+new_offset,MEA_FALSE);
    enet_Hw_Cluster2Port[queueId_i].ClusterLocation = new_offset;
    MEA_OS_memcpy(&(enet_Hw_Cluster2Port[queueId_i].mode),
                  &(new_entry_pi->dbt_public.mode),
                  sizeof(enet_Hw_Cluster2Port[0].mode));
    if(enet_Hw_Cluster2Port[queueId_i].ClusterLocation == 0){
        SetDefaultClusterToPort(port,queueId_i);
	}

    /* Update num of strict*/    
    if ((new_entry_pi->dbt_public.mode.type == ENET_QUEUE_MODE_TYPE_STRICT) && 
        (old_mode.type                      == ENET_QUEUE_MODE_TYPE_WFQ   )  ) { 
       enet_Hw_portReverseMappingIndx[port].numOf_strict++;
    } else {
        if ((new_entry_pi->dbt_public.mode.type == ENET_QUEUE_MODE_TYPE_WFQ   ) && 
            (old_mode.type                      == ENET_QUEUE_MODE_TYPE_STRICT)  ) { 
           enet_Hw_portReverseMappingIndx[port].numOf_strict--;
        }
    }
        
    /* Update PortInfo Location */
    enet_Hw_portReverseMappingIndx[port].location=startIndex;

    /* Update Hardware */
    enet_Cluster2Port_UpdateHw(MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort),MEA_TRUE);   
    enet_ClusterPortInfo_UpdateHw(port); 
#if !defined(MEA_ENV_256PORT)
    /* Update the pad area */
	for (index=WORK_PAD_LOCATION_INDEX;
		 index<WORK_PAD_LOCATION_INDEX+MAX_QUE_PER_PORT;
		 index++){
		 enet_Hw_portReverseMappingData[index]=UNASSIGNED_QUEUE_VAL;
         enet_Cluster2Port_UpdateHw(index,MEA_FALSE);
	}
#else
    if (queueId_i < 128){
        working_Pad = WORK_PAD_LOCATION_INDEX;
    }
    else{
        working_Pad = WORK_PAD_LOCATION_INDEX_LQ1;
    }

    for (index = (ENET_Uint16)working_Pad;
        index < (ENET_Uint16)(working_Pad + MAX_QUE_PER_PORT);
        index++){
        enet_Hw_portReverseMappingData[index] = UNASSIGNED_QUEUE_VAL;
        enet_Cluster2Port_UpdateHw(index, MEA_FALSE);
    }



#endif
    /* Return to Caller */
	return ENET_OK;
}

static ENET_Uint32 enet_getNumOfClusterInPort(ENET_PortId_t port)
{
	ENET_Uint16 indx = REVERSE_MAPPING_TABLE_SIZE-1;
	ENET_Uint32 retVal=0;


	for (indx=0;indx<REVERSE_MAPPING_TABLE_SIZE;indx++){
		if (enet_Hw_Cluster2Port[indx].PortNumber == port)
			retVal++;
	}
	return(retVal);
}

ENET_Status enet_queue_blockPriQ(MEA_Unit_t    unit_i,
                                 ENET_PortId_t portId_i,
                                 ENET_Bool     release_i) 
{

    ENET_Bool      found;
	enet_Queue_dbt entry;
	ENET_Uint32 len;
	ENET_QueueId_t internalQid;
    

	len=sizeof(entry);
	if ( ENET_GetFirst_GroupElement   (unit_i,
	                                   enet_Queue_group_id[unit_i],
									   &internalQid,
                                       &entry,
									   &len,
                                       &found) !=ENET_OK)	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_GetFirst_GroupElement failed \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}
    while (found) {
        if ((entry.dbt_private.external_id != UNASSIGNED_QUEUE_VAL) &&
            (entry.dbt_public.port.id.port  == portId_i)) {
           
            if (!release_i) {
                ENET_Uint32 j;
                for(j=0;j<ENET_PLAT_QUEUE_NUM_OF_PRI_Q;j++){
                    entry.dbt_public.pri_queues[j].max_q_size_Byte=0;
                    entry.dbt_public.pri_queues[j].max_q_size_Packets=0;
                }
            }

            /* write to pri_q  max_q_size_Byte  0*/
            if (enet_queue_UpdateHwMQS(unit_i,internalQid,&entry,NULL)!=ENET_OK) {
		            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - enet_queue_UpdateHwMQSfalied (queue=%d) ",
							 __FUNCTION__,internalQid);
		            return ENET_ERROR;
		   }
        }

		if (  ENET_GetNext_GroupElement   (unit_i,
										   enet_Queue_group_id[unit_i],
										   &internalQid,
										   &entry,
										   &len,
										   &found)!=ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - ENET_GetNext_GroupElement ailed \n",
							   __FUNCTION__);
			return ENET_ERROR;
		}
	}

    return ENET_OK;
}
   


ENET_Status enet_UpdateDevice_Queue (ENET_Unit_t       unit_i,
                                     ENET_QueueId_t    id_i,
                                     enet_Queue_dbt   *new_entry_i,
                                     enet_Queue_dbt   *old_entry_i) 
{

    ENET_Bool     port_change;
    ENET_Uint32   PriQueue;
    ENET_Uint32   numOf_blocks_PriQue_support;
    
	if (new_entry_i == NULL)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"new_entry_i == NULL\n",
			__FUNCTION__);
		return ENET_ERROR;
	}
	/* Check if port change */
    if  (old_entry_i==NULL) {
        port_change = MEA_FALSE;
    } else {
	    if (MEA_OS_memcmp(&new_entry_i->dbt_public.port       ,
                          &old_entry_i->dbt_public.port       ,
                          sizeof(new_entry_i->dbt_public.port))!=0) {            
            port_change = MEA_TRUE;
        } else {
            port_change = MEA_FALSE;
        }
    }

    /* Check if we have old and new and no port change */
    if ((old_entry_i       ) && 
        /*(new_entry_i       ) &&*/
        (!port_change      )  ) 
	{ 
        
        /* Check if we have update to the cluster */
        if ((new_entry_i->dbt_public.mode.type != old_entry_i->dbt_public.mode.type) ||
            ((new_entry_i->dbt_public.mode.type == ENET_QUEUE_MODE_TYPE_STRICT) && 
             (old_entry_i->dbt_public.mode.type == ENET_QUEUE_MODE_TYPE_STRICT) && 
             (new_entry_i->dbt_public.mode.value.strict_priority != 
              old_entry_i->dbt_public.mode.value.strict_priority              )  )    ) {
            if(Init_QUEUE)
                enet_queue_blockPriQ(unit_i,new_entry_i->dbt_public.port.id.port,ENET_FALSE);
            enet_updaterClusterInPort (new_entry_i,id_i);
            if (Init_QUEUE)
            enet_queue_blockPriQ(unit_i,new_entry_i->dbt_public.port.id.port,ENET_TRUE);
        } else {
            /* nothing to do */
        }

    } else {

        if  (port_change && (old_entry_i != NULL)) {
            if (Init_QUEUE)
                enet_queue_blockPriQ(unit_i,old_entry_i->dbt_public.port.id.port,ENET_FALSE);
            enet_removeClusterFromPort(old_entry_i,id_i);
            if (Init_QUEUE)
                enet_queue_blockPriQ(unit_i,old_entry_i->dbt_public.port.id.port,ENET_TRUE);
        }  
        if  ((old_entry_i==NULL) || (port_change)) {
            if (Init_QUEUE)
                enet_queue_blockPriQ(unit_i,new_entry_i->dbt_public.port.id.port,ENET_FALSE);
            enet_addClusterToPort(new_entry_i,id_i); 
            if (Init_QUEUE)
                enet_queue_blockPriQ(unit_i,new_entry_i->dbt_public.port.id.port,ENET_TRUE);
        }
    }



	if (new_entry_i->dbt_public.mode.type == ENET_QUEUE_MODE_TYPE_WFQ ){
        if(Init_QUEUE)
        enet_queue_UpdateHwClusterWFQ(unit_i,id_i,new_entry_i,old_entry_i);
	}

	if ( enet_queue_UpdateHwMTU(unit_i, id_i, new_entry_i, old_entry_i) != ENET_OK )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						   "%s - enet_queue_UpdateHwMTU failed\n",
        				   __FUNCTION__);
	}
    if(MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT){
        if (enet_queue_MultiPortsUpdateHw(unit_i, id_i, new_entry_i, old_entry_i) != ENET_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_queue_MultiPortsUpdateHw failed\n",
                __FUNCTION__);
        }
    }

	/* (2) Update the Priority Queue (MQS and strict/WFQ)*/
	if ((old_entry_i==NULL) ||
	    (MEA_OS_memcmp (new_entry_i->dbt_public.pri_queues,old_entry_i->dbt_public.pri_queues,sizeof(new_entry_i->dbt_public.pri_queues)) != 0)||
		(new_entry_i->dbt_private.is_fatherActive!=old_entry_i->dbt_private.is_fatherActive))
	{
		switch (new_entry_i->dbt_public.port.type) {
		case ENET_QUEUE_PORT_TYPE_PORT:
        case ENET_QUEUE_PORT_TYPE_VIRTUAL:
			
            /*Alex Weis 04/05/2016*/
            enet_queue_UpdateHw_mc_MQS(unit_i, id_i, new_entry_i, old_entry_i);
            /* update MQS */
            enet_queue_UpdateHwMQS(unit_i,id_i,new_entry_i,old_entry_i);
			enet_queue_UpdateHwPriStrictLevel(unit_i,id_i,new_entry_i,old_entry_i);
			enet_queue_UpdateHwPriWfq(unit_i,id_i,new_entry_i,old_entry_i);
            

		    break;
		case ENET_QUEUE_PORT_TYPE_PORT_GROUP:
		   /* T.B.D - Scan all the ports in the port group , 
					  and make same check as above */
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								   "%s - PORT_GROUP is not supported by Queue "
								   "(portGroup_id=%d,id_i=%d)\n",
								   __FUNCTION__,new_entry_i->dbt_public.port.id.portGroup,id_i);

			break;
       

		case ENET_QUEUE_PORT_TYPE_LAST:
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - Invalid Queue port type (%d) (id=%d)\n",
	        				   __FUNCTION__,new_entry_i->dbt_public.port.type,id_i);
			return ENET_ERROR;
			break;
		}
	}
    if( MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_CLUSTER_TYPE) == MEA_SHAPER_SUPPORT_CLUSTER_TYPE){	
    /* (3) Update Shaper */
        if(mea_Queue_UpdateHwShaperCmd(unit_i,id_i,new_entry_i,old_entry_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						   "%s - mea_Queue_UpdateHwShaperCmd failed\n",
        				   __FUNCTION__);
        }
    }

    if( MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE) == MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE){	
    /* (4) Update Shaper pri queue*/
        numOf_blocks_PriQue_support=mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support();
        for(PriQueue=0;PriQueue<8;PriQueue++){
            if( PriQueue >= (8-numOf_blocks_PriQue_support)){
                if(mea_PriQueue_UpdateHwShaperCmd(unit_i,id_i,PriQueue,
                                                 &new_entry_i->dbt_public.pri_queues[PriQueue],
                                                 (!old_entry_i) ? NULL :&old_entry_i->dbt_public.pri_queues[PriQueue])!=MEA_OK){
                                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                       "%s - mea_PriQueue_UpdateHwShaperCmd failed\n",
                                       __FUNCTION__);
                }
            }
        }
    }
 #if 1   
    if(enet_queue_UpdateHw_mc_MQS(unit_i,id_i,new_entry_i,old_entry_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - enet_queue_UpdateHw_mc_MQS failed (id=%d)\n",
            __FUNCTION__,id_i);
        return ENET_ERROR;
    }
#endif	
//     if ( enet_Cluster_Update_admin(unit_i, id_i, new_entry_i, old_entry_i) != ENET_OK ){
//         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
//             "%s - enet_Cluster_Update_admin failed\n",
//             __FUNCTION__);
//         return MEA_ERROR;
//     }

	/* Return to caller */
	return ENET_OK;
}







/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_DeleteDevice_Queue>                             */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_DeleteDevice_Queue (ENET_Unit_t       unit_i,
                                     ENET_QueueId_t     id_i)
{
 	enet_Queue_dbt entry;
	ENET_Uint32 len;
    ENET_Uint32 priQueue;
    ENET_Shaper_Profile_key_dbt shaperKeyDbt;

	len=sizeof(entry);
	if (ENET_Get_GroupElement(unit_i,
		                      enet_Queue_group_id[unit_i],
							  id_i,
		                      &entry,
							  &len) != ENET_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - ENET_Get_Queue failed (id=%d)\n",
						  __FUNCTION__,id_i);
	}


	if (enet_queue_SetDefaultMQS (unit_i,id_i) !=ENET_OK)
	{
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - enet_queue_SetDefaultMQS failed (id=%d)\n",
						  __FUNCTION__,id_i);
		return ENET_ERROR;
	}
    if (entry.dbt_public.port_group.valid){
        entry.dbt_public.port_group.valid=MEA_FALSE;
        if( enet_queue_MultiPortsUpdateHw(unit_i,id_i,&entry,NULL)!=ENET_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_queue_MultiPortsUpdateHw failed (id=%d)\n",
                __FUNCTION__,id_i);
            return ENET_ERROR;
        }
    }

    if(MEA_Platform_Get_ShaperClusterType_support()== MEA_TRUE){
        /* remove the shaper*/
        if (entry.dbt_public.shaper_enable) {
            MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
            shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_CLUSTER_TYPE;
            shaperKeyDbt.acm_mode    = 0;
            shaperKeyDbt.id = entry.dbt_public.shaper_Id;
            
            if (enet_DelOwner_Shaper_Profile(unit_i,&shaperKeyDbt) == ENET_ERROR) {
			        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							          "%s - can not Delete shaper profile queue %d\n",
							          __FUNCTION__,entry.dbt_public.shaper_Id);
			        return MEA_ERROR;
            }
        }
    }
    if(MEA_Platform_Get_ShaperPriQueueType_support()== MEA_TRUE){
        for(priQueue=0;priQueue < 8 ;priQueue++){
            if(entry.dbt_public.pri_queues[priQueue].shaperPri_enable == MEA_TRUE){
                MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
                shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE;
                shaperKeyDbt.acm_mode    = 0;
                shaperKeyDbt.id          = entry.dbt_public.pri_queues[priQueue].shaper_Id;
                if (enet_DelOwner_Shaper_Profile(unit_i,&shaperKeyDbt) == ENET_ERROR) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s - can not delete shaper profile queue %d\n",
                                          __FUNCTION__,entry.dbt_public.shaper_Id);
                    return MEA_ERROR;
                }
            }
        }
    }


    if (Init_QUEUE)
	    return	(enet_removeClusterFromPort(&entry,id_i));
    else
        return MEA_OK;

}




/*----------------------------------------------------------------------------*/
/*             API functions implementation                                   */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Init_Queue>                                            */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Init_Queue(ENET_Unit_t unit_i)
{

	ENET_Group_dbt group;
    static ENET_Bool QueueFirst = ENET_TRUE;
//	enet_Queue_dbt QueueDbt;
	ENET_QueueId_t internalQid;
    MEA_Queue_portsGroupe_t port_groupEntry;
    ENET_ind_write_t  ind_write;
    MEA_Port_t port;


	/* set default to Device */
	if ( enet_InitDevice_Queue(unit_i)!= ENET_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - enet_InitDevice_Queue failed \n",
							  __FUNCTION__);
			return ENET_ERROR;
	}

    if (QueueFirst) {
       ENET_Uint32 i;
       for (i=0;i<ENET_NUM_OF_ELEMENTS(enet_Queue_group_id);i++) {
          enet_Queue_group_id[unit_i] = ENET_PLAT_GENERATE_NEW_ID;
          enet_Queue_InitDone[unit_i] = ENET_FALSE;
       } 
       QueueFirst = ENET_FALSE;
    }

    if (enet_Queue_InitDone[unit_i] != ENET_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - unit %d is already init\n",
                          __FUNCTION__,unit_i);
       return ENET_ERROR;
    }

	enet_Queue_InitDone[unit_i] = ENET_FALSE;

    /* Create the Queue group  */
	MEA_OS_memset(&group,0,sizeof(group));
	MEA_OS_sprintf(group.name,"Queue group");
	group.max_number_of_elements = ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT;
        group.first_groupElementId = 1; /* Not allowed to use internalId cluster 0 */
	enet_Queue_group_id[unit_i] = ENET_PLAT_GENERATE_NEW_ID;
    if (ENET_Create_Group(unit_i,
		                  &group,
						  &(enet_Queue_group_id[unit_i])) != ENET_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - ENET_Create_Group failed\n",
						  __FUNCTION__);
	   return ENET_ERROR;
	}
#if 0 /* we not save*/
	/* take the broadcast cluster out of the pool */
	internalQid = ENET_PLAT_MC_DEFAULT_CLUSTER;
    MEA_OS_memset(&QueueDbt,0,sizeof(QueueDbt));
	QueueDbt.dbt_private.external_id=UNASSIGNED_QUEUE_VAL;
	QueueDbt.dbt_private.is_fatherActive=ENET_FALSE;
	MEA_OS_memcpy(QueueDbt.dbt_public.name,"reserved - MC    ",13);
	if (ENET_Create_GroupElement_withInRange(unit_i,
		                                     enet_Queue_group_id[unit_i],
		                                     &QueueDbt,
								             sizeof(QueueDbt),
                                            (ENET_GroupElementId_t) ENET_QUEUE_INTERNAL_ID_START_RANGE(QueueDbt.dbt_public.groupId),
                                            (ENET_GroupElementId_t) ENET_QUEUE_INTERNAL_ID_END_RANGE(QueueDbt.dbt_public.groupId),
								             &internalQid
                                            )!=ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MC can not create Queue element %d\n",
				 __FUNCTION__,internalQid);
		return ENET_ERROR;																			
	}	
#endif
    if(MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT ){
// init the table 77
    
        /* build the ind_write value */
        ind_write.tableType		= ENET_BM_TBL_CLUSTER_TO_MULTI_PORT;

        ind_write.cmdReg		= MEA_BM_IA_CMD;      
        ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
        ind_write.statusReg		= MEA_BM_IA_STAT;   
        ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
        ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
        ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)0;
        ind_write.funcParam3 = (MEA_funcParam)0;
        ind_write.funcParam4 = (MEA_funcParam)&port_groupEntry;
        ind_write.writeEntry = (MEA_funcParam)enet_queue_MultiPortsUpdateHwEntry;

        MEA_OS_memset(&port_groupEntry,0,sizeof(port_groupEntry));

        for(internalQid=0; internalQid <= ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT;internalQid++){

            /* build the ind_write value */
            
            ind_write.tableOffset	= internalQid;
            ind_write.funcParam4 = (MEA_funcParam)&port_groupEntry;
            ind_write.writeEntry    = (MEA_FUNCPTR)enet_queue_MultiPortsUpdateHwEntry;

            MEA_OS_memset(&port_groupEntry,0,sizeof(port_groupEntry));

            /* Write to the Indirect Table */
            if (ENET_WriteIndirect(unit_i,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d mdl=%d failed (queue=%d)\n",
                    __FUNCTION__,ind_write.tableType,ENET_MODULE_BM,internalQid);
                return MEA_ERROR;
            }


        }


//init table 78
        for (port = 0;port <=MEA_MAX_PORT_NUMBER; port++)
        {
            if( mea_queue_drv_config_potr_to_groupHw(unit_i,port,port)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_queue_drv_config_potr_to_groupHw\n",
                        __FUNCTION__);
                    return MEA_ERROR;
            }
        }

    }//MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT



    enet_Queue_InitDone[unit_i] = ENET_TRUE;

    /* Return to caller */
    return ENET_OK; 
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_ReInit_Queue>                                          */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_ReInit_Queue(ENET_Unit_t unit_i)
{

	if ( enet_ReInitDevice_Queue(unit_i)!= ENET_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - enet_ReInitDevice_Queue failed \n",
							  __FUNCTION__);
			return ENET_ERROR;
	}


	


    /* Return to caller */
    return ENET_OK; 
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Conclude_Queue>                                        */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Conclude_Queue(ENET_Unit_t unit_i)
{

	/* tbd - confirm that PriQue is empty*/
	/* tbd - Confirm that no service and forwarder entries points to this object*/
#if 1
    ENET_QueueId_t Queue_id;
	ENET_QueueId_t next_Queue_id;
	ENET_Bool            found;
//	ENET_QueueId_t internalQid;
#endif
    Init_QUEUE=MEA_FALSE;
	if (enet_Queue_InitDone[unit_i] == ENET_FALSE) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - Conclude call without Init before \n",
						   __FUNCTION__);
       return MEA_OK;
    }

#if 1

	/* Get the first Queue */
	if (ENET_GetFirst_Queue  (unit_i,
		                            &Queue_id,
							        NULL, /* entry_o*/
							        &found) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_GetFirst_Queue failed \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}

	/* scan all Queue and delete one by one */
	while(found) {

		next_Queue_id = Queue_id;
        if (ENET_GetNext_Queue   (unit_i,
		                                &next_Queue_id,
		  					            NULL, /* entry_o */
							            &found) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - enet_GetNext_Queue failed \n",
		 		  		       __FUNCTION__);
		    return ENET_ERROR;
		}

		
        if (enet_delete_Queue(unit_i,Queue_id) != ENET_OK) { 
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - enet_Delete_Queue failed \n",
		 		  		       __FUNCTION__);
		    return ENET_ERROR;
		}


		Queue_id = next_Queue_id;
	}
#if 0
	/* delete the broadcast cluster out of the pool */
	internalQid = ENET_PLAT_MC_DEFAULT_CLUSTER;
	if (ENET_Delete_GroupElement(unit_i,
		                         enet_Queue_group_id[unit_i],
							     internalQid) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_Delete_GroupElement failed (xPermission id=%d)\n",
						   __FUNCTION__,internalQid);
		return ENET_ERROR;
	}
#endif

#endif
    /* Delete the Queue Group  */
	if (ENET_Delete_Group(unit_i,enet_Queue_group_id[unit_i],MEA_TRUE) != ENET_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	 	                   "%s - ENET_Delete_Group failed \n",
	 		  		       __FUNCTION__);
	    return ENET_ERROR;
	}
    enet_Queue_group_id[unit_i] = ENET_PLAT_GENERATE_NEW_ID;

    if(enet_externalQidTable){
        MEA_OS_free(enet_externalQidTable);
        enet_externalQidTable = NULL;
    }
    
    if(enet_externalQid_state_Table){
        MEA_OS_free(enet_externalQid_state_Table);
        enet_externalQid_state_Table = NULL;
    }
    if (MEA_ClusterQueue_State) {
        MEA_OS_free(MEA_ClusterQueue_State);
        MEA_ClusterQueue_State = NULL;
    }

    
    
    /* Indicate InitDone to false for this unit */
    enet_Queue_InitDone[unit_i] = ENET_FALSE;

    /* Return to caller */
    return ENET_OK; 

}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Check_Queue>                                           */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Check_Queue (ENET_Unit_t        unit_i,
                              ENET_QueueId_t     id_i,     /* external */
                              ENET_Queue_dbt    *entry_i) 
{
	ENET_Uint32 priority_queue,PriQueue;
	ENET_Bool   strictZone = ENET_FALSE;
	//ENET_Bool   accumulatedWeight = 0;
    ENET_QueueId_t id;
	ENET_Bool      found_queue;
	ENET_Queue_dbt queue;
    MEA_Globals_Entry_dbt globals_entry;
    ENET_Uint32 numOf_blocks_PriQue_support;


  if ((id_i) != ENET_PLAT_GENERATE_NEW_ID) 
	  if (!IS_QUEUE_ID_VALID(id_i))
	  {
		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			  "%s - Queu Id invalid (id_i='%s')\n",
					__FUNCTION__,entry_i->name);
		  return ENET_ERROR;
	  }

		

	/* Check that name length not too long */
	ENET_ASSERT_VALID_NAME_LEN(entry_i->name,entry_i->name);
    
	/* if no name specify then generate default name */
	if ((id_i != ENET_PLAT_GENERATE_NEW_ID) &&
        (MEA_OS_strcmp(ENET_PLAT_GENERATE_NEW_NAME,entry_i->name) == 0)) {  
       MEA_OS_sprintf
		   (entry_i->name,
#ifdef ARCH64
		    "Q_%ld_%s_%ld",
#else		   
		    "Q_%d_%s_%d",
#endif			
		    id_i,
		    ((entry_i->port.type == ENET_QUEUE_PORT_TYPE_PORT      ) ? "P" :
	         (entry_i->port.type == ENET_QUEUE_PORT_TYPE_PORT_GROUP) ? "PG" :
             (entry_i->port.type == ENET_QUEUE_PORT_TYPE_VIRTUAL) ? "VP" : 
			 "Unknown"),
		    ((entry_i->port.type == ENET_QUEUE_PORT_TYPE_PORT      ) 
			 ? entry_i->port.id.port : 
			 (entry_i->port.type == ENET_QUEUE_PORT_TYPE_PORT_GROUP) 
				 ? entry_i->port.id.portGroup : 
            ((entry_i->port.type == ENET_QUEUE_PORT_TYPE_VIRTUAL) 
                 ? entry_i->port.id.virtualPort : 0)));
	}


	switch (entry_i->port.type) {
    case ENET_QUEUE_PORT_TYPE_PORT:
	    ENET_ASSERT_PORT_VALID(unit_i,(MEA_Port_t)entry_i->port.id.port);
		break;
    case ENET_QUEUE_PORT_TYPE_VIRTUAL:
        ENET_ASSERT_VIRTUAL_PORT_VALID(unit_i,(MEA_Port_t)entry_i->port.id.port);
        break;
 

    case ENET_QUEUE_PORT_TYPE_PORT_GROUP:
		/* T.B.D 
	    ENET_ASSERT_PORT_GROUP_VALID(unit_i,entry_i->port.id);  */
		break;
	case ENET_QUEUE_PORT_TYPE_LAST:
	default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - The Queue port type(%d)) is invalid (id=%d)\n",
				       	   __FUNCTION__,entry_i->port.type,id_i);
		return ENET_ERROR;
	}

    /* Get global mode */
    if (MEA_API_Get_Globals_Entry(unit_i,&globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_Get_Globals_Entry failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

	/* Check the mode */
	switch (entry_i->mode.type) {
	case ENET_QUEUE_MODE_TYPE_STRICT:

        if (globals_entry.bm_config.val.Cluster_mode != MEA_GLOBAL_CLUSTER_MODE_WFQ_PRIORITY) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - CASE STRICT Cluster mode %d (mode type %d) not supported \n",
							 __FUNCTION__,globals_entry.bm_config.val.Cluster_mode,entry_i->mode.type);
            return ENET_ERROR;
        }

		if (entry_i->mode.value.strict_priority  > MEA_MAX_OF_CLUSTER_STRICT_LEVEL - 1) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				               "%s - \n"
							   "queue mode strict priority (id=%d) > %d \n",
							   __FUNCTION__,id_i,MEA_MAX_OF_CLUSTER_STRICT_LEVEL);
			return ENET_ERROR;
        }

//
		break;
	case ENET_QUEUE_MODE_TYPE_WFQ:
        if(globals_entry.bm_config.val.Cluster_mode != MEA_GLOBAL_CLUSTER_MODE_WFQ_PRIORITY) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - CASE WFQ Cluster mode %d (mode type %d) not supported \n",
							 __FUNCTION__,globals_entry.bm_config.val.Cluster_mode,entry_i->mode.type);
            return ENET_ERROR;
        }
		break;
    case ENET_QUEUE_MODE_TYPE_RR:
        if(globals_entry.bm_config.val.Cluster_mode != MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - CASE RR Cluster mode %d (mode type %d) not supported \n",
							 __FUNCTION__,globals_entry.bm_config.val.Cluster_mode,entry_i->mode.type);
            return ENET_ERROR;
        }
        break;
	case ENET_QUEUE_MODE_TYPE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - The Queue mode type(%d)) is invalid (id=%d)\n",
				       	   __FUNCTION__,entry_i->mode.type,id_i);
		return ENET_ERROR;
	}

	switch (entry_i->mode.type) {
	case ENET_QUEUE_MODE_TYPE_STRICT:

		/* check if the mode queue cluster strict is support */
        if (enet_getNumOfClusterInPort(entry_i->port.id.port)!= 0){
            if((enet_Hw_portReverseMappingIndx[entry_i->port.id.port].numOf_strict > MEA_MAX_OF_CLUSTER_STRICT_LEVEL) ){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - The port %d have max value for strict (%d)> %d\n",
				       	   __FUNCTION__,entry_i->port.id.port,
                           enet_Hw_portReverseMappingIndx[entry_i->port.id.port].numOf_strict,
                           MEA_MAX_OF_CLUSTER_STRICT_LEVEL);
			    return ENET_ERROR;
            
            }
        }


		if (entry_i->mode.value.strict_priority > MEA_MAX_OF_CLUSTER_STRICT_LEVEL-1){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - for strict priority only val=0 is defined (type=%d,Q-%d)\n",
				       	   __FUNCTION__,entry_i->port.type,id_i);
			return ENET_ERROR;
		}
		/*need to check if the cluster is strict the value and if we are support it*/
       if (ENET_GetFirst_Queue(unit_i,&id,&queue,&found_queue) != ENET_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - ENET_GetFirst_Queue failed\n",
							  __FUNCTION__);
		   return ENET_ERROR;
	    }

	   while(found_queue) {

           if( (queue.mode.type==ENET_QUEUE_MODE_TYPE_STRICT) &&
               (queue.mode.value.strict_priority ==  entry_i->mode.value.strict_priority) &&
               (id_i != id)   &&
               queue.port.id.port == entry_i->port.id.port){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                      "%s - other queue keep same strict value (id=%d)\n",
				  			      __FUNCTION__,id);
		       return ENET_ERROR;
           }
		    
		   if (ENET_GetNext_Queue(unit_i,&id,&queue,&found_queue) != ENET_OK) {
		       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                      "%s - ENET_GetNext_Queue failed (id=%d)\n",
				  			      __FUNCTION__,id);
			return ENET_ERROR;
		   }
       }   




		break;
	case ENET_QUEUE_MODE_TYPE_RR:
		break;
	case ENET_QUEUE_MODE_TYPE_WFQ:
        if(entry_i->mode.value.wfq_weight > ENET_QUEUE_CLUSTER_WFQ_WEIGHT_MAX_VALUE ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						   "%s - wfq_weight failed value %d > max %d \n",
        				   __FUNCTION__,
                           entry_i->mode.value.wfq_weight,
                           ENET_QUEUE_CLUSTER_WFQ_WEIGHT_MAX_VALUE);
            return ENET_ERROR;
        }
        if(entry_i->mode.value.wfq_weight < ENET_QUEUE_CLUSTER_WFQ_WEIGHT_MIN_VALUE ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						   "%s - wfq_weight failed value %d < min %d \n",
        				   __FUNCTION__,
                           entry_i->mode.value.wfq_weight,
                           ENET_QUEUE_CLUSTER_WFQ_WEIGHT_MIN_VALUE);
            return ENET_ERROR;
        }
     
        break;
	case ENET_QUEUE_MODE_TYPE_LAST:
	default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - The Queue mode type(%d)) is invalid (id=%d)\n",
				       	   __FUNCTION__,entry_i->mode.type,id_i);
		return ENET_OK;
	}
    
    if ((MEA_QUEUE_MTU_SUPPORT_TYPE != MEA_QUEUE_MTU_SUPPORT_CLUSTER_TYPE) && (MEA_GLOBAL_MTU_PRIQ_REDUSE == MEA_FALSE) ){
        if((MEA_Mtu_t)entry_i->MTU != MEA_QUEUE_CLUSTER_MTU_DEF_VAL){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									      "%s - MTU  %d is different from the default value %d\n",
				           			      __FUNCTION__,
									      entry_i->MTU,
                                          MEA_QUEUE_CLUSTER_MTU_DEF_VAL);
				    return ENET_ERROR;	
        } 
    }   


	strictZone = ENET_FALSE;
    for (priority_queue=0;
	     priority_queue<ENET_NUM_OF_ELEMENTS(entry_i->pri_queues);
		 priority_queue++) 
		 {
            if(MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE){
			    if (entry_i->pri_queues[priority_queue].mtu > MEA_QUEUE_PRIQUEUE_MTU_MAX_VAL){
				                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									      "%s - the Mtu \n",
				           			      __FUNCTION__,
									      QUEUE_SIZE_IN_BYTES_MAX_VAL);
				    return ENET_ERROR;				  
			    }   
				    /* Verify granularity */       
				    if((entry_i->pri_queues[priority_queue].mtu % MEA_QUEUE_MTU_PRIQUEUE_GRANULARITY_VALUE) != 0){
					    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - pri queue %d mtu %d need to be in granularity of %d \n",
                               __FUNCTION__,priority_queue,(entry_i->pri_queues[priority_queue].mtu),MEA_QUEUE_MTU_PRIQUEUE_GRANULARITY_VALUE);
					    return MEA_ERROR; 
				    }	      
			}
			switch (entry_i->pri_queues[priority_queue].mode.type) {
	        case ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT:
				strictZone = ENET_TRUE;
                if(globals_entry.bm_config.val.pri_q_mode != MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY) {
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - priQueue %d mode (%d) not supported \n",
                              __FUNCTION__,
                              priority_queue,
                              entry_i->pri_queues[priority_queue].mode.type);
                   return ENET_ERROR;
                }
				if (entry_i->pri_queues[priority_queue].mode.value.strict_priority != 0){
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - strict PriQueue(%d) must have wight = 0\n",
				       	   __FUNCTION__,priority_queue);
					return ENET_ERROR;
				}
		       break;
	        case ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ:
                if(globals_entry.bm_config.val.pri_q_mode != MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY) {
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - priQueue %d mode (%d) not supported \n",
                              __FUNCTION__,
                              priority_queue,
                              entry_i->pri_queues[priority_queue].mode.type);
                   return ENET_ERROR;
                }
				if (strictZone == ENET_TRUE){
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                       "%s - PriQueue(%d) must  Strict since a lower priQueue is  strict\n",
				       	   __FUNCTION__,priority_queue);
					return ENET_ERROR;
				}
                if(entry_i->pri_queues[priority_queue].mode.value.wfq_weight > ENET_QUEUE_PRIORITY_WFQ_WEIGHT_MAX_VALUE ){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						       "%s -  wfq for pri_queues failed  value %d > max %d \n",
        				       __FUNCTION__,
                               entry_i->pri_queues[priority_queue].mode.value.wfq_weight,
                               ENET_QUEUE_PRIORITY_WFQ_WEIGHT_MAX_VALUE);
                return ENET_ERROR;
                }
                if(entry_i->pri_queues[priority_queue].mode.value.wfq_weight < ENET_QUEUE_PRIORITY_WFQ_WEIGHT_MIN_VALUE ){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						       "%s -  wfq for pri_queues failed  value %d < min %d \n",
        				       __FUNCTION__,
                               entry_i->pri_queues[priority_queue].mode.value.wfq_weight,
                               ENET_QUEUE_PRIORITY_WFQ_WEIGHT_MIN_VALUE);
                return ENET_ERROR;
                }
                

		       break;
               case ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR:
                    if(entry_i->pri_queues[priority_queue].mode.value.wfq_weight != ENET_QUEUE_PRIORITY_RR_WEIGHT_VALUE ){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						       "%s -  wfq for pri_queues failed  value %d != min %d \n",
        				       __FUNCTION__,
                               entry_i->pri_queues[priority_queue].mode.value.wfq_weight,
                               ENET_QUEUE_PRIORITY_RR_WEIGHT_VALUE);
                        return ENET_ERROR;
                    }
                   break;
	        case ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_LAST:
	        default:
               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
	                              "%s - The Priority Queue mode type(%d) is invalid (id=%d ,priority_queue=%d)\n",
				           	      __FUNCTION__,
								  entry_i->pri_queues[priority_queue].mode.type,
								  id_i,
								  priority_queue);
		       return ENET_OK;
			}
			if(entry_i->pri_queues[priority_queue].max_q_size_Byte%QUEUE_SIZE_IN_BYTES_GRANULARITY!=0){
			    entry_i->pri_queues[priority_queue].max_q_size_Byte = ((MEA_Uint32)(entry_i->pri_queues[priority_queue].max_q_size_Byte/QUEUE_SIZE_IN_BYTES_GRANULARITY))*QUEUE_SIZE_IN_BYTES_GRANULARITY;

			}
			if (entry_i->pri_queues[priority_queue].max_q_size_Byte > QUEUE_SIZE_IN_BYTES_MAX_VAL){
				               MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
									  "%s - the MQS in byte shall be 0..%d\n",
				           			  __FUNCTION__,
									  QUEUE_SIZE_IN_BYTES_MAX_VAL);
							   entry_i->pri_queues[priority_queue].max_q_size_Byte = QUEUE_SIZE_IN_BYTES_MAX_VAL;
			}
			if (entry_i->pri_queues[priority_queue].max_q_size_Packets > QUEUE_SIZE_IN_PACKET_MAX_VAL){
				               MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - the MQS in packet shall be 0..%d\n",
				           			  __FUNCTION__,
									  QUEUE_SIZE_IN_PACKET_MAX_VAL);
							   entry_i->pri_queues[priority_queue].max_q_size_Packets = QUEUE_SIZE_IN_PACKET_MAX_VAL;
			}

#if 1
            {
                ENET_Uint32 MqsByte;

                ENET_Mqs_t  userMqs_byte = entry_i->pri_queues[priority_queue].max_q_size_Byte;
            
                if (((userMqs_byte / 64) & 0xFFF0) == 0)
                {
                    //byteMultiplier = 0;
                    MqsByte = userMqs_byte / 64;
                }
                else if (((userMqs_byte / 64) & 0xFF00) == 0)
                {
                    //byteMultiplier = 1;
                    MqsByte = userMqs_byte / 1024;
                    if (userMqs_byte % 1024 != 0){
                        entry_i->pri_queues[priority_queue].max_q_size_Byte = MqsByte * 1024;
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " The bmqs[%d]= %d must be in multiples by 1024 >> Hw set (%d)  \n", priority_queue, userMqs_byte, MqsByte * 1024);
                    }

                }
                else if (((userMqs_byte / 64) & 0xF000) == 0)
                {
                    // byteMultiplier = 2;
                    MqsByte = userMqs_byte / 16384;
                    if (userMqs_byte % 16384 != 0){
                        entry_i->pri_queues[priority_queue].max_q_size_Byte = MqsByte * 16384;
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " The bmqs[%d]= %d must be in multiples by 16384 >> Hw set (%d)  \n", priority_queue, userMqs_byte, MqsByte * 16384);
                    }
                }
                else
                {
                    //byteMultiplier = 3;
                    MqsByte = userMqs_byte / 262144;
                    if (userMqs_byte % 262144 != 0){
                        entry_i->pri_queues[priority_queue].max_q_size_Byte = MqsByte * 262144;
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " The bmqs[%d]= %d must be in multiples by 262144 >> Hw set (%d)  \n", priority_queue, userMqs_byte, MqsByte * 262144);
                    }


                }




            
            }
#endif
#if 1
            { //

                
                ENET_Uint32 MqsPacket;
                
                ENET_Mqs_t  userMqs_packet = entry_i->pri_queues[priority_queue].max_q_size_Packets;

                if ((userMqs_packet & 0xFFF0) == 0){
                    //packetMultiplier = 0;
                    MqsPacket= userMqs_packet / 1;
                    

                }
                else if ((userMqs_packet & 0xFF00) == 0){
                   // packetMultiplier = 1;
                    MqsPacket = userMqs_packet / 16;
                    if (userMqs_packet % 16 != 0){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " The mqs[%d]= %d must be in multiples by 16 >> Hw set (%d)  \n", priority_queue, userMqs_packet,MqsPacket * 16);
                        entry_i->pri_queues[priority_queue].max_q_size_Packets = MqsPacket * 16;
                    }
                    
                }
                else if ((userMqs_packet & 0xF000) == 0){
                   // packetMultiplier = 2;
                    MqsPacket = userMqs_packet / 256;
                    if (userMqs_packet % 256 != 0){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " The mqs[%d]= %d must be in multiples by 256 >> user updat to  (%d)  \n", priority_queue, userMqs_packet, MqsPacket * 256);
                        entry_i->pri_queues[priority_queue].max_q_size_Packets = MqsPacket * 256;
                    }
                    
                }
                else{
                    //packetMultiplier = 3;
                    MqsPacket = userMqs_packet / 4096;
                    if (userMqs_packet % 4096 != 0){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " The mqs[%d]= %d must be in multiples 4096 >> user updat to  (%d)  \n", priority_queue, userMqs_packet, MqsPacket * 256);
                        entry_i->pri_queues[priority_queue].max_q_size_Packets = MqsPacket * 4096;
                    }

                }
            
            
            
            
            }
#endif
            if (entry_i->pri_queues[priority_queue].mc_MQS > QUEUE_SIZE_IN_MC_MQS_MAX_VAL){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - the mcMQS in shall be 0..%d\n",
                        __FUNCTION__,
                        QUEUE_SIZE_IN_MC_MQS_MAX_VAL);
                    return MEA_ERROR;
            }

			if(MEA_QUEUE_MTU_SUPPORT_TYPE != MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE){
                if (entry_i->pri_queues[priority_queue].mtu != MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - priqueue mtu is different %d from default value %d\n",
                        __FUNCTION__,entry_i->pri_queues[priority_queue].mtu,MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL);
                    return MEA_ERROR;
                }
            } 
    }

    if(MEA_Platform_Get_ShaperPriQueueType_support() == MEA_TRUE){
       for(PriQueue=0;PriQueue<8;PriQueue++){
           if (entry_i->pri_queues[PriQueue].shaperPri_enable == MEA_TRUE) {
               numOf_blocks_PriQue_support=mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support();
               if (!(PriQueue >= (8- numOf_blocks_PriQue_support))){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
  					          "%s - can not create shaper profile for cluster %d PriQueue %d  numOf_blocks %d\n",
							          __FUNCTION__,id_i,PriQueue, numOf_blocks_PriQue_support);
   		           return MEA_ERROR;
               }
           }
       }
    }



		 
        
    if ((MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_CLUSTER_TYPE) && (MEA_GLOBAL_MTU_PRIQ_REDUSE == MEA_FALSE)){
         /* Check for valid MTU parameter */
            if (entry_i->MTU > MEA_QUEUE_MTU_MAX_VALUE) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MTU 0x%08x is invalid (id_i=%d)\n",
                                 __FUNCTION__,(int)entry_i->MTU,id_i);
               return MEA_ERROR;
            }
         }   
           
         if (entry_i->groupId >= ENET_QUEUE_GROUP_ID_LAST) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - groupId %d >= max groupId %d \n",
                               __FUNCTION__,
                               entry_i->groupId,
                               ENET_QUEUE_GROUP_ID_LAST -1 );
             return MEA_ERROR;
         }
         if ((entry_i->groupId != ENET_QUEUE_GROUP_ID_0) &&
             (ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT == 64)) { 
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - groupId %d must be zero in this platform \n",
                               __FUNCTION__,
                               entry_i->groupId);
             return MEA_ERROR;
         }



	/* Return to caller */
	return ENET_OK;
}



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_IsAllowedToDelete_Queue>                               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_IsAllowedToDelete_Queue (ENET_Unit_t    unit_i,
                                          ENET_QueueId_t id_i) /* external */
{




   if((id_i == ((ENET_QueueId_t) ENET_PLAT_PORT_127_DEFAULT_CLUSTER)) ){
       return ENET_ERROR;
   }


	/* Return to caller */
	return ENET_OK;
}









/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Create_Queue>                                          */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Create_Queue(ENET_Unit_t       unit_i,
    ENET_Queue_dbt     *entry_i,
    ENET_QueueId_t     *id_io)  /*external QueueId*/

{
    enet_Queue_dbt QueueDbt;
    MEA_EgressPort_Entry_dbt EgressPort_Entry;
    ENET_Uint16 i;
    ENET_QueueId_t internalQid;
    //ENET_Bool silent=ENET_TRUE;
    ENET_Shaper_Profile_dbt      shaperDbt;
    ENET_Shaper_Profile_key_dbt  shaperKeyDbt;
    ENET_Uint32                 PriQueue;
    ENET_Uint32                 tmp_PriQueue;
    ENET_Uint32                 numOf_blocks_PriQue_support;
    MEA_Port_t temp_port;
    ENET_Uint32 count_port;
    ENET_Bool flag_found;

    MEA_API_LOG

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


        /* check unit_i  parameter */
        ENET_ASSERT_UNIT_VALID(unit_i);


    /* check entry_i parameter */
    ENET_ASSERT_NULL(entry_i, "entry_i");

    /* check id_io    parameter */
    ENET_ASSERT_NULL(id_io, "id_io");
    if ((*id_io) != ENET_PLAT_GENERATE_NEW_ID) {
        if (!IS_QUEUE_ID_VALID(*id_io)) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Queue Id is invalid (Queue id=%d)\n",
                __FUNCTION__, *id_io);
            return ENET_ERROR;
        }
    }

    if ((entry_i->port_group.valid == MEA_TRUE)
        && (!MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - port_group.valid when the  MEA_FRAGMENT_ENABLE not support(Queue id=%d)\n",
            __FUNCTION__, *id_io);
        return ENET_ERROR;
    }
    if ((MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT) &&
        (entry_i->port_group.valid == MEA_TRUE) &&
        (entry_i->port_group.num_of_port > MEA_MAX_PORT_FOR_GROUP)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- port_group.num_of_port %d > of max %d or min \n",
            __FUNCTION__, entry_i->port_group.num_of_port, MEA_MAX_PORT_FOR_GROUP);
        return ENET_ERROR;
    }

    if (entry_i->port_group.valid) {
        if (entry_i->port_group.ports_groupId > MEA_FRAGMENT_VSP_NUM_OF_GROUP){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s for port_group the cluster groupId out of range %d\n", __FUNCTION__, MEA_FRAGMENT_VSP_NUM_OF_GROUP);
            return MEA_ERROR;

        }
        count_port = 0;
        flag_found = MEA_FALSE;
		
        /*check if only */
        for (temp_port = 0; temp_port < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; temp_port++)
        {
            if (((ENET_Uint32*)(&(entry_i->port_group.Multiple_Port.out_ports_0_31)))[temp_port / 32]
                & (1 << (temp_port % 32))) {
                if (MEA_API_Get_IsPortValid(temp_port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE) == MEA_FALSE)  {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "port %d not Valid port\n", temp_port);
                    return MEA_ERROR;

                }
                if (temp_port == entry_i->port.id.port){
                    flag_found = MEA_TRUE;
                }

                count_port++;
            }
        }
        if (!flag_found){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- the entry_i->port %d is not part of the multi port \n",
                __FUNCTION__, entry_i->port.id.port);
            return MEA_ERROR;
        }
        if (count_port != entry_i->port_group.num_of_port){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- num_of_port %d is different as you add %d \n",
                __FUNCTION__, entry_i->port_group.num_of_port, count_port);
            return MEA_ERROR;
        }
    }

    if (entry_i->port.type == ENET_QUEUE_PORT_TYPE_VIRTUAL){
        //check how many virtualPort we support
        if (MEA_STANDART_BONDING_SUPPORT == MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n-  QUEUE_PORT_TYPE_VIRTUAL not support \n", __FUNCTION__);
            return MEA_ERROR;
        }
        if (entry_i->port.id.virtualPort > MEA_STANDART_BONDING_NUM_OF_GROUP_TX){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s \n-  The virtualPort is out of range \n", __FUNCTION__);
            return MEA_ERROR;

        }

        entry_i->port.id.port = MEA_QUEUE_VIRTUAL_PORT_START_SRC_PORT + entry_i->port.id.virtualPort;

    }

    /* check for entity attributes  */
    if (enet_Check_Queue(unit_i,
        (*id_io),
        entry_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Queue Id already exist (Queue id=%d)\n",
            __FUNCTION__, *id_io);
        return ENET_ERROR;
    }
    if (enet_getNumOfClusterInPort(entry_i->port.id.port) >= (ENET_Uint32)QUE_PER_PORT(entry_i->port.id.port))
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - exceeded max number of cluster(%d) per port (%d)\n",
            __FUNCTION__, QUE_PER_PORT(entry_i->port.id.port), entry_i->port.id.port);
        return ENET_ERROR;
    }

    if (MEA_Platform_Get_ShaperPriQueueType_support() == MEA_TRUE){
        for (PriQueue = 0; PriQueue < 8; PriQueue++){
            if (entry_i->pri_queues[PriQueue].shaperPri_enable == MEA_TRUE) {
                numOf_blocks_PriQue_support = mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support();
                if (!(PriQueue >= (8 - numOf_blocks_PriQue_support))){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - can not create shaper profile  PriQueue %d\n",
                        __FUNCTION__, PriQueue);
                    return MEA_ERROR;
                }
            }
        }
    }

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    MEA_OS_memset(&QueueDbt, 0, sizeof(QueueDbt));
    MEA_OS_memcpy(&QueueDbt.dbt_public, entry_i, sizeof(QueueDbt.dbt_public));

    /* update private data base */
    if (*id_io == ENET_PLAT_GENERATE_NEW_ID){
        for (i = 0;
            ((i < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT) &&
            (enet_externalQidTable[i].isValid));
        i++){
        }
        if (i == ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Queue pool is empty \n",
                __FUNCTION__);
            return ENET_ERROR;
        }
        QueueDbt.dbt_private.external_id = i;
        *id_io = i;
	} else {
        if (enet_externalQidTable[*id_io].isValid){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Queue Id already used (Queue id=%d)\n",
                __FUNCTION__, *id_io);
            return ENET_ERROR;
        }
        QueueDbt.dbt_private.external_id = *id_io;
    }

    if (QueueDbt.dbt_public.port.type == ENET_QUEUE_PORT_TYPE_PORT){
        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
            (MEA_Port_t)QueueDbt.dbt_public.port.id.port,
            &EgressPort_Entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Get_EgressPort_Entry for port %d failed\n",
                __FUNCTION__,
                (MEA_Port_t)QueueDbt.dbt_public.port.id.port);
            return MEA_OK;
        }
        QueueDbt.dbt_private.is_fatherActive = isPortEnable((&EgressPort_Entry));
        //  QueueDbt.dbt_private.is_fatherActive=MEA_TRUE;
    } else {
          QueueDbt.dbt_private.is_fatherActive=MEA_TRUE;
    }



    internalQid = ENET_PLAT_GENERATE_NEW_ID;
    if (*id_io == ENET_PLAT_PORT_127_DEFAULT_CLUSTER){
        internalQid = ENET_PLAT_PORT_127_DEFAULT_INTERNAL_CLUSTER;
    }

    if (MEA_VPLS_SUPPORT) { /*GW*/
        if (*id_io == ENET_PLAT_PORT_126_DEFAULT_CLUSTER){
            internalQid = ENET_PLAT_PORT_126_DEFAULT_INTERNAL_CLUSTER;
        }

    }

    /************************************************************************/
    /* Cluster for Lag port                                                  */
    /************************************************************************/






    {
        MEA_db_HwUnit_t hwUnit;
		mea_memory_mode_e save_globalMemoryMode;
        if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
            hwUnit = MEA_HW_UNIT_ID_GENERAL;
        } 
		MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        if (MEA_SHAPER_SUPPORT_TYPE(MEA_SHAPER_SUPPORT_CLUSTER_TYPE) != MEA_SHAPER_SUPPORT_CLUSTER_TYPE && 
            (entry_i->shaper_enable == MEA_TRUE)){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				    			  "%s - The shaper is not available for \" CLUSTER mode \" ",__FUNCTION__);
			  MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			  return MEA_ERROR;
        }
		MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

    }

    
    if ((entry_i->shaper_enable == MEA_TRUE) && 
        (MEA_Platform_Get_ShaperClusterType_support() == MEA_TRUE)) {
        
        MEA_OS_memset(&shaperDbt,0,sizeof(shaperDbt));
        shaperDbt.CIR  = QueueDbt.dbt_public.shaper_info.CIR;
		shaperDbt.CBS  = QueueDbt.dbt_public.shaper_info.CBS;
		shaperDbt.type = QueueDbt.dbt_public.shaper_info.resolution_type;
		shaperDbt.overhead = QueueDbt.dbt_public.shaper_info.overhead;
		shaperDbt.Cell_Overhead = QueueDbt.dbt_public.shaper_info.Cell_Overhead;
        shaperDbt.mode = QueueDbt.dbt_public.shaper_info.mode;
        MEA_OS_strcpy(shaperDbt.name,"");
        MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
        shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_CLUSTER_TYPE;
        shaperKeyDbt.acm_mode    = 0;
        shaperKeyDbt.id          = ENET_PLAT_GENERATE_NEW_ID;
		if (enet_AddOwner_Shaper_Profile(unit_i,&shaperKeyDbt,&shaperDbt) == ENET_ERROR) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - can not create shaper profile for queue %d\n",
							  __FUNCTION__,*id_io);
			return MEA_ERROR;
		}
        entry_i->shaper_Id            = shaperKeyDbt.id;
		QueueDbt.dbt_public.shaper_Id = shaperKeyDbt.id;
     
    }

    if(MEA_Platform_Get_ShaperPriQueueType_support() == MEA_TRUE){
        for(PriQueue=0;PriQueue<8;PriQueue++){
            if (entry_i->pri_queues[PriQueue].shaperPri_enable == MEA_TRUE) {


            MEA_OS_memset(&shaperDbt,0,sizeof(shaperDbt));
            shaperDbt.CIR  = QueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.CIR;
	        shaperDbt.CBS  = QueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.CBS;
	        shaperDbt.type = QueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.resolution_type;
	        shaperDbt.overhead = QueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.overhead;
	        shaperDbt.Cell_Overhead = QueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.Cell_Overhead;
            shaperDbt.mode = QueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.mode; 
            MEA_OS_strcpy(shaperDbt.name,"");
            MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
            shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE;
            shaperKeyDbt.acm_mode    = 0;
            shaperKeyDbt.id          = ENET_PLAT_GENERATE_NEW_ID;
	        if (enet_AddOwner_Shaper_Profile(unit_i,&shaperKeyDbt,&shaperDbt) == ENET_ERROR) {
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						          "%s - can not create shaper profile for PriQueue %d\n",
						          __FUNCTION__,*id_io);
                for(tmp_PriQueue=0;tmp_PriQueue<PriQueue;tmp_PriQueue++) {
                    if (entry_i->pri_queues[tmp_PriQueue].shaperPri_enable == MEA_TRUE) {
                        MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
                        shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE;
                        shaperKeyDbt.acm_mode    = 0;
                        shaperKeyDbt.id          = entry_i->pri_queues[tmp_PriQueue].shaper_Id;
  		                enet_DelOwner_Shaper_Profile(unit_i,&shaperKeyDbt);
                    }
                }
            	ENET_Delete_GroupElement(unit_i,
                                         enet_Queue_group_id[unit_i],
                                         internalQid);
		        return MEA_ERROR;
            }
            entry_i->pri_queues[PriQueue].shaper_Id            = shaperKeyDbt.id;
            QueueDbt.dbt_public.pri_queues[PriQueue].shaper_Id = shaperKeyDbt.id;
         
        }
    }
}
#if defined(MEA_ENV_256PORT)
    if (entry_i->port.type == ENET_QUEUE_PORT_TYPE_PORT)
    {
        if (entry_i->port.id.port <= 127)
        {
            if (QueueDbt.dbt_public.groupId != 0 && QueueDbt.dbt_public.groupId != 1){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - can not create Queue element %d the groupId %d but need to be 0 or 1\n", __FUNCTION__, *id_io, QueueDbt.dbt_public.groupId);
            
                return ENET_ERROR;
            }
        }
        else if (entry_i->port.id.port >= 128 && entry_i->port.id.port <= 255){
            if (QueueDbt.dbt_public.groupId != 2 && QueueDbt.dbt_public.groupId != 3){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - can not create Queue element %d the groupId %d but need to be 2 or 3\n", __FUNCTION__, *id_io, QueueDbt.dbt_public.groupId);

                return ENET_ERROR;
            }
            
        }

    }
#endif
	if (ENET_Create_GroupElement_withInRange(unit_i,
		                                     enet_Queue_group_id[unit_i],
		                                     &QueueDbt,
								             sizeof(QueueDbt),
                                             (ENET_GroupElementId_t)ENET_QUEUE_INTERNAL_ID_START_RANGE(QueueDbt.dbt_public.groupId),
                                             (ENET_GroupElementId_t)ENET_QUEUE_INTERNAL_ID_END_RANGE(QueueDbt.dbt_public.groupId),
								             &internalQid
                                            )!=ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - can not create Queue element %d\n",
				 __FUNCTION__,*id_io);
		return ENET_ERROR;																			
	}	
    
    
	enet_externalQidTable[QueueDbt.dbt_private.external_id].isValid = ENET_TRUE;
	enet_externalQidTable[QueueDbt.dbt_private.external_id].Qid =internalQid ;
    
    enet_externalQid_state_Table[QueueDbt.dbt_private.external_id].isValid=MEA_TRUE;
    enet_externalQid_state_Table[QueueDbt.dbt_private.external_id].portState=MEA_PORT_STATE_FORWARD;
    
    /* update the Queue instance */
    if (enet_UpdateDevice_Queue
		      (unit_i,
		       internalQid,
			   &QueueDbt,
               NULL) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_UpdateDevice_Queue failed (Queue id=%d)\n",
						   __FUNCTION__,*id_io);
		return ENET_ERROR;
	} 

    /* Clear Queue counters */
    if (MEA_API_Clear_Counters_Queue(unit_i,*id_io) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - MEA_API_Clear_Counters_Queuefailed (Queue id=%d)\n",
						   __FUNCTION__,*id_io);
		return ENET_ERROR;
    }
	 
    if(MEA_GLOBAL_FORWARDER_SUPPORT_DSE)
	///create x permission for this cluster
	{
		ENET_xPermissionId_t xPermission_id;
		 
		ENET_xPermission_dbt xPermission_entry;
		MEA_OS_memset(&xPermission_entry,0,sizeof(xPermission_entry));
		
		xPermission_id = (*id_io);
		if(xPermission_id == 0){
			((MEA_Uint32 *)(&xPermission_entry.out_ports_0_31))
				[0/32] |= 
				(unsigned long)(0x00000001 << (0%32));
			if (enet_Create_xPermission(unit_i,
				&xPermission_entry,
				0,
				&xPermission_id)!=ENET_OK){
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						"%s - enet_Create_xPermission %d\n",
						__FUNCTION__,xPermission_id);
					return MEA_ERROR;
			}
		}else{
		
		((MEA_Uint32 *)(&xPermission_entry.out_ports_0_31))
			[xPermission_id/32] |= 
					(unsigned long)(0x00000001 << (xPermission_id%32));
		if (enet_Create_xPermission(unit_i,
			&xPermission_entry,
			0,
			&xPermission_id)!=ENET_OK){
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					"%s - enet_Create_xPermission %d\n",
					__FUNCTION__,xPermission_id);
				
				return MEA_ERROR;
			}
		}

	}
  //////////////////////////////////////////////////////////////////////////
   
    
    if(enet_Cluster_Set_Update_admin(unit_i,
        internalQid,
        QueueDbt.dbt_public.adminOn,
        QueueDbt.dbt_private.is_fatherActive) != ENET_OK ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Cluster_Set_Update_admin failed\n",
                __FUNCTION__);
            return MEA_ERROR;
    }
  

   
    

    /************************************************************************/
    /* ADM                                                                  */
    /************************************************************************/
    mea_drv_ADM_Cluster_To_port(unit_i,
                                entry_i->port.id.port,
                                entry_i->groupId,
                                internalQid, /* Internal */
                                MEA_TRUE);



    

	/* return to caller */
    return ENET_OK;
}
                                 

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Set_Queue>                                             */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Set_Queue       (ENET_Unit_t     unit_i,
								  ENET_QueueId_t  id_i, /* external */
                                  ENET_Queue_dbt *entry_i)
{

    ENET_QueueId_t internalQid;
    enet_Queue_dbt oldQueueDbt;
    ENET_Uint32 len;
    MEA_EgressPort_Entry_dbt EgressPort_Entry;
    ENET_Bool flag_shaper_enable=MEA_FALSE;



    MEA_OS_memset(&EgressPort_Entry, 0, sizeof(MEA_EgressPort_Entry_dbt));

    /* check for entity attributes  */
    if (enet_Check_Queue(unit_i,
        id_i,
        entry_i) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Check_Queue failed (Queue id=%d)\n",
                __FUNCTION__,id_i);
            return ENET_ERROR;
    } 

    if ((internalQid=enet_cluster_external2internal(id_i))==UNASSIGNED_QUEUE_VAL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - in call to enet_cluster_external2internal \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    len=sizeof(oldQueueDbt);
    if (ENET_Get_GroupElement(unit_i,
        enet_Queue_group_id[unit_i],
        internalQid,
        &(oldQueueDbt),
        &len) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Get_GroupElement failed \n",
                __FUNCTION__);
            return ENET_ERROR;
    }
    if(oldQueueDbt.dbt_public.port.type == ENET_QUEUE_PORT_TYPE_PORT){
        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
            (MEA_Port_t)oldQueueDbt.dbt_public.port.id.port,
            &EgressPort_Entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_Get_EgressPort_Entry for port %d failed\n",
                    __FUNCTION__,
                    (MEA_Port_t)oldQueueDbt.dbt_public.port.id.port);
                return ENET_ERROR;
        }
    }

    if(MEA_OS_memcmp(&entry_i->mode,
                     &oldQueueDbt.dbt_public.mode,
                     sizeof(oldQueueDbt.dbt_public.mode))!=0 ){
           if(EgressPort_Entry.shaper_enable == MEA_TRUE){
                //flag
               flag_shaper_enable = MEA_TRUE;
               EgressPort_Entry.shaper_enable = MEA_FALSE;

               if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
                   (MEA_Port_t)oldQueueDbt.dbt_public.port.id.port,
                   &EgressPort_Entry) != MEA_OK) {
                       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - MEA_API_Get_EgressPort_Entry for port %d failed\n",
                           __FUNCTION__,
                           (MEA_Port_t)oldQueueDbt.dbt_public.port.id.port);
                       return ENET_ERROR;
               }




              
           }

         
    }


    if(ENET_drv_Set_Queue(unit_i,
                       id_i, /* external */
                       entry_i)!=ENET_OK){

        
            return ENET_ERROR;
    }

    if(flag_shaper_enable){

        EgressPort_Entry.shaper_enable = MEA_TRUE;

        if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,
            (MEA_Port_t)oldQueueDbt.dbt_public.port.id.port,
            &EgressPort_Entry) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_Get_EgressPort_Entry for port %d failed\n",
                    __FUNCTION__,
                    (MEA_Port_t)oldQueueDbt.dbt_public.port.id.port);
                return ENET_ERROR;
        }


    }


    
    
    return ENET_OK;
}
                                 



static ENET_Status ENET_drv_Set_Queue       (ENET_Unit_t     unit_i,
                                  ENET_QueueId_t  id_i, /* external */
                                  ENET_Queue_dbt *entry_i)


{

	enet_Queue_dbt oldQueueDbt,newQueueDbt;
    enet_Queue_dbt modify_oldQueueDbt;
	ENET_Uint32 len;
	MEA_EgressPort_Entry_dbt EgressPort_Entry;
	ENET_QueueId_t internalQid;
    ENET_Shaper_Profile_dbt      shaperDbt;
    ENET_Shaper_Profile_key_dbt  shaperKeyDbt;
    ENET_PriQueueId_t priQue,PriQueue,tmp_PriQueue;
    MEA_OutPorts_Entry_dbt Multiple_Port;
#if 1
    MEA_Port_t temp_port;
    ENET_Uint32 count_port;
    ENET_Bool flag_found;
    MEA_Uint16 old_sheperId;
    ENET_Bool  old_Shaper_valid;
#endif    

   MEA_API_LOG

    MEA_OS_memset(&Multiple_Port,0,sizeof(Multiple_Port));

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_QUEUE_VALID(unit_i,id_i);

	/* check entry_i parameter */
	ENET_ASSERT_NULL(entry_i,"entry_i");
#if 0
	/* check for entity attributes  */
	if (enet_Check_Queue(unit_i,
		                        id_i,
						        entry_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_Check_Queue failed (Queue id=%d)\n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	} 
#endif
    if((entry_i->port_group.valid == MEA_TRUE ) && 
       (!MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT )  ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - port_group.valid when the  MEA_FRAGMENT_ENABLE not support(Queue id=%d)\n",
                __FUNCTION__,id_i);
            return ENET_ERROR;
    }
    if( MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT ) {
        if((entry_i->port_group.valid == MEA_TRUE) && 
        ((entry_i->port_group.num_of_port > MEA_MAX_PORT_FOR_GROUP) ||
        (entry_i->port_group.num_of_port <=1)) ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- port_group.num_of_port %d < of min %d or min \n",
                __FUNCTION__,entry_i->port_group.num_of_port,1);
            return ENET_ERROR;
        }
        if((entry_i->port_group.valid == MEA_FALSE)&&
            (MEA_OS_memcmp(&entry_i->port_group.Multiple_Port,&Multiple_Port,sizeof(entry_i->port_group.Multiple_Port))!=0) ){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n-  Multiple_Port need to be zero when the port_group.valid=FALSE\n",
                    __FUNCTION__,entry_i->port_group.num_of_port);
                return ENET_ERROR;
        }



    }

    


    

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

	if ((internalQid=enet_cluster_external2internal(id_i))==UNASSIGNED_QUEUE_VAL){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - in call to enet_cluster_external2internal \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}

	len=sizeof(oldQueueDbt);
	if (ENET_Get_GroupElement(unit_i,
		                      enet_Queue_group_id[unit_i],
							  internalQid,
							  &(oldQueueDbt),
							  &len) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_Get_GroupElement failed \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}

#ifdef  ENET_SW_CHECK_INPUT_PARAMETERS 
     //////////////////////////////////////////////////////////////////////////
     // Check if we on fragment 
    //////////////////////////////////////////////////////////////////////////
     if(MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT ) {
         if((entry_i->port_group.valid == MEA_TRUE) && 
             (oldQueueDbt.dbt_public.port_group.valid == MEA_TRUE) &&
             (MEA_OS_memcmp(&entry_i->port_group.Multiple_Port,
             &oldQueueDbt.dbt_public.port_group.Multiple_Port,sizeof(entry_i->port_group.Multiple_Port))!=0)){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - not allow to to change the multi port modify \n",
                     __FUNCTION__);
                 return ENET_ERROR;
         
         }
     
     }

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */
	/* update new Queue db */
	MEA_OS_memcpy(&newQueueDbt,&oldQueueDbt,sizeof(newQueueDbt));
    MEA_OS_memcpy(&modify_oldQueueDbt,&oldQueueDbt,sizeof(modify_oldQueueDbt));
    old_Shaper_valid = oldQueueDbt.dbt_public.shaper_enable;
    old_sheperId     = oldQueueDbt.dbt_public.shaper_Id;

	MEA_OS_memcpy(&newQueueDbt.dbt_public,entry_i,sizeof(newQueueDbt.dbt_public));
	newQueueDbt.dbt_private.external_id = id_i;
	if(oldQueueDbt.dbt_public.port.type == ENET_QUEUE_PORT_TYPE_PORT){
    if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
		                         (MEA_Port_t)newQueueDbt.dbt_public.port.id.port,
						        &EgressPort_Entry) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				  "%s - MEA_API_Get_EgressPort_Entry for port %d failed\n",
				  __FUNCTION__,
			  (MEA_Port_t)newQueueDbt.dbt_public.port.id.port);
		return ENET_ERROR;
	}
    
    
    newQueueDbt.dbt_private.is_father1p1 = (EgressPort_Entry.protectType);
   
   

    newQueueDbt.dbt_private.is_fatherActive =isPortEnable((&EgressPort_Entry));
    
    //newQueueDbt.dbt_private.is_fatherActive =MEA_TRUE;
    }else{

        newQueueDbt.dbt_private.is_fatherActive = MEA_TRUE;
    }

    /* Not allowed to change group Id of cluster */
    if (oldQueueDbt.dbt_public.groupId != entry_i->groupId) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Not allowed to change groupId of queue \n",
                          __FUNCTION__);
        return ENET_ERROR;
    }
/*****************/

             

#if 1

   if((entry_i->port_group.valid) && 
       ( MEA_OS_memcmp(&entry_i->port_group,
       &oldQueueDbt.dbt_public.port_group,sizeof(entry_i->port_group))!=0)) {

        if(entry_i->groupId != 0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n-for port_group the cluster groupId need to be zero\n",__FUNCTION__);
            return MEA_ERROR;

        }
        count_port=0;
        flag_found=MEA_FALSE;
		

        /*check if only */
        for (temp_port = 0;temp_port <ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; temp_port++)
        {
            if (((ENET_Uint32*)(&(entry_i->port_group.Multiple_Port.out_ports_0_31)))[temp_port/32] 
            & (1 << (temp_port%32))) {
                if(MEA_API_Get_IsPortValid(temp_port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE)  {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "port %d not Valid port\n",temp_port);
                    return MEA_ERROR;

                }
                if(temp_port == entry_i->port.id.port){
                    flag_found=MEA_TRUE;
                }

                count_port++;
            }
        }
        if(!flag_found){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- the entry_i->port %d is not part of the multi port \n",
                __FUNCTION__,entry_i->port.id.port);
            return MEA_ERROR;
        }
        if(count_port != entry_i->port_group.num_of_port){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- num_of_port is deferent as you add \n",
                __FUNCTION__,entry_i->port_group.num_of_port,MEA_MAX_PORT_FOR_GROUP);
            return MEA_ERROR;
        }
    }
#endif

/*****************/



    /* Not allowed to enable shaper if not support shaper on queue */
    {
        MEA_db_HwUnit_t hwUnit;
		mea_memory_mode_e save_globalMemoryMode;
        if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
            hwUnit = MEA_HW_UNIT_ID_GENERAL;
        } else {
            if ((entry_i->port.id.port == 118) || 
                (entry_i->port.id.port == 127) || 
                (entry_i->port.id.port == 24)) {
                hwUnit = MEA_HW_UNIT_ID_UPSTREAM;
            } else {
                hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
            }
        }

		MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
        if (MEA_SHAPER_SUPPORT_CLUSTER_TYPE != MEA_SHAPER_SUPPORT_CLUSTER_TYPE && 
            (entry_i->shaper_enable == MEA_TRUE)){
            
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				    			  "%s - The shaper is not available for \" Queue mode \" ",
					    		  __FUNCTION__);
				MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			    return MEA_ERROR;
        }
		MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }

     
    if(oldQueueDbt.dbt_public.port.id.port != newQueueDbt.dbt_public.port.id.port){
      //check if we have place to Add new one to new port
        if (enet_getNumOfClusterInPort(newQueueDbt.dbt_public.port.id.port)+1 >(ENET_Uint32)QUE_PER_PORT(newQueueDbt.dbt_public.port.id.port)){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				    			  "%s - Can move cluster %d to port %d num of clusters  %d ",
					    		  __FUNCTION__, id_i,
                                  newQueueDbt.dbt_public.port.id.port,
                                  enet_getNumOfClusterInPort(newQueueDbt.dbt_public.port.id.port));
            
			return MEA_ERROR;
        }

    }

    enet_xPermission_disableCluster(unit_i,id_i);
    
    
    if(enet_Cluster_Set_Update_admin(unit_i,
        internalQid,
        MEA_FALSE,MEA_FALSE) != ENET_OK ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Cluster_Set_Update_admin failed\n",
                __FUNCTION__);
            return MEA_ERROR;
    }


   
   
     if(oldQueueDbt.dbt_public.port.id.port != newQueueDbt.dbt_public.port.id.port ||
        oldQueueDbt.dbt_public.pri_queues[0].max_q_size_Byte != newQueueDbt.dbt_public.pri_queues[0].max_q_size_Byte ||
        oldQueueDbt.dbt_public.pri_queues[1].max_q_size_Byte != newQueueDbt.dbt_public.pri_queues[1].max_q_size_Byte ||
        oldQueueDbt.dbt_public.pri_queues[2].max_q_size_Byte != newQueueDbt.dbt_public.pri_queues[2].max_q_size_Byte ||
        oldQueueDbt.dbt_public.pri_queues[3].max_q_size_Byte != newQueueDbt.dbt_public.pri_queues[3].max_q_size_Byte ||
        oldQueueDbt.dbt_public.pri_queues[4].max_q_size_Byte != newQueueDbt.dbt_public.pri_queues[4].max_q_size_Byte ||
        oldQueueDbt.dbt_public.pri_queues[5].max_q_size_Byte != newQueueDbt.dbt_public.pri_queues[5].max_q_size_Byte ||
        oldQueueDbt.dbt_public.pri_queues[6].max_q_size_Byte != newQueueDbt.dbt_public.pri_queues[6].max_q_size_Byte ||
        oldQueueDbt.dbt_public.pri_queues[7].max_q_size_Byte != newQueueDbt.dbt_public.pri_queues[7].max_q_size_Byte 
        )
     {
         /*First write the old with mqs 0 to all */
        /* update the device */
         for (priQue=0;priQue<ENET_MAX_NUMBER_OF_PRI_QUEUE;priQue++) {
            modify_oldQueueDbt.dbt_public.pri_queues[priQue].max_q_size_Byte=0;
            /*Alex Weis 04/05/2016*/
            modify_oldQueueDbt.dbt_public.pri_queues[priQue].max_q_size_Packets = 0;
            modify_oldQueueDbt.dbt_public.pri_queues[priQue].mc_MQS = 0;
         }
         modify_oldQueueDbt.dbt_public.MTU=0;
         modify_oldQueueDbt.dbt_public.shaper_enable=MEA_FALSE;
    	
         if(MEA_Platform_Get_ShaperPriQueueType_support() == MEA_TRUE){
            for(PriQueue=0;PriQueue<ENET_MAX_NUMBER_OF_PRI_QUEUE;PriQueue++){
                if (modify_oldQueueDbt.dbt_public.pri_queues[PriQueue].shaperPri_enable == MEA_TRUE) {
                    modify_oldQueueDbt.dbt_public.pri_queues[PriQueue].shaperPri_enable=MEA_FALSE;
                }
            }
        }
        

         if (enet_UpdateDevice_Queue
		          (unit_i,
		           internalQid,
                   &modify_oldQueueDbt,
			       &oldQueueDbt) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                   "%s - enet_UpdateDevice_Queue failed (Queue id=%d)\n",
						       __FUNCTION__,id_i);
		    return ENET_ERROR;
	    }
        
         modify_oldQueueDbt.dbt_public.port.id.port   = newQueueDbt.dbt_public.port.id.port;
        if (enet_UpdateDevice_Queue
		          (unit_i,
		           internalQid,
                   &modify_oldQueueDbt,
			       &oldQueueDbt) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                   "%s - enet_UpdateDevice_Queue failed (Queue id=%d)\n",
						       __FUNCTION__,id_i);
		    return ENET_ERROR;
	    }

     MEA_OS_memcpy(&oldQueueDbt,&modify_oldQueueDbt,sizeof(modify_oldQueueDbt));
    }



    /* Create new profile for the user entry */
     if (entry_i->shaper_enable == ENET_TRUE) { //enable
            MEA_OS_memset(&shaperDbt,0,sizeof(shaperDbt));
            shaperDbt.CIR           = newQueueDbt.dbt_public.shaper_info.CIR;
    		shaperDbt.CBS           = newQueueDbt.dbt_public.shaper_info.CBS;
		    shaperDbt.type          = newQueueDbt.dbt_public.shaper_info.resolution_type;
		    shaperDbt.overhead      = newQueueDbt.dbt_public.shaper_info.overhead;
		    shaperDbt.Cell_Overhead = newQueueDbt.dbt_public.shaper_info.Cell_Overhead;
            shaperDbt.mode = newQueueDbt.dbt_public.shaper_info.mode;

            MEA_OS_strcpy(shaperDbt.name,"");
            MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
            shaperKeyDbt.entity_type   = MEA_SHAPER_SUPPORT_CLUSTER_TYPE; 
            shaperKeyDbt.acm_mode      = 0;
            shaperKeyDbt.id            = ENET_PLAT_GENERATE_NEW_ID;
		    if (enet_AddOwner_Shaper_Profile(unit_i,&shaperKeyDbt,&shaperDbt) == ENET_ERROR) {
			    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				  			      "%s - can not create shaper profile for queue %d\n",
							      __FUNCTION__,id_i);
			    return MEA_ERROR;
		     }
             entry_i->shaper_Id               = shaperKeyDbt.id;
		     newQueueDbt.dbt_public.shaper_Id = shaperKeyDbt.id;
     }

    if(MEA_Platform_Get_ShaperPriQueueType_support() == MEA_TRUE){
        for(PriQueue=0;PriQueue<8;PriQueue++){
            if (entry_i->pri_queues[PriQueue].shaperPri_enable == MEA_TRUE) {
            MEA_OS_memset(&shaperDbt,0,sizeof(shaperDbt));
            shaperDbt.CIR  = newQueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.CIR;
	        shaperDbt.CBS  = newQueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.CBS;
	        shaperDbt.type = newQueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.resolution_type;
	        shaperDbt.overhead = newQueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.overhead;
	        shaperDbt.Cell_Overhead = newQueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.Cell_Overhead; 
            shaperDbt.mode = newQueueDbt.dbt_public.pri_queues[PriQueue].shaper_info.mode; 
            MEA_OS_strcpy(shaperDbt.name,"");
            MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
            shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE;
            shaperKeyDbt.acm_mode    = 0;
            shaperKeyDbt.id          = ENET_PLAT_GENERATE_NEW_ID;
	        if (enet_AddOwner_Shaper_Profile(unit_i,&shaperKeyDbt,&shaperDbt) == ENET_ERROR) {
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						          "%s - can not create shaper profile for PriQueue %d\n",
						          __FUNCTION__,id_i);
                for(tmp_PriQueue=0;tmp_PriQueue<PriQueue;tmp_PriQueue++) {
                    if (newQueueDbt.dbt_public.pri_queues[tmp_PriQueue].shaperPri_enable == MEA_TRUE) {
                        MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
                        shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE;
                        shaperKeyDbt.acm_mode    = 0;
                        shaperKeyDbt.id          = newQueueDbt.dbt_public.pri_queues[tmp_PriQueue].shaper_Id;
  		                enet_DelOwner_Shaper_Profile(unit_i,&shaperKeyDbt);
                    }
                }
            	ENET_Delete_GroupElement(unit_i,
                                         enet_Queue_group_id[unit_i],
                                         internalQid);
		        return MEA_ERROR;
            }
            entry_i->pri_queues[PriQueue].shaper_Id               = shaperKeyDbt.id;
            newQueueDbt.dbt_public.pri_queues[PriQueue].shaper_Id = shaperKeyDbt.id;
         
        }
    }
}




    
    /* update the device */
	if (enet_UpdateDevice_Queue
		      (unit_i,
		       internalQid,
			   &newQueueDbt,
               &oldQueueDbt) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_UpdateDevice_Queue failed (Queue id=%d)\n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	} 
   enet_xPermission_reEnableCluster(unit_i,id_i); 

    /* delete old shaper profile */
    //if (oldQueueDbt.dbt_public.shaper_enable) {
   if (old_Shaper_valid){
       

        MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
        shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_CLUSTER_TYPE;
        shaperKeyDbt.acm_mode    = 0;
        //shaperKeyDbt.id          = oldQueueDbt.dbt_public.shaper_Id;
        shaperKeyDbt.id            = old_sheperId;
        if (enet_DelOwner_Shaper_Profile(unit_i,&shaperKeyDbt) == ENET_ERROR) {
	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			  		          "%s - can not delete shaper profile for queue %d\n",
					          __FUNCTION__,id_i);
	        return MEA_ERROR;
        }
    }
    
    if(MEA_Platform_Get_ShaperPriQueueType_support() == MEA_TRUE){
            for(PriQueue=0;PriQueue<ENET_MAX_NUMBER_OF_PRI_QUEUE;PriQueue++){
                if (oldQueueDbt.dbt_public.pri_queues[PriQueue].shaperPri_enable == MEA_TRUE) {
                    MEA_OS_memset(&shaperKeyDbt,0,sizeof(shaperKeyDbt));
                    shaperKeyDbt.entity_type = MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE;
                    shaperKeyDbt.acm_mode    = 0;
                    shaperKeyDbt.id          = oldQueueDbt.dbt_public.pri_queues[PriQueue].shaper_Id;
                    if (enet_DelOwner_Shaper_Profile(unit_i,&shaperKeyDbt) == ENET_ERROR) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - can not delete shaper profile for Pri-queue %d\n",
                              __FUNCTION__,id_i);
                               return MEA_ERROR;
                    }

                }
            }
        }

    if(enet_Cluster_Set_Update_admin(unit_i,
        internalQid,
        newQueueDbt.dbt_public.adminOn,
        newQueueDbt.dbt_private.is_fatherActive) != ENET_OK ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_Cluster_Set_Update_admin failed\n",
                __FUNCTION__);
            return MEA_ERROR;
    }

	/* set entity information */
	if (ENET_Set_GroupElement(unit_i,
		                      enet_Queue_group_id[unit_i],
							  internalQid,
		                      &newQueueDbt,
							  sizeof(newQueueDbt)) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_Set_GroupElement failed (QueueDbt id=%d)\n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	}
	/* Return to caller */
    return ENET_OK;
}







/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Get_Queue>                                             */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Get_Queue (ENET_Unit_t       unit_i,
                            ENET_QueueId_t     id_i, /* external */
                            ENET_Queue_dbt     *entry_o)
{
 	enet_Queue_dbt entry;
	ENET_Uint32 len;
	ENET_QueueId_t internalQid;

   MEA_API_LOG

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_QUEUE_VALID(unit_i,id_i);

	/* check entry_o parameter */
	ENET_ASSERT_NULL(entry_o,"entry_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

	if ((internalQid=enet_cluster_external2internal(id_i))==UNASSIGNED_QUEUE_VAL){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - in call to enet_cluster_external2internal \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}

	len=sizeof(entry);
	if (ENET_Get_GroupElement(unit_i,
		                      enet_Queue_group_id[unit_i],
							  internalQid,
		                      &entry,
							  &len) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_Get_GroupElement failed (Queue id=%d)\n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	}

	MEA_OS_memcpy(entry_o,&entry.dbt_public,sizeof(*entry_o));

	/* Return to caller */
    return ENET_OK;
}                                 
                                 

/*----------------------------------------------------------------------------*/
/*               <enet_delete_Queue>                                             */
/*----------------------------------------------------------------------------*/
static ENET_Status enet_delete_Queue (ENET_Unit_t    unit_i,
                               ENET_QueueId_t id_i) /*external */
{
    ENET_QueueId_t internalQid;
    if ((internalQid=enet_cluster_external2internal(id_i))==UNASSIGNED_QUEUE_VAL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - in call to enet_cluster_external2internal \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    /* delete the device */
    if (enet_DeleteDevice_Queue(unit_i,
        internalQid) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - enet_DeleteDevice_Queue failed (Queue id=%d)\n",
                __FUNCTION__,id_i);
            return ENET_ERROR;
    } 

    /* delete the entity */
    if (ENET_Delete_GroupElement(unit_i,
        enet_Queue_group_id[unit_i],
        internalQid) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Delete_GroupElement failed (xPermission id=%d)\n",
                __FUNCTION__,id_i);
            return ENET_ERROR;
    }
    enet_externalQidTable[id_i].isValid=ENET_FALSE;
    enet_externalQidTable[id_i].Qid=UNASSIGNED_QUEUE_VAL;

    enet_externalQid_state_Table[id_i].isValid=ENET_TRUE;
    enet_externalQid_state_Table[id_i].portState=MEA_PORT_STATE_FORWARD;

 if(MEA_GLOBAL_FORWARDER_SUPPORT_DSE){   
	if (enet_Delete_xPermission   (unit_i,id_i) != ENET_OK) { 
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - enet_Delete_xPermission failed \n",
			__FUNCTION__);
		return ENET_ERROR;
	}
}

	
	/* Return to caller */
    return ENET_OK;


}
/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_Delete_Queue>                                          */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_Delete_Queue    (ENET_Unit_t    unit_i,
                                  ENET_QueueId_t id_i) /*external */
{
    ENET_Queue_dbt     entry;
    ENET_QueueId_t internalQid=0;

    MEA_API_LOG

    



#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_QUEUE_VALID(unit_i,id_i);


	/* check for entity attributes  */
	if (ENET_IsAllowedToDelete_Queue(unit_i,
		                                    id_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_IsAllowedToDelete_Queue failed Not allow to delete (Queue id=%d)\n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	} 


#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    MEA_OS_memset(&entry, 0, sizeof(ENET_Queue_dbt));
    if (ENET_Get_Queue(unit_i, id_i, &entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_Get_Queue (Queue id=%d) \n",
            __FUNCTION__, id_i);
        return ENET_ERROR;
    }




/************************************************************************/
/* delete The shaper                                               */
/************************************************************************/
    {
        
        int i;
        

        


        entry.shaper_enable=MEA_FALSE;
        for(i=0;i<ENET_MAX_NUMBER_OF_PRI_QUEUE;i++)
        {
            entry.pri_queues[i].shaperPri_enable =MEA_FALSE;
        }
        if(ENET_drv_Set_Queue(unit_i,id_i, &entry)!=MEA_OK ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Set_Queue (Queue id=%d) \n",
                __FUNCTION__,id_i);
            return ENET_ERROR;
        }
    
    
    }
    internalQid = enet_cluster_external2internal(id_i);
    
    mea_drv_ADM_Cluster_To_port(unit_i,
        entry.port.id.port,
        entry.groupId,
        internalQid,  /* Internal */
        MEA_FALSE);

    if(enet_delete_Queue(unit_i, id_i) != MEA_OK){/*external */
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - enet_delete_Queue (Queue id=%d) \n",
            __FUNCTION__,id_i);
    return ENET_ERROR;
    }

        

  
    return ENET_OK;
}
	
                                 



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_GetFirst_Queue>                                        */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_GetFirst_Queue  (ENET_Unit_t     unit_i,
								  ENET_QueueId_t *id_o,  /* external */
                                  ENET_Queue_dbt *entry_o,
                                  ENET_Bool      *found_o)
{

	enet_Queue_dbt entry;
	ENET_Uint32 len;
	ENET_QueueId_t internalQid;

    MEA_API_LOG

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_io parameter */
	ENET_ASSERT_NULL(id_o,"id_o");

	/* check found_o parameter */
	ENET_ASSERT_NULL(found_o,"found_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


	len=sizeof(entry);
	if ( ENET_GetFirst_GroupElement   (unit_i,
	                                   enet_Queue_group_id[unit_i],
									   &internalQid,
                                       &entry,
									   &len,
                                       found_o) !=ENET_OK)	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_GetFirst_GroupElement failed \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}
	if ((found_o)&&(entry.dbt_private.external_id == UNASSIGNED_QUEUE_VAL)){
		/* skip MC entry */
		if (  ENET_GetNext_GroupElement   (unit_i,
										   enet_Queue_group_id[unit_i],
										   &internalQid,
										   &entry,
										   &len,
										   found_o)!=ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - ENET_GetNext_GroupElement failed \n",
							   __FUNCTION__);
			return ENET_ERROR;
		}
	}

	if (entry_o) {
		MEA_OS_memcpy(entry_o,&entry.dbt_public,sizeof(*entry_o));
	}
	*id_o = entry.dbt_private.external_id;

	/* Return to caller */
    return ENET_OK;
}                                 
                                 
/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_GetNext_Queue>                                         */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status ENET_GetNext_Queue   (ENET_Unit_t     unit_i,
                                  ENET_QueueId_t *id_io,
                                  ENET_Queue_dbt *entry_o,
                                  ENET_Bool      *found_o)
{
	enet_Queue_dbt groupEntry;
	ENET_Uint32 len;
	ENET_QueueId_t internalQid;

   MEA_API_LOG

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_io parameter */
	ENET_ASSERT_NULL(id_io,"id_io");

	/* check found_o parameter */
	ENET_ASSERT_NULL(found_o,"found_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */
	if ((internalQid=enet_cluster_external2internal(*id_io))==UNASSIGNED_QUEUE_VAL){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - in call to enet_cluster_external2internal \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}
	len=sizeof(groupEntry);

    if (  ENET_GetNext_GroupElement   (unit_i,
	                                   enet_Queue_group_id[unit_i],
									   &internalQid,
                                       &groupEntry,
									   &len,
                                       found_o)!=ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - ENET_GetNext_GroupElement failed \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}
	if ((found_o)&&(groupEntry.dbt_private.external_id == UNASSIGNED_QUEUE_VAL)){
		/* skip MC entry */
		if (  ENET_GetNext_GroupElement   (unit_i,
										   enet_Queue_group_id[unit_i],
										   &internalQid,
										   &groupEntry,
										   &len,
										   found_o)!=ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - ENET_GetNext_GroupElement failed \n",
							   __FUNCTION__);
			return ENET_ERROR;
		}
	}
	if (entry_o) {
		MEA_OS_memcpy(entry_o,&groupEntry.dbt_public,sizeof(*entry_o));
	}
	*id_io =groupEntry.dbt_private.external_id;
	/* Return to caller */
    return ENET_OK;

}     

MEA_Status ENET_Delete_all_Queues(ENET_Unit_t     unit_i)
{

    ENET_QueueId_t queueId;
    ENET_QueueId_t next_queueId;
#if 0
    ENET_Bool      found;
    ENET_Queue_dbt entry;

    if (ENET_GetFirst_Queue(unit_i,
        &queueId,
        &entry,
        &found) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_GetFirst_Queue failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    while (found) {
        next_queueId = queueId;
        if (ENET_GetNext_Queue(unit_i,
            &next_queueId,
            &entry,
            &found) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_GetNext_Queue failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        if (ENET_IsAllowedToDelete_Queue(unit_i, queueId) == MEA_OK){
            if (ENET_Delete_Queue(unit_i,
                queueId) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_Delete_Queue failed \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }
        queueId = next_queueId;
    }
#else
    for (queueId = 0 ; queueId <= ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; queueId++){
        
        next_queueId = ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT - queueId;
        if (ENET_IsValid_Queue(unit_i, next_queueId,/*external*/ MEA_TRUE) == MEA_FALSE)
            continue;

        if (ENET_IsAllowedToDelete_Queue(unit_i, next_queueId) == MEA_OK){
            //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "Delete_Queue %d\n", next_queueId);
        if (ENET_Delete_Queue(unit_i,
            next_queueId) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_Delete_Queue failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }
    if (next_queueId == 0)
          break;
}
#endif
    /* Return to caller */
    return MEA_OK;
}
/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_GetIDByName_Queue>                                         */
/*                                                                            */
/*----------------------------------------------------------------------------*/

  ENET_Status ENET_GetIDByName_Queue (ENET_Unit_t                unit_i,					
									 char                       *name_i,					
						  		     ENET_QueueId_t             *id_o,	/*external*/					
								     ENET_Bool				    *found_o)					
{

	enet_Queue_dbt groupEntry;
	ENET_Uint32 len;
	ENET_QueueId_t internalQid;

   MEA_API_LOG

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check name_i parameter */
	ENET_ASSERT_NULL(name_i,"name_i");
	
    ENET_ASSERT_VALID_NAME_LEN(name_i,groupEntry.dbt_public.name);
    
	/* check id_o parameter */
	ENET_ASSERT_NULL(id_o,"id_o");

	/* check found_o parameter */
	ENET_ASSERT_NULL(found_o,"found_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */
   len=sizeof(groupEntry);
   if (ENET_GetFirst_GroupElement  (unit_i,
	                                enet_Queue_group_id[unit_i],
	 							    &internalQid,
                                    &groupEntry,
									&len,
                                    found_o) != ENET_OK) {
	   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                  "%s - ENET_GetFirst_GroupElement failed \n",
						  __FUNCTION__);
	   return ENET_ERROR;
   }

   while (*found_o) {

	   if(MEA_OS_strcmp(groupEntry.dbt_public.name,name_i) == 0) {
			*found_o = ENET_TRUE;
			*id_o = groupEntry.dbt_private.external_id;
			return ENET_OK;
	   }
       len=sizeof(groupEntry);
       if (ENET_GetNext_GroupElement  (unit_i,
	                                    enet_Queue_group_id[unit_i],
	 	  						        id_o,
                                        &groupEntry,
									    &len,
                                        found_o) != ENET_OK) {
	       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		                      "%s - ENET_GetFirst_GroupElement failed \n",
			    			  __FUNCTION__);
	       return ENET_ERROR;
	   }
   }

    *found_o = ENET_FALSE;
	*id_o = groupEntry.dbt_private.external_id;
	/* Return to caller */
    return ENET_OK;
}                                    
                                                             


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <ENET_IsValid_Queue>                                         */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Bool  ENET_IsValid_Queue(ENET_Unit_t    unit_i,
 							  ENET_QueueId_t id_i,/*external*/
							  ENET_Bool      silent_i)
{

	if (!IS_QUEUE_ID_VALID(id_i)){
    	if (!silent_i) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - Qid is out of range (Queue id=%d)\n",
							   __FUNCTION__,id_i);
		}
		return ENET_FALSE;
	} 	
	if (!enet_externalQidTable[id_i].isValid){
		if (!silent_i) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - 1 Qid is not created (Queue id=%d)\n",
							   __FUNCTION__,id_i);
		}
		return ENET_FALSE;
	} 	

#if 0
	if (ENET_IsValid_GroupElement(unit_i,
		                          enet_Queue_group_id[unit_i],
								  id_i,
								  ENET_TRUE) != ENET_TRUE) {

  	  if (!silent_i) {
  		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - Unit/Queue %d/%d is invalid\n",
							 __FUNCTION__,unit_i,id_i);
		  
      }

  	  return ENET_FALSE;  
    }
#endif    
    return ENET_TRUE;
}


void	enet_Debug_GetInfo  (ENET_Uint32 index, 
								 ENET_Bool *isIndexValid,
								 ENET_Uint32 *portId,
								 ENET_Bool   *isPortEnable,
								 ENET_Uint32 *internalQueueId,
								 ENET_Uint32 *externalQueueId,
								 ENET_Uint32 *location,
								 ENET_Bool *isPortIndex,
								 ENET_Bool *isLastIndex)
{
	ENET_Uint32 local_index = index;
	enet_Queue_dbt entry;
	ENET_Uint32 len;


	if (index >=REVERSE_MAPPING_TABLE_SIZE-1)
	{
		*isLastIndex = ENET_TRUE;
		local_index = REVERSE_MAPPING_TABLE_SIZE-1;
	}
	if (enet_Hw_portReverseMappingData[local_index]==UNASSIGNED_QUEUE_VAL)
		*isIndexValid = ENET_FALSE;
	else
	{
		*isIndexValid = ENET_TRUE;
		*internalQueueId = enet_Hw_portReverseMappingData[local_index];
		*portId  = enet_Hw_Cluster2Port[*internalQueueId].PortNumber;
		*location = enet_Hw_Cluster2Port[*internalQueueId].ClusterLocation;
        *isPortIndex =  (enet_Hw_portReverseMappingIndx[*portId].location==local_index)?ENET_TRUE:ENET_FALSE;
		len=sizeof(entry);
		if (ENET_Get_GroupElement(ENET_UNIT_0,
								  enet_Queue_group_id[ENET_UNIT_0],
								  (ENET_GroupElementId_t)*internalQueueId,
								  &entry,
								  &len) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - ENET_Get_GroupElement failed (Queue id=%d)\n",
							   __FUNCTION__,*internalQueueId);
			return ;
		}		
		*isPortEnable = entry.dbt_private.is_fatherActive;
		*externalQueueId = entry.dbt_private.external_id;
	}	
}



ENET_QueueId_t QueId_External2Internal(ENET_QueueId_t ext_Qid) 
{
	if (ext_Qid>ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT)
		return(UNASSIGNED_QUEUE_VAL);
	if (!enet_externalQidTable[ext_Qid].isValid)
		return(UNASSIGNED_QUEUE_VAL);

	return((ENET_QueueId_t)enet_externalQidTable[ext_Qid].Qid);
}

ENET_QueueId_t QueId_Internal2External(ENET_QueueId_t internal_Qid) 
{
    ENET_QueueId_t  ext_Qid;

//        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
//                          "%s - QueId_Internal2External is not supported\n",
//                          __FUNCTION__,internal_Qid);

    if (internal_Qid >= ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT)
        return(UNASSIGNED_QUEUE_VAL);

    for (ext_Qid = 0; ext_Qid < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; ext_Qid++){
        if (!enet_externalQidTable[ext_Qid].isValid)
            continue;
        if (enet_externalQidTable[ext_Qid].Qid == internal_Qid){
            return ext_Qid;
        }
    
    }

	return(UNASSIGNED_QUEUE_VAL);
}

/********************************** PriQueueSize_Info Table *******************************/

ENET_Status ENET_Get_Internal_PriQueueSize_Info(ENET_Unit_t               unit,
									   ENET_QueueId_t            id_i,/*internal*/
									   ENET_PriQueueId_t         pri_id_i,
									   ENET_PriQueueSize_Info_t *info)
{

	ENET_ind_read_t ind_read;
	ENET_Uint32 read_data[2];

	

	

	/* Check for valid queue parameter */
	if (pri_id_i >= ENET_MAX_NUMBER_OF_PRI_QUEUE) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Invalid priQueue(%d) (Queue=%d) \n",
			__FUNCTION__,pri_id_i,id_i);
		return ENET_ERROR;
	}

	if(id_i > ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		"%s - Queue=%d) out of range\n",
		__FUNCTION__,id_i);
	return ENET_ERROR;
	}

	/* build default ind_read structure */
	ind_read.cmdReg			=	ENET_BM_IA_CMD;      
	ind_read.cmdMask		=	ENET_BM_IA_CMD_MASK;      
	ind_read.statusReg		=	ENET_BM_IA_STAT;   
	ind_read.statusMask		=	ENET_BM_IA_STAT_MASK;   
	ind_read.tableAddrReg	=	ENET_BM_IA_ADDR;
	ind_read.tableAddrMask	=	ENET_BM_IA_ADDR_OFFSET_MASK;
	ind_read.tableOffset	=   (ENET_MAX_NUMBER_OF_PRI_QUEUE*id_i)+pri_id_i;
	ind_read.read_data	    =   read_data;


	/* read current queue size */ 
	ind_read.tableType		=   ENET_BM_TBL_TYP_CURRENT_Q_SIZE;
	if (ENET_ReadIndirect(ENET_UNIT_0,&ind_read,
		ENET_NUM_OF_ELEMENTS(read_data),
		ENET_MODULE_BM)!=ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - Read from hw failed tblType=%d tblOffset=%d "
				"(port=%d queue=%d)\n",
				__FUNCTION__,
				ind_read.tableType,
				ind_read.tableOffset,
				id_i,
				pri_id_i);
			
			return ENET_ERROR;
	}
	info->currentPacket = ((read_data[0] & MEA_CURRENT_Q_SIZE_MASK) 
		>> MEA_OS_calc_shift_from_mask(MEA_CURRENT_Q_SIZE_MASK));


	/* read average queue size */ 
	ind_read.tableType		=   ENET_BM_TBL_TYP_EGRESS_PORT_MQS;	
	if (ENET_ReadIndirect(unit,&ind_read,
		ENET_NUM_OF_ELEMENTS(read_data),
		ENET_MODULE_BM)!=ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - Read from hw failed tblType=%d tblOffset=%d "
				"(port=%d queue=%d)\n",
				__FUNCTION__,
				ind_read.tableType,
				ind_read.tableOffset,
				id_i,
				pri_id_i);
			
			return ENET_ERROR;
	}

	info->currentBytes   = ((read_data[0] & MEA_EGRESS_PORT_CQSB_MASK) 
		>> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_CQSB_MASK));
	info->currentBytes=info->currentBytes*64;





return MEA_OK;
}



ENET_Status ENET_Get_PriQueueSize_Info(ENET_Unit_t               unit,
                                       ENET_QueueId_t            id_i,/*external*/
                                       ENET_PriQueueId_t         pri_id_i,
                                       ENET_PriQueueSize_Info_t *info)
{

#define MQS_MSB_MASK				0x800
#define MQS_REG_LEN 				12
#define VA_Q_SIZE_REG_MSB_BYTE_MASK	0x7F80  

	ENET_ind_read_t ind_read;
	ENET_Uint32 read_data[2];
	ENET_QueueId_t internalQid;
    mea_memory_mode_e originalMode;
	
   MEA_API_LOG

       if (MEA_reinit_start) {
           if(info!= NULL)
           info->currentPacket = 0;
           return MEA_OK;
       }

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid port parameter */
	if (ENET_IsValid_Queue(unit,id_i,ENET_TRUE)==ENET_FALSE) { 
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Queue %d is not valid\n",
                         __FUNCTION__,id_i);
       return ENET_ERROR;
    }

    /* Check for valid queue parameter */
    if (pri_id_i >= ENET_MAX_NUMBER_OF_PRI_QUEUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid priQueue(%d) (Queue=%d) \n",
                         __FUNCTION__,pri_id_i,id_i);
       return ENET_ERROR;
    }

    /* Check for valid entry parameter */
    if (info == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - info == NULL (Queue=%d , priQueue=%d)\n",
                         __FUNCTION__,id_i,pri_id_i);
       return ENET_ERROR;
    }


#endif 	/* MEA_SW_CHECK_INPUT_PARAMETERS */
	if ((internalQid=enet_cluster_external2internal(id_i))==UNASSIGNED_QUEUE_VAL){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - in call to enet_cluster_external2internal \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}

    originalMode =globalMemoryMode; 
	if (globalMemoryMode == MEA_MODE_REGULAR_ENET4000){
		ENET_Queue_dbt Queue;
		if (ENET_Get_Queue(ENET_UNIT_0,id_i,&Queue)!= ENET_OK){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - ENET_Get_Queue failed )id=%d)\n",
			  			      __FUNCTION__,id_i);
		    return ENET_ERROR;
		}
		if (Queue.port.id.port == 118 || (Queue.port.id.port == 24)) {
		    globalMemoryMode = MEA_MODE_UPSTREEM_ONLY;
		} else {
		    globalMemoryMode = MEA_MODE_DOWNSTREEM_ONLY;
		}
    }


    /* build default ind_read structure */
	ind_read.cmdReg			=	ENET_BM_IA_CMD;      
	ind_read.cmdMask		=	ENET_BM_IA_CMD_MASK;      
	ind_read.statusReg		=	ENET_BM_IA_STAT;   
	ind_read.statusMask		=	ENET_BM_IA_STAT_MASK;   
	ind_read.tableAddrReg	=	ENET_BM_IA_ADDR;
	ind_read.tableAddrMask	=	ENET_BM_IA_ADDR_OFFSET_MASK;
	ind_read.tableOffset	=   (ENET_MAX_NUMBER_OF_PRI_QUEUE*internalQid)+pri_id_i;
	ind_read.read_data	    =   read_data;


    /* read current queue size */ 
	ind_read.tableType		=   ENET_BM_TBL_TYP_CURRENT_Q_SIZE;
	if (ENET_ReadIndirect(ENET_UNIT_0,&ind_read,
                             ENET_NUM_OF_ELEMENTS(read_data),
                             ENET_MODULE_BM)!=ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Read from hw failed tblType=%d tblOffset=%d "
                          "(port=%d queue=%d)\n",
                          __FUNCTION__,
                          ind_read.tableType,
                          ind_read.tableOffset,
                          id_i,
                          pri_id_i);
        globalMemoryMode = originalMode;
		return ENET_ERROR;
    }
	info->currentPacket = ((read_data[0] & MEA_CURRENT_Q_SIZE_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_CURRENT_Q_SIZE_MASK));
    

    /* read average queue size */ 
	ind_read.tableType		=   ENET_BM_TBL_TYP_EGRESS_PORT_MQS;	
	if (ENET_ReadIndirect(unit,&ind_read,
                             ENET_NUM_OF_ELEMENTS(read_data),
                             ENET_MODULE_BM)!=ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Read from hw failed tblType=%d tblOffset=%d "
                          "(port=%d queue=%d)\n",
                          __FUNCTION__,
                          ind_read.tableType,
                          ind_read.tableOffset,
                          id_i,
                          pri_id_i);
        globalMemoryMode = originalMode;
  		return ENET_ERROR;
    }

	info->currentBytes   = ((read_data[0] & MEA_EGRESS_PORT_CQSB_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_CQSB_MASK));
	info->currentBytes=info->currentBytes*64;


#if 1
	{
 	  enet_Queue_dbt entry;
	  ENET_Uint32 len;
	  ENET_QueueId_t internalQid;

  	  if ((internalQid=enet_cluster_external2internal(id_i))==UNASSIGNED_QUEUE_VAL){
	 	  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	               "%s - in call to enet_cluster_external2internal \n",
						   __FUNCTION__);
          globalMemoryMode = originalMode;
		  return ENET_ERROR;
	  }

	  len=sizeof(entry);
	  if (ENET_Get_GroupElement(unit,
	  	                        enet_Queue_group_id[unit],
							    internalQid,
		                        &entry,
							    &len) != ENET_OK) {
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		  	                "%s - ENET_Get_GroupElement failed (Queue id=%d)\n",
						    __FUNCTION__,id_i);
          globalMemoryMode = originalMode;
		  return ENET_ERROR;
	   }

	   info->ActualMqsByte   = entry.dbt_public.pri_queues[pri_id_i].max_q_size_Byte;
	   info->ActualMqsPacket = entry.dbt_public.pri_queues[pri_id_i].max_q_size_Packets;

       if(info->ActualMqsByte <= 16000){
          info->currentBytes=info->currentBytes/64;
       }

	   info->ActualMtu       = entry.dbt_public.pri_queues[pri_id_i].mtu;
	}
#else 
	{
	ENET_Uint32 tmp;
	info->ActualMqsPacket = ((read_data[0] & MEA_EGRESS_PORT_MQSP_MULTIPLIER_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSP_MULTIPLIER_MASK));
	info->ActualMqsPacket = info->ActualMqsPacket*4;
	info->ActualMqsPacket = 1<<info->ActualMqsPacket;
	tmp   = ((read_data[0] & MEA_EGRESS_PORT_MQSP_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSP_MASK));
	info->ActualMqsPacket= info->ActualMqsPacket*tmp;

	info->ActualMqsByte = ((read_data[0] & MEA_EGRESS_PORT_MQSB_MULTIPLIER_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSB_MULTIPLIER_MASK));
	info->ActualMqsByte = info->ActualMqsByte*4;
	info->ActualMqsByte = 1<<info->ActualMqsByte;
	tmp   = ((read_data[0] & MEA_EGRESS_PORT_MQSB_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSB_MASK));
	info->ActualMqsByte= info->ActualMqsByte*tmp*64;
	}
#endif


    globalMemoryMode = originalMode;

    /* Return to caller */
    return MEA_OK;

}




ENET_Status ENET_Get_realQueueMC_MQS_Size_Info_drv(ENET_Unit_t               unit,
                                               ENET_QueueId_t            id_i,/*external*/
                                               ENET_PriQueueId_t         pri_id_i,
                                               ENET_Queue_MC_MQS_Size_Info_t *info)

{
    ENET_ind_read_t ind_read;
    ENET_Uint32 read_data[1];
    ENET_QueueId_t internalQid;
    

    MEA_API_LOG



#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check for valid port parameter */
        if (ENET_IsValid_Queue(unit,id_i,ENET_TRUE)==ENET_FALSE) { 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Queue %d is not valid\n",
                __FUNCTION__,id_i);
            return ENET_ERROR;
        }

        /* Check for valid entry parameter */
        if (info == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - info == NULL (Queue=%d )\n",
                __FUNCTION__,id_i);
            return ENET_ERROR;
        }
        /* Check for valid queue parameter */
        if (pri_id_i >= ENET_MAX_NUMBER_OF_PRI_QUEUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Invalid priQueue(%d) (Queue=%d) \n",
                __FUNCTION__,pri_id_i,id_i);
            return ENET_ERROR;
        }


#endif 	/* MEA_SW_CHECK_INPUT_PARAMETERS */

    if(!MEA_QUEUE_MC_MQS_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - QUEUE MC MQS NO SUPPORT\n",__FUNCTION__);
        return MEA_ERROR;
    }
        internalQid=enet_cluster_external2internal(id_i);


        /* build default ind_read structure */
        ind_read.cmdReg			=	ENET_BM_IA_CMD;      
        ind_read.cmdMask		=	ENET_BM_IA_CMD_MASK;      
        ind_read.statusReg		=	ENET_BM_IA_STAT;   
        ind_read.statusMask		=	ENET_BM_IA_STAT_MASK;   
        ind_read.tableAddrReg	=	ENET_BM_IA_ADDR;
        ind_read.tableAddrMask	=	ENET_BM_IA_ADDR_OFFSET_MASK;
        ind_read.tableOffset	=   (internalQid * ENET_MAX_NUMBER_OF_PRI_QUEUE)+pri_id_i;
        ind_read.read_data	    =   read_data;


        /* read current queue size */ 
        ind_read.tableType		=   ENET_BM_TBL_TYP_CLUSTER_BC_MQS;
        if (ENET_ReadIndirect(ENET_UNIT_0,&ind_read,
            ENET_NUM_OF_ELEMENTS(read_data),
            ENET_MODULE_BM)!=ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Read from hw failed tblType=%d tblOffset=%d "
                    "queue=%d)\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    id_i );
                
                return ENET_ERROR;
        }

        info->configure_MC_MQS   = (read_data[0]     & 0x000001FF); 
        info->current_MC_MQS     =((read_data[0]>>9) & 0x000001FF); ;

        /* Return to caller */
        return MEA_OK;




}


ENET_Status ENET_Get_realPriQueueSize_Info_drv(ENET_Unit_t               unit,
                                       ENET_QueueId_t            id_i,/*external*/
                                       ENET_PriQueueId_t         pri_id_i,
                                       ENET_PriQueueSize_Info_t *info)
{

#define MQS_MSB_MASK				0x800
#define MQS_REG_LEN 				12
#define VA_Q_SIZE_REG_MSB_BYTE_MASK	0x7F80  

	ENET_ind_read_t ind_read;
	ENET_Uint32 read_data[2];
	ENET_Uint32 tmp;
	






    /* build default ind_read structure */
	ind_read.cmdReg			=	ENET_BM_IA_CMD;      
	ind_read.cmdMask		=	ENET_BM_IA_CMD_MASK;      
	ind_read.statusReg		=	ENET_BM_IA_STAT;   
	ind_read.statusMask		=	ENET_BM_IA_STAT_MASK;   
	ind_read.tableAddrReg	=	ENET_BM_IA_ADDR;
	ind_read.tableAddrMask	=	ENET_BM_IA_ADDR_OFFSET_MASK;
	ind_read.tableOffset	=   (ENET_MAX_NUMBER_OF_PRI_QUEUE*id_i)+pri_id_i;
	ind_read.read_data	    =   read_data;


    /* read current queue size */ 
	ind_read.tableType		=   ENET_BM_TBL_TYP_CURRENT_Q_SIZE;
	if (ENET_ReadIndirect(ENET_UNIT_0,&ind_read,
                             ENET_NUM_OF_ELEMENTS(read_data),
                             ENET_MODULE_BM)!=ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Read from hw failed tblType=%d tblOffset=%d "
                          "(port=%d queue=%d)\n",
                          __FUNCTION__,
                          ind_read.tableType,
                          ind_read.tableOffset,
                          id_i,
                          pri_id_i);
		return ENET_ERROR;
    }
	info->currentPacket = ((read_data[0] & MEA_CURRENT_Q_SIZE_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_CURRENT_Q_SIZE_MASK));
    

    /* read average queue size */ 
	ind_read.tableType		=   ENET_BM_TBL_TYP_EGRESS_PORT_MQS;	
	if (ENET_ReadIndirect(unit,&ind_read,
                             ENET_NUM_OF_ELEMENTS(read_data),
                             ENET_MODULE_BM)!=ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Read from hw failed tblType=%d tblOffset=%d "
                          "(port=%d queue=%d)\n",
                          __FUNCTION__,
                          ind_read.tableType,
                          ind_read.tableOffset,
                          id_i,
                          pri_id_i);
		return ENET_ERROR;
    }

	info->currentBytes   = ((read_data[0] & MEA_EGRESS_PORT_CQSB_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_CQSB_MASK));
	info->currentBytes=info->currentBytes*64;


	info->ActualMqsPacket = ((read_data[0] & MEA_EGRESS_PORT_MQSP_MULTIPLIER_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSP_MULTIPLIER_MASK));
    if (info->ActualMqsPacket == 0)
	  info->ActualMqsPacket = 1;
    else if (info->ActualMqsPacket == 1)
        info->ActualMqsPacket = 16;
    else if (info->ActualMqsPacket == 2)
        info->ActualMqsPacket = 256;
    else{
        info->ActualMqsPacket = 4096;
    }



	tmp   = ((read_data[0] & MEA_EGRESS_PORT_MQSP_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSP_MASK));
	info->ActualMqsPacket= info->ActualMqsPacket*tmp;

	info->ActualMqsByte = ((read_data[0] & MEA_EGRESS_PORT_MQSB_MULTIPLIER_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSB_MULTIPLIER_MASK));
    if (info->ActualMqsByte == 0)
    {
        info->ActualMqsByte = 64;
    }
    else if (info->ActualMqsByte == 1)
    {
        info->ActualMqsByte = 1024;
    }
    else if (info->ActualMqsByte == 2)
    {
        info->ActualMqsByte = 16384;
    }
    else
    {
        info->ActualMqsByte = 262144;
    }


	tmp   = ((read_data[0] & MEA_EGRESS_PORT_MQSB_MASK) 
                          >> MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSB_MASK));


	info->ActualMqsByte= info->ActualMqsByte*tmp*64;



    /* Return to caller */
    return MEA_OK;

}






/********************************** MQS Table *******************************/

/*---------------------------------------------------------------------------*/
/*            <enet_queue_UpdateHwEntryMQS>                                  */
/*---------------------------------------------------------------------------*/
static void enet_queue_UpdateHwEntryMQS(enet_Queue_dbt *entry,
										ENET_PriQueueId_t           priority)
{
    ENET_Uint32 entryVal = 0;
    ENET_Uint32 MqsByte,byteMultiplier;
    ENET_Uint32 MqsPacket,packetMultiplier;
	ENET_Mqs_t  userMqs_byte   =entry->dbt_public.pri_queues[priority].max_q_size_Byte ;
	ENET_Mqs_t  userMqs_packet =entry->dbt_public.pri_queues[priority].max_q_size_Packets ;

    if (entry->dbt_private.is_fatherActive==MEA_FALSE && entry->dbt_private.is_father1p1 == MEA_EGRESS_PROTECT_BYPASS){
		userMqs_packet = 0;
        userMqs_byte   = 0;
    }





#if 0
	packetMultiplier = MEA_OS_calc_Number_of_bits(userMqs_packet);
	if (packetMultiplier!=0) 
		packetMultiplier=packetMultiplier-1;
	packetMultiplier = packetMultiplier / 4;
	MqsPacket = userMqs_packet/(1<<(packetMultiplier*4));

	/* select the upper close and not the lower close */
	if ((ENET_Mqs_t)(MqsPacket*(1<<(packetMultiplier*4))) < userMqs_packet)
	{
		if (MqsPacket<15)
			MqsPacket =MqsPacket+1;
		else
		{
			packetMultiplier=packetMultiplier+1;
	        MqsPacket = 1;
		}
	}
#else
    if ((userMqs_packet & 0xFFF0) == 0){
        packetMultiplier = 0;
        MqsPacket=userMqs_packet / 1;
    }
    else if ((userMqs_packet & 0xFF00) == 0){
        packetMultiplier = 1;
        MqsPacket = userMqs_packet / 16;
    }
    else if ((userMqs_packet & 0xF000) == 0){
        packetMultiplier = 2;
        MqsPacket = userMqs_packet / 256;
    }
    else{
        packetMultiplier = 3;
        MqsPacket = userMqs_packet / 4096;
    
    }


#endif
#if 0
	byteMultiplier = MEA_OS_calc_Number_of_bits(userMqs_byte/64);
	if (byteMultiplier!=0) 
		byteMultiplier=byteMultiplier-1;

	byteMultiplier = byteMultiplier / 4;
	MqsByte = userMqs_byte/(64*(1<<(byteMultiplier*4)));
	/* select the upper close and not the lower close */
	if ((ENET_Mqs_t)(MqsByte*(64*(1<<(byteMultiplier*4)))) < userMqs_byte)
	{
		if (MqsByte<15)
			MqsByte =MqsByte+1;
		else
		{
			byteMultiplier=byteMultiplier+1;
	        MqsByte = 1;
		}
	}
#else
    if (((userMqs_byte / 64) & 0xFFF0) == 0)
    {
        byteMultiplier = 0;
        MqsByte = userMqs_byte / 64;
    }
    else if (((userMqs_byte / 64) & 0xFF00) == 0)
    {
        byteMultiplier = 1;
        MqsByte = userMqs_byte / 1024;
    }
    else if (((userMqs_byte / 64) & 0xF000) == 0)
    {
        byteMultiplier = 2;
        MqsByte = userMqs_byte / 16384;
    }
    else
    {
        byteMultiplier = 3;
        MqsByte = userMqs_byte / 262144;
    }



#endif
	entryVal  = ( MqsPacket<<
                 MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSP_MASK));
	entryVal  |= (( packetMultiplier  <<
                 MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSP_MULTIPLIER_MASK)) 
				  & MEA_EGRESS_PORT_MQSP_MULTIPLIER_MASK);
	entryVal  |= (( MqsByte  <<
                 MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSB_MASK)) 
				  & MEA_EGRESS_PORT_MQSB_MASK);
	entryVal  |= (( byteMultiplier  <<
                 MEA_OS_calc_shift_from_mask(MEA_EGRESS_PORT_MQSB_MULTIPLIER_MASK)) 
				  & MEA_EGRESS_PORT_MQSB_MULTIPLIER_MASK);


 	ENET_WriteReg(ENET_UNIT_0,ENET_BM_IA_WRDATA0,entryVal,ENET_MODULE_BM);
}



static void enet_queue_UpdateHwEntryMTU(enet_Queue_dbt *entry)
{
    MEA_Uint32 entryVal;

	entryVal  = (entry->dbt_public.MTU << MEA_OS_calc_shift_from_mask(MEA_QUEUE_MTU_SIZE_MASK));

 	MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,entryVal,MEA_MODULE_BM);
}

static void enet_queue_UpdateHwEntry_mc_MQS(MEA_Uint32 mc_mqs ,MEA_Bool is_fatherActive)
{
    MEA_Uint32 set_mc_mqs;
#if 1	
    if (is_fatherActive == MEA_FALSE) {
        set_mc_mqs = 0;
    }
    else {
        set_mc_mqs = mc_mqs;
    }
#else
       set_mc_mqs = mc_mqs;
	
#endif	

    MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0, set_mc_mqs,MEA_MODULE_BM);
}

static void enet_queue_UpdateHwEntryMTU_Pri_queue(MEA_Uint32 mtu)
{
    MEA_Uint32 entryVal;
    entryVal  = ((mtu/MEA_QUEUE_MTU_PRIQUEUE_GRANULARITY_VALUE) <<  MEA_OS_calc_shift_from_mask(MEA_QUEUE_MTU_SIZE_MASK));

	MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,entryVal,MEA_MODULE_BM);
}

 
static ENET_Status enet_Cluster_Set_Update_admin(ENET_Unit_t           unit_i,
                                             ENET_QueueId_t            queue_internal,
                                             ENET_Bool                 adminOn ,
                                             MEA_Bool                  is_fatherActive)
{

    MEA_Uint32 val=0;

    
        val=queue_internal;
        val|=(adminOn)<<8;
        if(MEA_QUEUE_ADMINON_SUPPORT){
            MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_CLUSTERS_ADMIN_REG,val,MEA_MODULE_IF);
        }
   

    return MEA_OK;
}

#if 0
static ENET_Status enet_Cluster_Update_admin(ENET_Unit_t                unit_i,
                          ENET_QueueId_t            queue,
                          enet_Queue_dbt            *entry,
                          enet_Queue_dbt            *old_entry)
{

    MEA_Uint32 val;

    if ((!enet_Queue_InitDone[unit_i])  ||
        (old_entry == NULL) ||
        (entry->dbt_public.adminOn !=old_entry->dbt_public.adminOn) )
    {
        val=queue;
        val|=(entry->dbt_public.adminOn)<<8;
        if(MEA_QUEUE_ADMINON_SUPPORT){
            MEA_API_WriteReg(MEA_UNIT_0,ENET_IF_CLUSTERS_ADMIN_REG,val,MEA_MODULE_IF);
        }
    }

    return MEA_OK;
}
#endif
static ENET_Status enet_queue_UpdateHw_mc_MQS(ENET_Unit_t                unit_i,
                                          ENET_QueueId_t            queue,/*internal*/
                                          enet_Queue_dbt            *entry,
                                          enet_Queue_dbt            *old_entry)
{
    ENET_ind_write_t  ind_write;
    MEA_Uint32        pri;




    
        if(MEA_QUEUE_MC_MQS_SUPPORT){
            /* build the ind_write value */
            ind_write.tableType		= ENET_BM_TBL_TYP_CLUSTER_BC_MQS;
            ind_write.tableOffset	= queue;
            ind_write.cmdReg		= MEA_BM_IA_CMD;      
            ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
            ind_write.statusReg		= MEA_BM_IA_STAT;   
            ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
            ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
            ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
            ind_write.writeEntry    = (MEA_FUNCPTR)enet_queue_UpdateHwEntry_mc_MQS;

            for(pri=0;pri< MEA_NUM_OF_ELEMENTS(entry->dbt_public.pri_queues);pri++){
                if((!enet_Queue_InitDone[unit_i])  || 
                    (old_entry == NULL)            || 
                    (entry->dbt_public.pri_queues[pri].mc_MQS != old_entry->dbt_public.pri_queues[pri].mc_MQS) || 
                    (entry->dbt_private.is_fatherActive       != old_entry->dbt_private.is_fatherActive) ){

                        ind_write.tableOffset	= (queue*MEA_NUM_OF_ELEMENTS(entry->dbt_public.pri_queues))+ pri;
                        ind_write.funcParam1 = (MEA_funcParam)((long)entry->dbt_public.pri_queues[pri].mc_MQS);
                        ind_write.funcParam2 = (MEA_funcParam)((long)entry->dbt_private.is_fatherActive);



            /* Write to the Indirect Table */
            if (Init_QUEUE)
            if (ENET_WriteIndirect(unit_i,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d mdl=%d failed (queue=%d)\n",
                    __FUNCTION__,ind_write.tableType,ENET_MODULE_BM,queue);
                return MEA_ERROR;
            }
                }
            }

         }
    
    return ENET_OK;
}
static ENET_Status enet_queue_UpdateHwMTU(ENET_Unit_t                unit_i,
                                          ENET_QueueId_t            queue,
                                          enet_Queue_dbt            *entry,
                                          enet_Queue_dbt            *old_entry)
{
	ENET_ind_write_t  ind_write;
    MEA_Uint32 pri;

   

	if ((!enet_Queue_InitDone[unit_i])  ||
		(old_entry == NULL) ||
			(entry->dbt_public.MTU !=old_entry->dbt_public.MTU) )
	{
        if ((MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_CLUSTER_TYPE)   && (MEA_GLOBAL_MTU_PRIQ_REDUSE == MEA_FALSE))
		{	
			/* build the ind_write value */
			ind_write.tableType		= ENET_BM_TBL_TYP_QUEUE_MTU_SIZE;
			ind_write.tableOffset	= queue;
			ind_write.cmdReg		= MEA_BM_IA_CMD;      
			ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
			ind_write.statusReg		= MEA_BM_IA_STAT;   
			ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
			ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
			ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
            ind_write.funcParam1 = (MEA_funcParam)entry;
            ind_write.writeEntry = (MEA_funcParam)enet_queue_UpdateHwEntryMTU;
            
			/* Write to the Indirect Table */
            if (Init_QUEUE)
			if (ENET_WriteIndirect(unit_i,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - ENET_WriteIndirect tbl=%d mdl=%d failed (queue=%d)\n",
								  __FUNCTION__,ind_write.tableType,ENET_MODULE_BM,queue);
				return MEA_ERROR;
			}
		} 
	 }

    if (MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE && (MEA_GLOBAL_MTU_PRIQ_REDUSE == MEA_FALSE))
	{
			ind_write.tableType		= ENET_BM_TBL_TYP_QUEUE_MTU_SIZE;
            ind_write.cmdReg		= MEA_BM_IA_CMD;      
			ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
			ind_write.statusReg		= MEA_BM_IA_STAT;   
			ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
			ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
			ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
			ind_write.writeEntry    = (MEA_FUNCPTR)enet_queue_UpdateHwEntryMTU_Pri_queue;

            for(pri=0;pri< MEA_NUM_OF_ELEMENTS(entry->dbt_public.pri_queues);pri++){
				if((!enet_Queue_InitDone[unit_i])  || 
					(old_entry == NULL)            || 
					(entry->dbt_public.pri_queues[pri].mtu != old_entry->dbt_public.pri_queues[pri].mtu)){

				ind_write.tableOffset	= (queue*MEA_NUM_OF_ELEMENTS(entry->dbt_public.pri_queues))+ pri;
                ind_write.funcParam1 = (MEA_funcParam)((long)entry->dbt_public.pri_queues[pri].mtu);
				/* Write to the Indirect Table */
                if (Init_QUEUE)
				if (ENET_WriteIndirect(unit_i,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - ENET_WriteIndirect tbl=%d mdl=%dooffset=%d failed (queue=%d)\n",
                                      __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_BM,queue);
					return MEA_ERROR;
				} 
			}
		}
	}


    /* return to caller */
	return ENET_OK;
}

void mea_queue_drv_config_potr_to_groupHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    MEA_Unit_t                 unit   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32 set_value=                (MEA_Uint32  )(long)arg4;

    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;
    /* Zero initialization the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    val[0]=set_value;

    

    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(unit,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }



}

static MEA_Status mea_queue_drv_config_potr_to_groupHw(ENET_Unit_t unit_i,MEA_Port_t port,MEA_Uint32 value)
{
    
    ENET_ind_write_t  ind_write;

    /* build the ind_write value */
    ind_write.tableType		= ENET_BM_TBL_CLUSTER_TO_PORTS_GROUP;
    ind_write.tableOffset	= port;
    ind_write.cmdReg		= MEA_BM_IA_CMD;      
    ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg		= MEA_BM_IA_STAT;   
    ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
    ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)0;
    ind_write.funcParam3 = (MEA_funcParam)0;
    ind_write.funcParam4 = (MEA_funcParam)((long) value);
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_queue_drv_config_potr_to_groupHwEntry;

    /* Write to the Indirect Table */
    if (Init_QUEUE)
    if (ENET_WriteIndirect(unit_i,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__,ind_write.tableType,ENET_MODULE_BM);
        return MEA_ERROR;
    }

 


    /* return to caller */
    return ENET_OK;
}

static void enet_queue_MultiPortsUpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)

{

    MEA_Unit_t             unit   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Queue_portsGroupe_t *port_groupEntry= (MEA_Queue_portsGroupe_t * )arg4;

    MEA_Uint32                 val[2];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    /* Zero initialization the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    

       val[0] = port_groupEntry->ports[0] ,
    count_shift=0;

#if 0
    MEA_OS_insert_value(count_shift,
        4,
        port_groupEntry->group ,
        &val[0]);
    count_shift+=4;
#endif
 
    MEA_OS_insert_value(count_shift,
        3,
        port_groupEntry->ports[1] ,
        &val[1]);
    count_shift+=3;

    MEA_OS_insert_value(count_shift,
        1,
        port_groupEntry->valid ,
        &val[1]);
    count_shift+=1;


    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        MEA_API_WriteReg(unit,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }
}

static ENET_Status enet_queue_MultiPortsUpdateHw(ENET_Unit_t                unit_i,
                                                 ENET_QueueId_t            queue,
                                                 enet_Queue_dbt            *entry,
                                                 enet_Queue_dbt            *old_entry)
{
    ENET_ind_write_t  ind_write;
    MEA_Uint32 value;
   
    MEA_Queue_portsGroupe_t port_groupEntry;
    MEA_Port_t port_tmp;
    MEA_OutPorts_Entry_dbt Multiple_Port;

    if(!MEA_GLOBAL_CLUSTER_TO_MULTI_PORT_SUPPORT)
      return MEA_OK;

    if ((!enet_Queue_InitDone[unit_i])  ||
        (old_entry == NULL) ||
        (MEA_OS_memcmp(&entry->dbt_public.port_group,&old_entry->dbt_public.port_group,sizeof(entry->dbt_public.port_group))!=0) )
    {
        /* build the ind_write value */
        ind_write.tableType		= ENET_BM_TBL_CLUSTER_TO_MULTI_PORT;
        ind_write.tableOffset	= queue;
        ind_write.cmdReg		= MEA_BM_IA_CMD;      
        ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
        ind_write.statusReg		= MEA_BM_IA_STAT;   
        ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
        ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
        ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)0;
        ind_write.funcParam3 = (MEA_funcParam)0;
        ind_write.funcParam4 = (MEA_funcParam)&port_groupEntry;
        ind_write.writeEntry    = (MEA_FUNCPTR)enet_queue_MultiPortsUpdateHwEntry;
            
        MEA_OS_memset(&port_groupEntry,0,sizeof(port_groupEntry));
        if(entry->dbt_public.port_group.valid == MEA_TRUE){
           port_groupEntry.valid = entry->dbt_public.port_group.valid;
           // we support only port 0 to 23 VDSL 
           // and 125 126 127
           port_groupEntry.ports[0] = entry->dbt_public.port_group.Multiple_Port.out_ports_0_31 & 0x00ffffff;
           port_groupEntry.ports[0] |=((entry->dbt_public.port_group.Multiple_Port.out_ports_96_127 & 0xe0000000)>>29)<<24;
           port_groupEntry.ports[0] |=((entry->dbt_public.port_group.Multiple_Port.out_ports_0_31 & 0x1F000000)>>24)<<27; //port 24 untill 28
           port_groupEntry.ports[1] |=((entry->dbt_public.port_group.Multiple_Port.out_ports_0_31 & 0xe0000000)>>29)<<0;//port 29 30 31
           port_groupEntry.ports[1] |= (entry->dbt_public.port_group.valid)<<3;

           port_groupEntry.group = entry->dbt_public.port_group.ports_groupId;
        }


        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,MEA_MODULE_BM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d mdl=%d failed (queue=%d)\n",
                __FUNCTION__,ind_write.tableType,ENET_MODULE_BM,queue);
            return MEA_ERROR;
        }
//// configure the multi group
        //////////////////////////////////////////////////////////////////////////
        // multi port.
        //////////////////////////////////////////////////////////////////////////
        MEA_OS_memcpy(&Multiple_Port,&entry->dbt_public.port_group.Multiple_Port,sizeof(Multiple_Port));
        if(!(old_entry == NULL)){ 
            if((entry->dbt_public.port_group.valid == MEA_TRUE) &&
                (old_entry->dbt_public.port_group.valid == MEA_FALSE)){
                // the old was disable to enable
               MEA_OS_memcpy(&Multiple_Port,&entry->dbt_public.port_group.Multiple_Port,sizeof(Multiple_Port));
            }
            if((entry->dbt_public.port_group.valid == MEA_FALSE)&&
                (old_entry->dbt_public.port_group.valid == MEA_TRUE)){
                    // the old was  enable to disable
                    MEA_OS_memcpy(&Multiple_Port,&old_entry->dbt_public.port_group.Multiple_Port,sizeof(Multiple_Port));
            }
            if((entry->dbt_public.port_group.valid == MEA_FALSE)&&
                (old_entry->dbt_public.port_group.valid == MEA_FALSE)){
                    //need to check the multi port change 
                    // the old was  disable to disable 
                   //not allow 

            }
            
            
        }
        
        for (port_tmp = 0;port_tmp <=MEA_MAX_PORT_NUMBER; port_tmp++)
        {
            if(entry->dbt_public.port_group.valid == MEA_FALSE){
            value=port_tmp;
            }else{
                value=entry->dbt_public.port.id.port;
            }
            if (((ENET_Uint32*)(&(Multiple_Port.out_ports_0_31)))[port_tmp/32] & (1 << (port_tmp%32))) {
                if( mea_queue_drv_config_potr_to_groupHw(unit_i,port_tmp,value)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_queue_drv_config_potr_to_group\n",
                        __FUNCTION__);
                    return MEA_ERROR;
                }
             }
        }
    } 

    /* return to caller */
    return ENET_OK;
}

/*---------------------------------------------------------------------------*/
/*            <enet_queue_UpdateHwMQS>                                       */
/*---------------------------------------------------------------------------*/
static ENET_Status enet_queue_SetDefaultMQS(ENET_Unit_t unit_i,
                                            ENET_QueueId_t qid)
{
	enet_Queue_dbt queueDB;
	ENET_PriQueueId_t pri;

	for (pri=0;pri<ENET_MAX_NUMBER_OF_PRI_QUEUE;pri++){
		queueDB.dbt_public.pri_queues[pri].max_q_size_Byte = 0;
		queueDB.dbt_public.pri_queues[pri].max_q_size_Packets = 0;
	}

	if(qid == UNASSIGNED_QUEUE_VAL){
		for(qid=0;qid<ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT;qid++){
			if (enet_queue_UpdateHwMQS(unit_i,qid,&queueDB,NULL)!=ENET_OK)
			{
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								 "%s - enet_queue_UpdateHwMQSfalied (queue=%d ",
								 __FUNCTION__,qid);
			   return ENET_ERROR;
			}
		}
	}else{
		if (enet_queue_UpdateHwMQS(unit_i,qid,&queueDB,NULL)!=ENET_OK)
		{
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - enet_queue_UpdateHwMQSfalied (queue=%d ",
							 __FUNCTION__,qid);
		   return ENET_ERROR;
		}
	}
	return ENET_OK;
}


static ENET_Status enet_queue_UpdateHwMQS(ENET_Unit_t                unit_i,
                                          ENET_QueueId_t            queue,
                                          enet_Queue_dbt            *entry,
                                          enet_Queue_dbt            *old_entry)
{
	ENET_ind_write_t  ind_write;
    ENET_PriQueueId_t priority;

    /* init the MQS_vec to default values */
	for (priority=0;priority<ENET_PLAT_QUEUE_NUM_OF_PRI_Q;priority++) {

		if ((!enet_Queue_InitDone[unit_i])  ||
			(old_entry == NULL)                  ||
			(entry->dbt_public.pri_queues[priority].max_q_size_Packets !=old_entry->dbt_public.pri_queues[priority].max_q_size_Packets) ||
			(entry->dbt_public.pri_queues[priority].max_q_size_Byte !=old_entry->dbt_public.pri_queues[priority].max_q_size_Byte)  ||
			(entry->dbt_private.is_fatherActive!=old_entry->dbt_private.is_fatherActive)  )
		{
			
      //      MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"QueueID=%d   priority=%d  Packets-zise(%d) Byte-Size(%d)\n",queue,priority,entry->dbt_public.pri_queues[priority].max_q_size_Packets,entry->dbt_public.pri_queues[priority].max_q_size_Byte);
            
            /* build the ind_write value */
			ind_write.tableType		= ENET_BM_TBL_TYP_EGRESS_PORT_MQS;
			ind_write.tableOffset	= (ENET_MAX_NUMBER_OF_PRI_QUEUE*queue)+priority;
			ind_write.cmdReg		= ENET_BM_IA_CMD;      
			ind_write.cmdMask		= ENET_BM_IA_CMD_MASK;      
			ind_write.statusReg		= ENET_BM_IA_STAT;   
			ind_write.statusMask	= ENET_BM_IA_STAT_MASK;   
 			ind_write.tableAddrReg	= ENET_BM_IA_ADDR;
   			ind_write.tableAddrMask	= ENET_BM_IA_ADDR_OFFSET_MASK;
            ind_write.funcParam1 = (MEA_funcParam)entry;
            ind_write.funcParam2 = (MEA_funcParam)((long)priority);
            ind_write.writeEntry = (MEA_FUNCPTR)enet_queue_UpdateHwEntryMQS;
		
			/* Write to the Indirect Table */
            if (Init_QUEUE)
			if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_BM)!=ENET_OK) {
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								 "%s - ENET_WriteIndirect tbl=%d mdl=%d failed "
								 "(queue=%d,priority=%d)\n",
								 __FUNCTION__,
								 ind_write.tableType,
								 MEA_MODULE_BM,
								 queue,
								 priority);

			   return ENET_ERROR;
			}
		}
    }
    /* return to caller */
	return ENET_OK;
}
/********************************** StrictLevel Table *******************************/
static void enet_queue_UpdateHwEntryPriStrictLevel(ENET_Uint32 strictLevel)
{
    ENET_Uint32 entryVal;
	entryVal  = ((strictLevel )<<
                 MEA_OS_calc_shift_from_mask(MEA_QUEUE_FAIRNESS_LEVEL));
 	ENET_WriteReg(ENET_UNIT_0,ENET_BM_IA_WRDATA0,entryVal,ENET_MODULE_BM);
}





static void enet_queue_UpdateHwEntryPriWfq(ENET_Uint32 weight)
{	
	ENET_Uint32 val;

	val = (weight); 
	if (val <= 3) val = 3;   /* 0 1 2 is not allowed */


 	ENET_WriteReg(ENET_UNIT_0,ENET_BM_IA_WRDATA0,val,ENET_MODULE_BM);
}

/*---------------------------------------------------------------------------*/
/*            <enet_queue_UpdatePriStrictLevel>                                       */
/*---------------------------------------------------------------------------*/

static ENET_Status enet_queue_UpdateHwPriStrictLevel(ENET_Unit_t                unit_i,
                                                     ENET_QueueId_t            queue,
                                                     enet_Queue_dbt            *entry,
                                                     enet_Queue_dbt            *old_entry)
{
	ENET_ind_write_t  ind_write;
	ENET_Uint32 strictLevel = ENET_MAX_NUMBER_OF_PRI_QUEUE;
	ENET_Uint32 priority_queue;

    if(Init_QUEUE == MEA_FALSE) {
        return MEA_OK;
    }

	if ((!enet_Queue_InitDone[unit_i])  ||
		(old_entry == NULL)                  ||
		(memcmp(entry->dbt_public.pri_queues ,old_entry->dbt_public.pri_queues,sizeof(ENET_Queue_priority_queue_dbt)* ENET_PLAT_QUEUE_NUM_OF_PRI_Q)!=0) ){
#ifdef MEA_ENV_256PORT			
        MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, (queue / 128), MEA_MODULE_BM);
#endif
          strictLevel = 0;
			for (priority_queue=0;
				 priority_queue<ENET_NUM_OF_ELEMENTS(entry->dbt_public.pri_queues);
				 priority_queue++)  {
					/* update strict Level */
					if  ((entry->dbt_public.pri_queues[priority_queue].mode.type != ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT))
						strictLevel = priority_queue;
			}
			strictLevel = 7-strictLevel;
			/* 0 -all RR , 1-strict:7 other RR, ... , 6-RR:0,1 other strict , 7 all strict */
			/* build the ind_write value */
			ind_write.tableType		= ENET_BM_TBL_TYP_CLUSTER_DATABASE;
#ifdef MEA_ENV_256PORT
            
            if (queue >=128)
                ind_write.tableOffset = queue-128;
            else
                ind_write.tableOffset = queue ;


#else
            ind_write.tableOffset	= queue;
#endif

			ind_write.cmdReg		= ENET_BM_IA_CMD;      
			ind_write.cmdMask		= ENET_BM_IA_CMD_MASK;      
			ind_write.statusReg		= ENET_BM_IA_STAT;   
			ind_write.statusMask	= ENET_BM_IA_STAT_MASK;   
 			ind_write.tableAddrReg	= ENET_BM_IA_ADDR;
   			ind_write.tableAddrMask	= ENET_BM_IA_ADDR_OFFSET_MASK;
            ind_write.funcParam1 = (MEA_funcParam)((long)strictLevel);
			ind_write.writeEntry    = (ENET_FUNCPTR)enet_queue_UpdateHwEntryPriStrictLevel;
			/* Write to the Indirect Table */
            
			if (ENET_WriteIndirect(ENET_UNIT_0,&ind_write,ENET_MODULE_BM)!=ENET_OK) {
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								 "%s - ENET_WriteIndirect tbl=%d mdl=%d failed "
								 "(queue=%d)\n",
								 __FUNCTION__,
								 ind_write.tableType,
								 MEA_MODULE_BM,
								 queue);
			   return ENET_ERROR;
			}
	}
    /* return to caller */
	return ENET_OK;
}


static ENET_Status enet_queue_UpdateHwPriWfq(ENET_Unit_t               unit_i,
                                             ENET_QueueId_t            queue,    /* cluster internal*/
                                             enet_Queue_dbt            *entry,
                                             enet_Queue_dbt            *old_entry)
{
	ENET_ind_write_t  ind_write;
	//ENET_Uint32 strictLevel = ENET_MAX_NUMBER_OF_PRI_QUEUE;
	ENET_Uint32 priority_queue;

    if (Init_QUEUE == MEA_FALSE){
        return MEA_OK;
    }

	if ((!enet_Queue_InitDone[unit_i])  ||
		(old_entry == NULL)                  ||
		(memcmp(entry->dbt_public.pri_queues ,old_entry->dbt_public.pri_queues,sizeof(ENET_Queue_priority_queue_dbt)* ENET_PLAT_QUEUE_NUM_OF_PRI_Q)!=0) ){

            /* update the Pri-Q WFQ table */
			for (priority_queue=0;
				 priority_queue<ENET_NUM_OF_ELEMENTS(entry->dbt_public.pri_queues);
				 priority_queue++)  
			{
				if ( entry->dbt_public.pri_queues[priority_queue].mode.type == ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ )
				{
#ifdef MEA_ENV_256PORT				
                    MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, (queue / 128), MEA_MODULE_BM);
#endif
                    
                    ind_write.tableType		= ENET_BM_TBL_TYP_CLUSTER_PQ_WFQ_INFORMATION;
#ifdef MEA_ENV_256PORT
                    if ((ENET_MAX_NUMBER_OF_PRI_QUEUE*queue) + priority_queue < 1024){
                        ind_write.tableOffset = (ENET_MAX_NUMBER_OF_PRI_QUEUE*queue) + priority_queue;
                    }else{
                        ind_write.tableOffset = ((ENET_MAX_NUMBER_OF_PRI_QUEUE*queue) + priority_queue ) -1024;
                    }
#else

					ind_write.tableOffset	= (ENET_MAX_NUMBER_OF_PRI_QUEUE*queue)+priority_queue;
#endif					
                    ind_write.cmdReg		= ENET_BM_IA_CMD;      
					ind_write.cmdMask		= ENET_BM_IA_CMD_MASK;      
					ind_write.statusReg		= ENET_BM_IA_STAT;   
					ind_write.statusMask	= ENET_BM_IA_STAT_MASK;   
 					ind_write.tableAddrReg	= ENET_BM_IA_ADDR;
   					ind_write.tableAddrMask	= ENET_BM_IA_ADDR_OFFSET_MASK;
                    ind_write.funcParam1 = (MEA_funcParam)((long)entry->dbt_public.pri_queues[priority_queue].mode.value.wfq_weight);
					ind_write.writeEntry    = (ENET_FUNCPTR)enet_queue_UpdateHwEntryPriWfq;					
					/* Write to the Indirect Table */
					if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_BM)!=ENET_OK) {
					   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
										 "%s - ENET_WriteIndirect tbl=%d mdl=%d failed "
										 "(queue=%d)\n",
										 __FUNCTION__,
										 ind_write.tableType,
										 MEA_MODULE_BM,
										 queue);
					   return ENET_ERROR;
					}
				}		
			}
	}
    /* return to caller */
	return ENET_OK;
}

/********************************** Cluster WFQ Table *******************************/

/*---------------------------------------------------------------------------*/
/*            <enet_queue_UpdateHwEntryClusterWFQ>                                  */
/*---------------------------------------------------------------------------*/
static void enet_queue_UpdateHwEntryClusterWFQ(ENET_Queue_dbt            *entry,
										       ENET_QueueId_t            queue)
{
    ENET_Uint32 entryVal;
	ENET_Uint32 val = (entry->mode.value.wfq_weight); 
	if (val<=3) val = 3;   /* 0 1 2 is not allowed */
	entryVal  = ((val & MEA_QUEUE_WFQ_WEIGHT_MASK)<<
                 MEA_OS_calc_shift_from_mask(MEA_QUEUE_WFQ_WEIGHT_MASK));
 	ENET_WriteReg(ENET_UNIT_0,ENET_BM_IA_WRDATA0,entryVal,ENET_MODULE_BM);
}

/*---------------------------------------------------------------------------*/
/*            <enet_queue_UpdateHwClusterWFQ>                                       */
/*---------------------------------------------------------------------------*/
static ENET_Status enet_queue_UpdateHwClusterWFQ(ENET_Unit_t                unit_i,
                                                 ENET_QueueId_t            queue,
                                                 enet_Queue_dbt            *entry,
                                                 enet_Queue_dbt            *old_entry)
{
	ENET_ind_write_t  ind_write;

	if ((!enet_Queue_InitDone[unit_i])  ||
		(old_entry == NULL)                  ||
		(entry->dbt_public.mode.type !=old_entry->dbt_public.mode.type) ||
		(entry->dbt_public.mode.value.wfq_weight !=old_entry->dbt_public.mode.value.wfq_weight)) {
#ifdef MEA_ENV_256PORT        
        MEA_API_WriteReg(MEA_UNIT_0, ENET_BM_LQ_ID_SELECT, (queue / 128), MEA_MODULE_BM);
#endif
			/* build the ind_write value */
			ind_write.tableType		= ENET_BM_TBL_TYP_CLUSTER_WFQ_INFORMATION;
			ind_write.tableOffset	= queue;
			ind_write.cmdReg		= ENET_BM_IA_CMD;      
			ind_write.cmdMask		= ENET_BM_IA_CMD_MASK;      
			ind_write.statusReg		= ENET_BM_IA_STAT;   
			ind_write.statusMask	= ENET_BM_IA_STAT_MASK;   
 			ind_write.tableAddrReg	= ENET_BM_IA_ADDR;
   			ind_write.tableAddrMask	= ENET_BM_IA_ADDR_OFFSET_MASK;
            ind_write.funcParam1 = (MEA_funcParam)entry;
            ind_write.funcParam2 = (MEA_funcParam)((long)queue);
			ind_write.writeEntry    = (ENET_FUNCPTR)enet_queue_UpdateHwEntryClusterWFQ;
			/* Write to the Indirect Table */
            if (Init_QUEUE)
			if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_BM)!=ENET_OK) {
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								 "%s - ENET_WriteIndirect tbl=%d mdl=%d failed "
								 "(queue=%d)\n",
								 __FUNCTION__,
								 ind_write.tableType,
								 MEA_MODULE_BM,
								 queue);
			   return ENET_ERROR;
			}
		}
    /* return to caller */
	return ENET_OK;
}

/*---------------------------------------------------------------------------*/
/*            <SetDefaultClusterToPort>                                      */
/*    This function is called by the Queue driver to initialize the DSE      */
/*    translation table from ingress port to egress queue                    */
/*---------------------------------------------------------------------------*/
void SetDefaultClusterToPort(ENET_PortId_t port,/* egress port */
							 ENET_QueueId_t cluster) /* internal cluster */
{
	MEA_PortsMapping_t portMap;
	enet_Queue_dbt QueueDbt;
	ENET_Uint32 len;
//    ENET_Uint32 i;
	ENET_QueueId_t externalCluster=cluster;



	/* 1. convert egress to ingress */
	if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
	                                    MEA_PORTS_TYPE_EGRESS_TYPE,
									   (MEA_Uint16)port,
									   &portMap)!=MEA_OK){
		     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on egress port= %d \n",port);
			 return;
    }
    if (!portMap.valid){
        if(!MEA_STANDART_BONDING_SUPPORT){
 		     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"egress port was not found (port=%d) %d \n",port);
    		 return;
        }else{
            portMap.portType=MEA_PORTTYPE_GBE_MAC;
        }
	}
	/* 2. convert internal  to external */
	if(cluster!=UNASSIGNED_QUEUE_VAL){
		len=sizeof(QueueDbt);
		if (ENET_Get_GroupElement(MEA_UNIT_0,
								  enet_Queue_group_id[MEA_UNIT_0],
								  cluster,
								  &QueueDbt,
								  &len) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							   "%s - ENET_Get_GroupElement failed (Queue id=%d)\n",
							   __FUNCTION__,cluster);
			return ;
		}
		externalCluster=QueueDbt.dbt_private.external_id;
	}
	switch (portMap.portType)
	{
    case     MEA_PORTTYPE_E1_T1_T1_193B         : 
    case     MEA_PORTTYPE_AAL5_ATM_TC           :     
    case     MEA_PORTTYPE_GBE_MAC               :
    case     MEA_PORTTYPE_GBE_1588              :
    case     MEA_PORTTYPE_10G_XMAC_ENET         :
    case     MEA_PORTTYPE_10_XMAC_Xilinx        :
    case     MEA_PORTTYPE_G999_1MAC             :
    case     MEA_PORTTYPE_G999_1MAC_1588        :
    case     MEA_PORTTYPE_AURORA_MAC            :
    case     MEA_PORTTYPE_Interlaken_MAC        :
    case     MEA_PORTTYPE_GENERATOR_ANLAZER     :
    case     MEA_PORTTYPE_40GBE                 :
    case    MEA_PORTTYPE_100GBE                 :
    case     MEA_PORTTYPE_VIRTUAL               :

			enet_SetDefaultClusterToPort(ENET_UNIT_0,(ENET_PortId_t)portMap.Ingressport,externalCluster);
			break;

	}
}


/*---------------------------------------------------------------------------*/
/*            <MEA_Queue_port_status_changed>                                */
/*    This function is called by the port driver , to inform that the port   */
/*    status has changed , the port status in fluance the MQS                 */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_Queue_port_status_changed(ENET_PortId_t port,MEA_Bool isPortEnabled)
{
	ENET_Uint16 i;
	enet_Queue_dbt entry;
	ENET_Uint32 len;

	if (!enet_Queue_InitDone[ENET_UNIT_0]) {
		return MEA_OK;
	}

	for(i=0;i<MEA_NUM_OF_ELEMENTS(enet_HwMirror_ClusterToPort);i++){
		if (enet_Hw_Cluster2Port[i].PortNumber == port)
		{

			len=sizeof(entry);
			if (ENET_Get_GroupElement(ENET_UNIT_0,
									  enet_Queue_group_id[ENET_UNIT_0],
									  (ENET_GroupElementId_t)i,
									  &entry,
									  &len) != ENET_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								   "%s - ENET_Get_GroupElement failed (internal Queue id=%d)\n",
								   __FUNCTION__,i);
				return MEA_ERROR;
			}	
            /* the update of the father is done in the driver and not in this function */

			if (ENET_drv_Set_Queue       (ENET_UNIT_0,
							(ENET_QueueId_t)entry.dbt_private.external_id,
								  &(entry.dbt_public))){
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								   "%s - ENET_Set_Queue failed (internal Queue id=%d)\n",
								   __FUNCTION__,i);
				return MEA_ERROR;
			}
		}
	}
	return MEA_OK;
}






ENET_QueueId_t enet_cluster_external2internal(ENET_QueueId_t queueId)
{
	ENET_QueueId_t retVal = UNASSIGNED_QUEUE_VAL;
	if (!IS_QUEUE_ID_VALID(queueId)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - Qid is out of range (Queue id=%d)\n",
						   __FUNCTION__,queueId);
		return ENET_ERROR;
	} 	
	if (!enet_externalQidTable[queueId].isValid){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - 2 Qid is not created (Queue id=%d)\n",
						   __FUNCTION__,queueId);
		return ENET_ERROR;
	} 	
	retVal=enet_externalQidTable[queueId].Qid;
	return(retVal);
}

ENET_QueueId_t enet_cluster_internal2external(ENET_QueueId_t internalqueueId)
{

    return QueId_Internal2External(internalqueueId);

}



MEA_Status mea_Get_queue_Drop_MTU_Priq(MEA_Unit_t      unit_i,
											ENET_QueueId_t  QueueId_i,
											MEA_Uint8      *drop_MTU_priq)
{
	MEA_ind_read_t ind_read;
	MEA_Uint32     read_data[1];
	MEA_Uint8      value;
	MEA_Uint32     pri;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
	if(drop_MTU_priq == NULL){
		 MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - drop_MTU_priq = NULL  \n",
						   __FUNCTION__);
		return MEA_ERROR; 
	}
    if(ENET_IsValid_Queue(unit_i,
 						  QueueId_i,
						  ENET_FALSE)==MEA_FALSE){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - ENET_IsValid_Queue =   \n",
						   __FUNCTION__,QueueId_i);
		return MEA_ERROR; 
	}


#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/
	value=0;
	if(MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE){
		ind_read.tableType		=   ENET_BM_TBL_TYP_CNT_QUEUE_MTU_DROP;
		ind_read.cmdReg			=	MEA_BM_IA_CMD;      
		ind_read.cmdMask		=	MEA_BM_IA_CMD_MASK;      
		ind_read.statusReg		=	MEA_BM_IA_STAT;   
		ind_read.statusMask		=	MEA_BM_IA_STAT_MASK;   
		ind_read.tableAddrReg	=	MEA_BM_IA_ADDR;
		ind_read.tableAddrMask	=	MEA_BM_IA_ADDR_OFFSET_MASK;
		

		for(pri=0;pri<ENET_PLAT_QUEUE_NUM_OF_PRI_Q;pri++){
			ind_read.tableOffset	=   ((MEA_Uint32)enet_cluster_external2internal(QueueId_i)*ENET_PLAT_QUEUE_NUM_OF_PRI_Q) + 
                                                     pri;
			ind_read.read_data	    =   read_data;
			if (MEA_API_ReadIndirect(unit_i,&ind_read,
								 MEA_NUM_OF_ELEMENTS(read_data),
                                 MEA_MODULE_BM)  != MEA_OK) 
            {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - ReadIndirect from tableType %d , tableOffset %d module BM failed "
							  "(queue=%d)\n",
							  __FUNCTION__,ind_read.tableType,ind_read.tableOffset,QueueId_i);

			    return MEA_ERROR;
			}
 			
			value |= ( ((read_data[0] & MEA_BM_TBL_TYP_CNT_QUEUE_MTU_DROP_MASK ) ) << pri); 
		}
	}
       *drop_MTU_priq=value;
	return MEA_OK;
}

ENET_Status ENET_queue_check_OutPorts(ENET_Unit_t unit_i,MEA_OutPorts_Entry_dbt *OutPorts_Entry)
{
    MEA_Uint32* ptr;
    MEA_Uint32  mask;
    MEA_Uint32        i;
    MEA_Port_t  outport;
    ENET_Queue_groupId_te last_groupId = ENET_QUEUE_GROUP_ID_LAST;
    ENET_Queue_groupId_te  curr_groupId;
    
    if (OutPorts_Entry != NULL) {
        
        for (i=0 , ptr = (MEA_Uint32*)OutPorts_Entry , outport = 0;
             i<(sizeof(*OutPorts_Entry)/sizeof(*ptr));
             i++ , ptr++) {
            for (mask = 0x00000001 ; (mask & 0xffffffff) != 0 ; mask <<= 1,outport++) {
                if (*ptr & mask) {
	                if (ENET_IsValid_Queue(unit_i,outport,ENET_FALSE)==ENET_FALSE) { 
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s - out cluster %d is invalid \n",
                                          __FUNCTION__,outport);
                        return ENET_ERROR; 
                    }
                    curr_groupId = ENET_CLUSTER_INTERNAL_2_GROUP_ID(enet_cluster_external2internal(outport));
                    if (last_groupId != ENET_QUEUE_GROUP_ID_LAST) {
                        if (last_groupId != curr_groupId) {
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                              "%s - All the p2mp out cluster should be in same group \n",
                                              __FUNCTION__);
                            return ENET_ERROR;
                        }
#if 0
                        if (curr_groupId != ENET_QUEUE_GROUP_ID_0) {
                            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                              "%s - All the p2mp out cluster should be in group 0\n",
                                              __FUNCTION__);
                            return ENET_ERROR;
                        }
#endif

                    }
                    last_groupId = curr_groupId;
                }
     
            }
        } 
    }


    


    return ENET_OK;
}


ENET_Status enet_drv_externalQid_State_Get(ENET_Unit_t unit_i,ENET_QueueId_t  QueueId_i,MEA_PortState_te*  state_o)
{
    if(ENET_IsValid_Queue(unit_i,
 						  QueueId_i,
						  ENET_FALSE)==MEA_FALSE){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - ENET_IsValid_Queue =   \n",
						   __FUNCTION__,QueueId_i);
		return MEA_ERROR; 
	}

   *state_o = enet_externalQid_state_Table[QueueId_i].portState;
return MEA_OK;
}


ENET_Status enet_drv_externalQid_State_Set(ENET_Unit_t unit_i,ENET_QueueId_t  QueueId_i,MEA_PortState_te  state_i)
{
    if(ENET_IsValid_Queue(unit_i,
 						  QueueId_i,
						  ENET_FALSE)==MEA_FALSE){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - ENET_IsValid_Queue =   \n",
						   __FUNCTION__,QueueId_i);
		return MEA_ERROR; 
	}
     switch (state_i) {
    case MEA_PORT_STATE_DISCARD:
    case MEA_PORT_STATE_LEARN:
    case MEA_PORT_STATE_FORWARD:
        break;
    case MEA_PORT_STATE_LAST:
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Unknown state_i %d \n",
                          __FUNCTION__,state_i);
        return MEA_ERROR;
    }

   enet_externalQid_state_Table[QueueId_i].portState = state_i;
    
   return MEA_OK;
}



#define  MEA_QUEUE_THRESH_SEC             (mea_global_QueueFull_Handler_reset_sec)
#define  MEA_QUEUE_THRESH_current_MC_MQS  (mea_global_QueueFull_Handler_TH_MQS)
#define  MEA_QUEUE_THRESHcurrentPacket    (mea_global_QueueFull_Handler_TH_currentPacket)



MEA_Status  MEA_Get_QueueEvent(MEA_Unit_t    unit,  MEA_Uint32 Offset, MEA_Uint32 numOfelements,MEA_Uint32 *array)
{
   MEA_ind_read_t         ind_read;

   if (array == NULL) {
       return MEA_ERROR;
   }

    ind_read.tableType = ENET_BM_TBL_EVENT_STATE;
    ind_read.cmdReg = MEA_BM_IA_CMD;
    ind_read.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_read.statusReg = MEA_BM_IA_STAT;
    ind_read.statusMask = MEA_BM_IA_STAT_MASK;
    ind_read.tableAddrReg = MEA_BM_IA_ADDR;
    ind_read.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_read.tableOffset = ENET_TBL_BM_EVENT60_OFFSET48 + Offset;
    ind_read.read_data = array;

    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        numOfelements,
        MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ReadIndirect from tableType %x module BM failed "
            "(offset=%d)\n",
            __FUNCTION__, ind_read.tableType, ind_read.tableOffset);

        return MEA_ERROR;
    }

    return MEA_OK;

}


MEA_Uint32 mea_queueGlobal_priEvent[32];



MEA_Status  MEA_QueueFull_Handler(MEA_Unit_t    unit, MEA_Bool *enableRinit)
{

    ENET_QueueId_t queueId, Id;
    
   
    MEA_Uint32                  priority;
    //ENET_PriQueueSize_Info_t        q_info;
    //ENET_Queue_MC_MQS_Size_Info_t   mcq_info;
    MEA_Counters_Queue_dbt       Counters_entry;
    ENET_Queue_dbt               Queue_entry;
    MEA_EgressPort_Entry_dbt  EgressPort_Entry;
    MEA_Status ret;
    MEA_Port_t port;
    MEA_Uint32 macReady=0, macMASK=0;
    MEA_egressMacReady_t MacReadyentry;
    ENET_QueueId_t internalQid;
    MEA_Uint32  array[2];
    MEA_Uint32  Offset;
    MEA_Uint32 Qpri_event;

    MEA_API_LOG

        if (!MEA_QUEUE_MC_MQS_SUPPORT)
            return MEA_OK;

    if (enableRinit == NULL) {
        return MEA_ERROR;
    }
    *enableRinit = MEA_FALSE;



        if (MEA_reinit_start)
            return MEA_OK;

        if (!MEA_Counters_Type_Enable[MEA_COUNTERS_QUEUE_HANDLER_TYPE]) 
            return MEA_OK;

       
        
       

        MEA_OS_memset(&MacReadyentry, 0, sizeof(MacReadyentry));

        ret = MEA_API_Get_EgressMACPortReady(&MacReadyentry);

        MEA_OS_memset(array, 0, sizeof(array));
        for (Offset = 0; Offset <= 15; Offset++) {
            ret = MEA_Get_QueueEvent(ENET_UNIT_0, Offset, 2, array);
            if (ret) {
                continue;
            }
            mea_queueGlobal_priEvent[(Offset * 2)] = array[0];
            mea_queueGlobal_priEvent[(Offset * 2) + 1] = array[1];
        }

        //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "Start Handler \n");
    for (queueId = 0; queueId < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; queueId++)
    {
        if (MEA_reinit_start)
            return MEA_OK;

        if (ENET_IsValid_Queue(ENET_UNIT_0, queueId, MEA_TRUE) == ENET_FALSE)
            continue;

        internalQid = enet_cluster_external2internal(queueId);

        ret = ENET_Get_Queue(unit, queueId, &Queue_entry);

        if (ret) {
            continue;
        }

        if(Queue_entry.disableAnalyse == MEA_TRUE)
            continue;

        if (Queue_entry.port.type == ENET_QUEUE_PORT_TYPE_PORT) {
            port = Queue_entry.port.id.port;
            if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,
                port,
                &EgressPort_Entry) != MEA_OK) {
                continue;
            }



            if (isPortEnable((&EgressPort_Entry)) == 0) {
                MEA_OS_memset(&MEA_ClusterQueue_State[queueId].queueST_count[0], 0, sizeof(MEA_ClusterQueue_State[queueId].queueST_count));
                continue;
            }
            
 
        }else {
		continue;
		
		}
		
		
		macReady = ((MacReadyentry.if2bm_egr.v[(port) / 32]) & (0x00000001 << ((port) % 32)));
        macMASK = ((MacReadyentry.if2bm_egr_mask.v[(port) / 32]) & (0x00000001 << ((port) % 32)));
        if ((macReady == 0) || (macMASK == 0)) {
            //MEA_ClusterQueue_State[queueId].queueST_count[priority] = 0;
		    MEA_OS_memset(&MEA_ClusterQueue_State[queueId].queueST_count[0], 0, sizeof(MEA_ClusterQueue_State[queueId].queueST_count));
                    
			continue;
        }


        MEA_OS_memset(&Counters_entry, 0, sizeof(Counters_entry));
        ret = MEA_API_Get_Counters_Queue(ENET_UNIT_0,queueId,&Counters_entry);
        if (ret) {
            continue;
        }
        /*getReady on port */

        

        
      

        for (priority = 0; priority < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; priority++)
        {
            if (MEA_reinit_start)
                return MEA_OK;
            

            
            if (Counters_entry.nopacket[priority] == MEA_TRUE) {
                Qpri_event = ((mea_queueGlobal_priEvent[((internalQid * 8) + priority) / 32]) & (0x00000001 << ((((internalQid * 8) + priority)) % 32)));
                if (Qpri_event != 0) {
                   

                    MEA_ClusterQueue_State[queueId].queueST_count[priority]++;
                    if (MEA_ClusterQueue_State[queueId].queueST_count[priority] > MEA_QUEUE_THRESH_SEC / 2) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "internalQid =%d \n", internalQid);
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "ready[%3s] mask[%3s] queueId[%d].[%d] = Qpri_event %s ST_count %d\n", ((macReady == 0) ? "NO" : "yes"), ((macMASK == 0) ? "NO" : "yes"), queueId, priority, ((Qpri_event == 0) ? "NO" : "YES"), MEA_ClusterQueue_State[queueId].queueST_count[priority]);
                    }
                    
                }
                else {
                    MEA_ClusterQueue_State[queueId].queueST_count[priority]=0; 
                }
                

                if (MEA_ClusterQueue_State[queueId].queueST_count[priority] == MEA_QUEUE_THRESH_SEC) 
                {

                    /*************************************/
                    /*clear the all db                   */
                    /*************************************/
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "################################\n");
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Start Reinit QueueFull ready[% 3s] mask[% 3s] queueId[%d].[%d] =  Qpri_event[%3s]  ST_count %d\n", ((macReady == 0) ? "NO" : "yes"), ((macMASK == 0) ? "NO" : "yes") ,queueId, priority, ((Qpri_event == 0) ? "NO" : "YES"), MEA_ClusterQueue_State[queueId].queueST_count[priority]);
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "################################\n");
                   
                    for (Id = 0; Id < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; Id++) {
						if (ENET_IsValid_Queue(ENET_UNIT_0, Id , MEA_TRUE) == ENET_FALSE)
							continue;                     
					 MEA_OS_memset(&MEA_ClusterQueue_State[Id].queueST_count[0], 0, sizeof(MEA_ClusterQueue_State[Id].queueST_count));
                    }

                    MEA_OS_memset(&mea_queueGlobal_priEvent[0], 0, sizeof(mea_queueGlobal_priEvent));

                   
                  
                   
                    *enableRinit = MEA_TRUE;
                    return MEA_OK;
                }
            }
            else {
#if 0			
                if (MEA_ClusterQueue_State[queueId].queueST_count[priority] >= 5) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "################################\n");
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " clear queueId[%d].[%d] \n",queueId, priority);
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "################################\n");

                }
#endif
                MEA_ClusterQueue_State[queueId].queueST_count[priority] = 0;
            }
        }


        
    }


    return MEA_OK;

}



MEA_Status MEA_drv_Queue_UPDATE_ForPort(ENET_Unit_t  unit_i, ENET_PortId_t port)
{

    ENET_Queue_dbt  entry;
    ENET_Uint16		i;
    MEA_Globals_Entry_dbt globals_entry;
    MEA_PortsMapping_t  portMap;
    
    MEA_Bool TLS_Mode = MEA_FALSE;
    MEA_Bool PortIs_10G = MEA_FALSE;

    

    MEA_OS_memset(&entry, 0, sizeof(entry));

   


    if (MEA_API_Get_Globals_Entry(unit_i, &globals_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_API_Get_Globals_Entry failed\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_Low_Get_Port_Mapping_By_key(unit_i,
        MEA_PORTS_TYPE_PHYISCAL_TYPE,
        port,
        &portMap) != MEA_OK) {
        return ENET_ERROR;
    }

    if (ENET_Get_Queue(unit_i, (ENET_QueueId_t) port, &entry) != MEA_OK)
    {

        return MEA_ERROR;
    }



    if (globals_entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) {
        entry.mode.type = ENET_QUEUE_MODE_TYPE_RR;
        entry.mode.value.wfq_weight = ENET_CLUSTER_RR_DEF_VAL; // because fpga use this
    }
    else {
        entry.mode.type = ENET_QUEUE_MODE_TYPE_WFQ;
        entry.mode.value.wfq_weight = ENET_CLUSTER_WFQ_DEF_VAL;
    }


    entry.MTU = MEA_QUEUE_CLUSTER_MTU_DEF_VAL;
    entry.shaper_enable = MEA_FALSE;
    entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
    MEA_OS_memset(&entry.shaper_info, 0, sizeof(entry.shaper_info));

    if (mea_drv_get_interfaceType_by_portId(unit_i, port) == MEA_INTERFACETYPE_ENHANCED_TLS) {
        TLS_Mode = MEA_TRUE;
    }
    if (mea_drv_get_interfaceType_by_portId(unit_i, port) == MEA_INTERFACETYPE_SINGLE10G) {
        PortIs_10G = MEA_TRUE;
    }

    for (i = 0; i < ENET_MAX_NUMBER_OF_PRI_QUEUE; i++)
    {



        if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G_EPC(i);
            entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G_EPC(i);
            entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G_EPC(i);
        }
        if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G_EMB(i);
            entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G_EMB(i);
            entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G_EMB(i);
        }
       
            
       

        if (TLS_Mode == MEA_TRUE) { /*The Size of BMQS and MQS need to change */
            if (port == 24 && MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
                entry.pri_queues[i].mtu                = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
                entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_24_EMB(i);
                entry.pri_queues[i].max_q_size_Byte    = ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_24_EMB(i);
                entry.pri_queues[i].mc_MQS             = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_24_EMB(i);

            }
          
        }
        if (PortIs_10G == MEA_TRUE) {

            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_10G_EPC(i);
            entry.pri_queues[i].max_q_size_Byte    = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_10G_EPC(i);
            entry.pri_queues[i].mc_MQS             = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_10G_EPC(i);
        }

        entry.pri_queues[i].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;

        if (port == 127) { /*CPU port*/
            if (MEA_Platform_Get_IS_EPC(0) == MEA_TRUE) {
                entry.pri_queues[i].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL_CPU;
                entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU_EPC(i);
                entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_CPU_EPC(i);
                entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_CPU_EPC(i);
            }
            if (MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
                entry.pri_queues[i].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL_CPU;
                entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU_EMB(i);
                entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_CPU_EMB(i);
                entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_CPU_EMB(i);
            }
        }
        if (port == 12 && MEA_Platform_Get_IS_EPC(1) == MEA_TRUE) {
            entry.pri_queues[i].mtu = MEA_QUEUE_PRIQUEUE_MTU_DEF_VAL;
            entry.pri_queues[i].max_q_size_Packets = ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU_EMB(i);
            entry.pri_queues[i].max_q_size_Byte = ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_CPU_EMB(i);
            entry.pri_queues[i].mc_MQS = ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_CPU_EMB(i);

        }
        


      
     

        if (MEA_GLOBAL_PRI_Q_MODE_DEF_VAL != MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY) {
            entry.pri_queues[i].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT;
            entry.pri_queues[i].mode.value.strict_priority = ENET_PQ_STRICT_DEF_VAL;
        }
        else {
            entry.pri_queues[i].mode.type = ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR;
            entry.pri_queues[i].mode.value.strict_priority = ENET_PQ_RR_DEF_VAL;
        }
        
    }


    if (ENET_Set_Queue(unit_i, (ENET_QueueId_t)port, &entry) != MEA_OK) {
        return MEA_ERROR;
    }

    return MEA_OK;

}





MEA_Status  mea_drv_Event_BM60(MEA_Unit_t    unit, 
                               MEA_Uint32 Offset, 
                               MEA_Uint32 numOfelements, 
                               MEA_Uint32 *array)
{
    MEA_ind_read_t         ind_read;

    if (array == NULL) {
        return MEA_ERROR;
    }

    ind_read.tableType = ENET_BM_TBL_EVENT_STATE;
    ind_read.cmdReg = MEA_BM_IA_CMD;
    ind_read.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_read.statusReg = MEA_BM_IA_STAT;
    ind_read.statusMask = MEA_BM_IA_STAT_MASK;
    ind_read.tableAddrReg = MEA_BM_IA_ADDR;
    ind_read.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_read.tableOffset =  Offset;
    ind_read.read_data = array;

    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        numOfelements,
        MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ReadIndirect from tableType %x module BM failed "
            "(offset=%d)\n",
            __FUNCTION__, ind_read.tableType, ind_read.tableOffset);

        return MEA_ERROR;
    }

    return MEA_OK;

}



MEA_Status MEA_API_Get_Queues_PauseStatus(MEA_Unit_t unit, MEA_Uint16 Group, MEA_Q_PauseStatus_dbt *Entry)
{

    MEA_Status retVal = MEA_ERROR;
    if (Entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Entry == NULL\n");
        return MEA_ERROR;
    }

    if (ENET_TBL_BM_EVENT60_OFFSET68 + Group > ENET_TBL_BM_EVENT60_OFFSET_PAUSE_END) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " Group is out of range \n");
        return MEA_ERROR;
    }

    retVal = mea_drv_Event_BM60(unit,
        ENET_TBL_BM_EVENT60_OFFSET68 + Group,
        2,
        &Entry->reg[0]);

    return retVal;
}



MEA_Status MEA_API_Port_Get_ShaperStatus(MEA_Unit_t unit, MEA_Uint16 Group, MEA_ShaperEligibilityStatus_dbt *Entry)
{

    MEA_Status retVal = MEA_ERROR;
    if (Entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Entry == NULL\n");
        return MEA_ERROR;
    }

    if (ENET_TBL_BM_EVENT60_OFFSET64 + Group > ENET_TBL_BM_EVENT60_OFFSET67) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " Group is out of range \n");
        return MEA_ERROR;
    }

    retVal=mea_drv_Event_BM60(unit,
        ENET_TBL_BM_EVENT60_OFFSET68 + Group,
        2,
        &Entry->reg[0]);

    return retVal;

}







