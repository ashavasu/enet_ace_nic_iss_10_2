/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_if_regs.h"
#include "mea_if_service_drv.h"
#include "mea_serviceEntry_drv.h"
#include "mea_db_drv.h"
#include "mea_lxcp_drv.h"
#include "mea_action_drv.h"
#include "mea_filter_drv.h"
#include "mea_deviceInfo_drv.h"
#include "enet_xPermission_drv.h"
#include "mea_fwd_tbl.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct {
    MEA_Uint8  valid            :1;
    MEA_Uint8  learn_en         :1;  /*('1' -learning, '0'-not learning)*/
    MEA_Uint8  ipv6_mask_type   :3;
    MEA_Uint8  pad              :3;

}MEA_Characteristics_entry_HW_dbt;
/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static MEA_db_dbt                             MEA_ServiceEntry_db            = NULL;
static MEA_Characteristics_entry_dbt         *MEA_CharacteristicsTBL = NULL;

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
 
/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_drv_Get_ServiceEntry_num_of_hw_Internal_size_func>             */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_ServiceEntry_num_of_hw_Internal_size_func(MEA_Unit_t     unit_i,
								   				        MEA_db_HwUnit_t   hwUnit_i,
												        MEA_db_HwId_t *length_o,
												        MEA_Uint16    *num_of_regs_o) 
{

	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) >= max value (%d) \n",
						  __FUNCTION__,
						  hwUnit_i,
						  mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	if (length_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - legnth_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (num_of_regs_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - num_of_regs_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


    *length_o = MEA_IF_CMD_PARAM_TBL_TYP_SERVICE_ENTRY_LENGTH(unit_i, hwUnit_i); /*MEA_MAX_NUM_OF_SERVICES_INTERNAL*/

    *num_of_regs_o = mea_ServiceEntry_calc_numof_regs(unit_i, hwUnit_i);

    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*               <mea_drv_Get_ServiceEntry_num_of_sw_Internal_size_func>    */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_ServiceEntry_num_of_sw_Internal_size_func(MEA_Unit_t    unit_i,
								         			    MEA_db_SwId_t *length_o,
												        MEA_Uint32    *width_o) 
{
	if (length_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - legnth_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (width_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - width_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


    *length_o = MEA_MAX_NUM_OF_SERVICES_INTERNAL;
	*width_o  = 0;

	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_ServiceEntry_Table>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_ServiceEntry_Table(MEA_Unit_t unit_i)
{

	/* Init db - Hardware shadow */
	if (mea_db_Init(unit_i,
	                "ServiceEntry ",
                    mea_drv_Get_ServiceEntry_num_of_sw_Internal_size_func,
		            mea_drv_Get_ServiceEntry_num_of_hw_Internal_size_func,
					&MEA_ServiceEntry_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Init failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


	/* TBD - need to set all entries in HW are set to their default val '0'?? */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_ServiceEntry_Table>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_ServiceEntry_Table(MEA_Unit_t unit_i)
{

    /* ReInit ServiceEntry table   */
    if (mea_db_ReInit(unit_i,
                      MEA_ServiceEntry_db,
                      MEA_IF_CMD_PARAM_TBL_TYP_SERVICE_ENTRY,
                      MEA_MODULE_IF,
                      NULL,
                      MEA_FALSE) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit for ServiceEntry failed \n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

    /* Return to Caller */
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Conclude_ServiceEntry_Table>                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Conclude_ServiceEntry_Table(MEA_Unit_t unit_i)
{
	/* phase2 - need to confirm that all entry are not in use by service */

	if (mea_db_Conclude(unit_i,&MEA_ServiceEntry_db) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_dn_Conclude failed \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


    return MEA_OK;
}





/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/
MEA_Uint32 mea_ServiceEntry_calc_numof_regs(MEA_Unit_t         unit_i,
                    MEA_db_HwUnit_t    hwUnit_i)
{

    MEA_Uint32                count = 0;
    MEA_Uint32 numofRegs = 0;

    count = (

        MEA_IF_SRV_ENTRY_UPD_ALLOW_ENABLE_WIDTH +
        MEA_IF_SRV_ENTRY_LRN_ENA_WIDTH + 
        MEA_IF_SRV_ENTRY_FRW_ENA_WIDTH + 
        MEA_IF_SRV_ENTRY_ADM_ENA_WIDTH +
        MEA_IF_SRV_ENTRY_X_PERMISSION_WIDTH(unit_i, hwUnit_i) +
        MEA_IF_SRV_ENTRY_UPD_ALLOW_SEND_TO_CPU_WIDTH + 
        MEA_IF_SRV_ENTRY_VPN_ENA_WIDTH +
        MEA_IF_SRV_ENTRY_DA_KEY_TYPE_WIDTH +
        MEA_IF_SRV_ENTRY_SA_KEY_TYPE_WIDTH +
        MEA_IF_SRV_ENTRY_LRN_MASK_TYPE_WIDTH +
        MEA_IF_SRV_ENTRY_FILTER_KEY_WIDTH +
        MEA_IF_SRV_ENTRY_FILTER_MASK_TYPE_WIDTH +
        MEA_IF_SRV_ENTRY_LRN_FILTER_PRI_WIDTH +
        MEA_IF_SRV_ENTRY_CFM_OAM_ME_LVL_VALID_WIDTH +
        MEA_IF_SRV_ENTRY_CFM_OAM_ME_LVL_VALUE_WIDTH +
        MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_0 +
        MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_0 + 
        MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_1 +
        MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_1 +
        MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_2 + 
        MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_2 +
        MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_3 +
        MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_3 +
        
        MEA_IF_SRV_ENTRY_ACL_MODE_AND_OR +

        MEA_IF_SRV_ENTRY_ACL_MASK_PROF_WIDTH +

        MEA_IF_SRV_ENTRY_LIMIT_PROFILE_WIDTH(unit_i, hwUnit_i) +
        MEA_IF_SRV_ENTRY_FLOOD_UC_WIDTH +
        MEA_IF_SRV_ENTRY_FLOOD_MC_WIDTH +
        MEA_IF_SRV_ENTRY_FLOOD_BC_WIDTH +
        MEA_IF_SRV_ENTRY_LXCP_WIDTH(unit_i, hwUnit_i) +
        MEA_IF_SRV_ENTRY_REVERSE_ACTION_WIDTH(unit_i, hwUnit_i) +
        MEA_IF_SRV_ENTRY_REVERSE_ACTION_VALID_WIDTH +
        MEA_IF_SRV_ENTRY_REVERSE_PERM_WIDTH(unit_i, hwUnit_i) +
        MEA_IF_SRV_ENTRY_REVERSE_PERM_VALID_WIDTH +
        MEA_IF_SRV_ENTRY_L3_L4_VALID_WIDTH +
        MEA_IF_SRV_ENTRY_L4PORT_MASK_VALID_WIDTH +
        MEA_IF_SRV_ENTRY_MY_MAC_TO_PPP_PERM_VALID_WIDTH +
        MEA_IF_SRV_ENTRY_ACL_WHITE_LIST_ENABLE_WIDTH +
        MEA_IF_SRV_ENTRY_SA_NO_MACH_SEND_TO_INTERNAL +
        MEA_IF_SRV_ENTRY_INNER_LEARN_FWD_TYPE +
        MEA_IF_SRV_ENTRY_DSE_MASK_FILED_TYPE +
        MEA_IF_SRV_ENTRY_IPMC_AWARE_VALID +
		MEA_IF_SRV_ENTRY_MAC_INTERNAL_PROF_WIDTH(unit_i, hwUnit_i) +
        MEA_IF_SRV_ENTRY_MAC_INTERNAL_VALID_WIDTH(unit_i, hwUnit_i) +

        MEA_IF_SRV_ENTRY_ACCESS_PORT_ID_WIDTH +
        MEA_IF_SRV_ENTRY_ACCESS_ADM_EN_WIDTH +
        MEA_IF_SRV_ENTRY_ACCESS_LOCAL_SW_WIDTH +
        MEA_IF_SRV_ENTRY_INGRESS_FLOW_POL(unit_i, hwUnit_i)+
        MEA_IF_SRV_ENTRY_ACL_PROF_VALID_WIDTH +
        MEA_IF_SRV_ENTRY_ACL_PROF_WIDTH +
        MEA_IF_SRV_ENTRY_HPM_MASK_PROF_WIDTH +
        MEA_IF_SRV_ENTRY_HPM_PROF_WIDTH +
        MEA_IF_SRV_ENTRY_UEPDN_WIDTH + 
        MEA_IF_SRV_ENTRY_FWD_SELECT_KEY +
        MEA_IF_SRV_ENTRY_SEGMENT_SWITCH_PROF +
        MEA_IF_SRV_ENTRY_HPM_IPV6_KEY + 

        MEA_IF_SRV_ACL5_KEY_TYPE + 
        MEA_IF_SRV_ACL5_INNER_FRAM + 
        MEA_IF_SRV_ACL5_IP_MASK_PROF + 
        MEA_IF_SRV_ACL5_KEY_MASK_PROF +
        MEA_IF_SRV_ACL5_RANGE_PROF +
        MEA_IF_SRV_ACL5_DO 



        );
#if 0
    numofRegs = count / 32;
    if (count % 32 != 0)
        numofRegs++;
    if (numofRegs < 3)
        numofRegs = 3;/*need 3*/
#endif
    if (count == 0){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "count == %d \n", count);
    }

 
    /*
    internal 10Regs 
    */

    numofRegs = 10;


    return numofRegs;

}


MEA_Status mea_ServiceEntry_buildHWData(MEA_Unit_t              unit_i,
                                        MEA_db_HwUnit_t         hwUnit_i,
                                        MEA_db_HwId_t           hwId_i,
                                        MEA_Service_Entry_dbt *entry_pi,
                                        MEA_Uint32 len,
                                        MEA_Uint32  *retVal)
{

    MEA_Uint32             numofregs = 10;
   
    MEA_db_HwId_t          filter_mask_hwId;
    MEA_db_HwId_t          learning_action_hwId;
    MEA_db_HwUnit_t        learning_action_hwUnit;
//    MEA_Uint32   set_mac_ls;
//    MEA_Uint32   set_mac_ms;
    MEA_Uint32             count_shift = 0;
    MEA_Uint32    access_ADM;
    MEA_Uint32  set_vpn=0; 
    //MEA_Uint16 vlan_i=0;
    //MEA_PortState_te  state_o = MEA_PORT_STATE_FORWARD;
    MEA_Bool          DSE_forwarding_enable = MEA_FALSE;
    MEA_Bool          DSE_learning_enable = MEA_FALSE;
    MEA_Bool          LxCp_enable = MEA_FALSE;
    MEA_LxCp_t        LxCp_Id = 0;
    MEA_Bool  UC_NotAllowed = MEA_TRUE;
    MEA_Bool  MC_NotAllowed = MEA_TRUE;
    MEA_Bool  BC_NotAllowed = MEA_TRUE;
    MEA_Uint32 DSE_forwarding_key_type = 0;
    MEA_Uint32 XperVal;
    MEA_Uint32 key_type;
    //  MEA_Bool filter_enable   = MEA_FALSE;

    MEA_ACL5_Mask_Grouping_Profiles entry_maskprof;

   
   MEA_OS_memset(&entry_maskprof,0,sizeof(MEA_ACL5_Mask_Grouping_Profiles));

    numofregs = mea_ServiceEntry_calc_numof_regs(unit_i, hwUnit_i);

    if (numofregs > len){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "register on ServiceEntry is not correct \n");
    }




    if (entry_pi->data.valid) {
        if(entry_pi->data.Acl5_mask_prof!=0)
            MEA_API_ACL5_Mask_Grouping_Profiles_Get(MEA_UNIT_0, entry_pi->data.Acl5_mask_prof, &entry_maskprof);


        switch (entry_pi->data.DSE_forwarding_key_type)
        {
        case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
            DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
            break;
        case  MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
            DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
            break;
        default:
            DSE_forwarding_key_type = entry_pi->data.DSE_forwarding_key_type;
            break;
        }
        
       
        set_vpn = entry_pi->data.vpn;

        switch (entry_pi->data.MSTP_Info.state){

        case(MEA_MSTP_TYPE_STATE_FORWARD) :
            DSE_forwarding_enable = entry_pi->data.DSE_forwarding_enable;
            DSE_learning_enable = entry_pi->data.DSE_learning_enable;
            LxCp_enable = entry_pi->data.LxCp_enable;
            LxCp_Id = entry_pi->data.LxCp_Id;
            UC_NotAllowed = entry_pi->data.UC_NotAllowed;
            MC_NotAllowed = entry_pi->data.MC_NotAllowed;
            BC_NotAllowed = entry_pi->data.BC_NotAllowed;
            //  filter_enable         = entry_pi->data.filter_enable;
            break;
        case(MEA_MSTP_TYPE_STATE_BLOCKING) :
        case(MEA_MSTP_TYPE_STATE_LISENING) :
                                           DSE_forwarding_enable = MEA_FALSE;
            DSE_learning_enable = MEA_FALSE;
            LxCp_enable = MEA_TRUE;
            LxCp_Id = MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP;
            UC_NotAllowed = MEA_TRUE;
            MC_NotAllowed = MEA_TRUE;
            BC_NotAllowed = MEA_TRUE;
            //  filter_enable = MEA_FALSE;

            break;
        case(MEA_MSTP_TYPE_STATE_LEARNING) :
            DSE_forwarding_enable = MEA_FALSE;
            DSE_learning_enable = entry_pi->data.DSE_learning_enable;
            LxCp_enable = MEA_TRUE;
            LxCp_Id = MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP;
            UC_NotAllowed = MEA_TRUE;
            MC_NotAllowed = MEA_TRUE;
            BC_NotAllowed = MEA_TRUE;
            // filter_enable = MEA_FALSE;
            break;
        case MEA_MSTP_TYPE_STATE_BLOCKING_SRV:

            DSE_learning_enable = MEA_FALSE;
            DSE_forwarding_enable = entry_pi->data.DSE_forwarding_enable;
            DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
            LxCp_enable = MEA_TRUE;
            LxCp_Id = MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP;
            UC_NotAllowed = MEA_TRUE;
            MC_NotAllowed = MEA_TRUE;
            BC_NotAllowed = MEA_TRUE;
            break;
            case MEA_MSTP_TYPE_STATE_BLOCKING_SRV_VPN0:
                DSE_learning_enable = MEA_FALSE;
                DSE_forwarding_enable = entry_pi->data.DSE_forwarding_enable;
                DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
                set_vpn = 0;
                LxCp_enable = MEA_TRUE;
                LxCp_Id = MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP;
                UC_NotAllowed = MEA_TRUE;
                MC_NotAllowed = MEA_TRUE;
                BC_NotAllowed = MEA_TRUE;
                break;
        default:
            entry_pi->data.MSTP_Info.state = MEA_MSTP_TYPE_STATE_BLOCKING;
            DSE_forwarding_enable = MEA_FALSE;
            DSE_learning_enable = MEA_FALSE;
            LxCp_enable = MEA_TRUE;
            LxCp_Id = MEA_LXCP_ID_TYPE_DISCARD_ALL_NOT_L2CP;
            UC_NotAllowed = MEA_TRUE;
            MC_NotAllowed = MEA_TRUE;
            BC_NotAllowed = MEA_TRUE;
            //  filter_enable = MEA_FALSE;

            break;

        }

        //    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," DBG  port %d vlan %d state %d ,lxcp %d\n",(MEA_Port_t)entry_pi->key.src_port,vlan_i,state_o,entry_pi->data.LxCp_Id);



        count_shift = 0;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_UPD_ALLOW_ENABLE_WIDTH,
            (DSE_learning_enable)
            ? entry_pi->data.DSE_learning_update_allow_enable
            : 0,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_UPD_ALLOW_ENABLE_WIDTH;


        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_LRN_ENA_WIDTH,
            DSE_learning_enable,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_LRN_ENA_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_FRW_ENA_WIDTH,
            DSE_forwarding_enable,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_FRW_ENA_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_ADM_ENA_WIDTH,
            entry_pi->data.ADM_ENA,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_ADM_ENA_WIDTH;

#ifdef MEA_TDM_SW_SUPPORT 
        XperVal = ((entry_pi->data.tdm_packet_type == 0) ? entry_pi->xPermissionId : entry_pi->data.tdm_cesId);
#else
        XperVal = entry_pi->xPermissionId;
#endif
        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_X_PERMISSION_WIDTH(unit_i, hwUnit_i),
            XperVal,
            retVal);

        count_shift += MEA_IF_SRV_ENTRY_X_PERMISSION_WIDTH(unit_i, hwUnit_i);

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_UPD_ALLOW_SEND_TO_CPU_WIDTH,
            (DSE_learning_enable)
            ? entry_pi->data.DSE_learning_update_allow_send_to_cpu
            : 0,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_UPD_ALLOW_SEND_TO_CPU_WIDTH;


        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_VPN_ENA_WIDTH,
            set_vpn,//entry_pi->data.vpn,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_VPN_ENA_WIDTH;





        if (DSE_forwarding_enable) {

            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_DA_KEY_TYPE_WIDTH,
                DSE_forwarding_key_type,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_DA_KEY_TYPE_WIDTH;
        }
        else {

            count_shift += MEA_IF_SRV_ENTRY_DA_KEY_TYPE_WIDTH;
        }


        if (DSE_learning_enable) {
            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_SA_KEY_TYPE_WIDTH,
                entry_pi->data.DSE_learning_key_type,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_SA_KEY_TYPE_WIDTH;
            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_LRN_MASK_TYPE_WIDTH,
                0, /* Not Support by Software */
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_LRN_MASK_TYPE_WIDTH;
        }
        else {
            count_shift += MEA_IF_SRV_ENTRY_SA_KEY_TYPE_WIDTH;
            count_shift += MEA_IF_SRV_ENTRY_LRN_MASK_TYPE_WIDTH;
        }



        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_FILTER_KEY_WIDTH,
            0, /* Not Support by Software */
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_FILTER_KEY_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_FILTER_MASK_TYPE_WIDTH,
            0, /* Not Support by Software */
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_FILTER_MASK_TYPE_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_LRN_FILTER_PRI_WIDTH,
            0, /* Not Support by Software */
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_LRN_FILTER_PRI_WIDTH;


        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_CFM_OAM_ME_LVL_VALID_WIDTH,
            entry_pi->data.CFM_OAM_ME_Level_Threshold_valid,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_CFM_OAM_ME_LVL_VALID_WIDTH;
        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_CFM_OAM_ME_LVL_VALUE_WIDTH,
            entry_pi->data.CFM_OAM_ME_Level_Threshold_value,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_CFM_OAM_ME_LVL_VALUE_WIDTH;
        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_0,
            entry_pi->data.ACL_filter_info.data_info[0].valid,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_0;

        key_type = entry_pi->data.ACL_filter_info.data_info[0].filter_key_type;
        switch (key_type)
        {
        case MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT:
        case MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT:
        case MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT:
        case MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT:
        case MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE:
            key_type = key_type - 20;
            break;
        default:
            break;
        }
        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_0,
            key_type,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_0;

        if (MEA_ACL_HIERARCHICAL_SUPPORT == MEA_TRUE){

            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_1,
                entry_pi->data.ACL_filter_info.data_info[1].valid,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_1;
            key_type = entry_pi->data.ACL_filter_info.data_info[1].filter_key_type;
            switch (key_type)
            {
            case MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT:
            case MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT:
            case MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT:
            case MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT:
            case MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE:
                key_type = key_type - 20;
                break;
            default:
                break;
            }
            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_1,
                key_type,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_1;



            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_2,
                entry_pi->data.ACL_filter_info.data_info[2].valid,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_2;
            key_type = entry_pi->data.ACL_filter_info.data_info[2].filter_key_type;
            switch (key_type)
            {
            case MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT:
            case MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT:
            case MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT:
            case MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT:
            case MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE:
                key_type = key_type - 20;
                break;
            default:
                break;
            }
            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_2,
                key_type,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_2;


            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_3,
                entry_pi->data.ACL_filter_info.data_info[3].valid,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_3;
            key_type = entry_pi->data.ACL_filter_info.data_info[3].filter_key_type;
            switch (key_type)
            {
            case MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT:
            case MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT:
            case MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT:
            case MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT:
            case MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE:
                key_type = key_type - 20;
                break;
            default:
                break;
            }
            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_3,
                key_type,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_3;




            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_ACL_MODE_AND_OR,
                entry_pi->data.ACL_filter_info.filter_mode_OR_AND_type,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_ACL_MODE_AND_OR;

        }



        if (entry_pi->data.filter_enable) {

            filter_mask_hwId = entry_pi->filter_mask_id;
        }
        else {
            filter_mask_hwId = 0;
        }

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_ACL_MASK_PROF_WIDTH,
            (MEA_Uint32)filter_mask_hwId,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_ACL_MASK_PROF_WIDTH;







        if (entry_pi->data.limiter_enable)
        {
            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_LIMIT_PROFILE_WIDTH(unit_i, hwUnit_i),
                (MEA_Uint32)entry_pi->data.limiter_id,
                retVal);
        }
        count_shift += MEA_IF_SRV_ENTRY_LIMIT_PROFILE_WIDTH(unit_i, hwUnit_i);



        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_FLOOD_UC_WIDTH,
            UC_NotAllowed,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_FLOOD_UC_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_FLOOD_MC_WIDTH,
            MC_NotAllowed,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_FLOOD_MC_WIDTH;


        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_FLOOD_BC_WIDTH,
            BC_NotAllowed,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_FLOOD_BC_WIDTH;


        if (LxCp_enable){

            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_LXCP_WIDTH(unit_i, hwUnit_i),
                LxCp_Id,
                retVal);
        }
        else{
            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_LXCP_WIDTH(unit_i, hwUnit_i),
                MEA_LXCP_PROFILE_ALL_TRANSPARENT,
                retVal);
        }
        count_shift += MEA_IF_SRV_ENTRY_LXCP_WIDTH(unit_i, hwUnit_i);


        if ((entry_pi->data.DSE_learning_enable) &&
            (entry_pi->data.DSE_learning_actionId_valid)) {
            if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000)  {
                learning_action_hwUnit = MEA_HW_UNIT_ID_GENERAL;
            }
            else {
                learning_action_hwUnit = MEA_HW_UNIT_ID_DOWNSTREAM;
            }
            MEA_DRV_GET_HW_ID(unit_i,
                mea_drv_Get_Action_db,
                entry_pi->data.DSE_learning_actionId,
                learning_action_hwUnit,
                learning_action_hwId,
                return MEA_ERROR;);


        }
        else {
            learning_action_hwId = 0;
        }

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_REVERSE_ACTION_WIDTH(unit_i, hwUnit_i),
            (MEA_Uint32)learning_action_hwId,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_REVERSE_ACTION_WIDTH(unit_i, hwUnit_i);


        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_REVERSE_ACTION_VALID_WIDTH,
            entry_pi->data.DSE_learning_actionId_valid,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_REVERSE_ACTION_VALID_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_REVERSE_PERM_WIDTH(unit_i, hwUnit_i),
            (entry_pi->data.DSE_learning_Vp_force == MEA_TRUE) ? (entry_pi->LearnxPermissionId) : entry_pi->data.DSE_learning_srcPort,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_REVERSE_PERM_WIDTH(unit_i, hwUnit_i);

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_REVERSE_PERM_VALID_WIDTH,
            ((entry_pi->data.DSE_learning_Vp_force == MEA_TRUE) || entry_pi->data.DSE_learning_srcPort_force == MEA_TRUE) ? 1 : 0,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_REVERSE_PERM_VALID_WIDTH;



        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_L3_L4_VALID_WIDTH,
            entry_pi->data.L3_L4_fwd_enable,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_L3_L4_VALID_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_L4PORT_MASK_VALID_WIDTH,
            entry_pi->data.L4port_mask_enable,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_L4PORT_MASK_VALID_WIDTH;



        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_MY_MAC_TO_PPP_PERM_VALID_WIDTH,
            entry_pi->data.ppp_fwd,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_MY_MAC_TO_PPP_PERM_VALID_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_ACL_WHITE_LIST_ENABLE_WIDTH,
            entry_pi->data.ACL_filter_info.filter_Whitelist_enable,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_ACL_WHITE_LIST_ENABLE_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_SA_NO_MACH_SEND_TO_INTERNAL,
            (MEA_Uint32)(entry_pi->data.do_sa_search_only),
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_SA_NO_MACH_SEND_TO_INTERNAL;

        MEA_OS_insert_value(count_shift,
			MEA_IF_SRV_ENTRY_INNER_LEARN_FWD_TYPE,
            (MEA_Uint32)(entry_pi->data.learn_fwd_inner_type),
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_INNER_LEARN_FWD_TYPE;




        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_DSE_MASK_FILED_TYPE,
            entry_pi->data.DSE_mask_field_type,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_DSE_MASK_FILED_TYPE;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_IPMC_AWARE_VALID,
            entry_pi->data.IP_MC_Aware,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_IPMC_AWARE_VALID;

        if (MEA_ST_FWD_SUPPORT){

            MEA_OS_insert_value(count_shift,
				MEA_IF_SRV_ENTRY_MAC_INTERNAL_PROF_WIDTH(unit_i, hwUnit_i), 
                (MEA_Uint32)(entry_pi->data.FWD_DaMAC_ID ),
                retVal);
			count_shift += MEA_IF_SRV_ENTRY_MAC_INTERNAL_PROF_WIDTH(unit_i, hwUnit_i) ;

            


            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_MAC_INTERNAL_VALID_WIDTH(unit_i, hwUnit_i),
                (MEA_Uint32)((entry_pi->data.FWD_DaMAC_valid == MEA_TRUE) ? 1 : 0),
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_MAC_INTERNAL_VALID_WIDTH(unit_i, hwUnit_i);

        }
        if (MEA_VPLS_ACCESS_PORT_SUPPORT){

            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_ACCESS_PORT_ID_WIDTH,
                entry_pi->data.Access_port_id,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_ACCESS_PORT_ID_WIDTH;

            access_ADM = ((entry_pi->data.Access_port_en == MEA_TRUE) ? (entry_pi->data.Access_port_adm_en) : 1);

            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_ACCESS_ADM_EN_WIDTH,
                access_ADM,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_ACCESS_ADM_EN_WIDTH;

            MEA_OS_insert_value(count_shift,
                MEA_IF_SRV_ENTRY_ACCESS_LOCAL_SW_WIDTH,
                entry_pi->data.Access_local_sw_en,
                retVal);
            count_shift += MEA_IF_SRV_ENTRY_ACCESS_LOCAL_SW_WIDTH;

        }

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_INGRESS_FLOW_POL(unit_i, hwUnit_i),
           (entry_pi->data.Ingress_flow_policer_ProfileId) ,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_INGRESS_FLOW_POL(unit_i, hwUnit_i);


        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_ACL_PROF_VALID_WIDTH,
            (entry_pi->data.ACL_Prof_valid) ,
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_ACL_PROF_VALID_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_ACL_PROF_WIDTH,
            (entry_pi->data.ACL_Prof),
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_ACL_PROF_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_HPM_MASK_PROF_WIDTH,
            (entry_pi->data.HPM_prof_mask),
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_HPM_MASK_PROF_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_HPM_PROF_WIDTH,
            (entry_pi->data.HPM_profileId),
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_HPM_PROF_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_UEPDN_WIDTH,
            (entry_pi->data.Uepdn_id),
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_UEPDN_WIDTH;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_FWD_SELECT_KEY,
            (entry_pi->data.fwd_select_key),
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_FWD_SELECT_KEY;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_SEGMENT_SWITCH_PROF,
            (entry_pi->data.segment_prof),
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_SEGMENT_SWITCH_PROF;

        MEA_OS_insert_value(count_shift,
            MEA_IF_SRV_ENTRY_HPM_IPV6_KEY,
            (entry_pi->data.HPM_ipv6_key),
            retVal);
        count_shift += MEA_IF_SRV_ENTRY_HPM_IPV6_KEY;

		MEA_OS_insert_value(count_shift,
			MEA_IF_SRV_ACL5_KEY_TYPE,
			(entry_maskprof.keyType),
			retVal);
		count_shift += MEA_IF_SRV_ACL5_KEY_TYPE;

		MEA_OS_insert_value(count_shift,
			MEA_IF_SRV_ACL5_INNER_FRAM,
			(entry_maskprof.Inner_Frame),
			retVal);
		count_shift += MEA_IF_SRV_ACL5_INNER_FRAM;

		MEA_OS_insert_value(count_shift,
			MEA_IF_SRV_ACL5_IP_MASK_PROF,
			(entry_maskprof.ip_mask_id),
			retVal);
		count_shift += MEA_IF_SRV_ACL5_IP_MASK_PROF;
        
		MEA_OS_insert_value(count_shift,
			MEA_IF_SRV_ACL5_KEY_MASK_PROF,
			(entry_maskprof.key_mask_id),
			retVal);
		count_shift += MEA_IF_SRV_ACL5_KEY_MASK_PROF;

		MEA_OS_insert_value(count_shift,
			MEA_IF_SRV_ACL5_RANGE_PROF,
			(entry_maskprof.range_mask_id),
			retVal);
		count_shift += MEA_IF_SRV_ACL5_RANGE_PROF;

		MEA_OS_insert_value(count_shift,
			MEA_IF_SRV_ACL5_DO,
			(entry_maskprof.valid),  // acl5_do
			retVal);
		count_shift += MEA_IF_SRV_ACL5_DO;
		
		MEA_OS_insert_value(count_shift,
			MEA_IF_SRV_LPM_VRF,
			(entry_pi->data.lpm_vrf),  // MEA_IF_SRV_LPM_VRF
			retVal);
		count_shift += MEA_IF_SRV_LPM_VRF;

     
    } /*data valid*/







    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_ServiceEntry_UpdateHwEntry>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void      mea_ServiceEntry_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4,
    MEA_funcParam arg5)
{

    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;

    MEA_db_Hw_Info_t       *Hw_Info = (MEA_db_Hw_Info_t*)arg2;

    //MEA_db_HwUnit_t        hwUnit_i  = (MEA_db_HwUnit_t)(long)arg2;
    //MEA_db_HwId_t          hwId_i    = (MEA_db_HwUnit_t)((long)arg3);
    MEA_Uint32             numofregs = (MEA_Uint32)((long)arg3);
    MEA_Uint32             *data = (MEA_Uint32*)arg4;
    MEA_Uint32 i;


    if (data == NULL){
        return;
    }

    /* Write to Hardware */
    for (i = 0; i < numofregs; i++) {
        MEA_API_WriteReg(unit_i, MEA_IF_WRITE_DATA_REG(0), data[i], MEA_MODULE_IF);

    }


    /* Update Hardware shadow */
    if (mea_db_Set_Hw_Regs(unit_i,
        MEA_ServiceEntry_db,
        Hw_Info->HwUnit,
        Hw_Info->HwId,
        (MEA_Uint16)numofregs,
        data) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
            __FUNCTION__,
            Hw_Info->HwUnit,
            Hw_Info->HwId);
    }


}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_ServiceEntry_UpdateHw>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_ServiceEntry_UpdateHw(MEA_Unit_t                  unit_i,
    MEA_Service_t               serviceId_i,
    MEA_db_HwUnit_t             hwUnit_i,
    MEA_db_HwId_t               hwId_i,
    MEA_Service_Entry_dbt      *new_entry_pi,
    MEA_Service_Entry_dbt      *old_entry_pi)
{
    MEA_ind_write_t          ind_write;
    MEA_db_SwHw_Entry_dbt    db_SwHw_entry;
    MEA_Uint32               val[10];
    MEA_Uint32               numofRegs;
    MEA_db_Hw_Info_t       Hw_Info;
    MEA_Service_Info_dbt      entry_info;
    

    /* check if we have any changes */
    if ((old_entry_pi) &&
        (MEA_OS_memcmp(new_entry_pi,
        old_entry_pi,
        sizeof(*new_entry_pi)) == 0)) {
        return MEA_OK;
    }

    MEA_OS_memset(&val, 0, sizeof(val));
    MEA_OS_memset(&entry_info, 0, sizeof(entry_info));

    MEA_API_Get_Service_InfoDb(unit_i, serviceId_i, &entry_info);


	if (new_entry_pi->data.valid) {
	   /* Check if we need  to allocate new entry */
	   if (mea_db_Get_SwHw(unit_i,
	  				       MEA_ServiceEntry_db,
                           (MEA_db_SwId_t)entry_info.Sid_Internal,
        				   hwUnit_i,
						   &db_SwHw_entry) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                 "%s - mea_db_Get_SwHw failed (id=%d)\n",
						     __FUNCTION__,serviceId_i);
		   return MEA_ERROR;
	   }
	   if (!(db_SwHw_entry.valid)) {
		   if (mea_db_Create(unit_i,
			                 MEA_ServiceEntry_db,
                             (MEA_db_SwId_t)entry_info.Sid_Internal,
						     hwUnit_i,
						     &hwId_i) != MEA_OK) {
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_db_Create failed \n",__FUNCTION__);
			  return MEA_ERROR;
		   }
	   }
	

    }//valid    

    numofRegs = mea_ServiceEntry_calc_numof_regs(unit_i, hwUnit_i);

    mea_ServiceEntry_buildHWData(unit_i,
        hwUnit_i,
        hwId_i,
        new_entry_pi,
        numofRegs, /*num of reg*/
        &val[0]);


    /* build the ind_write value */
	ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_ENTRY;
	ind_write.tableOffset	= hwId_i;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_ServiceEntry_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;

    Hw_Info.HwUnit = hwUnit_i;
    Hw_Info.HwId   = hwId_i;


    ind_write.funcParam2 = (MEA_funcParam)&Hw_Info;
    

    ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
    ind_write.funcParam4 = (MEA_funcParam)&val;

 	if (MEA_API_WriteIndirect(unit_i,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed (port=%d)\n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF,new_entry_pi->key.src_port);
		return MEA_ERROR;
    }

	/* If delete then delete the Hardware database */
	if (!(new_entry_pi->data.valid)) {
		if (mea_db_Delete(unit_i,
			              MEA_ServiceEntry_db,
                          (MEA_db_SwId_t)entry_info.Sid_Internal,
						  hwUnit_i) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_db_Delete failed \n",__FUNCTION__);
			return MEA_ERROR;
		}
	}


   


    /* return to caller */
    return MEA_OK;

}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_ServiceEntry_db>                         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_ServiceEntry_db(MEA_Unit_t unit_i,
								       MEA_db_dbt* db_po) {

	if (db_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - db_po == NULL\n",__FUNCTION__);
		return MEA_ERROR;
	}

	*db_po = MEA_ServiceEntry_db;

	return MEA_OK;

}



MEA_Status mea_drv_Get_ServiceEntry_OutPorts_ByPortState(MEA_Unit_t   unit_i,
                                                         MEA_Uint16   vlan_i,
                                                         MEA_OutPorts_Entry_dbt* outPorts_i,
                                                         MEA_OutPorts_Entry_dbt* outPorts_o) 
{

    MEA_Uint32* ptr;
    ENET_QueueId_t queueId;
    ENET_Queue_dbt entry;
    MEA_Uint32 i,j,mask;
    
    MEA_PortState_te state;

    MEA_OS_memcpy(outPorts_o,outPorts_i,sizeof(*outPorts_o));
    
    for (i=0,queueId=0,ptr=&(outPorts_o->out_ports_0_31);
        i<sizeof(*outPorts_o)/sizeof(MEA_Uint32);
        i++,ptr++) {
        
        for (j=0,mask=0x00000001;j<32;j++,mask <<= 1,queueId++) {
                if (*ptr & mask) {
                    if (ENET_Get_Queue(unit_i,queueId,&entry) != MEA_OK) {
                         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s ..ENET_Get_Queue failed.");
                         return MEA_ERROR;
                    }
                    if (MEA_API_Get_PortState(unit_i,(MEA_Port_t)entry.port.id.port,vlan_i,&state)!=MEA_OK ){
                       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s ..MEA_API_Get_PortState failed.");
                        return MEA_ERROR;
                    }
                    if (state != MEA_PORT_STATE_FORWARD) {
                        *ptr &= ~mask;
                    }
                }
        }
    }

    return MEA_OK;
}


/************************************************************************/
/*            ClassifierKEY_Character                                   */
/************************************************************************/
static void mea_drv_ClassifierKEY_Character_UpdateHwEntry(MEA_funcParam arg1,
                                                            MEA_funcParam arg2,
                                                            MEA_funcParam arg3,
                                                            MEA_funcParam arg4)
{

     MEA_Unit_t              unit_i = (MEA_Unit_t)arg1;
//     MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)arg2;
//     MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)arg3;
     MEA_Characteristics_entry_HW_dbt *entry = (MEA_Characteristics_entry_HW_dbt*)arg4;
//     MEA_Uint32                  value = (MEA_Uint32)((long)arg4);


    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
    //MEA_Uint32                  count_shift;


    



    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    //count_shift=0;


    val[0] = (MEA_Uint32)entry->learn_en;
    val[0] |= (MEA_Uint32)(entry->ipv6_mask_type & 0x7 )<< 1;
    



    /* Update Hw Entry */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }



}
MEA_Status mea_drv_ClassifierKEY_Character_UpdateEntry(MEA_Unit_t    unit,
                                                       MEA_Uint16 index, 
                                                       MEA_Characteristics_entry_dbt   *entry,
                                                       MEA_Characteristics_entry_dbt   *old_entry
    )
{
   
    MEA_Characteristics_entry_HW_dbt hwEntry;
    MEA_ind_write_t ind_write;
  


    if ((MEA_VPLS_CHACTRISTIC_SUPPORT) == MEA_FALSE){
        return MEA_OK;
    }

        

    if( ((old_entry) == NULL) ||  
            (MEA_OS_memcmp(entry, old_entry, sizeof(MEA_Characteristics_entry_dbt)) != 0)
          ){
       
        MEA_OS_memset(&hwEntry, 0, sizeof(hwEntry));
        hwEntry.valid = entry->valid;

        hwEntry.learn_en       = entry->learn_en;
        hwEntry.ipv6_mask_type = entry->ipv6_mask_type;
        /* Update the Hw */
        if (hwEntry.valid == MEA_FALSE)
        {

            hwEntry.learn_en = MEA_FALSE;
            hwEntry.ipv6_mask_type = 0;
            entry->learn_en = 0;
            entry->ipv6_mask_type = 0;

        }



            


        /* build the ind_write value */
        ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TEID_ACL_PROF;
        ind_write.tableOffset = index;
        ind_write.cmdReg = MEA_IF_CMD_REG;
        ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
        ind_write.statusReg = MEA_IF_STATUS_REG;
        ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
        ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_ClassifierKEY_Character_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit;
        ind_write.funcParam4 = (MEA_funcParam)(&hwEntry);


        /* Write to the Indirect Table */
        if (MEA_API_WriteIndirect(unit, &ind_write, MEA_MODULE_IF) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
            return MEA_ERROR;
        }

    }


    return MEA_OK;
}



MEA_Status mea_drv_ClassifierKEY_Character_Init(MEA_Unit_t    unit)
{

    MEA_Uint32 size;
    if (!MEA_GW_SUPPORT){
        return MEA_OK;
    }

   
    /* Allocate PacketGen Table */
    
    
    if (!MEA_VPLS_CHACTRISTIC_SUPPORT)
        return MEA_OK;
  

    if (MEA_CharacteristicsTBL==NULL) {
        size = sizeof(MEA_Characteristics_entry_dbt);
        size = MEA_GW_Characteristics_MAX_PROF * sizeof(MEA_Characteristics_entry_dbt);
        MEA_CharacteristicsTBL = (MEA_Characteristics_entry_dbt*) MEA_OS_malloc(size);
        if (MEA_CharacteristicsTBL == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_CharacteristicsTBL failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset((MEA_CharacteristicsTBL), 0, size);
    }



    

    return MEA_OK;
}
MEA_Status mea_drv_ClassifierKEY_Character_ReInit(MEA_Unit_t    unit)
{
    MEA_Uint32 index;

    if (!MEA_GW_SUPPORT){
        return MEA_OK;
    }
    
    if (!MEA_VPLS_CHACTRISTIC_SUPPORT)
        return MEA_OK;


    for (index = 0; index < MEA_GW_Characteristics_MAX_PROF; index++)
    {
        if (MEA_CharacteristicsTBL[index].valid == MEA_FALSE)
            continue;

        if (mea_drv_ClassifierKEY_Character_UpdateEntry(unit, index, &MEA_CharacteristicsTBL[index],NULL) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  mea_drv_ClassifierKEY_Character_UpdateEntry failed (index=%d)\n",
                __FUNCTION__, index);
            return MEA_ERROR;
        }
    }


    return MEA_OK;
}

MEA_Status mea_drv_ClassifierKEY_Character_Conclude(MEA_Unit_t unit_i)
{


    if (!MEA_GW_SUPPORT){
        return MEA_OK;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude GW CharacteristicsTBL..\n");

    /* Free the table */
    if (MEA_CharacteristicsTBL != NULL) {
        MEA_OS_free(MEA_CharacteristicsTBL);
        MEA_CharacteristicsTBL = NULL;
    }

    return MEA_OK;
}


MEA_Status MEA_API_Set_ClassifierKEY_Character_Entry(MEA_Unit_t unit, 
                                                     MEA_Uint32 TEId, 
                                                     MEA_Characteristics_entry_dbt   *entry)
{
   
   
    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    
    if (!MEA_GW_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "MEA_GW_SUPPORT not Support on this HW\n");
        return ENET_ERROR;
    }
    
    if (!MEA_VPLS_CHACTRISTIC_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "ClassifierKEY not support on this HW\n");
        return ENET_ERROR;
    }
    


    if (TEId >= MEA_GW_Characteristics_MAX_PROF){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            " TEId %d >= MAX %d \n", TEId, MEA_GW_Characteristics_MAX_PROF);
        return ENET_ERROR;

    }
   
 
    if(mea_drv_ClassifierKEY_Character_UpdateEntry(unit, TEId, entry, &MEA_CharacteristicsTBL[TEId]) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                " mea_drv_ClassifierKEY_Character_UpdateEntry file TEId %d \n", TEId );
            return ENET_ERROR;
    }
       
 



       MEA_OS_memcpy(&MEA_CharacteristicsTBL[TEId], entry, sizeof(MEA_CharacteristicsTBL[0]));

    return MEA_OK;
}

MEA_Status MEA_API_Get_ClassifierKEY_Character_Entry(MEA_Unit_t   unit,
                                                     MEA_Uint32     TEId,
                                                     MEA_Characteristics_entry_dbt   *entry)
{

    
    if (entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL\n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    if (!MEA_GW_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "MEA_GW_SUPPORT not Support on this HW\n");
        return ENET_ERROR;
    }
    if (!MEA_VPLS_CHACTRISTIC_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "ClassifierKEY not support on this HW\n");
        return ENET_ERROR;
    }

    if (TEId >= MEA_GW_Characteristics_MAX_PROF){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            " TEId %d >= MAX %d \n", TEId, MEA_GW_Characteristics_MAX_PROF);
        return ENET_ERROR;

    }

    MEA_OS_memcpy(entry, &MEA_CharacteristicsTBL[TEId],sizeof(*entry));

    

    return MEA_OK;
}


