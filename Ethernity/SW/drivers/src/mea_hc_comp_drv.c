/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/






/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#if 1
#ifdef MEA_OS_LINUX
#ifdef MEA_OS_OC
#include <linux/unistd.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#endif
#else
#include <time.h>
#endif
#endif
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_if_regs.h"
#include "mea_hc_comp_drv.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_HC_L2_PROTCOL 32
#define MEA_HC_L3_PROTCOL 32

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef enum {
    MEA_HC_COUNTER_RMON_INDEX_0 =0,
    MEA_HC_COUNTER_RMON_INDEX_1,
    MEA_HC_COUNTER_RMON_INDEX_2,
    MEA_HC_COUNTER_RMON_INDEX_3, /*NA*/
    MEA_HC_COUNTER_RMON_INDEX_4,
    MEA_HC_COUNTER_RMON_INDEX_5, /* MTU drop*/
    MEA_HC_COUNTER_RMON_INDEX_6,
    MEA_HC_COUNTER_RMON_INDEX_7,
    MEA_HC_COUNTER_RMON_INDEX_8,
    MEA_HC_COUNTER_RMON_INDEX_9,
    MEA_HC_COUNTER_RMON_INDEX_10,
    MEA_HC_COUNTER_RMON_INDEX_11,
    MEA_HC_COUNTER_RMON_INDEX_12,
    MEA_HC_COUNTER_RMON_INDEX_13,
    MEA_HC_COUNTER_RMON_INDEX_14,
    MEA_HC_COUNTER_RMON_INDEX_15,
    MEA_HC_COUNTER_RMON_INDEX_LAST  

}MEA_HC_if_Counter_Rmon_index;




typedef struct{
    MEA_HC_L2L3_CompInfo_dbt data;
}MEA_HC_L2L3_Comp_proto_entry_dbt;


typedef struct{
    MEA_Bool valid;
    MEA_Uint32 num_of_owners;
    MEA_HDC_Data_dbt data;
    MEA_Uint32   byteCount;
}MEA_HC_Comp_prof_entry_dbt;


typedef struct{ 

    union {
        struct{
            MEA_Uint32 RX_NoDeCompressed_Pkts;
            MEA_Uint32 TX_NoCompressed_Pkts;
            MEA_Uint32 RX_Bytes;
            MEA_Uint32 TX_Bytes;
        }val;
        MEA_Uint32 index_0_regs[4];
    }offset_0;


    union {
        struct{
            MEA_Uint32 RX_DeCompressed_Pkts;
            MEA_Uint32 TX_Compressed_Pkts;
            MEA_Uint32 RX_drop_bytes;
            MEA_Uint32 TX_actual_bytes; 
        }val;
        MEA_Uint32 index_1_regs[4];
    }offset_1;
    union {
        struct{

            MEA_Uint32 RX_learn_Pkts;
            MEA_Uint32 TX_learn_Pkts;
            MEA_Uint32 NA_2;         
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_2_regs[4];
    }offset_2;
#if 0    
    union {
        struct{

            MEA_Uint32 NA_0;
            MEA_Uint32 NA_1;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_3_regs[4];
    }offset_3;
    union {
        struct{

            MEA_Uint32 Phy_Error_Pkts;
            MEA_Uint32 Tx_Underrun_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_4_regs[4];
    }offset_4;
    union {
        struct{

            MEA_Uint32 Rx_Oversize_Pkts; /*MTU*/
            MEA_Uint32 Tx_Oversize_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_5_regs[4];
    }offset_5;
#endif    

    union {
        struct{

            MEA_Uint32 Rx_DeCompressedCRC_Err; 
            MEA_Uint32 Tx_CompressedCRC_Err;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_6_regs[4];
    }offset_6;
#if 0    

    union {
        struct{

            MEA_Uint32 NA_0; 
            MEA_Uint32 NA_1;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_7_regs[4];
    }offset_7;
#endif
    union {
        struct{
            MEA_Uint32 Rx_64Octets_Pkts; 
            MEA_Uint32 Tx_64Octets_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_8_regs[4];
    }offset_8;
    union {
        struct{
            MEA_Uint32 Rx_65to127Octets_Pkts; 
            MEA_Uint32 Tx_65to127Octets_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_9_regs[4];
    }offset_9;
    union {
        struct{
            MEA_Uint32 Rx_128to255Octets_Pkts; 
            MEA_Uint32 Tx_128to255Octets_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_10_regs[4];
    }offset_10;
    union {
        struct{
            MEA_Uint32 Rx_256to511Octets_Pkts; 
            MEA_Uint32 Tx_256to511Octets_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_11_regs[4];
    }offset_11;
    union {
        struct{
            MEA_Uint32 Rx_512to1023Octets_Pkts; 
            MEA_Uint32 Tx_512to1023Octets_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_12_regs[4];
    }offset_12;
    union {
        struct{
            MEA_Uint32 Rx_1024to1518Octets_Pkts; 
            MEA_Uint32 Tx_1024to1518Octets_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_13_regs[4];
    }offset_13;
    union {
        struct{
            MEA_Uint32 Rx_1519to2047Octets_Pkts; 
            MEA_Uint32 Tx_1519to2047Octets_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_14_regs[4];
    }offset_14;
    union {
        struct{
            MEA_Uint32 Rx_2048toMaxOctets_Pkts; 
            MEA_Uint32 Tx_2048toMaxOctets_Pkts;
            MEA_Uint32 NA_2; 
            MEA_Uint32 NA_3;
        }val;
        MEA_Uint32 index_15_regs[4];
    }offset_15;

} MEA_Counter_HcRMON_driver_dbt;

typedef struct{
    MEA_Bool  valid;
    MEA_Uint32 state;

}mea_HC_CompState_type_dbt;

typedef struct{
    MEA_Bool  valid;
 }mea_HC_CompBlock_type_dbt;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

static MEA_Counters_RMON_HC_dbt        *MEA_Counters_HcRMON_Table = NULL;
static MEA_HC_Comp_prof_entry_dbt       *MEA_HC_Comp_prof_info_Table   = NULL;
static MEA_HC_L2L3_Comp_proto_entry_dbt *MEA_HC_Comp_L2L3_info_Table   = NULL;

/*state*/
mea_HC_CompState_type_dbt *MEA_HeaderCompressState_Table = NULL;

mea_HC_CompBlock_type_dbt *MEA_HeaderCompressBlock_Table = NULL;


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/




static MEA_Status mea_drv_HC_Comp_l2l3_prof_UpdateHw(MEA_Unit_t                  unit_i,
                                                     MEA_HC_key_proto_t              *key,
                                                     MEA_HC_L2L3_CompInfo_dbt    *new_entry_pi,
                                                     MEA_HC_L2L3_CompInfo_dbt    *old_entry_pi);

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/




static void mea_drv_HC_Comp_Bitmap_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry;




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}


static MEA_Status mea_drv_HC_Comp_Bitmap_prof_UpdateHw(MEA_Unit_t                  unit_i,
                                                         MEA_Uint16             id_i,
                                                         MEA_HDC_Data_dbt    *new_entry_pi,
                                                         MEA_HDC_Data_dbt    *old_entry_pi)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32              value,index;
    MEA_ind_write_t      ind_write;


    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

        /* check if we have any changes */
        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi))== 0) ) { 
                return MEA_OK;
        }



        /* Prepare ind_write structure */
        ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_HC_COMPRESS_BIT_PROF;
        ind_write.tableOffset   = id_i; 
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_HC_Comp_Bitmap_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
        //ind_write.funcParam3    = (MEA_funcParam);



        for (index=0;index< MEA_HD_NM_WORD;index++)
        {

            ind_write.tableOffset   = (id_i* MEA_HD_NM_WORD)+index ;   
            value=new_entry_pi->bit_Of_byte_comp[index];
            ind_write.funcParam4 = (MEA_funcParam)((long)value);

            /* Write to the Indirect Table */
            if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                    __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
                return ENET_ERROR;
            }
        }


        /* Return to caller */
        return MEA_OK;
}


static void mea_drv_HC_Comp_ByteCnt_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry;





    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}
static MEA_Status mea_drv_HC_Comp_ByteCnt_prof_UpdateHw(MEA_Unit_t                  unit_i,
                                                        MEA_Uint16             id_i,
                                                        MEA_Uint32             setByteCnt)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32              value;
    MEA_ind_write_t      ind_write;


    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)





        /* Prepare ind_write structure */
    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_HC_PROF_COMPRESS_BYTE_COUNT;
    ind_write.tableOffset   = id_i; 
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg     = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_HC_Comp_ByteCnt_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
    //ind_write.funcParam3    = (MEA_funcParam);





    ind_write.tableOffset   = (id_i) ;
    value=setByteCnt;
    ind_write.funcParam4 = (MEA_funcParam)((long)value);

    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
        return ENET_ERROR;
    }



    /* Return to caller */
    return MEA_OK;
}






static MEA_Bool mea_drv_HC_Comp_prof_find_free(MEA_Unit_t       unit_i, 
                                                MEA_Uint16       *id_io)
{
    MEA_Uint16 i;

    for(i=0;i< MEA_HC_DECOMP_MAX_PROF; i++) {
        if (MEA_HC_Comp_prof_info_Table   [i].valid == MEA_FALSE){
            *id_io=i; 
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;

}



static MEA_Bool mea_drv_HC_Comp_prof_IsRange(MEA_Unit_t     unit_i,
                                               MEA_Uint16     id_i)
{

    if ((id_i) >= MEA_HC_DECOMP_MAX_PROF){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

static MEA_Status mea_drv_HC_Comp_prof_IsExist(MEA_Unit_t     unit_i,
                                                MEA_Uint16 id_i,
                                                MEA_Bool *exist)
{
    if(exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  exist = NULL \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    *exist=MEA_FALSE;

    if(mea_drv_HC_Comp_prof_IsRange(unit_i,id_i)!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed  for id=%d\n",__FUNCTION__,id_i); 
        return MEA_ERROR;
    }

    if( MEA_HC_Comp_prof_info_Table   [id_i].valid){
        *exist=MEA_TRUE;
        return MEA_OK;
    }

    return MEA_OK;
}



static MEA_Status mea_drv_HC_Comp_prof_check_parameters(MEA_Unit_t             unit_i,
                                                        MEA_HDC_Data_dbt      *entry_pi)
{
    MEA_Globals_Entry_dbt Globalentry;
    MEA_Uint32 reg;

    MEA_OS_memset(&Globalentry,0,sizeof(Globalentry));
    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&Globalentry) !=MEA_OK) {

        return MEA_ERROR;
    }
    if(Globalentry.HC_Offset_Mode == MEA_FALSE){
        if(entry_pi->start_offset !=0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed start_offset  need to be zero \n",__FUNCTION__); 
            return MEA_ERROR;
        }
    }


    if(entry_pi->start_offset >= 64){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed start_offset  >= 64 \n",__FUNCTION__,entry_pi->start_offset); 
        return MEA_ERROR;
    }
    if((entry_pi->bit_Of_byte_comp[0] == 0) && (entry_pi->bit_Of_byte_comp[1] == 0)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed no configure of the compress \n",__FUNCTION__,entry_pi->start_offset); 
        return MEA_ERROR;
    }

   if((entry_pi->start_offset % 8) !=0 ){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  start_offset %d  need to be multiple of 8 \n",__FUNCTION__,entry_pi->start_offset); 
       return MEA_ERROR;
   
   
   }
   reg = (entry_pi->bit_Of_byte_comp[1]) >> 29;

   if (reg > 0){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - should have ZERO byte 62:64 \n", __FUNCTION__);
       return MEA_ERROR;
   }

   


    return MEA_OK;
}

static MEA_Status mea_drv_HC_Comp_prof_add_owner(MEA_Unit_t       unit_i ,
                                                   MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_HC_Comp_prof_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }

    MEA_HC_Comp_prof_info_Table   [id_i].num_of_owners++;

    return MEA_OK;
}
static MEA_Status mea_drv_HC_Comp_prof_delete_owner(MEA_Unit_t       unit_i ,
                                                    MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_HC_Comp_prof_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }

    if(MEA_HC_Comp_prof_info_Table   [id_i].num_of_owners==1){
        MEA_HC_Comp_prof_info_Table   [id_i].valid=MEA_FALSE;
        MEA_HC_Comp_prof_info_Table   [id_i].num_of_owners=0;
        return MEA_OK;

    }


    MEA_HC_Comp_prof_info_Table   [id_i].num_of_owners--;

    return MEA_OK;
}

MEA_Status mea_drv_HC_Comp_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size,i;

    if (!MEA_HC_SUPPORT){

        return MEA_OK;   
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize HC Compression ...\n");


    /* Allocate Table */
    size = MEA_HC_DECOMP_MAX_PROF * sizeof(MEA_HC_Comp_prof_entry_dbt   );
    if (size != 0) {
        MEA_HC_Comp_prof_info_Table    = (MEA_HC_Comp_prof_entry_dbt   *)MEA_OS_malloc(size);
        if (MEA_HC_Comp_prof_info_Table    == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_HC_Comp_prof_info_Table    failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_HC_Comp_prof_info_Table   [0]),0,size);
    }

    size = MEA_HC_L2_PROTCOL * MEA_HC_L3_PROTCOL* sizeof(MEA_HC_L2L3_Comp_proto_entry_dbt   );
    if (size != 0) {
        MEA_HC_Comp_L2L3_info_Table    = (MEA_HC_L2L3_Comp_proto_entry_dbt   *)MEA_OS_malloc(size);
        if (MEA_HC_Comp_prof_info_Table    == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_HC_Comp_L2L3_info_Table    failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_HC_Comp_L2L3_info_Table[0]),0,size);
    }
    for(i=0;i< (MEA_HC_L2_PROTCOL * MEA_HC_L3_PROTCOL);i++){
        MEA_HC_Comp_L2L3_info_Table[i].data.enable= MEA_FALSE;
    }
    // Hw set zero on the init



    return MEA_OK;
}
MEA_Status mea_drv_HC_Comp_RInit(MEA_Unit_t       unit_i)
{
    MEA_Uint16 id;
    MEA_HC_key_proto_t              key;

    if (!MEA_HC_SUPPORT){

        return MEA_OK;   
    }

    for (id=0;id<MEA_HC_COMP_MAX_PROF;id++) {
        if (MEA_HC_Comp_prof_info_Table   [id].valid){

            if(  MEA_API_HC_Comp_Set_Profile(unit_i,
                id,
                &(MEA_HC_Comp_prof_info_Table   [id].data),
                MEA_TRUE) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  MEA_API_HC_Comp_Set_Profile failed (id=%d)\n",__FUNCTION__,id);
                    return MEA_ERROR;
            }
        }
    }

    for (id=0;id< MEA_HC_L2_PROTCOL * MEA_HC_L3_PROTCOL;id++)
    {
       
        if(MEA_HC_Comp_L2L3_info_Table[id].data.enable == MEA_TRUE){
            key.L2Type = ((id>>5) & 0x1f);
            key.L3Type = id & 0x1f;
            if(mea_drv_HC_Comp_l2l3_prof_UpdateHw(unit_i,&key,&MEA_HC_Comp_L2L3_info_Table[id].data,NULL)!=MEA_OK)
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_HC_Comp_l2l3_prof_UpdateHw fail\n",__FUNCTION__);
                return MEA_ERROR;
            }
        }
    }




    return MEA_OK;
}

MEA_Status mea_drv_HC_Comp_Conclude(MEA_Unit_t       unit_i)
{
    /* Check if support */
    if (!MEA_HC_SUPPORT){
        return MEA_OK;   
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude HC Compression..\n");

    /* Free the table */
    if (MEA_HC_Comp_prof_info_Table    != NULL) {
        MEA_OS_free(MEA_HC_Comp_prof_info_Table   );
        MEA_HC_Comp_prof_info_Table    = NULL;
    }

    return MEA_OK;
}
/************************************************************************/
/*  MEA_API_HC_Comp profile                                            */
/************************************************************************/


MEA_Status MEA_API_HC_Comp_Profile_IsExist(MEA_Unit_t                      unit_i,
                                        MEA_Uint16                       id_i,
                                        MEA_Bool                        *exist)
{
    /* Check if support */
    if (!MEA_HC_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HC_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    
    return mea_drv_HC_Comp_prof_IsExist(unit_i,id_i,exist);
}


MEA_Status MEA_API_HC_Comp_Create_Profile(MEA_Unit_t                unit_i,
                                            MEA_Uint16                *id_io,
                                            MEA_HDC_Data_dbt          *entry_pi)
{
    MEA_Bool    exist;
    MEA_Uint32  ByteCnt=0,i;
    MEA_Uint8   byteIndex[128];
    MEA_Uint32  LasBitOn;
    MEA_Uint32  minCompressPacket;
    MEA_Uint32  SetValue=0;
    


    MEA_API_LOG

        /* Check if support */
    if (!MEA_HC_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HC_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
        
        
        MEA_OS_memset(&byteIndex[0],0,sizeof(byteIndex));


    /* check parameter */
    if(mea_drv_HC_Comp_prof_check_parameters(unit_i,entry_pi) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_HC_Comp_prof_check_parameters failed \n",__FUNCTION__); 
        return MEA_ERROR;
    }

    /* Look for  id */
    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID){
        /*check if the id exist*/
        if (mea_drv_HC_Comp_prof_IsExist(unit_i,*id_io,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Comp_prof_IsExist failed (*id_io=%d\n",
                __FUNCTION__,*id_io);
            return MEA_ERROR;
        }
        if (exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - *Id_io %d is already exist\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
        }

    }else {
        /*find new place*/
        if (mea_drv_HC_Comp_prof_find_free(unit_i,id_io)!=MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Comp_prof_find_free failed\n",__FUNCTION__);
            return MEA_ERROR;
        }
    }


    for (i=0;i< MEA_HD_NM_WORD;i++) 
    {
        ByteCnt+= MEA_OS_NumOfu32bitSet(entry_pi->bit_Of_byte_comp[i]);
    }

    if(entry_pi->bit_Of_byte_comp[1]!=0){
          LasBitOn=msb32_idx(entry_pi->bit_Of_byte_comp[1]);
          LasBitOn+=32;
          
    }else{
         LasBitOn=msb32_idx(entry_pi->bit_Of_byte_comp[0]);
         
    }
    LasBitOn+=1;

    if((entry_pi->start_offset +LasBitOn+4 )  >= 132){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_pi->start_offset is out of range\n",__FUNCTION__);
        return MEA_ERROR;
    }
   
    minCompressPacket= (entry_pi->start_offset +LasBitOn+4 ) ;
    if((64+ByteCnt-4) >= minCompressPacket){
       minCompressPacket=(64+ByteCnt-4);
    }

    
    if( mea_drv_HC_Comp_Bitmap_prof_UpdateHw(unit_i,
        *id_io,
        entry_pi,
        NULL)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Comp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
    }
   
   SetValue = 0;
   SetValue = ByteCnt;
   SetValue |= minCompressPacket<<8;
   SetValue |= entry_pi->start_offset<<16;

    if(mea_drv_HC_Comp_ByteCnt_prof_UpdateHw(unit_i,*id_io,SetValue)!= MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_HC_Comp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,*id_io);
        return MEA_ERROR;
    }


    MEA_API_Clear_Counters_HC_RMON (unit_i,*id_io);

    MEA_HC_Comp_prof_info_Table   [*id_io].valid         = MEA_TRUE;
    MEA_HC_Comp_prof_info_Table   [*id_io].num_of_owners = 1;
    MEA_HC_Comp_prof_info_Table   [*id_io].byteCount      = ByteCnt;
    
    MEA_OS_memcpy(&(MEA_HC_Comp_prof_info_Table   [*id_io].data),entry_pi,sizeof(MEA_HDC_Data_dbt));



    return MEA_OK;

}


MEA_Status MEA_API_HC_Comp_Set_Profile(MEA_Unit_t                     unit_i,
    MEA_Uint16                      id_i,
    MEA_HDC_Data_dbt               *entry_pi,
    MEA_Bool                        force)
{
    MEA_Bool    exist;
    MEA_Uint32  ByteCnt=0,i;
   // MEA_Uint8   count=0;
    
    MEA_Uint32  LasBitOn;
    
    MEA_Uint32  minCompressPacket;
    MEA_Uint32  SetValue=0;

    MEA_API_LOG

        /* Check if support */
        if (!MEA_HC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }



        /*check if the id exist*/
        if (mea_drv_HC_Comp_prof_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Comp_prof_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - stream %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }

        /* check parameter*/
        if(mea_drv_HC_Comp_prof_check_parameters(unit_i,entry_pi)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_HC_Comp_prof_check_parameters filed \n",__FUNCTION__); 
            return MEA_ERROR;
        }

        if(force == MEA_FALSE){
            if ((MEA_OS_memcmp(entry_pi,
                &MEA_HC_Comp_prof_info_Table   [id_i].data,
                sizeof(*entry_pi))== 0) ) { 
                    return MEA_OK;
            }
        }

        for (i=0;i< MEA_HD_NM_WORD;i++) 
        {
            ByteCnt+= MEA_OS_NumOfu32bitSet(entry_pi->bit_Of_byte_comp[i]);
        }

        if(entry_pi->bit_Of_byte_comp[1]!=0){
            LasBitOn=msb32_idx(entry_pi->bit_Of_byte_comp[1]);
            LasBitOn+=32;
        }else{
            LasBitOn=msb32_idx(entry_pi->bit_Of_byte_comp[0]);
        }
        LasBitOn+=1;

        if((entry_pi->start_offset +LasBitOn+4 )  >= 132){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry_pi->start_offset is out of range\n",__FUNCTION__);
            return MEA_ERROR;
        }

        minCompressPacket= (entry_pi->start_offset +LasBitOn+4 ) ;
        if((64+ByteCnt-4) >= minCompressPacket){
            minCompressPacket=(64+ByteCnt-4);
        }

        if( mea_drv_HC_Comp_Bitmap_prof_UpdateHw(unit_i,
            id_i,
            entry_pi,
            (force==MEA_TRUE) ? (NULL) : (&MEA_HC_Comp_prof_info_Table   [id_i].data))!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_HC_Comp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
                return MEA_ERROR;
        }
       
        SetValue = 0;
        SetValue = ByteCnt;
        SetValue |= minCompressPacket<<8;
        SetValue |= entry_pi->start_offset<<16;

        if(mea_drv_HC_Comp_ByteCnt_prof_UpdateHw(unit_i,id_i,SetValue)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Comp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }







        MEA_OS_memcpy(&(MEA_HC_Comp_prof_info_Table   [id_i].data),
            entry_pi,
            sizeof(MEA_HC_Comp_prof_info_Table   [id_i].data));

        MEA_HC_Comp_prof_info_Table   [id_i].byteCount=ByteCnt;



        return MEA_OK;
}


MEA_Status MEA_API_HC_Comp_Delete_Profile(MEA_Unit_t                      unit_i,
    MEA_Uint16                       id_i)
{
    MEA_Bool                    exist;
    MEA_HDC_Data_dbt            entry_pi;
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_HC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        /*check if the id exist*/
        if (mea_drv_HC_Comp_prof_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Comp_prof_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - stream %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        if(MEA_HC_Comp_prof_info_Table   [id_i].num_of_owners >1){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - need to delete all the reference to this profile\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }

        /*Write to Hw All zero*/

        MEA_OS_memset(&entry_pi,0,sizeof(entry_pi));

        if( mea_drv_HC_Comp_Bitmap_prof_UpdateHw(unit_i,
            id_i,
            &entry_pi,
            NULL)!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_HC_Comp_Bitmap_prof_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
                return MEA_ERROR;
        }





        MEA_OS_memset(&MEA_HC_Comp_prof_info_Table   [id_i].data,0,sizeof(MEA_HC_Comp_prof_info_Table   [0].data));

        MEA_HC_Comp_prof_info_Table   [id_i].valid = MEA_FALSE;
        MEA_HC_Comp_prof_info_Table   [id_i].num_of_owners=0;
        MEA_HC_Comp_prof_info_Table   [id_i].byteCount = 0;

        /* Return to caller */
        return MEA_OK;
}

MEA_Status MEA_API_HC_Comp_Get_Profile(MEA_Unit_t                          unit_i,
    MEA_Uint16                           id_i,
    MEA_HDC_Data_dbt                   *entry_po)
{
    MEA_Bool    exist; 

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_HC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        /*check if the id exist*/
        if (mea_drv_HC_Comp_prof_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Comp_prof_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - id %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if(entry_po==NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry_po is null\n",__FUNCTION__,id_i);
            return MEA_ERROR;

        }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        /* Copy to caller structure */
        MEA_OS_memset(entry_po,0,sizeof(*entry_po));
        MEA_OS_memcpy(entry_po ,
            &(MEA_HC_Comp_prof_info_Table   [id_i].data),
            sizeof(*entry_po));

        /* Return to caller */
        return MEA_OK;

}

MEA_Status MEA_API_HC_Comp_GetFirst_Profile(MEA_Unit_t                         unit_i,
    MEA_Uint16                        *id_o,
    MEA_HDC_Data_dbt                  *entry_po, /* Can't be NULL */
    MEA_Bool                          *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_HC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - TDM not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        if (id_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_o)=0;
            (*id_o) < MEA_HC_DECOMP_MAX_PROF; 
            (*id_o)++ ) {
                if (MEA_HC_Comp_prof_info_Table   [(*id_o)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }

                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_HC_Comp_prof_info_Table   [(*id_o)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;

}

MEA_Status MEA_API_HC_Comp_GetNext_Profile(MEA_Unit_t                unit_i,
    MEA_Uint16                        *id_io,
    MEA_HDC_Data_dbt                  *entry_po, /* Can't be NULL */
    MEA_Bool                          *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_HC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        if (id_io == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_io)++;
            (*id_io) < MEA_HC_DECOMP_MAX_PROF; 
            (*id_io)++ ) {
                if(MEA_HC_Comp_prof_info_Table   [(*id_io)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }
                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_HC_Comp_prof_info_Table   [(*id_io)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;
}



/************************************************************************/
/*      HC_Comp_l2l3                                                    */
/************************************************************************/

static void mea_drv_HC_Comp_l2l3_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    //count_shift=0;

    val[0] = entry;




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}

static MEA_Status mea_drv_HC_Comp_l2l3_prof_UpdateHw(MEA_Unit_t                  unit_i,
    MEA_HC_key_proto_t              *key,
    MEA_HC_L2L3_CompInfo_dbt    *new_entry_pi,
    MEA_HC_L2L3_CompInfo_dbt    *old_entry_pi)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32              value;
    MEA_ind_write_t      ind_write;


    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

        /* check if we have any changes */
        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi))== 0) ) { 
                return MEA_OK;
        }

        
       

        /* Prepare ind_write structure */
        ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_HC_L2L3_PROF_ID;
#ifdef MEA_HC_SUPPORT_L3
        ind_write.tableOffset   = (key->L2Type<<5) | (key->L3Type); 
#else
        ind_write.tableOffset   = key->L2Type  ;
#endif

        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_HC_Comp_l2l3_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
        //ind_write.funcParam3    = (MEA_Uint32);






        value= (new_entry_pi->enable<<8) | (new_entry_pi->Compress_Prof_id);

        ind_write.funcParam4 = (MEA_funcParam)((long)value);

        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }



        /* Return to caller */
        return MEA_OK;
}

static MEA_Status mea_drv_HC_Comp_l2l3_check_parameters(MEA_Unit_t             unit_i,
    MEA_HC_L2L3_CompInfo_dbt      *entry)
{
    MEA_Bool exist;
    if(entry==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry is null\n",__FUNCTION__);
        return MEA_ERROR;

    }

    if(entry->enable == MEA_TRUE){
        /*check the profile set */
        if(mea_drv_HC_Comp_prof_IsExist(unit_i,entry->Compress_Prof_id,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -error on mea_drv_HC_Comp_prof_IsExist\n",__FUNCTION__);
            return MEA_ERROR;
        }
        if(exist==MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Comp_prof %d not Exist\n",__FUNCTION__,entry->Compress_Prof_id);
            return MEA_ERROR;
        }


    }

    return MEA_OK;
}


MEA_Status MEA_API_HC_Compress_Protocol_IsExist(MEA_Unit_t                      unit_i,
    MEA_HC_key_proto_t              *key,
    MEA_Bool                        *exist)
{
    MEA_Uint32 offsetId;
    
    if (!MEA_HC_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HC_SUPPORT not support \n",
            __FUNCTION__);
        *exist = MEA_FALSE;
        return MEA_ERROR;
    }

    offsetId=(key->L2Type<<5)| (key->L3Type);
    if(offsetId > (MEA_HC_L2_PROTCOL* MEA_HC_L3_PROTCOL) )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - The key offset is out of range \n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(MEA_HC_Comp_L2L3_info_Table[offsetId].data.enable == MEA_TRUE)
        *exist=MEA_TRUE;
    else
        *exist=MEA_FALSE;

    
    return MEA_OK;
}

MEA_Status MEA_API_HC_Set_Compress_Protocol(MEA_Unit_t                     unit,
    MEA_HC_key_proto_t              *key,
    MEA_HC_L2L3_CompInfo_dbt        *entry)
{

    MEA_Uint32 offsetId;
    

    MEA_API_LOG
        /* Check if support */
        if (!MEA_HC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        if(key==NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - key is null\n",__FUNCTION__);
            return MEA_ERROR;

        }

        if(entry==NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry is null\n",__FUNCTION__);
            return MEA_ERROR;

        }

        /*****************************************/
        /* check if the previous was set         */
        /*****************************************/
#ifdef MEA_HC_SUPPORT_L3        
        offsetId=(key->L2Type<<5)| (key->L3Type);
        if(offsetId > (32*32) ){
#else
        offsetId=key->L2Type ;  
        if(offsetId >= (32) ){

#endif
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - The key offset is out of range \n",__FUNCTION__);
            return MEA_ERROR;
        }




        /*check that the profile is set if is enable */
        /* if not then we need to set zero the hw.   */

        if(mea_drv_HC_Comp_l2l3_check_parameters(unit,entry)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry is null\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if(mea_drv_HC_Comp_l2l3_prof_UpdateHw(unit,key,entry,&MEA_HC_Comp_L2L3_info_Table[offsetId].data)!=MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_HC_Comp_l2l3_prof_UpdateHw fail\n",__FUNCTION__);
            return MEA_ERROR;
        }
        if(MEA_HC_Comp_L2L3_info_Table[offsetId].data.enable == MEA_TRUE){
            //delete owner
            mea_drv_HC_Comp_prof_delete_owner(unit,MEA_HC_Comp_L2L3_info_Table[offsetId].data.Compress_Prof_id);
        }

        if(entry->enable){
            //add owner
            mea_drv_HC_Comp_prof_add_owner(unit,entry->Compress_Prof_id);
        }
        /*update*/
        MEA_OS_memcpy(&MEA_HC_Comp_L2L3_info_Table[offsetId].data,entry,sizeof(MEA_HC_Comp_L2L3_info_Table[0].data));

      

        return MEA_OK;
}


MEA_Status MEA_API_HC_Get_Compress_Protocol(MEA_Unit_t                unit,
                                            MEA_HC_key_proto_t            *key,
                                            MEA_HC_L2L3_CompInfo_dbt      *entry   )
{
    

    MEA_Uint32 offsetId;
    


    MEA_API_LOG
    /* Check if support */
    if (!MEA_HC_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_HC_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }

    if(key==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key is null\n",__FUNCTION__);
        return MEA_ERROR;

    }

    if(entry==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry is null\n",__FUNCTION__);
        return MEA_ERROR;

    }
    offsetId=(key->L2Type<<5)| (key->L3Type);
    if(MEA_HC_Comp_L2L3_info_Table[offsetId].data.enable == MEA_FALSE  ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - for this protocol is not valid profile\n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(entry,&MEA_HC_Comp_L2L3_info_Table[offsetId].data,sizeof(*entry));

    return MEA_OK;
}





MEA_Status MEA_API_HC_Comp_GetFirst_Protocol(MEA_Unit_t                         unit_i,
    MEA_HC_key_proto_t                        *key,/* Can't be NULL */
    MEA_HC_L2L3_CompInfo_dbt                  *entry_po, /* Can't be NULL */
    MEA_Bool                          *found_o)
{
    
    MEA_Uint32 index;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_HC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - TDM not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        if (key == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - key == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        
        
        
        for ( (index)=0;
            (index) < MEA_HC_L2_PROTCOL * MEA_HC_L3_PROTCOL   ; 
            (index)++ ) {
                
                        if (MEA_HC_Comp_L2L3_info_Table   [index].data.enable == MEA_TRUE) {
                            if (found_o) {
                                *found_o= MEA_TRUE;
                            }

                            if (entry_po) {
                                MEA_OS_memcpy(entry_po,
                                    &MEA_HC_Comp_L2L3_info_Table   [index].data,
                                    sizeof(*entry_po));
                            }
                            
                               
                            break;
                        }
                
        }

        key->L2Type = ((index>>5) & 0x1f);
        key->L3Type = ((index)    & 0x1f);
       

        return MEA_OK;

}

MEA_Status MEA_API_HC_Comp_GetNext_Protocol(MEA_Unit_t                         unit_i,
    MEA_HC_key_proto_t                *key,/* Can't be NULL */
    MEA_HC_L2L3_CompInfo_dbt          *entry_po, /* Can't be NULL */
    MEA_Bool                          *found_o)
{
    MEA_Uint32 index;
    MEA_API_LOG


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_HC_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_HC_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        if (key == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }
        
        index=0;
        index = (key->L3Type);
        index |= (key->L2Type)<<5;
        

        for ( (index)++;
            (index) < MEA_HC_L2_PROTCOL * MEA_HC_L3_PROTCOL ; 
            (index)++ ) {
                
                        if (MEA_HC_Comp_L2L3_info_Table   [index].data.enable == MEA_TRUE) {
                            if (found_o) {
                                *found_o= MEA_TRUE;
                            }

                            if (entry_po) {
                                MEA_OS_memcpy(entry_po,
                                    &MEA_HC_Comp_L2L3_info_Table   [index].data,
                                    sizeof(*entry_po));
                            }
                           
                            break;
                        }
                
        }

        key->L2Type = ((index>>5) & 0x1f);
        key->L3Type = ((index)    & 0x1f);
    
        

        return MEA_OK;
}

/************************************************************************/
/* RMON HC                                                              */
/************************************************************************/

MEA_Status mea_drv_Init_IF_Counters_HC_RMON(MEA_Unit_t unit_i)
{
    MEA_Uint32 size;
    
     
    if (!MEA_HC_RMON_SUPPORT){
        return MEA_OK;   
    }

    if(b_bist_test){
        return MEA_OK;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize HC Rmon ...\n");


    size = MEA_HC_CLASSIFIER_MAX_OF_HCID * sizeof(mea_HC_CompState_type_dbt);
    if (size != 0) {
        MEA_HeaderCompressState_Table = (mea_HC_CompState_type_dbt   *)MEA_OS_malloc(size);
        if (MEA_HeaderCompressState_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_HeaderCompressState_Table    failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_HeaderCompressState_Table[0]), 0, size);
    }
    
    

    size = MEA_HC_CLASSIFIER_MAX_OF_HCID * sizeof(mea_HC_CompBlock_type_dbt);
    if (size != 0) {
        MEA_HeaderCompressBlock_Table = (mea_HC_CompBlock_type_dbt   *)MEA_OS_malloc(size);
        if (MEA_HeaderCompressBlock_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_HeaderCompressState_Table    failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_HeaderCompressBlock_Table[0]), 0, size);
    }




    /* Allocate PacketGen Table */
    size = MEA_HC_CLASSIFIER_MAX_OF_HCID * sizeof(MEA_Counters_RMON_HC_dbt   );
    if (size != 0) {
        MEA_Counters_HcRMON_Table    = (MEA_Counters_RMON_HC_dbt   *)MEA_OS_malloc(size);
        if (MEA_Counters_HcRMON_Table    == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_Counters_HcRMON_Table    failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_Counters_HcRMON_Table[0]),0,size);
    }

    /* Reset Counters RMON */
    if (MEA_API_Clear_Counters_HC_RMONs(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Clear_Counters_HC_RMONs failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }



    /* return to caller */
    return MEA_OK;

}

MEA_Status mea_drv_ReInit_IF_Counters_HC_RMON(MEA_Unit_t unit_i)
{
    MEA_Port_t port;
    MEA_Counters_RMON_HC_dbt  save_entry_db;
    MEA_Counters_RMON_HC_dbt* entry_db_p;

    
    if (!MEA_HC_RMON_SUPPORT){
        return MEA_OK;   
    }

    if(b_bist_test){
        return MEA_OK;
    }
    
    
    /* Scan all ports and read the RMON counters to clear from hardware ,
       without update the software shadow */
    for (port=0;port<MEA_HC_CLASSIFIER_MAX_OF_HCID;port++) {

   
        /* Check for valid port parameter */

    entry_db_p = &(MEA_Counters_HcRMON_Table[port]);
        MEA_OS_memcpy(&save_entry_db,entry_db_p,sizeof(save_entry_db));
        if (MEA_API_Collect_Counters_HC_RMON(unit_i,port,NULL)==MEA_ERROR){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect HC_RMON Counters for port=%d failed\n",
                              __FUNCTION__,port);
            MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));
            return MEA_ERROR;
        }
        MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));

    }

    /* Return to Callers */
    return MEA_OK;
    }

MEA_Status mea_drv_Conclude_IF_Counters_HC_RMON(MEA_Unit_t unit_i)
{
    /* Check if support */
    if (!MEA_HC_RMON_SUPPORT){
        return MEA_OK;   
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude HC Rmon..\n");

    /* Free the table */
    if (MEA_Counters_HcRMON_Table    != NULL) {
        MEA_OS_free(MEA_Counters_HcRMON_Table   );
        MEA_Counters_HcRMON_Table    = NULL;
    }
    return MEA_OK;
}


MEA_Status MEA_API_Collect_Counters_HC_RMON(MEA_Unit_t      unit, MEA_HDC_t   hcid,  MEA_Counters_RMON_HC_dbt* entry) 
{


    MEA_Counters_RMON_HC_dbt* entry_db;
    MEA_Uint32             rmon_rx_rd[1];



    MEA_ind_read_t         ind_read;
    MEA_Uint16             addres_offset;
    MEA_Counter_HcRMON_driver_dbt entry_drv; 
    mea_memory_mode_e save_globalMemoryMode;


    MEA_Counters_RMON_HC_dbt rmon_counter_entry;
    time_t t1,t2;


    MEA_API_LOG


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        /* Check for valid port parameter */
        /* Check if support */
        if (!MEA_HC_RMON_SUPPORT){
            return MEA_ERROR;   
        }
        if(hcid >=MEA_HC_CLASSIFIER_MAX_OF_HCID){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - RMON (hcid=%d) is out of range\n",
                __FUNCTION__,hcid);
            return MEA_ERROR;
        }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        save_globalMemoryMode = globalMemoryMode;


        /* set the entry_db pointer */
        entry_db = &(MEA_Counters_HcRMON_Table[hcid]);

        t1=MEA_Counters_HcRMON_Table[hcid].t1;


        MEA_OS_memset(&rmon_counter_entry,0,sizeof(rmon_counter_entry));
        MEA_OS_memcpy(&rmon_counter_entry,&MEA_Counters_HcRMON_Table[hcid],sizeof(rmon_counter_entry));



        MEA_OS_memset(&entry_drv,0,sizeof(MEA_Counter_HcRMON_driver_dbt));
        addres_offset=0;
        addres_offset=hcid<<4;

        /*  RMON  */
        ind_read.tableType		= ENET_IF_CMD_PARAM_TBL_HC_RMON_COUNTERS_COMP;
        ind_read.cmdReg	   	    = MEA_IF_CMD_REG;      
        ind_read.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
        ind_read.statusReg		= MEA_IF_STATUS_REG;   
        ind_read.statusMask   	= MEA_IF_STATUS_BUSY_MASK;   
        ind_read.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
        ind_read.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_read.read_data      = &(rmon_rx_rd[0]);


        /* Read offset 0 -  */
        ind_read.read_data      = &(entry_drv.offset_0.index_0_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_0;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_0.index_0_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }



        /* Read offset 1 -  */
        ind_read.read_data      = &(entry_drv.offset_1.index_1_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_1;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_1.index_1_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }


        /* Read offset 2 -  */
        ind_read.read_data      = &(entry_drv.offset_2.index_2_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_2;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_2.index_2_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }

        /* Read offset 3 -  */


        /* Read offset 4 -  */

        /* Read offset 5 -  */





        /* Read offset 6 -  */
        ind_read.read_data      = &(entry_drv.offset_6.index_6_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_6;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_6.index_6_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }


        /* Read offset 7 -  */


        /* Read offset 8 -  */
        ind_read.read_data      = &(entry_drv.offset_8.index_8_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_8;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_8.index_8_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }


        /* Read offset 9 -  */
        ind_read.read_data      = &(entry_drv.offset_9.index_9_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_9;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_9.index_9_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }


        /* Read offset 10 -  */
        ind_read.read_data      = &(entry_drv.offset_10.index_10_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_10;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_10.index_10_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }



        /* Read offset 11 -  */
        ind_read.read_data      = &(entry_drv.offset_11.index_11_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_11;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_11.index_11_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }


        /* Read offset 12 -  */
        ind_read.read_data      = &(entry_drv.offset_12.index_12_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_12;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_12.index_12_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }


        /* Read offset 13 -  */
        ind_read.read_data      = &(entry_drv.offset_13.index_13_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_13;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_13.index_13_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }

        /* Read offset 14 -  */
        ind_read.read_data      = &(entry_drv.offset_14.index_14_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_14;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_14.index_14_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }


        /* Read offset 15 -  */
        ind_read.read_data      = &(entry_drv.offset_15.index_15_regs[0] ); 
        ind_read.tableOffset	= addres_offset  + MEA_HC_COUNTER_RMON_INDEX_15;
        if (MEA_API_ReadIndirect(unit,
            &ind_read,
            MEA_NUM_OF_ELEMENTS(entry_drv.offset_15.index_15_regs),
            MEA_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
                    __FUNCTION__,
                    ind_read.tableType,
                    ind_read.tableOffset,
                    MEA_MODULE_IF);

                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_ReadIndirect tbl=%d mdl=%d failed (port=%d)\n",
                    __FUNCTION__,ind_read.tableType,MEA_MODULE_IF,hcid);
                globalMemoryMode = save_globalMemoryMode;

                return MEA_ERROR;
        }





        MEA_OS_UPDATE_COUNTER(entry_db->Tx_Bytes.val ,entry_drv.offset_0.val.TX_Bytes);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Bytes.val ,entry_drv.offset_0.val.RX_Bytes);
        MEA_OS_UPDATE_COUNTER(entry_db->Tx_NoCompressed_Pkts.val ,entry_drv.offset_0.val.TX_NoCompressed_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_NoDeCompressed_Pkts.val ,entry_drv.offset_0.val.RX_NoDeCompressed_Pkts);

        MEA_OS_UPDATE_COUNTER(entry_db->Tx_actual_bytes.val, entry_drv.offset_1.val.TX_actual_bytes);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Drop_Bytes.val ,entry_drv.offset_1.val.RX_drop_bytes);
        MEA_OS_UPDATE_COUNTER(entry_db->Tx_Compressed_Pkts.val ,entry_drv.offset_1.val.TX_Compressed_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_DeCompressed_Pkts.val ,entry_drv.offset_1.val.RX_DeCompressed_Pkts);
        /* need to Calculate  also the drop*/
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Bytes.val ,entry_drv.offset_1.val.RX_drop_bytes);






        MEA_OS_UPDATE_COUNTER(entry_db->Rx_learn_Pkts.val ,entry_drv.offset_2.val.RX_learn_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Tx_learn_Pkts.val ,entry_drv.offset_2.val.TX_learn_Pkts);



        MEA_OS_UPDATE_COUNTER(entry_db->Tx_CompressedCRC_Err.val ,entry_drv.offset_6.val.Tx_CompressedCRC_Err);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_DeCompressedCRC_Err.val ,entry_drv.offset_6.val.Rx_DeCompressedCRC_Err);



        MEA_OS_UPDATE_COUNTER(entry_db->Tx_64Octets_Pkts.val ,entry_drv.offset_8.val.Tx_64Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_64Octets_Pkts.val ,entry_drv.offset_8.val.Rx_64Octets_Pkts);

        MEA_OS_UPDATE_COUNTER(entry_db->Tx_65to127Octets_Pkts.val ,entry_drv.offset_9.val.Tx_65to127Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_65to127Octets_Pkts.val ,entry_drv.offset_9.val.Rx_65to127Octets_Pkts);

        MEA_OS_UPDATE_COUNTER(entry_db->Tx_128to255Octets_Pkts.val ,entry_drv.offset_10.val.Tx_128to255Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_128to255Octets_Pkts.val ,entry_drv.offset_10.val.Rx_128to255Octets_Pkts);

        MEA_OS_UPDATE_COUNTER(entry_db->Tx_256to511Octets_Pkts.val ,entry_drv.offset_11.val.Tx_256to511Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_256to511Octets_Pkts.val ,entry_drv.offset_11.val.Rx_256to511Octets_Pkts);

        MEA_OS_UPDATE_COUNTER(entry_db->Tx_512to1023Octets_Pkts.val ,entry_drv.offset_12.val.Tx_512to1023Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_512to1023Octets_Pkts.val ,entry_drv.offset_12.val.Rx_512to1023Octets_Pkts);

        MEA_OS_UPDATE_COUNTER(entry_db->Tx_1024to1518Octets_Pkts.val ,entry_drv.offset_13.val.Tx_1024to1518Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_1024to1518Octets_Pkts.val ,entry_drv.offset_13.val.Rx_1024to1518Octets_Pkts);

        MEA_OS_UPDATE_COUNTER(entry_db->Tx_1519to2047Octets_Pkts.val ,entry_drv.offset_14.val.Tx_1519to2047Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_1519to2047Octets_Pkts.val ,entry_drv.offset_14.val.Rx_1519to2047Octets_Pkts);




        MEA_OS_UPDATE_COUNTER(entry_db->Tx_2048toMaxOctets_Pkts.val ,entry_drv.offset_15.val.Tx_2048toMaxOctets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_2048toMaxOctets_Pkts.val ,entry_drv.offset_15.val.Rx_2048toMaxOctets_Pkts);

        /* calculate  total RX packets*/

        MEA_OS_UPDATE_COUNTER( entry_db->Rx_Pkts.val ,entry_drv.offset_8.val.Rx_64Octets_Pkts);
        MEA_OS_UPDATE_COUNTER( entry_db->Rx_Pkts.val ,entry_drv.offset_9.val.Rx_65to127Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_10.val.Rx_128to255Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_11.val.Rx_256to511Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_12.val.Rx_512to1023Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_13.val.Rx_1024to1518Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_14.val.Rx_1519to2047Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Rx_Pkts.val ,entry_drv.offset_15.val.Rx_2048toMaxOctets_Pkts);


        /* calculate  total TX packets*/ 

        MEA_OS_UPDATE_COUNTER( entry_db->Tx_Pkts.val ,entry_drv.offset_8.val.Tx_64Octets_Pkts);
        MEA_OS_UPDATE_COUNTER( entry_db->Tx_Pkts.val ,entry_drv.offset_9.val.Tx_65to127Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_10.val.Tx_128to255Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_11.val.Tx_256to511Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_12.val.Tx_512to1023Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_13.val.Tx_1024to1518Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_14.val.Tx_1519to2047Octets_Pkts);
        MEA_OS_UPDATE_COUNTER(entry_db->Tx_Pkts.val ,entry_drv.offset_15.val.Tx_2048toMaxOctets_Pkts);



        t2=time(&MEA_Counters_HcRMON_Table[hcid].t1);

        if(t2>t1 && t1 !=0){
            if (MEA_Counters_HcRMON_Table[hcid].Rx_Bytes.val != 0) {
                MEA_Uint64 tmp;
#if defined(__KERNEL__)
                MEA_Uint32 tdiff;
                MEA_uint64 rate;
                tmp.val = MEA_Counters_HcRMON_Table[hcid].Rx_Bytes.val -
                    rmon_counter_entry.Rx_Bytes.val;

                tdiff = (MEA_Uint32) t2-t1;
                do_div (tmp.val, tdiff);
                MEA_Counters_HcRMON_Table[hcid].Rx_rate = tmp.val;
#else
                tmp.val = MEA_Counters_HcRMON_Table[hcid].Rx_Bytes.val -
                    rmon_counter_entry.Rx_Bytes.val;
                MEA_Counters_HcRMON_Table[hcid].Rx_rate.val = (tmp.val /(t2-t1));
#endif
            }
            if (MEA_Counters_HcRMON_Table[hcid].Rx_Pkts.val != 0) {
                MEA_Uint64 tmp;
#if defined(__KERNEL__)
                MEA_Uint32 tdiff;
                MEA_uint64 rate;
                tmp.val = MEA_Counters_HcRMON_Table[hcid].Rx_Pkts.val -
                    rmon_counter_entry.Rx_Pkts.val;

                tdiff = (MEA_Uint32) t2-t1;
                do_div (tmp.val, tdiff);
                MEA_Counters_HcRMON_Table[port].Rx_Pkts = tmp.val;
#else
                tmp.val = MEA_Counters_HcRMON_Table[hcid].Rx_Pkts.val -
                    rmon_counter_entry.Rx_Pkts.val;
                MEA_Counters_HcRMON_Table[hcid].Rx_ratePacket.val = (tmp.val /(t2-t1));
#endif
            }



            if (MEA_Counters_HcRMON_Table[hcid].Tx_Bytes.val != 0) {
                MEA_Uint64 tmp;
#if defined(__KERNEL__)
                MEA_Uint32 tdiff;
                MEA_Uint64 rate;
                tmp.val = MEA_Counters_HcRMON_Table[hcid].Tx_Bytes.val -
                    rmon_counter_entry.Tx_Bytes.val;

                tdiff = (MEA_Uint32) t2-t1;
                do_div (tmp.val, tdiff);
                MEA_Counters_HcRMON_Table[hcid].Tx_rate = tmp.val; 
#else
                tmp.val = MEA_Counters_HcRMON_Table[hcid].Tx_Bytes.val -
                    rmon_counter_entry.Tx_Bytes.val;
                MEA_Counters_HcRMON_Table[hcid].Tx_rate.val= (tmp.val / (t2-t1));
#endif
            }
            if (MEA_Counters_HcRMON_Table[hcid].Tx_Pkts.val != 0) {
                MEA_Uint64 tmp;
#if defined(__KERNEL__)
                MEA_Uint32 tdiff;
                MEA_Uint64 rate;
                tmp.val = MEA_Counters_HcRMON_Table[hcid].Tx_Pkts.val -
                    rmon_counter_entry.Tx_Pkts.val;

                tdiff = (MEA_Uint32) t2-t1;
                do_div (tmp.val, tdiff);
                MEA_Counters_HcRMON_Table[hcid].Tx_rate = tmp.val; 
#else
                tmp.val = MEA_Counters_HcRMON_Table[hcid].Tx_Pkts.val -
                    rmon_counter_entry.Tx_Pkts.val;
                MEA_Counters_HcRMON_Table[hcid].Tx_ratePacket.val= (tmp.val / (t2-t1));
#endif
            }
            /**/
             {
                MEA_Uint32 tmp;
                MEA_Uint32 actual_bytes;
                    MEA_Uint32 TX_Bytes;

                MEA_Uint32  Tx_Compress_ratio = MEA_Counters_HcRMON_Table[hcid].Tx_Compress_ratio;
#if defined(__KERNEL__)
               
#else
               
                if ((entry_drv.offset_0.val.TX_Bytes != 0) && (entry_drv.offset_1.val.TX_actual_bytes != 0) ) {
                    tmp = 0;
                    if ((entry_drv.offset_1.val.TX_actual_bytes > entry_drv.offset_0.val.TX_Bytes) &&
                        //((entry_drv.offset_0.val.TX_Bytes * 100 ) > entry_drv.offset_1.val.TX_actual_bytes) && 

                        (entry_drv.offset_1.val.TX_Compressed_Pkts != 0) ){
                         tmp = 0;
                         

                        actual_bytes = entry_drv.offset_1.val.TX_actual_bytes;
                        TX_Bytes = entry_drv.offset_0.val.TX_Bytes;
                        

                        tmp = 100 * (actual_bytes - TX_Bytes) / actual_bytes;

                    }
                    else{
                        
                    }
                    
                    if (Tx_Compress_ratio != 0) {/*the first */
                        tmp = (Tx_Compress_ratio + tmp) / 2;
                    }
                    MEA_Counters_HcRMON_Table[hcid].Tx_Compress_ratio = tmp;

                } else {
                    tmp = 0;
                    if (Tx_Compress_ratio != 0){
                        tmp = (Tx_Compress_ratio + tmp) / 2;
                    }
                    MEA_Counters_HcRMON_Table[hcid].Tx_Compress_ratio = tmp;
                }
 

#endif
            }






        } /*  */




        /* Return to Callers */
        return MEA_OK;   

}

MEA_Status MEA_API_Collect_Counters_HC_RMONs (MEA_Unit_t unit) {


    MEA_Port_t port;

    MEA_API_LOG

        if(MEA_reinit_start)
            return MEA_OK;

    if (!MEA_HC_RMON_SUPPORT){
        return MEA_ERROR;   
    }


    for (port=0;port<MEA_HC_CLASSIFIER_MAX_OF_HCID;port++) { 

        

        /* Collect the Port Counters */
        if (MEA_API_Collect_Counters_HC_RMON(MEA_UNIT_0,port,NULL)==MEA_ERROR){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Collect RMON Counters for Hcid=%d failed\n",
                __FUNCTION__,port);
            return MEA_ERROR;
        }

    }










    /* Return to Callers */
    return MEA_OK;

}




MEA_Status MEA_API_Get_Counters_HC_RMON(MEA_Unit_t  unit, MEA_HDC_t   hcid,MEA_Counters_RMON_HC_dbt* entry)
{


        MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

            if (!MEA_HC_RMON_SUPPORT){
                return MEA_ERROR;   
            }


            if (entry == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - entry==NULL (hcid=%d)\n",
                    __FUNCTION__,hcid);
                return MEA_ERROR;  
            }

            if(hcid >=MEA_HC_CLASSIFIER_MAX_OF_HCID){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - RMON (hcid=%d) is out of range\n",
                    __FUNCTION__,hcid);
                return MEA_ERROR;
            }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

           


            /* Update output parameters */
            MEA_OS_memcpy(entry,
                &(MEA_Counters_HcRMON_Table[hcid]),
                sizeof(*entry));

            /* Return to Callers */
            return MEA_OK;

}





MEA_Status MEA_API_Clear_Counters_HC_RMONs   (MEA_Unit_t                unit) {


    MEA_HDC_t hcid;

    MEA_API_LOG


        if (!MEA_HC_RMON_SUPPORT){
            return MEA_ERROR;   
        }


        for (hcid=0;hcid<MEA_HC_CLASSIFIER_MAX_OF_HCID;hcid++) { 

            /* Check for valid port parameter */
            /* Collect the Port Counters */
            if (MEA_API_Clear_Counters_HC_RMON(MEA_UNIT_0,hcid)==MEA_ERROR){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Clear RMON Counters for port=%d failed\n",
                    __FUNCTION__,hcid);
            }

        }


        
        /* Return to Callers */
        return MEA_OK;

}


MEA_Status MEA_API_Clear_Counters_HC_RMON (MEA_Unit_t    unit,
                                        MEA_HDC_t hcid) {

        MEA_API_LOG

            if (!MEA_HC_RMON_SUPPORT){
                return MEA_ERROR;   
            }

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

            /* Check for valid port parameter */
          if(hcid >=MEA_HC_CLASSIFIER_MAX_OF_HCID){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                  "%s - RMON (hcid=%d) is out of range\n",
                  __FUNCTION__,hcid);
              return MEA_ERROR;
          }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


            /* Collect Port Counters from HW to cause HW counters zeros */
            if (MEA_API_Collect_Counters_HC_RMON(MEA_UNIT_0,hcid,NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Collect RMON counters failed (hcid=%d)\n",
                    __FUNCTION__,hcid);
                return MEA_ERROR;

            }
            /* Zeors the SW port counters db */
            MEA_OS_memset(&(MEA_Counters_HcRMON_Table[hcid]),
                0,
                sizeof(MEA_Counters_HcRMON_Table[0]));



            /* Return to Callers */
            return MEA_OK;

}





