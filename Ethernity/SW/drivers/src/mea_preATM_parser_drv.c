/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/






/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#if 1
#ifdef MEA_OS_LINUX
#ifdef MEA_OS_OC
#include <linux/unistd.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#endif
#else
#include <time.h>
#endif
#endif



#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_port_drv.h"

#include "mea_preATM_parser_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/


#define MEA_PRE_ATM_PARSER_NUM_OF_VSP  256     

#define MEA_PRE_ATM_VSP_NO_AVAILABEL   0x55

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/





typedef struct{
    
    MEA_PreATMParser_Data_dbt  data;
    MEA_Uint32                 valid  :1;
    MEA_Uint32                 pad   :31;
}MEA_PreATMParser_info_dbt;

typedef struct{
    MEA_PreATMParser_info_dbt info[MEA_PRE_ATM_PARSER_MAX_VSP_TYPE];
    
    MEA_Uint32              valid :1;
    MEA_Uint32              pad0  :31;
} MEA_PreATMParser_Entry_dbt; 



typedef struct{
    
    MEA_PreATMParser_Data_dbt data;

}MEA_PreATMParser_HW_Key_dbt;


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

MEA_PreATMParser_Entry_dbt    *MEA_PreATMParser_Entry_Table = NULL;


MEA_Uint8                     *MEA_PreATMParser_VSP_Table=NULL;





MEA_Bool MEA_PreATMParser_InitDone = MEA_FALSE;

/************************************************************************/
/*  internal function                                                   */
/************************************************************************/

static void mea_drv_PreATM_parser_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_PreATMParser_HW_Key_dbt * entry= (MEA_PreATMParser_HW_Key_dbt * )arg4;

    MEA_Uint32                 val[2];
    MEA_Uint32                 i=0;
    MEA_Uint32                 count_shift;
    MEA_Uint32 L2_protocol_type=0;


    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }

    switch (entry->data.key.L2_protocol_type)
    {
       
    case MEA_ATM_PROTOCOL_NONE:
            L2_protocol_type=entry->data.key.L2_protocol_type;
        break;
    case MEA_ATM_PROTOCOL_EoLLCoA:
    case MEA_ATM_PROTOCOL_EoVCMUXoA:
    case MEA_ATM_PROTOCOL_IPoLLCoA:
    case MEA_ATM_PROTOCOL_IPoVCMUXoA:
    case MEA_ATM_PROTOCOL_PPPoLLCoA:
    case MEA_ATM_PROTOCOL_PPPoVCMUXoA:
        L2_protocol_type=entry->data.key.L2_protocol_type;
        break;
    case MEA_ATM_PROTOCOL_VoEoLLCoA:
    case MEA_ATM_PROTOCOL_PPPoEoA:
            L2_protocol_type=17;
            break;
    case MEA_ATM_PROTOCOL_VoEoVCMUXoA:
    case MEA_ATM_PROTOCOL_PPPoEVCMUXoA:
            L2_protocol_type=18;
            break;
    default:
        L2_protocol_type=entry->data.key.L2_protocol_type;
         break;

    
    }
    



    
    count_shift=0;

    MEA_OS_insert_value(count_shift,
        16,
        entry->data.key.vci ,
        &val[0]);

    count_shift+=16;

    MEA_OS_insert_value(count_shift,
        8,
        entry->data.key.vpi ,
        &val[0]);

    count_shift+=8;

    MEA_OS_insert_value(count_shift,
        8,
        entry->data.vspId ,
        &val[0]);

    count_shift+=8;

    MEA_OS_insert_value(count_shift,
        4,
        L2_protocol_type & 0xf , //only 4 bit in HW
        &val[0]);

    count_shift+=4;

    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,MEA_IF_WRITE_DATA_REG(i),val[i],MEA_MODULE_IF);  
    }
}

static MEA_Status mea_drv_PreATM_parser_UpdateHw(MEA_Unit_t       unit_i,
                                                 MEA_Port_t       port,
                                                 MEA_Port_t       vsp_type,
                                         MEA_PreATMParser_info_dbt* entry,
                                         MEA_SETTING_TYPE_te setting_type) 
{


   MEA_PreATMParser_HW_Key_dbt  HWentry;


    MEA_ind_write_t ind_write;
    MEA_Uint32 index;


    if ( entry == NULL )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - NULL entry input\n",
            __FUNCTION__);
        return MEA_ERROR;
    }   

    /* Check if we have changes */
    MEA_OS_memset(&HWentry,0,sizeof(HWentry));

    if(setting_type !=MEA_SETTING_TYPE_DELETE){
       MEA_OS_memcpy(&HWentry.data,&entry->data,sizeof(HWentry.data));
       HWentry.data.vspId=entry->data.vspId;
    }
    
    if(port <= 31){
       index=(port - 0)*4  + vsp_type;
    }else{
        index=(port - 32)*4 + vsp_type;
    }



    /* build the ind_write value */
    ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_PRE_PARSER;
    ind_write.tableOffset	= index;
    ind_write.cmdReg		= MEA_IF_CMD_REG;      
    ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg		= MEA_IF_STATUS_REG;   
    ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_PreATM_parser_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
//     ind_write.funcParam2
//     ind_write.funcParam3
    ind_write.funcParam4 = (MEA_funcParam)(&HWentry);


    /* Write to the Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
        return MEA_ERROR;
    }


    return MEA_OK;


}

 MEA_Status mea_drv_PreATM_parser_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;

    if (!MEA_PRE_ATM_PARSER_SUPPORT )
        return MEA_OK;
    
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize PreATMParser ...\n");

    if(MEA_PreATMParser_InitDone == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_PreATM_parser_Init  all ready Init\n",
            __FUNCTION__);
        return MEA_ERROR;

    }
  


    /* Allocate PacketGen Table */
    size = (MEA_MAX_PORT_NUMBER+1) * sizeof(MEA_PreATMParser_Entry_dbt);
    if (size != 0) {
        MEA_PreATMParser_Entry_Table = (MEA_PreATMParser_Entry_dbt*)MEA_OS_malloc(size);
        if (MEA_PreATMParser_Entry_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_PreATMParser_Entry_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_PreATMParser_Entry_Table[0]),0,size);
    }
    /**/
    // TBD do i need to clear the memory in the FPGA

    size = MEA_PRE_ATM_PARSER_NUM_OF_VSP * sizeof(MEA_Uint8);
    if (size != 0) {
        MEA_PreATMParser_VSP_Table = (MEA_Uint8*)MEA_OS_malloc(size);
        if (MEA_PreATMParser_VSP_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_PreATMParser_VSP_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_PreATMParser_VSP_Table[0]),0,size);
    }
    // MEA_PreATMParser_VSP_Table is software


// The vsp 120 125,126,127 no AVAILABEL
    MEA_PreATMParser_VSP_Table[120]=MEA_PRE_ATM_VSP_NO_AVAILABEL;
    MEA_PreATMParser_VSP_Table[125]=MEA_PRE_ATM_VSP_NO_AVAILABEL;
    MEA_PreATMParser_VSP_Table[126]=MEA_PRE_ATM_VSP_NO_AVAILABEL;
    MEA_PreATMParser_VSP_Table[127]=MEA_PRE_ATM_VSP_NO_AVAILABEL;


    MEA_PreATMParser_InitDone = MEA_TRUE;

    return MEA_OK;
}


 MEA_Status mea_drv_PreATM_parser_RInit(MEA_Unit_t       unit_i)
{
    MEA_Port_t                   port;
    MEA_Uint8                    vsp_type;
    MEA_PreATMParser_info_dbt   entry;
    
    
    if (!MEA_PRE_ATM_PARSER_SUPPORT )
        return MEA_OK;

    //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"RInit PreATMParser ...\n");

    if(MEA_PreATMParser_InitDone == MEA_FALSE){
        return MEA_ERROR;
    }

    for(port=0;port<=MEA_MAX_PORT_NUMBER;port++){
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE)
            continue;
        for(vsp_type=0;vsp_type<MEA_PRE_ATM_PARSER_MAX_VSP_TYPE;vsp_type++){

            if(MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid == MEA_FALSE)
               continue;
            MEA_OS_memcpy(&entry.data,&MEA_PreATMParser_Entry_Table[port].info[vsp_type].data,sizeof(entry.data));

            if(mea_drv_PreATM_parser_UpdateHw(unit_i,
                port,
                vsp_type,
                &entry,
                MEA_SETTING_TYPE_UPDATE)!=MEA_OK)
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Port %d PreATM_parser_find_free_VSP  failed \n" ,port);
                return MEA_ERROR;

            }

        }
    }





    return MEA_OK;
}
 MEA_Status mea_drv_PreATM_parser_Conclude(MEA_Unit_t       unit_i)
{
    
    if (!MEA_PRE_ATM_PARSER_SUPPORT )
        return MEA_OK;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Pre ATM Parser ...\n");
    if(MEA_PreATMParser_InitDone == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - the Pre ATM_parser not Init\n" );
        return ENET_ERROR;

    }

    if(MEA_PreATMParser_Entry_Table){
        MEA_OS_free(MEA_PreATMParser_Entry_Table);
    }
    if(MEA_PreATMParser_VSP_Table){
        MEA_OS_free(MEA_PreATMParser_VSP_Table);
    }

   MEA_PreATMParser_InitDone=MEA_FALSE;
    return MEA_OK;
}


static MEA_Status mea_drv_PreATM_parser_check_parameters(MEA_Unit_t       unit_i,
                                                    MEA_Port_t       port,
                                                    MEA_Uint8        vsp_type,
                                                    MEA_PreATMParser_info_dbt* entry)
{
    MEA_PortsMapping_t  portMap;
    MEA_Uint8           i;

	if (!MEA_UTOPIA_SUPPORT) {
		return MEA_OK;
	}

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d is not Valid forUtopia\n" ,__FUNCTION__,port);
        return ENET_ERROR;
        
    }

    if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
        MEA_PORTS_TYPE_PHYISCAL_TYPE,
        port,
        &portMap)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_Low_Get_Port_Mapping_By_key failed forUtopia\n",
                __FUNCTION__);
            return ENET_ERROR;
    }
    if(portMap.portType != MEA_PORTTYPE_AAL5_ATM_TC  ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            " - Port %d is not Valid forUtopia\n" ,port);
        return ENET_ERROR;
    }

    // is

    if(vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            " - Port %d vsp_type %d is out of range \n" ,port,vsp_type);
        return ENET_ERROR;
    }
    if(entry->data.key.L2_protocol_type < MEA_ATM_PROTOCOL_EoLLCoA || 
        entry->data.key.L2_protocol_type >  MEA_ATM_PROTOCOL_PPPoEVCMUXoA     ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "  - Port %d vsp_type %d the L2_protocol is wrong \n" ,port,vsp_type);
            return ENET_ERROR;

    }


    //check if the we have this entry other index
    for (i=0;i<MEA_PRE_ATM_PARSER_MAX_VSP_TYPE;i++)
    {
        if(MEA_PreATMParser_Entry_Table[port].info[i].valid == MEA_TRUE){
            if(MEA_OS_memcmp(&MEA_PreATMParser_Entry_Table[port].info[i].data.key,
                &entry->data.key,
                sizeof(MEA_PreATMParser_Entry_Table[0].info[0].data.key))== 0)
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Port %d vsp_type %d are already set at vsp_type %d\n" ,__FUNCTION__,port,vsp_type,i);
                return ENET_ERROR;

            }
            if( (entry->data.key.vci == MEA_PreATMParser_Entry_Table[port].info[i].data.key.vci) && 
                (entry->data.key.vpi == MEA_PreATMParser_Entry_Table[port].info[i].data.key.vpi) ) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - Port %d vsp_type %d index % are already set vpi=%d vci=%d\n" ,__FUNCTION__,
                        port,
                        vsp_type,
                        i,
                        entry->data.key.vpi,
                        entry->data.key.vci);
                    return ENET_ERROR;
            }
        }
    }

    
    
    
    return MEA_OK;
}


MEA_Bool mea_drv_PreATM_parser_get_internal_vsp_number(MEA_Unit_t       unit_i,
                                                       MEA_Port_t       port,
                                                       MEA_Uint8        vsp_type,
                                                       MEA_Uint32       *vsp_id_o)
{
   

   if(vsp_id_o == NULL){
       return MEA_FALSE;
   }
    
   if(MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid){
    *vsp_id_o=(MEA_Uint32) MEA_PreATMParser_Entry_Table[port].info[vsp_type].data.vspId;
     return MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid;
   }

    return MEA_FALSE;
}



static MEA_Bool mea_drv_PreATM_parser_find_free_VSP(MEA_Unit_t       unit_i,
                                                    MEA_Uint8        *vspId)
{
  MEA_Uint16 index;
    
    
      
        for(index=0;index<MEA_PRE_ATM_PARSER_NUM_OF_VSP ;index++){
            if(MEA_PreATMParser_VSP_Table[index]== 0){
                *vspId=(MEA_Uint8)index;
                return MEA_TRUE;
            }
        }

return MEA_FALSE;
}

/************************************************************************/
/*  API preATM parser                                                   */
/************************************************************************/

MEA_Status MEA_API_ATM_Parser_Entry_Create(MEA_Unit_t                   unit_i,
                                           MEA_Port_t                   port,
                                           MEA_Uint8                    vsp_type,
                                           MEA_PreATMParser_Data_dbt    *data)
{
    MEA_PreATMParser_info_dbt   entry;
    MEA_Uint8                   vspId;

    
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_PRE_ATM_PARSER_SUPPORT ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PRE_ATM_PARSER_SUPPORT  Not support\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    if(data ==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data is null\n",__FUNCTION__);
        return MEA_ERROR;

    }
#endif
    MEA_OS_memcpy(&entry.data,data,sizeof(entry.data));
    
    if(mea_drv_PreATM_parser_check_parameters(unit_i,
                                            port,
                                            vsp_type,
                                            &entry)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - Port %d PreATM_parser_check_parameters failed\n" ,__FUNCTION__,port);
        return MEA_ERROR;

    }


 // check is is all ready create
    if(MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d PreATMParser_Entry already created\n" ,__FUNCTION__,port);
        return MEA_ERROR;

    }

    



 //find free vsp id

    if(mea_drv_PreATM_parser_find_free_VSP(unit_i,&vspId)!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d PreATM_parser_find_free_VSP  failed \n" ,__FUNCTION__,port);
        return MEA_ERROR;

    }

    entry.data.vspId=vspId;

   //write to HW

    if(mea_drv_PreATM_parser_UpdateHw(unit_i,
                                   port,
                                   vsp_type,
                                   &entry,
                                    MEA_SETTING_TYPE_CREATE)!=MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d PreATM_parser_find_free_VSP  failed \n" ,port);
        return MEA_ERROR;

    }

   
   MEA_PreATMParser_Entry_Table[port].valid=MEA_TRUE;

    MEA_OS_memcpy(&MEA_PreATMParser_Entry_Table[port].info[vsp_type].data,
                    &entry.data,
                    sizeof(MEA_PreATMParser_Entry_Table[0].info[0].data));
   
    MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid = MEA_TRUE;
  
   MEA_PreATMParser_VSP_Table[vspId]=MEA_TRUE;

   // need to clear the counters 



    return MEA_OK;
}

MEA_Status MEA_API_ATM_Parser_Entry_Set(MEA_Unit_t                 unit_i,
                                        MEA_Port_t                 port,
                                        MEA_Uint8                  vsp_type,
                                        MEA_PreATMParser_Data_dbt *data)
{

    MEA_PreATMParser_info_dbt entry;
    

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_PRE_ATM_PARSER_SUPPORT ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PRE_ATM_PARSER_SUPPORT  Not support\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(data ==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data is null\n",__FUNCTION__);
        return MEA_ERROR;

    }


#endif

    MEA_OS_memset(&entry,0,sizeof(entry));
    MEA_OS_memcpy(&entry.data,data,sizeof(entry));
	

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE)==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d is not Valid forUtopia\n" ,__FUNCTION__,port);
        return ENET_ERROR;

    }
    
    if(vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d vsp_type %d is out of range \n" ,__FUNCTION__,port,vsp_type);
        return ENET_ERROR;
    }


    // check if we create
    if(MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d vsp_type %d PreATMParser_Entry is not created\n" ,__FUNCTION__,port,vsp_type);
        return ENET_ERROR;

    }
    
    if(MEA_OS_memcmp(&MEA_PreATMParser_Entry_Table[port].info[vsp_type].data.key,
                     &entry.data.key,
                     sizeof(MEA_PreATMParser_Entry_Table[0].info[0].data.key))== 0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," no change\n");
            return MEA_OK;

    }
    entry.data.vspId=MEA_PreATMParser_Entry_Table[port].info[vsp_type].data.vspId; 


    if(mea_drv_PreATM_parser_check_parameters(unit_i,
        port,
        vsp_type,
        &entry)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Port %d PreATM_parser_check_parameters failed\n" ,__FUNCTION__,port);
            return ENET_ERROR;

    }

    //write to HW
    
    if(mea_drv_PreATM_parser_UpdateHw(unit_i,
        port,
        vsp_type,
        &entry,
        MEA_SETTING_TYPE_UPDATE)!=MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d mea_drv_PreATM_parser_UpdateHw  failed \n" ,__FUNCTION__,port);
        return MEA_ERROR;

    }

    //update db
    MEA_OS_memcpy(&MEA_PreATMParser_Entry_Table[port].info[vsp_type].data,
        &entry.data,
        sizeof(MEA_PreATMParser_Entry_Table[0].info[0].data));



    return MEA_OK;
}

MEA_Status MEA_API_ATM_Parser_Entry_Delete(MEA_Unit_t              unit_i,
                                           MEA_Port_t              port,
                                           MEA_Uint8              vsp_type)
{

    MEA_PreATMParser_info_dbt entry;
    MEA_Uint32                 vspId;
    MEA_Uint8 i,count;
    
    


    if (!MEA_PRE_ATM_PARSER_SUPPORT ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PRE_ATM_PARSER_SUPPORT  Not support\n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memset(&entry,0,sizeof(entry));

   


    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d is not Valid forUtopia\n" ,__FUNCTION__,port);
        return ENET_ERROR;

    }
    
    if(vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d vsp_type %d is out of range \n" ,__FUNCTION__,port,vsp_type);
        return ENET_ERROR;
    }


    // check ready create
    if(MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d vsp_type %d PreATMParser_Entry is not created\n" ,__FUNCTION__,port,vsp_type);
        return ENET_ERROR;

    }

    
    if(mea_drv_PreATM_parser_UpdateHw(unit_i,
        port,
        vsp_type,
        &entry,
        MEA_SETTING_TYPE_UPDATE)!=MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d mea_drv_PreATM_parser_UpdateHw  failed \n" ,__FUNCTION__,port);
        return MEA_ERROR;

    }


     //set the delete the VSP
    vspId= MEA_PreATMParser_Entry_Table[port].info[vsp_type].data.vspId;
    MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid=MEA_FALSE;
    MEA_OS_memset(&MEA_PreATMParser_Entry_Table[port].info[vsp_type],
                   0,
                  sizeof(MEA_PreATMParser_Entry_Table[0].info[0]));
    //check if all the vsp disable then disable the valid port

    MEA_PreATMParser_VSP_Table[vspId]=MEA_FALSE;
    // over all port vsp
    count=0;
    for(i=0;i<MEA_PRE_ATM_PARSER_MAX_VSP_TYPE;i++){
        if(MEA_PreATMParser_Entry_Table[port].info[i].valid ==MEA_TRUE){
            count++;
            break;
        }
    
    }
    if (count==0)
    {
        MEA_PreATMParser_Entry_Table[port].valid=MEA_FALSE;
    }
    



    return MEA_OK;
}

MEA_Status MEA_API_ATM_Parser_Entry_Get(MEA_Unit_t                 unit_i,
                                        MEA_Port_t                 port,
                                        MEA_Uint8                  vsp_type,
                                        MEA_PreATMParser_Data_dbt *data)
{

    if (!MEA_PRE_ATM_PARSER_SUPPORT ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PRE_ATM_PARSER_SUPPORT  Not support\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(data ==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key_o is null\n",__FUNCTION__);
        return MEA_ERROR;

    }


    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d is not Valid forUtopia\n" ,__FUNCTION__,port);
        return ENET_ERROR;

    }

    if(vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d vsp_type %d is out of range \n" ,__FUNCTION__,port,vsp_type);
        return ENET_ERROR;
    }


    // check ready create
    if(MEA_PreATMParser_Entry_Table[port].valid==MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d not create yet\n" ,port);
        return ENET_ERROR;
    }

    if(MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d vsp_type %d PreATMParser_Entry is not created\n" ,__FUNCTION__,port,vsp_type);
        return ENET_ERROR;

    }

    MEA_OS_memcpy(data,&MEA_PreATMParser_Entry_Table[port].info[vsp_type].data,sizeof(*data));


    return MEA_OK;
}


MEA_Status MEA_API_ATM_Parser_Entry_IsExist(MEA_Unit_t             unit_i,
                                        MEA_Port_t                 port,
                                        MEA_Uint8                  vsp_type,
                                        MEA_Bool                   silence,
                                        MEA_Bool                  *exist)
{

    if (!MEA_PRE_ATM_PARSER_SUPPORT ){
        if(!silence){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PRE_ATM_PARSER_SUPPORT  Not support\n",__FUNCTION__);
        }
        return MEA_ERROR;
    }

    if(exist ==NULL){
        if(!silence){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - exist is null\n",__FUNCTION__);
        }
        return MEA_ERROR;

    }


    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        if(!silence){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d is not Valid forUtopia\n" ,__FUNCTION__,port);
        }
        return ENET_ERROR;

    }

    if(vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
        if(!silence){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d vsp_type %d is out of range \n" ,__FUNCTION__,port,vsp_type);
        }
        return ENET_ERROR;
    }


    // check ready create
    if(MEA_PreATMParser_Entry_Table[port].valid==MEA_FALSE){
        if(!silence){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d not create yet for ATM\n" ,__FUNCTION__,port);
        }
        return ENET_ERROR;
    }

    if(MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid == MEA_FALSE){
        if(!silence){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port %d vsp_type %d PreATMParser_Entry is not created\n" ,__FUNCTION__,port,vsp_type);
        }
        return ENET_ERROR;

    }

    *exist=MEA_PreATMParser_Entry_Table[port].info[vsp_type].valid;


    return  ENET_OK;
}

