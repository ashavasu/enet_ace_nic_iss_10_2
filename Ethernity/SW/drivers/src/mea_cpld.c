/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*************************************************************************************/
/* 	Module Name:		 mea_drv_cpld.c.c		   									 */ 
/*																					 */
/*  Module Description:	 MEA driver	CPLD routines									 */
/*																					 */
/*  Date of Creation:	 													         */
/*																					 */
/*  Author Name:			 													         */
/*************************************************************************************/ 

/*-------------------------------- Includes ------------------------------------------*/

#include "MEA_platform.h"


#include "mea_api.h"


#include "mea_drv_common.h"


#include "mea_bm_drv.h"

#include "mea_port_drv.h"
#include "mea_cpld.h"
#include "mea_init.h"

/*************************************************************************************/
// Function Name:  mea_drv_CPLD_ResetModule 												
// 																					
// Description:	   Reset specific MEA module										
// Arguments:	   mea_module - The module to be reset
//														
// Return:		   MEA_Status
/************************************************************************************/
	  

MEA_Status mea_drv_CPLD_ResetModule(MEA_module_te mea_module)
{

    return MEA_OK;
}




MEA_Status mea_drv_CPLD_Init(void) {

#if 0 /* T.B.D alex*/
    MEA_Uint32 device_memory_addr;
    MEA_Int32  device_memory_id;
    int i;
    static const MEA_Uint32 addr[3] = { MEA_CPLD_BASE_ADDR | 0x000A0000,
                                        MEA_CPLD_BASE_ADDR | 0x000B0000,
                                        MEA_CPLD_BASE_ADDR | 0x000C0000};
 
    /* Reset the Phy and Quads */
    for (i=0;i<sizeof(addr)/sizeof(addr[0]);i++) {
	   if (MEA_OS_DeviceMemoryMap(addr[i],
                                  MEA_OS_GetPageSize(),
                                  &device_memory_addr,
                                  &device_memory_id) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                          "%s - Mapped address(0x%08x) failed\n",__FUNCTION__,addr[i]);
           return MEA_ERROR;
        }
	
        if (MEA_OS_DeviceMemoryWriteUint32(device_memory_id,
                                           device_memory_addr,
                                            0,
                                            0xFF) != MEA_OK) {
	       MEA_OS_DeviceMemoryUnMap (device_memory_addr,MEA_OS_GetPageSize(),device_memory_id);
           MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                             "%s - Write 0xFF to 0x%08x failed\n",__FUNCTION__,addr[i]);
           return MEA_ERROR;
        }

	    if (MEA_OS_DeviceMemoryUnMap (device_memory_addr,
                                      MEA_OS_GetPageSize(),
                                      device_memory_id) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                             "%s - Unmapped address(0x%08x) (id=%d) failed\n",
                             __FUNCTION__,
                             addr[i],
                             device_memory_id);
        }

    }
#endif

    return MEA_OK;
}



