/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*****************************************************************************/
/* 	Module Name:		 mea_sflow_drv.c    							     */ 
/*																		     */
/*  Module Description:	 MEA driver										     */
/*																			 */
/*  Date of Creation:	 													 */
/*																			 */
/*  Author Name: Alex Weis  29/01/2017  									 */
/*****************************************************************************/ 

/*-------------------------------- Includes ------------------------------------------*/

#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_drv_common.h"
#include "enet_queue_drv.h"
#include "mea_action_drv.h"
#include "mea_sflow_drv.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_IF_SFLOW_PROF_MAKE0_PKT_WIDTH 14
#define MEA_IF_SFLOW_PROF_MAKE0_TIMEOUT_TYPE_WIDTH 2
#define MEA_IF_SFLOW_PROF_MAKE0_PRI_QUEU_WIDTH 3

#define MEA_IF_SFLOW_ST_MAKE0_PKT_WIDTH 14
#define MEA_IF_SFLOW_ST_MAKE0_TIMEOUT_TYPE_WIDTH 2
#define MEA_IF_SFLOW_ST_MAKE0_PRI_QUEU_WIDTH 3



#define MEA_SFLOW_EV_dest_cluster   (MEA_OS_calc_from_size_to_width( mea_drv_Get_DeviceInfo_NumOfClusters((MEA_UNIT_0),(0))))  
#define MEA_SFLOW_EV_sflow_type     2
#define MEA_SFLOW_EV_PM             12
#define MEA_SFLOW_EV_SRC_PORT       10
#define MEA_SFLOW_EV_sflow_en       1
#define MEA_SFLOW_EV_P_TO_MP        1


/**
 DSA for Sflow 
 */
#define MEA_SFLOW_DSA_EV_dest_cluster   (MEA_SFLOW_EV_dest_cluster)  
#define MEA_SFLOW_DSA_EV_sflow_type     2


#define MEA_SFLOW_DSA_EV_PM_Start_from      12   
#define MEA_SFLOW_DSA_EV_PM             12
#define MEA_SFLOW_DSA_EV_SRC_PORT       10
#define MEA_SFLOW_DSA_EV_P_TO_MP        1
#define MEA_SFLOW_DSA_PAD               12
#define MEA_SFLOW_DSA_EV_sflow_en       1
#define MEA_SFLOW_DSA_ETH               16 




/*----------------------------------------------------------------------------*/
/*             Typedefs                                                        */
/*----------------------------------------------------------------------------*/

typedef struct {
    MEA_Bool valid;
    MEA_Uint32 num_of_owners;

    MEA_Sflow_Prof_dbt data;
    
}mea_sflow_profile_dbt;

typedef struct {

    MEA_Sflow_count_dbt data;

}mea_sflow_counter_dbt;

typedef struct {
    MEA_Uint16                st_packetCount;
    MEA_Sflow_timeOut_type_t  st_timeOut_type;
    MEA_Uint16                st_profile;

}mea_Sflow_Statistic_HW_dbt;



typedef struct {

    MEA_sflow_queue_t  *cluster_info;   /*external */
    /* HW */
    MEA_sflow_queue_t  *cluster_Internal; 

}mea_sflow_queue_dbt;

typedef struct {
    MEA_Sflow_Global_dbt   data;
    mea_sflow_queue_dbt    mea_sflow_queue;

    MEA_Uint32 mea_sflow_TICK_hw;
}MEA_Sflow_Global_Entry_dbt;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

mea_sflow_profile_dbt  *mea_sflow_prof_info_TBL = NULL;

mea_sflow_counter_dbt  *mea_sflow_Statistic_TBL = NULL;



MEA_Sflow_Global_Entry_dbt *mea_sflow_Global_Entry=NULL;






/*-------------------------------- External Variables --------------------------------*/



/*-------------------------------- static function   ----------------------------------*/

static MEA_Status mea_drv_SflowQueue_UpdateHw(MEA_Unit_t  unit_i);
static MEA_Status mea_drv_Sflow_TICK_UpdateHw(MEA_Unit_t  unit_i, MEA_Uint32 Tick);

static void mea_drv_Sflow_TICK_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4);

static MEA_Status mea_drv_Sflow_DsaTag_UpdateHw(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry);
static void mea_drv_Sflow_DsaTag_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4);

static MEA_Status mea_drv_Sflow_MtuCpu_UpdateHw(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry);
static void mea_drv_Sflow_MtuCpu_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4);


static MEA_Status mea_drv_sflow_Set_Tick(MEA_Unit_t unit_i, MEA_Uint32  time_Th);
static MEA_Status mea_drv_Sflow_Set_Global(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry, MEA_Sflow_Global_dbt *old_entry);
/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


static void mea_drv_Sflow_Satistic_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    mea_Sflow_Statistic_HW_dbt  *entry = (mea_Sflow_Statistic_HW_dbt  *)arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
    MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    count_shift = 0;


    MEA_OS_insert_value(count_shift,  
        MEA_IF_SFLOW_ST_MAKE0_PKT_WIDTH,
        (MEA_Uint32)entry->st_packetCount ,
        &val);
    count_shift += MEA_IF_SFLOW_ST_MAKE0_PKT_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_SFLOW_ST_MAKE0_TIMEOUT_TYPE_WIDTH,
        (MEA_Uint32)entry->st_timeOut_type,
        &val);
    count_shift += MEA_IF_SFLOW_ST_MAKE0_TIMEOUT_TYPE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_SFLOW_ST_MAKE0_PRI_QUEU_WIDTH,
        (MEA_Uint32)entry->st_profile,
        &val);
    count_shift += MEA_IF_SFLOW_ST_MAKE0_PRI_QUEU_WIDTH;






    /* Update Hw Entry */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }



}



static MEA_Status mea_drv_sflow_satistic_UpdateHw(MEA_Unit_t                        unit_i,
    MEA_Uint16                        id_i,
    mea_Sflow_Statistic_HW_dbt    *new_entry_pi)
{

    MEA_ind_write_t   ind_write;



   



    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_SFLOW_COUNT;
    ind_write.tableOffset = id_i;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_Sflow_Satistic_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    //ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
    ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

    if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }


    /* Return to caller */
    return MEA_OK;
}




static void mea_drv_SflowProf_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Sflow_Prof_dbt  *entry = (MEA_Sflow_Prof_dbt  *)arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
    MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    count_shift=0;


    MEA_OS_insert_value(count_shift,
        MEA_IF_SFLOW_PROF_MAKE0_PKT_WIDTH,
          ((MEA_Uint32)entry->packetCount-1),
        &val);
    count_shift += MEA_IF_SFLOW_PROF_MAKE0_PKT_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_SFLOW_PROF_MAKE0_TIMEOUT_TYPE_WIDTH,
        (MEA_Uint32)entry->timeOut_type,
        &val);
    count_shift += MEA_IF_SFLOW_PROF_MAKE0_TIMEOUT_TYPE_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_IF_SFLOW_PROF_MAKE0_PRI_QUEU_WIDTH,
        (MEA_Uint32)entry->pri_queue,
        &val);
    count_shift += MEA_IF_SFLOW_PROF_MAKE0_PRI_QUEU_WIDTH;



    


    /* Update Hw Entry */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }



}



static MEA_Status mea_drv_sflow_prof_UpdateHw(MEA_Unit_t                        unit_i,
    MEA_Uint16                        id_i,
    MEA_Sflow_Prof_dbt    *new_entry_pi,
    MEA_Sflow_Prof_dbt    *old_entry_pi)
{

    MEA_ind_write_t   ind_write;



    /* check if we have any changes */
    if ((old_entry_pi) &&
        (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi)) == 0)) {
        return MEA_OK;
    }



    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_write.tableOffset = MEA_GLOBAl_TBL_REG_SFLOW_PROF + id_i;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_SflowProf_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    //ind_write.funcParam3 = (MEA_funcParam)((long)numofRegs);
    ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

    if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }



    /* Return to caller */
    return MEA_OK;
}


static MEA_Bool mea_drv_sflow_Prof_find_free(MEA_Unit_t       unit_i,
    MEA_Uint16       *id_io)
{
    MEA_Uint16 i;

    for (i = 0; i < MEA_SFLOW_MAX_PROF; i++) {
        if (mea_sflow_prof_info_TBL[i].valid == MEA_FALSE) {
            *id_io = i;
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;

}







static MEA_Bool mea_drv_sflow_prof_IsRange(MEA_Unit_t     unit_i,
    MEA_Uint16 id_i)
{

if ((id_i) >= MEA_SFLOW_MAX_PROF) {
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "out range id_i %d >= \n", id_i, MEA_SFLOW_MAX_PROF);
    return MEA_FALSE;
}
return MEA_TRUE;
}

static MEA_Status mea_drv_sflow_prof_IsExist(MEA_Unit_t     unit_i,
    MEA_Uint16 id_i,
    MEA_Bool silent,
    MEA_Bool *exist)
{

    if (exist == NULL) {
        if(!silent)
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  exist = NULL \n", __FUNCTION__);

        return MEA_ERROR;
    }
    *exist = MEA_FALSE;

    if (mea_drv_sflow_prof_IsRange(unit_i, id_i) != MEA_TRUE) {
        if (!silent)
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_sflow_prof_IsRange failed  for id=%d  \n", __FUNCTION__, id_i);

        return MEA_ERROR;
    }

    if (mea_sflow_prof_info_TBL[id_i].valid) {
        *exist = MEA_TRUE;
        return MEA_OK;
    }

    return MEA_OK;
}

static MEA_Status mea_drv_sflow_prof_check_parameters(MEA_Unit_t       unit_i,
    MEA_Sflow_Prof_dbt      *entry_pi)
{
    if (entry_pi->packetCount != 0) {
        /* check the tick */
//         if (entry_pi->timeOut_type == MEA_SFLOW_TIMEOUT_TYPE_NO_TIME) {
//             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -timeOut_type  > %d   \n", __FUNCTION__, MEA_SFLOW_TIMEOUT_TYPE_NO_TIME);
//             return MEA_ERROR;
//         }
    }


    return MEA_OK;
}

static MEA_Status mea_drv_Sflow_prof_add_owner(MEA_Unit_t       unit_i,
    MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if (mea_drv_sflow_prof_IsExist(unit_i, id_i,MEA_TRUE, &exist) != MEA_OK) {
        return MEA_ERROR;
    }

    if (id_i == 0)
        return MEA_OK;  /*no need to add or delete for Id Zero*/


    mea_sflow_prof_info_TBL[id_i].num_of_owners++;

    return MEA_OK;
}

static MEA_Status mea_drv_Sflow_prof_delete_owner(MEA_Unit_t       unit_i,
                                                 MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if (mea_drv_sflow_prof_IsExist(unit_i, id_i,MEA_TRUE, &exist) != MEA_OK) {
        return MEA_ERROR;
    }

    if (id_i == 0)
        return MEA_OK;  /*no need to add or delete for Id Zero*/


    if (mea_sflow_prof_info_TBL[id_i].num_of_owners == 1) {
       
        // call to delete profile 
        if(MEA_API_Sflow_prof_Delete(unit_i, id_i) != MEA_OK){
            return MEA_ERROR;
        }
        return MEA_OK;
    }


    mea_sflow_prof_info_TBL[id_i].num_of_owners--;

    return MEA_OK;
}




static MEA_Status mea_drv_sflow_Set_Tick(MEA_Unit_t unit_i, MEA_Uint32  time_Th)
{
    MEA_uint64 sysclck;
    MEA_Uint32 tick;

    if (time_Th == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "time_Th != 0\n");
        return MEA_ERROR;
    }
    sysclck = MEA_GLOBAL_SYS_Clock;

    tick = (MEA_Uint32)(time_Th * (sysclck ) / 1000);

    if (tick >= 0xffffffff) {
        return MEA_ERROR;
    }


    if (tick <= (MEA_SFLOW_MAX_COUNTERS * 4)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "tick value need to me bigger then %d \n", (MEA_SFLOW_MAX_COUNTERS * 4));
        return MEA_ERROR;
    }


    if (mea_drv_Sflow_TICK_UpdateHw(unit_i, tick) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "mea_drv_Sflow_TICK_UpdateHw failed \n");
        return MEA_ERROR;
    }

    mea_sflow_Global_Entry->mea_sflow_TICK_hw = tick;




    return MEA_OK;

}



MEA_Status mea_drv_sflow_Init(MEA_Unit_t       unit_i)
{
    
    MEA_Uint32 size;
    MEA_Sflow_Global_dbt entry;
    MEA_Uint32 i;

    /* Check if support */
    if (!MEA_SFLOW_SUPPORT) {
        return MEA_OK;
    }
    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize SFLOW .\n");

    
    /* Allocate PacketGen Table */
    size = MEA_SFLOW_MAX_PROF * sizeof(mea_sflow_profile_dbt);
    if (size != 0) {
        mea_sflow_prof_info_TBL = (mea_sflow_profile_dbt*)MEA_OS_malloc(size);
        if (mea_sflow_prof_info_TBL == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_sflow_prof_info_TBL failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(mea_sflow_prof_info_TBL[0]), 0, size);
    }
 


    size = MEA_SFLOW_MAX_COUNTERS * sizeof(mea_sflow_counter_dbt);
    

    if (size != 0) {
        mea_sflow_Statistic_TBL = (mea_sflow_counter_dbt*)MEA_OS_malloc(size);
        if (mea_sflow_Statistic_TBL == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_sflow_Statistic_TBL failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(mea_sflow_Statistic_TBL[0]), 0, size);
    }

    
    size = 1 * sizeof(MEA_Sflow_Global_Entry_dbt);
    
    if (size != 0) {
        mea_sflow_Global_Entry = (MEA_Sflow_Global_Entry_dbt*)MEA_OS_malloc(size);
        if (mea_sflow_Statistic_TBL == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_sflow_Statistic_TBL failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(mea_sflow_Global_Entry->data), 0, sizeof(MEA_Sflow_Global_dbt));
        mea_sflow_Global_Entry->mea_sflow_TICK_hw =0;
    }
    /************************************************************************/
    /*       MEA_MAX_SFLOW_QUEUE_GROUP                                      */
    /************************************************************************/
    
    size = MEA_MAX_SFLOW_QUEUE_GROUP * sizeof(MEA_sflow_queue_t);
   
    if (size != 0) {
       
           
            mea_sflow_Global_Entry->mea_sflow_queue.cluster_info = (MEA_sflow_queue_t*)MEA_OS_malloc(size);
            
        

        if (mea_sflow_Global_Entry->mea_sflow_queue.cluster_info == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_sflow_Global_Entry->mea_sflow_queue.cluster_info failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
    }
   
    size = MEA_MAX_SFLOW_QUEUE_GROUP * sizeof(MEA_sflow_queue_t);
    
    if (size != 0) {
       
            
            mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal = (MEA_sflow_queue_t*)MEA_OS_malloc(size);
            
       
        if (mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
    }
    
    for (i=0; i< MEA_MAX_SFLOW_QUEUE_GROUP;i++)
    {
        MEA_OS_memset(&mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[i],0,sizeof(MEA_sflow_queue_t));
        mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[i].valid = 0;

        MEA_OS_memset(&mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal[i], 0, sizeof(MEA_sflow_queue_t));
         mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal[i].valid = 0;
    }

    /************************************************************************/
    /*                                                                      */
    /************************************************************************/


    
    

    MEA_OS_memset(&entry, 0, sizeof(entry));

    entry.time_Th = 1000; /* 1000msec */
    entry.dsa_tag_EthType = MEA_GLOBAL_SFLOW_DSA_ETH;
    entry.mtuCpu          = MEA_GLOBAL_SFLOW_MTUCPU_ETH;
    if (MEA_API_Sflow_Set_Global(unit_i, &entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Sflow_Set_Tick failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    return MEA_OK;
}

MEA_Status mea_drv_sflow_RInit(MEA_Unit_t       unit_i)
{
    MEA_Uint16 id;
    mea_Sflow_Statistic_HW_dbt entryHW;
    MEA_Uint32 profId;

    /* Check if support */
    if (!MEA_SFLOW_SUPPORT) {
       
        return MEA_OK;
    }

    for (id = 0; id < MEA_SFLOW_MAX_PROF; id++) {
        if (mea_sflow_prof_info_TBL[id].valid) {
            if (mea_drv_sflow_prof_UpdateHw(unit_i,
                id,
                &(mea_sflow_prof_info_TBL[id].data),
                NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  mea_drv_LAG_UpdateHw failed (id=%d)\n", __FUNCTION__, id);
                return MEA_ERROR;
            }
        }
    }



    for (id = 0; id < MEA_SFLOW_MAX_COUNTERS; id++) {
        if (mea_sflow_Statistic_TBL[id].data.valid == MEA_FALSE)
            continue;

        MEA_OS_memset(&entryHW, 0, sizeof(entryHW));
        /* Get the profile info */
        profId = mea_sflow_Statistic_TBL[id].data.profId;
        entryHW.st_packetCount = mea_sflow_prof_info_TBL[profId].data.packetCount;
        entryHW.st_timeOut_type = mea_sflow_prof_info_TBL[profId].data.timeOut_type;
        entryHW.st_profile = profId;


        /*Configure The HW  */
        if (mea_drv_sflow_satistic_UpdateHw(unit_i, id, &entryHW) != MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  mea_drv_sflow_satistic_UpdateHw %d failed \n",
                __FUNCTION__, id);
            return MEA_ERROR;
        }
    }

    if (mea_drv_SflowQueue_UpdateHw(unit_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "mea_drv_SflowQueue_UpdateHw failed\n");
        return MEA_ERROR;
    }


    mea_drv_Sflow_Set_Global(unit_i, &mea_sflow_Global_Entry->data, NULL);


    return MEA_OK;
}

MEA_Status mea_drv_sflow_Conclude(MEA_Unit_t       unit_i)
{
    /* Check if support */
    if (!MEA_SFLOW_SUPPORT) {
     
        return MEA_OK;
    }

    /* Free the table */
    if (mea_sflow_prof_info_TBL != NULL) {
        MEA_OS_free(mea_sflow_prof_info_TBL);
        mea_sflow_prof_info_TBL = NULL;
    }

    if (mea_sflow_Statistic_TBL != NULL) {
        MEA_OS_free(mea_sflow_Statistic_TBL);
        mea_sflow_Statistic_TBL = NULL;
    }

    if (mea_sflow_Global_Entry->mea_sflow_queue.cluster_info != NULL) {
        MEA_OS_free(mea_sflow_Global_Entry->mea_sflow_queue.cluster_info);
        mea_sflow_Global_Entry->mea_sflow_queue.cluster_info = NULL;
    }

    if (mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal != NULL) {
        MEA_OS_free(mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal);
        mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal = NULL;
    }

    return MEA_OK;
}

/************************************************************************/
/*  MEA_API  profile                                                */
/************************************************************************/
MEA_Status MEA_API_sflow_prof_IsExist(MEA_Unit_t     unit_i,
    MEA_Uint16 id_i,
    MEA_Bool silent,
    MEA_Bool *exist)
{

    return mea_drv_sflow_prof_IsExist(unit_i, id_i, silent, exist);
}

MEA_Status MEA_API_Sflow_Prof_Create(MEA_Unit_t                   unit_i,
                                     MEA_Sflow_Prof_dbt           *entry_pi,
                                     MEA_Uint16                   *id_io)
{
    MEA_Bool exist;

    MEA_API_LOG

        /* Check if support */
        if (!MEA_SFLOW_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_SFLOW_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    /* check parameter */
    if (mea_drv_sflow_prof_check_parameters(unit_i, entry_pi) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_sflow_prof_check_parameters failed \n", __FUNCTION__);
        return MEA_ERROR;
    }

    /* Look for stream id */
    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID) {
        /*check if the id exist*/
        if (mea_drv_sflow_prof_IsExist(unit_i, *id_io,MEA_TRUE, &exist) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_sflow_prof_IsExist failed (*id_io=%d\n",
                __FUNCTION__, *id_io);
            return MEA_ERROR;
        }
        if (exist) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - *Id_io %d is already exist\n", __FUNCTION__, *id_io);
            return MEA_ERROR;
        }

    }
    else {
        /*find new place*/
        if (mea_drv_sflow_Prof_find_free(unit_i, id_io) != MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_LAG_find_free failed\n", __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if (mea_drv_sflow_prof_UpdateHw(unit_i,
        *id_io,
        entry_pi,
        NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_sflow_prof_UpdateHw failed for id %d\n", __FUNCTION__, *id_io);
        return MEA_ERROR;
    }

    mea_sflow_prof_info_TBL[*id_io].valid = MEA_TRUE;
    mea_sflow_prof_info_TBL[*id_io].num_of_owners = 1;
    MEA_OS_memcpy(&(mea_sflow_prof_info_TBL[*id_io].data), entry_pi, sizeof(mea_sflow_prof_info_TBL[*id_io].data));



    return MEA_OK;

}


MEA_Status MEA_API_Sflow_prof_Set(MEA_Unit_t                     unit_i,
                                     MEA_Uint16                      id_i,
                                     MEA_Sflow_Prof_dbt       *entry_pi)
{
    MEA_Bool exist;

    MEA_API_LOG

        /* Check if support */
        if (!MEA_SFLOW_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - LAG not support \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

    /*check if the id exist*/
    if (mea_drv_sflow_prof_IsExist(unit_i, id_i,MEA_TRUE, &exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_LAG_IsExist failed (id_i=%d\n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - LAG %d not exist\n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }


    /* check parameter*/
    if (mea_drv_sflow_prof_check_parameters(unit_i, entry_pi) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_sflow_prof_check_parameters filed \n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_sflow_prof_UpdateHw(unit_i,
        id_i,
        entry_pi,
        &mea_sflow_prof_info_TBL[id_i].data) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_sflow_prof_UpdateHw failed for id %d\n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&(mea_sflow_prof_info_TBL[id_i].data),
        entry_pi,
        sizeof(mea_sflow_prof_info_TBL[id_i].data));



    return MEA_OK;
}



MEA_Status MEA_API_Sflow_prof_Delete(MEA_Unit_t                     unit_i,
                                    MEA_Uint16                      id_i)
{
    MEA_Bool exist;
    MEA_Sflow_Prof_dbt entry;
    MEA_API_LOG

        /* Check if support */
        if (!MEA_SFLOW_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - LAG not support \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

    /*check if the id exist*/
    if (mea_drv_sflow_prof_IsExist(unit_i, id_i,MEA_TRUE, &exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_sflow_prof_IsExist failed (id_i=%d\n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - LAG %d not exist\n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }

    if (id_i == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "Sflow id %d not allow to delete \n", id_i);
        return MEA_ERROR;
    }
    if (mea_sflow_prof_info_TBL[id_i].valid == MEA_TRUE) {
        if (mea_sflow_prof_info_TBL[id_i].num_of_owners > 1) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - need to delete all the reference to this profile\n", __FUNCTION__, id_i);
            return MEA_ERROR;
        }
    }


    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (mea_drv_sflow_prof_UpdateHw(unit_i,
        id_i,
        &entry,
        NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_sflow_prof_UpdateHw failed for id %d\n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&(mea_sflow_prof_info_TBL[id_i].data),
        &entry,
        sizeof(mea_sflow_prof_info_TBL[id_i].data));
    mea_sflow_prof_info_TBL[id_i].valid = MEA_FALSE;
    mea_sflow_prof_info_TBL[id_i].num_of_owners = 0;




    return MEA_OK;
}



MEA_Status MEA_API_Sflow_prof_Get(MEA_Unit_t                  unit_i,
    MEA_Uint16                   id_i,
    MEA_Sflow_Prof_dbt    *entry_po)
{
    MEA_Bool    exist;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_SFLOW_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
            return MEA_ERROR;
        }

    /*check if the id exist*/
    if (mea_drv_sflow_prof_IsExist(unit_i, id_i,MEA_TRUE, &exist) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_sflow_prof_IsExist failed (id_i=%d\n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }
    if (!exist) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id %d not exist\n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }
    if (entry_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_po is null\n", __FUNCTION__, id_i);
        return MEA_ERROR;

    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

    /* Copy to caller structure */
    MEA_OS_memset(entry_po, 0, sizeof(*entry_po));
    MEA_OS_memcpy(entry_po,
        &(mea_sflow_prof_info_TBL[id_i].data),
        sizeof(*entry_po));

    /* Return to caller */
    return MEA_OK;


}

MEA_Status MEA_API_Sflow_Prof_GetFirst(MEA_Unit_t               unit_i,
    MEA_Uint16                *id_o,
    MEA_Sflow_Prof_dbt               *entry_po, /* Can't be NULL */
    MEA_Bool                 *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_SFLOW_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
            return MEA_ERROR;
        }

    if (id_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - id_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (found_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ((*id_o) = 0;
        (*id_o) < MEA_SFLOW_MAX_PROF;
        (*id_o)++) {
        if (mea_sflow_prof_info_TBL[(*id_o)].valid == MEA_TRUE) {
            if (found_o) {
                *found_o = MEA_TRUE;
            }

            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                    &mea_sflow_prof_info_TBL[(*id_o)].data,
                    sizeof(*entry_po));
            }
            break;
        }
    }


    return MEA_OK;

}

MEA_Status MEA_API_Sflow_Prof_GetNext(MEA_Unit_t                  unit_i,
    MEA_Uint16                   *id_io,
    MEA_Sflow_Prof_dbt       *entry_po, /* Can't be NULL */
    MEA_Bool                    *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        /* Check if support */
        if (!MEA_SFLOW_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
            return MEA_ERROR;
        }
    if (id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id_io == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (found_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ((*id_io)++;
        (*id_io) < MEA_SFLOW_MAX_PROF;
        (*id_io)++) {
        if (mea_sflow_prof_info_TBL[(*id_io)].valid == MEA_TRUE) {
            if (found_o) {
                *found_o = MEA_TRUE;
            }
            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                    &mea_sflow_prof_info_TBL[(*id_io)].data,
                    sizeof(*entry_po));
            }
            break;
        }
    }


    return MEA_OK;
}






MEA_Bool mea_drv_Sflow_Statistic_range(MEA_Unit_t unit_i, MEA_Uint32 id_i) {

    if (id_i >= MEA_SFLOW_MAX_COUNTERS) {
        return MEA_FALSE;
    }

    return MEA_TRUE;
}

MEA_Bool mea_drv_Sflow_Statistic_IsExist(MEA_Unit_t unit_i, MEA_Uint32 id_i) {

    if (mea_drv_Sflow_Statistic_range(unit_i, id_i) != MEA_TRUE)
    {
        return MEA_FALSE;
    }

    if (mea_sflow_Statistic_TBL[id_i].data.valid == MEA_TRUE)
    {
        return MEA_TRUE;
    }



    return MEA_FALSE;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

MEA_Status MEA_API_Sflow_Statistic_IsExist(MEA_Unit_t unit_i, MEA_Uint32 id_i , MEA_Bool silent, MEA_Bool *exist)
{
    if (!MEA_SFLOW_SUPPORT) {
        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_SFLOW_SUPPORT not support \n",
                __FUNCTION__);
        }
        return MEA_ERROR;
    }

    if (exist == NULL) {
        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - exist == NULL \n",
                __FUNCTION__);
        }
        return MEA_ERROR;
    }

    if (mea_drv_Sflow_Statistic_range(unit_i, id_i) != MEA_TRUE)
    {
        if (!silent) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Sflow_Statistic %d out of range \n",
                __FUNCTION__, id_i);
        }
        
        return MEA_ERROR;
    }
    
    *exist = mea_sflow_Statistic_TBL[id_i].data.valid;


    return MEA_OK;
}

MEA_Status mea_drv_Sflow_Statistic_Set(MEA_Unit_t unit_i, MEA_Uint32 id_i, MEA_Sflow_count_dbt *entry)
{


    mea_Sflow_Statistic_HW_dbt entryHW;

    /* Check if support */
    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_SFLOW_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }



    if (mea_drv_Sflow_Statistic_range(unit_i, id_i) != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Sflow_Statistic %d out of range \n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }

    MEA_OS_memset(&entryHW, 0, sizeof(entryHW));

    if (entry->valid == MEA_TRUE) {
        if (entry->profId >= MEA_SFLOW_MAX_PROF) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  profile  %d out of range \n",
                __FUNCTION__, entry->profId);
            return MEA_ERROR;
        }
    }

    
        

    if (entry->valid == MEA_TRUE && entry->profId == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  profile  %d is not valid profile to use \n",
            __FUNCTION__, entry->profId);
        return MEA_ERROR;
    }

    if (entry->valid == MEA_TRUE && mea_sflow_prof_info_TBL[entry->profId].valid == MEA_FALSE ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  profile  %d is not create \n",
            __FUNCTION__, entry->profId);
        return MEA_ERROR;
    }

    if (entry->valid == MEA_FALSE && mea_sflow_Statistic_TBL[id_i].data.valid == MEA_TRUE) {
        // set disable bye using the profile 0;
        entry->profId = 0;

    }
    else {
        /* check if we have any changes */
        if ((MEA_OS_memcmp(entry,
            &mea_sflow_Statistic_TBL[id_i].data,
            sizeof(*entry)) == 0)) {
            return MEA_OK;
        }

    }

    if (entry->valid == MEA_FALSE &&  mea_sflow_Statistic_TBL[id_i].data.valid == MEA_FALSE) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  profile  %d is already disable \n",
                __FUNCTION__, id_i);
                return MEA_ERROR;
    }

    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    if (entry->valid == MEA_TRUE) {
        /* Get the profile info */
        entryHW.st_packetCount = mea_sflow_prof_info_TBL[entry->profId].data.packetCount;
        entryHW.st_timeOut_type = mea_sflow_prof_info_TBL[entry->profId].data.timeOut_type;
        entryHW.st_profile = entry->profId;
    }

    /*Configure The HW  */
    if (mea_drv_sflow_satistic_UpdateHw(unit_i, id_i, &entryHW) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  mea_drv_sflow_satistic_UpdateHw %d failed \n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }

    /*Add and delete owner for the profile */
    if (entry->valid == MEA_FALSE && mea_sflow_Statistic_TBL[id_i].data.valid == MEA_TRUE) {
        mea_drv_Sflow_prof_delete_owner(unit_i, mea_sflow_Statistic_TBL[id_i].data.profId);
    }
    if (entry->valid == MEA_TRUE && mea_sflow_Statistic_TBL[id_i].data.valid == MEA_FALSE) {
        mea_drv_Sflow_prof_add_owner(unit_i, entry->profId);
    }
    if (entry->valid == MEA_TRUE && mea_sflow_Statistic_TBL[id_i].data.valid == MEA_TRUE) {
        mea_drv_Sflow_prof_delete_owner(unit_i, mea_sflow_Statistic_TBL[id_i].data.profId);
        mea_drv_Sflow_prof_add_owner(unit_i, entry->profId);
    }



    return MEA_OK;
}

MEA_Status MEA_API_Sflow_Statistic_Set(MEA_Unit_t unit_i, MEA_Uint32 id_i, MEA_Sflow_count_dbt *entry)
{


    /* Check if support */
    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_SFLOW_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry->valid == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s used delete \n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    if (mea_drv_Sflow_Statistic_Set(unit_i, id_i, entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Sflow_Statistic_Set failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }




    MEA_OS_memcpy(&(mea_sflow_Statistic_TBL[id_i].data), entry, sizeof(mea_sflow_Statistic_TBL[id_i].data));


    return MEA_OK;
}


MEA_Status MEA_API_Sflow_Statistic_Get(MEA_Unit_t unit_i, MEA_Uint32 id_i, MEA_Sflow_count_dbt *entry)
{

    /* Check if support */
    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_SFLOW_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_drv_Sflow_Statistic_range(unit_i, id_i) != MEA_TRUE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Sflow_Statistic %d out of range \n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }
    if (mea_drv_Sflow_Statistic_IsExist(unit_i, id_i) != MEA_TRUE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Sflow_Statistic %d Is not Exist \n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }


    MEA_OS_memcpy(entry, &(mea_sflow_Statistic_TBL[id_i].data), sizeof(MEA_Sflow_count_dbt));


    return MEA_OK;
}


MEA_Status MEA_API_Sflow_Statistic_Delete(MEA_Unit_t unit_i, MEA_Uint32 id_i)
{
    MEA_Sflow_count_dbt entry;
    /* Check if support */
    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_SFLOW_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    MEA_OS_memset(&entry, 0, sizeof(entry));
    if (mea_drv_Sflow_Statistic_Set(unit_i, id_i, &entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Sflow_Statistic_Set failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&(mea_sflow_Statistic_TBL[id_i].data), &entry, sizeof(mea_sflow_Statistic_TBL[id_i].data));

    return MEA_OK;
}


MEA_Status MEA_API_Sflow_Statistic_GetFirst(MEA_Unit_t               unit_i,
    MEA_Uint16                *id_o,
    MEA_Sflow_Prof_dbt        *entry_po, /* Can't be NULL */
    MEA_Bool                  *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_SFLOW_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
            return MEA_ERROR;
        }

    if (id_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - id_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (found_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ((*id_o) = 0;
        (*id_o) < MEA_SFLOW_MAX_PROF;
        (*id_o)++) {
        if (mea_sflow_Statistic_TBL[(*id_o)].data.valid == MEA_TRUE) {
            if (found_o) {
                *found_o = MEA_TRUE;
            }

            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                    &mea_sflow_Statistic_TBL[(*id_o)].data,
                    sizeof(*entry_po));
            }
            break;
        }
    }


    return MEA_OK;

}

MEA_Status MEA_API_Sflow_Statistic_GetNext(MEA_Unit_t                  unit_i,
    MEA_Uint16               *id_io,
    MEA_Sflow_Prof_dbt       *entry_po, /* Can't be NULL */
    MEA_Bool                 *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        /* Check if support */
        if (!MEA_SFLOW_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
            return MEA_ERROR;
        }
    if (id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id_io == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (found_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ((*id_io)++;
        (*id_io) < MEA_SFLOW_MAX_PROF;
        (*id_io)++) {
        if (mea_sflow_Statistic_TBL[(*id_io)].data.valid == MEA_TRUE) {
            if (found_o) {
                *found_o = MEA_TRUE;
            }
            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                    &mea_sflow_Statistic_TBL[(*id_io)].data,
                    sizeof(*entry_po));
            }
            break;
        }
    }


    return MEA_OK;
}


static void mea_drv_SflowQueue_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    MEA_Uint32            module = (MEA_Uint32)(long)arg3;
    //MEA_Sflow_Prof_dbt  *entry = (MEA_Sflow_Prof_dbt  *)arg4;




    MEA_Uint32                 val[4];
    MEA_Uint32                 i = 0;
    MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    count_shift = 0;
    for (i = 0; i < MEA_MAX_SFLOW_QUEUE_GROUP; i++) {
        if (mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[i].valid)
        {
            MEA_OS_insert_value(count_shift,
                8,
                (MEA_Uint32)mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal[0].clusterId,
                &val);
        }
        count_shift += 8;
    }




    /* Update Hw Entry */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
    { 
        if(module == MEA_MODULE_IF)
          MEA_API_WriteReg(unit_i,MEA_IF_WRITE_DATA_REG(i), val[i],MEA_MODULE_IF);
        else {
            MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA0 + (i*4), val[i], MEA_MODULE_BM);
        }

    }



}


static MEA_Status mea_drv_SflowQueue_UpdateHw(MEA_Unit_t  unit_i)

{

    MEA_ind_write_t   ind_write;

    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_write.tableOffset = MEA_GLOBAl_TBL_REG_SFLOW_MAP_QUEUE_Group;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_SflowQueue_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)MEA_MODULE_IF);
    
    // ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

    if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }



    /* build the ind_write value */
    ind_write.tableType     = ENET_BM_TBL_TYP_GW_GLOBAL_REGS;
    ind_write.tableOffset   = MEA_GLOBAl_TBL_REG_SET_QUEUE_SFLOW;
    ind_write.cmdReg        = MEA_BM_IA_CMD;
    ind_write.cmdMask       = MEA_BM_IA_CMD_MASK;
    ind_write.statusReg     = MEA_BM_IA_STAT;
    ind_write.statusMask    = MEA_BM_IA_STAT_MASK;
    ind_write.tableAddrReg  = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_SflowQueue_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)MEA_MODULE_BM);

    // ind_write.funcParam4 = (MEA_funcParam)new_entry_pi;

    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i, &ind_write, MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d mdl=%d failed (id=%d)\n",
            __FUNCTION__, ind_write.tableType, ENET_MODULE_BM, MEA_GLOBAl_TBL_REG_SET_QUEUE_SFLOW);
        return MEA_ERROR;
    }





    return MEA_OK;

}

MEA_Status MEA_API_Sflow_Get_TargetCluster(MEA_Unit_t  unit_i, MEA_sflow_queue_t *entry)
{
    MEA_API_LOG

    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
        return MEA_ERROR;
    }

    if (entry->groupIndex >= MEA_MAX_SFLOW_QUEUE_GROUP)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "groupIndex %d >= \n", MEA_MAX_SFLOW_QUEUE_GROUP);

        return MEA_ERROR;
    }

    entry->clusterId = mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[entry->groupIndex].clusterId;
    entry->valid = mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[entry->groupIndex].valid;

    return MEA_OK;
}


MEA_Status MEA_API_Sflow_Set_TargetCluster(MEA_Unit_t  unit_i,
                                    MEA_sflow_queue_t *entry)
{



    MEA_API_LOG

    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
        return MEA_ERROR;
    }
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "entry == NULL \n");
        return MEA_ERROR;
    }


    if (entry->groupIndex >= MEA_MAX_SFLOW_QUEUE_GROUP)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "groupIndex %d >= %d \n", entry->groupIndex, MEA_MAX_SFLOW_QUEUE_GROUP);

        return MEA_ERROR;
    }
    /************************************************************************/
    /* check the Queue is exist                                             */
    /************************************************************************/

    if (ENET_IsValid_Queue(unit_i,
        entry->clusterId,/*external*/
        MEA_FALSE) == MEA_FALSE) {
        return MEA_ERROR;
    }


    if (entry->valid == MEA_TRUE) {
        mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[entry->groupIndex].valid = entry->valid;
        mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[entry->groupIndex].clusterId = entry->clusterId;
    }
    else {
        mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[entry->groupIndex].valid = entry->valid;
        mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[entry->groupIndex].clusterId = 0xffff;
    }
    if (mea_sflow_Global_Entry->mea_sflow_queue.cluster_info[entry->groupIndex].valid == MEA_TRUE)
        mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal[entry->groupIndex].clusterId = enet_cluster_external2internal(entry->clusterId);
    else
        mea_sflow_Global_Entry->mea_sflow_queue.cluster_Internal[entry->groupIndex].clusterId = 0;

    /************************************************************************/
    /* Update HW                                                            */
    /************************************************************************/
    if(mea_drv_SflowQueue_UpdateHw(unit_i) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "mea_drv_SflowQueue_UpdateHw failed\n");
        return MEA_ERROR;
    }


    
    return MEA_OK;
}



static void mea_drv_Sflow_TICK_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)(long)arg4;


    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    //count_shift = 0;


    val[0] = entry;



    /* Update Hw Entry */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);
    }



// return MEA_OK;
}



static MEA_Status mea_drv_Sflow_TICK_UpdateHw(MEA_Unit_t  unit_i, MEA_Uint32 Tick) 
{

    MEA_ind_write_t   ind_write;

    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL;
    ind_write.tableOffset = MEA_GLOBAl_TBL_REG_SFLOW_TIC;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_Sflow_TICK_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
   // ind_write.funcParam3 = (MEA_funcParam)((long));

    ind_write.funcParam4 = (MEA_funcParam)((long)Tick);;

    if (MEA_API_WriteIndirect(unit_i, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, MEA_MODULE_IF);
        return MEA_ERROR;
    }


    return MEA_OK;
}




static void mea_drv_Sflow_DsaTag_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_Uint32            module = (MEA_Uint32)arg3;
    MEA_Sflow_Global_dbt  *entry = (MEA_Sflow_Global_dbt  *)arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
    MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    count_shift = 0;


    MEA_OS_insert_value(count_shift,
        16,
        (MEA_Uint32)entry->dsa_tag_EthType,
        &val);
    count_shift += 16;





    /* Update Hw Entry */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
    {

        MEA_API_WriteReg(unit_i, MEA_BM_IA_WRDATA0 + (i * 4), val[i], MEA_MODULE_BM);

    }



}



static MEA_Status mea_drv_Sflow_DsaTag_UpdateHw(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry)

{

    MEA_ind_write_t   ind_write;



    /* build the ind_write value */
    ind_write.tableType = ENET_BM_TBL_TYP_GW_GLOBAL_REGS;
    ind_write.tableOffset = MEA_GLOBAl_TBL_REG_SET_DSA_TAG_SFLOW;
    ind_write.cmdReg = MEA_BM_IA_CMD;
    ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_write.statusReg = MEA_BM_IA_STAT;
    ind_write.statusMask = MEA_BM_IA_STAT_MASK;
    ind_write.tableAddrReg = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_Sflow_DsaTag_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)MEA_MODULE_BM);

    ind_write.funcParam4 = (MEA_funcParam)entry;

    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i, &ind_write, MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d mdl=%d failed (id=%d)\n",
            __FUNCTION__, ind_write.tableType, ENET_MODULE_BM, MEA_GLOBAl_TBL_REG_SET_QUEUE_SFLOW);
        return MEA_ERROR;
    }





return MEA_OK;

}


static void mea_drv_Sflow_MtuCpu_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_Uint32            module = (MEA_Uint32)arg3;
    MEA_Sflow_Global_dbt  *entry = (MEA_Sflow_Global_dbt  *)arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
    MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    count_shift = 0;


    MEA_OS_insert_value(count_shift,
        14,
        (MEA_Uint32)entry->mtuCpu,
        &val);
    count_shift += 14;





    /* Update Hw Entry */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
    {

        MEA_API_WriteReg(unit_i, MEA_BM_IA_WRDATA0 + (i * 4), val[i], MEA_MODULE_BM);

    }



}


static MEA_Status mea_drv_Sflow_MtuCpu_UpdateHw(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry)

{

    MEA_ind_write_t   ind_write;



    /* build the ind_write value */
    ind_write.tableType = ENET_BM_TBL_TYP_GW_GLOBAL_REGS;
    ind_write.tableOffset = MEA_GLOBAl_TBL_REG_SET_MTUCPU_SFLOW;
    ind_write.cmdReg = MEA_BM_IA_CMD;
    ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_write.statusReg = MEA_BM_IA_STAT;
    ind_write.statusMask = MEA_BM_IA_STAT_MASK;
    ind_write.tableAddrReg = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_Sflow_MtuCpu_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)MEA_MODULE_BM);

    ind_write.funcParam4 = (MEA_funcParam)entry;

    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i, &ind_write, MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d mdl=%d failed (id=%d)\n",
            __FUNCTION__, ind_write.tableType, ENET_MODULE_BM, MEA_GLOBAl_TBL_REG_SET_QUEUE_SFLOW);
        return MEA_ERROR;
    }





    return MEA_OK;

}





static MEA_Status mea_drv_Sflow_Set_Global(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry, MEA_Sflow_Global_dbt *old_entry)
{

    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
        return MEA_ERROR;
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "entry == NULL \n");
        return MEA_ERROR;
    }

    if ((old_entry != NULL) && (MEA_OS_memcmp((entry),
        &(old_entry), sizeof(*entry)) == 0))
        return MEA_OK;

    if ((old_entry == NULL) ||
        (entry->time_Th != old_entry->time_Th)) {

        if (mea_drv_sflow_Set_Tick(unit_i, entry->time_Th) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "mea_drv_sflow_set_Tick failed \n");
            return MEA_ERROR;
        }
    }

    /************************************************************************/
    /* Write The ETHTYPE                                                    */
    /************************************************************************/
    if ((old_entry == NULL) ||
        (entry->dsa_tag_EthType != old_entry->dsa_tag_EthType)) {

        if (mea_drv_Sflow_DsaTag_UpdateHw(unit_i, entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "mea_drv_Sflow_DsaTag_UpdateHw failed \n");
            return MEA_ERROR;
        }


    }

    if ((old_entry == NULL) ||
        (entry->mtuCpu != old_entry->mtuCpu)) {
        if (mea_drv_Sflow_MtuCpu_UpdateHw(unit_i, entry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "mea_drv_Sflow_MtuCpu_UpdateHw failed \n");
            return MEA_ERROR;
        }

    }

    return MEA_OK;
}


MEA_Status MEA_API_Sflow_Set_Global(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry)
{

    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
        return MEA_ERROR;
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "entry == NULL \n");
        return MEA_ERROR;
    }

    if (MEA_OS_memcmp((entry),
        &(mea_sflow_Global_Entry), sizeof(*entry)) == 0)
        return MEA_OK;
    if(mea_drv_Sflow_Set_Global(unit_i, entry, &mea_sflow_Global_Entry->data) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "mea_drv_Sflow_Set_Global failed \n");
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&mea_sflow_Global_Entry->data, entry, sizeof(MEA_Sflow_Global_dbt));

    return MEA_OK;
}


MEA_Status MEA_API_Sflow_Get_Global(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry)
{
    
    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
        return MEA_ERROR;
    }
    
    if (entry == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "time_Th == NULL\n");
        return MEA_ERROR;
    }
    

    MEA_OS_memcpy(entry, &mea_sflow_Global_Entry->data, sizeof(*entry));
    

    return MEA_OK;
}

MEA_Status mea_drv_Sflow_Get_HWTick(MEA_Unit_t  unit_i, MEA_Uint32  *time_Th /* msec */)
{
    if (time_Th == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "time_Th == NULL\n");
        return MEA_ERROR;
    }
    *time_Th = mea_sflow_Global_Entry->mea_sflow_TICK_hw;


    return MEA_OK;
}


MEA_Status MEA_API_Sflow_Events(MEA_Unit_t unit, MEA_sflow_Events_dbt   *entry)
{
    MEA_ind_read_t ind_read;
    MEA_Uint32 read_data[2];
    MEA_Uint32 count_shift;
    //MEA_Uint32 val;

    if (!MEA_SFLOW_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "MEA_SFLOW_SUPPORT %s \n", MEA_STATUS_STR(MEA_SFLOW_SUPPORT));
        return MEA_ERROR;
    }


    ind_read.tableType = ENET_BM_TBL_EVENT_STATE;
    ind_read.tableOffset = 4;



    ind_read.cmdReg = MEA_BM_IA_CMD;
    ind_read.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_read.statusReg = MEA_BM_IA_STAT;
    ind_read.statusMask = MEA_BM_IA_STAT_MASK;
    ind_read.tableAddrReg = MEA_BM_IA_ADDR;
    ind_read.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_read.read_data = read_data;


    if (MEA_API_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(read_data),
        MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ReadIndirect from tableType %d / tableOffset %d module BM failed \n",
            __FUNCTION__, ind_read.tableType, ind_read.tableOffset);
        return MEA_ERROR;
    }



    count_shift = 0;

   

        



    entry->dst_cluster  = MEA_OS_read_value(count_shift, MEA_SFLOW_EV_dest_cluster, read_data);
                         count_shift += MEA_SFLOW_EV_dest_cluster;
   
    entry->sflow_type   = MEA_OS_read_value(count_shift, MEA_SFLOW_EV_sflow_type, read_data);
                        count_shift += MEA_SFLOW_EV_sflow_type;
                        if ((MEA_SFLOW_EV_sflow_type + MEA_SFLOW_EV_dest_cluster) < 12) 
                        {
                            count_shift =12; 
                        }


    entry->pm_id        = MEA_OS_read_value(count_shift, MEA_SFLOW_EV_PM, read_data);
                        count_shift += MEA_SFLOW_EV_PM;
    entry->src_port     = MEA_OS_read_value(count_shift, MEA_SFLOW_EV_SRC_PORT, read_data);
                        count_shift += MEA_SFLOW_EV_SRC_PORT;
    entry->sflow_en     = MEA_OS_read_value(count_shift, MEA_SFLOW_EV_sflow_en, read_data);
                        count_shift += MEA_SFLOW_EV_sflow_en;
   entry->p_to_mp       = MEA_OS_read_value(count_shift, MEA_SFLOW_EV_P_TO_MP, read_data);
                        count_shift += MEA_SFLOW_EV_P_TO_MP;
 





    return MEA_OK;

}




