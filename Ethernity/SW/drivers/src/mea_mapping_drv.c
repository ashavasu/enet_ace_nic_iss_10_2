/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_if_regs.h"
#include "mea_globals_drv.h"
#include "mea_mapping_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_MAPPING_TABLE_KEY_TYPE_DEF_PRI_START_INDEX       0
#define MEA_MAPPING_TABLE_KEY_TYPE_DEF_PRI_END_INDEX         7
#define MEA_MAPPING_TABLE_KEY_TYPE_QTAG_START_INDEX          8
#define MEA_MAPPING_TABLE_KEY_TYPE_QTAG_END_INDEX            15
#define MEA_MAPPING_TABLE_KEY_TYPE_L2_PRI_START_INDEX        16
#define MEA_MAPPING_TABLE_KEY_TYPE_L2_PRI_END_INDEX          23
#define MEA_MAPPING_TABLE_KEY_TYPE_PRECEDENCE_START_INDEX    24
#define MEA_MAPPING_TABLE_KEY_TYPE_PRECEDENCE_END_INDEX      31
#define MEA_MAPPING_TABLE_KEY_TYPE_TOS_START_INDEX		     32
#define MEA_MAPPING_TABLE_KEY_TYPE_TOS_END_INDEX             47
#define MEA_MAPPING_TABLE_KEY_TYPE_DSCP_START_INDEX		     64
#define MEA_MAPPING_TABLE_KEY_TYPE_DSCP_END_INDEX            127

#if 0
#define MEA_MAPPING_HW_TABLE_0_START_INDEX 0
#define MEA_MAPPING_HW_TABLE_1_START_INDEX 128
#endif



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct {
    MEA_Mapping_t   id;
    MEA_Mapping_dbt mapping_info;
    MEA_Uint32      valid : 1;
    MEA_Uint32      pad0  : 31;
} MEA_Mapping_Entry_dbt;


typedef struct {
	MEA_Mapping_Entry_dbt MEA_Mapping_Table[MEA_MAPPING_MAX_ENTRIES];
	MEA_Uint32      num_of_mapping_entries;
	MEA_Uint32      num_of_registered;
    MEA_Uint32      valid : 1;
    MEA_Uint32      pad0  : 31;
} MEA_Mapping_Profile_dbt;

typedef struct {
    MEA_Uint32      valid   :1;
    MEA_Uint32 num_of_owners :31;
    MEA_Protocol_To_pri_data_dbt data[MEA_NUM_OF_ENTRY_PROTOCOL_TO_PRI];
    
}MEA_Protocol_To_pri_data_Entry_dbt;
/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static   MEA_Mapping_Profile_dbt MEA_Mapping_Profile_Table[MEA_MAPPING_PROFILE_MAX_ENTRIES];

MEA_Bool MEA_Mapping_InitDone = MEA_FALSE;

MEA_Bool MEA_Mapping_Enable = MEA_TRUE;

MEA_Protocol_To_pri_data_Entry_dbt     *MEA_Protocol_To_Priority_Mapping_Table=NULL;


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
static MEA_Status mea_Mapping_UpdateHw(MEA_Uint32               profile_id,
									   MEA_Uint32               index,
                                       MEA_Mapping_Entry_dbt*   entry,
                                       MEA_Mapping_Entry_dbt*   old_entry) ;

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Disable_Mapping>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Disable_Mapping(void)
{
	MEA_Uint32 i,j;
	MEA_Mapping_Entry_dbt entry;

	if ( MEA_Mapping_Enable == MEA_FALSE )
		return MEA_OK;

	for (j=1; j<MEA_MAPPING_PROFILE_MAX_ENTRIES;j++)
	{
		if ( MEA_Mapping_Profile_Table[j].valid )
		{
			for (i=0; 
				 i < MEA_MAPPING_MAX_ENTRIES;
				 i++)
			{
				MEA_OS_memset(&(entry),0,sizeof(entry));
		 
				/* Update the HW */
				if ( mea_Mapping_UpdateHw(j,i, &entry, 0)  != MEA_OK )
				{    
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - mea_Mapping_UpdateHw failed \n",__FUNCTION__);
					return MEA_ERROR;
				}    
			}
		}
	}

	MEA_Mapping_Enable = MEA_FALSE;

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Enable_Mapping>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Enable_Mapping(void)
{
	MEA_Uint32 i,j;

	if ( MEA_Mapping_Enable )
		return MEA_OK;

	for (j=1; j<MEA_MAPPING_PROFILE_MAX_ENTRIES;j++)
	{
		if ( MEA_Mapping_Profile_Table[j].valid )
		{

			for (i=0; 
				 i < MEA_MAPPING_MAX_ENTRIES;
				 i++)
			{
				/* Update the HW */
				if ( mea_Mapping_UpdateHw(j,i, &(MEA_Mapping_Profile_Table[j].MEA_Mapping_Table[i]), 0)  != MEA_OK )
				{    
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - mea_Mapping_UpdateHw failed \n",__FUNCTION__);
					return MEA_ERROR;
				}   
			}
		}
	}

	MEA_Mapping_Enable = MEA_TRUE;

	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_Mapping_Table>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_Mapping_Table(MEA_Unit_t unit_i)
{
	MEA_Mapping_Entry_dbt entry;
	MEA_Uint32 i,j;
    MEA_Mapping_InitDone = MEA_FALSE;


	MEA_Mapping_Enable = MEA_TRUE;
    if(b_bist_test){
        return MEA_OK;
    }
    /* Init IF Mapping Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Mapping table ...\n");

    MEA_OS_memset(&(MEA_Mapping_Profile_Table[0]),0,sizeof(MEA_Mapping_Profile_Table));

    MEA_OS_memset(&(entry),0,sizeof(entry));
	for ( i=1; i<MEA_MAPPING_PROFILE_MAX_ENTRIES; i++ )
	{
		for (j=0; 
			 j < MEA_MAPPING_MAX_ENTRIES;
			 j++)
		{
			
	 
			/* Update the HW */
			if ( mea_Mapping_UpdateHw(i,j, &entry, 0)  != MEA_OK )
			{    
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_Mapping_UpdateHw failed \n",__FUNCTION__);
				return MEA_ERROR;
			}    
		}
	}



    MEA_Mapping_InitDone = MEA_TRUE;     

    return MEA_OK; 
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_Mapping_Table>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_Mapping_Table(MEA_Unit_t unit_i)
{
	MEA_Uint32 i,j;
	for ( i=1; i<MEA_MAPPING_PROFILE_MAX_ENTRIES; i++ )
	{
		for (j=0; 
			 j < MEA_MAPPING_MAX_ENTRIES;
			 j++)
		{
			
	 
			/* Update the HW */
			if ( mea_Mapping_UpdateHw(i,
                                      j, 
                                      &(MEA_Mapping_Profile_Table[i].MEA_Mapping_Table[j]),
                                      NULL) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_Mapping_UpdateHw failed \n",__FUNCTION__);
				return MEA_ERROR;
			}    
		}
	}

    return MEA_OK; 
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_get_index_range_by_key_type>                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_get_index_range_by_key_type(MEA_Mapping_Type_te key_type,
											   MEA_Uint32         *start_o,
											   MEA_Uint32         *end_o)
{
	if ( start_o == NULL || end_o == NULL )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - NULL input\n",
				                 __FUNCTION__);
		return MEA_ERROR;         
	}

	*start_o = 0;
	*end_o = 0;

	switch (key_type)
	{
	case MEA_MAPPING_TYPE_L2_PRI:
		*start_o = MEA_MAPPING_TABLE_KEY_TYPE_L2_PRI_START_INDEX;
		*end_o = MEA_MAPPING_TABLE_KEY_TYPE_L2_PRI_END_INDEX;
		break;
	case MEA_MAPPING_TYPE_DEF_PRI:
		*start_o = MEA_MAPPING_TABLE_KEY_TYPE_DEF_PRI_START_INDEX;
		*end_o = MEA_MAPPING_TABLE_KEY_TYPE_DEF_PRI_END_INDEX;
		break;
	case MEA_MAPPING_TYPE_IPV4_TOS:
		*start_o = MEA_MAPPING_TABLE_KEY_TYPE_TOS_START_INDEX;
		*end_o = MEA_MAPPING_TABLE_KEY_TYPE_TOS_END_INDEX;
		break;
	case MEA_MAPPING_TYPE_IPV4_PRECEDENCE:
		*start_o = MEA_MAPPING_TABLE_KEY_TYPE_PRECEDENCE_START_INDEX;
		*end_o = MEA_MAPPING_TABLE_KEY_TYPE_PRECEDENCE_END_INDEX;
		break;
	case MEA_MAPPING_TYPE_IPV4_DSCP:
		*start_o = MEA_MAPPING_TABLE_KEY_TYPE_DSCP_START_INDEX;
		*end_o = MEA_MAPPING_TABLE_KEY_TYPE_DSCP_END_INDEX;
		break;
	case MEA_MAPPING_TYPE_IPV6_TC:
	default:
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - none-supported key type input %d\n",
				                 __FUNCTION__, key_type);
		return MEA_ERROR;         	
	}

	if ( *start_o >= MEA_MAPPING_MAX_ENTRIES ||
		 *end_o >= MEA_MAPPING_MAX_ENTRIES )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - invalid indexes %d-%d\n",
				                 __FUNCTION__, *start_o, *end_o);
        *start_o = 0;
        *end_o = 0;
		return MEA_ERROR;         	
	}
	return MEA_OK;
}


MEA_Status mea_drv_get_key_val_by_type(MEA_Mapping_dbt *entry_i, MEA_Uint32 *val_o)
{
	switch (entry_i->key.type)
	{
	case MEA_MAPPING_TYPE_L2_PRI:
		*val_o = entry_i->key.value.l2_pri.l2_pri;
		break;
	case MEA_MAPPING_TYPE_ETHERTYPE:
		*val_o = entry_i->key.value.ethertype.ethertype;
		break;
	case MEA_MAPPING_TYPE_IPV4_TOS:
		*val_o = entry_i->key.value.ipv4_tos.tos;
		break;
	case MEA_MAPPING_TYPE_IPV4_PRECEDENCE:
		*val_o = entry_i->key.value.ipv4_precedence.precedence;
		break;
	case MEA_MAPPING_TYPE_IPV4_DSCP:
		*val_o = entry_i->key.value.ipv4_dscp.dscp;
		break;
	case MEA_MAPPING_TYPE_IPV6_TC:
		*val_o = entry_i->key.value.ipv6_tc.tc;
		break;
	default:
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - none-supported key type input %d\n",
				                 __FUNCTION__, entry_i->key.type);
		return MEA_ERROR;         	
	}

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Mapping_GetFreeEntryIndex>                                */
/*                                                                           */
/* Pay attention that the function sets the Valid flag and the Mapping ID    */
/* If key type is EtherType - the index_io holds the already chosen CAM Id   */
/* and this function will only mark the selected entry as used               */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Mapping_GetFreeEntryIndex(	MEA_Uint32          profile_id,
													MEA_Mapping_Type_te key_type,
													MEA_Uint32          key_value,
													MEA_Mapping_t* index_io)
{
	MEA_Uint32 start, end;

    if ( index_io == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - index_io == NULL\n",
                             __FUNCTION__);
       return MEA_ERROR;         
    }

MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id)
	
	if ( MEA_Mapping_Profile_Table[profile_id].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id);
       return MEA_ERROR;         
	}

	if ( mea_drv_get_index_range_by_key_type( key_type,
											  &start,
											  &end ) != MEA_OK )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - mea_drv_get_index_range_by_key_type failed\n",
				                 __FUNCTION__);
		return MEA_ERROR;         
	}

	if ( start+key_value > end )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - index %d is out of range\n",
				                 __FUNCTION__, start+key_value);
		return MEA_ERROR;         
	}

	if ( MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[start+key_value].valid == MEA_FALSE )
    {
		MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[start+key_value].valid = MEA_TRUE;
        MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[start+key_value].id = *index_io;
		*index_io = (MEA_Mapping_t)(start+key_value);
        return MEA_OK;
    }     

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - no free Mapping table %d entry index for profile %d, key_type %d. index %d is occupied\n",
                             __FUNCTION__, profile_id, key_type, start+key_value);
    return MEA_ERROR;         
}

static MEA_Status mea_Mapping_GetFreeProfileIndex( MEA_Uint32 *index_o )

{
    if ( index_o == NULL )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - index_o == NULL\n",
                             __FUNCTION__);
       return MEA_ERROR;         
    }

	for (*index_o = 1; *index_o < MEA_MAPPING_PROFILE_MAX_ENTRIES; (*index_o)++)
	{
		if ( MEA_Mapping_Profile_Table[*index_o].valid == MEA_FALSE )
		{
			MEA_Mapping_Profile_Table[*index_o].valid = MEA_TRUE;
			return MEA_OK;
		}
	}   

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - no free Mapping Profile\n",
                             __FUNCTION__);
    return MEA_ERROR;         
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Is_Mapping_Entry_Exist>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Bool mea_drv_Is_Mapping_Entry_Exist (MEA_Uint32 profile_id,
										 MEA_Mapping_dbt *mapping_info_i)                                          
{
    MEA_Uint32 start, end, index;
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id)
	
	if ( MEA_Mapping_Profile_Table[profile_id].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id);
       return MEA_ERROR;         
	}

    if (mapping_info_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mapping_info_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
  
	if ( mea_drv_get_index_range_by_key_type( mapping_info_i->key.type,
											  &start,
											  &end ) != MEA_OK )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s - mea_drv_get_index_range_by_key_type failed\n",
				                 __FUNCTION__);
		return MEA_ERROR;         
	}

    for ( index = start; index <= end; index++ )
    {
        if ((MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].valid == MEA_TRUE) && 
			(MEA_OS_memcmp(&(mapping_info_i->data), 
			               &(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].mapping_info.data), 
						   sizeof(mapping_info_i->data) == 0)))           
        {
		     return MEA_TRUE; 
	    }
    }       

    return MEA_FALSE;
} 

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_mapping_UpdateHwEntry>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status  mea_mapping_UpdateHwEntry(MEA_Mapping_Entry_dbt* entry)
{
	MEA_Uint32	              val;
 
    val = 0;

	val  = ((entry->mapping_info.data.classifier_pri_value 
              << MEA_OS_calc_shift_from_mask(MEA_IF_MAPPING_TABLE_CLASS_PRI_MASK)
             ) & MEA_IF_MAPPING_TABLE_CLASS_PRI_MASK
            ); 

	val |= ((entry->mapping_info.data.pri_queue_value
              << MEA_OS_calc_shift_from_mask(MEA_IF_MAPPING_TABLE_PRI_Q_MASK)
             ) & MEA_IF_MAPPING_TABLE_PRI_Q_MASK
            );

	val |= ((entry->mapping_info.data.stamping_pri_value
              << MEA_OS_calc_shift_from_mask(MEA_IF_MAPPING_TABLE_STAMP_PRI_MASK)
             ) & MEA_IF_MAPPING_TABLE_STAMP_PRI_MASK
            );

	val |= ((entry->mapping_info.data.color_value
              << MEA_OS_calc_shift_from_mask(MEA_IF_MAPPING_TABLE_COLOR_MASK)
             ) & MEA_IF_MAPPING_TABLE_COLOR_MASK
            );

	val |= ((entry->mapping_info.data.classifier_pri_valid
              << MEA_OS_calc_shift_from_mask(MEA_IF_MAPPING_TABLE_CLASS_PRI_VALID_MASK)
             ) & MEA_IF_MAPPING_TABLE_CLASS_PRI_VALID_MASK
            );

	val |= ((entry->mapping_info.data.pri_queue_valid
              << MEA_OS_calc_shift_from_mask(MEA_IF_MAPPING_TABLE_PRI_Q_VALID_MASK)
             ) & MEA_IF_MAPPING_TABLE_PRI_Q_VALID_MASK
            );

	val |= ((entry->mapping_info.data.stamping_pri_valid
              << MEA_OS_calc_shift_from_mask(MEA_IF_MAPPING_TABLE_STAMP_PRI_VALID_MASK)
             ) & MEA_IF_MAPPING_TABLE_STAMP_PRI_VALID_MASK
            );

	val |= ((entry->mapping_info.data.color_valid
              << MEA_OS_calc_shift_from_mask(MEA_IF_MAPPING_TABLE_COLOR_VALID_MASK)
             ) & MEA_IF_MAPPING_TABLE_COLOR_VALID_MASK
            );

    MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0),val,MEA_MODULE_IF);

    return MEA_OK;
}

MEA_Status mea_drv_mapping_get_hw_params(MEA_Uint32 profile_id, MEA_Uint32 mapping_index, MEA_Uint32 *table_o,MEA_Uint32 *index_o)
{
	*table_o = MEA_IF_CMD_PARAM_TBL_TYP_MAPPING;
	*index_o = (profile_id-1)*MEA_MAPPING_MAX_ENTRIES + mapping_index;

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_Mapping_UpdateHw>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_Mapping_UpdateHw(MEA_Uint32               profile_id,
									   MEA_Uint32               index,
                                       MEA_Mapping_Entry_dbt*   entry,
                                       MEA_Mapping_Entry_dbt*   old_entry) 
{
	MEA_ind_write_t ind_write;
	MEA_Uint32 hw_index, hw_table;

    if ( entry == NULL )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - NULL entry input\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }   

    /* Check if we have changes */
    if (MEA_Mapping_InitDone && 
        old_entry != NULL      &&
       (MEA_OS_memcmp(entry,old_entry,sizeof(*entry)) == 0)) {
       return MEA_OK;
    }

	if ( mea_drv_mapping_get_hw_params(profile_id, 
		                              index,
									  &hw_table,
									  &hw_index) != MEA_OK )
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_mapping_get_hw_index failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
	}
	
    return MEA_OK;  // we not support  do not write to this table


	/* build the ind_write value */
	ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_MAPPING;
	ind_write.tableOffset	= hw_index;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_mapping_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)entry;

	
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }

    /* build the ind_write value */
	ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_MAPPING_2;
	ind_write.tableOffset	= hw_index;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_mapping_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)entry;

	
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }

	return MEA_OK;   	 
}


MEA_Status mea_drv_mapping_key_stamping_pri_handle ( MEA_Uint32         profile_id,
													 MEA_Mapping_dbt   *entry_i,
                     		                         MEA_Mapping_t       *id_o)
{
#if 0
	MEA_Uint32 index;
    MEA_Mapping_Entry_dbt entry;

	if ( entry_i->key.type != MEA_MAPPING_TYPE_L2_PRI )
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - only stamping pri key type can be in mea_drv_mapping_key_stamping_pri_handle \n",
                          __FUNCTION__);
		return MEA_ERROR;
    }
	if ( !entry_i->data.pri_queue_valid ||
		!entry_i->data.classifier_pri_valid)
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - data pri queue && classifier priority should be enabled in mea_drv_mapping_key_stamping_pri_handle \n",
                          __FUNCTION__);
		return MEA_ERROR;
    }

	mea_drv_get_index_range_by_key_type
	for ( index = 0; index < MEA_MAPPING_MAX_ENTRIES; index++ )
	{
		if ( MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].valid )
		{
			if ( MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].mapping_info.data.stamping_pri_valid &&
				 (MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].mapping_info.data.stamping_pri_value ==
				  entry_i->key.value.l2_pri.l2_pri) )
			{
				MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].mapping_info.data.pri_queue_valid = MEA_TRUE;
				MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].mapping_info.data.pri_queue_value = entry_i->data.pri_queue_value;
				MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].mapping_info.data.classifier_pri_valid = MEA_TRUE;
				MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].mapping_info.data.classifier_pri_value = entry_i->data.classifier_pri_value;
				
				MEA_OS_memset(&(entry),0,sizeof(entry));

				entry.id = index;

			    MEA_OS_memcpy( &(entry.mapping_info),
							   &(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[index].mapping_info),
							   sizeof(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[0].mapping_info) );  
				entry.valid = MEA_TRUE;


				 /* Update the HW */
				if ( mea_Mapping_UpdateHw( profile_id,
										   index,
										   &entry,
										   0) != MEA_OK )
				{		    
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
									  "%s - mea_Mapping_UpdateHw failed \n",__FUNCTION__);
					return MEA_ERROR;
				}                  
			}		
		}
	}

	*id_o = index;
	return MEA_OK;
#else
	MEA_Uint32 key_val;
    MEA_Mapping_Entry_dbt entry;

	key_val = 0;
	MEA_OS_memset(&(entry),0,sizeof(entry));

	if ( mea_drv_get_key_val_by_type(entry_i, &key_val) != MEA_OK )
	{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s -  mea_drv_get_key_val_by_type failed \n",__FUNCTION__);
			return MEA_ERROR;
	}
    /* Get free entry index */
	if (mea_Mapping_GetFreeEntryIndex(profile_id,
		                              entry_i->key.type, key_val, id_o) != MEA_OK) 
    {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_Mapping_GetFreeEntryIndex failed \n",__FUNCTION__);
        return MEA_ERROR;
    }
  

    entry.id = *id_o;

    MEA_OS_memcpy( &(entry.mapping_info),
                   entry_i,
                   sizeof(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[0].mapping_info) );  
    entry.valid = MEA_TRUE;


    /* Update the HW */
    if ( mea_Mapping_UpdateHw( profile_id,
							   *id_o,
                               &entry,
                               &(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[*id_o]) ) != MEA_OK )
    {
        /* free the new taken entry */
        MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[*id_o].valid = MEA_FALSE;
    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_Mapping_UpdateHw failed \n",__FUNCTION__);
        return MEA_ERROR;
    }                          

	MEA_OS_memcpy(&(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[*id_o]) , 
                  &entry, 
                  sizeof(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[0]));

    MEA_Mapping_Profile_Table[profile_id].num_of_mapping_entries++;	
#endif

	return MEA_OK;

}



MEA_Status mea_drv_mapping_key_ip_pri_handle ( MEA_Uint32         profile_id,
											   MEA_Mapping_dbt   *entry_i,
                     		                   MEA_Mapping_t     *id_o)
{

	MEA_Uint32 key_val;
    MEA_Mapping_Entry_dbt entry;

	key_val = 0;
	MEA_OS_memset(&(entry),0,sizeof(entry));

	if ( mea_drv_get_key_val_by_type(entry_i, &key_val) != MEA_OK )
	{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s -  mea_drv_get_key_val_by_type failed \n",__FUNCTION__);
			return MEA_ERROR;
	}
    /* Get free entry index */
	if (mea_Mapping_GetFreeEntryIndex(profile_id,
		                              entry_i->key.type, key_val, id_o) != MEA_OK) 
    {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_Mapping_GetFreeEntryIndex failed \n",__FUNCTION__);
        return MEA_ERROR;
    }
  

    entry.id = *id_o;

    MEA_OS_memcpy( &(entry.mapping_info),
                   entry_i,
                   sizeof(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[0].mapping_info) );  
    entry.valid = MEA_TRUE;


    /* Update the HW */
    if ( mea_Mapping_UpdateHw( profile_id,
							   *id_o,
                               &entry,
                               &(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[*id_o]) ) != MEA_OK )
    {
        /* free the new taken entry */
        MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[*id_o].valid = MEA_FALSE;
    
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_Mapping_UpdateHw failed \n",__FUNCTION__);
        return MEA_ERROR;
    }                          

	MEA_OS_memcpy(&(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[*id_o]) , 
                  &entry, 
                  sizeof(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[0]));

    MEA_Mapping_Profile_Table[profile_id].num_of_mapping_entries++;	

	return MEA_OK;
}


MEA_Status mea_drv_mapping_key_ethertype_handle ( MEA_Uint32         profile_id,
												  MEA_Mapping_dbt   *entry_i,
                     		                      MEA_Mapping_t     *id_o)
{
		MEA_CAM_Entry_dbt cam_entry;
		MEA_Uint32 cam_id;
		MEA_Mapping_Entry_dbt entry;

		MEA_OS_memset(&(cam_entry),0,sizeof(cam_entry));
		MEA_OS_memset(&(entry),0,sizeof(entry));

		cam_entry.ethertype = entry_i->key.value.ethertype.ethertype;
		cam_entry.valid = MEA_TRUE;
		cam_entry.ether_pri = entry_i->data.stamping_pri_value;
		
		if ( mea_drv_CAM_Add_Entry(&cam_entry,
                                   &cam_id ) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                  "%s -  mea_drv_CAM_Add_Entry failed \n",__FUNCTION__);
			return MEA_ERROR;
		}
		*id_o = (MEA_Mapping_t)cam_id;

		MEA_OS_memcpy(&(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[*id_o].mapping_info) , 
					  entry_i, 
					  sizeof(MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[0].mapping_info));

		MEA_Mapping_Profile_Table[profile_id].num_of_mapping_entries++;	
		MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[*id_o].valid = MEA_TRUE;
		MEA_Mapping_Profile_Table[profile_id].MEA_Mapping_Table[*id_o].id = *id_o;
		return MEA_OK;
}


MEA_Status mea_drv_mapping_register_profile ( MEA_Uint32 profile_id)
{
MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id)
	
	if ( MEA_Mapping_Profile_Table[profile_id].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id);
       return MEA_ERROR;         
	}

	MEA_Mapping_Profile_Table[profile_id].num_of_registered++;

	return MEA_OK;
}

MEA_Status mea_drv_mapping_unregister_profile ( MEA_Uint32 profile_id)
{
MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id)
	
	if ( MEA_Mapping_Profile_Table[profile_id].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id);
       return MEA_ERROR;         
	}

	MEA_Mapping_Profile_Table[profile_id].num_of_registered--;

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Create_Mapping_Profile                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_Mapping_Profile    (MEA_Unit_t        		unit_i,
                     	    	              MEA_Uint32     	     	*id_io)
{

	MEA_API_LOG
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_Create_Mapping_Profile not support\n",
        __FUNCTION__);
    return MEA_ERROR; 


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (id_io == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - id_io == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if(b_bist_test){
        return MEA_OK;
    }

	if ( MEA_Mapping_Enable == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Mapping is disabled\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}

	if ( *id_io != 0 )
	{
		MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(*id_io)	
		if ( MEA_Mapping_Profile_Table[*id_io].valid == MEA_TRUE )
		{
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - Mapping Profile with ID %d is occupied\n",
							 __FUNCTION__, *id_io);
		   return MEA_ERROR; 
		}
		
		MEA_Mapping_Profile_Table[*id_io].valid = MEA_TRUE;
	}
	else
	{
		if( mea_Mapping_GetFreeProfileIndex( id_io ) != MEA_OK )
		{
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							 "%s - mea_Mapping_GetFreeProfileIndex failed\n",
							 __FUNCTION__);
		   return MEA_ERROR; 
		}
	}

	return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Delete_Mapping_Profile                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_Mapping_Profile (MEA_Unit_t        		unit_i,
                                           MEA_Uint32   		    id_i)
{
	MEA_Uint32 j;
	MEA_Mapping_Entry_dbt entry;

	MEA_API_LOG
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_Delete_Mapping_Profile not support\n",
        __FUNCTION__);
    return MEA_ERROR; 

	if ( MEA_Mapping_Enable == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Mapping is disabled\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    /* Check for valid index parmeter */
MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(id_i)
	
	if ( MEA_Mapping_Profile_Table[id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,id_i);
       return MEA_ERROR;         
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	if ( MEA_Mapping_Profile_Table[id_i].num_of_registered > 0 )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d cannot be deleted. There are %d registered ports\n",
                             __FUNCTION__,id_i,MEA_Mapping_Profile_Table[id_i].num_of_registered);
       return MEA_ERROR;         
	}
	MEA_Mapping_Profile_Table[id_i].valid = MEA_FALSE;
	MEA_OS_memset(&(MEA_Mapping_Profile_Table[id_i]),
		          0,
				  sizeof(MEA_Mapping_Profile_Table[id_i]));

	for (j=0; 
		 j < MEA_MAPPING_MAX_ENTRIES;
		 j++)
	{
		MEA_OS_memset(&(entry),0,sizeof(entry));
 
		/* Update the HW */
		if ( mea_Mapping_UpdateHw(id_i,j, &entry, 0)  != MEA_OK )
		{    
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_Mapping_UpdateHw failed \n",__FUNCTION__);
			return MEA_ERROR;
		}    
	}

	return MEA_OK;
}


MEA_Status MEA_API_GetFirst_Mapping_Profile (MEA_Unit_t       unit,
										     MEA_Uint32      *profile_id_o,										   
										     MEA_Bool        *found_o)
{
	MEA_API_LOG
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_GetFirst_Mapping_Profile not support\n",
        __FUNCTION__);
    return MEA_ERROR; 


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (profile_id_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - profile_id_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (found_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	*found_o = MEA_FALSE;

	for ( *profile_id_o = 1; *profile_id_o < MEA_MAPPING_PROFILE_MAX_ENTRIES; (*profile_id_o)++)
	{
		if ( MEA_Mapping_Profile_Table[*profile_id_o].valid == MEA_TRUE )
		{
			*found_o = MEA_TRUE;
			return MEA_OK;
		}
	}
	
	return MEA_OK;
}


MEA_Status MEA_API_GetNext_Mapping_Profile  (MEA_Unit_t       unit,
                                           MEA_Uint32       profile_id_i,
										   MEA_Uint32      *profile_id_o,	
										   MEA_Bool        *found_o)
{

	MEA_API_LOG
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_GetNext_Mapping_Profile not support\n",
        __FUNCTION__);
    return MEA_ERROR; 

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id_i)
	
	if ( MEA_Mapping_Profile_Table[profile_id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id_i);
       return MEA_ERROR;         
	}

    if (profile_id_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - profile_id_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (found_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	*found_o = MEA_FALSE;

	for ( *profile_id_o = profile_id_i+1; *profile_id_o < MEA_MAPPING_PROFILE_MAX_ENTRIES; (*profile_id_o)++)
	{
		if ( MEA_Mapping_Profile_Table[*profile_id_o].valid == MEA_TRUE )
		{
			*found_o = MEA_TRUE;
			return MEA_OK;
		}
	}
	
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Add_Mapping_Entry                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Add_Mapping_Entry       (MEA_Unit_t        		 unit_i,
											MEA_Uint32    	     	 profile_id_i,
											MEA_Uint32   		    *entry_id_io,
											MEA_Mapping_dbt   		*entry_i)
{
	MEA_Mapping_t id;

	MEA_API_LOG
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_Add_Mapping_Entry not support\n",
        __FUNCTION__);
    return MEA_ERROR; 

	if ( MEA_Mapping_Enable == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Mapping is disabled\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (entry_i == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
      
MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id_i)
	
	if ( MEA_Mapping_Profile_Table[profile_id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id_i);
       return MEA_ERROR;         
	}

    if (entry_id_io == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_id_io == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	/* If key type is IPv6 TC - block it */
	if ( entry_i->key.type == MEA_MAPPING_TYPE_IPV6_TC )
	{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - IPv6 TC key type not supported\n",__FUNCTION__);
        return MEA_ERROR;
    } 

	if ( entry_i->key.type != MEA_MAPPING_TYPE_ETHERTYPE )
	{
		/* Check if an entry with such mapping_info already exists */
		if ( mea_drv_Is_Mapping_Entry_Exist(profile_id_i, entry_i ) == MEA_TRUE )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - there is an existing Mapping table entry with the input data\n",__FUNCTION__);
			return MEA_ERROR;
		} 
	}

	/* Check key type and handle accordingly */
	if ( entry_i->key.type == MEA_MAPPING_TYPE_ETHERTYPE )
	{
		if ( mea_drv_mapping_key_ethertype_handle(profile_id_i,entry_i, &id) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_mapping_key_ethertype_handle failed\n",__FUNCTION__);
			return MEA_ERROR;
		}
	}
	else if ( entry_i->key.type == MEA_MAPPING_TYPE_IPV4_TOS ||
		      entry_i->key.type == MEA_MAPPING_TYPE_IPV4_PRECEDENCE ||
			  entry_i->key.type == MEA_MAPPING_TYPE_IPV4_DSCP )
	{
		if ( mea_drv_mapping_key_ip_pri_handle(profile_id_i,entry_i, &id) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_mapping_key_ip_pri_handle failed\n",__FUNCTION__);
			return MEA_ERROR;
		}
	}
	else if ( entry_i->key.type == MEA_MAPPING_TYPE_L2_PRI )
	{
		if ( mea_drv_mapping_key_stamping_pri_handle(profile_id_i,entry_i, &id) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_mapping_key_stamping_pri_handle failed\n",__FUNCTION__);
			return MEA_ERROR;
		}
	}
	else
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				          "%s - non supported key type %d\n",__FUNCTION__, entry_i->key.type);
		return MEA_ERROR;
	}


	*entry_id_io = id;

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Remove_Mapping_Entry                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Remove_Mapping_Entry       (MEA_Unit_t        		 unit_i,
											   MEA_Uint32    	     	 profile_id_i,
											   MEA_Uint32   		     entry_id_i)
{
    MEA_Mapping_Entry_dbt entry;
	MEA_CAM_Entry_dbt cam_entry;
	MEA_Bool           found;

	MEA_API_LOG

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_Remove_Mapping_Entry not support\n",
        __FUNCTION__);
    return MEA_ERROR; 

	if ( MEA_Mapping_Enable == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Mapping is disabled\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    /* Check for valid index parameter */
    MEA_CHECK_MAPPING_ENTRY_INDEX_IN_RANGE(entry_id_i)

MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id_i)
	
	if ( MEA_Mapping_Profile_Table[profile_id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id_i);
       return MEA_ERROR;         
	}
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	/* L2 PRI and DEF PRI segments should not be erased */
	if (  entry_id_i <=MEA_MAPPING_TABLE_KEY_TYPE_DEF_PRI_END_INDEX )
	{
		if ( mea_drv_get_CAM_entry ((MEA_Uint32)entry_id_i,
                                    &cam_entry,
                                    &found) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_drv_get_CAM_entry failed \n",__FUNCTION__);
			return MEA_ERROR;
		}
		if ( found )
		{
			 if ( mea_drv_delete_CAM_entry (entry_id_i) != MEA_OK )
			 {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_delete_CAM_entry failed \n",__FUNCTION__);
				return MEA_ERROR;
			}

		}
	}
		
	MEA_OS_memset(&(MEA_Mapping_Profile_Table[profile_id_i].MEA_Mapping_Table[entry_id_i]),
		          0,
				  sizeof(MEA_Mapping_Profile_Table[profile_id_i].MEA_Mapping_Table[entry_id_i]));
	MEA_OS_memset(&(entry),0,sizeof(entry));

	/* Update the HW */
	if ( mea_Mapping_UpdateHw( profile_id_i,
							   entry_id_i,
							   &entry,
							  0 ) != MEA_OK )
	{    
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - mea_Mapping_UpdateHw failed \n",__FUNCTION__);
		return MEA_ERROR;
	}                          


	MEA_Mapping_Profile_Table[profile_id_i].num_of_mapping_entries--;

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                        MEA_API_Get_Mapping_Entry                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Mapping_Entry       (MEA_Unit_t        		   unit_i,
											MEA_Uint32                 profile_id_i,   
											MEA_Uint32   	           entry_id_i,
											MEA_Mapping_dbt   		  *entry_o,
											MEA_Bool                  *found_o)
{

	MEA_API_LOG
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_Get_Mapping_Entry not support\n",
        __FUNCTION__);
    return MEA_ERROR; 

	if ( MEA_Mapping_Enable == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Mapping is disabled\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (entry_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (found_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Check for valid index parmeter */
    MEA_CHECK_MAPPING_ENTRY_INDEX_IN_RANGE(entry_id_i)
 
MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id_i)
	
	if ( MEA_Mapping_Profile_Table[profile_id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id_i);
       return MEA_ERROR;         
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    *found_o = MEA_FALSE;   
    
    if ( MEA_Mapping_Profile_Table[profile_id_i].MEA_Mapping_Table[entry_id_i].valid == MEA_TRUE )
    {         
        MEA_OS_memcpy(entry_o,
                      &(MEA_Mapping_Profile_Table[profile_id_i].MEA_Mapping_Table[entry_id_i].mapping_info ) , 
                      sizeof(*entry_o)); 
        *found_o = MEA_TRUE;
    }

    return MEA_OK;
}


MEA_Status MEA_API_GetFirst_Mapping_Entry (MEA_Unit_t       unit,
										   MEA_Uint32       profile_id_i,										   
                                           MEA_Mapping_dbt *entry_o,
										   MEA_Uint32      *entry_id_o,
										   MEA_Bool        *found_o)
{
	MEA_API_LOG
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_Get_Mapping_Entry not support\n",
        __FUNCTION__);
    return MEA_ERROR; 

	if ( MEA_Mapping_Enable == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Mapping is disabled\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (entry_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (entry_id_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (found_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id_i)
	
	if ( MEA_Mapping_Profile_Table[profile_id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id_i);
       return MEA_ERROR;         
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    *found_o = MEA_FALSE;   

	for ( *entry_id_o=0; *entry_id_o<MEA_MAPPING_MAX_ENTRIES; (*entry_id_o)++ )
	{
		if ( MEA_Mapping_Profile_Table[profile_id_i].MEA_Mapping_Table[*entry_id_o].valid )
		{
			*found_o = MEA_TRUE;   
			MEA_OS_memcpy(entry_o,
						  &(MEA_Mapping_Profile_Table[profile_id_i].MEA_Mapping_Table[*entry_id_o].mapping_info ) , 
						  sizeof(*entry_o));
			return MEA_OK;
		}
	}

	return MEA_OK;
}

MEA_Status MEA_API_GetNext_Mapping_Entry  (MEA_Unit_t     unit,
                                           MEA_Uint32     profile_id_i,
										   MEA_Uint32     entry_id_i,
										   MEA_Mapping_dbt *entry_o,
										   MEA_Uint32      *entry_id_o,
										   MEA_Bool        *found_o)
{

	MEA_API_LOG
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_GetNext_Mapping_Entry not support\n",
        __FUNCTION__);
    return MEA_ERROR; 

	if ( MEA_Mapping_Enable == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Mapping is disabled\n",
                         __FUNCTION__);
       return MEA_ERROR; 
	}

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (entry_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (entry_id_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (found_o == NULL) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    MEA_CHECK_MAPPING_ENTRY_INDEX_IN_RANGE(entry_id_i)

	MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(profile_id_i)
	
	if ( MEA_Mapping_Profile_Table[profile_id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Profile %d does not exist == NULL\n",
                             __FUNCTION__,profile_id_i);
       return MEA_ERROR;         
	}

	if ( MEA_Mapping_Profile_Table[profile_id_i].MEA_Mapping_Table[entry_id_i].valid== MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Mapping Entry %d in Profile %d does not exist == NULL\n",
                             __FUNCTION__,entry_id_i,profile_id_i);
       return MEA_ERROR;         
	}

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    *found_o = MEA_FALSE;   

	for ( *entry_id_o=entry_id_i+1; *entry_id_o<MEA_MAPPING_MAX_ENTRIES; (*entry_id_o)++ )
	{
		if ( MEA_Mapping_Profile_Table[profile_id_i].MEA_Mapping_Table[*entry_id_o].valid )
		{
			*found_o = MEA_TRUE;   
			MEA_OS_memcpy(entry_o,
						  &(MEA_Mapping_Profile_Table[profile_id_i].MEA_Mapping_Table[*entry_id_o].mapping_info ) , 
						  sizeof(*entry_o)); 
			return MEA_OK;
		}
	}

	return MEA_OK;
}



MEA_Bool MEA_API_IsMappingEntryIndexInRange(MEA_Unit_t     unit, 
                                            MEA_Uint32     index)
{
	MEA_API_LOG
       
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - MEA_API_IsMappingEntryIndexInRange not support\n",
        __FUNCTION__);
    return MEA_ERROR; 

   if(index<MEA_MAPPING_MAX_ENTRIES)
   	return MEA_TRUE;
   else
    return MEA_FALSE;	
}

MEA_Bool MEA_API_IsMappingProfileIndexInRange(MEA_Unit_t     unit, 
                                              MEA_Uint32     index)
{
	MEA_API_LOG

   if(index > 0 && index<MEA_MAPPING_PROFILE_MAX_ENTRIES)
   	return MEA_TRUE;
   else
    return MEA_FALSE;	
}


/************************************************************************/
/*  Protocol To Priority  Mapping                                        */
/************************************************************************/
#if 0
static void mea_drv_Protocol_To_Priority_Mapping_HwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Protocol_To_pri_data_dbt * entry= (MEA_Protocol_To_pri_data_dbt * )arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    count_shift=0;

    MEA_OS_insert_value(count_shift,
        2,
        entry->l2_pri_rule ,
        &val[0]);
    count_shift+=2;

    MEA_OS_insert_value(count_shift,
        2,
        entry->cos_rule,
        &val[0]);
    count_shift+=2;

    MEA_OS_insert_value(count_shift,
        3,
        entry->cls_pri,
        &val[0]);
    count_shift+=3;

    MEA_OS_insert_value(count_shift,
        2,
       entry->cls_pri_rule  ,
        &val[0]);
    count_shift+=2;

    MEA_OS_insert_value(count_shift,
        2,
        entry->IP_pri_rule,
        &val[0]);
    count_shift+=2;

    MEA_OS_insert_value(count_shift,
        1,
        entry->col,
        &val[0]);
    count_shift+=1;

    MEA_OS_insert_value(count_shift,
        3,
        entry->l2_pri,
        &val[0]);
    count_shift+=3;
    MEA_OS_insert_value(count_shift,
        3,
        entry->cos,
        &val[0]);
    count_shift+=3;




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }


   
}
#endif

static MEA_Status mea_drv_Protocol_To_Priority_Mapping_UpdateHw(MEA_Unit_t                        unit_i,
                                                           MEA_Uint16                             id_i,
                                                           MEA_Protocol_To_pri_key_dbt            *key,
                                                           MEA_Protocol_To_pri_data_dbt           *new_entry_pi,
                                                           MEA_Protocol_To_pri_data_dbt           *old_entry_pi)
{
#if 0
     ENET_ind_write_t      ind_write;
    MEA_Uint32 hw_index;

    if ((old_entry_pi) &&
        (MEA_OS_memcmp(new_entry_pi,
        old_entry_pi,
        sizeof(*new_entry_pi))== 0) ) { 
            return MEA_OK;
    }


    hw_index = 0;
    hw_index = key->L2_type;
    hw_index |= key->L3_type<<5;
    hw_index |= id_i<<10;

    /* build the ind_write value */
    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_TYP_MAPPING;
    ind_write.tableOffset   = hw_index;
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg     = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_Protocol_To_Priority_Mapping_HwEntry;
    ind_write.funcParam1    =   unit_i;
    ind_write.funcParam4    = (MEA_Uint32)new_entry_pi;
#endif    

    return MEA_OK;
}


MEA_Status mea_drv_Protocol_To_Priority_Mapping_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;
    MEA_Uint16 id;

    if(b_bist_test){
       return MEA_OK;
    }
    
    /* Check if support */
    if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
        return MEA_OK;   
    }
    if(MEA_Protocol_To_Priority_Mapping_Table != NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- mea_drv_Protocol_To_Priority_Mapping_Init already Init\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF Protocol To Priority Mapping ..Init.\n");

    size = MEA_NUM_OF_PROFILE_PROTOCOL_TO_PRI * sizeof(MEA_Protocol_To_pri_data_Entry_dbt);
    if (size != 0) {
        MEA_Protocol_To_Priority_Mapping_Table = (MEA_Protocol_To_pri_data_Entry_dbt*)MEA_OS_malloc(size);
        if (MEA_Protocol_To_Priority_Mapping_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_Protocol_To_Priority_Mapping_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_Protocol_To_Priority_Mapping_Table[0]),0,size);
    }
    id=MEA_PLAT_GENERATE_NEW_ID;
    if(MEA_API_Protocol_To_Priority_Mapping_Create(unit_i, &id)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_API_Protocol_To_Priority_Mapping_Create failed \n",__FUNCTION__);
        return MEA_ERROR;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Protocol To Priority Mapping Create %d \n",id);

    id=MEA_PLAT_GENERATE_NEW_ID;
    if(MEA_API_Protocol_To_Priority_Mapping_Create(unit_i, &id)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Allocate MEA_API_Protocol_To_Priority_Mapping_Create failed \n",__FUNCTION__);
        return MEA_ERROR;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Protocol To Priority Mapping Create %d \n",id);

    return MEA_OK;
}
MEA_Status mea_drv_Protocol_To_Priority_Mapping_RInit(MEA_Unit_t       unit_i)
{
    MEA_Uint16                             id_i,index;
    
    MEA_Protocol_To_pri_key_dbt            key;
    MEA_Protocol_To_pri_data_dbt           data;
    
    if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
        return MEA_OK;   
    }

    for(id_i=0;id_i< MEA_NUM_OF_PROFILE_PROTOCOL_TO_PRI; id_i++){
        if(MEA_Protocol_To_Priority_Mapping_Table[id_i].valid !=MEA_TRUE){
            continue;
        }

        for(index=0;index< MEA_NUM_OF_ENTRY_PROTOCOL_TO_PRI;index++){
           
        MEA_OS_memcpy(&data ,&MEA_Protocol_To_Priority_Mapping_Table[id_i].data[index],sizeof(data));
         key.L2_type= ((index>>0) & 0x1f);
         key.L3_type= ((index>>5) & 0x1f);
        if( mea_drv_Protocol_To_Priority_Mapping_UpdateHw(unit_i,
            id_i,
            &key,
            &data,
            NULL)!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Protocol_To_Priority_Mapping__UpdateHw failed for id %d index %d\n",__FUNCTION__,id_i,index);
                
                return MEA_ERROR;
            }
        }
    }




    return MEA_OK;
}

MEA_Status mea_drv_Protocol_To_Priority_Mapping_Conclude(MEA_Unit_t       unit_i)
{

    if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
        return MEA_OK;   
    }

    /* Free the table */
    if (MEA_Protocol_To_Priority_Mapping_Table != NULL) {
        MEA_OS_free(MEA_Protocol_To_Priority_Mapping_Table);
        MEA_Protocol_To_Priority_Mapping_Table = NULL;
    }

    return MEA_OK;
}




static MEA_Bool mea_drv_Protocol_To_Priority_Mapping_IsRange(MEA_Unit_t     unit_i,
                                                             MEA_Uint16 id_i)
{

    if ((id_i >=MEA_NUM_OF_PROFILE_PROTOCOL_TO_PRI)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out of range  id = %d\n",__FUNCTION__,id_i); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

static MEA_Status mea_drv_Protocol_To_Priority_Mapping_IsExist(MEA_Unit_t     unit_i,
                                                               MEA_Uint16 id_i,
                                                               MEA_Bool *exist)
{

    if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Protocol_To_Priority_Mapping not support \n",__FUNCTION__); 
        return MEA_ERROR;   
    }
    
    if(exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  exist = NULL \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    *exist=MEA_FALSE;

    if(mea_drv_Protocol_To_Priority_Mapping_IsRange(unit_i,id_i)!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed  for id=%d\n",__FUNCTION__,id_i); 
        return MEA_ERROR;
    }

    if( MEA_Protocol_To_Priority_Mapping_Table[id_i].valid){
        *exist=MEA_TRUE;
        return MEA_OK;
    }

    return MEA_OK;
}

static MEA_Bool mea_drv_Protocol_To_Priority_Mapping_find_free(MEA_Unit_t       unit_i, 
                                                               MEA_Uint16       *id_io)
{
    MEA_Uint16 i;


    for(i=0;i< MEA_NUM_OF_PROFILE_PROTOCOL_TO_PRI; i++) {
        if (MEA_Protocol_To_Priority_Mapping_Table[i].valid == MEA_FALSE){
            *id_io=i; 
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;

}


static MEA_Status mea_drv_Protocol_To_Priority_Mapping_Check_Parameter(MEA_Unit_t                unit_i,
                                                                       MEA_Protocol_To_pri_key_dbt   *key,
                                                                       MEA_Protocol_To_pri_data_dbt  *data)
{
    
    
    if(data->valid == MEA_FALSE){
        return MEA_OK; // no need to check
    }
    
    if((key->L3_type == MEA_PARSING_L3_KEY_1588_event_message_over_ethernet) ||
       (key->L3_type == MEA_PARSING_L3_KEY_Default_ethernet) ||
       (key->L3_type == MEA_PARSING_L3_KEY_Pseudowire) ||
       (key->L3_type == MEA_PARSING_L3_KEY_ACH) ||
       (key->L3_type == MEA_PARSING_L3_KEY_Default_MPLS) ){
           if(data->IP_pri_rule != 0){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
          "%s - L3_type %d  not allow to configure IP\n",
          __FUNCTION__,key->L3_type);
            return MEA_ERROR;
           }
    }
    switch (key->L2_type)
    {
    case MEA_PARSING_L2_KEY_Untagged:
    case MEA_PARSING_L2_KEY_MPLS_Single_label_no_tag:
    case MEA_PARSING_L2_KEY_MPLS_Two_label_no_tag:
    case MEA_PARSING_L2_KEY_MPLS_Three_label_no_tag:
    case MEA_PARSING_L2_KEY_MPLS_Four_labelno_tag:
    case MEA_PARSING_L2_KEY_MPLS_N_labels_no_tag:

        if( (data->cos_rule == MEA_PROTOCOL_LOOK_INNER_TYP)    || 
            (data->l2_pri_rule == MEA_PROTOCOL_LOOK_INNER_TYP) ||
            (data->cls_pri_rule == MEA_PROTOCOL_LOOK_INNER_TYP))
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - L2_type %d  not allow to configure INNER_TYP\n",
                __FUNCTION__,key->L2_type);
            return MEA_ERROR;

        }
        if( (data->cos_rule == MEA_PROTOCOL_LOOK_OUTER_TYP)    || 
            (data->l2_pri_rule == MEA_PROTOCOL_LOOK_OUTER_TYP) ||
            (data->cls_pri_rule == MEA_PROTOCOL_LOOK_OUTER_TYP))
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - L2_type %d  not allow to configure OUTER_TYP\n",
                __FUNCTION__,key->L2_type);
            return MEA_ERROR;

        }
        break;
        case MEA_PARSING_L2_KEY_Ctag:
        case MEA_PARSING_L2_KEY_Stag:
            if( (data->cos_rule == MEA_PROTOCOL_LOOK_INNER_TYP)    || 
                (data->l2_pri_rule == MEA_PROTOCOL_LOOK_INNER_TYP) ||
                (data->cls_pri_rule == MEA_PROTOCOL_LOOK_INNER_TYP))
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - L2_type %d not allow to configure INNER_TYP\n",
                    __FUNCTION__,key->L2_type);
                return MEA_ERROR;

            }
        break;
        case MEA_PARSING_L2_KEY_TDM:
             if(data->valid !=0){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                  "%s - L2_type %d not allow to configure\n",
                  __FUNCTION__,key->L2_type);
              return MEA_ERROR;
              }
            break;
        case MEA_PARSING_L2_KEY_MPLS_not_over_ethernet:
        case MEA_PARSING_L2_KEY_Transparent:
            if((data->l2_pri_rule == MEA_PROTOCOL_LOOK_INNER_TYP) ||
                (data->cls_pri_rule == MEA_PROTOCOL_LOOK_INNER_TYP))
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - L2_type %d  not allow to configure INNER_TYP\n",
                    __FUNCTION__,key->L2_type);
                return MEA_ERROR;

            }
            if( (data->cos_rule == MEA_PROTOCOL_LOOK_OUTER_TYP)    || 
                (data->l2_pri_rule == MEA_PROTOCOL_LOOK_OUTER_TYP) ||
                (data->cls_pri_rule == MEA_PROTOCOL_LOOK_OUTER_TYP))
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - L2_type %d  not allow to configure OUTER_TYP\n",
                    __FUNCTION__,key->L2_type);
                return MEA_ERROR;

            }
            if(data->IP_pri_rule !=0){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - L2_type %d  not allow to configure IP\n",
                    __FUNCTION__,key->L2_type);
                return MEA_ERROR;
            }

             break;
        default:
            break;
    }
    
    
    return MEA_OK;
}




MEA_Status   mea_drv_Protocol_To_Priority_Mapping_Addowner(MEA_Unit_t     unit_i,MEA_Uint16 id_i)
{
    MEA_Bool exist;

    if(mea_drv_Protocol_To_Priority_Mapping_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Protocol_To_Priority_Mapping_IsExist failed Id_i %d \n",__FUNCTION__,id_i);
       
     return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id_i %d is not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

   MEA_Protocol_To_Priority_Mapping_Table[id_i].num_of_owners++;
    return MEA_OK;

}

MEA_Status   mea_drv_Protocol_To_Priority_Mapping_Deleteowner(MEA_Unit_t     unit_i,MEA_Uint16 id_i)
{
    MEA_Bool exist;

    if(mea_drv_Protocol_To_Priority_Mapping_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id_i %d is not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    MEA_Protocol_To_Priority_Mapping_Table[id_i].num_of_owners--;
    return MEA_OK;

}

/************************************************************************/
/*  API for Protocol_To_Priority                                        */
/************************************************************************/

MEA_Status MEA_API_Protocol_To_Priority_Mapping_IsExist(MEA_Unit_t     unit_i,
                                                        MEA_Uint16 id_i,
                                                        MEA_Bool *exist)
{
    return mea_drv_Protocol_To_Priority_Mapping_IsExist (unit_i,id_i,exist);
}

MEA_Status MEA_API_Protocol_To_Priority_Mapping_Create(MEA_Unit_t                unit_i,
                                                       MEA_Uint16                  *id_io)
{
    MEA_Bool exist;
    MEA_Protocol_To_pri_key_dbt   key;
    MEA_Protocol_To_pri_data_dbt  data;
    MEA_Uint32 L2_type;
    MEA_Uint32 L3_type;
    MEA_Uint32 index;

    /* Check if support */
    if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Protocol to pri mapping not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    if(id_io == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id_io = NULL \n",
            __FUNCTION__);
        return MEA_ERROR;  

    }

    
    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID){
        /*check if the id exist*/
        if (mea_drv_Protocol_To_Priority_Mapping_IsExist(unit_i,*id_io,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Protocol_To_Priority_Mapping_IsExist failed (*id_io=%d\n",
                __FUNCTION__,*id_io);
            return MEA_ERROR;
        }
        if (exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - *Id_io %d is already exist\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
        }

    }else{
        /*find new place*/
        if (mea_drv_Protocol_To_Priority_Mapping_find_free(unit_i,id_io)!=MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- mea_drv_Protocol_To_Priority_Mapping_find_free failed\n",__FUNCTION__);
            return MEA_ERROR;
        }

    }

    //update hw write
    for (L3_type=0;L3_type<MEA_PARSING_L3_KEY_LAST;L3_type++)
    {
        key.L3_type=L3_type;
        for (L2_type=0;L2_type<MEA_PARSING_L2_KEY_LAST ; L2_type++)
        {
        key.L2_type=L2_type;
        MEA_OS_memset(&data,0,sizeof(data));
        
        switch (key.L2_type)
        {
        case  MEA_PARSING_L2_KEY_RESERV13:
        case  MEA_PARSING_L2_KEY_RESERV14:
        case  MEA_PARSING_L2_KEY_RESERV15:
       
        case  MEA_PARSING_L2_KEY_DSA_TAG:
        case MEA_PARSING_L2_KEY_TDM:
              data.valid = MEA_FALSE;
            break;
        case MEA_PARSING_L2_KEY_Untagged:
        case MEA_PARSING_L2_KEY_MPLS_Single_label_no_tag:
        case MEA_PARSING_L2_KEY_MPLS_Two_label_no_tag:
        case MEA_PARSING_L2_KEY_MPLS_Three_label_no_tag:
        case MEA_PARSING_L2_KEY_MPLS_Four_labelno_tag:
        case MEA_PARSING_L2_KEY_MPLS_N_labels_no_tag:
            data.valid = MEA_TRUE;
            
            data.l2_pri_rule  = MEA_PROTOCOL_LOOK_DEFULT_TYP; 
            data.cos_rule     = MEA_PROTOCOL_LOOK_DEFULT_TYP;
            data.cls_pri      = 0;
            data.cls_pri_rule = MEA_PROTOCOL_LOOK_DEFULT_TYP;
            data.IP_pri_rule     = 0; //non IP
            data.col          = MEA_FALSE;// green
            data.l2_pri       =  0;
            data.cos          =  0;
            break;
        case MEA_PARSING_L2_KEY_MPLS_not_over_ethernet:
        case MEA_PARSING_L2_KEY_Transparent:
            data.valid = MEA_TRUE;
            data.l2_pri_rule  = MEA_PROTOCOL_LOOK_DEFULT_TYP; 
            data.cos_rule     = MEA_PROTOCOL_LOOK_DEFULT_TYP;
            data.cls_pri      = 0;
            data.cls_pri_rule = MEA_PROTOCOL_LOOK_DEFULT_TYP;
            data.IP_pri_rule     = 0; //non IP
            data.col          = MEA_FALSE;// green
            data.l2_pri       =  0;
            data.cos          =  0;
            break;
       default:
                data.valid = MEA_TRUE;
                data.l2_pri_rule  = MEA_PROTOCOL_LOOK_OUTER_TYP; 
                data.cos_rule     = MEA_PROTOCOL_LOOK_OUTER_TYP;
                data.cls_pri      = 0;
                data.cls_pri_rule = MEA_PROTOCOL_LOOK_OUTER_TYP;
                data.IP_pri_rule     = 0; //non IP
                data.col          = MEA_FALSE;// green
                data.l2_pri       =  0;
                data.cos          =  0;
                break;
        }
        
        
       
      
        
        if(key.L3_type == 11){
        index=0;
        }

        if(mea_drv_Protocol_To_Priority_Mapping_Check_Parameter(unit_i,&key,&data)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Protocol_To_Priority_Mapping_Check_Parameter failed\n",__FUNCTION__);
            return MEA_ERROR;
        }
            
        index  = 0;
        index  = L2_type;
        index |= L3_type<<5;

        MEA_OS_memcpy(&MEA_Protocol_To_Priority_Mapping_Table[*id_io].data[index],&data,sizeof(MEA_Protocol_To_pri_data_dbt));
        // need to set the data for each protocol.TBD
            if( mea_drv_Protocol_To_Priority_Mapping_UpdateHw(unit_i,
            *id_io,
             &key,
            &data,
            NULL)!= MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Protocol_To_Priority_Mapping__UpdateHw failed for id %d\n",__FUNCTION__,*id_io);
                MEA_OS_memset(&MEA_Protocol_To_Priority_Mapping_Table[*id_io].data,0,sizeof(MEA_Protocol_To_Priority_Mapping_Table[0].data));
                return MEA_ERROR;
            }
        }
    }
    MEA_Protocol_To_Priority_Mapping_Table[*id_io].valid = MEA_TRUE;
    MEA_Protocol_To_Priority_Mapping_Table[*id_io].num_of_owners = 1;

    
    return MEA_OK;
}
MEA_Status MEA_API_Protocol_To_Priority_Mapping_Set(MEA_Unit_t                unit_i,
                                                    MEA_Uint16                 id_i,
                                                    MEA_Protocol_To_pri_key_dbt   *key,
                                                    MEA_Protocol_To_pri_data_dbt  *data)
{
    
    MEA_Bool exist;
    MEA_Uint32 index;
    MEA_Protocol_To_pri_data_dbt old_data;
    
    if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Protocol to pri mapping not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }

    if(key == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key = NULL \n",
            __FUNCTION__);
        return MEA_ERROR;  

    }
    if(data == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data = NULL \n",
            __FUNCTION__);
        return MEA_ERROR;  

    }
    
    
    
    //check if the profile
    if (mea_drv_Protocol_To_Priority_Mapping_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Protocol_To_Priority_Mapping_IsExist failed (id_i=%d\n",
            __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id_i %d is not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
    
    //check parameter for key and data
    if(mea_drv_Protocol_To_Priority_Mapping_Check_Parameter(unit_i,key,data)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Protocol_To_Priority_Mapping_Check_Parameter  failed %d\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
    //update the database
    index  = 0;
    index  = key->L2_type;
    index |= (key->L3_type<<5);
    if(index>=MEA_NUM_OF_ENTRY_PROTOCOL_TO_PRI){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - the key is out of range  failed %d\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    MEA_OS_memcpy(&old_data,&MEA_Protocol_To_Priority_Mapping_Table[id_i].data[index],sizeof(old_data));

    if( mea_drv_Protocol_To_Priority_Mapping_UpdateHw(unit_i,
        id_i,
        key,
        data,
        &old_data)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Protocol_To_Priority_Mapping_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
        
            return MEA_ERROR;
    }

    

    MEA_OS_memcpy(&MEA_Protocol_To_Priority_Mapping_Table[id_i].data[index],data,sizeof(MEA_Protocol_To_pri_data_dbt));

   

    return MEA_OK;
}



MEA_Status MEA_API_Protocol_To_Priority_Mapping_Delete(MEA_Unit_t                unit_i,
                                                       MEA_Uint16                  id_i)

{
   MEA_Bool exist;

   if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - Protocol to pri mapping not support \n",
           __FUNCTION__);
       return MEA_ERROR;   
   }

    //check if the profile
    if (mea_drv_Protocol_To_Priority_Mapping_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Protocol_To_Priority_Mapping_IsExist failed (id_i=%d\n",
            __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id_i %d is not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    // check if we can delete if num of owner is more then 1 cant delete because other port used this profile. 
    if(MEA_Protocol_To_Priority_Mapping_Table[id_i].num_of_owners > 1){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - \n Can't delete id %d because some ports used this profile mapping\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    MEA_OS_memset(&MEA_Protocol_To_Priority_Mapping_Table[id_i],0,sizeof(MEA_Protocol_To_Priority_Mapping_Table[0]));
    //no need to set the HW because in the create we set default setting.

    MEA_Protocol_To_Priority_Mapping_Table[id_i].valid = MEA_FALSE;
    MEA_Protocol_To_Priority_Mapping_Table[id_i].num_of_owners = 0;

    return MEA_OK;
}








MEA_Status MEA_API_Protocol_To_Priority_Mapping_GetbyKey(MEA_Unit_t                unit_i,
                                                         MEA_Uint16                  id_i,
                                                         MEA_Protocol_To_pri_key_dbt   *key,
                                                         MEA_Protocol_To_pri_data_dbt  *data_o)
{

    MEA_Bool exist;
    MEA_Uint32 index;

    if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Protocol to pri mapping not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }

    //check if the profile
    if (mea_drv_Protocol_To_Priority_Mapping_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Protocol_To_Priority_Mapping_IsExist failed (id_i=%d\n",
            __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id_i %d is not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    if(key == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key = NULL\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    if(data_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data_o = NULL\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

   index=key->L2_type;
   index |= key->L3_type<<5;
   if(index>= MEA_NUM_OF_ENTRY_PROTOCOL_TO_PRI){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - the key is out of range L3_type %d L2_type\n",__FUNCTION__,key->L3_type,key->L2_type);
       return MEA_ERROR;
   }

   MEA_OS_memcpy(data_o ,&MEA_Protocol_To_Priority_Mapping_Table[id_i].data[index],sizeof(*data_o));

    
    
    return MEA_OK;
}

MEA_Status MEA_API_Protocol_To_Priority_Mapping_GetFirst_byKey (MEA_Unit_t                     unit_i,
                                                                MEA_Uint16                     id_i,                                           
                                                                MEA_Protocol_To_pri_key_dbt   *key_o,
                                                                MEA_Protocol_To_pri_data_dbt  *data_o,
                                                                MEA_Bool        *found_o)
{
    
   MEA_Uint16 index;
   MEA_Bool exist;

   if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - Protocol to pri mapping not support \n",
           __FUNCTION__);
       return MEA_ERROR;   
   }

   if (key_o == NULL){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - key_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
   }
   if (found_o == NULL){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - found_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
   }
   *found_o=MEA_FALSE;
   //check if the profile
   if (mea_drv_Protocol_To_Priority_Mapping_IsExist(unit_i,id_i,&exist)!=MEA_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - mea_drv_Protocol_To_Priority_Mapping_IsExist failed (id_i=%d\n",
           __FUNCTION__,id_i);
        
       return MEA_ERROR;
   }
   if (!exist){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - Id_i %d is not exist\n",__FUNCTION__,id_i);
       
       return MEA_ERROR;
   }

  


   index=0;
   key_o->L2_type=0;
   key_o->L3_type=0;
   key_o->pad=0;
   for(index=0;index< MEA_NUM_OF_ENTRY_PROTOCOL_TO_PRI;index++){
         *found_o=MEA_TRUE;
         key_o->L2_type = index & 0x1f;
         key_o->L3_type= (index>>5 )& 0x1f;
         break;
   }
   if(index >= MEA_NUM_OF_ENTRY_PROTOCOL_TO_PRI)
        *found_o=MEA_FALSE;

    if(data_o!=NULL)
        MEA_OS_memcpy(data_o ,&MEA_Protocol_To_Priority_Mapping_Table[id_i].data[index],sizeof(*data_o));
   
    

    return MEA_OK;
}

MEA_Status MEA_API_Protocol_To_Priority_Mapping_GetNext_byKey (MEA_Unit_t                     unit_i,
                                                                MEA_Uint16                     id_i,
                                                                MEA_Protocol_To_pri_key_dbt   *key_o,
                                                                MEA_Protocol_To_pri_data_dbt  *data_o,
                                                                MEA_Bool        *found_o)
{

    MEA_Uint32 index;
    MEA_Bool exist;


    if (!MEA_NEW_PROTOCOL_TO_PTI_MAPPING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Protocol to pri mapping not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }

    if (key_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - key_o == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
    if (found_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - found_o == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
    *found_o=MEA_FALSE;
    //check if the profile
    if (mea_drv_Protocol_To_Priority_Mapping_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Protocol_To_Priority_Mapping_IsExist failed (id_i=%d\n",
            __FUNCTION__,id_i);
        
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id_i %d is not exist\n",__FUNCTION__,id_i);
     
        return MEA_ERROR;
    }




    
    index=key_o->L2_type;
    index |=key_o->L3_type<<5;
    index++;
    if(index >= MEA_NUM_OF_ENTRY_PROTOCOL_TO_PRI)
    {        
        *found_o=MEA_FALSE;
        return MEA_OK;
    }
            *found_o=MEA_TRUE;
            key_o->L2_type = index & 0x1f;
            key_o->L3_type= (index>>5 )& 0x1f;
            key_o->pad =0;

    if(data_o!=NULL)
        MEA_OS_memcpy(data_o ,&MEA_Protocol_To_Priority_Mapping_Table[id_i].data[index],sizeof(*data_o));

    *found_o=MEA_TRUE;



    return MEA_OK;
}

