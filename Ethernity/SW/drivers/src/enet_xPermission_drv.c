/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/******************************************************************************
* 	Module Name:		 enet_xpermission_drv.c	   									     
*																					 
*   Module Description:	   
*                          
*                       
*	                    
*                       
*                       
*																					 
*  Date of Creation:	 29/10/06													 
*																					 
*  Autor Name:			 lior Levinsky.													 
*******************************************************************************/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "ENET_platform_types.h"
#include "enet_xPermission_drv.h"
#include "enet_group_drv.h"
#include "mea_device_regs.h"
#include "mea_if_regs.h"
#include "enet_queue_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

#define ENET_XPERMISSION_NUM_OF_STATIC_ENTRY  (ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT)

#define ENET_XPERMISSION_HASH_TABLE_SIZE     256

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/

struct enet_xPermission_freeList_s;
typedef struct enet_xPermission_freeList_s {
	ENET_xPermissionId_t xPermissionId;
	struct enet_xPermission_freeList_s *next;
} enet_xPermission_freeList_dbt;
struct enet_xPermission_hash_s;
typedef struct enet_xPermission_hash_s {
	ENET_xPermissionId_t xPermissionId;
	struct enet_xPermission_hash_s *next;
} enet_xPermission_hash_dbt;

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

static enet_xPermission_opposite_dbt *enet_xPermission_opposite_Table;
static enet_xPermission_dbp *enet_xPermission_Table;
static enet_xPermission_freeList_dbt *enet_xPermission_freeList_head = NULL;
static enet_xPermission_hash_dbt     *enet_xPermission_hash[ENET_XPERMISSION_HASH_TABLE_SIZE];
static ENET_Bool      enet_xPermission_InitDone[ENET_PLAT_MAX_UNIT_ID+1];

/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

ENET_Status enet_Get_xPermission_globalMemoryMode(ENET_Unit_t           unit_i,
                                                  ENET_xPermission_dbt *entry_i,
                                                  mea_memory_mode_e    *globalMemoryMode_o)
{
    ENET_QueueId_t queue_id;
    ENET_Queue_dbt queue_entry;

    if (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) {
        *globalMemoryMode_o = globalMemoryMode;
        return ENET_OK;
    }

    *globalMemoryMode_o = MEA_MODE_LAST;

    for (queue_id=0;queue_id<(sizeof(*entry_i)*8);queue_id++) {
        if (MEA_IS_SET_OUTPORT(entry_i,queue_id)) {
            if (ENET_Get_Queue(unit_i,
                               queue_id,
                               &queue_entry) != ENET_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - ENET_Get_Queue failed (queueId=%d)\n",
                                  __FUNCTION__,
                                  queue_id);
                return MEA_ERROR;
            }

            switch(queue_entry.port.id.port) {
            case 118:
            case  24:
                if ((*globalMemoryMode_o != MEA_MODE_LAST         ) &&
                    (*globalMemoryMode_o != MEA_MODE_UPSTREEM_ONLY)  ) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - We have out clusters not in same direction \n",
                                      __FUNCTION__);
                    return MEA_ERROR;
                }
                *globalMemoryMode_o = MEA_MODE_UPSTREEM_ONLY;
                break;
            case 127:
#if 0
                if ((*globalMemoryMode_o != MEA_MODE_LAST) &&
                    (*globalMemoryMode_o != MEA_MODE_REGULAR_ENET4000)) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - We have out clusters not in same direction \n",
                                      __FUNCTION__);
                    return MEA_ERROR;
                }
                *globalMemoryMode_o = MEA_MODE_REGULAR_ENET4000;
#endif
                break;

            default:
                if ((*globalMemoryMode_o != MEA_MODE_LAST) &&
                    (*globalMemoryMode_o != MEA_MODE_DOWNSTREEM_ONLY)) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - We have out clusters not in same direction \n",
                                      __FUNCTION__);
                    return MEA_ERROR;
                }
                *globalMemoryMode_o = MEA_MODE_DOWNSTREEM_ONLY;
                break;
            }
        }
    }
        
    if (*globalMemoryMode_o == MEA_MODE_LAST) {
        *globalMemoryMode_o = MEA_MODE_REGULAR_ENET4000;
    }

    return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_xPermission_OutPorts_ByPortState>                              */
/*                                                                            */
/*----------------------------------------------------------------------------*/
MEA_Status enet_xPermission_OutPorts_ByPortState(MEA_Unit_t   unit_i,
												 MEA_Bool                isCpu_i,
                                                 MEA_OutPorts_Entry_dbt* outPorts_i,
                                                 MEA_OutPorts_Entry_dbt* outPorts_o) 
{

    MEA_Uint32* ptr;
    ENET_QueueId_t queueId;
    MEA_Uint32 i,j,mask;
    
    MEA_PortState_te state;

    MEA_OS_memcpy(outPorts_o,outPorts_i,sizeof(*outPorts_o));
	if (isCpu_i) {
		return MEA_OK;
	}
    
    for (i=0,queueId=0,ptr=&(outPorts_o->out_ports_0_31);
        i<sizeof(*outPorts_o)/sizeof(MEA_Uint32);
        i++,ptr++) {
        
        for (j=0,mask=0x00000001;j<32;j++,mask <<= 1,queueId++) {
                if (*ptr & mask) {
                    state = ENET_XPERMISSION_OPPOSITE_TABLE(queueId)->portState;
                    if (state != MEA_PORT_STATE_FORWARD) {
                        *ptr &= ~mask;
                    }
                }
        }
    }

    return MEA_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_UpdateDevice_xPermission>                              */
/*                                                                            */
/*----------------------------------------------------------------------------*/
void enet_UpdateDeviceEntry_xPermission (ENET_xPermission_dbt *entry_i)
{

	ENET_QueueId_t queue,internalQ;
	MEA_OutPorts_Entry_dbt internalQueueDB;
	MEA_Uint32 *pPortGrp; 
	MEA_Uint32  shiftWord = 	0x00000001;  
	ENET_Uint32* ptr;
	ENET_Uint32  i,mask,numOfQueue;
    ENET_Uint32  groupId = 0;
	//ENET_Uint32 j;
	MEA_OS_memset(&internalQueueDB,0,sizeof(internalQueueDB));
	/* clac num of ports and convert external queue to internal queue */
	for (i=0,queue=0,ptr=&(entry_i->out_ports_0_31),numOfQueue=0;
		 i<sizeof(*entry_i)/sizeof(*ptr);
		 i++,ptr++) 
	{
#if 0
		 for (j=0,mask=0x00000001;
			  j<32;
			  j++,queue++,mask<<=1) 
#else
		for (mask = 0x00000001; (mask & 0xffffffff) != 0; mask <<= 1, queue++)
#endif
		 {
			 if ((*ptr) & mask) {
				 numOfQueue++;
				 internalQ =enet_cluster_external2internal(queue);
#ifdef MEA_SUPPORT_256_CLUSTERS
                 
                 groupId = ENET_CLUSTER_INTERNAL_2_GROUP_ID(internalQ);
                 internalQ %= ENET_QUEUE_NUMBER_OF_INTERNAL_QUEUES_PER_GROUP;
#endif
				 pPortGrp=(MEA_Uint32 *)(&(internalQueueDB.out_ports_0_31));
				 *(pPortGrp+(internalQ/32))|= (unsigned long)(shiftWord<<(internalQ%32));
			 }
		 }
	}


    if(MEA_DRV_XPER_NUM_CLUSTER_GROUP == 0){
	internalQueueDB.out_ports_64_95  = groupId;


	ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0),internalQueueDB.out_ports_0_31,ENET_MODULE_IF);
	ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(1),internalQueueDB.out_ports_32_63,ENET_MODULE_IF);
    ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(2),internalQueueDB.out_ports_64_95,ENET_MODULE_IF);
    }
    if(MEA_DRV_XPER_NUM_CLUSTER_GROUP == 1){
#ifdef MEA_OUT_PORT_128_255
        internalQueueDB.out_ports_128_159  = groupId;
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0),internalQueueDB.out_ports_0_31,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(1),internalQueueDB.out_ports_32_63,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(2),internalQueueDB.out_ports_64_95,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(3),internalQueueDB.out_ports_96_127,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(4),internalQueueDB.out_ports_128_159,ENET_MODULE_IF);
#endif
    }
    if(MEA_DRV_XPER_NUM_CLUSTER_GROUP == 2){
#ifdef MEA_OUT_PORT_128_255        
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0),internalQueueDB.out_ports_0_31,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(1),internalQueueDB.out_ports_32_63,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(2),internalQueueDB.out_ports_64_95,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(3),internalQueueDB.out_ports_96_127,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(4),internalQueueDB.out_ports_128_159,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(5),internalQueueDB.out_ports_160_191,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(6),internalQueueDB.out_ports_192_223,ENET_MODULE_IF);
        ENET_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(7),internalQueueDB.out_ports_224_255,ENET_MODULE_IF);
#endif
    }


}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_UpdateDevice_xPermission>                              */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_UpdateDevice_xPermission (ENET_Unit_t               unit_i,
                                           ENET_xPermissionId_t      id_i,
										   ENET_Bool                 isCpu_i,
                                           ENET_xPermission_dbt     *new_entry_pi,
                                           ENET_xPermission_dbt     *old_entry_pi) 
{

	ENET_ind_write_t ind_write;
    mea_memory_mode_e originalMode;

	ENET_xPermission_dbt     new_entry;

	if (enet_xPermission_OutPorts_ByPortState(unit_i,
		                                      isCpu_i,
		                                      new_entry_pi,
											  &new_entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - enet_xPermission_OutPorts_ByPortState failed \n",
                           __FUNCTION__);
		return ENET_ERROR;
	}


      /* build the ind_write value */
	ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION;
	ind_write.tableOffset	= id_i;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)enet_UpdateDeviceEntry_xPermission;
    ind_write.funcParam1 = (MEA_funcParam)&new_entry;

	
    /* Write to the Indirect Table */
	if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - ENET_WriteIndirect tbl=%d mdl=%d failed \n",
                           __FUNCTION__,ind_write.tableType,ENET_MODULE_IF);
		return ENET_ERROR;
    }
	
    originalMode =globalMemoryMode; 
    if (globalMemoryMode == MEA_MODE_DOWNSTREEM_ONLY ){
        if (MEA_API_Get_IsPortValid(24, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE/*silent*/)==MEA_TRUE) {
            ENET_xPermission_dbt     entry;
    
            MEA_OS_memset(&entry,0,sizeof(entry));
            globalMemoryMode=MEA_MODE_UPSTREEM_ONLY;

            entry.out_ports_0_31= 1<<24; /*set cluster 24 on*/
            ind_write.writeEntry    = (MEA_FUNCPTR)enet_UpdateDeviceEntry_xPermission;
            ind_write.funcParam1 = (MEA_funcParam)&entry;

            /* Write to the Indirect Table */
	        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                   "%s - ENET_WriteIndirect tbl=%d mdl=%d failed \n",
                                   __FUNCTION__,ind_write.tableType,ENET_MODULE_IF);
		           globalMemoryMode = originalMode;

                return ENET_ERROR;
            }
        }    
    
    
    }
     globalMemoryMode = originalMode;
    


	return ENET_OK;

}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_DeleteDevice_xPermission>                              */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_DeleteDevice_xPermission (ENET_Unit_t       unit_i,
                                           ENET_xPermissionId_t     id_i)
{

	ENET_xPermission_dbt entry;

	MEA_OS_memset(&entry,0,sizeof(entry));

	if (enet_UpdateDevice_xPermission (unit_i,id_i,ENET_TRUE,&entry,NULL) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - enet_UpdateDevice_xPermission failed (id=%d)\n",
			__FUNCTION__,id_i);
		return ENET_ERROR;
	}
	
	/* Return to caller */
	return ENET_OK;

}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_CheckDevice_xPermission>                               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_CheckDevice_xPermission (ENET_Unit_t           unit_i,
                                          ENET_xPermissionId_t  id_i,
                                          ENET_xPermission_dbt *entry_i) 
{

    

    if (id_i >=MEA_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(unit_i) ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		   "%s - No more space for Xpermission in the device (id_i=%d)\n",
		   __FUNCTION__,id_i);
	   return ENET_ERROR;
	}

    
	/* Return to caller */
	return ENET_OK;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Check_xPermission>                                     */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Check_xPermission (ENET_Unit_t               unit_i,
                                    ENET_xPermissionId_t     id_i,
                                    ENET_xPermission_dbt    *entry_i) 
{

#if 0
    ENET_PortId_t PortId;
	ENET_Uint32*   ptr;
	ENET_Uint32    i,j,mask;
#endif


	if (id_i != ENET_PLAT_GENERATE_NEW_ID) {
		if (id_i >=MEA_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(unit_i)) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - id_i (%d) >= max value (%d) \n",
					          __FUNCTION__,id_i,ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(MEA_UNIT_0));
			return MEA_ERROR;
		}
	}

#if 0 // Done in create/set to get globalMemoryMode

	for (i=0,PortId=0,ptr=&(entry_i->out_ports_0_31);
		 i<sizeof(*entry_i)/sizeof(*ptr);
		 i++,ptr++) 
	{
		if (*ptr == 0) {
			PortId+= 32;
			continue;
		}
		 for (j=0,mask=0x00000001;
			  j<32;
			  j++,PortId++,mask<<=1) 
		 {
			 if ((*ptr) & mask) {
                 ENET_ASSERT_QUEUE_VALID(unit_i,PortId);
			 }
		 }
	}
#endif 



	/* Return to caller */
	return ENET_OK;
}



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_IsAllowedToDelete_xPermission>                         */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_IsAllowedToDelete_xPermission (ENET_Unit_t           unit_i,
                                                ENET_xPermissionId_t id_i)
{

	
	
    
    /* Return to caller */
	return ENET_OK;
}

ENET_Uint32 enet_hash_xPermission (ENET_xPermission_dbt     *entry_i) 
{
    ENET_Uint32  result;
	ENET_Uint32 *ptr;
	ENET_Uint32  i;

	for (i=0,result=0,ptr=&(entry_i->out_ports_0_31);
		 i<sizeof(*entry_i)/sizeof(*ptr);
		 i++,ptr++) 
	{
		result ^= (((*ptr) & 0x000000ff) >> 0);
		result ^= (((*ptr) & 0x0000ff00) >> 8);
		result ^= (((*ptr) & 0x00ff0000) >>16);
		result ^= (((*ptr) & 0xff000000) >>24);
	}
	result %= ENET_XPERMISSION_HASH_TABLE_SIZE;
	return result;
}
/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Init_xPermission>                                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Init_xPermission(ENET_Unit_t unit_i)
{

	ENET_xPermissionId_t xPermissionId;
	ENET_PortId_t PortId;
    ENET_Uint32 size;
     
    static ENET_Bool first = ENET_TRUE;
    if(b_bist_test){
        return ENET_OK;  
    }
    /* Init xPermition object */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize xPermission...\n");
    if (first) {
       ENET_Uint32 i;
       for (i=0;i<ENET_NUM_OF_ELEMENTS(enet_xPermission_InitDone);i++) {
          enet_xPermission_InitDone[unit_i] = ENET_FALSE;
       } 
       first = ENET_FALSE;
    }

    if (enet_xPermission_InitDone[unit_i] != ENET_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - unit %d is already init\n",
                          __FUNCTION__,unit_i);
       return ENET_ERROR;
    }

	enet_xPermission_InitDone[unit_i] = ENET_FALSE;
     
 
	/* Init Opposite table */
    size = (sizeof(enet_xPermission_opposite_dbt)-sizeof(MEA_Uint32));
    size += (ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(unit_i)/32)*sizeof(MEA_Uint32); //
    size *= (sizeof(ENET_xPermission_dbt)*8);
   
    //size = (sizeof(enet_xPermission_opposite_dbt));
    //size *=ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT;
    if (size != 0) {
        enet_xPermission_opposite_Table = (enet_xPermission_opposite_dbt*)MEA_OS_malloc(size);
        if (enet_xPermission_opposite_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate enet_xPermission_opposite_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(enet_xPermission_opposite_Table,0,size);
    }

    /* Allocate enet_xPermission_Table */
    size = (ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(unit_i))* sizeof(enet_xPermission_dbp);
     if (size != 0) {
        enet_xPermission_Table = (enet_xPermission_dbp*)MEA_OS_malloc(size);
        if (enet_xPermission_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate enet_xPermission_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(enet_xPermission_Table,0,size);
    }



    /* Create the xPermission group  */
	
	MEA_OS_memset(enet_xPermission_hash,0,sizeof(enet_xPermission_hash));
	enet_xPermission_freeList_head = NULL;

	/* Add to the free List all non static entries */
	for (xPermissionId=PortId=MEA_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(unit_i)-1;
		 xPermissionId>=ENET_XPERMISSION_NUM_OF_STATIC_ENTRY;
		 xPermissionId--) {
		 enet_xPermission_freeList_dbt* freeList_entry;
		 freeList_entry = (enet_xPermission_freeList_dbt*)
			 MEA_OS_malloc(sizeof(enet_xPermission_freeList_dbt));
		 if (freeList_entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				               "%s - Allocate enet xPermission freeList entry failed \n",
						  __FUNCTION__);
			 return MEA_ERROR;
		 }
		 MEA_OS_memset(freeList_entry,0,sizeof(*freeList_entry));
		 freeList_entry->xPermissionId = xPermissionId;
		 freeList_entry->next = enet_xPermission_freeList_head;
		 enet_xPermission_freeList_head = freeList_entry;
	}

    /* set default to Device */
	for (xPermissionId=0;
		 xPermissionId<MEA_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(unit_i);
		 xPermissionId++) {
    	if (enet_DeleteDevice_xPermission(unit_i,
 		                                  xPermissionId) != ENET_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - enet_DeleteDevice_xPermission failed (id=%d)\n",
							  __FUNCTION__,xPermissionId);
			return ENET_ERROR;
		}
	}


	/* create static entries */




    enet_xPermission_InitDone[unit_i] = ENET_TRUE;

    /* Return to caller */
    return ENET_OK; 

}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_ReInit_xPermission>                                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_ReInit_xPermission(ENET_Unit_t unit_i)
{

    ENET_Bool            save_InitDone;
    ENET_xPermissionId_t xPermissionId;
    mea_memory_mode_e save_globalMemoryMode;

    /* Reset InitDone to force Hardware Update */
    save_InitDone = enet_xPermission_InitDone[unit_i];
    enet_xPermission_InitDone[unit_i] = ENET_FALSE;

	for (xPermissionId=0;
         xPermissionId<ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(MEA_UNIT_0);
         xPermissionId++) {
        if (enet_xPermission_Table[xPermissionId] == NULL) {
                 continue;
        }
        /* Change globalMemoryMode */
        save_globalMemoryMode = globalMemoryMode;
        globalMemoryMode = enet_xPermission_Table[xPermissionId]->globalMemoryMode;

        if (enet_UpdateDevice_xPermission (unit_i,
                                           xPermissionId,
										   enet_xPermission_Table[xPermissionId]->isCpu,
                                           &(enet_xPermission_Table[xPermissionId]->portMap),
                                           NULL
                                          ) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_UpdateDevice_xPermission Failed (id=%d)\n",
                              __FUNCTION__,
                              xPermissionId);
            enet_xPermission_InitDone[unit_i] = save_InitDone;
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return MEA_ERROR;
        }
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }

    /* Restore InitDone  */
    enet_xPermission_InitDone[unit_i] = save_InitDone;

    /* Return to caller */
    return ENET_OK; 

}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Conclude_xPermission>                                  */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Conclude_xPermission(ENET_Unit_t unit_i)
{

	ENET_xPermissionId_t xPermission_id;
	ENET_xPermissionId_t next_xPermission_id;
	ENET_Bool            found;
	
	if (enet_xPermission_InitDone[unit_i] == ENET_FALSE) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - Conclude call without Init before \n",
						   __FUNCTION__);
       return MEA_OK;
    }

    /* delete the static entries */



	/* Get the first xPermission */
	if (enet_GetFirst_xPermission  (unit_i,
		                            &xPermission_id,
							        NULL, /* entry_o*/
							        &found) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_GetFirst_xPermission failed \n",
						   __FUNCTION__);
		return ENET_ERROR;
	}

	/* scan all xPermission and delete one by one */
	while(found) {

		next_xPermission_id = xPermission_id;
        if (enet_GetNext_xPermission   (unit_i,
		                                &next_xPermission_id,
		  					            NULL, /* entry_o */
							            &found) != ENET_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - enet_GetNext_xPermission failed \n",
		 		  		       __FUNCTION__);
		    return ENET_ERROR;
		}

        if (enet_Delete_xPermission   (unit_i,xPermission_id) != ENET_OK) { 
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		 	                   "%s - enet_Delete_xPermission failed \n",
		 		  		       __FUNCTION__);
		    return ENET_ERROR;
		}


		xPermission_id = next_xPermission_id;
	}



	while (enet_xPermission_freeList_head) {
    	enet_xPermission_freeList_dbt* tmp;
		tmp = enet_xPermission_freeList_head;
		enet_xPermission_freeList_head = tmp->next;
		MEA_OS_memset(tmp,0,sizeof(*tmp));
		MEA_OS_free(tmp);
	}
    
     /* Free the table */
    if (enet_xPermission_Table != NULL) {
        MEA_OS_free(enet_xPermission_Table);
        enet_xPermission_Table = NULL;
    }

     /* Free the opposite table */
    if (enet_xPermission_opposite_Table != NULL) {
        MEA_OS_free(enet_xPermission_opposite_Table);
        enet_xPermission_opposite_Table = NULL;
    }


	/* Indicate InitDone to false for this unit */
    enet_xPermission_InitDone[unit_i] = ENET_FALSE;

    /* Return to caller */
    return ENET_OK; 

}


/*----------------------------------------------------------------------------*/
/*             API functions implementation                                   */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Create_xPermission>                                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Create_xPermission(ENET_Unit_t  unit_i,
									ENET_xPermission_dbt *entry_i,
                                    ENET_Bool  isCpu_i,
									ENET_xPermissionId_t *id_io)

{

	ENET_xPermissionId_t id;
	ENET_Bool            found;
	enet_xPermission_dbt          *entry;
	enet_xPermission_hash_dbt     *hash_entry=NULL;
	enet_xPermission_freeList_dbt *tmp;
	ENET_Uint32                    hashId=0;
    mea_memory_mode_e save_globalMemoryMode;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);


	/* check entry_i parameter */
	ENET_ASSERT_NULL(entry_i,"entry_i");

	/* check id_io    parameter */
	ENET_ASSERT_NULL(id_io,"id_io");
	if ((*id_io) != ENET_PLAT_GENERATE_NEW_ID) {
		if (*id_io >= ENET_XPERMISSION_NUM_OF_STATIC_ENTRY) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				"%s - You are not allowed to create xPermission "
				"with specific id (%d) >= max static id (%d)\n",
				__FUNCTION__,*id_io,ENET_XPERMISSION_NUM_OF_STATIC_ENTRY);
			return MEA_ERROR;
		} 
    }


	/* check for entity attributes  */
	if (enet_Check_xPermission(unit_i,
                               (*id_io),
  				               entry_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_Check_xPermission failed (xPermission id=%d)\n",
						   __FUNCTION__,*id_io);
		return ENET_ERROR;
	} 



#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */
	
    save_globalMemoryMode = globalMemoryMode;
    if (enet_Get_xPermission_globalMemoryMode(unit_i,
                                              entry_i,
                                              &globalMemoryMode
                                              ) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_Get_xPermission_globalMemoryMode failed \n",
                          __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return ENET_ERROR;
    }

	if (*id_io != ENET_PLAT_GENERATE_NEW_ID) {
		if (enet_xPermission_Table[*id_io] != NULL) {
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);


            enet_xPermission_Table[*id_io]->numberOfOwners ++;

			return enet_Set_xPermission(unit_i,*id_io,isCpu_i,entry_i);
		}
		id = *id_io;
		found = ENET_FALSE;
	} else {
    if (enet_GetIDByEntry_xPermission (unit_i,
		                               entry_i,
									   isCpu_i,
									   &id,
									   &found) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				               "%s - enet_GetIDByEntry_xPermission failed \n",
					           __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return ENET_ERROR;																			

	}

		}
	if (found){
		enet_xPermission_Table[id]->numberOfOwners ++;
	    *id_io = id;
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		return ENET_OK;

	} 
	

	/* Get free id */
	if (*id_io == ENET_PLAT_GENERATE_NEW_ID) {
		if (enet_xPermission_freeList_head == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%d - No free xPermissionId \n",
							  __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return ENET_ERROR;																			
		}
		id = enet_xPermission_freeList_head->xPermissionId;
	} else {
		id = *id_io;
	}



	/* Allocate new entry */
	entry = (enet_xPermission_dbt*)MEA_OS_malloc(sizeof(enet_xPermission_dbt));
	if (entry == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Allocate entry failed \n",
						  __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return ENET_ERROR;																			
		}		
	MEA_OS_memset(entry,0,sizeof(*entry));
	entry->numberOfOwners = 1;
    entry->globalMemoryMode = globalMemoryMode;
	entry->isCpu = isCpu_i;
	MEA_OS_memcpy(&(entry->portMap),entry_i,sizeof(entry->portMap));
	
	/* Allocate new hash entry */
	if (*id_io == ENET_PLAT_GENERATE_NEW_ID) {
		hash_entry = (enet_xPermission_hash_dbt*)MEA_OS_malloc(sizeof(enet_xPermission_hash_dbt));
		if (hash_entry == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate hash_entry failed \n",
							  __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return ENET_ERROR;																			
		}		
		MEA_OS_memset(hash_entry,0,sizeof(*hash_entry));
		hash_entry->xPermissionId = id;
		hashId = enet_hash_xPermission(entry_i);
		hash_entry->next = enet_xPermission_hash[hashId];
	}

     {
        ENET_QueueId_t queueId;
        ENET_Uint32 mask_queueId;
        ENET_Uint32 mask_xPermission;
        ENET_Uint32* ptr_new;
        ENET_Uint32  i,j;

        enet_xPermission_opposite_dbt* opposite_entry;

        for (i=0,queueId=0,
             ptr_new=&(entry->portMap.out_ports_0_31),
             mask_xPermission= (unsigned long)((0x00000001)<<(id%32));
             i<sizeof(entry->portMap)/sizeof(MEA_Uint32);
             i++,ptr_new++) {
            for (j=0,mask_queueId=0x00000001;
                 j<32;
                 j++,mask_queueId<<=1,queueId++) {
                 if ((*ptr_new) & mask_queueId) {
                     opposite_entry = ENET_XPERMISSION_OPPOSITE_TABLE(queueId);
                     opposite_entry->xPermissions[id/32] |= mask_xPermission;
                 }
            }
        }
    }



	/* update the xPermission instance */
	if (enet_UpdateDevice_xPermission(unit_i,
									  id,
									  isCpu_i,
									  entry_i,
									  NULL) != ENET_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_UpdateDevice_xPermission failed (xPermission id=%d)\n",
						   __FUNCTION__,id);
		MEA_OS_free(entry);
	    if (*id_io == ENET_PLAT_GENERATE_NEW_ID) {
			MEA_OS_free(hash_entry);
		}
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		   return ENET_ERROR;
	} 
	if (*id_io == ENET_PLAT_GENERATE_NEW_ID) {
		tmp = enet_xPermission_freeList_head;
		enet_xPermission_freeList_head = tmp->next;
		MEA_OS_free(tmp);
	}

   


    enet_xPermission_Table[id] = entry;
    if (*id_io == ENET_PLAT_GENERATE_NEW_ID) {
		enet_xPermission_hash[hashId] = hash_entry;
	}
	*id_io = id;

	/* return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return ENET_OK;

}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Set_xPermission>                                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Set_xPermission       (ENET_Unit_t            unit_i,
									    ENET_xPermissionId_t  id_i,
                                        ENET_Bool isCpu_i,
                                        ENET_xPermission_dbt *entry_i)
                                 
{

	enet_xPermission_dbt* entry;
    mea_memory_mode_e save_globalMemoryMode;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_XPERMISSION_VALID(unit_i,id_i);

	/* check entry_i parameter */
	ENET_ASSERT_NULL(entry_i,"entry_i");

	/* check for entity attributes  */
	if (enet_Check_xPermission(unit_i,
		                        id_i,
						        entry_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_Check_xPermission failed (xPermission id=%d)\n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	} 


#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    save_globalMemoryMode = globalMemoryMode;
    if (enet_Get_xPermission_globalMemoryMode(unit_i,
                                              entry_i,
                                              &globalMemoryMode
                                              ) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - enet_Get_xPermission_globalMemoryMode failed \n",
                          __FUNCTION__);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return ENET_ERROR;
    }

	entry = enet_xPermission_Table[id_i];
	if (entry == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - xPermission Id %d not exist \n",
						   __FUNCTION__,id_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		return ENET_ERROR;
	}


	/* check that portMap not change - T.B.D handle the case of change (delete/create) */
	if ((id_i>=ENET_XPERMISSION_NUM_OF_STATIC_ENTRY)&&
		(MEA_OS_memcmp(&(entry->portMap),entry_i,sizeof(entry->portMap)) != 0) ) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s - Not allowed to change the Port Mapping of specific xpermission (id=%d)\n",
			__FUNCTION__,id_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		return ENET_OK;
	}

    /* Verify correct direction for this xPermission */
    if ((entry->globalMemoryMode != globalMemoryMode) &&
        (globalMemoryMode        != MEA_MODE_REGULAR_ENET4000)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry->globalMemoryMode (%d) != globalMemoryMode (%d) (id_i=%d) \n",
                          __FUNCTION__,
                          entry->globalMemoryMode,
                          globalMemoryMode,
                          id_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }

	/* update the device */
	if (enet_UpdateDevice_xPermission
		      (unit_i,
		       id_i,
			   isCpu_i,
			   entry_i,
               &(entry->portMap)) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_UpdateDevice_xPermission failed (xPermission id=%d)\n",
						   __FUNCTION__,id_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		return ENET_ERROR;
	} 

    {
        ENET_QueueId_t queueId;
        ENET_Uint32 mask_queueId;
        ENET_Uint32 mask_xPermission;
        MEA_Uint32* ptr_new;
        ENET_Uint32* ptr_old;
        ENET_Uint32  i,j;

        enet_xPermission_opposite_dbt* opposite_entry;

        for (i=0,queueId=0,
             ptr_new=&(entry_i->out_ports_0_31),
             ptr_old=&(entry->portMap.out_ports_0_31),
             mask_xPermission= (unsigned long)((0x00000001)<<(id_i%32));
             i<sizeof(*entry_i)/sizeof(MEA_Uint32);
             i++,ptr_new++,ptr_old++) {
            for (j=0,mask_queueId=0x00000001;
                 j<32;
                 j++,mask_queueId<<=1,queueId++) {
                 if ((*ptr_old) & mask_queueId) {
                     opposite_entry = ENET_XPERMISSION_OPPOSITE_TABLE(queueId);
                     opposite_entry->xPermissions[id_i/32] &= (~mask_xPermission);
                 }
                 if ((*ptr_new) & mask_queueId) {
                     opposite_entry = ENET_XPERMISSION_OPPOSITE_TABLE(queueId);
                     opposite_entry->xPermissions[id_i/32] |= mask_xPermission;
                 }
            }
        }
    }


	/* set entity information */
	entry->isCpu = isCpu_i;
	MEA_OS_memcpy(&(entry->portMap),entry_i,sizeof(entry->portMap));
	

	/* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return ENET_OK;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Get_xPermission>                                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Get_xPermission (ENET_Unit_t       unit_i,
                                   ENET_xPermissionId_t     id_i,
                                   ENET_xPermission_dbt     *entry_o)
{

	enet_xPermission_dbt* entry;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_XPERMISSION_VALID(unit_i,id_i);

	/* check entry_o parameter */
	ENET_ASSERT_NULL(entry_o,"entry_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

	/* Get entity information */
	entry = enet_xPermission_Table[id_i];
	if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - xPermission Id %d not exist \n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	}

	MEA_OS_memcpy(entry_o,&(entry->portMap),sizeof(*entry_o));

	/* Return to caller */
    return ENET_OK;
}                                 

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Get_xPermission>                                      */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Get_xPermission_Internal (ENET_Unit_t       unit_i,
                                           ENET_xPermissionId_t     id_i,
                                           enet_xPermission_dbt     *entry_o)
{

	enet_xPermission_dbt* entry;

#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_XPERMISSION_VALID(unit_i,id_i);

	/* check entry_o parameter */
	ENET_ASSERT_NULL(entry_o,"entry_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

	/* Get entity information */
	entry = enet_xPermission_Table[id_i];
	if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - xPermission Id %d not exist \n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	}

	MEA_OS_memcpy(entry_o,entry,sizeof(*entry_o));

	/* Return to caller */
    return ENET_OK;
}                                 



/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_Delete_xPermission>                                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_Delete_xPermission    (ENET_Unit_t          unit_i,
                                        ENET_xPermissionId_t id_i)
{

	enet_xPermission_dbt* entry;
    mea_memory_mode_e save_globalMemoryMode;


#ifdef ENET_SW_CHECK_INPUT_PARAMETERS


	/* check unit_i  parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check id_i    parameter */
	ENET_ASSERT_XPERMISSION_VALID(unit_i,id_i);


	/* check for entity attributes  */
	if (enet_IsAllowedToDelete_xPermission(unit_i,
		                                   id_i) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_IsAllowedToDelete_xPermission failed (xPermission id=%d)\n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	} 


#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */

    
	/* Get entity information */
	entry = enet_xPermission_Table[id_i];
	if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - xPermission Id %d not exist \n",
						   __FUNCTION__,id_i);
		return ENET_ERROR;
	}

    

	if (entry->numberOfOwners > 1) {
        entry->numberOfOwners--;

		return ENET_OK;
	}

    /* Change globalMemoryMode */
    save_globalMemoryMode = globalMemoryMode;
    globalMemoryMode = entry->globalMemoryMode;

	/* delete the device */
	if (enet_DeleteDevice_xPermission(unit_i,
 		                              id_i) != ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			               "%s - enet_DeleteDevice_xPermission failed (xPermission id=%d)\n",
						   __FUNCTION__,id_i);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
		return ENET_ERROR;
	} 


	if (id_i >= ENET_XPERMISSION_NUM_OF_STATIC_ENTRY) {
		ENET_Uint32 hash_id;
		enet_xPermission_hash_dbt* hash_entry , *last_hash_entry;

		hash_id = enet_hash_xPermission(&(enet_xPermission_Table[id_i]->portMap));
		hash_entry = enet_xPermission_hash[hash_id];
		last_hash_entry = NULL;
		while(hash_entry) {
			if (hash_entry->xPermissionId == id_i) {
				break;
			}
			last_hash_entry = hash_entry;
			hash_entry = hash_entry->next;
		}
		if (!hash_entry) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - xPermissionId=%d Not found in hash Table (hash_id=%d)\n",
							  __FUNCTION__,id_i,hash_id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}
		if (last_hash_entry) {
			last_hash_entry->next          = hash_entry->next;
		} else {
			enet_xPermission_hash[hash_id] = hash_entry->next;
		}
		MEA_OS_free(hash_entry);
	}

    {
        ENET_QueueId_t queueId;
        ENET_Uint32 mask_queueId;
        ENET_Uint32 mask_xPermission;
        ENET_Uint32* ptr_old;
        ENET_Uint32  i,j;

        enet_xPermission_opposite_dbt* opposite_entry;

        for (i=0,queueId=0,
             ptr_old=&(entry->portMap.out_ports_0_31),
             mask_xPermission= (unsigned long)((0x00000001)<<(id_i%32));
             i<sizeof(entry->portMap)/sizeof(MEA_Uint32);
             i++,ptr_old++) {
            for (j=0,mask_queueId=0x00000001;
                 j<32;
                 j++,mask_queueId<<=1,queueId++) {
                 if ((*ptr_old) & mask_queueId) {
                     opposite_entry = ENET_XPERMISSION_OPPOSITE_TABLE(queueId);
                     opposite_entry->xPermissions[id_i/32] &= (~mask_xPermission);
                 }
            }
        }
    }
	if (id_i >= ENET_XPERMISSION_NUM_OF_STATIC_ENTRY) {
		enet_xPermission_freeList_dbt* freeList_entry;
		freeList_entry = (enet_xPermission_freeList_dbt*)
			 MEA_OS_malloc(sizeof(enet_xPermission_freeList_dbt));
		if (freeList_entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate freeList entry failed \n",
							  __FUNCTION__);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
			return MEA_ERROR;
		}
		MEA_OS_memset(freeList_entry,0,sizeof(*freeList_entry));
		freeList_entry->xPermissionId = id_i;
		freeList_entry->next = enet_xPermission_freeList_head;
		enet_xPermission_freeList_head = freeList_entry;
	}
	MEA_OS_free(enet_xPermission_Table[id_i]);
	enet_xPermission_Table[id_i] = NULL;

	/* Return to caller */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    return ENET_OK;
}
	


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_GetFirst_xPermission>                                   */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_GetFirst_xPermission (ENET_Unit_t           unit_i,							
	               				  	   ENET_xPermissionId_t *id_o,                  		
                                       ENET_xPermission_dbt *entry_o,						
                                       ENET_Bool            *found_o)						
{

	enet_xPermission_dbt* entry;
	ENET_xPermissionId_t  id;

	for (id=0;id<ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(MEA_UNIT_0);id++) {
		entry = enet_xPermission_Table[id];
		if (entry) {
			if (id_o) {
				*id_o = id;
	}
	if (entry_o) {
				MEA_OS_memcpy(entry_o,&(entry->portMap),sizeof(*entry_o));
			}
			if (found_o) {
				*found_o = ENET_TRUE;
			}
			return ENET_OK;
		}
	}
	if (found_o) {
		*found_o = ENET_FALSE;
	}
	return ENET_OK;
}                                   														
																							
/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_GetNext_xPermission>                                   */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_GetNext_xPermission (ENET_Unit_t            unit_i,						
                                      ENET_xPermissionId_t  *id_io,						
                                      ENET_xPermission_dbt  *entry_o,					
                                      ENET_Bool             *found_o)					
{
	enet_xPermission_dbt* entry;
	ENET_xPermissionId_t  id;

	if (id_io == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - id_io == NULL\n",
						   __FUNCTION__);
		return ENET_ERROR;
	}

	for (id=(*id_io)+1;id<ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(MEA_UNIT_0);id++) {
		entry = enet_xPermission_Table[id];
		if (entry) {
			if (id_io) {
				*id_io = id;
			}
	if (entry_o) {
				MEA_OS_memcpy(entry_o,&(entry->portMap),sizeof(*entry_o));
			}
			if (found_o) {
				*found_o = ENET_TRUE;
			}
			return ENET_OK;
		}
	}
	if (found_o) {
		*found_o = ENET_FALSE;
	}
	return ENET_OK; 

}           


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_GetIDByEntry_xPermission>                              */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status enet_GetIDByEntry_xPermission (ENET_Unit_t           unit_i,
								           ENET_xPermission_dbt *entry_i,
                                           ENET_Bool             isCpu_i,
								           ENET_xPermissionId_t *id_o,
                                           ENET_Bool             *found_o)
{

	enet_xPermission_hash_dbt *hash_entry;
	enet_xPermission_dbt       *entry;
    
#ifdef ENET_SW_CHECK_INPUT_PARAMETERS

	/* check unit_i parameter */
    ENET_ASSERT_UNIT_VALID(unit_i);

	/* check name_i parameter */
	ENET_ASSERT_NULL(entry_i,"entry_i");

	/* check id_o parameter */
	ENET_ASSERT_NULL(id_o,"id_o");

	/* check found_o parameter */
	ENET_ASSERT_NULL(found_o,"found_o");

#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */



	hash_entry = enet_xPermission_hash[enet_hash_xPermission(entry_i)];
	while(hash_entry) {
		entry = enet_xPermission_Table[hash_entry->xPermissionId];
		if ((MEA_OS_memcmp(entry_i,
			              &(entry->portMap),
						  sizeof(*entry_i)) == 0) && (entry->isCpu == isCpu_i)) {
			*found_o = ENET_TRUE;
			*id_o = hash_entry->xPermissionId;
			return ENET_OK;
	   }
		hash_entry = hash_entry->next;
	   }

    *found_o = ENET_FALSE;

	/* Return to caller */
    return ENET_OK;
}     

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_IsValid_xPermission>                                   */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Bool  enet_IsValid_xPermission(ENET_Unit_t   unit_i,
 							        ENET_xPermissionId_t id_i,
							        ENET_Bool     silent_i) 
{

  
	if (id_i >=MEA_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(unit_i)) {
  	  if (!silent_i) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - id_i (%d) >= max value (%d) \n",
					          __FUNCTION__,id_i,ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(MEA_UNIT_0));
	  }
	  return ENET_FALSE;
	}

	if (enet_xPermission_Table[id_i] == NULL) {
  	  if (!silent_i) {
  		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - Unit/xPermission %d/%d is invalid\n",
							 __FUNCTION__,unit_i,id_i);
		  
      }

  	  return ENET_FALSE;  
    }
    
    return ENET_TRUE;
}


/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_IsValid_xPermission>                                   */
/*      if Cluster == UNASSIGNED_QUEUE_VAL than no queue on port              */
/*----------------------------------------------------------------------------*/
ENET_PortId_t convertEgress2IngressPort(ENET_PortId_t port)
{
	ENET_PortId_t retVal = port;
#ifdef ENET_OS_Z1 
	if ((port>=0) && (port<8))
		retVal = retVal * 4;
	if ((port>=32) && (port<40))
		retVal = (retVal -32 +8)*4;
#endif
	return (retVal);
}

void  enet_SetDefaultClusterToPort(ENET_Unit_t   unit_i,
 							            ENET_PortId_t port, /*ingress*/
										ENET_QueueId_t cluster) /* external*/ 
{

	ENET_xPermission_dbt  entry;
	ENET_xPermissionId_t  id;
    ENET_Bool             isCpu=MEA_FALSE;
	

    if ( port != MEA_CPU_PORT){
        return;
    }

    /* To avoid remove from xPermission of the cpu we ignore request like this */
    if ((port == MEA_CPU_PORT) && 
        (cluster == UNASSIGNED_QUEUE_VAL)) {
        return;
    }

id = (ENET_xPermissionId_t)port;
	
	MEA_OS_memset(&entry,0,sizeof(entry));

	if (cluster!= UNASSIGNED_QUEUE_VAL){
		((MEA_Uint32 *)(&entry.out_ports_0_31))[cluster/32] |= 
			(unsigned long)(0x00000001 << (cluster%32));
	}
    if(port == 127){
    isCpu=MEA_TRUE;
    }
	if (enet_Create_xPermission(unit_i,&entry,isCpu,&id)!=ENET_OK){
  		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                 "%s - enet_Create_xPermission %d\n",
							 __FUNCTION__,id);
		  return ;
	}

    
    return ;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_xPermission_disableCluster>                            */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status  enet_xPermission_disableCluster(ENET_Unit_t    unit_i,
                                             ENET_QueueId_t cluster) 
{

    ENET_xPermissionId_t  id;
    enet_xPermission_dbt *entry;
    ENET_xPermission_dbt  temp_portMap;
    mea_memory_mode_e save_globalMemoryMode;


    /* scan all xPermissions */
    for (id=0;id<ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(MEA_UNIT_0);id++) {

        /* get current xPermission */
        entry = enet_xPermission_Table[id];
        
        /* check that this xPermission allocate */
        if (!entry) {
            continue;
        }

        /* Check that our cluster is part of this xPermission */
        if (!MEA_IS_SET_OUTPORT(&(entry->portMap),cluster)) {
            continue;
        }

        /* Change globalMemoryMode */
        save_globalMemoryMode = globalMemoryMode;
        globalMemoryMode = entry->globalMemoryMode;

        /* Save temporary bit map */
        MEA_OS_memcpy(&temp_portMap,&(entry->portMap),sizeof(temp_portMap));

        /* Clear our cluster in this cluster */
        MEA_CLEAR_OUTPORT(&temp_portMap,cluster);
        
        /* Update the hardware without our cluster */
        if (enet_UpdateDevice_xPermission (unit_i,
                                           id,
										   entry->isCpu,
                                           &temp_portMap,
                                           NULL
                                          ) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_UpdateDevice_xPermission Failed (id=%d)\n",
                              __FUNCTION__,
                              id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return ENET_ERROR;
        }
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

    }

    /* Return to caller */
    return ENET_OK;
}

/*----------------------------------------------------------------------------*/
/*                                                                            */
/*               <enet_xPermission_reEnableCluster>                           */
/*                                                                            */
/*----------------------------------------------------------------------------*/
ENET_Status  enet_xPermission_reEnableCluster(ENET_Unit_t    unit_i,
                                              ENET_QueueId_t cluster) 
{

    ENET_xPermissionId_t  id;
    enet_xPermission_dbt* entry;
    mea_memory_mode_e save_globalMemoryMode;


    /* scan all xPermissions */
    for (id=0;id<ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(MEA_UNIT_0);id++) {

        /* get current xPermission */
        entry = enet_xPermission_Table[id];
        
        /* check that this xPermission allocate */
        if (!entry) {
            continue;
        }

        /* Check that our cluster is part of this xPermission */
        if (!MEA_IS_SET_OUTPORT(&(entry->portMap),cluster)) {
            continue;
        }

        /* Change globalMemoryMode */
        save_globalMemoryMode = globalMemoryMode;
        globalMemoryMode = entry->globalMemoryMode;
        
        /* Update the hardware with our cluster */
        if (enet_UpdateDevice_xPermission (unit_i,
                                           id,
										   entry->isCpu,
                                           &(entry->portMap),
                                           NULL
                                          ) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - enet_UpdateDevice_xPermission Failed (id=%d)\n",
                              __FUNCTION__,
                              id);
            MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
            return ENET_ERROR;
        }
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

    }

    /* Return to caller */
    return ENET_OK;
}



MEA_Status  enet_xPermission_opposite_Get(MEA_Unit_t   unit_i ,ENET_QueueId_t queueId_i,enet_xPermission_opposite_dbt *entry){

   MEA_Uint32 size;
   
    if(ENET_IsValid_Queue(unit_i,
 						  queueId_i,
						  ENET_FALSE)==MEA_FALSE){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
  					       "%s - ENET_IsValid_Queue =   \n",
						   __FUNCTION__,queueId_i);
		return MEA_ERROR; 
	}
    
    size = (sizeof(enet_xPermission_opposite_dbt)-sizeof(MEA_Uint32));
    size += ((ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(MEA_UNIT_0)/32)*sizeof(MEA_Uint32));
    //size=sizeof(*entry);
    MEA_OS_memcpy(entry,ENET_XPERMISSION_OPPOSITE_TABLE(queueId_i),size);
    
    
    return MEA_OK;
}



MEA_Status enet_xPermission_UpdateByPortState(MEA_Unit_t   unit_i,
										      ENET_QueueId_t queueId_i,
											  MEA_PortState_te portState_i)
{

    ENET_Uint32* ptr;
    ENET_xPermissionId_t xPermissionId;
    MEA_Uint32 i,j,mask;
    enet_xPermission_opposite_dbt* entry;
	MEA_Uint32 count;
	MEA_db_HwUnit_t hwUnit;

	MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    entry = ENET_XPERMISSION_OPPOSITE_TABLE(queueId_i);
	entry->portState = portState_i;

    

	count = ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(hwUnit);

	for (i=0,xPermissionId=0,ptr=&(entry->xPermissions[0]);
         i<count/32;
         i++,ptr++) {
        for (j=0,mask=0x00000001;j<32;j++,mask <<= 1,xPermissionId++) {
            if ((enet_xPermission_Table[xPermissionId]!=NULL)&&(*ptr & mask) && (enet_xPermission_Table[xPermissionId]->numberOfOwners >=1)&&
			   (!enet_xPermission_Table[xPermissionId]->isCpu)) {
               if (enet_UpdateDevice_xPermission (unit_i,
				                                  xPermissionId,
												  enet_xPermission_Table[xPermissionId]->isCpu,
												  &(enet_xPermission_Table[xPermissionId]->portMap),
												  NULL) != ENET_OK) {
				  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, 
					                "%s - enet_UpdateDevice_xPermission failed \n",
									__FUNCTION__);
				  return MEA_ERROR;
			   }
           }
        }
    }

    return MEA_OK;
}


MEA_Status enet_xPermission_Add_owner(ENET_Unit_t    unit_i,ENET_xPermissionId_t id_io)
{


enet_xPermission_Table[id_io]->numberOfOwners++;

    return MEA_OK;
}

MEA_Status enet_xPermission_delete_owner(ENET_Unit_t    unit_i, ENET_xPermissionId_t id_i)
{

    if(enet_xPermission_Table[id_i]->numberOfOwners >1)
    enet_xPermission_Table[id_i]->numberOfOwners--;
    else{

      return enet_Delete_xPermission(unit_i, id_i);

    }

    return MEA_OK;
}








