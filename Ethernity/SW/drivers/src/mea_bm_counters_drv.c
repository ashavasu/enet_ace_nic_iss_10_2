/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#if 1
#ifdef MEA_OS_LINUX
#ifdef MEA_OS_OC
#include <linux/unistd.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#endif
#else
#include <time.h>
#endif
#endif



#include "MEA_platform.h"

#include "mea_api.h"
#include "mea_bm_drv.h"
#include "mea_drv_common.h"
#include "mea_init.h"
#include "mea_port_drv.h"
#include "enet_queue_drv.h"
#include "mea_db_drv.h"
#include "mea_events_drv.h"
#include "mea_bm_counters_drv.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

#define MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit_i,pmType_i) (((MEA_Uint16)(hwUnit_i)*MEA_PM_TYPE_LAST)+(pmType_i))

		



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct MyStruct
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 from_pm;
#else
    MEA_Uint32 from_pm;
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 to_pm :31;
    MEA_Uint32 en_blk : 1;
#else
    MEA_Uint32 en_blk :1;
    MEA_Uint32 to_pm :31;
#endif
}mea_pmClear_cmd_t;


typedef enum {
    MEA_COUNTERS_PM_COUNTER_TYPE_YELLOW_OR_RED_DISCARD_BYTE,
    MEA_COUNTERS_PM_COUNTER_TYPE_YELLOW_FWD_BYTE,
    MEA_COUNTERS_PM_COUNTER_TYPE_GREEN_DISCARD_BYTE,
    MEA_COUNTERS_PM_COUNTER_TYPE_GREEN_FWD_BYTE,
    MEA_COUNTERS_PM_COUNTER_TYPE_RED_DISCARD_PKT,
    MEA_COUNTERS_PM_COUNTER_TYPE_YELLOW_DISCARD_PKT,
    MEA_COUNTERS_PM_COUNTER_TYPE_YELLOW_FWD_PKT,
    MEA_COUNTERS_PM_COUNTER_TYPE_GREEN_DISCARD_PKT,
    MEA_COUNTERS_PM_COUNTER_TYPE_GREEN_FWD_PKT, 
    MEA_COUNTERS_PM_COUNTER_TYPE_OTHER_DISCARD_PKT,
    MEA_COUNTERS_PM_COUNTER_TYPE_LAST
} MEA_Counters_PM_CounterType_te;

#define MEA_COUNTERS_PM_COUNTER_TYPE_BYTE_LAST MEA_COUNTERS_PM_COUNTER_TYPE_GREEN_FWD_BYTE 
#define MEA_COUNTERS_PM_COUNTER_TYPE_UNKNOWN 0xffff

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

MEA_Uint64 RatePortsByCluster[MEA_MAX_PORT_NUMBER+1];

static MEA_Uint32                       MEA_Counters_BLK_ID;

static MEA_db_dbt                     MEA_Counters_PM_db[MEA_PM_TYPE_LAST]    = {NULL};
static MEA_drv_PM_dbt                *MEA_Counters_PM_Table = NULL;
static MEA_Counters_PM_dbt            *MEA_Counters_pm_value = NULL;
static mea_PM_HW_data_dbt              *MEA_Counters_pm_HW_BLK = NULL;



static MEA_Counters_PM_Type_dbt      *MEA_Counters_PM_Type_Table = NULL;
static MEA_Uint32                     MEA_Counters_PM_TBL[MEA_COUNTERS_PM_COUNTER_TYPE_LAST];
static MEA_Counters_Queue_dbt        *MEA_Counters_Queue_Table = NULL ;
static MEA_Counters_BM_dbt            MEA_Counters_BM;

MEA_Bool mea_PM_en_calc_rate;
/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
static MEA_Status MEA_drv_Clear_Counters_PMs_Block(MEA_Unit_t  unit, MEA_PmId_t    from_pmId, MEA_PmId_t    to_pmId);

/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/
MEA_Status mea_drv_pm_get_range(MEA_Unit_t      unit_i,
                                MEA_pm_type_te  pmIdType_i,
                                MEA_PmId_t     *base_index_o,
                                MEA_PmId_t     *end_index_o)
{
    MEA_pm_type_te i;
    *base_index_o=0;
 
    if (pmIdType_i >= MEA_PM_TYPE_LAST) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - pmIdTtpe_i %d not in valid range 0..%d\n",
                         __FUNCTION__,pmIdType_i,MEA_PM_TYPE_LAST-1);
       return MEA_ERROR;  
    }

    for (i=0;i<pmIdType_i;i++)
    {
        *base_index_o += MEA_Counters_PM_Type_Table[i].max_value;
    }
    *end_index_o = *base_index_o+MEA_Counters_PM_Type_Table[pmIdType_i].max_value - 1;

    if (*base_index_o==0)
    {
        *base_index_o = 1;
    }

    return MEA_OK;
}

MEA_Bool mea_drv_IsPmIdInRange(MEA_PmId_t      pmId_i)
{
    if (pmId_i<MEA_MAX_NUM_OF_PM_ID)
    {
        return MEA_TRUE;
    }

    return MEA_FALSE;

}

 
MEA_Bool mea_drv_ispm_freeEntry (MEA_Unit_t      unit_i,
                                 MEA_pm_type_te  pmIdType_i)
{
    MEA_PmId_t i;
    MEA_PmId_t  base_index;
    MEA_PmId_t  end_index;

    if (mea_drv_pm_get_range(unit_i,pmIdType_i,&base_index,&end_index) != MEA_OK)
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_get_pm_range failed \n",
                         __FUNCTION__);
        return MEA_FALSE;
    }

    for(i=base_index; i<=end_index;i++)
    {
        if(MEA_Counters_PM_Table[i].valid == MEA_FALSE)
        {
            return MEA_TRUE;
        }
    }

    return MEA_FALSE;
}


MEA_Bool MEA_API_Get_IsPM_FreeEntry(MEA_Unit_t      unit_i, MEA_pm_type_te  pmIdType_i)
{
    return mea_drv_ispm_freeEntry(unit_i, pmIdType_i);
}

static MEA_Status mea_drv_pm_add_owner(MEA_Unit_t      unit_i,
                                       MEA_PmId_t      id_i,
                                       MEA_pm_type_te  pmIdType_i)
{
    if (mea_drv_IsPmIdInRange(id_i) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }

	if ( MEA_Counters_PM_Table[id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - none existing PM entry with ID %d\n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }
    
    if(MEA_Counters_PM_Table[id_i].pm_type != pmIdType_i){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -  PM entry with ID %d is different pmId type\n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;
    }
	
    
    MEA_Counters_PM_Table[id_i].num_of_owners++;

	return MEA_OK;
}

static MEA_Status mea_drv_pm_delete_owner(MEA_Unit_t      unit_i,
                                          MEA_PmId_t      id_i,
                                          MEA_pm_type_te  pmIdType_i)
{
	MEA_db_HwUnit_t hwUnit;

    if (mea_drv_IsPmIdInRange(id_i) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }


	if ( MEA_Counters_PM_Table[id_i].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - none existing Policer entry with ID %d\n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;  
    }

    if(MEA_Counters_PM_Table[id_i].pm_type != pmIdType_i){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -  PM entry with ID %d is different pmId type\n",
                         __FUNCTION__,id_i);
       return MEA_ERROR;
    }

	MEA_Counters_PM_Table[id_i].num_of_owners--;

	if ( MEA_Counters_PM_Table[id_i].num_of_owners == 0 )
	{
		if (MEA_API_Clear_Counters_PM(unit_i,
									  id_i) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - MEA_API_Clear_Counters_PM for pmId=%d failed\n",
							  __FUNCTION__,id_i);
			MEA_Counters_PM_Table[id_i].num_of_owners++;
			return MEA_ERROR;
		}	

		MEA_Counters_PM_Table[id_i].valid = MEA_FALSE;

		/* Get Current HwUnit */
		MEA_DRV_GET_HW_UNIT(unit_i,
			                hwUnit,
						    MEA_Counters_PM_Table[id_i].num_of_owners++;
  		                    MEA_Counters_PM_Table[id_i].valid = MEA_TRUE;
						    return MEA_ERROR);

  	   /* Delete the HwId */
       if (mea_db_Delete(unit_i,
           MEA_Counters_PM_db[MEA_Counters_PM_Table[id_i].pm_type],
					     (MEA_db_SwId_t)id_i,
					     hwUnit) != MEA_OK) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
		  	                 "%s - mea_db_Delete failed (serviceId=%d,hwUnit=$d)\n",
						     __FUNCTION__,id_i,hwUnit);
		  MEA_Counters_PM_Table[id_i].num_of_owners++;
  		  MEA_Counters_PM_Table[id_i].valid = MEA_TRUE;
		  return MEA_ERROR;
	   }

	}


	return MEA_OK;
}



static MEA_Status mea_drv_get_free_pm_id(MEA_PmId_t *id_o,
                                         MEA_pm_type_te PmId_Type)
{
    MEA_PmId_t  base_index;
    MEA_PmId_t  end_index;

    if (mea_drv_pm_get_range(MEA_UNIT_0,PmId_Type,&base_index,&end_index) != MEA_OK)
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_get_pm_range failed \n",
                         __FUNCTION__);
        return MEA_FALSE;
    }

	for ( *id_o = base_index; *id_o <= end_index; (*id_o)++ )
	{
        if (*id_o == MEA_PLAT_GENERATE_NEW_ID) {
            continue;
        }
        if ( MEA_Counters_PM_Table[*id_o].valid == MEA_FALSE )
		{
			return MEA_OK;
		}
	}

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - no free PM entry\n",
                      __FUNCTION__);
	return MEA_ERROR;
}

MEA_Status mea_drv_get_first_pm_id(MEA_Bool *found_o, MEA_PmId_t *id_o)
{
	MEA_Uint32 i;
	*found_o = MEA_FALSE;
	
	for ( i=0; i < (MEA_Uint32)MEA_MAX_NUM_OF_PM_ID; i++ )
	{
		if (MEA_Counters_PM_Table[i].valid)
		{
			*id_o = (MEA_PmId_t)i;
			*found_o = MEA_TRUE;
			return MEA_OK;
		}
	}

	return MEA_OK;
}

MEA_Status mea_drv_get_next_pm_id(MEA_Bool       *found_o,
                                  MEA_PmId_t     *id_io)
{
	MEA_Uint32 i;
	*found_o = MEA_FALSE;

    if (mea_drv_IsPmIdInRange(*id_io) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,*id_io);
       return MEA_ERROR;  
    }

	for ( i=*id_io+1; i < (MEA_Uint32)MEA_MAX_NUM_OF_PM_ID; i++ )
	{
		if (MEA_Counters_PM_Table[i].valid)
		{
			*id_io = (MEA_PmId_t)i;
			*found_o = MEA_TRUE;
			return MEA_OK;
		}
	}

	return MEA_OK;
}

static MEA_Status mea_drv_create_pm_entry_force(MEA_Unit_t          unit_i,
                                                MEA_PmId_t          *id_io,
                                                MEA_pm_type_te    PmId_Type,
                                                MEA_Bool          force_create )
{
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId;


    /* Allocate HwId */
    MEA_DRV_GET_HW_UNIT(unit_i,
        hwUnit,
        return MEA_ERROR);
    hwId = *id_io;
    if (mea_db_Create(unit_i,
        MEA_Counters_PM_db[PmId_Type],
        (MEA_db_SwId_t)(*id_io),
        hwUnit,
        &hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_db_Create failed (swId=%d,hwUnit=%d) \n",
                __FUNCTION__,*id_io,hwUnit);
            return MEA_ERROR;
    }

    MEA_Counters_PM_Table[*id_io].valid       = MEA_TRUE;
    MEA_Counters_PM_Table[*id_io].hwUnit      = hwUnit;
    MEA_Counters_PM_Table[*id_io].hwId_offset = hwId; 
    MEA_Counters_PM_Table[*id_io].pm_type     = PmId_Type;

    if (MEA_API_Clear_Counters_PM(unit_i,
        *id_io) != MEA_OK) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Clear_Counters_PM for pmId=%d failed\n",
            __FUNCTION__,*id_io);
        return MEA_ERROR;
    }		


    if ( mea_drv_pm_add_owner(unit_i,*id_io,PmId_Type) != MEA_OK )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_pm_add_owner failed, id %d\n",
            __FUNCTION__, *id_io);
        MEA_Counters_PM_Table[*id_io].valid = MEA_FALSE;
        mea_db_Delete(unit_i,MEA_Counters_PM_db,(MEA_db_SwId_t)(*id_io),hwUnit);
        return MEA_ERROR;
    }

    return MEA_OK;
}


static MEA_Status mea_drv_create_pm_entry(MEA_Unit_t          unit_i,
                                          MEA_PmId_t          *id_io,
                                          MEA_pm_type_te    PmId_Type,
                                          MEA_Bool            force_create )
{
	MEA_db_HwUnit_t hwUnit;
	MEA_db_HwId_t   hwId;
    MEA_PmId_t      base_index;
    MEA_PmId_t      end_index;

    if (mea_drv_pm_get_range(unit_i,PmId_Type,&base_index,&end_index) != MEA_OK)
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_get_pm_range failed \n",
                         __FUNCTION__);
        return MEA_FALSE;
    }

	if ( force_create )
	{

        if ( !mea_drv_IsPmIdInRange(*id_io) )
        {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - non valid id_io %d\n",
                             __FUNCTION__, *id_io);
           return MEA_ERROR;	
        }

		if ( MEA_Counters_PM_Table[*id_io].valid == MEA_TRUE )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - PM entry %d already exist\n",
							  __FUNCTION__, *id_io);
			return MEA_ERROR;
		}
	}
	else
	{
		if ( mea_drv_get_free_pm_id(id_io,PmId_Type) != MEA_OK ) 
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_get_free_pm_id failed\n",
							  __FUNCTION__);
			return MEA_ERROR;
		}
	}

	/* Allocate HwId */
	MEA_DRV_GET_HW_UNIT(unit_i,
		                hwUnit,
		                return MEA_ERROR);
    if (base_index==1) base_index=0;
	hwId = (MEA_db_HwId_t)(*id_io - base_index);
	if (mea_db_Create(unit_i,
		              MEA_Counters_PM_db[PmId_Type],
					  (MEA_db_SwId_t)(*id_io),
					  hwUnit,
					  &hwId) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - mea_db_Create failed (swId=%d,hwUnit=%d) \n",
						  __FUNCTION__,*id_io,hwUnit);
		return MEA_ERROR;
	}

	MEA_Counters_PM_Table[*id_io].valid       = MEA_TRUE;
    MEA_Counters_PM_Table[*id_io].hwUnit      = hwUnit;
    MEA_Counters_PM_Table[*id_io].hwId_offset = hwId; 
    MEA_Counters_PM_Table[*id_io].pm_type     = PmId_Type;
#if 0 /*clear on delete entry */   
	if (MEA_API_Clear_Counters_PM(unit_i,
								  *id_io) != MEA_OK) 
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
						  "%s - MEA_API_Clear_Counters_PM for pmId=%d failed\n",
						  __FUNCTION__,*id_io);
		return MEA_ERROR;
	}		
#endif

	if ( mea_drv_pm_add_owner(unit_i,*id_io,PmId_Type) != MEA_OK )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_pm_add_owner failed, id %d\n",
                          __FUNCTION__, *id_io);
		MEA_Counters_PM_Table[*id_io].valid = MEA_FALSE;
		mea_db_Delete(unit_i,MEA_Counters_PM_db,(MEA_db_SwId_t)(*id_io),hwUnit);
		return MEA_ERROR;
	}

	return MEA_OK;
}


MEA_Status mea_drv_Set_PM(MEA_Unit_t               unit,
						  MEA_PmId_t              *id_io,
                          MEA_pm_type_te        PmId_Type,
						  MEA_Bool                 create_new_i)
{

	MEA_PmId_t pm_id;

	if ( !mea_drv_IsPmIdInRange(*id_io) )
	{
		 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - non valid id_io %d\n",
                           __FUNCTION__, *id_io);
		 return MEA_ERROR;	
	 }


	/* If input ID is 0 - if create_new_i is TRUE - allocate new PM, else return 0.
	   If input ID is not 0 - if exist, add owner. If not exist. create at that point. */

    /* T.B.D eyal - if input is 0 should we just return 0, not create new one? */
	pm_id = *id_io;

	if ( pm_id == 0 && create_new_i )
	{
		if ( mea_drv_create_pm_entry(unit, &pm_id, PmId_Type, MEA_FALSE) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_create_pm_entry failed\n",
							  __FUNCTION__);
			return MEA_ERROR;
		}

		*id_io = pm_id;
	}
	else if ( pm_id != 0 )
	{

		if ( !MEA_Counters_PM_Table[pm_id].valid ) /* create PM at that id */
		{
			if ( mea_drv_create_pm_entry(unit, &pm_id,PmId_Type ,MEA_TRUE) != MEA_OK )
			{
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_create_pm_entry failed\n",
								  __FUNCTION__);
				return MEA_ERROR;
			}
		}
		else 		/* Just add owner */
		{

            MEA_db_HwUnit_t hwUnit;
            
            MEA_DRV_GET_HW_UNIT(unit,hwUnit,return MEA_ERROR;)

            if(MEA_Counters_PM_Table[pm_id].hwUnit != hwUnit){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - You can not share PmId between upstream and downstream (Pmid=%d)\n",
				              __FUNCTION__,pm_id);
			    return MEA_ERROR;
            }

			if ( mea_drv_pm_add_owner(unit,pm_id,PmId_Type) != MEA_OK )
			{
				  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - mea_drv_pm_add_owner failed, index %d\n",
								  __FUNCTION__, pm_id);
				return MEA_ERROR; 
			}
		}
	}	
	else
		*id_io = 0;
	
	return MEA_OK;

} 
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Delete_Policer>                                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Delete_PM(MEA_Unit_t unit,
							 MEA_PmId_t id)
{
    /* Check for valid id parameter */
    if (mea_drv_IsPmIdInRange(id) != MEA_TRUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

	if ( MEA_Counters_PM_Table[id].valid == MEA_FALSE )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - no PM entry with ID %d\n",
                         __FUNCTION__, id);
       return MEA_ERROR; 
    }

	if ( mea_drv_pm_delete_owner(unit,id,MEA_Counters_PM_Table[id].pm_type) != MEA_OK )
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_pm_delete_owner failed, id %d\n",
                         __FUNCTION__, id);
       return MEA_ERROR; 
    }


	return MEA_OK;

}


MEA_Bool mea_drv_IsPm_Exist(MEA_Unit_t unit,MEA_PmId_t      index)
{
    if (mea_drv_IsPmIdInRange(index) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid index %d \n",
                         __FUNCTION__,index);
       return MEA_FALSE;  
    }
    if ( MEA_Counters_PM_Table[index].valid == MEA_TRUE )
	{
        return MEA_TRUE; /*Alex*/
    }
    return MEA_FALSE;
}
MEA_Status mea_drv_get_pm_pool_entry(MEA_PmId_t      index,
									 MEA_Bool       *found,
									 MEA_drv_PM_dbt *entry_o )
{
	*found = MEA_FALSE;

	if (mea_drv_IsPmIdInRange(index) != MEA_TRUE) 
	{
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid index %d \n",
                         __FUNCTION__,index);
       return MEA_ERROR;  
    }

	if ( MEA_Counters_PM_Table[index].valid == MEA_TRUE )
	{
		MEA_OS_memcpy(entry_o,
					  &(MEA_Counters_PM_Table[index]),
					  sizeof(*entry_o));

		*found = MEA_TRUE;
	}

	return MEA_OK;
}
/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Collect_Counters_PM>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Collect_Counters_PM  (MEA_Unit_t           unit_i,
                                         MEA_PmId_t           pmId,
                                         MEA_Counters_PM_dbt* entry) {


    MEA_ind_read_t                 ind_read;
    MEA_Uint32                     read_data[1];
    MEA_Uint32                     read_data_bytes[4];
    MEA_Uint32                     read_data_packets[7];
    MEA_Uint32                     val;


    MEA_db_HwUnit_t                hwUnit;
    MEA_db_HwId_t                  hwId;
    mea_memory_mode_e              save_globalMemoryMode;
    MEA_Counters_PM_dbt pm_counter_entry;
    time_t t1,t2;
#if defined(__KERNEL__)
    MEA_Uint32 tdiff;
#endif				 

	MEA_OS_memset(&t1, 0, sizeof(time_t));
	MEA_OS_memset(&t2, 0, sizeof(time_t));

    MEA_Uint64 rate;

    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit_i, MEA_DB_PM_TYPE) == MEA_TRUE)
        return MEA_OK;

    /* Get hwUnit and hwId */
    hwUnit= MEA_Counters_PM_Table[pmId].hwUnit;
    mea_drv_Get_pm_hwId_Calculate (unit_i,pmId,&hwId);

    /* Set Global Memory Mode */
    MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);
 
    /* build the ind_read structure values */
    ind_read.cmdReg		    = MEA_BM_IA_CMD;      
    ind_read.cmdMask		= MEA_BM_IA_CMD_MASK;      
    ind_read.statusReg		= MEA_BM_IA_STAT;   
    ind_read.statusMask	    = MEA_BM_IA_STAT_MASK;   
    ind_read.tableAddrReg	= MEA_BM_IA_ADDR;
    ind_read.tableAddrMask  = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_read.tableOffset	= hwId;
    ind_read.read_data	    = &read_data[0];

    t1=MEA_Counters_PM_Table[pmId].t1;


    MEA_OS_memset(&pm_counter_entry,0,sizeof(pm_counter_entry));
    MEA_OS_memcpy(&pm_counter_entry, &MEA_Counters_pm_value[pmId], sizeof(pm_counter_entry));




    /* read from HW */
    ind_read.read_data = &read_data_bytes[0];
    ind_read.tableType = ENET_BM_TBL_TYP_CNT_PM_READ_BYTE;
    if (MEA_API_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(read_data_bytes),
        MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ReadIndirect from tableType %d / tableOffset %d module BM failed "
            "(pmId=%d)\n",
            __FUNCTION__, ind_read.tableType, ind_read.tableOffset, pmId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    if (entry) {

            MEA_OS_UPDATE_COUNTER(entry->green_fwd_bytes.val, read_data_bytes[0]);
            MEA_OS_UPDATE_COUNTER(entry->green_dis_bytes.val, read_data_bytes[1]);
            MEA_OS_UPDATE_COUNTER(entry->yellow_fwd_bytes.val, read_data_bytes[2]);
            MEA_OS_UPDATE_COUNTER(entry->yellow_or_red_dis_bytes.val, read_data_bytes[3]);

    }

    ind_read.read_data = &read_data_packets[0];
    ind_read.tableType = ENET_BM_TBL_TYP_CNT_PM_READ_PKT;
    if (MEA_API_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(read_data_packets),
        MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ReadIndirect from tableType %d / tableOffset %d module BM failed "
            "(pmId=%d)\n",
            __FUNCTION__, ind_read.tableType, ind_read.tableOffset, pmId);
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
        return MEA_ERROR;
    }
    if (entry) {
            MEA_OS_UPDATE_COUNTER(entry->green_fwd_pkts.val,  read_data_packets[0]);
            MEA_OS_UPDATE_COUNTER(entry->green_dis_pkts.val,  read_data_packets[1]);
            MEA_OS_UPDATE_COUNTER(entry->yellow_fwd_pkts.val, read_data_packets[2]);
            MEA_OS_UPDATE_COUNTER(entry->yellow_dis_pkts.val, read_data_packets[3]);
            MEA_OS_UPDATE_COUNTER(entry->red_dis_pkts.val,    read_data_packets[4]);
            MEA_OS_UPDATE_COUNTER(entry->other_dis_pkts.val,  (read_data_packets[5] & 0x00ffffff )); /*24bit*/
#if 1            
            val = 0;
            val = (read_data_packets[5] >> 24) & 0xff;
            val |= (read_data_packets[6] & 0x00ffffff)<<8;
            MEA_OS_UPDATE_COUNTER(entry->dis_mtu_pkts.val, val);
#else
            //MEA_OS_UPDATE_COUNTER(entry->dis_mtu_pkts.val,    read_data_packets[6]);
            
#endif    
    }




    /* Restore global memory mode */
    MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    if((mea_PM_en_calc_rate))
	  t2=time(&MEA_Counters_PM_Table[pmId].t1);
    
	if(t2>t1 && t1 !=0  && (mea_PM_en_calc_rate)){

        if (MEA_Counters_pm_value[pmId].green_fwd_bytes.
            val != 0) {


            rate.val = MEA_Counters_pm_value[pmId].green_fwd_bytes.val -
                    pm_counter_entry.green_fwd_bytes.val;
#if defined(__KERNEL__)
                tdiff = (MEA_Uint32) t2-t1;
                do_div (rate.val, tdiff);
#else

                rate.val = rate.val/(t2-t1);
#endif
                MEA_Counters_pm_value[pmId].rate_green_fwd_bytes = rate;
        }
        if (MEA_Counters_pm_value[pmId].green_fwd_pkts.val != 0) {


            rate.val = MEA_Counters_pm_value[pmId].green_fwd_pkts.val -
                pm_counter_entry.green_fwd_pkts.val;
#if defined(__KERNEL__)
            tdiff = (MEA_Uint32) t2-t1;
            do_div (rate.val, tdiff);
#else

            rate.val = rate.val/(t2-t1);
#endif
            MEA_Counters_pm_value[pmId].rate_green_fwd_pkts = rate;
        }



        if (MEA_Counters_pm_value[pmId].yellow_fwd_bytes.val != 0) {


            rate.val = MEA_Counters_pm_value[pmId].yellow_fwd_bytes.val -
                pm_counter_entry.yellow_fwd_bytes.val;
#if defined(__KERNEL__)
            tdiff = (MEA_Uint32) t2-t1;
            do_div (rate.val, tdiff);
#else
            rate.val = rate.val/(t2-t1);
#endif
            MEA_Counters_pm_value[pmId].rate_yellow_fwd_bytes = rate;
        }
        if (MEA_Counters_pm_value[pmId].yellow_fwd_pkts.val != 0) {


            rate.val = MEA_Counters_pm_value[pmId].yellow_fwd_pkts.val -
                pm_counter_entry.yellow_fwd_pkts.val;
#if defined(__KERNEL__)
            tdiff = (MEA_Uint32) t2-t1;
            do_div (rate.val, tdiff);
#else
            rate.val = rate.val/(t2-t1);
#endif
            MEA_Counters_pm_value[pmId].rate_yellow_fwd_pkts = rate;
        }
/**************************************************************/
        if (MEA_Counters_pm_value[pmId].green_dis_pkts.val != 0) {


            rate.val = MEA_Counters_pm_value[pmId].green_dis_pkts.val - pm_counter_entry.green_dis_pkts.val;
#if defined(__KERNEL__)
            tdiff = (MEA_Uint32) t2-t1;
            do_div (rate.val, tdiff);
#else
            rate.val = rate.val/(t2-t1);
#endif
            MEA_Counters_pm_value[pmId].rate_green_dis_pkts = rate;
        }
        if (MEA_Counters_pm_value[pmId].yellow_dis_pkts.val != 0) {


            rate.val = MEA_Counters_pm_value[pmId].yellow_dis_pkts.val - pm_counter_entry.yellow_dis_pkts.val;
#if defined(__KERNEL__)
            tdiff = (MEA_Uint32) t2-t1;
            do_div (rate.val, tdiff);
#else
            rate.val = rate.val/(t2-t1);
#endif
            MEA_Counters_pm_value[pmId].rate_yellow_dis_pkts = rate;
        }
        if (MEA_Counters_pm_value[pmId].red_dis_pkts.val != 0) {


            rate.val = MEA_Counters_pm_value[pmId].red_dis_pkts.val - pm_counter_entry.red_dis_pkts.val;
#if defined(__KERNEL__)
            tdiff = (MEA_Uint32) t2-t1;
            do_div (rate.val, tdiff);
#else
            rate.val = rate.val/(t2-t1);
#endif
            MEA_Counters_pm_value[pmId].rate_red_dis_pkts = rate;
        }
        if (MEA_Counters_pm_value[pmId].yellow_or_red_dis_bytes.val != 0) {


            rate.val = MEA_Counters_pm_value[pmId].yellow_or_red_dis_bytes.val - pm_counter_entry.yellow_or_red_dis_bytes.val;
#if defined(__KERNEL__)
            tdiff = (MEA_Uint32) t2-t1;
            do_div (rate.val, tdiff);
#else
            rate.val = rate.val/(t2-t1);
#endif
            MEA_Counters_pm_value[pmId].rate_yellow_or_red_dis_bytes = rate;
        }
    }//t2>t1 && t1 !=0 && (mea_PM_en_calc_rate)


    /* Return to Callers */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Conclude_BM_Counters>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Conclude_BM_Counters(MEA_Unit_t unit_i)
{
    MEA_pm_type_te pm_type;

	if (MEA_Counters_PM_Table != NULL) {
		MEA_OS_free(MEA_Counters_PM_Table);
		MEA_Counters_PM_Table = NULL;
	}


    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit_i, MEA_DB_PM_TYPE) == MEA_FALSE)
    {
        if (MEA_Counters_pm_value != NULL) {
            MEA_OS_free(MEA_Counters_pm_value);
            MEA_Counters_pm_value = NULL;
        }
    }
    else {
        if (MEA_Counters_pm_HW_BLK != NULL) {
            MEA_OS_free(MEA_Counters_pm_HW_BLK);
            MEA_Counters_pm_HW_BLK = NULL;
        }

        
    }



    for (pm_type = 0; pm_type<MEA_PM_TYPE_LAST;pm_type++) {
        if (MEA_Counters_PM_db[pm_type]) {
	        if (mea_db_Conclude(unit_i,&MEA_Counters_PM_db[pm_type]) != MEA_OK) {
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			                      "%s - mea_db_Conclude failed (pm_type=%d)\n",
				        		  __FUNCTION__,pm_type);
		        return MEA_ERROR;
	        }
            MEA_Counters_PM_db[pm_type] = NULL;
        }
    }

    if (MEA_Counters_PM_Type_Table) {
        MEA_OS_free(MEA_Counters_PM_Type_Table);
        MEA_Counters_PM_Type_Table = NULL;
    }
    
    if(MEA_Counters_Queue_Table){
        MEA_OS_free(MEA_Counters_Queue_Table);
        MEA_Counters_Queue_Table = NULL;
    }
	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_Counters_PM_num_of_hw_size_func>         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Counters_PM_num_of_hw_size_func(MEA_Unit_t     unit_i,
								   				       MEA_db_HwUnit_t   hwUnit_i,
												       MEA_db_HwId_t *length_o,
												       MEA_Uint16    *num_of_regs_o) 
{

	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) >= max value (%d) \n",
						  __FUNCTION__,
						  hwUnit_i,
						  mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	if (length_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - legnth_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (num_of_regs_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - num_of_regs_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


    *length_o = MEA_Counters_PM_Type_Table[MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit_i,MEA_PM_TYPE_ALL)].max_value;
	*num_of_regs_o  = 0;

	return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_Counters_PM_num_of_sw_size_func>         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Counters_PM_num_of_sw_size_func(MEA_Unit_t    unit_i,
								     				   MEA_db_SwId_t *length_o,
									    			   MEA_Uint32    *width_o) 
{
	if (length_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - legnth_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (width_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - width_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


    *length_o = MEA_MAX_NUM_OF_PM_ID;
	*width_o  = 0;

	return MEA_OK;
}



#if 0												  

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Get_Counters_PM_Type_PKT_num_of_hw_size_func>         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Counters_PM_Type_PKT_num_of_hw_size_func(MEA_Unit_t     unit_i,
                                                                MEA_db_HwUnit_t   hwUnit_i,
                                                                MEA_db_HwId_t *length_o,
                                                                MEA_Uint16    *num_of_regs_o) 
{

	if (hwUnit_i >= mea_drv_num_of_hw_units) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_i (%d) >= max value (%d) \n",
						  __FUNCTION__,
						  hwUnit_i,
						  mea_drv_num_of_hw_units);
		return MEA_ERROR;
	}

	if (length_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - legnth_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (num_of_regs_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - num_of_regs_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


    *length_o = MEA_Counters_PM_Type_Table[MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit_i,MEA_PM_TYPE_PACKET_ONLY)].max_value;
	*num_of_regs_o  = 0;

	return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*              < mea_drv_Get_Counters_PM_Type_PKT_num_of_sw_size_func>      */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_Counters_PM_Type_PKT_num_of_sw_size_func(MEA_Unit_t    unit_i,
								     				   MEA_db_SwId_t *length_o,
									    			   MEA_Uint32    *width_o) 
{
	if (length_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - legnth_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	if (width_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - width_o == NULL\n",
						  __FUNCTION__);
		return MEA_ERROR;
	}


    *length_o = MEA_MAX_NUM_OF_PM_ID;
	*width_o  = 0;

	return MEA_OK;
}
#endif

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_BM_Counters>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_BM_Counters(MEA_Unit_t unit_i)
{

	
    MEA_db_HwUnit_t hwUnit;
    MEA_pm_type_te  pm_type;
    MEA_PmId_t      numOfPmIds;
    MEA_Uint32      size;
    MEA_Counters_PM_CounterType_te counterType;
    MEA_PmId_t PM_id;


    /* Init MEA_Counters_PM_TBL */
    for (counterType=0;counterType<MEA_COUNTERS_PM_COUNTER_TYPE_LAST;counterType++) {
        MEA_Counters_PM_TBL[counterType] = MEA_COUNTERS_PM_COUNTER_TYPE_UNKNOWN; 
    }


    /* Allocate MEA_Counters_PM_Type_Table */
    size = mea_drv_num_of_hw_units*MEA_PM_TYPE_LAST*sizeof(MEA_Counters_PM_Type_dbt);
    MEA_Counters_PM_Type_Table = (MEA_Counters_PM_Type_dbt*)MEA_OS_malloc(size);
    if (MEA_Counters_PM_Type_Table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate MEA_Counters_PM_Type_Table failed (size=%d)\n",
                          __FUNCTION__,size);
        return MEA_ERROR;
    }
    MEA_OS_memset((char*)MEA_Counters_PM_Type_Table,0,size);


    /* Init MEA_Counters_PM_Type_Table */
    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        for (pm_type=0;pm_type<MEA_PM_TYPE_LAST;pm_type++) {

            if (mea_drv_Get_DeviceInfo_NumOfPmIdsPerType(unit_i,
                                                         hwUnit,
                                                         pm_type,
                                                         &numOfPmIds
                                                         ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_drv_Get_DeviceInfo_NumOfPmIdsPerType failed\n",
                                  __FUNCTION__);
                return MEA_ERROR;
            }
            if (pm_type == 0) {
                MEA_Counters_PM_Type_Table[MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit,pm_type)].base_index = 0;
            } else {
                MEA_Counters_PM_Type_Table[MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit,pm_type)].base_index = 
                    MEA_Counters_PM_Type_Table[MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit,pm_type-1)].base_index;
                MEA_Counters_PM_Type_Table[MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit,pm_type)].base_index += 
                    MEA_Counters_PM_Type_Table[MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit,pm_type-1)].max_value;
            }
            MEA_Counters_PM_Type_Table[MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit,pm_type)].max_value = numOfPmIds;
        }
    }
                                                         
    
    
    /* Init db - Hardware Shadow */
    for (pm_type = 0; pm_type<MEA_PM_TYPE_LAST;pm_type++) {
        switch(pm_type) {
        case MEA_PM_TYPE_ALL:
            if (mea_db_Init(unit_i,
                            "PM All",
                            mea_drv_Get_Counters_PM_num_of_sw_size_func,
                            mea_drv_Get_Counters_PM_num_of_hw_size_func,
                            &MEA_Counters_PM_db[MEA_PM_TYPE_ALL]) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_db_Init failed (pm_type=%d)\n",
                                  __FUNCTION__,pm_type);
                return MEA_ERROR;
            }
            break;
#if 0
        case MEA_PM_TYPE_PACKET_ONLY:
            if (mea_db_Init(unit_i,
                            "PM Packets",
                            mea_drv_Get_Counters_PM_Type_PKT_num_of_sw_size_func,
                            mea_drv_Get_Counters_PM_Type_PKT_num_of_hw_size_func,
                            &MEA_Counters_PM_db[MEA_PM_TYPE_PACKET_ONLY]) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_db_Init failed (pm_type=%d)\n",
                                  __FUNCTION__,pm_type);
                return MEA_ERROR;
            }
            break;
#endif
        case MEA_PM_TYPE_LAST:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Unknown pm_type %d \n",__FUNCTION__,pm_type);
            return MEA_ERROR;
        }
    }


	/* Allocate MEA_Counters_PM_Table */
	if (MEA_Counters_PM_Table == NULL) {
		
		size = sizeof(MEA_drv_PM_dbt)*MEA_MAX_NUM_OF_PM_ID;
		MEA_Counters_PM_Table = (MEA_drv_PM_dbt*)MEA_OS_malloc(size);
		if (MEA_Counters_PM_Table == NULL) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - Allocate MEA_Counters_PM_Table failed \n",
							  __FUNCTION__);
			return MEA_ERROR;
		}
		MEA_OS_memset((char*)MEA_Counters_PM_Table,0,size);
	}

    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit_i, MEA_DB_PM_TYPE) == MEA_FALSE)
    {
        if (MEA_Counters_pm_value == NULL) {

            size = sizeof(MEA_Counters_PM_dbt)*MEA_MAX_NUM_OF_PM_ID;
            MEA_Counters_pm_value = (MEA_Counters_PM_dbt*)MEA_OS_malloc(size);
            if (MEA_Counters_pm_value == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Allocate MEA_Counters_pm_value failed \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
            MEA_OS_memset((char*)MEA_Counters_pm_value, 0, size);
        }
    } else {
    
        if (MEA_Counters_pm_HW_BLK == NULL) {

            size = sizeof(mea_PM_HW_data_dbt)*(MEA_COUNTERS_PM_BLK_READ);
            MEA_Counters_pm_HW_BLK = (mea_PM_HW_data_dbt*)MEA_OS_malloc(size);
            if (MEA_Counters_pm_HW_BLK == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Allocate MEA_Counters_pm_HW_BLK failed \n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
            MEA_OS_memset((char*)MEA_Counters_pm_HW_BLK, 0, size);
        }


        
    
    
    }
 


    /* Reset Counters Services */
    if (MEA_API_Clear_Counters_PMs(MEA_UNIT_0) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Clear_Counters_PMs failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
#if 0
    /* Reset Counters BM */
    if (MEA_API_Clear_Counters_BM(MEA_UNIT_0) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Clear_Counters_BM failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
#endif

    /* Allocate MEA_Counters_Queue_Table */
    size =  ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT * sizeof(MEA_Counters_Queue_dbt);
    MEA_Counters_Queue_Table = (MEA_Counters_Queue_dbt*)MEA_OS_malloc(size);
    if (MEA_Counters_Queue_Table == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate MEA_Counters_Queue_Table failed (size=%d)\n",
                          __FUNCTION__,size);
        return MEA_ERROR;
    }
    MEA_OS_memset(MEA_Counters_Queue_Table,0,size);

#if 0
    /* LIOR: This need to be done after enet_Init_Queue ,
             and this will be auto for any new create queue */
    /* Reset Counters Queue */
    if (MEA_API_Clear_Counters_Queues(MEA_UNIT_0) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Clear_Counters_Queues failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
#endif
    if(MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT && MEA_TDM_SUPPORT ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT  MEA_TDM_SUPPORT failed to create PM counters !!!!!!!!!\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT == MEA_TRUE && (MEA_TDM_SUPPORT == MEA_TRUE)){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Can't support TDM And  BONDING_SUPPORT!!!!!!!!!\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT){
        for(PM_id=(MEA_MAX_NUM_OF_PM_ID-32); PM_id< MEA_MAX_NUM_OF_PM_ID-32+24;PM_id++){
            mea_drv_create_pm_entry_force(MEA_UNIT_0,&PM_id,MEA_PM_TYPE_ALL,MEA_TRUE);
        }
    }
//     if(MEA_TDM_SUPPORT){
//         for(PM_id=(MEA_MAX_NUM_OF_PM_ID -48); PM_id< MEA_MAX_NUM_OF_PM_ID;PM_id++){
//             mea_drv_create_pm_entry_force(MEA_UNIT_0,&PM_id,MEA_PM_TYPE_ALL,MEA_TRUE);
//         }
// 
//     }


    /* return to caller */
	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_BM_Counters>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_BM_Counters(MEA_Unit_t unit_i)
{

	MEA_Bool              found;
	MEA_PmId_t            pmId;
	ENET_QueueId_t        queueId;
    MEA_Counters_BM_dbt   save_MEA_Counters_BM;
    MEA_Counters_Queue_dbt  save_counters_queue_entry;


    /* Read all Queue Counters from the fpga to cause clear of the Queue counters in the fpga */
	if (ENET_GetFirst_Queue(unit_i,&queueId,NULL,&found)!=ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Failed to get first Queue\n",
                          __FUNCTION__);
		return MEA_ERROR;
	}
    while (found) {
        MEA_OS_memcpy(&save_counters_queue_entry,
                      &(MEA_Counters_Queue_Table[queueId]),
                      sizeof(save_counters_queue_entry));
	    if (MEA_API_Collect_Counters_Queue(unit_i,queueId,NULL)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect Port Counters for queue=%d failed\n",
                              __FUNCTION__,queueId);
            MEA_OS_memcpy(&(MEA_Counters_Queue_Table[queueId]),
                          &save_counters_queue_entry,
                          sizeof(MEA_Counters_Queue_Table[0]));
            return MEA_ERROR;
        }
        MEA_OS_memcpy(&(MEA_Counters_Queue_Table[queueId]),
                      &save_counters_queue_entry,
                      sizeof(MEA_Counters_Queue_Table[0]));
        if( ENET_GetNext_Queue(unit_i,&queueId,NULL,&found) != ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Faild to get next Queue\n",
                             __FUNCTION__);
			return MEA_ERROR;
		}
    }


    /* Read all Pm Counters from the fpga to cause clear of the PM counters in the fpga */
   pmId=0;
    if ( mea_drv_get_first_pm_id(&found, &pmId) != MEA_OK) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_get_first_pm_id failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    while (found) {
        if (mea_drv_Collect_Counters_PM(unit_i,pmId,NULL) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - mea_drv_Collect_Counters_PM failed "
                               "(pmId=%d)\n",
                               __FUNCTION__,pmId);
            return MEA_ERROR;
        }
        if ( mea_drv_get_next_pm_id(&found, &pmId) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_get_first_pm_id failed\n",
							  __FUNCTION__);
			return MEA_ERROR;
		}
    }

    /* Read all BM Global Counters from the fpga to cause clear of the BM global counters in the fpga */
    MEA_OS_memcpy(&save_MEA_Counters_BM,
                  &MEA_Counters_BM,
                  sizeof(save_MEA_Counters_BM));
    if (MEA_API_Collect_Counters_BM(unit_i,NULL) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - MEA_API_Collect_Counters_BM failed\n",
							  __FUNCTION__);
            MEA_OS_memcpy(&MEA_Counters_BM,
                          &save_MEA_Counters_BM,
                          sizeof(MEA_Counters_BM));
			return MEA_ERROR;
    }
    MEA_OS_memcpy(&MEA_Counters_BM,
                  &save_MEA_Counters_BM,
                  sizeof(MEA_Counters_BM));


	return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <mea_drv_Get_pm_db>                                   */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Get_pm_db_ByType(MEA_Unit_t unit_i,
						            MEA_db_dbt* db_po,
                                    MEA_pm_type_te type_i) {

	if (db_po == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - db_po == NULL\n",__FUNCTION__);
		return MEA_ERROR;
	}

	*db_po = MEA_Counters_PM_db[type_i];

	return MEA_OK;

}

void mea_PmClear_UpdateHwEntry_Cmd(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)

{
    MEA_Unit_t          unit_i = (MEA_Unit_t)arg1;
   // MEA_db_HwUnit_t     hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    //MEA_db_HwId_t       hwId_i = (MEA_db_HwId_t)((long)arg3);
    mea_pmClear_cmd_t *entry_pi = (mea_pmClear_cmd_t*)arg4;
    //MEA_Uint32      count_shift;
    
    MEA_Uint32 val[2];

    val[0] = 0;
    val[1] = 0;
    val[0] = entry_pi->from_pm;
    val[1] = entry_pi->to_pm;
    val[1] |= entry_pi->en_blk << 31;
 
    MEA_API_WriteReg(unit_i, MEA_BM_IA_WRDATA0, val[0], MEA_MODULE_BM);
    MEA_API_WriteReg(unit_i, MEA_BM_IA_WRDATA1, val[1], MEA_MODULE_BM);

}


void  mea_drv_Get_pm_hwId_Calculate (MEA_Unit_t     unit_i,
                                     MEA_PmId_t     pmId,
                                     MEA_db_HwId_t *pm_hwId)
{
    MEA_Uint16 index;
    MEA_pm_type_te pm_type;
    MEA_db_HwUnit_t hwUnit;


    hwUnit  = MEA_Counters_PM_Table[pmId].hwUnit;
    pm_type = MEA_Counters_PM_Table[pmId].pm_type;

    index = MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit,pm_type);

    *pm_hwId   = MEA_Counters_PM_Type_Table[index].base_index;
    *pm_hwId  += MEA_Counters_PM_Table[pmId].hwId_offset;

}
/************************************************************************************/

void mea_drv_get_Counters_PM_fromblock(MEA_PmId_t pmId, MEA_Counters_PM_dbt *entry)
{
    MEA_PmId_t from_pm, to_pm;
    /*check if the pmId is on block*/
    MEA_PmId_t my_pm=0;
  

        from_pm = MEA_Counters_BLK_ID*MEA_COUNTERS_PM_BLK_READ;
        if (MEA_Counters_BLK_ID + 1 < MEA_COUNTERS_PM_MAX_OF_BLK)
            to_pm = ((MEA_Counters_BLK_ID + 1)*MEA_COUNTERS_PM_BLK_READ) - 1;
        else
            to_pm = MEA_Counters_BLK_ID*MEA_COUNTERS_PM_BLK_READ + MEA_COUNTERS_PM_BLK_READ - 1;

        if (pmId >= from_pm && pmId <= to_pm){
            
            if (pmId < MEA_COUNTERS_PM_BLK_READ){
                my_pm = pmId;
            } else{
                my_pm = pmId % MEA_COUNTERS_PM_BLK_READ;
            }

            entry->green_fwd_bytes.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.green_fwd_bytes;        /*0  */
            entry->green_dis_bytes.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.green_dis_bytes;        /*1  */
            entry->yellow_fwd_bytes.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.yellow_fwd_bytes;       /*2  */
            entry->yellow_or_red_dis_bytes.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.yellow_or_red_dis_bytes; /*3  */

            /* Packets counters */
            entry->green_fwd_pkts.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.green_fwd_pkts; /*4  */
            entry->green_dis_pkts.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.green_dis_pkts; /*5  */
            entry->yellow_fwd_pkts.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.yellow_fwd_pkts; /*6  */
            entry->yellow_dis_pkts.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.yellow_dis_pkts; /*7  */
            entry->red_dis_pkts.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.red_dis_pkts;    /*8  */
            entry->other_dis_pkts.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.other_dis_pkts; /*9  */
            entry->dis_mtu_pkts.s.lsw = MEA_Counters_pm_HW_BLK[my_pm].val.dis_mtu_pkts; /*10  */
            /*----------------------------------------*/
            entry->green_fwd_bytes.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.green_dis_bytes_green_fwd_bytes) & 0xffff; /*11  */
            entry->green_dis_bytes.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.green_dis_bytes_green_fwd_bytes >> 16) & 0xffff; /*11  */
            entry->yellow_fwd_bytes.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.yellow_or_red_dis_bytes_yellow_fwd_bytes) & 0xffff; /*12  */
            entry->yellow_or_red_dis_bytes.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.yellow_or_red_dis_bytes_yellow_fwd_bytes >> 16) & 0xffff; /*12  */
            /* Packets counters */
            entry->green_fwd_pkts.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.green_dis_pkts_green_fwd_pkts) & 0xffff; /*13  */
            entry->green_dis_pkts.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.green_dis_pkts_green_fwd_pkts >> 16) & 0xffff; /*13  */
            entry->yellow_fwd_pkts.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.yellow_dis_pkts_yellow_fwd_pkts) & 0xffff; /*14  */
            entry->yellow_dis_pkts.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.yellow_dis_pkts_yellow_fwd_pkts >> 16) & 0xffff; /*14  */

            entry->red_dis_pkts.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.other_dis_pkts_red_dis_pkts) & 0xffff; /*15  */
            entry->other_dis_pkts.s.msw = (MEA_Counters_pm_HW_BLK[my_pm].val.other_dis_pkts_red_dis_pkts) & 0xffff; /*15  */


#if 0 /*not support the rate*/
            entry->rate_green_fwd_pkts;
            entry->rate_green_dis_pkts;
            entry->rate_yellow_fwd_pkts;
            entry->rate_yellow_dis_pkts;
            entry->rate_red_dis_pkts;

            entry->rate_green_fwd_bytes;
            entry->rate_yellow_fwd_bytes;
            entry->rate_yellow_or_red_dis_bytes;

            entry->rate_Drop_bytes; //
#endif







        }
        else{
            /* the pm is not on the range */
        
        }

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_PMs_Block(MEA_Unit_t  unit  ,MEA_Uint32 block)
{
    MEA_Uint32 base_address;
    MEA_PmId_t pmId;
    
    if (MEA_reinit_start)
        return MEA_OK;

    /*copy the the block of PM from DB DDR */
    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_PM_TYPE) == MEA_FALSE)
        return MEA_OK;
    
    
    
    
    if (!MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TYPE])
        return MEA_OK;

    if (MEA_Counters_pm_HW_BLK == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_Counters_pm_HW_BLK failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (block >=MEA_COUNTERS_PM_MAX_OF_BLK)
        return MEA_ERROR;

    
    
  
    
    base_address = MEA_COUNTERS_PM_BASE_ADD + (block *(sizeof(mea_PM_HW_data_dbt)*MEA_COUNTERS_PM_BLK_READ));
    
    for (pmId = 0; pmId < MEA_COUNTERS_PM_BLK_READ; pmId++){
        MEA_DDR_Lock();
        MEA_DB_DDR_HW_ACCESS_ENABLE
        if (MEA_OS_Device_DATA_DDR_RW(MEA_MODULE_DATA_DDR, 0,/*0 read 1- write */
            base_address,
            MEA_ADDRES_DDR_BYTE*MEA_PM_BASE_ADDRES_DDR_WIDTH,
            &MEA_Counters_pm_HW_BLK[pmId].regs[0]
            ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR MODULE_DATA_DDR  Write to DDR \n");
            MEA_DB_DDR_HW_ACCESS_DISABLE
            MEA_DDR_Unlock();
            return MEA_ERROR;
        }
        base_address += (MEA_ADDRES_DDR_BYTE*MEA_PM_BASE_ADDRES_DDR_WIDTH);
        MEA_DB_DDR_HW_ACCESS_DISABLE
        MEA_DDR_Unlock();
    }
    
        
//        MEA_OS_memcpy(&MEA_Counters_pm_HW_BLK, &base_address, (sizeof(mea_PM_HW_data_dbt)*MEA_COUNTERS_PM_BLK_READ));
    MEA_Counters_BLK_ID = block;

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_PMs>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_PMs (MEA_Unit_t                unit) {
   
	MEA_Bool found;
	MEA_PmId_t pm_id;

	MEA_API_LOG

    if(MEA_reinit_start)
        return MEA_OK;

    /************************************************************************/
    /* Get the state of WRITE_ENGINE_STATE                                  */
    /************************************************************************/
    if (mea_drv_Get_WRITE_ENGINE_STATE(unit) != MEA_OK){
        return MEA_ERROR;
    }

    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_PM_TYPE) == MEA_TRUE){
        return MEA_OK;
    }

    if (!MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TYPE]){
        return MEA_OK;
    }

    if ( mea_drv_get_first_pm_id(&found, &pm_id) != MEA_OK ){
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_get_first_pm_id failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

	if ( !found )
	{
		return MEA_OK;
	}

    /* Collect the Service MEA_CPU_PM_ID  Counters */
    if (MEA_API_Collect_Counters_PM(unit,
                                    pm_id,
                                    NULL)==MEA_ERROR){
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Collect PM Counters for pmId=%d failed\n",
                          __FUNCTION__,pm_id);
        return MEA_ERROR;
	}

	while ( found )
	{
		if ( mea_drv_get_next_pm_id(&found, &pm_id) != MEA_OK )
		{
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_drv_get_first_pm_id failed\n",
							  __FUNCTION__);
			return MEA_ERROR;
		}

		if ( found )
		{
			if (MEA_API_Collect_Counters_PM(unit,
											pm_id,
											NULL)==MEA_ERROR){
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								  "%s - Collect PM Counters for pmId=%d failed\n",
								  __FUNCTION__,pm_id);
				return MEA_ERROR;
			}
		}
	}

    /* Return to Callers */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_PM>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_PM  (MEA_Unit_t           unit,
                                         MEA_PmId_t           pmId,
                                         MEA_Counters_PM_dbt* entry) {



#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool pmId_exist = MEA_FALSE;
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS


    /* Check valid parameter :  pmId */
    if (MEA_API_IsExist_PmId(unit,
                             pmId,
                             &pmId_exist,
                             NULL) != MEA_OK)  {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
                              __FUNCTION__,pmId);
        return MEA_ERROR;
    }

    if (pmId_exist == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - pmId %d not define\n",
                          __FUNCTION__,pmId);
        return MEA_ERROR;  
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (!MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TYPE])
        return MEA_OK;
      
    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_PM_TYPE) == MEA_FALSE)
    {

        if (mea_drv_Collect_Counters_PM(unit,
            pmId,
            &(MEA_Counters_pm_value[pmId])) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Collect_Counters_PM failed "
                "(pmId=%d)\n",
                __FUNCTION__, pmId);
            return MEA_ERROR;

        }
    
     if ( entry != NULL) {
        MEA_OS_memcpy(entry,
            &(MEA_Counters_pm_value[pmId]),
                      sizeof(*entry));
     }

    }
    





    /* Return to Callers */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Counters_PM>                                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_PM     (MEA_Unit_t                unit,
                                        MEA_PmId_t                pmId,
                                        MEA_Counters_PM_dbt*      entry) {

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool pmId_exist = MEA_FALSE;
#endif

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    /* Check valid parameter :  pmId */
    if (MEA_API_IsExist_PmId(unit,
                             pmId,
                             &pmId_exist,
                             NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
                              __FUNCTION__,pmId);
        return MEA_ERROR;
    }

    if (pmId_exist == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - pmId %d not define\n",
                          __FUNCTION__,pmId);
        return MEA_ERROR;  
    }

    /* Check valid parameter :  entry */
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry==NULL (pmId=%d)\n",
                          __FUNCTION__,pmId);
        return MEA_ERROR;  
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_PM_TYPE) == MEA_FALSE)
    {
        /* Update output parameters */
        MEA_OS_memcpy(entry,
            &(MEA_Counters_pm_value[pmId]),
            sizeof(*entry));

    }  else{
        /*check that the pm is on the range of the block */
        mea_drv_get_Counters_PM_fromblock(pmId, entry);

    }


   /* Return to Callers */
   return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Clear_Counters_PMs>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_PMs   (MEA_Unit_t                unit) {

	MEA_Bool found;
	MEA_PmId_t pm_id;
#if 0
    MEA_Uint32 base_address;
    MEA_Uint32  block;
    MEA_Uint32 i;
#endif
   // MEA_ULong_t base_address;
    //mea_PM_HW_data_dbt  entry;

	MEA_API_LOG
        if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_PM_TYPE) == MEA_FALSE){
            if (mea_drv_get_first_pm_id(&found, &pm_id) != MEA_OK)
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_get_first_pm_id failed\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }

            if (!found)
            {
                return MEA_OK;
            }

            /* Clear the Service MEA_CPU_PM_ID  Counters */
            if (MEA_API_Clear_Counters_PM(unit,
                pm_id) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Clear PM Counters for pmId=%d failed\n",
                    __FUNCTION__, pm_id);
                return MEA_ERROR;
            }



            while (found)
            {
                if (mea_drv_get_next_pm_id(&found, &pm_id) != MEA_OK)
                {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_drv_get_first_pm_id failed\n",
                        __FUNCTION__);
                    return MEA_ERROR;
                }

                if (found)
                {
                    if (MEA_API_Clear_Counters_PM(unit,
                        pm_id) != MEA_OK) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - Clear Service Counters for pmId=%d failed\n",
                            __FUNCTION__, pm_id);
                        return MEA_ERROR;
                    }
                }
            }
        }
        else {
            if (MEA_API_Clear_Counters_PMs_Block(unit, 0, MEA_MAX_NUM_OF_PM_ID - 1) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Clear MEA_API_Clear_Counters_PMs_Block  failed\n",
                    __FUNCTION__ );
                return MEA_ERROR;
            }
        
        }

    /* Return to Callers */
    return MEA_OK;

}



MEA_Status MEA_API_Clear_Counters_PMs_Block(MEA_Unit_t                unit,
                                            MEA_PmId_t    from_pmId,
                                            MEA_PmId_t    to_pmId)
{
    
    
    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_PM_TYPE) == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - This API not work on support on this HW \n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (!MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TYPE])
        return MEA_OK;
    
    return MEA_drv_Clear_Counters_PMs_Block(unit, from_pmId, to_pmId);
}

static MEA_Status MEA_drv_Clear_Counters_PMs_Block(MEA_Unit_t  unit, MEA_PmId_t    from_pmId, MEA_PmId_t    to_pmId)
{

    //MEA_Bool found;
    MEA_PmId_t pm_id;
    MEA_Uint32 base_address;
    
    MEA_ind_write_t ind_write;
    mea_pmClear_cmd_t       Hw_Value;
    MEA_Uint32             retVal[16];
    //int i;
    

    // MEA_ULong_t base_address;
    //mea_PM_HW_data_dbt  entry;

    MEA_API_LOG
        if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_PM_TYPE) == MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - this API not work on support on this HW \n", __FUNCTION__);
            return MEA_ERROR;
        } else {
            if (from_pmId >= MEA_MAX_NUM_OF_PM_ID ||
                to_pmId >= MEA_MAX_NUM_OF_PM_ID) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The pm is out of range \n", __FUNCTION__);
                return MEA_ERROR;
            }
            if (to_pmId < from_pmId){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - the from_pmId is bigger then to_pmId \n", __FUNCTION__);
                return MEA_ERROR;
            }
            
            //********************************    
            // Add the bm to block
            MEA_OS_memset(&Hw_Value, 0, sizeof(Hw_Value));

            ind_write.tableType = ENET_BM_TBL_TYP_CNT_PM_READ_BYTE;
            ind_write.tableOffset = 0;

            ind_write.cmdReg = MEA_BM_IA_CMD;
            ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
            ind_write.statusReg = MEA_BM_IA_STAT;
            ind_write.statusMask = MEA_BM_IA_STAT_MASK;
            ind_write.tableAddrReg = MEA_BM_IA_ADDR;
            ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
            ind_write.funcParam1 = (MEA_funcParam)unit;
            ind_write.funcParam2 = (MEA_funcParam)((long)0);
            ind_write.funcParam3 = (MEA_funcParam)0/*hwId*/;
            ind_write.funcParam4 = (MEA_funcParam)&Hw_Value;


            ind_write.writeEntry = (MEA_FUNCPTR)mea_PmClear_UpdateHwEntry_Cmd;
            
            Hw_Value.from_pm = from_pmId;
            Hw_Value.to_pm = to_pmId;
            Hw_Value.en_blk = MEA_TRUE;

            if (MEA_API_WriteIndirect(unit, &ind_write, MEA_MODULE_BM) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed  PM block <%d:%d>\n",
                    __FUNCTION__, ind_write.tableType, MEA_MODULE_BM,from_pmId,to_pmId);
                return MEA_ERROR;
            }

                //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "PM blk %d clear \n", block);
                
                MEA_OS_memset(&retVal, 0, sizeof(retVal));
                
                for (pm_id = from_pmId; pm_id <=to_pmId; pm_id++){
                    base_address = MEA_COUNTERS_PM_BASE_ADD + ((pm_id * MEA_PM_BASE_ADDRES_DDR_WIDTH*4) );
                    
                     MEA_DRV_WriteDDR_DATA_BLOCK(unit, pm_id, base_address, &retVal[0], MEA_PM_BASE_ADDRES_DDR_WIDTH, DDR_PMS);
                
                }
               

                //********************************    
                // remove the pm to block

                ind_write.writeEntry = (MEA_FUNCPTR)mea_PmClear_UpdateHwEntry_Cmd;
                Hw_Value.from_pm = from_pmId;
                Hw_Value.to_pm = to_pmId;
                Hw_Value.en_blk = MEA_FALSE;

                if (MEA_API_WriteIndirect(unit, &ind_write, MEA_MODULE_BM) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed PM open \n",
                        __FUNCTION__, ind_write.tableType, MEA_MODULE_BM);
                    return MEA_ERROR;
                }
        
        
        }

        /* Return to Callers */
        return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Clear_Counters_PM>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_PM    (MEA_Unit_t    unit,
                                         MEA_PmId_t    pmId) {

    MEA_API_LOG
//        MEA_ULong_t base_address;
    //mea_PM_HW_data_dbt  entry;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (pmId >= MEA_MAX_NUM_OF_PM_ID) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - pmId (%d) >= max allowed (%d) \n",
                         __FUNCTION__,pmId,MEA_MAX_NUM_OF_PM_ID);
       return MEA_ERROR; 
    }

    /* Note: PM ID can be not exist , because we clean counter
             before we add the it to service */


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (!MEA_Counters_Type_Enable[MEA_COUNTERS_PMS_TYPE])
        return MEA_OK;

    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(unit, MEA_DB_PM_TYPE) == MEA_FALSE){

        /* Collect PM Counters from HW to cause HW counters zeros */
        if (mea_drv_Collect_Counters_PM(unit,
            pmId,
            NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Collect_Counters_PM failed "
                "(pmId=%d)\n",
                __FUNCTION__, pmId);
            return MEA_ERROR;

        }


        /* Zeors the SW PM counters db */
        MEA_OS_memset(&(MEA_Counters_pm_value[pmId]),
            0,
            sizeof(MEA_Counters_pm_value[0]));
    }
    else {
        /* TBD need to make not update */
        MEA_drv_Clear_Counters_PMs_Block(unit, pmId, pmId);

    }

     


    /* Return to Callers */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_IsExist_PmId>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_PmId(MEA_Unit_t                 unit,
                                MEA_PmId_t                 pmId,
                                MEA_Bool                  *exist,
                                MEA_Uint32                *count) {

#if 0
    MEA_Service_t db_serviceId;
    MEA_ULong_t    db_cookie;
#endif

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid exist parameter */
	if (exist == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist == NULL \n",__FUNCTION__);
       return MEA_ERROR; 
    }

    /* check valid pmId  */
    if (pmId >= MEA_MAX_NUM_OF_PM_ID) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - pmId (%d) >= max allowed (%d) \n",
                         __FUNCTION__,pmId,MEA_MAX_NUM_OF_PM_ID);
       return MEA_ERROR; 
    }



#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* set default output */
    *exist = MEA_FALSE;
    if (count) {
       *count = 0;
    }

	if ( !MEA_Counters_PM_Table[pmId].valid )
	{
		return MEA_OK;
	}

	if (count) 
		*count = MEA_Counters_PM_Table[pmId].num_of_owners;

	*exist = MEA_TRUE;

	
#if 0
    /* Check for CPU PM ID */
    if (pmId == MEA_CPU_PM_ID) {
       *exist = MEA_TRUE;
       if (count) {
         *count = 1;
       }
       return MEA_OK;
    }

    /* Get the first service */
    if (MEA_API_GetFirst_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_GetFirst_Service failed\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    while (db_cookie != 0) { 
   
	    MEA_Service_Entry_Data_dbt service_data;


        if (MEA_API_Get_Service(unit,
                                db_serviceId,
				                NULL,          /* key           */
				                &service_data,
							    NULL,          /*OutPorts_Entry */
							    NULL,          /* Policer_Entry */
							    NULL           /* EHP_Entry     */
                                ) != MEA_OK) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Get_Service failed (serviceId=%d)\n",
                              __FUNCTION__,db_serviceId);
            return MEA_ERROR;
        }

        /* compare the key of this serviceId */
        if (pmId == service_data.pmId) {
            *exist = MEA_TRUE;
            if (count) {
               (*count)++;
            }
	    }

        /* get the next service Id */
        if (MEA_API_GetNext_Service(MEA_UNIT_0,&db_serviceId,&db_cookie) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_GetNext_Service failed\n",
                              __FUNCTION__);
            return MEA_ERROR; 
       }
    }
#endif

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_Queues>                              */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_Queues (MEA_Unit_t                unit) {
   
    

	ENET_QueueId_t queueId;
	ENET_Bool      retVal;
	ENET_Queue_dbt QueueDbt;

	MEA_API_LOG

    if(MEA_reinit_start)
            return MEA_OK;

	if(ENET_GetFirst_Queue  (unit,
							 &queueId,
                             &QueueDbt,
                             &retVal)!=ENET_OK) 
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
        "%s - Faild to get first Queue\n",
                     __FUNCTION__);
		return MEA_ERROR;
	}

	if (!retVal)
	{
		return MEA_OK;
	}

	do {
        if (MEA_reinit_start)
            return MEA_OK;
       /* Collect the Queue Counters */
	    if (MEA_API_Collect_Counters_Queue(unit,queueId,NULL)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect  Counters for queue=%d failed\n",
                              __FUNCTION__,queueId);
            return MEA_ERROR;
        }

		if( ENET_GetNext_Queue   (unit,
								 &queueId,
                                 &QueueDbt,
                                 &retVal)!=ENET_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - fail to get next Queue\n",
                             __FUNCTION__);
			return MEA_ERROR;
		}
	}while (retVal);


    /* Return to Callers */
    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_Queue>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#define MEA_QUEUE_MC_MQS_ALLOW 1

MEA_Status MEA_API_Collect_Counters_Queue (MEA_Unit_t             unit,
                                           ENET_QueueId_t           queue,
                                           MEA_Counters_Queue_dbt* entry) {

    MEA_ind_read_t ind_read;
	MEA_Uint32     read_data[4];
    MEA_Uint32     val;
    MEA_db_HwUnit_t hwUnit;
    MEA_Uint32                  priority;
    time_t t1, t2;

    MEA_Counters_Queue_dbt counter_entry;
	MEA_API_LOG
	
	if (b_bist_test){ 
	 return MEA_OK;
	}
    
    if (MEA_reinit_start)
        return MEA_OK;

   MEA_DRV_GET_HW_UNIT(MEA_UNIT_0,hwUnit,return MEA_ERROR;)

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

	  if (!IS_QUEUE_ID_VALID(queue))
	  {
		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			  "%s - Queue Id invalid (queue='%d')\n",
					__FUNCTION__,queue);
		  return ENET_ERROR;
	  }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    MEA_OS_memset(&counter_entry, 0, sizeof(counter_entry));
    MEA_OS_memcpy(&counter_entry, &MEA_Counters_Queue_Table[queue], sizeof(counter_entry));
 
    if (MEA_QUEUE_MC_MQS_SUPPORT  && MEA_QUEUE_MC_MQS_ALLOW )
    {
        ind_read.tableType		=   ENET_BM_TBL_TYP_CLUSTER_BC_MQS;
        ind_read.cmdReg			=	MEA_BM_IA_CMD;      
        ind_read.cmdMask		=	MEA_BM_IA_CMD_MASK;      
        ind_read.statusReg		=	MEA_BM_IA_STAT;   
        ind_read.statusMask		=	MEA_BM_IA_STAT_MASK;   
        ind_read.tableAddrReg	=	MEA_BM_IA_ADDR;
        ind_read.tableAddrMask	=	MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_read.read_data	    =   read_data;
        
        for (priority=0;priority<ENET_PLAT_QUEUE_NUM_OF_PRI_Q;priority++) 
        {

            ind_read.tableOffset	= (ENET_PLAT_QUEUE_NUM_OF_PRI_Q*(MEA_Uint32)enet_cluster_external2internal(queue))+priority  + (/*offset*/mea_drv_Get_DeviceInfo_NumOfClusters(unit,hwUnit)*ENET_PLAT_QUEUE_NUM_OF_PRI_Q) ;

#if 1
        if (MEA_LowLevel_ReadIndirect(unit,&ind_read,
            MEA_NUM_OF_ELEMENTS(read_data),
            MEA_MODULE_BM,
            globalMemoryMode,MEA_MEMORY_READ_SUM)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ReadIndirect from tableType %x module BM failed "
                    "(queue=%d)\n",
                    __FUNCTION__,ind_read.tableType,queue);

                return MEA_ERROR;
        }
#else
            if (MEA_API_ReadIndirect(MEA_UNIT_0,
                &ind_read,
                MEA_NUM_OF_ELEMENTS(read_data),
                MEA_MODULE_BM) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ReadIndirect from tableType %x module BM failed "
                    "(queue=%d)\n",
                    __FUNCTION__, ind_read.tableType, queue);
                return MEA_ERROR;
            }



#endif




        val= read_data[0];

        MEA_OS_UPDATE_COUNTER(MEA_Counters_Queue_Table[queue].mc_MQS_DRP[priority].val ,val);

        }


    }
    

    if (MEA_QUEUE_COUNT_SUPPORT)
    {
        ind_read.tableType     = ENET_BM_TBL_TYP_QUEUE_COUNT;
        ind_read.cmdReg        = MEA_BM_IA_CMD;
        ind_read.cmdMask       = MEA_BM_IA_CMD_MASK;
        ind_read.statusReg     = MEA_BM_IA_STAT;
        ind_read.statusMask    = MEA_BM_IA_STAT_MASK;
        ind_read.tableAddrReg  = MEA_BM_IA_ADDR;
        ind_read.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_read.read_data     = read_data;

        MEA_Counters_Queue_Table[queue].Tx_TotalPacketRate.val = 0;
        for (priority = 0; priority < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; priority++) {

            t1 = MEA_Counters_Queue_Table[queue].t1[priority];

            ind_read.tableOffset = (ENET_PLAT_QUEUE_NUM_OF_PRI_Q*(MEA_Uint32)enet_cluster_external2internal(queue)) + priority ;

#if 1
            if (MEA_LowLevel_ReadIndirect(unit, &ind_read,
                MEA_NUM_OF_ELEMENTS(read_data),
                MEA_MODULE_BM,
                globalMemoryMode, MEA_MEMORY_READ_SUM) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ReadIndirect from tableType %x module BM failed "
                    "(queue=%d)\n",
                    __FUNCTION__, ind_read.tableType, queue);

                return MEA_ERROR;
            }


#else
            /************************************************************************/
            /*                                                                      */
            /************************************************************************/
            if (MEA_API_ReadIndirect(MEA_UNIT_0,
                &ind_read,
                MEA_NUM_OF_ELEMENTS(read_data),
                MEA_MODULE_BM) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ReadIndirect from tableType %x module BM failed "
                    "(queue=%d)\n",
                    __FUNCTION__, ind_read.tableType, queue);
                return MEA_ERROR;
            }
#endif

            /************************************************************************/
            /*                                                                      */
            /************************************************************************/








            val = read_data[0];
            MEA_OS_UPDATE_COUNTER(MEA_Counters_Queue_Table[queue].forward_packet[priority].val, val);
            val = read_data[1];
            MEA_OS_UPDATE_COUNTER(MEA_Counters_Queue_Table[queue].forward_Byte[priority].val,val);
            val = read_data[2];
            MEA_OS_UPDATE_COUNTER(MEA_Counters_Queue_Table[queue].ingerss_packet[priority].val,val);
            val = read_data[3];
            MEA_OS_UPDATE_COUNTER(MEA_Counters_Queue_Table[queue].ingerss_Byte[priority].val,val);


            if (val == 0) {
                MEA_Counters_Queue_Table[queue].nopacket[priority] = MEA_TRUE;
            }
            else {
                MEA_Counters_Queue_Table[queue].nopacket[priority] = MEA_FALSE;
            }

            
            t2 = time(&MEA_Counters_Queue_Table[queue].t1[priority]);

            MEA_Counters_Queue_Table[queue].count[priority]++;
            if (MEA_Counters_Queue_Table[queue].count[priority] > ENET_CLUSTER_RATE_SEC || MEA_Counters_Queue_Table[queue].count==0) {
                MEA_Counters_Queue_Table[queue].count[priority] = 1;
                MEA_Counters_Queue_Table[queue].Tx_sumPacket[priority].val = 0;
            }

            if ((t2 > t1) && (t1 != 0) ) 
            {
                if (MEA_Counters_Queue_Table[queue].forward_packet[priority].val != 0) 
                {
                    MEA_Uint64 tmp;

                    tmp.val = MEA_Counters_Queue_Table[queue].forward_packet[priority].val -
                              counter_entry.forward_packet[priority].val;
                    MEA_Counters_Queue_Table[queue].Tx_sumPacket[priority].val += (tmp.val / (t2 - t1));
                    if (MEA_Counters_Queue_Table[queue].count[priority] > 0) 
                    {
                        MEA_Counters_Queue_Table[queue].Tx_ratePacket[priority].val = MEA_Counters_Queue_Table[queue].Tx_sumPacket[priority].val / MEA_Counters_Queue_Table[queue].count[priority];
                        MEA_Counters_Queue_Table[queue].Tx_TotalPacketRate.val += MEA_Counters_Queue_Table[queue].Tx_ratePacket[priority].val;
                    }
                }
            }
        }
    }



    /* Update the output Parameter if Supply */
    if ( entry != NULL) {
        MEA_OS_memcpy(entry,
                      &(MEA_Counters_Queue_Table[queue]),
                      sizeof(*entry));
    }

    /* Return to Callers */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Counters_Queue>                                   */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_Queue (MEA_Unit_t             unit,
                                       ENET_QueueId_t           queue,
                                       MEA_Counters_Queue_dbt* entry) {


	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

	  if (!IS_QUEUE_ID_VALID(queue))
	  {
		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			  "%s - Queue Id invalid (queue='%d')\n",
					__FUNCTION__,queue);
		  return ENET_ERROR;
	  }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry==NULL (queue=%d)\n",
                          __FUNCTION__,queue);
        return MEA_ERROR;  
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   /* Update output parameters */
   MEA_OS_memcpy(entry,&(MEA_Counters_Queue_Table[queue]), sizeof(*entry));

   /* Return to Callers */
   return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Clear_Counters_Queues>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_Queues   (MEA_Unit_t                unit) {

	ENET_QueueId_t queueId;
	ENET_Bool      found;

	MEA_API_LOG

    if(ENET_GetFirst_Queue  (unit,&queueId,NULL,&found)!=ENET_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Failed to get first Queue\n",
                          __FUNCTION__);
		return MEA_ERROR;
	}

    while(found) {

		/* clear the Queue Counters */
		if (MEA_API_Clear_Counters_Queue(unit,queueId)==MEA_ERROR){
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - Clear Port Counters for queueId=%d failed\n",
							  __FUNCTION__,queueId);
            return MEA_ERROR;
		}
        if( ENET_GetNext_Queue   (unit,&queueId,NULL,&found)!=ENET_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Failed to get next Queue\n",
                             __FUNCTION__);
			return MEA_ERROR;
		}
	}


    /* Return to Callers */
    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Clear_Counters_Queue>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_Queue    (MEA_Unit_t    unit,
                                            ENET_QueueId_t queue) {

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

	  if (!IS_QUEUE_ID_VALID(queue))
	  {
		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			  "%s - Queue Id invalid (queue='%d')\n",
					__FUNCTION__,queue);
		  return ENET_ERROR;
	  }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

   
    /* Collect Queue Counters from HW to cause HW counters zeros */
    if (MEA_API_Collect_Counters_Queue(unit,queue,NULL) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Collect_Counters_Queue failed (queue=%d)\n",
                          __FUNCTION__,queue);
        return MEA_ERROR;
      
    }

    /* Zeors the SW port counters db */
    MEA_OS_memset(&(MEA_Counters_Queue_Table[queue]),
                  0,
                  sizeof(MEA_Counters_Queue_Table[0]));


    /* Return to Callers */
    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_BMs>                                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_BMs (MEA_Unit_t                unit) {
   

	MEA_API_LOG
    
	return MEA_API_Collect_Counters_BM  (unit,NULL);

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Collect_Counters_BM>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_BM  (MEA_Unit_t           unit,
                                         MEA_Counters_BM_dbt* entry) {

    MEA_ind_read_t ind_read;
	MEA_Uint32     read_data[1];

	MEA_API_LOG
    
      if(MEA_reinit_start)
            return MEA_OK;

 	ind_read.tableType		=    ENET_BM_TBL_TYP_BUF_RESOURCE_LIMIT_DROP_PACKET_CNT;
	ind_read.tableOffset	=   0;
	ind_read.cmdReg			=	MEA_BM_IA_CMD;      
	ind_read.cmdMask		=	MEA_BM_IA_CMD_MASK;      
	ind_read.statusReg		=	MEA_BM_IA_STAT;   
	ind_read.statusMask		=	MEA_BM_IA_STAT_MASK;   
	ind_read.tableAddrReg	=	MEA_BM_IA_ADDR;
	ind_read.tableAddrMask	=	MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_read.read_data	    =   read_data;
	
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(read_data),
                             MEA_MODULE_BM,
                             globalMemoryMode,MEA_MEMORY_READ_COMPARE)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ReadIndirect from tableType %x module BM failed\n",
                          __FUNCTION__,ind_read.tableType);

		return MEA_ERROR;
    }

    MEA_OS_UPDATE_COUNTER(MEA_Counters_BM.BMBRL_drop_packets_counters.val ,
				          (read_data[0] & MEA_BM_TBL_TYP_BRL_DROP_PACKET_CNT_MASK));


 	ind_read.tableType		=    ENET_BM_TBL_TYP_DESC_RESOURCE_LIMIT_DROP_PACKET_CNT;
	ind_read.tableOffset	=   0;
	ind_read.cmdReg			=	MEA_BM_IA_CMD;      
	ind_read.cmdMask		=	MEA_BM_IA_CMD_MASK;      
	ind_read.statusReg		=	MEA_BM_IA_STAT;   
	ind_read.statusMask		=	MEA_BM_IA_STAT_MASK;   
	ind_read.tableAddrReg	=	MEA_BM_IA_ADDR;
	ind_read.tableAddrMask	=	MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_read.read_data	    =   read_data;
	
	if (MEA_LowLevel_ReadIndirect(MEA_UNIT_0,
                             &ind_read,
                             MEA_NUM_OF_ELEMENTS(read_data),
                             MEA_MODULE_BM,
                             globalMemoryMode,MEA_MEMORY_READ_COMPARE)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ReadIndirect from tableType %x module BM failed\n",
                          __FUNCTION__,ind_read.tableType);

		return MEA_ERROR;
    }

    MEA_OS_UPDATE_COUNTER(MEA_Counters_BM.BMDRL_drop_packets_counters.val ,
				          (read_data[0] & MEA_BM_TBL_TYP_DRL_DROP_PACKET_CNT_MASK));


    /* Update the output Parameter if Supply */
    if ( entry != NULL) {
        MEA_OS_memcpy(entry,
                      &(MEA_Counters_BM),
                      sizeof(*entry));
    }

    /* Return to Callers */
    return MEA_OK;

}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Current_Counters_BM>                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Current_Counters_BM  (MEA_Unit_t           unit,
                                         MEA_Current_Counters_BM_dbt* entry) {
    MEA_ind_read_t ind_read;
	MEA_Uint32     read_data[1];
    MEA_Uint32     i;


	MEA_API_LOG

        //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s -DBG  not support\n",__FUNCTION__);
return MEA_OK;

	if (entry == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - entry == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

#if 1
    /* tx fifo counters */

 	ind_read.tableType		=   ENET_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS;
	ind_read.cmdReg			=	MEA_BM_IA_CMD;      
	ind_read.cmdMask		=	MEA_BM_IA_CMD_MASK;      
	ind_read.statusReg		=	MEA_BM_IA_STAT;   
	ind_read.statusMask		=	MEA_BM_IA_STAT_MASK;   
	ind_read.tableAddrReg	=	MEA_BM_IA_ADDR;
	ind_read.tableAddrMask	=	MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_read.read_data	    =   read_data;

    for (i=0;i<ENET_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_LENGTH;i++) {

	    ind_read.tableOffset	=   i;

 	    if (MEA_API_ReadIndirect(MEA_UNIT_0,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(read_data),
                                 MEA_MODULE_BM
                                )!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ReadIndirect from tableType %d offset %d "
                              "module BM failed\n",
                              __FUNCTION__,ind_read.tableType,ind_read.tableOffset);
            return MEA_ERROR;
        }

        switch (i) {

        case MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_FULL :
          entry->tx_engine.full               = read_data[0];
          break;
        case MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_EMPTY :
          entry->tx_engine.empty              = read_data[0];
          break;
        case MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_MAX_EMPTY_LATENCY :
          entry->tx_engine.max_empty_latency  = read_data[0];
          break;
        case MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_MAX_BUFFER_LATENCY :
          entry->tx_engine.max_buffer_latency = read_data[0];
          break;
        case MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_AVERAGE_FIFO_SIZE :
          entry->tx_engine.average_fifo_size  = read_data[0];
          break;
        default: 
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - Unknown i value (%d) \n",__FUNCTION__,i);
          return MEA_ERROR;
        }


    }

    /* write fifo counters */
#if 0
 	ind_read.tableType		=   ENET_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS;
	ind_read.cmdReg			=	MEA_BM_IA_CMD;      
	ind_read.cmdMask		=	MEA_BM_IA_CMD_MASK;      
	ind_read.statusReg		=	MEA_BM_IA_STAT;   
	ind_read.statusMask		=	MEA_BM_IA_STAT_MASK;   
	ind_read.tableAddrReg	=	MEA_BM_IA_ADDR;
	ind_read.tableAddrMask	=	MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_read.read_data	    =   read_data;
#endif
    for (i=0;i<ENET_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS_LENGTH;i++) {
	    ind_read.tableOffset	=   i;
#if 0
 	    if (MEA_API_ReadIndirect(MEA_UNIT_0,
                                 &ind_read,
                                 MEA_NUM_OF_ELEMENTS(read_data),
                                 MEA_MODULE_BM
                                )!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ReadIndirect from tableType %d offset %d "
                              "module BM failed\n",
                              __FUNCTION__,ind_read.tableType,ind_read.tableOffset);
            return MEA_ERROR;
        }
#endif

        switch (i) {
   
        case MEA_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS_OFFSET_FULL :
          entry->write_fifo.full               = read_data[0];
          break;
        case MEA_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS_OFFSET_MAX_BUFFER_LATENCY :
          entry->write_fifo.max_buffer_latency = read_data[0];
          break;
        case MEA_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS_OFFSET_AVERAGE_FIFO_SIZE :
          entry->write_fifo.average_fifo_size  = read_data[0];
          break;
        default: 
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - Unknown i value (%d) \n",__FUNCTION__,i);
          return MEA_ERROR;
        }




    }




    /* current pending broadcast packets */
    read_data[0] = MEA_API_ReadReg(MEA_UNIT_0,
                          MEA_BM_CURRENT_PENDING_BROADCAST_PACKETS_REG,
                          MEA_MODULE_BM);

    entry->pending_broadcast_packets =
       ((read_data[0] &(MEA_BM_CURRENT_PENDING_BROADCAST_PACKETS_MASK))
         >> MEA_OS_calc_shift_from_mask(MEA_BM_CURRENT_PENDING_BROADCAST_PACKETS_MASK));

    /* current used buffers */
    read_data[0] = MEA_API_ReadReg(MEA_UNIT_0,
                                   (globalMemoryMode == MEA_MODE_REGULAR_ENET3000)
                                   ? MEA_BM_CURRENT_USED_BUFFERS_REG
                                   : MEA_BM_CURRENT_USED_BUFFERS_AND_DESCRIPTOR_REG,
                                   MEA_MODULE_BM);
    entry->used_buffers =  
       ((read_data[0] &(MEA_BM_CURRENT_USED_BUFFERS_MASK))
         >> MEA_OS_calc_shift_from_mask(MEA_BM_CURRENT_USED_BUFFERS_MASK));

    /* current used descriptors */
    read_data[0] = MEA_API_ReadReg(MEA_UNIT_0,
                                   (globalMemoryMode == MEA_MODE_REGULAR_ENET3000)
                                   ? MEA_BM_CURRENT_USED_DESCRIPTORS_REG
                                   : MEA_BM_CURRENT_USED_BUFFERS_AND_DESCRIPTOR_REG,
                                   MEA_MODULE_BM);
    entry->used_descriptors =  
       ((read_data[0] &(MEA_BM_CURRENT_USED_DESCRIPTORS_MASK))
         >> MEA_OS_calc_shift_from_mask(MEA_BM_CURRENT_USED_DESCRIPTORS_MASK));

#endif

    /* Return to Callers */
    return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_Counters_BM>                                      */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_BM      (MEA_Unit_t             unit,
                                         MEA_Counters_BM_dbt* entry) {

	MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry==NULL \n",
                          __FUNCTION__);
        return MEA_ERROR;  
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



   /* Update output parameters */
   MEA_OS_memcpy(entry,
                 &(MEA_Counters_BM),
                 sizeof(*entry));

   /* Return to Callers */
   return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Clear_Counters_BM>                                    */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_BM   (MEA_Unit_t                unit) {


    /* Clear the HW Counters */
    if (MEA_API_Collect_Counters_BMs (unit) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_API_Collect_Counters_BMs failed\n",
                         __FUNCTION__);
    }

    /* Zeors the SW ports counters db - for all table */
    MEA_OS_memset(&(MEA_Counters_BM),
                  0,
                  sizeof(MEA_Counters_BM));

    /* Return to Callers */
    return MEA_OK;

}

MEA_Status mea_drv_Get_Cunters_PM_Type_Entry(MEA_Unit_t                unit_i,
                                             MEA_db_HwUnit_t           hwUnit_i,
                                             MEA_pm_type_te            pm_type_i,
                                             MEA_Counters_PM_Type_dbt *entry_po) 
{

    MEA_Uint16 index;

    if (hwUnit_i >= mea_drv_num_of_hw_units) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - hwUnit_i %d >= max value %d \n",
                          __FUNCTION__,hwUnit_i,mea_drv_num_of_hw_units);
        return MEA_ERROR;
    }

    if (pm_type_i >= MEA_PM_TYPE_LAST) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - pm_type_i %d >= max value %d \n",
                          __FUNCTION__,pm_type_i,MEA_PM_TYPE_LAST);
        return MEA_ERROR;
    }

    index = MEA_COUNTERS_PM_TYPE_TABLE_INEDX(hwUnit_i,pm_type_i);

    MEA_OS_memcpy(entry_po,
                  &(MEA_Counters_PM_Type_Table[index]),
                  sizeof(*entry_po));

    return MEA_OK;


}

#if 1
// this was on old bonding need to delete
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Get_Bonding_PM>                                      */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Bonding_PM(MEA_Unit_t                    unit,
                                  MEA_Uint16                    groupId)
{

    
    if(!MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Only At FRAGMENT_BONDING_SUPPORT ",__FUNCTION__);
        return MEA_ERROR;
    }
    if(groupId >7 ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Only At FRAGMENT_BONDING_SUPPORT ",__FUNCTION__);
        return MEA_ERROR;

    }

   MEA_API_Clear_Bonding_PM(unit,(MEA_PmId_t)(480 + groupId));
   MEA_API_Clear_Bonding_PM(unit,(MEA_PmId_t)(488 + groupId));
   MEA_API_Clear_Bonding_PM(unit,(MEA_PmId_t)(496 + groupId));


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Get_Bonding_PM>                                      */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Bonding_PM(MEA_Unit_t                    unit,
                                  MEA_Uint16                    groupId,
                                  MEA_Counters_PM_Bonding_dbt* entry)
{

    MEA_Counters_PM_dbt  PM_entry;
    if(!MEA_GLOBAL_FRAGMENT_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Only At FRAGMENT_BONDING_SUPPORT ",__FUNCTION__);
        return MEA_ERROR;
    }
    if(groupId >7 ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Only At FRAGMENT_BONDING_SUPPORT ",__FUNCTION__);
        return MEA_ERROR;

    }

    if(entry == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  entry = NULL",__FUNCTION__);
        return MEA_ERROR;
    }



    MEA_API_Get_Counters_PM(unit,(MEA_PmId_t)(480 + groupId),&PM_entry);
    MEA_OS_memcpy(&entry->drop_pkts[0],&PM_entry.yellow_dis_pkts,sizeof(entry->drop_pkts[0]));
    MEA_API_Get_Counters_PM(unit,(MEA_PmId_t)(488 + groupId),&PM_entry);
    MEA_OS_memcpy(&entry->drop_pkts[1],&PM_entry.yellow_dis_pkts,sizeof(entry->drop_pkts[1]));
    MEA_API_Get_Counters_PM(unit,(MEA_PmId_t)(496 + groupId),&PM_entry);
    MEA_OS_memcpy(&entry->drop_pkts[2],&PM_entry.yellow_dis_pkts,sizeof(entry->drop_pkts[2]));






    return MEA_OK;
}
#endif


