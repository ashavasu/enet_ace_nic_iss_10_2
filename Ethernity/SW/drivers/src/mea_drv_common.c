/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*************************************************************************************/
/* 	Module Name:		 mea_drv_common.c											 */ 
/*																					 */
/*  Module Description:	 MEA driver	common routines									 */
/*																					 */
/*  Date of Creation:	 12/06/2011													 */
/*																					 */
/*  Author Name:Alex weis													         */
/*************************************************************************************/ 

 

/*-------------------------------- Includes ------------------------------------------*/
#ifdef MEA_OS_LINUX
#ifndef __KERNEL__
#include <pthread.h>
#endif
#endif



#include "MEA_platform.h"
#include "mea_api.h"
#include "MEA_platform_os_memory.h"
#include "mea_drv_common.h"









/*-------------------------------- Definitions ---------------------------------------*/
#define MEA_MODULE_NAME(mea_module) \
       (((mea_module) == MEA_MODULE_BM  ) ? "BM  " : \
	    ((mea_module) == MEA_MODULE_IF )  ? "IF  " : \
	    ((mea_module) == MEA_MODULE_BM_DOWNSTREEM  ) ? "BMd  " : \
        ((mea_module) == MEA_MODULE_IF_DOWNSTREEM   ) ? "IFd  " : \
        ((mea_module) == MEA_MODULE_CPLD) ? "CPLD" : "Unknown")


/*-------------------------------- External Functions --------------------------------*/



/*-------------------------------- External Variables --------------------------------*/
MEA_Bool MEA_API_LOG_Enable = MEA_FALSE;


/*-------------------------------- Forward Declarations ------------------------------*/


static void MEA_AcessSemTake(void);
static void MEA_AcessSemGive(void);


/*-------------------------------- Local Variables -----------------------------------*/
#ifndef __KERNEL__
#if !defined(MEA_OS_ADVA_TDM) && !defined(MEA_ENV_S1_MAIN)
static MEA_Semaphore_t MEA_Access_Sem  MEA_SEMAPHORE_INIT_DEF_VALUE;
static MEA_Semaphore_t MEA_drv_API_Sem MEA_SEMAPHORE_INIT_DEF_VALUE;
static MEA_Semaphore_t MEA_drv_FWD_Sem MEA_SEMAPHORE_INIT_DEF_VALUE;
static MEA_Semaphore_t MEA_drv_IND_Sem MEA_SEMAPHORE_INIT_DEF_VALUE;
static MEA_Semaphore_t MEA_drv_DDR_Sem MEA_SEMAPHORE_INIT_DEF_VALUE;
static MEA_Semaphore_t MEA_drv_BAR0_Sem MEA_SEMAPHORE_INIT_DEF_VALUE;

#else

static pthread_mutex_t MEA_Access_Sem  = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t MEA_drv_API_Sem = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t MEA_drv_FWD_Sem = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t MEA_drv_IND_Sem = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t MEA_drv_DDR_Sem = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t MEA_drv_BAR0_Sem = PTHREAD_MUTEX_INITIALIZER;

#endif

#endif /* __KERNEL__ */

static MEA_Uint32 mea_drv_dbg_saveTableBM=8888888;

mea_memory_mode_e    globalMemoryMode = MEA_MODE_REGULAR_ENET3000;   /*MEA_MODE_REGULAR_ENET4000;*/
MEA_subSysType_te    MEA_subSysType   = MEA_SUB_SYS_TYPE_GENERAL;
MEA_Uint32           MEA_board_cs_Type;
MEA_Uint32           globalSTP_Mode          = MEA_GLOBAL_STP_MODE_DISABLE;
MEA_Uint32           globalSTP_Default_State = MEA_GLOBAL_STP_MODE_STATE_DEF_VAL;
ENET_Bool b_bist_test = MEA_FALSE; 

 MEA_Uint32                 MEA_SE_Aging_interval;
 MEA_Bool                   MEA_SE_Aging_enable;

 MEA_Uint32                 Global_Aging_interval;
 MEA_Bool                   Global_Aging_enable;
 MEA_Bool                   GlobalBm_busy = 0;

ENET_Bool mea_debug_reg_all=MEA_FALSE;

ENET_Bool mea_SSSMII_interface_support=MEA_FALSE;

MEA_Uint16          mea_global_fwd_act=0;
ENET_Bool tdm_bist_test = MEA_FALSE;
ENET_Bool mea_hash_group_flag_show =MEA_FALSE;
MEA_Uint32 mea_reinit_count =0;



ENET_Uint32 MEA_reinit_start=MEA_FALSE;
ENET_Uint32 MEA_RxEnable_stateWrite=MEA_FALSE;
ENET_Uint32 MEA_TxEnable_stateWrite=MEA_FALSE;
MEA_Uint32 glClearDDR_servie = MEA_TRUE;

MEA_Uint32   gMEA_KillEnet = 0;




MEA_Bool mea_tdm_global_debug_Show_sw_current=MEA_FALSE;
MEA_Bool mea_tdm_global_debug_sw_restart=MEA_FALSE;
MEA_Bool mea_tdm_global_enable_sw_restart=MEA_TRUE;
MEA_Bool mea_tdm_global_enable_MTG1=MEA_TRUE;



mea_module_memory_dbt device_memory[MEA_MODULE_LAST];
static MEA_ULong_t  cpld_memory = 0;  //-1
static MEA_ULong_t cpld_base_addr;








 


 mea_global_environment_dbt MEA_device_environment_info;

 

 


 MEA_PCIe_Divice_dbt mea_pcie_device[10];  
 
 char device_cpu127[50];


/*-------------------------------- Global Variables ----------------------------------*/

MEA_Bool MEA_ignore_vxlan_info = 0;


static MEA_Status mea_initSem(void)
{
	


	return MEA_OK;
}


MEA_Status MEA_API_ENV_SET(mea_global_environment_dbt *entry)
{

    if (entry == NULL){
        return MEA_ERROR;
    }

    if (entry->valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " The.Valid is not set..to TRUE\n");
        return MEA_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " USER set ENV...\n");
    MEA_OS_memcpy(&MEA_device_environment_info,entry,sizeof(mea_global_environment_dbt));

    return MEA_OK;
}


void MEA_device_environment_init(void)
{
    char    *readEnv=NULL;

    MEA_OS_memset(&MEA_device_environment_info, 0, sizeof(MEA_device_environment_info));


//    MEA_device_environment_info.PRODUCT_NAME = "";
//    MEA_device_environment_info.SYSTEM_NAME = "";



#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) 
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Read the  MEA_device_environment_info ...\n");



  
     
    readEnv = MEA_OS_getenv("PRODUCT_NAME");
    if (readEnv) {
        MEA_OS_strcpy(&MEA_device_environment_info.PRODUCT_NAME[0], (readEnv));
        MEA_device_environment_info.MEA_PRODUCT_NAME_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("SYSTEM_NAME");
    if (readEnv) {
        MEA_OS_strcpy(&MEA_device_environment_info.SYSTEM_NAME[0], (readEnv));
        MEA_device_environment_info.MEA_SYSTEM_NAME_enable = MEA_TRUE;
    }

    
    readEnv = MEA_OS_getenv("MEA_REINIT_DISABLE");
    if (readEnv) {
        MEA_device_environment_info.MEA_REINIT_DISABLE = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_REINIT_DISABLE_enable = MEA_TRUE;
    }

    

    readEnv = MEA_OS_getenv("MEA_PERIODIC_INTERVAL");
    if (readEnv){
        MEA_device_environment_info.MEA_PERIODIC_INTERVAL = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_PERIODIC_INTERVAL_enable = MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_PERIODIC_ENABLE");
    if (readEnv){
        MEA_device_environment_info.MEA_PERIODIC_ENABLE = (MEA_Bool)(MEA_OS_atoi(readEnv));

    }

	readEnv = MEA_OS_getenv("MEA_NUM_OF_SYS_MN");
	if (readEnv) {
		MEA_device_environment_info.MEA_NUM_OF_SYS_MN_enable = MEA_TRUE;
		MEA_device_environment_info.MEA_NUM_OF_SYS_MN        = (MEA_Uint32)(MEA_OS_atoiNum(readEnv));

	}


	

    readEnv = MEA_OS_getenv("MEA_ROM_ENABLE");
    if (readEnv) {
        MEA_device_environment_info.MEA_ROM_ENABLE = (MEA_Bool)(MEA_OS_atoi(readEnv));
       
    }
    
    readEnv = MEA_OS_getenv("MEA_WARM_ENABLE");
    if (readEnv) {
        MEA_device_environment_info.MEA_WARM_ENABLE_en = MEA_TRUE;
        MEA_device_environment_info.MEA_WARM_ENABLE = (MEA_Bool)(MEA_OS_atoi(readEnv));
        if (MEA_device_environment_info.MEA_WARM_ENABLE == MEA_TRUE)
        {
            MEA_API_Set_Warm_Restart();
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Set_Warm ON\n");
            /*need to disable the env disable WARM_ENABLE */
            MEA_device_environment_info.MEA_WARM_ENABLE_en = MEA_FALSE;
            MEA_device_environment_info.MEA_WARM_ENABLE = MEA_FALSE;
        }


    }

    readEnv = MEA_OS_getenv("MEA_JUMBO_TYPE_IN");
    if (readEnv){
        MEA_device_environment_info.MEA_JUMBO_TYPE_IN = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_JUMBO_TYPE_IN_enable=MEA_TRUE;
    } 
    readEnv = MEA_OS_getenv("MEA_NUM_OF_DESCRIPTORS");
    if (readEnv){
        MEA_device_environment_info.MEA_NUM_OF_DESCRIPTORS = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_NUM_OF_DESCRIPTORS_enable=MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_NUM_OF_DATA_BUFF");
    if (readEnv) {
        MEA_device_environment_info.MEA_NUM_DATA_BUFF        = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_NUM_DATA_BUFF_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_DEVICE_SYS_CLK");
    if (readEnv){
        MEA_device_environment_info.MEA_DEVICE_SYS_CLK = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_DEVICE_SYS_CLK_enable=MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_WD_DISABLE_ENV");
    if (readEnv) {
        MEA_device_environment_info.MEA_WD_DISABLE_ENV = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_WD_DISABLE_ENV_enable = MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_GW_VPLS_EN");
    if (readEnv) {
        MEA_device_environment_info.MEA_GW_VPLS_EN        = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_GW_VPLS_EN_ebable = MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_FWD_DISABLE");
    if (readEnv){
        MEA_device_environment_info.MEA_FWD_DISABLE = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_FWD_DISABLE_ebable=MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_CLI_ENG_DISABLE");
    if (readEnv) {
        MEA_device_environment_info.MEA_CLI_ENG_DISABLE        = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_CLI_ENG_DISABLE_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_DEBUG_REGS");
    if (readEnv){
        glClearDDR_servie = MEA_FALSE;
            MEA_device_environment_info.MEA_DEBUG_REGS_Enable_On_Init = (MEA_Bool)(MEA_OS_atoi(readEnv));
            MEA_device_environment_info.MEA_DEBUG_REGS_Enable_On_Init_enable=MEA_TRUE;
            MEA_device_environment_info.MEA_PERIODIC_ENABLE=MEA_FALSE; /*if user forgot to set*/
			MEA_OS_DeviceMemoryDebugRegFlag= MEA_device_environment_info.MEA_DEBUG_REGS_Enable_On_Init;
    }


    readEnv = MEA_OS_getenv("MEA_CLEAR_DDR");
    if (readEnv) {
        glClearDDR_servie = MEA_FALSE;
            MEA_device_environment_info.MEA_CLEAR_DDR_Enable= MEA_TRUE;
            MEA_device_environment_info.MEA_CLEAR_DDR = (MEA_Bool)(MEA_OS_atoi(readEnv));
    }
    
    readEnv = MEA_OS_getenv("MEA_LAG_TYPE");
    if (readEnv) {
       
        MEA_device_environment_info.MEA_DEVICE_LAG_TYPE_enable = MEA_TRUE;
        MEA_device_environment_info.MEA_DEVICE_LAG_TYPE = (MEA_Bool)(MEA_OS_atoi(readEnv));
    }

    readEnv = MEA_OS_getenv("MEA_DEFAULT_LAG_SUPPORTED");
    if (readEnv) {

        MEA_device_environment_info.MEA_DEFAULT_LAG_SUPPORTED_enable = MEA_TRUE;
        MEA_device_environment_info.MEA_DEFAULT_LAG_SUPPORTED = (MEA_Bool)(MEA_OS_atoi(readEnv));
    }

    readEnv = MEA_OS_getenv("MEA_BAR0_SET");
    if (readEnv) {

        MEA_device_environment_info.MEA_BAR0_SET_enable = MEA_TRUE;
        MEA_device_environment_info.MEA_BAR0_SET = (MEA_Bool)(MEA_OS_atoi(readEnv));
    }
   
        

    readEnv = MEA_OS_getenv("MEA_WITH_IPC");
    if (readEnv){
        MEA_device_environment_info.MEA_WITH_IPC = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_WITH_IPC_enable= MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_PRE_SCH_PROF");
    if (readEnv){
        MEA_device_environment_info.MEA_PRE_SCH_PROF = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_PRE_SCH_PROF_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_PCIE_slot");
    if (readEnv){
        MEA_OS_strcpy(&MEA_device_environment_info.MEA_PCIE_slot[0], (readEnv));
        MEA_device_environment_info.MEA_PCIE_slot_enable = MEA_TRUE;
    }

    
    readEnv = MEA_OS_getenv("MEA_PCIE_DiviceName");
    if (readEnv) {
        MEA_OS_strcpy(&MEA_device_environment_info.MEA_PCIE_DiviceName[0], (readEnv));
        MEA_device_environment_info.MEA_PCIE_DiviceName_enable = MEA_TRUE;
    }


    readEnv = MEA_OS_getenv("MEA_DEVICE_CPU_ETH");
    if (readEnv){
        MEA_OS_strcpy(&MEA_device_environment_info.MEA_DEVICE_CPU_ETH[0], (readEnv));
        
        MEA_device_environment_info.MEA_DEVICE_CPU_ETH_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_ENET_CLI_PORT");
    if (readEnv) {
        MEA_device_environment_info.MEA_ENET_CLI_PORT= MEA_OS_atoi(readEnv);

        MEA_device_environment_info.MEA_ENET_CLI_PORT_enable = MEA_TRUE;
    }
  
    
    readEnv = MEA_OS_getenv("MEA_DEVICE_FPGA_PATH_MCS");
    if (readEnv){
        MEA_OS_strcpy(&MEA_device_environment_info.MEA_DEVICE_FPGA_PATH_MCS[0], (readEnv));

        MEA_device_environment_info.MEA_DEVICE_FPGA_PATH_MCS_enable = MEA_TRUE;
    }

    




    readEnv = MEA_OS_getenv("MEA_PORT_EGRESS_UPDATE_LINK");
    if (readEnv){
        MEA_device_environment_info.MEA_PORT_EGRESS_UPDATE_LINK = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_PORT_EGRESS_UPDATE_LINK_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_PORT_INGRESS_UPDATE_LINK");
    if (readEnv){
        MEA_device_environment_info.MEA_PORT_INGRESS_UPDATE_LINK = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_PORT_INGRESS_UPDATE_LINK_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_PORT_BONDING_LINK");
    if (readEnv){
        MEA_device_environment_info.MEA_PORT_BONDING_LINK = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_PORT_BONDING_LINK_enable = MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_PIGGY_RGMII_BOARD_A");
    if (readEnv){
        MEA_device_environment_info.MEA_PIGGY_RGMII_BOARD_A = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_PIGGY_RGMII_BOARD_A_enable = MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_SW_RESET_DEVICE_SUPPORT");
    if (readEnv){
        MEA_device_environment_info.MEA_SW_RESET_DEVICE_SUPPORT = (MEA_Bool)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_SW_RESET_DEVICE_SUPPORT_enable = MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_SFP_SUPPORT");
    if (readEnv){
        MEA_device_environment_info.MEA_SFP_SUPPORT = MEA_OS_atoi(readEnv);
        switch(MEA_device_environment_info.MEA_SFP_SUPPORT ){
        case  0:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"  SFP = Force MODE   \n",
                __FUNCTION__);
            break;
        case  1:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"  SFP = autoNeg   \n",
                __FUNCTION__);
            break;

        }
         
        

        MEA_device_environment_info.MEA_SFP_SUPPORT_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_DEVICE_DB_SERVICE_EX");
    if (readEnv) {
        MEA_device_environment_info.MEA_DEVICE_DB_SERVICE_EX = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_DEVICE_DB_SERVICE_EX_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_DEVICE_DB_EDITING");
    if (readEnv) {
        MEA_device_environment_info.MEA_DEVICE_DB_EDITING = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_DEVICE_DB_EDITING_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_DEVICE_DB_HPM");
    if (readEnv) {
        MEA_device_environment_info.MEA_DEVICE_DB_HPM        = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_DEVICE_DB_HPM_enable = MEA_TRUE;
    }

    

    


    /*TDM BOARD */
    readEnv = MEA_OS_getenv("MEA_TDM_CES_D_T");
    if (readEnv){
        MEA_device_environment_info.MEA_TDM_CES_D_T = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_TDM_CES_D_T_enable= MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_TDM_NUM_OF_BUFFER");
    if (readEnv){
        MEA_device_environment_info.MEA_TDM_NUM_OF_BUFFER = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_TDM_NUM_OF_BUFFER_enable = MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_TMUX_SLICE");
    if (readEnv){
        MEA_device_environment_info.MEA_TMUX_SLICE = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_TMUX_SLICE_enable = MEA_TRUE;
    }
    readEnv = MEA_OS_getenv("MEA_TMUX_SPE");
    if (readEnv){
        MEA_device_environment_info.MEA_TMUX_SPE = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_TMUX_SPE_enable = MEA_TRUE;
    }

    readEnv = MEA_OS_getenv("MEA_DEVICE_ITHI");
    if (readEnv){

        MEA_device_environment_info.MEA_DEVICE_ITHI = (MEA_Uint32)(MEA_OS_atoi(readEnv));
        MEA_device_environment_info.MEA_DEVICE_ITHI_enable = MEA_TRUE;
    }

#endif
    

    return;
}

static MEA_Uint32 mea_drv_get_statusReg(MEA_Uint32    statusReg,
                                        MEA_Uint32    statusMask,
                                        MEA_module_te mea_module,
                                        mea_memory_mode_e memoryMode)
{

    MEA_Uint32 tempVal = 0;

#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION    
    //MEA_Uint32 count1 = 0;
    MEA_Uint32 maxofcount = MEA_STATUS_REG_WAIT_COUNT_DEF_VAL;
    MEA_Uint32   check_statusMask= statusMask;


    MEA_Uint32 count, count_status_stable;
    MEA_Uint32 tempVal2 = 0;

    /* Check the status before we start */
    count = MEA_STATUS_REG_WAIT_COUNT_DEF_VAL;

    tempVal = MEA_LowLevel_ReadReg(MEA_UNIT_0, statusReg, mea_module, memoryMode, MEA_MEMORY_READ_SUM);
    for (count_status_stable = MEA_STATUS_REG_STABLE_COUNT_DEF_VAL;
        count_status_stable > 0;
        count_status_stable--) {
       // MEA_OS_sleep(MEA_STATUS_REG_WAIT_TIME_SEC_DEF_VAL,
       //     MEA_STATUS_REG_WAIT_TIME_NSEC_DEF_VAL);

        tempVal2 = MEA_LowLevel_ReadReg(MEA_UNIT_0, statusReg, mea_module, memoryMode, MEA_MEMORY_READ_SUM);
#if 0     
        if (tempVal2 == tempVal) {
            count1++;
            if (count1 == 3) { //Alex

                break;
            }
        }
#endif
        if (tempVal2 != tempVal) {
            if (maxofcount == MEA_STATUS_REG_WAIT_COUNT_DEF_VAL)
                maxofcount = count_status_stable = MEA_STATUS_REG_STABLE_COUNT_DEF_VAL + 1;
            
            //count1 = 0;
            tempVal = tempVal2;
        }
    }

    //count1 = 0;
    tempVal2 = 0;
    maxofcount = MEA_STATUS_REG_WAIT_COUNT_DEF_VAL;
#if 0	
    if ((mea_module == MEA_MODULE_IF)  && 
        ( (statusMask == MEA_IF_STATUS_COLLISION_MASK) ||
          (statusMask == MEA_IF_STATUS_FILTER_BUSY_MASK)||
            (statusMask == MEA_IF_STATUS_HC_BUSY_MASK)  ||
            (statusMask == MEA_IF_STATUS_EGR_CLASS_BUSY_MASK) ||
            (statusMask == MEA_IF_STATUS_EVC_CLASS_BUSY_MASK) ||
            (statusMask == MEA_IF_STATUS_COLLISION_MASK)  
            )){
        /*check that we not have busy*/  
        check_statusMask = MEA_IF_STATUS_BUSY_MASK;  // | statusMask;
    }
#endif	

        while ((count--) && (tempVal & check_statusMask)) {

            /* Give the table a change to stabilize  */
            MEA_OS_sleep(MEA_STATUS_REG_WAIT_TIME_SEC_DEF_VAL,
                MEA_STATUS_REG_WAIT_TIME_NSEC_DEF_VAL);

            tempVal = MEA_LowLevel_ReadReg(MEA_UNIT_0, statusReg, mea_module, memoryMode, MEA_MEMORY_READ_SUM);
          //  tempVal = tempVal & check_statusMask;
			for (count_status_stable = MEA_STATUS_REG_STABLE_COUNT_DEF_VAL;
                count_status_stable > 0;
                count_status_stable--) {

                tempVal2 = MEA_LowLevel_ReadReg(MEA_UNIT_0, statusReg, mea_module, memoryMode, MEA_MEMORY_READ_SUM);
            //    tempVal2 = tempVal2 & check_statusMask;
#if 0
                if (tempVal2 == tempVal) {
                    count1++;
                    if (count1 == 3)
                        break;
                }
#endif
                if (tempVal2 != tempVal) {
                    if (maxofcount == MEA_STATUS_REG_WAIT_COUNT_DEF_VAL)
                        maxofcount = count_status_stable = MEA_STATUS_REG_STABLE_COUNT_DEF_VAL + 1;
                   // count1 = 0;
                    tempVal = tempVal2;
                }
            }
        }
    
#endif

    return tempVal;
}







/****************************************************************************************
*							Driver Small Utilities					  		 			*
*****************************************************************************************/



/**************************************************************************************/
// Function Name: MEA_API_WriteReg																	 
// 																					  
// Description:	  Write 32 bit register to SWE/BM or via RPC (compilation flag)   																	  
// Arguments:	  Addr - address to be written to  data - data 															    
//				  mea_module - MEA referred module  																  
// Return:		  None.  																  
/**************************************************************************************/

void MEA_API_WriteReg(MEA_Unit_t unit,MEA_Uint32 Addr,MEA_Uint32 data,MEA_module_te mea_module)
{
	MEA_LowLevel_WriteReg(unit,Addr,data,mea_module,globalMemoryMode);
}



void MEA_LowLevel_WriteReg(MEA_Unit_t unit, MEA_ULong_t Addr,MEA_Uint32 data,MEA_module_te mea_module,mea_memory_mode_e memoryMode)
{
    MEA_ULong_t mem_base_addr;
    MEA_ULong_t  mem_id;
    MEA_ULong_t temp_Addr = Addr;

    if (MEA_OS_DeviceMemoryDebugRegFlag) {

        switch(mea_debug_reg_all){
            case 0:
                if( ((Addr != ENET_IF_STATUS_REG &&
                    Addr != ENET_IF_CMD_PARAM_REG &&
                    Addr != ENET_IF_CMD_REG       ) && (mea_module) == MEA_MODULE_IF )||

                    ((Addr != ENET_BM_IA_STAT &&
                    Addr != ENET_BM_IA_ADDR &&
                    Addr != ENET_BM_IA_CMD       ) && (mea_module) == MEA_MODULE_BM )  )
                {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                        "%s WriteReg (Addr=0x%08x , Value=0x%08x) \n",
                        MEA_MODULE_NAME(mea_module),
                        Addr,data);
                }
            break;

            case 1:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                    "%s WriteReg (Addr=0x%08x , Value=0x%08x) \n",
                    MEA_MODULE_NAME(mea_module),
                    Addr,data);
                break;

        }

    }
	MEA_AcessSemTake();
	if (memoryMode == MEA_MODE_REGULAR_ENET3000){
		mem_id        = MEA_GetMemoryId  (mea_module,temp_Addr); 
		mem_base_addr = MEA_GetMemoryAddr(mea_module,&temp_Addr);
		MEA_OS_DeviceMemoryWriteUint32(mea_module,
									   mem_id,
									   mem_base_addr,
									   temp_Addr,
									   data);
	}
	else { /* Enet4000 */
		if ((memoryMode == MEA_MODE_REGULAR_ENET4000 )||
			(memoryMode == MEA_MODE_UPSTREEM_ONLY)) {
			mem_id        = MEA_GetMemoryId  (mea_module,temp_Addr); 
			mem_base_addr = MEA_GetMemoryAddr(mea_module,&temp_Addr);
			MEA_OS_DeviceMemoryWriteUint32(mea_module,
										   mem_id,
										   mem_base_addr,
										   temp_Addr,
										   data);
		}
		if ((memoryMode == MEA_MODE_REGULAR_ENET4000 )||
			(memoryMode == MEA_MODE_DOWNSTREEM_ONLY)) {
			mem_id        = MEA_GetMemoryId  (mea_module+MEA_MODULE_DOWNSTREEM_OFFSET,temp_Addr); 
			mem_base_addr = MEA_GetMemoryAddr(mea_module+MEA_MODULE_DOWNSTREEM_OFFSET,&temp_Addr);
			MEA_OS_DeviceMemoryWriteUint32(mea_module,
										   mem_id,
										   mem_base_addr,
										   temp_Addr,
										   data);

		}
	} /* Enet4000 */
	MEA_AcessSemGive();
}


/**************************************************************************************/
// Function Name: MEA_API_ReadReg																	 
// 																					  
// Description:	 Read 32 bit register from SWE/BM or via RPC (compilation flag)																		  
// Arguments:	 Addr - address to be read																    
//				 mea_module - MEA refered module 
//
// Return:		 read value																	  
/**************************************************************************************/
MEA_Uint32 MEA_API_ReadReg(MEA_Unit_t unit,MEA_Uint32 Addr,MEA_module_te mea_module)
{
    MEA_Uint32 retVal = 0;
    if (mea_module == MEA_MODULE_BM) {
        if (GlobalBm_busy) {
            return retVal;
        }
    }

    return(MEA_LowLevel_ReadReg(unit,Addr,mea_module,globalMemoryMode,MEA_MEMORY_READ_COMPARE));
}

MEA_Uint32 MEA_LowLevel_ReadReg(MEA_Unit_t unit, MEA_ULong_t Addr, MEA_module_te mea_module, mea_memory_mode_e memoryMode,mea_memory_mode_read_e readMode)
{

	MEA_Uint32 data=0,tmp=0;
    MEA_ULong_t mem_base_addr;
    MEA_ULong_t  mem_id;
    MEA_ULong_t temp_Addr = Addr;

	MEA_AcessSemTake();

	if (memoryMode == MEA_MODE_REGULAR_ENET3000){
		mem_id        = MEA_GetMemoryId  (mea_module,temp_Addr); 
		mem_base_addr = MEA_GetMemoryAddr(mea_module,&temp_Addr); 
		MEA_OS_DeviceMemoryReadUint32(mea_module,
									  mem_id,
									  mem_base_addr,
									  temp_Addr,
									  &data);
	}
	else { /* Enet4000 */
		if ((memoryMode == MEA_MODE_REGULAR_ENET4000 )||
			(memoryMode == MEA_MODE_UPSTREEM_ONLY)) {
			mem_id        = MEA_GetMemoryId  (mea_module,temp_Addr); 
			mem_base_addr = MEA_GetMemoryAddr(mea_module,&temp_Addr); 
			MEA_OS_DeviceMemoryReadUint32(mea_module,
										  mem_id,
										  mem_base_addr,
										  temp_Addr,
										  &data);
		}
		if ((memoryMode == MEA_MODE_REGULAR_ENET4000 )||
			(memoryMode == MEA_MODE_DOWNSTREEM_ONLY)) {
			mem_id        = MEA_GetMemoryId  (mea_module+MEA_MODULE_DOWNSTREEM_OFFSET,temp_Addr); 
			mem_base_addr = MEA_GetMemoryAddr(mea_module+MEA_MODULE_DOWNSTREEM_OFFSET,&temp_Addr); 
			MEA_OS_DeviceMemoryReadUint32(mea_module,
										  mem_id,
										  mem_base_addr,
										  temp_Addr,
										  &tmp);
		}
		if (memoryMode != MEA_MODE_REGULAR_ENET4000)
			readMode = MEA_MEMORY_READ_SUM;
		switch (readMode){
			case MEA_MEMORY_READ_COMPARE:
				if (data!=tmp){
					MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
									  "%s upstream 0x%x downstream 0x%x conflict at address 0x%x) \n",
									  MEA_MODULE_NAME(mea_module),
									  data,tmp,
									  Addr);
				}
				break;
			case MEA_MEMORY_READ_SUM:
				data=data+tmp;
				break;
			case MEA_MEMORY_READ_IGNORE:
			case MEA_MEMORY_READ_LAST:
			default:;
		}
	}/* Enet4000 */
	MEA_AcessSemGive();

    if (MEA_OS_DeviceMemoryDebugRegFlag) {

        switch(mea_debug_reg_all){
            case 0:
                if( ((Addr != ENET_IF_STATUS_REG &&
                    Addr != ENET_IF_CMD_PARAM_REG &&
                    Addr != ENET_IF_CMD_REG       ) && (mea_module) == MEA_MODULE_IF )||

                    ((Addr != ENET_BM_IA_STAT &&
                    Addr != ENET_BM_IA_ADDR &&
                    Addr != ENET_BM_IA_CMD       ) && (mea_module) == MEA_MODULE_BM )  )
                {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                        "%s ReadReg  (Addr=0x%08x , Value=0x%08x) \n",
                        MEA_MODULE_NAME(mea_module),
                        Addr,data);

                }
                break;
            case 1:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                    "%s ReadReg  (Addr=0x%08x , Value=0x%08x) \n",
                    MEA_MODULE_NAME(mea_module),
                    Addr,data);
                break;
        }
    }


    return (data & 0xffffffff);
}





/**************************************************************************************/
// Function Name:  MEA_DRV_WriteInd																	 
// 																					  
// Description:	   Writes to MEA table in indirect manner
//				   This routine assumes that the data is already written to the 
//				   indirect data registers. It only preform the indirect access 
//				   cycle.
// Arguments:	   p_ind_write -  information needed to perfrom the indirect write 
//							   	  operation (see remarks of related structure)
//				   mea_module - refered MEA module.	
//				   
// Return:		  MEA_OK/MEA_ERROR 																  
/**************************************************************************************/


MEA_Status MEA_API_WriteIndirect(MEA_Unit_t unit,MEA_ind_write_t *p_ind_write,MEA_module_te mea_module)
{
	MEA_Status retVal;
	ENET_IND_Lock();
    
    if(mea_module == MEA_MODULE_BM){
        if (GlobalBm_busy) {
            ENET_IND_Unlock();
            return  MEA_OK;
        }
    }
	
    retVal= (MEA_LowLevel_WriteIndirect(unit,p_ind_write,mea_module,globalMemoryMode));
	ENET_IND_Unlock();
	return retVal;
}


MEA_Status MEA_LowLevel_WriteIndirect(MEA_Unit_t unit,MEA_ind_write_t *p_ind_write,MEA_module_te mea_module,mea_memory_mode_e memoryMode)
{

	MEA_Uint32 tempVal;                                                                
	//MEA_Uint32 collision_mask=0;

    switch (mea_module) {
	case MEA_MODULE_BM:
    case MEA_MODULE_IF:
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid mea_module(%d) \n",
                          __FUNCTION__,
                          mea_module);
        return MEA_ERROR;
    }

    if (p_ind_write == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - p_ind_write == NULL (mea_module=%d) \n",
                          __FUNCTION__,
                          mea_module);
        return MEA_ERROR;

    }

    if (MEA_OS_DeviceMemoryDebugRegFlag) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "%s WriteIndirect (table=%d,offset=%d) \n",
                          MEA_MODULE_NAME(mea_module),
                          p_ind_write->tableType,
                          p_ind_write->tableOffset);
    }




    /* Check the status before we start */
    tempVal = mea_drv_get_statusReg (p_ind_write->statusReg,
                                     p_ind_write->statusMask,
                                     mea_module,
									 memoryMode);
    if (tempVal & p_ind_write->statusMask) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Can not start Write Indirect "
                          "(tblTyp=%d,entry=%d,status=0x%08x,mea_module=%d)\n",
                          __FUNCTION__,
                          p_ind_write->tableType,
                          p_ind_write->tableOffset,
                          tempVal,
                          mea_module);
        return MEA_ERROR; 
    }

    if (mea_module == MEA_MODULE_IF) {

        if (p_ind_write->tableAddrReg != MEA_IF_CMD_PARAM_REG_IGNORE) {

            /* Set the table type & offset */
            tempVal = p_ind_write->tableOffset;
            tempVal |= ((p_ind_write->tableType)
                << MEA_OS_calc_shift_from_mask(p_ind_write->tableAddrMask));
            tempVal |= (MEA_IF_CMD_PARAM_RW_WRITE
                << MEA_OS_calc_shift_from_mask(MEA_IF_CMD_PARAM_RW_MASK));
            MEA_LowLevel_WriteReg(MEA_UNIT_0, p_ind_write->tableAddrReg,
                tempVal,
                mea_module,
                memoryMode);
        }



        //Write the entry data
        //T.B.D !! need to add memoryMode here
        MEA_OS_DeviceMemory_IndirectDataAccessMode = MEA_TRUE;
        p_ind_write->writeEntry(p_ind_write->funcParam1,
            p_ind_write->funcParam2,
            p_ind_write->funcParam3,
            p_ind_write->funcParam4,
            mea_module);
        MEA_OS_DeviceMemory_IndirectDataAccessMode = MEA_FALSE;

        /* Issue write command */
        MEA_LowLevel_WriteReg(MEA_UNIT_0, p_ind_write->cmdReg,
            p_ind_write->cmdMask,
            mea_module,
            memoryMode);


        /* Check the status */
        tempVal = mea_drv_get_statusReg (p_ind_write->statusReg,
                                         p_ind_write->statusMask,
                                         mea_module,
										 memoryMode);
        if (tempVal & p_ind_write->statusMask) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - failed (tblTyp=%d,entry=%d,status=0x%08x,mea_module=%d)\n",
                              __FUNCTION__,
                              p_ind_write->tableType,
                              p_ind_write->tableOffset,
                              tempVal,
                              mea_module);
            return MEA_ERROR; 
        }
        
		
  

        /* Return to caller */

    if (MEA_OS_DeviceMemoryDebugRegFlag) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "%s (IF) WriteIndirect (table=%d,offset=%d) Finish \n",
                          MEA_MODULE_NAME(mea_module),
                          p_ind_write->tableType,
                          p_ind_write->tableOffset);
    }
        if (MEA_OS_DeviceMemoryDebugRegFlag) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                             "WriteIndirect finish \n");
        }
                             
        return MEA_OK;

    } 

	// Set the table type & offset 
    tempVal =((p_ind_write->tableType)<<MEA_OS_calc_shift_from_mask(~p_ind_write->tableAddrMask)); 
	if( p_ind_write->tableType == 14 ||
        p_ind_write->tableType == 16 || 
        p_ind_write->tableType == 21 ||
        p_ind_write->tableType == 22 ||
        p_ind_write->tableType == 23 ||
        p_ind_write->tableType == 24 ||
        p_ind_write->tableType == 27 ||
        p_ind_write->tableType == 29 ||
        
        p_ind_write->tableType == 45 ||
        p_ind_write->tableType == 58 ||
        p_ind_write->tableType == 87 
       

       ){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s - failed (tblTyp=%d, not allow to write)\n",
               __FUNCTION__,
               p_ind_write->tableType,
               mea_module);
           return MEA_ERROR; 
        }





	MEA_LowLevel_WriteReg(MEA_UNIT_0,p_ind_write->tableAddrReg,(tempVal|p_ind_write->tableOffset),mea_module,memoryMode);


	//Write the entry data
	p_ind_write->writeEntry(p_ind_write->funcParam1,p_ind_write->funcParam2,
							p_ind_write->funcParam3,p_ind_write->funcParam4,
                            p_ind_write->numofregs,p_ind_write->retval,mea_module);

	MEA_LowLevel_WriteReg(MEA_UNIT_0,p_ind_write->cmdReg,(~p_ind_write->cmdMask&p_ind_write->cmdMask),mea_module,memoryMode);                                      

    /* Check the status */
    tempVal = mea_drv_get_statusReg (p_ind_write->statusReg,
                                     p_ind_write->statusMask,
                                     mea_module,
									 memoryMode);

    if (tempVal & p_ind_write->statusMask) {
        if (mea_module == MEA_MODULE_BM)
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "**********LOG_ERROR BUSY**************************\n");
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - failed (tblTyp=%d,entry=%d,status=0x%08x,mea_module=%d)\n",
                          __FUNCTION__,
                          p_ind_write->tableType,
                          p_ind_write->tableOffset,
                          tempVal,
                          mea_module);
        if (mea_module == MEA_MODULE_BM) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Last BM table %d \n", mea_drv_dbg_saveTableBM);
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "**********LOG_ERROR BUSY**************************\n");         GlobalBm_busy = MEA_TRUE;
        }
			
		
        return MEA_ERROR; 
    }
	
	if( mea_module == MEA_MODULE_BM && mea_drv_dbg_saveTableBM != p_ind_write->tableType)
		mea_drv_dbg_saveTableBM = p_ind_write->tableType;


    if (MEA_OS_DeviceMemoryDebugRegFlag) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "%s WriteIndirect (table=%d,offset=%d) Finish \n",
                          MEA_MODULE_NAME(mea_module),
                          p_ind_write->tableType,
                          p_ind_write->tableOffset);
    }
        if (MEA_OS_DeviceMemoryDebugRegFlag) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                             "WriteIndirect finish \n");
        }
	return MEA_OK;                                                                     
}





/**************************************************************************************/
// Function Name:  MEA_API_Read_IFIndirect												
// 																					  
// Description:	   Read MEA table in indirect manner
//				   
// Arguments:	   TableId -  TableId indirect read 
//					tableOffset : offset in Table 			
//				   num_of_regs - number of registers to be read	
//				   arry -  pointer to retuen date of uint32.	
//				   
// Return:		  MEA_OK/MEA_ERROR 												
/**************************************************************************************/

MEA_Status MEA_API_Read_IFIndirect(MEA_Unit_t             unit,
    MEA_Uint32             TableId,
    MEA_Uint32             tableOffset,
    MEA_Uint32             num_of_regs,
    MEA_Uint32             *arry)
{
    MEA_ind_read_t         ind_read;
   

    if (arry == NULL) {
        return MEA_ERROR;
    }
    if (num_of_regs == 0 || num_of_regs > 10) {
        return MEA_ERROR;
    }

    

    ind_read.tableType = TableId;
    ind_read.cmdReg = MEA_IF_CMD_REG;
    ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_read.statusReg = MEA_IF_STATUS_REG;
    ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;




    ind_read.read_data = (arry);
    ind_read.tableOffset = tableOffset;
    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        num_of_regs,
        MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_ReadIndirect tbl=%d offset=%d mdl=%d failed\n",
            __FUNCTION__,
            ind_read.tableType,
            ind_read.tableOffset,
            MEA_MODULE_IF);
        

        return MEA_ERROR;
    }

    return MEA_OK;
}

/**************************************************************************************/
// Function Name:  MEA_API_ReadIndirect												
// 																					  
// Description:	   Read MEA table in indirect manner
//				   
// Arguments:	   p_ind_read -  information needed to perform the indirect read 
//								 operation (see remarks of related structure)
//				   num_of_regs - number of registers to be read	
//				   mea_module - referred MEA module.	
//				   
// Return:		  MEA_OK/MEA_ERROR 												
/**************************************************************************************/


MEA_Status MEA_API_ReadIndirect(MEA_Unit_t unit,MEA_ind_read_t *p_ind_read,MEA_Uint32 num_of_regs,MEA_module_te mea_module)
{
	MEA_Status retVal;
	ENET_IND_Lock();
    if (mea_module == MEA_MODULE_BM) {
        if (GlobalBm_busy) {
            ENET_IND_Unlock();
            return  MEA_OK;
        }
    }
	retVal =(MEA_LowLevel_ReadIndirect(unit,p_ind_read,num_of_regs,mea_module,globalMemoryMode,MEA_MEMORY_READ_COMPARE));
	ENET_IND_Unlock();
	return retVal;
}

MEA_Status MEA_LowLevel_ReadIndirect(MEA_Unit_t unit,MEA_ind_read_t *p_ind_read,MEA_Uint32 num_of_regs,MEA_module_te mea_module,mea_memory_mode_e memoryMode,mea_memory_mode_read_e readMode)
{
	MEA_Uint32 tempVal,i;


    switch (mea_module) {
	case MEA_MODULE_BM:
    case MEA_MODULE_IF:
        break;
    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid mea_module(%d) \n",
                          __FUNCTION__,
                          mea_module);
        return MEA_ERROR;
    }

    if (p_ind_read == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - p_ind_read == NULL (mea_module=%d) \n",
                          __FUNCTION__,
                          mea_module);
        return MEA_ERROR;

    }

    if (MEA_OS_DeviceMemoryDebugRegFlag) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "%s ReadIndirect  (table=%d,offset=%d) \n",
                          MEA_MODULE_NAME(mea_module),
                          p_ind_read->tableType,
                          p_ind_read->tableOffset);
    }

#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
    /* Check the status before we start */
    tempVal = mea_drv_get_statusReg (p_ind_read->statusReg,
                                     p_ind_read->statusMask,
                                     mea_module,
									 memoryMode);

    if (tempVal & p_ind_read->statusMask) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Can not start Read Indirect (tblTyp=%d,entry=%d,status=0x%08x,mea_module=%d)\n",
                         __FUNCTION__,
                         p_ind_read->tableType,
                         p_ind_read->tableOffset,
                         tempVal,
                         mea_module);
       return MEA_ERROR; 
    }

#endif

    if (mea_module == MEA_MODULE_IF) { 

        if (p_ind_read->tableAddrReg != MEA_IF_CMD_PARAM_REG_IGNORE) {

	       /* Set the table type & offset & read indication */
           tempVal  = p_ind_read->tableOffset;
           tempVal |= ((p_ind_read->tableType)
                       << MEA_OS_calc_shift_from_mask(p_ind_read->tableAddrMask));
    	   tempVal |= (MEA_IF_CMD_PARAM_RW_READ      
                       << MEA_OS_calc_shift_from_mask(MEA_IF_CMD_PARAM_RW_MASK     ));
    	   MEA_LowLevel_WriteReg(MEA_UNIT_0,p_ind_read->tableAddrReg,
                            tempVal,
                            mea_module,
							memoryMode);
        }

        /* Issue write command */
 	    MEA_LowLevel_WriteReg(MEA_UNIT_0,p_ind_read->cmdReg,
                         p_ind_read->cmdMask,
                         mea_module,
						 memoryMode);

#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
        /* Check the status */
        tempVal = mea_drv_get_statusReg (p_ind_read->statusReg,
                                         p_ind_read->statusMask,
                                         mea_module,
										 memoryMode);
        if (tempVal & p_ind_read->statusMask) {
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - failed (tblTyp=%d,entry=%d,status=0x%08x.mea_module=%d)\n",
                              __FUNCTION__,
                              p_ind_read->tableType,
                              p_ind_read->tableOffset,
                              tempVal,
                              mea_module);
            return MEA_ERROR; 
        }
#endif

	    /* Read the entry data */
        MEA_OS_DeviceMemory_IndirectDataAccessMode = MEA_TRUE;
   	    for(i=0;i<num_of_regs;i++){
	       p_ind_read->read_data[i] = MEA_LowLevel_ReadReg(MEA_UNIT_0,MEA_IF_READ_DATA_REG(i),
                                                      MEA_MODULE_IF,memoryMode,readMode);
        }
        MEA_OS_DeviceMemory_IndirectDataAccessMode = MEA_FALSE;

        /* Return to caller */
    if (MEA_OS_DeviceMemoryDebugRegFlag) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "%s ReadIndirect (table=%d,offset=%d) Finish \n",
                          MEA_MODULE_NAME(mea_module),
                          p_ind_read->tableType,
                          p_ind_read->tableOffset);
    }
        return MEA_OK;

    }


	// Set the table type & offset
    tempVal =((p_ind_read->tableType)<<MEA_OS_calc_shift_from_mask(~p_ind_read->tableAddrMask));                                   
	MEA_LowLevel_WriteReg(MEA_UNIT_0,p_ind_read->tableAddrReg,(tempVal|p_ind_read->tableOffset),mea_module,memoryMode);


	// Issue read command                                                         
	MEA_LowLevel_WriteReg(MEA_UNIT_0,p_ind_read->cmdReg,p_ind_read->cmdMask,mea_module,memoryMode);                                      

    /* Check the status */
    tempVal = mea_drv_get_statusReg (p_ind_read->statusReg,
                                     p_ind_read->statusMask,
                                     mea_module,
									 memoryMode);
    if (tempVal & p_ind_read->statusMask) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - failed (tblTyp=%d,entry=%d,status=0x%08x,mea_module=%d)\n",
                         __FUNCTION__,
                         p_ind_read->tableType,
                         p_ind_read->tableOffset,
                         tempVal,
                         mea_module);
	   if( mea_module == MEA_MODULE_BM)
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Last BM table %d \n", mea_drv_dbg_saveTableBM);

       return MEA_ERROR; 
    }


	//Read the entry data
	for(i=0;i<num_of_regs;i++){
       switch (mea_module) {
	   case MEA_MODULE_BM:
			*(p_ind_read->read_data + i) = MEA_LowLevel_ReadReg(MEA_UNIT_0,MEA_BM_IA_RDDATA0+(i*4),
                                                           mea_module,memoryMode,readMode);
            break;
       case MEA_MODULE_IF:
			*(p_ind_read->read_data + i) = MEA_LowLevel_ReadReg(MEA_UNIT_0,MEA_IF_READ_DATA_REG(i),
                                                           mea_module,memoryMode,readMode);
            break;
       default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Invalid mea_module(%d) \n",
                              __FUNCTION__,
                              mea_module);
            return MEA_ERROR;
            
       }
	}

	if( mea_module == MEA_MODULE_BM && mea_drv_dbg_saveTableBM != p_ind_read->tableType)
		mea_drv_dbg_saveTableBM = p_ind_read->tableType;

			
    if (MEA_OS_DeviceMemoryDebugRegFlag) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,
                          "%s ReadIndirect (table=%d,offset=%d) Finish \n",
                          MEA_MODULE_NAME(mea_module),
                          p_ind_read->tableType,
                          p_ind_read->tableOffset);
    }
	return MEA_OK;                                                                     
}



MEA_Status MEA_DRV_WriteDDR_DATA_BLOCK(MEA_Unit_t unit_i, MEA_Uint32 hwId, MEA_Uint32 addressDDR, MEA_Uint32 *DataValue, MEA_Uint32 len, MEA_DDR_module Type)
{

    MEA_Uint32 i;
    MEA_Uint32 MyaddressDDR = addressDDR;


    if (MEA_OS_DeviceMemoryDebugRegFlag) {

        switch (Type) {
        case DDR_Action:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " DDR_START  DDR_Action Id %6d \n", hwId);
            break;
        case  DDR_EDITING:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " DDR_START  DDR_EDITING Id %6d \n", hwId);

            break;
        case DDR_Service_EXT:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " DDR_START  DDR_Service_EXT Id %6d \n", hwId);
            break;
        case DDR_PMS:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " DDR_START  DDR_PMS Id %6d \n", hwId);
            break;
        case DDR_ACL5:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " DDR_START  DDR_ACL5 Id %6d \n", hwId);
            break;
        case DDR_MODULE_LAST:
        default:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " DDR_START   ERROR\n");
            return MEA_ERROR;
            break;


        }
    }

    
      //  MEA_API_WriteReg(unit_i, addressDDR, DataValue[i], MEA_MODULE_DATA_DDR);
        
        
        MEA_DB_DDR_HW_ACCESS_ENABLE 
        MEA_DDR_Lock();
        if(MEA_OS_Device_DATA_DDR_RW(MEA_MODULE_DATA_DDR,1,/*0 read 1- write */
            MyaddressDDR,
            MEA_ADDRES_DDR_BYTE*len,
            &DataValue[0]      
            ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " ERROR MODULE_DATA_DDR  Write to DDR \n");
            MEA_DB_DDR_HW_ACCESS_DISABLE
            MEA_DDR_Unlock();
            return MEA_ERROR;
        }
        MEA_DB_DDR_HW_ACCESS_DISABLE
            MEA_DDR_Unlock();

            
    for (i = 0; i < len; i++)
    {
        if (MEA_OS_DeviceMemoryDebugRegFlag)
         {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "DDR  WriteReg (Addr=0x%08x , Value=0x%08x) \n", MyaddressDDR, DataValue[i]);
         }
         MyaddressDDR += MEA_ADDRES_DDR_BYTE;
   }
    if (MEA_OS_DeviceMemoryDebugRegFlag)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "DDR_END \n");
    }




    return MEA_OK;
}






static void MEA_AcessSemTake(void)
{
 #if defined(MEA_OS_LINUX) && !defined(__KERNEL__)   
	pthread_mutex_lock((pthread_mutex_t*) &MEA_Access_Sem);
#else
	MEA_LOCK(&MEA_Access_Sem);
#endif
}


/**************************************************************************************/
// Function Name: MEA_AcessSemGive																	 
// 																					  
// Description:	  Give MEA semaphore for accessing the internal address space.
// Arguments:	  None.
//				  
// Return:		  MEA_OK/MEA_ERROR  																  
/**************************************************************************************/

static void  MEA_AcessSemGive(void)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) 
	pthread_mutex_unlock((pthread_mutex_t*)&MEA_Access_Sem);
#else
	MEA_UNLOCK(&MEA_Access_Sem);
#endif
}





MEA_Uint32 MEA_API_Read_TMUX(MEA_Unit_t unit,MEA_Uint32 Addr)
{


    MEA_API_WriteReg(unit,ENET_IF_TMUX_SET_ADDRES_REG,Addr,MEA_MODULE_IF);
    MEA_API_WriteReg(unit,ENET_IF_TMUX_SET_CMD_REG,0,MEA_MODULE_IF);  //command read

    MEA_OS_sleep(0, 50*1000);	/* 5 microsecond*/


    return (MEA_API_ReadReg(unit,ENET_IF_TMUX_READ_DATA_REG , MEA_MODULE_IF));

}


void  MEA_API_Write_TMUX(MEA_Unit_t unit,MEA_Uint32 Addr,MEA_Uint32   data)
{
    MEA_API_WriteReg(unit,ENET_IF_TMUX_SET_ADDRES_REG,Addr,MEA_MODULE_IF);
    MEA_API_WriteReg(unit,ENET_IF_TMUX_SET_DATA_WRITE_REG,data,MEA_MODULE_IF);

    MEA_API_WriteReg(unit,ENET_IF_TMUX_SET_CMD_REG,1,MEA_MODULE_IF);  //command write

}



void   MEA_IND_TABE_Lock(void)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__) 
   pthread_mutex_lock((pthread_mutex_t*) &MEA_drv_IND_Sem);
 #else
 MEA_LOCK(&MEA_drv_IND_Sem);
 #endif
}

void MEA_IND_TABE_Unlock(void)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
	pthread_mutex_unlock((pthread_mutex_t*) &MEA_drv_IND_Sem);
#else
  MEA_UNLOCK(&MEA_drv_IND_Sem);
#endif
}

void MEA_FWD_Lock(void)
{ 
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)   
	pthread_mutex_lock((pthread_mutex_t*) &MEA_drv_FWD_Sem);
#else
MEA_LOCK(&MEA_drv_FWD_Sem);
#endif	
}

void MEA_FWD_Unlock(void)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
 pthread_mutex_unlock((pthread_mutex_t*) &MEA_drv_FWD_Sem);
#else
  MEA_UNLOCK(&MEA_drv_FWD_Sem);
#endif
}



void MEA_DDR_Lock(void)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)   
    pthread_mutex_lock((pthread_mutex_t*)&MEA_drv_DDR_Sem);
#else
    MEA_LOCK(&MEA_drv_DDR_Sem);
#endif	
}

void MEA_DDR_Unlock(void)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
    pthread_mutex_unlock((pthread_mutex_t*)&MEA_drv_DDR_Sem);
#else
    MEA_UNLOCK(&MEA_drv_DDR_Sem);
#endif
}

void MEA_BAR0_Lock(void)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
    pthread_mutex_lock((pthread_mutex_t*)&MEA_drv_BAR0_Sem);
#else
    MEA_LOCK(&MEA_drv_BAR0_Sem);
#endif
}

void MEA_BAR0_Unlock(void)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
    pthread_mutex_unlock((pthread_mutex_t*)&MEA_drv_BAR0_Sem);
#else
    MEA_UNLOCK(&MEA_drv_BAR0_Sem);
#endif
}



MEA_Status MEA_API_Lock(void)
{
#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)

 	pthread_mutex_lock((pthread_mutex_t*) &MEA_drv_API_Sem);

#else
	MEA_LOCK(&MEA_drv_API_Sem);
#endif
 
return MEA_OK;
}

MEA_Status MEA_API_Unlock(void)
{

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)

	pthread_mutex_unlock((pthread_mutex_t*) &MEA_drv_API_Sem);
#else
MEA_UNLOCK(&MEA_drv_API_Sem);
#endif
    return MEA_OK;
}



/**********************************************************************************/
/*                                                                                */
/*                                                                                */
/*                      memory allocation section                                 */
/*                                                                                */
/*                                                                                */
/**********************************************************************************/

MEA_Status mea_drv_Init_MemoryMap (MEA_ULong_t*   module_base_addr_vec) {
#if !defined(HW_BOARD_IS_PCI) && !defined(AXI_TRANS)  
    MEA_ULong_t physical_mem_addr;
#endif
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
	MEA_Uint32 hwLxversion=0;
#endif
	MEA_Uint32 sysType= MEA_MODE_REGULAR_ENET3000; ///MEA_IF_VER_LX_VERSION_H_SYSTYPE_ENET4000_VAL;

#if (defined(MEA_OS_DEVICE_MEMORY_SIMULATION)) 
            sysType=MEA_MODE_REGULAR_ENET3000;
#endif


//////////////////////////////////////////////////////////////////////////
 mea_initSem();
//////////////////////////////////////////////////////////////////////////


#if defined(HW_BOARD_IS_PCI)
 /* no need to work wit Memory map*/

    globalMemoryMode=MEA_MODE_REGULAR_ENET3000;

    if ((module_base_addr_vec[MEA_MODULE_DATA_DDR] != 0x00000000)) {
        
        device_memory[MEA_MODULE_DATA_DDR].moduleOffset = module_base_addr_vec[MEA_MODULE_DATA_DDR];
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "[MEA_MODULE_DATA_DDR].moduleOffset =0x%08x\n", device_memory[MEA_MODULE_DATA_DDR].moduleOffset);
    }

 return MEA_OK;
#endif 

	/* init CPLD space */
    if ((module_base_addr_vec[MEA_MODULE_CPLD] != 0x00000000) &&
        (MEA_OS_DeviceMemoryMap(module_base_addr_vec[MEA_MODULE_CPLD],
                               MEA_OS_GetPageSize(),
                               &cpld_base_addr,
                               &cpld_memory) != MEA_OK)) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Failed to map CPLD memory address 0x%08x\n",
                        __FUNCTION__,module_base_addr_vec[MEA_MODULE_CPLD]);
       return MEA_ERROR;
    } 

#if 1
    if ((module_base_addr_vec[MEA_MODULE_DATA_DDR] != 0x00000000)) {
        device_memory[MEA_MODULE_DATA_DDR].moduleOffset = module_base_addr_vec[MEA_MODULE_DATA_DDR];

    }
#else
    if ((module_base_addr_vec[MEA_MODULE_DATA_DDR] != 0x00000000) &&
        (MEA_OS_DeviceMemoryMap(module_base_addr_vec[MEA_MODULE_DATA_DDR],
        300*1024*1024 /*(MEA_OS_GetPageSize() )*/, /*512 * 1024 * 1024*/
        &(device_memory[MEA_MODULE_DATA_DDR].base_address),
        &(device_memory[MEA_MODULE_DATA_DDR].memory)) != MEA_OK)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Failed to map DATA_DDR memory address 0x%08x\n",
            __FUNCTION__, module_base_addr_vec[MEA_MODULE_DATA_DDR]);
        return MEA_ERROR;
    }
#endif



    


	/* init IF space */
   if (MEA_OS_DeviceMemoryMap(module_base_addr_vec[MEA_MODULE_IF],
                              MEA_OS_GetPageSize(),
                              &(device_memory[MEA_MODULE_IF].base_address),
                              &(device_memory[MEA_MODULE_IF].memory)) != MEA_OK) {
	  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - Failed to map IF memory address 0x%08x\n",
                        __FUNCTION__,
                        module_base_addr_vec[MEA_MODULE_IF] );
	  return MEA_ERROR;
   }
	/* check if we need to change the BM space */
#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
   
    if (!b_bist_test){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Reading sysType\n");
	    hwLxversion=MEA_LowLevel_ReadReg(MEA_UNIT_0,MEA_IF_VER_LX_VERSION_H_REG,MEA_MODULE_IF,MEA_MODE_UPSTREEM_ONLY,MEA_MEMORY_READ_IGNORE);
		sysType=((hwLxversion &(MEA_IF_VER_LX_VERSION_H_SYSTYPE_MASK))
			     >> MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_H_SYSTYPE_MASK));
    }   

#endif

#if (!defined(MEA_OS_ETH_PROJ_0))
   if(sysType != MEA_IF_VER_LX_VERSION_H_SYSTYPE_SINGLECORE_VAL){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT," Change to work SingleCore \n");
     sysType=MEA_IF_VER_LX_VERSION_H_SYSTYPE_SINGLECORE_VAL;
   }
#endif


        { /* Enet3000)*/
		
            if(sysType==MEA_IF_VER_LX_VERSION_H_SYSTYPE_SINGLECORE_VAL || b_bist_test == MEA_TRUE){
             
              MEA_board_cs_Type=2;
              MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"SysType single-Core\n");
              globalMemoryMode=MEA_MODE_REGULAR_ENET3000;
#if  defined (HW_BOARD_IS_PCI) || defined(AXI_TRANS)
              

              device_memory[MEA_MODULE_BM].base_address = device_memory[MEA_MODULE_IF].base_address;
              device_memory[MEA_MODULE_BM].memory        = device_memory[MEA_MODULE_IF].memory;


              MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, " PCI_CARD OR AXI_TRANS \n");
#else
              physical_mem_addr = module_base_addr_vec[MEA_MODULE_IF] + MEA_ENET4000_BM_OFFSET_VAL;

              if (MEA_OS_DeviceMemoryMap(physical_mem_addr,
                                      MEA_OS_GetPageSize(),
                                      &(device_memory[MEA_MODULE_BM].base_address),
                                      &(device_memory[MEA_MODULE_BM].memory)) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - Failed to map BM memory address 0x%08x\n",
                                __FUNCTION__,
                                physical_mem_addr);
                return MEA_ERROR;
              }
#endif
                
            } else {
            /* init BM space */
			globalMemoryMode=MEA_MODE_REGULAR_ENET3000;
            MEA_board_cs_Type=0;
			if (MEA_OS_DeviceMemoryMap(module_base_addr_vec[MEA_MODULE_BM],
									   MEA_OS_GetPageSize(),
									  &(device_memory[MEA_MODULE_BM].base_address),
									  &(device_memory[MEA_MODULE_BM].memory)) != MEA_OK) {
				 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
								   "%s - Failed to map BM memory address 0x%08x\n",
								   __FUNCTION__,module_base_addr_vec[MEA_MODULE_BM]);
				 return MEA_ERROR;
			    }
            }

		}

      



    return MEA_OK;   
}


MEA_Status mea_drv_Conclude_MemoryMap() {

#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
	//MEA_Uint32 hwLxversion=0;
#endif
	//MEA_Uint32 sysType=0;

#ifndef MEA_OS_DEVICE_MEMORY_SIMULATION
#if  defined(HW_BOARD_IS_PCI)
   // sysType=0;

    MEA_PCIe_Conclude(MEA_UNIT_0);
    return MEA_OK;
#else // !(HW_BOARD_IS_PCI)
   
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Reading sysType\n");
//	    hwLxversion=MEA_API_ReadReg(MEA_UNIT_0,MEA_IF_VER_LX_VERSION_H_REG,MEA_MODULE_IF);
// 		sysType=((hwLxversion &(MEA_IF_VER_LX_VERSION_H_SYSTYPE_MASK))
// 			     >> MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_H_SYSTYPE_MASK));
#endif




    if ((cpld_memory != -1) &&
        (MEA_OS_DeviceMemoryUnMap (cpld_base_addr,
                                  MEA_OS_GetPageSize(),
                                  cpld_memory) != MEA_OK)) {
        return MEA_ERROR;
    }


    





	
	{ /* Enet3000 */
		MEA_OS_DeviceMemoryUnMap(device_memory[MEA_MODULE_BM].base_address,
								 MEA_OS_GetPageSize(),
								 device_memory[MEA_MODULE_BM].memory);
	
		MEA_OS_DeviceMemoryUnMap(device_memory[MEA_MODULE_IF].base_address,
                                MEA_OS_GetPageSize(),
                                device_memory[MEA_MODULE_IF].memory);
#ifdef AXI_TRANS
#if !defined(HW_BOARD_IS_PCI)
        if(MEA_DATA_DDR_BASE_ADDR != 0){
            MEA_OS_DeviceMemoryUnMap(device_memory[MEA_MODULE_DATA_DDR].base_address,
                MEA_OS_GetPageSize(),
                device_memory[MEA_MODULE_DATA_DDR].memory);
        }
#endif
#endif // AXI_TRANS

	}	
#endif
  
    return MEA_OK;

}



MEA_ULong_t MEA_GetMemoryId  (MEA_module_te module, MEA_ULong_t Addr) {
    MEA_ULong_t mem_id = 0x0; //-1

    switch (module) {
    case MEA_MODULE_CPLD:
         mem_id = cpld_memory;
         break;
    case MEA_MODULE_BM :
	case MEA_MODULE_IF :
    case MEA_MODULE_DATA_DDR:
    case MEA_MODULE_BM_DOWNSTREEM :
    case MEA_MODULE_IF_DOWNSTREEM :
         mem_id = device_memory[module].memory;
         break;
    default:
         MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                           "%s - Invalid module type (%d) \n",__FUNCTION__,module);
         mem_id = 0x0; //-1;
         break;
    }
    return mem_id;
}

MEA_ULong_t  MEA_GetMemoryAddr(MEA_module_te module, MEA_ULong_t *Addr) {
    MEA_ULong_t mem_base_addr = 0;

    switch (module) {
    case MEA_MODULE_CPLD:
         mem_base_addr = cpld_base_addr;
         break;
    case MEA_MODULE_BM :
	case MEA_MODULE_IF :
    case MEA_MODULE_DATA_DDR:
    case MEA_MODULE_BM_DOWNSTREEM :
    case MEA_MODULE_IF_DOWNSTREEM :
         mem_base_addr = device_memory[module].base_address;
		 break;
    default:
         MEA_OS_LOG_logMsg(MEA_OS_LOG_FATAL,
                          "%s - Invalid module type (%d) \n",__FUNCTION__,module);
         mem_base_addr = 0;
         break;
    }
   
    return mem_base_addr;

}

/*----------------------------------------------------------------------*/
/*                                                                      */
/*                  <mea_drv_GetCurrentHwUnit>                          */
/*                                                                      */
/*----------------------------------------------------------------------*/
MEA_Status mea_drv_GetCurrentHwUnit(MEA_Unit_t unit_i,
									MEA_db_HwUnit_t *hwUnit_o) {

	if (hwUnit_o == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - hwUnit_o == NULL \n",
						  __FUNCTION__);
		return MEA_ERROR;
	}

	switch (globalMemoryMode) {
	case MEA_MODE_REGULAR_ENET3000: 
		*hwUnit_o = MEA_HW_UNIT_ID_GENERAL; 
		break;
	case MEA_MODE_UPSTREEM_ONLY:
		*hwUnit_o = MEA_HW_UNIT_ID_UPSTREAM; 
		break;
	case MEA_MODE_DOWNSTREEM_ONLY:
		*hwUnit_o = MEA_HW_UNIT_ID_DOWNSTREAM; 
		break;
	case MEA_MODE_REGULAR_ENET4000:
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Invalid globalMemoryMode (%d) \n",
						  __FUNCTION__,
						  globalMemoryMode);
		return MEA_ERROR;
		break;
	case MEA_MODE_LAST:
	default: 
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			              "%s - Unsupported globalMemoryMode (%d) \n",
						  __FUNCTION__,
						  globalMemoryMode);
		return MEA_ERROR;
		break;
	}

    return MEA_OK;
}




MEA_Status MEA_drv_common_destroy_mutexs(void)
{

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
  	pthread_mutex_destroy(&MEA_Access_Sem);
  	pthread_mutex_destroy(&MEA_drv_API_Sem);
  	pthread_mutex_destroy(&MEA_drv_FWD_Sem);
  	pthread_mutex_destroy(&MEA_drv_IND_Sem);
#endif

return MEA_OK;
}



MEA_ULong_t mea_drv_commn_base_address(MEA_module_te  type)
{

    return device_memory[type].base_address;

}





