/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_init.h"




/*-------------------------------- Includes ------------------------------------------*/



/*-------------------------------- Definitions ---------------------------------------*/



 
/*-------------------------------- External Functions --------------------------------*/


/*-------------------------------- External Variables --------------------------------*/


/*-------------------------------- Forward Declarations ------------------------------*/


/*-------------------------------- Local Variables -----------------------------------*/
/* VER_XXX*/

 

static char* sw_ver = "450.08.112A";


static char* sw_Mydate = "Qct 10 2018";
static char* sw_MyTime = "16:10:00";
#if 1
static char* sw_MyBuild = "TAG_D2018_10_10_T16_10_R450_08_112A";
#else
static char* sw_MyBuild = "---";
#endif
/*-------------------------------- Global Variables ----------------------------------*/



adap_print_version npapi_callback_func = NULL;
adap_print_version adap_callback_func = NULL;


MEA_Status MEA_API_Get_SoftwareVersion(char* buffer, MEA_Uint32 len) {

#if defined(MEA_ADSL_MODE_64)
	char* sw_Type = "(ADSL)";
#else
#if defined(MEA_VDSL_NG) 
	char* sw_Type = "(VDSL-NG)";
#else
#if defined(MEA_SUPPORT_1K_CLUSTER) 
	char* sw_Type = "(_1K)";
#else
	char* sw_Type = "";
#endif
#endif
#endif




	if (buffer == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - buffer == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (len < MEA_OS_strlen(sw_ver) + strlen(__DATE__) + MEA_OS_strlen(__TIME__) + MEA_OS_strlen(" ( ) ") + 1) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - len %d is too small\n", __FUNCTION__, len);
		return MEA_ERROR;
	}
#if 0
	MEA_OS_sprintf(buffer, "%s%s (%s %s)", sw_ver, sw_Type, __DATE__, __TIME__);
#else
	MEA_OS_sprintf(buffer, "%s%s (%s %s)", sw_ver, sw_Type, sw_Mydate, sw_MyTime);
#endif
	return MEA_OK;


}

MEA_Status MEA_API_Get_SoftwareBuildTag(char *buffer, MEA_Uint32 len)
{

	if (buffer == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - buffer == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}

	if (len < MEA_OS_strlen(sw_MyBuild) + 1) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - len %d is too small\n", __FUNCTION__, len);
		return MEA_ERROR;
	}

	MEA_OS_sprintf(buffer, "%s ", sw_MyBuild);
	return MEA_OK;
}

MEA_Status MEA_API_Get_HwVersion(MEA_HwVersion_info_t* version) {

	MEA_Uint32 getVersion = 0;

	if (b_bist_test) {
		return MEA_OK;
	}

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS    
	if (version == NULL)
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - version == NULL\n", __FUNCTION__);
		return MEA_ERROR;
	}
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

	MEA_IF_HW_ACCESS_ENABLE
		getVersion = MEA_API_ReadReg(MEA_UNIT_0, ENET_IF_VER_LX_VERSION_L_REG, MEA_MODULE_IF);
	MEA_IF_HW_ACCESS_DISABLE
		version->if_reg_HWversion.val.product_number =
		((getVersion &(MEA_IF_VER_LX_VERSION_L_PRODUCT_NUMBER_MASK))
			>> MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_L_PRODUCT_NUMBER_MASK));
	version->if_reg_HWversion.val.phase_version =
		((getVersion &(MEA_IF_VER_LX_VERSION_L_PHASE_VERSION_MASK))
			>> MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_L_PHASE_VERSION_MASK));

	version->if_reg_HWversion.val.release_number =
		((getVersion &(MEA_IF_VER_LX_VERSION_L_RELEASE_NUMBER_MASK))
			>> MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_L_RELEASE_NUMBER_MASK));



	version->if_reg_HWversion.val.vlsi_int_version =
		((getVersion &(MEA_IF_VER_LX_VERSION_L_VLSI_INT_VERSION_MASK))
			>> MEA_OS_calc_shift_from_mask(MEA_IF_VER_LX_VERSION_L_VLSI_INT_VERSION_MASK));

	version->if_reg_HWdate.reg_ver = MEA_API_ReadReg(MEA_UNIT_0, ENET_IF_VERSION_DATE_REG, MEA_MODULE_IF);

	return MEA_OK;
}

MEA_Status MEA_SetAdapCallBackFunction(adap_print_version adap_callback)
{
	adap_callback_func = adap_callback;


	return MEA_OK;
}

MEA_Status MEA_GetAdapCallBackFunction(char *buffer, MEA_Uint32 len)
{

	if (adap_callback_func != NULL)
	{
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "EnetNPAPI:\r\n");
		adap_callback_func(buffer, len);
	}
	return MEA_OK;

}

MEA_Status MEA_SetNpapiCallBackFunction(adap_print_version npapi_callback)
{
	npapi_callback_func = npapi_callback;


	return MEA_OK;
}

MEA_Status MEA_GetNpapiCallBackFunction(char *buffer, MEA_Uint32 len)
{

	if (npapi_callback_func != NULL)
	{
		//MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "EnetNPAPI:\r\n");
		npapi_callback_func(buffer, len);
	}
	return MEA_OK;

}
