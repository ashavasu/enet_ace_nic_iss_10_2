/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                                            */
/*----------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_bfd_drv.h"


/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/


/* HW BFD config*/

#define MEA_BFD_CONF_HW_EXPECTED_MEP_ID_WIDTH                16
#define MEA_BFD_CONF_HW_EXPECTED_MEG_ID_WIDTH                16
#define MEA_BFD_CONF_HW_EXPECTED_PERID_WIDTH                 3
#define MEA_BFD_CONF_HW_VALID_WIDTH                          1  /* counter enable */

#define MEA_BFD_PERIOD_PROF_CIR_WIDTH    11
#define MEA_BFD_PERIOD_PROF_CBS_WIDTH    20
#define MEA_BFD_PERIOD_PROF_FAST_WIDTH    1

#define MEA_BFD_PERIOD_BUCKET_XCIR_WIDTH        21
#define MEA_BFD_PERIOD_BUCKET_PROF_WIDTH         8
#define MEA_BFD_PERIOD_BUCKET_PAD_WIDTH          1
#define MEA_BFD_PERIOD_BUCKET_MASK_GROUP_WIDTH   1
#define MEA_BFD_PERIOD_BUCKET_BYPASS_WIDTH       1

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/

typedef struct {
    MEA_Bool            valid;
    MEA_Uint32          num_of_owners;
    MEA_PeriodBFD_dbt   data;
}mea_drv_bfd_PeirodProf_entry_dbt ;



typedef struct {
    MEA_Bool valid;
    MEA_Uint32 num_of_owners;
    MEA_BFD_Configure_dbt  data;
    
}mea_drv_bfd_entry_dbt;

typedef struct {

    MEA_Uint32 expected_MEP_Id  :16; /*0 15*/
    MEA_Uint32 expected_MEG_Id  :16; /*16 31*/
    MEA_Uint32 expected_period  : 3; /*32 34*/
    MEA_Uint32 counter_en       : 1; /*35 35*/ //counter
    MEA_Uint32 pad              : 28;/*36 63*/
}mea_BFD_drv_hwEntry_dbt;

typedef struct {
    MEA_Uint32 xcir       :21;
    MEA_Uint32 profId     :8;
    MEA_Uint32 mask_group :1;
    MEA_Uint32 reserve    : 1;
    MEA_Uint32 block : 1;
}MEA_PeriodBFD_Bucket_dbt;



typedef struct {
    MEA_Bool            valid;
    MEA_Bool            current_event;
    MEA_Bool            previous_event;
    MEA_Uint8           state;
    MEA_Uint8           count;
}mea_drv_bfd_event_data_dbt ;



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
/* BFD */

static MEA_Counters_BFD_dbt             *MEA_Counters_BFD_Table = NULL ;
static MEA_Bool                         MEA_Counters_BDF_InitDone =MEA_FALSE;



static MEA_Bool                              MEA_BFD_InitDone = MEA_FALSE;
static mea_drv_bfd_entry_dbt                *MEA_BFD_Table = NULL ;

static mea_drv_bfd_PeirodProf_entry_dbt      *MEA_BFD_PeirodProf_Table = NULL ;

#ifdef	MEA_BFD_BY_SW 
static MEA_Uint32 MEA_CHECK_BFD_SEM_FOR=0;
static mea_drv_bfd_event_data_dbt            *MEA_BFD_EVENT_BFD_Table = NULL;


//static MEA_Uint32 MEA_CHECK_BFD_SEM_FOR = 0;
static MEA_BFD_Event_group_dbt              MEA_BFD_EVENT_group;
static mea_drv_ccm_event_data_dbt           *MEA_BFD_EVENT_Table = NULL;

#endif

/*******************************************************/



static MEA_Status mea_drv_Create_periodBFD(MEA_Unit_t               unit_i,
                                           MEA_PeriodBFD_dbt        *entry_pi,
                                           MEA_periodBFD_Id_t       *id_io);





static MEA_Status mea_drv_Get_periodBFD_Entry(MEA_Unit_t                unit_i,
                                              MEA_BFDId_t               id_i,
                                              MEA_PeriodBFD_dbt         *entry_po);

static MEA_Status mea_drv_periodBFD_del_owner (MEA_Unit_t            unit_i, 
                                                  MEA_periodBFD_Id_t    id_i);

static MEA_Status MEA_drv_Get_BFD_Event(MEA_Unit_t    unit, MEA_BFD_Event_group_dbt *entry, MEA_Bool *exist);
static MEA_Status MEA_drv_Get_BFD_Event_GroupId(MEA_Unit_t    unit,MEA_Uint32 groupId, MEA_Uint32 *Event);

static MEA_Status mea_BFD_Count_UpdateHw(MEA_Unit_t  unit_i, MEA_BFDId_t  id_i);


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/




/*!--------------------------------------------------------------------------!*/
/*!                                                                          !*/
/*!                              Counters_CCM                                !*/
/*!                                                                          !*/
/*!                                                                          !*/
/*!--------------------------------------------------------------------------!*/
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Conclude_CCM_Counters>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Conclude_BFD_Counters(MEA_Unit_t  unit_i )
{

    if(MEA_Counters_BFD_Table !=NULL){
        MEA_OS_free(MEA_Counters_BFD_Table);
        MEA_Counters_BFD_Table = NULL;
    }

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Init_BFD_Counters>                                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_Init_BFD_Counters(MEA_Unit_t  unit_i )
{
    MEA_Uint32 size;

    if (!MEA_BFD_SUPPORT)
    {
       
        return ENET_OK;
    }

    /* Verify not already init */
    if (MEA_Counters_BDF_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Already init \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    MEA_Counters_BDF_InitDone = MEA_FALSE;

    /* Allocate PacketGen Table */
    size = MEA_ANALYZER_BFD_MAX_STREAMS * sizeof(MEA_Counters_BFD_dbt);
    if (size != 0) {
        MEA_Counters_BFD_Table = (MEA_Counters_BFD_dbt*)MEA_OS_malloc(size);
        if (MEA_Counters_BFD_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_Counters_BFD_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_Counters_BFD_Table[0]),0,size);
    }

    if(MEA_PACKET_ANALYZER_BFD_SUPPORT){
   /* Reset Counters RMON */
        if (MEA_API_Clear_Counters_BFDs(unit_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_API_Clear_Counters_BFDs failed\n",
                              __FUNCTION__);
            return MEA_ERROR;
        }
    }


    
    MEA_Counters_BDF_InitDone = MEA_TRUE;

    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <mea_drv_ReInit_CCM_Counters>                              */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
static MEA_Status mea_drv_ReInit_BFD_Counters(MEA_Unit_t unit_i)
{
    MEA_LmId_t id;
    MEA_Counters_BFD_dbt  save_entry_db;
    MEA_Counters_BFD_dbt* entry_db_p;
    MEA_Bool                     exist;
    if (!MEA_BFD_SUPPORT)
    {
        
        return ENET_OK;
    }

    if(!MEA_PACKET_ANALYZER_TYPE2_SUPPORT){
        return MEA_OK;
    }
    /* Scan all stream and read the ANALZER counters to clear from hardware ,
       without update the software shadow */
    for (id=0;id< MEA_ANALYZER_BFD_MAX_STREAMS;id++) {

        entry_db_p = &(MEA_Counters_BFD_Table[id]);
        MEA_OS_memcpy(&save_entry_db,entry_db_p,sizeof(save_entry_db));
        if(MEA_API_IsExist_BFD(unit_i,id,&exist)== MEA_ERROR){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - LM stream id=%d failed n",
                __FUNCTION__,id);
            return ENET_ERROR;
        }
        if(!exist){
            continue ;
        }
        if (MEA_API_Collect_Counters_BFD(unit_i,id,NULL)==MEA_ERROR){
		    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect BFD Counters for id=%d failed\n",
                              __FUNCTION__,id);
            MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));
            return MEA_ERROR;
        }
        MEA_OS_memcpy(entry_db_p,&save_entry_db,sizeof(*entry_db_p));

    }

    /* Return to Callers */
    return MEA_OK;
}



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Collect_Counters_CCM>                             */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Collect_Counters_BFD(MEA_Unit_t                        unit,
                                        MEA_BFDId_t                  id_i,
                                        MEA_Counters_BFD_dbt *entry)
{
    MEA_ind_read_t ind_read;
	MEA_Uint32     read_data[3];
//    MEA_Bool       exist;
    MEA_Uint32     index;
    

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_BFD_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_BFD_SUPPORT are not support \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

    if (!MEA_BFD_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_BFD_SUPPORT  are not support \n",
        __FUNCTION__);
        return ENET_ERROR;
    }
    if (id_i >= MEA_ANALYZER_BFD_MAX_STREAMS) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - BFD id %d  > MAX % \n", __FUNCTION__, id_i, MEA_ANALYZER_BFD_MAX_STREAMS);
        return ENET_ERROR;
    }
#if 0
    if(MEA_API_IsExist_BFD(unit,id_i,&exist)== MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - BFD stream id=%d failed ",
            __FUNCTION__,id_i);
        return ENET_ERROR;
    }
    if(!exist){
        //continue ;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - BFD stream id=%d failed not",
            __FUNCTION__,id_i);
         return ENET_ERROR;
    }
#endif 

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
      
    MEA_OS_memset(&read_data[0],0,sizeof(read_data));

    ind_read.tableType      = ENET_BM_TBL_TYP_BFD_STREAM_COUNTER;
    ind_read.cmdReg         =   MEA_BM_IA_CMD;      
    ind_read.cmdMask        =   MEA_BM_IA_CMD_MASK;      
    ind_read.statusReg      =   MEA_BM_IA_STAT;   
    ind_read.statusMask     =   MEA_BM_IA_STAT_MASK;   
    ind_read.tableAddrReg   =   MEA_BM_IA_ADDR;
    ind_read.tableAddrMask  =   MEA_BM_IA_ADDR_OFFSET_MASK;
    
    for(index=0 ;index < MEA_NUM_OF_ELEMENTS(read_data); index++){
         ind_read.tableOffset    =   (id_i * MEA_BFD_MAX_NUM_OF_COUNTERS )+ index;
        
        ind_read.read_data      =   &(read_data[index]);

        if (MEA_API_ReadIndirect(unit,
                                 &ind_read,
                                 1,
                                 MEA_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - ReadIndirect from tableType %x module BM failed "
                               "(id=%d)\n",
                                __FUNCTION__,ind_read.tableType,id_i);

            return MEA_ERROR;
        }
        /* Update */
     
        
    }
   
   

    MEA_OS_UPDATE_COUNTER(MEA_Counters_BFD_Table[id_i].Rx_packet.val ,(read_data[0]));
    MEA_OS_UPDATE_COUNTER(MEA_Counters_BFD_Table[id_i].Rx_Byte.val ,  (read_data[1]));

    //MEA_OS_UPDATE_COUNTER(MEA_Counters_BFD_Table[id_i].Tx_packet.val, (read_data[2]));
    //MEA_OS_UPDATE_COUNTER(MEA_Counters_BFD_Table[id_i].Tx_Byte.val,   (read_data[3]));

    
    

    /* Update the output Parameter if Supply */
    if ( entry != NULL) {
        MEA_OS_memcpy(entry,
                      &(MEA_Counters_BFD_Table[id_i]),
                      sizeof(*entry));
    }


    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Collect_Counters_CCMs>                             */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_BFDs (MEA_Unit_t unit_i)
{
    MEA_CcmId_t id;
//    MEA_Bool    exist;
    /* Scan all stream and read the ANALZER counters to clear from hardware ,
       without update the software shadow */
    if (!MEA_BFD_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_BFD_SUPPORT are not support \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    for (id=0;id< MEA_ANALYZER_BFD_MAX_STREAMS;id++) {
#if 0
        if(MEA_API_IsExist_BFD(unit_i,id,&exist)== MEA_ERROR){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - LM stream id=%d failed n",
                __FUNCTION__,id);
            return ENET_ERROR;
        }
        if(!exist){
            continue ;
        }
#endif       
        if (MEA_API_Collect_Counters_BFD(unit_i,id,NULL)==MEA_ERROR){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Collect BFD Counters for id=%d failed\n",
                              __FUNCTION__,id);
            return MEA_ERROR;
        }
    }
    
    
    return MEA_OK;
}

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Get_Counters_CCM>                                 */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_BFD(MEA_Unit_t                   unit,
                                    MEA_BFDId_t                  id_i,
                                    MEA_Counters_BFD_dbt *entry)
{
    MEA_Bool     exist;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_BFD_SUPPORT)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_BFD_SUPPORT are not support \n",
                __FUNCTION__);
            return ENET_ERROR;
        }

    if (!MEA_BFD_ENABLE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s  MEA_BFD_ENABLE not support \n",
        __FUNCTION__);
        return ENET_ERROR;
    }
    
    if(entry == NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s \n- entry = NULL\n",
        __FUNCTION__,id_i);
        return ENET_ERROR;
    }
    if(MEA_API_IsExist_BFD(unit,id_i,&exist)==MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - CCM Analyzer stream id=%d is not Exist\n",
            __FUNCTION__,id_i);
        return ENET_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

     MEA_OS_memcpy(entry ,&MEA_Counters_BFD_Table[id_i] ,sizeof(*entry));
    
    return MEA_OK;
}
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Clear_Counters_CCM>                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_BFD(MEA_Unit_t              unit,
                                      MEA_BFDId_t             id_i)
{
 //   MEA_Bool exist;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_BFD_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_BFD_SUPPORT are not support \n",
            __FUNCTION__);
        return ENET_ERROR;
    }
    
    if (!MEA_BFD_ENABLE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - PACKET ANALYZER are not support \n",
        __FUNCTION__);
        return ENET_ERROR;
    }
    if (id_i >= MEA_ANALYZER_BFD_MAX_STREAMS) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - BFD id %d  > MAX % \n",__FUNCTION__, id_i, MEA_ANALYZER_BFD_MAX_STREAMS);
        return ENET_ERROR;
    }

#if 0
    if(MEA_API_IsExist_BFD(unit,id_i,&exist)== MEA_ERROR){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Analyzer stream id=%d failed n",
            __FUNCTION__,id_i);
        return ENET_ERROR;
    }
    if(!exist){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Analyzer stream id=%d is not Exist\n",
        __FUNCTION__,id_i);
        return ENET_ERROR;
    }
#endif

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    
    if(MEA_API_Collect_Counters_BFD(unit,id_i,NULL) !=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_Collect_Counters_BFD id=%d failed  \n",
        __FUNCTION__,id_i);
        return ENET_ERROR;
    }

    MEA_OS_memset(&MEA_Counters_BFD_Table[id_i],0,sizeof(MEA_Counters_BFD_Table[0]));

    return MEA_OK;
}
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Clear_Counters_CCMs>                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Clear_Counters_BFDs(MEA_Unit_t              unit_i)
{
    MEA_CcmId_t             id;
//    MEA_Bool                exist;
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    
    if (!MEA_BFD_SUPPORT)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_BFD_SUPPORT are not support \n",
            __FUNCTION__);
        return ENET_ERROR;
    }


    if (!MEA_BFD_ENABLE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_BFD_ENABLER are not support \n",
            __FUNCTION__);
        return ENET_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    for (id = 0; id < MEA_ANALYZER_BFD_MAX_STREAMS; id++) {
#if 0
        if (MEA_API_IsExist_BFD(unit_i, id, &exist) == MEA_ERROR) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Analyzer BFD stream id=%d failed n",
                __FUNCTION__, id);
            return ENET_ERROR;
        }
        if (!exist) {
            continue;
        }
#endif
        if (MEA_API_Clear_Counters_BFD(unit_i, id) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_Clear_Counters_CCM id=%d failed  \n",
                __FUNCTION__, id);
            return ENET_ERROR;
        }
    }

    return MEA_OK;
}






/*!--------------------------------------------------------------------------!*/
/*!                                                                          !*/
/*!                              periodBFD                                   !*/
/*!                                                                          !*/
/*!                                                                          !*/
/*!--------------------------------------------------------------------------!*/
static MEA_Status mea_periodBFD_Bucket_drv_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_PeriodBFD_Bucket_dbt * entry= (MEA_PeriodBFD_Bucket_dbt * )arg4;

    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    /* Zero initialization the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    count_shift=0;



    MEA_OS_insert_value(count_shift,
        MEA_BFD_PERIOD_BUCKET_XCIR_WIDTH,
        entry->xcir ,
        &val[0]);
    count_shift+=MEA_BFD_PERIOD_BUCKET_XCIR_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_BFD_PERIOD_BUCKET_PROF_WIDTH,
        entry->profId ,
        &val[0]);
    count_shift+=MEA_BFD_PERIOD_BUCKET_PROF_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_BFD_PERIOD_BUCKET_PAD_WIDTH,
        0 ,
        &val[0]);
    count_shift+=MEA_BFD_PERIOD_BUCKET_PAD_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_BFD_PERIOD_BUCKET_MASK_GROUP_WIDTH,
        entry->mask_group,
        &val[0]);
    count_shift += MEA_BFD_PERIOD_BUCKET_MASK_GROUP_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_BFD_PERIOD_BUCKET_BYPASS_WIDTH,
        entry->block,
        &val[0]);
    count_shift+=MEA_BFD_PERIOD_BUCKET_BYPASS_WIDTH;




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }



    return MEA_OK;
}

static MEA_Status mea_periodBFD_Bucket_drv_UpdateHw(MEA_Unit_t                unit_i,
                                                  MEA_BFDId_t                 id_i,
                                                  MEA_PeriodBFD_Bucket_dbt    *new_entry_pi,
                                                  MEA_PeriodBFD_Bucket_dbt    *old_entry_pi)
{

    MEA_db_HwUnit_t hwUnit;
    MEA_ind_write_t ind_write;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(!new_entry_pi){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s  new_entry_pi = NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return 0;)

        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi))== 0) ) { 
                return MEA_OK;
        }


        ind_write.tableType         = ENET_BM_TBL_PERIOD_BUCKET_STREAM_BFD;
        ind_write.tableOffset       = id_i ;
        ind_write.cmdReg            = ENET_BM_IA_CMD;      
        ind_write.cmdMask           = ENET_BM_IA_CMD_MASK;      
        ind_write.statusReg         = ENET_BM_IA_STAT;   
        ind_write.statusMask        = ENET_BM_IA_STAT_MASK;   
        ind_write.tableAddrReg      = ENET_BM_IA_ADDR;
        ind_write.tableAddrMask     = ENET_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry        =  (MEA_FUNCPTR)mea_periodBFD_Bucket_drv_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        ind_write.funcParam2 = (MEA_funcParam)((long) hwUnit);
        ind_write.funcParam3 = (MEA_funcParam)((long) id_i);
        ind_write.funcParam4 = (MEA_funcParam)(new_entry_pi);

        if (MEA_API_WriteIndirect(ENET_UNIT_0,&ind_write,MEA_MODULE_BM) != ENET_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_API_WriteIndirect failed "
                "TableType=%d , TableOffset=%d , ENET_module=%d\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                ENET_MODULE_BM);
            return ENET_ERROR;
        } 



        return MEA_OK;
}




static MEA_Status mea_periodBFD_prof_drv_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_PeriodBFD_dbt * entry= (MEA_PeriodBFD_dbt * )arg4;

    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    /* Zero initialized the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    count_shift=0;

    MEA_OS_insert_value(count_shift,
        MEA_BFD_PERIOD_PROF_CIR_WIDTH,
        entry->cir ,
        &val[0]);
    count_shift+=MEA_BFD_PERIOD_PROF_CIR_WIDTH;
   
    MEA_OS_insert_value(count_shift,
        MEA_BFD_PERIOD_PROF_CBS_WIDTH,
        entry->cbs ,
        &val[0]);
    count_shift+=MEA_BFD_PERIOD_PROF_CBS_WIDTH;

    MEA_OS_insert_value(count_shift,
        MEA_BFD_PERIOD_PROF_FAST_WIDTH,
        entry->fast_slow ,
        &val[0]);
    count_shift+=MEA_BFD_PERIOD_PROF_FAST_WIDTH;




    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
      MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }



    return MEA_OK;
}

static MEA_Status mea_periodBFD_prof_drv_UpdateHw(MEA_Unit_t                  unit_i,
                                                  MEA_PacketGen_profile_info_t             id_i,
                                                  MEA_PeriodBFD_dbt    *new_entry_pi,
                                                  MEA_PeriodBFD_dbt    *old_entry_pi)
{
 
    MEA_db_HwUnit_t hwUnit;
    MEA_ind_write_t      ind_write;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(!new_entry_pi){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s  new_entry_pi = NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
#endif /* ENET_SW_CHECK_INPUT_PARAMETERS */


    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return 0;)

    if ((old_entry_pi) &&
        (MEA_OS_memcmp(new_entry_pi,
                       old_entry_pi,
                        sizeof(*new_entry_pi))== 0) ) { 
                return MEA_OK;
    }


    ind_write.tableType         = ENET_BM_TBL_TYP_BFD_CC_BUCKET_PROF;
    ind_write.tableOffset       = id_i ;
    ind_write.cmdReg            = ENET_BM_IA_CMD;      
    ind_write.cmdMask           = ENET_BM_IA_CMD_MASK;      
    ind_write.statusReg         = ENET_BM_IA_STAT;   
    ind_write.statusMask        = ENET_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg      = ENET_BM_IA_ADDR;
    ind_write.tableAddrMask     = ENET_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry        =  (MEA_FUNCPTR)mea_periodBFD_prof_drv_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)id_i);
    ind_write.funcParam4 = (MEA_funcParam)(new_entry_pi);

    if (MEA_API_WriteIndirect(ENET_UNIT_0,&ind_write,MEA_MODULE_BM) != ENET_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_API_WriteIndirect failed "
            "TableType=%d , TableOffset=%d , ENET_module=%d\n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            ENET_MODULE_BM);
        return ENET_ERROR;
    } 


    
    return MEA_OK;
}

#define MEA_PERIOD_CONST_CBS_BFD 16000
/************************************************************************/
/*                                                                      */
/************************************************************************/


MEA_Status mea_drv_Init_periodBFD_prof(MEA_Unit_t  unit_i )
{
    MEA_Uint32 size;     
    MEA_periodBFD_Id_t id;
    MEA_PeriodBFD_dbt    entry;
    MEA_PeriodBFD_dbt    entrySet[MEA_BFD_MAX_NUM_OF_BUCKET_PROF];


    if (!MEA_BFD_ENABLE){
        return MEA_OK;
    }


    /* Allocate MEA_CCM_PeirodProf_Table Table */
    size = MEA_BFD_MAX_NUM_OF_BUCKET_PROF * sizeof(mea_drv_bfd_PeirodProf_entry_dbt );
    if (size != 0) {
        MEA_BFD_PeirodProf_Table = (mea_drv_bfd_PeirodProf_entry_dbt *)MEA_OS_malloc(size);
        if (MEA_BFD_PeirodProf_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_CCM_PeirodProf_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_BFD_PeirodProf_Table[0]),0,size);
    }
    MEA_OS_memset(&entry,0,sizeof(entry));

    for (id=0;id<MEA_BFD_MAX_NUM_OF_BUCKET_PROF;id++) {
        
        if(mea_periodBFD_prof_drv_UpdateHw(unit_i,
                id,
                &entry,
                NULL) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  mea_periodCCM_prof_drv_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                    return MEA_ERROR;
            }
        
    }

/************************************************************************/
/*      create the profile after initialization                         */
/************************************************************************/
   MEA_OS_memset(&entrySet,0,sizeof(entrySet));
#if 1 
   entrySet[MEA_BFD_PERIOD_EVENT_3_3MS].cir = 100;
   entrySet[MEA_BFD_PERIOD_EVENT_3_3MS].cbs = MEA_PERIOD_CONST_CBS_BFD;//10000;
   entrySet[MEA_BFD_PERIOD_EVENT_3_3MS].fast_slow = MEA_TRUE;

   entrySet[MEA_BFD_PERIOD_EVENT_10MS].cir = 33;
   entrySet[MEA_BFD_PERIOD_EVENT_10MS].cbs = MEA_PERIOD_CONST_CBS_BFD;//10000;
   entrySet[MEA_BFD_PERIOD_EVENT_10MS].fast_slow = MEA_TRUE;

   entrySet[MEA_BFD_PERIOD_EVENT_100MS].cir = 3;
   entrySet[MEA_BFD_PERIOD_EVENT_100MS].cbs = MEA_PERIOD_CONST_CBS_BFD;//10000;
   entrySet[MEA_BFD_PERIOD_EVENT_100MS].fast_slow = MEA_TRUE;

   entrySet[MEA_BFD_PERIOD_EVENT_1S].cir = 400;
   entrySet[MEA_BFD_PERIOD_EVENT_1S].cbs = MEA_PERIOD_CONST_CBS_BFD;//10000;
   entrySet[MEA_BFD_PERIOD_EVENT_1S].fast_slow = MEA_FALSE;

   entrySet[MEA_BFD_PERIOD_EVENT_10S].cir = 40;
   entrySet[MEA_BFD_PERIOD_EVENT_10S].cbs = MEA_PERIOD_CONST_CBS_BFD;//10000;
   entrySet[MEA_BFD_PERIOD_EVENT_10S].fast_slow = MEA_FALSE;

   entrySet[MEA_BFD_PERIOD_EVENT_1MIN].cir = 6;
   entrySet[MEA_BFD_PERIOD_EVENT_1MIN].cbs = MEA_PERIOD_CONST_CBS_BFD;//10000;
   entrySet[MEA_BFD_PERIOD_EVENT_1MIN].fast_slow = MEA_FALSE;

   entrySet[MEA_BFD_PERIOD_EVENT_10MIN].cir = 1;
   entrySet[MEA_BFD_PERIOD_EVENT_10MIN].cbs = MEA_PERIOD_CONST_CBS_BFD;//10000;
   entrySet[MEA_BFD_PERIOD_EVENT_10MIN].fast_slow = MEA_FALSE;/* slow*/
#endif

   for (id=1;id<MEA_BFD_MAX_NUM_OF_BUCKET_PROF;id++) {
       if(mea_drv_Create_periodBFD(unit_i,&entrySet[id],&id)!=MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s -  mea_drv_Create_periodBFD failed (id=%d)\n",__FUNCTION__,id);
           return MEA_ERROR;
       }
   }

    return MEA_OK;
}

MEA_Status mea_drv_ReInit_periodBFD_prof(MEA_Unit_t  unit_i)
{
    MEA_periodCCM_Id_t id;
    if (!MEA_PACKET_ANALYZER_TYPE2_SUPPORT){
        return MEA_OK;
    }

    for (id=0;id<MEA_BFD_MAX_NUM_OF_BUCKET_PROF;id++) {
        if (MEA_BFD_PeirodProf_Table[id].valid){
            if(mea_periodBFD_prof_drv_UpdateHw(unit_i,
                id,
                &(MEA_BFD_PeirodProf_Table[id].data),
                NULL) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  mea_periodCCM_prof_drv_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                    return MEA_ERROR;
            }
        }
    }
    
    return MEA_OK;
}
MEA_Status mea_drv_Conclude_periodBFD_prof(MEA_Unit_t unit_i)
{

    MEA_periodCCM_Id_t id;
      
    if (!MEA_BFD_SUPPORT){
      return MEA_OK;
    }

    if (MEA_BFD_PeirodProf_Table != NULL) {
        for (id=0;id<MEA_BFD_MAX_NUM_OF_BUCKET_PROF;id++) {
            if(MEA_BFD_PeirodProf_Table[id].valid){
                while(MEA_BFD_PeirodProf_Table[id].num_of_owners){
                    if (mea_drv_periodBFD_del_owner(unit_i,id) != MEA_OK) {
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_drv_periodCCM_Delete_owner failed \n",
                        __FUNCTION__);
                        return MEA_OK;
                    }
                }
            }
        }
    }
 

    return MEA_OK;
}

static MEA_Bool mea_drv_periodBFD_prof_find_free(MEA_Unit_t       unit_i, 
                                                 MEA_BFDId_t       *id_io)
 {
     MEA_periodCCM_Id_t id;
     if(*id_io == MEA_PLAT_GENERATE_NEW_ID)
     {
        for (id=0;id<MEA_BFD_MAX_NUM_OF_BUCKET_PROF;id++) {
            if(MEA_BFD_PeirodProf_Table[id].valid == MEA_FALSE){
            *id_io=id;
            return MEA_TRUE;
            }
        }
    }else{
        if(*id_io>= MEA_BFD_MAX_NUM_OF_BUCKET_PROF) {
        return MEA_FALSE;
        }
        if(MEA_BFD_PeirodProf_Table[*id_io].valid == MEA_FALSE){
           
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;
 }

static MEA_Status mea_drv_periodBFD_prof_Exist(MEA_Unit_t       unit_i,
                                                 MEA_BFDId_t       id_i,
                                                 MEA_Bool *exsit_o )
{
    if(exsit_o == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exsit_o = NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
    if (id_i>= MEA_BFD_MAX_NUM_OF_BUCKET_PROF)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id is out of range\n",__FUNCTION__);
        return MEA_ERROR;
    }
    *exsit_o=MEA_FALSE;

    if(MEA_BFD_PeirodProf_Table[id_i].valid == MEA_TRUE){
        *exsit_o=MEA_TRUE;
        return MEA_OK;
    }
    
    return MEA_OK;
}

MEA_Status mea_drv_periodBFD_find_Profile (MEA_Unit_t                    unit_i,                        
                                      MEA_BFDId_t                *id_o,
                                      MEA_PeriodCCM_dbt                 *entry_i,                                 
                                      ENET_Bool                         *found_o)
{
    MEA_periodCCM_Id_t id;

    for (id=0;id<MEA_BFD_MAX_NUM_OF_BUCKET_PROF;id++) {
        if(MEA_BFD_PeirodProf_Table[id].valid){
          if (MEA_OS_memcmp(&MEA_BFD_PeirodProf_Table[id].data,
                             entry_i,sizeof(MEA_BFD_PeirodProf_Table[0].data))==0)
          {
           *id_o=id;
           *found_o=MEA_TRUE;
           return MEA_OK;
          }
          
        }
    }
 
    *found_o=MEA_FALSE;
    return MEA_OK;
}


static MEA_Status mea_drv_periodBFD_add_owner (MEA_Unit_t  unit_i, 
                                               MEA_BFDId_t  id_i)
{
    

    if (id_i >= MEA_BFD_MAX_NUM_OF_BUCKET_PROF)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id is out of range\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(MEA_BFD_PeirodProf_Table[id_i].valid==MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id -is not exist\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if (MEA_BFD_PeirodProf_Table[id_i].valid == MEA_TRUE){
        MEA_BFD_PeirodProf_Table[id_i].num_of_owners++;
    }
    
    return MEA_OK;
}

static MEA_Status mea_drv_periodBFD_del_owner (MEA_Unit_t            unit_i, 
                                               MEA_BFDId_t    id_i)
{
    
    
    if (id_i >=MEA_BFD_MAX_NUM_OF_BUCKET_PROF)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id is out of range\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(MEA_BFD_PeirodProf_Table[id_i].valid==MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id -is not exist\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if ((MEA_BFD_PeirodProf_Table[id_i].valid == MEA_TRUE) && 
        (MEA_BFD_PeirodProf_Table[id_i].num_of_owners == 1))
    {
        MEA_OS_memset(&MEA_BFD_PeirodProf_Table[id_i],0,sizeof(MEA_BFD_PeirodProf_Table[id_i]));
        MEA_BFD_PeirodProf_Table[id_i].valid = MEA_FALSE;
        MEA_BFD_PeirodProf_Table[id_i].num_of_owners=0;
        return MEA_OK;
    }
    
    MEA_BFD_PeirodProf_Table[id_i].num_of_owners--;
    
    return MEA_OK;
}


static MEA_Status mea_drv_Create_periodBFD(MEA_Unit_t               unit_i,
                                          MEA_PeriodBFD_dbt        *entry_pi,
                                          MEA_BFDId_t                *id_io)
{

    if (!MEA_BFD_SUPPORT){
        return MEA_OK;
    }

    // check if we have free space
    if(mea_drv_periodBFD_prof_find_free(unit_i,id_io)!=MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s\n - can't find free space  mea_drv_periodCCM_prof_find_free  \n",
            __FUNCTION__);
        return MEA_ERROR;

    }
    
   // update the HW 
    if(mea_periodBFD_prof_drv_UpdateHw(unit_i,
                                       *id_io,
                                        entry_pi,
                                        NULL)!=MEA_OK){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
          "%s\n - mea_periodCCM_prof_drv_UpdateHw  failed \n",
          __FUNCTION__);
      return MEA_ERROR;
    }


   MEA_OS_memcpy(&(MEA_BFD_PeirodProf_Table[*id_io].data),entry_pi,sizeof(MEA_BFD_PeirodProf_Table[0].data));

   MEA_BFD_PeirodProf_Table[*id_io].valid=MEA_TRUE;
   MEA_BFD_PeirodProf_Table[*id_io].num_of_owners = 1;
    return MEA_OK;
}






static MEA_Status mea_drv_Get_periodBFD_Entry(MEA_Unit_t                         unit_i,
                                              MEA_BFDId_t                 id_i,
                                              MEA_PeriodBFD_dbt                 *entry_po)
{
    
    if (id_i >=MEA_BFD_MAX_NUM_OF_BUCKET_PROF)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id is out of range\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(MEA_BFD_PeirodProf_Table[id_i].valid==MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id -is not exist\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

   MEA_OS_memcpy(entry_po,&(MEA_BFD_PeirodProf_Table[id_i].data),sizeof(*entry_po));

    return MEA_OK;
}





/*!--------------------------------------------------------------------------!*/
/*!                                                                          !*/
/*!                              API BFD                                    !*/
/*!                                                                          !*/
/*!                                                                          !*/
/*!--------------------------------------------------------------------------!*/

/*--------------------------------------------------------------------------*/
/*            <mea_CCM_UpdateHwEntry>                                       */
/*--------------------------------------------------------------------------*/
static MEA_Status mea_bfd_EXPECTED_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                  unit_i   = (MEA_Unit_t                  )arg1;
   //MEA_db_HwId_t              hwId_i     = (MEA_db_HwId_t             )arg2;
   // MEA_Uint32                 offset_i   = (MEA_Uint32                )arg3;
    mea_BFD_drv_hwEntry_dbt   *hwEntry_pi = (mea_BFD_drv_hwEntry_dbt  *)arg4;
    MEA_Uint32                 val[2];
    MEA_Uint32                 i=0;
    MEA_Uint32 count_shift;
    
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }

    count_shift=0;
 
 
  
           

    MEA_OS_insert_value(count_shift,
                            MEA_BFD_CONF_HW_EXPECTED_MEP_ID_WIDTH,
                            0 ,
                            &val[0]);
    count_shift+=MEA_BFD_CONF_HW_EXPECTED_MEP_ID_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_BFD_CONF_HW_EXPECTED_MEG_ID_WIDTH,
                            0 ,
                            &val[0]);
    count_shift+=MEA_BFD_CONF_HW_EXPECTED_MEG_ID_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_BFD_CONF_HW_EXPECTED_PERID_WIDTH,
                            hwEntry_pi->expected_period ,
                            &val[0]);
    count_shift+=MEA_BFD_CONF_HW_EXPECTED_PERID_WIDTH;
    
    MEA_OS_insert_value(count_shift,
        MEA_BFD_CONF_HW_VALID_WIDTH,
        hwEntry_pi->counter_en,
        &val[0]);
    count_shift+=MEA_BFD_CONF_HW_VALID_WIDTH;
    

    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
       MEA_API_WriteReg(unit_i,
                        ENET_BM_IA_WRDATA(i),
                        val[i],
                        MEA_MODULE_BM);  
    }
   
   
    return MEA_OK;
}
/*--------------------------------------------------------------------------*/
/*            <mea_CCM_UpdateHw>                                            */
/*--------------------------------------------------------------------------*/
static MEA_Status mea_BFD_EXPECTED_UpdateHw(MEA_Unit_t                  unit_i,
                            MEA_CcmId_t                 id_i,
                            mea_drv_bfd_entry_dbt      *new_entry_pi,
                            mea_drv_bfd_entry_dbt      *old_entry_pi)
{
    
    MEA_db_HwUnit_t       hwUnit;
    mea_BFD_drv_hwEntry_dbt    hwEntry;
  

    ENET_ind_write_t      ind_write;
   
    
    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    /* check if we have any changes */
    if ((old_entry_pi) &&
        (MEA_OS_memcmp(&new_entry_pi->data,
                       &old_entry_pi->data,
                       sizeof(new_entry_pi->data))== 0)
        ) { 
       return MEA_OK;
    }
    MEA_OS_memset(&hwEntry,0,sizeof(hwEntry));
   
    hwEntry.counter_en = new_entry_pi->data.counter_en;
   

    /* Prepare ind_write structure */
    ind_write.tableType         = ENET_BM_TBL_TYP_BFD_STREAM_EXPECTED_CONFIG;
    ind_write.tableOffset       = id_i; 
    ind_write.cmdReg            = ENET_BM_IA_CMD;      
    ind_write.cmdMask           = ENET_BM_IA_CMD_MASK;      
    ind_write.statusReg         = ENET_BM_IA_STAT;   
    ind_write.statusMask        = ENET_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg      = ENET_BM_IA_ADDR;
    ind_write.tableAddrMask     = ENET_BM_IA_ADDR_OFFSET_MASK;

    ind_write.writeEntry       = (MEA_FUNCPTR)mea_bfd_EXPECTED_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2       = 0; 
    ind_write.funcParam3       = 0; 
    ind_write.funcParam4 = (MEA_funcParam)(&hwEntry);


    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_BM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                              __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
    }
    /* Return to caller */
    return MEA_OK;
}







static MEA_Status mea_BFD_Count_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                  unit_i = (MEA_Unit_t)arg1;


    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
   // MEA_Uint32 count_shift;
    /* Zero initialization the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    
    //count_shift = 0;

    


    /* Update Hw Entry */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++)
    {
        MEA_API_WriteReg(unit_i,
            ENET_BM_IA_WRDATA(i),
            val[i],
            MEA_MODULE_BM);
    }


    return MEA_OK;
}


static MEA_Status mea_BFD_Count_UpdateHw(MEA_Unit_t                  unit_i,
    MEA_CcmId_t                 id_i)
{


    ENET_ind_write_t      ind_write;
    MEA_Uint32 index;

    //  MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    /* check if we have any changes */

    /* Prepare ind_write structure */
    ind_write.tableType = ENET_BM_TBL_TYP_BFD_STREAM_COUNTER;
    ind_write.cmdReg = ENET_BM_IA_CMD;
    ind_write.cmdMask = ENET_BM_IA_CMD_MASK;
    ind_write.statusReg = ENET_BM_IA_STAT;
    ind_write.statusMask = ENET_BM_IA_STAT_MASK;
    ind_write.tableAddrReg = ENET_BM_IA_ADDR;
    ind_write.tableAddrMask = ENET_BM_IA_ADDR_OFFSET_MASK;

    ind_write.writeEntry = (MEA_FUNCPTR)mea_BFD_Count_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = 0;
    ind_write.funcParam3 = 0;
    ind_write.funcParam4 = 0;

    for (index = 0; index < 2; index++){
        ind_write.tableOffset = (id_i * MEA_BFD_MAX_NUM_OF_COUNTERS) + index;
        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i, &ind_write, ENET_MODULE_BM) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_IF);
            return ENET_ERROR;
        }
    }
    /* Return to caller */

    return MEA_OK;
}





static MEA_Status mea_drv_Init_BFD_entry(MEA_Unit_t  unit_i )
{
    
    MEA_Uint32 size;
    MEA_CcmId_t id;
    mea_drv_bfd_entry_dbt entry;
    
    MEA_PeriodBFD_Bucket_dbt          entryBucket;
    if (!MEA_BFD_ENABLE) {
        return MEA_OK;
    }

    /* Verify not already init */
    if (MEA_BFD_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - already init \n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_BFD_InitDone = MEA_FALSE;
    
    /* Allocate PacketGen Table */
    size = MEA_ANALYZER_BFD_MAX_STREAMS * sizeof(mea_drv_bfd_entry_dbt);
    if (size != 0) {
        MEA_BFD_Table = (mea_drv_bfd_entry_dbt*)MEA_OS_malloc(size);
        if (MEA_BFD_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_BFD_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_BFD_Table[0]),0,size);
    }
    MEA_OS_memset(&entry,0,sizeof(entry));
    MEA_OS_memset(&entryBucket,0,sizeof(entryBucket));
    if (MEA_PACKET_ANALYZER_TYPE2_SUPPORT)
    {
        for (id=0;id<MEA_ANALYZER_BFD_MAX_STREAMS;id++) {
            if (mea_BFD_EXPECTED_UpdateHw(unit_i,
                                       id,
                                        &(entry),
                                       NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s -  mea_BFD_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                return MEA_ERROR;
            }
           
            if(mea_BFD_Count_UpdateHw(unit_i, id) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  mea_BFD_Count_UpdateHw failed (id=%d)\n", __FUNCTION__, id);
                return MEA_ERROR;
            }
            /*------------------------------*/
            /* update the bucket of stream */
            /*------------------------------*/
            if(mea_periodBFD_Bucket_drv_UpdateHw(unit_i,id,&entryBucket,NULL)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_periodBFD_Bucket_drv_UpdateHw fail\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }
    }

    

    MEA_BFD_InitDone = MEA_TRUE;

   

    return MEA_OK;
}
static MEA_Status mea_drv_ReInit_BFD_entry(MEA_Unit_t  unit_i)
{
       MEA_CcmId_t id;
       MEA_PeriodBFD_dbt                 entryPeriod;
       MEA_PeriodBFD_Bucket_dbt          entryBucket;

    if (!MEA_BFD_ENABLE){
        return MEA_OK;
    }

    for (id=0;id<MEA_ANALYZER_BFD_MAX_STREAMS;id++) {
        if (MEA_BFD_Table[id].valid){
            if(mea_BFD_EXPECTED_UpdateHw(unit_i,
                                id,
                                &(MEA_BFD_Table[id]),
                                            NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s -  mea_BFD_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                return MEA_ERROR;
            }
            if( mea_drv_Get_periodBFD_Entry(unit_i,
                (MEA_periodBFD_Id_t) MEA_BFD_Table[id].data.period_event,
                &entryPeriod) != MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Get_periodBFD_Entry fail (%d)\n",
                        __FUNCTION__,MEA_BFD_Table[id].data.period_event);
                    return MEA_ERROR;
            }
            /*------------------------------*/
            /* update the bucket of stream */
            /*------------------------------*/
            
            entryBucket.mask_group = MEA_BFD_Table[id].data.mask_group;
            entryBucket.block = MEA_BFD_Table[id].data.block ;

            if (MEA_BFD_Table[id].data.block == MEA_FALSE){
                entryBucket.profId = MEA_BFD_Table[id].data.period_event;
                entryBucket.xcir = entryPeriod.cir;
            }
            else
            {
                entryBucket.profId = 0;
                entryBucket.xcir = 0;
            }

            
           
            

            if(mea_periodBFD_Bucket_drv_UpdateHw(unit_i,id,&entryBucket,NULL)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_periodCCM_Bucket_drv_UpdateHw fail\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
        }
    }

    return MEA_OK;
}
static MEA_Status mea_drv_Conclude_BFD_entry(MEA_Unit_t unit_i)
{
    
    /* Free the table */
    if (MEA_BFD_Table != NULL) {
        MEA_OS_free(MEA_BFD_Table);
        MEA_BFD_Table = NULL;
    }

   

    /* Reset the init done */
    MEA_BFD_InitDone = MEA_FALSE;

    

    /* Return to the caller */
    return MEA_OK;
}

MEA_Status mea_drv_Init_BFD(MEA_Unit_t unit_i){

    if (!MEA_BFD_ENABLE) {
        return MEA_OK;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize BFD configure  ...\n");
    if(mea_drv_Init_periodBFD_prof (unit_i )!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Init_periodBFD_prof failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(mea_drv_Init_BFD_entry(unit_i )!=MEA_OK){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Init_BFD_entry failed\n",__FUNCTION__);
        return MEA_ERROR;
    }

   
    if(mea_drv_Init_BFD_Counters(unit_i )!=MEA_OK){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Init_BFD_Counters failed\n",__FUNCTION__);
        return MEA_ERROR;
    }



    return MEA_OK;
}

MEA_Status mea_drv_Conclude_BFD(MEA_Unit_t unit_i){
    if (!MEA_BFD_SUPPORT)
    {
         return ENET_OK;
    }


    if (!MEA_BFD_ENABLE) {
        return MEA_OK;
    }
    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude BFD..\n");
    if(mea_drv_Conclude_BFD_Counters(unit_i )!=MEA_OK){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Conclude_BFD_Counters failed\n",
                              __FUNCTION__);
        return MEA_ERROR;
    }
  

    if(mea_drv_Conclude_BFD_entry(unit_i )!=MEA_OK){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Conclude_BFD_entry failed\n",
                              __FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_Conclude_periodBFD_prof(unit_i )!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Conclude_periodBFD_prof failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
#ifdef MEA_BFD_BY_SW

   

    if(MEA_BFD_EVENT_BFD_Table !=NULL){
        MEA_OS_free(MEA_BFD_EVENT_BFD_Table);
        MEA_BFD_EVENT_BFD_Table = NULL;
    }

#endif
   
    return MEA_OK;
}

MEA_Status mea_drv_Reinit_BFD(MEA_Unit_t unit_i){

    if (!MEA_BFD_ENABLE) {
        return MEA_OK;
    }
    
    if(mea_drv_ReInit_periodBFD_prof(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_ReInit_periodCCM_prof \n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(mea_drv_ReInit_BFD_entry(unit_i)!=MEA_OK){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - mea_drv_ReInit_Counters_LM \n",__FUNCTION__);
                return MEA_ERROR;
    }
   
    if(mea_drv_ReInit_BFD_Counters(unit_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - mea_drv_ReInit_Counters_CCM \n",__FUNCTION__);
                return MEA_ERROR;
    }
    return MEA_OK;
}





static MEA_Status mea_BFD_drv_check_parameters(MEA_Unit_t                 unit_i ,
                                               MEA_BFD_Configure_dbt     *entry_pi)
{
    MEA_Bool  exsit;
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(entry_pi->period_event >= MEA_BFD_PERIOD_EVENT_LAST) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  period_event out of range\n",
                          __FUNCTION__);
        return MEA_ERROR;

    }
    if(mea_drv_periodBFD_prof_Exist(unit_i,(MEA_periodBFD_Id_t)entry_pi->period_event,&exsit) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  mea_drv_periodBFD_prof_Exist failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(!exsit){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  entry_pi->period_event not Exist \n",
            __FUNCTION__);
        return MEA_ERROR;

    }

    
#endif


    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_PacketGen_drv_find_free_Stream>                       */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Bool mea_BFD_drv_find_free_Stream(MEA_BFDId_t *id_io )
{
    MEA_BFDId_t i;

        for(i=0;i< MEA_ANALYZER_BFD_MAX_STREAMS; i++) {
            if (MEA_BFD_Table[i].valid == MEA_FALSE){
                *id_io=i; 
                return MEA_TRUE;
            }
        }
    
    
    return MEA_FALSE;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_CCM_drv_stream_IsRange>                         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Bool mea_BFD_drv_stream_IsRange(MEA_Unit_t     unit_i,
                                                  MEA_CcmId_t   id_i){
    
    if ((id_i) >= MEA_ANALYZER_BFD_MAX_STREAMS){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                <mea_CCM_drv_stream_IsExist>                         */ 
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_BFD_drv_stream_IsExist(MEA_Unit_t     unit_i,
                                                   MEA_CcmId_t id_i,
                                                   MEA_Bool *exist){
    
    
    if(exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - exist == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    (*exist)=MEA_FALSE;
    
    if (mea_BFD_drv_stream_IsRange(unit_i,id_i) == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_ERROR;
    }
    
    if(MEA_BFD_Table[id_i].valid == MEA_TRUE){
        (*exist)=MEA_TRUE;
    }

    return MEA_OK;
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Create_BFD_Entry>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_BFD_Entry(MEA_Unit_t                  unit_i,
                                    MEA_BFD_Configure_dbt      *entry_pi,
                                    MEA_BFDId_t                *id_io)
{
    MEA_Bool exist;
    mea_drv_bfd_entry_dbt       entry;
    
    MEA_PeriodBFD_dbt                 entryPeriod;
    MEA_PeriodBFD_Bucket_dbt          entryBucket;

    MEA_API_LOG 

  

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_BFD_ENABLE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - CCM not support this version\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_i == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_BFD_drv_check_parameters(unit_i ,entry_pi)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_CCM_drv_check_parameters failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

#endif


        /* Look for stream id */
    if (*id_io != MEA_PLAT_GENERATE_NEW_ID){
        /*check if the id exist*/
        if (mea_BFD_drv_stream_IsExist(unit_i,*id_io,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_BFD_drv_stream_IsExist failed (*id_io=%d\n",
                              __FUNCTION__,*id_io);
            return MEA_ERROR;
        }
        if (exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - *Id_io %d is already exist\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
        }
        

    }else {
        /*find new place*/
        if (mea_BFD_drv_find_free_Stream(id_io)!=MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - mea_BFD_drv_find_free_Stream failed\n",__FUNCTION__);
            return MEA_ERROR;
        }
    }
     
    /************************************************************************/
    /*   period profile                                                  */
    /************************************************************************/
    MEA_OS_memset(&entryPeriod,0,sizeof(entryPeriod));
    MEA_OS_memset(&entryBucket,0,sizeof(entryBucket));

    if(mea_drv_periodBFD_add_owner (unit_i,(MEA_periodBFD_Id_t)entry_pi->period_event)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_periodCCM_add_owner fail\n",
                __FUNCTION__);
            return MEA_ERROR;
    }
        
    if( mea_drv_Get_periodBFD_Entry(unit_i,
                                    (MEA_periodBFD_Id_t) entry_pi->period_event,
                                    &entryPeriod) != MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_periodCCM_add_owner fail (%d)\n",
                                      __FUNCTION__,entry_pi->period_event);
        mea_drv_periodBFD_del_owner(unit_i,(MEA_periodBFD_Id_t)entry_pi->period_event);
        return MEA_ERROR;
    }
      /*------------------------------*/
      /* update the bucket of stream */
      /*------------------------------*/
    entryBucket.block = entry_pi->block ;
    entryBucket.mask_group = entry_pi->mask_group;
    if (entryBucket.block == MEA_FALSE){
        entryBucket.profId = entry_pi->period_event;
        entryBucket.xcir   = entryPeriod.cir;
    }
    else
    {
        entryBucket.profId = 0;
        entryBucket.xcir   = 0;
    }

      
      

      if(mea_periodBFD_Bucket_drv_UpdateHw(unit_i,*id_io,&entryBucket,NULL)!=MEA_OK){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - mea_periodCCM_Bucket_drv_UpdateHw fail\n",
                            __FUNCTION__);
          return MEA_ERROR;
      }
       


    MEA_OS_memset(&entry,0,sizeof(entry));
    MEA_OS_memcpy(&entry.data,entry_pi,sizeof(entry.data));
    entry.valid      = MEA_TRUE;
#if 0    
    entry.expected_MEG_Id =  MEA_OS_crcsum(&entry_pi->MEG_data[0], 
                                           MEA_ANALYZER_CCM_MAX_MEG_DATA,
                                           MEA_CRC_INIT); 
#endif
    if (mea_BFD_EXPECTED_UpdateHw(unit_i,*id_io,&entry,NULL) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_CCM_UpdateHw id %d failed \n",__FUNCTION__,*id_io);
        mea_drv_periodBFD_del_owner(unit_i,(MEA_periodBFD_Id_t)entry_pi->period_event);
        MEA_OS_memset(&entryBucket,0,sizeof(entryBucket));
         mea_periodBFD_Bucket_drv_UpdateHw(unit_i,*id_io,&entryBucket,NULL);
        return MEA_ERROR;
    }
    

   // need to clear 
    
    if (mea_BFD_Count_UpdateHw(unit_i, *id_io) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  mea_BFD_Count_UpdateHw failed (id=%d)\n", __FUNCTION__, *id_io);
        return MEA_ERROR;
    }
     
    
    MEA_OS_memcpy(&MEA_BFD_Table[*id_io],&entry,sizeof(MEA_BFD_Table[0]));
   MEA_BFD_Table[*id_io].valid = MEA_TRUE;
   MEA_BFD_Table[*id_io].num_of_owners =1;
   
   
#ifdef MEA_BFD_BY_SW 
   
   while(MEA_CHECK_CCM_SEM_FOR){
       MEA_OS_sleep(0, 10*1000);
   } 
   MEA_CHECK_BFD_SEM_FOR++;
   if(entry_pi->mode_ccm_lmr==MEA_FALSE){
   MEA_BFD_EVENT_BFD_Table[*id_io].previous_event = MEA_FALSE;
   MEA_BFD_EVENT_BFD_Table[*id_io].valid=MEA_TRUE;

   
   }else{
	    MEA_BFD_EVENT_BFD_Table[*id_io].previous_event = MEA_FALSE;
        MEA_BFD_EVENT_BFD_Table[*id_io].valid=MEA_FALSE;
   }
   MEA_CHECK_BFD_SEM_FOR--;

#endif


    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Delete_CCM_Entry>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Delete_BFD_Entry(MEA_Unit_t                unit_i,
    MEA_BFDId_t               id_i)
{
    MEA_Bool                    exist;
    mea_drv_bfd_entry_dbt       entry;
    MEA_PeriodBFD_Bucket_dbt    entryBucket;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check if support */
    if (!MEA_BFD_ENABLE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_BFD_ENABLE  not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }

    /*check if the id exist*/
    if (mea_BFD_drv_stream_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_PacketGen_drv_stream_IsExist failed (id_i=%d\n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  stream %d not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

     // need to delete the profile of bracket
    MEA_OS_memset(&entry,0,sizeof(entry));
    if (mea_BFD_EXPECTED_UpdateHw(unit_i,
                              id_i,
                              &entry,
                              NULL) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_CCM_UpdateHw id %failed \n",__FUNCTION__,id_i);
            
        return MEA_ERROR;
    }
    MEA_OS_memset(&entryBucket,0,sizeof(entryBucket));

    if(mea_periodBFD_Bucket_drv_UpdateHw(unit_i,id_i,&entryBucket,NULL)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_periodCCM_Bucket_drv_UpdateHw fail\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    //delete the period profile
    if(mea_drv_periodBFD_del_owner (unit_i,
                                       (MEA_BFDId_t)MEA_BFD_Table[id_i].data.period_event) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  mea_drv_periodCCM_Delete_owner id %failed \n",__FUNCTION__,id_i);

        return MEA_ERROR;
    }

    //need to set zero the bucket stream TBD


    MEA_OS_memset(&MEA_BFD_Table[id_i],0,sizeof(MEA_BFD_Table[0]));

    MEA_BFD_Table[id_i].valid = MEA_FALSE;
    MEA_BFD_Table[id_i].num_of_owners = 0;



    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_BFD_Entry>                                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_BFD_Entry(MEA_Unit_t                     unit_i,
                                 MEA_BFDId_t                    id_i,
                                 MEA_BFD_Configure_dbt         *entry_pi)
{

    MEA_Bool                    exist;
    mea_drv_bfd_entry_dbt       entry;
    MEA_BFD_Configure_dbt       entry_old;
//    MEA_Bool                    Period_chang = MEA_FALSE;
    MEA_PeriodBFD_Bucket_dbt          entryBucket;
    MEA_PeriodBFD_dbt                 entryPeriod;



    MEA_API_LOG
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check if support */
    if (!MEA_BFD_ENABLE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - CCM not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    
    /*check if the id exist*/
    if (MEA_API_IsExist_BFD(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_CCM_drv_stream_IsExist failed (id_i=%d\n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - stream %d not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (mea_BFD_drv_check_parameters(unit_i ,entry_pi)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  mea_CCM_drv_check_parameters failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/ 

    


    MEA_OS_memset(&entry,0,sizeof(entry));

   if(MEA_API_Get_BFD_Entry(unit_i,id_i,&entry_old)!=MEA_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
           "%s - MEA_API_Get_BFD_Entry fail\n",
           __FUNCTION__);
       return MEA_ERROR;
        
    }
    
   if(MEA_OS_memcmp(entry_pi,&entry_old,sizeof(*entry_pi)) == 0){
       return  MEA_OK;
   }
   
   

   if((entry_pi->period_event   != entry_old.period_event)  ||
       (entry_pi->mask_group    != entry_old.mask_group)    ||
       (entry_pi->block         != entry_old.block)          
       ){
//     Period_chang = MEA_TRUE;
     if(mea_drv_periodBFD_add_owner (unit_i,(MEA_periodCCM_Id_t)entry_pi->period_event)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - mea_drv_periodCCM_add_owner fail\n",
             __FUNCTION__);
         return MEA_ERROR;
     }
     
    // update the Bucket;
     MEA_OS_memset(&entryBucket,0,sizeof(entryBucket));
     MEA_OS_memset(&entryPeriod,0,sizeof(entryPeriod));
     
     if( mea_drv_Get_periodBFD_Entry(unit_i,
                        (MEA_periodCCM_Id_t) entry_pi->period_event,
                        &entryPeriod) != MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - mea_drv_Get_periodCCM_Entry fail\n",
                 __FUNCTION__);
             mea_drv_periodBFD_del_owner(unit_i,(MEA_periodCCM_Id_t)entry_pi->period_event);
             return MEA_ERROR;
     }
     entryBucket.block = entry_pi->block ;
     entryBucket.mask_group = entry_pi->mask_group;
     if (entryBucket.block == MEA_FALSE){
         entryBucket.profId = entry_pi->period_event;
         entryBucket.xcir = entryPeriod.cir;
     }
     else
     {
         entryBucket.profId = 0;
         entryBucket.xcir = 0;
     }



     if(mea_periodBFD_Bucket_drv_UpdateHw(unit_i,id_i,&entryBucket,NULL)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
             "%s - mea_periodCCM_Bucket_drv_UpdateHw fail\n",
             __FUNCTION__);
         mea_drv_periodBFD_del_owner(unit_i,(MEA_periodCCM_Id_t)entry_pi->period_event);
         return MEA_ERROR;
     }
      mea_drv_periodBFD_del_owner(unit_i,(MEA_periodCCM_Id_t)entry_old.period_event);
   }
   

           
     
    MEA_OS_memcpy(&entry.data,entry_pi,sizeof(entry.data));
    entry.valid      = MEA_TRUE;


    if (mea_BFD_EXPECTED_UpdateHw(unit_i,
                              id_i,
                              &entry,
                              NULL) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s -  mea_BFD_EXPECTED_UpdateHw id %d failed \n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }

    

    
    if (mea_BFD_Count_UpdateHw(unit_i, id_i) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  mea_BFD_Count_UpdateHw failed (id=%d)\n", __FUNCTION__, id_i);
            return MEA_ERROR;
    }
        
    
     
    MEA_OS_memcpy(&MEA_BFD_Table[id_i].data,entry_pi,sizeof(MEA_BFD_Table[0].data));
    


    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_BFD_Entry>                                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_BFD_Entry(MEA_Unit_t                     unit_i,
                                 MEA_BFDId_t                    id_i,
                                 MEA_BFD_Configure_dbt         *entry_po)
{

    MEA_Bool exist;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (!MEA_BFD_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - CCM not support this version\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry_po == NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    if (mea_BFD_drv_stream_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_BFD_drv_stream_IsExist failed (id_i=%d\n",
                          __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - ccm stream %d not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
   
    
#endif //MEA_SW_CHECK_INPUT_PARAMETERS


     /* Copy to caller structure */
    MEA_OS_memcpy(entry_po ,
                  &(MEA_BFD_Table[id_i].data),
                  sizeof(*entry_po));



    
    return MEA_OK;
}

MEA_Status MEA_API_GetFirst_BFD_Entry(MEA_Unit_t                unit_i,
                                     MEA_BFDId_t              *id_o,
                                      MEA_BFD_Configure_dbt    *entry_po, /* Can be NULL */
                                      MEA_Bool                 *found_o)
{
     MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    if (!MEA_PACKET_ANALYZER_TYPE2_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Analyzer for ccm not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    
    if (id_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
    if (entry_po == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry_po == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
    if (found_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ( (*id_o)=0;
          (*id_o) < MEA_ANALYZER_MAX_STREAMS_TYPE2; 
          (*id_o)++ ) {
        if (MEA_BFD_Table[(*id_o)].valid == MEA_TRUE) {
            if (found_o) {
                *found_o= MEA_TRUE;
            }

            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                              &MEA_BFD_Table[(*id_o)].data,
                              sizeof(*entry_po));
            }
            break;
        }
    }

    
    
    
    return MEA_OK;
}

MEA_Status MEA_API_GetNext_BFD_Entry(MEA_Unit_t                   unit_i,
                                     MEA_BFDId_t                 *id_io,
                                     MEA_BFD_Configure_dbt       *entry_po, /* Can be NULL */
                                     MEA_Bool                    *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    
    if (!MEA_PACKET_ANALYZER_TYPE2_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Analyzer for ccm not support \n",
                          __FUNCTION__);
        return MEA_ERROR;   
    }
    
    if (id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_io == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
    if (entry_po == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry_po == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }
    if (found_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    }

    
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ( (*id_io)++;
          (*id_io) < MEA_ANALYZER_MAX_STREAMS_TYPE2; 
          (*id_io)++ ) {
        if(MEA_BFD_Table[(*id_io)].valid == MEA_TRUE) {
            if (found_o) {
                *found_o= MEA_TRUE;
            }
            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                    &MEA_BFD_Table[(*id_io)].data,
                              sizeof(*entry_po));
            }
            break;
        }
    }
    
    
    return MEA_OK;
}

MEA_Status MEA_API_IsExist_BFD(MEA_Unit_t               unit_i,
                               MEA_BFDId_t              id_i,
                               MEA_Bool                 *exist_o)
{
    
   return mea_BFD_drv_stream_IsExist(unit_i,id_i,exist_o);

}

/************************************************************************/
/* BFD                                                                  */
/************************************************************************/




static MEA_Status MEA_drv_Get_BFD_Event(MEA_Unit_t    unit, MEA_BFD_Event_group_dbt *entry, MEA_Bool *exist)
{
    MEA_Uint32 val;


    MEA_Uint32                 i;
#ifdef	MEA_BFD_BY_SW
    MEA_Uint32                 j, Event;
    MEA_Uint32                 start, end;
#endif

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_BFD_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -BFD is not support this version\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
    if (exist == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - exist == NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
#endif 
#ifndef MEA_BFD_BY_SW
    *exist = MEA_FALSE;
    MEA_OS_memset(entry, 0, sizeof(*entry));
    val = MEA_API_ReadReg(unit, MEA_BM_BFD_CC_GROUP, MEA_MODULE_BM);

    if (val) {
        *exist = MEA_TRUE;
        for (i = 0; i < MEA_BFD_MAX_GROUP; i++) {
            entry->Group[i] = ((val >> i) & 0x00000001);
        }
    }
#else
    while (MEA_CHECK_CCM_SEM_FOR) {
        MEA_OS_sleep(0, 10 * 1000);
    }
    MEA_CHECK_CCM_SEM_FOR++;
    start = 0;
    end = (MEA_BFD_MAX_STREAMS_TYPE2 / 32);

    for (i = start; i <= end; i++) {
        Event = 0;

        if (MEA_drv_Get_RDI_Event_GroupId(unit, i, &Event) != MEA_OK) {
            MEA_CHECK_CCM_SEM_FOR--;
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - CC_Event_GroupId  failed\n", __FUNCTION__);
            return MEA_ERROR;

        }

        for (j = 0; j < 32; j++)
        {
            val = ((Event >> j) & 0x00000001);
            if (((i * 32) + j) > MEA_ANALYZER_MAX_STREAMS_TYPE2)
                break;
            else {
                if (MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].valid) {
                    MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].current_event = val;
                    if (MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].previous_event == MEA_TRUE) {
                        if (MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].current_event == 0) {
                            MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].count++;
                            MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].current_event = MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].previous_event;
                        }
                        else {
                            MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].count = 0;
                        }
                        if (MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].count == 3) {
                            MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].current_event = val;
                            MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].count = 0;
                        }
                    }
                    if (MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].count == 0) {
                        if (MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].current_event != MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].previous_event) {
                            entry->Group[i] = 1;
                            MEA_CCM_EVENT_RDI_group.Group[i] = 1;
                            *exist = MEA_TRUE;
                        }
                        MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].previous_event = MEA_CCM_EVENT_RDI_Table[((i * 32) + j)].current_event;
                    }
                }
            }
        }
    }
    MEA_CHECK_CCM_SEM_FOR--;




#endif

    return MEA_OK;
}
MEA_Status MEA_drv_Get_BFD_Event_GroupId(MEA_Unit_t    unit, MEA_Uint32 groupId, MEA_Uint32 *Event)
{
    MEA_ind_read_t ind_read;
    MEA_Uint32     read_data[1];



    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_BFD_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -BFD is not support this version\n", __FUNCTION__);
            return MEA_ERROR;
        }
    if (Event == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
    if (groupId > MEA_BFD_MAX_GROUP) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - groupId %d is out of range ",
            __FUNCTION__, groupId);

        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    if (groupId > (MEA_Uint32)(MEA_ANALYZER_BFD_MAX_STREAMS / 32)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - groupId %d is out of range  max group ",
            __FUNCTION__, groupId, (MEA_ANALYZER_BFD_MAX_STREAMS / 32));
    }

    MEA_OS_memset(&read_data[0], 0, sizeof(read_data));

    ind_read.tableType = ENET_BM_TBL_TYP_BFD_CC_EVENT;
    ind_read.cmdReg = MEA_BM_IA_CMD;
    ind_read.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_read.statusReg = MEA_BM_IA_STAT;
    ind_read.statusMask = MEA_BM_IA_STAT_MASK;
    ind_read.tableAddrReg = MEA_BM_IA_ADDR;
    ind_read.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;

    ind_read.tableOffset = groupId;
    ind_read.read_data = &(read_data[0]);

    if (MEA_API_ReadIndirect(unit,
        &ind_read,
        1,
        MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ReadIndirect from tableType %x module BM failed "
            "(id=%d)\n",
            __FUNCTION__, ind_read.tableType, groupId);

        return MEA_ERROR;
    }



    *Event = read_data[0];

    return MEA_OK;

}


/*------------------------------------------------------------------------------ - */
/*                                                                               */
/*                    <MEA_API_Get_BFD_Event>                                    */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_BFD_Event(MEA_Unit_t  unit, 
                                 MEA_BFD_Event_group_dbt *entry, 
                                 MEA_Bool *exist)
{


    MEA_Status ret_val;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_BFD_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -BFD is not support this HW version\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
    if (exist == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - exist == NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
#endif 
#ifndef MEA_BFD_BY_SW
    ret_val = MEA_drv_Get_BFD_Event(unit, entry, exist);
#else
    MEA_OS_memcpy(entry, &MEA_BFD_EVENT_group, sizeof(*entry));

    while (MEA_CHECK_BFD_SEM_FOR) {
        MEA_OS_sleep(0, 10 * 1000);
    }
    ret_val = MEA_drv_Get_BFD_Event(unit, entry, exist);
    if (ret_val == MEA_OK) {
        
        MEA_OS_memset(&MEA_BFD_EVENT_group, 0, sizeof(MEA_BFD_EVENT_group));
        
    }

#endif

    return ret_val;
}


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    <MEA_API_Get_RDI_Event>                                    */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Get_BFD_Event_GroupId(MEA_Unit_t    unit, MEA_Uint32 groupId, MEA_Uint32 *Event)
{
#ifdef	MEA_CCM_RDI_BY_SW    
    MEA_Uint32 i, j;
#endif
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_BFD_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -RDI is not support this version\n", __FUNCTION__);
            return MEA_ERROR;
        }
    if (Event == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry == NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
    if (groupId > MEA_BFD_MAX_GROUP) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - groupId %d is out of range ",
            __FUNCTION__, groupId);

        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (groupId > (MEA_Uint32)(MEA_ANALYZER_BFD_MAX_STREAMS / 32)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - groupId %d is out of range  max group ",
            __FUNCTION__, groupId, (MEA_ANALYZER_BFD_MAX_STREAMS/32));
    }

#ifndef MEA_CCM_RDI_BY_SW
    return MEA_drv_Get_BFD_Event_GroupId(unit, groupId, Event);
#else

    *Event = 0;
    for (i = groupId; i <= groupId; i++) {
        for (j = 0; j < 32; j++)
        {
            if (((i * 32) + j) > MEA_ANALYZER_MAX_STREAMS_TYPE2)
                break;
            else {
                if (MEA_BFD_EVENT_Table[((i * 32) + j)].valid) {
                    *Event |= (MEA_BFD_EVENT_Table[((i * 32) + j)].current_event) << j;
                }

            }
        }
    }

#endif
    return MEA_OK;

}













