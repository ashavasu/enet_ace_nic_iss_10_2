/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_if_l2cp_drv.h"
#include "mea_api.h"
#include "mea_port_drv.h"
#include "mea_drv_common.h"
#include "mea_init.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/

					  

/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
MEA_Bool                    MEA_NetworkL2CP_InitDone = MEA_FALSE;
MEA_L2CP_Entry_dbt	        *MEA_NetworkL2CP_Table = NULL; // [MEA_MAX_PORT_NUMBER + 1];


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_NetworkL2CP_BaseAddr_UpdateHwEntry>                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_NetworkL2CP_BaseAddr_UpdateHwEntry(MEA_MacAddr* mac) { 

    MEA_Uint32 reg[2];

    reg[0]  = (((MEA_Uint32)(mac->b[1] & 0x3f)) << 26); 
    reg[0] |= (((MEA_Uint32)(mac->b[2] & 0xff)) << 18); 
    reg[0] |= (((MEA_Uint32)(mac->b[3] & 0xff)) << 10); 
    reg[0] |= (((MEA_Uint32)(mac->b[4] & 0xff)) <<  2);
    reg[0] |= (((MEA_Uint32)(mac->b[5] & 0xc0)) >>  6); 
     
    reg[1] =  (((MEA_Uint32)(mac->b[0]       )) << 2);
    reg[1] |= (((MEA_Uint32)(mac->b[1] & 0xc0)) >> 6);

	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0),reg[0],MEA_MODULE_IF);
	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(1),reg[1],MEA_MODULE_IF);

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_NetworkL2CP_BaseAddr_UpdateHw>                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_NetworkL2CP_BaseAddr_UpdateHw(MEA_MacAddr* mac,
                                                    MEA_MacAddr* old_mac) 
{

	MEA_ind_write_t ind_write;


    /* Check if we have changes */
    if (old_mac != NULL && 
        (MEA_OS_maccmp(mac,old_mac) == 0)) {
       return MEA_OK;
    }

    /* build the ind_write value */
	ind_write.tableType		= MEA_IF_CMD_PARAM_TBL_TYP_REG;
	ind_write.tableOffset	= MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_DA;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_NetworkL2CP_BaseAddr_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)mac;

	
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }


	return MEA_OK;
	
   	 
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             <mea_drv_Set_NetworkL2CP_BaseAddr>                            */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  mea_drv_Set_NetworkL2CP_BaseAddr(MEA_Unit_t unit,
                                             MEA_MacAddr* mac,
                                             MEA_MacAddr* old_mac)
{
    if (b_bist_test)
        return MEA_OK;

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if (mac == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mac == NULL \n",
                          __FUNCTION__);
		return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */


    /* update Hardware */
    if (mea_NetworkL2CP_BaseAddr_UpdateHw(mac,
                                          old_mac) != MEA_OK) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_NetworkL2CP_Base_UpdateHw failed \n",
                          __FUNCTION__);
		return MEA_ERROR;

    }

    /* return to Caller */
    return MEA_OK;
}





/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_IF_L2CP>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_IF_L2CP(MEA_Unit_t unit_i)
{

    MEA_Port_t port;
	MEA_Uint32 size;

    MEA_L2CP_Entry_dbt entry;

    if(b_bist_test){
        return MEA_OK;
    }
    /* Init IF L2CP Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF L2CP     table ..\n");      

    MEA_NetworkL2CP_InitDone = MEA_FALSE;

	
		size = (MEA_MAX_PORT_NUMBER_INGRESS + 1) * sizeof(MEA_L2CP_Entry_dbt);
		MEA_NetworkL2CP_Table = (MEA_L2CP_Entry_dbt*)MEA_OS_malloc(size);
	if (MEA_NetworkL2CP_Table == NULL) {
		MEA_OS_free(MEA_NetworkL2CP_Table);
		MEA_NetworkL2CP_Table = NULL;

		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s \n- Allocate MEA_NetworkL2CP_Table failed (size=%d)\n",
			__FUNCTION__,
			size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_NetworkL2CP_Table, 0, size);



	
	for(port=0;port<=MEA_MAX_PORT_NUMBER_INGRESS;port++) {
		MEA_OS_memset(&entry,MEA_NETWORK_L2CP_ACTION_8_SUFFIXES_DEF_VAL(port),sizeof(entry)); /* set all port entry to default */
		if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
		   continue;
		}
        if(MEA_GLOBAL_L2CP_SUPPORT){
		if (MEA_API_Set_NetworkL2CP_Entry(unit_i,
										  port,
										  &entry) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - MEA_API_Set_NetworkL2CP_Entry %d failed\n",
								  __FUNCTION__,port);
			return MEA_ERROR;
		}
        }
	}


    MEA_NetworkL2CP_InitDone = MEA_TRUE;


    /* Return to caller */
    return MEA_OK; 

}

MEA_Status mea_drv_Conclude_IF_L2CP(MEA_Unit_t unit_i)
{
	if (MEA_NetworkL2CP_Table) {
		MEA_OS_free(MEA_NetworkL2CP_Table);
	}
	
	return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_IF_L2CP>                                         */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_IF_L2CP(MEA_Unit_t unit_i)
{

    MEA_Port_t port;

	for(port=0;port<=MEA_MAX_PORT_NUMBER_INGRESS;port++) {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
		   continue;
		}

        if (mea_drv_l2cp_def_sid_hw_manager_set(port) != MEA_OK) {
    		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_l2cp_def_sid_hw_manager_set failed (port=%d)\n",
                              __FUNCTION__,port);
		    return MEA_ERROR;
        }
	}


    /* Return to caller */
    return MEA_OK; 

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             <MEA_API_Set_NetworkL2CP_Entry>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_NetworkL2CP_Entry      (MEA_Unit_t          unit,
                                               MEA_Port_t          port,
                                               MEA_L2CP_Entry_dbt* entry) 
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

	if(port > MEA_MAX_PORT_NUMBER){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid port address %d\n",
                          __FUNCTION__,port);
		return MEA_ERROR;
	}

    if (entry == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry == NULL (port=%d)\n",
                          __FUNCTION__,port);
		return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */





    /* Update the software db */
    MEA_OS_memcpy(&(MEA_NetworkL2CP_Table[port]),
                  entry,
                  sizeof(MEA_NetworkL2CP_Table[port]));



	/* Update the l2cp + def sid manager */
	if ( mea_drv_l2cp_def_sid_hw_manager_set(port) != MEA_OK )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_l2cp_def_sid_hw_manager_set failed (port=%d)\n",
                          __FUNCTION__,port);
		return MEA_ERROR;
    }

    /* return to Caller */
    return MEA_OK;

} 


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             <MEA_API_Get_NetworkL2CP_Entry>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_NetworkL2CP_Entry      (MEA_Unit_t          unit,
                                               MEA_Port_t          port,
                                               MEA_L2CP_Entry_dbt* entry) 
{

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

	if(port > MEA_MAX_PORT_NUMBER_INGRESS){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Invalid port address %d\n",
                          __FUNCTION__,port);
		return MEA_ERROR;
	}

    if (entry == NULL) {
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - entry == NULL (port=%d)\n",
                          __FUNCTION__,port);
		return MEA_ERROR;
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */



    /* Update output parameter */
    MEA_OS_memcpy(entry,
                  &(MEA_NetworkL2CP_Table[port]),
                  sizeof(*entry));

    /* return to Caller */
    return MEA_OK;

} 








/******************************************************************************************/
/******************************************************************************************/
/******************************************************************************************/
/*                  L2cp + default SID Manager                                            */
/******************************************************************************************/
/******************************************************************************************/
/******************************************************************************************/







/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_NetworkL2CP_UpdateHwEntry>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_NetworkL2CP_UpdateHwEntry(MEA_L2CP_Entry_dbt* l2cp_entry,
										  MEA_Def_SID_dbt* def_sid_entry,
		                                  MEA_Uint32       phase) {
    int i;
	//MEA_Uint32 def_sid_val;
    MEA_Uint32 val;


	if ((phase == 0) || (phase == 3)) {
		for(i=0;i<3;i++) {
			
            val=0;
            val=l2cp_entry->reg[i];
            if(i==2){
            val=(l2cp_entry->reg[i]& 0x000000ff);
            }
            MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(i),
					         val,
							 MEA_MODULE_IF);
		}
	}

	if ((phase == 1) || (phase == 3)) {
        val=0;
        val=(l2cp_entry->reg[2]>>8);
        val|=(l2cp_entry->reg[3]& 0x000000ff)<<24;
		MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(0),
					     val,
						 MEA_MODULE_IF);

		val=0;
        val=(l2cp_entry->reg[3]>>8);
        val|=((def_sid_entry->def_sid)& 0xff)<<24;
        
        
        MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(1),
					     val,
						 MEA_MODULE_IF);


       val=0;
#ifdef MEA_OS_ADVA_TDM
       val=((def_sid_entry->def_sid)>>8) & 0x1; //only 9bit
       val|=(def_sid_entry->action)<<1;
       
#else
      //support 14bi off default service
       val=((def_sid_entry->def_sid)>>8) & 0x2f;
       val|=(def_sid_entry->action)<<6;
#endif

         MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_WRITE_DATA_REG(2),
					     val,
						 MEA_MODULE_IF);

        
#if 0        
        def_sid_val = 0;

		def_sid_val = ((def_sid_entry->def_sid
			      << MEA_OS_calc_shift_from_mask(MEA_IF_DEFAULT_SID_SID_MASK)
				) & MEA_IF_DEFAULT_SID_SID_MASK
				); 

		def_sid_val |= ((def_sid_entry->action
			      << MEA_OS_calc_shift_from_mask(MEA_IF_DEFAULT_SID_ACTION_MASK)
				 ) & MEA_IF_DEFAULT_SID_ACTION_MASK
				); 

		MEA_API_WriteReg(MEA_UNIT_0,
			             MEA_IF_WRITE_DATA_REG(i),
				         def_sid_val,
					     MEA_MODULE_IF);
#endif
	}

	






}

static MEA_Bool mea_networkL2CP_Mapping(MEA_Port_t port, MEA_Uint32 *index){

    
    MEA_Port_type_te port_type;


    if (MEA_API_Get_IngressPort_Type(MEA_UNIT_0,port,&port_type) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_API_Get_IngressPort_Type failed (port=%d)\n",
            __FUNCTION__,port);
        return MEA_ERROR;
    }

    if(port_type == MEA_PORTTYPE_G999_1MAC_1588 ||
       port_type == MEA_PORTTYPE_G999_1MAC ){
     *index= port*2;
     }else{
#if defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_TDN1)
         *index= port*2;
#else
        switch (port) {

            case   0: *index =   0; break;
            case  12: *index =  24; break;
            case  24: *index =  48; break;
            case  36: *index =  72; break;
            case  48: *index =  96; break;
            case  72: *index =  98; break;
            case 100: *index = 100; break;
            case 101: *index = 102; break;
            case 102: *index = 104; break;
            case 103: *index = 106; break;
            case 104: *index = 108; break;
            case 105: *index = 110; break;
            case 106: *index = 112; break;
            case 107: *index = 114; break;
            case 118: *index = 116; break;
            case 119: *index = 118; break;
            case 120: *index = 120; break;
            case 125: *index = 122; break;
            case 126: *index = 124; break;
            case 127: *index = 126; break;
	    default  :
		    //start_index = ((MEA_Uint32)port & 0x0000003f) << 1;
            *index = 0; // the port that we not support will write to 0
    		
            return MEA_FALSE;
            break;
	    }
#endif
    }
    return MEA_TRUE;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_NetworkL2CP_UpdateHw>                                     */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_NetworkL2CP_UpdateHw(MEA_Port_t port,
                                           MEA_L2CP_Entry_dbt* l2cp_entry,
                                           MEA_Def_SID_dbt* def_sid_entry) 
{

	MEA_Uint32 index;
	MEA_Uint32 start_index=0;
	MEA_Uint32 end_index;
	MEA_ind_write_t ind_write;


    
    if(mea_networkL2CP_Mapping(port,&start_index) == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - mea_networkL2CP_Mapping  failed port = %d not support for L2CP\n",
					          __FUNCTION__,port);
			return MEA_ERROR;
        }
    
    end_index = start_index + 1;


	for (index=start_index;index<=end_index;index++) {

		/* build the ind_write value */
		ind_write.tableType		=ENET_IF_CMD_PARAM_TBL_TYP_L2CP;
		ind_write.tableOffset	= index;
		ind_write.cmdReg		= MEA_IF_CMD_REG;      
		ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
		ind_write.statusReg		= MEA_IF_STATUS_REG;   
		ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
		ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
		ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
		ind_write.writeEntry    = (MEA_FUNCPTR)mea_NetworkL2CP_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)l2cp_entry;
        ind_write.funcParam2 = (MEA_funcParam)def_sid_entry;

        ind_write.funcParam3 = (MEA_funcParam)((long)(index % 2));
    
		/* Write to the Indirect Table */
		if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
				              "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
					          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
			return MEA_ERROR;
		}
	}

	return MEA_OK;
	
   	 
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_l2cp_def_sid_hw_manager_set>                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_l2cp_def_sid_hw_manager_set(MEA_Port_t port)
{
	MEA_IngressPort_Entry_dbt ingress_entry;
	MEA_L2CP_Entry_dbt        l2cp_entry;
    MEA_Uint32                temp_index;
	if ( MEA_API_Get_NetworkL2CP_Entry(MEA_UNIT_0,
                                       port,
                                       &l2cp_entry) != MEA_OK )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_NetworkL2CP_Entry failed\n",
                          __FUNCTION__);
		return MEA_ERROR;
    }

	if ( MEA_API_Get_IngressPort_Entry(MEA_UNIT_0,
                                       port,
                                       &ingress_entry) != MEA_OK )
	{
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_Get_IngressPort_Entry failed\n",
                          __FUNCTION__);
		return MEA_ERROR;
    }
    if(MEA_GLOBAL_L2CP_SUPPORT){
        if (mea_networkL2CP_Mapping(port,&temp_index)== MEA_TRUE){
	        if ( mea_NetworkL2CP_UpdateHw(port,
                                          &l2cp_entry,
								          &(ingress_entry.parser_info.default_sid_info)) != MEA_OK )
	        {
		        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - mea_NetworkL2CP_UpdateHw failed\n",
                                  __FUNCTION__);
		        return MEA_ERROR;
            }
        }
    }
  


    /* return to Caller */
    return MEA_OK;
} 






