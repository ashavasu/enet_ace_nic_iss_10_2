/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/






/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#if 1
#ifdef MEA_OS_LINUX
#ifdef MEA_OS_OC
#include <linux/unistd.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#endif
#else
#include <time.h>
#endif
#endif



#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_if_regs.h"

#include "mea_bonding_drv.h"








/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

#define MEA_MAX_BONDING_TX_PORT_GROUP 16


/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct {
    MEA_Uint32   data_value :16;
    MEA_Uint32   pad  :15;
    MEA_Uint32   valid :1;
}MEA_BondingTx_HwEntry_dbt;


typedef struct {
    MEA_Bool   valid;
    MEA_BondingTx_dbt data;
    MEA_Uint32 num_of_owners;
    
   MEA_BondingTx_HwEntry_dbt hwdata;

}MEA_BondingTx_Entry_dbt;

typedef struct {
    MEA_Bool   valid;
    MEA_BondingCount_dbt data;
}MEA_drv_BondingCount_dbt;

typedef struct{
    MEA_Bool valid;
    MEA_Counter_t  rx_prev_packt;
    MEA_Uint16 state;        //0-start //1-TX_ENABLE 2 TX_DISABLE



}mea_drv_bondingState_dbt;



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

                         
static MEA_BondingTx_Entry_dbt           *MEA_BondingTx_info_Table=NULL;

static MEA_drv_BondingCount_dbt          *MEA_Bonding_Counters_Table = NULL;

mea_drv_bondingState_dbt MEA_Bonding_tbl_state[128];
/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
static MEA_Status mea_drv_BondingTx_check_parameters(MEA_Unit_t         unit_i,
                                                     MEA_Uint32         *id_i,
                                                     MEA_BondingTx_dbt  *entry_pi);





/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

static MEA_Status mea_drv_BondingTx_check_parameters(MEA_Unit_t           unit_i,
                                                        MEA_Uint32           *id_i,
                                                     MEA_BondingTx_dbt    *entry_pi)
{

    MEA_Port_t  temp_port;
    ENET_Uint32 count_port;
 
    MEA_Uint32  value_of_port;
    MEA_Uint32  temp_value;
    MEA_Uint32 j,start_off_set;


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if(entry_pi==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_pi is null\n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(id_i == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id_i is null\n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(entry_pi->num_of_port > MEA_MAX_BONDING_TX_PORT_GROUP){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_pi is null\n",__FUNCTION__);
        return MEA_ERROR;

    }
    
    if(*id_i >=MEA_STANDART_BONDING_NUM_OF_GROUP_TX || *id_i >=12 ){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - id is out of range up to (12) \n",__FUNCTION__);
        return MEA_ERROR;
    }

    if(entry_pi->valid) {
        
        count_port=0;
		
        /*check if only */
        for (temp_port = 0;temp_port <=MEA_MAX_PORT_NUMBER; temp_port++)
        {
            if (((ENET_Uint32*)(&(entry_pi->Multiple_Port.out_ports_0_31)))[temp_port/32] 
            & (1 << (temp_port%32))) {
                if(MEA_API_Get_IsPortValid(temp_port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE)  {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "port %d not Valid port\n",temp_port);
                    return MEA_ERROR;

                }
                if(temp_port > 47){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"port %d not support for bonding\n",temp_port);
                    return MEA_ERROR;
                }
                if(*id_i <= 3){
                    if(temp_port >=16){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"port %d is not part of the group %d only port(0 15)\n",temp_port,id_i);
                        return MEA_ERROR;
                    }
                }
                if(*id_i >= 4 && *id_i <= 7){
                    if(temp_port < 16 || temp_port >=32 ){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"port %d is not part of the group % only port(16 31)\n",temp_port,id_i);
                        return MEA_ERROR;
                    }
                }
                if(*id_i >= 8 && *id_i <= 11){
                    if(temp_port < 32 || temp_port >=48 ){
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"port %d is not part of the group % only port(32 47)\n",temp_port,id_i);
                        return MEA_ERROR;
                    }
                }


                count_port++;
            }
        }
        
        if(count_port != entry_pi->num_of_port){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s \n- num_of_port %d is different as you add %d \n",
                __FUNCTION__,entry_pi->num_of_port,count_port);
            return MEA_ERROR;
        }
    
       //need  group

        if( *id_i <= 3){
           value_of_port = (entry_pi->Multiple_Port.out_ports_0_31 & 0x0000ffff);
           start_off_set=0;
           

        }
        if(*id_i >= 4 && *id_i <= 7){
            start_off_set=4;
          value_of_port = (((entry_pi->Multiple_Port.out_ports_0_31) >>16) & 0x0000ffff); 
        }
        if(*id_i >= 8 && *id_i <= 11){
            start_off_set=8;
           value_of_port = (entry_pi->Multiple_Port.out_ports_32_63 & 0x0000ffff); 
        }

        for(j=start_off_set;j<4+ start_off_set ;j++){
            if (MEA_BondingTx_info_Table[j].valid == MEA_TRUE  && j != *id_i){
                temp_value=0;
                temp_value=MEA_BondingTx_info_Table[j].hwdata.data_value;
                temp_value &= value_of_port;
                if( temp_value ){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s \n-  The port you add is used on other grouping %d \n",
                        __FUNCTION__,j);
                    return MEA_ERROR;
                }
            }

        }
       
    
    
    }








#endif

    return MEA_OK;
}


void mea_drv_BondingTx_UpdateHwEntry(
    MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t           ) arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t     )arg2;
    MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t        )((long) arg3);
    MEA_BondingTx_dbt   *new_entry_pi    = (MEA_BondingTx_dbt *)arg4;




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    MEA_Uint32                  count_shift;
    MEA_Uint32               value_of_port;
    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }


    if (new_entry_pi->valid)
    {

        value_of_port=0;

        if( hwId_i <= 3){
            value_of_port = (new_entry_pi->Multiple_Port.out_ports_0_31 & 0x0000ffff);
        }
        if(hwId_i >= 4 && hwId_i <= 7){

            value_of_port = (((new_entry_pi->Multiple_Port.out_ports_0_31) >>16) & 0x0000ffff); 
        }
        if(hwId_i >= 8 && hwId_i <= 11){

            value_of_port = (new_entry_pi->Multiple_Port.out_ports_32_63 & 0x0000ffff); 
        }

    }else{

        value_of_port=0;
    }


    count_shift=0;

    MEA_OS_insert_value(count_shift,
        16,
        value_of_port ,
        &val[0]);
    count_shift+=16;

 


    /* Update Hw Entry */
    for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
    {
        MEA_API_WriteReg(unit_i,
            ENET_BM_IA_WRDATA0 +(i*4),
            val[i],
            MEA_MODULE_BM);  
    }

}



static MEA_Status mea_drv_BondingTx_UpdateHw(MEA_Unit_t                        unit_i,
                                                           MEA_Uint16                        id_i,
                                                           MEA_BondingTx_dbt    *new_entry_pi,
                                                           MEA_BondingTx_dbt    *old_entry_pi)
{
    
   
    MEA_db_HwUnit_t    hwUnit;

   ENET_ind_write_t      ind_write;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

        /* check if we have any changes */
        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi))== 0) ) { 
                return MEA_OK;
        }




        

        /* Prepare ind_write structure */
        ind_write.tableType     = ENET_BM_TBL_TYP_BONDING_TX_GROUP_PORT_MAPPING;
        ind_write.tableOffset   = id_i; 
        ind_write.cmdReg        = MEA_BM_IA_CMD;      
        ind_write.cmdMask        = MEA_BM_IA_CMD_MASK;      
        ind_write.statusReg        = MEA_BM_IA_STAT;   
        ind_write.statusMask    = MEA_BM_IA_STAT_MASK;   
        ind_write.tableAddrReg    = MEA_BM_IA_ADDR;
        ind_write.tableAddrMask    = MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_BondingTx_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
        ind_write.funcParam3 = (MEA_funcParam)((long)id_i);
        ind_write.funcParam4 = (MEA_funcParam)(new_entry_pi);

#if 1
        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_BM)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_BM);
            return ENET_ERROR;
        }
#endif


	  
    

        /* Return to caller */
        return MEA_OK;
}


static MEA_Bool mea_drv_BondingTx_find_free(MEA_Unit_t       unit_i, 
                                            MEA_Uint32       *id_io)
{
    MEA_Uint16 i;

    for(i=0;i< MEA_STANDART_BONDING_NUM_OF_GROUP_TX; i++) {
        if (MEA_BondingTx_info_Table[i].valid == MEA_FALSE){
            *id_io=i; 
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;

}







static MEA_Bool mea_drv_BondingTx_IsRange(MEA_Unit_t     unit_i,
                                                        MEA_Uint16 id_i)
{

    if ((id_i) >= MEA_STANDART_BONDING_NUM_OF_GROUP_TX){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

static MEA_Status mea_drv_BondingTx_IsExist(MEA_Unit_t     unit_i,
                                                          MEA_Uint32 id_i,
                                                          MEA_Bool *exist)
{

    if(exist == NULL){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  exist = NULL \n",__FUNCTION__); 
      return MEA_ERROR;
    }
    *exist=MEA_FALSE;

    if(mea_drv_BondingTx_IsRange(unit_i,id_i)!=MEA_TRUE){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed  for id=%d\n",__FUNCTION__,id_i); 
      return MEA_ERROR;
    }

    if( MEA_BondingTx_info_Table[id_i].valid){
      *exist=MEA_TRUE;
      return MEA_OK;
    }

    return MEA_OK;
}


#if 0
static MEA_Status mea_drv_BondingTx_add_owner(MEA_Unit_t       unit_i ,
                                              MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_BondingTx_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }

    MEA_BondingTx_info_Table[id_i].num_of_owners++;

    return MEA_OK;
}
static MEA_Status mea_drv_BondingTx_delete_owner(MEA_Unit_t       unit_i ,
                                                               MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_BondingTx_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }

    if(MEA_BondingTx_info_Table[id_i].num_of_owners==1){
        MEA_BondingTx_info_Table[id_i].valid=MEA_FALSE;
        MEA_BondingTx_info_Table[id_i].num_of_owners=0;
		// need to set set hw zero
        return MEA_OK;

    }


    MEA_BondingTx_info_Table[id_i].num_of_owners--;

    return MEA_OK;
}








#endif
/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_BondingTx(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;
    
	
	 if(b_bist_test){
        return MEA_OK;
    }
	if (!MEA_STANDART_BONDING_SUPPORT){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize BM BondingTx tables ..OFF.\n");
        return MEA_OK;
	}
	    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize BM BondingTx tables ...ON\n");
	
	/* Allocate BONDING Table */
    size = MEA_STANDART_BONDING_NUM_OF_GROUP_TX * sizeof(MEA_BondingTx_Entry_dbt);
    if (size != 0) {
        MEA_BondingTx_info_Table = (MEA_BondingTx_Entry_dbt*)MEA_OS_malloc(size);
        if (MEA_BondingTx_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_BondingTx_info_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_BondingTx_info_Table[0]),0,size);
    }
    /**/
    // TBD do i need to clear the memory in the FPGA

MEA_OS_memset(&MEA_Bonding_tbl_state[0],0,sizeof(MEA_Bonding_tbl_state));


    return MEA_OK;
}
MEA_Status mea_drv_RInit_BondingTx(MEA_Unit_t       unit_i)
{
	MEA_Uint16 id;
    if (!MEA_STANDART_BONDING_SUPPORT){
		return MEA_OK;
	}
	
    for (id=0;id<MEA_STANDART_BONDING_NUM_OF_GROUP_TX;id++) {
        if (MEA_BondingTx_info_Table[id].valid){
            if(mea_drv_BondingTx_UpdateHw(unit_i,
                id,
                &(MEA_BondingTx_info_Table[id].data),
                NULL) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -  mea_drv_Bonding_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                    return MEA_ERROR;
            }
        }
    }


    return MEA_OK;
}

MEA_Status mea_drv_Conclude_BondingTx(MEA_Unit_t       unit_i)
{

    /* Free the table */
	if (!MEA_STANDART_BONDING_SUPPORT){
		return MEA_OK;
	}
    if (MEA_BondingTx_info_Table != NULL) {
        MEA_OS_free(MEA_BondingTx_info_Table);
        MEA_BondingTx_info_Table = NULL;
    }

    return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_API_BondingTx_Create_Entry(MEA_Unit_t             unit_i,
                                          MEA_BondingTx_dbt     *entry_pi,
                                          MEA_Uint32            *id_io)
{
    MEA_Bool exist;
    MEA_Uint32 value_of_port;
    
    MEA_API_LOG

    /* Check if support */
    if (!MEA_STANDART_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
	
	if(entry_pi== NULL){
	 MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - entry_pi = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
	}
	
   

    /* Look for stream id */
    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID){
        /*check if the id exist*/
        if (mea_drv_BondingTx_IsExist(unit_i,*id_io,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_BondingTx_IsExist failed (*id_io=%d\n",
                __FUNCTION__,*id_io);
            return MEA_ERROR;
        }
        if (exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - *Id_io %d is already exist\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
        }

    }else {
        /*find new place*/
        if (mea_drv_BondingTx_find_free(unit_i,id_io)!=MEA_TRUE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_LAG_find_free failed\n",__FUNCTION__);
            return MEA_ERROR;
        }
    }
    /* check parameter */
    if(mea_drv_BondingTx_check_parameters(unit_i,id_io,entry_pi) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_BondingTx_check_parameters failed \n",__FUNCTION__); 
        return MEA_ERROR;
    }
	

    if( mea_drv_BondingTx_UpdateHw(unit_i,
        *id_io,
        entry_pi,
        NULL)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_BondingTx_UpdateHw failed for id %d\n",__FUNCTION__,*id_io);
            return MEA_ERROR;
    }
    
    
    if (entry_pi->valid)
    {

        value_of_port=0;

        if( *id_io <= 3){
            value_of_port = (entry_pi->Multiple_Port.out_ports_0_31 & 0x0000ffff);
        }
        if(*id_io >= 4 && *id_io <= 7){

            value_of_port = (((entry_pi->Multiple_Port.out_ports_0_31) >>16) & 0x0000ffff); 
        }
        if(*id_io >= 8 && *id_io <= 11){

            value_of_port = (entry_pi->Multiple_Port.out_ports_32_63 & 0x0000ffff); 
        }

    }else{

        value_of_port=0;
    }



    MEA_BondingTx_info_Table[*id_io].valid         = MEA_TRUE;
    MEA_BondingTx_info_Table[*id_io].num_of_owners = 1;
    MEA_BondingTx_info_Table[*id_io].hwdata.data_value = value_of_port;
    
    MEA_OS_memcpy(&(MEA_BondingTx_info_Table[*id_io].data),entry_pi,sizeof(MEA_BondingTx_info_Table[*id_io].data));
    
    
    


    return MEA_OK;

}


MEA_Status MEA_API_BondingTx_Set_Entry(MEA_Unit_t              unit_i,
                                          MEA_Uint32           id_i,
                                          MEA_BondingTx_dbt   *entry_pi)
{
    MEA_Bool exist;
    MEA_Uint32 value_of_port;

     MEA_API_LOG

    /* Check if support */
    if (!MEA_STANDART_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
	
	if(entry_pi== NULL){
	 MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - entry_pi = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
	}

    /*check if the id exist*/
    if (mea_drv_BondingTx_IsExist(unit_i,id_i,&exist)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_Bonding_IsExist failed (id_i=%d\n",
            __FUNCTION__,id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Bonding group %d not exist\n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }


    /* check parameter*/
    if(mea_drv_BondingTx_check_parameters(unit_i,&id_i,entry_pi)!= MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Bonding_check_parameters filed \n",__FUNCTION__); 
        return MEA_ERROR;
    }




    if( mea_drv_BondingTx_UpdateHw(unit_i,
        id_i,
        entry_pi,
        &MEA_BondingTx_info_Table[id_i].data)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_BondingTx_info_Table failed for id %d\n",__FUNCTION__,id_i);
            return MEA_ERROR;
    }

    if (entry_pi->valid)
    {

        value_of_port=0;

        if( id_i <= 3){
            value_of_port = (entry_pi->Multiple_Port.out_ports_0_31 & 0x0000ffff);
        }
        if(id_i >= 4 && id_i <= 7){

            value_of_port = (((entry_pi->Multiple_Port.out_ports_0_31) >>16) & 0x0000ffff); 
        }
        if(id_i >= 8 && id_i <= 11){

            value_of_port = (entry_pi->Multiple_Port.out_ports_32_63 & 0x0000ffff); 
        }

    }else{

        value_of_port=0;
    }

    MEA_OS_memcpy(&(MEA_BondingTx_info_Table[id_i].data),
                    entry_pi,
                    sizeof(MEA_BondingTx_info_Table[id_i].data));
    MEA_BondingTx_info_Table[id_i].hwdata.data_value = value_of_port;



    return MEA_OK;
}

MEA_Status MEA_API_BondingTx_Delete_Entry(MEA_Unit_t                  unit_i,
                                        MEA_Uint32                   id_i)
{

    MEA_Bool                    exist;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_STANDART_BONDING_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        /*check if the id exist*/
        if (mea_drv_BondingTx_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Bonding_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Bonding %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        if(MEA_BondingTx_info_Table[id_i].num_of_owners >1){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - need to delete all the reference to this profile\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }

        /*Write to Hw All zero*/

        MEA_OS_memset(&MEA_BondingTx_info_Table[id_i].data,0,sizeof(MEA_BondingTx_info_Table[0].data));

        MEA_BondingTx_info_Table[id_i].valid = MEA_FALSE;
        MEA_BondingTx_info_Table[id_i].num_of_owners=0;

        /* Return to caller */
        return MEA_OK;
}

MEA_Status MEA_API_BondingTx_Get_Entry(MEA_Unit_t         unit_i,
                                     MEA_Uint32           id_i,
                                     MEA_BondingTx_dbt    *entry_po)
{
    MEA_Bool    exist; 

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_STANDART_BONDING_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        /*check if the id exist*/
        if (mea_drv_BondingTx_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_BondingTx_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - id %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if(entry_po==NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry_po is null\n",__FUNCTION__,id_i);
            return MEA_ERROR;

        }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        /* Copy to caller structure */
        MEA_OS_memset(entry_po,0,sizeof(*entry_po));
        MEA_OS_memcpy(entry_po ,
            &(MEA_BondingTx_info_Table[id_i].data),
            sizeof(*entry_po));

        /* Return to caller */
        return MEA_OK;


}

MEA_Status MEA_API_BondingTx_GetFirst_Entry(MEA_Unit_t              unit_i,
                                          MEA_Uint32               *id_o,
                                          MEA_BondingTx_dbt        *entry_po, 
                                          MEA_Bool                 *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_STANDART_BONDING_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        if (id_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
       
        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_o)=0;
            (*id_o) < MEA_STANDART_BONDING_NUM_OF_GROUP_TX; 
            (*id_o)++ ) {
                if (MEA_BondingTx_info_Table[(*id_o)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }

                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_BondingTx_info_Table[(*id_o)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;

}

MEA_Status MEA_API_BondingTx_GetNext_Entry(MEA_Unit_t            unit_i,
                                         MEA_Uint32              *id_io,
                                         MEA_BondingTx_dbt       *entry_po, 
                                         MEA_Bool                *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_STANDART_BONDING_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        if (id_io == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
        
        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_io)++;
            (*id_io) < MEA_STANDART_BONDING_NUM_OF_GROUP_TX; 
            (*id_io)++ ) {
                if(MEA_BondingTx_info_Table[(*id_io)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }
                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_BondingTx_info_Table[(*id_io)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


    return MEA_OK;
}


MEA_Status MEA_API_BondingTx_IsExist(MEA_Unit_t                unit_i,
                                     MEA_Uint32                  id_i,
                                     MEA_Bool                    *exist)
{
    if (!MEA_STANDART_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    

    return mea_drv_BondingTx_IsExist(unit_i,id_i, exist);
}

/*---------------------------Bonding_Counters------------------------------------------------------*/


MEA_Status mea_drv_Init_Bonding_Counters(MEA_Unit_t unit_i)
{
    MEA_Uint32      size;
    MEA_Uint32     id;

     if(b_bist_test){
        return MEA_OK;
    }
	if (!MEA_STANDART_BONDING_SUPPORT){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize BM Bonding Count tables ..OFF.\n");
        return MEA_OK;
	}
	    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize BM Bonding Count tables ...ON\n");

    /* Allocate MEA_Counters_TDM_Table */
    if (MEA_Bonding_Counters_Table == NULL) {

        size = sizeof(MEA_drv_BondingCount_dbt)*MEA_STANDART_BONDING_NUM_OF_GROUP_RX;
        MEA_Bonding_Counters_Table = (MEA_drv_BondingCount_dbt*)MEA_OS_malloc(size);
        if (MEA_Bonding_Counters_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_Bonding_Counters_Table failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        MEA_OS_memset((char*)MEA_Bonding_Counters_Table,0,size);
    }
    for (id=0;id< MEA_STANDART_BONDING_NUM_OF_GROUP_RX;id++)
    {
        MEA_Bonding_Counters_Table[id].valid= MEA_TRUE;
    }
    





 return MEA_OK;
}
MEA_Status mea_drv_ReInit_Bonding_Counters(MEA_Unit_t unit_i)
{
    if (!MEA_STANDART_BONDING_SUPPORT){
        return MEA_OK;
    }

    return MEA_OK;
}

MEA_Status mea_drv_Conclude_Bonding_Counters(MEA_Unit_t unit_i)
{
    if (!MEA_STANDART_BONDING_SUPPORT){
		return MEA_OK;
	}
    
    if(MEA_Bonding_Counters_Table){
        MEA_OS_free(MEA_Bonding_Counters_Table);
        MEA_Bonding_Counters_Table = NULL;
    }
    
    return MEA_OK;
}



static MEA_Status mea_drv_Collect_Counters_Bonding(MEA_Unit_t           unit_i,
                                        MEA_Uint16           id_i)
{

    MEA_ind_read_t                 ind_read;
    MEA_Uint32                     read_data[3];
   
   
//    MEA_db_HwUnit_t                hwUnit;
//    MEA_db_HwId_t                  hwId;
//    mea_memory_mode_e              save_globalMemoryMode;
    MEA_BondingCount_dbt  *entry_db;
    
//    MEA_Uint32 RX_Reorder_packet=0;
    MEA_Uint32 RX_seq_out_of_bound=0;
    MEA_Uint32 RX_seq_duplication=0;
    MEA_Uint32 RX_size_drop=0;
    MEA_Uint32 TX_Idle_packets=0;
//    MEA_Uint32 TX_Resync_threshold=0;
//    MEA_Uint32 TX_drop_other=0;
   

   

   
    
      if (!MEA_STANDART_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
      if(id_i>=MEA_STANDART_BONDING_NUM_OF_GROUP_RX){
          MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
              "%s \n- MEA_STANDART_BONDING_NUM_OF_GROUP_RX is out of range \n",
              __FUNCTION__);
          return MEA_ERROR; 
      }

    entry_db=&MEA_Bonding_Counters_Table[id_i].data;
    /* Set Global Memory Mode */
  
    
 
    /* Get hwUnit and hwId */
    /* build the ind_read structure values */
    ind_read.cmdReg            = MEA_BM_IA_CMD;      
    ind_read.cmdMask        = MEA_BM_IA_CMD_MASK;      
    ind_read.statusReg        = MEA_BM_IA_STAT;   
    ind_read.statusMask        = MEA_BM_IA_STAT_MASK;   
    ind_read.tableAddrReg    = MEA_BM_IA_ADDR;
    ind_read.tableAddrMask  = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_read.tableOffset    = id_i;
    ind_read.read_data        = &read_data[0];

    ind_read.tableType=  ENET_BM_TBL_TYP_BONDING_GROUP_SATISTICS_COUNTER;      
#if 1
    if (MEA_API_ReadIndirect(MEA_UNIT_0,
        &ind_read,
        MEA_NUM_OF_ELEMENTS(read_data),
        MEA_MODULE_BM) != MEA_OK) {                 
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ReadIndirect from tableType %d / tableOffset %d module BM failed "
                "(pmId=%d)\n",
                __FUNCTION__,ind_read.tableType,ind_read.tableOffset,id_i);
            return MEA_ERROR;
    }
#endif
//     RX_Reorder_packet = read_data[0] & 0x00003fff;            // 1

     RX_seq_out_of_bound =   (read_data[0] & 0xf0000000)>>28;          // 3
     RX_seq_out_of_bound |= (read_data[1] & 0x000003ff)<<4; 
     
     RX_seq_duplication =    (read_data[1] & 0x00fffc00)>>10;           // 4
     RX_size_drop=0;
     RX_size_drop = (read_data[1] & 0xff000000)>>24;   // 5
     RX_size_drop |= (read_data[2] & 0x0000003f)<<8;

    /* ETH-> TDM */
     TX_Idle_packets = (read_data[0] & 0x0fffc000)>>14;           //2
 //    TX_Resync_threshold = (read_data[2] & 0x03c00000)>>22;;       // 7
//     TX_drop_other = (read_data[2] & 0xfc000000)>>26;;       // 8 
  
    //MEA_OS_UPDATE_COUNTER(entry_db->RX_Reorder_packet.val,          RX_Reorder_packet);
    MEA_OS_UPDATE_COUNTER(entry_db->RX_seq_out_of_bound.val,        RX_seq_out_of_bound);
    MEA_OS_UPDATE_COUNTER(entry_db->RX_seq_duplication.val,         RX_seq_duplication);
    //MEA_OS_UPDATE_COUNTER(entry_db->RX_jitter_buffer_full_drop.val, RX_jitter_buffer_full_drop);
    MEA_OS_UPDATE_COUNTER(entry_db->RX_size_drop.val, RX_size_drop);

    MEA_OS_UPDATE_COUNTER(entry_db->TX_drop.val,            TX_Idle_packets);
   








    return MEA_OK;
}

static MEA_Bool mea_drv_IsBonding_Id_InRange_Counter(MEA_Uint16 Id)
{

    if(Id<MEA_STANDART_BONDING_NUM_OF_GROUP_RX)
        return MEA_TRUE;
    else
        return MEA_FALSE;        

}


static MEA_Bool mea_drv_Is_exist_Bonding_Counter(MEA_Unit_t unit_i,MEA_Uint16 Id)
{
    if(mea_drv_IsBonding_Id_InRange_Counter(Id) != MEA_TRUE){
        return MEA_FALSE;
    } else {
        if(MEA_Bonding_Counters_Table[Id].valid == MEA_TRUE)
            return MEA_TRUE;
        else 
            return MEA_FALSE;
    }
    return MEA_FALSE;
}
#if 0
static MEA_Status mea_drv_get_first_Bonding_Id_Counter(MEA_Bool *found_o, MEA_Uint16 *id_io)
{
    MEA_Uint16 i;
    *found_o = MEA_FALSE;

    if (mea_drv_IsBonding_Id_InRange_Counter(*id_io) != MEA_TRUE) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Invalid id %d \n",
            __FUNCTION__,*id_io);
        return MEA_ERROR;  
    }

    for ( i=*id_io+1; i < MEA_STANDART_BONDING_NUM_OF_GROUP_RX; i++ )
    {
        if (MEA_Bonding_Counters_Table[i].valid)
        {
            *id_io = (MEA_Uint16)i;
            *found_o = MEA_TRUE;
            return MEA_OK;
        }
    }

    return MEA_OK;
}




static MEA_Status mea_drv_get_next_id_Bonding_Counter(MEA_Bool *found_o, MEA_Uint16 *id_io)
{
    MEA_Uint32 i;
    *found_o = MEA_FALSE;

    if (mea_drv_IsBonding_Id_InRange_Counter(*id_io) != MEA_TRUE) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Invalid id %d \n",
            __FUNCTION__,*id_io);
        return MEA_ERROR;  
    }

    for ( i=*id_io+1; i < MEA_STANDART_BONDING_NUM_OF_GROUP_RX; i++ )
    {
        if (MEA_Bonding_Counters_Table[i].valid)
        {
            *id_io = (MEA_Uint16)i;
            *found_o = MEA_TRUE;
            return MEA_OK;
        }
    }

    return MEA_OK;
}


#endif
/******************/
/*---------------------------------------------------------------------------*/
/*            <MEA_API_Collect_Counters_Bonding>                              */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_Bonding(MEA_Unit_t                    unit,
                                        MEA_Uint16                        id_i,
                                        MEA_BondingCount_dbt*             entry)
{
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool exist = MEA_FALSE;
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_STANDART_BONDING_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_STANDART_BONDING_SUPPORT not Support\n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        exist=mea_drv_Is_exist_Bonding_Counter(unit,id_i);

        if (exist == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Id_exist %d not define\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;  
        }
        

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

       

        if (mea_drv_Collect_Counters_Bonding(unit, id_i) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_Collect_Counters_Bonding failed "
                    "(Id=%d)\n",
                    __FUNCTION__,id_i);
                return MEA_ERROR;

        }

        if ( entry != NULL) {
            MEA_OS_memcpy(entry,
                &(MEA_Bonding_Counters_Table[id_i].data),
                sizeof(*entry));
        }



        /* Return to Callers */
        return MEA_OK;


}


/*---------------------------------------------------------------------------*/
/*            <MEA_API_Collect_Counters_Bonding_ALL>                         */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_Bonding_ALL(MEA_Unit_t                unit)
{
    
    MEA_Uint16 id=0;

    MEA_API_LOG
   
    
   

     if (!MEA_STANDART_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }

      for ( id=0; id < MEA_STANDART_BONDING_NUM_OF_GROUP_RX; id++ ){  

       
        if (MEA_API_Collect_Counters_Bonding(unit,
            id,
            NULL)==MEA_ERROR){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Collect Bonding Counters for Id=%d failed\n",
                    __FUNCTION__,id);
                return MEA_ERROR;
        }

      }

    

        /* Return to Callers */
        return MEA_OK;

}



/*---------------------------------------------------------------------------*/
/*            <MEA_API_Get_Counters_Bonding>                                     */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Counters_Bonding(MEA_Unit_t                    unit,
                                    MEA_Uint16                     id_i,
                                    MEA_BondingCount_dbt*          entry)
{
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    MEA_Bool exist = MEA_FALSE;
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if (!MEA_STANDART_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    if (id_i >= MEA_STANDART_BONDING_NUM_OF_GROUP_RX) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id (%d) >= max allowed (%d) \n",
            __FUNCTION__,id_i,MEA_STANDART_BONDING_NUM_OF_GROUP_RX);
        return MEA_ERROR; 
    }

        exist=mea_drv_Is_exist_Bonding_Counter(unit,id_i);

    if (exist == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id_exist %d not define\n",
            __FUNCTION__,id_i);
        return MEA_ERROR;  
    }

    /* Check valid parameter :  entry */
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry==NULL (id_i=%d)\n",
            __FUNCTION__,id_i);
        return MEA_ERROR;  
    }

#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    
    
    /* Update output parameters */
    MEA_OS_memcpy(entry,&(MEA_Bonding_Counters_Table[id_i].data),sizeof(*entry));
    
    


    return MEA_OK;
}

MEA_Status MEA_API_Clear_Counters_Bonding(MEA_Unit_t               unit,
                                          MEA_Uint16                id_i)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

     if (!MEA_STANDART_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    if (id_i >= MEA_STANDART_BONDING_NUM_OF_GROUP_RX) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Id (%d) >= max allowed (%d) \n",
            __FUNCTION__,id_i,MEA_STANDART_BONDING_NUM_OF_GROUP_TX);
        return MEA_ERROR; 
    }

       


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
 

    /* Collect Bonding Counters from HW to cause HW counters zeros */
    if (mea_drv_Collect_Counters_Bonding(unit,id_i) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Collect_Counters_Bonding failed "
                "(Id=%d)\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;

    }


        /* Zeros  counters database */
        MEA_OS_memset(&(MEA_Bonding_Counters_Table[id_i].data),
            0,
            sizeof(MEA_Bonding_Counters_Table[0].data));

        

        /* Return to Callers */
        return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*            <MEA_API_Clear_Counters_Bonding_All>                                  */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Clear_Counters_Bonding_All(MEA_Unit_t                    unit)
{
   
    MEA_Uint16 id=0;

    MEA_API_LOG
     if (!MEA_STANDART_BONDING_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_STANDART_BONDING_SUPPORT is not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }

     for ( id=0; id < MEA_STANDART_BONDING_NUM_OF_GROUP_RX; id++ ){

   
        if (MEA_API_Clear_Counters_Bonding(unit,
            id) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Clear Bonding Counters for Id=%d failed\n",
                    __FUNCTION__,id);
                return MEA_ERROR;
        }

    }

    

    /* Return to Callers */
    return MEA_OK;    
}


MEA_Status mea_drv_bonding_state_configure(MEA_Port_t port, MEA_Bool enable)
{
	
    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE/*silent*/)==MEA_FALSE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"port %d is invalid \n",port);
        return MEA_OK;
    } 

    
    if(enable== MEA_TRUE){
        if(MEA_Bonding_tbl_state[port].valid == MEA_FALSE){
            MEA_Bonding_tbl_state[port].state=0; 
            MEA_Bonding_tbl_state[port].rx_prev_packt.val=0xffff0000; 
            MEA_Bonding_tbl_state[port].valid= MEA_TRUE;
        }
        
    }
    if(enable== MEA_FALSE){
        
        if(MEA_Bonding_tbl_state[port].valid == MEA_TRUE){
            MEA_Bonding_tbl_state[0].valid= MEA_FALSE;
            
        }
    }


return MEA_OK;
}






void MEA_API_check_bonding_state()
{


MEA_Counters_RMON_dbt        entry;
MEA_EgressPort_Entry_dbt      Egress_entry;
MEA_Port_t    port;

//MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"bonding_state  \n");
#ifndef __KERNEL__
MEA_OS_fflush(stdout);
#endif

    for (port=0;port< MEA_MAX_PORT_NUMBER;port++)
    {
        if(MEA_Bonding_tbl_state[port].valid == MEA_FALSE){
            continue;
        }

        if(MEA_Bonding_tbl_state[port].state==0){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"[port=%d].state %s \n",port,"state 0");
#ifndef __KERNEL__
            MEA_OS_fflush(stdout);
#endif
        }


        //get rmon counter
        if( MEA_API_Get_Counters_RMON (MEA_UNIT_0, port,&entry)!=MEA_OK){
           continue;
        }

       if(MEA_Bonding_tbl_state[port].rx_prev_packt.val != entry.Rx_Pkts.val){
           if(MEA_Bonding_tbl_state[port].state == 1){
                MEA_Bonding_tbl_state[port].rx_prev_packt.val= entry.Rx_Pkts.val;
               continue;
           }
           if(MEA_Bonding_tbl_state[port].state == 2 || MEA_Bonding_tbl_state[port].state==0) {//was tx_disable
               //  config the Tx to enable
                 
                 if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,port,&Egress_entry) != MEA_OK){ 
                     continue;
                 }

                 Egress_entry.tx_enable=MEA_TRUE;
                 if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,port,&Egress_entry) != MEA_OK){ 
                     continue;
                  }

                 // change the state to 
                  MEA_Bonding_tbl_state[port].state=1;
                  MEA_Bonding_tbl_state[port].rx_prev_packt.val=entry.Rx_Pkts.val;
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"[port=%d].state %s \n",port,"Tx-->enable");
#ifndef __KERNEL__
                  MEA_OS_fflush(stdout);
#endif
           }



       }else {
           
               if(  MEA_Bonding_tbl_state[port].rx_prev_packt.val == entry.Rx_Pkts.val ){
                  if(MEA_Bonding_tbl_state[port].state == 2) 
                    continue;
                 
                  if(MEA_Bonding_tbl_state[port].state == 1 || MEA_Bonding_tbl_state[port].state == 0){
                     // set the port Tx to disable
                     
                     // change the state to 
                     if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,port,&Egress_entry) != MEA_OK) 
                         continue;
                     Egress_entry.tx_enable=MEA_FALSE;
                     if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,port,&Egress_entry) != MEA_OK) 
                         continue;
                     MEA_Bonding_tbl_state[port].state=2;
                     MEA_Bonding_tbl_state[port].rx_prev_packt.val=entry.Rx_Pkts.val;
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"[port=%d].state %s \n",port,"Tx-->disable");
#ifndef __KERNEL__
                     MEA_OS_fflush(stdout);
#endif

                 }
            }
        }
    }
}






