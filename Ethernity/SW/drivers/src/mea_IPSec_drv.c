/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/






/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#if 1
#ifdef MEA_OS_LINUX
#ifdef MEA_OS_OC
#include <linux/unistd.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#endif
#else
#include <time.h>
#endif
#endif



#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_if_regs.h"

#include "mea_IPSec_drv.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_NUM_OF_OFFSET_CRPO_PROF 8



/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/



typedef struct {
    MEA_Bool   valid;
    
    MEA_Uint32 num_of_owners;
    
    MEA_IPSec_dbt data;

}MEA_IPSec_Entry_dbt;





/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/

                         
static MEA_IPSec_Entry_dbt           *MEA_IPSecESP_info_Table=NULL;




/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/
static MEA_Status mea_drv_IPSec_check_parameters(MEA_Unit_t         unit_i,
                                              
                                                 MEA_IPSec_dbt  *entry_pi);





/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/

static MEA_Status mea_drv_IPSec_check_parameters(MEA_Unit_t           unit_i,
                                                
                                                 MEA_IPSec_dbt    *entry_pi)
{

  


#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    if(entry_pi==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_pi is null\n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    switch (entry_pi->security_type)
    {
        case   MEA_EDITING_IPSec_Security_INTEG_HMC_SHA1_96:
        case   MEA_EDITING_IPSec_Security_INTEG_HMC_SHA256_128:
        case   MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CBC:
        case MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_SHA1_96:
        case  MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_SHA256_128:
        case MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_SHA1_96:
        case MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_SHA256_128:
        case MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR:
        case MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_SHA1_96:
        case MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_SHA256_128:
        case MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR:
        case MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_SHA1_96:
        case MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_SHA256_128:
        case MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_GCM_128:
        case MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_GCM_128:
            break;

    default:
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s IPSec_Security Type %d not Support on HW \n", __FUNCTION__);
        return MEA_ERROR;
        break;
    }
    


   
#endif

    return MEA_OK;
}


void mea_drv_IPSec_UpdateHwEntry(   MEA_funcParam arg1,
                                    MEA_funcParam arg2,
                                    MEA_funcParam arg3,
                                    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t           ) arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t     )arg2;
    MEA_Uint32              len = (MEA_Uint32)((long)arg3);
    MEA_Uint32       *data = (MEA_Uint32*)arg4;
    MEA_Uint32 i;
    


    for (i = 0; i < len; i++) {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            data[i],
            MEA_MODULE_IF);

        // printf("data[%d]= 0x08%x\n",i, data[i]);

    }

  

}



static MEA_Status mea_drv_IPSec_UpdateHw(MEA_Unit_t                        unit_i,
                                         MEA_Uint32                        id_i,
                                         MEA_IPSec_dbt    *new_entry_pi,
                                         MEA_IPSec_dbt    *old_entry_pi)
{
    MEA_Uint32 start_offset;
    MEA_Uint32 offset;
    MEA_Uint32 val[5];
    MEA_Uint32 len = 5;
   
    MEA_db_HwUnit_t    hwUnit;

   ENET_ind_write_t      ind_write;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

        /* check if we have any changes */
        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi))== 0) ) { 
                return MEA_OK;
        }




        

        /* Prepare ind_write structure */
       
        ind_write.funcParam1    = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
        ind_write.funcParam3 = (MEA_funcParam)((long)id_i);
        ind_write.funcParam4 = (MEA_funcParam)(new_entry_pi);


        ind_write.tableType = ENET_IF_CMD_PARAM_TBL_CRPO;
       
        ind_write.cmdReg = MEA_IF_CMD_REG;
        ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
        ind_write.statusReg = MEA_IF_STATUS_REG;
        ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
        ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_IPSec_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);

   
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
        ind_write.funcParam3 = (MEA_funcParam)((long)len);
        ind_write.funcParam4 = (MEA_funcParam)(&val);




        start_offset = (id_i * MEA_NUM_OF_OFFSET_CRPO_PROF);
        for (offset = 0; offset < MEA_NUM_OF_OFFSET_CRPO_PROF; offset++)
        {
            ind_write.tableOffset = start_offset+ offset;

            MEA_OS_memset(&val[0], 0, sizeof(val));
            switch (offset)
            {
            case 0:
                val[0] = new_entry_pi->Integrity_key[0];
                val[1] = new_entry_pi->Integrity_key[1];
                val[2] = new_entry_pi->Integrity_key[2];
                val[3] = new_entry_pi->Integrity_key[3];
                val[4] = ((new_entry_pi->SPI)>>0) & 0xffff;


                break;
            case 1:
                val[0] = new_entry_pi->Integrity_key[4];
                val[1] = new_entry_pi->Integrity_key[5];
                val[2] = new_entry_pi->Integrity_key[6];
                val[3] = new_entry_pi->Integrity_key[7];
                val[4] = ((new_entry_pi->SPI) >> 16) & 0xffff;

                break;
            case 2:
                val[0] = new_entry_pi->Integrity_IV[0];
                val[1] = new_entry_pi->Integrity_IV[1];
                val[2] = new_entry_pi->Integrity_IV[2];
                val[3] = new_entry_pi->Integrity_IV[3];
                val[4] = ((new_entry_pi->security_type) ) & 0xff;
                val[4] |= ((new_entry_pi->TFC_padding_en))<<8;
                val[4] |= ((new_entry_pi->ESN_en)) << 9;


                break;
            case 3:
                val[0] = new_entry_pi->Integrity_IV[4];
                val[1] = new_entry_pi->Integrity_IV[5];
                val[2] = new_entry_pi->Integrity_IV[6];
                val[3] = new_entry_pi->Integrity_IV[7];
                val[4] = 0;
                break;
            case 4:
                val[0] = new_entry_pi->Confident_key[0];
                val[1] = new_entry_pi->Confident_key[1];
                val[2] = new_entry_pi->Confident_key[2];
                val[3] = new_entry_pi->Confident_key[3];
                val[4] = 0;

                break;
            case 5:
                val[0] = new_entry_pi->Confident_key[4];
                val[1] = new_entry_pi->Confident_key[5];
                val[2] = new_entry_pi->Confident_key[6];
                val[3] = new_entry_pi->Confident_key[7];
                val[4] = 0;
                break;
            case 6:
                val[0] = new_entry_pi->Confident_IV[0];
                val[1] = new_entry_pi->Confident_IV[1];
                val[2] = new_entry_pi->Confident_IV[2];
                val[3] = new_entry_pi->Confident_IV[3];
                val[4] = 0;
                break;
            case 7:
                val[0] = new_entry_pi->Confident_IV[4];
                val[1] = new_entry_pi->Confident_IV[5];
                val[2] = new_entry_pi->Confident_IV[6];
                val[3] = new_entry_pi->Confident_IV[7];
                val[4] = 0;

                break;



            
            }





#if 1
            /* Write to the Indirect Table */
            if (ENET_WriteIndirect(unit_i, &ind_write, ENET_MODULE_IF) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                    __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_BM);
                return ENET_ERROR;
            }
#endif

        }





	  
    

        /* Return to caller */
        return MEA_OK;
}


static MEA_Bool mea_drv_IPSec_find_free(MEA_Unit_t       unit_i,
                                            MEA_Uint32       *id_io)
{
    MEA_Uint16 i;

    for(i=0;i< MEA_MAX_NUM_OF_IPSecESP_ID; i++) {
        if (MEA_IPSecESP_info_Table[i].valid == MEA_FALSE){
            *id_io=i; 
            return MEA_TRUE;
        }
    }
    return MEA_FALSE;

}







static MEA_Bool mea_drv_IPSec_IsRange(MEA_Unit_t     unit_i,
                                                        MEA_Uint16 id_i)
{

    if ((id_i) >= MEA_MAX_NUM_OF_IPSecESP_ID){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
        return MEA_FALSE;
    }
    return MEA_TRUE;
}

static MEA_Status mea_drv_IPSecESP_IsExist(MEA_Unit_t     unit_i,
                                                          MEA_Uint32 id_i,
                                                          MEA_Bool *exist)
{

    if(exist == NULL){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  exist = NULL \n",__FUNCTION__); 
      return MEA_ERROR;
    }
    *exist=MEA_FALSE;

    if(mea_drv_IPSec_IsRange(unit_i,id_i)!=MEA_TRUE){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed  for id=%d\n",__FUNCTION__,id_i); 
      return MEA_ERROR;
    }

    if( MEA_IPSecESP_info_Table[id_i].valid){
      *exist=MEA_TRUE;
      return MEA_OK;
    }

    return MEA_OK;
}


#if 0
static MEA_Status mea_drv_IPSec_add_owner(MEA_Unit_t       unit_i ,
                                              MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_IPSecESP_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }

    MEA_IPSecESP_info_Table[id_i].num_of_owners++;

    return MEA_OK;
}
static MEA_Status mea_drv_IPSec_delete_owner(MEA_Unit_t       unit_i ,
                                                               MEA_Uint16       id_i)
{
    MEA_Bool exist;

    if(mea_drv_IPSecESP_IsExist(unit_i,id_i, &exist)!=MEA_OK){
        return MEA_ERROR;
    }

    if(MEA_IPSecESP_info_Table[id_i].num_of_owners==1){
        MEA_IPSecESP_info_Table[id_i].valid=MEA_FALSE;
        MEA_IPSecESP_info_Table[id_i].num_of_owners=0;
		// need to set set hw zero
        return MEA_OK;

    }


    MEA_IPSecESP_info_Table[id_i].num_of_owners--;

    return MEA_OK;
}








#endif
/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_IPSec(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;
    
	
	 if(b_bist_test){
        return MEA_OK;
    }
	if (!MEA_IPSec_SUPPORT){
		
        return MEA_OK;
	}
	    
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IPSecESP tables ...ON\n");
	
	/* Allocate BONDING Table */
    size = MEA_MAX_NUM_OF_IPSecESP_ID * sizeof(MEA_IPSec_Entry_dbt);
    if (size != 0) {
        MEA_IPSecESP_info_Table = (MEA_IPSec_Entry_dbt*)MEA_OS_malloc(size);
        if (MEA_IPSecESP_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_IPSecESP_info_Table failed (size=%d)\n",
                __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_IPSecESP_info_Table[0]),0,size);
    }
    /**/
    // TBD do i need to clear the memory in the FPGA




return MEA_OK;
}
MEA_Status mea_drv_RInit_IPSec(MEA_Unit_t       unit_i)
{
    MEA_Uint16 id;
    if (!MEA_IPSec_SUPPORT) {
        return MEA_OK;
    }

    for (id = 0; id < MEA_MAX_NUM_OF_IPSecESP_ID; id++) {
        if (MEA_IPSecESP_info_Table[id].valid) {
            if (mea_drv_IPSec_UpdateHw(unit_i,
                id,
                &(MEA_IPSecESP_info_Table[id].data),
                NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  mea_drv_IPSec_UpdateHw failed (id=%d)\n", __FUNCTION__, id);
                return MEA_ERROR;
            }
        }
    }


    return MEA_OK;
}

MEA_Status mea_drv_Conclude_IPSec(MEA_Unit_t       unit_i)
{

    /* Free the table */
    if (!MEA_IPSec_SUPPORT) {
        return MEA_OK;
    }
    if (MEA_IPSecESP_info_Table != NULL) {
        MEA_OS_free(MEA_IPSecESP_info_Table);
        MEA_IPSecESP_info_Table = NULL;
    }

    return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_API_IPSecESP_Create_Entry(MEA_Unit_t             unit_i,
    MEA_IPSec_dbt    *entry_p, MEA_Uint32            *id_io)
{
    MEA_Bool exist;


    MEA_API_LOG

        /* Check if support */
        if (!MEA_IPSec_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_IPSec_SUPPORT is not support \n",
                __FUNCTION__);
            return MEA_ERROR;
        }





    /* Look for stream id */
    if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID) {
        /*check if the id exist*/
        if (mea_drv_IPSecESP_IsExist(unit_i, *id_io, &exist) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_IPSecESP_IsExist failed (*id_io=%d\n",
                __FUNCTION__, *id_io);
            return MEA_ERROR;
        }
        if (exist) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - *Id_io %d is already exist\n", __FUNCTION__, *id_io);
            return MEA_ERROR;
        }

    }
    else {
        /*find new place*/
        if (mea_drv_IPSec_find_free(unit_i, id_io) != MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_IPSec_find_free failed\n", __FUNCTION__);
            return MEA_ERROR;
        }
    }

    if(mea_drv_IPSec_check_parameters(unit_i, entry_p) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  mea_drv_IPSec_check_parameters failed \n",
            __FUNCTION__);
        return MEA_ERROR;
    }


    if(mea_drv_IPSec_UpdateHw(unit_i, *id_io, entry_p, NULL) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_IPSec_UpdateHw failed\n", __FUNCTION__);
        return MEA_ERROR;
     }
    
   



    MEA_IPSecESP_info_Table[*id_io].valid         = MEA_TRUE;
    MEA_IPSecESP_info_Table[*id_io].num_of_owners = 1;
    
    
    MEA_OS_memcpy(&(MEA_IPSecESP_info_Table[*id_io].data), entry_p,
                   sizeof(MEA_IPSecESP_info_Table[*id_io].data));
    
    
    


    return MEA_OK;

}


MEA_Status MEA_API_IPSecESP_Delete_Entry(MEA_Unit_t                  unit_i,
                                        MEA_Uint32                   id_i)
{

    MEA_Bool                    exist;

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_IPSec_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_IPSec_SUPPORT is not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        /*check if the id exist*/
        if (mea_drv_IPSecESP_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_IPSecESP_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - IPSec %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        if(MEA_IPSecESP_info_Table[id_i].num_of_owners >1){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - need to delete all the reference to this profile\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }

        /*Write to Hw All zero*/



        MEA_OS_memset(&MEA_IPSecESP_info_Table[id_i].data,0,sizeof(MEA_IPSecESP_info_Table[0].data));
        if (mea_drv_IPSec_UpdateHw(unit_i, id_i, &MEA_IPSecESP_info_Table[id_i].data, NULL) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_IPSec_UpdateHw failed\n", __FUNCTION__);
            return MEA_ERROR;
        }


        MEA_IPSecESP_info_Table[id_i].valid = MEA_FALSE;
        MEA_IPSecESP_info_Table[id_i].num_of_owners=0;

        /* Return to caller */
        return MEA_OK;
}

MEA_Status MEA_API_IPSecESP_Get_Entry(MEA_Unit_t         unit_i,
                                     MEA_Uint32           id_i,
                                     MEA_IPSec_dbt    *entry_po)
{
    MEA_Bool    exist; 

    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        /* Check if support */
        if (!MEA_IPSec_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_IPSec_SUPPORT is not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        /*check if the id exist*/
        if (mea_drv_IPSecESP_IsExist(unit_i,id_i,&exist)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_IPSec_IsExist failed (id_i=%d\n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if (!exist){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - id %d not exist\n",__FUNCTION__,id_i);
            return MEA_ERROR;
        }
        if(entry_po==NULL){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry_po is null\n",__FUNCTION__,id_i);
            return MEA_ERROR;

        }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

        /* Copy to caller structure */
        MEA_OS_memset(entry_po,0,sizeof(*entry_po));
        MEA_OS_memcpy(entry_po ,
            &(MEA_IPSecESP_info_Table[id_i].data),
            sizeof(*entry_po));

        /* Return to caller */
        return MEA_OK;


}

MEA_Status MEA_API_IPSecESP_GetFirst_Entry(MEA_Unit_t              unit_i,
                                          MEA_Uint32               *id_o,
                                          MEA_IPSec_dbt        *entry_po,
                                          MEA_Bool                 *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_IPSec_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_IPSec_SUPPORT is not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        if (id_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
       
        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_o)=0;
            (*id_o) < MEA_MAX_NUM_OF_IPSecESP_ID;
            (*id_o)++ ) {
                if (MEA_IPSecESP_info_Table[(*id_o)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }

                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_IPSecESP_info_Table[(*id_o)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;

}

MEA_Status MEA_API_IPSecESP_GetNext_Entry(MEA_Unit_t            unit_i,
                                         MEA_Uint32              *id_io,
                                         MEA_IPSec_dbt       *entry_po,
                                         MEA_Bool                *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_IPSec_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_IPSec_SUPPORT is not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        if (id_io == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
        
        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_io)++;
            (*id_io) < MEA_MAX_NUM_OF_IPSecESP_ID;
            (*id_io)++ ) {
                if(MEA_IPSecESP_info_Table[(*id_io)].valid == MEA_TRUE) {
                    if (found_o) {
                        *found_o= MEA_TRUE;
                    }
                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_IPSecESP_info_Table[(*id_io)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


    return MEA_OK;
}


MEA_Status MEA_API_IPSecESP_IsExist(MEA_Unit_t                unit_i,
                                     MEA_Uint32                  id_i,
                                     MEA_Bool                    *exist)
{
    if (!MEA_IPSec_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_IPSec_SUPPORT is not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }
    

    return mea_drv_IPSecESP_IsExist(unit_i,id_i, exist);
}






