/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Ethernity  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include "enet_general_drv.h"
#include "mea_if_regs.h"
#include "mea_port_drv.h"

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_if_vsp_drv.h"
#include "enet_queue_drv.h"



/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/

#define MAX_NUMBER_OF_VSP 4

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/
typedef struct{
	MEA_Uint32 tmp;
}mea_vsp_private_dbt;

typedef struct{
	MEA_Port_t   VirtualPort[MAX_NUMBER_OF_VSP];
}MEA_vsp_Entry_dbt;

typedef struct{
	MEA_vsp_Entry_dbt   public_dbt;
	mea_vsp_private_dbt private_dbt;
}mea_vsp_dbt;
				

typedef struct{
MEA_fragment_vsp_Key_dbt  key;
MEA_fragment_vsp_Data_dbt data;
MEA_Uint32 valid          : 1;
MEA_Uint32 num_of_owners  : 31;

}MEA_drv_vsp_fragmentSession_dbt;

typedef struct{
    MEA_Bool valid;
    MEA_AC_Mtu_dbt  data;

}MEA_acMtu_dbt;



typedef struct{
    MEA_Bool valid;
    MEA_Uint8 groupId;
    MEA_OutPorts_Entry_dbt user_regularPacket;
    MEA_OutPorts_Entry_dbt user_l2cpPacket;

    MEA_OutPorts_Entry_dbt BLK_regularPacket;
    MEA_OutPorts_Entry_dbt BLK_l2cpPacket;
    
   
}MEA_VP_MSTP_entry_dbt;

typedef struct{
    MEA_Bool     valid;
    MEA_MacAddr  FWD_DaMAC_value;
    MEA_Uint32 num_of_owners;
}mea_drv_FWD_ENB_DA_dbt;
/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
static MEA_Bool       mea_vsp_InitDone[1];
static mea_vsp_dbt   *vsp_dbt_g = NULL;





static MEA_drv_vsp_fragmentSession_dbt    *MEA_VSP_fragmentSession_Table    = NULL;
static MEA_acMtu_dbt                      *MEA_AcMTU_info_Table=NULL;


static MEA_VP_MSTP_entry_dbt              *MEA_VP_MSTP_info_Table=NULL;

static mea_drv_FWD_ENB_DA_dbt             *MEA_FWD_ENB_DA_info_Table = NULL;

static MEA_acMtu_dbt                      *MEA_Target_MTU_info_Table = NULL;


/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_vsp_UpdateHwEntry>                                        */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static void mea_vsp_UpdateHwEntry(MEA_vsp_Entry_dbt* entry) { 

	MEA_Uint32                val0;
	MEA_Uint32	              val1;
	

    val0 = 0;
    val1 = 0;
	
	/*vsp0*/
	val0  = ((entry->VirtualPort[0]
              << MEA_OS_calc_shift_from_mask(MEA_IF_VSP_VSP0_MASK0)
             ) & MEA_IF_VSP_VSP0_MASK0
            );
	val0  |= ((0
              << MEA_OS_calc_shift_from_mask(MEA_IF_VSP_VSP0SOP_MASK0)
             ) & MEA_IF_VSP_VSP0SOP_MASK0
            );
	/*vsp1*/
	val0  |= ((entry->VirtualPort[1]
              << MEA_OS_calc_shift_from_mask(MEA_IF_VSP_VSP1_MASK0)
             ) & MEA_IF_VSP_VSP1_MASK0
            );
	val0  |= ((0
              << MEA_OS_calc_shift_from_mask(MEA_IF_VSP_VSP1SOP_MASK0)
             ) & MEA_IF_VSP_VSP1SOP_MASK0
            );
	/*vsp2*/
	val0  |= ((entry->VirtualPort[2]
              << MEA_OS_calc_shift_from_mask(MEA_IF_VSP_VSP2_MASK0)
             ) & MEA_IF_VSP_VSP2_MASK0
            );
	val0  |= ((0
              << MEA_OS_calc_shift_from_mask(MEA_IF_VSP_VSP2SOP_MASK0)
             ) & MEA_IF_VSP_VSP2SOP_MASK0
            );
	/*vsp3*/
	val0  |= ((entry->VirtualPort[3]
              << MEA_OS_calc_shift_from_mask(MEA_IF_VSP_VSP3_MASK0)
             ) & MEA_IF_VSP_VSP3_MASK0
            );

	val1  = (((entry->VirtualPort[3] ) >> 5) & MEA_IF_VSP_VSP3_MASK1);

	val1  |= ((0
              << MEA_OS_calc_shift_from_mask(MEA_IF_VSP_VSP3SOP_MASK1)
             ) & MEA_IF_VSP_VSP3SOP_MASK1
            );

	MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_SRV_MAKE0_REG , val0 , MEA_MODULE_IF);
    MEA_API_WriteReg(MEA_UNIT_0,MEA_IF_SRV_MAKE1_REG , val1 , MEA_MODULE_IF);
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_vsp_UpdateHw>                                             */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_vsp_UpdateHw(MEA_Port_t           phisicalPort,
								   MEA_vsp_Entry_dbt    *entry,
								   MEA_vsp_Entry_dbt    *oldEntry)

{

	MEA_ind_write_t ind_write;

    if (!mea_drv_Get_DeviceInfo_IsVspExist(MEA_UNIT_0,MEA_HW_UNIT_ID_GENERAL)) {
        return MEA_OK;
    }

    /* Check if we have changes */
    if (oldEntry != NULL && 
        (MEA_OS_memcmp(entry,oldEntry,sizeof(*entry)) == 0)) {
       return MEA_OK;
    }

    /* build the ind_write value */
	ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYP_VSP;
	ind_write.tableOffset	= phisicalPort;
	ind_write.cmdReg		= MEA_IF_CMD_REG;      
	ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
	ind_write.statusReg		= MEA_IF_STATUS_REG;   
	ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
	ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
	ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
	ind_write.writeEntry    = (MEA_FUNCPTR)mea_vsp_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)entry;

	
    /* Write to the Indirect Table */
	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
                          __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
		return MEA_ERROR;
    }
	return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Init_vsp>                                             */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_IF_VSP(MEA_Unit_t unit_i)
{

    MEA_Port_t port;
#if !defined(MEA_PLAT_S1) && !defined(MEA_ADSL_MODE_64) 
	MEA_Uint8  vsp=0;
#endif
    MEA_Globals_Entry_dbt entry;
    MEA_Uint32 size;
    MEA_PortsMapping_t portMap;


    if(b_bist_test){
        return MEA_OK;
    }
    /* Init IF vsp Table */
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF VSP     table ..\n");


    MEA_OS_memset(&entry,0,sizeof(MEA_Globals_Entry_dbt));
    /* read which system we are work*/
    if (MEA_API_Get_Globals_Entry (unit_i,&entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_API_Get_Globals_Entry failed\n");
       return MEA_OK;
    }
    

    if ((entry.verInfo.val.sysType == MEA_IF_VER_LX_VERSION_H_SYSTYPE_ENET4000_VAL)){
    }          

    mea_vsp_InitDone[MEA_UNIT_0] = MEA_FALSE;

    if (!mea_drv_Get_DeviceInfo_IsVspExist(MEA_UNIT_0,MEA_HW_UNIT_ID_GENERAL)) {
        mea_vsp_InitDone[MEA_UNIT_0] = MEA_TRUE;
        return MEA_OK;
    }


    size = ENET_IF_CMD_PARAM_TBL_TYP_VSP_LENGTH * sizeof(mea_vsp_dbt);
    if (size == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate vsp_dbt_g size is zero \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
        
    vsp_dbt_g = (mea_vsp_dbt*)MEA_OS_malloc(size);
    if (vsp_dbt_g == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Allocate vsp_dbt_g failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
	MEA_OS_memset(vsp_dbt_g,0,size);

	for(port=0;port<ENET_IF_CMD_PARAM_TBL_TYP_VSP_LENGTH;port++) {
        MEA_OS_memset(&portMap,0,sizeof(portMap));
        if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
            MEA_PORTS_TYPE_INGRESS_TYPE,
            (MEA_Uint16)port,
            &portMap)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n",port);
        }
        if((portMap.portType == MEA_PORTTYPE_AAL5_ATM_TC) && (portMap.valid == MEA_TRUE) ){
#if !defined(MEA_PLAT_S1) && !defined(MEA_ADSL_MODE_64)            
            for(vsp=0;vsp<MAX_NUMBER_OF_VSP;vsp++){
			    vsp_dbt_g[port].public_dbt.VirtualPort[vsp]=port*MAX_NUMBER_OF_VSP+vsp;
		    }
#else
            vsp_dbt_g[port].public_dbt.VirtualPort[0]=port;
            vsp_dbt_g[port].public_dbt.VirtualPort[1]=port;
            vsp_dbt_g[port].public_dbt.VirtualPort[2]=port;
            vsp_dbt_g[port].public_dbt.VirtualPort[3]=port;
#endif
        } else {
           //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"port %d\n",port);
            vsp_dbt_g[port].public_dbt.VirtualPort[0]=port;
            vsp_dbt_g[port].public_dbt.VirtualPort[1]=port;
            vsp_dbt_g[port].public_dbt.VirtualPort[2]=port;
            vsp_dbt_g[port].public_dbt.VirtualPort[3]=port;
        }
        
        if ((entry.verInfo.val.sysType == MEA_IF_VER_LX_VERSION_H_SYSTYPE_ENET4000_VAL)&& port == 0){
            //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"port %d\n",port);
            vsp_dbt_g[port].public_dbt.VirtualPort[0]=port;
            vsp_dbt_g[port].public_dbt.VirtualPort[1]=port;
            vsp_dbt_g[port].public_dbt.VirtualPort[2]=port;
            vsp_dbt_g[port].public_dbt.VirtualPort[3]=port;
        } 
        
        
        if (mea_vsp_UpdateHw(port,
							&(vsp_dbt_g[port].public_dbt),
							NULL) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_vsp_UpdateHw %d failed\n",
								  __FUNCTION__,port);
			return MEA_ERROR;
		}
	}

#ifdef MEA_OS_Z2
    for(port=0;port<=7;port++) {
        for(vsp=0;vsp<MAX_NUMBER_OF_VSP;vsp++){
        vsp_dbt_g[port].public_dbt.VirtualPort[vsp]=port*4+vsp;
        }
        if (mea_vsp_UpdateHw(port,
							&(vsp_dbt_g[port].public_dbt),
							NULL) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_vsp_UpdateHw %d failed\n",
								  __FUNCTION__,port);
			return MEA_ERROR;
		}

   }
   for(port=8;port<=31;port++) {
       for(vsp=0;vsp<MAX_NUMBER_OF_VSP;vsp++){
           vsp_dbt_g[port].public_dbt.VirtualPort[vsp]=0;
       }
       if (mea_vsp_UpdateHw(port,
							&(vsp_dbt_g[port].public_dbt),
							NULL) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_vsp_UpdateHw %d failed\n",
								  __FUNCTION__,port);
			return MEA_ERROR;
		}
   }
   for(port=32;port<=47;port++) {
        for(vsp=0;vsp<MAX_NUMBER_OF_VSP;vsp++){
        vsp_dbt_g[port].public_dbt.VirtualPort[vsp]=(port-32)*4+32+vsp;
        }
        if (mea_vsp_UpdateHw(port,
							&(vsp_dbt_g[port].public_dbt),
							NULL) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_vsp_UpdateHw %d failed\n",
								  __FUNCTION__,port);
			return MEA_ERROR;
		}

   } 



#endif
    mea_vsp_InitDone[unit_i] =  MEA_TRUE;
    /* Return to caller */
    return MEA_OK; 
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_ReInit_vsp>                                             */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_IF_VSP(MEA_Unit_t unit_i)
{

    MEA_Port_t port;

    if (!mea_drv_Get_DeviceInfo_IsVspExist(MEA_UNIT_0,MEA_HW_UNIT_ID_GENERAL)) {
        return MEA_OK;
    }


	for(port=0;port<ENET_IF_CMD_PARAM_TBL_TYP_VSP_LENGTH;port++) {
        
        if (mea_vsp_UpdateHw(port,
							&(vsp_dbt_g[port].public_dbt),
							NULL) != MEA_OK) {
			MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
							  "%s - mea_vsp_UpdateHw %d failed\n",
								  __FUNCTION__,port);
			return MEA_ERROR;
		}
	}


   /* Return to caller */
    return MEA_OK; 
}



/************************************************************************/
/*                      VSP_Fragment                                    */
/************************************************************************/


MEA_Bool mea_vsp_fragment_srcPort_support(MEA_Port_t port,MEA_Uint16 *index){

    if(MEA_FRAGMENT_VSP_NUM_OF_PORT == 127){
       *index = port;
        return MEA_TRUE;
    }
    if(MEA_FRAGMENT_VSP_NUM_OF_PORT == 24) {
        if(port<=23)
        *index = port;
        return MEA_TRUE;
    } else {
        if(MEA_FRAGMENT_VSP_NUM_OF_PORT == 1) {
                *index = 0;
              return MEA_TRUE;
        } 
    }
 
return MEA_FALSE;
}


static void mea_fragment_vsp_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t                      unit_i   = (MEA_Unit_t                     )arg1;
    //MEA_db_HwUnit_t                 hwUnit_i = (MEA_db_HwUnit_t                )arg2;
    //MEA_db_HwId_t                   hwId_i   = (MEA_db_HwId_t                  )arg3;
    MEA_fragment_vsp_Data_dbt *entry_pi = (MEA_fragment_vsp_Data_dbt*)arg4;
    MEA_Uint32                 val[ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_VSP_NUM_OF_REGS];
    MEA_Uint32                 i;
    MEA_Uint16                 num_of_regs;
    MEA_Uint32                 count_shift;

    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
        ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_VSP_NUM_OF_REGS);
    for (i=0;i<num_of_regs;i++) {
        val[i] = 0;
    }
    
    count_shift=0;
    
    MEA_OS_insert_value(count_shift,
        9,
        entry_pi->internal_id,
        &val[0]);
    count_shift+=9;

    MEA_OS_insert_value(count_shift,
        1,
        entry_pi->valid,
        &val[0]);
    count_shift+=1;
   
    /* Write to Hardware */
    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_IF_WRITE_DATA_REG(i) , val[i] , MEA_MODULE_IF);
    }

}
static MEA_Status mea_fragment_vsp_UpdateHw(MEA_Unit_t               unit_i,
                                            MEA_Uint32               index,
                                            MEA_fragment_vsp_Data_dbt *data)
{
    
    
    MEA_ind_write_t ind_write;

    /* build the ind_write value */
    ind_write.tableType		= ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_VSP;
    ind_write.tableOffset	= index;
    ind_write.cmdReg		= MEA_IF_CMD_REG;      
    ind_write.cmdMask		= MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg		= MEA_IF_STATUS_REG;   
    ind_write.statusMask	= MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg	= MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask	= MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_fragment_vsp_UpdateHwEntry;
    //ind_write.funcParam1	= (MEA_funcParam)unit_i;
    //ind_write.funcParam2	= (MEA_funcParam)hwUnit;
    //ind_write.funcParam3	= (MEA_funcParam)hwId;
    ind_write.funcParam4 = (MEA_funcParam)data;


    /* Write to the Indirect Table */
    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_IF)!=MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect tbl=%d mdl=%d failed \n",
            __FUNCTION__,ind_write.tableType,MEA_MODULE_IF);
        return MEA_ERROR;
    }
    return MEA_OK;


    return MEA_OK;
}



MEA_Status MEA_API_Set_fragment_vsp_Entry_ByKey (MEA_Unit_t              unit_i,
                                                 MEA_fragment_vsp_Key_dbt*  key_i,
                                                 MEA_fragment_vsp_Data_dbt* data_io, 
                                                 MEA_Uint32 *index_o)
{

    MEA_Uint16 index=0;
    
    MEA_Uint32 calc_index;

    if((!MEA_FRAGMENT_ENABLE)){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - MEA_FRAGMENT_ENABLE not support  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

    if(!MEA_FRAGMENT_VSP_SESSION_SUPPORT){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - FRAGMENT_VSP not support  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS    
    if(key_i == NULL) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - key_i = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

    if(data_io == NULL) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - data_io = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }
    if(index_o == NULL) {
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - index_o = NULL  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

#endif


    if(mea_vsp_fragment_srcPort_support((MEA_Port_t)key_i->src_port,&index) !=MEA_TRUE){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s \n - VSP fragment srcPort %d not support this version  \n",
            __FUNCTION__,key_i->src_port);
        return MEA_ERROR; 
    }

    if(key_i->group >= MEA_FRAGMENT_VSP_NUM_OF_GROUP){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s \n - key_i->group %d  is out of range\n",
            __FUNCTION__,key_i->group);
        return MEA_ERROR;
    }

    if(key_i->session_num >= MEA_FRAGMENT_VSP_NUM_OF_SESSION){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s \n - key_i->group %d  is out of range\n",
            __FUNCTION__,key_i->group);
        return MEA_ERROR;
    }
    
	

    if(data_io->valid ){
        if(key_i->session_num < 128){
            if (MEA_API_Get_IsPortValid((MEA_Port_t)key_i->session_num, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE/*silent*/)==MEA_TRUE) {
                MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                    "%s \n - data_io->internal_id %d  is same value of src port \n",
                    __FUNCTION__,data_io->internal_id);
                return MEA_ERROR;
            } 
        // check if the internal_id already used
        }
        data_io->internal_id=key_i->session_num;
    }

    calc_index = (index * MEA_FRAGMENT_VSP_NUM_OF_GROUP * MEA_FRAGMENT_VSP_NUM_OF_SESSION) + 
        (key_i->group * MEA_FRAGMENT_VSP_NUM_OF_SESSION)+ (key_i->session_num) ;
    if(mea_fragment_vsp_UpdateHw(unit_i,calc_index,data_io) != MEA_OK){

        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
                          "%s \n - mea_fragment_vsp_UpdateHw failed \n",
                                         __FUNCTION__);
                                     return MEA_ERROR;
    }

   MEA_OS_memcpy(&MEA_VSP_fragmentSession_Table[calc_index].key,
                  key_i,
                  sizeof(MEA_fragment_vsp_Key_dbt));
   MEA_OS_memcpy(&MEA_VSP_fragmentSession_Table[calc_index].data,
                    data_io,
                    sizeof(MEA_fragment_vsp_Data_dbt));

  MEA_VSP_fragmentSession_Table[calc_index].valid = data_io->valid;
  
    
    return MEA_OK;
}



MEA_Status MEA_API_Get_fragment_vsp_Entry_id (MEA_Unit_t              unit_i,
                                               MEA_Uint32             index,
                                               MEA_fragment_vsp_Key_dbt*  key_o,
                                               MEA_fragment_vsp_Data_dbt* data_o,
                                               MEA_Bool*               found_o )
{
    if((!MEA_FRAGMENT_ENABLE)){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - MEA_FRAGMENT_ENABLE not support  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }

    if(!MEA_FRAGMENT_VSP_SESSION_SUPPORT){
        MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
            "%s - FRAGMENT_VSP not support  \n",
            __FUNCTION__);
        return MEA_ERROR; 
    }
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS 
    if(key_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                            "%s - key_o == NULL\n",
                            __FUNCTION__);
      return MEA_ERROR;
    }
    if(data_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - data_o == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
    if(found_o == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - found_o == NULL\n",
            __FUNCTION__);
        return MEA_ERROR;
    }
#endif
    
    if(index >= ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_VSP_LENGTH){
        return MEA_ERROR;
    }

    if(!MEA_VSP_fragmentSession_Table[index].valid){
      return MEA_ERROR;
    }

   


    return MEA_OK;
}

MEA_Status MEA_API_Get_fragment_vsp_Entry_ByKey ( MEA_Unit_t              unit_i,
                                                 MEA_fragment_vsp_Key_dbt*  key_i,
                                                 MEA_fragment_vsp_Data_dbt* data_o,
                                                 MEA_Bool*               found_o )
{
    return MEA_OK;
}


MEA_Status mea_drv_VSP_Fragment_ReInit(MEA_Unit_t unit_i)
{
MEA_Uint32 index;
   
  if((!MEA_FRAGMENT_VSP_SESSION_SUPPORT) && (!MEA_FRAGMENT_ENABLE)){
        return MEA_OK; 
    }

    for (index=0; index < ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_VSP_LENGTH;index++)
    {
        if(MEA_VSP_fragmentSession_Table[index].valid == MEA_FALSE){
            continue;
        }
        if(mea_fragment_vsp_UpdateHw(unit_i,index,&MEA_VSP_fragmentSession_Table[index].data)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_fragment_vsp_UpdateHw  failed at ReInit\n",
                __FUNCTION__);
            return MEA_ERROR;
        }

    }



    /* Return to caller */
    return MEA_OK; 
}

MEA_Status mea_drv_VSP_Fragment_init(MEA_Unit_t unit_i)
{

    MEA_Uint32 size;
    if(b_bist_test){
    return MEA_OK;
    }


    if(!MEA_FRAGMENT_ENABLE){
        return MEA_OK;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF fragment VSP     table ..\n");

    if((MEA_FRAGMENT_VSP_SESSION_SUPPORT) && 
       (MEA_FRAGMENT_VSP_NUM_OF_PORT ==  0 || 
       MEA_FRAGMENT_VSP_NUM_OF_GROUP == 0 || 
       MEA_FRAGMENT_VSP_NUM_OF_SESSION == 0) ){
           
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s \n - Init_IF_VSP_Fragment %d failed on of parameter is zero\n",
               __FUNCTION__,"VSP_NUM_OF_PORT =%d VSP_NUM_OF_GROUP =%d  VSP_NUM_OF_SESSION =%d\n",
                                MEA_FRAGMENT_VSP_NUM_OF_PORT,
                                MEA_FRAGMENT_VSP_NUM_OF_GROUP,
                                MEA_FRAGMENT_VSP_NUM_OF_SESSION);
           /* Return to caller */
           return MEA_ERROR; 
    }

        /* Allocate MEA_VSP_fragmentSession_Table */
        if (MEA_VSP_fragmentSession_Table == NULL) {
            size = (MEA_FRAGMENT_VSP_NUM_OF_PORT     * 
                    MEA_FRAGMENT_VSP_NUM_OF_GROUP   * 
                    MEA_FRAGMENT_VSP_NUM_OF_SESSION * sizeof(MEA_drv_vsp_fragmentSession_dbt));
            MEA_VSP_fragmentSession_Table = (MEA_drv_vsp_fragmentSession_dbt*)MEA_OS_malloc(size);
            if (MEA_VSP_fragmentSession_Table == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Allocate MEA_VSP_fragmentSession_Table failed (size=%d)\n",
                    __FUNCTION__,
                    size);
                return MEA_ERROR;
            }
            MEA_OS_memset(MEA_VSP_fragmentSession_Table,0,size);
        }



    /* Return to caller */
    return MEA_OK; 
}


MEA_Status mea_drv_VSP_Fragment_Conclude(MEA_Unit_t unit)
{


    
        if (MEA_VSP_fragmentSession_Table!= NULL) {
            MEA_OS_free(MEA_VSP_fragmentSession_Table);
            MEA_VSP_fragmentSession_Table = NULL;
        }



    /* Return to caller */
    return MEA_OK; 
}






/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*          <MEA_API_ATM_AutoSense>                                                */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
MEA_Status MEA_API_ATM_AutoSense(MEA_Unit_t unit_i,
								 MEA_Port_t port,
								 MEA_Uint16  vspId,
								 MEA_Uint32 *retProtocol,
								 MEA_Bool *exist)
{

 MEA_Port_type_te porttype;
 MEA_Uint32 val=0;

	  if (retProtocol == NULL) {
		  MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
			  "%s - retProtocol = NULL  \n",
			  __FUNCTION__);
		  return MEA_ERROR; 
	  }
	  if (exist == NULL) {
		  MEA_OS_LOG_logMsg (MEA_OS_LOG_ERROR,
			  "%s - exist = NULL  \n",
			  __FUNCTION__);
		  return MEA_ERROR; 
	  }
	  
	  if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE/*silent*/)==MEA_FALSE) {
		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s port %d  is invalid \n",__FUNCTION__,port);
		  return MEA_ERROR;
	  } 

	*exist =MEA_FALSE;
	if(vspId <= MEA_NUM_OF_UTOPIA_VSP){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s vspId %d  is out of the range  \n",__FUNCTION__,port);
		return MEA_ERROR;
	}
	if(MEA_API_Get_IngressPort_Type(unit_i,port ,&porttype)!=MEA_OK){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," MEA_API_Get_IngressPort_Type failed port=%d\n",port);
		return MEA_ERROR;
	}
	if(porttype != MEA_PORTTYPE_AAL5_ATM_TC){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," The  port=%d is not Utopia \n",port);
		return MEA_ERROR;
	}
	if(porttype == MEA_PORTTYPE_AAL5_ATM_TC ){
		if(val != 0){
			*exist =MEA_TRUE;
			*retProtocol=val;
		}
	}
	return MEA_OK;
}


/************************************************************************/
/* MTU Action drop                                                     */
/************************************************************************/

static void mea_drv_AcMTU_prof_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i=0;
    MEA_Uint32               num_of_regs;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=entry;
    num_of_regs=1;




    /* Update Hw Entry */
    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }



}





static MEA_Status mea_drv_AcMTU_prof_UpdateHw(MEA_Unit_t             unit_i,
                                                MEA_Uint16           id_i,
                                                MEA_AC_Mtu_dbt      *new_entry_pi,
                                                MEA_AC_Mtu_dbt      *old_entry_pi)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32              value;
    MEA_ind_write_t ind_write;
    
    

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

        /* check if we have any changes */
      
        /* check if we have any changes */
        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi))== 0) ) { 
                return MEA_OK;
        }  
        
        
        value=new_entry_pi->mtu_size;


        /* Update the HW */
        ind_write.tableOffset	= id_i;
        ind_write.tableType		= ENET_BM_TBL_TYP_ACT_MTU_PROFILE_DROP;
        ind_write.cmdReg		= MEA_BM_IA_CMD;      
        ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
        ind_write.statusReg		= MEA_BM_IA_STAT;   
        ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
        ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
        ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
        ind_write.writeEntry    =(MEA_FUNCPTR)mea_drv_AcMTU_prof_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2	= (MEA_funcParam)hwUnit;
        //ind_write.funcParam3	= (MEA_funcParam)hwId;
        ind_write.funcParam4 = (MEA_funcParam)((long)value);
        ind_write.numofregs = (MEA_funcParam)((long)MEA_BM_IA_MAX_NUM_OF_WRITE_REGS);
        //ind_write.retval        = (MEA_Uint32)&retVal;

        if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM)==MEA_ERROR) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
                __FUNCTION__,
                ind_write.tableType,
                ind_write.tableOffset,
                MEA_MODULE_BM);
            return MEA_ERROR;
        }   



        /* Return to caller */
        return MEA_OK;
}


MEA_Status mea_drv_AC_MTU_prof_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;

    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_ACT_MTU_SUPPORT){
            return MEA_OK;   
        }


        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Ac MTU ...\n");


        /* Allocate PacketGen Table */
        size = MEA_MAX_ACT_MTU_ID * sizeof(MEA_acMtu_dbt);
        if (size != 0) {
            MEA_AcMTU_info_Table = (MEA_acMtu_dbt*)MEA_OS_malloc(size);
            if (MEA_AcMTU_info_Table == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Allocate MEA_AcMTU_info_Table failed (size=%d)\n",
                    __FUNCTION__,size);
                return MEA_ERROR;
            }
            MEA_OS_memset(&(MEA_AcMTU_info_Table[0]),0,size);
        }
        /**/
        // Hw set zero on the init
        MEA_AcMTU_info_Table[0].data.mtu_size=MEA_Platform_GetMaxMTU();
        MEA_AcMTU_info_Table[0].valid=MEA_TRUE;
        mea_drv_AcMTU_prof_UpdateHw(unit_i,0,&MEA_AcMTU_info_Table[0].data,NULL);



        return MEA_OK;
}



MEA_Status mea_drv_AC_MTU_prof_RInit(MEA_Unit_t       unit_i)
{


    MEA_Uint16 id;

    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_ACT_MTU_SUPPORT){
            return MEA_OK;   
        }

        for (id=0;id<MEA_MAX_ACT_MTU_ID;id++) {
            if (MEA_AcMTU_info_Table[id].valid){
                if(mea_drv_AcMTU_prof_UpdateHw(unit_i,id,&MEA_AcMTU_info_Table[id].data,NULL)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_drv_AcMTU_prof_UpdateHw failed (id=%d)\n",
                        __FUNCTION__,id);
                    return MEA_ERROR;
                }
            }
        }


        return MEA_OK;
}

MEA_Status mea_drv_AC_MTU_prof_Conclude(MEA_Unit_t       unit_i)
{
    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_ACT_MTU_SUPPORT){
            return MEA_OK;   
        }

        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude AC_MTU_prof..\n");

        /* Free the table */
        if (MEA_AcMTU_info_Table != NULL) {
            MEA_OS_free(MEA_AcMTU_info_Table);
            MEA_AcMTU_info_Table = NULL;
        }

        return MEA_OK;
}




MEA_Status MEA_API_Set_Flow_MTU_Prof(MEA_Unit_t                     unit_i,
                                    MEA_Uint16                      id_i,
                                    MEA_AC_Mtu_dbt                  *entry_pi)
{
    

    MEA_API_LOG

        /* Check if support */
        if (!MEA_ACT_MTU_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_ACT_MTU_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }


#if 0
        if(id_i == 0 ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Ac MTU for ID %d  can't be change  \n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
       
#endif
        /* check parameter*/

        if(id_i >=MEA_MAX_ACT_MTU_ID)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s -  ID %d  is out of range  \n",
                __FUNCTION__,id_i);
            return MEA_ERROR;
        }
        /************************************************************************/
        /* mtu                                                          */
        /************************************************************************/
      
        if(entry_pi->mtu_size > MEA_Platform_GetMaxMTU() ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Ac MTU if bigger the the Max  \n",
                __FUNCTION__);
            return MEA_ERROR;
        }



       

       
       if(mea_drv_AcMTU_prof_UpdateHw(unit_i,id_i,entry_pi,&MEA_AcMTU_info_Table[id_i].data) !=MEA_OK)
       {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
               "%s - mea_drv_AcMTU_prof_UpdateHw fail  for id %d\n",
               __FUNCTION__,id_i);
       }
        
        if(entry_pi->mtu_size !=0)
            MEA_AcMTU_info_Table[id_i].valid=MEA_TRUE;
        else
            MEA_AcMTU_info_Table[id_i].valid=MEA_FALSE;

        MEA_OS_memcpy(&(MEA_AcMTU_info_Table[id_i].data),
            entry_pi,
            sizeof(MEA_AcMTU_info_Table[id_i].data));

        



        return MEA_OK;
}


MEA_Status MEA_API_Get_Flow_MTU_Prof(MEA_Unit_t                     unit_i,
                                    MEA_Uint16                      id_i,
                                    MEA_AC_Mtu_dbt                  *entry_po,
                                    MEA_Bool                        *exist)
{

    if (!MEA_ACT_MTU_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ACT_MTU_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;   
    }

    if(entry_po==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_po is null\n",__FUNCTION__,id_i);
        return MEA_ERROR;

    }
    if(exist==NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - exist is null\n",__FUNCTION__,id_i);
        return MEA_ERROR;

    }

    if(id_i>= MEA_MAX_ACT_MTU_ID){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - out of range for ACT_MTU profile \n",__FUNCTION__,id_i);
        return MEA_ERROR;
    }
        

    if(MEA_AcMTU_info_Table[id_i].valid != MEA_TRUE){
     *exist=MEA_FALSE;
     return MEA_OK;
    }
    
    *exist=MEA_AcMTU_info_Table[id_i].valid;

    MEA_OS_memcpy(entry_po,&(MEA_AcMTU_info_Table[id_i].data),
        
        sizeof(*entry_po));



 return MEA_OK;

}


MEA_Status MEA_API_Flow_MTU_Prof_GetFirst(MEA_Unit_t                         unit_i,
    MEA_Uint16                        *id_o,
    MEA_AC_Mtu_dbt                  *entry_po, /* Can't be NULL */
    MEA_Bool                          *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_ACT_MTU_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_ACT_MTU_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }

        if (id_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_o)=0;
            (*id_o) < MEA_MAX_ACT_MTU_ID; 
            (*id_o)++ ) {
                if (MEA_AcMTU_info_Table[(*id_o)].valid == MEA_TRUE) {
                    
                        *found_o= MEA_TRUE;
                    

                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_AcMTU_info_Table[(*id_o)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;

}

MEA_Status MEA_API_Flow_MTU_Prof_GetNext(MEA_Unit_t                unit_i,
    MEA_Uint16                        *id_io,
    MEA_AC_Mtu_dbt                  *entry_po, /* Can't be NULL */
    MEA_Bool                          *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_ACT_MTU_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_ACT_MTU_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;   
        }
        if (id_io == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }

        if (found_o == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
            return MEA_ERROR;
        }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

        if (found_o) {
            *found_o = MEA_FALSE;
        }

        for ( (*id_io)++;
            (*id_io) < MEA_MAX_ACT_MTU_ID; 
            (*id_io)++ ) {
                if(MEA_AcMTU_info_Table[(*id_io)].valid == MEA_TRUE) {
                        *found_o= MEA_TRUE;
                   
                    if (entry_po) {
                        MEA_OS_memcpy(entry_po,
                            &MEA_AcMTU_info_Table[(*id_io)].data,
                            sizeof(*entry_po));
                    }
                    break;
                }
        }


        return MEA_OK;
}



/************************************************************************/
/*                                                                      */
/************************************************************************/

static void mea_drv_TargetMTU_prof_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




    MEA_Uint32                 val[1];
    MEA_Uint32                 i = 0;
    MEA_Uint32               num_of_regs;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    //count_shift=0;

    val[0] = entry;
    num_of_regs = 1;




    /* Update Hw Entry */
    for (i = 0; i < num_of_regs; i++) {
        MEA_API_WriteReg(unit_i, MEA_BM_IA_WRDATA(i), val[i], MEA_MODULE_BM);
    }



}





static MEA_Status mea_drv_Target_MTU_prof_UpdateHw(MEA_Unit_t             unit_i,
    MEA_Uint16           id_i,
    MEA_AC_Mtu_dbt      *new_entry_pi,
    MEA_AC_Mtu_dbt      *old_entry_pi)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32              value;
    MEA_ind_write_t ind_write;



    MEA_DRV_GET_HW_UNIT(unit_i, hwUnit, return MEA_ERROR;)

        /* check if we have any changes */

        /* check if we have any changes */
        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
                old_entry_pi,
                sizeof(*new_entry_pi)) == 0)) {
            return MEA_OK;
        }


    value = new_entry_pi->mtu_size;
    if (value == 0) {
        value = MEA_Platform_GetMaxMTU();
    }

    /* Update the HW */
    ind_write.tableType = ENET_BM_TBL_TYP_GW_GLOBAL_REGS;
    ind_write.tableOffset = MEA_GLOBAl_TBL_REG_TARGET_MTU_PROF0 +(id_i);
    
    ind_write.cmdReg = MEA_BM_IA_CMD;
    ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_write.statusReg = MEA_BM_IA_STAT;
    ind_write.statusMask = MEA_BM_IA_STAT_MASK;
    ind_write.tableAddrReg = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_TargetMTU_prof_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2	= (MEA_funcParam)hwUnit;
    //ind_write.funcParam3	= (MEA_funcParam)hwId;
    ind_write.funcParam4 = (MEA_funcParam)((long)value);
    ind_write.numofregs = (MEA_funcParam)((long)MEA_BM_IA_MAX_NUM_OF_WRITE_REGS);
    //ind_write.retval        = (MEA_Uint32)&retVal;

    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) == MEA_ERROR) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect failed (type=%d,offset=%d,module=%d)\n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            MEA_MODULE_BM);
        return MEA_ERROR;
    }



    /* Return to caller */
    return MEA_OK;
}






MEA_Status mea_drv_Target_MTU_prof_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32   size;
    MEA_Uint16   id_i;

    if (b_bist_test) {
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_TARGET_MTU_SUPPORT) {
            return MEA_OK;
        }


    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize Ac MTU ...\n");


    /* Allocate MEA_Target_MTU_info_Table Table */
    size = MEA_TARGET_MTU_MAX_PROF * sizeof(MEA_acMtu_dbt);
    if (size != 0) {
        MEA_Target_MTU_info_Table = (MEA_acMtu_dbt*)MEA_OS_malloc(size);
        if (MEA_Target_MTU_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_Target_MTU_info_Table failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_Target_MTU_info_Table[0]), 0, size);
    }
    /**/
    // Hw set zero on the init
    for (id_i = 0; id_i < MEA_TARGET_MTU_MAX_PROF; id_i++) {
        MEA_Target_MTU_info_Table[id_i].data.mtu_size = MEA_Platform_GetMaxMTU();
        MEA_Target_MTU_info_Table[id_i].valid = MEA_FALSE;
        mea_drv_Target_MTU_prof_UpdateHw(unit_i, id_i, &MEA_AcMTU_info_Table[id_i].data, NULL);
    }


    return MEA_OK;
}



MEA_Status mea_drv_Target_MTU_prof_RInit(MEA_Unit_t       unit_i)
{


    MEA_Uint16 id;

    if (b_bist_test) {
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
    if (!MEA_TARGET_MTU_SUPPORT) {
            return MEA_OK;
    }

    for (id = 0; id < MEA_TARGET_MTU_MAX_PROF; id++) {
        if (MEA_Target_MTU_info_Table[id].valid) {
            if (mea_drv_Target_MTU_prof_UpdateHw(unit_i, id, &MEA_Target_MTU_info_Table[id].data, NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_AcMTU_prof_UpdateHw failed (id=%d)\n",
                    __FUNCTION__, id);
                //return MEA_ERROR;
            }
        }
        else {
            MEA_Target_MTU_info_Table[id].data.mtu_size = MEA_Platform_GetMaxMTU();
            MEA_Target_MTU_info_Table[id].valid = MEA_FALSE;
            if (mea_drv_Target_MTU_prof_UpdateHw(unit_i, id, &MEA_Target_MTU_info_Table[id].data, NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_AcMTU_prof_UpdateHw failed (id=%d)\n",
                    __FUNCTION__, id);
                //return MEA_ERROR;
            }
        }
    }


    return MEA_OK;
}

MEA_Status mea_drv_Target_MTU_prof_Conclude(MEA_Unit_t       unit_i)
{
    if (b_bist_test) {
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_TARGET_MTU_SUPPORT) {
            return MEA_OK;
        }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude Target_MTU_prof..\n");

    /* Free the table */
    if (MEA_Target_MTU_info_Table != NULL) {
        MEA_OS_free(MEA_Target_MTU_info_Table);
        MEA_Target_MTU_info_Table = NULL;
    }

    return MEA_OK;
}


MEA_Status MEA_API_Set_Target_MTU_Prof(MEA_Unit_t                     unit_i,
    MEA_Uint16                      id_i,
    MEA_AC_Mtu_dbt                  *entry_pi)
{


    MEA_API_LOG

        /* Check if support */
        if (!MEA_TARGET_MTU_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_TARGET_MTU_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;
        }


    /* check parameter*/

    if (id_i >= MEA_TARGET_MTU_MAX_PROF)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  ID %d  is out of range  \n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }
    /************************************************************************/
    /* mtu                                                          */
    /************************************************************************/

    if (entry_pi->mtu_size > 16000) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Ac MTU if bigger the the Max  \n",
            __FUNCTION__);
        return MEA_ERROR;
    }






    if (mea_drv_Target_MTU_prof_UpdateHw(unit_i, id_i, entry_pi, &MEA_AcMTU_info_Table[id_i].data) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_AcMTU_prof_UpdateHw fail  for id %d\n",
            __FUNCTION__, id_i);
    }

    if (entry_pi->mtu_size != 0)
        MEA_Target_MTU_info_Table[id_i].valid = MEA_TRUE;
    else
        MEA_Target_MTU_info_Table[id_i].valid = MEA_FALSE;

    MEA_OS_memcpy(&(MEA_Target_MTU_info_Table[id_i].data),
        entry_pi,
        sizeof(MEA_Target_MTU_info_Table[id_i].data));





    return MEA_OK;
}

MEA_Status MEA_API_Get_Target_MTU_Prof(MEA_Unit_t                     unit_i,
    MEA_Uint16                      id_i,
    MEA_AC_Mtu_dbt                  *entry_po,
    MEA_Bool                        *exist)
{

    if (!MEA_TARGET_MTU_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ACT_MTU_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry_po == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_po is null\n", __FUNCTION__, id_i);
        return MEA_ERROR;

    }
    if (exist == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - exist is null\n", __FUNCTION__, id_i);
        return MEA_ERROR;

    }

    if (id_i >= MEA_TARGET_MTU_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - out of range for TARGET_MTU profile \n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }


    if (MEA_Target_MTU_info_Table[id_i].valid != MEA_TRUE) {
        *exist = MEA_FALSE;
        return MEA_OK;
    }

    *exist = MEA_Target_MTU_info_Table[id_i].valid;

    MEA_OS_memcpy(entry_po, &(MEA_Target_MTU_info_Table[id_i].data),

        sizeof(*entry_po));



    return MEA_OK;

}


MEA_Status MEA_API_Target_MTU_Prof_GetFirst(MEA_Unit_t                         unit_i,
    MEA_Uint16                        *id_o,
    MEA_AC_Mtu_dbt                  *entry_po, /* Can't be NULL */
    MEA_Bool                          *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

        if (!MEA_TARGET_MTU_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_TARGET_MTU_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

    if (id_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - id_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (found_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ((*id_o) = 0;
        (*id_o) < MEA_TARGET_MTU_MAX_PROF;
        (*id_o)++) {
        if (MEA_Target_MTU_info_Table[(*id_o)].valid == MEA_TRUE) {

            *found_o = MEA_TRUE;


            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                    &MEA_Target_MTU_info_Table[(*id_o)].data,
                    sizeof(*entry_po));
            }
            break;
        }
    }


    return MEA_OK;

}

MEA_Status MEA_API_Target_MTU_Prof_GetNext(MEA_Unit_t                unit_i,
    MEA_Uint16                        *id_io,
    MEA_AC_Mtu_dbt                  *entry_po, /* Can't be NULL */
    MEA_Bool                          *found_o)
{
    MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
        if (!MEA_TARGET_MTU_SUPPORT) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_TARGET_MTU_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    if (id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - Id_io == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (found_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - found_o == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

    if (found_o) {
        *found_o = MEA_FALSE;
    }

    for ((*id_io)++;
        (*id_io) < MEA_TARGET_MTU_MAX_PROF;
        (*id_io)++) {
        if (MEA_Target_MTU_info_Table[(*id_io)].valid == MEA_TRUE) {
            *found_o = MEA_TRUE;

            if (entry_po) {
                MEA_OS_memcpy(entry_po,
                    &MEA_Target_MTU_info_Table[(*id_io)].data,
                    sizeof(*entry_po));
            }
            break;
        }
    }


    return MEA_OK;
}



/************************************************************************/
/* VP_MSTP                                                          */
/************************************************************************/
static void mea_drv_VP_MSTP_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_VP_MSTP_entry_dbt  *new_entry_pi= (MEA_VP_MSTP_entry_dbt * )arg4;




    MEA_Uint32                 val[10];
    MEA_Uint32                 i=0;
    MEA_Uint32                 n=10;
    MEA_Uint32                 infoId=0;
    //MEA_Uint32                  count_shift;

    infoId = mea_drv_Get_chip_number();

    //Board 325
    if ((infoId) >= 7100 && (infoId < 7200))
    {
        n = 5;
    }
    //Board 410 and other
    else
    {
        n = 10;
    }

    /* Zero  the val */
    for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
        val[i]=0;
    }
    //count_shift=0;

    val[0]=new_entry_pi->BLK_regularPacket.out_ports_0_31;
    val[1]=new_entry_pi->BLK_regularPacket.out_ports_32_63;

    val[2]=new_entry_pi->BLK_l2cpPacket.out_ports_0_31;
    val[3]=new_entry_pi->BLK_l2cpPacket.out_ports_32_63;
    val[4] = new_entry_pi->groupId; /**/




    /* Update Hw Entry */
    for (i=0;i < n;i++)
    {
        MEA_API_WriteReg(unit_i,
            MEA_IF_WRITE_DATA_REG(i),
            val[i],
            MEA_MODULE_IF);  
    }



}

static MEA_Status mea_drv_VP_MSTP_UpdateHw(MEA_Unit_t    unit_i,
                                           MEA_Uint16             id_i,
    MEA_VP_MSTP_entry_dbt    *new_entry_pi,
    MEA_VP_MSTP_entry_dbt    *old_entry_pi)
{

    MEA_db_HwUnit_t       hwUnit;
    MEA_ind_write_t      ind_write;


    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

        /* check if we have any changes */
        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi))== 0) ) { 
                return MEA_OK;
        }



        /* Prepare ind_write structure */
        ind_write.tableType     = ENET_IF_CMD_TBL_MSTP_VPN;
        ind_write.tableOffset   = id_i; 
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_VP_MSTP_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
        //ind_write.funcParam3    = (MEA_funcParam);



       
        
        ind_write.funcParam4 = (MEA_funcParam)(new_entry_pi);

        /* Write to the Indirect Table */;
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }



        /* Return to caller */
        return MEA_OK;
}

MEA_Status mea_drv_VP_MSTP_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;
//    MEA_Uint16 id;
    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
    if (!MEA_DRV_MSTP_SUPPORT && !MEA_DRV_RSTP_SUPPORT){
            return MEA_OK;   
        }


        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize VP_MSTP ...\n");


        /* Allocate PacketGen Table */
        size = MEA_DRV_NUM_OF_MSTP  * sizeof(MEA_VP_MSTP_entry_dbt);
        if (size != 0) {
            MEA_VP_MSTP_info_Table = (MEA_VP_MSTP_entry_dbt*)MEA_OS_malloc(size);
            if (MEA_VP_MSTP_info_Table == NULL) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Allocate MEA_DSA_Dest_vl_info_Table failed (size=%d)\n",
                    __FUNCTION__,size);
                return MEA_ERROR;
            }
            MEA_OS_memset(&(MEA_VP_MSTP_info_Table[0]),0,size);


        }
        /**/
        // Hw set zero on the init
//          for (id=0;id<MEA_DRV_NUM_OF_MSTP;id++) {
//              MEA_VP_MSTP_info_Table[id].valid=MEA_TRUE;
//              MEA_API_Set_VP_State_Clear(unit_i, id);
//              MEA_VP_MSTP_info_Table[id].valid = MEA_FALSE;
//          }



        return MEA_OK;
}
MEA_Status mea_drv_VP_MSTP_RInit(MEA_Unit_t       unit_i)
{


    MEA_Uint16 id;

    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
    if (!MEA_DRV_MSTP_SUPPORT && !MEA_DRV_RSTP_SUPPORT){
            return MEA_OK;   
        }


        for (id=0;id<MEA_DRV_NUM_OF_MSTP;id++) {
            if (MEA_VP_MSTP_info_Table[id].valid == MEA_TRUE  ){
                if(mea_drv_VP_MSTP_UpdateHw(unit_i,0,&MEA_VP_MSTP_info_Table[id],NULL)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_drv_VP_MSTP_UpdateHw failed (id=%d)\n",
                        __FUNCTION__,id);
                    return MEA_ERROR;
                }
            }
        }


        return MEA_OK;
}
MEA_Status mea_drv_VP_MSTP_Conclude(MEA_Unit_t       unit_i)
{
    if(b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
    if (!MEA_DRV_MSTP_SUPPORT && !MEA_DRV_RSTP_SUPPORT){
            return MEA_OK;   
        }

        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude VP_MSTP..\n");

        /* Free the table */
        if (MEA_VP_MSTP_info_Table != NULL) {
            MEA_OS_free(MEA_VP_MSTP_info_Table);
            MEA_VP_MSTP_info_Table = NULL;
        }

        return MEA_OK;
}







#define	NUM_OF_HW_UPDATES	10

MEA_Status MEA_API_Set_VP_State(MEA_Unit_t unit, MEA_Uint16  vpn, ENET_QueueId_t  Cluster_id, MEA_VP_State_t State)
{

    ENET_Queue_dbt        entry_o;
    ENET_QueueId_t internalQid;
    MEA_VP_MSTP_entry_dbt  Mstp_info;
    MEA_Uint32 i;


    char  State_str[MEA_VP_STATE_BLK_ALL + 1][20];

    MEA_OS_strcpy(&(State_str[MEA_VP_STATE_FWD][0]), "STATE_FWD");
    MEA_OS_strcpy(&(State_str[MEA_VP_STATE_BLK_REGULAR][0]), "BLK_REGULAR");
    MEA_OS_strcpy(&(State_str[MEA_VP_STATE_BLK_L2CP][0]), "BLK_L2CP");
    MEA_OS_strcpy(&(State_str[MEA_VP_STATE_BLK_ALL][0]), "BLK_ALL");

    if (!MEA_DRV_MSTP_SUPPORT && !MEA_DRV_RSTP_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MSTP_SUPPORT is not support on this hw \n", __FUNCTION__);
        return MEA_ERROR;
    }



    if (vpn >= MEA_DRV_NUM_OF_MSTP){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s vpn is out of  \n", __FUNCTION__);
        return MEA_ERROR;
    }


    /************************************************************************/
    /* check cluster  only cluster on group 0 support                       */
    /************************************************************************/



    if (ENET_IsValid_Queue
        (unit, Cluster_id, MEA_FALSE) == ENET_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Cluster_id is not valid  \n", __FUNCTION__);
        return MEA_ERROR;

    }
    MEA_OS_memset(&entry_o, 0, sizeof(entry_o));

    if (ENET_Get_Queue(unit, Cluster_id, &entry_o) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Cluster_id fail on get info  \n", __FUNCTION__);
        return MEA_ERROR;
    }

//     if (entry_o.groupId != 0){
//         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " only cluster on group 0 support   \n", __FUNCTION__);
//         return MEA_ERROR;
//     }

    /*check if the state change */

    MEA_OS_memcpy(&Mstp_info, &MEA_VP_MSTP_info_Table[vpn], sizeof(Mstp_info));

    internalQid = enet_cluster_external2internal(Cluster_id);

    if (MEA_VP_MSTP_info_Table[vpn].valid == MEA_TRUE){
        if (MEA_VP_MSTP_info_Table[vpn].groupId != entry_o.groupId)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " cluster %d group %d is not same of other set cluster %d    \n",  Cluster_id, entry_o.groupId, MEA_VP_MSTP_info_Table[vpn].groupId);
            return MEA_ERROR;
        }
        
    }
    else{
        MEA_VP_MSTP_info_Table[vpn].groupId = entry_o.groupId;
        Mstp_info.groupId = entry_o.groupId;
        
    }

    switch (State)
    {
    case MEA_VP_STATE_FWD:
        MEA_CLEAR_OUTPORT(&Mstp_info.BLK_regularPacket, internalQid);
        MEA_CLEAR_OUTPORT(&Mstp_info.BLK_l2cpPacket, internalQid);
        MEA_CLEAR_OUTPORT(&Mstp_info.user_regularPacket, Cluster_id);
        MEA_CLEAR_OUTPORT(&Mstp_info.user_l2cpPacket, Cluster_id);
        break;
    case MEA_VP_STATE_BLK_REGULAR:
        MEA_SET_OUTPORT(&Mstp_info.BLK_regularPacket, internalQid);
        MEA_CLEAR_OUTPORT(&Mstp_info.BLK_l2cpPacket, internalQid);
        MEA_SET_OUTPORT(&Mstp_info.user_regularPacket, Cluster_id);
        MEA_CLEAR_OUTPORT(&Mstp_info.user_l2cpPacket, Cluster_id);

        break;
    case MEA_VP_STATE_BLK_L2CP:
        MEA_CLEAR_OUTPORT(&Mstp_info.BLK_regularPacket, internalQid);
        MEA_SET_OUTPORT(&Mstp_info.BLK_l2cpPacket, internalQid);
        MEA_CLEAR_OUTPORT(&Mstp_info.user_regularPacket, Cluster_id);
        MEA_SET_OUTPORT(&Mstp_info.user_l2cpPacket, Cluster_id);
        break;
    case MEA_VP_STATE_BLK_ALL:
        MEA_SET_OUTPORT(&Mstp_info.BLK_regularPacket, internalQid);
        MEA_SET_OUTPORT(&Mstp_info.BLK_l2cpPacket, internalQid);
        MEA_SET_OUTPORT(&Mstp_info.user_regularPacket, Cluster_id);
        MEA_SET_OUTPORT(&Mstp_info.user_l2cpPacket, Cluster_id);
        break;
    }

   for (i = 0; i < NUM_OF_HW_UPDATES; i++) {
	   if (mea_drv_VP_MSTP_UpdateHw(unit, vpn, &Mstp_info, &MEA_VP_MSTP_info_Table[vpn]) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "mea_drv_VP_MSTP_UpdateHw fail   \n", __FUNCTION__);
            return MEA_ERROR;
   	   }
   }

   



    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "SET VPN %d -->cluster %d State  %s\n", vpn, Cluster_id, State_str[State]);
    //QueId_Internal2External(ENET_QueueId_t internal_Qid)
    if (MEA_IS_CLEAR_ALL_OUTPORT(&Mstp_info.BLK_regularPacket) == MEA_TRUE && MEA_IS_CLEAR_ALL_OUTPORT(&Mstp_info.BLK_regularPacket) == MEA_TRUE)
    {
       
        Mstp_info.valid = MEA_FALSE;
    }
    else{
        if (MEA_IS_CLEAR_ALL_OUTPORT(&Mstp_info.BLK_regularPacket) == MEA_FALSE || MEA_IS_CLEAR_ALL_OUTPORT(&Mstp_info.BLK_regularPacket) == MEA_FALSE)
        {
            Mstp_info.valid = MEA_TRUE;
           
        }

    }


    MEA_OS_memcpy(&MEA_VP_MSTP_info_Table[vpn], &Mstp_info, sizeof(MEA_VP_MSTP_info_Table[0]));
    if (Mstp_info.valid == MEA_FALSE){
        MEA_OS_memset(&MEA_VP_MSTP_info_Table[vpn], 0, sizeof(MEA_VP_MSTP_entry_dbt));
        MEA_VP_MSTP_info_Table[vpn].valid = MEA_FALSE;
    }
    




    return MEA_OK;
}


MEA_Status MEA_API_Set_VP_State_Clear(MEA_Unit_t unit , MEA_Uint16  vpn)
{
    MEA_VP_MSTP_entry_dbt  Mstp_info;
    MEA_Uint32 i;
    
    if (!MEA_DRV_MSTP_SUPPORT && !MEA_DRV_RSTP_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MSTP_SUPPORT is not support on this hw \n",__FUNCTION__);
        return MEA_ERROR;   
    }



    if( vpn >= MEA_DRV_NUM_OF_MSTP){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s vpn is out of  \n",__FUNCTION__);
        return MEA_ERROR; 
    }

    
    MEA_OS_memcpy(&Mstp_info,&MEA_VP_MSTP_info_Table[vpn],sizeof(Mstp_info));

    MEA_OS_memset(&Mstp_info.BLK_l2cpPacket,0,sizeof(Mstp_info.BLK_l2cpPacket));
    MEA_OS_memset(&Mstp_info.BLK_regularPacket,0,sizeof(Mstp_info.BLK_regularPacket));

    for (i = 0; i < NUM_OF_HW_UPDATES; i++) {
    	if (mea_drv_VP_MSTP_UpdateHw(unit, vpn, &Mstp_info, &MEA_VP_MSTP_info_Table[vpn]) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "mea_drv_VP_MSTP_UpdateHw fail   \n", __FUNCTION__);
            return MEA_ERROR;
    	}
    }

    MEA_OS_memcpy(&MEA_VP_MSTP_info_Table[vpn],&Mstp_info,sizeof(MEA_VP_MSTP_info_Table[0]));


 
    return MEA_OK;
}

MEA_Status MEA_API_Get_VP_StateAsBLK(MEA_Unit_t unit, MEA_Uint16  vpn, MEA_Bool *exist)
{
   
    if (!MEA_DRV_MSTP_SUPPORT && !MEA_DRV_RSTP_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "DRV_MSTP_SUPPORT not support \n", __FUNCTION__);
        return MEA_ERROR;
    }
    
    if (exist == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "exist == NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }
    
    if (MEA_VP_MSTP_info_Table[vpn].valid == MEA_FALSE){
        *exist = MEA_FALSE;
    }
    else{
        *exist = MEA_TRUE;
    }

    return MEA_OK;
}

MEA_Status MEA_API_Get_VP_State(MEA_Unit_t unit, MEA_Uint16  vpn, MEA_VP_State_t State, MEA_OutPorts_Entry_dbt *out_blk)
{
    MEA_OutPorts_Entry_dbt Allout;
    if (!MEA_DRV_MSTP_SUPPORT && !MEA_DRV_RSTP_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "DRV_MSTP_SUPPORT not support \n", __FUNCTION__);
        return MEA_ERROR;
    }


    if (out_blk == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "out_blk == NULL \n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (State == MEA_VP_STATE_FWD)
    {
        if(MEA_VP_MSTP_info_Table[vpn].valid == MEA_FALSE)
            return MEA_OK;
        else{
            return MEA_OK;
        }
    }
    if (State == MEA_VP_STATE_BLK_REGULAR){
        MEA_OS_memcpy(out_blk, &MEA_VP_MSTP_info_Table[vpn].user_regularPacket, sizeof(MEA_OutPorts_Entry_dbt));
        return MEA_OK;
    }

    if (State == MEA_VP_STATE_BLK_L2CP){
        MEA_OS_memcpy(out_blk, &MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket, sizeof(MEA_OutPorts_Entry_dbt));
        return MEA_OK;
    }

    if (State == MEA_VP_STATE_BLK_ALL){
        MEA_OS_memset(&Allout, 0, sizeof(MEA_OutPorts_Entry_dbt));
            
        Allout.out_ports_0_31    = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_0_31    &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_0_31;
        Allout.out_ports_32_63   = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_32_63   &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_32_63;
        Allout.out_ports_64_95   = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_64_95   &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_64_95;
        Allout.out_ports_96_127  = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_96_127  &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_96_127;
#ifdef MEA_OUT_PORT_128_255
        Allout.out_ports_128_159 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_128_159 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_128_159;
        Allout.out_ports_160_191 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_160_191 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_160_191;
        Allout.out_ports_192_223 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_192_223 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_192_223;
        Allout.out_ports_224_255 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_224_255 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_224_255;
#endif
#ifdef MEA_OUT_PORT_256_511
        Allout.out_ports_256_287 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_256_287 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_256_287;
        Allout.out_ports_288_319 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_288_319 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_288_319;
        Allout.out_ports_320_351 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_320_351 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_320_351;
        Allout.out_ports_352_383 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_352_383 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_352_383;

        Allout.out_ports_384_415 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_384_415 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_384_415;
        Allout.out_ports_416_447 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_416_447 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_416_447;
        Allout.out_ports_448_479 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_448_479 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_448_479;
        Allout.out_ports_480_511 = MEA_VP_MSTP_info_Table[vpn].user_regularPacket.out_ports_480_511 &   MEA_VP_MSTP_info_Table[vpn].user_l2cpPacket.out_ports_480_511;
#endif
            MEA_OS_memcpy(out_blk, &Allout, sizeof(MEA_OutPorts_Entry_dbt));
        return MEA_OK;
    }

    return MEA_OK;
}


/********************************************************/
/*                                                      */
/*             FWD_ENB_DA                               */
/*                                                      */
/********************************************************/


static void mea_drv_FWD_ENB_DA_prof_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t             unit_i = (MEA_Unit_t)arg1;
    //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
    //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
    MEA_MacAddr  *entry      = (MEA_MacAddr*)(arg4);




    MEA_Uint32                 val[2];
    MEA_Uint32                 i = 0;
    MEA_Uint32               num_of_regs;
    //MEA_Uint32                  count_shift;

    /* Zero  the val */
    for (i = 0; i < MEA_NUM_OF_ELEMENTS(val); i++) {
        val[i] = 0;
    }
    //count_shift=0;

    val[0] = 0;
    val[1] = 0;

    num_of_regs = 2;

    val[0] |= (((MEA_Uint32)((unsigned char)(entry->b[2]))) << 24);
    val[0] |= (((MEA_Uint32)((unsigned char)(entry->b[3]))) << 16);
    val[0] |= (((MEA_Uint32)((unsigned char)(entry->b[4]))) << 8);
    val[0] |= (((MEA_Uint32)((unsigned char)(entry->b[5]))) << 0);
   
    val[1] = (((MEA_Uint32)((unsigned char)(entry->b[0]))) << 8);
    val[1] |= (((MEA_Uint32)((unsigned char)(entry->b[1]))) << 0);


    /* Update Hw Entry */
    for (i = 0; i < num_of_regs; i++) {
        MEA_API_WriteReg(unit_i, MEA_IF_WRITE_DATA_REG(i), val[i], MEA_MODULE_IF);
    }



}





static MEA_Status mea_drv_FWD_ENB_DA_prof_UpdateHw(MEA_Unit_t             unit_i,
    MEA_Uint16           id_i,
    MEA_MacAddr      *new_entry_pi,
    MEA_MacAddr      *old_entry_pi)
{

    MEA_db_HwUnit_t       hwUnit;
    
    MEA_ind_write_t ind_write;



    MEA_DRV_GET_HW_UNIT(unit_i, hwUnit, return MEA_ERROR;)

        /* check if we have any changes */

        /* check if we have any changes */
        if ((old_entry_pi) &&
            (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi)) == 0)) {
            return MEA_OK;
        }


    



    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_ENB_PROF_MAC;
    ind_write.tableOffset   = id_i;
    ind_write.cmdReg        = MEA_IF_CMD_REG;
    ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg     = MEA_IF_STATUS_REG;
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_FWD_ENB_DA_prof_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
    //ind_write.funcParam3    = (MEA_Uint32)hwId;
    ind_write.funcParam4 = (MEA_funcParam)((long)new_entry_pi);

    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect failed "
            "(module=%d , tableType=%d , tableOffset=%d)\n",
            __FUNCTION__,
            MEA_MODULE_IF,
            ind_write.tableType,
            ind_write.tableOffset);
    }


    /* Return to caller */
    return MEA_OK;
}


MEA_Status mea_drv_FWD_ENB_DA_prof_Init(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size=0;

    if (b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_FWD_ENB_DA_SUPPORT){
            return MEA_OK;
        }


    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize ENB.DA..Profile \n");


    /* Allocate PacketGen Table */
	size = MEA_MAX_FWD_ENB_DA * sizeof(mea_drv_FWD_ENB_DA_dbt);
    if (size != 0) {
        MEA_FWD_ENB_DA_info_Table = (mea_drv_FWD_ENB_DA_dbt*)MEA_OS_malloc(size);
        if (MEA_FWD_ENB_DA_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_FWD_ENB_DA_info_Table failed (size=%d)\n",
                __FUNCTION__, size );
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_FWD_ENB_DA_info_Table[0]), 0, size);
    }
    /**/

    



    return MEA_OK;
}



MEA_Status mea_drv_FWD_ENB_DA_prof_RInit(MEA_Unit_t       unit_i)
{


    MEA_Uint16 id;

    if (b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_FWD_ENB_DA_SUPPORT){
            return MEA_OK;
        }

    for (id = 0; id < MEA_MAX_FWD_ENB_DA; id++) {
        if (MEA_FWD_ENB_DA_info_Table[id].valid){
            if (mea_drv_FWD_ENB_DA_prof_UpdateHw(unit_i, id, &MEA_FWD_ENB_DA_info_Table[id].FWD_DaMAC_value, NULL) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_AcMTU_prof_UpdateHw failed (id=%d)\n",
                    __FUNCTION__, id);
                return MEA_ERROR;
            }
        }
    }


    return MEA_OK;
}

MEA_Status mea_drv_FWD_ENB_DA_prof_Conclude(MEA_Unit_t       unit_i)
{
    if (b_bist_test){
        return MEA_OK;
    }

    MEA_API_LOG

        /* Check if support */
        if (!MEA_FWD_ENB_DA_SUPPORT){
            return MEA_OK;
        }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude ENB_DA_prof..\n");

    /* Free the table */
    if (MEA_FWD_ENB_DA_info_Table != NULL) {
        MEA_OS_free(MEA_FWD_ENB_DA_info_Table);
        MEA_FWD_ENB_DA_info_Table = NULL;
    }

    return MEA_OK;
}






MEA_Status mea_drv_Create_FWD_ENB_DA_Prof(MEA_Unit_t                    unit_i,
                                          MEA_Uint16                    *id_i,
                                          MEA_MacAddr                    DA)
{
    MEA_Bool  exist = MEA_FALSE;
    MEA_Uint16 idx;


    MEA_API_LOG

        /* Check if support */
        if (!MEA_FWD_ENB_DA_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_FWD_ENB_DA_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;
        }


    /*check */




    /* check parameter*/

    if (*id_i >= MEA_MAX_FWD_ENB_DA  )
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  ID %d  is out of range  \n",
            __FUNCTION__, *id_i);
        return MEA_ERROR;
    }
    /************************************************************************/
    /* FWD_ENB_DA                                                          */
    /************************************************************************/

    /*check if  we have this entry then add owner */
    for (idx = 0; idx < MEA_MAX_FWD_ENB_DA; idx++)
    {
        if (MEA_FWD_ENB_DA_info_Table[idx].valid == MEA_FALSE)
            continue;
        
        if (MEA_OS_memcmp(&DA, &MEA_FWD_ENB_DA_info_Table[idx].FWD_DaMAC_value, sizeof(MEA_MacAddr)) == 0){
            exist = MEA_TRUE;
            *id_i = idx;
            break;
        }

    }

    if (exist == MEA_TRUE){
        /* Add owner   */
        MEA_FWD_ENB_DA_info_Table[*id_i].num_of_owners++;
        *id_i = idx;
        return MEA_OK;
    }

    /*check if we have free entry */

    for (idx = 0; idx < MEA_MAX_FWD_ENB_DA; idx++)
    {
        if (MEA_FWD_ENB_DA_info_Table[idx].valid == MEA_FALSE){
          break;
        }
        continue;
    
    }

    if (idx >= MEA_MAX_FWD_ENB_DA){
        *id_i = 0;
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - no free entry \n",
            __FUNCTION__ );
        return MEA_ERROR;
    }

    if (mea_drv_FWD_ENB_DA_prof_UpdateHw(unit_i, idx, &DA, &MEA_FWD_ENB_DA_info_Table[*id_i].FWD_DaMAC_value) != MEA_OK)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_AcMTU_prof_UpdateHw fail  for id %d\n",
            __FUNCTION__, id_i);
        return MEA_OK;
    }

    *id_i = idx;
    MEA_FWD_ENB_DA_info_Table[*id_i].valid = MEA_TRUE;
    MEA_FWD_ENB_DA_info_Table[*id_i].num_of_owners = 1;

    MEA_OS_memcpy(&(MEA_FWD_ENB_DA_info_Table[*id_i].FWD_DaMAC_value), &DA, sizeof(MEA_MacAddr));

    return MEA_OK;
}


MEA_Status mea_drv_Delete_FWD_ENB_DA_Prof(MEA_Unit_t           unit_i, MEA_Uint16        id_i)
{


    MEA_API_LOG

        /* Check if support */
        if (!MEA_FWD_ENB_DA_SUPPORT){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_FWD_ENB_DA_SUPPORT not support \n",
                __FUNCTION__);
            return MEA_ERROR;
        }


    /*check */




    /* check parameter*/

    if (id_i >= MEA_MAX_FWD_ENB_DA)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  ID %d  is out of range  \n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }

    if (MEA_FWD_ENB_DA_info_Table[id_i].valid == MEA_FALSE){
        return MEA_ERROR;
    
    }

    if (MEA_FWD_ENB_DA_info_Table[id_i].valid == MEA_TRUE){
        
        if (MEA_FWD_ENB_DA_info_Table[id_i].num_of_owners > 1){ 
            MEA_FWD_ENB_DA_info_Table[id_i].num_of_owners--;
            return MEA_OK;
        }
        if (MEA_FWD_ENB_DA_info_Table[id_i].num_of_owners == 1){
            /********************************************/
            /*                                          */
            /********************************************/
            MEA_OS_memset(&MEA_FWD_ENB_DA_info_Table[id_i].FWD_DaMAC_value, 0, sizeof(MEA_FWD_ENB_DA_info_Table[0].FWD_DaMAC_value));
            if (mea_drv_FWD_ENB_DA_prof_UpdateHw(unit_i, id_i, &MEA_FWD_ENB_DA_info_Table[id_i].FWD_DaMAC_value,NULL) != MEA_OK)
            {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - mea_drv_AcMTU_prof_UpdateHw fail  for id %d\n",
                    __FUNCTION__, id_i);
            }
            MEA_OS_memset(&MEA_FWD_ENB_DA_info_Table[id_i], 0, sizeof(MEA_FWD_ENB_DA_info_Table[0]));
            MEA_FWD_ENB_DA_info_Table[id_i].valid = MEA_FALSE;
        }


    }

    return MEA_OK;
}


MEA_Status mea_drv_Get_FWD_ENB_DA_Prof(MEA_Unit_t                     unit_i,
    MEA_Uint16                      id_i,
    MEA_AC_Mtu_dbt                  *entry_po,
    MEA_Bool                        *exist)
{

    if (!MEA_FWD_ENB_DA_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_ACT_MTU_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry_po == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - entry_po is null\n", __FUNCTION__, id_i);
        return MEA_ERROR;

    }
    if (exist == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - exist is null\n", __FUNCTION__, id_i);
        return MEA_ERROR;

    }

    if (id_i >= MEA_MAX_FWD_ENB_DA){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - out of range for ACT_MTU profile \n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }


    if (MEA_AcMTU_info_Table[id_i].valid != MEA_TRUE){
        *exist = MEA_FALSE;
        return MEA_OK;
    }

    *exist = MEA_AcMTU_info_Table[id_i].valid;

    MEA_OS_memcpy(entry_po, &(MEA_AcMTU_info_Table[id_i].data),

        sizeof(*entry_po));



    return MEA_OK;

}










