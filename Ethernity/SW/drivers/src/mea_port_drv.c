/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*************************************************************************************/
/* 	Module Name:		 mea_port_drv.c											 	 */
/*																					 */
/*  Module Description:	 MEA port configuration driver                               */
/*																					 */
/*  Date of Creation:	 20/01/04													 */
/*																					 */
/*  Author Name:			 													         */
/*************************************************************************************/ 



/*-------------------------------- Includes ------------------------------------------*/



#include "mea_api.h"

#include "mea_swe_ip_drv.h"
#include "mea_drv_common.h"
#include "mea_globals_drv.h"
#include "mea_bm_drv.h"
#include "enet_queue_drv.h"
#include "mea_bm_ehp_drv.h"
#include "mea_port_drv.h" 




/*-------------------------------- Definitions ---------------------------------------*/
#define MEA_PORT_RATE_METERING_HW_ENABEL  MEA_TRUE
#define MEA_PORT_RATE_METERING_HW_DISABLE MEA_FALSE

typedef struct {
    MEA_Bool    enable;
    MEA_Uint16 internalClaster;
    MEA_Uint16 port;
    MEA_Uint16 interfaceId;
    MEA_Uint16 interface_index;
    MEA_Bool   maskBit_en;
    MEA_Uint8 mask;

}mea_drv_lag_private_dbt;

typedef struct {
    MEA_Bool   valid;
    MEA_LAG_dbt data;
    MEA_Uint32 num_of_owners;
    
    mea_drv_lag_private_dbt lag_privateData[MEA_LAG_MAX_NUM_OF_CLUSTERS];
    MEA_Uint8               clusterOngroup;
    
}MEA_LAG_Entry_dbt;


typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 Cluster_mask       :8; /* 0:7   */
    MEA_Uint32 cluster_type       :2; /* 9:8   */ /*   */
    MEA_Uint32 cluster_leader     :8; /*17:10  */ /*internal cluster id */
    MEA_Uint32 cluster_group_id   :4; /* 21:18 */
    MEA_Uint32 pad                :9;
#else
    MEA_Uint32 pad               :9;
    MEA_Uint32 cluster_group_id  :4; /* 21:18 */
    MEA_Uint32 cluster_leader    :8; /*17:10  */ /*internal cluster id */
    MEA_Uint32 cluster_type      :2; /* 9:8   */ /*   */
    MEA_Uint32 Cluster_mask      :8; /* 0:7   */
#endif
    
    }val;
    MEA_Uint32 reg[1];

}MEA_LAG_Group_Reg1_dbt;


typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 Cluster_mask       :8; /* 0:7   */
    MEA_Uint32 cluster_type       :2; /* 9:8   */ /*   */
     MEA_Uint32 cluster_group_id  :2; /* 11:10 */
    MEA_Uint32 cluster_leader     :6; /*17:12  */ /*internal cluster id */
    MEA_Uint32 pad                :14;
#else
    MEA_Uint32 pad               :14;
    MEA_Uint32 cluster_leader    :6; /*17:12  */ /*internal cluster id */
    MEA_Uint32 cluster_group_id  :2; /* 11:10 */
    MEA_Uint32 cluster_type      :2; /* 9:8   */ /*   */
    MEA_Uint32 Cluster_mask      :8; /* 0:7   */
#endif
    
    }val;
    MEA_Uint32 reg[1];

}MEA_LAG_Group_Reg0_dbt;



/*BFD SRC port to Cluster */


typedef struct {
    MEA_Bool   valid;

    MEA_Uint32 num_of_owners;

    MEA_BFD_src_dbt data;

    

}MEA_BFD_Src_port_Entry_dbt;

typedef struct {
    MEA_Bool   valid;

    
    MEA_Bool remove_nvgre_header;
    MEA_Uint32 bitMap;

}MEA_NVGRE_port_Entry_dbt;



static MEA_BFD_Src_port_Entry_dbt *MEA_BFD_SRC_PORT_info_Table = NULL;



 static MEA_LAG_Entry_dbt           *MEA_LAG_info_Table=NULL;



 
 static  MEA_NVGRE_port_Entry_dbt MEA_NVGRE_PORT_info_Table[MEA_MAX_PORT_NUMBER + 1];
 static  MEA_Uint32               MEA_GlobalReg_Nvgre = 0;

/*-------------------------------- External Functions --------------------------------*/


/*-------------------------------- External Variables --------------------------------*/
#if 1 
 
 
#else
 extern MEA_drv_port2Interface_dbt MEA_Ports2InterfaceInfo[MEA_MAX_PORT_NUMBER+1];
 extern MEA_drv_Interface2Port_dbt MEA_Interface2PortsInfo[MEA_MAX_PORT_NUMBER+1];
#endif


/*-------------------------------- Forward Declarations ------------------------------*/










/*-------------------------------- Local Variables -----------------------------------*/

 MEA_map_Interface_dbt MEA_Interface_Mapping[MEA_MAX_PORT_NUMBER+1];


 MEA_PortsMapping_t *MEA_PortsMapping = NULL;    ///[MEA_MAX_PORT_NUMBER+1];


MEA_Bool MEA_PMC_Piggy_exist = MEA_FALSE;

static MEA_Uint32 mea_speed_rate_100_10_reg=0;


/*-------------------------------- Global Variables ----------------------------------*/

MEA_Port_t *MEA_port_map_array = NULL; //EGRESS
MEA_Port_t *MEA_port_map_arrayINGRESS = NULL;


MEA_Bool GdebugAllPortValid=MEA_FALSE;

#define MEA_PORT_NOT_VALID  (MEA_MAX_PORT_NUMBER_EGRESS+1)
#define MEA_PORT_INGRESS_NOT_VALID (MEA_MAX_PORT_NUMBER_INGRESS+1)




/**************************************************************************************/
/* Function Name:	MEA_PortMapInit											  		  */					  
/* 																					  */
/* Description:		This will initialize the port mapping table - to be invoked								  								  */					  
/*					only after HRR initialization																  */
/* Arguments:	   	None.												  			  */
/*																					  */
/* Return:		   	None.														  */
/*																					  */
/**************************************************************************************/

MEA_Status mea_drv_Init_PortMap(MEA_Unit_t unit)
{
   MEA_Port_t          port;
   MEA_PortsMapping_t  portMap;
   
   
   if(b_bist_test){
    return MEA_OK;
   }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Port Map ...\n");
	
	for (port=0;port<= MEA_MAX_PORT_NUMBER_EGRESS;port++) {
	   if (port == 128) {
		   port = 128;
	   }
	   
	   if (MEA_Low_Get_Port_Mapping_By_key(unit,
		                                   MEA_PORTS_TYPE_EGRESS_TYPE,
										   (MEA_Uint16)port,
										   &portMap)!=MEA_OK){
		     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n",port);
	   }
      
   

	   if (portMap.EgressValid) {
		   /* this port number is not used as physical or logical */
		   MEA_port_map_array[port] = portMap.Egressport; //(MEA_Port_t)port;
		   continue;
	   }

	   MEA_port_map_array[port] = MEA_PORT_NOT_VALID; //(MEA_Port_t)port;
	   }
	  

   
   
	for (port = 0; port <= MEA_MAX_PORT_NUMBER_INGRESS; port++) 
	{

		if (port == 24) {
			port = 24;
		}

		if (port == 128) {
			port = 128;
		}

		portMap.valid = MEA_FALSE;
			/* check if ingress port exist */
			if (MEA_Low_Get_Port_Mapping_By_key(unit,
				MEA_PORTS_TYPE_INGRESS_TYPE,
				(MEA_Uint16)port,
				&portMap) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n", port);
			}

			if (portMap.valid) {
				/* this port number is not used as physical or logical */
				MEA_port_map_arrayINGRESS[port] = (MEA_Port_t)port; 
				continue;
			}
		
			MEA_port_map_arrayINGRESS[port] = MEA_PORT_INGRESS_NOT_VALID;

	}
	
	return MEA_OK;
}





static MEA_Status mea_dev_get_port_Policer_Mapping(MEA_Unit_t unit, MEA_Port_t port ,MEA_Uint32 *index_o){

    
    if(index_o == NULL){
        return MEA_ERROR;
    }

    *index_o = port;

    return MEA_OK;
}

MEA_Status MEA_Low_Get_Port_Mapping_By_key(MEA_Unit_t unit,MEA_PortKeyType_t KeyType, MEA_Uint16 id,MEA_PortsMapping_t *portMap){
     
    MEA_Uint32 i;
    

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
    if(portMap == NULL){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - portMap == NULL\n",__FUNCTION__);
       return MEA_ERROR;
    } 
#endif

    MEA_OS_memset(portMap,0,sizeof(MEA_PortsMapping_t));
    

    switch (KeyType){
        case MEA_PORTS_TYPE_INGRESS_TYPE:
			if (id > MEA_MAX_PORT_NUMBER_INGRESS) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - id (%d) is out off range (0..%d)\n", __FUNCTION__, id, MEA_MAX_PORT_NUMBER_INGRESS);
				return MEA_ERROR;
			}
            for (i=0;i<=MEA_MAX_PORT_NUMBER_INGRESS;i++){
                if ((MEA_PortsMapping[i].valid == MEA_TRUE) && 
					(MEA_PortsMapping[i].ingressValid == MEA_TRUE) &&
					(MEA_PortsMapping[i].Ingressport == id)){
                   MEA_OS_memcpy(portMap, &MEA_PortsMapping[i], sizeof(MEA_PortsMapping_t));
                   //portMap->physicalport=(MEA_Uint8)i;
                   return MEA_OK;
                }
            }

            //MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - not found port %d with key MEA_PORTS_TYPE_INGRESS_TYPE\n",__FUNCTION__,port);
             return MEA_OK;
            break;
        case MEA_PORTS_TYPE_EGRESS_TYPE:
			if (id > MEA_MAX_PORT_NUMBER_EGRESS) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - MEA_PORTS_TYPE_EGRESS_TYPEid (%d) is out off range (0..%d)\n", __FUNCTION__, id, MEA_MAX_PORT_NUMBER_EGRESS);
				return MEA_ERROR;
			}
             for (i=0;i<= MEA_MAX_PORT_NUMBER_EGRESS;i++){
                 if (MEA_PortsMapping[i].valid == MEA_TRUE && 
					 (MEA_PortsMapping[i].EgressValid )&&
					 (MEA_PortsMapping[i].Egressport == id)){
                   MEA_OS_memcpy(portMap, &MEA_PortsMapping[i], sizeof(MEA_PortsMapping_t));
                   //portMap->physicalport=(MEA_Uint8)i;
                   return MEA_OK;
                }
            }  
            //MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - not found port %d with key MEA_PORTS_TYPE_EGRESS_TYPE\n",__FUNCTION__,port);
             return MEA_OK;
             break;
        case MEA_PORTS_TYPE_CLUSTER_TYPE:
			if (id > MEA_MAX_PORT_NUMBER_EGRESS) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -MEA_PORTS_TYPE_CLUSTER_TYPE id (%d) is out off range (0..%d)\n", __FUNCTION__, id, MEA_MAX_PORT_NUMBER_EGRESS);
				return MEA_ERROR;
			}
             for (i=0;i<=MEA_MAX_PORT_NUMBER_EGRESS;i++){
                 if (MEA_PortsMapping[i].valid == MEA_TRUE && 
					 MEA_PortsMapping[i].DefaltClaster == id){
                   MEA_OS_memcpy(portMap, &MEA_PortsMapping[i], sizeof(MEA_PortsMapping_t));
                   //portMap->physicalport=(MEA_Uint8)i;
                   return MEA_OK;
                }
            }
             //MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - not found port %d with key MEA_PORTS_TYPE_CLUSTER_TYPE\n",__FUNCTION__,port);
             return MEA_OK;
            break ;
        case MEA_PORTS_TYPE_PHYISCAL_TYPE:
			if (id > MEA_MAX_PORT_NUMBER) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -MEA_PORTS_TYPE_PHYISCAL_TYPE id (%d) is out off range (0..%d)\n", __FUNCTION__, id, MEA_MAX_PORT_NUMBER);
				return MEA_ERROR;
			}
            for (i=0;i<=MEA_MAX_PORT_NUMBER;i++){
                if (MEA_PortsMapping[i].valid == MEA_TRUE && MEA_PortsMapping[i].physicalport == id){
                   MEA_OS_memcpy(portMap, &MEA_PortsMapping[i], sizeof(MEA_PortsMapping_t));
                   //portMap->physicalport=(MEA_Uint8)i;
                   return MEA_OK;
                }
            }   

               return MEA_OK;

               break;
        case MEA_PORTS_TYPE_TO_INTERFACE_TYPE:
			if (id > MEA_MAX_PORT_NUMBER) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -MEA_PORTS_TYPE_TO_INTERFACE_TYPE id (%d) is out off range (0..%d)\n", __FUNCTION__, id, MEA_MAX_PORT_NUMBER);
				return MEA_ERROR;
			}
            if(id > MEA_MAX_PORT_NUMBER){
                   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - ID is out of range\n",__FUNCTION__);
                    return MEA_ERROR;
            }
               MEA_OS_memcpy(portMap, &MEA_PortsMapping[id], sizeof(MEA_PortsMapping_t));
             return MEA_OK;
            break;
        case MEA_PORTS_TYPE_VIRTUAL_INGRESS_TYPE:
            if (id > MEA_MAX_PORT_NUMBER_INGRESS) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - id (%d) is out off range (..%d)\n", __FUNCTION__, id, MEA_MAX_PORT_NUMBER_INGRESS);
                return MEA_ERROR;
            }
            for (i = MEA_PORT_VIRUAL_INGRESS_START; i <= MEA_MAX_PORT_NUMBER_INGRESS; i++) {
                
                if ((MEA_PortsMapping[i].valid == MEA_TRUE) &&
                    (MEA_PortsMapping[i].virualValid == MEA_TRUE) &&
                    (MEA_PortsMapping[i].ingressValid == MEA_TRUE) &&
                    (MEA_PortsMapping[i].Ingressport == id)) {
                    MEA_OS_memcpy(portMap, &MEA_PortsMapping[i], sizeof(MEA_PortsMapping_t));
                    //portMap->physicalport=(MEA_Uint8)i;
                    return MEA_OK;
                }
            }

            break;
        case MEA_PORTS_TYPE_LAST_TYPE:
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - not Support with key MEA_PORTS_TYPE_LAST_TYPE\n",__FUNCTION__);
             return MEA_ERROR;
            break;

        
    }
    
    
    
    
    return MEA_OK;
}
void set_AllPortValid(MEA_Bool val)
{
	GdebugAllPortValid = val;
}

MEA_Bool MEA_API_Get_Is_VirtualPort_Valid(MEA_Port_t port,MEA_Bool silent)
{
    MEA_API_LOG
        // need to check how many virtual we support
        if(MEA_STANDART_BONDING_SUPPORT){
            if((port >= (MEA_Uint32)MEA_QUEUE_VIRTUAL_PORT_START_SRC_PORT)  && ( port < (MEA_Uint32)(MEA_STANDART_BONDING_NUM_OF_GROUP_TX+ MEA_QUEUE_VIRTUAL_PORT_START_SRC_PORT))){
              return MEA_TRUE;
            }else{
                return MEA_FALSE;
            }

        }else{
            return MEA_FALSE;
        }

    return MEA_TRUE;
}



MEA_Bool  MEA_API_Get_IsPortValid(MEA_Port_t port, MEA_PortKeyType_t type, MEA_Bool silent)
{
	MEA_PortsMapping_t  portMap;

	MEA_API_LOG

		if (GdebugAllPortValid == MEA_TRUE)
			return(MEA_TRUE);

	switch (type)
	{
		case MEA_PORTS_TYPE_EGRESS_TYPE:
			if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
				MEA_PORTS_TYPE_EGRESS_TYPE,
				(MEA_Uint16)port,
				&portMap) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_Low_Get_Port_Mapping_By_key failed on Port %d EGRESS_TYPE\n", port);
			}
			if (!portMap.valid) {
				/* this port number is not used as physical or logical */
				if (!silent) {
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Port %d is invalid\n", port);
				}
				return MEA_FALSE;
			}
		break;
	
		case MEA_PORTS_TYPE_INGRESS_TYPE:
		   /* check if ingress port exist */
		   if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
			   MEA_PORTS_TYPE_INGRESS_TYPE,
			   (MEA_Uint16)port,
			   &portMap) != MEA_OK) {
			   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_Low_Get_Port_Mapping_By_key failed on Port %d INGRESS_TYPE\n", port);
		   }
		   if (!portMap.valid) {
			   /* this port number is not used as physical or logical */
			   if (!silent) {
				   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Port %d is invalid\n", port);
			   }
			   return MEA_FALSE;
		   }
		   break;
		case MEA_PORTS_TYPE_VIRTUAL_INGRESS_TYPE:
			/* check if ingress port exist */
			if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
				MEA_PORTS_TYPE_VIRTUAL_INGRESS_TYPE,
				(MEA_Uint16)port,
				&portMap) != MEA_OK) {
				MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "MEA_Low_Get_Port_Mapping_By_key failed on Port %d VIRTUAL_INGRESS_TYPE\n", port);
			}
			if (!portMap.valid) {
				/* this port number is not used as physical or logical */
				if (!silent) {
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Port %d is invalid\n", port);
				}
				return MEA_FALSE;
			}
			break;
		default:
			
				/* this port number is not used as physical or logical */
				if (!silent) {
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "Port %d is invalid\n", port);
				}
				return MEA_FALSE;
			
			break;
	}
    return MEA_TRUE;
}
MEA_Port_t mea_get_port_mapArray(MEA_Port_t port) {
	return	MEA_port_map_array[port];
}

MEA_Port_t mea_get_port_mapArrayINGRESS(MEA_Port_t port) {
	return	MEA_port_map_arrayINGRESS[port];
}
MEA_Status mea_Set_Port_Speed_Rate(MEA_Port_t port ,MEA_Uint32 speed)
{
    MEA_Uint32 val=0;
    MEA_Bool speed_set=0;  
    MEA_Uint32 phy_Bit;  

    switch (port) 
    {
        case 127 :
            phy_Bit= 0x40;
            break;
        case 126 :
            phy_Bit= 0x20;
            break;
        case 125 :
            phy_Bit= 0x10;
            break;
        case 72 :
            phy_Bit= 0x08;
            break;
        case 48 :
            phy_Bit= 0x04;
            break;
        case 24 :
            phy_Bit= 0x02;
            break;
        case 0 :
            phy_Bit= 0x01;
            break;
        
        default:
       		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Error Port number is part of the phy only 127 126 125 126 72 48 0\n");
            return MEA_ERROR;
            break;
    }
    
    switch (speed) {
            case 100:
            speed_set=1;
            break;
        case 10:
            speed_set=0;
            break;
        default:
     		  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Error speed is wrong \n");
            return MEA_ERROR;
            break;
    }  
    if (speed_set !=0 ){
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "speed is 100 \n");
    }

   MEA_IF_INIT_HW_ACCESS_ENABLE
     val=mea_speed_rate_100_10_reg;
   // val = MEA_API_ReadReg(MEA_UNIT_0,MEA_IF_PHY_SPEED_RATE_100_10_REG,MEA_MODULE_IF);
    
    if(speed == 10) {
        val = val  & (~phy_Bit);
    } else {
        val = val  | (phy_Bit);
    }

    MEA_API_WriteReg(MEA_UNIT_0,
                         ENET_IF_PHY_SPEED_RATE_100_10_REG ,
                         val, 
                         MEA_MODULE_IF);
    
    mea_speed_rate_100_10_reg=val;

   MEA_IF_INIT_HW_ACCESS_DISABLE
    return MEA_OK;
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Port_RateMeter_UpdateHwEntry>                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status  mea_drv_Port_RateMeter_UpdateHwEntry(MEA_funcParam arg1, MEA_funcParam arg2, MEA_funcParam arg3, MEA_funcParam arg4)
    
    
    
{
    MEA_IngressPort_PolicerType_te policerType = (MEA_IngressPort_PolicerType_te)((long)arg1);
    MEA_Policer_Entry_dbt* entry_i              = (MEA_Policer_Entry_dbt*)arg2;
    MEA_Bool               rate_enable          = (MEA_Bool)((long)arg3);
    MEA_Uint32             tableType            = (MEA_Uint32)((long)arg4);
   // MEA_Bool SET_MTU = MEA_FALSE;
    MEA_EbsCbs_t   myCBS=0;
    char Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_LAST + 1][20];
    
    
    
    
    MEA_Uint32 val,val1,val2,val3,temp;
	MEA_uint64 norm_cir;
	MEA_Uint32 tick;
    MEA_PolicerTicks_Entry_dbt PolicerTicks_Entry;
    MEA_Uint32 flag_fast_service;
    MEA_uint64 round_cir;
    MEA_Uint32 maxVal, mtu_size;
    
    //MEA_uint64 sysclck=100;
     

#if MEA_PORT_RATE_METER_2_BUCKET    
    MEA_Uint32 round_eir;
    MEA_Uint32 norm_eir;
#endif

#if MEA_PORT_POLICER_DISABLE_HW
    return MEA_OK;
#endif    
    if( entry_i == NULL){
        return MEA_ERROR; 
    }
    
    
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_MC][0]),      "MC     ");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_BC][0]),      "BC     ");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST][0]), "UNICAST");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN][0]),  "UNKNOWN");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_TOTAL][0]),   "TOTAL");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_LAST][0]),    "LAST");
 
        

    
    /* get the policer ticks entry */
    if (MEA_API_Get_PolicerTicks_Entry(MEA_UNIT_0,&PolicerTicks_Entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_GEt_PolicerTicks_Entry failed\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
  
    /* get the relevant tick value according to the service type (normal/fast) */
#if 0
    if(port>MEA_POLICER_LAST_FAST_SERVICE) {
		tick = PolicerTicks_Entry.ticks_for_slow_service;
		flag_fast_service=MEA_FALSE;
	} else {
		tick = PolicerTicks_Entry.ticks_for_fast_service;
		flag_fast_service=MEA_TRUE;
    }
#else
     /*for now we work only slow*/ 
     tick = PolicerTicks_Entry.ticks_for_slow ;/*need to set the tick for a port*/
	 flag_fast_service=MEA_FALSE;
     if (tick == 0){
//         MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "tick = %d \n");
     }

#endif

#if 1
     /* calculate normalize cir and eir */
    if((entry_i->CIR) != 0){
        if (flag_fast_service == MEA_TRUE){
            round_cir=(entry_i->CIR) % MEA_PORT_RATE_METER_CIR_EIR_FAST_GN;
            norm_cir=((entry_i->CIR)/MEA_PORT_RATE_METER_CIR_EIR_FAST_GN);
            if (  (round_cir >= MEA_PORT_RATE_METER_CIR_EIR_FAST_GN/2) ) { 
                round_cir=1;
            } else {
                round_cir=0;
            }         
            if (round_cir +  norm_cir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK) 
                norm_cir = round_cir + norm_cir;
            else
                norm_cir= MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK;
           
         } else {
                round_cir=(entry_i->CIR) % MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN;
                norm_cir=(entry_i->CIR)/MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN;
                if (  (round_cir  >= MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN/2) ) { 
                  round_cir=1;
               } else {
                  round_cir=0;
               }
               if (round_cir +  norm_cir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK) 
                    norm_cir = round_cir + norm_cir ;
               else
                   norm_cir= MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK;

       }
    }else{
        norm_cir=0;
    } 
#else
     if((entry_i->CIR) != 0){
         temp=(sysclck * 8*1000000);
        round_cir=  ((entry_i->CIR) * tick ) % temp ;
        norm_cir = ((entry_i->CIR) * tick ) / temp;
       round_cir=1;/**/

       if(round_cir +  norm_cir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK )
         norm_cir += round_cir;

     }else{
         norm_cir=0;
     }



#endif


/**/

#if MEA_PORT_RATE_METER_2_BUCKET 
     /* calculate normalize cir and eir */
    if((MEA_Uint32)(entry_i->EIR) != 0){
        if (flag_fast_service == MEA_TRUE){
            round_eir=(MEA_Uint32)(entry_i->CIR) % MEA_PORT_RATE_METER_CIR_EIR_FAST_GN;
            norm_eir=((MEA_Uint32)(entry_i->CIR)/MEA_PORT_RATE_METER_CIR_EIR_FAST_GN);
            if (  (round_eir >= MEA_PORT_RATE_METER_CIR_EIR_FAST_GN/2) ) { 
                round_eir=1;
            } else {
                round_eir=0;
            }         
            if (round_eir +  norm_eir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK) 
                norm_eir += round_eir  ;
           
         } else {
                round_eir=(MEA_Uint32)(entry_i->EIR) % MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN;
                norm_eir=(MEA_Uint32)(entry_i->EIR)/MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN;
                if (  (round_eir >= MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN/2) ) { 
                  round_eir=1;
               } else {
                  round_eir=0;
               }
               if (round_eir +  norm_eir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK) 
                    norm_eir += round_eir  ;
       }
    }else{
        norm_eir=0;
    }
#endif
    
    if((entry_i->CIR) != 0 && (entry_i->CBS !=0))
    {
        if (entry_i->maxMtu == 0){
          //  SET_MTU = MEA_FALSE;
            if (entry_i->CBS < 32000){
                myCBS = 32000;
            }
            else{
                myCBS = entry_i->CBS;
            }

            mtu_size = MEA_Platform_GetMaxMTU();
        }
        else {
         //   SET_MTU = MEA_TRUE;
            mtu_size = entry_i->maxMtu;
            myCBS = entry_i->CBS;
        }

        maxVal = MEA_OS_MAX(((MEA_Uint32)(norm_cir << (entry_i->gn_type)) + mtu_size), (mtu_size * 2));

        //norm_cbs = norm_cbs;
        if (maxVal > myCBS)
        {
            if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_PROF) 
                MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " ****** ThePortPolicer %s CBS is smaller than the recommended %d ****** \n", 
                Str_policerType[policerType],maxVal);
            //return MEA_ERROR;
        }
    }
    
    /* Build register 0 */
    val   = 0;

    temp  = (MEA_Uint32)norm_cir;
    temp &= (MEA_PORT_RATE_METER_PROF_CIR_MASK  >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CIR_MASK )); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CIR_MASK );
    val  |= temp;
        
    temp = myCBS; //(MEA_Uint32)entry_i->CBS;
    temp &= (MEA_PORT_RATE_METER_PROF_CBS_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CBS_MASK)); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CBS_MASK);
    val  |= temp;
#if MEA_PORT_RATE_METER_2_BUCKET          
  
	temp  = (MEA_Uint32)norm_eir;
    temp &= (MEA_PORT_RATE_METER_PROF_EIR0_3_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_EIR0_3_MASK)); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_EIR0_3_MASK);
    val  |= temp;
#endif

    
    
    if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_PROF) {
	    MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,val,MEA_MODULE_BM);
    }

    /* Build register 1 */        
    
    val1 = 0;
#if MEA_PORT_RATE_METER_2_BUCKET 
	temp  = (MEA_Uint32)norm_eir;
    temp >>= (32-MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_EIR0_3_MASK)); 
    temp &= (MEA_SRV_RATE_METER_PROF_EIR4_11_MASK >> MEA_OS_calc_shift_from_mask(MEA_POLICER_EIR_MASK2)); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_SRV_RATE_METER_PROF_EIR4_11_MASK);
    val1  |= temp;  
	
	temp  = (MEA_Uint32)entry_i->EBS;
    temp &= (MEA_SRV_RATE_METER_PROF_EBS_MASK >> MEA_OS_calc_shift_from_mask(MEA_SRV_RATE_METER_PROF_EBS_MASK)); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_SRV_RATE_METER_PROF_EBS_MASK);
    val1  |= temp;

#endif
    temp  = (MEA_Uint32)entry_i->comp;
    temp &= (MEA_PORT_RATE_METER_PROF_COMP_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_COMP_MASK)); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_COMP_MASK);
    val1  |= temp;

    temp  = (MEA_Uint32)entry_i->cup;
    temp &= (MEA_PORT_RATE_METER_PROF_CF_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CF_MASK)); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CF_MASK);
    val1  |= temp;
    
    temp  = (MEA_Uint32)entry_i->color_aware;
    temp &= (MEA_PORT_RATE_METER_PROF_CA_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CA_MASK)); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CA_MASK);
    val1  |= temp;
    
    
    if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_PROF) {
	    MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA1,val1,MEA_MODULE_BM);
    }
       
    
    val2   = 0;

    temp = myCBS; //(MEA_Uint32)entry_i->CBS;
    temp &= (MEA_PORT_RATE_METER_CONTEX_XCIR  >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_XCIR )); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_XCIR );
    val2  |= temp;
    
#if MEA_PORT_RATE_METER_2_BUCKET

    temp  = (MEA_Uint32)entry_i->EBS;
    temp &= (MEA_PORT_RATE_METER_CONTEX_XEIR_MASK  >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_XEIR_MASK )); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_XEIR_MASK );
    val2  |= temp;
#endif    
   if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_BUCKET) {
	   
       MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA0,val2,MEA_MODULE_BM);
    }
    
    
    val3=0;
    temp  = ((MEA_Uint32)rate_enable == MEA_PORT_RATE_MEETRING_DISABLE_DEF_VAL) ? MEA_PORT_RATE_METERING_HW_DISABLE : MEA_PORT_RATE_METERING_HW_ENABEL;
    temp &= (MEA_PORT_RATE_METER_CONTEX_ENABL  >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_ENABL )); 
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_ENABL );
    val3  |= temp;
    
    
    if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_BUCKET) {
	   
        MEA_API_WriteReg(MEA_UNIT_0,MEA_BM_IA_WRDATA1,val3,MEA_MODULE_BM);
    }

  


    return MEA_OK;
}






/**************************************************************************************/
/* Function Name: mea_drv_Port_RateMeter_UpdateHw*/					  
/* */
/* Description:	*/					  
/**/
/**/
/**/
/* Return:		   	MEA_OK/MEA_ERROR.*/
/*																					  */
/**************************************************************************************/
static MEA_Status mea_drv_Port_RateMeter_UpdateHw(MEA_Uint32 offset, MEA_Policer_Entry_dbt* entry_i, MEA_Bool rate_Enable, MEA_IngressPort_PolicerType_te type)
{

	MEA_ind_write_t ind_write;     
#if MEA_PORT_POLICER_DISABLE_HW
     return MEA_OK;
#endif
    
    ind_write.tableType		= ENET_BM_TBL_TYP_INGRESS_RATE_METERING_PROF;
	ind_write.tableOffset   = offset;
	ind_write.cmdReg		= MEA_BM_IA_CMD;      
	ind_write.cmdMask		= MEA_BM_IA_CMD_MASK;      
	ind_write.statusReg		= MEA_BM_IA_STAT;   
	ind_write.statusMask	= MEA_BM_IA_STAT_MASK;   
	ind_write.tableAddrReg	= MEA_BM_IA_ADDR;
	ind_write.tableAddrMask	= MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_write.writeEntry     =  (MEA_FUNCPTR)mea_drv_Port_RateMeter_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)((long)type);
    ind_write.funcParam2 = (MEA_funcParam)entry_i;
    ind_write.funcParam3 = (MEA_funcParam)((long)rate_Enable);
    ind_write.funcParam4 = (MEA_funcParam)((long)ind_write.tableType);


	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect failed "
                          "TableType=%d , TableOffset=%d , mea_module=%d\n",
                          __FUNCTION__,
                          ind_write.tableType,
                          ind_write.tableOffset,
                          MEA_MODULE_BM);
 	    return MEA_ERROR;
    }                

	ind_write.tableType		= ENET_BM_TBL_TYP_INGRESS_RATE_METERING_BUCKET;        
    ind_write.funcParam4 = (MEA_funcParam)((long)ind_write.tableType);

	if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect failed "
                          "TableType=%d , TableOffset=%d , mea_module=%d\n",
                          __FUNCTION__,
                          ind_write.tableType,
                          ind_write.tableOffset,
                          MEA_MODULE_BM);
 	    return MEA_ERROR;
    }                


     return MEA_OK;


}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_check_Ingress_Policer_prof  >                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_check_Ingress_Policer_prof(MEA_Policer_Entry_dbt *entry_i){
    
    MEA_uint64 slow_max;
    MEA_Bool request_for_fast_tmId;
	MEA_Policer_Entry_dbt *Policer_Entry;
    MEA_uint64 min;
    MEA_uint64 max;
    

    if(entry_i == NULL){
        return MEA_ERROR;
    }
    
    Policer_Entry=entry_i;
    
    
    if (Policer_Entry->CIR > 1000000001 || Policer_Entry->EIR > 1000000001){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - currently CIR/EIR max value  1G   \n",
                             __FUNCTION__ );
     return MEA_ERROR;
    }

    if (entry_i->gn_type != 0) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - Port Policer not support gn_type (%d) != 0 \n",
                             __FUNCTION__ ,entry_i->gn_type);
     return MEA_ERROR;
    }
    if (entry_i->gn_sign != 0) {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - Port Policer not support gn_sign (%d) != 0 \n",
                             __FUNCTION__ ,entry_i->gn_sign);
     return MEA_ERROR;
    }
    
    slow_max=(MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_MAX_VAL(entry_i->gn_sign,entry_i->gn_type);
       
    if ((entry_i->CIR   > slow_max) ||
        (entry_i->EIR   > slow_max)  ) {
         request_for_fast_tmId = MEA_TRUE;
         min = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_FAST_MIN_VAL(entry_i->gn_sign,entry_i->gn_type);
         max = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_FAST_MAX_VAL(entry_i->gn_sign,entry_i->gn_type);
    } else {
         request_for_fast_tmId = MEA_FALSE;
         min = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_MIN_VAL(entry_i->gn_sign,entry_i->gn_type);
         max = (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_MAX_VAL(entry_i->gn_sign,entry_i->gn_type);
    }
    if (Policer_Entry->CIR == 0 && Policer_Entry->CBS !=0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - CBS (%d) need to be Zero when Cir is Zero \n",
                             __FUNCTION__,Policer_Entry->CBS);
           
       return MEA_ERROR;
     }
     if (Policer_Entry->CIR != 0 && Policer_Entry->CBS == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - CIR (%d) need to be Zero when CBS is Zero \n",
                             __FUNCTION__,Policer_Entry->CIR);   
           
           return MEA_ERROR;
    }
     if(request_for_fast_tmId == MEA_FALSE){
       //  MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"request_for_fast_tmId == MEA_FALSE \n"); 
     }

#if MEA_PORT_RATE_METER_2_BUCKET    
     if (Policer_Entry->EIR == 0 && Policer_Entry->EBS !=0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - EBS (%d) need to be Zero when EIR is Zero \n",
                             __FUNCTION__,Policer_Entry->EBS);
           
           return MEA_ERROR;
    }
    if (Policer_Entry->EIR != 0 && Policer_Entry->EBS == 0) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - EIR (%d) need to be Zero when EBS is Zero \n",
                             __FUNCTION__,Policer_Entry->EIR);
           
           return MEA_ERROR;
    }
    if (Policer_Entry->EIR != 0 && Policer_Entry->EIR < min ) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - EIR value %ld (0x%08x) < %ld min value (0x%08x) \n",
                             __FUNCTION__,Policer_Entry->EIR,Policer_Entry->EIR,min,min);
           
           return MEA_ERROR;
    }
    if (Policer_Entry->EIR >  max    ) {
	    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - EIR value %ld (0x%08x) > max value %ld (0x%08x) \n",
                             __FUNCTION__,
                             Policer_Entry->EIR,Policer_Entry->EIR,
                             max,max);
           
           return MEA_ERROR;
    }
#else
    if(Policer_Entry->EIR != 0 || Policer_Entry->EBS !=0){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - \nEIR and EBS mast be zero only one bucket support\n",__FUNCTION__);
        return MEA_ERROR;
    }
#endif

    if (Policer_Entry->CIR != 0 &&  Policer_Entry->CIR < min   ) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s -ERROR3 CIR value %ld  < min value %ld  \n",
                             __FUNCTION__,Policer_Entry->CIR,min);
           
           return MEA_ERROR;
    }
    
    if (Policer_Entry->CIR >  max    ) {
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s -ERROR4 CIR value %ld (0x%08x) > max value %ld (0x%08x) \n",
                             __FUNCTION__,
                             Policer_Entry->CIR,Policer_Entry->CIR,max,max);
           
           return MEA_ERROR;
    }
    if (Policer_Entry->maxMtu != 0){
        if (Policer_Entry->maxMtu < 64){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s  maxMtu %d  and the minimum 64\n",
                __FUNCTION__,
                Policer_Entry->maxMtu);

            return MEA_ERROR;

        }
    }
  
    
	return MEA_OK;
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_check_Ingress_Policer_prof  >                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  mea_drv_set_port_ingress_policer(MEA_Unit_t unit,
                                             MEA_Port_t port, 
											 MEA_IngressPort_Entry_dbt *entry,
											 MEA_IngressPort_Entry_dbt *old_entry){

    MEA_Uint32 i;
    MEA_Uint32 index,offset;
    MEA_Policer_Entry_dbt polcer_entry;
    
    if(mea_dev_get_port_Policer_Mapping(unit,port ,&index) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_dev_get_port_index failed ",__FUNCTION__);
        return MEA_ERROR;
    } 
    
    
    for (i=0; i<MEA_INGRESS_PORT_POLICER_TYPE_LAST ;i++) {
        
		/* Ignore TOTAL Policer for the HW */
		if (i==MEA_INGRESS_PORT_POLICER_TYPE_TOTAL) {
			continue;
		}

		/* Verify for no change */
		if ((MEA_InitDone) &&
            (old_entry != NULL) &&
		    (MEA_OS_memcmp(&(entry->policer_vec[i]),
			               &(old_entry->policer_vec[i]),
			  			   sizeof(entry->policer_vec[i])) == 0)) {
		    continue;
		}

        /* need to calculate*/
        offset = index*4 + i;

        MEA_OS_memcpy(&polcer_entry,&entry->policer_vec[i].sla_params, sizeof(MEA_Policer_Entry_dbt));
        entry->policer_vec[i].tmId=offset;
        if (mea_drv_check_Ingress_Policer_prof(&polcer_entry) !=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_check_Ingress_Policer_prof failed ",__FUNCTION__);
            return MEA_ERROR;
        }
        if (mea_drv_Port_RateMeter_UpdateHw(offset ,&polcer_entry ,entry->policer_vec[i].tmId_enable,i)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Port_RateMeter_UpdateHw failed ",__FUNCTION__);
            return MEA_ERROR;
        }
   
   }


    return MEA_OK;
}

MEA_Bool mea_is_portUtopia(MEA_Port_t port){
	MEA_Bool retVal = MEA_FALSE;
	MEA_PortsMapping_t portMap;


    if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
                                       MEA_PORTS_TYPE_INGRESS_TYPE,
									   (MEA_Uint16)port,
									   &portMap)!=MEA_OK){
	        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n",port);
	}
	if (!portMap.valid){
		   /* check if ingress port exist */
		   if (MEA_Low_Get_Port_Mapping_By_key(MEA_UNIT_0,
											   MEA_PORTS_TYPE_INGRESS_TYPE,
											   (MEA_Uint16)port,
											   &portMap)!=MEA_OK){
				 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"MEA_Low_Get_Port_Mapping_By_key failed on Port %d \n",port);
		   }
	 }
     if (!portMap.valid){
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"can not find Port %d \n",port);
	 } else{
		 if ( portMap.portType == MEA_PORTTYPE_AAL5_ATM_TC){
			 retVal=MEA_TRUE;
		 }
	 }
	return (retVal);
}



MEA_Status MEA_API_Get_IngressPort_Type(MEA_Unit_t unit,MEA_Port_t port ,MEA_Port_type_te *type){

   MEA_PortsMapping_t portMap;

   MEA_API_LOG

   MEA_OS_memset(&portMap,0,sizeof(MEA_PortsMapping_t));

   if((MEA_PACKET_ANALYZER_TYPE1_SUPPORT ||  MEA_PACKETGEN_SUPPORT )     &&
       (port == MEA_PACKET_GEN_RMON_PORT )) {
           (*type)=MEA_PORTTYPE_VIRTUAL;

           return MEA_OK ;
   }
   
//    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) != MEA_TRUE) {
//        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," %s %d is not Valid\n",__FUNCTION__,port);
//    return MEA_ERROR ;
//    }
   
   if (MEA_Low_Get_Port_Mapping_By_key(unit,MEA_PORTS_TYPE_INGRESS_TYPE, (MEA_Uint16)port,&portMap)!=MEA_OK){
   return MEA_ERROR ;
   }
  
   (*type)=(MEA_Port_type_te)portMap.portType;
    
   return MEA_OK ;
}



MEA_Status MEA_Low_Init_mapping_Interface_Id(MEA_Unit_t unit_i)
{

#if 0  
    MEA_db_HwUnit_t         hwUnit; 
    MEA_DeviceInfo_dbt      entry;
    MEA_Uint16 Interface_Id;

    hwUnit=0;
  

    if (MEA_API_Get_DeviceInfo(unit_i,hwUnit,&entry)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"Error at : MEA_API_Get_DeviceInfo (hwUnit=%d)\n",hwUnit);
            return MEA_OK;
    }

    Interface_Id= mea_drv_Get_DeviceInfo_Interface_Id(unit_i);

    MEA_OS_memset(&MEA_Interface_Mapping[0],0,sizeof(MEA_Interface_Mapping));


    if(Interface_Id == MEA_OS_DEVICE_SIMULATION_4200_SYAC){
        /* 1 interlaken */
        /* 4  RXAUI      */
        MEA_Interface_Mapping[0].valid=MEA_TRUE; 
        MEA_Interface_Mapping[0].interpaceType = MEA_INTERFACETYPE_Interlaken;
        MEA_Interface_Mapping[0].Ingressport_start=0;
        MEA_Interface_Mapping[0].Ingressport_end=63;  



         
         
        MEA_Interface_Mapping[118].valid=MEA_TRUE; 
        MEA_Interface_Mapping[118].interpaceType = MEA_INTERFACETYPE_RXAUI;
        MEA_Interface_Mapping[118].Ingressport_start=118;
        MEA_Interface_Mapping[118].Ingressport_end=118;

        MEA_Interface_Mapping[119].valid=MEA_TRUE; 
        MEA_Interface_Mapping[119].interpaceType = MEA_INTERFACETYPE_RXAUI;
        MEA_Interface_Mapping[119].Ingressport_start=119;
        MEA_Interface_Mapping[119].Ingressport_end=119;


    }


#endif



    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_Low_Port_Mapping_init_New>                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_Low_Port_Mapping_Init(MEA_Unit_t unit_i)
{

    MEA_Port_t port;

//    MEA_Uint32 num_of_giga,num_of_Qxgmii;
    
   
    
    //MEA_Uint32 policer_map=0;
	MEA_Uint32 size;
	MEA_Uint32 interface_ID;


    if(b_bist_test){
        return MEA_OK;
    }
    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Ports Mapping...\n");
//     num_of_giga=0;
//     num_of_Qxgmii=0;
    
    
    
//    MEA_OS_memset(&MEA_PortsTypeSetting[0],0,sizeof(MEA_PortsTypeSetting));

	size = (MEA_MAX_PORT_NUMBER_INGRESS+1) * sizeof(MEA_PortsMapping_t);
	MEA_PortsMapping = (MEA_PortsMapping_t*)MEA_OS_malloc(size);
	if (MEA_PortsMapping == NULL) {
		MEA_OS_free(MEA_PortsMapping);
		MEA_PortsMapping = NULL;
		
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s \n- Allocate MEA_PortsMapping failed (size=%d)\n",
			__FUNCTION__,
			size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_PortsMapping, 0, size);



	
	size = (MEA_MAX_PORT_NUMBER_EGRESS + 1) * sizeof(MEA_Port_t);
	MEA_port_map_array = (MEA_Port_t*)MEA_OS_malloc(size);
	if (MEA_port_map_array == NULL) {
		MEA_OS_free(MEA_port_map_array);
		MEA_port_map_array = NULL;

		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s \n- Allocate MEA_port_map_array failed (size=%d)\n",
			__FUNCTION__,
			size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_port_map_array, 0, size);


	size = (MEA_MAX_PORT_NUMBER_INGRESS + 1) * sizeof(MEA_Port_t);
	MEA_port_map_arrayINGRESS = (MEA_Port_t*)MEA_OS_malloc(size);
	if (MEA_port_map_arrayINGRESS == NULL) {
		MEA_OS_free(MEA_port_map_arrayINGRESS);
		MEA_port_map_arrayINGRESS = NULL;

		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
			"%s \n- Allocate MEA_port_map_arrayINGRESS failed (size=%d)\n",
			__FUNCTION__,
			size);
		return MEA_ERROR;
	}
	MEA_OS_memset((char*)MEA_port_map_arrayINGRESS, 0, size);

	

    for(port=0;port<= MEA_MAX_PORT_NUMBER_INGRESS;port++){

		
        if(mea_drv_get_Ports2InterfaceInfo_Type(port, MEA_TYPE_Ports2Interf_Interface_Port_type) == 0 )
            continue;
    
		if (port == 128) {
			port = 128;
		}

        MEA_PortsMapping[port].valid = MEA_TRUE;
        MEA_PortsMapping[port].physicalport  = port;
        MEA_PortsMapping[port].Ingressport   = port;
        
		MEA_PortsMapping[port].portType = mea_drv_get_Ports2InterfaceInfo_Type(port, MEA_TYPE_Ports2Interf_Interface_Port_type); //MEA_Ports2InterfaceInfo[port].val.Port_type;
		if (MEA_PortsMapping[port].portType == MEA_PORTTYPE_VIRTUAL) {
			MEA_PortsMapping[port].ingressValid = MEA_TRUE;
			MEA_PortsMapping[port].Ingressport = port;
            MEA_PortsMapping[port].virualValid = MEA_TRUE;
            MEA_PortsMapping[port].virtualport = port;

			MEA_PortsMapping[port].EgressValid = MEA_FALSE;
			MEA_PortsMapping[port].Egressport = MEA_PORT_NOT_VALID;
			MEA_PortsMapping[port].DefaltClasterValid = MEA_FALSE;
			MEA_PortsMapping[port].DefaltClaster = 0xFFFF;
		}
		else {
			MEA_PortsMapping[port].ingressValid = MEA_TRUE;
			MEA_PortsMapping[port].Ingressport = port;
			if (port <= MEA_MAX_PORT_NUMBER_EGRESS) {
				MEA_PortsMapping[port].EgressValid = MEA_TRUE;
				MEA_PortsMapping[port].Egressport = port;
			}
			else {
				MEA_PortsMapping[port].EgressValid = MEA_FALSE;
				MEA_PortsMapping[port].Egressport = MEA_PORT_NOT_VALID;
			}

			MEA_PortsMapping[port].DefaltClasterValid = MEA_TRUE;
			MEA_PortsMapping[port].DefaltClaster = port;
		
		}

        
        
        //MEA_PortsMapping[port].port_slice_ID = 
		interface_ID=mea_drv_get_Ports2InterfaceInfo_Type(port, MEA_TYPE_Ports2Interf_Interface_interface_ID);

		MEA_PortsMapping[port].PortTo_interfaceId = interface_ID;

		

        MEA_PortsMapping[port].PortTo_interfaceId_Type = mea_drv_get_Ports2InterfaceInfo_Type(port, MEA_TYPE_Ports2Interf_Interface_Type); 
		//MEA_Ports2InterfaceInfo[MEA_Ports2InterfaceInfo[port].val.interface_ID].val.Interface_type;


#if 0  // we have profile of Flow_Policer
        if(mea_dev_get_port_Policer_Mapping(unit_i,(MEA_Port_t)MEA_PortsMapping[port].Ingressport,&policer_map) !=MEA_OK){
            MEA_OS_LOG_logMsg (MEA_OS_LOG_WARNING,"%s - invalid port policer map(%d) \n",
							 __FUNCTION__,(int)MEA_PortsMapping[port].Ingressport);
         
         }
         MEA_PortsMapping[port].policer_map=(MEA_Uint16)policer_map;
#endif
        

    }

    mea_drv_Get_DeviceInfo_Show_Port_Interface();
 



    return MEA_OK;
}

MEA_Status MEA_Low_Port_Mapping_Conclude(MEA_Unit_t unit_i)
{

	
	if (b_bist_test) {
		return MEA_OK;
	}
	

	

	
	if (MEA_PortsMapping != NULL) {
		MEA_OS_free(MEA_PortsMapping);
	}

	if (MEA_port_map_array != NULL) {
		MEA_OS_free(MEA_port_map_array);
		MEA_port_map_array = NULL;
	}

	if (MEA_port_map_arrayINGRESS != NULL) {
		MEA_OS_free(MEA_port_map_arrayINGRESS);
		MEA_port_map_arrayINGRESS = NULL;
	}


	return MEA_OK;
}


MEA_Status MEA_API_Get_Port2Interface_Id(MEA_Unit_t       unit,
                                            MEA_Port_t  port,
                                            MEA_Interface_t  *Id_o)
{

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        return MEA_ERROR;
    }
  

    *Id_o=(MEA_Interface_t)MEA_PortsMapping[port].PortTo_interfaceId;


    return MEA_OK;

}



 MEA_Status MEA_low_configure_Port_Shaper_default()
{

    MEA_Port_t                port;
    MEA_EgressPort_Entry_dbt entry;


    for (port=0; port <= MEA_MAX_PORT_NUMBER;port++ )
    {
        MEA_OS_memset(&entry,0,sizeof(entry));

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }
        if(port==127)
            continue;

        if(MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,port,&entry)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"ERROR: MEA_API_Get_EgressPort_Entry failed port=%d\n",port);
            return MEA_ERROR;
        }


        entry.shaper_info.CIR  = 1000000000 ;     

        entry.shaper_info.CBS  = 1536 ;     
        entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;  
        entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
        entry.shaper_info.overhead = 0 ;//20
        entry.shaper_info.Cell_Overhead = 0;
        entry.shaper_enable = ENET_TRUE;
        MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,port,&entry);


    }//end for

    return MEA_OK;
}








 /************************************************************************/
 /*   LAG profiles                                                      */
 /************************************************************************/
 static void mea_drv_LagClusterReg_UpdateHwEntry(MEA_funcParam arg1,
     MEA_funcParam arg2,
     MEA_funcParam arg3,
     MEA_funcParam arg4)
 {
     MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
     //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
     //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
     MEA_Uint32  entry = (MEA_Uint32)((long)arg4);




     MEA_Uint32                 val[1];
     MEA_Uint32                 i=0;
     //MEA_Uint32                  count_shift;

     /* Zero  the val */
     for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
         val[i]=0;
     }
     //count_shift=0;

     val[0]=entry;


     /* Update Hw Entry */
     for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
     {
         MEA_API_WriteReg(unit_i,
             MEA_IF_WRITE_DATA_REG(i),
             val[i],
             MEA_MODULE_IF);  
     }



 }



 static void mea_drv_LagGroupReg_UpdateHwEntry(MEA_funcParam arg1,
     MEA_funcParam arg2,
     MEA_funcParam arg3,
     MEA_funcParam arg4)
 {
     MEA_Unit_t             unit_i   = (MEA_Unit_t            )arg1;
     //MEA_db_HwUnit_t        hwUnit_i = (MEA_db_HwUnit_t       )arg2;
     //MEA_db_HwId_t          hwId_i   = (MEA_db_HwId_t         )arg3;
     MEA_Uint32    entry = (MEA_Uint32)((long)arg4);




     MEA_Uint32                 val[1];
     MEA_Uint32                 i=0;
     //MEA_Uint32                  count_shift;

     /* Zero  the val */
     for (i=0;i<MEA_NUM_OF_ELEMENTS(val);i++) {
         val[i]=0;
     }
     //count_shift=0;

     val[0]=entry;


     /* Update Hw Entry */
     for (i=0;i < MEA_NUM_OF_ELEMENTS(val);i++) 
     {
         MEA_API_WriteReg(unit_i,
             MEA_IF_WRITE_DATA_REG(i),
             val[i],
             MEA_MODULE_IF);  
     }



 }








static MEA_Status mea_drv_LAG_UpdateHw(MEA_Unit_t     unit_i,
                                       MEA_Uint16     id_i,
                                       MEA_LAG_dbt    *new_entry_pi,
                                       MEA_LAG_dbt    *old_entry_pi  /*,   MEA_SETTING_TYPE_te type*/ )
{

    
   
    mea_drv_lag_private_dbt privateData[MEA_LAG_MAX_NUM_OF_CLUSTERS];
    MEA_Uint32            set_mask_cluster[32];


    MEA_ind_write_t      ind_write;
    MEA_LAG_Group_Reg0_dbt lagGropEntry0;
    MEA_LAG_Group_Reg1_dbt lagGropEntry1;
    MEA_Uint32              value;
    MEA_Uint8 clusterOngroup=0xFF;
    ENET_Queue_dbt     entryCluster;

    
    MEA_Uint32 i;
    
    
    MEA_OS_memset(&privateData,0,sizeof(privateData));
    MEA_OS_memset(&set_mask_cluster[0],0,sizeof(set_mask_cluster));

    if ((old_entry_pi) &&
        (MEA_OS_memcmp(new_entry_pi,
        old_entry_pi,
        sizeof(*new_entry_pi))== 0) ) { 
            return MEA_OK;
    }
    
    
    for (i=0 ; i<new_entry_pi->numof_clusters;i++)
    {
        //check if the cluster
        if (!ENET_IsValid_Queue(unit_i,
            new_entry_pi->cluster[i],
            MEA_TRUE)) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  (%d) is not valid \n",
                    __FUNCTION__,
                    new_entry_pi->cluster[i]);

                return MEA_ERROR;
        }
        /**/

        MEA_OS_memset(&entryCluster,0,sizeof(entryCluster));
        if(clusterOngroup==0xff){
            /*******/
            /*  take the first cluster with the group for else    */
            /************/
            if(ENET_Get_Queue (unit_i,
                new_entry_pi->cluster[i], /* external */
                &entryCluster)!=MEA_OK) {

                    return MEA_ERROR;
            }
            //internal
            
            if(entryCluster.port.type != ENET_QUEUE_PORT_TYPE_PORT ){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"The cluster type must be QUEUE_PORT_TYPE_PORT\n ");
                return MEA_ERROR;
            }
            privateData[i].enable           = MEA_TRUE;
            privateData[i].maskBit_en       = MEA_TRUE;
            privateData[i].internalClaster = enet_cluster_external2internal(new_entry_pi->cluster[i]);
            privateData[i].port             = entryCluster.port.id.port;
			privateData[i].interfaceId = (MEA_Uint8)mea_drv_get_Ports2InterfaceInfo_Type(entryCluster.port.id.port, MEA_TYPE_Ports2Interf_Interface_interface_ID); //MEA_Ports2InterfaceInfo[entryCluster.port.id.port].val.interface_ID;
            privateData[i].interface_index  = mea_drv_Get_Interface_Hw_index(unit_i, privateData[i].interfaceId);
            privateData[i].mask =  (1<<i);

//             if(entryCluster.groupId>= MEA_LAG_MAX_GROUPS){
//                 return MEA_ERROR;
//             }
            clusterOngroup=entryCluster.groupId;


        }else{


            if(ENET_Get_Queue (unit_i,
                new_entry_pi->cluster[i], /* external */
                &entryCluster)!=MEA_OK){
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s -   clusters %d  \n",
                        __FUNCTION__,
                        new_entry_pi->cluster[i]);
                    return MEA_ERROR;
            }
            if(clusterOngroup != entryCluster.groupId ){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  (%d) is not same group id of clusters %d \n",
                    __FUNCTION__,
                    new_entry_pi->cluster[i],clusterOngroup);

                return MEA_ERROR;
            }
            
            if(entryCluster.port.type != ENET_QUEUE_PORT_TYPE_PORT ){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"The cluster type must be QUEUE_PORT_TYPE_PORT\n ");
                return MEA_ERROR;
            }
            privateData[i].enable           = MEA_TRUE;
            //internal
            privateData[i].maskBit_en       = MEA_TRUE;
            privateData[i].internalClaster = enet_cluster_external2internal(new_entry_pi->cluster[i]);
            privateData[i].port             = entryCluster.port.id.port;
            privateData[i].interfaceId      =    (MEA_Uint8)mea_drv_get_Ports2InterfaceInfo_Type(entryCluster.port.id.port, MEA_TYPE_Ports2Interf_Interface_interface_ID);
            privateData[i].interface_index  = mea_drv_Get_Interface_Hw_index(unit_i, privateData[i].interfaceId);
            privateData[i].mask =  (1<<i);


        }

    }


    /*Set lag_group_Table */
    MEA_OS_memset (&lagGropEntry0,0,sizeof(lagGropEntry0));
    MEA_OS_memset(&lagGropEntry1, 0, sizeof(lagGropEntry1));
    value=0;
    if(new_entry_pi->numof_clusters==0){

    }else{
        
        lagGropEntry0.val.cluster_leader   =  privateData[0].internalClaster; 
        lagGropEntry0.val.Cluster_mask     =  new_entry_pi->clustermask;
        lagGropEntry0.val.cluster_group_id =  clusterOngroup;

        lagGropEntry1.val.cluster_leader = privateData[0].internalClaster;
        lagGropEntry1.val.Cluster_mask = new_entry_pi->clustermask;
        lagGropEntry1.val.cluster_group_id = clusterOngroup;
        
        switch (new_entry_pi->numof_clusters)
        {
           case 1:
           case 2:
                lagGropEntry0.val.cluster_type= 1;
                lagGropEntry1.val.cluster_type = 1;
            break;
            case 3:
            case 4:
                lagGropEntry0.val.cluster_type= 2;
                lagGropEntry1.val.cluster_type = 2;
            break;
            case 5:
            case 6:
            case 7:
            case 8:
                lagGropEntry0.val.cluster_type= 3;
                lagGropEntry1.val.cluster_type = 3;
            break;
            default:
                lagGropEntry0.val.cluster_type = 0;
                lagGropEntry1.val.cluster_type = 0;
           break;
        }
        
        
    }

    if (MEA_device_environment_info.MEA_DEVICE_LAG_TYPE == 1) {
        value = lagGropEntry1.reg[0];
    }
    else {
        value = lagGropEntry0.reg[0];
    }
    
    /* Prepare ind_write structure */
    ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_LAG_GROUP_ID;
    ind_write.tableOffset   = id_i; 
    ind_write.cmdReg        = MEA_IF_CMD_REG;      
    ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
    ind_write.statusReg     = MEA_IF_STATUS_REG;   
    ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
    ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_LagGroupReg_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
    //ind_write.funcParam3    = (MEA_funcParam);

        ind_write.tableOffset   = id_i ;
        
        ind_write.funcParam4 = (MEA_funcParam)((long)value);

        /* Write to the Indirect Table */
        if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
            return ENET_ERROR;
        }
    








    /* */

        

            /* Prepare ind_write structure */
        ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_LAG_GROUP_ID_T0_CLUSTER;
        
        ind_write.cmdReg        = MEA_IF_CMD_REG;      
        ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
        ind_write.statusReg     = MEA_IF_STATUS_REG;   
        ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
        ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
        ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
        ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_LagClusterReg_UpdateHwEntry;
        ind_write.funcParam1 = (MEA_funcParam)unit_i;
        //ind_write.funcParam2    = (MEA_Uint32)hwUnit;
        //ind_write.funcParam3    = (MEA_Uint32);
        for(i=0;i<8;i++){
        ind_write.tableOffset   = (id_i * 8) + i ;

        ind_write.funcParam4 = (MEA_funcParam)((long)privateData[i].internalClaster);

        /* Write to the Indirect Table */
            if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                    __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
                return ENET_ERROR;
            }
        }








    /* */
        if(new_entry_pi->numof_clusters!=0){
           for(i=0;i<8;i++){
               if(privateData[i].enable == MEA_TRUE )
                set_mask_cluster[privateData[i].interface_index]|=(MEA_Uint32)privateData[i].mask;
           }
        }
             /* Prepare ind_write structure */
         ind_write.tableType     = ENET_IF_CMD_PARAM_TBL_LAG_MASK_INTERFACE_CLUSTER;

         ind_write.cmdReg        = MEA_IF_CMD_REG;      
         ind_write.cmdMask       = MEA_IF_CMD_PARSER_MASK;      
         ind_write.statusReg     = MEA_IF_STATUS_REG;   
         ind_write.statusMask    = MEA_IF_STATUS_BUSY_MASK;   
         ind_write.tableAddrReg  = MEA_IF_CMD_PARAM_REG;
         ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
         ind_write.writeEntry    = (MEA_FUNCPTR)mea_drv_LagClusterReg_UpdateHwEntry;
         ind_write.funcParam1 = (MEA_funcParam)unit_i;
         //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
         //ind_write.funcParam3    = (MEA_funcParam);
         for(i=0;i<MEA_MAX_INERTNAL_INTERFACE_NUMBER;i++){
             ind_write.tableOffset   = (id_i * MEA_MAX_INERTNAL_INTERFACE_NUMBER) + i ;
             if(new_entry_pi->numof_clusters!=0){
                  if(set_mask_cluster[i]==0)
                      continue;
                  ind_write.funcParam4 = (MEA_funcParam)((long)set_mask_cluster[i]);
                 }else {
                 ind_write.funcParam4    = 0;
             }

             /* Write to the Indirect Table */
             if (ENET_WriteIndirect(unit_i,&ind_write,ENET_MODULE_IF)!=MEA_OK) {
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
                     __FUNCTION__,ind_write.tableType,ind_write.tableOffset,ENET_MODULE_IF);
                 return ENET_ERROR;
             }
         }

    MEA_OS_memcpy(&MEA_LAG_info_Table[id_i].lag_privateData,&privateData,sizeof(MEA_LAG_info_Table[0].lag_privateData));



     /* Return to caller */
     return MEA_OK;
}


static MEA_Bool mea_drv_LAG_find_free(MEA_Unit_t       unit_i,MEA_Uint16       *id_io)
{
     MEA_Uint16 i;

     for(i=0;i< MEA_LAG_MAX_PROF; i++) {
         if (MEA_LAG_info_Table[i].valid == MEA_FALSE){
             *id_io=i; 
             return MEA_TRUE;
         }
     }
     return MEA_FALSE;

}


MEA_Bool mea_drv_LAG_find_by_cluster(MEA_Unit_t       unit_i, MEA_Port_t		cluster, MEA_Uint16       *id_io)
{
     MEA_Uint16 i, j;

     for(i = 0; i < MEA_LAG_MAX_PROF; i++)
     {
        if (MEA_LAG_info_Table[i].valid == MEA_FALSE)
            continue;

		 for (j = 0; j < MEA_NUM_OF_ELEMENTS(MEA_LAG_info_Table[i].data.cluster); j++)
		 {
			 if (MEA_LAG_info_Table[i].data.cluster[j] == cluster)
			 {
				 *id_io = i;
				 return MEA_TRUE;
			 }
		 }
     }

     return MEA_FALSE;
}






static MEA_Bool mea_drv_LAG_IsRange(MEA_Unit_t     unit_i,MEA_Uint16 id_i)
 {

     if ((id_i) >= MEA_LAG_MAX_PROF){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - out range \n",__FUNCTION__); 
         return MEA_FALSE;
     }
     return MEA_TRUE;
 }

static MEA_Status mea_drv_LAG_IsExist(MEA_Unit_t     unit_i,
                                      MEA_Uint16 id_i,
                                      MEA_Bool *exist)
{

     if(exist == NULL){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  exist = NULL \n",__FUNCTION__); 
         return MEA_ERROR;
     }
     *exist=MEA_FALSE;

     if(mea_drv_LAG_IsRange(unit_i,id_i)!=MEA_TRUE){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  failed  for id=%d\n",__FUNCTION__,id_i); 
         return MEA_ERROR;
     }

     if( MEA_LAG_info_Table[id_i].valid){
         *exist=MEA_TRUE;
         return MEA_OK;
     }

     return MEA_OK;
}

static MEA_Status mea_drv_LAG_check_parameters(MEA_Unit_t       unit_i,
                                               MEA_LAG_dbt      *entry_pi)
{


    if(entry_pi->numof_clusters >MEA_LAG_MAX_NUM_OF_CLUSTERS)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  entry_pi->numof_clusters = %d wrong \n",__FUNCTION__,entry_pi->numof_clusters); 
        return MEA_ERROR;
    }
#if 0
    if(entry_pi->numof_clusters  !=2   &&
        entry_pi->numof_clusters != 4 && 
        entry_pi->numof_clusters !=8 ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  entry_pi->numof_clusters = %d wrong \n",__FUNCTION__,entry_pi->numof_clusters); 
            return MEA_ERROR;
    }
#endif    

    if (entry_pi->numof_clusters == 0){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s -  entry_pi->numof_clusters = %d wrong must be >=2 \n",__FUNCTION__,entry_pi->numof_clusters); 
        return MEA_ERROR;
    }
    

    



     return MEA_OK;
}
#if 0
static MEA_Status mea_drv_LAG_add_owner(MEA_Unit_t       unit_i ,
                                        MEA_Uint16       id_i)
{
     MEA_Bool exist;

     if(mea_drv_LAG_IsExist(unit_i,id_i, &exist)!=MEA_OK){
         return MEA_ERROR;
     }

     MEA_LAG_info_Table[id_i].num_of_owners++;

     return MEA_OK;
}
static MEA_Status mea_drv_LAG_delete_owner(MEA_Unit_t       unit_i ,
                                            MEA_Uint16       id_i)
{
     MEA_Bool exist;

     if(mea_drv_LAG_IsExist(unit_i,id_i, &exist)!=MEA_OK){
         return MEA_ERROR;
     }

     if(MEA_LAG_info_Table[id_i].num_of_owners==1){
         MEA_LAG_info_Table[id_i].valid=MEA_FALSE;
         MEA_LAG_info_Table[id_i].num_of_owners=0;
         // need to set set hw zero
         return MEA_OK;

     }


     MEA_LAG_info_Table[id_i].num_of_owners--;

     return MEA_OK;
}
#endif
MEA_Status mea_drv_LAG_Init(MEA_Unit_t       unit_i)
{
     MEA_Uint32 size;
     MEA_Uint16 id;

     MEA_LAG_dbt entry;
     if (!MEA_LAG_SUPPORT){
        return MEA_OK;   
     }
     if(MEA_LAG_info_Table){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_LAG_info_Table already Init \n",__FUNCTION__);
        return MEA_ERROR; 
     }

     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize IF LAG table  \n");
     /* Allocate PacketGen Table */
     size = MEA_LAG_MAX_PROF * sizeof(MEA_LAG_Entry_dbt);
     if (size != 0) {
         MEA_LAG_info_Table = (MEA_LAG_Entry_dbt*)MEA_OS_malloc(size);
         if (MEA_LAG_info_Table == NULL) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - Allocate MEA_LAG_info_Table failed (size=%d)\n",
                 __FUNCTION__,size);
             return MEA_ERROR;
         }
         MEA_OS_memset(&(MEA_LAG_info_Table[0]),0,size);
     }
     /**/
     
    
     
     MEA_OS_memset(&entry,0,sizeof(entry));
     // TBD do i need to clear the memory in the FPGA
     for(id=0;id<MEA_LAG_MAX_PROF ;id++){
         if(mea_drv_LAG_UpdateHw(unit_i,id,&entry,NULL) !=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - Allocate mea_drv_LAG_UpdateHw failed init id= \n", __FUNCTION__,id);
          return MEA_ERROR;
         }
     }
     return MEA_OK;
}
MEA_Status mea_drv_LAG_ReInit(MEA_Unit_t       unit_i)
 {
     MEA_Uint16 id;
    
     MEA_LAG_Entry_dbt lag_info;
     MEA_Bool enable;
     ENET_QueueId_t clusterId;
    
     
     MEA_Uint32 i;

     if (!MEA_LAG_SUPPORT){
         return MEA_OK;   
     }


     for (id=0;id<MEA_LAG_MAX_PROF;id++) {
         if (MEA_LAG_info_Table[id].valid == MEA_FALSE) 
             continue;

         MEA_OS_memcpy(&lag_info, &MEA_LAG_info_Table[id], sizeof(lag_info));

          if (MEA_LAG_info_Table[id].valid == MEA_TRUE ){
             if(mea_drv_LAG_UpdateHw(unit_i,
                 id,
                 &(MEA_LAG_info_Table[id].data),
                 NULL) != MEA_OK) {
                     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s -  mea_drv_LAG_UpdateHw failed (id=%d)\n",__FUNCTION__,id);
                     return MEA_ERROR;
             }
             for (i = 0; i< lag_info.data.numof_clusters; i++) {
                 enable = lag_info.lag_privateData[i].maskBit_en;
                 clusterId = lag_info.data.cluster[i];
                 if (enable == MEA_FALSE) {
                     if (MEA_API_LAG_Set_ClusterMask(unit_i, id, clusterId, enable) != MEA_OK)
                     {
                         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  LAG_Set_ClusterMask failed (id=%d)\n", __FUNCTION__, id);
                     }
                 }
             }
         }

         
         
     }


     return MEA_OK;
}

MEA_Status mea_drv_LAG_Conclude(MEA_Unit_t       unit_i)
{

     /* Free the table */
     if (MEA_LAG_info_Table != NULL) {
         MEA_OS_free(MEA_LAG_info_Table);
         MEA_LAG_info_Table = NULL;
     }

     return MEA_OK;
}


MEA_Status mea_drv_LAG_Update_Entry(MEA_Unit_t  unit_i, MEA_Uint16  id_i, MEA_Bool Force)
{
    ENET_QueueId_t cluster;
    MEA_drv_mc_ehp_dbt data;
    MEA_drv_mc_ehp_dbt dataNew;
    MEA_Uint32 i, index,slot;
  
        cluster = MEA_LAG_info_Table[id_i].data.cluster[0];
        for (slot = 0; slot < MEA_MAX_NUM_OF_MC_ID; slot++)
        {
            MEA_OS_memset(&data, 0, sizeof(MEA_drv_mc_ehp_dbt));
            if (Force == MEA_TRUE){
                index = enet_cluster_external2internal(cluster)*MEA_MAX_NUM_OF_MC_ID + slot;
                if (mea_drv_mc_ehp_Get_Info(unit_i, index, &data) != MEA_OK){

                }
            }
            
            for (i = 1; i<=MEA_LAG_info_Table[id_i].data.numof_clusters-1; i++)
            {
                MEA_OS_memset(&dataNew, 0, sizeof(MEA_drv_mc_ehp_dbt));
                index = enet_cluster_external2internal(MEA_LAG_info_Table[id_i].data.cluster[i])*MEA_MAX_NUM_OF_MC_ID + slot;
                if (mea_drv_mc_ehp_Get_Info(unit_i, index, &dataNew) != MEA_OK){

                }
                if (Force == MEA_TRUE){
                    if (MEA_OS_memcmp(&(data),
                        &(dataNew),
                        sizeof(MEA_drv_mc_ehp_dbt)) != 0){
                        MEA_OS_memcpy(&dataNew, &data, sizeof(MEA_drv_mc_ehp_dbt));
                        mea_drv_mc_ehp_Update_HwInfo(unit_i, index, &dataNew);
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " update cluster %d slot %d \n",  MEA_LAG_info_Table[id_i].data.cluster[i],slot);

                    }
                    else
                    {
                        continue;
                    }

                }
                else
                {
                    if (mea_drv_mc_ehp_Get_Info(unit_i, index, &dataNew) != MEA_OK){

                    }
                    if (MEA_OS_memcmp(&(data),
                        &(dataNew),
                        sizeof(MEA_drv_mc_ehp_dbt)) != 0){
                        MEA_OS_memcpy(&dataNew, &data, sizeof(MEA_drv_mc_ehp_dbt));
                        mea_drv_mc_ehp_Update_HwInfo(unit_i, index, &dataNew);
                        MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, " clear cluster %d slot %d \n", MEA_LAG_info_Table[id_i].data.cluster[i], slot);

                    }
                    else
                    {
                        continue;
                    }
                }
           
                
               
            }
        }/* slot */



    

    return MEA_OK;

}


 /************************************************************************/
 /*  MEA_API LAG                                                         */
 /************************************************************************/

MEA_Status MEA_API_LAG_Create_Entry(MEA_Unit_t            unit_i,
                                    MEA_LAG_dbt           *entry_pi,
                                    MEA_Uint16            *id_io)
{
     MEA_Bool exist;

     MEA_API_LOG

         /* Check if support */
         if (!MEA_LAG_SUPPORT){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - LAG not support \n",
                 __FUNCTION__);
             return MEA_ERROR;   
         }




/* check parameter */
if (mea_drv_LAG_check_parameters(unit_i, entry_pi) != MEA_OK){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_LAG_check_parameters failed \n", __FUNCTION__);
    return MEA_ERROR;
}

         {
             MEA_Uint8  calcMask;
             if (entry_pi->clustermask == 0){
                 calcMask = (MEA_Uint8)((1 << (entry_pi->numof_clusters)) - 1);
                 entry_pi->clustermask = calcMask;
             }

         }



         /* Look for stream id */
         if ((*id_io) != MEA_PLAT_GENERATE_NEW_ID){
             /*check if the id exist*/
             if (mea_drv_LAG_IsExist(unit_i, *id_io, &exist) != MEA_OK){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - mea_drv_LAG_IsExist failed (*id_io=%d\n",
                     __FUNCTION__, *id_io);
                 return MEA_ERROR;
             }
             if (exist){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - *Id_io %d is already exist\n", __FUNCTION__, *id_io);
                 return MEA_ERROR;
             }

         }
         else {
             /*find new place*/
             if (mea_drv_LAG_find_free(unit_i, id_io) != MEA_TRUE){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - mea_drv_LAG_find_free failed\n", __FUNCTION__);
                 return MEA_ERROR;
             }
         }

         if (mea_drv_LAG_UpdateHw(unit_i,
             *id_io,
             entry_pi,
             NULL) != MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - mea_drv_LAG_UpdateHw failed for id %d\n", __FUNCTION__, *id_io);
             return MEA_ERROR;
         }

         MEA_LAG_info_Table[*id_io].valid = MEA_TRUE;
         MEA_LAG_info_Table[*id_io].num_of_owners = 1;
         MEA_OS_memcpy(&(MEA_LAG_info_Table[*id_io].data), entry_pi, sizeof(MEA_LAG_info_Table[*id_io].data));

         mea_drv_LAG_Update_Entry(unit_i, *id_io, MEA_TRUE);

         return MEA_OK;

}


MEA_Status MEA_API_LAG_UpDate_Entry(MEA_Unit_t         unit_i, MEA_Uint16         id_i)
{
    MEA_Bool exist = MEA_FALSE;

    /*check if the id exist*/
    if (mea_drv_LAG_IsExist(unit_i, id_i, &exist) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - mea_drv_LAG_IsExist failed (id_i=%d\n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }
    if (!exist){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - LAG %d not exist\n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }

    if (mea_drv_LAG_Update_Entry(unit_i, id_i, MEA_TRUE) != MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - LAG %d mea_drv_LAG_Upate_Entry \n", __FUNCTION__, id_i);
        return MEA_ERROR;
    }



    return MEA_OK;
}


MEA_Status MEA_API_LAG_Set_ClusterMask(MEA_Unit_t unit_i, MEA_Uint16         id_i, ENET_QueueId_t clusterId, MEA_Bool enable)
{

    MEA_ind_write_t        ind_write;
    MEA_LAG_Group_Reg0_dbt lagGropEntry0;
    MEA_LAG_Group_Reg1_dbt lagGropEntry1;
    MEA_Uint32             value;

    MEA_Uint32 i;
    MEA_Bool exist = MEA_FALSE;
    MEA_Bool cluster_found = MEA_FALSE;
    MEA_Uint32 Mask = 0;

    /* Check if support */
    if (!MEA_LAG_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - LAG not support \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if(MEA_API_LAG_IsExist(unit_i, id_i, &exist) != MEA_OK){
        return MEA_ERROR;
    }
    if (exist == MEA_FALSE)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - LAG %d not Exist \n",
            __FUNCTION__, id_i);
        return MEA_ERROR;
    }

    /*Set lag_group_Table */
    MEA_OS_memset(&lagGropEntry0, 0, sizeof(lagGropEntry0));
    MEA_OS_memset(&lagGropEntry1, 0, sizeof(lagGropEntry1));
    value=0;
    {

        for (i = 0; i < MEA_LAG_info_Table[id_i].data.numof_clusters; i++){

            if (MEA_LAG_info_Table[id_i].data.cluster[i] == clusterId  && MEA_LAG_info_Table[id_i].lag_privateData[0].enable == MEA_TRUE){
                cluster_found = MEA_TRUE;
            }

        }

        if (cluster_found == MEA_FALSE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - LagId %d  the ClusterId %d is not part of the Lag \n", __FUNCTION__, id_i, clusterId);
            return MEA_ERROR;
        }

        Mask = 0;
        for (i = 0; i < MEA_LAG_info_Table[id_i].data.numof_clusters; i++){
            
            if (MEA_LAG_info_Table[id_i].data.cluster[i] == clusterId  && MEA_LAG_info_Table[id_i].lag_privateData[i].enable == MEA_TRUE){
                cluster_found = MEA_TRUE;
                MEA_LAG_info_Table[id_i].lag_privateData[i].maskBit_en = enable;
            }
            Mask |= ((MEA_LAG_info_Table[id_i].lag_privateData[i].maskBit_en) << i);

        }
        MEA_LAG_info_Table[id_i].data.clustermask = Mask;


        lagGropEntry0.val.cluster_leader = MEA_LAG_info_Table[id_i].lag_privateData[0].internalClaster;
        lagGropEntry0.val.Cluster_mask = Mask;
        lagGropEntry0.val.cluster_group_id = MEA_LAG_info_Table[id_i].clusterOngroup;

        lagGropEntry1.val.cluster_leader = MEA_LAG_info_Table[id_i].lag_privateData[0].internalClaster;
        lagGropEntry1.val.Cluster_mask = Mask;
        lagGropEntry1.val.cluster_group_id = MEA_LAG_info_Table[id_i].clusterOngroup;


        switch (MEA_LAG_info_Table[id_i].data.numof_clusters)
        {
        case 1:
        case 2:
            lagGropEntry0.val.cluster_type = 1;
            lagGropEntry1.val.cluster_type = 1;
            break;
        case 3:
        case 4:
            lagGropEntry0.val.cluster_type = 2;
            lagGropEntry1.val.cluster_type = 2;
            break;
        case 5:
        case 6:
        case 7:
        case 8:
            lagGropEntry0.val.cluster_type = 3;
            lagGropEntry1.val.cluster_type = 3;
            break;
        default:
            lagGropEntry0.val.cluster_type = 0;
            lagGropEntry1.val.cluster_type = 0;
            break;
        }


    }
    
    if (MEA_device_environment_info.MEA_DEVICE_LAG_TYPE == 1) {
        value= lagGropEntry1.reg[0];
    }
    else {
        value = lagGropEntry0.reg[0];
    }



    /* Prepare ind_write structure */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_LAG_GROUP_ID;
    ind_write.tableOffset = id_i;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_LagGroupReg_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    //ind_write.funcParam2    = (MEA_funcParam)hwUnit;
    //ind_write.funcParam3    = (MEA_funcParam);

    ind_write.tableOffset = id_i;

    ind_write.funcParam4 = (MEA_funcParam)((long)value);

    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit_i, &ind_write, ENET_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_IF);
        return ENET_ERROR;
    }













    return MEA_OK;
}

MEA_Status MEA_API_LAG_Set_Entry(MEA_Unit_t         unit_i,
                                 MEA_Uint16         id_i,
                                 MEA_LAG_dbt        *entry_pi)
 {
     MEA_Bool exist;

     MEA_API_LOG

         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
         "%s - Not support  use delete and creat (id_i=%d\n",
         __FUNCTION__, id_i);
     return MEA_ERROR;

         /* Check if support */
         if (!MEA_LAG_SUPPORT){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - LAG not support \n",
                 __FUNCTION__);
             return MEA_ERROR;   
         }

         /*check if the id exist*/
         if (mea_drv_LAG_IsExist(unit_i,id_i,&exist)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - mea_drv_LAG_IsExist failed (id_i=%d\n",
                 __FUNCTION__,id_i);
             return MEA_ERROR;
         }
         if (!exist){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - LAG %d not exist\n",__FUNCTION__,id_i);
             return MEA_ERROR;
         }


         /* check parameter that not change only the mask can be change*/
        
         if(entry_pi->numof_clusters != MEA_LAG_info_Table[id_i].data.numof_clusters ){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - numof_clusters on lag can't  change \n",__FUNCTION__,id_i);
             return MEA_ERROR;
         }
         
         if (MEA_OS_memcmp(&entry_pi->cluster       ,
             &MEA_LAG_info_Table[id_i].data.cluster ,
             sizeof(entry_pi->cluster))!=0) {            
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - clusters on lag can't change \n",__FUNCTION__,id_i);
                 return MEA_ERROR;

         } 

         {
             MEA_Uint8 calcMask;
             calcMask= (MEA_Uint8)((1<<(entry_pi->numof_clusters))-1);

             if(entry_pi->clustermask > calcMask){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - clustermask on lag wrong setting \n",__FUNCTION__,id_i);
                 return MEA_ERROR;
             }
         
         
         }

         if( mea_drv_LAG_UpdateHw(unit_i,
             id_i,
             entry_pi,
             &MEA_LAG_info_Table[id_i].data
             
             )!= MEA_OK){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - mea_drv_TDM_interface_prof_Idle_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
                 return MEA_ERROR;
         }

         MEA_OS_memcpy(&(MEA_LAG_info_Table[id_i].data),
             entry_pi,
             sizeof(MEA_LAG_info_Table[id_i].data));



         return MEA_OK;
}

MEA_Status MEA_API_LAG_Delete_Entry(MEA_Unit_t                  unit_i,
                                    MEA_Uint16                   id_i)
{

     MEA_Bool                    exist;

     MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

         /* Check if support */
         if (!MEA_LAG_SUPPORT){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - LAG not support \n",
                 __FUNCTION__);
             return MEA_ERROR;   
         }

         /*check if the id exist*/
         if (mea_drv_LAG_IsExist(unit_i,id_i,&exist)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - mea_drv_LAG_IsExist failed (id_i=%d\n",
                 __FUNCTION__,id_i);
             return MEA_ERROR;
         }
         if (!exist){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - LAG %d not exist\n",__FUNCTION__,id_i);
             return MEA_ERROR;
         }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

         if(MEA_LAG_info_Table[id_i].num_of_owners >1){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - need to delete all the reference to this profile\n",__FUNCTION__,id_i);
             return MEA_ERROR;
         }

         /*need to delete all the MC of the LAG*/
          mea_drv_LAG_Update_Entry(unit_i, id_i, MEA_FALSE);

         

         MEA_LAG_info_Table[id_i].data.numof_clusters =0;

         /*Write to Hw All zero*/
         if( mea_drv_LAG_UpdateHw(unit_i,
             id_i,
             &MEA_LAG_info_Table[id_i].data,
             NULL)!= MEA_OK){
                 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                     "%s - mea_drv_TDM_interface_prof_Idle_UpdateHw failed for id %d\n",__FUNCTION__,id_i);
                 return MEA_ERROR;
         }
         /* need to delete from the cluster the that is */


         MEA_OS_memset(&MEA_LAG_info_Table[id_i].data,0,sizeof(MEA_LAG_info_Table[0].data));
         MEA_LAG_info_Table[id_i].valid = MEA_FALSE;
         MEA_LAG_info_Table[id_i].num_of_owners=0;

         /* Return to caller */
         return MEA_OK;
}

MEA_Status MEA_API_LAG_Get_Entry(MEA_Unit_t                  unit_i,
                                     MEA_Uint16                   id_i,
                                     MEA_LAG_dbt    *entry_po)
 {
     MEA_Bool    exist; 

     MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

         /* Check if support */
         if (!MEA_LAG_SUPPORT){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - LAG not support \n",
                 __FUNCTION__);
             return MEA_ERROR;   
         }

         /*check if the id exist*/
         if (mea_drv_LAG_IsExist(unit_i,id_i,&exist)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - mea_drv_LAG_IsExist failed (id_i=%d\n",
                 __FUNCTION__,id_i);
             return MEA_ERROR;
         }
         if (!exist){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - id %d not exist\n",__FUNCTION__,id_i);
             return MEA_ERROR;
         }
         if(entry_po==NULL){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - entry_po is null\n",__FUNCTION__,id_i);
             return MEA_ERROR;

         }
#endif /*MEA_SW_CHECK_INPUT_PARAMETERS*/

         /* Copy to caller structure */
         MEA_OS_memset(entry_po,0,sizeof(*entry_po));
         MEA_OS_memcpy(entry_po ,
             &(MEA_LAG_info_Table[id_i].data),
             sizeof(*entry_po));

         /* Return to caller */
         return MEA_OK;


 }

MEA_Status MEA_API_LAG_GetFirst_Entry(MEA_Unit_t        unit_i,
                                       MEA_Uint16       *id_o,
                                       MEA_LAG_dbt      *entry_po, 
                                       MEA_Bool         *found_o)
 {
     MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

         if (!MEA_LAG_SUPPORT){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - LAG not support \n",
                 __FUNCTION__);
             return MEA_ERROR;   
         }

         if (id_o == NULL) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - id_o == NULL\n",__FUNCTION__);
             return MEA_ERROR;
         }

         if (found_o == NULL) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
             return MEA_ERROR;
         }


#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

         if (found_o) {
             *found_o = MEA_FALSE;
         }

         for ( (*id_o)=0;
             (*id_o) < MEA_LAG_MAX_PROF; 
             (*id_o)++ ) {
                 if (MEA_LAG_info_Table[(*id_o)].valid == MEA_TRUE) {
                     if (found_o) {
                         *found_o= MEA_TRUE;
                     }

                     if (entry_po) {
                         MEA_OS_memcpy(entry_po,
                             &MEA_LAG_info_Table[(*id_o)].data,
                             sizeof(*entry_po));
                     }
                     break;
                 }
         }


         return MEA_OK;

 }

MEA_Status MEA_API_LAG_GetNext_Entry(MEA_Unit_t     unit_i,
                                     MEA_Uint16     *id_io,
                                     MEA_LAG_dbt    *entry_po, 
                                     MEA_Bool       *found_o)
 {
     MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
         if (!MEA_LAG_SUPPORT){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s - LAG not support \n",
                 __FUNCTION__);
             return MEA_ERROR;   
         }
         if (id_io == NULL) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Id_io == NULL\n",__FUNCTION__);
             return MEA_ERROR;
         }

         if (found_o == NULL) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - found_o == NULL\n",__FUNCTION__);
             return MEA_ERROR;
         }
#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */

         if (found_o) {
             *found_o = MEA_FALSE;
         }

         for ( (*id_io)++;
             (*id_io) < MEA_LAG_MAX_PROF; 
             (*id_io)++ ) {
                 if(MEA_LAG_info_Table[(*id_io)].valid == MEA_TRUE) {
                     if (found_o) {
                         *found_o= MEA_TRUE;
                     }
                     if (entry_po) {
                         MEA_OS_memcpy(entry_po,
                             &MEA_LAG_info_Table[(*id_io)].data,
                             sizeof(*entry_po));
                     }
                     break;
                 }
         }


         return MEA_OK;
 }


MEA_Status MEA_API_LAG_IsExist(MEA_Unit_t     unit_i,
                               MEA_Uint16 id_i,
                               MEA_Bool *exist)
{

    return mea_drv_LAG_IsExist(unit_i,id_i,exist);
}




MEA_Status MEA_API_Port_Serdes_Set_Reset(MEA_Unit_t     unit_i,MEA_Port_t port)
{
   MEA_Uint32 val;
   MEA_Port_type_te porttype;
    
    if(!MEA_SERDES_RESET_SUPPORT){
        return MEA_ERROR;
    }

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        return MEA_ERROR;
    }

    if(MEA_API_Get_IngressPort_Type(unit_i,port ,&porttype)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," MEA_API_Get_IngressPort_Type failed port=%d\n",port);
        return MEA_ERROR;
    }

    if(((porttype != MEA_PORTTYPE_GBE_MAC) &&
        (porttype != MEA_PORTTYPE_GBE_1588)       )     ){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR," The API support Only port-type  MEA_PORTTYPE_GBE_MAC/ QXGMII  \n",port);
            return MEA_ERROR;
    }


    val=0;
    val   =  (1<<(port%32));


    MEA_API_WriteReg(MEA_UNIT_0,
        ENET_IF_SERDES_RESET_31_0_REG+((port/32)*4),
        val,
        MEA_MODULE_IF);


return MEA_OK;

}






/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/


static void mea_drv_Src_to_Cluster_UpdateHW_Entry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{

    //MEA_Unit_t              unit_i      = (MEA_Unit_t             )arg1;
    // MEA_db_HwUnit_t         hwUnit_i    = (MEA_db_HwUnit_t        )arg2;
    MEA_Uint32           len = (MEA_Uint32)(long)arg3;
    MEA_Uint32 *data = (MEA_Uint32*)arg4;

    MEA_Uint32      i;



    for (i = 0; i < len; i++) {
        MEA_API_WriteReg(MEA_UNIT_0,
            MEA_IF_WRITE_DATA_REG(i),
            data[i],
            MEA_MODULE_IF);
    }



}




MEA_Status mea_drv_SrcPort_to_Cluster_UpdateHW(MEA_Unit_t unit, 
                                               MEA_Port_t port, 
                                               MEA_BFD_src_dbt    *new_entry_pi,
                                               MEA_BFD_src_dbt    *old_entry_pi)
{
    MEA_ind_write_t   ind_write;
    MEA_Uint32 val[1];
    MEA_Uint32 len = 1;
    

    if (!MEA_BFD_SUPPORT) {
        return MEA_OK;
    }

    


    if ((old_entry_pi) &&
        (MEA_OS_memcmp(new_entry_pi,
            old_entry_pi,
            sizeof(*new_entry_pi)) == 0)) {
        return MEA_OK;
    }

    val[0] = 0;
      //get internal cluster
    if (new_entry_pi->enable) {
        if (ENET_IsValid_Queue(unit, new_entry_pi->queue_Id,/*external*/
            MEA_TRUE)) {
            val[0] = enet_cluster_external2internal(new_entry_pi->queue_Id);
        }
        else {
            new_entry_pi->enable = MEA_FALSE;
        }
    }
    else {
        new_entry_pi->enable = MEA_FALSE;
    }

    
   


    /* build the ind_write value */
    ind_write.tableType = ENET_IF_CMD_PARAM_TBL_LOOP_SRCPORT_TO_CLUSTER;
    ind_write.tableOffset = port;
    ind_write.cmdReg = MEA_IF_CMD_REG;
    ind_write.cmdMask = MEA_IF_CMD_PARSER_MASK;
    ind_write.statusReg = MEA_IF_STATUS_REG;
    ind_write.statusMask = MEA_IF_STATUS_BUSY_MASK;
    ind_write.tableAddrReg = MEA_IF_CMD_PARAM_REG;
    ind_write.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_Src_to_Cluster_UpdateHW_Entry;


    ind_write.funcParam1 = (MEA_funcParam)unit;
    ind_write.funcParam2 = (MEA_funcParam)0;
    ind_write.funcParam3 = (MEA_funcParam)(long)len;/*len*/
    ind_write.funcParam4 = (MEA_funcParam)&val;

    /* Write to the Indirect Table */
    if (ENET_WriteIndirect(unit, &ind_write, ENET_MODULE_IF) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - ENET_WriteIndirect tbl=%d offset=%d mdl=%d failed \n",
            __FUNCTION__, ind_write.tableType, ind_write.tableOffset, ENET_MODULE_IF);
        return ENET_ERROR;
    }


    return MEA_OK;
}


MEA_Status mea_drv_Init_BFD_SRC_PORT(MEA_Unit_t       unit_i)
{
    MEA_Uint32 size;
    MEA_Port_t port;


    if (b_bist_test) {
        return MEA_OK;
    }
    if (!MEA_BFD_SUPPORT) {

        return MEA_OK;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize BFD SRC PORT tables ...\n");

    /* Allocate BONDING Table */
    size = (MEA_MAX_PORT_NUMBER+1) * sizeof(MEA_BFD_Src_port_Entry_dbt);
    if (size != 0) {
        MEA_BFD_SRC_PORT_info_Table = (MEA_BFD_Src_port_Entry_dbt*)MEA_OS_malloc(size);
        if (MEA_BFD_SRC_PORT_info_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_BFD_SRC_PORT_info_Table failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_BFD_SRC_PORT_info_Table[0]), 0, size);
    }
    /**/
   
    for (port = 0; port <= MEA_MAX_PORT_NUMBER; port++)
    {
       

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) == MEA_FALSE) {
            continue;
        }
        MEA_BFD_SRC_PORT_info_Table[port].valid = MEA_TRUE;

    }





    return MEA_OK;
}
MEA_Status mea_drv_RInit_BFD_SRC_PORT(MEA_Unit_t       unit_i)
{
    MEA_Uint16 id;
    if (!MEA_BFD_SUPPORT) {
        return MEA_OK;
    }

    for (id = 0; id <=MEA_MAX_PORT_NUMBER; id++) {
        if (MEA_BFD_SRC_PORT_info_Table[id].valid && 
            (MEA_BFD_SRC_PORT_info_Table[id].data.enable == MEA_TRUE))
        {
            
            if (mea_drv_SrcPort_to_Cluster_UpdateHW(unit_i,
                id,
                &(MEA_BFD_SRC_PORT_info_Table[id].data),
                NULL) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s -  mea_drv_SrcPort_to_Cluster_UpdateHW failed (id=%d)\n", __FUNCTION__, id);
                return MEA_ERROR;
            }
        }
    }


    return MEA_OK;
}

MEA_Status mea_drv_Conclude_BFD_SRC_PORT(MEA_Unit_t       unit_i)
{

    /* Free the table */
    if (!MEA_BFD_SUPPORT) {
        return MEA_OK;
    }
    if (MEA_BFD_SRC_PORT_info_Table != NULL) {
        MEA_OS_free(MEA_BFD_SRC_PORT_info_Table);
        MEA_BFD_SRC_PORT_info_Table = NULL;
    }

    return MEA_OK;
}



/************************************************************************/
/* for BFD loop ingress port to VP                                      */
/************************************************************************/
MEA_Status MEA_API_Set_SrcPort_to_Cluster_loop(MEA_Unit_t unit,
                                             MEA_Port_t port,
                                             MEA_BFD_src_dbt    *entry)
{
  

    if (!MEA_BFD_SUPPORT) {
        return MEA_ERROR;
    }

    if (port > MEA_MAX_PORT_NUMBER) 
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  (port=%d) > max of port %d\n", __FUNCTION__, port, MEA_MAX_PORT_NUMBER);
        return MEA_ERROR;
    }
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  entry == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE) == MEA_FALSE) {
        return MEA_ERROR;
    }



    if (mea_drv_SrcPort_to_Cluster_UpdateHW(unit,
        port,
        entry,
        &(MEA_BFD_SRC_PORT_info_Table[port].data)
        ) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  mea_drv_SrcPort_to_Cluster_UpdateHW failed (id=%d)\n", __FUNCTION__, port);
        return MEA_ERROR;
    }


    
    MEA_OS_memcpy(&MEA_BFD_SRC_PORT_info_Table[port].data, entry, sizeof(MEA_BFD_src_dbt));
     


    return MEA_OK;
}





MEA_Status MEA_API_Get_SrcPort_to_Cluster_loop(MEA_Unit_t unit,
                                            MEA_Port_t port,
                                            MEA_BFD_src_dbt    *entry) {

    if (!MEA_BFD_SUPPORT) {
        return MEA_ERROR;
    }
    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  entry == NULL\n", __FUNCTION__);
        return MEA_ERROR;
    }


    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE) == MEA_FALSE)
    {
        return MEA_ERROR;
    }

    
    MEA_OS_memcpy(entry, &MEA_BFD_SRC_PORT_info_Table[port].data, sizeof(MEA_BFD_src_dbt));


    return MEA_OK;
}





/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status mea_drv_Port_Remove_NVGRE_Header_Init(MEA_Unit_t unit) 
{
    MEA_DeviceInfo_dbt  entry_po;
    
    MEA_OS_memset(&entry_po, 0, sizeof(entry_po));

    MEA_OS_memset(&
    MEA_NVGRE_PORT_info_Table[0], 0, sizeof(MEA_NVGRE_PORT_info_Table));
    MEA_GlobalReg_Nvgre = 0;

    if ((!MEA_NVGRE_SUPPORT) && (!MEA_VXLAN_SUPPORT)) 
    {
        //MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " - MEA_NVGRE_SUPPORT not support \n");
        return MEA_OK;
    }

    /*Init the port */
    MEA_API_Get_DeviceInfo(unit, 0, &entry_po);

    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx0].valid = MEA_TRUE;
    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx0].bitMap = 0;
    
    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx1].valid = MEA_TRUE;
    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx1].bitMap = 1;

    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx2].valid = MEA_TRUE;
    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx2].bitMap = 2;

    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx3].valid = MEA_TRUE;
    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx3].bitMap = 3;

    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx4].valid = MEA_TRUE;
    MEA_NVGRE_PORT_info_Table[entry_po.nvgr_tunnel.val.portIndx4].bitMap = 4;
        
   
     mea_Global_TBL_offset_UpdateEntry(unit, MEA_GLOBAl_TBL_REG_NVGRE_TUNNEL_REMOVE, MEA_GlobalReg_Nvgre);
    


    return MEA_OK;
}

MEA_Status mea_drv_Port_Remove_NVGRE_Header_Renit(MEA_Unit_t unit)
{
    if (MEA_NVGRE_SUPPORT) {
        mea_Global_TBL_offset_UpdateEntry(unit, MEA_GLOBAl_TBL_REG_NVGRE_TUNNEL_REMOVE, MEA_GlobalReg_Nvgre);
    }
    return MEA_OK;
}

MEA_Bool MEA_API_NVGRE_Port_Remove_Header_IsValid(MEA_Unit_t unit, MEA_Port_t port)
{
    if ((!MEA_NVGRE_SUPPORT) && (!MEA_VXLAN_SUPPORT)) {
        return MEA_FALSE;
    }

    if (port > MEA_MAX_PORT_NUMBER)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  (port=%d) > max of port %d\n", __FUNCTION__, port, MEA_MAX_PORT_NUMBER);
        return MEA_FALSE;
    }

    if (MEA_NVGRE_PORT_info_Table[port].valid == MEA_TRUE) {
        return MEA_TRUE;
    }

    return MEA_FALSE;

}

MEA_Status MEA_API_NVGRE_Set_Port_Remove_Header(MEA_Unit_t unit,
    MEA_Port_t port, MEA_Bool enable )
{
    MEA_Uint32 val;

    if ((!MEA_NVGRE_SUPPORT) && (!MEA_VXLAN_SUPPORT)) {
        
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " - not support \n");
         return MEA_ERROR;
     }

    if (port > MEA_MAX_PORT_NUMBER)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  (port=%d) > max of port %d\n", __FUNCTION__, port, MEA_MAX_PORT_NUMBER);
        return MEA_ERROR;
    }

    if (MEA_NVGRE_PORT_info_Table[port].valid != MEA_TRUE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  (port=%d)  is not support for remove Header \n", __FUNCTION__, port);
        return MEA_ERROR;
    }

    /*Check if this port is valid to remove the NVGRE*/

    if (enable == MEA_TRUE) {
        MEA_GlobalReg_Nvgre |= (1 << (MEA_NVGRE_PORT_info_Table[port].bitMap));
    }
    else {
        val = 0;
        val = (1 << (MEA_NVGRE_PORT_info_Table[port].bitMap));
        val = (~val)  ;
        MEA_GlobalReg_Nvgre &= (val & 0x1F);  /**/

    }


    mea_Global_TBL_offset_UpdateEntry(unit, MEA_GLOBAl_TBL_REG_NVGRE_TUNNEL_REMOVE, MEA_GlobalReg_Nvgre);

    MEA_NVGRE_PORT_info_Table[port].remove_nvgre_header = enable;
   



    return MEA_OK;
}


MEA_Status MEA_API_NVGRE_Get_Port_Remove_Header(MEA_Unit_t unit,
    MEA_Port_t port, MEA_Bool *enable) 
{
    if ((!MEA_NVGRE_SUPPORT) && (!MEA_VXLAN_SUPPORT)) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, " - not support \n");
        return MEA_ERROR;
    }

    if (port > MEA_MAX_PORT_NUMBER)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  (port=%d) > max of port %d\n", __FUNCTION__, port, MEA_MAX_PORT_NUMBER);
        return MEA_ERROR;
    }
    if (enable == NULL)
    {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -  enable == NULL\n", __FUNCTION__);
        return MEA_ERROR; 
    }

    if (MEA_NVGRE_PORT_info_Table[port].valid)
        *enable = MEA_NVGRE_PORT_info_Table[port].remove_nvgre_header;
    else
        *enable = MEA_FALSE;

    return MEA_OK;

}