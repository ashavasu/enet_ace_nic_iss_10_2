/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "mea_if_service_drv.h"
#include "mea_api.h"
#include "MEA_platform.h"
#include "mea_port_drv.h"
#include "mea_drv_common.h"
#include "mea_bm_regs.h"
#include "mea_policer_drv.h"
#include "mea_policer_profile_drv.h"
#include "mea_init.h"
#include "mea_action_drv.h"
#include "mea_deviceInfo_drv.h"
//#include "mea_deviceInfo_drv.h"






/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_SRV_RATE_METERING_HW_ENABEL  MEA_TRUE
#define MEA_SRV_RATE_METERING_HW_DISABLE MEA_FALSE


#define  MEA_MAX_NUM_OF_HW_TM_ID(unit_i,hwUnit_i) ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_SW_LENGTH((unit_i),(hwUnit_i))
#define  MEA_POLICER_START_BUCKET_ONE 0
#define  MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit_i) \
     ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) \
     ? ( (mea_drv_Get_DeviceInfo_NumOfPolicers((unit_i),(hwUnit_i))< 2048 ) ? (mea_drv_Get_DeviceInfo_NumOfPolicers((unit_i),(hwUnit_i))*2) :(mea_drv_Get_DeviceInfo_NumOfPolicers((unit_i),(hwUnit_i)) )) \
      : (mea_drv_Get_DeviceInfo_NumOfPolicers((unit_i),(hwUnit_i))  ) )

#define MEA_DEBUG_SRV_POLICER 0



     

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/
typedef struct {
    MEA_Bool                  valid;
    mea_Ingress_Flow_policer_dbt  Ingress_Flow_policer;

}mea_Ingress_Flow_policer_Table_dbt;

static MEA_Bool                     MEA_Policer_InitDone              = MEA_FALSE;
static MEA_db_dbt                   MEA_Policer_Pointer_db            = NULL;
static MEA_db_dbt                   MEA_Policer_db                    = NULL;
static MEA_drv_policer_dbt         *MEA_Policer_Table                 = NULL;
static MEA_drv_policer_hwUnit_dbt  *MEA_Policer_hwUnit_Table          = NULL;

static mea_Ingress_Flow_policer_Table_dbt *MEA_Ingress_Flow_policer_Table   = NULL;



/*----------------------------------------------------------------------------*/
/*             Forward declaration                                            */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             local functions implementation                                 */
/*----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_policer_UpdateHwAction_BySwTmId>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_policer_UpdateHwAction_BySwTmId(MEA_Unit_t unit_i,
                                                          MEA_TmId_t tm_id_i) 
{

    MEA_Action_t actionId;
   // MEA_Bool show_title = MEA_TRUE;
    MEA_Action_HwEntry_dbt actionHwEntry;

    
    for(actionId=0;actionId< MEA_ACTION_SIZE_MAX_TBL ;actionId++)
    {

        if (mea_Action_GetHw(unit_i,actionId,&actionHwEntry) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_Action_GetHw failed (actionId=%d)\n",
                              __FUNCTION__,actionId);
            return MEA_ERROR;
        }

        if ((actionHwEntry.valid == MEA_TRUE              ) && 
            (actionHwEntry.tm_id == tm_id_i               )  ) {

            if (mea_Action_UpdateHw(unit_i,
                                    actionId,
                                    &actionHwEntry) != MEA_OK)   {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                     "%s - mea_Action_UpdateHw failed (actionId=%d)\n",
                                  __FUNCTION__,actionId);
                return MEA_ERROR;
            }
        }
    }

    return MEA_OK;
}


#if 0
void My_function_Checker(char *ch){

    MEA_ind_read_t ind_read;
    MEA_Uint32 read_data[2];


    ind_read.tableType        =   ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF;
    ind_read.tableOffset    =   0;
    ind_read.cmdReg            =    MEA_BM_IA_CMD;      
    ind_read.cmdMask        =    MEA_BM_IA_CMD_MASK;      
    ind_read.statusReg        =    MEA_BM_IA_STAT;   
    ind_read.statusMask        =    MEA_BM_IA_STAT_MASK;   
    ind_read.tableAddrReg    =    MEA_BM_IA_ADDR;
    ind_read.tableAddrMask    =    MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_read.read_data        =   read_data;
    
MEA_BM_HW_ACCESS_ENABLE
    if (MEA_API_ReadIndirect(MEA_UNIT_0,&ind_read,
                             2,
                             MEA_MODULE_BM)==MEA_ERROR) {
        return ;//MEA_ERROR;
    }
MEA_BM_HW_ACCESS_DISABLE
    
      
    if(read_data[0] == 0 && read_data[1]== 0){
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s table %d  [0] = 0x%08x [1] = 0x%08x",&ch,ind_read.tableType,read_data[0],read_data[1]);
    }
   

  
}
#endif





/* END Policer Profile -----------------------------------------------------------*/
/* Start bucket and Pointer--------------------------------------------------------*/
static void MEA_Policer_Bucket_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
   MEA_Unit_t              unit_i   = (MEA_Unit_t             )arg1;
   MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
   MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)((long)arg3);
   MEA_drv_policer_hw_dbt* entry_pi = (MEA_drv_policer_hw_dbt*)arg4;
   
   MEA_Uint32             val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
   MEA_Uint32             i;
   MEA_Uint16             num_of_regs;
   MEA_Uint32             count_shift;
   


    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
                             ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_NUM_OF_REGS(hwUnit_i));
    for (i=0;i<num_of_regs;i++) {
        val[i] = 0;
    }

count_shift=0;


    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_BUCKET_CIR_WIDTH,
                            entry_pi->cbs_ebs,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_BUCKET_CIR_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_BUCKET_GN_TYPE_WIDTH,
                            (MEA_Uint32)(entry_pi->gn_type) ,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_BUCKET_GN_TYPE_WIDTH;

    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_BUCKET_GN_SIGN_WIDTH,
                            (MEA_Uint32)(entry_pi->gn_sign) ,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_BUCKET_GN_SIGN_WIDTH;

    
        MEA_OS_insert_value(count_shift,
        MEA_SRV_RATE_METER_BUCKET_BAG_WIDTH,
        (MEA_Uint32)(entry_pi->type_mode) ,
        &val[0]);
       count_shift+=MEA_SRV_RATE_METER_BUCKET_BAG_WIDTH;

       MEA_OS_insert_value(count_shift,
           MEA_SRV_RATE_METER_BUCKET_LMAX_WIDTH,
           (MEA_Uint32)(entry_pi->Lmax>>4) , //Lmax
           &val[0]);
       count_shift+=MEA_SRV_RATE_METER_BUCKET_LMAX_WIDTH;




    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_BUCKET_RESERV_WIDTH,
                            (MEA_Uint32)(0) ,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_BUCKET_RESERV_WIDTH;
    
   
     
    

    MEA_OS_insert_value(count_shift,
                            MEA_SRV_RATE_METER_BUCKET_ENABL_WIDTH,
                            ((entry_pi->rateEnable == MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL) ?  MEA_SRV_RATE_METERING_HW_ENABEL : MEA_SRV_RATE_METERING_HW_DISABLE ) ,
                            &val[0]);
    count_shift+=MEA_SRV_RATE_METER_BUCKET_ENABL_WIDTH;
    
    
   

    for (i=0;i<num_of_regs;i++) {
         //MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"BUCKET [%d]=0x%08x  \n",i,val[i]);
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }

   if ((entry_pi->mode  == MEA_POLICER_MODE_SINGLE     ) ||
       (hwId_i           <  (MEA_db_HwId_t)MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit_i))  ) {
      if (mea_db_Set_Hw_Regs(unit_i,
                           MEA_Policer_db,
                           hwUnit_i,
                           hwId_i,
                           num_of_regs,
                           val) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                             __FUNCTION__,
                             hwUnit_i,
                             hwId_i);
      }
   }


}

static MEA_Status MEA_Policer_Bucket_UpdateHw(MEA_Unit_t unit_i,
                                              MEA_TmId_t sw_tmId_i,
                                              MEA_TmId_t hw_tmId_i,
                                              MEA_drv_policer_hw_dbt* hwEntry_pi) 
{
      
    MEA_ind_write_t ind_write;     
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwUnit_t hwId;

    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

    hwId = (MEA_db_HwId_t)hw_tmId_i;



    
    ind_write.tableType        = ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET;    
    ind_write.tableOffset   = hw_tmId_i;
    ind_write.cmdReg        = MEA_BM_IA_CMD;      
    ind_write.cmdMask        = MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg        = MEA_BM_IA_STAT;   
    ind_write.statusMask    = MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg    = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask    = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry     =  (MEA_FUNCPTR)MEA_Policer_Bucket_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)hwId);
    ind_write.funcParam4 = (MEA_funcParam)hwEntry_pi;

if (MEA_IS_POLICER_SUPPORT){
    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect failed "
                          "TableType=%d , TableOffset=%d , mea_module=%d\n",
                          __FUNCTION__,
                          ind_write.tableType,
                          ind_write.tableOffset,
                          MEA_MODULE_BM);
         return MEA_ERROR;
    }  
}
    return MEA_OK;
}

void MEA_Policer_ProfPointer_UpdateHwEntry(MEA_funcParam arg1,
    MEA_funcParam arg2,
    MEA_funcParam arg3,
    MEA_funcParam arg4)
{
    MEA_Unit_t              unit_i   = (MEA_Unit_t             )arg1;
    MEA_db_HwUnit_t         hwUnit_i = (MEA_db_HwUnit_t)((long)arg2);
    MEA_db_HwId_t           hwId_i = (MEA_db_HwId_t)((long)arg3);
    MEA_drv_policer_hw_dbt* entry_pi = (MEA_drv_policer_hw_dbt*)arg4;
//    MEA_Uint32         temp;
    MEA_db_HwId_t      prof_hwId;
    MEA_Uint32         val[MEA_BM_IA_MAX_NUM_OF_WRITE_REGS];
    MEA_Uint32         i;
    MEA_Uint16         num_of_regs;
    MEA_Uint32       count_shift;
    MEA_Uint32       numofbit;

    num_of_regs = MEA_OS_MIN(MEA_NUM_OF_ELEMENTS(val),
                             ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT_NUM_OF_REGS(hwUnit_i));
    for (i=0;i<num_of_regs;i++) {
        val[i] = 0;
    }

    /* Build register 0 */
    if (mea_drv_Policer_Profile_GetHwProfId (unit_i,
                                             hwUnit_i,
                                             entry_pi->prof_id,
                                             &prof_hwId) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - failed " 
                          "(ACM_Mode=%d,id=%d,hwUnit=%d)\n",
                          __FUNCTION__,0,entry_pi->prof_id,hwUnit_i);
        return;
    }


    count_shift=0;
    numofbit = MEA_OS_calc_Number_of_bits(MEA_POLICER_MAX_PROF-1);
    MEA_OS_insert_value(count_shift,
          numofbit,
        (MEA_Uint32)prof_hwId,
        &val[0]);



    for (i=0;i<num_of_regs;i++) {
        MEA_API_WriteReg(unit_i,MEA_BM_IA_WRDATA(i),val[i],MEA_MODULE_BM);
    }

   if ((entry_pi->mode  == MEA_POLICER_MODE_SINGLE     ) ||
       (hwId_i           <  MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit_i))  ) {
      if (mea_db_Set_Hw_Regs(unit_i,
                           MEA_Policer_Pointer_db,
                           hwUnit_i,
                           hwId_i,
                           num_of_regs,
                           val) != MEA_OK) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - mea_db_Set_Hw_Regs failed (unit=%d,hwId=%d)\n",
                             __FUNCTION__,
                             hwUnit_i,
                             hwId_i);
      }
   }







}

MEA_Status MEA_Policer_ProfPointer_UpdateHw(MEA_Unit_t         unit_i,
                                            MEA_TmId_t         sw_tmId_i ,
                                            MEA_TmId_t         hw_tmId_i , 
                                            MEA_drv_policer_hw_dbt* entry_pi) {


    MEA_ind_write_t ind_write;     
    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   hwId;

    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);
    hwId = hw_tmId_i;
    
    ind_write.tableType        = ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT;    
    ind_write.tableOffset   = hw_tmId_i;
    ind_write.cmdReg        = MEA_BM_IA_CMD;      
    ind_write.cmdMask        = MEA_BM_IA_CMD_MASK;      
    ind_write.statusReg        = MEA_BM_IA_STAT;   
    ind_write.statusMask    = MEA_BM_IA_STAT_MASK;   
    ind_write.tableAddrReg    = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask    = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry    = (MEA_FUNCPTR)MEA_Policer_ProfPointer_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)unit_i;
    ind_write.funcParam2 = (MEA_funcParam)((long)hwUnit);
    ind_write.funcParam3 = (MEA_funcParam)((long)hwId);
    ind_write.funcParam4 = (MEA_funcParam)entry_pi;
    

if (MEA_IS_POLICER_SUPPORT){
    if (MEA_API_WriteIndirect(MEA_UNIT_0,&ind_write,MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_API_WriteIndirect failed "
                          "TableType=%d , TableOffset=%d , mea_module=%d\n",
                          __FUNCTION__,
                          ind_write.tableType,
                          ind_write.tableOffset,
                          MEA_MODULE_BM);
         return MEA_ERROR;
    }
}

    return MEA_OK;
}




/*END bucket*/
/* Start */
static MEA_Bool MEA_PolicerHW_IsTmIdInRange(MEA_Unit_t unit_i,
                                            MEA_TmId_t Hwid){

    MEA_db_HwUnit_t  hwUnit;

    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_FALSE);
    
    if(Hwid<MEA_MAX_NUM_OF_HW_TM_ID(unit_i,hwUnit)) //<= -1
        return MEA_TRUE;
    else
        return MEA_FALSE;
}
MEA_Status MEA_PolicerHW_Tmid_GetFreeTmid(MEA_Unit_t unit_i,
                                          MEA_Policer_prof_t profId ,MEA_TmId_t *Hwid_o){

    MEA_Uint32 i,startindex,endindex;
    MEA_Bool flag_fast_service=MEA_FALSE;
    MEA_Bool dual_Flag=MEA_FALSE;
    MEA_Policer_Entry_dbt policer_entry;
    MEA_Policer_Prof_dbt entry_i;
    MEA_db_HwUnit_t  hwUnit;
    MEA_uint64 cir_eir_slowVal;

    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);
  
    MEA_OS_memset(&entry_i,0,sizeof(MEA_Policer_Prof_dbt));
    MEA_OS_memset(&policer_entry,0,sizeof(policer_entry));

    if(mea_drv_Policer_Profile_Get(MEA_UNIT_0,0,profId,&entry_i)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - mea_drv_Policer_Profile_Get failed \n",__FUNCTION__);
        return MEA_ERROR;
    }
    
    MEA_OS_memcpy(&policer_entry,&entry_i.rateMeter,sizeof(policer_entry));
    /* get the relevant tick value according to the service type (normal/fast) */
    cir_eir_slowVal= (MEA_uint64)(MEA_POLICER_CIR_EIR_SLOW_MAX_VAL((policer_entry.gn_sign),(policer_entry.gn_type)));

    if(policer_entry.gn_type == 0){
    if(((policer_entry.CIR) > cir_eir_slowVal ) || 
       ((policer_entry.EIR) > cir_eir_slowVal ) ){
        flag_fast_service=MEA_TRUE;
        startindex=0;
        endindex=MEA_POLICER_LAST_FAST_SERVICE; /*for fast poicer*/
    } else {
        flag_fast_service=MEA_FALSE;
        startindex=MEA_POLICER_LAST_FAST_SERVICE+1;
        endindex=MEA_MAX_NUM_OF_HW_TM_ID(unit_i,hwUnit); /*for slow poicer*/ //-1
    }
    }else{
        flag_fast_service=MEA_FALSE;
        startindex=MEA_POLICER_LAST_FAST_SERVICE+1;
        endindex=MEA_MAX_NUM_OF_HW_TM_ID(unit_i,hwUnit); /*for slow poicer*/ //-1
    }


#if 0  
  /*check if is singal or dual*/
    if(((MEA_Uint32)(policer_entry.EIR)!= 0)){
        dual_Flag=MEA_TRUE;
    } else {
        dual_Flag=MEA_FALSE; 
    }
#else
    /*Alex   21/10/07 */ 
    dual_Flag=MEA_TRUE;
#endif

    if(*Hwid_o == MEA_PLAT_GENERATE_NEW_ID){
    for(i=startindex;i<=endindex ;i++) {
        if(dual_Flag){
           if (MEA_Policer_hwUnit_Table[hwUnit].table[i].valid == MEA_FALSE && 
               MEA_Policer_hwUnit_Table[hwUnit].table[i +MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid == MEA_FALSE){
               *Hwid_o = (MEA_TmId_t)i;
               MEA_Policer_hwUnit_Table[hwUnit].table[i].mode  = MEA_POLICER_MODE_DUAL;
               MEA_Policer_hwUnit_Table[hwUnit].table[i].valid=MEA_TRUE;
              
               MEA_Policer_hwUnit_Table[hwUnit].table[i+ MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid=MEA_TRUE; 
               MEA_Policer_hwUnit_Table[hwUnit].table[i+ MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].mode  = MEA_POLICER_MODE_DUAL;
               return MEA_OK;
           }
        } else {
            /*signal */
            if(MEA_Policer_hwUnit_Table[hwUnit].table[i].valid == MEA_FALSE){
                *Hwid_o= (MEA_TmId_t) i;
                MEA_Policer_hwUnit_Table[hwUnit].table[i].mode  = MEA_POLICER_MODE_SINGLE;
                MEA_Policer_hwUnit_Table[hwUnit].table[i].valid = MEA_TRUE;
               return MEA_OK;
            } else{
                if(MEA_Policer_hwUnit_Table[hwUnit].table[i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid == MEA_FALSE){
                    *Hwid_o=(MEA_TmId_t) i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit);
                    MEA_Policer_hwUnit_Table[hwUnit].table[*Hwid_o].mode  = MEA_POLICER_MODE_SINGLE;
                    MEA_Policer_hwUnit_Table[hwUnit].table[*Hwid_o].valid=MEA_TRUE;
                    return MEA_OK;
                }
            }
        }
  
    }
    }else{
        if(*Hwid_o > MEA_MAX_NUM_OF_HW_TM_ID(unit_i,hwUnit) ){ // -1
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The Hw police is out of range ");
            return MEA_ERROR;
        }
        if(dual_Flag){
            if (MEA_Policer_hwUnit_Table[hwUnit].table[*Hwid_o].valid == MEA_FALSE && 
                MEA_Policer_hwUnit_Table[hwUnit].table[*Hwid_o + MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid == MEA_FALSE){
                    //*Hwid_o = (MEA_TmId_t)i;
                     MEA_Policer_hwUnit_Table[hwUnit].table[*Hwid_o].mode  = MEA_POLICER_MODE_DUAL;
                    MEA_Policer_hwUnit_Table[hwUnit].table[*Hwid_o].valid=MEA_TRUE;
                    MEA_Policer_hwUnit_Table[hwUnit].table[*Hwid_o + MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid=MEA_TRUE;

                    return MEA_OK;
           }else{
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - *Hwid_o %d is not free \n",__FUNCTION__,*Hwid_o);
                  return MEA_ERROR;
            }

        }
    }

   
    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - GetFreeTmid no more free %s policer\n",__FUNCTION__,(flag_fast_service == MEA_TRUE) ? "FAST" : "SLOW");

 return MEA_ERROR;

}





MEA_Status MEA_PolicerHW_Tmid_Set(MEA_Unit_t unit_i,
                                  MEA_TmId_t tm_id,
                                  MEA_TmId_t         Hwid_i,
                                  MEA_Policer_prof_t profId_i,
                                  MEA_Bool           rateEnable_i) 
{
      
   MEA_Policer_Entry_dbt policer_entry;
   MEA_Policer_Prof_dbt   entry;
   MEA_db_HwUnit_t hwUnit;

   MEA_DRV_GET_HW_UNIT(unit_i,
                       hwUnit,
                       return MEA_ERROR);

   if (MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].valid == MEA_FALSE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - id %d not exist \n",
                              __FUNCTION__,Hwid_i);
            return MEA_ERROR;
   }

    MEA_OS_memset(&entry,0,sizeof(MEA_Policer_Prof_dbt));
    MEA_OS_memset(&policer_entry,0,sizeof(policer_entry));

   
   if(mea_drv_Policer_Profile_Get(MEA_UNIT_0,0,profId_i,&entry)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Profile_Delete_Get failed (profId=%d)\n",
                          __FUNCTION__,profId_i);
        return MEA_ERROR;
    }
    MEA_OS_memcpy(&policer_entry,&entry.rateMeter,sizeof(policer_entry));

    MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].prof_id    =profId_i;
    MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].rateEnable = rateEnable_i;  
    //MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].cbs_ebs    = policer_entry.CBS;
    MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].cbs_ebs= entry.Calculat.CBS;
    MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].Lmax   = entry.Calculat.Lmax;

    MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].comp       = (MEA_Uint16)policer_entry.comp;
    MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].gn_type    =(MEA_Uint16)policer_entry.gn_type;
    MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].gn_sign    =(MEA_Uint16)policer_entry.gn_sign;
    MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].type_mode  = policer_entry.type_mode;
    
    
    if(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i].mode == MEA_POLICER_MODE_SINGLE){
        if(MEA_Policer_ProfPointer_UpdateHw(unit_i,
                                            tm_id,
                                            Hwid_i,
                                            &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i])
                                            )!=MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_Policer_ProfPointer_UpdateHw failed \n",__FUNCTION__);
           return MEA_ERROR;
        }
        if(MEA_Policer_Bucket_UpdateHw(unit_i,
                                       tm_id,
                                       Hwid_i,
                                       &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i])) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_Policer_Bucket_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }
    }else {
        /*MEA_POLICER_DUAL*/
        if(MEA_Policer_ProfPointer_UpdateHw(unit_i,
                                            tm_id,
                                            Hwid_i,
                                            &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i])
                                            )!=MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_Policer_ProfPointer_UpdateHw failed \n",__FUNCTION__);
           return MEA_ERROR;
        }
        if(MEA_Policer_Bucket_UpdateHw(unit_i,
                                       tm_id,
                                       Hwid_i,
                                       &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i])) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_Policer_Bucket_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }
        MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].prof_id=profId_i;
        MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].rateEnable=rateEnable_i;
        MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].mode = MEA_POLICER_MODE_DUAL;
        //MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].cbs_ebs=policer_entry.EBS;
       MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].cbs_ebs=entry.Calculat.EBS;
       MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].Lmax   = entry.Calculat.Lmax;// no need
       
        MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].comp=(MEA_Uint16)policer_entry.comp;
        MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].gn_type=(MEA_Uint16)policer_entry.gn_type;
        MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].gn_sign=(MEA_Uint16)policer_entry.gn_sign;
        MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].type_mode   = policer_entry.type_mode; 

        if(MEA_Policer_ProfPointer_UpdateHw(unit_i,
                                            tm_id,
                                            (MEA_TmId_t)(Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)),
                                            &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)])
                                            )!=MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_Policer_ProfPointer_UpdateHw failed \n",__FUNCTION__);
           return MEA_ERROR;
        }
        if(MEA_Policer_Bucket_UpdateHw(unit_i,
                                       tm_id,
                                       (MEA_TmId_t)(Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)),
                                       &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid_i+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)])) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - MEA_Policer_Bucket_UpdateHw failed \n",__FUNCTION__);
            return MEA_ERROR;
        }
        
    }

    return MEA_OK;
}

MEA_Status MEA_PolicerHW_Tmid_Create(MEA_Unit_t         unit_i,
                                     MEA_TmId_t         tm_id,
                                     MEA_Policer_prof_t profId_i,
                                     MEA_Bool           rateEnable_i,
                                     MEA_Bool           forceHwId_i,
                                     MEA_TmId_t        *Hwid_io)
{

    MEA_db_HwUnit_t hwUnit;
    MEA_db_HwId_t   db_hwId;

    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

   if(forceHwId_i == MEA_FALSE){
       if(MEA_PolicerHW_Tmid_GetFreeTmid(unit_i,profId_i,Hwid_io) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_PolicerHW_Tmid_GetFreeTmid failed \n",
                              __FUNCTION__);
          return MEA_ERROR;
        }

        db_hwId = *Hwid_io;
        if (mea_db_Create(unit_i,
                          MEA_Policer_db,
                          (MEA_db_SwId_t)tm_id,
                          hwUnit,
                          &db_hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - mea_db_Create Policer failed (hwUnit=%d,swId=%d)\n",
                                __FUNCTION__,hwUnit,tm_id);
            MEA_Policer_hwUnit_Table[hwUnit].table[(*Hwid_io)].valid  = MEA_FALSE;
            if (MEA_Policer_hwUnit_Table[hwUnit].table[(*Hwid_io)].mode == MEA_POLICER_MODE_DUAL) {
                MEA_Policer_hwUnit_Table[hwUnit].table[(*Hwid_io)+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid=MEA_FALSE;
            }
            return MEA_ERROR;
        }

        db_hwId = *Hwid_io;
        if (mea_db_Create(unit_i,
                          MEA_Policer_Pointer_db,
                          (MEA_db_SwId_t)tm_id,
                          hwUnit,
                          &db_hwId) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Create Policer_Pointer failed (hwUnit=%d,swId=%d)\n",
                               __FUNCTION__,hwUnit,tm_id);
            mea_db_Delete(unit_i,MEA_Policer_db,(MEA_db_SwId_t)tm_id,hwUnit);
            MEA_Policer_hwUnit_Table[hwUnit].table[(*Hwid_io)].valid  = MEA_FALSE;
            if (MEA_Policer_hwUnit_Table[hwUnit].table[(*Hwid_io)].mode == MEA_POLICER_MODE_DUAL) {
                MEA_Policer_hwUnit_Table[hwUnit].table[(*Hwid_io)+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid=MEA_FALSE;
            }
            return MEA_ERROR;
        }
    }

    if (MEA_PolicerHW_Tmid_Set(unit_i,
                               tm_id,
                               (*Hwid_io),
                               profId_i,
                               rateEnable_i) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_PolicerHW_Tmid_Set failed (id=%d) \n",
                          __FUNCTION__,(*Hwid_io));
        if (forceHwId_i == MEA_FALSE){
             mea_db_Delete(unit_i,MEA_Policer_db,(MEA_db_SwId_t)tm_id,hwUnit);
             mea_db_Delete(unit_i,MEA_Policer_Pointer_db,(MEA_db_SwId_t)tm_id,hwUnit);
            MEA_Policer_hwUnit_Table[hwUnit].table[(*Hwid_io)].valid  = MEA_FALSE;
            if (MEA_Policer_hwUnit_Table[hwUnit].table[(*Hwid_io)].mode == MEA_POLICER_MODE_DUAL) {
                MEA_Policer_hwUnit_Table[hwUnit].table[(*Hwid_io)+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid=MEA_FALSE;
            }
        }
        return MEA_ERROR;
    }


    return MEA_OK;
}


MEA_Status MEA_PolicerHW_Tmid_Delete(MEA_Unit_t unit_i,
                                     MEA_TmId_t tm_id,
                                     MEA_TmId_t Hwid){

    MEA_db_HwUnit_t hwUnit;

    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                          return MEA_ERROR);

   /*chack Hwid_o is inrange*/
    if (MEA_PolicerHW_IsTmIdInRange(unit_i,Hwid)==MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,Hwid);
       return MEA_ERROR;
    }

    if(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].valid==MEA_FALSE){
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,Hwid);
       return MEA_ERROR;
    }

    /**/
    if(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].mode == MEA_POLICER_MODE_SINGLE){
         MEA_OS_memset(&MEA_Policer_hwUnit_Table[hwUnit].table[Hwid],0,sizeof(MEA_drv_policer_hw_dbt));
         MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].valid=MEA_FALSE;
         MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].mode=MEA_POLICER_MODE_SINGLE;
         MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].prof_id = mea_drv_Policer_Profile_GetDisableProfId(hwUnit);
         if (MEA_Policer_ProfPointer_UpdateHw(unit_i,
                                               tm_id,
                                              Hwid,
                                              &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid])
                                              )!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - fail to update  MEA_Policer_ProfPointer_UpdateHw id %d \n",
                             __FUNCTION__,Hwid);
           return MEA_ERROR;
        }
        
        if(MEA_Policer_Bucket_UpdateHw(unit_i,
                                       tm_id,
                                       Hwid,
                                       &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid])
                                       ) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - fail to update  MEA_Policer_ProfPointer_UpdateHw id %d \n",
                             __FUNCTION__,Hwid);
           return MEA_ERROR;
        }
    } else {
            MEA_OS_memset(&MEA_Policer_hwUnit_Table[hwUnit].table[Hwid],0,sizeof(MEA_drv_policer_hw_dbt));
            MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].valid=MEA_FALSE;
            MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].mode=MEA_POLICER_MODE_DUAL;
            MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].prof_id = mea_drv_Policer_Profile_GetDisableProfId(hwUnit);
            if (MEA_Policer_ProfPointer_UpdateHw(unit_i,
                                            tm_id,
                                            Hwid,
                                            &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid])
                                            )!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - fail to update  MEA_Policer_ProfPointer_UpdateHw id %d \n",
                             __FUNCTION__,Hwid);
                return MEA_ERROR;
            }
        
            if(MEA_Policer_Bucket_UpdateHw(unit_i,
                                           tm_id,
                                           Hwid,
                                            &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid])
                                             ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - fail to update  MEA_Policer_ProfPointer_UpdateHw id %d \n",
                             __FUNCTION__,Hwid);
                return MEA_ERROR;
            }
            
            MEA_OS_memset(&MEA_Policer_hwUnit_Table[hwUnit].table[Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)],0,sizeof(MEA_drv_policer_hw_dbt));
            MEA_Policer_hwUnit_Table[hwUnit].table[Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid=MEA_FALSE;
            MEA_Policer_hwUnit_Table[hwUnit].table[Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].mode=MEA_POLICER_MODE_DUAL;
            MEA_Policer_hwUnit_Table[hwUnit].table[Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].prof_id = mea_drv_Policer_Profile_GetDisableProfId(hwUnit);
            if (MEA_Policer_ProfPointer_UpdateHw(unit_i,
                                            tm_id,
                                            (MEA_TmId_t)(Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)),
                                            &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)])
                                            )!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - fail to update  MEA_Policer_ProfPointer_UpdateHw id %d \n",
                             __FUNCTION__,Hwid);
                return MEA_ERROR;
            }
        
            if(MEA_Policer_Bucket_UpdateHw(unit_i,
                                           tm_id,
                                           (MEA_TmId_t)(Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)),
                                            &(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)])
                                             ) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - fail to update  MEA_Policer_ProfPointer_UpdateHw id %d \n",
                             __FUNCTION__,Hwid);
                return MEA_ERROR;
            }
    
    }

                                     
   
    return MEA_OK;
}








MEA_Status MEA_PolicerHW_Tmid_GetInfo(MEA_Unit_t unit_i,
                                      MEA_db_HwUnit_t hwUnit,
                                      MEA_TmId_t Hwid,
                                      MEA_drv_policer_hw_dbt *entry,
                                      MEA_Bool *found){
 

    if(entry == NULL || found == NULL){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry or found== NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }
    if(Hwid > MEA_MAX_NUM_OF_HW_TM_ID(unit_i,hwUnit)){ //>=
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - HwId (%d) > max value (%d) \n",__FUNCTION__,Hwid,MEA_MAX_NUM_OF_HW_TM_ID(unit_i,hwUnit));
        return MEA_ERROR;
    }
    if(MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].valid==MEA_FALSE){
        *found=MEA_FALSE;
        return MEA_OK;
    } else {
    MEA_OS_memcpy(entry,&MEA_Policer_hwUnit_Table[hwUnit].table[Hwid],sizeof(MEA_drv_policer_hw_dbt));
    *found=MEA_TRUE;
    return MEA_OK;
    }
    
    return MEA_OK;
}



//MEA_Policer_HW_Table
/* End */
static MEA_Bool mea_drv_IsTmIdInRange(MEA_TmId_t tmId)
{

   if(tmId>=1 && tmId<MEA_MAX_NUM_OF_TM_ID)
       return MEA_TRUE;
   else
    return MEA_FALSE;        

}

MEA_Status mea_drv_policer_Tmid_Valid(MEA_Unit_t unit_i,
                                      MEA_TmId_t id){
    if (mea_drv_IsTmIdInRange(id) != MEA_TRUE) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    if ( MEA_Policer_Table[id].valid == MEA_FALSE )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - none existing Policer entry with ID %d\n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    } else {
        return MEA_OK;
    }

}

MEA_Status mea_drv_policer_add_owner(MEA_Unit_t unit_i,
                                     MEA_TmId_t id)
{
    if (mea_drv_IsTmIdInRange(id) != MEA_TRUE) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    if ( MEA_Policer_Table[id].valid == MEA_FALSE )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - none existing Policer entry with ID %d\n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    MEA_Policer_Table[id].num_of_owners++;

    return MEA_OK;
}


static MEA_Status mea_drv_policer_delete_owner(MEA_Unit_t unit_i,
                                               MEA_TmId_t id)
{
    MEA_db_HwUnit_t       hwUnit;

    MEA_DRV_GET_HW_UNIT(unit_i,hwUnit,return MEA_ERROR;)

    if (mea_drv_IsTmIdInRange(id) != MEA_TRUE) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    if ( MEA_Policer_Table[id].valid == MEA_FALSE )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - none existing Policer entry with ID %d\n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    MEA_Policer_Table[id].num_of_owners--;
    
    

    if ( MEA_Policer_Table[id].num_of_owners == 0 )
    {
        /*Delete HW*/
        if (MEA_PolicerHW_Tmid_Delete(unit_i,id,MEA_Policer_Table[id].hwTmid)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Can't Delete HWTnid ID %d\n",
                         __FUNCTION__,MEA_Policer_Table[id].hwTmid);
            return MEA_ERROR;
        }
        /*Delete profile policer*/
        if(mea_drv_Policer_Profile_Delete(unit_i,0,MEA_Policer_Table[id].prof_id)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Can't Delete policer profile ID %d\n",
                         __FUNCTION__,MEA_Policer_Table[id].hwTmid);
            return MEA_ERROR;
        }

        MEA_OS_memset(&(MEA_Policer_Table[id]), 0, sizeof(MEA_drv_policer_dbt));
        MEA_Policer_Table[id].valid=MEA_FALSE;

        if (mea_db_Delete(unit_i,
                          MEA_Policer_db,
                          (MEA_db_SwId_t)id,
                          hwUnit) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Delete Policer failed (hwUnit=%d,swId=%d)\n",
                              __FUNCTION__,hwUnit,id);
            return MEA_ERROR;
        }
        if (mea_db_Delete(unit_i,
                          MEA_Policer_Pointer_db,
                          (MEA_db_SwId_t)id,
                          hwUnit) != MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_db_Delete Policer_Pointer failed (hwUnit=%d,swId=%d)\n",
                              __FUNCTION__,hwUnit,id);
            return MEA_ERROR;
        }
    }


    return MEA_OK;
}

 

static MEA_Status mea_drv_get_free_policer_id(MEA_TmId_t *id_o,MEA_Bool fast_slow, MEA_Policer_Entry_dbt *entry_i)
{
    
    MEA_TmId_t start;
    MEA_TmId_t end;


    if(*id_o == 0) { 
           if(fast_slow == MEA_TRUE){
               start=1;
               end= 15;
           }else{
               start=16;
               end= MEA_MAX_NUM_OF_TM_ID-1; //
           }

        for ( *id_o = start ; *id_o <= end; (*id_o)++ )
        {
            if ( MEA_Policer_Table[*id_o].valid == MEA_FALSE )
            {
            MEA_Policer_Table[*id_o].valid = MEA_TRUE;
            MEA_Policer_Table[*id_o].num_of_owners=1;
            return MEA_OK;
            }
        }
    } else {
        if(*id_o > MEA_MAX_NUM_OF_TM_ID){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - Tm id  %d out of range \n",
                      __FUNCTION__,*id_o);
          return MEA_ERROR;
        }

        if(fast_slow==MEA_TRUE){
            if(*id_o > 15){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Tm id  %d is for slow policer \n",
                    __FUNCTION__,*id_o);
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "CIR/EIR < %llu \n", (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_MAX_VAL((entry_i->gn_sign), (entry_i->gn_type)));

                return MEA_ERROR;
            }
        }else {
            if(*id_o < 16){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - Tm id  %d is for fast policer \n",
                    __FUNCTION__,*id_o);
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"CIR/EIR > %llu \n", (MEA_uint64)MEA_SRV_POLICER_CIR_EIR_SLOW_MAX_VAL((entry_i->gn_sign), (entry_i->gn_type)));

                return MEA_ERROR;
            }

        }


        if ( MEA_Policer_Table[*id_o].valid == MEA_FALSE )
        {
            MEA_Policer_Table[*id_o].valid = MEA_TRUE;
            MEA_Policer_Table[*id_o].num_of_owners=1;
            return MEA_OK;
         }

    }
    
   

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - no free Policer entry \n",
                      __FUNCTION__);
    return MEA_ERROR;
}

MEA_Status MEA_PolicerSW_Tmid_GetInfo(MEA_Unit_t unit_i,
                                      MEA_TmId_t          sw_tmid_i,
                                      MEA_drv_policer_dbt *entry_po,
                                      MEA_Bool *found_o)
{

    if (found_o) {
        *found_o=MEA_FALSE;
    }


    if (mea_drv_IsTmIdInRange(sw_tmid_i) != MEA_TRUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,sw_tmid_i);
       return MEA_ERROR;  
    }
    if ( MEA_Policer_Table[sw_tmid_i].valid == MEA_TRUE ) {
        if (entry_po) {
            MEA_OS_memcpy(entry_po,
                          &(MEA_Policer_Table[sw_tmid_i]),
                          sizeof(*entry_po));
        }
        if (found_o) {
            *found_o=MEA_TRUE;
        }
    }


    return MEA_OK;
}
/*----------------------------------------------------------------------------*/
/*             MEA driver private functions implementation                    */
/*----------------------------------------------------------------------------*/

MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Policer_num_of_sw_size_func,
                                         MEA_MAX_NUM_OF_TM_ID,
                                         0)

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Policer_num_of_hw_size_func,
                                         ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_LENGTH(hwUnit_i),
                                         ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_NUM_OF_REGS(hwUnit_i))

MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_Policer_db,
                             MEA_Policer_db) 

MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Policer_Pointer_num_of_sw_size_func,
                                         MEA_MAX_NUM_OF_TM_ID,
                                         0)

MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(mea_drv_Get_Policer_Pointer_num_of_hw_size_func,
                                         ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT_LENGTH(hwUnit_i),
                                         ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT_NUM_OF_REGS(hwUnit_i))

MEA_DB_GET_DB_FUNC_IMPLEMENT(mea_drv_Get_Policer_Pointer_db,
                             MEA_Policer_Pointer_db) 

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Conclude_Policer>                                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Conclude_Policer(MEA_Unit_t unit_i) {

    MEA_db_HwUnit_t       hwUnit;


    if (!MEA_Policer_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - not Init \n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Policer Table ...\n");

    if (MEA_Policer_hwUnit_Table != NULL) {
        for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
            if (MEA_Policer_hwUnit_Table[hwUnit].table != NULL) {
                MEA_OS_free(MEA_Policer_hwUnit_Table[hwUnit].table);
                MEA_Policer_hwUnit_Table[hwUnit].table = NULL;
                MEA_Policer_hwUnit_Table[hwUnit].num_of_entries = 0;
            }
        }
        MEA_OS_free(MEA_Policer_hwUnit_Table);
        MEA_Policer_hwUnit_Table = NULL;
    }

    if (MEA_Policer_Table != NULL) {
         MEA_OS_free(MEA_Policer_Table);
         MEA_Policer_Table = NULL;
    }

    if (mea_db_Conclude(unit_i,&MEA_Policer_Pointer_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_dn_Conclude (Policer_Pointer) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (mea_db_Conclude(unit_i,&MEA_Policer_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_dn_Conclude (Policer) failed \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Conclude Profile Policer ...\n");
    mea_drv_Policer_Profile_Conclude(unit_i);



    MEA_Policer_InitDone = MEA_FALSE;

    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_Init_Policer>                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_Policer(MEA_Unit_t unit_i) {
    
    MEA_db_HwUnit_t       hwUnit;
    MEA_Uint32            size;     

    if (MEA_Policer_InitDone) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Already Init \n",__FUNCTION__);
        return MEA_ERROR;
    }

    MEA_Policer_InitDone = MEA_FALSE;


     
     /*create profile with all Zero for service Disable*/
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Profile Policer ...\n");
     if(mea_drv_Policer_Profile_Init(unit_i) !=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"!!!!!! Init Profile Policer Failed !!!!!!\n");
        return MEA_ERROR;
     }

     /*init the HWtmid policer profile*/
     MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Initialize Policer Table ...\n");



    /* Init db - Hardware shadow */
    if (mea_db_Init(unit_i,
                    "Policer ",
                    mea_drv_Get_Policer_num_of_sw_size_func,
                    mea_drv_Get_Policer_num_of_hw_size_func,
                    &MEA_Policer_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init failed (Policer) \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }
    if (mea_db_Init(unit_i,
                    "Policer_Pointer ",
                    mea_drv_Get_Policer_Pointer_num_of_sw_size_func,
                    mea_drv_Get_Policer_Pointer_num_of_hw_size_func,
                    &MEA_Policer_Pointer_db) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Init failed (Policer_Pointer) \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    /* Allocate MEA_Policer_Table */
    if (MEA_Policer_Table == NULL) {
        size = MEA_MAX_NUM_OF_TM_ID * sizeof(MEA_drv_policer_dbt);
        MEA_Policer_Table = (MEA_drv_policer_dbt*)MEA_OS_malloc(size);
        if (MEA_Policer_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_Policer_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset((char*)MEA_Policer_Table,0,size);
    }

    /* Allocate MEA_Policer_hwUnit_Table */
    if (MEA_Policer_hwUnit_Table == NULL) {
        size = mea_drv_num_of_hw_units * sizeof(MEA_drv_policer_hwUnit_dbt);
        MEA_Policer_hwUnit_Table = (MEA_drv_policer_hwUnit_dbt*)MEA_OS_malloc(size);
        if (MEA_Policer_hwUnit_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - Allocate MEA_Policer_HwUnit_Table failed (size=%d)\n",
                              __FUNCTION__,size);
            return MEA_ERROR;
        }
        MEA_OS_memset((char*)MEA_Policer_hwUnit_Table,0,size);
        for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
            MEA_Policer_hwUnit_Table[hwUnit].num_of_entries = ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_LENGTH(hwUnit);
            size = MEA_Policer_hwUnit_Table[hwUnit].num_of_entries * 
                   sizeof(MEA_drv_policer_hw_dbt);
            if (size) {
                MEA_Policer_hwUnit_Table[hwUnit].table = (MEA_drv_policer_hw_dbt*)MEA_OS_malloc(size);
                if (MEA_Policer_hwUnit_Table[hwUnit].table == NULL) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                      "%s - Allocate MEA_Policer_HwUnit_Table[%d].table failed (size=%d)\n",
                                      __FUNCTION__,hwUnit,size);
                    MEA_Policer_hwUnit_Table[hwUnit].num_of_entries = 0;
                    return MEA_ERROR;
                }
                MEA_OS_memset((char*)MEA_Policer_hwUnit_Table[hwUnit].table,0,size);
            }
        }
    }


    

     MEA_Policer_InitDone = MEA_TRUE;

     return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_Policer_callback>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_Policer_callback
                                (MEA_Unit_t unit_i,
                                 MEA_ind_write_t* ind_write_pio)
{

    MEA_db_HwUnit_t          hwUnit;
    MEA_db_HwId_t            hwId;
    MEA_db_SwId_t            swId;
    
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

    hwId = (MEA_db_HwId_t)(ind_write_pio->tableOffset);

    /* If Dual we need to write the second bucket that is not save in db */
    if(MEA_Policer_hwUnit_Table[hwUnit].table[hwId].mode == MEA_POLICER_MODE_DUAL){
        MEA_DRV_GET_SW_ID(unit_i,
                          mea_drv_Get_Policer_db,
                          hwUnit,
                          hwId,
                          swId,
                          return MEA_ERROR);
        hwId += MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit);
        if(MEA_Policer_Bucket_UpdateHw(unit_i,
                                       swId,
                                       hwId,
                                       &(MEA_Policer_hwUnit_Table[hwUnit].table[hwId])
                                      ) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_Policer_Bucket_UpdateHw failed (hwId=%d)\n",
                             __FUNCTION__,hwId);
            return MEA_ERROR;
        }
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_Policer_Pointer_callback>                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_Policer_Pointer_callback
                                (MEA_Unit_t unit_i,
                                 MEA_ind_write_t* ind_write_pio)
{

    MEA_db_HwUnit_t          hwUnit;
    MEA_db_HwId_t            hwId;
    MEA_db_SwId_t            swId;
    
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

    hwId = (MEA_db_HwId_t)(ind_write_pio->tableOffset);

    /* If Dual we need to write the second bucket that is not save in db */
    if(MEA_Policer_hwUnit_Table[hwUnit].table[hwId].mode == MEA_POLICER_MODE_DUAL){
        MEA_DRV_GET_SW_ID(unit_i,
                          mea_drv_Get_Policer_Pointer_db,
                          hwUnit,
                          hwId,
                          swId,
                          return MEA_ERROR);
        hwId += MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit);
        if (MEA_Policer_ProfPointer_UpdateHw(unit_i,
                                              swId,
                                             hwId,
                                             &(MEA_Policer_hwUnit_Table[hwUnit].table[hwId])
                                           ) != MEA_OK) {
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                             "%s - MEA_Policer_ProfPointer_UpdateHw failed (hwId=%d)\n",
                             __FUNCTION__,hwId);
            return MEA_ERROR;
        }
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_ReInit_Policer>                                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_ReInit_Policer(MEA_Unit_t unit_i) {
    

    if (mea_drv_Policer_Profile_ReInit(unit_i) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_drv_Policer_Profile_ReInit failed\n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

    /* ReInit Policer bucket */
    if (mea_db_ReInit(unit_i,
                      MEA_Policer_db,
                      ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET,
                      MEA_MODULE_BM,
                      mea_drv_ReInit_Policer_callback,
                      MEA_FALSE) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit for Policer Bucket\n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

    /* ReInit Policer Pointer */
    if (mea_db_ReInit(unit_i,
                      MEA_Policer_Pointer_db,
                      ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT,
                      MEA_MODULE_BM,
                      mea_drv_ReInit_Policer_Pointer_callback,
                      MEA_FALSE) != MEA_OK) {
      MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - mea_db_ReInit for Policer Pointer\n",
                        __FUNCTION__);
      return MEA_ERROR;
    }

     return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*            <   mea_drv_isExistSwTmId_Policer>                             */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_isExistSwTmId_Policer(MEA_Unit_t unit,MEA_TmId_t      id_i,  MEA_Bool  *found_o)

{

    
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
   /* Check for valid data parmeter */
    if (found_o == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - found_o == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }


#endif    
     
     *found_o=MEA_FALSE;

      if(id_i > MEA_MAX_NUM_OF_TM_ID){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                      "%s - Tm id  %d out of range \n",
                      __FUNCTION__,id_i);
          return MEA_ERROR;
        }
        if ( MEA_Policer_Table[id_i].valid == MEA_FALSE )
        {
            *found_o=MEA_FALSE;
            return MEA_OK;
        } else {
            *found_o=MEA_TRUE;
            return MEA_OK; 
                    
        }
 
    
      
    return MEA_OK;

} 




/*---------------------------------------------------------------------------*/
/*            <   mea_drv_CreateNew_Policer>                                 */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_CreateNew_Policer(MEA_Unit_t unit_i,
                                     MEA_IngressPort_Proto_t  port_proto_prof_i,
                               MEA_Policer_Entry_dbt   *Policer_Entry_i,
                               MEA_Bool                 force_update_i,
                               MEA_Bool                 rateEnable,  
                               MEA_Policer_prof_t      *prof_id_io,
                               MEA_TmId_t              *id_io)
{

    MEA_TmId_t tm_id;
    MEA_TmId_t Hwid_o;
    MEA_Policer_Prof_dbt entry_prof;
    MEA_db_HwUnit_t    hwUnit;
    MEA_Bool          fast_slow;
  
    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

    MEA_OS_memset(&entry_prof,0,sizeof(MEA_Policer_Prof_dbt));
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
   /* Check for valid data parameter */
    if (id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - id_io == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
    if (Policer_Entry_i == NULL &&  (*id_io)==0 ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Policer_Entry_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
    if (prof_id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - prof_id_io == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

#endif    
    
 
    
    tm_id=*id_io;
    
   

    if ( (tm_id == 0 && Policer_Entry_i != NULL) &&  (force_update_i == MEA_FALSE) )
    {
        
        /* check if the parameters is ok*/
        if(mea_drv_Policer_Profile_Check(unit_i,Policer_Entry_i,&fast_slow)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Policer_Profile_Check failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        
        
        /*get free TMID */
        if(mea_drv_get_free_policer_id(&tm_id,fast_slow, Policer_Entry_i) !=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_get_free_policer_id failed \n",
                              __FUNCTION__); 
            return MEA_ERROR;
        }
        /*create profile */
        if(mea_drv_Policer_Profile_Create(unit_i,port_proto_prof_i,Policer_Entry_i,0,prof_id_io)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Policer_Profile_Create failed \n",
                              __FUNCTION__); 
            MEA_Policer_Table[tm_id].valid=MEA_FALSE; 
            return MEA_ERROR;
        }
#if 0        
        Hwid_o=MEA_PLAT_GENERATE_NEW_ID;
#else
        Hwid_o=tm_id;
#endif
        if(MEA_PolicerHW_Tmid_Create(unit_i,tm_id,*prof_id_io,rateEnable,MEA_FALSE,&Hwid_o)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_PolicerHW_Tmid_Create failed  failed \n",
                              __FUNCTION__); /* LIOR 28/10/2007 */
            /* Delete Profile - T.B.D LIOR 28/10/2007 */
            MEA_Policer_Table[tm_id].valid=MEA_FALSE;
            return MEA_ERROR;
        }
        

        MEA_Policer_Table[tm_id].prof_id=*prof_id_io;
        MEA_Policer_Table[tm_id].hwTmid=Hwid_o;
        MEA_Policer_Table[tm_id].hwUnit=hwUnit; 
        MEA_Policer_Table[tm_id].valid=MEA_TRUE;
        MEA_Policer_Table[tm_id].num_of_owners=1;
        
        *id_io = tm_id;
        return MEA_OK;  
    }
    
    return MEA_ERROR;

} 


/*---------------------------------------------------------------------------*/
/*            <   mea_drv_getOwner_Policer >                                 */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_getOwner_Policer(MEA_Unit_t  unit,
                                    MEA_TmId_t  tm_id_i,
                                    MEA_Uint32 *numOfOwners_o) 
{

    if (numOfOwners_o == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - numOfOwners_o==NULL\n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    if (!MEA_Policer_Table[tm_id_i].valid ) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - no existing Policer entry with ID %d\n",
                          __FUNCTION__, tm_id_i);
        return MEA_ERROR; 
    }

    *numOfOwners_o = MEA_Policer_Table[tm_id_i].num_of_owners;

    return MEA_OK;
}
                                        

/*---------------------------------------------------------------------------*/
/*            <   mea_drv_addOwner_Policer >                                 */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_addOwner_Policer(MEA_Unit_t unit_i,
                                    MEA_IngressPort_Proto_t  port_proto_prof_i,
                               MEA_Policer_Entry_dbt   *Policer_Entry_i,
                               MEA_Bool                 force_update_i,
                               MEA_Bool                 rateEnable,  
                               MEA_TmId_t              *id_io)
{

    MEA_TmId_t tm_id;
    

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
   /* Check for valid data parmeter */
    if (id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - id_io == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
   

#endif    
    
 
    
    tm_id=*id_io;
    
   

    if ( (tm_id != 0 && Policer_Entry_i == NULL) &&  (force_update_i == MEA_FALSE) )
    {
     /* Just add owner */
        if ( !MEA_Policer_Table[tm_id].valid )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - no existing Policer entry with ID %d\n",
                              __FUNCTION__, tm_id);
            return MEA_ERROR; 
        }

        if ( mea_drv_policer_add_owner(unit_i,tm_id) != MEA_OK )
        {
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_policer_add_owner failed, index %d\n",
                              __FUNCTION__, tm_id);
            return MEA_ERROR; 
        }
        return MEA_OK;  
    }
    
    return MEA_ERROR;

}


/*---------------------------------------------------------------------------*/
/*            <   mea_drv_CreateId_Policer>                                 */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_CreateId_Policer(MEA_Unit_t unit_i,
                               MEA_IngressPort_Proto_t  port_proto_prof_i,
                               MEA_Policer_Entry_dbt   *Policer_Entry_i,
                               MEA_Bool                 force_update_i,
                               MEA_Bool                 rateEnable,  
                               MEA_Policer_prof_t      *prof_id_io,
                               MEA_TmId_t              *id_io)
{

    MEA_TmId_t tm_id;
    MEA_TmId_t Hwid_o;
    MEA_Policer_Prof_dbt entry_prof;
    MEA_drv_policer_dbt entryInfo;
    MEA_Policer_Prof_dbt   entryProfInfo;
    MEA_Bool found;
    MEA_db_HwUnit_t hwUnit;
    MEA_Policer_Entry_dbt   tmpPolicer_Entry;
    MEA_Uint32 comp;
    MEA_Bool   fast_slow;
    

    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

    MEA_OS_memset(&entry_prof,0,sizeof(entry_prof));
    MEA_OS_memset(&entryInfo,0,sizeof(entryInfo));
    MEA_OS_memset(&entryProfInfo,0,sizeof(entryProfInfo));


    

    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
   
    /* Check for valid data parameter */
    if (id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - id_io == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    /* Check valid *id_io value */
    if (*id_io == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - *id_io == 0\n",
                         __FUNCTION__);
        return MEA_ERROR; 
    }

    /* Check valid Policer_entry value */
    if (Policer_Entry_i == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - Policer_Entry_i == NULL\n",
                         __FUNCTION__);
        return MEA_ERROR; 
    }



#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    
 
    
    tm_id=*id_io;

    if (MEA_Policer_Table[tm_id].valid == MEA_FALSE) {

        /* check if the parameters is ok*/
        if(mea_drv_Policer_Profile_Check(unit_i,Policer_Entry_i,&fast_slow)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Policer_Profile_Check failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }

        if (mea_drv_get_free_policer_id(&tm_id,fast_slow, Policer_Entry_i) !=MEA_OK) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_get_free_policer_id failed \n",
                              __FUNCTION__); 
            return MEA_ERROR;
         }
         
         if (mea_drv_Policer_Profile_Create(unit_i,port_proto_prof_i,Policer_Entry_i,0,prof_id_io)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - mea_drv_Policer_Profile_Create failed \n",
                               __FUNCTION__); 
             MEA_Policer_Table[tm_id].valid=MEA_FALSE; 
             return MEA_ERROR;
         }
#if 0        
         Hwid_o=MEA_PLAT_GENERATE_NEW_ID;
#else
         Hwid_o=tm_id;
#endif  

         if (MEA_PolicerHW_Tmid_Create(unit_i,tm_id,*prof_id_io,rateEnable,MEA_FALSE,&Hwid_o)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - MEA_PolicerHW_Tmid_Create failed  failed \n",
                              __FUNCTION__);
             /*TBD need to delete the profile */
             MEA_Policer_Table[tm_id].valid=MEA_FALSE;
             return MEA_ERROR;
         }


         MEA_OS_memset((char*)(&(MEA_Policer_Table[tm_id])),
                       0,
                       sizeof(MEA_Policer_Table[0]));
         MEA_Policer_Table[tm_id].prof_id=*prof_id_io;
         MEA_Policer_Table[tm_id].hwTmid=Hwid_o;
         MEA_Policer_Table[tm_id].valid=MEA_TRUE;
          MEA_Policer_Table[tm_id].hwUnit = hwUnit;
         MEA_Policer_Table[tm_id].num_of_owners=1;
            
         *id_io = tm_id;
    
    } else {
         /* compare the Data*/
         /*  if input data and the new id internal data are not same 
             then error
             else addOwner only  */
         if (MEA_PolicerSW_Tmid_GetInfo(unit_i,
                                        tm_id,
                                        &entryInfo,
                                        &found)!=MEA_OK){
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                               "%s - MEA_PolicerSW_Tmid_GetInfo failed\n",
                              __FUNCTION__); 
             return MEA_ERROR;
          }

                
          if (mea_drv_Policer_Profile_Get(unit_i,0,entryInfo.prof_id,&entryProfInfo )!=MEA_OK){
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - mea_drv_Policer_Profile_Get failed\n",
                                __FUNCTION__); 
              return MEA_ERROR;
          }
          
		  if(*prof_id_io!=0){
             if(*prof_id_io != entryInfo.prof_id){
				 MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
					 "%s - policer profile id=%d is different and tmId profile used  \n",
					 __FUNCTION__); 
				 return MEA_ERROR;
			 }
		  }

          MEA_OS_memcpy(&tmpPolicer_Entry,Policer_Entry_i,sizeof(tmpPolicer_Entry));
          comp=tmpPolicer_Entry.comp;
          if(MEA_policer_compensation(port_proto_prof_i,&comp)!=MEA_OK){
           MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                "%s - MEA_policer_compensation failed\n",
                                __FUNCTION__); 
              return MEA_ERROR;
          }
          tmpPolicer_Entry.comp=comp;

          if ((MEA_OS_memcmp(&entryProfInfo.rateMeter,
                            &tmpPolicer_Entry,
                            sizeof(entryProfInfo.rateMeter))!=0) || 
               (tmpPolicer_Entry.gn_type != 
                MEA_Policer_hwUnit_Table[hwUnit].table[entryInfo.hwTmid].gn_type)) {
              if ((force_update_i == MEA_FALSE) ||
                  (MEA_Policer_Table[tm_id].num_of_owners  > 1)) {
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "%s - the data is not same of the DB (tmId=%d,profId=%d)\n",
                                    __FUNCTION__,tm_id,entryInfo.prof_id);   
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "old sizeof=%d new sizeof=%d \n",
                                      sizeof(entryProfInfo.rateMeter),
                                    sizeof(tmpPolicer_Entry));
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "old pad1=%d , cup=%d color_awaer=%d\n",
                                    entryProfInfo.rateMeter.pad1,
                                    entryProfInfo.rateMeter.cup,
                                    entryProfInfo.rateMeter.color_aware);
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "new pad1=%d , cup=%d color_awaer=%d\n",
                                    tmpPolicer_Entry.pad1,
                                    tmpPolicer_Entry.cup,
                                    tmpPolicer_Entry.color_aware);
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "old=(CIR/EIR=%llu/%llu,CBS/EBS=%d/%d,comp=%d\n",
                                    entryProfInfo.rateMeter.CIR,
                                    entryProfInfo.rateMeter.EIR,
                                    entryProfInfo.rateMeter.CBS,
                                    entryProfInfo.rateMeter.EBS,
                                    entryProfInfo.rateMeter.comp);
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "new=(CIR/EIR=%llu/%llu,CBS/EBS=%d/%d,comp=%d\n",
                                    tmpPolicer_Entry.CIR,
                                    tmpPolicer_Entry.EIR,
                                    tmpPolicer_Entry.CBS,
                                    tmpPolicer_Entry.EBS,
                                    tmpPolicer_Entry.comp);
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "new=(gn_type=%d)\n",
                                    tmpPolicer_Entry.gn_type);
                  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                    "old=(gn_type=%d)\n",
                                    MEA_Policer_hwUnit_Table[hwUnit].table[entryInfo.hwTmid].gn_type);
                  return MEA_ERROR;
              } 
              if (force_update_i == MEA_TRUE) {
                 /* Update Policer data - assume not pass between fast and slow */
                 if (mea_drv_Policer_Profile_Set(unit_i,
                                                 port_proto_prof_i,
                                                 0,
                                                 MEA_Policer_Table[tm_id].prof_id,
                                                 Policer_Entry_i) != MEA_OK) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                          "%s - mea_drv_Policer_Profile_Set failed (tm_id=%d)\n",
                                     __FUNCTION__,tm_id);
                   return MEA_ERROR;
                 }
              }
          }

          if (mea_drv_policer_add_owner(unit_i,tm_id) != MEA_OK ) {
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                 "%s - mea_drv_policer_add_owner failed, index %d\n",
                                __FUNCTION__, tm_id);
              return MEA_ERROR; 
          }

		   if(*prof_id_io==0){
			   *prof_id_io=entryInfo.prof_id;
		   }





    }
        
    return MEA_OK;    

}










/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Set_Policer>                                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Set_Policer(MEA_Unit_t               unit_i,
                               MEA_IngressPort_Proto_t  port_proto_prof_i,
                               MEA_Policer_Entry_dbt   *Policer_Entry_i,
                               MEA_Bool                 force_update_i,
                               MEA_Bool                 rateEnable,  
                               MEA_Policer_prof_t      *prof_id_io,
                               MEA_TmId_t              *id_io)
{

    MEA_TmId_t tm_id;
    MEA_TmId_t Hwid_o;
    MEA_Policer_Prof_dbt entry_prof;
    MEA_db_HwUnit_t hwUnit;
    MEA_Bool fast_slow;

    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);
  
    MEA_OS_memset(&entry_prof,0,sizeof(MEA_Policer_Prof_dbt));
    
#ifdef MEA_SW_CHECK_INPUT_PARAMETERS
   /* Check for valid data parmeter */
    if (id_io == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - id_io == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }
    if (Policer_Entry_i == NULL &&  (*id_io)==0 ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Policer_Entry_i == NULL\n",
                         __FUNCTION__);
       return MEA_ERROR; 
    }

    if (prof_id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - prof_id_io == NULL\n",
            __FUNCTION__);
        return MEA_ERROR; 
    }
    if(*prof_id_io== MEA_PLAT_GENERATE_NEW_ID){
        *prof_id_io=0;
    }

#endif    
    
 
    
    tm_id=*id_io;
    
    /****************************/
    /* . check if tmId id 0  and Policer_Entry_i !=NULL  need new tmId*/
    /* . if tmId !=0 then check is valid and add add owner */
    /* . if Force is True then need to update the policer profile and */
      
    /*****************************/  
    if((tm_id != 0 && Policer_Entry_i != NULL) && (force_update_i == MEA_TRUE))
    {
        
        
        /* need to check if we have place`````````````*/
        if(mea_drv_Policer_Profile_Get(MEA_UNIT_0,
                                0,
                                MEA_Policer_Table[tm_id].prof_id,
                                &entry_prof )!=MEA_OK){
         return MEA_ERROR;
        }

        if(entry_prof.num_of_owners == 1){
			
			if(*prof_id_io != 0){
				if(*prof_id_io != MEA_Policer_Table[tm_id].prof_id){
					MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"The policer profile are not the same\n");

                 return MEA_ERROR;
				}
			}

            if(mea_drv_Policer_Profile_Set(unit_i,
                                           port_proto_prof_i,
                                           0,
                                           MEA_Policer_Table[tm_id].prof_id,                                
                                           Policer_Entry_i)!=MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - can't update tmid %d with Policer profile ID %d \n",
                              __FUNCTION__, tm_id, MEA_Policer_Table[tm_id].prof_id);
                                           return MEA_ERROR;
            }

			if(*prof_id_io == 0){
				*prof_id_io=MEA_Policer_Table[tm_id].prof_id;
			}

            return MEA_OK;
        }
        /* need to to crate new one */
        if(mea_drv_Policer_Profile_Create(unit_i,port_proto_prof_i,Policer_Entry_i,0,prof_id_io)!=MEA_OK){
            return MEA_ERROR;
        }
/*T.B.D - need to Update the HwTm id  
 if we want to create new ONE we need to Update all the Action */

        /* update HwTmid*/
        Hwid_o=MEA_Policer_Table[tm_id].hwTmid;
         if(MEA_PolicerHW_Tmid_Set(unit_i,tm_id,Hwid_o,*prof_id_io,rateEnable)!=MEA_OK){
            
            return MEA_ERROR;
        }
        
         if (mea_drv_Policer_Profile_Delete(unit_i,0,MEA_Policer_Table[tm_id].prof_id)!=MEA_OK){
            return MEA_ERROR;
         }
         
         /* update */
         MEA_Policer_Table[tm_id].prof_id=*prof_id_io;
         MEA_Policer_Table[tm_id].hwTmid=Hwid_o;

       return MEA_OK;
    }

    if ( (tm_id == 0 && Policer_Entry_i != NULL) &&  (force_update_i == MEA_FALSE) )
    {
        /* check if the parameters is ok*/
        if(mea_drv_Policer_Profile_Check(unit_i,Policer_Entry_i,&fast_slow)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - mea_drv_Policer_Profile_Check failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        
        /*get free TMID */
        if(mea_drv_get_free_policer_id(&tm_id,fast_slow, Policer_Entry_i) !=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_get_free_policer_id failed \n",
                              __FUNCTION__); 
            return MEA_ERROR;
        }
        /*create profile */
        if(mea_drv_Policer_Profile_Create(unit_i,port_proto_prof_i,Policer_Entry_i,0,prof_id_io)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_Policer_Profile_Create failed \n",
                              __FUNCTION__); /* LIOR 28/10/2007 */
            MEA_Policer_Table[tm_id].valid=MEA_FALSE; /* LIOR 28/10/2007 */
            return MEA_ERROR;
        }
#if 0        
        Hwid_o=MEA_PLAT_GENERATE_NEW_ID;
#else
        Hwid_o=tm_id;
#endif
        if(MEA_PolicerHW_Tmid_Create(unit_i,tm_id,*prof_id_io,rateEnable,MEA_FALSE,&Hwid_o)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - MEA_PolicerHW_Tmid_Create failed  failed \n",
                              __FUNCTION__); /* LIOR 28/10/2007 */
            /* Delete Profile - T.B.D LIOR 28/10/2007 */
            MEA_Policer_Table[tm_id].valid=MEA_FALSE;
            return MEA_ERROR;
        }


        MEA_OS_memset((char*)(&(MEA_Policer_Table[tm_id])),
                      0,
                      sizeof(MEA_Policer_Table[0]));
        MEA_Policer_Table[tm_id].prof_id=*prof_id_io;
        MEA_Policer_Table[tm_id].hwTmid=Hwid_o;
        MEA_Policer_Table[tm_id].valid=MEA_TRUE;
        MEA_Policer_Table[tm_id].hwUnit = hwUnit;
        MEA_Policer_Table[tm_id].num_of_owners=1;
        
        *id_io = tm_id;

    }
    else if ( tm_id != 0 )
    {
        /* Just add owner */
        if ( !MEA_Policer_Table[tm_id].valid )
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - no existing Policer entry with ID %d\n",
                              __FUNCTION__, tm_id);
            return MEA_ERROR; 
        }

        if ( mea_drv_policer_add_owner(unit_i,tm_id) != MEA_OK )
        {
              MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_policer_add_owner failed, index %d\n",
                              __FUNCTION__, tm_id);
            return MEA_ERROR; 
        }
    }    
    else
        *id_io = 0;
    //My_function_Checker(__FUNCTION__);
    return MEA_OK;

} 
MEA_Status MEA_API_Set_TM_ID_Info(MEA_Unit_t                 unit_i,
                                  MEA_TmId_t                 tm_id,
                                  MEA_Policer_prof_t        *prof_id_io,
                                  MEA_Policer_Entry_dbt      *Policer_entry )
{
 
    MEA_TmId_t              Hwid;
    MEA_Policer_prof_t      old_prof_id;
    MEA_TmId_t              old_Hwid;
    MEA_Bool                rateEnable;
    MEA_db_HwUnit_t         hwUnit;
//    MEA_db_HwId_t           db_hwId;
    MEA_Bool                fast_slow;
    MEA_Bool                exsit;

   MEA_API_LOG

#ifdef MEA_SW_CHECK_INPUT_PARAMETERS

    /* Check for valid key parameter */
    if (Policer_entry == NULL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Policer_entry == NULL \n",__FUNCTION__);
       return MEA_ERROR; 
    }
    if (prof_id_io == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - prof_id_io == NULL \n",__FUNCTION__);
        return MEA_ERROR; 
    }

    if (tm_id == 0) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - tm_id == 0 \n",__FUNCTION__);
       return MEA_ERROR; 
    }

    if ( !mea_drv_IsTmIdInRange(tm_id) )
    {
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                           "%s - non valid tm_id %d\n",
                           __FUNCTION__, tm_id);
         return MEA_ERROR;    
     }
    
        /*check if the Tm_id is available*/
    if (MEA_Policer_Table[tm_id].valid == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - The TmId %d does not exist\n",
                              __FUNCTION__, tm_id);
         
        return MEA_ERROR;
    } 

    if (*prof_id_io == MEA_PLAT_GENERATE_NEW_ID){
        *prof_id_io=0;
    }

    if(*prof_id_io >= MEA_POLICER_MAX_PROF) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -*prof_id_io %d >= max_value %d \n",
            __FUNCTION__,*prof_id_io, MEA_POLICER_MAX_PROF);
        return MEA_ERROR;
    }



#endif /* MEA_SW_CHECK_INPUT_PARAMETERS */
    

    /* Set Global Memory Mode */


    MEA_DRV_GET_HW_UNIT(unit_i,
                        hwUnit,
                        return MEA_ERROR);

    /* Check the policer parameters that are valid */
    if (mea_drv_Policer_Profile_Check(unit_i,Policer_entry,&fast_slow)!=MEA_OK){
         MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Policer_Profile_Check failed \n",
                         __FUNCTION__);
         return MEA_ERROR;    
    }

    if(tm_id< 16 && fast_slow == MEA_FALSE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - can't change from fast policer to slow  \n",
            __FUNCTION__);
    }
    if(tm_id >=16 && fast_slow == MEA_TRUE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - can't change from slow policer to fast  \n",
            __FUNCTION__);
    return MEA_ERROR;
    }


    if (mea_drv_Policer_Profile_Is_profile_exsit(unit_i,hwUnit,0,*prof_id_io ,&exsit)!=MEA_OK){
         
        return MEA_ERROR;
    }


    if (tm_id != 0 && *prof_id_io != 0 && *prof_id_io != MEA_PLAT_GENERATE_NEW_ID && exsit == MEA_TRUE)
    {
        if (mea_drv_Policer_Profile_Set(unit_i, MEA_INGRESS_PORT_PROTO_TRANS, MEA_POLICER_MODE_MEF, *prof_id_io, Policer_entry)!=MEA_OK)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_Policer_Profile_Set failed \n",
                             __FUNCTION__);
            return MEA_ERROR;
        }


        /* update All the Action with new SW tmID*/
        if (mea_drv_policer_UpdateHwAction_BySwTmId(unit_i,tm_id)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                              "%s - mea_drv_policer_UpdateHwAction_BySwTmId failed \n",
                             __FUNCTION__);
        
        }
        // alex
        rateEnable=MEA_Policer_hwUnit_Table[hwUnit].table[MEA_Policer_Table[tm_id].hwTmid].rateEnable;
        if(MEA_PolicerHW_Tmid_Set(unit_i, tm_id,  tm_id, *prof_id_io, rateEnable)!= MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - MEA_PolicerHW_Tmid_Set update the policer to TM \n",
                __FUNCTION__);
        }


    }
    else
    {
    /* Create policer profile */
    if (mea_drv_Policer_Profile_Create(unit_i,0,Policer_entry,0,prof_id_io)!=MEA_OK){
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_Policer_Profile_Create failed \n",
                         __FUNCTION__);
         return MEA_ERROR;
        }
#if 0
    /* Get free HwTmId */
    if (MEA_PolicerHW_Tmid_GetFreeTmid(unit_i,prof_id,&Hwid) != MEA_OK) {
        mea_drv_Policer_Profile_Delete(unit_i,0,prof_id);
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - fail No free TM HW id \n",
                          __FUNCTION__);
        return MEA_ERROR;
    }

    db_hwId = Hwid;
    mea_db_Delete(unit_i,MEA_Policer_db,(MEA_db_SwId_t)tm_id,hwUnit);
    if (mea_db_Create(unit_i,
                      MEA_Policer_db,
                      (MEA_db_SwId_t)tm_id,
                      hwUnit,
                      &db_hwId) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_db_Create Policer failed (hwUnit=%d,swId=%d)\n",
                          __FUNCTION__,hwUnit,tm_id);
       mea_drv_Policer_Profile_Delete(unit_i,0,prof_id);
       MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].valid  = MEA_FALSE;
       if (MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].mode == MEA_POLICER_MODE_DUAL) {
             MEA_Policer_hwUnit_Table[hwUnit].table[Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid=MEA_FALSE;
       }
       return MEA_ERROR;
    }

    db_hwId = Hwid;
    mea_db_Delete(unit_i,MEA_Policer_Pointer_db,(MEA_db_SwId_t)tm_id,hwUnit);
    if (mea_db_Create(unit_i,
                      MEA_Policer_Pointer_db,
                      (MEA_db_SwId_t)tm_id,
                      hwUnit,
                      &db_hwId) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_db_Create Policer_Pointer failed (hwUnit=%d,swId=%d)\n",
                           __FUNCTION__,hwUnit,tm_id);
        mea_drv_Policer_Profile_Delete(unit_i,0,prof_id);
        MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].valid  = MEA_FALSE;
        if (MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].mode == MEA_POLICER_MODE_DUAL) {
               MEA_Policer_hwUnit_Table[hwUnit].table[Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid=MEA_FALSE;
        }
        mea_db_Delete(unit_i,MEA_Policer_db,(MEA_db_SwId_t)tm_id,hwUnit);
        return MEA_ERROR;
    }

#endif        
    /* Get Current rateEnable mode */
    rateEnable=MEA_Policer_hwUnit_Table[hwUnit].table[MEA_Policer_Table[tm_id].hwTmid].rateEnable;
    

    /* Save old values */
    old_prof_id = MEA_Policer_Table[tm_id].prof_id;
    old_Hwid    = MEA_Policer_Table[tm_id].hwTmid;
    Hwid=tm_id;
    /* Create new Hw TmId */
    if (MEA_PolicerHW_Tmid_Create(unit_i,
                                  tm_id,
                                  *prof_id_io,
                                  rateEnable,
                                  MEA_TRUE, /* Force */
                                  &Hwid)!=MEA_OK){
       mea_drv_Policer_Profile_Delete(unit_i,0,*prof_id_io);
       MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].valid  = MEA_FALSE;
       if (MEA_Policer_hwUnit_Table[hwUnit].table[Hwid].mode == MEA_POLICER_MODE_DUAL) {
           MEA_Policer_hwUnit_Table[hwUnit].table[Hwid+MEA_POLICER_START_BUCKET_TWO(unit_i,hwUnit)].valid=MEA_FALSE;
       }
       return MEA_ERROR;
     }

    /* Save old values */
    old_prof_id = MEA_Policer_Table[tm_id].prof_id;
    old_Hwid    = MEA_Policer_Table[tm_id].hwTmid;

    /* update */
    MEA_Policer_Table[tm_id].prof_id=*prof_id_io;
    MEA_Policer_Table[tm_id].hwTmid=Hwid;
        
    /* update All the Action with new SW tmID*/
    if (mea_drv_policer_UpdateHwAction_BySwTmId(unit_i,tm_id)!=MEA_OK){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_policer_UpdateHwAction_BySwTmId failed \n",
                         __FUNCTION__);
        mea_drv_Policer_Profile_Delete(unit_i,0,*prof_id_io);
        MEA_PolicerHW_Tmid_Delete(unit_i,tm_id,Hwid);
        MEA_Policer_Table[tm_id].prof_id=old_prof_id;
        MEA_Policer_Table[tm_id].hwTmid=old_Hwid;
        return MEA_ERROR;
    }

     /* Delete old Policer profile */  
    if (mea_drv_Policer_Profile_Delete(unit_i,0,old_prof_id)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - mea_drv_Policer_Profile_Delete failed \n",
                          __FUNCTION__);
            MEA_PolicerHW_Tmid_Delete(unit_i,tm_id,old_Hwid);
            return MEA_ERROR;
        }
#if 0        
    /* free old hwID*/
    if (MEA_PolicerHW_Tmid_Delete(unit_i,tm_id,old_Hwid)!=MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - MEA_PolicerHW_Tmid_Delete  failed \n",
                          __FUNCTION__);
            return MEA_ERROR;
         }
#endif
         
    /* Return to Caller */
    }





    return MEA_OK;
}


MEA_Status mea_drv_GetTmid_Policer_INFO(MEA_Unit_t unit_i,
                                        MEA_TmId_t             id,
                                        MEA_drv_policer_dbt *entry){
 
    MEA_Bool found;
    if (MEA_PolicerSW_Tmid_GetInfo(unit_i,id,entry,&found) != MEA_OK) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - MEA_PolicerSW_Tmid_GetInfo failed id %d \n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Get_Policer>                                          */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status mea_drv_Get_Policer(MEA_Unit_t unit_i,
                               MEA_TmId_t             id,
                               MEA_Policer_Entry_dbt *pSLA_Params)
{
    
    
    MEA_Policer_Prof_dbt entry;
    MEA_Policer_Entry_dbt policer_entry;

    MEA_OS_memset(&entry,0,sizeof(MEA_Policer_Prof_dbt));
    MEA_OS_memset(&policer_entry,0,sizeof(MEA_Policer_Entry_dbt));
    
    if(pSLA_Params == NULL){
     MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                          "%s - pSLA_Params == NULL \n",
                          __FUNCTION__);
       return ENET_ERROR;
    } 
    
    /* Check for valid id parameter */
    if (mea_drv_IsTmIdInRange(id) != MEA_TRUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    if ( !MEA_Policer_Table[id].valid )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Policer Entry with ID %d does not exist\n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    /* get policer profile*/
    
    if(mea_drv_Policer_Profile_Get(MEA_UNIT_0,
                                   0,
                                MEA_Policer_Table[id].prof_id,
                                &entry )!=MEA_OK){

    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Policer profile ID %d does not exist\n",
                         __FUNCTION__,MEA_Policer_Table[id].prof_id);
       return MEA_ERROR; 
    }
    
    MEA_OS_memcpy(pSLA_Params,&entry.rateMeter,sizeof(MEA_Policer_Entry_dbt));
    
    
    return MEA_OK;

} 



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Delete_Policer>                                       */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status mea_drv_Delete_Policer(MEA_Unit_t unit_i,
                                  MEA_TmId_t id)
{
    /* Check for valid id parameter */
    if (mea_drv_IsTmIdInRange(id) != MEA_TRUE) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid id %d \n",
                         __FUNCTION__,id);
       return MEA_ERROR;  
    }

    if ( MEA_Policer_Table[id].valid == MEA_FALSE )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - no Policer entry with ID %d\n",
                         __FUNCTION__, id);
       return MEA_ERROR; 
    }

    if ( mea_drv_policer_delete_owner(unit_i,id) != MEA_OK )
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - mea_drv_policer_delete_owner failed, id %d\n",
                         __FUNCTION__, id);
       return MEA_ERROR; 
    }


    return MEA_OK;

}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*             MEA driver APIs implementation                                */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Set_PolicerTicks_Entry>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_PolicerTicks_Entry (MEA_Unit_t                  unit_i,
                                           MEA_PolicerTicks_Entry_dbt* entry)  {

    mea_memory_mode_read_e save_globalMemoryMode;
    MEA_db_HwUnit_t             hwUnit;
   
    MEA_Uint32                 temp;
   MEA_API_LOG

 

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        if (mea_drv_Get_DeviceInfo_NumOfPolicers(unit_i,hwUnit) <= 2) {
            continue;
        }

        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);

		temp = (MEA_Uint32)(MEA_TICKS_FOR_SLOW_SERVICE_MIN_VAL(0, 0));

    if (entry->ticks_for_slow < temp ) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry->ticks_for_slow_service (%d) < Min value (%d) \n",
                         __FUNCTION__,
                         entry->ticks_for_slow,
                         MEA_TICKS_FOR_SLOW_SERVICE_MIN_VAL(unit_i,hwUnit));
       return MEA_ERROR;
    }

    if (entry->ticks_for_slow > MEA_TICKS_FOR_SLOW_SERVICE_MAX_VAL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry->ticks_for_slow_service (%d) > Max value (%d) \n",
                         __FUNCTION__,
                         entry->ticks_for_slow,
                         MEA_TICKS_FOR_SLOW_SERVICE_MAX_VAL);
       return MEA_ERROR;
    }

    if (entry->ticks_for_fast < MEA_TICKS_FOR_FAST_SERVICE_MIN_VAL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry->ticks_for_fast_service (%d) < Min value (%d) \n",
                         __FUNCTION__,
                         entry->ticks_for_fast,
                         MEA_TICKS_FOR_FAST_SERVICE_MIN_VAL);
       return MEA_ERROR;
    }

    if (entry->ticks_for_fast > MEA_TICKS_FOR_FAST_SERVICE_MAX_VAL) {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - entry->ticks_for_fast_service (%d) > Max value (%d) \n",
                         __FUNCTION__,
                         entry->ticks_for_fast,
                         MEA_TICKS_FOR_FAST_SERVICE_MAX_VAL);
       return MEA_ERROR;
    }


    MEA_API_WriteReg(unit_i,
                     ENET_BM_NSBP, 
                     entry->ticks_for_slow,
                     MEA_MODULE_BM);

    MEA_Global_ServicePoliceTick.ticks_for_slow = entry->ticks_for_slow;
    MEA_Global_ServicePoliceTick.ticks_for_fast = entry->ticks_for_fast;



    temp = (MEA_Global_IngressFlowPolicerTick.ticks_for_fast << 16) | MEA_Global_ServicePoliceTick.ticks_for_fast;

    MEA_API_WriteReg(unit_i,
                     ENET_BM_HSBP, 
                     temp,
                     MEA_MODULE_BM);


      MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }

    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <MEA_API_Get_PolicerTicks_Entry>                               */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_PolicerTicks_Entry (MEA_Unit_t                  unit_i,
                                           MEA_PolicerTicks_Entry_dbt* entry)  {

   mea_memory_mode_read_e save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit;
    



   MEA_API_LOG
       

    if (entry == NULL) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - entry == NULL\n",__FUNCTION__);
        return MEA_ERROR;
    }

    for (hwUnit=0;hwUnit<mea_drv_num_of_hw_units;hwUnit++) {
        if (mea_drv_Get_DeviceInfo_NumOfPolicers(unit_i,hwUnit) <= 2) {
            continue;
        }

        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,hwUnit);


#if 0
    entry->ticks_for_slow =  MEA_API_ReadReg(unit_i,
                                                     MEA_BM_NSBP, 
                                                     MEA_MODULE_BM);
    entry->ticks_for_fast =  (MEA_API_ReadReg(unit_i, MEA_BM_HSBP,MEA_MODULE_BM) & 0xffff) ;
#else
        entry->ticks_for_slow = MEA_Global_ServicePoliceTick.ticks_for_slow;
        entry->ticks_for_fast = MEA_Global_ServicePoliceTick.ticks_for_fast;

#endif

      MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }



    return MEA_OK;

}



MEA_Status MEA_API_Set_IngressFlowPolicerTicks_Entry(MEA_Unit_t                  unit,
    MEA_PolicerTicks_Entry_dbt* entry)
{

    mea_memory_mode_read_e save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit;
   
    MEA_Uint32 temp;


    MEA_API_LOG
    if (b_bist_test) {
            return MEA_OK;
    }

   


        if (entry == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - entry == NULL\n", __FUNCTION__);
            return MEA_ERROR;
        }

    for (hwUnit = 0; hwUnit<mea_drv_num_of_hw_units; hwUnit++) {
        if (mea_drv_Get_DeviceInfo_NumOFIngress_flowPolicers(unit, hwUnit) <= 2) {
            continue;
        }

        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode, hwUnit);


        if (entry->ticks_for_slow < (MEA_Uint32)(MEA_ING_FLOW_TICKS_FOR_SLOW_MIN_VAL(unit, hwUnit))) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
                "%s - entry->ticks_for_slow (%d) < Min value (%d) \n",
                __FUNCTION__,
                entry->ticks_for_slow,
                (MEA_Uint32)(MEA_ING_FLOW_TICKS_FOR_SLOW_MIN_VAL(unit, hwUnit)));
            //return MEA_ERROR;
        }

        if (entry->ticks_for_slow > MEA_ING_FLOW_TICKS_FOR_SLOW_MAX_VAL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING,
                "%s - entry->ticks_for_slow (%d) > Max value (%d) \n",
                __FUNCTION__,
                entry->ticks_for_slow,
                MEA_ING_FLOW_TICKS_FOR_SLOW_MAX_VAL);
            //return MEA_ERROR;
        }

        if (entry->ticks_for_fast < MEA_ING_FLOW_TICKS_FOR_FAST_MIN_VAL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry->ticks_for_fast (%d) < Min value (%d) \n",
                __FUNCTION__,
                entry->ticks_for_fast,
                MEA_ING_FLOW_TICKS_FOR_FAST_MIN_VAL);
            //return MEA_ERROR;
        }

        if (entry->ticks_for_fast > MEA_ING_FLOW_TICKS_FOR_FAST_MAX_VAL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - entry->ticks_for_fast_service (%d) > Max value (%d) \n",
                __FUNCTION__,
                entry->ticks_for_fast,
                MEA_ING_FLOW_TICKS_FOR_FAST_MAX_VAL);
            //return MEA_ERROR;
        }


        MEA_API_WriteReg(unit,
            ENET_BM_INGFLOW_POLICER_SLOW_TICKS,
            entry->ticks_for_slow,
            MEA_MODULE_BM);

        MEA_Global_IngressFlowPolicerTick.ticks_for_slow = entry->ticks_for_slow;
        MEA_Global_IngressFlowPolicerTick.ticks_for_fast = entry->ticks_for_fast;



        temp = (MEA_Global_IngressFlowPolicerTick.ticks_for_fast <<16) | MEA_Global_ServicePoliceTick.ticks_for_fast;

        MEA_API_WriteReg(unit,
            ENET_BM_HSBP,
            temp,
            MEA_MODULE_BM);
        

        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }





    return MEA_OK;
}

MEA_Status MEA_API_Get_IngressFlowPolicerTicks_Entry(MEA_Unit_t  unit_i,
                                                     MEA_PolicerTicks_Entry_dbt* entry)
{


    mea_memory_mode_read_e save_globalMemoryMode;
    MEA_db_HwUnit_t hwUnit;

    MEA_API_LOG

        if (entry == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - entry == NULL\n", __FUNCTION__);
            return MEA_ERROR;
        }

    for (hwUnit = 0; hwUnit<mea_drv_num_of_hw_units; hwUnit++) {
        if (mea_drv_Get_DeviceInfo_NumOfPolicers(unit_i, hwUnit) <= 2) {
            continue;
        }

        MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode, hwUnit);


#if 0
        entry->ticks_for_slow = MEA_API_ReadReg(unit_i,ENET_BM_INGFLOW_POLICER_SLOW_TICKS,MEA_MODULE_BM);
        entry->ticks_for_fast = (MEA_API_ReadReg(unit_i, MEA_BM_HSBP, MEA_MODULE_BM) & 0xffff0000)>>16;
#else
        entry->ticks_for_slow = MEA_Global_IngressFlowPolicerTick.ticks_for_slow;
        entry->ticks_for_fast = MEA_Global_IngressFlowPolicerTick.ticks_for_fast;

#endif
        MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);
    }



    return MEA_OK;




    return MEA_OK;
}




MEA_Status mea_drv_get_policer_pool_entry(MEA_Unit_t unit_i,
                                          MEA_TmId_t       index,
                                      MEA_Bool            *found,
                                      MEA_drv_policer_dbt *entry_o )
{
    *found = MEA_FALSE;

    if (mea_drv_IsTmIdInRange(index) != MEA_TRUE) 
    {
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                         "%s - Invalid index %d \n",
                         __FUNCTION__,index);
       return MEA_ERROR;  
    }

    if ( MEA_Policer_Table[index].valid == MEA_TRUE )
    {
        
        
        MEA_OS_memcpy(entry_o,
                      &(MEA_Policer_Table[index]),
                      sizeof(*entry_o));

        *found = MEA_TRUE;
    }

    return MEA_OK;
}




MEA_Status MEA_policer_compensation(MEA_Uint32 port_proto_prof_i,MEA_Uint32 *comp)
{

    if (*comp == MEA_POLICER_COMP_ENFORCE)
    {  
        /* The compensation is done according to protocol - no enforcement */
        *comp=MEA_POLICER_COMP_DEF_VAL;

        switch(port_proto_prof_i){
            case MEA_INGRESS_PORT_PROTO_TRANS:
                *comp += MEA_POLICER_COMP_TRANS_LEN_DEF_VAL;
                break;


            case MEA_INGRESS_PORT_PROTO_VLAN:
                *comp += MEA_POLICER_COMP_VLAN_LEN_DEF_VAL;
                break;
            
            case MEA_INGRESS_PORT_PROTO_QTAG:
            case MEA_INGRESS_PORT_PROTO_QTAG_OUTER:
                *comp += MEA_POLICER_COMP_QTAG_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_MPLSoE:
                *comp += MEA_POLICER_COMP_VLAN_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_MARTINI:
                *comp += MEA_POLICER_COMP_MARTINI_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_MPLS:
                *comp += MEA_POLICER_COMP_MPLS_LEN_DEF_VAL;
                break;

            case MEA_INGRESS_PORT_PROTO_EoLLCoATM :
                *comp += MEA_POLICER_COMP_EoLLCoATM_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_IPoEoLLCoATM :
                *comp += MEA_POLICER_COMP_IPoEoLLCoATM_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_PPPoEoLLCoATM :
                *comp += MEA_POLICER_COMP_PPPoEoLLCoATM_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_PPPoEoVcMUXoATM :
                *comp += MEA_POLICER_COMP_PPPoEoVcMUXoATM_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_EoVcMUXoATM :
                *comp += MEA_POLICER_COMP_EoVcMUXoATM_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_IPoLLCoATM :
                *comp += MEA_POLICER_COMP_IPoLLCoATM_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_IPoVcMUXoATM :
                *comp += MEA_POLICER_COMP_IPoVcMUXoATM_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_PPPoLLCoATM   :
                *comp += MEA_POLICER_COMP_PPPoLLCoATM_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_PPPoVcMUXoATM :
                *comp += MEA_POLICER_COMP_PPPoVcMUXoA_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_IP :
                *comp += MEA_POLICER_COMP_IP_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_IPoVLAN :
                *comp += MEA_POLICER_COMP_IPoVLAN_LEN_DEF_VAL;
                break;
            case MEA_INGRESS_PORT_PROTO_ETHER_TYPE :
                *comp += MEA_POLICER_COMP_VLAN_LEN_DEF_VAL;
                break;
            
            default:
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                                  "%s - Invalid profile %d\n",
                                  __FUNCTION__,
                                  port_proto_prof_i);
                return MEA_ERROR;
            }
    }



return MEA_OK;
}



/************************************************************************/
/*                                                                      */
/************************************************************************/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*            <mea_drv_Ingress_flow_RateMeter_UpdateHwEntry>                 */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status  mea_drv_Ingress_flow_RateMeter_UpdateHwEntry(MEA_funcParam arg1, 
                                                                MEA_funcParam arg2, 
                                                                MEA_funcParam arg3, 
                                                                MEA_funcParam arg4)



{
    MEA_IngressPort_PolicerType_te policerType = (MEA_IngressPort_PolicerType_te)((long)arg1);
    MEA_Policer_Entry_dbt* entry_i = (MEA_Policer_Entry_dbt*)arg2;
    MEA_Bool               rate_enable = (MEA_Bool)((long)arg3);
    MEA_Uint32             tableType = (MEA_Uint32)((long)arg4);
    // MEA_Bool SET_MTU = MEA_FALSE;
    MEA_EbsCbs_t   myCBS = 0;
    char Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_LAST + 1][20];




    MEA_Uint32 val, val1, val2, val3, temp;
    MEA_uint64 norm_cir;
    MEA_Uint32 tick;
    MEA_PolicerTicks_Entry_dbt PolicerTicks_Entry;
    MEA_Uint32 flag_fast_service;
    MEA_uint64 round_cir;
    MEA_Uint32 maxVal, mtu_size;

    //MEA_uint64 sysclck = 100;


#if MEA_PORT_RATE_METER_2_BUCKET    
    MEA_Uint32 round_eir;
    MEA_Uint32 norm_eir;
#endif

#if MEA_PORT_POLICER_DISABLE_HW
    return MEA_OK;
#endif    
    if (entry_i == NULL){
        return MEA_ERROR;
    }


    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_MC][0]), "MC     ");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_BC][0]), "BC     ");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_UNICAST][0]), "UNICAST");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN][0]), "UNKNOWN");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_TOTAL][0]), "TOTAL");
    MEA_OS_strcpy(&(Str_policerType[MEA_INGRESS_PORT_POLICER_TYPE_LAST][0]), "LAST");




    /* get the policer ticks entry */
    if (MEA_API_Get_IngressFlowPolicerTicks_Entry(MEA_UNIT_0, &PolicerTicks_Entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_GEt_PolicerTicks_Entry failed\n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    //sysclck = MEA_GLOBAL_SYS_Clock;
    
   
    /* get the relevant tick value according to the service type (normal/fast) */
#if 0
    if (port > MEA_POLICER_LAST_FAST_SERVICE) {
        tick = PolicerTicks_Entry.ticks_for_slow_service;
        flag_fast_service = MEA_FALSE;
    }
    else {
        tick = PolicerTicks_Entry.ticks_for_fast_service;
        flag_fast_service = MEA_TRUE;
    }
#else
    /*for now we work only slow*/
    tick = PolicerTicks_Entry.ticks_for_slow;/*need to set the tick for a port*/
    flag_fast_service = MEA_FALSE;
    if (tick == 0){
        //         MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG, "tick = %d \n");
    }

#endif

#if 1
    /* calculate normalize cir and eir */
    if ((entry_i->CIR) != 0){
        if (flag_fast_service == MEA_TRUE){
            round_cir = (entry_i->CIR) % MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_FAST_VAL;
            norm_cir = ((entry_i->CIR) / MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_FAST_VAL);
            if ((round_cir >= MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_FAST_VAL / 2)) {
                round_cir = 1;
            }
            else {
                round_cir = 0;
            }
            if (round_cir + norm_cir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK)
                norm_cir = round_cir + norm_cir;
            else
                norm_cir = MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK;

        }
        else {
            round_cir = (entry_i->CIR) % MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_SLOW_VAL;
            norm_cir = (entry_i->CIR) / MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_SLOW_VAL;
            if ((round_cir >= MEA_ING_FLOW_POLICER_GLOBAL_GRANULARITY_SLOW_VAL / 2)) {
                round_cir = 1;
            }
            else {
                round_cir = 0;
            }
            if (round_cir + norm_cir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK)
                norm_cir = round_cir + norm_cir;
            else
                norm_cir = MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK;

        }
    }
    else{
        norm_cir = 0;
    }
#else
    if ((entry_i->CIR) != 0){
        temp = (sysclck * 8 * 1000000);
        round_cir = ((entry_i->CIR) * tick) % temp;
        norm_cir = ((entry_i->CIR) * tick) / temp;
        round_cir = 1;/**/

        if (round_cir + norm_cir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK)
            norm_cir += round_cir;

    }
    else{
        norm_cir = 0;
    }



#endif


    /**/

#if MEA_PORT_RATE_METER_2_BUCKET 
    /* calculate normalize cir and eir */
    if ((MEA_Uint32)(entry_i->EIR) != 0){
        if (flag_fast_service == MEA_TRUE){
            round_eir = (MEA_Uint32)(entry_i->CIR) % MEA_PORT_RATE_METER_CIR_EIR_FAST_GN;
            norm_eir = ((MEA_Uint32)(entry_i->CIR) / MEA_PORT_RATE_METER_CIR_EIR_FAST_GN);
            if ((round_eir >= MEA_PORT_RATE_METER_CIR_EIR_FAST_GN / 2)) {
                round_eir = 1;
            }
            else {
                round_eir = 0;
            }
            if (round_eir + norm_eir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK)
                norm_eir += round_eir;

        }
        else {
            round_eir = (MEA_Uint32)(entry_i->EIR) % MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN;
            norm_eir = (MEA_Uint32)(entry_i->EIR) / MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN;
            if ((round_eir >= MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN / 2)) {
                round_eir = 1;
            }
            else {
                round_eir = 0;
            }
            if (round_eir + norm_eir < MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK)
                norm_eir += round_eir;
        }
    }
    else{
        norm_eir = 0;
    }
#endif

    if ((entry_i->CIR) != 0 && (entry_i->CBS != 0))
    {
        if (entry_i->maxMtu == 0){
            //  SET_MTU = MEA_FALSE;
            if (entry_i->CBS < 32000){
                myCBS = 32000;
            }
            else{
                myCBS = entry_i->CBS;
            }

            mtu_size = MEA_Platform_GetMaxMTU();
        }
        else {
            //   SET_MTU = MEA_TRUE;
            mtu_size = entry_i->maxMtu;
            myCBS = entry_i->CBS;
        }

        maxVal = MEA_OS_MAX(((MEA_Uint32)(norm_cir << (entry_i->gn_type)) + mtu_size), (mtu_size * 2));

        //norm_cbs = norm_cbs;
        if (maxVal > myCBS)
        {
            if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_PROF)
                MEA_OS_LOG_logMsg(MEA_OS_LOG_WARNING, " ****** ThePortPolicer %s CBS is smaller than the recommended %d ****** \n",
                Str_policerType[policerType], maxVal);
            //return MEA_ERROR;
        }
    }

    /* Build register 0 */
    val = 0;

    temp = (MEA_Uint32)norm_cir;
    temp &= (MEA_PORT_RATE_METER_PROF_CIR_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CIR_MASK));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CIR_MASK);
    val |= temp;

    temp = myCBS; //(MEA_Uint32)entry_i->CBS;
    temp &= (MEA_PORT_RATE_METER_PROF_CBS_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CBS_MASK));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CBS_MASK);
    val |= temp;
#if MEA_PORT_RATE_METER_2_BUCKET          

    temp = (MEA_Uint32)norm_eir;
    temp &= (MEA_PORT_RATE_METER_PROF_EIR0_3_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_EIR0_3_MASK));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_EIR0_3_MASK);
    val |= temp;
#endif



    if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_PROF) {
        MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA0, val, MEA_MODULE_BM);
    }

    /* Build register 1 */

    val1 = 0;
#if MEA_PORT_RATE_METER_2_BUCKET 
    temp = (MEA_Uint32)norm_eir;
    temp >>= (32 - MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_EIR0_3_MASK));
    temp &= (MEA_SRV_RATE_METER_PROF_EIR4_11_MASK >> MEA_OS_calc_shift_from_mask(MEA_POLICER_EIR_MASK2));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_SRV_RATE_METER_PROF_EIR4_11_MASK);
    val1 |= temp;

    temp = (MEA_Uint32)entry_i->EBS;
    temp &= (MEA_SRV_RATE_METER_PROF_EBS_MASK >> MEA_OS_calc_shift_from_mask(MEA_SRV_RATE_METER_PROF_EBS_MASK));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_SRV_RATE_METER_PROF_EBS_MASK);
    val1 |= temp;

#endif
    temp = (MEA_Uint32)entry_i->comp;
    temp &= (MEA_PORT_RATE_METER_PROF_COMP_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_COMP_MASK));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_COMP_MASK);
    val1 |= temp;

    temp = (MEA_Uint32)entry_i->cup;
    temp &= (MEA_PORT_RATE_METER_PROF_CF_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CF_MASK));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CF_MASK);
    val1 |= temp;

    temp = (MEA_Uint32)entry_i->color_aware;
    temp &= (MEA_PORT_RATE_METER_PROF_CA_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CA_MASK));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_PROF_CA_MASK);
    val1 |= temp;


    if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_PROF) {
        MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA1, val1, MEA_MODULE_BM);
    }


    val2 = 0;

    temp = myCBS; //(MEA_Uint32)entry_i->CBS;
    temp &= (MEA_PORT_RATE_METER_CONTEX_XCIR >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_XCIR));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_XCIR);
    val2 |= temp;

#if MEA_PORT_RATE_METER_2_BUCKET

    temp = (MEA_Uint32)entry_i->EBS;
    temp &= (MEA_PORT_RATE_METER_CONTEX_XEIR_MASK >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_XEIR_MASK));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_XEIR_MASK);
    val2 |= temp;
#endif    
    if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_BUCKET) {

        MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA0, val2, MEA_MODULE_BM);
    }


    val3 = 0;
    temp = ((MEA_Uint32)rate_enable == MEA_INGRESS_FLOW_RATE_MEETRING_DISABLE_DEF_VAL) ? MEA_INGRESS_FLOW_RATE_METERING_HW_DISABLE : MEA_INGRESS_FLOW_RATE_METERING_HW_ENABEL;
    temp &= (MEA_PORT_RATE_METER_CONTEX_ENABL >> MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_ENABL));
    temp <<= MEA_OS_calc_shift_from_mask(MEA_PORT_RATE_METER_CONTEX_ENABL);
    val3 |= temp;


    if (tableType == ENET_BM_TBL_TYP_INGRESS_RATE_METERING_BUCKET) {

        MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA1, val3, MEA_MODULE_BM);
    }




    return MEA_OK;
}






/**************************************************************************************/
/* Function Name: mea_drv_Ingress_flow_RateMeter_UpdateHw*/
/* */
/* Description:	*/
/**/
/**/
/**/
/* Return:		   	MEA_OK/MEA_ERROR.*/
/*																					  */
/**************************************************************************************/
static MEA_Status mea_drv_Ingress_flow_RateMeter_UpdateHw(MEA_Uint32 offset, MEA_Policer_Entry_dbt* entry_i, MEA_Bool rate_Enable, MEA_IngressPort_PolicerType_te type)
{

    MEA_ind_write_t ind_write;
#if MEA_PORT_POLICER_DISABLE_HW
    return MEA_OK;
#endif

    ind_write.tableType = ENET_BM_TBL_TYP_INGRESS_RATE_METERING_PROF;
    ind_write.tableOffset = offset;
    ind_write.cmdReg = MEA_BM_IA_CMD;
    ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
    ind_write.statusReg = MEA_BM_IA_STAT;
    ind_write.statusMask = MEA_BM_IA_STAT_MASK;
    ind_write.tableAddrReg = MEA_BM_IA_ADDR;
    ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.writeEntry = (MEA_FUNCPTR)mea_drv_Ingress_flow_RateMeter_UpdateHwEntry;
    ind_write.funcParam1 = (MEA_funcParam)((long)type);
    ind_write.funcParam2 = (MEA_funcParam)entry_i;
    ind_write.funcParam3 = (MEA_funcParam)((long)rate_Enable);
    ind_write.funcParam4 = (MEA_funcParam)((long)ind_write.tableType);


    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect failed "
            "TableType=%d , TableOffset=%d , mea_module=%d\n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            MEA_MODULE_BM);
        return MEA_ERROR;
    }

    ind_write.tableType = ENET_BM_TBL_TYP_INGRESS_RATE_METERING_BUCKET;
    ind_write.funcParam4 = (MEA_funcParam)((long)ind_write.tableType);

    if (MEA_API_WriteIndirect(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - MEA_API_WriteIndirect failed "
            "TableType=%d , TableOffset=%d , mea_module=%d\n",
            __FUNCTION__,
            ind_write.tableType,
            ind_write.tableOffset,
            MEA_MODULE_BM);
        return MEA_ERROR;
    }


    return MEA_OK;


}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <mea_drv_check_Ingress_flow_Policer_prof  >                   */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status mea_drv_check_Ingress_flow_Policer_prof(MEA_Policer_Entry_dbt *entry_i){

    MEA_uint64 slow_max;
    MEA_Bool request_for_fast_tmId;
    MEA_Policer_Entry_dbt *Policer_Entry;
    MEA_uint64 min;
    MEA_uint64 max;


    if (entry_i == NULL){
        return MEA_ERROR;
    }

    Policer_Entry = entry_i;


    if (Policer_Entry->CIR > 100000001 || 
        Policer_Entry->EIR > 100000001){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - currently CIR/EIR max value  100M   \n",
            __FUNCTION__);
        return MEA_ERROR;
    }

    if (entry_i->gn_type != 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port Policer not support gn_type (%d) != 0 \n",
            __FUNCTION__, entry_i->gn_type);
        return MEA_ERROR;
    }
    if (entry_i->gn_sign != 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - Port Policer not support gn_sign (%d) != 0 \n",
            __FUNCTION__, entry_i->gn_sign);
        return MEA_ERROR;
    }

    slow_max = (MEA_uint64)MEA_ING_POLICER_CIR_EIR_SLOW_MAX_VAL(entry_i->gn_sign, entry_i->gn_type);

    if ((entry_i->CIR > slow_max) ||
        (entry_i->EIR > slow_max)) {
        request_for_fast_tmId = MEA_TRUE;
        min = (MEA_uint64)MEA_ING_POLICER_CIR_EIR_FAST_MIN_VAL(entry_i->gn_sign, entry_i->gn_type);
        max = (MEA_uint64)MEA_ING_POLICER_CIR_EIR_FAST_MAX_VAL(entry_i->gn_sign, entry_i->gn_type);
    }
    else {
        request_for_fast_tmId = MEA_FALSE;
        min = (MEA_uint64)MEA_ING_POLICER_CIR_EIR_SLOW_MIN_VAL(entry_i->gn_sign, entry_i->gn_type);
        max = (MEA_uint64)MEA_ING_POLICER_CIR_EIR_SLOW_MAX_VAL(entry_i->gn_sign, entry_i->gn_type);
    }
    if (Policer_Entry->CIR == 0 && Policer_Entry->CBS != 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - CBS (%d) need to be Zero when Cir is Zero \n",
            __FUNCTION__, Policer_Entry->CBS);

        return MEA_ERROR;
    }
    if (Policer_Entry->CIR != 0 && Policer_Entry->CBS == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - CIR (%d) need to be Zero when CBS is Zero \n",
            __FUNCTION__, Policer_Entry->CIR);

        return MEA_ERROR;
    }
    if (request_for_fast_tmId == MEA_FALSE){
        //  MEA_OS_LOG_logMsg(MEA_OS_LOG_DEBUG,"request_for_fast_tmId == MEA_FALSE \n"); 
    }

#if MEA_PORT_RATE_METER_2_BUCKET    
    if (Policer_Entry->EIR == 0 && Policer_Entry->EBS != 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - EBS (%d) need to be Zero when EIR is Zero \n",
            __FUNCTION__, Policer_Entry->EBS);

        return MEA_ERROR;
    }
    if (Policer_Entry->EIR != 0 && Policer_Entry->EBS == 0) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - EIR (%d) need to be Zero when EBS is Zero \n",
            __FUNCTION__, Policer_Entry->EIR);

        return MEA_ERROR;
    }
    if (Policer_Entry->EIR != 0 && Policer_Entry->EIR < min) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - EIR value %ld (0x%08x) < %ld min value (0x%08x) \n",
            __FUNCTION__, Policer_Entry->EIR, Policer_Entry->EIR, min, min);

        return MEA_ERROR;
    }
    if (Policer_Entry->EIR > max) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s - EIR value %ld (0x%08x) > max value %ld (0x%08x) \n",
            __FUNCTION__,
            Policer_Entry->EIR, Policer_Entry->EIR,
            max, max);

        return MEA_ERROR;
    }
#else
    if (Policer_Entry->EIR != 0 || Policer_Entry->EBS != 0){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - \nEIR and EBS mast be zero only one bucket support\n", __FUNCTION__);
        return MEA_ERROR;
    }
#endif

    if (Policer_Entry->CIR != 0 && Policer_Entry->CIR < min) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -ERROR3 CIR value %ld  < min value %ld  \n",
            __FUNCTION__, Policer_Entry->CIR, min);

        return MEA_ERROR;
    }

    if (Policer_Entry->CIR > max) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
            "%s -ERROR4 CIR value %ld (0x%08x) > max value %ld (0x%08x) \n",
            __FUNCTION__,
            Policer_Entry->CIR, Policer_Entry->CIR, max, max);

        return MEA_ERROR;
    }
    if (Policer_Entry->maxMtu != 0){
        if (Policer_Entry->maxMtu < 64){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s  maxMtu %d  and the minimum 64\n",
                __FUNCTION__,
                Policer_Entry->maxMtu);

            return MEA_ERROR;

        }
    }


    return MEA_OK;
}



MEA_Status MEA_API_Create_Ingress_flow_policerId(MEA_Unit_t unit, MEA_Uint16 *profId)
{
    MEA_Uint16 index;
    MEA_Bool  found = MEA_FALSE;
    
    if (!MEA_IS_INGRESS_FLOW_POLICER_SUPPORT) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow not support On this hw ", __FUNCTION__, profId);
        return MEA_ERROR;
    }

    if (*profId == MEA_PLAT_GENERATE_NEW_ID) 
    {
        for (index =1; index<MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE ; index ++)
        {
            if (MEA_Ingress_Flow_policer_Table[index].valid == MEA_FALSE) {
                *profId = index;
                found = MEA_TRUE;
                break;
            }
        }
        if (found == MEA_FALSE)
        {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile  no free profile \n ", __FUNCTION__ );
            return MEA_ERROR;
        }
        else {
            MEA_Ingress_Flow_policer_Table[index].valid = MEA_TRUE;
        }


    }
    else 
    {
        if (*profId >= (MEA_Uint16)MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d is out from range  \n", __FUNCTION__, profId);
            return MEA_ERROR;
        }

        if (MEA_Ingress_Flow_policer_Table[*profId].valid == MEA_TRUE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d already create \n", __FUNCTION__, profId);
            return MEA_ERROR;
        }

        MEA_Ingress_Flow_policer_Table[*profId].valid = MEA_TRUE;


    
    }

     




    return MEA_OK;
}
 

MEA_Status MEA_API_Delete_Ingress_flow_policerId(MEA_Unit_t unit, MEA_Uint16 profId)
{

    mea_Ingress_Flow_policer_dbt entry;

    /*check the profile id range */
    if (profId >= (MEA_Uint16)MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d is out from range  ", __FUNCTION__, profId);
        return MEA_ERROR;
    }


    if (MEA_Ingress_Flow_policer_Table[profId].valid == MEA_FALSE) {

        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d not create  ", __FUNCTION__, profId);
        return MEA_ERROR;
    }

    MEA_OS_memset(&entry, 0, sizeof(mea_Ingress_Flow_policer_dbt));

    if (MEA_API_Set_Ingress_flow_policer(unit, profId, &entry) != MEA_OK) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - TMEA_API_Set_Ingress_flow_policer %d fail  ", __FUNCTION__, profId);
        return MEA_ERROR;
    
    }

    MEA_Ingress_Flow_policer_Table[profId].valid = MEA_FALSE;


    return MEA_OK;

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Set_Ingress_flow_policer  >                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status  MEA_API_Set_Ingress_flow_policer(MEA_Unit_t unit, MEA_Uint16 profId, mea_Ingress_Flow_policer_dbt *entry)
{

    MEA_Uint32 i;
    MEA_Uint32 index, offset;
    MEA_Policer_Entry_dbt polcer_entry;
    MEA_Globals_Entry_dbt       Global_entry;

    if (!MEA_IS_INGRESS_FLOW_POLICER_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow not support On this hw ", __FUNCTION__, profId);
        return MEA_ERROR;
    }

    MEA_API_Get_Globals_Entry(unit, &Global_entry);

    if (Global_entry.alow_ingflow_pol_zero == MEA_FALSE) {
        if (profId < (MEA_Uint16)MEA_INGRESS_FLOW_POLICER_FIRST_PROFILE) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d is not alow  ", __FUNCTION__, profId);
            return MEA_ERROR;
        }
    }

    /*check the profile id range */
    if (profId >= (MEA_Uint16)MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d is out from range  ", __FUNCTION__, profId);
        return MEA_ERROR;
    }
    
    
    if (MEA_Ingress_Flow_policer_Table[profId].valid == MEA_FALSE) {
        
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d not create  ", __FUNCTION__, profId);
        return MEA_ERROR;
    }

    index = profId;
    for (i = 0; i < MEA_INGRESS_PORT_POLICER_TYPE_LAST; i++) {

        /* Ignore TOTAL Policer for the HW */
        if (i == MEA_INGRESS_PORT_POLICER_TYPE_TOTAL) {
            continue;
        }

        /* Verify for no change */
        if ((MEA_OS_memcmp(&(entry->policer_vec[i]),
            &(MEA_Ingress_Flow_policer_Table[profId].Ingress_Flow_policer.policer_vec[i]),
            sizeof(entry->policer_vec[0])) == 0)) {
            continue;
        }

        /* need to calculate*/
        offset = index * 4 + i;

        MEA_OS_memcpy(&polcer_entry, &entry->policer_vec[i].sla_params, sizeof(MEA_Policer_Entry_dbt));
        
        if (mea_drv_check_Ingress_flow_Policer_prof(&polcer_entry) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_check_Ingress_flow_Policer_prof failed ", __FUNCTION__);
            return MEA_ERROR;
        }
        entry->policer_vec[i].tmId = offset;
        if (mea_drv_Ingress_flow_RateMeter_UpdateHw(offset, &polcer_entry, entry->policer_vec[i].tmId_enable, i) != MEA_OK){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_Port_RateMeter_UpdateHw failed ", __FUNCTION__);
            return MEA_ERROR;
        }

        MEA_OS_memcpy(&MEA_Ingress_Flow_policer_Table[profId].Ingress_Flow_policer.policer_vec[i], &entry->policer_vec[i], sizeof(MEA_Ingress_Flow_policer_Table[0].Ingress_Flow_policer.policer_vec[0]));
    }



    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Get_Ingress_flow_policer  >                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status  MEA_API_Get_Ingress_flow_policer(MEA_Unit_t unit, MEA_Uint16 profId, mea_Ingress_Flow_policer_dbt *entry)
{

    if (!MEA_IS_INGRESS_FLOW_POLICER_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow not support On this hw ", __FUNCTION__, profId);
        return MEA_ERROR;
    }

    /*check the profile id range */
    if (profId >= (MEA_Uint16)MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d is out from range  ", __FUNCTION__, profId);
        return MEA_ERROR;
    }

    if (MEA_Ingress_Flow_policer_Table[profId].valid == MEA_FALSE) {
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d is not created  ", __FUNCTION__, profId);
        return MEA_ERROR;
    }


    MEA_OS_memcpy(entry, &MEA_Ingress_Flow_policer_Table[profId].Ingress_Flow_policer.policer_vec  , sizeof(mea_Ingress_Flow_policer_dbt));

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*            <MEA_API_Ingress_flow_policer_Prof_IsExist  >                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status  MEA_API_Ingress_flow_policer_Prof_IsExist(MEA_Unit_t unit, MEA_Uint16 profId, MEA_Bool *IsExist)
{
   
    
    
    if (!MEA_IS_INGRESS_FLOW_POLICER_SUPPORT){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow not support On this hw ", __FUNCTION__, profId);
        return MEA_ERROR;
    }

    /*check the profile id range */
    if (profId >= (MEA_Uint16)MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE){
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - The ingress flow profile %d is out from range  ", __FUNCTION__, profId);
        return MEA_ERROR;
    }
    if (IsExist == NULL){
        return MEA_ERROR;
    }
    *IsExist = MEA_FALSE;

    if (MEA_Ingress_Flow_policer_Table[profId].valid == MEA_TRUE) {
        *IsExist = MEA_TRUE;
    
    }



#if 0
    for (i = 0; i < MEA_INGRESS_PORT_POLICER_TYPE_LAST; i++) {

        /* Ignore TOTAL Policer for the HW */
        if (i == MEA_INGRESS_PORT_POLICER_TYPE_TOTAL) {
            continue;
        }
        if (MEA_Ingress_Flow_policer_Table[profId].policer_vec[i].tmId_enable == MEA_TRUE){
            *IsExist = MEA_TRUE;
            return MEA_OK;
        }
    
    }
#endif    
    return MEA_OK;
}


MEA_Status  mea_drv_Ingress_flow_policer_Init(MEA_Unit_t unit)
{
    MEA_Uint32 size;

    if (!MEA_IS_INGRESS_FLOW_POLICER_SUPPORT)
        return MEA_OK;

    MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Initialize Ingress flow policer  ...\n");

    


    /* Allocate Table */
    size = MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE * sizeof(mea_Ingress_Flow_policer_Table_dbt);
    if (size != 0) {
        MEA_Ingress_Flow_policer_Table = (mea_Ingress_Flow_policer_Table_dbt   *)MEA_OS_malloc(size);
        if (MEA_Ingress_Flow_policer_Table == NULL) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - Allocate MEA_Ingress_Flow_policer_Table    failed (size=%d)\n",
                __FUNCTION__, size);
            return MEA_ERROR;
        }
        MEA_OS_memset(&(MEA_Ingress_Flow_policer_Table[0]), 0, size);
    }


    
    return MEA_OK;
}

MEA_Status  mea_drv_Ingress_flow_policer_Reinit(MEA_Unit_t unit)
{ 
    MEA_Uint32 i;
    MEA_Uint32 index, offset;
    MEA_Uint32 profId;
    
    if (!MEA_IS_INGRESS_FLOW_POLICER_SUPPORT)
        return MEA_OK;
    
    for (profId = 0; profId < MEA_INGRESS_FLOW_POLICER_MAX_OF_PROFILE; profId++){
        index = profId;
        for (i = 0; i < MEA_INGRESS_PORT_POLICER_TYPE_LAST; i++) {

            /* Ignore TOTAL Policer for the HW */
            if (i == MEA_INGRESS_PORT_POLICER_TYPE_TOTAL) {
                continue;
            }

            if (MEA_Ingress_Flow_policer_Table[index].Ingress_Flow_policer.policer_vec[i].tmId_enable == MEA_FALSE)
                continue;

            /* need to calculate*/
            offset = index * 4 + i;



            if (mea_drv_Ingress_flow_RateMeter_UpdateHw(offset, &MEA_Ingress_Flow_policer_Table[index].Ingress_Flow_policer.policer_vec[i].sla_params, MEA_Ingress_Flow_policer_Table[index].Ingress_Flow_policer.policer_vec[i].tmId_enable, i) != MEA_OK){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s - mea_drv_Ingress_flow_RateMeter_UpdateHw failed ", __FUNCTION__);

            }

        }
    }


    
    return MEA_OK;
}

MEA_Status  mea_drv_Ingress_flow_policer_Conclude(MEA_Unit_t unit)
{
    if (!MEA_IS_INGRESS_FLOW_POLICER_SUPPORT){
        return MEA_OK;
    }
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "Conclude Ingress flow policer..\n");

        /* Free the table */
        if (MEA_Ingress_Flow_policer_Table != NULL) {
            MEA_OS_free(MEA_Ingress_Flow_policer_Table);
            MEA_Ingress_Flow_policer_Table = NULL;
        }

    return MEA_OK;
}



