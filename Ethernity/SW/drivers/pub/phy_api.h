#ifdef MEA_OS_EPC_ZYNQ7045

#ifndef __PHY_API_H__
#define __PHY_API_H__

#define PHY_RC_OK           0
#define PHY_RC_ERROR        -1

#define PHY_SPEED_10        0
#define PHY_SPEED_100       1
#define PHY_SPEED_1000      2

#define PHY_AUTONEG_DISABLE 0
#define PHY_AUTONEG_ENABLE  1

#define PHY_DUPLEX_HALF     0
#define PHY_DUPLEX_FULL     1

#define PHY_AN_INPROGRESS   0
#define PHY_AN_COMPLETE     1
#define PHY_AN_FAIL         2
#define PHY_AN_DISABLE      3

#ifdef __cplusplus
    extern "C" {
#endif

/// Structure holds PHY specific initial values for word (16-bit width) access.
typedef struct
{
    unsigned char   page;   // PHY internal memory page number
    unsigned char   reg;    // Register address
    unsigned short  value;  // Value to write 
    unsigned short  mask;   // Bits of interest
    unsigned short  test;   // Access test type (RO/WO/RW)
    
} phy_init_t;

void phy_set_netdev(char *name);
int phy_netdev_exist(char *name);
int phy_reset(unsigned char id);
int phy_restart_an(unsigned char id);
int phy_set_speed(unsigned char id, unsigned char speed);
int phy_set_an(unsigned char id, unsigned char an);
int phy_set_duplex(unsigned char id, unsigned char duplex);

int phy_get_speed(unsigned char id);
int phy_get_duplex(unsigned char id);
int phy_get_an(unsigned char id);
int phy_get_an_status(unsigned char id);
int phy_get_link(unsigned char id);
int phy_get_oui(unsigned char id);
int phy_get_model(unsigned char id);

int phy_init(unsigned char id, phy_init_t *table, unsigned short size);
unsigned long phy_mdio_write(unsigned char id, unsigned short addr, unsigned short value);
unsigned long phy_mdio_read(unsigned char id, unsigned short addr, unsigned short *value);
#endif

#ifdef __cplusplus
       }
#endif/* __PHY_API_H__ */

#endif 


/* EOF */
