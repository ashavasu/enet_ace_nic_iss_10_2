/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish 
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_API_H
#define MEA_API_H


#include "MEA_platform.h"
#include "ENET_platform.h"

#ifdef __cplusplus
extern "C" {
#endif 

typedef  struct{
        MEA_Uint32 out_ports_0_31;        // bit fields
        MEA_Uint32 out_ports_32_63;
        MEA_Uint32 out_ports_64_95;
        MEA_Uint32 out_ports_96_127;

#ifdef MEA_OUT_PORT_128_255
        MEA_Uint32 out_ports_128_159;
        MEA_Uint32 out_ports_160_191;
        MEA_Uint32 out_ports_192_223;
        MEA_Uint32 out_ports_224_255;
#endif 

#ifdef MEA_OUT_PORT_256_511
        MEA_Uint32 out_ports_256_287;
        MEA_Uint32 out_ports_288_319;
        MEA_Uint32 out_ports_320_351;
        MEA_Uint32 out_ports_352_383;

        MEA_Uint32 out_ports_384_415;
        MEA_Uint32 out_ports_416_447;
        MEA_Uint32 out_ports_448_479;
        MEA_Uint32 out_ports_480_511;
#endif

#ifdef MEA_OUT_PORT_512_1023
        MEA_Uint32 out_ports_512_543;
        MEA_Uint32 out_ports_544_575;
        MEA_Uint32 out_ports_576_607;
        MEA_Uint32 out_ports_608_639;
        MEA_Uint32 out_ports_640_671;
        MEA_Uint32 out_ports_672_703;
        MEA_Uint32 out_ports_704_735;
        MEA_Uint32 out_ports_736_767;

        MEA_Uint32 out_ports_768_799;
        MEA_Uint32 out_ports_800_831;
        MEA_Uint32 out_ports_832_863;
        MEA_Uint32 out_ports_864_895;
        MEA_Uint32 out_ports_896_927;
        MEA_Uint32 out_ports_928_959;
        MEA_Uint32 out_ports_960_991;
        MEA_Uint32 out_ports_992_1023;
#endif

} MEA_OutPorts_Entry_dbt;








MEA_Status MEA_API_Set_Warm_Restart(void);
MEA_Status MEA_API_Clear_Warm_Restart(void);
MEA_Bool MEA_API_Get_Warm_Restart(void);








typedef struct {

    MEA_Bool   valid;


    char        PRODUCT_NAME[50];  /*  *PRODUCT_NAME  */
    MEA_Bool    MEA_PRODUCT_NAME_enable;
    char        SYSTEM_NAME[50];   /*  *SYSTEM_NAME  */
    MEA_Bool    MEA_SYSTEM_NAME_enable;


    MEA_Uint32 MEA_PERIODIC_INTERVAL;
    MEA_Uint32 MEA_PERIODIC_INTERVAL_enable;

    MEA_Bool  MEA_PERIODIC_ENABLE;
    MEA_Bool  MEA_PERIODIC_ENABLE_enable;

    MEA_Uint32 MEA_JUMBO_TYPE_IN;
    MEA_Bool   MEA_JUMBO_TYPE_IN_enable;

	MEA_Uint32 MEA_NUM_OF_SYS_MN;
	MEA_Bool   MEA_NUM_OF_SYS_MN_enable;


    MEA_Uint32 MEA_NUM_OF_DESCRIPTORS;
    MEA_Bool MEA_NUM_OF_DESCRIPTORS_enable;

    MEA_Uint32 MEA_NUM_DATA_BUFF;
    MEA_Bool   MEA_NUM_DATA_BUFF_enable;

    MEA_Uint32 MEA_DEBUG_REGS_Enable_On_Init;
    MEA_Bool MEA_DEBUG_REGS_Enable_On_Init_enable;

    MEA_Uint32 MEA_CLEAR_DDR;
    MEA_Bool MEA_CLEAR_DDR_Enable;


    MEA_Uint32 MEA_REINIT_DISABLE;
    MEA_Bool MEA_REINIT_DISABLE_enable;


    MEA_Bool MEA_CLI_ENG_DISABLE;
    MEA_Bool   MEA_CLI_ENG_DISABLE_enable;

    



    char MEA_PCIE_slot[50];
    MEA_Bool MEA_PCIE_slot_enable;

    char MEA_PCIE_DiviceName[50];
    MEA_Bool MEA_PCIE_DiviceName_enable;

    
    


    char MEA_DEVICE_CPU_ETH[50];
    MEA_Bool MEA_DEVICE_CPU_ETH_enable;

    MEA_Uint32 MEA_ENET_CLI_PORT;
    MEA_Bool MEA_ENET_CLI_PORT_enable;



    MEA_Bool  MEA_ROM_ENABLE;
    
    MEA_Bool MEA_WARM_ENABLE_en;
    MEA_Bool MEA_WARM_ENABLE;
    

    char MEA_DEVICE_FPGA_PATH_MCS[256];
    MEA_Bool MEA_DEVICE_FPGA_PATH_MCS_enable;

    MEA_Uint32 MEA_DEVICE_SYS_CLK;
    MEA_Bool MEA_DEVICE_SYS_CLK_enable;

    MEA_Bool MEA_WD_DISABLE_ENV;
    MEA_Bool MEA_WD_DISABLE_ENV_enable;

    MEA_Bool MEA_FWD_DISABLE;
    MEA_Bool MEA_FWD_DISABLE_ebable;

    MEA_Bool MEA_GW_VPLS_EN;
    MEA_Bool MEA_GW_VPLS_EN_ebable;

    MEA_Bool  MEA_SFP_SUPPORT;
    MEA_Bool  MEA_SFP_SUPPORT_enable;


    MEA_Bool MEA_WITH_IPC;
    MEA_Bool MEA_WITH_IPC_enable;

    MEA_Uint32 MEA_PRE_SCH_PROF;
    MEA_Bool MEA_PRE_SCH_PROF_enable;

    MEA_Bool MEA_PORT_EGRESS_UPDATE_LINK;
    MEA_Bool MEA_PORT_EGRESS_UPDATE_LINK_enable;

    MEA_Bool MEA_PORT_INGRESS_UPDATE_LINK;
    MEA_Bool MEA_PORT_INGRESS_UPDATE_LINK_enable;

    MEA_Bool MEA_PORT_BONDING_LINK;
    MEA_Bool MEA_PORT_BONDING_LINK_enable;

    MEA_Bool MEA_PIGGY_RGMII_BOARD_A;
    MEA_Bool MEA_PIGGY_RGMII_BOARD_A_enable;

    MEA_Bool MEA_SW_RESET_DEVICE_SUPPORT;
    MEA_Bool MEA_SW_RESET_DEVICE_SUPPORT_enable;

    MEA_Bool    MEA_DEVICE_SET_HOST_MAC_enable;
    MEA_MacAddr MEA_DEVICE_SET_HOST_MAC;

    MEA_Uint32 MEA_DEVICE_DB_ACL5_EX;
    MEA_Bool MEA_DEVICE_DB_ACL5_EX_enable;

    MEA_Uint32 MEA_DEVICE_DB_SERVICE_EX;
    MEA_Bool MEA_DEVICE_DB_SERVICE_EX_enable;
    
    MEA_Uint32 MEA_DEVICE_DB_ACTION;
    MEA_Bool MEA_DEVICE_DB_ACTION_enable;

    MEA_Uint32 MEA_DEVICE_DB_EDITING;
    MEA_Bool MEA_DEVICE_DB_EDITING_enable;

    MEA_Uint32 MEA_DEVICE_DB_HPM;
    MEA_Bool MEA_DEVICE_DB_HPM_enable;

    MEA_Uint32 MEA_DEVICE_LAG_TYPE;
    MEA_Bool   MEA_DEVICE_LAG_TYPE_enable;

    MEA_Uint32 MEA_DEFAULT_LAG_SUPPORTED;
    MEA_Bool   MEA_DEFAULT_LAG_SUPPORTED_enable;

    MEA_Uint32 MEA_BAR0_SET;
    MEA_Bool   MEA_BAR0_SET_enable;

    /*TDM BOARD */
    MEA_Uint32 MEA_TDM_CES_D_T;
    MEA_Uint32 MEA_TDM_CES_D_T_enable;

    MEA_Uint32 MEA_TDM_NUM_OF_BUFFER;
    MEA_Uint32 MEA_TDM_NUM_OF_BUFFER_enable;

    MEA_Uint32 MEA_TMUX_SLICE;
    MEA_Uint32 MEA_TMUX_SLICE_enable;

    MEA_Uint32 MEA_TMUX_SPE;
    MEA_Uint32 MEA_TMUX_SPE_enable;

    MEA_Uint32 MEA_DEVICE_ITHI;
    MEA_Uint32 MEA_DEVICE_ITHI_enable;


}mea_global_environment_dbt;





/*************************  INDEX- MAIN ******************************************/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*             MEA API file index                                                */
/*    1. Driver module (construction / destruction )                              */
/*             1.1 driver init                             (basic)               */
/*             1.2 driver conclude                                               */
/*             1.3 Device version                          (advanced - option)   */
/*    2. databases Object                                                         */
/*             2.1 Group Methods                                                */
/*             2.2 Group element Methods                                         */
/*    3 provisioning.                                                            */
/*    3.1 port                                                                   */
/*         3.1.1 Ingress                                                         */
/*             3.1.1.1 Ingress port                        (basic)               */
/*             3.1.1.2 Ingress parser                      (advanced - option)   */
/*             3.1.1.3 Ingress pre-parser                  (advanced - option)   */
/*             3.1.1.4 Ingress L2CP                        (advanced - option)   */
/*         3.1.2 Virtual Ingress Cluster                                         */
/*         3.1.3 Egress                                                          */
/*             3.1.3.1 port Shaper                         (advanced - option)   */
/*             3.1.3.2 Egress port                         (Basic)               */
/*             3.1.3.3 W-Red                                (advanced - option)   */
/*         3.1.3 Virtual Egress Cluster                                          */
/*             3.1.4.1 Egress Queue cluster                                      */
/*    3.2 Actions                                                                */
/*             3.2.1 Edit                                  (advanced - option)   */
/*             3.2.2 policer                               (advanced - option)   */
/*             3.2.3 counter profile                       (advanced - option)   */
/*             3.2.4 action profile                        (advanced - option)   */
/*    3.3 classification                                                          */
/*             3.3.1 service - classifier                   (basic)               */
/*             3.3.2 filter                                (advanced - option)   */
/*             3.3.3 Search Engine (SE)  (Forwarder)                             */
/*             3.3.3.1 Data Search Engine(and switching)                         */
/*             3.3.3.2 Limiter                             (advanced - option)   */
/*             3.3.4.1 FlowCoSmappingProfile               (advanced - option)   */
/*             3.3.4.2 FlowMarkingmappingProfile           (advanced - option)   */
/*             3.3.4.3 mapping                             (advanced - option)   */
/*    3.4 packet generator and analyzer                    (advanced - option)   */
/*    4 performance monitoring and counters.                                     */
/*             4.1 RMON counters                                                 */
/*             4.2 Performance monitor ( see also section 2.4.2)                 */
/*    5 maintenance                                                              */
/*             5.1 Event Attributes                      (advanced - option)     */
/*             5.2 Low level device access               (advanced - option)     */
/*             5.3 scheduler                             (advanced - option)     */
/*             5.4 check unit validation                 (advanced - option)     */
/*             5.5 Global Attributes                     (advanced - option)     */
/*             5.6 Device-Info                            (advanced - option)     */
/*    6 legacy methods                                                          */
/*                                                                               */
/*********************************************************************************/

/************************* INDEX-  section 1.  driver basic **********************/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*    1. Driver module (construction / destruction )                             */
/*             1.1 driver init                           (basic)                 */
/*             1.2 driver conclude                                               */
/*             1.3 Device version                        (advanced - option)     */
/*                                                                               */
/*                                                                               */
/*********************************************************************************/

/******************************************************************************
* Function Name:       < MEA_API_Init>
*                       
* Description: Initializes (Constructor) MEA Driver         
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*         unit_i                  -> identifier of the device.
*         moduule_base_addr_vec_i -> The base address of each device module
*                                    One Address for each enum value in 
*                                    enum MEA_module_te 
*                    
* Output Parameters:  
*                   none            
*
* Return values:
*     MEA_OK, the function performed the function correctly.
*     MEA_ERROR, the function did not perform the function.
*
*******************************************************************************/
MEA_Status MEA_API_Device_environment_Init(mea_global_environment_dbt  *entry);

void MEA_PCI_Init_bdf(void);

MEA_Status MEA_API_ENV_SET(mea_global_environment_dbt *entry);

MEA_Status MEA_API_Init(MEA_Unit_t unit_i,
                        MEA_ULong_t * module_base_addr_vec_i);

MEA_Status MEA_API_Init_Device_Start(void);
MEA_Status MEA_API_DeInit_Device_End(void);

/************************************************************************/
/*  Overall Control of Interrupts                                       */
/************************************************************************/
MEA_Status MEA_API_IF_InterrUp_Enable(MEA_Unit_t  unit, MEA_Bool Enable);
MEA_Status MEA_API_IF_InterrUp_Set_MASK(MEA_Unit_t  unit, MEA_Uint32 SetValue);
MEA_Status MEA_API_IF_InterrUp_Get_Status(MEA_Unit_t  unit, MEA_Uint32 *GetValue);
/*----------------------------------------------------------------------*/
MEA_Status MEA_API_BM_InterrUp_Enable(MEA_Unit_t  unit, MEA_Bool Enable);
MEA_Status MEA_API_BM_InterrUp_Set_MASK  (MEA_Unit_t  unit, MEA_Uint32 SetValue);
MEA_Status MEA_API_BM_InterrUp_Get_Status(MEA_Unit_t  unit, MEA_Uint32 *GetValue);

/******************************************************************************
* Function Name: < MEA_API_Delete_all_entities>
*                
* Description: delete all entities (service/filter/limiter/Action/lxcp)  (Destructor) MEA Driver         
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*                   unit_i                  -> identifier of the device.
*                    
* Output Parameters:  
*                   none            
* Input Parameters: 
*
* Return values:
*     MEA_OK, the function performed the function correctly.
*     MEA_ERROR, the function did not perform the function.
*
*******************************************************************************/
MEA_Status MEA_API_Delete_all_entities(MEA_Unit_t unit);


/******************************************************************************
* Function Name: < MEA_API_Conclude>
*                
* Description: Conclude (Destructor) MEA Driver         
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*                   unit_i                  -> identifier of the device.
*                    
* Output Parameters:  
*                   none            
* Input Parameters: 
*
* Return values:
*     MEA_OK, the function performed the function correctly.
*     MEA_ERROR, the function did not perform the function.
*
*******************************************************************************/
MEA_Status MEA_API_Conclude(MEA_Unit_t unit_i);

/******************************************************************************
* Function Name:       < MEA_API_ReInit>
*                       
* Description: Re Initializes the Device with all the driver shadows information.
*              Assumption: The reset for the Device already done before call
*              To this API.
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*         unit_i                  -> identifier of the device.
*                    
* Output Parameters:  
*                   none            
*
* Return values:
*     MEA_OK, the function performed the function correctly.
*     MEA_ERROR, the function did not perform the function.
*
*******************************************************************************/
MEA_Status MEA_API_ReInit(MEA_Unit_t unit_i);

/******************************************************************************
* Function Name:       < MEA_API_CheckDeviceHealth>
*                       
* Description: Check the health of the device
*              This API should be call periodically by the user.
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*         unit_i                  -> identifier of the device.
*                    
* Output Parameters:  
*         info_po - pointer to output bit mask that indicate health status.
*                   0 - Good , 1 - Bad
*
* Return values:
*     MEA_OK, the function performed the function correctly.
*     MEA_ERROR, the function did not perform the function.
*
*******************************************************************************/
typedef union {
	struct {
		MEA_Uint32 queue_stuck:1;
		MEA_Uint32 pad:31;
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif

	} val;
	MEA_Uint32 raw;
} MEA_DeviceHealthInfo_dbt;
MEA_Status MEA_API_CheckDeviceHealth(MEA_Unit_t unit_i,
				     MEA_DeviceHealthInfo_dbt * info_po);

/******************************************************************************
* Function Name: < MEA_API_Get_SoftwareVersion >
*
* Description:   This function is responsible for retrieving the 
*                software driver version
*                      
******************************************************************************/


typedef struct {
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    /* 0.. 3*/ 
    MEA_Uint32 vlsi_int_version         : 4;
    /* 11..4 */ 
    MEA_Uint32 release_number           : 8;
    /* 15..12 */ 
    MEA_Uint32 phase_version     	    : 4;
    /* 31..16 */ 
    MEA_Uint32 product_number           : 16;

#ifdef ARCH64
	MEA_Uint32 pad64_0					:32;
#endif

#else

#ifdef ARCH64
	MEA_Uint32 pad64_0					:32;
#endif
    /* 31..16 */ 
    MEA_Uint32 product_number           : 16;
    /* 15..12 */ 
    MEA_Uint32 phase_version            : 4;
    /* 11..8 */ 
    MEA_Uint32 release_number           : 8;
    /* 0.. 3*/ 
    MEA_Uint32 vlsi_int_version         : 4;
#endif
        } val;
    MEA_Uint32 reg_ver;
    }if_reg_HWversion;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    /* 7.. 0*/ 
     MEA_Uint32 hour             : 8;
    /* 15..8 */  
    MEA_Uint32 year              : 8;
    /* 23..16 */ 
    MEA_Uint32 month             : 8;
    /* 31..24 */ 
    MEA_Uint32 day               : 8;
#ifdef ARCH64
	MEA_Uint32 pad64_0			 :32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0			 :32;
#endif
    /* 31..24 */ 
    MEA_Uint32 day               : 8;
    /* 23..16 */ 
    MEA_Uint32 month             : 8; 
    /* 15..8 */ 
    MEA_Uint32 year              : 8;
    /* 7.. 0*/ 
    MEA_Uint32 hour               : 8;
#endif
        } val;
    MEA_Uint32 reg_ver;
    }if_reg_HWdate;
}MEA_HwVersion_info_t;
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Version APIs                                           */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_SoftwareVersion(char *buffer, MEA_Uint32 len);

MEA_Status MEA_API_Get_HwVersion(MEA_HwVersion_info_t * version);

MEA_Status MEA_API_Get_SoftwareBuildTag(char *buffer, MEA_Uint32 len);
MEA_Status MEA_API_Get_ChipName(char*      buffer, MEA_Uint32 len);


MEA_Status MEA_API_Get_TmuxVersion(char*      buffer,MEA_Uint32 len);


typedef void(*adap_print_version)(char *buff, MEA_Uint32 len);

MEA_Status MEA_SetAdapCallBackFunction(adap_print_version iss_callback);
MEA_Status MEA_GetAdapCallBackFunction(char *buffer, MEA_Uint32 len);
MEA_Status MEA_SetNpapiCallBackFunction(adap_print_version iss_callback);
MEA_Status MEA_GetNpapiCallBackFunction(char *buffer, MEA_Uint32 len);

typedef MEA_Status(*reset_func)(int);

MEA_Status MEA_API_RegisterFpgaSwReset(reset_func function);
MEA_Status MEA_API_RegisterFpgaHwReset(reset_func function);

/******************************************************************************
* Function Name: < MEA_API_IsValid_Unit>
*                
* Description: Check if the unit id is valid and exist
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*                   unit_i   -> identifier of the device.
*                   silent_i -> MEA_FALSE - Print message
*                               MEA_TRUE  - Quiet mode
*                    
* Output Parameters:  
*                   none            
* Input Parameters: 
*
* Return values:
*     MEA_TRUE, the unit id is valid and exist
*     MEA_FALSE, the unit id is not valid or not exist
*
*******************************************************************************/
MEA_Bool MEA_API_IsValid_Unit(MEA_Unit_t unit_i, MEA_Bool silent_i);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA API Lock/Unlock                                        */
/*                                                                               */
/* Note: The caller is responsible to lock/unlock the driver                     */
/*       ===================================================                     */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Lock(void);
MEA_Status MEA_API_Unlock(void);




/* INTERFACE*/

typedef enum {
    MEA_INTERFACETYPE_NONE             =0,
    MEA_INTERFACETYPE_GMII             =1,
    MEA_INTERFACETYPE_RGMII            =2,
    MEA_INTERFACETYPE_MII              =3,
    MEA_INTERFACETYPE_SSSMII           =4,
    MEA_INTERFACETYPE_UTOPIA           =5,
    MEA_INTERFACETYPE_POS2             =6,
    MEA_INTERFACETYPE_SGMII            =7, 
    MEA_INTERFACETYPE_AURORA           =8,
    MEA_INTERFACETYPE_INTERNAL_TLS     =9,
    MEA_INTERFACETYPE_EXTERNAL_Radio   =10, 
    MEA_INTERFACETYPE_TDM              =11,
    MEA_INTERFACETYPE_XGMII            =12,
    MEA_INTERFACETYPE_sXAUI            =13, 
    MEA_INTERFACETYPE_RMII             =14,
    MEA_INTERFACETYPE_XAUI             =15, 
    MEA_INTERFACETYPE_Interlaken       =16,
    MEA_INTERFACETYPE_RXAUI            =17,
    MEA_INTERFACETYPE_SINGLE10G        =18,
    MEA_INTERFACETYPE_PCI_e            =19,
    MEA_INTERFACETYPE_QSGMII           =20,
    MEA_INTERFACETYPE_XLAUI            =21,
    MEA_INTERFACETYPE_CAUI             =22,
    MEA_INTERFACETYPE_CAUI_4           =23,
    MEA_INTERFACETYPE_ENHANCED_TLS     = 24,

    MEA_INTERFACETYPE_LAST
} MEA_InterfaceType_t;
  
typedef enum {
    MEA_INTERFACE_Rate_NONE=0,
    MEA_INTERFACE_Rate_10Mbs= 1,
    MEA_INTERFACE_Rate_100Mbs= 2,
    MEA_INTERFACE_Rate_1Gbs= 3,
    MEA_INTERFACE_Rate_2_5Gbs= 4,
    MEA_INTERFACE_Rate_10Gbs= 5,
    MEA_INTERFACE_Rate_40Gbs= 6,
    MEA_INTERFACE_Rate_100Gbs= 7,
    MEA_INTERFACE_Rate_LAST= 8,

}MEA_InterfaceRate_t;

typedef enum {
    MEA_PORTTYPE_NONE               =0,
    MEA_PORTTYPE_E1_T1_T1_193B      =1,
    MEA_PORTTYPE_AAL5_ATM_TC        =2,
    MEA_PORTTYPE_GBE_MAC            =3,
    MEA_PORTTYPE_GBE_1588           =4,
    MEA_PORTTYPE_10G_XMAC_ENET      =5,
    MEA_PORTTYPE_10_XMAC_Xilinx     =6,
    MEA_PORTTYPE_G999_1MAC          =7, 
    MEA_PORTTYPE_G999_1MAC_1588     =8, 
    MEA_PORTTYPE_AURORA_MAC         =9,
    MEA_PORTTYPE_Interlaken_MAC     =10, 
    MEA_PORTTYPE_GENERATOR_ANLAZER  =11,
    MEA_PORTTYPE_40GBE              =12,
    MEA_PORTTYPE_100GBE             =13, 
    MEA_PORTTYPE_VIRTUAL            =14, 
    
    MEA_PORTTYPE_LAST
} MEA_Port_type_te;





typedef union{

    
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 Port_type            :5;  
            MEA_Uint32 interface_ID         :7;
            MEA_Uint32 port_sliceId         :2;
            MEA_Uint32 reserved1            :2;
            MEA_Uint32 Interface_type       :5;
            MEA_Uint32 Interface_BW         :4;
            MEA_Uint32 Interface_Hw_index   :5;
            MEA_Uint32 Interface_Packet_mode : 1;
            MEA_Uint32 reserved2            :1;
#ifdef ARCH64
	MEA_Uint32 pad64_0					    :32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0					    :32;
#endif
            MEA_Uint32 reserved2             :1;
            MEA_Uint32 Interface_Packet_mode :1;
            MEA_Uint32 Interface_Hw_index   :5;
            MEA_Uint32 Interface_BW         :4;
            MEA_Uint32 Interface_type       :5;
            MEA_Uint32 reserved1            :2;
            MEA_Uint32 port_sliceId         :2;
            MEA_Uint32 interface_ID         :7;
            MEA_Uint32 Port_type            :5;
#endif
        }val;
        MEA_Uint32 index_regs[1];
    


    

}MEA_drv_port2Interface_dbt;




typedef struct{

    MEA_Bool valid;
    MEA_InterfaceType_t     InterfacType;
    MEA_Uint8               Interface_BW;
    MEA_Uint8               Interface_Hw_index;
    MEA_OutPorts_Entry_dbt  OutPorts;
    MEA_Bool                ShaperEnable_On_interface;
    MEA_Uint8               Packet_mode;
    char                   str_rate[20];


    

}MEA_drv_Interface2Port_dbt;






/************************* INDEX-  section 3.1.1 Ingress port    *****************/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*         3.1.1 Ingress                                                         */
/*             3.1.1.1 Ingress port                        (basic)               */
/*             3.1.1.2 Ingress parser                       (advanced - option)   */
/*             3.1.1.3 Ingress pre-parser                   (advanced - option)   */
/*             3.1.1.4 Ingress L2CP                        (advanced - option)   */
/*         3.1.2 Virtual Ingress Cluster                                         */
/*                                                                               */
/*********************************************************************************/
typedef enum {
	MEA_PORTS_TYPE_INGRESS_TYPE = 0,
	MEA_PORTS_TYPE_EGRESS_TYPE,
	MEA_PORTS_TYPE_CLUSTER_TYPE,
	MEA_PORTS_TYPE_PHYISCAL_TYPE,
	MEA_PORTS_TYPE_TO_INTERFACE_TYPE,
	MEA_PORTS_TYPE_VIRTUAL_INGRESS_TYPE,
	MEA_PORTS_TYPE_LAST_TYPE
} MEA_PortKeyType_t;
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    <IsPortValid APIs                                          */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Bool MEA_API_Get_IsPortValid(MEA_Port_t port, MEA_PortKeyType_t type,MEA_Bool silent);

MEA_Bool MEA_API_Get_Is_VirtualPort_Valid(MEA_Port_t port,MEA_Bool silent);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Ingress Port Table Defines                             */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define MEA_INGRESS_PORT_MTU_MAX_VALUE(port) (MEA_Platform_GetMaxMTU())
#define MEA_INGRESS_PORT_MTU_GRANULARITY_VALUE  64

/* number of possible network Priorities    */
#define MEA_INGRESS_PORT_NET_PRI_NUM  8 


#define MEA_WILDCARD_ALL_SRC_PORTS            0x0000007F
#define MEA_WILDCARD_ALL_NTAGS                (MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ? (0x0001FFFF) : (0x00FFFFFF)
#define MEA_WILDCARD_ALL_PRI                  (0x00000007) 
typedef  enum{
    MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP=0,
    MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_PRECEDENCE=1,
    MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_TOS =       2,
    MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_DSCP =      3,
    MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_LAST
}MEA_IngressPort_Ip_pri_type_t;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Ingress Port Table Typedefs                            */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


/* Ingress port protocol profiles */
typedef  enum{
	MEA_INGRESS_PORT_PROTO_TRANS = 0,
	MEA_INGRESS_PORT_PROTO_VLAN = 1,
	MEA_INGRESS_PORT_PROTO_QTAG = 2,
	MEA_INGRESS_PORT_PROTO_MARTINI = 3,
	MEA_INGRESS_PORT_PROTO_MPLS = 4,
	MEA_INGRESS_PORT_PROTO_EoLLCoATM = 5,
	MEA_INGRESS_PORT_PROTO_IP = 6,
	MEA_INGRESS_PORT_PROTO_IPoVLAN = 7,
	MEA_INGRESS_PORT_PROTO_IPoEoLLCoATM = 8,
	MEA_INGRESS_PORT_PROTO_PPPoEoLLCoATM = 9,
	MEA_INGRESS_PORT_PROTO_EoVcMUXoATM = 10,
	MEA_INGRESS_PORT_PROTO_IPoLLCoATM = 11,
	MEA_INGRESS_PORT_PROTO_IPoVcMUXoATM = 12,
	MEA_INGRESS_PORT_PROTO_PPPoLLCoATM = 13,
	MEA_INGRESS_PORT_PROTO_PPPoVcMUXoATM = 14,
	MEA_INGRESS_PORT_PROTO_PPPoEoVcMUXoATM = 15,
	MEA_INGRESS_PORT_PROTO_ETHER_TYPE = 16,
	MEA_INGRESS_PORT_PROTO_QTAG_OUTER = 17,
	MEA_INGRESS_PORT_PROTO_MPLSoE = 18,
    MEA_INGRESS_PORT_PROTO_RESERV = 19,
    MEA_INGRESS_PORT_PROTO_DC = 20,
    MEA_INGRESS_PORT_PROTO_TDM = 21,
    
    

    MEA_INGRESS_PORT_PROTO_LAST 
} MEA_IngressPort_Proto_t;


/* definition of 2 bits action that define for each port in the L2CP entry */
typedef enum {
   MEA_L2CP_ACTION_PEER = 0 ,
   MEA_L2CP_ACTION_CPU      ,
   MEA_L2CP_ACTION_RESERVED ,
   MEA_L2CP_ACTION_DISCARD  ,
   MEA_L2CP_ACTION_LAST
} MEA_L2CP_Action_te;

#define L2CP_ADDR_MLDV2 40

typedef union {
	struct {
		MEA_Uint32 Act_MacSuffix_0_15;	/* Each Mac suffix consumes 2 bits for action type */
		MEA_Uint32 Act_MacSuffix_16_31;
		MEA_Uint32 Act_MacSuffix_32_47;
		MEA_Uint32 Act_MacSuffix_48_63;
	} f;
	MEA_Uint32 reg[4];
} MEA_L2CP_Entry_dbt;

typedef enum {
	MEA_DEF_SID_ACTION_DISCARD = 0,
	MEA_DEF_SID_ACTION_CPU,
	MEA_DEF_SID_ACTION_DEF_SID,
	MEA_DEF_SID_ACTION_LAST
} MEA_Def_SID_Action_te;

typedef enum {
	MEA_FILTER_KEY_INTERFACE_TYPE_SOURCE_PORT = 0,
	MEA_FILTER_KEY_INTERFACE_TYPE_SID,
	MEA_FILTER_KEY_INTERFACE_TYPE_LAST
} MEA_Filter_key_interface_type_te;

typedef struct {
    MEA_Uint32 def_sid : 14;
    MEA_Uint32 action  : 2; /* MEA_Def_SID_Action_te */
    MEA_Uint32 valid   : 1;
    MEA_Uint32 pad0    : 15;
} MEA_Def_SID_dbt;


#define MEA_PARSER_DIST_SET_L2            0x1
#define MEA_PARSER_DIST_SET_L3            0x2
#define MEA_PARSER_DIST_SET_L4            0x4
#define MEA_PARSER_DIST_SET_MPLS          0x8
#define MEA_PARSER_DIST_SET_ALL           0xf

typedef enum {
    MEA_DEF_PORT_TYPE_SID_PB = 0,
    MEA_DEF_PORT_TYPE_SID_PPB = 1,
    MEA_DEF_PORT_TYPE_SID_MPLS_TP = 2,
    MEA_DEF_PORT_TYPE_SID_LAST
} MEA_def_Port_typeSid_te;


typedef struct {

    /* protocol definition 
       Notes - IP Awareness enable means that , when IP packet identified , 
               its IP info fields will override the bottom layer protocol 
               (for instance VLAN). 
               When this happen :
               - if priority awareness enable 
                 then QoS bits will be taken from IP DSCP(TOS) bits.
                      rather than the VLAN tag priority bits.
               - if color    awareness enable 
                 then color    will be taken from IP DSCP(TOS) bits ,
                      rather than the VLAN tag color    bits.
               - if not match by protocol and the parser discard the packet ,
                 the user can trap the packet to cpu , by set trap_discard_2_cpu to 1
    */
    MEA_Uint32  port_proto_prof     :  5; /* protocol profile                */
    MEA_Uint32  port_ip_aw          :  1; /* IP awareness                    */
    MEA_Uint32  port_cos_aw         :  1; /* priority awareness              */
    MEA_Uint32  port_col_aw         :  1; /* color awareness                 */
    MEA_Uint32  trap_discard_2_cpu  :  1; 
    MEA_Uint32  pad_w0              :  23;



    /* mask the net_tag/priority/src_port value extract by parser from the packet , 
       as input key to service table. 
       Note: !!! The mask done in OR operation !!! 
     */
    MEA_Uint32  net_wildcard_valid  :  1; /* Network tag wildcard mask valid */
    MEA_Uint32  net_wildcard        : 24; /* Network tag wildcard mask       */
    MEA_Uint32  pri_wildcard_valid  :  1; /* Priority    wildcard mask valid */
    MEA_Uint32  pad7                :  6; /* pad for alignment                 */
    
    MEA_Uint32  pri_wildcard        :  8; /* Priority    wildcard mask       */
    MEA_Uint32  sp_wildcard_valid   :  1; /* Source Port wildcard mask valid */
    MEA_Uint32  sp_wildcard         :  7; /* Source Port wildcard mask       */
    MEA_Uint32  pad8                : 16; /* pad for alignment                 */


    /* discard packet with DA not equal to specific value */
    MEA_Uint16  fwd_only_DA_valid   :  1; /* fwd only DA (valid)             */
    MEA_Uint16  fwd_only_DA_offset  :  6; /* fwd only DA (DA offset in packet*/
    MEA_Uint16  pad1                :  9; /* pad for alignment                 */
    MEA_MacAddr fwd_only_DA_value;        /* fwd only DA (DA value to compare)*/
    
    
    MEA_MacAddr my_Mac;
    
    
    /* network OAM info  
       Notes: - The default value for network_OAM_valid is MEA_FALSE
              - The default value for OAM EthType is 0x0000 and L2CP_TBL_Entry 0 
              - The user OAMs definition is done globally and not per port via 
                the APIs MEA_API_Get/Set_UserOAM_Entry 
     */
    MEA_Uint32  cfm_oam_untag              :  1;
    MEA_Uint32  cfm_oam_tag                :  1;
    MEA_Uint32  cfm_oam_qtag               :  1;

    MEA_Uint32  network_OAM_L2CP_Suffix    :  6; /* define the action if EthType match*/
    MEA_Uint32  network_OAM_Location       :  2; /* 00 - disable, 01 - check first Vlan, 10 - check second vlan, 11 - check both Vlans*/
    MEA_Uint32  local_MAC_enable           :  4;     /* 0xf all the globalMy mac enable */
    MEA_Uint32  pad3                       :  1; /* pad for alignment                  */
    MEA_Uint32  network_OAM_EthType_value  : 16; /* OAM EthType value to compare      */

    /* Network / User L2CP info  
       Notes: - The default value for network_L2CP_action_valid is MEA_FALSE 
              - The default value for all ports for all entries in the  
                network_l2cp_action_table is MEA_NETWORK_L2CP_ACTION_DEF_VAL 
              - The default value for Network L2CP BaseAddr is 
                MEA_NETWORK_L2CP_BASE_ADDR_DEF_VAL ,  and can be change by 
                API MEA_API_Set_Globals_Entry
              - To set/get network_L2CP_action_table field please use the MACROs 
                MEA_INGRESS_PORT_SET_L2CP_ACTION , MEA_INGRESS_PORT_GET_L2CP_ACTION 
                define above
              - The default value for all ports for all entries in the  
                user_l2cp_action_table is MEA_USER_L2CP_ACTION_DEF_VAL 
              - The default value for User L2CP BaseAddr is  
                MEA_USER_L2CP_BASE_ADDR_DEF_VAL , and can be change by API 
                MEA_API_Set_Globals_Entry 
              - To set/get user_L2CP_action_table field please use the MACROs 
                MEA_INGRESS_PORT_SET_L2CP_ACTION , MEA_INGRESS_PORT_GET_L2CP_ACTION 
                define above
     */
    MEA_Uint32  pad4                       : 31; /* pad for alignment                  */
    MEA_Uint32  network_L2CP_action_valid  :  1; /* Enable Compare DA to L2CP BaseAddr*/
    
    MEA_L2CP_Entry_dbt  network_L2CP_action_table;   //  2 bits for each port divide to 4 Uint32 

    MEA_Def_SID_dbt default_sid_info; /* 32b */

    MEA_Uint32  default_pri                : 3; 
    MEA_Uint32  afdx_enable                : 1;
    MEA_Uint32  default_net_tag_value      : 12; /* pvid for untagged */
    MEA_Uint32  allowed_tagged             : 1;  /*allowed (MEA_TRUE = 1) / not allowed ( MEA_FALSE = 0)*/
    MEA_Uint32  allowed_untagged           : 1;  /*allowed (MEA_TRUE = 1) / not allowed ( MEA_FALSE = 0)*/
    MEA_Uint32  DSA_Type_enable            : 1;  /*1 - enable*/
    MEA_Uint32  L4_SRC_DST                 : 1;  /*‘0’ – L4_SRC_PORT, ‘1’- L4_DST_PORT */
    MEA_Uint32  IP_pri_mask_valid          : 1;
    MEA_Uint32  IP_pri_mask_type           : 2; /*  ‘0’ – NON IP (IP priority no aware)  - should be the default value (simulation and software)
                                                    ‘1’ – PRECEDENCE (IP priority aware)
                                                    ‘2’ – TOS (IP priority aware)
                                                    ‘3’ – DSCP (IP priority aware)
                                                */
 
                                               


    MEA_Uint32  IP_pri_mask_IPv4           : 1;
    MEA_Uint32  IP_pri_mask_IPv6           : 1; /*1 - Enable*/
    
    MEA_Uint32  evc_external_internal       : 1;  /* 0-external 1 internal */
    MEA_Uint32  def_sid_port_type           : 2; /* see MEA_def_Port_typeSid_te - 0 - PB(l2_type = 0-6), 2 - PPB(l2_type = 7-10), 3 - MPLS-TP(l2_type = 16-23)*/
    MEA_Uint32  l2type_consolidate          : 1; /* enable only l2Type 0,1,2 valid*/
    MEA_Uint32  pad5                        : 3;

        /* to Map this port as part of Logical Port that has several
           ports in this LAG (Trunk) you need to assign all the
           src ports in this LAG to one src_port (normally the first 
           src port in the LAG) , and the services will be create 
           on this source port.
           Note: this logical source port is relevant only to classifier 
                 and not to the forwarder block , and not for stamping source
                 port in case send to CPU. 
        */
    MEA_Uint32 LAG_src_port_value          :  10;
    MEA_Uint32 LAG_src_port_valid          :  1; 
    MEA_Uint32 mapping_enable              :  1;
    MEA_Uint32 mapping_profile             :  9;
    MEA_Uint32 filter_key_interface_type   :  1; /*see type MEA_Filter_key_interface_type_te */
    MEA_Uint32 lag_distribution_mask       :  4;   /*see the MEA_PARSER_DIST_SET_ALL*/
    MEA_Uint32 vlanrange_prof_Id           :  6;

    MEA_Uint32 vlanrange_enable            : 1;
    MEA_Uint32 ENNI_enable                 : 1;
    MEA_Uint32 pad6                        : 30;
   
    MEA_Uint32  net2_wildcard_valid      : 1; /* Network2 tag wildcard mask valid */
    MEA_Uint32  net2_wildcard            :24; /* Network2 tag wildcard mask       */
    MEA_Uint32  l2_protocol_valid        : 1; /* l2 protocol type wildcard mask valid */
    MEA_Uint32  l2_protocol_wildcard     : 5; /* 2 protocol type wildcard mask       */
    MEA_Uint32  pad_8                     : 1;


} MEA_IngressPort_ParserInfo_dbt;


typedef struct  
{
    MEA_Uint32  time_stampeL ;

#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32  time_stampeM :16;
    MEA_Uint32  add_sub      :1; //0-add 1 sub
    MEA_Uint32  pad :15;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  pad          :15;
    MEA_Uint32  add_sub      :1; //0-add 1 sub
    MEA_Uint32  time_stampeM :16;
#endif

}MEA_IngressPort_TS_Cpu_offset_dbt;

typedef enum {
   MEA_INGRESS_PORT_POLICER_TYPE_MC,
   MEA_INGRESS_PORT_POLICER_TYPE_BC,
   MEA_INGRESS_PORT_POLICER_TYPE_UNICAST,     
   MEA_INGRESS_PORT_POLICER_TYPE_UNKNOWN,
   MEA_INGRESS_PORT_POLICER_TYPE_TOTAL,
   MEA_INGRESS_PORT_POLICER_TYPE_LAST
} MEA_IngressPort_PolicerType_te;

typedef enum {
MEA_POLICER_MODE_MEF=0,
MEA_POLICER_MODE_BAG=1,
MEA_POLICER_MODE_LAST
}MEA_Policer_Type_mode_t;

typedef struct MEA_Policer_Entry_dbs {
    MEA_Policer_Type_mode_t type_mode; 
    MEA_EbsCbs_t  CBS;  /* bytes 0..(MEA_POLICER_CBS_EBS_MAX_VAL*(2^<gn_type>)) */
    MEA_EbsCbs_t  EBS;  /* bytes 0..(MEA_POLICER_CBS_EBS_MAX_VAL*(2^<gn_type>)) */

    MEA_EirCir_t  CIR; /* bps */  /* on mode MEA_POLICER_MODE_BAG 1,2,4,8.. (mseec)*/
    MEA_EirCir_t  EIR; /* bps */

    MEA_Uint32    cup         :  1;  
    MEA_Uint32    comp        :  9;
    MEA_Uint32    color_aware :  1;
    MEA_Uint32    pad1        : 15; 

    MEA_Uint32  maxMtu;         /*0 will with max packet */

    /* Policer granularity type:
       - For CBS/EBS the granularity will be 
         (2^type) bytes.
       - For CIR/EIR the granularity will be 
         ((2^type)*MEA_SRV_POLICER_CIR_EIR_SLOW/FAST_GN) bps
       
              - gn_type=0 is mean 2^0=1 that has no effect */
    MEA_Uint32    gn_type     :  3;
    MEA_Uint32    gn_sign     :  1; // 0 -positive and  1 - negative granularity type
#if 0
    MEA_Uint32    soft_wred   :  2; // 00  - backward compatible  end of queue (offset=127)
                                    // 01 – offset = 96
                                    // 10 – offset = 64
                                    // 11 – offset = 32 
#endif

    MEA_Uint32  slow_mode  :1;

} MEA_Policer_Entry_dbt;

typedef enum { 
MEA_INGRESS_PORT_PROTECT_BYPASS=0,
MEA_INGRESS_PORT_PROTECT_1_PLUS_1=1,
MEA_INGRESS_PORT_PROTECT_LAST
}MEA_IngressPort_Protect_t;

typedef enum { 
    MEA_INGRESS_PORT_PROTECT_1P1_MASTER=0,
    MEA_INGRESS_PORT_PROTECT_1P1_SECONDARY=1,
    MEA_INGRESS_PORT_PROTECT_1P1_LAST
}MEA_IngressPort_Protect1p1_t;


typedef struct {
MEA_Bool enable;
MEA_IngressPort_Protect1p1_t master_secondary;
 

}MEA_IngressPort_Protect_1p1_dbt;


typedef enum {
    MEA_PTP1588_MODE_DISABLE=0,
    MEA_PTP1588_MODE_STAMPED=1,
    MEA_PTP1588_MODE_TRANS_E2E=2,
    MEA_PTP1588_MODE_TRANS_P2P=3,
    MEA_PTP1588_MODE_OC_BC_MASTER_NON_PEER=4,
    MEA_PTP1588_MODE_OC_BC_MASTER_PEER=5,
    MEA_PTP1588_MODE_OC_BC_SLAVE_NON_PEER=6,
    MEA_PTP1588_MODE_OC_BC_SLAVE_PEER=7,
    MEA_PTP1588_MODE_TRANS_P2P_OC_SLAVE_PEER=8,
    MEA_PTP1588_MODE_TRANS_P2P_OC_SLAVE_NON_PEER=9,
    MEA_PTP1588_MODE_LAST,

}MEA_PTP1588_Mode_Type_t;



typedef struct {
    MEA_Uint32  rx_enable         :  1;
    /* Relevant only to ATM ports that can be POS / Utopia */
    MEA_Uint32  utopia            :  1; /* 0=POS or 1=Utopia */
    MEA_Uint32  MTU               : 14;  /*Mtu needs must be with granularity of MEA_INGRESS_PORT_MTU_GRANULARITY_VALUE move to priQueue*/
    MEA_Uint32  crc_check         :  1;
    MEA_Uint32  GigaDisable       :  3; /*MEA_INGRESSPORT_GIGA_DISABLE_xxx */
    MEA_Uint32  userParser        :  1; /*T.B.D    if this bit set to 1 the ingress port to can't change the parser*/
    MEA_Uint32  autoneg_enable    :  1;
    MEA_Uint32 ing_hw_switch_protection    :  1; /*TBD*/
    MEA_Uint32 vxlan_cpe          : 1;
    MEA_Uint32  pad               : 8;
    
#ifdef ARCH64
		MEA_Uint32 pad64_0			  :32;
#endif
    
    union {
        struct{
// word 0
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 Water_mark_low_thresh  : 12; // set to default only
    MEA_Uint32 Water_mark_high_thresh : 12; // set to default only
    MEA_Uint32 ingress_mac_thersh     : 8;  // set to default only

#ifdef ARCH64
		MEA_Uint32 pad64_0			  :32;
#endif

#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		      :32;
#endif

    MEA_Uint32 ingress_mac_thersh      : 8; // set to default only
    MEA_Uint32 Water_mark_high_thresh : 12;// set to default only
    MEA_Uint32 Water_mark_low_thresh  : 12;// set to default only
#endif
// word 1
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32  working_mode      :  1; //0 chunk mode 1 Store & Forward
    MEA_Uint32  fragment          :  1; //0 disable 1 enable
    MEA_Uint32  pause_enable      :  1; //0 disable 1 enable 
    MEA_Uint32  pad               : 29;

#ifdef ARCH64
		MEA_Uint32 pad64_1		  :32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		  :32;
#endif
    MEA_Uint32  pad               : 29;
    MEA_Uint32  fragment          :  1; //no fragment =0 fragment =1
    MEA_Uint32  pause_enable      :  1; //0 disable 1 enable  
    MEA_Uint32  working_mode      :  1; //0 chunk mode 1 Store & Forward
#endif
        }val;

        MEA_Uint32 regs[2];
    }ingress_fifo;

    union {
        struct{
            // word 0
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 valid                    : 1; 
            MEA_Uint32 groupId                  : 8;
            MEA_Uint32 pad                      : 23; 

#ifdef ARCH64
		MEA_Uint32 pad64_0					    :32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
            MEA_Uint32 pad                      : 23; 
            MEA_Uint32 groupId                  : 8;
            MEA_Uint32 valid                    : 1; 
#endif
        }val;

        MEA_Uint32 regs[1];
    }bonding_group;

    MEA_IngressPort_ParserInfo_dbt parser_info; /* parser_info */
    struct {
        MEA_Uint32            tmId_enable :  1;
        MEA_Uint32            tmId         :  9;
        MEA_Uint32            pad          : 22;
        MEA_Policer_Entry_dbt    sla_params;
    } policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_LAST];
    MEA_IngressPort_TS_Cpu_offset_dbt  ts_cpu_offset; // adjustment for 1588.
    
    MEA_IngressPort_Protect_t       ProtectType; 
    MEA_IngressPort_Protect_1p1_dbt Protect_1p1_info; 

    
    



} MEA_IngressPort_Entry_dbt;









#define MEA_INGRESSPORT_GIGA_DISABLE_GIGA 0
#define MEA_INGRESSPORT_GIGA_DISABLE_100M 1
#define MEA_INGRESSPORT_GIGA_DISABLE_10M  2
#define MEA_INGRESSPORT_GIGA_DISABLE_10G  3
#define MEA_INGRESSPORT_GIGA_DISABLE_2dot5G  4


typedef enum {
MEA_INGRESS_PARSER_OFFSET_12= 0,
MEA_INGRESS_PARSER_OFFSET_16= 1,
MEA_INGRESS_PARSER_OFFSET_20= 2,
MEA_INGRESS_PARSER_OFFSET_34= 3,
MEA_INGRESS_PARSER_OFFSET_38= 4,
MEA_INGRESS_PARSER_OFFSET_LAST
}MEA_Global_Parser_type_t;

typedef struct{
    MEA_Uint32 valid     :1;
    MEA_Uint32 etherType :16;
    MEA_Uint32 pad       :15;
}MEA_Global_Parser_dbt;

typedef enum{
    MEA_PORT_INGRESS_1P1_STATE_OFF=     0,
    MEA_PORT_INGRESS_1P1_STATE_IDLE=    1,
    MEA_PORT_INGRESS_1P1_STATE_WORKING= 2,
    MEA_PORT_INGRESS_1P1_STATE_PROTECT= 3,
    MEA_PORT_INGRESS_1P1_STATE_LAST
}MEA_PortIngress_status_1p1_dbt;

typedef enum{
    MEA_BRG_PORT_TYPE_UNTAG=0,
    MEA_BRG_PORT_TYPE_TAG=1,
    MEA_BRG_PORT_TYPE_DOUBLE=2,
    MEA_BRG_PORT_TYPE_LAST
}MEA_BRG_PortType_t;

typedef enum{
    MEA_BRG_SWAP_SELECT_LAN=0,
    MEA_BRG_SWAP_SELECT_ACCSESS
}MEA_BRG_SWAP_SELECT_t;

typedef enum{
    MEA_BRG_ENCAP_MODE_SWAP_EXTERNAL_ONLY=0,
    MEA_BRG_ENCAP_MODE_NO_SWAP=1,
    MEA_BRG_ENCAP_MODE_SWAP_INTERNAL_ONLY=2,
    MEA_BRG_ENCAP_MODE_SWAP_BOTH=3
}MEA_BRG_ENCAP_MODE_t;


/************************************************************************/
/*          BRG_PVID                                                    */
/************************************************************************/




typedef struct{
    struct{
        MEA_Uint32   Pvid        :12;  /* 0 not valid for pvid*/
        MEA_Uint32   egress_type     : 2;  /* 0 untag 1 tag 2 double tag 3 else */
        MEA_Uint32   encap_swap_mode : 2;
        MEA_Uint32  external_select : 1;  /*0 lan 1 Access*/
        MEA_Uint32  internal_select : 1;  /*0 lan 1 Access*/
        MEA_Uint32  pad :14;
    } egress ;
    struct{
        MEA_Uint32    Pvid :12;  /* 0 not valid for pvid*/
        MEA_Uint32     pad :20;
    }ingress;

}MEA_BRG_PVID_dbt;

MEA_Status MEA_API_Set_BRG_PVID_Entry(MEA_Unit_t         unit,
                                      MEA_Port_t         port,
                                      MEA_BRG_PVID_dbt* entry);

MEA_Status MEA_API_Get_BRG_PVID_Entry(MEA_Unit_t         unit,
                                      MEA_Port_t         port,
                                      MEA_BRG_PVID_dbt* entry);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*   IN                 MEA Ingress Port Table APIs                                */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_IngressPort_Entry(MEA_Unit_t                 unit,
                                         MEA_Port_t                 port,
                                         MEA_IngressPort_Entry_dbt* entry);

MEA_Status MEA_API_Get_IngressPort_Entry(MEA_Unit_t                 unit,
                                         MEA_Port_t                 port,
                                         MEA_IngressPort_Entry_dbt* entry);

MEA_Status MEA_API_Get_IngressPort_Type(MEA_Unit_t unit,MEA_Port_t port ,MEA_Port_type_te *type);



MEA_Status MEA_API_Set_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Uint8 L2Type, MEA_Def_SID_dbt *entry );

MEA_Status MEA_API_Get_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Uint8 L2Type, MEA_Def_SID_dbt *entry );
MEA_Status MEA_API_IngressPort_Default_L2Type_Sid_GetFirst(MEA_Unit_t unit, MEA_Port_t port, MEA_Uint8 *L2Type, MEA_Bool *found, MEA_Def_SID_dbt *entry);
MEA_Status MEA_API_IngressPort_Default_L2Type_Sid_GetNext(MEA_Unit_t unit, MEA_Port_t port, MEA_Uint8 *L2Type, MEA_Bool *found, MEA_Def_SID_dbt *entry);




typedef enum{
MEA_DEF_PDUsType_LACP=0,
MEA_DEF_PDUsType_L2CP=1,
MEA_DEF_PDUsType_LAST
}MEA_Def_Sid_PDUsType;


MEA_Status MEA_API_Set_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Def_Sid_PDUsType PDUsType, MEA_Def_SID_dbt *entry );
MEA_Status MEA_API_Get_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit,MEA_Port_t port, MEA_Def_Sid_PDUsType PDUsType, MEA_Def_SID_dbt *entry );


MEA_Status MEA_API_Set_IngressPort_RxEnable(MEA_Unit_t                 unit,
                                            MEA_Port_t                 port,
                                            MEA_Bool                   rx_enable);

MEA_Status MEA_API_Set_IngressPort_SHAPER_POS_RX(ENET_Unit_t unit_i,
                                         MEA_Port_t port,
                                         MEA_Uint8 Rx_num, 
                                         MEA_Uint32 value);


MEA_Status MEA_API_Set_Global_ParserEntry(MEA_Unit_t                 unit,
                                          MEA_Uint16                 index,
                                          MEA_Global_Parser_type_t     type,
                                          MEA_Global_Parser_dbt        *entry);


MEA_Status MEA_API_Get_Global_ParserEntry(MEA_Unit_t                 unit,
                                          MEA_Uint16                 index,
                                          MEA_Global_Parser_type_t     type,
                                          MEA_Global_Parser_dbt        *entry);



MEA_Status MEA_API_Port_Serdes_Set_Reset(MEA_Unit_t unit,MEA_Port_t port);

MEA_Status MEA_API_PortIngress_Event_AdminStatus(MEA_Unit_t               unit_i,
                                            MEA_OutPorts_Entry_dbt  *eventPorts,  
                                            MEA_Bool                *exist);

MEA_Status MEA_API_PortIngress_UpdateAdminUp(MEA_Unit_t               unit_i);

MEA_Status MEA_API_Port_Read_Link_protection_Status(MEA_Unit_t      unit_i);  // read this API on period of 1 Sec


MEA_Status MEA_API_port_Get_Link_protection_Status(MEA_Unit_t      unit_i,MEA_OutPorts_Entry_dbt *portEntry);
MEA_Status MEA_API_port_Set_Link_protection_Status_mask(MEA_Unit_t  unit_i,MEA_Port_t port,MEA_Bool enable  );
MEA_Status MEA_API_port_Get_Link_protection_Status_mask(MEA_Unit_t  unit_i,MEA_OutPorts_Entry_dbt *portEntry);


MEA_Bool MEA_API_Set_IngressPort_Is_AdminUp(MEA_Unit_t unit, MEA_Port_t port);

MEA_Status MEA_API_portIngress_1P1_Status(MEA_Unit_t unit_i,MEA_Port_t port ,MEA_PortIngress_status_1p1_dbt *state);
MEA_Status MEA_API_port_1P1_SwitchOver(MEA_Unit_t unit_i,MEA_Port_t port);

/************************************************************************/
/*                                                                      */
/************************************************************************/
#define  MEA_PARSING_L2_MAX 32

typedef struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 l2_Protocol_type            :5;
    MEA_Uint32 external_nettag_mask_valid  :1;
    MEA_Uint32 internal_nettag_mask_valid  :1;
    MEA_Uint32 pad                         :25;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad                         :25;
    MEA_Uint32 internal_nettag_mask_valid  :1;
    MEA_Uint32 external_nettag_mask_valid  :1;
    MEA_Uint32 l2_Protocol_type            :5;
#endif

}MEA_L2_MAP_dbt;

typedef struct{
MEA_Bool valid;
MEA_L2_MAP_dbt data [MEA_PARSING_L2_MAX];

}MEA_L2Type_MAP_dbt;

MEA_Status MEA_API_Create_Mapping_L2_Ingress_Protocol(MEA_Unit_t                   unit_i,
                                                      MEA_Uint16                  *Id_o);

MEA_Status MEA_API_Set_Mapping_L2_Ingress_Protocol(MEA_Unit_t                   unit_i,
                                                      MEA_Uint16                  Id_o,
                                                      MEA_L2Type_MAP_dbt        *entry);

MEA_Status MEA_API_Get_Mapping_L2_Ingress_Protocol(MEA_Unit_t                   unit_i,
                                                  MEA_Uint16                  Id_o,
                                                  MEA_L2Type_MAP_dbt        *entry);







/************************************************************************/
/*                                                                      */
/************************************************************************/
typedef struct{

    MEA_Uint16  sec;
    MEA_Uint32 nSec;
    MEA_Uint8  frac_nano;

}MEA_ptp1588_delay_dbt;



#define MEA_1588_NAM_OF_DELAY 3
typedef enum{
    MEA_1588_DELAY_ASYMMETRY=0,
    MEA_1588_DELAY_PEER2PEER=1,
    MEA_1588_DELAY_EGRESS=2,
    MEA_1588_DELAY_LAST=3
}MEA_ptp1588_delay_te;

typedef struct{

    MEA_PTP1588_Mode_Type_t ptp1588_Mode;

    MEA_ptp1588_delay_dbt delay[MEA_1588_DELAY_LAST];
}MEA_Port_Ptp1588_dbt;

MEA_Status MEA_API_Set_port_PTP1588(MEA_Unit_t unit_i,MEA_Port_t port ,MEA_Port_Ptp1588_dbt *entry);
MEA_Status MEA_API_Get_port_PTP1588(MEA_Unit_t unit_i,MEA_Port_t port ,MEA_Port_Ptp1588_dbt *entry);

typedef union{
    
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 num_of_words     :9;
    MEA_Uint32 reserve          :5;
    MEA_Uint32 empty            :1;
    MEA_Uint32 overflow_fifo    :1;
    MEA_Uint32 pad               :16;

#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
   MEA_Uint32 pad64_0	    	:32;
#endif
   MEA_Uint32 pad               :16;
   MEA_Uint32 overflow_fifo    :1;
   MEA_Uint32 empty            :1;
   MEA_Uint32 reserve          :5;
   MEA_Uint32 num_of_words     :9;
#endif
    }val;
    MEA_Uint32 regs[1];
   
}MEA_Ptp1588_statusTx_dbt;

typedef struct{
    MEA_Uint8  portId;
    MEA_Uint32 seq_number;
    MEA_ptp1588_delay_dbt timestemp;

}MEA_Ptp1588_info_fifo_dbt;

MEA_Status MEA_API_Get_PTP1588_Info_statusFifo(MEA_Unit_t unit_i,MEA_Uint16 instance ,MEA_Ptp1588_statusTx_dbt *entry);
MEA_Status MEA_API_Get_PTP1588_Info_ReadFifo(MEA_Unit_t unit_i, MEA_Uint16 instance,MEA_Ptp1588_info_fifo_dbt *entry);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Ingress_Flow_policer Defines                           */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

typedef struct
{
    struct {
        MEA_Uint32            tmId_enable : 1;
        MEA_Uint32            tmId : 9;
        MEA_Uint32            pad : 22;
        MEA_Policer_Entry_dbt    sla_params;
    } policer_vec[MEA_INGRESS_PORT_POLICER_TYPE_LAST];
}mea_Ingress_Flow_policer_dbt;

MEA_Status MEA_API_Create_Ingress_flow_policerId(MEA_Unit_t unit, MEA_Uint16 *profId);
MEA_Status MEA_API_Delete_Ingress_flow_policerId(MEA_Unit_t unit, MEA_Uint16 profId);

MEA_Status  MEA_API_Set_Ingress_flow_policer(MEA_Unit_t unit, MEA_Uint16 profId, mea_Ingress_Flow_policer_dbt *entry);
MEA_Status  MEA_API_Get_Ingress_flow_policer(MEA_Unit_t unit, MEA_Uint16 profId, mea_Ingress_Flow_policer_dbt *entry);
MEA_Status  MEA_API_Ingress_flow_policer_Prof_IsExist(MEA_Unit_t unit, MEA_Uint16 profId, MEA_Bool *IsExist);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Parser Table Defines                                   */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/




/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Parser Table Typedefs                                  */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

/* Parser priority rule precedence left most is highest (NTAG, VLAN, IP) */
/****************************
0 - Outer, Inner, IP, Default
1 - Outer, IP, Inner, Default
2 - Inner, Outer, IP, Default
3 - Inner, IP, Outer, Default
4 - IP, Outer, Inner, Default
5 - IP, Inner, Default
6 - Outer, Inner, IP, Default
7 - IP , Default
******************************/
typedef enum{
    MEA_PARSER_PRI_RULE_NTAG_VLAN_IP=0,
    MEA_PARSER_PRI_RULE_NTAG_IP_VLAN=1,
    MEA_PARSER_PRI_RULE_VLAN_NTAG_IP=2,
    MEA_PARSER_PRI_RULE_VLAN_IP_NTAG=3,
    MEA_PARSER_PRI_RULE_IP_NTAG_VLAN=4,
    MEA_PARSER_PRI_RULE_IP_VLAN_NTAG=5,
    MEA_PARSER_PRI_RULE_NTAG_VLAN_IP_L2_PRI_VLAN,
    MEA_PARSER_PRI_RULE_LAST
} MEA_Parser_pri_rule_te;

#define  MEA_OAM_LOCATION_NONE   0
#define  MEA_OAM_LOCATION_FIRST  1
#define  MEA_OAM_LOCATION_SECOND 2
#define  MEA_OAM_LOCATION_BOTH   3



typedef struct{
    struct{
    MEA_Uint32 pri_rule                           :  3; /*  0.. 2 */
    MEA_Uint32 DSA_Type_enable                    :  1; /*  3.. 3 */
    MEA_Uint32 L4_SRC_DST                         :  1; /*  4.. 4 */
    MEA_Uint32 default_sid                        : 14; /* 5..18 */ //net_tag_mask_0_15 /*will be the Sid 2-ac 14 sid*/
    MEA_Uint32 default_sid_act                    :  2; /*19.. 20 */
    MEA_Uint32 logical_src_port                   :  10; /* 21..27 */ /*7 bit an 3bit on end*/
    MEA_Uint32 logical_src_port_replace           :  1; /* 28..28 */
    MEA_Uint32 mapping_profile                    :  1; /* 29..29 */ 
    MEA_Uint32 filter_key_interface_type          :  1; /* 30..30 */
    MEA_Uint32 color_valid                        :  1; /* 31..31 */


    MEA_Uint32 ENNI_enable                       :1;     /*0*/  /*32 32*/ /*need to be reserve*/
    MEA_Uint32 or_mask_net2_0_15                :16;     /*1 25*/ /*33 49*/
    MEA_Uint32 or_mask_net2_16_23               :8;               /*49 56*/
    MEA_Uint32 or_mask_net_tag_20_23            :4;     /*25 29*/ /*57 60*/
    MEA_Uint32 or_mask_pri_3_7                  :5;     /* */     /*61 65*/
    MEA_Uint32 or_mask_L2_Type                  :5;     /*1 16*/ /*66 70*/
    MEA_Uint32 transparent_mode                 :1;    /*1 16*/ /*71 71*/
    MEA_Uint32 protocol2pri_mapping_enable      :1;
    MEA_Uint32 or_mask_pri                      :3;
    MEA_Uint32 or_mask_src_port                 :10;
    MEA_Uint32 or_mask_net_tag_0_13             :14;
    MEA_Uint32 or_mask_net_tag_14_19            :6;
    MEA_Uint32 evc_external_internal            : 1;
    MEA_Uint32 default_net_tag_value            :12;
    MEA_Uint32 afdx_enable                      : 1;
    MEA_Uint32 default_pri                      : 3;
    MEA_Uint32 pri_Ip_type                      : 2;
    MEA_MacAddr my_Mac;
    MEA_Uint32 vlanrange_prof_Id               :6;
    MEA_Uint32 vlanrange_prof_valid            :1;
    MEA_Uint32 lag_distribution_mask           :4;
    MEA_Uint32 def_sid_port_type               : 2;
    MEA_Uint32 l2type_consolidate              : 1;
    MEA_Uint32 local_MAC_enable                : 4;
         }f;


/* private */
     struct{
         MEA_Uint32 Prsr_Data_reg0;
         MEA_Uint32 Prsr_Data_reg1;
         MEA_Uint32 Prsr_Data_reg2;
         MEA_Uint32 Prsr_Data_reg3;
         MEA_Uint32 Prsr_Data_reg4;
         MEA_Uint32 Prsr_Data_reg5;
         MEA_Uint32 Prsr_Data_reg6;
         
     }r;

     MEA_Uint32 reg[7];

}MEA_Parser_Entry_dbt;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Parser  Port Table APIs                                */
/*                                                                               */
/* Note: This APIs is low level API.                                             */
/*       It is recommended to use MEA_API_Get/Set_IngressPort_Entry APIs          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_Parser_Entry(MEA_Unit_t            unit,
                                    MEA_Port_t            port,
                                    MEA_Parser_Entry_dbt* entry);

MEA_Status MEA_API_Get_Parser_Entry(MEA_Unit_t            unit,
                                    MEA_Port_t            port,
                                    MEA_Parser_Entry_dbt* entry);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA PreATMParser table Typedefs                             */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef enum {
    MEA_ATM_PROTOCOL_NONE=0,
    MEA_ATM_PROTOCOL_EoLLCoA=17,
    MEA_ATM_PROTOCOL_EoVCMUXoA=18,
    MEA_ATM_PROTOCOL_IPoLLCoA=19,
    MEA_ATM_PROTOCOL_IPoVCMUXoA=20,
    MEA_ATM_PROTOCOL_PPPoLLCoA=21,
    MEA_ATM_PROTOCOL_PPPoVCMUXoA=22,
    MEA_ATM_PROTOCOL_VoEoLLCoA=23,
    MEA_ATM_PROTOCOL_VoEoVCMUXoA=24,
    MEA_ATM_PROTOCOL_PPPoEoA=25,
    MEA_ATM_PROTOCOL_PPPoEVCMUXoA=26,
    MEA_ATM_PROTOCOL_LAST=27
}MEA_ATM_PROTOCOL_ATM_Parser_t;


typedef struct{
    MEA_Uint32   pad               :3;
    MEA_Uint32   L2_protocol_type  :5; // see MEA_ATM_PROTOCOL_ATM_Parser_t
    MEA_Uint32   vpi               :8;
    MEA_Uint32   vci               :16;
}MEA_PreATMParser_Key_dbt;

typedef struct{
    MEA_PreATMParser_Key_dbt key;
    // internal parameter
    MEA_Uint8               vspId;
}MEA_PreATMParser_Data_dbt;




/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA PreATMParser table API                                 */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_ATM_Parser_Entry_Create(MEA_Unit_t              unit_i,
                                           MEA_Port_t              port,
                                           MEA_Uint8              vsp_type,
                                           MEA_PreATMParser_Data_dbt *data);

MEA_Status MEA_API_ATM_Parser_Entry_Set(MEA_Unit_t                 unit_i,
                                        MEA_Port_t                 port,
                                        MEA_Uint8                  vsp_type,
                                        MEA_PreATMParser_Data_dbt *data);

MEA_Status MEA_API_ATM_Parser_Entry_Delete(MEA_Unit_t              unit_i,
                                           MEA_Port_t              port,
                                           MEA_Uint8              vsp_type);

MEA_Status MEA_API_ATM_Parser_Entry_Get(MEA_Unit_t                 unit_i,
                                        MEA_Port_t                 port,
                                        MEA_Uint8                  vsp_type,
                                        MEA_PreATMParser_Data_dbt *data);

MEA_Status MEA_API_ATM_Parser_Entry_IsExist(MEA_Unit_t             unit_i,
                                            MEA_Port_t                 port,
                                            MEA_Uint8                  vsp_type,
                                            MEA_Bool                   silence,
                                            MEA_Bool                  *exist);








/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Pre Parser table Typedefs                              */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

typedef union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 vci             : 16;
            MEA_Uint32 vpi             :  8;
            MEA_Uint32 pad0            :  8;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad0            :  8;
            MEA_Uint32 vpi             :  8;
            MEA_Uint32 vci             : 16;
#endif            
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 vci_mask        : 16 ;
            MEA_Uint32 vpi_mask        :  8 ;
            MEA_Uint32 pad1            :  8 ;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 pad1            :  8 ;
            MEA_Uint32 vpi_mask        :  8 ;
            MEA_Uint32 vci_mask        : 16 ;
#endif
        } atm  ;
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 ether_type      : 16 ;
            MEA_Uint32 pad0            : 16 ;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad0            : 16 ;
            MEA_Uint32 ether_type      : 16 ;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 ether_type_mask : 16;
            MEA_Uint32 pad1            : 16 ;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 pad1            : 16 ;
            MEA_Uint32 ether_type_mask : 16;
#endif            
        } eth ;
        
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 net_tag         : 24;
            MEA_Uint32 pad0            :  8;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad0            :  8;
            MEA_Uint32 net_tag         : 24;
#endif            
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32 net_tag_mask    : 24;
            MEA_Uint32 pad1            :  8;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
           MEA_Uint32 pad1            :  8;
           MEA_Uint32 net_tag_mask    : 24;
#endif
        } raw ;
} MEA_PreParser_Key_dbt;


typedef struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 vsp_type : 2 ;
    MEA_Uint32 pad0     : 30;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad0     : 30;
    MEA_Uint32 vsp_type : 2 ;
#endif    
} MEA_PreParser_Data_dbt;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Pre Parser Table APIs                                  */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Get_PreParser_Entry_ByKey ( MEA_Unit_t              unit_i,
                                               MEA_PreParser_Key_dbt*  key_i,
                                               MEA_PreParser_Data_dbt* data_o,
                                               MEA_Bool*               found_o );

MEA_Status MEA_API_Get_PreParser_Entry_ByIndex ( MEA_Unit_t              unit_i,
                                                 MEA_Uint32              index_i,
                                                 MEA_PreParser_Key_dbt*  key_o,
                                                 MEA_PreParser_Data_dbt* data_o,
                                                 MEA_Bool*               found_o );

MEA_Status MEA_API_Set_PreParser_Entry_Create ( MEA_Unit_t              unit_i,
                                                MEA_PreParser_Key_dbt*  key_i,
                                                MEA_PreParser_Data_dbt* data_i,
                                                MEA_Uint32*             index_o );

MEA_Status MEA_API_Set_PreParser_Entry_ByIndex ( MEA_Unit_t              unit_i,
                                                 MEA_Uint32              index_i,
                                                 MEA_PreParser_Data_dbt* data_i );

MEA_Status MEA_API_Set_PreParser_Entry_ByKey ( MEA_Unit_t              unit_i,
                                               MEA_PreParser_Key_dbt*  key_i,
                                               MEA_PreParser_Data_dbt* data_i );

MEA_Status MEA_API_Set_PreParser_Entry_DeleteByKey ( MEA_Unit_t              unit_i,
                                                     MEA_PreParser_Key_dbt*  key_i  );

MEA_Status MEA_API_Set_PreParser_Entry_DeleteByIndex ( MEA_Unit_t     unit_i,
                                                       MEA_Uint32     index_i );


MEA_Bool MEA_API_IsPreParserIndexInRange(MEA_Unit_t     unit, 
                                         MEA_Uint32     index);

#define MEA_CHECK_PRE_PARSER_INDEX_IN_RANGE(index) \
    if(!MEA_API_IsPreParserIndexInRange(MEA_UNIT_0,index)){ \
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - Index %d is out of range\n", \
                          __FUNCTION__,index); \
        return MEA_ERROR;    \
    }                    \

MEA_Bool MEA_API_IsPreParserNetTagInRange(MEA_Unit_t     unit, 
                                          MEA_Uint32  net_tag);

#define MEA_CHECK_PRE_PARSER_NET_TAG_IN_RANGE(net_tag) \
    if(!MEA_API_IsPreParserNetTagInRange(MEA_UNIT_0,(net_tag) )){ \
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - net_tag 0x%08x is out of range\n", \
                          __FUNCTION__, (net_tag)); \
        return MEA_ERROR;    \
    }    

MEA_Bool MEA_API_IsPreParserVspTypeInRange(MEA_Unit_t unit, 
                                           MEA_Uint32 vsp_type);

#define MEA_CHECK_PRE_PARSER_VSP_TYPE_IN_RANGE(vsp) \
    if(!MEA_API_IsPreParserVspTypeInRange(MEA_UNIT_0,vsp)){ \
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - VSP type %d is out of range\n", \
                          __FUNCTION__, vsp); \
        return MEA_ERROR;    \
    }    

MEA_Bool MEA_API_IsPreParserNetTagMaskInRange(MEA_Unit_t unit, 
                                           MEA_Uint32 net_tag_mask);

#define MEA_CHECK_PRE_PARSER_NET_TAG_MASK_IN_RANGE(net_tag_mask) \
    if(!MEA_API_IsPreParserNetTagMaskInRange(MEA_UNIT_0,(net_tag_mask))){ \
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - NetTag mask 0x%08x is out of range\n", \
                          __FUNCTION__, (net_tag_mask)); \
        return MEA_ERROR;    \
    }    

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA VSP fragment Table Defines                             */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    MEA_Uint32 src_port    : 7;
    MEA_Uint32 session_num : 16;
    MEA_Uint32 group       : 8;
    
}MEA_fragment_vsp_Key_dbt;

typedef struct {
    
    MEA_Uint32  pad    :15;
    MEA_Uint32  valid :1;
    MEA_Uint32  internal_id    :16;

}MEA_fragment_vsp_Data_dbt;
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA VSP fragment API                                       */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_fragment_vsp_Entry_ByKey ( MEA_Unit_t              unit_i,
                                              MEA_fragment_vsp_Key_dbt*  key_i,
                                              MEA_fragment_vsp_Data_dbt* data_i, 
                                              MEA_Uint32 *index_o);


MEA_Status MEA_API_Get_fragment_vsp_Entry_ByKey ( MEA_Unit_t              unit_i,
                                                 MEA_fragment_vsp_Key_dbt*  key_i,
                                                 MEA_fragment_vsp_Data_dbt* data_o,
                                                 MEA_Bool*               found_o );





/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA L2CP Table Defines                                     */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define MEA_L2CP_SUFFIX_SIZE        64

#define MEA_L2CP_ENTRY_SET_ACTION(entry,macSuffix,action) \
{ \
    (entry).reg[(macSuffix)/16] &= ~((                       0x00000003)<<(2*((macSuffix)%16))); \
    (entry).reg[(macSuffix)/16] |=   ((MEA_Uint32)(action) & 0x00000003)<<(2*((macSuffix)%16)) ; \
}

#define MEA_L2CP_ENTRY_GET_ACTION(entry,macSuffix) \
    ((MEA_L2CP_Action_te)(((entry).reg[(macSuffix)/16] & ((0x00000003)<<(2*((macSuffix)%16)))) >>(2*((macSuffix)%16))))



       



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA L2CP Table APIs                                        */
/*                                                                               */
/* Note: This APIs are low level APIs and recommended to use                      */
/*       MEA_API_Get/Set_IngressPort_Entry High Level APIs                       */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_NetworkL2CP_Entry   (MEA_Unit_t          unit,
                                            MEA_Port_t          port,
                                            MEA_L2CP_Entry_dbt* entry);

MEA_Status MEA_API_Get_NetworkL2CP_Entry   (MEA_Unit_t          unit,
                                            MEA_Port_t          port,
                                            MEA_L2CP_Entry_dbt* entry);



/************************* INDEX-  section 3.1.3 Egress port    ******************/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*         3.1.3 Egress                                                          */
/*             3.1.3.1 port Shaper                         (advanced - option)   */
/*             3.1.3.2 Egress port                         (Basic)               */
/*             3.1.3.3 Wred                                (advanced - option)   */
/*         3.1.4 Virtual Egress Cluster                                          */
/*             3.1.4.1 Egress Queue cluster                                      */
/*                                                                               */
/*********************************************************************************/

/*-------------------------------------------------------------------------------*/             
/*                                                                               */             
/*             < Shaper_Profile > Defines                                        */             
/*                                                                               */             
/*-------------------------------------------------------------------------------*/             
#define SHAPER_PROFILE_TYPE_BIT_VAL   0  
#define SHAPER_PROFILE_TYPE_PACKET_VAL 1



/*-------------------------------------------------------------------------------*/             
/*                                                                               */             
/*             < Shaper_Profile > Typedefs                                       */             
/*                                                                               */             
/*-------------------------------------------------------------------------------*/             
typedef enum {
MEA_SHAPER_TYPE_MEF=0,
MEA_SHAPER_TYPE_BAG=1,
MEA_SHAPER_TYPE_LAST
} ENET_Shaper_mode_te;

typedef struct {

    char   name[ENET_PLAT_SHAPER_PROFILE_MAX_NAME_SIZE];
    ENET_Shaper_mode_te mode;
    ENET_EirCir_t CIR;     
    ENET_EbsCbs_t CBS;                
    ENET_Uint32  type          : 1;            /* SHAPER_PROFILE_TYPE_XXX_VAL */
    ENET_Uint32  overhead      : 8;
    ENET_Uint32  Cell_Overhead : 8;
    ENET_Uint32  pad0 : 7;            

} ENET_Shaper_Profile_dbt;

typedef struct {
    ENET_ShaperSupportType_te      entity_type;
    MEA_AcmMode_t                  acm_mode;
    ENET_ShaperId_t                id;
} ENET_Shaper_Profile_key_dbt;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < Shaper_Profile > APIs                                           */
/*                                                                               */ 
/*-------------------------------------------------------------------------------*/


/******************************************************************************                 
* Function Name:       < ENET_Create_Shaper_Profile >                                                 
* Description: This function find or create new Shaper_Profile entity instance, and gives                      
* Shaper_Profile id handle that can be used later on to retrieve this entity instance.                  
*-------------------------------------------------------------------------------                  
* Input Parameters:                                                                               
*  unit_i   ->  identifier of the device.                                                     
*  key_io   ->  pointer to the shaper profile key structure 
*               Note: The field id may ENET_PLAT_GENERATE_NEW_ID to create auto new id 
*                     in this case the caller structure will be update with the new id value.
*                     The entity_type and acm_mode fields in the key structure must be supply.
*  entry_i  ->  pointer to the Shaper_Profile structure containing the new Shaper_Profile                      
*               parameters.                                                                      
*
* Output Parameters:                                                                               
*  key_io   ->  pointer to the shaper profile key structure 
*               If The field id supply as input with the value ENET_PLAT_GENERATE_NEW_ID then
*               the output of this field in the key structure will contain the new id
*                                                                                                 
* Return values:                                                                                 
*     ENET_OK, the function performed the function correctly.                                     
*     ENET_ERROR, the function did not perform the function.                                         
*                                                                                                 
*******************************************************************************/               
ENET_Status ENET_Create_Shaper_Profile   (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_io,
                                          ENET_Shaper_Profile_dbt     *entry_i);



/******************************************************************************                 
* Function Name:       < ENET_Set_Shaper_Profile >                                                     
* Description: This function is responsible for modifying an existing Shaper_Profile                      
* entity instance.                                                                              
*-------------------------------------------------------------------------------                  
* Input Parameters:                                                                               
*  unit_i   ->  identifier of the device.                                                     
*  key_i    ->  pointer to the shaper profile key structure 
*  entry_i  ->  pointer to the Shaper_Profile structure containing the new Shaper_Profile                      
*               parameters.                                                                      
*                                                                                                 
* Output Parameters:                                                                               
*   none
*                                                                                                 
* Return values:                                                                                 
*     ENET_OK, the function performed the function correctly.                                     
*     ENET_ERROR, the function did not perform the function.                                         
*                                                                                                 
*******************************************************************************/               
ENET_Status ENET_Set_Shaper_Profile      (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_i,
                                          ENET_Shaper_Profile_dbt     *entry_i);
                               


/******************************************************************************                 
* Function Name:       < ENET_Get_Shaper_Profile >                                                     
* Description: This function is responsible for modifying an existing Shaper_Profile                      
* entity instance.                                                                              
*-------------------------------------------------------------------------------                  
* Input Parameters:                                                                               
*  unit_i   ->  identifier of the device.                                                     
*  key_i    ->  pointer to the shaper profile key structure 
*                                                                                                 
* Output Parameters:                                                                               
*  entry_o  ->  pointer to a Shaper_Profile structure where to store the retrieved entity's          
*               parameters.                                                                      
*                                                                                                 
* Return values:                                                                                 
*     ENET_OK, the function performed the function correctly.                                     
*     ENET_ERROR, the function did not perform the function.                                         
*                                                                                                 
*******************************************************************************/                  
ENET_Status ENET_Get_Shaper_Profile      (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_i,
                                          ENET_Shaper_Profile_dbt     *entry_o);

/******************************************************************************                 
* Function Name:       < ENET_Delete_Shaper_Profile >                                                 
* Description: This function is responsible for deleting an existing Shaper_Profile                      
*              entity instance, by its Shaper_Profile ID.                                              
*-------------------------------------------------------------------------------                  
* Input Parameters:                                                                               
*  unit_i   ->    identifier of the device.                                                     
*  key_i    ->    pointer to structure that define the key of the shaper profile
*                                                                                               
* Output Parameters:                                                                               
*   none
*
* Return values:                                                                                 
*     ENET_OK, the function performed the function correctly.                                     
*     ENET_ERROR, the function did not perform the function.                                         
*                                                                                                 
*******************************************************************************/               
ENET_Status ENET_Delete_Shaper_Profile   (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_i);

/******************************************************************************                
* Function Name:       < ENET_GetFirst_Shaper_Profile >                                            
* Description: This function is responsible for retrieving the first Shaper_Profile                 
* entity instance in the DB.                                                                 
*-------------------------------------------------------------------------------             
* Input Parameters:                                                                          
*  unit_i  ->   identifier of the device.                                                    
*                                                                                                
* Output Parameters:                                                                          
*  key_o   ->  pointer to structure that define the key of the first shaper profile
*  entry_o ->  pointer to a Shaper_Profile structure where to store the first entity's                 
*              parameters.                                                                    
*  found_o ->  pointer to ENET_Bool with the search results                                 
*              ENET_TRUE-found, ENET_FALSE-not found.                                        
*                                                                                               
* Return values:                                                                                
*     ENET_OK, the function performed the function correctly.                                    
*     ENET_ERROR, the function did not perform the function.                                        
*                                                                                                
*******************************************************************************/               
ENET_Status ENET_GetFirst_Shaper_Profile (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_o,
                                          ENET_Shaper_Profile_dbt     *entry_o,
                                          ENET_Bool                   *found_o);

                                                                                               

/******************************************************************************                 
* Function Name:       < ENET_GetNext_Shaper_Profile >                                                 
* Description: This function is responsible for retrieving the next Shaper_Profile                      
*              entity instance in the DB, after a specified instance.                          
*-------------------------------------------------------------------------------                  
* Input Parameters:                                                                          
*  unit_i  ->   identifier of the device.                                                    
*  key_io  ->  pointer to structure that define the key of the previous shaper profile
*                                                                                                
* Output Parameters:                                                                          
*  key_io   ->  pointer to structure that define the key of the next shaper profile
*  entry_o ->  pointer to a Shaper_Profile structure where to store the first entity's                 
*              parameters.                                                                    
*  found_o ->  pointer to ENET_Bool with the search results                                 
*              ENET_TRUE-found, ENET_FALSE-not found.                                        
*                                                                                                 
* Return values:                                                                                 
*     ENET_OK, the function performed the function correctly.                                     
*     ENET_ERROR, the function did not perform the function.                                         
*                                                                                                 
*******************************************************************************/                 
ENET_Status ENET_GetNext_Shaper_Profile (ENET_Unit_t                  unit_i,                         
                                         ENET_Shaper_Profile_key_dbt *key_io,
                                         ENET_Shaper_Profile_dbt     *entry_o,
                                         ENET_Bool                   *found_o);                         

                                                                                               
                                                                         
/******************************************************************************                 
* Function Name:       < ENET_GetIDByName_Shaper_Profile >                                             
* Description: This function is responsible for retrieving the next Shaper_Profile                      
*              entity instance in the DB, after a specified instance.                          
*-------------------------------------------------------------------------------                  
* Input Parameters:                                                                               
*  unit_i   ->  identifier of the device.                                                     
*  name_i   ->  pointer to a Shaper_Profile structure where to store the first entity's              
*               parameters.                                                                     
* Output Parameters:                                                                               
*  key_o    ->  pointer to structure that define the key of the first shaper profile with this name
*  found_o  ->  pointer to ENET_Bool with the search results                                  
              - ENET_TRUE-found, ENET_FALSE-not found.                                         
*                                                                                                 
*                                                                                                
* Return values:                                                                                 
*     ENET_OK, the function performed the function correctly.                                     
*     ENET_ERROR, the function did not perform the function.                                         
*                                                                                                 
*******************************************************************************/                 
ENET_Status ENET_GetIDByName_Shaper_Profile (ENET_Unit_t                  unit_i,
                                             char                        *name_i,
                                             ENET_Shaper_Profile_key_dbt *key_o,
                                             ENET_Bool                   *found_o);


/******************************************************************************
* Function Name: < ENET_IsValid_Shaper_Profile >
*
* Description:   This function is responsible for checking if the shaper id 
*                is valid , and the shaper instance exist
*------------------------------------------------------------------------------- 
* Input Parameters:                                                                          
*  unit_i   -> identifier of the device.                                                    
*  key_i    -> pointer to structure that define the key of the shaper profile
*  silent_i -> Boolean that define if not print log error message
*
* Output Parameters: 
*  none
*
* Return values:
*     ENET_TRUE, the id is     valid and    exist.
*     ENET_FALSE,the id is not valid or not exist
*
*******************************************************************************/                                                                                                               
ENET_Bool  ENET_IsValid_Shaper_Profile(ENET_Unit_t                    unit_i,
                                       ENET_Shaper_Profile_key_dbt   *key_i,
                                       ENET_Bool                      silent_i);



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA ShaperTicks Defines                                   */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

/* should not be here */
#define MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MIN_VAL  (0)
#define MEA_TICKS_FOR_SHAPER_FAST_SERVICE_MAX_VAL  ((MEA_GLOBAL_SHAPER_XCIR_SUPPORT == MEA_FALSE) ? (0x0000ffff) : (0x000fffff)) /* 16 bits */
#define MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MIN_VAL  (0)
#define MEA_TICKS_FOR_SHAPER_SLOW_MULTIPLEX_SERVICE_MAX_VAL  (7) /* 3 bits */

/* ticks is ticks_for_slow_service or ticks_for_fast_service */

#define MEA_SHAPER_CIR_EIR_FAST_GN_PORT  (MEA_Platform_GetShaperFastGnPort())
#define MEA_SHAPER_CIR_EIR_SLOW_GN_PORT  (MEA_Platform_GetShaperSlowGnPort())

#define MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER  (MEA_Platform_GetShaperFastGnCluster())
#define MEA_SHAPER_CIR_EIR_SLOW_GN_CLUSTER  (MEA_Platform_GetShaperSlowGnCluster())

#define MEA_SHAPER_CIR_EIR_FAST_GN_PRIQUEUE  (MEA_Platform_GetShaperFastGnPriQueue())
#define MEA_SHAPER_CIR_EIR_SLOW_GN_PRIQUEUE  (MEA_Platform_GetShaperSlowGnPriQueue())




#define MEA_SHAPER_CIR_EIR_SLOW_GN_PORT_TICKs      (MEA_Platform_GetShaperSlowGnPort_TICKs())
#define MEA_SHAPER_CIR_EIR_SLOW_GN_CLUSTER_TICKs   (MEA_Platform_GetShaperSlowGnCluster_TICKs())
#define MEA_SHAPER_CIR_EIR_SLOW_GN_PRIQUEUE_TICKs  (MEA_Platform_GetShaperSlowGnPriQueue_TICKs())

#define MEA_SHAPER_CIR_EIR_FAST_GN_PORT_TICKs      (MEA_Platform_GetShaperFastGnPort_TICKs())
#define MEA_SHAPER_CIR_EIR_FAST_GN_CLUSTER_TICKs   (MEA_Platform_GetShaperFastGnCluster_TICKs())
#define MEA_SHAPER_CIR_EIR_FAST_GN_PRIQUEUE_TICKs  (MEA_Platform_GetShaperFastGnPriQueue_TICKs())

#define MEA_SHAPER_SLOW_MULTYPLESCAN_PORT          (MEA_Platform_GetShaperSlowGnPort())
#define MEA_SHAPER_SLOW_MULTYPLESCAN_CLUSTER       (MEA_Platform_GetShaperSlowGnCluster())
#define MEA_SHAPER_SLOW_MULTYPLESCAN_PRIQUEUE      (MEA_Platform_GetShaperSlowGnPriQueue())


 
typedef enum {
    MEA_SHAPER_RESOLUTION_TYPE_BIT=0,
    MEA_SHAPER_RESOLUTION_TYPE_PKT=1,
    MEA_SHAPER_RESOLUTION_TYPE_LAST
} MEA_Shaper_Resolution_type_te;  

#define ENET_SHAPER_RESOLUTION_TYPE_BIT  MEA_SHAPER_RESOLUTION_TYPE_BIT
#define ENET_SHAPER_RESOLUTION_TYPE_PKT  MEA_SHAPER_RESOLUTION_TYPE_PKT
#define ENET_SHAPER_RESOLUTION_TYPE_LAST MEA_SHAPER_RESOLUTION_TYPE_LAST
#define ENET_Shaper_Resolution_type_te   MEA_Shaper_Resolution_type_te



#define ENET_SHAPER_COMPENSATION_TYPE_CELL      0
#define ENET_SHAPER_COMPENSATION_TYPE_PPACKET   1
#define ENET_SHAPER_COMPENSATION_TYPE_AAL5      3

#define ENET_SHAPER_BYPASS_TYPE_ENABLE  0
#define ENET_SHAPER_BYPASS_TYPE_DISABLE 1



typedef struct {
        ENET_Shaper_mode_te mode;
        MEA_EirCir_t      CIR;
        MEA_EbsCbs_t      CBS;
        MEA_Shaper_Resolution_type_te resolution_type;
        MEA_Uint32  overhead      : 8;
        MEA_Uint32  Cell_Overhead : 8;
        MEA_Uint32  pad0          : 16; 
} MEA_shaper_info_dbt;                









/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Egress Port Table Defines                              */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/



#define MEA_EGRESS_PORT_MQS_MAX_VALUE (0x01FF)

#define MEA_EGRESS_PORT_MTU_GRANULARITY_VALUE  1




/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Egress Port Table Typedefs                             */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef enum{
    MEA_EGRESS_PORT_PROTO_TRANS           = 0,
    MEA_EGRESS_PORT_PROTO_VLAN            = 1,
    MEA_EGRESS_PORT_PROTO_QTAG            = 2,
    MEA_EGRESS_PORT_PROTO_MARTINI         = 3,
    MEA_EGRESS_PORT_PROTO_EoLLCoATM       = 4,
    MEA_EGRESS_PORT_PROTO_IP              = 5,
    MEA_EGRESS_PORT_PROTO_IPoVLAN         = 6,
    MEA_EGRESS_PORT_PROTO_IPoEoLLCoATM    = 7,
    MEA_EGRESS_PORT_PROTO_PPPoEoLLCoATM   = 8,
    MEA_EGRESS_PORT_PROTO_EoVcMUXoATM     = 9,
    MEA_EGRESS_PORT_PROTO_IPoLLCoATM      = 10,
    MEA_EGRESS_PORT_PROTO_IPoVcMUXoATM    = 11,
    MEA_EGRESS_PORT_PROTO_PPPoLLCoATM     = 12,
    MEA_EGRESS_PORT_PROTO_PPPoVcMUXoATM   = 13,
    MEA_EGRESS_PORT_PROTO_PPPoEoVcMUXoATM = 14,
    MEA_EGRESS_PORT_PROTO_ETHER_TYPE      = 15,
    MEA_EGRESS_PORT_PROTO_MPLS            = 16,
    MEA_EGRESS_PORT_PROTO_MPLSoE          = 17,
    MEA_EGRESS_PORT_PROTO_RESERV1         =18,
    MEA_EGRESS_PORT_PROTO_RESERV2         =19,
    MEA_EGRESS_PORT_PROTO_DC              =20,
    MEA_EGRESS_PORT_PROTO_TDM              =21,

    MEA_EGRESS_PORT_PROTO_LAST
} MEA_EgressPort_Proto_t;


typedef enum{
    MEA_EGRESS_PORT_FILTER_FWD_ALL=0,
    MEA_EGRESS_PORT_FILTER_FWD_ONLY_BPDU=1,
    MEA_EGRESS_PORT_FILTER_DROP_ONLY_BPDU=2,///not from cpu
    MEA_EGRESS_PORT_FILTER_FWD_ONLY_CPU=3,
    MEA_EGRESS_PORT_FILTER_FWD_ONLY_CPU_AND_BPDU=4,
    MEA_EGRESS_PORT_FILTER_DROP_FROM_CPU=5,
    MEA_EGRESS_PORT_FILTER_RESERV=6,
    MEA_EGRESS_PORT_FILTER_DROP_ALL_AND_BPDU=7,
    MEA_EGRESS_PORT_FILTER_LAST
}MEA_EgressPort_Filter_t;

typedef struct  
{
    
    MEA_Uint32  CRC_XON;                    // calculate bye driver 
    MEA_Uint32  CRC_XOFF;                   // calculate bye driver
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32  Transmit_threshold  : 10;   // not allowed to change 
    MEA_Uint32  AF_threshold        : 10;   // not allowed to change 
    MEA_Uint32  Working_mode        : 1;       //(0=chunk mode)
    MEA_Uint32  pad2                : 11;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  pad2                : 11;
    MEA_Uint32  Working_mode        : 1;       //(0=chunk mode 1 store & forward)
    MEA_Uint32  AF_threshold        : 10;   // not allowed to change 
    MEA_Uint32  Transmit_threshold  : 10;   // not allowed to change 
#endif
}MEA_EgressPort_pause_send_dbt;


typedef enum{
MEA_SERDES_LB_NONE=0,
MEA_SERDES_LB_NEAR_PCS=1, /*Internal*/
MEA_SERDES_LB_NEAR_PMA=2,
MEA_SERDES_LB_FAR_PMA=4, /* External*/
MEA_SERDES_LB_FAR_PCS=6,
MEA_SERDES_LB_MAC=7      /*MAC Loop back*/

}MEA_Egress_Mac_Serdes_lb_t;


typedef enum{
    MEA_EGRESS_PROTECT_BYPASS=0,
    MEA_EGRESS_PROTECT_1_PLUS_1=1,
    MEA_EGRESS_PROTECT_1_TWO_1=2,
    MEA_EGRESS_PROTECT_MAP_PORT2INT=3,
    MEA_EGRESS_PROTECT_LAST
}MEA_Egress_protectType_t;








typedef struct{
  MEA_Bool   enable_1P1;
  MEA_Port_t outPortId_1p1;     
 
}MEA_Egress_protect_1p1_dbt;  


typedef struct{
    MEA_Uint8 net1_enableMask  :1;
    MEA_Uint8 net2_enableMask  :1;
    MEA_Uint8 pad              :6;
}MEA_EgresPort_Mask_t;

#define MEA_EGRESS_MAX_MASK_L2_TYPE 32

typedef struct{

    /* word 0 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32                     pad0   :  7;  /*  0.. 6 */
    MEA_Uint32                     halt       :  1;  /*  7.. 7 */
    MEA_Uint32                     flush      :  1;  /*  8.. 8 */
    MEA_Uint32                     MTU        : 14;  /*  9..22 */
    MEA_Uint32                     proto      :  7;  /* 23..30 */
    MEA_Uint32                     tx_enable  :  1;  /* 30..30 */     
    MEA_Uint32                     calc_crc   :  1;  /* 31..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32                     calc_crc   :  1;  /* 31..31 */
    MEA_Uint32                     tx_enable  :  1;  /* 30..30 */
    MEA_Uint32                     proto      :  7;  /* 23..30 */
    MEA_Uint32                     MTU        : 14;  /*  9..22 */
    MEA_Uint32                     flush      :  1;  /*  8.. 8 */
    MEA_Uint32                     halt       :  1;  /*  7.. 7 */
    MEA_Uint32                     pad0   :  7;  /*  0.. 6 */
#endif
    MEA_Uint32                     phy_port            : 10;
    MEA_Uint32                     shaper_enable       : 1; /* 0- Disable 1-Enable*/  
    MEA_Uint32                     Shaper_compensation : 2; /* 00-Chunk 01-Packet 11-AAL5 */
    MEA_Uint32                     g9991_frg_scheduler :1;
    MEA_Uint32                     ing_hw_switch_protection :1;
    MEA_Uint32                     pad                 :17;

            
    ENET_ShaperId_t                shaper_Id ; 
    MEA_shaper_info_dbt            shaper_info;
    MEA_MacAddr                    My_Mac;
    MEA_EgressPort_pause_send_dbt  pause_send;



    
    MEA_Egress_protectType_t protectType;
    MEA_Egress_protect_1p1_dbt protect_1p1_info;


    /******Egress ACL *********/
    struct {
    MEA_Bool                filter_Enable;
    MEA_EgressPort_Filter_t egress_port_state;
    MEA_EgresPort_Mask_t    L2Type_Mask[MEA_EGRESS_MAX_MASK_L2_TYPE];
    MEA_Bool                compress_enable;
   
    }egressFilter;
   /********************/
    MEA_Bool                enable_te_id_stamping;
    
   
} MEA_EgressPort_Entry_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Egress Port Table APIs                                 */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_EgressPort_Entry(MEA_Unit_t                unit,
                                        MEA_Port_t                port,
                                        MEA_EgressPort_Entry_dbt* entry);

MEA_Status MEA_API_Get_EgressPort_Entry(MEA_Unit_t                unit,
                                        MEA_Port_t                port,
                                        MEA_EgressPort_Entry_dbt* entry);

MEA_Status MEA_API_Set_EgressPort_phyPort(MEA_Unit_t                unit,
                                          MEA_Port_t                port,
                                          MEA_Port_t                phy_port);

MEA_Bool MEA_API_Set_EgressPort_Is_AdminUp(MEA_Unit_t unit, MEA_Port_t port);


MEA_Status MEA_API_PortEgress_UpdateAdminUp(MEA_Unit_t               unit_i);

MEA_Status MEA_API_PortEgress_Event_AdminStatus(MEA_Unit_t               unit_i,
                                                MEA_OutPorts_Entry_dbt  *eventPorts,  //1-port have event
                                                MEA_Bool                *exist);

MEA_Status MEA_API_Set_EgressPort_TxEnable(MEA_Unit_t     unit,  MEA_Port_t   port,MEA_Bool   tx_enable);



MEA_Status MEA_API_Get_Port2Interface_Id(MEA_Unit_t       unit,
                                        MEA_Port_t  port,
                                        MEA_Interface_t  *Id_o);

typedef struct{
    
    MEA_MacAddr        My_Mac;
}MEA_global_Egress_sa_mac_dbt;

MEA_Status MEA_API_Set_BRAS_SA_MAC_MASK(MEA_Unit_t     unit,
                                           MEA_Uint16     index,
                                           MEA_global_Egress_sa_mac_dbt * entry);
MEA_Status MEA_API_Get_BRAS_SA_MAC_MASK(MEA_Unit_t     unit,
                                           MEA_Uint16     index,
                                           MEA_global_Egress_sa_mac_dbt * entry);

MEA_Status MEA_API_Set_BRAS_SA_MAC_BY_Interface(MEA_Unit_t     unit, MEA_Interface_t id, MEA_Bool Enable);

 MEA_Status MEA_API_Get_BRAS_SA_MAC_BY_Interface(MEA_Unit_t     unit, MEA_OutPorts_Entry_dbt *interfaceInfo);

 MEA_Status MEA_API_Get_BRAS_Event_SA_MAC_InterfaceInfo(MEA_Unit_t     unit, MEA_OutPorts_Entry_dbt *interfaceInfo);

/************************************************************************/
/*                                                                      */
/************************************************************************/
typedef struct{

    MEA_Uint32      nettag1 :16;
    MEA_Uint32      nettag2 :16;

    MEA_Uint32      srcPort        : 7;
    MEA_Uint32      packet_type    : 2; // 0 - untag 1-Single VLAN 2-double tag 3- I-SID
    MEA_Uint32      action_state   : 3; // see MEA_EgressPort_Filter_t
    MEA_Uint32      enable_Sniffer : 1;
    MEA_Uint32      pad            :19;

}MEA_EgressACL_Entry_dbt;

MEA_Status MEA_API_EgressACL_Create(MEA_Unit_t               unit_i,
                                    MEA_EgressACL_Entry_dbt  *entry,
                                    MEA_Uint16                *indexId_o);

MEA_Status MEA_API_EgressACL_Set(MEA_Unit_t                unit_i,
                                 MEA_Uint16                indexId_i,
                                 MEA_EgressACL_Entry_dbt   *entry);

MEA_Status MEA_API_EgressACL_Delete(MEA_Unit_t               unit_i,
                                    MEA_Uint16                indexId_i);

MEA_Status MEA_API_EgressACL_Get(MEA_Unit_t               unit_i,
                                 MEA_Uint16                indexId_i,
                                 MEA_EgressACL_Entry_dbt  *entry);





/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA WRED Defines                                           */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_WRED_PROFILES_HW_SUPPORT  (MEA_Platform_Get_WredProfiles_SUPPORT())
#define MEA_WRED_NUM_OF_PROFILES (MEA_Platform_Get_MaxWredProfiles())
#define MEA_WRED_NUM_OF_PRI      (ENET_PLAT_QUEUE_NUM_OF_PRI_Q)
#define MEA_WRED_NUM_OF_ACM_MODE  (MEA_Platform_Get_MaxWredACM())    
#define MEA_WRED_DROP_PROBABILITY_MAX_VALUE    255  
#define MEA_WRED_NORMALIZE_AQS_MAX_VALUE    127


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA WRED Typedefs                                          */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct{
    MEA_Uint32 ECN_enable :1;
    MEA_Uint32 ECN_minTH  :8;  /* 0 ..60   jump of 4 */
    MEA_Uint32 ECN_maxTH  :8;  /* 64 .92   jump of 4 */
    MEA_Uint32 pad        :15;
}MEA_ECN_Entry_Data_dbt;


/* WRED specific entry data */
typedef struct {
    MEA_DropProbability_t  drop_probability;
} MEA_WRED_Entry_Data_dbt;

/* WRED Profile priority data */
typedef struct {
    MEA_WRED_Entry_Data_dbt wredTable[MEA_WRED_NORMALIZE_AQS_MAX_VALUE+1];
} MEA_WRED_Profile_Data_Priority_dbt;

/* WRED Profile data */
typedef struct {
    MEA_ECN_Entry_Data_dbt ECN_data;
    MEA_WRED_Profile_Data_Priority_dbt priorityTable[MEA_WRED_NUM_OF_PRI];
} MEA_WRED_Profile_Data_dbt;

/* WRED Profile key */
typedef struct {
    MEA_AcmMode_t        acm_mode;    /* 0 .. MEA_WRED_NUM_OF_ACM_MODE-1 */
    MEA_WredProfileId_t  profile_id;  /* 0 .. MEA_WRED_NUM_OF_PROFILES-1 */
} MEA_WRED_Profile_Key_dbt;

/* WRED Specific Entry key */
typedef struct {
    MEA_WRED_Profile_Key_dbt profile_key;
    MEA_Priority_t           priority;      /* 0 .. MEA_WRED_NUM_OF_PRI-1 */
    MEA_NormalizeAQS_t       normalize_aqs; /* 0 .. MEA_WRED_NORMALIZE_AQS_MAX_VALUE */
} MEA_WRED_Entry_Key_dbt;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA WRED APIs                                              */
/*                                                                               */
/* Notes: - There is one WRED Table per WRED profile id.                         */
/*          The number of WRED profiles is 8 or 4                                */
/*          (depend on the device support - MEA_WRED_NUM_OF_PROFILES)            */
/*                                                                               */
/*          The WRED profile id is associate with the priority queue that the    */
/*          was schedule to it (regardless the cluster queue).                   */
/*          Note: In case the device support only 4 WRED profiles then each      */
/*                2 priority queues associate to the same WRED profile.          */
/*                                                                               */
/*        - Each Table has 0..MEA_WRED_NORMALIZE_AQS_MAX_VALUE rows ,            */
/*          that each row identify  Normalize Average Queue Size Utilization -   */
/*                                (AQS/MQS) * MEA_WRED_NORMALIZE_AQS_MAX_VALUE   */
/*          used to access this table when yellow packet arrive                   */
/*        - The value in each entry define the probability to drop yellow packet  */
/*          in case we are in specific normalize aqs.                            */
/*        - When yellow packet arrive we generate random number in range of      */
/*          0..MEA_WRED_DROP_PROBABILITY ,  that will be compare against the     */
/*          drop_probability value in the WRED entry.                            */
/*          If the value is less then the packet will be drop.                   */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

/* Create   WRED Profile */
MEA_Status MEA_API_Create_WRED_Profile  (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i,
                                         MEA_WRED_Profile_Data_dbt *data_i);

/* Delete   WRED Profile */
MEA_Status MEA_API_Delete_WRED_Profile  (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i);

/* Set      WRED Profile */
MEA_Status MEA_API_Set_WRED_Profile     (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i,
                                         MEA_WRED_Profile_Data_dbt *data_i);

/* Get      WRED Profile */
MEA_Status MEA_API_Get_WRED_Profile     (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i,
                                         MEA_WRED_Profile_Data_dbt *data_o);

/* GetFirst WRED Profile */
MEA_Status MEA_API_GetFirst_WRED_Profile(MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_o,
                                         MEA_WRED_Profile_Data_dbt *data_o,
                                         MEA_Bool                  *found_o);

/* GetNext WRED Profile */
MEA_Status MEA_API_GetNext_WRED_Profile (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_io,
                                         MEA_WRED_Profile_Data_dbt *data_o,
                                         MEA_Bool                  *found_o);

/* IsValid WRED Profile - Check if exist silent */
MEA_Bool MEA_API_IsValid_WRED_Profile   (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i,
                                         MEA_Bool                   silent_i);

/* Get specific WRED Entry */
MEA_Status MEA_API_Get_WRED_Entry       (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Entry_Key_dbt    *key_i,
                                         MEA_WRED_Entry_Data_dbt   *data_o);

/* Set specific WRED Entry */
MEA_Status MEA_API_Set_WRED_Entry       (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Entry_Key_dbt    *key_i,
                                         MEA_WRED_Entry_Data_dbt   *data_i);

/* set same value data_i->drop_probability for all entries 
   key_from_i->normalize_aqs .. key_to_i->normalize_aqs 
   Note: key_from_i->profile/priority must be equal to key_to_i->profile/priority */
MEA_Status MEA_API_Set_WRED_EntryRange  (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Entry_Key_dbt    *key_from_i,
                                         MEA_WRED_Entry_Key_dbt    *key_to_i,
                                         MEA_WRED_Entry_Data_dbt   *data_i);

/* set drop probability graph of values 

     DY = data_from_i->drop_probability  .. data_to_i->drop_probability
     DX = key_from_i->normalize_aqs      .. key_to_i->normalize_aqs 

     Note: key_from_i->profile/priority must be equal to to_key_i->profile/priority 

     Algorithm (Pseudo Code) :

       MEA_Wred_Entry_Key_dbt  key;
       MEA_Wred_Entry_Data_dbt data;

       MEA_OS_memsset(&data,0,sizeof(&data));
       MEA_OS_memcpy(&key,key_from_i,sizeof(key));
     - slope = 10*DY/DX 
     - for each key.normalize_aqs between key_from_i->normalize_aqs..key_to_i->normalize_aqs loop

         - data.drop_probability = (key.normalize_aqs - key_from_i->normalize_aqs)*slope;
         - if (data.drop_probability % 10 > 5) {
              - data.drop_prbability /= 10;
              - data.drop_prbability ++;
           } else {
              - data.drop_probability /= 10;
           }
         - data.drop_probability +=  data_from_i->drop_probability;  

         - MEA_API_Set_WRED_Entry(unit,&key,&data);

       end loop

 */   
MEA_Status MEA_API_Set_WRED_EntryCurve  (MEA_Unit_t                 unit_i,
                                         MEA_WRED_Entry_Key_dbt    *key_from_i,
                                         MEA_WRED_Entry_Key_dbt    *key_to_i,
                                         MEA_WRED_Entry_Data_dbt   *data_from_i,
                                         MEA_WRED_Entry_Data_dbt   *data_to_i);






/*===============================================================================*/
/* Entity Name        : Queue                                                    */
/*                                                                               */
/* Entity Description : Define the queue                                         */
/*                      Each queue point to port or port group. And can be more  */
/*                      then one queue per port/portGroup.                       */
/*                      The schedule between the queues in the same              */
/*                      port/portGroup done according to the queue type           */ 
/*                      strict or cwrr.                                          */
/*                      Each Queue has n priority queues , and the schedule      */
/*                      between the priority queues inside the queue done by the */
/*                      priority queue type (strict or cwrr)                     */
/*                      Each priority queue has definition for max queue size    */
/*                                                                               */
/* Entity Key         : ENET_Unit_t + ENET_QueueId_t                             */
/*                                                                               */
/* Entity Attributes  : ENET_Queue_dbt                                           */
/*                                                                               */
/*===============================================================================*/

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < Queue > Defines                                                 */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT ((MEA_Platform_Get_MaxNumOfClusters() == 64 ) ? 128 :(MEA_Platform_Get_MaxNumOfClusters()))
#define ENET_MAX_NUM_OF_INTERNAL_QUEUE_ELEMENT (MEA_Platform_Get_MaxNumOfClusters())
#define ENET_MAX_NUM_OF_INTERNAL_MC_ELEMENT      (MEA_Platform_Get_MaxNumOfClusters()) //256 //128 //64


#define ENET_QUEUE_NUMBER_OF_INTERNAL_QUEUES_PER_GROUP ((MEA_DRV_XPER_NUM_CLUSTER_GROUP == 0) ? 64 : (MEA_DRV_XPER_NUM_CLUSTER_GROUP == 1) ? 128 : (MEA_DRV_XPER_NUM_CLUSTER_GROUP == 2) ? 256 : 64       )
#define ENET_QUEUE_INTERNAL_ID_START_RANGE(groupId) ((groupId)*ENET_QUEUE_NUMBER_OF_INTERNAL_QUEUES_PER_GROUP)
#define ENET_QUEUE_INTERNAL_ID_END_RANGE(groupId) ((ENET_QUEUE_INTERNAL_ID_START_RANGE(groupId+1))-1)

#define MEA_QUEUE_MTU_MAX_VALUE (MEA_MTU_SIZE_DEF_VAL)
#define MEA_QUEUE_MTU_PRIQUEUE_MAX_VALUE(MEA_QUEUE_PRIQUEUE_MTU_MAX_VAL)
#define MEA_QUEUE_MTU_CLUSTER_MAX_VALUE(MEA_QUEUE_CLUSTER_MTU_MAX_VAL)

#define MEA_QUEUE_MTU_PRIQUEUE_GRANULARITY_VALUE  64

#define ENET_QUEUE_CLUSTER_WFQ_WEIGHT_MAX_VALUE 112
#define ENET_QUEUE_CLUSTER_WFQ_WEIGHT_MIN_VALUE 3
#define ENET_QUEUE_PRIORITY_WFQ_WEIGHT_MAX_VALUE 112
#define ENET_QUEUE_PRIORITY_WFQ_WEIGHT_MIN_VALUE 3

#define ENET_QUEUE_PRIORITY_RR_WEIGHT_VALUE 10


#define MEA_QUEUE_VIRTUAL_PORT_START_SRC_PORT 80

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < Queue > Typedefs                                                */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* Queue Parent Port type */
typedef enum {
    ENET_QUEUE_PORT_TYPE_PORT,
    ENET_QUEUE_PORT_TYPE_PORT_GROUP,
    ENET_QUEUE_PORT_TYPE_VIRTUAL, 
    ENET_QUEUE_PORT_TYPE_LAST
} ENET_Queue_port_type_te;

/* Queue Parent Port definitions */
typedef struct {
  ENET_Queue_port_type_te type;
  union {
    ENET_PortId_t        port;
    ENET_PortGroupId_t   portGroup;
    ENET_VirtualPortId_t virtualPort;
  } id;
} ENET_Queue_port_dbt; 

/* Queue scheduling mode type */
typedef enum {
    ENET_QUEUE_MODE_TYPE_STRICT = 0,
    ENET_QUEUE_MODE_TYPE_WFQ,
    ENET_QUEUE_MODE_TYPE_RR,
    ENET_QUEUE_MODE_TYPE_LAST
} ENET_Queue_mode_type_te;


/* Queue scheduling mode definitions */
typedef struct {
    ENET_Queue_mode_type_te  type;
    union {
       ENET_Uint8  wfq_weight;  /* ENET_QUEUE_CLUSTER_WFQ_WEIGHT_MAX/MIN_VALUE */
       ENET_Uint8  strict_priority;     
    } value;
    ENET_Uint8  pad1;
    ENET_Uint16 pad2;
} ENET_Queue_mode_dbt;

/* Queueu priority queue scheduling mode type */
typedef enum {
    ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT,
    ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ,
    ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR,
    ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_LAST
} ENET_Queue_priority_queue_mode_type_te;


/* priority Queue scheduling mode definitions */
typedef struct {
    ENET_Queue_priority_queue_mode_type_te  type;
    union {
       ENET_Uint8  wfq_weight;  /* ENET_QUEUE_PRIORITY_WFQ_WEIGHT_MAX/MIN_VALUE */
       ENET_Uint8  strict_priority;     
    } value;
    ENET_Uint8  pad1;
    ENET_Uint16 pad2;
} ENET_Queue_priority_queue_mode_dbt;

/* Queue Priority Queue definitions */
typedef struct {
    ENET_Queue_priority_queue_mode_dbt mode;
    ENET_Mqs_t                         max_q_size_Packets;
    ENET_Mqs_t                         max_q_size_Byte;
    MEA_Mtu_t                          mtu;
    ENET_Uint16                        mc_MQS;
    ENET_ShaperId_t                    shaper_Id              ;
    MEA_Uint32                         shaperPri_enable       :  1; /* 0- Disable 1-Enable*/
    MEA_Uint32                         Shaper_compensation    :  2; /* 00-Chank 01-Packet 11-AAL5 */
    MEA_Uint32                         pad                    : 29;
#ifdef ARCH64
		MEA_Uint32 pad64_0			  :32;
#endif

    MEA_shaper_info_dbt                shaper_info;
    
} ENET_Queue_priority_queue_dbt;


/* Queue (cluster) group id. 
   When user duplicate traffic to more then one cluster ,
   all cluster that are as output cluster of this kind of traffic
   must be from the same group id.
   In case the traffic is p2p , there is no meaning to the group 
   */
typedef enum {
    ENET_QUEUE_GROUP_ID_0,
    ENET_QUEUE_GROUP_ID_1,
    ENET_QUEUE_GROUP_ID_2,
    ENET_QUEUE_GROUP_ID_3,
    ENET_QUEUE_GROUP_ID_4,
    ENET_QUEUE_GROUP_ID_5,
    ENET_QUEUE_GROUP_ID_6,
    ENET_QUEUE_GROUP_ID_7,
    ENET_QUEUE_GROUP_ID_8,
    ENET_QUEUE_GROUP_ID_9,
    ENET_QUEUE_GROUP_ID_10,
    ENET_QUEUE_GROUP_ID_11,
    ENET_QUEUE_GROUP_ID_12,
    ENET_QUEUE_GROUP_ID_13,
    ENET_QUEUE_GROUP_ID_14,
    ENET_QUEUE_GROUP_ID_15,
    ENET_QUEUE_GROUP_ID_16,
    ENET_QUEUE_GROUP_ID_LAST=17
    
} ENET_Queue_groupId_te;

#define ENET_QUEUE_GROUP_ID_LAST ((MEA_DRV_XPER_NUM_CLUSTER_GROUP == 0) ? (ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT / 64) : (MEA_DRV_XPER_NUM_CLUSTER_GROUP == 1 ) ? (ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT/128) :(ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT/128)) 

/* Queue Definitions */
typedef struct {

    /* Null terminated string that hold the name of the port
       Note: If the value is ENET_GENERATE_NEW_NAME that the name will be generate 
             automatic */
    char                name [ENET_PLAT_QUEUE_MAX_NAME_SIZE+1];
    MEA_Bool            adminOn; /*enable disable this cluster */
    /* The parent Port/portGroup of this queue */
    ENET_Queue_port_dbt port;

    /* scheduling mode information  */
    ENET_Queue_mode_dbt mode;

    /* shaper mode information  */

    /* priority queues */
    ENET_Queue_priority_queue_dbt  pri_queues[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];

    ENET_Uint32                    MTU    :14;
    ENET_Uint32                    pad0   :18;
#ifdef ARCH64
		MEA_Uint32 pad64_0			  :32;
#endif
    MEA_Uint32                     shaper_enable       : 1; /* 0- Disable 1-Enable*/  
    MEA_Uint32                     Shaper_compensation : 2; /* 00-Chunk 01-Packet 11-AAL5 */
    MEA_Uint32                     pad                 :29;
#ifdef ARCH64
		MEA_Uint32 pad64_1			  :32;
#endif
    ENET_ShaperId_t                shaper_Id ; 
    MEA_shaper_info_dbt            shaper_info;
    //ENET_Queue_groupId_te groupId;
    MEA_Uint32 groupId;
    MEA_Bool              disableAnalyse;          
    struct {
        MEA_Uint32 valid            :1;
        MEA_Uint32 ports_groupId     :4;
        MEA_Uint32 num_of_port      :16;
        MEA_Uint32 pad              :11;
#ifdef ARCH64
		MEA_Uint32 pad64_2			  :32;
#endif
        MEA_OutPorts_Entry_dbt Multiple_Port; // only for bonding non
    }port_group;

    MEA_Uint32 InternalQueueId;
    MEA_Uint32 Internal_LQId;


} ENET_Queue_dbt;


typedef struct {
    ENET_Uint32 currentPacket;
    ENET_Uint32 currentBytes;
    ENET_Uint32 ActualMqsPacket;
    ENET_Uint32 ActualMqsByte;
    MEA_Mtu_t   ActualMtu;
} ENET_PriQueueSize_Info_t;


typedef struct {
ENET_Uint32 current_MC_MQS;
ENET_Uint32 configure_MC_MQS;
}ENET_Queue_MC_MQS_Size_Info_t;

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*             < Queue > APIs                                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/******************************************************************************
* Function Name:       < ENET_Create_Queue>
*
* Description:         This function creates a new Queue entity instance, 
*                      and gives Queue id handle that can be used later on to 
*                      retrieve this entity instance.
*------------------------------------------------------------------------------- 
* Input Parameters: 
*                   unit_i -> identifier of the device.
*                   entry_i ->  pointer to the Queue structure containing 
*                                  the new Queue parameters.  
*                   id_io   -> pointer to Queue id as input use to 
*                              define the new queue id 
*                              OR ENET_GENERATE_NEW_ID to generate automatic new id 
* Output Parameters: 
*                   id_io   -> pointer to queue id ass output use to 
*                              get the id that used for create the queue instance
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Create_Queue      (ENET_Unit_t            unit_i,
                                    ENET_Queue_dbt         *entry_i,
                                     ENET_QueueId_t         *id_o);



/******************************************************************************
* Function Name: < ENET_Set_Queue>
*
* Description:   This function is responsible for modifying an existing Queue 
*                 entity instance.                    
*                      
*------------------------------------------------------------------------------- 
* Input Parameters: unit_i  -> identifier of the device.
*                   id_i    -> Queue id to use for the search of the entity that
*                              needs to be modified.        
*                   entry_i -> pointer to a Queue structure containing the 
*                              Queue's  new parameters.
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/                                                 
ENET_Status ENET_Set_Queue         (ENET_Unit_t            unit_i,
                                    ENET_QueueId_t         id_i,
                                    ENET_Queue_dbt         *entry_i);




/******************************************************************************
* Function Name: < ENET_Get_Queue >
*
* Description:   This function is responsible for retrieving an existing Queue
*                entity instance by its Queue id.
*                      
*------------------------------------------------------------------------------- 
* Input Parameters: unit_i  -> identifier of the device.
*                   id_i    -> Queue id to use for the search of the entity that 
*                              needs to be retrieved.                              
*                   entry_i -> pointer to a Queue structure containing the 
*                              Queue's  new parameters
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/        
ENET_Status ENET_Get_Queue         (ENET_Unit_t            unit_i,
                                    ENET_QueueId_t         id_i,
                                    ENET_Queue_dbt         *entry_o );




/******************************************************************************
* Function Name: < ENET_Delete_Queue >
*
* Description:   This function is responsible for deleting an existing Queue 
*                entity instance, by its Queue ID.
*                      
*------------------------------------------------------------------------------- 
* Input Parameters: 
*                   unit_i -> identifier of the device.
*                   id_i   -> id of the Queue instance that needs to be deleted. 
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/                                           
ENET_Status ENET_Delete_Queue      (ENET_Unit_t            unit_i,
                                    ENET_QueueId_t         id_i);


ENET_Status ENET_Delete_all_Queues(ENET_Unit_t            unit_i);

/******************************************************************************
* Function Name: < ENET_GetFirst_Queue >
*
* Description:   This function is responsible for retrieving the first Queue 
*                entity instance in the DB.
*                      
*------------------------------------------------------------------------------- 
* Input Parameters: 
*                   unit_i  -> identifier of the device.
* Output Parameters: 
*                   id_o    -> id of the first Queue instance.
*                   entry_o -> pointer to a Queue structure where to store the 
*                               first entity's parameters. Optional.
*                               If NULL - function will not return the entity's parameters.
*                    found_o -> pointer to ENET_Bool with the search results 
*                               - ENET_TRUE-found, ENET_FALSE-not found.
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/                                       
ENET_Status ENET_GetFirst_Queue    (ENET_Unit_t            unit_i,

                                    ENET_QueueId_t         *id_o,
                                    ENET_Queue_dbt         *entry_o,
                                    ENET_Bool              *found_o);




/******************************************************************************
* Function Name: < ENET_GetNext_Queue >
*
* Description:   This function is responsible for retrieving the next Queue  
*                entity instance in the DB, after a specified instance.
*                      
*------------------------------------------------------------------------------- 
* Input Parameters: 
*                   unit_i  -> identifier of the device.
*                   id_io    -> pointer that on input points to the current Queue ID.
*                               On output it points to the ID of the next Queue.
* Output Parameters: 
*                   id_io    -> pointer that on input points to the current Queue ID.
*                               On output it points to the ID of the next Queue.
*                   entry_o -> pointer to a Queue structure where to store the 
*                               retrieved entity's parameters.
*                   found_o -> pointer to ENET_Bool with the search results 
*                              - ENET_TRUE-found, ENET_FALSE-not found.
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/                                       
ENET_Status ENET_GetNext_Queue     (ENET_Unit_t            unit_i,
                                    ENET_QueueId_t         *id_io,
                                    ENET_Queue_dbt         *entry_o,
                                    ENET_Bool              *found_o);




/******************************************************************************
* Function Name: < ENET_GetIDByName_Queue >
*
* Description:   This function is responsible for retrieving a Queue 
*                instance's ID parameter, by is name.
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i  -> identifier of the device.
*                   name    -> string containing the name of the Queue instance.
* Output Parameters: 
*                    id_o    -> pointer for ENET_QueueId_t that will contain the Queue's ID.
*                    found_o -> pointer to a Boolean that states if the matching Queue was found.
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/                                         
ENET_Status ENET_GetIDByName_Queue (ENET_Unit_t               unit_i,
                                    char                     *name_i,
                                     ENET_QueueId_t           *id_o,
                                    ENET_Bool                *found_o);       

/******************************************************************************
* Function Name: < ENET_IsValid_Queue >
*
* Description:   This function is responsible for checking if the queue id 
*                is valid , and the queue instance exist
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i   -> identifier of the device.
*                    id_i     -> ENET_QueueId_t that will contain the Queue's ID.
*                    silent_i -> Boolean that define if not print log error message
*
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_TRUE, the queue id is     valid and    exist.
*     ENET_FALSE,the queue id is not valid or not exist
*
*******************************************************************************/                                                                                                               
ENET_Bool  ENET_IsValid_Queue(ENET_Unit_t    unit_i,
                              ENET_QueueId_t id_i,
                              ENET_Bool      silent_i);


ENET_Status ENET_IsAllowedToDelete_Queue (ENET_Unit_t    unit_i,
                                          ENET_QueueId_t id_i); /* external */

/******************************************************************************
* Function Name: < ENET_Get_PriQueueSize_Info >
*
* Description:   This function is responsible for retrieving a Queue status 
*                By Queue_ID + priorityQueue_ID.
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i   -> identifier of the device.
*                    id_i     -> ENET_QueueId_t the ID of the Que containing the PriQueue.
*                    pri_id_i -> ENET_PriQueueId_t The ID of the priQueue in the Cluster.
* Output Parameters: 
*                    info -> pointer to a ENET_PriQueueSize_Info_t that contains the priQueue status
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/                                         
ENET_Status ENET_Get_PriQueueSize_Info(ENET_Unit_t               unit,
                                       ENET_QueueId_t            id_i,
                                       ENET_PriQueueId_t         pri_id_i,
                                       ENET_PriQueueSize_Info_t *info);




ENET_Status ENET_Get_realQueueMC_MQS_Size_Info_drv(ENET_Unit_t               unit,
                                                   ENET_QueueId_t            id_i,/*external*/
                                                   ENET_PriQueueId_t         pri_id_i,
                                                   ENET_Queue_MC_MQS_Size_Info_t *info);



/************************* INDEX-  3.2 Actions    ********************************/
/*                                                                               */
/*                                                                               */
/*    3.2 Actions                                                                */
/*             3.2.1 Edit                                  (advanced - option)   */
/*             3.2.2 policer                               (advanced - option)   */
/*             3.2.3 counter profile                       (advanced - option)   */
/*             3.2.4 action profile                        (advanced - option)   */
/*                                                                               */
/*********************************************************************************/


#define MEA_SET_OUTPORT(OutPorts_Entry_pi,port_i) \
    (((MEA_Uint32*)(&((OutPorts_Entry_pi)->out_ports_0_31))))[(port_i)/32] |= ( (0x00000001 << ((port_i)%32)))

#define MEA_CLEAR_OUTPORT(OutPorts_Entry_pi,port_i) \
    (((MEA_Uint32*)(&((OutPorts_Entry_pi)->out_ports_0_31))))[(port_i)/32] &= (~(0x00000001 << ((port_i)%32)))

#define MEA_IS_SET_OUTPORT(OutPorts_Entry_pi,port_i) \
   ((((MEA_Uint32*)(&((OutPorts_Entry_pi)->out_ports_0_31))))[(port_i)/32] &  ( (0x00000001 << ((port_i)%32))))

#define MEA_IS_CLEAR_OUTPORT(OutPorts_Entry_pi,port_i) \
    (!MEA_IS_SET_OUTPORT((OutPorts_Entry_pi),(port_i)))

#define MEA_CLEAR_ALL_OUTPORT(OutPorts_Entry_pi) \
    MEA_OS_memset((char*)(&((OutPorts_Entry_pi)->out_ports_0_31)),0,sizeof(*(OutPorts_Entry_pi)))
MEA_Bool MEA_Is_Clear_All_OUTPORT(MEA_OutPorts_Entry_dbt *OutPorts_Entry_pi);

#define MEA_IS_CLEAR_ALL_OUTPORT(OutPorts_Entry_pi) (MEA_Is_Clear_All_OUTPORT(OutPorts_Entry_pi))


#define MEA_OUTPORT_FOR_LOOP(OutPorts_Entry_pi,port_io) \
    for ((port_io)=0;(port_io)<(sizeof(*(OutPorts_Entry_pi))*8);(port_io)++) \
         if (MEA_IS_SET_OUTPORT((OutPorts_Entry_pi),(port_io)))  






#define MEA_SET_SID(MEA_PUint32_t,sid_i) \
    (((MEA_Uint32*)(&(MEA_PUint32_t[0]))))[(sid_i)/32] |= ( (0x00000001 << ((sid_i)%32)))

#define MEA_CLEAR_SID(MEA_PUint32_t,sid_i) \
    (((MEA_Uint32*)(&(MEA_PUint32_t[0]))))[(sid_i)/32] &= (~(0x00000001 << ((sid_i)%32)))

#define MEA_IS_SET_SID(MEA_PUint32_t,sid_i) \
 ( ( ((((MEA_Uint32*)(&(MEA_PUint32_t[0]))))[(sid_i)/32] &  ( (0x00000001 << ((sid_i)%32)))) > 0) ? (MEA_TRUE) : (MEA_FALSE) )

#define MEA_IS_CLEAR_SID(MEA_PUint32_t,sid_i) \
    (!MEA_IS_SET_SID((MEA_PUint32_t),(sid_i)))











/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*             MEA Egress Headers Processor Defines                              */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

// MEA_EgressHeaderProc_Entry_dbt val.vlan.eth_type  field values 
#define MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE    0x8100

// MEA_EgressHeaderProc_Entry_dbt stamp_color    field values 
#define MEA_EGRESS_HEADER_PROC_NO_STAMP_COLOR    0    
#define MEA_EGRESS_HEADER_PROC_STAMP_COLOR       1

// MEA_EgressHeaderProc_Entry_dbt stamp_priority field values 
#define MEA_EGRESS_HEADER_PROC_NO_STAMP_PRIORITY 0    
#define MEA_EGRESS_HEADER_PROC_STAMP_PRIORITY    1


// MEA_EgressHeaderProc_Entry_dbt command        field values 
#define MEA_EGRESS_HEADER_PROC_CMD_TRANS         0    
#define MEA_EGRESS_HEADER_PROC_CMD_APPEND        1
#define MEA_EGRESS_HEADER_PROC_CMD_EXTRACT       2
#define MEA_EGRESS_HEADER_PROC_CMD_SWAP          3
#define MEA_EGRESS_HEADER_PROC_CMD_SWAP_MAC      4
#define MEA_EGRESS_HEADER_PROC_CMD_SWAP_MAC_AND_IP_ANS_SET_TTL      5

// MEA_EgressHeaderProc_Entry_dbt valid          field values 
#define MEA_EGRESS_HEADER_PROC_NO_VALID          0    
#define MEA_EGRESS_HEADER_PROC_VALID             1

// MEA_EgressHeaderProc_Entry_dbt default_sid    field values 
#define MEA_EGRESS_HEADER_PROC_NO_DEFAULT_SID    0    
#define MEA_EGRESS_HEADER_PROC_DEFAULT_SID       1



#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_CFI_BIT_OFFSET                12
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_PRI_BIT0_OFFSET                13
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_PRI_BIT1_OFFSET                14
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_VLAN_PRI_BIT2_OFFSET                15
 
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_E_LSP_CL_BIT_OFFSET      12
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_E_LSP_PRI_BIT0_OFFSET        9
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_E_LSP_PRI_BIT1_OFFSET       10
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_E_LSP_PRI_BIT2_OFFSET    11

#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_L_LSP_CL_BIT_OFFSET       9
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_L_LSP_PRI_BIT0_OFFSET       12
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_L_LSP_PRI_BIT1_OFFSET       13
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_MARTINI_L_LSP_PRI_BIT2_OFFSET    14

#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_CFI_BIT_OFFSET                 0
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT0_OFFSET                 4
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT1_OFFSET                 5
#define MEA_EGRESS_HEADER_PROC_STAMP_INFO_ATM_PRI_BIT2_OFFSET                 6



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*             MEA Egress Headers Processor Typedefs                             */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    MEA_Uint32 valid        :1;
    MEA_Uint32 protocol     :2;  // 0-   1-vlan 2- 3-
    MEA_Uint32 llc          :1;  // 1-llc
    MEA_Uint32 pad          :28;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
}MEA_EgressProce_Protocol_Attribute_dbt;

typedef struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32  color_bit0_loc      :5;  /*  0.. 4 */
    MEA_Uint32  color_bit0_valid    :1;  /*  5.. 5 */
    MEA_Uint32  color_bit1_loc      :5;  /*  6..10 */
    MEA_Uint32  color_bit1_valid    :1;  /* 11..11 */
    MEA_Uint32  pri_bit0_loc        :5;  /* 12..16 */ 
    MEA_Uint32  pri_bit0_valid      :1;  /* 17..17 */
    MEA_Uint32  pri_bit1_loc        :5;  /* 18..22 */
    MEA_Uint32  pri_bit1_valid      :1;  /* 23..23 */
    MEA_Uint32  pri_bit2_loc        :5;  /* 24..28 */
    MEA_Uint32  pri_bit2_valid      :1;  /* 29..29 */
    MEA_Uint32  pad                 :2;  /* 30..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  pad                 :2;  /* 30..31 */
    MEA_Uint32  pri_bit2_valid      :1;  /* 29..29 */
    MEA_Uint32  pri_bit2_loc        :5;  /* 24..28 */
    MEA_Uint32  pri_bit1_valid      :1;  /* 23..23 */
    MEA_Uint32  pri_bit1_loc        :5;  /* 18..22 */
    MEA_Uint32  pri_bit0_valid      :1;  /* 17..17 */
    MEA_Uint32  pri_bit0_loc        :5;  /* 12..16 */ 
    MEA_Uint32  color_bit1_valid    :1;  /* 11..11 */
    MEA_Uint32  color_bit1_loc      :5;  /*  6..10 */
    MEA_Uint32  color_bit0_valid    :1;  /*  5.. 5 */
    MEA_Uint32  color_bit0_loc      :5;  /*  0.. 4 */
#endif


}MEA_EgressHeaderProc_EthStampingInfo_dbt;

typedef struct{ 
    MEA_Uint16  EtherType;
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint16  E_L_LSP : 1;    
    MEA_Uint16  pad     :15;
#ifdef ARCH64
	//MEA_Uint32 pad64_0	    	:48;
#endif
#else
#ifdef ARCH64
	//MEA_Uint32 pad64_0	    	:48;
#endif
    MEA_Uint16  pad     :15;
    MEA_Uint16  E_L_LSP : 1;    
#endif
    MEA_MacAddr DA;       
    MEA_MacAddr SA;
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32  martini_cmd :2;
    MEA_Uint32  pad_martini:30;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
     MEA_Uint32  pad_martini:30;
     MEA_Uint32  martini_cmd :2;
#endif

} MEA_EgressHeaderProc_MartiniInfo_dbt;


typedef struct{
    union {
       struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32 vpi_hi  : 4;
         MEA_Uint32 gfc     : 4;
         MEA_Uint32 vci_hi  : 4;
         MEA_Uint32 vpi_lo  : 4;
         MEA_Uint32 vci     : 8;
         MEA_Uint32 clp     : 1;
         MEA_Uint32 pti_eof : 1;
         MEA_Uint32 pti_congestion : 1;
         MEA_Uint32 pti_ctl : 1;
         MEA_Uint32 vci_lo  : 4;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32 vci_lo  : 4;
         MEA_Uint32 pti_ctl : 1;
         MEA_Uint32 pti_congestion : 1;
         MEA_Uint32 pti_eof : 1;
         MEA_Uint32 clp     : 1;
         MEA_Uint32 vci     : 8;
         MEA_Uint32 vpi_lo  : 4;
         MEA_Uint32 vci_hi  : 4;
         MEA_Uint32 gfc     : 4;
         MEA_Uint32 vpi_hi  : 4;
#endif 
       } cell_header;

       struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32 pppose_session : 16;
         MEA_Uint32 pad0 : 16;
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32 pad0 : 16;
         MEA_Uint32 pppose_session : 16;
#endif 
       } pppoe_session_id;

       struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
     MEA_Uint32  vid            : 12;
     MEA_Uint32  ci             :  1;
     MEA_Uint32  pri            :  3;
     MEA_Uint32  eth_type       : 16;
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
     MEA_Uint32  eth_type       : 16;
     MEA_Uint32  pri            :  3;
     MEA_Uint32  ci             :  1;
     MEA_Uint32  vid            : 12;
#endif
       } double_vlan_tag;

          MEA_Uint32 all;
    } val;

#if __BYTE_ORDER == __LITTLE_ENDIAN
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  stamp_color    :  1; 
    MEA_Uint32  stamp_priority :  1; 
    MEA_Uint32  double_tag_cmd :  2;
    MEA_Uint32  mapping_enable :  1; /* editing mapping enable */
    MEA_Uint32  mapping_id     :  4; /* editing mapping profile id */
    MEA_Uint32  pad0           :  23;
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  pad0           :  23;
    MEA_Uint32  mapping_id     :  4; /* editing mapping profile id */
    MEA_Uint32  mapping_enable :  1; /* editing mapping enable */
    MEA_Uint32  double_tag_cmd :  2;
    MEA_Uint32  stamp_priority :  1; 
    MEA_Uint32  stamp_color    :  1; 
#endif

} MEA_EgressHeaderProc_Atm_Entry_dbt;

typedef enum{
	MEA_EHP_FLOW_TYPE_NONE  =0,
	MEA_EHP_FLOW_TYPE_UNTAG =1,
	MEA_EHP_FLOW_TYPE_TAG   =2,
	MEA_EHP_FLOW_TYPE_QTAG  =3,
	MEA_EHP_FLOW_TYPE_LAST  
}MEA_ehp_Flow_type_t;
typedef enum {
	MEA_EHP_SWAP_TYPE_NONE = 0,
	MEA_EHP_SWAP_TYPE_TAG = 1,
	MEA_EHP_SWAP_TYPE_QTAG = 2,
	MEA_EHP_SWAP_TYPE_BOTH = 3,
	MEA_EHP_SWAP_TYPE_LAST
}MEA_ehp_Swap_type_t;
typedef enum{
    MEA_EHP_FLOW_MARK_NONE = 0,
    MEA_EHP_FLOW_MARK_INNER = 1,
    MEA_EHP_FLOW_MARK_OUTER = 2,
    MEA_EHP_FLOW_MARK_OUTER_INNER = 3,
    MEA_EHP_FLOW_MARK_LAST
}MEA_ehp_mark_from_type_t;

typedef struct{
    union {

        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  flow_type           : 2;
            MEA_Uint32  enc_swap_mode       : 2; /*bit 3:2*/
            MEA_Uint32  enc_outer_type      : 1; /*bit 4*/   /*atm*/
            MEA_Uint32  enc_inner_type      : 1; /*bit 5*//*bit 1:0*/ /*vlan*/
            MEA_Uint32  notused             : 2; /*7:6*/ 
            MEA_Uint32  profid              : 3; /*10:8*/
            MEA_Uint32  Attribute_protocol  : 2;    /*  nMEA_EgressProce_Protocol_Attribute_dbt */
            MEA_Uint32  Attribute_llc       : 1;    /*  MEA_EgressProce_Protocol_Attribute_dbt  */
            MEA_Uint32  stamp802_1p         : 3;
            MEA_Uint32  pad                 : 15;
            
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32  pad                 : 15;
            MEA_Uint32  stamp802_1p         : 3;
            MEA_Uint32  Attribute_llc       : 1; /*  MEA_EgressProce_Protocol_Attribute_dbt */
            MEA_Uint32  Attribute_protocol  : 2; /*  MEA_EgressProce_Protocol_Attribute_dbt  */
            MEA_Uint32  profid              : 3; /*10:8*/ /*gw_ip*/
            MEA_Uint32  notused             : 2; /*7:6*/ 
            MEA_Uint32  enc_inner_type      : 1; /*bit 5*//*bit 1:0*/ /*vlan*/
            MEA_Uint32  enc_outer_type      : 1; /*bit 4*/   /*atm*/
            MEA_Uint32  enc_swap_mode       : 2; /*bit 3:2*/
            MEA_Uint32  flow_type           : 2; /*bit 1:0*/ /*see MEA_ehp_Flow_type_t */
#endif
        } data_wbrg;

        MEA_Uint32 all;
    } val;



} MEA_EgressHeaderProc_Wbrg_Entry_dbt;

typedef enum {
/*  0 transparent
    1 - swap DA & SA , LBM -> LBR | DMM -> DMR | LMM -> LMR, DMR TX stamp 3rd(TxTimeStampb) |  LMM->LMR, stamp LMR RX 2nd(RxFCf) & LMR TX 3rd(TxFCb)
    2 - DMM TX - stamp 1st(TxTimeStampf) | LMM TX - stamp 1st(TxFCf) | CCM TX - stamp 1st(TxFCf),2nd(RxFCb),3rd(TxFCb)
    3 - LMR RX - stamp 4th(RxFCb) | CCM RX - stamp 4th(RxFCf)
    4 count CCM Transmit regular packet 1st(txfcf)
    5 - count CCM Receive regular packet 4th(rxfcf)
*/


    MEA_LM_COMMAND_TYPE_TRANSPARENT=0,
    MEA_LM_COMMAND_TYPE_SWAP_OAM=1,
    MEA_LM_COMMAND_TYPE_STAMP_TX=2,
    MEA_LM_COMMAND_TYPE_STAMP_RX=3,
    MEA_LM_COMMAND_TYPE_COUNT_CCM_TX=4,
    MEA_LM_COMMAND_TYPE_COUNT_CCM_RX=5,

    MEA_LM_COMMAND_TYPE_LAST
} MEA_LM_Command_t;

typedef struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 Command_cfm                  : 3; /*MEA_LM_Command_t*/
    MEA_Uint32 Command_dasa                 : 3; /* see */
    MEA_Uint32 LmId                         : 7;
    MEA_Uint32 Command_dscp_stamp_value     : 6;
    MEA_Uint32 Command_dscp_stamp_enable    : 1;
    MEA_Uint32 ETH_CS_packet_is_valid       : 1;
    MEA_Uint32 calcIp_checksum              : 1;
    MEA_Uint32 sw_calcIp_checksum_valid     : 1;
    MEA_Uint32 sw_calcIp_checksum_force     : 1;
    MEA_Uint32 valid                        : 1;
    MEA_Uint32 Command_IpPrioityType        : 2;
    MEA_Uint32 pad                          : 5;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad                          : 5;
    MEA_Uint32 Command_IpPrioityType        : 2;
    MEA_Uint32 valid                        : 1;
    MEA_Uint32 sw_calcIp_checksum_force     : 1;
    MEA_Uint32 sw_calcIp_checksum_valid     : 1;
    MEA_Uint32 calcIp_checksum              : 1;
    MEA_Uint32 ETH_CS_packet_is_valid       : 1;
    MEA_Uint32 Command_dscp_stamp_enable    : 1;
    MEA_Uint32 Command_dscp_stamp_value     : 6;
    MEA_Uint32 LmId                         : 7;
    MEA_Uint32 Command_dasa                 : 3; /* see */
    MEA_Uint32 Command_cfm                  : 3; /* see MEA_LM_Command_t*/

#endif
}MEA_EgressHeaderProc_LM_CounterId_dbt;

typedef struct{
    union { 
       struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32  vid            : 12;
         MEA_Uint32  ci             :  1;
         MEA_Uint32  pri            :  3;
         MEA_Uint32  eth_type       : 16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32  eth_type       : 16;
         MEA_Uint32  pri            :  3;
         MEA_Uint32  ci             :  1;
         MEA_Uint32  vid            : 12;
#endif
       } vlan;
       struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32  ttl            :  8;
         MEA_Uint32  s_bit          :  1;
         MEA_Uint32  exp            :  3;
         MEA_Uint32  label          : 20;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32  label          : 20;
         MEA_Uint32  exp            :  3;
         MEA_Uint32  s_bit          :  1;
         MEA_Uint32  ttl            :  8;
#endif
       } mpls;
       struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32  ttl            :  8;
         MEA_Uint32  s_bit          :  1;
         MEA_Uint32  exp            :  3;
         MEA_Uint32  label          : 20;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32  label          : 20;
         MEA_Uint32  exp            :  3;
         MEA_Uint32  s_bit          :  1;
         MEA_Uint32  ttl            :  8;
#endif
       } martini;

       struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32  sessionId      : 16;
         MEA_Uint32  pad            : 16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32  pad            : 16;
         MEA_Uint32  sessionId      : 16;
#endif
       } pppoe;

       MEA_Uint32 all;
    } val;

#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32  stamp_color    :  1; /* relevant only for command SWAP / APPEND */
    MEA_Uint32  stamp_priority :  1; /* relevant only for command SWAP / APPEND */    
    MEA_Uint32  command        :  3; /* see defines MEA_EGRESS_HEADER_PROC_CMD_XXX */
    MEA_Uint32  mapping_enable :  1; /* editing mapping enable */
    MEA_Uint32  mapping_id     :  4; /* editing mapping profile id */
    MEA_Uint32  pad            : 22;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  pad            : 22;
    MEA_Uint32  mapping_id     :  4; /* editing mapping profile id */
    MEA_Uint32  mapping_enable :  1; /* editing mapping enable */
    MEA_Uint32  command        :  3; /* see defines MEA_EGRESS_HEADER_PROC_CMD_XXX */
    MEA_Uint32  stamp_priority :  1; /* relevant only for command SWAP / APPEND */    
    MEA_Uint32  stamp_color    :  1; /* relevant only for command SWAP / APPEND */
#endif

} MEA_EgressHeaderProc_Eth_Entry_dbt;

typedef struct  
{
    MEA_Uint32 valid             : 1;
    MEA_Uint32 pad_0             : 31;

#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 Session_num      : 16;
    MEA_Uint32 enable           : 1; /* append Session */
    MEA_Uint32 extract_enable   : 1;  /* extract Session */
    MEA_Uint32 pad             : 14;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad              : 14;
    MEA_Uint32 extract_enable   : 1;  //
    MEA_Uint32 enable           : 1;
    MEA_Uint32 Session_num      : 16; // up to 512 session
#endif
}MEA_EgressHeaderProc_fragmentSession_dbt;

typedef struct  
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 clock_resolution     : 3;  /*bit 0..2*/
    MEA_Uint32 clock_domain         : 2;  /*bit3..4*/
    MEA_Uint32 rtp_exist            : 1;  /*bit5*/
    MEA_Uint32 vlan_exist           : 1;  /*bit6*/
    MEA_Uint32 ces_type             : 2;  /*bit7..8*/
    MEA_Uint32 loss_R               : 1;  /*bit9*/
    MEA_Uint32 loss_L               : 1;  /*bit10*/
    MEA_Uint32 valid                : 1; /*bit11*/
    MEA_Uint32 pad                  : 20;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad                  : 20;
    MEA_Uint32 valid                : 1;/*bit11*/
    MEA_Uint32 loss_L               : 1;  /*bit10*/
    MEA_Uint32 loss_R               : 1;  /*bit9*/
    MEA_Uint32 ces_type             : 2;  /*bit7..8*/  // 0-udp 1-vlan  2-MPLS 3-ip
    MEA_Uint32 vlan_exist           : 1;  /*bit6*/
    MEA_Uint32 rtp_exist            : 1;  /*bit5*/ 
    MEA_Uint32 clock_domain         : 2;  /*bit3..4*/ 
    MEA_Uint32 clock_resolution     : 3;  /*bit 0..2*/      
#endif
}MEA_EgressHeaderProc_pw_control_dbt;

typedef struct  
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
MEA_Uint32 lable ;
#else
MEA_Uint32 lable; /*0 31*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
MEA_Uint32 valid : 1;
MEA_Uint32 pad   : 31;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
MEA_Uint32 pad   : 31;
MEA_Uint32 valid : 1;/*32*/
#endif

}MEA_EgressHeaderProc_mpls_label_dbt;
typedef struct
{
    MEA_Bool     valid;
    MEA_Uint32   NAT_type; /*  2 - User_to_Network   3- Network_to_User*/
    MEA_MacAddr  Da;
    MEA_Uint32   Ip;
    MEA_Uint16   L4Port;
    MEA_Uint16   pad;
} MEA_EgressHeaderProc_NAT_dbt;

typedef struct {
    MEA_Bool     valid;
    MEA_MacAddr  Da_eNB;
    MEA_Uint32   dst_eNB_Ip;
    MEA_Uint32   TE_ID;
    MEA_Uint8    sgw_ipId;  /*0:7*/ /*not support*/
}MEA_EgressHeaderProc_IPCS_DL_dbt;
typedef struct {
    MEA_Bool valid;
    
    MEA_MacAddr Da_next_hop_mac;
    MEA_MacAddr Sa_host_mac;
    MEA_Uint32 apn_vlan;
    MEA_Uint8   id_pdn_IP;  /*0:7*/ 
} MEA_EgressHeaderProc_IPCS_UL_dbt;





typedef struct {
    MEA_Bool     valid;
    MEA_MacAddr  Da_eNB;
    MEA_Uint32   dst_eNB_Ip;
    MEA_Uint32   TE_ID;
    MEA_Uint32   dst_UE_Ip;
    MEA_Uint8    sgw_ipId;  /*0:7*/ /*not support*/
    
}MEA_EgressHeaderProc_ETHCS_DL_dbt;



typedef struct {
    MEA_Bool     valid;
    MEA_MacAddr  vxlan_Da;   /*vtep*/
    MEA_Uint32   vxlan_vni;

    MEA_Uint32   vxlan_dst_Ip;   /*vtep*/
    MEA_Uint32   l4_src_Port  :16;
    MEA_Uint32   l4_dest_Port :16;
    MEA_Uint32   ttl          : 8;
    MEA_Uint32   sgw_ipId     : 3;
    
}MEA_EgressHeaderProc_VxLan_DL_dbt;


typedef struct {
    MEA_Bool     valid;
    MEA_MacAddr Outer_da_MAC;
    MEA_Uint32  GRE_DST_IP;
    MEA_Uint32 VSID   :24 ;   //(virtual subnet ID)
    MEA_Uint32 FlowID : 8;

}MEA_EgressHeaderProc_NVGRE_dbt;

typedef enum {
    MEA_IPSec_tunnel_TYPE_NVGRE = 0,
    MEA_IPSec_tunnel_TYPE_only  = 1,
    MEA_IPSec_tunnel_TYPE_VXLAN = 2
}MEA_IPSec_tunnel_type_t;

typedef struct {
    MEA_Bool    valid;
    MEA_IPSec_tunnel_type_t    type;   /* 0-nvgre 1-Ipsec only  2-vxlan  */

    MEA_Uint32  IPSec_src_IP; //relevant IPSec Only
    MEA_Uint32  IPSec_DST_IP;
 

}MEA_EgressHeaderProc_IPSec_dbt;


typedef enum {
    MEA_Tunnel_TYPE_GRE = 0,
    MEA_Tunnel_TYPE_L2TP = 1
}MEA_Tunnel_type_GREL2TP_t;

typedef struct {
    MEA_Bool     valid;
    MEA_Uint8    Tunne_type; /*0 -GRE  1-L2TP   see MEA_Tunnel_type_GREL2TP_t*/

    MEA_MacAddr  Da;
    MEA_MacAddr  Sa;

    MEA_Uint32  src_IPv4;
    MEA_Uint32  dst_IPv4;
    
    MEA_Uint32 TunnelId ;
    MEA_Uint32 SessionId;


}MEA_EgressHeaderProc_GRE_L2TP_dbt;



typedef enum {
    UnTagged = 0,
    CTagged,
    STagged,
    DoubleTagged
} MEA_ETHCS_UL_EditType_t;

typedef struct {
    MEA_Bool                 valid;
    MEA_ETHCS_UL_EditType_t  mode;
    MEA_Uint32               s_vlan;
    MEA_Uint32               c_vlan;
} MEA_EgressHeaderProc_ETHCS_UL_dbt;


typedef struct {
    MEA_Uint8  inner_mapping_enable;
    MEA_Uint8  inner_mapping_id;
    MEA_Uint8  outer_mapping_enable;
    MEA_Uint8  outer_mapping_id;


}mea_editmarking_prof_dbt;

typedef struct {
    MEA_Uint8  inner_stamp_color;
    MEA_Uint8  inner_stamp_priority;
    MEA_Uint8  outer_stamp_color;
    MEA_Uint8  outer_stamp_priority;
}mea_edit_stemping_dbt;

typedef enum{
    
    MEA_MACHINE_PROTO_LLC =0,
    MEA_MACHINE_PROTO_MARTINI = 1,
    MEA_MACHINE_PROTO_OUTER = 2,
    MEA_MACHINE_PROTO_VLAN = 3,
    MEA_MACHINE_PROTO_PROTO = 4,
    MEA_MACHINE_VAL=5,
    MEA_MACHINE_CALC_CHECKSUM=6,
    MEA_MACHINE_PROTO_LAST

}MEA_machin_Proto_t;

typedef enum {
    MEA_EDITINGTYPE_NORMAL = 0,
    MEA_EDITINGTYPE_APPEND = 1,
    MEA_EDITINGTYPE_APPEND_APPEND = 2,
    MEA_EDITINGTYPE_EXTRACT = 3,
    MEA_EDITINGTYPE_EXTRACT_EXTRACT = 4,
    MEA_EDITINGTYPE_SWAP = 5,
    MEA_EDITINGTYPE_SWAP_APPEND = 6,
    MEA_EDITINGTYPE_SWAP_EXTRACT = 7,
    MEA_EDITINGTYPE_SWAP_SWAP = 8,

    MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL = 9,
    MEA_EDITINGTYPE_MPLS_APPEND_MPLS_ETHERTYPE_LABEL = 10,
    MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_LABEL = 11,
    MEA_EDITINGTYPE_MPLS_APPEND_MPLS_ETHERTYPE_LABEL_LABLE = 12,
    MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_LABEL = 13,
    MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_ETHERTYPE_LABEL = 14,
    MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_LABEL_LABEL = 15,
    MEA_EDITINGTYPE_MPLS_EXTRACT_MPLS_ETHERTYPE_LABEL_LABLE = 16,
    MEA_EDITINGTYPE_MPLS_SWAP_MPLS_LABEL = 17,
    MEA_EDITINGTYPE_MPLS_SWAP_MPLS_LABEL_LABEL = 18,


    MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_APPEND_BMAC_BTAG_ITAG = 19,
    MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_EXTRACT_BMAC_BTAG_ITAG = 20,
    MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_SWAP_BMAC_BTAG_ITAG = 21,


    MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_APPEND_BMAC_APPEND_ITAG = 22,
    MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_EXTRACT_BMAC_EXTRACT_ITAG = 23,
    MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_SWAP_BMAC_SWAP_ITAG = 24,
    MEA_EDITINGTYPE_MAC_IN_MAC_NO_BTAG_APPEND_BTAG_SWAP_ITAG = 25,
    MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_SWAP_ISID = 26,
    MEA_EDITINGTYPE_MAC_IN_MAC_BTAG_EXTRACT_BTAG_SWAP_ISID = 27,

    MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_VLAN = 28,
    MEA_EDITINGTYPE_IPCS_DL_Append_IPCS_Extract_Eth_QTAG = 29,

    MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_VLAN = 30,
    MEA_EDITINGTYPE_IPCS_UL_Extract_IPCS_Append_ETH_QTAG = 31,

    /*Append IP_CS,IPoGRE, internal VLAN transparent SM1 & SM1a & SM2 & SM2b  */
    MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent = 32,

    /*Append IP_CS,IPoGRE, Extract internal VLAN SM2a & SM3 & SM5  */
    MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN = 33,

    /*Append IP_CS,IPoGRE, swap external VLAN SM4  */
    MEA_EDITINGTYPE_VPWS_DL_Swap_external_VLAN_TAG = 34,  /*NOT Support*/

    /*Append IP_CS,IPoGRE, Extract external VLAN swap internal VLAN SM5a  */
    MEA_EDITINGTYPE_VPWS_DL_Extract_external_VLAN_Swap_Internal_VLAN = 35, /*NOT Support*/

    /*SM1 & SM1a & SM2 - Extract ETH_CS header ,Transparent  Ethernet packet*/
    MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS = 36,

    /*SM2a & SM3 & SM5 & SM2b  – Extract ETH_CS header ,Append QTAG (4B) Note : in case of SM3 the machine appends VLAN 0x8100 instead of 0x88A8 */
    MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG = 37, /*only one vlan*/

    /*SM4 – Extract ETH_CS header ,swap  external VLAN_TAG (4B)*/
    MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Swap_External_TAG = 38,

    /* SM5a – Extract ETH_CS header ,Append QTAG (4B) and swap internal VLAN (4B)*/
    MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QTAG_Swap_Internal_VLAN = 39,


    /*40	Append MPLS label(+4) + Ethertype(0x8847 / 8)(+2) + Swap CFM ethertype(0x8902)(-2) to G - ACH(MPLS CFM) (0x1000, 0xXXXX)(+4)*/
    MEA_EDITINGTYPE_MPLS_APEND_MPLS_LABEL_SWAP_CFM_ETH_TO_MPLS_CFM = 40,

    /* 41	Append, Append MPLS label(+8) + MPLS Ethertype(0x8847 / 8)(+2) + Swap CFM ethertype(0x8902)(-2) to G - ACH(MPLS CFM) (0x1000, 0xXXXX)(+4)*/
    MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_LABEL_APPEND_ETHTYRTYP_MPLS_SWAP_CFM_ETH_TO_MPLS_CFM = 41,
    /* 42	Extract MPLS(-4) + Extract MPLS Ethertype(0x8847 / 8)(-2) + Swap G - ACH(MPLS CFM) (0x1000, 0xXXXX)(-4) to MPLS CFM Ethertype(0x8902)(+2)*/
    MEA_EDITINGTYPE_MPLS_Extract_MPLS_LABEL_Extract_ETHTYRTYP_MPLS_SWAP_MPLS_CFM_TO_CFM_ETH = 42,
    /* 43	Extract, Extract MPLS label(-8) + Extract MPLS Ethertype(0x8847 / 8)(-2) + Swap G - ACH(MPLS CFM) (0x1000, 0xXXXX)(-4) to MPLS CFM Ethertype(0x8902)(+2)*/
    MEA_EDITINGTYPE_MPLS_Extract_MPLS_LABEL_LABEL_Extract_ETHTYRTYP_MPLS_SWAP_MPLS_CFM_TO_CFM_ETH = 43,

    MEA_EDITINGTYPE_MAC_IN_MAC_NoBTAG_SWAP_ITAG = 44,

    MEA_EDITINGTYPE_MPLS_APPEND_MPLS_LABEL_OFSET12 = 45,

    //MEA_EDITINGTYPE_VPWS_DL_Internal_VLAN_Transparent = 32
    MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Internal_VLAN_Transparent = 46,

    /*Append IP_CS,IPoGRE, Extract internal VLAN SM2a & SM3 & SM5  */
    MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN = 47,

    MEA_EDITINGTYPE_Swap_DA_SA = 48,


    MEA_EDITINGTYPE_VPWS_UL_Extract_ETHCS_Append_QinQ = 49,
    MEA_EDITINGTYPE_VPWS_DL_Extract_Extract_QInQ = 50,
   
    //ETH_CS_DL - Append ETH_CS header(78B) + Extract external VLAN(-4B) + Extract internal VLAN(-4B) + Extract MPLS label and MPLS Ethertype(-6B)
    MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_Extract_QInQ = 51,


    MEA_EDITINGTYPE_IPCS_DL_IPinIP_Append_SGW =52,

    MEA_EDITINGTYPE_IPCS_UL_IPinIP_Extract_SGW = 53,


    MEA_EDITINGTYPE_IPCS_DL_UL_SGW_Swap_header =54,

   /* VxLAN machines VXLAN_header 50B */
   MEA_EDITINGTYPE_VXLAN_header_Append_vlan                     = 55,
   MEA_EDITINGTYPE_VXLAN_header_Append_vlan_Extract_INNER_vlan  = 56,
   MEA_EDITINGTYPE_VXLAN_header                                 = 57,
   MEA_EDITINGTYPE_VXLAN_header_Extract_INNER_vlan              = 58,

   MEA_EDITINGTYPE_NVGRE_Append_Tunnel                      =59,
   MEA_EDITINGTYPE_EA_60 = 60,         // for fragment by HW
   MEA_EDITINGTYPE_EA_61 = 61,        // for fragment by HW


   MEA_EDITINGTYPE_VXLAN_UL_Extract_header                                     = 62,
   MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Append_vlan                         = 63,
   MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Extract_vlan                        = 64,
   MEA_EDITINGTYPE_VXLAN_UL_Extract_header_Extract_OuterVlan_Append_Inner_VLAN = 65,

   MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_VXLAN_header                    = 66,
   MEA_EDITINGTYPE_VXLAN_DL_Extract_MPLS_Append_Extract_Inner_vlan_VXLAN_header = 67,

   
   MEA_EDITINGTYPE_NVGRE_Append_Tunnel_ADD_Outer_vlan  = 68,
   MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE           = 69,
   MEA_EDITINGTYPE_IPSec_Append_Tunnel_NVGRE_ADD_Outer_vlan  = 70,  //NVGRE
   MEA_EDITINGTYPE_IPSec_Append_Tunnel_ONLY            = 71,
   MEA_EDITINGTYPE_IPSec_Append_Tunnel_VxLAN           = 72,

   MEA_EDITINGTYPE_GRE_Append_Tunnel                  = 73,
   MEA_EDITINGTYPE_GRE_Extract_Tunnel                 = 74,
   MEA_EDITINGTYPE_L2TP_Append_Tunnel                 = 75,
   MEA_EDITINGTYPE_L2TP_Extract_Tunnel                = 76,
   MEA_EDITINGTYPE_IPSec_Append_Tunnel_Extract_vlan   = 77,
   MEA_EDITINGTYPE_PPPoE_DL_Append_header_Swap_vlan   = 78,
   MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Swap_vlan  = 79,


  // PPPoE DL - Append PPPoE header (8B) + Extract Vlan (-4B) + (Stamp DA,SA - swap_editing - #1)
  MEA_EDITINGTYPE_PPPoE_DL_Append_header_Extract_valn_Stamp_DA_SA = 80,
  // PPPoE UL - Extract PPPoE header (8B) + Append Vlan (4B)  + (Stamp DA,SA - swap_editing - #1)
  MEA_EDITINGTYPE_PPPoE_UL_Extract_header_Append_vlan_Stamp_DA_SA = 81,


        
    /*Append IP_CS,IPoGRE, swap external VLAN SM4  */
	MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Swap_external_VLAN_TAG = 100, /*TBD*/

	/*Append IP_CS,IPoGRE, Extract external VLAN swap internal VLAN SM5a  */
	MEA_EDITINGTYPE_VPWS_DL_Extract_MPLS_LABEL_Extract_external_VLAN_Swap_Internal_VLAN = 101, /*TBD*/
   

   
    




    MEA_EDITINGTYPE_LAST
}MEA_EditingType_t;



typedef enum 
{

    MEA_EDITING_ENC_DEC_NO         = 0,
    MEA_EDITING_ENC_DEC_Encryption = 1,
    MEA_EDITING_ENC_DEC_Decryption = 2,
    MEA_EDITING_ENC_DEC_NOT_VALID  =3

}MEA_Editing_encryption_decryption_t;











typedef struct {
    MEA_Uint32                               EditingType; /* see MEA_EditingType_t*/
    

    
    MEA_EgressHeaderProc_IPCS_DL_dbt            IPCS_DL_info;
    MEA_EgressHeaderProc_IPCS_UL_dbt            IPCS_UL_info;
    

    MEA_EgressHeaderProc_ETHCS_DL_dbt           ETHCS_DL_info;
    MEA_EgressHeaderProc_ETHCS_UL_dbt           ETHCS_UL_info;
    MEA_EgressHeaderProc_NAT_dbt                NAT_info;

    MEA_EgressHeaderProc_VxLan_DL_dbt           VxLan_info;
    MEA_EgressHeaderProc_NVGRE_dbt              NVGre_info;
    MEA_EgressHeaderProc_IPSec_dbt              IPSec_info;
    MEA_EgressHeaderProc_GRE_L2TP_dbt           GRE_L2TP_info;



    MEA_EgressProce_Protocol_Attribute_dbt   Attribute_info;
    MEA_EgressHeaderProc_Eth_Entry_dbt       eth_info;
    MEA_EgressHeaderProc_EthStampingInfo_dbt eth_stamping_info;
    MEA_EgressHeaderProc_Atm_Entry_dbt       atm_info;      //outer
    MEA_EgressHeaderProc_Wbrg_Entry_dbt      wbrg_info;
    MEA_EgressHeaderProc_MartiniInfo_dbt     martini_info;
    MEA_EgressHeaderProc_LM_CounterId_dbt    LmCounterId_info;
    MEA_EgressHeaderProc_fragmentSession_dbt fragmentSession_info;
    MEA_EgressHeaderProc_pw_control_dbt      pw_control_info;
    MEA_EgressHeaderProc_mpls_label_dbt      mpls_label_info;
    MEA_Editing_encryption_decryption_t      encryp_decryp_type;
    MEA_Uint32                               crypto_ESP_Id;  /*Database of the crypt o Info */

} MEA_EgressHeaderProc_Entry_dbt;

typedef struct {
    MEA_OutPorts_Entry_dbt                   output_info;               
    MEA_EgressHeaderProc_Entry_dbt           ehp_data;
} MEA_EHP_Info_dbt;

typedef struct {
        MEA_Uint32        num_of_entries; 
        MEA_EHP_Info_dbt *ehp_info;
        
} MEA_EgressHeaderProc_Array_Entry_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*             MEA Egress Headers Processor APIs                                 */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Get_EgressHeaderProc_Entry (MEA_Unit_t                     unit,
                                               MEA_Editing_t                  editId,
                                               MEA_EgressHeaderProc_Entry_dbt *entry);

// Note: Stamping is allowed only for APPEND / SWAP command 
MEA_Status MEA_API_Set_EgressHeaderProc_Entry (MEA_Unit_t                     unit,
                                               MEA_EgressHeaderProc_Entry_dbt *entry_i,
                                               MEA_Editing_t                  *id_io,
                                               MEA_Bool                        force_update_i);

MEA_Status MEA_API_Delete_EgressHeaderProc_Entry(MEA_Unit_t                     unit,
                                                 MEA_Editing_t                  editId);


MEA_Status MEA_API_Get_EgressHeaderProc_ProtoEthStampingInfo
                                   (MEA_EgressPort_Proto_t        proto,
                                    MEA_Bool                      isMartini_E_L_LSP,
                                    MEA_EgressHeaderProc_EthStampingInfo_dbt *stamp_info);

MEA_Status MEA_API_Get_EditingFree_id(MEA_Unit_t  unit_i,MEA_Editing_t                  *id_o);

typedef struct {
    MEA_Uint32  usedEdit;
    MEA_Uint32 TotalEdit;

}MEA_EgressHeaderProc_numInfo_dbt;


MEA_Status  MEA_API_Get_NumOfEdit(MEA_Unit_t unit_i, MEA_EgressHeaderProc_numInfo_dbt *Info);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA EditingMappingProfile Defines                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_EDITING_MAPPING_KEY_MAX_NUM_OF_PRIORITY (8)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA EditingMappingProfile Typedefs                         */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
 
typedef struct {
    MEA_Uint8    stamp_priority_value :  3; /* Stamping L2 Priority for Editing */
    MEA_Uint8    stamp_color_value    :  1; /* Stamping L2 Priority for Editing */
    MEA_Uint8    pad                  :  4; /* padding                          */
} MEA_EditingMappingProfile_ColorItem_Entry_dbt;

typedef enum {
    MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN,
    MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW,
    MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_LAST
} MEA_EditingMappingProfile_ColorItem_Id_te;

#define MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_LAST    (2)

typedef struct {
    MEA_EditingMappingProfile_ColorItem_Entry_dbt colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_LAST];
} MEA_EditingMappingProfile_Item_Entry_dbt;

typedef struct {
    MEA_EditingMappingProfile_Item_Entry_dbt item_table [MEA_EDITING_MAPPING_KEY_MAX_NUM_OF_PRIORITY];
    MEA_Uint32                               direction; /* use MEA_DIRECTION_XXX */
} MEA_EditingMappingProfile_Entry_dbt,*MEA_EditingMappingProfile_Entry_dbp;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA EditingMappingProfile APIs                             */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* Note: id_io can be as input specific value or MEA_PLAT_GENERATE_NEW_ID */
MEA_Status MEA_API_Create_EditingMappingProfile_Entry
                          (MEA_Unit_t                           unit_i,
                           MEA_EditingMappingProfile_Entry_dbt *entry_pi,
                           MEA_EditingMappingProfile_Id_t      *id_io);

MEA_Status MEA_API_Delete_EditingMappingProfile_Entry
                          (MEA_Unit_t                           unit_i,
                           MEA_EditingMappingProfile_Id_t       id_i);

MEA_Status MEA_API_Set_EditingMappingProfile_Entry
                          (MEA_Unit_t                           unit_i,
                           MEA_EditingMappingProfile_Id_t       id_i,
                           MEA_EditingMappingProfile_Entry_dbt *entry_pi);

MEA_Status MEA_API_Get_EditingMappingProfile_Entry
                          (MEA_Unit_t                           unit_i,
                           MEA_EditingMappingProfile_Id_t       id_i,
                           MEA_EditingMappingProfile_Entry_dbt *entry_po);

MEA_Status MEA_API_GetFirst_EditingMappingProfile_Entry
                          (MEA_Unit_t                               unit_i,
                           MEA_EditingMappingProfile_Id_t          *id_o,
                           MEA_EditingMappingProfile_Entry_dbt     *entry_po,
                           MEA_Bool                                *found_o);

MEA_Status MEA_API_GetNext_EditingMappingProfile_Entry
                          (MEA_Unit_t                               unit_i,
                           MEA_EditingMappingProfile_Id_t          *id_io,
                           MEA_EditingMappingProfile_Entry_dbt     *entry_po,
                           MEA_Bool                                *found_o);

MEA_Status MEA_API_AddOwner_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t       id_i);

MEA_Status MEA_API_DelOwner_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t       id_i);

MEA_Status MEA_API_GetOwner_EditingMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_EditingMappingProfile_Id_t       id_i,
                                     MEA_Uint32                          *num_of_owners_o);

MEA_Status MEA_Delete_all_editing_mapping(MEA_Unit_t                           unit_i);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Policer APIs                                           */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


MEA_Status MEA_API_Create_Policer_ACM_Profile(MEA_Unit_t                 unit,
                                          MEA_Uint16                    *policer_id,
                                          MEA_AcmMode_t             ACM_Mode,
                                          MEA_IngressPort_Proto_t        port_proto_prof_i,
                                          MEA_Policer_Entry_dbt         *Entry_i );

MEA_Status MEA_API_Set_Policer_ACM_Profile(MEA_Unit_t                 unit,
                                       MEA_Uint16                     policer_id,
                                       MEA_AcmMode_t             ACM_Mode,
                                       MEA_IngressPort_Proto_t        port_proto_prof_i,
                                       MEA_Policer_Entry_dbt         *Entry_i );

MEA_Status MEA_API_Delete_Policer_ACM_Profile(MEA_Unit_t              unit,
                                          MEA_Uint16                 policer_id,
                                          MEA_AcmMode_t          ACM_Mode);

MEA_Status MEA_API_Get_Policer_ACM_Profile(MEA_Unit_t                  unit,
                                       MEA_Uint16                      policer_id,
                                       MEA_AcmMode_t              ACM_Mode,
                                       MEA_Policer_Entry_dbt          *Entry_o );


MEA_Status MEA_API_GetFirst_Policer_ACM_Profile(MEA_Unit_t              unit,
                                           MEA_Uint16                  *policer_id,
                                           MEA_AcmMode_t           ACM_Mode,
                                           MEA_Policer_Entry_dbt       *Entry_o,
                                           MEA_Bool                    *found_o);

MEA_Status MEA_API_GetNext_Policer_ACM_Profile(MEA_Unit_t                unit,
                                                MEA_Uint16              *policer_id,
                                                MEA_AcmMode_t       ACM_Mode,
                                                MEA_Policer_Entry_dbt   *Entry_o,
                                                MEA_Bool                *found_o);




MEA_Status mea_drv_Policer_Profile_IsExist(MEA_Unit_t                 unit_i,
                                           MEA_AcmMode_t      ACM_Mode_i,
                                           MEA_Policer_prof_t         id_i,
                                           MEA_Bool                  *exist_o);


MEA_Status MEA_API_Set_TM_ID_Info(MEA_Unit_t                 unit,
                                  MEA_TmId_t                 tm_id,
                                  MEA_Policer_prof_t        *prof_id_io,
                                  MEA_Policer_Entry_dbt      *Policer_Entry_i );



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA PM APIs                                                */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_IsExist_PmId(MEA_Unit_t                 unit,
                                MEA_PmId_t                 pmId,
                                MEA_Bool                  *exist,
                                MEA_Uint32                *count);



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA action Table Defines                                   */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define MEA_ACTION_ID_TO_CPU 0


#define MEA_ACTION_FORCE_COMMAND_DISABLE  0 
#define MEA_ACTION_FORCE_COMMAND_VALUE    1 
#define MEA_ACTION_FORCE_COMMAND_MAX      2 

/* Protocol definition: Eth Port -> Eth Port */
#define MEA_ACTION_PROTOCOL_ETH_TO_ETH_TRANS     (0)
#define MEA_ACTION_PROTOCOL_ETH_TO_ETH_VLAN      (1)
#define MEA_ACTION_PROTOCOL_ETH_TO_ETH_NAT       (2)
#define MEA_ACTION_PROTOCOL_ETH_TO_ETH_MART      (3)

/* Protocol definition: ATM Port -> Eth Port */
#define MEA_ACTION_PROTOCOL_ATM_EoA_TO_ETH       (0)
#define MEA_ACTION_PROTOCOL_ATM_IPoA_TO_ETH_IPoA (1)
#define MEA_ACTION_PROTOCOL_ATM_PPPoEoA_TO_ETH   (2)
#define MEA_ACTION_PROTOCOL_ATM_PPPoA_TO_ETH     (3)

/* Protocol definition: Eth Port -> ATM Port */
#define MEA_ACTION_PROTOCOL_ETH_TO_ATM_EoA       (0)
#define MEA_ACTION_PROTOCOL_ETH_TO_ATM_IPoA      (1)
#define MEA_ACTION_PROTOCOL_ETH_TO_ATM_PPPoEoA   (2)
#define MEA_ACTION_PROTOCOL_ETH_TO_ATM_PPPoA     (3)

#define MEA_DIRECTION_GENERAL    (0) 
#define MEA_DIRECTION_UPSTREAM   (1) 
#define MEA_DIRECTION_DOWNSTREAM (2) 


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA action Table Typedefs                                  */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

typedef enum {
    MEA_PM_TYPE_ALL                      = 0,
   /* MEA_PM_TYPE_PACKET_ONLY              = 1,*/
    MEA_PM_TYPE_LAST                     = 1
} MEA_pm_type_te;

typedef enum {
    MEA_INGGRESS_TS_TYPE_NONE=0,
    MEA_INGGRESS_TS_TYPE_ENABLE=1,
    MEA_INGGRESS_TS_TYPE_LAST
}MEA_Ingress_TS_type_t;

typedef struct {    
    MEA_Uint32  output_ports_valid  : 1;
    MEA_Uint32  pm_id_valid         : 1;
    MEA_Uint32  tm_id_valid         : 1;
    MEA_Uint32  tmId_disable        : 1;  /*disable cir eir */
    MEA_Uint32  ed_id_valid         : 1;
    MEA_Uint32  force_cos_valid     : 2;  /* see MEA_ACTION_FORCE_COMMAND_XXX defines */
    MEA_Uint32  COS                 : 3;  
    MEA_Uint32  force_l2_pri_valid  : 2;  /* see MEA_ACTION_FORCE_COMMAND_XXX defines */
    MEA_Uint32  L2_PRI              : 3;
    MEA_Uint32  force_color_valid   : 2;  /* see MEA_ACTION_FORCE_COMMAND_XXX defines */
    MEA_Uint32  COLOR               : 2;
    MEA_Uint32  proto_llc_valid     : 1;
    MEA_Uint32  protocol_llc_force  : 1;
    MEA_Uint32  Protocol            : 2; /* use MEA_ACTION_PROTOCOL_XXX defines */
    MEA_Uint32  Llc                 : 1; /* 0=no LLC / 1=exist LLC - relevant to ATM */
    MEA_Uint32  direction           : 2; /* use MEA_DIRECTION_XXX defines */
    MEA_Uint32  opposite_direction_actionId_valid : 1;
    MEA_Uint32  pm_type             : 1; /* use MEA_pm_type_te */
    MEA_Uint32  wred_profId         : 3;
    MEA_Uint32  set_1588            : 1;
    
    /************************************************************************/
    MEA_Uint32  flowCoSMappingProfile_force     : 1;
    MEA_Uint32  flowCoSMappingProfile_id        : 6;
    MEA_Uint32  flowMarkingMappingProfile_force : 1;
    MEA_Uint32  flowMarkingMappingProfile_id    : 6;
    MEA_Uint32  fwd_ingress_TS_type             : 1; /* see MEA_Ingress_TS_type_t  note: relevant only at forwarder with Key type 4*/
    MEA_Uint32  fragment_enable                 : 1;
    MEA_Uint32  afdx_enable                     : 1; // This block relevant only if AFDX support  
    MEA_Uint32  afdx_Rx_sessionId               :16; //
    MEA_Uint32  afdx_A_B                        : 1; //



    MEA_Uint32  afdx_Bag_info                   ; 
#ifdef MEA_TDM_SW_SUPPORT 
    /* parameters for  TDM*/
    MEA_Uint32 tdm_packet_type                  :1; /* 1- eth to TDM*/
    MEA_Uint32 tdm_remove_byte                  :4;
    MEA_Uint32 tdm_remove_line                  :3;
    MEA_Uint32 tdm_ces_type                     :2;  /* 0 -UDP 1 -vlan 2-mpls 3-IP*/
    MEA_Uint32 tdm_used_vlan                    :1;
    MEA_Uint32 tdm_used_rtp                     :1;
    MEA_Uint32 tdm_cesId                        :16; /* relevant only if tdm_packet_type = TRUE*/
    MEA_Uint32 pad2                             : 4;
#endif
/************************************************************************/
   MEA_Uint32    lag_bypass                     : 1;
   MEA_Uint32    flood_ED                       : 1;
   MEA_Uint32    Drop_en                        : 1;
   
   MEA_Uint32   overRule_vpValid                : 1;
   MEA_Uint32   overRule_vp_val                 :10;

   MEA_Uint32    policer_prof_id_valid          : 1;

   MEA_Uint32 no_allow_del_Act                : 1;
   MEA_Uint32 pad3                           : 16;
    
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
   MEA_Uint32 Fragment_IP                  : 1;
   MEA_Uint32 IP_TUNNEL                    : 1;

   MEA_Uint32 Defrag_ext_IP                : 1;
   MEA_Uint32 Defrag_int_IP                : 1;
   MEA_Uint32 Defrag_Reassembly_L2_profile : 3;
   MEA_Uint32 fragment_ext_int             : 1;
   MEA_Uint32 fragment_target_mtu_prof     : 2;
   MEA_Uint32 fwd_win                      : 1;
   MEA_Uint32 sflow_Type                   : 2;
   MEA_Uint32 overRule_bfd_sp              : 1;
    
    
    MEA_Action_t    opposite_direction_actionId;
    MEA_PmId_t      pm_id;
    MEA_TmId_t      tm_id;
    MEA_Editing_t   ed_id;
    MEA_Uint16      mtu_Id;
    MEA_Policer_prof_t policer_prof_id;

	mea_action_type_te Action_type;

   
    

} MEA_Action_Entry_Data_dbt;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA action Table APIs                                     */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

/* Note: To generate new id the Action_Id_io should be set (before call create) 
         to MEA_GENERATE_NEW_VALUE */
MEA_Status MEA_API_Create_Action (MEA_Unit_t                              unit_i,
                                   MEA_Action_Entry_Data_dbt             *Action_Data_pio,
                                   MEA_OutPorts_Entry_dbt                *Action_OutPorts_pi,
                                   MEA_Policer_Entry_dbt                 *Action_Policer_pi,
                                   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_pi,
                                   MEA_Action_t                          *Action_Id_io);


MEA_Status  MEA_API_Set_Action    (MEA_Unit_t                             unit_i,
                                   MEA_Action_t                           Action_Id_i,
                                   MEA_Action_Entry_Data_dbt             *Action_Data_pio,
                                   MEA_OutPorts_Entry_dbt                *Action_OutPorts_pi,
                                   MEA_Policer_Entry_dbt                 *Action_Policer_pi,
                                   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_pi);

MEA_Status  MEA_API_Get_Action    (MEA_Unit_t                             unit_i,
                                   MEA_Action_t                           Action_Id_i,
                                   MEA_Action_Entry_Data_dbt             *Action_Data_po,
                                   MEA_OutPorts_Entry_dbt                *Action_OutPorts_po,
                                   MEA_Policer_Entry_dbt                 *Action_Policer_po,
                                   MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing_po);


MEA_Status  MEA_API_Delete_Action (MEA_Unit_t                            unit_i,
                                   MEA_Action_t                          Action_Id_i);


MEA_Status MEA_API_DeleteAll_Action(MEA_Unit_t unit_i);


MEA_Status MEA_API_GetFirst_Action (MEA_Unit_t                           unit_i,
                                    MEA_Action_t                        *Action_Id_o , 
                                    MEA_Bool                            *found_o,
									mea_action_type_te Action_type);

MEA_Status MEA_API_GetNext_Action  (MEA_Unit_t                           unit_i,
                                    MEA_Action_t                        *Action_Id_io, 
                                    MEA_Bool                            *found_o,
									mea_action_type_te Action_type);


MEA_Status MEA_API_IsExist_Action  (MEA_Unit_t                            unit_i,
                                    MEA_Action_t                          Action_Id_i, 
                                    MEA_Bool                             *found_o);

MEA_Status MEA_API_IsExist_Action_ByType(MEA_Unit_t     unit_i,
	MEA_Action_t   ActionId_i,
	mea_action_type_te Action_type,
	MEA_Bool      *exist_o);

MEA_Bool MEA_API_IsValid_Action_Id (MEA_Unit_t                            unit_i, 
                                    MEA_Action_t                          Action_Id_i,
									mea_action_type_te Action_type);

typedef struct {
    MEA_Uint32  usedAction;
    MEA_Uint32 TotalActionType;

}MEA_Action_numInfo_dbt;


MEA_Status MEA_API_Get_NumOf_Action(MEA_Unit_t unit_i, mea_action_type_te Action_type, MEA_Action_numInfo_dbt *Info);


MEA_Status  MEA_API_Delete_Action_HPM(MEA_Unit_t    unit_i, MEA_Action_t  ActionId_i, mea_action_type_te Action_type);
MEA_Status MEA_API_DeleteAll_Action_HPM(MEA_Unit_t unit_i);



/************************* INDEX-  section x.  CFM classification   ***************/

typedef struct {
              MEA_Uint32   valid  :1;
              MEA_Uint32   pad    :31;
              MEA_MacAddr  my_Da; 

}MEA_CFM_OAM_MyMAC_dbt;

MEA_Status MEA_CFM_OAM_Create_MyMAC (MEA_Unit_t unit_i,MEA_CFM_OAM_MyMAC_dbt entry , MEA_Uint16 *Id);

MEA_Status MEA_CFM_OAM_Set_MyMAC(MEA_Unit_t unit_i, MEA_Uint16 Id,  MEA_CFM_OAM_MyMAC_dbt entry );

MEA_Status MEA_CFM_OAM_Delete_MyMAC(MEA_Unit_t unit_i,MEA_Uint16 Id);

MEA_Bool MEA_CFM_OAM_MyMAC_ID_Valid(MEA_Unit_t unit_i, MEA_Uint16 Id);

MEA_Status MEA_CFM_OAM_Get_MyMAC(MEA_Unit_t unit_i, MEA_Uint16 Id,  MEA_CFM_OAM_MyMAC_dbt *entry );



/*********************************************************************************/


/************************************************************************/
/* MTU flow                                                         */
/************************************************************************/
typedef struct {
    
    MEA_Uint32  mtu_size;
}MEA_AC_Mtu_dbt;


MEA_Status MEA_API_Set_Flow_MTU_Prof(MEA_Unit_t                     unit_i,
                                    MEA_Uint16                      id_i,
                                    MEA_AC_Mtu_dbt                  *entry_pi);

MEA_Status MEA_API_Get_Flow_MTU_Prof(MEA_Unit_t                     unit_i,
                                    MEA_Uint16                      id_i,
                                    MEA_AC_Mtu_dbt                  *entry_po,
                                    MEA_Bool                        *exist);
MEA_Status MEA_API_Flow_MTU_Prof_GetFirst(MEA_Unit_t                      unit_i,
                                         MEA_Uint16                      *id_o,
                                         MEA_AC_Mtu_dbt                  *entry_po, /* Can't be NULL */
                                         MEA_Bool                        *found_o);

MEA_Status MEA_API_Flow_MTU_Prof_GetNext(MEA_Unit_t                unit_i,
                                         MEA_Uint16                        *id_io,
                                         MEA_AC_Mtu_dbt                  *entry_po, /* Can't be NULL */
                                         MEA_Bool                          *found_o);


/************************************************************************/
/* Target flow                                                             */
/************************************************************************/
MEA_Status MEA_API_Set_Target_MTU_Prof(MEA_Unit_t                     unit_i,
    MEA_Uint16                      id_i,
    MEA_AC_Mtu_dbt                  *entry_pi);

MEA_Status MEA_API_Get_Target_MTU_Prof(MEA_Unit_t                     unit_i,
    MEA_Uint16                      id_i,
    MEA_AC_Mtu_dbt                  *entry_po,
    MEA_Bool                        *exist);
MEA_Status MEA_API_Target_MTU_Prof_GetFirst(MEA_Unit_t                      unit_i,
    MEA_Uint16                      *id_o,
    MEA_AC_Mtu_dbt                  *entry_po, /* Can't be NULL */
    MEA_Bool                        *found_o);

MEA_Status MEA_API_Target_MTU_Prof_GetNext(MEA_Unit_t                unit_i,
    MEA_Uint16                        *id_io,
    MEA_AC_Mtu_dbt                  *entry_po, /* Can't be NULL */
    MEA_Bool                          *found_o);



/**------------------------------------------------------------------------------**/

/************************* INDEX-  section x.  CFM  classification ***************/

typedef struct {
    MEA_Uint32   valid                  :1; 
    MEA_Uint32   my_me_level_bit        :8; /*each bit is set enable me_level*/  
    MEA_Uint32   my_Da_mac_prof_Id      :6; /*profile of MAC */
    MEA_Uint32   MC_MAC_MAK_enabel      :1;
    MEA_Uint32   ac_th_below            :1;
    MEA_Uint32   ac_th_above            :1; 
    MEA_Uint32   cfm_me_level_TH_value  :3;


} MEA_CFM_OAM_Classifier_Data_dbt;


MEA_Status MEA_CFM_OAM_Create_Data (MEA_Unit_t unit_i,MEA_CFM_OAM_Classifier_Data_dbt *entry , MEA_Uint16 *Cfm_Id);

MEA_Status MEA_CFM_OAM_Set_Data(MEA_Unit_t unit_i, MEA_Uint16 Cfm_Id,  MEA_CFM_OAM_Classifier_Data_dbt *entry );

MEA_Status MEA_CFM_OAM_Delete_Data(MEA_Unit_t unit_i,MEA_Uint16 Cfm_Id);

MEA_Bool MEA_CFM_OAM_Data_ID_Valid(MEA_Unit_t unit_i, MEA_Uint16 Cfm_Id);

MEA_Status MEA_CFM_OAM_Get_Data(MEA_Unit_t unit_i, MEA_Uint16 Cfm_Id,  MEA_CFM_OAM_Classifier_Data_dbt *entry );





/*********************************************************************************/


/************************* INDEX-  section 3.3 classification    ******************/
/*                                                                               */
/*                                                                               */
/*    3.3 classification                                                          */
/*             3.3.1 service - classifier                   (basic)               */
/*             3.3.2 filter                                (advanced - option)   */
/*             3.3.3 Search Engine (SE)                                          */
/*             3.3.3.1 Data Search Engine(and switching)                         */
/*             3.3.3.2 Limiter                             (advanced - option)   */
/*             3.3.4 mapping                               (advanced - option)   */
/*                                                                               */
/*********************************************************************************/


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Service Table Typedefs                                 */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef enum {
    MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN = 0,
    MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN = 1,
#ifdef PPP_MAC 
    MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID = 2,
#else
	MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN = 2,
#endif   
	MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN = 3,
    MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN = 4,
    MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN = 5,
    MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN = 6,
    MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN = 7,
    MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN = 16,
    MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN=17,



    MEA_SE_FORWARDING_KEY_TYPE_LAST = 18
} MEA_SE_Forwarding_key_type_te;

typedef enum {
    MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN = 0,

    MEA_SE_LEARNING_KEY_TYPE_MPLS_VPN = 1,

    MEA_SE_LEARNING_KEY_TYPE_DA_PLUS_PPPOE_SESSION_ID,
    MEA_SE_LEARNING_KEY_TYPE_VLAN_VPN,
    MEA_SE_LEARNING_KEY_TYPE_OAM_VPN,
    MEA_SE_LEARNING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN,
    MEA_SE_LEARNING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN,
    MEA_SE_LEARNING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN,
    MEA_SE_LEARNING_KEY_TYPE_LAST
} MEA_SE_Learning_key_type_te;


typedef enum {
    MEA_PACKET_COMMAND_TRANSPARENT=0,
    MEA_PACKET_COMMAND_ADD_CTAG=1,
    MEA_PACKET_COMMAND_EXTRACT_CTAG=2,
    MEA_PACKET_COMMAND_SWAP_CTAG=3,
    MEA_PACKET_COMMAND_ADD_CTAG_CTAG=4,
    MEA_PACKET_COMMAND_EXTRACT_CTAG_CTAG=5,
    MEA_PACKET_COMMAND_SWAP_CTAG_CTAG=6,
    MEA_PACKET_COMMAND_ADD_CTAG_SWAP_CTAG=7,
    



    MEA_PACKET_COMMAND_LAST
} MEA_PacketCommand_te;





typedef struct {
    
    /*--------------------------------------------------------------------------------------- -*/
    /*   63   61    60          51    47              31              15              0        */
    /*   |____|_____|___________|_____|_______________|_______________|_______________|        */
    /*   |_2b_|__1b_|___9b/7b___|__4b_|______16b______|______16b______|______16b______|        */
    /*   |____|_____|___________|_____|_______________|_______________|_______________|        */
    /*   | -- | fIV | SID/src_p | key |   field III   |   field II    |   field I     |        */
    /*                                                                                         */
    /*                                                                                         */
    /*                                                                                         */
    /*                 L.S.B.                          M.S.B.                                  */
    /*    _______________^_______________ ______________^_______________                       */
    /*   |_______________|_______________|__|___|___|___|_______________|                      */
    /*   |   field II    |   field I     |--|fVI|S/s|key|   field III   |                      */
    /*                                                                                         */
    /*                                                                                         */
    /*   TIPI di FILTRI:                                                                       */
    /*                                                                                         */
    /*   |                filter_mask.mask_32_63             |     filter_mask.mask_0_31     | */
    /*   |                                                   |                               | */
    /*   |63_________________________________________________|31____________________________0| */
    /*   |   |   |           |               |               |               |               | */
    /*   |2 b|1 b|  9/7 bit  |     4 bit     |    16 bit     |     16 bit    |    16 bit     | */
    /*   |___|___|___________|_______________|_______________|_______________|_______________| */
    /*   |   |   |           |               |               |               |               | */
    /*   | - |fIV| SID/SRC p | Key /ACL type |   field III   |   field II    |   field I     | */
    /*   |___|___|___________|_______________|_______________|_______________|_______________| */
    /*   |   |   |           |               |                                               | */
    /*   | 0 | - |           |       0       |           DESTINATION MAC ADDRESS             | */
    /*   |___|___|___________|_______________|_______________________________________________| */
    /*   |   |   |           |               |                                               | */
    /*   | 0 | - |           |       1       |              SOURCE MAC ADDRESS               | */
    /*   |___|___|___________|_______________|_______________________________________________| */
    /*   |   |   |           |               |                               |               | */
    /*   | 0 | - |           |       2       |  Dest_IPv4/32 LSB Dest_IPv6   | Dst TCP/UDP p | */
    /*   |___|___|___________|_______________|_______________________________|_______________| */
    /*   |   |   |           |               |                               |               | */
    /*   | 0 | - |           |       3       |   Src_IPv4/32 LSB Src_IPv6    | Dst TCP/UDP p | */
    /*   |___|___|___________|_______________|_______________________________|_______________| */
    /*   |   |   |           |               |                               |               | */
    /*   | 0 | - |           |       4       |  Dest_IPv4/32 LSB Dest_IPv6   | Src TCP/UDP p | */
    /*   |___|___|___________|_______________|_______________________________|_______________| */
    /*   |   |   |           |               |                               |               | */
    /*   | 0 | - |           |       5       |   Src_IPv4/32 LSB Src_IPv6    | Src TCP/UDP p | */
    /*   |___|___|___________|_______________|_______________________________|_______________| */
    /*   |   |   |           |               |               |               |Outer_VLAN     | */
    /*   | 0 | - |           |       6       |  Outer_ETYPE  |  Inner_ETYPE  |pr3b,c1b,ID12b | */
    /*   |___|___|___________|_______________|_______________|_______________|_______________| */
    /*   |   |   |           |               |                               |               | */
    /*   | 0 | - |           |       7       |   Src_IPv4/32 LSB Src_IPv6    | Applic_ETYPE  | */
    /*   |___|___|___________|_______________|_______________________________|_______________| */
    /*   |   |   |           |               | L3/2 |IP (7b) |               |               | */
    /*   | 0 | - |           |       8       | pri  |protocol| Applic_ETYPE  | Dst TCP/UDP p | */
    /*   |___|___|___________|_______________|______|________|_______________|_______________| */
    /*   |   |   |           |               | L3/2 |IP (7b) |               |               | */
    /*   | 0 | - |           |       9       | pri  |protocol| Src TCP/UDP p | Dst TCP/UDP p | */
    /*   |___|___|___________|_______________|______|________|_______________|_______________| */
    /*   |   |   |           |               |Outer_VLAN     |Inner_VLAN     |               | */
    /*   | 0 | - |           |      10       |pr3b,c1b,ID12b |pr3b,c1b,ID12b |  Outer_ETYPE  | */
    /*   |___|___|___________|_______________|_______________|_______________|_______________| */
    /*   |   |   |           |               |Outer_VLAN     |               |               | */
    /*   | 0 | - |           |      11       |pr3b,c1b,ID12b | Dst TCP/UDP p | Src TCP/UDP p | */
    /*   |___|___|___________|_______________|_______________|_______________|_______________| */
    /*   |   |   |           |               |Inner_VLAN     |               |               | */
    /*   | 0 | - |           |      12       |pr3b,c1b,ID12b | Dst TCP/UDP p | Src TCP/UDP p | */
    /*   |___|___|___________|_______________|_______________|_______________|_______________| */
    /*   |   |   |           |               | PPPoE         | PPPoE         |               | */
    /*   | 0 | - |           |      13       |   session ID  |  protocol ID  |Src MAC(LSB-16)| */
    /*   |___|___|___________|_______________|_______________|_______________|_______________| */
    /*   |   |   |           |               |               |               |Outer_VLAN     | */
    /*   | 0 | - |           |      14       |  Outer_ETYPE  | Applic_ETYPE  |pr3b,c1b,ID12b | */
    /*   |___|___|___________|_______________|_______________|_______________|_______________| */
    /*   |   |   |           |               |                               |Outer_VLAN     | */
    /*   | 0 | # |           |      15       |  Dest_IPv4/32 LSB Dest_IPv6   |pr3b,c1b,ID12b | */
    /*   |___|___|___________|_______________|_______________________________|_______________| */
    /*         ^                                                                               */
    /*         |___ 1b match MAC_IP_SWAP B-DA(48 b) External DA MAC address                    */
    /*                                                                                         */

    MEA_Uint32 mask_0_31;
    MEA_Uint32 mask_32_63;   
} MEA_Filter_mask_dbt;


typedef enum {
    MEA_DSE_MASK_IP_32 = 0,
    MEA_DSE_MASK_IP_24 = 1,
    MEA_DSE_MASK_IP_16 = 2,
    MEA_DSE_MASK_IP_8  = 3
}MEA_DSE_MASK_Field_Ipv4_Type;

typedef enum {
    MEA_DSE_MASK_NONE = 0,
    MEA_DSE_MASK_INTERNAL_MPLS = 1,
    MEA_DSE_MASK_EXTERNAL_MPLS = 2,
    MEA_DSE_MASK_TBD = 3
}MEA_DSE_MASK_Field_MPLS_Type;

typedef enum {
    MEA_DSE_MASK_CFMOAM_NONE = 0,
    MEA_DSE_MASK_CFMOAM_MPLS_VLAN = 1,
    MEA_DSE_MASK_CFMOAM_TBD = 2,
    MEA_DSE_MASK_CFMOAM_TBD1 = 3
}MEA_DSE_MASK_Field_CFMOAM_Type; /*Key 4 */

typedef enum {
    MEA_POLICER_COLOR_VAL_YELLOW=0,
    MEA_POLICER_COLOR_VAL_RED,
    MEA_POLICER_COLOR_VAL_GREEN,
    MEA_POLICER_COLOR_VAL_LAST
} MEA_Policer_Color_val_te;

typedef enum {
    MEA_FILTER_KEY_TYPE_DA_MAC=0,
    MEA_FILTER_KEY_TYPE_SA_MAC=1,
    MEA_FILTER_KEY_TYPE_DST_IPV4_DST_PORT=2,
    MEA_FILTER_KEY_TYPE_SRC_IPV4_DST_PORT=3,
    MEA_FILTER_KEY_TYPE_DST_IPV4_SRC_PORT=4,
    MEA_FILTER_KEY_TYPE_SRC_IPV4_SRC_PORT=5,    
    MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG=6,
    MEA_FILTER_KEY_TYPE_SRC_IPV4_APPL_ETHERTYPE=7,
    MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT=8,
    MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_SRC_PORT_DST_PORT=9,
    MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_INNER_VLAN_TAG_OUTER_ETHERTYPE=10,
    MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_DST_PORT_SRC_PORT=11,
    MEA_FILTER_KEY_TYPE_INNER_VLAN_TAG_DST_PORT_SRC_PORT=12,
    MEA_FILTER_KEY_TYPE_PPPOE_SESSION_ID_PPP_PROTO_SA_MAC_LSB=13,
    MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_APPL_ETHERTYPE_OUTER_VLAN_TAG=14,
    MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4 = 15,


    MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT = 22,
    MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT = 23,
    MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT = 24,
    MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT = 25,
    MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE = 27,

    MEA_FILTER_KEY_TYPE_LAST=30
} MEA_Filter_Key_Type_te;



typedef struct{
    MEA_Bool               valid;
    MEA_Filter_Key_Type_te filter_key_type;
    MEA_Filter_mask_dbt    mask;
}MEA_ACL_mask_dbt;

typedef struct{
    
    MEA_Uint32           filter_mode_OR_AND_type :2;   /*  0 - All OR
                                                           1 -Two AND  else is OR 
                                                           2 -Three AND else is OR 
                                                           3 - All AND     */
    MEA_Uint32         filter_Whitelist_enable :1;
//    MEA_Uint32         filter_mode_src_sid     :1;    /* 0-port 1-sid */
    MEA_Uint32         filter_pad              : 28;
    MEA_ACL_mask_dbt data_info[MEA_ACL_HIERARC_NUM_OF_FLOW];

   

}MEA_ACL_prof_Key_mask_dbt;





#define  MEA_PARSING_ATM_PROTOCOL_EoLLCoA         17
#define  MEA_PARSING_ATM_PROTOCOL_EoVCMUXoA       18
#define  MEA_PARSING_ATM_PROTOCOL_IPoLLCoA        19
#define  MEA_PARSING_ATM_PROTOCOL_IPoVCMUXoA      20
#define  MEA_PARSING_ATM_PROTOCOL_PPPoLLCoA       21
#define  MEA_PARSING_ATM_PROTOCOL_PPPoVCMUXoA     22
#define  MEA_PARSING_ATM_PROTOCOL_VoEoLLCoA       23
#define  MEA_PARSING_ATM_PROTOCOL_VoEoVCMUXoA     24
#define  MEA_PARSING_ATM_PROTOCOL_PPPoEoA         25 
#define  MEA_PARSING_ATM_PROTOCOL_PPPoEVCMUXoA    26 





typedef enum {
    MEA_PARSING_L2_Sub_Untagged_protocol_NONE=0,
    MEA_PARSING_L2_Sub_Untagged_protocol_AFDX=1,
    MEA_PARSING_L2_Sub_Untagged_protocol_NETWARE=2,
    MEA_PARSING_L2_Sub_Untagged_protocol_SNAP=3,
    MEA_PARSING_L2_Sub_Untagged_protocol_LLC=4,
    MEA_PARSING_L2_Sub_Untagged_protocol_GRE = 8,
    MEA_PARSING_L2_Sub_Untagged_protocol_L2TPoE_DP=11,
    MEA_PARSING_L2_Sub_Untagged_protocol_vxlan=14,
    MEA_PARSING_L2_Sub_Untagged_protocol_LAST
}MEA_PARSING_L2_Sub_Untagged_protocol_t;

typedef enum {

    MEA_PARSING_L2_Sub_DSA_TAG_protocol_PriTag_src0=0,
    MEA_PARSING_L2_Sub_DSA_TAG_protocol_PriTag_src1=1,
    MEA_PARSING_L2_Sub_DSA_TAG_protocol_VLAN_src0=2,
    MEA_PARSING_L2_Sub_DSA_TAG_protocol_VLAN_src1=3,
    MEA_PARSING_L2_Sub_DSA_TAG_protocol_MNG_src0=4,
    MEA_PARSING_L2_Sub_DSA_TAG_protocol_MNG_src1=5,
    MEA_PARSING_L2_Sub_DSA_TAG_protocol_2MNG=6,

    MEA_PARSING_L2_Sub_DSA_TAG_protocol_LAST
}MEA_PARSING_L2_Sub_DSA_TAG_protocol_t;

typedef enum {
    MEA_PARSING_L2_Sub_MPLS_OUTER_NONE = 0,
    MEA_PARSING_L2_Sub_MPLS_OUTER_VLAN = 1,
    MEA_PARSING_L2_Sub_MPLS_OUTER_TWO_VLAN = 2,
    MEA_PARSING_L2_Sub_MPLS_OUTER_LAST
}MEA_PARSING_L2_Sub_MPLS_OUTER_protocol_t;




typedef enum {
    MEA_PARSING_L2_Sub_Btag_ON = 0,
    MEA_PARSING_L2_Sub_Btag_OFF = 1,

    MEA_PARSING_L2_Sub_Btag_LAST
}MEA_PARSING_L2_Sub_Btag_protocol_t;

typedef enum {
    MEA_PARSING_L2_Sub_Ctag_protocol_NONE = 0,
    MEA_PARSING_L2_Sub_Ctag_protocol_IPinIP_CS_DL_IPv4 = 1, /*filter #10 - IPinIP_CS_DL_v4 */
    MEA_PARSING_L2_Sub_Ctag_protocol_DL_VPWS_IPV6 = 2, /*filter #11 - IPinIP_CS_DL_v6 */
    MEA_PARSING_L2_Sub_Ctag_protocol_IP_CS_DL = 3,
    MEA_PARSING_L2_Sub_Ctag_protocol_IPCS_IPV6_DL = 4,
    MEA_PARSING_L2_Sub_Ctag_protocol_IPCS_IPV4_IPV6_UL = 5,
    MEA_PARSING_L2_Sub_Ctag_protocol_UL_VPWS  = 6,
    MEA_PARSING_L2_Sub_Ctag_protocol_IP_CS_UL = 7,
    MEA_PARSING_L2_Sub_Ctag_protocol_GRE = 8,
    MEA_PARSING_L2_Sub_Ctag_protocol_NVGRE = 9,
    
    MEA_PARSING_L2_Sub_Ctag_protocol_L2TP = 11,
    MEA_PARSING_L2_Sub_Ctag_protocol_vxlan=14,
    MEA_PARSING_L2_Sub_Ctag_protocol_L3 = 16,
    MEA_PARSING_L2_Sub_Ctag_IPSEC_NVGRE_ENCRYPT=19,
    MEA_PARSING_L2_Sub_Ctag_protocol_LAST=31

}MEA_PARSING_L2_Sub_Ctag_protocol_t;


typedef enum {
    MEA_PARSING_L2_Sub_GW_protocol_NONE = 0,
    MEA_PARSING_L2_Sub_GW_protocol_IPinIP_CS_DL_IPv4 = 1, /*filter #10 - IPinIP_CS_DL_v4 */
    MEA_PARSING_L2_Sub_GW_protocol_DL_VPWS_IPV6 = 2, /*filter #11 - IPinIP_CS_DL_v6 */
    MEA_PARSING_L2_Sub_GW_protocol_IP_CS_DL = 3,
    MEA_PARSING_L2_Sub_GW_protocol_IPCS_IPV6_DL = 4,
    MEA_PARSING_L2_Sub_GW_protocol_IPCS_IPV4_IPV6_UL = 5,
    MEA_PARSING_L2_Sub_GW_protocol_UL_VPWS = 6,
    MEA_PARSING_L2_Sub_GW_protocol_IP_CS_UL = 7,
    MEA_PARSING_L2_Sub_GW_protocol_ETHCS_INNER_DEFRAG = 8,
    MEA_PARSING_L2_Sub_GW_protocol_ETHCS_OUTER_DEFRAG = 9,
    MEA_PARSING_L2_Sub_GW_protocol_IP_CS_UL_SGW = 11,
}MEA_PARSING_L2_Sub_GW_protocol_t;

typedef enum {
    MEA_PARSING_L2_KEY_Ctag_Ctag_SubType_NVGRE =9, 
    MEA_PARSING_L2_KEY_Ctag_Ctag_SubType_LAST=31
}MEA_PARSING_L2_Sub_Ctag_Ctag_protocol_t;

typedef enum {
    
    MEA_PARSING_L2_KEY_Untagged=0,
    MEA_PARSING_L2_KEY_Ctag=1,
    MEA_PARSING_L2_KEY_Ctag_Ctag=2,
    MEA_PARSING_L2_KEY_Stag=3,
    MEA_PARSING_L2_KEY_Stag_Ctag=4,
    MEA_PARSING_L2_KEY_Stag_Stag=5,
    MEA_PARSING_L2_KEY_Stag_Stag_Ctag=6,
    MEA_PARSING_L2_KEY_Btag_Itag=7,
    MEA_PARSING_L2_KEY_Btag_Itag_Ctag=8,
    MEA_PARSING_L2_KEY_Btag_Itag_Stag=9,
    MEA_PARSING_L2_KEY_Btag_Itag_Stag_Ctag=10,
    MEA_PARSING_L2_KEY_GW=11,
    MEA_PARSING_L2_KEY_DSA_TAG=12,
    MEA_PARSING_L2_KEY_RESERV13 =13,
    MEA_PARSING_L2_KEY_RESERV14=14,
    MEA_PARSING_L2_KEY_RESERV15 =15,                           
    MEA_PARSING_L2_KEY_MPLS_Single_label_no_tag=16,           
    MEA_PARSING_L2_KEY_MPLS_Two_label_no_tag=17,              // if the ATM support we used  this protocol below
    MEA_PARSING_L2_KEY_MPLS_Three_label_no_tag=18,            //MEA_PARSING_ATM_PROTOCOL_EoLLCoA     
    MEA_PARSING_L2_KEY_MPLS_Four_labelno_tag=19,              //MEA_PARSING_ATM_PROTOCOL_EoVCMUXoA   
    MEA_PARSING_L2_KEY_MPLS_N_labels_no_tag=20,               //MEA_PARSING_ATM_PROTOCOL_IPoLLCoA    
    MEA_PARSING_L2_KEY_MPLS_Single_label_with_tag=21,         //MEA_PARSING_ATM_PROTOCOL_IPoVCMUXoA  
    MEA_PARSING_L2_KEY_MPLS_Two_label_with_tag=22,            //MEA_PARSING_ATM_PROTOCOL_PPPoLLCoA   
    MEA_PARSING_L2_KEY_MPLS_Three_label_with_tag=23,          //MEA_PARSING_ATM_PROTOCOL_PPPoVCMUXoA 
    MEA_PARSING_L2_KEY_Reserv24=24,                           //MEA_PARSING_ATM_PROTOCOL_VoEoLLCoA   
    MEA_PARSING_L2_KEY_MPLS_N_labels_with_tag=25,             //MEA_PARSING_ATM_PROTOCOL_VoEoVCMUXoA 
    MEA_PARSING_L2_KEY_MPLS_Single_label_with_two_tag=26,     //MEA_PARSING_ATM_PROTOCOL_PPPoEoA     
    MEA_PARSING_L2_KEY_MPLS_Two_label_with_two_tag=27,        //MEA_PARSING_ATM_PROTOCOL_PPPoEVCMUXoA
    
    MEA_PARSING_L2_KEY_MPLS_N_label_with_two_tag=28,
    MEA_PARSING_L2_KEY_TDM=29,
    MEA_PARSING_L2_KEY_MPLS_not_over_ethernet=30,
    MEA_PARSING_L2_KEY_Transparent=31,
    MEA_PARSING_L2_KEY_DEF_SID=32,
    MEA_PARSING_L2_KEY_LAST
}MEA_PARSING_L2_KEY_t;

typedef enum {
    MEA_PARSING_L3_KEY_1588_event_over_UDP_IPv4=0,
    MEA_PARSING_L3_KEY_UDP_IPv4_wo_options=1,
    MEA_PARSING_L3_KEY_TCP_IPv4_wo_options=2,
    MEA_PARSING_L3_KEY_Default_IPv4_wo_options=3,

    MEA_PARSING_L3_KEY_UDP_IPv4_with_options=4,
    MEA_PARSING_L3_KEY_TCP_IPv4_with_options=5,
    MEA_PARSING_L3_KEY_Default_IPv4_with_options=6,

    MEA_PARSING_L3_KEY_1588_event_over_UDP_IPv6=7,
    MEA_PARSING_L3_KEY_UDP_IPv6=8,
    MEA_PARSING_L3_KEY_TCP_IPv6=9,
    MEA_PARSING_L3_KEY_Default_IPv6=10,

    MEA_PARSING_L3_KEY_1588_event_message_over_ethernet=11,

    MEA_PARSING_L3_KEY_IPv4_PPPoE=12,
    MEA_PARSING_L3_KEY_IPv6_PPPoE=13,
    MEA_PARSING_L3_KEY_PPPoE_default=14,
    MEA_PARSING_L3_KEY_CFM=15,
    MEA_PARSING_L3_KEY_Default_ethernet=16,
    MEA_PARSING_L3_KEY_1588_event_over_UDP_IPv4_MPLS=17,
    MEA_PARSING_L3_KEY_UDP_IPv4_MPLS_wo_options=18,
    MEA_PARSING_L3_KEY_TCP_IPv4_mpls_wo_options=19,
    MEA_PARSING_L3_KEY_Default_IPv4_mpls_wo_options=20,
    MEA_PARSING_L3_KEY_UDP_IPv4_MPLS_with_options=21,
    MEA_PARSING_L3_KEY_TCP_IPv4_mpls_with_options=22,
    MEA_PARSING_L3_KEY_Default_IPv4_mpls_with_options=23,
    MEA_PARSING_L3_KEY_1588_event_over_UDP_IPv6_mpls=24,
    MEA_PARSING_L3_KEY_UDP_IPv6_mpls=25,
    MEA_PARSING_L3_KEY_Default_IPv6_mpls=26,
    MEA_PARSING_L3_KEY_Pseudowire=27,
    MEA_PARSING_L3_KEY_ACH=28,
    MEA_PARSING_L3_KEY_Default_MPLS=29,
    MEA_PARSING_L3_KEY_802_1_TDM_IP=30,
    MEA_PARSING_L3_KEY_802_1_TDM_UDP=31,
    MEA_PARSING_L3_KEY_LAST
}MEA_PARSING_L3_KEY_t;

typedef  struct{
    MEA_Uint32 pri_0_31;        // bit fields
    MEA_Uint32 pri_32_63;

} MEA_pri_mask_dbt;



typedef enum {
    MEA_AFDX_RX_TYPE_0=0,   /*32000pps*/
    MEA_AFDX_RX_TYPE_1=1,   /*16000pps*/
    MEA_AFDX_RX_TYPE_2=2,   /* 8000pps*/
    MEA_AFDX_RX_TYPE_3=3,   /* 4000pps*/
    MEA_AFDX_RX_TYPE_4=4,   /* 2000pps*/
    MEA_AFDX_RX_TYPE_5=5,   /* 1000pps*/
    MEA_AFDX_RX_TYPE_6=6,   /*  500pps*/
    MEA_AFDX_RX_TYPE_7=7,   /*  256pps*/
    MEA_AFDX_RX_TYPE_8=8,   /*  128pps*/
    MEA_AFDX_RX_TYPE_9=9,   /*   64pps*/
    MEA_AFDX_RX_TYPE_A=10,  /*   32pps*/
    MEA_AFDX_RX_TYPE_B=11,  /*   16pps*/
    MEA_AFDX_RX_TYPE_C=12,  /*    8pps*/
    MEA_AFDX_RX_TYPE_D=13,  /*    4pps*/
    MEA_AFDX_RX_TYPE_E=14,   /*   2pps*/
    MEA_AFDX_RX_TYPE_F=15,  /*    1pps*/
    MEA_AFDX_RX_TYPE_LAST
}MEA_Afdx_Rx_Type_t;


typedef enum {
    MEA_MSTP_TYPE_STATE_FORWARD = 0,
    MEA_MSTP_TYPE_STATE_BLOCKING = 1,
    MEA_MSTP_TYPE_STATE_LEARNING = 2,
    MEA_MSTP_TYPE_STATE_LISENING = 3,
    MEA_MSTP_TYPE_STATE_BLOCKING_SRV = 4,
    MEA_MSTP_TYPE_STATE_BLOCKING_SRV_VPN0=5,
    MEA_MSTP_TYPE_STATE_LAST

}MEA_MSTP_State_t;

typedef struct {
    MEA_Uint32 info;
}MEA_MSTP_Data_dbt;

typedef struct {
    MEA_MSTP_State_t state;
    MEA_MSTP_Data_dbt data;

}MEA_service_MSTP_State_dbt;

typedef enum {
    MEA_SRV_INNER_MAC_LEARN_SA_NONE_FWR_DA_NONE = 0,
    MEA_SRV_INNER_MAC_LEARN_SA_NONE_FWR_DA_ON   = 1,
    MEA_SRV_INNER_MAC_LEARN_SA_ON_FWR_DA_NONE   = 2,
    MEA_SRV_INNER_MAC_LEARN_SA_ON_FWR_DA_ON     = 3
}MEA_service_Learn_fwd_inner_t;

typedef enum {
    MEA_ClassifierKEY_DEFUALT = 0,
    MEA_ClassifierKEY_IPinIP_CS_DL_IPv4 = 1,
    MEA_ClassifierKEY_DL_VPWS_IPV6 = 2,
    MEA_ClassifierKEY_IP_CS_DL = 3,
    MEA_ClassifierKEY_DL_IPCS_IPV6 = 4,
    MEA_ClassifierKEY_UL_IPCS_IPV4_IPV6 = 5,
    MEA_ClassifierKEY_Eth_CS_Tag_UL = 6,
    MEA_ClassifierKEY_IP_CS_UL = 7,
    MEA_ClassifierKEY_ETHCS_INNER_DEFRAG = 8,
    MEA_ClassifierKEY_ETHCS_OUTER_DEFRAG = 9,
    MEA_ClassifierKEY_IP_CS_UL_SGW = 11,
    MEA_ClassifierKEY_Eth_CS_UnTag_UL = 16,
    MEA_ClassifierKEY_IPSEC_NVGRE_ENCRYPT=19,
    MEA_ClassifierKEY_REANGE = 30,

}MEA_serviceKeyType_t;






typedef struct{
	MEA_serviceKeyType_t ClassifierKEY    : 5;

	MEA_PARSING_L2_KEY_t L2_protocol_type : 6;
    MEA_Uint32 sub_protocol_type          : 5; /* See type hw 4bit*/
    MEA_Uint32 src_port                   : 16;
    MEA_Uint32 virtual_port               : 1;
   
    MEA_Uint32 direction                  :  2; /* use MEA_DIRECTION_XXX defines */
    MEA_Uint32 def_sid_valid              :  1;
    MEA_Uint32 evc_enable                 :  1;
    MEA_Uint32 external_internal          :  1;  /* only if the evc_enable */
    



    MEA_Uint32 net_tag                    : 32;    

    MEA_Uint32 net_tag_to_valid           : 1;
    MEA_Uint32 net_tag_to_value           : 32;    
    
   
    MEA_Uint32 priType                    : 2; /* 00 -Non IP 1-IPv4 precedence 2-IPv4 ToS 3-IPv4 DSCP*/
    MEA_Uint32 pri                        : 6;
    MEA_Uint32 pri_to_valid               : 1;  /*Not support */
    MEA_Uint32 pri_to_value               : 6;  /*Not support */
    
    
    MEA_Uint32 inner_netTag_from_valid    :  1;
    MEA_Uint32 inner_netTag_from_value    : 32;      


    

    MEA_Uint32 inner_netTag_to_valid      :  1;
    MEA_Uint32 inner_netTag_to_value      : 32;      

#ifdef MEA_IPV6_CLASS_SUPPORT
    MEA_Uint32 IPV6_msb_valid                : 1;
    MEA_Uint64 IPV6_msb;
#endif

#ifdef PRI_GROUP 
    MEA_Uint32        pri_group_mask_enable      : 1; /* not support*/
   MEA_pri_mask_dbt  pri_group_mask;                /* not support*/
#endif    
} MEA_Service_Entry_Key_dbt;


typedef enum 
{
    MEA_SRV_MODE_NORMAL   = 0,
    MEA_SRV_MODE_REGULAR  = 1,  /*internal*/
    MEA_SRV_MODE_EXTERNAL = 2,
    MEA_SRV_MODE_LAST
}MEA_SRV_mode_t;




/* Note 1:  ( for notation convenient we use the word resource for PMid or TMid and the word object for Service or forwarder entry)
   
   On Create  you may use value '0' in case you want to use a new resource from pool 
                   or use any allocated index to reuse a resource allocated for another object.
   On Set if you modify the resource parameter for all the owner user the resource ID , else use '0'  */
#define SE_Action_Profile
typedef struct{
    MEA_Uint32 valid                            :  1;
	MEA_Uint32 no_allow_del                     :  1;
    MEA_Uint32 CM                               :  1;
    MEA_Uint32 ADM_ENA                          :  1;




/***********SRV Action paramters **************************/
#if 0    

    MEA_Action_Entry_Data_dbt action_params;
#else
    MEA_Editing_t editId;
    MEA_PmId_t    pmId;
    MEA_TmId_t    tmId;
    MEA_Uint16    mtu_Id;


    MEA_Policer_prof_t policer_prof_id;

    MEA_Uint32 protocol_llc_force : 1;
    MEA_Uint32 Protocol           : 2; /* use MEA_ACTION_PROTOCOL_XXX defines */
    MEA_Uint32 Llc                : 1; /* 0=no LLC / 1=exist LLC - relevant to ATM */

    MEA_Uint32 pm_type            : 1; /* use MEA_pm_type_te */
    MEA_Uint32 tmId_disable       : 1;


    MEA_Uint32 COS                              :  3;
    MEA_Uint32 COS_FORCE                        :  2; /* see MEA_ACTION_FORCE_COMMAND */
    MEA_Uint32 COLOR                            :  2;
    MEA_Uint32 COLOR_FORSE                      :  2; /* see MEA_ACTION_FORCE_COMMAND */
    MEA_Uint32 L2_PRI                           :  3;
    MEA_Uint32 L2_PRI_FORCE                     :  2; /* see MEA_ACTION_FORCE_COMMAND */
    MEA_Uint32 flowCoSMappingProfile_force      :  1;
    MEA_Uint32 flowCoSMappingProfile_id         :  6;
    MEA_Uint32 flowMarkingMappingProfile_force  :  1;
    MEA_Uint32 flowMarkingMappingProfile_id     :  6;


    MEA_Uint32 fragment_enable                  : 1; /*disable(0)     / enable (1) */
    MEA_Uint32 wred_profId                      : 3;
    MEA_Uint32 ingress_TS_type                  : 1;
    MEA_Uint32 reservb5                         : 1;
    MEA_Uint32 set_1588                         : 1;
    MEA_Uint32 lag_bypass                       : 1;
    MEA_Uint32 flood_ED                         : 1;
    MEA_Uint32 Drop_en                          : 1;

    MEA_Uint32   overRule_vpValid               : 1;
    MEA_Uint32   overRule_vp_val                : 10;



    MEA_Uint32 afdx_enable                      : 1;
    MEA_Uint32 afdx_Rx_sessionId                : 16;
    MEA_Uint32 afdx_A_B                         : 1;
    MEA_Uint32 afdx_Bag_info                    : 4;/* see MEA_Afdx_Rx_Type_t */
    MEA_Uint32 afdx_pad                         : 10;


    MEA_Uint32 Fragment_IP                      : 1; /*enable disable Fragment*/
    MEA_Uint32 IP_TUNNEL                        : 1; /*enable disable IP_TUNNEL*/

    MEA_Uint32 Defrag_ext_IP                    : 1;
    MEA_Uint32 Defrag_int_IP                    : 1;
    MEA_Uint32 Defrag_Reassembly_L2_profile     : 3;

    MEA_Uint32  fragment_ext_int                : 1; /*external =1  internal=0*/
    MEA_Uint32  fragment_target_mtu_prof        : 2;

    MEA_Uint32 fwd_win                          : 1;
    MEA_Uint32 sflow_Type                       : 2;  /* 0-disable 1-ingress_sflow 2-cyber_sFlow 3-sflowNocount     */
    MEA_Uint32 overRule_bfd_sp                  : 1;

#ifdef MEA_TDM_SW_SUPPORT
    MEA_Uint32 tdm_packet_type  : 1; /* 1- eth to TDM*/
    MEA_Uint32 tdm_remove_byte  : 4;
    MEA_Uint32 tdm_remove_line  : 3;
    MEA_Uint32 tdm_ces_type     : 2;  /* 0 -UDP 1 -vlan 2-mpls 3-IP*/
    MEA_Uint32 tdm_used_vlan    : 1;
    MEA_Uint32 tdm_used_rtp     : 1;
    MEA_Uint32 tdm_cesId        : 16; /* relevant only if tdm_packet_type = TRUE*/
    MEA_Uint32 pad_4            : 4;
#endif


#endif

    /********End ACtion************************************************************/

    MEA_Uint32 DSE_forwarding_enable            :  1;
    MEA_Uint32 DSE_forwarding_key_type          :  5; /* MEA_SE_Forwarding_key_type_te */
    MEA_Uint32 DSE_mask_field_type              :  2;     /* see  MEA_DSE_MASK_Field_ */
    MEA_Uint32 DSE_learning_enable              :  1; 
    MEA_Uint32 DSE_learning_key_type            :  5; /* MEA_SE_Learning_key_type_te */

    MEA_Uint32 DSE_learning_actionId_valid      :  1; /* 0-not valid , 1-valid */
    MEA_Action_t DSE_learning_actionId; /* Learning Action ID for the SE  */

    MEA_Uint32 DSE_learning_update_allow_enable      :  1; /* Allow to update entry in the SE while learning process */
    MEA_Uint32 DSE_learning_update_allow_send_to_cpu : 1; /* In case we need to update SE while learning then send the packet to cpu */
    
    MEA_Uint32    limiter_enable             :  1;
    MEA_Limiter_t limiter_id;

    MEA_Uint32 UC_NotAllowed                :  1; /* NotAllowed (1) / Allowed (0) */
    MEA_Uint32 MC_NotAllowed                :  1; /* NotAllowed (1) / Allowed (0) */
    MEA_Uint32 BC_NotAllowed                :  1; /* NotAllowed (1) / Allowed (0) */
    MEA_Uint32 vpn                          : 12;

    MEA_Uint32 DSE_learning_srcPort_force   :  1; /* 0-learn normal srcPort , 1=force learning srcPort */
    MEA_Uint32 DSE_learning_srcPort         : 10; /* force value for learning cluster */ 
    MEA_Uint32 DSE_learning_Vp_force        : 1;
    
    
    


        
    MEA_Uint32 CFM_OAM_ME_Level_Threshold_valid : 1;
    MEA_Uint32 CFM_OAM_ME_Level_Threshold_value : 4;
    MEA_Uint32 stp                              : 1; /*disable(0)     / enable (1) stp*/ 
    MEA_Uint32 do_sa_search_only                : 1; /* add  to fifo */
    MEA_Uint32 learn_fwd_inner_type             : 2;  /*MEA_service_Learn_fwd_inner_t*/
    
    /************************************************************************/
    /*                                                                      */
    /************************************************************************/                                                  
      


    MEA_Uint32 cfm_oam_profId                   : 8;
    MEA_Uint32 L3_L4_fwd_enable                 : 1;   /*router enable with myMac*/
    MEA_Uint32 L4port_mask_enable               : 1;  /*mask the l4Port  */
    MEA_Uint32 ppp_fwd                          : 1;  /**/
    MEA_Uint32 IP_MC_Aware                      : 1;
    
    MEA_Uint32 Access_port_en     : 1;
    MEA_Uint32 Access_port_id     :10;            
    MEA_Uint32 Access_port_adm_en : 1;
    MEA_Uint32 Access_local_sw_en : 1;


    /**********VPLSi DL **************/
    MEA_Uint32        en_vpls_Ins : 1;
    MEA_Uint32        vpls_Ins_Id : 8;
    /********************************/
    
    /*--------------------------------*/
    /*                                */
    /*--------------------------------*/
    MEA_Uint32 Uepdn_valid     :1;
    MEA_Uint32 Uepdn_id;            
    
    MEA_Uint32 HPM_valid       :1;
    MEA_Uint32 HPM_profileId   :6;
    MEA_Uint32 HPM_prof_mask   :10; /**/

    MEA_Uint32 fwd_select_key : 1;  /*0- select key 6  1- select key 7*/
    MEA_Uint32 segment_prof   : 2;  /*0 regular (1,2,3 profile) */
    MEA_Uint32 HPM_ipv6_key        : 2;  /* 0 - DST-IPv6  1 - SRC-IPv6  2 DST IPv6 & SRC IPv6    */
   /***************************************/
   /*                                      */
   /***************************************/
   
    

    
    
    
    MEA_service_MSTP_State_dbt MSTP_Info;
    
    /**********************************************************************************/
    /* Filter Setting                                                                 */
    /* filter_enable  MEA_TRUE/MEA_FALSE                                              */
    /* ACL_filter_info  set the mask type of ACL works THE HIERARC KEY                */
    /**********************************************************************************/
    MEA_Bool filter_enable;
    MEA_ACL_prof_Key_mask_dbt ACL_filter_info;
    MEA_Bool   ACL_Prof_valid;              /*0-will take from source port   1- from profile*/
    MEA_Uint16 ACL_Prof;
    /**********************************************************************************/

	
    MEA_Uint32  Acl5_mask_prof;

	MEA_Uint32  lpm_vrf  :8; 

    MEA_Uint32    LxCp_enable : 1; /* disable(0)     / enable (1) */
    MEA_LxCp_t    LxCp_Id;
    
    MEA_Uint16 Ingress_flow_policer_ProfileId;
    

    MEA_Uint16            FWD_DaMAC_valid;
    MEA_MacAddr           FWD_DaMAC_value;
    MEA_Uint16            FWD_DaMAC_ID;

    
    
    
    
    
    /**********************************************/
    MEA_SRV_mode_t SRV_mode     ;   /*0 normal 1  internal  2-external   */

    /*********************************************/



/*** Save the hw info for warm reset only ****/

    MEA_Uint16 save_hw_LearnxPermision;
    MEA_Uint16 save_hw_xPermision; /*out flooding*/


} MEA_Service_Entry_Data_dbt;


typedef struct{
    MEA_Uint32 SRV_mode : 2;
    MEA_Uint32 pad         : 30;

    MEA_Bool eb_Sid_Internal;
    MEA_Uint32 Sid_Internal;

    MEA_Bool eb_Sid_External;
    MEA_Uint32 Sid_External;

    MEA_Uint32 hashAdress;


}MEA_Service_Info_dbt;


typedef struct {
    MEA_Service_Entry_Key_dbt             *key;
    MEA_Service_Entry_Data_dbt            *data;
    MEA_OutPorts_Entry_dbt                *OutPorts_Entry;
    MEA_Policer_Entry_dbt                 *Policer_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt   *EHP_Entry;
}MEA_Service_Config_entry_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Service Table APIs                                     */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

/* Note : Only key and data and Policer_entry are mandatory */
MEA_Status MEA_API_Create_Service (MEA_Unit_t                           unit,
                                   MEA_Service_Entry_Key_dbt           *key,
                                   MEA_Service_Entry_Data_dbt          *data,
                                   MEA_OutPorts_Entry_dbt              *OutPorts_Entry,
                                   MEA_Policer_Entry_dbt                 *Policer_entry,
                                   MEA_EgressHeaderProc_Array_Entry_dbt   *EHP_Entry,
                                   MEA_Service_t                       *o_serviceId);

/* Note : Only ServiceId is mandatory */
MEA_Status  MEA_API_Set_Service    (MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Policer_Entry_dbt          *Policer_entry,
                                    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry);

/* Note : Only ServiceId is mandatory */
MEA_Status  MEA_API_Get_Service    (MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId,
                                    MEA_Service_Entry_Key_dbt      *key,
                                    MEA_Service_Entry_Data_dbt     *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Policer_Entry_dbt          *Policer_entry,
                                    MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry);


MEA_Status  MEA_API_Get_Service_InfoDb(MEA_Unit_t                      unit,
    MEA_Service_t                   serviceId,
    MEA_Service_Info_dbt      *entry);


MEA_Status  MEA_API_Delete_Service (MEA_Unit_t                      unit,
                                    MEA_Service_t                   serviceId);

MEA_Status MEA_API_Delete_all_services(MEA_Unit_t                      unit);

MEA_Uint32 MEA_API_Get_NumOf_Service(MEA_Unit_t unit_i, MEA_Uint8 serviceType);


/* Note : o_cookie will be zero if no services */
MEA_Status MEA_API_GetFirst_Service (MEA_Unit_t     unit,
                                     MEA_Service_t *o_serviceId , 
                                     MEA_ULong_t    *o_cookie);

/* Note : io_cookie will be zero if no more services */
MEA_Status MEA_API_GetNext_Service  (MEA_Unit_t     unit,
                                     MEA_Service_t *o_serviceId , 
                                     MEA_ULong_t    *io_cookie);

MEA_Status MEA_API_IsExist_Service_ById  (MEA_Unit_t     unit,
                                          MEA_Service_t  serviceId, 
                                          MEA_Bool      *exist);


/* Note: only key and exist are mandatory */
MEA_Status MEA_API_IsExist_Service_ByKey (MEA_Unit_t                 unit,
                                          MEA_Service_Entry_Key_dbt *key,
                                          MEA_Bool                  *exist,
                                          MEA_Service_t             *serviceId);

MEA_Status MEA_API_IsExist_Service_BySRV_TYPE(MEA_Unit_t     unit_i,
    MEA_Service_t  serviceId_i,
    MEA_SRV_mode_t SRV_mode,
    MEA_Bool      *exist_o);

MEA_Bool MEA_API_IsServiceIdInRange(MEA_Unit_t     unit, 
                                    MEA_Service_t  serviceId);

#define MEA_CHECK_SERVICE_IN_RANGE(serviceId) \
    if(!MEA_API_IsServiceIdInRange(MEA_UNIT_0,serviceId)){ \
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - Service Id %d is out of range\n", \
                          __FUNCTION__,serviceId); \
        return MEA_ERROR;    \
    }                    \

MEA_Bool MEA_API_IsOutPortOf_Service(MEA_Unit_t    unit,
                                     MEA_Service_t serviceId,
                                     MEA_Port_t    outPort);



MEA_Status MEA_API_Service_Set_MSTP_State(MEA_Unit_t unit, MEA_Service_t serviceId, MEA_MSTP_State_t mstp_state);
MEA_Status MEA_API_Service_Set_LXCP(MEA_Unit_t unit, MEA_Service_t serviceId, MEA_Bool enable,MEA_Uint16 LXId );

MEA_Uint16 MEA_API_Get_Num_OutPorts(MEA_Unit_t     unit_i, MEA_Service_t  serviceId_i);



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Filter Table Defines                                   */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Filter Table Typedefs                                  */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* Note 1:  ( for notation convinces we use the word resource for PMid or TMid and the word object for Service, filter, or forwarder entry)
   
   On Create  you may use value '0' in case you want to use a new resource from pool 
                   or use any allocated index to reuse a resource allocated for another object.
   On Set if you modify the resource parameter for all the owner user the resource ID , else use '0'  */



typedef enum {
    MEA_FILTER_L3_TYPE_IPV4_TOS,
    MEA_FILTER_L3_TYPE_IPV4_PRECEDENCE,
    MEA_FILTER_L3_TYPE_IPV4_DSCP,
    MEA_FILTER_L3_TYPE_IPV6_TC,
    MEA_FILTER_L3_TYPE_LAST
} MEA_Filter_L3_Pri_Type_te;

typedef union  {
    struct {
        MEA_Uint32   tos             : 4;
        MEA_Uint32   pad0           : 28;
        } ipv4_tos;
    struct {
        MEA_Uint32   precedence     : 3;
        MEA_Uint32   pad0           : 29;
        } ipv4_precedence;
    struct {
        MEA_Uint32   dscp           : 6;
        MEA_Uint32   pad0           : 26;
        } ipv4_dscp;
    struct {
        MEA_Uint32   tc               : 8;
        MEA_Uint32   pad0           : 24;
        } ipv6_tc;
} MEA_Filter_l3_priority_val_dbt;

typedef struct {
    MEA_Uint32                    ethertype_valid         :  1;
    MEA_Uint32                    priority_valid          :  1;
    MEA_Uint32                  cf_valid                :  1;
    MEA_Uint32                  vlan_valid              :  1;
    MEA_Uint32                    pad                     : 28;

    MEA_Uint32                    ethertype               : 16;
    MEA_Uint32                    priority                :  3;
    MEA_Uint32                  cf                      :  1;
    MEA_Uint32                  vlan                    : 12;
} MEA_Filter_Key_VlanTag_dbt;

typedef struct {
    MEA_Uint32 Acl_hierarchical_id :2;/*0:3*/
    MEA_Uint32 pad                 : 30;

    struct {
        MEA_Uint32                 src_port_valid           : 1;
        MEA_Uint32                 sid_valid                : 1;
        MEA_Uint32                 pad0                     : 30;
        MEA_Port_t                 src_port;
        MEA_Service_t              acl_prof;
       
        
    } interface_key;

    struct {
        struct {
            MEA_Uint32                    DA_valid :  1;
            MEA_Uint32                    SA_valid :  1;
            MEA_Uint32                  pad0     : 30;
            MEA_MacAddr                    DA;   
            MEA_MacAddr                    SA;
        } ethernet;

        MEA_Filter_Key_VlanTag_dbt outer_vlanTag;

        MEA_Filter_Key_VlanTag_dbt inner_vlanTag;


        struct {
            MEA_Uint32                    mpls_label_valid        : 1;
            MEA_Uint32                    mpls_label              : 20;
            MEA_Uint32                  pad                     : 11;
        }mpls;

        struct {
            MEA_Uint32                    pppoe_session_id_valid  : 1;
            MEA_Uint32                    ppp_protocol_valid      : 1;
            MEA_Uint32                    pppoe_session_id_from   : 16;
            MEA_Uint32                    pad                     : 14;
            MEA_Uint32                    ppp_protocol            : 16;
            MEA_Uint32                    pad1                    : 16;
        }ppp;
    } layer2;

    struct {
        MEA_Uint32                        ethertype3_valid         : 1;
        MEA_Uint32                         ip_version_valid        : 1;
        MEA_Uint32                         ip_protocol_valid       : 1;
        MEA_Uint32                         l3_pri_valid            : 1;
        MEA_Uint32                       ethertype3               : 16;
        MEA_Uint32                         pad2                   : 12;
        MEA_Uint32                         ip_version             : 4;
        MEA_Uint32                         ip_protocol            : 8;                           
        MEA_Uint32                         pad3                      : 20;
        MEA_Filter_L3_Pri_Type_te       l3_pri_type;
        MEA_Filter_l3_priority_val_dbt  l3_pri_value;

        struct {
            MEA_Uint32                         dst_ip_valid            : 1;
            MEA_Uint32                         src_ip_valid            : 1;
            MEA_Uint32                      pad                     :30;
            MEA_Uint32                        dst_ip;
            MEA_Uint32                        src_ip;
        } IPv4;

        struct {
            MEA_Uint32                      dst_ipv6_valid : 1;
            MEA_Uint32                      src_ipv6_valid : 1;
            MEA_Uint32                      pad : 30;
            MEA_Uint32                      dst_ipv6[4];
            MEA_Uint32                      src_ipv6[4];
        } IPv6;

    } layer3;

    struct {
        MEA_Uint32                 dst_port_valid             : 1;
        MEA_Uint32                 src_port_valid             : 1;
        MEA_Uint32                 pad4                      : 30;
        MEA_Uint32                 dst_port;
        MEA_Uint32                 src_port;        
    } layer4;        

} MEA_Filter_Key_dbt;

typedef enum {
    MEA_FILTER_ACTION_DISCARD,
    MEA_FILTER_ACTION_SEND_TO_CPU,
    MEA_FILTER_ACTION_TO_ACTION,
    MEA_FILTER_ACTION_TO_PEER,
    MEA_FILTER_ACTION_LAST
} MEA_Filter_Action_te;


typedef struct {

    MEA_Filter_Action_te action_type;

    /* use this parameter in case the action is TO_ACTION */
    MEA_Action_Entry_Data_dbt action_params;

    MEA_Bool      force_Action_en;
    MEA_Action_t  Action_Id;


    /*context*/
    MEA_Uint32 fwd_Act_en               : 1;
    MEA_Uint32 lxcp_win                 : 1;
    MEA_Uint32 hpm_cos                  : 3;
    MEA_Uint32 hpm_cos_valid            : 1;
    
    MEA_Uint32 fwd_enable               : 1;
    MEA_Uint32 fwd_enable_Internal_mac  : 1;
    MEA_Uint32 fwd_Key                  : 3;
    MEA_Uint32 force_fwd_rule           : 1;
    MEA_Uint32 DSE_mask_field_type      : 2;
    MEA_Uint32 pad1                     : 23;

    /**/
/*** Save the hw info for warm reset only ****/
    
    MEA_Uint16 save_hw_xPermissionId;
MEA_Uint16 padx;

} MEA_Filter_Data_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Filter Table APIs                                      */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* Note : Only keys and data and Policer_entry are mandatory */
/*        key_to parameter MUST be NULL
           
*/
MEA_Status MEA_API_Create_Filter (MEA_Unit_t                           unit,
                                  MEA_Filter_Key_dbt                   *key_from,
                                  MEA_Filter_Key_dbt                   *key_to,
                                  MEA_Filter_Data_dbt                  *data,
                                  MEA_OutPorts_Entry_dbt               *OutPorts_Entry,
                                  MEA_Policer_Entry_dbt                *Policer_entry,
                                  MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry,
                                  MEA_Filter_t                         *o_filterId);

/* Note : Only filterId is mandatory */
MEA_Status  MEA_API_Set_Filter    (MEA_Unit_t                      unit,
                                   MEA_Filter_t                    filterId,
                                   MEA_Filter_Data_dbt            *data,
                                   MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                   MEA_Policer_Entry_dbt          *Policer_entry,
                                   MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry);

/* Note : Only filterId is mandatory */
/* Note : key_to parameter MUST be NULL*/

MEA_Status  MEA_API_Get_Filter     (MEA_Unit_t                      unit,
                                    MEA_Filter_t                    filterId,
                                    MEA_Filter_Key_dbt             *key_from,
                                    MEA_Filter_Key_dbt             *key_to,
                                    MEA_Filter_Data_dbt               *data,
                                    MEA_OutPorts_Entry_dbt         *OutPorts_Entry,
                                    MEA_Policer_Entry_dbt          *Policer_entry,
                                    MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry);

MEA_Status MEA_API_Get_Filter_KeyType(MEA_Unit_t unit_i,
                                      MEA_Filter_Key_dbt *key_i,
                                      MEA_Filter_Key_Type_te *keyType_o);

MEA_Status  MEA_API_Delete_Filter (MEA_Unit_t                      unit,
                                   MEA_Filter_t                    filterId);

MEA_Status MEA_API_Delete_all_filters(MEA_Unit_t unit);


MEA_Status MEA_API_GetFirst_Filter (MEA_Unit_t     unit,
                                    MEA_Filter_t *o_filterId,
                                    MEA_Bool     *o_found);

MEA_Status MEA_API_GetNext_Filter  (MEA_Unit_t     unit,
                                    MEA_Filter_t *io_filterId , 
                                    MEA_Bool     *o_found);


MEA_Status MEA_API_IsExist_Filter_ById  (MEA_Unit_t     unit,
                                          MEA_Filter_t  filterId, 
                                          MEA_Bool      *exist);


/* Note: only keys and exist are mandatory */
/* Note : key_to parameter MUST be NULL*/
MEA_Status MEA_API_IsExist_Filter_ByKey (MEA_Unit_t                 unit,
                                          MEA_Filter_Key_dbt       *key_from,
                                          MEA_Filter_Key_dbt       *key_to,
                                          MEA_Bool                  *exist,
                                          MEA_Filter_t             *filterId);


MEA_Bool MEA_API_IsFilterIdInRange(MEA_Unit_t     unit, 
                                    MEA_Filter_t  filterId);

#define MEA_CHECK_FILTER_IN_RANGE(filterId) \
    if(!MEA_API_IsFilterIdInRange(MEA_UNIT_0,filterId)){ \
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - Filter Id %d is out of range\n", \
                          __FUNCTION__,filterId); \
        return MEA_ERROR;    \
    }                    \

/************************************************************************/
/* default Filter rule                                                        */
/************************************************************************/
MEA_Status MEA_API_ACL_Set_Default_rule(MEA_Unit_t unit, MEA_Uint32 filterId);

/*-------------------------------------------------------------------------------*/
/*      Filter counter                                                           */
/*------------------------------------------------------------------------------*/
typedef struct {
    MEA_Counter_t         ACLDrop[MEA_ACL_HIERARC_NUM_OF_FLOW];
} MEA_Counters_ACL_dbt;

MEA_Status MEA_API_ACL_Set_CountersBySID(MEA_Unit_t unit, MEA_Service_t Sid);
MEA_Service_t MEA_API_ACL_Get_CountersBySID(MEA_Unit_t unit);
MEA_Status MEA_API_Collect_Counters_ACLs(MEA_Unit_t unit);
MEA_Status MEA_API_Clear_Counters_ACLs(MEA_Unit_t unit);
MEA_Status MEA_API_Get_Counters_ACL(MEA_Unit_t unit, MEA_Counters_ACL_dbt *entry);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA ACL5 APIs                                      */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct 
{
    MEA_Bool enable_Ipv4;
    MEA_Uint8 Ipv4_dest_mask;
	MEA_Uint8 Ipv4_source_mask;
    MEA_Bool enable_Ipv6;
    MEA_Uint8 Ipv6_dest_mask;
	MEA_Uint8 Ipv6_source_mask;


}MEA_ACL5_Ipmask_dbt;




MEA_Status MEA_API_ACL5_IP_Mask_Prof_Create(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_io, MEA_ACL5_Ipmask_dbt* entry);
MEA_Status MEA_API_ACL5_IP_Mask_Prof_Set(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5_Ipmask_dbt* entry);
MEA_Status MEA_API_ACL5_IP_Mask_Prof_Delete(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i);
MEA_Status MEA_API_ACL5_IP_Mask_Prof_Get(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5_Ipmask_dbt* entry);
MEA_Status MEA_API_ACL5_IP_Mask_Prof_isExist(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_Bool silent, MEA_Bool *exist);
MEA_Status MEA_API_ACL5_IP_Mask_Prof_GetFirst(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5_Ipmask_dbt* entry, MEA_Bool *found_o);
MEA_Status MEA_API_ACL5_IP_Mask_Prof_GetNext(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5_Ipmask_dbt* entry, MEA_Bool *found_o);




typedef struct{
   
	MEA_Bool valid;

    MEA_Uint16  L4dest_min;
    MEA_Uint16  L4dest_max;
    MEA_Uint16  L4src_min;
    MEA_Uint16  L4src_max;
    MEA_Uint8   dscp_min;  /*Dscp priority*/
    MEA_Uint8   dscp_max;
    
    MEA_Uint8  Id;
    MEA_Uint8  priValue;   /* priority level */


}MEA_ACL5_Range_Mask_data_dbt;




MEA_Status MEA_API_ACL5_Range_Mask_Prof_Create(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_io);
MEA_Status MEA_API_ACL5_Range_Mask_Prof_Set(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5Index_t index, MEA_ACL5_Range_Mask_data_dbt* entry);
MEA_Status MEA_API_ACL5_Range_Mask_Prof_Delete(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i);
MEA_Status MEA_API_ACL5_Range_Mask_Index_Delete(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5Index_t index);
MEA_Status MEA_API_ACL5_Range_Mask_Prof_Get(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5Index_t index, MEA_ACL5_Range_Mask_data_dbt* entry);
MEA_Status MEA_API_ACL5_Range_Mask_Prof_isExist (MEA_Unit_t  unit, MEA_ACL5Id_t  Id_i, MEA_Bool silent,MEA_Bool *exist);
MEA_Status MEA_API_ACL5_Range_Mask_Prof_isExistIndex(MEA_Unit_t  unit, MEA_ACL5Id_t  Id_i, MEA_ACL5Index_t index, MEA_Bool silent, MEA_Bool *exist);
MEA_Status MEA_API_ACL5_Range_Mask_Prof_GetFirst(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5Index_t *index, MEA_Bool *found_o, MEA_ACL5_Range_Mask_data_dbt* entry);
MEA_Status MEA_API_ACL5_Range_Mask_Prof_GetNext (MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5Index_t *index, MEA_Bool *found_o, MEA_ACL5_Range_Mask_data_dbt* entry);
MEA_Status MEA_API_ACL5_Range_Mask_Prof_GetmaxOf_Index(MEA_Unit_t  unit, MEA_ACL5Index_t *index);



typedef struct
{
    
    
    MEA_Uint32 TAG1             : 1;
    MEA_Uint32 TAG1_p           : 1;
    MEA_Uint32 TAG2             : 1;
    MEA_Uint32 TAG3             : 1;
    MEA_Uint32 L3_EtherType     : 1;
    MEA_Uint32 DSCP_TC          : 1;       /*IPv4 DSCP/IPv6 TC*/
    MEA_Uint32 Ip_Protocol_NH   : 1;      /*IPv4 protocol/IPv6 Next Header*/
    MEA_Uint32 IPv6FlowLabel    : 1;
    MEA_Uint32 L4src            : 1;
    MEA_Uint32 L4dst            : 1;
    MEA_Uint32 SID              : 1;
    MEA_Uint32 IPv4_Flags       : 1;
    MEA_Uint32 TCP_Flags        : 1;
    MEA_Uint32 pad				:15;


}MEA_ACL5_KeyMask_dbt;


MEA_Status MEA_API_ACL5_Key_Mask_Prof_Create(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_io, MEA_ACL5_KeyMask_dbt* entry);
MEA_Status MEA_API_ACL5_Key_Mask_Prof_Set(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5_KeyMask_dbt* entry);
MEA_Status MEA_API_ACL5_Key_Mask_Prof_Delete(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i);
MEA_Status MEA_API_ACL5_Key_Mask_Prof_Get(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_ACL5_KeyMask_dbt* entry);
MEA_Status MEA_API_ACL5_Key_Mask_Prof_isExist(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_Bool silent, MEA_Bool *exist);
MEA_Status MEA_API_ACL5_Key_Mask_Prof_GetFirst(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5_KeyMask_dbt* entry, MEA_Bool *found_o);
MEA_Status MEA_API_ACL5_Key_Mask_Prof_GetNext(MEA_Unit_t  unit, MEA_ACL5Id_t *Id_o, MEA_ACL5_KeyMask_dbt* entry, MEA_Bool *found_o);



/*------------------------------------*/

typedef enum {
    MEA_ACL5_KeyType_NONE=0,
    MEA_ACL5_KeyType_L2=1,
    MEA_ACL5_KeyType_IPV4 = 2,
    MEA_ACL5_KeyType_IPV6_FULL = 3,
    MEA_ACL5_KeyType_IPV6_DST = 4,
    MEA_ACL5_KeyType_IPV6_SRC = 5,
    MEA_ACL5_KeyType_IPV6_SIG_Dest = 6,
    MEA_ACL5_KeyType_IPV6_SIG_SRC = 7,


}MEA_ACL5_KeyType_te;

typedef struct {

	MEA_Bool valid;     // DoAcl5
	MEA_ACL5_KeyType_te keyType;
	MEA_ACL5Id_t ip_mask_id;
	MEA_ACL5Id_t range_mask_id;
	MEA_ACL5Id_t key_mask_id;
	MEA_Uint32 Inner_Frame : 1;

}MEA_ACL5_Mask_Grouping_Profiles;

MEA_Status MEA_API_ACL5_Mask_Grouping_Profiles_Create(MEA_Unit_t  unit_i, MEA_ACL5_Mask_Grouping_Profiles* entry, MEA_ACL5Id_t *id_io);
MEA_Status MEA_API_ACL5_Mask_Grouping_Profiles_Get(MEA_Unit_t  unit_i, MEA_ACL5Id_t Id_i, MEA_ACL5_Mask_Grouping_Profiles* entry_prof);
MEA_Status MEA_API_ACL5_Mask_Grouping_Profiles_isExist(MEA_Unit_t  unit, MEA_ACL5Id_t Id_o, MEA_Bool silent, MEA_Bool *exist);
MEA_Status MEA_API_ACL5_Mask_Grouping_Profiles_Delete(MEA_Unit_t  unit_i, MEA_ACL5Id_t Id_i);


typedef struct {
   
    MEA_ACL5_KeyType_te keyType;

       
	MEA_ACL5Id_t    ACLprof;        /* Service profId 12bit*/



    
    struct {
         
        struct {

			MEA_Uint32      MacDA_en   : 1;
			MEA_Uint32      MacSA_en   : 1;
			MEA_Uint32      MAC_UMB_en : 1;
            MEA_Uint32      MAC_UMB    : 2;  /**/
            MEA_Uint32      pad0       : 27;
            MEA_MacAddr     MacDA;
            MEA_MacAddr     MacSA;
            

        } Mac;

         MEA_Uint32 Tag1;
         MEA_Uint32 Tag1_p;
         MEA_Uint32 Tag2;
         MEA_Uint32 Tag3;
		 MEA_Bool Tag1_valid;
		 MEA_Bool Tag1_p_valid;
		 MEA_Bool Tag2_valid;
		 MEA_Bool Tag3_valid;

    } layer2;

    struct {
        MEA_Uint32                         IPv4_Flags_valid : 1;
        MEA_Uint32 IPv4_Flags : 2;
       
        MEA_Uint32                         l3_dscp_valid     : 1;
        MEA_Uint32                         ethertype3_valid  : 1;
        MEA_Uint32                         ethertype3        : 16;
        
        MEA_Uint32                         ip_protocol_valid : 1;
        MEA_Uint32                         ip_protocol       : 8;
       
        MEA_Filter_L3_Pri_Type_te          l3_pri_type;      
        MEA_Filter_l3_priority_val_dbt     l3_dscp_value;

        struct {
            MEA_Uint32                         dst_ip_valid : 1;
            MEA_Uint32                         src_ip_valid : 1;
            MEA_Uint32                      pad : 30;
            MEA_Uint32                        dst_ip;
            MEA_Uint32                        src_ip;
        } IPv4;

        struct {
            MEA_Uint32                      Dst_Ipv6_valid  : 1;
            MEA_Uint32                      Src_Ipv6_valid  : 1;
            MEA_Uint32                      IPv6_Flow_Label : 20;
			MEA_Bool                      IPv6_Flow_Label_valid;
            MEA_Uint32                      pad             : 10;
            MEA_Uint32                      Dst_Ipv6[4];
            MEA_Uint32                      Src_Ipv6[4];
        } IPv6;

    } layer3;

    struct {
		MEA_Bool                   TCP_Flags_valid;
        MEA_Uint32                 TCP_Flags      : 8;
        MEA_Uint32                 dst_port_valid : 1; //0 with range 1-match 
        MEA_Uint32                 src_port_valid : 1; // 0 with range 1-match
        MEA_Uint32                 pad4 : 22;
        MEA_Uint32                 dst_port;
        MEA_Uint32                 src_port;
    } layer4;

    // profiles 
	MEA_Uint32 Acl5_mask_prof_id;
	MEA_ACL5_Mask_Grouping_Profiles Acl5_mask_prof;
	
	MEA_ACL5_KeyMask_dbt ACL5_key_mask_prof;

    MEA_Uint32 rangkProf_index    : 8;


    MEA_Uint32 rangkProf_rule     : 8;



} MEA_ACL5_Key_dbt;


typedef struct { 

    MEA_Bool               OutPorts_valid;  
    MEA_OutPorts_Entry_dbt OutPorts;        

    
    MEA_Uint32 lxcp_win		:1;
    MEA_Uint32 fwd_Act_en   :1;   // take the Action form FWD ??? fwd_win
	MEA_Uint32 fwd_int_mac  :1;
	MEA_Uint32 fwd_key      :4;
	MEA_Uint32 fwd_force    :1;


    MEA_Uint32 Action_Valid;
    MEA_Uint32 Action_Id;           //Action for ACL5


} MEA_ACL5_Entry_Data_dbt;



MEA_Status MEA_API_ACL5_Create_Entry(MEA_Unit_t              unit, 
                                     MEA_ACL5_Key_dbt        *key,
                                     MEA_ACL5_Entry_Data_dbt *data,
									 MEA_ACL5Id_t              *Id_io);


MEA_Status MEA_API_ACL5_Get_Entry(MEA_Unit_t              unit,
								  MEA_ACL5Id_t              Id_i,
                                  MEA_ACL5_Key_dbt        *key,
                                  MEA_ACL5_Entry_Data_dbt *data);

MEA_Status MEA_API_ACL5_Delete_Entry(MEA_Unit_t              unit,
									 MEA_ACL5Id_t              Id_i);

MEA_Status MEA_API_ACL5_isExist(MEA_Unit_t  unit, MEA_ACL5Id_t Id_i, MEA_Bool silent, MEA_Bool *exist);

 /*---------------------------------------------*/                                  
typedef struct {
    MEA_Bool   valid;

    MEA_Action_Entry_Data_dbt                *action_params;
    MEA_OutPorts_Entry_dbt                   *OutPorts_Entry;
    MEA_Policer_Entry_dbt                    *Policer_Entry;
    MEA_EgressHeaderProc_Array_Entry_dbt     *EHP_Entry;
}MEA_Action_rule_t;


MEA_Status MEA_API_Create_ACL5Action(MEA_Unit_t unit, MEA_Action_t *Id_io, MEA_Action_rule_t entry);
MEA_Status MEA_API_Get_ACL5Action(MEA_Unit_t unit, MEA_Action_t Id_i, MEA_Action_rule_t entry);
MEA_Status MEA_API_Delete_ACL5Action(MEA_Unit_t unit, MEA_Action_t Id_i);

MEA_Status MEA_API_DeleteAll_Action_ACL5(MEA_Unit_t unit);
MEA_Status MEA_API_ACL5Action_isExist(MEA_Unit_t  unit, MEA_Action_t Id_i, MEA_Bool silent, MEA_Bool *exist);


typedef struct {

    MEA_ACL5_PAC_MODE flag_match;
    MEA_ACL5_PAC_MODE flag_unmatch;
	MEA_ACL5_Key_dbt Last;
	MEA_ACL5_Key_dbt unmatch;

} MEA_Events_ACL5_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA SE Table Typedefs                                     */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define MEA_FWD_VLAN_RANGE_SUPPORT

typedef struct {
   
    MEA_SE_Forwarding_key_type_te type;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN       
       MEA_MacAddr MAC              ;     //  0..47 MAC address (48 bits) - DA (forwarding) / SA (learning) 
       MEA_Uint32  VPN             : 12;     // 48..59 Virtual Private Network  
//       MEA_Uint32 mask_type        : 2;  /* 58-59 */
       MEA_Uint32  reserverd       :  4;     // 61..63
       MEA_Uint32  pad_0[2];
       //MEA_SE_Forwarding_key_type_te type;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
      // MEA_SE_Forwarding_key_type_te type;
       MEA_Uint32  pad_0[2];
       MEA_Uint32  reserverd       :  4;     // 61..63
//       MEA_Uint32 mask_type        :  2;  /* 58-59 */
       MEA_Uint32  VPN             : 12;     // 48..59 Virtual Private Network  
       MEA_MacAddr MAC                    ;     //  0..47 MAC address (48 bits) - DA (forwarding) / SA (learning) 
     
#endif           
    } mac_plus_vpn;
//#ifdef MEA_FWD_MAC_ONLY_SUPPORT    
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_MacAddr MAC                    ;     //  0..47 MAC address (48 bits) - DA (forwarding) / SA (learning) 
       MEA_Uint32  reserverd       : 16;     // 48..63
       MEA_Uint32  pad_1[2];
       //MEA_SE_Forwarding_key_type_te type; 
#else
       //MEA_SE_Forwarding_key_type_te type;  
       MEA_Uint32  pad_1[2];
       MEA_Uint32  reserverd       : 16;     // 48..63
       MEA_MacAddr MAC                    ;     //  0..47 MAC address (48 bits) - DA (forwarding) / SA (learning) 

#endif       
    
    } mac;
//#endif
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  label_1          : 20;     //  0:11 VLAN Id,0:19MPLS label  
        MEA_Uint32  VPN              : 10;
        MEA_Uint32 mask_type         : 2;  /*  */
        MEA_Uint32  label_2          : 20;     //  0:11 VLAN Id,0:19MPLS label 
        MEA_Uint32  reserverd2       : 12;    // 45..63
        MEA_Uint32  pad_3[2];
        //MEA_Uint32  align_pad        : 32;    // Special pad because of alignment problem
        //MEA_SE_Forwarding_key_type_te type;

#else
       // MEA_SE_Forwarding_key_type_te type;
        // MEA_Uint32  align_pad        : 32;    // Special pad because of alignment problem
        MEA_Uint32  pad_3[2];
        MEA_Uint32  reserverd2       : 12;    // 45..63
        MEA_Uint32  label_2          : 20;
        MEA_Uint32 mask_type         : 2;  /*  */
        MEA_Uint32  VPN              : 10; 
        MEA_Uint32  label_1          : 20;     //  0:11 VLAN Id,0:19MPLS label 

#endif        
    } mpls_vpn;
#ifdef PPP_MAC  
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_MacAddr MAC                    ;     //  0..47 MAC address (48 bits) - SA (forwarding) / DA (learning) 
       MEA_Uint32  PPPoE_session_id    : 16; // 48..63 PPPoE Session Id
       MEA_Uint32  pad_2[2];
       //MEA_SE_Forwarding_key_type_te type;
       
#else
       //MEA_SE_Forwarding_key_type_te type;
       MEA_Uint32  pad_2[2];
       MEA_Uint32  PPPoE_session_id    : 16; // 48..63 PPPoE Session Id
       MEA_MacAddr MAC                    ;     //  0..47 MAC address (48 bits) - SA (forwarding) / DA (learning)  
#endif           
    } mac_plus_pppoe_session_id;
#else
	struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN       
		MEA_Uint32  PPPoE_session_id  : 16 ;
		MEA_Uint32  pad1   : 16;
		MEA_Uint32  pad2   : 16;
		MEA_Uint32  VPN       : 10;     // 48..59 Virtual Private Network  
        MEA_Uint32 mask_type  :  2;  /* 58-59 */
		MEA_Uint32  reserverd : 4;     // 60..63
		MEA_Uint32  pad_0[2];
		//MEA_SE_Forwarding_key_type_te type;
#else
		// MEA_SE_Forwarding_key_type_te type;
		MEA_Uint32  pad_0[2];
		MEA_Uint32  reserverd : 4;     // 61..63
									   //       MEA_Uint32 mask_type        :  2;  /* 58-59 */
		MEA_Uint32  VPN  : 10;     // 48..59 Virtual Private Network
		MEA_Uint32  pad2 : 16;
		MEA_Uint32  pad1 : 16;
		MEA_Uint32  PPPoE_session_id : 16;

#endif           
	} PPPOE_plus_vpn;
#endif
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32  VLAN             : 12;     //  0..11 VLAN Id 
       MEA_Uint32  VPN              : 10;
       MEA_Uint32  mask_type         : 2;  /* 58-59 */
       MEA_Uint32  reserverd1       : 8;     // 12..31
       MEA_Uint32  VLAN_to_enable   : 1;     // 32..32 /*Not Support*/
       MEA_Uint32  VLAN_to_val      : 12;    // 33..44 /*Not Support*/
       MEA_Uint32  reserverd2       : 19;    // 45..63
       MEA_Uint32  pad_3[2];
       //MEA_Uint32  align_pad        : 32;    // Special pad because of alignment problem
       //MEA_SE_Forwarding_key_type_te type;

#else
      // MEA_SE_Forwarding_key_type_te type;
      // MEA_Uint32  align_pad        : 32;    // Special pad because of alignment problem
       MEA_Uint32  pad_3[2];
       MEA_Uint32  reserverd2       : 19;    // 45..63
       MEA_Uint32  VLAN_to_val      : 12;    // 33..44 /*Not Support*/
       MEA_Uint32  VLAN_to_enable   : 1;     // 32..32 /*Not Support*/
       MEA_Uint32  reserverd1       : 8;     // 12..31
       MEA_Uint32  mask_type         : 2;  /* 58-59 */
       MEA_Uint32  VPN              : 10; 
       MEA_Uint32  VLAN             : 12;     //  0..11 VLAN Id 
       
#endif        
    } vlan_vpn;
    struct {

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  label_1                : 20;     //  0..19
        MEA_Uint32  MD_level               : 4;     // 20..23
        MEA_Uint32  op_code                : 8;     // 24..31
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32  op_code              : 8;    // 24..31
        MEA_Uint32  MD_level             : 4;    // 20..23
        MEA_Uint32  label_1              : 20;    //  0..19
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32  packet_is_untag_mpls   : 1;    // 32..32
       MEA_Uint32  Src_port               : 10;    // 33..42
       MEA_Uint32  reserverd_1            : 5;    // 43 47
       MEA_Uint32  VPN                    : 10;    // 55..48
       MEA_Uint32 mask_type               : 2;  /* 58-59 */
       MEA_Uint32  pad1                   :  4;    // 60..63
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
        MEA_Uint32  pad1                 : 4;    // 60..63
        MEA_Uint32 mask_type             : 2;  /* 58-59 */
        MEA_Uint32  VPN                 : 10;   // 59..48
        MEA_Uint32  reserverd_1         : 5;   // 40 47
        MEA_Uint32  Src_port            : 10;    // 33..39
        MEA_Uint32  packet_is_untag_mpls : 1;    // 32..32
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32  pad_4[2];
       //MEA_SE_Forwarding_key_type_te type;
#else
       // MEA_SE_Forwarding_key_type_te type;
        MEA_Uint32  pad_4[2]           ;
#endif
        
    } oam_vpn;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32  DstIPv4          ;
#else
        MEA_Uint32  DstIPv4         ;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32  SrcIPv4          ; 
#else
        MEA_Uint32  SrcIPv4         ;   
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32  VPN              : 10;
       MEA_Uint32  mask_type        : 2;
       MEA_Uint32  reserverd        : 20;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32  reserverd        : 20;
        MEA_Uint32  VPN : 10;
        MEA_Uint32  mask_type : 2;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32  pad_5[1];
#else
       MEA_Uint32  pad_5[1];
#endif        
    } DstIPv4_plus_SrcIPv4_plus_vpn;

    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  IPv4             ;
#else
        MEA_Uint32  IPv4     ;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  L4port           : 16;     
        MEA_Uint32  VPN              : 10;
        MEA_Uint32 mask_type         : 2;
        MEA_Uint32  reserverd        : 4;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32  reserverd        : 4;
        MEA_Uint32 mask_type         : 2;
        MEA_Uint32  VPN              : 10;
        MEA_Uint32  L4port           : 16;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  pad[2];

#else
        MEA_Uint32  pad[2];
#endif        
    } DstIPv4_plus_L4_DstPort_plus_vpn;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  IPv4;
#else
        MEA_Uint32  IPv4;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  L4port           : 16;     
        MEA_Uint32  VPN              : 10;
        MEA_Uint32  mask_type        : 2;
        MEA_Uint32  reserverd        : 4;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32  reserverd : 4;
        MEA_Uint32  mask_type : 2;
        MEA_Uint32  VPN : 10;
        MEA_Uint32  L4port : 16;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  pad_7[2];
#else
        MEA_Uint32  pad_7[2];
#endif        
    } SrcIPv4_plus_L4_srcPort_plus_vpn;

    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  IPv6[4];
#else
        MEA_Uint32  IPv6[4];
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  L4port    : 16;
        MEA_Uint32  VPN       : 10;
        MEA_Uint32  mask_type : 2;
        MEA_Uint32  reserverd : 4;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32  reserverd : 4;
        MEA_Uint32  mask_type : 2;
        MEA_Uint32  VPN       : 10;
        MEA_Uint32  L4port    : 16;
#endif
       
    }SE_DstIPv6_plus_L4DstPort_plus_vpn;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  srcIPv6[4];
#else
        MEA_Uint32  IPv6[4];
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  L4port : 16;
        MEA_Uint32  VPN : 10;
        MEA_Uint32  mask_type : 2;
        MEA_Uint32  reserverd : 4;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32  reserverd : 4;
        MEA_Uint32  mask_type : 2;
        MEA_Uint32  VPN : 10;
        MEA_Uint32  L4port : 16;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  pad_7[2];
#else
        MEA_Uint32  pad_7[2];
#endif        
    }SE_SrcIPv6_plus_L4_srcPort_plus_vpn;

} MEA_SE_Entry_key_dbt;



typedef struct {
    MEA_Bool               OutPorts_valid;
    MEA_OutPorts_Entry_dbt OutPorts;    
    MEA_Uint32     static_flag     :  1;
    MEA_Uint32     aging           :  2;
    MEA_Uint32     valid           :  1;
    MEA_Uint32     sa_discard      :  1;      

    MEA_Uint32     access_port      : 10;


    MEA_Uint32     reserved        : 10;      
    MEA_Uint32     limiterId_valid :  1;      
    MEA_Uint32     actionId_valid  :  1;      
    MEA_Action_t   actionId;                
    MEA_Limiter_t  limiterId;

    //internal for filter
    MEA_Bool             xPermissionId_valid;
    ENET_xPermissionId_t xPermissionId;  /*read from HW*/
    MEA_Bool             actionId_err;
    MEA_Bool             xPermissionId_err;

 /*** Save the hw info for warm reset only ****/
    MEA_Uint16 save_hw_xPermissionId;

} MEA_SE_Entry_data_dbt;




typedef struct {
    MEA_SE_Entry_key_dbt  key;
    MEA_SE_Entry_data_dbt data;
    MEA_Bool  filterEnable;
    MEA_Uint8 filterId;

} MEA_SE_Entry_dbt;







typedef struct {
    
	MEA_Uint32            fwd_mask_key_enable :1;
    MEA_SE_Entry_key_dbt  fwd_mask_key;
    MEA_SE_Entry_data_dbt fwd_mask_data;
    
    
/////////////////////////////////////////
	MEA_Uint32 fwd_fields_key_enable :1;

    MEA_SE_Entry_key_dbt  fwd_fields_key;
    MEA_SE_Entry_data_dbt fwd_fields_data;
    
    MEA_Uint32   fwd_reg_enable :1;

    MEA_Uint32 fwd_mask_data_reg[2];
    MEA_Uint32 fwd_mask_key_reg[2];
    MEA_Uint32 fwd_fields_data_reg[2];
    MEA_Uint32 fwd_fields_key_reg[2];

}MEA_SE_filter_Entry_dbt;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA SE APIs                                               */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    MEA_Uint32 fwd_vpn;
    MEA_Uint16 fwd_KeyType;
}MEA_FWD_VPN_dbt;

typedef struct {
    MEA_Bool valid;

    MEA_Uint32 IPv6_Signature;

    MEA_Uint32 DestIPv6[4];

}mea_fwd_vpn_IPv6_info_dbt;

MEA_Bool MEA_API_Exist_FWD_VPN_Ipv6(MEA_Unit_t    unit, MEA_FWD_VPN_dbt *entry);
MEA_Status MEA_API_Create_FWD_VPN_Ipv6(MEA_Unit_t    unit, MEA_FWD_VPN_dbt *entry);
MEA_Status MEA_API_Delete_FWD_VPN_Ipv6(MEA_Unit_t unit, MEA_FWD_VPN_dbt *entry);

MEA_Status MEA_API_Get_FWD_VPN_Ipv6(MEA_Unit_t unit, MEA_FWD_VPN_dbt *entry);
MEA_Status MEA_API_GetFirst_FWD_VPN_Ipv6(MEA_Unit_t unit, MEA_FWD_VPN_dbt *entry, MEA_Bool*  found);
MEA_Status MEA_API_GetNext_FWD_VPN_Ipv6(MEA_Unit_t unit, MEA_FWD_VPN_dbt *entry, MEA_Bool*  found);

MEA_Status MEA_API_GetFirst_FWD_VPN_Ipv6DB(MEA_Unit_t unit, MEA_Uint32 vpn, mea_fwd_vpn_IPv6_info_dbt *IPv6_info, MEA_Bool*  found);
MEA_Status MEA_API_GetNext_FWD_VPN_Ipv6DB(MEA_Unit_t unit, MEA_Uint32 vpn, mea_fwd_vpn_IPv6_info_dbt *IPv6_info, MEA_Bool*  found);



MEA_Status MEA_API_Create_SE_Entry     (MEA_Unit_t                        unit,
                                        MEA_SE_Entry_dbt               *entry);


MEA_Status MEA_API_Delete_SE_Entry     (MEA_Unit_t            unit_i,
                                        MEA_SE_Entry_key_dbt *key_pi);

MEA_Status MEA_API_Set_SE_Entry   (MEA_Unit_t                      unit,
                                   MEA_SE_Entry_dbt               *entry);

MEA_Status MEA_API_Get_SE_Entry        (MEA_Unit_t                        unit,
                                        MEA_SE_Entry_dbt               *entry);



MEA_Status MEA_API_GetFirst_SE_Entry   (MEA_Unit_t               unit,
                                        MEA_SE_Entry_dbt       *entry,
                                        MEA_Bool*                found);



MEA_Status MEA_API_GetNext_SE_Entry   (MEA_Unit_t               unit,
                                        MEA_SE_Entry_dbt *entry,
                                        MEA_Bool*                found);




MEA_Status MEA_API_Set_SE_Aging (MEA_Unit_t unit,
                                  MEA_Uint32 seconds,
                                  MEA_Bool   admin_status);

MEA_Status MEA_API_Get_SE_Aging  (MEA_Unit_t unit,
                                   MEA_Uint32 *seconds,
                                   MEA_Bool   *admin_status);

MEA_Status MEA_API_DeleteAll_SE (MEA_Unit_t unit);

/*-------------------------------------------------------------------------------*/
/*        API   SE_Entry_xxxFilter                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_SE_Entry_CreateFilter (MEA_Unit_t               unit_i,
                                          MEA_Uint16                index);

MEA_Status MEA_API_SE_Entry_SetFilter (MEA_Unit_t               unit_i,
                                       MEA_Uint16                index,
                                       MEA_SE_filter_Entry_dbt  *entry_pi);

MEA_Status MEA_API_SE_Entry_GetFilter (MEA_Unit_t               unit_i,
                                       MEA_Uint16                index,
                                       MEA_SE_filter_Entry_dbt  *entry_o);

MEA_Status MEA_API_SE_Entry_DeleteFilter (MEA_Unit_t               unit_i,
                                          MEA_Uint16                index);

MEA_Bool MEA_API_SE_Entry_IsExistFilter (MEA_Unit_t               unit_i,
                                         MEA_Uint16                index);




MEA_Status MEA_API_FWD_BrgFlushPortFid (MEA_Port_t EnetPortId , MEA_Uint16 VPN, MEA_SE_Forwarding_key_type_te keyType);


/************************************************************************/
/* GW     API                                                             */
/************************************************************************/
typedef enum{
    MEA_VPLS_EVENT_ACCESS_TYPE_FWD_NORMAL = 0,
    MEA_VPLS_EVENT_ACCESS_TYPE_FWD_DISCARD_FROM_LRN = 1, 
    MEA_VPLS_EVENT_ACCESS_TYPE_OVERRULE = 2,
    MEA_VPLS_EVENT_ACCESS_TYPE_FWD_DISCARD_FROM_CNTX =3,
    MEA_VPLS_EVENT_ACCESS_TYPE_DISCARD_FROM_FWD_AND_ST_CNTX_LOCAL_SW =4,
    MEA_VPLS_EVENT_ACCESS_TYPE_FWD_HANDHOVER_IN_0=5,
    MEA_VPLS_EVENT_ACCESS_TYPE_FWD_HANDHOVER_IN_1 =6
    

}mea_GW_VPLS_EventType_t;

typedef struct
{

    MEA_Uint16 root_filterId;
    MEA_Uint16 GTPv1_flag;
    MEA_MacAddr DA;
    MEA_Uint16 GTP_message_type;
    MEA_Uint16 ip_protocol_int;
    MEA_MacAddr SA;
    MEA_Uint32 sgw_teid;
    MEA_Uint32 src_ipv4_int;
    MEA_Uint32 dst_ipv4_int;
    MEA_Uint16 ethertype_int;
    MEA_Uint16 vlan_int; 

    /*----------*/

        struct {
        MEA_Uint32 st_cntx_ac_port      : 10;
        MEA_Uint32 lrn_ac_port          : 10;
        MEA_Uint32 fwd_ac_port          : 10;
        MEA_Uint32 pad0                 :  2;
        MEA_Uint32 info                 :  3; /* mea_GW_VPLS_EventType_t */
        MEA_Uint32 pad                  : 29;

    }event_vpls_access;


} MEA_GW_evetEntry_dbt;


MEA_Status MEA_API_GW_Get_EVENT(MEA_Unit_t unit, MEA_GW_evetEntry_dbt   *EventEntry);





typedef enum {
    MEA_ROOT_FILTER_TYPE_DHCP_REALY_REPLY = 0,
    MEA_ROOT_FILTER_TYPE_DHCP_IP_CS_REQUEST = 1,
    MEA_ROOT_FILTER_TYPE_DHCP_ETH_CS_REQUEST = 2,
    MEA_ROOT_FILTER_TYPE_DL_IP_CS = 3,
    MEA_ROOT_FILTER_TYPE_DL_IPCS_IPV6      = 4,               /* (L2_type=4) – regular packet*/
    MEA_ROOT_FILTER_TYPE_UL_IPCS_IPV4_IPV6 = 5,               /* (L2_type=1) – regular packet*/
    MEA_ROOT_FILTER_TYPE_UL_VPWS = 6,                   /* Internal packet can be untag or tag */
    MEA_ROOT_FILTER_TYPE_UL_IP_CS = 7,
    MEA_ROOT_FILTER_TYPE_INTERNAL_FRAG = 8,
    MEA_ROOT_FILTER_TYPE_EXTERNAL_FRAG = 9, 
    MEA_ROOT_FILTER_TYPE_INFRA_ARP_REQUST_TO_CP = 10,
    MEA_ROOT_FILTER_TYPE_INFRA_ARP_RESPON_TO_CP = 11,
    MEA_ROOT_FILTER_TYPE_INFRA_ARP_REQUST_TO_UE = 12,
    MEA_ROOT_FILTER_TYPE_INFRA_ARP_REQUST_TO_MNG = 13,
    MEA_ROOT_FILTER_TYPE_INFRA_ARP_RESPONE_TO_MNG = 14,
    MEA_ROOT_FILTER_TYPE_CONTROL_PLANE_SCTP = 15, /* SCTP_eNB_MME*/
    MEA_ROOT_FILTER_TYPE_CONTROL_PLANE_GTP = 16, /* GTP-U_Keep_Alive*/
    MEA_ROOT_FILTER_TYPE_LOCAL_MNG = 17,

    MEA_ROOT_FILTER_TYPE_LAST
}MEA_root_Filter_type_t;


MEA_Status MEA_API_Set_RootFilter_Entry(MEA_Unit_t                 unit,
    MEA_root_Filter_type_t     index,
    MEA_Bool                   enable);

MEA_Status MEA_API_Get_RootFilter_Entry(MEA_Unit_t                  unit,
    MEA_root_Filter_type_t      index,
    MEA_Bool                    *enable);

/************************************************************************************/
#define MEA_GW_MAX_OF_APN_VLAN  8
#define MEA_GW_MAX_OF_DHCP      8
#define MEA_GW_MAX_OF_SGW_IP    8
#define MEA_GW_MAX_OF_LEARN_SID  1
#define MEA_GW_MAX_OF_PDN_IPV4    8


typedef struct {
    /*each param is zero it means that it is not valid*/
    MEA_MacAddr My_Host_MAC;
    MEA_MacAddr My_PGW_MAC;
    MEA_Uint16 APN_vlanId[MEA_GW_MAX_OF_APN_VLAN];
    MEA_Uint8  APN_Ipv6_mask_Type[MEA_GW_MAX_OF_APN_VLAN];
    MEA_Uint64 APN_IpV6[MEA_GW_MAX_OF_APN_VLAN];
    
    MEA_Uint32 PDN_Ipv4[MEA_GW_MAX_OF_PDN_IPV4];

    MEA_Bool  Ipv6_enable; /*0-Ipv4 1-IpV6*/
    MEA_Uint32 DHCP_IP[MEA_GW_MAX_OF_DHCP];
    MEA_Uint32 Infra_VLAN;
    MEA_Uint8  Infra_VLAN_Ipv6mask;
    MEA_Uint32 S_GW_IP[MEA_GW_MAX_OF_SGW_IP];
    MEA_Uint32 P_GW_U_IP;
    MEA_Uint32 MNG_vlanId;
    MEA_Uint32 Conf_D_IP;
    MEA_Uint32 MME_IP;

    MEA_Def_SID_dbt default_sid_info; /* 32b */

    MEA_Def_SID_dbt IPCS_default_sid_info;
    MEA_Uint32      IPCS_default_sid_srcport;
    MEA_Def_SID_dbt ETHCS_UL_default_sid_info;

    MEA_Uint8 edit_IP_TTL;
    MEA_Uint8 InternalDscp;
    MEA_Uint32 local_mng_ip;

    MEA_Uint32 learn_enb_vpn;
    MEA_Uint32 vxlan_L4_dst_port;
    MEA_Uint32 vxlan_upto_vlan;

   

}MEA_Gw_Global_dbt;

MEA_Status MEA_API_Set_GW_Global_Entry(MEA_Unit_t                 unit,
    MEA_Gw_Global_dbt         *entry);

MEA_Status MEA_API_Get_GW_Global_Entry(MEA_Unit_t                 unit,
    MEA_Gw_Global_dbt          *entry);

/******************************************************************************************/

typedef enum {
    MEA_CHARACTER_TYPE_TEID = 0,
    MEA_CHARACTER_TYPE_GRE = 1,
    MEA_CHARACTER_TYPE_LAST
}mea_Character_t;



typedef enum {
    MEA_CHARACTER_TYPE_TEID_MODE_NONE = 0,
    MEA_CHARACTER_TYPE_TEID_MODE_SM1 = 1,
    MEA_CHARACTER_TYPE_TEID_MODE_SM1a = 2,
    MEA_CHARACTER_TYPE_TEID_MODE_SM2 = 3,
    MEA_CHARACTER_TYPE_TEID_MODE_SM2a = 4,
    MEA_CHARACTER_TYPE_TEID_MODE_SM2b = 5,
    MEA_CHARACTER_TYPE_TEID_MODE_SM3 = 6,
    MEA_CHARACTER_TYPE_TEID_MODE_SM4 = 7,
    MEA_CHARACTER_TYPE_TEID_MODE_SM5 = 8,
    MEA_CHARACTER_TYPE_TEID_MODE_SM5a = 9,
    MEA_CHARACTER_TYPE_TEID_MODE_SM3a = 10,     /*like SM3*/
    MEA_CHARACTER_TYPE_TEID_MODE_IP_CS_UL = 11, /*Not support*/
    MEA_CHARACTER_TYPE_TEID_MODE_IP_CS_UL_Learn = 12, /*Not support*/

    MEA_CHARACTER_TYPE_TEID_MODE_Vlan_To_Vxlan  = 20,
    MEA_CHARACTER_TYPE_TEID_MODE_Vlan_To_Vxlan_noVlan = 21,

    MEA_CHARACTER_TYPE_TEID_MODE_LAST
}mea_Character_TEID_mode_t;





typedef enum {
    MEA_TYPE_TEID_IPV6_MASK_64 = 0,
    MEA_TYPE_TEID_IPV6_MASK_63 = 1,
    MEA_TYPE_TEID_IPV6_MASK_62 = 2,
    MEA_TYPE_TEID_IPV6_MASK_61 = 3,
    MEA_TYPE_TEID_IPV6_MASK_60 = 4,
    MEA_TYPE_TEID_IPV6_MASK_59 = 5,
    MEA_TYPE_TEID_IPV6_MASK_LAST=6

}MEA_IPCS_IPV6_MASK_Type_t;

typedef struct {
    MEA_Bool                  valid;
    mea_Character_t           type;
    MEA_IPCS_IPV6_MASK_Type_t ipv6_mask_type;
    MEA_Bool                  learn_en;  /*('1' -learning, '0'-not learning)*/
}MEA_Characteristics_entry_dbt;

MEA_Status MEA_API_Set_ClassifierKEY_Character_Entry(MEA_Unit_t unit,MEA_Uint32  TEId,MEA_Characteristics_entry_dbt   *entry);

MEA_Status MEA_API_Get_ClassifierKEY_Character_Entry(MEA_Unit_t unit, MEA_Uint32 TEId, MEA_Characteristics_entry_dbt   *entry);

   


/************************************************************************/
/* VPLS                                                                 */
/************************************************************************/



typedef struct{
    MEA_Uint32         max_of_fooding;
    MEA_Uint16         vpn;

    /*need to get info of the packet type and info*/
    /* and what the */
    
    mea_Character_TEID_mode_t SM_mode;
    /*
        MEA_CHARACTER_TYPE_TEID_MODE_SM1     Append vpls ID Extract Vpls ID
        MEA_CHARACTER_TYPE_TEID_MODE_SM2     Append vpls ID Extract Vpls ID
        MEA_CHARACTER_TYPE_TEID_MODE_SM2a    Swap   vpls ID Extract Vpls ID
        MEA_CHARACTER_TYPE_TEID_MODE_SM3     Swap   vpls ID Extract Vpls ID
        MEA_CHARACTER_TYPE_TEID_MODE_SM3a    Swap   vpls ID 2 Extract Vpls ID    TBD
        MEA_CHARACTER_TYPE_TEID_MODE_SM4     Append vpls ID Extract Vpls ID
        MEA_CHARACTER_TYPE_TEID_MODE_SM5     Swap   vpls ID Extract Vpls ID
        MEA_CHARACTER_TYPE_TEID_MODE_SM5a   Swap   vpls ID Extract Vpls ID and Swap vlan

    */

    
   
 

}MEA_VPLS_dbt;

typedef struct{

    MEA_Action_t ActionId; /*from the Action we have the editId then we have all info to create the DownLink VPWS */
    //MEA_Uint16   vpn;
    MEA_Uint16    Access_port_id;


    MEA_Bool en_service_TFT;
    MEA_Service_Entry_Data_dbt            *Service_Entry_data;
    //MEA_OutPorts_Entry_dbt                *Service_Entry_OutPorts;
    MEA_Policer_Entry_dbt                 *Service_Entry_Policer;
    MEA_EgressHeaderProc_Array_Entry_dbt  *EHP_Entry;

    MEA_Bool                                 en_output;
    MEA_OutPorts_Entry_dbt                   *Service_Entry_OutPorts;

    /*Save the ServiceId*/
    MEA_Service_t serviceId;

}MEA_VPLS_UE_Pdn_dl_dbt;

MEA_Status MEA_API_VPLSi_Create_Entry(MEA_Unit_t  unit, MEA_Uint16 *vpls_Ins_Id, MEA_VPLS_dbt* entry);
MEA_Status MEA_API_VPLSi_Delete_Entry(MEA_Unit_t  unit, MEA_Uint16 vpls_Ins_Id);
MEA_Status MEA_API_VPLSi_isExist(MEA_Unit_t  unit, MEA_Uint16 vpls_Ins_Id, MEA_Bool *exist);
MEA_Status MEA_API_VPLSi_Get_Entry(MEA_Unit_t  unit, MEA_Uint16 vpls_Ins_Id, MEA_VPLS_dbt* entry);
MEA_Status MEA_API_VPLSi_Add_Group(MEA_Unit_t  unit, MEA_Uint16 vpls_Ins_Id);



MEA_Status MEA_API_VPLSi_UE_PDN_DL_Create_Entry(MEA_Unit_t               unit,
    MEA_Uint16              vpls_Ins_Id,
    MEA_Uint32              UE_pdn_Id,
    MEA_VPLS_UE_Pdn_dl_dbt  *entry);

MEA_Status MEA_API_VPLSi_UE_PDN_DL_Get_Entry(MEA_Unit_t               unit,
    MEA_Uint16              vpls_Ins_Id,
    MEA_Uint32              UE_pdn_Id,
    MEA_VPLS_UE_Pdn_dl_dbt  *entry);

MEA_Status MEA_API_VPLSi_UE_PDN_DL_Delete_Entry(MEA_Unit_t         unit,
    MEA_Uint16               vpls_Ins_Id,
    MEA_Uint32               UE_pdn_Id);


MEA_Status MEA_API_VPLSi_UE_PDN_DL_GetFirst_Entry(MEA_Unit_t               unit,
                                                  MEA_Uint16   vpls_Ins_Id,
                                                  MEA_Uint32   *UE_pdn_Id,
                                                  MEA_Bool     *o_found);

MEA_Status MEA_API_VPLSi_UE_PDN_DL_GetNext_Entry(MEA_Unit_t               unit,
                                                 MEA_Uint16              vpls_Ins_Id,
                                                 MEA_Uint32              *UE_pdn_Id,
                                                 MEA_Bool     *o_found);


/************************************************************************/
/*  API_TFT                                                             */
/************************************************************************/
#define MEA_TFT_L3_Internal_EtherType_1 0x8902
#define MEA_TFT_L3_Internal_EtherType_2 0x8863 /*0x8864*/
#define MEA_TFT_L3_Internal_EtherType_3 0x88f7 


typedef struct 
{
        MEA_Uint32 valid : 1;
        MEA_Uint32 EtherType : 16;
        MEA_Uint32 pad : 15;

}MEA_TFT_L3_EtheType_dbt;


MEA_Status MEA_API_TFT_L3_Internal_EtherType_Set(MEA_Unit_t unit, MEA_Uint8 index, MEA_TFT_L3_EtheType_dbt *entry);
MEA_Status MEA_API_TFT_L3_Internal_EtherType_Get(MEA_Unit_t unit, MEA_Uint8 index, MEA_TFT_L3_EtheType_dbt *entry);


typedef enum {
    MEA_TFT_TYPE_DISABLE = 0,
    MEA_TFT_TYPE_NO_IP   = 1,
    MEA_TFT_TYPE_IPV4    = 2,
    MEA_TFT_TYPE_IPV6    = 3,  /*Uplink*/
    MEA_TFT_TYPE_IPV6_Source = 4,   /*Downlink*/
	MEA_TFT_TYPE_LAST=5
}MEA_TFT_Type_t;



typedef struct{

    MEA_TFT_Type_t   Type;   
    /*****************************************/
    /* L2 parameter                          */
    /*****************************************/
    MEA_Uint32  L2_enable               : 1;
    MEA_Uint32  L2_Pri                  : 8; /*bitmap to enable priority*/
    

    /******************************************/
    /* L3 parameter                           */
    /******************************************/
    
    

    MEA_Uint32  L3_DSCP_valid    : 1;
    MEA_Uint32  L3_from_DSCP     : 6;
    MEA_Uint32  L3_end_DSCP      : 6;


    

    
    MEA_Uint32  L3_EtherType         : 3; /*profile of EtherType 0:3 [0-D.C 1-0x8892 2-0x8863/8864 3-88f7]      4:7  configure */

    MEA_Uint32  IP_protocol_valid    : 1; 
    MEA_Uint32  IP_protocol          : 8;

       
    MEA_Bool    Dest_IPv4_valid;
    MEA_Uint32  Dest_IPv4;
    MEA_Uint32  Dest_IPv4_mask        :6;

    MEA_Bool    Source_IPv4_valid;
    MEA_Uint32  Source_IPv4;
    MEA_Uint32  Source_IPv4_mask      :6;
    
   
    MEA_Bool   Ipv6_valid;
    MEA_Uint32 Ipv6[4];
    MEA_Uint32 Ipv6_mask             : 8;
    MEA_Uint32 Flow_Label            : 20;
    

    /*****************************************/
    /* L4 parameter                           */
    /*****************************************/
  
    
    MEA_Uint32  L4_Source_port_valid    : 1;
    MEA_Uint32  L4_from_Source_port     : 16;
    MEA_Uint32  L4_end_Source_port      : 16;
   
    MEA_Uint32  L4_dest_valid           : 1;
    MEA_Uint32  L4_from_Dest_port       : 16;
    MEA_Uint32  L4_end_Dest_port        : 16;



    /* Rule  */
    MEA_Uint32  Priority_Rule:5;
    MEA_Uint32  QoS_Index : 3;       /*to uepdnTEID rule   */
    MEA_Uint32  pad1 : 24;

}MEA_TFT_key_t;


typedef struct
{
    MEA_TFT_key_t    TFT_info;

}MEA_TFT_data_dbt;




typedef struct {
    MEA_Bool   en_rule_type;
    MEA_Uint8   rule_type;

}MEA_TFT_entry_data_dbt;

MEA_Status MEA_API_TFT_Create_QoS_Profile(MEA_Unit_t unit, MEA_TFT_t  *tft_profile_Id, MEA_TFT_entry_data_dbt *entry);
MEA_Status MEA_API_TFT_Delete_QoS_Profile(MEA_Unit_t unit, MEA_TFT_t  tft_profile_Id);
MEA_Status MEA_API_TFT_QoS_Profile_IsExist(MEA_Unit_t unit, MEA_TFT_t  tft_profile_Id, MEA_Bool silent, MEA_Bool *exist);



MEA_Status MEA_API_TFT_Set_ClassificationRule(MEA_Unit_t unit, MEA_TFT_t tft_profile_Id, MEA_Uint8 index, MEA_TFT_data_dbt  *entry);
MEA_Status MEA_API_TFT_Get_ClassificationRule(MEA_Unit_t unit, MEA_TFT_t tft_profile_Id, MEA_Uint8 index, MEA_TFT_data_dbt  *entry);


typedef struct
{
    
    MEA_Uint32 mask;
    
 /*********************/   
    MEA_Bool    en_rule_type;
    MEA_Uint32  rule_type : 3;

}MEA_TFT_mask_data_dbt;

MEA_Status MEA_API_TFT_Prof_Mask_Create(MEA_Unit_t unit, MEA_Uint16 *MaskId);
MEA_Status MEA_API_TFT_Prof_Mask_Delete(MEA_Unit_t unit, MEA_Uint16 MaskId);
MEA_Status MEA_API_TFT_Prof_Mask_Set(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_TFT_mask_data_dbt *entry);
MEA_Status MEA_API_TFT_Prof_Mask_Get(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_TFT_mask_data_dbt *entry);
MEA_Status MEA_API_TFT_Prof_Mask_IsExist(MEA_Unit_t unit, MEA_Uint16 MaskId, MEA_Bool silent, MEA_Bool *exist);




typedef struct{
    MEA_Bool   valid;
    
    MEA_Action_Entry_Data_dbt                 *action_params;
    MEA_OutPorts_Entry_dbt                   *OutPorts_Entry;
    MEA_Policer_Entry_dbt                    *Policer_Entry;
    MEA_EgressHeaderProc_Array_Entry_dbt      *EHP_Entry;
}mea_TFT_Action_rule_t;


MEA_Status MEA_API_Create_UEPDN_TEID_ConfigAction(MEA_Unit_t unit, MEA_Uint16 UEPDN_Id, MEA_Uint8 index, mea_TFT_Action_rule_t *entry);
MEA_Status MEA_API_Set_UEPDN_TEID_ConfigAction(MEA_Unit_t unit, MEA_Uint16 UEPDN_Id, MEA_Uint8 index, mea_TFT_Action_rule_t *entry);
MEA_Status MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_Unit_t unit, MEA_Uint16 UEPDN_Id, MEA_Uint8 index, mea_TFT_Action_rule_t *entry);
MEA_Status MEA_API_Delete_UEPDN_TEID_ConfigAction(MEA_Unit_t unit, MEA_Uint16 UEPDN_Id, MEA_Uint8 index);


MEA_Status MEA_API_UEPDN_TFT_TEID_ConfigAction_IsExist(MEA_Unit_t unit_i, MEA_Uint16 UEPDN_Id, MEA_Uint8 index,MEA_Bool  *exist);


typedef  struct {

        ENET_QueueId_t  clusterId;
        MEA_Uint32  en_vp       : 1;
        MEA_Uint32  access_port : 10;
        MEA_Uint32  vpn         : 12;
        MEA_Uint32  force       : 1;




}mea_UEpdn_Handover_dbt;

MEA_Status MEA_API_UEPDN_Set_Handover(MEA_Unit_t unit, MEA_Uint8 index, mea_UEpdn_Handover_dbt *entry);
MEA_Status MEA_API_UEPDN_Get_Handover(MEA_Unit_t unit, MEA_Uint8 index, mea_UEpdn_Handover_dbt *entry);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA SFLOW Info                                             */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef enum
{
    MEA_SFLOW_TIMEOUT_TYPE_NO_TIME = 0,
    MEA_SFLOW_TIMEOUT_TYPE_1       = 1,
    MEA_SFLOW_TIMEOUT_TYPE_2       = 2,
    MEA_SFLOW_TIMEOUT_TYPE_3       = 3

}MEA_Sflow_timeOut_type_t;


typedef struct
{
    MEA_Uint32  packetCount  :14;
    MEA_Uint32  timeOut_type : 2; /*MEA_Sflow_timeOut_type_t*/
    MEA_Uint32  pri_queue    : 3;
    MEA_Uint32  pad          : 13;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

}MEA_Sflow_Prof_dbt;



MEA_Status MEA_API_Sflow_Prof_Create(MEA_Unit_t                   unit_i,
                                     MEA_Sflow_Prof_dbt           *entry_pi,
                                     MEA_Uint16                   *id_io);


MEA_Status MEA_API_Sflow_prof_Set(MEA_Unit_t                     unit_i,
                                  MEA_Uint16                      id_i,
                                  MEA_Sflow_Prof_dbt            *entry_pi);

MEA_Status MEA_API_sflow_prof_IsExist(MEA_Unit_t     unit_i,
    MEA_Uint16 id_i,
    MEA_Bool silent,
    MEA_Bool *exist);

MEA_Status MEA_API_Sflow_prof_Delete(MEA_Unit_t                     unit_i,
    MEA_Uint16                      id_i);

MEA_Status MEA_API_Sflow_prof_Get(MEA_Unit_t                  unit_i,
                                  MEA_Uint16                   id_i,
                                  MEA_Sflow_Prof_dbt    *entry_po);

MEA_Status MEA_API_Sflow_Prof_GetFirst(MEA_Unit_t               unit_i,
    MEA_Uint16                *id_o,
    MEA_Sflow_Prof_dbt               *entry_po, /* Can't be NULL */
    MEA_Bool                 *found_o);


MEA_Status MEA_API_Sflow_Prof_GetNext(MEA_Unit_t                  unit_i,
    MEA_Uint16                   *id_io,
    MEA_Sflow_Prof_dbt       *entry_po, /* Can't be NULL */
    MEA_Bool                    *found_o);



typedef struct
{
    MEA_Uint32  time_Th;         /*msec*/
    MEA_Uint32  dsa_tag_EthType : 16;
    MEA_Uint32  mtuCpu          : 14;
    MEA_Uint32  pad             : 2;
    

}MEA_Sflow_Global_dbt;

MEA_Status MEA_API_Sflow_Set_Global(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry);
MEA_Status MEA_API_Sflow_Get_Global(MEA_Unit_t  unit_i, MEA_Sflow_Global_dbt *entry);





typedef struct
{
    MEA_Bool               valid;
    MEA_Uint16             profId;

}MEA_Sflow_count_dbt;


MEA_Status MEA_API_Sflow_Statistic_Set(MEA_Unit_t unit_i, MEA_Uint32 id_i, MEA_Sflow_count_dbt *entry);

MEA_Status MEA_API_Sflow_Statistic_IsExist(MEA_Unit_t unit_i, MEA_Uint32 id_i, MEA_Bool silent, MEA_Bool *exist);

MEA_Status MEA_API_Sflow_Statistic_Get(MEA_Unit_t unit_i, MEA_Uint32 id_i, MEA_Sflow_count_dbt *entry);
MEA_Status MEA_API_Sflow_Statistic_GetFirst(MEA_Unit_t                unit_i,
    MEA_Uint16                *id_o,
    MEA_Sflow_Prof_dbt        *entry_po, /* Can't be NULL */
    MEA_Bool                 *found_o);

MEA_Status MEA_API_Sflow_Statistic_GetNext(MEA_Unit_t                  unit_i,
    MEA_Uint16               *id_io,
    MEA_Sflow_Prof_dbt       *entry_po, /* Can't be NULL */
    MEA_Bool                 *found_o);

MEA_Status MEA_API_Sflow_Statistic_Delete(MEA_Unit_t unit_i, MEA_Uint32 id_i);




typedef struct {
    MEA_Bool valid;
    ENET_QueueId_t clusterId;
    MEA_Uint8 groupIndex;
}MEA_sflow_queue_t;

MEA_Status MEA_API_Sflow_Set_TargetCluster(MEA_Unit_t  unit_i, MEA_sflow_queue_t *entry);

MEA_Status MEA_API_Sflow_Get_TargetCluster(MEA_Unit_t  unit_i, MEA_sflow_queue_t *entry);

typedef struct {
    MEA_Bool sflow_en;
    MEA_Uint32 src_port;
    MEA_Uint32 pm_id;
    MEA_Uint32 sflow_type;
    MEA_Bool p_to_mp;
    MEA_Uint32 dst_cluster;






}MEA_sflow_Events_dbt;

MEA_Status MEA_API_Sflow_Events(MEA_Unit_t unit, MEA_sflow_Events_dbt   *entry);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA PBB read Info                                          */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


typedef struct {
    MEA_Bool valid;
    
    MEA_MacAddr B_Sa_mac;
    MEA_MacAddr inner_Sa_mac;
    MEA_MacAddr inner_Da_mac; 
    MEA_Uint32 Itag;
    MEA_Uint32 vpn;
    MEA_Uint32 src_port;


}MEA_PBB_Lean_dbt;


MEA_Status MEA_API_PBB_GetLearnInfo(MEA_Unit_t  unit, MEA_PBB_Lean_dbt  *entry);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Limiter Defines                                        */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* Specail limiter id that not allowed to delete or update 
   Note: This limiter use by the driver for limiter with action disable 
         and use as default limiter profile for all service that the user
         not enable the limiter on it */
#define MEA_LIMITER_DONT_CARE 0

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Limiter Typedefs                                       */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


typedef enum {
    MEA_LIMIT_ACTION_DISABLE,
    MEA_LIMIT_ACTION_DISCARD,
    MEA_LIMIT_ACTION_FWD_WITHOUT_LEARNING,
    MEA_LIMIT_ACTION_SEND_TO_CPU,
    MEA_LIMIT_ACTION_TO_ACTION,
    MEA_LIMIT_ACTION_LAST
} MEA_Limit_Action_te;

typedef struct{
    MEA_Uint32                 limit;
    MEA_Limit_Action_te        action_type;
    MEA_Action_Entry_Data_dbt  action_params;
} MEA_Limiter_Entry_dbt;


typedef struct{
    MEA_Uint32                 number_of_used_entries;
} MEA_Limiter_status;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Limiter APIs                                           */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* if io_limiterId == MEA_PLAT_GENERATE_NEW_ID ==>  a new ID will be generated from pool */
/* else (io_limiterId!=MEA_PLAT_GENERATE_NEW_ID) ==> if io_limiterId is not used it will be takken from pool */



MEA_Status MEA_API_Create_Limiter (MEA_Unit_t                           unit,
                                   MEA_Limiter_Entry_dbt               *data,
                                   MEA_Limiter_t                       *io_limiterId);

MEA_Status MEA_API_Set_Limiter    (MEA_Unit_t                          unit,
                                   MEA_Limiter_Entry_dbt               *data,
                                   MEA_Limiter_t                       i_limiterId);


MEA_Status  MEA_API_Get_Limiter    (MEA_Unit_t                      unit,
                                    MEA_Limiter_t                   limiterId,
                                    MEA_Limiter_Entry_dbt           *data_o);

MEA_Status  MEA_API_Get_Limiter_Status    (MEA_Unit_t                      unit,
                                    MEA_Limiter_t                   limiterId,
                                    MEA_Limiter_status             *data_o);


MEA_Status  MEA_API_Delete_Limiter (MEA_Unit_t                      unit,
                                    MEA_Limiter_t                   limiterId);

MEA_Status MEA_API_Delete_all_limiters(MEA_Unit_t unit);


MEA_Status MEA_API_GetFirst_Limiter (MEA_Unit_t     unit,
                                    MEA_Limiter_t   *o_limiterId,
                                    MEA_Bool       *o_found);

MEA_Status MEA_API_GetNext_Limiter  (MEA_Unit_t     unit,
                                     MEA_Limiter_t *o_limiterId , 
                                     MEA_Bool      *o_found);


MEA_Status MEA_API_IsExist_Limiter_ById  (MEA_Unit_t     unit,
                                          MEA_Limiter_t  limiterId, 
                                          MEA_Bool      *exist);


MEA_Bool MEA_API_IsLimiterIdInRange(MEA_Unit_t     unit, 
                                    MEA_Limiter_t  limiterId);

#define MEA_CHECK_LIMITER_IN_RANGE(limiterId) \
    if(!MEA_API_IsLimiterIdInRange(MEA_UNIT_0,limiterId)){ \
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - Limiter Id %d is out of range\n", \
                          __FUNCTION__,limiterId); \
        return MEA_ERROR;    \
    }                    

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA LxCP Defines                                           */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA LxCP Typedefs                                          */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
 
typedef struct{
    MEA_Uint32 direction; /* use defines MEA_DIRECTION_XXX */
} MEA_LxCP_Entry_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA LxCP APIs                                              */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* if io_LxCpId == MEA_PLAT_GENERATE_NEW_ID ==>  a new ID will be generated from pool */
/* else (io_LxCpId!=MEA_PLAT_GENERATE_NEW_ID) ==> if io_LxCpId is not used it will be takken from pool */
MEA_Status MEA_API_Create_LxCP    (MEA_Unit_t                           unit,
                                   MEA_LxCP_Entry_dbt*                  pi_entry,
                                   MEA_LxCp_t                          *io_LxCpId);



MEA_Status  MEA_API_Delete_LxCP     (MEA_Unit_t                      unit,
                                     MEA_LxCp_t                      i_LxCpId);
MEA_Status MEA_API_Delete_all_LxCPs(MEA_Unit_t                  unit); 


MEA_Status MEA_API_Get_LxCP         (MEA_Unit_t          unit,
                                     MEA_LxCp_t          i_LxCpId,
                                     MEA_LxCP_Entry_dbt *po_entry);

MEA_Status MEA_API_GetFirst_LxCP   (MEA_Unit_t          unit,
                                    MEA_LxCp_t         *o_LxCpId,
                                    MEA_LxCP_Entry_dbt *po_entry,
                                    MEA_Bool           *o_found);

MEA_Status MEA_API_GetNext_LxCP     (MEA_Unit_t          unit,
                                     MEA_LxCp_t         *io_LxCpId , 
                                     MEA_LxCP_Entry_dbt *po_entry,
                                     MEA_Bool           *o_found);

MEA_Status MEA_API_IsExist_LxCP_ById     (MEA_Unit_t     unit,
                                          MEA_LxCp_t  i_LxCpId, 
                                          MEA_Bool      *exist);


MEA_Bool MEA_API_IsInRange_LxCP    (MEA_Unit_t     unit, 
                                    MEA_LxCp_t  i_LxCpId);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA LxCp Protocol Typedefs                                 */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


typedef enum {
    MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT = 0,
    MEA_LXCP_PROTOCOL_ACTION_CPU         = 1,
    MEA_LXCP_PROTOCOL_ACTION_ACTION      = 2, 
    MEA_LXCP_PROTOCOL_ACTION_DISCARD     = 3,
    MEA_LXCP_PROTOCOL_ACTION_LAST
} MEA_LxCP_Protocol_Action_te;

typedef enum {
    MEA_LXCP_PROTOCOL_IN_BPDU_0                     =  0, /* 01:80:c2:00:00:00 */
    MEA_LXCP_PROTOCOL_IN_BPDU_1                     =  1, /* 01:80:c2:00:00:01 */
    MEA_LXCP_PROTOCOL_IN_BPDU_2                     =  2, /* 01:80:c2:00:00:02 */ /* (e.g. LACP, OAM, LLDP) , with EtherType 8809 --> EFM 802.1 ah*/
    MEA_LXCP_PROTOCOL_IN_BPDU_3                     =  3, /* 01:80:c2:00:00:03 */ /*Port Level Secure Authentication*/
    MEA_LXCP_PROTOCOL_IN_BPDU_4                     =  4, /* 01:80:c2:00:00:04 */ /*IEEE MAC specific control protocols*/
    MEA_LXCP_PROTOCOL_IN_BPDU_5                     =  5, /* 01:80:c2:00:00:05 */
    MEA_LXCP_PROTOCOL_IN_BPDU_6                     =  6, /* 01:80:c2:00:00:06 */
    MEA_LXCP_PROTOCOL_IN_BPDU_7                     =  7, /* 01:80:c2:00:00:07 */ /*E-LMI EtherType 0x88EE*/
    MEA_LXCP_PROTOCOL_IN_BPDU_8                     =  8, /* 01:80:c2:00:00:08 */
    MEA_LXCP_PROTOCOL_IN_BPDU_9                     =  9, /* 01:80:c2:00:00:09 */
    MEA_LXCP_PROTOCOL_IN_BPDU_10                    = 10, /* 01:80:c2:00:00:0A */
    MEA_LXCP_PROTOCOL_IN_BPDU_11                    = 11, /* 01:80:c2:00:00:0B */
    MEA_LXCP_PROTOCOL_IN_BPDU_12                    = 12, /* 01:80:c2:00:00:0C */
    MEA_LXCP_PROTOCOL_IN_BPDU_13                    = 13, /* 01:80:c2:00:00:0D */
    MEA_LXCP_PROTOCOL_IN_BPDU_14                    = 14, /* 01:80:c2:00:00:0E */
    MEA_LXCP_PROTOCOL_IN_BPDU_15                    = 15, /* 01:80:c2:00:00:0F */
    MEA_LXCP_PROTOCOL_IN_BPDU_16                    = 16, /* 01:80:c2:00:00:10 */
    MEA_LXCP_PROTOCOL_IN_BPDU_MMRP                  = 17, /* 01:80:c2:00:00:20 */
    MEA_LXCP_PROTOCOL_IN_BPDU_MVRP                  = 18, /* 01:80:c2:00:00:21 */
    MEA_LXCP_PROTOCOL_IN_BPDU_34_BPDU_47            = 19, /* 01:80:c2:00:00:22-->2F */
    MEA_LXCP_PROTOCOL_IN_R_APS                      = 20,
    
    MEA_LXCP_PROTOCOL_IN_BFD_CONTROL                = 21, /*MY-IPv4, type UDP, L4 dest port = 3784, L4, [49152 <= source port <= 65535]*/
    MEA_LXCP_PROTOCOL_IN_BFD_ECO_LOOP               = 22, /*IPv4, type UDP, L4 dest port = 3785, L4, [49152 <= source port <= 65535]  !re*/
    MEA_LXCP_PROTOCOL_IN_BFD_ECO_ANALYZER           = 23, /*IPv4, type UDP, L4 dest port = 3785, L4, [49152 <= source port <= 65535]  reg=75*/
    MEA_LXCP_PROTOCOL_IN_BFD_ECO_SW_MANGE           = 24, /*IPv4, type UDP, L4 dest port = 3785, L4, [49152 <= source port <= 65535]  reg=75*/
 
    
    MEA_LXCP_PROTOCOL_IN_IS_IS                      = 25, /*01-80-C2-00-00-(14/15) */
    MEA_LXCP_PROTOCOL_ARP_Replay                    = 26,
    MEA_LXCP_PROTOCOL_BGP                           = 27,/*Packet is IP and IP Protocol=6 (TCP) ,TCP port(L4_Dest_port) = 179*/
    MEA_LXCP_PROTOCOL_802_1X                        = 28, /*EtherType 0x888E */
    MEA_LXCP_PROTOCOL_IN_PIM                        = 29, /* IP_protocol = 103 */
    MEA_LXCP_PROTOCOL_IN_OSPF                       = 30, /* IP_protocol = 89 */
    MEA_LXCP_PROTOCOL_RESERV31                      = 31, /* IPv6 , next header= 58,ICMP message type =130 */
    MEA_LXCP_PROTOCOL_RESERV32                      = 32, /* IPv6 , next header= 58,ICMP message type =143 */
    MEA_LXCP_PROTOCOL_ARP                           = 33,
    MEA_LXCP_PROTOCOL_PPPoE_DISCOVERY               = 34,
    MEA_LXCP_PROTOCOL_PPPoE_ICP_LCP                 = 35,
    MEA_LXCP_PROTOCOL_IGMPoV4                       = 36,
    MEA_LXCP_PROTOCOL_ICMPoV4                       = 37,
    MEA_LXCP_PROTOCOL_IPV4                          = 38,
    MEA_LXCP_PROTOCOL_IPV6                          = 39,
    MEA_LXCP_PROTOCOL_DHCP_SERVER                   = 40, /* IP protocol equal 0x11 (UDP) and destination port equal 67*/
    MEA_LXCP_PROTOCOL_DHCP_CLIENT                   = 41, /* IP protocol equal 0x11 (UDP) and destination port equal 68*/
    MEA_LXCP_PROTOCOL_DNSoIPv4                      = 42, /* DNS - IP Protocol = 6 (TCP) / 17 (UDP)   DstL4Port = 53*/
    MEA_LXCP_PROTOCOL_ICMPoV6                       = 43, /**/
    MEA_LXCP_PROTOCOL_RIP                           = 44,
    MEA_LXCP_PROTOCOL_DHCP_SERVER_IPv6              = 45,
    MEA_LXCP_PROTOCOL_VSTP                          = 46, /* DA_MAC = 01:00:0c:cc:cc:cd*/
    MEA_LXCP_PROTOCOL_CDP_VTP                       = 47, /* DA_MAC = 01:00:0c:cc:cc:cc */
    MEA_LXCP_PROTOCOL_BPDU_TUNNEL                   = 48, /* DA_MAC = 01:00:0c:cd:cd:d0 configure register */
    MEA_LXCP_PROTOCOL_CFM_OAM                       = 49,
    MEA_LXCP_PROTOCOL_IEEE1588_PTP_Event            = 50,
    MEA_LXCP_PROTOCOL_IEEE1588_PTP_General          = 51,
    MEA_LXCP_PROTOCOL_PTP_Primary_over_IPv4         = 52,
    MEA_LXCP_PROTOCOL_PTP_Primary_over_IPv6         = 53, 
    MEA_LXCP_PROTOCOL_PTP_Delay_over_IPv4           = 54,
    MEA_LXCP_PROTOCOL_PTP_Delay_over_IPv6           = 55,
    MEA_LXCP_PROTOCOL_DHCP_CLIENT_IPv6              = 56,
    MEA_LXCP_PROTOCOL_PPPoE_SESSION                 = 57,
    MEA_LXCP_PROTOCOL_VDSL_Vectoring                = 58,
    MEA_LXCP_PROTOCOL_IPSEC                         = 59,
    MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP        = 60,
    MEA_LXCP_PROTOCOL_L2TPoE                        = 61,
    MEA_LXCP_PROTOCOL_DDOS                          = 62, 
    MEA_LXCP_PROTOCOL_OTHER                         = 63,
    MEA_LXCP_PROTOCOL_LAST                          = 64
} MEA_LxCP_Protocol_te;


#define MEA_LXCP_PROTOCOL_STP  MEA_LXCP_PROTOCOL_IN_BPDU_0
/*** GVRP and GMRP PDU MAC per IEEE Std 802.1D - 2004 ***/
#define MEA_LXCP_PROTOCOL_GMRP MEA_LXCP_PROTOCOL_IN_BPDU_MMRP
#define MEA_LXCP_PROTOCOL_GVRP MEA_LXCP_PROTOCOL_IN_BPDU_MVRP

typedef struct{
    MEA_LxCP_Protocol_Action_te   action_type;

    /* use this parameter in case the action is TO_ACTION */
                      
    MEA_Bool                  ActionId_valid;
    MEA_Action_t              ActionId;
    MEA_Bool                  OutPorts_valid;
    MEA_OutPorts_Entry_dbt    OutPorts;
	MEA_Bool                  WinWhiteList;

/*** Save the hw info for warm reset only ****/
    MEA_Uint16 save_hw_xPermision;
   
} MEA_LxCP_Protocol_data_dbt;


typedef struct{
    MEA_LxCP_Protocol_te  protocol;
} MEA_LxCP_Protocol_key_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA LxCP Protocol APIs                                     */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Set_LxCP_Protocol (MEA_Unit_t                          unit,
                                       MEA_LxCp_t                          LxCp_Id_i,
                                      MEA_LxCP_Protocol_key_dbt          *key_pi,
                                      MEA_LxCP_Protocol_data_dbt         *data_pi);


MEA_Status  MEA_API_Get_LxCP_Protocol (MEA_Unit_t                      unit,
                                       MEA_LxCp_t                      LxCp_Id_i,
                                       MEA_LxCP_Protocol_key_dbt      *key_pi,
                                       MEA_LxCP_Protocol_data_dbt     *data_po);

MEA_Status MEA_API_GetFirst_LxCP_Protocol (MEA_Unit_t                  unit_i,
                                           MEA_LxCp_t                  LxCp_Id_i,
                                           MEA_LxCP_Protocol_key_dbt  *key_po,
                                           MEA_LxCP_Protocol_data_dbt *data_po,
                                           MEA_Bool                   *found_o);


MEA_Status MEA_API_GetNext_LxCP_Protocol  (MEA_Unit_t                  unit_i,
                                           MEA_LxCp_t                  LxCp_Id_i,
                                           MEA_LxCP_Protocol_key_dbt  *key_po,
                                           MEA_LxCP_Protocol_data_dbt *data_po,
                                           MEA_Bool                   *found_o);

MEA_Bool  MEA_API_IsSupport_LxCP_Protocol (MEA_LxCP_Protocol_te protocol_i);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA FlowCoSMappingProfile Defines                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_FLOW_COS_MAPPING_MAX_NUM_OF_ITEMS (64)
#define MEA_FLOW_COS_MAPPING_NUM_OF_ITEMS(type) \
    ((type==MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_DSCP) ? 64 : (type==MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_TOS) ? 16 : 8)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA FlowCoSMappingProfile Typedefs                         */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef enum {
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_ETHERTYPE=0,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_INNER_VLAN,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_PRECEDENCE,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_DSCP,
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_TOS, 
    MEA_FLOW_COS_MAPPING_PROFILE_TYPE_LAST
} MEA_FlowCoSMappingProfile_Type_te;

typedef struct {
    MEA_Uint8    valid     : 1;  /* valid entry                               */
    MEA_Uint8    COS       : 3;  /* Priority Queue                            */
    MEA_Uint8    COLOR     : 1;  /* Color input to Policer 0-Green / 1-Yellow */
    MEA_Uint8    pad       : 3;  /* padding                                   */
} MEA_FlowCoSMappingProfile_Item_Entry_dbt;

typedef struct {
    MEA_FlowCoSMappingProfile_Type_te        type;
    MEA_FlowCoSMappingProfile_Item_Entry_dbt item_table[MEA_FLOW_COS_MAPPING_MAX_NUM_OF_ITEMS];
} MEA_FlowCoSMappingProfile_Entry_dbt,*MEA_FlowCoSMappingProfile_Entry_dbp;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA FlowCoSMappingProfile APIs                             */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* Note: id_io can be as input specific value or MEA_PLAT_GENERATE_NEW_ID */
MEA_Status MEA_API_Create_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_pi,
                                     MEA_FlowCoSMappingProfile_Id_t      *id_io);

MEA_Status MEA_API_Delete_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t       id_i);

MEA_Status MEA_API_Set_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t       id_i,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_pi);

MEA_Status MEA_API_Get_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t       id_i,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_po);

MEA_Status MEA_API_GetFirst_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t      *id_o,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_po,
                                     MEA_Bool                            *found_o);

MEA_Status MEA_API_GetNext_FlowCoSMappingProfile_Entry
                                    (MEA_Unit_t                           unit_i,
                                     MEA_FlowCoSMappingProfile_Id_t      *id_io,
                                     MEA_FlowCoSMappingProfile_Entry_dbt *entry_po,
                                     MEA_Bool                            *found_o);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA FlowMarkingMappingProfile Defines                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_FLOW_MARKING_MAPPING_MAX_NUM_OF_ITEMS (64)
#define MEA_FLOW_MARKING_MAPPING_NUM_OF_ITEMS(type) \
    ((type==MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_DSCP) ? 64 : (type==MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_TOS) ? 16 : 8)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA FlowMarkingMappingProfile Typedefs                         */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef enum {
    MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_L2_PRI_ETHERTYPE=0,
    MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_L2_PRI_INNER_VLAN,
    MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN,
    MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_PRECEDENCE,
    MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_DSCP,
    MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_TOS,
    MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_LAST
} MEA_FlowMarkingMappingProfile_Type_te;

typedef struct {
    MEA_Uint8    valid         : 1;  /* valid entry                        */
    MEA_Uint8    L2_PRI        : 3;  /* Stamping L2 Priority   for Editing */
    MEA_Uint8    pad           : 4;  /* padding                            */
} MEA_FlowMarkingMappingProfile_Item_Entry_dbt;

typedef struct {
    MEA_FlowMarkingMappingProfile_Type_te        type;
    MEA_FlowMarkingMappingProfile_Item_Entry_dbt item_table[MEA_FLOW_MARKING_MAPPING_MAX_NUM_OF_ITEMS];
} MEA_FlowMarkingMappingProfile_Entry_dbt,*MEA_FlowMarkingMappingProfile_Entry_dbp;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA FlowMarkingMappingProfile APIs                         */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* Note: id_io can be as input specific value or MEA_PLAT_GENERATE_NEW_ID */
MEA_Status MEA_API_Create_FlowMarkingMappingProfile_Entry
                          (MEA_Unit_t                               unit_i,
                           MEA_FlowMarkingMappingProfile_Entry_dbt *entry_pi,
                           MEA_FlowMarkingMappingProfile_Id_t      *id_io);

MEA_Status MEA_API_Delete_FlowMarkingMappingProfile_Entry
                          (MEA_Unit_t                               unit_i,
                           MEA_FlowMarkingMappingProfile_Id_t       id_i);

MEA_Status MEA_API_Set_FlowMarkingMappingProfile_Entry
                          (MEA_Unit_t                           unit_i,
                           MEA_FlowMarkingMappingProfile_Id_t       id_i,
                           MEA_FlowMarkingMappingProfile_Entry_dbt *entry_pi);

MEA_Status MEA_API_Get_FlowMarkingMappingProfile_Entry
                          (MEA_Unit_t                               unit_i,
                           MEA_FlowMarkingMappingProfile_Id_t       id_i,
                           MEA_FlowMarkingMappingProfile_Entry_dbt *entry_po);

MEA_Status MEA_API_GetFirst_FlowMarkingMappingProfile_Entry
                          (MEA_Unit_t                               unit_i,
                           MEA_FlowMarkingMappingProfile_Id_t      *id_o,
                           MEA_FlowMarkingMappingProfile_Entry_dbt *entry_po,
                           MEA_Bool                                *found_o);

MEA_Status MEA_API_GetNext_FlowMarkingMappingProfile_Entry
                          (MEA_Unit_t                               unit_i,
                           MEA_FlowMarkingMappingProfile_Id_t      *id_o,
                           MEA_FlowMarkingMappingProfile_Entry_dbt *entry_po,
                           MEA_Bool                                *found_o);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Mapping Typedefs                                       */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

typedef enum {
    MEA_MAPPING_TYPE_DEF_PRI,
    MEA_MAPPING_TYPE_L2_PRI,
    MEA_MAPPING_TYPE_ETHERTYPE,
    MEA_MAPPING_TYPE_IPV4_TOS,
    MEA_MAPPING_TYPE_IPV4_PRECEDENCE,
    MEA_MAPPING_TYPE_IPV4_DSCP,
    MEA_MAPPING_TYPE_IPV6_TC,
    MEA_MAPPING_TYPE_LAST
} MEA_Mapping_Type_te;

typedef struct {
    MEA_Mapping_Type_te type;
    union {
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32   l2_pri : 3;
        MEA_Uint32   pad0   : 29;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32   pad0   : 29;
        MEA_Uint32   l2_pri : 3;
#endif
        } l2_pri;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32   ethertype : 16;
        MEA_Uint32   pad0      : 16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32   pad0      : 16;
        MEA_Uint32   ethertype : 16;
#endif
        } ethertype;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32   tos       : 4;
        MEA_Uint32   pad0      : 28;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32   pad0      : 28;
        MEA_Uint32   tos       : 4;
#endif
        } ipv4_tos;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32   precedence : 3;
        MEA_Uint32   pad0       : 29;
#else
        MEA_Uint32   pad0       : 29;
        MEA_Uint32   precedence : 3;
#endif
        } ipv4_precedence;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        ENET_Uint32   dscp      : 6;
        ENET_Uint32   pad0      : 26;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        ENET_Uint32   pad0      : 26;
        ENET_Uint32   dscp      : 6;
#endif
        } ipv4_dscp;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32   tc         : 8;
        MEA_Uint32   pad0       : 24;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32   pad0       : 24;
        MEA_Uint32   tc         : 8;
#endif
        } ipv6_tc;        
    } value;
    MEA_Uint8 weight;    
} MEA_Mapping_Key_dbt;

typedef struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32              classifier_pri_value  : 3;
    MEA_Uint32              pri_queue_value       : 3;
    MEA_Uint32              stamping_pri_value    : 3;
    MEA_Uint32              color_value           : 2;
    MEA_Uint32              classifier_pri_valid  : 1;
    MEA_Uint32              pri_queue_valid       : 1;
    MEA_Uint32              stamping_pri_valid    : 1;
    MEA_Uint32              color_valid           : 1;
    MEA_Uint32              pad0                  : 17;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32              pad0                  : 13;
    MEA_Uint32              color_valid           : 1;
    MEA_Uint32              stamping_pri_valid    : 1;
    MEA_Uint32              pri_queue_valid       : 1;
    MEA_Uint32              classifier_pri_valid  : 1;
    MEA_Uint32              color_value           : 2;
    MEA_Uint32              stamping_pri_value    : 3;
    MEA_Uint32              pri_queue_value       : 3;
    MEA_Uint32              classifier_pri_value  : 3;
#endif
} MEA_Mapping_Data_dbt;

typedef struct {
        MEA_Mapping_Key_dbt   key;
        MEA_Mapping_Data_dbt  data;    
} MEA_Mapping_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < Mapping > APIs                                                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/******************************************************************************

* Function Name:       < MEA_API_Create_Mapping>

* Description:         This function creates a new Mapping entity instance, 

*                      and gives Mapping id handle that can be used later on to 

*                      retrieve this entity instance.

*------------------------------------------------------------------------------- 

* Input Parameters:  unit_i -> identifier of the device.

*                    entry_i ->  pointer to the Mapping structure containing 

*                                  the new Mapping parameters.  

*                    id_o    ->  pointer to MEA_Mapping_t in which the new 

*                                  allocated Mapping id will be set.
*******************************************************************************/
MEA_Status MEA_API_Create_Mapping_Profile    (MEA_Unit_t                unit_i,
                                               MEA_Uint32                  *id_io);



/******************************************************************************

* Function Name: < MEA_API_Delete_Mapping_Profile >

* Description:   This function is responsible for deleting an existing Mapping 

*                Profile instance, by its Mapping ID.

*                      

*------------------------------------------------------------------------------- 

* Input Parameters: unit_i -> identifier of the device.

*                   id_i   -> id of the Mapping Profile instance that needs to be deleted. 

*

* Return values:

*     MEA_OK, the function performed the function correctly.

*     MEA_ERROR, the function did not perform the function.

*

*******************************************************************************/                                        
MEA_Status MEA_API_Delete_Mapping_Profile (MEA_Unit_t                unit_i,
                                           MEA_Uint32               id_i);


MEA_Status MEA_API_GetFirst_Mapping_Profile (MEA_Unit_t       unit,
                                             MEA_Uint32      *profile_id_o,                                           
                                             MEA_Bool        *found_o);


MEA_Status MEA_API_GetNext_Mapping_Profile  (MEA_Unit_t       unit,
                                           MEA_Uint32       profile_id_i,
                                           MEA_Uint32      *profile_id_o,    
                                           MEA_Bool        *found_o);

MEA_Status MEA_API_Get_Mapping_Profile     (MEA_Unit_t       unit,
                                           MEA_Uint32       profile_id_i,
                                           MEA_Bool        *found_o);

/******************************************************************************

* Function Name: < MEA_API_Set_Mapping>

* Description:   This function is responsible for modifying an existing Mapping 

*                 entity instance.                    

*                      

*------------------------------------------------------------------------------- 

* Input Parameters: unit_i  -> identifier of the device.

*                   id_i    -> Mapping id to use for the search of the entity that

*                              needs to be modified.        

*                   entry_i -> pointer to a Mapping structure containing the 

*                              Mapping's  new parameters.

*

* Return values:

*     MEA_OK, the function performed the function correctly.

*     MEA_ERROR, the function did not perform the function.

*

*******************************************************************************/                                          
MEA_Status MEA_API_Add_Mapping_Entry       (MEA_Unit_t                 unit_i,
                                            MEA_Uint32                  profile_id_i,
                                            MEA_Uint32               *entry_id_io,
                                            MEA_Mapping_dbt           *entry_i);


/******************************************************************************

* Function Name: < MEA_API_Remove_Mapping_Entry>

* Description:   This function is responsible for removing a Mapping 

*                 entity instance from a mapping profile.                    

*                      

*------------------------------------------------------------------------------- 

* Input Parameters: unit_i  -> identifier of the device.
*         
*                   profile_id_i -> mapping profile ID from which to remove the mapping entry

*                   entry_id -> id of the mapping entry ID to be removed 

*

* Return values:

*     MEA_OK, the function performed the function correctly.

*     MEA_ERROR, the function did not perform the function.

*

*******************************************************************************/                                          
MEA_Status MEA_API_Remove_Mapping_Entry       (MEA_Unit_t                 unit_i,
                                               MEA_Uint32                  profile_id_i,
                                               MEA_Uint32                entry_id_i);

/******************************************************************************

* Function Name: < MEA_API_Get_Mapping >

* Description:   This function is responsible for retrieving an existing Mapping

*                entity instance by its Mapping id.

*                      

*------------------------------------------------------------------------------- 

* Input Parameters: unit_i  -> identifier of the device.

*                   id_i    -> Mapping id to use for the search of the entity that 

*                              needs to be retrieved.                              

*                   entry_i -> pointer to a Mapping structure containing the 

*                              Mapping's  new parameters

*

* Return values:

*     MEA_OK, the function performed the function correctly.

*     MEA_ERROR, the function did not perform the function.

*

*******************************************************************************/                                      
MEA_Status MEA_API_Get_Mapping_Entry       (MEA_Unit_t                   unit_i,
                                            MEA_Uint32                 profile_id_i,   
                                            MEA_Uint32                  entry_id_i,
                                            MEA_Mapping_dbt             *entry_o,
                                            MEA_Bool                  *found_o);



MEA_Status MEA_API_GetFirst_Mapping_Entry (MEA_Unit_t       unit,
                                           MEA_Uint32       profile_id_i,                                           
                                           MEA_Mapping_dbt *entry_o,
                                           MEA_Uint32      *entry_id_o,
                                           MEA_Bool        *found_o);


MEA_Status MEA_API_GetNext_Mapping_Entry  (MEA_Unit_t     unit,
                                           MEA_Uint32     profile_id_i,
                                           MEA_Uint32     entry_id_i,
                                           MEA_Mapping_dbt *entry_o,
                                           MEA_Uint32      *entry_id_o,
                                           MEA_Bool        *found_o);




#define MEA_CHECK_MAPPING_ENTRY_INDEX_IN_RANGE(index) \
    if(!MEA_API_IsMappingEntryIndexInRange(MEA_UNIT_0,index)){ \
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - Mapping Entry index %d is out of range\n", \
                          __FUNCTION__,index); \
        return MEA_ERROR;    \
    }                    \

MEA_Bool MEA_API_IsMappingEntryIndexInRange(MEA_Unit_t     unit, 
                                            MEA_Uint32     index);


#define MEA_CHECK_MAPPING_PROFILE_INDEX_IN_RANGE(index) \
    if(!MEA_API_IsMappingProfileIndexInRange(MEA_UNIT_0,index)){ \
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - Mapping Profile index %d is out of range\n", \
                          __FUNCTION__,index); \
        return MEA_ERROR;    \
    }                    \

MEA_Bool MEA_API_IsMappingProfileIndexInRange(MEA_Unit_t     unit, 
                                              MEA_Uint32     index);
                         
/************************************************************************/
/*  typedef for Protocol_To_Priority                                    */
/************************************************************************/



typedef struct {
    MEA_Uint32   L2_type :5; /*MEA_PARSING_L2_KEY_t*/
    MEA_Uint32   L3_type :5;
    MEA_Uint32   pad     :22;
}MEA_Protocol_To_pri_key_dbt; 

typedef enum{
MEA_PROTOCOL_LOOK_DEFULT_TYP=0,
MEA_PROTOCOL_LOOK_INNER_TYP=1,
MEA_PROTOCOL_LOOK_OUTER_TYP=2,
MEA_PROTOCOL_LOOK_VALUE_TYP=3,

}MEA_Protorol_look_t;
typedef struct {
     
    MEA_Bool    valid; // valid

    MEA_Uint32  l2_pri_rule     : 2;//3-val 2-outer 1-inner 0-default
    MEA_Uint32  l2_pri          : 3;
    
    MEA_Uint32  cos_rule        : 2;//3-val 2-outer 1-inner 0-default
    MEA_Uint32  cos             : 3;
   
    MEA_Uint32  cls_pri_rule    :2; //3-val 2-outer 1-inner 0-default
    MEA_Uint32  cls_pri         :3;
    MEA_Uint32  col             :1; // 0 green 1 -yellow
    
    MEA_Uint32 IP_pri_rule       :2; // 3 DSCP 2 - TOS 1 Precedence 0 None IP

}MEA_Protocol_To_pri_data_dbt;


MEA_Status MEA_API_Protocol_To_Priority_Mapping_Create(MEA_Unit_t                unit_i,
                                                       MEA_Uint16                  *id_io);

MEA_Status MEA_API_Protocol_To_Priority_Mapping_Set(MEA_Unit_t                unit_i,
                                                    MEA_Uint16                 id_i,
                                                    MEA_Protocol_To_pri_key_dbt   *key,
                                                    MEA_Protocol_To_pri_data_dbt  *data);
MEA_Status MEA_API_Protocol_To_Priority_Mapping_Delete(MEA_Unit_t                unit_i,
                                                       MEA_Uint16                  id_i);

MEA_Status MEA_API_Protocol_To_Priority_Mapping_GetbyKey(MEA_Unit_t                unit_i,
                                                         MEA_Uint16                  id_i,
                                                         MEA_Protocol_To_pri_key_dbt   *key,
                                                         MEA_Protocol_To_pri_data_dbt  *data_o);

MEA_Status MEA_API_Protocol_To_Priority_Mapping_GetFirst_byKey (MEA_Unit_t                     uunit_i,
                                                                MEA_Uint16                     id_i,
                                                                MEA_Protocol_To_pri_key_dbt   *key_o,
                                                                MEA_Protocol_To_pri_data_dbt  *data_o,
                                                                MEA_Bool        *found_o);

MEA_Status MEA_API_Protocol_To_Priority_Mapping_GetNext_byKey (MEA_Unit_t                     unit_i,
                                                               MEA_Uint16                     id_i,                                           
                                                               MEA_Protocol_To_pri_key_dbt   *key_o,
                                                               MEA_Protocol_To_pri_data_dbt  *data_o,
                                                               MEA_Bool        *found_o);

MEA_Status MEA_API_Protocol_To_Priority_Mapping_IsExist(MEA_Unit_t     unit_i,
                                                        MEA_Uint16 id_i,
                                                        MEA_Bool *exist);


/************************* INDEX-  section  3.4 performance monitoring  **********/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*    3.4 performance monitoring and counters.                                   */
/*             3.4.1 RMON counters                                               */
/*             3.4.2 Performance monitor ( see also section 2.4.2)               */
/*             3.4.3 BM Counters                                                 */
/*             3.4.3 Queue counters                                              */
/*             3.4.3 Ingress port counters                                       */
/*                                                                               */
/*                                                                               */
/*********************************************************************************/



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Counters Typedefs                                      */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/* Note: In case the device not support the enhance pm accounting 
         (currently only ENET3000 support the enhance pm)
         the next counters are effected:
         - other_dis_pkts will be always zero
         - yellow/red discard packets count in yellow_dis_pkts ,
           and red_dis_pkts will be always zero.
         - yellow forward packets count in yellow_fwd_pkts.
           (red forward packets are not allowed).
         - yellow forward bytes count in yellow_fwd_bytes.
           (red forward bytes are not allowed).
 */
typedef struct{           

    /* Bytes counters */
    MEA_Counter_t         green_fwd_bytes;
    MEA_Counter_t         green_dis_bytes;
    MEA_Counter_t        yellow_fwd_bytes;
    MEA_Counter_t yellow_or_red_dis_bytes;

    /* Packets counters */
    MEA_Counter_t         green_fwd_pkts ;
    MEA_Counter_t         green_dis_pkts ;
    MEA_Counter_t        yellow_fwd_pkts ;
    MEA_Counter_t        yellow_dis_pkts ;
    MEA_Counter_t           red_dis_pkts ;

    /* Other counters to count drop events that not related policer */
    MEA_Counter_t   other_dis_pkts ;
    MEA_Counter_t   dis_mtu_pkts ;

    MEA_Uint64      rate_green_fwd_pkts;
    MEA_Uint64      rate_green_dis_pkts;
    MEA_Uint64      rate_yellow_fwd_pkts;
    MEA_Uint64      rate_yellow_dis_pkts;
    MEA_Uint64      rate_red_dis_pkts;

    MEA_Uint64      rate_green_fwd_bytes;
    MEA_Uint64      rate_yellow_fwd_bytes;
    MEA_Uint64      rate_yellow_or_red_dis_bytes;
    
    MEA_Uint64      rate_Drop_bytes; //


} MEA_Counters_PM_dbt;  

typedef struct {
    MEA_Counter_t           drop_pkts[3] ;
}MEA_Counters_PM_Bonding_dbt;

#define ENET_CLUSTER_RATE_SEC 5 


typedef struct {
  //MEA_Counter_t MTU_DRP;
   MEA_Uint64    Tx_TotalPacketRate;
  MEA_Counter_t mc_MQS_DRP[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];

  MEA_Counter_t MQS_DRP[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];
  
  MEA_Counter_t forward_packet [ENET_PLAT_QUEUE_NUM_OF_PRI_Q];  /*Egress packet 0*/
  MEA_Bool      nopacket       [ENET_PLAT_QUEUE_NUM_OF_PRI_Q];
  MEA_Counter_t forward_Byte   [ENET_PLAT_QUEUE_NUM_OF_PRI_Q];  /*Egress byte 1*/
  MEA_Counter_t ingerss_packet [ENET_PLAT_QUEUE_NUM_OF_PRI_Q];  /*Egress packet */
  MEA_Counter_t ingerss_Byte   [ENET_PLAT_QUEUE_NUM_OF_PRI_Q];  /*Egress Byte */

 
  MEA_Uint8     count[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];
  MEA_Uint64    Tx_sumPacket[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];
     
  MEA_Uint64    Tx_ratePacket[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];


  time_t        t1[ENET_PLAT_QUEUE_NUM_OF_PRI_Q];
} MEA_Counters_Queue_dbt;


typedef struct{
    MEA_Counter_t L2CP_Discard;
    MEA_Counter_t OAM_Discard;
    MEA_Counter_t Mismatch_Discard;
    MEA_Counter_t Rx_Fifo_Full_Discard;
} MEA_Counters_IngressPort_dbt;


typedef struct{

       MEA_Counter_t Rx_Pkts; 
       MEA_Counter_t Rx_BC_Pkts;
       MEA_Counter_t Rx_MC_Pkts;
       MEA_Counter_t Rx_UC_Pkts;
       MEA_Counter_t Rx_64Octets_Pkts;
       MEA_Counter_t Rx_65to127Octets_Pkts;
       MEA_Counter_t Rx_128to255Octets_Pkts;
       MEA_Counter_t Rx_256to511Octets_Pkts;
       MEA_Counter_t Rx_512to1023Octets_Pkts;
       MEA_Counter_t Rx_1024to1518Octets_Pkts;
       MEA_Counter_t Rx_1519to2047Octets_Pkts;
       MEA_Counter_t Rx_2048toMaxOctets_Pkts;
       MEA_Counter_t Rx_CRC_Err;
       MEA_Counter_t Rx_fragmant_Pkts;
       MEA_Counter_t Rx_Oversize_Bytes;
       MEA_Counter_t Rx_Bytes; 
       MEA_Counter_t Rx_Drop_Bytes; 
       MEA_Counter_t Rx_Error_Pkts;
       
       MEA_Counter_t Rx_Mac_Drop_Pkts;
       MEA_Counter_t Rx_Mac_DropSmallfragment_Pkts;

       MEA_Uint64    Rx_ratePacket;
       MEA_Uint64    Rx_rate;
   /********* TX Counter **********************/
      
       MEA_Counter_t Tx_Pkts; 
       MEA_Counter_t Tx_BC_Pkts;
       MEA_Counter_t Tx_MC_Pkts;
       MEA_Counter_t Tx_UC_Pkts;
       MEA_Counter_t Tx_64Octets_Pkts;
       MEA_Counter_t Tx_65to127Octets_Pkts;
       MEA_Counter_t Tx_128to255Octets_Pkts;
       MEA_Counter_t Tx_256to511Octets_Pkts;
       MEA_Counter_t Tx_512to1023Octets_Pkts;
       MEA_Counter_t Tx_1024to1518Octets_Pkts;
       MEA_Counter_t Tx_1519to2047Octets_Pkts;
       MEA_Counter_t Tx_2048toMaxOctets_Pkts;
       MEA_Counter_t Tx_Underrun_Pkts;
       MEA_Counter_t Tx_Overrun_Pkts;
       MEA_Counter_t Tx_Oversize_Pkts;
       MEA_Counter_t Tx_Oversize_Bytes;
       MEA_Counter_t Tx_CRC_Err;
       MEA_Uint64    Tx_L2CP_Pkts;
       MEA_Counter_t Tx_Drop_Pkts_egr_rule;
       MEA_Counter_t Tx_Bytes;
       MEA_Counter_t Tx_Drop_Bytes;
       MEA_Uint64    Tx_ratePacket;
       MEA_Uint64    Tx_rate;
       
       time_t t1;

} MEA_Counters_RMON_dbt;


typedef struct {
    MEA_Counter_t BMBRL_drop_packets_counters;/*BMBRL=bm_buffers_resource_limition*/
    MEA_Counter_t BMDRL_drop_packets_counters;/*BMDRL=bm_descriptors_resource_limition*/

} MEA_Counters_BM_dbt;


typedef struct {

        MEA_Uint32     pending_broadcast_packets;
        MEA_Uint32     used_buffers             ;
        MEA_Uint32     used_descriptors         ;
   
        struct {
            MEA_Uint32 full;
            MEA_Uint32 empty;
            MEA_Uint32 max_empty_latency;
            MEA_Uint32 max_buffer_latency;
            MEA_Uint32 average_fifo_size;
        } tx_engine;

        struct {
            MEA_Uint32 full;
            MEA_Uint32 max_buffer_latency;
            MEA_Uint32 average_fifo_size;
        } write_fifo;
} MEA_Current_Counters_BM_dbt;


typedef    struct{
    MEA_Counter_t fwd_key_match;
    MEA_Counter_t learn_key_not_match;
    MEA_Counter_t learn_failed;
    
    MEA_Counter_t learn_dynamic;
    /* SW counter*/
    MEA_Counter_t unicast_static_MAC   ;
    MEA_Counter_t multicast_static_MAC;
  

}MEA_Counters_Forwarder_dbt;

typedef struct {
    MEA_Counter_t NP_TX;
    MEA_Counter_t TM_RX;
    MEA_Counter_t ddr_RX;

}MEA_Counters_dbg_ing_dbt;
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Counters APIs                                          */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

/* Add HW all PMs/ports/RMONs/BMs/Forwarder counters to SW db */
MEA_Status MEA_API_Collect_Counters_All         (MEA_Unit_t                unit) ;

MEA_Status MEA_API_Collect_Counters_PMs_Block(MEA_Unit_t  unit, MEA_Uint32 block);
MEA_Status MEA_API_Clear_Counters_PMs_Block(MEA_Unit_t                unit,
    MEA_PmId_t    from_pmId,
    MEA_PmId_t    to_pmId);

MEA_Status MEA_API_Collect_Counters_PMs         (MEA_Unit_t                unit);
MEA_Status MEA_API_Collect_Counters_Queues      (MEA_Unit_t                unit);
MEA_Status MEA_API_Collect_Counters_IngressPorts(MEA_Unit_t                unit);
MEA_Status MEA_API_Collect_Counters_RMONs       (MEA_Unit_t                unit);
MEA_Status MEA_API_Collect_Counters_BMs         (MEA_Unit_t                unit);
MEA_Status MEA_API_Collect_Counters_Forwarder   (MEA_Unit_t                unit);
MEA_Status MEA_API_Collect_Counters_IngressS    (MEA_Unit_t                    unit);

MEA_Status MEA_API_Collect_RMON_Enable(MEA_Unit_t                          unit_i,
                                         MEA_Port_t                        port_i,
                                         MEA_Bool                          enable);

MEA_Bool MEA_API_Collect_RMON_IsEnable(MEA_Unit_t                          unit_i,
                                       MEA_Port_t                          port_i);



MEA_Status MEA_QueueFull_Handler(MEA_Unit_t                    unit, MEA_Bool *enableReInit);

/* Add HW [PM/port/RMON/BM] counters to SW[service/port/RMON/BM] counters db */
MEA_Status MEA_API_Collect_Counters_PM         (MEA_Unit_t                    unit,
                                                MEA_PmId_t                    pmId,
                                                MEA_Counters_PM_dbt*          entry);
MEA_Status MEA_API_Collect_Counters_Queue      (MEA_Unit_t                    unit,
                                                ENET_QueueId_t                queue,
                                                MEA_Counters_Queue_dbt*  entry);
MEA_Status MEA_API_Collect_Counters_IngressPort(MEA_Unit_t                    unit,
                                                MEA_Port_t                    port,
                                                MEA_Counters_IngressPort_dbt* entry);
MEA_Status MEA_API_Collect_Counters_RMON       (MEA_Unit_t                    unit,
                                                MEA_Port_t                    port,
                                                MEA_Counters_RMON_dbt*        entry);
MEA_Status MEA_API_Collect_Counters_BM         (MEA_Unit_t                    unit,
                                                MEA_Counters_BM_dbt*         entry);

MEA_Status MEA_API_Collect_Counters_Ingress    (MEA_Unit_t                    unit,
                                                MEA_Counters_dbg_ing_dbt*         entry);

/*bonding*/
MEA_Status MEA_API_Get_Bonding_PM            (MEA_Unit_t                    unit,
                                              MEA_Uint16                    groupId,
                                               MEA_Counters_PM_Bonding_dbt* entry);
MEA_Status MEA_API_Clear_Bonding_PM(MEA_Unit_t                    unit,         
                                    MEA_Uint16                    groupId);


/* Get PM/port/RMON/BM/Forwarder counters from SW PM/port/RMON/BM/Forwarder counters db */
MEA_Status MEA_API_Get_Counters_PM            (MEA_Unit_t                    unit,
                                               MEA_PmId_t                    pmId,
                                               MEA_Counters_PM_dbt*          entry);
MEA_Status MEA_API_Get_Counters_Queue          (MEA_Unit_t                   unit,
                                               ENET_QueueId_t                queue,
                                               MEA_Counters_Queue_dbt*       entry);
MEA_Status MEA_API_Get_Counters_IngressPort   (MEA_Unit_t                    unit,
                                               MEA_Port_t                    port,
                                               MEA_Counters_IngressPort_dbt* entry);
MEA_Status MEA_API_Get_Counters_RMON          (MEA_Unit_t                    unit,
                                               MEA_Port_t                    port,
                                               MEA_Counters_RMON_dbt*        entry);
MEA_Status MEA_API_Get_Counters_BM            (MEA_Unit_t                    unit,
                                               MEA_Counters_BM_dbt*          entry);
MEA_Status MEA_API_Get_Counters_Forwarder     (MEA_Unit_t                    unit,
                                               MEA_Counters_Forwarder_dbt*   entry);

MEA_Status MEA_API_Get_Counters_Ingress    (MEA_Unit_t                    unit,
                                            MEA_Counters_dbg_ing_dbt*         entry);

typedef struct {
    MEA_Uint32 reg[2];
}MEA_Q_PauseStatus_dbt;


MEA_Status MEA_API_Get_Queues_PauseStatus(MEA_Unit_t unit, MEA_Uint16 Group ,MEA_Q_PauseStatus_dbt *Entry);
typedef struct {
    MEA_Uint32 reg[2];
}MEA_ShaperEligibilityStatus_dbt;

MEA_Status MEA_API_Port_Get_ShaperStatus(MEA_Unit_t unit, MEA_Uint16 Group, MEA_ShaperEligibilityStatus_dbt *Entry);


MEA_Status  MEA_API_Clear_Counters_All(MEA_Unit_t                    unit);
/* Reset all [PMs/ports/RMONs/BM/Forwarder] counters */
MEA_Status MEA_API_Clear_Counters_PMs         (MEA_Unit_t                    unit);
MEA_Status MEA_API_Clear_Counters_Queues      (MEA_Unit_t                    unit);
MEA_Status MEA_API_Clear_Counters_IngressPorts(MEA_Unit_t                    unit);

MEA_Status MEA_API_Clear_Counters_RMONs       (MEA_Unit_t                    unit);
MEA_Status MEA_API_Clear_Counters_BM          (MEA_Unit_t                    unit);
MEA_Status MEA_API_Clear_Counters_Forwarder   (MEA_Unit_t                  unit);
MEA_Status MEA_API_Clear_Counters_IngressS    (MEA_Unit_t                    unit);

/* Reset service/port/RMON counters */
MEA_Status MEA_API_Clear_Counters_PM          (MEA_Unit_t                    unit,
                                               MEA_PmId_t                    pmId);
MEA_Status MEA_API_Clear_Counters_Queue       (MEA_Unit_t                    unit,
                                               ENET_QueueId_t                queue);
MEA_Status MEA_API_Clear_Counters_IngressPort (MEA_Unit_t                    unit,
                                               MEA_Port_t                    port);


MEA_Status MEA_API_Clear_Counters_RMON        (MEA_Unit_t                    unit,
                                               MEA_Port_t                    port);


/* Special function to get if the port is valid for rmon 
   to include also special RMON ports , like generator/analyzer (120) */
MEA_Bool  MEA_API_Get_IsRmonPortValid(MEA_Port_t port,
                                      MEA_Bool   silent);


/**/

typedef struct{           
    MEA_Counter_t     rx_pkt;
    MEA_Counter_t     rx_byte;
    MEA_Counter_t     rx_crc;

}MEA_Counters_Utopia_Rmon_Vsp_dbt;


typedef struct{           
    MEA_Counter_t     rx_pkt;
    MEA_Counter_t     rx_byte;
    MEA_Counter_t     rx_crc;
    
//  MEA_Counter_t     rx_unmatched_L2Type; // per src port
  
    MEA_Counter_t     rx_unmatched_VP_VC;  // per src port

    MEA_Uint64        rate_rx;

	MEA_Counter_t     tx_pkt;     // per src port
    MEA_Counter_t     tx_byte;    // per src port
    
    MEA_Uint64        rate_tx;
} MEA_Counters_Utopia_Rmon_dbt;

typedef struct {
    MEA_Uint16	Queue_Drop_PKT;        // –m packets being dropped per queue
    MEA_Uint32	Queue_Ingress_PKT;       //– packets entering the queue, not counting the drop packets
    MEA_Uint32	Queue_Ingress_Byte;
    MEA_Uint32	Queue_Egress_PKT;
    MEA_Uint32	Queue_Egress_Byte;
    MEA_Uint32	Queue_size_delta_PKT;
    MEA_Uint32	Queue_size_delta_Byte;
} MEA_Counters_Ex_Queue_dbt;

MEA_Status MEA_API_Get_Counters_ExQueue(MEA_Unit_t                   unit,
    ENET_QueueId_t                queue,
    MEA_Counters_Ex_Queue_dbt*       entry);


MEA_Status MEA_API_IsExist_Utopia_RMON_Counter(MEA_Unit_t                      unit,
                                               MEA_Port_t                      port,
                                               MEA_Bool                        *exist);

MEA_Status MEA_API_Collect_Counters_Utopia_RMON(MEA_Unit_t                     unit,
                                                MEA_Port_t                     port,
                                                MEA_Counters_Utopia_Rmon_dbt*  entry);

MEA_Status MEA_API_Collect_Counters_Utopia_RMONs(MEA_Unit_t                     unit);

MEA_Status MEA_API_Get_Counters_Utopia_RMON(MEA_Unit_t                          unit,
                                            MEA_Port_t                          port,
                                            MEA_Counters_Utopia_Rmon_dbt*       entry);

MEA_Status MEA_API_Clear_Counters_Utopia_RMONs(MEA_Unit_t                    unit);

MEA_Status MEA_API_Clear_Counters_Utopia_RMON(MEA_Unit_t                    unit,
                                              MEA_Port_t                    port);

/*--------------------------------------------------------------------*/
typedef struct {
	MEA_Counter_t     Rx_byte;
	MEA_Counter_t     Rx_pkt;


	MEA_Counter_t     Tx_pkt;
	MEA_Counter_t     Tx_byte;


}MEA_Counters_Virtual_Rmon_dbt;

MEA_Status MEA_API_IsExist_Virtual_Rmon(MEA_Unit_t                    unit_i,
	MEA_Virtual_Rmon_t               id_i,
	MEA_Bool                      *exist_o);

MEA_Status MEA_API_Collect_Counters_Virtual_Rmon(MEA_Unit_t             unit,
	MEA_Virtual_Rmon_t             id_i,
	MEA_Counters_Virtual_Rmon_dbt* entry);


MEA_Status MEA_API_Collect_Counters_Virtual_Rmons(MEA_Unit_t unit);


MEA_Status MEA_API_Get_Counters_Virtual_Rmon(MEA_Unit_t                  unit,
										     MEA_Virtual_Rmon_t             id_i,
											 MEA_Counters_Virtual_Rmon_dbt  *entry);

MEA_Status MEA_API_Clear_Counters_Virtual_Rmon(MEA_Unit_t    unit,
											   MEA_Virtual_Rmon_t             id_i);


MEA_Status MEA_API_Clear_Counters_Virtual_Rmons(MEA_Unit_t                unit);




/************************* INDEX-  section 3.5 maintenance ***********************/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*    3.5 maintenance                                                            */
/*             3.5.1 Event Attributes                      (advanced - option)   */
/*             3.5.2 Low level device access               (advanced - option)   */
/*             3.5.3 scheduler                             (advanced - option)   */
/*             3.5.4 policer Tick                          (advanced - option)   */
/*                                                                               */
/*                                                                               */
/*********************************************************************************/


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Events  Defines                                        */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Events      Typedefs                                   */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


typedef struct  
{
	MEA_Uint32 search_key_pri          : 8 ;
    MEA_Uint32 search_key_priType      : 2;
	MEA_Uint32 search_key_src_port     : 10;
	MEA_Uint32 search_key_net_tag      : 20;
    MEA_Uint32 search_key_subType      : 4;
	MEA_Uint32 search_key_l2Type       : 5;
	MEA_Uint32 search_key_net_tag1     :24;
    MEA_Uint32 search_key_net2         : 27;
    MEA_Uint32 search_key_Sid          : 20;


}MEA_if_search_key_dbt;

typedef struct {
    MEA_Uint32  type_IPv4_6        : 3;
    MEA_Uint32  Internal_ethertype : 3;
    MEA_Uint32  DSCP               : 6;
    MEA_Uint32  IP_protocol        : 8;
    MEA_Uint32  L2_pri             : 3;                //[130:128] - .P[3b] - l2 priority                                                                 
    MEA_Uint32  Dest_IPv4;
    MEA_Uint32  Source_IPv4;
    MEA_Uint32  L4_Dest           : 16;
    MEA_Uint32  L4_Source         : 16;
    MEA_Uint32  IPV6[4];
    MEA_Uint32 next_heder          : 8;
    MEA_Uint32 Flow_label          : 20;
    
    MEA_Uint32 prof_rule          : 6;
    MEA_Uint32 UE_id              :14;

    MEA_Uint32  Qos_win         : 3;
    MEA_Uint32  pri_rule_win   : 5;

    

}MEA_if_HPM_event;


typedef union { 
	struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
		//word 0
		MEA_Uint32 ingr_fifo_ovflw_event   : 16; /*0 15*/
		MEA_Uint32 egr_fifo_ovflw_event    : 16; /*16 31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
		MEA_Uint32 egr_fifo_ovflw_event    : 16; /*16 31*/
		MEA_Uint32 ingr_fifo_ovflw_event   : 16; /*0 15*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN    
		//word 1
		MEA_Uint32 egr_fifo_under_flw_event     : 16;
		MEA_Uint32 pad1                         : 16;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
		MEA_Uint32 pad1                         : 16; /*48 63*/
		MEA_Uint32 egr_fifo_under_flw_event     : 16; /*32 47*/
#endif

	}val;
	MEA_Uint32 regs[2];
}MEA_if_tdm_event ;


typedef union {
      struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 Cpld_Ver                             : 3;/*  0.. 2 */
        MEA_Uint32 Cpld_Customer                        : 4; /*  3.. 6 */
        MEA_Uint32 Cpld_fail                            : 1; /*  7..7 */
        MEA_Uint32 Cpld_valid                           : 1; /*  8..8 */
        MEA_Uint32 cpld_read                            : 1; /*9*/
        MEA_Uint32 failed_wrongC                         : 1; /*10*/
        MEA_Uint32 failed_wrongD                         : 1; /*11*/
        MEA_Uint32 Cpld_month                           : 4; /*  12..15 */
        MEA_Uint32 Cpld_year                            : 4; /*  16..19 */
        MEA_Uint32 pad                                  :12; /*  20..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 pad                                  :12; /*  20..31 */
        MEA_Uint32 Cpld_year                            : 4; /*  16..19 */
        MEA_Uint32 Cpld_month                           : 4; /*  12..15 */
        MEA_Uint32 failed_wrongD                        : 1; /*11*/
        MEA_Uint32 failed_wrongC                        : 1; /*10*/
        MEA_Uint32 cpld_read                            : 1; /*9*/
        MEA_Uint32 Cpld_valid                           : 1; /*  8..8 */
        MEA_Uint32 Cpld_fail                            : 1; /*  7..7 */
        MEA_Uint32 Cpld_Customer                        : 4; /*  3.. 6 */
        MEA_Uint32 Cpld_Ver                             : 3; /*  0.. 2 */
#endif

      } val;
      MEA_Uint32 reg[1];
} MEA_Events_System_Entry_dbt;


typedef union {
    
struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 pkt_type     : 2;
    MEA_Uint32 TM2_id       : 12;
    MEA_Uint32 reserved     : 1;
    MEA_Uint32 drop_valid   : 1;
    MEA_Uint32 pad          : 16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad          : 16;
    MEA_Uint32 drop_valid   : 1;
    MEA_Uint32 reserved     : 1;
    MEA_Uint32 TM2_id       : 12;
    MEA_Uint32 pkt_type     : 2;
#endif
    } val;
    MEA_Uint32 reg[1];
}MEA_Events_pol_tm2_dbt;


typedef struct {
    union {
        struct {
            MEA_Uint32 if2bm_egress_ready0;
            MEA_Uint32 if2bm_egress_ready1;
            MEA_Uint32 if2bm_egress_ready2;
            MEA_Uint32 if2bm_egress_ready3;

            MEA_Uint32 if2bm_egress_ready4;
            MEA_Uint32 if2bm_egress_ready5;
            MEA_Uint32 if2bm_egress_ready6;
            MEA_Uint32 if2bm_egress_ready7;

        }regs;
        MEA_Uint32 v[8];
    }if2bm_egr; // offset 15


    union {
        struct {
            MEA_Uint32 if2bm_egress_ready_mask0;
            MEA_Uint32 if2bm_egress_ready_mask1;
            MEA_Uint32 if2bm_egress_ready_mask2;
            MEA_Uint32 if2bm_egress_ready_mask3;

            MEA_Uint32 if2bm_egress_ready_mask4;
            MEA_Uint32 if2bm_egress_ready_mask5;
            MEA_Uint32 if2bm_egress_ready_mask6;
            MEA_Uint32 if2bm_egress_ready_mask7;

        }regs;
        MEA_Uint32 v[8];
    }if2bm_egr_mask; //offset 22
}MEA_egressMacReady_t;


typedef struct{
    union {
    struct {

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 max_clk : 16; /*  0..15 */
        MEA_Uint32 tbl_fail : 7;
        MEA_Uint32 Ack_fail : 1;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 Ack_fail : 1;
        MEA_Uint32 tbl_fail : 7;
        MEA_Uint32 max_clk : 16; /*  0..15 */
#endif
    } val;
     MEA_Uint32 reg[1];
    }info;
} MEA_BM_ack_info;


typedef struct {
    union {
      struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 parser_discard                                   : 1;  /*  0.. 0 */
        MEA_Uint32 discard_min_pkt_size_flow0                       : 1;  /*  1.. 1 */  
        MEA_Uint32 wr0_reserv_bit2                                  : 1;  /*  2.. 2 */
        MEA_Uint32 slice_0_double_SOP_or_SOC                        : 1;  /*  3.. 3 */
        MEA_Uint32 slice_0_double_EOP_or_EOC                        : 1;  /*  4.. 4 */
        MEA_Uint32 slice_1_double_SOP_or_SOC                        : 1;  /*  5.. 5 */
        MEA_Uint32 slice_1_double_EOP_or_EOC                        : 1;  /*  6.. 6 */
        MEA_Uint32 ps2cpu_nxt                                       : 1;  /*  7.. 7 */
        MEA_Uint32 ttl_2_cpu                                        : 1;  /*  8.. 8 */
        MEA_Uint32 service_tbl_no_hit                               : 1;  /*  9.. 9 */
        MEA_Uint32 error_from_if_to_bm                              : 1;  /* 10..10 */
        MEA_Uint32 sw2ef_buff_0_overflow                            : 1;  /* 11..11 */
        MEA_Uint32 sw2ef_buff_1_overflow                            : 1;  /* 12..12 */
        MEA_Uint32 sw2ef_buff_0_underflow                           : 1;  /* 13..13 */
        MEA_Uint32 IF_to_BM_data_FIFO_underflow_1                   : 1; /* 14..14 */
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_1                    : 1; /* 15..15 */
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_1                   : 1; /* 16..16 */
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_1                    : 1; /* 17..17 */
        
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_1                    : 1; /* 18..18 */
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_1                  : 1; /* 19..19 */
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_1                 : 1; /* 20..20 */
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_0                    : 1; /* 21..21 */
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_0                  : 1; /* 22..22 */
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_0                 : 1; /* 23..23 */
        MEA_Uint32 sw2ef_buff_1_underflow                           : 1;  /* 24..24 */
        MEA_Uint32 wr0_reserv_25                                    : 1;  /* 25..25 */
        MEA_Uint32 forwarder_FIFO_wrap_around                       : 1;  /* 26..26 */
        MEA_Uint32 forwarder_tried_write_to_cam_violation           : 1; /* 27..27 */
        MEA_Uint32 forwarder_tried_write_to_cam_static_violation    : 1; /* 28..28 */
        MEA_Uint32 search_engine_drop_mandatory_command             : 1; /* 29..29 */
        MEA_Uint32 search_engine_cam_database_error                 : 1; /* 30..30 */
        MEA_Uint32 psr_send2cpu_root_cls                            : 1; /* 31..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

        MEA_Uint32 psr_send2cpu_root_cls                            : 1; /* 31..31 */
        MEA_Uint32 search_engine_cam_database_error                 : 1; /* 30..30 */
        MEA_Uint32 search_engine_drop_mandatory_command             : 1; /* 29..29 */
        MEA_Uint32 forwarder_tried_write_to_cam_static_violation    : 1; /* 28..28 */
        MEA_Uint32 forwarder_tried_write_to_cam_violation           : 1; /* 27..27 */
        MEA_Uint32 forwarder_FIFO_wrap_around                       : 1;  /* 26..26 */
        MEA_Uint32 utopia1_pos_full                                 : 1;  /* 25..25 */
        MEA_Uint32 sw2ef_buff_1_underflow                           : 1;  /* 24..24 */
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_0                   : 1; /* 23..23 */
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_0                    : 1; /* 22..22 */
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_0                      : 1; /* 21..21 */
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_1                  : 1; /* 20..20 */
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_1                   : 1; /* 19..19 */
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_1                    : 1; /* 18..18 */
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_1                    : 1; /* 17..17 */
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_1                   : 1; /* 16..16 */
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_1                    : 1; /* 15..15 */
        MEA_Uint32 IF_to_BM_data_FIFO_underflow_1                   : 1; /* 14..14 */
        MEA_Uint32 sw2ef_buff_0_underflow                           : 1;  /* 13..13 */
        MEA_Uint32 sw2ef_buff_1_overflow                            : 1;  /* 12..12 */
        MEA_Uint32 sw2ef_buff_0_overflow                            : 1;  /* 11..11 */
        MEA_Uint32 error_from_if_to_bm                              : 1;  /* 10..10 */
        MEA_Uint32 service_tbl_no_hit                               : 1;  /*  9.. 9 */
        MEA_Uint32 ttl_2_cpu                                        : 1;  /*  8.. 8 */
        MEA_Uint32 ps2cpu_nxt                                       : 1;  /*  7.. 7 */
        MEA_Uint32 slice_1_double_EOP_or_EOC                        : 1;  /*  6.. 6 */
        MEA_Uint32 slice_1_double_SOP_or_SOC                        : 1;  /*  5.. 5 */
        MEA_Uint32 slice_0_double_EOP_or_EOC                        : 1;  /*  4.. 4 */
        MEA_Uint32 slice_0_double_SOP_or_SOC                        : 1;  /*  3.. 3 */
        MEA_Uint32 wr0_reserv_bit2                                  : 1;  /*  2.. 2 */
        MEA_Uint32 discard_min_pkt_size_flow0                       : 1;  /*  1.. 1 */  
        MEA_Uint32 parser_discard                                   : 1;  /*  0.. 0 */
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 wr1_reserv_bit0                      : 1; /* 32..32 */
        MEA_Uint32 CPU_learn_to_memory_failed           : 1; /* 33..33 */
        MEA_Uint32 wr1_reserv_bit2                      : 1; /* 34..34 */
        MEA_Uint32 forwarder_fwd_key_not_match          : 1; /* 35..35 */
        MEA_Uint32 forwarder_learn_key_not_match        : 1; /* 36..36 */
        MEA_Uint32 IF_to_BM_data_FIFO_underflow_0         : 1; /* 37..37 */
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_0          : 1; /* 38..38 */
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_0         : 1; /* 39..39 */
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_0          : 1; /* 40..40 */
        MEA_Uint32 ip_fragment_discard_both_Ext_internal  : 1; /* 41..41 */
        MEA_Uint32 sys_sssmii_rx_wr_overflow              :16; /* 42..57 */
        MEA_Uint32 sys_sssmii_rx_wr_underflow_0_5         : 6; /* 58..63 */
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
        MEA_Uint32 sys_sssmii_rx_wr_underflow_0_5       : 6; /* 58..63 */
        MEA_Uint32 sys_sssmii_rx_wr_overflow            :16; /* 42..57 */
        MEA_Uint32 ip_fragment_discard_both_Ext_internal : 1; /* 41..41 */
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_0        : 1; /* 40..40 */
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_0       : 1; /* 39..39 */
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_0        : 1; /* 38..38 */
        MEA_Uint32 IF_to_BM_data_FIFO_underflow_0       : 1; /* 37..37 */
        MEA_Uint32 forwarder_learn_key_not_match        : 1; /* 36..36 */
        MEA_Uint32 forwarder_fwd_key_not_match          : 1; /* 35..35 */
        MEA_Uint32 wr1_reserv_bit2                      : 1; /* 34..34 */
        MEA_Uint32 CPU_learn_to_memory_failed           : 1; /* 33..33 */
        MEA_Uint32 wr1_reserv_bit0                      : 1; /* 32..32 */
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 sys_sssmii_rx_wr_underflow_6_15      :10; /*  0.. 8 */
        MEA_Uint32 sys_sssmii_tx_wr_overflow            :16; /*  9..24 */
        MEA_Uint32 sys_sssmii_tx_wr_underflow_0_5       : 6; /* 25..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
        MEA_Uint32 sys_sssmii_tx_wr_underflow_0_5       : 6; /*90..95*/
        MEA_Uint32 sys_sssmii_tx_wr_overflow            :16; /*74..89*/
        MEA_Uint32 sys_sssmii_rx_wr_underflow_6_15      :10; /*64..73*/
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 sys_sssmii_tx_wr_underflow_6_15      : 10;/*  0.. 9 */ /*96 - 105*/
        MEA_Uint32 sw2hp_ack_err                        : 1; /* 10..10 */ /*106- 106*/
        MEA_Uint32 If_To_BM_ready                       : 1;  /* 11..11 */ /*107- 107*/
        MEA_Uint32 encryption_failed                    : 1; /* 12..12 */ /*108- 108*/
        MEA_Uint32 TTL_is_Zero                          : 1; /* 13..13 */ /*109- 109*/
        MEA_Uint32 wr3_reserv_bit14                     : 1; /* 14..14 */ /*110- 110*/
        MEA_Uint32 wr3_reserv_bit15                     : 1; /* 15..15 */ /*111- 111*/
        MEA_Uint32 fwd_data_path_lrn_collision          : 1; /* 16..16 */ /*112- 112*/
        MEA_Uint32 wr3_reserv_bit17 : 1; /* 17..17 */ /*113- 113*/
        MEA_Uint32 wr3_reserv_bit18 : 1; /* 18..18 */ /*114- 114*/
        MEA_Uint32 IP_defrag_Session_init_done           : 1; /* 19..19*/  /*115 - 115*/
        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_0      : 1; /* 20*/      /*116 - 116*/
        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_1      : 1; /* 21*/      /*117 - 117*/
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_0     : 1; /* 22*/      /*118 - 118*/
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_1     : 1; /* 23*/      /*119 - 119*/
        MEA_Uint32 discard_min_pkt_size_flow1            : 1;  /*24 */     // 120
        
        
        MEA_Uint32 Fifo_daf0                            : 1;  /* 25*/      /*121*/
        MEA_Uint32 Fifo_caf0                            : 1; /* 26*/      /*122*/
        MEA_Uint32 sycBufferSrcError_1                  : 1; /* 27*/      /*123*/
        MEA_Uint32 ihp_ctrl_0_chunk_size_invalid        : 1; /* 28*/      /*124*/
        MEA_Uint32 sycBufferSrcError_0                  : 1; /* 29*/      /*125*/
        MEA_Uint32 ihp_ctrl_1_chunk_size_invalid        : 1; /* 30*/      /*126*/
        MEA_Uint32 forwarderDelall                      : 1; /* 31*/      /*127*/ 
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
        MEA_Uint32 forwarderDelall                      : 1; /* 31*/      /*127*/ 
        MEA_Uint32 ihp_ctrl_1_chunk_size_invalid        : 1; /* 30*/      /*126*/ 
        MEA_Uint32 sycBufferSrcError_0                  : 1; /* 29*/      /*125*/ 
        MEA_Uint32 ihp_ctrl_0_chunk_size_invalid        : 1; /* 28*/      /*124*/ 
        MEA_Uint32 sycBufferSrcError_1                  : 1; /* 27*/      /*123*/
        MEA_Uint32 Fifo_caf0                            : 1; /* 26*/      /*122*/
        MEA_Uint32 Fifo_daf0                            : 1;  /* 25*/      /*121*/
        

        MEA_Uint32 discard_min_pkt_size_flow1           : 1;  /*24 */ // 120
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_1    : 1; /* 23*/      /*119 - 119*/
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_0    : 1; /* 22*/      /*118 - 118*/
        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_1     : 1; /* 21*/      /*117 - 117*/
        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_0     : 1; /* 20*/      /*116 - 116*/
        MEA_Uint32 IP_defrag_Session_init_done          : 1; /* 19..19*/  /*115 - 115*/
        MEA_Uint32 wr3_reserv_bit18                     : 1; /* 18..18 */ /*114- 114*/
        MEA_Uint32 wr3_reserv_bit17                     : 1; /* 17..17 */ /*113- 113*/
        MEA_Uint32 fwd_data_path_lrn_collision          : 1; /* 16..16 */ /*112- 112*/
        MEA_Uint32 wr3_reserv_bit15                     : 1; /* 15..15 */ /*111- 111*/
        MEA_Uint32 wr3_reserv_bit14                     : 1; /* 14..14 */ /*110- 110*/
        MEA_Uint32 TTL_is_Zero                          : 1; /* 13..13 */ /*109- 109*/
        MEA_Uint32 encryption_failed                    : 1; /* 12..12 */ /*108- 108*/
        MEA_Uint32 If_To_BM_ready                       : 1; /* 11..11 */ /*107- 107*/
        MEA_Uint32 sw2hp_ack_err                        : 1; /* 10..10 */ /*106- 106*/
        MEA_Uint32 sys_sssmii_tx_wr_underflow_6_15      :10; /*  0.. 9 */ /*96 - 105*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 init_acl_or_service                  : 1; /* 0 0*/    /*128  */
        MEA_Uint32 sw2ef_buff_2_overflow                : 1; /* 1 1*/    /*129  */
        MEA_Uint32 sw2ef_buff_3_overflow                : 1; /* 2 2*/    /*130  */
        MEA_Uint32 wr4_reserv_bit3                      : 1; /* 3 3*/    /*131  */
        MEA_Uint32 no_hit_on_acl                        : 1; /* 4 4*/    /*132  */
        MEA_Uint32 psr_drop_da_sa                       : 1; /* 5 */    /*133 */
        MEA_Uint32 psr_drop_eth_cs_packet               : 1; /* 6 */    /*134 */

        MEA_Uint32 sw2ef_buff_2_underflow               : 1; /* 7 7 */   /*135*/
        MEA_Uint32 sw2ef_buff_3_underflow               : 1;  /* 8 8*/   /*136 136*/
        MEA_Uint32 Fifo_daf1                            : 1;  /* 9 9*/   /*137 137*/
        MEA_Uint32 Fifo_caf1                            : 1;  /* 10 10*/  /*138 138*/
        MEA_Uint32 prsr_arb_0_underrun                  : 1;  /* 11 11*/  /*139 139*/
        MEA_Uint32 prsr_arb_1_underrun                  : 1;  /* 12 12*/  /*140 140*/
        MEA_Uint32 prsr_arb_0_full                      : 1;  /* 13 13*/  /*141 141*/
        MEA_Uint32 prsr_arb_1_full                      : 1;  /* 14 14*/  /*142 142*/
        MEA_Uint32  ipcs_packet_is_too_shor             : 1;  /* 15 15*/  /*143 143*/
        MEA_Uint32  ethcs_packet_is_too_shor            : 1;  /* 16 16*/  /*144 144*/
        MEA_Uint32  drop_Ipfragmant                     : 1;  /* 17 17*/  /*145 145*/
        MEA_Uint32 drop_limit_exceed                    : 1;  /* 16 16*/  /*146 146*/
        MEA_Uint32 drop_try_to_learn_mc_sa              : 1; /* 17 17*/  /*147 147*/
        MEA_Uint32 drop_update_not_allow                : 1; /* 18 18*/  /*148 148*/
        MEA_Uint32 drop_unknown_fwd_no_match            : 1; /* 19 19*/  /*149 149*/
       

        MEA_Uint32 IP_defrag_class_manage_FIFO_overflow_or_FIFO_Error   : 1; /*150*/
        MEA_Uint32 IP_defrag_no_available_session_ID                    : 1; /*151*/
        MEA_Uint32 IP_defrag_Learn_collision                            : 1; /*152*/
        MEA_Uint32 IP_defrag_Reassembly_session_ID_FIFO_overflow        : 1; /*153*/
        MEA_Uint32 IP_defrag_session_ID_Alloc_twice                     : 1; /*154*/
        MEA_Uint32 IP_defrag_session_ID_Release_twice                   : 1; /*155*/
        MEA_Uint32 IHP_trans_IF_session_ID_FIFO_overflow                : 1; /*156*/



        MEA_Uint32 prsr_arb_2_underrun                  : 1;    /*157*/
        MEA_Uint32 prsr_arb_3_underrun                  : 1;    /*158*/
        MEA_Uint32 prsr_arb_2_full                      : 1;    /*159*/
        
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif


#else
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif
        MEA_Uint32 prsr_arb_2_full                                      : 1; /*159*/
        MEA_Uint32 prsr_arb_3_underrun                                  : 1; /*158*/
        MEA_Uint32 prsr_arb_2_underrun                                  : 1; /*157*/
        MEA_Uint32 IHP_trans_IF_session_ID_FIFO_overflow                : 1; /*156*/
        MEA_Uint32 IP_defrag_session_ID_Release_twice                   : 1; /*155*/
        MEA_Uint32 IP_defrag_session_ID_Alloc_twice                     : 1; /*154*/
        MEA_Uint32 IP_defrag_Reassembly_session_ID_FIFO_overflow        : 1; /*153*/
        MEA_Uint32 IP_defrag_Learn_collision                            : 1; /*152*/
        MEA_Uint32 IP_defrag_no_available_session_ID                    : 1; /*151*/
        MEA_Uint32 IP_defrag_class_manage_FIFO_overflow_or_FIFO_Error   : 1; /*150*/


        MEA_Uint32 drop_unknown_fwd_no_match            : 1; /* 19 19*/  /*149 149*/

        MEA_Uint32 drop_update_not_allow                : 1; /* 18 18*/  /*148 148*/
        MEA_Uint32 drop_try_to_learn_mc_sa              : 1; /* 17 17*/  /*147 147*/
        MEA_Uint32 drop_limit_exceed                    : 1;  /* 16 16*/  /*146 146*/


        MEA_Uint32  drop_Ipfragmant                     : 1;  /* 17 17*/  /*145 145*/
        MEA_Uint32  ethcs_packet_is_too_shor            : 1;  /* 16 16*/  /*144 144*/
        MEA_Uint32  ipcs_packet_is_too_shor             : 1;  /* 15 15*/  /*143 143*/
        MEA_Uint32 prsr_arb_1_full                      : 1;  /* 14 14*/  /*142 142*/
        MEA_Uint32 prsr_arb_0_full                      : 1;  /* 13 13*/  /*141 141*/
        MEA_Uint32 prsr_arb_1_underrun                  : 1;  /* 12 12*/  /*140 140*/
        MEA_Uint32 prsr_arb_0_underrun                  : 1;  /* 11 11*/  /*139 139*/
        MEA_Uint32 Fifo_caf1                            : 1;  /* 10 10*/  /*138 138*/
        MEA_Uint32 Fifo_daf1                            : 1;  /* 9 9*/   /*137 137*/
        MEA_Uint32 sw2ef_buff_3_underflow               : 1;  /* 8 8*/   /*136 136*/
        MEA_Uint32 sw2ef_buff_2_underflow               : 1; /* 7 7 */   /*135*/
        MEA_Uint32 psr_drop_eth_cs_packet               : 1; /* 6 */    /*134 */
        MEA_Uint32 psr_drop_da_sa                       : 1; /* 5 5*/    /*133 */
         
        MEA_Uint32 no_hit_on_acl                        : 1; /* 4 4*/    /*132  */   
        MEA_Uint32 wr4_reserv_bit3                     : 1; /* 3 3*/    /*131  */
        MEA_Uint32 sw2ef_buff_3_overflow               : 1; /* 2 2*/    /*130  */
        MEA_Uint32 sw2ef_buff_2_overflow               : 1; /* 1 1*/    /*129  */
        MEA_Uint32 init_acl_or_service                  : 1; /* 0 0*/    /*128  */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 prsr_arb_3_full           : 1; /*160*/
        MEA_Uint32 slice_2_double_SOP_or_SOC : 1;  /*161*/
        MEA_Uint32 slice_2_double_EOP_or_EOC : 1;   /*162*/
        MEA_Uint32 slice_3_double_SOP_or_SOC : 1;  /*163*/
        MEA_Uint32 slice_3_double_EOP_or_EOC : 1;   /*164*/

        MEA_Uint32 IF_to_BM_data_FIFO_underflow_2 : 1;   /*165*/
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_2  : 1;   /*166*/
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_2 : 1;   /*167*/
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_2  : 1;   /*168*/
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_2  : 1;   /*169*/
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_2 : 1;   /*170*/
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_2 : 1;   /*171*/
        MEA_Uint32 Fifo_daf2                        : 1;   /*172*/
        MEA_Uint32 Fifo_caf2                        : 1;   /*173*/
            


        MEA_Uint32 IF_to_BM_data_FIFO_underflow_3 : 1;   /*174*/
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_3 : 1;   /*175*/
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_3 : 1;   /*176*/
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_3 : 1;   /*177*/
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_3 : 1;   /*178*/
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_3 : 1;   /*179*/
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_3 : 1;   /*180*/
        MEA_Uint32 Fifo_daf3 : 1;   /*181*/
        MEA_Uint32 Fifo_caf3 : 1;   /*182*/


        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_2 : 1;   /*183*/
        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_3 : 1;   /*18*/
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_2 : 1;   /*185*/
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_3 : 1;   /*186*/
        MEA_Uint32 discard_min_pkt_size_flow2        : 1; /*187*/
        MEA_Uint32 discard_min_pkt_size_flow3        : 1; /*188*/
        MEA_Uint32 sycBufferSrcError_2               : 1;/*189*/
        MEA_Uint32 sycBufferSrcError_3              : 1;/*190*/
        MEA_Uint32 ihp_ctrl_2_chunk_size_invalid : 1;/*191*/

#ifdef ARCH64
	MEA_Uint32 pad64_5	    	:32;
#endif
        
#else
#ifdef ARCH64
	MEA_Uint32 pad64_5	    	:32;
#endif
        MEA_Uint32 ihp_ctrl_2_chunk_size_invalid 		: 1;/*191*/
        MEA_Uint32 sycBufferSrcError_3 					: 1;/*190*/
        MEA_Uint32 sycBufferSrcError_2 					: 1;/*189*/
        MEA_Uint32 discard_min_pkt_size_flow3 			: 1; /*188*/
        MEA_Uint32 discard_min_pkt_size_flow2 			: 1; /*187*/
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_3 	: 1;   /*186*/
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_2 	: 1;   /*185*/
        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_3 	: 1;   /*184*/
        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_2 	: 1;   /*183*/
        
        MEA_Uint32 Fifo_caf3 							: 1;   /*182*/
        MEA_Uint32 Fifo_daf3 							: 1;   /*181*/
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_3 	: 1;   /*180*/
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_3 		: 1;   /*179*/
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_3 		: 1;   /*178*/
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_3 		: 1;   /*177*/
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_3 		: 1;   /*176*/
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_3 		: 1;   /*175*/
        MEA_Uint32 IF_to_BM_data_FIFO_underflow_3 		: 1;   /*174*/

        MEA_Uint32 Fifo_caf2 							: 1;   /*173*/
        MEA_Uint32 Fifo_daf2 							: 1;   /*172*/
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_2 	: 1;   /*171*/
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_2 		: 1;   /*170*/
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_2 		: 1;   /*169*/
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_2 		: 1;   /*168*/
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_2 		: 1;   /*167*/
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_2 		: 1;   /*166*/
        MEA_Uint32 IF_to_BM_data_FIFO_underflow_2 		: 1;   /*165*/
        MEA_Uint32 slice_3_double_EOP_or_EOC 			: 1;   /*164*/
        MEA_Uint32 slice_3_double_SOP_or_SOC 			: 1;  /*163*/
        MEA_Uint32 slice_2_double_EOP_or_EOC 			: 1;   /*162*/
        MEA_Uint32 slice_2_double_SOP_or_SOC 			: 1;  /*161*/
        MEA_Uint32 ttt : 1;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 ihp_ctrl_3_chunk_size_invalid 		: 1;/*192*/
        MEA_Uint32  SID_update_not_allow 				: 21;  /*193 213*/
        MEA_Uint32  SID_mc_0_9 							: 10;  /*214 223*/
#ifdef ARCH64
	MEA_Uint32 pad64_6	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_6	    	:32;
#endif
        MEA_Uint32  SID_mc_0_9                   		: 10;  /*214 223*/
        MEA_Uint32  SID_update_not_allow         		: 21;  /*193 213*/
        MEA_Uint32 ihp_ctrl_3_chunk_size_invalid 		: 1;/*192*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  SID_mc_10_20 						: 11;  /*224 234*/
        MEA_Uint32  SID_fwd_unmatch 					: 21;  /*235 255*/
#ifdef ARCH64
	MEA_Uint32 pad64_7	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_7	    	:32;
#endif
        MEA_Uint32  SID_fwd_unmatch 					: 21;  /*235 255*/
        MEA_Uint32  SID_mc_10_20 						: 11;  /*224 234*/
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 slice_4_double_SOP_or_SOC : 1;               //     256
        MEA_Uint32 slice_4_double_EOP_or_EOC : 1;              //     257
        MEA_Uint32 Fifo_caf4 : 1;                               //     258
        MEA_Uint32 Fifo_daf4 : 1;                               //     259
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_4 : 1;        //     260
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_4 : 1;        //     261
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_4 : 1;          //     262
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_4 : 1;         //     263
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_4 : 1;          //     264
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_4 : 1;         //     265
        MEA_Uint32 IF_to_BM_data_FIFO_underflow_4 : 1;         //     266
        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_4 : 1;        //     267
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_4 : 1;       //     268
        MEA_Uint32 discard_min_pkt_size_flow4 : 1;              //     269
        MEA_Uint32 sycBufferSrcError_4 : 1;                    //     270
        MEA_Uint32 ihp_ctrl_4_chunk_size_invalid : 1;           //     271
        MEA_Uint32 sw2ef_buff_4_overflow : 1;                   //     272
        MEA_Uint32 prsr_arb_4_underrun : 1;                    //     273
        MEA_Uint32 prsr_arb_4_full : 1;                       //     274
        MEA_Uint32 sw2ef_buff_4_underflow : 1;                  //     275
        MEA_Uint32 sw2ef_DP_underrun : 1;                    //     276
        MEA_Uint32 sw2ef_DP_overflow : 1;                     //      277
        MEA_Uint32 pad9 : 11;
        
#ifdef ARCH64
	MEA_Uint32 pad64_8	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_8	    	:32;
#endif
        MEA_Uint32 pad9  :11;
        MEA_Uint32 sw2ef_DP_overflow   : 1;                     //      277
        MEA_Uint32 sw2ef_DP_underrun : 1;                    //     276
        MEA_Uint32 sw2ef_buff_4_underflow : 1;                  //     275
        MEA_Uint32 prsr_arb_4_full : 1;                       //     274
        MEA_Uint32 prsr_arb_4_underrun : 1;                    //     273
        MEA_Uint32 sw2ef_buff_4_overflow : 1;                   //     272
        MEA_Uint32 ihp_ctrl_4_chunk_size_invalid : 1;           //     271
        MEA_Uint32 sycBufferSrcError_4 : 1;                    //     270
        MEA_Uint32 discard_min_pkt_size_flow4 : 1;              //     269
        MEA_Uint32 if_to_bm_adapt_q_underflow_flow_4 : 1;       //     268
        MEA_Uint32 if_to_bm_adapt_q_overflow_flow_4 : 1;        //     267
        MEA_Uint32 IF_to_BM_data_FIFO_underflow_4 : 1;         //     266
        MEA_Uint32 IF_to_BM_data_FIFO_overflow_4 : 1;         //     265
        MEA_Uint32 IF_to_BM_ctrl_FIFO_underflow_4 : 1;          //     264
        MEA_Uint32 IF_to_BM_ctrl_FIFO_overflow_4 : 1;         //     263
        MEA_Uint32 IF_to_BM_alr_out_FIFO_error_4 : 1;          //     262
        MEA_Uint32 IF_to_BM_alr_in_FIFO_overflow_4 : 1;        //     261
        MEA_Uint32 IF_to_BM_alr_in_FIFO_underflow_4 : 1;        //     260
        MEA_Uint32 Fifo_daf4 : 1;                               //     259
        MEA_Uint32 Fifo_caf4 : 1;                               //     258
        MEA_Uint32 slice_4_double_EOP_or_EOC : 1;              //     257
        MEA_Uint32 slice_4_double_SOP_or_SOC : 1;               //     256

#endif


      } val;

      MEA_Uint32 regs[9];

    } if_events;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 pause_valid                         : 1;  /*  0.. 0 */
        MEA_Uint32 pause_src_port                      : 10;  /*  1.. 10 */  
        MEA_Uint32 pad                                 :11;  /*  11..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 pad                                 :11;  /*  8..31 */
        MEA_Uint32 pause_src_port                      : 10;  /*  1.. 10 */  
        MEA_Uint32 pause_valid                         : 1;  /*  0.. 0 */
#endif
       } val;
       MEA_Uint32 reg;
    } if_last_pause;

    MEA_Uint32 if_last_llc_machine_output;
    MEA_Uint32 if_last_classifier_search_key[3];
    
    union { 
        struct {

            MEA_Uint32 AAL5_header0;
            MEA_Uint32 AAL5_header1;
            MEA_Uint32 Cell_header0;
            MEA_Uint32 Cell_header1;
#if __BYTE_ORDER == __LITTLE_ENDIAN
            
    MEA_Uint32 ATM_L2_TYPE:5;
    MEA_Uint32 ATM_src_port :7;
    MEA_Uint32 pad          :20;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad          :20;
    MEA_Uint32 ATM_src_port :7;
    MEA_Uint32 ATM_L2_TYPE  :5;
#endif
        }val;
        MEA_Uint32 reg[5];
    }if_last_preparser_search_key; 

    


    



    
    MEA_Uint64 if_last_forwarder_learn_key;
	union { 
		struct {
			MEA_Uint32 temp0;
			MEA_Uint32 temp1;
			MEA_Uint32 temp2;
			MEA_Uint32 temp3;
		}regs;
		MEA_Uint32 v[4];
	}if_last_forwarder_Search_key; 
    
    union { 
        struct {
             MEA_Uint32 temp0;
             MEA_Uint32 temp1;
             MEA_Uint32 temp2;
             MEA_Uint32 temp3;
             MEA_Uint32 temp4;
             MEA_Uint32 temp5;
          }regs;
        MEA_Uint32 v[6];
    }if_last_if_to_bm[8];


    union { 
        struct {
            MEA_Uint32 if2bm_egress_ready0; 
            MEA_Uint32 if2bm_egress_ready1;
            MEA_Uint32 if2bm_egress_ready2;
            MEA_Uint32 if2bm_egress_ready3;

            MEA_Uint32 if2bm_egress_ready4; 
            MEA_Uint32 if2bm_egress_ready5;
            MEA_Uint32 if2bm_egress_ready6;
            MEA_Uint32 if2bm_egress_ready7;

        }regs;
        MEA_Uint32 v[8];
    }if2bm_egr; // offset 15

    union {
        struct {
            MEA_Uint32 if2bm_egress_ready_mask0;
            MEA_Uint32 if2bm_egress_ready_mask1;
            MEA_Uint32 if2bm_egress_ready_mask2;
            MEA_Uint32 if2bm_egress_ready_mask3;

            MEA_Uint32 if2bm_egress_ready_mask4;
            MEA_Uint32 if2bm_egress_ready_mask5;
            MEA_Uint32 if2bm_egress_ready_mask6;
            MEA_Uint32 if2bm_egress_ready_mask7;

        }regs;
        MEA_Uint32 v[8];
    }if2bm_egr_mask; //offset 22

    union {
        struct {
            MEA_Uint32 EVC_search_key;
            MEA_Uint32 EVC_unmatch_key;
            MEA_Uint32 EVC_info;
        }val;
        MEA_Uint32 reg[3];
    }if_EVC_info;

    struct {
	
		MEA_if_search_key_dbt last_search_key;
		MEA_if_search_key_dbt last_unmatch_key;
		
	
	}if_SRV_EXT_info;
   
    struct {
        MEA_Uint32  pkt_is_IP_mc                : 1;
        MEA_Uint32  pkt_is_mpls_cfm             : 1;
        MEA_Uint32  pkt_is_mpls                 : 1;             
        MEA_Uint32  pkt_is_lacp                 : 1;             
        MEA_Uint32  pkt_is_l2cp                 : 1;             
        MEA_Uint32  pkt_is_cfm                  : 1;             
        MEA_Uint32  pkt_is_my_mac               :1;   
        MEA_Uint32  pkt_is_my_mac_ppp           :1;
        MEA_Uint32  pkt_is_IPv6                 :1; 
        MEA_Uint32  pkt_is_IPv4                 :1; 
        MEA_Uint32  IPv6_l4_dport               :16;  
        MEA_Uint32  IPv6_l4_sport               :16; 
        MEA_Uint32  dst_ipv6_xor                :32; 
        MEA_Uint32  src_ipv6_xor                :32; 
        MEA_Uint32  IPv4_l4_dport               :16; 
        MEA_Uint32  IPv4_l4_sport               :16; 
        MEA_Uint32  dst_ipv4                    :32; 
        MEA_Uint32  src_ipv4                    :32; 
        MEA_Uint32  ip_protocol_udp             :1; 
        MEA_Uint32  ip_protocol_tcp             :1; 
        MEA_Uint32  ipv4_ipv6_protocol          :8; 
        MEA_Uint32  ip_header_len               :4; 

       
    }if_L3_L4 ;
     
        
    union { 
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32  ppp_session_id      :16;  // [15:0]
    MEA_Uint32  ppp_protocol        :16;       // [31:16]
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  ppp_protocol        :16;       // [31:16]
    MEA_Uint32  ppp_session_id      :16;  // [15:0]
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 pkt_is_ppp         :1;         //[32]           //pkt_is_pppoe
    MEA_Uint32 pppoe_discovery    :1;          //[33]              
    MEA_Uint32 pkt_is_ppp_lcp     :1;          //[34]
    MEA_Uint32 pad                :29;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
    MEA_Uint32 pad                :29;
    MEA_Uint32 pkt_is_ppp_lcp     :1;          //[34]
    MEA_Uint32 pppoe_discovery    :1;          //[33] 
    MEA_Uint32 pkt_is_ppp         :1;         //[32]           //pkt_is_pppoe
#endif

            
        }regs;
        MEA_Uint32 v[2];
    }if_last_ppp ;

  

    struct {
        MEA_Uint32  errors_exactMatch_fifo                  :1;
        MEA_Uint32  errors_range_fifo                       :1;
        MEA_Uint32  exactMatchService_hit                   :1;
        MEA_Uint32  rangeMatchService_hit                   :1;
        MEA_Uint32  last_service_ctx_id                     :16;
        MEA_Uint32  range_service_last_key_net_tag1;
        MEA_Uint32  range_service_last_key_net_tag2;
        MEA_Uint32  range_service_last_key_pri             : 8;
        MEA_Uint32  range_service_last_key_l2type          :5;
        MEA_Uint32  range_service_Reserv_WIDTH             ;    //12

		
        MEA_Uint32 unmatch_ISv6 : 1;
        MEA_Uint32  unmatch_sig[4];


        MEA_Uint32  unmatch_pri                            :8;
        MEA_Uint32  unmatch_src_port                       :16;
        MEA_Uint32  unmatch_net_tag1;
        MEA_Uint32  unmatch_subType                       : 4;
        MEA_Uint32  unmatch_L2_type                       :5;
        MEA_Uint32  unmatch_net_tag2;
        MEA_Uint32  unmatch_out_src_port                 :16;
		MEA_Uint32  unmatch_last_valid : 1;
		MEA_Uint32  No_match_all_entry : 1;
		MEA_Uint32  match_External_srv : 1; 
       




    }if_service_event;




    union { 
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 drop                         : 1;  /*  0.. 0 */
        MEA_Uint32 sid                          : 31;  /*  1.. 9 */  
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 sid                          : 31;  /*  1.. 9 */  
        MEA_Uint32 drop                         : 1;  /*  0.. 0 */
#endif
        }regs;
        MEA_Uint32 v[1];
    }if_cfm_oam_melevel ;


    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 l2type    : 5;
            MEA_Uint32 l3type    : 5;
            MEA_Uint32 l3_offset : 6;

            MEA_Uint32 pkt_is_bfd_sw_ip_match : 1;  /*16*/
            MEA_Uint32 pkt_is_bfd_my_ip_match : 1; /*17*/
            MEA_Uint32 bfd_echo_analyzer      : 1; /*18*/
            MEA_Uint32 pkt_is_bfd_echo_loop   : 1; /*19*/
            MEA_Uint32 pkt_is_bfd_echo        : 1; /*20*/
            MEA_Uint32 pkt_is_bfd_cntrl       : 1; /*21*/
            MEA_Uint32 last_LXCP_protocol     : 8;
            MEA_Uint32 pkt_is_my_IP           : 1; /*30*/
            MEA_Uint32 pad                    : 1; /*31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad                    : 1; /*31*/
            MEA_Uint32 pkt_is_my_IP           : 1; /*30*/
            MEA_Uint32 last_LXCP_protocol     : 8;
            MEA_Uint32 pkt_is_bfd_cntrl       : 1; /*21*/
            MEA_Uint32 pkt_is_bfd_echo        : 1; /*20*/
            MEA_Uint32 pkt_is_bfd_echo_loop   : 1; /*19*/
            MEA_Uint32 bfd_echo_analyzer      : 1; /*18*/
            MEA_Uint32 pkt_is_bfd_my_ip_match : 1; /*17*/
            MEA_Uint32 pkt_is_bfd_sw_ip_match : 1;  /*16*/
            MEA_Uint32 l3_offset : 6; /*10:15*/
            MEA_Uint32 l3type    : 5; /*5:9*/
            MEA_Uint32 l2type    : 5; /*0:4*/
#endif
        }val;
        MEA_Uint32 reg[1];
    }if_psrser_protocol;


   union {
       struct {
       // word 	0	0	
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32	qxaui_port_12_err 	:4;	/*	0	3	*/	
       MEA_Uint32	qxaui_port_36_err 	:4;	/*	4	7	*/	
       MEA_Uint32	qxaui_port_0_err  	:4;	/*	8	11	*/	
       MEA_Uint32	qxaui_port_24_err 	:4;	/*	12	15	*/	
       MEA_Uint32	qxaui_port_48_err 	:4;	/*	16	19	*/	
       MEA_Uint32	qxaui_port_72_err 	:4;	/*	20	23	*/	
       MEA_Uint32	qxaui_port_125_err	:4;	/*	24	27	*/	
       MEA_Uint32	qxaui_port_126_err	:4;	/*	28	31	*/	

#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
       MEA_Uint32	qxaui_port_126_err	:4;	/*	28	31	*/	
       MEA_Uint32	qxaui_port_125_err	:4;	/*	24	27	*/	
       MEA_Uint32	qxaui_port_72_err 	:4;	/*	20	23	*/	
       MEA_Uint32	qxaui_port_48_err 	:4;	/*	16	19	*/	
       MEA_Uint32	qxaui_port_24_err 	:4;	/*	12	15	*/	
       MEA_Uint32	qxaui_port_0_err  	:4;	/*	8	11	*/	
       MEA_Uint32	qxaui_port_36_err 	:4;	/*	4	7	*/	
       MEA_Uint32	qxaui_port_12_err 	:4;	/*	0	3	*/	

#endif
       // word 	1	32
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32	xaui_port_118_err      	:4;	/*	32	35	*/	
       MEA_Uint32	xaui_port_119_err      	:4;	/*	36	39	*/	
       MEA_Uint32	tx_port_12_under_flow  	:1;	/*	40	40	*/	
       MEA_Uint32	tx_port_36_under_flow  	:1;	/*	41	41	*/	
       MEA_Uint32	tx_port_0_under_flow   	:1;	/*	42	42	*/	
       MEA_Uint32	tx_port_24_under_flow  	:1;	/*	43	43	*/	
       MEA_Uint32	tx_port_48_under_flow  	:1;	/*	44	44	*/	
       MEA_Uint32	tx_port_72_under_flow  	:1;	/*	45	45	*/	
       MEA_Uint32	tx_port_125_under_flow 	:1;	/*	46	46	*/	
       MEA_Uint32	tx_port_126_under_flow 	:1;	/*	47	47	*/	
       MEA_Uint32	tx_port_127_under_flow 	:1;	/*	48	48	*/	
       MEA_Uint32	tx_port_96_under_flow  	:1;	/*	49	49	*/	
       MEA_Uint32	tx_port_97_under_flow  	:1;	/*	50	50	*/	
       MEA_Uint32	tx_port_98_under_flow  	:1;	/*	51	51	*/	
       MEA_Uint32	tx_port_99_under_flow  	:1;	/*	52	52	*/	
       MEA_Uint32	tx_port_100_under_flow 	:1;	/*	53	53	*/	
       MEA_Uint32	tx_port_101_under_flow 	:1;	/*	54	54	*/	
       MEA_Uint32	tx_port_102_under_flow 	:1;	/*	55	55	*/	
       MEA_Uint32	tx_port_103_under_flow 	:1;	/*	56	56	*/	
       MEA_Uint32	tx_port_104_under_flow 	:1;	/*	57	57	*/	
       MEA_Uint32	tx_port_105_under_flow 	:1;	/*	58	58	*/	
       MEA_Uint32	tx_port_106_under_flow 	:1;	/*	59	59	*/	
       MEA_Uint32	tx_port_107_under_flow 	:1;	/*	60	60	*/	
       MEA_Uint32	tx_port_108_under_flow 	:1;	/*	61	61	*/	
       MEA_Uint32	tx_port_109_under_flow 	:1;	/*	62	62	*/	
       MEA_Uint32	tx_port_110_under_flow 	:1;	/*	63	63	*/	
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif

       MEA_Uint32	tx_port_110_under_flow 	:1;/*	63	63	*/	
       MEA_Uint32	tx_port_109_under_flow 	:1;/*	62	62	*/	
       MEA_Uint32	tx_port_108_under_flow 	:1;/*	61	61	*/	
       MEA_Uint32	tx_port_107_under_flow 	:1;/*	60	60	*/	
       MEA_Uint32	tx_port_106_under_flow 	:1;/*	59	59	*/	
       MEA_Uint32	tx_port_105_under_flow 	:1;/*	58	58	*/	
       MEA_Uint32	tx_port_104_under_flow 	:1;/*	57	57	*/	
       MEA_Uint32	tx_port_103_under_flow 	:1;/*	56	56	*/	
       MEA_Uint32	tx_port_102_under_flow 	:1;/*	55	55	*/	
       MEA_Uint32	tx_port_101_under_flow 	:1;/*	54	54	*/	
       MEA_Uint32	tx_port_100_under_flow 	:1;/*	53	53	*/	
       MEA_Uint32	tx_port_99_under_flow  	:1;/*	52	52	*/	
       MEA_Uint32	tx_port_98_under_flow  	:1;/*	51	51	*/	
       MEA_Uint32	tx_port_97_under_flow  	:1;/*	50	50	*/	
       MEA_Uint32	tx_port_96_under_flow  	:1;/*	49	49	*/	
       MEA_Uint32	tx_port_127_under_flow 	:1;/*	48	48	*/	
       MEA_Uint32	tx_port_126_under_flow 	:1;/*	47	47	*/	
       MEA_Uint32	tx_port_125_under_flow 	:1;/*	46	46	*/	
       MEA_Uint32	tx_port_72_under_flow  	:1;/*	45	45	*/	
       MEA_Uint32	tx_port_48_under_flow  	:1;/*	44	44	*/	
       MEA_Uint32	tx_port_24_under_flow  	:1;/*	43	43	*/	
       MEA_Uint32	tx_port_0_under_flow   	:1;/*	42	42	*/	
       MEA_Uint32	tx_port_36_under_flow  	:1;/*	41	41	*/	
       MEA_Uint32	tx_port_12_under_flow  	:1;/*	40	40	*/	
       MEA_Uint32	xaui_port_119_err      	:4;/*	36	39	*/	
       MEA_Uint32	xaui_port_118_err      	:4;/*	32	35	*/	

#endif	
           // word 	2	64
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32	tx_port_111_under_flow	:1;	/*	64	64	*/	
       MEA_Uint32	evnt_Rmon_Rx_packet_FIFO_error              	:7;	/*	65	71	*/	
       MEA_Uint32	rx_port_12_fifo_full  	:1;	/*	72	72	*/	
       MEA_Uint32	rx_port_36_fifo_full  	:1;	/*	73	73	*/	
       MEA_Uint32	rx_port_0_fifo_full   	:1;	/*	74	74	*/	
       MEA_Uint32	rx_port_24_fifo_full  	:1;	/*	75	75	*/	
       MEA_Uint32	rx_port_48_fifo_full  	:1;	/*	76	76	*/	
       MEA_Uint32	rx_port_72_fifo_full  	:1;	/*	77	77	*/	
       MEA_Uint32	rx_port_125_fifo_full 	:1;	/*	78	78	*/	
       MEA_Uint32	rx_port_126_fifo_full 	:1;	/*	79	79	*/	
       MEA_Uint32	rx_port_127_fifo_full 	:1;	/*	80	80	*/	
       MEA_Uint32	rx_port_96_fifo_full  	:1;	/*	81	81	*/	
       MEA_Uint32	rx_port_97_fifo_full  	:1;	/*	82	82	*/	
       MEA_Uint32	rx_port_98_fifo_full  	:1;	/*	83	83	*/	
       MEA_Uint32	rx_port_99_fifo_full  	:1;	/*	84	84	*/	
       MEA_Uint32	rx_port_100_fifo_full 	:1;	/*	85	85	*/	
       MEA_Uint32	rx_port_101_fifo_full 	:1;	/*	86	86	*/	
       MEA_Uint32	rx_port_102_fifo_full 	:1;	/*	87	87	*/	
       MEA_Uint32	rx_port_103_fifo_full 	:1;	/*	88	88	*/	
       MEA_Uint32	rx_port_104_fifo_full 	:1;	/*	89	89	*/	
       MEA_Uint32	rx_port_105_fifo_full 	:1;	/*	90	90	*/	
       MEA_Uint32	rx_port_106_fifo_full 	:1;	/*	91	91	*/	
       MEA_Uint32	rx_port_107_fifo_full 	:1;	/*	92	92	*/	
       MEA_Uint32	rx_port_108_fifo_full 	:1;	/*	93	93	*/	
       MEA_Uint32	rx_port_109_fifo_full 	:1;	/*	94	94	*/	
       MEA_Uint32	rx_port_110_fifo_full 	:1;	/*	95	95	*/	

#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
       MEA_Uint32	rx_port_110_fifo_full 	:1;	/*	95	95	*/	
       MEA_Uint32	rx_port_109_fifo_full 	:1;	/*	94	94	*/	
       MEA_Uint32	rx_port_108_fifo_full 	:1;	/*	93	93	*/	
       MEA_Uint32	rx_port_107_fifo_full 	:1;	/*	92	92	*/	
       MEA_Uint32	rx_port_106_fifo_full 	:1;	/*	91	91	*/	
       MEA_Uint32	rx_port_105_fifo_full 	:1;	/*	90	90	*/	
       MEA_Uint32	rx_port_104_fifo_full 	:1;	/*	89	89	*/	
       MEA_Uint32	rx_port_103_fifo_full 	:1;	/*	88	88	*/	
       MEA_Uint32	rx_port_102_fifo_full 	:1;	/*	87	87	*/	
       MEA_Uint32	rx_port_101_fifo_full 	:1;	/*	86	86	*/	
       MEA_Uint32	rx_port_100_fifo_full 	:1;	/*	85	85	*/	
       MEA_Uint32	rx_port_99_fifo_full  	:1;	/*	84	84	*/	
       MEA_Uint32	rx_port_98_fifo_full  	:1;	/*	83	83	*/	
       MEA_Uint32	rx_port_97_fifo_full  	:1;	/*	82	82	*/	
       MEA_Uint32	rx_port_96_fifo_full  	:1;	/*	81	81	*/	
       MEA_Uint32	rx_port_127_fifo_full 	:1;	/*	80	80	*/	
       MEA_Uint32	rx_port_126_fifo_full 	:1;	/*	79	79	*/	
       MEA_Uint32	rx_port_125_fifo_full 	:1;	/*	78	78	*/	
       MEA_Uint32	rx_port_72_fifo_full  	:1;	/*	77	77	*/	
       MEA_Uint32	rx_port_48_fifo_full  	:1;	/*	76	76	*/	
       MEA_Uint32	rx_port_24_fifo_full  	:1;	/*	75	75	*/	
       MEA_Uint32	rx_port_0_fifo_full   	:1;	/*	74	74	*/	
       MEA_Uint32	rx_port_36_fifo_full  	:1;	/*	73	73	*/	
       MEA_Uint32	rx_port_12_fifo_full  	:1;	/*	72	72	*/	
       MEA_Uint32	evnt_Rmon_Rx_packet_FIFO_error               	:7;	/*	65	71	*/	
       MEA_Uint32	tx_port_111_under_flow	:1	;	/*	64	64	*/	

#endif	
       // word 	3	96
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32	rx_port_111_fifo_full       :1;	/*	96	96	*/	
       MEA_Uint32	evnt_Rmon_Tx_packet_FIFO_error                    :7;	/*	97	103	*/	
       MEA_Uint32	rx_xaui_port_118_fifo_full	:1;/*	104	104	*/	
       MEA_Uint32	tx_xaui_port_118_fifo_full 	:1;/*	105	105	*/	
       MEA_Uint32	rx_xaui_port_119_fifo_full 	:1;/*	106	106	*/	
       MEA_Uint32	tx_xaui_port_119_fifo_full 	:1;/*	107	107	*/	
       MEA_Uint32	reserv_HW                   :1;	/*	108	108	*/	
       MEA_Uint32	sys_rx12_flush_small        :1;/*	109	109	*/	
       MEA_Uint32	sys_rx36_flush_small        :1;/*	110	110	*/	
       MEA_Uint32	sys_rx0_flush_small        	:1;/*	111	111	*/	
       MEA_Uint32	sys_rx24_flush_small       	:1;/*	112	112	*/	
       MEA_Uint32	sys_rx48_flush_small       	:1;/*	113	113	*/	
       MEA_Uint32	sys_rx72_flush_small       	:1;/*	114	114	*/	
       MEA_Uint32	sys_rx125_flush_small      	:1;	/*	115	115	*/	
       MEA_Uint32	sys_rx126_flush_small      	:1;	/*	116	116	*/	
       MEA_Uint32	sys_rx127_flush_small      	:1;	/*	117	117	*/	
       MEA_Uint32	sys_rx96_flush_small       	:1;	/*	118	118	*/	
       MEA_Uint32	sys_rx97_flush_small       	:1;	/*	119	119	*/	
       MEA_Uint32	sys_rx98_flush_small       	:1;	/*	120	120	*/	
       MEA_Uint32	sys_rx99_flush_small       	:1;	/*	121	121	*/	
       MEA_Uint32	sys_rx100_flush_small      	:1;	/*	122	122	*/	
       MEA_Uint32	sys_rx101_flush_small      	:1;	/*	123	123	*/	
       MEA_Uint32	sys_rx102_flush_small      	:1;	/*	124	124	*/	
       MEA_Uint32	sys_rx103_flush_small      	:1;	/*	125	125	*/	
       MEA_Uint32	sys_rx104_flush_small      	:1;	/*	126	126	*/	
       MEA_Uint32	sys_rx105_flush_small      	:1;	/*	127	127	*/	

#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif

       MEA_Uint32	sys_rx105_flush_small       :1; /*	127	127	*/	
       MEA_Uint32	sys_rx104_flush_small       :1; /*	126	126	*/	
       MEA_Uint32	sys_rx103_flush_small       :1; /*	125	125	*/	
       MEA_Uint32	sys_rx102_flush_small       :1; /*	124	124	*/	
       MEA_Uint32	sys_rx101_flush_small       :1; /*	123	123	*/	
       MEA_Uint32	sys_rx100_flush_small       :1; /*	122	122	*/	
       MEA_Uint32	sys_rx99_flush_small        :1; /*	121	121	*/	
       MEA_Uint32	sys_rx98_flush_small        :1; /*	120	120	*/	
       MEA_Uint32	sys_rx97_flush_small        :1; /*	119	119	*/	
       MEA_Uint32	sys_rx96_flush_small        :1; /*	118	118	*/	
       MEA_Uint32	sys_rx127_flush_small       :1; /*	117	117	*/	
       MEA_Uint32	sys_rx126_flush_small       :1; /*	116	116	*/	
       MEA_Uint32	sys_rx125_flush_small       :1; /*	115	115	*/	
       MEA_Uint32	sys_rx72_flush_small        :1; /*	114	114	*/	
       MEA_Uint32	sys_rx48_flush_small        :1; /*	113	113	*/	
       MEA_Uint32	sys_rx24_flush_small       	:1; /*	112	112	*/	
       MEA_Uint32	sys_rx0_flush_small       	:1; /*	111	111	*/	
       MEA_Uint32	sys_rx36_flush_small        :1;/*	110	110	*/	
       MEA_Uint32	sys_rx12_flush_small        :1;/*	109	109	*/	
       MEA_Uint32	reserv_HW                   :1; /*	108	108	*/
       MEA_Uint32	tx_xaui_port_119_fifo_full 	:1; /*	107	107	*/	
       MEA_Uint32	rx_xaui_port_119_fifo_full 	:1; /*	106	106	*/	
       MEA_Uint32	tx_xaui_port_118_fifo_full 	:1; /*	105	105	*/	
       MEA_Uint32	rx_xaui_port_118_fifo_full	:1; /*	104	104	*/	
       MEA_Uint32	evnt_Rmon_Tx_packet_FIFO_error                    :7; /*	97	103	*/	
       MEA_Uint32	rx_port_111_fifo_full       :1; /*	96	96	*/	


#endif
       // word 	4	128
#if __BYTE_ORDER == __LITTLE_ENDIAN	
       MEA_Uint32	sys_rx106_flush_small       	:1; /*	128	128	*/	
       MEA_Uint32	sys_rx107_flush_small       	:1; /*	129	129	*/	
       MEA_Uint32	sys_rx108_flush_small       	:1; /*	130	130	*/	
       MEA_Uint32	sys_rx109_flush_small       	:1; /*	131	131	*/	
       MEA_Uint32	sys_rx110_flush_small       	:1; /*	132	132	*/	
       MEA_Uint32	sys_rx111_flush_small       	:1; /*	133	133	*/	
       MEA_Uint32	pad_w4                          :25; /*	134	158	*/
       MEA_Uint32   IL_TX_mac_FIFO_full             :1;  /*159*/
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif
       
#else
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif
       MEA_Uint32   IL_TX_mac_FIFO_full             :1;  /*159*/
       MEA_Uint32	pad_w4                          :25; /*	134	158	*/	
       MEA_Uint32	sys_rx111_flush_small       	:1; /*	133	133	*/	
       MEA_Uint32	sys_rx110_flush_small       	:1; /*	132	132	*/	
       MEA_Uint32	sys_rx109_flush_small       	:1; /*	131	131	*/	
       MEA_Uint32	sys_rx108_flush_small       	:1; /*	130	130	*/	
       MEA_Uint32	sys_rx107_flush_small       	:1; /*	129	129	*/	
       MEA_Uint32	sys_rx106_flush_small       	:1; /*	128	128	*/	

#endif
       // word 	5	160	
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  IL_TX_mac_overflow              :1;  /*160*/
        MEA_Uint32  IL_Reserved_1                   :1; /*161*/
        MEA_Uint32  IL_RX_mac_FIFO_under_run        :1; /*162*/
        MEA_Uint32  IL_RX_mac_FIFO_full             :1; /*163*/
        MEA_Uint32  IL_RX_mac_FIFO_over_flow        :1; /*164*/
        MEA_Uint32  IL_RX_mac_blk_packet            :1; /*165*/
        MEA_Uint32  IL_Reserved_2                   :1; /*166*/
        MEA_Uint32  IL_Reserved_3                   :1; /*167*/
        MEA_Uint32  xmac0rx_sop                     :1; /*168 168*/
        MEA_Uint32  xmac0rx_eop                     :1; /*169 169*/
        MEA_Uint32  xmac1rx_sop                     :1; /*170 170*/
        MEA_Uint32  xmac1rx_eop                     :1; /*171 171*/
        MEA_Uint32  xmac2rx_sop                     :1; /*172 172*/
        MEA_Uint32  xmac2rx_eop                     :1; /*173 173*/
        MEA_Uint32  xmac3rx_sop                     :1; /*174 174*/
        MEA_Uint32  xmac3rx_eop                     :1; /*175 175*/
        MEA_Uint32   pad_w5_1                       : 1; /*176 176*/
        MEA_Uint32  rmon_rx_drop_err                : 1; /*177 177*/
        MEA_Uint32   pad_w5                         : 6; /*	178	183	*/	
        MEA_Uint32  rx_xoff_reg_0                   :1; /*	184	184	*/	
        MEA_Uint32  rx_xoff_reg_12                  :1; /*	185	185	*/	
        MEA_Uint32  rx_xoff_reg_24                  :1; /*	186	186	*/	
        MEA_Uint32  rx_xoff_reg_36                  :1; /*	187	187	*/	
        MEA_Uint32  rx_xoff_reg_48                  :1; /*	188	188	*/	
        MEA_Uint32  rx_xoff_reg_72                  :1; /*	189	189	*/	
        MEA_Uint32  rx_xoff_reg_96                  :1; /*	190	190	*/	
        MEA_Uint32  rx_xoff_reg_97                  :1; /*	191	191	*/	
#ifdef ARCH64
	MEA_Uint32 pad64_5	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_5	    	:32;
#endif
       MEA_Uint32   rx_xoff_reg_97                  :1; /*	191	191	*/	
       MEA_Uint32   rx_xoff_reg_96                  :1; /*	190	190	*/	
       MEA_Uint32   rx_xoff_reg_72                  :1; /*	189	189	*/	
       MEA_Uint32   rx_xoff_reg_48                  :1; /*	188	188	*/	
       MEA_Uint32   rx_xoff_reg_36                  :1; /*	187	187	*/	
       MEA_Uint32   rx_xoff_reg_24                  :1; /*	186	186	*/	
       MEA_Uint32   rx_xoff_reg_12                  :1; /*	185	185	*/	
       MEA_Uint32   rx_xoff_reg_0                   :1; /*	184	184	*/	
       MEA_Uint32   pad_w5                          :6; /*	178	183	*/
       MEA_Uint32  rmon_rx_drop_err                 : 1; /*177 177*/
       MEA_Uint32   pad_w5_1                        : 1; /*176 176*/
       MEA_Uint32   xmac3rx_eop                     :1; /*175 175*/
       MEA_Uint32   xmac3rx_sop                     :1; /*174 174*/
       MEA_Uint32   xmac2rx_eop                     :1; /*173 173*/
       MEA_Uint32   xmac2rx_sop                     :1; /*172 172*/
       MEA_Uint32   xmac1rx_eop                     :1; /*171 171*/
       MEA_Uint32   xmac1rx_sop                     :1; /*170 170*/
       MEA_Uint32   xmac0rx_eop                     :1; /*169 169*/
       MEA_Uint32   xmac0rx_sop                     :1; /*168 168*/
       MEA_Uint32   IL_Reserved_3                  :1; /*167*/
       MEA_Uint32   IL_Reserved_2                  :1; /*166*/
       MEA_Uint32   IL_RX_mac_blk_packet           :1; /*165*/
       MEA_Uint32   IL_RX_mac_FIFO_over_flow       :1; /*164*/
       MEA_Uint32   IL_RX_mac_FIFO_full            :1; /*163*/
       MEA_Uint32   IL_RX_mac_FIFO_under_run       :1; /*162*/
       MEA_Uint32   IL_Reserved_1                  :1; /*161*/
       MEA_Uint32   IL_TX_mac_overflow             :1; /*160*/

#endif
       // word 	6	192
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32	rx_xoff_reg_98              :1 ;	/*	192	192	*/	
       MEA_Uint32	rx_xoff_reg_99              :1; /*	193	193	*/	
       MEA_Uint32	rx_xoff_reg_100             :1; /*	194	194	*/	
       MEA_Uint32	rx_xoff_reg_101             :1; /*	195	195	*/	
       MEA_Uint32	rx_xoff_reg_102             :1; /*	196	196	*/	
       MEA_Uint32	rx_xoff_reg_103             :1; /*	197	197	*/	
       MEA_Uint32	rx_xoff_reg_104             :1; /*	198	198	*/	
       MEA_Uint32	rx_xoff_reg_105             :1; /*	199	199	*/	
       MEA_Uint32	rx_xoff_reg_106             :1; /*	200	200	*/	
       MEA_Uint32	rx_xoff_reg_107             :1; /*	201	201	*/	
       MEA_Uint32	rx_xoff_reg_108             :1; /*	202	202	*/	
       MEA_Uint32	rx_xoff_reg_109             :1; /*	203	203	*/	
       MEA_Uint32	rx_xoff_reg_110             :1; /*	204	204	*/	
       MEA_Uint32	rx_xoff_reg_111             :1; /*	205	205	*/	
       MEA_Uint32	rx_xoff_reg_125             :1; /*	206	206	*/	
       MEA_Uint32	rx_xoff_reg_126             :1; /*	207	207	*/	
       MEA_Uint32	gbe_118_rx_fifo_full        :1; /*	208	208	*/	
       MEA_Uint32	gbe_119_rx_fifo_full        :1; /*	209	209	*/	
       MEA_Uint32	gbe_118_rx_sys_flush_small  :1; /*	210	210	*/	
       MEA_Uint32	gbe_119_rx_sys_flush_small  :1; /*	211	211	*/	
       MEA_Uint32	gbe_118_rx_underrun         :1; /*	212	212	*/	
       MEA_Uint32	gbe_119_rx_underrun         :1; /*	213	213	*/	
       MEA_Uint32	tx_port_92_under_flow       :1; /*	214	214	*/	
       MEA_Uint32	tx_port_93_under_flow       :1; /*	215	215	*/	
       MEA_Uint32	tx_port_94_under_flow       :1; /*	216	216	*/	
       MEA_Uint32	tx_port_95_under_flow       :1; /*	217	217	*/	
       MEA_Uint32	rx_port_92_fifo_full        :1; /*	218	218	*/	
       MEA_Uint32	rx_port_93_fifo_full        :1; /*	219	219	*/	
       MEA_Uint32	rx_port_94_fifo_full        :1; /*	220	220	*/	
       MEA_Uint32	rx_port_95_fifo_full        :1; /*	221	221	*/	
       MEA_Uint32	sys_rx92_flush_small        :1; /*	222	222	*/	
       MEA_Uint32	sys_rx93_flush_small        :1; /*	223	223	*/	
#ifdef ARCH64
	MEA_Uint32 pad64_6	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_6	    	:32;
#endif
       MEA_Uint32	sys_rx93_flush_small        :1; /*	223	223	*/	
       MEA_Uint32	sys_rx92_flush_small        :1; /*	222	222	*/	
       MEA_Uint32	rx_port_95_fifo_full        :1; /*	221	221	*/	
       MEA_Uint32	rx_port_94_fifo_full        :1; /*	220	220	*/	
       MEA_Uint32	rx_port_93_fifo_full        :1; /*	219	219	*/	
       MEA_Uint32	rx_port_92_fifo_full        :1; /*	218	218	*/	
       MEA_Uint32	tx_port_95_under_flow       :1; /*	217	217	*/	
       MEA_Uint32	tx_port_94_under_flow       :1; /*	216	216	*/	
       MEA_Uint32	tx_port_93_under_flow       :1; /*	215	215	*/	
       MEA_Uint32	tx_port_92_under_flow       :1; /*	214	214	*/	
       MEA_Uint32	gbe_119_rx_underrun         :1; /*	213	213	*/	
       MEA_Uint32	gbe_118_rx_underrun         :1; /*	212	212	*/	
       MEA_Uint32	gbe_119_rx_sys_flush_small  :1; /*	211	211	*/	
       MEA_Uint32	gbe_118_rx_sys_flush_small  :1; /*	210	210	*/	
       MEA_Uint32	gbe_119_rx_fifo_full	    :1; /*	209	209	*/	
       MEA_Uint32	gbe_118_rx_fifo_full	    :1; /*	208	208	*/	
       MEA_Uint32	rx_xoff_reg_126             :1; /*	207	207	*/	
       MEA_Uint32	rx_xoff_reg_125             :1; /*	206	206	*/	
       MEA_Uint32	rx_xoff_reg_111             :1; /*	205	205	*/	
       MEA_Uint32	rx_xoff_reg_110             :1; /*	204	204	*/	
       MEA_Uint32	rx_xoff_reg_109             :1; /*	203	203	*/	
       MEA_Uint32	rx_xoff_reg_108             :1; /*	202	202	*/	
       MEA_Uint32	rx_xoff_reg_107             :1; /*	201	201	*/	
       MEA_Uint32	rx_xoff_reg_106             :1; /*	200	200	*/	
       MEA_Uint32	rx_xoff_reg_105             :1; /*	199	199	*/	
       MEA_Uint32	rx_xoff_reg_104             :1; /*	198	198	*/	
       MEA_Uint32	rx_xoff_reg_103             :1; /*	197	197	*/	
       MEA_Uint32	rx_xoff_reg_102             :1; /*	196	196	*/	
       MEA_Uint32	rx_xoff_reg_101             :1; /*	195	195	*/	
       MEA_Uint32	rx_xoff_reg_100             :1; /*	194	194	*/	
       MEA_Uint32	rx_xoff_reg_99              :1; /*	193	193	*/	
       MEA_Uint32	rx_xoff_reg_98              :1; /*	192	192	*/	

#endif
       // word 	7	224
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32	sys_rx94_flush_small        :1; /*	224	224	*/	
       MEA_Uint32	sys_rx95_flush_small        :1; /*	225	225	*/	
       MEA_Uint32	tx_port_80_under_flow       :1; /*	226	226	*/	
       MEA_Uint32	rx_port_80_fifo_full        :1; /*	227	227	*/	
       MEA_Uint32	sys_rx80_flush_small        :1; /*	228	228	*/	
       MEA_Uint32	pad7                        :27;/*	229	255	*/	
#ifdef ARCH64
	MEA_Uint32 pad64_7	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_7	    	:32;
#endif
       MEA_Uint32	pad7                        :27; /*	229	255	*/	
       MEA_Uint32	sys_rx80_flush_small        :1; /*	228	228	*/	
       MEA_Uint32	rx_port_80_fifo_full        :1; /*	227	227	*/	
       MEA_Uint32	tx_port_80_under_flow       :1; /*	226	226	*/	
       MEA_Uint32	sys_rx95_flush_small        :1; /*	225	225	*/	
       MEA_Uint32	sys_rx94_flush_small        :1; /*	224	224	*/	


#endif


        }val;
        MEA_Uint32 regs[8];
    }if_port_indication;

    MEA_if_tdm_event tdm_event;

    MEA_Uint32      IP_defrag_session_available;
    mea_ip_reassembly_lastEvent_dbt match_ip_reassembly_entry;
    mea_ip_reassembly_lastEvent_dbt unmatch_ip_reassembly_entry;

    struct { 
         MEA_Uint32 xmac0_cnt;
         MEA_Uint32 xmac1_cnt;
         MEA_Uint32 xmac0_ipg_too_short :1;
         MEA_Uint32 xmac1_ipg_too_short :1;
         MEA_Uint32 xpad_pad            :30;



    }if_XAUI_xmac ; 




    union { 
        struct {
//WORD 0
        MEA_Uint32 mpls_label_2nd;
//WORD 1
        MEA_Uint32 mpls_label_1nd;
//WORD 2
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 IPv4_l4_dest_port    :16; /*0..15*//*64 79*/
    MEA_Uint32  IPv4_l4_src_port     :16;/*16 31*//*80 95*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32  IPv4_l4_src_port     :16;/*16 31*//*80 95*/
        MEA_Uint32 IPv4_l4_dest_port     :16; /*0..15*//*64 79*/

#endif
        //WORD 3
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 IPv6_l4_dest_port    :16; /*0..15*//*96 111*/
        MEA_Uint32  IPv6_l4_src_port     :16;/*16 31*//*112 127*/
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
        MEA_Uint32  IPv6_l4_src_port     :16;/*16 31*//*112 127*/
        MEA_Uint32 IPv6_l4_dest_port     :16; /*0..15*//*96 111*/

#endif
// WORD 4
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 PPPoE_protocol       :16; /*0..15*//*128 143*/
        MEA_Uint32 next_header          :8;/*16 23*//*144 151*/
        MEA_Uint32 Ip_protocol          :8;/*24 31*//*152 159*/
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
        MEA_Uint32 Ip_protocol          :8;/*24 31*//*152 159*/
        MEA_Uint32 next_header          :8;/*16 23*//*144 151*/
        MEA_Uint32 PPPoE_protocol       :16; /*0..15*//*128 143*/

#endif
        // WORD 5
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 ip_header_lenght     :4; /*0..3*//*160 163*/
        MEA_Uint32  IP_ver              :4;/*4 7*//*164 167*/
        MEA_Uint32 ip_ethertype         :16;/*8 23*//*168 183*/
        MEA_Uint32 Ipv6_tc              :8;/*8 23*//*168 183*/
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
        MEA_Uint32 Ipv6_tc              :8;/*8 23*//*168 183*/
        MEA_Uint32 ip_ethertype         :16;/*8 23*//*168 183*/
        MEA_Uint32  IP_ver              : 4;/*4 7*//*164 167*/
        MEA_Uint32 ip_header_lenght     : 4; /*0..3*//*160 163*/

#endif

        }val;
        MEA_Uint32 regs[6];
    }if_parser_debug1_event ;
    union { 
        struct {
//word 0
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 L2_type                  : 5;/* 0  4*/
            MEA_Uint32 L3_type                  : 5;/* 5  9*/
            MEA_Uint32 L3_Offset                : 6;/*10 15*/
            MEA_Uint32 mep_id                   :16;/*16 31*/
            
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 mep_id                   :16;/*16 31*/
            MEA_Uint32 L3_Offset                : 6;/*10 15*/
            MEA_Uint32 L3_type                  : 5;/* 5  9*/
            MEA_Uint32 L2_type                  : 5;/* 0  4*/

#endif
// Word 1
         MEA_Uint32 dst_ipv4;
// Word 2
         MEA_Uint32 src_ipv4;
// Word 3
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32 ppp_session_id           :16;/* 0  15*/
         MEA_Uint32 C_TAG_inner              :16;/* 16  31*/

#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
         MEA_Uint32 C_TAG_inner              :16;/* 16  31*/
         MEA_Uint32 ppp_session_id           :16;/* 0  15*/

#endif
         // Word 4
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32 C_TAG_outer           :16;/* 0  15*/
         MEA_Uint32 S_TAG_inner              :16;/* 16  31*/
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif

         MEA_Uint32 S_TAG_inner              :16;/* 16  31*/
         MEA_Uint32 C_TAG_outer           :16;/* 0  15*/

#endif
         // Word 5
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32 S_TAG_outer        :16;/* 0  15*/
         MEA_Uint32 B_TAG              :16;/* 16  31*/
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
         MEA_Uint32 B_TAG              :16;/* 16  31*/
         MEA_Uint32 S_TAG_outer        :16;/* 0  15*/

#endif
// Word 6
         MEA_Uint32 I_TAG;
// Word 7
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32 op_code_L3             :8;/* 0  7*/
         MEA_Uint32 me_level_uc            :3;/* 8  10*/
         MEA_Uint32 message_1588           :4;/* 11  14*/
         MEA_Uint32 DSCP                   :6;/*15 20*/
         MEA_Uint32 pad                    :11;/*21 31*/
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif
         MEA_Uint32 pad                    :11;/*21 31*/
         MEA_Uint32 DSCP                   :6;/*15 20*/
         MEA_Uint32 message_1588           :4;/* 11  14*/
         MEA_Uint32 me_level_uc            :3;/* 8  10*/
         MEA_Uint32 op_code_L3             :8;/* 0  7*/

#endif
        }val;

            MEA_Uint32 regs[8];
    }if_parser_debug2_event ;
    union { 
        struct {
        MEA_Uint32 src_ipv6[4];
        MEA_Uint32 des_ipv6[4];

        }val;
        MEA_Uint32 regs[8];
    }if_parser_debug3_event ;
    union { 
        struct {
// Word 0
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 ethertype_4th         :16;
            MEA_Uint32 ethertype_3rd         :16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 ethertype_3rd         :16;
            MEA_Uint32 ethertype_4th         :16;
#endif
// Word 1
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 ethertype_2nd         :16;
            MEA_Uint32 ethertype_1st         :16;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 ethertype_1st         :16;
            MEA_Uint32 ethertype_2nd         :16;
#endif

//Word 2

            MEA_Uint32 sa_2ndL         :32;


//Word 3
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32 sa_2ndM         :16;
            MEA_Uint32 da_2ndL         :16;
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32 da_2ndL         :16;
            MEA_Uint32 sa_2ndM         :16;
#endif
// Word 4
            MEA_Uint32 da_2ndM         :32;

//Word 5
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32 sa_1stL         :32;
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
            MEA_Uint32 sa_1stL         :32;

#endif
//Word 6
#if __BYTE_ORDER == __LITTLE_ENDIAN
            
            MEA_Uint32 sa_1stM         :16;
            MEA_Uint32 da_1stL         :16;
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif
            MEA_Uint32 da_1stL         :16;
            MEA_Uint32 sa_1stM         :16;
            
#endif
//Word 7

            MEA_Uint32 da_1stM         :32;


        }val;
        MEA_Uint32 regs[8];
    }if_parser_debug4_event ;
    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 all_port_tx_mac_overflow;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 all_port_tx_mac_overflow;
#endif


#if __BYTE_ORDER == __LITTLE_ENDIAN
            
            MEA_Uint32 all_port_tx_mac_underrun;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 all_port_tx_mac_underrun;
            
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32 tls_data_fifo_full           : 1; /*64*/
            MEA_Uint32 tls_data_fifo_overflow       : 1; /*65*/
            MEA_Uint32 tls_data_fifo_underflow      : 1; /*66*/
            MEA_Uint32 tls_cntrl_fifo_overflow      : 1; /*67*/
            MEA_Uint32 tls_cntrl_fifo_underflow     : 1; /*68*/
            MEA_Uint32 tls_pad : 27;
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32 tls_pad : 27;
            MEA_Uint32 tls_cntrl_fifo_underflow : 1; /*68*/
            MEA_Uint32 tls_cntrl_fifo_overflow : 1; /*67*/
            MEA_Uint32 tls_data_fifo_underflow : 1; /*66*/
            MEA_Uint32 tls_data_fifo_overflow : 1; /*65*/
            MEA_Uint32 tls_data_fifo_full : 1; /*64*/
#endif


        }val;
        MEA_Uint32 regs[3];
    }if_tx_mac_overflow ;
    union {
        struct{
            MEA_Uint32 info;
        }val;
        MEA_Uint32 regs[1];
    }if_check_tbl_event ;


    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  p1588_control                   : 16;
            MEA_Uint32  p1588_current_pkt_port_st        : 4;
            MEA_Uint32  p1588_current_ptp_msg            : 4;
            MEA_Uint32  destPort                        : 7;
            MEA_Uint32  sop                             : 1;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
           
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32  sop                             : 1;
            MEA_Uint32  destPort                        : 7;
            MEA_Uint32  p1588_current_ptp_msg            : 4;
            MEA_Uint32  p1588_current_pkt_port_st        : 4;
            MEA_Uint32  p1588_control                   : 16;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  p1588_current_ts_fifo_usdw    : 10;
            MEA_Uint32  pad0                         : 6;
            MEA_Uint32  time_stamp_fifo_p            : 2;
            MEA_Uint32  time_stamp_fifo_overflow     : 1;
            MEA_Uint32  time_stamp_fifo_underflow    : 1;
            MEA_Uint32  pad_1                        : 12;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32  pad_1                       : 12;
            MEA_Uint32  time_stamp_fifo_underflow   : 1;
            MEA_Uint32  time_stamp_fifo_overflow    : 1;
            MEA_Uint32  time_stamp_fifo_p           : 2;
            MEA_Uint32  pad0                        : 6;
            MEA_Uint32  p1588_current_ts_fifo_usdw   : 10;
#endif
          
        }val;
        MEA_Uint32 regs[2];
    }if_1588_event;


    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  L4_checksum_last_pkt         : 16;
            MEA_Uint32  last_pkt_is_UDP              : 1;
            MEA_Uint32  UDP_CS_is_overwrite_last_pkt : 1; /*17*/
            MEA_Uint32  UDP_length_err_last_pkt      : 1; /*18*/
            MEA_Uint32  UDP_checksum_err_last_pkt    : 1; /*19*/
            MEA_Uint32  pad : 11;
            MEA_Uint32  events_valid                 : 1;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32  events_valid                    : 1;
            MEA_Uint32  pad                             : 11;
            MEA_Uint32  UDP_checksum_err_last_pkt       : 1; /*19*/
            MEA_Uint32  UDP_length_err_last_pkt         : 1; /*18*/
            MEA_Uint32  UDP_CS_is_overwrite_last_pkt    : 1; /*17*/
            MEA_Uint32  last_pkt_is_UDP                 : 1;
            MEA_Uint32  L4_checksum_last_pkt            : 16;
#endif

        }val;
        MEA_Uint32 regs[1];
    }if_XLAUI_L4checksum_event;


    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  act_id_fifo_usedw            : 4;
            MEA_Uint32  act_in_fifo_usedw            : 10;
            MEA_Uint32  pad                          : 2;
            MEA_Uint32  read_data_hfifo_usedw        : 8;
            MEA_Uint32  act_id_fifo_allmost_full     : 1;
            MEA_Uint32  act_id_fifo_overflow         : 1;
            MEA_Uint32  xpr_fifo_allmost_full        : 1;
            MEA_Uint32  read_data_hfifo_underflow    : 1;
            MEA_Uint32  read_data_lfifo_underflow    : 1;
            MEA_Uint32  act_in_fifo_overflow         : 1;
            MEA_Uint32  act_in_fifo_underflow        : 1;
            MEA_Uint32  pad1                         : 1;
            
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32  pad1                            : 1 ;
            MEA_Uint32  act_in_fifo_underflow           : 1 ;
            MEA_Uint32  act_in_fifo_overflow            : 1 ;
            MEA_Uint32  read_data_lfifo_underflow       : 1 ;
            MEA_Uint32  read_data_hfifo_underflow       : 1 ;
            MEA_Uint32  xpr_fifo_allmost_full           : 1 ;
            MEA_Uint32  act_id_fifo_overflow            : 1 ;
            MEA_Uint32  act_id_fifo_allmost_full        : 1 ;
            MEA_Uint32  read_data_hfifo_usedw           : 8 ;
            MEA_Uint32  pad                             : 2 ;
            MEA_Uint32  act_in_fifo_usedw               : 10 ;
            MEA_Uint32  act_id_fifo_usedw               : 4;

#endif

        }val;
        MEA_Uint32 regs[2];
    }if_external_Act_event;
	
	struct {
		MEA_Uint32 regs[5];

		MEA_Bool   lastlpm_action_Valid;
		MEA_Uint32 lastlpm_action; 
		MEA_Uint32 lastlpm_match;
		MEA_Bool   lastlpm_key_unmatch_type;
		MEA_Uint8  last_unmatch_vrf;
		MEA_Uint64 last_searchKey_unmatch;
		MEA_Bool   lastlpm_key_type_ip;
		MEA_Uint8  lastlpm_key_type;
		MEA_Uint8  last_vrf;
		MEA_Uint64 lastlpm_searchKey;
		
	}if_Lpm_event;

    MEA_if_HPM_event unmatch_hpm;
    MEA_if_HPM_event match_hpm;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 ihp_trans_if_0_wr_stm       : 4;  /*  0..3   */
            MEA_Uint32 ihp_trans_if_0_rdy_stm      : 4;  /*  7..4   */
            MEA_Uint32 ihp_trans_if_1_wr_stm       : 4;  /* 11..8   */
            MEA_Uint32 ihp_trans_if_1_rdy_stm      : 4;  /* 15..12  */
            MEA_Uint32 if2bm_data_stm              : 3;  /* 18..16  */
            MEA_Uint32 if2bm_rdy_stm               : 3;  /* 21..19  */
            MEA_Uint32 ihp_trans_if_2_wr_stm       : 4;  /*  25..22   */
            MEA_Uint32 ihp_trans_if_2_rdy_stm      : 4;  /*  29..26   */

            MEA_Uint32 ihp_trans_if_3_wr_stm_1_0   : 2; /* 31..30 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 ihp_trans_if_3_wr_stm_1_0 : 2; /* 31..30 */

            MEA_Uint32 ihp_trans_if_2_rdy_stm : 4;  /*  29..26   */
            MEA_Uint32 ihp_trans_if_2_wr_stm : 4;  /*  25..22   */
            MEA_Uint32 if2bm_rdy_stm               : 3;  /* 21..19  */           
            MEA_Uint32 if2bm_data_stm              : 3;  /* 18..16  */             
            MEA_Uint32 ihp_trans_if_1_rdy_stm      : 4;  /* 15..12  */     
            MEA_Uint32 ihp_trans_if_1_wr_stm       : 4;  /* 11..8   */
            MEA_Uint32 ihp_trans_if_0_rdy_stm      : 4;  /*  7..4   */   
            MEA_Uint32 ihp_trans_if_0_wr_stm       : 4;  /*  0..3   */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 ihp_trans_if_3_wr_stm_3_2 : 2;
            MEA_Uint32 ihp_trans_if_3_rdy_stm : 4;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 ihp_trans_if_3_rdy_stm : 4;
            MEA_Uint32 ihp_trans_if_3_wr_stm_3_2 : 2;
            
#endif

        } val;

        MEA_Uint32 reg[2];

    } if_events_state_machine ;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32 dse_slv_wrreq_slv_wrreqd_full_flag               :1;   /* 0..0  */       
            MEA_Uint32 dse_slv_wrreq_slv_wrreqa_full_flag               :1;   /* 1..1  */		 
            MEA_Uint32 dse_rdresp_slv_rdresp_push_full_flag             :1;   /* 2..2  */ 		 
            MEA_Uint32 dse_rdreq_slv_rdreq_pop_full_flag                :1;   /* 3..3  */ 		 
            MEA_Uint32 dse_resparb_respq_full_flag                      :1;   /* 4..4  */  		 
            MEA_Uint32 dse_wrreq_wrreqd_push_full_flag                  :1;   /* 5..5  */ 	    
            MEA_Uint32 dse_wrreq_wrreqa_push_full_flag                  :1;   /* 6..6  */  
            MEA_Uint32 dse_cmdsch_rdrsltq_crdt_full_flag                :1;   /* 7..7  */    	
            MEA_Uint32 dse_fwd_full_flag                                :1;   /* 8..8  */  	
            MEA_Uint32 dse_cls_full_flag                                :1;   /* 9..9  */  
            MEA_Uint32 acl_sync_aclsfifo_full_flag                      :1;   /* 10..10  */ 
            MEA_Uint32 acl_sync_lxcpq_full_flag                         :1;   /* 11..11  */ 
            MEA_Uint32 acl_sync_aclq_full_flag                          :1;   /* 12..12  */   	
            MEA_Uint32 fwd_dseresp_full_flag                            :1;   /* 13..13  */	
            MEA_Uint32 fwd_holdq_full_flag                              :1;   /* 14..14  */
            MEA_Uint32 fwd_lmtq_full_flag                               :1;   /* 15..15  */
            MEA_Uint32 fwd2kg_kq_full_flag                              :1;   /* 16..16  */
            MEA_Uint32 fwd_fwdcq_full_flag                              :1;   /* 17..17  */
            MEA_Uint32 fwd_fwdpq_full_flag                              :1;   /* 18..18  */
            MEA_Uint32 fwd_lmt_dseq_full_flag                           :1;   /* 19..19  */
            MEA_Uint32 fwd_lmt_st2hpq_full_flag                         :1;   /* 20..20  */
            MEA_Uint32 acl_keygen_kgpsrq_full_flag                      :1;   /* 21..21  */ 
            MEA_Uint32 acl_keygen_clskq_full_flag                       :1;   /* 22..22  */ 
            MEA_Uint32 acl_keygen_clsq_full_flag                        :1;   /* 23..23  */ 
            MEA_Uint32 fwd_keygen_kgpsrq_full_flag                      :1;   /* 24..24  */ 	
            MEA_Uint32 fwd_keygen_clsq_full_flag                        :1;   /* 25..25  */ 
            MEA_Uint32 fwd_keygen_clskq_full_flag                       :1;   /* 26..26  */  
            MEA_Uint32 fwd_keygen_clsq2_full_flag                       :1;   /* 27..27  */ 
            MEA_Uint32 ihp_lxcp_clsprf_fifo_full_flag                   :1;   /* 28..28  */
            MEA_Uint32 ihp_lxcp_psrprt_fifo_full_flag                   :1;   /* 29..29  */
            MEA_Uint32 xper_action_fifo_alf                             :1;   /*30*/
            MEA_Uint32 acl_sync_st2acl_full                             :1;   /* 31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 acl_sync_st2acl_full                             :1;   /* 31 */
            MEA_Uint32 xper_action_fifo_alf                             :1;   /*30*/
            MEA_Uint32 ihp_lxcp_psrprt_fifo_full_flag                   :1;   /* 29..29  */
            MEA_Uint32 ihp_lxcp_clsprf_fifo_full_flag                   :1;   /* 28..28  */
            MEA_Uint32 fwd_keygen_clsq2_full_flag                       :1;   /* 27..27  */                    
            MEA_Uint32 fwd_keygen_clskq_full_flag                       :1;   /* 26..26  */                     
            MEA_Uint32 fwd_keygen_clsq_full_flag                        :1;   /* 25..25  */                 
            MEA_Uint32 fwd_keygen_kgpsrq_full_flag                      :1;   /* 24..24  */                
            MEA_Uint32 acl_keygen_clsq_full_flag                        :1;   /* 23..23  */                        
            MEA_Uint32 acl_keygen_clskq_full_flag                       :1;   /* 22..22  */                          
            MEA_Uint32 acl_keygen_kgpsrq_full_flag                      :1;   /* 21..21  */                            
            MEA_Uint32 fwd_lmt_st2hpq_full_flag                         :1;   /* 20..20  */
            MEA_Uint32 fwd_lmt_dseq_full_flag                           :1;   /* 19..19  */
            MEA_Uint32 fwd_fwdpq_full_flag                              :1;   /* 18..18  */
            MEA_Uint32 fwd_fwdcq_full_flag                              :1;   /* 17..17  */
            MEA_Uint32 fwd2kg_kq_full_flag                              :1;   /* 16..16  */
            MEA_Uint32 fwd_lmtq_full_flag                               :1;   /* 15..15  */
            MEA_Uint32 fwd_holdq_full_flag                              :1;   /* 14..14  */
            MEA_Uint32 fwd_dseresp_full_flag                            :1;   /* 13..13  */
            MEA_Uint32 acl_sync_aclq_full_flag                          :1;   /* 12..12  */                      
            MEA_Uint32 acl_sync_lxcpq_full_flag                         :1;   /* 11..11  */                    
            MEA_Uint32 acl_sync_aclsfifo_full_flag                      :1;   /* 10..10  */                                                                   
            MEA_Uint32 dse_cls_full_flag                                :1;   /* 9..9  */                               
            MEA_Uint32 dse_fwd_full_flag                                :1;   /* 8..8  */                        
            MEA_Uint32 dse_cmdsch_rdrsltq_crdt_full_flag                :1;   /* 7..7  */        
            MEA_Uint32 dse_wrreq_wrreqa_push_full_flag                  :1;   /* 6..6  */       
            MEA_Uint32 dse_wrreq_wrreqd_push_full_flag                  :1;   /* 5..5  */       
            MEA_Uint32 dse_resparb_respq_full_flag                      :1;   /* 4..4  */                        
            MEA_Uint32 dse_rdreq_slv_rdreq_pop_full_flag                :1;   /* 3..3  */          
            MEA_Uint32 dse_rdresp_slv_rdresp_push_full_flag             :1;   /* 2..2  */          
            MEA_Uint32 dse_slv_wrreq_slv_wrreqa_full_flag               :1;   /* 1..1  */                                
            MEA_Uint32 dse_slv_wrreq_slv_wrreqd_full_flag               :1;   /* 0..0  */         

#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 fwd_hpm_keygen_kgpsrq_full_flag      : 1;/*32*/
    MEA_Uint32 fwd_hpm_keygen_psrq_full_flag        : 1; /*33*/
    MEA_Uint32 fwd_hpm_keygen_clsq_full_flag        : 1; /*34*/
    MEA_Uint32 dse_rdreq_sys_push_full_flag         : 1; /*35*/
    MEA_Uint32 dse_rdresp_sys_pop_full_flag         : 1; /*36*/
    MEA_Uint32 acl_hirar_ctlf_full_flag             : 1;  /*37*/
    MEA_Uint32 acl_hirar_dbgf_full_flag             : 1;  /*38*/
    MEA_Uint32 dse_reqarb_clsq_full_flag            : 1;  /*39*/
    MEA_Uint32 dse_reqarb_fwdq_full_flag            : 1;  /*40*/
    MEA_Uint32 dse_respar_dse_semaphored_cmd_full   : 1;  /*41*/
    MEA_Uint32 fwd_acl_syncbuff_hpm_full_flag       : 1;  /*42*/
    MEA_Uint32 fwd_xper_actf_full                   : 1;  /*43*/
    MEA_Uint32 fwd_xper_prot_fifo_full              : 1;  /*44*/
    MEA_Uint32 fwd_xper_rddatfh_full                : 1;  /*45*/
    MEA_Uint32 fwd_xper_rddatfl_full                : 1;  /*46*/
    MEA_Uint32 fwd_xper_tmp_fifo_full               : 1;  /*47*/
    MEA_Uint32 fwd_xper_infifo_full                 : 1;  /*48*/
    MEA_Uint32 fwd_xper_actidf_full_flag            : 1;  /*49*/
    MEA_Uint32 ihp_lxcp_prf_ptr_rdy_fifo_full       : 1;  /*50*/
    MEA_Uint32 sflow_cntn_psr_data_fifo_full        : 1;  /*51*/
    MEA_Uint32 dse_cmdresp_sfifo_full_flag          : 1;  /*52*/          
    MEA_Uint32 Reserved1                            : 11;  /*53 :63*/
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
    MEA_Uint32 Reserved1                            :11;  /*53 :63*/
    MEA_Uint32 dse_cmdresp_sfifo_full_flag          : 1;  /*52*/
    MEA_Uint32 sflow_cntn_psr_data_fifo_full        : 1;  /*51*/
    MEA_Uint32 ihp_lxcp_prf_ptr_rdy_fifo_full       : 1;  /*50*/
    MEA_Uint32 fwd_xper_actidf_full_flag            : 1;  /*49*/
    MEA_Uint32 fwd_xper_infifo_full                 : 1;  /*48*/
    MEA_Uint32 fwd_xper_tmp_fifo_full               : 1;  /*47*/
    MEA_Uint32 fwd_xper_rddatfl_full                : 1;  /*46*/
    MEA_Uint32 fwd_xper_rddatfh_full                : 1;  /*45*/
    MEA_Uint32 fwd_xper_prot_fifo_full              : 1;  /*44*/
    MEA_Uint32 fwd_xper_actf_full                   : 1;  /*43*/
    MEA_Uint32 fwd_acl_syncbuff_hpm_full_flag       : 1;  /*42*/
    MEA_Uint32 dse_respar_dse_semaphored_cmd_full   : 1;  /*41*/
    MEA_Uint32 dse_reqarb_fwdq_full_flag            : 1;  /*40*/
    MEA_Uint32 dse_reqarb_clsq_full_flag            : 1;  /*39*/
    MEA_Uint32 acl_hirar_dbgf_full_flag             : 1;  /*38*/
    MEA_Uint32 acl_hirar_ctlf_full_flag             : 1;  /*37*/
    MEA_Uint32 dse_rdresp_sys_pop_full_flag         : 1; /*36*/
    MEA_Uint32 dse_rdreq_sys_push_full_flag         : 1; /*35*/
    MEA_Uint32 fwd_hpm_keygen_clsq_full_flag        : 1; /*34*/
    MEA_Uint32 fwd_hpm_keygen_psrq_full_flag        : 1; /*33*/
    MEA_Uint32 fwd_hpm_keygen_kgpsrq_full_flag      : 1;/*32*/

#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32  ihp_lxcp_prf_ptrq_error                 :1;   /* 64  */ 
            MEA_Uint32  cfg_int_fwdlmt_error                    :2;   /* 65:66  */ 	
            MEA_Uint32  cfg_int_acl_syncbuff_error              :2;   /* 67:68  */
            MEA_Uint32  cfg_int_fwd_xper_error                  :3;   /* 69:71  */	
            MEA_Uint32  cfg_int_acl_kg_error                    :3;   /* 72:74  */	
            MEA_Uint32  cfg_int_fwd_kg_error                    :2;   /* 75:77  */	
            MEA_Uint32  cfg_int_dse_respq_error                 :1;   /* 78  */ 	
            MEA_Uint32  cfg_int_fwd_lmtq_error                  :1;   /* 79  */	
            MEA_Uint32  cfg_int_fwd_holdq_error                 :1;   /* 80  */   
            MEA_Uint32  cfg_int_fwd_fwdkq_error                 :1;   /* 81  */    
            MEA_Uint32  cfg_int_fwd_fwdcq_error                 :2;   /* 82:83  */
            MEA_Uint32  cfg_int_fwd_acl2kg_error                : 1;   /* 84  */
            MEA_Uint32  cfg_int_fwd_clsq2_error                 : 1; /*85*/
            MEA_Uint32  cfg_fwd_xpermission                     : 7; /* 86:92 */
            MEA_Uint32  cfg_int_rdreqq_pop_error                : 1; /*93*/
            MEA_Uint32  cfg_int_rdreqq_push_error               : 1; /*94*/
            MEA_Uint32  cfg_int_rdrespq_push_error              : 1; /*95*/

#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32  cfg_int_rdrespq_push_error          : 1; /*95*/
            MEA_Uint32  cfg_int_rdreqq_push_error           : 1; /*94*/
            MEA_Uint32  cfg_int_rdreqq_pop_error            : 1; /*93*/
            MEA_Uint32  cfg_fwd_xpermission                 : 7; /* 86:92 */
            MEA_Uint32  cfg_int_fwd_clsq2_error             : 1; /*85*/
            MEA_Uint32  cfg_int_fwd_acl2kg_error            : 1;   /* 84  */
            MEA_Uint32  cfg_int_fwd_fwdcq_error             : 2;   /* 82:83  */
            MEA_Uint32  cfg_int_fwd_fwdkq_error             : 1;   /* 81  */
            MEA_Uint32  cfg_int_fwd_holdq_error             : 1;   /* 80  */
            MEA_Uint32  cfg_int_fwd_lmtq_error              : 1;   /* 79  */
            MEA_Uint32  cfg_int_dse_respq_error             : 1;   /* 78  */
            MEA_Uint32  cfg_int_fwd_kg_error                : 2;   /* 75:77  */
            MEA_Uint32  cfg_int_acl_kg_error                : 3;   /* 72:74  */
            MEA_Uint32  cfg_int_fwd_xper_error              : 3;   /* 69:71  */
            MEA_Uint32  cfg_int_acl_syncbuff_error          : 2;   /* 67:68  */
            MEA_Uint32  cfg_int_fwdlmt_error                : 2;   /* 65:66  */
            MEA_Uint32  ihp_lxcp_prf_ptrq_error             : 1;   /* 64  */

#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 cfg_int_rdrespq_pop_error                    : 1; /*96*/
            MEA_Uint32 dse_wrreq_slv                                : 2; /*97:98*/
            MEA_Uint32 dse_wrreq_sys                                : 2; /*99 100*/
            MEA_Uint32 ctlf_error                                   : 1; /*101*/
            MEA_Uint32 dbgf_error                                   : 1; /*102*/
            MEA_Uint32 clsq_error                                   : 1; /*103*/
            MEA_Uint32 fwdq_error                                   : 1; /*104*/
            MEA_Uint32 resparb_error                                : 1; /*105*/
            MEA_Uint32 dse_semaphored_cmd_error                     : 1; /*106*/
            MEA_Uint32 aclqsync_error                               : 1; /*107*/
            MEA_Uint32 lxcpqsync_error                              : 1; /*108*/
            MEA_Uint32 aclsync_error                                : 1; /*109*/
            MEA_Uint32 fwd_acl_syncbuff_st2acl_error                : 1; /*110*/
            MEA_Uint32 fwd_acl_syncbuff_hpmsync_error               : 1; /*111*/
            MEA_Uint32 cfg_int_fwd_psrq_error                       : 1; /*112*/
            MEA_Uint32 fwd_hpm_keygen_cfg_int_acl_clsq_error        : 1; /*113*/
            MEA_Uint32 fwd_hpm_keygen_int_acl_prsq_error            : 1; /*114*/
            MEA_Uint32 ihp_lxcp_st2hpq_error                        : 1; /*115*/
            MEA_Uint32 ihp_lxcp_ps2lxcpq_error                      : 1; /*116*/
            MEA_Uint32 sflow_cntn_psr_data_rafifo_error             : 1; /*117*/
            MEA_Uint32 rdrsltq_crdt_error                           : 1; /*118*/
            MEA_Uint32 cmdresp_sfifo_error                          : 1; /*119*/
            MEA_Uint32 Reserved3                                    : 8;

#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
                MEA_Uint32     Reserved3                            :8;
                MEA_Uint32 cmdresp_sfifo_error                      :1; /*119*/
                MEA_Uint32 rdrsltq_crdt_error                       :1; /*118*/
                MEA_Uint32 sflow_cntn_psr_data_rafifo_error         :1; /*117*/
                MEA_Uint32 ihp_lxcp_ps2lxcpq_error                  :1; /*116*/
                MEA_Uint32 ihp_lxcp_st2hpq_error                    :1; /*115*/
                MEA_Uint32 fwd_hpm_keygen_int_acl_prsq_error        :1; /*114*/ 
                MEA_Uint32 fwd_hpm_keygen_cfg_int_acl_clsq_error    :1; /*113*/
                MEA_Uint32  cfg_int_fwd_psrq_error                  :1; /*112*/
                MEA_Uint32 fwd_acl_syncbuff_hpmsync_error               : 1; /*111*/
                MEA_Uint32 fwd_acl_syncbuff_st2acl_error                : 1; /*110*/
                MEA_Uint32 aclsync_error                                : 1; /*109*/
                MEA_Uint32 lxcpqsync_error                              : 1; /*108*/
                MEA_Uint32 aclqsync_error                               : 1; /*107*/
                MEA_Uint32 dse_semaphored_cmd_error                     : 1; /*106*/
                MEA_Uint32 resparb_error                                : 1; /*105*/
                MEA_Uint32 fwdq_error                                   : 1; /*104*/
                MEA_Uint32 clsq_error                                   : 1; /*103*/
                MEA_Uint32 dbgf_error                                   : 1; /*102*/
                MEA_Uint32  ctlf_error                                  : 1; /*101*/
                MEA_Uint32 dse_wrreq_sys                                : 2; /*99 100*/
                MEA_Uint32 dse_wrreq_slv                                : 2; /*97:98*/
                MEA_Uint32 cfg_int_rdrespq_pop_error                    : 1; /*96*/

#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 fwd_xpermission_xper_discard_event   : 4;
            MEA_Uint32 fwd_xpermission_da_discard_event     : 4;
            MEA_Uint32 fwd_xpermission_sa_discard_event     : 4;
            MEA_Uint32 dcsn_drop_event                      : 4;
            MEA_Uint32 lag_discard_event                    : 4;
            MEA_Uint32 Reserved4                            :12;
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_4	    	:32;
#endif
            MEA_Uint32 Reserved4                            :12;
            MEA_Uint32 lag_discard_event                    : 4; /*144:147*/
            MEA_Uint32 dcsn_drop_event                      : 4; /*140:143*/
            MEA_Uint32 fwd_xpermission_sa_discard_event     : 4; /*136:139*/
            MEA_Uint32 fwd_xpermission_da_discard_event     : 4; /*132:135*/
            MEA_Uint32 fwd_xpermission_xper_discard_event   : 4;/*128:131*/

#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  prot_fifo_almost_full                : 1;
            MEA_Uint32  lxcp2psr_almost_full                 : 1;
            MEA_Uint32  kgfwd2psr_almost_full_flag           : 1;
            MEA_Uint32  kghpm2psr_almost_full_flag           : 1;
            MEA_Uint32  wrreq_almost_full                    : 1;
            MEA_Uint32  holdq_almost_full_flag               : 1;
            MEA_Uint32  kg2psr_almost_full_flag              : 1;
            MEA_Uint32  clskq_almost_full                    : 1;
            MEA_Uint32  clsq_almost_full                     : 1;
            MEA_Uint32  dse_cmdresp_sfifo_almost_full_flag   : 1;
            MEA_Uint32  aclsfifo_almost_full_flag            : 1;
            MEA_Uint32  parser_fwd_fifo                      : 1;
            MEA_Uint32  Reserved5                            : 20;
#ifdef ARCH64
	MEA_Uint32 pad64_5	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_5	    	:32;
#endif
         MEA_Uint32 Reserved5 : 20;
         MEA_Uint32 parser_fwd_fifo                    : 1;
         MEA_Uint32 aclsfifo_almost_full_flag          : 1;
         MEA_Uint32 dse_cmdresp_sfifo_almost_full_flag : 1;
         MEA_Uint32 clsq_almost_full                   : 1;
         MEA_Uint32 clskq_almost_full                  : 1;
         MEA_Uint32  kg2psr_almost_full_flag           : 1;
         MEA_Uint32 holdq_almost_full_flag             : 1;
         MEA_Uint32  wrreq_almost_full                 : 1;
         MEA_Uint32  kghpm2psr_almost_full_flag        : 1;
         MEA_Uint32  kgacl2psr_almost_full_flag        : 1;
         MEA_Uint32  kgfwd2psr_almost_full_flag        : 1;
         MEA_Uint32  lxcp2psr_almost_full              : 1;
         MEA_Uint32  prot_fifo_almost_full             : 1;
#endif


        } val;


        MEA_Uint32 reg[6];

    } if_events_FWD ;

/**********************************************************/
    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN

    MEA_Uint32 DDR_12_rd_count_FIFO_full    : 1; /* 0 0*/
    MEA_Uint32 DDR_34_rd_count_FIFO_full    : 1; /*1 1 */
    MEA_Uint32 DDR_56_rd_count_FIFO_full    : 1; /*2 2 */
    MEA_Uint32 DDR_78_rd_count_FIFO_full    : 1; /*3 3 */
    MEA_Uint32 DDR_12_rd_Request_FIFO_full  : 1; /*4 4 */
    MEA_Uint32 DDR_34_rd_Request_FIFO_full  : 1; /*5 5*/
    MEA_Uint32 DDR_56_rd_Request_FIFO_full  : 1; /*6 6*/
    MEA_Uint32 DDR_78_rd_Request_FIFO_full  : 1; /*7  7*/
    MEA_Uint32 DDR_12_wr_Control_FIFO_full  : 1; /*8 8*/
    MEA_Uint32 DDR_34_wr_Control_FIFO_full  : 1; /*9 9 */
    MEA_Uint32 DDR_56_wr_Control_FIFO_full  : 1; /*10 10 */
    MEA_Uint32 DDR_78_wr_Control_FIFO_full  : 1; /*11 11 */
    MEA_Uint32 DDR_12_wr_data_FIFO_full     : 1; /*12 12 */
    MEA_Uint32 DDR_34_wr_data_FIFO_full     : 1; /*13 13 */
    MEA_Uint32 DDR_56_wr_data_FIFO_full     : 1; /*14 14 */
    MEA_Uint32 DDR_78_wr_data_FIFO_full     : 1; /*15 15 */
    MEA_Uint32 pad                          : 16; /* 16 31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad                          : 16; /* 16 31*/
    MEA_Uint32 DDR_78_wr_data_FIFO_full     : 1; /*15 15 */
    MEA_Uint32 DDR_56_wr_data_FIFO_full     : 1; /*14 14 */
    MEA_Uint32 DDR_34_wr_data_FIFO_full     : 1; /*13 13 */
    MEA_Uint32 DDR_12_wr_data_FIFO_full     : 1; /*12 12 */
    MEA_Uint32 DDR_78_wr_Control_FIFO_full  : 1; /*11 11 */
    MEA_Uint32 DDR_56_wr_Control_FIFO_full  : 1; /*10 10 */
    MEA_Uint32 DDR_34_wr_Control_FIFO_full  : 1; /*9 9 */
    MEA_Uint32 DDR_12_wr_Control_FIFO_full  : 1; /*8 8*/
    MEA_Uint32 DDR_78_rd_Request_FIFO_full  : 1; /*7  7 */
    MEA_Uint32 DDR_56_rd_Request_FIFO_full  : 1; /*6 6*/
    MEA_Uint32 DDR_34_rd_Request_FIFO_full  : 1; /*5 5*/
    MEA_Uint32 DDR_12_rd_Request_FIFO_full  : 1; /*4 4 */
    MEA_Uint32 DDR_78_rd_count_FIFO_full    : 1; /*3 3 */
    MEA_Uint32 DDR_56_rd_count_FIFO_full    : 1; /*2 2 */
    MEA_Uint32 DDR_34_rd_count_FIFO_full    : 1; /*1 1 */
    MEA_Uint32 DDR_12_rd_count_FIFO_full    : 1; /* 0 0*/
#endif
            
        }val;
        MEA_Uint32 reg[1];
    }bm_ddr_event ;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 last_vlan_editingMappingProfile_id       :  2; /*  0..  1 */
            MEA_Uint32 last_qtag_editingMappingProfile_id       :  2; /*  2..  3 */
            MEA_Uint32 last_editingMappingProfile_key           :  4; /*  4..  7 */
            MEA_Uint32 stamping_vlan_priority_to_EHPE           :  3; /*  8.. 10 */
            MEA_Uint32 stamping_vlan_color_to_EHPE              :  2; /* 11.. 12 */
            MEA_Uint32 stamping_qtag_priority_to_EHPE           :  3; /* 13.. 15 */
            MEA_Uint32 stamping_qtag_color_to_EHPE              :  2; /* 16.. 17 */
            MEA_Uint32 post_MC_Editing_editId                   :  11; /* 18.. 28 */
            MEA_Uint32 post_MC_Editing_protocol                 :  2; /* 29.. 30 */
            MEA_Uint32 post_MC_Editing_llc                      :  1; /* 31.. 31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            
            MEA_Uint32 post_MC_Editing_llc                      :  1; /* 31.. 31 */
            MEA_Uint32 post_MC_Editing_protocol                 :  2; /* 29.. 30 */
            MEA_Uint32 post_MC_Editing_editId                   :  11; /* 18.. 28 */
            MEA_Uint32 stamping_qtag_color_to_EHPE              :  2; /* 16.. 17 */
            MEA_Uint32 stamping_qtag_priority_to_EHPE           :  3; /* 13.. 15 */
            MEA_Uint32 stamping_vlan_color_to_EHPE              :  2; /* 11.. 12 */
            MEA_Uint32 stamping_vlan_priority_to_EHPE           :  3; /*  8.. 10 */
            MEA_Uint32 last_editingMappingProfile_key           :  4; /*  4..  7 */
            MEA_Uint32 last_qtag_editingMappingProfile_id       :  2; /*  2..  3 */
            MEA_Uint32 last_vlan_editingMappingProfile_id       :  2; /*  0..  1 */
#endif        
           } val;
           MEA_Uint32 reg;
       } bm_ehp_events;
    union {
        struct {

#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 atm_packet_too_short                         :  1;
            MEA_Uint32 atm_packet_too_long                            :  1;
            MEA_Uint32 Editor_unsuported_protocol                :  1;
            MEA_Uint32 IF_ingress_error                         :  1;
            MEA_Uint32 IF_ingress_ack_error_protocol_violation  :  1;
            MEA_Uint32 Drop_MQS                                    :  1;
            MEA_Uint32 Egress_Drop_MTU                            :  1;
            MEA_Uint32 Ingres_Drop_MTU                            :  1;
            MEA_Uint32 drop_bmqs                                 :  1;
            MEA_Uint32 IF_egress_not_ready_for_Port_0            :  1;
            MEA_Uint32 IF_egress_not_ready_for_Port_1            :  1;
            MEA_Uint32 IF_egress_not_ready_for_Port_125            :  1;
            MEA_Uint32 IF_egress_not_ready_for_Port_126            :  1;
            MEA_Uint32 AllocFull_buffers_full                     :  1;
            MEA_Uint32 AllocFull_descriptors_full                 :  1;
            MEA_Uint32 eth_packet_too_short                     :  1;
            MEA_Uint32 eth_packet_too_long                      :  1;            
            MEA_Uint32 eth_short_chunk                          :  1;
            MEA_Uint32 eth_long_chunk                           :  1;
            MEA_Uint32 descr_ddr_parity_err_bus                  :  4;  /*19-22*/        
            MEA_Uint32 mqs_wrap_err                                :  1;  /*23*/        
            MEA_Uint32 bmqs_wrap_err                            :  1;  /*24*/
            MEA_Uint32 analyzer_rec_no_cfm                      :  1;  /*25*/
            MEA_Uint32 not_used                                    :  6;  /*26-31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 not_used                                    :  6;  /*26-31*/
            MEA_Uint32 analyzer_rec_no_cfm                      :  1;  /*25*/
            MEA_Uint32 bmqs_wrap_err                            :  1;  /*24*/        
            MEA_Uint32 mqs_wrap_err                                :  1;  /*23*/        
            MEA_Uint32 descr_ddr_parity_err_bus                  :  4;  /*19-22*/        
            MEA_Uint32 eth_long_chunk                           :  1;  /*18*/        
            MEA_Uint32 eth_short_chunk                          :  1;  /*17*/        
            MEA_Uint32 eth_packet_too_long                      :  1;  /*16*/        
            MEA_Uint32 eth_packet_too_short                     :  1;  /*15*/        
            MEA_Uint32 AllocFull_descriptors_full                 :  1;  /*14*/        
            MEA_Uint32 AllocFull_buffers_full                     :  1;  /*13*/        
            MEA_Uint32 IF_egress_not_ready_for_Port_126            :  1;  /*12*/        
            MEA_Uint32 IF_egress_not_ready_for_Port_125            :  1;  /*11*/        
            MEA_Uint32 IF_egress_not_ready_for_Port_1            :  1;  /*10*/        
            MEA_Uint32 IF_egress_not_ready_for_Port_0            :  1;  /* 9*/        
            MEA_Uint32 drop_bmqs                                 :  1;  /* 8*/        
            MEA_Uint32 Ingres_Drop_MTU                            :  1;  /* 7*/        
            MEA_Uint32 Egress_Drop_MTU                            :  1;  /* 6*/        
            MEA_Uint32 Drop_MQS                                    :  1;  /* 5*/        
            MEA_Uint32 IF_ingress_ack_error_protocol_violation  :  1;  /* 4*/        
            MEA_Uint32 IF_ingress_error                         :  1;  /* 3*/        
            MEA_Uint32 Editor_unsuported_protocol                :  1;  /* 2*/        
            MEA_Uint32 atm_packet_too_long                       :  1;  /* 1*/        
            MEA_Uint32 atm_packet_too_short                     :  1;  /* 0*/        
#endif        
           } val;
           MEA_Uint32 reg;
       } bm_events1;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN                
            MEA_Uint32 SOP_error_for_Port_0     :  1;
            MEA_Uint32 EOP_error_for_Port_0        :  1;
            MEA_Uint32 SOP_error_for_Port_1        :  1;
            MEA_Uint32 EOP_error_for_Port_1        :  1;
            MEA_Uint32 SOP_error_for_Port_125    :  1;
            MEA_Uint32 EOP_error_for_Port_125    :  1;
            MEA_Uint32 SOP_error_for_Port_126    :  1;
            MEA_Uint32 EOP_error_for_Port_126    :  1;
            MEA_Uint32 not_used                    : 24;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 not_used                    : 24;    
            MEA_Uint32 EOP_error_for_Port_126    :  1;
            MEA_Uint32 SOP_error_for_Port_126    :  1;
            MEA_Uint32 EOP_error_for_Port_125    :  1;
            MEA_Uint32 SOP_error_for_Port_125    :  1;
            MEA_Uint32 EOP_error_for_Port_1        :  1;
            MEA_Uint32 SOP_error_for_Port_1        :  1;
            MEA_Uint32 EOP_error_for_Port_0        :  1;        
            MEA_Uint32 SOP_error_for_Port_0     :  1;       
#endif             
        } val;
           MEA_Uint32 reg;
       } bm_events2;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 Inter_chunk_gap_counter_port_125     :  8;
            MEA_Uint32 not_used                          : 24;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 not_used                          : 24;
            MEA_Uint32 Inter_chunk_gap_counter_port_125     :  8;
#endif         
        } val;
           MEA_Uint32 reg;
       } bm_events3;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  Inter_chunk_gap_counter_port_126     : 8;
            MEA_Uint32 not_used                              : 24;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 not_used                              : 24;
            MEA_Uint32  Inter_chunk_gap_counter_port_126     : 8;
#endif
        } val;
        MEA_Uint32 reg;
    } bm_events4;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 state     : 5;
            MEA_Uint32 pad                              : 27;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad                              : 27;
            MEA_Uint32 state     : 5;
#endif
        } val;
        MEA_Uint32 reg;

    }bm_events5;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 infoackerr;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 infoackerr;
#endif
        } val;
        MEA_Uint32 reg;

    }bm_events6;

/* need to add event to table number 60  */

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 reserved0                                :1; /* bit0 */
            MEA_Uint32 reserved1                                :1; /* bit1 */
            MEA_Uint32 bmqs_wrap_err                            :1; /* bit2 */
            MEA_Uint32 mqs_wrap_err                             :1; /* bit3 */
            MEA_Uint32 Queue_manager_decrement_error            :1; /* bit4 */
            MEA_Uint32 tmp_5_6                                  :2; /* bit5..6 */
            MEA_Uint32 WRED_drop_packet                         :1; /* bit7 */
            MEA_Uint32 Policer_GY_tick_is_too_fast              :1; /* bit8 */
            MEA_Uint32 Policer_YR_tick_is_too_fast              :1; /* bit9 */
            MEA_Uint32 Policer_port_tick_is_too_fast            :1; /* bit10 */
            MEA_Uint32 reserved11                               : 1; /* bit11 */
            MEA_Uint32 not_used                                 :11; /* bit12 .. bit22 */ 
            MEA_Uint32 Reorder_buf_full                         :1; /* bit23 */
            MEA_Uint32 not_used1                                :8; /* bit24 31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 not_used1                                :8; /* bit24 31*/
            MEA_Uint32 Reorder_buf_full                         :1;/* bit23 */
            MEA_Uint32 not_used                                 :11; /* bit12 .. bit22 */ 
            MEA_Uint32 reserved11                               :1; /* bit11 */
            MEA_Uint32 Policer_port_tick_is_too_fast            :1; /* bit10 */
            MEA_Uint32 Policer_YR_tick_is_too_fast              :1; /* bit9 */
            MEA_Uint32 Policer_GY_tick_is_too_fast              :1; /* bit8 */
            MEA_Uint32 WRED_drop_packet                     :1; /* bit7 */
            MEA_Uint32 tmp_5_6                              :2; /* bit5..6 */
            MEA_Uint32 Queue_manager_decrement_error            :1; /* bit4 */
            MEA_Uint32 mqs_wrap_err                         :1; /* bit3 */
            MEA_Uint32 bmqs_wrap_err                        :1; /* bit2 */
            MEA_Uint32 reserved1                            :1; /* bit1 */
            MEA_Uint32 reserved0                            :1; /* bit0 */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
MEA_Uint32 temp1;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
MEA_Uint32 temp1;
#endif

        } val;
        MEA_Uint32 reg[2];
    } bm_events60_0;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 alloc_same_desc_twice            : 1; /* 0..0*/
            MEA_Uint32 release_same_desc_twice          : 1; /* 1..1*/
            MEA_Uint32 alloc_same_buf_twice             : 1; /* 2..2*/
            MEA_Uint32 release_same_buf_twice           : 1; /* 3..3*/
            MEA_Uint32 st_alloc_same_desc_twice         : 1; /* 4..4*/
            MEA_Uint32 st_release_same_desc_twice       : 1; /* 5..5*/
            MEA_Uint32 st_alloc_same_buf_twice          : 1; /* 6..6*/
            MEA_Uint32 st_release_same_buf_twice        : 1; /* 7..7*/
            MEA_Uint32 Alloc_desc_buffer_full           : 1; /* 8..8*/
            MEA_Uint32 Alloc_desc_buffer_empty_read     : 1; /* 9..9*/
            MEA_Uint32 Alloc_data_buffer_full           : 1; /* 10..10*/
            MEA_Uint32 Alloc_data_buffer_empty_read     : 1; /* 11..11*/
            MEA_Uint32 Alloc_desc_buffer_usedwrd_0      : 1; /* 12..12*/
            MEA_Uint32 Alloc_data_buffer_usedwrd_0      : 1; /* 13..13*/
            MEA_Uint32 Alloc_Reserved                   : 2; /* 14..15*/
            MEA_Uint32 Release_desc_buffer_full_write   : 1; /* 16..16*/
            MEA_Uint32 Release_desc_buffer_empty_read   : 1; /* 17..17*/
            MEA_Uint32 Release_data_buffer_full_write   : 1; /* 18..18*/
            MEA_Uint32 Release_data_buffer_empty_read   : 1; /* 19..19*/
            MEA_Uint32 Release_desc_buffer_full         : 1; /* 20..20*/
            MEA_Uint32 Release_data_buffer_full         : 1; /* 21..21*/
            MEA_Uint32 temp0                             : 2; /*  22..23 */
            MEA_Uint32 Desc_alloc_rel_wrap_MAX_wait_cyc : 8; /*24 31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            
            
            
            MEA_Uint32 Desc_alloc_rel_wrap_MAX_wait_cyc : 8; /*24 31*/
            MEA_Uint32 temp0                            : 2; /*  22..23 */
            MEA_Uint32 Release_data_buffer_full         : 1; /* 21..21*/
            MEA_Uint32 Release_desc_buffer_full         : 1; /* 20..20*/
            MEA_Uint32 Release_data_buffer_empty_read   : 1; /* 19..19*/
            MEA_Uint32 Release_data_buffer_full_write   : 1; /* 18..18*/
            MEA_Uint32 Release_desc_buffer_empty_read   : 1; /* 17..17*/
            MEA_Uint32 Release_desc_buffer_full_write   : 1; /* 16..16*/
            MEA_Uint32 Alloc_Reserved                   : 2; /* 14..15*/
            MEA_Uint32 Alloc_data_buffer_usedwrd_0      : 1; /* 13..13*/
            MEA_Uint32 Alloc_desc_buffer_usedwrd_0      : 1; /* 12..12*/
            MEA_Uint32 Alloc_data_buffer_empty_read     : 1; /* 11..11*/
            MEA_Uint32 Alloc_data_buffer_full           : 1; /* 10..10*/
            MEA_Uint32 Alloc_desc_buffer_empty_read     : 1; /* 9..9*/
            MEA_Uint32 Alloc_desc_buffer_full           : 1; /* 8..8*/
            MEA_Uint32 st_release_same_buf_twice        : 1; /* 7..7*/
            MEA_Uint32 st_alloc_same_buf_twice          : 1; /* 6..6*/
            MEA_Uint32 st_release_same_desc_twice       : 1; /* 5..5*/
            MEA_Uint32 st_alloc_same_desc_twice         : 1; /* 4..4*/
            MEA_Uint32 release_same_buf_twice           : 1; /* 3..3*/
            MEA_Uint32 alloc_same_buf_twice             : 1; /* 2..2*/
            MEA_Uint32 release_same_desc_twice          : 1; /* 1..1*/
            MEA_Uint32 alloc_same_desc_twice            : 1; /* 0..0*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32 buf_alloc_rel_wrap_MAX_wait_cyc               : 8; /*32 39*/
            MEA_Uint32 Desc_alloc_rel_wrap_Numof_avail_inst_with_ptr : 5; /*40 44*/
            MEA_Uint32 buf_alloc_rel_wrap_all_inst_empy              : 1; /*45*/
            MEA_Uint32 Desc_alloc_rel_wrap_all_inst_empy             : 1; /*46*/
            MEA_Uint32 Release_FIFO_Err                              : 1;/*47*/
            MEA_Uint32 buf_alloc_rel_wrap_Numof_avail_inst_with_ptr  : 5;/*48 52*/
            MEA_Uint32 temp1                                         : 3; /*53  55*/
            MEA_Uint32 Desc_alloc_rel_wrap_request_with_no_avail_PTR : 1;/*56*/
            MEA_Uint32 buf_alloc_rel_wrap_request_with_no_avail_PTR  : 1; /*57*/
            MEA_Uint32 Desc_alloc_rel_wrap_FST_search_no_avail_PTR   : 1; /*58*/
            MEA_Uint32 buf_alloc_rel_wrap_FST_search_no_avail_PTR    : 1;/*59*/
            MEA_Uint32 temp2                                         : 4; /*60:63*/
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif




#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 temp2                                         : 4; /*60:63*/
            MEA_Uint32 buf_alloc_rel_wrap_FST_search_no_avail_PTR    : 1;/*59*/
            MEA_Uint32 Desc_alloc_rel_wrap_FST_search_no_avail_PTR   : 1; /*58*/
            MEA_Uint32 buf_alloc_rel_wrap_request_with_no_avail_PTR  : 1; /*57*/
            MEA_Uint32 Desc_alloc_rel_wrap_request_with_no_avail_PTR : 1;/*56*/
            MEA_Uint32 temp1                                         : 3; /*53  55*/
            MEA_Uint32 buf_alloc_rel_wrap_Numof_avail_inst_with_ptr : 5;/*48 52*/
            MEA_Uint32 Release_FIFO_Err                             : 1;/*47*/
            MEA_Uint32 Desc_alloc_rel_wrap_all_inst_empy            : 1; /*46*/
            MEA_Uint32 buf_alloc_rel_wrap_all_inst_empy             : 1; /*45*/
            MEA_Uint32 Desc_alloc_rel_wrap_Numof_avail_inst_with_ptr : 5; /*40 44*/
            MEA_Uint32 buf_alloc_rel_wrap_MAX_wait_cyc               : 8; /*32 39*/
            
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_1;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 aal5_llc_exists  :  1; /*  0.. 0 */
            MEA_Uint32 mrt_cmd          :  2; /*  1.. 2 */
            MEA_Uint32 qtag_cmd         :  2; /*  3.. 4 */
            MEA_Uint32 vlan_cmd         :  2; /*  5.. 6 */
            MEA_Uint32 protocol_type    :  2; /*  7.. 8 */
            MEA_Uint32 pkt_attr         :  2; /*  9..10 */
            MEA_Uint32 Last_edit_ID     : 18; /* 11..28 */
            
            MEA_Uint32 dst_port         :  3; /* 29..31 */

#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            
            MEA_Uint32 dst_port        :  3; /* 29..31 */
            MEA_Uint32 Last_edit_ID    : 18; /* 11..28 */
            MEA_Uint32 pkt_attr        :  2; /*  9..10 */
            MEA_Uint32 protocol_type   :  2; /*  7.. 8 */
            MEA_Uint32 vlan_cmd        :  2; /*  5.. 6 */
            MEA_Uint32 qtag_cmd        :  2; /*  3.. 4 */
            MEA_Uint32 mrt_cmd         :  2; /*  1.. 2 */
            MEA_Uint32 aal5_llc_exists :  1; /*  0.. 0 */
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 dst_port8_9      : 7; /* 32.38*/
            MEA_Uint32 unsp_prt         : 1;  /*39*/
            MEA_Uint32 pad1             : 24;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 pad1             : 24;
            MEA_Uint32 unsp_prt         :1; /*39*/
            MEA_Uint32 dst_port8_9      : 7; /* 32..38 */
            
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_2;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 red_drop_ing_policer_event0 :16;
           
            MEA_Uint32 red_drop_ing_policer_event1 :16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 red_drop_ing_policer_event1;

            MEA_Uint32 red_drop_ing_policer_event0;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 red_drop_ing_policer_event2 :16;

            MEA_Uint32 red_drop_ing_policer_event3 :16;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 red_drop_ing_policer_event3 :16;

            MEA_Uint32 red_drop_ing_policer_event2 :16;
#endif



        } val;
        MEA_Uint32 reg[2];
    } bm_events60_3;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 bmf2h_prt_type_191_160  : 32; /*  0..31 */
            MEA_Uint32 bmf2h_prt_type_159_128 : 32; /* 32..63 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 bmf2h_prt_type_191_160 : 32; /* 32..63 */
            MEA_Uint32 bmf2h_prt_type_159_128 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_4;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 bmf2h_prt_type_223_192 : 32; /*  0..31 */
            MEA_Uint32 bmf2h_prt_type_255_224 : 32; /* 32..63 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 bmf2h_prt_type_255_224 : 32; /* 32..63 */
            MEA_Uint32 bmf2h_prt_type_223_192 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_5;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 last_desc_with_parity_err_31_0  : 32; /*  0..31 */
            MEA_Uint32 last_desc_with_parity_err_32_63 : 32; /* 32..63 */
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 last_desc_with_parity_err_32_63 : 32; /* 32..63 */
            MEA_Uint32 last_desc_with_parity_err_31_0  : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_6;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 last_desc_with_parity_err_96_127 : 32; /*  0..31 */
            MEA_Uint32 last_desc_with_parity_err_64_95  : 32; /* 32..63 */
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 last_desc_with_parity_err_96_127 : 32; /* 32..63 */
            MEA_Uint32 last_desc_with_parity_err_64_95  : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_7;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 reorder_buff_counter    : 7; /*  0..6 */
            MEA_Uint32 temp                    : 25; /*  7..31 */


            MEA_Uint32 Request_for_descriptor_32_40 :  8; /* 32..39 */
            MEA_Uint32 temp0                        : 24; /* 40..63 */ 
#else
            MEA_Uint32 temp0                        : 24; /* 40..63 */ 
            MEA_Uint32 Request_for_descriptor_32_40 :  8; /* 32..39 */
            
            MEA_Uint32 temp                    : 25; /*  7..31 */
            MEA_Uint32 reorder_buff_counter    : 7; /*  0..6 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_8;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 Unused0_7                            : 8;   /*0:7*/
            MEA_Uint32 Unused8                              : 1;   /*8 8*/
            MEA_Uint32 Parity_error_SrcPort                 : 1;   /*9 9*/
            MEA_Uint32 Unused10                             : 1;   /*10 10*/
            MEA_Uint32 Parity_error_on_Pointer_TimeStamp    : 1;   /*11 11*/
            MEA_Uint32 Parity_error_on_ByteCount            : 1;   /*12 12*/
            MEA_Uint32 Parity_error_on_BufferID             : 1;   /*13 13*/
            MEA_Uint32 DDR_data_ECC                         : 1;  /*14 14*/
            MEA_Uint32 pad1                                 : 17;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad1                                : 17;
            MEA_Uint32 DDR_data_ECC                             : 1;  /*14 14*/
            MEA_Uint32 Parity_error_on_BufferID                 : 1;   /*13 13*/
            MEA_Uint32 Parity_error_on_ByteCount                : 1;   /*12 12*/
            MEA_Uint32 Parity_error_on_Pointer_TimeStamp        : 1;   /*11 11*/
            MEA_Uint32 Unused10                                 : 1;   /*10 10*/
            MEA_Uint32 Parity_error_SrcPort                     : 1;   /*9 9*/
            MEA_Uint32 Unused8                                  : 1;   /*8 8*/
            MEA_Uint32 Unused0_7                                : 8;   /*0:7*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 temp0;
#else
            MEA_Uint32 temp0 ;
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_9;
  
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 Desc_addr_Last_alloc_twice_addr : 16;
            MEA_Uint32 Desc_addr_Last_rel_twice_addr   : 16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 Desc_addr_Last_rel_twice_addr : 16;
            MEA_Uint32 Desc_addr_Last_alloc_twice_addr : 16;
            
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 Desc_addr_alloc_twice            : 1; /*32*/
            MEA_Uint32 Desc_axi_addr_rel_twice_addr     : 1;
            MEA_Uint32 Desc_axi_Long_wait_to_HIT        : 1;
            MEA_Uint32 Desc_axi_FIFO_read_empty         : 1;
            MEA_Uint32 Desc_axi_FIFO_write_full         : 1;
            MEA_Uint32 Desc_axi_FIFO_almost_FULL        : 1;
            MEA_Uint32 Desc_axi_MAX_wait_cyc_to_HIT    : 26;

#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32  Desc_axi_MAX_wait_cyc_to_HIT : 26;
            MEA_Uint32 Desc_axi_FIFO_almost_FULL    : 1;
            MEA_Uint32 Desc_axi_FIFO_write_full     : 1;
            MEA_Uint32 Desc_axi_FIFO_read_empty     : 1;
            MEA_Uint32 Desc_axi_Long_wait_to_HIT    : 1;
            MEA_Uint32 Desc_axi_addr_rel_twice_addr : 1;
            MEA_Uint32 Desc_axi_addr_alloc_twice    : 1; /*32*/
#endif

        } val;
        MEA_Uint32 reg[2];
    } bm_events60_10;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 int_ptr_fifo_used            : 12; /*  0.11 */
            MEA_Uint32 reserved                     : 1; /*  12 */
            MEA_Uint32 alloc_same_buffer_twice      : 1; /*  13 */
            MEA_Uint32 release_same_buffer_twice    : 1; /*  14 */
            MEA_Uint32 int_ptr_fifo_overflow        : 1; /*  15 */
            MEA_Uint32 int_mem_pkts_off_mem_cnt     :16; /* 16..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
           
            MEA_Uint32 int_mem_pkts_off_mem_cnt     :16; /* 16..31 */
            MEA_Uint32 int_ptr_fifo_overflow        : 1; /*  15 */
            MEA_Uint32 release_same_buffer_twice    : 1; /*  14 */
            MEA_Uint32 alloc_same_buffer_twice      : 1; /*  13 */
            MEA_Uint32 reserved                     : 1; /*  12 */
            MEA_Uint32 int_ptr_fifo_used            : 12; /*  0.11 */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 int_mem_pkts_on_mem_cnt     :16; /* 0..15 */
            MEA_Uint32  Qstat_RAFIFO_rd_empty      : 1; // 53
            MEA_Uint32  Qstat_Toggle_state         : 1; // 54            
            MEA_Uint32  Stat_BUFF_CS_0             : 1; // 55
            MEA_Uint32  Qstat_BUFF_CS_1            : 1; // 56
            MEA_Uint32  Qstat_Buff_initdone        : 1; // 57
            MEA_Uint32  Ing_queue_req_twice        : 1; // 58
            MEA_Uint32  Egr_queue_req_twice        : 1; // 59  
            MEA_Uint32  Drop_queue_req_twice       : 1; //60   
            MEA_Uint32 reserved_reg1               : 3; /* 61:63 */
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
        MEA_Uint32 reserved_reg1                   :3; /* 61:63 */
        MEA_Uint32  Drop_queue_req_twice    : 1; // 60
        MEA_Uint32  Egr_queue_req_twice     : 1; // 59
        MEA_Uint32  Ing_queue_req_twice     : 1; // 58
        MEA_Uint32  Qstat_Buff_initdone     : 1; // 57
        MEA_Uint32  Qstat_BUFF_CS_1         : 1; // 56
        MEA_Uint32  Stat_BUFF_CS_0          : 1; // 55
        MEA_Uint32  Qstat_Toggle_state      : 1; // 54
        MEA_Uint32  Qstat_RAFIFO_rd_empty   : 1; // 53
        MEA_Uint32  Qstat_RAFIFO_wr_full    : 1; // 52
        MEA_Uint32  Qstat_RAFIFO_Full       : 1; // 51
        MEA_Uint32  Qstat_STM               : 2; // 49:50
        MEA_Uint32  Arbiter_STM             : 1; // 48






        MEA_Uint32 int_mem_pkts_on_mem_cnt     :16; /* 0..15 */
#endif



        } val;
        MEA_Uint32 reg[2];
    } bm_events60_11;


    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32 qshaper_qelig_full                           :1; /* 0..0 */            
            MEA_Uint32 clfifo_full                                  :1; /* 1..1 */ 
            MEA_Uint32 qfifo_full                                   :1; /* 2..2 */            
            MEA_Uint32 fifo_backpr_full                             :1; /* 3..3 */                
            MEA_Uint32 req_master_full                              :1; /* 4..4 */        
            MEA_Uint32 rngrd_req_fifo_full                          :1; /* 5..5 */            
            MEA_Uint32 hold_fifo_full                               :1; /* 6..6 */            
            MEA_Uint32 ddr_rdinfo_fifo_alfull                       :1; /* 7..7 */                
            MEA_Uint32 ddr_rdsize_fifo_alfull                       :1; /* 8..8 */                
            MEA_Uint32 ififo_full                                   :1; /* 9..9 */ 
            MEA_Uint32 rngrd_fifo_full                              :1; /* 10..10 */
            MEA_Uint32 ddr_dscr_req_full                            :1; /* 11..11 */
            MEA_Uint32 almost_Aloc_AlocR_Full                       :1; /* 12..12 */
            MEA_Uint32 bcfifo_full                                  :1; /* 13..13 */  
            MEA_Uint32 tx_eng_buff_comp_full                        :1; /* 14..14 */
            MEA_Uint32 int_alocrfull                                :1; /* 15..15 */     
            MEA_Uint32 int_alocfull                                 :1; /* 16..16 */
            MEA_Uint32 alocrngmcfull                                :1; /* 17..17 */     
            MEA_Uint32 alocrngfull                                  :1; /* 18..18 */           
            MEA_Uint32 alocfull                                     :1; /* 19..19 */              
            MEA_Uint32 xrelblkadr_fifo_full                         :1; /* 20..20 */            
            MEA_Uint32 hold_fifo_almost_full                        :1; /* 21..21 */            
            MEA_Uint32 dq_cnt_checker                               :1; /* 22..22 */            
            MEA_Uint32 ddr_desc_axi_wr_data_fifo_almost_full        :1; /* 23..23 */             
            MEA_Uint32 ddr_desc_axi_wr_data_fifo_overflow           :1; /* 24..24 */            
            MEA_Uint32 ddr_desc_axi_ddr_unit_MIG_response           :2; /* 26..25 */            
            MEA_Uint32 ddr_desc_axi_rd_cntrl_fifo_almost_full       :1; /* 27..27 */            
            MEA_Uint32 ddr_desc_axi_rd_cont_overflow                :1; /* 28..28 */            
            MEA_Uint32 ddr_desc_axi_wr_cont_overflow                :1; /* 29..29 */            
            MEA_Uint32 ddr_desc_axi_desc_sync_full_overflow         :1; /* 30..30 */            
            MEA_Uint32 ddr_desc_axi_wr_cont_full                    :1; /* 31..31 */                
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
  

            MEA_Uint32 ddr_desc_axi_wr_cont_full                        :1; /* 31..31 */    
            MEA_Uint32 ddr_desc_axi_desc_sync_full_overflow             :1; /* 30..30 */    
            MEA_Uint32 ddr_desc_axi_wr_cont_overflow                    :1; /* 29..29 */    
            MEA_Uint32 ddr_desc_axi_rd_cont_overflow                    :1; /* 28..28 */    
            MEA_Uint32 ddr_desc_axi_rd_cntrl_fifo_almost_full           :1; /* 27..27 */     
            MEA_Uint32 ddr_desc_axi_ddr_unit_MIG_response               :2; /* 26..25 */        
            MEA_Uint32 ddr_desc_axi_wr_data_fifo_overflow               :1; /* 24..24 */        
            MEA_Uint32 ddr_desc_axi_wr_data_fifo_almost_full            :1; /* 23..23 */          
            MEA_Uint32 dq_cnt_checker                                   :1; /* 22..22 */            
            MEA_Uint32 hold_fifo_almost_full                            :1; /* 21..21 */           
            MEA_Uint32 xrelblkadr_fifo_full                             :1; /* 20..20 */             
            MEA_Uint32 alocfull                                         :1; /* 19..19 */            
            MEA_Uint32 alocrngfull                                      :1; /* 18..18 */             
            MEA_Uint32 alocrngmcfull                                    :1; /* 17..17 */             
            MEA_Uint32 int_alocfull                                     :1; /* 16..16 */            
            MEA_Uint32 int_alocrfull                                    :1; /* 15..15 */         
            MEA_Uint32 tx_eng_buff_comp_full                            :1; /* 14..14 */        
            MEA_Uint32 bcfifo_full                                      :1; /* 13..13 */          
            MEA_Uint32 almost_Aloc_AlocR_Full                           :1; /* 12..12 */        
            MEA_Uint32 ddr_dscr_req_full                                :1; /* 11..11 */        
            MEA_Uint32 rngrd_fifo_full                                  :1; /* 10..10 */        
            MEA_Uint32 ififo_full                                       :1; /* 9..9 */          
            MEA_Uint32 ddr_rdsize_fifo_alfull                           :1; /* 8..8 */            
            MEA_Uint32 ddr_rdinfo_fifo_alfull                           :1; /* 7..7 */            
            MEA_Uint32 hold_fifo_full                                   :1; /* 6..6 */            
            MEA_Uint32 rngrd_req_fifo_full                              :1; /* 5..5 */            
            MEA_Uint32 req_master_full                                 :1; /* 4..4 */        
            MEA_Uint32 fifo_backpr_full                                :1; /* 3..3 */            
            MEA_Uint32  qfifo_full                                      :1; /* 2..2 */            
            MEA_Uint32  clfifo_full                                     :1; /* 1..1 */             
            MEA_Uint32 qshaper_qelig_full                               :1; /* 0..0 */            

#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 ddr_desc_axi_TO_wr_data_fifo_full                            : 1; /*32*/ 
            MEA_Uint32 ddr_desc_axi_TO_rd_cont_wrfull                               : 1; /*33*/  
            MEA_Uint32 ddr_desc_axi_TO_desc_sync_full                               : 1; /*34*/    
            MEA_Uint32 ddr_desc_axi_TO_desc_hold_fifo_full                          : 1; /*35*/ 
            MEA_Uint32 ddr_desc_axi_TO_desc_hold_fifo_full_AND_desc_hold_wr_en      : 1; /*36*/
            MEA_Uint32 ddr_data_axi_TO_wr_data_fifo_almost_full                     : 1; /*37*/ 
            MEA_Uint32 ddr_data_axi_TO_wr_data_fifo_overflow                        : 1; /*38*/ 
            MEA_Uint32 ddr_data_axi_TO_ddr_unit_MIG_response                        : 2; /*40..39*/
            MEA_Uint32 ddr_data_axi_TO_rd_cntrl_fifo_almost_full                    : 1; /*41*/    
            MEA_Uint32 ddr_data_axi_TO_rd_cont_fifo_overflow                        : 1; /*42*/    
            MEA_Uint32 ddr_data_axi_TO_wr_cont_overflow                             : 1; /*43*/    
            MEA_Uint32 ddr_data_axi_TO_wr_cont_full                                 : 1; /*44*/    
            MEA_Uint32 ddr_data_axi_TO_wr_data_fifo_full                            : 1; /*45*/        
            MEA_Uint32 ddr_data_axi_TO_rd_cont_fifo_full                            : 1; /*46*/
            MEA_Uint32 lq_fifo_backpr_full_1                                        : 1; /*47*/
            MEA_Uint32 lq_fifo_backpr_full_2                                        : 1; /*48*/
            MEA_Uint32 lq_fifo_backpr_full_3                                        : 1; /*49*/
            MEA_Uint32 lq_fifo_backpr_full_4                                        : 1; /*50*/
            MEA_Uint32 qfifo_full_1                                                 : 1; /*51*/
            MEA_Uint32 qfifo_full_2                                                 : 1; /*52*/
            MEA_Uint32 qfifo_full_3                                                 : 1; /*53*/
            MEA_Uint32 qfifo_full_4                                                 : 1; /*54*/
            MEA_Uint32 cluster_fifo_full_1                                          : 1; /*55*/
            MEA_Uint32 cluster_fifo_full_2                                          : 1; /*56*/
            MEA_Uint32 cluster_fifo_full_3                                          : 1; /*57*/
            MEA_Uint32 cluster_fifo_full_4                                          : 1; /*58*/
            MEA_Uint32 qshaper_qelig_fifo_1                                        : 1; /*59*/
            MEA_Uint32 qshaper_qelig_fifo_2                                        : 1; /*60*/
            MEA_Uint32 qshaper_qelig_fifo_3                                        : 1; /*61*/
            MEA_Uint32 qshaper_qelig_fifo_4                                        : 1; /*62*/
            MEA_Uint32 swap_edit_ctrl_fifo_full                                    : 1; /*63*/
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif

            MEA_Uint32  swap_edit_ctrl_fifo_full                                    :1; /*63*/
            MEA_Uint32  qshaper_qelig_fifo_4                                        :1; /*62*/
            MEA_Uint32  qshaper_qelig_fifo_3                                        :1; /*61*/
            MEA_Uint32  qshaper_qelig_fifo_2                                        :1; /*60*/
            MEA_Uint32  qshaper_qelig_fifo_1                                        :1; /*59*/
            MEA_Uint32 cluster_fifo_full_4                                          :1; /*58*/
            MEA_Uint32 cluster_fifo_full_3                                          :1; /*57*/
            MEA_Uint32 cluster_fifo_full_2                                          :1; /*56*/
            MEA_Uint32 cluster_fifo_full_1                                          :1; /*55*/
            MEA_Uint32 qfifo_full_4                                                 :1; /*54*/
            MEA_Uint32 qfifo_full_3                                                 :1; /*53*/
            MEA_Uint32 qfifo_full_2                                                 :1; /*52*/
            MEA_Uint32 qfifo_full_1                                                 :1; /*51*/
            MEA_Uint32 lq_fifo_backpr_full_4                                        :1; /*50*/
            MEA_Uint32 lq_fifo_backpr_full_3                                        :1; /*49*/
            MEA_Uint32 lq_fifo_backpr_full_2                                        :1; /*48*/
            MEA_Uint32 lq_fifo_backpr_full_1                                        :1; /*47*/  
            MEA_Uint32 ddr_data_axi_TO_rd_cont_fifo_full                             :1; /*46..46*/     
            MEA_Uint32 ddr_data_axi_TO_wr_data_fifo_full                             :1; /*45..45*/    
            MEA_Uint32 ddr_data_axi_TO_wr_cont_full                                  :1; /*44..44*/    
            MEA_Uint32 ddr_data_axi_TO_wr_cont_overflow                              :1; /*43..43*/
            MEA_Uint32 ddr_data_axi_TO_rd_cont_fifo_overflow                         :1; /*42..42*/    
            MEA_Uint32 ddr_data_axi_TO_rd_cntrl_fifo_almost_full                     :1; /*41..41*/    
            MEA_Uint32 ddr_data_axi_TO_ddr_unit_MIG_response                         :2; /*40..39*/    
            MEA_Uint32 ddr_data_axi_TO_wr_data_fifo_overflow                         :1; /*38..38*/ 
            MEA_Uint32 ddr_data_axi_TO_wr_data_fifo_almost_full                      :1; /*37..37*/ 
            MEA_Uint32 ddr_desc_axi_TO_desc_hold_fifo_full_AND_desc_hold_wr_en       :1; /*36..36*/
            MEA_Uint32 ddr_desc_axi_TO_desc_hold_fifo_full                           :1; /*35..35*/           
            MEA_Uint32 ddr_desc_axi_TO_desc_sync_full                                :1; /*34..34*/       
            MEA_Uint32 ddr_desc_axi_TO_rd_cont_wrfull                                :1; /*33..33*/        
            MEA_Uint32 ddr_desc_axi_TO_wr_data_fifo_full                             :1; /*32..32*/                


#endif

        } val;
        MEA_Uint32 reg[2];
    } bm_events60_12;


    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 Arlen_fifo_in_Data_AXI_convertor_full                        : 1; /* 0 */
            MEA_Uint32 Read_data_fifo_in_Data_AXI_convertor_full                    : 1; /* 1 */
            MEA_Uint32 Write_data_fifo_in_Data_AXI_convertor_full                   : 1; /* 2 */
            MEA_Uint32 Backpressure_from_AXI_data_convertor_to_DDR_unit_data_AXI    : 1; /* 3 */
            MEA_Uint32 temp0                                                        : 28;
            MEA_Uint32 temp1                                                        : 32;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 temp1                                                        : 32; 
            MEA_Uint32 temp0                                                        : 28; 
            MEA_Uint32 Backpressure_from_AXI_data_convertor_to_DDR_unit_data_AXI    :1; /* 3 */
            MEA_Uint32 Write_data_fifo_in_Data_AXI_convertor_full                   :1; /* 2 */
            MEA_Uint32 Read_data_fifo_in_Data_AXI_convertor_full                    :1; /* 1 */
            MEA_Uint32 Arlen_fifo_in_Data_AXI_convertor_full                        :1; /* 0 */

#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_13;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 aal5_llc_exists :  1; /*  0.. 0 */
            MEA_Uint32 mrt_cmd         :  2; /*  1.. 2 */
            MEA_Uint32 qtag_cmd        :  2; /*  3.. 4 */
            MEA_Uint32 vlan_cmd        :  2; /*  5.. 6 */
            MEA_Uint32 protocol_type   :  2; /*  7.. 8 */
            MEA_Uint32 pkt_attr        :  2; /*  9..10 */
            MEA_Uint32 temp0           : 21; /* 11..31 */
            MEA_Uint32 temp1           : 32; /* 32..63 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 temp1           : 32; /* 32..63 */
            MEA_Uint32 temp0           : 21; /* 11..31 */
            MEA_Uint32 pkt_attr        :  2; /*  9..10 */
            MEA_Uint32 protocol_type   :  2; /*  7.. 8 */
            MEA_Uint32 vlan_cmd        :  2; /*  5.. 6 */
            MEA_Uint32 qtag_cmd        :  2; /*  3.. 4 */
            MEA_Uint32 mrt_cmd         :  2; /*  1.. 2 */
            MEA_Uint32 aal5_llc_exists :  1; /*  0.. 0 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_14;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 bmf_din_0_31  : 32; /*  0..31 */
            MEA_Uint32 bmd_din_32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 bmd_din_32_63 : 32; /* 32..63 */
            MEA_Uint32 bmf_din_0_31  : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_15;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 bmf_din_64_95  : 32; /*  0..31 */
            MEA_Uint32 bmd_din_96_127 : 32; /* 32..63 */
#else
            MEA_Uint32 bmd_din_96_127 : 32; /* 32..63 */
            MEA_Uint32 bmf_din_64_95  : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_16;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
              MEA_Uint32 input_state  :  2; /*  0.. 1 */
              MEA_Uint32 temp0        :  6; /*  2.. 7 */
              MEA_Uint32 output_state :  2; /*  8.. 9 */
              MEA_Uint32 temp1        :  6; /* 10..15 */
              MEA_Uint32 st_init      :  2; /* 16..17 */
              MEA_Uint32 temp2        :  6; /* 18..23 */
              MEA_Uint32 st_PreSched  :  3; /* 24..26 */
              MEA_Uint32 temp3        :  5; /* 27..31 */
              MEA_Uint32 st_Sched     :  3; /* 32..34 */
              MEA_Uint32 temp4        :  5; /* 35..39 */
              MEA_Uint32 temp5        : 24; /* 40..63 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
              MEA_Uint32 temp5        : 24; /* 40..63 */
              MEA_Uint32 temp4        :  5; /* 35..39 */
              MEA_Uint32 st_Sched     :  3; /* 32..34 */
              MEA_Uint32 temp3        :  5; /* 27..31 */
              MEA_Uint32 st_PreSched  :  3; /* 24..26 */
              MEA_Uint32 temp2        :  6; /* 18..23 */
              MEA_Uint32 st_init      :  2; /* 16..17 */
              MEA_Uint32 temp1        :  6; /* 10..15 */
              MEA_Uint32 output_state :  2; /*  8.. 9 */
              MEA_Uint32 temp0        :  6; /*  2.. 7 */
              MEA_Uint32 input_state  :  2; /*  0.. 1 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_17;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32 parity_check_of_descriptor_pointer_0_bits_0_15   :  1; /*  0.. 0 */
            MEA_Uint32 parity_check_of_descriptor_pointer_1_bits_16_31  :  1; /*  1.. 1 */
            MEA_Uint32 parity_check_of_descriptor_pointer_2_bits_32_47 :  1; /*  2.. 2 */
            MEA_Uint32 parity_check_of_descriptor_pointer_3_bits_48_63 :  1; /*  3.. 3 */
            MEA_Uint32 parity_check_of_descriptor_pointer_4_bits_64_79 :  1; /*  4.. 4 */
            MEA_Uint32 parity_check_of_descriptor_pointer_5_bits_80_95 :  1; /*  5.. 5 */
            MEA_Uint32 parity_check_of_descriptor_pointer_6_bits_96_111 :  1; /*  6.. 6 */
            MEA_Uint32 parity_check_of_descriptor_pointer_7_bits_112_127 :  1; /*  7.. 7 */
            MEA_Uint32 temp0                                           : 24; /*  8..31 */
            MEA_Uint32 temp1                                           : 32; /* 32..63 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 temp1                                           : 32; /* 32..63 */
            MEA_Uint32 temp0                                           : 24; /*  8..31 */
            MEA_Uint32 parity_check_of_descriptor_pointer_7_bits_112_127 :  1; /*  7.. 7 */
            MEA_Uint32 parity_check_of_descriptor_pointer_6_bits_96_111 :  1; /*  6.. 6 */
            MEA_Uint32 parity_check_of_descriptor_pointer_5_bits_80_95 :  1; /*  5.. 5 */
            MEA_Uint32 parity_check_of_descriptor_pointer_4_bits_64_79 :  1; /*  4.. 4 */
            MEA_Uint32 parity_check_of_descriptor_pointer_3_bits_48_63 :  1; /*  3.. 3 */
            MEA_Uint32 parity_check_of_descriptor_pointer_2_bits_32_47 :  1; /*  2.. 2 */
            MEA_Uint32 parity_check_of_descriptor_pointer_1_bits_16_31  :  1; /*  1.. 1 */
            MEA_Uint32 parity_check_of_descriptor_pointer_0_bits_0_15   :  1; /*  0.. 0 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_18;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32 modified_tag_type                :  2; /* 0.. 1 */
            MEA_Uint32 modified_vlan_command            :  2; /* 2 3*/
            MEA_Uint32 modified_qtag_command            :  2; /* 4 5*/ 
            MEA_Uint32 pvid_equal                       :  1; /* 6  */
            MEA_Uint32 original_vlan_command            :  2; /* 7 8*/
            MEA_Uint32 original_qtag_command            :  2; /* 9 10 */ 
            MEA_Uint32 original_ingress_packet_type     :  2; /* 11:12 */
            MEA_Uint32 modified_vlan_ethtype            : 16;/*13 :28*/
            MEA_Uint32 temp0                            : 4; /*  8..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif			
            MEA_Uint32 modified_qtag_ethtype            : 16; /*32 47 */
            MEA_Uint32 temp1                            : 16; /* 16..63 */
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 temp1                            : 16; /* 48..63 */
            MEA_Uint32 modified_qtag_ethtype            : 16; /*32 47 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif			
			
            MEA_Uint32 temp0                            : 4; /*  29..31 */
            MEA_Uint32 modified_vlan_ethtype            : 16;/*13 :28*/
            MEA_Uint32 original_ingress_packet_type     :  2; /* 11:12 */
            MEA_Uint32 original_qtag_command            :  2; /* 9 10 */           
            MEA_Uint32 original_vlan_command            :  2; /* 7 8*/
            MEA_Uint32 pvid_equal                       :  1; /* 6  */
            MEA_Uint32 modified_qtag_command            :  2; /* 4 5*/           
            MEA_Uint32 modified_vlan_command            :  2; /* 2 3*/
            MEA_Uint32 modified_tag_type                :  2; /* 0.. 1 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_21;
    
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 Sequence_ID;
#else
           MEA_Uint32 Sequence_ID;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN

           
            MEA_Uint32 Payload_Length   : 16; /* 32 47 */
            MEA_Uint32 Op_code          : 8;
            MEA_Uint32 Tlv_type         : 8;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 Tlv_type         : 8;
            MEA_Uint32 Op_code          : 8;
            MEA_Uint32 Payload_Length   : 16; /* 32 47 */
           
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_22;


    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  MEP_ID : 16; /*0 15*/
            MEA_Uint32  Tlv_offset          : 8; /*16:23*/


            MEA_Uint32  data_type : 3;
            MEA_Uint32  period_CCM : 3;
            MEA_Uint32  RDI_event : 1;
            MEA_Uint32  un_stream_Id : 1;/*31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 un_stream_Id     :1; /*31*/
            MEA_Uint32  RDI_event : 1; /*30*/
            MEA_Uint32  period_CCM : 3;
            MEA_Uint32  data_type : 3;
            MEA_Uint32  Tlv_offset : 8; /*16:23*/
            MEA_Uint32  MEP_ID  : 16; /*0 15*/

#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  Pkt_size             : 14; /*32:45*/         
            MEA_Uint32  un_opcode            : 1;/*46*/
            MEA_Uint32  no_cfm_packet        : 1;/*47*/
            MEA_Uint32  in_valid_timeStamp   : 1;/*48*/
            MEA_Uint32  anlyzer_drop         : 1;/*49*/
            MEA_Uint32  stream_id            : 12;
            MEA_Uint32  reserved1            : 2;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            
#else
           
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 reserved1            :2;
            MEA_Uint32 stream_id            :12;
            MEA_Uint32 anlyzer_drop         :1;/*49*/
            MEA_Uint32 in_valid_timeStamp   :1;/*48*/
            MEA_Uint32 no_cfm_packet        :1;/*47*/
            MEA_Uint32 un_opcode            :1;/*46*/
            MEA_Uint32  Pkt_size            :14; /*32:45*/
            
            

            
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_23;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 scheduler_xown_status31_0 : 32; /*  0..31 */
            MEA_Uint32 scheduler_xown_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 scheduler_xown_status32_63 : 32; /* 32..63 */
            MEA_Uint32 scheduler_xown_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_24;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 scheduler_xown_status31_0 : 32; /*  0..31 */
            MEA_Uint32 scheduler_xown_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 scheduler_xown_status32_63 : 32; /* 32..63 */
            MEA_Uint32 scheduler_xown_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_25;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 scheduler_xown_status31_0 : 32; /*  0..31 */
            MEA_Uint32 scheduler_xown_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 scheduler_xown_status32_63 : 32; /* 32..63 */
            MEA_Uint32 scheduler_xown_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_26;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 scheduler_xown_status31_0 : 32; /*  0..31 */
            MEA_Uint32 scheduler_xown_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 scheduler_xown_status32_63 : 32; /* 32..63 */
            MEA_Uint32 scheduler_xown_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_27;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 scheduler_xptrok_status31_0 : 32; /*  0..31 */
            MEA_Uint32 scheduler_xptrok_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 scheduler_xptrok_status32_63 : 32; /* 32..63 */
            MEA_Uint32 scheduler_xptrok_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_28;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 scheduler_xptrok_status31_0 : 32; /*  0..31 */
            MEA_Uint32 scheduler_xptrok_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 scheduler_xptrok_status32_63 : 32; /* 32..63 */
            MEA_Uint32 scheduler_xptrok_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_29;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 scheduler_xptrok_status31_0 : 32; /*  0..31 */
            MEA_Uint32 scheduler_xptrok_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 scheduler_xptrok_status32_63 : 32; /* 32..63 */
            MEA_Uint32 scheduler_xptrok_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_30;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 scheduler_xptrok_status31_0 : 32; /*  0..31 */
            MEA_Uint32 scheduler_xptrok_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 scheduler_xptrok_status32_63 : 32; /* 32..63 */
            MEA_Uint32 scheduler_xptrok_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_31;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 arbcret_xreadycert_status31_0 : 32; /*  0..31 */
            MEA_Uint32 arbcret_xreadycert_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 arbcret_xreadycert_status32_63 : 32; /* 32..63 */
            MEA_Uint32 arbcret_xreadycert_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_32;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 arbcret_xreadycert_status31_0 : 32; /*  0..31 */
            MEA_Uint32 arbcret_xreadycert_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 arbcret_xreadycert_status32_63 : 32; /* 32..63 */
            MEA_Uint32 arbcret_xreadycert_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_33;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 arbcret_xreadycert_status31_0 : 32; /*  0..31 */
            MEA_Uint32 arbcret_xreadycert_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 arbcret_xreadycert_status32_63 : 32; /* 32..63 */
            MEA_Uint32 arbcret_xreadycert_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_34;
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 arbcret_xreadycert_status31_0 : 32; /*  0..31 */
            MEA_Uint32 arbcret_xreadycert_status32_63 : 32; /* 32..63 */
#else
            MEA_Uint32 arbcret_xreadycert_status32_63 : 32; /* 32..63 */
            MEA_Uint32 arbcret_xreadycert_status31_0 : 32; /*  0..31 */
#endif
        } val;
        MEA_Uint32 reg[2];
    } bm_events60_35;

    /*********************************************/
    /*   counter on BM                           */
    /* bm_events60_36  */
    /* bm_events60_37  */
    /* bm_events60_38  */
    /* bm_events60_40  */
    /*********************************************/

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count_General_parity_on_descriptor; /*  0..31 */
#else
            MEA_Uint32 count_General_parity_on_descriptor; /*  0..31 */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count_Descriptor_parity_on_source_port; /* 32..63 */
#else
            MEA_Uint32 count_Descriptor_parity_on_source_port; /* 32..63 */
#endif


        } val;
        MEA_Uint32 reg[2];
    } bm_events60_36;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count_Descriptor_parity_on_Edit_ID; /*  0..31 */
#else
            MEA_Uint32 count_Descriptor_parity_on_Edit_ID; /*  0..31 */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count_Descriptor_parity_on_TS_pointer; /* 32..63 */
#else
            MEA_Uint32 count_Descriptor_parity_on_TS_pointer ; /* 32..63 */
#endif


        } val;
        MEA_Uint32 reg[2];
    } bm_events60_37;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count_Descriptor_parity_on_Byte_count; /*  0..31 */
#else
            MEA_Uint32 count_Descriptor_parity_on_Byte_count; /*  0..31 */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count_Descriptor_parity_on_buffer_pointer; /* 32..63 */
#else
            MEA_Uint32 count_Descriptor_parity_on_buffer_pointer ; /* 32..63 */
#endif


        } val;
        MEA_Uint32 reg[2];
    } bm_events60_38;

    union {
        struct {
                /*PM DDR EVENT*/

#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 ingress_fifo_used_words      :12; /*  0:11 */
            MEA_Uint32 reserved1                    : 3;/*12:14*/
            MEA_Uint32 ingress_fifo_overflow        : 1; /*15*/
            MEA_Uint32 hold_fifo_used_words        : 12;/*16:23*/
            MEA_Uint32 reserved2                   : 6; /*24:29*/
            MEA_Uint32 hold_fifo_underflow          : 1; /*30*/
            MEA_Uint32 hold_fifo_overflow           : 1; /*31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 hold_fifo_overflow           : 1; /*31*/
            MEA_Uint32 hold_fifo_underflow          : 1; /*30*/
            MEA_Uint32  reserved2                   : 6; /*24:29*/
            MEA_Uint32  hold_fifo_used_words        : 12;/*16:23*/
            MEA_Uint32 ingress_fifo_overflow        : 1; /*15*/
            MEA_Uint32 reserved1                    : 3;/*12:14*/
            MEA_Uint32 ingress_fifo_used_words      :12; /*  0:11 */

#endif

 

#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 read_data_fifo_used_words    : 8; /* 39:32*/
            MEA_Uint32 reserved3                    : 6; /*45:40*/
            MEA_Uint32 read_data_fifo_underflow     : 1; /* 46*/
            MEA_Uint32 read_data_fifo_overflow      : 1; /* 47*/
            MEA_Uint32 hold_fifo_unmach_error       : 1; /* 48*/
            MEA_Uint32 reserved4                    : 1; /* 49*/
            MEA_Uint32 read_data_fifo_last_error    : 1; /* 50*/
            MEA_Uint32 reserved5                    :13; /*63:51*/

#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 reserved5                    :13; /*63:51*/
            MEA_Uint32 read_data_fifo_last_error    : 1; /* 50*/
            MEA_Uint32 reserved4                    : 1; /* 49*/
            MEA_Uint32 hold_fifo_unmach_error       : 1; /* 48*/
            MEA_Uint32 read_data_fifo_overflow      : 1; /* 47*/
            MEA_Uint32 read_data_fifo_underflow     : 1; /* 46*/
            MEA_Uint32 reserved3                    : 6; /*45:40*/
            MEA_Uint32 read_data_fifo_used_words : 8; /* 39:32*/
#endif


        } val;
        MEA_Uint32 reg[2];
    } bm_events60_39;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            
        MEA_Uint32 edt_on_ddr_hold_fifo_used_words          : 8; /*0 7*/
        MEA_Uint32 edt_on_ddr_ddr_read_commands_usedw       : 8; /*8 15*/
        MEA_Uint32 edt_on_ddr_ddr_read_data_fifo_usedw      : 8; /*16 23*/
        MEA_Uint32 edt_on_ddr_pad                           : 2; /*24 25*/
        MEA_Uint32 edt_on_ddr_ddr_read_data_fifo_underflow  : 1; /*26*/
        MEA_Uint32 edt_on_ddr_ddr_read_data_fifo_overflow   : 1; /*27*/
        MEA_Uint32 edt_on_ddr_ddr_read_command_underflow    : 1; /*28*/
        MEA_Uint32 edt_on_ddr_ddr_read_command_overflow     : 1; /*29*/
        MEA_Uint32 edt_on_ddr_hold_fifo_underflow           : 1; /*30*/
        MEA_Uint32 edt_on_ddr_hold_fifo_overflow            : 1; /*31*/

#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 edt_on_ddr_hold_fifo_overflow : 1; /*31*/
            MEA_Uint32 edt_on_ddr_hold_fifo_underflow : 1; /*30*/
            MEA_Uint32 edt_on_ddr_ddr_read_command_overflow : 1; /*29*/
            MEA_Uint32 edt_on_ddr_ddr_read_command_underflow : 1; /*28*/
            MEA_Uint32 edt_on_ddr_ddr_read_data_fifo_overflow : 1; /*27*/
            MEA_Uint32 edt_on_ddr_ddr_read_data_fifo_underflow : 1; /*26*/
            MEA_Uint32 edt_on_ddr_pad : 2; /*24 25*/
            MEA_Uint32 edt_on_ddr_ddr_read_data_fifo_usedw : 8; /*16 23*/
            MEA_Uint32 edt_on_ddr_ddr_read_commands_usedw : 8; /*8 15*/


#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 edt_on_ddr_tm_TM_hold_fifo_usedw     : 8; /* 32..39 */
            MEA_Uint32 edt_on_ddr_tm_TM_hold_fifo_overflow  : 1; /*40*/
            MEA_Uint32 edt_on_ddr_tm_TM_hold_fifo_undeflow  : 1; /*41*/
            MEA_Uint32 edt_on_ddr_tm_pad                    :23;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 edt_on_ddr_tm_pad                   : 23;
            MEA_Uint32 edt_on_ddr_tm_TM_hold_fifo_undeflow : 1; /*41*/
            MEA_Uint32 edt_on_ddr_tm_TM_hold_fifo_overflow : 1; /*40*/
            MEA_Uint32 edt_on_ddr_tm_TM_hold_fifo_usedw    : 8; /* 32..39 */

#endif

              





        } val;
        MEA_Uint32 reg[2];
    } bm_events60_40;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 ing_pkt_byte_count : 16; /*  0..15 */
            MEA_Uint32 ing_mtu            : 16; /*16 :31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 ing_mtu            : 16; /*16 :31*/
            MEA_Uint32 ing_pkt_byte_count : 16; /*  0..15 */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 queue_MTU                                        : 8;
            MEA_Uint32 original_packet_DF_bit                           : 1;
            MEA_Uint32 original_packet_is_IP_fragment                   : 1;
            MEA_Uint32 packet_should_sent_by_tunnel                     : 1;
            MEA_Uint32 packet_should_be_fragmented                      : 1;
            MEA_Uint32 packet_IPv6                                      : 1;
            MEA_Uint32 packet_IPv4                                      : 1;
            MEA_Uint32 packet_should_be_dis_by_fragmentation_alg        : 1;
            MEA_Uint32 packet_should_be_fwd_and_frag_by_frag_alg        : 1;
            MEA_Uint32 pad                                              : 16;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 pad : 16;
            MEA_Uint32 packet_should_be_fwd_and_frag_by_frag_alg : 1;
            MEA_Uint32 packet_should_be_dis_by_fragmentation_alg : 1;
            MEA_Uint32 packet_IPv4 : 1;
            MEA_Uint32 packet_IPv6 : 1;
            MEA_Uint32 packet_should_be_fragmented      : 1;
            MEA_Uint32 packet_should_sent_by_tunnel     : 1;
            MEA_Uint32 original_packet_is_IP_fragment   : 1;
            MEA_Uint32 original_packet_DF_bit : 1;
            MEA_Uint32 queue_MTU              : 8;
#endif







        } val;
        MEA_Uint32 reg[2];
    } bm_events60_41;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count0; /*  0..31 */
#else
            MEA_Uint32 count0; /*  0..31 */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count1; /* 32..63 */
#else
            MEA_Uint32 count1; /* 32..63 */
#endif


        } val;
        MEA_Uint32 reg[2];
    } bm_events60_42;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count0; /*  0..31 */
#else
            MEA_Uint32 count0; /*  0..31 */
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 count1; /* 32..63 */
#else
            MEA_Uint32 count1; /* 32..63 */
#endif


        } val;
        MEA_Uint32 reg[2];
    } bm_events60_43;


    MEA_Uint32  BM_descriptor_buffer_status;
    
    MEA_Uint32  BM_descriptor_count_status;
    MEA_Uint32  BM_buffer_count_status;
    
    MEA_BM_ack_info ack_info;
    
    

    MEA_Uint8     drop_MTU_priq[256]; /*one bit per priQueue*/
    MEA_Events_System_Entry_dbt system_events;

    MEA_Uint32  system_status;

} MEA_Events_Entry_dbt;

typedef struct{
    MEA_Uint64 acl_key[MEA_ACL_HIERARC_NUM_OF_FLOW];
    MEA_Uint8  result[MEA_ACL_HIERARC_NUM_OF_FLOW];
    MEA_Uint8  doAcl;
    MEA_Uint8  command;
    MEA_Uint32  last_Sid;
} MEA_Events_ACL_key_dbt;

typedef struct{

    MEA_Events_ACL_key_dbt Last;
    MEA_Events_ACL_key_dbt unmatch;

} MEA_Events_ACL_dbt;




MEA_Status MEA_API_Get_EgressMACPortReady(MEA_egressMacReady_t *entry_o);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */

MEA_Status MEA_API_Get_ACL_Events(MEA_Unit_t   unit,  MEA_Events_ACL_dbt *entry);

MEA_Status MEA_API_Get_ACL5_Events(MEA_Unit_t   unit, MEA_Events_ACL5_dbt *entry, MEA_ACL5_PAC_MODE ACL5_parser_mode, MEA_Bool *valid);

MEA_Status MEA_API_BM_INFO_ACK(MEA_Unit_t    unit, MEA_BM_ack_info *entry);



MEA_Status MEA_API_Get_WRITE_ENGINE_STATE(MEA_Unit_t   unit, MEA_Uint32 *state);
MEA_Status MEA_API_Clear_WRITE_ENGINE_STATE(MEA_Unit_t   unit);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Events       APIs                                      */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Events_System_Entry  (MEA_Unit_t             unit,
                                             MEA_Events_System_Entry_dbt*  entry);
MEA_Status MEA_API_Get_Events_Entry (MEA_Unit_t                  unit,
                                     MEA_Events_Entry_dbt*       entry);

MEA_Status MEA_API_Get_Events_Drop_MTU_Priq(MEA_Unit_t      unit_i,
                                            ENET_QueueId_t  QueueId_i,
                                            MEA_Uint8*      drop_MTU_priq); /* 1 bit per priQ */

MEA_Status MEA_API_Get_Current_Counters_BM  (MEA_Unit_t           unit,
                                         MEA_Current_Counters_BM_dbt* entry);

MEA_Status MEA_API_GetFirst_Mapping_Profile (MEA_Unit_t       unit,
                                             MEA_Uint32      *profile_id_o,                                           
                                             MEA_Bool        *found_o);


MEA_Status MEA_API_GetNext_Mapping_Profile  (MEA_Unit_t       unit,
                                           MEA_Uint32       profile_id_i,
                                           MEA_Uint32      *profile_id_o,    
                                           MEA_Bool        *found_o);

/************************************************************************/
/*                                                                      */
/************************************************************************/

typedef  struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 NEXT_POINTER_BANK :3;
    MEA_Uint32 NEXT_POINTER      :12;
    MEA_Uint32 Trigger          : 1;
    MEA_Uint32 AXI_BANK_ADDR    : 3;
    MEA_Uint32 Packet_length    : 11;
    MEA_Uint32 pad0 : 2;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad0             : 2;
    MEA_Uint32 Packet_length    :11;
    MEA_Uint32 AXI_BANK_ADDR     :3;
    MEA_Uint32 Trigger          : 1;
    MEA_Uint32 NEXT_POINTER      :12;
    MEA_Uint32 NEXT_POINTER_BANK :3;
#endif



#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 AXI_ADDRESS      :13;
    MEA_Uint32 AXI_HoldTime     :11;
    MEA_Uint32 pad1 : 8;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
    MEA_Uint32 pad1             : 8;
    MEA_Uint32 AXI_HoldTime     :16;
    MEA_Uint32 AXI_ADDRESS      :11;
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 AXI_TimeStamp    :24;
    MEA_Uint32 type            :8;/* 0 write 1 read */
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
    MEA_Uint32 type             :8;/* 0 write 1 read */
    MEA_Uint32 AXI_TimeStamp    :24;
#endif

    


    MEA_Uint32 index;

}MEA_debugLogerAxiEntry_dbt;


typedef  struct {
    MEA_Uint32 Des_write_NEXT_POINTER_BANK ;  // 3
    MEA_Uint32 Des_write_NEXT_POINTER      ;      //15
    MEA_Uint32 Des_write_Trigger            ; //1
    MEA_Uint32 Des_write_AXI_BANK_ADDR     ;    //3
    MEA_Uint32 Des_write_AXI_ADDRESS       ;      //13
    MEA_Uint32 Des_write_AXI_HoldTime      ;      //7 //16
    MEA_Uint32 Des_write_AXI_Packet_length ;      //11
    MEA_Uint32 Des_write_AXI_TimeStamp     ;    //22  //24
    
    MEA_Uint32 Des_read_NEXT_POINTER_BANK  ;    //3
    MEA_Uint32 Des_read_NEXT_POINTER       ;      //15
    MEA_Uint32 Des_read_Trigger            ; //1
    MEA_Uint32 Des_read_AXI_BANK_ADDR      ;     // 3
    MEA_Uint32 Des_read_AXI_ADDRESS        ;       // 13
    MEA_Uint32 Des_read_AXI_HoldTime       ;     //7 //16
    MEA_Uint32 Des_read_AXI_Packet_length  ;//11
    MEA_Uint32 Des_read_AXI_TimeStamp      ;    //22 //24


    
} MEA_logDesEntry_dbt;


MEA_Status MEA_API_Get_logDescInfo(MEA_Unit_t unit, MEA_Uint32 index, MEA_logDesEntry_dbt   *logEntry);
MEA_Status MEA_API_Clear_logDescInfo(MEA_Unit_t unit, MEA_Uint32 index);
MEA_Status MEA_API_collectDesAXI_logDescInfo(MEA_Unit_t unit);
MEA_Status MEA_API_Get_loggerAXI_DescInfo(MEA_Unit_t unit, MEA_Uint32 index, MEA_debugLogerAxiEntry_dbt   *logEntry);

/************************************************************************/
/*                                                                      */
/************************************************************************/
typedef  enum{
    MEA_SERDES_STATE_LINKUP=0,
    MEA_SERDES_STATE_POWER_DOWN =1,
    MEA_SERDES_STATE_PLL_LOCK=2,
    MEA_SERDES_STATE_BUSY=3,
    MEA_SERDES_STATE_FREQUENCY_LOCK=4,
    MEA_SERDES_STATE_WAIT=5,
    MEA_SERDES_STATE_STUCK=6,
    MEA_SERDES_STATE_RESERVED=7,
    MEA_SERDES_STATE_LAST
}MEA_serdes_State_t;







typedef struct{
    MEA_Uint8  InterfaceType;

    union{
        
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
                MEA_Uint32 reset_state                  :3;
                MEA_Uint32 sync_status                  :1;
                MEA_Uint32 freq_locked                  :1;
                MEA_Uint32 pll_lck                      :1;
                MEA_Uint32 error_detect                 :1;
                MEA_Uint32 disperr                      :1;
                MEA_Uint32 pad_qxaui                    :24;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
                MEA_Uint32 pad_qxaui                    :24;
                MEA_Uint32 disperr                      :1;
                MEA_Uint32 error_detect                 :1;
                MEA_Uint32 pll_lck                      :1;
                MEA_Uint32 freq_locked                  :1;
                MEA_Uint32 sync_status                  :1;
                MEA_Uint32 reset_state                  :3;
#endif

            }mea_Gbe;
    struct {
    #if __BYTE_ORDER == __LITTLE_ENDIAN
                MEA_Uint32 pcs2xmacrx_syncstatus        :8;
                MEA_Uint32 pcs2xmacrx_freqlocked        :4;
                MEA_Uint32 pcs2xmacrx_channelaligned    :1;
                MEA_Uint32 pcs2xmacrx_pll_locked        :1;
                MEA_Uint32 PCS_sync_state_machine       :3;   
                MEA_Uint32 pcs_digital_rst              :1;
                MEA_Uint32 pcs_analog_rst               :1;
                MEA_Uint32 pcs_pwrdwn                   :1;
                MEA_Uint32 pad_xaui                     :12;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    #else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
                MEA_Uint32 pad_xaui                     :12;
                MEA_Uint32 pcs_pwrdwn                   :1;
                MEA_Uint32 pcs_analog_rst               :1;
                MEA_Uint32 pcs_digital_rst              :1;
                MEA_Uint32 PCS_sync_state_machine       :3;
                MEA_Uint32 pcs2xmacrx_pll_locked        :1;
                MEA_Uint32 pcs2xmacrx_channelaligned    :1;
                MEA_Uint32 pcs2xmacrx_freqlocked        :4;
                MEA_Uint32 pcs2xmacrx_syncstatus        :8;

    #endif

            }mea_XAUI;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 reset_state                  :3;
            MEA_Uint32 sync_status                  :1;
            MEA_Uint32 freq_locked                  :1;
            MEA_Uint32 pll_lck                      :1;
            MEA_Uint32 error_detect                 :1;
            MEA_Uint32 disperr                      :1;
            MEA_Uint32 pad_qxaui                    :24;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad_qxaui                    :24;
            MEA_Uint32 disperr                      :1;
            MEA_Uint32 error_detect                 :1;
            MEA_Uint32 pll_lck                      :1;
            MEA_Uint32 freq_locked                  :1;
            MEA_Uint32 sync_status                  :1;
            MEA_Uint32 reset_state                  :3;


#endif
        }mea_Qxaui;
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32   utopia_syncOk       :1;  //[0]
        MEA_Uint32   Pad                 :31;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32   Pad                 :31;
        MEA_Uint32   utopia_syncOk       :1;  //[0]
#endif


	}mea_Utopia;

    struct {

#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 tx_local_fault           :1;
            MEA_Uint32 rx_local_fault           :1;
            MEA_Uint32 sync_status              :3;
            MEA_Uint32 align_status             :1;
            MEA_Uint32 rx_link_status           :1;
            MEA_Uint32 pad                      :2;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad                      :2;
            MEA_Uint32 rx_link_status           :1;
            MEA_Uint32 align_status             :1;
            MEA_Uint32 sync_status              :3;
            MEA_Uint32 rx_local_fault          :1;
            MEA_Uint32 tx_local_fault           :1;

#endif
    }mea_rxauvi;
        
    }Status_Event;    

}MEA_Port_Status_Event_t;

MEA_Status MEA_API_Port_Status(MEA_Unit_t      unit_i,
                               MEA_Port_t       port,
                               MEA_Port_Status_Event_t        *eventStatus);





/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Write/Read Reg APIs                                    */
/*                                                                               */
/*  Note: This APIs are very low level APIs                                      */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef enum {
    DDR_Action = 0,
    DDR_EDITING = 1,
    DDR_Service_EXT = 2,
    DDR_PMS = 3,
    DDR_ACL5 = 4,

    DDR_MODULE_LAST
}MEA_DDR_module;

typedef enum{
    MEA_MODULE_CPLD = 0,
    MEA_MODULE_IF   = 1,
    MEA_MODULE_BM   = 2,
    MEA_MODULE_DATA_DDR =3,
    MEA_MODULE_DOWNSTREEM_OFFSET=0x50,
    MEA_MODULE_IF_DOWNSTREEM=(MEA_MODULE_DOWNSTREEM_OFFSET+MEA_MODULE_IF),
    MEA_MODULE_BM_DOWNSTREEM=(MEA_MODULE_DOWNSTREEM_OFFSET+MEA_MODULE_BM),
    MEA_MODULE_LAST
}MEA_module_te;  

void         MEA_API_WriteReg               (MEA_Unit_t   unit,
                                            MEA_Uint32   Addr,
                                            MEA_Uint32   data,
                                            MEA_module_te mea_module);

MEA_Uint32  MEA_API_ReadReg                (MEA_Unit_t   unit,
                                            MEA_Uint32   Addr,
                                            MEA_module_te mea_module);


#define ENET_WriteReg MEA_API_WriteReg
#define ENET_ReadReg MEA_API_ReadReg

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA WriteIndirect/ReadIndirect Tables APIs                 */
/*                                                                               */
/*  Note: This APIs are very low level APIs                                      */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

      
      
      
typedef struct{
    MEA_Uint32 tableType;      /* The table type */
    MEA_Uint32 tableOffset;     /* The Offset in the table */
    MEA_Uint32 cmdReg;         /* cmdReg - command register (read/write operation) */
    MEA_Uint32 cmdMask;         /* cmdMask - masks the command bit in previous register.  */
    MEA_Uint32 statusReg;       /* statusReg - status register to be check after write.  */
    MEA_Uint32 statusMask;     /* statusMask - status register mask bit                  */
    MEA_Uint32 tableAddrReg; /* tableAddrReg - the table address register               */
    MEA_Uint32 tableAddrMask;/* tableAddrMask - masks the table address (the ~tableAddrMask masks the table type) */
    MEA_FUNCPTR writeEntry;     /* writeEntry - callback to the function which preform the table entry write    */
    MEA_funcParam  funcParam1;  /* callback first parameter                                                      */
    MEA_funcParam  funcParam2;  /* callback second parameter                                                     */
    MEA_funcParam  funcParam3;  /* callback third parameter                                                     */
    MEA_funcParam  funcParam4;  /* callback third parameter                                                     */
   
    
    MEA_funcParam   numofregs;
    MEA_funcParam   retval;  /* callback third parameter                                                     */
}MEA_ind_write_t;

#define ENET_ind_write_t MEA_ind_write_t


typedef struct{
    MEA_Uint32 tableType;      /* The table type */
    MEA_Uint32 tableOffset;     /* The Offset in the table */
    MEA_Uint32 cmdReg;         /* cmdReg - command register (read/write operation) */
    MEA_Uint32 cmdMask;         /* cmdMask - masks the command bit in previous register.  */
    MEA_Uint32 statusReg;       /* statusReg - status register to be check after write.  */
    MEA_Uint32 statusMask;     /* statusMask - status register mask bit                  */
    MEA_Uint32 tableAddrReg; /* tableAddrReg - the table adress register               */
    MEA_Uint32 tableAddrMask;/* tableAddrMask - masks the table address (the ~tableAddrMask masks the table type) */
    MEA_Uint32  *read_data;  /* points to the data that should be read                                                      */
}MEA_ind_read_t;






MEA_Status  MEA_API_WriteIndirect(MEA_Unit_t     unit,
                                  MEA_ind_write_t *p_ind_write,
                                  MEA_module_te mea_module);

MEA_Status  MEA_API_ReadIndirect(MEA_Unit_t      unit,
                                 MEA_ind_read_t *p_ind_read,
                                 MEA_Uint32      num_of_regs,
                                 MEA_module_te   mea_module);


MEA_Status MEA_API_Read_IFIndirect(MEA_Unit_t             unit,
                                    MEA_Uint32             TableId,
                                    MEA_Uint32             tableOffset,
                                    MEA_Uint32             num_of_regs,
                                    MEA_Uint32             *arry);


MEA_Uint32 MEA_API_Read_TMUX(MEA_Unit_t unit,MEA_Uint32 Addr);
void  MEA_API_Write_TMUX(MEA_Unit_t unit,MEA_Uint32 Addr,MEA_Uint32   data);


MEA_Status MEA_PCIe_WRITE_AXI(int Addr, int Write_val,int bar);
MEA_Status MEA_PCIe_READ_AXI(int Addr_read, int *get_value, int bar);



/************************************************************************/
/*    MEA_API_GPIO_Z70_RW                                               */
/*     read_write  0-read 1-write                                       */
/*     *value      return/ set value                                    */
/************************************************************************/

MEA_Status MEA_API_GPIO_Z70_RW(MEA_Unit_t unit, MEA_Uint8 read_write, MEA_Uint32 *value);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Scheduler Table Defines                                */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_SCHEDULER_TABLE_SIZE 512

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Scheduler Table Typedefs                               */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    MEA_Bool   valid;
    MEA_Uint8  type;  //0-src 1-Virtual 
    MEA_Port_t port;
} MEA_Scheduler_Entry_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Scheduler Table APIs                                   */
/*                                                                               */
/*  Note: This APIs are very low level APIs                                      */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Get_Scheduler_Entry (MEA_Unit_t unit,
                                        MEA_Uint32 direction,  /* Use MEA_DIRECTION_XXX */
                                        MEA_Uint32 slot,
                                        MEA_Scheduler_Entry_dbt* entry);

MEA_Status MEA_API_Set_Scheduler_Entry (MEA_Unit_t unit,
                                        MEA_Uint32 direction,  /* Use MEA_DIRECTION_XXX */
                                        MEA_Uint32 slot,
                                        MEA_Scheduler_Entry_dbt* entry);

MEA_Status MEA_API_Set_Scheduler_Length(MEA_Unit_t unit_i,
                                        MEA_Uint32 direction_i,
                                        MEA_Uint32 length_i);

MEA_Status MEA_API_Get_Scheduler_Length(MEA_Unit_t  unit_i,
                                        MEA_Uint32  direction_i,
                                        MEA_Uint32 *length_o);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA PolicerTicks Defines                                   */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_POLICER_LAST_FAST_SERVICE        15
#define MEA_POLICER_ACTION_NUM_OF_CYCLES    6 

#define MEA_TICKS_FOR_FAST_SERVICE_MIN_VAL  \
           ((MEA_POLICER_LAST_FAST_SERVICE+1) * \
            MEA_POLICER_ACTION_NUM_OF_CYCLES)  /* 40 */

#define MEA_TICKS_FOR_FAST_SERVICE_MAX_VAL  (0x0000ffff) /* 16 bits */

#if 0
#define MEA_TICKS_FOR_SLOW_SERVICE_MIN_VAL(unit_i,hwUnit_i)  \
           ((mea_Get_PolicersGroup((unit_i),(hwUnit_i)) \
             -(MEA_POLICER_LAST_FAST_SERVICE+1))* \
            MEA_POLICER_ACTION_NUM_OF_CYCLES)  /* 8152 */
#endif

#define MEA_TICKS_FOR_SLOW_SERVICE_MIN_VAL(unit_i,hwUnit_i)  ((mea_Get_PolicersGroup(0, 0) * MEA_POLICER_ACTION_NUM_OF_CYCLES) )


#define MEA_TICKS_FOR_SLOW_SERVICE_MAX_VAL  (0x000fffff) /* 16 bits */


/*************************************************************************/

#define MEA_ING_FLOW_POLICER_LAST_FAST        0
#define MEA_ING_FLOW_POLICER_ACTION_NUM_OF_CYCLES    6 

#define MEA_ING_FLOW_TICKS_FOR_FAST_MIN_VAL  \
           ((MEA_ING_FLOW_POLICER_LAST_FAST+1) * \
            MEA_ING_FLOW_POLICER_ACTION_NUM_OF_CYCLES)  /* 40 */

#define MEA_ING_FLOW_TICKS_FOR_FAST_MAX_VAL  (0x0000ffff) /* 16 bits */

#define MEA_ING_FLOW_TICKS_FOR_SLOW_MIN_VAL(unit_i,hwUnit_i)  \
           ((mea_drv_Get_DeviceInfo_NumOFIngress_flowPolicers((unit_i),(hwUnit_i)) \
             -(MEA_ING_FLOW_POLICER_LAST_FAST+1))* \
            MEA_ING_FLOW_POLICER_ACTION_NUM_OF_CYCLES)  /* 8152 */

#define MEA_ING_FLOW_TICKS_FOR_SLOW_MAX_VAL  (0x000fffff) /* 16 bits */





/*************************************************************************/
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA PolicerTicks Typedefs                                  */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
   MEA_Uint32 ticks_for_slow ;
   MEA_Uint32 ticks_for_fast ;
} MEA_PolicerTicks_Entry_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA PolicerTicks APIs                                      */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_PolicerTicks_Entry (MEA_Unit_t                  unit,
                                           MEA_PolicerTicks_Entry_dbt* entry);

MEA_Status MEA_API_Get_PolicerTicks_Entry (MEA_Unit_t                  unit,
                                           MEA_PolicerTicks_Entry_dbt* entry);


MEA_Status MEA_API_Set_IngressFlowPolicerTicks_Entry(MEA_Unit_t                  unit,
    MEA_PolicerTicks_Entry_dbt* entry);

MEA_Status MEA_API_Get_IngressFlowPolicerTicks_Entry(MEA_Unit_t                  unit,
    MEA_PolicerTicks_Entry_dbt* entry);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Global Defines  Interface_G999_1                       */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

typedef struct {
    union {
        struct {

#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 Fragment_999_threshold       :10; /* 0..9 */
            MEA_Uint32 Fragment_999_pause_enable    :1; /* 10..10 */
            MEA_Uint32 pad                          : 5; /*11 15*/
            MEA_Uint32 mng_channel                  : 7; /*15 22*/
			MEA_Uint32 packet_9991_threshold        :8; /*23 30*/
            MEA_Uint32 increment_enable             :1; /* 31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

            MEA_Uint32 increment_enable             :1; /* 31 */
			MEA_Uint32 packet_9991_threshold        :8; /*23 30*/
            MEA_Uint32 mng_channel                  :7; /*15 22*/
            MEA_Uint32 pad                          :5; /*11 15*/
            MEA_Uint32 Fragment_999_pause_enable    :1 ; /* 10..10 */ 
            MEA_Uint32 Fragment_999_threshold       :10; /* 0..9 */ 
#endif
        } val;

        MEA_Uint32 reg; 

    } if_G999_1_th_config;



}MEA_Global_Interface_G999_1_dbt;


MEA_Status MEA_API_Set_Global_Interface_G999_1_Entry (MEA_Unit_t                  unit,
                                                      MEA_Global_Interface_G999_1_dbt*      entry);

MEA_Status MEA_API_Get_Global_Interface_G999_1_Entry (MEA_Unit_t                            unit,
                                                      MEA_Global_Interface_G999_1_dbt*      entry);


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Globals Defines                                        */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_GLOBALS_OAM_NUM_OF_ENTRIES 8

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Globals      Typedefs                                  */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

typedef enum{
    MEA_FWD_TBL_HASH_TYPE_0    = 0,
    MEA_FWD_TBL_HASH_TYPE_1    = 1,
    MEA_FWD_TBL_HASH_TYPE_LAST 
} MEA_Fwd_Tbl_HashType_t;




/* WRED Shift value for calculate AQS (Average Queue Size) */
typedef enum {
    MEA_GLOBAL_WRED_SHIFT_K_EQ_6 = 0,
    MEA_GLOBAL_WRED_SHIFT_K_EQ_7 = 1,
    MEA_GLOBAL_WRED_SHIFT_K_EQ_8 = 2,
    MEA_GLOBAL_WRED_SHIFT_K_EQ_9 = 3
} MEA_Global_WredShift_te;

typedef enum {
    MEA_GLOBAL_STP_MODE_DISABLE = 0,
    MEA_GLOBAL_STP_MODE_STP = 1,
    MEA_GLOBAL_STP_MODE_MSTP = 2,
    MEA_GLOBAL_STP_MODE_LAST = 3
}MEA_Global_STP_mode_te;

typedef enum {
    MEA_GLOBAL_FRAGMENT_CHUNK_128 = 0,
    MEA_GLOBAL_FRAGMENT_CHUNK_256 = 1,
    MEA_GLOBAL_FRAGMENT_CHUNK_512 = 2,
    MMEA_GLOBAL_FRAGMENT_CHUNK_LAST = 3
}MEA_Global_fragmentChunk_te;


typedef     union {

    struct {



#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 AF_threshold         : 8; /*  0.. 7 */
        MEA_Uint32 tx_threshold         : 8; /*  8..15 */
        MEA_Uint32 RX_mac_threshold     : 8; /* 16..23 */
        MEA_Uint32 rx_pre_EOC_threshold : 4; /* 24..27 */
        MEA_Uint32 pad_threshold        : 4; /* 28..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

        MEA_Uint32 pad_threshold        : 4; /* 28..31 */
        MEA_Uint32 rx_pre_EOC_threshold : 4; /* 24..27 */
        MEA_Uint32 RX_mac_threshold     : 8; /* 16..23 */
        MEA_Uint32 tx_threshold         : 8; /*  8..15 */
        MEA_Uint32 AF_threshold         : 8; /*  0.. 7 */
#endif

    } val;

    MEA_Uint32 reg;

} MEA_rx_tx_mac_thresholds_t;



typedef struct {
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 Rx1 : 8;
            MEA_Uint32 Rx2 : 8;
            MEA_Uint32 Tx1 : 4;
            MEA_Uint32 Tx2 : 4;
            MEA_Uint32 Tx3 : 4;
            MEA_Uint32 Tx4 : 4;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 Tx4 :4;
            MEA_Uint32 Tx3 :4;
            MEA_Uint32 Tx2 :4;
            MEA_Uint32 Tx1 :4;
            MEA_Uint32 Rx2 :8;
            MEA_Uint32 Rx1 :8;
#endif
        } val;
        MEA_Uint32 reg[1];
    }polling;
}MEA_pos2_config_polling;

typedef struct {

    
    /* word 1 - descriptors thresholds */
    MEA_Uint32 max_used_descriptors_high : 14;
    MEA_Uint32 pad3                      :  2;
    MEA_Uint32 max_used_descriptors_low  : 14;
    MEA_Uint32 pad4                      :  2;
    
    

    /* word 2 - buffers thresholds */
    MEA_Uint32 max_used_buffers_high     : 14;
    MEA_Uint32 pad5                      :  2;
    MEA_Uint32 max_used_buffers_low      : 14;
    MEA_Uint32 pad_w2                    :  2;
 
    /* word 3,4 - L2CP base address */        
    MEA_MacAddr network_L2CP_BaseAddr;

    /* word 5,6 - L2TP Tunnel address */        
    MEA_MacAddr L2TP_Tunnel_address;


    
    MEA_Uint32 Periodic_enable          :  1;
    MEA_Uint32 portEgress_Link_update   :  1;
    MEA_Uint32 portIngress_Link_update  :  1;

    MEA_Uint32 portBondingLink_update   :  1;

    

    union{
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 Counters_PMs             :1;
        MEA_Uint32 Counters_Queues          :1;
        MEA_Uint32 Counters_BMs             :1;
        MEA_Uint32 Counters_IngressPorts    :1;
        MEA_Uint32 Counters_RMONs           :1;
        MEA_Uint32 Counters_ANALYZERs       :1;
        MEA_Uint32 Counters_LMs             :1;
        MEA_Uint32 Counters_CCMs            :1;
        MEA_Uint32 Counters_PMs_TDM         :1;
        MEA_Uint32 Counters_RMONs_TDM       :1;
        MEA_Uint32 eventCes                 :1;
        MEA_Uint32 Counters_UTOPIA_RMONs    :1;
        MEA_Uint32 Counters_EgressFifo_TDM  :1;
        MEA_Uint32 Counters_Bonding         :1;
        MEA_Uint32 Counters_forwarder       :1;
        MEA_Uint32 Counters_Ingress         :1;
        MEA_Uint32 Counters_HC_RMON         :1;
        MEA_Uint32 Counters_ACL             :1;
        MEA_Uint32 Counters_QUEUE_HANDLER   : 1;
        MEA_Uint32 Counters_Virtual_Rmon    : 1;
        MEA_Uint32 pad_Counters             :12;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            
        MEA_Uint32 pad_Counters             :12;
        MEA_Uint32 Counters_Virtual_Rmon    :1;
        MEA_Uint32 Counters_QUEUE_HANDLER   :1;
        MEA_Uint32 Counters_ACL             :1;
        MEA_Uint32 Counters_HC_RMON         :1;
        MEA_Uint32 Counters_Ingress         :1;
        MEA_Uint32 Counters_forwarder       :1; 
        MEA_Uint32 Counters_Bonding         :1;
        MEA_Uint32 Counters_EgressFifo_TDM   :1;
        MEA_Uint32 Counters_UTOPIA_RMONs    :1;
        MEA_Uint32 eventCes                 :1;
        MEA_Uint32 Counters_RMONs_TDM       :1;
        MEA_Uint32 Counters_PMs_TDM         :1;
        MEA_Uint32 Counters_CCMs            :1;
        MEA_Uint32 Counters_LMs             :1;
        MEA_Uint32 Counters_ANALYZERs       :1;
        MEA_Uint32 Counters_RMONs           :1;
        MEA_Uint32 Counters_IngressPorts    :1;
        MEA_Uint32 Counters_BMs             :1;
        MEA_Uint32 Counters_Queues          :1;
        MEA_Uint32 Counters_PMs             :1;
#endif
    }val;
        MEA_Uint32 reg;

    } Counters_Enable;

    union {

      struct {



#if __BYTE_ORDER == __LITTLE_ENDIAN

         MEA_Uint32 ipg_gmii              : 8; /*  0.. 7 */
         MEA_Uint32 ipg_mii               : 8; /*  8..15 */
         MEA_Uint32 h2dse_lmt_enable      : 1;  /* 16..16 */ 
         MEA_Uint32 generator_enable      : 1;  /* 17..17 */
         MEA_Uint32 fwd_act_offset        : 2; /* 18..19 */ 
         MEA_Uint32 srv_external_hash_function : 2; /* 20..21 */
         MEA_Uint32 en_ddr_nic            : 1; /* 22..22 */
         MEA_Uint32 en_HPM                : 1; /* 23..23 */
         MEA_Uint32 semi_back_pressure    : 6; /* 24..29 */
         MEA_Uint32 vdsl_conf             : 1; /* 30..30 */
         MEA_Uint32 pause_enable          : 1; /* 31..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32 pause_enable          : 1; /* 31..31 */   
         MEA_Uint32 vdsl_conf             : 1; /* 30..30 */  
         MEA_Uint32 semi_back_pressure    : 6; /* 24..29 */ 
         MEA_Uint32 en_HPM                : 1; /* 23..23 */
         MEA_Uint32 en_ddr_nic            : 1; /* 22..22 */
         MEA_Uint32 srv_Eternal_hash_function : 2; /* 20..21 */
         MEA_Uint32 fwd_act_offset        : 2; /* 18..19 */ 
         MEA_Uint32 generator_enable      : 1;  /* 17..17 */
         MEA_Uint32 h2dse_lmt_enable      : 1;  /* 16..16 */ 
         MEA_Uint32 ipg_mii               : 8; /*  8..15 */
         MEA_Uint32 ipg_gmii              : 8; /*  0.. 7 */
#endif

      } val;

      MEA_Uint32 reg; 

    } if_global0;



    union {

      struct {


#if __BYTE_ORDER == __LITTLE_ENDIAN

         MEA_Uint32 Dse_Output_Algoritm : 1;  /*bit0 */
         MEA_Uint32 reserved            : 1;   /*bit1 */
         MEA_Uint32 Lxcp_Enable         : 1;   /*bit2 */ 
         MEA_Uint32 PS_To_En_127        : 1;  /*bit3 */
         MEA_Uint32 reserved_4          : 1;  /*bit4 */
         MEA_Uint32 reserved_5          : 1;  /*bit5 */
         MEA_Uint32 reserved_6          : 1;  /*bit6 */

         /* FWD next command: any transaction indicate 
            read of next valid entry.
            Toggling that bit cause the Search Engine 
            to run across  the address space  looking 
            for next valid entry */
         MEA_Uint32 forwarder_next_cmd : 1; /*  7.. 7 */

         MEA_Uint32 drop_unmatch_pkts  : 1; /* 8.. 8 */

         MEA_Uint32 learnPBB_info_Enable : 1;  /*bit9 */
         MEA_Uint32 learning_disable     : 1;  /*bit10 */
          

         MEA_Uint32 rstp_enable         : 1; /* 11..11 */

         
         MEA_Uint32 link_up_mask : 1; /* 12..12 */


         /* Init forwarder external memory: 
            0 to 1 reset forwarder external memory.
            The operation cause the forwarder block to write 
            zeros to all of the DDR SDRAM address space */
         MEA_Uint32 forwarder_flush    : 1; /* 13..13 */

         /* Forwarder enable: '0' disable  / '1' enable  
            Default is '1'-enable. */
         MEA_Uint32 forwarder_enable   : 1; /* 14..14 */

         /* Utopia Master to Master loop: 
            '0' no loop (default) / 1' loop
             Cell coming from the Utopia master Rx interface are 
             loop back to Utopia TX.
             This mean no chip logic , only Utopia is working. */
         MEA_Uint32 close_loop_utopia_master_tx_to_master_rx : 1; /* 15..15 */

        /* the service table hash function , may be modified when there are no services*/
        /* shall be set to hash1 = 11,hash2=00 =0xc   */
         MEA_Uint32 hash2              : 2; /*16..17*/
         MEA_Uint32 hash1              : 2; /*18..19*/
         MEA_Uint32 Acl_hash           : 4 ; /* 20..23 */
         MEA_Uint32 mstp_enable        : 1 ; /* 24 */ 
         MEA_Uint32 sw_loop            : 1;  /*bit25 */
         MEA_Uint32 lag_enable         : 1 ; /* 26..26 */
         MEA_Uint32 wd_disable         : 1 ; /* 27..27*/
         /* The if_Protection_bit is relevant only in port 125 and used to swap it with port 126  */
         MEA_Uint32 if_Protection_bit  : 1 ; /* 28..28 */


         /* Utopia M2S loop: '0' no loop (default) / '1' loop
            Cells come from the chip via the Utopia Master Tx 
            can be looped using external piggy loopback card 
            to loop the cells toward Utopia slave RX */
         MEA_Uint32 close_loop_utopia_master_tx_to_slave_tx : 1; /* 29..29 */

         /* Init Service table: 0 to 1 reset Service Table 
            internal memory. The service table is written with zeros 
            therefore invalids all service table entries */
         MEA_Uint32 service_table_flush  : 1;  /* 30..30 */

         /* reserved */
         MEA_Uint32 acl_table_flush    : 1;  /* 31..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         /* define the algorithm for output select in the DSE 
            0 - out of broadcast domain ( defined in service
            1 - force output port ( default)                 */

         MEA_Uint32 acl_table_flush    : 1;  /* 31..31 */

         /* Init Service table: 0 to 1 reset Service Table 
            internal memory. The service table is written with zeros 
            therefore invalids all service table entries */
         MEA_Uint32 service_table_flush: 1;  /* 30..30 */

         /* Utopia M2S loop: '0' no loop (default) / '1' loop
            Cells come from the chip via the Utopia Master Tx 
            can be looped using external piggy loopback card 
            to loop the cells toward Utopia slave RX */
         MEA_Uint32 close_loop_utopia_master_tx_to_slave_tx : 1; /* 29..29 */
         /* The if_Protection_bit is relevant only in port 125 and used to swap it with port 126  */
         MEA_Uint32 if_Protection_bit          : 1 ; /* 28..28 */
         MEA_Uint32 wd_disable                  : 1 ; /* 27..27*/
         /* reserved */
         MEA_Uint32 lag_enable                  : 1 ; /* 26..26*/   //
         MEA_Uint32 sw_loop                     : 1;  /*bit25 */
         MEA_Uint32 mstp_enable                 : 1 ; /* 24 */
         MEA_Uint32 Acl_hash                    : 4 ; /* 20..23 */
        /* the service table hash function , may be modified when there are no services*/
        /* shall be set to hash1 = 11,hash2=00    */
         MEA_Uint32 hash1                       : 2; /*18..19*/
         MEA_Uint32 hash2                       : 2; /*16..17*/

         /* Utopia Master to Master loop: 
            '0' no loop (default) / 1' loop
             Cell coming from the Utopia master Rx interface are 
             loop back to Utopia TX.
             This mean no chip logic , only Utopia is working. */
         MEA_Uint32 close_loop_utopia_master_tx_to_master_rx : 1; /* 15..15 */

         /* Forwarder bypass: '0' Bypass off / '1' Bypass on 
            Default is '0'-Bypass off.
            Enable to bypass the forwarder block in 
            ingress control  pipeline */
         MEA_Uint32 forwarder_enable        : 1; /* 14..14 */

         /* Init forwarder external memory: 
            0 to 1 reset forwarder external memory.
            The operation cause the forwarder block to write 
            zeros to all of the DDR SDRAM address space */
         MEA_Uint32 forwarder_flush         : 1; /* 13..13 */

        
         MEA_Uint32 link_up_mask        : 1; /* 12..12 */ /*link_up_mask */

        
         MEA_Uint32 rstp_enable                 : 1; /* 11..11 */

        
         MEA_Uint32 learning_disable            : 1;  /*bit10 */
         MEA_Uint32 learnPBB_info_Enable        : 1;  /*bit9 */ 
         MEA_Uint32 drop_unmatch_pkts           : 1; /* 8.. 8 */

         /* FWD next command: any transaction indicate 
            read of next valid entry.
            Toggling that bit cause the Search Engine 
            to run across  the address space  looking 
            for next valid entry */
         MEA_Uint32 forwarder_next_cmd      : 1; /*  7.. 7 */

         
         MEA_Uint32 reserved_6              :1;  /*bit6 */ 
         MEA_Uint32 reserved_5              :1;  /*bit5 */
         MEA_Uint32 reserved_4              :1;  /*bit4 */
         MEA_Uint32 PS_To_En_127            :1;  /*bit3 */ 
         MEA_Uint32 Lxcp_Enable             :1;  /*bit2 */  
         MEA_Uint32 reserved                :1;   /*bit1 */  /*system status*/
         MEA_Uint32 Dse_Output_Algoritm     :1;  /*bit0 */

#endif

 


      } val;

      MEA_Uint32 reg; 

    } if_global1;

    /* start TX Rx MAC threshold */

    MEA_rx_tx_mac_thresholds_t if_global2_Gbe;      /* 0xcc */
    MEA_rx_tx_mac_thresholds_t if_threshold_G999;  /* 0xE8*/
    MEA_rx_tx_mac_thresholds_t if_threshold_TLS;   /* 0xEC*/
    MEA_rx_tx_mac_thresholds_t if_threshold_CPU;   /*0x74*/
    
    /* end TX Rx MAC threshold */
    

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 thresh_frag_art_eop  :  10; /*  0.. 9 */
            MEA_Uint32 pad                  :  12;
#else 
            MEA_Uint32 pad                  :  12;
            MEA_Uint32 thresh_frag_art_eop  :  10; /*  0.. 9 */
#endif


        } val;

        MEA_Uint32 reg; 

    } if_configure_1 ;

   

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 if_ingress_shaper    :  8; /*  0.. 7 */
            MEA_Uint32 pad                  :  24;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad                  :  24;
            MEA_Uint32 if_ingress_shaper  :  8; /*  0.. 7 */
#endif


        } val;

        MEA_Uint32 reg; 

    } if_configure_3 ;
    
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 if_External_srv_mode       : 2; /*  0.1 */
            MEA_Uint32 if_External_srv_shaper_clk : 4;/*2:5*/
            MEA_Uint32 pad : 26;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad : 26;
            MEA_Uint32 if_External_srv_shaper_clk   : 4;/*2:5*/
            MEA_Uint32 if_External_srv_mode         : 2; /*  0.1 */
#endif


        } val;

        MEA_Uint32 reg;

    } if_External_srv;

    union {

      struct {


#if __BYTE_ORDER == __LITTLE_ENDIAN

         MEA_Uint32 rx_memory_segment_size :  6; /*  0.. 5 */
         MEA_Uint32 rx_ready_threshold     :  6; /*  6..11 */
         MEA_Uint32 rx_select_control      :  4; /* 12..15 */
         MEA_Uint32 reserved               : 16; /* 16..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32 reserved               : 16; /* 16..31 */
         MEA_Uint32 rx_select_control      :  4; /* 12..15 */
         MEA_Uint32 rx_ready_threshold     :  6; /*  6..11 */
         MEA_Uint32 rx_memory_segment_size :  6; /*  0.. 5 */
#endif



      } val;

      MEA_Uint32 reg; 

    } if_sssmii_rx_config;


    union {

      struct {



#if __BYTE_ORDER == __LITTLE_ENDIAN


         /* 0=10M / 1=100M - default 100M  - currently 10M not support */
         MEA_Uint32 speed                  :  1; /*  0.. 0 */ 

         /* 0=half / 1=full - default full - currently half not support */
         MEA_Uint32 duplex                 :  1; /*  1.. 1 */

         /* 0=OK  / 1=Error (drop jabber packet) - default: 0 */
         MEA_Uint32 jabber                 :  1; /*  2.. 2 */

         /* TX IPG 11 for 12 bytes , minimum value 11 */
         MEA_Uint32 ipg                    :  8; /*  3..10 */ 

         /* TX preamble value - default 0x7*/
         MEA_Uint32 preamble               :  4; /* 11..14 */ 

         /* Reserved */
         MEA_Uint32 reserved               : 17; /* 15..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

         /* Reserved */
         MEA_Uint32 reserved               : 17; /* 15..31 */ 

         /* TX preamble value - default 0x7*/
         MEA_Uint32 preamble               :  4; /* 11..14 */ 

         /* TX IPG 11 for 12 bytes , minimum value 11 */
         MEA_Uint32 ipg                    :  8; /*  3..10 */ 

         /* 0=OK  / 1=Error (drop jabber packet) - default: 0 */
         MEA_Uint32 jabber                 :  1; /*  2.. 2 */

         /* 0=half / 1=full - default full - currently half not support */
         MEA_Uint32 duplex                 :  1; /*  1.. 1 */

         /* 0=10M / 1=100M - default 100M  - currently 10M not support */
         MEA_Uint32 speed                  :  1; /*  0.. 0 */ 

#endif


      } val;

      MEA_Uint32 reg; 

    } if_sssmii_tx_config0;


    union {

      struct {



#if __BYTE_ORDER == __LITTLE_ENDIAN


         /* TX Link Down : Link Down = '0'b / Link Up = '1'b  */
         MEA_Uint32 tx_link_up             : 16; /*  0..15 */ 

         /* TX memory segment  */
         MEA_Uint32 tx_memory_segment_size :  6; /* 16..21 */

         /* TX Ready threshold */
         MEA_Uint32 tx_ready_threshold     :  6; /* 22..27 */

         /* TX transmit threshold */
         MEA_Uint32 tx_transmit_threshold  :  4; /* 28..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

         /* TX transmit threshold */
         MEA_Uint32 tx_transmit_threshold  :  4; /* 28..31 */ 

         /* TX Ready threshold */
         MEA_Uint32 tx_ready_threshold     :  6; /* 22..27 */

         /* TX memory segment  */
         MEA_Uint32 tx_memory_segment_size :  6; /* 16..21 */

         /* TX Link Down : Link Down = '0'b / Link Up = '1'b  */
         MEA_Uint32 tx_link_up             : 16; /*  0..15 */ 

#endif


      } val;

      MEA_Uint32 reg; 

    } if_sssmii_tx_config1;

    union {

        struct {



#if __BYTE_ORDER == __LITTLE_ENDIAN

    
    MEA_Uint32 port_cesId               :11; /* 0..10 */ 
    MEA_Uint32 port_ces_type            : 1 ; /* 11..11 */ /* 0-port 1-cesId */
    MEA_Uint32 enable                   : 1 ; /* 12..12 */ /* 0-disable 1-Enable */
    MEA_Uint32 dest_src_port            : 9;
    MEA_Uint32 ingress_sniffer_1G       : 7;
   
    MEA_Uint32 en_ingress_sniffer_1G    : 1;
    MEA_Uint32 ingress_sniffer_10G      : 1;  /*'1' -119, '0' -118*/
    MEA_Uint32 en_ingress_sniffer_10G   : 1;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
     
     MEA_Uint32 en_ingress_sniffer_10G  :1;
     MEA_Uint32 ingress_sniffer_10G     :1;  /*'1' -119, '0' -118*/
     MEA_Uint32 en_ingress_sniffer_1G   :1;
     MEA_Uint32 ingress_sniffer_1G      :7;
     MEA_Uint32 dest_src_port           : 9;
     MEA_Uint32 enable                  : 1 ; /* 12..12 */ /* 0-disable 1-Enable */
     MEA_Uint32 port_ces_type           : 1 ; /* 11..11 */ /* 0-port 1-cesId */
     MEA_Uint32 port_cesId              :11; /* 0..10 */ 
#endif
        } val;

        MEA_Uint32 reg; 

    } if_mirror_ces_port; //Egress Mirror
    
    union {
        struct {
                
  
#if __BYTE_ORDER == __LITTLE_ENDIAN           
             MEA_Uint32  port_monitor_1G  : 7;
             MEA_Uint32  en_monitor_1G    : 1;
             MEA_Uint32  port_monitor_10G : 7;
             MEA_Uint32  en_monitor_10G   : 1;
             MEA_Uint32  pad              :16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32  pad : 16;
            MEA_Uint32  en_monitor_10G : 1;
            MEA_Uint32  port_monitor_10G : 7;
            MEA_Uint32  en_monitor_1G : 1;
            MEA_Uint32  port_monitor_1G : 7;

#endif
     
        } val;
        MEA_Uint32 reg;
    } Interface_mirror;


    union {

        struct {



#if __BYTE_ORDER == __LITTLE_ENDIAN


            MEA_Uint32 arb_shaper      :4;
            MEA_Uint32 pad             :28;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
 
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad             :28;
            MEA_Uint32 arb_shaper      :4;
#endif
        } val;

        MEA_Uint32 reg; 

    } if_parser_arb_shaper;


    union {

        struct {



#if __BYTE_ORDER == __LITTLE_ENDIAN

             MEA_Uint32 Comp_bypass               :1;
             MEA_Uint32 Decomp_bypass             :1;
             MEA_Uint32 Comp_bypass_aligner       :1;
             MEA_Uint32 Decomp_bypass_aligner     :1;
             MEA_Uint32 set1588ptp_val            :4;
             MEA_Uint32 pad             :24;

#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad                       :24;
            MEA_Uint32 set1588ptp_val            :4; /*4 7*/
            MEA_Uint32 Decomp_bypass_aligner     :1;
            MEA_Uint32 Comp_bypass_aligner       :1;
            MEA_Uint32 Decomp_bypass             :1;
            MEA_Uint32 Comp_bypass               :1;
#endif
        } val;

        MEA_Uint32 reg[1]; 

    } if_HDC_global;

    struct{

        MEA_Uint16  comp_eth;
        MEA_Uint16  learn_eth;
        MEA_Uint16  Invalidate_eth;
        MEA_Uint16  min_packetsize;
    } if_HDC_EhterType;


    struct{

        MEA_Uint8  to_down;
        MEA_Uint16 to_Up;
        MEA_Uint8 pad;

    } if_1p1_down_up_count;

    struct {
        MEA_Bool en_Rmon_calc_rate;
    } if_sw_global_parmamter;


    union {

        struct {



#if __BYTE_ORDER == __LITTLE_ENDIAN

            MEA_Uint32  key0_hashO : 2;
            MEA_Uint32  key0_hash1 : 2;
            MEA_Uint32  key1_hashO : 2;
            MEA_Uint32  key1_hash1 : 2;
            MEA_Uint32  key2_hashO : 2;
            MEA_Uint32  key2_hash1 : 2;
            MEA_Uint32  key3_hashO : 2;
            MEA_Uint32  key3_hash1 : 2;
            MEA_Uint32  key4_hashO : 2;
            MEA_Uint32  key4_hash1 : 2;
            MEA_Uint32  key5_hashO : 2;
            MEA_Uint32  key5_hash1 : 2;
            MEA_Uint32  key6_hashO : 2;
            MEA_Uint32  key6_hash1 : 2;
            MEA_Uint32  key7_hashO : 2;
            MEA_Uint32  key7_hash1 : 2;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32  key7_hash1 : 2;
            MEA_Uint32  key7_hashO : 2;
            MEA_Uint32  key6_hash1 : 2;
            MEA_Uint32  key6_hashO : 2;
            MEA_Uint32  key5_hash1 : 2;
            MEA_Uint32  key5_hashO : 2;
            MEA_Uint32  key4_hash1 : 2;
            MEA_Uint32  key4_hashO : 2;
            MEA_Uint32  key3_hash1 : 2;
            MEA_Uint32  key3_hashO : 2;
            MEA_Uint32  key2_hash1 : 2;
            MEA_Uint32  key2_hashO : 2;
            MEA_Uint32  key1_hash1 : 2;
            MEA_Uint32  key1_hashO : 2;
            MEA_Uint32  key0_hash1 : 2;
            MEA_Uint32  key0_hashO : 2;
#endif
        } val;

        MEA_Uint32 reg[1];

    } if_fwd_hash;

    
   struct {
            MEA_Uint32  L4_dst_port : 16;
            MEA_Uint32  Net_Tag1    : 12;
            MEA_Uint32  pad1        : 4;

            MEA_Uint32  srcPort       : 7;
            MEA_Uint32  L2_type       : 5;
            MEA_Uint32  Ip_type       : 1;
            MEA_Uint32  protocol_type : 1;
            MEA_Uint32  pad2          : 18;
    }ts_measurement_ingress;


/********** BM Offset  ***************************************/
   union {
       struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
           MEA_Uint32  wd_threshold : 16;
           MEA_Uint32  enable : 1;
           MEA_Uint32  pad : 15;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
           MEA_Uint32  pad : 15;
           MEA_Uint32  enable : 1;
           MEA_Uint32  wd_threshold : 16;
#endif
       }val;
       MEA_Uint32 reg[1];
   }bm_watchdog_parm;


   struct {
       MEA_Uint32  L4_dst_port  : 16;
       MEA_Uint32  Net_Tag1     : 12;
       MEA_Uint32  pad1         : 4;
       MEA_Uint32  srcPort      : 7;
       MEA_Uint32  L2_type      : 5;
       MEA_Uint32  Ip_type      : 1;
       MEA_Uint32 protocol_type : 1;
       MEA_Uint32  pad2 : 18;
   }ts_measurement_egress;


    /* word 0 */
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 bcst_drp_lvl             : 14; /*0:13*/
    MEA_Uint32 shaper_cluster_enable    : 1;  /*14*/
    MEA_Uint32 shaper_priQueue_enable   : 1; /*15*/ 
    MEA_Uint32 wred_shift               : 2; /*16:17*/
    MEA_Uint32 bm_config_res18          : 1; /*18*/
    MEA_Uint32 bm_config_bmqs           : 1; /*19*/
    MEA_Uint32 bm_config_wred           : 1; /*20*/
    MEA_Uint32 bm_config_mqs            : 1; /*21*/
    MEA_Uint32 bm_config_Pol_pri        : 1; /*22*/
    MEA_Uint32 bm_config_res23          : 1; /*23*/
    MEA_Uint32 mc_mqs_enable            : 1; /*24*/
	MEA_Uint32 bm_config_res25          : 1; /*25*/
    MEA_Uint32 mode_best_eff_enable     : 1;/*26*/
    MEA_Uint32 pri_q_mode               : 1; /*27*/
    MEA_Uint32 Cluster_mode             : 1; /*28*/
    MEA_Uint32 shaper_port_enable       : 1; /*29*/
    MEA_Uint32 policer_enable           : 1; /*30*/
    MEA_Uint32 bm_enable                : 1; /*31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 bm_enable                : 1; /*31*/
    MEA_Uint32 policer_enable           : 1; /*30*/
    MEA_Uint32 shaper_port_enable       : 1; /*29*/
    MEA_Uint32 Cluster_mode             : 1; /*28*/
    MEA_Uint32 pri_q_mode               : 1; /*27*/
    MEA_Uint32 mode_best_eff_enable     : 1;/*26*/
	MEA_Uint32 bm_config_res25          : 1; /*25*/
    MEA_Uint32 mc_mqs_enable            : 1;/*24*/
    MEA_Uint32 bm_config_res23          : 1;/*23*/
    MEA_Uint32 bm_config_Pol_pri        : 1;/*21*/
    MEA_Uint32 bm_config_mqs            : 1; /*21*/
    MEA_Uint32 bm_config_wred           : 1;/*20*/
    MEA_Uint32 bm_config_bmqs           : 1; /*19*/
    MEA_Uint32 bm_config_res18          : 1; /*18*/
    MEA_Uint32 wred_shift               : 2;/*16:17*/
    MEA_Uint32 shaper_priQueue_enable   : 1; /*15*/
    MEA_Uint32 shaper_cluster_enable    : 1;/*14*/
    MEA_Uint32 bcst_drp_lvl             : 14;/*0:13*/
#endif
        } val;
        MEA_Uint32 reg[1];
    } bm_config;

    union {
      struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32 pkt_short_limit        :  16;
         MEA_Uint32 pkt_high_limit         :  16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32 pkt_high_limit         :  16;
         MEA_Uint32 pkt_short_limit        :  16;     
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32 chunk_short_limit      :  16;
         MEA_Uint32 chunk_high_limit       :  16;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
         MEA_Uint32 chunk_high_limit       :  16;
         MEA_Uint32 chunk_short_limit      :  16;     
#endif
      } val;
      MEA_Uint32 reg[2];
    } bm_config0;

     union {
      struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
      MEA_Uint32 wfq_deficite_tresh     :  5; /* 0.. 4*/
      MEA_Uint32 reserve                :  3; /* 5.. 7*/
      MEA_Uint32 cnfg_bkpq_tresh        :  5; /* 8..12*/
      //MEA_Uint32 mode_best_eff_enable   : 1;  /*13..13*/
      //MEA_Uint32 vcmux_BCM              :  1;  /*14*/
      MEA_Uint32 pad                    : 19; /*15  31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
      MEA_Uint32 pad                    : 19; /*15  31*/
      //MEA_Uint32 vcmux_BCM              :  1;  /*14*/
       
      MEA_Uint32 cnfg_bkpq_tresh        :  5; /* 8..12*/
      MEA_Uint32 reserve                :  3; /* 5.. 7*/
      MEA_Uint32 wfq_deficite_tresh     :  5; /* 0.. 4*/
#endif
   } val;
      MEA_Uint32 reg[1];
  }bm_config1;

     union {
         struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
             MEA_Uint32 wred_offset            : 8; /* 0.. 7*/
             MEA_Uint32 en_frag_after_fragment : 1;
             MEA_Uint32 en_externl_frag_internal : 1;
             MEA_Uint32 pad                    : 6; /*10  15*/
             MEA_Uint32 target_MTU             : 16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
             MEA_Uint32 target_MTU             : 16;/*16:31*/
             MEA_Uint32 pad                    : 6; /*10  15*/
             MEA_Uint32 en_externl_frag_internal : 1;
             MEA_Uint32 en_frag_after_fragment : 1; /*8*/
             MEA_Uint32 wred_offset            :  8; /* 0.. 7*/
#endif
         } val;
         MEA_Uint32 reg[1];
     }bm_config2;

     struct {
        MEA_Bool en_calc_rate;
     }bm_sw_global_parmamter;

     union {
         struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN

             MEA_Uint32 access_ethertype     :  16; /* 0.. 15*/
             MEA_Uint32 lan_ethertype        :  16; /* 16.. 31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
             MEA_Uint32 lan_ethertype        :  16; /* 16.. 31*/
             MEA_Uint32 access_ethertype     :  16; /* 0.. 15*/
#endif
         } val;
         MEA_Uint32 reg[1];
     }WBRG_swap_Eth;

     MEA_Uint32 reorder_frag_time_out;

    MEA_Uint32 shaper_fast_port                    : 16;
    MEA_Uint32 shaper_slow_multiplexer_port        : 3;
    MEA_Uint32 shaper_pad_port                     : 13;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

    MEA_Uint32 shaper_fast_cluster                 : 16;
    MEA_Uint32 shaper_slow_multiplexer_cluster     : 3;
    MEA_Uint32 shaper_pad_cluster                  : 13;
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif


    MEA_Uint32 shaper_fast_PriQueue                : 16;
    MEA_Uint32 shaper_slow_multiplexer_PriQueue    : 3;
    MEA_Uint32 shaper_pad_PriQueue                 : 13;
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
        
    MEA_Uint32 ticks_for_slow_service;
    MEA_Uint32 ticks_for_fast_service;

    MEA_Uint32 ticks_for_slow_IngFlow;
    MEA_Uint32 ticks_for_fast_IngFlow;

    
    MEA_Uint32 ticks_for_afdx;

    MEA_Bool alow_ingflow_pol_zero;

    MEA_Bool check_srv_key;

    /* For CCM and BFD Stream */
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 fastTick             :16;
            MEA_Uint32 slowTick             :16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 slowTick             :16;
            MEA_Uint32 fastTick             :16;
#endif

        }val;
        MEA_Uint32 reg;
        
    }CCM_Tick;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 wm             :16;
            MEA_Uint32 xCir          :16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 xCir           :16;
            MEA_Uint32 wm             :16;
#endif

        }val;
        MEA_Uint32 reg[1];

    }CCM_Bucket;


    union { 
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
   /* 0 ..7*/
    MEA_Uint32 NOP              : 8;
    MEA_Uint32 dpDdr               : 8;
    MEA_Uint32 technologyType      : 4;
    MEA_Uint32 forwarder_Addr_Width  : 6;
    MEA_Uint32 sysType             : 6;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 sysType             : 6;
    MEA_Uint32 forwarder_Addr_Width  : 6;    
    MEA_Uint32 technologyType      : 4;    
    MEA_Uint32 dpDdr               : 8;
    MEA_Uint32 NOP : 8;
#endif

        }val;
        MEA_Uint32 if_reg_verInfo;
    }verInfo;

    union { 
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 enable              :  1; /* 0.. 0*/
        MEA_Uint32 reserved            :  2; /* 1.. 2*/
        MEA_Uint32 lastTable           :  5; /* 5.. 7*/
        MEA_Uint32 sweepInterval       : 24; /* 8..31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 sweepInterval       : 24; /* 8..31*/
        MEA_Uint32 lastTable           :  5; /* 3.. 7*/
        MEA_Uint32 reserved            :  2; /* 1.. 2*/
        MEA_Uint32 enable              :  1; /* 0.. 0*/
#endif
        }val;
        MEA_Uint32 if_mac_ageing;
    }macAging;
    
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 oam_id : 3;
    MEA_Uint32 ethertype : 16;
    MEA_Uint32 l2cp   : 6;
    MEA_Uint32 valid  : 1; 
    MEA_Uint32 pad0   : 6;
	
    MEA_Uint32 A;
#else
    MEA_Uint32 A;
    MEA_Uint32 pad0   : 6; 
    MEA_Uint32 valid  : 1;
    MEA_Uint32 l2cp   : 6;
    MEA_Uint32 ethertype : 16;
    MEA_Uint32 oam_id : 3;
#endif
    } OAM_data[MEA_GLOBALS_OAM_NUM_OF_ENTRIES];

    struct {

        struct {
        MEA_MacAddr    Mac_Address;
        MEA_Uint32     command  ;
        MEA_Uint32     pad : 16; 
        }MAC_802_1ag;
 

        struct {
        MEA_Uint32  DestIp;
        MEA_Uint32  sourceIp;
        }MC_ServerIp;
    }Globals_Device;
    
    struct {
     MEA_Uint32     valid       :1;
     MEA_Uint32     EtherType   :16;
     MEA_Uint32     pad         :15;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif

     MEA_MacAddr    MC_Mac_Address;   /* 01:80:c2:xx:xx:xx */
     MEA_MacAddr    UC_Mac_Address;          
    }CFM_OAM;

    MEA_MacAddr    SA_My_Mac;

    union {
        struct {

#if __BYTE_ORDER == __LITTLE_ENDIAN

         MEA_Uint32 EtherType          : 16; /* 0..15 */
         MEA_Uint32 port               :  8; /* 17 ..23*/
         MEA_Uint32 pad0               :  7; /* 24..30 */
         MEA_Uint32 enable             :  1; /* 31..31 */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
          MEA_Uint32 enable             :  1; /* 31..31 */
          MEA_Uint32 pad0               :  7; /* 24..30  */
          MEA_Uint32 port               :  8; /* 17 ..23*/
          MEA_Uint32 EtherType          : 16; /* 0   15 */
#endif
      } val;
      MEA_Uint32 reg; 
    } PacketAnalyzer;

    union {
      struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32 mode                  :  4;
         MEA_Uint32 default_state         :  4;
         MEA_Uint32  pad                  :24;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
         MEA_Uint32  pad                    :24; 
         MEA_Uint32 default_state           : 4; // MEA_PortState_te forward or discard
         MEA_Uint32 mode                    : 4; // see MEA_Global_STP_mode_te  
#endif

      } val;
      MEA_Uint32 reg[1];
    } stp_mode;
   
    union {
      struct {

#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32   clock_grn      : 6;      /*4..0*/
       MEA_Uint32   clock_val      :25;     /*30..5*/
       MEA_Uint32   clock_Add_sub  :1;      /* 31 31*/ 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
       
       MEA_Uint32   clock_Add_sub  :1;      /* 31 31*/           // Add 1 sub 0
       MEA_Uint32   clock_val      :25;     /*30..5*/
       MEA_Uint32   clock_grn     : 6;      /*4..0*/
#endif
      } val;
      MEA_Uint32 reg[1];
    } WallClock_Calibrate;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
          MEA_Uint32 value;
#else
          MEA_Uint32 value;
#endif
        } val;
      MEA_Uint32 reg[1];
    }  WallClock_1Sec;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 delay_data_DDR    :16;/*0..15*/
            MEA_Uint32 delay_descriptor  :16; /*16..31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 delay_descriptor  :16; /*16..31*/
            MEA_Uint32 delay_data_DDR    :16;/*0..15*/
#endif
        } val;
        MEA_Uint32 reg[1];
    }  bm_set_delay;

    union {
        struct {
            MEA_Uint32 pck_reorder_timeout ; // xx is in micro Sec
            MEA_Uint32 age_reorder_timeout ; // xx is in micro Sec
        } val;
        MEA_Uint32 reg[2];
    }set_reorder_timeout;

    MEA_pos2_config_polling pos_config;
    
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32         data_threshold           :8;
            MEA_Uint32         EOC_threshold            :5;
            MEA_Uint32         Ingress_FIFO_auto_flush  :1;
            MEA_Uint32         pad                      :16;
            MEA_Uint32         rx_parity_check          :1;
            MEA_Uint32         tx_parity_check          :1;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32         tx_parity_check          :1;
            MEA_Uint32         rx_parity_check          :1;
            MEA_Uint32         pad                      :16;
            MEA_Uint32         Ingress_FIFO_auto_flush  :1;
            MEA_Uint32         EOC_threshold            :5;    
            MEA_Uint32         data_threshold           :8;    
#endif
        } val;
        MEA_Uint32 reg[1];
    }utopia_threshold_reg;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  acm_pol   :3;
            MEA_Uint32  acm_wred  :2;
            MEA_Uint32  pad       :27;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32  pad       :27;
            MEA_Uint32  acm_wred  :2;    
            MEA_Uint32  acm_pol   :3;    
#endif
        } val;
        MEA_Uint32 reg[1];
    }acm_mode_configure;
         
    union {
        struct {
//             Shut down transceiver block 5 {ports: 104-107 and 93}
//             Shut down transceiver block 4 {ports: 119 and 111}
//             Shut down transceiver block 3 {ports: 118 and 110}
//             Shut down transceiver block 2 {ports: 100-103 and 92}
//             Shut down transceiver block 1 {ports: 48,72,125,126 and 109}
//             Shut down transceiver block 0 {ports: 0,12,24,36 and 108}


#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32         group_0      :1;    
            MEA_Uint32         group_1      :1;    
            MEA_Uint32         group_2      :1;    
            MEA_Uint32         group_3      :1;    
            MEA_Uint32         group_4      :1;    
            MEA_Uint32         group_5      :1;    
            MEA_Uint32         pad        :26;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32         pad          :26;
            MEA_Uint32         group_5      :1;
            MEA_Uint32         group_4      :1; 
            MEA_Uint32         group_3      :1; 
            MEA_Uint32         group_2      :1; 
            MEA_Uint32         group_1      :1; 
            MEA_Uint32         group_0      :1;    
#endif

        } val;
        MEA_Uint32 reg[1];
      }power_saving;   // set the device to working saving mode

    MEA_Uint32   mea_eth24_sfp_reg;

  struct {
        MEA_Uint32  my_Ip;
   }Globals_eth0_Device;

  struct {
      MEA_Uint32  my_Ip;
  }Device_edit_src_ip;

  struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
      MEA_Uint32  disable_fwd_del_on_reinit  :1;
      MEA_Uint32  pad_fwd  :31;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
      MEA_Uint32  pad_fwd  :31;
      MEA_Uint32  disable_fwd_del_on_reinit  :1;
#endif
  }forwarder;
  
  MEA_Uint32 iTHi;

  MEA_Bool HC_Offset_Mode;  /*0-header compress offset disable   1- offset enable */
  union {
  struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
      MEA_Uint32  init_flush  :1;
      MEA_Uint32  hash_func1   :2;
      MEA_Uint32  hash_func2   :2;
      MEA_Uint32  max_class_use_hcid   :8;
      MEA_Uint32  reserved   :19; 
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
      
      MEA_Uint32  reserved   :19; 
      MEA_Uint32  max_class_use_hcid   :8;
      MEA_Uint32  hash_func2   :2;
      MEA_Uint32  hash_func1   :2;
      MEA_Uint32  init_flush  :1;
#endif
  }val;
  MEA_Uint32 reg[1];
  }Hc_classifier;

    MEA_MacAddr    Local_Mac_Address[MEA_MAX_LOCAL_MAC];
    MEA_Uint32     Local_IP_Address[MEA_MAX_LOCAL_IP];
    MEA_Uint8      Local_IP_Address_mode[MEA_MAX_LOCAL_IP]; /*0-Ipv4,1-Ipv6*/
   /************************************************************************/
   /* BFD                                                                 */
   /************************************************************************/
    MEA_Uint32     Local_BFD_SW_IP_Address;
    MEA_Uint32     MyBFD_Discriminator;
   
    /************************************************************************/
    /* IpSec NVGER                                                           */
    /************************************************************************/

    MEA_Uint32     Local_IP_my_Ipsec;
    struct {
        MEA_MacAddr Gre_src_mac;
        MEA_Uint32  outer_vlanId;
        MEA_Uint32  Gre_ttl;
        MEA_Uint32  Gre_src_Ipv4;
    }nvgre_info;
    struct{
        MEA_Uint32 ipsec_src_Ipv4; //For Editing
        MEA_Uint32 ipsec_ttl;      //For Editing
        
            
   }Ipsec_info;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  offset12_EthType : 16;
            MEA_Uint32  offset16_EthType : 16;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32  offset16_EthType : 16;
            MEA_Uint32  offset12_EthType : 16;
#endif
        }val;
        MEA_Uint32 reg[1];
    }EthType_cam;


    /*Queue handler*/

    MEA_Uint32 Queue_reset_sec;
    MEA_Uint32 QueueTH_MQS;
    MEA_Uint32 QueueTH_currentPacket;


} MEA_Globals_Entry_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Globals      APIs                                      */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_Globals_Entry (MEA_Unit_t                  unit,
                                      MEA_Globals_Entry_dbt*      entry);

MEA_Status MEA_API_Get_Globals_Entry (MEA_Unit_t                  unit,
                                      MEA_Globals_Entry_dbt*      entry);

MEA_Bool MEA_API_Get_Globals_IS_best_eff_enable(MEA_Unit_t                  unit);

MEA_Bool MEA_API_Get_Globals_IS_NIC_DDR_enable(MEA_Unit_t                  unit);

/*******************************************************************************/

/********************************************************************************/
typedef union {
    struct{

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32   Buff_Id         : 16;
        MEA_Uint32   En_Search      : 1;
        MEA_Uint32   Force_release  : 1;
        MEA_Uint32   pad            : 14;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32   pad            : 14;
        MEA_Uint32   Force_release  : 1;
        MEA_Uint32   En_Search      : 1;
        MEA_Uint32   Buff_Id         : 16;
#endif
 

    }val;
    MEA_Uint32 regs[1];
}mea_BuffSearch_dbt;

MEA_Status MEA_API_Buffer_Search(MEA_Unit_t unit,
                                 mea_BuffSearch_dbt *entry_i);

MEA_Status MEA_API_Buffer_Status(MEA_Unit_t unit,
                                mea_BuffSearch_dbt *entry_o);


/************************************************************************/
/*                                                                      */
/************************************************************************/
typedef struct {
    MEA_Uint32 sec;
    MEA_Uint32 nsec;
}MEA_Clock_t;

MEA_Status MEA_API_Globals_WallCLock_Set(MEA_Unit_t unit ,MEA_Clock_t entry);
MEA_Status MEA_API_Globals_WallCLock_Get(MEA_Unit_t unit ,MEA_Clock_t *entry);

MEA_Status MEA_API_WallCLock_Set_1588(MEA_Unit_t unit, MEA_Clock_t *entry, MEA_Bool SW_Enable);

/**/
MEA_Status MEA_API_Get_Ts_MEASUREMENT(MEA_Unit_t unit, MEA_Uint32 *ret_masurement);

/************************************************************************/
/*                                                                      */
/************************************************************************/
/*
        TS_CTL1_REG
        TS_SET_SEC_REG *
        TS_GET_SEC_REG *
        TS_SET_NS_REG *
        TS_GET_NS_REG *
        TS_SET_COMP_REG
        TS_SET_COR_REG
*/

typedef enum{
    MEA_WALLCLOCK_TYPE_READ  =0,
    MEA_WALLCLOCK_TYPE_WRITE = 1,

}MEA_WallCLock_wr_t;


typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 

        MEA_Uint32 TS_ENBL                  : 1; /*bit0*/
        MEA_Uint32 ENDIAN_TS_FORMAT         : 1; /*bit1*/
        MEA_Uint32 SHORT_TS_FORMAT          : 1; /*bit2*/
        MEA_Uint32 TS_LEN_MODE              : 2;   /*bit3 :4*/
        MEA_Uint32 MIX_TS_FORMAT            : 3; /*5-7*/
        MEA_Uint32 STATUS_FLAGS_TO_NS       : 1; /*8*/
        MEA_Uint32 Enable_assert_Tx_Error   : 1; /*9*/
        MEA_Uint32 Reserved_b10             : 1; /*10*/
        MEA_Uint32 DIS_TS_CTLB              : 1; /*11*/
        MEA_Uint32 TS_SIGN                  : 4; /*12-15*/
        MEA_Uint32 SEL_PPS_SRC              : 4; /*16-19*/
        MEA_Uint32 SET_PPS_OUT1             : 1; /*20*/
        MEA_Uint32 SET_PPS_OUT2             : 1; /*21*/
        MEA_Uint32 SET_PPS_OUT3             : 1; /*22*/
        MEA_Uint32 SET_PPS_OUT4             : 1; /*23*/
        MEA_Uint32 ENBL_TS_COMP             : 1; /*24*/
        MEA_Uint32 SEL_TS_COMP              : 1; /*25*/
        MEA_Uint32 HW_COMP_METHOD           : 1; /*26-28*/
        MEA_Uint32 ENBL_TS_SW_COMP          : 1; /*29*/
        MEA_Uint32 Reserved : 2;/*30-31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 Reserved : 2;/**/
        MEA_Uint32 ENBL_TS_SW_COMP : 1; /*29*/
        MEA_Uint32 HW_COMP_METHOD : 1; /*26-28*/
        MEA_Uint32 SEL_TS_COMP : 1; /*25*/
        MEA_Uint32 ENBL_TS_COMP : 1; /*24*/
        MEA_Uint32 SET_PPS_OUT4 : 1; /*23*/
        MEA_Uint32 SET_PPS_OUT3 : 1; /*22*/
        MEA_Uint32 SET_PPS_OUT2 : 1; /*21*/
        MEA_Uint32 SET_PPS_OUT1             : 1; /*20*/
        MEA_Uint32 SEL_PPS_SRC              : 4; /*16-19*/
        MEA_Uint32 TS_SIGN                  : 4; /*12-15*/
        MEA_Uint32 DIS_TS_CTLB              : 1; /*11*/
        MEA_Uint32 Reserved_b10             : 1; /*10*/
        MEA_Uint32 Enable_assert_Tx_Error   : 1; /*9*/
        MEA_Uint32 STATUS_FLAGS_TO_NS       : 1; /*8*/
        MEA_Uint32 MIX_TS_FORMAT            : 3; /*5-7*/
        MEA_Uint32 TS_LEN_MODE              : 2;   /*bit3 :4*/
        MEA_Uint32 SHORT_TS_FORMAT          : 1; /*bit2*/
        MEA_Uint32 ENDIAN_TS_FORMAT         : 1; /*bit1*/
        MEA_Uint32 TS_ENBL                  : 1; /*bit0*/
 
#endif

    } val;
    MEA_Uint32 regs[1];
} mea_WallCLock_TS_CTL0_REG;

MEA_Status MEA_API_WallCLock_CTL0_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_CTL0_REG *entry);

typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 

        MEA_Uint32 TS_WR_LATCH              : 1; /*bit0*/
        MEA_Uint32 TS_RD_LATCH              : 1; /*bit1*/
        MEA_Uint32 ENBL_FORCE_NS_ALL_ZERO   : 1; /*bit2*/
        MEA_Uint32 SW_update_en             : 1; /*bit3*/
        MEA_Uint32 Reserved                 :28;/*4:31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 Reserved                 :28;/*4:31*/
        MEA_Uint32 SW_update_en             :1; /*bit3*/
        MEA_Uint32 ENBL_FORCE_NS_ALL_ZERO   :1; /*bit2*/
        MEA_Uint32 TS_RD_LATCH              :1; /*bit1*/
        MEA_Uint32 TS_WR_LATCH              :1; /*bit0*/
#endif

    } val;
    MEA_Uint32 regs[1];
} mea_WallCLock_TS_CTL1_REG;

MEA_Status MEA_API_WallCLock_CTL1_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_CTL1_REG *entry);


typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32 SEC;
#else
       
        MEA_Uint32 SEC ; 
#endif

    } val;
    MEA_Uint32 regs[1];
} mea_WallCLock_TS_SEC_REG;

MEA_Status MEA_API_WallCLock_SEC_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_SEC_REG *entry);
/************************************************************************/
/*                                                                      */
/************************************************************************/
typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32 NS;
#else

        MEA_Uint32 NS;
#endif

    } val;
    MEA_Uint32 regs[1];
} mea_WallCLock_TS_NS_REG;

MEA_Status MEA_API_WallCLock_NS_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_NS_REG *entry);


/******************************************************************************

*******************************************************************************/
typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32 TS_SET_COMP          : 16; /* 0:15*/
        MEA_Uint32 TS_COMP_DIR          : 1; /*16*/
        MEA_Uint32 Reserved             : 14;/*17:30*/
        MEA_Uint32 ENBL_TS_CLK_COMP_CTL : 1;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 ENBL_TS_CLK_COMP_CTL : 1;
        MEA_Uint32 Reserved : 14;/*17:30*/
        MEA_Uint32 TS_COMP_DIR : 1; /*16*/
        MEA_Uint32 TS_SET_COMP : 16; /* 0:15*/
#endif

    } val;
    MEA_Uint32 regs[1];
} mea_WallCLock_TS_COMP_REG;
MEA_Status MEA_API_WallCLock_COMP_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_COMP_REG *entry);



/************************************************************************/
typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32 TS_SET_COR              : 16; /* 0:15*/
        MEA_Uint32 TS_COR_DIR              : 1; /*16*/
        MEA_Uint32 Reserved                : 15;/*17:30*/
        MEA_Uint32 ENBL_TS_CLK_CORRECT_CTL : 1;
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 ENBL_TS_CLK_CORRECT_CTL  : 1;
        MEA_Uint32 Reserved                 : 15;/*17:30*/
        MEA_Uint32 TS_COR_DIR               : 1; /*16*/
        MEA_Uint32 TS_SET_COR               : 16; /* 0:15*/
#endif

    } val;
    MEA_Uint32 regs[1];
} mea_WallCLock_TS_COR_REG;

MEA_Status MEA_API_WallCLock_COR_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_COR_REG *entry);


typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32 ACUM_INC_VAL : 24; /* 0:23*/
        MEA_Uint32 Reserved : 8;    /*24:31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        #else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 Reserved : 8;    /*24:31*/
        MEA_Uint32 ACUM_INC_VAL : 24; /* 0:23*/
#endif

    } val;
    MEA_Uint32 regs[1];
} mea_WallCLock_TS_ACUM_CFG1_REG;

MEA_Status MEA_API_WallCLock_ACUM_CFG1_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_ACUM_CFG1_REG *entry);


typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32 ACUM_INC_STEP : 14; /* 0:13*/
        MEA_Uint32 Reserved      : 18;    /*14:31*/
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 Reserved : 18;    /*14:31*/
        MEA_Uint32 ACUM_INC_STEP : 14; /* 0:13*/
#endif

    } val;
    MEA_Uint32 regs[1];
} mea_WallCLock_TS_ACUM_CFG2_REG;

MEA_Status MEA_API_WallCLock_ACUM_CFG2_REG(MEA_Unit_t unit, MEA_WallCLock_wr_t type_RW, mea_WallCLock_TS_ACUM_CFG2_REG *entry);



typedef enum {
    MEA_BM_DISCRIPTOR_TYPE_1K  = 0,
    MEA_BM_DISCRIPTOR_TYPE_2K  = 1,
    MEA_BM_DISCRIPTOR_TYPE_4K  = 2,
    MEA_BM_DISCRIPTOR_TYPE_8K  = 3,
    MEA_BM_DISCRIPTOR_TYPE_16K = 4,
    MEA_BM_DISCRIPTOR_TYPE_64K  = 5,
    MEA_BM_DISCRIPTOR_TYPE_128K = 6,
    MEA_BM_DISCRIPTOR_TYPE_256K = 7,


}MEA_BM_DISCRIPTOR_TYPE_t;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA DeviceInfo      Typedefs                               */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32  encription_enable           :1; /*bit0*/
        MEA_Uint32  protection_enable           :1; /*bit1*/
        MEA_Uint32  bmf_martini_support         :1; /*bit2*/
        MEA_Uint32  NVGRE_enable                :1; /*bit3*/
        MEA_Uint32  rstp_support                :1; /*bit4*/
        MEA_Uint32  forwarder_external          :1; /*bit5*/
        MEA_Uint32  desc_external               :1; /*bit6*/
        MEA_Uint32  desc_checker_exist          :1; /*bit7*/
        MEA_Uint32  host_reduce                 :1; /*bit8*/ 
        MEA_Uint32  pre_sched_enable            :1;/*bit9*/
        MEA_Uint32  pri_map_enable              :1;/*bit10*/
        MEA_Uint32  pause_support               :1;/*bit11*/
        MEA_Uint32  flowCos_Mapping_exist       :1;/*bit12*/
        MEA_Uint32  flowMarking_Mapping_exist   :1;/*bit13*/
        MEA_Uint32  Swap_machine_enable         :1;/*bit14*/
        MEA_Uint32  fragment_exist              :1;/*bit15*/
        MEA_Uint32  tx_chunk_size               :4;/*bit16-19*/
        MEA_Uint32  vpls_access_enable          :1; /*bit20*/
        MEA_Uint32  vpls_chactristic_enable     :1; /*bit21*/
        MEA_Uint32  TS_MEASUREMENT_en           :1; //bit22
        MEA_Uint32  G999_1_channel_format       :2; /*23:24*/
        MEA_Uint32  freeBit25                   :1;/*bit25*/
        MEA_Uint32  ACM_exist                   :1;/*bit26*/
        MEA_Uint32  fragment_bonding            :1;/*bit27*/
        MEA_Uint32  cpe_exist                   :1;/*bit28*/  /* need to remove */
        MEA_Uint32  dual_latency_support        :1; /*bit29*/
        MEA_Uint32  num_of_Egress_port          :2; /*bit30 -31*/ /*0-128 1-256 2-512 3-1024  */
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif        
        
#else     
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif    
		MEA_Uint32  num_of_Egress_port          : 2; /*bit30 -31*/ /*0-128 1-256 2-512 3-1024  */
        MEA_Uint32  dual_latency_support        :1;/*bit29*/
        MEA_Uint32  cpe_exist                   :1;/*bit28*/
        MEA_Uint32  fragment_bonding            :1;/*bit27*/
        MEA_Uint32  ACM_exist                   :1;/*bit26*/ 
        MEA_Uint32  freeBit25                   :1;/*bit25*/
        MEA_Uint32  G999_1_channel_format       : 2; /*23:24*/
        MEA_Uint32  TS_MEASUREMENT_en           :1; //bit22
        MEA_Uint32  vpls_chactristic_enable     : 1; /*bit21*/
        MEA_Uint32  vpls_access_enable          :1; /*bit20*/
        MEA_Uint32  tx_chunk_size               :4;/*bit16-19*/  
        MEA_Uint32  fragment_exist              :1;/*bit15*/
        MEA_Uint32  Swap_machine_enable         :1;/*bit14*/
        MEA_Uint32  flowMarking_Mapping_exist   :1;/*bit13*/
        MEA_Uint32  flowCos_Mapping_exist       :1;/*bit12*/
        MEA_Uint32  pause_support               :1;/*bit11*/
        MEA_Uint32  pri_map_enable              :1;/*bit10*/
        MEA_Uint32  pre_sched_enable            :1;/*bit9*/
        MEA_Uint32  host_reduce                 :1; /*bit8*/
        MEA_Uint32  desc_checker_exist          :1; /*bit7*/
        MEA_Uint32  desc_external               :1; /*bit6*/
        MEA_Uint32  forwarder_external          :1; /*bit5*/
        MEA_Uint32  rstp_support                :1; /*bit4*/
        MEA_Uint32  NVGRE_enable                :1; /*bit3*/
        MEA_Uint32  bmf_martini_support         :1; /*bit2*/
        MEA_Uint32  protection_enable           :1; /*bit1*/
        MEA_Uint32  encription_enable           :1; /*bit0*/
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  tdm_enable                  :1; /*bit32*/
        MEA_Uint32  large_BM_ind_address        :1;/*bit33*/
        MEA_Uint32  rmonVP_enable               :1; /*bit34*/   
        MEA_Uint32  protocolMapping             :1; /*bit35*/
        MEA_Uint32  cls_large                   :1; /*bit36*/
        MEA_Uint32  Support_1588                :1; /*bit37*/
        MEA_Uint32  BM_bus_width                :2;  /*bit38.39*/  // 0-64bit 01-128bit 10-TBD 11 TBD 
        MEA_Uint32  rx_pause_support            :1;/*bit40*/
        MEA_Uint32  lag_support                 :1;/*bit41*/
        MEA_Uint32  mstp_support                :1;/*bit42*/
        MEA_Uint32  mc_mqs_Support              :1; /*bit43*/
        MEA_Uint32  cluster_adminon_support     :1;/*bit44*/
        MEA_Uint32  g999_num_of_channel         :1;/*bit45*/  /*0- 4 port 1 8*/
        MEA_Uint32  mtu_pri_queue_reduce        :1;/*bit46*/
        MEA_Uint32  ingressMacfifo_small        :1;/*bit47*/
        MEA_Uint32  AlloC_AllocR                :1; /*bit48*/
        MEA_Uint32  freeBit49                   : 1; /*bit49*/
        MEA_Uint32  MAC_LB_support              :1; /*bit50*/
        MEA_Uint32  QueueStat_en                :1; /*bit51*/
        MEA_Uint32  reduce_lq                   :1; /*bit52*/
        MEA_Uint32  reduce_parser_tdm           :1; /*bit53*/
		MEA_Uint32  fragment_9991               :1; /*bit54*/
        MEA_Uint32  forworder_filter_support    :1; /*bit55*/
        MEA_Uint32  serdesReset_support         :1; /*bit56*/
        MEA_Uint32  autoneg_support             :1;  /*bit57*/
        MEA_Uint32  GATEWAY_EN                  : 1;  /*bit58*/
        MEA_Uint32  standard_bonding_support    :1;  /*bit59*/
        MEA_Uint32  BFD_support                 :1;  /*bit60*/
        MEA_Uint32  rmonRx_drop_support         :1;  /*bit61*/
        MEA_Uint32  afdx_support                :1;  /*bit62*/
        MEA_Uint32  ip_sec_support              :1;  /*bit63*/ 
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
        MEA_Uint32  ip_sec_support             :1; /*bit63*/
        MEA_Uint32  afdx_support                :1;  /*bit62*/
        MEA_Uint32  rmonRx_drop_support         :1;  /*bit61*/
        MEA_Uint32  BFD_support                 :1;  /*bit60*/
        MEA_Uint32  standard_bonding_support    :1;/*bit59*/
        MEA_Uint32  GATEWAY_EN                  :1;  /*bit58*/
        MEA_Uint32  autoneg_support             :1;  /*bit57*/
        MEA_Uint32  serdesReset_support         :1; /*bit56*/
        MEA_Uint32  forworder_filter_support    :1; /*bit55*/
        MEA_Uint32  fragment_9991               :1; /*bit54*/
        MEA_Uint32  reduce_parser_tdm           :1; /*bit53*/
        MEA_Uint32  reduce_lq                   :1; /*bit52*/
        MEA_Uint32  QueueStat_en                :1; /*bit51*/
        MEA_Uint32  MAC_LB_support              :1; /*bit50*/
        MEA_Uint32  freeBit49                   :1; /*bit49*/
        MEA_Uint32  AlloC_AllocR                :1; /*bit48*/
        MEA_Uint32  ingressMacfifo_small        :1;/*bit47*/
        MEA_Uint32  mtu_pri_queue_reduce        :1;/*bit46*/
        MEA_Uint32  g999_num_of_channel         :1;/*bit45*/ /*service_port_Range_reduced*/
        MEA_Uint32  cluster_adminon_support     :1;/*bit44*/
        MEA_Uint32  mc_mqs_Support              :1; /*bit43*/
        MEA_Uint32  mstp_support                :1;/*bit42*/
        MEA_Uint32  lag_support                 :1;/*bit41*/
        MEA_Uint32  rx_pause_support            :1;/*bit40*/
        MEA_Uint32  BM_bus_width                :2;  /*bit38.39*/  // 0-64bit 01-128bit 10-TBD 11 TBD 
        MEA_Uint32  Support_1588                :1; /*bit37*/
        MEA_Uint32  cls_large                   :1; /*bit36*/
        MEA_Uint32  protocolMapping             :1; /*bit35*/
        MEA_Uint32  rmonVP_enable               :1; /*bit34*/
        MEA_Uint32  large_BM_ind_address        :1; /*bit33*/
        MEA_Uint32  tdm_enable                  :1; /*bit32*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  num_of_slice                :4; /*64 67*/  
        MEA_Uint32  num_bonding_groupTx         :5; /*68 72*/
        MEA_Uint32  num_bonding_groupRx         :5; /*73 77*/
        MEA_Uint32  GRE_enable                  :1;/*78*/
        MEA_Uint32  L2TP_enable                 :1;/*79*/

        MEA_Uint32  chip_name                   :7;
        MEA_Uint32  sub_family                  :3;
        MEA_Uint32  package                     :4;
        MEA_Uint32  speed_grade                 :2;
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
        
        MEA_Uint32  speed_grade                   :2;
        MEA_Uint32  package                       :4;
        MEA_Uint32  sub_family                    :3;
        MEA_Uint32  chip_name                     :7;
        MEA_Uint32  L2TP_enable                   : 1;/*79*/
        MEA_Uint32  GRE_enable                    :1; /*78*/
        MEA_Uint32  num_bonding_groupRx           :5; /*73 77*/
        MEA_Uint32  num_bonding_groupTx           :5; /*68 72*/
        MEA_Uint32  num_of_slice                  :4; /*64 67*/  // 0 no slice 1 of (BM_bus_width)
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
       
        MEA_Uint32 pad                           : 16;
        MEA_Uint32 protect_1p1_suppport          :1;  /*bit112*/
        MEA_Uint32 rmonRx_IL_drop_support        :1;  /*bit113*/
        MEA_Uint32 HeaderCompress_support        :1;  /*bit114*/ 
        MEA_Uint32 my_mac_support                :1;  /*bit115*/
        MEA_Uint32 ingress_count                 :1;  /*bit116*/
        MEA_Uint32 ac_MTU_support                :1;  /*bit117*/
        MEA_Uint32 NewAlloc                      :1;  /*bit118*/
        MEA_Uint32 tdm_act_disable               :1;  /*bit119*/
        MEA_Uint32 HeaderDeCompress_support      :1;  /*bit120*/ 
        MEA_Uint32 PTP1588_support               :1;  /*bit121*/
        MEA_Uint32 extend_default_sid            :1;  /*bit122*/
        MEA_Uint32 wbrg_support                  :1;  /*bit123*/
        MEA_Uint32 ECN_support                   :1;  /*bit124*/
        MEA_Uint32 num_of_port_Ingress           :2; /*0-128 1-256 2-512 3-1024 */
        MEA_Uint32 ASIC_Enable                   : 1;
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
        MEA_Uint32 ASIC_Enable                  :1;
        MEA_Uint32 num_of_port_Ingress          :2; /*0-128 1-256 2-512 3-1024 */
        MEA_Uint32 ECN_support                  :1;  /*bit124*/
        MEA_Uint32 wbrg_support                 :1;  /*bit123*/
        MEA_Uint32 extend_default_sid           :1;  /*bit122*/
        MEA_Uint32 PTP1588_support              :1;  /*bit121*/
        MEA_Uint32 HeaderDeCompress_support     :1;  /*bit120*/ 
        MEA_Uint32 tdm_act_disable              :1;  /*bit119*/
        MEA_Uint32 NewAlloc                     :1;  /*bit118*/
        MEA_Uint32 ac_MTU_support               :1;  /*bit117*/
        MEA_Uint32 ingress_count                :1;  /*bit116*/
		MEA_Uint32 my_mac_support               :1;  /*bit115*/
        MEA_Uint32 HeaderCompress_support       :1;  /*bit114*/
        MEA_Uint32 rmonRx_IL_drop_support       :1;  /*bit113*/
        MEA_Uint32 protect_1p1_suppport         :1;  /*bit112*/
       
        MEA_Uint32 pad                          :16;/*96:111 reserve */
        
#endif

    } val;
     MEA_Uint32 index_0_regs[4]; 
    } global;

    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32  sys_M           : 8;
            MEA_Uint32  sys_N           : 8;
            MEA_Uint32 ACTION_ON_DDR    : 1; /*16*/
            MEA_Uint32 EDITING_ON_DDR   : 1; /*17*/
            MEA_Uint32 PM_ON_DDR        : 1; /*18*/
            MEA_Uint32 SERVICE_EXTERNAL : 1; /*19*/
            MEA_Uint32 FWD_IS_256       : 1; /*20*/
            MEA_Uint32 vxlan_support    : 1; /*21*/
            MEA_Uint32 TS_support       : 1; /*22*/
            MEA_Uint32 type_DB_DDR      : 1; /*23*/
            MEA_Uint32 pushClk          : 8; /*24:31*/

#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pushClk          : 8; /*24:31*/
            MEA_Uint32 type_DB_DDR      : 1; /*23*/
            MEA_Uint32 TS_support       : 1; /*22*/
            MEA_Uint32 vxlan_support    : 1; /*21*/
            MEA_Uint32 FWD_IS_256       : 1; /*20*/
            MEA_Uint32 SERVICE_EXTERNAL : 1; /*19*/
            MEA_Uint32 PM_ON_DDR        : 1; /*18*/
            MEA_Uint32 EDITING_ON_DDR   : 1; /*17*/
            MEA_Uint32 ACTION_ON_DDR    : 1; /*16*/
            MEA_Uint32 sys_N            : 8;
            MEA_Uint32 sys_M            : 8;

#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 max_point_2_multipoint :16;
            MEA_Uint32  hierarchical_en       : 1;   /*48*/
            MEA_Uint32 instanc_fifo_1588      : 2; /*49 :50*/
            MEA_Uint32 freeBit51              : 1;/*51*/
            MEA_Uint32 max_numof_descriptor   : 4;/*52 55*/
            MEA_Uint32 max_numof_DataBuffer   : 4;/*56 59*/
			MEA_Uint32 num_enb_mac			  : 4;/*60:63*/

#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_1	    	:32;
#endif
			MEA_Uint32 num_enb_mac            : 4; /*60:63*/  /*0-2000, 2^1 ,2^2......   */
            MEA_Uint32 max_numof_DataBuffer   : 4; /*56 59*/
            MEA_Uint32 max_numof_descriptor   : 4;
            MEA_Uint32 freeBit51              : 1; /*51*/
            MEA_Uint32 instanc_fifo_1588      : 2;
            MEA_Uint32 hierarchical_en        : 1;   /*48*/
            MEA_Uint32 max_point_2_multipoint :16;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32  ip_fragmentation        : 1; /*64*/
            MEA_Uint32  ip_reassembly           : 1; /*65*/ 
            MEA_Uint32  ActToBMbus              : 8; /*66:73*/
            MEA_Uint32  ingress_sniffer_ID      : 7;
            MEA_Uint32  ingress_sniffer_support : 1;
            MEA_Uint32  pad2                    : 14;

#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32  pad2                    :14;
            MEA_Uint32  ingress_sniffer_support : 1;
            MEA_Uint32  ingress_sniffer_ID      : 7;
            MEA_Uint32  ActToBMbus              : 8;/*66:73*/
            MEA_Uint32  ip_reassembly           :1;
            MEA_Uint32  ip_fragmentation        :1;

#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32  ip_reassembly_session_id_key_width  : 8;
            MEA_Uint32  reserv3                             : 16;
            MEA_Uint32  ip_session_id_width                 : 4;
            MEA_Uint32  pad3                                : 4;

#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
	MEA_Uint32 pad64_3	    	:32;
#endif
            MEA_Uint32  pad3                                : 4;
            MEA_Uint32  ip_session_id_width                 : 4;
            MEA_Uint32  reserv3                             : 16;
            MEA_Uint32  ip_reassembly_session_id_key_width  : 8;
#endif
        }val;
        MEA_Uint32         index_1_regs[4];
     } global1;
    /****************************************/
    /* hpm info                           */
    /****************************************/
    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 hpm_valid        : 1; /*0*/
            MEA_Uint32 numof_UE_PDN     : 5; /*5:1*/
            MEA_Uint32 hpm_profile      : 7; /*12:6*/
            MEA_Uint32 hpm_numof_rules  : 6; /*18:13*/
            MEA_Uint32 hpm_Type         : 1; /*19*/  /*1 - L2_HPM + L3_HPM, ‘0’ - L2_HPM only */
            MEA_Uint32 hpm_numOfAct     : 5; /*24:20 */
            MEA_Uint32 hpm_Mask_profile : 4; /*28:25*/
            MEA_Uint32 hpm_new          : 1; /*29*/
            MEA_Uint32 reserved         : 2; /*31:30*/

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 reserved         : 2; /*31:30*/
            MEA_Uint32 hpm_new          : 1; /*29*/
            MEA_Uint32 hpm_Mask_profile : 4; /*28:25*/
            MEA_Uint32 hpm_numOfAct     : 5; /*24:20 */
            MEA_Uint32 hpm_Type         : 1; /*19*/  /*1 - L2_HPM + L3_HPM, ‘0’ - L2_HPM only */
            MEA_Uint32 hpm_numof_rules  : 6; /*18:13*/
            MEA_Uint32 hpm_profile      : 7; /*12:6*/
            MEA_Uint32 numof_UE_PDN     : 5; /*5:1*/
            MEA_Uint32 hpm_valid        : 1; /*0*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 hpm_BASE : 20;
            MEA_Uint32 SER_TeId : 5;
            MEA_Uint32 pad1     : 7;

#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 pad1     : 7;
            MEA_Uint32 SER_TeId : 5;
            MEA_Uint32 hpm_BASE : 20;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 temp2;
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32 temp2;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 temp3;
#ifdef ARCH64
            MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_3	    	:32;
#endif
            MEA_Uint32 temp3;
#endif

            } val;
        MEA_Uint32         index_2_regs[4];
     } hpm;


     union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 exist             : 1;
            MEA_Uint32 reserved          : 7;
            MEA_Uint32 table_size        : 16;
            MEA_Uint32 vsp_exist         : 1;
            MEA_Uint32 cam_exist         : 1;
            MEA_Uint32 pad               : 6;

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad               : 6;
            MEA_Uint32 cam_exist         : 1; // for table 4
            MEA_Uint32 vsp_exist         : 1; // for ATM
            MEA_Uint32 table_size        : 16;
            MEA_Uint32 reserved          : 7;
            MEA_Uint32 exist             : 1;
#endif
         }val;
         MEA_Uint32 index_3_regs[1];
     }pre_parser;

     union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
           

             MEA_Uint32 exist               : 1; /*0*/
             MEA_Uint32 parser_etype_cam    : 1; /*1*/
             MEA_Uint32 reduce_l2_bigof6    : 1; /*2*/
             MEA_Uint32 support_IPV6        : 1; /*3*/
             MEA_Uint32 reduce_l2_mpls      : 1; /*4*/
             MEA_Uint32 reserved            : 3;
             MEA_Uint32 table_size          : 15;
             MEA_Uint32 numofBit            : 9;

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 numofBit             : 9;
            MEA_Uint32 table_size           : 15;
            MEA_Uint32 reserved             : 3;
            MEA_Uint32 reduce_l2_mpls       : 1; /*4*/
            MEA_Uint32 support_IPV6         : 1; /*3*/
            MEA_Uint32 reduce_l2_bigof6     : 1; /*2*/
            MEA_Uint32 parser_etype_cam     : 1; /*1*/
            MEA_Uint32 exist                : 1; /*0*/

#endif
         }val;
         MEA_Uint32 index_4_regs[1];
     }parser;
    
     union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
             MEA_Uint32 exist             : 1;
             MEA_Uint32 serviceRangeExist : 1;
             MEA_Uint32 reserved          : 6;
             MEA_Uint32 table_size        : 16;
             MEA_Uint32 num_service_hash  : 3;
             MEA_Uint32 pad               : 5;

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad               : 5;
            MEA_Uint32 num_service_hash  : 3;
            MEA_Uint32 table_size        : 16;
            MEA_Uint32 reserved          : 6;
            MEA_Uint32 serviceRangeExist : 1; /*1*/
            MEA_Uint32 exist             : 1; /*0*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
         MEA_Uint32 ServiceRange_Num_of_range : 8;  
         MEA_Uint32 numofRangeProfile         : 7;
		 MEA_Uint32 pad1                     : 17;

#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 pad1                  : 17;
            MEA_Uint32 numofRangeProfile     : 7;
            MEA_Uint32 ServiceRange_Num_of_range:8;  /*32:39*/
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 EVC_Classifier_exist          : 1;  /*64*/
            MEA_Uint32 EVC_Classifier_num_hash_group : 4; /*65:68*/
            MEA_Uint32 EVC_classifier_key_width      : 5; /*69:73*/
            MEA_Uint32 reserv_1                      : 1; /*74*/
            MEA_Uint32 srv_externl_size               :3; /*75:78*/
            MEA_Uint32 pad2                          : 19; /*79:95*/

#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32 pad2                          : 19; /*79:95*/
            MEA_Uint32 srv_externl_size              : 3; /*75:78*/   /*0-no external 1-128K  2-256K 3-512K 4-1M */
            MEA_Uint32 reserv_1                      : 1; /*74*/
            MEA_Uint32 EVC_classifier_key_width       : 5; /*69:73*/
            MEA_Uint32 EVC_Classifier_num_hash_group  : 4; /*65:68*/
            MEA_Uint32 EVC_Classifier_exist           : 1;  /*64*/
#endif


         }val;
         MEA_Uint32 index_5_regs[3];
     }service;

     union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            

            MEA_Uint32 exist             : 1;
            MEA_Uint32 extended_lxcp     : 1;
			MEA_Uint32 reserved          : 6;
            MEA_Uint32 table_size        : 16;
            MEA_Uint32 lxcp_act_width     : 4;
            MEA_Uint32 lxcp_prof_width    : 4;  //  4 for 16 prof
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 lxcp_prof_width    : 4;  //  4 for 16 prof 
            MEA_Uint32 lxcp_act_width     : 4; // Num_of Action
            MEA_Uint32 table_size        : 16;
            MEA_Uint32 reserved          : 6;
			MEA_Uint32 extended_lxcp     : 1;
            MEA_Uint32 exist             : 1;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
		   MEA_Uint32 new_lxcp_act_width     : 16; //
           MEA_Uint32 new_lxcp_prof_width    : 16;  // 

#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
			MEA_Uint32 new_lxcp_prof_width    : 16;  //  
			MEA_Uint32 new_lxcp_act_width     : 16; // 
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 acl_context : 16; /*64 79*/
            MEA_Uint32 pad3 : 16;  // 
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32 pad3 : 16;  //  
            MEA_Uint32 acl_context : 16; /*64 79*/ 
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN  
    MEA_Uint32  ACL5_exist           : 1 ;     // bit 96
    MEA_Uint32  ACL5_Ipmask_size     : 4;
    MEA_Uint32  ACL5_rangeProf_size  : 4;
    MEA_Uint32  ACL5_keymask_size    : 4;
    MEA_Uint32  ACL5_rangeBlock      : 3; /* 1-2 2-4 3-8 4-16 5-32 */
    MEA_Uint32  ACL5_External        :1;
    MEA_Uint32  ACL5_Internal        :1;
    MEA_Uint32 pad4                  :14;
#ifdef ARCH64
            MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_3	    	:32;
#endif
    MEA_Uint32  pad4 : 14;
    MEA_Uint32  ACL5_Internal : 1;
    MEA_Uint32  ACL5_External : 1;
    MEA_Uint32  ACL5_rangeBlock : 3; /* 1-2 2-4 3-8 4-16 5-32 */
    MEA_Uint32  ACL5_keymask_size : 4;
    MEA_Uint32  ACL5_rangeProf_size : 4;
    MEA_Uint32  ACL5_Ipmask_size : 4;
    MEA_Uint32  ACL5_exist : 1;     // bit 96
#endif
         }val;
         MEA_Uint32 index_6_regs[4];
     }acl;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            

             MEA_Uint32 exist             : 1;
             MEA_Uint32 reserved          : 7;
             MEA_Uint32 table_size        : 16;
             MEA_Uint32 pad               : 8;

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad               : 8;
            MEA_Uint32 table_size        : 16;
            MEA_Uint32 reserved          : 7;
            MEA_Uint32 exist             : 1;

#endif
         }val;
         MEA_Uint32 index_7_regs[1];
     }forwarder_lmt;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 

            MEA_Uint32 exist                    : 1;
            MEA_Uint32 adm_support              : 1;
            MEA_Uint32 dse_external             : 1;
            MEA_Uint32 segment_switch_support   : 1;
            MEA_Uint32 reserved                 : 4;
            MEA_Uint32 addres_size              : 16;
            MEA_Uint32 fwd_action               : 5;
            MEA_Uint32 pad3                     : 3;

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad3                     : 3;
            MEA_Uint32 fwd_action               : 5;
            MEA_Uint32 addres_size              : 16;
            MEA_Uint32 reserved                 : 4;
            MEA_Uint32 segment_switch_support   : 1;
            MEA_Uint32 dse_external             : 1;
            MEA_Uint32 adm_support              : 1;
            MEA_Uint32 exist                    : 1;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            
             MEA_Uint32 reserve               :  7;
             MEA_Uint32 width_learn_action_id :  5;
             MEA_Uint32 width_learn_src_port  :  4;
             MEA_Uint32 show_all              : 1;
             MEA_Uint32 lpm_enable            : 1;
             MEA_Uint32 lpm_ipv4              : 9;
             MEA_Uint32 lpm_ipv6              : 4;
             MEA_Uint32 pad                   : 1;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
             MEA_Uint32 pad                   : 1;

             MEA_Uint32 lpm_ipv6              : 4;
             MEA_Uint32 lpm_ipv4              : 9;
             MEA_Uint32 lpm_enable            : 1;
             MEA_Uint32 show_all              : 1;
             MEA_Uint32 width_learn_src_port  :  4;
             MEA_Uint32 width_learn_action_id :  5;
             MEA_Uint32 reserve               :  7;
#endif
         }val;
         MEA_Uint32 index_8_regs[2];
     }forwarder;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 

             MEA_Uint32 shaper_per_port_exist       : 1;  /* 0 - 1*/
             MEA_Uint32 shaper_per_cluster_exist    : 1;  /* 1 - 1*/
             MEA_Uint32 shaper_per_Priqueue_exist   : 1;  /* 2 - 2*/
             MEA_Uint32 reserve                    : 7;  /* 3 - 9 */
             MEA_Uint32 xCIR_en                     :1;   /* 10 */
             MEA_Uint32 reserve1                    : 4;  /* 11 - 14 */
             MEA_Uint32 num_of_shaper_profile       : 17;/*15 21*/
             
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
           
            MEA_Uint32 num_of_shaper_profile       : 17;/*15 21*/
            MEA_Uint32 reserve1 : 4;  /* 11 - 14 */
            MEA_Uint32 xCIR_en : 1;   /* 10 */
            MEA_Uint32 reserve : 7;  /* 3 - 9 */
            MEA_Uint32 shaper_per_Priqueue_exist   : 1;  /* 2 - 2*/
            MEA_Uint32 shaper_per_cluster_exist    : 1;  /* 1 - 1*/
            MEA_Uint32 shaper_per_port_exist       : 1;  /* 0 - 1*/

#endif
         }val;
         MEA_Uint32 index_9_regs[1];
     }shaper;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 

             MEA_Uint32 exist                    : 1;
             MEA_Uint32 Ingress_flow_exist       : 1;
             MEA_Uint32 reduce                   : 1;
             MEA_Uint32 num_of_acm               : 4;
             MEA_Uint32 reserved                 : 1;
             MEA_Uint32 num_of_TMID              : 16;
             MEA_Uint32 hidden_profile           : 3;
             MEA_Uint32 Ingress_flow_num_of_prof : 5;
             
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 Ingress_flow_num_of_prof  : 5;
            MEA_Uint32 hidden_profile            : 3;
            MEA_Uint32 table_size                :16; /*TM*/
            MEA_Uint32 yellow_reduce             : 1; /* yellow reduce*/
            MEA_Uint32 num_of_acm                : 4;
            MEA_Uint32 reduce                    : 1;
            MEA_Uint32 Ingress_flow_exist        : 1;
            MEA_Uint32 exist                     : 1;

#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 num_of_profile   : 8; /* 32.39 */
            MEA_Uint32 num_of_wred_prof : 4;/*40 43*/
            MEA_Uint32 num_of_wred_acm  : 3;/*44..46*/
            MEA_Uint32 policer_slowslow : 1; /*47*/
			MEA_Uint32 CBS_bucket_bits  : 8; /*48:55*/
			MEA_Uint32 CIR_Token_bits   : 8; /*56:63*/
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 CIR_Token_bits   : 8; /*56:63*/
			MEA_Uint32 CBS_bucket_bits  : 8; /*48:55*/       /*If the value is zero in old designs the bit width is 18.*/
            MEA_Uint32 policer_slowslow : 1; /*47*/
            MEA_Uint32 num_of_wred_acm  : 3; /*44..46*/
            MEA_Uint32 num_of_wred_prof : 4; /* 40 43*/
            MEA_Uint32 num_of_profile   : 8; /* 32.39 */
#endif

         }val;
         MEA_Uint32 index_10_regs[2];
     }policer;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 

            MEA_Uint32 cluster_exist                : 1;
            MEA_Uint32 queue_exist                  : 1;
            MEA_Uint32 clusterTo_multiport_exist    : 1;
            MEA_Uint32 queue_count_exist            : 1;
            MEA_Uint32 pad                          : 4;
            MEA_Uint32 num_of_cluster               : 16;
            MEA_Uint32 maxCluserperport             : 8;

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 maxCluserperport             : 8;
            MEA_Uint32 num_of_cluster               : 16; //num of cluster
            MEA_Uint32 pad                          : 4;
            MEA_Uint32 queue_count_exist            : 1;
            MEA_Uint32 clusterTo_multiport_exist    : 1;
            MEA_Uint32 queue_exist                  : 1;
            MEA_Uint32 cluster_exist                : 1;
#endif
         }val;
         MEA_Uint32 index_11_regs[1];
     }wfq; 

    union {
         struct{

#if __BYTE_ORDER == __LITTLE_ENDIAN 

             MEA_Uint32 exist             : 1;
             MEA_Uint32 reserved          : 7;
             MEA_Uint32 table_size        : 20;
             MEA_Uint32 pad               : 4;

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad               : 4;
            MEA_Uint32 table_size        : 20;
            MEA_Uint32 reserved          : 7;
            MEA_Uint32 exist             : 1;

#endif
         }val;
         MEA_Uint32 index_12_regs[1];
     }action;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
             MEA_Uint32 exist               : 1; /* 0  0*/
             MEA_Uint32 LAG_groups_InGroup  : 5; /* 1  5*/
             MEA_Uint32 reserved            : 2; /* 6  7*/
             MEA_Uint32 xper_table_size     : 16;/* 8 23 */
             MEA_Uint32 numof_mc_lag        : 4; /*24:27*/
             MEA_Uint32 pad                 : 4; /*24 31*/

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad                  : 4; /*28 31*/
            MEA_Uint32 numof_mc_lag         : 4; /*24:27*/
            MEA_Uint32 xper_table_size      : 16;/* 8 23 */
            MEA_Uint32 reserved             : 1; /* 7  7*/ //newxpr
            MEA_Uint32 LAG_groups_InGroup   : 5; /* 1  6*/
            MEA_Uint32 exist                : 1; /* 0  0*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 num_lag_prof          : 5; /*32:36*/
            MEA_Uint32 reserved1              :4;
            MEA_Uint32 num_clusterInGroup    : 2; /*9 10*/ /*41 42*/    // 0-64 1- 128 2-256 .
            MEA_Uint32 num_ofMSTP            : 4;
            MEA_Uint32 sflow_support         : 1; /*15*/    /*47*/
            MEA_Uint32 sflow_count_lg        : 4; /*16 19*/ /*48 51*/
            MEA_Uint32 pad1                  : 12;  

#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
           
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
           MEA_Uint32 pad1                  : 12;
           MEA_Uint32 sflow_count_lg        : 4; /*16 19*/ /*48 51*/
           MEA_Uint32 sflow_support         : 1;
           MEA_Uint32 num_ofMSTP            : 4; /* 43:*/
           MEA_Uint32 num_clusterInGroup    : 2; /*9 10*/ /*41 42*/    // 0-64 1- 128 2-256 
           MEA_Uint32 reserved1             : 4;
           MEA_Uint32 num_lag_prof          : 5; /*32:36*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
           MEA_Uint32 num_xper_vec_Group    : 16; /*64:79*/
           MEA_Uint32 pad2                  : 16;

#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
           MEA_Uint32 pad2                : 16;
           MEA_Uint32 num_xper_vec_Group  : 16; /*64:79*/
#endif


         }val;
         MEA_Uint32 index_13_regs[3];
     }xpermission;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
           

             MEA_Uint32 exist             : 1;
             MEA_Uint32 reserved          : 7;
             MEA_Uint32 table_size        : 16;
             MEA_Uint32 pad               : 8;

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad               : 8;
            MEA_Uint32 table_size        : 16;
            MEA_Uint32 reserved          : 7;
            MEA_Uint32 exist             : 1;
#endif
         }val;
         MEA_Uint32 index_14_regs[1];
     }sar_machine;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 

            MEA_Uint32 maritini_mac_support    : 1;/*bit 0 */
            MEA_Uint32 atm_support             : 1; /*bit 1 */
            MEA_Uint32 router_support          : 1;/*bit 2 */
            MEA_Uint32 PBB_editing_machines_en              : 1; /*bit 3 */
            MEA_Uint32 stamp802_1p_support                  : 1; /*bit 4 */
            MEA_Uint32 PPPoE_editing_machines_en            : 1; /*bit 5 */ 
            MEA_Uint32 mpls_flooding_editing_machines_en    : 1; /*bit 6 */
            MEA_Uint32 MPLS_editing_machines_en             : 1; /*bit  7 */
            MEA_Uint32 num_of_edit_width                    : 5; /*bit 8 12*/
            MEA_Uint32 pad11                   : 3; /**13 14 15*/
            MEA_Uint32 EDIT_DB_EN              :1; /*16*/
            MEA_Uint32 EDIT_DB_num_of_bits     :7;

            MEA_Uint32 BMF_CCM_count_stamp     : 1;
            MEA_Uint32 BMF_CCM_count_EN        : 1;
            MEA_Uint32 pad                     : 6;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad                     : 6;
            MEA_Uint32 BMF_CCM_count_EN        : 1;
            MEA_Uint32 BMF_CCM_count_stamp     : 1;
            MEA_Uint32 EDIT_DB_num_of_bits     : 7;
            MEA_Uint32 EDIT_DB_EN              : 1; /*16*/
            MEA_Uint32 pad11                   : 3; /**13 14 15*/
            MEA_Uint32 num_of_edit_width       : 5;
            MEA_Uint32 MPLS_editing_machines_en : 1;
            MEA_Uint32 mpls_flooding_editing_machines_en : 1; /*bit 6 */
            MEA_Uint32 PPPoE_editing_machines_en : 1; /*bit 5 */
            MEA_Uint32 stamp802_1p_support     : 1; /*bit 4 */
            MEA_Uint32 PBB_editing_machines_en : 1;
            MEA_Uint32 router_support          : 1;
            MEA_Uint32 atm_support             : 1;
            MEA_Uint32 maritini_mac_support    : 1;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32   num_of_lm_count            :16;
        MEA_Uint32 num_of_MC_Edit_Per_Cluster   :12;
        MEA_Uint32 MC_Edit_new_mode             : 1;
        MEA_Uint32 pad1                         : 3;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
       MEA_Uint32 pad1                          : 3;
       MEA_Uint32 MC_Edit_new_mode              : 1;
       MEA_Uint32 num_of_MC_Edit_Per_Cluster    :12;
       MEA_Uint32 num_of_lm_count               :16;
#endif
         }val;
         MEA_Uint32 index_15_regs[2];
     }editor_BMF;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
             MEA_Uint32 reserved                : 16;
             MEA_Uint32 width_pm_id             :  5;  /*NumOfPmIds*/
             MEA_Uint32 num0fBlocksForPmPackets :  5;
             MEA_Uint32 num0fBlocksForBytes     :  5;
             MEA_Uint32 pad                     :  1;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad                      :  1;
            MEA_Uint32 num0fBlocksForBytes      :  5;
            MEA_Uint32 num0fBlocksForPmPackets  :  5;
            MEA_Uint32 width_pm_id              :  5;
            MEA_Uint32 reserved                 : 16;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 extend_PM_NUM : 16;
            MEA_Uint32 pad1          : 16;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 pad1          : 16;
            MEA_Uint32 extend_PM_NUM : 16;
#endif
         }val;
         MEA_Uint32 index_16_regs[2];
     }pm;

    union {
         struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
             MEA_Uint32 PacketGen_exist                     : 1;
             MEA_Uint32 Analyzer_exist                      : 1;
             MEA_Uint32 PacketGen_On_Port_num               : 7;
             MEA_Uint32 Analyzer_On_Port_num                : 7;
             MEA_Uint32 Pktgen_type1_num_of_streams         : 16;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
             MEA_Uint32 Pktgen_type1_num_of_streams         :16;/*16 31*/ 
             MEA_Uint32 Analyzer_On_Port_num                : 7;/*9 15*/
             MEA_Uint32 PacketGen_On_Port_num               : 7;/*2 8*/
             MEA_Uint32 Analyzer_exist                      : 1;/*1 1*/
             MEA_Uint32 PacketGen_exist                     : 1; /*0 0*/
#endif
              /* word 1*/
#if __BYTE_ORDER == __LITTLE_ENDIAN 
             MEA_Uint32 Pktgen_type2_num_of_streams     :16;
             MEA_Uint32 Analyzer_type1_num_of_streams   :16;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
             MEA_Uint32 Analyzer_type1_num_of_streams   :16;
             MEA_Uint32 Pktgen_type2_num_of_streams     :16; /* 0 15 */   /*32 47*/
             
#endif
                           /* word 2*/
#if __BYTE_ORDER == __LITTLE_ENDIAN 
             
             MEA_Uint32 Analyzer_type2_num_of_streams   :16;
             MEA_Uint32 PacketGen_num_of_support_profile :7;
             MEA_Uint32 Analyzer_num_of_support_profile  :9;
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
             
#else
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
             
             MEA_Uint32 Analyzer_num_of_support_profile  :9;
             MEA_Uint32 PacketGen_num_of_support_profile :7;
             MEA_Uint32 Analyzer_type2_num_of_streams   :16;//64
             
#endif


         }val;


             MEA_Uint32 index_17_regs[3];
    }packetGen;

    union {
        struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 slice0_tunnel : 1; /*0*/
            MEA_Uint32 slice1_tunnel : 1; /*1*/
            MEA_Uint32 slice2_tunnel : 1; /*2*/
            MEA_Uint32 slice3_tunnel : 1; /*3*/
            MEA_Uint32 slice4_tunnel : 1; /*4*/
            MEA_Uint32 pad_0         : 3; /*5:7 */
            MEA_Uint32 portIndx0     : 8; /*8:15*/
            MEA_Uint32 portIndx1     : 8; /*16 :23*/
            MEA_Uint32 portIndx2     : 8; /*24:31*/
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 portIndx2     : 8; /*24:31*/
            MEA_Uint32 portIndx1     : 8; /*16 :23*/
            MEA_Uint32 portIndx0     : 8; /*8:15*/
            MEA_Uint32 pad_0         : 3; /*5:7 */
            MEA_Uint32 slice4_tunnel : 1; /*4*/
            MEA_Uint32 slice3_tunnel : 1; /*3*/
            MEA_Uint32 slice2_tunnel : 1; /*2*/
            MEA_Uint32 slice1_tunnel : 1; /*1*/
            MEA_Uint32 slice0_tunnel : 1; /*0*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 portIndx3 : 8; /*32:39*/
            MEA_Uint32 portIndx4 : 8; /*40:47*/
            MEA_Uint32 pad_1     : 16;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 pad_1     : 16;
            MEA_Uint32 portIndx4 : 8; /*40:47*/
            MEA_Uint32 portIndx3 : 8; /*32:39*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 crypto_db_prof_width : 4; /*64:67*/
            MEA_Uint32 pad_2 : 18;
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32 pad_2 : 18;
            MEA_Uint32 crypto_db_prof_width : 4; /*64:67*/
#endif

           
        }val;
        MEA_Uint32 index_18_regs[3];
    }nvgr_tunnel;


    union {
        struct{
            MEA_Uint32 Interface_0;        // 2bit fields
            MEA_Uint32 Interface_1;
            MEA_Uint32 Interface_2;
            MEA_Uint32 Interface_3;
        }val;
        MEA_Uint32 index_21_regs[4];
    }Rx_mac_fifo;
    
    union {
    struct{
        MEA_Uint32 Interface_0;        // 2bit fields
        MEA_Uint32 Interface_1;
        MEA_Uint32 Interface_2;
        MEA_Uint32 Interface_3;
        }val;
        MEA_Uint32 index_22_regs[4];
    }Tx_mac_fifo;

    union {
        struct{
            /* word 0*/ // for AFDX
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 fragment_Edit_exist                 : 1; /*0 0*/
            MEA_Uint32 fragment_Edit_num_of_session        : 15;/*1 15*/
            MEA_Uint32 fragment_Edit_num_of_port           : 8;/*16 23*/
            MEA_Uint32 fragment_Edit_num_of_group          : 8;/*24 31*/
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 fragment_Edit_num_of_group          : 8;/*24 31*/
            MEA_Uint32 fragment_Edit_num_of_port           : 8;/*16 23*/
            MEA_Uint32 fragment_Edit_num_of_session        : 15;/*1 15*/
            MEA_Uint32 fragment_Edit_exist                 : 1; /*0 0*/
#endif
            /* word 1*/
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 fragment_vsp_num_of_session        : 16;/*1 15*/
            MEA_Uint32 fragment_vsp_num_of_port           : 8;/*16 23*/
            MEA_Uint32 fragment_vsp_num_of_group          : 8;/*24 31*/
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 fragment_vsp_num_of_group          : 8;/*24 31*/
            MEA_Uint32 fragment_vsp_num_of_port           : 8;/*16 23*/
            MEA_Uint32 fragment_vsp_num_of_session        : 16;/*1 15*/

#endif
           // word 2
#if __BYTE_ORDER == __LITTLE_ENDIAN 
     MEA_Uint32 fragment_vsp_exist                 : 1; /*0 0*/
     MEA_Uint32 pad2                               : 31;
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32 pad2                               : 31;
            MEA_Uint32 fragment_vsp_exist                 : 1; /*0 0*/
#endif
        }val;


        MEA_Uint32 index_23_regs[3];
    }fragment;

    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 exist             : 1;
            MEA_Uint32 type              : 3;
            MEA_Uint32 num0fspe          : 4;
            MEA_Uint32 start_ts          : 9;
            MEA_Uint32 pad               : 15;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad               : 15;
            MEA_Uint32 start_ts          : 9;
            MEA_Uint32 num0fspe          : 4;
            MEA_Uint32 type              : 3; // sbi = 0 pcm =1 pcm_mux=2
            MEA_Uint32 exist             : 1;
#endif
        }val;
        MEA_Uint32 index_24_regs[1];
    }Interface_Tdm_A;

    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 exist             : 1;
            MEA_Uint32 type              : 3;
            MEA_Uint32 num0fspe          : 4;
            MEA_Uint32 start_ts          : 9;
            MEA_Uint32 pad               : 15;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad               : 15;
            MEA_Uint32 start_ts          : 9;
            MEA_Uint32 num0fspe          : 4;
            MEA_Uint32 type              : 3; // sbi = 0 pcm =1 pcm_mux=2
            MEA_Uint32 exist             : 1;
#endif
        }val;
        MEA_Uint32 index_25_regs[1];
    }Interface_Tdm_B;
    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 exist             : 1;
            MEA_Uint32 type              : 3;
            MEA_Uint32 num0fspe          : 4;
            MEA_Uint32 start_ts          : 9;
            MEA_Uint32 pad               : 15;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad               : 15;
            MEA_Uint32 start_ts          : 9;
            MEA_Uint32 num0fspe          : 4;
            MEA_Uint32 type              : 3; // sbi = 0 pcm =1 pcm_mux=2
            MEA_Uint32 exist             : 1; /*bit0*/
#endif
        }val;
        MEA_Uint32 index_26_regs[1];
    }Interface_Tdm_C;
    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32 num_of_ces        :12; /*bit0 11*/
        MEA_Uint32 ts_size           :10;     
        MEA_Uint32 mode_satop_only   :1;
        MEA_Uint32 pad               :9;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 pad               :9;
        MEA_Uint32 mode_satop_only   :1;
        MEA_Uint32 ts_size           :10;     
        MEA_Uint32 num_of_ces        :12; /*bit0 11*/
#endif
        }val;
        MEA_Uint32 index_27_regs[1];
    }global_Tdm;

    union {
        struct{
            MEA_Uint32 ports_mii_0_31;        // bit fields
            MEA_Uint32 ports_mii_32_63;
            MEA_Uint32 ports_mii_64_95;
            MEA_Uint32 ports_mii_96_127;
        }val;
        MEA_Uint32 index_29_regs[4];
    }mac_mii_support;

    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 Hc_classifier_exist       :1; /*0*/  
            MEA_Uint32 Hc_classifierRang_exist   :1; /*1*/
            MEA_Uint32 Hc_rmon_exist             :1; /*2*/
            MEA_Uint32 Num_of_hcid               :16;
            MEA_Uint32 Hc_number_of_hash_groups  :2;     
            MEA_Uint32 Hc_classifier_key_width   :11;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 Hc_classifier_key_width     :11; /*31:21*/
            MEA_Uint32 Hc_number_of_hash_groups    :2;  /*20;19*/
            MEA_Uint32 Num_of_hcid                 :16; /*18:3*/ 
            MEA_Uint32 Hc_rmon_exist               :1;  /*2*/
            MEA_Uint32 Hc_classifierRang_exist     :1;  /*1*/
            MEA_Uint32 Hc_classifier_exist         :1;  /*0*/
#endif
        /*w1*/    
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 Hc_num_of_ranges             :8; 
            MEA_Uint32 Hc_rangs_onPort              :6;
            MEA_Uint32 Hc_number_hashflow           :16;  /*61:46*/
            MEA_Uint32 w1_reserved1                 :2;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 w1_reserved1                 :2;
            MEA_Uint32 Hc_number_hashflow           :16;  /*61:46*/
            MEA_Uint32 Hc_rangs_onPort              :6;  /*45:40*/
           MEA_Uint32 Hc_num_of_ranges              :8; /*39:32*/
#endif

            MEA_Uint32 reserved2;
            MEA_Uint32 reserved3;
        }val;
        MEA_Uint32 index_34_regs[4];
    }Hc_Classifier ;

    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 Egress_filter_exist                  :1; /*0*/  
            MEA_Uint32 Egress_classifierRang_exist          :1; /*1*/
            MEA_Uint32 Egress_classifier_number_contex      :13; /*2:14*/
            MEA_Uint32 Egress_classifier_Key_width           :7; /*15 21 */
            MEA_Uint32 Egress_classifier_hash_groups        :2; /*22:23*/    
            MEA_Uint32 Egress_classifier_num_of_rangPort    :8; /*24:31 */
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 Egress_classifier_num_of_rangPort    :8; /*24:31 */
            MEA_Uint32 Egress_classifier_hash_groups        :2; /*22:23*/   
            MEA_Uint32 Egress_classifier_Key_width           :7; /*15 21 */
            MEA_Uint32 Egress_classifier_number_contex                  :13; /*2:14*/
            MEA_Uint32 Egress_classifierRang_exist          :1; /*1*/
            MEA_Uint32 Egress_filter_exist                  :1; /*0*/  
#endif
            /*w1*/    
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 Egress_classifier_of_ranges          :8;
            MEA_Uint32 Egress_classifier_number_hashflow    :16;
            MEA_Uint32 Egress_classifier_exist              :1; /*56*/ 
            MEA_Uint32 Egress_parser_exist                  : 1; /*57*/
            MEA_Uint32 w1_reserved1                         :6;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 w1_reserved1                            :6;
            MEA_Uint32 Egress_parser_exist                     :1; /*57*/ 
            MEA_Uint32 Egress_classifier_exist                 :1; /*56*/ 
            MEA_Uint32 Egress_classifier_number_hashflow       :16; /*55:40*/
            MEA_Uint32 Egress_classifier_of_ranges             :8; /*39:32*/
#endif

            MEA_Uint32 reserved2;
            MEA_Uint32 reserved3;
        }val;
        MEA_Uint32 index_35_regs[4];
    }Egress_Classifier ;

    union {
        struct{
            MEA_Uint32 ports_0_31;        // bit fields
            MEA_Uint32 ports_32_63;
            MEA_Uint32 ports_64_95;
            MEA_Uint32 ports_96_127;
        }val;
        MEA_Uint32 index_40_regs[4];
    }slice0;
    union {
        struct{
            MEA_Uint32 ports_0_31;        // bit fields
            MEA_Uint32 ports_32_63;
            MEA_Uint32 ports_64_95;
            MEA_Uint32 ports_96_127;
        }val;
        MEA_Uint32 index_41_regs[4];
    }slice1;

    union {
        struct{
            MEA_Uint32 ports_0_31;        // bit fields
            MEA_Uint32 ports_32_63;
            MEA_Uint32 ports_64_95;
            MEA_Uint32 ports_96_127;
        }val;
        MEA_Uint32 index_42_regs[4];
    }slice2;
    union {
        struct{
            MEA_Uint32 ports_0_31;        // bit fields
            MEA_Uint32 ports_32_63;
            MEA_Uint32 ports_64_95;
            MEA_Uint32 ports_96_127;
        }val;
        MEA_Uint32 index_43_regs[4];
    }slice3;



}MEA_DeviceInfo_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA DeviceInfo  APIs                                      */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Get_DeviceInfo(MEA_Unit_t          unit_i,
                                  MEA_Uint32          hwUnit_i,
                                  MEA_DeviceInfo_dbt  *entry_po);


typedef enum {
    MEA_DEVICEINFO_NUM_OF_SERVICE = 0,
    MEA_DEVICEINFO_NUM_OF_ACTION = 1,
    MEA_DEVICEINFO_NUM_OF_ACL = 2,
    MEA_DEVICEINFO_NUM_OF_PM = 3,
    MEA_DEVICEINFO_NUM_OF_TM = 4,
    MEA_DEVICEINFO_NUM_OF_ED = 5,
    MEA_DEVICEINFO_NUM_OF_POLICER_PROF = 6,
    MEA_DEVICEINFO_NUM_OF_CLUSTERS = 7,
    MEA_DEVICEINFO_NUM_OF_CLUSTERS_ON_PORT = 8,

    MEA_DEVICEINFO_LAST

}MEA_DeviceInfo_Type_t;

MEA_Status MEA_API_Get_DeviceInfo_Type(MEA_Unit_t          unit_i,
    MEA_Uint32          hwUnit_i,
	MEA_DeviceInfo_Type_t          type,
    MEA_Uint32  *retvalue);



/************************* INDEX-  section 2  DataBase Object*********************/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*    2. DataBase Object                                                         */
/*             2.1 Group Methods                                                */
/*             2.2 Group element Methods                                         */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*********************************************************************************/



/*===============================================================================*/
/* Entity Name        : Group                                                    */
/*                                                                               */
/* Entity Description : Group of Generic Elements                                */
/*                      The group can have limiter or unlimited number of        */
/*                      of elements.                                             */
/*                      The Element can be in any size and in any format.        */
/*                      Each Element has Id and the Element can have data or     */
/*                      without data. This allowed to hold group of IDs only.    */
/*                                                                               */
/* Entity Key         : ENET_Unit_t + ENET_GroupId_t                             */
/*                                                                               */
/* Entity Attributes  : ENET_Group_dbt                                           */
/*                                                                               */
/*===============================================================================*/


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < Group > Defines                                                 */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define ENET_GROUP_MAX_NUMBER_OF_ELEMENTS_UNLIMITED 0xffffffff 
#define ENET_MAX_GROUP_ELEMENT_ID                   0xffffffff

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < Group > Typedefs                                                */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

/* Callback function prototype when delete/set the Group */ 
typedef ENET_Status  (*ENET_Group_Callback       ) (ENET_Unit_t     unit_i,
                                                    ENET_GroupId_t  id_i,
                                                    void*           entry_i);

/* Callback function prototype when create/delete/set tGroupElement in this Group */ 
typedef ENET_Status  (*ENET_GroupElement_Callback) (ENET_Unit_t           unit_i,
                                                     ENET_GroupId_t        groupId_i,
                                                     ENET_GroupElementId_t id_i,
                                                    void*                 entry_i,
                                                    ENET_Uint32           entry_len_i);


/* Group definition */
typedef struct {

  /* Null terminated string that hold the name of the group, 
     Note: If the value is ENET_GENERATE_NEW_NAME that the name will be generate 
           automatic */
  char               name [ENET_PLAT_GROUP_MAX_NAME_SIZE+1];

  /* Max number of elements in this group. 
     Nute: ENET_GROUP_MAX_NUMBER_OF_ELEMENTS_UNLIMITED mean no limit 
           to the number of elements in the group. */
  ENET_Uint32        max_number_of_elements; 

  ENET_GroupElementId_t first_groupElementId;
  /* Optional Callback functions when delete/set the Group */
  struct {
      ENET_Group_Callback deleteX;
      ENET_Group_Callback set;
  } GroupCallbackFunc;

  /* Optional Callback functions when create/delete/set GroupElement in this group */
  struct {
      ENET_GroupElement_Callback create;
      ENET_GroupElement_Callback deleteX;
      ENET_GroupElement_Callback set;
  } GroupElementCallbackFunc;


} ENET_Group_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < Group > APIs                                                    */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/******************************************************************************
* Function Name:       < ENET_Create_Group>
*
* Description:         This function creates a new Group entity instance, 
*                      and gives Group id handle that can be used later on to 
*                      retrieve this entity instance.
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*                   unit_i  -> identifier of the device.
*                   entry_i -> pointer to the Group structure containing 
*                              the new Group parameters.  
*                   id_io   -> pointer to Group id as input use to 
*                              define the new group id 
*                              OR ENET_GENERATE_NEW_ID to generate automatic new id 
* Output Parameters: 
*                   id_io   -> pointer to Group id as output use to 
*                              get the id that used for create the group instance
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Create_Group (ENET_Unit_t          unit_i,
                               ENET_Group_dbt      *entry_i,
                                ENET_GroupId_t      *id_io);


/******************************************************************************
* Function Name:       < ENET_Delete_Group>
*
* Description:         This function delete an exist Group entity instance, 
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*                   unit_i  -> identifier of the device.
*                   id_i    -> Group id that need to be delete
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Delete_Group(ENET_Unit_t    unit_i,
    ENET_GroupId_t id_i, ENET_Bool Force);




/******************************************************************************
* Function Name: < ENET_Set_Group>
*
* Description:   This function is responsible for modifying an existing 
*                Group entity instance.                    
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i  -> identifier of the device.
*                   id_i    -> Group id to use for the search of the entity that
*                              needs to be modified.        
*                   entry_i -> pointer to a Group structure containing the 
*                              Group's  new parameters
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Set_Group   (ENET_Unit_t      unit_i,
                              ENET_GroupId_t  id_i,
                              ENET_Group_dbt *entry_i);

/******************************************************************************
* Function Name: < ENET_Get_Group>
*
* Description:   This function is responsible for retrieve an existing 
*                Group entity instance.                    
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i  -> identifier of the device.
*                   id_i    -> Group id to use for the search of the entity that
*                              needs to be retrieve.  
* Output Parameters: 
*                   entry_o -> pointer to a Group structure containing the 
*                              current Group's  parameters
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Get_Group   (ENET_Unit_t      unit_i,
                              ENET_GroupId_t  id_i,
                              ENET_Group_dbt *entry_o);

/******************************************************************************
* Function Name: < ENET_GetFirst_Group >
*
* Description:   This function is responsible for retrieving the first 
*                Group entity instance in the DB.
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i  -> identifier of the device.
* Output Parameters: 
*                   id_o    -> pointer to the id of the first Group instance.
*                   entry_o -> pointer to a Group where to store the 
*                              first entity's parameters. 
*                              Note: Can be NULL to reterive only the ID.
*                    found_o -> pointer to ENET_Bool with the search results 
*                              ENET_TRUE=found, ENET_FALSE=not found.
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_GetFirst_Group (ENET_Unit_t       unit_i,
                                 ENET_GroupId_t      *id_o,
                                 ENET_Group_dbt   *entry_o,
                                 ENET_Bool          *found_o);

/******************************************************************************
* Function Name: < ENET_GetNext_Group >
*
* Description:   This function is responsible for retrieving the next 
*                Group entity instance in the DB.
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i  -> identifier of the device.
*                   id_io   -> pointer to the id of the Group instance.before the next
* Output Parameters: 
*                   id_io   -> pointer to the id of the next Group instance.
*                   entry_o -> pointer to a Group where to store the 
*                              first entity's parameters. 
*                              Note: Can be NULL to reterive only the ID.
*                    found_o -> pointer to ENET_Bool with the search results 
*                              ENET_TRUE=found, ENET_FALSE=not found.
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_GetNext_Group (ENET_Unit_t          unit_i,
                                ENET_GroupId_t     *id_io,
                                ENET_Group_dbt   *entry_o,
                                ENET_Bool          *found_o);


/******************************************************************************
* Function Name: < ENET_GetIDByName_Group >
*
* Description:   This function is responsible for retrieving a Group 
*                instance's ID parameter, by is name.
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i  -> identifier of the device.
*                   name_i   -> string containing the name of the Group instance.
* Output Parameters: 
*                    id_o    -> pointer for ENET_GroupId_t that will contain the 
*                              Group's ID.
*                    found_o -> pointer to a Boolean that states if the matching 
*                              Group was found.
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/                                                                                                            
ENET_Status ENET_GetIDByName_Group (ENET_Unit_t         unit_i,
                                    char            *name_i,
                                    ENET_GroupId_t    *id_o,
                                      ENET_Bool        *found_o); 

/******************************************************************************
* Function Name: < ENET_IsValid_Group >
*
* Description:   This function is responsible for checking if the group id 
*                is valid , and the group instance exist
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i   -> identifier of the device.
*                    id_i     -> ENET_GroupId_t that will contain the Group's ID.
*                    silent_i -> Boolean that define if not print log error message
*
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_TRUE, the port id is     valid and    exist.
*     ENET_FALSE,the port id is not valid or not exist
*
*******************************************************************************/                                                                                                               
ENET_Bool  ENET_IsValid_Group(ENET_Unit_t    unit_i,
                              ENET_GroupId_t id_i,
                              ENET_Bool      silent_i);






/*===============================================================================*/
/* Entity Name        : GroupElement                                             */
/*                                                                               */
/* Entity Description : Generic Element in Group                                 */
/*                      The Element can be in any size and in any format.        */
/*                      Each Element has Id and the Element can have data or     */
/*                      without data. This allowed to hold group of IDs only.    */
/*                                                                               */
/* Entity Key         : ENET_Unit_t + ENET_GroupId_t + ENET_GroupElementId_t     */
/*                                                                               */
/* Entity Attributes  : ENET_GroupElement_dbt                                    */
/*                                                                               */
/*                                                                               */
/*===============================================================================*/


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < GroupElement > Defines                                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < GroupElement > Typedefs                                         */
/*                                                                               */
/*-------------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*             < GroupElement > APIs                                             */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
/******************************************************************************
* Function Name:       < ENET_Create_GroupElement>
*
* Description:         This function Add a new GroupElement entity instance ,
*                      to a Group. 
*                      and gives GroupElement id handle that can be used later on to 
*                      retrieve this entity instance.
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*                   unit_i      -> identifier of the device.
*                   groupId_i   -> identifier of the group .
*                   entry_i     -> pointer to the GroupElement data 
*                   entry_len_i -> Length of the GroupElement data
*                   id_io       -> pointer to GroupElement id as input use to 
*                                  define the new groupElement id 
*                                  OR ENET_GENERATE_NEW_ID to generate automatic new id 
*                                  Note: The id can be anyvalue , but should be unique
*                                        inside the group.
* Output Parameters: 
*                   id_io   -> pointer to GroupElement id as output use to 
*                              get the id that used for create the groupEleent instance
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Create_GroupElement (ENET_Unit_t            unit_i,
                                       ENET_GroupId_t         groupId_i,
                                      void                  *entry_i,
                                      ENET_Uint32            entry_len_i,
                                       ENET_GroupElementId_t *id_io);

/******************************************************************************
* Function Name:       < ENET_Create_GroupElement_withInRange>
*
* Description:         This function Add a new GroupElement entity instance ,
*                      to a Group. 
*                      and gives GroupElement id handle that can be used later on to 
*                      retrieve this entity instance.
*                      This API will allocate the id withIn range
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*                   unit_i      -> identifier of the device.
*                   groupId_i      -> identifier of the group .
*                   entry_i        -> pointer to the GroupElement data 
*                   entry_len_i    -> Length of the GroupElement data
*                   startRangeId_i ->  Start valid id
*                   endRangeId_i   -> end   valid id
*                   id_io          -> pointer to GroupElement id as input use to 
*                                     define the new groupElement id 
*                                     OR ENET_GENERATE_NEW_ID to generate automatic new id 
*                                     Note: The id can be any value , but should be unique
*                                           inside the group.
*
* Output Parameters: 
*                   id_io   -> pointer to GroupElement id as output use to 
*                              get the id that used for create the groupEleent instance
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Create_GroupElement_withInRange (ENET_Unit_t            unit_i,
                                                   ENET_GroupId_t         groupId_i,
                                                  void                  *entry_i,
                                                  ENET_Uint32            entry_len_i,
                                                  ENET_GroupElementId_t  startRangeId_i,
                                                  ENET_GroupElementId_t  endRangeId_i,
                                                  ENET_GroupElementId_t *id_io);



/******************************************************************************
* Function Name:       < ENET_Delete_GroupElement>
*
* Description:         This function delete an exist GroupElement entity from 
*                      Group 
*------------------------------------------------------------------------------- 
* Input  Parameters:  
*                   unit_i    -> identifier of the device.
*                   groupId_i -> identifier of the group .
*                   id_i      -> pointer to GroupElement id that need to be delete
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Delete_GroupElement (ENET_Unit_t           unit_i,
                                      ENET_GroupId_t        groupId_i,
                                       ENET_GroupElementId_t id_i);




/******************************************************************************
* Function Name: < ENET_Set_GroupElement>
*
* Description:   This function is responsible for modifying an existing 
*                GroupElement entity instance.                    
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i      -> identifier of the device.
*                   groupId_i   -> identifier of the group .
*                   id_i        -> Group id to use for the search of the entity that
*                                  needs to be modified.        
*                   entry_i     -> pointer to the GroupElement data 
*                   entry_len_i -> Length of the GroupElement data
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Set_GroupElement (ENET_Unit_t              unit_i,
                                   ENET_GroupId_t         groupId_i,
                                   ENET_GroupElementId_t  id_i,
                                   void                  *entry_i,
                                   ENET_Uint32            entry_len_i);

/******************************************************************************
* Function Name: < ENET_Get_GroupElement>
*
* Description:   This function is responsible for retrive an existing 
*                GroupElement entity instance.                    
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i      -> identifier of the device.
*                   groupId_i   -> identifier of the group .
*                   id_i        -> Group id to use for the search of the entity that
*                                  needs to be retrieve.        
*                   entry_len_io -> Length of the GroupElement data. On input - states the length of the entry_o, on output, returns the act*                                    ual length of the returned data.
* Output Parameters: 
*                   entry_o     -> pointer to the GroupElement data 
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_Get_GroupElement (ENET_Unit_t              unit_i,
                                   ENET_GroupId_t         groupId_i,
                                   ENET_GroupElementId_t  id_i,
                                   void                  *entry_o,
                                   ENET_Uint32            *entry_len_io);



/******************************************************************************
* Function Name: < ENET_GetFirst_GroupElement >
*
* Description:   This function is responsible for retrieving the first 
*                GroupElement entity instance inside its group 
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i      -> identifier of the device.
*                   groupId_i   -> identifier of the group .
*                   
* Output Parameters: 
*                   id_o        -> pointer to the id of the first GroupElement
*                                  instance.
*                   entry_o     -> pointer to a GroupElement where to store  
*                                  the first entity's parameters. 
*                                  Note: Can be NULL to reterive only the ID., 
*                                        and then the entry_len_i should be zero.
*                   entry_len_io-> pointer to Length of the GroupElement data    
*                    found_o     -> pointer to ENET_Bool with the search results 
*                                  ENET_TRUE=found, ENET_FALSE=not found.
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_GetFirst_GroupElement (ENET_Unit_t             unit_i,
                                        ENET_GroupId_t          groupId_i,
                                        ENET_GroupElementId_t  *id_o,
                                        void                   *entry_o,
                                        ENET_Uint32             *entry_len_io,
                                        ENET_Bool                *found_o);

/******************************************************************************
* Function Name: < ENET_GetNext_GroupElement >
*
* Description:   This function is responsible for retrieving the next 
*                GroupElement entity instance inside its group 
*                      
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i      -> identifier of the device.
*                   groupId_i   -> identifier of the group .
*                   
*                   id_io       -> pointer to the id of the current GroupElement
*                                  instance Id in this group. (before the next).
* Output Parameters: 
*                   id_io       -> pointer to the id of the next GroupElement
*                                  instance in this group.
*                   entry_o     -> pointer to a GroupElement where to store  
*                                  the next entity's parameters. 
*                                  Note: Can be NULL to reterive only the ID., 
*                                        and then the entry_len_i should be zero.
*                    entry_len_io-> pointer Length of the GroupElement data
*                   found_o     -> pointer to ENET_Bool with the search results 
*                                  ENET_TRUE=found, ENET_FALSE=not found.
*
* Return values:
*     ENET_OK, the function performed the function correctly.
*     ENET_ERROR, the function did not perform the function.
*
*******************************************************************************/
ENET_Status ENET_GetNext_GroupElement  (ENET_Unit_t                unit_i,
                                        ENET_GroupId_t          groupId_i,
                                        ENET_GroupElementId_t  *id_io,
                                        void                   *entry_o,
                                        ENET_Uint32             *entry_len_io,
                                        ENET_Bool               *found_o);

/******************************************************************************
* Function Name: < ENET_IsValid_GroupElement >
*
* Description:   This function is responsible for checking if the groupElement id 
*                is valid , and the groupElement instance exist
*------------------------------------------------------------------------------- 
* Input  Parameters: 
*                   unit_i    -> identifier of the device.
*                   groupId_i -> identifier of the group .
*                    id_i      -> ENET_GroupElementId_t that will contain the 
*                               Group Element's ID.
*                    silent_i  -> Boolean that define if not print log error message
*
* Output Parameters: 
*                   none
*
* Return values:
*     ENET_TRUE, the id is     valid and    exist.
*     ENET_FALSE,the id is not valid or not exist
*
*******************************************************************************/                                                                                                               
ENET_Bool  ENET_IsValid_GroupElement(ENET_Unit_t           unit_i,
                                     ENET_GroupId_t        groupId_i,
                                     ENET_GroupElementId_t id_i,
                                     ENET_Bool             silent_i);








/************************* INDEX-  section 6. obsulite methodes ******************/
/*                                                                               */
/*         The following function are obsulite , please don't use them in new    */
/*         implementations                                                       */
/*                                                                               */
/*                                                                               */
/*********************************************************************************/

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA Conclude API                                           */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Conclude_Services(MEA_Unit_t               unit);



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA CpuPort      Defines                                   */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

#define MEA_MAX_NUMBER_OF_CPU_PORT_INPUT_HOOK_FUNC 5

#define MEA_CPU_PORT_MAX_PACKET_LEN  (MEA_CPU_MAX_MTU_SIZE - sizeof(MEA_CPU_frame_header_t))


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA CpuPort      Typedefs                                  */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

/* MEA Cpu Port Input Hook Function Typedef 
   return 
       MEA_TRUE  if the packet        handle by the HookFunc 
       MEA_FALSE if the packet is not handle by the HookFunc
   Note: Can be more then one Hook Function. 
 */
typedef MEA_Bool (*MEA_CpuPort_InputHookFunc)(MEA_Unit_t unit,
                                              MEA_Port_t src_port,
                                              MEA_Uint32 vsp,
                                              char*      packet,
                                              MEA_Uint32 len);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA CpuPort      APIs                                      */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Set_CpuPort_FramesShowFlag(MEA_Unit_t  unit,
                                              MEA_Bool    flag);

MEA_Status MEA_API_Get_CpuPort_FramesShowFlag(MEA_Unit_t  unit,
                                              MEA_Bool   *flag);

MEA_Status MEA_API_Set_CpuPort_FramesDoubleVlanFlag(MEA_Unit_t  unit,
                                                    MEA_Bool    flag);

MEA_Status MEA_API_Get_CpuPort_FramesDoubleVlanFlag(MEA_Unit_t  unit,
                                                    MEA_Bool   *flag);


MEA_Status MEA_API_Set_CpuPort_SendPakcet(MEA_Unit_t unit,
                                          MEA_Port_t dst_port,
                                          MEA_Uint32 net_tag,
                                          char*      packet,
                                          MEA_Uint32 len);




MEA_Status MEA_API_Set_CpuPort_AddInputHookFunc(MEA_Unit_t unit,
                                                MEA_CpuPort_InputHookFunc rx_hook_func);

MEA_Status MEA_API_Set_CpuPort_DelInputHookFunc(MEA_Unit_t unit,
                                                MEA_CpuPort_InputHookFunc rx_hook_func);



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                    MEA Port State APIs                                        */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PORT_STATE_VLAN_DONT_CARE 0xffff

#define MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID 0x0fff

typedef enum {
    MEA_PORT_STATE_FORWARD=0,
    MEA_PORT_STATE_DISCARD=1,
    MEA_PORT_STATE_LEARN,
    MEA_PORT_STATE_LAST
} MEA_PortState_te;

MEA_Status MEA_API_Set_PortState(MEA_Unit_t unit_i,
                                 MEA_Port_t port_i,
                                 MEA_Uint16 vlan_i, 
                                 MEA_PortState_te state_i,
                                 ENET_QueueId_t  QueueId_i); 

MEA_Status MEA_API_Get_PortState(MEA_Unit_t unit_i,
                                 MEA_Port_t port_i,
                                 MEA_Uint16 vlan_i, 
                                 MEA_PortState_te*  state_o); 


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         PacketGen Defines                                     */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_PACKET_GEN_RMON_PORT       120
#define MEA_PACKET_GEN_MAX_HEADER_LEN   56

#define MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2   120


#define MEA_PACKET_GEN_MAX_NUMBER_PACKET_TO_SEND      0x0000fffe /* 0xffff use for infinite loop */
#define MEA_PACKET_GEN_INFINITE_NUMBER_PACKET_TO_SEND 0x0000ffff 

#define MEA_PACKET_GEN_DATA_PATTERN_VALUE_0000  (0x0000)
#define MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55  (0xAA55)
#define MEA_PACKET_GEN_DATA_PATTERN_VALUE_FFFF  (0xFFFF)

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         PacketGen Typedefs                                    */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef enum {
MEA_PACKET_GEN_PRBS_TYPE_2_9__1,
MEA_PACKET_GEN_PRBS_TYPE_2_11__1,
MEA_PACKET_GEN_PRBS_TYPE_2_15__1,
MEA_PACKET_GEN_PRBS_TYPE_2_20__1,       /*17th + 20th +1*/
MEA_PACKET_GEN_PRBS_TYPE_2_20__1_NOTE1, /*3rd + 20th +1*/ 
MEA_PACKET_GEN_PRBS_TYPE_2_23__1,
MEA_PACKET_GEN_PRBS_TYPE_2_29__1,
MEA_PACKET_GEN_PRBS_TYPE_2_31__1,
MEA_PACKET_GEN_PRBS_TYPE_LAST
}MEA_PacketGen_PRBS_Type_te;
typedef enum {
    MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE=0, /* See MEA_PACKET_GEN_DATA_PATTERN_VALUE_XXX */
    MEA_PACKET_GEN_DATA_PATTERN_TYPE_PRBS,
    MEA_PACKET_GEN_DATA_PATTERN_TYPE_LAST
} MEA_PacketGen_DataPatternType_te;

typedef enum {
    MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE=0,
    MEA_PACKET_GEN_LENGTH_TYPE_INCREMANT,
    MEA_PACKET_GEN_LENGTH_TYPE_DECREMENT,
    MEA_PACKET_GEN_LENGTH_TYPE_RANDOM,
    MEA_PACKET_GEN_LENGTH_TYPE_LAST,
} MEA_PacketGen_LengthType_te;

typedef enum {
    MEA_PACKET_GEN_TIME_STAMP_TYPE_NONE=0,
    MEA_PACKET_GEN_TIME_STAMP_TYPE_STAMP=1,
    MEA_PACKET_GEN_TIME_STAMP_TYPE_LAST
} MEA_PacketGen_TimeStamp_type_te;

typedef enum {
    MEA_PACKET_GEN_SEQ_ID_STAMP_TYPE_NONE=0,
    MEA_PACKET_GEN_SEQ_ID_STAMP_TYPE_STAMP=1,
    MEA_PACKET_GEN_SEQ_ID_STAMP_TYPE_LAST
} MEA_PacketGen_SeqIdStamp_type_te;

typedef enum {
MEA_PACKET_GEN_HEADER_TYPE_1=0,
MEA_PACKET_GEN_HEADER_TYPE_2,
MEA_PACKET_GEN_HEADER_TYPE_LAST
}MEA_PacketGen_Header_type_te;
typedef struct {
    struct {
        MEA_Uint16  valid;
        MEA_Uint16  id;
        MEA_PacketGen_Header_type_te  headerType;  /*0 - 56bytes   1- 120Byte*/
    }profile_info;
    struct {  
        MEA_Uint32     length; /*will take from profile*/
        MEA_Uint8      data[MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2];  
     } header;

    struct {
        MEA_PacketGen_DataPatternType_te pattern_type;
           
        union {
            MEA_Uint8  b[2];
            MEA_Uint16 s;      /* NOTE network order htons(x)*/
        } pattern_value; /*relevant only MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE*/

        
       MEA_Bool    withCRC;
    } data;

    struct{

        MEA_PacketGen_LengthType_te type;
        MEA_Uint16                  start;
        MEA_Uint16                  end;  
        MEA_Uint16                  step; 

    } length;

    struct{
        MEA_Bool                        start_valid;
        MEA_Uint32                      start_value;
        MEA_Bool                        end_valid;
        MEA_Uint32                      end_Value;
        MEA_Uint32                      step_value;
    } seqIdStamp;
    struct{
       MEA_PacketGen_TimeStamp_type_te  type;
       MEA_Uint32                       loction_offset;
    } timeStamp;

    struct {   
        MEA_Uint32    numOf_packets; /* See also MEA_PACKET_GEN_MAX/INFINITE_NUMBER_PACKET_TO_SEND */
        
        /* Note: loop is relevant only at mode  TYPE_INCREMANT,TYPE_DECREMENT 
           the  numOf_packets is not relevant  at this mod the 
           loop filed is (1-30 loops) and for infinity 31*/
        MEA_Uint32    loop;         
        MEA_Uint32    Burst_size;    /* TBD we support 1 burst_size*/
        MEA_Bool      next_packetGen_valid;
        MEA_Uint32    next_packetGen_id;   
        MEA_Bool      active;
        MEA_Bool      Packet_error;
    } control;
    struct {
    MEA_Uint32                     shaper_enable       : 1; /* 0- Disable 1-Enable*/  
    MEA_Uint32                     Shaper_compensation : 2; /* 00-Chunk 01-Packet 11-AAL5 */
    MEA_Uint32                     pad                 :29;
    ENET_ShaperId_t                shaper_Id ; 
    MEA_shaper_info_dbt            shaper_info;
    }shaper;
    struct {
         MEA_Bool resetPrbs;
    }setting;
} MEA_PacketGen_Entry_dbt;

typedef enum {
      MEA_PACKETGEN_SEQ_TYPE_NONE=0,
      MEA_PACKETGEN_SEQ_TYPE_1,   
      MEA_PACKETGEN_SEQ_TYPE_2,
      MEA_PACKETGEN_SEQ_TYPE_3
}MEA_PacketGen_drv_Profile_info_seq_te;

typedef enum {
      MEA_PACKETGEN_STREAM_TYPE_GENERIC=0,
      MEA_PACKETGEN_STREAM_TYPE_LBM_LBR,   
      MEA_PACKETGEN_STREAM_TYPE_DMM_DMR,
      MEA_PACKETGEN_STREAM_TYPE_CCM,
      MEA_PACKETGEN_STREAM_TYPE_LAST
}MEA_PacketGen_drv_Profile_info_stream_type_te;


typedef struct {

#if __BYTE_ORDER == __LITTLE_ENDIAN 
    
    MEA_Uint32  header_length       : 8; /* 0   7 */
    MEA_Uint32  seq_stamping_enable : 1; /* 8   8*/ 
    MEA_Uint32  seq_offset_type     : 2; /* 9  10*/ /* MEA_PacketGen_drv_Profile_info_seq_te */
    MEA_Uint32  forceErrorPrbs      : 1; /* 11 11*/
    MEA_Uint32  stream_type          : 3; /* 12 14*/
    MEA_Uint32  pad                 : 17;/* 15 31*/
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  pad                 : 17;/* 15 31*/
    MEA_Uint32  stream_type         : 3; /* 12 14*/
    MEA_Uint32  forceErrorPrbs      : 1; /* 11 11*/
    MEA_Uint32  seq_offset_type     : 2; /* 9  10*/ /* MEA_PacketGen_drv_Profile_info_seq_te */
    MEA_Uint32  seq_stamping_enable : 1; /* 8   8*/ 
    MEA_Uint32  header_length       : 8; /* 0   7 */
#endif
    
}MEA_PacketGen_drv_Profile_info_entry_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         PacketGen_Profile_info APIs                           */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Create_PacketGen_Profile_info(MEA_Unit_t              unit_i,
                          MEA_PacketGen_drv_Profile_info_entry_dbt      *entry_pi,
                          MEA_PacketGen_profile_info_t                  *id_io);

MEA_Status MEA_API_Set_PacketGen_Profile_info_Entry(MEA_Unit_t                           unit_i,
                           MEA_PacketGen_profile_info_t                 id_i,
                           MEA_PacketGen_drv_Profile_info_entry_dbt     *entry_pi);

MEA_Status MEA_API_Delete_PacketGen_Profile_info_Entry(MEA_Unit_t                    unit_i,
                                                       MEA_PacketGen_profile_info_t  id_i);

MEA_Status MEA_API_Get_PacketGen_Profile_info_Entry(MEA_Unit_t           unit_i,
                               MEA_PacketGen_profile_info_t                           id_i,
                               MEA_PacketGen_drv_Profile_info_entry_dbt  *entry_po);/* Can be NULL */ 

MEA_Status MEA_API_GetFirst_PacketGen_Profile_info_Entry(MEA_Unit_t          unit_i,
                                MEA_PacketGen_profile_info_t                *id_o,
                                MEA_PacketGen_drv_Profile_info_entry_dbt    *entry_po,/* Can be NULL */ 
                                MEA_Bool                                    *found_o);


MEA_Status MEA_API_GetNext_PacketGen_Profile_info_Entry(MEA_Unit_t                      unit_i,
                               MEA_PacketGen_profile_info_t                 *id_io,
                               MEA_PacketGen_drv_Profile_info_entry_dbt     *entry_po,/* Can be NULL */
                               MEA_Bool                                     *found_o);

MEA_Status MEA_API_IsExist_PacketGen_Profile_info(MEA_Unit_t               unit_i,
                                           MEA_PacketGen_profile_info_t    Id_i,
                                           MEA_Bool                       *exist_o);





/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         PacketGen APIs                                        */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/





/* Note: To generate new id the id_io should be set (before call create) 
         to MEA_GENERATE_NEW_VALUE 
        you need  profile info before you call the Create_PacketGen*/
MEA_Status MEA_API_Create_PacketGen_Entry(MEA_Unit_t                    unit_i,
                                          MEA_PacketGen_Entry_dbt      *entry_pi,
                                          MEA_PacketGen_t              *id_io);

MEA_Status MEA_API_Delete_PacketGen_Entry(MEA_Unit_t                    unit_i,
                                          MEA_PacketGen_t               id_i);

MEA_Status MEA_API_Set_PacketGen_Entry(MEA_Unit_t                       unit_i,
                                       MEA_PacketGen_t                  id_i,
                                       MEA_PacketGen_Entry_dbt         *entry_pi);

MEA_Status MEA_API_Get_PacketGen_Entry(MEA_Unit_t                       unit_i,
                                       MEA_PacketGen_t                  id_i,
                                       MEA_PacketGen_Entry_dbt         *entry_po);

MEA_Status MEA_API_GetFirst_PacketGen_Entry(MEA_Unit_t                  unit_i,
                                            MEA_PacketGen_t            *id_o,
                                            MEA_PacketGen_Entry_dbt    *entry_po, /* Can be NULL */
                                            MEA_Bool                   *found_o);

MEA_Status MEA_API_GetNext_PacketGen_Entry(MEA_Unit_t                   unit_i,
                                           MEA_PacketGen_t             *id_io,
                                           MEA_PacketGen_Entry_dbt     *entry_po, /* Can be NULL */
                                           MEA_Bool                    *found_o);

MEA_Status MEA_API_IsExist_PacketGen      (MEA_Unit_t                    unit_i,
                                           MEA_PacketGen_t               id_i,
                                           MEA_Bool                     *exist_o);

MEA_Status MEA_API_Delete_PacketGen_All_Entry(MEA_Unit_t                    unit_i);
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         Analyzer Defines                                     */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_ANALYZER_RMON_PORT MEA_PACKET_GEN_RMON_PORT
#define MEA_ANALYZER_CCM_MAX_MEG_DATA                48
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         Analyzer Typedefs                                    */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    MEA_Uint32    lastseqID; /* not counter */
    MEA_Counter_t num_Of_Bits_Error;  /* NOTE num of bit error after header according the pattern type (define in the OAM packets )*/
    MEA_Counter_t Bytes;
    MEA_Counter_t Pkts;
    
    MEA_Counter_t seq_ID_err;
    MEA_Counter_t seq_ID_reorder;
    MEA_Counter_t drop;
    
    MEA_Uint32 AVG_latency; /* not counter */
    MEA_Uint32 MAX_jitter;  /* not counter */
    MEA_Uint32 MIN_jitter;  /* not counter */
    
 
} MEA_Counters_Analyzer_dbt;

typedef struct {
    MEA_Uint32 activate         :1;
    MEA_Uint32 resetPrbs        :1;
    MEA_Uint32 enable_clearSeq  :1;
    MEA_Uint32 pad              :29;
    MEA_Uint32 value_nextSeq;
}MEA_Analyzer_configure_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         Analyzer APIs                                        */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/



MEA_Status MEA_API_Set_Analyzer(MEA_Unit_t                    unit_i,
                                MEA_Analyzer_t                id_i,
                                MEA_Analyzer_configure_dbt    *entry_pi);
MEA_Status MEA_API_Get_Analyzer(MEA_Unit_t                    unit_i,
                                MEA_Analyzer_t                id_i,
                                MEA_Analyzer_configure_dbt    *entry_po);

MEA_Status MEA_API_IsExist_Analyzer(MEA_Unit_t                    unit_i,
                                    MEA_Analyzer_t               id_i,
                                    MEA_Bool                      *exist_o);

MEA_Status MEA_API_Collect_Counters_ANALYZER(MEA_Unit_t             unit,
                                             MEA_Analyzer_t             id_i,
                                             MEA_Counters_Analyzer_dbt* entry);

MEA_Status MEA_API_Collect_Counters_ANALYZERs (MEA_Unit_t unit);


MEA_Status MEA_API_Get_Counters_ANALYZER(MEA_Unit_t             unit,
                                         MEA_Analyzer_t             id_i,
                                         MEA_Counters_Analyzer_dbt* entry);

MEA_Status MEA_API_Clear_Counters_ANALYZER(MEA_Unit_t    unit,
                                           MEA_Analyzer_t             id_i);

MEA_Status MEA_API_Clear_Counters_ANALYZERs(MEA_Unit_t                unit);



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         CCM Defines                                           */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         Analyzer Typedefs                                    */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    MEA_Counter_t NearEndLoss;
    MEA_Counter_t FarEndLoss;
    MEA_Uint32 TxFCfp;
    MEA_Uint32 RxFCbp;
    MEA_Uint32 TxFCbp;
    MEA_Uint32 RxFCfp;

} MEA_Counters_LM_dbt;

typedef struct {
    MEA_Counter_t Unexpected_MEP_ID;
    MEA_Counter_t Unexpected_MEG_ID;
    MEA_Counter_t Unexpected_period;
    MEA_Uint32    LastSequenc;
    MEA_Counter_t reorder;

} MEA_Counters_CCM_Defect_dbt;

typedef struct {
    MEA_Uint32 cir :11;
    MEA_Uint32 cbs :20;
    MEA_Uint32 fast_slow :1; /*1 fast*/
}MEA_PeriodCCM_dbt;

typedef enum  
{
    MEA_PERIOD_EVENT_3_3MS=1,
    MEA_PERIOD_EVENT_10MS=2,
    MEA_PERIOD_EVENT_100MS=3,
    MEA_PERIOD_EVENT_1S=4,
    MEA_PERIOD_EVENT_10S=5,
    MEA_PERIOD_EVENT_1MIN=6,
    MEA_PERIOD_EVENT_10MIN=7,
    MEA_PERIOD_EVENT_LAST,
}MEA_Period_event_t;




typedef struct {
      MEA_Uint8  MEG_data[MEA_ANALYZER_CCM_MAX_MEG_DATA]; 
      
      MEA_Uint32 expected_MEP_Id : 16;

      MEA_Uint32 period_event       : 8;    /* see MEA_Period_event_t*/
      MEA_Uint32 expected_period    : 3;
      MEA_Uint32 enable_clearSeq    : 1;   
      MEA_Uint32 mode_ccm_lmr       : 1; /*0-ccm 1-lmr*/
      MEA_Uint32 mask_group         : 1;
      MEA_Uint32 block              : 1;
      MEA_Uint32 pad                : 2;
      MEA_Uint32 value_nextSeq;

} MEA_CCM_Configure_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         CC Event and RDI Event APIs                           */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#define MEA_CC_RDI_MAX_GROUP  16
typedef struct {
    MEA_Bool Group[MEA_CC_RDI_MAX_GROUP];
}MEA_CC_RDI_Event_group_dbt;

/*NOTE : the Api  CC_Event and CC_Event
  need to read by scheduling Timer for xx msec */
MEA_Status MEA_API_Get_CC_Event(MEA_Unit_t    unit, MEA_CC_RDI_Event_group_dbt *entry ,MEA_Bool *exist);  /*All group*/
MEA_Status MEA_API_Get_RDI_Event(MEA_Unit_t    unit,MEA_CC_RDI_Event_group_dbt *entry ,MEA_Bool *exist);  /*All group*/


MEA_Status MEA_API_Get_CC_Event_GroupId(MEA_Unit_t unit,
                                        MEA_Uint32 groupId, 
                                        MEA_Uint32 *Event   /* mapping  32 steam */
                                        );  


MEA_Status MEA_API_Get_RDI_Event_GroupId(MEA_Unit_t    unit,
                                         MEA_Uint32 groupId, 
                                         MEA_Uint32 *Event   /* mapping  32 steam */
                                         );  






/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         CCM APIs                                              */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_Create_CCM_Entry(MEA_Unit_t                  unit_i,
                                    MEA_CCM_Configure_dbt      *entry_pi,
                                    MEA_CcmId_t                *id_io);

MEA_Status MEA_API_Delete_CCM_Entry(MEA_Unit_t                unit_i,
                                    MEA_CcmId_t               id_i);

MEA_Status MEA_API_Set_CCM_Entry(MEA_Unit_t                     unit_i,
                                 MEA_CcmId_t                    id_i,
                                 MEA_CCM_Configure_dbt         *entry_pi);

MEA_Status MEA_API_Get_CCM_Entry(MEA_Unit_t                     unit_i,
                                 MEA_CcmId_t                    id_i,
                                 MEA_CCM_Configure_dbt         *entry_po);

MEA_Status MEA_API_GetFirst_CCM_Entry(MEA_Unit_t                unit_i,
                                      MEA_CcmId_t              *id_o,
                                      MEA_CCM_Configure_dbt    *entry_po, /* Can be NULL */
                                      MEA_Bool                 *found_o);

MEA_Status MEA_API_GetNext_CCM_Entry(MEA_Unit_t                   unit_i,
                                     MEA_CcmId_t                 *id_io,
                                     MEA_CCM_Configure_dbt       *entry_po, /* Can be NULL */
                                     MEA_Bool                    *found_o);

MEA_Status MEA_API_IsExist_CCM(MEA_Unit_t               unit_i,
                               MEA_CcmId_t              id_i,
                               MEA_Bool                 *exist_o);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         CCM counter APIs                                       */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_CCM(MEA_Unit_t                        unit,
                                             MEA_CcmId_t                  id_i,
                                             MEA_Counters_CCM_Defect_dbt *entry);

MEA_Status MEA_API_Collect_Counters_CCMs (MEA_Unit_t unit);


MEA_Status MEA_API_Get_Counters_CCM(MEA_Unit_t                   unit,
                                    MEA_CcmId_t                  id_i,
                                    MEA_Counters_CCM_Defect_dbt *entry);

MEA_Status MEA_API_Clear_Counters_CCM(MEA_Unit_t              unit,
                                      MEA_CcmId_t             id_i);

MEA_Status MEA_API_Clear_Counters_CCMs(MEA_Unit_t              unit);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         LM counter APIs                                       */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Collect_Counters_LM(MEA_Unit_t               unit,
                                       MEA_LmId_t                id_i,
                                       MEA_Counters_LM_dbt    *entry);

MEA_Status MEA_API_Collect_Counters_LMs(MEA_Unit_t unit);


MEA_Status MEA_API_Get_Counters_LM(MEA_Unit_t             unit,
                                   MEA_LmId_t               id_i,
                                   MEA_Counters_LM_dbt   *entry);

MEA_Status MEA_API_Clear_Counters_LM(MEA_Unit_t           unit,
                                     MEA_LmId_t             id_i);

MEA_Status MEA_API_Clear_Counters_LMs(MEA_Unit_t                unit);


MEA_Status MEA_API_Clear_Counters_drv_LM(MEA_Unit_t           unit,
                                         MEA_LmId_t           id_i);




/************************************************************************/
/*   BFD  Entity                                                        */
/************************************************************************/

typedef struct {
    MEA_Bool      enable;
    MEA_Uint32 queue_Id; /* Id*/
}MEA_BFD_src_dbt;

/************************************************************************/
/* Set cluster LoopBack for echo packet                                 */
/************************************************************************/
MEA_Status MEA_API_Set_SrcPort_to_Cluster_loop(MEA_Unit_t unit, 
                                             MEA_Port_t port, 
                                              MEA_BFD_src_dbt    *entry);

MEA_Status MEA_API_Get_SrcPort_to_Cluster_loop(MEA_Unit_t unit,
                                             MEA_Port_t port,
                                             MEA_BFD_src_dbt    *entry);





typedef struct {
    MEA_Uint32 cir : 11;
    MEA_Uint32 cbs : 20;
    MEA_Uint32 fast_slow : 1; /*1 fast*/
}MEA_PeriodBFD_dbt;

typedef enum
{
    MEA_BFD_PERIOD_EVENT_3_3MS = 1,
    MEA_BFD_PERIOD_EVENT_10MS = 2,
    MEA_BFD_PERIOD_EVENT_100MS = 3,
    MEA_BFD_PERIOD_EVENT_1S = 4,
    MEA_BFD_PERIOD_EVENT_10S = 5,
    MEA_BFD_PERIOD_EVENT_1MIN = 6,
    MEA_BFD_PERIOD_EVENT_10MIN = 7,
    MEA_BFD_PERIOD_EVENT_LAST,
}MEA_PeriodBFD_event_t;







typedef struct{
    MEA_Counter_t Rx_packet;
    MEA_Counter_t Rx_Byte;

    MEA_Counter_t Tx_packet;
    MEA_Counter_t Tx_Byte;

}MEA_Counters_BFD_dbt;


MEA_Status MEA_API_Collect_Counters_BFD(MEA_Unit_t                        unit,
    MEA_BFDId_t                  id_i,
    MEA_Counters_BFD_dbt *entry);


MEA_Status MEA_API_Collect_Counters_BFDs(MEA_Unit_t unit_i);

MEA_Status MEA_API_Get_Counters_BFD(MEA_Unit_t                   unit,
                                    MEA_BFDId_t                  id_i,
                                    MEA_Counters_BFD_dbt *entry);

MEA_Status MEA_API_Clear_Counters_BFD(MEA_Unit_t              unit,
                                      MEA_BFDId_t             id_i);

MEA_Status MEA_API_Clear_Counters_BFDs(MEA_Unit_t              unit_i);



#define MEA_BFD_MAX_GROUP  1
typedef struct {
    MEA_Bool Group[MEA_BFD_MAX_GROUP];
}MEA_BFD_Event_group_dbt;

/*NOTE : the Api  BFD_Event 
need to read by scheduling Timer for xx msec */
MEA_Status MEA_API_Get_BFD_Event(MEA_Unit_t    unit, MEA_BFD_Event_group_dbt *entry, MEA_Bool *exist);  /*All group*/


MEA_Status MEA_API_Get_BFD_Event_GroupId(MEA_Unit_t unit,
                                        MEA_Uint32 groupId,
                                         MEA_Uint32 *Event   /* mapping  32 steam */);


typedef struct {
    
    MEA_Bool counter_en;

   
    MEA_Uint32 period_event : 8;    /* see MEA_PeriodBFD_event_t*/
    MEA_Uint32 mask_group   : 1;    /*0 - enable event  1- mask the event*/
    MEA_Uint32 block        : 1;   /**/
    MEA_Uint32 pad          : 2;
    

}MEA_BFD_Configure_dbt;


MEA_Status MEA_API_Create_BFD_Entry(MEA_Unit_t                  unit_i,
    MEA_BFD_Configure_dbt      *entry_pi,
    MEA_BFDId_t                *id_io);


MEA_Status MEA_API_Set_BFD_Entry(MEA_Unit_t                     unit_i,
    MEA_BFDId_t                    id_i,
    MEA_BFD_Configure_dbt         *entry_pi);


MEA_Status MEA_API_Delete_BFD_Entry(MEA_Unit_t                unit_i,
    MEA_BFDId_t               id_i);

MEA_Status MEA_API_Get_BFD_Entry(MEA_Unit_t                     unit_i,
    MEA_BFDId_t                    id_i,
    MEA_BFD_Configure_dbt         *entry_po);

MEA_Status MEA_API_GetFirst_BFD_Entry(MEA_Unit_t                unit_i,
    MEA_BFDId_t              *id_o,
    MEA_BFD_Configure_dbt    *entry_po, /* Can be NULL */
    MEA_Bool                 *found_o);

MEA_Status MEA_API_GetNext_BFD_Entry(MEA_Unit_t                   unit_i,
    MEA_BFDId_t                 *id_io,
    MEA_BFD_Configure_dbt       *entry_po, /* Can be NULL */
    MEA_Bool                    *found_o);

MEA_Status MEA_API_IsExist_BFD(MEA_Unit_t               unit_i,
    MEA_BFDId_t              id_i,
    MEA_Bool                 *exist_o);



MEA_Status MEA_API_IsExist_BFD(MEA_Unit_t               unit_i,
                               MEA_BFDId_t              id_i,
                               MEA_Bool                 *exist_o);


/******************************************************************************************************/
/************************************************************************/
/* NVGRE                                                                     */
/************************************************************************/

MEA_Bool   MEA_API_NVGRE_Port_Remove_Header_IsValid(MEA_Unit_t unit, MEA_Port_t port);
MEA_Status MEA_API_NVGRE_Set_Port_Remove_Header(MEA_Unit_t unit, MEA_Port_t port, MEA_Bool enable);
MEA_Status MEA_API_NVGRE_Get_Port_Remove_Header(MEA_Unit_t unit, MEA_Port_t port, MEA_Bool *enable);



/************************************************************************/
/*  IPSec                                                               */
/************************************************************************/
typedef enum
{
    /*-------------*/
    MEA_EDITING_IPSec_Security_INTEG_HMC_MD5_96                     = 1,
    MEA_EDITING_IPSec_Security_INTEG_HMC_SHA1_96                    = 2,
    MEA_EDITING_IPSec_Security_INTEG_HMC_SHA256_128                 = 3,
    MEA_EDITING_IPSec_Security_INTEG_HMC_SHA384_192                 = 4,
    MEA_EDITING_IPSec_Security_INTEG_HMC_SHA512_256                 = 5,
    MEA_EDITING_IPSec_Security_INTEG_AES_XCBC_MAC_96                = 6,
    MEA_EDITING_IPSec_Security_INTEG_AES_CMAC_96                    = 7,
    MEA_EDITING_IPSec_Security_INTEG_AES_128_GMAC_128               = 0xCB,
    MEA_EDITING_IPSec_Security_INTEG_AES_192_GMAC_128               = 0xDB,
    MEA_EDITING_IPSec_Security_INTEG_AES_256_GMAC_128               = 0xEB,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_3DES_CBC                   = 0x10,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CBC                = 0x40,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CBC                = 0x50,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CBC                = 0x60,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR                = 0x80,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR                = 0x90,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR                = 0xA0,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_3DES_CBC_HMAC_MD5_96                 = 0x11,
    MEA_EDITING_IPSec_Security_3DES_CBC_HMAC_SHA1_96                = 0x12,
    MEA_EDITING_IPSec_Security_3DES_CBC_HMAC_SHA256_128             = 0x13,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_MD5_96              = 0x41,
    MEA_EDITING_IPSec_Security_AES_192_CBC_HMAC_MD5_96              = 0x51,
    MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_MD5_96              = 0x61,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_SHA1_96             = 0x42,
    MEA_EDITING_IPSec_Security_AES_192_CBC_HMAC_SHA1_96             = 0x52,
    MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_SHA1_96             = 0x62,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_SHA256_128          = 0x43,
    MEA_EDITING_IPSec_Security_AES_192_CBC_HMAC_SHA256_128          = 0x53,
    MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_SHA256_128          = 0x63,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_AES_128_CBC_AES_XCBC_MAC_96          = 0x46,
    MEA_EDITING_IPSec_Security_AES_192_CBC_AES_XCBC_MAC_96          = 0x56,
    MEA_EDITING_IPSec_Security_AES_256_CBC_AES_XCBC_MAC_96          = 0x66,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_MD5_96   = 0x81,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_HMAC_MD5_96    = 0x91,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_MD5_96    = 0xa1,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_SHA1_96   = 0x82,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_HMAC_SHA1_96   = 0x92,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_SHA1_96   = 0xA2,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_SHA256_128 = 0x83,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_HMAC_SHA256_128 = 0x93,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_SHA256_128 = 0xA3,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_AES_XCBC_MAC_96 = 0x86,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_AES_XCBC_MAC_96 = 0x96,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_AES_XCBC_MAC_96 = 0xA6,
    /*----------------------------------------------------------*/
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_GCM_64 = 0xc8,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_GCM_96 = 0xc9,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_GCM_128 = 0xCA,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_GCM_64 = 0xd8,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_GCM_96 = 0xd9,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_GCM_128 = 0xda,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_GCM_64 = 0xe8,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_GCM_96 = 0xe9,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_GCM_128 = 0xEA,

    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CCM_64 = 0xf8,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CCM_96 = 0xf9,
    MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CCM_128 = 0xfa,
    /*----------------------------------------------------------*/
    MEA_EDITING_IPSec_Security_CONFIDENT_LAST            =0xff
}MEA_IPSec_Security_Type_t;




#define MEA_MAX_IPSECE_INTEG_KEY_WORD 8
#define MEA_MAX_IPSECE_INTEG_IV_WORD 8 

#define MEA_MAX_IPSECE_CONFID_KEY_WORD 8
#define MEA_MAX_IPSECE_CONFID_IV_WORD 8 

typedef struct {
     
    MEA_Uint32  Integrity_key[MEA_MAX_IPSECE_INTEG_KEY_WORD]; 
    MEA_Uint32  Integrity_IV [MEA_MAX_IPSECE_INTEG_IV_WORD];
    MEA_Uint32  Confident_key[MEA_MAX_IPSECE_CONFID_KEY_WORD];
    MEA_Uint32  Confident_IV [MEA_MAX_IPSECE_CONFID_IV_WORD];
    MEA_Uint32  SPI;    /*ESP Security Parameters*/

    MEA_Uint32  ESN_en                      : 1;      /*Extended Sequence Number */
    MEA_Uint32 TFC_padding_en               : 1;
    MEA_IPSec_Security_Type_t security_type ;

}MEA_IPSec_dbt;


MEA_Status MEA_API_IPSecESP_Create_Entry(MEA_Unit_t       unit_i,
                                         MEA_IPSec_dbt    *entry_p, 
                                         MEA_Uint32       *id_io);


MEA_Status MEA_API_IPSecESP_Delete_Entry(MEA_Unit_t                  unit_i,
    MEA_Uint32                   id_i);

MEA_Status MEA_API_IPSecESP_Get_Entry(MEA_Unit_t         unit_i,
    MEA_Uint32           id_i,
    MEA_IPSec_dbt    *entry_po);

MEA_Status MEA_API_IPSecESP_GetFirst_Entry(MEA_Unit_t              unit_i,
    MEA_Uint32               *id_o,
    MEA_IPSec_dbt        *entry_po,
    MEA_Bool                 *found_o);

MEA_Status MEA_API_IPSecESP_GetNext_Entry(MEA_Unit_t            unit_i,
    MEA_Uint32              *id_io,
    MEA_IPSec_dbt       *entry_po,
    MEA_Bool                *found_o);



MEA_Status MEA_API_IPSecESP_IsExist(MEA_Unit_t                unit_i,
    MEA_Uint32              id_i,
    MEA_Bool                *exist);





/************************************************************************/
/*   TDM  Entity                                                        */
/************************************************************************/
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         TDM  Typedefs                                         */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef  enum{
MEA_TDM_CES_ENCP_TYPE_UDP=0,
MEA_TDM_CES_ENCP_TYPE_VLAN=1,
MEA_TDM_CES_ENCP_TYPE_MPLS=2,
MEA_TDM_CES_ENCP_TYPE_IP
}MEA_TDM_CES_ENCP_Type_te;

#define MEA_TDM_MAX_SPE_FOR_SBI             12
#define MEA_TDM_MAX_SPE_FOR_PCM             8
#define MEA_TDM_MAX_SPE_FOR_PCM_MUX         6

#define MEA_TDM_MAX_CHANNEL_FOR_SBI         32
#define MEA_TDM_MAX_CHANNEL_FOR_PCM         1
#define MEA_TDM_MAX_CHANNEL_FOR_PCM_MUX     8

#define MEA_TDM_NUM_OF_CLK_DOMAIN           4

typedef  enum{
    MEA_TDM_INTERFACE_NONE=0,
    MEA_TDM_INTERFACE_SBI=1,
    MEA_TDM_INTERFACE_PCM=2,
    MEA_TDM_INTERFACE_PCM_MUX=3,
    MEA_TDM_INTERFACE_LAST=4
}MEA_TDM_Interface_te;


typedef struct{
    MEA_Uint32 pad     :7;
    MEA_Uint32 repeat  :13; // time 1ms till 8000ms 
    MEA_Uint32 pattern :8; //16
    MEA_Uint32 len     :4;   // 2 4 8
}MEA_TDM_Loopback_Code_te; 

typedef struct{
MEA_TDM_Loopback_Code_te set_loopback;
MEA_TDM_Loopback_Code_te cancel_loopback;
/*private*/
MEA_Uint16  private_loopback_set_pattern;
MEA_Uint16  private_loopback_cancel_pattern;

}MEA_TDM_Prof_Loopback_Code_dbt; 


typedef struct{
    MEA_Uint8           idle;

}MEA_TDM_Prof_Idel_dbt;         

/* for CES */
typedef struct{
    MEA_Uint32  pad1        :16;
    MEA_Uint32  threshold   :16;
    MEA_Uint32  pad         :12;
    MEA_Uint32  dec         :10;
    MEA_Uint32  inc         :10;
}MEA_TDM_Prof_Resync_dbt;         



typedef struct{
    
    MEA_Uint32  entry_in      :16;
    MEA_Uint32  entry_out     :16;
}MEA_TDM_Prof_Loss_dbt;            

typedef struct{
    MEA_Uint32  entry_in      :16;
    MEA_Uint32  entry_out     :16;
}MEA_TDM_Prof_Ais_dbt;


typedef struct {
    MEA_Uint32 pad         :21;
    MEA_Uint32 active      :1;   
    MEA_Uint32 prof_lbk    :2;   
    MEA_Uint32 prof_idle   :3;  
    MEA_Uint32 clk_domain  :3; // only 0:3  (4) loopback for PCM
    MEA_Uint32 ces_satop   :1; // 0=ces  1=satop
    MEA_Uint32 t1_mode     :1; /*bit0 */  /* 1-193bit 0-25byte      */ 

}MEA_TDM_SPE_Channel_info_dbt;

typedef struct {
    MEA_Uint32 spe_enabel :1;
    MEA_Uint32 type_e1_t1 :1; //E1=0 T1=1
    MEA_Uint32 pad        :30; 
}MEA_TDM_SPE_info_dbt;

 



typedef struct {
    MEA_Uint32   interfaceId : 2;
    MEA_Uint32   spe_num     : 4;
    MEA_Uint32   channelId   : 5;
    MEA_Uint32   pad         : 21;
}MEA_Interface_Key_dbt;






typedef struct {
	MEA_Bool sbi_loopback_Enable;
    MEA_Bool etu_mode_Enable;
    MEA_Bool  e1_t1_mode;  // 0-e1 1-T1
    MEA_Bool  filter_current_jb; //for T1
    MEA_Bool   numof_ces_anable;
    MEA_Uint16 numof_ces;

}MEA_drv_TDM_global_dbt;

MEA_Status MEA_API_TDM_Set_Global(MEA_Unit_t                   unit_i,
                                  MEA_drv_TDM_global_dbt      *entry);

MEA_Status MEA_API_TDM_Get_Global(MEA_Unit_t                   unit_i,
                                 MEA_drv_TDM_global_dbt      *entry);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    MEA_Uint32   clock_is_T1 : 4; //0-clock sync 1-make adjustment
    MEA_Uint32   clock_adjust_enable : 4;
    MEA_Uint32   clock_mux_mode :1;  //0- received the 4xE1/T1 Clocks form the 16xE1/T1        
                                    // 1- received the 4xE1/T1 Clocks from the CLK_MUX_MODULE
    MEA_Uint32   pad       :23;
    MEA_Uint32   clk_sel[MEA_TDM_NUM_OF_CLK_DOMAIN];  // 0--15

}MEA_TDM_Interface_Pcm_Clock_dbt;


MEA_Status MEA_API_TDM_Set_Pcm_Clock(MEA_Unit_t                   unit_i,
                                MEA_TDM_Interface_Pcm_Clock_dbt *entry_pi);

MEA_Status MEA_API_TDM_Get_Pcm_Clock(MEA_Unit_t                   unit_i,
                                MEA_TDM_Interface_Pcm_Clock_dbt *entry_o);


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                                                                           */
/*                         TDM  prof  Api's                                  */
/*                                                                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

MEA_Status MEA_API_TDM_Create_LoopBack_Prof(MEA_Unit_t                unit_i,
                                  MEA_TDM_Prof_Loopback_Code_dbt     *entry_pi,
                                  MEA_Uint16                         *id_o);

MEA_Status MEA_API_TDM_Set_LoopBack_Prof(MEA_Unit_t                  unit_i,
                              MEA_Uint16                             id_i,
                              MEA_TDM_Prof_Loopback_Code_dbt        *entry_pi);

MEA_Status MEA_API_TDM_Delete_LoopBack_Prof(MEA_Unit_t              unit_i,
                                   MEA_Uint16                       id_i);

MEA_Status MEA_API_TDM_Get_LoopBack_Prof(MEA_Unit_t                  unit_i,
                              MEA_Uint16                             id_i,
                              MEA_TDM_Prof_Loopback_Code_dbt        *entry_po);

MEA_Status MEA_API_TDM_GetFirst_LoopBack_Prof(MEA_Unit_t             unit_i,
                                MEA_Uint16                          *id_o,
                                MEA_TDM_Prof_Loopback_Code_dbt      *entry_po,
                                MEA_Bool                            *found_o);

MEA_Status MEA_API_TDM_GetNext_LoopBack_Prof(MEA_Unit_t              unit_i,
                                 MEA_Uint16                         *id_io,
                                 MEA_TDM_Prof_Loopback_Code_dbt     *entry_po,
                                 MEA_Bool                           *found_o);

MEA_Status MEA_API_TDM_LoopBack_Prof_IsExist(MEA_Unit_t                unit_i,
                                         MEA_Uint16                    id_i,
                                         MEA_Bool                     *exist);

MEA_Status MEA_API_TDM_Delete_all_LoopBack_Prof(MEA_Unit_t             unit_i);

/******************************************************************************/
/*   API Idle_Prof                                                            */
/******************************************************************************/

MEA_Status MEA_API_TDM_Create_Idle_Prof(MEA_Unit_t                    unit_i,
                                        MEA_TDM_Prof_Idel_dbt        *entry_pi,
                                        MEA_Uint16                   *id_o);

MEA_Status MEA_API_TDM_Set_Idle_Prof(MEA_Unit_t                      unit_i,
                                     MEA_Uint16                      id_i,
                                     MEA_TDM_Prof_Idel_dbt          *entry_pi);


MEA_Status MEA_API_TDM_Delete_Idle_Prof(MEA_Unit_t                  unit_i,
                                        MEA_Uint16                   id_i);

MEA_Status MEA_API_TDM_Get_Idle_Prof(MEA_Unit_t                  unit_i,
                                     MEA_Uint16                  id_i,
                                     MEA_TDM_Prof_Idel_dbt      *entry_po);

MEA_Status MEA_API_TDM_GetFirst_Idle_Prof(MEA_Unit_t                unit_i,
                                          MEA_Uint16               *id_o,
                                          MEA_TDM_Prof_Idel_dbt    *entry_po,
                                          MEA_Bool                 *found_o);

MEA_Status MEA_API_TDM_GetNext_Idle_Prof(MEA_Unit_t                   unit_i,
                                         MEA_Uint16                  *id_io,
                                         MEA_TDM_Prof_Idel_dbt       *entry_po,
                                         MEA_Bool                    *found_o);

MEA_Status MEA_API_TDM_Idle_Prof_IsExist(MEA_Unit_t                  unit_i,
                                         MEA_Uint16                  id_i,
                                         MEA_Bool                    *exist);

MEA_Status MEA_API_TDM_Delete_all_Idle_Prof(MEA_Unit_t                  unit_i);

/******************************************************************************/
/*   API Resync_Prof                                                          */
/******************************************************************************/
MEA_Status MEA_API_TDM_Create_Resync_Prof(MEA_Unit_t                 unit_i,
                                      MEA_TDM_Prof_Resync_dbt        *entry_pi,
                                      MEA_Uint16                     *id_o);

MEA_Status MEA_API_TDM_Set_Resync_Prof(MEA_Unit_t                   unit_i,
                                   MEA_Uint16                       id_i,
                                   MEA_TDM_Prof_Resync_dbt         *entry_pi);

MEA_Status MEA_API_TDM_Delete_Resync_Prof(MEA_Unit_t                  unit_i,
                                          MEA_Uint16                  id_i);

MEA_Status MEA_API_TDM_Get_Resync_Prof(MEA_Unit_t                 unit_i,
                                       MEA_Uint16                  id_i,
                                       MEA_TDM_Prof_Resync_dbt    *entry_po);

MEA_Status MEA_API_TDM_GetFirst_Resync_Prof(MEA_Unit_t                 unit_i,
                                            MEA_Uint16                *id_o,
                                            MEA_TDM_Prof_Resync_dbt  *entry_po, 
                                            MEA_Bool                 *found_o);

MEA_Status MEA_API_TDM_GetNext_Resync_Prof(MEA_Unit_t                 unit_i,
                                         MEA_Uint16                  *id_io,
                                         MEA_TDM_Prof_Resync_dbt     *entry_po, 
                                         MEA_Bool                    *found_o);

MEA_Status MEA_API_TDM_Resync_Prof_IsExist(MEA_Unit_t                 unit_i,
                                           MEA_Uint16                 id_i,
                                           MEA_Bool                  *exist);

MEA_Status MEA_API_TDM_Delete_all_Resync_Prof(MEA_Unit_t               unit_i);


/******************************************************************************/
/*   API Loss_Prof                                                            */
/******************************************************************************/

MEA_Status MEA_API_TDM_Create_Loss_Prof(MEA_Unit_t                   unit_i,
                                        MEA_TDM_Prof_Loss_dbt       *entry_pi,
                                        MEA_Uint16                  *id_o);

MEA_Status MEA_API_TDM_Set_Loss_Prof(MEA_Unit_t                     unit_i,
                                     MEA_Uint16                     id_i,
                                     MEA_TDM_Prof_Loss_dbt         *entry_pi);

MEA_Status MEA_API_TDM_Delete_Loss_Prof(MEA_Unit_t                  unit_i,
                                          MEA_Uint16                id_i);

MEA_Status MEA_API_TDM_Get_Loss_Prof(MEA_Unit_t                     unit_i,
                                       MEA_Uint16                   id_i,
                                       MEA_TDM_Prof_Loss_dbt       *entry_po);

MEA_Status MEA_API_TDM_GetFirst_Loss_Prof(MEA_Unit_t                unit_i,
                                            MEA_Uint16             *id_o,
                                            MEA_TDM_Prof_Loss_dbt  *entry_po,
                                            MEA_Bool               *found_o);

MEA_Status MEA_API_TDM_GetNext_Loss_Prof(MEA_Unit_t                 unit_i,
                                         MEA_Uint16                *id_io,
                                         MEA_TDM_Prof_Loss_dbt     *entry_po, 
                                         MEA_Bool                  *found_o);

MEA_Status MEA_API_TDM_Loss_Prof_IsExist(MEA_Unit_t                  unit_i,
                                         MEA_Uint16                  id_i,
                                         MEA_Bool                    *exist);
MEA_Status MEA_API_TDM_Delete_all_Loss_Prof(MEA_Unit_t               unit_i);

/*****************************************************************************/
/*   API AIS_Prof                                                            */
/*****************************************************************************/
MEA_Status MEA_API_TDM_Create_AIS_Prof(MEA_Unit_t                unit_i,
                                       MEA_TDM_Prof_Ais_dbt     *entry_pi,
                                       MEA_Uint16               *id_io);

MEA_Status MEA_API_TDM_Set_AIS_Prof(MEA_Unit_t                 unit_i,
                                MEA_Uint16                     id_i,
                                MEA_TDM_Prof_Ais_dbt            *entry_pi);

MEA_Status MEA_API_TDM_Delete_AIS_Prof(MEA_Unit_t                unit_i,
                                       MEA_Uint16                id_i);

MEA_Status MEA_API_TDM_Get_AIS_Prof(MEA_Unit_t                      unit_i,
                                MEA_Uint16                          id_i,
                                MEA_TDM_Prof_Ais_dbt             *entry_po);

MEA_Status MEA_API_TDM_GetFirst_AIS_Prof(MEA_Unit_t                unit_i,
                                     MEA_Uint16                   *id_o,
                                     MEA_TDM_Prof_Ais_dbt         *entry_po, 
                                     MEA_Bool                     *found_o);

MEA_Status MEA_API_TDM_GetNext_AIS_Prof(MEA_Unit_t                     unit_i,
                                        MEA_Uint16                     *id_io,
                                        MEA_TDM_Prof_Ais_dbt           *entry_po, 
                                        MEA_Bool                       *found_o);

MEA_Status MEA_API_TDM_AIS_Prof_IsExist(MEA_Unit_t                      unit_i,
                                        MEA_Uint16                       id_i,
                                        MEA_Bool                        *exist);

MEA_Status MEA_API_TDM_Delete_all_AIS_Prof(MEA_Unit_t                      unit_i);
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         Interface_SPE  Api's                                  */
/*                         Interface_Channel                                     */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_TDM_Interface_SPE_Set(MEA_Unit_t                unit_i,
                                         MEA_Interface_Key_dbt     *key,
                                         MEA_TDM_SPE_info_dbt      *entry);

MEA_Status MEA_API_TDM_Interface_SPE_Get(MEA_Unit_t             unit_i,
                                         MEA_Interface_Key_dbt  *key, 
                                         MEA_TDM_SPE_info_dbt   *entry);

MEA_Status MEA_API_TDM_Interface_SPE_maxOf_spe(MEA_Unit_t      unit_i,
                                               MEA_Uint8       interfaceId,
                                               MEA_Uint8      *max_valu);

MEA_Status MEA_API_TDM_Interface_Channel_Set(MEA_Unit_t               unit_i,
                                        MEA_Interface_Key_dbt        *key,
                                        MEA_TDM_SPE_Channel_info_dbt *entry);

MEA_Status MEA_API_TDM_Interface_Channel_Get(MEA_Unit_t            unit_i,
                                             MEA_Interface_Key_dbt     *key, 
                                             MEA_TDM_SPE_Channel_info_dbt *entry);
MEA_Status MEA_API_TDM_Interface_Channel_maxOf_channel(MEA_Unit_t             unit_i,
                                                       MEA_Uint8              interfaceId,
                                                       MEA_Uint8  *max_valu);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         CES Typedefs                                          */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/



typedef struct{

    MEA_Interface_Key_dbt key;
    
    MEA_Uint32 AdminCes      : 1;
    MEA_Uint32 jb_tdm_frame  : 9;  // 125uSec to 64mSec
    MEA_Uint32 pag1          : 14; 
    MEA_Uint32 mode_193      : 1;
    MEA_Uint32 N             : 6;  // num of TS


    MEA_Uint32 M             : 6;  //num_of_tdm_frames;  
    MEA_Uint32 remove_byte   : 4;
    MEA_Uint32 prof_loss     : 5; 
    MEA_Uint32 prof_resync   : 5;
    MEA_Uint32 prof_idle     : 5;
    MEA_Uint32 prof_Ais      : 5;
    MEA_Uint32 pag2          : 5;
    MEA_Uint32 Time_slot;   // valid bit for each ts
    
    
    
}MEA_TDM_CES_Configure_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         TDM_CES  Api's                                        */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

MEA_Status MEA_API_TDM_CES_Create_Entry(MEA_Unit_t                unit_i,
                                        MEA_TDM_CES_Configure_dbt *entry,
                                        MEA_TdmCesId_t            *TdmCesId);

MEA_Status MEA_API_TDM_CES_Set_Entry(MEA_Unit_t                unit_i,
                                     MEA_TdmCesId_t TdmCesId ,
                                     MEA_TDM_CES_Configure_dbt *entry);

MEA_Status MEA_API_TDM_CES_Delete_Entry(MEA_Unit_t                unit_i,
                                        MEA_TdmCesId_t TdmCesId);

MEA_Status MEA_API_TDM_CES_Get_Entry(MEA_Unit_t                unit_i,
                                     MEA_TdmCesId_t TdmCesId,
                                     MEA_TDM_CES_Configure_dbt *entry);

MEA_Status MEA_API_TDM_CES_GetFirst_Entry(MEA_Unit_t                unit_i,
                                  MEA_TdmCesId_t             *id_o,
                                  MEA_TDM_CES_Configure_dbt  *entry_po, /* Can't be NULL */
                                  MEA_Bool                   *found_o);

MEA_Status MEA_API_TDM_CES_GetNext_Entry(MEA_Unit_t                   unit_i,
                                         MEA_TdmCesId_t              *id_io,
                                         MEA_TDM_CES_Configure_dbt   *entry_po, /* Can't be NULL */
                                         MEA_Bool                    *found_o);
MEA_Status MEA_API_TDM_CES_IsExist(MEA_Unit_t                      unit_i,
                                   MEA_TdmCesId_t                  id_i,
                                   MEA_Bool                        *exist);

MEA_Bool  MEA_API_Get_IsCESValid(MEA_Unit_t     unit_i ,MEA_Uint16 id_i,MEA_Bool silent);

MEA_Status MEA_API_TDM_Delete_all_CES_Entry(MEA_Unit_t                unit_i);

MEA_Status MEA_API_TDM_CES_Resync_Reset_Set(MEA_Unit_t                unit_i,
                                            MEA_TdmCesId_t            TdmCesId);

/************************************************************************/
/*            TDM_PM's                                                    */
/************************************************************************/
/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA Counters TDM                                           */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct{
    MEA_Counter_t under_run;
    MEA_Uint32 fifo_min;
    MEA_Uint32 fifo_max;
}MEA_Counters_TDM_EgressFifo_dbt;


MEA_Status MEA_API_IsExist_TdmId_EgressFifo(MEA_Unit_t                 unit,
                                            MEA_TdmCesId_t             Id_i,
                                            MEA_Bool                  *exist);

MEA_Status MEA_API_Collect_Counters_TDM_EgressFifoS(MEA_Unit_t                unit);

MEA_Status MEA_API_Collect_Counters_TDM_EgressFifo(MEA_Unit_t                    unit,
                                        MEA_TdmCesId_t                id_i,
                                        MEA_Counters_TDM_EgressFifo_dbt*         entry);

MEA_Status MEA_API_Get_Counters_TDM_EgressFifo(MEA_Unit_t                    unit,
                                    MEA_TdmCesId_t                id_i,
                                    MEA_Counters_TDM_EgressFifo_dbt*          entry);

MEA_Status MEA_API_Clear_Counters_TDM_EgressFifoS(MEA_Unit_t                    unit);

MEA_Status MEA_API_Clear_Counters_TDM_EgressFifo(MEA_Unit_t                    unit,
                                      MEA_TdmCesId_t                id_i);




typedef struct{           
 
    /* TDM ->>> ETH  */

    MEA_Counter_t RX_Reorder_packet;            // 1

    MEA_Counter_t RX_seq_out_of_bound;          // 3
    MEA_Counter_t RX_seq_duplication;           // 4
    MEA_Counter_t RX_jitter_buffer_full_drop;   
    MEA_Counter_t RX_etu_drop;              // 5


    /* ETH-> TDM */
    MEA_Counter_t TX_Idle_packets;           //2
    MEA_Counter_t TX_Resync_threshold;       // 7
    MEA_Counter_t TX_drop_other ;            // 8
    
    MEA_Uint32 current_jb;

    MEA_Uint32 awr_round;

    MEA_Uint32 RX_AVG_latency; /* not counter */
    MEA_Uint32 RX_MAX_jitter;  /* not counter */
    MEA_Uint32 RX_MIN_jitter;  /* not counter */

} MEA_Counters_TDM_dbt;

MEA_Status MEA_API_IsExist_TdmId(MEA_Unit_t                 unit,
                                 MEA_TdmCesId_t             Id_i,
                                 MEA_Bool                  *exist);

MEA_Status MEA_API_Collect_Counters_TDMs(MEA_Unit_t                unit);

MEA_Status MEA_API_Collect_Counters_TDM(MEA_Unit_t                    unit,
                                        MEA_TdmCesId_t                id_i,
                                        MEA_Counters_TDM_dbt*         entry);

MEA_Status MEA_API_Get_Counters_TDM(MEA_Unit_t                    unit,
                                    MEA_TdmCesId_t                id_i,
                                    MEA_Counters_TDM_dbt*          entry);

MEA_Status MEA_API_Clear_Counters_TDMs(MEA_Unit_t                    unit);

MEA_Status MEA_API_Clear_Counters_TDM(MEA_Unit_t                    unit,
                                      MEA_TdmCesId_t                id_i);


/*-------------------------------------------------------------------------------*/
/*                    MEA Counters TDM       Rmon                                */
/*-------------------------------------------------------------------------------*/
typedef struct{           

MEA_Counter_t     tdm_rx_pkt;
MEA_Counter_t     tdm_rx_byte;
MEA_uint64        rate_rx;
MEA_Counter_t     tdm_tx_pkt;
MEA_Counter_t     tdm_tx_byte;
MEA_uint64        rate_tx;

} MEA_Counters_TDM_Rmon_dbt;

MEA_Status MEA_API_IsExist_TdmId_RMON_TDM_Counter(MEA_Unit_t             unit,
                                                  MEA_TdmCesId_t         Id_i,
                                                  MEA_Bool              *exist);

MEA_Status MEA_API_Collect_Counters_RMON_TDMs(MEA_Unit_t                unit);

MEA_Status MEA_API_Collect_Counters_RMON_TDM(MEA_Unit_t                   unit,
                                             MEA_TdmCesId_t               id_i,
                                             MEA_Counters_TDM_Rmon_dbt*   entry);

MEA_Status MEA_API_Get_Counters_RMON_TDM(MEA_Unit_t                    unit,
                                         MEA_TdmCesId_t                id_i,
                                         MEA_Counters_TDM_Rmon_dbt*    entry);

MEA_Status MEA_API_Clear_Counters_RMON_TDMs(MEA_Unit_t                 unit);

MEA_Status MEA_API_Clear_Counters_RMON_TDM(MEA_Unit_t                  unit,
                                           MEA_TdmCesId_t              id_i);

#define MEA_TDM_MAX_GROUP_EVENT  64
#define MEA_TDM_MAX_SUB_GROUP_EVENT  32

#define MEA_TDM_CHANNEL_MAX_GROUP_EVENT  16

typedef struct {
    MEA_Uint32 Group0;
    MEA_Uint32 Group1;
}MEA_TDM_Event_group_dbt;

typedef struct {
    MEA_Uint8 SubGruopEvent[MEA_TDM_MAX_SUB_GROUP_EVENT];
}MEA_TDM_Event_Subgroup_dbt;

typedef struct {
  MEA_Interface_Key_dbt key;
  MEA_Uint8            eventSubInfo;
} MEA_TDM_Event_ChannelSubGroupInfo_dbt;

typedef struct {
    MEA_Bool                              SubEvent;
    MEA_TDM_Event_ChannelSubGroupInfo_dbt SubGruopEvent[MEA_TDM_MAX_SUB_GROUP_EVENT];
}MEA_TDM_Event_SubgroupChannel_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA TDM Channel EVENT                                      */
/*                                                                               */
/*-------------------------------------------------------------------------------*/

typedef struct  
{
    MEA_Uint8 pad        :6;
    MEA_Uint8 LBK_cancel :1;
    MEA_Uint8 LBK_start  :1;
}MEA_TDM_Channel_Event_dbt;

MEA_Status MEA_API_Get_TDM_Channel_EventGroup(MEA_Unit_t    unit, 
                                               MEA_TDM_Event_group_dbt *entry ,
                                               MEA_Bool *exist);  
// subgroup 4 ,8,12 not Active (spe 0 not valid)
MEA_Status MEA_API_Get_TDM_Channel_Event_SubGroup(MEA_Unit_t unit,
                                                 MEA_Uint32 groupId, 
                                                 MEA_TDM_Event_SubgroupChannel_dbt *entry);

MEA_Status MEA_API_Get_TDM_Channel_Event(MEA_Unit_t unit,
                                         MEA_Interface_Key_dbt     *key, 
                                         MEA_TDM_Channel_Event_dbt *entry);

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                    MEA TDM Ces EVENT                                          */
/*                                                                               */
/*-------------------------------------------------------------------------------*/



typedef struct  
{
    MEA_Uint8  pad                          :1;
    MEA_Uint8  Stray_packets                :1; /*not support*/
    MEA_Uint8  Malformed_packets            :1; /*not support*/
    MEA_Uint8  Excessive_packet_loss_rate   :1; /*not support*/
    MEA_Uint8  Buffer_overrun               :1; /*not support*/
    MEA_Uint8  LOSS                         :1;
    MEA_Uint8  Lbit                         :1;
    MEA_Uint8  Remote_packet_loss           :1; 

}MEA_TDM_CES_Event_dbt;




/*NOTE : the Api  
need to read by scheduling Timer for xx msec */
MEA_Status MEA_API_Get_TDM_CES_EventGroup(MEA_Unit_t    unit, MEA_TDM_Event_group_dbt *entry ,MEA_Bool *exist);  /*All group*/


MEA_Status MEA_API_Get_TDM_CES_Event_SubGroup
									   (MEA_Unit_t unit,
                                        MEA_Uint32 groupId, 
                                        MEA_TDM_Event_Subgroup_dbt *entry);  


MEA_Status MEA_API_Get_TDM_CES_Event(MEA_Unit_t unit,
                                     MEA_TdmCesId_t Id, 
                                     MEA_TDM_CES_Event_dbt *entry);




/************************************************************************/
/*               Standard bonding                                       */
/************************************************************************/

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         BondingTx Defines                                     */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         BondingTx Typedefs                                    */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    MEA_Uint32 valid             :1;
    MEA_Uint32 num_of_port       :8;
    MEA_Uint32 pad              :23;
    MEA_OutPorts_Entry_dbt Multiple_Port; 
}MEA_BondingTx_dbt;

typedef struct {


    MEA_Counter_t RX_seq_out_of_bound;          
    MEA_Counter_t RX_seq_duplication;           
    MEA_Counter_t RX_size_drop;
    MEA_Counter_t RX_drop_not_sync ;   
    
    MEA_Counter_t TX_drop;            

}MEA_BondingCount_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                         BondingTx  APIs                                       */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


MEA_Status MEA_API_BondingTx_Create_Entry(MEA_Unit_t             unit_i,
                                          MEA_BondingTx_dbt     *entry_pi,
                                          MEA_Uint32            *id_io);


MEA_Status MEA_API_BondingTx_Set_Entry(MEA_Unit_t              unit_i,
                                       MEA_Uint32           id_i,
                                       MEA_BondingTx_dbt   *entry_pi);

MEA_Status MEA_API_BondingTx_Delete_Entry(MEA_Unit_t                  unit_i,
                                          MEA_Uint32                   id_i);

MEA_Status MEA_API_BondingTx_Get_Entry(MEA_Unit_t         unit_i,
                                       MEA_Uint32           id_i,
                                       MEA_BondingTx_dbt    *entry_po);

MEA_Status MEA_API_BondingTx_GetFirst_Entry(MEA_Unit_t              unit_i,
                                            MEA_Uint32               *id_o,
                                            MEA_BondingTx_dbt        *entry_po, 
                                            MEA_Bool                 *found_o);

MEA_Status MEA_API_BondingTx_GetNext_Entry(MEA_Unit_t            unit_i,
                                           MEA_Uint32              *id_io,
                                           MEA_BondingTx_dbt       *entry_po, 
                                           MEA_Bool                *found_o);



MEA_Status MEA_API_BondingTx_IsExist(MEA_Unit_t                unit_i,
                                   MEA_Uint32                  id_i,
                                   MEA_Bool                    *exist);
/************************************************************************/
/*                    Counters_Bonding                                  */
/************************************************************************/

MEA_Status MEA_API_Collect_Counters_Bonding(MEA_Unit_t              unit,
                                        MEA_Uint16                  id_i,
                                        MEA_BondingCount_dbt*       entry);

MEA_Status MEA_API_Collect_Counters_Bonding_ALL(MEA_Unit_t          unit);

MEA_Status MEA_API_Get_Counters_Bonding(MEA_Unit_t                    unit,
                                        MEA_Uint16                     id_i,
                                        MEA_BondingCount_dbt*          entry);

MEA_Status MEA_API_Clear_Counters_Bonding(MEA_Unit_t               unit,
                                          MEA_Uint16                id_i);


MEA_Status MEA_API_Clear_Counters_Bonding_All(MEA_Unit_t                    unit);


void MEA_API_check_bonding_state(void);


/************************************************************************/
/*                                                                      */
/************************************************************************/
typedef struct {
    MEA_Uint32 a[3];
}alexDBG_t;

MEA_Status MEA_API_ALEX_DBG(MEA_Uint32 num);

/************************************************************************/
/*      LAG API                                                         */
/************************************************************************/


typedef struct  
{
    MEA_Uint32     numof_clusters;
    ENET_QueueId_t cluster[MEA_LAG_MAX_NUM_OF_CLUSTERS];
    MEA_Uint8      clustermask;
}MEA_LAG_dbt;



MEA_Status MEA_API_LAG_Create_Entry(MEA_Unit_t            unit_i,
                                    MEA_LAG_dbt           *entry_pi,
                                    MEA_Uint16            *id_io);

/*Update cluster   */
MEA_Status MEA_API_LAG_UpDate_Entry(MEA_Unit_t         unit_i, MEA_Uint16         id_i);

MEA_Status MEA_API_LAG_Set_Entry(MEA_Unit_t         unit_i,
    MEA_Uint16         id_i,
    MEA_LAG_dbt        *entry_pi);
	
MEA_Status MEA_API_LAG_Set_ClusterMask(MEA_Unit_t unit_i, MEA_Uint16         id_i, ENET_QueueId_t clusterId, MEA_Bool enbale);
MEA_Status MEA_API_LAG_Delete_Entry(MEA_Unit_t                  unit_i,
                                    MEA_Uint16                   id_i);

MEA_Status MEA_API_LAG_Get_Entry(MEA_Unit_t                  unit_i,
                                 MEA_Uint16                  id_i,
                                 MEA_LAG_dbt                 *entry_po);

MEA_Status MEA_API_LAG_GetFirst_Entry(MEA_Unit_t        unit_i,
                                      MEA_Uint16        *id_o,
                                      MEA_LAG_dbt       *entry_po, 
                                      MEA_Bool          *found_o);

MEA_Status MEA_API_LAG_GetNext_Entry(MEA_Unit_t     unit_i,
                                     MEA_Uint16     *id_io,
                                     MEA_LAG_dbt    *entry_po, 
                                     MEA_Bool       *found_o);

MEA_Status MEA_API_LAG_IsExist(MEA_Unit_t       unit_i,
                               MEA_Uint16       id_i,
                               MEA_Bool         *exist);







/************************************************************************/
/*      ATM AutoSense API                                               */
/************************************************************************/

MEA_Status MEA_API_ATM_AutoSense(MEA_Unit_t unit_i,
								 MEA_Port_t port,
								 MEA_Uint16  vspId,
								 MEA_Uint32 *retProtocol,
								 MEA_Bool *exist);




/************************************************************************/
/* PRBS_Genrator  analyzer                                             */
/************************************************************************/

typedef struct {

   union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 lane_sync        :4;
            MEA_Uint32 Rx_aligned_status:1;
            MEA_Uint32 reserved             :2;
            MEA_Uint32 activate         :1; // 0 -disable 1-enable
            MEA_Uint32 pad              :24;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 pad              :24;
            MEA_Uint32 activate         :1; // 0 -disable 1-enable
            MEA_Uint32 reserved             :2;
            MEA_Uint32 Rx_aligned_status:1;
            MEA_Uint32 lane_sync        :4;
#endif
        }val;
        MEA_Uint32 reg[1];
    }v;

}MEA_PRBS_Gen_configure_dbt;

MEA_Status MEA_API_PRBS_SET_Generator(MEA_Unit_t  unit,MEA_Uint16 Id_i,MEA_PRBS_Gen_configure_dbt *entry);
MEA_Status MEA_API_PRBS_Get_Generator(MEA_Unit_t  unit,MEA_Uint16 Id_i,MEA_PRBS_Gen_configure_dbt *entry);




typedef struct {

MEA_Bool event_Lock;
MEA_Counter_t error_ber;
MEA_Counter_t good_bit;


}MEA_PRBS_analyzer_event_Counters_dbt;



MEA_Status MEA_API_Collect_Counters_PRBSs(MEA_Unit_t  unit);

MEA_Status MEA_API_Collect_Counters_PRBS(MEA_Unit_t  unit,MEA_Uint16 Id_i);

MEA_Status MEA_API_Clear_Counters_PRBSs(MEA_Unit_t  unit);

MEA_Status MEA_API_Clear_Counters_PRBS(MEA_Unit_t               unit,
                                       MEA_Uint16               Id_i);

MEA_Status MEA_API_Get_Counters_PRBS(MEA_Unit_t                             unit,
                                     MEA_Uint16                             Id_i,
                                     MEA_PRBS_analyzer_event_Counters_dbt *entry);



/************************************************************************/
/*     Interface                                                        */
/************************************************************************/
MEA_Status MEA_API_Get_Interface_CurrentLinkStatus(MEA_Unit_t unit,MEA_Uint16 interfaceId, MEA_Bool *sttate ,MEA_Bool silence);


MEA_Status MEA_API_Check_Interface_Link_Status(MEA_Unit_t unit,MEA_Bool current); /* set this on periodic timer */

MEA_Status MEA_API_get_Interface_oper_Link_Status(MEA_Unit_t unit, MEA_Port_t interfaceId, MEA_Bool *status);


typedef MEA_Status (*pGetIntExtLinkSttCallBack)(MEA_Unit_t unit, MEA_Port_t interfaceId, MEA_Bool *status);

MEA_Status MEA_API_set_Interface_ext_Link_callBack(MEA_Unit_t unit,pGetIntExtLinkSttCallBack callBack);

MEA_Status MEA_API_get_Interface_ext_Link_Status(MEA_Unit_t unit, MEA_Port_t interfaceId, MEA_Bool *status);

MEA_Status MEA_API_get_Interface_ext_Link_callBack(MEA_Unit_t unit, MEA_Port_t interfaceId, MEA_Bool *status);


MEA_Bool MEA_API_Interface_Valid(MEA_Unit_t Unit,MEA_Interface_t Id,MEA_Bool silent);

MEA_Bool MEA_API_Interface_Valid_TypeCorrect(MEA_Unit_t Unit,MEA_Interface_t Id,MEA_InterfaceType_t InterfaceType ,MEA_Bool silent);


/************************* section  Interface Type  *****************/


/************************************************************************/
/*  Interface Interlaken  Type                                          */
/************************************************************************/

typedef struct{
    
    MEA_Uint32  rx_serdes_resetn        :16;//bit per LANES TBD
    MEA_Uint32  ctl_rx_mframelen_minus1 :16;

    MEA_Uint32  ctl_rx_force_resync  :1;
    MEA_Uint32  ctl_rx_packet_mode   :1;
    MEA_Uint32  ctl_rx_reset_n       :1;
    MEA_Uint32  ctl_rx_burstmax      :2;

    /* tx */
    MEA_Uint32  ctl_tx_enable         :1;
    MEA_Uint32  ctl_tx_reset_n        :1;
    
    /*rare limit*/
    MEA_Uint32  ctl_tx_rlim_enable   :1;
    MEA_Uint32  ctl_tx_rlim_max      :12;
    MEA_Uint32  ctl_tx_rlim_delta    :12;
    MEA_Uint32  ctl_tx_rlim_intv     :8;

    MEA_Uint32  ctl_tx_burstmax      :2;
                                        /*
                                        0x0 = 64 bytes
                                        0x1 = 128 bytes
                                        0x2 = 192 bytes
                                        0x3 = 256 bytes
                                        */

    MEA_Uint32 ctl_tx_burstshort :3; 
                                        /*
                                        0x0 = 32 bytes
                                        0x1 = 64 bytes
                                        0x2 = 96 bytes
                                        0x3 = 128 bytes
                                        0x4 = 160 bytes
                                        0x5 = 192 bytes
                                        0x6 = 224 bytes
                                        0x7 = 256 bytes
                                        */

    MEA_Uint32 ctl_tx_fc_callen :4;  /*
                                        0x0 = 16 entries
                                        0x1 = 32 entries
                                        0x3 = 64 entries
                                        0x7 = 128 entries
                                        0xF = 256 entries
                                        All other values are reserved*/
    
    MEA_Uint32 ctl_tx_mubits              :8;                                  
                                    
    MEA_Uint32 ctl_tx_rdyout_thresh       :4;
    MEA_Uint32 ctl_tx_disabel_skipword    :1;
    MEA_Uint32 ctl_tx_mframelen_minus1    :16;

    MEA_Uint32 ctl_tx_diagword_lanestat   :8;//bit per LANES 
    MEA_Uint32 ctl_tx_diagword_intfstat   :1;
    
    MEA_Uint32 lbus                      :1;    //0 RX to TX  1- TX to RX
    MEA_Uint32 loopback_type             :3;
    MEA_Uint32 ctl_fc_enable             :1;  //RX
    MEA_Uint32 ctl_tx_fc_enable          :1;

 

} MEA_Interlaken_Entry_dbt;


typedef struct{
   
    union {
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 stat_rx_framing_err   :1;
    MEA_Uint32 stat_rx_mf_err        :1;
    MEA_Uint32 stat_rx_descram_err   :1;
    MEA_Uint32 stat_rx_mf_repeat_err :1;
    MEA_Uint32 stat_rx_mf_len_err    :1;
    MEA_Uint32 stat_rx_synced_err    :1;
    MEA_Uint32 stat_rx_synced        :1;
    
    MEA_Uint32 pad                   :25;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad                   :25;
    MEA_Uint32 stat_rx_synced        :1;
    MEA_Uint32 stat_rx_synced_err    :1;
    MEA_Uint32 stat_rx_mf_len_err    :1;
    MEA_Uint32 stat_rx_mf_repeat_err :1;
    MEA_Uint32 stat_rx_descram_err   :1;
    MEA_Uint32 stat_rx_mf_err        :1;
    MEA_Uint32 stat_rx_framing_err   :1;

#endif
        }val;
        MEA_Uint32 regs[1];

   }stat_lanes[MEA_INTERLAKEN_MAX_OF_LANES];


    
    
    MEA_Uint32 stat_rx_aligned      :1;
    MEA_Uint32 stat_rx_aligned_err  :1;
    MEA_Uint32 stat_rx_crc32_err    :1;
    MEA_Uint32 stat_rx_meop_err     :1;
    MEA_Uint32 stat_rx_msop_err     :1;
    MEA_Uint32 stat_rx_burst_err    :1;
    MEA_Uint32 pad1                 :26;
    
    MEA_Uint32 stat_tx_overflow_err     :1;
    MEA_Uint32 stat_tx_burst_err        :1;
    MEA_Uint32 stat_tx_underflow_err    :1;
    MEA_Uint32 pad2                     :29;


} MEA_Interlaken_Status_Entry_dbt;




/************************************************************************/
/*  Interface RXAUI   Type                                              */
/************************************************************************/                                          

typedef struct { 

        union{
        struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 loopback_mac       :1; //0 not-loop back 1-loop
    MEA_Uint32 power_tx_down      :1;
    MEA_Uint32 reset_fault_status :1;
    MEA_Uint32 reset_rx_link      :1;
    MEA_Uint32 reserved           :2;
    MEA_Uint32 reset_serdes_type  :2;
    MEA_Uint32 pad                :24;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad                :24;
    MEA_Uint32 reset_serdes_type  :2;  /*   0 no reset
                                            1 RX reset
                                            2'TX reset
                                            3 Global Tx and RX */

    MEA_Uint32 reserved           :2;
    MEA_Uint32 reset_rx_link      :1;
    MEA_Uint32 reset_fault_status :1;
    MEA_Uint32 power_tx_down      :1;
    MEA_Uint32 loopback_mac       :1; //0 not-loop back 1-loop
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 rxauvi_lb_type            :3;
     MEA_Uint32 pad1                     :29;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
       MEA_Uint32 pad1                   :29; 
       MEA_Uint32 rxauvi_lb_type         :3;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 xmac_reserved0   :1; /*0*/
    MEA_Uint32 xmac_reserved1   :1; /*1*/  /*F(not in use)*/
    MEA_Uint32 xmac_Force_ready :1; /*2*/
    MEA_Uint32 xmac_IFG_mode    :1;  /*3*/     /*('1' = WAN, '0' = LAN), we use '0'*/
    MEA_Uint32 xmac_DIC         :2; /*4:5*/
       /* 0 - no DIC
        1 - DIC enable average ipg- 12(9-15)
        2 - DIC enable average ipg - 8(5-11)
        3 - DIC enable average ipg - 6 (5-8)*/



    MEA_Uint32 xmac_reserved6_7 :2; /*6 7*/
    MEA_Uint32 pad2             :24;
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
#else //__BIG_ENDIAN
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
    MEA_Uint32 pad2             :24;
    MEA_Uint32 xmac_reserved6_7 :2; /*5 7*/
    MEA_Uint32 xmac_DIC         :2; /*4*/
    MEA_Uint32 xmac_IFG_mode    :1;  /*3*/     /*('1' = WAN, '0' = LAN), we use '0'*/
    MEA_Uint32 xmac_Force_ready :1; /*2*/
    MEA_Uint32 xmac_reserved1   :1; /*1*/  /*F(not in use)*/
    MEA_Uint32 xmac_reserved0   :1; /*0*/






#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN

    MEA_Uint32  rxaui_Id                   : 8;
    MEA_Uint32  rxaui_Loopback             : 1;   //(N.E only)
    MEA_Uint32  rxaui_PowerDown            : 1;
    MEA_Uint32  rxaui_Reset_Local_Fault    : 1;
    MEA_Uint32  rxaui_Reset_RX_Link_Status : 1;
    MEA_Uint32  rxaui_Test_enable          : 1;
    MEA_Uint32  rxaui_TestPattern          : 1;
    MEA_Uint32  rxaui_reserved             : 1;  //bit15
    MEA_Uint32  rxaui_Pad                  : 15;
#ifdef ARCH64
            MEA_Uint32 pad64_3	    	:32;
#endif
 
#else
#ifdef ARCH64
            MEA_Uint32 pad64_3	    	:32;
#endif
    MEA_Uint32  rxaui_Pad : 15;
    MEA_Uint32  rxaui_reserved : 1;  //bit15
    MEA_Uint32  rxaui_TestPattern : 1;
    MEA_Uint32  rxaui_Test_enable : 1;
    MEA_Uint32  rxaui_Reset_RX_Link_Status : 1;
    MEA_Uint32  rxaui_Reset_Local_Fault : 1;
    MEA_Uint32  rxaui_PowerDown : 1;
    MEA_Uint32  rxaui_Loopback : 1;   //(N.E only)
    MEA_Uint32  rxaui_Id : 8;


#endif

       
      }val;
        MEA_Uint32 regs[4];
      }RXAUI_reg;

}MEA_RXAUI_Entry_dbt;


typedef struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 tx_local_fault           :1;
    MEA_Uint32 rx_local_fault           :1;
    MEA_Uint32 sync_status              :4;
    MEA_Uint32 align_status             :1;
    MEA_Uint32 rx_link_status           :1;
    MEA_Uint32 Rx_reset_done_lane0      :1;
    MEA_Uint32 Rx_reset_done_lane1      :1;
    MEA_Uint32 Tx_reset_d_lane0         :1;
    MEA_Uint32 Tx_reset_d_lane1         :1;
    MEA_Uint32 Parity                   :4;
    MEA_Uint32 pad                      :16;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad                      :16;
    MEA_Uint32 Parity                   :4;
    MEA_Uint32 Tx_reset_d_lane1         :1;
    MEA_Uint32 Tx_reset_d_lane0         :1;
    MEA_Uint32 Rx_reset_done_lane1      :1;
    MEA_Uint32 Rx_reset_done_lane0      :1;
    MEA_Uint32 rx_link_status           :1;
    MEA_Uint32 align_status             :1;
    MEA_Uint32 sync_status              :4;
    MEA_Uint32 rx_local_fault           :1;
    MEA_Uint32 tx_local_fault           :1;
    
#endif

}MEA_RXAUI_Status_Entry_dbt;

/************************************************************************/
/*   Interface SFP_PLUS   Type  MEA_INTERFACETYPE_SINGLE10G             */
/************************************************************************/

typedef struct {

    union{
        struct{

#if __BYTE_ORDER == __LITTLE_ENDIAN 
            MEA_Uint32 xmac_Rx_pause_en     : 1; /*0*/
            MEA_Uint32 xmac_reserved1    : 1; /*1*/
            MEA_Uint32 xmac_IFG_mode     : 1;  /*2*/     /*('1' = WAN, '0' = LAN), we use '0'*/
            MEA_Uint32 xmac_DIC          : 2; /*3:4*/
            /*      0 - no DIC
                    1 - DIC enable average ipg- 12(9-15)
                    2 - DIC enable average ipg - 8(5-11)
                    3 - DIC enable average ipg - 6 (5-8)
            */



            MEA_Uint32 xmac_pma_reset                       : 1; /*5*/
            MEA_Uint32 xmac_pcs_reset                       : 1; /*6*/
            MEA_Uint32 xmac_pmd_tx_disable                  : 1; /*7*/
            MEA_Uint32 xmac_enable_RX_test_pattern_check    : 1; /*8*/
            MEA_Uint32 xmac_enable_TX_test_pattern          : 1; /*9*/
            MEA_Uint32 xmac_data_patter_select              : 1; /*10*/
            MEA_Uint32 xmac_test_patter_select              : 1; /*11*/
            MEA_Uint32 xmac_PRBS31_test_tx_pattern_enable   : 1; /*12*/
            MEA_Uint32 xmac_PRBS31_test_rx_pattern_enable   : 1; /*13*/
            MEA_Uint32 gt0_eyescanreset                     : 1;
            MEA_Uint32 gt0_eyescantrigger                   : 1;
            MEA_Uint32 gt0_rxcdrhold                        : 1;
            MEA_Uint32 gt0_txprbsforceerr                   : 1;
            MEA_Uint32 gt0_txpolarity                       : 1;
            MEA_Uint32 gt0_rxpolarity                       : 1;
            MEA_Uint32 gt0_txpmareset                       : 1;
            MEA_Uint32 gt0_rxpmareset                       : 1;
            MEA_Uint32 gt0_rxdfelpmreset                    : 1;
            MEA_Uint32 gt0_rxlpmen                          : 1;
            MEA_Uint32 gt0_txdiffctrl                       : 4;
            MEA_Uint32 gt0_rxrate                           : 3;

            MEA_Uint32 pad1                                 : 1;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else //__BIG_ENDIAN

#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 pad1             : 1;

        MEA_Uint32 gt0_rxrate           : 3;
        MEA_Uint32 gt0_txdiffctrl       : 4;
        MEA_Uint32 gt0_rxlpmen          : 1;
        MEA_Uint32 gt0_rxdfelpmreset    : 1;
        MEA_Uint32 gt0_rxpmareset       : 1;
        MEA_Uint32 gt0_txpmareset       : 1;
        MEA_Uint32 gt0_rxpolarity       : 1;
        MEA_Uint32 gt0_txpolarity       : 1;
        MEA_Uint32 gt0_txprbsforceerr   : 1;
        MEA_Uint32 gt0_rxcdrhold        : 1;
        MEA_Uint32 gt0_eyescantrigger   : 1;
        MEA_Uint32 gt0_eyescanreset     : 1;
        MEA_Uint32 xmac_PRBS31_test_rx_pattern_enable   : 1; /*13*//*45*/
        MEA_Uint32 xmac_PRBS31_test_tx_pattern_enable   : 1; /*12*/
        MEA_Uint32 xmac_test_patter_select              : 1; /*11*/
        MEA_Uint32 xmac_data_patter_select              : 1; /*10*/
        MEA_Uint32 xmac_enable_TX_test_pattern          : 1; /*9*/
        MEA_Uint32 xmac_enable_RX_test_pattern_check    : 1; /*8*/
        MEA_Uint32 xmac_pmd_tx_disable                  : 1; /*7*/
        MEA_Uint32 xmac_pcs_reset                       : 1; /*6*/
        MEA_Uint32 xmac_pma_reset                       : 1; /*5*/
        MEA_Uint32 xmac_DIC                             : 2; /*3:4*/
        /*      0 - no DIC
        1 - DIC enable average ipg- 12(9-15)
        2 - DIC enable average ipg - 8(5-11)
        3 - DIC enable average ipg - 6 (5-8)
        */
        MEA_Uint32 xmac_IFG_mode                        : 1;  /*2*/     /*('1' = WAN, '0' = LAN), we use '0'*/
        MEA_Uint32 xmac_reserved1                       : 1;
        MEA_Uint32 xmac_Rx_pause_en                     : 1; /*0*/




#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 gt0_txprecursor      : 5;
            MEA_Uint32 gt0_txpostcursor     : 5;
            MEA_Uint32 pad2                 : 6;
            MEA_Uint32 XmacRX_block_th      :16;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
            MEA_Uint32 XmacRX_block_th      :16;
            MEA_Uint32 pad2                 : 6;
            MEA_Uint32 gt0_txpostcursor     : 5;
            MEA_Uint32 gt0_txprecursor      : 5;

#endif


        }val;
        MEA_Uint32 regs[2];
    }SFP_PLUS;

}MEA_SFP_PLUS_Entry_dbt;

typedef union{
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32  PMA_Reset                   : 1; /*bit0*/
        MEA_Uint32  PMA_PMD_RX_Link_Status      : 1; /*bit1*/
        MEA_Uint32  PMA_PMD_Fault               : 1; /*bit2*/
        MEA_Uint32  PMA_PMD_RX_Fault            : 1; /*bit3*/
        MEA_Uint32  PMA_PMD_TX_Fault            : 1; /*bit4*/
        MEA_Uint32  PMD_RX_Signal_Detect        : 1; /*bit5*/
        MEA_Uint32  Core_Info_reserved0         : 1; /*bit6*/
        MEA_Uint32  Core_Info_patch_number      : 3; /*bit7:9*/
        MEA_Uint32  Core_Info_FEC_include       : 1; /*bit10*/
        MEA_Uint32  Core_Info_AN_include        : 1; /*bit11*/
        MEA_Uint32  Core_Info_reserved1         : 1; /*bit12*/
        MEA_Uint32  Core_Info_KR_include        : 1; /*bit13*/
        MEA_Uint32  Core_Info_parameter         : 4; /*bit14:17*/
        MEA_Uint32  Core_Info_SFP_plus_version  : 4; /*bit18:21*/
        MEA_Uint32  PCS_Reset                   : 1;   /*22*/
        MEA_Uint32  PCS_RX_Link_Status_latch    : 1;   /*23*/
        MEA_Uint32  PCS_Fault                   : 1;   /*24*/
        MEA_Uint32  PCS_RX_Fault                : 1;   /*25*/
        MEA_Uint32  PCS_TX_Fault                : 1;   /*26*/
        MEA_Uint32  RX_Locked                   : 1;   /*27*/
        MEA_Uint32  high_BER                    : 1;   /*28*/
        MEA_Uint32  PCS_RX_Link_Status          : 1;   /*29*/
        MEA_Uint32  RX_high_BER                 : 1;   /*30*/
        MEA_Uint32  RX_Block_Lock               : 1;   /*31*/
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif

#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif

        MEA_Uint32  RX_Block_Lock        :1;   /*31*/
        MEA_Uint32  RX_high_BER          :1;   /*30*/
        MEA_Uint32  PCS_RX_Link_Status   :1;   /*29*/
        MEA_Uint32  high_BER             :1;   /*28*/
        MEA_Uint32  RX_Locked            :1;   /*27*/
        MEA_Uint32  PCS_TX_Fault         :1;   /*26*/
        MEA_Uint32  PCS_RX_Fault         :1;   /*25*/
        MEA_Uint32  PCS_Fault            :1;   /*24*/
        MEA_Uint32  PCS_RX_Link_Status_latch   :1;   /*23*/
        MEA_Uint32  PCS_Reset            :1;   /*22*/



        MEA_Uint32  Core_Info_SFP_plus_version  :4;  /*18 21*/
        MEA_Uint32  Core_Info_parameter         :4;  /*14 17*/
        MEA_Uint32  Core_Info_KR_include        :1;
        MEA_Uint32  Core_Info_reserved1          :1;
        MEA_Uint32  Core_Info_AN_include        :1;
        MEA_Uint32  Core_Info_FEC_include       :1;
        MEA_Uint32  Core_Info_patch_number      :3;
        MEA_Uint32  Core_Info_reserved0          :1;



        MEA_Uint32  PMD_RX_Signal_Detect    :1; /*bit5*/
        MEA_Uint32  PMA_PMD_TX_Fault        :1; /*bit4*/
        MEA_Uint32  PMA_PMD_RX_Fault        :1; /*bit3*/
        MEA_Uint32  PMA_PMD_Fault           :1; /*bit2*/
        MEA_Uint32  PMA_PMD_RX_Link_Status  :1; /*bit1*/
        MEA_Uint32  PMA_Reset               :1; /*bit0*/

#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  PCS_Error_Blocks_Counter    : 8;
        MEA_Uint32  PCS_BER_Counter             : 6;
        MEA_Uint32  Test_Pattern_Error_Counter  : 16;
        MEA_Uint32  Reset_done                  : 1;
        MEA_Uint32  PCS_lock                    : 1;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif

#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif

        MEA_Uint32  PCS_lock                   : 1;
        MEA_Uint32  Reset_done                 : 1;
        MEA_Uint32  Test_Pattern_Error_Counter :16;
        MEA_Uint32  PCS_BER_Counter            :6;
        MEA_Uint32  PCS_Error_Blocks_Counter   :8;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 txresetdone          : 1;
        MEA_Uint32 rxresetdone          : 1;
        MEA_Uint32 eyescandataerro      : 1;
        MEA_Uint32 rxprbserr            : 1;
        MEA_Uint32 Rxbufstatus          : 3;
        MEA_Uint32 resetdone            : 1;
        MEA_Uint32 Txbufstatus          : 2;
        MEA_Uint32 txuserrdy            : 1;
        MEA_Uint32 Dmonitorout          : 8;
        MEA_Uint32 pad2                 : 14;
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
        MEA_Uint32 pad2                 : 14;
        MEA_Uint32 Dmonitorout          : 8;
        MEA_Uint32 txuserrdy            : 1;
        MEA_Uint32 Txbufstatus          : 2;
        MEA_Uint32 resetdone            : 1;
        MEA_Uint32 Rxbufstatus          : 3;
        MEA_Uint32 rxprbserr            : 1;
        MEA_Uint32 eyescandataerro      : 1;
        MEA_Uint32 rxresetdone          : 1;
        MEA_Uint32 txresetdone          : 1;


#endif

    }val;
    MEA_Uint32 regs[3];
}MEA_SFP_PLUS_Status_Entry_dbt;
/************************************************************************/
/*  Interface XLAUI   Type 40Giga                                        */
/************************************************************************/
typedef struct {

    union{
        struct{


#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 tx_enable                :1;
            MEA_Uint32 tx_rfi                   :1;
            MEA_Uint32 tx_idle                  :1;
            MEA_Uint32 tx_inscrc                :1;
            MEA_Uint32 tx_igncrc                :1;
            MEA_Uint32 XLAUI_loopback           :3;
            MEA_Uint32 rx_enable                :1;/*bit8*/
            MEA_Uint32 rx_chkpre                :1;
            MEA_Uint32 rx_chksfd                :1;
            MEA_Uint32 rx_delcrc                :1;
            MEA_Uint32 rx_igncrc                :1;
            MEA_Uint32 pad1                     :3;
            MEA_Uint32 RX_PAUSE_PRI0_EN         : 1; /*16*/
            MEA_Uint32 RX_PAUSE_PRI1_EN         : 1; /*17*/
            MEA_Uint32 RX_PAUSE_PRI2_EN         : 1; /*18*/
            MEA_Uint32 RX_PAUSE_PRI3_EN         : 1; /*19*/
            MEA_Uint32 RX_PAUSE_PRI4_EN         : 1; /*20*/
            MEA_Uint32 RX_PAUSE_PRI5_EN         : 1; /*21*/
            MEA_Uint32 RX_PAUSE_PRI6_EN         : 1; /*22*/
            MEA_Uint32 RX_PAUSE_PRI7_EN         : 1; /*23*/
            MEA_Uint32 RX_PAUSE_GLBL_EN         : 1; /*24*/
            MEA_Uint32 RSVD25                   : 1; /*25*/
            MEA_Uint32 RX_FWD_CTL_PKT           : 1; /*26*/
            MEA_Uint32 RSVD27                   : 1; /*27*/
            MEA_Uint32 RX_ENABLE_PCP            : 1; /*28*/
            MEA_Uint32 RX_ENABLE_PPP            : 1; /*29*/
            MEA_Uint32 RX_ENABLE_GPP            : 1; /*30*/
            MEA_Uint32 RX_ENABLE_GCP            : 1; /*31*/
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif


#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
            MEA_Uint32 RX_ENABLE_GCP            : 1; /*31*/
            MEA_Uint32 RX_ENABLE_GPP            : 1; /*30*/
            MEA_Uint32 RX_ENABLE_PPP            : 1; /*29*/
            MEA_Uint32 RX_ENABLE_PCP            : 1; /*28*/
            MEA_Uint32 RSVD27                   : 1; /*27*/
            MEA_Uint32 RX_FWD_CTL_PKT           : 1; /*26*/
            MEA_Uint32 RSVD25                   : 1; /*25*/
            MEA_Uint32 RX_PAUSE_GLBL_EN         : 1; /*24*/
            MEA_Uint32 RX_PAUSE_PRI7_EN         : 1; /*23*/
            MEA_Uint32 RX_PAUSE_PRI6_EN         : 1; /*22*/
            MEA_Uint32 RX_PAUSE_PRI5_EN         : 1; /*21*/
            MEA_Uint32 RX_PAUSE_PRI4_EN         : 1; /*20*/
            MEA_Uint32 RX_PAUSE_PRI3_EN         : 1; /*19*/
            MEA_Uint32 RX_PAUSE_PRI2_EN         : 1; /*18*/
            MEA_Uint32 RX_PAUSE_PRI1_EN         : 1; /*17*/
            MEA_Uint32 RX_PAUSE_PRI0_EN         : 1; /*16*/

            MEA_Uint32 pad1                     :3;
            MEA_Uint32 rx_igncrc                :1;
            MEA_Uint32 rx_delcrc                :1;
            MEA_Uint32 rx_chksfd                :1;
            MEA_Uint32 rx_chkpre                :1;
            MEA_Uint32 rx_enable                :1;/*bit8*/
            MEA_Uint32 XLAUI_loopback           :3;
            MEA_Uint32 tx_igncrc                : 1;
            MEA_Uint32 tx_inscrc                :1;
            MEA_Uint32 tx_idle                  :1;
            MEA_Uint32 tx_rfi                   :1;
            MEA_Uint32 tx_enable                :1;
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 diag_rx_frsnc         : 1; /*bit0*/ /* auto cleared*/
            MEA_Uint32 diag_tx_rfi           : 1;
            MEA_Uint32 diag_tx_IDLE          : 1;
            MEA_Uint32 diag_tx_lb_serdes     : 1;
            MEA_Uint32 diag_rx_plfi          : 1;
            MEA_Uint32 diag_rx_test_pattern  : 1;
            MEA_Uint32 diag_tx_test_pattern  : 1;
            MEA_Uint32 diag_pad4             : 25;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
           



#else  
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
           
            MEA_Uint32 diag_pad4             :25;
            MEA_Uint32 diag_tx_test_pattern  :1;
            MEA_Uint32 diag_rx_test_pattern  :1;
            MEA_Uint32 diag_rx_plfi          :1;
            MEA_Uint32 diag_tx_lb_serdes     :1;
            MEA_Uint32 diag_tx_IDLE          :1;
            MEA_Uint32 diag_tx_rfi           :1;
            MEA_Uint32 diag_rx_frsnc         :1; /*bit0*/ /* auto cleared*/
#endif
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 max_packetLen    :15;
            MEA_Uint32 reserv4          :1;
            MEA_Uint32 min_packetLen    :8;
            MEA_Uint32 pad5             :8;
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_2	    	:32;
#endif
            MEA_Uint32 pad5             :8;
            MEA_Uint32 min_packetLen    :8;
            MEA_Uint32 reserv4          :1;
            MEA_Uint32 max_packetLen    :15;

#endif

/************************************************************************/
/*offset 33  */
#if __BYTE_ORDER == __LITTLE_ENDIAN
            MEA_Uint32 check_Udp         : 1;
            MEA_Uint32 calculate_new_Udp : 1;
            MEA_Uint32 check_tcp         : 1;
            MEA_Uint32 calculate_new_tcp : 1;
            MEA_Uint32 pad6              : 28;
#ifdef ARCH64
            MEA_Uint32 pad64_3	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_3	    	:32;
#endif
            MEA_Uint32 pad6              : 28;
            MEA_Uint32 calculate_new_tcp : 1;
            MEA_Uint32 check_tcp         : 1;
            MEA_Uint32 calculate_new_Udp : 1;
            MEA_Uint32 check_Udp         : 1;

#endif

 /*offset 36 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
	MEA_Uint32 TXpause;
#ifdef ARCH64
            MEA_Uint32 pad64_4	    	:32;
#endif	
	
#else
#ifdef ARCH64
            MEA_Uint32 pad64_4	    	:32;
#endif
	MEA_Uint32 TXpause;
#endif


        }val;
        MEA_Uint32 regs[5];
    }XLAUI;

}MEA_XLAUI_Entry_dbt;

typedef union{
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32 rxsts            : 1; /*bit0*/
        MEA_Uint32 rxalign          : 1; /*bit1*/
        MEA_Uint32 rxalign_eror     :1; /*bit2*/
        MEA_Uint32 rxmisal          :1; /*bit3*/
        MEA_Uint32 rxmtf            :1; /*bit4*/
        MEA_Uint32 rxlclf           :1; /*bit5*/
        MEA_Uint32 rxrmtf           :1; /*bit6*/
        MEA_Uint32 rxerrp           :1; /*bit7*/
        MEA_Uint32 rxhiber          :1; /*bit8*/
        MEA_Uint32 txlrclf          :1; /*bit9*/
        MEA_Uint32 tx_underflow     :1;/*bit10*/
        MEA_Uint32 tx_overflow      :1;/*bit11*/
        MEA_Uint32 pad0             : 20;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 pad0             :20;
        MEA_Uint32 tx_overflow      :1;/*bit11*/
        MEA_Uint32 tx_underflow     :1;/*bit10*/
        MEA_Uint32 txlrclf          :1; /*bit9*/
        MEA_Uint32 rxhiber          :1; /*bit8*/
        MEA_Uint32 rxerrp           :1; /*bit7*/
        MEA_Uint32 rxrmtf           :1; /*bit6*/
        MEA_Uint32 rxlclf           :1; /*bit5*/
        MEA_Uint32 rxmtf            :1; /*bit4*/
        MEA_Uint32 rxmisal          :1; /*bit3*/
        MEA_Uint32 rxalign_eror     :1; /*bit2*/
        MEA_Uint32 rxalign          :1; /*bit1*/
        MEA_Uint32 rxsts            :1; /*bit0*/
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 L0SYNC  : 1;
        MEA_Uint32 L0SYNCE : 1;
        MEA_Uint32 L0LENE  : 1;
        MEA_Uint32 L0LENRE : 1;
        MEA_Uint32 L0MFE   : 1;
        MEA_Uint32 L0BLCK  : 1;
        MEA_Uint32 L0DMX   : 1;
        MEA_Uint32 L0BIPE  : 1;

        MEA_Uint32  L1SYNC : 1;
        MEA_Uint32 L1SYNCE : 1;
        MEA_Uint32 L1LENE   : 1;
        MEA_Uint32 L1LENRE  : 1;
        MEA_Uint32 L1MFE    : 1;
        MEA_Uint32 L1BLCK   : 1;
        MEA_Uint32 L1DMX    : 1;
        MEA_Uint32 L1BIPE   : 1;

        MEA_Uint32 L2SYNC   : 1;
        MEA_Uint32 L2SYNCE : 1;
        MEA_Uint32  L2LENE  : 1;
        MEA_Uint32  L2LENRE : 1;
        MEA_Uint32 L2MFE    : 1;
        MEA_Uint32 L2BLCK   : 1;
        MEA_Uint32 L2DMX    : 1;
        MEA_Uint32 L2BIPE   : 1;

        MEA_Uint32 L3SYNC   : 1;
        MEA_Uint32 L3SYNCE  : 1;
        MEA_Uint32 L3LENE   : 1;
        MEA_Uint32 L3LENRE  : 1;
        MEA_Uint32 L3MFE    : 1;
        MEA_Uint32 L3BLCK   : 1;
        MEA_Uint32 L3DMX    : 1;
        MEA_Uint32 L3BIPE   : 1;
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif

#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif

        MEA_Uint32 L3BIPE  : 1;
        MEA_Uint32 L3DMX   : 1;
        MEA_Uint32 L3BLCK  : 1;
        MEA_Uint32 L3MFE   : 1;
        MEA_Uint32 L3LENRE : 1;
        MEA_Uint32 L3LENE  : 1;
        MEA_Uint32 L3SYNCE : 1;
        MEA_Uint32 L3SYNC  : 1;

        MEA_Uint32 L2BIPE  : 1;
        MEA_Uint32 L2DMX   : 1;
        MEA_Uint32 L2BLCK  : 1;
        MEA_Uint32 L2MFE   : 1;
        MEA_Uint32 L2LENRE : 1;
        MEA_Uint32 L2LENE  : 1;
        MEA_Uint32 L2SYNCE : 1;
        MEA_Uint32 L2SYNC  : 1;

        MEA_Uint32 L1BIPE  : 1;
        MEA_Uint32 L1DMX   : 1;
        MEA_Uint32 L1BLCK  : 1;
        MEA_Uint32 L1MFE   : 1;
        MEA_Uint32 L1LENRE : 1;
        MEA_Uint32 L1LENE  : 1;
        MEA_Uint32 L1SYNCE : 1;
        MEA_Uint32 L1SYNC  : 1;


        MEA_Uint32 L0BIPE  : 1;
        MEA_Uint32 L0DMX   : 1;
        MEA_Uint32 L0BLCK  : 1;
        MEA_Uint32 L0MFE   : 1;
        MEA_Uint32 L0LENRE : 1;
        MEA_Uint32 L0LENE  : 1;
        MEA_Uint32 L0SYNCE : 1;
        MEA_Uint32 L0SYNC  : 1;
        



#endif

    }val;
    MEA_Uint32 regs[2];
}MEA_XLAUI_Status_Entry_dbt;

/************************************************************************/
/*  Interface RGMII   Type                                              */
/************************************************************************/   



typedef union {
    struct{

#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32  duplex           :1; /**/
        MEA_Uint32  speed            :2;
        MEA_Uint32  link             :1; /**/
        MEA_Uint32  pad               :28;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32  pad               :28;
        MEA_Uint32  link             :1; /**/
        MEA_Uint32  speed            :2;
        MEA_Uint32  duplex           :1; /**/
#endif
    }value;
    MEA_Uint32 reg[1];



}MEA_RGMII_Status_Entry_dbt;




/************************************************************************/
/*   Set_Interface                                                      */
/************************************************************************/
typedef struct
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 lb_type         :3; /*see MEA_Egress_Mac_Serdes_lb_t*/
    MEA_Uint32 pag             :29;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pag             :29;
    MEA_Uint32 lb_type  :3; /*see MEA_Egress_Mac_Serdes_lb_t*/
#endif
}MEA_LoopBack_dbt;


#define MEA_INTEFACE_SPEED_GIGA 0
#define MEA_INTEFACE_SPEED_100M 1
#define MEA_INTEFACE_SPEED_10M  2
#define MEA_INTEFACE_SPEED_10G  3
#define MEA_INTEFACE_SPEED_2dot5G  4

typedef struct{
    
    MEA_InterfaceType_t     interfaceTyps;

    
    MEA_LoopBack_dbt      loopback; 


    ENET_ShaperId_t                    shaper_Id              ;
    MEA_Uint32                         shaper_enable       :  1; /* 0- Disable 1-Enable*/
    MEA_Uint32                         Shaper_compensation    :  2; /* 00-Chunk 01-Packet 11-AAL5 */
    MEA_Uint32                         pad                    : 29;
    MEA_shaper_info_dbt                shaper_info;
    
    
    MEA_Uint32                          speed           : 3;
    MEA_Uint32                          autoneg         : 1;
    MEA_Uint32                          mdio_Disable    : 1; /**/
    
    MEA_Uint32                          pad1 : 27;

    MEA_Uint32                         Tx_Disable :1;           



}MEA_Interface_Entry_dbt;


MEA_Status MEA_API_IS_Interface_Valid(MEA_Unit_t       unit, MEA_Interface_t  id, MEA_Bool *Valid, MEA_Bool silence);

MEA_Status MEA_API_Set_Interface_ConfigureEntry(MEA_Unit_t       unit,
                                                MEA_Interface_t  id,
                                                MEA_Interface_Entry_dbt*     entry);

MEA_Status MEA_API_Get_Interface_ConfigureEntry(MEA_Unit_t         unit,
                                                MEA_Interface_t     Id,
                                                MEA_Interface_Entry_dbt* entry);



MEA_Status MEA_API_Get_Interface_Status_Entry(MEA_Unit_t         unit,
                                              MEA_Interface_t     Id,
                                              MEA_Interlaken_Status_Entry_dbt* entry);



MEA_Status MEA_API_Get_Interface_ToPortMapp(MEA_Unit_t       unit,
                                            MEA_Interface_t  Id,
                                            MEA_OutPorts_Entry_dbt  *OutPorts);



/*-----------------------------------------------------------------------------------------------------*/

MEA_Status MEA_API_Set_Interface_Interlaken_Entry(MEA_Unit_t       unit,
                                                  MEA_Interface_t  id,
                                     MEA_Interlaken_Entry_dbt*     entry);

MEA_Status MEA_API_Get_Interface_Interlaken_Entry(MEA_Unit_t        unit,
                                         MEA_Interface_t            Id,
                                         MEA_Interlaken_Entry_dbt*  entry);


MEA_Status MEA_API_Get_Interface_Interlaken_Status_Entry(MEA_Unit_t         unit,
                                                        MEA_Interface_t     Id,
                                                        MEA_Interlaken_Status_Entry_dbt* entry);







/************************************************************************/
/*  Interface RXAUI   Status                                             */
/************************************************************************/                                          

MEA_Status MEA_API_Set_Interface_RXAUI_Entry(MEA_Unit_t    unit,
                                             MEA_Interface_t    Id,
                                             MEA_RXAUI_Entry_dbt*   entry);

MEA_Status MEA_API_Get_Interface_RXAUI_Entry(MEA_Unit_t        unit,
                                             MEA_Interface_t                 Id,
                                             MEA_RXAUI_Entry_dbt* entry);




MEA_Status MEA_API_Get_Interface_RXAUI_Status_Entry(MEA_Unit_t    unit,
                                                    MEA_Interface_t     Id,
                                                    MEA_RXAUI_Status_Entry_dbt*   entry);


/************************************************************************/
/*  Interface SFP_PLUS   API                                             */
/************************************************************************/

MEA_Status MEA_API_Set_Interface_SFP_PLUS_Entry(MEA_Unit_t    unit,
    MEA_Interface_t    Id,
    MEA_SFP_PLUS_Entry_dbt*   entry);

MEA_Status MEA_API_Get_Interface_SFP_PLUS_Entry(MEA_Unit_t        unit,
    MEA_Interface_t                 Id,
    MEA_SFP_PLUS_Entry_dbt* entry);




MEA_Status MEA_API_Get_Interface_SFP_PLUS_Status_Entry(MEA_Unit_t    unit,
    MEA_Interface_t     Id,
    MEA_SFP_PLUS_Status_Entry_dbt*   entry);


/************************************************************************/
/*         Interface XLAUI   API                                        */
/************************************************************************/
MEA_Status MEA_API_Set_Interface_XLAUI_Entry(MEA_Unit_t    unit,
                                             MEA_Interface_t    Id,
                                             MEA_XLAUI_Entry_dbt*   entry);

MEA_Status MEA_API_Get_Interface_XLAUI_Entry(MEA_Unit_t             unit,
                                             MEA_Interface_t        Id,
                                             MEA_XLAUI_Entry_dbt*  entry);


MEA_Status MEA_API_Get_Interface_XLAUI_Status_Entry(MEA_Unit_t    unit,
                                                       MEA_Interface_t     Id,
                                                        MEA_XLAUI_Status_Entry_dbt*   entry);


/************************************************************************/
/*   RGMII                                                              */
/************************************************************************/


MEA_Status MEA_API_Get_Interface_RGMII_Status_Entry(MEA_Unit_t    unit_i, MEA_Interface_t Id,
                                                    MEA_RGMII_Status_Entry_dbt   *entry);



/************************************************************************/
/*   SGMII                                                              */
/************************************************************************/
typedef enum MyEnum
{
    MEA_SGMII_SPEED_1G   = 0,
    MEA_SGMII_SPEED_10M  = 2,
    MEA_SGMII_SPEED_100M = 3,
}MEA_SGMII_SPEED_t;


typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
        MEA_Uint32 Unidirectional_Enable    : 1; /*32*/
        MEA_Uint32 reserved0                : 1;
        MEA_Uint32 Powerdown                : 1;
        MEA_Uint32 electrically_Isolate     : 1;
        MEA_Uint32 speed                    : 2; /*36:37*/ /*0 -1GiGa 1-NA  2-10M  3-100M*/
        MEA_Uint32 RX_PMA_reset             : 1; /*38*/
        MEA_Uint32 TX_PMA_reset             : 1; /*39*/
        MEA_Uint32 AN_reset                 : 1; /*40*/ /* restart Auto Negotiation*/
        MEA_Uint32 reserv_5                 : 5; /*41:45*/
        MEA_Uint32 basex_or_sgmii           : 1;  /*46*/     /*0-1000BASE-X  1-sgmii*/
        MEA_Uint32 assert_en                : 1;
        MEA_Uint32 pad                      : 16;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
        MEA_Uint32 pad                      : 16;
        MEA_Uint32 basex_or_sgmii           : 1;  /*46*/     /*0-1000BASE-X  1-sgmii*/
        MEA_Uint32 reserv_5                 : 5; /*41:45*/
        MEA_Uint32 AN_reset                 : 1; /*40*/ /* restart Auto Negotiation*/
        MEA_Uint32 TX_PMA_reset             : 1; /*39*/
        MEA_Uint32 RX_PMA_reset             : 1; /*38*/
        MEA_Uint32 speed                    : 2; /*36:37*/ /*0 -1GiGa 1-NA  2-10M  3-100M*/
        MEA_Uint32 electrically_Isolate     : 1;
        MEA_Uint32 Powerdown                : 1;
        MEA_Uint32 reserved0                : 1;
        MEA_Uint32 Unidirectional_Enable    : 1;
           

#endif
    }value;
    MEA_Uint32 reg[1];

} MEA_SGMII_Entry_dbt;




typedef union {
       struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    
    
   MEA_Uint32  link_Status      :1;
   MEA_Uint32  Link_sync        :1; 
   MEA_Uint32  rudi_c           :1;
   MEA_Uint32  rudi_i           :1;
   MEA_Uint32  rudi_invalid     :1; 
   MEA_Uint32  rxdisperr        :1; 
   MEA_Uint32  rx_notintable    :1;
   MEA_Uint32  phy_linkstatuse  :1;
   MEA_Uint32  remote_fault_encoding :2;
   MEA_Uint32  speed            :2;
   MEA_Uint32  duplex           :1;
   MEA_Uint32  remote_fault     :1;
   MEA_Uint32  pause            :2;
    MEA_Uint32 pad              :16;
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32 pad               :16;
    MEA_Uint32  pause            :2;/*14 15*/
    MEA_Uint32  remote_fault     :1; /*13*/
    MEA_Uint32  duplex           :1; /*12*/
    MEA_Uint32  speed            :2; /*10 11*/
    MEA_Uint32  remote_fault_encoding :2; /*8 9*/
    MEA_Uint32  phy_linkstatuse  :1; /*7*/
    MEA_Uint32  rx_notintable    :1;/*6*/
    MEA_Uint32  rxdisperr        :1; /*5*/
    MEA_Uint32  rudi_invalid     :1; /*4*/
    MEA_Uint32  rudi_i           :1; /*3*/
    MEA_Uint32  rudi_c           :1; /*2*/
    MEA_Uint32  Link_sync        :1; /*1*/
    MEA_Uint32  link_Status      :1; /*0*/

#endif
       }value;
       MEA_Uint32 reg[1];

}MEA_SGMII_Status_Entry_dbt;




MEA_Status MEA_API_Get_Interface_SGMII_Status_Entry(MEA_Unit_t unit_i, 
    MEA_Interface_t Id,
    MEA_SGMII_Status_Entry_dbt   *entry);


MEA_Status MEA_API_Set_Interface_SGMII_Entry(MEA_Unit_t    unit,
    MEA_Interface_t    Id,
    MEA_SGMII_Entry_dbt*   entry);

MEA_Status MEA_API_Get_Interface_SGMII_Entry(MEA_Unit_t             unit,
    MEA_Interface_t        Id,
    MEA_SGMII_Entry_dbt*   entry);

/************************************************************************/
/*   QSGMII                                                              */
/************************************************************************/


typedef struct{
    MEA_SGMII_Status_Entry_dbt qsgmii[4];

}MEA_QSGMII_Status_Entry_dbt;

MEA_Status MEA_API_Get_Interface_QSGMII_Status_Entry(MEA_Unit_t unit_i, 
                                                     MEA_Interface_t Id,
                                                     MEA_QSGMII_Status_Entry_dbt   *entry);
typedef struct{
struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32  reset_full_transmit         : 1; /*0*/
    MEA_Uint32  reset_TX_PMA                : 1; /*1*/
    MEA_Uint32  reset_TX_PCS                : 1; /*2*/
    MEA_Uint32  RX_pcs_reset                : 1; /*3*/
    MEA_Uint32  TX_diffctrl                 : 4; /*4:7*/
    MEA_Uint32  TX_postcursor               : 5; /*8:12*/
    MEA_Uint32  TX_precursor                : 5;/*13:17*/
    MEA_Uint32  RX_buf_reset                 : 1; /*18*/
    MEA_Uint32  RX_dfelpmreset              : 1; /*19*/
    MEA_Uint32  RX_dfeagcovrden             : 1; /*20*/
    MEA_Uint32  RX_lpmen                    : 1; /*21*/
    MEA_Uint32  TX_PRBS_Pattern             : 3; /*22:24*/
    MEA_Uint32  TX_prbs_forceerr            : 1;/*25*/
    MEA_Uint32  RX_prbs_cntreset            : 1;/*26*/
    MEA_Uint32  RX_prbssel                  : 3;/*27:29*/
    MEA_Uint32  RX_reset                    : 1;/*30*/
    MEA_Uint32  RX_pma_reset                 : 1;/*31*/
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  RX_pma_reset : 1;/*31*/
    MEA_Uint32  RX_reset : 1;/*30*/
    MEA_Uint32  RX_prbssel : 3;/*27:29*/
    MEA_Uint32  RX_prbs_cntreset : 1;/*26*/
    MEA_Uint32  TX_prbs_forceerr : 1;/*25*/
    MEA_Uint32  TX_PRBS_Pattern : 3; /*22:24*/
    MEA_Uint32  RX_lpmen : 1; /*21*/
    MEA_Uint32  RX_dfeagcovrden : 1; /*20*/
    MEA_Uint32  RX_dfelpmreset : 1; /*19*/
    MEA_Uint32  RX_buf_reset : 1; /*18*/
    MEA_Uint32  TX_precursor : 5;/*13:17*/
    MEA_Uint32  TX_postcursor : 5; /*8:12*/
    MEA_Uint32  TX_diffctrl : 4; /*4:7*/
    MEA_Uint32  RX_pcs_reset : 1; /*3*/
    MEA_Uint32  reset_TX_PCS : 1; /*2*/
    MEA_Uint32  reset_TX_PMA : 1; /*1*/
    MEA_Uint32  reset_full_transmit : 1; /*0*/

#endif
}value;
MEA_Uint32 regs[1];

}MEA_QSGMII_GT_Entry_dbt;

typedef struct{

    MEA_SGMII_Entry_dbt    Sgmii_info;
    MEA_QSGMII_GT_Entry_dbt GT;

} MEA_QSGMII_Entry_dbt;



MEA_Status MEA_API_Set_Interface_QSGMII_Entry(MEA_Unit_t    unit,
    MEA_Interface_t    Id,
    MEA_QSGMII_Entry_dbt*   entry);

MEA_Status MEA_API_Get_Interface_QSGMII_Entry(MEA_Unit_t    unit,
    MEA_Interface_t    Id,
    MEA_QSGMII_Entry_dbt*   entry);



typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32  state_lpm_dfe                           : 17; /*0:16*/
    MEA_Uint32  GTX_GTH_RX_DFE_Signal                   : 7; /*23:17*/
    MEA_Uint32  Indicates_received                      : 4; /*27:24*/
    MEA_Uint32  Indicates_disparity_err_received_data   : 4;/*31:28*/
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
#else
#ifdef ARCH64
            MEA_Uint32 pad64_0	    	:32;
#endif
    MEA_Uint32  Indicates_disparity_err_received_data   :4;/*31:28*/
    MEA_Uint32  Indicates_received                      : 4; /*27:24*/
    MEA_Uint32  GTX_GTH_RX_DFE_Signal                   : 7; /*23:17*/
    MEA_Uint32  state_lpm_dfe                           : 17; /*0:16*/

#endif


#if __BYTE_ORDER == __LITTLE_ENDIAN 

    MEA_Uint32  Indicates_status_RX_buffer         : 3; /*34:32*/
    MEA_Uint32  Indicates_status_TX_buffer          : 2; /*36:35*/
    MEA_Uint32  GT_Status_Rx_charisk                : 4; /*40:37*/
    MEA_Uint32  GT_Status_RX_chariscomma            : 4; /*44:41*/
    MEA_Uint32  GT_rxratedone                       : 1; /*45*/
    MEA_Uint32  GT_eyescandataerror                 : 1; /*46*/
    MEA_Uint32  GT_RX_PMA_reset_done                : 1; /*47*/
    MEA_Uint32  GT_PLL_lock                         : 1; /*48*/
    MEA_Uint32  GT_Status_RX_resetdone              : 1; /*49*/
    MEA_Uint32  GT_Status_TX_resetdone              : 1; /*50*/
    MEA_Uint32  RX_PRBS_Pattern_Checker_error       : 1; /*51*/
    MEA_Uint32  RX_comma_is_detected_indication     : 1;/*52*/
    MEA_Uint32  Rx_Byte_is_realigned                : 1;/*53*/
    MEA_Uint32  RX_Byte_is_aligned_indication       : 1;/*54*/
    MEA_Uint32  pad                                 : 9;/*63: 55*/
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif

#else
#ifdef ARCH64
            MEA_Uint32 pad64_1	    	:32;
#endif
    MEA_Uint32  pad                                 :9;/*63: 55*/
    MEA_Uint32  RX_Byte_is_aligned_indication       : 1;/*54*/
    MEA_Uint32  Rx_Byte_is_realigned                :1;/*53*/
    MEA_Uint32  RX_comma_is_detected_indication     : 1;/*52*/
    MEA_Uint32  RX_PRBS_Pattern_Checker_error       : 1; /*51*/
    MEA_Uint32  GT_Status_TX_resetdone              : 1; /*50*/
    MEA_Uint32  GT_Status_RX_resetdone              : 1; /*49*/
    MEA_Uint32  GT_PLL_lock                         : 1; /*48*/
    MEA_Uint32  GT_RX_PMA_reset_done                : 1; /*47*/
    MEA_Uint32  GT_eyescandataerror                 : 1; /*46*/
    MEA_Uint32  GT_rxratedone                       : 1; /*45*/
    MEA_Uint32  GT_Status_RX_chariscomma            : 4; /*44:41*/
    MEA_Uint32  GT_Status_Rx_charisk                : 4; /*40:37*/
    MEA_Uint32  Indicates_status_TX_buffer          : 2; /*36:35*/
    MEA_Uint32  Indicates_status_RX_buffer          : 3; /*34:32*/
#endif

    }value;
    MEA_Uint32 regs[2];

}MEA_QSGMII_GT_Status_Entry_dbt;


MEA_Status MEA_API_Get_Interface_QSGMII_GT_Status_Entry(MEA_Unit_t unit_i,
    MEA_Interface_t Id,
    MEA_QSGMII_GT_Status_Entry_dbt   *entry);


/********************************************************************************************************************/


/************************************************************************/
/* HC API  Rmon counter                                                */
/************************************************************************/

typedef struct  
{

    MEA_Counter_t Rx_Pkts; 
    MEA_Counter_t Rx_learn_Pkts;
    MEA_Counter_t Rx_DeCompressed_Pkts;
    MEA_Counter_t Rx_NoDeCompressed_Pkts;
    MEA_Counter_t Rx_64Octets_Pkts;
    MEA_Counter_t Rx_65to127Octets_Pkts;
    MEA_Counter_t Rx_128to255Octets_Pkts;
    MEA_Counter_t Rx_256to511Octets_Pkts;
    MEA_Counter_t Rx_512to1023Octets_Pkts;
    MEA_Counter_t Rx_1024to1518Octets_Pkts;
    MEA_Counter_t Rx_1519to2047Octets_Pkts;
    MEA_Counter_t Rx_2048toMaxOctets_Pkts;
    MEA_Counter_t Rx_DeCompressedCRC_Err;
    
    MEA_Counter_t Rx_Bytes; 
    MEA_Counter_t Rx_Drop_Bytes; 
    MEA_Counter_t Rx_Error_Pkts;

    
    MEA_Uint64    Rx_ratePacket;
    MEA_Uint64    Rx_rate;
    /********* TX Counter **********************/

    MEA_Counter_t Tx_Pkts; 
    MEA_Counter_t Tx_learn_Pkts;
    MEA_Counter_t Tx_Compressed_Pkts;
    MEA_Counter_t Tx_NoCompressed_Pkts;
    MEA_Counter_t Tx_64Octets_Pkts;
    MEA_Counter_t Tx_65to127Octets_Pkts;
    MEA_Counter_t Tx_128to255Octets_Pkts;
    MEA_Counter_t Tx_256to511Octets_Pkts;
    MEA_Counter_t Tx_512to1023Octets_Pkts;
    MEA_Counter_t Tx_1024to1518Octets_Pkts;
    MEA_Counter_t Tx_1519to2047Octets_Pkts;
    MEA_Counter_t Tx_2048toMaxOctets_Pkts;
    
  
   
    MEA_Counter_t Tx_CompressedCRC_Err;
    MEA_Counter_t Tx_Bytes;
    MEA_Counter_t Tx_actual_bytes;
    MEA_Uint64    Tx_ratePacket;
    MEA_Uint64    Tx_rate;
    MEA_Uint32    Tx_Compress_ratio;

    time_t t1;


}MEA_Counters_RMON_HC_dbt;



MEA_Status MEA_API_Collect_Counters_HC_RMONs(MEA_Unit_t                unit);

MEA_Status MEA_API_Clear_Counters_HC_RMONs(MEA_Unit_t                    unit);

MEA_Status MEA_API_Clear_Counters_HC_RMON(MEA_Unit_t                    unit,
                                           MEA_HDC_t                    hcId);


MEA_Status MEA_API_Collect_Counters_HC_RMON(MEA_Unit_t                    unit,
                                             MEA_HDC_t                    hcId,
                                             MEA_Counters_RMON_HC_dbt        *entry);


MEA_Status MEA_API_Get_Counters_HC_RMON(MEA_Unit_t                    unit,
                                         MEA_HDC_t                    hcId,
                                         MEA_Counters_RMON_HC_dbt         *entry);


/************************************************************************/
/* HDC API  create compress Profile                                     */
/************************************************************************/
typedef struct{
        MEA_Uint32  bit_Of_byte_comp[MEA_HD_NM_WORD];
        MEA_Uint32  start_offset;
}MEA_HDC_Data_dbt;


MEA_Status MEA_API_HC_Comp_Profile_IsExist(MEA_Unit_t                      unit_i,
                                           MEA_Uint16                       id_i,
                                           MEA_Bool                        *exist);

MEA_Status MEA_API_HC_Comp_Create_Profile(MEA_Unit_t                unit,
                                            MEA_Uint16                *id_io,
                                            MEA_HDC_Data_dbt          *entry_pi);


MEA_Status MEA_API_HC_Comp_Set_Profile(MEA_Unit_t                   unit_i,
                                         MEA_Uint16                 id_i,
                                         MEA_HDC_Data_dbt           *entry_pi,
                                         MEA_Bool                   force);

MEA_Status MEA_API_HC_Comp_Delete_Profile(MEA_Unit_t              unit,
                                          MEA_Uint16              id_i);

MEA_Status MEA_API_HC_Comp_Get_Profile(MEA_Unit_t                          unit_i,
                                         MEA_Uint16                           id_i,
                                         MEA_HDC_Data_dbt                   *entry_po);

MEA_Status MEA_API_HC_Comp_GetFirst_Profile(MEA_Unit_t                         unit_i,
                                              MEA_Uint16                        *id_o,
                                              MEA_HDC_Data_dbt                  *entry_po, /* Can't be NULL */
                                              MEA_Bool                          *found_o);

MEA_Status MEA_API_HC_Comp_GetNext_Profile(MEA_Unit_t                unit_i,
                                             MEA_Uint16                        *id_io,
                                             MEA_HDC_Data_dbt                  *entry_po, /* Can't be NULL */
                                             MEA_Bool                          *found_o);

/************************************************************************/
/* HDC API  create Decompress Profile                                     */
/************************************************************************/
MEA_Status MEA_API_HC_Decomp_Profile_IsExist(MEA_Unit_t                      unit_i,
                                          MEA_Uint16                       id_i,
                                          MEA_Bool                        *exist);

MEA_Status MEA_API_HC_Decomp_Create_Profile(MEA_Unit_t                unit,
                                      MEA_Uint16                *id_io,
                                      MEA_HDC_Data_dbt          *entry_pi);


MEA_Status MEA_API_HC_Decomp_Set_Profile(MEA_Unit_t                     unit_i,
                                         MEA_Uint16                     id_i,
                                         MEA_HDC_Data_dbt               *entry_pi,
                                         MEA_Bool                       force);

MEA_Status MEA_API_HC_Decomp_Delete_Profile(MEA_Unit_t              unit,
                                      MEA_Uint16                    id_i);

MEA_Status MEA_API_HC_Decomp_Get_Profile(MEA_Unit_t                          unit_i,
                                         MEA_Uint16                           id_i,
                                         MEA_HDC_Data_dbt                   *entry_po);

MEA_Status MEA_API_HC_Decomp_GetFirst_Profile(MEA_Unit_t                         unit_i,
                                              MEA_Uint16                        *id_o,
                                              MEA_HDC_Data_dbt                  *entry_po, /* Can't be NULL */
                                              MEA_Bool                          *found_o);

MEA_Status MEA_API_HC_Decomp_GetNext_Profile(MEA_Unit_t                unit_i,
                                         MEA_Uint16                        *id_io,
                                         MEA_HDC_Data_dbt                  *entry_po, /* Can't be NULL */
                                         MEA_Bool                          *found_o);


/************************************************************************/
/* HDC API set compress Profile to protocol Type                         */
/************************************************************************/
typedef struct{
    MEA_Uint32   L2Type  :5;
    MEA_Uint32   L3Type  :5;
    MEA_Uint32   pad     :12;

}MEA_HC_key_proto_t;

typedef struct{
    MEA_Uint8  Compress_Prof_id;
    MEA_Bool enable;
}MEA_HC_L2L3_CompInfo_dbt;


MEA_Status MEA_API_HC_Compress_Protocol_IsExist(MEA_Unit_t                 unit_i,
                                                MEA_HC_key_proto_t         *key,
                                                MEA_Bool                   *exist);

MEA_Status MEA_API_HC_Set_Compress_Protocol(MEA_Unit_t             unit,
                                        MEA_HC_key_proto_t                 *key,
                                        MEA_HC_L2L3_CompInfo_dbt        *entry);


MEA_Status MEA_API_HC_Get_Compress_Protocol(MEA_Unit_t                  unit,
                                         MEA_HC_key_proto_t            *key,
                                         MEA_HC_L2L3_CompInfo_dbt      *entry);
MEA_Status MEA_API_HC_Comp_GetFirst_Protocol(MEA_Unit_t                         unit_i,
                                                MEA_HC_key_proto_t              *key,/* Can't be NULL */
                                                MEA_HC_L2L3_CompInfo_dbt        *entry_po, 
                                                MEA_Bool                        *found_o);

MEA_Status MEA_API_HC_Comp_GetNext_Protocol(MEA_Unit_t                         unit_i,
                                            MEA_HC_key_proto_t                *key,/* Can't be NULL */
                                            MEA_HC_L2L3_CompInfo_dbt          *entry_po, /* Can't be NULL */
                                            MEA_Bool                          *found_o);


/************************************************************************/
/* HDC API  compress   Hc ID                                            */
/************************************************************************/
typedef enum{
    MEA_HDC_GET_CONTROL=0,
    MEA_HDC_GET_INFO=1,
    MEA_HDC_GET_LAST=2

}MEA_hdc_Get_info_t;

typedef enum{
    MEA_HDC_TYPE_COMPRESS_DISABLE=0,
    MEA_HDC_TYPE_COMPRESS_ENABLE=1,
    MEA_HDC_TYPE_COMPRESS_DISCARD=2,
    MEA_HDC_TYPE_COMPRESS_TO_CPU=3,
    MEA_HDC_TYPE_COMPRESS_ENABLE_ADD_SNIFFER=4,
    MEA_HDC_TYPE_LAST
}MEA_hdc_type_t;


typedef struct{
     MEA_hdc_type_t type;

    MEA_Bool   LearnEnable;
    MEA_Uint32 LearnCount;
    MEA_Uint32 InvalidateCount;



}MEA_HDC_control_dbt;

typedef struct{
    MEA_Uint8   L2Type;
    MEA_Uint8   L3Type;

    MEA_Uint8  vp;
    MEA_Uint8  num_of_data;
    MEA_Uint8  *header;

}MEA_HC_Header_Key_dbt;

typedef struct{
    MEA_hdc_Get_info_t             type;
    MEA_HDC_control_dbt    status;
    
/*info*/    
    MEA_Bool  header_valid;
    MEA_HC_Header_Key_dbt header_data;
    
    
}MEA_hdc_Get_info_dbt;







MEA_Status MEA_API_HC_Set_Compress_Config(MEA_Unit_t                unit,
                                    MEA_HDC_t                  hcId,
                                    MEA_HDC_control_dbt       *entry);



MEA_Status MEA_API_HC_Delete_Compress_Config(MEA_Unit_t  unit, MEA_HDC_t   Id);

MEA_Status MEA_API_HC_Get_Compress_Config_Info(MEA_Unit_t                unit,
                                          MEA_HDC_t                  hcId,
                                          MEA_hdc_Get_info_dbt      *entry);


typedef struct{
    MEA_Uint32 Hc_31_0;        // bit fields
    MEA_Uint32 Hc_63_32;
    MEA_Uint32 Hc_95_64;
    MEA_Uint32 Hc_127_96;

    MEA_Uint32 Hc_159_128;
    MEA_Uint32 Hc_191_160;
    MEA_Uint32 Hc_223_192;
    MEA_Uint32 Hc_255_224;

}MEA_HDC_ALERT_t;


MEA_Status MEA_API_HDC_New_HcId_Alert_HDC(MEA_Unit_t     unit, MEA_HDC_ALERT_t  *data);

MEA_Status MEA_API_HDC_New_HcId_Alert_DEC(MEA_Unit_t     unit, MEA_HDC_ALERT_t  *data);




MEA_Status MEA_API_HDC_Get_Mng_HcId_DEC(MEA_Unit_t                unit,
                                        MEA_HDC_t                  hcId,
                                        MEA_hdc_Get_info_dbt       data);
/**********************End HDC*********************/


/************************************************************************/
/*       MSTP                                                           */
/************************************************************************/
typedef enum{ 
    MEA_VP_STATE_FWD=0,
    MEA_VP_STATE_BLK_REGULAR=1,
    MEA_VP_STATE_BLK_L2CP=2,
    MEA_VP_STATE_BLK_ALL=3
}MEA_VP_State_t;




MEA_Status MEA_API_Set_VP_State(MEA_Unit_t unit , MEA_Uint16  vpn,ENET_QueueId_t Cluster_id,MEA_VP_State_t State) ;

MEA_Status MEA_API_Set_VP_State_Clear(MEA_Unit_t unit , MEA_Uint16  vpn);

MEA_Status MEA_API_Get_VP_StateAsBLK(MEA_Unit_t unit, MEA_Uint16  vpn, MEA_Bool *exist);
MEA_Status MEA_API_Get_VP_State(MEA_Unit_t unit, MEA_Uint16  vpn, MEA_VP_State_t State, MEA_OutPorts_Entry_dbt *out_blk);

/************************************************************************/
/* Egress Filter                                                        */
/************************************************************************/

typedef struct{
    MEA_Bool                valid;
    MEA_EgressPort_Filter_t data_Rule;
}MEA_EgressFilter_Act_dbt;


MEA_Status MEA_API_Configure_EgressFilter_Act(MEA_Unit_t                           unit,
                                              MEA_EgressFilter_Act_dbt            *data,
                                              MEA_Uint16                          *egressFilter_ACT_Id);






typedef struct{


    MEA_Uint32 dest_port                  :  7;
    MEA_Uint32 L2_protocol_type           :  5; /*see type   MEA_PARSING_L2_KEY_t */

   
    MEA_Uint32 net_tag                    : 20;
    MEA_Uint32 sub_protocol_type          :  4; /* See type */

    
    MEA_Uint32 net_tag_to_valid           :  1;
    MEA_Uint32 net_tag_to_value           : 20;
   
    MEA_Uint32 inner_netTag_from_valid    :  1;
    MEA_Uint32 inner_netTag_from_value    : 24;

    MEA_Uint32 inner_netTag_to_valid      :  1;
    MEA_Uint32 inner_netTag_to_value      : 24;


} MEA_EgressFilter_Entry_Key_dbt;



typedef struct{
    MEA_Bool      valid;
    MEA_Uint16    egressFilter_ACT_Id; /*if 0 GENRET NEW   */
                                           
}MEA_EgressFilter_Entry_Data_dbt;



MEA_Status MEA_API_Create_EgressFilter(MEA_Unit_t                           unit,
                                       MEA_EgressFilter_Entry_Key_dbt       *key,
                                       MEA_EgressFilter_Entry_Data_dbt      *data,
                                       MEA_Uint16                       *egressFilterId);


MEA_Status MEA_API_Delete_EgressFilter(MEA_Unit_t            unit, MEA_Uint16   egressFilterId);

MEA_Status MEA_API_Get_EgressFilter(MEA_Unit_t                           unit,
                                    MEA_Uint16                       egressFilterId,
                                    MEA_EgressFilter_Entry_Key_dbt       *key,
                                    MEA_EgressFilter_Entry_Data_dbt          *entry_po);


MEA_Status MEA_API_EgressFilter_GetFirst(MEA_Unit_t                           unit_i,
                                                    MEA_EgressFilter_Entry_Key_dbt     *key,/* Can't be NULL */
                                                    MEA_EgressFilter_Entry_Data_dbt        *entry_po, 
                                                    MEA_Bool                        *found_o);

MEA_Status MEA_API_EgressFilter_GetNext(MEA_Unit_t                          unit_i,
                                        MEA_EgressFilter_Entry_Key_dbt      *key,/* Can't be NULL */
                                        MEA_EgressFilter_Entry_Data_dbt     *entry_po, /* Can't be NULL */
                                        MEA_Bool                            *found_o);




typedef struct{
    MEA_Bool user_temp_alarm;
    MEA_Bool over_temp_alarm;

    MEA_Bool vccint_alarm;
    MEA_Bool vccaux_alarm;
    MEA_Bool vbram_alarm;

    MEA_Int32 temperature[3];  /*current Max Min*/

    MEA_Uint32 volt[3];  /*0 vccint 1 vccaux 2 vbram*/
    MEA_Uint32 vccint_volt[2]; /*  Max Min */
    MEA_Uint32 vccaux_volt[2]; /*  Max Min */
    MEA_Uint32 vbram_volt[2];  /*  Max Min */


}MEA_XADC_Info_dbt;


MEA_Status MEA_API_XADC_FPGA_Status(MEA_Unit_t      unit_i, MEA_XADC_Info_dbt *entry);


typedef struct {

    MEA_Uint32 number_words         : 10;
    MEA_Uint32 vxlan_snp_empty      : 1;
    MEA_Uint32 vxlan_snp_full       : 1;
    MEA_Uint32 vxlan_snp_overflow   : 1;
    MEA_Uint32 vxlan_snp_underflow  : 1;
    MEA_Uint32 pad                  : 18;



}MEA_Snoop_info_dbt;

MEA_Status MEA_API_Get_Vxlan_snooping_Info(MEA_Unit_t  unit, MEA_Snoop_info_dbt *infoEntry);

typedef struct {
    MEA_Uint32  vlan_vxlan_int   : 12;
    MEA_Uint32  vni              : 24;
    MEA_Uint32  L3_type          : 5;
    MEA_Uint32  L2_sub_type      : 4;
    MEA_Uint32  L2_type          : 5;
    MEA_Uint32  src_port         : 7;
    MEA_Uint32  src_ipv4_int;
    MEA_Uint32  src_ipv4_ext;
    MEA_MacAddr sa_mac_ext;

}MEA_Snoop_Entry_dbt;

MEA_Status MEA_API_Get_Vxlan_snooping_Entry(MEA_Unit_t  unit, MEA_Snoop_Entry_dbt *Entry);


/************************************************************************/
/*   ISOLATE profiles                                                    */
/************************************************************************/

typedef struct {
    MEA_OutPorts_Entry_dbt outQueue;
}MEA_ISOLATE_dbt;

MEA_Status MEA_API_ISOLATE_IsExist(MEA_Unit_t unit_i, MEA_Uint16 id_i, MEA_Bool silent, MEA_Bool *exist);
MEA_Status MEA_API_ISOLATE_Create_Entry(MEA_Unit_t   unit_i, MEA_Uint16     *id_io);
MEA_Status MEA_API_ISOLATE_Set_Entry(MEA_Unit_t   unit_i, MEA_Uint16     id_io, MEA_ISOLATE_dbt  *entry_pi);
MEA_Status MEA_API_ISOLATE_Delete_Entry(MEA_Unit_t   unit_i, MEA_Uint16     id_io);
MEA_Status MEA_API_ISOLATE_Get_Entry(MEA_Unit_t   unit_i, MEA_Uint16     id_io, MEA_ISOLATE_dbt  *entry_pi);







/*--------------------------------------------------------------------------------*/

typedef struct {
    MEA_Bool    Ipv4Addr_en;
    MEA_IpAddr_t IpAddr;          /* Host order */
    MEA_Bool    Ipv6Addr_en;
    MEA_Uint32  Ipv6Addr[4];

    MEA_Uint32    IpMaskInBits;    /* IPv4: 8..31, IPv6: 16..64 */
    MEA_Uint32    vrf; /* 0 - 7 */
    MEA_Uint8    maskKeyType;
    MEA_Uint8    pad[2];
} MEA_LPM_Entry_key_t;

typedef struct {

    MEA_Bool               OutPorts_valid;
    MEA_OutPorts_Entry_dbt OutPorts;

    MEA_Uint32     actionId_valid : 1;
    MEA_Action_t   actionId;


    MEA_Uint32  dummy;           /* T.B.D       */
} MEA_LPM_Entry_data_t;

/**
* @brief Creates a new LPM entry
*
* @param unit_i : unit number
* @param key_pi : A key entry to insert to the table
* @param data_pi : A data entry to insert to the table, that matches the key
* @return 1 MEA_OK if no error occurred MEA_ERROR otherwise
*/
MEA_Status MEA_API_Create_LPM(MEA_Unit_t unit_i, MEA_LPM_Entry_key_t *key_pi, MEA_LPM_Entry_data_t *data_pi);




/**
* @brief Deletes an existing LPM entry
*
* @param unit_i : unit number
* @param key_pi : A key entry to delete from the table
* @return MEA_OK if no error occurred MEA_ERROR otherwise
*/
MEA_Status MEA_API_Delete_LPM(MEA_Unit_t unit_i, MEA_LPM_Entry_key_t *key_pi);

/**
* @brief Gets an entry from the LPM table by the key given as parameter
*
* @param unit_i : unit number
* @param key_pi : A key entry to get from the table
* @param data_po : A data entry to get from the table, that matches the key
* @return MEA_OK if no error occurred MEA_ERROR otherwise
*/
MEA_Status MEA_API_Get_LPM(MEA_Unit_t unit_i, MEA_LPM_Entry_key_t *key_pi, MEA_LPM_Entry_data_t *data_po);

/**
* @brief Gets the first entry in the LPM table
*
* @param unit_i : unit number
* @param key_po : A key entry to get from the table
* @param data_po : A data entry to get from the table, that matches the key
* @param found_o : A pointer to a Boolean, the informs if an entry was found.
* @return MEA_OK if no error occurred MEA_ERROR otherwise
*/
MEA_Status MEA_API_GetFirst_LPM(MEA_Unit_t unit_i, MEA_LPM_Entry_key_t *key_po, MEA_LPM_Entry_data_t *data_po, MEA_Bool *found_o);

/**
* @brief Gets the next entry in the LPM table after key_pio's entry
*
* @param unit_i : unit number
* @param key_pio : A key entry to get the next entry after it from the table
* @param data_po : A data entry to get from the table, that matches the key
* @param found_o : A pointer to a Boolean, the informs if an entry was found.
* @return MEA_OK if no error occurred MEA_ERROR otherwise
*/
MEA_Status MEA_API_GetNext_LPM(MEA_Unit_t unit_i, MEA_LPM_Entry_key_t *key_pio, MEA_LPM_Entry_data_t *data_po, MEA_Bool *found_o);



MEA_Status MEA_API_Set_LPM(MEA_Unit_t            unit_i,
    MEA_LPM_Entry_key_t  *key_pi,
    MEA_LPM_Entry_data_t *data_pi);


/*--------------------------------------------------------------------------------*/

typedef struct
{
    MEA_Counter_t droppedPackets;
    MEA_Counter_t egressPackets;
    MEA_Counter_t ingressPackets;
    MEA_Counter_t egressBytes;
    MEA_Counter_t ingressBytes;
    MEA_Int32 deltaIngressPackets;
    MEA_Int32 deltaIngressBytes;
    MEA_Int32 curQueueSizePackets;
    MEA_Int32 curQueueSizeBytes;
}MEA_Counters_QueueStatistics_qStats_dbt;

/**
* @brief Starts the collections of packet statistics
*
* @param unit_i : unit number
* @return MEA_OK if no error occurred MEA_ERROR otherwise
*/
MEA_Status MEA_API_Start_QueueStatistics(MEA_Unit_t unit_i);

/**
* @brief Stops the collections of packet statistics
*
* @param unit_i : unit number
* @return MEA_OK if no error occurred MEA_ERROR otherwise
*/
MEA_Status MEA_API_Stop_QueueStatistics(MEA_Unit_t unit_i);

/**
* @brief Clears the counters of packet statistics from accumulated data.
*
* @param unit_i : unit number
* @return MEA_OK if no error occurred MEA_ERROR otherwise
*/
MEA_Status MEA_API_ClearCounters_QueueStatistics(MEA_Unit_t unit_i);

/**
* @brief Gets the whole database of queues' accumulated statistics.
*
* @param unit_i : unit number
* @param dbStats : A pointer to a pointer to struct of type MEA_Counters_QueueStatistics_qStats_dbt,
*                  which will hold an array of all the queues' statistics, in all the clusters of the database.
* @return MEA_OK if no error occurred MEA_ERROR otherwise
*/
MEA_Status MEA_API_GetStatDataBase_QueueStatistics(MEA_Unit_t unit_i, const MEA_Counters_QueueStatistics_qStats_dbt **dbStats);

/******************************** end of API ***************************************************/
/******************************** end of API ***************************************************/
/******************************** end of API ***************************************************/
/******************************** end of API ***************************************************/

/************************* INDEX-  section 7. Under development ******************/
/*                                                                               */
/*         The following function are Under development , please don't           */
/*         use them                                                              */
/*                                                                               */
/*                                                                               */
/*********************************************************************************/

MEA_Bool MEA_API_IsOutPortOf_Service(MEA_Unit_t    unit,
                                     MEA_Service_t serviceId,
                                     MEA_Port_t    outPort);




#define MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN                         MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN
#define MEA_DSE_FORWARDING_KEY_TYPE_DA                                  MEA_SE_FORWARDING_KEY_TYPE_DA

#define MEA_DSE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID            MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID
#define MEA_DSE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN                MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN


#define MEA_DSE_FORWARDING_KEY_TYPE_VLAN_VPN                            MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN
#define MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN                             MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN
#define MEA_DSE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN     MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN
#define MEA_DSE_FORWARDING_KEY_TYPE_LAST                                MEA_SE_FORWARDING_KEY_TYPE_LAST
#define MEA_DSE_Forwarding_key_type_te                                  MEA_SE_Forwarding_key_type_te

#define MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN                           MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN
#define MEA_DSE_LEARNING_KEY_TYPE_SA                                    MEA_SE_LEARNING_KEY_TYPE_SA
#define MEA_DSE_LEARNING_KEY_TYPE_DA_PLUS_PPPOE_SESSION_ID              MEA_SE_LEARNING_KEY_TYPE_DA_PLUS_PPPOE_SESSION_ID
#define MEA_DSE_LEARNING_KEY_TYPE_VLAN_VPN                              MEA_SE_LEARNING_KEY_TYPE_VLAN_VPN
#define MEA_DSE_LEARNING_KEY_TYPE_OAM_VPN                               MEA_SE_LEARNING_KEY_TYPE_OAM_VPN
#define MEA_DSE_LEARNING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN       MEA_SE_LEARNING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN
#define MEA_DSE_LEARNING_KEY_TYPE_LAST                                  MEA_SE_LEARNING_KEY_TYPE_LAST
#define MEA_DSE_Learning_key_type_te                                    MEA_SE_Learning_key_type_te


#define MEA_DSE_Entry_key_dbt MEA_SE_Entry_key_dbt


#if MEA_OLD_DSE_SUPPORT

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                         Old APIs                                              */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA DSE Table Typedefs                                     */
/*                                                                               */
/*  Note:  The MEA_API_XXX_DSE_Entry is for backward compatibility ,             */
/*         The new API call MEA_API_XXX_SE_Entry                                 */
/*                                                                               */
/*-------------------------------------------------------------------------------*/





typedef struct {
    MEA_Bool      action_force                   ; /* 0 - ignore action structure , 1 - force action structure */
    MEA_pm_type_te pm_type;
    MEA_PmId_t    pmId                         ; /* See note1  */
    MEA_TmId_t    tmId                         ; /* see note1  */
    MEA_Editing_t editId                       ; /* See note1  */
    /********************* Low level - shall be set to 0 **********************/
    MEA_Uint16    reserved                     ;
}MEA_DSE_Entry_action_dbt;

typedef struct {
    MEA_OutPorts_Entry_dbt OutPorts;    //  0..127 out ports bits mask 
    MEA_Uint32  static_flag     :  1;     //  0..  0 Static MAC adress   (1 bit)
    MEA_Uint32  aging            :  2;    //  1..  2 Aging counter value (2 bit)    
    MEA_Uint32  valid             :  1;    //  3..  3 Valid entry         (1 bit)      
    MEA_Uint32  sa_discard      :  1;   //  4..  4 SA discard flag (1 bit)
    MEA_Uint32 protocol_llc_force       :  1;
    MEA_Uint32 Protocol                 :  2; /* use MEA_ACTION_PROTOCOL_XXX defines */
    MEA_Uint32 Llc                      :  1;
    MEA_Uint32 ptmp                     :  1;
    MEA_Uint32 mc_fid_valid             :  1;
    MEA_Uint32 mc_fid_edit              :  3; 
    MEA_Uint32  cos_force                :  2;
    MEA_Uint32  cos                        :  3;
    MEA_Uint32  l2pri_force                :  2;
    MEA_Uint32  l2pri                    :  3;
    MEA_Uint32  color_force                :  2;
    MEA_Uint32  color                    :  2;

    MEA_Uint32  flowCoSMappingProfile_force     :  1;
    MEA_Uint32  flowCoSMappingProfile_id        :  6;
    MEA_Uint32  flowMarkingMappingProfile_force :  1;
    MEA_Uint32  flowMarkingMappingProfile_id    :  6;
    
    MEA_Uint32 access_port                 : 10;
    
    MEA_Limiter_t  LimiterId;
    MEA_DSE_Entry_action_dbt  action;
} MEA_DSE_Entry_data_dbt;

typedef struct {
    MEA_DSE_Entry_key_dbt key;
    MEA_DSE_Entry_data_dbt data;
} MEA_DSE_Entry_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                    MEA DSE APIs                                               */
/*                                                                               */
/*  Note:  The MEA_API_XXX_DSE_Entry is for backward compatibility ,             */
/*         The new API call MEA_API_XXX_SE_Entry                                 */
/*                                                                               */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status MEA_API_Create_DSE_Entry    (MEA_Unit_t                        unit,
                                        MEA_DSE_Entry_dbt               *entry,
                                        MEA_Policer_Entry_dbt          *Policer_entry,
                                        MEA_EgressHeaderProc_Entry_dbt *EHP_Entry);


MEA_Status MEA_API_Delete_DSE_Entry    (MEA_Unit_t                     unit,
                                        MEA_DSE_Entry_key_dbt         *key);

MEA_Status MEA_API_Set_DSE_Entry  (MEA_Unit_t                      unit,
                                   MEA_DSE_Entry_dbt              *entry,
                                     MEA_Policer_Entry_dbt          *Policer_Entry,
                                   MEA_EgressHeaderProc_Entry_dbt *EHP_Entry);

MEA_Status MEA_API_Get_DSE_Entry       (MEA_Unit_t                        unit,
                                        MEA_DSE_Entry_dbt               *entry,
                                        MEA_Policer_Entry_dbt          *Policer_entry,
                                        MEA_EgressHeaderProc_Entry_dbt *EHP_Entry );



MEA_Status MEA_API_GetFirst_DSE_Entry  (MEA_Unit_t                      unit_i,
                                        MEA_DSE_Entry_dbt              *entry_po,
                                        MEA_Policer_Entry_dbt          *policer_po, /* Can be NULL */
                                        MEA_EgressHeaderProc_Entry_dbt *editing_po, /* Can be NULL */
                                        MEA_Bool                       *found_o);

MEA_Status MEA_API_GetNext_DSE_Entry   (MEA_Unit_t                      unit_i,
                                        MEA_DSE_Entry_dbt              *entry_po,
                                        MEA_Policer_Entry_dbt          *policer_po, /* Can be NULL */
                                        MEA_EgressHeaderProc_Entry_dbt *editing_po, /* Can be NULL */
                                        MEA_Bool                       *found_o);
 


MEA_Status MEA_API_Set_DSE_Aging (MEA_Unit_t unit,
                                 MEA_Uint32 seconds,
                                 MEA_Bool   admin_status);
MEA_Status MEA_API_Get_DSE_Aging  (MEA_Unit_t unit,
                                   MEA_Uint32 *seconds,
                                   MEA_Bool   *admin_status);

MEA_Status MEA_API_DeleteAll_DSE (MEA_Unit_t unit);

MEA_Status MEA_API_Delete_all_forwarder_entries(MEA_Unit_t unit);

#endif //MEA_OLD_DSE_SUPPORT





#ifdef __cplusplus
 }
#endif 



#endif /*_MEA_API_H */
