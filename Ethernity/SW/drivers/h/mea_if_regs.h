/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


#ifndef MEA_IF_REGS_H
#define	MEA_IF_REGS_H

#include "MEA_platform.h"
#include "mea_device_regs.h"

#ifdef __cplusplus
extern "C" {
#endif



#define MEA_IF_WRITE_DATA_REG(x)     MEA_IF_WRITE_DATA_INFO_REG
#define MEA_IF_READ_DATA_REG(x)      MEA_IF_READ_DATA_INFO_REG

/* IF globals 3.0: c4 , c8  3.5: c4,c8,cc and 00-5c */
#define MEA_IF_GLOBAL_REG(i) ((MEA_Uint32) \
                                (((i)<3) ? (ENET_IF_GLOBAL0_REG + (((i)  )*4)) : (ENET_IF_GLOBAL3_REG +(((i)-3)*4)) ))

#define MEA_IF_SRV_MAKE0_REG                       MEA_IF_WRITE_DATA_REG(0)
#define MEA_IF_SRV_MAKE1_REG                       MEA_IF_WRITE_DATA_REG(1)
#define MEA_IF_SRV_MAKE2_REG                       MEA_IF_WRITE_DATA_REG(2)
#define MEA_IF_SRV_MAKE3_REG                       MEA_IF_WRITE_DATA_REG(3)


/* special value in case we work with the service table */
#define MEA_IF_CMD_PARAM_REG_IGNORE              0xffffffff


/* MEA_IF_CMD_REG fields */ 
#define MEA_IF_CMD_GET_NEXT_MASK                0x00000001
#define MEA_IF_CMD_MAKE_REG_MASK                0x00000002
#define MEA_IF_CMD_RST_NEXT_MASK                0x00000004
#define MEA_IF_CMD_PARSER_MASK	                0x00000008
#define MEA_IF_CMD_FILTER_GET_NEXT_MASK         0x00000010
#define MEA_IF_CMD_FILTER_MAKE_REG_MASK         0x00000020
#define MEA_IF_CMD_FILTER_RST_NEXT_MASK         0x00000040

#define MEA_IF_CMD_HC_GET_NEXT_MASK            0x00001000
#define MEA_IF_CMD_HC_MAKE_REG_MASK            0x00002000
#define MEA_IF_CMD_HC_RST_NEXT_MASK            0x00004000

#define MEA_IF_CMD_EGR_CLASS_GET_NEXT_MASK     0x00010000
#define MEA_IF_CMD_EGR_CLASS_MAKE_REG_MASK     0x00020000
#define MEA_IF_CMD_EGR_CLASS_RST_NEXT_MASK     0x00040000

#define MEA_IF_CMD_SRVEVC_CLASS_GET_NEXT_MASK     0x00100000
#define MEA_IF_CMD_SRVEVC_CLASS_MAKE_REG_MASK     0x00200000
#define MEA_IF_CMD_SRVEVC_CLASS_RST_NEXT_MASK     0x00400000


//bit 9,8 is group of hash
#define MEA_IF_CMD_MAKE_REG_MASK_SRV_HASH_GROUP(group)         ((group)<<8)


/**       ENET_IF_HASH_EVENT_REG **/
/*Read the Collision group  */
#define  MEA_IF_COLL_HASH_EVENT_ST_MASK      0x0000000f
#define  MEA_IF_COLL_HASH_EVENT_ST_GROUP     0
#define  MEA_IF_COLL_HASH_EVENT_EVC_MASK     0x000000f0
#define  MEA_IF_COLL_HASH_EVENT_EVC_GROUP    4
#define  MEA_IF_COLL_HASH_EVENT_FILTER_MASK  0x00000f00
#define  MEA_IF_COLL_HASH_EVENT_FILTER_GROUP 8
#define  MEA_IF_COLL_HASH_EVENT_HD_MASK      0x0000f000
#define  MEA_IF_COLL_HASH_EVENT_HD_GROUP     12
#define  MEA_IF_COLL_HASH_EVENT_EG_MASK      0x000f0000 
#define  MEA_IF_COLL_HASH_EVENT_EG_GROUP    16




/* MEA_IF_SRV_MAKE0_REG fields */
#define MEA_IF_SRV_PRI_WIDTH                    (MEA_CLS_LARGE_SUPPORT == MEA_FALSE)? (3) : (6) 
#define MEA_IF_SRV_PRI_START                    (0)

#define MEA_IF_SRV_PRI_TYPE_WIDTH                    (MEA_CLS_LARGE_SUPPORT == MEA_FALSE)? (0) : (2) 
#define MEA_IF_SRV_PRI_TYPE_START                    (MEA_IF_SRV_PRI_START + \
                                                      MEA_IF_SRV_PRI_WIDTH )



#define MEA_IF_SRV_SRC_PORT_START               (MEA_IF_SRV_PRI_TYPE_START + \
                                                 MEA_IF_SRV_PRI_TYPE_WIDTH )
#define MEA_IF_SRV_SRC_PORT_WIDTH               (7 + MEA_Platform_GetMaxNumOfPort_BIT())

#define MEA_IF_SRV_NET_TAG_WIDTH                (MEA_CLS_LARGE_SUPPORT == MEA_FALSE)? (17) : (20) 
#define MEA_IF_SRV_NET_TAG_START                (MEA_IF_SRV_SRC_PORT_START + \
                                                 MEA_IF_SRV_SRC_PORT_WIDTH )

#define MEA_IF_SRV_SUB_TYPE_WIDTH                (MEA_CLS_LARGE_SUPPORT == MEA_FALSE)? (0) : (4) 
#define MEA_IF_SRV_SUB_TYPE_START                (MEA_IF_SRV_NET_TAG_START + \
                                                  MEA_IF_SRV_NET_TAG_WIDTH )



#define MEA_IF_SRV_L2_TYPE_WIDTH                (MEA_CLS_LARGE_SUPPORT == MEA_FALSE)? (0) : (5) 
#define MEA_IF_SRV_L2_TYPE_START                (MEA_IF_SRV_SUB_TYPE_START + \
                                                 MEA_IF_SRV_SUB_TYPE_WIDTH )


#define MEA_IF_SRV_NET1_TAG_WIDTH                (MEA_CLS_LARGE_SUPPORT == MEA_FALSE)? (0) : (24) 
#define MEA_IF_SRV_NET1_TAG_START                (MEA_IF_SRV_L2_TYPE_START + \
                                                 MEA_IF_SRV_L2_TYPE_WIDTH )

#define MEA_IF_SRV_IPV6_TAG_WIDTH             (MEA_PARSER_SUPPORT_IPV6 == MEA_FALSE) ? 0 : (27)
#define MEA_IF_SRV_IPV6_TAG_START             (MEA_IF_SRV_NET1_TAG_WIDTH + \
                                               MEA_IF_SRV_NET1_TAG_START)



#define MEA_IF_SRV_CID_WIDTH(unit_i,hwUnit_i)   (MEA_OS_calc_from_size_to_width \
                                                   ((mea_drv_Get_DeviceInfo_NumOfService((unit_i  ),\
                                                                                        (hwUnit_i)))))    //*2
#define MEA_IF_SRV_CID_START                    (MEA_IF_SRV_IPV6_TAG_WIDTH + \
                                                 MEA_IF_SRV_IPV6_TAG_START )

#define MEA_IF_SRV_LAST_WIDTH                   (1)
#define MEA_IF_SRV_LAST_START(unit_i,hwUnit_i)  (MEA_IF_SRV_CID_START + \
                                                 MEA_IF_SRV_CID_WIDTH((unit_i),(hwUnit_i)))


#define MEA_IF_EXTERNAL_CLASS_FILTER_ID          (12)


#define MEA_IF_SRV_INTERNAL_NUMOF_REGS            ((MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ? (2) : ((MEA_PARSER_SUPPORT_IPV6 == MEA_TRUE) ? (4) : 3 ) )



/* MEA_IF_SRV_CONTEX fields */
#define MEA_IF_SRV_ENTRY_UPD_ALLOW_ENABLE_WIDTH                 1    
#define MEA_IF_SRV_ENTRY_LRN_ENA_WIDTH                          1 
#define MEA_IF_SRV_ENTRY_FRW_ENA_WIDTH                          1
#define MEA_IF_SRV_ENTRY_ADM_ENA_WIDTH                          1
#define MEA_IF_SRV_ENTRY_X_PERMISSION_WIDTH(unit_i,hwUnit_i)     MEA_OS_calc_from_size_to_width( mea_drv_Get_DeviceInfo_NumOfXpermission((unit_i),(hwUnit_i)))    //( 10 ) /* Even if xPermission Table length is 512 */
#define MEA_IF_SRV_ENTRY_UPD_ALLOW_SEND_TO_CPU_WIDTH            1
#define MEA_IF_SRV_ENTRY_VPN_ENA_WIDTH                          12
#define MEA_IF_SRV_ENTRY_DA_KEY_TYPE_WIDTH                      4
#define MEA_IF_SRV_ENTRY_SA_KEY_TYPE_WIDTH                      4
#define MEA_IF_SRV_ENTRY_LRN_MASK_TYPE_WIDTH                    ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 0 : 2)
#define MEA_IF_SRV_ENTRY_FILTER_KEY_WIDTH                       ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 0 : 2)
#define MEA_IF_SRV_ENTRY_FILTER_MASK_TYPE_WIDTH                 ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 0 : 2)
#define MEA_IF_SRV_ENTRY_LRN_FILTER_PRI_WIDTH                   ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 0 : 1)
#define MEA_IF_SRV_ENTRY_CFM_OAM_ME_LVL_VALID_WIDTH             ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 1 : 0) 
#define MEA_IF_SRV_ENTRY_CFM_OAM_ME_LVL_VALUE_WIDTH             ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 4 : 0)
#define MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_0		                    1  /*valid key index0*/
#define MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_0                   4  /*valid key index0*/


#define MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_1		                   ((MEA_ACL_HIERARCHICAL_SUPPORT == MEA_TRUE)? 1 : 0)  /*valid key index1*/
#define MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_1                  ((MEA_ACL_HIERARCHICAL_SUPPORT == MEA_TRUE)? 4 : 0 )/*valid key index1*/
#define MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_2		                   ((MEA_ACL_HIERARCHICAL_SUPPORT == MEA_TRUE)? 1 : 0) /*valid key index2*/
#define MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_2                  ((MEA_ACL_HIERARCHICAL_SUPPORT == MEA_TRUE)? 4 : 0) /*valid key index2*/
#define MEA_IF_SRV_ENTRY_DO_ACL_WIDTH_3		                   ((MEA_ACL_HIERARCHICAL_SUPPORT == MEA_TRUE)? 1 : 0 ) /*valid key index3*/
#define MEA_IF_SRV_ENTRY_ACL_KEY_TYPE_WIDTH_3                  ((MEA_ACL_HIERARCHICAL_SUPPORT == MEA_TRUE)? 4 : 0 )/*valid key index3*/

#define MEA_IF_SRV_ENTRY_ACL_MODE_AND_OR                       ((MEA_ACL_HIERARCHICAL_SUPPORT == MEA_TRUE)? 2 : 0 )

#define MEA_IF_SRV_ENTRY_ACL_MASK_PROF_WIDTH                    6

#define MEA_IF_SRV_ENTRY_LIMIT_PROFILE_WIDTH(unit_i,hwUnit_i)   MEA_OS_calc_from_size_to_width(mea_drv_Get_DeviceInfo_NumOfLimiters(unit_i,hwUnit_i))
#define MEA_IF_SRV_ENTRY_FLOOD_UC_WIDTH                         1
#define MEA_IF_SRV_ENTRY_FLOOD_MC_WIDTH 	                    1    
#define MEA_IF_SRV_ENTRY_FLOOD_BC_WIDTH 	                    1
#define MEA_IF_SRV_ENTRY_LXCP_WIDTH(unit_i,hwUnit_i)            MEA_OS_calc_from_size_to_width(mea_drv_Get_DeviceInfo_Lxcp_num_of_prof((MEA_UNIT_0),(hwUnit_i))) 
#define MEA_IF_SRV_ENTRY_REVERSE_ACTION_WIDTH(unit_i,hwUnit_i)  mea_drv_Get_DeviceInfo_GetForwarderLearnActionIdWidth(unit_i,hwUnit_i) 
#define MEA_IF_SRV_ENTRY_REVERSE_ACTION_VALID_WIDTH             1
#define MEA_IF_SRV_ENTRY_REVERSE_PERM_WIDTH(unit_i,hwUnit_i)    mea_drv_Get_DeviceInfo_GetForwarderLearnSrcPortValueWidth(unit_i,hwUnit_i)
#define MEA_IF_SRV_ENTRY_REVERSE_PERM_VALID_WIDTH               1
#define MEA_IF_SRV_ENTRY_L3_L4_VALID_WIDTH                      1
#define MEA_IF_SRV_ENTRY_L4PORT_MASK_VALID_WIDTH                1
#define MEA_IF_SRV_ENTRY_MY_MAC_TO_PPP_PERM_VALID_WIDTH         1
#define MEA_IF_SRV_ENTRY_ACL_WHITE_LIST_ENABLE_WIDTH            1
#define MEA_IF_SRV_ENTRY_SA_NO_MACH_SEND_TO_INTERNAL            1
#define MEA_IF_SRV_ENTRY_INNER_LEARN_FWD_TYPE                   2
#define MEA_IF_SRV_ENTRY_DSE_MASK_FILED_TYPE                    2
#define MEA_IF_SRV_ENTRY_IPMC_AWARE_VALID                       1


#define MEA_IF_SRV_ENTRY_MAC_INTERNAL_PROF_WIDTH(unit_i,hwUnit_i)  ((mea_drv_Get_DeviceInfo_NumOf_FWD_ENB_DA(unit_i,hwUnit_i) == 0) ? 0 : (MEA_OS_calc_from_size_to_width(mea_drv_Get_DeviceInfo_NumOf_FWD_ENB_DA(unit_i,hwUnit_i)))  )
#define MEA_IF_SRV_ENTRY_MAC_INTERNAL_VALID_WIDTH(unit_i,hwUnit_i) ((mea_drv_Get_DeviceInfo_NumOf_FWD_ENB_DA(unit_i,hwUnit_i) == 0 ) ? 0   : 1 )

#define MEA_IF_SRV_ENTRY_ACCESS_PORT_ID_WIDTH                   ((MEA_VPLS_ACCESS_PORT_SUPPORT == MEA_TRUE) ? 10  : 0)
#define MEA_IF_SRV_ENTRY_ACCESS_ADM_EN_WIDTH                    ((MEA_VPLS_ACCESS_PORT_SUPPORT == MEA_TRUE) ? 1   : 0)
#define MEA_IF_SRV_ENTRY_ACCESS_LOCAL_SW_WIDTH                  ((MEA_VPLS_ACCESS_PORT_SUPPORT == MEA_TRUE) ? 1   : 0)

#define MEA_IF_SRV_ENTRY_INGRESS_FLOW_POL(unit_i,hwUnit_i)       ((MEA_IS_INGRESS_FLOW_POLICER_SUPPORT == MEA_TRUE) ? MEA_OS_calc_from_size_to_width( mea_drv_Get_DeviceInfo_Ingress_flow_Policer_profile((unit_i),(hwUnit_i))) : (0))

#define MEA_IF_SRV_ENTRY_ACL_PROF_VALID_WIDTH                       1
#define MEA_IF_SRV_ENTRY_ACL_PROF_WIDTH                             6
#define MEA_IF_SRV_ENTRY_HPM_MASK_PROF_WIDTH                        ((MEA_HPM_SUPPORT == MEA_TRUE) ? (MEA_OS_calc_from_size_to_width(MEA_TFT_MASK_MAX_PROFILE)) : 0)
#define MEA_IF_SRV_ENTRY_HPM_PROF_WIDTH                             ((MEA_HPM_SUPPORT == MEA_TRUE) ? 6 : 0)
#define MEA_IF_SRV_ENTRY_UEPDN_WIDTH                                ((MEA_HPM_SUPPORT == MEA_TRUE) ? (MEA_OS_calc_from_size_to_width(MEA_UEPDN_MAX_HW_ID)) : 0)
#define MEA_IF_SRV_ENTRY_FWD_SELECT_KEY                             1
#define MEA_IF_SRV_ENTRY_SEGMENT_SWITCH_PROF                        ((MEA_SEGMENT_SWITCH_SUPPORT == MEA_TRUE) ? 2 : 0)
#define MEA_IF_SRV_ENTRY_HPM_IPV6_KEY                               ((MEA_HPM_NEW_SUPPORT == MEA_TRUE) ? 2 : 0)

#define MEA_IF_SRV_ACL5_KEY_TYPE									((MEA_ACL5_SUPPORT == MEA_TRUE) ? 4 : 0)
#define MEA_IF_SRV_ACL5_INNER_FRAM									((MEA_ACL5_SUPPORT == MEA_TRUE) ? 1 : 0)
#define MEA_IF_SRV_ACL5_IP_MASK_PROF								((MEA_ACL5_SUPPORT == MEA_TRUE) ? 9 : 0)
#define MEA_IF_SRV_ACL5_KEY_MASK_PROF								((MEA_ACL5_SUPPORT == MEA_TRUE) ? 9 : 0)
#define MEA_IF_SRV_ACL5_RANGE_PROF									((MEA_ACL5_SUPPORT == MEA_TRUE) ? 9 : 0)
#define MEA_IF_SRV_ACL5_DO											((MEA_ACL5_SUPPORT == MEA_TRUE) ? 1 : 0)
#define MEA_IF_SRV_LPM_VRF                                          ((MEA_LPM_SUPPORT == MEA_TRUE)  ? 3 : 0)









/*VLAN_RANGE MASK*/
#define MEA_IF_SRV_VLAN_RANGE_NETTAG_START                    (0)
#define MEA_IF_SRV_VLAN_RANGE_NETTAG_WIDTH                    (MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ?(12): 24 


#define MEA_IF_SRV_VLAN_RANGE_NETTAG_TO_START    (MEA_IF_SRV_VLAN_RANGE_NETTAG_START + \
                                                 MEA_IF_SRV_VLAN_RANGE_NETTAG_WIDTH )
#define MEA_IF_SRV_VLAN_RANGE_NETTAG_TO_WIDTH                 (MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ?(12): 24  


#define MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_FROM_START    (MEA_IF_SRV_VLAN_RANGE_NETTAG_TO_START + \
                                                          MEA_IF_SRV_VLAN_RANGE_NETTAG_TO_WIDTH )
#define MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_FROM_WIDTH                    (MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ?(12): 24  

#define MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_TO_START    (MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_FROM_START + \
                                                        MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_FROM_WIDTH )
#define MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_TO_WIDTH                    (MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ?(12): 24  



#define MEA_IF_SRV_VLAN_RANGE_PRI_START    (MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_TO_START + \
                                            MEA_IF_SRV_VLAN_RANGE_INNER_NETTAG_TO_WIDTH )

#define MEA_IF_SRV_VLAN_RANGE_PRI_WIDTH                         (MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ?(6): (8)


#define MEA_IF_SRV_VLAN_RANGE_PRI_TO_START    (MEA_IF_SRV_VLAN_RANGE_PRI_START + \
                                               MEA_IF_SRV_VLAN_RANGE_PRI_WIDTH )

#define MEA_IF_SRV_VLAN_RANGE_PRI_TO_WIDTH                       (MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ?(6): (8)

#define MEA_IF_SRV_VLAN_RANGE_L2TYPE_START (MEA_IF_SRV_VLAN_RANGE_PRI_TYPE_START + \
                                            MEA_IF_SRV_VLAN_RANGE_PRI_TYPE_WIDTH )

#define MEA_IF_SRV_VLAN_RANGE_L2TYPE_WIDTH                       (MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ?(0): (5)

#define MEA_IF_SRV_VLAN_RANGE_PTR_CONTEX_START     (MEA_IF_SRV_VLAN_RANGE_L2TYPE_START + \
                                                    MEA_IF_SRV_VLAN_RANGE_L2TYPE_WIDTH )
#if MEA_DBG_ACTION
#define MEA_IF_SRV_VLAN_RANGE_PTR_CONTEX_WIDTH(unit_i,hwUnit_i)             (MEA_OS_calc_from_size_to_width (mea_drv_Get_DeviceInfo_NumOfService((unit_i),(hwUnit_i)) * 2) )
#else
#define MEA_IF_SRV_VLAN_RANGE_PTR_CONTEX_WIDTH(unit_i,hwUnit_i)             (MEA_OS_calc_from_size_to_width (mea_drv_Get_DeviceInfo_NumOfService((unit_i),(hwUnit_i)) ) )
#endif

#define MEA_IF_SRV_VLAN_RANGE_VALID_START     (MEA_IF_SRV_VLAN_RANGE_RESERVE_START + \
                                                 MEA_IF_SRV_VLAN_RANGE_RESERVE_WIDTH )
#define MEA_IF_SRV_VLAN_RANGE_VALID_WIDTH                          (1)



/************************************************************************/
/* If_service_Event                                                     */
/************************************************************************/

#define MEA_IF_SRV_EVENT_errors_exactMatch_fifo_WIDTH               (1)
#define MEA_IF_SRV_EVENT_errors_range_fifo_WIDTH                    (1)
#define MEA_IF_SRV_EVENT_exactMatchService_hit_WIDTH                (1)
#define MEA_IF_SRV_EVENT_rangeMatchService_hit_WIDTH                (1)
#define MEA_IF_SRV_EVENT_last_service_ctx_id_WIDTH                  (13)
#define MEA_IF_SRV_EVENT_range_service_last_key_net_tag1_WIDTH      (24)
#define MEA_IF_SRV_EVENT_range_service_last_key_net_tag2_WIDTH      (24)
#define MEA_IF_SRV_EVENT_range_service_last_key_pri_WIDTH           (8)
#define MEA_IF_SRV_EVENT_range_service_last_key_l2type_WIDTH        (5)
#define MEA_IF_SRV_EVENT_range_service_Reserv_WIDTH                 (12)

#define MEA_IF_SRV_EVENT_unmatch_pri_WIDTH                          (8)
#define MEA_IF_SRV_EVENT_unmatch_src_port_WIDTH                   ( 7 + MEA_Platform_GetMaxNumOfPort_BIT())
#define MEA_IF_SRV_EVENT_unmatch_net_tag1_WIDTH                     (20)
#define MEA_IF_SRV_EVENT_unmatch_SubType                            (4)
#define MEA_IF_SRV_EVENT_unmatch_L2_type_WIDTH                      (5)
#define MEA_IF_SRV_EVENT_unmatch_net_tag2_WIDTH                     (24)
#define MEA_IF_SRV_EVENT_unmatch_out_src_port_WIDTH               ( 7 + MEA_Platform_GetMaxNumOfPort_BIT())
#define MEA_IF_SRV_EVENT_unmatch_last_unmatch_VALID                 1
#define MEA_IF_SRV_EVENT_No_match_all_entry                         1
#define MEA_IF_SRV_EVENT_match_External_srv                         1



/************************************************************************/
/*                                                                      */
/************************************************************************/

#define MEA_IF_SRV_EVENT_EXTNAL_service_last_key_pri_WIDTH					(6)
#define MEA_IF_SRV_EVENT_EXTNAL_service_last_key_priType_WIDTH				(2)
#define MEA_IF_SRV_EVENT_EXTNAL_service_last_src_port_WIDTH					( 7 + MEA_Platform_GetMaxNumOfPort_BIT())
#define MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag_WIDTH				(20)
#define MEA_IF_SRV_EVENT_EXTNAL_service_last_key_subType_WIDTH				(4)
#define MEA_IF_SRV_EVENT_EXTNAL_service_last_key_l2type_WIDTH				(5)
#define MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag1_WIDTH				(24)
#define MEA_IF_SRV_EVENT_EXTNAL_service_last_key_net_tag2_WIDTH             (MEA_IPV6_CLASS_SUPPORT == MEA_TRUE) ? (27) : (0)
#define MEA_IF_SRV_EVENT_EXTNAL_service_last_sid							(20) 










/************************************************************************/
/*                                                                      */
/************************************************************************/

/*MEA_IF_VSP_MAKE fields */	
#define MEA_IF_VSP_VSP0_MASK0           		    0x000000fF
#define MEA_IF_VSP_VSP0SOP_MASK0           		    0x00000100
#define MEA_IF_VSP_VSP1_MASK0           		    0x0001fe00
#define MEA_IF_VSP_VSP1SOP_MASK0           		    0x00020000
#define MEA_IF_VSP_VSP2_MASK0           		    0x03fc0000
#define MEA_IF_VSP_VSP2SOP_MASK0           		    0x04000000
#define MEA_IF_VSP_VSP3_MASK0           		    0xf8000000
#define MEA_IF_VSP_VSP3_MASK1           		    0x00000007
#define MEA_IF_VSP_VSP3SOP_MASK1           		    0x00000008



#define MEA_IF_ACTION_MAKE0_EDIT_ID_WIDTH(unit_i,hwUnit_i)  (mea_drv_Get_DeviceInfo_EHPs_width(unit_i,hwUnit_i))
#define MEA_IF_ACTION_MAKE0_PROTOCOL_WIDTH                  ((MEA_ACTION_PROTOCOL_LLC_REMOVE_BIT == MEA_TRUE) ? (2)  : (0))
#define MEA_IF_ACTION_MAKE0_LLC_EXIST_WIDTH                ( (MEA_ACTION_PROTOCOL_LLC_REMOVE_BIT == MEA_TRUE) ? (1)  : (0))

#define MEA_IF_ACTION_MAKE0_FRAGMENT_WIDTH                  ((MEA_REMOVE_BIT_HW_ACTION   == MEA_TRUE) ? (1) :(0) )

#define MEA_IF_ACTION_MAKE0_AFDX_EXTRACT_SESSION_WIDTH  ((MEA_AFDX_SUPPORT==MEA_TRUE) ? (1) : (0)) 
#define MEA_IF_ACTION_MAKE0_AFDX_SESSION_ID_WIDTH       ((MEA_AFDX_SUPPORT==MEA_TRUE) ? (8) : (0)) 
#define MEA_IF_ACTION_MAKE0_AFDX_VALID_WIDTH            ((MEA_AFDX_SUPPORT==MEA_TRUE) ? (1) : (0)) 


#define MEA_IF_ACTION_MAKE0_PM_ID_WIDTH(unit_i,hwUnit_i)    (mea_drv_Get_DeviceInfo_PmWidth(unit_i,hwUnit_i))
#define MEA_IF_ACTION_MAKE0_TM_ID_WIDTH(unit_i,hwUnit_i)    (MEA_OS_calc_from_size_to_width(mea_drv_Get_DeviceInfo_NumOfPolicersPtr((unit_i),(hwUnit_i))))
#define MEA_IF_ACTION_MAKE0_COS_WIDTH						3
#define MEA_IF_ACTION_MAKE1_COS_FORCE_WIDTH					1
#define MEA_IF_ACTION_MAKE1_COLOR_WIDTH						2
#define MEA_IF_ACTION_MAKE1_COLOR_FORCE_WIDTH				1
#define MEA_IF_ACTION_MAKE1_L2_PRI_WIDTH					3
#define MEA_IF_ACTION_MAKE1_L2_PRI_FORCE_WIDTH				1
#define MEA_IF_ACTION_MAKE1_MC_FID_SKIP_WIDTH				1
#define MEA_IF_ACTION_MAKE1_MC_FID_EDIT_WIDTH				((MEA_REMOVE_BIT_HW_ACTION   == MEA_TRUE) ? (3) : (0))
#define MEA_IF_ACTION_MAKE1_ED_ID_FORCE_WIDTH				1
#define MEA_IF_ACTION_MAKE1_PM_ID_FORCE_WIDTH				1
#define MEA_IF_ACTION_MAKE1_TM_ID_FORCE_WIDTH				1
#define MEA_IF_ACTION_MAKE1_FORCE_MARKING_MAP_WIDTH        ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 1 : 0)
#define MEA_IF_ACTION_MAKE1_FORCE_COS_MAP_WIDTH            ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 1 : 0)
#define MEA_IF_ACTION_MAKE1_INGRESS_TS_TYPE_WIDTH             1
#define MEA_IF_ACTION_MAKE1_l2_pri_prof_b5                    1

#define MEA_IF_ACTION_MAKE1_TDM_PACKET_TYPE_WIDTH           ((MEA_TDM_ACT_WITH_DISABLE == MEA_FALSE) ? (1) : (0) )
#define MEA_IF_ACTION_MAKE1_TDM_REMOVE_BYTE_WIDTH           ((MEA_TDM_ACT_WITH_DISABLE == MEA_FALSE) ? (4) : (0) )
#define MEA_IF_ACTION_MAKE1_TDM_REMOVE_LINE_WIDTH           ((MEA_TDM_ACT_WITH_DISABLE == MEA_FALSE) ? (3) : (0) )
#define MEA_IF_ACTION_MAKE1_TDM_CES_TYPE_WIDTH              ((MEA_TDM_ACT_WITH_DISABLE == MEA_FALSE) ? (2) : (0) )
#define MEA_IF_ACTION_MAKE1_TDM_USED_VLAN_WIDTH             ((MEA_TDM_ACT_WITH_DISABLE == MEA_FALSE) ? (1) : (0) )
#define MEA_IF_ACTION_MAKE1_TDM_USED_RTP_WIDTH              ((MEA_TDM_ACT_WITH_DISABLE == MEA_FALSE) ? (1) : (0) )
#define MEA_IF_ACTION_MAKE1_WRED_PROF_WIDTH                   3
#define MEA_IF_ACTION_MAKE1_SET1588_WIDTH                     1
#define MEA_IF_ACTION_MAKE1_MTU_ID_WIDTH                      ((MEA_ACT_MTU_SUPPORT==MEA_TRUE) ? (9) : (0)) 
#define MEA_IF_ACTION_MAKE1_LAG_BYPASS_WIDTH                 1
#define MEA_IF_ACTION_MAKE1_FLOOD_ED_WIDTH                   1
#define MEA_IF_ACTION_MAKE1_DROP_WIDTH                       1
#define MEA_IF_ACTION_MAKE1_cluster_id(hwUnit_i)            (MEA_OS_calc_from_size_to_width( mea_drv_Get_DeviceInfo_NumOfClusters((MEA_UNIT_0),(hwUnit_i))))
#define MEA_IF_ACTION_MAKE1_cluster_id_valid                 1
#define MEA_IF_ACTION_MAKE1_FRAG_IP                         ((MEA_IP_FRAGMENT_SUPPORT)? 1 : 0) 
#define MEA_IF_ACTION_MAKE1_IP_TUNNEL                       ((MEA_IP_FRAGMENT_SUPPORT)? 1 : 0) 
#define MEA_IF_ACTION_MAKE1_DEFRAG_EXT_IP                   ((MEA_IP_FRAGMENT_SUPPORT)? 1 : 0) 
#define MEA_IF_ACTION_MAKE1_DEFRAG_INT_IP                   ((MEA_IP_FRAGMENT_SUPPORT)? 1 : 0) 
#define MEA_IF_ACTION_MAKE1_DEFRAG_Reassembly_L2            ((MEA_IP_FRAGMENT_SUPPORT)? 3 : 0) 
#define MEA_IF_ACTION_MAKE1_FRAG_IP_EXTERNAL_INTERNAL       ((MEA_IP_FRAGMENT_SUPPORT)? 1 : 0) 
#define MEA_IF_ACTION_MAKE1_FRAG_IP_TARGET_MTU              ((MEA_TARGET_MTU_SUPPORT) ? 2 : 0) 
#define MEA_IF_ACTION_MAKE1_fwd_win                         1
#define MEA_IF_ACTION_MAKE1_SFLOW_TYPE                      ((MEA_SFLOW_SUPPORT) ? 2 : 0)
#define MEA_IF_ACTION_MAKE1_overRule_bfd_sp                 ((MEA_BFD_SUPPORT) ? 1 : 0)

/*
cluster_over_rule : num of cluster
cluster_over_rule_valid   :1


*/









/* MEA_IF_STATUS_REG fields */
#define MEA_IF_STATUS_BUSY_MASK                       0x00000001		
#define MEA_IF_STATUS_COLLISION_MASK                  0x00000002
#define MEA_IF_STATUS_MASK                            0x00000003
#define MEA_IF_STATUS_FWD_TBL_INIT_BUSY_MASK          0x00000004
#define MEA_IF_STATUS_FWD_CMD_RESPONSE_BUSY_MASK      0x00000008
#define MEA_IF_STATUS_FWD_CMD_RESPONSE_COLLISION_MASK 0x00000010
#define MEA_IF_STATUS_FILTER_BUSY_MASK                0x00000020
#define MEA_IF_STATUS_FILTER_COLLISION_MASK           0x00000040
#define MEA_IF_STATUS_HC_BUSY_MASK                    0x00000080
#define MEA_IF_STATUS_HC_COLLISION_MASK               0x00000100


#define MEA_IF_STATUS_EGR_CLASS_BUSY_MASK             0x00000200
#define MEA_IF_STATUS_EGR_CLASS_COLLISION_MASK        0x00000400
#define MEA_IF_STATUS_EVC_CLASS_BUSY_MASK             0x00000800
#define MEA_IF_STATUS_EVC_CLASS_COLLISION_MASK        0x00001000



#define MEA_IF_STATUS_RX_PORT_CONTROL_MASK            0x0001fe00 /* SSSMII Control */

/* MEA_IF_CMD_PARAM_REG fields */
#define MEA_IF_CMD_PARAM_ENTRY_MASK		   	 	0x00FFFFFF
#define MEA_IF_CMD_PARAM_TBL_TYP_MASK      	 	0x7F000000
#define MEA_IF_CMD_PARAM_RW_MASK   		   		0x80000000

/* MEA_IF_CMD_PARAM_TBL_TYP values */
#include "mea_device_regs.h"



/* MEA_IF_CMD_PARAM_RW values */
#define MEA_IF_CMD_PARAM_RW_READ                0x0000001
#define MEA_IF_CMD_PARAM_RW_WRITE               0x0000000
/* EVENTS offset */ 
/* MEA_IF_CMD_PARAM_ENTRY values for MEA_IF_CMD_PARAM_TBL_TYP_REG */







/* MEA counter of drop packet because of minimum packet size 
   Note: Currently global counter : IF tbl #3 entry 1 
         In 3.5.1.7 it should be change to per port counter 
         as part of if table #1 */
#define MEA_IF_PARSER_COUNTERS_MIN_PACKET_MASK		                        0xFFFFFFFF


/* MEA IF Counters Table entry definition */
#define MEA_IF_PARSER_COUNTERS_MISS_DISCARD_MASK		                    0x000003FF
#define MEA_IF_PARSER_COUNTERS_MEA_L2CP_ACTION_DISCARD_MASK                 0x000FFC00
#define MEA_IF_PARSER_COUNTERS_OAM_DISCARD_MASK		                        0x3FF00000
#define MEA_IF_PARSER_COUNTERS_RESERVED_MASK		                        0xC0000000 
#define MEA_IF_PARSER_COUNTERS_RX_FIFO_FULL_DISCARD_MASK                    0x0000FFFF


/* MEA IF Pre Parser Table entry definition */
#define MEA_IF_PRE_PARSER_TABLE_REG0_NET_TAG_MASK       0x00ffffff /* 0-23 */
#define MEA_IF_PRE_PARSER_TABLE_REG0_NET_TAG_MASK_MASK  0xff000000 /* 24-31 */
#define MEA_IF_PRE_PARSER_TABLE_REG1_NET_TAG_MASK_MASK  0x0000ffff /* 32-47 */
#define MEA_IF_PRE_PARSER_TABLE_REG1_VALID_MASK         0x00010000 /* 48-48 */
#define MEA_IF_PRE_PARSER_TABLE_REG1_VSP_TYPE_MASK      0x00060000 /* 49-50 */


/* MEA IF Mapping Table entry definition */
#define MEA_IF_MAPPING_TABLE_CLASS_PRI_MASK       0x00000007 /* 0-2 */
#define MEA_IF_MAPPING_TABLE_PRI_Q_MASK           0x00000038 /* 3-5 */
#define MEA_IF_MAPPING_TABLE_STAMP_PRI_MASK       0x000001c0 /* 6-8 */
#define MEA_IF_MAPPING_TABLE_COLOR_MASK           0x00000600 /* 9-10 */
#define MEA_IF_MAPPING_TABLE_CLASS_PRI_VALID_MASK 0x00000800 /* 11-11 */
#define MEA_IF_MAPPING_TABLE_PRI_Q_VALID_MASK     0x00001000 /* 12-12 */
#define MEA_IF_MAPPING_TABLE_STAMP_PRI_VALID_MASK 0x00002000 /* 13-13 */
#define MEA_IF_MAPPING_TABLE_COLOR_VALID_MASK     0x00004000 /* 14-14 */


/* MEA IF CAM Table entry definition */
#define MEA_IF_CAM_TABLE_ETHERTYPE_MASK           0x0000ffff  /* 0-15 */
#define MEA_IF_CAM_TABLE_PRI_MASK                 0x00070000  /* 16-18 */
#define MEA_IF_CAM_TABLE_L2CP_MASK                0x01f80000  /* 19-24 */
#define MEA_IF_CAM_TABLE_VALID_MASK               0x02000000  /* 25-25 */

/* MEA IF Default SID entry (within l2cp) definition */
#define MEA_IF_DEFAULT_SID_SID_MASK               0x000001ff  /* 0-8 */
#define MEA_IF_DEFAULT_SID_ACTION_MASK            0x00000600  /* 9-10 */

/* MEA IF RMON RX Table entry definition */
#define MEA_IF_RMON_REG_RX_PKTS_MASK               0xFFFFFFFF
#define MEA_IF_RMON_REG_RX_BYTES_MASK              0x0000FFFF  /*not support*/

/* MEA IF RMON TX Table entry definition */
#define MEA_IF_RMON_REG_TX_PKTS_MASK               0xFFFFFFFF
#define MEA_IF_RMON_REG_TX_BYTES_MASK              0x0000FFFF /*not support*/

#define MEA_IF_RMON_REG_RX_CRC_ERR_MASK            0xFFFFFFFF



/* MEA IF XPERMISSION Table entry definition */
#define MEA_IF_XPERMISSION_WORD_0_PORTS_0_31_MASK     0xffffffff     
#define MEA_IF_XPERMISSION_WORD_1_PORTS_32_63_MASK    0xffffffff
#define MEA_IF_XPERMISSION_WORD_2_PORTS_64_79_MASK    0x0000ffff 
#define MEA_IF_XPERMISSION_WORD_2_PORT_125_MASK       0x00010000
#define MEA_IF_XPERMISSION_WORD_2_PORT_126_MASK       0x00020000
#define MEA_IF_XPERMISSION_WORD_2_PORT_127_MASK       0x00040000
#define MEA_IF_XPERMISSION_WORD_2_NUM_OF_PORTS_MASK   0x03f80000 

/* MEA IF FILTER MASK PROFILE Table entry definition */
#define MEA_IF_FILTER_MASK_PROF_WORD_0_FIELD0_MASK         0x0000ffff
#define MEA_IF_FILTER_MASK_PROF_WORD_0_FIELD1_MASK         0xffff0000
#define MEA_IF_FILTER_MASK_PROF_WORD_1_FIELD0_VALID_MASK   0x00000001
#define MEA_IF_FILTER_MASK_PROF_WORD_1_FIELD1_VALID_MASK   0x00000002
#define MEA_IF_FILTER_MASK_PROF_WORD_2_FIELD2_MASK         0x0000ffff
#define MEA_IF_FILTER_MASK_PROF_WORD_2_KEY_TYPE_MASK       0x000f0000
#define MEA_IF_FILTER_MASK_PROF_WORD_2_SRC_PORT_MASK       0x07f00000
#define MEA_IF_FILTER_MASK_PROF_WORD_2_DA_MASK             0x08000000
#define MEA_IF_FILTER_MASK_PROF_WORD_3_FIELD2_VALID_MASK   0x00000001
#define MEA_IF_FILTER_MASK_PROF_WORD_3_SRC_PORT_VALID_MASK 0x00000002

/* MEA IF FILTER CTX Table entry definition */
#define MEA_IF_FILTER_WORD_0_XPERMISSION_ID_WIDTH(hwUnit_i)           (MEA_OS_calc_from_size_to_width(ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH((hwUnit_i))))
#define MEA_IF_FILTER_WORD_0_XPERMISSION_VALID_WIDTH        1
#define MEA_IF_FILTER_WORD_0_AID_VALID_WIDTH                1
#define MEA_IF_FILTER_WORD_0_LX_WIN_VALID_WIDTH             1
#define MEA_IF_FILTER_WORD_0_HPM_COS_WIDTH                  ((MEA_HPM_SUPPORT) ? 3 : 0  ) 
#define MEA_IF_FILTER_WORD_0_HPM_COS_VALID_WIDTH            ((MEA_HPM_SUPPORT) ? 1 : 0  ) 

#define MEA_IF_FILTER_WORD_0_ACTION_ID                      (MEA_OS_calc_from_size_to_width(MEA_MAX_NUM_OF_FILTER_ACTIONS))
#define MEA_IF_FILTER_WORD_0_FWD_EN                         1
#define MEA_IF_FILTER_WORD_0_FWD_EN_INTERNAL_MAC            1
#define MEA_IF_FILTER_WORD_0_FWD_KEY                        4
#define MEA_IF_FILTER_WORD_0_FWD_FORCE                      1
#define MEA_IF_FILTER_WORD_0_FWD_DSE_MASK_FILED_TYPE        2

/* MEA IF FILTER HASH Table entry definition */
#define MEA_IF_FILTER_WORD_0_FIELD0_MASK                   0x0000ffff
#define MEA_IF_FILTER_WORD_0_FIELD1_MASK                   0xffff0000
#define MEA_IF_FILTER_WORD_1_FIELD2_MASK                   0x0000ffff
#define MEA_IF_FILTER_WORD_1_KEY_TYPE_MASK                 0x000f0000
#define MEA_IF_FILTER_WORD_1_SRC_PORT_SID_MASK             0xfff00000
   



#define MEA_IF_FILTER_WORD_2_FILTER_ID_MASK           0x000ffff 
#define MEA_IF_FILTER_WORD_2_LAST_RESERV                0x1<<10
#define MEA_IF_FILTER_WORD_2_LAST_ENTRY_MASK            0x1<<11

#define MEA_IF_XPERMISSION_WORD_1_PORTS_32_63_MASK    0xffffffff
#define MEA_IF_XPERMISSION_WORD_2_PORTS_64_79_MASK    0x0000ffff 
#define MEA_IF_XPERMISSION_WORD_2_PORT_125_MASK       0x00010000
#define MEA_IF_XPERMISSION_WORD_2_PORT_126_MASK       0x00020000
#define MEA_IF_XPERMISSION_WORD_2_PORT_127_MASK       0x00040000
#define MEA_IF_XPERMISSION_WORD_2_NUM_OF_PORTS_MASK   0x03f80000 



/* MEA_IF_CONTROL_REG */
/* T.B.D. - Add this flags to globals , and add defaults in platform */
#define MEA_IF_CONTROL_TX_PORT_125_ADD_CRC_MASK         0x00000001 /* default '1'b */
#define MEA_IF_CONTROL_TX_PORT_126_ADD_CRC_MASK         0x00000002 /* default '1'b */
#define MEA_IF_CONTROL_TX_PORT_127_ADD_CRC_MASK         0x00000004 /* default '1'b */
#define MEA_IF_CONTROL_RESERVED1                        0x00000008 /* default '0'b */
#define MEA_IF_CONTROL_RX_PORT_125_REM_CRC_MASK         0x00000010 /* default '1'b */
#define MEA_IF_CONTROL_RX_PORT_126_REM_CRC_MASK         0x00000020 /* default '1'b */
#define MEA_IF_CONTROL_RX_PORT_127_REM_CRC_MASK         0x00000040 /* default '1'b */
#define MEA_IF_CONTROL_RESERVED2                        0x00000080 /* default '0'b */
#define MEA_IF_CONTROL_MII_PORT_125_MASK                0x00000100 /* default '0'b */
#define MEA_IF_CONTROL_MII_PORT_126_MASK                0x00000200 /* default '0'b */
#define MEA_IF_CONTROL_MII_PORT_127_MASK                0x00000400 /* default '1'b */
#define MEA_IF_CONTROL_RESERVED3                        0x00000800 /* default '0'b */
#define MEA_IF_CONTROL_REMOVE_ATM_HEADER_MASK           0x00001000 /* default '1'b */
#define MEA_IF_CONTROL_RESERVED4                        0x00002000 /* default '0'b */
#define MEA_IF_CONTROL_RESERVED5                        0x00004000 /* default '0'b */
#define MEA_IF_CONTROL_IGNORE_BM_MASK                   0x00008000 /* default '0'b */
#define MEA_IF_CONTROL_RESERVED6                        0x7fff0000 /* default 0x0000 */
#define MEA_IF_CONFIG_ENABLE_MASK                       0x80000000



/* MEA_IF_VER_LX_VERSION_L_REG  MASK's*/
#define MEA_IF_VER_LX_VERSION_L_PRODUCT_NUMBER_MASK         0xFFFF0000
#define MEA_IF_VER_LX_VERSION_L_PHASE_VERSION_MASK          0x0000F000
#define MEA_IF_VER_LX_VERSION_L_RELEASE_NUMBER_MASK         0x00000Ff0
#define MEA_IF_VER_LX_VERSION_L_VLSI_INT_VERSION_MASK       0x0000000F

/* MEA_IF_VER_LX_VERSION_H_REG MASK's */
#define MEA_IF_VER_LX_VERSION_H_SYSTYPE_MASK                0xFC000000
#define MEA_IF_VER_LX_VERSION_H_FORWARDER_ADDR_WIDTH_MASK   0x03F00000    
#define MEA_IF_VER_LX_VERSION_H_TECHNOLOGYTYPE_MASK         0x000F0000    
#define MEA_IF_VER_LX_VERSION_H_DPDDR_MASK                  0x0000FF00
#define MEA_IF_VER_LX_VERSION_H_SYSCLK_MASK                 0x000000FF


#define MEA_IF_VER_LX_VERSION_H_SYSTYPE_ENET4000_VAL        4

#if defined(MEA_OS_ETH_PROJ_1) || defined(MEA_OS_TDN1)
#define MEA_ENET4000_BM_OFFSET_VAL                          0x04000 
#else
#define MEA_ENET4000_BM_OFFSET_VAL                          0x08000 
#endif

//for dual core
#define MEA_ENET4000_IF_DOWNSTREEM_OFFSET_VAL               0x10000
#define MEA_ENET4000_BM_DOWNSTREEM_OFFSET_VAL               0x18000


#define MEA_IF_VER_LX_VERSION_H_SYSTYPE_SINGLECORE_VAL        5



 /* MEA_IF_VER_PORT_MAPPING_L_REG  MASK*/
#define MEA_IF_VER_PORT_MAPPING_L_REG_GROUP0_MASK                   0x0000000F
#define MEA_IF_VER_PORT_MAPPING_L_REG_GROUP1_MASK                   0x000000F0
#define MEA_IF_VER_PORT_MAPPING_L_REG_GROUP2_MASK                   0x00000f00
#define MEA_IF_VER_PORT_MAPPING_L_REG_GROUP3_MASK                   0x0000F000
#define MEA_IF_VER_PORT_MAPPING_L_REG_GROUP4_MASK                   0x000f0000
#define MEA_IF_VER_PORT_MAPPING_L_REG_GROUP5_MASK                   0x00F00000
#define MEA_IF_VER_PORT_MAPPING_L_REG_GROUP6_MASK                   0x0F000000
#define MEA_IF_VER_PORT_MAPPING_L_REG_GROUP7_MASK                   0xF0000000

/* MEA_IF_VER_PORT_MAPPING_H_REG  MASK*/                        
#define MEA_IF_VER_PORT_MAPPING_H_REG_GROUP8_MASK                    0x0000000F
#define MEA_IF_VER_PORT_MAPPING_H_REG_GROUP9_MASK                    0x000000F0
#define MEA_IF_VER_PORT_MAPPING_H_REG_GROUP10_MASK                   0x00000f00
#define MEA_IF_VER_PORT_MAPPING_H_REG_GROUP11_MASK                   0x0000F000
#define MEA_IF_VER_PORT_MAPPING_H_REG_GROUP_RESERVE_MASK             0xFFFF0000


/* MEA_IF_LIMITER_MASK  MASK*/                        
#define MEA_IF_LIMITER_CURRENT_LIMIT_MASK1                           0x0000FFFF
#define MEA_IF_LIMITER_CONFIG_LIMIT_MASK1                            0xFFFF0000
#define MEA_IF_LIMITER_ACTION_MASK2                                  0x00000003
#define MEA_IF_LIMITER_RES1_MASK2                                    0x0000000C
#define MEA_IF_LIMITER_UPDATE_CURRENT_MASK2                          0x00000010  /*1- update current , 0- dont update current */

/* MEA_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION MASK*/
#ifdef MEA_NEW_FWD_ROW_SET
#define MEA_IF_LXCP_ACTION_H_REG_ACT_ID_WIDTH(unit_i, hwUnit_i)      (MEA_IF_SRV_ENTRY_REVERSE_ACTION_WIDTH((unit_i), (hwUnit_i))  ) 
#else
#define MEA_IF_LXCP_ACTION_H_REG_ACT_ID_WIDTH(unit_i, hwUnit_i)       (12)
#endif

#define MEA_IF_LXCP_ACTION_H_REG_ACT_FORCE_WIDTH    1
#define MEA_IF_LXCP_ACTION_H_REG_PER_ID_WIDTH(hwUnit_i)  (MEA_OS_calc_from_size_to_width(ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH((hwUnit_i))))
#define MEA_IF_LXCP_ACTION_H_REG_PER_FORCE_WIDTH    1

#define MEA_IF_LXCP_WinWhiteList_H_REG_PER_WIDTH 1


/* MEA_IF_CMD_PARAM_TBL_TYP_LXCP */
#define MEA_IF_LXCP_PROF_WIDTH(hwUnit_i)         (MEA_OS_calc_from_size_to_width((MEA_Platform_Get_DeviceInfo_NumOfLxcpAction((hwUnit_i))))) 


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  MEA FWD table definitions                                                    */
/*                                                                               */
/*-------------------------------------------------------------------------------*/


#define MEA_FWD_TABLE_READ_CMD       0x6
#define MEA_FWD_TABLE_READ_TBL_ID(key_type) (key_type) 
#define MEA_FWD_TABLE_READ_PRI       0
#define MEA_FWD_TABLE_READ_CAM_MEM   0
#define MEA_FWD_TABLE_WRITE_CMD      0x7
#define MEA_FWD_TABLE_WRITE_TBL_ID(key_type) (key_type)
#define MEA_FWD_TABLE_WRITE_PRI      0
#define MEA_FWD_TABLE_WRITE_CAM_MEM  0
#define MEA_FWD_TABLE_SEARCH_CMD     0x1
#define MEA_FWD_TABLE_SEARCH_TBL_ID(key_type) (key_type)
#define MEA_FWD_TABLE_SEARCH_PRI     0
#define MEA_FWD_TABLE_ADD_CMD        0x2
#define MEA_FWD_TABLE_ADD_TBL_ID(key_type)     (key_type)
#define MEA_FWD_TABLE_ADD_PRI        0
#define MEA_FWD_TABLE_ADD_AGE_MIN    1
#define MEA_FWD_TABLE_UPDATE_CMD     0x3
#define MEA_FWD_TABLE_UPDATE_TBL_ID(key_type) (key_type)
#define MEA_FWD_TABLE_UPDATE_PRI     0
#define MEA_FWD_TABLE_UPDATE_AGE_MIN 1
#define MEA_FWD_TABLE_DEL_CMD        0x3
#define MEA_FWD_TABLE_DEL_TBL_ID(key_type)     (key_type)
#define MEA_FWD_TABLE_DEL_PRI        0
#define MEA_FWD_TABLE_RST_NEXT_CMD   0xf
#define MEA_FWD_TABLE_RST_NEXT_CMD_TBL_ID (0)
#define MEA_FWD_TABLE_RST_NEXT_PRI   0



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*  MEA_IF_CMD_PARAM_TBL_TYP_DEVICE_CONTEXT table definitions                    */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
// ENET_IF_CMD_PARAM_TBL_TYP_DEVICE_CONTEXT

typedef enum {
    MEA_IF_CONTEXT_INDEX_0 = 0, /*Global 0*/
    MEA_IF_CONTEXT_INDEX_1,     /*Global 1*/
    MEA_IF_CONTEXT_INDEX_2,     /*reserve */
    MEA_IF_CONTEXT_INDEX_3,     /*pre_parser*/
    MEA_IF_CONTEXT_INDEX_4,     /*parser    */
    MEA_IF_CONTEXT_INDEX_5,     /*service    */
    MEA_IF_CONTEXT_INDEX_6,     /*ACL */
    MEA_IF_CONTEXT_INDEX_7,     /*forwarder_Limter */
    MEA_IF_CONTEXT_INDEX_8,     /*forwarder*/
    MEA_IF_CONTEXT_INDEX_9,     /*Shaper*/
    MEA_IF_CONTEXT_INDEX_10,    /*policer*/
    MEA_IF_CONTEXT_INDEX_11,    /*WFQ*/
    MEA_IF_CONTEXT_INDEX_12,    /*action*/
    MEA_IF_CONTEXT_INDEX_13,    /*xpermission */
    MEA_IF_CONTEXT_INDEX_14,    /*sar_machine*/
    MEA_IF_CONTEXT_INDEX_15,    /*editor_BMF*/
    MEA_IF_CONTEXT_INDEX_16,    /*PM*/
    MEA_IF_CONTEXT_INDEX_17,    /*packetGen*/
    
    MEA_IF_CONTEXT_INDEX_18, /* nvgr_tunnel*/
    MEA_IF_CONTEXT_INDEX_19, /*reserve *//*portMap*/
    MEA_IF_CONTEXT_INDEX_20, /*reserve *//*portMap*/
    MEA_IF_CONTEXT_INDEX_21, /*reserve *//*portMap*/
    
    MEA_IF_CONTEXT_INDEX_22, /*port mac fifo*/
    MEA_IF_CONTEXT_INDEX_23, /*fragment*/
    
    MEA_IF_CONTEXT_INDEX_24, /*TDM interface A*/
    MEA_IF_CONTEXT_INDEX_25, /*TDM interface B*/
    MEA_IF_CONTEXT_INDEX_26, /*TDM interface c*/
    MEA_IF_CONTEXT_INDEX_27, /*TDM global*/
    
    MEA_IF_CONTEXT_INDEX_28, /*reserve */

    MEA_IF_CONTEXT_INDEX_29, /* mii support*/

    MEA_IF_CONTEXT_INDEX_30, /*reserve */ /*portMap*/
    MEA_IF_CONTEXT_INDEX_31, /*reserve */ /*portMap*/
    MEA_IF_CONTEXT_INDEX_32, /*reserve */ /*portMap*/
    MEA_IF_CONTEXT_INDEX_33, /*reserve */ /*portMap*/
    
    MEA_IF_CONTEXT_INDEX_34,/*Hc*/
    MEA_IF_CONTEXT_INDEX_35,/*Hc*/
    
    MEA_IF_CONTEXT_INDEX_36,/*reserve */ /*interface*/
    MEA_IF_CONTEXT_INDEX_37,/*reserve */ /*interface*/
    MEA_IF_CONTEXT_INDEX_38,/*reserve */ /*interface*/
    MEA_IF_CONTEXT_INDEX_39,/*reserve */ /*interface*/

    MEA_IF_CONTEXT_INDEX_40, /*slice0*/
    MEA_IF_CONTEXT_INDEX_41, /*slice1*/
    MEA_IF_CONTEXT_INDEX_42,/*slice2*/
    MEA_IF_CONTEXT_INDEX_43,/*slice0*/

    MEA_IF_CONTEXT_INDEX_LAST  

}MEA_if_Context_index;

/*if_last_if_to_bm event*/
#define MEA_EVENT_IFLASTIFTOBM_SOURCE_PORT_START   	       0
#define MEA_EVENT_IFLASTIFTOBM_SOURCE_PORT_WIDTH           (7 + MEA_Platform_GetMaxNumOfPort_BIT())   /**/

#define MEA_EVENT_IFLASTIFTOBM_FRAG_SCHD_PRI          (MEA_EVENT_IFLASTIFTOBM_SOURCE_PORT_WIDTH + \
                                                           MEA_EVENT_IFLASTIFTOBM_SOURCE_PORT_START )       
#define MEA_EVENT_IFLASTIFTOBM_FRAG_SCHD_PRI_WIDTH     3 

#define MEA_EVENT_IFLASTIFTOBM_HW_EDITID_START            (MEA_EVENT_IFLASTIFTOBM_FRAG_SCHD_PRI + \
                                                           MEA_EVENT_IFLASTIFTOBM_FRAG_SCHD_PRI_WIDTH )

#define MEA_EVENT_IFLASTIFTOBM_HW_EDITID_WIDTH(hwUnit_i)     (MEA_IF_ACTION_MAKE0_EDIT_ID_WIDTH((MEA_UNIT_0),(hwUnit_i)))
/*alr edit id 32bit*/
#define MEA_EVENT_IFLASTIFTOBM_EDIT_PROTOCOL_START(hwUnit_i)       (MEA_EVENT_IFLASTIFTOBM_HW_EDITID_START + \
                                                                   MEA_EVENT_IFLASTIFTOBM_HW_EDITID_WIDTH((hwUnit_i)) )
#define MEA_EVENT_IFLASTIFTOBM_EDIT_PROTOCOL_WIDTH	       2

#define MEA_EVENT_IFLASTIFTOBM_EDIT_LLC_START(hwUnit_i)             (MEA_EVENT_IFLASTIFTOBM_EDIT_PROTOCOL_START((hwUnit_i)) + \
                                                           MEA_EVENT_IFLASTIFTOBM_EDIT_PROTOCOL_WIDTH )
#define MEA_EVENT_IFLASTIFTOBM_EDIT_LLC_WIDTH			   1


#define MEA_EVENT_IFLASTIFTOBM_FRAGMANT_START        (17+MEA_EVENT_IFLASTIFTOBM_SOURCE_PORT_WIDTH  + MEA_EVENT_IFLASTIFTOBM_FRAG_SCHD_PRI_WIDTH)
#define MEA_EVENT_IFLASTIFTOBM_FRAGMANT_WIDTH 1

#define MEA_EVENT_IFLASTIFTOBM_HW_PM_ID_START(hwUnit_i)    (32+MEA_EVENT_IFLASTIFTOBM_SOURCE_PORT_WIDTH + MEA_EVENT_IFLASTIFTOBM_FRAG_SCHD_PRI_WIDTH )
#define MEA_EVENT_IFLASTIFTOBM_HW_PM_ID_WIDTH(hwUnit_i)    (MEA_IF_ACTION_MAKE0_PM_ID_WIDTH((MEA_UNIT_0),(hwUnit_i))) //11

#define MEA_EVENT_IFLASTIFTOBM_HW_TM_ID_START(hwUnit_i)   (MEA_EVENT_IFLASTIFTOBM_HW_PM_ID_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_HW_PM_ID_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_HW_TM_ID_WIDTH(hwUnit_i)   (MEA_IF_ACTION_MAKE0_TM_ID_WIDTH((MEA_UNIT_0),(hwUnit_i))) //11

#define MEA_EVENT_IFLASTIFTOBM_PRIORITY_START(hwUnit_i)   (MEA_EVENT_IFLASTIFTOBM_HW_TM_ID_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_HW_TM_ID_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_PRIORITY_WIDTH(hwUnit_i)		       3

#define MEA_EVENT_IFLASTIFTOBM_PRIORITY_VALID_START(hwUnit_i)       (MEA_EVENT_IFLASTIFTOBM_PRIORITY_START(hwUnit_i) + \
                                                                     MEA_EVENT_IFLASTIFTOBM_PRIORITY_WIDTH(hwUnit_i) )
#define MEA_EVENT_IFLASTIFTOBM_PRIORITY_VALID_WIDTH(hwUnit_i)	       1

#define MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_START(hwUnit_i)          (MEA_EVENT_IFLASTIFTOBM_PRIORITY_VALID_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_PRIORITY_VALID_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_WIDTH(hwUnit_i)		   3

#define MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_VALID_START(hwUnit_i)    (MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_VALID_WIDTH(hwUnit_i)     1

#define MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_START(hwUnit_i)         (MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_VALID_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_L2_PRIORITY_VALID_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_WIDTH(hwUnit_i)		   2

#define MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_AWARE_START(hwUnit_i)   (MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_AWARE_WIDTH(hwUnit_i)    1

#define MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_START(hwUnit_i)            (MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_AWARE_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_PARSER_COLOR_AWARE_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_WIDTH(hwUnit_i)		       2

#define MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_FORCE_START(hwUnit_i)      (MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_FORCE_WIDTH(hwUnit_i)	   1

#define MEA_EVENT_IFLASTIFTOBM_PARSER_COS_START(hwUnit_i)          (MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_FORCE_WIDTH(hwUnit_i) + MEA_EVENT_IFLASTIFTOBM_ALR_COLOR_FORCE_START(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_PARSER_COS_WIDTH(hwUnit_i)		       3

#define MEA_EVENT_IFLASTIFTOBM_ALR_COS_START(hwUnit_i)              (MEA_EVENT_IFLASTIFTOBM_PARSER_COS_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_PARSER_COS_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_ALR_COS_WIDTH(hwUnit_i)			   3

#define MEA_EVENT_IFLASTIFTOBM_ALR_FORCE_COS_START(hwUnit_i)        ( MEA_EVENT_IFLASTIFTOBM_ALR_COS_START(hwUnit_i) + \
                                                            MEA_EVENT_IFLASTIFTOBM_ALR_COS_WIDTH(hwUnit_i)) 
#define MEA_EVENT_IFLASTIFTOBM_ALR_FORCE_COS_WIDTH(hwUnit_i)	       1

#define MEA_EVENT_IFLASTIFTOBM_POS_OR_ATM_START(hwUnit_i)           (MEA_EVENT_IFLASTIFTOBM_ALR_FORCE_COS_START(hwUnit_i) + \
                                                           MEA_EVENT_IFLASTIFTOBM_ALR_FORCE_COS_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_POS_OR_ATM_WIDTH(hwUnit_i)		       1

#define MEA_EVENT_IFLASTIFTOBM_PACKET_TO_CPU_START(hwUnit_i)        (MEA_EVENT_IFLASTIFTOBM_POS_OR_ATM_START(hwUnit_i) + \
                                                                     MEA_EVENT_IFLASTIFTOBM_POS_OR_ATM_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_PACKET_TO_CPU_WIDTH(hwUnit_i)	       1

#define MEA_EVENT_IFLASTIFTOBM_DEST_PORT_START(hwUnit_i)		      (MEA_EVENT_IFLASTIFTOBM_PACKET_TO_CPU_START(hwUnit_i) + \
                                                                       MEA_EVENT_IFLASTIFTOBM_PACKET_TO_CPU_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_DEST_PORT_WIDTH(hwUnit_i)		       9

#define MEA_EVENT_IFLASTIFTOBM_PORT_NUMBER_START(hwUnit_i)          (MEA_EVENT_IFLASTIFTOBM_DEST_PORT_WIDTH(hwUnit_i) + \
                                                                    MEA_EVENT_IFLASTIFTOBM_DEST_PORT_START(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_PORT_NUMBER_WIDTH(hwUnit_i)		   7

#define MEA_EVENT_IFLASTIFTOBM_FRAG_HEDER_START(hwUnit_i)          (MEA_EVENT_IFLASTIFTOBM_PORT_NUMBER_START(hwUnit_i) + \
                                                                    MEA_EVENT_IFLASTIFTOBM_PORT_NUMBER_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_FRAG_HEDER_WIDTH(hwUnit_i)           16

#define MEA_EVENT_IFLASTIFTOBM_DEFRAG_START(hwUnit_i)          (MEA_EVENT_IFLASTIFTOBM_FRAG_HEDER_START(hwUnit_i) + \
                                                                MEA_EVENT_IFLASTIFTOBM_FRAG_HEDER_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_DEFRAG_WIDTH(hwUnit_i)           1

#define MEA_EVENT_IFLASTIFTOBM_FLOODING_EDITING_START(hwUnit_i) (MEA_EVENT_IFLASTIFTOBM_DEFRAG_START(hwUnit_i) + \
                                                                 MEA_EVENT_IFLASTIFTOBM_DEFRAG_WIDTH(hwUnit_i))

#define MEA_EVENT_IFLASTIFTOBM_FLOODING_EDITING_WIDTH(hwUnit_i)     (mea_drv_Get_DeviceInfo_ActToBMbus((MEA_UNIT_0),hwUnit_i))  //(1) /*ActToBMbus*/


#define MEA_EVENT_IFLASTIFTOBM_ING_FLOW_POLICER_PROF_START(hwUnit_i) (MEA_EVENT_IFLASTIFTOBM_FLOODING_EDITING_START(hwUnit_i) + \
                                                                      MEA_EVENT_IFLASTIFTOBM_FLOODING_EDITING_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_ING_FLOW_POLICER_PROF_WIDTH(hwUnit_i)   (MEA_IF_SRV_ENTRY_INGRESS_FLOW_POL((MEA_UNIT_0), hwUnit_i))


#define MEA_EVENT_IFLASTIFTOBM_outer_IP_fragmented_START(hwUnit_i) (MEA_EVENT_IFLASTIFTOBM_ING_FLOW_POLICER_PROF_START(hwUnit_i) + \
                                                                      MEA_EVENT_IFLASTIFTOBM_ING_FLOW_POLICER_PROF_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_outer_IP_fragmented_WIDTH(hwUnit_i)   (1)

#define MEA_EVENT_IFLASTIFTOBM_inner_IP_fragmented_START(hwUnit_i) (MEA_EVENT_IFLASTIFTOBM_outer_IP_fragmented_START(hwUnit_i) + \
                                                                      MEA_EVENT_IFLASTIFTOBM_outer_IP_fragmented_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_inner_IP_fragmented_WIDTH(hwUnit_i)   (1)

#define MEA_EVENT_IFLASTIFTOBM_pcacket_reassembl_START(hwUnit_i)     (MEA_EVENT_IFLASTIFTOBM_inner_IP_fragmented_START(hwUnit_i) + \
                                                                      MEA_EVENT_IFLASTIFTOBM_inner_IP_fragmented_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_pcacket_reassembl_WIDTH(hwUnit_i)     (1)

#define MEA_EVENT_IFLASTIFTOBM_reassembl_sessionID_START(hwUnit_i)    (MEA_EVENT_IFLASTIFTOBM_pcacket_reassembl_START(hwUnit_i) + \
                                                                      MEA_EVENT_IFLASTIFTOBM_pcacket_reassembl_WIDTH(hwUnit_i))
#define MEA_EVENT_IFLASTIFTOBM_reassembl_sessionID_WIDTH(hwUnit_i)    ((mea_drv_Get_DeviceInfo_ip_reassembly_session_id_width((MEA_UNIT_0),(hwUnit_i) )) )

#define MEA_EVENT_IFLASTIFTOBM_ing_flow_mtu_START(hwUnit_i)    (MEA_EVENT_IFLASTIFTOBM_reassembl_sessionID_START(hwUnit_i) +  \
                                                                MEA_EVENT_IFLASTIFTOBM_reassembl_sessionID_WIDTH(hwUnit_i) )
#define MEA_EVENT_IFLASTIFTOBM_ing_flow_mtu_WIDTH(hwUnit_i)    (9)












//////////////////////////////////////////////////////////////////////////

#define MEA_EVENT_CLS_PRI_START             0
#define MEA_EVENT_CLS_PRI_WIDTH             6

#define MEA_EVENT_CLS_PRI_TYPE_START                 (MEA_EVENT_CLS_PRI_START + \
                                                       MEA_EVENT_CLS_PRI_WIDTH)
#define MEA_EVENT_CLS_PRI_TYPE_WIDTH         2

#define MEA_EVENT_CLS_SRC_PORT_START                 (MEA_EVENT_CLS_PRI_TYPE_START + \
                                                      MEA_EVENT_CLS_PRI_TYPE_WIDTH)

#define MEA_EVENT_CLS_SRC_PORT_WIDTH         (7 + MEA_Platform_GetMaxNumOfPort_BIT())


#define MEA_EVENT_CLS_NET_TAG1_START                 (MEA_EVENT_CLS_SRC_PORT_START + \
                                                      MEA_EVENT_CLS_SRC_PORT_WIDTH)
#define MEA_EVENT_CLS_NET_TAG1_WIDTH            20

#define MEA_EVENT_CLS_NET_SubType_START      (MEA_EVENT_CLS_NET_TAG1_START + \
                                                    MEA_EVENT_CLS_NET_TAG1_WIDTH)

#define MEA_EVENT_CLS_NET_SubType_WIDTH          4

#define MEA_EVENT_CLS_L2TYPE_START                 (MEA_EVENT_CLS_NET_SubType_START + \
                                                    MEA_EVENT_CLS_NET_SubType_WIDTH)
#define MEA_EVENT_CLS_L2TYPE_WIDTH         5


#define MEA_EVENT_CLS_NET_TAG2_START                 (MEA_EVENT_CLS_L2TYPE_START + \
                                                       MEA_EVENT_CLS_L2TYPE_WIDTH)
#define MEA_EVENT_CLS_NET_TAG2_WIDTH         24
#define MEA_EVENT_CLS_NET2_WIDTH             27 



/*last_classifier_search*/

#define MEA_EVENT_LAST_CLS_SIG0_START           (0)
#define MEA_EVENT_LAST_CLS_SIG0_WIDTH            (9)

#define MEA_EVENT_LAST_CLS_SIG1_3B_START     (MEA_EVENT_LAST_CLS_SIG0_START + MEA_EVENT_LAST_CLS_SIG0_WIDTH)
#define MEA_EVENT_LAST_CLS_SIG1_3B_WIDTH         (3)

#define MEA_EVENT_LAST_CLS_SRC_PORT_START     (MEA_EVENT_LAST_CLS_SIG1_3B_START+MEA_EVENT_LAST_CLS_SIG1_3B_WIDTH)
#define MEA_EVENT_LAST_CLS_SRC_PORT_WIDTH        (7 + MEA_Platform_GetMaxNumOfPort_BIT())

#define MEA_EVENT_LAST_CLS_NET1_START           (MEA_EVENT_LAST_CLS_SRC_PORT_START + MEA_EVENT_LAST_CLS_SRC_PORT_WIDTH)
#define MEA_EVENT_LAST_CLS_NET1_WIDTH            (16)

#define MEA_EVENT_LAST_CLS_SubType_START          (MEA_EVENT_LAST_CLS_NET1_START + MEA_EVENT_LAST_CLS_NET1_WIDTH)
#define MEA_EVENT_LAST_CLS_SubType_WIDTH         (4)


#define MEA_EVENT_LAST_CLS_L2TYPE_START          (MEA_EVENT_LAST_CLS_SubType_START + MEA_EVENT_LAST_CLS_SubType_WIDTH)
#define MEA_EVENT_LAST_CLS_L2TYPE_WIDTH          (5)

#define MEA_EVENT_LAST_CLS_SIG1_6B_START         (MEA_EVENT_LAST_CLS_L2TYPE_START + MEA_EVENT_LAST_CLS_L2TYPE_WIDTH)
#define MEA_EVENT_LAST_CLS_SIG1_6B_WIDTH         (6)

#define MEA_EVENT_LAST_CLS_SIG2_START            (MEA_EVENT_LAST_CLS_SIG1_6B_START + MEA_EVENT_LAST_CLS_SIG1_6B_WIDTH)
#define MEA_EVENT_LAST_CLS_SIG2_WIDTH            (9)
 
#define MEA_EVENT_LAST_CLS_SIG3_START            (MEA_EVENT_LAST_CLS_SIG2_START + MEA_EVENT_LAST_CLS_SIG2_WIDTH)
#define MEA_EVENT_LAST_CLS_SIG3_WIDTH            (9)

#define MEA_EVENT_LAST_UNMATCH_OUTPORT_START     (MEA_EVENT_LAST_CLS_SIG3_START + MEA_EVENT_LAST_CLS_SIG3_WIDTH )
#define MEA_EVENT_LAST_UNMATCH_OUTPORT_WIDTH      (7 + MEA_Platform_GetMaxNumOfPort_BIT())

#define MEA_EVENT_LAST_UNMATCH_VALID_START       (MEA_EVENT_LAST_UNMATCH_OUTPORT_START + MEA_EVENT_LAST_UNMATCH_OUTPORT_WIDTH)
#define MEA_EVENT_LAST_UNMATCH_VALID_WIDTH        (1)

#define MEA_EVENT_LAST_NO_UNMATCH_ALL_START        (MEA_EVENT_LAST_UNMATCH_VALID_START + MEA_EVENT_LAST_UNMATCH_VALID_WIDTH)
#define MEA_EVENT_LAST_NO_UNMATCH_ALL_WIDTH        (1)

#define MEA_EVENT_LAST_MATCH_EXTERNAL_START        (MEA_EVENT_LAST_NO_UNMATCH_ALL_START + MEA_EVENT_LAST_NO_UNMATCH_ALL_WIDTH )
#define MEA_EVENT_LAST_MATCH_EXTERNAL_WIDTH        (1)

/************************************************************************/
/* Table 9 ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL register offset */
/***********************************************************************/

#define MEA_GLOBAl_TBL_REG_My_Host_MAC    0
#define MEA_GLOBAl_TBL_REG_APN_vlan       1  /* 1 to 8*/

#define MEA_GLOBAl_TBL_REG_DHCP_IP        9   /*9 to 16*/

#define MEA_GLOBAl_TBL_REG_Infra_VLAN                   17
#define MEA_GLOBAl_TBL_REG_S_GW_IP0                     18   /*up to */
#define MEA_GLOBAl_TBL_REG_My_PGW_MAC                   19  
#define MEA_GLOBAl_TBL_REG_IPCS_default_sid_info        20  
#define MEA_GLOBAl_TBL_REG_NVGRE_TUNNEL_REMOVE          21  
#define MEA_GLOBAl_TBL_REG_IF_BM_ACK_INFO               22 
#define MEA_GLOBAl_TBL_REG_ETHETYPE_CAM                 23 


#define MEA_GLOBAl_TBL_REG_S_ing_ts_flow_def 24
#define MEA_GLOBAl_TBL_REG_S_ing_ts_value    25
#define MEA_GLOBAl_TBL_REG_P_GW_U_IP        26
#define MEA_GLOBAl_TBL_REG_default_sid_info 27
#define MEA_GLOBAl_TBL_REG_MNG_vlanId       28
#define MEA_GLOBAl_TBL_REG_Conf_D_IP        29
#define MEA_GLOBAl_TBL_REG_MME_IP           30



#define MEA_GLOBAl_TBL_REG_default_SID_ETHCS_UL     32


#define MEA_GLOBAl_TBL_REG_EPC_IPV6_0        33
#define MEA_GLOBAl_TBL_REG_EPC_IPV6_1        34
#define MEA_GLOBAl_TBL_REG_EPC_IPV6_2        35
#define MEA_GLOBAl_TBL_REG_EPC_IPV6_3        36


#define MEA_GLOBAl_TBL_REG_LOCAL_MAC0            37
#define MEA_GLOBAl_TBL_REG_LOCAL_MAC1            38
#define MEA_GLOBAl_TBL_REG_LOCAL_MAC2            39
#define MEA_GLOBAl_TBL_REG_LOCAL_MAC3            40

#define MEA_GLOBAl_TBL_REG_LOCAL_IP0            41
#define MEA_GLOBAl_TBL_REG_LOCAL_IP1            42
#define MEA_GLOBAl_TBL_REG_LOCAL_IP2            43
#define MEA_GLOBAl_TBL_REG_LOCAL_IP3            44
#define MEA_GLOBAl_TBL_REG_LOCAL_IP4            45
#define MEA_GLOBAl_TBL_REG_LOCAL_IP5            46
#define MEA_GLOBAl_TBL_REG_LOCAL_IP6            47
#define MEA_GLOBAl_TBL_REG_LOCAL_IP7_MY_IpSec   48
#define MEA_GLOBAl_TBL_REG_IPv6_enable          49

#define MEA_GLOBAl_TBL_REG_enB_vpn               50
#define MEA_GLOBAl_TBL_L3_Internal_ETHERTYPE     51

#define MEA_GLOBAl_TBL_REG_EPC_IPV6_4            52
#define MEA_GLOBAl_TBL_REG_EPC_IPV6_5            53
#define MEA_GLOBAl_TBL_REG_EPC_IPV6_6            54
#define MEA_GLOBAl_TBL_REG_EPC_IPV6_7            55

#define MEA_GLOBAl_TBL_REG_PDN_IPV4_INDEX_0      56  /*Up to 63*/
#define MEA_GLOBAl_TBL_REG_SFLOW_MAP_QUEUE_Group        64
#define MEA_GLOBAl_TBL_REG_SFLOW_PROF            65  /*65,66,67,68*/
#define MEA_GLOBAl_TBL_REG_SFLOW_TIC             69
#define MEA_GLOBAl_TBL_REG_VXLAN_L4_DEST_port    70
#define MEA_GLOBAl_TBL_REG_VXLAN_CPE_port        71
#define MEA_GLOBAl_TBL_REG_SEGMANT_ISOLATE_PROF0  72  /*72,73,74*/


#define MEA_GLOBAl_TBL_REG_BFD_MY_Discriminator   75 
#define MEA_GLOBAl_TBL_REG_BFD_MY_SW_IPV4_ADDRES  76 


#define MEA_GLOBAl_TBL_REG_I2C_COMMAND      77

#define MEA_GLOBAl_TBL_REG_I2C_RW_DATA      78


























// INTERLAKEN register 
#define MEA_INTERLAKEN_CONTROL_REG_0_TX_RX_RESET                0
#define MEA_INTERLAKEN_CONTROL_REG_1_LOOP                       1

#define MEA_INTERLAKEN_CONTROL_REG_4_CTL_TX                     4
#define MEA_INTERLAKEN_CONTROL_REG_8_CTL_TX_READY               8
#define MEA_INTERLAKEN_CONTROL_REG_12_CTL_TX_RATE_LLIMITER     12
#define MEA_INTERLAKEN_CONTROL_REG_16_CTL_TX_DIAGNOSTIC        16
#define MEA_INTERLAKEN_CONTROL_REG_20_CTL_RX_CONF              20

#define MEA_INTERLAKEN_REG_0_RX_STATUS1                0
#define MEA_INTERLAKEN_REG_4_RX_STATUS2                4
#define MEA_INTERLAKEN_REG_8_RX_STATUS2                8
#define MEA_INTERLAKEN_REG_12_TX_STATUS2               12



/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_TBL_LOG_Des_write_NEXT_POINTER_BANK     3
#define MEA_TBL_LOG_Des_write_NEXT_POINTER          12
#define MEA_TBL_LOG_Des_write_Trigger               1
#define MEA_TBL_LOG_Des_write_AXI_BANK_ADDR         3
#define MEA_TBL_LOG_Des_write_AXI_ADDRESS           13
#define MEA_TBL_LOG_Des_write_AXI_HoldTime          7 //16
#define MEA_TBL_LOG_Des_write_AXI_Packet_length     11 
#define MEA_TBL_LOG_Des_write_AXI_TimeStamp         22
#define MEA_TBL_LOG_Des_read_NEXT_POINTER_BANK      3
#define MEA_TBL_LOG_Des_read_NEXT_POINTER           12
#define MEA_TBL_LOG_Des_read_Trigger                1
#define MEA_TBL_LOG_Des_read_AXI_BANK_ADDR          3
#define MEA_TBL_LOG_Des_read_AXI_ADDRESS            13
#define MEA_TBL_LOG_Des_read_AXI_HoldTime           7
#define MEA_TBL_LOG_Des_read_AXI_Packet_length     11 
#define MEA_TBL_LOG_Des_read_AXI_TimeStamp          22


/* TBL_TYP_WALL_CLOCK_REGS  */
#define ENET_BM_WALL_CLOCK_CTL0_REG          16
#define ENET_BM_WALL_CLOCK_STS0_REG          17 /*TBD*/
#define ENET_BM_WALL_CLOCK_CTL1_REG          18
#define ENET_BM_WALL_CLOCK_STS1_REG          19 /*TBD*/
#define ENET_BM_WALL_CLOCK_9BYTE_NUM_REG      20 /*TBD*/

#define ENET_BM_WALL_CLOCK_SET_SEC_REG       21 /* use the global reg*/
#define ENET_BM_WALL_CLOCK_GET_SEC_REG       22 /* use the global reg*/
#define ENET_BM_WALL_CLOCK_SET_NS_REG        23 /* use the global reg*/
#define ENET_BM_WALL_CLOCK_GET_NS_REG        24 /* use the global reg*/
#define ENET_BM_WALL_CLOCK_COMP_REG          25
#define ENET_BM_WALL_CLOCK_Reserv_REG        26 /*TBD*/
#define ENET_BM_WALL_CLOCK_LAST_ERR_REG      27 /*TBD*/
#define ENET_BM_WALL_CLOCK_COR_REG           28
#define ENET_BM_WALL_CLOCK_PPS_SET_UP_REG    29 /*TBD*/
#define ENET_BM_WALL_CLOCK_PPS_CNT_REG       30 /*TBD*/

#define ENET_BM_WALL_CLOCK_ACUM_CFG1_REG       31
#define ENET_BM_WALL_CLOCK_ACUM_CFG2_REG       32



/************************************************************************/
/* TFT_LPM                                                               */
/************************************************************************/
#define MEA_LPM_REG_L4_Source_min 16
#define MEA_LPM_REG_L4_Source_max 16
#define MEA_LPM_REG_L4_Dest_min   16
#define MEA_LPM_REG_L4_Dest_max   16
#define MEA_LPM_REG_Source_IPv4   32
#define MEA_LPM_REG_Dest_IPv4     32
#define MEA_LPM_REG_Dest_pri_p     8
#define MEA_LPM_REG_IP_protocol    8
#define MEA_LPM_REG_DSCP_min       6
#define MEA_LPM_REG_DSCP_max       6
#define MEA_LPM_REG_Internal_Etype 3
#define MEA_LPM_REG_Type           2
#define MEA_LPM_REG_Dest_IP_mask   5
#define MEA_LPM_REG_Source_IP_mask 5
#define MEA_LPM_REG_Priority_Rule  5
#define MEA_LPM_REG_QoS_Index      3




#define MEA_LPM_REG_NEW_L4_Source_min       16
#define MEA_LPM_REG_NEW_L4_Source_max       16
#define MEA_LPM_REG_NEW_L4_Dest_min         16
#define MEA_LPM_REG_NEW_L4_Dest_max         16
#define MEA_LPM_REG_NEW_Source_IPv4         32
#define MEA_LPM_REG_NEW_Dest_IPv4           32
#define MEA_LPM_REG_NEW_IP_protocol          8
#define MEA_LPM_REG_NEW_DSCP_min             6
#define MEA_LPM_REG_NEW_DSCP_max             6
#define MEA_LPM_REG_NEW_Dest_pri_p           8
#define MEA_LPM_REG_NEW_Internal_Etype       3
#define MEA_LPM_REG_NEW_Dest_IP_mask         5
#define MEA_LPM_REG_NEW_Source_IP_mask       5
#define MEA_LPM_REG_NEW_PAD                  5
#define MEA_LPM_REG_NEW_Type                 3
#define MEA_LPM_REG_NEW_Priority_Rule        5
#define MEA_LPM_REG_NEW_QoS_Index            3



#define MEA_LPM_REG_NEW_KEY_IPV6_L4_Source_min      16
#define MEA_LPM_REG_NEW_KEY_IPV6_L4_Source_max      16
#define MEA_LPM_REG_NEW_KEY_IPV6_L4_Dest_min        16
#define MEA_LPM_REG_NEW_KEY_IPV6_L4_Dest_max        16
#define MEA_LPM_REG_NEW_KEY_IPV6_IPV6_L             32
#define MEA_LPM_REG_NEW_KEY_IPV6_IPV6_H             32
#define MEA_LPM_REG_NEW_KEY_IPV6_IP_protocol         8
#define MEA_LPM_REG_NEW_KEY_IPV6_DSCP_min            6
#define MEA_LPM_REG_NEW_KEY_IPV6_DSCP_max            6
#define MEA_LPM_REG_NEW_KEY_IPV6_Flow_Label         20
#define MEA_LPM_REG_NEW_KEY_IPV6_IPv6_mask           6

#define MEA_LPM_REG_NEW_KEY_IPV6_Type                3
#define MEA_LPM_REG_NEW_KEY_IPV6_Priority_Rule       5
#define MEA_LPM_REG_NEW_KEY_IPV6_QoS_Index           3












#ifdef __cplusplus
}
#endif

#endif /* MEA_IF_REGS_H */






