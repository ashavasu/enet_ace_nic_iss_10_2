/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef MEA_INIT_H
#define	MEA_INIT_H

#include "MEA_platform.h"

#ifdef __cplusplus
extern "C" {
#endif


MEA_Status mea_drv_BasicInit(MEA_Unit_t                 unit,
                             MEA_ULong_t             *   module_base_addr_vec);

MEA_Status mea_drv_AdvanceInit(MEA_Unit_t                 unit);

extern MEA_Bool MEA_InitDone;


MEA_ULong_t   MEA_GetMemoryId  (MEA_module_te module, MEA_ULong_t Addr);
MEA_ULong_t  MEA_GetMemoryAddr(MEA_module_te module, MEA_ULong_t *Addr);
MEA_Status  mea_drv_Conclude_MemoryMap(void);
MEA_Status MEA_API_Get_HwInternalVersion(MEA_Uint32* if_version,
                                         MEA_Uint32* bm_version);


#ifdef __cplusplus
}
#endif
#endif /* #ifndef MEA_INIT_H */

