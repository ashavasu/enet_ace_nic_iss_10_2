/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/******************************************************************************
* 	Module Name:		 enet_general_drv.H	   									     
*																					 
*   Module Description:	     
*                            
*                         
*	                     
*                        
*                        
*																					 
*  Date of Creation:	 14/09/06													 
*																					 
*  Author Name:			 Alex Weis.													 
*******************************************************************************/
#ifndef _ENET_GENERAL_DRV_H_
#define _ENET_GENERAL_DRV_H_


#include "MEA_platform.h"

/*-------------------------------------------------------------------------------*/			
/*             !!! temp link to MEA                                              */			
/*-------------------------------------------------------------------------------*/			
#include "mea_api.h"
#include "mea_bm_regs.h"
#include "mea_deviceInfo_drv.h"

#ifdef __cplusplus
extern "C" {
#endif

ENET_Status enet_Init_Scheduler_Table(ENET_Unit_t unit_i); 
#ifdef __cplusplus
}
#endif
/* --------------- End Of Tem Link To Mea ---------------------------------------*/

ENET_Bool ENET_IsValid_Unit(ENET_Unit_t unit_i, ENET_Bool silent_i);

/*--------------------------------------------------------------------*/
/*                                                                    */
/* IF Direct registers                                                */
/*                                                                    */
/*                                                                    */
/*--------------------------------------------------------------------*/
#define ENET_IF_GLOBAL3_REG                                 0x00
#define ENET_IF_REG_00                                      0x00
#define ENET_IF_REG_04                                      0x04
#define ENET_IF_REG_08                                      0x08
#define ENET_IF_REG_0c                                      0x0c
#define ENET_IF_REG_10                                      0x10
#define ENET_IF_REG_14                                      0x14
#define ENET_IF_REG_18                                      0x18
#define ENET_IF_REG_1C                                      0x1c
#define ENET_IF_PORT_UTOPIA_POS_31_0_REG                    0x20
#define ENET_IF_PORT_UTOPIA_POS_63_32_REG                   0x24
#define ENET_IF_PORT_UTOPIA_POS_95_64_REG                   0x28
#define ENET_IF_PORT_UTOPIA_POLLING_ADDR_REG                0x2c

#define ENET_IF_PORT_RX_CRC_CHECK_31_0_REG                  0x30
#define ENET_IF_PORT_RX_CRC_CHECK_63_32_REG                 0x34
#define ENET_IF_PORT_RX_CRC_CHECK_95_64_REG                 0x38
#define ENET_IF_PORT_RX_CRC_CHECK_127_96_REG                0x3c

#define ENET_IF_G999_reserv                                 0x40
#define ENET_IF_REG_I2C_CONTROL                             0x44  /* W =control R=status  */
#define ENET_IF_REG_I2C_DATA                                0x48  /* for i2c  data */ 
#define ENET_IF_REG_4c                                      0x4c   
#define ENET_IF_REG_50                                      0x50
#define ENET_IF_REG_54                                      0x54
#define ENET_IF_REG_58                                      0x58
#define ENET_IF_REG_55                                      0x5C
#define ENET_IF_SSSMII_RX_CONFIG_REG                        0x60
#define ENET_IF_SSSMII_TX_CONFIG0_REG                       0x64
#define ENET_IF_SSSMII_TX_CONFIG1_REG                       0x68
#define ENET_IF_INTERRUPT_REG                               0x6c /*interrupt status */
#define ENET_IF_INTERRUPT_MASK_REG                          0x70 /* interrupt mask */
#define ENET_IF_PHY_CPU_TH                                  0x74
#define ENET_IF_PHY_SPEED_RATE_100_10_REG                   0x78
#define ENET_IF_CPLD_VER                                    0x7c
#define ENET_IF_VERSION_DATE_REG                            0x80
#define ENET_IF_STATUS_REG                                  0x88
#define ENET_IF_802_1AG_COMMAND_REG                         0x8c
#define ENET_IF_CMD_PARAM_REG                               0x90
#define ENET_IF_CMD_REG                                     0x94
#define ENET_IF_WRITE_DATA_INFO_REG                         0x98
#define ENET_IF_READ_DATA_INFO_REG                          0x9c
#define ENET_IF_802_1AG_MAC_LO_REG                          0xA0
#define ENET_IF_802_1AG_MAC_HI_REG                          0xA4

#define ENET_IF_IP_DFRAG_STATUS_AVALBEL_SESSION             0xA8    
#define ENET_IF_QSGMII_RST_REG                               0xAC  /*  Bit2 ---> 108:111   bit1---> 104:107    bit0 --100:103*/  

#define ENET_IF_TABLE_REQ                                   0xB0
#define ENET_IF_MAC_SERDES_LB                               0xB4
#define ENET_IF_SWITC_1PLUS1_OVER                           0xB8

#define ENET_IF_SOFTWARE_RESET                              0xBC    /*RESET set  0x1F2E3D4C    Clear 0x0*/

#define ENET_IF_SCRATCH_REG                                 0xc0
#define ENET_IF_GLOBAL0_REG                                 0xc4
#define ENET_IF_GLOBAL1_REG                                 0xc8
#define ENET_IF_GLOBAL2_REG                                 0xcc

#define ENET_IF_SERVICRANGE_REG                             0xD0
#define ENET_IF_GLOBAL_BM_TO_IF_BUFF_THRESHOLD_SLICE1_REG   0xD4 
#define ENET_IF_GLOBAL_BM_TO_IF_BUFF_THRESHOLD_SLICE2_REG   0xD8 /*  */
#define ENET_IF_MAC_AGEING_REG                              0xDC

#define ENET_IF_VER_LX_VERSION_L_REG                        0xE0
#define ENET_IF_VER_LX_VERSION_H_REG                        0xE4
#define ENET_IF_G999_TH_REG                                 0xE8  /* HW configure*/
#define ENET_IF_TLS_TH_REG                                  0xEC  /* HW configure*/

#define ENET_IF_HASH_EVENT_REG                              0xF0 
#define ENET_IF_ACL5_CONFG1_REG                              0xF4 /**/
#define ENET_IF_ACL5_CONFG2_REG                             0xF8 /*reserve1*/
#define ENET_IF_ACL5_CONFG3_REG                             0xFc /*reserve2*/

#define ENET_IF_GLOBAL_BM_TO_IF_BUFF_THRESHOLD_SLICE3_REG   0x100

#define ENET_IF_XMAC_RX_PAUSE_01_REG                        0x10c
#define ENET_IF_XMAC_RX_PAUSE_23_REG                        0x110
#define ENET_IF_GLOBAL_BM_TO_IF_BUFF_THRESHOLD_SLICE4_REG   0x114 
#define ENET_IF_TO_XMAC_REG                                 0x118
#define ENET_IF_TO_BM_INGRESS_SHAPER_REG                    0x11c

#define ENET_IF_FWD_HASH_REG                                0x120 /**/
#define ENET_IF_ACL_COUNT_BY_SID_REG                        0x124 
#define ENET_IF_WD_TA_TIMEOUT                               0x128  /*bit31 interrupt Enable */
#define ENET_IF_G999_1_TH_REG                               0x12C

#define ENET_IF_INTERLAKEN_TH_REG                           0x130
#define ENET_IF_INTERLAKEN_FiFo                             0x134
#define ENET_IF_DBG_COUNT_CTRL_FIFO                         0x138
#define ENET_IF_CTR_GMII_SPEED                              0x13c

#define ENET_IF_GLOBAL_FILTER_DA_MAC_31_0_REG               0x140
#define ENET_IF_GLOBAL_FILTER_DA_MAC_47_32_REG              0x144
#define ENET_IF_REG_148                                     0x148
#define ENET_IF_FAULT_CNT_REG1                              0x14c 

#define ENET_IF_FAULT_CNT_REG2                              0x150
#define ENET_IF_REG_154                                     0x154
#define ENET_IF_REG_158                                     0x158
#define ENET_IF_GLOBAL_L2TP_TUNNEL_MAC_LO                   0x15c

#define ENET_IF_GLOBAL_L2TP_TUNNEL_MAC_HI                   0x160
#define ENET_IF_REG_164                                     0x164
#define ENET_IF_GLOBAL_TRANS_IF_TH_REG                      0x168   /*HW*/
#define ENET_IF_SERVICE_EXTERNAL_REG                        0x16C   

#define ENET_IF_EGRESS_ACL_GLOBAL_REG                       0x170
#define ENET_IF_GLOBAL_PACKETGEN_RESRET_PRBS_REG            0x174
#define ENET_IF_REG_178                                     0x178
#define ENET_IF_REG_17c                                     0x17c

#define ENET_IF_REG_180                                     0x180
#define ENET_IF_REG_184                                     0x184
#define ENET_IF_INTERFACE_SA_MASK_BRAS                      0x188
#define ENET_IF_INTERNAL_POS_INGRESS_THS_REG                0x18c 

#define ENET_IF_PORT_RX_TO_GROUP_P7_P0_REG                  0x190 /*old bonding*/
#define ENET_IF_PORT_RX_TO_GROUP_P15_P8_REG                 0x194 /*old bonding*/
#define ENET_IF_PORT_RX_TO_GROUP_P23_P16_REG                0x198 /*old bonding*/
#define ENET_IF_PORT_RX_TO_GROUP_P127_P125_REG              0x19c /*old bonding*/

#define ENET_IF_RX_CLAVE_THR                                0x1a0
#define ENET_IF_FRAG_ART_EOP_SIZE                           0x1a4
#define ENET_IF_SW_PR_TH_IL                                 0x1a8
#define ENET_IF_HDC_CLASS_CONF                              0x1aC


#define ENET_IF_RADIO_RATE                                  0x1b0
#define ENET_IF_GLOBAL_GENERAL_REG                          0x1b4
#define ENET_IF_DEBUG_VECTOR_REG                            0x1b8
#define ENET_IF_BM_TO_IF_BUFF_THRESHOLD_REG                 0x1bc  // 16bit -31 16 block write   //16bit 0-15 read   resetValue   0x00cd0001

#define ENET_IF_TDM_SPE_A_TYPE_REG                          0x1c0
#define ENET_IF_TDM_SPE_B_TYPE_REG                          0x1c4 
#define ENET_IF_TDM_SPE_C_TYPE_REG                          0x1c8 
#define ENET_IF_TDM_IDLE_PROF0_DATA_REG                     0x1cc     /*each 8bit is prof*/

#define ENET_IF_TDM_IDLE_PROF1_DATA_REG                     0x1d0
#define ENET_IF_TDM_DBG_INCREMANT_BYTE_CES_REG              0x1d4
#define ENET_IF_DBG_MIRROR_CES_PORT_REG                     0x1d8
#define ENET_IF_REG_HDC_RELESE_HC                           0x1dc

#define ENET_IF_CLUSTERS_ADMIN_REG                          0x1e0
#define ENET_IF_TDM_RATE_INT_ADJ_REG                        0x1e4    // bit0
#define ENET_IF_TDM_LB_GROUP0_REG                           0x1e8
#define ENET_IF_TDM_LB_GROUP1_REG                           0x1ec


#define ENET_IF_HC_REG1_REG                                 0x1f0
#define ENET_IF_HC_REG2_REG                                 0x1f4
#define ENET_IF_SPEED_PORT_REG                              0x1f8  /*only for 10M 100M */
 

#define ENET_IF_XLAUI_INTERFACE_REG                          0x200

#define ENET_IF_TDM_SCAN_CES_REG                            0x210
#define ENET_IF_HDC_LEARN_COMPRESS_ETHERTYPE                0x214
#define ENET_IF_HDC_GLOBAL0_REG                             0x218
#define ENET_IF_HDC_DEL_ETHERTYPE                           0x21c


#define ENET_IF_TMUX_SET_ADDRES_REG                         0x240
#define ENET_IF_TMUX_SET_DATA_WRITE_REG                     0x244
#define ENET_IF_TMUX_SET_CMD_REG                            0x248
#define ENET_IF_TMUX_READ_DATA_REG                          0x24c

#define ENET_IF_REG_250                                     0x250
#define ENET_IF_REG_254                                     0x254
#define ENET_IF_REG_258                                     0x258
#define ENET_IF_TDM_SYSCLK_SBI_REG                          0x25c
 
#define ENET_IF_REG_260                                     0x260
#define ENET_IF_REG_264                                     0x264
#define ENET_IF_REG_268                                     0x268
#define ENET_IF_REG_26c                                     0x26c


#define ENET_IF_SERDES_RESET_31_0_REG                       0x270
#define ENET_IF_SERDES_RESET_63_32_REG                      0x274
#define ENET_IF_SERDES_RESET_95_64_REG                      0x278
#define ENET_IF_SERDES_RESET_127_96_REG                     0x27c


#define ENET_IF_SERDES_POWER_GROUP_REG                      0x280

#define ENET_IF_AUTONEGOTIATE_ENABLE_31_0_REG               0x284
#define ENET_IF_AUTONEGOTIATE_ENABLE_63_32_REG              0x28c
#define ENET_IF_AUTONEGOTIATE_ENABLE_95_64_REG              0x290
#define ENET_IF_AUTONEGOTIATE_ENABLE_127_96_REG             0x294

#define ENET_IF_REG_298                                     0x298
#define ENET_IF_REG_Z70_GPIO                                0x29c 


/************************************************************************/
/* Regs for BIST MODE   PRBS/ANA                                        */
/************************************************************************/
#define ENET_IF_BIST_PRBS_GENRATOR                           0x3A0
#define ENET_IF_BIST_PRBS_ANA0_COUNT_BER                     0x3A4
#define ENET_IF_BIST_PRBS_ANA0_COUNT_GOOD                    0x3A8
#define ENET_IF_BIST_PRBS_ANA0_EVNT_LOCK                     0x3Ac
#define ENET_IF_BIST_PRBS_ANA1_COUNT_BER                     0x3B0
#define ENET_IF_BIST_PRBS_ANA1_COUNT_GOOD                    0x3B4
#define ENET_IF_BIST_PRBS_ANA1_EVNT_LOCK                     0x3B8
#define ENET_IF_BIST_PRBS_ANA2_COUNT_BER                     0x3BC
#define ENET_IF_BIST_PRBS_ANA2_COUNT_GOOD                    0x3C0
#define ENET_IF_BIST_PRBS_ANA2_EVNT_LOCK                     0x3C4
#define ENET_IF_BIST_PRBS_ANA3_COUNT_BER                     0x3C8
#define ENET_IF_BIST_PRBS_ANA3_COUNT_GOOD                    0x3CC
#define ENET_IF_BIST_PRBS_ANA3_EVNT_LOCK                     0x3D0

#define ENET_IF_BIST_EVENT_DDR_LOCK                          0x3D4
/************************************************************************/
#define ENET_IF_1P1_STATE_PROTECT                              0x3D8
#define ENET_IF_EG_1P1_P0_REG                                  0x3E0
#define ENET_IF_EG_1P1_P1_REG                                  0x3E4
#define ENET_IF_EG_1P1_ENABLE_REG                              0x3E8 //


#define ENET_IF_IG_1P1_P0_REG                                  0x3Ec
#define ENET_IF_IG_1P1_P1_REG                                  0x3f0
#define ENET_IF_IG_1P1_ENABLE_REG                              0x3f4 //



#define MEA_IF_INTERUPT_ENABLE                              0x3F8 /*interrupt enable*/
#define MEA_IF_REG_LAST                                     0x3FC
/************************************************************************/
/*                                                                      */
/************************************************************************/
#define MEA_IF_REG_CUSTOMER_REGS                            0x400  
#define MEA_IF_REG_CUSTOMER_REGS_END                        0x4FC



/************************************************************************/
/*                                                                      */
/************************************************************************/


/*--------------------------------------------------------------------*/
/*                                                                    */
/* BM Direct registers                                                */
/*                                                                    */
/*                                                                    */
/*--------------------------------------------------------------------*/
#define ENET_BM_STATUS                                      0x400
#define ENET_BM_CONFIG                                      0x404
#define ENET_BM_EVENT_INFO                                  0x408
#define ENET_BM_PRE_SCH_CY_LEN                              0x40c
#define ENET_BM_IA_ADDR                                     0x410
#define ENET_BM_GLOBAL_MY_MAC_LO                            0x414
#define ENET_BM_GLOBAL_MY_MAC_HI                            0x418
#define ENET_BM_GLOBAL_WALL_CLOCK_LO_REG                    0x41c
#define ENET_BM_GLOBAL_WALL_CLOCK_HI_REG                    0x420
#define ENET_BM_IA_CMD                                      0x424
#define ENET_BM_IA_STAT                                     0x428
#define ENET_BM_DESC_H_LIMIT                                0x42c
#define ENET_BM_DESC_L_LIMIT                                0x430
#define ENET_BM_RED_OFFSET                                  0x434
#define ENET_BM_NSBP                                        0x438
#define ENET_BM_HSBP                                        0x43c

#define ENET_BM_IMR                                         0x440 /*interrupt mask*/
#define ENET_BM_ENABLE                                      0x444  /*interrupt enable*/
#define ENET_BM_ISR                                         0x448 /*interrupt status*/

#define ENET_BM_EVENT1                                      0x44c
#define ENET_BM_EVENT2                                      0x450
#define ENET_BM_EVENT3                                      0x454
#define ENET_BM_EVENT4                                      0x458
#define ENET_BM_LATENCY_MODE3                               0x45c
#define ENET_BM_LATENCY_MODE2                               0x460
#define ENET_BM_LATENCY_MODE1                               0x464
#define ENET_BM_LATENCY_MODE0                               0x468

#define ENET_BM_SEARCH_BUFFER                               0x46c 
#define ENET_BM_INGFLOW_POLICER_SLOW_TICKS                  0x470 

#define ENET_BM_ECN_3_TO_0_PROF                             0x474
#define ENET_BM_ECN_7_TO_4_PROF                             0x478

#define ENET_BM_PKT_SHORT_LIMIT_REG                         0x47c
#define ENET_BM_PKT_HIGH_LIMIT_REG                          0x480
#define ENET_BM_CHUNK_SHORT_LIMIT_REG                       0x484
#define ENET_BM_CHUNK_HIGH_LIMIT_REG                        0x488
#define ENET_BM_CURRENT_PENDING_BROADCAST_PACKETS_REG       0x48c
#define ENET_BM_CURRENT_USED_BUFFERS_REG                    0x490
#define ENET_BM_CURRENT_USED_DESCRIPTORS_REG                0x494
#define ENET_BM_SHAPER_FAST_PORT_CLUSTER_REG                0x498
#define ENET_BM_SHAPER_SLOW_MULTIPLEXER_REG                 0x49c
#define ENET_BM_SCRATCH_REG                                 0x4a0
#define ENET_BM_DESCRIPTOR_COUNT_STATUS_REG                 0x4a4

#define ENET_BM_IPFRAG_CONTROL_REG                          0x4a8 
#define ENET_BM_BUFFER_COUNT_STATUS_REG                     0x4aC

#define ENET_BM_CLUSTER_SINGLE_FIRST_CLUSTER                0x4b0
#define ENET_BM_CLUSTER_SINGLE_FIRST_PRI                    0x4c0
#define ENET_BM_CLUSTER_DOUBLE_FIRST_CLUSTER                0x4b4
#define ENET_BM_CLUSTER_DOUBLE_FIRST_PRI                    0x4c4
#define ENET_BM_CLUSTER_QUAD_FIRST_CLUSTER                  0x4b8
#define ENET_BM_CLUSTER_OCTAL_FIRST_CLUSTER                 0x4bc
#define ENET_BM_CLUSTER_QUAD_FIRST_PRI                      0x4c8
#define ENET_BM_CLUSTER_OCTAL_FIRST_PRI                     0x4cc
#define ENET_BM_BEST_EFFORT_REG                             0x4d0
#define ENET_BM_DISCRIPTOR_CONF_REG                         0x4d4
#define ENET_BM_CURRENT_USED_BUFFERS_AND_DESCRIPTOR_REG     0x4dc
#define ENET_BM_SHAPER_PROFILE_OR_MASK_MSB_REG_PORT         0x4e8
#define ENET_BM_SHAPER_PROFILE_OR_MASK_LSB_REG_PORT         0x4ec
#define ENET_BM_ANALZER_ETERTYPE_REG                        0x4f4
#define ENET_BM_EHP_EVENTS_REG                              0x4f8
#define ENET_BM_SHAPER_PROFILE_OR_MASK_MSB_REG_CLUSTER      0x4fc
#define ENET_BM_SHAPER_PROFILE_OR_MASK_LSB_REG_CLUSTER      0x500
#define ENET_BM_SHAPER_PROFILE_OR_MASK_MSB_REG_PRIQUEUE     0x504
#define ENET_BM_SHAPER_PROFILE_OR_MASK_LSB_REG_PRIQUEUE     0x508
#define ENET_BM_SHAPER_FAST_PRIQUEUE_REG                    0x50c

//Write to BM table
#define ENET_BM_IA_WRDATA0                                  0x510
#define ENET_BM_IA_WRDATA1                                  0x514
#define ENET_BM_IA_WRDATA2                                  0x518
#define ENET_BM_IA_WRDATA3                                  0x51C
#define ENET_BM_IA_WRDATA4                                  0x520
#define ENET_BM_IA_WRDATA5                                  0x524
#define ENET_BM_IA_WRDATA6                                  0x528

#define ENET_BM_IA_WRDATA7                                  0x52c
#define ENET_BM_IA_WRDATA8                                  0x530
#define ENET_BM_IA_WRDATA9                                  0x534
#define ENET_BM_IA_MAX_NUM_OF_WRITE_REGS                       8 //7 //3 //currently we support only 3 regs 
#define ENET_BM_IA_WRDATA(index) (ENET_BM_IA_WRDATA0 + ((index)*4))
//Read
#define ENET_BM_IA_RDDATA0                                  0x538
#define ENET_BM_IA_RDDATA1                                  0x53c
#define ENET_BM_IA_RDDATA2                                  0x540
#define ENET_BM_IA_RDDATA3                                  0x544
#define ENET_BM_IA_RDDATA4                                  0x548
#define ENET_BM_IA_RDDATA5                                  0x54c
#define ENET_BM_IA_RDDATA6                                  0x550

#define ENET_BM_IA_RDDATA7                                  0x554
#define ENET_BM_IA_RDDATA8                                  0x558
#define ENET_BM_IA_RDDATA9                                  0x55c

#define ENET_BM_IA_MAX_NUM_OF_READ_REGS                         8 //currently we support only 7 regs 
#define ENET_BM_IA_RDDATA(index) (ENET_BM_IA_RDDATA0 + ((index)*4))
#define ENET_BM_ADD_SUB_GRN_WALL_CLOCK_REG                  0x560
#define ENET_BM_ADD_1SEC_WALL_CLOCK_THR_REG                 0x564  // 1 sec trh hold wall clock (default 0x05f5e0ff)
#define ENET_BM_WALL_CLOCK_LO_READ_REG                      0x568
#define ENET_BM_WALL_CLOCK_HI_READ_REG                      0x56c

#define ENET_BM_WRITE_ENGINE_STATE_REG                      0x570

#define ENET_BM_EVNT_ST_WRONG_TBL_REG                      0x574

#define ENET_BM_ANALZER_EVENT_READ_REG                      0x578
#define ENET_BM_CC_EVENT_READ_REG                           0x57c
#define ENET_BM_RDI_EVENT_READ_REG                          0x580

#define ENET_BM_FRAG_SCH_PORT31_0_REG                       0x584
#define ENET_BM_FRAG_SCH_PORT63_32_REG                      0x588
#define ENET_BM_DDR_BURST_SIZE_REG                          0x58C


#define ENET_BM_CCM_TICK_READ_REG                           0x590 
#define ENET_BM_RESET_PRBS_STREAM_REG                       0x594
//PPPoE_Delta_Length                                        0x598

#define ENET_BM_REORDER_TIMEOUT                             0x59c
#define ENET_BM_SRC_IP                                      0x5a0
#define ENET_BM_DDR_DESC_BURST_SIZE_REG                     0x5A4
#define ENET_BM_ACM_MODE_REG                                0x5a8

#define ENET_BM_CCM_BUCK_REG_CONF                           0x5ac 


//#define ENET_BM_DDR_HYSTERESIS_REG                         0x5b0
#define MEA_BM_BFD_CC_GROUP                                 0x5b0


#define ENET_BM_DDR_TH_REG                                 0x5b4

#define ENET_BM_DBG_COUNT_RX_REG                           0x5b8
#define ENET_BM_DBG_COUNT_FIFO_REG                         0x5bc

#define ENET_BM_TDM_RESYNC_ACK_REG                         0x5c0
#define ENET_BM_AFDX_BAG_TICKS                             0x5c4
#define ENET_BM_DDR_DESCRIPTOR_AXI_TH_REG                  0x5C8
#define ENET_BM_BMF_CCM_DBG_REG                            0x5CC 

#define ENET_BM_DESCRIPTOR_DELAY_REG                       0x5d0
#define ENET_BM_TDM_L_R_GRP0_REG                           0x5d4
#define ENET_BM_TDM_L_R_GRP1_REG                           0x5d8

#define ENET_BM_WBRG_ETH_REG                                0x5dC

#define ENET_BM_MASK_ELIG_0                                0x5e0
#define ENET_BM_MASK_ELIG_1                                0x5e4
#define ENET_BM_MASK_ELIG_2                                0x5e8
#define ENET_BM_MASK_ELIG_3                                0x5ec

#define ENET_BM_INTERNAL_DATA_MEM_CONF_STAT                0x5f0

#define ENET_BM_LQ_ID_SELECT                                 0x5f4 //LQ_id
#define ENET_BM_TM_CODE                                    0x5f8 //TM_CORE

#define ENET_BM_DDR_UINT_EVENT                            0x5fc //last reg

/*--------------------------------------------------------------------*/
/*                                                                    */
/* IF Indirect tables                                                 */
/*                                                                    */
/*                                                                    */
/*--------------------------------------------------------------------*/
#define ENET_IF_CMD_PARAM_TBL_TYP_L2CP                              0x0   /* free */

#define ENET_IF_CMD_PARAM_TBL_TYP_SNOOP_VXLAN                       0x1   

#define ENET_IF_CMD_PARAM_TBL_TYP_PARSER                            0x2

#define ENET_IF_CMD_PARAM_TBL_TYP_REG                               0x3

#define ENET_IF_CMD_PARAM_TBL_TYP_PRE_PARSER                        0x4

#define ENET_IF_CMD_PARAM_TBL_TYP_RMON_RX                           0x5

#define ENET_IF_CMD_PARAM_TBL_TYP_PORT_INTERFACE_INFO               0x6

#define ENET_IF_CMD_PARAM_TBL_CRPO                                 0x7

#define ENET_IF_CMD_PARAM_TBL_TYP_SFLOW_COUNT                      0x8

#define ENET_IF_CMD_PARAM_TBL_TYP_GW_GLOBAL                        0x9


#define ENET_IF_CMD_PARAM_TBL_TYP_MAPPING                           0xa

#define ENET_IF_CMD_PARAM_TBL_TYP_MAPPING_2                         0xb  // free 

#define ENET_IF_CMD_PARAM_TBL_TYP_VSP                               0xc
#define ENET_IF_CMD_PARAM_TBL_TYP_VSP_LENGTH   \
    ((mea_drv_Get_DeviceInfo_IsVspExist(MEA_UNIT_0,MEA_HW_UNIT_ID_GENERAL)) ? 128 : 0)

/* Internal Service*/
#define ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_ENTRY                     0xd /* 13 */
#define ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_ENTRY_NUM_OF_REGS(hwUnit_i) 4
#define ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_ENTRY_LENGTH(unit_i,hwUnit_i)   mea_drv_Get_DeviceInfo_NumOfService((unit_i),(hwUnit_i)) 
/* Internal Action*/
#define ENET_IF_CMD_PARAM_TBL_TYP_ACTION                            0xe /* 14 */
#define ENET_IF_CMD_PARAM_TBL_TYP_ACTION_NUM_OF_REGS(hwUnit_i)        4 //8   //((mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(MEA_UNIT_0, MEA_DB_ACTION_TYPE)== MEA_TRUE)  ? 8 : 3)

#define ENET_IF_CMD_PARAM_TBL_TYP_ACTION_NUM_OF_REGS_Internal(hwUnit_i)     (mea_action_calc_numof_regs((MEA_UNIT_0),(hwUnit_i)))  //3






//#define MEA_ACTION_ID_TO_CPU_UPSTREAM    MEA_ACTION_ID_TO_CPU
#define MEA_ACTION_ID_TO_CPU_DOWNSTREAM (MEA_ACTION_TBL_FWD_START_INDEX)


#define ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_ACTION_LENGTH(hwUnit_i)       ( MEA_Platform_SERVICE_ACTION_LENGTH_calculate((hwUnit_i)) )
#define ENET_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_LENGTH(hwUnit_i)           (MEA_Platform_FWD_ACTION_LENGTH_calculate((hwUnit_i)))
#define ENET_IF_CMD_PARAM_TBL_TYP_FILTER_ACTION_LENGTH(hwUnit_i)        mea_drv_Get_DeviceInfo_NumOfACL(MEA_UNIT_0,((hwUnit_i)))
#define ENET_IF_CMD_PARAM_TBL_TYP_HPM_ACTION_LENGTH(hwUnit_i)           mea_drv_Get_DeviceInfo_HPM_numof_Act(MEA_UNIT_0,((hwUnit_i)))

#define ENET_IF_CMD_PARAM_TBL_TYP_EXTERNAL_SERVICE_ACTION_LENGTH(hwUnit_i)    ( MEA_MAX_NUM_OF_SERVICES_EXTERNAL_SW )
#define ENET_IF_CMD_PARAM_TBL_TYP_EXTERNAL_ACL5_ACTION_LENGTH(hwUnit_i)       ( MEA_MAX_NUM_OF_ACL5_EXTERNAL_SW )


#define ENET_IF_CMD_PARAM_TBL_TYP_ACTION_LENGTH(hwUnit_i)               (mea_drv_Get_DeviceInfo_NumOfActions(MEA_UNIT_0,(hwUnit_i))) //internal



#define ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_ACTION_START(hwUnit_i)      (0)


#define ENET_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_START(hwUnit_i) (MEA_Platform_FWD_ACTION_START_calculate(hwUnit_i))

#define ENET_IF_CMD_PARAM_TBL_TYP_FILTER_ACTION_START(hwUnit_i)(mea_drv_Get_DeviceInfo_NumOfActions_noACL(MEA_UNIT_0,(hwUnit_i)))

#define ENET_IF_CMD_PARAM_TBL_TYP_XXX_ACTION_LENGTH(hwUnit_i,type_i)  \
	((type_i == MEA_ACTION_TYPE_SERVICE  ) ?                          \
	    ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_ACTION_LENGTH((hwUnit_i)) : \
     (type_i == MEA_ACTION_TYPE_FWD      ) ?                          \
	    ENET_IF_CMD_PARAM_TBL_TYP_FWD_ACTION_LENGTH    ((hwUnit_i)) : \
     (type_i == MEA_ACTION_TYPE_FILTER   ) ?                          \
	    ENET_IF_CMD_PARAM_TBL_TYP_FILTER_ACTION_LENGTH ((hwUnit_i)) : \
     (type_i == MEA_ACTION_TYPE_HPM      ) ?                          \
         ENET_IF_CMD_PARAM_TBL_TYP_HPM_ACTION_LENGTH((hwUnit_i))     : \
     (type_i == MEA_ACTION_TYPE_SERVICE_EXTERNAL   ) ?                          \
	    ENET_IF_CMD_PARAM_TBL_TYP_EXTERNAL_SERVICE_ACTION_LENGTH ((hwUnit_i)) : \
     (type_i == MEA_ACTION_TYPE_ACL5   ) ?                          \
	    ENET_IF_CMD_PARAM_TBL_TYP_EXTERNAL_ACL5_ACTION_LENGTH ((hwUnit_i)) : \
        0)

#define MEA_SERVICE_RANGE_NUM_OF_PROFILE                (mea_drv_Get_DeviceInfo_ServiceRange_numofRangeProfile(MEA_UNIT_0))
#define MEA_SERVICE_RANGE_MAX_NUM_SERVICES_PER_PORT     (mea_drv_Get_DeviceInfo_ServiceRange_Num_of_Service_in_port(MEA_UNIT_0))

#define ENET_IF_CMD_PARAM_TBL_SERVICE_VLAN_RANGE                    0xf // 15
#define ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_VLAN_RANGE_LENGTH         (MEA_SERVICE_RANGE_MAX_NUM_SERVICES_PER_PORT * MEA_SERVICE_RANGE_NUM_OF_PROFILE)
#define ENET_IF_CMD_PARAM_TBL_TYP_SERVICE_VLAN_RANGE_NUM_OF_REGS    (5)

#define ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION                      0x10 // 16
#define ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(hwUnit_i) ( mea_drv_Get_DeviceInfo_NumOfXpermission((MEA_UNIT_0),(hwUnit_i)))

#define ENET_IF_CMD_PARAM_TBL_TYP_FILTER_MASK_PROF                 0x11 // 17
#define ENET_IF_CMD_PARAM_TBL_TYP_FILTER_MASK_PROF_NUM_OF_REGS(hwUnit_i) 2
#define ENET_IF_CMD_PARAM_TBL_TYP_FILTER_MASK_PROF_LENGTH(hwUnit_i) (64*2)

#define ENET_IF_CMD_PARAM_TBL_TYP_FILTER_CTX                        0x12 // 18
#define ENET_IF_CMD_PARAM_TBL_TYP_FILTER_CTX_NUM_OF_REGS(hwUnit_i)    4
#define ENET_IF_CMD_PARAM_TBL_TYP_FILTER_CTX_LENGTH(hwUnit_i)           mea_drv_Get_DeviceInfo_NumOfACL_contex(MEA_UNIT_0,((hwUnit_i)))   //mea_drv_Get_DeviceInfo_NumOfACL(MEA_UNIT_0,((hwUnit_i))) // 256

#define ENET_IF_CMD_LIMITER                 				       0x13 // 19
#define ENET_IF_CMD_LIMITER_LENGTH(unit_i,hwUnit_i) (mea_drv_Get_DeviceInfo_NumOfLimiters((unit_i),(hwUnit_i)))

#define ENET_IF_CMD_PARAM_TBL_TYP_LXCP                             0x14 // 20
#define ENET_IF_CMD_PARAM_TBL_TYP_LXCP_NUM_OF_PROF(hwUnit_i)       (mea_drv_Get_DeviceInfo_Lxcp_num_of_prof((MEA_UNIT_0),(hwUnit_i))) 

#define ENET_IF_CMD_PARAM_TBL_TYP_LXCP_LENGTH(hwUnit_i)   \
    (ENET_IF_CMD_PARAM_TBL_TYP_LXCP_NUM_OF_PROF((hwUnit_i)) * \
     MEA_LXCP_PROTOCOL_LAST)
#define ENET_IF_CMD_PARAM_TBL_TYP_LXCP_NUM_OF_REGS(hwUnit_i)       1

#define ENET_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION                      0x15 // 21
#define ENET_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION_LENGTH(hwUnit_i)        (MEA_Platform_Get_DeviceInfo_NumOfLxcpAction((hwUnit_i)))
#define ENET_IF_CMD_PARAM_TBL_TYP_LXCP_ACTION_NUM_OF_REGS(hwUnit_i)   1

#define ENET_IF_CMD_PARAM_TBL_BONDING_PORT_TO_GROUT                 0x16  //22 

#define ENET_IF_CMD_PARAM_TBL_protction_Port_To_Cluster             0x17 // 23  free 

#define ENET_IF_CMD_PARAM_TBL_protction_Static_MASK_cluster                 0x18 // 24 free 

#define ENET_IF_CMD_PARAM_TBL_classifier_TDM                       0x19 // 25 free 
  

#define ENET_IF_CMD_PARAM_TBL_LAG_GROUP_ID                         0x1A // 26
#define ENET_IF_CMD_PARAM_TBL_LAG_GROUP_ID_T0_CLUSTER              0x1B // 27
#define ENET_IF_CMD_PARAM_TBL_LAG_MASK_INTERFACE_CLUSTER           0x1C // 28

#define ENET_IF_CMD_PARAM_TBL_ADM_PORT                             0x1D // 29                                                                   /*29*/ /*free*/
#define ENET_IF_CMD_PARAM_TBL_TFT_LPM_PROF                         0x1E // 30
#define ENET_IF_CMD_PARAM_TBL_TFT_MASK_PROF                        0x1F // 31

#define ENET_IF_CMD_PARAM_TBL_TYP_DEVICE_CONTEXT                   0x20  // 32  
#define ENET_IF_CMD_PARAM_TBL_TYP_DEVICE_CONTEXT_LENGTH              15

#define ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_STREAM               0x21   //33
#define ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_LENGTH               (MEA_Platform_Get_PacketGen_MaxNumOfStreams_Type1() * 8)
#define ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_NUM_OF_REGS          3

#define ENET_IF_CMD_PARAM_TBL_TYPE_INGRESS_MTU                    0x22 //34 free 
#define ENET_IF_CMD_PARAM_TBL_TYPE_INGRESS_MTU_LENGTH             128
#define ENET_IF_CMD_PARAM_TBL_TYPE_INGRESS_MTU_NUM_OF_REGS        1


#define MEA_MAPPING_COS_PROFILE 64 

#define ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING                    0x23  // 35
#define ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_PROFILES(hwUnit_i) \
    ((MEA_FLOW_COS_MAPPING_PROFILE_SUPPORT == MEA_FALSE) ? 0 : MEA_MAPPING_COS_PROFILE)
#define ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS(hwUnit_i) \
    (MEA_FLOW_COS_MAPPING_MAX_NUM_OF_ITEMS/2) /* 2 items in one row */
#define ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_LENGTH(hwUnit_i)   \
    (ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_PROFILES((hwUnit_i)) * \
     ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_NUM_OF_ITEMS((hwUnit_i)))
#define ENET_IF_CMD_PARAM_TBL_TYPE_COS_MAPPING_OF_REGS(hwUnit)       1

#define MEA_MAPPING_MARK_PROFILE 64 

#define ENET_IF_CMD_PARAM_TBL_TYPE_MARKING_MAPPING                    0x24  // 36
#define ENET_IF_CMD_PARAM_TBL_TYPE_MARKING_MAPPING_NUM_OF_PROFILES(hwUnit_i) \
    ((MEA_FLOW_MARKING_MAPPING_PROFILE_SUPPORT == MEA_FALSE) ? 0 : MEA_MAPPING_MARK_PROFILE)
#define ENET_IF_CMD_PARAM_TBL_TYPE_MARKING_MAPPING_NUM_OF_ITEMS(hwUnit_i) \
    (MEA_FLOW_MARKING_MAPPING_MAX_NUM_OF_ITEMS/2) /* 2 items in one row */
#define ENET_IF_CMD_PARAM_TBL_TYPE_MARKING_MAPPING_LENGTH(hwUnit_i)   \
    (ENET_IF_CMD_PARAM_TBL_TYPE_MARKING_MAPPING_NUM_OF_PROFILES((hwUnit_i)) * \
     ENET_IF_CMD_PARAM_TBL_TYPE_MARKING_MAPPING_NUM_OF_ITEMS((hwUnit_i)))
#define ENET_IF_CMD_PARAM_TBL_TYPE_MARKING_MAPPING_OF_REGS(hwUnit)       1


#define ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_PROF_INFO                     0x25 //37
#define ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_PROF_INFO_LENGTH               8
#define ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_PROF_INFO_NUM_OF_REGS          1


#define ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_STREAM_TYPE2               0x26   // 38
#define ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_LENGTH_TYPE2               (MEA_Platform_Get_PacketGen_MaxNumOfStreams_Type2() * 16)
#define ENET_IF_CMD_PARAM_TBL_TYPE_PACKETGEN_TYPE2_NUM_OF_REGS            3

#define ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_FIFO_WATERMARK               0x27   // 39     /* free */

#define ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_VSP                          0x28   // 40
#define ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_VSP_NUM_OF_REGS                1
#define ENET_IF_CMD_PARAM_TBL_TYPE_FRAGMENT_VSP_LENGTH              (MEA_FRAGMENT_VSP_NUM_OF_PORT * MEA_FRAGMENT_VSP_NUM_OF_GROUP * MEA_FRAGMENT_VSP_NUM_OF_SESSION)


#define ENET_IF_CMD_PARAM_TBL_TYPE_INGRESS_PAUSE                             0x29   // 41
#define ENET_IF_CMD_PARAM_TBL_TYPE_EGRESS_MAC_MASK                          0x2a   // 42

#define ENET_IF_CMD_PARAM_TBL_TYPE_TDM_INTERFAC_A_CHANEL_INFO                0x2a   // 42
#define ENET_IF_CMD_PARAM_TBL_TYPE_TDM_INTERFAC_B_CHANEL_INFO                0x2b   // 43
#define ENET_IF_CMD_PARAM_TBL_TYPE_TDM_INTERFAC_C_CHANEL_INFO                0x2c   // 44

#define ENET_IF_CMD_PARAM_TBL_TYPE_TDM_TIME_SLOT                             0x2d  // 45
#define ENET_IF_CMD_PARAM_TBL_TYPE_TDM_CES_INFO                              0x2e  // 46
#define ENET_IF_CMD_PARAM_TBL_TYPE_TDM_PROF_LBK_INFO                         0x2f  // 47 
#define ENET_IF_CMD_PARAM_TBL_TYPE_TDM_RMON                                  0x30  // 48 

 

#define ENET_IF_CMD_PARAM_TBL_POS_RX1_INGRESS_SHAPER                          0x31  // 49  free
#define ENET_IF_CMD_PARAM_TBL_POS_RX2_INGRESS_SHAPER                          0x32  // 50  free 


/* New CAM parser*/
#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12                              0x33  // 51
#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12_LENGTH               8

#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16                              0x34  // 52
#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16_LENGTH               8

#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20                              0x35  //53
#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20_LENGTH               5

#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34                              0x36  //54
#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34_LENGTH               2

#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38                              0x37  //55
#define ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38_LENGTH               2

#define ENET_IF_CMD_PARAM_TBL_TYPE_CONF_PROTOCOL                              0x38  //56
#define ENET_IF_CMD_PARAM_TBL_TYPE_CONF_PROTOCOL_LENGTH                       18

#define ENET_IF_CMD_PARAM_TBL_L2_TO_L3_1588_OFFSET                                0x39  //57  /*L2type to  L3 offset*/
#define ENET_IF_CMD_PARAM_TBL_L2_TO_L3_1588_LENGT                                  32

#define ENET_IF_CMD_PARAM_TBL_1588_CPU_OFFSET                                     0x3a  //58

#define ENET_IF_CMD_PARAM_TBL_RANGE_PORT_INDX_OFFSET                               0x3b  //59  //free 
#define ENET_IF_CMD_PARAM_TBL_RANGE_PORT_INDEX_LENGTH                             128




#define ENET_IF_CMD_PARAM_TBL_CLOCK_RECOVER_OFFSET                             0x3c  //60  //free

#define ENET_IF_CMD_PARAM_TBL_TDM_LB_CHANNEL_INFO_EVENT                        0x3d  //61 

#define ENET_IF_CMD_PARAM_TBL_UTOPIA_RMON_COUNTER_TX                              0x3e // 62 TX    //free
#define ENET_IF_CMD_PARAM_TBL_UTOPIA_RMON_COUNTER_TX_LENGTH                        256  //(src_port)

#define ENET_IF_CMD_PARAM_TBL_DA_MAC_FILTER                                    0x3f // 63   // free 
#define ENET_IF_CMD_PARAM_TBL_DA_MAC_FILTER_LENGTH                             4    //(only port 48 72 125 126)


#define ENET_IF_CMD_PARAM_TBL_OAM_CONFIG                                       0x40 // 64 (on version 387)  /free 

#define ENET_IF_CMD_PARAM_TBL_RMON_UTOPIA_RX                                   0x41 //65     //free 
#define ENET_IF_CMD_PARAM_TBL_RMON_UTOPIA_RX_LENGTH                            256   //vsp index


#define ENET_IF_CMD_PARAM_TBL_RMON_UTOPIA_RX_CAM_DROP_CELL                     0x42 //66 //free 
#define ENET_IF_CMD_PARAM_TBL_RMON_UTOPIA_RX_CAM_DROP_CELL_LENGTH              256


#define ENET_IF_CMD_PARAM_TBL_TDM_EGRESS_CHECKR                                0x43 //67


#define ENET_IF_CMD_PARAM_TBL_STANDART_BONDING_RX_PORT_TO_BONDING_GROUPID     0x44 //68

#define ENET_IF_CMD_PARAM_TBL_69_FREE                                        0x45 //69

#define ENET_IF_CMD_PARAM_TBL_INTERLAKEN_CONTROL                              0x46 //70
#define ENET_IF_CMD_PARAM_TBL_XLAUI_CONTROL                                   0x46 //70

#define ENET_IF_CMD_PARAM_TBL_RX_MAC_DROP_PACKET                              0x47 //71

#define ENET_IF_CMD_PARAM_TBL_IL_DROP_PACKET                                  0x48 //72

#define ENET_IF_CMD_PARAM_TBL_HDC_ALIGNER                                     0x49  // 73   /free 

#define ENET_IF_CMD_PARAM_TBL_HC_COMPRESS_BIT_PROF              0x4a  // 74
#define ENET_IF_CMD_PARAM_TBL_HC_DECOMPRESS_BIT_PROF            0x4b  // 75
#define ENET_IF_CMD_PARAM_TBL_HC_DECOMPRESS_BYTE_MAP_PROF       0x4c  // 76
#define ENET_IF_CMD_PARAM_TBL_HC_DECOMPRESS_CONTEXT_PROF        0x4d  // 77 //learn
#define ENET_IF_CMD_PARAM_TBL_HC_PROF_COMPRESS_BYTE_COUNT       0x4e  // 78
#define ENET_IF_CMD_PARAM_TBL_HC_PROF_DECOMPRESS_BYTE_COUNT     0x4f  // 79 
#define ENET_IF_CMD_PARAM_TBL_HC_RMON_COUNTERS_COMP            0x50  // 80
#define ENET_IF_CMD_PARAM_TBL_HC_CLASS_CONTEX                  0x51  // 81

#define ENET_IF_CMD_PARAM_TBL_82_FREE                                        0x51 //82

#define ENET_IF_CMD_PARAM_TBL_HC_L2L3_PROF_ID                  0x53  // 83
#define ENET_IF_CMD_TBL_HC_CLASS                               0x54  // 84

#define ENET_IF_CMD_TBL_MSTP_VPN                               0x55  // 85 

#define ENET_IF_CMD_PARAM_TBL_EGRESS_DEC_PARSER                 0x56  // 86
#define ENET_IF_CMD_PARAM_TBL_EGRESS_CLASSIFIER_RANGE           0x57  // 87  //free 
#define ENET_IF_CMD_PARAM_TBL_EGRESS_CLASSIFIER_CONTEXT         0x58  // 88



#define ENET_IF_CMD_PARAM_TBL_TEID_ACL_PROF                      0x59 //89

#define ENET_IF_CMD_PARAM_TBL_1588_MODE_TYPE                      0x5A  // 90
#define ENET_IF_CMD_PARAM_TBL_1588_DELAY_TYPE                     0x5B  // 91
#define ENET_IF_CMD_PARAM_TBL_1588_MESSAGE_INFO                   0x5C  // 92

#define ENET_IF_CMD_PARAM_TBL_LOOP_SRCPORT_TO_CLUSTER            0x5d  // 93  
#define ENET_IF_CMD_PARAM_TBL_QGMII_INTERFACE                    0x5e  // 94  //free 

#define ENET_IF_CMD_PARAM_TBL_L2TYPE_DEFUALT_SID                 0x5f  // 95
#define ENET_IF_CMD_PARAM_TBL_LACP_L2CP_DEFUALT_SID              0x60  // 96
#define ENET_IF_CMD_PARAM_TBL_ENB_PROF_MAC                       0x61  // 97


#define ENET_IF_CMD_PARAM_TBL_99_HASH                           0x63  // 99  /*Last*/

#define ENET_IF_CMD_PARAM_TBL_100_REGISTER                      0x64  // IF_TBL_100



#define ENET_IF_CMD_PARAM_TBL_101_VRMON                        0x65  // IF_TBL_101

#define ENET_IF_CMD_PARAM_TBL_102_REGISTER                      0x66  // IF_TBL_102

#define ENET_IF_CMD_PARAM_TBL_RANGE_ACL5                       0x69  // 105    //320bit
#define ENET_IF_CMD_PARAM_TBL_IPMASK_ACL5                      0x6a  // 106 //28bit
#define ENET_IF_CMD_PARAM_TBL_KEYMASK_ACL5                     0x6B  //107 //17bit

#define ENET_IF_CMD_PARAM_TBL_LPM_PATTERN                     108                                 
#define ENET_IF_CMD_PARAM_TBL_LPM_CONTEX                      109





/*--------------------------------------------------------------------*/ 
/*                                                                    */ 
/* BM Indirect tables                                                 */ 
/*                                                                    */ 
/*                                                                    */ 
/*--------------------------------------------------------------------*/ 
#define ENET_BM_TBL_TYP_EHP_ETH                                     0x0
#define ENET_BM_TBL_TYP_EHP_ETH_NUM_OF_REGS(hwUnit_i)                 8 //2             
#define ENET_BM_TBL_TYP_EHP_ETH_LENGTH(hwUnit_i)                    (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i)))

#define ENET_BM_TBL_TYP_EHP_ATM                                     0x1
#define ENET_BM_TBL_TYP_EHP_ATM_NUM_OF_REGS(hwUnit_i)                 2
#define ENET_BM_TBL_TYP_EHP_ATM_LENGTH(hwUnit_i)                    (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i)))




#define ENET_BM_TBL_TYP_EHP_DA                                  0x2
#define ENET_BM_TBL_TYP_EHP_DA_NUM_OF_REGS(hwUnit_i)              2
#define ENET_BM_TBL_TYP_EHP_DA_LENGTH(hwUnit_i)                 (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i)))

#define ENET_BM_TBL_TYP_EHP_SA_LSS                                  0x3
#define ENET_BM_TBL_TYP_EHP_SA_LSS_NUM_OF_REGS(hwUnit_i)              1
#define ENET_BM_TBL_TYP_EHP_SA_LSS_LENGTH(hwUnit_i)                 (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i)))

#define ENET_BM_TBL_TYP_EHP_PROFILE_ID                              0x4
#define ENET_BM_TBL_TYP_EHP_PROFILE_ID_NUM_OF_REGS(hwUnit_i)          1
#define ENET_BM_TBL_TYP_EHP_PROFILE_ID_LENGTH(hwUnit_i)             (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i))) 

#define ENET_BM_TBL_TYP_EHP_PROFILE                                 0x5
#define ENET_BM_TBL_TYP_EHP_PROFILE_NUM_OF_REGS(hwUnit_i)             1
#define ENET_BM_TBL_TYP_EHP_PROFILE_STAMPING_LENGTH(hwUnit_i)         4
#define ENET_BM_TBL_TYP_EHP_PROFILE_SA_DSS_LENGTH(hwUnit_i)           4
#define ENET_BM_TBL_TYP_EHP_PROFILE_SA_MSS_LENGTH(hwUnit_i)           4
#define ENET_BM_TBL_TYP_EHP_PROFILE_ETHER_TYPE_LENGTH(hwUnit_i)       4
#define ENET_BM_TBL_TYP_EHP_PROFILE_LENGTH(hwUnit_i)                 \
        (ENET_BM_TBL_TYP_EHP_PROFILE_STAMPING_LENGTH  ((hwUnit_i)) + \
         ENET_BM_TBL_TYP_EHP_PROFILE_SA_DSS_LENGTH    ((hwUnit_i)) + \
         ENET_BM_TBL_TYP_EHP_PROFILE_SA_MSS_LENGTH    ((hwUnit_i)) + \
         ENET_BM_TBL_TYP_EHP_PROFILE_ETHER_TYPE_LENGTH((hwUnit_i)) )
#define ENET_BM_TBL_TYP_EHP_PROFILE_STAMPING_START(hwUnit_i)         \
	    (0)
#define ENET_BM_TBL_TYP_EHP_PROFILE_SA_DSS_START(hwUnit_i)           \
	    (ENET_BM_TBL_TYP_EHP_PROFILE_STAMPING_START   ((hwUnit_i)) + \
	     ENET_BM_TBL_TYP_EHP_PROFILE_STAMPING_LENGTH  ((hwUnit_i)) )
#define ENET_BM_TBL_TYP_EHP_PROFILE_SA_MSS_START(hwUnit_i)           \
	    (ENET_BM_TBL_TYP_EHP_PROFILE_SA_DSS_START     ((hwUnit_i)) + \
	     ENET_BM_TBL_TYP_EHP_PROFILE_SA_DSS_LENGTH    ((hwUnit_i)) )
#define ENET_BM_TBL_TYP_EHP_PROFILE_ETHER_TYPE_START(hwUnit_i)       \
	    (ENET_BM_TBL_TYP_EHP_PROFILE_SA_MSS_START     ((hwUnit_i)) + \
	     ENET_BM_TBL_TYP_EHP_PROFILE_SA_MSS_LENGTH    ((hwUnit_i)) )


#define ENET_BM_TBL_TYP_EHP_SA_LSS                                  0x3

#define ENET_BM_TBL_TYP_EHP_PROFILE_ID                              0x4

#define ENET_BM_TBL_TYP_EHP_PROFILE                                 0x5




#define ENET_BM_TBL_TYP_GW_GLOBAL_REGS                              0x7

#define ENET_BM_TBL_TYP_BFD_STREAM_COUNTER                          0x8 // 8
#define ENET_BM_TBL_TYP_BFD_CC_EVENT                                0x9 // 9
#define ENET_BM_TBL_TYP_BFD_STREAM_EXPECTED_CONFIG                           0xa // 10  //Valid counter
#define ENET_BM_TBL_TYP_BFD_CC_BUCKET_PROF                          0xb // 11
#define ENET_BM_TBL_PERIOD_BUCKET_STREAM_BFD                        0x11  // 17 


//#define ENET_BM_TBL_TYP_EHP_PPPoE                                   0xc // 12
#define ENET_BM_TBL_TYP_WALL_CLOCK_REGS                               0xc // 12


#define ENET_BM_TBL_TYP_IP_FRAG_STAT                           0xd // 13
 

#define ENET_BM_TBL_PORT_PROTECT                                    0xe // 14

#define ENET_BM_TBL_TYP_INGRESS_PORT_MTU_SIZE                       0xf // 15
#define ENET_BM_TBL_TYP_ACT_MTU_PROFILE_DROP                         ENET_BM_TBL_TYP_INGRESS_PORT_MTU_SIZE 
 // #define ENET_BM_TBL_TYP_ACT_MTU_PROFILE_DROP_LENGTH                  512
#define ENET_BM_TBL_TYP_ACT_MTU_PROFILE_DROP_NUM_OF_REGS        1
#define ENET_BM_TBL_TYP_ACT_MTU_PROFILE_DROP_LENGTH(hwUnit_i)  (mea_drv_Get_DeviceInfo_NumOf_AC_MTUs(MEA_UNIT_0,(hwUnit_i)) )




#define ENET_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS             0x10 // 16
#define ENET_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_LENGTH         5

#define ENET_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS            0x11 // 17
#define ENET_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS_LENGTH        3

#define ENET_BM_TBL_TYP_CLUSTER_DATABASE                           0x12 //18

#define ENET_BM_TBL_TYP_CLUSTER_TO_PORT                            0x13 // 19
#if defined(MEA_ENV_256PORT)
#define ENET_BM_TBL_TYP_CLUSTER_TO_PORT_LENGTH                     1024
#else
#define ENET_BM_TBL_TYP_CLUSTER_TO_PORT_LENGTH                      (1024) //512  //128 ????
#endif

#define ENET_BM_TBL_TYP_CLUSTER_PORT_INFORMATION                   0x14 // 20
#if defined(MEA_ENV_256PORT)
#define ENET_BM_TBL_TYP_CLUSTER_PORT_INFORMATION_LENGTH             256  
#else
#define ENET_BM_TBL_TYP_CLUSTER_PORT_INFORMATION_LENGTH             128                /* 256   */
#endif



#define ENET_BM_TBL_TYP_CLUSTER_PQ_ELIGIBILITY_INFORMATION         0x18 // 24

#define ENET_BM_TBL_TYP_CLUSTER_WFQ_INFORMATION                    0x19 // 25
#define ENET_BM_TBL_TYP_QUEUE_ARBITER                             0x1a // 26  //HW only



#define ENET_BM_TBL_TYP_CLUSTER_PQ_WFQ_INFORMATION                 0x1c // 28

#define ENET_BM_TBL_TYP_CLUSTER_PQ_AVAILABILITY_INFORMATION        0x1d // 29

#define ENET_BM_TBL_TYP_EHP_MULTICAST                              0x1e // 30

#define ENET_BM_TBL_TYP_SCHEDULER_PRIORITY                         0x1f //31
#define ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH(hwUnit_i)  \
     ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? ( (MEA_TDM_SUPPORT==MEA_TRUE) ? 2048 : 512 ) : \
      (((hwUnit_i) == MEA_HW_UNIT_ID_UPSTREAM) ? 512 : 512))



#define ENET_BM_TBL_TYP_SCHEDULER_PRIORITY_NUM_OF_REGS(hwUnit_i)      1


#define ENET_BM_TBL_TYP_CNT_PM_READ_BYTE                           0x20 // 32 /*All byte*/
#define ENET_BM_TBL_TYP_CNT_PM_READ_PKT                            0x21 // 33


#define ENET_BM_TBL_TYP_CNT_PM_XXX_LENGTH(unit_i,hwUnit_i)         (mea_drv_Get_DeviceInfo_NumOfPmIds((unit_i),(hwUnit_i)))

#define ENET_BM_TBL_SHAPER_BUCKET_PORT                             0x28 // 40
#define ENET_BM_TBL_SHAPER_BUCKET_PORT_LENGTH                       256

#define ENET_BM_TBL_SHAPER_PROFILE_PORT                            0x29 // 41
#define ENET_BM_TBL_SHAPER_PROFILE_PORT_IS_ACM_SUPPORT(unit_i,hwUnit_i) \
        (mea_drv_Get_DeviceInfo_IsAcmSupport((unit_i),(hwUnit_i))) 
#define ENET_BM_TBL_SHAPER_PROFILE_PORT_LENGTH(unit_i,hwUnit_i)   ((MEA_Uint16)((mea_drv_Get_DeviceInfo_NumOfAcmModes((unit_i),(hwUnit_i))) * (mea_drv_Get_DeviceInfo_Get_numOf_Shaper_Profile((unit_i),(hwUnit_i))) ))
         
#define ENET_BM_TBL_SHAPER_PROFILE_PORT_NUM_OF_ACM_MODES(unit_i,hwUnit_i) ((MEA_Uint16) mea_drv_Get_DeviceInfo_NumOfAcmModes((unit_i),(hwUnit_i)))
        
#define ENET_BM_TBL_SHAPER_PROFILE_PORT_LENGTH_PER_ACM(unit_i,hwUnit_i)  \
        (ENET_BM_TBL_SHAPER_PROFILE_PORT_LENGTH((unit_i),(hwUnit_i)) / \
         ENET_BM_TBL_SHAPER_PROFILE_PORT_NUM_OF_ACM_MODES((unit_i),(hwUnit_i)))




#define ENET_BM_TBL_TYP_CNT_QUEUE_MTU_DROP                         0x2a  // 42 /* this memory is not valid at enet300 but we read */
#define ENET_BM_TBL_TYP_CNT_QUEUE_MTU_DROP_NUM_OF_REGS(hwUnit_i)    1
#define ENET_BM_TBL_TYP_CNT_QUEUE_MTU_DROP_LENGTH(unit_i,hwUnit_i) ((MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE) \
                                                                    ? (256*8) : 256)     
#define ENET_BM_TBL_TYP_CNT_FLOW_MTU_DROP                         0x2a 

#define ENET_BM_TBL_TYP_QUEUE_MTU_SIZE                             0x2b // 43
#define ENET_BM_TBL_TYP_QUEUE_MTU_NUM_OF_REGS(hwUnit_i)            1
#define ENET_BM_TBL_TYP_QUEUE_MTU_LENGTH(unit_i,hwUnit_i)         ( (MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE) \
                                                                     ? (256*8) : 256)  /*Alex 256 clusters*/

#define ENET_BM_TBL_TYP_PRI_DROP_PROP                              0x2c // 44

#define ENET_BM_TBL_TYP_CURRENT_Q_SIZE                             0x2d // 45


#define ENET_BM_TBL_TYP_INGRESS_RATE_METERING_TM2                  0x2F // 47
#define ENET_BM_TBL_TYP_INGRESS_RATE_METERING_PROF                 0x30 // 48

#define ENET_BM_TBL_TYP_INGRESS_RATE_METERING_BUCKET               0x31  //49




#define ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF                     0x32 // 50
#define ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_NUM_OF_REGS(hwUnit_i)  3
#define ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_LENGTH(hwUnit_i)      (mea_drv_Get_DeviceInfo_NumOfPolicerProfiles((MEA_UNIT_0),(hwUnit_i))== 0 ) ? (1) :    (mea_drv_Get_DeviceInfo_NumOfPolicerProfiles((MEA_UNIT_0),(hwUnit_i)))

#define ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET                   0x33  // 51
#define ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_NUM_OF_REGS(hwUnit_i) 2
#define ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_LENGTH(hwUnit_i)   \
     ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) \
      ? ((mea_drv_Get_DeviceInfo_NumOfPolicers(MEA_UNIT_0,(hwUnit_i))*2)*2) \
      :  (mea_drv_Get_DeviceInfo_NumOfPolicers(MEA_UNIT_0,(hwUnit_i))*2))
#define ENET_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_SW_LENGTH(unit_i,hwUnit_i) \
                            mea_drv_Get_DeviceInfo_NumOfPolicers((unit_i),(hwUnit_i))

#define ENET_BM_TBL_TYP_EGRESS_PORT_MQS                            0x34 // 52
#define ENET_BM_TBL_TYP_CNT_QUEUE_MQS_DROP                         0x35  // 53



 //#define ENET_BM_TBL_TYP_54                                         0x36  // 54 /* was before pswrr */
#define ENET_BM_TBL_TYP_AFDX_REDUNDANCY_BAG_CONFIGURE                  0x36  // 54 

#define ENET_BM_TBL_TYP_BUF_RESOURCE_LIMIT_DROP_PACKET_CNT         0x37 // 55

#define ENET_BM_TBL_TYP_DESC_RESOURCE_LIMIT_DROP_PACKET_CNT        0x38 // 56

#define ENET_BM_TBL_HALT_FLUSH_CMD_NOT_ACTIVE                      0x39 // 57

#define ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT              0x3b // 59
#define ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT_NUM_OF_REGS(hwUnit_i) 1
#define ENET_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT_LENGTH(hwUnit_i) \
     ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) \
      ? ((mea_drv_Get_DeviceInfo_NumOfPolicers(MEA_UNIT_0,(hwUnit_i))*2)*2) \
      :  (mea_drv_Get_DeviceInfo_NumOfPolicers(MEA_UNIT_0,(hwUnit_i))*2))


#define ENET_BM_TBL_EVENT_STATE                                    0x3C // 60
#define ENET_BM_TBL_ANALZER_CNT                                    0x3d  // 61
#define ENET_BM_TBL_ANALZER_CNT_NUM_OF_REGS                        3
#define ENET_BM_TBL_ANALZER_CNT_LENGTH                            (MEA_PACKET_ANALYZER_SUPPORT == MEA_TRUE ? MEA_ANALYZER_MAX_STREAMS : 0)


#define ENET_BM_TBL_TYP_EDITING_MAPPING                            0x3e // 62
#define ENET_BM_TBL_TYP_EDITING_MAPPING_NUM_OF_REGS(hwUnit_i)      2

#define ENET_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE         16
#define ENET_BM_TBL_TYP_EDITING_MAPPING_NUM_OF_PROFILES(hwUnit_i) \
       ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 4 : 0)
#define ENET_BM_TBL_TYP_EDITING_MAPPING_LENGTH(hwUnit_i)      \
       (ENET_BM_TBL_TYP_EDITING_MAPPING_NUM_OF_PROFILES((hwUnit_i)) * \
        ENET_BM_TBL_TYP_EDITING_MAPPING_LENGTH_PER_PROFILE)

#define ENET_BM_TBL_CCM_CNT                                    0x3f // 63
#define ENET_BM_TBL_CCM_CNT_NUM_OF_REGS                        3
#define ENET_BM_TBL_CCM_CNT_LENGTH                            (MEA_PACKET_ANALYZER_SUPPORT == MEA_TRUE ? MEA_ANALYZER_MAX_STREAMS : 0)


#define ENET_BM_TBL_SHAPER_BUCKET_CLUSTER                             0x40 //64
#define ENET_BM_TBL_SHAPER_BUCKET_CLUSTER_LENGTH(unit_i,hwUnit_i)     (256) // need to read num of clusters

#define ENET_BM_TBL_SHAPER_PROFILE_CLUSTER                            0x41 //65
#define ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_IS_ACM_SUPPORT(unit_i,hwUnit_i) \
        (mea_drv_Get_DeviceInfo_IsAcmSupport((unit_i),(hwUnit_i))) 
#define ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_LENGTH(unit_i,hwUnit_i)             ((MEA_Uint16)((mea_drv_Get_DeviceInfo_NumOfAcmModes((unit_i),(hwUnit_i))) * (mea_drv_Get_DeviceInfo_Get_numOf_Shaper_Profile((unit_i),(hwUnit_i))) ))  // Alex TBD
#define ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_NUM_OF_ACM_MODES(unit_i,hwUnit_i)   ((MEA_Uint16) mea_drv_Get_DeviceInfo_NumOfAcmModes((unit_i),(hwUnit_i)))
#define ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_LENGTH_PER_ACM(unit_i,hwUnit_i)   \
        (ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_LENGTH          ((unit_i),(hwUnit_i)) / \
         ENET_BM_TBL_SHAPER_PROFILE_CLUSTER_NUM_OF_ACM_MODES((unit_i),(hwUnit_i)) ) 

#define ENET_BM_TBL_SHAPER_BUCKET_PRIQUEUE                             0x42 //66
#ifdef MEA_ENV_256PORT
#define ENET_BM_TBL_SHAPER_BUCKET_PRIQUEUE_LENGTH(unit_i,hwUnit_i)     (mea_drv_Get_DeviceInfo_NumOfClusters(unit_i,hwUnit_i)* mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support()*2) /*2 support 2 LQ*/
#else
#define ENET_BM_TBL_SHAPER_BUCKET_PRIQUEUE_LENGTH(unit_i,hwUnit_i)     (mea_drv_Get_DeviceInfo_NumOfClusters(unit_i,hwUnit_i)* mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support()) 
#endif

#define ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE                            0x43 //67
#define ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_IS_ACM_SUPPORT(unit_i,hwUnit_i) \
        (mea_drv_Get_DeviceInfo_IsAcmSupport((unit_i),(hwUnit_i))) 
#define ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_LENGTH(unit_i,hwUnit_i)            ((MEA_Uint16)((mea_drv_Get_DeviceInfo_NumOfAcmModes((unit_i),(hwUnit_i))) * (mea_drv_Get_DeviceInfo_Get_numOf_Shaper_Profile((unit_i),(hwUnit_i))) ))  // Alex TBD

#define ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_NUM_OF_ACM_MODES(unit_i,hwUnit_i)  ((MEA_Uint16) mea_drv_Get_DeviceInfo_NumOfAcmModes((unit_i),(hwUnit_i)))
        
#define ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_LENGTH_PER_ACM(unit_i,hwUnit_i)   \
        (ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_LENGTH          ((unit_i),(hwUnit_i)) / \
         ENET_BM_TBL_SHAPER_PROFILE_PRIQUEUE_NUM_OF_ACM_MODES((unit_i),(hwUnit_i)) ) 


#define ENET_BM_TBL_LM_CNT_PRVIOUS                                    0x44 //68
#define ENET_BM_TBL_LM_CNT_PRVIOUS_NUM_OF_REGS                        4
#define ENET_BM_TBL_LM_CNT_PRVIOUS_LENGTH(unit_i,hwUnit_i)            (256)

#define ENET_BM_TBL_CCM_CONFIG                                    0x45 //69
#define ENET_BM_TBL_CCM_CONFIG_NUM_OF_REGS                        2
#define ENET_BM_TBL_CCM_CONFIG_LENGTH(unit_i,hwUnit_i)            (256)

#define ENET_BM_TBL_PERIOD_BUCKET_PROF                                    0x46 //70 
#define ENET_BM_TBL_PERIOD_BUCKET_PROF_NUM_OF_REGS                        2
#define ENET_BM_TBL_PERIOD_BUCKET_PROF_LENGTH                            8


#define ENET_BM_TBL_CC_EVENT                                    0x47 //71 
#define ENET_BM_TBL_CC_EVENT_NUM_OF_REGS                        1
#define ENET_BM_TBL_CC_EVENT_LENGTH                            16

#define ENET_BM_TBL_RDI_EVENT                                    0x48 // 72 
#define ENET_BM_TBL_RDI_EVENT_NUM_OF_REGS                        1
#define ENET_BM_TBL_RDI_EVENT_LENGTH                            16

#define ENET_BM_TBL_TYP_EHP_PROFILE2_ID                          0x49 // 73
#define ENET_BM_TBL_TYP_EHP_PROFILE2_ID_NUM_OF_REGS(hwUnit_i)    1
#define ENET_BM_TBL_TYP_EHP_PROFILE2_ID_LENGTH(hwUnit_i)         (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i)))

#define ENET_BM_TBL_TYP_EHP_PROFILE2_LM                                      0x4A //74
#define ENET_BM_TBL_TYP_EHP_PROFILE2_LM_NUM_OF_REGS(hwUnit_i)               ( (MEA_DRV_BM_BUS_WIDTH ==0) ? (1) : (2) )
#define ENET_BM_TBL_TYP_EHP_PROFILE2_LM_LENGTH(hwUnit_i)                     (mea_drv_Get_DeviceInfo_NumOflm_count(MEA_UNIT_0,(hwUnit_i)))    //16

#define ENET_BM_TBL_PERIOD_BUCKET_CCM                                  0x4B // 75 
#define ENET_BM_TBL_PERIOD_BUCKET_CCM_NUM_OF_REGS                       1
#define ENET_BM_TBL_PERIOD_BUCKET_CCM_LENGTH                            256


#define ENET_BM_TBL_EHP_FRAGMENT_SESSION                               0x4C //76 
#define ENET_BM_TBL_EHP_FRAGMENT_NUM_OF_REGS(hwUnit_i)                 1
#define ENET_BM_TBL_EHP_FRAGMENT_LENGTH(hwUnit_i)                     (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i)))

#define ENET_BM_TBL_EHP_PW_CONTROL                                       (ENET_BM_TBL_EHP_FRAGMENT_SESSION) 
#define ENET_BM_TBL_EHP_PW_CONTROL_NUM_OF_REGS(hwUnit_i)                 1
#define ENET_BM_TBL_EHP_PW_CONTROL_LENGTH(hwUnit_i)                     (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i)))


#define ENET_BM_TBL_CLUSTER_TO_MULTI_PORT                                       0x4d //77 
#define ENET_BM_TBL_CLUSTER_TO_MULTI_PORTT_NUM_OF_REGS(hwUnit_i)                 2
#define ENET_BM_TBL_CLUSTER_TO_MULTI_PORT_LENGTH(hwUnit_i)                     (mea_drv_Get_DeviceInfo_NumOfClusters(unit_i,hwUnit_i))

#define ENET_BM_TBL_CLUSTER_TO_PORTS_GROUP                                       0x4e //78 
#define ENET_BM_TBL_CLUSTER_TO_PORTS_GROUP_NUM_OF_REGS                              1
#define ENET_BM_TBL_CLUSTER_TO_PORTS_GROUP_LENGTH                                (128) //num of port


#define ENET_BM_TBL_TDM_CES_JB_INFO                                          0x4f  // 79

#define ENET_BM_TBL_TDM_RESYNC_PROF_INFO                                     0x50  // 80

#define ENET_BM_TBL_EHP_MPLS_LABEL                                         (0x51) // 81 
#define ENET_BM_TBL_EHP_MPLS_LABEL_NUM_OF_REGS(hwUnit_i)                 1
#define ENET_BM_TBL_EHP_MPLS_LABEL_LENGTH(hwUnit_i)                     (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i)))

#define ENET_BM_TBL_TYP_CLUSTER_BC_MQS                                  0x52 // 82
#define ENET_BM_TBL_CLUSTER_BC_MQS_LENGTH(hwUnit_i)                     ((mea_drv_Get_DeviceInfo_NumOfClusters(MEA_UNIT_0,(hwUnit_i)))

 
#define ENET_BM_TBL_TYP_TDM_SATISTICS_COUNTER                          0x53 // 83

#define ENET_BM_TBL_TYP_TDM_L_R_CES_EVENT                            0x54 // 84

#define ENET_BM_TBL_TYP_TDM_LOSS_AIS_PROFILE                         0x55 // 85


#define ENET_BM_TBL_TYP_TDM_CURRENT_JB_SATISTICS                       0x56 // 86

#define ENET_BM_TBL_TYP_QUEUE_COUNT                                    0x57 //87

#define ENET_BM_TBL_TYP_BONDING_TX_GROUP_PORT_MAPPING                  0x58 // 88

#define ENET_BM_TBL_TYP_BONDING_GROUP_SATISTICS_COUNTER                0x59 // 89

#define ENET_BM_TBL_TYP_AFDX_SEQUENCE                                 0x5A // 90



#define ENET_BM_TBL_TYP_DEST_WBRG_PORT_ATTRIBUTE                             0x5B // 91
#define ENET_BM_TBL_TYP_TRANSFORM_EDIT                                       0x5C // 92

#define ENET_BM_TBL_TYP_WBRG_EDITE_COMMAND                                  0x5d // 93
#define ENET_BM_TBL_TYP_EHP_WBRG                                      ENET_BM_TBL_TYP_WBRG_EDITE_COMMAND
#define ENET_BM_TBL_TYP_EHP_WBRG_NUM_OF_REGS(hwUnit_i)               1 
#define ENET_BM_TBL_TYP_EHP_WBRG_LENGTH(hwUnit_i)                   (mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_UNIT_0,(hwUnit_i)))


#define ENET_BM_TBL_TYP_LOG_DESC_AXI                                127
#define ENET_BM_TBL_LOG_DESC_AXI_LENGTH                             (16*1024)


/***********************************************************************************/
#define  ENET_BM_IA_CMD_MASK                0x00000001
#define  ENET_BM_IA_STAT_MASK				0x00000001

#define  ENET_BM_IA_ADDR_OFFSET_MASK		((MEA_GLOBAL_LARGE_IA_BM_ADDRESS == MEA_FALSE) ? 0x0000FFFF : 0x000FFFFF )

/*---------------------------------------------------------*/
/********************************************************************************/
#define MEA_DB_RECORDE_TYPE_64K          (64*1024)
#define MEA_DB_RECORDE_TYPE_128K         (128*1024)
#define MEA_DB_RECORDE_TYPE_256K         (256*1024)
#define MEA_DB_RECORDE_TYPE_512K         (512*1024)
#define MEA_DB_RECORDE_TYPE_1M           (1*1024*1024)

#define MEA_ADDRES_DDR_BYTE 4

#define MEA_ACTION_ADDRES_DDR_REGS             (ENET_IF_CMD_PARAM_TBL_TYP_ACTION_NUM_OF_REGS(0))  //4   /*128bit*/

#define MEA_ACTION_ADDRES_DDR_MAX_WIDTH        16
#define MEA_EDITING_ADDRES_DDR_MAX_WIDTH        16

#define MEA_SRV_BASE_ADDRES_DDR_MAX_WIDTH     16


#define MEA_ACTION_BASE_ADDRES_DDR_START            0
#define MEA_ACTION_BASE_ADDRES_DDR_WIDTH            ((MEA_DB_DDR_TYPE_SUPPORT == MEA_TRUE)  ? (16) : (8) )   /*16=512bit  8=256bit*/
#define MEA_ACTION_BASE_ADDRES_DDR_NUM_OF_RECORD    (((MEA_DB_DDR_TYPE_SUPPORT == MEA_TRUE) ? MEA_DB_RECORDE_TYPE_256K : MEA_DB_RECORDE_TYPE_512K))
#define MEA_ACTION_BASE_ADDRES_DDR_END              (((MEA_ACTION_BASE_ADDRES_DDR_NUM_OF_RECORD) * MEA_ACTION_BASE_ADDRES_DDR_WIDTH * MEA_ADDRES_DDR_BYTE) -1)

#define MEA_EDITING_BASE_ADDRES_DDR_START           (MEA_ACTION_BASE_ADDRES_DDR_END+1)   //0x1000000
#define MEA_EDITING_BASE_ADDRES_DDR_WIDTH           ((MEA_DB_DDR_TYPE_SUPPORT == MEA_TRUE) ? (16) : (8))   /*16=512bit  8=256bit*/
#define MEA_EDITING_BASE_ADDRES_DDR_NUM_OF_RECORD   (((MEA_DB_DDR_TYPE_SUPPORT == MEA_TRUE) ? MEA_DB_RECORDE_TYPE_256K : MEA_DB_RECORDE_TYPE_512K))
#define MEA_EDITING_BASE_ADDRES_DDR_END             ((((MEA_EDITING_BASE_ADDRES_DDR_NUM_OF_RECORD)*MEA_EDITING_BASE_ADDRES_DDR_WIDTH * MEA_ADDRES_DDR_BYTE) + MEA_EDITING_BASE_ADDRES_DDR_START)-1)

#define MEA_COUNTERS_PM_BASE_ADD                    (MEA_EDITING_BASE_ADDRES_DDR_END+1) //0x2000000 

#define MEA_PM_BASE_ADDRES_DDR_START                MEA_COUNTERS_PM_BASE_ADD
#define MEA_PM_BASE_ADDRES_DDR_WIDTH               ((MEA_DB_DDR_TYPE_SUPPORT == MEA_TRUE) ? (16) : (16))
#define MEA_PM_BASE_ADDRES_DDR_NUM_OF_RECORD       (((MEA_DB_DDR_TYPE_SUPPORT == MEA_TRUE) ? MEA_DB_RECORDE_TYPE_512K : MEA_DB_RECORDE_TYPE_512K))
#define MEA_PM_BASE_ADDRES_DDR_END                 ((((MEA_PM_BASE_ADDRES_DDR_NUM_OF_RECORD) * MEA_PM_BASE_ADDRES_DDR_WIDTH * MEA_ADDRES_DDR_BYTE) + MEA_PM_BASE_ADDRES_DDR_START)-1)


#define MEA_SRV_BASE_ADDRES_DDR_START              (MEA_PM_BASE_ADDRES_DDR_END +1)  //0x4000000
#define MEA_SRV_BASE_ADDRES_DDR_WIDTH              ((MEA_DB_DDR_TYPE_SUPPORT == MEA_TRUE) ? 16 : 16)  //16  /*512bit*/
#define MEA_SRV_BASE_ADDRES_NUM_OF_SRV             ((1*1024 *1024 ))
#define MEA_SRV_BASE_ADDRES_DDR_END               (MEA_SRV_BASE_ADDRES_DDR_START + 64*MEA_SRV_BASE_ADDRES_NUM_OF_SRV -1)

#define MEA_ACL5_BASE_ADDRES_DDR_START           (MEA_SRV_BASE_ADDRES_DDR_END +1)
#define MEA_ACL5_BASE_ADDRES_DDR_WIDTH           ((MEA_DB_DDR_TYPE_SUPPORT == MEA_TRUE) ? 16 : 16)  //16  /*512bit*/
#define MEA_ACL5_BASE_ADDRES_NUM_OF_ACL5          ((1*1024 *1024 )) 
#define MEA_ACL5_BASE_ADDRES_DDR_END             (MEA_ACL5_BASE_ADDRES_DDR_START + 64*MEA_ACL5_BASE_ADDRES_NUM_OF_ACL5 -1)


#define MEA_SRV_BASE_ADDRES_DDR_SW_START   0x10000000
#define MEA_SRV_BASE_ADDRES_DDR_SW_END     0x0fffffff




/*******************************************************************************/
/*     ENET_IF_CMD_PARAM_TBL_100_REGISTER    */ 
/*******************************************************************************/
#define   ENET_REGISTER_TBL100_RX_CRC_CHECK_MAC_0_255               0
#define   ENET_REGISTER_TBL100_RX_CRC_CHECK_MAC_256_511             1
#define   ENET_REGISTER_TBL100_RX_CRC_CHECK_MAC_512_767             2
#define   ENET_REGISTER_TBL100_RX_CRC_CHECK_MAC_768_1023            3

#define   ENET_REGISTER_TBL100_TX_CRC_CALC_MAC_0_255                4
#define   ENET_REGISTER_TBL100_TX_CRC_CALC_MAC_256_511               5
#define   ENET_REGISTER_TBL100_TX_CRC_CALC_MAC_512_767               6
#define   ENET_REGISTER_TBL100_TX_CRC_CALC_MAC_768_1023              7



#define   ENET_REGISTER_TBL100_Admin_RX_MAC_0_255                   8
#define   ENET_REGISTER_TBL100_Admin_RX_MAC_256_511                 9
#define   ENET_REGISTER_TBL100_Admin_RX_MAC_512_767                 10
#define   ENET_REGISTER_TBL100_Admin_RX_MAC_768_1023                11

#define   ENET_REGISTER_TBL100_Admin_TX_MAC_0_255                   12
#define   ENET_REGISTER_TBL100_Admin_TX_MAC_256_511                 13
#define   ENET_REGISTER_TBL100_Admin_TX_MAC_512_767                 14
#define   ENET_REGISTER_TBL100_Admin_TX_MAC_768_1023                15


#define   ENET_REGISTER_TBL100_FLUSH_0_255                          16
#define   ENET_REGISTER_TBL100_FLUSH_256_511                        17
#define   ENET_REGISTER_TBL100_FLUSH_512_767                        18
#define   ENET_REGISTER_TBL100_FLUSH_768_1023                       19

#define   ENET_REGISTER_TBL100_XMAC_TX_CONF                         20    
#define   ENET_REGISTER_TBL100_XMAC_STATUS                          21    


#define   ENET_REGISTER_TBL100_Pause_TX_MAC_0_255                   22
#define   ENET_REGISTER_TBL100_Pause_TX_MAC_256_511                 23
#define   ENET_REGISTER_TBL100_Pause_TX_MAC_512_768                 24
#define   ENET_REGISTER_TBL100_Pause_TX_MAC_768_1024                25

#define   ENET_REGISTER_TBL100_SGMII_config                         26

#define   ENET_REGISTER_TBL100_PauseStatus_0_255                   27
#define   ENET_REGISTER_TBL100_PauseStatus_256_511                 28
#define   ENET_REGISTER_TBL100_PauseStatus_512_768                 29
#define   ENET_REGISTER_TBL100_PauseStatus_768_1024                30


#define   ENET_REGISTER_TBL100_QSGMII_config_GT                    31 
#define   ENET_REGISTER_TBL100_QSGMII_Status_GT                    32
#define   ENET_REGISTER_TBL100_Configure_Handover_0                33
#define   ENET_REGISTER_TBL100_Configure_Handover_1                34

#define   ENET_REGISTER_TBL100_Configure_RX_Bonding_enable         35   // 256Bit
#define   ENET_REGISTER_TBL100_Configure_RXAUI                     37 
#define   ENET_REGISTER_TBL100_Configure_INTERFACE_TX_Disable      38

#define   ENET_REGISTER_TBL100_Configure_INGRESS_MIROR             39 





/***********************************************************************************/
/*         IF_EVENT OFFSET    EVENT                                                */
/***********************************************************************************/
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_DA                        0
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_HPM_MATCH_EVENTS          0 
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_HPM_UNMATCH_EVENTS        1  
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_IF_EVENTS                 2
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_IF_LAST_SEARCH_KEY        3
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_WRITE             4
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_READ                  5
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_COUNTERS              6
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_XMAC_EVENT                      7

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_LAST_LEARN_KEY        8
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_LAST_FWD_KEY          9
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PREPRASER_LAST_SEARCH_KEY 10
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_INFO_INTRNAL_HASH_RESULT        11

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_1           13 // flow 1
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_0           14 // flow 0
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_POS_PARITY_EVENT          15
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_L3L4                 16

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_PPP                  19


#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF2BM_EGR_PRT_RDY_MSK    22
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_PAUSE                    23
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_CFM                      24

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_PROTOCOL_EVENT        26
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_IF2BM_STATE_MACHINE          27
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_SERVICE_HASH_SEARCH          28


#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_TX_MAC_OVER_FLOW_EVENT     29

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_SERVICE_EVENT             30
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_IF_INDICATIONS_EVENT      31 

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_DBG1_EVENT      32
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_DBG2_EVENT      33
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_DBG3_EVENT      34
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_PARSER_DBG4_EVENT      35

#define MEA_IF_PARSER_CMD_PARAM_TBL_IP_Reassembly_EVENT                    36
#define MEA_IF_PARSER_CMD_PARAM_TBL_IP_Reassembly_UNMACH_EVENT             37




#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_FWD_TBL_MASK_RESULT_WRITE      39

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_2          40

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_HDC_CLASS_EVENT                41
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_DUMMY_ACK_TO_IF_TBL            42

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_EGRESS_CLASS_IF_TBL_EVENT       43
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_1588_EVENT                          44  
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_LINK_UP_INTERFACE_TBL_EVENT         45

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_EVENT_COMPRESS                      46
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_EVENT_DECOMPRESS                    47

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_GW_EVENT                  48

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_3           49


#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_FIFO_PBB_EVENT                   50
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_FIFO_PBB_FIFO_EVENT              51

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_RX_EPD_FULL_EVENT                52
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_XADC_POWER                       53


#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL_COUNT                     54   /*4 counter*/
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_ACL_0_1                  55
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_ACL_2_3                  56
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_UNMATCH_ACL_0_1          57
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_UNMATCH_ACL_2_3          58
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_EVENT_ACTION                  59  
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_BRAS_EVENT_LAST_INTERFACE     60 
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_EVENT_VPLS_FWD_ACCESS_PORT    61
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_EVENT_EXTERNAL_SERVICE        62
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_EVENT_CHANEL_G999_CRC_ERR     63

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_4               64
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_5               65
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_6               66
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LAST_IF_TO_BM_7               67
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_LPM_EVENT                     68

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL5_EVNT1                    70
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL5_EVNT2                    71

#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL5_EVNT3                    72
#define MEA_IF_PARSER_CMD_PARAM_TBL_TYP_REG_ENTRY_ACL5_EVNT4                    73


/*********************************************************************************/
/*        BM_EVENT 60                                                             */
/*********************************************************************************/

#define  ENET_TBL_BM_EVENT60_OFFSET_0	0
#define  ENET_TBL_BM_EVENT60_OFFSET_43	43

/*internal cluster for priqueue event of current Q */
#define ENET_TBL_BM_EVENT60_OFFSET48    48
#define ENET_TBL_BM_EVENT60_OFFSET63    63

#define ENET_TBL_BM_EVENT60_OFFSET64  64  /*shaper port Eligibility Status */
#define ENET_TBL_BM_EVENT60_OFFSET65  65  /*shaper port Eligibility Status */
#define ENET_TBL_BM_EVENT60_OFFSET66  66  /*shaper port Eligibility Status */
#define ENET_TBL_BM_EVENT60_OFFSET67  67  /*shaper port Eligibility Status */
#define ENET_TBL_BM_EVENT60_OFFSET64_NUMOF_OFFSET 4
  /*queue_pause status vector*/
#define ENET_TBL_BM_EVENT60_OFFSET68  68  /*Q pause Status */
#define ENET_TBL_BM_EVENT60_OFFSET_PAUSE_END  83  /*Q  pause Status */
#define ENET_TBL_BM_EVENT60_OFFSET68_NUMOF_OFFSET 16

#define ENET_TBL_BM_EVENT60_OFFSET_START ENET_TBL_BM_EVENT60_OFFSET_0
#define ENET_TBL_BM_EVENT60_OFFSET_END   ENET_TBL_BM_EVENT60_OFFSET_43





#endif  /* _ENET_GENERAL_DRV_H_ */



