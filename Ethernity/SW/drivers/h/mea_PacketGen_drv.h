/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_PACKETGEN_DRV_H
#define MEA_PACKETGEN_DRV_H

#include "mea_api.h"

#ifdef __cplusplus
 extern "C" {
#endif 



MEA_Status mea_drv_Init_PacketGen(MEA_Unit_t unit_i);

MEA_Status mea_drv_ReInit_PacketGen(MEA_Unit_t unit_i);

MEA_Status mea_drv_Conclude_PacketGen(MEA_Unit_t unit_i);

MEA_Status mea_drv_Get_PacketGen_db(MEA_Unit_t unit_i, MEA_db_dbt * db_po);

/***********************ANALYZER**************************/

MEA_Status mea_drv_Init_Analyzer(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_Counters_ANALYZE(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_Analyzer(MEA_Unit_t unit_i);

/************************************************************************/
/* PRBS                                                                 */
/************************************************************************/
MEA_Status mea_drv_PRBS_Init(MEA_Unit_t  unit);
MEA_Status mea_drv_PRBS_ReInit(MEA_Unit_t  unit);
MEA_Status mea_drv_PRBS_Conclude(MEA_Unit_t  unit);

#ifdef __cplusplus
 }
#endif 

#endif /* MEA_PACKETGEN_DRV_H */


