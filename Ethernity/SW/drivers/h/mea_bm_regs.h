/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_BM_REGS_H
#define	MEA_BM_REGS_H



#include "MEA_platform.h"
#include "mea_device_regs.h"

#ifdef __cplusplus
    extern "C" {
#endif


/*----------------------- MEA BM Registers Masks--------------------------------------*/
/* MEA_BM_STATUS register */

#define MEA_BM_InitDone_reng_MASK                0x00000200
#define MEA_BM_InitDone_aloc_MASK                0x00000100
#define MEA_BM_InitDone_alocR_MASK               0x00000800
#define MEA_BM_InitDone_Counters_MASK            0x00000040
#define MEA_BM_InitDone_Counts_MASK              0x00000020
#define MEA_BM_MRT_RAMInit_MASK                  0x00000010
#define MEA_BM_InitDone_DDR_calib_done_desc_MASK 0x00000008
#define MEA_BM_InitDone_DDR_calib_done_dse_MASK  0x00000004
#define MEA_BM_InitDone_DDR_calib_done_data_MASK 0x00000002

#define MEA_BM_INIT_STATUS_READY_MASK            0x00000001

		




/* MEA_BM_CONFIG register */
#define MEA_BM_CONFIG_BCST_DRP_LVL_MASK             0x00003fff /* def=0x180 */
#define MEA_BM_CONFIG_SHAPER_CLUSTER_ENABLE_MASK    0x00004000 //  shaper 14 cluster  
#define MEA_BM_CONFIG_SHAPER_PRIQUEUE_ENABLE_MASK   0x00008000 //  shaper 15 priqueue 
#define MEA_BM_CONFIG_WRED_SHIFT_MASK               0x00030000 /* def='01'b */
#define MEA_BM_CONFIG_RESERVE18_MASK                0x00040000 /* was in the past pswrr */
#define MEA_BM_CONFIG_BMQS_MODE_MASK                0x00080000
#define MEA_BM_CONFIG_WRED_MODE_MASK                0x00100000 
#define MEA_BM_CONFIG_MQS_MODE_MASK                 0x00200000 
#define MEA_BM_CONFIG_POLICER_PRI                   0x00400000 /*policer type*/
#define MEA_BM_CONFIG_RESERVE23_MASK                0x00800000 //not working //23
#define MEA_BM_CONFIG_MC_MQS_MASK                   0x01000000 // 24
#define MEA_BM_CONFIG_RESERVE25_MASK                0x02000000 //not working //25
#define MEA_BM_CONFIG_BAST_EFFORT                   0x04000000// 26 bs 
#define MEA_BM_CONFIG_PRI_Q_WFQ_MASK                0x08000000
#define MEA_BM_CONFIG_CLUSTER_WFQ_MASK              0x10000000
#define MEA_BM_CONFIG_SHAPER_PORT_ENABLE_MASK       0x20000000 // port
#define MEA_BM_CONFIG_POLICER_ENABLE_MASK           0x40000000
#define MEA_BM_CONFIG_ENABLE_MASK	                0x80000000 /* def=0 disable */


/* MEA_BM_EVENT_INFO register */
#define MEA_BM_EVENT_INFO_SERVICE_ID_MASK     0x000007ff
#define MEA_BM_EVENT_INFO_COLOR_MASK          0x00001800
#define MEA_BM_EVENT_INFO_FWD_DIS_MASK        0x00002000 /* 1 - forward / 0 -discard*/
#define MEA_BM_EVENT_INFO_PKTS_BYTES_MASK     0x00004000 /* 1 - Packets / 0 - bytes */
#define MEA_BM_EVENT_INFO_RESERVED_MASK       0x00008000
#define MEA_BM_EVENT_INFO_MQS_DROP_QUEUE_MASK 0x01ff0000
#define MEA_BM_EVENT_INFO_MTU_DROP_PORT_MASK  0xfe000000




/* MEA_BM_PRE_SCH_CY_LEN register */
#define MEA_BM_PRE_SCH_CY_LEN_MASK  0x000003ff


/* MEA_BM_IA_ADDR register  */
#define   MEA_BM_IA_ADDR_OFFSET_MASK		ENET_BM_IA_ADDR_OFFSET_MASK
#define	  MEA_BM_IA_ADDR_TBL_TYP_MASK		0x0003F000



/* MEA_BM_IA_WRDATA0 register */


/* MEA_BM_IA_WRDATA1 register */


/* MEA_BM_IA_RDDATA0 register */


/* MEA_BM_IA_RDDATA1 register */


/* MEA_BM_IA_CMD register */
#define  MEA_BM_IA_CMD_MASK             ENET_BM_IA_CMD_MASK

/* MEA_BM_IA_STAT register */
#define MEA_BM_IA_STAT_MASK				ENET_BM_IA_STAT_MASK



/*  MEA_BM_DESC_H_LIMIT	register */
#define MEA_BM_DESC_H_LIMIT_MAX_USED_DESCRIPTORS_LIMITER_MASK   0x00003FFF
#define MEA_BM_DESC_H_LIMIT_MAX_USED_BUFFERS_LIMITER_MASK		0x3fff0000

/*  MEA_BM_DESC_L_LIMIT register */ 
#define MEA_BM_DESC_L_LIMIT_MAX_USED_DESCRIPTORS_LIMITER_MASK   0x00003FFF
#define MEA_BM_DESC_L_LIMIT_MAX_USED_BUFFERS_LIMITER_MASK		0x3fff0000




/* MEA_BM_NSBP (Normal Speed Bucket Period) register */
#define MEA_BM_NSBP_VAL_MASK                0x0000ffff

/* MEA_BM_HSBP (High   Speed Bucket Period) register */
#define MEA_BM_HSBP_VAL_MASK                0x0000ffff



/* MEA_BM_IMR register */


#define MEA_BM_IMR_MASK0  0x00000001
#define MEA_BM_IMR_MASK1  0x00000002
#define MEA_BM_IMR_MASK2  0x00000004
#define MEA_BM_IMR_MASK3  0x00000008
#define MEA_BM_IMR_MASK4  0x00000010
#define MEA_BM_IMR_MASK5  0x00000020
#define MEA_BM_IMR_MASK6  0x00000040
#define MEA_BM_IMR_MASK7  0x00000080




/* MEA_BM_ISR register */
#define MEA_BM_ISR_STATUS0  0x00000001
#define MEA_BM_ISR_STATUS1  0x00000002
#define MEA_BM_ISR_STATUS2  0x00000004
#define MEA_BM_ISR_STATUS3  0x00000008
#define MEA_BM_ISR_STATUS4  0x00000010
#define MEA_BM_ISR_STATUS5  0x00000020
#define MEA_BM_ISR_STATUS6  0x00000040
#define MEA_BM_ISR_STATUS7  0x00000080

/* MEA_BM_IPR register */
#define MEA_BM_IPR_ENABLE   0x00000001


#define MEA_CLUSTER_2_PORT_PORT_MASK                               0x0000007f
#define MEA_CLUSTER_2_PORT_RSRV1_MASK                              0x00000080
#define MEA_CLUSTER_2_PORT_LOCATION_MASK                           0x00003f00
#define MEA_CLUSTER_2_PORT_RSRV2_MASK                              0x0000c000
#define MEA_CLUSTER_2_PORT_CLUSTER_NUMBER_MASK                     0x03ff0000

/* ENET_BM_TBL_TYP_CLUSTER_PORT_INFORMATION*/
#define MEA_CLUSTER_PORT_INFO_LOCATION_MASK                        0x000003ff
#define MEA_CLUSTER_PORT_INFO_STRICT_LEVEL                         0x0000fc00 // 

#define MEA_CLUSTER_PORT_INFO_LAST_COMITTED_ATRIB_MASK             0x00001f80
#define MEA_CLUSTER_PORT_INFO_LAST_EXCESS_ATRIB_MASK               0x0007e000
#define MEA_CLUSTER_PORT_INFO_RSRV_MASK                            0x7ff80000
#define MEA_CLUSTER_PORT_INFO_WFQ_ENA_MASK                         0x80000000


#define MEA_BM_PKT_SHORT_LIMIT_MASK         0x0000FFFF
#define MEA_BM_PKT_HIGH_LIMIT_MASK          0x0000FFFF
#define MEA_BM_CHUNK_SHORT_LIMIT_MASK       0x0000FFFF
#define MEA_BM_CHUNK_HIGH_LIMIT_MASK        0x0000FFFF



#define MEA_BM_CURRENT_PENDING_BROADCAST_PACKETS_MASK          0x000001ff

#define MEA_BM_CURRENT_USED_BUFFERS_MASK  \
    ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 0x0000ffff : 0x0000ffff)

#define MEA_BM_CURRENT_USED_DESCRIPTORS_MASK  \
    ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 0x0000ffff : 0xffff0000)

#define MEA_BM_SCRATCH_MASK                                0xffffffff


#define MEA_BM_VERSION_VERSION_MASK         0x000000F8
#define MEA_BM_VERSION_SUB_VERSION_MASK     0x00000007


/*------------ MEA_BM_IA_ADDR indirect tables adresses ------------------------------*/

#include "mea_device_regs.h"



#define MEA_MC_EHP_DWORD_MASK_ED_ID_WIDTH(unit_i,hwUnit_i)      (mea_drv_Get_DeviceInfo_EHPs_width(unit_i,hwUnit_i))
//#define MEA_MC_EHP_DWORD_MASK_PROTO_WIDTH 2
//#define MEA_MC_EHP_DWORD_MASK_LLC_WIDTH   1


/* MEA_BM_TBL_TYP_EGRESS_HEADER_PROC_XX  indirect table entry format */
#define MEA_EGRESS_HEADER_PROC_DWORD_1_MASK_VAL    	  	    0xFFFFFFFF 
#define MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_COLOR  	0x00000001 
#define MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_STAMP_PRIORITY	0x00000002 
#define MEA_EGRESS_HEADER_PROC_DWORD_2_MASK_COMMAND 		0x0000000C 


#define MEA_EGRESS_HEADER_PROC_MARTINI_DA_DWORD_1_MASK		0xFFFFFFFF    
#define MEA_EGRESS_HEADER_PROC_MARTINI_DA_DWORD_2_MASK    	0x0000FFFF
#define MEA_EGRESS_HEADER_PROC_MARTINI_SA_DWORD_1_MASK		0xFFFFFFFF    
#define MEA_EGRESS_HEADER_PROC_MARTINI_SA_DWORD_2_MASK    	0x0000FFFF
#define MEA_EGRESS_HEADER_PROC_MEA_MARTINI_ETHER_TYPE_MASK	0x0000FFFF


/* MEA_BM_TBL_PORT_PROTECT indirect table entry format  */
#define MEA_BM_TBL_PORT_PROTECT_MASK                        0x000003ff

/* MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS indirect table entry format  */

#define MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_MASK 0x00ffffff

#define MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_FULL               0
#define MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_EMPTY              1
#define MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_MAX_EMPTY_LATENCY  2 
#define MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_MAX_BUFFER_LATENCY 3
#define MEA_BM_TBL_TYP_TX_ENGINE_PERFORMANCE_COUNTERS_OFFSET_AVERAGE_FIFO_SIZE  4

/* MEA_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS indirect table entry format */

#define MEA_BM_TBL_TYP_WRITE_ENGINE_PERFORMANCE_COUNTERS_MASK 0x00ffffff

#define MEA_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS_OFFSET_FULL               0
#define MEA_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS_OFFSET_MAX_BUFFER_LATENCY 1
#define MEA_BM_TBL_TYP_WRITE_FIFO_PERFORMANCE_COUNTERS_OFFSET_AVERAGE_FIFO_SIZE  2







/* MEA_BM_TBL_TYP_SCHEDULER_PRIORITY	 indirect table entry format */


/* MEA_BM_TBL_TYP_CNT_PM_XXX_DISCARD_BYTE   indirect table entry format */
#define MEA_BM_TBL_TYP_CNT_PM_DISCARD_BYTE_MASK   0x01FFFFFF

/* MEA_BM_TBL_TYP_CNT_PM_XXX_FWD_BYTE       indirect table entry format */
#define MEA_BM_TBL_TYP_CNT_PM_FWD_BYTE_MASK       0x07FFFFFF

/* MEA_BM_TBL_TYP_CNT_PM_XXX_DISCARD_PKT   indirect table entry format */
#define MEA_BM_TBL_TYP_CNT_PM_DISCARD_PKT_MASK    0x0007FFFF

/* MEA_BM_TBL_TYP_CNT_PM_XXX_FWD_PKT          indirect table entry format */
#define MEA_BM_TBL_TYP_CNT_PM_FWD_PKT_MASK        0x001FFFFF


/* MEA_BM_TBL_TYP_CNT_PORT_MTU_DROP      indirect table entry format */
#define MEA_BM_TBL_TYP_CNT_QUEUE_MTU_DROP_MASK   ((MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_CLUSTER_TYPE)  ? (0x000000ff) : (0x00000001))  


/* MEA_BM_TBL_TYP_QUEUE_MTU_SIZE   indirect table entry format */
#define MEA_QUEUE_MTU_SIZE_MASK		((MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_CLUSTER_TYPE) ? (0x00003FFF) : (0x000000ff))  



/* MEA_BM_TBL_TYP_INGRESS_PORT_MTU_SIZE   indirect table entry format */
#define MEA_INGRESS_PORT_MTU_SIZE_MASK		        0x00003FFF

/* MEA_BM_TBL_TYP_PRI_DROP_PROP    indirect table entry format */

/* MEA_BM_TBL_TYP_CURRENT_Q_SIZE   indirect table entry format */
#define MEA_CURRENT_Q_SIZE_MASK 0x00000fff

/* MEA_BM_TBL_TYP_AVERAGE_Q_SIZE   indirect table entry format */
#define MEA_AVERAGE_Q_SIZE_AQS_FRAC_MASK 0x0000000f
#define MEA_AVERAGE_Q_SIZE_AQS_MASK      0x00001ff0



/* MEA_BM_TBL_TYP_EGRESS_PORT_MARTINI_DA indirect table entry format */        
#define MEA_EGRESS_PORT_MARTINI_DA_DWORD_1_MASK		0xFFFFFFFF    
#define MEA_EGRESS_PORT_MARTINI_DA_DWORD_2_MASK    	0x0000FFFF

/* MEA_BM_TBL_TYP_EGRESS_PORT_MARTINI_SA indirect table entry format */        
#define MEA_EGRESS_PORT_MARTINI_SA_DWORD_1_MASK		0xFFFFFFFF    
#define MEA_EGRESS_PORT_MARTINI_SA_DWORD_2_MASK    	0x0000FFFF

/* MEA_BM_TBL_TYP_EGRESS_PORT_MARTINI_ETHERTYPE indirect table entry format */
#define MEA_EGRESS_PORT_MEA_MARTINI_ETHER_TYPE_MASK		0x0000FFFF

/*  MEA_BM_TBL_TYP_EGRESS_PORT_STAMP_INFO indirect table entry format */        
#define MEA_EGRESS_PORT_STAMP_COL_BIT0_LOC_MASK     0x0000001F
#define MEA_EGRESS_PORT_STAMP_COL_BIT0_VALID_MASK   0x00000020
#define MEA_EGRESS_PORT_STAMP_COL_BIT1_LOC_MASK     0x000007C0
#define MEA_EGRESS_PORT_STAMP_COL_BIT1_VALID_MASK   0x00000800
#define MEA_EGRESS_PORT_STAMP_PRI_BIT0_LOC_MASK     0x0001F000
#define MEA_EGRESS_PORT_STAMP_PRI_BIT0_VALID_MASK   0x00020000
#define MEA_EGRESS_PORT_STAMP_PRI_BIT1_LOC_MASK     0x007C0000
#define MEA_EGRESS_PORT_STAMP_PRI_BIT1_VALID_MASK   0x00800000
#define MEA_EGRESS_PORT_STAMP_PRI_BIT2_LOC_MASK     0x1F000000
#define MEA_EGRESS_PORT_STAMP_PRI_BIT2_VALID_MASK   0x20000000







/* MEA_BM_TBL_TYP_EGRESS_METERING_MSB  indirect table entry format */

/* reg 0 */
#define MEA_POLICER_EIR_MASK2                       0x0000007f
#define MEA_POLICER_CIR_MASK                        0x0003ff80
#define MEA_POLICER_CUP_MASK                        0x00040000
#define MEA_POLICER_COMP_MASK                       0x0ff80000
#define MEA_POLICER_CA_MASK                         0x10000000



#define MEA_SRV_RATE_METER_PROF_CIR_EIR_MASK          (( 1<<(( MEA_Platform_Get_CIR_Token_Bits () == 0) ? (12) : (MEA_Platform_Get_CIR_Token_Bits()) ))-1)





/***/
/* policer profile*/
#define MEA_SRV_RATE_METER_PROF_CIR_WIDTH                    (( MEA_Platform_Get_CIR_Token_Bits() == 0) ? (12) : (MEA_Platform_Get_CIR_Token_Bits()) )
#  /*12*/
#define MEA_SRV_RATE_METER_PROF_CBS_WIDTH                    (( MEA_Platform_Get_CBS_bucket_Bits() == 0) ? (18) : (MEA_Platform_Get_CBS_bucket_Bits()) )
#define MEA_SRV_RATE_METER_PROF_EIR_WIDTH                    (( MEA_Platform_Get_CIR_Token_Bits() == 0) ? (12) : (MEA_Platform_Get_CIR_Token_Bits()) )  
#define MEA_SRV_RATE_METER_PROF_EBS_WIDTH                    (( MEA_Platform_Get_CBS_bucket_Bits() == 0) ? (18) : (MEA_Platform_Get_CBS_bucket_Bits()) )
#define MEA_SRV_RATE_METER_PROF_COMP_WIDTH                    6
#define MEA_SRV_RATE_METER_PROF_CF_WIDTH                      1
#define MEA_SRV_RATE_METER_PROF_CA_WIDTH                      1
#define MEA_SRV_RATE_METER_PROF_TYPE_BAG_WIDTH                0 /* bit is not valid on HW */
#define MEA_SRV_RATE_METER_PROF_SLOWSLOW                      1

/* policer bucket*/

#define MEA_SRV_RATE_METER_BUCKET_CIR_WIDTH                   (( MEA_Platform_Get_CBS_bucket_Bits() == 0) ? (18) : (MEA_Platform_Get_CBS_bucket_Bits()) )
#define MEA_SRV_RATE_METER_BUCKET_GN_TYPE_WIDTH               3
#define MEA_SRV_RATE_METER_BUCKET_GN_SIGN_WIDTH               1
#define MEA_SRV_RATE_METER_BUCKET_BAG_WIDTH                   1
#define MEA_SRV_RATE_METER_BUCKET_LMAX_WIDTH                  8
#define MEA_SRV_RATE_METER_BUCKET_RESERV_WIDTH                ( ( MEA_Platform_Get_CBS_bucket_Bits() == 0) ? (5) : (MEA_Platform_Get_CBS_bucket_Bits() - 13)  ) 
#define MEA_SRV_RATE_METER_BUCKET_ENABL_WIDTH                 1











#define MEA_SRV_RATE_METER_POINT_PROF_MASK                   0x0000003f


#define MEA_PORT_RATE_METER_PROF_CIR_MASK                     0x00000FFF
#define MEA_PORT_RATE_METER_PROF_CBS_MASK                     0x0FFFF000
#define MEA_PORT_RATE_METER_PROF_EIR0_3_MASK                  0xF0000000
#define MEA_PORT_RATE_METER_PROF_EIR4_11_MASK                 0x000000ff   
#define MEA_PORT_RATE_METER_PROF_EBS_MASK                     0x00ffff00
#define MEA_PORT_RATE_METER_PROF_COMP_MASK                    0x3f000000
#define MEA_PORT_RATE_METER_PROF_CF_MASK                      0x40000000
#define MEA_PORT_RATE_METER_PROF_CA_MASK                      0x80000000

#define MEA_PORT_RATE_METER_CONTEX_XCIR                  0x0000ffff 
#define MEA_PORT_RATE_METER_CONTEX_XEIR                  0xffff0000
#define MEA_PORT_RATE_METER_CONTEX_ENABL                 0x00000001


#define MEA_PORT_RATE_METER_PROF_CIR_EIR_MASK           0x00000FFF /*12bit*/




/* MEA_BM_TBL_TYP_EGRESS_PORT_MQS        indirect table entry format */
#define MEA_EGRESS_PORT_MQSP_MASK					0x0000000F
#define MEA_EGRESS_PORT_MQSP_MULTIPLIER_MASK		0x00000030
#define MEA_EGRESS_PORT_MQSB_MASK					0x000003c0
#define MEA_EGRESS_PORT_MQSB_MULTIPLIER_MASK		0x00000c00

#define MEA_EGRESS_PORT_CQSB_MASK					0x07fff000

/* MEA_QUEUE_WFQ_WEIGHT_MASK        indirect table entry format */
#define MEA_QUEUE_WFQ_WEIGHT_MASK					0x0000007F

/* MEA_QUEUE_FAIRNESS_LEVEL        indirect table entry format */
#define MEA_QUEUE_FAIRNESS_LEVEL					0x38000000

/* MEA_BM_TBL_TYP_CNT_PORT_MQS_DROP      indirect table entry format */
#define MEA_BM_TBL_TYP_CNT_QUEUE_MQS_DROP_MASK  0x000000FF



/* MEA_BM_TBL_TYP_BUF_RESOURCE_LIMIT_DROP_PACKET_CNT  indirect table entry format */
#define MEA_BM_TBL_TYP_BRL_DROP_PACKET_CNT_MASK 0x00003fff


/* MEA_BM_TBL_TYP_DESC_RESOURCE_LIMIT_DROP_PACKET_CNT indirect table entry format */
#define MEA_BM_TBL_TYP_DRL_DROP_PACKET_CNT_MASK 0x00003fff



/* MEA_BM_TBL_HALT_FLUSH_CMD                          indirect table entry format */
#define MEA_BM_TBL_HALT_FLUSH_CMD_FLUSH_MASK          0x00000001
#define MEA_BM_TBL_HALT_FLUSH_CMD_HALT_MASK		      0x00000002


/* MEA_BM_EGRESS_SHAPER                                  indirect table entry format */
#define MEA_BM_SHAPER_CIR_BACKET_MASK_PORT                 0x001fffff
#define MEA_BM_SHAPER_PROF_NUMBER_MASK_PORT                0x1fe00000  
#define MEA_BM_SHAPER_COMP_MASK_PORT                       0x60000000
#define MEA_BM_SHAPER_BYPASS_MASK_PORT                     0x80000000

#define MEA_BM_SHAPER_CIR_BACKET_MASK_CLUSTER              0x001fffff
#define MEA_BM_SHAPER_PROF_NUMBER_MASK_CLUSTER             0x1fe00000
#define MEA_BM_SHAPER_COMP_MASK_CLUSTER                    0x60000000
#define MEA_BM_SHAPER_BYPASS_MASK_CLUSTER                  0x80000000

#define MEA_BM_SHAPER_CIR_BACKET_MASK_PRIQUEUE              0x001fffff
#define MEA_BM_SHAPER_PROF_NUMBER_MASK_PRIQUEUE             0x1fe00000
#define MEA_BM_SHAPER_COMP_MASK_PRIQUEUE                    0x60000000
#define MEA_BM_SHAPER_BYPASS_MASK_PRIQUEUE                  0x80000000


/*ENET_BM_DISCRIPTOR_CONF_REG */

#define MEA_BM_DISCRIPTOR_BUFFER_MASK             ((!MEA_ALLOC_ALLOCR_NEW_SUPPORT) ? 0x00001fff : 0x0000000F)
#define MEA_BM_DISCRIPTOR_DATA_BUFFER_MASK        ((!MEA_ALLOC_ALLOCR_NEW_SUPPORT) ? 0x00000000 : 0x000000F0)
#define MEA_BM_DISCRIPTOR_RESERV_MASK             ((!MEA_ALLOC_ALLOCR_NEW_SUPPORT) ? 0x00000000 : 0x00001F00)
#define MEA_BM_DISCRIPTOR_TRHESHOLD_MASK          ((!MEA_ALLOC_ALLOCR_NEW_SUPPORT) ? 0x00ffe000 : 0x00ffe000)
#define MEA_BM_DISCRIPTOR_JUMBO_MASK               0xff000000


/* MEA_BM_TBL_TYP_EDITING_MAPPING    indirect table entry format */
#define MEA_BM_TBL_TYP_EDITING_MAPPING_STAMP_COLOR_WIDTH    1
#define MEA_BM_TBL_TYP_EDITING_MAPPING_STAMP_PRIORITY_WIDTH 3


/* MEA_BM_TBL_EHP_FRAGMENT_SESSION    indirect table entry format */
#define MEA_BM_TBL_EHP_FRAGMENT_SESSION_SESSION_ID_WIDTH    (MEA_OS_calc_from_size_to_width( MEA_Platform_Get_fragment_Edit_num_of_session()))  
#define MEA_BM_TBL_EHP_FRAGMENT_SESSION_SESSION_VALID_WIDTH  1
#define MEA_BM_TBL_EHP_FRAGMENT_SESSION_EX_ENABLE_WIDTH         ((MEA_AFDX_SUPPORT == MEA_TRUE)? (1) :(0) )   //(MEA_OS_calc_from_size_to_width( MEA_Platform_Get_fragment_Edit_num_of_group())))  

/* MEA_BM_TBL_EHP_PW_CONTROL  indirect table entry format */


/*ENET_BM_TBL_TYP_GW_GLOBAL_REGS  7*/

/*Table 7 BM */
#define MEA_GLOBAl_TBL_BM_REG_edit_IP_TTL        1
#define MEA_GLOBAl_TBL_BM_REG_InternalDscp       2

#define MEA_GLOBAl_TBL_REG_SET_TH_MQS_MCMQS      4
#define MEA_GLOBAl_TBL_REG_SET_QUEUE_SFLOW       5
#define MEA_GLOBAl_TBL_REG_SET_DSA_TAG_SFLOW     6
#define MEA_GLOBAl_TBL_REG_SET_MTUCPU_SFLOW      7
#define MEA_GLOBAl_TBL_REG_SET_BM_WD             8

#define MEA_GLOBAl_TBL_REG_SET_MY_GRE_MAC        9
#define MEA_GLOBAl_TBL_REG_SET_OUTER_TUNNEL_VID  10    /*16bit*/      
#define MEA_GLOBAl_TBL_REG_SET_IP_TTL_GRE        11   /*8bit*/
#define MEA_GLOBAl_TBL_REG_SET_SRC_IPV4_GRE      12
#define MEA_GLOBAl_TBL_REG_SET_IP_TTL_IPSEC      13   /*8bit*/
#define MEA_GLOBAl_TBL_REG_SET_SRC_IPV4_IPSEC    14








#define MEA_GLOBAl_TBL_REG_S_EGRESS_ts_flow_def 24
#define MEA_GLOBAl_TBL_REG_S_EGRESS_ts_value    25
#define MEA_GLOBAl_TBL_REG_BM_PDN_IPV4_INDEX_0      56  /*Up to 63*/

#define MEA_GLOBAl_TBL_REG_TARGET_MTU_PROF0     64
#define MEA_GLOBAl_TBL_REG_TARGET_MTU_PROF1     65
#define MEA_GLOBAl_TBL_REG_TARGET_MTU_PROF2     66
#define MEA_GLOBAl_TBL_REG_TARGET_MTU_PROF3     67

#define MEA_GLOBAl_TBL_BM_TH_SLICE0             80
#define MEA_GLOBAl_TBL_BM_TH_SLICE1             81
#define MEA_GLOBAl_TBL_BM_TH_SLICE2             82
#define MEA_GLOBAl_TBL_BM_TH_SLICE3             83
#define MEA_GLOBAl_TBL_BM_TH_SLICE4             84
#define MEA_GLOBAl_TBL_BM_TH_SLICE5             85
#define MEA_GLOBAl_TBL_BM_TH_SLICE6             86
#define MEA_GLOBAl_TBL_BM_TH_SLICE7             87




 
#ifdef __cplusplus
 }
#endif 


#endif /* MEA_BM_REGS_H */


