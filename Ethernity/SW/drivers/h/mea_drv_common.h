/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef  MEA_DRV_COMMON_H
#define  MEA_DRV_COMMON_H



#include "MEA_platform.h"

#include "mea_api.h"
#include "mea_if_regs.h"
#include "mea_bm_regs.h"

#include "mea_init.h"
#include "mea_db_drv.h"
#ifdef __cplusplus
 extern "C" {
 #endif 

#define MEA_SCRATCH_TST_PATTERN	                    0x8888AAAA
#define ENET_IF_SOFTWARE_RESET_PATTERN              0x1F2E3D4C
#define ENET_IF_SOFTWARE_RESET_DISEABLE_PATTERN     0

#define ENET_IND_Lock          MEA_IND_TABE_Lock
#define ENET_IND_Unlock        MEA_IND_TABE_Unlock

#define ENET_FWD_Lock          MEA_FWD_Lock
#define ENET_FWD_Unlock        MEA_FWD_Unlock



typedef enum{
	MEA_MEMORY_READ_COMPARE,
	MEA_MEMORY_READ_SUM,
	MEA_MEMORY_READ_IGNORE,
	MEA_MEMORY_READ_LAST
}mea_memory_mode_read_e;







typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32  llc : 1;
        MEA_Uint32  martini_cmd : 2;
        MEA_Uint32  outer_cmd : 2;
        MEA_Uint32  vlan_cmd : 2;
        MEA_Uint32  protocol : 2;
        MEA_Uint32  pad : 22;
        MEA_Uint32 calc_checksum :1;
#else
        MEA_Uint32 calc_checksum :1;
        MEA_Uint32  pad : 22;
        MEA_Uint32  protocol : 2;
        MEA_Uint32  vlan_cmd : 2;
        MEA_Uint32  outer_cmd : 2;
        MEA_Uint32  martini_cmd : 2;
        MEA_Uint32  llc : 1;
#endif
   

    }val;
    MEA_Uint32 regs[1];


}MEA_editing_machin_dbt;


extern MEA_Uint64 RatePortsByCluster[MEA_MAX_PORT_NUMBER + 1];
extern mea_memory_mode_e globalMemoryMode;

extern MEA_subSysType_te    MEA_subSysType;

extern mea_module_memory_dbt device_memory[MEA_MODULE_LAST];


extern ENET_Bool b_bist_test;
extern ENET_Bool tdm_bist_test;
extern ENET_Bool mea_debug_reg_all;
extern ENET_Bool mea_SSSMII_interface_support;
extern ENET_Bool mea_hash_group_flag_show;
extern MEA_Bool  GlobalBm_busy;
extern MEA_Bool Init_QUEUE ;


extern MEA_uint64 MEA_GLOBAL_SYS_Clock;

extern MEA_Bool   MEA_Counters_Type_Enable[MEA_COUNTERS_LAST_TYPE];


extern MEA_Uint32   gMEA_KillEnet ;

extern ENET_Uint32 MEA_reinit_start;
extern ENET_Uint32 MEA_RxEnable_stateWrite;
extern ENET_Uint32 MEA_TxEnable_stateWrite;

extern MEA_Bool mea_tdm_global_debug_Show_sw_current;
extern MEA_Bool mea_tdm_global_debug_sw_restart;
extern MEA_Bool mea_tdm_global_enable_sw_restart;
extern MEA_Bool mea_tdm_global_enable_MTG1;

extern MEA_Uint32          globalSTP_Mode;         
extern MEA_Uint32          globalSTP_Default_State;
extern MEA_Uint16          mea_global_fwd_act;

extern MEA_Uint32  MEA_TDM_CESs_buffer;

extern MEA_Uint32 glClearDDR_servie;

extern MEA_Bool mea_PM_en_calc_rate;
extern MEA_Bool mea_RMON_en_calc_rate;

extern MEA_Uint32 mea_global_1p1_down_up_count_And_ports_enable;

extern mea_global_environment_dbt MEA_device_environment_info;


extern MEA_Uint32                 MEA_SE_Aging_interval;
extern MEA_Bool                   MEA_SE_Aging_enable;

extern MEA_Uint32                 Global_Aging_interval;
extern MEA_Bool                   Global_Aging_enable;

extern MEA_PolicerTicks_Entry_dbt   MEA_Global_ServicePoliceTick;
extern MEA_PolicerTicks_Entry_dbt   MEA_Global_IngressFlowPolicerTick;

extern MEA_PCIe_Divice_dbt mea_pcie_device[10];     /*0-bus,1-slot,2-fun*/

extern char device_cpu127[50];

extern MEA_Bool MEA_Warm_Start_Write_Disable;

extern MEA_Bool MEA_ignore_vxlan_info;


void MEA_IND_TABE_Lock(void);
void MEA_IND_TABE_Unlock(void);
void MEA_FWD_Lock(void);
void MEA_FWD_Unlock(void);

void MEA_DDR_Lock(void);
void MEA_DDR_Unlock(void);
void MEA_BAR0_Lock(void);
void MEA_BAR0_Unlock(void);

MEA_Status MEA_drv_common_destroy_mutexs(void);

MEA_Status mea_drv_write_IF_TBL_X_offset_UpdateEntry_pArray(MEA_Unit_t  unit, MEA_Uint32 tableId, MEA_Uint32 offset, MEA_Uint32 numofRegs, MEA_Uint32 *new_entry);


MEA_Status MEA_DRV_WriteDDR_DATA_BLOCK(MEA_Unit_t unit_i, MEA_Uint32 hwId, MEA_Uint32 addressDDR, MEA_Uint32 *DataValue, MEA_Uint32 len, MEA_DDR_module Type);

void MEA_LowLevel_WriteReg(MEA_Unit_t unit, MEA_ULong_t Addr,MEA_Uint32 data,MEA_module_te mea_module,mea_memory_mode_e memoryMode);
MEA_Uint32 MEA_LowLevel_ReadReg(MEA_Unit_t unit, MEA_ULong_t Addr, MEA_module_te mea_module, mea_memory_mode_e memoryMode,mea_memory_mode_read_e readMode);
MEA_Status MEA_LowLevel_WriteIndirect(MEA_Unit_t unit,MEA_ind_write_t *p_ind_write,MEA_module_te mea_module,mea_memory_mode_e memoryMode);
MEA_Status MEA_LowLevel_ReadIndirect(MEA_Unit_t unit,MEA_ind_read_t *p_ind_read,MEA_Uint32 num_of_regs,MEA_module_te mea_module,mea_memory_mode_e memoryMode,mea_memory_mode_read_e readMode);

void MEA_device_environment_init(void);
MEA_ULong_t mea_drv_commn_base_address(MEA_module_te  type);
#ifdef MEA_LOCAL_MNG
MEA_Status MEA_Device_LOCAL_MNG_127_100(MEA_Unit_t unit);
#endif

#define MEA_HW_UNIT_ID_GENERAL    0
#define	MEA_HW_UNIT_ID_UPSTREAM   0
#define	MEA_HW_UNIT_ID_DOWNSTREAM 1

#define mea_drv_num_of_hw_units \
	((MEA_db_HwUnit_t)((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 1 : 2))

#define mea_drv_getGlobalMemeoryModeByHwUnit(hwUnit_i) \
	(((globalMemoryMode) == MEA_MODE_REGULAR_ENET3000) ? MEA_MODE_REGULAR_ENET3000 : \
	 ((hwUnit_i        ) == MEA_HW_UNIT_ID_DOWNSTREAM) ? MEA_MODE_DOWNSTREEM_ONLY  : \
	  MEA_MODE_UPSTREEM_ONLY)

MEA_Status mea_drv_GetCurrentHwUnit(MEA_Unit_t       unit_i,
									MEA_db_HwUnit_t *hwUnit_o);

#define MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode_i,hwUnit_i) \
	 { \
	   (save_globalMemoryMode_i) = globalMemoryMode; \
       globalMemoryMode = mea_drv_getGlobalMemeoryModeByHwUnit((hwUnit_i)); \
	 }

#define MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode_i) \
	 { \
	 	   globalMemoryMode = (save_globalMemoryMode_i); \
	 }

#define MEA_DRV_GET_SW_ID_WITH_EXIST(unit_i,get_db_func_i,hwUnit_i,hwId_i,swId_o,exist_o,restore_i) \
		{ \
		  MEA_db_Hw_Entry_dbt   db_Hw_entry; \
		  MEA_db_dbt            db; \
  		  if ((get_db_func_i)((unit_i),&db) != MEA_OK) { \
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
			                    "%s - get_db_func failed \n", \
								__FUNCTION__); \
			  restore_i; \
		  } \
		  if (mea_db_Get_Hw((unit_i), \
			                db, \
 						   (hwUnit_i), \
 						   (hwId_i), \
			               &db_Hw_entry) != MEA_OK) { \
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
			                    "%s - mea_db_Get_Hw failed (hwUnit=%d,hwId=%d)\n", \
						        __FUNCTION__,(hwUnit_i),(hwId_i)); \
			  restore_i; \
		  } \
		  (exist_o) = db_Hw_entry.valid; \
		  (swId_o) = db_Hw_entry.swId; \
	  }

#define MEA_DRV_GET_SW_ID(unit_i,get_db_func_i,hwUnit_i,hwId_i,swId_o,restore_i) \
		{ \
          MEA_Bool my_db_hwId_exist; \
          MEA_DRV_GET_SW_ID_WITH_EXIST((unit_i), \
                                       (get_db_func_i), \
                                       (hwUnit_i), \
                                       (hwId_i), \
                                       (swId_o), \
                                       my_db_hwId_exist, \
                                       restore_i) \
		  if (!my_db_hwId_exist) { \
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
			  				    "%s - db_Hw_entry not valid (hwUnit=%d,hwId=%d)\n", \
							    __FUNCTION__,(hwUnit_i),(hwId_i)); \
			  restore_i; \
		  } \
	  }


#define MEA_DRV_GET_HW_ID(unit_i,get_db_func_i,swId_i,hwUnit_i,hwId_o,restore_i) \
		{ \
		  MEA_db_SwHw_Entry_dbt db_SwHw_entry; \
		  MEA_db_dbt            db; \
  		  if ((get_db_func_i)((unit_i),&db) != MEA_OK) { \
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
			                    "%s:%d - get_db_func failed \n", \
								__FUNCTION__,__LINE__); \
			  restore_i; \
		  } \
		  if (mea_db_Get_SwHw((unit_i), \
			                  db, \
							  (MEA_db_SwId_t)(swId_i), \
							  (hwUnit_i), \
			                  &db_SwHw_entry) != MEA_OK) { \
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
			                    "%s:%d - mea_db_Get_SwHw failed (id=%d)\n", \
						        __FUNCTION__,__LINE__,(swId_i)); \
			  restore_i; \
		  } \
		  if (!db_SwHw_entry.valid) { \
			  MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
			                    "%s:%d - db_SwHw_entry not valid (id=%d)\n", \
							    __FUNCTION__,__LINE__,(swId_i)); \
			  restore_i; \
		  } \
		  (hwId_o) = db_SwHw_entry.hwId; \
	  }

#define MEA_DRV_GET_HW_UNIT(unit_i,hwUnit_o,restore_i) \
	{ \
	   if (mea_drv_GetCurrentHwUnit((unit_i),&(hwUnit_o)) != MEA_OK) { \
		   MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
				             "%s - mea_drv_GetCurrentHwUnit failed \n", \
			 			     __FUNCTION__); \
		   restore_i; \
		} \
	}


#define MEA_DRV_GET_HW_UNIT_AND_HW_ID(unit_i,get_db_func_i,swId_i,hwUnit_o,hwId_o,restore_i) \
	{ \
		MEA_DRV_GET_HW_UNIT(unit_i,hwUnit_o,restore_i); \
		MEA_DRV_GET_HW_ID(unit_i,get_db_func_i,swId_i,hwUnit_o,hwId_o,restore_i); \
	}

extern MEA_Bool MEA_API_LOG_Enable;

#define MEA_API_LOG \
	if (MEA_API_LOG_Enable) { \
		MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"%s:%d\n",__FUNCTION__,__LINE__); \
	}
#ifdef __cplusplus
 }
#endif 

#endif

