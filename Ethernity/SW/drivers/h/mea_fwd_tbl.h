/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_FWD_TBL_H
#define MEA_FWD_TBL_H

 


/*-------------------------------------------------------------------------------*/
/*   Includes                                                                    */
/*-------------------------------------------------------------------------------*/

#include "mea_api.h"
#include "mea_serviceEntry_drv.h"
#include "mea_action_drv.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------------------------------------------------*/
/*   Defines                                                                     */
/*-------------------------------------------------------------------------------*/

// T.B.D The FPGA add always the base by itself for ENET4000
// #define MEA_ENET4000_FWD_OFFSET_ENABLE 

#define MEA_FWD_TBL_MAX_AGE        3

/* to Calculate aging time precision 15%  we need to  MEA_DSE_AGING_GAN/MEA_DSE_AGING_GAN1 */
#define MEA_DSE_AGING_GAN 5	  
#define MEA_DSE_AGING_GAN1 2

#define MEA_FWD_HW_UNIT \
    ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) \
      ? MEA_HW_UNIT_ID_GENERAL : MEA_HW_UNIT_ID_UPSTREAM)
/*-------------------------------------------------------------------------------*/
/*   Typedefs                                                                    */
/*-------------------------------------------------------------------------------*/




/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                <MEA_Forwarder_Key_dbt>                                        */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef union {
	       struct {
               /* word 0 */
               MEA_Uint32       signature0;  /* 0-31 */
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif


               /* word 1 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32       signature1 : 28; /* 32-59 */
		       MEA_Uint32       key_type   :  3; /* 60-62 */
               MEA_Uint32       valid      :  1; /* 63-63 */
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
               MEA_Uint32       valid      :  1; /* 63-63 */
		       MEA_Uint32       key_type   :  3; /* 60-62 */
               MEA_Uint32       signature1 : 28; /* 32-59 */
#endif
	       } raw;

           struct {

               /* word 0 */
               MEA_Uint32 mac_0_31;  /* 0-31 */
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
               /* word 3 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32 mac_32_47        : 16;  /* 32-47 */
               MEA_Uint32 vpn              : 12;  /* 48-57 */
//               MEA_Uint32 mask_type        : 2;  /* 58-59 */
		       MEA_Uint32 key_type         :  3; /* 60-62 */
               MEA_Uint32 valid            :  1;  /* 63-63 */  
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
               MEA_Uint32 valid            :  1;  /* 63-63 */  
		       MEA_Uint32 key_type         :  3; /* 60-62 */
//               MEA_Uint32 mask_type        : 2;  /* 58-59 */
               MEA_Uint32 vpn              : 12;  /* 48-57 */
               MEA_Uint32 mac_32_47        : 16;  /* 32-47 */
#endif

	       } mac_plus_vpn;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
           struct {
               /* word 0 */
               MEA_Uint32 mac_0_31;  /* 0-31 */
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
               /* word 3 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32 mac_32_47        : 16;  /* 32-47 */
               MEA_Uint32 dont_care1       : 12;  /* 48-59 */  
		       MEA_Uint32 key_type         :  3; /* 60-62 */
               MEA_Uint32 valid            :  1;  /* 63-63 */  
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
               MEA_Uint32 valid            :  1;  /* 63-63 */  
		       MEA_Uint32 key_type         :  3; /* 60-62 */
               MEA_Uint32 dont_care1       : 12;  /* 48-59 */  
               MEA_Uint32 mac_32_47        : 16;  /* 32-47 */
#endif

           } mac;
#endif
           struct {

               /* word 0 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32 label_1          : 20;  /* 0- 19 */
               MEA_Uint32 label_2_lsb      : 12;  /* 1-12 */
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else 
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
               
               MEA_Uint32 label_2_lsb      : 12;  /* 20-31 */
               MEA_Uint32 label_1          : 20;  /* 0- 19 */
#endif

               /* word 1 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32 label_2_msb       : 8;  /* 32-39 */
               MEA_Uint32 dont_care1       :  8;  /* 40-47 */
               MEA_Uint32 vpn              : 10;  /* 48-57 */
               MEA_Uint32 mask_type        :  2;  /* 58 59  */
               MEA_Uint32 key_type         :  3; /* 60-62 */
               MEA_Uint32 valid            :  1;  /* 63-63 */  
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
               MEA_Uint32 valid            :  1;  /* 63-63 */  
               MEA_Uint32 key_type         :  3; /* 60-62 */
               MEA_Uint32 mask_type        :  2;  /* 58 59  */
               MEA_Uint32 vpn              : 10;  /* 48-57 */
               MEA_Uint32 dont_care1       :  8;  /* 40-47 */
               MEA_Uint32 label_2_msb      :  8;  /* 32-39 */
#endif

           } mpls_vpn;
#ifdef PPP_MAC 
           struct {

               /* word 0 */
               MEA_Uint32 mac_0_31;  /* 0-31 */
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
               /* word 3 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32 mac_32_47            : 16;  /* 32-47 */
               MEA_Uint32 pppoe_session_id_lsb : 12;  /* 48-59 */
		       MEA_Uint32 key_type             :  3; /* 60-62 */
               MEA_Uint32 valid                :  1;  /* 63-63 */  
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
               MEA_Uint32 valid                :  1;  /* 63-63 */  
		       MEA_Uint32 key_type             :  3; /* 60-62 */
               MEA_Uint32 pppoe_session_id_lsb : 12;  /* 48-59 */
               MEA_Uint32 mac_32_47            : 16;  /* 32-47 */
#endif

	       } mac_plus_pppoe_session_id;
#else
		   struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
			   /* word 0 */
			   MEA_Uint32 pppoe_session_id : 16 ;  /* 0-15 */
			   MEA_Uint32 pad              : 16;
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
			   MEA_Uint32 pad              : 16;
			   MEA_Uint32 pppoe_session_id : 16;  /* 0-15 */
#endif
									 /* word 3 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
			   MEA_Uint32 pad1       : 16;
			   MEA_Uint32 vpn        : 10;  /* 48-57 */
			   MEA_Uint32 mask_type  : 2;  /* 58-59 */
			   MEA_Uint32 key_type   : 3; /* 60-62 */
			   MEA_Uint32 valid      : 1;  /* 63-63 */
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
			   MEA_Uint32 valid       : 1;  /* 63-63 */
			   MEA_Uint32 key_type    : 3; /* 60-62 */
			   MEA_Uint32 mask_type   : 2;  /* 58-59 */
			   MEA_Uint32 vpn         : 10;  /* 48-57 */
			   MEA_Uint32 pad1        : 16;
#endif

		   } pppoe_session_id_vpn;
#endif
           struct {

               /* word 0 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32 vlan_to_enable   :  1;  /* 0- 0 */
               MEA_Uint32 vlan_to_val      : 12;  /* 1-12 */
               MEA_Uint32 dont_care0       : 19;  /*13-31 */
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else 
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
               MEA_Uint32 dont_care0       : 19;  /* 0-31 */
               MEA_Uint32 vlan_to_val      : 12;  /* 1-12 */
               MEA_Uint32 vlan_to_enable   :  1;  /* 0- 0 */
#endif

               /* word 3 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32 vlan             : 12;  /* 32-43 */
               MEA_Uint32 dont_care1       :  4;  /* 44-47 */
               MEA_Uint32 vpn              : 10;  /* 48-57 */
               MEA_Uint32 mask_type        : 2;  /* 58-59 */
		       MEA_Uint32 key_type         :  3; /* 60-62 */
               MEA_Uint32 valid            :  1;  /* 63-63 */  
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
               MEA_Uint32 valid            :  1;  /* 63-63 */  
		       MEA_Uint32 key_type         :  3; /* 60-62 */
               MEA_Uint32 mask_type        :  2;  /* 58-59 */
               MEA_Uint32 vpn              : 10;  /* 48-59 */
               MEA_Uint32 dont_care1       :  4;  /* 32-43 */
               MEA_Uint32 vlan             : 12;  /* 44-47 */
#endif

	       } vlan_vpn;
            
           struct {
               /* word 0 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32  label_1                : 20;     //  0..19
               MEA_Uint32  MD_level               : 4;     // 20..23
               MEA_Uint32  op_code                : 8;     // 24..31
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
               MEA_Uint32  op_code          : 8;     // 24..31
               MEA_Uint32  MD_level         : 4;     // 20..23
               MEA_Uint32  label_1         : 20;     //  0..19
			   
#endif

               /* word 1 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32  packet_is_untag_mpls   : 1;    // 32..32
               MEA_Uint32  Src_port               : 8;    // 33..40  /* ??? 256Port */
               MEA_Uint32  reserverd_1            : 7;    // 41 47
               MEA_Uint32 vpn                     : 10;  /* 48-59 */
               MEA_Uint32 mask_type               : 2;  /* 58-59 */
               MEA_Uint32 key_type                :  3;  /* 60-62 */  
               MEA_Uint32 valid                   :  1;  /* 63-63 */
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
               MEA_Uint32 valid                   :  1;  /* 63-63 */  
               MEA_Uint32 key_type                :  3;  /* 60-62 */
               MEA_Uint32 mask_type               :  2;  /* 58-59 */
               MEA_Uint32 vpn                     : 10;  /* 48-57 */
               MEA_Uint32  reserverd_1            : 7;    // 41 47
               MEA_Uint32  Src_port               : 8;    // 33..40
               MEA_Uint32  packet_is_untag_mpls   : 1;    // 32..32
#endif
	     } oam_vpn;
           
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32 DstIPv4_23_0     : 24;  /*  0-15 */
               MEA_Uint32 SrcIPv4_7_0      :  8;  /* 16-22 */
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
               MEA_Uint32 SrcIPv4_7_0      :  8;  /* 16-22 */    
               MEA_Uint32 DstIPv4_23_0     : 24;  /*  0-15 */
#endif

               /* word 1 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
               MEA_Uint32 SrcIPv4_23_8     : 16;  /* 32-43 */
               MEA_Uint32 vpn              : 10;  /* 44-47 */
               MEA_Uint32 mask_type        : 2;  /* 46-47 */
               MEA_Uint32 key_type         :  3;  /* 60-62 */  
               MEA_Uint32 valid            :  1;  /* 63-63 */  
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
               MEA_Uint32 valid            :  1;  /* 63-63 */  
               MEA_Uint32 key_type         :  3;  /* 60-62 */  
               MEA_Uint32 mask_type        :  2;  /* 46-47 */
               MEA_Uint32 vpn              : 10;  /* 44-47 */
               MEA_Uint32 SrcIPv4_23_8     : 16;  /* 32-43 */
#endif
     } DstIPv4_plus_SrcIPv4_plus_vpn;

    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 L4Port           : 16; /*0 15*/  
    MEA_Uint32 IPv4_bit_0_15    : 16;
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
    MEA_Uint32 IPv4_bit_0_15    : 16;      
    MEA_Uint32 L4Port           : 16; /*0 15*/  
#endif

    /* word 1 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 IPv4_bit_16_32   : 16;  /* 32-47 */
    MEA_Uint32 vpn              : 10;  /* 48-57 */
    MEA_Uint32 mask_type        :  2;  /* 58-59 */
    MEA_Uint32 key_type         :  3;  /* 60-62 */  
    MEA_Uint32 valid            :  1;  /* 63-63 */  
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
    MEA_Uint32 valid            :  1;  /* 63-63 */  
    MEA_Uint32 key_type         :  3;  /* 60-62 */
    MEA_Uint32 mask_type        :  2;  /* 58-59 */
    MEA_Uint32 vpn              : 10;  /* 48-47 */
    MEA_Uint32 IPv4_bit_16_32   : 16;  /* 32-47 */
#endif
    } DstIPv4_plus_L4DstPort_plus_vpn;
            
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 L4Port          : 16; /*0 15*/  
    MEA_Uint32 IPv4_bit_0_15    : 16;
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
    MEA_Uint32 IPv4_bit_0_15    : 16;      
    MEA_Uint32 L4Port          : 16; /*0 15*/  
#endif

    /* word 1 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
    MEA_Uint32 IPv4_bit_16_32   : 16;  /* 32-47 */
    MEA_Uint32 vpn              : 10;  /* 48-57 */
    MEA_Uint32 mask_type        : 2;  /* 58-59 */
    MEA_Uint32 key_type         :  3;  /* 60-62 */  
    MEA_Uint32 valid            :  1;  /* 63-63 */  
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
    MEA_Uint32 valid            :  1;  /* 63-63 */  
    MEA_Uint32 key_type         :  3;  /* 60-62 */
    MEA_Uint32 mask_type        :  2;  /* 58-59 */
    MEA_Uint32 vpn              : 10;  /* 48-57 */
    MEA_Uint32 IPv4_bit_16_32   : 16;  /* 32-47 */
#endif
    } SrcIPv4_plus_L4srcPort_plus_vpn;
//is same DstIPv4_plus_SrcIPv4_plus_vpn
#if 0
    struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 L4Port : 16; /*0 15*/
        MEA_Uint32 IPv6_bit_0_15 : 16;
#else
        MEA_Uint32 IPv6_bit_0_15 : 16;
        MEA_Uint32 L4Port : 16; /*0 15*/
#endif

                                /* word 1 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 IPv6_bit_16_32 : 16;  /* 32-47 */
        MEA_Uint32 vpn            : 10;  /* 48-57 */
        MEA_Uint32 mask_type      : 2;  /* 58-59 */
        MEA_Uint32 key_type       : 3;  /* 60-62 */
        MEA_Uint32 valid          : 1;  /* 63-63 */
#else
        MEA_Uint32 valid          : 1;  /* 63-63 */
        MEA_Uint32 key_type       : 3;  /* 60-62 */
        MEA_Uint32 mask_type      : 2;  /* 58-59 */
        MEA_Uint32 vpn            : 10;  /* 48-47 */
        MEA_Uint32 IPv6_bit_16_32 : 16;  /* 32-47 */
#endif
    } DstIPv6_plus_L4DstPort_plus_vpn;
#endif
} MEA_Forwarder_Key_dbt; //HW



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                <MEA_Forwarder_Data_dbt>                                     */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
#ifdef MEA_NEW_FWD_ROW_SET

#define MEA_FWD_DATA_STATIC_START          (0)
#define MEA_FWD_DATA_STATIC_WIDTH          (1)

#define MEA_FWD_DATA_AGE_START             (MEA_FWD_DATA_STATIC_START + \
	                                        MEA_FWD_DATA_STATIC_WIDTH   )
#define MEA_FWD_DATA_AGE_WIDTH             (2)

#define MEA_FWD_DATA_XPERMISSION_ID_START  (MEA_FWD_DATA_AGE_START + \
	                                        MEA_FWD_DATA_AGE_WIDTH   )
#define MEA_FWD_DATA_XPERMISSION_ID_WIDTH  (11)  /* even if the xPermission table length is only 512 */


#define MEA_FWD_DATA_RESERV_START (MEA_FWD_DATA_XPERMISSION_ID_START + \
                                               MEA_FWD_DATA_XPERMISSION_ID_WIDTH   )

#define MEA_FWD_DATA_RESERV_WIDTH    (4)


#define MEA_FWD_DATA_SA_DISCARD_START      (MEA_FWD_DATA_RESERV_START + \
	                                        MEA_FWD_DATA_RESERV_WIDTH   )
#define MEA_FWD_DATA_SA_DISCARD_WIDTH      (1)

#define MEA_FWD_DATA_ACCESS_PORT_START  (MEA_FWD_DATA_SA_DISCARD_START + \
		                                           MEA_FWD_DATA_SA_DISCARD_WIDTH   )

#define MEA_FWD_DATA_ACCESS_PORT_WIDTH          (10)




#define MEA_FWD_DATA_ACTION_ID_VALID_START (MEA_FWD_DATA_ACCESS_PORT_START + \
	                                        MEA_FWD_DATA_ACCESS_PORT_WIDTH   )
#define MEA_FWD_DATA_ACTION_ID_VALID_WIDTH (1)


#define MEA_FWD_DATA_ACTION_ID_START       (MEA_FWD_DATA_ACTION_ID_VALID_START + \
	                                        MEA_FWD_DATA_ACTION_ID_VALID_WIDTH   )
#define MEA_FWD_DATA_ACTION_ID_WIDTH        (18)






#define MEA_FWD_DATA_LIMITER_ID_START      (MEA_FWD_DATA_ACTION_ID_START + \
		                                    MEA_FWD_DATA_ACTION_ID_WIDTH)


#define MEA_FWD_DATA_LIMITER_ID_WIDTH  (13)

#define MEA_FWD_DATA_RESERV1_START (MEA_FWD_DATA_LIMITER_ID_START + \
                                               MEA_FWD_DATA_LIMITER_ID_WIDTH   )

#define MEA_FWD_DATA_RESERV1_WIDTH    (1)


#define MEA_FWD_DATA_PARITY_START (MEA_FWD_DATA_RESERV1_START + \
		                                           MEA_FWD_DATA_RESERV1_WIDTH   )
#define MEA_FWD_DATA_PARITY_WIDTH ( 2)


#define MEA_FWD_DATA_WIDTH (MEA_FWD_DATA_PARITY_START + \
		                    MEA_FWD_DATA_PARITY_WIDTH   )
#else
#define MEA_FWD_DATA_STATIC_START          (0)
#define MEA_FWD_DATA_STATIC_WIDTH          (1)

#define MEA_FWD_DATA_AGE_START             (MEA_FWD_DATA_STATIC_START + \
	                                        MEA_FWD_DATA_STATIC_WIDTH   )
#define MEA_FWD_DATA_AGE_WIDTH             (2)

#define MEA_FWD_DATA_XPERMISSION_ID_START  (MEA_FWD_DATA_AGE_START + \
	                                        MEA_FWD_DATA_AGE_WIDTH   )
#define MEA_FWD_DATA_XPERMISSION_ID_WIDTH  (11)  /* even if the xPermission table length is only 512 */

#define MEA_FWD_DATA_SKIP_MC_EDIT_START    (MEA_FWD_DATA_XPERMISSION_ID_START + \
	                                        MEA_FWD_DATA_XPERMISSION_ID_WIDTH   )
#define MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH    (1)
#if 0
#define MEA_FWD_DATA_MC_EDIT_ID_START      (MEA_FWD_DATA_SKIP_MC_EDIT_START + \
	                                        MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH   )
#define MEA_FWD_DATA_MC_EDIT_ID_WIDTH      (3)
#else
#define MEA_FWD_DATA_ACCESS_PORT_BIT0_2_START (MEA_FWD_DATA_SKIP_MC_EDIT_START + \
	                                           MEA_FWD_DATA_SKIP_MC_EDIT_WIDTH   )

#define MEA_FWD_DATA_ACCESS_PORT_BIT0_2    (3)

#endif

#define MEA_FWD_DATA_SA_DISCARD_START      (MEA_FWD_DATA_ACCESS_PORT_BIT0_2_START + \
	                                        MEA_FWD_DATA_ACCESS_PORT_BIT0_2   )
#define MEA_FWD_DATA_SA_DISCARD_WIDTH      (1)

#define MEA_FWD_DATA_COS_FORCE_START      (MEA_FWD_DATA_SA_DISCARD_START + \
	                                        MEA_FWD_DATA_SA_DISCARD_WIDTH   )
#define MEA_FWD_DATA_COS_FORCE_WIDTH      (1)

#define MEA_FWD_DATA_COS_START            (MEA_FWD_DATA_COS_FORCE_START + \
	                                       MEA_FWD_DATA_COS_FORCE_WIDTH   )
#define MEA_FWD_DATA_COS_WIDTH            (3)

#define MEA_FWD_DATA_PRI_FORCE_START      (MEA_FWD_DATA_COS_START + \
	                                       MEA_FWD_DATA_COS_WIDTH   )
#define MEA_FWD_DATA_PRI_FORCE_WIDTH      (1)

#define MEA_FWD_DATA_PRI_START            (MEA_FWD_DATA_PRI_FORCE_START + \
	                                       MEA_FWD_DATA_PRI_FORCE_WIDTH   )
#define MEA_FWD_DATA_PRI_WIDTH            (3)

#define MEA_FWD_DATA_COL_FORCE_START      (MEA_FWD_DATA_PRI_START + \
	                                       MEA_FWD_DATA_PRI_WIDTH   )
#define MEA_FWD_DATA_COL_FORCE_WIDTH      (1)

#define MEA_FWD_DATA_COL_START            (MEA_FWD_DATA_COL_FORCE_START + \
	                                       MEA_FWD_DATA_COL_FORCE_WIDTH   )
#define MEA_FWD_DATA_COL_WIDTH            (2)

#define MEA_FWD_DATA_ACTION_ID_VALID_START (MEA_FWD_DATA_COL_START + \
	                                        MEA_FWD_DATA_COL_WIDTH   )
#define MEA_FWD_DATA_ACTION_ID_VALID_WIDTH (1)


#define MEA_FWD_DATA_ACTION_ID_START       (MEA_FWD_DATA_ACTION_ID_VALID_START + \
	                                        MEA_FWD_DATA_ACTION_ID_VALID_WIDTH   )
#define MEA_FWD_DATA_ACTION_ID_WIDTH        (12)
#if 0   
(mea_drv_Get_DeviceInfo_GetForwarderLearnActionIdWidth \
	      (MEA_UNIT_0, \
           (globalMemoryMode == MEA_MODE_REGULAR_ENET3000) \
			? MEA_HW_UNIT_ID_GENERAL: MEA_HW_UNIT_ID_UPSTREAM) + \
	    ((globalMemoryMode == MEA_MODE_REGULAR_ENET3000) ? 0 : 0)) //Alex
#endif

#if 0
#define MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_START (MEA_FWD_DATA_ACTION_ID_START + \
		                                           MEA_FWD_DATA_ACTION_ID_WIDTH   )
#define MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH (1 )

#define MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_START (MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_START + \
		                                           MEA_FWD_DATA_ME_LVL_THRESHOLD_VALID_WIDTH   )
#define MEA_FWD_DATA_ME_LVL_THRESHOLD_VALUE_WIDTH (4 )
#else

#define MEA_FWD_DATA_ACCESS_PORT_BIT3_7_START  (MEA_FWD_DATA_ACTION_ID_START + \
		                                           MEA_FWD_DATA_ACTION_ID_WIDTH   )

#define MEA_FWD_DATA_ACCESS_PORT_BIT3_7          (5)

#endif

#define MEA_FWD_DATA_LIMITER_ID_START      (MEA_FWD_DATA_ACCESS_PORT_BIT3_7_START + \
		                                    MEA_FWD_DATA_ACCESS_PORT_BIT3_7)


#define MEA_FWD_DATA_LIMITER_ID_WIDTH  (13)

#if 0
#define MEA_FWD_DATA_RESERVE_BITS_START (MEA_FWD_DATA_LIMITER_ID_START + \
		                                           MEA_FWD_DATA_LIMITER_ID_WIDTH   )

#define MEA_FWD_DATA_RESERVE_BITS_WIDTH (1)
#else
#define MEA_FWD_DATA_ACCESS_PORT_BIT_8_START (MEA_FWD_DATA_LIMITER_ID_START + \
		                                           MEA_FWD_DATA_LIMITER_ID_WIDTH   )
#define MEA_FWD_DATA_ACCESS_PORT_BIT_8  (1)
#endif


#define MEA_FWD_DATA_PARITY_START (MEA_FWD_DATA_ACCESS_PORT_BIT_8_START + \
		                                           MEA_FWD_DATA_ACCESS_PORT_BIT_8   )
#define MEA_FWD_DATA_PARITY_WIDTH ( 2)


#define MEA_FWD_DATA_WIDTH (MEA_FWD_DATA_PARITY_START + \
		                    MEA_FWD_DATA_PARITY_WIDTH   )
#endif //MEA_NEW_FWD_ROW_SET

typedef struct {
    MEA_Uint32 val[2];
} MEA_Forwarder_Data_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                <MEA_fwd_Tbl_ResultType_t>                                     */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef enum{
	MEA_FWD_TBL_RESULT_TYPE_RESULT       = 0,
    MEA_FWD_TBL_RESULT_TYPE_SIGNATURE    = 1
} MEA_Fwd_Tbl_ResultType_t;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                <MEA_Forwarder_Info_dbt>                                       */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct { 
#if __BYTE_ORDER == __LITTLE_ENDIAN
       MEA_Uint32       match      : 1;
       MEA_Uint32       table_id   : 4;
       MEA_Uint32       last       : 1;
       MEA_Uint32       pad0       :26;
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
       MEA_Uint32       pad0       :26; 
       MEA_Uint32       last       : 1;
       MEA_Uint32       table_id   : 4;
       MEA_Uint32       match      : 1;
#endif
} MEA_Forwarder_Info_dbt;


/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                <MEA_Forwarder_Signature_dbt>                                  */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef union {
   struct {
	   MEA_Forwarder_Key_dbt  key;
	   MEA_Forwarder_Info_dbt info;
   } val;
   MEA_Uint32 regs[3];
} MEA_Forwarder_Signature_dbt;

/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                <MEA_Forwarder_Result_dbt>                                     */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef union {
   struct {
       MEA_Forwarder_Data_dbt data;
	   MEA_Forwarder_Info_dbt info;
   } val;
   MEA_Uint32 regs[3];              
} MEA_Forwarder_Result_dbt;



/*-------------------------------------------------------------------------------*/
/*                                                                               */
/*                <MEA_Forwarder_cmd_dbt>                                       */
/*                                                                               */
/*-------------------------------------------------------------------------------*/
typedef struct {
    union {
      struct  {
             /* word 0 and word 1 */
			 MEA_Forwarder_Data_dbt data;

             /* word 2 and word 3 and word 4 */
             union {
                    struct {
                            /* word 2*/
                            MEA_Uint32 dont_care0 ;  /* 0-31  */

                            /* word 3*/
                            MEA_Uint32 dont_care1;  /* 32-63 */

                            /* word 4*/
#if __BYTE_ORDER == __LITTLE_ENDIAN
                            MEA_Uint32 address     :28;  /* 64-84 */
                            MEA_Uint32 hash_type   : 1;  /* 92-92 */
                            MEA_Uint32 result_type : 2;  /* 93-94 */
                            MEA_Uint32 cam_mem     : 1;  /* 95-95 */
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
                            MEA_Uint32 cam_mem     : 1;  /* 95-95 */
                            MEA_Uint32 result_type : 2;  /* 93-94 */
                            MEA_Uint32 hash_type   : 1;  /* 92-92 */
                            MEA_Uint32 address     :28;  /* 64-84 */
#endif
                    } read_key;

                   struct {

				   	   /* word 2 and word 3 */
	                   MEA_Forwarder_Key_dbt key;


                       /* word 4 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
                       MEA_Uint32 address          :28;  /* 64-79 */
                       MEA_Uint32 hash_type        : 1;  /* 92-92 */
                       MEA_Uint32 result_type      : 2;  /* 93-94 */
                       MEA_Uint32 cam_mem          : 1;  /* 95-95 */
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_1		:32;
#endif
                       MEA_Uint32 cam_mem          : 1;  /* 95-95 */
                       MEA_Uint32 result_type      : 2;  /* 93-94 */
                       MEA_Uint32 hash_type        : 1;  /* 92-92 */
                       MEA_Uint32 address          :28;  /* 64-79 */
#endif
                   } write_key;



                   struct {

				   	   /* word 2 and word 3 */
	                   MEA_Forwarder_Key_dbt key;

                       /* word 4 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
					   MEA_Uint32      dont_care4  :31;  /* 64-95 */
                       MEA_Uint32      addCmd      : 1;  /* 64-95 */
#ifdef ARCH64
		MEA_Uint32 pad64_2		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_2		:32;
#endif
					   MEA_Uint32      addCmd      : 1;  /* 64-95 */
					   MEA_Uint32      dont_care4  :31;  /* 64-95 */
#endif


                   } add_key;

                   struct {

   				   	   /* word 2 and word 3 */
	                   MEA_Forwarder_Key_dbt key;

                       /* word 4 */
                       MEA_Uint32      dont_care4;  /* 64-95 */

                   } delete_key;
                

                   struct {

   				   	   /* word 2 and word 3 */
	                   MEA_Forwarder_Key_dbt key;

                       /* word 4 */
                       MEA_Uint32       dont_care4;  /* 64-95 */

                   } search_key;
                
                } key;

              /* word 5 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
              MEA_Uint32    priority : 2;
              MEA_Uint32    table_id : 4;
              MEA_Uint32    command  : 4;
              MEA_Uint32    pad0     :22;
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
#else
#ifdef ARCH64
		MEA_Uint32 pad64_0		:32;
#endif
              MEA_Uint32    pad0     :22;
              MEA_Uint32    command  : 4;
              MEA_Uint32    table_id : 4;
              MEA_Uint32    priority : 2;
#endif
        } val;


        MEA_Uint32   regs[6];
    } cmd;


} MEA_Forwarder_cmd_dbt;


/*-------------------------------------------------------------------------------*/
/*   Internal APIs                                                               */
/*-------------------------------------------------------------------------------*/
MEA_Status mea_drv_Init_Forwarder(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_Forwarder(MEA_Unit_t unit_i);

MEA_Status MEA_fwd_tbl_readByIndex( MEA_Fwd_Tbl_HashType_t       hash_type,
                                    MEA_Uint32                   entry_number,
                                    MEA_Forwarder_Signature_dbt *signature_o,
                                    MEA_Forwarder_Result_dbt    *result_o );

MEA_Status MEA_fwd_tbl_WriteByIndex( MEA_Fwd_Tbl_HashType_t       hash_type,
                                     MEA_Uint32                   entry_number,
                                     MEA_Forwarder_Signature_dbt *signature_i,
                                     MEA_Forwarder_Result_dbt    *result_i );


MEA_Status MEA_fwd_key_convert_to_SE_key_Entry(MEA_Uint64   info_key,
                                    MEA_SE_Entry_key_dbt    *key_po);



#ifdef __cplusplus
 }
#endif 


#endif /* MEA_IF_PRE_PARSER_DRV_H */


