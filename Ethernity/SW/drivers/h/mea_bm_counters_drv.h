/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/******************************************************************************
* 	Module Name:		 mea_bm_counters_drv.h
*
*   Module Description:
*
*
*  Date of Creation:	 17/09/06
*
*  Author Name:			 Alex Weis.
*******************************************************************************/

#ifndef MEA_BM_COUNTERS_DRV_H
#define MEA_BM_COUNTERS_DRV_H


#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
     extern "C" {
#endif


typedef union{
    struct {
    MEA_Uint32         green_fwd_bytes;        /*0  */
    MEA_Uint32         green_dis_bytes;        /*1  */
    MEA_Uint32         yellow_fwd_bytes;       /*2  */
    MEA_Uint32         yellow_or_red_dis_bytes; /*3  */

    /* Packets counters */
    MEA_Uint32         green_fwd_pkts; /*4  */
    MEA_Uint32         green_dis_pkts; /*5  */
    MEA_Uint32        yellow_fwd_pkts; /*6  */
    MEA_Uint32        yellow_dis_pkts; /*7  */
    MEA_Uint32        red_dis_pkts;    /*8  */
    MEA_Uint32        other_dis_pkts; /*9  */
    MEA_Uint32        dis_mtu_pkts; /*10  */
    /*----------------------------------------*/
    MEA_Uint32         green_dis_bytes_green_fwd_bytes; /*11  */

    MEA_Uint32         yellow_or_red_dis_bytes_yellow_fwd_bytes; /*12  */
    /* Packets counters */
    MEA_Uint32         green_dis_pkts_green_fwd_pkts; /*13  */

    MEA_Uint32        yellow_dis_pkts_yellow_fwd_pkts; /*14  */

    MEA_Uint32        other_dis_pkts_red_dis_pkts; /*15  */
    }val;
    MEA_Uint32 regs[16];
} mea_PM_HW_data_dbt;


typedef struct {
	MEA_Bool			valid;
	MEA_Uint32			num_of_owners ;
    MEA_db_HwUnit_t     hwUnit;
    MEA_db_HwId_t       hwId_offset;
    MEA_Uint16          pad;
    MEA_pm_type_te      pm_type; 
	/*MEA_Counters_PM_dbt  pm_value;*/
    time_t t1;
} MEA_drv_PM_dbt;

typedef struct {
    MEA_PmId_t base_index;
    MEA_PmId_t max_value;
} MEA_Counters_PM_Type_dbt;

MEA_Status mea_drv_Get_Cunters_PM_Type_Entry(MEA_Unit_t                unit_i,
                                             MEA_db_HwUnit_t           hwUnit_i,
                                             MEA_pm_type_te            pm_type_i,
                                             MEA_Counters_PM_Type_dbt *entry_po);

MEA_Bool mea_drv_IsPmIdInRange(MEA_PmId_t pmId);
MEA_Bool mea_drv_ispm_freeEntry(MEA_Unit_t unit_i,MEA_pm_type_te PmId_Type);
MEA_Bool mea_drv_IsPm_Exist(MEA_Unit_t unit, MEA_PmId_t index);
MEA_Status mea_drv_Init_BM_Counters(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_BM_Counters(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_BM_Counters(MEA_Unit_t unit_i);
MEA_Status mea_drv_Delete_PM(MEA_Unit_t unit, MEA_PmId_t id);

MEA_Status mea_drv_Set_PM(MEA_Unit_t               unit,
						  MEA_PmId_t              *id_io,
                          MEA_pm_type_te        PmId_Type,
						  MEA_Bool                 create_new_i);

MEA_Status mea_drv_get_pm_pool_entry(MEA_PmId_t      index,
									 MEA_Bool       *found,
									 MEA_drv_PM_dbt *entry_o );

MEA_Status mea_drv_Get_pm_db_ByType(MEA_Unit_t unit_i,
                                    MEA_db_dbt* db_po,
                                    MEA_pm_type_te type_i);


void  mea_drv_Get_pm_hwId_Calculate(MEA_Unit_t unit_i,
                                    MEA_PmId_t pmId,
                                    MEA_db_HwId_t *pm_hwId);

#ifdef __cplusplus
 }
#endif 


#endif /* MEA_BM_COUNTERS_DRV_H */
