/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_IF_SERVICE_DRV_H
#define MEA_IF_SERVICE_DRV_H



#include "MEA_platform.h"
#include "ENET_platform_types.h"
#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
extern "C" {
#endif 



#define MEA_SRV_PARSING_START_OFFSET_HASH_ADD_Start (61 +7 + MEA_Platform_GetMaxNumOfPort_BIT())
#define MEA_SRV_PARSING_START_OFFSET_HASH_ADD_WIDTH  10
#define MEA_SRV_PARSING_START_OFFSET_MEM_GROUP        2
#define MEA_SRV_PARSING_START_OFFSET_HASH_GROUP       3



typedef enum {
	MEA_SERVICE_CLASSIFCATION_REGULAR = 0,
	MEA_SERVICE_CLASSIFCATION_RANGE   = 1,
    MEA_SERVICE_CLASSIFCATION_EVC     = 2,
    MEA_SERVICE_CLASSIFCATION_DEFUALT_SID=3,
	MEA_SERVICE_CLASSIFCATION_LAST    = 4
} MEA_Service_classification_t;

typedef struct {
    MEA_Uint32     valid :1;
    MEA_Uint32     hush_group:2;
    MEA_Uint32     address_group:14;
    MEA_Uint32     pad           :15;

}MEA_Service_hush_data_dbt;

typedef struct {
    MEA_Bool   en_Sid_Internal;
    MEA_Uint32 Sid_Internal;
    MEA_Bool en_Sid_External;
    MEA_Uint32 Sid_External;
    MEA_Uint32 hashaddress_External;


	MEA_Service_Entry_Key_dbt key;
	MEA_Service_Entry_Data_dbt data;
    

	MEA_FilterMask_t filter_mask_id;
	ENET_xPermissionId_t xPermissionId;
    ENET_xPermissionId_t LearnxPermissionId;
	MEA_Service_classification_t service_Type;
    //MEA_Service_hush_data_dbt   hush_pri[64];
	MEA_Uint16 rangeId;	/* relevant only if the key vlan or pri range valid */

    MEA_Uint8                 hashGroup;
    MEA_OutPorts_Entry_dbt out_ports;
    MEA_OutPorts_Entry_dbt Learn_OutPorts;


} MEA_Service_Entry_dbt;

typedef struct{
 
    MEA_Uint32  Hw_priority                 : 6;
    MEA_Uint32  Hw_priType                  : 2;
    MEA_Uint32  Hw_src_port                 : 16; /**/
    MEA_Uint32  Hw_net_tag                  : 20;
    MEA_Uint32  Hw_sub_protocol_type        : 4;
    MEA_Uint32  Hw_L2_protocol_type         : 5;
    MEA_Uint32  Hw_inner_netTag_from_value  : 24;
    MEA_Uint32  HW_Ipv6msb_tag              : 27;

    

}MEA_Service_HW_SRV_Key;



typedef struct {
    MEA_Uint32 net1    : 20;
    MEA_Uint32 net2    : 24;
    MEA_Uint32 subType : 4;
    MEA_Uint32 L2Type  : 5;
    MEA_Uint32 priType : 2;
    MEA_Uint32 pri     : 6;
    MEA_Uint32 srcport : 16;
    MEA_Uint32 sig[4];
    
    MEA_Uint32  hash_address : 10;
    MEA_Uint32  hash_type    : 2;
    MEA_Uint32  hash_group   : 3;

}mea_event_hash_key_parsing_Internal_dbt;




typedef struct {
	MEA_Bool valid;
	MEA_Bool rate_enable;
	MEA_Uint32 num_of_owner;
	MEA_Port_t src_port;
	MEA_Uint32 vlan;
	MEA_Service_t Sid;
	MEA_Uint16 tmId;
} MEA_Service_PortVlan_dbt;

typedef struct {

	MEA_Service_Entry_Key_dbt key;
	MEA_Uint32 prof_number:16;
	MEA_Uint32 portVlanId:16;
	MEA_Uint32 valid:1;
	MEA_Uint32 rate_enable:1;
	MEA_TmId_t TmId;

} MEA_Service_PortVlanSid_dbt;
typedef struct {
	MEA_Uint32 valid:1;
	MEA_Service_Entry_Key_dbt key;
	MEA_Uint32 portVlanId:16;
	MEA_TmId_t SwTmId;

} MEA_Srv_PortVlanSid_dbt;


typedef struct{
    MEA_Uint32    valid                 : 1;
    MEA_Uint32    net_tag               :20;
    MEA_Uint32    src_port              : 7;
     MEA_Uint32    pad0                  : 4;

    MEA_Uint32    net_tag_to_value      :20;
    MEA_Uint32    priType               : 2;
    MEA_Uint32    pri                   : 6;
    MEA_Uint32    pad1                  : 4;
    
    MEA_Uint32    pri_to_value          : 6;
    MEA_Uint32    L2_protocol_type      :  5; /*see type   MEA_PARSING_L2_Type_t */
    MEA_Uint32    sub_protocol_type     : 4;
    MEA_Uint32    pad2                  : 17;
   

    MEA_Uint32    inner_netTag_from     :24;
    MEA_Uint32    inner_pri_from        : 6; /*NOT support to pri*/
    MEA_Uint32    pad3                  : 2;


    MEA_Uint32    inner_netTag_to       :24;
    MEA_Uint32    inner_pri_to          : 6; /*NOT support to pri*/
    MEA_Uint32    pri_DC_enable         : 1; 
    MEA_Uint32    net_tag_DC_enable     : 1;
    
    MEA_Uint32    hwId                  :16;
    MEA_Uint32    ptr_context           :16;

    


}MEA_SrvRange_Vlan_pri_dbt;

typedef struct{
    MEA_Uint32    valid       : 1;
    MEA_Uint32    portId;

    MEA_Uint32 num_of_owners;

}MEA_vlanRange_prof_dbt;


typedef struct
{
    MEA_Bool valid;
    MEA_Uint32 num_of_owners;

}MEA_srv_filter_prof_dbt;


//extern MEA_Service_t MEA_ServiceId_127_to_118;

MEA_Status mea_drv_Init_Service_Table(MEA_Unit_t unit_i);

MEA_Status mea_drv_ReInit_Service_Table(MEA_Unit_t unit_i);

MEA_Status mea_drv_Conclude_Service_Table(MEA_Unit_t unit_i);

MEA_Status mea_Init_Cpu_Service(MEA_Unit_t unit_i);

MEA_Status mea_Conclude_Cpu_Service(MEA_Unit_t unit_i);

MEA_Status mea_drv_Flush_HW_Service_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_Init_HW_serviceRange_Table(MEA_Unit_t unit_i);

MEA_Status mea_drv_Get_Service_Internal_db(MEA_Unit_t unit_i, MEA_db_dbt * db_po);
MEA_Status mea_drv_Get_ServiceRange_db(MEA_Unit_t unit_i,MEA_db_dbt * db_po);


MEA_Status mea_drv_Get_xPermissionIdbyIndex(MEA_Uint32 index,ENET_xPermissionId_t *xPermissionId);
MEA_Status mea_drv_serviceRange_Get_Entry(MEA_Uint16 index,MEA_SrvRange_Vlan_pri_dbt  *entry_o);

MEA_Status mea_drv_Service_ReUpdateByPortAndVlan(MEA_Unit_t    unit_i,
                                                 MEA_Port_t    port_i,
                                                 MEA_Uint16    vlan_i,
                                                 MEA_PortState_te state_i);



MEA_Status mea_drv_Init_vlanrange_profile(MEA_Unit_t unit_i);
MEA_Bool mea_drv_RInit_vlanrange_profile(MEA_Unit_t       unit_i);
MEA_Bool mea_drv_vlanrange_profile_Conclude(MEA_Unit_t       unit_i);

MEA_Status mea_drv_vlanRange_Mapping_profile(MEA_Unit_t unit_i,  MEA_Port_t port,MEA_Bool   *valid,MEA_Uint16   *id_io );

MEA_Status mea_ServiceEntry_buildHWData(MEA_Unit_t              unit_i,
    MEA_db_HwUnit_t         hwUnit_i,
    MEA_db_HwId_t           hwId_i,
    MEA_Service_Entry_dbt *entry_pi,
    MEA_Uint32 len,
    MEA_Uint32  *retVal);

MEA_Status  MEA_DRV_Delete_Service(MEA_Unit_t         unit_i, MEA_Service_t    serviceId_i, MEA_Bool force);
MEA_Status  MEA_DRV_Delete_all_services(MEA_Unit_t    unit, MEA_Bool force);
MEA_Status mea_drv_Get_Service_ExtrnalHash(MEA_Unit_t unit_i,
                                            MEA_Service_t  serviceId_i,
                                            MEA_SRV_mode_t SRV_mode,
                                            MEA_Uint32     *hashaddress,
                                            MEA_Bool      *exist_o);

MEA_Status mea_drv_service_intrenal_Get_result_info(MEA_Unit_t unit, MEA_db_HwUnit_t hwUnit, MEA_db_HwId_t hwId, MEA_Uint32 *val);
MEA_Status mea_drv_service_intrenal_result_info_parsing(MEA_Unit_t unit_i, MEA_Uint32 *val, mea_event_hash_key_parsing_Internal_dbt *entry);
void mea_drv_ServiceGetInternal_group(MEA_Service_t serviceId, MEA_Uint8 *hashGroup);

#ifdef __cplusplus
 }
#endif 

#endif /* MEA_IF_SERVICE_DRV_H */

