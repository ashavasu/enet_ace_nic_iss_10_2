/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_GLOBALS_DRV_H
#define MEA_GLOBALS_DRV_H



#include "MEA_platform.h"
#include "mea_drv_common.h"

#ifdef __cplusplus
extern "C" {
#endif 

MEA_Status mea_drv_Init_Globals_Entry(MEA_Unit_t unit);
MEA_Status mea_drv_ReInit_Globals_Entry(MEA_Unit_t unit_i);
MEA_Status mea_drv_Init_Global_Interface_G999_1_Entry(MEA_Unit_t unit);
MEA_Status mea_drv_ReInit_Global_Interface_G999_1_Entry(MEA_Unit_t unit_i);

MEA_Status mea_drv_set_if_global1(MEA_Unit_t  unit,MEA_Uint32 val);
MEA_Uint32 mea_drv_get_if_global1(MEA_Unit_t  unit);
MEA_Uint32 mea_grv_get_if_FWD_address(MEA_Unit_t  unit);
MEA_Uint32 mea_drv_get_SRV_E_hash(MEA_Unit_t  unit);
MEA_Uint32 mea_drv_get_SRV_E_srv_mode(MEA_Unit_t  unit);

MEA_Status mea_Global_offsetMAC_UpdateEntry(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_MacAddr new_entry);
MEA_Status mea_Global_offsetMAC_UpdateEntryBM(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_MacAddr new_entry);

MEA_Status mea_Global_TBL_offset_UpdateEntry(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_Uint32 new_entry);
MEA_Status mea_Global_TBL_offset_UpdateEntryBM(MEA_Unit_t  unit, MEA_Uint32 offset, MEA_Uint32 new_entry);

MEA_Status mea_drv_global_GW_init(MEA_Unit_t  unit);
MEA_Status mea_drv_global_GW_Reinit(MEA_Unit_t  unit);
MEA_Status mea_drv_global_GW_Conclude(MEA_Unit_t  unit);

/*-------------------------------------------------------------------------------*/
/*                    MEA CAM                                                    */
/*-------------------------------------------------------------------------------*/
#define MEA_CAM_TBL_MAX_ENTRIES   8

typedef struct {
	MEA_Uint32      ethertype : 16;  /* 0-15  */
	MEA_Uint32      ether_pri : 3;   /* 16-18 */
	MEA_Uint32      l2cp      : 6;   /* 19-24 */
	MEA_Uint32      valid     : 1;   /* 25-25 */
	MEA_Uint32      pad0      : 6;   /* 26-31 */
} MEA_CAM_Entry_dbt;

MEA_Status mea_drv_Init_CAM_Tbl(void);
MEA_Bool   mea_drv_Is_CAM_EtherType_Exist( MEA_Uint16 ethertype );
MEA_Status mea_drv_CAM_Add_Entry(MEA_CAM_Entry_dbt*  entry_i,
								 MEA_Uint32          *id_o);
MEA_Status mea_drv_get_CAM_entry (MEA_Uint32 	  	   id_i,
                                  MEA_CAM_Entry_dbt    *entry_o,
                                  MEA_Bool             *found_o);
MEA_Status mea_drv_delete_CAM_entry (MEA_Uint32 id_i);


#define MEA_CHECK_CAM_INDEX_IN_RANGE(index) \
	if(!MEA_API_IsCAMIndexInRange(index)){ \
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                          "%s - Index %d is out of range\n", \
                          __FUNCTION__,index); \
		return MEA_ERROR;	\
	}					\

MEA_Bool MEA_API_IsCAMIndexInRange(MEA_Uint32     index);

MEA_Status mea_drv_global_set_if_protection_bit (MEA_Bool val);

#ifdef __cplusplus
 }
#endif 

#endif /* MEA_GLOBALS_DRV_H */

