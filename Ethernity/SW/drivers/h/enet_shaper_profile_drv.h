/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/******************************************************************************
* 	Module Name:		 enet_Shaper_Profile_drv.h
*
*   Module Description:
*
*
*  Date of Creation:	 17/09/06
*
*  Author Name:			 Alex Weis.
*******************************************************************************/
#ifndef _ENET_SHAPER_PROFILE_DRV_H_
#define  _ENET_SHAPER_PROFILE_DRV_H_

#include "ENET_platform_types.h"
#include "mea_api.h"

#ifdef __cplusplus
extern "C" {
#endif
ENET_Status enet_Init_Shaper_Profile    (ENET_Unit_t unit_i);
ENET_Status enet_ReInit_Shaper_Profile  (ENET_Unit_t unit_i);
ENET_Status enet_Conclude_Shaper_Profile(ENET_Unit_t unit_i);

/* The next two APIs should be called internally by the driver 
   when egressPort,cluster,priorityQueue want to refer to this 
   profile */
ENET_Status enet_AddOwner_Shaper_Profile (ENET_Unit_t                  unit_i,
                                          ENET_Shaper_Profile_key_dbt *key_io,
                                          ENET_Shaper_Profile_dbt     *entry_i);
ENET_Status enet_DelOwner_Shaper_Profile(ENET_Unit_t                  unit_i,
                                         ENET_Shaper_Profile_key_dbt *key_i);



#ifdef __cplusplus
}
#endif
#endif				/*  _ENET_SHAPER_PROFILE_DRV_H_ */
