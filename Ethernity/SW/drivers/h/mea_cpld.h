/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_cpld.h,v $
#
#------------------------------------------------------------------------------
*/

#ifndef _MEA_CPLD_H_
#define _MEA_CPLD_H_

#include "MEA_platform.h"

#ifdef __cplusplus
 extern "C" {
 #endif 




MEA_Status mea_drv_CPLD_ResetModule(MEA_module_te mea_module);
MEA_Status mea_drv_CPLD_Init(void);
//MEA_Status mea_drv_CPLD_MappedMemory();
//MEA_Status mea_drv_CPLD_CleanUp_MappedMemory();

#ifdef __cplusplus
 }
#endif 


#endif
