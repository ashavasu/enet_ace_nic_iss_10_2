/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_DB_DRV_H
#define MEA_DB_DRV_H



/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/
#include "MEA_platform.h"

#ifdef __cplusplus
extern "C" {
#endif 
/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/
#define MEA_DB_GENERATE_NEW_ID MEA_PLAT_GENERATE_NEW_ID

/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/


typedef enum {
	MEA_DB_MODE_CREATE,
	MEA_DB_MODE_UPDATE,
	MEA_DB_MODE_DELETE,
	MEA_DB_MODE_LAST
} MEA_db_mode_te;

typedef MEA_Status (*MEA_db_num_of_sw_size_func_t)(MEA_Unit_t  unit_i,
												   MEA_db_SwId_t *length_o,
												   MEA_Uint32 *width_o);

typedef MEA_Status (*MEA_db_num_of_hw_size_func_t)(MEA_Unit_t     unit_i,
												   MEA_db_HwUnit_t   hwUnit_i,
												   MEA_db_HwId_t *length_o,
												   MEA_Uint16    *num_of_regs_o);

typedef MEA_Status (*MEA_db_ReInit_callback_func_t)(MEA_Unit_t       unit_i,
												    MEA_ind_write_t* ind_write_pio);

typedef struct {
	MEA_Uint16 valid:1;
	MEA_Uint16 pad:15;
	MEA_db_HwId_t hwId;
} MEA_db_SwHw_Entry_dbt;

typedef struct {
	MEA_Uint16 valid:1;
	MEA_Uint16 pad:15;
	MEA_db_SwId_t swId;
    long *regs; /*regs of 32bit but the pointer is long*/
} MEA_db_Hw_Entry_dbt;

typedef struct {
	MEA_db_HwId_t num_of_entries;
	MEA_db_HwId_t used_entries;
	MEA_Uint16 pad;
	MEA_Uint16 num_of_regs;
	MEA_db_Hw_Entry_dbt *table;
} MEA_db_HwUnit_Entry_dbt;

/*----------------------------------------------------------------------------*/
/*             APIs                                                           */
/*----------------------------------------------------------------------------*/
MEA_Status mea_db_Init(MEA_Unit_t unit_i,
		       const char *name_i,
		       MEA_db_num_of_sw_size_func_t num_of_sw_size_func_i,
		       MEA_db_num_of_hw_size_func_t num_of_hw_size_func_i,
		       MEA_db_dbt * db_o);

MEA_Status mea_db_ReInit(MEA_Unit_t unit_i,
			 MEA_db_dbt db_i,
			 MEA_Uint32 tableType_i,
			 MEA_module_te module_i,
			 MEA_db_ReInit_callback_func_t callback_i,
             MEA_Bool   Is_DDR);

MEA_Status mea_db_Conclude(MEA_Unit_t unit_i, MEA_db_dbt * db_io);

MEA_Status mea_db_Create(MEA_Unit_t unit_i,
                         MEA_db_dbt db_i,
                         MEA_db_SwId_t swId_i,
                         MEA_db_HwUnit_t hwUnit_i,
                         MEA_db_HwId_t * hwId_io);

MEA_Status mea_db_Delete(MEA_Unit_t unit_i,
                         MEA_db_dbt db_i,
                         MEA_db_SwId_t swId_i,
                         MEA_db_HwUnit_t hwUnit_i);

MEA_Status mea_db_Get_SwHw(MEA_Unit_t            unit_i,
                           MEA_db_dbt            db_i,
                           MEA_db_SwId_t         swId_i,
                           MEA_db_HwUnit_t       hwUnit_i,
                           MEA_db_SwHw_Entry_dbt *entry_po);

MEA_Status mea_db_GetFirst_SwHw(MEA_Unit_t                    unit_i,
                                MEA_db_dbt                    db_i,
                                MEA_db_SwId_t                *swId_o,
                                MEA_db_HwUnit_t              *hwUnit_o,
                                MEA_db_SwHw_Entry_dbt        *entry_po,
                                MEA_Bool                     *found_o);

MEA_Status mea_db_GetNext_SwHw(MEA_Unit_t                    unit_i,
                               MEA_db_dbt                    db_i,
                               MEA_db_SwId_t                *swId_io,
                               MEA_db_HwUnit_t              *hwUnit_io,
                               MEA_db_SwHw_Entry_dbt        *entry_po,
                               MEA_Bool                     *found_o);

MEA_Status mea_db_Get_HwUnit(MEA_Unit_t                    unit_i,
                             MEA_db_dbt                    db_i,
                             MEA_db_HwUnit_t               hwUnit_i,
                             MEA_db_HwUnit_Entry_dbt      *entry_po);

MEA_Status mea_db_GetFirst_HwUnit(MEA_Unit_t                    unit_i,
                                  MEA_db_dbt                    db_i,
                                  MEA_db_HwUnit_t              *hwUnit_o,
                                  MEA_db_HwUnit_Entry_dbt      *entry_po,
                                  MEA_Bool                     *found_o);

MEA_Status mea_db_GetNext_HwUnit(MEA_Unit_t                    unit_i,
					             MEA_db_dbt                    db_i,
                                 MEA_db_HwUnit_t                 *hwUnit_io,
						         MEA_db_HwUnit_Entry_dbt      *entry_po,
							     MEA_Bool                     *found_o);

MEA_Status mea_db_Get_Hw(MEA_Unit_t                    unit_i,
                         MEA_db_dbt                    db_i,
                         MEA_db_HwUnit_t                  hwUnit_i,
                         MEA_db_HwId_t                 hwId_i,
                         MEA_db_Hw_Entry_dbt          *entry_po);

MEA_Status mea_db_GetFirst_Hw(MEA_Unit_t                    unit_i,
                              MEA_db_dbt                    db_i,
                              MEA_db_HwUnit_t               hwUnit_i,
                              MEA_db_HwId_t                *hwId_o,
                              MEA_db_Hw_Entry_dbt          *entry_po,
                              MEA_Bool                     *found_o);

MEA_Status mea_db_GetNext_Hw(MEA_Unit_t                    unit_i,
                             MEA_db_dbt                    db_i,
                             MEA_db_HwUnit_t                  hwUnit_i,
                             MEA_db_HwId_t                *hwId_io,
                             MEA_db_Hw_Entry_dbt          *entry_po,
                             MEA_Bool                     *found_o);

MEA_Status mea_db_Set_Hw_Regs(MEA_Unit_t                  unit_i,
					          MEA_db_dbt                  db_i,
                              MEA_db_HwUnit_t                hwUnit_i,
					          MEA_db_HwId_t               hwId_i,
						      MEA_Uint16                  num_of_regs,
						      MEA_Uint32*                 regs_pi);


/*----------------------------------------------------------------------------*/
/*           MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT                         */
/*----------------------------------------------------------------------------*/
#define MEA_DB_GET_NUM_OF_SW_SIZE_FUNC_IMPLEMENT(func_name_i,length_i,width_i) \
MEA_Status func_name_i(MEA_Unit_t       unit_i,                                \
					   MEA_db_SwId_t   *length_o,                              \
					   MEA_Uint32      *width_o)                               \
{                                                                              \
                                                                               \
                                                                               \
	if (length_o == NULL) {                                                    \
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,                                    \
			              "%s - legnth_o == NULL\n",                           \
						  __FUNCTION__);                                       \
		return MEA_ERROR;                                                      \
	}                                                                          \
                                                                               \
	if (width_o == NULL) {                                                     \
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,                                    \
			              "%s - width_o == NULL\n",                            \
						  __FUNCTION__);                                       \
		return MEA_ERROR;                                                      \
	}                                                                          \
                                                                               \
    *length_o = (length_i);                                                    \
	*width_o  = (width_i);                                                     \
                                                                               \
	return MEA_OK;                                                             \
                                                                               \
}

/*----------------------------------------------------------------------------*/
/*           MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT                         */
/*----------------------------------------------------------------------------*/
#define MEA_DB_GET_NUM_OF_HW_SIZE_FUNC_IMPLEMENT(func_name_i,length_i,num_of_regs_i) \
MEA_Status func_name_i(MEA_Unit_t       unit_i,                                \
					   MEA_db_HwUnit_t  hwUnit_i,                              \
					   MEA_db_HwId_t   *length_o,                              \
					   MEA_Uint16      *num_of_regs_o)                         \
{                                                                              \
                                                                               \
	if (hwUnit_i >= mea_drv_num_of_hw_units) {                                 \
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,                                    \
			              "%s - hwUnit_i (%d) >= max value (%d) \n",           \
						  __FUNCTION__,                                        \
						  hwUnit_i,                                            \
						  mea_drv_num_of_hw_units);                            \
		return MEA_ERROR;                                                      \
	}                                                                          \
                                                                               \
	if (length_o == NULL) {                                                    \
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,                                    \
			              "%s - legnth_o == NULL\n",                           \
						  __FUNCTION__);                                       \
		return MEA_ERROR;                                                      \
	}                                                                          \
                                                                               \
	if (num_of_regs_o == NULL) {                                               \
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,                                    \
			              "%s - num_of_regs_o == NULL\n",                      \
						  __FUNCTION__);                                       \
		return MEA_ERROR;                                                      \
	}                                                                          \
                                                                               \
    *length_o       = (length_i);                                                    \
	*num_of_regs_o  = (num_of_regs_i);                                               \
                                                                               \
	return MEA_OK;                                                             \
                                                                               \
}

/*----------------------------------------------------------------------------*/
/*           MEA_DB_GET_DB_FUNC_IMPLEMENT                                     */
/*----------------------------------------------------------------------------*/
#define MEA_DB_GET_DB_FUNC_IMPLEMENT(func_name_i,db_i) \
MEA_Status func_name_i(MEA_Unit_t unit_i,            \
					   MEA_db_dbt* db_po) {          \
                                                     \
	if (db_po == NULL) {                             \
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,          \
			              "%s - db_po == NULL\n",    \
						  __FUNCTION__);             \
		return MEA_ERROR;                            \
	}                                                \
                                                     \
	*db_po = (db_i);                                 \
                                                     \
	return MEA_OK;                                   \
                                                     \
}

#ifdef __cplusplus
 }
#endif 


#endif /* MEA_DB_DRV_H */



