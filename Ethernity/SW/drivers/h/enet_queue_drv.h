/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/******************************************************************************
* 	Module Name:		 enet_queue_drv.h	   									     
*																					 
*   Module Description:	     
*                            
*                         
*	                     
*                        
*                        
*																					 
*  Date of Creation:	 14/09/06													 
*																					 
*  Autor Name:			 Alex Weis.													 
*******************************************************************************/
#ifndef _ENET_QUEUE_DRV_H_
#define _ENET_QUEUE_DRV_H_

#include "MEA_platform.h"
#include "enet_general_drv.h"
#ifdef __cplusplus
extern "C" {
#endif

#define ENET_QUEUE_MC_MQS_DEF_VAL 32   /*MAX val*/

#define MEA_MQS_MAX_ALLOW           (2*1024)

#define IS_INTERNAL_QUEUE_ID_VALID(q) (q<64)
#define IS_QUEUE_ID_VALID(q) (q<ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT)

#define ENET_MAX_NUMBER_OF_PRI_QUEUE    ENET_PLAT_QUEUE_NUM_OF_PRI_Q

#define ENET_CLUSTER_STRICT_DEF_VAL 0
#define ENET_CLUSTER_RR_DEF_VAL    10
#define ENET_CLUSTER_WFQ_DEF_VAL   20
#define ENET_PQ_WFQ_DEF_VAL        64
#define ENET_PQ_STRICT_DEF_VAL      0
#define ENET_PQ_RR_DEF_VAL         10


#define ENET_PRI_QUEUE_MQS_PACKET_DEF_0        0
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_1        1
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_2        2
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_4        4
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_8        8
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_16       16
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_32       32
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_64       64
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_128      128
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_256      256
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_512      512
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_1K      1024
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_2K      2048




/************************************************************************/
/*                                                                      */
/************************************************************************/




#define ENET_PRI_QUEUE_MQS_PACKET_DEF0_VAL(pri) \
    ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
     (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
     (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
     (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
     (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
     (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_4) : \
     (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_4) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_16))


#define ENET_PRI_QUEUE_MQS_BYTES_DEF0_VAL(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF0_VAL(pri)*2048)
   
#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF0_VAL(pri) \
   ((pri == 0) ? (32) : \
    (pri == 1) ? (8) : \
    (pri == 2) ? (8) : \
    (pri == 3) ? (8) : \
    (pri == 4) ? (4) : \
    (pri == 5) ? (4) : \
    (pri == 6) ? (2) : \
    (pri == 7) ? (2) : \
    (4))




/************************************************************************/
/*                                                                      */
/************************************************************************/

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL(pri) \
    ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_16))


#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL(pri)*2048)
 

#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL(pri) \
   ((pri == 0) ? (32) : \
    (pri == 1) ? (16) : \
    (pri == 2) ? (16) : \
    (pri == 3) ? (16) : \
    (pri == 4) ? (16) : \
    (pri == 5) ? (8) : \
    (pri == 6) ? (8) : \
    (pri == 7) ? (8) : \
    (4))


/************************************************************************/
/*                                                                      */
/************************************************************************/
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS(pri) \
    ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
     (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
     (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
     (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
     (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
     (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_4) : \
     (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_4) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_16))


#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_TLS(pri)  (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS(pri)*2048)
 

#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS(pri) \
   ((pri == 0) ? (32) : \
    (pri == 1) ? (16) : \
    (pri == 2) ? (16) : \
    (pri == 3) ? (16) : \
    (pri == 4) ? (16) : \
    (pri == 5) ? (8) : \
    (pri == 6) ? (8) : \
    (pri == 7) ? (8) : \
    (4))



/************************************************************************/
/*                                                                      */
/************************************************************************/




/************************************************************************/
/*                                                                      */
/************************************************************************/

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_128_VAL(pri)  \
        ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
        (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_4) : \
        (ENET_PRI_QUEUE_MQS_PACKET_DEF_128))

#define ENET_PRI_QUEUE_MQS_BYTES_DEF_128_VAL(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_128_VAL(pri) *2048)

#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_128_VAL(pri) \
   ((pri == 0) ? (32) : \
    (pri == 1) ? (16) : \
    (pri == 2) ? (16) : \
    (pri == 3) ? (16) : \
    (pri == 4) ? (16) : \
    (pri == 5) ? (16) : \
    (pri == 6) ? (8) : \
    (pri == 7) ? (8) : \
    (2))

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G(pri) \
    ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
     (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_16))


#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G(pri) \
   ((pri == 0) ? (32) : \
    (pri == 1) ? (16) : \
    (pri == 2) ? (16) : \
    (pri == 3) ? (16) : \
    (pri == 4) ? (16) : \
    (pri == 5) ? (8) : \
    (pri == 6) ? (8) : \
    (pri == 7) ? (8) : \
    (4))



/************************************************************************/
/*                                                                      */
/************************************************************************/

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_10G(pri)  \
        ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
        (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
        (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
        (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
        (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
        (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
        (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_64) : \
        (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_64) : \
        (ENET_PRI_QUEUE_MQS_PACKET_DEF_128))

#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_10G(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_10G(pri) *2048)
 


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_10G(pri) \
   ((pri == 0) ? (32) : \
    (pri == 1) ? (16) : \
    (pri == 2) ? (16) : \
    (pri == 3) ? (16) : \
    (pri == 4) ? (16) : \
    (pri == 5) ? (8) : \
    (pri == 6) ? (8) : \
    (pri == 7) ? (8) : \
    (2))


/************************************************************************/
/*                                                                      */
/************************************************************************/

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU(pri)  \
        ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (ENET_PRI_QUEUE_MQS_PACKET_DEF_32))



#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_CPU(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU(pri) *2048)
    

#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_CPU(pri) \
   ((pri == 0) ? (16) : \
    (pri == 1) ? (16) : \
    (pri == 2) ? (16) : \
    (pri == 3) ? (8) : \
    (pri == 4) ? (8) : \
    (pri == 5) ? (8) : \
    (pri == 6) ? (8) : \
    (pri == 7) ? (4) : \
    (2))

/************************************************************************/
/*     EPC configure                                                     */
/************************************************************************/

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_10G_EPC(pri)  \
         ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
          (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
          (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
          (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
          (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
          (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_64) : \
          (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_64) : \
          (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_64) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_8))

#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_10G_EPC(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_10G_EPC(pri) *2048)




#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_10G_EPC(pri) \
   ((pri == 0) ? (16) : \
    (pri == 1) ? (16) : \
    (pri == 2) ? (16) : \
    (pri == 3) ? (16) : \
    (pri == 4) ? (16) : \
    (pri == 5) ? (16) : \
    (pri == 6) ? (16) : \
    (pri == 7) ? (16) : \
    (4))




#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G_EPC(pri) \
   ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_8))



 
#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G_EPC(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G_EPC(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G_EPC(pri) \
   ((pri == 0) ? (16) : \
    (pri == 1) ? (16) : \
    (pri == 2) ? (16) : \
    (pri == 3) ? (16) : \
    (pri == 4) ? (16) : \
    (pri == 5) ? (16) : \
    (pri == 6) ? (8) : \
    (pri == 7) ? (8) : \
    (4))







/************************************************************************/
/*                                                                      */
/************************************************************************/

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_126(pri) \
   ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_8))





#define ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_126(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_126(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_126(pri) \
  ((pri == 0) ? (32) : \
    (pri == 1) ? (16) : \
    (pri == 2) ? (16) : \
    (pri == 3) ? (16) : \
    (pri == 4) ? (16) : \
    (pri == 5) ? (16) : \
    (pri == 6) ? (16) : \
    (pri == 7) ? (16) : \
    (4))







/************************************************************************/
/*                                                                      */
/************************************************************************/
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_25(pri) \
   ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_8))

#define ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_25(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_25(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_25(pri) \
   ((pri == 0) ? (0) : \
    (pri == 1) ? (0) : \
    (pri == 2) ? (0) : \
    (pri == 3) ? (0) : \
    (pri == 4) ? (0) : \
    (pri == 5) ? (0) : \
    (pri == 6) ? (0) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_64) : \
    (2))









/************************************************************************/
/*                                                                      */
/************************************************************************/

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_26(pri) \
   ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_8))




#define ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_26(pri)  (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_26(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_26(pri) \
   ((pri == 0) ? (0) : \
    (pri == 1) ? (0) : \
    (pri == 2) ? (0) : \
    (pri == 3) ? (0) : \
    (pri == 4) ? (0) : \
    (pri == 5) ? (0) : \
    (pri == 6) ? (0) : \
    (pri == 7) ? (0) : \
    (2))

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_27(pri) \
   ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_16))




#define ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_27(pri)  (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_27(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_27(pri) \
   ((pri == 0) ? (0) : \
    (pri == 1) ? (0) : \
    (pri == 2) ? (0) : \
    (pri == 3) ? (0) : \
    (pri == 4) ? (0) : \
    (pri == 5) ? (0) : \
    (pri == 6) ? (0) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_128) : \
    (2))





#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU_EPC(pri)  \
        ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (ENET_PRI_QUEUE_MQS_PACKET_DEF_32))





#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_CPU_EPC(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU_EPC(pri) *2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_CPU_EPC(pri) \
   ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
    (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
    (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
    (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
    (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
    (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
    (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
    (2))








/************************************************************************/
/*   EMB                                                                   */
/************************************************************************/
#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G_EMB(pri) \
       ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (ENET_PRI_QUEUE_MQS_PACKET_DEF_32))


#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_1G_EMB(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_1G_EMB(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_1G_EMB(pri) \
   ((pri == 0) ? (8) : \
    (pri == 1) ? (8) : \
    (pri == 2) ? (8) : \
    (pri == 3) ? (8) : \
    (pri == 4) ? (8) : \
    (pri == 5) ? (8) : \
    (pri == 6) ? (8) : \
    (pri == 7) ? (8) : \
    (4))

#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU_EMB(pri)  \
    ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (ENET_PRI_QUEUE_MQS_PACKET_DEF_32))



#define ENET_PRI_QUEUE_MQS_BYTES_DEF_VAL_CPU_EMB(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_CPU_EMB(pri) *2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_CPU_EMB(pri) \
   ((pri == 0) ? (4) : \
    (pri == 1) ? (4) : \
    (pri == 2) ? (4) : \
    (pri == 3) ? (4) : \
    (pri == 4) ? (4) : \
    (pri == 5) ? (4) : \
    (pri == 6) ? (4) : \
    (pri == 7) ? (4) : \
    (2))





#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_126_EMB(pri) \
       ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
        (ENET_PRI_QUEUE_MQS_PACKET_DEF_16))



#define ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_126_EMB(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_126_EMB(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_126_EMB(pri) \
   ((pri == 0) ? (0) : \
    (pri == 1) ? (0) : \
    (pri == 2) ? (0) : \
    (pri == 3) ? (0) : \
    (pri == 4) ? (0) : \
    (pri == 5) ? (0) : \
    (pri == 6) ? (0) : \
    (pri == 7) ? (0) : \
    (2))


#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_24_EMB(pri) \
        ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (ENET_PRI_QUEUE_MQS_PACKET_DEF_32))


#define ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_24_EMB(pri)  (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_24_EMB(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_24_EMB(pri) \
   ((pri == 0) ? (4) : \
    (pri == 1) ? (4) : \
    (pri == 2) ? (4) : \
    (pri == 3) ? (4) : \
    (pri == 4) ? (4) : \
    (pri == 5) ? (4) : \
    (pri == 6) ? (4) : \
    (pri == 7) ? (4) : \
    (2))





#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_25_EMB(pri) \
   ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
     (ENET_PRI_QUEUE_MQS_PACKET_DEF_8))

#define ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_25_EMB(pri) (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_25_EMB(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_25_EMB(pri) \
   ((pri == 0) ? (0) : \
    (pri == 1) ? (0) : \
    (pri == 2) ? (0) : \
    (pri == 3) ? (0) : \
    (pri == 4) ? (0) : \
    (pri == 5) ? (0) : \
    (pri == 6) ? (0) : \
    (pri == 7) ? (16) : \
    (2))




#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_26_EMB(pri) \
        ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_8) : \
        (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_32) : \
        (ENET_PRI_QUEUE_MQS_PACKET_DEF_32))


#define ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_26_EMB(pri)  (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_26_EMB(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_26_EMB(pri) \
   ((pri == 0) ? (0) : \
    (pri == 1) ? (0) : \
    (pri == 2) ? (0) : \
    (pri == 3) ? (0) : \
    (pri == 4) ? (0) : \
    (pri == 5) ? (0) : \
    (pri == 6) ? (0) : \
    (pri == 7) ? (0) : \
    (2))


#define ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_27_EMB(pri) \
   ((pri == 0) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 1) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 2) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 3) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 4) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 5) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 6) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_0) : \
    (pri == 7) ? (ENET_PRI_QUEUE_MQS_PACKET_DEF_16) : \
    (ENET_PRI_QUEUE_MQS_PACKET_DEF_16))




#define ENET_PRI_QUEUE_MQS_BYTES_DEF_TLS_27_EMB(pri)  (ENET_PRI_QUEUE_MQS_PACKET_DEF_VAL_TLS_27_EMB(pri)*2048)


#define ENET_PRI_QUEUE_MC_MQS_PACKET_DEF_VAL_TLS_27_EMB(pri) \
   ((pri == 0) ? (0) : \
    (pri == 1) ? (0) : \
    (pri == 2) ? (0) : \
    (pri == 3) ? (0) : \
    (pri == 4) ? (0) : \
    (pri == 5) ? (0) : \
    (pri == 6) ? (0) : \
    (pri == 7) ? (16) : \
    (2))







#define UNASSIGNED_QUEUE_VAL  0xffff    //999




#define REVERSE_MAPPING_TABLE_SIZE ENET_BM_TBL_TYP_CLUSTER_TO_PORT_LENGTH
#define REVERSE_MAPPING_PORT_INDX_SIZE ENET_BM_TBL_TYP_CLUSTER_PORT_INFORMATION_LENGTH

#define ENET_CLUSTER_INTERNAL_2_GROUP_ID(internal_qid) ((internal_qid)/ENET_QUEUE_NUMBER_OF_INTERNAL_QUEUES_PER_GROUP)

typedef struct {
	ENET_Uint32 location;	/*in the HW we have 7 bit but we initiation */
	ENET_Uint32 numOf_strict;	
} enet_ReverseMappingIndx_dbt;

typedef ENET_QueueId_t enet_ReverseMappingData_dbt;

typedef struct {
	ENET_Uint16 ClusterLocation;
	ENET_PortId_t PortNumber;
	ENET_Queue_mode_dbt mode;
    ENET_Uint16 pad;
} enet_Queue_Cluster2Port_dbt;

typedef struct {
	ENET_Uint16 location;
	ENET_Uint16 strict_level;
} enet_HwMirror_ClusterPortInformation_dbt;

typedef struct {
	ENET_PortId_t PortNumber;
	ENET_Uint16 ClusterLocation;
	ENET_QueueId_t queueId;
    ENET_Uint16 pad;
} enet_HwMirror_ClusterToPort_dbt;

extern enet_ReverseMappingIndx_dbt              enet_Hw_portReverseMappingIndx      [REVERSE_MAPPING_PORT_INDX_SIZE];  /* this is table 20*/
extern enet_Queue_Cluster2Port_dbt              enet_Hw_Cluster2Port                [REVERSE_MAPPING_TABLE_SIZE];
extern enet_ReverseMappingData_dbt              enet_Hw_portReverseMappingData      [REVERSE_MAPPING_TABLE_SIZE];  /* this is table 20*/
extern enet_HwMirror_ClusterPortInformation_dbt enet_HwMirror_ClusterPortInformation[REVERSE_MAPPING_PORT_INDX_SIZE];
extern enet_HwMirror_ClusterToPort_dbt          enet_HwMirror_ClusterToPort         [REVERSE_MAPPING_TABLE_SIZE];


ENET_Status enet_Init_Queue     (ENET_Unit_t unit_i);
ENET_Status enet_ReInit_Queue   (ENET_Unit_t unit_i);
ENET_Status enet_Conclude_Queue (ENET_Unit_t unit_i);
void	enet_Debug_GetInfo  (ENET_Uint32 index,ENET_Bool *isIndexValid,
							 ENET_Uint32 *portId,ENET_Bool *isPortEnable,ENET_Uint32 *internalQueueId,ENET_Uint32 *externalQueueId, ENET_Uint32* location,
							 ENET_Bool *isPortIndex,ENET_Bool *isLastIndex);
ENET_Status enet_InitDefaultQueueForPort (ENET_Unit_t  unit_i,ENET_PortId_t port,ENET_QueueId_t Qid);
MEA_Status  MEA_Queue_port_status_changed(ENET_PortId_t port,MEA_Bool isPortEnabled);
ENET_QueueId_t enet_cluster_external2internal(ENET_QueueId_t queueId);
ENET_QueueId_t enet_cluster_internal2external(ENET_QueueId_t internalqueueId);

ENET_Status ENET_Get_realPriQueueSize_Info_drv(ENET_Unit_t               unit,
                                       ENET_QueueId_t            id_i,/*external*/
                                       ENET_PriQueueId_t         pri_id_i,
                                       ENET_PriQueueSize_Info_t *info);
MEA_Status mea_Get_queue_Drop_MTU_Priq(MEA_Unit_t      unit_i,
											ENET_QueueId_t  QueueId_i,
											MEA_Uint8      *drop_MTU_priq);




ENET_Status ENET_queue_check_OutPorts(ENET_Unit_t unit_i,MEA_OutPorts_Entry_dbt *OutPorts_Entry);

ENET_Status ENET_Get_Internal_PriQueueSize_Info(ENET_Unit_t               unit,
												ENET_QueueId_t            id_i,/*internal*/
												ENET_PriQueueId_t         pri_id_i,
												ENET_PriQueueSize_Info_t *info);

MEA_Status MEA_drv_Queue_UPDATE_ForPort(ENET_Unit_t  unit_i, ENET_PortId_t port);


#ifdef __cplusplus
}
#endif

#endif  /* _ENET_QUEUE_DRV_H_ */



