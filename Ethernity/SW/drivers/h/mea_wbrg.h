/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_cpld.h,v $
#
#------------------------------------------------------------------------------
*/

#ifndef _MEA_CPLD_H_
#define _MEA_CPLD_H_

#include "MEA_platform.h"

#ifdef __cplusplus
 extern "C" {
 #endif 
#define MEA_WBRG_EDIT_TRANSFORM_DATA \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    22, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    22, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    0, \
    5, \
    6, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    13, \
    30, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    13, \
    30, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    13, \
    30, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    1, \
    6, \
    3, \
    8, \
    13, \
    30, \
    3, \
    40, \
    9, \
    2, \
    3, \
    40, \
    9, \
    2, \
    3, \
    40, \
    9, \
    2, \
    3, \
    40, \
    45, \
    14, \
    3, \
    40, \
    9, \
    2, \
    3, \
    40, \
    9, \
    2, \
    3, \
    40, \
    9, \
    2, \
    3, \
    40, \
    45, \
    14, \
    3, \
    40, \
    9, \
    2, \
    3, \
    40, \
    9, \
    2, \
    3, \
    40, \
    9, \
    2, \
    3, \
    40, \
    45, \
    14, \
    3, \
    40, \
    9, \
    50, \
    3, \
    40, \
    9, \
    50, \
    3, \
    40, \
    9, \
    50, \
    3, \
    40, \
    45, \
    62, \
    3, \
    3, \
    3, \
    3, \
    3, \
    7, \
    7, \
    7, \
    7, \
    11, \
    11, \
    11, \
    11, \
    15, \
    15, \
    15, \
    15, \
    19, \
    19, \
    19, \
    19, \
    23, \
    23, \
    23, \
    23, \
    27, \
    27, \
    27, \
    27, \
    31, \
    31, \
    31, \
    31, \
    35, \
    35, \
    35, \
    35, \
    39, \
    39, \
    39, \
    39, \
    43, \
    43, \
    43, \
    43, \
    47, \
    47, \
    47, \
    47, \
    51, \
    51, \
    51, \
    51, \
    55, \
    55, \
    55, \
    55, \
    59, \
    59, \
    59, \
    59, \
    63, \
    63, \
    63, \
    63

/*------------------------------------------------------------------------------------------*/



    MEA_Status mea_drv_WBRG_Init(MEA_Unit_t       unit_i);
    MEA_Status mea_drv_WBRG_RInit(MEA_Unit_t       unit_i);
    MEA_Status mea_drv_WBRG_Conclude(MEA_Unit_t       unit_i);




#ifdef __cplusplus
 }
#endif 


#endif


