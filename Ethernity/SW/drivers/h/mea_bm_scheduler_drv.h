/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef _MEA_BM_SCHEDULER_DRV_H_
#define _MEA_BM_SCHEDULER_DRV_H_



#include "MEA_platform.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
     extern "C" {
#endif



MEA_Status mea_drv_Init_Scheduler_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_Scheduler_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_Scheduler_Table(MEA_Unit_t unit_i);

MEA_Status mea_drv_Get_Scheduler_db(MEA_Unit_t unit_i, MEA_db_dbt * db_po);



#ifdef __cplusplus
}
#endif

#endif /*  _MEA_BM_SCHEDULER_DRV_H_ */





