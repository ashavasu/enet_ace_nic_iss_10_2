/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_DEVICEINFO_DRV_H
#define MEA_DEVICEINFO_DRV_H



#include "MEA_platform.h"

#include "mea_drv_common.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
extern "C" {
#endif 


MEA_Status mea_Device_Entry_Context_Init (void);
MEA_Status mea_DeviceInfo_Conclude(MEA_Unit_t unit);
MEA_Status mea_DeviceInfo_Init (MEA_Unit_t unit_i);

MEA_Bool MEA_Device_HW_interfaceIndex(MEA_Unit_t unit, MEA_Uint32 interfaceId, MEA_Uint32 *Id_o);

MEA_Bool mea_drv_Get_DeviceInfo_Egress_Filter_exist(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Egress_classifier_exist (MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Egress_parser_exist(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Egress_classifierRang_exist(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Egress_classifier_number_contex(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Egress_classifier_Key_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Egress_classifier_hash_groups(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Egress_classifier_num_of_rangPort(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Egress_classifier_of_ranges(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Egress_classifier_number_hashflow(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

/*Global1 */
MEA_Bool mea_drv_Get_DeviceInfo_Get_Port_new_setting(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Get_mtu_pri_queue_reduce(MEA_Unit_t unit_i,
    MEA_db_HwUnit_t hwUnit_i);


MEA_Bool mea_drv_Get_DeviceInfo_Get_type_DB_DDR(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);



MEA_Bool mea_drv_Get_DeviceInfo_Forwader_supportDSE(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_ADM_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_segment_switch_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);


MEA_Service_t mea_drv_Get_DeviceInfo_NumOfExternalService(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16 mea_drv_Get_DeviceInfo_ExternalService_mode(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);


MEA_Service_t mea_drv_Get_DeviceInfo_NumOfService(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Service_NumOf_groupHash(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Service_NumOf_groupHash_EVC(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Service_EVC_classifier_key_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool   mea_drv_Get_DeviceInfo_Service_EVC_Classifier_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Action_t mea_drv_Get_DeviceInfo_NumOfForworwarAction(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_fwd_LPM_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_fwd_LPM_Ipv4_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_fwd_LPM_Ipv6_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_queueStatistics_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16 mea_drv_Get_DeviceInfo_GetMaxNumOfPort_INGRESS(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_GetMaxLQ(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16 mea_drv_Get_DeviceInfo_GetMaxNumOf_EgressPort(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Action_t mea_drv_Get_DeviceInfo_NumOfActions(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Action_t mea_drv_Get_DeviceInfo_NumOfActions_noACL(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Filter_t mea_drv_Get_DeviceInfo_NumOfACL(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool     mea_drv_Get_DeviceInfo_ACL_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_ACL_hierarchical_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Vxlan_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Filter_t mea_drv_Get_DeviceInfo_NumOfACL_contex(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_instance_fifo_1588(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_NumOf_FWD_ENB_DA(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_ActToBMbus(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_max_numof_descriptor(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_max_numof_DataBuffer(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint32 mea_drv_Get_DeviceInfo_ip_reassembly_session_id_key_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_ip_reassembly_session_id_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_ip_reassembly_numof_session_id(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);


MEA_Uint32 mea_Get_PolicersGroup(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint32 mea_drv_Get_DeviceInfo_CBS_bucket(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_CIR_Token(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_TmId_t mea_drv_Get_DeviceInfo_NumOfPolicers(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_TmId_t mea_drv_Get_DeviceInfo_NumOfPolicersPtr(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Editing_t mea_drv_Get_DeviceInfo_NumOfEHPs(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Editing_t mea_drv_Get_DeviceInfo_EHPs_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Bool  mea_drv_Get_DeviceInfo_MC_Editor_new_mode_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_NumOfMC_EHPs(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_Editor_new_dataBase_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint32  mea_drv_Get_DeviceInfo_Editor_new_dataBase_numofBits(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);



MEA_Bool mea_drv_Get_DeviceInfo_ip_fragmentation_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_bmf_stamp802_1p_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_NumOflm_count(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_bmf_ccm_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_PmId_t mea_drv_Get_DeviceInfo_NumOfPmIds(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_PmWidth(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
ENET_xPermissionId_t mea_drv_Get_DeviceInfo_NumOfXpermission(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Uint32 mea_drv_Get_DeviceInfo_crypto_db_prof_width(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_crypto_db_numOf_profile(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);


MEA_Uint32 mea_drv_Get_DeviceInfo_num_ofMSTP(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_mstp_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_rstp_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_ts_measuremant_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_ECN_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Uint8 mea_drv_Get_DeviceInfo_Xpermission_num_clusterInGroup(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

ENET_QueueId_t mea_drv_Get_DeviceInfo_NumOfClusters(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool      mea_drv_Get_DeviceInfo_IsLimiterSupport(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Limiter_t mea_drv_Get_DeviceInfo_NumOfLimiters(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool      mea_drv_Get_DeviceInfo_IsPortPolicerSupport(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool      mea_drv_Get_DeviceInfo_IsPolicerSupport(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool      mea_drv_Get_DeviceInfo_policer_slowslowSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_chip_number(void);

MEA_Uint16 mea_drv_Get_DeviceInfo_EPC_max_VPLS_Instance(void);
MEA_Uint32 mea_drv_Get_DeviceInfo_EPC_max_VPLS_FLOOD(void);

MEA_Uint16 mea_drv_Get_DeviceInfo_EPC_max_VPLS_OF_USER_ON_GROUP(void);
MEA_Uint16 mea_drv_Get_DeviceInfo_EPC_max_VPLS_OF_CY_GROUP(void);
 

/************************************************************************/
/* HPM                                                                     */
/************************************************************************/

MEA_Bool   mea_drv_Get_DeviceInfo_HPM_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_HPM_newSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_numof_UE_PDN(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_numof_Profile(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint8  mea_drv_Get_DeviceInfo_HPM_numof_Rules(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool   mea_drv_Get_DeviceInfo_HPM_Type_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_numof_Act(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_hpm_BASE(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint32 mea_drv_Get_DeviceInfo_HPM_numof_Mask_Profile(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

/************************************************************************/
/*   SFLOW                                                              */
/************************************************************************/
MEA_Bool mea_drv_Get_DeviceInfo_SFLOW_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_SFLOW_count(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);


MEA_Bool mea_drv_Get_DeviceInfo_rmonVP_enable(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);


/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Bool   mea_drv_Get_DeviceInfo_Is_Ingress_flow_PolicerSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_Ingress_flow_Policer_profile(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_NumOFIngress_flowPolicers(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Bool      mea_drv_Get_DeviceInfo_GetShaperPriQueueType_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool      mea_drv_Get_DeviceInfo_GetShaperClusterType_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool      mea_drv_Get_DeviceInfo_GetShaperPortType_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_GetShaper_xCIR_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint32 mea_drv_Get_DeviceInfo_Get_numOf_blocks_ShaperPriQue_support(void);
MEA_Uint16 mea_drv_Get_DeviceInfo_Get_numOf_Shaper_Profile(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Uint32 mea_drv_Get_DeviceInfo_systemCalc_CLK(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_Get_Max_point_2_multipoint(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);


MEA_LxcpAction_t mea_drv_Get_DeviceInfo_NumOfLxcpAction(MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_Lxcp_num_of_prof(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Uint32 mea_drv_Get_DeviceInfo_GetForwarderLearnActionIdWidth(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_GetForwarderLearnSrcPortValueWidth(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool   mea_drv_Get_DeviceInfo_IsIngressMtuSupport(MEA_Unit_t unit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Isqueue_mc_mqs_Support(MEA_Unit_t unit_i,
                                                       MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_Isqueue_count_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_Isqueue_adminOn_Support(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_IsLAG_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_NumLAG_Profile(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16 mea_drv_Get_DeviceInfo_Num_of_Slice(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16 mea_drv_Get_DeviceInfo_Get_ParsernumofBit(MEA_Unit_t unit_i,
                                                     MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_Support_IPV6(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_extend_default_sid_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool      mea_drv_Get_DeviceInfo_IsServiceRangeSupport(MEA_Unit_t unit_i);
MEA_QueueMtuSupportType_te mea_drv_Get_DeviceInfo_IsMtuPriQueueSupport(MEA_Unit_t unit_i);
MEA_Bool      mea_drv_Get_DeviceInfo_EnhancePmCounterSupport(MEA_Unit_t unit_i);
MEA_Status  mea_drv_Get_DeviceInfo_NumOfPmIdsPerType(MEA_Unit_t       unit_i,
                                                     MEA_db_HwUnit_t  hwUnit_i,
                                                     MEA_pm_type_te   pm_type_i,
                                                     MEA_PmId_t      *numOfPmIds_o);



MEA_WredProfileId_t mea_drv_Get_DeviceInfo_GetMaxWredProfiles(MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_WredProfiles_SUPPORT(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_AcmMode_t mea_drv_Get_DeviceInfo_GetMaxWredACM(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i);

MEA_Bool            mea_drv_Get_DeviceInfo_ClusterWfqSupport (MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_t);
MEA_Bool            mea_drv_Get_DeviceInfo_PriQWfqSupport    (MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_t);
MEA_Bool mea_drv_Get_DeviceInfo_clusterTo_multiport_Support(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit_t);
MEA_Bool    mea_drv_Get_DeviceInfo_CFM_OAM_Support(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit_t);

MEA_Uint32      mea_drv_Get_DeviceInfo_ServiceRange_Num_of_Service_in_port(MEA_Unit_t unit_i);
MEA_Uint32      mea_drv_Get_DeviceInfo_ServiceRange_numofRangeProfile(MEA_Unit_t unit_i);

MEA_Bool    mea_drv_Get_DeviceInfo_CFM_OAM_ME_LvL_FWD_Support(MEA_Unit_t unit_i);

MEA_Status mea_drv_Get_DeviceInfo_ServiceRange_PortSupport(MEA_Unit_t unit_i, 
                                                           MEA_Port_t port ,
                                                           MEA_Bool *support, 
                                                           MEA_Uint16 *indexStart);


MEA_Bool mea_drv_Get_DeviceInfo_IsPacketGeneratorSupport(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_IsPacketGeneratorSupportType2(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i);
MEA_Bool    mea_drv_Get_DeviceInfo_Desc_external(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i);


MEA_Bool mea_drv_Get_DeviceInfo_ingress_count(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_HeaderCompress_support(MEA_Unit_t unit_i,   MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_HeaderDeCompress_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);


MEA_Bool    mea_drv_Get_DeviceInfo_pause_support(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_rx_pause_support(MEA_Unit_t unit_i,
                                        MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_pre_sched_enable(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Desc_my_mac_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_IsPacketAnalyzerSupportType1(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_IsPacketAnalyzerSupportType2(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i);

MEA_PacketGen_t mea_drv_Get_DeviceInfo_PacketGen_num_of_streams(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i);

MEA_PacketGen_t mea_drv_Get_DeviceInfo_PacketGen_num_of_streams_Type1(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i);
MEA_PacketGen_t mea_drv_Get_DeviceInfo_PacketGen_num_of_streams_Type2(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i);


MEA_PacketGen_t mea_drv_Get_DeviceInfo_Analyzer_num_of_streamType1(MEA_Unit_t unit_i,
                                                               MEA_db_HwUnit_t hwUnit_i);

MEA_PacketGen_t mea_drv_Get_DeviceInfo_Analyzer_num_of_streamType2(MEA_Unit_t unit_i,
                                                               MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_IsVspExist(MEA_Unit_t unit_i,
                                           MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_IsCamATMExist(MEA_Unit_t unit_i,
                                              MEA_db_HwUnit_t hwUnit_i);


MEA_Uint32      mea_drv_Get_DeviceInfo_maxClusterPerPort(MEA_Unit_t unit_i);


MEA_Bool  mea_drv_Get_DeviceInfo_flowCos_Mapping_Support(MEA_Unit_t unit_i,
                                                MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_flowMarking_Mapping_Support(MEA_Unit_t unit_i,
                                                MEA_db_HwUnit_t hwUnit_i);

MEA_PacketGen_t mea_drv_Get_DeviceInfo_PacketGen_num_of_profile(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i);

MEA_Bool  mea_drv_Get_DeviceInfo_Swap_machine_Support(MEA_Unit_t unit_i,
                                                MEA_db_HwUnit_t hwUnit_i);

MEA_Bool  mea_drv_Get_DeviceInfo_ingress_port_count_support(MEA_Unit_t unit_i,
                                                            MEA_db_HwUnit_t hwUnit_i);

MEA_Bool  mea_drv_Get_DeviceInfo_ingress_extended_mac_fifo(MEA_Unit_t unit_i,MEA_Port_t port);

MEA_Uint8 mea_drv_Get_DeviceInfo_RX_mac_fifo(MEA_Unit_t unit_i, MEA_Interface_t InterfaceId);
MEA_Uint8 mea_drv_Get_DeviceInfo_TX_mac_fifo(MEA_Unit_t unit_i, MEA_Interface_t InterfaceId);


MEA_Bool  mea_drv_Get_DeviceInfo_bmf_atm_over_pos_support(MEA_Unit_t unit_i,
                                                          MEA_db_HwUnit_t hwUnit_i);

MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_Edit_num_of_group(MEA_Unit_t unit_i,
                                                              MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_Edit_num_of_port(MEA_Unit_t unit_i,
                                                             MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_Edit_num_of_session(MEA_Unit_t unit_i,
                                                                MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_fragment_Edit_Support(MEA_Unit_t unit_i,
                                                       MEA_db_HwUnit_t hwUnit_i);


MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_vsp_num_of_group(MEA_Unit_t unit_i,
                                                             MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_vsp_num_of_port(MEA_Unit_t unit_i,
                                                            MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32  mea_drv_Get_DeviceInfo_fragment_vsp_num_of_session(MEA_Unit_t unit_i,
                                                               MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_fragment_vsp_Support(MEA_Unit_t unit_i,
                                                      MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_fragment_Global_Support(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_Blocking_Class_Support(MEA_Unit_t unit_i,
                                                             MEA_db_HwUnit_t hwUnit_i);

MEA_Bool   mea_drv_Get_DeviceInfo_IsAcmSupport             (MEA_Unit_t unit_i,
                                                            MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_NumOfAcmModes            (MEA_Unit_t unit_i,
                                                            MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32  mea_drv_Get_DeviceInfo_tx_chunk_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16 mea_drv_Get_DeviceInfo_NumOfAcmHiddenProfiles   (MEA_Unit_t unit_i,
                                                            MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16  mea_drv_Get_DeviceInfo_NumOfPolicerProfiles(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i);

MEA_Bool  mea_drv_Get_DeviceInfo_Dual_Latency_Support(MEA_Unit_t unit_i,
                                                      MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_Fragment_Bonding_Support(MEA_Unit_t unit_i,
                                                          MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_CPE_Bonding(MEA_Unit_t unit_i,
                                                   MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_pw_control_Edit_Support(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_forwarder_State_show(MEA_Unit_t unit_i,
                                                     MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_large_BM_ind_address(MEA_Unit_t unit_i,
                                            MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_new_parser_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_afdx_Support(MEA_Unit_t unit_i,
                                             MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_new_parser_reduce(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_ProtocolMapping_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_BFD_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_L2TP_enable(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_GRE_enable(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);


MEA_Bool mea_drv_Get_DeviceInfo_cls_large_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_1588_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_BM_bus_width(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_ingressMacfifo_small(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_ingressPolicer_reduced(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_AlloC_AllocR(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_RmonRx_drop_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_RmonRx_IL_drop_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Protect_1p1_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_fwd_filter_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_MAC_LB_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);



MEA_Bool  mea_drv_Get_DeviceInfo_MII_reduce_PortSupport(MEA_Unit_t unit_i,MEA_Port_t port);

MEA_Bool mea_drv_Get_DeviceInfo_Utopia_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_st_contx_mac_support(MEA_Unit_t unit_i,  MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_numof_SER_TEID(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_fragment_9991_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_SerdesReset_Support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_autoneg_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_GATEWAY_EN_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_standard_bonding_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_standard_bonding_num_of_groupTx(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_standard_bonding_num_of_groupRx(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_IsActionMtuSupport(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Uint16 mea_drv_Get_DeviceInfo_NumOf_AC_MTUs(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool  mea_drv_Get_DeviceInfo_IsNewAllocSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Bool  mea_drv_Get_DeviceInfo_dest_vl_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);
MEA_Bool  mea_drv_Get_DeviceInfo_tdm_act_disable(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16 mea_drv_Get_DeviceInfo_Interface_Id(MEA_Unit_t unit_i);
MEA_Bool mea_drv_Get_DeviceInfo_wbrg_support(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(MEA_Unit_t unit_i, MEA_Uint8  type);


MEA_Status mea_drv_Get_DeviceInfo_Get_Port_Interface_str(mea_drv_portInterface_str_t type, MEA_Uint8 index, char *name_str);
MEA_Status mea_drv_Get_DeviceInfo_GetPort2Interface_Init(MEA_Unit_t unit_i);
MEA_Status mea_drv_Get_DeviceInfo_Show_Port_Interface(void);

MEA_Uint32 mea_drv_get_Ports2InterfaceInfo_Type(MEA_Port_t port, MEA_Uint16 Type);
MEA_Uint32 mea_drv_get_interfaceType_by_portId(MEA_Unit_t unit, MEA_Port_t port);
MEA_Uint32 mea_drv_Get_Interface_Hw_index(MEA_Unit_t       unit,
	MEA_Interface_t  Id);

MEA_Status mea_drv_Get_Interface_ToPortMapp(MEA_Unit_t       unit,
	MEA_Interface_t  Id,
	MEA_OutPorts_Entry_dbt  *OutPorts);

MEA_Bool mea_drv_Get_Interface_ShaperIs_On_interface(MEA_Unit_t       unit,
	MEA_Interface_t  Id);
MEA_Bool mea_drv_Get_Interface2port_isValid(MEA_Unit_t       unit,
	MEA_Interface_t  Id);
MEA_Status mea_drv_Get_Interface2PortsInfo(MEA_Unit_t       unit,
	MEA_Interface_t  Id, MEA_drv_Interface2Port_dbt *entry);

/************************************************************************/
/* IP-Sec                                                               */
/************************************************************************/

MEA_Bool mea_drv_Get_DeviceInfo_Ip_sec_support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_NVGRE_enable(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit);

/************************************************************************/
/* ACL5                                                                 */
/************************************************************************/

MEA_Bool   mea_drv_Get_DeviceInfo_ACL5_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool   mea_drv_Get_DeviceInfo_ACL5_ExternalSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool   mea_drv_Get_DeviceInfo_ACL5_InternalSupport(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_ACL5_Ipmask_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_ACL5_keymask_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_ACL5_rangeProf_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Uint32 mea_drv_Get_DeviceInfo_ACL5_rangeBlock_size(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);






/************************************************************************/
/* TDM                                                                  */
/************************************************************************/
MEA_Bool mea_drv_Get_DeviceInfo_TDM_Support(MEA_Unit_t unit_i,
                                            MEA_db_HwUnit_t hwUnit_i);
MEA_TdmCesId_t mea_drv_Get_DeviceInfo_TDM_num_of_CES(MEA_Unit_t unit_i,
                                                     MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_TDM_mode_Satop(MEA_Unit_t unit_i,
                                               MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_TDM_Interface_X_Support(MEA_Unit_t unit_i,
                                                        MEA_db_HwUnit_t hwUnit_i,MEA_Uint8 interfaceId);
MEA_Uint16 mea_drv_Get_DeviceInfo_TDM_Interface_Type(MEA_Unit_t unit_i,
                                                     MEA_db_HwUnit_t hwUnit_i,MEA_Uint8 interfaceId);

MEA_Uint16 mea_drv_Get_DeviceInfo_TDM_Interface_num0fspe(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i,MEA_Uint8 interfaceId);

MEA_Uint32 mea_drv_Get_DeviceInfo_TDM_Interface_start_ts(MEA_Unit_t unit_i,
                                                         MEA_db_HwUnit_t hwUnit_i,MEA_Uint8 interfaceId);

MEA_Uint16 mea_drv_Get_DeviceInfo_TDM_num_of_Ces(MEA_Unit_t unit_i,
                                                 MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16 mea_drv_Get_DeviceInfo_TDM_ts_size(MEA_Unit_t unit_i,
                                              MEA_db_HwUnit_t hwUnit_i);

/************************************************************************/
/*             Hc_Classifier                                            */
/************************************************************************/
MEA_Bool mea_drv_Get_DeviceInfo_Hc_Classifier_exist(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Bool mea_drv_Get_DeviceInfo_Hc_Classifier_Rang_exist(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Hc_rmon_exist(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16  mea_drv_Get_DeviceInfo_Hc_Classifier_Num_of_hcid(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16  mea_drv_Get_DeviceInfo_Hc_Classifier_Hc_number_of_hash_groups(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);

MEA_Uint16  mea_drv_Get_DeviceInfo_Hc_Classifier_Hc_classifier_key_width(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);


int MEA_DRV_Get_ROM_INFO(void);

/************************************************************************/
/* PTP1588                                                              */
/************************************************************************/

MEA_Bool mea_drv_Get_DeviceInfo_PTP1588_exist(MEA_Unit_t unit_i,MEA_db_HwUnit_t hwUnit_i);


/************************************************************************/
/*   VPLS Accses support                                                */
/************************************************************************/
MEA_Bool mea_drv_Get_DeviceInfo_Isvpls_access_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);
MEA_Bool mea_drv_Get_DeviceInfo_Isvpls_chactristic_Support(MEA_Unit_t unit_i, MEA_db_HwUnit_t hwUnit_i);

#ifdef __cplusplus
 }
#endif 


#endif /* MEA_DEVICEINFO_DRV_H */


