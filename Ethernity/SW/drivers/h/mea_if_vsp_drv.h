/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Ethernity  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_IF_VSP_DRV_H
#define MEA_IF_VSP_DRV_H



#include "MEA_platform.h"

#ifdef __cplusplus
extern "C" {
#endif

MEA_Status mea_drv_Init_IF_VSP(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_IF_VSP(MEA_Unit_t unit_i);


MEA_Status mea_drv_VSP_Fragment_init(MEA_Unit_t unit_i);
MEA_Status mea_drv_VSP_Fragment_Conclude(MEA_Unit_t unit);
MEA_Status mea_drv_VSP_Fragment_ReInit(MEA_Unit_t unit_i);

MEA_Status mea_drv_AC_MTU_prof_Init(MEA_Unit_t       unit_i);
MEA_Status mea_drv_AC_MTU_prof_RInit(MEA_Unit_t       unit_i);
MEA_Status mea_drv_AC_MTU_prof_Conclude(MEA_Unit_t       unit_i);

MEA_Status mea_drv_Target_MTU_prof_Init(MEA_Unit_t       unit_i);
MEA_Status mea_drv_Target_MTU_prof_RInit(MEA_Unit_t       unit_i);
MEA_Status mea_drv_Target_MTU_prof_Conclude(MEA_Unit_t       unit_i);


MEA_Status mea_drv_VP_MSTP_Init(MEA_Unit_t       unit_i);
MEA_Status mea_drv_VP_MSTP_RInit(MEA_Unit_t       unit_i);
MEA_Status mea_drv_VP_MSTP_Conclude(MEA_Unit_t       unit_i);

MEA_Status mea_drv_FWD_ENB_DA_prof_Init(MEA_Unit_t       unit_i);
MEA_Status mea_drv_FWD_ENB_DA_prof_RInit(MEA_Unit_t       unit_i);
MEA_Status mea_drv_FWD_ENB_DA_prof_Conclude(MEA_Unit_t       unit_i);

MEA_Status mea_drv_Create_FWD_ENB_DA_Prof(MEA_Unit_t                    unit_i,
    MEA_Uint16                    *id_i,
    MEA_MacAddr                    DA);
MEA_Status mea_drv_Delete_FWD_ENB_DA_Prof(MEA_Unit_t           unit_i, MEA_Uint16        id_i);



#ifdef __cplusplus
 }
#endif 

#endif /* MEA_IF_VSP_DRV_H */
