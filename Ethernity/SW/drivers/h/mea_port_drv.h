/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_PORT_DRV_H
#define	MEA_PORT_DRV_H



#include "MEA_platform.h"
#include "mea_api.h"

#ifdef __cplusplus
extern "C" {
#endif 
/*-------------------------------- Definitions --------------------------------------*/

    


typedef struct {
    MEA_Uint32 unit_i    :8;
    MEA_Uint32 valid     :1;
    MEA_InterfaceType_t  interpaceType;
    
    MEA_Uint32 Ingressport_start :16;	/*ing */
    MEA_Uint32 Ingressport_end   :16;	/*ing */

    MEA_Uint32 Egressport_start :16;	/*Eg */
    MEA_Uint32 Egressport_end   :16;	/*Eg */

}MEA_map_Interface_dbt;


#define MEA_PORTS_DEVICE_TOPOLOGY_GROUP_MAX_COUNT 12



typedef struct {
	MEA_Uint32 valid:1;
   
	MEA_Uint32 physicalport:16;
	MEA_Uint32 Egressport:16;	/*Eg */
	MEA_Uint32 portType  :16;
	MEA_Uint32 Ingressport:16;	/*ing */
	MEA_Uint32 policer_map:16;   // not need 
	MEA_Uint32 DefaltClaster:16;
    MEA_Uint32 virtualport : 16;
	MEA_Uint32 pad:16;
    MEA_Bool   ingressValid;
    MEA_Bool   EgressValid;
    MEA_Bool   DefaltClasterValid;
    MEA_Bool   virualValid;
    
    MEA_Uint8  port_slice_ID;


    MEA_Bool    PortTo_interfaceId_valid;
    MEA_Uint32  PortTo_interfaceId          :8; /* */
    MEA_Uint32  PortTo_interfaceId_Type     :8;

} MEA_PortsMapping_t;

typedef struct {

	MEA_Uint8 first_physical_port;
	MEA_Uint8 last_physical_port;
	MEA_Uint8 step_physical_port;

	MEA_Uint8 first_portIngress;
	MEA_Uint8 last_portIngress;
	MEA_Uint8 step_portIngress;

	MEA_Uint8 first_portEgress;
	MEA_Uint8 last_portEgress;
	MEA_Uint8 step_portEgress;

	MEA_Uint8 first_Defcluster;
	MEA_Uint8 last_Defcluster;
	MEA_Uint8 step_Defcluster;

	MEA_Uint8 first_PolMap;
	MEA_Uint8 last_PolMap;
	MEA_Uint8 step_PolMap;
	MEA_Port_type_te port_type;

} MEA_Ports_DeviceTopology_group_dbt;

/* ticks is ticks_for_slow_service or ticks_for_fast_service */

#define MEA_PORT_RATE_METER_CIR_EIR_SLOW_GN      (MEA_Platform_Get_PolicerGranularity())    //128000


#define MEA_PORT_RATE_METER_CIR_EIR_FAST_GN     (MEA_POLICER_GLOBAL_GRANULARITY_SLOW_VAL*4) //500000



/*-------------------------------- External variables -------------------------------*/
//extern MEA_Port_t MEA_port_map_array[];


/*-------------------------------- Functions ----------------------------------------*/

MEA_Status mea_drv_Init_PortMap(MEA_Unit_t unit);
MEA_Status mea_drv_Init_MinPktVioSize  (MEA_Unit_t  unit);
MEA_Status mea_drv_Set_MinPktVioSize(MEA_Unit_t unit, MEA_Uint16 value);
MEA_Status mea_drv_Get_MinPktVioSize(MEA_Unit_t unit, MEA_Uint16 * value);
MEA_Status mea_Set_Port_Speed_Rate(MEA_Port_t port, MEA_Uint32 speed);
MEA_Status mea_drv_set_port_ingress_policer(MEA_Unit_t unit,MEA_Port_t port, 
											MEA_IngressPort_Entry_dbt *entry,
											MEA_IngressPort_Entry_dbt *old_entry);
MEA_Status MEA_Low_Port_Mapping_Init(MEA_Unit_t unit_i);
MEA_Port_t mea_get_port_mapArray(MEA_Port_t port);
MEA_Port_t mea_get_port_mapArrayINGRESS(MEA_Port_t port);

MEA_Status MEA_Low_Get_Port_Mapping_By_key(MEA_Unit_t unit,MEA_PortKeyType_t KeyType, MEA_Uint16 id,MEA_PortsMapping_t *portMap);
MEA_Bool mea_is_portUtopia(MEA_Port_t port);
void set_AllPortValid(MEA_Bool val);

MEA_Status MEA_low_configure_Port_Shaper_default(void);

extern MEA_Bool MEA_PMC_Piggy_exist;


MEA_Status mea_drv_LAG_Init(MEA_Unit_t       unit_i);
MEA_Status mea_drv_LAG_ReInit(MEA_Unit_t       unit_i);
MEA_Status mea_drv_LAG_Conclude(MEA_Unit_t       unit_i);
MEA_Bool	mea_drv_LAG_find_by_cluster(MEA_Unit_t       unit_i, MEA_Port_t		cluster, MEA_Uint16       *id_io);

MEA_Status mea_drv_Port_Remove_NVGRE_Header_Init(MEA_Unit_t unit);
MEA_Status mea_drv_Port_Remove_NVGRE_Header_Renit(MEA_Unit_t unit);


MEA_Status mea_Tdn_configPortAdmin(MEA_Port_t port,MEA_Bool eStat);

MEA_Status mea_drv_Init_BFD_SRC_PORT(MEA_Unit_t       unit_i);
MEA_Status mea_drv_RInit_BFD_SRC_PORT(MEA_Unit_t       unit_i);
MEA_Status mea_drv_Conclude_BFD_SRC_PORT(MEA_Unit_t       unit_i);


#ifdef __cplusplus
 }
 #endif 
#endif /* MEA_PORT_DRV_H */
