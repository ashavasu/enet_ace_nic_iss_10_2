/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_MAPPING_DRV_H
#define	MEA_MAPPING_DRV_H



#include "MEA_platform.h"
#include "mea_api.h"

#ifdef __cplusplus
extern "C" {
#endif 
/*-------------------------------- Definitions --------------------------------------*/

/*-------------------------------- External variables -------------------------------*/

/*-------------------------------- Functions ----------------------------------------*/
MEA_Status mea_drv_Init_Mapping_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_Mapping_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_Enable_Mapping(void);
MEA_Status mea_drv_Disable_Mapping(void);
MEA_Status mea_drv_mapping_register_profile ( MEA_Uint32 profile_id);
MEA_Status mea_drv_mapping_unregister_profile ( MEA_Uint32 profile_id);

MEA_Status mea_drv_Protocol_To_Priority_Mapping_Addowner(MEA_Unit_t     unit_i,MEA_Uint16 id_i);
MEA_Status mea_drv_Protocol_To_Priority_Mapping_Deleteowner(MEA_Unit_t     unit_i,MEA_Uint16 id_i);
MEA_Status mea_drv_Protocol_To_Priority_Mapping_Init(MEA_Unit_t       unit_i);
MEA_Status mea_drv_Protocol_To_Priority_Mapping_RInit(MEA_Unit_t       unit_i);
MEA_Status mea_drv_Protocol_To_Priority_Mapping_Conclude(MEA_Unit_t       unit_i);



#ifdef __cplusplus
 }
#endif 

#endif /* MEA_MAPPING_DRV_H */

