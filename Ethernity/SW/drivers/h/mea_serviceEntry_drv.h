/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_serviceEntry_drv.h,v $
#
#------------------------------------------------------------------------------
*/

#ifndef MEA_IF_SERVICE_ENTRY_DRV_H
#define MEA_IF_SERVICE_ENTRY_DRV_H



#include "MEA_platform.h"
#include "ENET_platform_types.h"
#include "mea_if_service_drv.h"
#include "mea_bm_ehp_drv.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
extern "C" {
#endif 

MEA_Status mea_drv_ClassifierKEY_Character_Init(MEA_Unit_t                unit_i);
MEA_Status mea_drv_ClassifierKEY_Character_ReInit(MEA_Unit_t                unit_i);
MEA_Status mea_drv_ClassifierKEY_Character_Conclude(MEA_Unit_t                unit_i);

MEA_Status mea_drv_Init_ServiceEntry_Table     (MEA_Unit_t                unit_i);

MEA_Status mea_drv_ReInit_ServiceEntry_Table   (MEA_Unit_t                unit_i);

MEA_Status mea_drv_Conclude_ServiceEntry_Table (MEA_Unit_t                unit_i);

MEA_Status mea_drv_Get_ServiceEntry_db         (MEA_Unit_t                 unit_i,
								                MEA_db_dbt                *db_po);


MEA_Status mea_ServiceEntry_UpdateHw           (MEA_Unit_t                  unit_i,
									            MEA_Service_t               serviceId,
									            MEA_db_HwUnit_t             hwUnit_i,
									            MEA_db_HwId_t               hwId_i,
								                MEA_Service_Entry_dbt      *new_entry,
									            MEA_Service_Entry_dbt      *old_entry);


MEA_Status mea_drv_Get_ServiceEntry_OutPorts_ByPortState(MEA_Unit_t   unit_i,
                                                         MEA_Uint16   vlan_i,
                                                         MEA_OutPorts_Entry_dbt* outPorts_i,
                                                         MEA_OutPorts_Entry_dbt* outPorts_o);

MEA_Uint32 mea_ServiceEntry_calc_numof_regs(MEA_Unit_t         unit_i,
    MEA_db_HwUnit_t    hwUnit_i);


#ifdef __cplusplus
 }
#endif 

#endif /* MEA_IF_SERVICE_ENTRY_DRV_H */
