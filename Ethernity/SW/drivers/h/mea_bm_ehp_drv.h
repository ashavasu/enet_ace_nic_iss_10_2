/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_bm_ehp_drv.h,v $
#
#------------------------------------------------------------------------------
*/

#ifndef MEA_BM_EHP_DRV_H
#define MEA_BM_EHP_DRV_H



#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
     extern "C" {
#endif






#define MEA_EHP_SWAP_MAC_ENTRY_SW_INDEX (MEA_MAX_NUM_OF_EDIT_ID-1)
#define MEA_EHP_SWAP_MAC_ENTRY_HW_INDEX(hwUnit) (MEA_BM_TBL_TYP_EHP_ETH_LENGTH((hwUnit))-1)

MEA_Status mea_drv_Init_EHP(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_EHP(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_EHP(MEA_Unit_t unit_i);

typedef struct{
	MEA_Uint32 ptmp                     :  1;
	MEA_Uint32 mc_fid_skip              :  1;
	MEA_Uint32 mc_fid_edit              :  3; 
	MEA_Uint32 pad0                     :  27;
} mea_drv_mc_ehp_info_dbt; 

typedef struct {
	MEA_Bool					    valid;
	MEA_db_HwUnit_t                 hwUnit;
	MEA_Uint32						num_of_owners;
	MEA_EgressHeaderProc_Entry_dbt  editing_info;

    MEA_Uint32 regs[8];
} MEA_drv_ehp_dbt;

typedef struct {
	MEA_Bool is_mc;
	union {
		struct {
			MEA_Editing_t id;
		} uni_id;
		struct {
			MEA_Uint32 id;
			
		} mc_id;
	} id;
} mea_drv_ehp_id_dbt;

typedef struct {
	MEA_Uint32 mc_editing_id;

	MEA_Uint32 mc_proto_type : 2;
	MEA_Uint32 mc_llc        : 1;
	MEA_Uint32 mc_pad        : 29;

	MEA_Uint32 num_of_owners ;
} mea_mc_ehp_dbt;

typedef struct {
	mea_mc_ehp_dbt editing_info;
	MEA_Bool       valid;
} MEA_drv_mc_ehp_dbt;

MEA_Status mea_drv_mc_ehp_Update_HwInfo(MEA_Unit_t unit_i,
                                        MEA_Uint32 index,
                                        MEA_drv_mc_ehp_dbt *data);
MEA_Status mea_drv_mc_ehp_Get_Info(MEA_Unit_t unit_i,
    MEA_Uint32 index,
    MEA_drv_mc_ehp_dbt *data);


MEA_Status mea_drv_get_ehp_pool_entry(MEA_Editing_t index,
									  MEA_Bool      *found_o,
									  MEA_drv_ehp_dbt *entry_o);

MEA_Status mea_drv_ehp_add_owner(MEA_Editing_t id);
MEA_Bool mea_drv_is_EHP_id_exist(MEA_Unit_t unit_i,
								 MEA_Editing_t id_i);
MEA_Bool mea_drv_IsEditingIdInRange(MEA_Editing_t editId);

MEA_Status mea_drv_ehp_get_info_db(MEA_Unit_t              unit,
                                   MEA_Editing_t           index,
                                   MEA_drv_ehp_dbt         *entry_o);

MEA_Status mea_drv_ehp_replace_output(MEA_Unit_t              unit_i, 
									  MEA_OutPorts_Entry_dbt *old_output_i,
									  MEA_OutPorts_Entry_dbt *new_output_i,
									  mea_drv_ehp_id_dbt	 *ehp_id_io);
           


MEA_Status mea_drv_ED_Set_EgressHeaderProc_Entry(MEA_Unit_t                     unit,
                                              MEA_EgressHeaderProc_Entry_dbt *entry_i,
											  MEA_Editing_t                  *id_io,
                                              MEA_Bool                        create_i);

/**********/
/* MC EHP */
/**********/
MEA_Status mea_drv_ehp_mc_is_MC(MEA_EgressHeaderProc_Array_Entry_dbt *ehp_array_i,
								MEA_Bool                             *is_mc_ehp_o, 
								MEA_EgressHeaderProc_Entry_dbt       *unicast_ehp_info_o);

MEA_Status mea_drv_ehp_set(MEA_Unit_t                            unit_i,
						   MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry_i, 
						   mea_drv_ehp_id_dbt                   *ehp_id_io) ;

MEA_Status mea_drv_ehp_info_check(MEA_EgressHeaderProc_Array_Entry_dbt *EHP_array_i, 
								MEA_OutPorts_Entry_dbt               *OutPorts_Entry);

MEA_Status mea_drv_ehp_delete(MEA_Unit_t              unit_i,
							  mea_drv_ehp_id_dbt      *ehp_id,
							  MEA_OutPorts_Entry_dbt *output_info);

MEA_Status mea_drv_ehp_mc_get_info( MEA_Unit_t                            unit_i,
								    MEA_Uint16                             mc_id_i,
								    MEA_OutPorts_Entry_dbt               *OutPorts_Entry_i,
								    MEA_EgressHeaderProc_Array_Entry_dbt *EHP_Entry_o );


MEA_Status mea_drv_debug_mc_ehp_show_all(MEA_Uint32 index, MEA_Bool *found, 
                                         MEA_drv_mc_ehp_dbt *entry_o);

typedef struct {

    MEA_MacAddr da;
    MEA_Uint16 pad;

    MEA_Uint32 valid         : 1;
    MEA_Uint32 hwUnit        : 2;
    MEA_Uint32 num_of_owners : 29;


} MEA_drv_ehp_da_dbt;
			
typedef struct {

    MEA_Uint32 sa_lss; //only16bit 
  
    
    MEA_Uint32 valid : 1;
    MEA_Uint32 hwUnit : 2;
    MEA_Uint32 num_of_owners : 29;

} MEA_drv_ehp_sa_lss_dbt;

typedef struct {
	MEA_Editing_t profile_stamping  ;
	//MEA_Editing_t profile_sa_dss    ;
    MEA_Editing_t martini_command   ;// 2bit is not profile
    MEA_Editing_t profile_sa_mss    ;
    MEA_Editing_t profile_ether_type;

	MEA_Uint32 valid              : 1;
	MEA_Uint32 hwUnit             : 2;
	MEA_Uint32 num_of_owners      : 29;
} MEA_drv_ehp_profileId_dbt;

typedef struct {
	MEA_EgressHeaderProc_EthStampingInfo_dbt data;
	MEA_Uint32 valid         : 1;
	MEA_Uint32 hwUnit        : 2;
	MEA_Uint32 num_of_owners : 29;
} MEA_drv_ehp_profile_stamping_dbt;



typedef struct {
	MEA_Uint32 sa_mss         ;

	MEA_Uint32 valid         :  1;
	MEA_Uint32 hwUnit        :  2;
    MEA_Uint32 num_of_owners :  29;
} MEA_drv_ehp_profile_sa_mss_dbt;

typedef struct {
	MEA_Uint32 ether_type    ; /*only16bit*/
	MEA_Uint32 valid         : 1;
	MEA_Uint32 hwUnit        :  2;
    MEA_Uint32 num_of_owners : 29;
} MEA_drv_ehp_profile_ether_type_dbt;

typedef struct {
	MEA_Editing_t profile2_lm        ;

	
    MEA_Uint32 Command_dasa              : 3;
    MEA_Uint32 Command_cfm               : 3;
    MEA_Uint32 Command_dscp_stamp_value  : 6;
    MEA_Uint32 Command_dscp_stamp_enable : 1;
    MEA_Uint32 ETH_CS_packet_is_valid    : 1;
    MEA_Uint32 calcIp_checksum           : 1;
    MEA_Uint32 sw_calcIp_checksum_valid  : 1;
    MEA_Uint32 sw_calcIp_checksum_force  : 1;
    MEA_Uint32 pad                        :15;

    MEA_Uint32 valid         : 1;
    MEA_Uint32 hwUnit        : 2;
    MEA_Uint32 num_of_owners :29;

} MEA_drv_ehp_profile2Id_dbt;

typedef struct {
	MEA_Uint32 valid         :  1;
	MEA_Uint32 hwUnit        :  2;
	MEA_Uint32 num_of_owners : 29;
} MEA_drv_ehp_profile2_lm_dbt;

typedef struct {
    MEA_Uint32 session_num    : 16;
    MEA_Uint32 extract_enable : 1;
    MEA_Uint32 enable         : 1;
    MEA_Uint32 pad            : 14;

    MEA_Uint32 valid         : 1;
    MEA_Uint32 hwUnit        : 2;
    MEA_Uint32 num_of_owners :29;
    MEA_Editing_t editId;
} MEA_drv_ehp_fragmentSession_dbt;


typedef struct {
    MEA_EgressHeaderProc_pw_control_dbt pw_control;
    MEA_Uint32 valid                    : 1;
    MEA_Uint32 hwUnit                   : 2;
    MEA_Uint32 num_of_owners            : 29;
    MEA_Editing_t editId;
} MEA_drv_ehp_pw_control_dbt;

typedef struct {
    MEA_EgressHeaderProc_mpls_label_dbt  mpls_label;
    MEA_Uint32 valid                    : 1;
    MEA_Uint32 hwUnit                   : 2;
    MEA_Uint32 num_of_owners            :29;
    MEA_Editing_t editId;
}MEA_drv_ehp_mpls_label_dbt;


MEA_Status mea_drv_Get_EHP_profile_stamping  (MEA_Unit_t                        unit_i,
                                              MEA_Editing_t                     id_i,
                                              MEA_drv_ehp_profile_stamping_dbt *entry_po);

MEA_Status mea_drv_Get_EHP_profile_sa_mss    (MEA_Unit_t                        unit_i,
                                              MEA_Editing_t                     id_i,
                                              MEA_drv_ehp_profile_sa_mss_dbt *entry_po);
MEA_Status mea_drv_Get_EHP_profile_ether_type(MEA_Unit_t                        unit_i,
                                              MEA_Editing_t                     id_i,
                                              MEA_drv_ehp_profile_ether_type_dbt *entry_po);
MEA_Status mea_drv_Get_EHP_profileId         (MEA_Unit_t                 unit_i,
                                              MEA_Editing_t              id_i,
                                              MEA_drv_ehp_profileId_dbt *entry_po);
MEA_Status mea_drv_Get_EHP_da            (MEA_Unit_t                 unit_i,
                                              MEA_Editing_t              id_i,
                                              MEA_drv_ehp_da_dbt    *entry_po);
MEA_Status mea_drv_Get_EHP_sa_lss            (MEA_Unit_t                 unit_i,
                                              MEA_Editing_t              id_i,
                                              MEA_drv_ehp_sa_lss_dbt    *entry_po);

MEA_Status mea_drv_Get_EHP_fragmentSession(MEA_Unit_t                        unit_i,
                                           MEA_Editing_t                     id_i,
                                           MEA_drv_ehp_fragmentSession_dbt   *entry_po);
MEA_Status mea_drv_Get_EHP_pw_control(MEA_Unit_t                        unit_i,
                                           MEA_Editing_t                     id_i,
                                           MEA_drv_ehp_pw_control_dbt   *entry_po);

MEA_Status mea_drv_Get_EHP_mpls_label(MEA_Unit_t                        unit_i,
                                     MEA_Editing_t                     id_i,
                                     MEA_drv_ehp_mpls_label_dbt   *entry_po);




MEA_Status mea_drv_Get_EHP_ETH_db               (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_ATM_db               (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_WBRG_db               (MEA_Unit_t unit_i,
                                                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_da_db            (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_sa_lss_db            (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_profileId_db         (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_profile_stamping_db  (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_profile_sa_dss_db    (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_profile_sa_mss_db    (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_profile_ether_type_db(MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_profile2Id_db        (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_profile2_lm_db       (MEA_Unit_t unit_i,
							   	                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_fragmentSession_db  (MEA_Unit_t unit_i,
                                                 MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_pw_control_db  (MEA_Unit_t unit_i,
                                                MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_EHP_mpls_label_db  (MEA_Unit_t unit_i,
                                           MEA_db_dbt* db_po);


/* this function is not for the user is for driver only */
MEA_Status MEA_API_Delete_EgressHeaderProc_Entry(MEA_Unit_t                     unit,
                                                 MEA_Editing_t                  editId);

MEA_Uint32 mea_drv_ProtocolMachine_get_command(MEA_Uint16 index, MEA_Uint16 Type);
#ifdef __cplusplus
 }
#endif 

#endif /* MEA_BM_EHP_DRV_H */



