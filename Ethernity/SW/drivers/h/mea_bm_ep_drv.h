/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_BM_EGRESS_PORT_DRV_H
#define MEA_BM_EGRESS_PORT_DRV_H


#include "MEA_platform.h"

#ifdef __cplusplus
     extern "C" {
#endif



MEA_Status mea_drv_Init_EgressPort_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_EgressPort_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_TxEnable(MEA_Unit_t unit_i);

MEA_Status mea_EgressPort_Debug_UpdateHwTxEnable(MEA_Port_t  port,
                                                 MEA_Bool    Enable);
MEA_Status mea_EgressPort_Debug_UpdateHwTxFlush(MEA_Port_t  port, MEA_Bool Enable);

MEA_Status mea_EgressPort_Debug_UpdateHw_PauseEnable(MEA_Port_t  port, MEA_Bool Enable);

MEA_Status mea_EgressPort_Debug_PauseStatus_info(MEA_Unit_t unit, MEA_OutPorts_Entry_dbt *out_entry);

MEA_Status mea_drv_Global_Egress_SA_MAC_Init(MEA_Unit_t unit);
MEA_Status mea_drv_Global_Egress_SA_MAC_ReInit(MEA_Unit_t unit);

#define isPortEnable(epEntryPointer) ((epEntryPointer->tx_enable==0) ? 0 : 1)  //(epEntryPointer->flush==0)

#ifdef __cplusplus
 }
#endif 


#endif /* MEA_BM_EGRESS_PORT_DRV_H */
