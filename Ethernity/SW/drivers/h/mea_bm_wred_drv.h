/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


#ifndef MEA_BM_WRED_DRV_H
#define MEA_BM_WRED_DRV_H

#ifdef __cplusplus
 extern "C" {
 #endif 
 #include "mea_api.h"

MEA_Status mea_drv_Init_WRED_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_WRED_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_WRED_Table(MEA_Unit_t unit_i);

MEA_Status mea_drv_AddOwner_WRED_Profile(MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i);

MEA_Status mea_drv_DelOwner_WRED_Profile(MEA_Unit_t                 unit_i,
                                         MEA_WRED_Profile_Key_dbt  *key_i);


#ifdef __cplusplus
 }
#endif 
#endif /* MEA_BM_WRED_DRV_H */
