/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/******************************************************************************
* 	Module Name:		 mea_action_drv.h
*
*   Module Description:
*
*
*  Date of Creation:	 17/09/06
*
*  Author Name:			 Alex Weis.
*******************************************************************************/

#ifndef MEA_ACTION_DRV_H
#define MEA_ACTION_DRV_H



/*---------------------------------------------------------------------------*/
/* Includes                                                                  */
/*---------------------------------------------------------------------------*/
#include "mea_api.h"
#include "mea_bm_ehp_drv.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
extern "C" {
#endif


/*---------------------------------------------------------------------------*/
/* Defines                                                                   */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Typedefs                                                                  */
/*---------------------------------------------------------------------------*/

typedef struct {
        MEA_Uint32    valid : 1;
    
    
        MEA_Editing_t ed_id;
		MEA_TmId_t    tm_id;
		MEA_PmId_t    pm_id;
		MEA_Uint16    mtu_id;

        MEA_Editing_t    mc_fid_edit;
        /************************************************************************/
		MEA_Uint32    cos			:  3;
		MEA_Uint32    cos_force		:  1;
		MEA_Uint32    color         :  2;
	 	MEA_Uint32    color_force   :  1;
		MEA_Uint32    l2_pri        :  3;
		MEA_Uint32    l2_pri_force  :  1;
		MEA_Uint32    ptmp          :  1;
		MEA_Uint32    mc_fid_skip   :  1;
	    
        MEA_Uint32    edit_force    : 1;
        MEA_Uint32    pm_force      : 1;
        MEA_Uint32    tm_force      : 1;

		MEA_Uint32    protocol      :  2;
		MEA_Uint32    llc           :  1;
        
        MEA_Uint32    pm_type       :  1;
        MEA_Uint32    flowCoSMappingProfile_force : 1;
        MEA_Uint32    flowMarkingMappingProfile_force : 1;
        MEA_Uint32    fwd_ingress_TS_type   : 1;
        MEA_Uint32    l2_pri_prof_b5        : 1;
        MEA_Uint32    fragment_enable       : 1;  /*Bonding*/
        MEA_Uint32    lag_bypass            : 1;
        MEA_Uint32    flood_ED              : 1;

		MEA_Uint32    Drop_en                :1;

        MEA_Uint32   overRule_vpValid       : 1;
        MEA_Uint32   overRule_vp_val        :10;

        MEA_Uint32 Fragment_IP             : 1;
        MEA_Uint32 IP_TUNNEL               : 1;

        MEA_Uint32 Defrag_ext_IP           : 1;
        MEA_Uint32 Defrag_int_IP           : 1;
        MEA_Uint32 Defrag_Reassembly_L2_profile   : 3;

        MEA_Uint32 fragment_ext_int          : 1;
        MEA_Uint32 fragment_target_mtu_prof  : 2;
        MEA_Uint32 fwd_win                   : 1;
        MEA_Uint32 sflow_Type                : 2;
        MEA_Uint32 overRule_bfd_sp           : 1;


        /************************************************************************/
#ifdef MEA_TDM_SW_SUPPORT 
        MEA_Uint32 tdm_packet_type                  :1;
        MEA_Uint32 tdm_remove_byte                  :4;
        MEA_Uint32 tdm_remove_line                  :3;
        MEA_Uint32 tdm_ces_type                     :2;  /* 0 -UDP 1 -vlan 2-mpls 3-IP*/
        MEA_Uint32 tdm_used_vlan                    :1;
        MEA_Uint32 tdm_used_rtp                     :1;
#endif        
        MEA_Uint32 wred_prof                        :3;
        MEA_Uint32 set_1588                         :1;
        
        MEA_Uint32 afdx_enable                      :1;
        MEA_Uint32 afdx_Rx_sessionId                :8;
        MEA_Uint32 afdx_A_B                         :1;
        MEA_Uint32 pad2                             :6;


		mea_action_type_te Action_type;


} MEA_Action_HwEntry_dbt;


typedef struct{
    MEA_Bool					    valid;
	MEA_Uint32						num_of_owners;
    MEA_Action_Entry_Data_dbt       data;
    MEA_OutPorts_Entry_dbt          out_ports;
    MEA_Bool                        EHP_MC;
	MEA_Uint32                       MC_ID;
} mea_action_drv_dbt;




/*---------------------------------------------------------------------------*/
/* APIs                                                                      */
/*---------------------------------------------------------------------------*/

MEA_Status mea_Action_GetOutport(MEA_Unit_t unit_i,
    MEA_Action_t actionId_i,
    MEA_OutPorts_Entry_dbt          *out_ports);

// MEA_Status mea_Action_SetType(MEA_Unit_t unit_i,
// 			      mea_action_type_te type_i);

MEA_Status  MEA_DRV_Delete_Action(MEA_Unit_t    unit_i, MEA_Action_t  ActionId_i, mea_action_type_te Action_type);

MEA_Status mea_Action_GetType(MEA_Unit_t unit_i,
			      MEA_Action_t actionId_i,
			      mea_action_type_te * type_o);

MEA_Status mea_Action_GetDirection(MEA_Unit_t unit_i,
				   MEA_Action_t actionId_i,
				   MEA_Uint32 * direction_o);
MEA_Status mea_Calc_ProtocolAndLlc(MEA_Unit_t unit_i,
				   MEA_Port_t srcPort_i,
				   MEA_OutPorts_Entry_dbt * outPorts_pi,
				   MEA_Uint32 * protocol_o,
				   MEA_Bool * llc_o);

MEA_Status mea_Action_UpdateHw(MEA_Unit_t unit_i,
			       MEA_Action_t actionId_i,
			       MEA_Action_HwEntry_dbt * hwEntry_pio);

MEA_Status mea_Action_GetHw(MEA_Unit_t unit_i,
			    MEA_Action_t actionId_i,
			    MEA_Action_HwEntry_dbt * hwEntry_po);

MEA_Status mea_drv_Get_Action_db(MEA_Unit_t unit_i, MEA_db_dbt * db_po);

MEA_Status mea_Action_Init(MEA_Unit_t unit_i);

MEA_Status mea_Action_ReInit(MEA_Unit_t unit_i);

MEA_Status mea_Action_Conclude(MEA_Unit_t unit_i);

MEA_Status mea_Action_Create_ActionToCpu(MEA_Unit_t unit_i);

MEA_Status mea_Action_Delete_ActionToCpu(MEA_Unit_t unit_i);

MEA_Status MEA_API_AddOwner_Action(MEA_Unit_t unit_i,
				   MEA_Action_t actionId_i);

MEA_Status MEA_API_DelOwner_Action(MEA_Unit_t unit_i,
				   MEA_Action_t actionId_i);

MEA_Status MEA_API_GetOwner_Action(MEA_Unit_t unit_i,
				   MEA_Action_t actionId_i,
				   MEA_Uint32 * numOfOwners_o);

MEA_Uint32 mea_action_calc_numof_regs(MEA_Unit_t         unit, MEA_db_HwUnit_t    hwUnit);

MEA_Status mea_drv_action_hw_info_parsing(MEA_Unit_t unit_i,
    MEA_Action_HwEntry_dbt *action_info, void *retVal);

MEA_Status mea_action_Get_hw_info(MEA_Unit_t unit_i, MEA_Action_t actionId_i, MEA_Uint32 *Action_val);


#if 0
MEA_Status MEA_drv_showMC_Editing(MEA_EgressHeaderProc_Array_Entry_dbt *
				  EHP_Entry);
#endif

#ifdef __cplusplus
 }
#endif 

#endif /* MEA_ACTION_DRV_H */

