/*
This file/directory and the information contained in it are
proprietary and confidential to Ethernity Networks Ltd.
No person is allowed to copy, reprint, reproduce or publish
any part of this document, nor disclose its contents to others,
nor make any use of it, nor allow or assist others to make any
use of it - unless by prior written express
authorization of Ethernity  Networks Ltd and then only to the
extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/



#ifndef MEA_ACL5_PROFILES_DRV_H
#define MEA_ACL5_PROFILES_DRV_H


#include "MEA_platform.h"
#include "mea_api.h"

#ifdef __cplusplus
extern "C" {
#endif



	MEA_Status mea_drv_Init_ACL5_Profiles(MEA_Unit_t unit_i);
	MEA_Status mea_drv_ReInit_ACL5_Table(MEA_Unit_t unit_i);
	MEA_Status mea_drv_ACL5_Conclude(MEA_Unit_t  unit_i);

#ifdef __cplusplus
}
#endif 

#endif /* MEA_ACL5_PROFILES_DRV_H */
