/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_FILTER_DRV_H
#define MEA_FILTER_DRV_H

#include "MEA_platform_types.h"

#ifdef __cplusplus
extern "C" {
#endif 

typedef struct {
	MEA_Bool valid;
	MEA_db_HwUnit_t hwUnit;
	MEA_Uint32 num_of_owners;
	

    MEA_ACL_prof_Key_mask_dbt  data;
} MEA_drv_filter_mask_profile_dbt;

typedef struct {
    MEA_Bool valid;

    MEA_Filter_Key_dbt key;
	MEA_Filter_Data_dbt data;
	MEA_OutPorts_Entry_dbt out_ports;
    
    MEA_Bool             force_xper;
    MEA_Filter_t         filter_CtxId;
    MEA_Action_t         filter_action;
    
	ENET_xPermissionId_t xpermission_id;
	
} MEA_Filter_Entry_dbt;

typedef struct {
    MEA_Uint32   valid;

    MEA_Action_Entry_Data_dbt action_params;

    MEA_Uint32  num_of_owners;
} MEA_Filter_Action_dbt;


typedef struct {
    MEA_Uint32   valid            :1;
    MEA_Uint32   HIe_id             : 2; /*0:3*/

    MEA_Uint32   Xpermission_id     : 16;
    MEA_Uint32   force_Xpermission  : 1;
    MEA_Uint32   force_action_en    : 1;  /*Sw*/
    MEA_Uint32   lxcp_win           : 1;
    MEA_Uint32   fwd_enable         : 1;
    MEA_Uint32   fwd_Key            : 4;
    MEA_Uint32   force_fwd_rule     : 1;
    MEA_Uint32   DSE_mask_field_type : 2;
    MEA_Uint32   fwd_Act_en          : 1; /*1-- the the action will be from fwd HWset 0*/
                                          /*0-- the the action will be from ACL HWset 1*/
    MEA_Uint32   pad8                : 8;
    MEA_Action_t action_id;
    


}MEA_Filter_Context_dbt;



#define MEA_FILTER_VAL_DA               0x00000001
#define MEA_FILTER_VAL_SA               0x00000002
#define MEA_FILTER_VAL_INNER_ETHERTYPE  0x00000004
#define MEA_FILTER_VAL_INNER_PRI        0x00000008
#define MEA_FILTER_VAL_INNER_CFI        0x00000010
#define MEA_FILTER_VAL_INNER_VID        0x00000020
#define MEA_FILTER_VAL_OUTER_ETHERTYPE  0x00000040
#define MEA_FILTER_VAL_OUTER_PRI        0x00000080
#define MEA_FILTER_VAL_OUTER_CFI        0x00000100
#define MEA_FILTER_VAL_OUTER_VID        0x00000200
#define MEA_FILTER_VAL_MPLS_LABEL       0x00000400
#define MEA_FILTER_VAL_PPPOE_SESSION_ID 0x00000800
#define MEA_FILTER_VAL_APPL_ETHERTYPE   0x00001000
#define MEA_FILTER_VAL_IP_VERSION       0x00002000
#define MEA_FILTER_VAL_IP_PROTOCOL      0x00004000
#define MEA_FILTER_VAL_L3_PRI           0x00008000
#define MEA_FILTER_VAL_DST_IPV4         0x00010000
#define MEA_FILTER_VAL_SRC_IPV4         0x00020000
#define MEA_FILTER_VAL_DST_PORT_L4      0x00040000
#define MEA_FILTER_VAL_SRC_PORT_L4      0x00080000
#define MEA_FILTER_VAL_PPP_PROTOCOL     0x00100000
#define MEA_FILTER_VAL_DST_IPV6         0x00200000
#define MEA_FILTER_VAL_SRC_IPV6         0x00400000
#define MEA_FILTER_VAL_RESERVED3        0x00800000
#define MEA_FILTER_VAL_TOS              0x01000000
#define MEA_FILTER_VAL_PRECEDENCE       0x02000000
#define MEA_FILTER_VAL_DSCP             0x04000000
#define MEA_FILTER_VAL_TC               0x08000000
#define MEA_FILTER_VAL_RESERVED4        0x10000000
#define MEA_FILTER_VAL_RESERVED5        0x20000000
#define MEA_FILTER_VAL_RESERVED6        0x40000000
#define MEA_FILTER_VAL_RESERVED7        0x80000000




typedef enum {

    /* key type 0 */
    MEA_FILTER_KEY_TYPE_VAL_DA_MAC                  = MEA_FILTER_VAL_DA,

    /* key type 1 */
    MEA_FILTER_KEY_TYPE_VAL_SA_MAC                  = MEA_FILTER_VAL_SA,

    /* key type 2 */
    MEA_FILTER_KEY_TYPE_VAL_DST_IPV4_DST_PORT       = (MEA_FILTER_VAL_DST_IPV4 | 
                                                       MEA_FILTER_VAL_DST_PORT_L4),

    /* key type 3 */
    MEA_FILTER_KEY_TYPE_VAL_SRC_IPV4_DST_PORT       = (MEA_FILTER_VAL_SRC_IPV4 | 
                                                       MEA_FILTER_VAL_DST_PORT_L4),

    /* key type 4 */
    MEA_FILTER_KEY_TYPE_VAL_DST_IPV4_SRC_PORT       = (MEA_FILTER_VAL_DST_IPV4 | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4),

    /* key type 5 */
    MEA_FILTER_KEY_TYPE_VAL_SRC_IPV4_SRC_PORT       = (MEA_FILTER_VAL_SRC_IPV4 | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4),

    /* key type 6 */
    MEA_FILTER_KEY_TYPE_VAL_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG = 
                                                      (MEA_FILTER_VAL_OUTER_ETHERTYPE |
                                                       MEA_FILTER_VAL_INNER_ETHERTYPE |
                                                       MEA_FILTER_VAL_OUTER_PRI       |
                                                       MEA_FILTER_VAL_OUTER_CFI       |
                                                       MEA_FILTER_VAL_OUTER_VID       ),

    /* key type 7 */
    MEA_FILTER_KEY_TYPE_VAL_SRC_IPV4_APPL_ETHERTYPE = (MEA_FILTER_VAL_SRC_IPV4 | 
                                                       MEA_FILTER_VAL_APPL_ETHERTYPE),

    /* key type 8 inner priority */
    MEA_FILTER_KEY_TYPE_VAL_PRI_INNER_VLAN_IP_PROTO_APPL_ETHERTYPE_DST_PORT = 
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_APPL_ETHERTYPE |
                                                       MEA_FILTER_VAL_INNER_PRI      ),

    /* key type 8 outer priority */
    MEA_FILTER_KEY_TYPE_VAL_PRI_OUTER_VLAN_IP_PROTO_APPL_ETHERTYPE_DST_PORT =           
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_APPL_ETHERTYPE |
                                                       MEA_FILTER_VAL_OUTER_PRI      ),
    /* key type 8 TOS */
    MEA_FILTER_KEY_TYPE_VAL_PRI_TOS_IP_PROTO_APPL_ETHERTYPE_DST_PORT=                   
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_APPL_ETHERTYPE |
                                                       MEA_FILTER_VAL_L3_PRI         |
                                                       MEA_FILTER_VAL_TOS            ),

    /* key type 8 Precedence */
    MEA_FILTER_KEY_TYPE_VAL_PRI_PREC_IP_PROTO_APPL_ETHERTYPE_DST_PORT=  
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_APPL_ETHERTYPE |
                                                       MEA_FILTER_VAL_L3_PRI         |
                                                       MEA_FILTER_VAL_PRECEDENCE     ),

    /* key type 8 DSCP */
    MEA_FILTER_KEY_TYPE_VAL_PRI_DSCP_IP_PROTO_APPL_ETHERTYPE_DST_PORT=
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_APPL_ETHERTYPE |
                                                       MEA_FILTER_VAL_L3_PRI         |
                                                       MEA_FILTER_VAL_DSCP           ),

    /* key type 8 TC */
    MEA_FILTER_KEY_TYPE_VAL_PRI_TC_IP_PROTO_APPL_ETHERTYPE_DST_PORT=  
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_APPL_ETHERTYPE |
                                                       MEA_FILTER_VAL_L3_PRI         |
                                                       MEA_FILTER_VAL_TC             ),
    
    
    
    /* key type 9 Inner priority */
    MEA_FILTER_KEY_TYPE_VAL_PRI_INNER_VLAN_IP_PROTO_SRC_PORT_DST_PORT = 
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_INNER_PRI      ),

    /* key type 9 outer priority */
    MEA_FILTER_KEY_TYPE_VAL_PRI_OUTER_VLAN_IP_PROTO_SRC_PORT_DST_PORT =
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_OUTER_PRI      ),

    /* key type 9 TOS */
    MEA_FILTER_KEY_TYPE_VAL_PRI_TOS_IP_PROTO_SRC_PORT_DST_PORT=
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_L3_PRI         |
                                                       MEA_FILTER_VAL_TOS            ),

    /* key type 9 Precedence */
    MEA_FILTER_KEY_TYPE_VAL_PRI_PREC_IP_PROTO_SRC_PORT_DST_PORT= 
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_L3_PRI         |
                                                       MEA_FILTER_VAL_PRECEDENCE     ),

    /* key type 9 DSCP */
    MEA_FILTER_KEY_TYPE_VAL_PRI_DSCP_IP_PROTO_SRC_PORT_DST_PORT= 
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_L3_PRI         |
                                                       MEA_FILTER_VAL_DSCP           ),

    /* key type 9 TC */
    MEA_FILTER_KEY_TYPE_VAL_PRI_TC_IP_PROTO_SRC_PORT_DST_PORT= 
                                                      (MEA_FILTER_VAL_DST_PORT_L4    | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4    | 
                                                       MEA_FILTER_VAL_IP_PROTOCOL    |
                                                       MEA_FILTER_VAL_L3_PRI         |
                                                       MEA_FILTER_VAL_TC             ),

    /* key type 10 */
    MEA_FILTER_KEY_TYPE_VAL_OUTER_VLAN_TAG_INNER_VLAN_TAG_OUTER_ETHERTYPE = 
                                                      (MEA_FILTER_VAL_OUTER_ETHERTYPE |
                                                       MEA_FILTER_VAL_INNER_PRI       |
                                                       MEA_FILTER_VAL_INNER_CFI       |
                                                       MEA_FILTER_VAL_INNER_VID       |
                                                       MEA_FILTER_VAL_OUTER_PRI       |
                                                       MEA_FILTER_VAL_OUTER_CFI       |
                                                       MEA_FILTER_VAL_OUTER_VID       ),

    /* key type 11 */
    MEA_FILTER_KEY_TYPE_VAL_OUTER_VLAN_TAG_DST_PORT_SRC_PORT = 
                                                      (MEA_FILTER_VAL_DST_PORT_L4     | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4     | 
                                                       MEA_FILTER_VAL_OUTER_PRI       |
                                                       MEA_FILTER_VAL_OUTER_CFI       |
                                                       MEA_FILTER_VAL_OUTER_VID       ),

    /* key type 12 */
    MEA_FILTER_KEY_TYPE_VAL_INNER_VLAN_TAG_DST_PORT_SRC_PORT = 
                                                      (MEA_FILTER_VAL_DST_PORT_L4     | 
                                                       MEA_FILTER_VAL_SRC_PORT_L4     | 
                                                       MEA_FILTER_VAL_INNER_PRI       |
                                                       MEA_FILTER_VAL_INNER_CFI       |
                                                       MEA_FILTER_VAL_INNER_VID       ),

    /* key type 13 */
    MEA_FILTER_KEY_TYPE_VAL_PPPOE_SESSION_ID_PPP_PROTO_SA_MAC_LSB = 
                                                      (MEA_FILTER_VAL_PPPOE_SESSION_ID| 
                                                       MEA_FILTER_VAL_PPP_PROTOCOL    | 
                                                       MEA_FILTER_VAL_SA              ),

    /* key type 14 */
    MEA_FILTER_KEY_TYPE_VAL_OUTER_ETERTYPE_APPL_ETHERTYPE_OUTER_VLAN_TAG=
                                                      (MEA_FILTER_VAL_OUTER_ETHERTYPE |
                                                       MEA_FILTER_VAL_APPL_ETHERTYPE  |
                                                       MEA_FILTER_VAL_OUTER_PRI       |
                                                       MEA_FILTER_VAL_OUTER_CFI       |
                                                       MEA_FILTER_VAL_OUTER_VID       ),

    /* key type 15 */
    MEA_FILTER_KEY_TYPE_VAL_DA_MAC_OUTER_VLAN_TAG_DST_IPV4=
                                                      (MEA_FILTER_VAL_DST_IPV4        |
                                                       MEA_FILTER_VAL_OUTER_PRI       |
                                                       MEA_FILTER_VAL_OUTER_CFI       |
                                                       MEA_FILTER_VAL_OUTER_VID       |
                                                       MEA_FILTER_VAL_DA              ),
   /* key type 15 */
   MEA_FILTER_KEY_TYPE_VAL_DA_MAC_NOT_VALID_OUTER_VLAN_TAG_DST_IPV4 = 
                                                               (MEA_FILTER_VAL_DST_IPV4        |
                                                               MEA_FILTER_VAL_OUTER_PRI       |
                                                               MEA_FILTER_VAL_OUTER_CFI       |
                                                               MEA_FILTER_VAL_OUTER_VID  ),



    /* key type 22 */
    MEA_FILTER_KEY_TYPE_VAL_DST_IPV6_DST_PORT = (MEA_FILTER_VAL_DST_IPV6 |
                                                 MEA_FILTER_VAL_DST_PORT_L4),

    /* key type 23 */
    MEA_FILTER_KEY_TYPE_VAL_SRC_IPV6_DST_PORT = (MEA_FILTER_VAL_SRC_IPV6 |
                                                 MEA_FILTER_VAL_DST_PORT_L4),

    /* key type 24 */
    MEA_FILTER_KEY_TYPE_VAL_DST_IPV6_SRC_PORT = (MEA_FILTER_VAL_DST_IPV6 |
                                                 MEA_FILTER_VAL_SRC_PORT_L4),

    /* key type 25 */
    MEA_FILTER_KEY_TYPE_VAL_SRC_IPV6_SRC_PORT = (MEA_FILTER_VAL_SRC_IPV6 |
                                                 MEA_FILTER_VAL_SRC_PORT_L4),
   /* key type 27 */
  MEA_FILTER_KEY_TYPE_VAL_SRC_IPV6_APPL_ETHERTYPE = (MEA_FILTER_VAL_SRC_IPV6 |
                                                     MEA_FILTER_VAL_APPL_ETHERTYPE)





        

 

} mea_drv_Filter_Key_Type_Val_te;

#define MEA_FILTER_PRI_TYPE_MASK_L2_PRI_UNTAG  0x00000000 
#define MEA_FILTER_PRI_TYPE_MASK_L2_PRI_INNER  ((MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ? (0x00001000) : (0x00000000))  
#define MEA_FILTER_PRI_TYPE_MASK_L2_PRI_OUTER  ((MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ? (0x00000800) : (0x00000000))
#define MEA_FILTER_PRI_TYPE_MASK_L3_TOS        ((MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ? (0x0000a000) : (0x00008000)) 
#define MEA_FILTER_PRI_TYPE_MASK_L3_PRECEDENCE ((MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ? (0x00005800) : (0x00004000))  
#define MEA_FILTER_PRI_TYPE_MASK_L3_DSCP       ((MEA_CLS_LARGE_SUPPORT == MEA_FALSE) ? (0x0000c000) : (0x0000c000))  


MEA_Status mea_drv_Init_Filter_Table            (MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_Filter_Table            (MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_Filter_Table        (MEA_Unit_t unit_i);

MEA_Status mea_drv_set_Filter_Mask_Prof         (MEA_Unit_t           unit_i,
                                                 MEA_ACL_prof_Key_mask_dbt *entry_pi,
										         MEA_FilterMask_t    *id_o);

MEA_Status mea_drv_filter_mask_prof_delete_owner(MEA_Unit_t           unit_t,
												 MEA_FilterMask_t     id_i);

MEA_Status mea_drv_dbg_filter_mask_prof_get     (MEA_Unit_t                       unit_i,
											     MEA_FilterMask_t                 id_i,
											     MEA_Bool                        *found_o,
	                                             MEA_drv_filter_mask_profile_dbt *entry_po);

#if 0
MEA_Status mea_drv_Get_Filter_db           (MEA_Unit_t  unit_i,
								            MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_Filter_Ctx_db       (MEA_Unit_t  unit_i,
								            MEA_db_dbt* db_po);


MEA_Status mea_drv_Get_Filter_Mask_db      (MEA_Unit_t  unit_i,
								            MEA_db_dbt* db_po);
#endif
                                          

MEA_Filter_Key_Type_te mea_filter_get_key_type(MEA_Filter_Key_dbt *Filter_Key);

MEA_Int8* mea_drv_acl_Get_Filter_Key_Type_STR(MEA_Filter_Key_Type_te key_type);
MEA_Int8* MEA_drv_acl_Get_Filter_Action_STR(MEA_Filter_Action_te action);


#ifdef __cplusplus
 }
#endif 


#endif /* MEA_FILTER_DRV_H */

