/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_IF_PARSER_DRV_H
#define MEA_IF_PARSER_DRV_H

#include "mea_api.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
extern "C" {
#endif 


#define MEA_PARSET_pri_rule                             3
#define MEA_PARSET_DSA_Type_enable                      1
#define MEA_PARSET_L4_SRC_DST                           1
#define MEA_PARSET_default_sid                          14
#define MEA_PARSET_default_sid_act                      2
#define MEA_PARSET_logical_src_port                     7
#define MEA_PARSET_logical_src_port_replace             1
#define MEA_PARSET_mapping_profile                      1
#define MEA_PARSET_filter_key_interface_type            1  // NVGRE remove tunnel 
#define MEA_PARSET_color_valid                          1

#define MEA_PARSET_da_fwrd_l2                           1       /*ENNI*/
#define MEA_PARSET_or_mask_net2_0_15                    16
#define MEA_PARSET_or_mask_net2_16_23                   8
#define MEA_PARSET_or_mask_net_tag_20_23                4
#define MEA_PARSET_or_mask_pri_3_7                      5

#define MEA_PARSET_or_mask_L2_Type                      5
#define MEA_PARSET_transparent_mode                     1
#define MEA_PARSET_protocol2pri_mapping_enable          1
#define MEA_PARSET_or_mask_pri                          3
#define MEA_PARSET_or_mask_src_port                     7
#define MEA_PARSET_or_mask_net_tag_0_13                 14
#define MEA_PARSET_or_mask_net_tag_14_19                6
#define MEA_PARSET_evc_external_internal                1
#define MEA_PARSET_default_net_tag_value                12
#define MEA_PARSET_afdx_enable                          1  
#define MEA_PARSET_default_pri                          3
#define MEA_PARSET_pri_Ip_type                          2
#define MEA_PARSET_my_mac_0                             8
#define MEA_PARSET_my_mac_1                             8
#define MEA_PARSET_my_mac_2                             8
#define MEA_PARSET_my_mac_3                             8
#define MEA_PARSET_my_mac_4                             8
#define MEA_PARSET_my_mac_5                             8
#define MEA_PARSET_vlanrange_Prof_Id                    6
#define MEA_PARSET_vlanrange_Prof_Valid                 1
#define MEA_PARSET_LAG_dis_MASK                         4  /*   bit3 mpls bit2-l4 bit1-l3  bit0-l2 */ /*default need to be 0xf */
#define MEA_PARSET_def_sid_port_type                    2
#define MEA_PARSET_l2type_consolidate                   1

#define MEA_PARSET_or_mask_src_port_3BIT                3
#define MEA_PARSET_logical_src_port_3BIT                3

#define MEA_PARSET_globalMyMac_enable                   4


MEA_Status mea_drv_Init_Parser_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_Parser_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_Parser_Table(MEA_Unit_t unit_i);

MEA_Status mea_drv_Init_Global_parser(MEA_Unit_t unit_i);
MEA_Status mea_drv_Renit_Global_parser(MEA_Unit_t            unit);
MEA_Status mea_drv_Conclude_Global_parser(MEA_Unit_t            unit);

MEA_Bool mea_drv_Get_Parsing_L2_Key_Support(MEA_Unit_t unit,MEA_PARSING_L2_KEY_t        type);
MEA_Status mea_drv_update_Parsing_L2_Key_update(MEA_Unit_t unit);

MEA_Status mea_get_Last_if_to_bm_Info(MEA_Unit_t unit, MEA_Uint32 *get_info, mea_if_to_bm_dbt *entry_o);
MEA_Status mea_get_Last_Ip_reassembly_last_error(MEA_Unit_t unit, MEA_Uint32 *get_info, mea_ipfrag_reassembly_dbt *entry_o);
MEA_Status mea_get_Last_Reassembly_classifier_event(MEA_Unit_t unit, MEA_Uint8 type, mea_ip_reassembly_lastEvent_dbt *entry_o);

#ifdef __cplusplus
 }
#endif 

#endif /* MEA_IF_PARSER_DRV_H */
