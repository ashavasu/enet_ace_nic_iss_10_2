/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_lxcp_drv.h,v $
#
#------------------------------------------------------------------------------
*/

#ifndef MEA_INTERFACE_DRV_H
#define MEA_INTERFACE_DRV_H



#include "mea_api.h"


#ifdef __cplusplus
extern "C" {
#endif 

MEA_Bool mea_drv_Interface_Valid_TypeCorrect(MEA_Unit_t Unit,MEA_Interface_t Id,MEA_InterfaceType_t InterfaceType ,MEA_Bool silent);


    MEA_Status mea_drv_Interface_Conclude(MEA_Unit_t Unit);
    MEA_Status mea_drv_Interface_Init(MEA_Unit_t Unit);
    MEA_Status mea_drv_Interface_Reinit(MEA_Unit_t Unit);

    MEA_Status  mea_DRV_Serdes_Link_check(MEA_Unit_t unit);
    MEA_Status mea_drv_Get_Interface_Link_Status(MEA_Unit_t unit ,MEA_OutPorts_Entry_dbt *interfaceStatus);
    MEA_Status mea_drv_SFP_PLUS_Flash_UpdateHw_SetID(MEA_Unit_t         unit_i, MEA_Interface_t               Id);
  
#ifdef __cplusplus
 }
#endif 

#endif /* MEA_LXCP_DRV_H */

