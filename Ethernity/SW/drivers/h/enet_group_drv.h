/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/******************************************************************************
* 	Module Name:		 enet_group_drv.h	   									     
*																					 
*   Module Description:	     
*                            
*                         
*	                     
*                        
*                        
*																					 
*  Date of Creation:	 14/09/06													 
*																					 
*  Author Name:			 Alex Weis.													 
*******************************************************************************/
#ifndef _ENET_GROUP_DRV_H_
#define _ENET_GROUP_DRV_H_

#include "MEA_platform.h"
#ifdef __cplusplus
extern "C" {
#endif


ENET_Status enet_Init_Group(ENET_Unit_t unit_i);
ENET_Status enet_Conclude_Group(ENET_Unit_t unit_i);

/* Delete all elements in specifc group */
ENET_Status enet_Conclude_GroupElement(ENET_Unit_t unit_i,
				       ENET_GroupId_t group_id_i);



#ifdef __cplusplus
}
#endif

#endif				/* _ENET_GROUP_DRV_H_ */
