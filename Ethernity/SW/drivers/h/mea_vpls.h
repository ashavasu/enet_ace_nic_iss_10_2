/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Ethernity  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_IF_VPLS_DRV_H
#define MEA_IF_VPLS_DRV_H



#include "MEA_platform.h"

#ifdef __cplusplus
extern "C" {
#endif

    MEA_Status mea_drv_VPLS_Init(MEA_Unit_t       unit_i);
    MEA_Status mea_drv_VPLS_RInit(MEA_Unit_t       unit_i);
    MEA_Status mea_drv_VPLS_Conclude(MEA_Unit_t       unit_i);

    MEA_Status mea_drv_vplsi_Get_outport(MEA_Uint16 vpls_Ins_Id, MEA_OutPorts_Entry_dbt     *OutPorts);
    MEA_Status MEA_drv_VPLS_Service(MEA_Uint8 vpls_Ins_Id, MEA_Service_t serviceId, MEA_Port_t src_port);

#ifdef __cplusplus
 }
#endif 

#endif /* MEA_IF_VSP_DRV_H */
