/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


#ifndef MEA_PREATM_PARSER_DRV_H
#define MEA_PREATM_PARSER_DRV_H



#include "mea_api.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
extern "C" {
#endif 


MEA_Status mea_drv_PreATM_parser_Init(MEA_Unit_t       unit_i);
MEA_Status mea_drv_PreATM_parser_RInit(MEA_Unit_t       unit_i);
MEA_Status mea_drv_PreATM_parser_Conclude(MEA_Unit_t       unit_i);


MEA_Bool mea_drv_PreATM_parser_get_internal_vsp_number(MEA_Unit_t       unit_i,
                                                       MEA_Port_t       port,
                                                       MEA_Uint8        vsp_type,
                                                       MEA_Uint32       *vsp_id_o);


#ifdef __cplusplus
 }
#endif 

#endif /* MEA_TDM_CES_H */
