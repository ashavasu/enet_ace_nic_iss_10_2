/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_LIMITER_DRV_H
#define MEA_LIMITER_DRV_H

#include "MEA_platform.h"

#ifdef __cplusplus
    extern "C" {
#endif 

MEA_Status mea_Init_limiter(MEA_Unit_t unit_i);
MEA_Status mea_ReInit_limiter(MEA_Unit_t unit_i);
MEA_Status mea_Conclude_limiter(MEA_Unit_t unit_i);
MEA_Status mea_Limiter_static_entry(MEA_Unit_t unit_i,
				    MEA_Limiter_t limiterId,
				    MEA_Bool isAdd);
MEA_Status mea_drv_limiter_clear_counters(MEA_Unit_t unit);
/************************************************************************/
/* ADM Function                                                         */
/************************************************************************/

MEA_Status mea_drv_ADM_Init(MEA_Unit_t unit);
MEA_Status mea_drv_ADM_ReInit(MEA_Unit_t unit);
MEA_Status mea_drv_ADM_Conclude(MEA_Unit_t unit);

MEA_Status mea_drv_ADM_Cluster_To_port(MEA_Unit_t unit,
    MEA_Port_t port,
    MEA_Uint8 group,
    MEA_Uint16 clusterId , /* Internal */
    MEA_Bool enable);

MEA_Status mea_drv_ADM_port_ToTrunk(MEA_Unit_t unit,
    MEA_Port_t port,
    MEA_Port_t trunkId,
    MEA_Bool enable);

/************************************************************************/
/* mea_drv_ISOLATE_Init                                                 */
/************************************************************************/
 MEA_Bool mea_drv_ISOLATE_Init(MEA_Unit_t         unit_i);
 MEA_Bool mea_drv_ISOLATE_RInit(MEA_Unit_t        unit_i);
 MEA_Bool mea_drv_ISOLATE_Conclude(MEA_Unit_t     unit_i);


#ifdef __cplusplus
 }
#endif

#endif /* MEA_LIMITER_DRV_H */
