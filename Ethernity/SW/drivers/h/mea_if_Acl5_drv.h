/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_IF_ACL5_DRV_H
#define MEA_IF_ACL5_DRV_H

#include "MEA_platform.h"


#ifdef __cplusplus
extern "C" {
#endif 

#define MEA_ACL5_MAX_ENTRY_SW   (256*1024)
#define MEA_ACL5_MAX_IPV6_REG_LEN	0xffffffff
#define MEA_ACL5_MAX_MAC_REG_LEN	0xff

#define MEA_IF_ACL5_FOUR_BYTES_WIDTH	32
#define MEA_IF_ACL5_TAG3_29_BITS_WIDTH	29
#define MEA_IF_ACL5_TAG1_WIDTH			20
#define MEA_IF_ACL5_FLOW_LABEL_WIDTH	20
#define MEA_IF_ACL5_TAG2_WIDTH			24
#define MEA_IF_ACL5_TWO_BYTES_WIDTH		16
#define MEA_IF_ACL5_SID_WIDTH			12
#define MEA_IF_ACL5_IP_MSK_PFILE_WIDTH	9
#define MEA_IF_ACL5_ONE_BYTE_WIDTH		8
#define MEA_IF_ACL5_DSCP_WIDTH			6
#define MEA_IF_ACL5_RNG_RSLT_WIDTH		6
#define MEA_IF_ACL5_KEY_TYPE_WIDTH		4
#define MEA_IF_ACL5_TAG1_P_WIDTH		3
#define MEA_IF_ACL5_IP_FLAGS_WIDTH		2
#define MEA_IF_ACL5_UMB_WIDTH			2
#define MEA_IF_ACL5_PRIORITY_WIDTH		2
#define MEA_IF_ACL5_ONE_BIT_WIDTH		1


#define MEA_IF_ACL5_KEY_TYPE_START  124
MEA_Status mea_drv_Init_IF_ACL5(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_IF_ACL5(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_ACL5(MEA_Unit_t unit_i);
MEA_Uint32 mea_drv_acl5_get_hashAdress(MEA_ACL5Id_t Id_i);


#ifdef __cplusplus
 }
#endif 

#endif /* MEA_IF_ACL5_DRV_H */
