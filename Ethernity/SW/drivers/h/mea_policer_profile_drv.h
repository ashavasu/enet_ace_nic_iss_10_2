/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_policer_profile_drv.h,v $
#
#------------------------------------------------------------------------------
*/

#ifndef MEA_POLICER_PROFILE_DRV_H
#define	MEA_POLICER_PROFILE_DRV_H

#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
extern "C" {
#endif 
/***************************************************************/
/*                                                             */
/*            Policer Profile Software Database Structures     */
/*                                                             */
/***************************************************************/

typedef struct{
    MEA_Uint32 CBS;
    MEA_Uint32 EBS;
    MEA_Uint32  Lmax;
}MEA_Policer_CBS_EBS_dbt;

typedef struct{
    MEA_Bool                 valid;
    MEA_Uint32               num_of_owners;
    MEA_Policer_Entry_dbt    rateMeter;
    MEA_Bool                 fast_slow;
    MEA_Policer_CBS_EBS_dbt  Calculat;
	MEA_db_HwUnit_t          hwUnit;
    MEA_db_HwId_t            hwProfId;
}MEA_Policer_Prof_dbt;

typedef struct {
   MEA_Policer_Prof_dbt   *policer_profile_table; 
} MEA_Policer_AcmProf_dbt;

/***************************************************************/
/*                                                             */
/*            Policer Profile Hardware Database Structures     */
/*                                                             */
/***************************************************************/

typedef struct {
    MEA_Uint16                         valid;
    MEA_Policer_prof_t                 policer_profile_id; /* sw id */
    MEA_Uint32                        *regs;
} MEA_Policer_Prof_hw_dbt;


typedef struct {
    MEA_Uint16                          used_profEntries;
    MEA_Uint16                          pad;
    MEA_Policer_Prof_hw_dbt            *profTable;
} MEA_Policer_Prof_acm_hw_dbt;

typedef struct {
    MEA_Uint16                          num_of_hiddenEntries;
    MEA_Uint16                          num_of_acmEntries;
    MEA_Uint16                          num_of_profEntries;
    MEA_Uint16                          num_of_regs;
    MEA_Policer_Prof_acm_hw_dbt        *acmTable;
} MEA_Policer_Prof_hwUnit_dbt;


/***************************************************************/
/*                                                             */
/*            Policer Profile APIs                             */
/*                                                             */
/***************************************************************/


MEA_Status mea_drv_Policer_Profile_Create (MEA_Unit_t                unit_i,
                                           MEA_IngressPort_Proto_t   port_proto_prof_i,
                                           MEA_Policer_Entry_dbt    *entry_i,
                                           MEA_AcmMode_t     ACM_Mode_i,
                                           MEA_Policer_prof_t       *id_io);

MEA_Status mea_drv_Policer_Profile_Delete (MEA_Unit_t                unit_i,
                                           MEA_AcmMode_t     ACM_Mode_i,
                                           MEA_Policer_prof_t        id_i);

MEA_Status mea_drv_Policer_Profile_Set    (MEA_Unit_t                unit_i,
                                           MEA_IngressPort_Proto_t   port_proto_prof_i,
                                           MEA_AcmMode_t     ACM_Mode_i,
                                           MEA_Policer_prof_t        id_i,
                                           MEA_Policer_Entry_dbt    *entry_i);

MEA_Status mea_drv_Policer_Profile_Get    (MEA_Unit_t                 unit_i,
                                           MEA_AcmMode_t      ACM_Mode_i,
                                           MEA_Policer_prof_t         id_i,
                                           MEA_Policer_Prof_dbt      *entry_o );

MEA_Status mea_drv_Policer_Profile_IsExist(MEA_Unit_t                 unit_i,
                                           MEA_AcmMode_t      ACM_Mode_i,
                                           MEA_Policer_prof_t         id_i,
                                           MEA_Bool                  *exist_o);

MEA_Status mea_drv_Policer_Profile_Check  (MEA_Unit_t                 unit_i,
                                           MEA_Policer_Entry_dbt     *entry_i,
                                           MEA_Bool                  *fast_lsow);

MEA_Status mea_drv_Policer_Profile_GetHwProfId (MEA_Unit_t                unit_i,
                                                MEA_db_HwUnit_t           hwUnit_i,
                                                MEA_Policer_prof_t        id_i,
                                                MEA_db_HwId_t            *hwId_o);

MEA_Policer_Prof_hwUnit_dbt *mea_drv_Policer_Profile_GetHwDb(void);

                   
MEA_Policer_prof_t  mea_drv_Policer_Profile_GetDisableProfId (MEA_db_HwUnit_t hwUnit_i);

MEA_Status mea_drv_Policer_Profile_Conclude(MEA_Unit_t unit_i);

MEA_Status mea_drv_Policer_Profile_Init    (MEA_Unit_t unit_i);

MEA_Status mea_drv_Policer_Profile_ReInit  (MEA_Unit_t unit_i);


MEA_Status mea_drv_Policer_Profile_Is_profile_exsit(MEA_Unit_t            unit_i,
    MEA_db_HwUnit_t       hwUnit_i,
    MEA_AcmMode_t  ACM_Mode,
    MEA_Policer_prof_t    id_i ,
    MEA_Bool *exsit);


#ifdef __cplusplus
 }
#endif 

#endif /* MEA_POLICER_PROFILE_DRV_H */
