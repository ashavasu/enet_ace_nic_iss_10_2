/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/******************************************************************************
* 	Module Name:		 enet_xPermission_drv.h
*
*   Module Description:
*
*
*  Date of Creation:	 17/09/06
*
*  Author Name:			 Alex Weis.
*******************************************************************************/

#ifndef ENET_XPERMISSION_DRV_H
#define	ENET_XPERMISSION_DRV_H

#include "MEA_platform.h"
#include "mea_api.h"

#ifdef __cplusplus
extern "C" {
#endif


#define ENET_XPERMISSION_OPPOSITE_TABLE(queueId_i) \
   ((enet_xPermission_opposite_dbt*) \
    ((char*)enet_xPermission_opposite_Table + \
     ((queueId_i)* \
      ((sizeof(enet_xPermission_opposite_dbt) - 4) + \
       (ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(0)/8)))))

/*-------------------------------- Definitions --------------------------------------*/
typedef MEA_OutPorts_Entry_dbt ENET_xPermission_dbt;

typedef struct {
	ENET_Uint32 numberOfOwners;
	ENET_xPermission_dbt portMap;
	mea_memory_mode_e globalMemoryMode;
	ENET_Bool isCpu;
} enet_xPermission_dbt, *enet_xPermission_dbp;

typedef struct enet_xPermission_opposite_dbs {
	MEA_PortState_te portState;
	ENET_Uint32 xPermissions[1];	/* [ENET_IF_CMD_PARAM_TBL_TYP_XPERMISSION_LENGTH(hwUnit)/32] */
} enet_xPermission_opposite_dbt, *enet_xPermission_opposite_dbp;

/*-------------------------------- External variables -------------------------------*/

/*-------------------------------- Functions ----------------------------------------*/

ENET_Status enet_Init_xPermission(ENET_Unit_t unit_i);
ENET_Status enet_ReInit_xPermission(ENET_Unit_t unit_i);

ENET_Status enet_Conclude_xPermission(ENET_Unit_t unit_i);

ENET_Status enet_Create_xPermission(ENET_Unit_t unit_i,
                                    ENET_xPermission_dbt * entry_i,
                                    ENET_Bool isCpu,
				                    ENET_xPermissionId_t * id_io);

ENET_Status enet_Set_xPermission(ENET_Unit_t unit_i,
                                 ENET_xPermissionId_t id_i,
				                 ENET_Bool isCpu,
                                 ENET_xPermission_dbt * entry_i);

ENET_Status enet_Get_xPermission_Internal(ENET_Unit_t unit_i,
                                          ENET_xPermissionId_t id_i,
                                          enet_xPermission_dbt * entry_o);

ENET_Status enet_Get_xPermission(ENET_Unit_t unit_i,
                                 ENET_xPermissionId_t id_i,
                                 ENET_xPermission_dbt *entry_o);

ENET_Status enet_Delete_xPermission(ENET_Unit_t unit_i,
                                    ENET_xPermissionId_t id_i);

ENET_Status enet_GetFirst_xPermission(ENET_Unit_t unit_i,
                                      ENET_xPermissionId_t *id_o,
                                      ENET_xPermission_dbt *entry_o,
				                      ENET_Bool *found_o);

ENET_Status enet_GetNext_xPermission(ENET_Unit_t unit_i,
                                     ENET_xPermissionId_t *id_io,
                                     ENET_xPermission_dbt *entry_o,
                                     ENET_Bool *found_o);

ENET_Status enet_GetIDByEntry_xPermission(ENET_Unit_t unit_i,
                                          ENET_xPermission_dbt *entry_i,
                                          ENET_Bool isCpu_i,
                                          ENET_xPermissionId_t *id_o,
                                          ENET_Bool *found_o);

ENET_Bool enet_IsValid_xPermission(ENET_Unit_t unit_i,
                                   ENET_xPermissionId_t id_i,
                                   ENET_Bool silent_i);

void enet_SetDefaultClusterToPort(ENET_Unit_t unit_i,
                                  ENET_PortId_t port,
				                  ENET_QueueId_t cluster);

ENET_Status enet_UpdateDevice_xPermission(ENET_Unit_t unit_i,
                                          ENET_xPermissionId_t id_i,
                                          ENET_Bool isCpu_i,
                                          ENET_xPermission_dbt *new_entry_i,
                                          ENET_xPermission_dbt *old_entry_i);

ENET_Status enet_xPermission_disableCluster(ENET_Unit_t unit_i,
                                            ENET_QueueId_t cluster);

ENET_Status enet_xPermission_reEnableCluster(ENET_Unit_t unit_i,
                                             ENET_QueueId_t cluster);

MEA_Status enet_xPermission_UpdateByPortState(MEA_Unit_t unit_i,
                                              ENET_QueueId_t queueId_i,
                                              MEA_PortState_te portState_i);

MEA_Status enet_xPermission_opposite_Get(MEA_Unit_t unit_i,
					 ENET_QueueId_t queueId_i,
					 enet_xPermission_opposite_dbt *entry);




MEA_Status enet_xPermission_Add_owner(ENET_Unit_t    unit_i,ENET_xPermissionId_t id_io);
MEA_Status enet_xPermission_delete_owner(ENET_Unit_t    unit_i, ENET_xPermissionId_t id_i);



#ifdef __cplusplus
}
#endif


#endif				/* ENET_XPERMISSION_DRV_H */
