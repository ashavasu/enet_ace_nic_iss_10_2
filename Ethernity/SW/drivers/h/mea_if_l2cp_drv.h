/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_IF_L2CP_DRV_H
#define MEA_IF_L2CP_DRV_H

#include "MEA_platform.h"


#ifdef __cplusplus
extern "C" {
#endif 
MEA_Status mea_drv_Init_IF_L2CP(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_IF_L2CP(MEA_Unit_t unit_i);

MEA_Status mea_drv_ReInit_IF_L2CP(MEA_Unit_t unit_i);

MEA_Status mea_drv_Set_NetworkL2CP_BaseAddr(MEA_Unit_t unit,
					    MEA_MacAddr * mac,
					    MEA_MacAddr * old_mac);

MEA_Status mea_drv_l2cp_def_sid_hw_manager_set(MEA_Port_t port);

#ifdef __cplusplus
 }
#endif 

#endif /* MEA_IF_L2CP_DRV_H */
