/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_POLICER_DRV_H
#define	MEA_POLICER_DRV_H



#include "MEA_platform.h"
#include "mea_api.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
extern "C" {
#endif 
typedef struct {
	MEA_Bool			  valid;
	MEA_Uint32			  num_of_owners;
    MEA_Policer_prof_t    prof_id;
	MEA_db_HwUnit_t       hwUnit;
    MEA_TmId_t            hwTmid;
} MEA_drv_policer_dbt;



#define MEA_POLICER_MODE_SINGLE MEA_FALSE 
#define MEA_POLICER_MODE_DUAL   MEA_TRUE

typedef struct{
	MEA_EbsCbs_t        cbs_ebs;
    MEA_EbsCbs_t        Lmax;
	MEA_Uint16          valid      : 1;
	MEA_Uint16          mode       : 1;   /* MEA_POLICER_MODE_XXX */
	MEA_Uint16          rateEnable :  1; 
	MEA_Uint16          comp       :  9;
    MEA_Uint16          gn_type    :  3;
    MEA_Uint16          gn_sign    :  1;
    MEA_Uint16          type_mode  :  1;


    MEA_Policer_prof_t  prof_id;
} MEA_drv_policer_hw_dbt;

typedef struct {
	MEA_Uint32              num_of_entries;
	MEA_drv_policer_hw_dbt *table;
} MEA_drv_policer_hwUnit_dbt;


MEA_Status mea_drv_Init_Policer(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_Policer(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_Policer(MEA_Unit_t unit_i);


MEA_Status  mea_drv_Ingress_flow_policer_Init(MEA_Unit_t unit);
MEA_Status  mea_drv_Ingress_flow_policer_Reinit(MEA_Unit_t unit);
MEA_Status  mea_drv_Ingress_flow_policer_Conclude(MEA_Unit_t unit);


MEA_Status mea_drv_Set_Policer(MEA_Unit_t               unit_i,
							   MEA_IngressPort_Proto_t  port_proto_prof_i,
                               MEA_Policer_Entry_dbt   *Policer_Entry_i,
							   MEA_Bool                 force_update_i,
                               MEA_Bool                 rateEnable,
                               MEA_Policer_prof_t      *prof_id_io,
							   MEA_TmId_t              *id_io);

MEA_Status mea_drv_policer_Tmid_Valid(MEA_Unit_t unit_i,
									  MEA_TmId_t id);
MEA_Status mea_drv_policer_add_owner(MEA_Unit_t unit_i,
									 MEA_TmId_t id);
MEA_Status  mea_drv_Get_Policer(MEA_Unit_t unit_i,
								MEA_TmId_t             id,
                                MEA_Policer_Entry_dbt *pSLA_Params);

MEA_Status mea_drv_Delete_Policer(MEA_Unit_t unit_i,
								  MEA_TmId_t id);

MEA_Status mea_drv_get_policer_pool_entry(MEA_Unit_t unit_i,
										  MEA_TmId_t       index,
									  MEA_Bool            *found,
									  MEA_drv_policer_dbt *entry_o );

MEA_Status mea_drv_GetTmid_Policer_INFO(MEA_Unit_t unit_i,
										MEA_TmId_t             id,
                                        MEA_drv_policer_dbt *entry);

MEA_Status MEA_PolicerHW_Tmid_GetInfo(MEA_Unit_t unit_i,
									  MEA_db_HwUnit_t        hwUnit_i,
									  MEA_TmId_t             Hwid,
                                      MEA_drv_policer_hw_dbt *entry,
                                      MEA_Bool              *found);
MEA_Status MEA_PolicerSW_Tmid_GetInfo(MEA_Unit_t unit_i,
									  MEA_TmId_t           sw_tmid_i,
                                      MEA_drv_policer_dbt *entry_po ,
                                      MEA_Bool            *found_o  );
MEA_Status mea_drv_CreateNew_Policer(MEA_Unit_t unit_i,
                                     MEA_IngressPort_Proto_t  port_proto_prof_i,
                               MEA_Policer_Entry_dbt   *Policer_Entry_i,
							   MEA_Bool                 force_update_i,
                               MEA_Bool                 rateEnable,  
                               MEA_Policer_prof_t      *prof_id_io,
							   MEA_TmId_t              *id_io);

MEA_Status mea_drv_getOwner_Policer(MEA_Unit_t  unit_i,
                                    MEA_TmId_t  tm_id_i,
									MEA_Uint32 *numOfOwners_o);

MEA_Status mea_drv_addOwner_Policer(MEA_Unit_t unit_i,
                                    MEA_IngressPort_Proto_t  port_proto_prof_i,
                               MEA_Policer_Entry_dbt   *Policer_Entry_i,
							   MEA_Bool                 force_update_i,
                               MEA_Bool                 rateEnable,  
							   MEA_TmId_t              *id_io);

MEA_Status mea_drv_CreateId_Policer(MEA_Unit_t unit_i,
                               MEA_IngressPort_Proto_t  port_proto_prof_i,
                               MEA_Policer_Entry_dbt   *Policer_Entry_i,
							   MEA_Bool                 force_update_i,
                               MEA_Bool                 rateEnable,  
                               MEA_Policer_prof_t      *prof_id_io,
							   MEA_TmId_t              *id_io);

MEA_Status mea_drv_Get_Policer_Pointer_db   (MEA_Unit_t  unit_i,
								             MEA_db_dbt* db_po);
MEA_Status mea_drv_Get_Policer_db           (MEA_Unit_t  unit_i,
								             MEA_db_dbt* db_po);
MEA_Status MEA_policer_compensation(MEA_Uint32 port_proto_prof_i,MEA_Uint32 *comp);

#ifdef __cplusplus
 }
#endif 

#endif /* MEA_POLICER_DRV_H */
