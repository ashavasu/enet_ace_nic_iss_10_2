/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_lxcp_drv.h,v $
#
#------------------------------------------------------------------------------
*/

#ifndef MEA_LXCP_DRV_H
#define MEA_LXCP_DRV_H



#include "mea_api.h"
#include "mea_db_drv.h"

#ifdef __cplusplus
extern "C" {
#endif 

#define MEA_LXCP_PROFILE_ALL_TRANSPARENT 0


MEA_Status mea_lxcp_drv_Init(MEA_Unit_t  unit );
MEA_Status mea_lxcp_drv_ReInit(MEA_Unit_t  unit );
MEA_Status mea_lxcp_drv_Conclude(MEA_Unit_t  unit );

MEA_Status mea_lxcp_drv_delete_owner(MEA_Unit_t  unit,
                                            MEA_LxCp_t  i_LxCpId);
MEA_Status mea_lxcp_drv_Add_owner(MEA_Unit_t  unit,
                                         MEA_LxCp_t  i_LxCpId);


MEA_Bool mea_Lxcp_drv_IsOutPortOf_LxCP_Protocol(MEA_Unit_t                   unit_i, 
                                           MEA_LxCp_t                   LxCp_Id_i,
                                           MEA_LxCP_Protocol_key_dbt   *key_po,
                                           MEA_Port_t                   outPort_i);

MEA_Status mea_drv_Get_LxCP_db          (MEA_Unit_t  unit_i,
                                         MEA_db_dbt* db_po);

MEA_Status mea_drv_Get_LxCP_Protocol_db (MEA_Unit_t  unit_i,
                                         MEA_db_dbt* db_po);

MEA_Status mea_drv_Get_LxCP_Action_db   (MEA_Unit_t  unit_i,
                                         MEA_db_dbt* db_po);


#ifdef __cplusplus
 }
#endif 

#endif /* MEA_LXCP_DRV_H */
