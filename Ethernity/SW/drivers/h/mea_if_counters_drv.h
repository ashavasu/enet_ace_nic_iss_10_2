/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_IF_COUNTERS_DRV_H
#define MEA_IF_COUNTERS_DRV_H



#include "MEA_platform.h"

#ifdef __cplusplus
extern "C" {
#endif

MEA_Status mea_drv_Init_IF_Counters_IngressPort(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_IF_Counters_IngressPort(MEA_Unit_t unit_i);

MEA_Status mea_drv_Init_IF_Counters_RMON(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_IF_Counters_RMON(MEA_Unit_t unit_i);


MEA_Status mea_drv_Init_Utopia_RMON_Counters(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_Utopia_RMON_Counters(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_Utopia_RMON_Counters(MEA_Unit_t unit_i);


MEA_Status mea_drv_Init_Virtual_Rmon_Counters(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_Virtual_Rmon_Counters(MEA_Unit_t unit_i);
MEA_Status mea_drv_Conclude_Virtual_Rmon_Counters(MEA_Unit_t unit_i);


#ifdef __cplusplus
 }
#endif 

#endif /* MEA_IF_COUNTERS_DRV_H */
