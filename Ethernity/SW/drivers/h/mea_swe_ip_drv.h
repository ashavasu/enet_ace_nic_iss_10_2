/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_SWE_INGRESS_PORT_DRV_H
#define MEA_SWE_INGRESS_PORT_DRV_H



#include "MEA_platform.h"

#ifdef __cplusplus
 extern "C" {
#endif

MEA_Status mea_drv_Init_IngressPort_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_IngressPort_Table(MEA_Unit_t unit_i);
MEA_Status mea_drv_ReInit_RxEnable(MEA_Unit_t unit_i);

MEA_Status mea_drv_Init_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit);
MEA_Status mea_drv_Reinit_IngressPort_Default_L2Type_Sid(MEA_Unit_t unit);
MEA_Status mea_drv_Init_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit);
MEA_Status mea_drv_Reinit_IngressPort_Default_PDUs_Sid(MEA_Unit_t unit);
//MEA_Status mea_IngressPort_Write_Mtu(MEA_Port_t port, MEA_Uint32 mtuValue);

MEA_Status mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_Init(MEA_Unit_t  unit_i );
MEA_Status mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_RInit(MEA_Unit_t  unit_i );
MEA_Status mea_drv_IngressGlobal_L2_TO_L3_1588_Offset_Conclude(MEA_Unit_t  unit_i);
MEA_Status mea_drv_ReInit_RxDisable(MEA_Unit_t unit_i);

MEA_Bool mea_drv_Init_Rx_AfdxBag_prof(MEA_Unit_t       unit_i);
MEA_Bool mea_drv_RInit_Rx_AfdxBag_prof(MEA_Unit_t       unit_i);
MEA_Bool mea_drv_Conclude_Rx_AfdxBag_prof(MEA_Unit_t       unit_i);
MEA_Status mea_drv_Rx_AfdxBag_prof_IsExist(MEA_Unit_t     unit_i,MEA_Uint16 id_i,MEA_Bool *exist);
MEA_Status mea_drv_Rx_AfdxBag_prof_add_owner(MEA_Unit_t       unit_i ,
                                          MEA_Uint16       id_i);
MEA_Status mea_drv_Rx_AfdxBag_prof_delete_owner(MEA_Unit_t       unit_i ,
                                                MEA_Uint16       id_i);

MEA_Bool mea_drv_Rx_AfdxBag_prof_IsRange(MEA_Unit_t     unit_i,
                                         MEA_Uint16 id_i);

MEA_Status mea_drv_Rx_AfdxBag_Set_Ingress(MEA_Unit_t                     unit_i,
                                       MEA_Uint16                      id_i,
                                       MEA_RxAfdxBag_dbt *entry_pi);


MEA_Status mea_drv_Rx_AfdxBag_Create_Ingress(MEA_Unit_t                      unit_i,
                                          MEA_RxAfdxBag_dbt           *entry_pi,
                                          MEA_Uint16                  *id_io);



MEA_Status mea_drv_Rx_AfdxBag_Get_Ingress(MEA_Unit_t          unit_i,
                                       MEA_Uint16            id_i,
                                       MEA_RxAfdxBag_dbt    *entry_po);
MEA_Status mea_drv_Rx_AfdxBag_GetFirst_Ingress(MEA_Unit_t               unit_i,
                                            MEA_Uint16                *id_o,
                                            MEA_RxAfdxBag_dbt    *entry_po, /* Can't be NULL */
                                            MEA_Bool                 *found_o);
MEA_Status mea_drv_Rx_AfdxBag_GetNext_Ingress(MEA_Unit_t                  unit_i,
                                           MEA_Uint16                   *id_io,
                                           MEA_RxAfdxBag_dbt       *entry_po, /* Can't be NULL */
                                           MEA_Bool                    *found_o);


MEA_Status mea_drv_Init_port_PTP1588_Delay(MEA_Unit_t unit_i);
MEA_Status mea_drv_Rinit_port_PTP1588_Delay(MEA_Unit_t unit_i);

MEA_Status MEA_Configure_BlockOF256PORTS_TABLE100(MEA_Unit_t unit, MEA_Internal_Block_256OutPorts_dbt *BlockOutPort, MEA_Uint32 offset);
MEA_Status MEA_Get_BlockOF256PORTS_TABLE100(MEA_Unit_t unit, MEA_Internal_Block_256OutPorts_dbt *BlockOutPort, MEA_Uint32 offset);

MEA_Status mea_drv_Counclude_IngressPort_Table(MEA_Unit_t unit_i);
#ifdef __cplusplus
 }
#endif 

#endif /* MEA_SWE_INGRESS_PORT_DRV_H */
