/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_cli_ccm.h,v $
#
#------------------------------------------------------------------------------
*/


#ifndef _MEA_CLI_CCM_H_
#define _MEA_CLI_CCM_H_


#include "MEA_platform_types.h"




MEA_Status MEA_CLI_CCM_AddDebugCmds(void);




#endif /*_MEA_CLI_ccm_H_*/
