/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_cli_lxcp.h,v $
#
#------------------------------------------------------------------------------
*/


#ifndef _MEA_CLI_ACL_FILTER_H_
#define _MEA_CLI_ACL_FILTER_H_


#include "MEA_platform_types.h"

typedef enum {
    MEA_CLI_FILTER_PRI_TYPE_DEFAULT_UNTAG = 0,
    MEA_CLI_FILTER_PRI_TYPE_OUTER_VLAN_TAG = 1,
    MEA_CLI_FILTER_PRI_TYPE_INNER_VLAN_TAG = 2,
    MEA_CLI_FILTER_PRI_TYPE_IPV4_PRECEDENCE = 3,
    MEA_CLI_FILTER_PRI_TYPE_IPV4_TOS = 4,
    MEA_CLI_FILTER_PRI_TYPE_IPV4_DSCP = 5,
    MEA_CLI_FILTER_PRI_TYPE_LAST = 6,
} MEA_CLI_Filter_Pri_type_te;


MEA_Status MEA_CLI_ACL_filter_AddDebugCmds(void);




#endif /*_MEA_CLI_LXCP_H_*/
