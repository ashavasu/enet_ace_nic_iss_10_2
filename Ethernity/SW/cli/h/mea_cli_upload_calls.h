/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_cli_upload_calls.h,v $
#
#-----------------------------------------------------------------------------
*/

#if 0

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_CMD_PARAM_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_CMD_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_WRITE_DATA_INFO_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_READ_DATA_INFO_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_SCRATCH_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_GLOBAL0_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_GLOBAL1_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_GLOBAL2_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_GLOBAL3_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_SSSMII_RX_CONFIG_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_SSSMII_TX_CONFIG0_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_SSSMII_TX_CONFIG1_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_INTERRUPT_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_IF,
             MEA_IF_INTERRUPT_MASK_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_CONFIG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_PRE_SCH_CY_LEN);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_IA_ADDR);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_IA_WRDATA0);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_IA_WRDATA1);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_IA_RDDATA0);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_IA_RDDATA1);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_IA_CMD);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_IA_STAT);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_DESC_H_LIMIT);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_DESC_L_LIMIT);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_NSBP);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_HSBP);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_IMR);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_IPR);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_ISR);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_LATENCY_MODE3);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_LATENCY_MODE2);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_LATENCY_MODE1);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_LATENCY_MODE0);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_H2EGCRC_CALCENB3);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_H2EGCRC_CALCENB2);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_H2EGCRC_CALCENB1);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_H2EGCRC_CALCENB0);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_PKT_SHORT_LIMIT_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_PKT_HIGH_LIMIT_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_CHUNK_SHORT_LIMIT_REG);

MEA_CLI_UPLOAD_read_reg(MEA_MODULE_BM,
             MEA_BM_CHUNK_HIGH_LIMIT_REG);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_IF,
             MEA_IF_CMD_PARAM_TBL_TYP_L2CP,
             MEA_IF_CMD_PARAM_TBL_TYP_L2CP_LENGTH,
             MEA_IF_CMD_PARAM_TBL_TYP_L2CP_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_IF,
             MEA_IF_CMD_PARAM_TBL_TYP_PARSER,
             MEA_IF_CMD_PARAM_TBL_TYP_PARSER_LENGTH,
             MEA_IF_CMD_PARAM_TBL_TYP_PARSER_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_IF,
             MEA_IF_CMD_PARAM_TBL_TYP_REG,
             MEA_IF_CMD_PARAM_TBL_TYP_REG_LENGTH,
             MEA_IF_CMD_PARAM_TBL_TYP_REG_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_IF,
             MEA_IF_CMD_PARAM_TBL_TYP_PRE_PARSER,
             MEA_IF_CMD_PARAM_TBL_TYP_PRE_PARSER_LENGTH,
             MEA_IF_CMD_PARAM_TBL_TYP_PRE_PARSER_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_ETH,
             MEA_BM_TBL_TYP_EHP_ETH_LENGTH,
             MEA_BM_TBL_TYP_EHP_ETH_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_ATM,
             MEA_BM_TBL_TYP_EHP_ATM_LENGTH,
             MEA_BM_TBL_TYP_EHP_ATM_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_MARTINI_DA_LSB,
             MEA_BM_TBL_TYP_EHP_MARTINI_DA_LSB_LENGTH,
             MEA_BM_TBL_TYP_EHP_MARTINI_DA_LSB_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_MARTINI_SA_LSB,
             MEA_BM_TBL_TYP_EHP_MARTINI_SA_LSB_LENGTH,
             MEA_BM_TBL_TYP_EHP_MARTINI_SA_LSB_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_MARTINI_PROFILES_ID,
             MEA_BM_TBL_TYP_EHP_MARTINI_PROFILES_ID_LENGTH,
             MEA_BM_TBL_TYP_EHP_MARTINI_PROFILES_ID_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_MARTINI_PROFILES,
             MEA_BM_TBL_TYP_EHP_MARTINI_PROFILES_LENGTH,
             MEA_BM_TBL_TYP_EHP_MARTINI_PROFILES_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_MARTINI_CMD,
             MEA_BM_TBL_TYP_EHP_MARTINI_CMD_LENGTH,
             MEA_BM_TBL_TYP_EHP_MARTINI_CMD_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_EOA_MSB,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_EOA_MSB_LENGTH,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_EOA_MSB_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_EOA_LSB,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_EOA_LSB_LENGTH,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_EOA_LSB_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_IPOA,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_IPOA_LENGTH,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_IPOA_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_PPPOA,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_PPPOA_LENGTH,
             MEA_BM_TBL_TYP_EHP_LLC_HDR_PPPOA_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_AAL5_TRAILER,
             MEA_BM_TBL_TYP_EHP_AAL5_TRAILER_LENGTH,
             MEA_BM_TBL_TYP_EHP_AAL5_TRAILER_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_PPPoE,
             MEA_BM_TBL_TYP_EHP_PPPoE_LENGTH,
             MEA_BM_TBL_TYP_EHP_PPPoE_WIDTH);

#if 0 /* Not implement */
MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EHP_VCMUX_HDR_EOA,
             MEA_BM_TBL_TYP_EHP_VCMUX_HDR_EOA_LENGTH,
             MEA_BM_TBL_TYP_EHP_VCMUX_HDR_EOA_WIDTH);
#endif

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_SCHEDULER_PRIORITY,
             MEA_BM_TBL_TYP_SCHEDULER_PRIORITY_LENGTH,
             MEA_BM_TBL_TYP_SCHEDULER_PRIORITY_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_QUEUE_MTU_SIZE,
             MEA_BM_TBL_TYP_QUEUE_MTU_SIZE_LENGTH,
             MEA_BM_TBL_TYP_QUEUE_MTU_SIZE_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_PRI_DROP_PROP,
             MEA_BM_TBL_TYP_PRI_DROP_PROP_LENGTH,
             MEA_BM_TBL_TYP_PRI_DROP_PROP_WIDTH);
#if 0
MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EGRESS_METERING_LSB,
             MEA_BM_TBL_TYP_EGRESS_METERING_LSB_LENGTH,
             MEA_BM_TBL_TYP_EGRESS_METERING_LSB_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EGRESS_METERING_MSB,
             MEA_BM_TBL_TYP_EGRESS_METERING_MSB_LENGTH,
             MEA_BM_TBL_TYP_EGRESS_METERING_MSB_WIDTH);
#endif
MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
                          MEA_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT,
                          MEA_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT_LENGTH ,               
                          MEA_BM_TBL_TYP_SRV_RATE_METERING_PROF_POINIT_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
                          MEA_BM_TBL_TYP_SRV_RATE_METERING_PROF,                              
                          MEA_BM_TBL_TYP_SRV_RATE_METERING_PROF_LENGTH ,                          
                          MEA_BM_TBL_TYP_SRV_RATE_METERING_PROF_WIDTH);                            

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
                          MEA_BM_TBL_TYP_SRV_RATE_METERING_BUCKET ,
                          MEA_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_LENGTH ,                         
                          MEA_BM_TBL_TYP_SRV_RATE_METERING_BUCKET_WIDTH);



MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,
             MEA_BM_TBL_TYP_EGRESS_PORT_MQS,
             MEA_BM_TBL_TYP_EGRESS_PORT_MQS_LENGTH,
             MEA_BM_TBL_TYP_EGRESS_PORT_MQS_WIDTH);

MEA_CLI_UPLOAD_read_table(MEA_MODULE_BM,

             MEA_BM_TBL_HALT_FLUSH_CMD_NOT_ACTIVE,
             MEA_BM_TBL_HALT_FLUSH_CMD_NOT_ACTIVE_LENGTH,
             MEA_BM_TBL_HALT_FLUSH_CMD_NOT_ACTIVE_WIDTH);

#endif
