/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_cli_forwarder.h,v $
#
#------------------------------------------------------------------------------
*/

#ifndef MEA_CLI_FORWARDER_H
#define	MEA_CLI_FORWARDER_H

MEA_Status mea_frwarder_print_key(MEA_SE_Entry_key_dbt *forwarder_key, MEA_Bool settintCR);

/* Add MEA CLI Forwarder commands */
MEA_Status MEA_CLI_Forwarder_AddCmds (void);




#endif /* MEA_CLI_FORWARDER_H */
