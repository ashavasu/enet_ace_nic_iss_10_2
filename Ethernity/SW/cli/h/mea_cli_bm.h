/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_cli_bm.h,v $
#
#------------------------------------------------------------------------------
*/


#ifndef _MEA_CLI_BM_H_
#define _MEA_CLI_BM_H_

#ifdef __cplusplus
extern "C" {
#endif





/* Add MEA CLI BM Debug commands */
MEA_Status MEA_CLI_BN_AddDebugCmds (void);

/* MEA BM CLI Debug commands functions */
MEA_Status MEA_CLI_BM_Reset                       (int argc, char *argv[]);
MEA_Status MEA_CLI_CHIP_BM_Reset                  (int argc, char *argv[]);
MEA_Status MEA_CLI_BM_Write                       (int argc, char *argv[]);
MEA_Status MEA_CLI_BM_Read                        (int argc, char *argv[]);
MEA_Status MEA_CLI_BM_WriteInd                    (int argc, char *argv[]);
MEA_Status MEA_CLI_BM_ReadInd                     (int argc, char *argv[]);
MEA_Status MEA_CLI_BM_GetCnt                      (int argc, char *argv[]);
MEA_Status MEA_CLI_DebugPMShow					  (int argc, char *argv[]);


MEA_Status MEA_CLI_CreateEgressWredProfile        (int argc, char *argv[]);
MEA_Status MEA_CLI_SetEgressWredProfile           (int argc, char *argv[]);
MEA_Status MEA_CLI_DeleteEgressWredProfile        (int argc, char *argv[]);
MEA_Status MEA_CLI_ShowEgressWredProfile          (int argc, char *argv[]);
MEA_Status MEA_CLI_SetEgressWredEntry             (int argc, char *argv[]);
MEA_Status MEA_CLI_SetEgressWredStep              (int argc, char *argv[]);
MEA_Status MEA_CLI_SetEgressWredCurve             (int argc, char *argv[]);
MEA_Status MEA_CLI_SetEgressResetCounters         (int argc, char *argv[]);
MEA_Status MEA_CLI_Prompt                         (int argc, char *argv[]);
MEA_Status MEA_CLI_mode                           (int argc, char *argv[]);
MEA_Status MEA_CLI_Show_Counters_PM_exist         (int argc, char *argv[]);
MEA_Status MEA_CLI_Show_Counters_PMType           (int argc, char *argv[]);
MEA_Status MEA_CLI_Show_Counters_PM               (int argc, char *argv[]);
MEA_Status MEA_CLI_Clear_Counters_PM              (int argc, char *argv[]);
MEA_Status MEA_CLI_Collect_Counters_PM            (int argc, char *argv[]);
MEA_Status MEA_CLI_Collect_Counters_PM_blk        (int argc, char *argv[]);
MEA_Status MEA_CLI_Collect_Counters_PM_blkShow    (int argc, char *argv[]);


MEA_Status MEA_CLI_Show_Counters_Queue             (int argc, char *argv[]);
MEA_Status MEA_CLI_Clear_Counters_Queue            (int argc, char *argv[]);
MEA_Status MEA_CLI_Collect_Counters_Queue          (int argc, char *argv[]);
MEA_Status MEA_CLI_Show_Counters_BM               (int argc, char *argv[]);
MEA_Status MEA_CLI_Clear_Counters_BM              (int argc, char *argv[]);
MEA_Status MEA_CLI_Collect_Counters_BM            (int argc, char *argv[]);

MEA_Status MEA_CLI_Show_clusters_portRate          (int argc, char *argv[]);


MEA_Status MEA_CLI_ShowEgressPortEntry            (int argc, char *argv[]);
MEA_Status MEA_CLI_Show_EgressPortQueueSize_Info  (int argc, char *argv[]);
MEA_Status MEA_CLI_Show_EgressPortShaper_Info     (int argc, char *argv[]);
MEA_Status MEA_CLI_Show_EgressPortTEID_stamp_Info(int argc, char *argv[]);
MEA_Status MEA_CLI_ShowEgressWred             (int argc, char *argv[]);
MEA_Status MEA_CLI_Show_EgressPort_my_mac(int argc, char *argv[]);

MEA_Status MEA_CLI_Show_EgressPort_protect_1p1(int argc, char *argv[]);
MEA_Status MEA_CLI_Show_EgressPort_g999_frg_scheduler(int argc, char *argv[]);
MEA_Status MEA_CLI_Show_EgressPort_rule_state(int argc, char *argv[]);
MEA_Status MEA_CLI_Show_EgressPort_L2_mask(int argc, char *argv[]);


MEA_Status MEA_CLI_DebugBM_Table                  (int argc, char *argv[]);
#ifdef __cplusplus
}
#endif

#endif
