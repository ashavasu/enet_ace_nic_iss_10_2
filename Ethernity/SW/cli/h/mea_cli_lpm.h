/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
/*
#------------------------------------------------------------------------------
#
# $RCSfile: mea_cli_lpm.h,v $
#
#------------------------------------------------------------------------------
*/

#ifndef MEA_CLI_LPM_H
#define	MEA_CLI_LPM_H

/* Add MEA CLI LPM commands */
MEA_Status MEA_CLI_LPM_AddCmds (void);


#endif /* MEA_CLI_LPM_H */
