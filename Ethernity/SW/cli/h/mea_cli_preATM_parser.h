/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/



#ifndef _MEA_CLI_PREATM_PARSER_H_
#define _MEA_CLI_PREATM_PARSER_H_


#include "MEA_platform_types.h"

#ifdef __cplusplus
extern "C" {
#endif 




MEA_Status MEA_CLI_preATM_parser_AddDebugCmds(void);
#ifdef __cplusplus
}
#endif





#endif /* _MEA_CLI_FLOW_MARKING_MAPPING_PROFILE_H_ */
