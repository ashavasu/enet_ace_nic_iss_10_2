/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_CLI_H
#define	MEA_CLI_H

#include "MEA_platform.h"
#include "mea_api.h"

#ifdef __cplusplus
extern "C" {
#endif 

 

#if defined(MEA_PLAT_EVALUATION_BOARD)
    /*#define WITH_MEA_EXAMPLE_CLI*/
    //#define WITH_MEA_EXAMPLE2_CLI
    //#define WITH_MEA_EXAMPLE3_CLI
    //#define WITH_MEA_EXAMPLE4_CLI
#define WITH_MEA_EXAMPLE5_CLI

#ifdef MEA_WITH_TMUX
#define WITH_MEA_EXAMPLE_TMUX_CLI
#endif

#endif



#define MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch)   if ((len) >= (MEA_Uint16) (argc)) { \
    CLI_print(" missing arguments in command %s \n", (ch)); \
    return MEA_ERROR; }



typedef struct {
  char  *str;
  MEA_Uint32  value;
} MEA_CLI_ENUM_VAL;

typedef struct {
  unsigned long length;
  unsigned char data[1536];
} MEA_CLI_PacketData_t;

typedef struct {
    MEA_Bool bCmd;
    MEA_Uint16 cmd;
    MEA_Uint16 value;
} MEA_CLI_force_cmd_t;







typedef enum {
    MEA_CLI_EDITING_INFO_EDITING,
    MEA_CLI_EDITING_INFO_MARTINI,
    MEA_CLI_EDITING_INFO_STAMPING,
    MEA_CLI_EDITING_INFO_SESSION,
    MEA_CLI_EDITING_INFO_LMID,
    MEA_CLI_EDITING_INFO_MPLS,
    MEA_CLI_EDITING_INFO_PW,
    MEA_CLI_EDITING_INFO_DSCP,
    MEA_CLI_EDITING_INFO_IPCS_DL,
    MEA_CLI_EDITING_INFO_IPCS_UL,
    MEA_CLI_EDITING_INFO_ETHCS_DL,
    MEA_CLI_EDITING_INFO_ETHCS_UL,
    MEA_CLI_EDITING_INFO_NAT,
    MEA_CLI_EDITING_INFO_PROTOLLC,
    MEA_CLI_EDITING_INFO_VXLAN_DL,
    MEA_CLI_EDITING_INFO_NVGRE,
    MEA_CLI_EDITING_INFO_IPSEC,
    MEA_CLI_EDITING_INFO_L2TPGRE,


    
    MEA_CLI_EDITING_INFO_LAST
} mea_cli_editing_info_te;


typedef enum {
   MEA_CLI_PACKET_TYPE_VoE =0,
   MEA_CLI_PACKET_TYPE_PPPoVoE_DISCOVERY =1,
   MEA_CLI_PACKET_TYPE_PPPoEoA_DISCOVERY =2,
   MEA_CLI_PACKET_TYPE_PPPoVoE_LCP =3,
   MEA_CLI_PACKET_TYPE_PPPoA_LCP =4,
   MEA_CLI_PACKET_TYPE_PPPoA_CHAP =5,
   MEA_CLI_PACKET_TYPE_PPPoA_PAP =6,
   MEA_CLI_PACKET_TYPE_PPPoVoE_NCP =7,
   MEA_CLI_PACKET_TYPE_PPPoA_NCP =8,
   MEA_CLI_PACKET_TYPE_ARPoE =9,
   MEA_CLI_PACKET_TYPE_IGMP =10,
   MEA_CLI_PACKET_TYPE_DHCP =11,
   MEA_CLI_PACKET_TYPE_IPoA =12,
   MEA_CLI_PACKET_TYPE_EoA =13,
   MEA_CLI_PACKET_TYPE_LAST
} MEA_CLI_PacketType_te;


typedef struct {
    MEA_Uint32              type;
    MEA_Action_t            Action_Id;
    MEA_Uint32              index;
    mea_cli_editing_info_te info;
    MEA_Uint32              count;
    MEA_EgressHeaderProc_Array_Entry_dbt  *Action_Editing;

} mea_cli_showEditing_info_t;


/************************************************************************/
/*                                                                      */
/************************************************************************/
extern char meacli_LongHelp_Create_Action[];



/************************************************************************/
/*                                                                      */
/************************************************************************/

void mea_cli_Acton_Print_Editing_info(mea_cli_showEditing_info_t *entry);

MEA_Status MEA_CLI_LowLevel_ShowEvents(MEA_Uint8 Type, MEA_Events_Entry_dbt *pEntry);
void mea_drv_show_event_value(MEA_Uint8 type, char *str_name, MEA_Uint32 value);

MEA_Status MEA_CLI_ActionCreate(int argc, char *argv[]);
MEA_Status MEA_CLI_ActionModify(int argc, char *argv[]);

MEA_Status MEA_CLI_ShowEvents(int argc, char *argv[]);

MEA_Status mea_deviceInfo_print(MEA_Unit_t unit, MEA_db_HwUnit_t         hwUnit);


MEA_Status MEA_CLI_Show_wr_ENG_State(int argc, char *argv[]);

MEA_Status MEA_CLI_ShowPBB_Learn(int argc, char *argv[]);

MEA_Uint32 MEA_CLI_set_memory_mode(mea_memory_mode_e mode,
				   MEA_Bool changePrompt);

MEA_Status MEA_CLI_AddDebugCmds(void);

#ifdef __cplusplus
}
#endif

#endif

