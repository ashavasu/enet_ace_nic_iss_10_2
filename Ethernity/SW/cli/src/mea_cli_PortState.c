/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_PortState.h"

static MEA_Status MEA_CLI_PortState_set(int argc, char *argv[]);
static MEA_Status MEA_CLI_PortState_show(int argc, char *argv[]);

MEA_Status MEA_CLI_PORTSTATE_AddDebugCmds(void)
{

	CLI_defineCmd("MEA port state set",
		      (CmdFunc) MEA_CLI_PortState_set,
		      "Set    state \n",
		      "\n"
		      "--------------------------------------------------------------------------------\n"
		      " state <port> <vlan> <state>\n"
		      "     <port> source port\n"
		      "     <vlan>  0-4095  (D.C relavant for STP)\n"
		      "     <state>   0-FORWARD 1-DISCARD 2-LEARN\n"
		      "     <queueid>  cluster Id                 \n"
		      "--------------------------------------------------------------------------------\n"
		      " option\n"
		      "\n"
		      "\n"
		      "--------------------------------------------------------------------------------\n"
		      "Examples: \n"
		      "         set 125 D.C 1 125\n"
		      "         set 125   1 1 126\n"
		      "         set 125   1 2 125\n" "\n");

	CLI_defineCmd("MEA port state show",
		      (CmdFunc) MEA_CLI_PortState_show,
		      "Show   state (s)\n",
		      "show <All|port> <all|from vlan> <to valn>\n"
		      "---------------------------------------------------\n"
		      "\n"
		      "\n"
		      "---------------------------------------------------\n"
		      "Examples: - \n"
		      "             show all\n"
		      "             show 125  1  10\n"
		      "             show 125  all\n" "            \n");

	return MEA_OK;
}
static MEA_Status MEA_CLI_PortState_set(int argc, char *argv[])
{

	MEA_PortState_te state;
	MEA_Port_t port;
	MEA_Uint16 vlan;
	ENET_QueueId_t QueueId_i;

	if (argc < 4) {
		CLI_print("Error: Minimum parameters is missing\n");
		return MEA_ERROR;
	}

	port = MEA_OS_atoi(argv[1]);
	
	if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE /*silent */ ) ==
	    MEA_FALSE) {
		CLI_print("Error: port %d (%s) is invalid \n", port,
			  argv[1]);
		return MEA_OK;
	}

	if (!MEA_OS_strcmp(argv[2], "D.C")) {
		vlan = MEA_PORT_STATE_VLAN_DONT_CARE;
	} else {
		vlan = MEA_OS_atoi(argv[2]);

	}

	if (MEA_OS_atoi(argv[3]) > MEA_PORT_STATE_LAST) {
		CLI_print("Error: port %d  unknown state %d\n", port,
			  argv[3]);
		return MEA_OK;
	}

	state = MEA_OS_atoi(argv[3]);

	QueueId_i = MEA_OS_atoi(argv[4]);

	if (MEA_API_Set_PortState(MEA_UNIT_0, port, vlan, state, QueueId_i)
	    != MEA_OK) {
		CLI_print
		    ("Error: MEA_API_Set_PortState fail for port %d \n",
		     port);
		return MEA_OK;
	}

	CLI_print("Done\n");
	return MEA_OK;
}

static MEA_Status MEA_CLI_PortState_show(int argc, char *argv[])
{

	MEA_PortState_te state;

	MEA_Port_t port, from_port, to_port;
	MEA_Uint32 vlan, from_vlan, to_vlan;
	char typeStr[MEA_PORT_STATE_LAST + 1][6];
	MEA_Uint32  vlan_count;

	MEA_OS_strcpy(&(typeStr[MEA_PORT_STATE_DISCARD][0]), "DSC");
	MEA_OS_strcpy(&(typeStr[MEA_PORT_STATE_LEARN][0]), "LRN");
	MEA_OS_strcpy(&(typeStr[MEA_PORT_STATE_FORWARD][0]), "FRW");
	MEA_OS_strcpy(&(typeStr[MEA_PORT_STATE_LAST][0]), "LST");

	if (argc < 2) {
		CLI_print("Error:num of parameter is wrong\n");
		return MEA_ERROR;
	}

	if (MEA_OS_strcmp(argv[1], "all") == 0) {
		from_port = 0;
		to_port = MEA_MAX_PORT_NUMBER;
		from_vlan = 0;
		to_vlan = MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID;
	} else {
		from_port = MEA_OS_atoi(argv[1]);
		to_port = from_port;
		from_vlan = 0;
		to_vlan = MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID;
	}
	if (argc == 3) {
		if (MEA_OS_strcmp(argv[2], "all") == 0) {
			from_vlan = 0;
			to_vlan = MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID;
		} else {
			if (MEA_OS_strcmp(argv[2], "D.C") == 0) {
				from_vlan = MEA_PORT_STATE_VLAN_DONT_CARE;
				to_vlan = MEA_PORT_STATE_VLAN_DONT_CARE;
			} else {
				CLI_print
				    ("Error:parameter wrong only all or D.C relevant\n");
			}
		}
	}
	if (argc == 4) {
		from_vlan = MEA_OS_atoi(argv[2]);
		to_vlan = MEA_OS_atoi(argv[3]);
		if (from_vlan > to_vlan) {
			CLI_print("Error:vlan from %d >  vlan to %d \n",
				  from_vlan, to_vlan);
			return MEA_OK;
		}
		if (to_vlan > MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID
		    || from_vlan > MEA_PORT_STATE_VLAN_MAX_VALID_VLAN_ID) {
			CLI_print("Error:vlan range ERROR wrong\n");
			return MEA_OK;
		}

	}

	if (argc > 4) {
		CLI_print("Error:num of parameter is wrong\n");
		return MEA_OK;
	}

	
	for (port = from_port; port <= to_port; port++) {

		if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE,MEA_TRUE) == MEA_FALSE) {
			continue;
		}

		CLI_print("**** The PortState port %3d ****\n", port);
		CLI_print
		    (" index 000 001 002 003 004 005 006 007 008 009\n");

		vlan_count = 0;

		for (vlan = from_vlan; vlan <= to_vlan; vlan++) {
			if (vlan_count == 0) {
				CLI_print(" %5d", vlan);
			}

			if (vlan_count == 10) {
				CLI_print("\n");
				CLI_print(" %5d", vlan);
				vlan_count = 0;
			}
			if (MEA_API_Get_PortState(MEA_UNIT_0,
						  port,
						  (MEA_Uint16) vlan,
						  &state) != MEA_OK) {
				CLI_print
				    ("ERROR:MEA_API_Get_PortState failed port= %d vlan %d",
				     port, vlan);
				return MEA_OK;
			}
			if (state >= MEA_PORT_STATE_FORWARD && state < MEA_PORT_STATE_LAST)
				CLI_print(" %3s", typeStr[state]);
			else
				CLI_print(" UNK");
			vlan_count++;
		}
		CLI_print("\n");

	}

	return MEA_OK;
}

