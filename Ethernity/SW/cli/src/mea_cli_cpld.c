/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "mea_drv_common.h"
#include "mea_cli_cpld.h"
#include "cli_eng.h"
#include "mea_cpld.h"

MEA_Status MEA_CLI_CPLD_AddDebugCmds(void)
{
#ifdef MEA_MODULE_CPLD_ON

#ifdef MEA_MODULE_BM_ON
	CLI_defineCmd("MEA debug reset CPLD BM",
		      (CmdFunc) MEA_CLI_CPLD_BM_Reset,
		      "CPLD level BM reset", "CPLD level BM reset");
#endif

#ifdef MEA_MODULE_IF_ON
	CLI_defineCmd("MEA debug reset CPLD IF",
		      (CmdFunc) MEA_CLI_CPLD_IF_Reset,
		      "CPLD level IF reset", "CPLD level IF reset");
#endif

#endif
	return MEA_OK;
}

#ifdef MEA_MODULE_BM_ON
#ifdef MEA_MODULE_CPLD_ON
MEA_Status MEA_CLI_CPLD_BM_Reset(int argc, char *argv[])
{
	MEA_CPLD_HW_ACCESS_ENABLE
	    return mea_drv_CPLD_ResetModule(MEA_MODULE_BM);
MEA_CPLD_HW_ACCESS_DISABLE}
#endif
#endif

#ifdef MEA_MODULE_CPLD_ON
MEA_Status MEA_CLI_CPLD_IF_Reset(int argc, char *argv[])
{
	MEA_CPLD_HW_ACCESS_ENABLE
	    return mea_drv_CPLD_ResetModule(MEA_MODULE_IF);
MEA_CPLD_HW_ACCESS_DISABLE}
#endif
