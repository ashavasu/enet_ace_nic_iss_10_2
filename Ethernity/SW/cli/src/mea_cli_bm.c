/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#if 1
#ifdef MEA_OS_LINUX
#ifdef MEA_OS_OC
#include <linux/unistd.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#endif
#else
#include <time.h>
#endif
#endif



#include "mea_api.h"
#include "mea_cli_bm.h"
#include "cli_eng.h"
#include "mea_bm_drv.h"
#include "mea_drv_common.h"
#include "mea_cpld.h"
#include "mea_cli.h"
#include "mea_bm_counters_drv.h"
#include "enet_shaper_profile_drv.h"


static MEA_Status MEA_CLI_Get_Counters_PM(MEA_PmId_t     first_pmId,
    MEA_PmId_t    last_pmId,
    MEA_Bool	  rate_fmt,
    MEA_Bool	  dis_only);

MEA_Status MEA_CLI_BN_AddDebugCmds(void)
{

	CLI_defineCmd("MEA debug general prompt",
		      (CmdFunc) MEA_CLI_Prompt,
		      "change CLI prompt", "prompt myPrompt");

	CLI_defineCmd("MEA debug general mode",
		      (CmdFunc) MEA_CLI_mode,
		      "change memory working mode\n",
		      " mode <md>\n"
		      "      md = 1 - regular,2-upstream,3-downstream\n"
		      "      option: if second parameter is defined the prompt will not be changed"
		      "mode 1");

	CLI_defineCmd("MEA debug reset CHIP BM",
		      (CmdFunc) MEA_CLI_CHIP_BM_Reset,
		      "CHIP level BM reset", "CHIP level BM reset");

	CLI_defineCmd("MEA debug module BM_Write",
		      (CmdFunc) MEA_CLI_BM_Write,
		      "Write <Address> with <DWORD> BM module",
		      "Write <Address> with <DWORD> BM module");

	CLI_defineCmd("MEA debug module BM_read",
		      (CmdFunc) MEA_CLI_BM_Read,
		      "Read DWORD from <Address> BM module",
		      "Read DWORD from <Address> BM module");

	CLI_defineCmd("MEA debug module bm_write_ind",
		      (CmdFunc) MEA_CLI_BM_WriteInd,
		      "Write BM indirect <tbl_addr> <tbl_offset> <param_num> <params...>",
		      "Write BM indirect <tbl_addr> <tbl_offset> <param_num> <params...>");

	CLI_defineCmd("MEA debug module bm_read_ind",
		      (CmdFunc) MEA_CLI_BM_ReadInd,
		      "Read BM indirect <tbl_addr> <tbl_offset> <numofparamters>",
		      "Read BM indirect <tbl_addr> <tbl_offset> <numofparamters>");

	CLI_defineCmd("MEA debug module bm_table",
		      (CmdFunc) MEA_CLI_DebugBM_Table,
		      "Read BM table <tbl_addr> <from_size> <to_size>",
		      "Read BM table <tbl_addr> <from_size> <to_size>");

	CLI_defineCmd("MEA debug profile pm show",
                  (CmdFunc)MEA_CLI_DebugPMShow,
                  "Show PM pool\n",
                  "show \n");



    CLI_defineCmd("MEA profile wred profile create ",
                  (CmdFunc)MEA_CLI_CreateEgressWredProfile,
                  "Create Egress WRED Drop Probability table profile",  
                  "create <acm_mode> <profile_id>\n" 
                  "Example: \n"
                  "        create 1 3\n");
    CLI_defineCmd("MEA profile wred profile modify ",
        (CmdFunc)MEA_CLI_SetEgressWredProfile,
        "Set Egress WRED Drop Probability table profile",  
        "set <acm_mode> <profile_id>\n"
        " option "
        " -ecn <valid> <min><max>\n"
        "Example: \n"
        "        set 1 3 -ecn 1 40 72\n");


    CLI_defineCmd("MEA profile wred profile delete ",
                  (CmdFunc)MEA_CLI_DeleteEgressWredProfile,
                  "Delete Egress WRED Drop Probability table profile",  
                  "Delete all|<acm_mode> [all|<profile_id>]\n" 
                  "Example: \n"
                  "        delete all\n"
                  "        delete 1 3\n"
                  "        delete 1 all\n");

    CLI_defineCmd("MEA profile wred profile show",
                  (CmdFunc)MEA_CLI_ShowEgressWredProfile,
                  "Show Egress WRED Drop Probability table profile",  
                  "show  all|<acm_mode> [all|<profile_id>]\n" 
                  "Example: \n"
                  "        show all\n"
                  "        show 1 3\n"
                  "        show 1 all\n");


	CLI_defineCmd("MEA profile wred set entry",
                  (CmdFunc)MEA_CLI_SetEgressWredEntry,
			      "Set Egress WRED Drop Probability table entry",
			      "entry <priority>  <normalize_aqs> <drop_probability>\n" 
                  "      <priority>           - 0..3/7 (depend on the device support)\n"
                  "      <normalize_aqs>      - 0..127 \n"
                  "      <drop_probability>   - 0..255 \n"
                  "option\n" 
                  "       [-f (profile id)]\n"
                  "       [-acm (acm mode)]\n"
                  "Example: \n"
                  "        entry 3 20 128 -f 0 -acm 0\n");

	CLI_defineCmd("MEA profile wred set range",
                  (CmdFunc)MEA_CLI_SetEgressWredStep,
			      "Set Egress WRED Drop Probability table entries",
			      "range <priority>  <from_normalize_aqs> <to_normalize_aqs> <drop_probability>\n" 
                  "      <priority>           - 0..3/7 (depend on the device support) \n"
                  "      <from_normalize_aqs> - 0..127 \n" 
                  "      <  to_normalize_aqs> - 0..127 \n"    
                  "      <drop_probability>   - 0..255 \n"
                  "option\n" 
                  "       [-f (profile id)]\n"
                  "       [-acm (acm mode)]\n"
                  "Example: \n"
                  "        range 3 0 254 128 -f 0 -acm 0\n");

	CLI_defineCmd("MEA profile wred set curve",
                  (CmdFunc)MEA_CLI_SetEgressWredCurve,
			      "Set Egress WRED Drop Probability table entries graph",
			      "curve <priority>  <from_normalize_aqs> <to_normalize_aqs> <from_drop_probability> <to_drop_probability>\n"
                  "      <priority>               - 0..3/7 (depend on the device support) \n"
                  "      <from_normalize_aqs>     - 0..127 \n" 
                  "      <  to_normalize_aqs>     - 0..127 \n"    
                  "      <from_drop_probability>  - 0..255 \n"
                  "      <  to_drop_probability>  - 0..255 \n"
                  "option\n" 
                  "       [-f (profile id)]\n"
                  "       [-acm (acm mode)]\n"
                  "example: \n"
                  "   curve 3 0 255 0 255 -f 0 -acm 0\n");

    CLI_defineCmd("MEA profile wred show entry",
                  (CmdFunc)MEA_CLI_ShowEgressWred,
                  "Show egress wred table",
                  "entry <pri>|all [-f <profile id>] [-acm <acm mode>]\n"
                  "------------------------------------------------------------\n"
                  "      <priority> - 0..3/7 | all (depend on the device support) \n"
                  "option\n" 
                  "       [-f (profile id)]\n"
                  "       [-acm (acm mode)]\n"
                  "------------------------------------------------------------\n"
                  "example: \n"
                  "entry all \n"
                  "entry 3   \n"
                  "entry all -f 0 -acm 0 \n");




    CLI_defineCmd("MEA port egress show",
                  (CmdFunc)MEA_CLI_ShowEgressPortEntry,
			      "Show egress port ",
                  "show <port>|all -b | -s | -my_mac"
                  "\n"
                  "------------------------------\n"
                  "   <port> : port number OR all\n"
                  "       -b          : basic configuration\n"
                  "       -s          : shaper information \n"
                  "       -my_mac     : MAC address \n"
                  "       -TEID_stamp_en :    stamping teid to vlan\n"
                  "       -pro_1p1   : 1p1 information\n"
                  "       -port_state : state of egress \n"
                  
                  "-------------------------------\n"
                  "Examples: - show all -b \n"
                  "          - show all -s \n"
                  "          - show all -my_mac \n"
                  "            show all -lb \n"
                  "            show all -pro_1p1   \n"
                  "            show all -g999_frg_sch   \n"
                  );


    CLI_defineCmd("MEA counters cluster collect",
                  (CmdFunc)MEA_CLI_Collect_Counters_Queue,
                  "Collect cluster Counters from HW",
                  "collect <cluster>|all\n"
                  "        <cluster> - cluster Number\n"
                  "Examples: - collect 0  \n" 
                  "          - collect all\n");

    CLI_defineCmd("MEA counters cluster clear",
                  (CmdFunc)MEA_CLI_Clear_Counters_Queue,
                  "Clear   Queue Counters",
                  "clear <cluster>|all\n"
                  "      <cluster> - cluster number\n"
                  "Examples: - clear 0  \n" 
                  "          - clear all\n");

    
    
        CLI_defineCmd("MEA counters clusterPortRate show",
        (CmdFunc)MEA_CLI_Show_clusters_portRate,
            "Show clusterPortRate show",
            "show <port>|all  ]\n"

            "     <port> - port number\n"
            "Examples: - show clusterPortRate 105  -len 1000\n"
            
            "          - show all\n");



    
    
    CLI_defineCmd("MEA counters cluster show",
                  (CmdFunc)MEA_CLI_Show_Counters_Queue,
                  "Show    cluster Counters",
		      "show <cluster>|all  ]\n"

		      "     <cluster> - cluster number\n"
		      "Examples: - show 0  \n" "          - show all\n");

	
    CLI_defineCmd("MEA counters PM blk ",
        (CmdFunc)MEA_CLI_Collect_Counters_PM_blk,
        "Collect PM Counters from HW",
        "collect <blk>\n"
        "        <blk> -  \n"
        "Examples: - blk 0  \n"
        "          - blk 1 \n");


	CLI_defineCmd("MEA counters PM BLKshow ",
		(CmdFunc)MEA_CLI_Collect_Counters_PM_blkShow,
		"Collect PM Counters from HW",
		"collect <blk>\n"
		"        <blk> -  \n"
		"Examples: - BLKshow 0  \n"
		"          - BLKshow 1 \n");
    
    
    CLI_defineCmd("MEA counters PM collect",
		      (CmdFunc) MEA_CLI_Collect_Counters_PM,
		      "Collect PM Counters from HW",
		      "collect <pmId>|all\n"
		      "        <pmId> - PM Id OR all \n"
		      "Examples: - collect 1  \n"
		      "          - collect all\n");





	CLI_defineCmd("MEA counters PM clear",
		      (CmdFunc) MEA_CLI_Clear_Counters_PM,
		      "Clear   PM Counters",
		      "clear <pmId>|all\n"
		      "      <pmId> - PM Id OR all \n"
		      "Examples: - clear 1  \n" "          - clear all\n");

	CLI_defineCmd("MEA counters PM show",
		      (CmdFunc) MEA_CLI_Show_Counters_PM,
		      "Show    PM Counters",

		      "show all|<from pmId> [<to_pmId>]\n"

              "     <from pmId> - From pmId Id OR all [-dis -rate]\n"
		      "     <to   pmId> - To   pmId Id \n"
		      "Examples: - show 1  \n"
		      "          - show 10 15\n" "          - show all\n");
    
    
        CLI_defineCmd("MEA counters PM exist",
        (CmdFunc)MEA_CLI_Show_Counters_PM_exist,
            "Show    PM exist",

            "show all|<from pmId> [<to_pmId>]\n"

            "     <from pmId> - From pmId Id OR all [-dis -rate]\n"
            "     <to   pmId> - To   pmId Id \n"
            "Examples: - show 1  \n"
            "          - show 10 15\n" "          - show all\n");



    
    CLI_defineCmd("MEA counters PM showType",
        (CmdFunc) MEA_CLI_Show_Counters_PMType,
        "Show    PM Counters",

        "show pmId \n"

        "     <pmId> -  [-rate]\n"
       
        "Examples: - show 1  \n"
        "          - show 10 \n" 
        );


	CLI_defineCmd("MEA counters BM collect",
		      (CmdFunc) MEA_CLI_Collect_Counters_BM,
		      "Collect BM Counters from HW",
		      "collect \n" "Example: collect \n");

	CLI_defineCmd("MEA counters BM clear",
		      (CmdFunc) MEA_CLI_Clear_Counters_BM,
		      "Clear   BM Counters",
		      "clear \n" "Example: clear\n");

	CLI_defineCmd("MEA counters BM show",
		      (CmdFunc) MEA_CLI_Show_Counters_BM,
		      "Show    BM Counters", "show \n" "Example: show \n");

	return MEA_OK;
}

MEA_Status MEA_CLI_CHIP_BM_Reset(int argc, char *argv[])
{

	mea_drv_CPLD_ResetModule(MEA_MODULE_BM);
	return MEA_OK;

}

MEA_Status MEA_CLI_BM_Write(int argc, char *argv[])
{

	if (argc != 3) {
		return MEA_ERROR;
	}

	MEA_BM_HW_ACCESS_ENABLE
	    MEA_API_WriteReg(MEA_UNIT_0, MEA_OS_atohex(argv[1]),
			     MEA_OS_atohex(argv[2]), MEA_MODULE_BM);

	MEA_BM_HW_ACCESS_DISABLE return MEA_OK;
}

MEA_Status MEA_CLI_BM_Read(int argc, char *argv[])
{

	if (argc != 2) {
		return MEA_ERROR;
	}

	MEA_BM_HW_ACCESS_ENABLE
	    CLI_print("Addr 0x%08x == 0x%08x BM module\n",
		      MEA_OS_atohex(argv[1]),
		      MEA_API_ReadReg(MEA_UNIT_0, MEA_OS_atohex(argv[1]),
				      MEA_MODULE_BM));

	MEA_BM_HW_ACCESS_DISABLE 
        
  return MEA_OK;
}

static void mea_cli_phy_bm_WriteInd(MEA_Uint32 param_num,
				    MEA_Uint32 * param)
{
	MEA_Uint32 i;

	for (i = 0; i < param_num; i++) {
		MEA_API_WriteReg(MEA_UNIT_0, MEA_BM_IA_WRDATA0 + i * 4,
				 param[i], MEA_MODULE_BM);
	}

}

MEA_Status MEA_CLI_BM_WriteInd(int argc, char *argv[])
{
	MEA_Uint32 ind_params[ENET_BM_IA_MAX_NUM_OF_WRITE_REGS];
	MEA_ind_write_t ind_write;
	MEA_Uint32 i;
	MEA_Uint32 NumofRegs;

	if (argc < 4) {
		CLI_print("ERROR minimum 5 parameter\n");
		return MEA_ERROR;
	}

	ind_write.tableType = MEA_OS_atoi(argv[1]);
	ind_write.tableOffset = MEA_OS_atoi(argv[2]);

	NumofRegs = MEA_OS_atoi(argv[3]);
	if (NumofRegs > ENET_BM_IA_MAX_NUM_OF_WRITE_REGS) {
		CLI_print
		    ("BM indirect table ERROR num of regs %d > MAX of regs is %d\n",
		     NumofRegs, ENET_BM_IA_MAX_NUM_OF_WRITE_REGS);
		return MEA_ERROR;
	}

	ind_write.cmdReg = MEA_BM_IA_CMD;
	ind_write.cmdMask = MEA_BM_IA_CMD_MASK;
	ind_write.statusReg = MEA_BM_IA_STAT;
	ind_write.statusMask = MEA_BM_IA_STAT_MASK;
	ind_write.tableAddrReg = MEA_BM_IA_ADDR;
	ind_write.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
    ind_write.funcParam1 = (MEA_funcParam)((long)NumofRegs);	//number of parameters to write
    ind_write.funcParam2 = (MEA_funcParam)& ind_params;

	if (!(4 + NumofRegs == (MEA_Uint32) argc)) {
		CLI_print("BM indirect table ERROR num of parameter\n");
		return MEA_OK;
	}
	for (i = 0; i < NumofRegs; i++) {
		ind_params[i] = MEA_OS_atohex(argv[4 + i]);
	}

	ind_write.writeEntry = (MEA_FUNCPTR) mea_cli_phy_bm_WriteInd;

	MEA_BM_HW_ACCESS_ENABLE
	    if (MEA_API_WriteIndirect
		(MEA_UNIT_0, &ind_write, MEA_MODULE_BM) == MEA_ERROR) {
		CLI_print("ERROR BM MEA_API_WriteIndirect\n");
		return MEA_ERROR;
	}

	MEA_BM_HW_ACCESS_DISABLE 
    
    CLI_print("Done\n");
	return MEA_OK;
}

MEA_Status MEA_CLI_BM_ReadInd(int argc, char *argv[])
{
	MEA_ind_read_t ind_read;
	MEA_Uint32 read_data[ENET_BM_IA_MAX_NUM_OF_READ_REGS];
	MEA_Uint32 NumofRegs, i;

	if (argc < 3) {
		return MEA_ERROR;
	}

	ind_read.tableType = MEA_OS_atoi(argv[1]);
	ind_read.tableOffset = MEA_OS_atoi(argv[2]);
	NumofRegs = MEA_OS_atoi(argv[3]);
	if (NumofRegs > ENET_BM_IA_MAX_NUM_OF_READ_REGS) {
		CLI_print
		    ("BM indirect table ERROR num of regs %d > MAX of reg is %d\n",
		     NumofRegs, ENET_BM_IA_MAX_NUM_OF_READ_REGS);
		return MEA_OK;
	}

	ind_read.cmdReg = MEA_BM_IA_CMD;
	ind_read.cmdMask = MEA_BM_IA_CMD_MASK;
	ind_read.statusReg = MEA_BM_IA_STAT;
	ind_read.statusMask = MEA_BM_IA_STAT_MASK;
	ind_read.tableAddrReg = MEA_BM_IA_ADDR;
	ind_read.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
	ind_read.read_data = read_data;

	MEA_BM_HW_ACCESS_ENABLE
	    if (MEA_API_ReadIndirect(MEA_UNIT_0, &ind_read,
				     NumofRegs,
				     MEA_MODULE_BM) == MEA_ERROR) {
          MEA_BM_HW_ACCESS_DISABLE
		return MEA_ERROR;
	}
	MEA_BM_HW_ACCESS_DISABLE
	    CLI_print("BM indirect table 0x%x at offset %d:\n",
		      MEA_OS_atoi(argv[1]), MEA_OS_atoi(argv[2]));
	for (i = 0; i < NumofRegs; i++) {
		CLI_print("Dword[%d] == 0x%x \n", i, read_data[i]);
	}
	CLI_print("\n");

	return MEA_OK;
}


MEA_Status MEA_CLI_CreateEgressWredProfile(int argc, char *argv[])
{
    MEA_WRED_Profile_Key_dbt  key;

    if (argc!=3) {
       return MEA_ERROR;
    }

    MEA_OS_memset(&key,0,sizeof(key));
    key.acm_mode     = MEA_OS_atoi(argv[1]);
    key.profile_id   = MEA_OS_atoi(argv[2]);
    
    
    
    if (MEA_API_Create_WRED_Profile(MEA_UNIT_0,&key,NULL) != MEA_OK) {
        CLI_print("Error: MEA_API_Create_WRED_Profile failed\n");
        return MEA_OK;
    }

    return MEA_OK;

}






MEA_Status MEA_CLI_SetEgressWredProfile(int argc, char *argv[])
{
    MEA_WRED_Profile_Key_dbt  key;
    MEA_WRED_Profile_Data_dbt  data_o;
    int i;

    if (argc < 3) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&key,0,sizeof(key));
    key.acm_mode     = MEA_OS_atoi(argv[1]);
    key.profile_id   = MEA_OS_atoi(argv[2]);

    if (MEA_API_Get_WRED_Profile(MEA_UNIT_0,&key,&data_o) != MEA_OK) {
        CLI_print("Error: MEA_API_Create_WRED_Profile failed\n");
        return MEA_OK;
    }
    for(i=3; i<argc ;i++){

        if(!MEA_OS_strcmp(argv[i],"-ecn")){ 
            if(i + 3 >=  argc ){
                CLI_print("Error: miss parameter of -ecn \n");
                return MEA_OK;
            }
            data_o.ECN_data.ECN_enable = MEA_OS_atoi(argv[i+1]);
            data_o.ECN_data.ECN_minTH = MEA_OS_atoi(argv[i+2]);
            data_o.ECN_data.ECN_maxTH = MEA_OS_atoi(argv[i+3]);
        }
    }




    if (MEA_API_Set_WRED_Profile(MEA_UNIT_0,&key,&data_o) != MEA_OK) {
        CLI_print("Error: MEA_API_Set_WRED_Profile failed\n");
        return MEA_OK;
    }

    return MEA_OK;

}







MEA_Status MEA_CLI_DeleteEgressWredProfile(int argc, char *argv[])
{
    MEA_WRED_Profile_Key_dbt  key;
    MEA_AcmMode_t             acm_mode_from,acm_mode_to;
    MEA_WredProfileId_t       profile_id_from,profile_id_to;

    if ((argc!=2) && (argc!=3)) {
       return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all")==0) {
        acm_mode_from=0;
        acm_mode_to=MEA_WRED_NUM_OF_ACM_MODE-1;
    } else {
        acm_mode_from = MEA_OS_atoi(argv[1]);
        acm_mode_to   = acm_mode_from;
    } 
    if ((argc!=3) ||
        (MEA_OS_strcmp(argv[2],"all")==0)) {
        profile_id_from=1;
        profile_id_to=MEA_WRED_NUM_OF_PROFILES-1;
    } else {
        profile_id_from = MEA_OS_atoi(argv[2]);
        profile_id_to   = profile_id_from;
    }

    if ((acm_mode_from == acm_mode_to) && (profile_id_from == profile_id_to)) {
        MEA_OS_memset(&key,0,sizeof(key));
        key.acm_mode = acm_mode_from;
        key.profile_id = profile_id_from;
         if ((key.acm_mode == 0) && (key.profile_id == 0)) {
             CLI_print("Error: Not allowed to delete profile 0/0 \n");
             return MEA_OK;
         }
        if (!MEA_API_IsValid_WRED_Profile(MEA_UNIT_0,&key,MEA_FALSE)) {
            CLI_print("Error: Profile %d/%d not exist/valid \n");
            return MEA_OK;
        }
    }

    MEA_OS_memset(&key,0,sizeof(key));
    for (key.acm_mode=acm_mode_from;key.acm_mode<=acm_mode_to;key.acm_mode++) {
        for (key.profile_id=profile_id_from;key.profile_id<=profile_id_to;key.profile_id++) {
             if ((key.acm_mode == 0) && (key.profile_id == 0)) {
                 continue;
             }
            if (!MEA_API_IsValid_WRED_Profile(MEA_UNIT_0,&key,MEA_TRUE)) {
                continue;
            }
           if (MEA_API_Delete_WRED_Profile(MEA_UNIT_0,&key) != MEA_OK) {
                CLI_print("Error: MEA_API_Delete_WRED_Profile failed\n");
                return MEA_OK;
            }
        }
    }

    return MEA_OK;

}

MEA_Status MEA_CLI_ShowEgressWredProfile(int argc, char *argv[])
{
    MEA_WRED_Profile_Key_dbt  key;
    MEA_Bool                  found;
    MEA_WRED_Profile_Data_dbt data_o;
    MEA_AcmMode_t             acm_mode_from,acm_mode_to;
    MEA_WredProfileId_t       profile_id_from,profile_id_to;
    MEA_Uint32 count;

    if ((argc!=2) && (argc!=3)) {
       return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all")==0) {
        acm_mode_from=0;
        acm_mode_to=MEA_WRED_NUM_OF_ACM_MODE-1;
    } else {
        acm_mode_from = MEA_OS_atoi(argv[1]);
        acm_mode_to   = acm_mode_from;
    } 
    if ((argc!=3) ||
        (MEA_OS_strcmp(argv[2],"all")==0)) {
        profile_id_from=0;
        profile_id_to=MEA_WRED_NUM_OF_PROFILES-1;
    } else {
        profile_id_from = MEA_OS_atoi(argv[2]);
        profile_id_to   = profile_id_from;
    }

    if (MEA_API_GetFirst_WRED_Profile(MEA_UNIT_0,&key,&data_o,&found) != MEA_OK) {
        CLI_print("Error: MEA_API_GetFirst_WRED_Profile failed\n");
        return MEA_OK;
    }
    count=0;
    while (found) {
        if ((key.acm_mode   >= acm_mode_from  ) &&
            (key.acm_mode   <= acm_mode_to    ) &&
            (key.profile_id >= profile_id_from) &&
            (key.profile_id <= profile_id_to  )  ) {
            if (count++ == 0) {
                CLI_print("Acm  Profile ECN  \n"
                          "Mode Id      valid maxTH minTH\n"
                          "---- ------- ----- ----- -----\n");
            }
            

            CLI_print("%4d %7d ",key.acm_mode,key.profile_id);
            CLI_print("%5s %5d %5d ",MEA_STATUS_STR(data_o.ECN_data.ECN_enable),data_o.ECN_data.ECN_maxTH,data_o.ECN_data.ECN_minTH);
            CLI_print("\n");
        }
        if (MEA_API_GetNext_WRED_Profile(MEA_UNIT_0,&key,&data_o,&found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetNext_WRED_Profile failed\n");
            return MEA_OK;
        }
    }


    return MEA_OK;

}



MEA_Status MEA_CLI_SetEgressWredStep(int argc, char *argv[])
{
    MEA_WRED_Entry_Data_dbt data;
    MEA_WRED_Entry_Key_dbt  key_from;
    MEA_WRED_Entry_Key_dbt  key_to;
    MEA_Uint32 i;

    if (argc<5) {
       return MEA_ERROR;
    }


    MEA_OS_memset(&key_from,0,sizeof(key_from));
    key_from.profile_key.acm_mode   = 0;
    key_from.profile_key.profile_id = 0;
    key_from.priority               = MEA_OS_atoi(argv[1]),
    key_from.normalize_aqs          = MEA_OS_atoi(argv[2]);

    MEA_OS_memcpy(&key_to,&key_from,sizeof(key_from));
    key_to.normalize_aqs            = MEA_OS_atoi(argv[3]);

    MEA_OS_memset(&data,0,sizeof(data));
    data.drop_probability           = MEA_OS_atoi(argv[4]);


    
    for (i=5; i < (MEA_Uint32)argc; i++) {
        if(!strcmp(argv[i],"-f")){
            if (i +1 > (MEA_Uint32) argc){
                CLI_print("Error  %s missing parameters\n",argv[i][0]);
                return MEA_ERROR;
            } 
            key_from.profile_key.profile_id = MEA_OS_atoi(argv[i+1]);
            key_to.profile_key.profile_id   = key_from.profile_key.profile_id;
        }
        if(!strcmp(argv[i],"-acm")){
            if (i +1 > (MEA_Uint32) argc){
                CLI_print("Error  %s missing parameters\n",argv[i][0]);
                return MEA_ERROR;
            } 
            key_from.profile_key.acm_mode = MEA_OS_atoi(argv[i+1]);
            key_to.profile_key.acm_mode   = key_from.profile_key.acm_mode;
        }

    }

    if (MEA_API_Set_WRED_EntryRange(MEA_UNIT_0,
                                    &key_from,
                                    &key_to,
                                    &data) != MEA_OK) {
        CLI_print("Error: MEA_API_Set_WRED_EntryRange failed\n");
        return MEA_ERROR;
    }

    return MEA_OK;


}


MEA_Status MEA_CLI_SetEgressWredEntry(int argc, char *argv[])
{
    MEA_WRED_Entry_Data_dbt data;
    MEA_WRED_Entry_Key_dbt  key;
    MEA_Uint32 i;

	if (argc<4) {
       return MEA_ERROR;
    }

    MEA_OS_memset(&key,0,sizeof(key));
    key.profile_key.acm_mode   = 0;
    key.profile_key.profile_id = 0;
    key.priority               = MEA_OS_atoi(argv[1]),
    key.normalize_aqs          = MEA_OS_atoi(argv[2]);

    MEA_OS_memset(&data,0,sizeof(data));
    data.drop_probability      = MEA_OS_atoi(argv[3]);
    
    for (i=4; i < (MEA_Uint32)argc; i++) {
        if(!strcmp(argv[i],"-f")){
            if (i +1 > (MEA_Uint32) argc){
                CLI_print("Error  %s missing parameters\n",argv[i][0]);
                return MEA_ERROR;
            } 
            key.profile_key.profile_id = MEA_OS_atoi(argv[i+1]);
        }
        if(!strcmp(argv[i],"-acm")){
            if (i +1 > (MEA_Uint32) argc){
                CLI_print("Error  %s missing parameters\n",argv[i][0]);
                return MEA_ERROR;
            } 
            key.profile_key.acm_mode = MEA_OS_atoi(argv[i+1]);
        }

    }


    if (MEA_API_Set_WRED_Entry(MEA_UNIT_0,
                               &key,
                               &data) != MEA_OK) {
        CLI_print("Error: MEA_API_Set_WRED_Entry failed\n");
        return MEA_ERROR;
    }

    return MEA_OK;

}


MEA_Status MEA_CLI_SetEgressWredCurve(int argc, char *argv[])
{
    MEA_WRED_Entry_Data_dbt data_from;
    MEA_WRED_Entry_Data_dbt data_to;
    MEA_WRED_Entry_Key_dbt  key_from;
    MEA_WRED_Entry_Key_dbt  key_to;
    MEA_Uint32 i;

	if (argc<6) {
       return MEA_ERROR;
    }

    MEA_OS_memset(&key_from,0,sizeof(key_from));
    key_from.profile_key.acm_mode   = 0;
    key_from.profile_key.profile_id = 0;
    key_from.priority               = MEA_OS_atoi(argv[1]),
    key_from.normalize_aqs          = MEA_OS_atoi(argv[2]);

    MEA_OS_memcpy(&key_to,&key_from,sizeof(key_to));
    key_to.normalize_aqs            = MEA_OS_atoi(argv[3]);

    MEA_OS_memset(&data_from,0,sizeof(data_from));
    data_from.drop_probability     = MEA_OS_atoi(argv[4]);

    MEA_OS_memset(&data_to,0,sizeof(data_to));
    data_to.drop_probability       = MEA_OS_atoi(argv[5]);

    for (i=4; i < (MEA_Uint32)argc; i++) {
        if(!strcmp(argv[i],"-f")){
            if (i +1 > (MEA_Uint32) argc){
                CLI_print("Error  %s missing parameters\n",argv[i][0]);
                return MEA_ERROR;
            } 
            key_from.profile_key.profile_id = MEA_OS_atoi(argv[i+1]);
            key_to.profile_key.profile_id   = key_from.profile_key.profile_id;
        }
        if(!strcmp(argv[i],"-acm")){
            if (i +1 > (MEA_Uint32) argc){
                CLI_print("Error  %s missing parameters\n",argv[i][0]);
                return MEA_ERROR;
            } 
            key_from.profile_key.acm_mode = MEA_OS_atoi(argv[i+1]);
            key_to.profile_key.acm_mode   = key_from.profile_key.acm_mode;
        }

    }


    if (MEA_API_Set_WRED_EntryCurve(MEA_UNIT_0,
                                       &key_from,
                                       &key_to,
                                       &data_from,
                                       &data_to) != MEA_OK) {
        CLI_print("Error: MEA_API_Set_WRED_EntryCurve failed\n");
        return MEA_ERROR;
    }

    return MEA_OK;
}


MEA_Status MEA_CLI_Prompt(int argc, char *argv[])
{
    if ((argc !=2)) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR; 
    } 

	CLI_setPrompt(argv[1]);
	return MEA_OK;
}


MEA_Status MEA_CLI_mode(int argc, char *argv[])
{
	mea_memory_mode_e mode;
	MEA_Bool prompt=MEA_TRUE;
    if ((argc !=2)&&(argc !=3)) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR; 
    } 

	if(argc == 3){
        CLI_print("change in memory mode without changing prompt\n");
		prompt=MEA_FALSE;
	}

	mode = (mea_memory_mode_e)MEA_OS_atoi(argv[1]);



	if (MEA_CLI_set_memory_mode(mode,prompt)!=MEA_OK){
       CLI_print("error in MEA_CLI_set_memory_mode\n");
	}
	return MEA_OK;
}

MEA_Status MEA_CLI_Show_Counters_PMType(int argc, char *argv[])
{


    int                      i;
    //    int                      num_of_count=0;
    MEA_PmId_t               pmId;
    MEA_PmId_t               first_pmId;
    MEA_PmId_t               last_pmId;
    MEA_Counters_PM_dbt entry,total_entry;
    MEA_Bool pmId_exist;
    
    //MEA_Counters_PM_Bonding_dbt Bonding_entry;
    //    MEA_Uint16 groupId;

    MEA_Bool rate_fmt = MEA_FALSE;
    MEA_Bool tr_only =MEA_FALSE;

    char buf[500];
    char *up_ptr;


    if ((argc < 2) ) {

        CLI_print("Invalid number of parameters\n");
        return MEA_ERROR;
    }

    if (strcmp(argv[1], "all") != 0) {

        //first_pmId = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_pmId = MEA_OS_atoi(buf);
            last_pmId   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            pmId=first_pmId = last_pmId = MEA_OS_atoi(buf);
        }

    } else {


        first_pmId = 0;
        last_pmId = MEA_MAX_NUM_OF_PM_ID - 1;
        
    }

    for(i=2; i <argc ;i++){
        if(!strcmp(argv[i], "-rate")) {
            rate_fmt = MEA_TRUE;
        }
        if(!strcmp(argv[i], "-tr")) {
            tr_only = MEA_TRUE;
        }

    }



    MEA_OS_memset(&total_entry, 0, sizeof(total_entry));
    CLI_print("\n");
    
    



    for (pmId = first_pmId; pmId <= last_pmId; pmId++) {

        /* Check valid last_pmId */
        if (MEA_API_IsExist_PmId(MEA_UNIT_0,
            pmId,
            &pmId_exist, NULL) != MEA_OK) {
                CLI_print
                    ("%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
                    __FUNCTION__, pmId);
                return MEA_OK;
        }

        if (pmId_exist == MEA_FALSE) {
            continue;
        }
        MEA_OS_memset(&entry, 0, sizeof(entry));
        if (MEA_API_Get_Counters_PM(MEA_UNIT_0,
            pmId, &entry) != MEA_OK) {
                CLI_print
                    ("%s - Get Counters PM for pmId %d failed\n",
                    __FUNCTION__, pmId);
                return MEA_ERROR;
        }

            if(rate_fmt){
                CLI_print("PmId= %4d %s green= %llu yellow = %llu drop= %llu\n",pmId,"Rate(bps)",
                    entry.rate_green_fwd_bytes.val *8, 
                    entry.rate_yellow_fwd_bytes.val*8,
                    entry.rate_yellow_or_red_dis_bytes.val*8);

                
            }
            if(tr_only == MEA_TRUE){
                CLI_print("PmId= %4d %s %llu \n",pmId, "TR",entry.rate_green_fwd_bytes.val*8 + entry.rate_yellow_fwd_bytes.val*8);
            }
            

        }// pmId

    return MEA_OK;

}



MEA_Status MEA_CLI_Show_Counters_PM(int argc, char *argv[])
{


    int                      i;
    //int count, count_discard,
//    int                      num_of_count=0;
    MEA_PmId_t               pmId;
    MEA_PmId_t               first_pmId;
    MEA_PmId_t               last_pmId;
    //MEA_Counters_PM_dbt entry, total_entry;
    MEA_Bool pmId_exist;
   
//MEA_Counters_PM_Bonding_dbt Bonding_entry;
//    MEA_Uint16 groupId;

	MEA_Bool rate_fmt = MEA_FALSE;
    MEA_Bool dis_only =MEA_FALSE;

   
	
	char buf[500];
	char *up_ptr;


	if ((argc < 2) ) {

		CLI_print("Invalid number of parameters\n");
		return MEA_ERROR;
	}

	if (strcmp(argv[1], "all") != 0) {

		//first_pmId = MEA_OS_atoi(argv[1]);
		MEA_OS_strcpy(buf, argv[1]);
		if((up_ptr=strchr(buf, ':')) != 0)
		{
			(*up_ptr) = 0;
			up_ptr++;
			first_pmId = MEA_OS_atoi(buf);
			last_pmId   = MEA_OS_atoi(up_ptr);
		}
		else
		{
			pmId=first_pmId = last_pmId = MEA_OS_atoi(buf);
            if (MEA_API_IsExist_PmId(MEA_UNIT_0,
                pmId,
                &pmId_exist, NULL) != MEA_OK) {
                CLI_print
                    ("%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
                        __FUNCTION__, pmId);
                return MEA_OK;
            }
		}

	} else {


		first_pmId = 0;
		last_pmId = MEA_MAX_NUM_OF_PM_ID - 1;
        
	}

    for(i=2; i <argc ;i++){
        if(!strcmp(argv[i], "-rate")) {
            rate_fmt = MEA_TRUE;
        }
        if(!strcmp(argv[i], "-dis")) {
            dis_only = MEA_TRUE;
        }
        

    }



   return MEA_CLI_Get_Counters_PM(first_pmId, last_pmId, rate_fmt, dis_only);




}


static MEA_Status MEA_CLI_Get_Counters_PM(MEA_PmId_t     first_pmId,
									MEA_PmId_t    last_pmId,
									MEA_Bool	  rate_fmt,
									MEA_Bool	  dis_only)
{
	


	int                      count, count_discard;
	//    int                      num_of_count=0;
	MEA_PmId_t               pmId;
	
	MEA_Counters_PM_dbt entry, total_entry;
	MEA_Bool pmId_exist;





	MEA_OS_memset(&total_entry, 0, sizeof(total_entry));
	CLI_print("\n");
	count = 0;
	count_discard = 0;



	for (pmId = first_pmId; pmId <= last_pmId; pmId++) {

		/* Check valid last_pmId */
		if (MEA_API_IsExist_PmId(MEA_UNIT_0,
			pmId,
			&pmId_exist, NULL) != MEA_OK) {
			CLI_print
				("%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
				__FUNCTION__, pmId);
			return MEA_OK;
		}

		if (pmId_exist == MEA_FALSE) {
			continue;
		}
#if 0		
			if (count == 0 || (count > 0 && (count % 10 == 0))) {
				CLI_print("%4s %16s %16s %16s %16s %16s %16s %16s\n",
					"pmid", "FwdGreen", "FwdYellow", "DisGreen", "DisYellow", "DisRed", "DisOther", "DisMtu");
				CLI_print("---- ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------\n");
			}
#endif	
		

		count++;


		MEA_OS_memset(&entry, 0, sizeof(entry));
		if (MEA_API_Get_Counters_PM(MEA_UNIT_0,
			pmId, &entry) != MEA_OK) {
			CLI_print
				("%s - Get Counters PM for pmId %d failed\n",
				__FUNCTION__, pmId);
			return MEA_ERROR;
		}




		if (dis_only == MEA_FALSE){
            if (
                entry.green_fwd_pkts.val     != 0 ||
                entry.yellow_fwd_pkts.val    != 0 ||
                entry.green_dis_pkts.val     != 0 ||
                entry.yellow_dis_pkts.val    != 0 ||
                entry.red_dis_pkts.val       != 0 ||
                entry.other_dis_pkts.val     != 0 ||
                entry.dis_mtu_pkts.val       != 0 ||
                entry.green_fwd_bytes.val    != 0 ||
                entry.yellow_fwd_bytes.val   != 0 ||
                entry.green_dis_bytes.val    != 0 ||
                entry.yellow_or_red_dis_bytes.val !=0

                ) {
                CLI_print("------ ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------\n");
                CLI_print("%6lu %16s %16s %16s %16s %16s %16s %16s\n",
                    pmId, "FwdGreen", "FwdYellow", "DisGreen", "DisYellow", "DisRed", "DisOther", "DisMtu");
               
                //CLI_print("%4lu \n", pmId);
                CLI_print("%6s %16llu %16llu %16llu %16llu %16llu %16llu %16llu\n",
                    " Pkt",
                    entry.green_fwd_pkts.val,
                    entry.yellow_fwd_pkts.val,
                    entry.green_dis_pkts.val,
                    entry.yellow_dis_pkts.val,
                    entry.red_dis_pkts.val,
                    entry.other_dis_pkts.val,
                    entry.dis_mtu_pkts.val);
                CLI_print("%6s %16llu %16llu %16llu %16llu %16s %16s %16s\n",
                    "Byte",
                    entry.green_fwd_bytes.val,
                    entry.yellow_fwd_bytes.val,
                    entry.green_dis_bytes.val,
                    entry.yellow_or_red_dis_bytes.val, "NA", "NA", "NA");

                if (rate_fmt) {
                    CLI_print("Rate\n");
                    CLI_print("%4s %16llu %16llu %16llu %16llu %16llu %16s\n", " Pkt", entry.rate_green_fwd_pkts.val
                        , entry.rate_yellow_fwd_pkts.val
                        , entry.rate_green_dis_pkts.val
                        , entry.rate_yellow_dis_pkts.val
                        , entry.rate_red_dis_pkts.val, "");
                    CLI_print("%4s %16llu %16llu %16s %16llu %16s\n", " bps", entry.rate_green_fwd_bytes.val * 8
                        , entry.rate_yellow_fwd_bytes.val * 8
                        , ""
                        , entry.rate_yellow_or_red_dis_bytes.val * 8, "");

                    CLI_print("%6s %16llu \n", "TR", entry.rate_green_fwd_bytes.val * 8 + entry.rate_yellow_fwd_bytes.val * 8);
                }
               // CLI_print("------ ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------\n");
            }
		}
		else {
			if (entry.other_dis_pkts.val  != 0 ||
				entry.red_dis_pkts.val    != 0 ||
				entry.yellow_dis_pkts.val != 0 ||
				entry.green_dis_pkts.val  != 0 ||
				entry.dis_mtu_pkts.val    != 0 ||
                entry.green_dis_bytes.val != 0 
				){
				count_discard++;
                CLI_print("------ ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------\n");
                CLI_print("%6lu %16s %16s %16s %16s %16s %16s %16s\n",
                    pmId, "FwdGreen", "FwdYellow", "DisGreen", "DisYellow", "DisRed", "DisOther", "DisMtu");
                
				//CLI_print("%4lu \n", pmId);
				CLI_print("%4s %16llu %16llu %16llu %16llu %16llu %16llu %16llu\n",
					" Pkt",
					entry.green_fwd_pkts.val,
					entry.yellow_fwd_pkts.val,
					entry.green_dis_pkts.val,
					entry.yellow_dis_pkts.val,
					entry.red_dis_pkts.val,
					entry.other_dis_pkts.val,
					entry.dis_mtu_pkts);
				CLI_print("%4s %16llu %16llu %16llu %16llu %16s %16s %16s\n",
					"Byte",
					entry.green_fwd_bytes.val,
					entry.yellow_fwd_bytes.val,
					entry.green_dis_bytes.val,
					entry.yellow_or_red_dis_bytes.val, "NA", "NA", "NA");
				CLI_print("\n");
				//CLI_print("------ ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------\n");
			}

		}

		MEA_OS_SUM_COUNTER(total_entry.green_fwd_bytes,
			entry.green_fwd_bytes);
		MEA_OS_SUM_COUNTER(total_entry.yellow_fwd_bytes,
			entry.yellow_fwd_bytes);
		MEA_OS_SUM_COUNTER(total_entry.green_dis_bytes,
			entry.green_dis_bytes);
		MEA_OS_SUM_COUNTER(total_entry.yellow_or_red_dis_bytes,
			entry.yellow_or_red_dis_bytes);
		MEA_OS_SUM_COUNTER(total_entry.green_fwd_pkts,
			entry.green_fwd_pkts);
		MEA_OS_SUM_COUNTER(total_entry.yellow_fwd_pkts,
			entry.yellow_fwd_pkts);
		MEA_OS_SUM_COUNTER(total_entry.green_dis_pkts,
			entry.green_dis_pkts);
		MEA_OS_SUM_COUNTER(total_entry.yellow_dis_pkts,
			entry.yellow_dis_pkts);
		MEA_OS_SUM_COUNTER(total_entry.red_dis_pkts,
			entry.red_dis_pkts);
		MEA_OS_SUM_COUNTER(total_entry.other_dis_pkts,
			entry.other_dis_pkts);
		MEA_OS_SUM_COUNTER(total_entry.dis_mtu_pkts,
			entry.dis_mtu_pkts);


		

	}			/* for(serviceId=first_serviceId;serviceId<=last_serviceId;serviceId++) */

    if (last_pmId == first_pmId) {
        CLI_print("------ ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------\n");
    }

	if (count > 1) {
        if (dis_only == MEA_FALSE) {
            if (
                total_entry.green_fwd_pkts.val    != 0 ||
                total_entry.yellow_fwd_pkts.val   != 0 ||
                total_entry.green_dis_pkts.val    != 0 ||
                total_entry.yellow_dis_pkts.val   != 0 ||
                total_entry.red_dis_pkts.val      != 0 ||
                total_entry.other_dis_pkts.val    != 0 ||
                total_entry.dis_mtu_pkts.val      != 0 ||
                total_entry.green_fwd_bytes.val   != 0 ||
                total_entry.yellow_fwd_bytes.val  != 0 ||
                total_entry.green_dis_bytes.val   != 0 ||
                total_entry.yellow_or_red_dis_bytes.val != 0
                
                ){
            CLI_print("===========================================================================================================================\n");

            CLI_print("%5s ", "Total\n");

            CLI_print("%3s %16s %16s %16s %16s %16s %16s %16s\n",
                "  ", "FwdGreen", "FwdYellow", "DisGreen", "DisYellow", "DisRed", "DisOther", "DisMtu");
            CLI_print("------ ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------\n");
            CLI_print("%6s %16llu %16llu %16llu %16llu %16llu %16llu %16llu\n",

                " Pkt",
                total_entry.green_fwd_pkts.val,
                total_entry.yellow_fwd_pkts.val,
                total_entry.green_dis_pkts.val,
                total_entry.yellow_dis_pkts.val,
                total_entry.red_dis_pkts.val,
                total_entry.other_dis_pkts.val,
                total_entry.dis_mtu_pkts.val);
            CLI_print("%6s %16llu %16llu %16llu %16llu %16s %16s %16s\n",
                "Byte",
                total_entry.green_fwd_bytes.val,
                total_entry.yellow_fwd_bytes.val,
                total_entry.green_dis_bytes.val,
                total_entry.yellow_or_red_dis_bytes.val, "NA", "NA", "NA");
            CLI_print("===========================================================================================================================\n");
          }
        }
        else {
            if (
                total_entry.other_dis_pkts.val   != 0 ||
                total_entry.red_dis_pkts.val     != 0 ||
                total_entry.yellow_dis_pkts.val  != 0 ||
                total_entry.green_dis_pkts.val   != 0 ||
                total_entry.dis_mtu_pkts.val     != 0 ||
                total_entry.green_dis_bytes.val  != 0

                ) {
                CLI_print("===========================================================================================================================\n");

                CLI_print("%5s ", "Total\n");

                CLI_print("%6s %16s %16s %16s %16s %16s %16s %16s\n",
                    "      ", "FwdGreen", "FwdYellow", "DisGreen", "DisYellow", "DisRed", "DisOther", "DisMtu");
                CLI_print("------ ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------\n");
                CLI_print("%6s %16llu %16llu %16llu %16llu %16llu %16llu %16llu\n",

                    " Pkt",
                    total_entry.green_fwd_pkts.val,
                    total_entry.yellow_fwd_pkts.val,
                    total_entry.green_dis_pkts.val,
                    total_entry.yellow_dis_pkts.val,
                    total_entry.red_dis_pkts.val,
                    total_entry.other_dis_pkts.val,
                    total_entry.dis_mtu_pkts.val);
                CLI_print("%6s %16llu %16llu %16llu %16llu %16s %16s %16s\n",
                    "Byte",
                    total_entry.green_fwd_bytes.val,
                    total_entry.yellow_fwd_bytes.val,
                    total_entry.green_dis_bytes.val,
                    total_entry.yellow_or_red_dis_bytes.val, "NA", "NA", "NA");
                CLI_print("===========================================================================================================================\n");



            }

        }

	}



	return MEA_OK;

}

MEA_Status MEA_CLI_Collect_Counters_PM(int argc, char *argv[])
{

	MEA_Bool pmId_exist;
	MEA_PmId_t pmId,start_pmId,end_pmId;
	char buf[500];
	char *up_ptr;
    MEA_Status retval;
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
    MEA_Bool ffff=MEA_TRUE; 
    struct timeval tvBegin, tvEnd, tvDiff;

    time_t in_t;
    time_t out_t; 	 

#endif
	
	if (argc != 2) {
		CLI_print("Invalid number of parameters\n");
		return MEA_ERROR;
	}

	if (strcmp(argv[1], "all") != 0) {
		
		//MEA_PmId_t pmId = MEA_OS_atoi(argv[1]);
		MEA_OS_strcpy(buf, argv[1]);
		if((up_ptr=strchr(buf, ':')) != 0)
		{
			(*up_ptr) = 0;
			up_ptr++;
			start_pmId = MEA_OS_atoi(buf);
			end_pmId   = MEA_OS_atoi(up_ptr);
		}
		else
		{
			pmId=start_pmId = end_pmId = MEA_OS_atoi(buf);
		}
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME) ) && !defined(__KERNEL__)
        if (ffff==MEA_TRUE){

            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"PM time start\n");
            /*get the time*/
            time(&in_t);
            gettimeofday(&tvBegin, NULL);

            ffff=MEA_FALSE;

        } 
#endif 
		for(pmId=start_pmId;pmId<=end_pmId;pmId++){
			/* Check valid first_pmId */
			if (MEA_API_IsExist_PmId(MEA_UNIT_0,
						 pmId,
						 &pmId_exist, NULL) != MEA_OK) {
				CLI_print
					("%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
					 __FUNCTION__, pmId);
				return MEA_OK;
			}

			if (pmId_exist == MEA_FALSE) {
			 continue;
			}

			/* collect the counters of this service */
			if (MEA_API_Collect_Counters_PM(MEA_UNIT_0, pmId, NULL) !=
				MEA_OK) {
				CLI_print
					("%s - Collect_Counters_PM failed for pmId %d \n",
					 __FUNCTION__, pmId);
				return MEA_ERROR;
			}
		}
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
        time(&out_t);
        gettimeofday(&tvEnd, NULL);
        MEA_OS_timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
        CLI_print("Begin    ");
        MEA_OS_timeval_print(&tvBegin);
        CLI_print("End      ");
        MEA_OS_timeval_print(&tvEnd);
        CLI_print("Diff = %ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);


#endif    

	} else {

#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME) ) && !defined(__KERNEL__)
        if (ffff==MEA_TRUE){

            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"PM time start\n");
            /*get the time*/
            time(&in_t);
            gettimeofday(&tvBegin, NULL);

            ffff=MEA_FALSE;

        } 
#endif 
 
        retval= MEA_API_Collect_Counters_PMs(MEA_UNIT_0);

#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
        time(&out_t);
        gettimeofday(&tvEnd, NULL);
        MEA_OS_timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
        CLI_print("Begin    ");
        MEA_OS_timeval_print(&tvBegin);
        CLI_print("End      ");
        MEA_OS_timeval_print(&tvEnd);
        CLI_print("Diff = %ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);


#endif    


		/* collect the counters for all PMs */
		if ( retval != MEA_OK) {
			CLI_print("%s - Collect_Counters_PMs failed \n",
				  __FUNCTION__);
			return MEA_ERROR;
		}
	}

	return MEA_OK;

}

/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_Get_Counters_PM_exist(MEA_PmId_t     first_pmId,
                                                MEA_PmId_t    last_pmId)
{



    //int                      count;
    //    int                      num_of_count=0;
    MEA_PmId_t               pmId;

    
    MEA_Bool pmId_exist;





    CLI_print("\n");
    //count = 0;
    


    for (pmId = first_pmId; pmId <= last_pmId; pmId++) {
        /* Check valid last_pmId */
        pmId_exist = MEA_FALSE;
        if (MEA_API_IsExist_PmId(MEA_UNIT_0,
            pmId,
            &pmId_exist, NULL) != MEA_OK) {
        }

        CLI_print("PmId %6d  exist = %s\n", pmId, MEA_STATUS_STR(pmId_exist) );

        if (pmId_exist == MEA_FALSE) {
            continue;
        }





    }			/* for(serviceId=first_serviceId;serviceId<=last_serviceId;serviceId++) */




    return MEA_OK;

}



MEA_Status MEA_CLI_Show_Counters_PM_exist(int argc, char *argv[])
{


    
    //int count, count_discard,
    //    int                      num_of_count=0;
    MEA_PmId_t               pmId;
    MEA_PmId_t               first_pmId;
    MEA_PmId_t               last_pmId;
    //MEA_Counters_PM_dbt entry, total_entry;
    MEA_Bool pmId_exist;

    //MEA_Counters_PM_Bonding_dbt Bonding_entry;
    //    MEA_Uint16 groupId;

 


    char buf[500];
    char *up_ptr;


    if ((argc < 2)) {

        CLI_print("Invalid number of parameters\n");
        return MEA_ERROR;
    }

    if (strcmp(argv[1], "all") != 0) {

        //first_pmId = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if ((up_ptr = strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_pmId = MEA_OS_atoi(buf);
            last_pmId = MEA_OS_atoi(up_ptr);
        }
        else
        {
            pmId = first_pmId = last_pmId = MEA_OS_atoi(buf);
            if (MEA_API_IsExist_PmId(MEA_UNIT_0,
                pmId,
                &pmId_exist, NULL) != MEA_OK) {
                CLI_print
                ("%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
                    __FUNCTION__, pmId);
                return MEA_OK;
            }
        }

    }
    else {


        first_pmId = 0;
        last_pmId = MEA_MAX_NUM_OF_PM_ID - 1;

    }

 


    return MEA_CLI_Get_Counters_PM_exist(first_pmId, last_pmId);




}


MEA_Status MEA_CLI_Collect_Counters_PM_blk(int argc, char *argv[])
{

   
    MEA_PmId_t pmId, start_pmId, end_pmId;
    char buf[500];
    char *up_ptr;
    //MEA_Status retval;
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
    MEA_Bool ffff = MEA_TRUE;
    struct timeval tvBegin, tvEnd, tvDiff;

    time_t in_t;
    time_t out_t;

#endif

    if (argc != 2) {
        CLI_print("Invalid number of parameters\n");
        return MEA_ERROR;
    }

    if (strcmp(argv[1], "all") != 0) {

        //MEA_PmId_t pmId = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if ((up_ptr = strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            start_pmId = MEA_OS_atoi(buf);
            end_pmId = MEA_OS_atoi(up_ptr);
        }
        else
        {
            pmId = start_pmId = end_pmId = MEA_OS_atoi(buf);
        }
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME) ) && !defined(__KERNEL__)
        if (ffff == MEA_TRUE){

            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "PM time start\n");
            /*get the time*/
            time(&in_t);
            gettimeofday(&tvBegin, NULL);

            ffff = MEA_FALSE;

        }
#endif 
        for (pmId = start_pmId; pmId <= end_pmId; pmId++){
            /* Check valid first_pmId */
            CLI_print("Block %d\n", pmId);

            /* collect the counters of this service */
            if (MEA_API_Collect_Counters_PMs_Block(MEA_UNIT_0, pmId) !=
                MEA_OK) {
                CLI_print
                    ("%s - Collect_Counters_PM failed for pmId %d \n",
                    __FUNCTION__, pmId);
                return MEA_ERROR;
            }
        }
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
        time(&out_t);
        gettimeofday(&tvEnd, NULL);
        MEA_OS_timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
        CLI_print("Begin    ");
        MEA_OS_timeval_print(&tvBegin);
        CLI_print("End      ");
        MEA_OS_timeval_print(&tvEnd);
        CLI_print("Diff = %ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);


#endif    

    }
    else {

#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME) ) && !defined(__KERNEL__)
        if (ffff == MEA_TRUE){

            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "PM time start\n");
            /*get the time*/
            time(&in_t);
            gettimeofday(&tvBegin, NULL);

            ffff = MEA_FALSE;

        }
#endif 

       

#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
        time(&out_t);
        gettimeofday(&tvEnd, NULL);
        MEA_OS_timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
        CLI_print("Begin    ");
        MEA_OS_timeval_print(&tvBegin);
        CLI_print("End      ");
        MEA_OS_timeval_print(&tvEnd);
        CLI_print("Diff = %ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);


#endif    


    }

    return MEA_OK;

}




MEA_Status MEA_CLI_Collect_Counters_PM_blkShow(int argc, char *argv[])
{


	MEA_PmId_t pm_blk, start_pmId, end_pmId;
	char buf[500];
	char *up_ptr;
	MEA_PmId_t     first_pmId;
	MEA_PmId_t    last_pmId;
	MEA_Bool	  rate_fmt = MEA_FALSE;
	MEA_Bool	  dis_only = MEA_FALSE;
	int i;

	//MEA_Status retval;
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
	MEA_Bool ffff = MEA_TRUE;
	struct timeval tvBegin, tvEnd, tvDiff;

	time_t in_t;
	time_t out_t;

#endif

	if (argc < 2) {
		CLI_print("Invalid number of parameters\n");
		return MEA_ERROR;
	}

    if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(MEA_UNIT_0, MEA_DB_PM_TYPE) == MEA_FALSE) 
    {
        CLI_print("#### use command  mea counter pm show all\n");
        return MEA_OK;
    }

    if (strcmp(argv[1], "all") != 0) {

        //MEA_PmId_t pmId = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if ((up_ptr = strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            start_pmId = MEA_OS_atoi(buf);
            end_pmId = MEA_OS_atoi(up_ptr);
        }
        else
        {
            pm_blk = start_pmId = end_pmId = MEA_OS_atoi(buf);
        }
    }
    else  {
        start_pmId = 0; /*start block*/
        end_pmId   = MEA_COUNTERS_PM_MAX_OF_BLK-1;
    
    
    
    }

		for (i = 2; i < argc; i++){
			if (!strcmp(argv[2], "-rate")) {
				rate_fmt = MEA_TRUE;
			}
			if (!strcmp(argv[2], "-dis")) {
				dis_only = MEA_TRUE;
			}

		}



#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME) ) && !defined(__KERNEL__)
		if (ffff == MEA_TRUE){

			MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT, "PM time start\n");
			/*get the time*/
			time(&in_t);
			gettimeofday(&tvBegin, NULL);

			ffff = MEA_FALSE;

		}
#endif 
		for (pm_blk = start_pmId; pm_blk <= end_pmId; pm_blk++){
			/* Check valid first_pmId */
            CLI_print("====================\n");
            CLI_print(" PM_Block %d\n", pm_blk);
            CLI_print("====================\n");

			/* collect the counters of this service */
			if (MEA_API_Collect_Counters_PMs_Block(MEA_UNIT_0, pm_blk) !=
				MEA_OK) {
				CLI_print
					("%s - Collect_Counters_PM failed for pmId %d \n",
					__FUNCTION__, pm_blk);
				return MEA_ERROR;
			}

			first_pmId = pm_blk * MEA_COUNTERS_PM_BLK_READ;
			last_pmId = (first_pmId + MEA_COUNTERS_PM_BLK_READ) - 1;
    

		    MEA_CLI_Get_Counters_PM(first_pmId, last_pmId, rate_fmt, dis_only);




		} //for
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
		time(&out_t);
		gettimeofday(&tvEnd, NULL);
		MEA_OS_timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
		CLI_print("Begin    ");
		MEA_OS_timeval_print(&tvBegin);
		CLI_print("End      ");
		MEA_OS_timeval_print(&tvEnd);
		CLI_print("Diff = %ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);


#endif    

	


	return MEA_OK;

}



MEA_Status MEA_CLI_Clear_Counters_PM(int argc, char *argv[])
{

	MEA_Bool pmId_exist;
	MEA_PmId_t pmId,start_pmId,end_pmId;
	char buf[500];
	char *up_ptr;


	if (argc != 2) {
		CLI_print("Invalid number of parameters\n");
		return MEA_ERROR;
	}

    if (strcmp(argv[1], "all") != 0) {
        //MEA_PmId_t pmId = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if ((up_ptr = strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            start_pmId = MEA_OS_atoi(buf);
            end_pmId = MEA_OS_atoi(up_ptr);
        }
        else
        {
            pmId = start_pmId = end_pmId = MEA_OS_atoi(buf);
        }
        if (mea_drv_Get_DeviceInfo_ENTITY_Type_DB_DDR(MEA_UNIT_0, MEA_DB_PM_TYPE) == MEA_FALSE)
        {
            for (pmId = start_pmId; pmId <= end_pmId; pmId++){
                /* Check valid first_pmId */

                if (MEA_API_IsExist_PmId(MEA_UNIT_0,
                    pmId,
                    &pmId_exist, NULL) != MEA_OK) {
                    CLI_print
                        ("%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
                        __FUNCTION__, pmId);
                    return MEA_OK;
                }

                if (pmId_exist == MEA_FALSE) {
                    continue;
                }

                /* clear the counters of this pmId */
                if (MEA_API_Clear_Counters_PM(MEA_UNIT_0, pmId) != MEA_OK) {
                    CLI_print
                        ("%s - Clear_Counters_PM failed for pmId %d \n",
                        __FUNCTION__, pmId);
                    return MEA_ERROR;
                }
            }//
        } else{
            if(MEA_API_Clear_Counters_PMs_Block(MEA_UNIT_0, start_pmId, end_pmId) != MEA_OK) {
                CLI_print("%s - MEA_API_Clear_Counters_PMs_Block failed \n",
                    __FUNCTION__);
                return MEA_ERROR;
                }
        }

	} else {

		/* clear the counters for all PMs */
		if (MEA_API_Clear_Counters_PMs(MEA_UNIT_0) != MEA_OK) {
			CLI_print("%s - Clear_Counters_PM failed \n",
				  __FUNCTION__);
			return MEA_ERROR;
		}
	}

	return MEA_OK;

}

MEA_Status MEA_CLI_Show_Counters_BM(int argc, char *argv[])
{

	MEA_Counters_BM_dbt entry;

	if (MEA_API_Get_Counters_BM(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("%s - Get Counters BM failed\n", __FUNCTION__);
		return MEA_ERROR;
	}

	CLI_print("\n");

	CLI_print("Total   counters :\n");

	CLI_print("%-7s %-40s : ", " ",
		  "Drop packets because of no buffers");
#ifdef MEA_COUNTER_64_SUPPORT
	CLI_print("%10lu ", entry.BMBRL_drop_packets_counters.s.msw);
#endif
	CLI_print("%10lu\n", entry.BMBRL_drop_packets_counters.s.lsw);

	CLI_print("%-7s %-40s : ", " ",
		  "Drop packets because of no descriptors");

#ifdef MEA_COUNTER_64_SUPPORT
	CLI_print("%10lu ", entry.BMDRL_drop_packets_counters.s.msw);
#endif
	CLI_print("%10lu\n", entry.BMDRL_drop_packets_counters.s.lsw);

	CLI_print("\n");

	return MEA_OK;

}

MEA_Status MEA_CLI_Collect_Counters_BM(int argc, char *argv[])
{

	if (MEA_API_Collect_Counters_BMs(MEA_UNIT_0) != MEA_OK) {
		CLI_print("%s - Collect_Counters_BM failed \n",
			  __FUNCTION__);
		return MEA_ERROR;
	}

	return MEA_OK;

}

MEA_Status MEA_CLI_Clear_Counters_BM(int argc, char *argv[])
{

	if (MEA_API_Clear_Counters_BM(MEA_UNIT_0) != MEA_OK) {
		CLI_print("%s - Clear_Counters_BM failed \n",
			  __FUNCTION__);
		return MEA_ERROR;
	}

	return MEA_OK;

}


MEA_Status MEA_CLI_Show_clusters_portRate(int argc, char *argv[])
{
    

    ENET_QueueId_t port, from_port, to_port;
    ENET_QueueId_t queueId; //from_queueId, to_queueId;
    //MEA_Bool isAllPort = MEA_FALSE;
   
    MEA_Uint32 packet_Len=1000;


    ENET_Bool      retVal;
    ENET_Queue_dbt QueueDbt;
    MEA_Uint16 count = 0;
    MEA_Counters_Queue_dbt entry;
    MEA_Uint32 priority;
    //  MEA_Uint32 msw,msw1;
    MEA_Uint32 i;
   
    MEA_Uint64 TotalPacket;
    //MEA_Uint64 TotalRate;

    //MEA_Bool rate = MEA_FALSE;
    MEA_Bool foundPort = MEA_FALSE;

    char buf[500];
    char *up_ptr;




    if (argc < 2)
        return MEA_ERROR;

    if (MEA_OS_strcmp(argv[1], "all") == 0) {
       
        from_port = 0;
        to_port = MEA_MAX_PORT_NUMBER;
    }
    else {
        MEA_OS_strcpy(buf, argv[1]);
        if ((up_ptr = strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            from_port = MEA_OS_atoi(buf);
            to_port = MEA_OS_atoi(up_ptr);
        }
        else
        {
            port = from_port = to_port = MEA_OS_atoi(argv[1]);
            if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) == MEA_FALSE) {
                CLI_print("Error: port %d (%s) is invalid \n", port, argv[1]);
                return MEA_OK;
            }
        }

    }

    MEA_OS_memset(&RatePortsByCluster, 0, sizeof(RatePortsByCluster));


    for (i = 1; i < (MEA_Uint32)argc; i++) {
        if (MEA_OS_strcmp(argv[i], "-len") == 0) {
            
            packet_Len = MEA_OS_atoi(argv[i+1]);
            if (packet_Len < 64) {
                packet_Len = 1000;
            }

        }
        
    }//for

    count = 0;
    for (port = from_port; port <= to_port; port++) 
    {
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE) == MEA_FALSE)
            continue;
		
         
        if (ENET_GetFirst_Queue(ENET_UNIT_0,
            &queueId,
            &QueueDbt, &retVal) != ENET_OK) {
            CLI_print("%s - Failed to get first Queue\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        if (!retVal) {
            CLI_print("There are no configured Queue\n");
            return MEA_OK;
        }
        /************************************************************************/
        /*                                                                      */
        /************************************************************************/
        foundPort = MEA_FALSE;
        do {

            if ((QueueDbt.port.type == ENET_QUEUE_PORT_TYPE_PORT) && (QueueDbt.port.id.port == port)) 
            {
                foundPort = MEA_TRUE;

                if (MEA_API_Get_Counters_Queue(MEA_UNIT_0,
                    queueId,
                    &entry) != MEA_OK) {
                    CLI_print
                    ("%s - Get Counters Queue for queueId %d failed\n",
                        __FUNCTION__, queueId);
                    return MEA_ERROR;
                }
                if (count++ == 0) {
                    
                   
                        CLI_print
                           ("             rate            rate            Total                Type      \n"
                            "port Queue   (pps)           (Bit)            packet               configure  \n"
                            "---- ------- --------------- --------------- -------------------- ---------  \n");
                  

                        //123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                }
                
                TotalPacket.val = 0;
                for (priority = 0; priority < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; priority++)
                {
                    TotalPacket.val += entry.forward_packet[priority].val;

                }
                
                RatePortsByCluster[port].val += entry.Tx_TotalPacketRate.val;
                CLI_print("%4d ", port);
                CLI_print("%7d %15llu ", queueId, entry.Tx_TotalPacketRate.val);
                CLI_print("%15llu ", entry.Tx_TotalPacketRate.val *packet_Len * 8);
               

                CLI_print("%20llu ", TotalPacket.val);
                if (QueueDbt.mode.type == ENET_QUEUE_MODE_TYPE_STRICT)
                    CLI_print("STRICT \n" );
                if (QueueDbt.mode.type == ENET_QUEUE_MODE_TYPE_WFQ)
                    CLI_print("WFQ    %d \n", QueueDbt.mode.value);
                if (QueueDbt.mode.type == ENET_QUEUE_MODE_TYPE_RR)
                    CLI_print("RR     %d \n", QueueDbt.mode.value);
            
            }




            if (ENET_GetNext_Queue(ENET_UNIT_0,
                &queueId,
                &QueueDbt, &retVal) != ENET_OK) {
                CLI_print("%s - Failed to get next Queue\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
            if (retVal == MEA_FALSE && foundPort == MEA_TRUE) {
                CLI_print("------------------------------------------------------\n");
                CLI_print(" TOTAL ClusterPortRate\n");
                CLI_print("port               : %4d \n",port);
                CLI_print("Frames Sent Rate   : %10llu \n"  ,RatePortsByCluster[port].val);
                CLI_print("Bits Received Rate : %10llu \n", RatePortsByCluster[port].val*packet_Len*8);
              
                
                CLI_print("------------------------------ \n");
               
                    
                count = 0;
            }


        } while ((retVal));

    
    }//port













    return MEA_OK;

}
/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_CLI_printQueuePriorityRate(ENET_QueueId_t queueId, MEA_Bool rate, MEA_Uint32 packet_Len, MEA_Bool ShowTitle)
{

    MEA_Uint32 priority;
    
    ENET_Queue_dbt Entry_Queue;
    MEA_Counters_Queue_dbt entry;

    MEA_Uint64 TotalRate;
    MEA_Uint64 TotalPacket;

    MEA_Uint64 TotalwfqRate;
    MEA_Uint32  TotalPri_wfq;
    MEA_Uint32  tempCalcwfq;




    if (MEA_API_Get_Counters_Queue(MEA_UNIT_0,
        queueId,
        &entry) != MEA_OK) {
        CLI_print
        ("%s - Get Counters Queue for queueId %d failed\n",
            __FUNCTION__, queueId);
        return MEA_ERROR;
    }

    MEA_OS_memset(&Entry_Queue, 0, sizeof(Entry_Queue));


    ENET_Get_Queue(ENET_UNIT_0, queueId, &Entry_Queue);

    if (ShowTitle) {

        if (rate == MEA_FALSE) {
            CLI_print
            ("Queue   priQ  packet               mcMQSdrop \n"
                "------- ----- -------------------- -------------------- \n");
        }
        if (rate == MEA_TRUE) {
            CLI_print("Queue   priQ  rate(pps)       BitRate         weight      ConfType    packet                mcMQSdrop \n");
            CLI_print("------- ----- --------------- --------------- ---------- ----------- -------------------- -------------------- \n");
        }
        //123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
    }

    //msw = 0;
    // msw1=0;
#ifdef MEA_COUNTER_64_SUPPORT
    //msw  = entry.MTU_DRP.s.msw;
#endif
    TotalwfqRate.val = 0;
    TotalPri_wfq = 0;

    for (priority = 0; priority < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; priority++)
    {
        if ((priority == 0) && (MEA_API_Get_Globals_IS_best_eff_enable(MEA_UNIT_0) == MEA_TRUE)) {

        }
        else {
            if (Entry_Queue.pri_queues[priority].mode.type == ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ) {

                TotalPri_wfq += (MEA_Uint32)Entry_Queue.pri_queues[priority].mode.value.wfq_weight;
                TotalwfqRate.val += entry.forward_packet[priority].val;

            }
        }

    }




    CLI_print("%7d                             \n", queueId);
    TotalRate.val = 0;
    TotalPacket.val = 0;
    for (priority = 0; priority < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; priority++)
    {
        tempCalcwfq = 0;
        if (Entry_Queue.pri_queues[priority].mode.type == ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ) {
            if ((priority == 0) && (MEA_API_Get_Globals_IS_best_eff_enable(MEA_UNIT_0) == MEA_TRUE)) {

            }
            else {
                if ((TotalwfqRate.val != 0)) {
                    tempCalcwfq = (MEA_Uint32)((entry.forward_packet[priority].val) * TotalPri_wfq / (TotalwfqRate.val));
                    if ((entry.Tx_ratePacket[priority].val*packet_Len * 8) == 0) {
                        tempCalcwfq = 0;
                    }
                    else {
                        if (Entry_Queue.pri_queues[priority].mode.type == ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ) {
                            if (tempCalcwfq > (MEA_Uint32)Entry_Queue.pri_queues[priority].mode.value.wfq_weight)  {
                                tempCalcwfq = (MEA_Uint32)Entry_Queue.pri_queues[priority].mode.value.wfq_weight;
                            }
                        }
                    }
                    //tempCalcwfq = (MEA_Uint32)TotalPri_wfq /(TotalwfqRate.val/ entry.Tx_ratePacket[priority].val);
                }
            }
        }

        TotalPacket.val += entry.forward_packet[priority].val;
        if (rate == MEA_FALSE)
            CLI_print("%7s %5d %20llu %20llu\n", "", priority, entry.forward_packet[priority].val, entry.mc_MQS_DRP[priority].val);
        else {
            TotalRate.val += entry.Tx_ratePacket[priority].val;

            CLI_print("%7s %5d %15llu %15llu ", "", priority, entry.Tx_ratePacket[priority].val, (entry.Tx_ratePacket[priority].val*packet_Len * 8));
            CLI_print("%10d ", tempCalcwfq);
            if ((priority == 0) && (MEA_API_Get_Globals_IS_best_eff_enable(MEA_UNIT_0) == MEA_TRUE)) {
                CLI_print("BEF         ");
            }
            else {
                if (Entry_Queue.pri_queues[priority].mode.type == ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_STRICT)
                    CLI_print("STRICT      ");
                if (Entry_Queue.pri_queues[priority].mode.type == ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_WFQ)
                    CLI_print("WFQ    %3d  ", Entry_Queue.pri_queues[priority].mode.value.wfq_weight);
                if (Entry_Queue.pri_queues[priority].mode.type == ENET_QUEUE_PRIORITY_QUEUE_MODE_TYPE_RR)
                    CLI_print("RR          ");
            }
            CLI_print("%20llu ", entry.forward_packet[priority].val);
            CLI_print("%20llu ", entry.mc_MQS_DRP[priority].val);
            CLI_print("\n");

        }
    }


    if (rate == MEA_TRUE) {
        CLI_print("------- ----- --------------- --------------- ----------- -------------------- -------------------- \n");
        CLI_print("%7d Total %15llu %15llu %11s %20llu\n", queueId, entry.Tx_TotalPacketRate.val, entry.Tx_TotalPacketRate.val*packet_Len * 8, "", TotalPacket.val);
        CLI_print("----------------------------------------------------------------------------------------------- \n");

    }
    else {
        CLI_print("------- ----- --------------------\n");
        CLI_print("%7d Total %10llu\n", queueId, TotalPacket.val);
        CLI_print("-----------------------------------\n");
    }

    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_CLI_Show_Counters_Queue(int argc, char *argv[])
{
    ENET_QueueId_t queueId, from_queueId, to_queueId;
    MEA_Bool isAllQueue = MEA_FALSE;
    ENET_Bool      retVal;
    ENET_Queue_dbt QueueDbt;
    

    
    
    MEA_Uint32 packet_Len = 1000;

    MEA_Bool rate = MEA_FALSE;
    MEA_Uint32 i;
    char buf[500];
    char *up_ptr;




    if (argc < 2)
        return MEA_ERROR;

    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        isAllQueue = MEA_TRUE;
        if (ENET_GetFirst_Queue(ENET_UNIT_0,
            &queueId,
            &QueueDbt, &retVal) != ENET_OK) {
            CLI_print("%s - Failed to get first Queue\n",
                __FUNCTION__);
            return MEA_ERROR;
        }
        if (!retVal) {
            CLI_print("There are no configured Queue\n");
            return MEA_OK;
        }
    }
    else {
        MEA_OS_strcpy(buf, argv[1]);
        if ((up_ptr = strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            from_queueId = MEA_OS_atoi(buf);
            to_queueId = MEA_OS_atoi(up_ptr);
            if (to_queueId > ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT)
                to_queueId = ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT;
        }
        else
        {
            queueId = from_queueId = to_queueId = MEA_OS_atoi(buf);
            if (ENET_IsValid_Queue(ENET_UNIT_0, queueId, ENET_TRUE	/*silent */
            ) != ENET_TRUE) {
                CLI_print("Queue %d  is invalid \n", queueId);
                return MEA_ERROR;
            }

        }

    }


    for (i = 1; i < (MEA_Uint32)argc; i++) {

        if (MEA_OS_strcmp(argv[i], "-rate") == 0) {
            rate = MEA_TRUE;
        }
        if (MEA_OS_strcmp(argv[i], "-len") == 0) {

            packet_Len = MEA_OS_atoi(argv[i + 1]);
            if (packet_Len == 0) {
                packet_Len = 1000;
            }
        }


    }//for



    if (isAllQueue == MEA_TRUE) {

        do {

            if(MEA_CLI_printQueuePriorityRate(queueId, rate, packet_Len, MEA_TRUE) != MEA_OK){
                CLI_print("%s - Failed to get next Queue\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }

            if (ENET_GetNext_Queue(ENET_UNIT_0,
                &queueId,
                &QueueDbt, &retVal) != ENET_OK) {
                CLI_print("%s - Failed to get next Queue\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
            

        } while ((isAllQueue) && (retVal));

    }
    else {

        
        for (queueId = from_queueId; queueId <= to_queueId; queueId++) 
        {
            

            if (ENET_IsValid_Queue(ENET_UNIT_0, queueId, ENET_TRUE	/*silent */) != ENET_TRUE) {
                continue;
            }

            if (MEA_CLI_printQueuePriorityRate(queueId, rate, packet_Len, MEA_TRUE) != MEA_OK) {
                CLI_print("%s - Failed to get next Queue\n",
                    __FUNCTION__);
                return MEA_ERROR;
            }
           

        }//queueId

    }





	return MEA_OK;
}

MEA_Status MEA_CLI_Collect_Counters_Queue(int argc, char *argv[])
{

	ENET_QueueId_t queueId, from_queueId, to_queueId;
	
	
	MEA_Counters_Queue_dbt entry;
    char buf[500];
    char *up_ptr;


	if (argc != 2) {
		CLI_print("Invalid number of parameters\n");
		return MEA_ERROR;
	}

	if (MEA_OS_strcmp(argv[1], "all") == 0) {
		/* collect the counters for all queues */
		if (MEA_API_Collect_Counters_Queues(MEA_UNIT_0) != MEA_OK) {
			CLI_print
			    ("%s - MEA_API_Collect_Counters_Queues failed \n",
			     __FUNCTION__);
			return MEA_ERROR;
		}
	} else {
		
        queueId = MEA_OS_atoi(argv[1]);

        MEA_OS_strcpy(buf, argv[1]);
        if ((up_ptr = strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            from_queueId = MEA_OS_atoi(buf);
            to_queueId = MEA_OS_atoi(up_ptr);
            if (to_queueId > ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT)
                to_queueId = ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT;
        }
        else
        {
            queueId = from_queueId = to_queueId = MEA_OS_atoi(buf);
            if (ENET_IsValid_Queue(ENET_UNIT_0, queueId, ENET_TRUE	/*silent */
                ) != ENET_TRUE) {
                CLI_print("Queue %d  is invalid \n", queueId);
                return MEA_ERROR;
            }

        }

        for (queueId = from_queueId; queueId <= to_queueId; queueId++)
        {
            if (ENET_IsValid_Queue(ENET_UNIT_0, queueId, ENET_TRUE	/*silent */ ) != ENET_TRUE)
                continue;

            if (MEA_API_Collect_Counters_Queue(MEA_UNIT_0,
                queueId,
                &entry) != MEA_OK) {
                CLI_print
                    ("%s - Collect Counters Queue for queueId %d failed\n",
                        __FUNCTION__, queueId);
                return MEA_ERROR;
            }
        }
	}

	return MEA_OK;

}

MEA_Status MEA_CLI_Clear_Counters_Queue(int argc, char *argv[])
{

	ENET_QueueId_t queueId, from_queueId, to_queueId;
	//MEA_Bool       isAllQueue = MEA_TRUE;
	
    char buf[500];
    char *up_ptr;

	if (argc < 2) {
		CLI_print("Invalid number of parameters\n");
		return MEA_ERROR;
	}


   




	if (MEA_OS_strcmp(argv[1], "all") == 0) {
		/* collect the counters for all queues */
		if (MEA_API_Clear_Counters_Queues(MEA_UNIT_0) != MEA_OK) {
			CLI_print
			    ("%s - MEA_API_Clear_Counters_Queues failed \n",
			     __FUNCTION__);
			return MEA_ERROR;
		}
	} else {
		
		queueId = MEA_OS_atoi(argv[1]);

        MEA_OS_strcpy(buf, argv[1]);
        if ((up_ptr = strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            from_queueId = MEA_OS_atoi(buf);
            to_queueId = MEA_OS_atoi(up_ptr);
            if (to_queueId > ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT)
                to_queueId = ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT;
        }
        else
        {
            queueId = from_queueId = to_queueId = MEA_OS_atoi(buf);
            if (ENET_IsValid_Queue(ENET_UNIT_0, queueId, ENET_TRUE	/*silent */
                ) != ENET_TRUE) {
                CLI_print("Queue %d  is invalid \n", queueId);
                return MEA_ERROR;
            }

        }
		
        for (queueId = from_queueId; queueId <= to_queueId; queueId++)
        {
            if (ENET_IsValid_Queue(ENET_UNIT_0, queueId, ENET_TRUE	/*silent */ ) != ENET_TRUE)
                continue;
            
            if (MEA_API_Clear_Counters_Queue(MEA_UNIT_0, queueId) != MEA_OK) {
                CLI_print
                    ("%s - Clear Counters Queue for queueId %d failed\n",
                        __FUNCTION__, queueId);
                return MEA_ERROR;
            }
        }



	}

	return MEA_OK;

}

MEA_Status MEA_CLI_ShowEgressPortEntry(int argc, char *argv[])
{
	MEA_Port_t port;
	MEA_Port_t first_port, last_port;
	int count;

	MEA_EgressPort_Entry_dbt entry;
	MEA_EgressPort_Proto_t protocol;
	char protocol_str[MEA_EGRESS_PORT_PROTO_LAST][20];

	if (argc < 3)
		return MEA_ERROR;

	if (MEA_OS_strcmp(argv[1], "all") == 0) {
		first_port = 0;
		last_port = MEA_MAX_PORT_NUMBER;
	} else {
		first_port = MEA_OS_atoi(argv[1]);
		last_port = first_port;
		
		if (MEA_API_Get_IsPortValid(first_port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE) ==
		    MEA_FALSE) {
			CLI_print("Port %s (%d) not valid\n", argv[1],
				  first_port);
			return MEA_ERROR;
		}
	}
    
        if (MEA_OS_strcmp(argv[2], "-TEID_stamp_en") == 0) {
            return MEA_CLI_Show_EgressPortTEID_stamp_Info(argc - 1, argv);
        }
	if (MEA_OS_strcmp(argv[2], "-s") == 0) {
		return MEA_CLI_Show_EgressPortShaper_Info(argc - 1, argv);
	}
    
    if (MEA_OS_strcmp(argv[2], "-pro_1p1") == 0) {
        return MEA_CLI_Show_EgressPort_protect_1p1(argc - 1, argv);
    }
    if (MEA_OS_strcmp(argv[2], "-g999_frg_sch") == 0) {
        return MEA_CLI_Show_EgressPort_g999_frg_scheduler(argc - 1, argv);
    }
    if (MEA_OS_strcmp(argv[2], "-port_state") == 0) {
        return MEA_CLI_Show_EgressPort_rule_state(argc - 1, argv);
    }
    if (MEA_OS_strcmp(argv[2], "-l2_mask") == 0) {
            return MEA_CLI_Show_EgressPort_L2_mask(argc - 1, argv);
   }
    


	if (MEA_OS_strcmp(argv[2], "-b") != 0) {
		return MEA_ERROR;
	}

	for (protocol = 0; protocol < MEA_EGRESS_PORT_PROTO_LAST;
	     protocol++) {
		strcpy(protocol_str[protocol], "Unknown");
	}
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_TRANS][0]),
		      "TRANS");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_VLAN][0]),
		      "VLAN");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_QTAG][0]),
		      "QTAG");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_MARTINI][0]),
		      "MARTINI");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_EoLLCoATM][0]),
		      "EoLLCoATM");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_IP][0]), "IP");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_IPoVLAN][0]),
		      "IPoVlan");
	MEA_OS_strcpy(&
		      (protocol_str[MEA_EGRESS_PORT_PROTO_IPoEoLLCoATM]
		       [0]), "IPoEoLLCoATM");
	MEA_OS_strcpy(&
		      (protocol_str[MEA_EGRESS_PORT_PROTO_PPPoEoVcMUXoATM]
		       [0]), "PPPoEoVcMUXoATM");
	MEA_OS_strcpy(&
		      (protocol_str[MEA_EGRESS_PORT_PROTO_PPPoEoLLCoATM]
		       [0]), "PPPoEoLLCoATM");
	MEA_OS_strcpy(&
		      (protocol_str[MEA_EGRESS_PORT_PROTO_EoVcMUXoATM][0]),
		      "EoVcMUXoATM");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_IPoLLCoATM][0]),
		      "IPoLLCoATM");
	MEA_OS_strcpy(&
		      (protocol_str[MEA_EGRESS_PORT_PROTO_IPoVcMUXoATM]
		       [0]), "IPoVcMUXoATM");
	MEA_OS_strcpy(&
		      (protocol_str[MEA_EGRESS_PORT_PROTO_PPPoLLCoATM][0]),
		      "PPPoLLCoATM  ");
	MEA_OS_strcpy(&
		      (protocol_str[MEA_EGRESS_PORT_PROTO_PPPoVcMUXoATM]
		       [0]), "PPPoVcMUXoATM");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_ETHER_TYPE][0]),
		      "EtherType");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_MPLS][0]),
		      "MPLS");
	MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_MPLSoE][0]),
		      "MPLSoE");
    MEA_OS_strcpy(&(protocol_str[MEA_EGRESS_PORT_PROTO_DC][0]),
        "DC");
    

	CLI_print("\n");
	count = 0;
	for (port = first_port; port <= last_port; port++) {

		if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) == MEA_FALSE) {
			continue;
		}

		if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0, port, &entry)
		    != MEA_OK) {
			return MEA_ERROR;
		}

		if (count++ == 0) {

			CLI_print
			         ("Port Phy  Tx     Flush  Calc Halt work  AF   TX   com   Protocol     \n"
			          "     Port Enable Enable CRC       mode       Th   press             \n"
			          "---- ---- ------ ------ ---- ---- ----- ---- ---- ----- -------------\n");


        }
	    CLI_print("%4d %4d "
                  "%6s %6s %4s "
                  "%4s %5s %4d %4d %5s %-13s \n",
                  port,
                  entry.phy_port,
                  MEA_STATUS_STR(entry.tx_enable ),
                  MEA_STATUS_STR(entry.flush ),
                  MEA_STATUS_STR(entry.calc_crc ),
                  MEA_STATUS_STR(entry.halt ),
                  (entry.pause_send.Working_mode == 0) ? "chunk" : "store",
                   entry.pause_send.AF_threshold,
                   entry.pause_send.Transmit_threshold,
                   MEA_STATUS_STR(entry.egressFilter.compress_enable),
                   protocol_str[entry.proto]);
    }

	if (count > 0) {
		CLI_print
		    ("-----------------------------------------------\n");
	}

	CLI_print("Number Of Entries : %d\n\n", count);

	return MEA_OK;
}


MEA_Status MEA_CLI_Show_EgressPort_g999_frg_scheduler(int argc, char *argv[])
{

    MEA_Port_t               port,from_port,to_port;
    MEA_EgressPort_Entry_dbt entry;



    if(argc!=2  ) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        port=from_port = to_port = MEA_OS_atoi(argv[1]);

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE) {
            return MEA_ERROR;
        }
    } else {
        from_port = 0;
        to_port   = MEA_MAX_PORT_NUMBER;
    }

    CLI_print ("     G999 fragment scheduler  \n"
        "Port  enable      \n"
        "----  ------  \n");  

    for (port=from_port;port<=to_port;port++) {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_EgressPort_Entry for port %d failed\n",port);
            return MEA_ERROR;
        }
        CLI_print("%4d %6s \n",
            port,
            MEA_STATUS_STR(entry.g9991_frg_scheduler)) ;


    }


    return MEA_OK;
}

MEA_Status MEA_CLI_Show_EgressPort_L2_mask(int argc, char *argv[])
{
    MEA_Port_t               port,from_port,to_port;
    int i;
    MEA_EgressPort_Entry_dbt entry;


    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        port=from_port = to_port = MEA_OS_atoi(argv[1]);

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE)==MEA_FALSE) {
            return MEA_ERROR;
        }
    } else {
        from_port = 0;
        to_port   = MEA_MAX_PORT_NUMBER;
    }

    

    for (port=from_port;port<=to_port;port++) {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }
        CLI_print ("     Egress L2 Mask  \n"
                   "Port l2 mask      \n"
                   "---- ---------------------------------  \n");  
        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_EgressPort_Entry for port %d failed\n",port);
            return MEA_ERROR;
        }
        CLI_print("%4d ",port
        
        ) ;
        for(i=0;i<MEA_EGRESS_MAX_MASK_L2_TYPE;i++){
            if(i==0){
              CLI_print("%2d %d \n",i,entry.egressFilter.L2Type_Mask[i]);
            }else{
             CLI_print("     %2d %d \n",i,entry.egressFilter.L2Type_Mask[i]);
            }
        }


    }

    
    
    return MEA_OK;
}

MEA_Status MEA_CLI_Show_EgressPort_rule_state(int argc, char *argv[])
{

    MEA_Port_t               port,from_port,to_port;
    MEA_EgressPort_Entry_dbt entry;
   char  egress_State_str[MEA_EGRESS_PORT_FILTER_LAST+1][25];


    if(argc!=2  ) {
        return MEA_ERROR;
    }


    MEA_OS_strcpy(&(egress_State_str[MEA_EGRESS_PORT_FILTER_FWD_ALL][0]),"FWD_ALL    ");
    MEA_OS_strcpy(&(egress_State_str[MEA_EGRESS_PORT_FILTER_FWD_ONLY_BPDU][0]),"FWD_ONLY_BPDU");
    MEA_OS_strcpy(&(egress_State_str[MEA_EGRESS_PORT_FILTER_DROP_ONLY_BPDU][0]),"DROP_ONLY_BPDU");
    MEA_OS_strcpy(&(egress_State_str[MEA_EGRESS_PORT_FILTER_FWD_ONLY_CPU][0]),"FWD_ONLY_CPU");
    MEA_OS_strcpy(&(egress_State_str[MEA_EGRESS_PORT_FILTER_FWD_ONLY_CPU_AND_BPDU][0]),"FWD_ONLY_CPU_AND_BPDU");
    MEA_OS_strcpy(&(egress_State_str[MEA_EGRESS_PORT_FILTER_DROP_FROM_CPU][0]),"DROP_FROM_CPU");
    MEA_OS_strcpy(&(egress_State_str[MEA_EGRESS_PORT_FILTER_RESERV][0]),"RESERV");
    MEA_OS_strcpy(&(egress_State_str[MEA_EGRESS_PORT_FILTER_DROP_ALL_AND_BPDU][0]),"DROP_ALL");



    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        port=from_port = to_port = MEA_OS_atoi(argv[1]);

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE) {
            return MEA_ERROR;
        }
    } else {
        from_port = 0;
        to_port   = MEA_MAX_PORT_NUMBER;
    }

    CLI_print ("     Egress port Rule  \n"
               "Port  TYPE      \n"
               "---- -- -------------------------------  \n");  

    for (port=from_port;port<=to_port;port++) {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_EgressPort_Entry for port %d failed\n",port);
            return MEA_ERROR;
        }
        CLI_print("%4d %2d %-25s \n",
            port,
            entry.egressFilter.egress_port_state,
            egress_State_str[entry.egressFilter.egress_port_state]
            ) ;
        

    }


    return MEA_OK;
}



MEA_Status MEA_CLI_Show_EgressPort_protect_1p1(int argc, char *argv[])
{

    MEA_Port_t               port,from_port,to_port;
    MEA_EgressPort_Entry_dbt entry;
    char str[MEA_EGRESS_PROTECT_LAST+1][20];

    MEA_OS_strcpy(&(str[MEA_EGRESS_PROTECT_BYPASS][0]),"BYPASS");
    MEA_OS_strcpy(&(str[MEA_EGRESS_PROTECT_1_PLUS_1][0]),"1_PLUS_1");
    MEA_OS_strcpy(&(str[MEA_EGRESS_PROTECT_1_TWO_1][0]),"1_TWO_1");
    MEA_OS_strcpy(&(str[MEA_EGRESS_PROTECT_MAP_PORT2INT][0]),"PORT2INT");
    MEA_OS_strcpy(&(str[MEA_EGRESS_PROTECT_LAST][0]),"LAST");


    if(argc!=2  ) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        port=from_port = to_port = MEA_OS_atoi(argv[1]);

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE)==MEA_FALSE) {
            return MEA_ERROR;
        }
    } else {
        from_port = 0;
        to_port   = MEA_MAX_PORT_NUMBER;
    }

    CLI_print ("Port type     1p1    out  \n"
               "              enable      \n"
               "---- -------- ------ ----   \n");  






    for (port=from_port;port<=to_port;port++) {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_EgressPort_Entry for port %d failed\n",port);
            return MEA_ERROR;
        }
        CLI_print("%4d %8s %6s %4d\n",
            port,
            str[entry.protectType],
            MEA_STATUS_STR(entry.protect_1p1_info.enable_1P1),
            entry.protect_1p1_info.outPortId_1p1);


    }


    return MEA_OK;
}


MEA_Status MEA_CLI_Show_EgressPort_my_mac(int argc, char *argv[])
{

    MEA_Port_t               port,from_port,to_port;
    MEA_EgressPort_Entry_dbt entry;
    


    if(argc!=2  ) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        port=from_port = to_port = MEA_OS_atoi(argv[1]);

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE) {
            return MEA_ERROR;
        }
    } else {
        from_port = 0;
        to_port   = MEA_MAX_PORT_NUMBER;
    }

    CLI_print ("Port  MY_MAC Address     \n"
        "---- --------------------\n");  






    for (port=from_port;port<=to_port;port++) {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_EgressPort_Entry for port %d failed\n",port);
            return MEA_ERROR;
        }
        CLI_print("%4d %02x:%02x:%02x:%02x:%02x:%02x \n",
            port,
            (unsigned char)(entry.My_Mac.b[0]),
            (unsigned char)(entry.My_Mac.b[1]),
            (unsigned char)(entry.My_Mac.b[2]),
            (unsigned char)(entry.My_Mac.b[3]),
            (unsigned char)(entry.My_Mac.b[4]),
            (unsigned char)(entry.My_Mac.b[5])
            );

        
    }
    return MEA_OK;


}
MEA_Status MEA_CLI_Show_EgressPortShaper_Info(int argc, char *argv[])
{
    MEA_Port_t               port,from_port,to_port;
	MEA_EgressPort_Entry_dbt entry;
    char str_mode[MEA_SHAPER_TYPE_LAST+1][6];
    

	if(argc!=2  ) {
       return MEA_ERROR;
    }
     
    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_MEF    ][0]),"MEF");
    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_BAG    ][0]),"BAG");
    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_LAST    ][0]),"ERORR");

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
       port=from_port = to_port = MEA_OS_atoi(argv[1]);

  	   if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE) {
           return MEA_ERROR;
       }
    } else {
       from_port = 0;
       to_port   = MEA_MAX_PORT_NUMBER;
    }

    CLI_print ("Port ena Comp Spr ********   Shaper profile   ********\n"
		       "         Type Id  MODE Cir          Cbs     type overhead\n"
               "---- --- ---- --- ---- ------------ ------- ---- --------\n");  
                                                             

            
            

    
    for (port=from_port;port<=to_port;port++) {

  	    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
            continue;
        }

		if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,port,&entry) != MEA_OK) {
	        CLI_print("Error: MEA_API_Get_EgressPort_Entry for port %d failed\n",port);
		    return MEA_ERROR;
		}

		if (entry.shaper_enable == ENET_TRUE) {
			 CLI_print("%4d %3s %4d %3d %4s %12llu %7d %4s %8d\n",
					  port,
					  "on",
					  entry.Shaper_compensation,
					  entry.shaper_Id,
                      str_mode[entry.shaper_info.mode],
					  entry.shaper_info.CIR,
					  entry.shaper_info.CBS,
					  (entry.shaper_info.resolution_type==MEA_SHAPER_RESOLUTION_TYPE_BIT) 
                      ? "bps" : "pps",
                      entry.shaper_info.overhead);
		}else{
				CLI_print("%4d Off\n",port);
		}
    }
    return MEA_OK;
}


MEA_Status MEA_CLI_Show_EgressPortTEID_stamp_Info(int argc, char *argv[])
{
    MEA_Port_t               port, from_port, to_port;
    MEA_EgressPort_Entry_dbt entry;
    


    if (argc != 2) {
        return MEA_ERROR;
    }

   

    if (MEA_OS_strcmp(argv[1], "all") != 0) {
        port=from_port = to_port = MEA_OS_atoi(argv[1]);

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE) == MEA_FALSE) {
            return MEA_ERROR;
        }
    }
    else {
        from_port = 0;
        to_port = MEA_MAX_PORT_NUMBER;
    }

    CLI_print("Port  TEID_stamping \n"
              "                    \n"
              "---- -------------- \n");






    for (port = from_port; port <= to_port; port++) {

        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) == MEA_FALSE) {
            continue;
        }

        if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0, port, &entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_EgressPort_Entry for port %d failed\n", port);
            return MEA_ERROR;
        }

        
            CLI_print("%4d %s\n",port,MEA_STATUS_STR(entry.enable_te_id_stamping));
       
    }
    return MEA_OK;
}


MEA_Status MEA_CLI_ShowEgressWred(int argc, char *argv[])
{

    MEA_WRED_Entry_Data_dbt   data;
    MEA_WRED_Entry_Key_dbt    key;
    MEA_AcmMode_t             acm_mode_from,acm_mode_to;
    MEA_WredProfileId_t       profile_id_from,profile_id_to;
    MEA_Priority_t            priority_from,priority_to;
    MEA_NormalizeAQS_t        normalize_aqs_from,normalize_aqs_to;
    MEA_Uint32                i;

  	if(argc<2) {
      return MEA_ERROR;
    }
    
    MEA_OS_memset(&key,0,sizeof(key));

    if (MEA_OS_strcmp(argv[1],"all") == 0 ) {
        priority_from=0;
        priority_to=MEA_WRED_NUM_OF_PRI-1;
    } else {
        priority_to = priority_from = MEA_OS_atoi(argv[1]);
        if (priority_to >= MEA_WRED_NUM_OF_PRI) {
            CLI_print("Error- <priority> %d >= max value %d \n",priority_to,MEA_WRED_NUM_OF_PRI);
            return MEA_ERROR;
        }
    }

    normalize_aqs_from = 0;
    normalize_aqs_to   = MEA_WRED_NORMALIZE_AQS_MAX_VALUE;

    profile_id_from = 0;
    profile_id_to   = MEA_WRED_NUM_OF_PROFILES-1;
    acm_mode_from   = 0;
    acm_mode_to     = MEA_WRED_NUM_OF_ACM_MODE-1;
    for (i=2; i < (MEA_Uint32)argc; i++) {
        if(!strcmp(argv[i],"-f")){
            if (i +1 > (MEA_Uint32) argc){
                CLI_print("Error  %s missing parameters\n",argv[i]);
                return MEA_ERROR;
            } 
            profile_id_to   = profile_id_from = MEA_OS_atoi(argv[i+1]);
            
        } 
        if(!strcmp(argv[i],"-acm")){
            if (i +1 > (MEA_Uint32) argc){
                CLI_print("Error  %s missing parameters\n",argv[i]);
                return MEA_ERROR;
            } 
            acm_mode_to   = acm_mode_from = MEA_OS_atoi(argv[i+1]);
            
       } 

    }


    CLI_print ("Acm Pro Pri Probability Values                                             \n"
               "    Id                                                                     \n"
               "--- --- --- ---------------------------------------------------------------\n");
    for (key.profile_key.acm_mode = acm_mode_from; 
         key.profile_key.acm_mode<= acm_mode_to; 
         key.profile_key.acm_mode++) {
        
        
        for (key.profile_key.profile_id = profile_id_from; 
             key.profile_key.profile_id<= profile_id_to; 
             key.profile_key.profile_id++) {

            if (!MEA_API_IsValid_WRED_Profile(MEA_UNIT_0,
                                              &(key.profile_key),
                                              MEA_TRUE)) {
                continue;
            }


            for (key.priority=priority_from; 
                 key.priority<=priority_to;
                 key.priority++) {

                if (key.priority==priority_from) {
                    CLI_print("%3d %3d %3d ",
                              key.profile_key.acm_mode,
                              key.profile_key.profile_id,
                              key.priority);
                } else {
                    CLI_print("%3s %3s %3d ",
                              " "," ",key.priority);
                }
        
                for (key.normalize_aqs=normalize_aqs_from;
                     key.normalize_aqs<=normalize_aqs_to;
                     key.normalize_aqs++) {

                    if (MEA_API_Get_WRED_Entry(MEA_UNIT_0,
                                               &key,
                                               &data) != MEA_OK) {
                        CLI_print("Error: MEA_API_Get_WRED_Entry failed \n");
                        return MEA_ERROR;
                    }
                
                    CLI_print("%3d ",data.drop_probability);

                    if ((key.normalize_aqs+1) % 16 == 0) {
                        CLI_print("\n%3s %3s %3s ","","","");
                    }
                }
                CLI_print("\n");
            }
        }
        CLI_print("--- --- --- ---------------------------------------------------------------\n");
    }

    return MEA_OK;
}


MEA_Status MEA_CLI_DebugPMShow(int argc, char *argv[])
{
    MEA_PmId_t index;
    MEA_Bool   show_title, found;
	MEA_drv_PM_dbt entry;
    show_title = MEA_TRUE;

    for (index = 0; index < MEA_MAX_NUM_OF_PM_ID; index++)
    {
		if ( mea_drv_get_pm_pool_entry( index,
									    &found,
									     &entry ) != MEA_OK )
        {
			return MEA_ERROR;
		}
		
		if ( found )
		{
            if ( show_title )
            {
 				CLI_print("ID   Owners pm type\n"
						  "---- ------ -------\n");
				show_title = MEA_FALSE;
			}
				CLI_print("%4d %6d %-7s\n",
						  index,
                          entry.num_of_owners,
                          (entry.pm_type == MEA_PM_TYPE_ALL        ) ? "All" : 
                         /* (entry.pm_type == MEA_PM_TYPE_PACKET_ONLY) ? "Pkt" :*/ "Unknown");

			
		}
        
	}
	return MEA_OK;
}




