/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "mea_deviceInfo_drv.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_PacketGen.h"

static MEA_Status MEA_CLI_PacketGen_Create(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_Delete(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_Modify(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_Show_Header(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_Show_Controls(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_Show_Shaper(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_Show_SeqId(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_Show_prof(int argc, char *argv[]);

static MEA_Status MEA_CLI_PacketGen_prof_info_Delete(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_prof_info_Modify(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_prof_info_Create(int argc, char *argv[]);
static MEA_Status MEA_CLI_PacketGen_prof_info_Show(int argc, char *argv[]);



static MEA_Status MEA_CLI_analyzer_generic_set(int argc, char *argv[]);
static MEA_Status MEA_CLI_analyzer_generic_Show(int argc, char *argv[]);



MEA_Status MEA_CLI_PacketGen_AddDebugCmds(void)
{

    CLI_defineCmd("MEA packet profile set delete",
                  (CmdFunc)MEA_CLI_PacketGen_prof_info_Delete, 
                  "delete packet profile info definition\n",
                  "delete all|<id>\n"
                  "----------------------------------------------------------\n"
                  "  all|<id> - delete specific profile <id>  or all \n"
                  "----------------------------------------------------------\n"
                  " Examples: - delete all\n"
                  "           - delete 2\n");

    CLI_defineCmd("MEA packet profile set modify",
                  (CmdFunc)MEA_CLI_PacketGen_prof_info_Modify, 
                 "Create packet profile info definition",
                 "Usage: modify <id>     \n"
                 "  [-seq  <valid><value>]\n"
                 "  [-len  <type>        ]\n"
                 "  [-prbs  <forceError>  ]\n"
                 "----------------------------------------------------------------\n"
                 "    <id>  - profile id or auto                                  \n"
                 "    options:                                                    \n"
                 "    --------                                                    \n"
                 "    -len (header length)                                        \n"
                 "          <value>     (value from 20..56  or 58..120)           \n"
                 "    -prbs (add to prbs Error)                                   \n"
                 "          <forceErrorPrbs>    (0- disable 1- enable)            \n"
                 "    -seq  (sequence Stamping option parameters)                 \n"
                 "         <enable>  0 disable 1- enable                          \n"
                 "         <offset>    1,2,3                                      \n" 
                 "                     1  for offset 18\n" 
                 "                     2  for offset 22\n"
                 "                     3  for offset 26\n"
                 "    -stream_type  (packet type in the stream)\n"
                 "                     0 - GENERIC  \n"
                 "                     1 - LBM/LBR  \n"
                 "                     2 - DMM_DMR  \n"
                 "                     3 - CCM      \n"
                 "----------------------------------------------------------------\n"
                 "Examples:\n" 
                 
                 " - modify 0 -len 20  -seq 0 0 \n"
		         " - modify 1    -len 20  -seq 1 2\n");

     CLI_defineCmd("MEA packet profile set create",
                  (CmdFunc)MEA_CLI_PacketGen_prof_info_Create, 
                 "Create packet profile info definition",
                 "Usage: create <id>|auto   \n"
                 "  [-seq  <valid><value> ]\n"
                 "  [-len  <type>         ]\n"
                 "  [-prbs  <forceError>  ]\n"
                 "----------------------------------------------------------------\n"
                 "    <id>  - profile id or auto                                  \n"
                 "    options:                                                    \n"
                 "    --------                                                    \n"
                 "    -len (header length)                                        \n"
                 "          <value>     (value from (20..58)  or 68..120)         \n"
                 "    -prbs (add to prbs Error)                                   \n"
                 "          <forceErrorPrbs>    (0- disable 1- enable)            \n"
                 "    -seq  (sequence Stamping option parameters)                 \n"
                 "         <enable>  0 disable 1- enable                          \n"
                 "         <offset>    1,2,3                                      \n" 
                 "                     1  for offset 18\n" 
                 "                     2  for offset 22\n"
                 "                     3  for offset 26\n"  
                 "    -stream_type  (packet type in the stream)\n"
                 "                     0 - GENERIC  \n"
                 "                     1 - LBM/LBR  \n"
                 "                     2 - DMM_DMR  \n"
                 "                     3 - CCM      \n"
                 "----------------------------------------------------------------\n"
                 "Examples:\n" 
                 " - create auto \n"
                 " - create auto -len 20  -seq 0 0 \n"
                 " - create 1    -len 100  -seq 1 2\n" 
                 );

    CLI_defineCmd("MEA packet profile show",
                  (CmdFunc)MEA_CLI_PacketGen_prof_info_Show, 
                  "Show   packet profile info(s)\n",
                  "Usage:  profile all|<Id> \n"
                  "---------------------------------------------------\n"
                  "    <id>  - profile id                              \n"
                  "---------------------------------------------------\n"
                  "Examples: - profile all \n"
                  "          - profile 2   \n");

/**************************************************************************************/

    CLI_defineCmd("MEA packet generator set delete",
                  (CmdFunc)MEA_CLI_PacketGen_Delete, 
                  "delete packet generator stream definition\n",
                  "delete all|<id>\n"
                  "----------------------------------------------------------\n"
                  "  all|<id> - delete specific packet generator <id>  or all \n"
                  "----------------------------------------------------------\n"
                  " Examples: - delete all\n"
                  "           - delete 2\n");


    CLI_defineCmd("MEA packet generator set modify",
         (CmdFunc)MEA_CLI_PacketGen_Modify, 
         "Modify packet generator stream definition",
         "Usage: modify <id>\n"
         "      [-profid <valid> <Id><headerType>]       \n"
         "      [-len  <type> <start> <end> [<step>]      ]       \n"
         "      [-header <header Lenght> [<byte0> [<byte1>..[<byte63>]]]\n"        
         "      [-data <patternType> <value> <withCRC>    ]       \n"
         "      [-count <value> [<burst>]                 ]       \n"
         "      [-loop  <value> (1-30 31 for infinity)    ]       \n"
         "      [-next  <enable> [<id>]                   ]       \n"
         "      [-active <enable>                         ]       \n"
         "      [-reset_prbs <enable>                     ]       \n"
         "      [-error <enable>                          ]       \n"
#if 0
         "      [-seqId <type> <offset> <start> <end> [<step>]]   \n"
#else 
            "      [-seqId  <start> <end> [<step>]]   \n"
#endif
         "      [-time <type> <offset>                       ]    \n"
         "      [-s <shaper_Enable><CIR>[<CBS><shaper_type><compensation><p_overhead><c_overhead>]\n"
         "----------------------------------------------------------------\n"
         "    <id>  - stream id                                           \n"
         "                                                                \n"
         "    options:                                                    \n"
         "    --------                                                    \n"
         "    -header  (user header )                                     \n"
         "         <header Length>  : The length of the packet header  (0 - 64)\n"
         "         <byte0>   : First  byte of the packet header (2 hex digits) \n"
         "         <byte1>   : Second byte of the packet header (2 hex digits) \n"
         "         ....                                                        \n"
         "         <byte63>  : Last   byte of the packet header (2 hex digits) \n"
         "    -len (length option parameters)                             \n"
         "        <type>        - packet Length type                      \n"
         "                        0 - value                               \n"
         "                        1 - increment                           \n"
         "                        2 - decrement                           \n"
         "                        3 - random                              \n"
         "        <start>       - Packet length start value               \n"
         "        <end>         - Packet length end   value               \n"
         "        <step>        - Packet length step  value (Default: 1)  \n"
         "    -data (data option parameters)                              \n"
         "        <patternType> - 0 (value) / 1 (prbs)                    \n"
         "        <value>       - 4 hex digits value to be fill as data   \n"
         "                        For example: 0000 FFFF AA55             \n"
         "        <withCRC>     - 0 (Without CRC) / 1 (With CRC)          \n" 
         "    -count (count option parameters)                            \n"
         "        <value>       - Number of packets to transmit           \n"
         "        [<burst>]       - Number of packets in one burst        \n"
         "   -loop   (relevant only at increment decrement)               \n"
         "            <number> (0-30 31 for infinity)                     \n"
         "    -next (next option parameters)                              \n"
         "        <enable>      - Define next stream id enable (0 or 1)   \n"
         "        <id>          - Next stream id                          \n"
         "    -active (active option parameters)                          \n"
         "        <enable>      - Define if this stream is active (0 or 1)\n" 
         "    -error ( sending packet with error)                         \n"
         "          <enable>       enable (1) / disable (0)               \n"
         "    -reset_prbs ( reset PRBS sending packet                     \n"
         "          <enable>       enable (1) / disable (0)               \n"
         "    -seqId  (sequence Id Stamping option parameters)            \n"
        //"        <type>        - Seq Id Stamping type                    \n"
        // "                        0 - none                                \n"
        // "                        1 - stamp                               \n"
        // "        <offset>      - Offset Location in the packet to stamp  \n"
        // "        <start>       - Start Sequence Id value(Hex) to Stamp OR D.C \n" 
         "        <end>         - End   Sequence Id value(Hex) to Stamp OR D.C \n"
         "        <step>        - The value to be use in the seq change   \n" 
         "    -time  (time Stamping option parameters)                   \n"
         "        <type>        - Time stamping type                      \n"
         "                        0 - none                                \n"
         "                        1 - stamp                               \n"
         "        <offset>      - Offset Location in the packet to stamp  \n"
         "    -s (assign a shaper )\n"
         "         <shaper_Enable> -  enable (1) / disable (0). \n"
         "         <CIR>           - Committed Information Rate [bps/pps, see shaper_type]\n"
         "         <CBS>           - Committed Burst Size [bytes/packets, see shaper_type]\n"
         "         <shaper_type>   - bit shaper (1) / packet shaper (0) \n"
         "         <compensation>  - 0 - chunk / 1-Packet / 3- chunk+Packet \n"
         "         <p_overhead>    - byte overhead for packet \n"
         "         <c_overhead>    - overhead for chunk \n"
         "----------------------------------------------------------------\n"
         "Examples:\n"
         " - modify 1 -header 10 00 01 02 03 04 05 10 11 12 13 14 15 81 00 00 07 -active 1\n" 
         "            -count 1  -data 0 AA55 1 -len 1 100 200 1\n"
         " - modify 1 -active 1 \n"
         " - modify 1 -active 0 \n"
         );

    CLI_defineCmd("MEA packet generator set create",
                  (CmdFunc)MEA_CLI_PacketGen_Create, 
     
                  "Create packet generator stream definition",
     "Usage: create <id>|auto   \n"
     "  must[-profid <valid> <Id><headerType>]       \n"
     "      [-len  <type> <start> <end> [<step>]      ]       \n"
     "      [-header <header Lenght> [<byte0> [<byte1>..[<byte63>]]]\n"        
     "      [-data <patternType> <value> <withCRC>    ]       \n"
     "      [-count <value> [<burst>]                 ]       \n"
     "      [-loop  <value>                           ]       \n"
     "      [-next  <enable> [<id>]                   ]       \n"
     "      [-active <enable>                         ]       \n"
     "      [-reset_prbs <enable>                     ]       \n"
     "      [-error <enable>                          ]       \n"
#if 0
    "      [-seqId <type> <offset> <start> <end> [<step>]]   \n"
#else 
    "      [-seqId  <start> <end> [<step>]]   \n"
#endif
     "      [-time  <type> <offset>                       ]       \n"
     "      [-s <shaper_Enable><CIR>[<CBS><shaper_type><compensation><p_overhead><c_overhead>]\n"
     "----------------------------------------------------------------\n"
     "    <id>  - stream id or auto                                   \n"
     "    options:                                                    \n"
     "    --------                                                    \n"
     "    -profid (profile info)                                      \n"
     "          <valid>       1 enable                                \n"
     "          <id>          the profile id                          \n"
     "          <type>    0- (until 56 header len)                    \n"
     "                    1- (from 64 until 120 header len)           \n"
     "    -len (length option parameters)                             \n"
     "        <type>        - packet Length type                      \n"
     "                        0 - value                               \n"
     "                        1 - increment                           \n"
     "                        2 - decrement                           \n"
     "                        3 - random                              \n"
     "        <start>       - Packet length start value               \n"
     "        <end>         - Packet length end   value               \n"
     "        <step>        - Packet length step  value (Default: 1)  \n"
     "    -header  (user header )                                       \n"
     "        <header Length>  : The length of the packet header  (0 - 64)   \n"
     "        <byte0>   : First  byte of the packet header (2 hex digits) \n"
     "        <byte1>   : Second byte of the packet header (2 hex digits) \n"
     "         ....                                                        \n"
     "        <byte63>  : Last   byte of the packet header (2 hex digits) \n"
     "    -data (data option parameters)                              \n"
     "        <patternType> - 0 (value) / 1 (prbs)                    \n"
     "        <value>       - 4 hex digits value to be fill as data   \n"
     "                        For example: 0000 FFFF AA55             \n"
     "        <withCRC>     - 0 (Without CRC) / 1 (With CRC)          \n" 
     "    -count (count option parameters)                            \n"
     "        <value>       - Number of packets to transmit           \n"
     "        <burst>       - Number of packets in one burst          \n"
     "   -loop   (relevant only at increment decrement)               \n"
     "            <number> (0-30 31 for infinity)                     \n"
     "    -next (next option parameters)                              \n"
     "        <enable>      - Define next stream id enable (0 or 1)   \n"
     "        <id>          - Next stream id                          \n"
     "    -active (active option parameters)                          \n"
     "        <enable>      - Define if this stream is active (0 or 1)\n"
     "    -error ( sending packet with error)                         \n"
     "          <enable>       enable (1) / disable (0)               \n"
     "    -reset_prbs ( reset PRBS sending packet                     \n"
     "          <enable>       enable (1) / disable (0)               \n"

    // "    -seqId  (sequence Id Stamping option parameters)            \n"
    // "        <type>        - Seq Id Stamping type                    \n"
    // "                        0 - none                                \n"
    // "                        1 - stamp                               \n"
    // "        <offset>      - Offset Location in the packet to stamp  \n"
     "        <start>       - Start Sequence Id value(Hex) to Stamp OR D.C \n" 
     "        <end>         - End   Sequence Id value(Hex) to Stamp OR D.C \n"
     "        <step>        - The value to be use in the seq change   \n" 
     "    -time    (time Stamping option parameters)                   \n"
     "        <type>        - Time stamping type                      \n"
     "                        0 - none                                \n"
     "                        1 - stamp                               \n"
     "        <offset>      - Offset Location in the packet to stamp  \n"
     "    -s (assign a shaper )\n"
     "         <shaper_Enable> -  enable (1) / disable (0). \n"
     "         <CIR>           - Committed Information Rate [bps/pps, see shaper_type]\n"
     "         <CBS>           - Committed Burst Size [bytes/packets, see shaper_type]\n"
     "         <shaper_type>   - bit shaper (1) / packet shaper (0) \n"
     "         <compensation>  - 0 - chunk / 1-Packet / 3- chunk+Packet \n"
     "         <p_overhead>    - byte overhead for packet \n"
     "         <c_overhead>    - overhead for chunk \n"
     "----------------------------------------------------------------\n"
     "Examples:\n" 
     " - create auto \n"
     " -       create auto -profid 1 0 0 -header 16 00 01 02 03 04 05 10 11 12 13 14 15 81 00 00 0a \n"
     "               -active 0  -count 0 -loop 1  -data 0 AA55 0 -len 1 100 200 1 \n"
     "               -s 1 10 0 0 1 0 0 \n"
     " - create 1    -profid 0 -header 16 00 01 02 03 04 05 10 11 12 13 14 15 81 00 00 08\n" 
     "                -active 1 -count 100  -data 0 FFFF 0 -len 1 64 1500 1    \n"
     );



    CLI_defineCmd("MEA packet generator show shaper",
                  (CmdFunc)MEA_CLI_PacketGen_Show_Shaper, 
                  "Show   packet generator shaper(s)\n",
                  "Usage: show shaper all|<Id> \n"
                  "---------------------------------------------------\n"
                  "    <id>  - stream id                              \n"
                  "---------------------------------------------------\n"
                  "Examples: - shaper all \n"
                  "          - shaper 2   \n");

    CLI_defineCmd("MEA packet generator show header",
                  (CmdFunc)MEA_CLI_PacketGen_Show_Header, 
                  "Show   packet generator header(s)\n",
                  "Usage: show header all|<Id> \n"
                  "---------------------------------------------------\n"
                  "    <id>  - stream id                                           \n"
                  "---------------------------------------------------\n"
                  "Examples: - header all \n"
                  "          - header 2   \n");

    CLI_defineCmd("MEA packet generator show controls",
                  (CmdFunc)MEA_CLI_PacketGen_Show_Controls, 
                  "Show   packet generator controls(s)\n",
                  "Usage: show controls all|<Id> \n"
                  "-----------------------------------\n"
                  "    <id>  - stream id                                           \n"
                  "-----------------------------------\n"
                  "Examples: - controls all \n"
                  "          - controls 2   \n");
    CLI_defineCmd("MEA packet generator show seqId",

                  (CmdFunc)MEA_CLI_PacketGen_Show_SeqId, 
                  "Show   show packet generator seqId(s)\n",
                  "Usage: show seqId all|<Id> \n"
                  "-----------------------------------\n"
                  "    <id>  - stream id                                           \n"
                  "-----------------------------------\n"
                  "Examples: - seqId all \n"
                  "          - seqId 2   \n");
    
    CLI_defineCmd("MEA packet generator show profile",
                  (CmdFunc)MEA_CLI_PacketGen_Show_prof, 
                  "Show   packet generator used profile\n",
                  "Usage: show profile all|<Id> \n"
                  "-----------------------------------\n"
                  "    <id>  - stream id                                           \n"
                  "-----------------------------------\n"
                  "Examples: - profile all \n"
                  "          - profile 2   \n");


    
       
    CLI_defineCmd("MEA analyzer generic set modify",
        (CmdFunc)MEA_CLI_analyzer_generic_set, 
        "    Analyzer generic stream configuration\n",
        "         <Id>   - specific Id \n"
        "       [-active <enable>]                        \n"
        "       [-reset_seq <enable> <value>]             \n"
        "       [-reset_prbs <enable>]                    \n"
        " Description:\n"
        "-------------\n"
        "      -active  (Active analyzer stream)\n"
        "               <enable> 0- disable 1- enable \n"
        "      -reset_seq    reset the sequence number\n" 
        "               <enable> 0- disable 1- enable\n"
        "               <value>  number to start the next sequence \n"
        "      -reset_prbs  reset the PRBS \n"
        "               <enable> 0- disable 1- enable\n"
        "Examples: - modify 1 -active 1 -reset_prbs 0 -reset_seq 0 0\n"
        "          - modify 2 -reset_seq  1 100 \n"
        "          - modify 3 -reset_prbs 1    \n");

    CLI_defineCmd("MEA analyzer generic show active",
        (CmdFunc)MEA_CLI_analyzer_generic_Show, 
        "    Show analyzer generic stream configuration\n",
        "       active  all|<Id>   - specific Id \n"
        "                              \n"
        "Examples: - show active all \n"
        "          - show active 2  \n"
        "                          \n");

    
    


    return MEA_OK;
}




static MEA_Status mea_Cli_Analyzer_CommandResult(int              argc,
                                            char                   *argv[],
                                            MEA_Bool               flagType,
                                            MEA_Uint16             startIndex,
                                            MEA_Analyzer_configure_dbt  *entry)
{
    int i;
    int num_of_params;
    


    for(i=(startIndex); i<argc ;i++){

        if(!MEA_OS_strcmp(argv[i],"-active")){ 

            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->activate= MEA_OS_atoi(argv[i+1]);


            i += num_of_params;
            continue;

        }

        if(!MEA_OS_strcmp(argv[i],"-reset_prbs")){ 

            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->resetPrbs  = MEA_OS_atoi(argv[i+1]);

            i += num_of_params;
            continue;

        }
        if(!MEA_OS_strcmp(argv[i],"-reset_seq")){ 

            num_of_params=2;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->enable_clearSeq  = MEA_OS_atoi(argv[i+1]);
            entry->value_nextSeq    = MEA_OS_atoi(argv[i+2]);

            i += num_of_params;
            continue;

        }



    }//end for


    return MEA_OK;
}
static MEA_Status MEA_CLI_analyzer_generic_set(int argc, char *argv[])
{
    
    MEA_Analyzer_t                id;
    MEA_Analyzer_configure_dbt    entry;
    MEA_Uint32 i;
    
    if (argc < 2) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&entry,0,sizeof(entry));
    

    id= MEA_OS_atoi(argv[1]);
    for (i=1; i < (MEA_Uint32)argc; i++) {

        if(  (argv[i][0] == '-'        )   && (
            (MEA_OS_strcmp(argv[i],"-active")        != 0) &&
            (MEA_OS_strcmp(argv[i],"-reset_prbs")        != 0) &&
            (MEA_OS_strcmp(argv[i],"-reset_seq")        != 0) )){
            CLI_print(" ERROR Not support option %s \n",argv[i]);
        return MEA_OK;
        }    
    }

    if(MEA_API_Get_Analyzer(MEA_UNIT_0,id,&entry)!=MEA_OK){
        CLI_print("Error: MEA_API_Get_Analyzer %d filed\n",argv[i]);
        return MEA_OK;
    }
    if(mea_Cli_Analyzer_CommandResult(argc,argv,MEA_FALSE,
        2,&entry)!=MEA_OK){
            CLI_print("Error: mea_PacketGenCommandResult \n");
            return MEA_OK;
    } 
    
    

    if(MEA_API_Set_Analyzer(MEA_UNIT_0,id,
                            &entry)!=MEA_OK){
        CLI_print("Error: MEA_API_Set_Analyzer %d filed\n",id);
        return MEA_OK;
    }
    
    CLI_print("Done : id=%d \n",id);
    return MEA_OK;
}





static MEA_Status MEA_CLI_analyzer_generic_Show(int argc, char *argv[])
{
    
    MEA_Analyzer_t          id_start,id_end,i;
    MEA_Bool                exist;
    MEA_Uint32              counter;
    
    if (argc < 2) {
        return MEA_ERROR;
    }
    
    if(!MEA_OS_strcmp(argv[1],"all")){
     id_start=0;
     id_end= MEA_ANALYZER_MAX_STREAMS_TYPE1-1;
    }else{
        id_start=id_end= MEA_OS_atoi(argv[1]);
        if(id_start >=MEA_ANALYZER_MAX_STREAMS_TYPE1){
            CLI_print(" Analyzer id=%d is out of range \n",id_start);
            return MEA_ERROR;

        }
    }
    counter=0;
    for (i=id_start;i<=id_end;i++)
    {
        if(MEA_API_IsExist_Analyzer(MEA_UNIT_0,i,&exist)!=MEA_OK){
            CLI_print(" ERROR Analyzer id=%d error \n",i);
            return MEA_ERROR;
        }
        if(!exist)
            continue;

        if(counter==0){
            CLI_print(" Analyzer    \n"
                      " ------------\n"
                      " ID   Active \n"
                      " ---- ------ \n");
        }
        counter++;
       CLI_print(" %4d %s\n",i,MEA_STATUS_STR(exist));


   }
    
    
    
    
    
    
    return MEA_OK;
}


static MEA_Status mea_PacketGen_prof_info_CommandResult(int                 argc,
                                             char               *argv[],
                                             MEA_Bool            flagType,
                                             MEA_Uint16          startIndex,
                                MEA_PacketGen_drv_Profile_info_entry_dbt         *entry)
{
    int i;
    int num_of_params;
    
    
    for(i=(startIndex); i<argc ;i++){

      
        
        if(!MEA_OS_strcmp(argv[i],"-len")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->header_length  = MEA_OS_atoi(argv[i+1]);
            i += num_of_params;
            continue;
            
        }
        if(!MEA_OS_strcmp(argv[i],"-stream_type")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->stream_type  = MEA_OS_atoi(argv[i+1]);
            i += num_of_params;
            continue;
            
        }

        if(!MEA_OS_strcmp(argv[i],"-prbs")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->forceErrorPrbs  = MEA_OS_atoi(argv[i+1]);
            i += num_of_params;
            continue;
            
        }
        
        if(!MEA_OS_strcmp(argv[i],"-seq")){ 
            
            num_of_params=2;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->seq_stamping_enable  = MEA_OS_atoi(argv[i+1]);
            entry->seq_offset_type     = MEA_OS_atoi(argv[i+2]);
            i += num_of_params;
            continue;
        
        }
        
        
    
    }//end for


    return MEA_OK;
}



static MEA_Status MEA_CLI_PacketGen_prof_info_Delete(int argc, char *argv[])
{
    
    MEA_Uint32  PacketGenId;
    MEA_Uint32  start_PacketGen,end_PacketGen;
    MEA_Bool    exist;
    MEA_Uint32  count; 

     


    if (argc < 2) {
       return MEA_ERROR;
    }

    if(!MEA_OS_strcmp(argv[1],"all")){
     start_PacketGen=0;
     end_PacketGen=(MEA_PACKETGEN_MAX_PROFILE-1);
    }    
    else{
        start_PacketGen=(MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_PacketGen=start_PacketGen;
        if ((MEA_API_IsExist_PacketGen_Profile_info(MEA_UNIT_0,(MEA_PacketGen_t)start_PacketGen,&exist)!=MEA_OK) ||
            (!exist)) {
            CLI_print(" ERROR: Profile_info id %d is not exist \n",start_PacketGen);
            return MEA_OK;
        }
    }
    count=0;
    for(PacketGenId=start_PacketGen; PacketGenId <=end_PacketGen; PacketGenId++){    
        if(MEA_API_IsExist_PacketGen_Profile_info(MEA_UNIT_0,(MEA_PacketGen_profile_info_t)PacketGenId,&exist)==MEA_OK){
            if (exist){

                if(MEA_API_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,(MEA_PacketGen_profile_info_t)PacketGenId )!=MEA_OK){
                   CLI_print(" ERROR: can't Delete Profile_info_Entry id %d \n",PacketGenId);
                }
                count++;
            }
        }
    }
    
    if(count== 0){
        CLI_print("Profile_info Table is empty\n");
    }


    
    return MEA_OK;
}
static MEA_Status MEA_CLI_PacketGen_prof_info_Modify(int argc, char *argv[])
{
    int i;
    MEA_PacketGen_drv_Profile_info_entry_dbt        entry;
    MEA_PacketGen_profile_info_t                    Id;
    MEA_Bool                                        exist;

     if (argc < 2) {
       return MEA_ERROR;
    }
    
    Id=MEA_OS_atoi(argv[1]);


    if(MEA_API_IsExist_PacketGen_Profile_info(MEA_UNIT_0,Id,&exist)!=MEA_OK){
        CLI_print("ERROR: MEA_API_IsExist_PacketGen_Profile_info id=%d fail\n",Id);
            return MEA_OK;
    }
    if(!exist){
        CLI_print("ERROR: PacketGen_Profile_info id=%d not exist\n",Id);
            return MEA_OK;
    }
                                        
    for (i=1; i < argc; i++) {
         
        if(  (argv[i][0] == '-'        )   && (
            
           
            (MEA_OS_strcmp(argv[i],"-len")            != 0) &&
            (MEA_OS_strcmp(argv[i],"-prbs")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-stream_type")    != 0) &&
            (MEA_OS_strcmp(argv[i],"-seq")            != 0) )){
                CLI_print("ERROR: Not support option %s \n",argv[i]);
            return MEA_OK;
        }
    }
    
    if(MEA_API_Get_PacketGen_Profile_info_Entry(MEA_UNIT_0,Id,&entry)!=MEA_OK){
        CLI_print("ERROR: MEA_API_Get_PacketGen_Profile_info_Entry fail %d \n",Id);
        return MEA_OK;
    }
      /*get parameters from User*/
    if(mea_PacketGen_prof_info_CommandResult(argc,argv,MEA_FALSE,
        2,&entry)!=MEA_OK){
       CLI_print("Error: mea_PacketGenCommandResult \n");
        return MEA_OK;
    }

    if(MEA_API_Set_PacketGen_Profile_info_Entry(MEA_UNIT_0,Id,&entry)!=MEA_OK){
        CLI_print("Error: MEA_API_Set_PacketGen_Profile_info_Entry id %d \n",Id);
        return MEA_OK;
    }


    CLI_print("Done\n");
    return MEA_OK;
}

static MEA_Status MEA_CLI_PacketGen_prof_info_Create(int argc, char *argv[])
{
    int i;
   
    MEA_PacketGen_drv_Profile_info_entry_dbt      entry;
    MEA_PacketGen_profile_info_t              Id_io;

     if (argc < 2) {
       return MEA_ERROR;
    }
     
    for (i=1; i < argc; i++) {
         
        if(  (argv[i][0] == '-'        )   && (
            
           
            (MEA_OS_strcmp(argv[i],"-len")            != 0) &&
            (MEA_OS_strcmp(argv[i],"-prbs")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-stream_type")    != 0) &&
            (MEA_OS_strcmp(argv[i],"-seq")          != 0) )){
                CLI_print("ERROR: Not support option %s \n",argv[i]);
            return MEA_OK;
        }
    }

     if(!MEA_OS_strcmp(argv[1],"auto")){ /* only for create*/
            Id_io=MEA_PLAT_GENERATE_NEW_ID;
    }else{
     Id_io=MEA_OS_atoi(argv[1]);
    }
    MEA_OS_memset(&entry,0,sizeof(entry));
     entry.header_length = 56;
    
     /*get parameters from User*/
    if(mea_PacketGen_prof_info_CommandResult(argc,argv,MEA_FALSE,
        2,&entry)!=MEA_OK){
       CLI_print("Error: mea_PacketGenCommandResult \n");
        return MEA_OK;
    } 
        

    if (MEA_API_Create_PacketGen_Profile_info(MEA_UNIT_0,
                                      &entry,
                                      &Id_io)!=MEA_OK){
    CLI_print("Error: MEA_API_Create_PacketGen_Profile_info fail\n");
        return MEA_OK;
    }
    
    CLI_print("Done id %d\n",Id_io);
    return MEA_OK;
}




static MEA_Status MEA_CLI_PacketGen_prof_info_Show(int argc, char *argv[]){
    MEA_PacketGen_profile_info_t                  Id_i,start,end;
    MEA_PacketGen_drv_Profile_info_entry_dbt          entry;
    MEA_Bool                         exist;
    MEA_Uint32                       count;
    


    if ((argc < 2) || (argc > 3)) {
       return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        end=(MEA_PacketGen_t)(MEA_PACKETGEN_MAX_PROFILE-1);
    } else{
          Id_i=MEA_OS_atoi(argv[1]);
          if(Id_i>MEA_PACKETGEN_MAX_STREAMS){
          CLI_print("ERROR Id % is out of range\n",Id_i);
          return MEA_OK;
          }
          start=end=Id_i;
    }
    count=0;
    for(Id_i=start; Id_i<=end ;Id_i++){
        if (MEA_API_IsExist_PacketGen_Profile_info(MEA_UNIT_0,Id_i,&exist) != MEA_OK) {
             CLI_print("ERROR MEA_API_IsExist_PacketGen_Profile_info failed (id=%d)\n",Id_i);
             return MEA_OK;
        }
        if (!exist) {
            continue;
        }    
        
        if(MEA_API_Get_PacketGen_Profile_info_Entry(MEA_UNIT_0,Id_i,&entry)!=MEA_OK){
            CLI_print("ERROR MEA_API_IsExist_PacketGen_Profile_info failed (id=%d)\n",Id_i);
             return MEA_OK;
        }
        if (count++ == 0) {
            CLI_print("\n");
            CLI_print(" Id  header seq Stamping prbs \n"
                      "     len    en  offset   err  \n"
                      " --- ------ --- -------- ----  \n");
                    // 01234567890123456789012345678901234567890123456789012345678901234567890123456789
        }
        

        CLI_print(" %3d %6d %3s %5d %4d \n",Id_i,
            entry.header_length,
            MEA_STATUS_STR(entry.seq_stamping_enable),
            entry.seq_offset_type,
            entry.forceErrorPrbs
            );
        
    }
    
    if(count == 0){
        CLI_print("Profile_info Table is empty\n");
    }
    
    
    
    return MEA_OK;
}

static MEA_Status mea_PacketGenCommandResult(int                 argc,
                                             char               *argv[],
                                             MEA_Bool            flagType,
                                             MEA_Uint16          startIndex,
                                MEA_PacketGen_Entry_dbt         *entry)
{
    int i,j;
    int num_of_params;
    
    
    for(i=(startIndex); i<argc ;i++){

        if(!MEA_OS_strcmp(argv[i],"-data")){ /* only for create*/
            
            num_of_params=3;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->data.pattern_type   = MEA_OS_atoi(argv[i+1]);
             
            entry->data.pattern_value.s  = (MEA_Uint16)MEA_OS_atohex(argv[i+2]);;
            

            entry->data.withCRC        = MEA_OS_atoi(argv[i+3]);
            i += num_of_params;
            continue;
        }
        if(!MEA_OS_strcmp(argv[i],"-header")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->header.length=MEA_OS_atoi(argv[i+1]);
            if ((MEA_Uint32)(i+num_of_params + entry->header.length) > (MEA_Uint32)argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            if((entry->header.length > MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2) /*||(entry->header.length ==0)*/){
                CLI_print("Error: 1:length %d of header is out of range %d \n",entry->header.length,MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2);
                return MEA_ERROR;
            }


            if (((i+num_of_params + entry->header.length +1) <= (MEA_Uint32)argc) &&
                    (argv[i+num_of_params+entry->header.length +1][0] != '-' )  ) {
                    CLI_print("Error: 2:header data %d is out of range  \n",entry->header.length);
                return MEA_ERROR;

            }           
            
            if((entry->header.length > 0) && 
               (entry->header.length <=MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2 )){

               for(j=0 ; j<(int)(entry->header.length) ;j++){
                    
                    entry->header.data[j]= (MEA_Uint8)MEA_OS_atohex(argv[i+2+j]);
                }


            }else {
                if((entry->header.length == 0)){
                    for(j=0 ; j<MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2  ;j++){
                    
                    entry->header.data[j]= 0;
                }
                }
            } 
            num_of_params+=entry->header.length;
            i += num_of_params;
            continue;
            
        }
        if(!MEA_OS_strcmp(argv[i],"-len")){ 
            
            num_of_params=3;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->length.type  = MEA_OS_atoi(argv[i+1]);
            entry->length.start = MEA_OS_atoi(argv[i+2]);
            entry->length.end   = MEA_OS_atoi(argv[i+3]);
            if (((i+num_of_params+1) < argc) &&
                    (argv[i+4][0]        != '-' )  ) {
                    num_of_params++;
                    entry->length.step   = MEA_OS_atoi(argv[i+4]);
            }           
            
            i += num_of_params;
            continue;
            
        }
        if(!MEA_OS_strcmp(argv[i],"-count")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            
            entry->control.numOf_packets=MEA_OS_atoi(argv[i+1]);
            if (((i+num_of_params+1) < argc) &&
                    (argv[i+2][0]        != '-' ) && MEA_OS_strcmp(argv[i+2],"")) {
                    num_of_params++;
                    entry->control.Burst_size   = MEA_OS_atoi(argv[i+2]);
            }else {
              
            }
            
            i += num_of_params;
            continue;
        }
        if(!MEA_OS_strcmp(argv[i],"-next")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            
            entry->control.next_packetGen_valid=MEA_OS_atoi(argv[i+1]);
            if (((i+num_of_params+1) < argc) &&
                    (argv[i+2][0]        != '-' )  ) {
                    num_of_params++;
                    entry->control.next_packetGen_id   = MEA_OS_atoi(argv[i+2]);
            }
            
            i += num_of_params;
            continue;
        }
        
        
        if(!MEA_OS_strcmp(argv[i],"-profid")){ 
            
            num_of_params=3;
            if ((i+num_of_params )>= argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->profile_info.valid =MEA_OS_atoi(argv[i+1]);
            entry->profile_info.id =MEA_OS_atoi(argv[i+2]);
            entry->profile_info.headerType =MEA_OS_atoi(argv[i+3]);
            
            
            i += num_of_params;
            continue;
        }
        if(!MEA_OS_strcmp(argv[i],"-reset_prbs")){ 

            num_of_params=1;
            if ((i+num_of_params )>= argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->setting.resetPrbs =MEA_OS_atoi(argv[i+1]);
            

            i += num_of_params;
            continue;
        }
        if(!MEA_OS_strcmp(argv[i],"-active")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->control.active =MEA_OS_atoi(argv[i+1]);
            
            
            i += num_of_params;
            continue;
        }
        if(!MEA_OS_strcmp(argv[i],"-error")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->control.Packet_error =MEA_OS_atoi(argv[i+1]);
            i += num_of_params;
            continue;
        }
        if(!MEA_OS_strcmp(argv[i],"-loop")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->control.loop =MEA_OS_atoi(argv[i+1]);
            
            
            i += num_of_params;
            continue;
        }

        if(!MEA_OS_strcmp(argv[i],"-seqId")){ 
           
            num_of_params=2;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
          
            //entry->seqIdStamp.type    = MEA_OS_atoi(argv[i+1]);
            //entry->seqIdStamp.loction_offset  = MEA_OS_atoi(argv[i+2]);
            if(!MEA_OS_strcmp(argv[i+1],"D.C")){
                entry->seqIdStamp.start_valid=MEA_FALSE;
                entry->seqIdStamp.start_value   = 0;
            }else {
                entry->seqIdStamp.start_valid=MEA_TRUE;
                entry->seqIdStamp.start_value   = MEA_OS_atohex(argv[i+1]);
            }
            if(!MEA_OS_strcmp(argv[i+2],"D.C")){
                entry->seqIdStamp.end_valid = MEA_FALSE;
                entry->seqIdStamp.end_Value       = 0;
            }else {
                entry->seqIdStamp.end_valid = MEA_TRUE;
                entry->seqIdStamp.end_Value = MEA_OS_atohex(argv[i+2]);
            }
            if (((i+num_of_params+1) <= argc) &&
                    (argv[i+3][0]        != '-' )  ) {
                    num_of_params++;
                    entry->seqIdStamp.step_value   = MEA_OS_atoi(argv[i+3]);
            }
            
            i += num_of_params;
            continue;
        }
        if(!MEA_OS_strcmp(argv[i],"-time")){ 
            
            num_of_params=2;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->timeStamp.type=MEA_OS_atoi(argv[i+1]);
            entry->timeStamp.loction_offset=MEA_OS_atoi(argv[i+2]);
            
            i += num_of_params;
            continue;
        }
        if(MEA_OS_strcmp(argv[i],"-s") == 0){

            
            /* */
            
            if (argc - i < 2)   {
               CLI_print ("Error: Minimum parameters for option -s is missing\n");
               return MEA_ERROR;
            }
            
            if (MEA_OS_atoi(argv[i+1]) != 0)  {/* if enable need more parameters*/
                entry->shaper.shaper_enable = ENET_TRUE;
                if (argc - i < 8)   {
                    CLI_print ("Error: Minimum parameters for option -s is missing\n");
                    return MEA_ERROR;
                }
                entry->shaper.shaper_info.CIR  = MEA_OS_atoiNum64(argv[i+2]) ;     
                entry->shaper.shaper_info.CBS = MEA_OS_atoiNum(argv[i + 3]);
                
                switch (MEA_OS_atoi(argv[i+4]))
                {
                    case 0:
                        entry->shaper.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_PKT;  
                        break;
                    case 1:
                    default:
                        entry->shaper.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
                        break;
                }
                
                switch (MEA_OS_atoi(argv[i+5]))
                {
                    case 0:
                        entry->shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_CELL;  
                        break;
                    case 3:
                        entry->shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_AAL5;  
                        break;

                    case 1:
                    default:
                        entry->shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
                        break;
                }
            entry->shaper.shaper_info.overhead = MEA_OS_atoi(argv[i+6]) ;
            entry->shaper.shaper_info.Cell_Overhead = MEA_OS_atoi(argv[i+7]);
            }else {
                entry->shaper.shaper_enable = MEA_FALSE;
            }
            
        }
    
    }


return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_PacketGen_Create                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_PacketGen_Create(int argc, char *argv[])
{
    int                           i;
    
    MEA_PacketGen_t               Id_io;
    MEA_PacketGen_Entry_dbt       entry;
    MEA_Uint8      data[MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2];
    
    MEA_Uint16     EtherType;
    MEA_Uint16     vlanId;

    if (argc < 2) {
       return MEA_ERROR;
    }
   
        for (i=1; i < argc; i++) {
         
        if(  (argv[i][0] == '-'        )   && (
            
            (MEA_OS_strcmp(argv[i],"-header")         != 0) &&
            (MEA_OS_strcmp(argv[i],"-data")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-len")            != 0) &&
            (MEA_OS_strcmp(argv[i],"-error")          != 0) &&
            (MEA_OS_strcmp(argv[i],"-count")          != 0) &&
            (MEA_OS_strcmp(argv[i],"-next")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-active")         != 0) &&
            (MEA_OS_strcmp(argv[i],"-seqId")          != 0) &&
            (MEA_OS_strcmp(argv[i],"-s")              != 0) &&
            (MEA_OS_strcmp(argv[i],"-loop")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-profid")         != 0) &&
            (MEA_OS_strcmp(argv[i],"-reset_prbs")         != 0) &&
            (MEA_OS_strcmp(argv[i],"-time")           != 0))){
                CLI_print("ERROR: Not support option %s \n",argv[i]);
            return MEA_OK;
        }
    }

    MEA_OS_memset(&entry,0,sizeof(entry));
    MEA_OS_memset(&data,0,sizeof(data));
    
    entry.header.data[0] = 0x01;
    entry.header.data[1] = 0x02;
    entry.header.data[2] = 0x03;
    entry.header.data[3] = 0x04;
    entry.header.data[4] = 0x05;
    entry.header.data[5] = 0x06;


    entry.header.data[6]  = 0x11;
    entry.header.data[7]  = 0x12;
    entry.header.data[8]  = 0x13;
    entry.header.data[9]  = 0x14;
    entry.header.data[10] = 0x15;
    entry.header.data[11] = 0x16;
    
    EtherType = 0x8100;
    vlanId    = 0x0007;

    
    
    entry.header.data[12] = (MEA_Uint8)((EtherType & 0xff00 ) >>8 ) ;
    entry.header.data[13] = (MEA_Uint8)((EtherType & 0x00ff ) >>0 );
    entry.header.data[14] = (MEA_Uint8)((vlanId    & 0xff00 ) >>8 );
    entry.header.data[15] = (MEA_Uint8)((vlanId    & 0x00ff ) >>0 );


    /* Add the Default parameter*/
    entry.length.start = 64;
    entry.length.end   = 64;
    entry.length.step  = 1;
    entry.length.type  = MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE;
    
    entry.data.pattern_type     =  MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE;
    entry.data.pattern_value.s  =  MEA_PACKET_GEN_DATA_PATTERN_VALUE_0000 ;
    

    entry.data.withCRC        =  MEA_PACKETGEN_DEF_WITH_CRC ;
    
    entry.header.length       = 16;



    entry.control.Burst_size     = MEA_PACKETGEN_DEF_BURST_SIZE;
    entry.control.numOf_packets  = 10;
    entry.control.active         = MEA_TRUE;
    
    //entry.seqIdStamp.type    = MEA_PACKETGEN_DEF_SEQ_ID ;   
    //entry.timeStamp.type     = MEA_PACKETGEN_DEF_TIME_STAMP;
    
    if(!MEA_OS_strcmp(argv[1],"auto")){ /* only for create*/
            Id_io=MEA_PLAT_GENERATE_NEW_ID;
          
    }else{
     Id_io=MEA_OS_atoi(argv[1]);
    }

    /*get parameters from User*/
    if(mea_PacketGenCommandResult(argc,argv,MEA_FALSE,
        2,&entry)!=MEA_OK){
       CLI_print("Error: mea_PacketGenCommandResult \n");
        return MEA_OK;
    }

    if(MEA_API_Create_PacketGen_Entry(MEA_UNIT_0,
                                      &entry,
                                      &Id_io)!=MEA_OK){
        CLI_print("Error: can't create PacketGen \n");
        return MEA_OK;
    }

    CLI_print("Done create PacketGen with Id = %d  \n",Id_io); 
    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_PacketGen_Modify                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_PacketGen_Modify(int argc, char *argv[])
{
    
    MEA_PacketGen_t              Id_i;
    int                          i;
    MEA_PacketGen_Entry_dbt       entry;
    
   
    if (argc < 2) {
       return MEA_ERROR;
    }
    
    
   Id_i=MEA_OS_atoi(argv[1]);
    
    for (i=1; i < argc; i++) {
         
        if(  (argv[i][0] == '-'        )   && (
            (MEA_OS_strcmp(argv[i],"-header")          != 0) &&
            (MEA_OS_strcmp(argv[i],"-data")            != 0) &&
            (MEA_OS_strcmp(argv[i],"-len")              != 0) &&
            (MEA_OS_strcmp(argv[i],"-error")              != 0) &&
            (MEA_OS_strcmp(argv[i],"-count")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-next")            != 0) &&
            (MEA_OS_strcmp(argv[i],"-active")          != 0) &&
            (MEA_OS_strcmp(argv[i],"-seqId")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-s")              != 0) &&
            (MEA_OS_strcmp(argv[i],"-loop")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-reset_prbs")         != 0) &&
            (MEA_OS_strcmp(argv[i],"-profid")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-time")            != 0)   )        ){
                CLI_print("ERROR: Not support option %s \n",argv[i]);
            return MEA_OK;
        }
    }
    /*get old parameters */
    if(MEA_API_Get_PacketGen_Entry(MEA_UNIT_0,
                                Id_i,
                                &entry)!=MEA_OK){
            CLI_print(" MEA_API_Get_PacketGen_Entry %d failed\n",Id_i);
            return MEA_OK;
    }
    
      /*get parameters from User*/
    if(mea_PacketGenCommandResult(argc,argv,MEA_TRUE,
        2,&entry)!=MEA_OK){
       CLI_print("Error: mea_PacketGenCommandResult failed \n");
        return MEA_OK;
    }

    if(MEA_API_Set_PacketGen_Entry(MEA_UNIT_0,
                                   Id_i,
                                   &entry)!=MEA_OK){
    
       CLI_print("Error: MEA_API_Set_PacketGen_Entry failed \n");
        return MEA_OK;
    }
    CLI_print("Done Modify PacketGen with Id = %d  \n",Id_i);
       
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_PacketGen_Delete                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_PacketGen_Delete(int argc, char *argv[])
{

    MEA_Uint32  PacketGenId;
    MEA_Uint32  start_PacketGen,end_PacketGen;
    MEA_Bool    exist;
    MEA_Uint32  count;  


    if (argc < 2) {
       return MEA_ERROR;
    }

    if(!MEA_OS_strcmp(argv[1],"all")){
     start_PacketGen=0;
     if(MEA_PACKETGEN_MAX_STREAMS == 0){
         CLI_print(" ERROR: MEA_PACKETGEN_MAX_STREAMS = %d  \n",MEA_PACKETGEN_MAX_STREAMS);
            return MEA_OK;
     }
     end_PacketGen=(MEA_PACKETGEN_MAX_STREAMS-1);
    }    
    else{
        start_PacketGen=(MEA_LxCp_t)MEA_OS_atoi(argv[1]);;
        end_PacketGen=start_PacketGen;
        if ((MEA_API_IsExist_PacketGen(MEA_UNIT_0,(MEA_PacketGen_t)start_PacketGen,&exist)!=MEA_OK) ||
            (!exist)) {
            CLI_print(" ERROR: PacketGen id %d is not exist \n",start_PacketGen);
            return MEA_OK;
        }
    }
    count=0;
    for(PacketGenId=start_PacketGen; PacketGenId <=end_PacketGen; PacketGenId++){    
        if(MEA_API_IsExist_PacketGen(MEA_UNIT_0,(MEA_PacketGen_t)PacketGenId,&exist)==MEA_OK){
            if (exist){

                if(MEA_API_Delete_PacketGen_Entry(MEA_UNIT_0,(MEA_PacketGen_t)PacketGenId )!=MEA_OK){
                   CLI_print(" ERROR: can't Delete PacketGen_Entry id %d \n",PacketGenId);
                }
                count++;
            }
        }
    }
    
    if(count== 0){
        CLI_print("PacketGen Table is empty\n");
    }
    return MEA_OK;
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/*            <MEA_CLI_PacketGen_Show_Header>                               */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_PacketGen_Show_Header(int argc, char *argv[])
{
  
    MEA_PacketGen_t                 Id_i,start,end;
    MEA_PacketGen_Entry_dbt         entry;
    MEA_Bool                        exist;
    MEA_Uint32                      count;
    MEA_Uint32                      i,j;
    MEA_PacketGen_drv_Profile_info_entry_dbt  entry_profile;
    MEA_Uint32 len;
    
    if (!MEA_PACKETGEN_SUPPORT){
        CLI_print("MEA_PACKETGEN_SUPPORT : Not Suport \n");
        return MEA_OK;
    }

    len =MEA_PACKET_GEN_MAX_HEADER_LEN;

    if ((argc < 2) || (argc > 3)) {
       return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        end=(MEA_PacketGen_t)(MEA_PACKETGEN_MAX_STREAMS-1);
    } else{
          Id_i=MEA_OS_atoi(argv[1]);
          if(Id_i>MEA_PACKETGEN_MAX_STREAMS){
          CLI_print("ERROR Id % is out of range\n",Id_i);
          return MEA_OK;
          }
          start=end=Id_i;
    }
    count=0;
    for(Id_i=start; Id_i<=end ;Id_i++){
        if (MEA_API_IsExist_PacketGen(MEA_UNIT_0,Id_i,&exist) != MEA_OK) {
             CLI_print("ERROR MEA_API_IsExist_PacketGen failed (id=%d)\n",Id_i);
             return MEA_OK;
        }
        if (!exist) {
            continue;
        }    
        
        
        
        if(MEA_API_Get_PacketGen_Entry(MEA_UNIT_0,
                                    Id_i,
                                    &entry)!=MEA_OK){

        }
        if(MEA_API_Get_PacketGen_Profile_info_Entry(MEA_UNIT_0,entry.profile_info.id,
            &entry_profile)!=MEA_OK){
        }

        
        if (count++ == 0) {          
            CLI_print(" Id  header Index  Header data \n"
                      "     len                                                   \n"
                      " --- ------ ------ ----------------------------------------\n");
        }
        
        CLI_print(" %3d %6d \n",Id_i,entry_profile.header_length);
        if(entry.profile_info.headerType == 0){
            len =MEA_PACKET_GEN_MAX_HEADER_LEN;
        }
        if(entry.profile_info.headerType==1){
            len =MEA_PACKET_GEN_MAX_HEADER_LEN_TYPE2;
        } 
		
        for(i=0;i<len; i+=8){  
            CLI_print("           %02x: ",i);
            for (j=i;j<i+8;j++) {
                CLI_print("%02x ",entry.header.data[j]);
            }
            for (j=i;j<i+8;j++) {
                CLI_print("%c",MEA_OS_isprint(entry.header.data[j]) ? entry.header.data[j] : '.');
            }
            CLI_print("\n");
       }
       
          CLI_print("----------------------------------------------------------\n");
    }
    
    if(count == 0){
        CLI_print("packetGen Table is empty\n");
    }

    return MEA_OK;
}
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*            <MEA_CLI_PacketGen_Show_Controls>                             */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_PacketGen_Show_SeqId(int argc, char *argv[])
{
    
    MEA_PacketGen_t                  Id_i,start,end;
    MEA_PacketGen_Entry_dbt          entry;
    MEA_Bool                         exist;
    MEA_Uint32                       count;
    MEA_PacketGen_drv_Profile_info_entry_dbt  entry_profile;


    if ((argc < 2) || (argc > 3)) {
       return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        end=(MEA_PacketGen_t)(MEA_PACKETGEN_MAX_STREAMS-1);
    } else{
          Id_i=MEA_OS_atoi(argv[1]);
          if(Id_i>MEA_PACKETGEN_MAX_STREAMS){
          CLI_print("ERROR Id % is out of range\n",Id_i);
          return MEA_OK;
          }
          start=end=Id_i;
    }
    count=0;
    for(Id_i=start; Id_i<=end ;Id_i++){
        if (MEA_API_IsExist_PacketGen(MEA_UNIT_0,Id_i,&exist) != MEA_OK) {
             CLI_print("ERROR MEA_API_IsExist_PacketGen failed (id=%d)\n",Id_i);
             return MEA_OK;
        }
        if (!exist) {
            continue;
        }    
        
        if(MEA_API_Get_PacketGen_Entry(MEA_UNIT_0,
                                    Id_i,
                                    &entry)!=MEA_OK){

        }
        if(MEA_API_Get_PacketGen_Profile_info_Entry(MEA_UNIT_0,entry.profile_info.id,
            &entry_profile)!=MEA_OK){
        }
        if (count++ == 0) {
            CLI_print("\n");
            CLI_print(" Id  seqIdStamp\n"
                      " --- ------ ------------- -------------------------\n"
                      "                 start start    End   End      step\n"
                      "     Type offset valid Value(h) valid Value(h)      \n"
                      " --- ---- ------ ----- -------- ----- -------- ---- \n");
                    // 01234567890123456789012345678901234567890123456789012345678901234567890123456789
        }
        
        CLI_print(" %3d %4d %6d %5d %8x %5d %8x %4d\n",Id_i,
            entry_profile.seq_stamping_enable,
            (entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_NONE)                        ? 0 :   
            (entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_1) ?  18 :
            (entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_2) ?  22 :
            (entry_profile.seq_offset_type == MEA_PACKETGEN_SEQ_TYPE_3) ?  26 : 0,
            
            entry.seqIdStamp.start_valid,
            entry.seqIdStamp.start_value,
            entry.seqIdStamp.end_valid,
            entry.seqIdStamp.end_Value,
            entry.seqIdStamp.step_value
            );
        
    }
    
    if(count == 0){
        CLI_print("packetGen Table is empty\n");
    }

    
    
    return MEA_OK;
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/*            <MEA_CLI_PacketGen_Show_prof>                             */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_PacketGen_Show_prof(int argc, char *argv[]){
    MEA_PacketGen_t                  Id_i,start,end;
    MEA_PacketGen_Entry_dbt          entry;
    MEA_Bool                         exist;
    MEA_Uint32                       count;
    


    if ((argc < 2) || (argc > 3)) {
       return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        end=(MEA_PacketGen_t)(MEA_PACKETGEN_MAX_STREAMS-1);
    } else{
          Id_i=MEA_OS_atoi(argv[1]);
          if(Id_i>MEA_PACKETGEN_MAX_STREAMS){
          CLI_print("ERROR Id % is out of range\n",Id_i);
          return MEA_OK;
          }
          start=end=Id_i;
    }
    count=0;
    for(Id_i=start; Id_i<=end ;Id_i++){
        if (MEA_API_IsExist_PacketGen(MEA_UNIT_0,Id_i,&exist) != MEA_OK) {
             CLI_print("ERROR MEA_API_IsExist_PacketGen failed (id=%d)\n",Id_i);
             return MEA_OK;
        }
        if (!exist) {
            continue;
        }    
        
        if(MEA_API_Get_PacketGen_Entry(MEA_UNIT_0,
                                    Id_i,
                                    &entry)!=MEA_OK){

        }
        if (count++ == 0) {
            CLI_print("\n");
            CLI_print("      profile   \n"
                      " -------------------\n"
                      " stream prof header \n"
                      " Id     Id   Type   \n"
                      " ------ ---- ------- \n");
                    // 01234567890123456789012345678901234567890123456789012345678901234567890123456789
        }
        
        CLI_print(" %6d %4d %7d \n",Id_i,
            entry.profile_info.id,
            entry.profile_info.headerType
            );
        
    }
    
    if(count == 0){
        CLI_print("packetGen Table is empty\n");
    }

    
    

return MEA_OK;

}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/*            <MEA_CLI_PacketGen_Show_Controls>                             */
/*                                                                          */
/*--------------------------------------------------------------------------*/

static MEA_Status MEA_CLI_PacketGen_Show_Controls(int argc, char *argv[])
{
    
    MEA_PacketGen_t                  Id_i,start,end;
    MEA_PacketGen_Entry_dbt          entry;
    MEA_Bool                         exist;
    MEA_Uint32                       count;
    


    if ((argc < 2) || (argc > 3)) {
       return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        end=(MEA_PacketGen_t)(MEA_PACKETGEN_MAX_STREAMS-1);
    } else{
          Id_i=MEA_OS_atoi(argv[1]);
          if(Id_i>MEA_PACKETGEN_MAX_STREAMS){
          CLI_print("ERROR Id % is out of range\n",Id_i);
          return MEA_OK;
          }
          start=end=Id_i;
    }
    count=0;
    for(Id_i=start; Id_i<=end ;Id_i++){
        if (MEA_API_IsExist_PacketGen(MEA_UNIT_0,Id_i,&exist) != MEA_OK) {
             CLI_print("ERROR MEA_API_IsExist_PacketGen failed (id=%d)\n",Id_i);
             return MEA_OK;
        }
        if (!exist) {
            continue;
        }    
        
        if(MEA_API_Get_PacketGen_Entry(MEA_UNIT_0,
                                    Id_i,
                                    &entry)!=MEA_OK){

        }
        if (count++ == 0) {
            CLI_print("\n");
            CLI_print(" Id  header Data          Control                           Length       shaper\n"
                      " --- ------ ------------- ----------------------------- ---------------- ------\n"
                      "                              numof Loop Burst next Err                        \n"
                      "     len    Type val  CRC act pkt        Size  Id   Pkt type start stop  valid \n"
                      " --- ------ ---- ---- --- --- ----- ---- ----- ---- --- ---- ----- ----- ------\n");
                    // 01234567890123456789012345678901234567890123456789012345678901234567890123456789
        }
        

        CLI_print(" %3d %6d %4d %4x %-3s %-3s %5d %5d %4d ",Id_i,
            entry.header.length,
            entry.data.pattern_type,
            entry.data.pattern_value.s,
            MEA_STATUS_STR(entry.data.withCRC),
            MEA_STATUS_STR(entry.control.active),
            entry.control.numOf_packets,
            entry.control.loop,
            entry.control.Burst_size
            );
        if(entry.control.next_packetGen_valid ==MEA_TRUE){ 
           CLI_print("%4d ",entry.control.next_packetGen_id);
        }else{
            CLI_print("%-4s ","NA");
        }
        CLI_print("%-3s ",MEA_STATUS_STR(entry.control.Packet_error));

        CLI_print("%4d %5d %5d %-6s \n",entry.length.type,
                                        entry.length.start,
                                        entry.length.end,
                                        MEA_STATUS_STR(entry.shaper.shaper_enable));
    }
    
    if(count == 0){
        CLI_print("packetGen Table is empty\n");
    }
    return MEA_OK;
}
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*            <MEA_CLI_PacketGen_Show_Shaper>                               */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_PacketGen_Show_Shaper(int argc, char *argv[])
{

    MEA_PacketGen_t                  Id_i,start,end;
    MEA_PacketGen_Entry_dbt          entry;
    MEA_Bool                         exist;
    MEA_Uint32                       count;
    
    
    
    if ((argc < 2) || (argc > 3)) {
       return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        end=(MEA_PacketGen_t)(MEA_PACKETGEN_MAX_STREAMS-1);
    } else{
          Id_i=MEA_OS_atoi(argv[1]);
          if(Id_i>MEA_PACKETGEN_MAX_STREAMS){
          CLI_print("ERROR Id % is out of range\n",Id_i);
          return MEA_OK;
          }
          start=end=Id_i;
    }
    count=0;
    for(Id_i=start; Id_i<=end ;Id_i++){
        if (MEA_API_IsExist_PacketGen(MEA_UNIT_0,Id_i,&exist) != MEA_OK) {
             CLI_print("ERROR MEA_API_IsExist_PacketGen failed (id=%d)\n",Id_i);
             return MEA_OK;
        }
        if (!exist) {
            continue;
        }    
        
        if(MEA_API_Get_PacketGen_Entry(MEA_UNIT_0,
                                    Id_i,
                                    &entry)!=MEA_OK){
        }
        if (count++ == 0) {
            CLI_print("\n");
            CLI_print ("Pac  ena Comp Spr ******* Shaper profile *******\n"
                       " Id      Type Id  Cir(Pps)      Cbs   type overhead\n"
                       "---- --- ---- --- ------------ ----- ---- --------\n");  
        }
        if(entry.shaper.shaper_enable){ 
             CLI_print("%4d %3s %4d %3d %12llu %5d %4s %8d\n",
                        Id_i,
                        "on",
                        entry.shaper.Shaper_compensation,
                        entry.shaper.shaper_Id,
                        entry.shaper.shaper_info.CIR,
                        entry.shaper.shaper_info.CBS,
                        (entry.shaper.shaper_info.resolution_type==MEA_SHAPER_RESOLUTION_TYPE_BIT) 
                        ? "bps" : "pps",
                      entry.shaper.shaper_info.overhead);
         }else{
            CLI_print("%4d %3s\n",Id_i,"off");
         }   
    }
    
    if(count == 0){
        CLI_print("packetGen Table is empty\n");
    }
    return MEA_OK;
}








