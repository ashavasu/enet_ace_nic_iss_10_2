/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "mea_cli_flowMarkingMapping.h"
#include "cli_eng.h"
#include "mea_cli.h"

MEA_Status MEA_CLI_FlowMarkingMappingProfile_Create(int argc, char *argv[])
{

    MEA_FlowMarkingMappingProfile_Entry_dbt entry;
    MEA_FlowMarkingMappingProfile_Id_t      id;
    int i;
    int num_of_params=0;
    MEA_Uint32 item;
    MEA_Uint32 valid;
    MEA_Uint32 L2_PRI;

    /* Check minimum number of parameters */
    if (argc < 3) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"auto") == 0) {
        id = MEA_PLAT_GENERATE_NEW_ID;
    } else {
        id = MEA_OS_atoi(argv[1]);
    }

    MEA_OS_memset(&entry,0,sizeof(entry));
    entry.type = MEA_OS_atoi(argv[2]);
    i=3;
    while(i<argc) {
        if (MEA_OS_strcmp(argv[i],"-i") == 0) {
            num_of_params=4;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            item  = MEA_OS_atoi(argv[i+1]);
            valid = MEA_OS_atoi(argv[i+2]);
            L2_PRI   = MEA_OS_atoi(argv[i+3]);
            if (item>=MEA_NUM_OF_ELEMENTS(entry.item_table)) {
                CLI_print("Error: <itemX> %d >= max value (%d) \n",
                          item,
                          MEA_NUM_OF_ELEMENTS(entry.item_table));
                return MEA_ERROR;
            }
            if ((valid != 0) && (valid != 1)) {
                CLI_print("Error: Invalid value for valid %d \n",valid);
                return MEA_ERROR;
            }
            if (L2_PRI > 7) {
                CLI_print("Error: Invalid value for Marking %d  \n",L2_PRI);
                return MEA_ERROR;
            }
            MEA_OS_memset(&(entry.item_table[item]),
                          0,
                          sizeof(entry.item_table[0]));
            entry.item_table[item].valid  = (MEA_Uint8)valid;
            entry.item_table[item].L2_PRI  = (MEA_Uint8)L2_PRI;
            i+=num_of_params;
        } else {
            CLI_print("Error: Invalid option\n");
            return MEA_ERROR;
        }

    }

    if (MEA_API_Create_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,&entry,&id) != MEA_OK) {
        CLI_print("Error: MEA_API_Create_FlowMarkingMappingProfile_Entry failed \n");
        return MEA_OK;
    }

    CLI_print("Done. (id=%d)\n",id);

    /* Return to Caller */
    return MEA_OK;
}

MEA_Status MEA_CLI_FlowMarkingMappingProfile_Modify(int argc, char *argv[])
{

    MEA_FlowMarkingMappingProfile_Entry_dbt entry;
    MEA_FlowMarkingMappingProfile_Id_t      id;
    int i;
    int num_of_params=0;
    MEA_Uint32 item;
    MEA_Uint32 valid;
    MEA_Uint32 L2_PRI;

    /* Check minimum number of parameters */
    if (argc < 2) {
        return MEA_ERROR;
    }



    id = MEA_OS_atoi(argv[1]);

    if (MEA_API_Get_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,id,&entry) != MEA_OK) {
        CLI_print("Error: MEA_API_Get_FlowMarkingMappingProfile_Entry failed \n");
        return MEA_OK;
    }

    i=2;
    while(i<argc) {
        if (MEA_OS_strcmp(argv[i],"-i") == 0) {
            num_of_params=4;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough paramter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            item  = MEA_OS_atoi(argv[i+1]);
            valid = MEA_OS_atoi(argv[i+2]);
            L2_PRI   = MEA_OS_atoi(argv[i+3]);
            if (item>=MEA_NUM_OF_ELEMENTS(entry.item_table)) {
                CLI_print("Error: <itemX> %d >= max value (%d) \n",
                          item,
                          MEA_NUM_OF_ELEMENTS(entry.item_table));
                return MEA_ERROR;
            }
            if ((valid != 0) && (valid != 1)) {
                CLI_print("Error: Invalid value for valid %d \n",valid);
                return MEA_ERROR;
            }
            if (L2_PRI > 7) {
                CLI_print("Error: Invalid value for Marking %d  \n",L2_PRI);
                return MEA_ERROR;
            }
            MEA_OS_memset(&(entry.item_table[item]),
                          0,
                          sizeof(entry.item_table[0]));
            entry.item_table[item].valid = (MEA_Uint8)valid;
            entry.item_table[item].L2_PRI   = (MEA_Uint8)L2_PRI;
            i+=num_of_params;
        } else {
            if (MEA_OS_strcmp(argv[i],"-t") == 0) {
                num_of_params=2;
                if ((i+num_of_params)>argc) {
                    CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                    return MEA_ERROR;
                }
                entry.type  = MEA_OS_atoi(argv[i+1]);
                i+=num_of_params;
            } else {
                CLI_print("Error: Invalid option\n");
                return MEA_ERROR;
            } 
        }

    }

    if (MEA_API_Set_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,id,&entry) != MEA_OK) {
        CLI_print("Error: MEA_API_Set_FlowMarkingMappingProfile_Entry failed \n");
        return MEA_OK;
    }


    /* Return to Caller */
    return MEA_OK;
}

MEA_Status MEA_CLI_FlowMarkingMappingProfile_Delete(int argc, char *argv[])
{

    MEA_FlowMarkingMappingProfile_Id_t      id;
    MEA_Bool                            found;

    /* Check minimum number of prarmaters */
    if (argc != 2) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        id = MEA_OS_atoi(argv[1]);
        if (MEA_API_Delete_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,id) != MEA_OK) {
            CLI_print("Error: MEA_API_Delete_FlowMarkingMappingProfile_Entry failed \n");
            return MEA_ERROR;
        }
    } else {

        if (MEA_API_GetFirst_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,
                                                         &id,
                                                         NULL,
                                                         &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetFirst_FlowMarkingMappingProfile_Entry failed \n");
            return MEA_OK;
        }
        while (found) {
            if (MEA_API_Delete_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,id) != MEA_OK) {
                CLI_print("Error: MEA_API_Delete_FlowMarkingMappingProfile_Entry failed \n");
                return MEA_OK;
            }
            if (MEA_API_GetNext_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,
                                                            &id,
                                                            NULL,
                                                            &found) != MEA_OK) {
                CLI_print("Error: MEA_API_GetNext_FlowMarkingMappingProfile_Entry failed \n");
                return MEA_OK;
            }
        }
    }


    /* Return to Caller */
    return MEA_OK;
}

MEA_Status MEA_CLI_FlowMarkingMappingProfile_Show(int argc, char *argv[])
{
    MEA_FlowMarkingMappingProfile_Entry_dbt entry;
    MEA_FlowMarkingMappingProfile_Id_t      id;
    MEA_Bool                            found;
    MEA_Uint32                          count;
    MEA_Uint32                          item;
    MEA_Bool                            all_flag=MEA_TRUE;

    static char                         type_str[MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_LAST][20];


    MEA_OS_strcpy(&(type_str[MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_L2_PRI_ETHERTYPE][0]),"L2 ETH");
    MEA_OS_strcpy(&(type_str[MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_L2_PRI_INNER_VLAN][0]),"L2 INNER VLAN");
    MEA_OS_strcpy(&(type_str[MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN][0]),"L2 OUTER VLAN");
    MEA_OS_strcpy(&(type_str[MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_PRECEDENCE][0])  ,"IPv4_PRECEDENCE");
    MEA_OS_strcpy(&(type_str[MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_DSCP][0])        ,"IPv4_DSCP");
    MEA_OS_strcpy(&(type_str[MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_IPv4_TOS][0])         ,"IPv4_TOS");



    /* Check minimum number of parameters */
    if (argc != 2) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        id = MEA_OS_atoi(argv[1]);
        if (MEA_API_Get_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,id,&entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_FlowMarkingMappingProfile_Entry failed \n");
            return MEA_ERROR;
        }
        found = MEA_TRUE;
        all_flag = MEA_FALSE;
    } else {
        if (MEA_API_GetFirst_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,
                                                         &id,
                                                         &entry,
                                                         &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetFirst_FlowMarkingMappingProfile_Entry failed \n");
            return MEA_OK;
        }
    }

    count=0;
    while (found) {
        if (count++ == 0) {
            CLI_print("ProfileId Type                 Item# Valid L2_PRI\n"
                      "--------- -------------------- ----- ----- ------\n");
        }
        CLI_print("%9d %20s ",id,type_str[entry.type]);
        for (item=0;item<(MEA_Uint32)MEA_FLOW_MARKING_MAPPING_NUM_OF_ITEMS(entry.type);item++) {
            if(item != 0){
                CLI_print("                               ");
            }
            CLI_print("%5d %-5s %6d\n",
                      item,
                      (entry.item_table[item].valid) ? "Yes" : "No",
                      entry.item_table[item].L2_PRI);
        }

        if (!all_flag) {
            break;
        }

        if (MEA_API_GetNext_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,
                                                        &id,
                                                        &entry,
                                                        &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetNext_FlowMarkingMappingProfile_Entry failed \n");
            return MEA_OK;
        }
        if (count!=0)
            CLI_print("--------- -------------------- ----- ----- ------\n");
    }

    if (count==0) {
        CLI_print("No Flow Marking Mapping Profiles\n");
        return MEA_OK;
    } 
    
    
    if (count!=0)
        CLI_print("Display %d Profiles \n",count);

    /* Return to Caller */
    return MEA_OK;
}


MEA_Status MEA_CLI_FlowMarkingMappingProfile_AddDebugCmds(void)
{


           CLI_defineCmd("MEA flow mapping Marking show",
                  (CmdFunc)MEA_CLI_FlowMarkingMappingProfile_Show, 
                  "Show   Flow Marking Mapping Profile(s) \n",
                  "show   <id>|all\n"
                  "--------------------------------------------------------------------------------\n"
                  "       <id>}all - Profile id  or all\n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - show all \n"
                  "          - show 2\n");

           CLI_defineCmd("MEA flow mapping Marking delete",
                  (CmdFunc)MEA_CLI_FlowMarkingMappingProfile_Delete, 
                  "Delete Flow Marking Mapping Profile(s) \n",
                  "delete <id>|all\n"
                  "--------------------------------------------------------------------------------\n"
                  "       <id>}all - Profile id  or all\n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - delete all \n"
                  "          - delete 2\n");

           CLI_defineCmd("MEA flow mapping Marking modify",
                  (CmdFunc)MEA_CLI_FlowMarkingMappingProfile_Modify, 
                  "Update Flow Marking Mapping Profile \n",
                  "modify <id> \n"
                  "       [-t <type> ]\n"
                  "       [-i <item<x>> <valid> <L2_PRI> ]\n"
                  "--------------------------------------------------------------------------------\n"
                  "       <id> - Profile id \n"
                  "\n"
                  "       (-t option) \n"
                  "            <type>    - Profile type (Index to mapping table)\n"
                  "                        0 - Layer2 priority EtherType\n"
                  "                        1 - Layer2 priority inner vlan\n"
                  "                        2 - Layer2 priority outer vlan\n"
                  "                        3 - IPv4 Precedence \n"
                  "                        4 - IPv4 DSCP \n"
                  "                        5 - IPv4 TOS \n"

                  "\n"
                  "       (-i option) \n"
                  "           <item<X> - item number (0..7/63) \n"
                  "                      Note: 0..7 for TOS/Precedence and 63 for DSCP \n"
                  "           <valid>  - 0 or 1 \n"
                  "           <L2_PRI> - Stamping Priority (0-7) \n"
                  "       Note: The -i option can repeat many time for any item we \n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - modify    0 \n"
                  "          - modify    0 -i  2 1  2 -i  3 1 7 \n"
                  "          - modify    2 -i 44 1  2 -i 45 1 3 \n");

           CLI_defineCmd("MEA flow mapping Marking create",
                  (CmdFunc)MEA_CLI_FlowMarkingMappingProfile_Create, 
                  "Create Flow Marking Mapping Profile \n",
                  "create auto|<id>  <type> \n"
                  "       [-i <item<x>> <valid> <L2_PRI> ]\n"
                  "--------------------------------------------------------------------------------\n"
                  "       auto|<id> - Profile id or auto to generate new id \n"
                  "       <type>    - Profile type (Index to mapping table)\n"
                  "                        0 - Layer2 priority EtherType\n"
                  "                        1 - Layer2 priority inner vlan\n"
                  "                        2 - Layer2 priority outer vlan\n"
                  "                        3 - IPv4 Precedence \n"
                  "                        4 - IPv4 DSCP \n"
                  "                        5 - IPv4 TOS \n"
                  "\n"
                  "       (-i option) \n"
                  "           <item<X> - item number (0..7/63) \n"
                  "                      Note: 0..7 for TOS/Precedence and 63 for DSCP \n"
                  "           <valid>  - 0 or 1 \n"
                  "           <L2_PRI> - Stamping Priority (0-7) \n"
                  "       Note: The -i option can repeat many time for any item we \n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - create auto 0 \n"
                  "          - create auto 0 -i  2 1  2 -i  3 1 7 \n"
                  "          - create    2 2 -i 44 1  2 -i 45 1 7 \n");


    return MEA_OK;
}

