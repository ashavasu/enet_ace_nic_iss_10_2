/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*---------------------------------------------------------------------------*/
/* Includes                                                                  */
/*---------------------------------------------------------------------------*/
#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli_debug_db.h"
#include "mea_db_drv.h" 
#include "mea_if_service_drv.h"
#include "mea_serviceEntry_drv.h"
#include "mea_action_drv.h"
#include "mea_bm_counters_drv.h"
#include "mea_policer_drv.h"
#include "mea_filter_drv.h"
#include "mea_PacketGen_drv.h"
#include "mea_editingMapping_drv.h"
/*---------------------------------------------------------------------------*/
/* Defines                                                                   */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Typedefs                                                                  */
/*---------------------------------------------------------------------------*/
typedef enum {
	MEA_DB_ENTRY_TYP_SERVICE = 0,
	MEA_DB_ENTRY_TYP_SERVICE_ENTRY = 1,
	MEA_DB_ENTRY_TYP_SERVICE_RANGE = 2,
	MEA_DB_ENTRY_TYP_ACTION = 3,
	MEA_DB_ENTRY_TYP_PM_PKT_BYTE = 4,
	MEA_DB_ENTRY_TYP_PM_PKT = 5,
	MEA_DB_ENTRY_TYP_POLICER_PROFILE = 6,
	MEA_DB_ENTRY_TYP_POLICER_PTR = 7,
	MEA_DB_ENTRY_TYP_POLICER = 8,
	MEA_DB_ENTRY_TYP_EHP_ETH = 9,
	MEA_DB_ENTRY_TYP_EHP_ATM = 10,
	MEA_DB_ENTRY_TYP_EHP_DA = 11,
	MEA_DB_ENTRY_TYP_EHP_SA_LSS = 12,
	MEA_DB_ENTRY_TYP_EHP_PROFILE_ID = 13,
	MEA_DB_ENTRY_TYP_EHP_PROFILE_STAMPING = 14,
	MEA_DB_ENTRY_TYP_EHP_PROFILE_SA_DSS = 15,
	MEA_DB_ENTRY_TYP_EHP_PROFILE_SA_MSS = 16,
	MEA_DB_ENTRY_TYP_EHP_PROFILE_ETHER_TYPE = 17,
	MEA_DB_ENTRY_TYP_EHP_PROFILE2_ID = 18,
	MEA_DB_ENTRY_TYP_EHP_PROFILE2_LM = 19,
	MEA_DB_ENTRY_TYP_FILTER = 20,
	MEA_DB_ENTRY_TYP_FILTER_CTX = 21,
	MEA_DB_ENTRY_TYP_FILTER_MASK = 22,
	MEA_DB_ENTRY_TYP_PACKETGEN = 23,
	MEA_DB_ENTRY_TYP_EDIT_MAPPING_PROF = 24,
	MEA_DB_ENTRY_TYP_EDIT_MAPPING = 25,
    MEA_DB_ENTRY_TYP_EDIT_FRAGMENT_SESSION = 26,
    MEA_DB_ENTRY_TYP_EHP_WBRG = 27,
    MEA_DB_ENTRY_TYP_LAST
}MEA_DB_ENTRY_Type_te;


/*---------------------------------------------------------------------------*/
/* Global Variables                                                          */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Forward declaration                                                       */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Local functions implementation                                            */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*          <MEA_CLI_DebugGetDb>                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_DebugGetDb(MEA_Uint32 db_id_i, MEA_db_dbt * db_o)
{

	MEA_db_dbt                  db;

	switch(db_id_i) {
	case MEA_DB_ENTRY_TYP_SERVICE: /* Service      */
        if (mea_drv_Get_Service_Internal_db(MEA_UNIT_0, &db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_Service_Internal_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_SERVICE_ENTRY: /* ServiceEntry */
		if (mea_drv_Get_ServiceEntry_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_ServiceEntry_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_SERVICE_RANGE: /* ServiceRange */
		if (mea_drv_Get_ServiceRange_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_ServiceEntry_db failed \n");
			return MEA_ERROR;
		}
		break;
     case MEA_DB_ENTRY_TYP_ACTION: /* Action */
		if (mea_drv_Get_Action_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_Action_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_PM_PKT_BYTE: /* PM packet and byte*/
		if (mea_drv_Get_pm_db_ByType(MEA_UNIT_0,&db,MEA_PM_TYPE_ALL) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_pm_db failed \n");
			return MEA_ERROR;
		}
		break;
#if 0
    case MEA_DB_ENTRY_TYP_PM_PKT: /* PM packet */
		if (mea_drv_Get_pm_db_ByType(MEA_UNIT_0,&db,MEA_PM_TYPE_PACKET_ONLY) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_pm_db failed \n");
			return MEA_ERROR;
		}
		break;
#endif
	case MEA_DB_ENTRY_TYP_POLICER_PROFILE: /* Policer Profile  */
        CLI_print("Error: Policer Profile not support currently by DB \n");
        return MEA_ERROR;
		break;
	case MEA_DB_ENTRY_TYP_POLICER_PTR: /* Policer Pointer  */
		if (mea_drv_Get_Policer_Pointer_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_Policer_pointer_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_POLICER: /* Policer */
		if (mea_drv_Get_Policer_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_Policer_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_EHP_ETH: /* EHP ETH */
		if (mea_drv_Get_EHP_ETH_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_ETH_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_EHP_ATM: /* EHP ATM */
		if (mea_drv_Get_EHP_ATM_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_ATM_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_EHP_DA: /* EHP DA LSW */
		if (mea_drv_Get_EHP_da_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_da_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_EHP_SA_LSS: /* EHP SA LSW */
		if (mea_drv_Get_EHP_sa_lss_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_sa_lss_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_EHP_PROFILE_ID: /* EHP profileId */
		if (mea_drv_Get_EHP_profileId_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_profileId_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_EHP_PROFILE_STAMPING: /* EHP profile_stamping */
		if (mea_drv_Get_EHP_profile_stamping_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_profile_stamping_db failed \n");
			return MEA_ERROR;
		}
		break;

	case MEA_DB_ENTRY_TYP_EHP_PROFILE_SA_MSS: /* EHP profile_sa_mss */
		if (mea_drv_Get_EHP_profile_sa_mss_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_profile_sa_mss_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_EHP_PROFILE_ETHER_TYPE: /* EHP profile_ether_type */
		if (mea_drv_Get_EHP_profile_ether_type_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_profile_ether_type_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_EHP_PROFILE2_ID: /* EHP profile2Id */
		if (mea_drv_Get_EHP_profile2Id_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_profile2Id_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_EHP_PROFILE2_LM: /* EHP profile2_lm */
		if (mea_drv_Get_EHP_profile2_lm_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_profile2_lm_db failed \n");
			return MEA_ERROR;
		}
		break;
#if 0
	case MEA_DB_ENTRY_TYP_FILTER: /* Filter  */
		if (mea_drv_Get_Filter_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_Filter_db failed \n");
			return MEA_ERROR;
		}
		break;
	case MEA_DB_ENTRY_TYP_FILTER_CTX: /* Filter_Ctx  */
		if (mea_drv_Get_Filter_Ctx_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_Filter_Ctx_db failed \n");
			return MEA_ERROR;
		}
		break;

	case MEA_DB_ENTRY_TYP_FILTER_MASK: /* Filter_Mask */
		if (mea_drv_Get_Filter_Mask_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_Filter_Mask_db failed \n");
			return MEA_ERROR;
		}
		break;
#endif
    case MEA_DB_ENTRY_TYP_PACKETGEN: /* PacketGen*/
        if (mea_drv_Get_PacketGen_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_PacketGen_db failed \n");
			return MEA_ERROR;
		}
        break; 


	
    case MEA_DB_ENTRY_TYP_EDIT_MAPPING_PROF: /* EditingMappingProfile */
        if (mea_drv_Get_EditingMappingProfile_db(MEA_UNIT_0,&db) != MEA_OK) {
            CLI_print("Error: mea_drv_Get_EditingMappingProfile_db failed \n");
            return MEA_ERROR;
        }
        break;
    case MEA_DB_ENTRY_TYP_EDIT_MAPPING: /* EditingMapping */
        if (mea_drv_Get_EditingMapping_db(MEA_UNIT_0,&db) != MEA_OK) {
            CLI_print("Error: mea_drv_Get_EditingMapping_db failed \n");
            return MEA_ERROR;
        }
        break;
	case MEA_DB_ENTRY_TYP_EDIT_FRAGMENT_SESSION: /* EHP Session */
		if (mea_drv_Get_EHP_fragmentSession_db(MEA_UNIT_0,&db) != MEA_OK) {
			CLI_print("Error: mea_drv_Get_EHP_fragmentSession_db failed \n");
			return MEA_ERROR;
		}
		break;
    case MEA_DB_ENTRY_TYP_EHP_WBRG: /* EHP WBRG */
        if (mea_drv_Get_EHP_WBRG_db(MEA_UNIT_0,&db) != MEA_OK) {
            CLI_print("Error: mea_drv_Get_EHP_ATM_db failed \n");
            return MEA_ERROR;
        }
        break;
	case MEA_DB_ENTRY_TYP_LAST: 
	default:
		CLI_print("Error: Unknown db id %d \n",db_id_i);
		return MEA_ERROR;
	}

	*db_o = db;

	return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*          <MEA_CLI_DebugDbShowSwHw>                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_DebugDbShowSwHw(int argc, char *argv[])
{

	MEA_db_dbt                  db;
	MEA_db_HwUnit_t                hwUnit;
	MEA_db_SwId_t               swId;
	MEA_db_SwHw_Entry_dbt       SwHw_entry;
	MEA_db_HwUnit_Entry_dbt     HwUnit_entry;
	MEA_db_Hw_Entry_dbt         Hw_entry;
	MEA_Bool                    found=MEA_FALSE;
	MEA_Uint32                  count;
	MEA_db_HwUnit_t                user_hwUnit          = MEA_DB_GENERATE_NEW_ID;
	MEA_Bool                    user_hwUnit_specific = MEA_FALSE;
	MEA_db_SwId_t               user_swId            = MEA_DB_GENERATE_NEW_ID;
	MEA_Bool                    user_swId_specific   = MEA_FALSE;
	MEA_Uint32                  i;

	if ((argc != 3) &&
	    (argc != 4)  ) {
		return MEA_ERROR;
	}

    if (MEA_CLI_DebugGetDb(MEA_OS_atoi(argv[1]),
		                               &db) != MEA_OK) {
		return MEA_ERROR;
	}

	if (MEA_OS_strcmp(argv[2],"all") != 0) {
		user_swId          = MEA_OS_atoi(argv[2]);
		user_swId_specific = MEA_TRUE;
	}

	if ((argc == 4) &&
		(MEA_OS_strcmp(argv[3],"all") != 0)) {
		user_hwUnit          = MEA_OS_atoi(argv[3]);
		user_hwUnit_specific = MEA_TRUE;
	}


	if (mea_db_GetFirst_SwHw(MEA_UNIT_0,
		                     db,
							 &swId,
							 &hwUnit,
							 &SwHw_entry,
							 &found) != MEA_OK) {
		CLI_print("Error: mea_db_GetFirst_SwHw failed\n");
		return MEA_OK; 
	}


	count=0;
	while (found) {

		if (((user_swId_specific   == MEA_FALSE) ||
			 (user_swId            == swId     )  ) &&
			((user_hwUnit_specific == MEA_FALSE) ||
			  (user_hwUnit         == hwUnit   )  )  ) {

			if (mea_db_Get_HwUnit(MEA_UNIT_0,
							      db,
							      hwUnit,
							      &HwUnit_entry) != MEA_OK) {
				CLI_print("Error: mea_db_Get_HwUnit failed\n");
				return MEA_OK; 
			}

			if (mea_db_Get_Hw(MEA_UNIT_0,
							  db,
							  hwUnit,
							  SwHw_entry.hwId,
							  &Hw_entry) != MEA_OK) {
				CLI_print("Error: mea_db_Get_Hw failed\n");
				return MEA_OK; 
			}

			if (count++ == 0) {
				CLI_print("Sw   Hw   SwHw  SwHw Hw    Hw   ");
				for (i=0;i<HwUnit_entry.num_of_regs;i++) {
					CLI_print("Hw       ");
				}
				CLI_print("\n");
				CLI_print("Id   Unit valid HwId valid SwId ");
				for (i=0;i<HwUnit_entry.num_of_regs;i++) {
					CLI_print("Reg%d     ",i);
				}
				CLI_print("\n");
				CLI_print("---- ---- ----- ---- ----- ---- ");
				for (i=0;i<HwUnit_entry.num_of_regs;i++) {
					CLI_print("-------- ");
				}
				CLI_print("\n");
			}

			CLI_print("%4d %4d %5d %4d %5d %4d ",
				      swId,
					  hwUnit,
					  SwHw_entry.valid,
					  SwHw_entry.hwId,
					  Hw_entry.valid,
				      Hw_entry.swId);
			for (i=0;i<HwUnit_entry.num_of_regs;i++) {
				CLI_print("%08x ",Hw_entry.regs[i]); 
			}
			CLI_print("\n");
		}

		if (mea_db_GetNext_SwHw(MEA_UNIT_0,
			                    db,
							    &swId,
								&hwUnit,
								&SwHw_entry,
								&found) != MEA_OK) {
			CLI_print("Error: mea_db_GetNext_SwHw failed\n");
			return MEA_OK; 
		}
	}

	if (count == 0) {
		CLI_print("Info: Table is empty\n");
        return MEA_OK;
    }

	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*          <MEA_CLI_DebugDbShowHw>                                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_DebugDbShowHw(int argc, char *argv[])
{

	MEA_db_dbt                  db;
	MEA_db_HwUnit_t                hwUnit;
	MEA_db_HwId_t               hwId;
	MEA_db_HwUnit_Entry_dbt     HwUnit_entry;
	MEA_db_Hw_Entry_dbt         Hw_entry;
	MEA_db_SwHw_Entry_dbt       SwHw_entry;
	MEA_Bool                    found_hwUnit;
	MEA_Bool                    found_hw;
	MEA_Uint32                  count;
	MEA_db_HwUnit_t                user_hwUnit          = MEA_DB_GENERATE_NEW_ID;
	MEA_Bool                    user_hwUnit_specific = MEA_FALSE;
	MEA_db_HwId_t               user_hwId            = MEA_DB_GENERATE_NEW_ID;
	MEA_Bool                    user_hwId_specific   = MEA_FALSE;
	MEA_Uint32                  i;

	if ((argc != 3) &&
	    (argc != 4)  ) {
		return MEA_ERROR;
	}

    if (MEA_CLI_DebugGetDb(MEA_OS_atoi(argv[1]),
		                               &db) != MEA_OK) {
		return MEA_ERROR;
	}

	if (MEA_OS_strcmp(argv[2],"all") != 0) {
		user_hwUnit          = MEA_OS_atoi(argv[2]);
		user_hwUnit_specific = MEA_TRUE;
	}

	if ((argc == 4) &&
		(MEA_OS_strcmp(argv[3],"all") != 0)) {
		user_hwId          = MEA_OS_atoi(argv[3]);
		user_hwId_specific = MEA_TRUE;
	}


	if (mea_db_GetFirst_HwUnit(MEA_UNIT_0,
		                       db,
							   &hwUnit,
							   &HwUnit_entry,
							   &found_hwUnit) != MEA_OK) {
		CLI_print("Error: mea_db_GetFirst_HwUnit failed\n");
		return MEA_OK; 
	}


	while (found_hwUnit) {

		CLI_print("\n");
		CLI_print("Hw Unit        : %d\n",hwUnit);
		CLI_print("num of entries : %d\n",HwUnit_entry.num_of_entries);
		CLI_print("used entries   : %d\n",HwUnit_entry.used_entries);
		CLI_print("Num of regs    : %d\n",HwUnit_entry.num_of_regs);
		CLI_print("------------------------------------------------------------------\n");

		if (mea_db_GetFirst_Hw(MEA_UNIT_0,
		                       db,
							   hwUnit,
							   &hwId,
							   &Hw_entry,
							   &found_hw) != MEA_OK) {
			CLI_print("Error: mea_db_GetFirst_Hw failed\n");
			return MEA_OK; 
		}

		count=0;
		while (found_hw) {
			
			if (((user_hwUnit_specific == MEA_FALSE) ||
				 (user_hwUnit          == hwUnit     )  ) &&
				((user_hwId_specific   == MEA_FALSE) ||
				 (user_hwId            == hwId   )  )  ) {

				if (mea_db_Get_SwHw(MEA_UNIT_0,
					                db,
								    Hw_entry.swId,
									hwUnit,
									&SwHw_entry) != MEA_OK) {
					CLI_print("Error: mea_db_Get_SwHw failed\n");
					return MEA_OK; 
				}

				if (count++ == 0) {
					CLI_print("Hw   Hw   Hw    Hw   ");
				    for (i=0;i<HwUnit_entry.num_of_regs;i++) {
					    CLI_print("Hw       ");
				    }
					CLI_print("SwHw  SwHw \n");

					CLI_print("Unit Id   Valid SwId ");
				    for (i=0;i<HwUnit_entry.num_of_regs;i++) {
					    CLI_print("Reg%d     ",i);
				    }
					CLI_print("valid HwId\n");

					CLI_print("---- ---- ----- ---- ");
				    for (i=0;i<HwUnit_entry.num_of_regs;i++) {
					    CLI_print("-------- ");
				    }
					CLI_print("----- ----\n");
				}

				CLI_print("%4d %4d %5d %4d ",
					      hwUnit,
						  hwId,
						  Hw_entry.valid,
						  Hw_entry.swId);

			    for (i=0;i<HwUnit_entry.num_of_regs;i++) {
					CLI_print("%08x ",Hw_entry.regs[i]);
			    }
				CLI_print("%5d %4d\n",
						  SwHw_entry.valid,
						  SwHw_entry.hwId);

			}

			if (mea_db_GetNext_Hw(MEA_UNIT_0,
				                  db,
								  hwUnit,
								  &hwId,
								  &Hw_entry,
								  &found_hw) != MEA_OK) {
				CLI_print("Error: mea_db_GetNext_Hw failed\n");
				return MEA_OK; 
			}
		}
		CLI_print("==================================================================\n");
		
		if (mea_db_GetNext_HwUnit(MEA_UNIT_0,
								  db,
							      &hwUnit,
							      &HwUnit_entry,
							      &found_hwUnit) != MEA_OK) {
			CLI_print("Error: mea_db_GetNext_HwUnit failed\n");
			return MEA_OK; 
		}
	}


	return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/* External functions implementation                                         */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                     <MEA_CLI_AddDebugDbCmds>                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_CLI_AddDebugDbCmds (void)
{

#define MEA_CLI_DEBUG_DB_ID_HELP_STR \
	              "       <type> : 0  (Service)               \n" \
				  "                1  (ServiceEntry)          \n" \
                  "                2  (ServiceRange)          \n" \
				  "                3  (Action)                \n" \
				  "                4  (PM packets and bytes)  \n" \
                  "                5  (PM packets only)       \n" \
				  "                6  (Policer Profile)       \n" \
				  "                7  (Policer Pointer)       \n" \
				  "                8  (Policer)               \n" \
				  "                9  (EHP ETH)               \n" \
				  "                10 (EHP ATM)               \n" \
				  "                11 (EHP DA)                \n" \
				  "                12 (EHP SA LSS)            \n" \
				  "                13 (EHP Profile Id)        \n" \
				  "                14 (EHP profile stamping)  \n" \
				  "                15 (EHP profile SA DSS)    \n" \
				  "                16 (EHP profile SA MSS)    \n" \
				  "                17 (EHP profile ether type)\n" \
				  "                18 (EHP Profile2 Id)       \n" \
				  "                19 (EHP profile2 lm)       \n" \
	              "                20 (Filter)                \n" \
	              "                21 (Filter Ctx)            \n" \
	              "                22 (Filter Mask)           \n" \
                  "                23 (PacketGen)             \n" \
                  "                24 (Editing Mapping Profile - SW Only)\n" \
                  "                25 (Editing Mapping)       \n"\
                  "                26 (EHP session)           \n" \
				  "                27 (EHP wbrg)               \n" 
    CLI_defineCmd("MEA debug db show swHw",
		          (CmdFunc)MEA_CLI_DebugDbShowSwHw,
				  "Show Software Hardware information\n",
				  "Usage: swHw <type> <swId>|all [<hwUnit>|all] \n"
				  "--------------------------------------\n"
                  MEA_CLI_DEBUG_DB_ID_HELP_STR 
				  "       <swId>   : swId   or all       \n"
				  "       <hwUnit> : hwUnit or all       \n"
				  "--------------------------------------\n"
				  "Examples: - swHw 0 all\n"
				  "          - swHw 0 1  \n"
				  "          - swHw 0 all all\n"
				  "          - swHw 0 all 0\n"
				  "          - swHw 0 1   all\n"
				  "          - swHw 0 1   0\n");

    CLI_defineCmd("MEA debug db show hw",
		          (CmdFunc)MEA_CLI_DebugDbShowHw,
				  "Show Hardware Software information\n",
				  "Usage: hw <type> <hwUnit>|all [<hwId>|all] \n"
				  "-------------------------------------------\n"
                  MEA_CLI_DEBUG_DB_ID_HELP_STR 
				  "       <hwUnit> : hwUnit or all            \n"
				  "       <hwId>   : hwId   or all            \n"
				  "-------------------------------------------\n"
				  "Examples: - hw 0 all    \n"
				  "          - hw 0 0      \n"
				  "          - hw 0 all all\n"
				  "          - hw 0 all 1  \n"
				  "          - hw 0 0   all\n"
				  "          - hw 0 0   1  \n");

	return MEA_OK;
}
