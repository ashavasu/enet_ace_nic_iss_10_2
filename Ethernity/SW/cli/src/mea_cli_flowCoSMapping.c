/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "mea_cli_flowCoSMapping.h"
#include "cli_eng.h"
#include "mea_cli.h"

MEA_Status MEA_CLI_FlowCoSMappingProfile_Create(int argc, char *argv[])
{

    MEA_FlowCoSMappingProfile_Entry_dbt entry;
    MEA_FlowCoSMappingProfile_Id_t      id;
    int i;
    int num_of_params;
    MEA_Uint32 item;
    MEA_Uint32 valid;
    MEA_Uint32 cos;
    MEA_Uint32 color;

    /* Check minimum number of parameters */
    if (argc < 3) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"auto") == 0) {
        id = MEA_PLAT_GENERATE_NEW_ID;
    } else {
        id = MEA_OS_atoi(argv[1]);
    }

    MEA_OS_memset(&entry,0,sizeof(entry));
    entry.type = MEA_OS_atoi(argv[2]);
    i=3;
    while(i<argc) {
        if (MEA_OS_strcmp(argv[i],"-i") == 0) {
            num_of_params=5;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            item  = MEA_OS_atoi(argv[i+1]);
            valid = MEA_OS_atoi(argv[i+2]);
            cos   = MEA_OS_atoi(argv[i+3]);
            color = MEA_OS_atoi(argv[i+4]);
            if (item>=MEA_NUM_OF_ELEMENTS(entry.item_table)) {
                CLI_print("Error: <itemX> %d >= max value (%d) \n",
                          item,
                          MEA_NUM_OF_ELEMENTS(entry.item_table));
                return MEA_ERROR;
            }
            if ((valid != 0) && (valid != 1)) {
                CLI_print("Error: Invalid value for valid %d \n",valid);
                return MEA_ERROR;
            }
            if (cos > 7) {
                CLI_print("Error: Invalid value for CoS %d  \n",cos);
                return MEA_ERROR;
            }
            if ((color != 0) && (color != 1)) {
                CLI_print("Error: Invalid value for color %d \n",color);
                return MEA_ERROR;
            }
            MEA_OS_memset(&(entry.item_table[item]),
                          0,
                          sizeof(entry.item_table[0]));
            entry.item_table[item].valid = (MEA_Uint8)valid;
            entry.item_table[item].COS   = (MEA_Uint8)cos;
            entry.item_table[item].COLOR = (MEA_Uint8)color;
            i+=num_of_params;
        } else {
            CLI_print("Error: Invalid option\n");
            return MEA_ERROR;
        }

    }

    if (MEA_API_Create_FlowCoSMappingProfile_Entry(MEA_UNIT_0,&entry,&id) != MEA_OK) {
        CLI_print("Error: MEA_API_Create_FlowCoSMappingProfile_Entry failed \n");
        return MEA_OK;
    }

    CLI_print("Done. (id=%d)\n",id);

    /* Return to Caller */
    return MEA_OK;
}

MEA_Status MEA_CLI_FlowCoSMappingProfile_Modify(int argc, char *argv[])
{

    MEA_FlowCoSMappingProfile_Entry_dbt entry;
    MEA_FlowCoSMappingProfile_Id_t      id;
    int i;
    int num_of_params;
    MEA_Uint32 item;
    MEA_Uint32 valid;
    MEA_Uint32 cos;
    MEA_Uint32 color;

    /* Check minimum number of prarmaters */
    if (argc < 2) {
        return MEA_ERROR;
    }



    id = MEA_OS_atoi(argv[1]);

    if (MEA_API_Get_FlowCoSMappingProfile_Entry(MEA_UNIT_0,id,&entry) != MEA_OK) {
        CLI_print("Error: MEA_API_Get_FlowCoSMappingProfile_Entry failed \n");
        return MEA_OK;
    }

    i=2;
    while(i<argc) {
        if (MEA_OS_strcmp(argv[i],"-i") == 0) {
            num_of_params=5;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough paramter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            item  = MEA_OS_atoi(argv[i+1]);
            valid = MEA_OS_atoi(argv[i+2]);
            cos   = MEA_OS_atoi(argv[i+3]);
            color = MEA_OS_atoi(argv[i+4]);
            if (item>=MEA_NUM_OF_ELEMENTS(entry.item_table)) {
                CLI_print("Error: <itemX> %d >= max value (%d) \n",
                          item,
                          MEA_NUM_OF_ELEMENTS(entry.item_table));
                return MEA_ERROR;
            }
            if ((valid != 0) && (valid != 1)) {
                CLI_print("Error: Invalid value for valid %d \n",valid);
                return MEA_ERROR;
            }
            if (cos > 7) {
                CLI_print("Error: Invalid value for CoS %d  \n",cos);
                return MEA_ERROR;
            }
            if ((color != 0) && (color != 1)) {
                CLI_print("Error: Invalid value for color %d \n",color);
                return MEA_ERROR;
            }
            MEA_OS_memset(&(entry.item_table[item]),
                          0,
                          sizeof(entry.item_table[0]));
            entry.item_table[item].valid = (MEA_Uint8)valid;
            entry.item_table[item].COS   = (MEA_Uint8)cos;
            entry.item_table[item].COLOR = (MEA_Uint8)color;
            i+=num_of_params;
        } else {
            if (MEA_OS_strcmp(argv[i],"-t") == 0) {
                num_of_params=2;
                if ((i+num_of_params)>argc) {
                    CLI_print("Error: Not enough paramter for option %s \n",argv[i]);
                    return MEA_ERROR;
                }
                entry.type  = MEA_OS_atoi(argv[i+1]);
                i+=num_of_params;
            } else {
                CLI_print("Error: Invalid option\n");
                return MEA_ERROR;
            } 
        }

    }

    if (MEA_API_Set_FlowCoSMappingProfile_Entry(MEA_UNIT_0,id,&entry) != MEA_OK) {
        CLI_print("Error: MEA_API_Set_FlowCoSMappingProfile_Entry failed \n");
        return MEA_OK;
    }


    /* Return to Caller */
    return MEA_OK;
}

MEA_Status MEA_CLI_FlowCoSMappingProfile_Delete(int argc, char *argv[])
{

    MEA_FlowCoSMappingProfile_Id_t      id;
    MEA_Bool                            found;

    /* Check minimum number of prarmaters */
    if (argc != 2) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        id = MEA_OS_atoi(argv[1]);
        if (MEA_API_Delete_FlowCoSMappingProfile_Entry(MEA_UNIT_0,id) != MEA_OK) {
            CLI_print("Error: MEA_API_Delete_FlowCoSMappingProfile_Entry failed \n");
            return MEA_ERROR;
        }
    } else {

        if (MEA_API_GetFirst_FlowCoSMappingProfile_Entry(MEA_UNIT_0,
                                                         &id,
                                                         NULL,
                                                         &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetFirst_FlowCoSMappingProfile_Entry failed \n");
            return MEA_OK;
        }
        while (found) {
            if (MEA_API_Delete_FlowCoSMappingProfile_Entry(MEA_UNIT_0,id) != MEA_OK) {
                CLI_print("Error: MEA_API_Delete_FlowCoSMappingProfile_Entry failed \n");
                return MEA_OK;
            }
            if (MEA_API_GetNext_FlowCoSMappingProfile_Entry(MEA_UNIT_0,
                                                            &id,
                                                            NULL,
                                                            &found) != MEA_OK) {
                CLI_print("Error: MEA_API_GetNext_FlowCoSMappingProfile_Entry failed \n");
                return MEA_OK;
            }
        }
    }


    /* Return to Caller */
    return MEA_OK;
}

MEA_Status MEA_CLI_FlowCoSMappingProfile_Show(int argc, char *argv[])
{
    MEA_FlowCoSMappingProfile_Entry_dbt entry;
    MEA_FlowCoSMappingProfile_Id_t      id;
    MEA_Bool                            found;
    MEA_Uint32                          count;
    MEA_Uint32                          item;
    MEA_Bool                            all_flag=MEA_TRUE;
    static char                         type_str[MEA_FLOW_COS_MAPPING_PROFILE_TYPE_LAST][20];

 
                                                                                        //01234567890123456789
        MEA_OS_strcpy(&(type_str[MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_ETHERTYPE][0]),"L2 ETH");
        MEA_OS_strcpy(&(type_str[MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_INNER_VLAN][0]),"L2 INNER VLAN");
        MEA_OS_strcpy(&(type_str[MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN][0]),"L2 OUTER VLAN");
        MEA_OS_strcpy(&(type_str[MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_PRECEDENCE][0])  ,"IPv4_PRECEDENCE");
        MEA_OS_strcpy(&(type_str[MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_DSCP][0])        ,"IPv4_DSCP");
        MEA_OS_strcpy(&(type_str[MEA_FLOW_COS_MAPPING_PROFILE_TYPE_IPv4_TOS][0])         ,"IPv4_TOS");


    /* Check minimum number of parameters */
    if (argc != 2) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        id = MEA_OS_atoi(argv[1]);
        if (MEA_API_Get_FlowCoSMappingProfile_Entry(MEA_UNIT_0,id,&entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_FlowCoSMappingProfile_Entry failed \n");
            return MEA_ERROR;
        }
        found = MEA_TRUE;
        all_flag = MEA_FALSE;
    } else {
        if (MEA_API_GetFirst_FlowCoSMappingProfile_Entry(MEA_UNIT_0,
                                                         &id,
                                                         &entry,
                                                         &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetFirst_FlowCoSMappingProfile_Entry failed \n");
            return MEA_OK;
        }
    }

    count=0;
    while (found) {
        if (count++ == 0) {
            CLI_print("ProfileId Type                 Item# Valid CoS Color  \n"
                      "--------- -------------------- ----- ----- --- ------ \n");
        }
        
        CLI_print("%9d %20s ",id,type_str[entry.type]);

        for (item=0;item<(MEA_Uint32)MEA_FLOW_COS_MAPPING_NUM_OF_ITEMS(entry.type);item++) {
            if(item != 0){
                CLI_print("                               ");
            }

            CLI_print("%5d %-5s %3d %6s \n",
                      item,
                      (entry.item_table[item].valid) ? "Yes" : "No",
                      entry.item_table[item].COS,
                      (entry.item_table[item].COLOR) ?  "Yellow" : "Green");
        }

        if (!all_flag) {
            break;
        }

        if (MEA_API_GetNext_FlowCoSMappingProfile_Entry(MEA_UNIT_0,
                                                        &id,
                                                        &entry,
                                                        &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetNext_FlowCoSMappingProfile_Entry failed \n");
            return MEA_OK;
        }
        if (count!=0)
            CLI_print("--------- -------------------- ----- ----- --- ------ \n");
    }

    if (count==0) {
        CLI_print("No Flow CoS Mapping Profiles\n");
        return MEA_OK;
    } 
    
    if (count!=0)
    CLI_print("Display %d Profiles \n",count);

    /* Return to Caller */
    return MEA_OK;
}


MEA_Status MEA_CLI_FlowCoSMappingProfile_AddDebugCmds(void)
{


           CLI_defineCmd("MEA flow mapping CoS show",
                  (CmdFunc)MEA_CLI_FlowCoSMappingProfile_Show, 
                  "Show   Flow CoS Mapping Profile(s) \n",
                  "show   <id>|all\n"
                  "--------------------------------------------------------------------------------\n"
                  "       <id>}all - Profile id  or all\n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - show all \n"
                  "          - show 2\n");

           CLI_defineCmd("MEA flow mapping CoS delete",
                  (CmdFunc)MEA_CLI_FlowCoSMappingProfile_Delete, 
                  "Delete Flow CoS Mapping Profile(s) \n",
                  "delete <id>|all\n"
                  "--------------------------------------------------------------------------------\n"
                  "       <id>}all - Profile id  or all\n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - delete all \n"
                  "          - delete 2\n");

           CLI_defineCmd("MEA flow mapping CoS modify",
                  (CmdFunc)MEA_CLI_FlowCoSMappingProfile_Modify, 
                  "Update Flow CoS Mapping Profile \n",
                  "modify <id> \n"
                  "       [-t <type> ]\n"
                  "       [-i <item<x>> <valid> <CoS> <Color> ]\n"
                  "--------------------------------------------------------------------------------\n"
                  "       <id> - Profile id \n"
                  "\n"
                  "       (-t option) \n"
                  "            <type>    - Profile type (Index to mapping table)\n"
                  "                        0 - Layer2 priority EtherType\n"
                  "                        1 - Layer2 priority inner vlan\n"
                  "                        2 - Layer2 priority outer vlan\n"
                  "                        3 - IPv4 Precedence \n"
                  "                        4 - IPv4 DSCP \n"
                  "                        5 - IPv4 TOS \n"
                  
                  "\n"
                  "       (-i option) \n"
                  "           <item<X> - item number (0..7/63) \n"
                  "                      Note: 0..7 for TOS/Precedence and 63 for DSCP \n"
                  "           <valid>  - 0 or 1 \n"
                  "           <CoS>    - Priority Queue (0-7) \n"
                  "           <Color>  - Color input to police (0-green/1-yellow)\n"
                  "       Note: The -i option can repeat many time for any item we \n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - modify    0 \n"
                  "          - modify    0 -i  2 1  2 0 -i  3 1 7 0 \n"
                  "          - modify    2 -i 44 1  2 0 -i 45 1 7 0 \n");

           CLI_defineCmd("MEA flow mapping CoS create",
                  (CmdFunc)MEA_CLI_FlowCoSMappingProfile_Create, 
                  "Create Flow CoS Mapping Profile \n",
                  "create auto|<id>  <type> \n"
                  "       [-i <item<x>> <valid> <CoS> <Color> ]\n"
                  "--------------------------------------------------------------------------------\n"
                  "       auto|<id> - Profile id or auto to generate new id \n"
                  "       <type>    - Profile type (Index to mapping table)\n"
                  "                        0 - Layer2 priority EtherType\n"
                  "                        1 - Layer2 priority inner vlan\n"
                  "                        2 - Layer2 priority outer vlan\n"
                  "                        3 - IPv4 Precedence \n"
                  "                        4 - IPv4 DSCP \n"
                  "                        5 - IPv4 TOS \n"
                  "\n"
                  "       (-i option) \n"
                  "           <item<X> - item number (0..7/63) \n"
                  "                      Note: 0..7 for TOS/Precedence and 63 for DSCP \n"
                  "           <valid>  - 0 or 1 \n"
                  "           <CoS>    - Priority Queue (0-7) \n"
                  "           <Color>  - Color input to police (0-green/1-yellow)\n"
                  "       Note: The -i option can repeat many time for any item we \n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - create auto 0 \n"
                  "          - create auto 0 -i  2 1  2 0 -i  3 1 7 0 \n"
                  "          - create    2 2 -i 44 1  2 0 -i 45 1 7 0 \n");


    return MEA_OK;
}

