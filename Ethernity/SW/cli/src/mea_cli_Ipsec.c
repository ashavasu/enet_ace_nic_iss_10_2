/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"

#include "mea_IPSec_drv.h"

#if IPSEC_ADAPTOR_WANTED
#include "../../../../enet_adaptor/adap_ipsec.h"
#endif
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_Ipsec.h"

#if 1 //MEA_IPSec_ENABLE





/************************************************************************/
/*   IPSec                                                              */
/************************************************************************/
static MEA_Status  MEA_CLI_Set_Globals_Local_IPSec_src_Ipv4(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Local_IPSec_ttl(int argc, char* argv[]);

static MEA_Status MEA_CLI_Set_Globals_Localmy_Ipsec(int argc, char* argv[]);




/************************************************************************/
/* IPSec                                                               */
/************************************************************************/
static MEA_Status  MEA_CLI_Set_Globals_Local_IPSec_src_Ipv4(int argc, char* argv[])
{
    MEA_Globals_Entry_dbt entry;

    if (argc < 2) {
        return MEA_ERROR;
    }
    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("MEA_API_Get_Globals_Entry failed\n");
        return MEA_OK;
    }

    entry.Ipsec_info.ipsec_src_Ipv4 = MEA_OS_inet_addr(argv[1]);


    if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("MEA_API_Set_Globals_Entry failed\n");
        return MEA_OK;
    }
    CLI_print("Done.\n");
    return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_Globals_Local_IPSec_ttl(int argc, char* argv[])
{
    MEA_Globals_Entry_dbt entry;

    if (argc < 2) {
        return MEA_ERROR;
    }
    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("MEA_API_Get_Globals_Entry failed\n");
        return MEA_OK;
    }


    entry.Ipsec_info.ipsec_ttl = (MEA_Uint32)MEA_OS_atoiNum(argv[1]);



    if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("MEA_API_Set_Globals_Entry failed\n");
        return MEA_OK;
    }
    CLI_print("Done.\n");
    return MEA_OK;
}


static MEA_Status mea_Cli_IPSecESP_CommandResult(int              argc,
    char                   *argv[],
    MEA_Bool               flagType,
    MEA_Uint16             startIndex,
    MEA_IPSec_dbt  *entry)
{
    int i;
    int num_of_params;
    int index;


   
   


    for (i = (startIndex); i < argc; i++) {

        if (!MEA_OS_strcmp(argv[i], "-security_type")) {

            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry->security_type = MEA_OS_atoiNum(argv[i + 1]);

            i += num_of_params;
            continue;

        }

        if (!MEA_OS_strcmp(argv[i], "-TFC_en")) {

            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry->TFC_padding_en = MEA_OS_atoiNum(argv[i + 1]);

            i += num_of_params;
            continue;

        }

        if (!MEA_OS_strcmp(argv[i], "-ESN_en")) {

            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry->ESN_en = MEA_OS_atoiNum(argv[i + 1]);

            i += num_of_params;
            continue;

        }

        if (!MEA_OS_strcmp(argv[i], "-SPI")) {

            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry->SPI = MEA_OS_atoiNum(argv[i + 1]);

            i += num_of_params;
            continue;

        }

        if (!MEA_OS_strcmp(argv[i], "-Integrity_key")) 
        {

            num_of_params = MEA_MAX_IPSECE_INTEG_KEY_WORD;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            for (index = 0; index < MEA_MAX_IPSECE_INTEG_KEY_WORD; index++) {
            entry->Integrity_key[index] = MEA_OS_atoiNum(argv[i + 1 + index]);
            }
 
            i += num_of_params;
            continue;

        }

        if (!MEA_OS_strcmp(argv[i], "-Integrity_IV")) {

            num_of_params = MEA_MAX_IPSECE_INTEG_IV_WORD;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            for (index = 0; index < MEA_MAX_IPSECE_INTEG_IV_WORD; index++) {
                entry->Integrity_IV[index] = MEA_OS_atoiNum(argv[i + 1 + index]);
            }


            i += num_of_params;
            continue;

        }

        if (!MEA_OS_strcmp(argv[i], "-Confident_key")) {

            num_of_params = MEA_MAX_IPSECE_CONFID_KEY_WORD;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            for (index = 0; index < MEA_MAX_IPSECE_CONFID_KEY_WORD; index++) {
                entry->Confident_key[index] = MEA_OS_atoiNum(argv[i + 1 + index]);
            }


            i += num_of_params;
            continue;

        }
 
        if (!MEA_OS_strcmp(argv[i], "-Confident_IV")) {

            num_of_params = MEA_MAX_IPSECE_CONFID_IV_WORD;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            for (index = 0; index < MEA_MAX_IPSECE_CONFID_IV_WORD; index++) {
                entry->Confident_IV[index] = MEA_OS_atoiNum(argv[i + 1 + index]);
            }


            i += num_of_params;
            continue;

        }




    }//end for


    return MEA_OK;
}






static MEA_Status MEA_CLI_IPSecESP_Show_entry(int argc, char *argv[])
{

    MEA_Uint32        Id_i, start, end;
    MEA_IPSec_dbt     entry;
    MEA_Bool          exist;
    MEA_Uint32        count;
    MEA_Uint32        index;
    static char                         protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_LAST][65];

  


        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_HMC_MD5_96][0]), "Security_INTEG_HMC_MD5_96                     ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_HMC_SHA1_96][0]), "Security_INTEG_HMC_SHA1_96                    ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_HMC_SHA256_128][0]), "Security_INTEG_HMC_SHA256_128                 ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_HMC_SHA384_192][0]), "Security_INTEG_HMC_SHA384_192                 ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_HMC_SHA512_256][0]), "Security_INTEG_HMC_SHA512_256                 ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_AES_XCBC_MAC_96][0]), "Security_INTEG_AES_XCBC_MAC_96                ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_AES_CMAC_96][0]), "Security_INTEG_AES_CMAC_96                    ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_AES_128_GMAC_128][0]), "Security_INTEG_AES_128_GMAC_128               ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_AES_192_GMAC_128][0]), "Security_INTEG_AES_192_GMAC_128               ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_INTEG_AES_256_GMAC_128][0]), "Security_INTEG_AES_256_GMAC_128               ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_3DES_CBC][0]), "Security_CONFIDENT_3DES_CBC                   ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CBC][0]), "Security_CONFIDENT_AES_128_CBC                ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CBC][0]), "Security_CONFIDENT_AES_192_CBC                ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CBC][0]), "Security_CONFIDENT_AES_256_CBC                ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR][0]), "Security_CONFIDENT_AES_128_CTR                ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR][0]), "Security_CONFIDENT_AES_192_CTR                ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR][0]), "Security_CONFIDENT_AES_256_CTR                ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_3DES_CBC_HMAC_MD5_96][0]), "Security_3DES_CBC_HMAC_MD5_96                 ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_3DES_CBC_HMAC_SHA1_96][0]), "Security_3DES_CBC_HMAC_SHA1_96                ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_3DES_CBC_HMAC_SHA256_128][0]), "Security_3DES_CBC_HMAC_SHA256_128             ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_MD5_96][0]), "Security_AES_128_CBC_HMAC_MD5_96              ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_192_CBC_HMAC_MD5_96][0]), "Security_AES_192_CBC_HMAC_MD5_96              ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_MD5_96][0]), "Security_AES_256_CBC_HMAC_MD5_96              ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_SHA1_96][0]), "Security_AES_128_CBC_HMAC_SHA1_96             ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_192_CBC_HMAC_SHA1_96][0]), "Security_AES_192_CBC_HMAC_SHA1_96             ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_SHA1_96][0]), "Security_AES_256_CBC_HMAC_SHA1_96             ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_128_CBC_HMAC_SHA256_128][0]), "Security_AES_128_CBC_HMAC_SHA256_128          ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_192_CBC_HMAC_SHA256_128][0]), "Security_AES_192_CBC_HMAC_SHA256_128          ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_256_CBC_HMAC_SHA256_128][0]), "Security_AES_256_CBC_HMAC_SHA256_128          ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_128_CBC_AES_XCBC_MAC_96][0]), "Security_AES_128_CBC_AES_XCBC_MAC_96          ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_192_CBC_AES_XCBC_MAC_96][0]), "Security_AES_192_CBC_AES_XCBC_MAC_96          ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_AES_256_CBC_AES_XCBC_MAC_96][0]), "Security_AES_256_CBC_AES_XCBC_MAC_96                ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_MD5_96][0]), "Security_CONFIDENT_AES_128_CTR_HMAC_MD5_96    ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_HMAC_MD5_96][0]), "Security_CONFIDENT_AES_192_CTR_HMAC_MD5_96    ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_MD5_96][0]), "Security_CONFIDENT_AES_256_CTR_HMAC_MD5_96    ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_SHA1_96][0]), "Security_CONFIDENT_AES_128_CTR_HMAC_SHA1_96   ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_HMAC_SHA1_96][0]), "Security_CONFIDENT_AES_192_CTR_HMAC_SHA1_96      ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_SHA1_96][0]), "Security_CONFIDENT_AES_256_CTR_HMAC_SHA1_96      ");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_HMAC_SHA256_128][0]), "Security_CONFIDENT_AES_128_CTR_HMAC_SHA256_128");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_HMAC_SHA256_128][0]), "Security_CONFIDENT_AES_192_CTR_HMAC_SHA256_128");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_HMAC_SHA256_128][0]), "Security_CONFIDENT_AES_256_CTR_HMAC_SHA256_128");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_128_CTR_AES_XCBC_MAC_96][0]), "Security_CONFIDENT_AES_128_CTR_AES_XCBC_MAC_96");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_192_CTR_AES_XCBC_MAC_96][0]), "Security_CONFIDENT_AES_192_CTR_AES_XCBC_MAC_96");
        MEA_OS_strcpy(&(protocol_str[MEA_EDITING_IPSec_Security_CONFIDENT_AES_256_CTR_AES_XCBC_MAC_96][0]), "Security_CONFIDENT_AES_256_CTR_AES_XCBC_MAC_96");






   




    if (argc < 2) {
        return MEA_ERROR;
    }
    if (!MEA_IPSec_SUPPORT) {
        CLI_print("%s - MEA_IPSec_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_OK;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        start = 0;
        end = (MEA_PacketGen_t)(MEA_MAX_NUM_OF_IPSecESP_ID - 1);
      
    }
    else {
        Id_i = MEA_OS_atoi(argv[1]);
        if (Id_i >= MEA_MAX_NUM_OF_IPSecESP_ID) {
            CLI_print("ERROR Id % is out of range\n", Id_i);
            return MEA_OK;
        }
        start = end = Id_i;
    }
    count = 0;
    MEA_OS_memset(&entry, 0, sizeof(entry));

    for (Id_i = start; Id_i <= end; Id_i++) {
        if (MEA_API_IPSecESP_IsExist(MEA_UNIT_0, Id_i, &exist) != MEA_OK) {
            CLI_print("ERROR MEA_API_IPSecESP_IsExist failed (id=%d)\n", Id_i);
            return MEA_OK;
        }
        if (!exist) {
            continue;
        }

        if (MEA_API_IPSecESP_Get_Entry(MEA_UNIT_0,
            Id_i,
            &entry) != MEA_OK) {

        }

        if (count++ == 0) {
            CLI_print("\n");
            CLI_print("           IPSecESP      \n"
                " ----------------------------\n"
                "      sec \n"
                "  ID  type  SPI         ESN  TFC \n"
                " ---- ---- ----------- ---- ------------------------------------------------------------------------ \n");
               // 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789
        }

        CLI_print(" %4d %4d 0x%08x %4s %4s %s\n", Id_i,
                               entry.security_type,
                               entry.SPI,
            MEA_STATUS_STR(entry.ESN_en),
            MEA_STATUS_STR(entry.TFC_padding_en),
            protocol_str[entry.security_type]);

        
       
        
      

        CLI_print("%s %13s ","     " ,"Integrity_key");
        for (index=0;index<MEA_MAX_IPSECE_INTEG_KEY_WORD;index++)
        {
           
                CLI_print("0x%08x ", entry.Integrity_key[index]);
        }
        CLI_print("\r\n");
        CLI_print("%s %13s ", "     ", "Integrity_IV");
        for (index = 0; index < MEA_MAX_IPSECE_INTEG_IV_WORD; index++)
        {
            CLI_print("0x%08x ", entry.Integrity_IV[index]);
        }

        CLI_print("\r\n");
        CLI_print("%s %13s ", "     ", "Confident_key");
        for (index = 0; index < MEA_MAX_IPSECE_CONFID_KEY_WORD; index++)
        {
             CLI_print(" 0x%08x ", entry.Confident_key[index]);
        }
        CLI_print("\r\n");
        CLI_print("%s %13s ", "     ", "Confident_IV");
        for (index = 0; index < MEA_MAX_IPSECE_CONFID_IV_WORD; index++)
        {
          CLI_print(" 0x%08x ", entry.Confident_IV[index]);
        }
        CLI_print("\r\n");

        

    }

    if (count == 0) {
        CLI_print("IPSecESP Table is empty\n");
    }






    return MEA_OK;
}

static MEA_Status MEA_CLI_IPSecESP_Create(int argc, char *argv[])
{

    MEA_IPSec_dbt      entry;
    MEA_Uint32                id_io;

    int i;

    if (argc < 2) {
        return MEA_ERROR;
    }

    for (i = 1; i < argc; i++) {

        if ((argv[i][0] == '-') && (

            (MEA_OS_strcmp(argv[i], "-security_type") != 0) &&
            (MEA_OS_strcmp(argv[i], "-TFC_en")        != 0) &&
            (MEA_OS_strcmp(argv[i], "-ESN_en")        != 0) &&
            (MEA_OS_strcmp(argv[i], "-SPI")           != 0) &&
            (MEA_OS_strcmp(argv[i], "-Integrity_key") != 0) &&
            (MEA_OS_strcmp(argv[i], "-Integrity_IV")  != 0) &&
            (MEA_OS_strcmp(argv[i], "-Confident_key") != 0) &&
            (MEA_OS_strcmp(argv[i], "-Confident_IV")  != 0)) ) {
            CLI_print("ERROR: Not support option %s \n", argv[i]);
            return MEA_OK;
        }
    }
    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (!MEA_OS_strcmp(argv[1], "auto")) { /* only for create*/
        id_io = MEA_PLAT_GENERATE_NEW_ID;
    }
    else {
        id_io = MEA_OS_atoi(argv[1]);
    }

    if (mea_Cli_IPSecESP_CommandResult(argc, argv, MEA_FALSE,
        2, &entry) != MEA_OK) {
        CLI_print("Error: mea_Cli_IPSecESP_CommandResult \n");
        return MEA_OK;
    }

  

    if (MEA_API_IPSecESP_Create_Entry(MEA_UNIT_0, &entry, &id_io) != MEA_OK) {
        CLI_print("Error: can't create IPSecESP  \n");
        return MEA_ERROR;
    }



    CLI_print("Done create IPSecESP with Id = %d  \n", id_io);
    return MEA_OK;
}



static MEA_Status MEA_CLI_IPSecESP_Delete(int argc, char *argv[])
{
    MEA_Uint32  BFDId, start_bfd, end_bfd;
    MEA_Bool      exist;

    if (argc < 2) {
        return MEA_ERROR;
    }

    if (!MEA_OS_strcmp(argv[1], "all")) {
        start_bfd = 0;
        
            end_bfd = MEA_MAX_NUM_OF_IPSecESP_ID - 1;
    }
    else {
        start_bfd = (MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_bfd = start_bfd;
        if ((MEA_API_IPSecESP_IsExist(MEA_UNIT_0, start_bfd, &exist) != MEA_OK) ||
            (!exist)) {
            CLI_print(" ERROR: id %d is not exist \n", start_bfd);
            return MEA_OK;
        }
    }

    for (BFDId = start_bfd; BFDId <= end_bfd; BFDId++) {
        if (MEA_API_IPSecESP_IsExist(MEA_UNIT_0, BFDId, &exist) == MEA_OK) {
            if (exist) {

                if (MEA_API_IPSecESP_Delete_Entry(MEA_UNIT_0, BFDId) != MEA_OK) {
                    CLI_print(" ERROR: can't Delete IPSecESP id %d \n", BFDId);
                }
            }
        }
    }


    return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_Localmy_Ipsec(int argc, char* argv[])
{
    MEA_Globals_Entry_dbt entry;
   

    if (argc < 2) {
        return MEA_ERROR;
    }
    if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("MEA_API_Get_Globals_Entry failed\n");
        return MEA_OK;
    }



    entry.Local_IP_my_Ipsec = MEA_OS_inet_addr(argv[1]);


    if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("MEA_API_Set_Globals_Entry failed\n");
        return MEA_OK;
    }
    CLI_print("Done.\n");
    return MEA_OK;
}


MEA_Status MEA_CLI_IPSEC_AddDebugCmds(void)
{
 
    CLI_defineCmd("MEA IPSec global set TTL",
        (CmdFunc)MEA_CLI_Set_Globals_Local_IPSec_ttl,
        "Set global IPSec TTL <number>\n",
        "Usage  : IPSec TTL <number>\n"
        "          <number>  value \n"
        "Example: IPSec global set TTL 64 \n");

    CLI_defineCmd("MEA IPSec global set src_Ipv4",
        (CmdFunc)MEA_CLI_Set_Globals_Local_IPSec_src_Ipv4,
        "Set global IPSec src_Ipv4\n",
        "Usage  : src_Ipv4  <IPv4>\n"
        "         <ip> - src Ip address (IP format x.x.x.x) \n"
        "Example: src_Ipv4  192.168.1.2 \n");


    CLI_defineCmd("MEA IPSec global set my_Ipsce_Ipv4",
        (CmdFunc)MEA_CLI_Set_Globals_Localmy_Ipsec,
        "Set global IPSec my_Ipsce_Ipv4\n",
        "Usage  : my_Ipsce_Ipv4  <IPv4>\n"
        "         <ip> - my_Ipsce_Ipv4 address (IP format x.x.x.x) \n"
        "Example: my_Ipsce_Ipv4  192.168.1.2 \n");




    CLI_defineCmd("MEA IPSec ESP show entry",
        (CmdFunc)MEA_CLI_IPSecESP_Show_entry,
        "Show   IPSec ESP configuration \n",
        "show entry all|<Id> \n"
        "---------------------------------------------------\n"
        "  entry  all|< Id> - all or id \n"
        "---------------------------------------------------\n"
        "Examples: - show entry all \n"
        "          - show entry 2    \n"
        "             \n");

    CLI_defineCmd("MEA IPSec ESP set create",
        (CmdFunc)MEA_CLI_IPSecESP_Create,
        "create IPsec ESP configuration \n",

        "   -security_type  <value>  \n"
        "   -TFC_en    <valid>       \n"
        "   -ESN_en    <valid>       \n"
        "   -SPI       <value>       \n"
        "   -Integrity_key           \n"
        "   -Integrity_IV             \n"       
        "   -Confident_key            \n"
        "   -Confident_IV             \n"


   
        "---------------------------------------------------------------\n"
        "    auto - generate new ESP profile id automatically           \n"
        "    <id>   force BFD profile id                                \n"
        "\n"
        "   -Integrity_key   input to the Authentication algorithm      \n"
        "        <value 0-7>   <0x0><0x1><0x2><0x3><0x4><0x5><06><0x7> \n"
        
        "   -Integrity_IV      Initialization Vector that is used for authentication \n"
        "        <value 0-7>   <0x0><0x1><0x2><0x3><0x4><0x5><06><0x7> \n"
        
        "   -Confident_key     input to the Encryption algorithm \n"
        "        <value 0-7>   <0x0><0x1><0x2><0x3><0x4><0x5><06><0x7> \n"

        "   -Confident_IV       \n"
        "        <value 0-7>   <0x0><0x1><0x2><0x3><0x4><0x5><06><0x7> \n"
        
        " -SPI   ESP Security Parameters index                           \n"
        "          <value>                                               \n"
        
        "  -TFC_en  padding insertion enable                             \n"
        "       <valid>  enable/disable                                  \n"
        
        "  -ESN_en  Extended sequence number                            \n"
        "       <valid>  enable/disable                                  \n"
        
        "       -security_type                                           \n"
                "       <value>                                         \n"
         "       Security_INTEG_HMC_MD5_96 = 1                                  \n"
         "       Security_INTEG_HMC_SHA1_96 = 2                                 \n"
         "       Security_INTEG_HMC_SHA256_128 = 3                              \n"
         "       Security_INTEG_HMC_SHA384_192 = 4                              \n"
         "       Security_INTEG_HMC_SHA512_256 = 5                              \n"
         "       Security_INTEG_AES_XCBC_MAC_96 = 6                             \n"
         "       Security_INTEG_AES_CMAC_96 = 7,                                \n"
         "       Security_INTEG_AES_128_GMAC_128 = 0xCB                         \n"
         "       Security_INTEG_AES_192_GMAC_128 = 0xDB                         \n"
         "       Security_INTEG_AES_256_GMAC_128 = 0xEB                         \n"
         "       ------------------------------------------                     \n"
         "       Security_CONFIDENT_3DES_CBC = 0x10,                            \n"
         "       Security_CONFIDENT_AES_128_CBC = 0x40,                         \n"
         "       Security_CONFIDENT_AES_192_CBC = 0x50,                         \n"
         "       Security_CONFIDENT_AES_256_CBC = 0x60,                         \n"
         "       Security_CONFIDENT_AES_128_CTR = 0x80,                         \n"
         "       Security_CONFIDENT_AES_192_CTR = 0x90,                         \n"
         "       Security_CONFIDENT_AES_256_CTR = 0xA0,                         \n"
         "       ------------------------------------------                     \n"
         "       Security_3DES_CBC_HMAC_MD5_96 = 0x11,                          \n"
         "       Security_3DES_CBC_HMAC_SHA1_96 = 0x12,                         \n"
         "       Security_3DES_CBC_HMAC_SHA256_128 = 0x13,                      \n"
         "       ------------------------------------------                     \n"
         "       Security_AES_128_CBC_HMAC_MD5_96 = 0x41,                       \n"
         "       Security_AES_192_CBC_HMAC_MD5_96 = 0x51,                       \n"
         "       Security_AES_256_CBC_HMAC_MD5_96 = 0x61,                       \n"
         "       ------------------------------------------                     \n"
         "       Security_AES_128_CBC_HMAC_SHA1_96 = 0x42,                      \n"
         "       Security_AES_192_CBC_HMAC_SHA1_96 = 0x52,                      \n"
         "       Security_AES_256_CBC_HMAC_SHA1_96 = 0x62,                      \n"
         "       ------------------------------------------                     \n"
         "       Security_AES_128_CBC_HMAC_SHA256_128 = 0x43,                   \n"
         "       Security_AES_192_CBC_HMAC_SHA256_128 = 0x53,                   \n"
         "       Security_AES_256_CBC_HMAC_SHA256_128 = 0x63,                   \n"
         "       ------------------------------------------                     \n"
         "       Security_AES_128_CBC_AES_XCBC_MAC_96 = 0x46,                   \n"
         "       Security_AES_192_CBC_AES_XCBC_MAC_96 = 0x56,                   \n"
         "       Security_AES_256_CBC_AES_XCBC_MAC_96 = 0x66,                   \n"
         "       ------------------------------------------                     \n"
         "       Security_CONFIDENT_AES_128_CTR_HMAC_MD5_96 = 0x81,             \n"
         "       Security_CONFIDENT_AES_192_CTR_HMAC_MD5_96 = 0x91,             \n"
         "       Security_CONFIDENT_AES_256_CTR_HMAC_MD5_96 = 0xa1,             \n"
         "       ------------------------------------------* /                  \n"
         "       Security_CONFIDENT_AES_128_CTR_HMAC_SHA1_96 = 0x82,            \n"
         "       Security_CONFIDENT_AES_192_CTR_HMAC_SHA1_96 = 0x92,            \n"
         "       Security_CONFIDENT_AES_256_CTR_HMAC_SHA1_96 = 0xa2,            \n"
         "       ------------------------------------------                     \n"
         "       Security_CONFIDENT_AES_128_CTR_HMAC_SHA256_128 = 0x83,         \n"
         "       Security_CONFIDENT_AES_192_CTR_HMAC_SHA256_128 = 0x93,         \n"
         "       Security_CONFIDENT_AES_256_CTR_HMAC_SHA256_128 = 0xa3,         \n"
         "       --------------------------------------------------------       \n"
         "       Security_CONFIDENT_AES_128_CTR_AES_XCBC_MAC_96 = 0x86,         \n"
         "       Security_CONFIDENT_AES_192_CTR_AES_XCBC_MAC_96 = 0x96,         \n"
         "       Security_CONFIDENT_AES_256_CTR_AES_XCBC_MAC_96 = 0xa6,         \n"
         "       --------------------------------------------------------       \n"
         "       Security_CONFIDENT_AES_128_GCM_64 = 0xc8,                      \n"
         "       Security_CONFIDENT_AES_128_GCM_96 = 0xc9,                      \n"
         "       Security_CONFIDENT_AES_128_GCM_128 = 0xca,                     \n"
         "       --------------------------------------------------------       \n"
         "       Security_CONFIDENT_AES_192_GCM_64 = 0xd8,                      \n"
         "       Security_CONFIDENT_AES_192_GCM_96 = 0xd9,                      \n"
         "       Security_CONFIDENT_AES_192_GCM_128 = 0xda,                     \n"
         "       ------------------------------------------                     \n"
         "       Security_CONFIDENT_AES_256_GCM_64 = 0xe8,                      \n"
         "       Security_CONFIDENT_AES_256_GCM_96 = 0xe9,                      \n"
         "       Security_CONFIDENT_AES_256_GCM_128 = 0xea,                     \n"
         "       -------------------------------------------------------------- \n"
         "       Security_CONFIDENT_AES_128_CCM_64 = 0xf8,                      \n"
         "       Security_CONFIDENT_AES_128_CCM_96 = 0xf9,                      \n"
         "       Security_CONFIDENT_AES_128_CCM_128 = 0xfa,                     \n"
        "\n"





        "-------------------------------------------------------------------------\n"
        " Examples:\n"
        "  - create auto   \n"
        "                   \n"
            "  - create 1  -security_type 1                                \n"
        "               -Integrity_key 0x0 0x1 0x2 0x3 0x04 0x5 0x6 0x7    \n"
        "               -Integrity_IV  0x0 0x1 0x2 0x3 0x04 0x5 0x6 0x7    \n"
        "               -Confident_key 0x0 0x1 0x2 0x3 0x04 0x5 0x6 0x7    \n"
        "               -Confident_IV  0x0 0x1 0x2 0x3 0x04 0x5 0x6 0x7    \n"
        );



    CLI_defineCmd("MEA IPSec ESP set delete",
        (CmdFunc)MEA_CLI_IPSecESP_Delete,
        "delete    ESP delete profile \n",
        "         all|<Id>   - delete specific Id \n"
        "Examples: - delete 1\n"
        "          - delete all \n"
        "          - delete all \n");




    return MEA_OK;
}

#if IPSEC_ADAPTOR_WANTED
static MEA_Status adap_cli_ipsec_init(int argc, char *argv[])
{
	adap_ipsec_params params;

	params.ipsec_ttl = 0x25;
	params.my_ip = 0xc0a80001;
	params.nvgre_sip = 0xc0a80006;
	params.nvgre_ttl = 0x52;
	params.nvgre_vlan = 6;
	params.nvgre_smac.ether_addr_octet[0] = 0xaa;
	params.nvgre_smac.ether_addr_octet[1] = 0xbb;
	params.nvgre_smac.ether_addr_octet[2] = 0xcc;
	params.nvgre_smac.ether_addr_octet[3] = 0xdd;
	params.nvgre_smac.ether_addr_octet[4] = 0xee;
	params.nvgre_smac.ether_addr_octet[5] = 0xfe;

	EnetHal_ipsec_init(&params);

    return MEA_OK;
}

static MEA_Status adap_cli_ipsec_deinit(int argc, char *argv[])
{
	EnetHal_ipsec_deinit();

    return MEA_OK;
}

static MEA_Status adap_cli_ipsec_create_policy(int argc, char *argv[])
{
	adap_ipsec_policy policy;
	adap_ipsec_policy_id policy_id;

	policy.action = ADAP_IPSEC_POLICY_ACTION_PROTECT;
	policy.priority = 6;
//	policy.dir = ADAP_IPSEC_POLICY_DIR_OUT;
	policy.dir = ADAP_IPSEC_POLICY_DIR_IN;
	policy.ifindex = 1;
	policy.tagged = ADAP_FALSE;
	policy.selectors.sip_mask = 0;
	policy.selectors.dip_mask = 0;
	policy.selectors.next_proto_mode = ADAP_IPSEC_POLICY_SELECTOR_MODE_VALUE;
	policy.selectors.next_proto = 0x11;
	policy.selectors.port_selectors.sport_mode = ADAP_IPSEC_POLICY_SELECTOR_MODE_ANY;
	policy.selectors.port_selectors.dport_mode = ADAP_IPSEC_POLICY_SELECTOR_MODE_VALUE;
	policy.selectors.port_selectors.dport_start = 123;
	policy.selectors.port_selectors.dport_end = 123;

	EnetHal_ipsec_create_policy(&policy,
    		                    &policy_id);

    CLI_print("Policy ID is %d\n", policy_id);

    return MEA_OK;
}

static MEA_Status adap_cli_ipsec_get_policy(int argc, char *argv[])
{
	adap_ipsec_policy policy;

	memset(&policy, 0, sizeof(adap_ipsec_policy));
	if (EnetHal_ipsec_get_policy(0, &policy) == ENETHAL_STATUS_SUCCESS) {
		CLI_print("Policy: {%d, %d, %d, %d, %d, %d}\n", policy.action, policy.priority, policy.dir, policy.ifindex, policy.vlan_id, policy.selectors.next_proto_mode);
	}

    return MEA_OK;
}

static MEA_Status adap_cli_ipsec_destroy_policy(int argc, char *argv[])
{
	EnetHal_ipsec_destroy_policy(0);

    return MEA_OK;
}

static MEA_Status adap_cli_ipsec_create_sa(int argc, char *argv[])
{
	adap_ipsec_sa sa;
	adap_ipsec_sa_id sa_id;
	int i;

	sa.selectors.spi = 0xcafecafe;
	sa.mode = ADAP_IPSEC_MODE_TUNNEL;
	sa.nonce = 0x12345678;
	sa.auth_alg = ADAP_AUTH_ALG_SHA1_96;
	sa.encr_alg = ADAP_ENCR_ALG_AES_128_CTR;

	sa.encap = ADAP_IPSEC_POLICY_ENCAP_IPSEC_ONLY;
	sa.ipsec.dmac.ether_addr_octet[0] = 0x00;
	sa.ipsec.dmac.ether_addr_octet[1] = 0x11;
	sa.ipsec.dmac.ether_addr_octet[2] = 0x22;
	sa.ipsec.dmac.ether_addr_octet[3] = 0x33;
	sa.ipsec.dmac.ether_addr_octet[4] = 0x44;
	sa.ipsec.dmac.ether_addr_octet[5] = 0x55;
	sa.ipsec.smac.ether_addr_octet[0] = 0x00;
	sa.ipsec.smac.ether_addr_octet[1] = 0x11;
	sa.ipsec.smac.ether_addr_octet[2] = 0x22;
	sa.ipsec.smac.ether_addr_octet[3] = 0x33;
	sa.ipsec.smac.ether_addr_octet[4] = 0x44;
	sa.ipsec.smac.ether_addr_octet[5] = 0x66;

//	6501c7e20816bbab8acfe264ae33c2be2015d37ba76ac0679cd82229f2ce71dc217d061ca5d6842c99134b51451d5f3b207f7b89f858f71945aeb64dad08f45f330c9610de928cea4c7cff1ba3982ebf1414d9a1

//	sa.encap = ADAP_IPSEC_POLICY_ENCAP_IPSEC_NVGRE;
//	sa.nvgre.flow_id = 0x5a;
//	sa.nvgre.outer_dmac[0] = 0x00;
//	sa.nvgre.outer_dmac[1] = 0x11;
//	sa.nvgre.outer_dmac[2] = 0x22;
//	sa.nvgre.outer_dmac[3] = 0x33;
//	sa.nvgre.outer_dmac[4] = 0x44;
//	sa.nvgre.outer_dmac[5] = 0x66;
//	sa.nvgre.virtual_subnet_id = 0xdeadbeef;
//	sa.nvgre.gre_dip = 0xc0a80003;


//	sa.encap = ADAP_IPSEC_POLICY_ENCAP_IPSEC_VXLAN;
//	sa.vxlan.dip = 0xc0a80003;
//	sa.vxlan.dport = 0x123;
//	sa.vxlan.dport = 0x124;
//	sa.vxlan.network_id = 60;
//	sa.vxlan.sgw = 1;
//	sa.vxlan.outer_dmac[0] = 0x00;
//	sa.vxlan.outer_dmac[1] = 0x11;
//	sa.vxlan.outer_dmac[2] = 0x22;
//	sa.vxlan.outer_dmac[3] = 0x33;
//	sa.vxlan.outer_dmac[4] = 0x44;
//	sa.vxlan.outer_dmac[5] = 0x55;
//	sa.vxlan.ttl = 0x45;

	sa.selectors.tunnel_dip = 0xc0a80001;
	sa.selectors.tunnel_sip = 0xc0a80002;
	for(i=0;i<ADAP_AUTH_KEY_MAX_LENGTH;i++) {
		sa.auth_key[i] = i;
	}
	for(i=0;i<ADAP_ENCR_KEY_MAX_LENGTH;i++) {
		sa.encr_key[i] = i;
	}
	sa.output_port = 104;

	EnetHal_ipsec_create_sa(&sa,
    		                &sa_id);

    CLI_print("SA ID is %d\n", sa_id);

    return MEA_OK;
}

static MEA_Status adap_cli_ipsec_get_sa(int argc, char *argv[])
{
	adap_ipsec_sa sa;

	memset(&sa, 0, sizeof(adap_ipsec_policy));
	if (EnetHal_ipsec_get_sa(0, &sa) == ENETHAL_STATUS_SUCCESS) {
		CLI_print("Policy: {%d, %d, %d, %d}\n", sa.mode, sa.auth_alg, sa.encr_alg, sa.output_port);
	}

    return MEA_OK;
}

static MEA_Status adap_cli_ipsec_destroy_sa(int argc, char *argv[])
{
	EnetHal_ipsec_destroy_sa(0);

    return MEA_OK;
}

static MEA_Status adap_cli_ipsec_attach(int argc, char *argv[])
{
//	EnetHal_ipsec_attach_sa(0, 0);
	EnetHal_ipsec_attach_sa(32, 0);

    return MEA_OK;
}

static MEA_Status adap_cli_ipsec_detach(int argc, char *argv[])
{
	EnetHal_ipsec_detach_sa(0);

    return MEA_OK;
}

MEA_Status ADAP_CLI_IPSEC_AddDebugCmds(void)
{

    CLI_defineCmd("ADAP IPSec init",
        (CmdFunc)adap_cli_ipsec_init,
        "IPsec init\n",
        "Usage  : IPSec init\n");

    CLI_defineCmd("ADAP IPSec deinit",
        (CmdFunc)adap_cli_ipsec_deinit,
        "IPsec deinit\n",
        "Usage  : IPSec deinit\n");

    CLI_defineCmd("ADAP IPSec create policy",
        (CmdFunc)adap_cli_ipsec_create_policy,
        "IPsec create policy\n",
        "Usage  : IPSec create policy\n");

    CLI_defineCmd("ADAP IPSec get policy",
        (CmdFunc)adap_cli_ipsec_get_policy,
        "IPsec get policy\n",
        "Usage  : IPSec get policy\n");

    CLI_defineCmd("ADAP IPSec destroy policy",
        (CmdFunc)adap_cli_ipsec_destroy_policy,
        "IPsec destroy policy\n",
        "Usage  : IPSec destroy policy\n");

    CLI_defineCmd("ADAP IPSec create sa",
        (CmdFunc)adap_cli_ipsec_create_sa,
        "IPsec create sa\n",
        "Usage  : IPSec create sa\n");

    CLI_defineCmd("ADAP IPSec get sa",
        (CmdFunc)adap_cli_ipsec_get_sa,
        "IPsec get sa\n",
        "Usage  : IPSec get sa\n");

    CLI_defineCmd("ADAP IPSec destroy sa",
        (CmdFunc)adap_cli_ipsec_destroy_sa,
        "IPsec destroy sa\n",
        "Usage  : IPSec destroy sa\n");

    CLI_defineCmd("ADAP IPSec attach",
        (CmdFunc)adap_cli_ipsec_attach,
        "IPsec attach\n",
        "Usage  : IPSec attach\n");

    CLI_defineCmd("ADAP IPSec detach",
        (CmdFunc)adap_cli_ipsec_detach,
        "IPsec detach\n",
        "Usage  : IPSec detach\n");


    return MEA_OK;
}

#endif // #ifdef IPSEC_ADAPTOR_WANTED

#endif //MEA_IPSec_ENABLE

