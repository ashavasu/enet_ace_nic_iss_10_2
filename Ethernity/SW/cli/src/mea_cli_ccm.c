/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_ccm_drv.h"
#include "mea_cli_ccm.h"

#if MEA_CCM_ENABLE
static MEA_Status MEA_CLI_CCM_Create(int argc, char *argv[]);
static MEA_Status MEA_CLI_CCM_Set(int argc, char *argv[]);
static MEA_Status MEA_CLI_CCM_Delete(int argc, char *argv[]);
static MEA_Status MEA_CLI_CCM_Show_entry(int argc, char *argv[]);
static MEA_Status MEA_CLI_CCM_Show_MEG(int argc, char *argv[]);

static MEA_Status MEA_CLI_CCM_Show_cc_Event(int argc, char *argv[]);
static MEA_Status MEA_CLI_CCM_Show_cc_group(int argc, char *argv[]);
static MEA_Status MEA_CLI_CCM_Show_rdi_Event(int argc, char *argv[]);
static MEA_Status MEA_CLI_CCM_Show_rdi_group(int argc, char *argv[]);

static MEA_Status MEA_CLI_Clear_Counters_LM(int argc, char *argv[]);
static MEA_Status MEA_CLI_Collect_Counters_LM(int argc, char *argv[]);
static MEA_Status MEA_CLI_Show_Counters_LM(int argc, char *argv[]);

static MEA_Status MEA_CLI_Clear_Counters_CCM(int argc, char *argv[]);
static MEA_Status MEA_CLI_Collect_Counters_CCM(int argc, char *argv[]);
static MEA_Status MEA_CLI_Show_Counters_CCM(int argc, char *argv[]);

static MEA_Status MEA_CLI_Clear_Counters_analyzer_LM(int argc, char *argv[]);

MEA_Status MEA_CLI_CCM_AddDebugCmds(void)
{


    CLI_defineCmd("MEA counters LM collect",
                  (CmdFunc)MEA_CLI_Collect_Counters_LM,
                  "Collect LM Counters from HW",
                  "collect <lmId>|all\n"
                  "        <lmId> - LM Id OR all \n"
                  "Examples: - collect 1  \n" 
                  "          - collect all\n");

    CLI_defineCmd("MEA counters LM clear",
                  (CmdFunc)MEA_CLI_Clear_Counters_LM,
                  "Clear   LM Counters",
                  "clear <lmId>|all\n"
                  "      <lmId> - LM Id OR all \n"
                  "Examples: - clear 1  \n" 
                  "          - clear all\n");

    CLI_defineCmd("MEA counters LM show",
                  (CmdFunc)MEA_CLI_Show_Counters_LM,
                  "Show    LM Counters",
                  "show all|<from Id> [<to_Id>]\n"
                  "     <from Id> - From Id Id OR all \n"
                  "     <to   Id> - To    Id \n"
                  "Examples: - show 1  \n" 
                  "          - show 10 15\n"
                  "          - show all\n");

    CLI_defineCmd("MEA counters CCM collect",
                  (CmdFunc)MEA_CLI_Collect_Counters_CCM,
                  "Collect CCM Counters from HW",
                  "collect <ccmId>|all\n"
                  "        <ccmId> - CCM Id OR all \n"
                  "Examples: - collect 1  \n" 
                  "          - collect all\n");

    CLI_defineCmd("MEA counters CCM clear",
                  (CmdFunc)MEA_CLI_Clear_Counters_CCM,
                  "Clear   CCM Counters",
                  "clear <Id>|all\n"
                  "      <Id> - LM Id OR all \n"
                  "Examples: - clear 1  \n" 
                  "          - clear all\n");

	CLI_defineCmd("MEA counters CCM show",
                  (CmdFunc)MEA_CLI_Show_Counters_CCM,
                  "Show    CCM Counters",
                  "show all|<from Id> [<to_Id>]\n"
                  "     <from Id> - From Id Id OR all \n"
                  "     <to   ccmId> - To   Id \n"
                  "Examples: - show 1  \n" 
                  "          - show 10 15\n"
                  "          - show all\n");





       CLI_defineCmd("MEA analyzer CCM show entry",
                  (CmdFunc)MEA_CLI_CCM_Show_entry, 
                  "Show   CCM stream configuration \n",
                  "show entry all|<CCM Id> \n"
                  "---------------------------------------------------\n"
                  "  entry  all|<CCM Id> - all or specific CCM id \n"
                  "---------------------------------------------------\n"
                  "Examples: - show entry all \n"
                  "          - show entry 2    \n"
                  "             \n");

        CLI_defineCmd("MEA analyzer CCM show meg",
                  (CmdFunc)MEA_CLI_CCM_Show_MEG, 
                  "Show   CCM stream configuration meg\n",
                  "show meg all|<CCM Id> \n"
                  "---------------------------------------------------\n"
                  "  meg  all|<CCM Id> - all or specific CCM id \n"
                  "---------------------------------------------------\n"
                  "Examples: - show meg all \n"
                  "          - show meg 2    \n"
                  "                 \n");

        




     CLI_defineCmd("MEA analyzer CCM show event cc",
                  (CmdFunc)MEA_CLI_CCM_Show_cc_Event, 
                  "Show   CC event\n",
                  "show cc all \n"
                  "---------------------------------------------------\n"
                  "                                                   \n"
                  "---------------------------------------------------\n"
                  "Examples: - show event cc all \n"
                  "                              \n"
                  "                              \n");

    CLI_defineCmd("MEA analyzer CCM show event cc_group",
                  (CmdFunc)MEA_CLI_CCM_Show_cc_group, 
                  "Show   cc_group\n",
                  "show cc_group all|<group> \n"
                  "---------------------------------------------------\n"
                  "                                                   \n"
                  "---------------------------------------------------\n"
                  "Examples: - show event cc_group all \n"
                  "            show event cc_group 1 \n"
                  "                              \n");

     CLI_defineCmd("MEA analyzer CCM show event rdi",
                  (CmdFunc)MEA_CLI_CCM_Show_rdi_Event, 
                  "Show   rdi event\n",
                  "show rdi all  \n"
                  "---------------------------------------------------\n"
                  "                                                   \n"
                  "---------------------------------------------------\n"
                  "Examples: - show event rdi all \n"
                  "                              \n"
                  "                              \n");

     CLI_defineCmd("MEA analyzer CCM show event rdi_group",
                  (CmdFunc)MEA_CLI_CCM_Show_rdi_group, 
                  "Show   rdi_group\n",
                  "show rdi all|<group> \n"
                  "---------------------------------------------------\n"
                  "                                                   \n"
                  "---------------------------------------------------\n"
                  "Examples: - show event rdi_group all \n"
                  "            show event rdi_group 1 \n"
                  "                              \n");




    CLI_defineCmd("MEA analyzer CCM set delete",
                  (CmdFunc)MEA_CLI_CCM_Delete, 
                  "delete    CCM stream configuration\n",
                  "         all|<Id>   - delete specific Id \n"
                  "Examples: - delete 1\n"
                  "          - delete all \n"
                  "          - delete all \n");

    CLI_defineCmd("MEA analyzer CCM set modify",
                  (CmdFunc)MEA_CLI_CCM_Set, 
                  "modify   ccmId configuration\n",
                  "    [-meg_data <len> <data-0....data-n>]\n"
                  "    [-mep_id   <id> ]\n"
                  "    [-period_id <id> ]\n"
                  "    [-time_event <value> ]\n"
                  "    [-event_mask <mask_group>]\n"
                  "    [-event_block] <block> ]\n"
                  "    [-clear_segid <enable> <value> ]\n"
                  "--------------------------------------------------------------------------------\n"
                  "    <id>   force CCM profile id                                \n"
                  "\n"
                  "     -meg_data    expected MEG Data\n"
                  "         <length>  max is 48 byte\n"
                  "         <byte0>   : First  byte of Meg (2 hex digits) \n"
                  "         <byte1>   : Second byte of Meg (2 hex digits) \n"
                  "     -mep_id      expected MEP Id\n"
                  "            <id value>  mep id value \n"
                  "     -event_mask  Mask stream period event in the group \n"
                  "             <mask_group> 0 disable 1 enable           \n"
                  "     -event_block  <block all event> ]\n"
                  "             <block> 0 disable 1 enable           \n"
                  "     -period_id   the period id at the stream packet\n"
                  "             <value> 1 to 7 \n"
                  "     -time_event  Time to the receipt of event\n"
                  "             <value>   1 -   3.3ms \n"
                  "                       2 -  10msec\n"
                  "                       3 - 100msec\n"
                  "                       4 -   1 sec\n"
                  //"                       5 -  10 sec\n"
                  "                       6 -   1 Min\n"
                  "                       7 -  10 Min\n"
                  "     -clear_segid  clear the sequence \n"
                  "             <enable> 0 disable 1 enable \n"
                  "--------------------------------------------------------------------------------\n"
                  " Examples:\n" 
                  "   - modify 1 -meg_data 18 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18\n"  
                  "              -mep_id 1234 -time_event 2\n"
                  "   - modify 1  -clear_segid 1 100 -mep_id 1234\n"
                  "                   \n");
	CLI_defineCmd("MEA analyzer CCM set create",
                  (CmdFunc)MEA_CLI_CCM_Create, 
                  "create ccm stream configuration \n",
                  "    [-meg_data   <len> <byte0....byte..>]\n"
				  "    [-mep_id     <id> ]\n"
				  "    [-period_id   <id> ]\n"
				  "    [-time_event <value> ]\n"
                  "    [-event_mask <mask_group>\n"
                  "    [-event_block] <block> ]\n"
				  "    [-clear_segid <enable> <value> ]\n"
				  "    [-type_mode  <mode> ]\n"
                  "---------------------------------------------------------------\n"
                  "    auto - generate new CCM profile id automatically           \n"
                  "    <id>   force CCM profile id                                \n"
                  "\n"
                  "     -meg_data    expected MEG Data\n"
                  "         <length>  max is 48 byte\n"
                  "         <byte0>   : First  byte of Meg (2 hex digits) \n"
                  "         <byte1>   : Second byte of Meg (2 hex digits) \n"
                  "     -mep_id      expected MEP Id\n"
                  "            <id value>   mep id value \n"
                  "     -event_mask  Mask stream period event in the group \n"
                  "             <mask_group> 0 disable 1 enable           \n"
                  "     -event_block  <block all event> ]\n"
                  "             <block> 0 disable 1 enable           \n"
                  "     -period_id   expected period Id\n"
                  "             <value> 1 to 7 \n"
                  "     -time_event  Time to the receipt of event\n"
                  "             <value>   1 -   3.3ms \n"
                  "                       2 -  10msec\n"
                  "                       3 - 100msec\n"
                  "                       4 -   1 sec\n"
                  "                       5 -  10 sec\n"
                  "                       6 -   1 Min\n"
                  "                       7 -  10 Min\n"
                  "     -clear_segid  clear the sequence \n"
                  "             <enable> 0 disable 1 enable \n"
                  "    -type_mode (set the mode analyzer )\n"
                  "               <mode> 0-ccm 1-lmr \n"
                  "-------------------------------------------------------------------------\n"
                  " Examples:\n" 
                  "  - create auto -mep_id 1234 -time_event 2 -clear_segid 0 0 -period_id 1\n"
                  "                  -meg_data 10 00 01 02 03 04 05 06 07 08 09 \n"
                  "  - create 1 -mep_id 1234 -time_event 1  -clear_segid 0 0 -period_id 1\n" 
                  "              -clear_segid 1 100 \n"
                  "                   \n");

    CLI_defineCmd("MEA debug counters analyzer_LM clear",
        (CmdFunc)MEA_CLI_Clear_Counters_analyzer_LM,
        "Clear   analyzer_LM",
        "clear <Id>|all\n"
        "      <Id> - Id OR all \n"
        "Examples: - clear 1  \n" 
        "          - clear all\n");

    return MEA_OK;
}


static MEA_Status mea_Cli_ccm_CommandResult(int              argc,
                                     char                   *argv[],
                                     MEA_Bool               flagType,
                                     MEA_Uint16             startIndex,
                                     MEA_CCM_Configure_dbt  *entry)
{
    int i,j;
    int num_of_params;
    int len;
    
    
    for(i=(startIndex); i<argc ;i++){
        
        if(!MEA_OS_strcmp(argv[i],"-meg_data")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            len  = MEA_OS_atoi(argv[i+1]);
            if(len > MEA_ANALYZER_CCM_MAX_MEG_DATA){
                 CLI_print("Error: meg_data len %d > \n",MEA_ANALYZER_CCM_MAX_MEG_DATA);
                return MEA_ERROR;
            }
            if ((i+num_of_params + len )>argc) {
                CLI_print("Error: Not enough parameter for option %s len\n",argv[i]);
                return MEA_ERROR;
            }
            for(j=0;j<len;j++){
                entry->MEG_data[j]=(MEA_Uint8)MEA_OS_atohex(argv[i+2+j]);
            }
            i += num_of_params+len;
            continue;
            
        }
        if(!MEA_OS_strcmp(argv[i],"-mep_id")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->expected_MEP_Id = MEA_OS_atoiNum(argv[i + 1]);
            

            i += num_of_params;
            continue;
            
        }
		if(!MEA_OS_strcmp(argv[i],"-type_mode")){ 

			num_of_params=1;
			if ((i+num_of_params)>argc ) {
				CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
				return MEA_ERROR;
			}
			entry->mode_ccm_lmr= MEA_OS_atoi(argv[i+1]);


			i += num_of_params;
			continue;

		}
		
        
        if(!MEA_OS_strcmp(argv[i],"-period_id")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->expected_period = MEA_OS_atoiNum(argv[i + 1]);
            
            i += num_of_params;
            continue;
        
        }
        if(!MEA_OS_strcmp(argv[i],"-event_mask")){
         num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->mask_group = MEA_OS_atoiNum(argv[i + 1]);
       

        i += num_of_params;
        continue;

        }
        if (!MEA_OS_strcmp(argv[i], "-event_block")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry->block = MEA_OS_atoiNum(argv[i + 1]);


            i += num_of_params;
            continue;

        }
        
        if(!MEA_OS_strcmp(argv[i],"-time_event")){ 
            
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->period_event = MEA_OS_atoiNum(argv[i + 1]);
            
            i += num_of_params;
            continue;
        
        }
        if(!MEA_OS_strcmp(argv[i],"-clear_segid")){ 
            
            num_of_params=2;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry->enable_clearSeq  = MEA_OS_atoi(argv[i+1]);
            entry->value_nextSeq = MEA_OS_atoiNum(argv[i + 2]);
            
            i += num_of_params;
            continue;
        
        }
        
        
    
    }//end for


    return MEA_OK;
}


static MEA_Status MEA_CLI_CCM_Create(int argc, char *argv[])
{

    MEA_CCM_Configure_dbt      entry;
    MEA_CcmId_t                id_io;
    int i;

    if (argc < 2) {
       return MEA_ERROR;
    }
    
    for (i=1; i < argc; i++) {
         
        if(  (argv[i][0] == '-'        )   && (
            (MEA_OS_strcmp(argv[i],"-meg_data")            != 0) &&
            (MEA_OS_strcmp(argv[i],"-mep_id")              != 0) &&
            (MEA_OS_strcmp(argv[i],"-period_id")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-event_mask")          != 0) &&
            (MEA_OS_strcmp(argv[i], "-event_block")        != 0) &&
            (MEA_OS_strcmp(argv[i],"-time_event")          != 0) &&
            (MEA_OS_strcmp(argv[i],"-type_mode")           != 0) &&
            
            (MEA_OS_strcmp(argv[i],"-clear_segid")         != 0) )){
                CLI_print("ERROR: Not support option %s \n",argv[i]);
            return MEA_OK;
        }
    }
    MEA_OS_memset(&entry,0,sizeof(entry));

    if(!MEA_OS_strcmp(argv[1],"auto")){ /* only for create*/
            id_io=MEA_PLAT_GENERATE_NEW_ID;
    }else{
     id_io=MEA_OS_atoi(argv[1]);
    }

    if(mea_Cli_ccm_CommandResult(argc,argv,MEA_FALSE,
        2,&entry)!=MEA_OK){
       CLI_print("Error: mea_PacketGenCommandResult \n");
        return MEA_OK;
    } 

   if(MEA_API_Create_CCM_Entry( MEA_UNIT_0,&entry,&id_io)!=MEA_OK){
        CLI_print("Error: can't create CCM_Entry \n");
         return MEA_ERROR;
    }
    
    
    
    CLI_print("Done create CCM with Id = %d  \n",id_io); 
    return MEA_OK;
}

static MEA_Status MEA_CLI_CCM_Delete(int argc, char *argv[])
{
    MEA_CcmId_t  ccmId ,start_ccm,end_ccm;
    MEA_Bool      exist;
    
    if (argc < 2) {
       return MEA_ERROR;
    }

    if(!MEA_OS_strcmp(argv[1],"all")){
     start_ccm=0;
     if(MEA_ANALYZER_MAX_STREAMS_TYPE2)
        end_ccm=MEA_ANALYZER_MAX_STREAMS_TYPE2-1;
     else
        end_ccm=0;
    } else {
        start_ccm=(MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_ccm=start_ccm;
        if ((MEA_API_IsExist_CCM(MEA_UNIT_0,start_ccm,&exist)!=MEA_OK) ||
            (!exist)) {
            CLI_print(" ERROR: CCM id %d is not exist \n",start_ccm);
            return MEA_OK;
        }
    }
    
    for(ccmId=start_ccm; ccmId <=end_ccm; ccmId++){    
        if(MEA_API_IsExist_CCM(MEA_UNIT_0,ccmId,&exist)==MEA_OK){
            if (exist){
               
                if(MEA_API_Delete_CCM_Entry(MEA_UNIT_0,ccmId )!=MEA_OK){
                   CLI_print(" ERROR: can't Delete CCM id %d \n",ccmId);
                }
            }
        }
    }
    
    
    return MEA_OK;
}


static MEA_Status MEA_CLI_CCM_Set(int argc, char *argv[])
{

    MEA_CCM_Configure_dbt      entry;
    MEA_CcmId_t                id_io;
    MEA_Bool                   exist;
    int i;
 
    if (argc < 2) {
       return MEA_ERROR;
    }
    
    for (i=1; i < argc; i++) {
         
        if(  (argv[i][0] == '-'        )   && (
            (MEA_OS_strcmp(argv[i],"-meg_data")             != 0) &&
            (MEA_OS_strcmp(argv[i],"-mep_id")               != 0) &&
            (MEA_OS_strcmp(argv[i],"-period_id")            != 0) &&
            (MEA_OS_strcmp(argv[i],"-time_event")           != 0) &&
            (MEA_OS_strcmp(argv[i],"-event_mask")           != 0) &&
            (MEA_OS_strcmp(argv[i], "-event_block")         != 0) &&
            (MEA_OS_strcmp(argv[i],"-clear_segid")          != 0) )){
                CLI_print("ERROR: Not support option %s \n",argv[i]);
            return MEA_OK;
        }
    }

   
     id_io=MEA_OS_atoi(argv[1]);

   // check if the id is Exist
     if(MEA_API_IsExist_CCM(MEA_UNIT_0,id_io,&exist)!=MEA_OK){
     CLI_print("Error: MEA_API_IsExist_CCM \n");
        return MEA_OK;
     }

    if(MEA_API_Get_CCM_Entry(MEA_UNIT_0,
                             id_io,
                             &entry) !=MEA_OK){
        CLI_print("Error: MEA_API_Get_CCM_Entry  %d \n",id_io);
        return MEA_OK;
    }

    
     if(mea_Cli_ccm_CommandResult(argc,argv,MEA_FALSE,
        2,&entry)!=MEA_OK){
       CLI_print("Error: mea_Cli_ccm_CommandResult \n");
        return MEA_OK;
    } 

   if(MEA_API_Set_CCM_Entry( MEA_UNIT_0,id_io,&entry)!=MEA_OK){
        CLI_print("Error: MEA_API_Set_CCM_Entry %d \n",id_io);
         return MEA_ERROR;
    }
    

    return MEA_OK;
}

static MEA_Status MEA_CLI_Clear_Counters_analyzer_LM(int argc, char *argv[])
{
 
    
    MEA_Bool                   exist;
    int i,start_from,end;

    if (MEA_OS_strcmp(argv[1],"all") == 0){
        start_from=0;
        end=MEA_ANALYZER_MAX_STREAMS_TYPE2;
    } else{
        start_from=end=MEA_OS_atoi(argv[1]);

    }



    for(i=start_from;i< end;i++){
        if(MEA_API_IsExist_CCM(MEA_UNIT_0,i,&exist)!=MEA_OK){
            CLI_print("Error: MEA_API_IsExist_CCM \n");
            return MEA_OK;
        }
        if(!exist)
            continue;

        if(mea_ccm_drv_clear_lm_and_Sequenc(MEA_UNIT_0,(MEA_CcmId_t)i)!=MEA_OK){
            CLI_print("Error: mea_ccm_drv_clear_lm_and_Sequenc \n");
            return MEA_OK;
        }

    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_CCM_Show_entry(int argc, char *argv[])
{
 
    MEA_CcmId_t                  Id_i,start,end;
    MEA_CCM_Configure_dbt          entry;
    MEA_Bool                         exist;
    MEA_Uint32                       count;
    char       *str_period[] = {
        "NA",
        " 3.33ms",
        "   10ms",
        "  100ms",
        "   1sec",
        "  10sec",
        "   1min",
        "  10min"
        "ERROR  "
    };

    
    if (argc < 2) {
       return MEA_ERROR;
    }
    if (!MEA_PACKET_ANALYZER_TYPE2_SUPPORT){
        CLI_print("%s - ccm not support \n",
                          __FUNCTION__);
        return MEA_OK;   
    }


    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        if(MEA_ANALYZER_MAX_STREAMS_TYPE2)
            end=(MEA_PacketGen_t)(MEA_ANALYZER_MAX_STREAMS_TYPE2-1);
        else
           end=0;
    } else{
          Id_i=MEA_OS_atoi(argv[1]);
          if(Id_i>MEA_ANALYZER_MAX_STREAMS_TYPE2){
          CLI_print("ERROR Id % is out of range\n",Id_i);
          return MEA_OK;
          }
          start=end=Id_i;
    }
    count=0;
    MEA_OS_memset(&entry,0,sizeof(entry));

    for(Id_i=start; Id_i<=end ;Id_i++){
        if (MEA_API_IsExist_CCM(MEA_UNIT_0,Id_i,&exist) != MEA_OK) {
             CLI_print("ERROR MEA_API_IsExist_CCM failed (id=%d)\n",Id_i);
             return MEA_OK;
        }
        if (!exist) {
            continue;
        }    
        
        if(MEA_API_Get_CCM_Entry(MEA_UNIT_0,
                                    Id_i,
                                    &entry)!=MEA_OK){

        }
       
        if (count++ == 0) {
            CLI_print("\n");
            CLI_print("           CCM Entry      \n"
                      " ----------------------------\n"
                      "      MEP  period Time     mode mask  block\n"
                      "  ID  id   id     event         group      \n"
                      " ---- ---- ------ -------- ---- ----- -----\n");
                    // 01234567890123456789012345678901234567890123456789012345678901234567890123456789
        }
        
        CLI_print(" %4d %4d %6d %8s %4s %5s %5s\n", Id_i,
            entry.expected_MEP_Id,
            entry.expected_period,
            str_period[entry.period_event],
            ((entry.mode_ccm_lmr == 0) ? "CCM" : "LMR"),
            MEA_STATUS_STR(entry.mask_group),
            MEA_STATUS_STR(entry.block)
            );
        
    }
    
    if(count == 0){
        CLI_print("CCM Table is empty\n");
    }




    
    
    return MEA_OK;
}



static MEA_Status MEA_CLI_CCM_Show_MEG(int argc, char *argv[])
{
 
    MEA_CcmId_t                  Id_i,start,end;
    MEA_CCM_Configure_dbt          entry;
    MEA_Bool                         exist;
    MEA_Uint32                       count;
    MEA_Uint32 i,j;
    MEA_Uint32 crc;
    
    if (argc < 2) {
       return MEA_ERROR;
    }
    if (!MEA_PACKET_ANALYZER_TYPE2_SUPPORT){
        CLI_print("%s - ccm not support \n",
                          __FUNCTION__);
        return MEA_OK;   
    }


    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        if (MEA_ANALYZER_MAX_STREAMS_TYPE2)
         end=(MEA_PacketGen_t)(MEA_ANALYZER_MAX_STREAMS_TYPE2-1);
        else
          end=0;

    } else{
          Id_i=MEA_OS_atoi(argv[1]);
          if(Id_i>MEA_ANALYZER_MAX_STREAMS_TYPE2){
          CLI_print("ERROR Id % is out of range\n",Id_i);
          return MEA_OK;
          }
          start=end=Id_i;
    }
    count=0;

    for(Id_i=start; Id_i<=end ;Id_i++){
        if (MEA_API_IsExist_CCM(MEA_UNIT_0,Id_i,&exist) != MEA_OK) {
             CLI_print("ERROR MEA_API_IsExist_CCM failed (id=%d)\n",Id_i);
             return MEA_OK;
        }
        if (!exist) {
            continue;
        }    
        
        if(MEA_API_Get_CCM_Entry(MEA_UNIT_0,
                                    Id_i,
                                    &entry)!=MEA_OK){

        }
        crc = MEA_OS_crcsum(&entry.MEG_data[0], 
                            MEA_ANALYZER_CCM_MAX_MEG_DATA,
                            MEA_CRC_INIT);
        if (count++ == 0) {
            CLI_print("\n");
            CLI_print("           CCM MEG      \n"
                      " -------------------------\n"
                      "      MEG Data    \n"
                      "  ID           \n"
                      " ---- ---------------------------------------------\n");
                    // 01234567890123456789012345678901234567890123456789012345678901234567890123456789
        }
        
        CLI_print(" %4d ",Id_i);
        

        for(i=0;i<6; i++){
            if(i != 0){
                CLI_print("      ");
            }
            for (j=0;j< 8;j++) {
                CLI_print("%02x ",entry.MEG_data[(i*8)+j]);
            }
            CLI_print("\n");
       }
       CLI_print("      ");
       CLI_print("CRC16: 0x%04x\n",crc);


        
    }
    
    if(count == 0){
        CLI_print("CCM Table is empty\n");
    }




    
    
    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                    <MEA_CLI_CCM_Show_cc_Event>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

static MEA_Status MEA_CLI_CCM_Show_cc_Event(int argc, char *argv[])
{
    MEA_CC_RDI_Event_group_dbt entry;
    MEA_Bool                   exist;
    MEA_Uint32                 i;
    if (argc < 2) {
       return MEA_ERROR;
    }
    if (MEA_API_Get_CC_Event(MEA_UNIT_0, &entry ,&exist)!=MEA_OK){
        CLI_print(" MEA_API_Get_CC_Event failed\n");
    return MEA_OK;
    }
    if(exist){
        for(i=0;i<MEA_CC_RDI_MAX_GROUP;i++){
           CLI_print("CC Group[%d]= %s\n",i,MEA_STATUS_STR(entry.Group[i]));
        }
    }
    if(!exist){
    CLI_print(" no exist event for CC\n");
    }
    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                    <MEA_CLI_CCM_Show_cc_group>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_CCM_Show_cc_group(int argc, char *argv[])
{
    MEA_Uint32                 i,j;
    MEA_Uint32                 Event;
    MEA_Uint32                 value;
    MEA_Uint32                 start,end;
    
    if (argc < 2) {
       return MEA_ERROR;
    }
    
    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        end=   MEA_ANALYZER_MAX_CC_GROUP; //(MEA_ANALYZER_MAX_STREAMS_TYPE2 / 32)  ;//(MEA_CC_RDI_MAX_GROUP);
        
		if(end != 0)
		  end=end-1;
		
		
		
		
		

    } else{
          
          start=end=MEA_OS_atoi(argv[1]);
          
         
          

          if(start >= MEA_ANALYZER_MAX_CC_GROUP){
          CLI_print(" start group is out of range\n");
          return MEA_ERROR;
          }
    }

CLI_print("-----------------------------------------\n");
CLI_print("                 CC Group Event          \n");
CLI_print("-----------------------------------------\n");
   for(i=start;i<=end;i++){
        Event=0;
        if(MEA_API_Get_CC_Event_GroupId(MEA_UNIT_0,
                                        i, 
                                        &Event) !=MEA_OK){

        }
        for(j=0;j<32;j++){
            value=((Event>>j)& 0x00000001);
            if(j==0){
            CLI_print(" Group stream Event\n",i);
            CLI_print(" %5d %6d %s\n",i,(i*32)+j,MEA_STATUS_STR(value));
            }else{
            CLI_print("       %6d %s\n",(i*32)+j,MEA_STATUS_STR(value));
            }
      }
      CLI_print("-----------------------------------------\n");
   }
    return MEA_OK;



}
static MEA_Status MEA_CLI_CCM_Show_rdi_Event(int argc, char *argv[])
{
    MEA_CC_RDI_Event_group_dbt entry;
    MEA_Bool                   exist;
    MEA_Uint32                 i;
    
    if (argc < 2) {
       return MEA_ERROR;
    }
    if (MEA_API_Get_RDI_Event(MEA_UNIT_0, &entry ,&exist)!=MEA_OK){
        CLI_print(" MEA_API_Get_CC_Event failed\n");
    return MEA_OK;
    }
    if(exist){
        for(i=0;i<MEA_CC_RDI_MAX_GROUP;i++){
            CLI_print("RDI Group[%d]= %s\n",i,MEA_STATUS_STR(entry.Group[i]));
        }
    }
    
    if(!exist){
        CLI_print(" no exist event for RDI\n");
    }
    return MEA_OK;
}
static MEA_Status MEA_CLI_CCM_Show_rdi_group(int argc, char *argv[])
{

    MEA_Uint32                 i,j;
    MEA_Uint32                 Event;
    MEA_Uint32                 value;
    MEA_Uint32                 start,end;
    
    if (argc < 2) {
       return MEA_ERROR;
    }
    
    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        end=  MEA_ANALYZER_MAX_CC_GROUP; //(MEA_ANALYZER_MAX_STREAMS_TYPE2 / 32)  ;//(MEA_CC_RDI_MAX_GROUP);

        if(end != 0)
            end=end-1;

    } else{
          
          start=end=MEA_OS_atoi(argv[1]);
          if(start>=MEA_ANALYZER_MAX_CC_GROUP){
          CLI_print(" start group is out of range\n");
          return MEA_ERROR;
          }
    }

CLI_print("-----------------------------------------\n");
CLI_print("                 RDI Group Event         \n");
CLI_print("-----------------------------------------\n");
   for(i=start;i<=end;i++){
        Event=0;
        if(MEA_API_Get_RDI_Event_GroupId(MEA_UNIT_0,
                                        i, 
                                        &Event) !=MEA_OK){

        }
        for(j=0;j<32;j++){
            value=((Event>>j)& 0x00000001);
            if(j==0){
            CLI_print(" Group stream Event\n",i);
            CLI_print(" %5d %6d %s\n",i,(i*32)+j,MEA_STATUS_STR(value));
            }else{
            CLI_print("       %6d %s\n",(i*32)+j,MEA_STATUS_STR(value));
            }
      }
      CLI_print("-----------------------------------------\n");
   }
    return MEA_OK;




 
}

static MEA_Status MEA_CLI_Show_Counters_LM(int argc, char *argv[])
{


    int                      count,i;
	MEA_LmId_t               Id;
    MEA_LmId_t               first_Id;
    MEA_LmId_t               last_Id;
	MEA_Counters_LM_dbt entry,total_entry;
    MEA_Bool flag_current=MEA_FALSE;
    MEA_Bool exist;
    char buf[500];
    char *up_ptr;
                    
    
    
    if ((argc < 2) ) {

        CLI_print("Invalid number of parameters\n");
        return MEA_ERROR;
    }


    if (strcmp(argv[1], "all") != 0) {

        //first_pmId = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_Id = MEA_OS_atoi(buf);
            last_Id   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            first_Id = last_Id = MEA_OS_atoi(buf);
        }

    } else {


        first_Id = 0;
        last_Id  = MEA_ANALYZER_MAX_STREAMS_TYPE2-1;
    }

    for(i=2; i <argc ;i++){
        if(!strcmp(argv[i], "-d")) {
            flag_current = MEA_TRUE;
        }
      

    }







    

    MEA_OS_memset(&total_entry,0,sizeof(total_entry));
    CLI_print("\n");
    count=0;



    for(Id=first_Id;Id<=last_Id;Id++) {

        /* Check valid last_pmId */
        if (MEA_API_IsExist_CCM(MEA_UNIT_0,
                                 Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_PmId failed (Id=%d) \n",
                              __FUNCTION__,Id);
            return MEA_OK;
        }

        if (!exist ) {
           continue;
        }

        if (MEA_API_Get_Counters_LM(MEA_UNIT_0,
                                    Id,
                                    &entry) != MEA_OK) {
            CLI_print(
                              "%s - Get Counters LM for pmId %d failed\n",
                              __FUNCTION__,Id);
            return MEA_ERROR;  
        }

        if (count++==0) {
            if(flag_current==MEA_FALSE){
                CLI_print("LM          NearEnd               FarEnd     \n"
                          "Id          Loss                  Loss       \n"
                          "----- -------------------- -------------------- \n"
                          );
            }else{
                CLI_print("LM      Name                  \n"
                          "Id                          \n"
                          "----- ---------- ---------- \n"
                    );

            }
             
        }
        
        CLI_print("%5d ",Id);
if(flag_current==MEA_FALSE){
#ifdef MEA_COUNTER_64_SUPPORT  
        CLI_print("%20llu %20llu \n",
            entry.NearEndLoss.val,
            entry.FarEndLoss.val);
       
#else


        CLI_print("%10lu %10lu \n",
                  entry.NearEndLoss.s.lsw,
                  entry.FarEndLoss.s.lsw);
#endif



        MEA_OS_SUM_COUNTER(total_entry.NearEndLoss,entry.NearEndLoss);
        MEA_OS_SUM_COUNTER(total_entry.FarEndLoss,entry.FarEndLoss);
}else{
     
    CLI_print("%10s %10lu \n","TxFCfp",entry.TxFCfp);
    CLI_print("      %10s %10lu \n","RxFCbp",entry.RxFCbp);
    CLI_print("      %10s %10lu \n","TxFCbp",entry.TxFCbp);
    CLI_print("      %10s %10lu \n","RxFCfp",entry.RxFCfp);

}


        if (count > 0) {
          if(flag_current==MEA_FALSE){
          CLI_print("-------------------------- ---------------------\n");
          }else{
              CLI_print("--------------------------- \n");

          }
        }


    } /* for(serviceId=first_serviceId;serviceId<=last_serviceId;serviceId++) */


if((flag_current==MEA_FALSE) && (count > 0)){

    CLI_print("%5s ","Total");
#ifdef MEA_COUNTER_64_SUPPORT  

    CLI_print("%20llu %20llu \n",
              total_entry.NearEndLoss.val,
              total_entry.FarEndLoss.val);
    
#else
    CLI_print("%5s %-6s "," ","     L");
   CLI_print("%10lu %10lu \n",
             total_entry.NearEndLoss.s.lsw,
             total_entry.FarEndLoss.s.lsw);
#endif    
        
   if (count > 0) {
       if(flag_current==MEA_FALSE){
        CLI_print("-------------------------- ---------------------\n");
        }
   }
}

    CLI_print("Number Of Entries : %d\n\n",count);
   

    return MEA_OK;

}

static MEA_Status MEA_CLI_Collect_Counters_LM(int argc, char *argv[])
{

	if (argc != 2) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR;
    }

    if (strcmp(argv[1],"all") != 0) {
        MEA_Bool   exist;
        MEA_LmId_t Id = MEA_OS_atoi(argv[1]);


        /* Check valid first_pmId */
        if (MEA_API_IsExist_CCM(MEA_UNIT_0,
                                 Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_CCM failed (lmId=%d) \n",
                              __FUNCTION__,Id);
            return MEA_OK;
        }

        if (!exist) {
 
            CLI_print(
                              "%s - LmId %d not define\n",
                              __FUNCTION__,Id);
            return MEA_OK;  
        }
     
        /* collect the counters of this service */
    	if (MEA_API_Collect_Counters_LM(MEA_UNIT_0,Id,NULL) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Collect_Counters_LM failed for lmId %d \n",
                            __FUNCTION__,Id);
            return MEA_ERROR;  
        }
           
    } else {

        /* collect the counters for all PMs */
    	if (MEA_API_Collect_Counters_LMs(MEA_UNIT_0) != MEA_OK) {
            CLI_print(
                             "%s - Collect_Counters_PMs failed \n",
                            __FUNCTION__);
            return MEA_ERROR;  
        }
    }


	return MEA_OK;


}

static MEA_Status MEA_CLI_Clear_Counters_LM(int argc, char *argv[])
{

	if (argc != 2) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR;
    }

    if (strcmp(argv[1],"all") != 0) {

        MEA_Bool   exist;
        MEA_LmId_t Id = MEA_OS_atoi(argv[1]);


        /* Check valid first_pmId */
        if (MEA_API_IsExist_CCM(MEA_UNIT_0,
                                 Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
                              __FUNCTION__,Id);
            return MEA_OK;
        }

        if (!exist ) {
 
            CLI_print(
                              "%s - LmId %d not define\n",
                              __FUNCTION__,Id);
            return MEA_OK;  
        }
     

        /* clear the counters of this pmId */
    	if (MEA_API_Clear_Counters_LM(MEA_UNIT_0,Id) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Clear_Counters_LM failed for pmId %d \n",
                            __FUNCTION__,Id);
            return MEA_ERROR;  
        }
           
    } else {

        /* clear the counters for all PMs */
    	if (MEA_API_Clear_Counters_LMs(MEA_UNIT_0) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Clear_Counters_LMs failed \n",
                            __FUNCTION__);
            return MEA_ERROR;  
        }
    }


	return MEA_OK;

}




static MEA_Status MEA_CLI_Show_Counters_CCM(int argc, char *argv[])
{


    int                         count;
	MEA_CcmId_t                 Id;
    MEA_CcmId_t                 first_Id;
    MEA_CcmId_t                 last_Id;
	MEA_Counters_CCM_Defect_dbt entry,total_entry;
    MEA_Bool exist;
                    
    
    
    if ((argc != 2) && (argc != 3)) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR; 
    } 


    if (strcmp(argv[1],"all") != 0) {



        first_Id = MEA_OS_atoi(argv[1]);
     

        /* Check valid first_pmId */
        if (MEA_API_IsExist_CCM(MEA_UNIT_0,
                                 first_Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_CCM failed (first_pmId=%d) \n",
                              __FUNCTION__,first_Id);
            return MEA_OK;
        }

        if (!exist) {
 
            CLI_print(
                              "%s - first_Id %d not define \n",
                              __FUNCTION__,first_Id);
            return MEA_OK;  
        }

        if (argc <  3) {
            last_Id = first_Id;
        } else {
            last_Id = MEA_OS_atoi(argv[2]);


            /* Check valid last_pmId */
            if (MEA_API_IsExist_CCM(MEA_UNIT_0,
                                     last_Id,
                                     &exist) != MEA_OK) {
                CLI_print(
                                  "%s - MEA_API_IsExist_PmId failed (last_pmId=%d) \n",
                                  __FUNCTION__,last_Id);
                return MEA_OK;
            }

            if (!exist) {
 
                CLI_print(
                                  "%s - last_Id %d not define \n",
                                  __FUNCTION__,last_Id);
                return MEA_OK;  
            }

        }

    } else { 

        if (argc != 2) {
           CLI_print("Invalid number of parameters\n");
           return MEA_ERROR; 
        }

        first_Id = 0;
        last_Id  = MEA_ANALYZER_MAX_STREAMS_TYPE2-1;
    }

    MEA_OS_memset(&total_entry,0,sizeof(total_entry));
    CLI_print("\n");
    count=0;



    for(Id=first_Id;Id<=last_Id;Id++) {

        /* Check valid last_pmId */
        if (MEA_API_IsExist_CCM(MEA_UNIT_0,
                                 Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_PmId failed (Id=%d) \n",
                              __FUNCTION__,Id);
            return MEA_OK;
        }

        if (!exist ) {
           continue;
        }

        if (MEA_API_Get_Counters_CCM(MEA_UNIT_0,
                                    Id,
                                    &entry) != MEA_OK) {
            CLI_print(
                              "%s - Get Counters LM for pmId %d failed\n",
                              __FUNCTION__,Id);
            return MEA_ERROR;  
        }

        if (count++==0) {
#if MEA_CCM_MEG_ID_SUPPORT            
            CLI_print("LM    Unexpected           Unexpected           Unexpected                                 LastSequenc        \n"
                      "Id    MEG_ID               MEP_ID               period               reorder                          \n"
                      "----- -------------------- -------------------- -------------------- -------------------- ----------- \n"
                );
#else
            CLI_print("LM    Unexpected                                 \n"
                      "Id    period               reorder               LastSequenc \n"
                      "----- -------------------- -------------------- -----------\n" );



#endif
        }
        
        CLI_print("%5d ",Id);






#ifdef MEA_COUNTER_64_SUPPORT  
       
       
#if MEA_CCM_MEG_ID_SUPPORT		
        CLI_print("%20llu %20llu %20llu %20llu %11d\n",

            entry.Unexpected_MEG_ID.val,
            entry.Unexpected_MEP_ID.val,
            entry.Unexpected_period.val,
            entry.reorder.val, entry.LastSequenc);
#else
       CLI_print(" %20llu %20llu 11%d\n",

            

            entry.Unexpected_period.val,
            entry.reorder.val,
            entry.LastSequenc);
#endif     
       
#else
       CLI_print("%-5s %-6s ","","     L");


#if MEA_CCM_MEG_ID_SUPPORT
        CLI_print("%10lu %10lu %10lu %10lu \n",
            entry.Unexpected_MEG_ID.s.lsw,
            entry.Unexpected_MEP_ID.s.lsw,
            entry.Unexpected_period.s.lsw,
            entry.reorder.s.lsw);
#else
         CLI_print("%10lu %10lu %10lu \n",
            entry.Unexpected_MEP_ID.s.lsw,
            entry.Unexpected_period.s.lsw,
            entry.reorder.s.lsw);

#endif			
#endif
        MEA_OS_SUM_COUNTER(total_entry.Unexpected_MEG_ID,entry.Unexpected_MEG_ID);
        MEA_OS_SUM_COUNTER(total_entry.Unexpected_MEP_ID,entry.Unexpected_MEP_ID);
        MEA_OS_SUM_COUNTER(total_entry.Unexpected_period,entry.Unexpected_period);
        MEA_OS_SUM_COUNTER(total_entry.reorder,entry.reorder);



        if (count > 0) {
#if MEA_CCM_MEG_ID_SUPPORT
            CLI_print("----- -------------------- -------------------- -------------------- -------------------- ----------- \n");
#else
          CLI_print("--------------------------------------------------------\n");
#endif
          
        }


    } /* for(serviceId=first_serviceId;serviceId<=last_serviceId;serviceId++) */


    if(count){

    CLI_print("%5s ","Total");
#ifdef MEA_COUNTER_64_SUPPORT  

#if MEA_CCM_MEG_ID_SUPPORT
    CLI_print("%20llu %20llu %20llu %20llu\n",
              total_entry.Unexpected_MEG_ID.val,
              total_entry.Unexpected_MEP_ID.val,
              total_entry.Unexpected_period.val,
              total_entry.reorder.val);
#else
    CLI_print("%20llu %20llu\n",
        total_entry.Unexpected_period.val,
        total_entry.reorder.val);


#endif



    
#else
    

#if MEA_CCM_MEG_ID_SUPPORT
   CLI_print("%10lu %10lu %10lu %10lu\n",
             total_entry.Unexpected_MEG_ID.s.lsw,
              total_entry.Unexpected_MEP_ID.s.lsw,
              total_entry.Unexpected_period.s.lsw,
              total_entry.reorder.s.lsw);
#else
    CLI_print("%10lu %10lu %10lu\n",
        total_entry.Unexpected_MEP_ID.s.lsw,
        total_entry.Unexpected_period.s.lsw,
        total_entry.reorder.s.lsw);

#endif
#endif
    }        
    if (count > 0) {
#if MEA_CCM_MEG_ID_SUPPORT
        CLI_print("----- -------------------- -------------------- -------------------- --------------------\n");
#else
        CLI_print("--------------------------------------------------------\n");
#endif    
    }


    CLI_print("Number Of Entries : %d\n\n",count);
   

    return MEA_OK;

}

static MEA_Status MEA_CLI_Collect_Counters_CCM(int argc, char *argv[])
{

	if (argc != 2) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR;
    }

    if (strcmp(argv[1],"all") != 0) {
        MEA_Bool   exist;
        MEA_LmId_t Id = MEA_OS_atoi(argv[1]);


        /* Check valid first_pmId */
        if (MEA_API_IsExist_CCM(MEA_UNIT_0,
                                 Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_CCM failed (Id=%d) \n",
                              __FUNCTION__,Id);
            return MEA_OK;
        }

        if (!exist) {
 
            CLI_print(
                              "%s - Id %d not define\n",
                              __FUNCTION__,Id);
            return MEA_OK;  
        }
     
        /* collect the counters of this service */
    	if (MEA_API_Collect_Counters_CCM(MEA_UNIT_0,Id,NULL) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Collect_Counters_LM failed for Id %d \n",
                            __FUNCTION__,Id);
            return MEA_ERROR;  
        }
           
    } else {

        /* collect the counters for all PMs */
    	if (MEA_API_Collect_Counters_CCMs(MEA_UNIT_0) != MEA_OK) {
            CLI_print(
                             "%s - Collect_Counters_PMs failed \n",
                            __FUNCTION__);
            return MEA_ERROR;  
        }
    }


	return MEA_OK;


}

static MEA_Status MEA_CLI_Clear_Counters_CCM(int argc, char *argv[])
{

	if (argc != 2) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR;
    }

    if (strcmp(argv[1],"all") != 0) {

        MEA_Bool   exist;
        MEA_LmId_t Id = MEA_OS_atoi(argv[1]);


        /* Check valid first_pmId */
        if (MEA_API_IsExist_CCM(MEA_UNIT_0,
                                 Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_PmId failed (pmId=%d) \n",
                              __FUNCTION__,Id);
            return MEA_OK;
        }

        if (!exist ) {
 
            CLI_print(
                              "%s - LmId %d not define\n",
                              __FUNCTION__,Id);
            return MEA_OK;  
        }
     

        /* clear the counters of this pmId */
    	if (MEA_API_Clear_Counters_CCM(MEA_UNIT_0,Id) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Clear_Counters_LM failed for pmId %d \n",
                            __FUNCTION__,Id);
            return MEA_ERROR;  
        }
           
    } else {

        /* clear the counters for all PMs */
    	if (MEA_API_Clear_Counters_CCMs(MEA_UNIT_0) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Clear_Counters_LMs failed \n",
                            __FUNCTION__);
            return MEA_ERROR;  
        }
    }


	return MEA_OK;

}










#endif //MEA_CCM_ENABLE

