/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)

#include <sys/types.h>
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>  
//#include <linux/in.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/ioctl.h>
#include <errno.h>

#endif /* MEA_OS_LINUX */


#include "mea_api.h"
#include "mea_deviceInfo_drv.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_gw.h"



static const char* MEA_CLI_GW_Report_Commands[] =
{
    "top",
    "#  ***** GW  Start *****",
    "MEA GW rootfilter show all",
    "MEA GW global show all",
    "MEA GW TE_ID mode get all",
    
    "MEA GW VPLSi show all",
    "MEA GW VPLSi leaf show all all",
    
    "MEA GW event show all",
    
    "MEA GW TFT_UE show entry all all",

    "MEA GW TFT_UE show edit all all -h",
    "MEA GW TFT_UE show edit all all -hETHCS_DL",
    "MEA GW TFT_UE show edit all all -hIPCS_DL",

   "MEA GW TE_ID mode show all",
   "MEA GW HPM prof show all all",
   "MEA GW HPM mask_prof show all",

    


    "#  ***** GW  End *****"
    

};





static MEA_Status MEA_CLI_GW_Global_Set(int argc, char *argv[])
{
    MEA_Gw_Global_dbt  Gw_Global_entry;
    int i;
    
    MEA_Uint32 index;
    MEA_Uint16 vlanId;
    MEA_Uint32 Ip_val;
    MEA_Uint8 value;
    MEA_Uint64 val64;
    


    if (argc < 2) {
        return MEA_ERROR;
    }


    MEA_OS_memset(&Gw_Global_entry, 0, sizeof(Gw_Global_entry));

    if (MEA_API_Get_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK) {
        CLI_print("Error the MEA_API_Get_GW_Global_Entry failed \n");
        return MEA_OK;
    }


    for (i = 0; i < argc; i++){

        if (!MEA_OS_strcmp(argv[i], "-Host_MAC")){

            MEA_OS_atohex_MAC(argv[i + 1], &Gw_Global_entry.My_Host_MAC);

        }
        if (!MEA_OS_strcmp(argv[i], "-PGW_MAC")) {

            MEA_OS_atohex_MAC(argv[i + 1], &Gw_Global_entry.My_PGW_MAC);

        }


        if (!MEA_OS_strcmp(argv[i], "-APN_vlan")){
            index = (MEA_Uint16)MEA_OS_atoiNum(argv[i + 1]);
            vlanId = (MEA_Uint16)MEA_OS_atoiNum(argv[i + 2]);
            if (index >= MEA_GW_MAX_OF_APN_VLAN){
                CLI_print("Error %s index %d out of range \n", argv[i], index);
                return MEA_OK;
            }

            Gw_Global_entry.APN_vlanId[index] = vlanId;
        }
        if (!MEA_OS_strcmp(argv[i], "-APN_Ipv6_mask")) {
            index = (MEA_Uint16)MEA_OS_atoiNum(argv[i + 1]);
            value = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 2]);
            if (index >= MEA_GW_MAX_OF_APN_VLAN) {
                CLI_print("Error %s index %d out of range \n", argv[i], index);
                return MEA_OK;
            }

            Gw_Global_entry.APN_Ipv6_mask_Type[index] = value;
        }
        if (!MEA_OS_strcmp(argv[i], "-APN_Ipv6")) {
            unsigned char buf[sizeof(struct in6_addr)];
            int  s;

            index = (MEA_Uint16)MEA_OS_atoiNum(argv[i + 1]);

           
            
            if (index >= MEA_GW_MAX_OF_APN_VLAN) {
                CLI_print("Error %s index %d out of range \n", argv[i], index);
                return MEA_OK;
            }
            s = inet_pton(AF_INET6, argv[i + 2], buf);
            if (s <= 0) {
                CLI_print("ipv6 format error\n");
                return MEA_ERROR;
            }

            
            val64.s.lsw = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
            val64.s.msw = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;
  
            Gw_Global_entry.APN_IpV6[index].val = val64.val;
        }

        if (!MEA_OS_strcmp(argv[i], "-DHCP_IP")){
            index = MEA_OS_atoiNum(argv[i + 1]);
            Ip_val = MEA_OS_inet_addr(argv[i + 2]);
            if (index >= MEA_GW_MAX_OF_DHCP){
                CLI_print("Error %s index %d out of range \n", argv[i], index);
                return MEA_OK;
            }
            Gw_Global_entry.DHCP_IP[index] = Ip_val;

        }
        if (!MEA_OS_strcmp(argv[i], "-Infra_VLAN")){
            Gw_Global_entry.Infra_VLAN = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-Infra_VLAN_Ipv6_mask")) {
            Gw_Global_entry.Infra_VLAN_Ipv6mask = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "-PDN_IP")) {
            index = MEA_OS_atoiNum(argv[i + 1]);
            Ip_val = MEA_OS_inet_addr(argv[i + 2]);
            if (index >= MEA_GW_MAX_OF_PDN_IPV4) {
                CLI_print("Error %s index %d out of range \n", argv[i], index);
                return MEA_OK;
            }
            Gw_Global_entry.PDN_Ipv4[index] = Ip_val;
        }

        if (!MEA_OS_strcmp(argv[i], "-SGW_IP")){
            index = MEA_OS_atoiNum(argv[i + 1]);
            Ip_val = MEA_OS_inet_addr(argv[i + 2]);
            if (index >= MEA_GW_MAX_OF_SGW_IP){
                CLI_print("Error %s index %d out of range \n", argv[i], index);
                return MEA_OK;
            }
            Gw_Global_entry.S_GW_IP[index] = Ip_val;
        }
        if (!MEA_OS_strcmp(argv[i], "-PGW_U_IP")){

            Ip_val = MEA_OS_inet_addr(argv[i + 1]);

            Gw_Global_entry.P_GW_U_IP = Ip_val;
        }
        if (!MEA_OS_strcmp(argv[i], "-MNG_vlanId") || 
            !MEA_OS_strcmp(argv[i], "-MNG_VLAN")){
            Gw_Global_entry.MNG_vlanId = MEA_OS_atoi(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-Conf_D_IP")){
            Ip_val = MEA_OS_inet_addr(argv[i + 1]);

            Gw_Global_entry.Conf_D_IP = Ip_val;
        }
        if (!MEA_OS_strcmp(argv[i], "-local_mng_ip")){
            Ip_val = MEA_OS_inet_addr(argv[i + 1]);

            Gw_Global_entry.local_mng_ip = Ip_val;
        }
        if (!MEA_OS_strcmp(argv[i], "-MME_IP")){
            Ip_val = MEA_OS_inet_addr(argv[i + 1]);

            Gw_Global_entry.MME_IP = Ip_val;
        }
        if (!MEA_OS_strcmp(argv[i], "-default_sid")){
            Gw_Global_entry.default_sid_info.action = MEA_OS_atoi(argv[i + 1]);
            Gw_Global_entry.default_sid_info.def_sid = MEA_OS_atoi(argv[i + 2]);
        }
        if (!MEA_OS_strcmp(argv[i], "-default_sid_ETHCS_UL")) {
            Gw_Global_entry.ETHCS_UL_default_sid_info.action = MEA_OS_atoi(argv[i + 1]);
            Gw_Global_entry.ETHCS_UL_default_sid_info.def_sid = MEA_OS_atoi(argv[i + 2]);
        }

        if (!MEA_OS_strcmp(argv[i], "-default_sid_IPCS")) {

            Gw_Global_entry.IPCS_default_sid_info.action   = MEA_OS_atoi(argv[i + 1]);
            Gw_Global_entry.IPCS_default_sid_info.def_sid  = MEA_OS_atoi(argv[i + 2]);
            Gw_Global_entry.IPCS_default_sid_srcport       = MEA_OS_atoi(argv[i + 3]);


        }

       


        if (!MEA_OS_strcmp(argv[i], "-DSCP")){
            Gw_Global_entry.InternalDscp = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
        }
        


        if (!MEA_OS_strcmp(argv[i], "-gw_ttl")){
            Gw_Global_entry.edit_IP_TTL = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "-learn_enb_vpn")){
            Gw_Global_entry.learn_enb_vpn = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "-vxlan_L4_dst_port")) {
            Gw_Global_entry.vxlan_L4_dst_port = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-vxlan_upto_vlan")) {
            Gw_Global_entry.vxlan_upto_vlan = MEA_OS_atoiNum(argv[i + 1]);
        }
        


    }


    if (MEA_API_Set_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK){
        CLI_print("Error MMEA_API_Set_GW_Global_Entry failed \n");
        return MEA_OK;
    }


    CLI_print("Done\n");
    return MEA_OK;
}

static MEA_Status MEA_CLI_GW_Show_Enent(int argc, char *argv[])
{
    MEA_Uint32               srcIp;
    MEA_Uint32               dstIp;
    char                     srcIp_str[20];
    char                     dstIp_str[20];
    
    MEA_GW_evetEntry_dbt   EventEntry;



    if(MEA_API_GW_Get_EVENT(MEA_UNIT_0, &EventEntry) != MEA_OK){
        CLI_print("Error MEA_API_GW_Get_EVENT failed \n");
        return MEA_OK;
    }
    CLI_print("--------------------------------------\n");
    CLI_print("              GW_EVENT                 \n");
    CLI_print("--------------------------------------\n");

    
    
    CLI_print("%-40s : %d \n", "filterId", EventEntry.root_filterId);
    CLI_print("%-40s : %d \n", "GTPv1_flag", EventEntry.GTPv1_flag);
    CLI_print("%-40s : %02x:%02x:%02x:%02x:%02x:%02x \n",
        "DA Internal",
        (unsigned char)(EventEntry.DA.b[0]),
        (unsigned char)(EventEntry.DA.b[1]),
        (unsigned char)(EventEntry.DA.b[2]),
        (unsigned char)(EventEntry.DA.b[3]),
        (unsigned char)(EventEntry.DA.b[4]),
        (unsigned char)(EventEntry.DA.b[5]));

        
        CLI_print("%-40s : %d \n", "GTP_message_type", EventEntry.GTP_message_type);
        CLI_print("%-40s : %d \n", "ip_protocol int", EventEntry.ip_protocol_int);
        CLI_print("%-40s : %02x:%02x:%02x:%02x:%02x:%02x \n",
            "SA Internal",
            (unsigned char)(EventEntry.SA.b[0]),
            (unsigned char)(EventEntry.SA.b[1]),
            (unsigned char)(EventEntry.SA.b[2]),
            (unsigned char)(EventEntry.SA.b[3]),
            (unsigned char)(EventEntry.SA.b[4]),
            (unsigned char)(EventEntry.SA.b[5]));
        CLI_print("%-40s : %d \n", "sgw_teid", EventEntry.sgw_teid);

        srcIp = EventEntry.src_ipv4_int;
        MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), srcIp_str, sizeof(srcIp_str));
        CLI_print("%-40s : %s \n", "SrcIPv4 Internal", srcIp_str);
        dstIp = EventEntry.dst_ipv4_int;
        MEA_OS_inet_ntoa(MEA_OS_htonl(dstIp), dstIp_str, sizeof(dstIp_str));
        CLI_print("%-40s : %s \n", "DstIpv4 Internal", dstIp_str);
        CLI_print("%-40s : 0x%x \n", "ethertype Internal", EventEntry.ethertype_int);
        CLI_print("%-40s : %d \n", "vlan Internal", EventEntry.vlan_int);
        CLI_print("------------------------------------------------------\n");
        CLI_print("            Event Access port \n");
        CLI_print("------------------------------------------------------\n");
        switch (EventEntry.event_vpls_access.info){
        case MEA_VPLS_EVENT_ACCESS_TYPE_FWD_NORMAL:
        case  MEA_VPLS_EVENT_ACCESS_TYPE_FWD_DISCARD_FROM_LRN:
        case  MEA_VPLS_EVENT_ACCESS_TYPE_OVERRULE :
        case MEA_VPLS_EVENT_ACCESS_TYPE_FWD_DISCARD_FROM_CNTX:
        case MEA_VPLS_EVENT_ACCESS_TYPE_DISCARD_FROM_FWD_AND_ST_CNTX_LOCAL_SW:
            CLI_print("%-40s : %d \n", "st_cntx_ac_port      ", EventEntry.event_vpls_access.st_cntx_ac_port);
            CLI_print("%-40s : %d \n", "lrn_ac_port          ", EventEntry.event_vpls_access.lrn_ac_port);
            CLI_print("%-40s : %d \n", "fwd_ac_port          ", EventEntry.event_vpls_access.fwd_ac_port);
            CLI_print("%-40s : %d \n", "info                 ", EventEntry.event_vpls_access.info);
        break;
        case  MEA_VPLS_EVENT_ACCESS_TYPE_FWD_HANDHOVER_IN_0:
        case MEA_VPLS_EVENT_ACCESS_TYPE_FWD_HANDHOVER_IN_1:

            CLI_print("%-40s : %d \n", "handover access port ", EventEntry.event_vpls_access.st_cntx_ac_port);
            CLI_print("%-40s : %d \n", "destination cluster  ", EventEntry.event_vpls_access.lrn_ac_port);
            CLI_print("%-40s : %d \n", "fwd_ac_port          ", EventEntry.event_vpls_access.fwd_ac_port);
            CLI_print("%-40s : %d \n", "info                 ", EventEntry.event_vpls_access.info);

            break;

        }
		CLI_print("%-40s : %d \n", "rule_info          ", EventEntry.event_vpls_access.info);
        switch (EventEntry.event_vpls_access.info){
           
        case MEA_VPLS_EVENT_ACCESS_TYPE_FWD_NORMAL:
            CLI_print("%        -40s \n", "packet FWD_NORMAL" );
            break;
        case  MEA_VPLS_EVENT_ACCESS_TYPE_FWD_DISCARD_FROM_LRN :
            CLI_print("%        -40s \n", "discard due to src_access_port(lrn) == dst_access_port(fwd)  ");
            break;
        case MEA_VPLS_EVENT_ACCESS_TYPE_OVERRULE:
            CLI_print("%        -40s \n", "overrule xpermission - send the packet to cluster #62");
            break;
        case MEA_VPLS_EVENT_ACCESS_TYPE_FWD_DISCARD_FROM_CNTX:
            CLI_print("%        -40s \n", "fwd_access_port_discard due to src_access_port(lrn) == dst_access_port(st_cntx)");
            break;
        case MEA_VPLS_EVENT_ACCESS_TYPE_DISCARD_FROM_FWD_AND_ST_CNTX_LOCAL_SW:
            CLI_print("%        -40s \n", "discard due to(dst_access_port(from the fwd) != 0) & (dst_access_port(from the st_cntx) != 0) & (local_sw_en = 0)");
            break;
        case  MEA_VPLS_EVENT_ACCESS_TYPE_FWD_HANDHOVER_IN_0:
        case MEA_VPLS_EVENT_ACCESS_TYPE_FWD_HANDHOVER_IN_1:
            CLI_print("%        -40s %d\n", "HANDHOVER from ", (EventEntry.event_vpls_access.info == MEA_VPLS_EVENT_ACCESS_TYPE_FWD_HANDHOVER_IN_0) ? 0 : 1);

            break;


        default:
            CLI_print("%        -40s \n", "Not Define ");
            break;

        
        }


        CLI_print("------------------------------------------------------\n");
            



    return MEA_OK;
}

static MEA_Status MEA_CLI_GW_Global_Set_TE_ID_Mode(int argc, char *argv[])
{
    
   
    MEA_Characteristics_entry_dbt      entry;
    MEA_Status                         retVal=MEA_ERROR;
    
    int i;
    MEA_Uint32     TEId;
    MEA_Bool      valid = MEA_FALSE;

    if (argc < 3) {
        return MEA_ERROR;
    }


    
    MEA_OS_memset(&entry, 0, sizeof(entry));

   
    TEId = MEA_OS_atoi(argv[1]);
    valid = MEA_OS_atoi(argv[2]);
    retVal=MEA_API_Get_ClassifierKEY_Character_Entry(MEA_UNIT_0, TEId, &entry);
    if (retVal == MEA_ERROR) {
        CLI_print("Error: MEA_API_Get_ClassifierKEY_Character_Entry for TeiD %d\n", TEId);
        return MEA_ERROR;

    }
    entry.valid = valid;
    for (i = 2; i < argc; i++) {
       
        if (!MEA_OS_strcmp(argv[i], "-type")) {
            entry.type = MEA_OS_atoi(argv[i+1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-learn")) {

            entry.learn_en = MEA_OS_atoi(argv[i+1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-maskIpv6")) {

            entry.ipv6_mask_type= (MEA_OS_atoi(argv[i + 1]) & 0x07 );
        }


    }

    
    retVal=MEA_API_Set_ClassifierKEY_Character_Entry(MEA_UNIT_0, TEId, &entry);

    if (retVal == MEA_ERROR){
        CLI_print("Error: MEA_API_Set_ClassifierKEY_Character_Entry for TeiD %d\n", TEId);
        return MEA_ERROR;
    
    }


        return MEA_OK;

}



static MEA_Status MEA_CLI_GW_Global_Get_TE_ID_Mode(int argc, char *argv[])
{

  
    MEA_Uint32 from_index, to_index;
    MEA_Characteristics_entry_dbt      entry;
    MEA_Status                         retVal = MEA_ERROR;
    MEA_Uint32     TEId;


    char info_mask_Type[MEA_TYPE_TEID_IPV6_MASK_LAST + 1][20];

    MEA_OS_strcpy(&(info_mask_Type[MEA_TYPE_TEID_IPV6_MASK_64][0]), "IPV6_MASK_64");
    MEA_OS_strcpy(&(info_mask_Type[MEA_TYPE_TEID_IPV6_MASK_63][0]), "IPV6_MASK_63");
    MEA_OS_strcpy(&(info_mask_Type[MEA_TYPE_TEID_IPV6_MASK_62][0]), "IPV6_MASK_62");
    MEA_OS_strcpy(&(info_mask_Type[MEA_TYPE_TEID_IPV6_MASK_61][0]), "IPV6_MASK_61");
    MEA_OS_strcpy(&(info_mask_Type[MEA_TYPE_TEID_IPV6_MASK_60][0]), "IPV6_MASK_60");
    MEA_OS_strcpy(&(info_mask_Type[MEA_TYPE_TEID_IPV6_MASK_59][0]), "IPV6_MASK_59");


    if (argc < 2) {
        return MEA_ERROR;
    }

    if (!MEA_VPLS_CHACTRISTIC_SUPPORT)
        return MEA_OK;
  

    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        from_index = 0;
        to_index = MEA_GW_Characteristics_MAX_PROF-1;
    }
    else {
        from_index = to_index = MEA_OS_atoi(argv[1]);
        
    }

   
    CLI_print(" GW  TE_ID Mode  \n");
    CLI_print("-------------------------------------------------------------\n");
    for (TEId = from_index; TEId <= to_index; TEId++)
    {
        MEA_OS_memset(&entry, 0, sizeof(entry));

        retVal = MEA_API_Get_ClassifierKEY_Character_Entry(MEA_UNIT_0, TEId, &entry);
        if (retVal == MEA_ERROR){
            continue;
        }
        if(entry.valid == MEA_FALSE)
            continue;

        CLI_print(" TEId= %5d learn=%3s %s \r\n", TEId, MEA_STATUS_STR(entry.learn_en), info_mask_Type[entry.ipv6_mask_type]);

    
    }
    CLI_print("-------------------------------------------------------------\n");
    

    return MEA_OK;

}

static MEA_Status MEA_CLI_GW_Global_Get(int argc, char *argv[])
{
    MEA_Gw_Global_dbt  Gw_Global_entry;
    int i;
    unsigned char buf[sizeof(struct in6_addr)];
    //int  s;
    char str[INET6_ADDRSTRLEN];
    
   
    MEA_Uint32 Ip_val;
    char                     Ip_str[20];
   
    if (argc < 2) {
    return MEA_ERROR;
    }


    MEA_OS_memset(&Gw_Global_entry, 0, sizeof(Gw_Global_entry));

    if (MEA_API_Get_GW_Global_Entry(MEA_UNIT_0, &Gw_Global_entry) != MEA_OK) {
        CLI_print("Error the MEA_API_Get_GW_Global_Entry failed \n");
        return MEA_OK;
    }

    CLI_print(" GW Global Setting \n");
    CLI_print("-------------------------------------------------------------\n");
    
    CLI_print(" %20s    : %02x:%02x:%02x:%02x:%02x:%02x\n"," Host_MAC",
        (MEA_Uint8)(Gw_Global_entry.My_Host_MAC.b[0]),
        (MEA_Uint8)(Gw_Global_entry.My_Host_MAC.b[1]),
        (MEA_Uint8)(Gw_Global_entry.My_Host_MAC.b[2]),
        (MEA_Uint8)(Gw_Global_entry.My_Host_MAC.b[3]),
        (MEA_Uint8)(Gw_Global_entry.My_Host_MAC.b[4]),
        (MEA_Uint8)(Gw_Global_entry.My_Host_MAC.b[5]));
    CLI_print(" %20s    : %02x:%02x:%02x:%02x:%02x:%02x\n", " PGW_MAC",
        (MEA_Uint8)(Gw_Global_entry.My_PGW_MAC.b[0]),
        (MEA_Uint8)(Gw_Global_entry.My_PGW_MAC.b[1]),
        (MEA_Uint8)(Gw_Global_entry.My_PGW_MAC.b[2]),
        (MEA_Uint8)(Gw_Global_entry.My_PGW_MAC.b[3]),
        (MEA_Uint8)(Gw_Global_entry.My_PGW_MAC.b[4]),
        (MEA_Uint8)(Gw_Global_entry.My_PGW_MAC.b[5]));
    CLI_print("-------------------------------------------------------------\n");

    for (i = 0; i < MEA_GW_MAX_OF_APN_VLAN;i++)
    {
        CLI_print("index %d", i);
        mea_drv_show_event_value(1, "APN_vlanId", Gw_Global_entry.APN_vlanId[i]);
        mea_drv_show_event_value(2, "APN_Ipv6_mask", Gw_Global_entry.APN_Ipv6_mask_Type[i]);
        MEA_OS_memset(&buf, 0, sizeof(buf));
        buf[0] = (Gw_Global_entry.APN_IpV6[i].s.msw >> 24) & 0xff;
        buf[1] = (Gw_Global_entry.APN_IpV6[i].s.msw >> 16) & 0xff;
        buf[2] = (Gw_Global_entry.APN_IpV6[i].s.msw >> 8) & 0xff;
        buf[3] = (Gw_Global_entry.APN_IpV6[i].s.msw >> 0) & 0xff;

        buf[4] = (Gw_Global_entry.APN_IpV6[i].s.lsw >> 24) & 0xff;
        buf[5] = (Gw_Global_entry.APN_IpV6[i].s.lsw >> 16) & 0xff;
        buf[6] = (Gw_Global_entry.APN_IpV6[i].s.lsw >> 8) & 0xff;
        buf[7] = (Gw_Global_entry.APN_IpV6[i].s.lsw >> 0) & 0xff;
        if (inet_ntop(AF_INET6, buf, str, INET6_ADDRSTRLEN) == NULL) {
            CLI_print(">>>inet_ntop ERROR<<< \n");
        }
        CLI_print("APN_IPv6 %s  ", str);
        
    }

    CLI_print("-------------------------------------------------------------\n");
    
    for (i = 0; i < MEA_GW_MAX_OF_DHCP; i++)

    {
        Ip_val = Gw_Global_entry.DHCP_IP[i];
        MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
        CLI_print(" %20s[%d]    : %s \n", "DHCP_IP", i, Ip_str);
    }
    CLI_print("-------------------------------------------------------------\n");
    for (i = 0; i < MEA_GW_MAX_OF_SGW_IP; i++)

    {
        Ip_val = Gw_Global_entry.S_GW_IP[i];
        MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
        CLI_print(" %20s[%d]    : %s \n", "S_GW_IP", i, Ip_str);
    }
	for (i = 0; i < MEA_GW_MAX_OF_PDN_IPV4; i++)

    {
        Ip_val = Gw_Global_entry.PDN_Ipv4[i];
        MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
        CLI_print(" %20s[%d]    : %s \n", "PDN_Ipv4", i, Ip_str);
    }
    CLI_print("-------------------------------------------------------------\n");
    CLI_print(" %20s    : 0x%x \n", "ETHtype_Infra_VLAN", (Gw_Global_entry.Infra_VLAN>>16) & 0xffff);
    CLI_print(" %20s    : %d \n", "Infra_VLAN", Gw_Global_entry.Infra_VLAN & 0xfff);
    CLI_print(" %20s    : 0x%x \n", "Infra_VLAN_Ipv6mask", Gw_Global_entry.Infra_VLAN_Ipv6mask);

    CLI_print(" %20s    : %d \n", "MNG_vlanId", Gw_Global_entry.MNG_vlanId);
    CLI_print("-------------------------------------------------------------\n");
    {
        Ip_val = Gw_Global_entry.P_GW_U_IP;
        MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
        CLI_print(" %20s    : %s \n", "P_GW_U_IP", Ip_str);
    }
    CLI_print("-------------------------------------------------------------\n");
    {
        Ip_val = Gw_Global_entry.Conf_D_IP;
        MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
        CLI_print(" %20s    : %s \n", "Conf_D_IP", Ip_str);
    }
    CLI_print("-------------------------------------------------------------\n");
    {
        Ip_val = Gw_Global_entry.MME_IP;
        MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
        CLI_print(" %20s    : %s \n", "MME_IP", Ip_str);
    }
    CLI_print("-------------------------------------------------------------\n");


    CLI_print(" %20s    : %d \n", "TTL", Gw_Global_entry.edit_IP_TTL);
    CLI_print(" %20s    : %d \n", "Dscp Internal", Gw_Global_entry.InternalDscp);
    CLI_print("-------------------------------------------------------------\n");
    Ip_val = Gw_Global_entry.local_mng_ip;
    MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
    CLI_print(" %20s    : %s \n", "local_mng_ip", Ip_str);
    CLI_print("-------------------------------------------------------------\n");
   
    CLI_print(" %20s %d:   :Action %d Sid %d   \n", "default Service for TeId's", i,
        Gw_Global_entry.default_sid_info.action,
        Gw_Global_entry.default_sid_info.def_sid);

    CLI_print(" %20s %d:   :Action %d Sid %d   \n", "default Service ETHCS_UL", i,
        Gw_Global_entry.ETHCS_UL_default_sid_info.action,
        Gw_Global_entry.ETHCS_UL_default_sid_info.def_sid);
                            
    
   
    CLI_print("-------------------------------------------------------------\n");
    CLI_print(" %20s    : %d \n", "learn_enb_vpn", Gw_Global_entry.learn_enb_vpn);
    CLI_print("-------------------------------------------------------------\n");
    
 

return MEA_OK;


}

static MEA_Status MEA_CLI_GW_Set_RootFilter(int argc, char *argv[])
{
    MEA_Uint32 index;
    MEA_Bool   enable;
    if (argc < 2) {
        return MEA_ERROR;
    }

    index= MEA_OS_atoi(argv[1]);
    enable = MEA_OS_atoi(argv[2]);

    if (MEA_API_Set_RootFilter_Entry(MEA_UNIT_0, index, enable) != MEA_OK){
        CLI_print("Error MEA_API_Set_RootFilter_Entry failed \n");
        return MEA_OK;
    }
    
    
    CLI_print("Done :\n");
    return MEA_OK;
}

static MEA_Status MEA_CLI_GW_Get_RootFilter(int argc, char *argv[])
{
    MEA_Uint32 index;
    MEA_Bool   enable;
    if (argc < 2) {
        return MEA_ERROR;
    }

    
    CLI_print(" Root classification \n");
    CLI_print("---------------------------\n");
    CLI_print(" index  enable             \n");
    CLI_print("---------------------------\n");

    for (index = 0; index < MEA_ROOT_FILTER_TYPE_LAST; index++)
    {
        if (MEA_API_Get_RootFilter_Entry(MEA_UNIT_0, index, &enable) != MEA_OK){
            CLI_print("Error MEA_API_Get_RootFilter_Entry failed \n");
            return MEA_OK;
        }

        CLI_print(" %5d  %5s\n", index, MEA_STATUS_STR(enable));

    }
    CLI_print("---------------------------\n");
    

    CLI_print("Done :\n");
    return MEA_OK;
}








/************************************************************************/
/*                                                                      */
/************************************************************************/

static MEA_Status MEA_CLI_GW_VPLSi_Create_Entry(int argc, char *argv[])

{
    MEA_VPLS_dbt entry;
    MEA_Uint16 vpls_Ins_Id = MEA_PLAT_GENERATE_NEW_ID;
    

    int i;

    if (argc < 2) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&entry, 0, sizeof(MEA_VPLS_dbt));

    for (i = 1; i < argc; i++) {

        if (!MEA_OS_strcmp(argv[i], "-f")){
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            vpls_Ins_Id = MEA_OS_atoi(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-max_leaf")){
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.max_of_fooding = MEA_OS_atoi(argv[i + 1]);
        }

        
        if (!MEA_OS_strcmp(argv[i], "-vpn")){
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.vpn = MEA_OS_atoi(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "-SM_X")){
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.SM_mode = (mea_Character_TEID_mode_t) MEA_OS_atoi(argv[i + 1]);
        }

    }



    if (MEA_API_VPLSi_Create_Entry(MEA_UNIT_0, &vpls_Ins_Id, &entry) != MEA_OK)
    {
            CLI_print("Error MEA_API_VPLS_Create_Entry failed \n");
            return MEA_OK;
    }

    CLI_print("Vpls_ins_Id %d   \n", vpls_Ins_Id);
   

 





return MEA_OK;
}


static MEA_Status MEA_CLI_GW_VPLSi_add_Group(int argc, char *argv[])
{
    MEA_Uint16 vpls_Ins_Id;
    if (argc < 2) {
        return MEA_ERROR;
    }

    vpls_Ins_Id = MEA_OS_atoi(argv[1]);
    
    return MEA_API_VPLSi_Add_Group(MEA_UNIT_0, vpls_Ins_Id);
}
/************************************************************************/
/*                                                                      */
/************************************************************************/

static MEA_Status MEA_CLI_GW_VPLSi_Show_Entry(int argc, char *argv[])
{
    MEA_Uint16 vpls_Ins_Id, vpls_Ins_Id_start, vpls_Ins_Id_end;
    MEA_VPLS_dbt entry;
    MEA_Bool exist;
    MEA_Uint32 count;

    if (argc < 2) {
        return MEA_ERROR;
    }

    if (!MEA_OS_strcmp(argv[1], "all"))
    {
         vpls_Ins_Id_start = 0;
         vpls_Ins_Id_end = (MEA_VPLS_MAX_OF_INSTANCE - 1);
            
    }
    else{
         vpls_Ins_Id_start = MEA_OS_atoi(argv[1]);
         vpls_Ins_Id_end= vpls_Ins_Id_start;
         if (vpls_Ins_Id_end == MEA_VPLS_MAX_OF_INSTANCE){
             CLI_print("Error max of instance %d >= %d\n", vpls_Ins_Id_start, MEA_VPLS_MAX_OF_INSTANCE);
             return MEA_ERROR;
         }
    }


    count = 0;
    for (vpls_Ins_Id = vpls_Ins_Id_start; vpls_Ins_Id <= vpls_Ins_Id_end; vpls_Ins_Id++)
        {
            MEA_API_VPLSi_isExist(MEA_UNIT_0, vpls_Ins_Id, &exist);
            if (exist == MEA_FALSE)
                continue;

            if (MEA_API_VPLSi_Get_Entry(MEA_UNIT_0, vpls_Ins_Id, &entry) != MEA_OK){
                CLI_print("Error MEA_API_Get_VPLS_Entry %d failed \n", vpls_Ins_Id);
            }

            if (count == 0){
                CLI_print(" ------VPLSi----------     \n");
                CLI_print(" Id   SM_X vpn maxLeaf       \n");
                CLI_print(" ---- ---- --- -------    \n");
            }
            CLI_print(" %4d %3d %7d %3d  \n",
                        vpls_Ins_Id,
                        entry.SM_mode,
                        entry.vpn,
                        entry.max_of_fooding);

            count++;
        }

    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_VPLSi_Delete_Entry(int argc, char *argv[])
{
    MEA_Uint16 vpls_Ins_Id;
    MEA_Bool exist;

    if (argc < 2) {
        return MEA_ERROR;
    }

    
    if (!MEA_OS_strcmp(argv[1], "all"))
    {
        for (vpls_Ins_Id = 0; vpls_Ins_Id < MEA_VPLS_MAX_OF_INSTANCE; vpls_Ins_Id++)
        {
            MEA_API_VPLSi_isExist(MEA_UNIT_0, vpls_Ins_Id, &exist);
            if (exist== MEA_FALSE)
                continue;

            if (MEA_API_VPLSi_Delete_Entry(MEA_UNIT_0, vpls_Ins_Id) != MEA_OK){
                CLI_print("Error MEA_API_VPLSi_Delete_Entry %d failed \n", vpls_Ins_Id);
            }
        }
    
    }
    else
    {
        vpls_Ins_Id = (MEA_Filter_t)MEA_OS_atoi(argv[1]);
        if (MEA_API_VPLSi_Delete_Entry(MEA_UNIT_0, vpls_Ins_Id) != MEA_OK){
            CLI_print("Error MEA_API_VPLSi_Delete_Entry failed \n");
        }
    }

    
    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_VPLSi_UE_PDN_DL_Create_Entry(int argc, char *argv[])
{

    MEA_Uint16                vpls_Ins_Id;
    MEA_Uint32                UE_pdn_Id;
    MEA_VPLS_UE_Pdn_dl_dbt    entry;
    MEA_Uint32                 from_ue_pdn;
    MEA_Uint32                 to_ue_pdn;
    MEA_Uint32                 from_act;
    MEA_Uint32                 to_act;
   
    MEA_Service_Entry_Data_dbt            Service_Entry_data;
    MEA_Policer_Entry_dbt                 Service_Entry_Policer;
    MEA_Bool Policer_en = MEA_FALSE;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_EHP_Entry;
    MEA_EHP_Info_dbt                      Service_editing_info[1] ;
    





    char buf[500];
    char *up_ptr;


    int i;
    //MEA_Uint32 accsses_increase = 0;

    if (argc < 3) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&entry, 0, sizeof(entry));

    vpls_Ins_Id = MEA_OS_atoi(argv[1]);


    MEA_OS_strcpy(buf, argv[2]);
    if ((up_ptr = strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        from_ue_pdn = MEA_OS_atoi(buf);
        to_ue_pdn = MEA_OS_atoi(up_ptr);
    }
    else
    {
        UE_pdn_Id = from_ue_pdn = to_ue_pdn = MEA_OS_atoi(buf);

    }



   
    MEA_OS_memset(&Service_Entry_data, 0, sizeof(Service_Entry_data));
    MEA_OS_memset(&Service_Entry_Policer, 0, sizeof(Service_Entry_Policer));
    MEA_OS_memset(&Service_EHP_Entry, 0, sizeof(Service_EHP_Entry));
    MEA_OS_memset(&Service_editing_info, 0, sizeof(Service_editing_info));
    Service_EHP_Entry.num_of_entries = 1;
    Service_EHP_Entry.ehp_info = &Service_editing_info[0];



    for (i = 3; i < argc; i++) {

        if (!MEA_OS_strcmp(argv[i], "-action")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            MEA_OS_strcpy(buf, argv[i+1]);
            if ((up_ptr = strchr(buf, ':')) != 0)
            {
                (*up_ptr) = 0;
                up_ptr++;
                from_act = MEA_OS_atoi(buf);
                to_act = MEA_OS_atoi(up_ptr);
                entry.ActionId = from_act;
            }
            else {
                entry.ActionId =from_act=to_act= MEA_OS_atoi(buf);
                
            }
        }
        

        if (!MEA_OS_strcmp(argv[i], "-access_port")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.Access_port_id = MEA_OS_atoi(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "TFT")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.en_service_TFT = MEA_TRUE;
       
        }

        if (!MEA_OS_strcmp(argv[i], "-hType")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }

            if (entry.en_service_TFT == MEA_FALSE) {
                CLI_print("Error  set  TFT valid before the Editing \n");
                return MEA_ERROR;
            }

            Service_EHP_Entry.ehp_info[0].ehp_data.EditingType  = MEA_OS_atoi(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-dscp")) {
            if (i + 3 > argc) {
                return MEA_ERROR;
            }

            Service_EHP_Entry.ehp_info[0].ehp_data.LmCounterId_info.Command_dscp_stamp_enable = MEA_OS_atoiNum(argv[i + 1]);
            Service_EHP_Entry.ehp_info[0].ehp_data.LmCounterId_info.Command_IpPrioityType = MEA_OS_atoiNum(argv[i + 2]);
            Service_EHP_Entry.ehp_info[0].ehp_data.LmCounterId_info.Command_dscp_stamp_value = MEA_OS_atoiNum(argv[i + 3]);
        }
        if (!MEA_OS_strcmp(argv[i], "-h802_1p")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }

            Service_EHP_Entry.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.stamp802_1p = MEA_OS_atoiNum(argv[i + 1]);
            
        }
        if (!MEA_OS_strcmp(argv[i], "-mtu_id")) {
            Service_Entry_data.mtu_Id = MEA_OS_atoi(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-frag_IP")) {
            Service_Entry_data.Fragment_IP = MEA_OS_atoi(argv[i+1]);
            Service_Entry_data.IP_TUNNEL = MEA_OS_atoi(argv[i + 2]);
            Service_Entry_data.fragment_ext_int = MEA_OS_atoi(argv[i + 3]);
            Service_Entry_data.fragment_target_mtu_prof = MEA_OS_atoi(argv[i + 4]);
        }
        
        if (!MEA_OS_strcmp(argv[i], "-hvxlan_DL"))
        {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            Service_EHP_Entry.ehp_info[0].ehp_data.VxLan_info.valid = MEA_OS_atoi(argv[i+1]);
            MEA_OS_atohex_MAC(argv[i + 2], &Service_EHP_Entry.ehp_info[0].ehp_data.VxLan_info.vxlan_Da);
            Service_EHP_Entry.ehp_info[0].ehp_data.VxLan_info.vxlan_dst_Ip = MEA_OS_inet_addr(argv[i + 3]);
            Service_EHP_Entry.ehp_info[0].ehp_data.VxLan_info.vxlan_vni = MEA_OS_atoiNum(argv[i + 4]);
            Service_EHP_Entry.ehp_info[0].ehp_data.VxLan_info.l4_dest_Port = MEA_OS_atoiNum(argv[i + 5]);
            Service_EHP_Entry.ehp_info[0].ehp_data.VxLan_info.l4_src_Port = MEA_OS_atoiNum(argv[i + 6]);
            Service_EHP_Entry.ehp_info[0].ehp_data.VxLan_info.ttl = MEA_OS_atoiNum(argv[i + 7]);
            Service_EHP_Entry.ehp_info[0].ehp_data.VxLan_info.sgw_ipId = MEA_OS_atoiNum(argv[i + 8]);



        }
        if (!MEA_OS_strcmp(argv[i], "-hETHCS_DL")) 
        {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }

            if (entry.en_service_TFT == MEA_FALSE) {
                CLI_print("Error  set  TFT valid before the Editing \n");
                return MEA_ERROR;
            }

            Service_EHP_Entry.ehp_info[0].ehp_data.ETHCS_DL_info.valid = MEA_OS_atoi(argv[i+1]);
            if (Service_EHP_Entry.ehp_info[0].ehp_data.ETHCS_DL_info.valid) {
                if (i + 6 > argc) {
                    CLI_print("Error  total parameter is wrong  \n");
                    return MEA_ERROR;
                }
            }


            if (Service_EHP_Entry.ehp_info[0].ehp_data.ETHCS_DL_info.valid) {
                MEA_OS_atohex_MAC(argv[i+2], &Service_EHP_Entry.ehp_info[0].ehp_data.ETHCS_DL_info.Da_eNB);
                Service_EHP_Entry.ehp_info[0].ehp_data.ETHCS_DL_info.dst_eNB_Ip = MEA_OS_inet_addr(argv[i+3]);
                Service_EHP_Entry.ehp_info[0].ehp_data.ETHCS_DL_info.TE_ID = MEA_OS_atoiNum(argv[i+4]);
                Service_EHP_Entry.ehp_info[0].ehp_data.ETHCS_DL_info.dst_UE_Ip = MEA_OS_inet_addr(argv[i+5]);
                Service_EHP_Entry.ehp_info[0].ehp_data.ETHCS_DL_info.sgw_ipId = MEA_OS_atoiNum(argv[i+6]);
            }
        }


        if (!MEA_OS_strcmp(argv[i], "-Pm")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            if (entry.en_service_TFT == MEA_FALSE) {
                CLI_print("Error  set  the TFT  \n");
                return MEA_ERROR;
            }
            Service_Entry_data.pmId = MEA_OS_atoiNum(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "-Tm")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            if (entry.en_service_TFT == MEA_FALSE) {
                CLI_print("Error  set  the TFT  \n");
                return MEA_ERROR;
            }
            Service_Entry_data.tmId = MEA_OS_atoiNum(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "-policer")) {
            
            if (entry.en_service_TFT == MEA_FALSE) {
                CLI_print("Error  set  the TFT  \n");
                return MEA_ERROR;
            }

            Policer_en = MEA_TRUE;
            Service_Entry_Policer.CIR = MEA_OS_atoiNum64(argv[i + 1]);
            Service_Entry_Policer.EIR = MEA_OS_atoiNum64(argv[i + 2]);
            Service_Entry_Policer.CBS = MEA_OS_atoiNum(argv[i + 3]);
            Service_Entry_Policer.EBS = MEA_OS_atoiNum(argv[i + 4]);
            Service_Entry_Policer.cup = MEA_OS_atoiNum(argv[i + 5]);
            Service_Entry_Policer.color_aware = MEA_OS_atoiNum(argv[i + 6]);
            Service_Entry_Policer.comp = MEA_OS_atoiNum(argv[i + 7]);
            
        }

        if (!MEA_OS_strcmp(argv[i], "-pol_id")) {
            Service_Entry_data.policer_prof_id = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-pgt")) {
            Service_Entry_Policer.gn_type = MEA_OS_atoiNum(argv[i + 1]);

        
        }

        if (!MEA_OS_strcmp(argv[i], "-uepdn")) {
            if (entry.en_service_TFT == MEA_FALSE) {
                CLI_print("Error  set  the TFT  \n");
                return MEA_ERROR;
            }
            Service_Entry_data.Uepdn_valid = MEA_OS_atoiNum(argv[i + 1]);
            Service_Entry_data.Uepdn_id    = MEA_OS_atoiNum(argv[i + 2]);

        }

        if (!MEA_OS_strcmp(argv[i], "-HPM")) {
            if (entry.en_service_TFT == MEA_FALSE) {
                CLI_print("Error  set  the TFT  \n");
                return MEA_ERROR;
            }
            Service_Entry_data.HPM_valid     = MEA_OS_atoiNum(argv[i + 1]);
            Service_Entry_data.HPM_profileId = MEA_OS_atoiNum(argv[i + 2]);
            Service_Entry_data.HPM_prof_mask = MEA_OS_atoiNum(argv[i + 3]);


        }

       

        if (!MEA_OS_strcmp(argv[i], "-flow_pol")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            if (entry.en_service_TFT == MEA_FALSE) {
                CLI_print("Error  set  the TFT  \n");
                return MEA_ERROR;
            }
            Service_Entry_data.Ingress_flow_policer_ProfileId = MEA_OS_atoiNum(argv[i + 1]);
        }



       
    
    
    }

    for (UE_pdn_Id = from_ue_pdn; UE_pdn_Id <= to_ue_pdn; UE_pdn_Id++)
    {
        if (entry.en_service_TFT == MEA_TRUE) {
            entry.Service_Entry_data = &Service_Entry_data;
            entry.EHP_Entry = &Service_EHP_Entry;
            if (Policer_en == MEA_TRUE) {
                entry.Service_Entry_Policer= &Service_Entry_Policer;
            
            }
        
        }



        if (MEA_API_VPLSi_UE_PDN_DL_Create_Entry(MEA_UNIT_0,
            vpls_Ins_Id,
            UE_pdn_Id,
            &entry) != MEA_OK) 
        {

            CLI_print("Error  failed \n");
            return MEA_ERROR;
        }

        if (entry.en_service_TFT == MEA_FALSE) {
            if (from_act < to_act) {
                if ((entry.ActionId + 1) < to_act){
                    entry.ActionId++;
                    entry.Access_port_id++;
                 }
            }
            if ((UE_pdn_Id % 100) == 0)
                CLI_print(".##. ");
        }

        
    }
    CLI_print("Done \n");
    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

static MEA_Status MEA_CLI_GW_VPLSi_UE_PDN_DL_Delete_Entry(int argc, char *argv[])
{
    MEA_Uint16 vpls_Ins_Id;
    MEA_Uint32 UE_pdn_Id;
    MEA_Bool exist;
    MEA_Bool o_found = MEA_FALSE;

    if (argc <= 2) {
        CLI_print("Error min parameter is 3\n");
        return MEA_ERROR;
    }


    if (!MEA_OS_strcmp(argv[1], "all") &&
        !MEA_OS_strcmp(argv[2], "all"))
    {
        
        
        
        for (vpls_Ins_Id = 0; vpls_Ins_Id < MEA_VPLS_MAX_OF_INSTANCE; vpls_Ins_Id++)
        {
            exist = MEA_FALSE;
            if (MEA_API_VPLSi_isExist(MEA_UNIT_0, vpls_Ins_Id, &exist) != MEA_OK){
                return MEA_OK;
            }
            if (exist == MEA_FALSE){
                continue;
            }
            UE_pdn_Id = 0;

            if (MEA_API_VPLSi_UE_PDN_DL_GetFirst_Entry(MEA_UNIT_0,
                vpls_Ins_Id,
                &UE_pdn_Id,
                &o_found) != MEA_OK){
                CLI_print("Error MEA_API_VPLSi_Delete_Entry failed \n");
                return MEA_OK;
            }

                while (o_found == MEA_TRUE){
                    if (MEA_API_VPLSi_UE_PDN_DL_Delete_Entry(MEA_UNIT_0,
                        vpls_Ins_Id,
                        UE_pdn_Id) != MEA_OK){
                        CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_Delete_Entry failed \n");
                        return MEA_OK;
                    }
                    if (MEA_API_VPLSi_UE_PDN_DL_GetNext_Entry(MEA_UNIT_0,
                        vpls_Ins_Id,
                        &UE_pdn_Id,
                        &o_found) != MEA_OK){
                        CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_GetNext_Entery failed \n");
                        return MEA_OK;
                    }
                }
            
        }

    }
    else if ( MEA_OS_strcmp(argv[1], "all") &&
             !MEA_OS_strcmp(argv[2], "all"))
    {
     
        vpls_Ins_Id = (MEA_Uint16)MEA_OS_atoi(argv[1]);
            MEA_API_VPLSi_isExist(MEA_UNIT_0, vpls_Ins_Id, &exist);
            if (exist == MEA_TRUE){
                UE_pdn_Id = 0;

                if (MEA_API_VPLSi_UE_PDN_DL_GetFirst_Entry(MEA_UNIT_0,
                    vpls_Ins_Id,
                    &UE_pdn_Id,
                    &o_found) != MEA_OK){
                    CLI_print("Error MEA_API_VPLSi_Delete_Entry failed \n");
                    return MEA_OK;
                }

                while (o_found == MEA_TRUE){
                    if (MEA_API_VPLSi_UE_PDN_DL_Delete_Entry(MEA_UNIT_0,
                        vpls_Ins_Id,
                        UE_pdn_Id) != MEA_OK){
                        CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_Delete_Entry failed \n");
                        return MEA_OK;
                    }
                    if (MEA_API_VPLSi_UE_PDN_DL_GetNext_Entry(MEA_UNIT_0,
                        vpls_Ins_Id,
                        &UE_pdn_Id,
                        &o_found) != MEA_OK){
                        CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_GetNext_Entery failed \n");
                        return MEA_OK;
                    }

                }
            }

    } else {
        if (!MEA_OS_strcmp(argv[1], "all") && MEA_OS_strcmp(argv[2], "all")){
         
            return MEA_ERROR;
        }

        vpls_Ins_Id = (MEA_Uint16)MEA_OS_atoi(argv[1]);
        UE_pdn_Id = MEA_OS_atoi(argv[2]);

        if (MEA_API_VPLSi_UE_PDN_DL_Delete_Entry(MEA_UNIT_0,
                                                  vpls_Ins_Id,
                                                  UE_pdn_Id) != MEA_OK){
            CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_Delete_Entry failed \n");
            return MEA_OK;
        }
        
    }


    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_VPLSi_UE_PDN_DL_Show_Entry(int argc, char *argv[])
{

    MEA_Uint16 vpls_Ins_Id;
    MEA_Uint32 UE_pdn_Id;
    MEA_Bool exist;
    MEA_Bool o_found = MEA_FALSE;
    MEA_VPLS_UE_Pdn_dl_dbt  entry;
    MEA_Service_Entry_Data_dbt SrviceData;

    if (argc <= 2) {
        CLI_print("Error min parameter is 3\n");
        return MEA_ERROR;
    }


    CLI_print(" ***************** UE_PDN_DL_Show_Entry ***************\n");
    CLI_print("---------------------------------------------------------------  \n");
    CLI_print("InsID  Leaf    ActionId Access   Sid      TFT Uepdn     HPM         \n");
    CLI_print("                        port-Id           en         profId mask     \n");
    CLI_print("----- -------- -------- -------- -------- --- ------ ------ ------      \n");
    if (!MEA_OS_strcmp(argv[1], "all") &&
        !MEA_OS_strcmp(argv[2], "all"))
    {
        
        for (vpls_Ins_Id = 0; vpls_Ins_Id < MEA_VPLS_MAX_OF_INSTANCE; vpls_Ins_Id++)
        {
            MEA_API_VPLSi_isExist(MEA_UNIT_0, vpls_Ins_Id, &exist);
            if (exist == MEA_FALSE)
                continue;
            UE_pdn_Id = 0;
            CLI_print("%5d\n", vpls_Ins_Id);
            if (MEA_API_VPLSi_UE_PDN_DL_GetFirst_Entry(MEA_UNIT_0,
                vpls_Ins_Id,
                &UE_pdn_Id,
                &o_found) != MEA_OK){
                CLI_print("Error MEA_API_VPLSi_Delete_Entry failed \n");
                return MEA_OK;
            }
            
            while (o_found == MEA_TRUE){
               
                if (MEA_API_VPLSi_UE_PDN_DL_Get_Entry(MEA_UNIT_0,
                    vpls_Ins_Id,
                    UE_pdn_Id,
                    &entry) != MEA_OK){
                    CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_Get_Entry failed \n");
                    return MEA_OK;
                }
                MEA_API_Get_Service(MEA_UNIT_0, entry.serviceId, NULL, &SrviceData, NULL, NULL, NULL);

                CLI_print("      %8d %8d %8d %8d %3s ", UE_pdn_Id, entry.ActionId, entry.Access_port_id, entry.serviceId, MEA_STATUS_STR(entry.en_service_TFT) );
                if (SrviceData.HPM_valid && SrviceData.Uepdn_valid) {
                    CLI_print(" %6d %6d %6d \n", SrviceData.Uepdn_id,
                                                 SrviceData.HPM_profileId,
                                                 SrviceData.HPM_prof_mask);

                }
                else {
                    CLI_print(" \n");
                }




                if (MEA_API_VPLSi_UE_PDN_DL_GetNext_Entry(MEA_UNIT_0,
                    vpls_Ins_Id,
                    &UE_pdn_Id,
                    &o_found) != MEA_OK){
                    CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_GetNext_Entery failed \n");
                    return MEA_OK;
                }
            }

        }

    }
    else if (MEA_OS_strcmp(argv[1], "all") &&
        !MEA_OS_strcmp(argv[2], "all"))
    {

        vpls_Ins_Id = (MEA_Uint16)MEA_OS_atoi(argv[1]);
        MEA_API_VPLSi_isExist(MEA_UNIT_0, vpls_Ins_Id, &exist);
        if (exist == MEA_TRUE){
            UE_pdn_Id = 0;
            CLI_print("%5d\n", vpls_Ins_Id);
            if (MEA_API_VPLSi_UE_PDN_DL_GetFirst_Entry(MEA_UNIT_0,
                vpls_Ins_Id,
                &UE_pdn_Id,
                &o_found) != MEA_OK){
                CLI_print("Error MEA_API_VPLSi_Delete_Entry failed \n");
                return MEA_OK;
            }

            while (o_found == MEA_TRUE){
                if (MEA_API_VPLSi_UE_PDN_DL_Get_Entry(MEA_UNIT_0,
                    vpls_Ins_Id,
                    UE_pdn_Id,
                    &entry) != MEA_OK){
                    CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_Delete_Entry failed \n");
                    return MEA_OK;
                }
                MEA_API_Get_Service(MEA_UNIT_0, entry.serviceId, NULL, &SrviceData, NULL, NULL, NULL);

                CLI_print("      %8d %8d %8d %8d %3s ", UE_pdn_Id, entry.ActionId, entry.Access_port_id, entry.serviceId, MEA_STATUS_STR(entry.en_service_TFT));
                if (SrviceData.HPM_valid && SrviceData.Uepdn_valid) {
                    CLI_print(" %6d %6d %6d \n", SrviceData.Uepdn_id,
                        SrviceData.HPM_profileId,
                        SrviceData.HPM_prof_mask);

                }
                else {
                    CLI_print(" \n");
                }

                if (MEA_API_VPLSi_UE_PDN_DL_GetNext_Entry(MEA_UNIT_0,
                    vpls_Ins_Id,
                    &UE_pdn_Id,
                    &o_found) != MEA_OK){
                    CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_GetNext_Entery failed \n");
                    return MEA_OK;
                }

            }
        }

    }
    else {
        vpls_Ins_Id = (MEA_Uint16)MEA_OS_atoi(argv[1]);
        UE_pdn_Id = MEA_OS_atoi(argv[2]);

        if (MEA_API_VPLSi_UE_PDN_DL_Get_Entry(MEA_UNIT_0,
            vpls_Ins_Id,
            UE_pdn_Id,
            &entry) != MEA_OK){
            CLI_print("Error MEA_API_VPLSi_UE_PDN_DL_Get_Entry failed \n");
            return MEA_OK;
        }
        CLI_print("%5d\n", vpls_Ins_Id);
        MEA_API_Get_Service(MEA_UNIT_0, entry.serviceId, NULL, &SrviceData, NULL, NULL, NULL);

        CLI_print("      %8d %8d %8d %8d %3s ", UE_pdn_Id, entry.ActionId, entry.Access_port_id, entry.serviceId, MEA_STATUS_STR(entry.en_service_TFT));
        if (SrviceData.HPM_valid && SrviceData.Uepdn_valid) {
            CLI_print(" %6d %6d %6d \n", SrviceData.Uepdn_id,
                SrviceData.HPM_profileId,
                SrviceData.HPM_prof_mask);

        }
        else {
            CLI_print(" \n");
        }


    }




    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

static MEA_Status MEA_CLI_GW_VPLSi_Set_Handover_Entry(int argc, char *argv[])
{
    
    MEA_Uint32 index;
    mea_UEpdn_Handover_dbt entry;



    if (argc < 3) {
        return MEA_ERROR;
    }
    //<index> <valid><vpn><access port><en_vp><vp>
   
    MEA_OS_memset(&entry, 0, sizeof(entry));

    index= MEA_OS_atoi(argv[1]);
    entry.force = MEA_OS_atoi(argv[2]);

    if (argc > 3){
        if (argc < 6){
            return MEA_ERROR;
        }
        entry.vpn = MEA_OS_atoi(argv[3]);
        entry.access_port = MEA_OS_atoi(argv[4]);
        entry.en_vp = MEA_OS_atoi(argv[5]);
        entry.clusterId = MEA_OS_atoi(argv[6]);

    }




    if(MEA_API_UEPDN_Set_Handover(MEA_UNIT_0,index, &entry) != MEA_OK)
    {
        CLI_print("Error  failed MEA_API_UEPDN_Set_Handover \n");
        return MEA_ERROR;
    }

    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/

static MEA_Status MEA_CLI_GW_VPLSi_Get_Handover_Entry(int argc, char *argv[])
{


    MEA_Uint8 index, index_start, index_end;
    
    mea_UEpdn_Handover_dbt entry;
    
    MEA_Uint32 count;

    if (argc < 2) {
        return MEA_ERROR;
    }

    if (!MEA_OS_strcmp(argv[1], "all"))
    {
        index_start = 0;
        index_end = MEA_MAX_OF_VPLS_Handover - 1;

    }
    else{
        index_start = MEA_OS_atoi(argv[1]);
        index_end = index_start;
        if (index_end == MEA_MAX_OF_VPLS_Handover){
            index_end = MEA_MAX_OF_VPLS_Handover - 1;
        }
    }


    count = 0;
    for (index = index_start; index <= index_end; index++)
    {
 
        
            if (MEA_API_UEPDN_Get_Handover(MEA_UNIT_0, index, &entry) != MEA_OK){
                CLI_print("Error MEA_API_UEPDN_Get_Handover %d failed \n", index);
        }

        if (count == 0){
            CLI_print(" ------VPLSi---Handover--------------     \n");
            CLI_print(" Id   force VPN  Access en  cluster      \n");
            CLI_print(" ---- ----- ---- ------ ---------- \n");
        }
        count++;
        CLI_print(" %4d", index);
        if (entry.force == MEA_FALSE){
            CLI_print(" %5s\n", MEA_STATUS_STR(entry.force));
            continue;
        }

        CLI_print(" %5s %4d %6d ",

            MEA_STATUS_STR(entry.force),
            entry.vpn,
            entry.access_port);
           CLI_print("%3s ", MEA_STATUS_STR(entry.en_vp));
           if (entry.en_vp == MEA_TRUE){
               CLI_print("%4d ", entry.clusterId);
           }
           else{
               CLI_print("%s ", "Drop");
           }
           CLI_print("\n");


        count++;
    }

    return MEA_OK;


}

/*------------------------------------------------------------
TFT CLI
-------------------------------------------------------------*/
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Create_Entry(int argc, char *argv[])
{
    MEA_TFT_t  tft_Id;
    MEA_TFT_entry_data_dbt entry;
    int i;

    if (argc < 2) {
        return MEA_ERROR;
    }
    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (MEA_OS_strcmp(argv[1], "auto") == 0) {
        tft_Id = MEA_PLAT_GENERATE_NEW_ID;
    }
    else {
        tft_Id = MEA_OS_atoiNum(argv[1]);
    }
    for (i = 1; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "-rule_type")) {
            entry.en_rule_type = MEA_TRUE;
            entry.rule_type = MEA_OS_atoiNum(argv[i + 1]);
        }
    }


    if (MEA_API_TFT_Create_QoS_Profile(MEA_UNIT_0, &tft_Id, &entry) != MEA_OK) {
        CLI_print("Error MEA_API_TFT_Create_QoS_Profile failed \n");
        return MEA_OK;
    }
   
    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Delete_Entry(int argc, char *argv[])
{

    MEA_Uint16 Id;
    MEA_Uint16 from_Id;
    MEA_Uint16 to_Id;
    MEA_Bool exist;

    if (argc < 2) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_Id = 0;
        to_Id = MEA_TFT_L3L4_MAX_PROFILE - 1;

    }
    else {
        from_Id = MEA_OS_atoiNum(argv[1]);
        to_Id = MEA_OS_atoiNum(argv[1]);
        Id = from_Id;
        if (MEA_API_TFT_QoS_Profile_IsExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
            CLI_print("Error  MEA_API_TFT_Prof_Mask_IsExist failed  MaskId=%d\n", Id);
            return MEA_OK;
        }
    }


    for (Id = from_Id; Id <= to_Id; Id++)
    {
        exist = MEA_FALSE;

        if (MEA_API_TFT_QoS_Profile_IsExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
            continue;

        if (!exist)
            continue;



        if (MEA_API_TFT_Delete_QoS_Profile(MEA_UNIT_0, Id) != MEA_OK)
        {
            CLI_print("Error  MEA_API_TFT_Delete_QoS_Profile failed \n");
            return MEA_OK;
        }
    }
        
    
    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/

static MEA_Status MEA_CLI_GW_TFT_Set_EntryData(int argc, char *argv[])
{
    int i;
    MEA_TFT_t tft_Id;
    MEA_Uint8 index;
    MEA_TFT_data_dbt  entry;
    
    MEA_Bool exist   =MEA_FALSE;


    


    MEA_OS_memset(&entry, 0, sizeof(MEA_TFT_data_dbt));
    





    if (argc < 3) {
        return MEA_ERROR;
    }

    tft_Id = MEA_OS_atoiNum(argv[1]);
    index = MEA_OS_atoiNum(argv[2]);
    


    if (MEA_API_TFT_QoS_Profile_IsExist(MEA_UNIT_0, tft_Id, MEA_FALSE, &exist) != MEA_OK)
    {
        CLI_print("Error  MEA_API_TFT_QoS_Profile_IsExist %d \n", tft_Id);
        return MEA_OK;
    }
    if (!exist) {
        CLI_print("Error   tft_Id %d not exist\n", tft_Id);
        return MEA_OK;
    }


    if(MEA_API_TFT_Get_ClassificationRule(MEA_UNIT_0, tft_Id, index, &entry) != MEA_OK)
    {
        CLI_print("Error  MEA_API_TFT_Get_ClassificationRule %d \n", tft_Id);
        return MEA_OK;
    }

    for (i = 3; i < argc; i++){
        if (!MEA_OS_strcmp(argv[i], "type")) {
            entry.TFT_info.Type = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "Priority_Rule")) {
            entry.TFT_info.Priority_Rule = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "QoS_Index")) {
            entry.TFT_info.QoS_Index = MEA_OS_atoiNum(argv[i + 1]);
        }

       
        

        /* MEA_TFT_TYPE_L2 */
        if (!MEA_OS_strcmp(argv[i], "L2_pri")){
            entry.TFT_info.L2_enable        = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.L2_Pri           = MEA_OS_atoiNum(argv[i + 2]);
                
        }
        if (!MEA_OS_strcmp(argv[i], "L3_ethtype")) {
            entry.TFT_info.L3_EtherType = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "DSCP")) {
            entry.TFT_info.L3_DSCP_valid = MEA_OS_atoiNum(argv[i + 1]);

            entry.TFT_info.L3_from_DSCP = MEA_OS_atoiNum(argv[i + 2]);
            entry.TFT_info.L3_end_DSCP = MEA_OS_atoiNum(argv[i + 3]);
            
        }
        if (!MEA_OS_strcmp(argv[i], "IP_protocol")) {
            entry.TFT_info.IP_protocol_valid = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.IP_protocol = MEA_OS_atoiNum(argv[i + 2]);
        }
        if (!MEA_OS_strcmp(argv[i], "src_ipv4")) {
            entry.TFT_info.Source_IPv4_valid = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.Source_IPv4       = MEA_OS_inet_addr(argv[i + 2]);
            entry.TFT_info.Source_IPv4_mask  = MEA_OS_atoiNum(argv[i + 3]);
        
        }
        if (!MEA_OS_strcmp(argv[i], "dest_ipv4")) {
            entry.TFT_info.Dest_IPv4_valid = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.Dest_IPv4 = MEA_OS_inet_addr(argv[i + 2]);
            entry.TFT_info.Dest_IPv4_mask = MEA_OS_atoiNum(argv[i + 3]);

        }
        {
            unsigned char buf[sizeof(struct in6_addr)];
            int  s;
            //char str[INET6_ADDRSTRLEN];

            if (!MEA_OS_strcmp(argv[i], "ipv6")) 
            {
                /* ------- TBD ---- */
                entry.TFT_info.Ipv6_valid = MEA_OS_atoiNum(argv[i + 1]);
                entry.TFT_info.Ipv6_mask = MEA_OS_atoiNum(argv[i + 3]);
                if (entry.TFT_info.Ipv6_valid == MEA_TRUE) {
                    s = inet_pton(AF_INET6, argv[i + 2], buf);
                    if (s <= 0) {
                        CLI_print("ipv6 format error\n");
                        return MEA_ERROR;
                    }
                    entry.TFT_info.Ipv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
                    entry.TFT_info.Ipv6[1] = (buf[8 ] << 24) | (buf[9 ] << 16) | (buf[10] << 8) | buf[11] << 0;
                    entry.TFT_info.Ipv6[2] = (buf[4 ] << 24) | (buf[5 ] << 16) | (buf[6 ] << 8) | buf[7 ] << 0;
                    entry.TFT_info.Ipv6[3] = (buf[0 ] << 24) | (buf[1 ] << 16) | (buf[2 ] << 8) | buf[3 ] << 0;
                }
                else {
                    entry.TFT_info.Ipv6[0] = 0;
                    entry.TFT_info.Ipv6[1] = 0;
                    entry.TFT_info.Ipv6[2] = 0;
                    entry.TFT_info.Ipv6[3] = 0;
                    entry.TFT_info.Ipv6_mask = 0;
                }


            }
        }
        if (!MEA_OS_strcmp(argv[i], "flow_lable")) {
            entry.TFT_info.Flow_Label = MEA_OS_atoiNum(argv[i + 1]);
        }


        if (!MEA_OS_strcmp(argv[i], "L4_srcport")) {

            entry.TFT_info.L4_Source_port_valid = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.L4_from_Source_port = MEA_OS_atoiNum(argv[i + 2]);
            entry.TFT_info.L4_end_Source_port = MEA_OS_atoiNum(argv[i + 3]);
        }
        if (!MEA_OS_strcmp(argv[i], "L4_destport")) {
            entry.TFT_info.L4_dest_valid         = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.L4_from_Dest_port     = MEA_OS_atoiNum(argv[i + 2]);
            entry.TFT_info.L4_end_Dest_port      = MEA_OS_atoiNum(argv[i + 3]);
        }
    
    }



    
       
 

    if (MEA_API_TFT_Set_ClassificationRule(MEA_UNIT_0, tft_Id, index, &entry) != MEA_OK)
    {
        CLI_print("Error MEA_API_TFT_Add_EntryData failed \n");
        return MEA_OK;
    }


    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Get_EntryData(int argc, char *argv[])
{
    int i;
    MEA_TFT_t tft_Id, from_tft_Id, To_tft_Id;
    MEA_Uint8 index, fromPri, ToPri;
    MEA_TFT_data_dbt  entry;
    
    //MEA_Uint32  count = 0;
    MEA_Bool exist;
    char                     Ip_str[20];
    MEA_Uint32 IPaddress;

    unsigned char buf[sizeof(struct in6_addr)];
    //int  s;
    char str[INET6_ADDRSTRLEN];


    MEA_TFT_key_t TFT_info;

    char info_TFT_Type[MEA_TFT_TYPE_LAST][20];

    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_DISABLE][0]), "DISABLE");
    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_NO_IP][0]),   "NO_IP");
    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_IPV4][0]),    "IPV4");
    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_IPV6][0]),    "IPV6_Dest");
    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_IPV6_Source][0]),    "IPV6_Source");
    


    MEA_OS_memset(&TFT_info, 0, sizeof(MEA_TFT_key_t));




    if (argc < 3) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_tft_Id = 0;
        To_tft_Id = MEA_TFT_L3L4_MAX_PROFILE - 1;
    }
    else {
        from_tft_Id = MEA_OS_atoiNum(argv[1]);
        To_tft_Id = from_tft_Id;

    }

    if (MEA_OS_strcmp(argv[2], "all") == 0) {
        fromPri = 0;
        ToPri = MEA_TFT_L3L4_MAX_Rule - 1;


    }
    else {
        fromPri = MEA_OS_atoiNum(argv[2]);
        ToPri = fromPri;

    }

    for (tft_Id = from_tft_Id; tft_Id <= To_tft_Id; tft_Id++) {
        if (MEA_API_TFT_QoS_Profile_IsExist(MEA_UNIT_0, tft_Id, MEA_TRUE, &exist) != MEA_OK)
            continue;
        if (!exist)
            continue;
        CLI_print("------------------------------------ \n");
        CLI_print("         Profile Id:%4d \n", tft_Id);
        CLI_print("------------------------------------ \n");
        for (index = fromPri; index <= ToPri; index++) {
            MEA_OS_memset(&entry, 0, sizeof(MEA_TFT_data_dbt));
            if (MEA_API_TFT_Get_ClassificationRule(MEA_UNIT_0, tft_Id, index, &entry) != MEA_OK) {
                CLI_print("Error MEA_API_TFT_Get_ClassificationRule tft_Id[%2d][%2d]\n", tft_Id, index);
                continue;
            }
            
            
            if (entry.TFT_info.Type == MEA_TFT_TYPE_DISABLE) {
                //CLI_print("index:%2d Type: %10s  \n", index, info_TFT_Type[entry.TFT_info.Type], entry.TFT_info.Priority_Rule, entry.TFT_info.QoS_Index);
                continue;
            }

            CLI_print(" index:%2d Type: %15s >> Priority_Rule=%2d QoS=%2d \n", index, info_TFT_Type[entry.TFT_info.Type],entry.TFT_info.Priority_Rule, entry.TFT_info.QoS_Index);

           

            
            if (entry.TFT_info.L2_enable == MEA_TRUE) {
                CLI_print(" L2_Priority -  pri=0x%2x  ", entry.TFT_info.L2_Pri  );
                for (i = 0; i < 8; i++) {
                    
                    CLI_print(" .p[%d]=[%s] ", i, MEA_STATUS_STR( (( entry.TFT_info.L2_Pri >> i ) & 0x1) ));
                
                }
                CLI_print("\n");
 
            }
            else {
                CLI_print(" L2_Priority -  D.C  \n");
            }
           
           
            if (entry.TFT_info.L3_EtherType != 0) {
                    CLI_print(" L3_EtherType %d \n", entry.TFT_info.L3_EtherType);
            }
            else {
                    CLI_print(" L3_EtherType D.C \n");
            }

            if (entry.TFT_info.L3_DSCP_valid == MEA_TRUE) {
                CLI_print(" L3_pri   %d:%d \n",
                    entry.TFT_info.L3_from_DSCP,
                    entry.TFT_info.L3_end_DSCP);
            }

            if (entry.TFT_info.IP_protocol_valid) {
                CLI_print(" IP-protocol[YES]=  %4d \n", entry.TFT_info.IP_protocol);
            }
            else {
                CLI_print(" IP-protocol[NO ]=  0x%x \n", 0xff);
            }



            if (entry.TFT_info.Ipv6_valid == MEA_TRUE) {
                    


                MEA_OS_ipv6_to_buf(&entry.TFT_info.Ipv6[0], &buf[0]);


                if (inet_ntop(AF_INET6, buf, str, INET6_ADDRSTRLEN) == NULL) {
                    CLI_print(">>>inet_ntop<<< \n");
                }

                CLI_print("  Ipv6 %s  mask %d \n", str, entry.TFT_info.Ipv6_mask);
                CLI_print("  flow-lable 0x%08x \n", entry.TFT_info.Flow_Label);

            }
            else {

                if (entry.TFT_info.Dest_IPv4_valid == MEA_TRUE) {
                    IPaddress = entry.TFT_info.Dest_IPv4;
                    MEA_OS_inet_ntoa(MEA_OS_htonl(IPaddress), Ip_str, sizeof(Ip_str));
                    CLI_print(" DestIPv4: %s  %d \n", Ip_str, entry.TFT_info.Dest_IPv4_mask);
                }
                if (entry.TFT_info.Source_IPv4_valid == MEA_TRUE) {
                    IPaddress = entry.TFT_info.Source_IPv4;
                    MEA_OS_inet_ntoa(MEA_OS_htonl(IPaddress), Ip_str, sizeof(Ip_str));
                    CLI_print(" SrcIPv4: %s %d \n", Ip_str, entry.TFT_info.Source_IPv4_mask);
                }
                               
               
                if (entry.TFT_info.L4_Source_port_valid) {
                    CLI_print(" L4srcPort  %4d : %4d  \n",
                        entry.TFT_info.L4_from_Source_port,
                        entry.TFT_info.L4_end_Source_port);
                }
                else {
                    CLI_print(" L4srcPort D.C\n");
                }
                if (entry.TFT_info.L4_dest_valid) {
                    CLI_print(" L4DestPort  %4d : %4d \n",
                        entry.TFT_info.L4_from_Dest_port,
                        entry.TFT_info.L4_end_Dest_port);
                }
                else {
                    CLI_print(" L4DestPort DC\n");
                }
           }



                CLI_print("------------------------------------ \n");
        }

    }

    CLI_print("------------------------------------ \n");



    return MEA_OK;

}


/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Create_mask_prof(int argc, char *argv[])
{
    //int i;
    MEA_Uint16 MaskId;







    if (argc < 2) {
        return MEA_ERROR;
    }

    if (!MEA_OS_strcmp(argv[1], "auto")) {
        MaskId = MEA_PLAT_GENERATE_NEW_ID;
    }
    else {
        MaskId = MEA_OS_atoiNum(argv[1]);
    }





    if (MEA_API_TFT_Prof_Mask_Create(MEA_UNIT_0, &MaskId) != MEA_OK)
    {
        CLI_print("Error MEA_API_TFT_Prof_Mask_Create failed \n");
        return MEA_OK;
    }

    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Set_mask_prof(int argc, char *argv[])
{
    int i;
    MEA_Uint16 MaskId;
    MEA_TFT_mask_data_dbt entry;



    if (argc < 2) {
        return MEA_ERROR;
    }

    MaskId = MEA_OS_atoiNum(argv[1]);
    MEA_OS_memset(&entry, 0, sizeof(entry));

    for (i = 2; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "-mask")) {
            entry.mask = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-rule_type")) {
            entry.en_rule_type = MEA_TRUE;
            entry.rule_type = MEA_OS_atoiNum(argv[i + 1]);
        }
    
    }


    if (MEA_API_TFT_Prof_Mask_Set(MEA_UNIT_0, MaskId,&entry) != MEA_OK)
    {
        CLI_print("Error MEA_API_TFT_Prof_Mask_Create failed \n");
        return MEA_OK;
    }


    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Get_mask_prof(int argc, char *argv[])
{

    MEA_Uint16 MaskId;
    MEA_Uint16 from_MaskId;
    MEA_Uint16 to_MaskId;

    MEA_TFT_mask_data_dbt entry;
    MEA_Uint32 count = 0;

    MEA_Bool exist;


    if (argc < 1) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_MaskId = 0;
        to_MaskId = MEA_TFT_MASK_MAX_PROFILE - 1;

    }
    else {
        from_MaskId = MEA_OS_atoiNum(argv[1]);
        to_MaskId = MEA_OS_atoiNum(argv[1]);
        MaskId = from_MaskId;
        if (MEA_API_TFT_Prof_Mask_IsExist(MEA_UNIT_0, MaskId, MEA_FALSE, &exist) != MEA_OK) {
        
        }
        if (!exist) {
            CLI_print("ERROR: TFT_Prof_Mask_IsExist %d\n", MaskId);
            return MEA_OK;
        }

    }

        CLI_print("-------------------------------------------\n");
        CLI_print("         TFT_MASK_Profile                  \n");
        CLI_print("-------------------------------------------\n");

    for (MaskId = from_MaskId; MaskId <= to_MaskId; MaskId++) 
    {
        if (MEA_API_TFT_Prof_Mask_IsExist(MEA_UNIT_0, MaskId, MEA_TRUE, &exist) != MEA_OK)
            continue;
        if (!exist)
            continue;

       


        if(MEA_API_TFT_Prof_Mask_Get(MEA_UNIT_0, MaskId, &entry) != MEA_OK){
            return MEA_ERROR;
        }
        if (count == 0)
        {
            CLI_print("-----------------------------\n");
            CLI_print(" Mask   Val      set   num   \n");
            CLI_print(" Id     mask     rule  ofRule\n");
            CLI_print(" ----- ---------- ----- ------\n");
        }

        count++;
        

            CLI_print(" %5d 0x%08x %4s %3d\n", MaskId,
                entry.mask,
                MEA_STATUS_STR(entry.en_rule_type),
                entry.rule_type);
        

    }

    CLI_print("------------------------------------ \n");



    return MEA_OK;

}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Delete_mask_prof(int argc, char *argv[])
{
   
    MEA_Uint16 MaskId;
    MEA_Uint16 from_MaskId;
    MEA_Uint16 to_MaskId;
    MEA_Bool exist;
    if (argc < 2) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_MaskId = 0;
        to_MaskId = MEA_TFT_MASK_MAX_PROFILE - 1;

    }
    else {
        from_MaskId = MEA_OS_atoiNum(argv[1]);
        to_MaskId = MEA_OS_atoiNum(argv[1]);
        MaskId = from_MaskId;
        if (MEA_API_TFT_Prof_Mask_IsExist(MEA_UNIT_0, MaskId, MEA_FALSE, &exist) != MEA_OK) {
            CLI_print("Error  MEA_API_TFT_Prof_Mask_IsExist failed  MaskId=%d\n", MaskId);
            return MEA_OK;
        }
    }


    for (MaskId = from_MaskId; MaskId <= to_MaskId; MaskId++)
    {
        exist = MEA_FALSE;

        if (MEA_API_TFT_Prof_Mask_IsExist(MEA_UNIT_0, MaskId, MEA_TRUE, &exist) != MEA_OK)
            continue;

        if (!exist)
            continue;



        if (MEA_API_TFT_Prof_Mask_Delete(MEA_UNIT_0, MaskId) != MEA_OK)
        {
            CLI_print("Error  MEA_API_TFT_Prof_Mask_Delete failed to delete MaskId=%d\n", MaskId);
            //return MEA_OK;
            continue;
        }

        CLI_print("Done: %d \n", MaskId);
    }

    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Create_UEPDN_ConfigAction(int argc, char *argv[])
{
    MEA_Uint32 i;

    MEA_Uint32 mandatory_argc;

    MEA_Uint16 uepdn_Id;
    MEA_Uint8 uepdn_Qos;
    MEA_Bool  ACT_HPM_en = MEA_FALSE;


    if (argc < 3) {
        return MEA_ERROR;
    }

    for (i = 1; i < (MEA_Uint32)argc; i++) {
        if (MEA_OS_strcmp(argv[i], "ACT_HPM") == 0) {
            mandatory_argc = 2;
            if (i + 1 + mandatory_argc > (MEA_Uint32)argc) {
                CLI_print(
                    "Missing parameters after %s\n", argv[i]);
                return MEA_OK;
            }
            uepdn_Id = (MEA_Uint16)MEA_OS_atoi(argv[i + 1]);
            uepdn_Qos = (MEA_Uint8)MEA_OS_atoi(argv[i + 2]);

            if (uepdn_Id >= MEA_UEPDN_MAX_ID) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  uepdn_Id is out  \n", __FUNCTION__);
                return MEA_ERROR;
            }
            if (uepdn_Qos > 7) {
                CLI_print(
                    "Error uepdn_Qos [%d] not on range 0:7 \n", argv[i]);
                return MEA_OK;
            }

            ACT_HPM_en = MEA_TRUE;
        }

    }


    if (ACT_HPM_en) {
        return MEA_CLI_ActionCreate(argc, argv);
    }
    else {
        CLI_print("not set ACT_HPM\n");
        return MEA_OK;
    }





    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Set_UEPDN_ConfigAction(int argc, char *argv[]){
    MEA_Uint32 i;
   
   
    MEA_Uint32 mandatory_argc;

    MEA_Uint16 uepdn_Id;
    MEA_Uint8 uepdn_Qos;
    MEA_Bool  ACT_HPM_en = MEA_FALSE;


    if (argc < 3) {
        return MEA_ERROR;
    }

    for (i = 1; i < (MEA_Uint32)argc; i++) {
        if (MEA_OS_strcmp(argv[i], "ACT_HPM") == 0) {
            mandatory_argc = 2;
            if (i + 1 + mandatory_argc > (MEA_Uint32)argc) {
                CLI_print(
                    "Missing parameters after %s\n", argv[i]);
                return MEA_OK;
            }
            uepdn_Id = (MEA_Uint16)MEA_OS_atoi(argv[i + 1]);
            uepdn_Qos = (MEA_Uint8)MEA_OS_atoi(argv[i + 2]);
            if (uepdn_Id >= MEA_UEPDN_MAX_ID) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  uepdn_Id is out  \n", __FUNCTION__);
                return MEA_ERROR;
            }
            if (uepdn_Qos > 7) {
                CLI_print(
                    "Error uepdn_Qos [%d] not on range 0:7 \n", argv[i]);
                return MEA_OK;
            }
            ACT_HPM_en = MEA_TRUE;
        }

    }


    if (ACT_HPM_en) {
        return MEA_CLI_ActionModify(argc, argv);
    }
    else {
        CLI_print("not set ACT_HPM\n");
        return MEA_OK;
    }

    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Get_UEPDN_ConfigAction_Entry(int argc, char *argv[]){
    mea_TFT_Action_rule_t entry;
    MEA_Uint16  UEPDN_Id, start_UEPDN_Id, end_UEPDN_Id;
    MEA_Uint32 count = 0;
    MEA_Bool                        exist;
    MEA_Uint8 priority, fromPri, ToPri;

    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_out_ports_tbl_entry;
    MEA_Policer_Entry_dbt                   Action_sla_params_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_Editing;
    


   

    if (argc < 3) {
        return MEA_ERROR;
    }

    
    MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
    MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
    MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */

    if (!MEA_OS_strcmp(argv[1], "all")){
        start_UEPDN_Id = 0;
        end_UEPDN_Id = MEA_UEPDN_MAX_ID-1 ;
    }
    else {
        start_UEPDN_Id = (MEA_Uint16)MEA_OS_atoiNum(argv[1]);
        end_UEPDN_Id   = start_UEPDN_Id;
        UEPDN_Id       = start_UEPDN_Id;
        if (UEPDN_Id >= MEA_UEPDN_MAX_ID) {
            CLI_print("error for UEPDN id\n");
            return MEA_ERROR;
        }
    }

    if (MEA_OS_strcmp(argv[2], "all") == 0) {
        fromPri = 0;
        ToPri = MEA_TFT_MAX_TEID_Rule-1;
       
    }
    else{
        fromPri = MEA_OS_atoiNum(argv[2]);
        ToPri = fromPri;
        if (ToPri >= MEA_TFT_MAX_TEID_Rule) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - index %d is out of range \n",
                __FUNCTION__, ToPri);
            return MEA_ERROR;

        }

    }


    count = 0;
    for (UEPDN_Id = start_UEPDN_Id; UEPDN_Id <= end_UEPDN_Id; UEPDN_Id++){
        for (priority = fromPri; priority <= ToPri; priority++) {
            if (MEA_API_UEPDN_TFT_TEID_ConfigAction_IsExist(MEA_UNIT_0, UEPDN_Id, priority, &exist) != MEA_OK){
               break;
            } else {
                if (!exist)
                    continue;

                entry.action_params = NULL;
                entry.EHP_Entry = NULL;
                entry.OutPorts_Entry = NULL;
                entry.Policer_Entry = NULL;



                MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
                MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
                MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
                MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */

                entry.action_params = &Action_data;
                entry.OutPorts_Entry = &Action_out_ports_tbl_entry;
                entry.EHP_Entry      = &Action_Editing;


                if (MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority, &entry) != MEA_OK)
                {
                    CLI_print("Error MEA_API_Set_UEPDN_ConfigAction failed UEPDN_Id %d \n", UEPDN_Id);
                    continue;
                }
                /* set the number of editing */

                if (Action_Editing.num_of_entries > 0) {
                    Action_Editing.ehp_info = (MEA_EHP_Info_dbt*)MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt)*Action_Editing.num_of_entries);
                    if (Action_Editing.ehp_info == NULL) {
                        CLI_print("ERROR: Failed to malloc ehp data\n");
                        return MEA_OK;
                    }

                    if (MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority, &entry) != MEA_OK)
                    {
                        CLI_print("Error MEA_API_Get_UEPDN_TEID_ConfigAction failed UEPDN_Id %d \n", UEPDN_Id);
                        MEA_OS_free(Action_Editing.ehp_info);

                        return MEA_OK;
                    }

                }

                if (count++ == 0) {
                    CLI_print("------------------------------------------------------------------------------------------------------\n");
                    CLI_print(" UEPDN     Pm     Tm     Edit   pro llc Pri Pri CoS Cos Col Ing fra wred lag Drop Frag_Ip    Defrag_Ip \n"
                              "  Id   Qos Id     Id     Id             F/M V/P F/M V/P     TS      pro  byP En   F T ex mtu ex in Re \n"
                              "------ --- ------ ------ ------ --- --- --- --- --- --- --- --- --- ---- --- ---- - - -- --- -- -- -- \n");
                    //12345678901234567890123456789012345678901234567890123456789012345678901234567890
                }
                if ((count % 100) == 0) {
                    count = 0;
                }
                CLI_print("%6d ", UEPDN_Id);
                CLI_print("%3d ", priority);


                if (entry.action_params->pm_id_valid) {
                    CLI_print("%6d ", entry.action_params->pm_id);
                }
                else {
                    CLI_print("%6s ", "NA");
                }

                if (entry.action_params->tm_id_valid) {
                    CLI_print("%6d ", entry.action_params->tm_id);
                }
                else {
                    CLI_print("%6s ", "NA");
                }

                if (entry.action_params->ed_id_valid) {
                    if (entry.EHP_Entry->num_of_entries != 1) {
                        CLI_print("%6s ", "MC");
                    }
                    else {
                        CLI_print("%6d ", entry.action_params->ed_id);
                    }
                }
                else {
                    CLI_print("%6s ", "NA");
                }

                if (entry.action_params->proto_llc_valid) {
                    if (entry.action_params->protocol_llc_force) {
                        CLI_print("%3d %3d ",
                            entry.action_params->Protocol,
                            entry.action_params->Llc);
                    }
                    else {
                        CLI_print("%3s %3s ",
                            "Aut", "Aut"); \
                    }
                }
                else {
                    CLI_print("%3s %3s ",
                        "NA", "NA");
                }

                CLI_print("%-3s %3d ",
                    (entry.action_params->flowMarkingMappingProfile_force) ? "Map" :
                    (entry.action_params->force_l2_pri_valid == MEA_ACTION_FORCE_COMMAND_DISABLE) ? "NA " :
                    (entry.action_params->force_l2_pri_valid == MEA_ACTION_FORCE_COMMAND_VALUE) ? "Val" :
                    (entry.action_params->force_l2_pri_valid == MEA_ACTION_FORCE_COMMAND_MAX) ? "Max" :
                    "???",
                    (entry.action_params->flowMarkingMappingProfile_force)
                    ? entry.action_params->flowMarkingMappingProfile_id
                    : entry.action_params->L2_PRI);

                CLI_print("%-3s %3d ",
                    (entry.action_params->flowCoSMappingProfile_force) ? "Map" :
                    (entry.action_params->force_cos_valid == MEA_ACTION_FORCE_COMMAND_DISABLE) ? "NA " :
                    (entry.action_params->force_cos_valid == MEA_ACTION_FORCE_COMMAND_VALUE) ? "Val" :
                    (entry.action_params->force_cos_valid == MEA_ACTION_FORCE_COMMAND_MAX) ? "Max" :
                    "???",
                    (entry.action_params->flowCoSMappingProfile_force)
                    ? entry.action_params->flowCoSMappingProfile_id
                    : entry.action_params->COS);


                if (entry.action_params->force_color_valid) {
                    CLI_print("%3d ", entry.action_params->COLOR);
                }
                else {
                    CLI_print("%3s ", "NA");
                }
                CLI_print("%3s ", MEA_STATUS_STR(entry.action_params->fwd_ingress_TS_type));
                CLI_print("%3s ", MEA_STATUS_STR(entry.action_params->fragment_enable));
                CLI_print("%4d ", entry.action_params->wred_profId);
                CLI_print("%3s ", MEA_STATUS_STR(entry.action_params->lag_bypass));
                CLI_print("%4s ", MEA_STATUS_STR(entry.action_params->Drop_en));
                CLI_print("%1s ", MEA_STATUS_STR_Y(entry.action_params->Fragment_IP ));
                CLI_print("%1s ", MEA_STATUS_STR_Y(entry.action_params->IP_TUNNEL ) );
                CLI_print("%2s ", (entry.action_params->fragment_ext_int == MEA_TRUE) ? "Y" : "N");
                CLI_print("%2d ", entry.action_params->fragment_target_mtu_prof);
                CLI_print("%2s ", MEA_STATUS_STR_Y(entry.action_params->Defrag_ext_IP));
                CLI_print("%2s ", MEA_STATUS_STR_Y(entry.action_params->Defrag_int_IP) );
                CLI_print("%2d ", (entry.action_params->Defrag_Reassembly_L2_profile));

                CLI_print("\n");

                if (Action_Editing.ehp_info) {
                    MEA_OS_free(Action_Editing.ehp_info);
                    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing));
                }

            }//IsExist
            
        }//index priority
        
    }//UE
    


    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Get_UEPDN_ConfigAction_editing(int argc, char *argv[]) 
{
    
    
    mea_TFT_Action_rule_t entry;
    MEA_Uint16  UEPDN_Id, start_UEPDN_Id, end_UEPDN_Id;
    MEA_Uint32 count = 0;
    MEA_Uint32 i;
    MEA_Bool                        exist;
    MEA_Uint8 priority, fromPri, ToPri;

    mea_cli_editing_info_te info = MEA_CLI_EDITING_INFO_EDITING;
    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_out_ports_tbl_entry;
    MEA_Policer_Entry_dbt                   Action_sla_params_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_Editing;

    mea_cli_showEditing_info_t show_entry;
   // MEA_Bool set_relavant_input=MEA_FALSE;





    if (argc <=2) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
    MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
    MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */

    start_UEPDN_Id = 0;
    end_UEPDN_Id = 0;
    fromPri = 0;
    ToPri = 7;

    for (i = 0; i < (MEA_Uint32)argc; i++) {
        if (i == 1) {
            if (!MEA_OS_strcmp(argv[1], "all")) {
                start_UEPDN_Id = 0;
                end_UEPDN_Id = MEA_UEPDN_MAX_ID - 1;
            }
            else {
                start_UEPDN_Id = (MEA_Uint16)MEA_OS_atoi(argv[1]);
                end_UEPDN_Id = start_UEPDN_Id;
                UEPDN_Id = start_UEPDN_Id;

            }
        }
        if (i == 2) {
            if (MEA_OS_strcmp(argv[2], "all") == 0) {
                fromPri = 0;
                ToPri = MEA_TFT_MAX_TEID_Rule - 1;

            }
            else {
                fromPri = MEA_OS_atoiNum(argv[2]);
                ToPri = fromPri;
                if (ToPri >= MEA_TFT_MAX_TEID_Rule) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - index %d is out of range \n",
                        __FUNCTION__, ToPri);
                    return MEA_ERROR;

                }

            }
        }
    }



    for (i = 1; i < (MEA_Uint32)argc; i++)
    {
        if (!MEA_OS_strcmp(argv[i], "-hvxlan_DL")) {
            info = MEA_CLI_EDITING_INFO_VXLAN_DL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hIPCS_DL")) {
            info = MEA_CLI_EDITING_INFO_IPCS_DL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hIPCS_UL")) {
            info = MEA_CLI_EDITING_INFO_IPCS_UL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hETHCS_DL")) {
            info = MEA_CLI_EDITING_INFO_ETHCS_DL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hETHCS_UL")) {
            info = MEA_CLI_EDITING_INFO_ETHCS_UL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hNAT")) {
            info = MEA_CLI_EDITING_INFO_NAT;
        }

        if (!MEA_OS_strcmp(argv[i], "-m")) {
            info = MEA_CLI_EDITING_INFO_MARTINI;
        }
        if (!MEA_OS_strcmp(argv[i], "-hpl")) {
            info = MEA_CLI_EDITING_INFO_PROTOLLC;
        }
        if (!MEA_OS_strcmp(argv[i], "-s")) {
            info = MEA_CLI_EDITING_INFO_STAMPING;
        }
        if (!MEA_OS_strcmp(argv[i], "-hs")) {
            info = MEA_CLI_EDITING_INFO_SESSION;
        }
        if (!MEA_OS_strcmp(argv[i], "-lmid")) {
            info = MEA_CLI_EDITING_INFO_LMID;
        }
        if (!MEA_OS_strcmp(argv[i], "-dscp")) {
            info = MEA_CLI_EDITING_INFO_DSCP;
        }
        if (!MEA_OS_strcmp(argv[i], "-hmpls")) {
            info = MEA_CLI_EDITING_INFO_MPLS;
        }
        if (!MEA_OS_strcmp(argv[i], "-hpw")) {
            info = MEA_CLI_EDITING_INFO_PW;
        }

    }

    MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
    MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
    MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */


    count = 0;
    for (UEPDN_Id = start_UEPDN_Id; UEPDN_Id <= end_UEPDN_Id; UEPDN_Id++) {
        for (priority = fromPri; priority <= ToPri; priority++) {
            if (MEA_API_UEPDN_TFT_TEID_ConfigAction_IsExist(MEA_UNIT_0, UEPDN_Id, priority, &exist) == MEA_OK)
            {
                if (!exist)
                    continue;

                
                
                MEA_OS_memset(&show_entry, 0, sizeof(show_entry));


                MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
                MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
                MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
                MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */

                
                entry.action_params = &Action_data;
                entry.Policer_Entry = &Action_sla_params_entry;
                entry.OutPorts_Entry = &Action_out_ports_tbl_entry;
                entry.EHP_Entry = &Action_Editing;


                if (MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority, &entry) != MEA_OK)
                {
                    CLI_print("Error MEA_API_Set_UEPDN_ConfigAction failed UEPDN_Id %d \n", UEPDN_Id);
                    continue;
                }
                /* set the number of editing */

                if (Action_Editing.num_of_entries > 0) {
                    Action_Editing.ehp_info = (MEA_EHP_Info_dbt*)MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt)*Action_Editing.num_of_entries);
                    if (Action_Editing.ehp_info == NULL) {
                        CLI_print("ERROR: Failed to malloc ehp data\n");
                        return MEA_OK;
                    }

                    if (MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority, &entry) != MEA_OK)
                    {
                        CLI_print("Error MEA_API_Get_UEPDN_TEID_ConfigAction failed UEPDN_Id %d \n", UEPDN_Id);
                        MEA_OS_free(Action_Editing.ehp_info);

                        return MEA_OK;
                    }

                }


                show_entry.type = MEA_ACTION_TYPE_HPM;
                show_entry.Action_Id = (MEA_Action_t)UEPDN_Id;
                show_entry.index = (MEA_Uint32)priority;
                show_entry.info = info;
                
                show_entry.Action_Editing = entry.EHP_Entry;
                
                if (count == 100)
                    count = 0;
                show_entry.count = count;

                mea_cli_Acton_Print_Editing_info(&show_entry);

                count++;
                if (Action_Editing.ehp_info) {
                    MEA_OS_free(Action_Editing.ehp_info);
                    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing));
                }

            }//IsExist

        }//index priority

    }//UE



    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_TFT_Delete_UEPDN_ConfigAction_Entry(int argc, char *argv[])
{
    
    MEA_Uint16  UEPDN_Id, start_UEPDN_Id, end_UEPDN_Id;
    MEA_Uint32 count = 0;
    MEA_Bool                        exist;
    MEA_Uint8 priority, fromPri, ToPri;


    if (argc < 3) {
        return MEA_ERROR;
    }

    if (!MEA_OS_strcmp(argv[1], "all")) {
        start_UEPDN_Id = 0;
        end_UEPDN_Id = MEA_UEPDN_MAX_ID - 1;
    }
    else {
        start_UEPDN_Id = (MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_UEPDN_Id = start_UEPDN_Id;
        UEPDN_Id = start_UEPDN_Id;

        if (UEPDN_Id >= MEA_UEPDN_MAX_ID) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  uepdn_Id is out  \n", __FUNCTION__);
            return MEA_ERROR;
        }
       

    }

    if (MEA_OS_strcmp(argv[2], "all") == 0) {
        fromPri = 0;
        ToPri = MEA_TFT_MAX_TEID_Rule - 1;

    }
    else {
        fromPri = MEA_OS_atoiNum(argv[2]);
        ToPri = fromPri;
        
        if (fromPri > 7) {
            CLI_print(
                "Error uepdn_Qos [%d] not on range 0:7 \n", argv[2]);
            return MEA_OK;
        }

    }


    count = 0;
    for (UEPDN_Id = start_UEPDN_Id; UEPDN_Id <= end_UEPDN_Id; UEPDN_Id++) {
        for (priority = fromPri; priority <= ToPri; priority++) {
            if (MEA_API_UEPDN_TFT_TEID_ConfigAction_IsExist(MEA_UNIT_0, UEPDN_Id, priority, &exist) == MEA_OK)
            {
                if (!exist)
                    continue;
                count++;
                if(MEA_API_Delete_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority) != MEA_OK){
                    CLI_print("ERROR MEA_API_Delete_UEPDN_TEID_ConfigAction (ue=%d :%d )\n", UEPDN_Id, priority);
                    return MEA_OK;
                }

            }//IsExist

        }//index priority

    }//UE
    


    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_L3_Internal_EtherType_Get(int argc, char *argv[])
{
    MEA_Uint8 MaskId;
    MEA_Uint8 from_MaskId;
    MEA_Uint8 to_MaskId;

    MEA_TFT_L3_EtheType_dbt entry;
    MEA_Uint32 count = 0;


    if (argc < 1) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_MaskId = 1;
        to_MaskId = MAT_MAX_OF_L3_Internal_EtherType - 1;

    }
    else {
        from_MaskId = MEA_OS_atoiNum(argv[1]);
        to_MaskId = MEA_OS_atoiNum(argv[1]);
        MaskId = from_MaskId;

    }

    CLI_print("-------------------------------------------\n");
    CLI_print("         TFT_MASK_Profile                  \n");
    CLI_print("-------------------------------------------\n");

    for (MaskId = from_MaskId; MaskId <= to_MaskId; MaskId++)
    {

        if (MEA_API_TFT_L3_Internal_EtherType_Get(MEA_UNIT_0, MaskId, &entry) != MEA_OK) {
            return MEA_ERROR;
        }
        if (count == 0)
        {
            CLI_print("---------------------\n");
            CLI_print(" Id    Valid  ETH    \n");
            CLI_print(" ---- ----- ------  \n");
        }

        count++;


        CLI_print(" %4d %3s 0x%04x \n", MaskId,
            MEA_STATUS_STR(entry.valid),
            entry.EtherType);


    }

    CLI_print("------------------------------------ \n");


    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_GW_L3_Internal_EtherType_Set(int argc, char *argv[])
{

    int i;
    MEA_Uint8 MaskId;
    MEA_TFT_L3_EtheType_dbt entry;
    MEA_Uint32 set_en = MEA_FALSE;


    if (argc < 2) {
        return MEA_ERROR;
    }

    MaskId = MEA_OS_atoiNum(argv[1]);
    MEA_OS_memset(&entry, 0, sizeof(entry));

    for (i = 2; i < argc; i++) {
       
        if (!MEA_OS_strcmp(argv[i], "-ethertype")) {
            entry.valid     = MEA_OS_atoiNum(argv[i + 1]);
            entry.EtherType = MEA_OS_atoiNum(argv[i + 2]);
            set_en = MEA_TRUE;
        }

    }

    if (set_en) {
        if (MEA_API_TFT_L3_Internal_EtherType_Set(MEA_UNIT_0, MaskId, &entry) != MEA_OK)
        {
            CLI_print("Error MEA_API_TFT_Prof_Mask_Create failed \n");
            return MEA_OK;
        }
    }



    if (set_en)
        CLI_print("Done:\n");

    return MEA_OK;
    
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_CLI_GW_Report_show()
{

    MEA_Uint32 i;
    MEA_Uint32 NumOfCommands;

    unsigned int add_hist;

    if (MEA_GW_SUPPORT){
        NumOfCommands = MEA_NUM_OF_ELEMENTS(MEA_CLI_GW_Report_Commands);
        for (i = 0; i < NumOfCommands; i++) {
            CLI_print("\n ## %s ##\n", MEA_CLI_GW_Report_Commands[i]);
            CLI_execCmd((char*)MEA_CLI_GW_Report_Commands[i], &add_hist);

        }
    }

    return MEA_OK;
}


MEA_Uint32 my_count = 0;
MEA_Uint32 my_Service_count = 0;

#if GW_DEBUG_TEST_CONFIG
/* Debug */
static MEA_Status MEA_CLI_GW_debug_create_IPCS(int argc, char *argv[]) {
    
    MEA_Port_t src_port;
    MEA_Uint32 IpAdress,startIp,endIp ;
    MEA_Uint32 APNVlan;
    MEA_Uint32 TEID;
    MEA_Uint32 TEID_step;
    MEA_Uint32 numofUe;


    MEA_Service_Entry_Key_dbt            service_key;
    MEA_Service_Entry_Data_dbt           service_data;
    MEA_OutPorts_Entry_dbt               service_outports;
    MEA_Policer_Entry_dbt                service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt service_editing;
    MEA_EHP_Info_dbt                     service_editing_info[1];
    MEA_Service_t                        service_id;

    MEA_Uint32                          SRV_mode;
    int i;

    //MEA_Uint32 my_count         = 0;
    //MEA_Uint32 my_Service_count = 0;


    MEA_Status retval;

    for (i = 1; i < argc; i++) {

        if (!MEA_OS_strcmp(argv[i], "-clear")) {
            my_count = 0;
            my_Service_count = 0;
            return MEA_OK;
        }
        
    }
	
    if (argc <= 7) {
        return MEA_ERROR;
    }






    src_port  = MEA_OS_atoiNum(argv[1]);
    startIp   = MEA_OS_inet_addr(argv[2]);
    APNVlan   = MEA_OS_atoiNum(argv[3]);
    TEID      = MEA_OS_atoiNum(argv[4]);
    TEID_step = MEA_OS_atoiNum(argv[5]);
    numofUe   = MEA_OS_atoiNum(argv[6]);
    SRV_mode  = MEA_OS_atoiNum(argv[7]);

    for (i = 1; i < argc; i++) {

        if (!MEA_OS_strcmp(argv[i], "-clear")) {
            my_count = 0;
            my_Service_count = 0;
        }
    }
    
    endIp = (startIp + numofUe);

    for (IpAdress = startIp; IpAdress < endIp; IpAdress++ ) {

        /*create service IPCS_DL*/

        MEA_OS_memset(&service_key, 0, sizeof(service_key));
        service_key.L2_protocol_type = 1;
        service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Ctag_protocol_IP_CS_DL;
        service_key.src_port = src_port;
        service_key.net_tag = 0xff000 | APNVlan;

        service_key.inner_netTag_from_valid = MEA_TRUE;
        service_key.inner_netTag_from_value = IpAdress;


        service_key.pri = 7;
        MEA_OS_memset(&service_data, 0, sizeof(service_data));
        service_data.SRV_mode = SRV_mode;
        service_data.DSE_forwarding_enable = MEA_TRUE;
        service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        service_data.DSE_learning_enable = MEA_TRUE;
        service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
        service_data.vpn = 3;
        

        service_data.tmId = 100;
        service_data.tmId_disable = MEA_TRUE;

        service_data.pmId = 0;
        service_data.pm_type = MEA_PM_TYPE_ALL;


        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
 

        MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
        service_policer.CIR = 0;  // 1Giga
        service_policer.CBS = 0;
        service_policer.comp = 0;

        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
        service_editing.num_of_entries = 1;
        service_editing.ehp_info = &(service_editing_info[0]);

        service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;
#if 1                
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc = 12;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc = 13;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid = MEA_TRUE;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc = 14;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid = MEA_TRUE;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc = 15;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid = MEA_TRUE;
#endif
        service_id = MEA_PLAT_GENERATE_NEW_ID;
        
        retval = MEA_API_Create_Service(MEA_UNIT_0,
            &service_key,
            &service_data,
            &service_outports,
            &service_policer,
            NULL,//&service_editing,
            &service_id);

          
        
        if (retval == MEA_OK) {
            my_Service_count++;
        }
        else {
            my_count++;
        }



        /************************************************/

        /*create service IPCS_UL*/

        /************************************************/

        MEA_OS_memset(&service_key, 0, sizeof(service_key));
        service_key.L2_protocol_type = 1;
        service_key.sub_protocol_type = MEA_PARSING_L2_Sub_Ctag_protocol_IP_CS_UL;
        service_key.src_port = src_port;
        service_key.net_tag = TEID;

        service_key.inner_netTag_from_valid = MEA_TRUE;
        service_key.inner_netTag_from_value = IpAdress;


        service_key.pri = 7;
        MEA_OS_memset(&service_data, 0, sizeof(service_data));
        service_data.SRV_mode = SRV_mode;
        service_data.DSE_forwarding_enable = MEA_TRUE;
        service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        service_data.DSE_learning_enable = MEA_TRUE;
        service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
        service_data.vpn = 3;


        service_data.tmId = 100;
        service_data.tmId_disable = MEA_TRUE;

        service_data.pmId = 0;
        service_data.pm_type = MEA_PM_TYPE_ALL;


        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));


        MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
        service_policer.CIR = 0;  // 1Giga
        service_policer.CBS = 0;
        service_policer.comp = 0;

        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
        service_editing.num_of_entries = 1;
        service_editing.ehp_info = &(service_editing_info[0]);

        service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;
#if 1                
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc = 12;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc = 13;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid = MEA_TRUE;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc = 14;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid = MEA_TRUE;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc = 15;
        service_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid = MEA_TRUE;
#endif
        service_id = MEA_PLAT_GENERATE_NEW_ID;
        retval = MEA_API_Create_Service(MEA_UNIT_0,
            &service_key,
            &service_data,
            &service_outports,
            &service_policer,
            NULL,//&service_editing,
            &service_id);

        /************************************************************************/
        /* UP vpls                                                              */
        /************************************************************************/




        /************************************************************************/
        /* Create leaf VPLS 0  my Action is default=x for all                   */
        /************************************************************************/


        if (retval == MEA_OK) {
            my_Service_count++;
        }
        else {
            my_count++;
        }



        TEID += TEID_step;






    }
        
    
     CLI_print("Number of service %d COLLISION  %d\n\n", my_Service_count, my_count);
    


    return MEA_OK;
}



static MEA_Status MEA_CLI_GW_debug_createION(int argc, char *argv[]) {

    MEA_Service_Entry_Key_dbt            service_key;
    MEA_Service_Entry_Data_dbt           service_data;
    MEA_OutPorts_Entry_dbt               service_outports;
    MEA_Policer_Entry_dbt                service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt service_editing;
    MEA_EHP_Info_dbt                     service_editing_info[1];
    MEA_Service_t                        service_id;
    MEA_Status                           retval;
    MEA_Uint32  numofError = 0;
    MEA_Port_t src_port = 104;
    MEA_Uint32                          SRV_mode = 0;
    MEA_Uint32 i,j;
    MEA_Uint32 vlanId, OuterVlan;
    MEA_Uint32 UserIp;
    MEA_Uint32 TEID;
    MEA_VPLS_UE_Pdn_dl_dbt    VPLS_UE_Pdn_entry;
    char                     srcIp_str[20];

    
    /************************************************************************/
    /* Delete All entity                                                    */
    /************************************************************************/
   
    
    
    MEA_API_VPLSi_Delete_Entry(MEA_UNIT_0, 0);
    MEA_API_VPLSi_Delete_Entry(MEA_UNIT_0, 1);
    MEA_API_Delete_all_services(MEA_UNIT_0);
    MEA_API_DeleteAll_Action(MEA_UNIT_0);
    

    

    /*  UNTAG service */
    for (i = 0; i <= 1; i++) {
    MEA_OS_memset(&service_key, 0, sizeof(service_key));
    MEA_OS_memset(&service_data, 0, sizeof(service_data));
    MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
    MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
    MEA_OS_memset(&service_editing, 0, sizeof(service_editing));

    service_key.L2_protocol_type = 0;
    service_key.sub_protocol_type = 0; 
    if(i==0)
    service_key.src_port = 100;
    if(i==1)
        service_key.src_port = 127;

    service_key.net_tag = 0xff000;  /**/
    service_key.inner_netTag_from_valid = MEA_FALSE;
    service_key.inner_netTag_from_value = 0xffffff;
    service_key.pri = 7;

    service_data.SRV_mode = SRV_mode;
    service_data.DSE_forwarding_enable = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    service_data.DSE_learning_enable = MEA_TRUE;
    service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn = 0;


    service_data.tmId = 100;
    service_data.tmId_disable = MEA_TRUE;

    service_data.pmId = 0;
    service_data.pm_type = MEA_PM_TYPE_ALL;

    MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
    MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
    service_policer.CIR = 0;  // 1Giga
    service_policer.CBS = 0;
    service_policer.comp = 0;

    MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
    MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info = &(service_editing_info[0]);

    service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
    service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
    service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;

    service_id = MEA_PLAT_GENERATE_NEW_ID;

    retval = MEA_API_Create_Service(MEA_UNIT_0,
        &service_key,
        &service_data,
        &service_outports,
        &service_policer,
        &service_editing,
        &service_id);

    if (retval != MEA_OK) {
        CLI_print("error create service \n");
        numofError++;
    }
    /************************************************************************/
}



/***************************************************************/

 
    
    vlanId = 0xe;
    for (i = 0; i <= 2; i++) {

        MEA_OS_memset(&service_key, 0, sizeof(service_key));
        MEA_OS_memset(&service_data, 0, sizeof(service_data));
        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));

        service_key.L2_protocol_type = 1;
        
        if (i == 0) {
            service_key.src_port = src_port;
            service_key.sub_protocol_type = 3;  /*IP_CS_DL*/
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = MEA_OS_inet_addr("192.168.14.211");
        }
        if (i == 1) {
            service_key.src_port = src_port;
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.sub_protocol_type = 0;  
            service_key.inner_netTag_from_valid = MEA_FALSE;
            service_key.inner_netTag_from_value = 0xffffff;
        }
        if (i == 2) {
            service_key.src_port = 127;
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.sub_protocol_type = 0;  
            service_key.inner_netTag_from_valid = MEA_FALSE;
            service_key.inner_netTag_from_value = 0xffffff;
        }

        
        service_key.pri = 7;

        service_data.SRV_mode = SRV_mode;
        service_data.DSE_forwarding_enable = MEA_TRUE;
        service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        service_data.DSE_learning_enable = MEA_TRUE;
        service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
        service_data.vpn = 0;


        service_data.tmId = 100;
        service_data.tmId_disable = MEA_TRUE;

        service_data.pmId = 0;
        service_data.pm_type = MEA_PM_TYPE_ALL;

        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
        service_policer.CIR = 0;  // 1Giga
        service_policer.CBS = 0;
        service_policer.comp = 0;

        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
        service_editing.num_of_entries = 1;
        service_editing.ehp_info = &(service_editing_info[0]);

        service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;

        service_id = MEA_PLAT_GENERATE_NEW_ID;

        retval = MEA_API_Create_Service(MEA_UNIT_0,
            &service_key,
            &service_data,
            &service_outports,
            &service_policer,
            &service_editing,
            &service_id);

        if (retval != MEA_OK) {
            CLI_print("error create service \n");
            numofError++;
        }
    
    }


    vlanId = 0xb;
    for (i = 0; i <= 2; i++) {

        MEA_OS_memset(&service_key, 0, sizeof(service_key));
        MEA_OS_memset(&service_data, 0, sizeof(service_data));
        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));

        service_key.L2_protocol_type = 1;

        if (i == 0) {
            service_key.src_port = src_port;
            service_key.sub_protocol_type = 3;  /*IP_CS_DL*/
            service_key.net_tag = 0xff000 | vlanId;
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = MEA_OS_inet_addr("192.168.11.211");
        }
        if (i == 1) {
            service_key.src_port = src_port;
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.sub_protocol_type = 0;  
            service_key.inner_netTag_from_valid = MEA_FALSE;
            service_key.inner_netTag_from_value = 0xffffff;
        }
        if (i == 2) {
            service_key.src_port = 127;
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.sub_protocol_type = 0;  /*IP_CS_DL*/
            service_key.inner_netTag_from_valid = MEA_FALSE;
            service_key.inner_netTag_from_value = 0xffffff;
        }

        

        service_key.pri = 7;

        service_data.SRV_mode = SRV_mode;
        service_data.DSE_forwarding_enable = MEA_TRUE;
        service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        service_data.DSE_learning_enable = MEA_TRUE;
        service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
        service_data.vpn = 0;


        service_data.tmId = 100;
        service_data.tmId_disable = MEA_TRUE;

        service_data.pmId = 0;
        service_data.pm_type = MEA_PM_TYPE_ALL;

        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
        service_policer.CIR = 0;  // 1Giga
        service_policer.CBS = 0;
        service_policer.comp = 0;

        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
        service_editing.num_of_entries = 1;
        service_editing.ehp_info = &(service_editing_info[0]);

        service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;

        service_id = MEA_PLAT_GENERATE_NEW_ID;

        retval = MEA_API_Create_Service(MEA_UNIT_0,
            &service_key,
            &service_data,
            &service_outports,
            &service_policer,
            NULL,//&service_editing,
            &service_id);

        if (retval != MEA_OK) {
            CLI_print("error create service \n");
            numofError++;
        }









    }


    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    vlanId = 0xfa0;
    for (i = 1; i <= 2; i++) {

        MEA_OS_memset(&service_key, 0, sizeof(service_key));
        MEA_OS_memset(&service_data, 0, sizeof(service_data));
        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));

        service_key.L2_protocol_type = 1;

    if (i == 0) {
        service_key.src_port = src_port;
        service_key.sub_protocol_type = 3;  /*IP_CS_DL*/
        service_key.inner_netTag_from_valid = MEA_TRUE;
        service_key.inner_netTag_from_value = MEA_OS_inet_addr("192.168.14.211");
    }
    if (i == 1) {
        service_key.src_port = src_port;
        service_key.net_tag = 0xff000 | vlanId;  /**/
        service_key.sub_protocol_type = 0;
        service_key.inner_netTag_from_valid = MEA_FALSE;
        service_key.inner_netTag_from_value = 0xffffff;
    }
    if (i == 2) {
        service_key.src_port = 127;
        service_key.net_tag = 0xff000 | vlanId;  /**/
        service_key.sub_protocol_type = 0;  /*IP_CS_DL*/
        service_key.inner_netTag_from_valid = MEA_FALSE;
        service_key.inner_netTag_from_value = 0xffffff;
    }



    service_key.pri = 7;

    service_data.SRV_mode = SRV_mode;
    service_data.DSE_forwarding_enable = MEA_TRUE;
    service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    service_data.DSE_learning_enable = MEA_TRUE;
    service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    service_data.vpn = 0;


    service_data.tmId = 100;
    service_data.tmId_disable = MEA_TRUE;

    service_data.pmId = 0;
    service_data.pm_type = MEA_PM_TYPE_ALL;

    MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
    MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
    service_policer.CIR = 0;  // 1Giga
    service_policer.CBS = 0;
    service_policer.comp = 0;

    MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
    MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
    service_editing.num_of_entries = 1;
    service_editing.ehp_info = &(service_editing_info[0]);

    service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
    service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
    service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
    service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;

    service_id = MEA_PLAT_GENERATE_NEW_ID;

    retval = MEA_API_Create_Service(MEA_UNIT_0,
        &service_key,
        &service_data,
        &service_outports,
        &service_policer,
        &service_editing,
        &service_id);

    if (retval != MEA_OK) {
        CLI_print("error create service \n");
        numofError++;
    }


    }

    CLI_print("VPLSi_Create_Entry 0\n");
    /* plsi create v0*/
    {
        MEA_VPLS_dbt VPLSi_entry;
        
       
        MEA_Uint16 vpls_Ins_Id = 0;

        MEA_OS_memset(&VPLSi_entry, 0, sizeof(VPLSi_entry));
        VPLSi_entry.vpn = 100;
        VPLSi_entry.SM_mode = 8;
        VPLSi_entry.max_of_fooding = 1023;

        if(MEA_API_VPLSi_Create_Entry(MEA_UNIT_0, &vpls_Ins_Id, &VPLSi_entry) != MEA_OK){
            CLI_print("error MEA_API_VPLSi_Create_Entry Id %d \n", vpls_Ins_Id);
            numofError++;
        }



    }


  /************************************************************************/
  /* create VPLS Service                                                  */
  /************************************************************************/
    vlanId = 0x459;
    OuterVlan = 0x458;

    CLI_print("create VPLS Service OuterVlan %d vlanId %d\n", OuterVlan, vlanId);
   
    for (i = 1; i <= 2; i++) {

        MEA_OS_memset(&service_key, 0, sizeof(service_key));
        MEA_OS_memset(&service_data, 0, sizeof(service_data));
        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));

        service_key.L2_protocol_type = 4;

        if (i == 0) {
            service_key.src_port = src_port;
            service_key.sub_protocol_type = 3;  /*IP_CS_DL*/
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = MEA_OS_inet_addr("192.168.14.211");
        }
        if (i == 1) {
            service_key.src_port = src_port;
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.sub_protocol_type = 0;
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = 0xfff000 | OuterVlan;
        }
        if (i == 2) {
            service_key.src_port = 24;
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.sub_protocol_type = 0;  
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = 0xfff000 | OuterVlan;
        }



        service_key.pri = 7;

        service_data.SRV_mode = SRV_mode;
        service_data.DSE_forwarding_enable = MEA_TRUE;
        service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        service_data.DSE_learning_enable = MEA_TRUE;
        service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
        service_data.vpn = 0;


        service_data.tmId = 100;
        service_data.tmId_disable = MEA_TRUE;

        service_data.pmId = 0;
        service_data.pm_type = MEA_PM_TYPE_ALL;

        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
        service_policer.CIR = 0;  // 1Giga
        service_policer.CBS = 0;
        service_policer.comp = 0;

        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
        service_editing.num_of_entries = 1;
        service_editing.ehp_info = &(service_editing_info[0]);

        service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;

        service_id = MEA_PLAT_GENERATE_NEW_ID;

        retval = MEA_API_Create_Service(MEA_UNIT_0,
            &service_key,
            &service_data,
            &service_outports,
            &service_policer,
            &service_editing,
            &service_id);

        if (retval != MEA_OK) {
            CLI_print("error create service \n");
            numofError++;
        }









    }

    CLI_print("VPLSi_Create_Entry 1\n");
    /* create vplsi 1*/
    {
        MEA_VPLS_dbt VPLSi_entry;
        

        MEA_Uint16 vpls_Ins_Id = 1;

        MEA_OS_memset(&VPLSi_entry, 0, sizeof(VPLSi_entry));
        VPLSi_entry.vpn = 100;
        VPLSi_entry.SM_mode = 8;
        VPLSi_entry.max_of_fooding = 1023;

        if (MEA_API_VPLSi_Create_Entry(MEA_UNIT_0, &vpls_Ins_Id, &VPLSi_entry) != MEA_OK) {
            CLI_print("error MEA_API_VPLSi_Create_Entry Id %d \n", vpls_Ins_Id);
            numofError++;
        }



    }

    vlanId = 0x463;
    OuterVlan = 0x462;
    CLI_print("create VPLS Service OuterVlan %d vlanId %d\n", OuterVlan, vlanId);
    for (i = 1; i <= 2; i++) {

        MEA_OS_memset(&service_key, 0, sizeof(service_key));
        MEA_OS_memset(&service_data, 0, sizeof(service_data));
        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));

        service_key.L2_protocol_type = 4;

        if (i == 0) {
            service_key.src_port = src_port;
            service_key.sub_protocol_type = 3;  /*IP_CS_DL*/
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = MEA_OS_inet_addr("192.168.14.211");
        }
        if (i == 1) {
            service_key.src_port = src_port;
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.sub_protocol_type = 0;
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = 0xfff000 | OuterVlan;
        }
        if (i == 2) {
            service_key.src_port = 24;
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.sub_protocol_type = 0;
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = 0xfff000 | OuterVlan;
        }



        service_key.pri = 7;

        service_data.SRV_mode = SRV_mode;
        service_data.DSE_forwarding_enable = MEA_TRUE;
        service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        service_data.DSE_learning_enable = MEA_TRUE;
        service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
        service_data.vpn = 0;


        service_data.tmId = 100;
        service_data.tmId_disable = MEA_TRUE;

        service_data.pmId = 0;
        service_data.pm_type = MEA_PM_TYPE_ALL;

        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
        service_policer.CIR = 0;  // 1Giga
        service_policer.CBS = 0;
        service_policer.comp = 0;

        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
        service_editing.num_of_entries = 1;
        service_editing.ehp_info = &(service_editing_info[0]);

        service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;

        service_id = MEA_PLAT_GENERATE_NEW_ID;

        retval = MEA_API_Create_Service(MEA_UNIT_0,
            &service_key,
            &service_data,
            &service_outports,
            &service_policer,
            &service_editing,
            &service_id);

        if (retval != MEA_OK) {
            CLI_print("error create service \n");
            numofError++;
        }









    }


    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    TEID = 2;
    UserIp = MEA_OS_inet_addr("10.40.0.10");
vlanId = 4000;
OuterVlan = 1112;
for (j = 1; j < 1024; j++) {
    for (i = 0; i <= 2; i++) {

        MEA_OS_memset(&service_key, 0, sizeof(service_key));
        MEA_OS_memset(&service_data, 0, sizeof(service_data));
        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));



        if (i == 0) {
            service_key.src_port = src_port;
            service_key.L2_protocol_type = 1;
            service_key.net_tag = 0xff000 | vlanId;  /**/
            service_key.sub_protocol_type = 3;  /*IP_CS_DL*/
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = UserIp;
            //CLI_print("create VPLS Service IP_CS_DL srcport %d vlan=%8d  IPv4: ", service_key.src_port,vlanId );
            
            MEA_OS_inet_ntoa(MEA_OS_htonl(UserIp), srcIp_str, sizeof(srcIp_str));
           // CLI_print("IPv4:%s \n", srcIp_str);

        }
        if (i == 1) {
            service_key.src_port = src_port;
            service_key.L2_protocol_type = 1;
            service_key.net_tag = TEID;  /*IP_CS_UL*/
            service_key.sub_protocol_type = 7;
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value =  UserIp;
            //CLI_print("create VPLS Service IP_CS_UL srcport %d TEID=%8d  IPv4: ", service_key.src_port, TEID);

            MEA_OS_inet_ntoa(MEA_OS_htonl(UserIp), srcIp_str, sizeof(srcIp_str));
            //CLI_print("IPv4:%s \n", srcIp_str);
        }
        if (i == 2) {
            service_key.src_port = src_port;
            service_key.L2_protocol_type = 1;
            service_key.net_tag = TEID;  /*UL_VPWS*/
            service_key.sub_protocol_type = 6;
            service_key.inner_netTag_from_valid = MEA_TRUE;
            service_key.inner_netTag_from_value = 0xfff000 | OuterVlan;
            //CLI_print("create VPLS Service IP_CS_UL srcport %d TEID=%8d  IPv4: ", service_key.src_port, TEID);

            MEA_OS_inet_ntoa(MEA_OS_htonl(UserIp), srcIp_str, sizeof(srcIp_str));
            //CLI_print("IPv4:%s \n", srcIp_str);

        }




        service_key.pri = 7;

        service_data.SRV_mode = SRV_mode;
        service_data.DSE_forwarding_enable = MEA_TRUE;
        service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        service_data.DSE_learning_enable = MEA_TRUE;
        service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
        service_data.vpn = 0;


        service_data.tmId = 100;
        service_data.tmId_disable = MEA_TRUE;

        service_data.pmId = 0;
        service_data.pm_type = MEA_PM_TYPE_ALL;

        MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
        MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
        service_policer.CIR = 0;  // 1Giga
        service_policer.CBS = 0;
        service_policer.comp = 0;

        MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
        MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
        service_editing.num_of_entries = 1;
        service_editing.ehp_info = &(service_editing_info[0]);

        service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
        service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
        service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;

        service_id = MEA_PLAT_GENERATE_NEW_ID;

        retval = MEA_API_Create_Service(MEA_UNIT_0,
            &service_key,
            &service_data,
            &service_outports,
            &service_policer,
            &service_editing,
            &service_id);

        if (retval != MEA_OK) {
            CLI_print("error create service \n");
            numofError++;
        }


    } // endfor
        MEA_OS_memset(&VPLS_UE_Pdn_entry, 0, sizeof(VPLS_UE_Pdn_entry));
        VPLS_UE_Pdn_entry.ActionId = 0;
        VPLS_UE_Pdn_entry.vpn = 100;
        /*create leaf*/
        if(MEA_API_VPLSi_UE_PDN_DL_Create_Entry(MEA_UNIT_0, 0, j, &VPLS_UE_Pdn_entry) != MEA_OK){
            CLI_print("error MEA_API_VPLSi_UE_PDN_DL_Create_Entry vplsIns0 %d \n",j);
            return MEA_ERROR;
        }

        TEID += 2;
        if (UserIp + 1 == 255) {
            UserIp += 3;
        }
        else {
            UserIp += 1;
        }



    }//

/************************************************************************/
/*                                                                      */
/************************************************************************/
    vlanId = 4000;
    OuterVlan = 1122;
    for (j = 1; j < 1024; j++) {
        for (i = 0; i <= 2; i++) {

            MEA_OS_memset(&service_key, 0, sizeof(service_key));
            MEA_OS_memset(&service_data, 0, sizeof(service_data));
            MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
            MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
            MEA_OS_memset(&service_editing, 0, sizeof(service_editing));



            if (i == 0) {
                service_key.src_port = src_port;
                service_key.L2_protocol_type = 1;
                service_key.net_tag = 0xff000 | vlanId;  /**/
                service_key.sub_protocol_type = 3;  /*IP_CS_DL*/
                service_key.inner_netTag_from_valid = MEA_TRUE;
                service_key.inner_netTag_from_value = UserIp;
            }
            if (i == 1) {
                service_key.src_port = src_port;
                service_key.L2_protocol_type = 1;
                service_key.net_tag = TEID;  /*IP_CS_UL*/
                service_key.sub_protocol_type = 7;
                service_key.inner_netTag_from_valid = MEA_TRUE;
                service_key.inner_netTag_from_value = 0xfff000 | UserIp;
            }
            if (i == 2) {
                service_key.src_port = src_port;
                service_key.L2_protocol_type = 1;
                service_key.net_tag = TEID;  /*UL_VPWS*/
                service_key.sub_protocol_type = 6;
                service_key.inner_netTag_from_valid = MEA_TRUE;
                service_key.inner_netTag_from_value = 0xfff000 | OuterVlan;
            }




            service_key.pri = 7;

            service_data.SRV_mode = SRV_mode;
            service_data.DSE_forwarding_enable = MEA_TRUE;
            service_data.DSE_forwarding_key_type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
            service_data.DSE_learning_enable = MEA_TRUE;
            service_data.DSE_learning_key_type = MEA_SE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
            service_data.vpn = 0;


            service_data.tmId = 100;
            service_data.tmId_disable = MEA_TRUE;

            service_data.pmId = 0;
            service_data.pm_type = MEA_PM_TYPE_ALL;

            MEA_OS_memset(&service_outports, 0, sizeof(service_outports));
            MEA_OS_memset(&service_policer, 0, sizeof(service_policer));
            service_policer.CIR = 0;  // 1Giga
            service_policer.CBS = 0;
            service_policer.comp = 0;

            MEA_OS_memset(&service_editing_info, 0, sizeof(service_editing_info));
            MEA_OS_memset(&service_editing, 0, sizeof(service_editing));
            service_editing.num_of_entries = 1;
            service_editing.ehp_info = &(service_editing_info[0]);

            service_editing.ehp_info[0].ehp_data.eth_info.val.all = 0;
            service_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
            service_editing.ehp_info[0].ehp_data.eth_info.stamp_color = MEA_FALSE;
            service_editing.ehp_info[0].ehp_data.eth_info.stamp_priority = MEA_FALSE;
            service_editing.ehp_info[0].ehp_data.atm_info.val.all = 0;
            service_editing.ehp_info[0].ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
            service_editing.ehp_info[0].ehp_data.atm_info.stamp_color = MEA_FALSE;
            service_editing.ehp_info[0].ehp_data.atm_info.stamp_priority = MEA_FALSE;

            service_id = MEA_PLAT_GENERATE_NEW_ID;

            retval = MEA_API_Create_Service(MEA_UNIT_0,
                &service_key,
                &service_data,
                &service_outports,
                &service_policer,
                &service_editing,
                &service_id);

            if (retval != MEA_OK) {
                CLI_print("error create service \n");
                numofError++;
            }


        } // endfor


        MEA_OS_memset(&VPLS_UE_Pdn_entry, 0, sizeof(VPLS_UE_Pdn_entry));
        VPLS_UE_Pdn_entry.ActionId = 0;
        VPLS_UE_Pdn_entry.vpn = 100;
        /*create leaf*/
        if (MEA_API_VPLSi_UE_PDN_DL_Create_Entry(MEA_UNIT_0, 1, j, &VPLS_UE_Pdn_entry) != MEA_OK) {
            CLI_print("error MEA_API_VPLSi_UE_PDN_DL_Create_Entry vplsIns0 %d \n", j);
            return MEA_ERROR;
        }

        TEID += 2;
        if (UserIp + 1 == 255) {
            UserIp += 3;
        }
        else {
            UserIp += 1;
        }



    }//


    CLI_print("Done: numOf Error %d \n", numofError);
    return MEA_OK;
}
#endif


MEA_Status MEA_CLI_GW_AddDebugCmds(void)
{

    
    CLI_defineCmd("MEA GW global set",
        (CmdFunc)MEA_CLI_GW_Global_Set,
        "set   Global parameters \n",
        "set    global set \n"
        "---------------------------------------------------    \n"
        "    -Host_MAC      <My host Mac Address>               \n"
        "    -PGW_MAC       <My host Mac Address>               \n"
        "    -APN_vlan      <index> <vlanId>                    \n"
        "    -APN_Ipv6_mask  <index> <mask>                     \n"
        "    -APN_Ipv6       <index> <Ipv6msb>                  \n"
        "    -DHCP_IP       <index> <Ip Address>                \n"
        "    -Infra_VLAN    <vlanId>                            \n"
        "    -Infra_VLAN_Ipv6_mask <mask>                       \n"
        "    -SGW_IP   	    <index> <Ip Address>                \n"
        "    -PDN_IP        <index> <Ip Address>               \n"
        "    -PGW_U_IP      <Ip Address>                        \n"
        "    -MNG_VLAN    <vlanId>                              \n"
        "    -Conf_D_IP     <Ip Address>                        \n"
        "    -MME_IP        <Ip Address>                        \n"
        "    -gw_ttl        <TTL>                               \n"
        "    -DSCP          <dscpVal>                           \n"
        "    -default_sid   <action><sid>                       \n"
        "    -local_mng_ip   <Ip Address>                      \n"
        "    -default_sid_IPCS <action><sid><srcport>          \n"
        "    -learn_enb_vpn   <vpnId>                          \n"
        "    -vxlan_L4_dst_port <value>                         \n"
        "    -vxlan_upto_vlan   <vlan-id>                       \n"
        "------------------------------------------------------\n"

        " -default_sid   (default service for te_id)          \n"
        "         <action>      - Discard-0, 2-Default SID \n"
        "         <sid> - service id                       \n"
        " -default_sid_ETHCS_UL    (default service for ETHCS_UL )    \n"
        "         <action>      - Discard-0, 2-Default SID \n"
        "         <sid> - service id                       \n"
        


        "Examples: \n"
        "       set -Host_MAC 00:01:02:03:04:05 \n"
        "       set -APN_vlan 0 10  -PGW_U_IP 100.1.2.3\n"

        "             \n");

    CLI_defineCmd("MEA GW global show",
        (CmdFunc)MEA_CLI_GW_Global_Get,
        "show   Global parameters \n",
        "show    global show \n"
        "---------------------------------------------------\n"
        "---------------------------------------------------\n"
        "Examples: \n"
        "       show all \n"

        "             \n");
    
    
    
    


#if GW_DEBUG_TEST_CONFIG
        
            CLI_defineCmd("MEA GW debug createION",
                (CmdFunc)MEA_CLI_GW_debug_createION
                ,
                "debug createION\n",
                "debug createION \n");
       

            CLI_defineCmd("MEA GW debug create_IPCS",
                (CmdFunc)MEA_CLI_GW_debug_create_IPCS
                ,
                "debug create_IPCS\n",
                "debug create_IPCS  \n"

                "    src_port  \n"
                "    UeIp      \n"
                "    APNVlan   \n"
                "    TEID      \n"
                "    TEID_step \n"
                "     numofUe  \n"
                "     SRV_mode  \n"


                "---------------------------------------------------\n"
                "Examples: \n"
                "       create_IPCS  104 172.30.3.120 101 1 1 2000  2   \n"
                );
#endif
            /************************************************************************/
            /************************************************************************/
            CLI_defineCmd("MEA GW TFT_UEPDN set create",
                (CmdFunc)MEA_CLI_GW_TFT_Create_UEPDN_ConfigAction,
                "create TPT_UEPDN <ue><index>   \n",
                meacli_LongHelp_Create_Action);


            CLI_defineCmd("MEA GW TFT_UEPDN set modify",
                (CmdFunc)MEA_CLI_GW_TFT_Set_UEPDN_ConfigAction,
                "modify TFT_UEPDN <ue><index>   \n",
                meacli_LongHelp_Create_Action
                );

            CLI_defineCmd("MEA GW TFT_UEPDN set delete",
                (CmdFunc)MEA_CLI_GW_TFT_Delete_UEPDN_ConfigAction_Entry,
                " delete TFT_UEPDN <ue><index>   \n",
                " delete TFT_UEPDN <ue><index> \n"
                "---------------------------------------------------\n"
                "Examples: \n"
                "       TFT_UEPDN delete All All       \n"
                "       TFT_UEPDN delete 10 7         \n"
                );


            
                CLI_defineCmd("MEA GW TFT_UEPDN show entry",
                    (CmdFunc)MEA_CLI_GW_TFT_Get_UEPDN_ConfigAction_Entry,
                    "TFT_UEPDN show entry  \n",
                    "TFT_UEPDN show entry  \n"

                    "    <UEPDN id>  value   \n"
                    "    <index> 0:7    \n"
                    
                    "---------------------------------------------------\n"
                    "Examples: \n"
                    "       TFT_UEPDN show entry All All       \n"
                    "       TFT_UEPDN show entry 10 7      \n"
                    );

                CLI_defineCmd("MEA GW TFT_UEPDN show editing",
                    (CmdFunc)MEA_CLI_GW_TFT_Get_UEPDN_ConfigAction_editing,
                    "TFT_UEPDN show editing  \n",
                    "TFT_UEPDN show  \n"

                    "    <UEPDN id>  value   \n"
                    "    <index> 0:7    \n"

                    "---------------------------------------------------\n"
                    "Examples: \n"
                    "       TFT_UEPDN show editing All All       \n"
                    "       TFT_UEPDN show editing 10 7      \n"
                    );


                

                    CLI_defineCmd("MEA GW HPM l3_eth set",
                        (CmdFunc)MEA_CLI_GW_L3_Internal_EtherType_Set,
                        "create   HPM\n",
                        "create   HPM  \n"
                        "    <index>  value (4:7)\n"
                        "    -ethertype <valid><ethetype> \n"
                        "---------------------------------------------------\n"
                        "Examples: \n"
                        "        HPM l3_eth set  4  -ethertype 0 0x0    \n"
                        "       HPM l3_eth set   5  -ethertype 1 0x0806 \n"
                        );

                CLI_defineCmd("MEA GW HPM l3_eth show",
                    (CmdFunc)MEA_CLI_GW_L3_Internal_EtherType_Get,
                    "create   HPM\n",
                    "create   HPM  \n"
                    "    <index/all>  \n"
                    
                    "---------------------------------------------------\n"
                    "Examples: \n"
                    "        HPM l3_eth show  1     \n"
                    "       HPM l3_eth show   all   \n"
                    );
                
       /*---------------------------------------------------------------*/
        CLI_defineCmd("MEA GW HPM prof create",
            (CmdFunc)MEA_CLI_GW_TFT_Create_Entry,
            "create   HPM\n",
            "create   HPM  \n"
            "    <tft_Id/auto>  value (0:63)\n"
            "    <-rule_type> 0:7 \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "        HPM prof create 1  \n"
            "       HPM prof create 2  -rule_type 7\n"
            );

        CLI_defineCmd("MEA GW HPM prof delete",
            (CmdFunc)MEA_CLI_GW_TFT_Delete_Entry,
            "delete   HPM\n",
            "delete   HPM  \n"
            "    <Id>  value (0:63)\n"
           
            "---------------------------------------------------\n"
            "Examples: \n"
            "       HPM prof delete 1  \n"
            "       HPM prof delete 2  \n"
            );

        CLI_defineCmd("MEA GW HPM prof set",
            (CmdFunc)MEA_CLI_GW_TFT_Set_EntryData,
            " HPM prof set   n",
            " HPM prof add   \n"
            "    <Id>  value (0:63)\n"
            "    <index>  (0:31)\n"
            
            "-------------------------------------------\n"
            "     type  <0 - disable 1-noIp 2-Ipv4 3-DestIpv6 4-srcIpv6  >\n"
            "     Priority_Rule   0:31  \n"
            "     QoS_Index       0:7 \n"
            "------------------------------\n"
            "     L2_pri <valid><l2Pri (0xff)>\n"
            "     L3_ethtype <int-ethtype>\n"
            "     DSCP  <valid><from DSCP><end_DSCP>\n"
            "     IP_protocol <valid><IP_protocol>\n"
            "     src_ipv4 <valid><SrcIp><mask>\n"
            "     dest_ipv4 <valid><DesIp><mask>\n"
            "     ipv6 <valid><DesIp><ipv6mask>\n"
            "     flow_label <value>\n"
            "     L4_srcport <valid><from_Src_prot><end_Src_port>\n"
            "     L4_destport <valid> <from_Dest_port><end_Dest_port>\n"
           
            "---------------------------------------------------\n"
            
            "      L2_pri   (802.p) \n"
            "           <valid>  0-disable 1-enable \n"
            "           <L2Pri>  <value>  0 : 0xff\n"
            "     L3_ethtype  inner ethertype \n"
            "                <value> 0:7\n"
            "---------------------------------------------------\n"
            "     DSCP  (L3 DSCP min max) \n"
            "          <valid> 0-disable 1-enable\n"
            "          <fromdscp> 0:63\n"
            "          <end_dscp>  0:63\n"
            "---------------------------------------------------\n"
            "     IP_protocol (IP_protocol parameters) \n"
            "             <valid><IP_protocol> \n"
            "---------------------------------------------------\n"
            "     src_ipv4 (source IPv4 parameters) \n"
            "            <valid> 0-disable 1-enable \n"
            "            <SrcIp>  Ip Address \n"
            "            <mask>  0:32 \n"
            "---------------------------------------------------\n"
            "     dest_ipv4 (destination IPv4 parameters)  \n"
            "              <valid> 0-disable 1-enable \n"
            "              <DesIp> Ip Address         \n"
            "              <mask> 0:32                \n"
            "------------------------------------------------------\n"
            "         Ipv6  (Dest/srcIP IPv6 parameters )          \n"
            "              <valid> 0-disable 1-enable              \n"
            "              <IpV6> Ipv6 Address x:x:x:x:x:x:x:x    \n"
            "              <mask> 0:128                            \n"
            "       flow_label  (Ipv6 flow label)                   \n"
            "              <flow_label>                             \n"
            "-------------------------------------------------------\n"
            "        L4_srcport                      \n"  
            "        <valid>  0-disable 1-enable     \n"
            "        <from_Src_prot><end_Src_port>  (0- 65,536) \n"
            "---------------------------------------------------\n"
            "       L4_destport                     \n"
            "        <from_Dest_port><end_Dest_port> (0-65,536)\n"
            
            "---------------------------------------------------\n"
            "Examples: \n"
            "    HPM prof set 0 1    L2_pri 1 0x7  DSCP 1 24 50  Priority_Rule 31 QoS_Index 4 type 2\n"
            "    HPM prof set 0 2    DSCP 1 0 63 src_ipv4 1 100.0.0.138 32  Priority_Rule 1 QoS_Index 7 type 2 \n"
            "    HPM prof set 0 2   dest_ipv4 1 100.0.0.138 24  type 2\n"
            "    HPM prof set 0 2   type 0  \n"
            "    HPM prof set 1 7  IP_protocol 1 0x11  ipv6 1 fcfb:dd02:0304:0506::0  64 flow_lable 0x12345  L4_srcport 1 1000 2000  L4_destport 1 312 555  DSCP 1 0 63   Priority_Rule 10  QoS_Index 1 type 3\n"
            "---------------------------------------------------\n"
            );

        CLI_defineCmd("MEA GW HPM prof show",
            (CmdFunc)MEA_CLI_GW_TFT_Get_EntryData,
            "show   HPM prof\n",
            "show   HPM prof \n"
            "    <tft_Id>  all/Id\n"
            "    <priority> All/priority \n"
            
            "---------------------------------------------------\n"
            "Examples: \n"
            "       HPM prof show 1 all all   \n"
            "       HPM prof show 2 all all  \n"
            );



        /*------------------------------------------------------------------------*/
        CLI_defineCmd("MEA GW HPM mask_prof create",
            (CmdFunc)MEA_CLI_GW_TFT_Create_mask_prof,
            " Create HPM mask_prof\n",
            " Create HPM mask_prof \n"
            "    <maskId/auto>  Id\n"
            "       "
            "---------------------------------------------------\n"
            "Examples: \n"
            "       HPM mask_prof create auto  \n"
            "       HPM mask_prof create 10    \n"
            );
        CLI_defineCmd("MEA GW HPM mask_prof delete",
            (CmdFunc)MEA_CLI_GW_TFT_Delete_mask_prof,
            "delete   HPM mask_prof\n",
            "delete   HPM mask_prof \n"
            "    <Id>  Mask prof id \n"

            "---------------------------------------------------\n"
            "Examples: \n"
            "       HPM mask_prof delete all  \n"
            "       HPM mask_prof delete 2  \n"
            );
        
        CLI_defineCmd("MEA GW HPM mask_prof set",
            (CmdFunc)MEA_CLI_GW_TFT_Set_mask_prof,
            "set   HPM mask_prof\n",
            "set   HPM mask_prof \n"
            "    <maskId>  Id\n"
            "-----------------"
            " option:\n"
            " -rule_type <val> 1:7\n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       HPM mask_prof set 1  -mask 0x0000ff  \n"
            "       HPM mask_prof set 10 -mask 0xffffffff  -rule_type 7 \n"
            );

        CLI_defineCmd("MEA GW HPM mask_prof show",
            (CmdFunc)MEA_CLI_GW_TFT_Get_mask_prof,
            "show   HPM mask_prof\n",
            "show   HPM mask_prof \n"
            "    <tft_Id>  all/Id\n"

            "---------------------------------------------------\n"
            "Examples: \n"
            "       HPM mask_prof show 1    \n"
            "       HPM mask_prof show all   \n"
            );
        /*------------------------------------------------------------------------*/

        CLI_defineCmd("MEA GW VPLSi create",
            (CmdFunc)MEA_CLI_GW_VPLSi_Create_Entry,
            "create   VPLSi\n",
            "create   VPLSi  \n"
            "    \n"
            "    -f <vplsi_Id>\n"
            "    -max_leaf <number>\n"
            "    -vpn <id>\n"
            "    -SM_X <value> \n"
          
            "           MODE_SM1   = 1,     \n"
            "           MODE_SM1a  = 2,    \n"
            "           MODE_SM2   = 3,     \n"
            "           MODE_SM2a  = 4,   \n"
            "           MODE_SM2b  = 5,  \n"
            "           MODE_SM3   = 6,   \n"
            "           MODE_SM4   = 7,   \n"
            "           MODE_SM5   = 8,   \n"
            "           MODE_SM5a  = 9,   \n"
            "           MODE_SM3a  = 10,  \n"



            "---------------------------------------------------\n"
            "Examples: \n"
            "       VPLSi create -max_leaf 16 -vpn 10 -SM_X 1\n"
           );
        
            CLI_defineCmd("MEA GW VPLSi addGroup",
            (CmdFunc)MEA_CLI_GW_VPLSi_add_Group,
                "addGroup   VPLSi  (add the one group) \n",
                "addGroup   VPLSi  <id> \n"
                "---------------------------------------------------\n"
                "Examples: \n"
                "       VPLSi addGroup 1\n"
            );


        CLI_defineCmd("MEA GW VPLSi show",
            (CmdFunc)MEA_CLI_GW_VPLSi_Show_Entry,
            "show   VPLSi\n",
            "show   VPLSi  <id> \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       VPLSi show 1\n"
            );

        CLI_defineCmd("MEA GW VPLSi delete",
            (CmdFunc)MEA_CLI_GW_VPLSi_Delete_Entry,
            "delete   VPLSi\n",
            "delete   VPLSi  <id> \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       VPLSi delete 1\n"
            );
        /*********************************************************************/

        CLI_defineCmd("MEA GW VPLSi leaf add",
            (CmdFunc)MEA_CLI_GW_VPLSi_UE_PDN_DL_Create_Entry,
            "VPLSi leaf add  <vplsiId> <leafId>\n",
            "VPLSi leaf add  <vplsiId> <leafId> \n"
            "---------------------------------------------------\n"
            "option \n"
            "   -action <Id> \n"
            "   -access_port <id 1:1023>\n"

            " option TFT mode \n"
            "-----------------------\n"
            "  TFT <enable> \n"
            "   -uepdn <valid> <id>\n"
            "   -HPM <valid><profId><maskprofId> \n"
            "  -Pm <Id>\n"
            "  -frag_IP  <en_fragment Ip> <en_TUNNEL> <frag_ex_int> <target_mtu_prof> \n"
            "-----------------------------------------------------\n"
            "    Editing parameters\n"
            "-----------------------\n"
            "    [-hType <EditType>]\n"
            "    [-hETHCS_DL <valid> <DA_eNB><Ip dst enb><TE_ID><IP ue><Id - of -sgw_ip>\n"
            "---------------------------------------------------------------------------- \n"
            "  policer parameters\n"
            "-----------------------\n"
            "    [-flow_pol (Ingress flow policer) <ProfileId>\n"
            "    [-Tm <Id>]\n"
            "    [-policer <CIR> <EIR> <CBS> <EBS> <coupling> <col_aware> <comp>] \n"
            
            "\n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "    VPLSi add 0 0 -action 4200 -access_port 1 \n"
            "    VPLSi leaf add 0 0 -access_port 1  TFT 1 -uepdn 1 1 -HPM 1 10 200 -hType 32 -hETHCS_DL 1 B0:12:34:56:78:9a 171.11.81.1 100 10.0.0.1 0 -Pm  100 -Tm 0 -policer 512000 0 32000 0 0 0 20\n"
            "\n"
            );

        CLI_defineCmd("MEA GW VPLSi leaf delete",
            (CmdFunc)MEA_CLI_GW_VPLSi_UE_PDN_DL_Delete_Entry,
            "delete   VPLSi\n",
            "delete   VPLSi  <vplsiId> <UE_pdn_Id> \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       VPLSi leaf delete 1 1\n"
            );

        CLI_defineCmd("MEA GW VPLSi leaf show ",
            (CmdFunc)MEA_CLI_GW_VPLSi_UE_PDN_DL_Show_Entry,
            "show   VPLSi\n",
            "show   VPLSi  <id><All/UE pdnId> \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       VPLSi leaf show 1 all\n"
            );

        CLI_defineCmd("MEA GW VPLSi Handover set ",
            (CmdFunc)MEA_CLI_GW_VPLSi_Set_Handover_Entry,
            "set  VPLSi Handover\n",
            "set   VPLSi Handover  <index> <valid><vpn><access port><en_vp><vp> \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       Handover set 0 1 100 2 1 100\n"
            );

        CLI_defineCmd("MEA GW VPLSi Handover get ",
            (CmdFunc)MEA_CLI_GW_VPLSi_Get_Handover_Entry,
            "show   VPLSi\n",
            "show   VPLSi  <id><All/UE pdnId> \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       VPLSi leaf show 1 all\n"
            );
        

        /************************************************************************/
        /*                                                                      */
        /************************************************************************/

            CLI_defineCmd("MEA GW event show",
                (CmdFunc)MEA_CLI_GW_Show_Enent,
            "show   event show\n",
            "show    event  \n"
            "   GW event  \n"

            "---------------------------------------------------\n"
            "Examples: \n"
            "       event show all \n"
            "             \n");

            CLI_defineCmd("MEA GW rootfilter show",
                (CmdFunc)MEA_CLI_GW_Get_RootFilter,
            "show   rootfilter  classification\n",
            "show    rootfilter  \n"
            "   rootfilter \n"

            "---------------------------------------------------\n"
            "Examples: \n"
            "       rootfilter show all \n"
            "             \n");


        CLI_defineCmd("MEA GW rootfilter set",
            (CmdFunc)MEA_CLI_GW_Set_RootFilter,
            "set   rootfilter enable/disable \n",
            "set    rootfilter set \n"
            "   rootfilter set <filterType> <enable/disable>        \n"
            "         <filterType>                                  \n"
            "               DHCP_REALY_REPLY = 0,                   \n"
            "               DHCP_IP_CS_REQUEST = 1,                 \n"
            "               DHCP_ETH_CS_REQUEST = 2,                \n"
            "               DL_IPCS = 3,                            \n"
            "               DL_VPWS_QIQ = 4,                        \n"
            "               DL_VPWS_VID = 5,                        \n"
            "               UL_VPWS = 6,                            \n"
            "               UL_IPCS = 7,                            \n"
            "               MNG_NCS = 8,                            \n"
            "               CONTROL_PLANE_AAA = 9,                  \n"
            "               INFRA_ARP_REQUST_TO_CP = 10,            \n"
            "               INFRA_ARP_RESPON_TO_CP = 11,            \n"
            "               INFRA_ARP_REQUST_TO_UE = 12,            \n"
            "               INFRA_ARP_REQUST_TO_MNG = 13,           \n"
            "               INFRA_ARP_RESPONE_TO_MNG = 14,          \n"
            "               CONTROL_PLANE_SCTP = 15,                \n"
            "               CONTROL_PLANE_GTP = 16,                 \n"
            "      <enable/disable>  1-enable 0-disable             \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       rootfilter set 0 1 \n"
            "             \n");


        

        CLI_defineCmd("MEA GW TE_ID mode set ",
            (CmdFunc)MEA_CLI_GW_Global_Set_TE_ID_Mode,
            "set   TE_ID Mode type \n",
            "set    TE_ID mode set \n"
            "--------------------------------------------------\n"
            "  TE_ID mode set <id> <valid>                     \n"
            "            valid 0-disable 1-enable              \n"
           
            "  -learn   0-disable 1-enable                   \n"
            "  -maskIpv6  <value>\n"  
            "                       0 � 64 MSB bits \n"
            "                       1 � 63 MSB bits \n"
            "                       2 � 62 MSB bits \n"
            "                       3 � 61 MSB bits \n"
            "                       4 � 60 MSB bits \n"
            "                       5 � 59 MSB bits \n"

            "---------------------------------------------------\n"
            "Examples: \n"
            "       TE_ID mode set 1 1 -tag 0  -learn 1 \n"
           
            "             \n");
        CLI_defineCmd("MEA GW TE_ID mode show ",
            (CmdFunc)MEA_CLI_GW_Global_Get_TE_ID_Mode,
            "get   TE_ID Mode type \n",
            "get    TE_ID mode show \n"
            "--------------------------------------------------\n"
            "  TE_ID mode get <id/All>                         \n"
            

            "---------------------------------------------------\n"
            "Examples: \n"
            "       TE_ID mode show 10  \n"
            "       TE_ID mode show all  \n"

            "             \n");
        
        


    


        return MEA_OK;
}

