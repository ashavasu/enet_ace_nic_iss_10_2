/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_Interface_drv.h"
#include "mea_cli_Interface.h"



MEA_Status MEA_CLI_Interface_Interlaken_Set(int argc, char *argv[])
{
    MEA_Interlaken_Entry_dbt  entry;
    MEA_Interface_t                 port;
    int i;
    int num_of_params;


    if (argc < 3) {
        return MEA_ERROR;
    }

    port = (MEA_Port_t)MEA_OS_atoiNum(argv[1]);


    if(MEA_API_Get_Interface_Interlaken_Entry(MEA_UNIT_0,port,&entry)!=MEA_OK){
        CLI_print("Error MEA_API_Get_Interface_Interlaken_Entry failed\n");
        return MEA_OK;
    }

    for(i=(2); i<argc ;i++){

        if(!MEA_OS_strcmp(argv[i],"-rx_ctl_reset")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_rx_reset_n = MEA_OS_atoiNum(argv[i+1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-tx_ctl_reset")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_tx_reset_n = MEA_OS_atoiNum(argv[i+1]);
        }

        


        if(!MEA_OS_strcmp(argv[i],"-rx_resync")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_rx_force_resync = MEA_OS_atoiNum(argv[i + 1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-ctl_fc")){  //RX
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_fc_enable = MEA_OS_atoiNum(argv[i + 1]);
        }
		if(!MEA_OS_strcmp(argv[i],"-ctl_tx_fc")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_tx_fc_enable = MEA_OS_atoiNum(argv[i + 1]);
        }
        

        if(!MEA_OS_strcmp(argv[i],"-burst")){ 
            num_of_params=2;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_tx_burstshort = MEA_OS_atoiNum(argv[i + 1]);
            entry.ctl_tx_burstmax = MEA_OS_atoiNum(argv[i + 2]);
        
        }
        if(!MEA_OS_strcmp(argv[i],"-lanestat")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_tx_diagword_lanestat = MEA_OS_atoiNum(argv[i + 1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-tx_enable")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_tx_enable = MEA_OS_atoiNum(argv[i + 1]);
         }
        if(!MEA_OS_strcmp(argv[i],"-fc_callen")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_tx_fc_callen = MEA_OS_atoiNum(argv[i + 1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-tx_rdyout_thresh")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_tx_rdyout_thresh = MEA_OS_atoiNum(argv[i + 1]);
        }

        if(!MEA_OS_strcmp(argv[i],"-tx_rlim")){ 
            num_of_params=4;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.ctl_tx_rlim_enable = MEA_OS_atoiNum(argv[i + 1]);
            entry.ctl_tx_rlim_delta  = MEA_OS_atoiNum(argv[i + 2]);
            entry.ctl_tx_rlim_intv   = MEA_OS_atoiNum(argv[i + 3]);
            entry.ctl_tx_rlim_max    = MEA_OS_atoiNum(argv[i + 4]);
        }
        if(!MEA_OS_strcmp(argv[i],"-serdes_reset")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.rx_serdes_resetn = MEA_OS_atoiNum(argv[i + 1]);
        }
        
        if(!MEA_OS_strcmp(argv[i],"-lb_mac")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.lbus = MEA_OS_atoiNum(argv[i + 1]);


        }
        
        
        if(!MEA_OS_strcmp(argv[i],"-loopback")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
          
            entry.loopback_type = MEA_OS_atoiNum(argv[i + 1]);
            
        }
    
    
    
    }
    
    if(MEA_API_Set_Interface_Interlaken_Entry(MEA_UNIT_0,port,&entry)!=MEA_OK){
        CLI_print("Error MEA_API_Set_Interface_Interlaken_Entry failed port %d\n",port);
        return MEA_OK;
    }


    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_Interlaken_Get(int argc, char *argv[])
{
    MEA_Interface_t port,start_port,end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count=0;
    MEA_Uint16 flag_typeshow = 0;

    MEA_Interlaken_Entry_dbt entry;


    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if((up_ptr=strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port   = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1],"all") == 0) {
            start_port=0;
            end_port=MEA_INTERFACE_MAX_ID-1;
        } else {
            port=start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_Interlaken,MEA_TRUE/*silent*/)==MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not RXAUI or not valid\n",port);
                return MEA_OK;
            }

        }
    }

    if(argc > 2){
      if (MEA_OS_strcmp(argv[2],"-tx_rlim") == 0) {
        flag_typeshow=1;
      }
      if (MEA_OS_strcmp(argv[2],"-loopback") == 0) {
          flag_typeshow=1;
      }

    }

    for(port=start_port;port<=end_port;port++){
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_Interlaken,MEA_TRUE/*silent*/)==MEA_FALSE)
            continue;
        MEA_OS_memset(&entry,0,sizeof(entry));
        if(MEA_API_Get_Interface_Interlaken_Entry(MEA_UNIT_0,port,&entry) !=MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_RXAUI_Entry failed port %d\n",port);
            return MEA_OK;
        }
        if(count==0){
            CLI_print("  Interface Interlaken setting       \n");
                if(flag_typeshow == 0){
                    CLI_print("port tx     packet burst  burst  force  rdyout  diagw   Rx_fc  Tx_fc \n");
                    CLI_print("     enable mode   short  max    callen  th     lanest  enable enable\n");
                    CLI_print("---- ------ ------ ------ ------ ------ ------ -------- ------ ------  \n");
                }
                if (flag_typeshow == 1 && MEA_OS_strcmp(argv[2],"-tx_rlim") == 0) {
                     CLI_print ("--------------------------------\n"
                                 "         rate_limit       \n"
                                 "  port en     max    delta  intv\n"
                                 "---- ------ ------ ------ ------ \n");
                }
                if (flag_typeshow == 1 && MEA_OS_strcmp(argv[2],"-loopback") == 0) {
                    CLI_print ("--------------------------------\n"
                              
                               "port  loop  type \n"
                               "---- ------ ------\n");
                }
            }
            
        
        count++;

        CLI_print("%4d",port);
        if(flag_typeshow == 0){

             CLI_print("%6s ",MEA_STATUS_STR(entry.ctl_tx_enable));
             CLI_print("%6s ",MEA_STATUS_STR(entry.ctl_rx_packet_mode));
            
             CLI_print("%6d ",entry.ctl_tx_burstshort);
             CLI_print("%6d ",entry.ctl_tx_burstmax);

             CLI_print("%6d ",entry.ctl_tx_fc_callen);
             CLI_print("%6d ",entry.ctl_tx_rdyout_thresh);
             CLI_print("%6d ",entry.ctl_tx_diagword_lanestat);
             CLI_print("%6s ",MEA_STATUS_STR(entry.ctl_fc_enable));
             CLI_print("%6s \n",MEA_STATUS_STR(entry.ctl_tx_fc_enable));
             CLI_print("---- ------ ------ ------ ------ ------ ------ -------- ------ ------ \n");
        }
        if (flag_typeshow == 1 && MEA_OS_strcmp(argv[2],"-tx_rlim") == 0) {
                
                CLI_print("%6s ",MEA_STATUS_STR(entry.ctl_tx_rlim_enable));
                CLI_print("%6d ",entry.ctl_tx_rlim_max   );
                CLI_print("%6d ",entry.ctl_tx_rlim_delta );
                CLI_print("%6d \n",entry.ctl_tx_rlim_intv );
                CLI_print("---- ------ ------ ------ ------ \n");
         }

        if (flag_typeshow == 1 && MEA_OS_strcmp(argv[2],"-loopback") == 0) {

            CLI_print("%6s ",MEA_STATUS_STR(entry.lbus));
            CLI_print("%6d ",entry.loopback_type   );
            
            CLI_print("---- ------ ------\n");
        }

    }
    

    CLI_print("Done\n");

    
    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_Interlaken_status(int argc, char *argv[])
{

    MEA_Interface_t port,start_port,end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count=0;
    MEA_Uint8 lanes;

    MEA_Interlaken_Status_Entry_dbt entry;

   


    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if((up_ptr=strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port   = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1],"all") == 0) {
            start_port=0;
            end_port=MEA_INTERFACE_MAX_ID-1;
        } else {
            port=start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_Interlaken    ,MEA_TRUE/*silent*/)==MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not RXAUI or not valid\n",port);
                return MEA_OK;
            }

        }
    }

    for(port=start_port;port<=end_port;port++)
    {
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_Interlaken,MEA_TRUE/*silent*/)==MEA_FALSE)
            continue;
        MEA_OS_memset(&entry,0,sizeof(entry));
        if(MEA_API_Get_Interface_Interlaken_Status_Entry(MEA_UNIT_0,port,&entry) !=MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_Interlaken_Status_Entry failed port %d\n",port);
            return MEA_OK;
        }
        if(count==0){
            CLI_print("   status Interlaken \n");
            CLI_print("----------------------\n");
        }
             
            CLI_print("------ RX Interlaken------ ------ ------ ------\n");
            CLI_print("  ID   rx    align  crc32  msop   meop   burst \n");
            CLI_print("       align err    err    err    err    err    \n");
            CLI_print("------ ------ ------ ------ ------ ------ ------\n");
        
        count++;
        
        CLI_print("  %4d",port);
        
        CLI_print("%6s ",MEA_STATUS_STR(entry.stat_rx_aligned));     
        CLI_print("%6s ",MEA_STATUS_STR(entry.stat_rx_aligned_err)); 
        CLI_print("%6s ",MEA_STATUS_STR(entry.stat_rx_crc32_err ));  
        CLI_print("%6s ",MEA_STATUS_STR(entry.stat_rx_msop_err ));   
        CLI_print("%6s ",MEA_STATUS_STR(entry.stat_rx_meop_err ));   
        CLI_print("%6s \n",MEA_STATUS_STR(entry.stat_rx_burst_err ));  
        CLI_print("------ ------ ------ ------ ------ ------\n");

        CLI_print("------ TX status Interlaken -------- \n");
        CLI_print("  ID   overflow  burst     underflow \n");
        CLI_print("       err       err       err       \n");
        CLI_print("------ --------- --------- ---------- \n");
        CLI_print("  %4d",port);
        CLI_print("%9s ",MEA_STATUS_STR(entry.stat_tx_overflow_err));
        CLI_print("%9s ",MEA_STATUS_STR(entry.stat_tx_burst_err));
        CLI_print("%9s \n",MEA_STATUS_STR(entry.stat_tx_underflow_err));
        CLI_print("--------------------------------------\n");

        CLI_print("--------------------------------------\n");
        CLI_print("    Lanes  status                     \n");
        CLI_print("--------------------------------------\n");

        for(lanes=0;lanes<MEA_INTERLAKEN_MAX_OF_LANES;lanes++){
            CLI_print("------------------------------\n");
           CLI_print("    lanes %4d\n",lanes);
           
          CLI_print("    %-20s : %6s \n","rx_synced",MEA_STATUS_STR(entry.stat_lanes[lanes].val.stat_rx_synced ));      
          CLI_print("    %-20s : %6s \n","synced_err",MEA_STATUS_STR(entry.stat_lanes[lanes].val.stat_rx_synced_err));   
          CLI_print("    %-20s : %6s \n","mf_len_err",MEA_STATUS_STR(entry.stat_lanes[lanes].val.stat_rx_mf_len_err));   
          CLI_print("    %-20s : %6s \n","mf_repeat_err",MEA_STATUS_STR(entry.stat_lanes[lanes].val.stat_rx_mf_repeat_err));
          CLI_print("    %-20s : %6s \n","descram_err",MEA_STATUS_STR(entry.stat_lanes[lanes].val.stat_rx_descram_err));  
          CLI_print("    %-20s : %6s \n","rx_mf_err",MEA_STATUS_STR(entry.stat_lanes[lanes].val.stat_rx_mf_err));       
          CLI_print("    %-20s : %6s \n","framing_err",MEA_STATUS_STR(entry.stat_lanes[lanes].val.stat_rx_framing_err));  
        }
        
        
         

       
    }

    CLI_print("Done\n");







    return MEA_OK;
}
/************************************************************************/
/*                CLI_Interface_RXAUI                                   */
/************************************************************************/
MEA_Status MEA_CLI_Interface_RXAUI_Set(int argc, char *argv[])
{
    MEA_RXAUI_Entry_dbt  entry;
    MEA_Interface_t                 Id;
    int i;
    int num_of_params;


    if (argc < 2) {
        return MEA_ERROR;
    }

    Id = (MEA_Port_t)MEA_OS_atoiNum(argv[1]);


    if(MEA_API_Get_Interface_RXAUI_Entry(MEA_UNIT_0,Id,&entry)!=MEA_OK){
        CLI_print("Error MEA_API_Get_Interface_RXAUI_Entry failed port %d\n",Id);
        return MEA_OK; 
    }

    for(i=(1); i<argc ;i++){

       
           if(!MEA_OS_strcmp(argv[i],"-loopback")){ 
               num_of_params=1;
               if ((i+num_of_params)>argc ) {
                   CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                   return MEA_ERROR;
               }
               entry.RXAUI_reg.val.rxauvi_lb_type = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);


           }
        
        if(!MEA_OS_strcmp(argv[i],"-lb_mac")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.RXAUI_reg.val.loopback_mac = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
            

        }
        
        if(!MEA_OS_strcmp(argv[i],"-reset_serdes")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.RXAUI_reg.val.reset_serdes_type = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-power_tx")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.RXAUI_reg.val.power_tx_down = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-reset_f_status")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.RXAUI_reg.val.reset_fault_status = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-reset_rx_link")){ 
            num_of_params=1;
            if ((i+num_of_params)>argc ) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.RXAUI_reg.val.reset_rx_link = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
        }
        
        if(!MEA_OS_strcmp(argv[i],"-ifg")){ 
                num_of_params=2;
                if ((i+num_of_params)>argc ) {
                    CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                    return MEA_ERROR;
                }
                entry.RXAUI_reg.val.xmac_IFG_mode = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 1]);
                entry.RXAUI_reg.val.xmac_DIC = (MEA_Uint8)MEA_OS_atoiNum(argv[i + 2]);

        }
       

        

    }

    if(MEA_API_Set_Interface_RXAUI_Entry(MEA_UNIT_0,Id,&entry)!=MEA_OK){
        CLI_print("Error MEA_API_Set_Interface_RXAUI_Entry failed port %d\n",Id);
        return MEA_OK;
    }

    
    
    
    
    
    
    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_RXAUI_Get(int argc, char *argv[])
{
     MEA_Interface_t port,start_port,end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count=0;

    MEA_RXAUI_Entry_dbt entry;
    
   
    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if((up_ptr=strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port   = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1],"all") == 0) {
            start_port=0;
            end_port=MEA_INTERFACE_MAX_ID-1;
        } else {
            port=start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_RXAUI,MEA_TRUE/*silent*/)==MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not RXAUI or not valid\n",port);
                return MEA_OK;
            }

        }
    }
    
     for(port=start_port;port<=end_port;port++){
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_RXAUI,MEA_TRUE/*silent*/)==MEA_FALSE)
            continue;
        MEA_OS_memset(&entry,0,sizeof(entry));
        if(MEA_API_Get_Interface_RXAUI_Entry(MEA_UNIT_0,port,&entry) !=MEA_OK){
           CLI_print("Error MEA_API_Get_Interface_RXAUI_Entry failed port %d\n",port);
            return MEA_OK;
        }
        if(count==0){
            CLI_print("  Interface RXAUI Configure setting              \n");
            CLI_print(" Id  power  Loop     reset    reset    Loop serdes\n");
            CLI_print("     txDown mac      f_status rx_link       reset \n");
            CLI_print("---- ------ -------- -------- -------- ---- ------ \n");
        }
          count++;
        CLI_print("%4d ",port);
       
        CLI_print("%6s ",MEA_STATUS_STR(entry.RXAUI_reg.val.power_tx_down)  );
        CLI_print("%8s ",MEA_STATUS_STR(entry.RXAUI_reg.val.loopback_mac));

        CLI_print("%8s ",MEA_STATUS_STR(entry.RXAUI_reg.val.reset_fault_status));
        CLI_print("%8s ",MEA_STATUS_STR(entry.RXAUI_reg.val.reset_rx_link));
        
        if(entry.RXAUI_reg.val.rxauvi_lb_type)
            CLI_print("%4d ",entry.RXAUI_reg.val.rxauvi_lb_type);
        else
           CLI_print("%4s ","NA");
        CLI_print("%ds \n",entry.RXAUI_reg.val.reset_serdes_type);


       
   
         }
         CLI_print("---- ------ ----- -------- -------- -------- ---- \n");

CLI_print("Done\n");

    
    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_RXAUI_status(int argc, char *argv[])
{
    MEA_Port_t port,start_port,end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count=0;

    MEA_RXAUI_Status_Entry_dbt entry;
    

    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if((up_ptr=strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port   = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1],"all") == 0) {
            start_port=0;
            end_port=MEA_INTERFACE_MAX_ID-1;
        } else {
            port=start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_RXAUI,MEA_TRUE/*silent*/)==MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not RXAUI or not valid\n",port);
                return MEA_OK;
            }

        }
    }

    for(port=start_port;port<=end_port;port++)
    {
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_RXAUI,MEA_TRUE/*silent*/)==MEA_FALSE)
            continue;
        MEA_OS_memset(&entry,0,sizeof(entry));
        if(MEA_API_Get_Interface_RXAUI_Status_Entry(MEA_UNIT_0,port,&entry) !=MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_RXAUI_Status_Entry failed port %d\n",port);
            return MEA_OK;
        }
        if(count==0){
            CLI_print("   status RXAUI \n");
            CLI_print("  port Link   Tx     RX     sync   align    Tx_reset      Rx_reset   Parity\n");
            CLI_print("       status fault  fault  status status lane1  lane0  lane1  lane0         \n");
            CLI_print("------ ------ ------ ------ ------ ------ ------ ------ ------ ------ ------ \n");
        }
        count++;
        CLI_print("%6d",port);
        CLI_print("%6s ",(entry.rx_link_status==MEA_TRUE) ? "UP": "Down"); 
        CLI_print("%6s ",MEA_STATUS_STR(entry.tx_local_fault)); 
        CLI_print("%6s ",MEA_STATUS_STR(entry.rx_local_fault ));
        CLI_print("%6x ",entry.sync_status );   
        CLI_print("%6s  ",MEA_STATUS_STR( entry.align_status));
        CLI_print("%5s ", MEA_STATUS_STR( entry.Tx_reset_d_lane1 ));
        CLI_print("%5s ", MEA_STATUS_STR( entry.Tx_reset_d_lane0));
        CLI_print("%5s ", MEA_STATUS_STR( entry.Rx_reset_done_lane1));
        CLI_print("%5s ", MEA_STATUS_STR( entry.Rx_reset_done_lane0));
        CLI_print("%6d \n", entry.Parity);
               
        CLI_print("------ ------ ------ ------ ------ ------ ------\n");
    }

    CLI_print("Done\n");

    
    
    return MEA_OK;
}


/************************************************************************/
/*                CLI_Interface_SFP_PLUS                                */
/************************************************************************/
MEA_Status MEA_CLI_Interface_SFP_PLUS_Set(int argc, char *argv[])
{
    MEA_SFP_PLUS_Entry_dbt  entry;
    MEA_Interface_t                 Id;
    int i;
    int num_of_params;


    if (argc < 2) {
        return MEA_ERROR;
    }

    Id = (MEA_Port_t)MEA_OS_atoiNum(argv[1]);


    if (MEA_API_Get_Interface_SFP_PLUS_Entry(MEA_UNIT_0, Id, &entry) != MEA_OK){
        CLI_print("Error MEA_API_Get_Interface_SFP_PLUS_Entry failed port %d\n", Id);
        return MEA_OK;
    }

    for (i = (1); i<argc; i++){



        if (!MEA_OS_strcmp(argv[i], "-ifg")){
            num_of_params = 2;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_IFG_mode = MEA_OS_atoiNum(argv[i + 1]);
            entry.SFP_PLUS.val.xmac_DIC      = MEA_OS_atoiNum(argv[i + 2]);

        }

        
            if (!MEA_OS_strcmp(argv[i], "-rx_pause")){
                num_of_params = 1;
                if ((i + num_of_params) > argc) {
                    CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                    return MEA_ERROR;
                }
                entry.SFP_PLUS.val.xmac_Rx_pause_en = MEA_OS_atoiNum(argv[i + 1]);


            }

        if (!MEA_OS_strcmp(argv[i], "-pma_reset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_pma_reset = MEA_OS_atoiNum(argv[i + 1]);
            

        }
        if (!MEA_OS_strcmp(argv[i], "-pcs_reset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_pcs_reset = MEA_OS_atoiNum(argv[i + 1]);


        }

        if (!MEA_OS_strcmp(argv[i], "-pmd_tx_disable")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_pmd_tx_disable = MEA_OS_atoiNum(argv[i + 1]);


        }

        if (!MEA_OS_strcmp(argv[i], "-RX_pattern_check")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_enable_RX_test_pattern_check = MEA_OS_atoiNum(argv[i + 1]);


        }
        
        if (!MEA_OS_strcmp(argv[i], "-TX_pattern")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_enable_TX_test_pattern = MEA_OS_atoiNum(argv[i + 1]);


        }

        if (!MEA_OS_strcmp(argv[i], "-data_pattern_select")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_data_patter_select = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-test_pattern_select")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_test_patter_select = MEA_OS_atoiNum(argv[i + 1]);


        }

        if (!MEA_OS_strcmp(argv[i], "-prbs31_tx_pattern")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_PRBS31_test_tx_pattern_enable = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-prbs31_rx_pattern")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.xmac_PRBS31_test_rx_pattern_enable = MEA_OS_atoiNum(argv[i + 1]);


        }

        if (!MEA_OS_strcmp(argv[i], "-gt0_rxrate")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_rxrate = MEA_OS_atoiNum(argv[i + 1]);


        }

        if (!MEA_OS_strcmp(argv[i], "-gt0_txdiffctrl")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_txdiffctrl = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_rxlpmen")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_rxlpmen = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_rxdfelpmreset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_rxdfelpmreset = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_rxpmareset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_rxpmareset = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_rxpolarity")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_rxpolarity = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_txpolarity")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_txpolarity = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_txprbsforceerr")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_txprbsforceerr = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_rxcdrhold")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_rxcdrhold = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_eyescantrigger")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_eyescantrigger = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_eyescanreset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_eyescanreset = MEA_OS_atoiNum(argv[i + 1]);

        }

        if (!MEA_OS_strcmp(argv[i], "-gt0_txprecursor")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_txprecursor = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-gt0_txpostcursor")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.SFP_PLUS.val.gt0_txpostcursor = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-rx_block_th")){
            num_of_params = 1;
        if ((i + num_of_params) > argc) {
            CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
            return MEA_ERROR;
        }
        entry.SFP_PLUS.val.XmacRX_block_th = MEA_OS_atoiNum(argv[i + 1]);

        }
        
            


    }

    if (MEA_API_Set_Interface_SFP_PLUS_Entry(MEA_UNIT_0, Id, &entry) != MEA_OK){
        CLI_print("Error MEA_API_Set_Interface_RXAUI_Entry failed port %d\n", Id);
        return MEA_OK;
    }







    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_SFP_PLUS_Get(int argc, char *argv[])
{
    MEA_Interface_t port, start_port, end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count = 0;

    MEA_SFP_PLUS_Entry_dbt entry;


    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if ((up_ptr = strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1], "all") == 0) {
            start_port = 0;
            end_port = MEA_INTERFACE_MAX_ID - 1;
        }
        else {
            port = start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_SINGLE10G, MEA_TRUE/*silent*/) == MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not SINGLE10G or not valid\n", port);
                return MEA_OK;
            }

        }
    }

    for (port = start_port; port <= end_port; port++){
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_SINGLE10G, MEA_TRUE/*silent*/) == MEA_FALSE)
            continue;
        MEA_OS_memset(&entry, 0, sizeof(entry));
        if (MEA_API_Get_Interface_SFP_PLUS_Entry(MEA_UNIT_0, port, &entry) != MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_RXAUI_Entry failed port %d\n", port);
            return MEA_OK;
        }
        if (count == 0){
            CLI_print("  Interface SFP_PLUS Configure setting              \n");
            CLI_print("----------------------------------------------------------------- \n");
        }
        count++;
        CLI_print("%-30s : %4d \n","ID" ,port);
        
        
        CLI_print("%-30s :%8s \n", "IFG_mode"    , (entry.SFP_PLUS.val.xmac_IFG_mode == MEA_TRUE) ? "WAN" : "LAN" );
        CLI_print("%-30s : %6d \n", "DIC mode"   , entry.SFP_PLUS.val.xmac_DIC);
        
        CLI_print("%-30s : %6s \n", "Rx_pause_en", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_Rx_pause_en));
        CLI_print("%-30s : %6s \n", "pma_reset", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_pma_reset));
        CLI_print("%-30s : %6s \n", "pcs_reset", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_pcs_reset));
        CLI_print("%-30s : %6s \n", "pmd_tx_disable", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_pmd_tx_disable));
        CLI_print("%-30s : %6s \n", "RX_test_pattern_check", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_enable_RX_test_pattern_check));
        CLI_print("%-30s : %6s \n", "TX_test_pattern", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_enable_TX_test_pattern));
        CLI_print("%-30s : %6s \n", "data_patter_select", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_data_patter_select));
        CLI_print("%-30s : %6s \n", "test_patter_select", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_test_patter_select));
        CLI_print("%-30s : %6s \n", "PRBS31_tx_pattern_enable", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_PRBS31_test_tx_pattern_enable));
        CLI_print("%-30s : %6s \n", "PRBS31_rx_pattern_enable", MEA_STATUS_STR(entry.SFP_PLUS.val.xmac_PRBS31_test_rx_pattern_enable));
        CLI_print("%-30s : %6s \n", "gt0_eyescanreset   ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_eyescanreset));
        CLI_print("%-30s : %6s \n", "gt0_eyescantrigger ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_eyescantrigger));
        CLI_print("%-30s : %6s \n", "gt0_rxcdrhold      ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_rxcdrhold));
        CLI_print("%-30s : %6s \n", "gt0_txprbsforceerr ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_txprbsforceerr));
        CLI_print("%-30s : %6s \n", "gt0_txpolarity     ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_txpolarity));
        CLI_print("%-30s : %6s \n", "gt0_rxpolarity     ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_rxpolarity));
        CLI_print("%-30s : %6s \n", "gt0_txpmareset     ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_txpmareset));
        CLI_print("%-30s : %6s \n", "gt0_rxpmareset     ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_rxpmareset));
        CLI_print("%-30s : %6s \n", "gt0_rxdfelpmreset  ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_rxdfelpmreset));
        CLI_print("%-30s : %6s \n", "gt0_rxlpmen        ", MEA_STATUS_STR(entry.SFP_PLUS.val.gt0_rxlpmen));
        CLI_print("%-30s : %6d \n", "gt0_txdiffctrl     ", entry.SFP_PLUS.val.gt0_txdiffctrl);
        CLI_print("%-30s : %6d \n", "gt0_rxrate         ", entry.SFP_PLUS.val.gt0_rxrate);
        CLI_print("%-30s : %6d \n", "gt0_txpostcursor   ", entry.SFP_PLUS.val.gt0_txpostcursor);
        CLI_print("%-30s : %6d \n", "gt0_txprecursor    ", entry.SFP_PLUS.val.gt0_txprecursor);
        CLI_print("%-30s : %6d \n", "XmacRX_block_th    ", entry.SFP_PLUS.val.XmacRX_block_th);
        CLI_print("----------------------------------------------------------------- \n");




    }
   
    CLI_print("Done\n");


    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_SFP_PLUS_status(int argc, char *argv[])
{
    MEA_Port_t port, start_port, end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count = 0;

    MEA_SFP_PLUS_Status_Entry_dbt entry;


    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if ((up_ptr = strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1], "all") == 0) {
            start_port = 0;
            end_port = MEA_INTERFACE_MAX_ID - 1;
        }
        else {
            port = start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_SINGLE10G, MEA_TRUE/*silent*/) == MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not SFP_PLUS or not valid\n", port);
                return MEA_OK;
            }

        }
    }

    for (port = start_port; port <= end_port; port++)
    {
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_SINGLE10G, MEA_TRUE/*silent*/) == MEA_FALSE)
            continue;
        MEA_OS_memset(&entry, 0, sizeof(entry));
        if (MEA_API_Get_Interface_SFP_PLUS_Status_Entry(MEA_UNIT_0, port, &entry) != MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_SFP_PLUS_Status_Entry failed port %d\n", port);
            return MEA_OK;
        }
        if (count == 0){
            CLI_print("   status SFP_PLUS \n");
            CLI_print("------------------------------------------------------------------ \n");
        }
        count++;
        CLI_print("%-30s : %6d  \n","ID", port);
       

        CLI_print("%-30s : %6s \n", "PMA_Reset", MEA_STATUS_STR(entry.val.PMA_Reset));
        CLI_print("%-30s : %6s \n", "PMA_PMD_RX_Link_Status", MEA_STATUS_STR(entry.val.PMA_PMD_RX_Link_Status));
        CLI_print("%-30s : %6s \n", "PMA_PMD_Fault", MEA_STATUS_STR(entry.val.PMA_PMD_Fault));
        CLI_print("%-30s : %6s \n", "PMA_PMD_RX_Fault", MEA_STATUS_STR(entry.val.PMA_PMD_RX_Fault));
        CLI_print("%-30s : %6s \n", "PMA_PMD_TX_Fault", MEA_STATUS_STR(entry.val.PMA_PMD_TX_Fault));
        CLI_print("%-30s : %6s \n", "PMD_RX_Signal_Detect", MEA_STATUS_STR(entry.val.PMD_RX_Signal_Detect));
        //CLI_print("%-30s : %6s \n", "pma_reset", MEA_STATUS_STR(entry.val.Core_Info_reserved0));
        CLI_print("%-30s : %6d \n", "Core_Info_patch_number", entry.val.Core_Info_patch_number);
        CLI_print("%-30s : %6s \n", "Core_Info_FEC_include", MEA_STATUS_STR(entry.val.Core_Info_FEC_include));
        CLI_print("%-30s : %6s \n", "Core_Info_AN_include", MEA_STATUS_STR(entry.val.Core_Info_AN_include));
        CLI_print("%-30s : %6s \n", "Core_Info_reserved1", MEA_STATUS_STR(entry.val.Core_Info_reserved1));
        CLI_print("%-30s : %6s \n", "Core_Info_KR_include", MEA_STATUS_STR(entry.val.Core_Info_KR_include));
        CLI_print("%-30s : %6d \n", "Core_Info_parameter", entry.val.Core_Info_parameter);
        CLI_print("%-30s : %6d \n", "Core_Info_SFP_plus_version", entry.val.Core_Info_SFP_plus_version);
        CLI_print("%-30s : %6s \n", "PCS_Reset", MEA_STATUS_STR(entry.val.PCS_Reset));
        CLI_print("%-30s : %6s \n", "PCS_RX_LinkStatus_latch", MEA_STATUS_STR(entry.val.PCS_RX_Link_Status_latch));
        CLI_print("%-30s : %6s \n", "PCS_Fault", MEA_STATUS_STR(entry.val.PCS_Fault));
        CLI_print("%-30s : %6s \n", "PCS_RX_Fault", MEA_STATUS_STR(entry.val.PCS_RX_Fault));
        CLI_print("%-30s : %6s \n", "PCS_TX_Fault", MEA_STATUS_STR(entry.val.PCS_TX_Fault));
        CLI_print("%-30s : %6s \n", "RX_Locked", MEA_STATUS_STR(entry.val.RX_Locked));
        CLI_print("%-30s : %6s \n", "high_BER", MEA_STATUS_STR(entry.val.high_BER));
        CLI_print("%-30s : %6s \n", "PCS_RX_Link_Status", MEA_STATUS_STR(entry.val.PCS_RX_Link_Status));
        CLI_print("%-30s : %6s \n", "RX_high_BER", MEA_STATUS_STR(entry.val.RX_high_BER));
        CLI_print("%-30s : %6s \n", "RX_Block_Lock", MEA_STATUS_STR(entry.val.RX_Block_Lock));

        CLI_print("%-30s : %6d \n", "PCS_Error_Blocks_Counter", entry.val.PCS_Error_Blocks_Counter);
        CLI_print("%-30s : %6d \n", "PCS_BER_Counter", entry.val.PCS_BER_Counter);
        CLI_print("%-30s : %6d \n", "Pattern_Error_Counter", entry.val.Test_Pattern_Error_Counter);
        CLI_print("%-30s : %6s \n", "Reset_done", MEA_STATUS_STR(entry.val.Reset_done));
        CLI_print("%-30s : %6s \n", "PCS_lock", MEA_STATUS_STR(entry.val.PCS_lock));

        CLI_print("%-30s : %6s \n", "txresetdone     ", MEA_STATUS_STR(entry.val.txresetdone));
        CLI_print("%-30s : %6s \n", "rxresetdone     ", MEA_STATUS_STR(entry.val.rxresetdone));
        CLI_print("%-30s : %6s \n", "eyescandataerro ", MEA_STATUS_STR(entry.val.eyescandataerro));
        CLI_print("%-30s : %6s \n", "rxprbserr       ", MEA_STATUS_STR(entry.val.rxprbserr));
        CLI_print("%-30s : %6d \n", "Rxbufstatus     ", entry.val.Rxbufstatus);
        CLI_print("%-30s : %6s \n", "resetdone       ", MEA_STATUS_STR(entry.val.resetdone));
        CLI_print("%-30s : %6d \n", "Txbufstatus     ", entry.val.Txbufstatus);
        CLI_print("%-30s : %6s \n", "txuserrdy       ", MEA_STATUS_STR(entry.val.txuserrdy));
        CLI_print("%-30s : %6d \n", "Dmonitorout     ", entry.val.Dmonitorout);



       

        CLI_print("------------------------------------------------------------------ \n");
    }

    CLI_print("Done\n");



    return MEA_OK;
}

/************************************************************************/
/*                CLI Interface_G999_1                                  */
/************************************************************************/

MEA_Status  MEA_CLI_Set_Interface_G999_1_pause_threshold(int argc, char* argv[])
{


    MEA_Global_Interface_G999_1_dbt entry;

    MEA_Uint32            val1,val2;




    if (argc != 3) {

        return MEA_ERROR;

    }



    val1 = MEA_OS_atoi(argv[1]);

    val2 = MEA_OS_atoi(argv[2]);
    



    if (MEA_API_Get_Global_Interface_G999_1_Entry (MEA_UNIT_0,&entry) != MEA_OK) {

        CLI_print("CLI Error MEA_API_Get_Global_Interface_G999_1_Entry failed\n");

        return MEA_OK;

    }


    entry.if_G999_1_th_config.val.Fragment_999_pause_enable =  val1;
    entry.if_G999_1_th_config.val.Fragment_999_threshold    = val2;


    if (MEA_API_Set_Global_Interface_G999_1_Entry (MEA_UNIT_0,&entry) != MEA_OK) {

        CLI_print("CLI Error MEA_API_Set_Global_Interface_G999_1_Entry failed\n");

        return MEA_OK;

    }



    CLI_print("Done.\n");



    return MEA_OK;


}


MEA_Status  MEA_CLI_Set_Interface_G999_1_packet_9991_threshold(int argc, char* argv[])
{


	MEA_Global_Interface_G999_1_dbt entry;

	MEA_Uint32            val1;




	if (argc < 2) {

		return MEA_ERROR;

	}



	val1 = MEA_OS_atoi(argv[1]);

	




	if (MEA_API_Get_Global_Interface_G999_1_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("CLI Error MEA_API_Get_Global_Interface_G999_1_Entry failed\n");

		return MEA_OK;

	}


	entry.if_G999_1_th_config.val.packet_9991_threshold = val1;
	


	if (MEA_API_Set_Global_Interface_G999_1_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("CLI Error MEA_API_Set_Global_Interface_G999_1_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;


}

MEA_Status  MEA_CLI_Set_Interface_G999_1_mng_channel(int argc, char* argv[])
{


    MEA_Global_Interface_G999_1_dbt entry;

    MEA_Uint32            val1;




    if (argc < 2) {

        return MEA_ERROR;

    }



    val1 = MEA_OS_atoi(argv[1]);

   




    if (MEA_API_Get_Global_Interface_G999_1_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

        CLI_print("CLI Error MEA_API_Get_Global_Interface_G999_1_Entry failed\n");

        return MEA_OK;

    }


    entry.if_G999_1_th_config.val.mng_channel = val1;
   


    if (MEA_API_Set_Global_Interface_G999_1_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

        CLI_print("CLI Error MEA_API_Set_Global_Interface_G999_1_Entry failed\n");

        return MEA_OK;

    }



    CLI_print("Done.\n");



    return MEA_OK;


}


MEA_Status MEA_CLI_Get_Interface_G999_1(int argc, char* argv[])
{
    MEA_Global_Interface_G999_1_dbt entry;

    if (argc < 2) {
        return MEA_ERROR;
    }


    if(MEA_API_Get_Global_Interface_G999_1_Entry(MEA_UNIT_0, &entry) != MEA_OK){
        CLI_print("MEA_API_Get_Global_Interface_G999_1_Entry failed\n");
        return MEA_OK;
    }
    
    CLI_print("    Global G999_1   \n");
    CLI_print("---------------------------------------------\n");
    CLI_print("     %-25s  : %s \n","pause enable",MEA_STATUS_STR(entry.if_G999_1_th_config.val.Fragment_999_pause_enable));
    CLI_print("     %-25s  : %d \n","threshold",entry.if_G999_1_th_config.val.Fragment_999_threshold);
    CLI_print("     %-25s  : %d \n","mng_channel",entry.if_G999_1_th_config.val.mng_channel);
	CLI_print("     %-25s  : %d \n", "packet_9991_threshold", entry.if_G999_1_th_config.val.packet_9991_threshold);
    CLI_print("     %-25s  : %s \n","Increment enable",MEA_STATUS_STR(entry.if_G999_1_th_config.val.increment_enable));
    CLI_print("---------------------------------------------\n");
    
    
    
    return MEA_OK;

}

/************************************************************************/
/*                CLI_Interface_RGMII                                   */
/************************************************************************/
MEA_Status MEA_CLI_Interface_RGMII_status(int argc, char *argv[])
{
    MEA_Port_t port,start_port,end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count=0;

    MEA_RGMII_Status_Entry_dbt entry;


    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if((up_ptr=strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port   = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1],"all") == 0) {
            start_port=0;
            end_port=MEA_INTERFACE_MAX_ID-1;
        } else {
            port=start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_RGMII,MEA_TRUE/*silent*/)==MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not RGMII or not valid\n",port);
                return MEA_OK;
            }

        }
    }

    for(port=start_port;port<=end_port;port++)
    {
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_RGMII,MEA_TRUE/*silent*/)==MEA_FALSE)
            continue;
        MEA_OS_memset(&entry,0,sizeof(entry));
        if(MEA_API_Get_Interface_RGMII_Status_Entry(MEA_UNIT_0,port,&entry) !=MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_RGMII_Status_Entry failed port %d\n",port);
            return MEA_OK;
        }
        if(count==0){
            CLI_print("   status RGMII \n");
            CLI_print(" port Link   speed    duplex  \n");
            CLI_print(" ---- ------ -------- ------  \n");
        }
        count++;
        CLI_print(" %4d",port);
        CLI_print("%6s ",(entry.value.link==MEA_TRUE) ? "UP": "Down");
        if(entry.value.speed == 0)
            CLI_print("%s ", " 10Mb/s");
        if(entry.value.speed == 1)
            CLI_print("%s ", "100Mb/s");
        if(entry.value.speed == 2)
            CLI_print("%s ", "  1Gb/s");

        CLI_print("%6s \n",(entry.value.duplex==MEA_TRUE) ? "Full": "Half");
          

         CLI_print(" ---- ------ -------- ------  \n");
    }

    CLI_print("Done\n");



    return MEA_OK;
}


/************************************************************************/
/*                CLI_Interface_SGMII                                   */
/************************************************************************/
MEA_Status  MEA_CLI_Interface_SGMII_status(int argc, char *argv[])
{
    MEA_Port_t port,start_port,end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count=0;

    MEA_SGMII_Status_Entry_dbt entry;
    MEA_Interface_Entry_dbt    ConfigureEntry;

    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if((up_ptr=strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port   = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1],"all") == 0) {
            start_port=0;
            end_port=MEA_INTERFACE_MAX_ID-1;
        } else {
            port=start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_SGMII,MEA_TRUE/*silent*/)==MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed Id %d is not SGMII or not valid\n",port);
                return MEA_OK;
            }

        }
    }

    for(port=start_port;port<=end_port;port++)
    {
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0,port,MEA_INTERFACETYPE_SGMII,MEA_TRUE/*silent*/)==MEA_FALSE)
            continue;
        MEA_OS_memset(&entry,0,sizeof(entry));
        if(MEA_API_Get_Interface_SGMII_Status_Entry(MEA_UNIT_0,port,&entry) !=MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_RGMII_Status_Entry failed port %d\n",port);
            return MEA_OK;
        }
        if (MEA_API_Get_Interface_ConfigureEntry(MEA_UNIT_0, port , &ConfigureEntry) != MEA_OK){

        }
        if(count==0){
            CLI_print("   status SGMII \n");
            CLI_print(" port Link   link   rudi rudi rudi    rxdisp rx     phy    remote     speed   duplex remote pause\n");
            CLI_print("             sync   C    I    invalid err    notin  linkS  fault_e                   f            \n");
            CLI_print(" ---- ------ ------ ---- ---- ------- ------ ------ ------ ------- - ------- ------ ------ ----- \n");
        }
        count++;
        CLI_print("%6d",port);

        CLI_print("%6s ", (entry.value.link_Status == MEA_TRUE) ? "UP" : "Down");
        CLI_print("%6s ", MEA_STATUS_STR(entry.value.Link_sync));
        CLI_print("%4s ", MEA_STATUS_STR(entry.value.rudi_c));
        CLI_print("%4s ", MEA_STATUS_STR(entry.value.rudi_i));
        CLI_print("%7s ", MEA_STATUS_STR(entry.value.rudi_invalid));
        CLI_print("%6s ", MEA_STATUS_STR(entry.value.rxdisperr));
        CLI_print("%6s ", MEA_STATUS_STR(entry.value.rx_notintable));
        CLI_print("%6s ", MEA_STATUS_STR(entry.value.phy_linkstatuse));
        CLI_print("%7d ", entry.value.remote_fault_encoding);
        if (ConfigureEntry.autoneg == MEA_FALSE){
            CLI_print("F ");
        }
        else{
            CLI_print("A ");
        }
        if (entry.value.speed == 0)
        CLI_print("%7s ", "10Mb/S");
        if (entry.value.speed == 1)
        CLI_print("%7s ", "100Mb/S");
        if (entry.value.speed == 2)
        CLI_print("%7s ", "1Gb/S");
        if (entry.value.speed == 3)
        CLI_print("%7s ", "NA");
        CLI_print("%6s ", (entry.value.duplex == MEA_TRUE) ? "Full" : "Half");
        CLI_print("%6s ", MEA_STATUS_STR(entry.value.remote_fault));
        CLI_print("%5d ", entry.value.pause);
        CLI_print("\n");

        CLI_print(" ---- ------ ------ ---- ---- ------- ------ ----- ----- ------- ------- ------- ------ ----- \n");
    }

    CLI_print("Done\n");



    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_SGMII_Conf(int argc, char *argv[])
{
    MEA_SGMII_Entry_dbt  entrySgmii;
    MEA_Interface_t                 Id;
    int i;
    int num_of_params;


    if (argc < 2) {
        return MEA_ERROR;
    }

    Id = (MEA_Port_t)MEA_OS_atoiNum(argv[1]);


    if (MEA_API_Get_Interface_SGMII_Entry(MEA_UNIT_0, Id, &entrySgmii) != MEA_OK) {
        CLI_print("Error MEA_API_Get_Interface_SGMII_Entry failed port %d\n", Id);
        return MEA_OK;
    }

    for (i = (1); i < argc; i++) {

        if (!MEA_OS_strcmp(argv[i], "-sgmii.Unidirectional_Enable")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entrySgmii.value.Unidirectional_Enable = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.Powerdown")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entrySgmii.value.Powerdown = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.Isolate")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entrySgmii.value.electrically_Isolate = MEA_OS_atoiNum(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "-sgmii.speed")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entrySgmii.value.speed = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.PMA_RX")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entrySgmii.value.RX_PMA_reset = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.PMA_TX")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entrySgmii.value.TX_PMA_reset = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.AN_reset")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entrySgmii.value.AN_reset = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii_1000BASE")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entrySgmii.value.basex_or_sgmii = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.assert_en")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entrySgmii.value.assert_en = MEA_OS_atoiNum(argv[i + 1]);
        }
       

    }

    if (MEA_API_Set_Interface_SGMII_Entry(MEA_UNIT_0, Id, &entrySgmii) != MEA_OK) {
        CLI_print("Error MEA_API_Set_Interface_QSGMII_Entry failed port %d\n", Id);
        return MEA_OK;
    }







    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_SGMII_Get(int argc, char *argv[])
{
    MEA_Interface_t port, start_port, end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count = 0;

    MEA_SGMII_Entry_dbt entry;


    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
MEA_OS_strcpy(buf, argv[1]);
if ((up_ptr = strchr(buf, ':')) != 0)
{
    (*up_ptr) = 0;
    up_ptr++;
    start_port = MEA_OS_atoi(buf);
    end_port = MEA_OS_atoi(up_ptr);
}
else
{
    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        start_port = 0;
        end_port = MEA_INTERFACE_MAX_ID - 1;
    }
    else {
        port = start_port = end_port = MEA_OS_atoi(buf);
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_SGMII, MEA_TRUE/*silent*/) == MEA_FALSE) {
            CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not SINGLE10G or not valid\n", port);
            return MEA_OK;
        }

    }
}

for (port = start_port; port <= end_port; port++) {
    if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_SGMII, MEA_TRUE/*silent*/) == MEA_FALSE)
        continue;
    MEA_OS_memset(&entry, 0, sizeof(entry));
    if (MEA_API_Get_Interface_SGMII_Entry(MEA_UNIT_0, port, &entry) != MEA_OK) {
        CLI_print("Error MEA_API_Get_Interface_RXAUI_Entry failed port %d\n", port);
        return MEA_OK;
    }

    CLI_print("  Interface SGMII  Configure setting              \n");
    CLI_print("----------------------------------------------------------------- \n");

    count++;
    CLI_print("%-40s : %4d \n", "ID", port);
    CLI_print("%-40s : %4d \n", "Unidirectional_Enable", entry.value.Unidirectional_Enable);
    CLI_print("%-40s : %4d \n", "Powerdown", entry.value.Powerdown);
    CLI_print("%-40s : %4d \n", "electrically_Isolate", entry.value.electrically_Isolate);
    CLI_print("%-40s : %4d \n", "electrically_Isolate", entry.value.electrically_Isolate);

    CLI_print("%-40s : %4d \n", "speed", entry.value.speed);
    CLI_print("%-40s : %4d \n", "RX_PMA_reset", entry.value.RX_PMA_reset);
    CLI_print("%-40s : %4d \n", "TX_PMA_reset", entry.value.TX_PMA_reset);
    CLI_print("%-40s : %4d \n", "AN_reset", entry.value.AN_reset);
    CLI_print("%-40s : %4d \n", "basex_or_sgmii", entry.value.basex_or_sgmii);
    CLI_print("%-40s : %4d \n", "assert_en", entry.value.assert_en);

    CLI_print("----------------------------------------------------------------- \n");




}

CLI_print("Done\n");


return MEA_OK;
}

/************************************************************************/
/*                CLI_Interface_XLAUI                                   */
/************************************************************************/
MEA_Status MEA_CLI_Interface_XLAUI_Conf(int argc, char *argv[])
{

    MEA_XLAUI_Entry_dbt  entryXLAUI;
    MEA_Interface_t                 Id;
    int i;
    int num_of_params;


    if (argc < 2) {
        return MEA_ERROR;
    }

    Id = (MEA_Port_t)MEA_OS_atoiNum(argv[1]);


    if (MEA_API_Get_Interface_XLAUI_Entry(MEA_UNIT_0, Id, &entryXLAUI) != MEA_OK) {
        CLI_print("Error MEA_API_Get_Interface_XLAUI_Entry failed port %d\n", Id);
        return MEA_OK;
    }

    for (i = (1); i < argc; i++) {

        if (!MEA_OS_strcmp(argv[i], "-check_udp")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.check_Udp = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-check_tcp")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.check_tcp = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-calc_udp")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.calculate_new_Udp = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-calc_tcp")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.calculate_new_tcp = MEA_OS_atoiNum(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "-rx_pause_en")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.RX_PAUSE_GLBL_EN = MEA_OS_atoiNum(argv[i + 1]);
            entryXLAUI.XLAUI.val.RX_ENABLE_GCP = entryXLAUI.XLAUI.val.RX_PAUSE_GLBL_EN;
            entryXLAUI.XLAUI.val.RX_ENABLE_GPP = entryXLAUI.XLAUI.val.RX_PAUSE_GLBL_EN;
            
        }
       
        if (!MEA_OS_strcmp(argv[i], "-tx_enable")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.tx_enable = MEA_OS_atoiNum(argv[i + 1]);
           

        }
        if (!MEA_OS_strcmp(argv[i], "-rx_enable")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.rx_enable = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-rx_igncrc")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.rx_igncrc = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-tx_inscrc")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.tx_inscrc = MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-max_packetLen")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.max_packetLen = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-max_packetLen")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entryXLAUI.XLAUI.val.min_packetLen = MEA_OS_atoiNum(argv[i + 1]);


        }
		if (!MEA_OS_strcmp(argv[i], "-Txpause_th")) {
			num_of_params = 1;
			if ((i + num_of_params) > argc) {
				CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
				return MEA_ERROR;
			}
			entryXLAUI.XLAUI.val.TXpause = MEA_OS_atoiNum(argv[i + 1]);
		}

        
        
       


    }

   

    if(MEA_API_Set_Interface_XLAUI_Entry(MEA_UNIT_0, Id, &entryXLAUI) != MEA_OK)
    {
        CLI_print("Error MEA_API_Set_Interface_XLAUI_Entry failed Id %d \n", Id);

        return MEA_OK;
    }




    return MEA_OK;
}
MEA_Status MEA_CLI_Interface_XLAUI_Get(int argc, char *argv[])
{
    MEA_Interface_t port, start_port, end_port;
    char buf[500];
    char *up_ptr;
   // MEA_Uint32 count = 0;

    MEA_XLAUI_Entry_dbt entryXLAUI;


    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if ((up_ptr = strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1], "all") == 0) {
            start_port = 0;
            end_port = MEA_INTERFACE_MAX_ID - 1;
        }
        else {
            port = start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_QSGMII, MEA_TRUE/*silent*/) == MEA_FALSE) {
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not SINGLE10G or not valid\n", port);
                return MEA_OK;
            }

        }
    }

    for (port = start_port; port <= end_port; port++) {
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_XLAUI, MEA_TRUE/*silent*/) == MEA_FALSE)
            continue;
        MEA_OS_memset(&entryXLAUI, 0, sizeof(entryXLAUI));
        if (MEA_API_Get_Interface_XLAUI_Entry(MEA_UNIT_0, port, &entryXLAUI) != MEA_OK) {
            CLI_print("Error MEA_API_Get_Interface_XLAUI_Entry failed port %d\n", port);
            return MEA_OK;
        }

        CLI_print("  Interface XLAUI  Configure setting  Id=%d                            \n", port);
        CLI_print("----------------------------------------------------------------- \n");

        mea_drv_show_event_value(0, "tx_enable", entryXLAUI.XLAUI.val.tx_enable);
        mea_drv_show_event_value(0, "tx_rfi", entryXLAUI.XLAUI.val.tx_rfi);
        mea_drv_show_event_value(0, "tx_idle", entryXLAUI.XLAUI.val.tx_idle);
        mea_drv_show_event_value(0, "tx_inscrc", entryXLAUI.XLAUI.val.tx_inscrc);
        mea_drv_show_event_value(0, "tx_igncrc", entryXLAUI.XLAUI.val.tx_igncrc);
        mea_drv_show_event_value(1, "loopback", entryXLAUI.XLAUI.val.XLAUI_loopback);
        mea_drv_show_event_value(0, "rx_enable", entryXLAUI.XLAUI.val.rx_enable);
        mea_drv_show_event_value(0, "rx_chkpre", entryXLAUI.XLAUI.val.rx_chkpre);
        mea_drv_show_event_value(0, "rx_chksfd", entryXLAUI.XLAUI.val.rx_chksfd);
        mea_drv_show_event_value(0, "rx_delcrc", entryXLAUI.XLAUI.val.rx_delcrc);
        mea_drv_show_event_value(0, "rx_igncrc", entryXLAUI.XLAUI.val.rx_igncrc);
        //mea_drv_show_event_value(0, "check_Udp", entryXLAUI.XLAUI.val.pad1);
        mea_drv_show_event_value(0, "RX_PAUSE_PRI0_EN", entryXLAUI.XLAUI.val.RX_PAUSE_PRI0_EN);
        mea_drv_show_event_value(0, "RX_PAUSE_PRI1_EN", entryXLAUI.XLAUI.val.RX_PAUSE_PRI1_EN);
        mea_drv_show_event_value(0, "RX_PAUSE_PRI2_EN", entryXLAUI.XLAUI.val.RX_PAUSE_PRI2_EN);
        mea_drv_show_event_value(0, "RX_PAUSE_PRI3_EN", entryXLAUI.XLAUI.val.RX_PAUSE_PRI3_EN);
        mea_drv_show_event_value(0, "RX_PAUSE_PRI4_EN", entryXLAUI.XLAUI.val.RX_PAUSE_PRI4_EN);
        mea_drv_show_event_value(0, "RX_PAUSE_PRI5_EN", entryXLAUI.XLAUI.val.RX_PAUSE_PRI5_EN);
        mea_drv_show_event_value(0, "RX_PAUSE_PRI6_EN", entryXLAUI.XLAUI.val.RX_PAUSE_PRI6_EN);
        mea_drv_show_event_value(0, "RX_PAUSE_PRI7_EN", entryXLAUI.XLAUI.val.RX_PAUSE_PRI7_EN);
        mea_drv_show_event_value(0, "RX_PAUSE_GLBL_EN", entryXLAUI.XLAUI.val.RX_PAUSE_GLBL_EN);
        // mea_drv_show_event_value(0, "check_Udp", entryXLAUI.XLAUI.val.RSVD25);
        mea_drv_show_event_value(0, "RX_FWD_CTL_PKT", entryXLAUI.XLAUI.val.RX_FWD_CTL_PKT);
        //   mea_drv_show_event_value(0, "check_Udp", entryXLAUI.XLAUI.val.RSVD27);
        mea_drv_show_event_value(0, "RX_ENABLE_PCP", entryXLAUI.XLAUI.val.RX_ENABLE_PCP);
        mea_drv_show_event_value(0, "RX_ENABLE_PPP", entryXLAUI.XLAUI.val.RX_ENABLE_PPP);
        mea_drv_show_event_value(0, "RX_ENABLE_GPP", entryXLAUI.XLAUI.val.RX_ENABLE_GPP);
        mea_drv_show_event_value(0, "RX_ENABLE_GCP", entryXLAUI.XLAUI.val.RX_ENABLE_GCP);


        mea_drv_show_event_value(0, "diag_Rx_frsnc", entryXLAUI.XLAUI.val.diag_rx_frsnc);
        mea_drv_show_event_value(0, "diag_Tx_rfi", entryXLAUI.XLAUI.val.diag_tx_rfi);
        mea_drv_show_event_value(0, "diag_Tx_IDLE", entryXLAUI.XLAUI.val.diag_tx_IDLE);
        mea_drv_show_event_value(0, "diag_Tx_lb_serdes", entryXLAUI.XLAUI.val.diag_tx_lb_serdes);
        mea_drv_show_event_value(0, "diag_Rx_plfi", entryXLAUI.XLAUI.val.diag_rx_plfi);
        mea_drv_show_event_value(0, "diag_Rx_test_pattern", entryXLAUI.XLAUI.val.diag_rx_test_pattern);
        mea_drv_show_event_value(0, "diag_Tx_test_pattern", entryXLAUI.XLAUI.val.diag_tx_test_pattern);
            // mea_drv_show_event_value(0, "RX_ENABLE_PCP", entryXLAUI.XLAUI.val.diag_pad4);

        mea_drv_show_event_value(0, "diag_Tx_test_pattern", entryXLAUI.XLAUI.val.max_packetLen);
        //mea_drv_show_event_value(0, "diag_Tx_test_pattern", reserv4);
        mea_drv_show_event_value(0, "diag_Tx_test_pattern", entryXLAUI.XLAUI.val.min_packetLen);
         



        mea_drv_show_event_value(0, "check_Udp", entryXLAUI.XLAUI.val.check_Udp);
        mea_drv_show_event_value(0, "calculate new Udp checksum", entryXLAUI.XLAUI.val.calculate_new_Udp);
        mea_drv_show_event_value(0, "check_tcp", entryXLAUI.XLAUI.val.check_tcp);
        mea_drv_show_event_value(0, "calculate new tcp checksum", entryXLAUI.XLAUI.val.calculate_new_tcp);

      
    
    }
    CLI_print("----------------------------------------------------------------- \n");




    return MEA_OK;
    
}

/************************************************************************/
/*                CLI_Interface_QSGMII                                   */
/************************************************************************/
MEA_Status  MEA_CLI_Interface_QSGMII_status(int argc, char *argv[])
{
    MEA_Port_t port, start_port, end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count = 0;

    MEA_QSGMII_Status_Entry_dbt entry;
    MEA_Interface_Entry_dbt    ConfigureEntry;
    int subindex = 0;

    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if ((up_ptr = strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1], "all") == 0) {
            start_port = 0;
            end_port = MEA_INTERFACE_MAX_ID - 1;
        }
        else {
            port = start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_QSGMII, MEA_TRUE/*silent*/) == MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed Id %d is not QSGMII or not valid\n", port);
                return MEA_OK;
            }

        }
    }

    for (port = start_port; port <= end_port; port++)
    {
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_QSGMII, MEA_TRUE/*silent*/) == MEA_FALSE)
            continue;
        MEA_OS_memset(&entry, 0, sizeof(entry));





        if (MEA_API_Get_Interface_QSGMII_Status_Entry(MEA_UNIT_0, port, &entry) != MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_RGMII_Status_Entry failed port %d\n", port);
            return MEA_OK;
        }
        if (count == 0){
            CLI_print("   status SGMII \n");
            CLI_print(" port Link   link   rudi rudi rudi    rxdisp rx     phy    remote     speed   duplex remote pause\n");
            CLI_print("             sync   C    I    invalid err    notin  linkS  fault_e                   f            \n");
            CLI_print(" ---- ------ ------ ---- ---- ------- ------ ------ ------ ------- - ------- ------ ------ ----- \n");
        }
        count++;
        for (subindex = 0; subindex < 4; subindex++){
            CLI_print("%6d", port + subindex);

            if(MEA_API_Get_Interface_ConfigureEntry(MEA_UNIT_0, port + subindex, &ConfigureEntry) != MEA_OK){

            }

        CLI_print("%6s ",(entry.qsgmii[subindex].value.link_Status==MEA_TRUE) ? "UP": "Down");
        CLI_print("%6s ",MEA_STATUS_STR(entry.qsgmii[subindex].value.Link_sync));
        CLI_print("%4s ",MEA_STATUS_STR(entry.qsgmii[subindex].value.rudi_c));
        CLI_print("%4s ",MEA_STATUS_STR(entry.qsgmii[subindex].value.rudi_i));
        CLI_print("%7s ",MEA_STATUS_STR(entry.qsgmii[subindex].value.rudi_invalid)); 
        CLI_print("%6s ",MEA_STATUS_STR(entry.qsgmii[subindex].value.rxdisperr));
        CLI_print("%6s ",MEA_STATUS_STR(entry.qsgmii[subindex].value.rx_notintable));
        CLI_print("%6s ",MEA_STATUS_STR(entry.qsgmii[subindex].value.phy_linkstatuse));
        CLI_print("%7d ",entry.qsgmii[subindex].value.remote_fault_encoding);
        if (ConfigureEntry.autoneg == MEA_FALSE){
            CLI_print("F ");
        }
        else{
            CLI_print("A ");
        }

        if(entry.qsgmii[subindex].value.speed == 0)
            CLI_print("%7s ","10Mb/S"   );
        if(entry.qsgmii[subindex].value.speed == 1)
            CLI_print("%7s ","100Mb/S"   );
        if(entry.qsgmii[subindex].value.speed == 2)
            CLI_print("%7s ","1Gb/S"   );
        if(entry.qsgmii[subindex].value.speed == 3)
            CLI_print("%7s ","NA"   );
        CLI_print("%6s ",(entry.qsgmii[subindex].value.duplex==MEA_TRUE) ? "Full" :"Half");
        CLI_print("%6s ",MEA_STATUS_STR(entry.qsgmii[subindex].value.remote_fault));
        CLI_print("%5d ",entry.qsgmii[subindex].value.pause);
        CLI_print("\n");
        }
        CLI_print(" ---- ------ ------ ---- ---- ------- ------ ------ ------ ------- - ------- ------ ------ ----- \n");
        
    }

    CLI_print("Done\n");



    return MEA_OK;
}


MEA_Status MEA_CLI_Interface_QSGMII_GTstatus(int argc, char *argv[])
{
    MEA_Port_t port, start_port, end_port;
    char buf[500];
    char *up_ptr;
  

    MEA_QSGMII_GT_Status_Entry_dbt entry;
    
 

    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if ((up_ptr = strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1], "all") == 0) {
            start_port = 0;
            end_port = MEA_INTERFACE_MAX_ID - 1;
        }
        else {
            port = start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_QSGMII, MEA_TRUE/*silent*/) == MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed Id %d is not QSGMII or not valid\n", port);
                return MEA_OK;
            }

        }
    }

    for (port = start_port; port <= end_port; port++)
    {
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_QSGMII, MEA_TRUE/*silent*/) == MEA_FALSE)
            continue;
        MEA_OS_memset(&entry, 0, sizeof(entry));


     


        if (MEA_API_Get_Interface_QSGMII_GT_Status_Entry(MEA_UNIT_0, port, &entry) != MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_QSGMII_GT_Status_Entry failed port %d\n", port);
            return MEA_OK;
        }

        
        
            CLI_print("InterfaceId %4d\n", port );
            CLI_print("%-40s : [ %4d]  [%s]\n", " state_lpm_dfe                          ", (entry.value.state_lpm_dfe), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " GTX_GTH_RX_DFE_Signal                  ", (entry.value.GTX_GTH_RX_DFE_Signal), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " Indicates_received                     ", (entry.value.Indicates_received), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " Indicates_disparity_err_received_data  ", (entry.value.Indicates_disparity_err_received_data), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " Indicates_status_RX_buffer             ", (entry.value.Indicates_status_RX_buffer), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " Indicates_status_TX_buffer             ", (entry.value.Indicates_status_TX_buffer), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " GT_Status_Rx_charisk                   ", (entry.value.GT_Status_Rx_charisk), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " GT_Status_RX_chariscomma               ", (entry.value.GT_Status_RX_chariscomma), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " GT_rxratedone                          ", (entry.value.GT_rxratedone), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " GT_eyescandataerror                    ", (entry.value.GT_eyescandataerror), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " GT_RX_PMA_reset_done                   ", (entry.value.GT_RX_PMA_reset_done), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " GT_PLL_lock                            ", (entry.value.GT_PLL_lock), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " GT_Status_RX_resetdone                 ", (entry.value.GT_Status_RX_resetdone), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " GT_Status_TX_resetdone                 ", (entry.value.GT_Status_TX_resetdone), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " RX_PRBS_Pattern_Checker_error          ", (entry.value.RX_PRBS_Pattern_Checker_error), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " RX_comma_is_detected_indication        ", (entry.value.RX_comma_is_detected_indication), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " Rx_Byte_is_realigned                   ", (entry.value.Rx_Byte_is_realigned), "");
            CLI_print("%-40s : [ %4d]  [%s]\n", " RX_Byte_is_aligned_indication          ", (entry.value.RX_Byte_is_aligned_indication), "");

        
        CLI_print(" ---- ------ ------ ---- ---- ------- ------ ------ ------ ------- - ------- ------ ------ ----- \n");

    }

    CLI_print("Done\n");



    return MEA_OK;
}




MEA_Status MEA_CLI_Interface_QSGMII_Conf(int argc, char *argv[])
{
    MEA_QSGMII_Entry_dbt  entry;
    MEA_Interface_t                 Id;
    int i;
    int num_of_params;


    if (argc < 2) {
        return MEA_ERROR;
    }

    Id = (MEA_Port_t)MEA_OS_atoiNum(argv[1]);


    if (MEA_API_Get_Interface_QSGMII_Entry(MEA_UNIT_0, Id, &entry) != MEA_OK){
        CLI_print("Error MEA_API_Get_Interface_SFP_PLUS_Entry failed port %d\n", Id);
        return MEA_OK;
    }

    for (i = (1); i<argc; i++){
        
        if (!MEA_OS_strcmp(argv[i], "-sgmii.Unidirectional_Enable")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.Sgmii_info.value.Unidirectional_Enable = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.Powerdown")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.Sgmii_info.value.Powerdown = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.Isolate")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.Sgmii_info.value.electrically_Isolate = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.speed")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.Sgmii_info.value.speed = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.PMA_RX")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.Sgmii_info.value.RX_PMA_reset = MEA_OS_atoiNum(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "-sgmii.PMA_TX")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.Sgmii_info.value.TX_PMA_reset = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.AN_reset")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.Sgmii_info.value.AN_reset = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-sgmii.sgmii_1000BASE")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.Sgmii_info.value.basex_or_sgmii = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "assert_en")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.Sgmii_info.value.assert_en = MEA_OS_atoiNum(argv[i + 1]);
        }




        /*-----------------------------------------------------------------------*/

        if (!MEA_OS_strcmp(argv[i], "-GT.reset_transmit")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.reset_full_transmit = MEA_OS_atoiNum(argv[i + 1]);
            

        }
        if (!MEA_OS_strcmp(argv[i], "-GT.reset_TX_PMA")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.reset_TX_PMA = MEA_OS_atoiNum(argv[i + 1]);


        }



        if (!MEA_OS_strcmp(argv[i], "-GT.reset_TX_PMA")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.reset_TX_PMA = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.reset_TX_PCS")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.reset_TX_PCS = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.RX_pcs_reset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.RX_pcs_reset = MEA_OS_atoiNum(argv[i + 1]);


        }



        if (!MEA_OS_strcmp(argv[i], "-GT.TX_diffctrl")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.TX_diffctrl = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.TX_postcursor")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.TX_postcursor = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.TX_precursor")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.TX_precursor = MEA_OS_atoiNum(argv[i + 1]);


        }
            
                
        if (!MEA_OS_strcmp(argv[i], "-GT.RX_buf_reset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.RX_buf_reset = MEA_OS_atoiNum(argv[i + 1]);


        }
            
        
        if (!MEA_OS_strcmp(argv[i], "-GT.RX_dfelpmreset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.RX_dfelpmreset = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.RX_dfeagcovrden")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.RX_dfeagcovrden = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.RX_lpm")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
            CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
            return MEA_ERROR;
            }
            entry.GT.value.RX_lpmen = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.TX_PRBS_Pattern")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.TX_PRBS_Pattern = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.TX_prbs_forceerr")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.TX_prbs_forceerr = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.RX_prbs_cntreset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.RX_prbs_cntreset = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.RX_prbssel")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.RX_prbssel = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.RX_reset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.RX_reset = MEA_OS_atoiNum(argv[i + 1]);


        }
        if (!MEA_OS_strcmp(argv[i], "-GT.RX_pma_reset")){
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry.GT.value.RX_pma_reset = MEA_OS_atoiNum(argv[i + 1]);


        }

    }

    if (MEA_API_Set_Interface_QSGMII_Entry(MEA_UNIT_0, Id, &entry) != MEA_OK){
        CLI_print("Error MEA_API_Set_Interface_QSGMII_Entry failed port %d\n", Id);
        return MEA_OK;
    }







    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_QSGMII_Get(int argc, char *argv[])
{
    MEA_Interface_t port, start_port, end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count = 0;

    MEA_QSGMII_Entry_dbt entry;


    if (argc < 2) {
        return MEA_ERROR;
    }

    //port      = MEA_OS_atoi(argv[1]);
    MEA_OS_strcpy(buf, argv[1]);
    if ((up_ptr = strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1], "all") == 0) {
            start_port = 0;
            end_port = MEA_INTERFACE_MAX_ID - 1;
        }
        else {
            port = start_port = end_port = MEA_OS_atoi(buf);
            if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_QSGMII, MEA_TRUE/*silent*/) == MEA_FALSE){
                CLI_print("Error MEA_API_Interface_Valid_TypeCorrect failed port %d is not SINGLE10G or not valid\n", port);
                return MEA_OK;
            }

        }
    }

    for (port = start_port; port <= end_port; port++){
        if (MEA_API_Interface_Valid_TypeCorrect(MEA_UNIT_0, port, MEA_INTERFACETYPE_QSGMII, MEA_TRUE/*silent*/) == MEA_FALSE)
            continue;
        MEA_OS_memset(&entry, 0, sizeof(entry));
        if (MEA_API_Get_Interface_QSGMII_Entry(MEA_UNIT_0, port, &entry) != MEA_OK){
            CLI_print("Error MEA_API_Get_Interface_RXAUI_Entry failed port %d\n", port);
            return MEA_OK;
        }
       
            CLI_print("  Interface QSGMII  Configure setting              \n");
            CLI_print("----------------------------------------------------------------- \n");
        
        count++;
        CLI_print("%-40s : %4d \n", "ID", port);
        CLI_print("%-40s : %4d \n", "Sgmii Unidirectional_Enable", entry.Sgmii_info.value.Unidirectional_Enable);
        CLI_print("%-40s : %4d \n", "Sgmii Powerdown",             entry.Sgmii_info.value.Powerdown);
        CLI_print("%-40s : %4d \n", "Sgmii electrically_Isolate",  entry.Sgmii_info.value.electrically_Isolate);
        CLI_print("----------\n");
        CLI_print("%-40s : %4d \n", "GT reset_full_transmit", entry.GT.value.reset_full_transmit);
        CLI_print("%-40s : %4d \n", "GT reset_TX_PMA", entry.GT.value.reset_TX_PMA);
        CLI_print("%-40s : %4d \n", "GT reset_TX_PCS", entry.GT.value.reset_TX_PCS);
        CLI_print("%-40s : %4d \n", "GT RX_pcs_reset", entry.GT.value.RX_pcs_reset);
        CLI_print("%-40s : %4d \n", "GT TX_diffctrl", entry.GT.value.TX_diffctrl);
        CLI_print("%-40s : %4d \n", "GT TX_postcursor", entry.GT.value.TX_postcursor);
        CLI_print("%-40s : %4d \n", "GT TX_precursor", entry.GT.value.TX_precursor);
        CLI_print("%-40s : %4d \n", "GT RX_buf_reset", entry.GT.value.RX_buf_reset);
        CLI_print("%-40s : %4d \n", "GT RX_dfelpmreset", entry.GT.value.RX_dfelpmreset);
        CLI_print("%-40s : %4d \n", "GT RX_dfeagcovrden", entry.GT.value.RX_dfeagcovrden);
        CLI_print("%-40s : %4d \n", "GT RX_lpmen", entry.GT.value.RX_lpmen);
        CLI_print("%-40s : %4d \n", "GT TX_PRBS_Pattern", entry.GT.value.TX_PRBS_Pattern);
        CLI_print("%-40s : %4d \n", "GT TX_prbs_forceerr", entry.GT.value.TX_prbs_forceerr);
        CLI_print("%-40s : %4d \n", "GT RX_prbs_cntreset", entry.GT.value.RX_prbs_cntreset);
        CLI_print("%-40s : %4d \n", "GT RX_prbssel", entry.GT.value.RX_prbssel);
        CLI_print("%-40s : %4d \n", "GT RX_reset", entry.GT.value.RX_reset);
        CLI_print("%-40s : %4d \n", "GT RX_pma_reset", entry.GT.value.RX_pma_reset);
        CLI_print("----------------------------------------------------------------- \n");




    }

    CLI_print("Done\n");


    return MEA_OK;
}


/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_CLI_Interface_Get_CurrentLinkStatus (int argc, char *argv[])
{
    MEA_Bool state;
    MEA_Port_t port,start_port,end_port;
    char buf[500];
    char *up_ptr;
    MEA_Uint32 count=0;
    if (argc < 2) {
        return MEA_ERROR;
    }

	MEA_OS_strcpy(buf, argv[1]);
    if((up_ptr=strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_port = MEA_OS_atoi(buf);
        end_port   = MEA_OS_atoi(up_ptr);
    }
    else
        {
            if (MEA_OS_strcmp(argv[1],"all") == 0) {
                start_port=0;
                end_port=MEA_INTERFACE_MAX_ID-2;
            } else {
                port=start_port = end_port = MEA_OS_atoi(buf);
            

            }
        }

        for(port=start_port;port<=end_port;port++)
        {
            state=MEA_FALSE;
            if(MEA_API_Get_Interface_CurrentLinkStatus(MEA_UNIT_0,port, &state ,MEA_TRUE) == MEA_OK){
                if(count==0){
                    CLI_print(" Interface CurrentLinkStatus \n");
                    CLI_print(" Id     state                 \n");
                    CLI_print("---- ---------- \n");
                }
                count++;
                    CLI_print(" %3d   %s\n", port,(state == MEA_TRUE) ? "UP" : "Down");


            }
        }

    return MEA_OK;
}




MEA_Status MEA_CLI_Interface_Config_Set(int argc, char *argv[])
{
    
    
    MEA_Interface_t Id, from_Id, to_Id;
    int i;
    int num_of_params;
    MEA_Uint32 tmp32;
    MEA_Interface_Entry_dbt     entry;
    
    
    MEA_OS_memset(&entry,0,sizeof(entry));

    if (argc < 4) {
        return MEA_ERROR;
    }

    

    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        from_Id = 0;
        to_Id = MEA_MAX_PORT_NUMBER;
    }
    else {
        Id = from_Id = to_Id = (MEA_Interface_t)MEA_OS_atoiNum(argv[1]);
        if (MEA_API_Interface_Valid(MEA_UNIT_0, Id, MEA_FALSE) != MEA_TRUE){
            CLI_print("Error MEA_API_Interface_Valid %d failed\n", Id);
            return MEA_OK;
        }
    }


    for (Id = from_Id; Id <= to_Id; Id++) {

        if (MEA_API_Interface_Valid(MEA_UNIT_0, Id, MEA_TRUE) != MEA_TRUE)
            continue;

        if (MEA_API_Get_Interface_ConfigureEntry(MEA_UNIT_0, Id, &entry) != MEA_OK){
            CLI_print("Error MEA_API_Interface_Valid %d failed\n", Id);
            return MEA_OK;
        }

        for (i = (2); i < argc; i++){
            if (MEA_OS_strcmp(argv[i], "-lb") == 0){
                num_of_params = 1;
                if (i + num_of_params > argc) {
                    CLI_print("error miss parameters  for %s \n", argv[i]);
                    return MEA_OK;
                }
                entry.loopback.lb_type = MEA_OS_atoi(argv[i + 1]);
            }
            if (MEA_OS_strcmp(argv[i], "-speed") == 0){
                num_of_params = 1;
                if (i + num_of_params > argc) {
                    CLI_print("error miss parameters  for %s \n", argv[i]);
                    return MEA_OK;
                }
                entry.speed = MEA_OS_atoi(argv[i + 1]);
            }
            if (MEA_OS_strcmp(argv[i], "-autoneg") == 0){
                num_of_params = 1;
                if (i + num_of_params > argc) {
                    CLI_print("error miss parameters  for %s \n", argv[i]);
                    return MEA_OK;
                }
                entry.autoneg = MEA_OS_atoi(argv[i + 1]);
            }
            if (MEA_OS_strcmp(argv[i], "-Tx_Disable") == 0) {
                num_of_params = 1;
                if (i + num_of_params > argc) {
                    CLI_print("error miss parameters  for %s \n", argv[i]);
                    return MEA_OK;
                }

                entry.Tx_Disable = MEA_OS_atoi(argv[i + 1]);
            }
            

            if (MEA_OS_strcmp(argv[i], "-s") == 0 ||
                MEA_OS_strcmp(argv[i], "-shaper") == 0){

                /*need to check if the shaper is egress port mode */

                /* */

                if (argc - i < 2)   {
                    CLI_print("Error: Minimum parameters for option -s is missing\n");
                    return MEA_ERROR;
                }
                tmp32 = MEA_OS_atoi(argv[i + 1]);
                if (tmp32 != 0)  {/* if enable need more parameters*/
                    entry.shaper_enable = ENET_TRUE;
                    if (argc - i < 8)   {
                        CLI_print("Error: Minimum parameters for option -shaper is missing\n");
                        return MEA_ERROR;
                    }
                    entry.shaper_info.CIR = MEA_OS_atoiNum64(argv[i + 2]);
                    entry.shaper_info.CBS = MEA_OS_atoiNum(argv[i + 3]);
                    tmp32 = MEA_OS_atoi(argv[i + 4]);
                    switch (tmp32)
                    {
                    case 0:
                        entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_PKT;
                        break;
                    case 1:
                    default:
                        entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
                        break;
                    }
                    tmp32 = MEA_OS_atoi(argv[i + 5]);
                    switch (tmp32)
                    {
                    case 0:
                        entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_CELL;
                        break;
                    case 3:
                        entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_AAL5;
                        break;

                    case 1:
                    default:
                        entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
                        break;
                    }
                    entry.shaper_info.overhead = MEA_OS_atoi(argv[i + 6]);
                    entry.shaper_info.Cell_Overhead = MEA_OS_atoi(argv[i + 7]);
                }
                else {
                    entry.shaper_enable = ENET_FALSE;
                }

            }//end shaper



        }

        if (MEA_API_Set_Interface_ConfigureEntry(MEA_UNIT_0, Id, &entry) != MEA_OK){
            CLI_print("Error MEA_API_Set_Interface_ConfigureEntry failed Id %d\n", Id);
            return MEA_OK;
        }
    }//for Id
    
    return MEA_OK;
}

MEA_Status MEA_CLI_Interface_Config_Get(int argc, char *argv[])
{

  MEA_Uint32 count;

    MEA_Interface_t               Id,from_Id,to_Id;
    MEA_Interface_Entry_dbt       entry;
    char str_mode[MEA_SHAPER_TYPE_LAST+1][6];
    char str[MEA_SERDES_LB_MAC+1][20];

    

    if(argc != 3  ) {
        return MEA_ERROR;
    }
    
    MEA_OS_strcpy(&(str[MEA_SERDES_LB_NONE][0]),"NONE");
    MEA_OS_strcpy(&(str[MEA_SERDES_LB_NEAR_PCS][0]),"NEAR_PCS");
    MEA_OS_strcpy(&(str[MEA_SERDES_LB_NEAR_PMA][0]),"NEAR_PMA");
    MEA_OS_strcpy(&(str[MEA_SERDES_LB_FAR_PMA][0]),"FAR_PMA");
    MEA_OS_strcpy(&(str[MEA_SERDES_LB_FAR_PCS][0]),"NEAR_PCS");
    MEA_OS_strcpy(&(str[MEA_SERDES_LB_MAC][0]),"MAC");



    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_MEF    ][0]),"MEF");
    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_BAG    ][0]),"BAG");
    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_LAST   ][0]),"ERORR");

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        from_Id = to_Id = MEA_OS_atoi(argv[1]);

        if (MEA_API_Interface_Valid(MEA_UNIT_0,from_Id,MEA_FALSE)==MEA_FALSE) { 
            return MEA_ERROR;
        }
    } else {
        from_Id = 0;
        to_Id   = MEA_MAX_PORT_NUMBER;
    }



   count=0;
    for (Id=from_Id; Id<=to_Id;  Id++) {

        if (MEA_API_Interface_Valid(MEA_UNIT_0,Id,MEA_TRUE)==MEA_FALSE) { 
            continue;
        }
        MEA_OS_memset(&entry,0,sizeof(entry));
        if (MEA_API_Get_Interface_ConfigureEntry(MEA_UNIT_0,Id,&entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_EgressPort_Entry for port %d failed\n",Id);
            return MEA_ERROR;
        }
        if (MEA_OS_strcmp(argv[2],"-lb") == 0) {
            if (count++ == 0) { 
                CLI_print ("Id   LB type \n"
                           "---- -------------\n"); 
            }
            CLI_print("%4d %s\n",
                Id,
                str[entry.loopback.lb_type] );
        
        
        
        
        }
        if (MEA_OS_strcmp(argv[2], "-autoneg") == 0) {
            if (count++ == 0) {
                CLI_print("Id   autoneg speed  MDIO\n");
                CLI_print("                    Dis\n"
                          "---- ------- ------ ----\n");
            }
            CLI_print("%4d %7s %d %4s\n",
                Id,
                MEA_STATUS_STR(entry.autoneg),
                entry.speed,
                MEA_STATUS_STR(entry.mdio_Disable)
                );




        }
        if (MEA_OS_strcmp(argv[2], "-Tx_Disable") == 0) {
            if (count++ == 0) {
                CLI_print("Id   Tx_Disable \n");
                CLI_print("                \n"
                          "---- ----------- \n");
            }
            CLI_print("%4d %7s \n",
                Id,
                MEA_STATUS_STR(entry.Tx_Disable)
            );

        }


        if (MEA_OS_strcmp(argv[2],"-shaper") == 0) {
            if (count++ == 0) { 
            CLI_print ("Id  ena Comp Spr ********   Shaper profile   ********\n"
                       "         Type Id  MODE Cir          Cbs     type overhead\n"
                       "---- --- ---- --- ---- ------------ ------- ---- --------\n"); 
            }
        

        if (entry.shaper_enable == ENET_TRUE) {
            CLI_print("%4d %3s %4d %3d %4s %12llu %7d %4s %8d\n",
                Id,
                "on",
                entry.Shaper_compensation,
                entry.shaper_Id,
                str_mode[entry.shaper_info.mode],
                entry.shaper_info.CIR,
                entry.shaper_info.CBS,
                (entry.shaper_info.resolution_type==MEA_SHAPER_RESOLUTION_TYPE_BIT) 
                ? "bps" : "pps",
                entry.shaper_info.overhead);
        }else{
            CLI_print("%4d Off\n",Id);
        }






        }/*end shaper*/
    
    } /* End for */

    if (MEA_OS_strcmp(argv[2],"-shaper") == 0) {
        if (count++ != 0) { 
            CLI_print ("---- --- ---- --- ---- ------------ ------- ---- --------\n"); 
        }
    }




    return MEA_OK;






    return MEA_OK;
}


/************************************************************************/
/*                CLI Interface_conf                                    */
/************************************************************************/



MEA_Status MEA_CLI_Interface_AddDebugCmds(void)
{

    
    CLI_defineCmd("MEA Interface config set ",
        (CmdFunc)MEA_CLI_Interface_Config_Set,
        "set config interface ",
        "config <id> \n"
        "        <id> - id  \n"
        "    option \n"
        "----------------\n"
        "    [-lb <Loop back type>]\n"
        "    [-shaper <shaper_Enable> <CIR> <CBS> <shaper_type> <compensation> <p_overhead> <c_overhead>]\n"
        "    [-speed <value>]\n"
        "    [-Tx_Disable <valid>]\n"

        "    [-autoneg  <valid>]\n"
       

        " option description \n"
        "--------------------------------------------\n"
        "    -lb (loop back port)\n"
        "               <type>                    \n"
        "                     0-no loop           \n"
        "                     1-Near-End PCS      \n"
        "                     2-Near-End PMA      \n"
        "                     4-Far-End  PMA      \n"
        "                     6-Far-End  PCS      \n"
        "                     7-Mac\n"
        "\n"
        "    -shaper (assign a shaper to the port]\n"
        "           <shaper_Enable> -  enable (1) / disable (0). \n"
        "           <CIR>           - Committed Information Rate [bps/pps, see shaper_type]\n"
        "           <CBS>           - Committed Burst Size [bytes/packets, see shaper_type]\n"
        "           <shaper_type>   - bit shaper (1) / packet shaper (0) \n"
        "           <compensation>  - 0 - chunk / 1-Packet / 3- chunk+Packet \n"
        "           <p_overhead>    - byte overhead for packet \n"
        "           <c_overhead>    - overhead for chunk \n"
        "\n"
        "    -Tx_Disable ( Transmit ) \n"
        "        <valid>  (0-Disable 1-Enable)  \n"
        "\n"
        "     -speed (speed rate)\n"
        "             0-1G 1-100M 2-10   \n"
        "     -autoneg ()\n"

        "Examples: - config set 0  -shaper 1 100000000 10000 1 1 20 0 \n" 
        "          - config set 12 -shaper 1 100000000 10000 1 1 20 0 \n"
        "          - config set 12 -lb 7 \n"
        
        "\n");
        
    CLI_defineCmd("MEA Interface config show",
        (CmdFunc)MEA_CLI_Interface_Config_Get,
        "Get Interface",
        "show <id>|all\n"
        "        <Id> - Interface OR all \n"
        "-------------------------------------\n"
        "    [-shaper]     \n"
        "-------------------------------------\n"
        "Examples: - show 1  \n" 
        "          - show all -shaper \n"
        "          - show all -lb     \n"
        "          - show all -autoneg  \n"
        
        );
   /***********************************************************/  
    
    CLI_defineCmd("MEA Interface Link status",
        (CmdFunc)MEA_CLI_Interface_Get_CurrentLinkStatus,
        "status Link status",
        "status <id>|all\n"
        "        <id> - id OR all \n"
        "Examples: - status 0  \n" 
        "          - status all\n");
 
    
    /***********************************************************/
    /*        Interface RGMII                                  */
    /***********************************************************/
    CLI_defineCmd("MEA Interface RGMII status",
        (CmdFunc)MEA_CLI_Interface_RGMII_status,
        "status RGMII",
        "status <port>|all\n"
        "        <port> - port OR all \n"
        "Examples: - status 0  \n" 
        "          - status all\n");


    /***********************************************************/
    /*        Interface SGMII                                  */
    /***********************************************************/

    CLI_defineCmd("MEA Interface SGMII status",
        (CmdFunc)MEA_CLI_Interface_SGMII_status,
        "status RGMII",
        "status <port>|all\n"
        "        <port> - port OR all \n"
        "Examples: - status 0  \n" 
        "          - status all\n");


    CLI_defineCmd("MEA Interface SGMII show",
        (CmdFunc)MEA_CLI_Interface_SGMII_Get,
        "Get Interface SGMII",
        "show <port>|all\n"
        "        <port> - port OR all \n"
        "Examples: - show 100  \n"
        "          - show all\n");





    CLI_defineCmd("MEA Interface SGMII set",
        (CmdFunc)MEA_CLI_Interface_SGMII_Conf,
        "set Interface SGMII ",
        "set <interfaceId>\n"
        " options \n"
        "     -sgmii.Unidirectional_Enable \n"
        "     -sgmii.Powerdown \n"
        "     -sgmii.electrically_Isolate \n"

        "     -sgmii.Isolate     \n"
        "     -sgmii.speed     0 -1GiGa  2-10M  3-100M \n"
        "     -sgmii.RX_PMA_reset   \n"
        "     -sgmii.TX_PMA_reset  \n"
        "     -sgmii.AN_reset  \n"

        "     -sgmii.sgmii_1000BSE   0- 1000BASE 1-SGMII\n"
        "     -sgmii.assert_en  \n"
      


        "Examples: set 100  -sgmii.speed 0  \n"
        "          set 100  -sgmii.speed 0  -sgmii.sgmii_1000BSE 0 \n");

    /***********************************************************/
    /*        Interface XLAUI                                  */
    /***********************************************************/

   
    

        CLI_defineCmd("MEA Interface XLAUI set",
        (CmdFunc)MEA_CLI_Interface_XLAUI_Conf,
            "set Interface XLAUI ",
            "set <interfaceId>\n"
            " options \n"
            
            "   -rx_enable  <valid>   - (0-Disable 1-Enable)\n"
            "   -rx_igncrc  <valid>   - (0-Disable 1-Enable)\n"
            "\n"
            "   -tx_enable  <valid>   - (0-Disable 1-Enable)\n"
            "   -tx_inscrc  <valid>   - (0-Disable 1-Enable)\n"

            "   -max_packetLen  <value>   - (0:16383)\n"
            "   -min_packetLen  <value>   - (64)\n"
            "\n"
			" -Txpause_th <value> 0: 0xffffffff \n"
			"\n"
            "   -check_udp    <valid>   - (0-Disable 1-Enable)   \n"
            "   -calc_udp     <valid>   - (0-Disable 1-Enable)    \n"
            "   -check_tcp    <valid>   - (0-Disable 1-Enable)   \n"
            "   -calc_tcp     <valid>   -(0-Disable 1-Enable)    \n"
            "   -rx_pause_en <valid>   - (0-Disable 1-Enable) \n"
            "\n"

            "Examples: set 127  -check_udp 0  -calc_udp 1 \n"
            "          set 127  -check_tcp 0  -calc_tcp 1 \n");

        CLI_defineCmd("MEA Interface XLAUI show",
            (CmdFunc)MEA_CLI_Interface_XLAUI_Get,
            "Get Interface XLAUI",
            "show <port>|all\n"
            "        <port> - port OR all \n"
            "Examples: - show 100  \n"
            "          - show all\n");


    /***********************************************************/
    /*        Interface QSGMII                                  */
    /***********************************************************/

    
        CLI_defineCmd("MEA Interface QSGMII show",
            (CmdFunc)MEA_CLI_Interface_QSGMII_Get,
            "Get Interface QSGMII",
            "show <port>|all\n"
            "        <port> - port OR all \n"
            "Examples: - show 100  \n"
            "          - show all\n");


       


    CLI_defineCmd("MEA Interface QSGMII set",
        (CmdFunc)MEA_CLI_Interface_QSGMII_Conf,
        "set Interface QSGMII ",
        "set <interfaceId>\n"
        " options \n"
        "     -sgmii.Unidirectional_Enable \n"
        "     -sgmii.Powerdown \n"
        "     -sgmii.electrically_Isolate \n"

        "     -sgmii.Isolate     \n"
        "     -sgmii.speed     0 -1GiGa  2-10M  3-100M \n"
        "     -sgmii.RX_PMA_reset   \n"
        "     -sgmii.TX_PMA_reset  \n"
        "     -sgmii.AN_reset  \n"

        "     -sgmii.sgmii_1000BSE   0- 1000BASE 1-SGMII\n"
        "     -sgmii.assert_en  \n"



        "------------------"
        "     -GT.reset_transmit            \n"
        "     -GT.reset_TX_PMA              \n"
        "     -GT.reset_TX_PCS              \n"
        "     -GT.RX_pcs_reset              \n"
        "     -GT.TX_diffctrl               \n"
        "     -GT.TX_postcursor             \n"
        "     -GT.TX_precursor              \n"
        "     -GT.RX_buf_reset              \n"
        "     -GT.RX_dfelpmreset            \n"
        "     -GT.RX_dfeagcovrden           \n"
        "     -GT.RX_lpm                    \n"
        "     -GT.TX_PRBS_Pattern           \n"
        "     -GT.TX_prbs_forceerr          \n"
        "     -GT.RX_prbs_cntreset          \n"
        "     -GT.RX_prbssel                \n"
        "     -GT.RX_reset                  \n"
        "     -GT.RX_pma_reset              \n"


        "Examples: set 100  -GT.RX_pma_reset 1 \n"
        "          set 100  -GT.RX_pma_reset 0\n");



    CLI_defineCmd("MEA Interface QSGMII status",
        (CmdFunc)MEA_CLI_Interface_QSGMII_status,
        "status QSGMII",
        "status <port>|all\n"
        "        <port> - port OR all \n"
        "Examples: - status 0  \n" 
        "          - status all\n");

    CLI_defineCmd("MEA Interface QSGMII GTstatus",
        (CmdFunc)MEA_CLI_Interface_QSGMII_GTstatus,
        "status QSGMII",
        "status <port>|all\n"
        "        <port> - port OR all \n"
        "Examples: - status 0  \n"
        "          - status all\n");





    /***********************************************************/
    /*        Interface Interlaken                             */
    /***********************************************************/
    CLI_defineCmd("MEA Interface Interlaken set",
                  (CmdFunc)MEA_CLI_Interface_Interlaken_Set,
                  "set Interlaken",
                  "set <interface>\n"
                  "        -rx_resync -(RX Re sync input)\n"
                  "                  <valid> - (0-Disable 1-Enable) \n"
                  "        -tx_enable -(RX Re sync input)\n"
                  "                  <valid> - (0-Disable 1-Enable) \n"
                  "       -rx_ctl_reset (RX Re sync input)\n"
                  "                  <valid> - (0-Disable 1-Enable) \n"
                  "       -tx_ctl_reset (RX Re sync input)\n"
                  "                  <valid> - (0-Disable 1-Enable) \n"
                  "        -burst -(bursts as described)\n"
                  "               <sort><max>\n"
                  "                  <short> \n" 
                  "                     0x0 = 64 bytes \n"    
                  "                     0x1 = 128 bytes\n"
                  "                     0x2 = 192 bytes\n"
                  "                     0x3 = 256 bytes\n"
                  "                  <max>  enable \n"
                  "                     0x0 = 32 bytes\n"
                  "                     0x1 = 64 bytes\n"
                  "                     0x2 = 96 bytes\n"
                  "                     0x3 = 128 bytes\n"
                  "                     0x4 = 160 bytes\n"
                  "                     0x5 = 192 bytes\n"
                  "                     0x6 = 224 bytes\n"
                  "                     0x7 = 256 bytes\n"
                  "        -lanestat -(Lane Status messaging)\n"
                  "                  <value>bit per lane 0..0x3f\n"
                  "        -fc_callen -(flow control calendar length)\n"
                  "                  <value> \n"
                  "                         0x0 = 16 entries\n"
                  "                         0x1 = 32 entries\n"
                  "                         0x3 = 64 entries\n"
                  "                         0x7 = 128 entries\n"
                  "                         0xF = 256 entries\n"
                  "        -ctl_fc (flow control enable for rx IL)\n"
                  "                 <valid> - (0-Disable 1-Enable)\n"
                  "        -ctl_tx_fc (flow control enable for tx IL)\n"
                  "                 <valid> - (0-Disable 1-Enable)\n"
                  
                  "        -rdyout_th -(Threshold value)\n"
                  "                  <value>  0..7\n"
                  "        -tx_rdyout_thresh -(Threshold value)\n"
                  "                  <value>  0..7\n"
                  "        -tx_rlim -(TX Rate Limiting)\n"
                  "                   <rlim_enable> - (0-Disable 1-Enable)\n"
                  "                   <rlim_delta>  - value \n"
                  "                   <rlim_intv>   - value\n"
                  "                   <rlim_max>    - value \n"

                  "        -lb_mac -(loopback mac)\n"
                  "                  <type> - (0- none 1-Enable) \n"
                  "       -loopback  (loopback configure)\n"
                  "              <type> 0-normal     no loop  \n"
                  "                     1-Near-End PCS  (internal)    \n"
                  "                     2-Near-End PMA  (internal)    \n"
                  "                     4-Far-End  PMA  (external)    \n"
                  "                     6-Far-End  PCS  (external)    \n"
                  "Examples: - set 0 -fc_callen 16  \n" 
                  "          - set 0  \n");

    CLI_defineCmd("MEA Interface Interlaken show",
        (CmdFunc)MEA_CLI_Interface_Interlaken_Get,
        "Get Interlaken",
        "show <port>|all\n"
        "        <port> - port OR all [-tx_rlim]\n"
        "-------------------------------------\n"
        "    -tx_rlim  TX Rate Limiting \n"
        "-------------------------------------\n"
        "Examples: - show 1  \n" 
        "          - show all\n");

    CLI_defineCmd("MEA Interface Interlaken status",
        (CmdFunc)MEA_CLI_Interface_Interlaken_status,
        "status Interlaken",
        "status <port>|all\n"
        "        <port> - port OR all \n"
        "Examples: - status 1  \n" 
        "          - status all\n");
    /***********************************************************/
    /*        Interface RXAUI                                  */
    /***********************************************************/
    CLI_defineCmd("MEA Interface RXAUI set",
        (CmdFunc)MEA_CLI_Interface_RXAUI_Set,
        "set Interface_RXAUI",
        "set <port>\n"
        "       -loopback  (loopback configure)\n"
        "              <type> 0-normal     no loop  \n"
      
        "                     4-Far-End loop      \n"
        "                     6-Far-End loop and forward  \n"

        "        -lb_mac -(loopback mac)\n"
        "                  <type> - (0- none 1-Enable) \n"
        "        -power_tx -(Power down transceiver)\n"
        "                  <valid> - (0-Disable 1-Enable) \n"
        "        -reset_f_status -(reset_fault_status)\n"
        "                  <valid> - (0-Disable 1-Enable) \n"
        "        -reset_rx_link -(reset_rx_link)\n"
        "                  <valid> - (0-Disable 1-Enable) \n"
        "        -ifg (ifg mode)\n"
        "         <val> - (1 = WAN, 0 = LAN) \n"
        "         <dic>    0 - no DIC, \n" 
        "                  1 - DIC enable average ipg- 12(9-15)  \n"
        "                  2 - DIC enable average ipg - 8(5-11)  \n"
        "                  3 - DIC enable average ipg - 6 (5-8)  \n"
        "        -reset_serdes -(-reset serdes type)\n"
        "                  <type> -                   \n"
        "                          0 no reset         \n"
        "                          1 RX reset         \n"
        "                          2 TX reset         \n"
        "                          3 Global Tx and RX \n"

        "Examples: - set 118 -lb_mac 0 -loopback 1  \n" 
        "          - set 119 -loopback  0  \n");

    CLI_defineCmd("MEA Interface RXAUI show",
        (CmdFunc)MEA_CLI_Interface_RXAUI_Get,
        "Get Interface_RXAUI",
        "show <port>|all\n"
        "        <port> - port OR all \n"
        "Examples: - show 118  \n" 
        "          - show all\n");

    CLI_defineCmd("MEA Interface RXAUI status",
        (CmdFunc)MEA_CLI_Interface_RXAUI_status,
        "Get Interface_RXAUI status",
        "status <port>|all\n"
        "        <port> - poer OR all \n"
        "Examples: - status 1  \n" 
        "          - status all\n");
 /***********************************************************/
    CLI_defineCmd("MEA Interface SFP_PLUS set",
        (CmdFunc)MEA_CLI_Interface_SFP_PLUS_Set,
        "set Interface_SFP_PLUS",
        "set <interfaceId>\n"
        "    -ifg (ifg mode)\n"
        "         <val> - (1 = WAN, 0 = LAN) \n"
        "         <dic>    0 - no DIC, \n"
        "                  1 - DIC enable average ipg- 12(9-15)  \n"
        "                  2 - DIC enable average ipg - 8(5-11)  \n"
        "                  3 - DIC enable average ipg - 6 (5-8)  \n"
       
        "    -rx_pause (rx pause)\n"
        "           <valid> -(0 - Disable 1 - Enable)\n"

        "    -pma_reset (pma reset ) \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -pca_reset (pca reset)\n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -pma_tx_disable\n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -RX_pattern_check\n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -TX_pattern\n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -data_pattern_select\n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -test_pattern_select\n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -prbs31_tx_pattern\n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -prbs31_rx_pattern\n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_rxrate  () \n"
        "              <value> - (0-7)\n"
        "    -gt0_txdiffctrl  () \n"
        "              <value> - (0-15)\n"
        "    -gt0_rxlpmen  () \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_rxdfelpmreset   () \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_rxpmareset   () \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_txpmareset () \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_rxpolarity() \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_txpolarity () \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_txprbsforceerr () \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_rxcdrhold () \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_eyescantrigger () \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_eyescanreset () \n"
        "              <valid> - (0-Disable 1-Enable)\n"
        "    -gt0_txprecursor () \n"
        "              <value> - (0-31)\n"
        "    -gt0_txpostcursor () \n"
        "              <value> - (0-31)\n"
        "    -rx_block_th ()\n"
        "                     <value> 0:16653\n"




        "Examples: - set 104 -ifg 0 0   \n"
        "          - set 105 -ifg 1 3  \n");

    CLI_defineCmd("MEA Interface SFP_PLUS show",
        (CmdFunc)MEA_CLI_Interface_SFP_PLUS_Get,
        "Get Interface_SFP_PLUS",
        "show <Id>|all\n"
        "        <port> - iD OR all \n"
        "Examples: - show 118  \n"
        "          - show all\n");

    CLI_defineCmd("MEA Interface SFP_PLUS status",
        (CmdFunc)MEA_CLI_Interface_SFP_PLUS_status,
        "Get Interface_RXAUI status",
        "status <ID>|all\n"
        "        <ID> - Id OR all \n"
        "Examples: - status 1  \n"
        "          - status all\n");
    /******************************************/






    /***********************************************************/

      CLI_defineCmd("MEA Interface G999 set pause_threshold",

        (CmdFunc)MEA_CLI_Set_Interface_G999_1_pause_threshold,

        "Set Interface G999 pause_threshold\n",

        "Usage  : G999 pause_threshold <pause_enable><threshold_value>\n"
        "\n"
        "Example: - pause_threshold 1 128 \n"
        "           pause_threshold 0 128 \n"
        );

      CLI_defineCmd("MEA Interface G999 set mng_channel",

          (CmdFunc)MEA_CLI_Set_Interface_G999_1_mng_channel,

          "Set Interface G999 mng_channel\n",

          "Usage  : mng_channel <valu>\n"
          "\n"
          "Example: - mng_channel    64 \n");

      

	  CLI_defineCmd("MEA Interface G999 set packet_threshold",

		  (CmdFunc)MEA_CLI_Set_Interface_G999_1_packet_9991_threshold,

		  "Set Interface G999packet_threshold\n",

		  "Usage  : G999 packet_threshold <threshold_value>\n"
		  "\n"
		  "Example: - packet_threshold  \n"
		  "            \n"
		  );


	  
#if 0
     
    CLI_defineCmd("MEA Interface G999 set SID",

        (CmdFunc)MEA_CLI_Set_Interface_G999_1_set_SID,

        "Set interface global G999 SID\n",

        "Usage  : SID  -id <0:2>< inside><tx_val>\n"
        "Usage  : SID  -id <0:2>< inside><tx_val>\n"
        "\n"
        "Example: - SID  -id 0 0 0\n"
        "           SID  -id 1 1 0 \n"
        "           SID  -id 2 2 0 \n"
        "           SID  -increment 1 \n"

        );
#endif

    

    CLI_defineCmd("MEA Interface G999 show ",

        (CmdFunc)MEA_CLI_Get_Interface_G999_1,

        "show interface global G999_1 \n",

        "Usage  : show G999_1 all parameters\n"
        "\n"
        "Example: - show all \n"
        "           show all \n"
        );
//////////////////////////////////////////////////////////////////////////

    




    return MEA_OK;
}

