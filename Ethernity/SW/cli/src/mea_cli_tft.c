/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_tft.h"


static MEA_Status MEA_CLI_TFT_Create_QoS_Profile(int argc, char *argv[])
{
    MEA_TFT_t  tft_Id;
    MEA_TFT_entry_data_dbt entry;
    int i;

    if (argc < 2) {
        return MEA_ERROR;
    }
    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (MEA_OS_strcmp(argv[1], "auto") == 0) {
        tft_Id = MEA_PLAT_GENERATE_NEW_ID;
    }
    else {
        tft_Id = MEA_OS_atoiNum(argv[1]);
    }
    for (i = 1; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "-rule_type")) {
            entry.en_rule_type = MEA_TRUE;
            entry.rule_type = MEA_OS_atoiNum(argv[i + 1]);
        }
    }


    if (MEA_API_TFT_Create_QoS_Profile(MEA_UNIT_0, &tft_Id, &entry) != MEA_OK) {
        CLI_print("Error MEA_API_TFT_Create_QoS_Profile failed \n");
        return MEA_OK;
    }
   
    return MEA_OK;
}

static MEA_Status MEA_CLI_TFT_Delete_QoS_Profile(int argc, char *argv[])
{

    MEA_Uint16 Id;
    MEA_Uint16 from_Id;
    MEA_Uint16 to_Id;
    MEA_Bool exist;

    if (argc < 2) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_Id = 0;
        to_Id = MEA_TFT_L3L4_MAX_PROFILE - 1;

    }
    else {
        from_Id = MEA_OS_atoiNum(argv[1]);
        to_Id = MEA_OS_atoiNum(argv[1]);
        Id = from_Id;
        if (MEA_API_TFT_QoS_Profile_IsExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
            CLI_print("Error  MEA_API_TFT_Prof_Mask_IsExist failed  MaskId=%d\n", Id);
            return MEA_OK;
        }
    }


    for (Id = from_Id; Id <= to_Id; Id++)
    {
        exist = MEA_FALSE;

        if (MEA_API_TFT_QoS_Profile_IsExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
            continue;

        if (!exist)
            continue;



        if (MEA_API_TFT_Delete_QoS_Profile(MEA_UNIT_0, Id) != MEA_OK)
        {
            CLI_print("Error  MEA_API_TFT_Delete_QoS_Profile failed \n");
            return MEA_OK;
        }
    }
        
    
    return MEA_OK;
}


static MEA_Status MEA_CLI_TFT_Set_ClassificationRule(int argc, char *argv[])
{
    int i;
    MEA_TFT_t tft_Id;
    MEA_Uint8 index;
    MEA_TFT_data_dbt  entry;
    
    MEA_Bool exist   =MEA_FALSE;


    


    MEA_OS_memset(&entry, 0, sizeof(MEA_TFT_data_dbt));
    





    if (argc < 3) {
        return MEA_ERROR;
    }

    tft_Id = MEA_OS_atoiNum(argv[1]);
    index = MEA_OS_atoiNum(argv[2]);
    


    if (MEA_API_TFT_QoS_Profile_IsExist(MEA_UNIT_0, tft_Id, MEA_FALSE, &exist) != MEA_OK)
    {
        CLI_print("Error  MEA_API_TFT_QoS_Profile_IsExist %d \n", tft_Id);
        return MEA_OK;
    }
    if (!exist) {
        CLI_print("Error   tft_Id %d not exist\n", tft_Id);
        return MEA_OK;
    }


    if(MEA_API_TFT_Get_ClassificationRule(MEA_UNIT_0, tft_Id, index, &entry) != MEA_OK)
    {
        CLI_print("Error  MEA_API_TFT_Get_ClassificationRule %d \n", tft_Id);
        return MEA_OK;
    }

    for (i = 3; i < argc; i++){
        if (!MEA_OS_strcmp(argv[i], "type")) {
            entry.TFT_info.Type = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "Priority_Rule")) {
            entry.TFT_info.Priority_Rule = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "QoS_Index")) {
            entry.TFT_info.QoS_Index = MEA_OS_atoiNum(argv[i + 1]);
        }

       
        

        /* MEA_TFT_TYPE_L2 */
        if (!MEA_OS_strcmp(argv[i], "L2_pri")){
            entry.TFT_info.L2_enable        = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.L2_Pri           = MEA_OS_atoiNum(argv[i + 2]);
                
        }
        if (!MEA_OS_strcmp(argv[i], "L3_ethtype")) {
            entry.TFT_info.L3_EtherType = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "DSCP")) {
            entry.TFT_info.L3_DSCP_valid = MEA_OS_atoiNum(argv[i + 1]);

            entry.TFT_info.L3_from_DSCP = MEA_OS_atoiNum(argv[i + 2]);
            entry.TFT_info.L3_end_DSCP = MEA_OS_atoiNum(argv[i + 3]);
            
        }
        if (!MEA_OS_strcmp(argv[i], "IP_protocol")) {
            entry.TFT_info.IP_protocol_valid = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.IP_protocol = MEA_OS_atoiNum(argv[i + 2]);
        }
        if (!MEA_OS_strcmp(argv[i], "src_ipv4")) {
            entry.TFT_info.Source_IPv4_valid = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.Source_IPv4       = MEA_OS_inet_addr(argv[i + 2]);
            entry.TFT_info.Source_IPv4_mask  = MEA_OS_atoiNum(argv[i + 3]);
        
        }
        if (!MEA_OS_strcmp(argv[i], "dest_ipv4")) {
            entry.TFT_info.Dest_IPv4_valid = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.Dest_IPv4 = MEA_OS_inet_addr(argv[i + 2]);
            entry.TFT_info.Dest_IPv4_mask = MEA_OS_atoiNum(argv[i + 3]);

        }
        {
            unsigned char buf[sizeof(struct in6_addr)];
            int  s;
            //char str[INET6_ADDRSTRLEN];

            if (!MEA_OS_strcmp(argv[i], "ipv6")) {
                /* ------- TBD ---- */
                entry.TFT_info.Ipv6_valid = MEA_OS_atoiNum(argv[i + 1]);
                entry.TFT_info.Ipv6_mask = MEA_OS_atoiNum(argv[i + 3]);
                if (entry.TFT_info.Ipv6_valid == MEA_TRUE) {
                    s = inet_pton(AF_INET6, argv[i + 2], buf);
                    if (s <= 0) {
                        CLI_print("ipv6 format error\n");
                        return MEA_ERROR;
                    }
                    entry.TFT_info.Ipv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
                    entry.TFT_info.Ipv6[1] = (buf[8 ] << 24) | (buf[9 ] << 16) | (buf[10] << 8) | buf[11] << 0;
                    entry.TFT_info.Ipv6[2] = (buf[4 ] << 24) | (buf[5 ] << 16) | (buf[6 ] << 8) | buf[7 ] << 0;
                    entry.TFT_info.Ipv6[3] = (buf[0 ] << 24) | (buf[1 ] << 16) | (buf[2 ] << 8) | buf[3 ] << 0;
                }
                else {
                    entry.TFT_info.Ipv6[0] = 0;
                    entry.TFT_info.Ipv6[1] = 0;
                    entry.TFT_info.Ipv6[2] = 0;
                    entry.TFT_info.Ipv6[3] = 0;
                    entry.TFT_info.Ipv6_mask = 0;
                }


            }
        }
        if (!MEA_OS_strcmp(argv[i], "flow_lable")) {
            entry.TFT_info.Flow_Label = MEA_OS_atoiNum(argv[i + 1]);
        }

        if (!MEA_OS_strcmp(argv[i], "L4_srcport")) {

            entry.TFT_info.L4_Source_port_valid = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.L4_from_Source_port = MEA_OS_atoiNum(argv[i + 2]);
            entry.TFT_info.L4_end_Source_port = MEA_OS_atoiNum(argv[i + 3]);
        }
        if (!MEA_OS_strcmp(argv[i], "L4_destport")) {
            entry.TFT_info.L4_dest_valid         = MEA_OS_atoiNum(argv[i + 1]);
            entry.TFT_info.L4_from_Dest_port     = MEA_OS_atoiNum(argv[i + 2]);
            entry.TFT_info.L4_end_Dest_port      = MEA_OS_atoiNum(argv[i + 3]);
        }
    
    }



    
       
 

    if (MEA_API_TFT_Set_ClassificationRule(MEA_UNIT_0, tft_Id, index, &entry) != MEA_OK)
    {
        CLI_print("Error MEA_API_TFT_Add_EntryData failed \n");
        return MEA_OK;
    }


    return MEA_OK;
}


static MEA_Status MEA_CLI_TFT_Get_ClassificationRule(int argc, char *argv[])
{
    int i;
    MEA_TFT_t tft_Id, from_tft_Id, To_tft_Id;
    MEA_Uint8 index, fromPri, ToPri;
    MEA_TFT_data_dbt  entry;
    
    //MEA_Uint32  count = 0;
    MEA_Bool exist;
    char                     Ip_str[20];
    MEA_Uint32 IPaddress;

    unsigned char buf[sizeof(struct in6_addr)];
    //int  s;
    char str[INET6_ADDRSTRLEN];


    MEA_TFT_key_t TFT_info;

    char info_TFT_Type[MEA_TFT_TYPE_LAST][20];

    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_DISABLE][0]), "DISABLE");
    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_NO_IP][0]),   "NO_IP");
    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_IPV4][0]),    "IPV4");
    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_IPV6][0]),    "IPV6-Dest");
    MEA_OS_strcpy(&(info_TFT_Type[MEA_TFT_TYPE_IPV6_Source][0]), "IPV6-source");
    


    MEA_OS_memset(&TFT_info, 0, sizeof(MEA_TFT_key_t));




    if (argc < 3) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_tft_Id = 0;
        To_tft_Id = MEA_TFT_L3L4_MAX_PROFILE - 1;
    }
    else {
        from_tft_Id = MEA_OS_atoiNum(argv[1]);
        To_tft_Id = from_tft_Id;

    }

    if (MEA_OS_strcmp(argv[2], "all") == 0) {
        fromPri = 0;
        ToPri = MEA_TFT_L3L4_MAX_Rule - 1;


    }
    else {
        fromPri = MEA_OS_atoiNum(argv[2]);
        ToPri = fromPri;

    }

    for (tft_Id = from_tft_Id; tft_Id <= To_tft_Id; tft_Id++) {
        if (MEA_API_TFT_QoS_Profile_IsExist(MEA_UNIT_0, tft_Id, MEA_TRUE, &exist) != MEA_OK)
            continue;
        if (!exist)
            continue;
        CLI_print("------------------------------------ \n");
        CLI_print("         TFT_Profile Id:%4d \n", tft_Id);
        CLI_print("------------------------------------ \n");
        for (index = fromPri; index <= ToPri; index++)
        {
            MEA_OS_memset(&entry, 0, sizeof(MEA_TFT_data_dbt));
            if (MEA_API_TFT_Get_ClassificationRule(MEA_UNIT_0, tft_Id, index, &entry) != MEA_OK) {
                CLI_print("Error MEA_API_TFT_Get_ClassificationRule tft_Id[%2d][%2d]\n", tft_Id, index);
                continue;
            }


            if (entry.TFT_info.Type == MEA_TFT_TYPE_DISABLE) {
                //CLI_print("index:%2d Type: %10s  \n", index, info_TFT_Type[entry.TFT_info.Type], entry.TFT_info.Priority_Rule, entry.TFT_info.QoS_Index);
                continue;
            }

            CLI_print(" index:%2d Type: %15s >> Priority_Rule=%2d QoS=%2d \n", index, info_TFT_Type[entry.TFT_info.Type], entry.TFT_info.Priority_Rule, entry.TFT_info.QoS_Index);


            if (entry.TFT_info.Type == MEA_TFT_TYPE_NO_IP) {



            }

            if (MEA_HPM_NEW_SUPPORT)
            {
                if ((entry.TFT_info.Type == MEA_TFT_TYPE_IPV4) ||
                    (entry.TFT_info.Type == MEA_TFT_TYPE_NO_IP))
                {

                    if (entry.TFT_info.L2_enable == MEA_TRUE) {
                        CLI_print(" L2_Priority -  pri=0x%2x  ", entry.TFT_info.L2_Pri);
                        for (i = 0; i < 8; i++) {

                            CLI_print(" .p[%d]=[%s] ", i, MEA_STATUS_STR(((entry.TFT_info.L2_Pri >> i) & 0x1)));

                        }
                        CLI_print("\n");

                    }
                    else {
                        CLI_print(" L2_Priority -  D.C  \n");
                    }
                    if (entry.TFT_info.L3_EtherType != 0) {
                        CLI_print(" L3_EtherType %d \n", entry.TFT_info.L3_EtherType);
                    }
                    else {
                        CLI_print(" L3_EtherType D.C \n");
                    }

                    if (entry.TFT_info.Type != MEA_TFT_TYPE_NO_IP) {
                        if (entry.TFT_info.L3_DSCP_valid == MEA_TRUE) {
                            CLI_print(" DSCP:    %d:%d \n",
                                entry.TFT_info.L3_from_DSCP,
                                entry.TFT_info.L3_end_DSCP);
                        }
                        else {
                            CLI_print(" DSCP:   %s \n", "NA");
                        }
                    }
                    if (entry.TFT_info.Dest_IPv4_valid == MEA_TRUE) {
                        IPaddress = entry.TFT_info.Dest_IPv4;
                        MEA_OS_inet_ntoa(MEA_OS_htonl(IPaddress), Ip_str, sizeof(Ip_str));
                        CLI_print(" DestIPv4: %s    mask:%d \n", Ip_str, entry.TFT_info.Dest_IPv4_mask);
                    }
                    if (entry.TFT_info.Source_IPv4_valid == MEA_TRUE) {
                        IPaddress = entry.TFT_info.Source_IPv4;
                        MEA_OS_inet_ntoa(MEA_OS_htonl(IPaddress), Ip_str, sizeof(Ip_str));
                        CLI_print(" SrcIPv4: %s    mask:%d \n", Ip_str, entry.TFT_info.Source_IPv4_mask);
                    }
                    if (entry.TFT_info.IP_protocol_valid) {
                        CLI_print(" IP-protocol[YES]=  %4d \n", entry.TFT_info.IP_protocol);
                    }
                    if (entry.TFT_info.Type != MEA_TFT_TYPE_NO_IP) {
                        if (entry.TFT_info.L4_Source_port_valid) {
                            CLI_print(" L4srcPort:  %4d : %4d  \n",
                                entry.TFT_info.L4_from_Source_port,
                                entry.TFT_info.L4_end_Source_port);
                        }
                        else {
                            CLI_print(" L4srcPort:  D.C\n");
                        }
                        if (entry.TFT_info.L4_dest_valid) {
                            CLI_print(" L4DestPort:  %4d : %4d \n",
                                entry.TFT_info.L4_from_Dest_port,
                                entry.TFT_info.L4_end_Dest_port);
                        }
                        else {
                            CLI_print(" L4DestPort:  DC\n");
                        }
                    }



                } //end MEA_TFT_TYPE_IPV4
                if ((entry.TFT_info.Type == MEA_TFT_TYPE_IPV6) ||
                    (entry.TFT_info.Type == MEA_TFT_TYPE_IPV6_Source))
                {


                    if (entry.TFT_info.L3_DSCP_valid == MEA_TRUE) {
                        CLI_print(" DSCP:   %d:%d \n",
                            entry.TFT_info.L3_from_DSCP,
                            entry.TFT_info.L3_end_DSCP);
                    }
                    else {
                        CLI_print(" DSCP:   %s \n", "NA");
                    }

                    if (entry.TFT_info.Ipv6_valid == MEA_TRUE) {

                        buf[0] = (entry.TFT_info.Ipv6[3] >> 24) & 0xff;
                        buf[1] = (entry.TFT_info.Ipv6[3] >> 16) & 0xff;
                        buf[2] = (entry.TFT_info.Ipv6[3] >> 8) & 0xff;
                        buf[3] = (entry.TFT_info.Ipv6[3] >> 0) & 0xff;

                        buf[4] = (entry.TFT_info.Ipv6[2] >> 24) & 0xff;
                        buf[5] = (entry.TFT_info.Ipv6[2] >> 16) & 0xff;
                        buf[6] = (entry.TFT_info.Ipv6[2] >> 8) & 0xff;
                        buf[7] = (entry.TFT_info.Ipv6[2] >> 0) & 0xff;

                        buf[8] = (entry.TFT_info.Ipv6[1] >> 24) & 0xff;
                        buf[9] = (entry.TFT_info.Ipv6[1] >> 16) & 0xff;
                        buf[10] = (entry.TFT_info.Ipv6[1] >> 8) & 0xff;
                        buf[11] = (entry.TFT_info.Ipv6[1] >> 0) & 0xff;

                        buf[12] = (entry.TFT_info.Ipv6[0] >> 24) & 0xff;
                        buf[13] = (entry.TFT_info.Ipv6[0] >> 16) & 0xff;
                        buf[14] = (entry.TFT_info.Ipv6[0] >> 8) & 0xff;
                        buf[15] = (entry.TFT_info.Ipv6[0] >> 0) & 0xff;





                        if (inet_ntop(AF_INET6, buf, str, INET6_ADDRSTRLEN) == NULL) {
                            CLI_print(">>>inet_ntop<<< \n");
                        }

                        CLI_print(" Ipv6 %s  mask: %d \n", str, entry.TFT_info.Ipv6_mask);
                        CLI_print(" Flow_Label 0x%05x \n", entry.TFT_info.Flow_Label);

                    }


                    if (entry.TFT_info.IP_protocol_valid) {
                        CLI_print(" IP-protocol[YES]=  %4d \n", entry.TFT_info.IP_protocol);
                    }
                    else {
                        CLI_print(" IP-protocol[NO ]=  0x%x \n", 0xff);
                    }
                    if (entry.TFT_info.L4_Source_port_valid) {
                        CLI_print(" L4srcPort:  %4d : %4d  \n",
                            entry.TFT_info.L4_from_Source_port,
                            entry.TFT_info.L4_end_Source_port);
                    }
                    else {
                        CLI_print(" L4srcPort:  D.C\n");
                    }
                    if (entry.TFT_info.L4_dest_valid) {
                        CLI_print(" L4DestPort:  %4d : %4d \n",
                            entry.TFT_info.L4_from_Dest_port,
                            entry.TFT_info.L4_end_Dest_port);
                    }
                    else {
                        CLI_print(" L4DestPort:  DC\n");
                    }


                }//end MEA_TFT_TYPE_IPV6

            }

            if (!MEA_HPM_NEW_SUPPORT)
            {

                if (entry.TFT_info.L2_enable == MEA_TRUE) {
                    CLI_print(" L2_Priority -  pri=0x%2x  ", entry.TFT_info.L2_Pri);
                    for (i = 0; i < 8; i++) {

                        CLI_print(" .p[%d]=[%s] ", i, MEA_STATUS_STR(((entry.TFT_info.L2_Pri >> i) & 0x1)));

                    }
                    CLI_print("\n");

                }
                else {
                    CLI_print(" L2_Priority -  D.C  \n");
                }


                if (entry.TFT_info.L3_EtherType != 0) {
                    CLI_print(" L3_EtherType %d \n", entry.TFT_info.L3_EtherType);
                }
                else {
                    CLI_print(" L3_EtherType D.C \n");
                }

                if (entry.TFT_info.L3_DSCP_valid == MEA_TRUE) {
                    CLI_print(" L3_pri   %d:%d \n",
                        entry.TFT_info.L3_from_DSCP,
                        entry.TFT_info.L3_end_DSCP);
                }

                if (entry.TFT_info.IP_protocol_valid) {
                    CLI_print(" IP-protocol[YES]=  %4d \n", entry.TFT_info.IP_protocol);
                }
                else {
                    CLI_print(" IP-protocol[NO ]=  0x%x \n", 0xff);
                }



                if (entry.TFT_info.Ipv6_valid == MEA_TRUE) {

                    buf[0] = (entry.TFT_info.Ipv6[3] >> 24) & 0xff;
                    buf[1] = (entry.TFT_info.Ipv6[3] >> 16) & 0xff;
                    buf[2] = (entry.TFT_info.Ipv6[3] >> 8) & 0xff;
                    buf[3] = (entry.TFT_info.Ipv6[3] >> 0) & 0xff;

                    buf[4] = (entry.TFT_info.Ipv6[2] >> 24) & 0xff;
                    buf[5] = (entry.TFT_info.Ipv6[2] >> 16) & 0xff;
                    buf[6] = (entry.TFT_info.Ipv6[2] >> 8) & 0xff;
                    buf[7] = (entry.TFT_info.Ipv6[2] >> 0) & 0xff;

                    buf[8] = (entry.TFT_info.Ipv6[1] >> 24) & 0xff;
                    buf[9] = (entry.TFT_info.Ipv6[1] >> 16) & 0xff;
                    buf[10] = (entry.TFT_info.Ipv6[1] >> 8) & 0xff;
                    buf[11] = (entry.TFT_info.Ipv6[1] >> 0) & 0xff;

                    buf[12] = (entry.TFT_info.Ipv6[0] >> 24) & 0xff;
                    buf[13] = (entry.TFT_info.Ipv6[0] >> 16) & 0xff;
                    buf[14] = (entry.TFT_info.Ipv6[0] >> 8) & 0xff;
                    buf[15] = (entry.TFT_info.Ipv6[0] >> 0) & 0xff;





                    if (inet_ntop(AF_INET6, buf, str, INET6_ADDRSTRLEN) == NULL) {
                        CLI_print(">>>inet_ntop<<< \n");
                    }

                    CLI_print(" destIpv6 %s  mask %d \n", str, entry.TFT_info.Ipv6_mask);

                }
                else {

                    if (entry.TFT_info.Dest_IPv4_valid == MEA_TRUE) {
                        IPaddress = entry.TFT_info.Dest_IPv4;
                        MEA_OS_inet_ntoa(MEA_OS_htonl(IPaddress), Ip_str, sizeof(Ip_str));
                        CLI_print(" DestIPv4: %s  %d \n", Ip_str, entry.TFT_info.Dest_IPv4_mask);
                    }
                    if (entry.TFT_info.Source_IPv4_valid == MEA_TRUE) {
                        IPaddress = entry.TFT_info.Source_IPv4;
                        MEA_OS_inet_ntoa(MEA_OS_htonl(IPaddress), Ip_str, sizeof(Ip_str));
                        CLI_print(" SrcIPv4: %s %d \n", Ip_str, entry.TFT_info.Source_IPv4_mask);
                    }


                    if (entry.TFT_info.L4_Source_port_valid) {
                        CLI_print(" L4srcPort  %4d : %4d  \n",
                            entry.TFT_info.L4_from_Source_port,
                            entry.TFT_info.L4_end_Source_port);
                    }
                    else {
                        CLI_print(" L4srcPort D.C\n");
                    }
                    if (entry.TFT_info.L4_dest_valid) {
                        CLI_print(" L4DestPort  %4d : %4d \n",
                            entry.TFT_info.L4_from_Dest_port,
                            entry.TFT_info.L4_end_Dest_port);
                    }
                    else {
                        CLI_print(" L4DestPort DC\n");
                    }
                }

            }//!MEA_HPM_NEW_SUPPORT
            CLI_print("-------------------------------------------------------------------------------------------- \n");
         }


            
            





        CLI_print("------------------------------------ \n");
        

    }





    return MEA_OK;

}


/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_TFT_Prof_Mask_Create(int argc, char *argv[])
{
    //int i;
    MEA_Uint16 MaskId;







    if (argc < 2) {
        return MEA_ERROR;
    }

    if (!MEA_OS_strcmp(argv[1], "auto")) {
        MaskId = MEA_PLAT_GENERATE_NEW_ID;
    }
    else {
        MaskId = MEA_OS_atoiNum(argv[1]);
    }





    if (MEA_API_TFT_Prof_Mask_Create(MEA_UNIT_0, &MaskId) != MEA_OK)
    {
        CLI_print("Error MEA_API_TFT_Prof_Mask_Create failed \n");
        return MEA_OK;
    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_TFT_Prof_Mask_Set(int argc, char *argv[])
{
    int i;
    MEA_Uint16 MaskId;
    MEA_TFT_mask_data_dbt entry;



    if (argc < 2) {
        return MEA_ERROR;
    }

    MaskId = MEA_OS_atoiNum(argv[1]);
    MEA_OS_memset(&entry, 0, sizeof(entry));

    for (i = 2; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "-mask")) {
            entry.mask = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-rule_type")) {
            entry.en_rule_type = MEA_TRUE;
            entry.rule_type = MEA_OS_atoiNum(argv[i + 1]);
        }
    
    }


    if (MEA_API_TFT_Prof_Mask_Set(MEA_UNIT_0, MaskId,&entry) != MEA_OK)
    {
        CLI_print("Error MEA_API_TFT_Prof_Mask_Create failed \n");
        return MEA_OK;
    }


    return MEA_OK;
}

static MEA_Status MEA_CLI_TFT_Prof_Mask_Get(int argc, char *argv[])
{

    MEA_Uint16 MaskId;
    MEA_Uint16 from_MaskId;
    MEA_Uint16 to_MaskId;

    MEA_TFT_mask_data_dbt entry;
    MEA_Uint32 count = 0;

    MEA_Bool exist;


    if (argc < 1) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_MaskId = 0;
        to_MaskId = MEA_TFT_MASK_MAX_PROFILE - 1;

    }
    else {
        from_MaskId = MEA_OS_atoiNum(argv[1]);
        to_MaskId = MEA_OS_atoiNum(argv[1]);
        MaskId = from_MaskId;
        if (MEA_API_TFT_Prof_Mask_IsExist(MEA_UNIT_0, MaskId, MEA_FALSE, &exist) != MEA_OK) {
        
        }
        if (!exist) {
            CLI_print("ERROR: TFT_Prof_Mask_IsExist %d\n", MaskId);
            return MEA_OK;
        }

    }

        CLI_print("-------------------------------------------\n");
        CLI_print("         TFT_MASK_Profile                  \n");
        CLI_print("-------------------------------------------\n");

    for (MaskId = from_MaskId; MaskId <= to_MaskId; MaskId++) 
    {
        if (MEA_API_TFT_Prof_Mask_IsExist(MEA_UNIT_0, MaskId, MEA_TRUE, &exist) != MEA_OK)
            continue;
        if (!exist)
            continue;

       


        if(MEA_API_TFT_Prof_Mask_Get(MEA_UNIT_0, MaskId, &entry) != MEA_OK){
            return MEA_ERROR;
        }
        if (count == 0)
        {
            CLI_print("-----------------------------\n");
            CLI_print(" Mask   Val      set   num   \n");
            CLI_print(" Id     mask     rule  ofRule\n");
            CLI_print(" ----- ---------- ----- ------\n");
        }

        count++;
        

            CLI_print(" %5d 0x%08x %4s %3d\n", MaskId,
                entry.mask,
                MEA_STATUS_STR(entry.en_rule_type),
                entry.rule_type);
        

    }

    CLI_print("------------------------------------ \n");



    return MEA_OK;

}

static MEA_Status MEA_CLI_TFT_Prof_Mask_Delete(int argc, char *argv[])
{
   
    MEA_Uint16 MaskId;
    MEA_Uint16 from_MaskId;
    MEA_Uint16 to_MaskId;
    MEA_Bool exist;
    if (argc < 2) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_MaskId = 0;
        to_MaskId = MEA_TFT_MASK_MAX_PROFILE - 1;

    }
    else {
        from_MaskId = MEA_OS_atoiNum(argv[1]);
        to_MaskId = MEA_OS_atoiNum(argv[1]);
        MaskId = from_MaskId;
        if (MEA_API_TFT_Prof_Mask_IsExist(MEA_UNIT_0, MaskId, MEA_FALSE, &exist) != MEA_OK) {
            CLI_print("Error  MEA_API_TFT_Prof_Mask_IsExist failed  MaskId=%d\n", MaskId);
            return MEA_OK;
        }
    }


    for (MaskId = from_MaskId; MaskId <= to_MaskId; MaskId++)
    {
        exist = MEA_FALSE;

        if (MEA_API_TFT_Prof_Mask_IsExist(MEA_UNIT_0, MaskId, MEA_TRUE, &exist) != MEA_OK)
            continue;

        if (!exist)
            continue;



        if (MEA_API_TFT_Prof_Mask_Delete(MEA_UNIT_0, MaskId) != MEA_OK)
        {
            CLI_print("Error  MEA_API_TFT_Prof_Mask_Delete failed to delete MaskId=%d\n", MaskId);
            //return MEA_OK;
            continue;
        }

        CLI_print("Done: %d \n", MaskId);
    }

    return MEA_OK;
}


/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_TFT_Create_UEPDN_ConfigAction(int argc, char *argv[])
{
    MEA_Uint32 i;

    MEA_Uint32 mandatory_argc;

    MEA_Uint16 uepdn_Id;
    MEA_Uint8 uepdn_Qos;
    MEA_Bool  ACT_HPM_en = MEA_FALSE;


    if (argc < 3) {
        return MEA_ERROR;
    }

    for (i = 1; i < (MEA_Uint32)argc; i++) {
        if (MEA_OS_strcmp(argv[i], "ACT_HPM") == 0) {
            mandatory_argc = 2;
            if (i + 1 + mandatory_argc > (MEA_Uint32)argc) {
                CLI_print(
                    "Missing parameters after %s\n", argv[i]);
                return MEA_OK;
            }
            uepdn_Id = (MEA_Uint16)MEA_OS_atoi(argv[i + 1]);
            uepdn_Qos = (MEA_Uint8)MEA_OS_atoi(argv[i + 2]);

            if (uepdn_Id >= MEA_UEPDN_MAX_ID) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  uepdn_Id is out  \n", __FUNCTION__);
                return MEA_ERROR;
            }
            if (uepdn_Qos > 7) {
                CLI_print(
                    "Error uepdn_Qos [%d] not on range 0:7 \n", argv[i]);
                return MEA_OK;
            }

            ACT_HPM_en = MEA_TRUE;
        }

    }


    if (ACT_HPM_en) {
        return MEA_CLI_ActionCreate(argc, argv);
    }
    else {
        CLI_print("not set ACT_HPM\n");
        return MEA_OK;
    }





    return MEA_OK;
}
static MEA_Status MEA_CLI_TFT_Set_UEPDN_ConfigAction(int argc, char *argv[]){
    MEA_Uint32 i;
   
   
    MEA_Uint32 mandatory_argc;

    MEA_Uint16 uepdn_Id;
    MEA_Uint8 uepdn_Qos;
    MEA_Bool  ACT_HPM_en = MEA_FALSE;


    if (argc < 3) {
        return MEA_ERROR;
    }

    for (i = 1; i < (MEA_Uint32)argc; i++) {
        if (MEA_OS_strcmp(argv[i], "ACT_HPM") == 0) {
            mandatory_argc = 2;
            if (i + 1 + mandatory_argc > (MEA_Uint32)argc) {
                CLI_print(
                    "Missing parameters after %s\n", argv[i]);
                return MEA_OK;
            }
            uepdn_Id = (MEA_Uint16)MEA_OS_atoi(argv[i + 1]);
            uepdn_Qos = (MEA_Uint8)MEA_OS_atoi(argv[i + 2]);
            if (uepdn_Id >= MEA_UEPDN_MAX_ID) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  uepdn_Id is out  \n", __FUNCTION__);
                return MEA_ERROR;
            }
            if (uepdn_Qos > 7) {
                CLI_print(
                    "Error uepdn_Qos [%d] not on range 0:7 \n", argv[i]);
                return MEA_OK;
            }
            ACT_HPM_en = MEA_TRUE;
        }

    }


    if (ACT_HPM_en) {
        return MEA_CLI_ActionModify(argc, argv);
    }
    else {
        CLI_print("not set ACT_HPM\n");
        return MEA_OK;
    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_TFT_Get_UEPDN_ConfigAction_Entry(int argc, char *argv[]){
    mea_TFT_Action_rule_t entry;
    MEA_Uint16  UEPDN_Id, start_UEPDN_Id, end_UEPDN_Id;
    MEA_Uint32 count = 0;
    MEA_Bool                        exist;
    MEA_Uint8 priority, fromPri, ToPri;

    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_out_ports_tbl_entry;
    MEA_Policer_Entry_dbt                   Action_sla_params_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_Editing;
    


   

    if (argc < 3) {
        return MEA_ERROR;
    }

    
    MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
    MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
    MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */

    if (!MEA_OS_strcmp(argv[1], "all")){
        start_UEPDN_Id = 0;
        end_UEPDN_Id = MEA_UEPDN_MAX_ID-1 ;
    }
    else {
        start_UEPDN_Id = (MEA_Uint16)MEA_OS_atoiNum(argv[1]);
        end_UEPDN_Id   = start_UEPDN_Id;
        UEPDN_Id       = start_UEPDN_Id;
        if (UEPDN_Id >= MEA_UEPDN_MAX_ID) {
            CLI_print("error for UEPDN id\n");
            return MEA_ERROR;
        }
    }

    if (MEA_OS_strcmp(argv[2], "all") == 0) {
        fromPri = 0;
        ToPri = MEA_TFT_MAX_TEID_Rule-1;
       
    }
    else{
        fromPri = MEA_OS_atoiNum(argv[2]);
        ToPri = fromPri;
        if (ToPri >= MEA_TFT_MAX_TEID_Rule) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - index %d is out of range \n",
                __FUNCTION__, ToPri);
            return MEA_ERROR;

        }

    }


    count = 0;
    for (UEPDN_Id = start_UEPDN_Id; UEPDN_Id <= end_UEPDN_Id; UEPDN_Id++){
        for (priority = fromPri; priority <= ToPri; priority++) {
            if (MEA_API_UEPDN_TFT_TEID_ConfigAction_IsExist(MEA_UNIT_0, UEPDN_Id, priority, &exist) != MEA_OK){
               break;
            } else {
                if (!exist)
                    continue;

                entry.action_params = NULL;
                entry.EHP_Entry = NULL;
                entry.OutPorts_Entry = NULL;
                entry.Policer_Entry = NULL;



                MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
                MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
                MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
                MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */

                entry.action_params = &Action_data;
                entry.OutPorts_Entry = &Action_out_ports_tbl_entry;
                entry.EHP_Entry      = &Action_Editing;


                if (MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority, &entry) != MEA_OK)
                {
                    CLI_print("Error MEA_API_Set_UEPDN_ConfigAction failed UEPDN_Id %d \n", UEPDN_Id);
                    continue;
                }
                /* set the number of editing */

                if (Action_Editing.num_of_entries > 0) {
                    Action_Editing.ehp_info = (MEA_EHP_Info_dbt*)MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt)*Action_Editing.num_of_entries);
                    if (Action_Editing.ehp_info == NULL) {
                        CLI_print("ERROR: Failed to malloc ehp data\n");
                        return MEA_OK;
                    }

                    if (MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority, &entry) != MEA_OK)
                    {
                        CLI_print("Error MEA_API_Get_UEPDN_TEID_ConfigAction failed UEPDN_Id %d \n", UEPDN_Id);
                        MEA_OS_free(Action_Editing.ehp_info);

                        return MEA_OK;
                    }

                }

                if (count++ == 0) {
                    CLI_print("------------------------------------------------------------------------------------------------------\n");
                    CLI_print(" UEPDN     Pm     Tm     Edit   pro llc Pri Pri CoS Cos Col Ing fra wred lag Drop Frag_Ip    Defrag_Ip \n"
                              "  Id   Qos Id     Id     Id             F/M V/P F/M V/P     TS      pro  byP En   F T ex mtu ex in Re \n"
                              "------ --- ------ ------ ------ --- --- --- --- --- --- --- --- --- ---- --- ---- - - -- --- -- -- -- \n");
                    //12345678901234567890123456789012345678901234567890123456789012345678901234567890
                }
                if ((count % 100) == 0) {
                    count = 0;
                }
                CLI_print("%6d ", UEPDN_Id);
                CLI_print("%3d ", priority);


                if (entry.action_params->pm_id_valid) {
                    CLI_print("%6d ", entry.action_params->pm_id);
                }
                else {
                    CLI_print("%6s ", "NA");
                }

                if (entry.action_params->tm_id_valid) {
                    CLI_print("%6d ", entry.action_params->tm_id);
                }
                else {
                    CLI_print("%6s ", "NA");
                }

                if (entry.action_params->ed_id_valid) {
                    if (entry.EHP_Entry->num_of_entries != 1) {
                        CLI_print("%6s ", "MC");
                    }
                    else {
                        CLI_print("%6d ", entry.action_params->ed_id);
                    }
                }
                else {
                    CLI_print("%6s ", "NA");
                }

                if (entry.action_params->proto_llc_valid) {
                    if (entry.action_params->protocol_llc_force) {
                        CLI_print("%3d %3d ",
                            entry.action_params->Protocol,
                            entry.action_params->Llc);
                    }
                    else {
                        CLI_print("%3s %3s ",
                            "Aut", "Aut"); \
                    }
                }
                else {
                    CLI_print("%3s %3s ",
                        "NA", "NA");
                }

                CLI_print("%-3s %3d ",
                    (entry.action_params->flowMarkingMappingProfile_force) ? "Map" :
                    (entry.action_params->force_l2_pri_valid == MEA_ACTION_FORCE_COMMAND_DISABLE) ? "NA " :
                    (entry.action_params->force_l2_pri_valid == MEA_ACTION_FORCE_COMMAND_VALUE) ? "Val" :
                    (entry.action_params->force_l2_pri_valid == MEA_ACTION_FORCE_COMMAND_MAX) ? "Max" :
                    "???",
                    (entry.action_params->flowMarkingMappingProfile_force)
                    ? entry.action_params->flowMarkingMappingProfile_id
                    : entry.action_params->L2_PRI);

                CLI_print("%-3s %3d ",
                    (entry.action_params->flowCoSMappingProfile_force) ? "Map" :
                    (entry.action_params->force_cos_valid == MEA_ACTION_FORCE_COMMAND_DISABLE) ? "NA " :
                    (entry.action_params->force_cos_valid == MEA_ACTION_FORCE_COMMAND_VALUE) ? "Val" :
                    (entry.action_params->force_cos_valid == MEA_ACTION_FORCE_COMMAND_MAX) ? "Max" :
                    "???",
                    (entry.action_params->flowCoSMappingProfile_force)
                    ? entry.action_params->flowCoSMappingProfile_id
                    : entry.action_params->COS);


                if (entry.action_params->force_color_valid) {
                    CLI_print("%3d ", entry.action_params->COLOR);
                }
                else {
                    CLI_print("%3s ", "NA");
                }
                CLI_print("%3s ", MEA_STATUS_STR(entry.action_params->fwd_ingress_TS_type));
                CLI_print("%3s ", MEA_STATUS_STR(entry.action_params->fragment_enable));
                CLI_print("%4d ", entry.action_params->wred_profId);
                CLI_print("%3s ", MEA_STATUS_STR(entry.action_params->lag_bypass));
                CLI_print("%4s ", MEA_STATUS_STR(entry.action_params->Drop_en));
                CLI_print("%1s ", MEA_STATUS_STR_Y(entry.action_params->Fragment_IP ));
                CLI_print("%1s ", MEA_STATUS_STR_Y(entry.action_params->IP_TUNNEL ) );
                CLI_print("%2s ", (entry.action_params->fragment_ext_int == MEA_TRUE) ? "Y" : "N");
                CLI_print("%2d ", entry.action_params->fragment_target_mtu_prof);
                CLI_print("%2s ", MEA_STATUS_STR_Y(entry.action_params->Defrag_ext_IP));
                CLI_print("%2s ", MEA_STATUS_STR_Y(entry.action_params->Defrag_int_IP) );
                CLI_print("%2d ", (entry.action_params->Defrag_Reassembly_L2_profile));

                CLI_print("\n");

                if (Action_Editing.ehp_info) {
                    MEA_OS_free(Action_Editing.ehp_info);
                    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing));
                }

            }//IsExist
            
        }//index priority
        
    }//UE
    


    return MEA_OK;
}

static MEA_Status MEA_CLI_TFT_Get_UEPDN_ConfigAction_editing(int argc, char *argv[]) {
    
    
    mea_TFT_Action_rule_t entry;
    MEA_Uint16  UEPDN_Id, start_UEPDN_Id, end_UEPDN_Id;
    MEA_Uint32 count = 0;
    MEA_Uint32 i;
    MEA_Bool                        exist;
    MEA_Uint8 priority, fromPri, ToPri;

    mea_cli_editing_info_te info = MEA_CLI_EDITING_INFO_EDITING;
    MEA_Action_Entry_Data_dbt               Action_data;
    MEA_OutPorts_Entry_dbt                  Action_out_ports_tbl_entry;
    MEA_Policer_Entry_dbt                   Action_sla_params_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_Editing;

    mea_cli_showEditing_info_t show_entry;
   // MEA_Bool set_relavant_input=MEA_FALSE;





    if (argc <=2) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
    MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
    MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */

    start_UEPDN_Id = 0;
    end_UEPDN_Id = 0;
    fromPri = 0;
    ToPri = 7;

    for (i = 0; i < (MEA_Uint32)argc; i++) {
        if (i == 1) {
            if (!MEA_OS_strcmp(argv[1], "all")) {
                start_UEPDN_Id = 0;
                end_UEPDN_Id = MEA_UEPDN_MAX_ID - 1;
            }
            else {
                start_UEPDN_Id = (MEA_Uint16)MEA_OS_atoi(argv[1]);
                end_UEPDN_Id = start_UEPDN_Id;
                UEPDN_Id = start_UEPDN_Id;

            }
        }
        if (i == 2) {
            if (MEA_OS_strcmp(argv[2], "all") == 0) {
                fromPri = 0;
                ToPri = MEA_TFT_MAX_TEID_Rule - 1;

            }
            else {
                fromPri = MEA_OS_atoiNum(argv[2]);
                ToPri = fromPri;
                if (ToPri >= MEA_TFT_MAX_TEID_Rule) {
                    MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                        "%s - index %d is out of range \n",
                        __FUNCTION__, ToPri);
                    return MEA_ERROR;

                }

            }
        }
    }



    for (i = 1; i < (MEA_Uint32)argc; i++)
    {
        if (!MEA_OS_strcmp(argv[i], "-hvxlan_DL")) {
            info = MEA_CLI_EDITING_INFO_VXLAN_DL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hIPCS_DL")) {
            info = MEA_CLI_EDITING_INFO_IPCS_DL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hIPCS_UL")) {
            info = MEA_CLI_EDITING_INFO_IPCS_UL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hETHCS_DL")) {
            info = MEA_CLI_EDITING_INFO_ETHCS_DL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hETHCS_UL")) {
            info = MEA_CLI_EDITING_INFO_ETHCS_UL;
        }
        if (!MEA_OS_strcmp(argv[i], "-hNAT")) {
            info = MEA_CLI_EDITING_INFO_NAT;
        }

        if (!MEA_OS_strcmp(argv[i], "-m")) {
            info = MEA_CLI_EDITING_INFO_MARTINI;
        }
        if (!MEA_OS_strcmp(argv[i], "-hpl")) {
            info = MEA_CLI_EDITING_INFO_PROTOLLC;
        }
        if (!MEA_OS_strcmp(argv[i], "-s")) {
            info = MEA_CLI_EDITING_INFO_STAMPING;
        }
        if (!MEA_OS_strcmp(argv[i], "-hs")) {
            info = MEA_CLI_EDITING_INFO_SESSION;
        }
        if (!MEA_OS_strcmp(argv[i], "-lmid")) {
            info = MEA_CLI_EDITING_INFO_LMID;
        }
        if (!MEA_OS_strcmp(argv[i], "-hdscp")) {
            info = MEA_CLI_EDITING_INFO_DSCP;
        }
        if (!MEA_OS_strcmp(argv[i], "-hmpls")) {
            info = MEA_CLI_EDITING_INFO_MPLS;
        }
        if (!MEA_OS_strcmp(argv[i], "-hpw")) {
            info = MEA_CLI_EDITING_INFO_PW;
        }

    }

    MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
    MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
    MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */


    count = 0;
    for (UEPDN_Id = start_UEPDN_Id; UEPDN_Id <= end_UEPDN_Id; UEPDN_Id++) {
        for (priority = fromPri; priority <= ToPri; priority++) {
            if (MEA_API_UEPDN_TFT_TEID_ConfigAction_IsExist(MEA_UNIT_0, UEPDN_Id, priority, &exist) == MEA_OK)
            {
                if (!exist)
                    continue;

                
                
                MEA_OS_memset(&show_entry, 0, sizeof(show_entry));


                MEA_OS_memset(&Action_data, 0, sizeof(MEA_Action_Entry_Data_dbt));
                MEA_OS_memset(&Action_sla_params_entry, 0, sizeof(Action_sla_params_entry));
                MEA_OS_memset(&Action_out_ports_tbl_entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
                MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing)); /* Critical */

                entry.action_params = &Action_data;
                
                entry.Policer_Entry = &Action_sla_params_entry;
                entry.OutPorts_Entry = &Action_out_ports_tbl_entry;
                entry.EHP_Entry = &Action_Editing;


                if (MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority, &entry) != MEA_OK)
                {
                    CLI_print("Error MEA_API_Set_UEPDN_ConfigAction failed UEPDN_Id %d \n", UEPDN_Id);
                    continue;
                }
                /* set the number of editing */

                if (Action_Editing.num_of_entries > 0) {
                    Action_Editing.ehp_info = (MEA_EHP_Info_dbt*)MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt)*Action_Editing.num_of_entries);
                    if (Action_Editing.ehp_info == NULL) {
                        CLI_print("ERROR: Failed to malloc ehp data\n");
                        return MEA_OK;
                    }

                    if (MEA_API_Get_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority, &entry) != MEA_OK)
                    {
                        CLI_print("Error MEA_API_Get_UEPDN_TEID_ConfigAction failed UEPDN_Id %d \n", UEPDN_Id);
                        MEA_OS_free(Action_Editing.ehp_info);

                        return MEA_OK;
                    }

                }


                show_entry.type = MEA_ACTION_TYPE_HPM;
                show_entry.Action_Id = (MEA_Action_t)UEPDN_Id;
                show_entry.index = (MEA_Uint32)priority;
                show_entry.info = info;
                
                show_entry.Action_Editing = entry.EHP_Entry;

                if (count == 100)
                    count = 0;
                show_entry.count = count;

                mea_cli_Acton_Print_Editing_info(&show_entry);

                count++;
                if (Action_Editing.ehp_info) {
                    MEA_OS_free(Action_Editing.ehp_info);
                    MEA_OS_memset(&Action_Editing, 0, sizeof(Action_Editing));
                }

            }//IsExist

        }//index priority

    }//UE



    return MEA_OK;
}


static MEA_Status MEA_CLI_TFT_Delete_UEPDN_ConfigAction_Entry(int argc, char *argv[])
{
    
    MEA_Uint16  UEPDN_Id, start_UEPDN_Id, end_UEPDN_Id;
    MEA_Uint32 count = 0;
    MEA_Bool                        exist;
    MEA_Uint8 priority, fromPri, ToPri;

    





    if (argc < 3) {
        return MEA_ERROR;
    }





    if (!MEA_OS_strcmp(argv[1], "all")) {
        start_UEPDN_Id = 0;
        end_UEPDN_Id = MEA_UEPDN_MAX_ID - 1;
    }
    else {
        start_UEPDN_Id = (MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_UEPDN_Id = start_UEPDN_Id;
        UEPDN_Id = start_UEPDN_Id;

        if (UEPDN_Id >= MEA_UEPDN_MAX_ID) {
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, "%s -  uepdn_Id is out  \n", __FUNCTION__);
            return MEA_ERROR;
        }
       

    }

    if (MEA_OS_strcmp(argv[2], "all") == 0) {
        fromPri = 0;
        ToPri = MEA_TFT_MAX_TEID_Rule - 1;

    }
    else {
        fromPri = MEA_OS_atoiNum(argv[2]);
        ToPri = fromPri;
        
        if (fromPri > 7) {
            CLI_print(
                "Error uepdn_Qos [%d] not on range 0:7 \n", argv[2]);
            return MEA_OK;
        }

    }


    count = 0;
    for (UEPDN_Id = start_UEPDN_Id; UEPDN_Id <= end_UEPDN_Id; UEPDN_Id++) {
        for (priority = fromPri; priority <= ToPri; priority++) {
            if (MEA_API_UEPDN_TFT_TEID_ConfigAction_IsExist(MEA_UNIT_0, UEPDN_Id, priority, &exist) == MEA_OK)
            {
                if (!exist)
                    continue;
                
                if(MEA_API_Delete_UEPDN_TEID_ConfigAction(MEA_UNIT_0, UEPDN_Id, priority) != MEA_OK){
                    CLI_print("ERROR MEA_API_Delete_UEPDN_TEID_ConfigAction (ue=%d :%d )\n", UEPDN_Id, priority);
                    return MEA_OK;
                }
				count++;
            }//IsExist

        }//index priority

    }//UE
    
	if (count) {
		CLI_print("Delete_UEPDN %d \n", count);
	}

    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

static MEA_Status MEA_CLI_TFT_L3_Internal_EtherType_Get(int argc, char *argv[])
{
    MEA_Uint8 MaskId;
    MEA_Uint8 from_MaskId;
    MEA_Uint8 to_MaskId;

    MEA_TFT_L3_EtheType_dbt entry;
    MEA_Uint32 count = 0;

   


    if (argc < 1) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_MaskId = 1;
        to_MaskId = MAT_MAX_OF_L3_Internal_EtherType - 1;

    }
    else {
        from_MaskId = MEA_OS_atoiNum(argv[1]);
        to_MaskId = MEA_OS_atoiNum(argv[1]);
        MaskId = from_MaskId;

    }

    CLI_print("-------------------------------------------\n");
    CLI_print("         TFT_MASK_Profile                  \n");
    CLI_print("-------------------------------------------\n");

    for (MaskId = from_MaskId; MaskId <= to_MaskId; MaskId++)
    {

        if (MEA_API_TFT_L3_Internal_EtherType_Get(MEA_UNIT_0, MaskId, &entry) != MEA_OK) {
            return MEA_ERROR;
        }
        if (count == 0)
        {
            CLI_print("---------------------\n");
            CLI_print(" Id    Valid  ETH    \n");
            CLI_print(" ---- ----- ------  \n");
        }

        count++;


        CLI_print(" %4d %3s 0x%04x \n", MaskId,
            MEA_STATUS_STR(entry.valid),
            entry.EtherType);


    }

    CLI_print("------------------------------------ \n");


    return MEA_OK;
}
static MEA_Status MEA_CLI_TFT_L3_Internal_EtherType_Set(int argc, char *argv[])
{

    int i;
    MEA_Uint8 MaskId;
    MEA_TFT_L3_EtheType_dbt entry;
    MEA_Uint32 set_en = MEA_FALSE;


    if (argc < 2) {
        return MEA_ERROR;
    }

    MaskId = MEA_OS_atoiNum(argv[1]);
    MEA_OS_memset(&entry, 0, sizeof(entry));

    for (i = 2; i < argc; i++) {
       
        if (!MEA_OS_strcmp(argv[i], "-ethertype")) {
            entry.valid     = MEA_OS_atoiNum(argv[i + 1]);
            entry.EtherType = MEA_OS_atoiNum(argv[i + 2]);
            set_en = MEA_TRUE;
        }

    }

    if (set_en) {
        if (MEA_API_TFT_L3_Internal_EtherType_Set(MEA_UNIT_0, MaskId, &entry) != MEA_OK)
        {
            CLI_print("Error MEA_API_TFT_Prof_Mask_Create failed \n");
            return MEA_OK;
        }
    }



    if (set_en)
        CLI_print("Done:\n");

    return MEA_OK;
    
}




MEA_Status MEA_CLI_tft_AddDebugCmds(void)
{

    /**********************************************************/
    /*                                                        */
    /**********************************************************/
	
	        
            CLI_defineCmd("MEA TFT UEPDN set create",
                (CmdFunc)MEA_CLI_TFT_Create_UEPDN_ConfigAction,
                "create TPT UEPDN <ue><index>   \n",
                meacli_LongHelp_Create_Action);


            CLI_defineCmd("MEA TFT UEPDN set modify",
                (CmdFunc)MEA_CLI_TFT_Set_UEPDN_ConfigAction,
                "modify TFT UEPDN <ue><index>   \n",
                meacli_LongHelp_Create_Action
                );

            CLI_defineCmd("MEA TFT UEPDN set delete",
                (CmdFunc)MEA_CLI_TFT_Delete_UEPDN_ConfigAction_Entry,
                " delete UEPDN <ue><index>   \n",
                " delete UEPDN <ue><index> \n"
                "---------------------------------------------------\n"
                "Examples: \n"
                "        UEPDN delete All All       \n"
                "        UEPDN delete 10 7         \n"
                );


            
                CLI_defineCmd("MEA TFT UEPDN show entry",
                    (CmdFunc)MEA_CLI_TFT_Get_UEPDN_ConfigAction_Entry,
                    " UEPDN show entry  \n",
                    " UEPDN show entry  \n"

                    "    <UEPDN id>  value   \n"
                    "    <index> 0:7    \n"
                    
                    "---------------------------------------------------\n"
                    "Examples: \n"
                    "       UEPDN show entry All All       \n"
                    "       UEPDN show entry 10 7      \n"
                    );

                CLI_defineCmd("MEA TFT UEPDN show editing",
                    (CmdFunc)MEA_CLI_TFT_Get_UEPDN_ConfigAction_editing,
                    "TFT_UEPDN show editing  \n",
                    "TFT_UEPDN show  \n"

                    "    <UEPDN id>  value   \n"
                    "    <index> 0:7    \n"

                    "---------------------------------------------------\n"
                    "Examples: \n"
                    "       TFT_UEPDN show editing All All       \n"
                    "       TFT_UEPDN show editing 10 7      \n"
                    );


                

                    CLI_defineCmd("MEA TFT l3_eth set",
                        (CmdFunc)MEA_CLI_TFT_L3_Internal_EtherType_Set,
                        "create   HPM\n",
                        "create   HPM  \n"
                        "    <index>  value (4:7)\n"
                        "    -ethertype <valid><ethetype> \n"
                        "---------------------------------------------------\n"
                        "Examples: \n"
                        "        l3_eth set  4  -ethertype 0 0x0    \n"
                        "        l3_eth set  5  -ethertype 1 0x0806 \n"
                        );

                CLI_defineCmd("MEA TFT l3_eth show",
                    (CmdFunc)MEA_CLI_TFT_L3_Internal_EtherType_Get,
                    "show   l3_eth\n",
                    "show   l3_eth  \n"
                    "    <index/all>  \n"
                    
                    "---------------------------------------------------\n"
                    "Examples: \n"
                    "        l3_eth show  1     \n"
                    "        l3_eth show   all   \n"
                    );
                
       /*---------------------------------------------------------------*/
        CLI_defineCmd("MEA TFT prof create",
            (CmdFunc)MEA_CLI_TFT_Create_QoS_Profile,
            "create   HPM\n",
            "create   HPM  \n"
            "    <tft_Id/auto>  value (0:63)\n"
            "    <-rule_type> 0:7 \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "        HPM prof create 1  \n"
            "        HPM prof create 2  -rule_type 7\n"
            );

        CLI_defineCmd("MEA TFT prof delete",
            (CmdFunc)MEA_CLI_TFT_Delete_QoS_Profile,
            "delete   HPM\n",
            "delete   HPM  \n"
            "    <Id>  value (0:63)\n"
           
            "---------------------------------------------------\n"
            "Examples: \n"
            "       prof delete 1  \n"
            "       prof delete 2  \n"
            );

        CLI_defineCmd("MEA TFT prof set",
            (CmdFunc)MEA_CLI_TFT_Set_ClassificationRule,
            " HPM prof set   n",
            " HPM prof add   \n"
            "    <Id>  value (0:63)\n"
            "    <index>  (0:31)\n"
            
            "-------------------------------------------\n"
            "     type  <0 - disable 1 - noIp 2 - Ipv4 3 - DestIpv6 4 - srcIpv6  >\n"
            "     Priority_Rule   0:31  \n"
            "     QoS_Index       0:7 \n"
            "------------------------------\n"
            "     L2_pri <valid><l2Pri (0xff)>\n"
            "     L3_ethtype <int-ethtype>\n"
            "     DSCP  <valid><from DSCP><end_DSCP>\n"
            "     IP_protocol <valid><IP_protocol>\n"
            "     src_ipv4 <valid><SrcIp><mask>\n"
            "     dest_ipv4 <valid><DesIp><mask>\n"
            "     ipv6 <valid><DesIp><mask>\n"
            "     L4_srcport <valid><from_Src_prot><end_Src_port>\n"
            "     L4_destport <valid> <from_Dest_port><end_Dest_port>\n"
            "    flow_label   (ipv6 flow label)\n"
           
            "---------------------------------------------------\n"
            
            "      L2_pri   (802.p) \n"
            "           <valid>  0-disable 1-enable \n"
            "           <L2Pri>  <value>  0 : 0xff\n"
            "     L3_ethtype  inner ethertype \n"
            "                <value> 0:7\n"
            "---------------------------------------------------\n"
            "     DSCP  (L3 DSCP min max) \n"
            "          <valid> 0-disable 1-enable\n"
            "          <fromdscp> 0:63\n"
            "          <end_dscp>  0:63\n"
            "---------------------------------------------------\n"
            "     IP_protocol (IP_protocol parameters) \n"
            "             <valid><IP_protocol> \n"
            "---------------------------------------------------\n"
            "     src_ipv4 (source IPv4 parameters) \n"
            "            <valid> 0-disable 1-enable \n"
            "            <SrcIp>  Ip Address \n"
            "            <mask>  0:32 \n"
            "---------------------------------------------------\n"
            "     dest_ipv4 (destination IPv4 parameters)  \n"
            "              <valid> 0-disable 1-enable \n"
            "              <DesIp> Ip Address         \n"
            "              <mask> 0:32                \n"
            "------------------------------------------------------\n"
            
            "         Ipv6  (Dest/srcIP IPv6 parameters )          \n"
            "              <valid> 0-disable 1-enable              \n"
            "              <IpV6> Ipv6 Address x:x:x:x:x:x:x:x    \n"
            "              <mask> 0:128                            \n"
            "       flow_label  (Ipv6 flow label)                   \n"
            "              <flow_label>                             \n"
            "-------------------------------------------------------\n"
            "        L4_srcport                      \n"  
            "        <valid>  0-disable 1-enable     \n"
            "        <from_Src_prot><end_Src_port>  (0- 65,536) \n"
            "---------------------------------------------------\n"
            "       L4_destport                     \n"
            "        <from_Dest_port><end_Dest_port> (0-65,536)\n"
            
            "---------------------------------------------------\n"
            "Examples: \n"
            "        prof set 0 1    L2_pri 1 0x7  DSCP 1 24 50  Priority_Rule 31 QoS_Index 4 type 2\n"
            "        prof set 0 2    DSCP 1 0 63 src_ipv4 1 100.0.0.138 32  Priority_Rule 1 QoS_Index 7 type 2 \n"
            "        prof set 0 2   dest_ipv4 1 100.0.0.138 24  type 2\n"
            "        prof set 0 2   type 0  \n"
            "---------------------------------------------------\n"
            );

        CLI_defineCmd("MEA TFT prof show",
            (CmdFunc)MEA_CLI_TFT_Get_ClassificationRule,
            "show   prof\n",
            "show   prof \n"
            "    <tft_Id>  all/Id\n"
            "    <priority> All/priority \n"
            
            "---------------------------------------------------\n"
            "Examples: \n"
            "       HPM prof show 1 all all   \n"
            "       HPM prof show 2 all all  \n"
            );



        /*------------------------------------------------------------------------*/
        CLI_defineCmd("MEA TFT mask_prof create",
            (CmdFunc)MEA_CLI_TFT_Prof_Mask_Create,
            " Create mask_prof\n",
            " Create mask_prof \n"
            "    <maskId/auto>  Id\n"
            "       "
            "---------------------------------------------------\n"
            "Examples: \n"
            "       mask_prof create auto  \n"
            "       mask_prof create 10    \n"
            );
        CLI_defineCmd("MEA TFT mask_prof delete",
            (CmdFunc)MEA_CLI_TFT_Prof_Mask_Delete,
            "delete   HPM mask_prof\n",
            "delete   HPM mask_prof \n"
            "    <Id>  Mask prof id \n"

            "---------------------------------------------------\n"
            "Examples: \n"
            "       HPM mask_prof delete all  \n"
            "       HPM mask_prof delete 2  \n"
            );
        
        CLI_defineCmd("MEA TFT mask_prof set",
            (CmdFunc)MEA_CLI_TFT_Prof_Mask_Set,
            "set   mask_prof\n",
            "set   mask_prof \n"
            "    <maskId>  Id\n"
            "-----------------"
            " option:\n"
            " -rule_type <val> 1:7\n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       mask_prof set 1  -mask 0x0000ff  \n"
            "       mask_prof set 10 -mask 0xffffffff  -rule_type 7 \n"
            );

        CLI_defineCmd("MEA TFT mask_prof show",
            (CmdFunc)MEA_CLI_TFT_Prof_Mask_Get,
            "show   mask_prof\n",
            "show   mask_prof \n"
            "    <tft_Id>  all/Id\n"

            "---------------------------------------------------\n"
            "Examples: \n"
            "       mask_prof show 1    \n"
            "       mask_prof show all   \n"
            );
        /*------------------------------------------------------------------------*/
	
	
	
   

    

    return MEA_OK;
}














