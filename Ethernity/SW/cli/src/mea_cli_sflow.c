/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/

#include "mea_api.h"
#include "enet_queue_drv.h"
#include "mea_sflow_drv.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_sflow.h"


static MEA_Status MEA_CLI_Sflow_Prof_Create(int argc, char *argv[])
{

    int i;
    MEA_Uint16 Id = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Sflow_Prof_dbt entry;



    if (argc < 2) {
        return MEA_ERROR;
    }

    
    MEA_OS_memset(&entry, 0, sizeof(entry));

    for (i = 0; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "-f")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }

            Id = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-pkt")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.packetCount = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-timeOut")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.timeOut_type = MEA_OS_atoiNum(argv[i + 1]);;
        }
        if (!MEA_OS_strcmp(argv[i], "-priQ")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.pri_queue = MEA_OS_atoiNum(argv[i + 1]);
        }

    }

   

        if (MEA_API_Sflow_Prof_Create(MEA_UNIT_0, &entry, &Id) != MEA_OK)
    {
        CLI_print("Error MEA_API_TFT_Prof_Mask_Create failed \n");
        return MEA_OK;
    }


    return MEA_OK;
}


static MEA_Status MEA_CLI_Sflow_Prof_Set(int argc, char *argv[])
{

    int i;
    MEA_Uint16 Id = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Sflow_Prof_dbt entry;



    if (argc < 2) {
        return MEA_ERROR;
    }

    Id = MEA_OS_atoiNum(argv[1]);
    MEA_OS_memset(&entry, 0, sizeof(entry));

    for (i = 1; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "-f")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            Id = MEA_OS_atoiNum(argv[i + 1]);
        }
       
        if (!MEA_OS_strcmp(argv[i], "-pkt")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.packetCount = MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!MEA_OS_strcmp(argv[i], "-timeOut")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.timeOut_type = MEA_OS_atoiNum(argv[i + 1]);;
        }
        if (!MEA_OS_strcmp(argv[i], "-priQ")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            entry.pri_queue = MEA_OS_atoiNum(argv[i + 1]);
        }

    }



    if (MEA_API_Sflow_prof_Set(MEA_UNIT_0, Id ,&entry) != MEA_OK)
    {
        CLI_print("Error MEA_API_TFT_Prof_Mask_Create failed \n");
        return MEA_OK;
    }


    return MEA_OK;
}

static MEA_Status MEA_CLI_Sflow_Prof_Get(int argc, char *argv[])
{

    MEA_Uint16 Id;
    MEA_Uint16 from_Id;
    MEA_Uint16 to_Id;

    MEA_Sflow_Prof_dbt entry;
    MEA_Uint32 count = 0;

    MEA_Bool exist;


    if (argc < 2) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_Id = 1;
        to_Id = MEA_SFLOW_MAX_PROF - 1;

    }
    else {
        from_Id = MEA_OS_atoiNum(argv[1]);
        to_Id = MEA_OS_atoiNum(argv[1]);
        Id = from_Id;
        if (MEA_API_sflow_prof_IsExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {

        }
        if (!exist) {
            CLI_print("ERROR: MEA_API_sflow_prof_IsExist %d\n", Id);
            return MEA_OK;
        }

    }

    CLI_print("-------------------------------------------\n");
    CLI_print("         sflow Profile                  \n");
    CLI_print("-------------------------------------------\n");

    for (Id = from_Id; Id <= to_Id; Id++)
    {
        if (MEA_API_sflow_prof_IsExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
            continue;
        if (!exist)
            continue;




        if (MEA_API_Sflow_prof_Get(MEA_UNIT_0, Id, &entry) != MEA_OK) {
            return MEA_ERROR;
        }
        if (count == 0)
        {
            CLI_print("-----------------------------\n");
           
            CLI_print(" Id    pkt        timeOut  priQ \n");
            CLI_print(" ----- ---------- ------- -----\n");
        }

        count++;


        CLI_print(" %5d %10d %7d %5d\n",Id,
            entry.packetCount,
            entry.timeOut_type,
            entry.pri_queue);


    }

    CLI_print("------------------------------------ \n");

    

    return MEA_OK;
}


static MEA_Status MEA_CLI_Sflow_Prof_Delete(int argc, char *argv[])
{

    MEA_Uint16 Id;
    MEA_Uint16 from_Id;
    MEA_Uint16 to_Id;
    MEA_Bool exist;
    if (argc < 2) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_Id = 1;
        to_Id = MEA_SFLOW_MAX_PROF - 1;

    }
    else {
        from_Id = MEA_OS_atoiNum(argv[1]);
        to_Id = MEA_OS_atoiNum(argv[1]);
        Id = from_Id;
        if (MEA_API_sflow_prof_IsExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
            CLI_print("Error  MEA_API_sflow_prof_IsExist failed  MaskId=%d\n", Id);
            return MEA_OK;
        }
    }


    for (Id = from_Id; Id <= to_Id; Id++)
    {
        exist = MEA_FALSE;

        if (MEA_API_sflow_prof_IsExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
            continue;

        if (!exist)
            continue;



        if (MEA_API_Sflow_prof_Delete(MEA_UNIT_0, Id) != MEA_OK)
        {
            CLI_print("Error  MEA_API_Sflow_prof_Delete failed to delete %d\n", Id);
            //return MEA_OK;
            continue;
        }

        CLI_print("Done: %d \n", Id);
    }


    return MEA_OK;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
static MEA_Status MEA_CLI_Sflow_count_set(int argc, char *argv[])
{
    MEA_Sflow_count_dbt entry;
    MEA_Uint32 Id,from_Id,to_Id;
    int i;

    if (argc < 2) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_Id = 0;
        to_Id = MEA_SFLOW_MAX_COUNTERS - 1;

    }
    else {
        from_Id = MEA_OS_atoiNum(argv[1]);
        to_Id = MEA_OS_atoiNum(argv[1]);
        Id = from_Id;
        
    }
     
    
    
    MEA_OS_memset(&entry, 0, sizeof(entry));

    for (i = 2; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "-prof")) {
            if (i + 2 > argc) {
                return MEA_ERROR;
            }
            entry.valid = MEA_OS_atoiNum(argv[i + 1]);
            entry.profId = MEA_OS_atoiNum(argv[i + 2]);
        }

    }

    for (Id = from_Id; Id <= to_Id; Id++)
    {
        if (MEA_API_Sflow_Statistic_Set(MEA_UNIT_0, Id, &entry) != MEA_OK) {
            CLI_print("ERROR: MEA_API_Sflow_Statistic_Set %d failed\n", Id);
            return MEA_OK;
        }
    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_Sflow_count_Get(int argc, char *argv[])
{
    MEA_Uint16 Id;
    MEA_Uint16 from_Id;
    MEA_Uint16 to_Id;

    MEA_Sflow_count_dbt entry;
    MEA_Uint32 count = 0;

    MEA_Bool exist;

    if (argc < 1) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_Id = 0;
        to_Id = MEA_SFLOW_MAX_COUNTERS - 1;

    }
    else {
        from_Id = MEA_OS_atoiNum(argv[1]);
        to_Id = MEA_OS_atoiNum(argv[1]);
        Id = from_Id;
        if (MEA_API_Sflow_Statistic_IsExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {

        }
        if (!exist) {
            CLI_print("ERROR: MEA_API_Sflow_Statistic_IsExist %d\n", Id);
            return MEA_OK;
        }

    }

    CLI_print("-------------------------------------------\n");
    CLI_print("         Sflow_Statistic                  \n");
    CLI_print("-------------------------------------------\n");

    for (Id = from_Id; Id <= to_Id; Id++)
    {
        if (MEA_API_Sflow_Statistic_IsExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
            continue;
        if (!exist)
            continue;




        if (MEA_API_Sflow_Statistic_Get(MEA_UNIT_0, Id, &entry) != MEA_OK) {
            return MEA_ERROR;
        }
        if (count == 0)
        {
            CLI_print("-----------------------------\n");
            CLI_print("Id   Prof \n");
            CLI_print("---- ----\n");
        }

        count++;


        CLI_print("%4d %4d\n", Id, entry.profId);


    }

    CLI_print("------------------------------------ \n");



    return MEA_OK;
}

static MEA_Status MEA_CLI_Sflow_count_delete(int argc, char *argv[])
{
    
    MEA_Uint32 Id, from_Id, to_Id;
    

    if (argc < 2) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_Id = 0;
        to_Id = MEA_SFLOW_MAX_COUNTERS - 1;

    }
    else {
        from_Id = MEA_OS_atoiNum(argv[1]);
        to_Id = MEA_OS_atoiNum(argv[1]);
        Id = from_Id;

    }



    

    
    
   
    for (Id = from_Id; Id <= to_Id; Id++)
    {
        if (MEA_API_Sflow_Statistic_Delete(MEA_UNIT_0, Id) != MEA_OK) {
            CLI_print("ERROR: MEA_API_Sflow_Statistic_Set %d failed\n", Id);
            return MEA_OK;
        }
    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_Sflow_SetTick(int argc, char *argv[])
{
    MEA_Uint32 time_Th;
    MEA_Sflow_Global_dbt entry;

    if (argc < 2) {
        return MEA_ERROR;
    }

    time_Th = MEA_OS_atoiNum(argv[1]); /* msec */

    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (MEA_API_Sflow_Get_Global(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("ERROR: MEA_API_Sflow_Set_Tick failed\n");
        return MEA_OK;
    }

    entry.time_Th = time_Th;


    if(MEA_API_Sflow_Set_Global(MEA_UNIT_0, &entry ) != MEA_OK){
        CLI_print("ERROR: MEA_API_Sflow_Set_Tick failed\n");
        return MEA_OK;
    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_Sflow_GetTick(int argc, char *argv[])
{
    MEA_Sflow_Global_dbt entry;
    MEA_Uint32 hw_time_Th;

    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (MEA_API_Sflow_Get_Global(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("ERROR: MEA_API_Sflow_Set_Tick failed\n");
        return MEA_OK;
    }
        if (mea_drv_Sflow_Get_HWTick(MEA_UNIT_0, &hw_time_Th /* msec */) != MEA_OK) {
            CLI_print("ERROR: mea_drv_Sflow_Get_HWTick  failed\n");
            return MEA_OK;
       }
     CLI_print(" --------- SFLOW TiCK ----------- \n");
     CLI_print("UserTime  %d msec \n", entry.time_Th);
     CLI_print("Tick     0x%08x \n", hw_time_Th);

     CLI_print("------------------------------------\n");



    return MEA_OK;
}


static MEA_Status MEA_CLI_Sflow_SetDsaEthType(int argc, char *argv[])
{
    MEA_Uint32 EthType;
    MEA_Sflow_Global_dbt entry;

    if (argc < 2) {
        return MEA_ERROR;
    }

    EthType = MEA_OS_atoiNum(argv[1]); 

    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (MEA_API_Sflow_Get_Global(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("ERROR: MEA_API_Sflow_Set_Tick failed\n");
        return MEA_OK;
    }

    entry.dsa_tag_EthType = EthType;


    if (MEA_API_Sflow_Set_Global(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("ERROR: MEA_API_Sflow_Set_Tick failed\n");
        return MEA_OK;
    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_Sflow_GetDsaEthType(int argc, char *argv[])
{
    MEA_Sflow_Global_dbt entry;
   

    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (MEA_API_Sflow_Get_Global(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("ERROR: MEA_API_Sflow_Set_Tick failed\n");
        return MEA_OK;
    }
    
    CLI_print(" --------- SFLOW DSA_TAG ----------- \n");
    CLI_print("EthType  0x%x  \n", entry.dsa_tag_EthType);
    CLI_print("------------------------------------\n");



    return MEA_OK;
}

static MEA_Status MEA_CLI_Sflow_SetMtuCpu(int argc, char *argv[])
{
    MEA_Uint32 mtu;
    MEA_Sflow_Global_dbt entry;

    if (argc < 2) {
        return MEA_ERROR;
    }

    mtu = MEA_OS_atoiNum(argv[1]);

    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (MEA_API_Sflow_Get_Global(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("ERROR: MEA_API_Sflow_Set_Tick failed\n");
        return MEA_OK;
    }

    entry.mtuCpu = mtu;


    if (MEA_API_Sflow_Set_Global(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("ERROR: MEA_API_Sflow_Set_Tick failed\n");
        return MEA_OK;
    }

    return MEA_OK;
}
static MEA_Status MEA_CLI_Sflow_GetMtuCpu(int argc, char *argv[])
{
    MEA_Sflow_Global_dbt entry;
   

    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (MEA_API_Sflow_Get_Global(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("ERROR: MEA_API_Sflow_Get_Global failed\n");
        return MEA_OK;
    }

    CLI_print(" --------- SFLOW DSA_TAG ----------- \n");
    CLI_print("EthType  0x%x  \n", entry.dsa_tag_EthType);
    CLI_print("------------------------------------\n");



    return MEA_OK;
}


static MEA_Status MEA_CLI_Sflow_Set_TargetCluster(int argc, char *argv[])
{
    
    
    MEA_sflow_queue_t entry;

    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (argc < 4) {
        return MEA_ERROR;
    }

    
   

    
    entry.groupIndex = MEA_OS_atoiNum(argv[1]);
    entry.valid =  MEA_OS_atoiNum(argv[2]);
    entry.clusterId = MEA_OS_atoiNum(argv[3]);


    if (MEA_API_Sflow_Set_TargetCluster(MEA_UNIT_0, &entry) != MEA_OK) {
        CLI_print("ERROR: TargetCluster failed\n");
        return MEA_OK;
    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_Sflow_Get_TargetCluster(int argc, char *argv[])
{
    MEA_Uint32 coun=0;
    MEA_Uint8 Id,from_Id, to_Id;
   
    MEA_sflow_queue_t entry;

    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (argc < 2) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {

        from_Id = 0;
        to_Id = MEA_MAX_SFLOW_QUEUE_GROUP - 1;

    }
    else {

        return MEA_ERROR;
    }


    for (Id = from_Id; Id <= to_Id; Id++)
    {
        entry.groupIndex = Id;
        if (MEA_API_Sflow_Get_TargetCluster(MEA_UNIT_0, &entry) != MEA_OK) {
            CLI_print("ERROR: TargetCluster failed\n");
            return MEA_OK;
        }
       
            if (coun++ == 0) {
                CLI_print("--------- SFLOW-Queue --------------------\n");
                CLI_print("-------------------------------------------\n");
               
            }
            CLI_print(" valid=%5s group=%5d cluster=%6d \n", MEA_STATUS_STR(entry.valid), entry.groupIndex, entry.clusterId);
        
        MEA_OS_memset(&entry, 0, sizeof(entry));

    }
    return MEA_OK;
}



static MEA_Status MEA_CLI_Sflow_GetEvents(int argc, char *argv[])
{
    MEA_Uint32 coun = 0;
    

    MEA_sflow_Events_dbt entry;

    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (argc < 2) {
        return MEA_ERROR;
    }


    if (MEA_API_Sflow_Events(MEA_UNIT_0, &entry) != MEA_OK) {

   }


    

        if (coun++ == 0) {
            CLI_print("--------- SFLOW-EVENTS --------------------\n");
            CLI_print("-------------------------------------------\n");

        }
        mea_drv_show_event_value(1, "    cluster", enet_cluster_internal2external(entry.dst_cluster));

        mea_drv_show_event_value(1, "    internal_cluster", entry.dst_cluster);
        mea_drv_show_event_value(0, "    p_to_mp", entry.p_to_mp);
        mea_drv_show_event_value(1, "    sflow_type", entry.sflow_type);
        mea_drv_show_event_value(1, "    pmId", entry.pm_id);
        mea_drv_show_event_value(1, "    src_port", entry.src_port);
        mea_drv_show_event_value(1, "    sflow_en", entry.sflow_en);




    
    return MEA_OK;
}



MEA_Status MEA_CLI_sflow_AddDebugCmds(void)
{

    CLI_defineCmd("MEA profile sflow event show",
        (CmdFunc)MEA_CLI_Sflow_GetEvents,
        "show   sflow event\n",
        "show   sflow event \n"

        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow event show all \n"

    );

    CLI_defineCmd("MEA profile sflow cluster show",
        (CmdFunc)MEA_CLI_Sflow_Get_TargetCluster,
        "show   sflow queue\n",
        "show   sflow queue \n"

        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow queue show all \n"

    );

    CLI_defineCmd("MEA profile sflow cluster add",
        (CmdFunc)MEA_CLI_Sflow_Set_TargetCluster,
        "set sflow dsaEth\n",
        "set sflow dsaEth\n"
        "    <group> <valid> <cluster>  \n"
        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow queue set 0 1 127   \n"
        "       sflow queue set 1 1 80    \n"

    );
    CLI_defineCmd("MEA profile sflow dsaEth show",
        (CmdFunc)MEA_CLI_Sflow_GetDsaEthType,
        "show   dsaEth\n",
        "show   dsaEth \n"

        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow dsaEth show all \n"

    );

    CLI_defineCmd("MEA profile sflow dsaEth set",
        (CmdFunc)MEA_CLI_Sflow_SetDsaEthType,
        "set sflow dsaEth\n",
        "set sflow dsaEth\n"
        "    <dsaEth>   EtherType \n"
        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow dsaEth set 0x1212  \n"

    );

    CLI_defineCmd("MEA profile sflow mtu show",
        (CmdFunc)MEA_CLI_Sflow_GetMtuCpu,
        "show  sflow mtu\n",
        "show  sflow mtu \n"

        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow mtu show all \n"

    );
    
        CLI_defineCmd("MEA profile sflow mtu set",
        (CmdFunc)MEA_CLI_Sflow_SetMtuCpu,
            "set sflow mtu\n",
            "set sflow mtu\n"
            "    <vlaue>   mtu \n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       sflow mtu set 1500  \n"

        );


        CLI_defineCmd("MEA profile sflow tick show",
        (CmdFunc)MEA_CLI_Sflow_GetTick,
            "show  sflow tick\n",
            "show  sflow tick \n"
         
            "---------------------------------------------------\n"
            "Examples: \n"
            "       sflow tick show all \n"
           
        );

        CLI_defineCmd("MEA profile sflow tick set",
            (CmdFunc)MEA_CLI_Sflow_SetTick,
            "set sflow tick\n",
            "set sflow tick\n"
            "    <value>  10: 2000(msec)\n"
            "---------------------------------------------------\n"
            "Examples: \n"
            "       sflow tick set  \n"

        );



    CLI_defineCmd("MEA profile sflow Prof set create",
        (CmdFunc)MEA_CLI_Sflow_Prof_Create,
        "create   profile\n",
        "create   profile \n"
        "    -f <Id>   (1:3)\n"
        "   -pkt  <packets>\n"
        "   -timeOut  <0:3> \n"
        "   -priQ  <value>  0:7\n"
        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow create -f 1 -pkt 16000 -timeOut 0 -priQ 3 \n"
        "           \n"
    );
    CLI_defineCmd("MEA profile sflow Prof set modify",
        (CmdFunc)MEA_CLI_Sflow_Prof_Set,
        "set   profile\n",
        "set   profile \n"
        "    <Id>  \n"
        "   -pkt  <packets>\n"
        "   -timeOut  <0:3> \n"
        "   -priQ  <value>  0:7\n"
        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow set modify 1  -pkt 16000 -timeOut 1  -priQ 2  \n"
        "       sflow set modify 2   \n"
    );

    CLI_defineCmd("MEA profile sflow Prof show",
        (CmdFunc)MEA_CLI_Sflow_Prof_Get,
        "show   sflow Prof\n",
        "show   sflow Prof\n"
        "    <Id>  all/Id\n"

        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow Prof show 1    \n"
        "       sflow Prof show all   \n"
    );

    CLI_defineCmd("MEA profile sflow Prof set delete",
        (CmdFunc)MEA_CLI_Sflow_Prof_Delete,
        "delete   sflow Prof\n",
        "delete   sflow Prof\n"
        "    <Id>  all/Id\n"

        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow Prof delete 1    \n"
        "       sflow Prof delete all   \n"
    );



    CLI_defineCmd("MEA profile sflow count set",
        (CmdFunc)MEA_CLI_Sflow_count_set,
        "show   sflow set counter prof \n",
        "show   sflow \n"
        "    <Id>  all/Id\n"
        "     -prof <valid> <value 1 : 3>\n"
        "---------------------------------------------------\n"
        "Examples: \n"
        "       sflow count set 1  -prof 1 3 \n"
        "       sflow count set 20 -prof 0 0  \n"
    );
    
        CLI_defineCmd("MEA profile sflow count delete",
        (CmdFunc)MEA_CLI_Sflow_count_delete,
            "show   sflow count\n",
            "show   sflow count \n"
            "    <Id>  all/Id\n"

            "---------------------------------------------------\n"
            "Examples: \n"
            "       count show 1    \n"
            "       count show all   \n"
        );

    CLI_defineCmd("MEA profile sflow count show",
        (CmdFunc)MEA_CLI_Sflow_count_Get,
        "show   sflow count\n",
        "show   sflow count \n"
        "    <Id>  all/Id\n"

        "---------------------------------------------------\n"
        "Examples: \n"
        "       count show 1    \n"
        "       count show all   \n"
    );


    
    return MEA_OK;
}














