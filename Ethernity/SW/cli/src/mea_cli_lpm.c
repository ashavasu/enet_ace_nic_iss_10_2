/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*---------------------------------------------------------------------------*/
/*                                 Includes                                  */
/*---------------------------------------------------------------------------*/

#include "MEA_platform.h"
#include "mea_drv_common.h"
#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_lpm.h"

/*---------------------------------------------------------------------------*/
/*                                 Defines                                   */
/*---------------------------------------------------------------------------*/

#define ADDR_BUFF_SIZE 64
#define STRING_MAX_SIZE 256
#define MAX_PREFIX_LEN_IPV4 32
#define MAX_PREFIX_LEN_IPV6 64
#define MAX_VRF_LEN 7

typedef enum
{
    IPv4,
    IPv6
}MEA_CLI_LPM_IP_Version;

/*---------------------------------------------------------------------------*/
/*                                 Globals                                   */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                            Forward declaration                            */
/*---------------------------------------------------------------------------*/

static MEA_Status MEA_CLI_LPM_ParseIP(char *str, MEA_LPM_Entry_key_t* key, MEA_CLI_LPM_IP_Version ipVer);
static MEA_Status MEA_CLI_LPM_parseArgs(int argc, char *argv[], MEA_Uint32 i, MEA_LPM_Entry_key_t* key, MEA_CLI_LPM_IP_Version ipVer);
static MEA_Status MEA_CLI_LPM_parseAction(int argc, char *argv[], MEA_Uint32 i, MEA_LPM_Entry_data_t* data);
static MEA_Status MEA_CLI_LPM_PrintIPv4TableEntry(const MEA_LPM_Entry_key_t* key, MEA_Bool* first, char* ipAddr_str);
static MEA_Status MEA_CLI_LPM_PrintIPv6TableEntry(const MEA_LPM_Entry_key_t* key, MEA_Bool* first, char* ipAddr_str);
static MEA_Status MEA_CLI_LPM_Show_Generic(MEA_CLI_LPM_IP_Version ipVer);
static MEA_Status MEA_CLI_LPM_parseMaskKeyType(int argc, char *argv[], MEA_Uint32 i, MEA_LPM_Entry_key_t* key);

/*---------------------------------------------------------------------------*/
/*                            CLI Functions                                  */
/*---------------------------------------------------------------------------*/

static MEA_Status MEA_CLI_LPM_PrintIPv4TableEntry(const MEA_LPM_Entry_key_t* key, MEA_Bool* first, char* ipAddr_str)
{
    MEA_OS_inet_ntoa(MEA_OS_htonl(key->IpAddr.all), ipAddr_str, sizeof(ipAddr_str));
    if(*first) 
    {
        CLI_print("Ip Address         Mask  VRF  Mask Key Type\n"
                  "---------------    ----  ---  -------------\n");
        *first = MEA_FALSE;
    }
    CLI_print("%-15s  %4d%5d%5d\n",
                  ipAddr_str,
                  key->IpMaskInBits,
                  key->vrf,
                  key->maskKeyType);
    return MEA_OK;
}

static MEA_Status MEA_CLI_LPM_PrintIPv6TableEntry(const MEA_LPM_Entry_key_t* key, MEA_Bool* first, char* ipAddr_str)
{
    struct in6_addr address;
    ((MEA_Uint32 *)(address.s6_addr))[0] = MEA_OS_htonl(key->Ipv6Addr[0]);
    ((MEA_Uint32 *)(address.s6_addr))[1] = MEA_OS_htonl(key->Ipv6Addr[1]);
    ((MEA_Uint32 *)(address.s6_addr))[2] = MEA_OS_htonl(key->Ipv6Addr[2]);
    ((MEA_Uint32 *)(address.s6_addr))[3] = MEA_OS_htonl(key->Ipv6Addr[3]);
    if(inet_ntop(AF_INET6, &address, ipAddr_str, ADDR_BUFF_SIZE) == NULL)
    {
        return MEA_ERROR;
    }
    if(*first) 
    {
        CLI_print("Ip Address                  Mask  VRF\n"
                  "---------------             ----  ---\n");
        *first = MEA_FALSE;
    }
    CLI_print("%-24s  %4d%5d\n", ipAddr_str, key->IpMaskInBits, key->vrf);

    return MEA_OK;
}

static MEA_Status MEA_CLI_LPM_Show_Generic(MEA_CLI_LPM_IP_Version ipVer)
{
    MEA_LPM_Entry_key_t   key;
    MEA_LPM_Entry_data_t  data;
    MEA_Bool              found;
    char                  ipAddr_str[ADDR_BUFF_SIZE];
    MEA_Bool              first = MEA_TRUE;
    if(ipVer == IPv4)
    {
        key.Ipv4Addr_en = 1;
        key.Ipv6Addr_en = 0;
    }
    else
    {
        key.Ipv4Addr_en = 0;
        key.Ipv6Addr_en = 1;
    }
    if (MEA_API_GetFirst_LPM(MEA_UNIT_0,
                             &key,
                             &data,
                             &found) != MEA_OK) {
        CLI_print("Error: MEA_API_GetFirst_LPM failed\n");
        return MEA_ERROR;
    }

    while (found) 
    {
        if(ipVer == IPv4)
        {
            if(MEA_CLI_LPM_PrintIPv4TableEntry(&key, &first, ipAddr_str) != MEA_OK)
            {
                CLI_print("Error: Show IPv4 table failed\n");
                return MEA_ERROR;
            }
        }
        else
        {
            if(MEA_CLI_LPM_PrintIPv6TableEntry(&key, &first, ipAddr_str) != MEA_OK)
            {
                CLI_print("Error: Show IPv6 table failed\n");
                return MEA_ERROR;
            }
        }
        if (MEA_API_GetNext_LPM(MEA_UNIT_0,
                                &key,
                                &data,
                                &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetNext_LPM failed\n");
            return MEA_ERROR;
        }
    }

    if (first) {
        CLI_print("No LPM entries\n");
    }
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_LPM_Show>                                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_LPM_Show(int argc, char *argv[])
{
    MEA_Status err = MEA_OK;
    if (argc != 1) {
        return MEA_ERROR;
    }
    CLI_print("IPv4 table entries:\n");
    err = MEA_CLI_LPM_Show_Generic(IPv4);
    CLI_print("\nIPv6 table entries:\n");
    err |= MEA_CLI_LPM_Show_Generic(IPv6);

    return err;
}

static MEA_Status MEA_CLI_LPM_ParseIP(char *ipStr, MEA_LPM_Entry_key_t* key, MEA_CLI_LPM_IP_Version ipVer)
{
    if(ipVer == IPv4)
    {
        key->Ipv4Addr_en = 1;
        key->Ipv6Addr_en = 0;
        MEA_Uint32 err = inet_pton(AF_INET , ipStr, &(key->IpAddr.all));
        if (err <= 0) 
        {
            if (err == 0)
                CLI_print("IP not in IPv4 presentation format\n");
            return MEA_ERROR;
        }
        key->IpAddr.all = MEA_OS_ntohl(key->IpAddr.all);
    }
    else
    {
        key->Ipv4Addr_en = 0;
        key->Ipv6Addr_en = 1;
        MEA_Uint32 err = inet_pton(AF_INET6 , ipStr, key->Ipv6Addr);
        if (err <= 0) 
        {
            if (err == 0)
                CLI_print("IP not in IPv6 presentation format\n");
            return MEA_ERROR;
        }
        key->Ipv6Addr[0] = MEA_OS_ntohl(key->Ipv6Addr[0]);
        key->Ipv6Addr[1] = MEA_OS_ntohl(key->Ipv6Addr[1]);
        key->Ipv6Addr[2] = MEA_OS_ntohl(key->Ipv6Addr[2]);
        key->Ipv6Addr[3] = MEA_OS_ntohl(key->Ipv6Addr[3]);
    }
    return MEA_OK;
}

static MEA_Status MEA_CLI_LPM_parseArgs(int argc, char *argv[], MEA_Uint32 i, MEA_LPM_Entry_key_t* key, MEA_CLI_LPM_IP_Version ipVer)
{
    if((i + 1) < (MEA_Uint32)argc)
    {
        if(MEA_CLI_LPM_ParseIP(argv[i + 1], key, ipVer) != MEA_OK)
        {
            return MEA_ERROR;
        }
    }
    else
    {
        return MEA_ERROR; // we expect to receive at least one legal value 
    }
    if((((i + 2) < (MEA_Uint32)argc)))
    {
        key->IpMaskInBits = MEA_OS_atoi(argv[i + 2]);
    }
    else
    {
        return MEA_OK; 
    }
    if((((i + 3) < (MEA_Uint32)argc)))
    {
        key->vrf = MEA_OS_atoi(argv[i + 3]);
    }
    if(ipVer == IPv4)
    {
        if(key->IpMaskInBits > MAX_PREFIX_LEN_IPV4)
        {
            CLI_print("Prefix length for IPv4 should not exceed %u bits.\n", MAX_PREFIX_LEN_IPV4);
            return MEA_ERROR;
        }
    }
    else
    {
        if(key->IpMaskInBits > MAX_PREFIX_LEN_IPV6)
        {
            CLI_print("Prefix length for IPv6 should not exceed %u bits.\n", MAX_PREFIX_LEN_IPV6);
            return MEA_ERROR;
        }
    }
    if(key->vrf > MAX_VRF_LEN)
    {
        CLI_print("VRF length should not exceed %u.\n", MAX_VRF_LEN);
        return MEA_ERROR;
    }
    return MEA_OK;
}

static MEA_Status MEA_CLI_LPM_parseAction(int argc, char *argv[], MEA_Uint32 i, MEA_LPM_Entry_data_t* data)
{
    if((((i + 1) < (MEA_Uint32)argc)))
    {
        data->actionId_valid = MEA_OS_atoi(argv[i + 1]);
    }
    else
    {
        return MEA_ERROR; // we expect to receive at least one legal value 
    }
    if((((i + 2) < (MEA_Uint32)argc)))
    {
        data->actionId = MEA_OS_atoi(argv[i + 2]);
    }
    return MEA_OK;
}

static MEA_Status MEA_CLI_LPM_parseMaskKeyType(int argc, char *argv[], MEA_Uint32 i, MEA_LPM_Entry_key_t* key)
{
    if((((i + 1) < (MEA_Uint32)argc)))
    {
        key->maskKeyType = MEA_OS_atoi(argv[i + 1]);
    }
    else
    {
        return MEA_ERROR; // we expect to receive at least one legal value 
    }
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_LPM_Create>                                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_LPM_Create(int argc, char *argv[])
{
    MEA_LPM_Entry_key_t   key;
    MEA_LPM_Entry_data_t  data;
    int i;


    if ((argc < 3) || (argc > 10)) { 
        return MEA_ERROR;
    }
    MEA_OS_memset(&key,'\0',sizeof(key));
    MEA_OS_memset(&data,'\0',sizeof(data));

    for (i = 1; i < argc; i++) 
    {
        if(MEA_OS_strcmp(argv[i], "-IPv4") == 0) 
        {
            if(MEA_CLI_LPM_parseArgs(argc, argv, i, &key, IPv4) != MEA_OK)
            {
                return MEA_ERROR;
            }
        }
        if(MEA_OS_strcmp(argv[i], "-IPv6") == 0) 
        {
            if(MEA_CLI_LPM_parseArgs(argc, argv, i, &key, IPv6) != MEA_OK)
            {
                return MEA_ERROR;
            }
        }
        if(MEA_OS_strcmp(argv[i], "-Action") == 0) 
        {
            if(MEA_CLI_LPM_parseAction(argc, argv, i, &data) != MEA_OK)
            {
                return MEA_ERROR;
            }
        }
        if(MEA_OS_strcmp(argv[i], "-MaskKeyType") == 0) 
        {
            if(MEA_CLI_LPM_parseMaskKeyType(argc, argv, i, &key) != MEA_OK)
            {
                return MEA_ERROR;
            }
        }
    }
    if (MEA_API_Create_LPM(MEA_UNIT_0, &key, &data) != MEA_OK) 
    {
        CLI_print("Error: MEA_API_Create_LPM failed\n");
        return MEA_ERROR;
    }

    CLI_print("Info: LPM Entry successfully created\n");
 
    return MEA_OK;
}

static MEA_Status MEA_CLI_LPM_deleteAllPerIpVersion(MEA_CLI_LPM_IP_Version ipVer)
{
    MEA_LPM_Entry_key_t   key;
    MEA_Bool              found;
    if(ipVer == IPv4)
    {
        key.Ipv4Addr_en = 1;
        key.Ipv6Addr_en = 0;
    }
    else
    {
        key.Ipv4Addr_en = 0;
        key.Ipv6Addr_en = 1;   
    }
    if (MEA_API_GetFirst_LPM(MEA_UNIT_0,
                             &key,
                             NULL,
                             &found) != MEA_OK) {
        CLI_print("Error: MEA_API_GetFirst_LPM failed\n");
        return MEA_ERROR;
    }

    while (found) {
        if (MEA_API_Delete_LPM(MEA_UNIT_0, &key) != MEA_OK) {
            CLI_print("Error: MEA_API_Delete_LPM failed\n");
            return MEA_ERROR;
        }

        if (MEA_API_GetNext_LPM(MEA_UNIT_0,
                                &key,
                                NULL,
                                &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetNext_LPM failed\n");
            return MEA_ERROR;
        }
        
    }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_LPM_DeleteAll>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_LPM_DeleteAll(void)
{
    MEA_Status err = MEA_CLI_LPM_deleteAllPerIpVersion(IPv4);
    err |= MEA_CLI_LPM_deleteAllPerIpVersion(IPv6);

    return err;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_LPM_Delete>                                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_LPM_Delete(int argc, char *argv[])
{
    MEA_LPM_Entry_key_t   key;
    int i;


    if ((argc                         == 2) && 
        (MEA_OS_strcmp(argv[1],"all") == 0)  ) {
        return MEA_CLI_LPM_DeleteAll();
    }

    if (argc != 5) {
        return MEA_ERROR;
    }
    MEA_OS_memset(&key,'\0',sizeof(key));

    for (i = 1; i < argc; i++)
    {
        if(MEA_OS_strcmp(argv[i], "-IPv4") == 0) 
        {
            if(MEA_CLI_LPM_parseArgs(argc, argv, i, &key, IPv4) != MEA_OK)
            {
                return MEA_ERROR;
            }
        }
        if(MEA_OS_strcmp(argv[i], "-IPv6") == 0) 
        {
            if(MEA_CLI_LPM_parseArgs(argc, argv, i, &key, IPv6) != MEA_OK)
            {
                return MEA_ERROR;
            }
        }
    }
    
    if (MEA_API_Delete_LPM(MEA_UNIT_0,
                           &key) != MEA_OK) {
        CLI_print("Error: MEA_API_Delete_LPM failed\n");
        return MEA_ERROR;
    }

    CLI_print("Info: LPM Entry successfully deleted\n");
 
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                            <MEA_CLI_LPM_AddCmds>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_CLI_LPM_AddCmds(void) 
{
    CLI_defineCmd("MEA LPM show all",
        (CmdFunc)MEA_CLI_LPM_Show,
        "Show LPM(s)\n",
        "show\n");

    CLI_defineCmd("MEA LPM set create",
        (CmdFunc)MEA_CLI_LPM_Create,
        "Create LPM\n",
        "create -<ipType> <ipAddr> [<ipMaskInBits>] [<vrf>] [-MaskKeyType <maskKeyType>] [-Action <isValid> [<actionID>]]\n"
        "     -<ipType> : IP protocol (-IPv4, -IPv6) \n"
        "          <ipAddr> : IP address (1.2.3.4, 1:2:3:4:5:6:7:8 (all shortcuts for IPv6 available: 1:2:3:4::0, etc)) \n"        
        "          <ipMaskInBits> : address prefix (for IPv4: 8 - 32, for IPv6: 16 - 64) \n"
        "          <vrf> : virtual routing and forwarding (0 - 7) \n"
        "     -MaskKeyType (round number - this option is relevant only for IPv4) \n"
        "          <maskKeyType> (0 - 3) \n"
        "     -Action (action ID) \n"
        "          <isValid> (0 or 1) \n"
        "          <actionID> (action ID) \n"
        "------------------------------------------------------------------\n"
        "Example: MEA LPM set create -IPv4 1.2.3.4 24 5 -MaskKeyType 2 -Action 1 25\n"
        "Example: MEA LPM set create -IPv6 1:2:3:4:5:6:7:8 24 5 -Action 1 26\n");

    CLI_defineCmd("MEA LPM set delete",
        (CmdFunc)MEA_CLI_LPM_Delete,
        "Delete LPM\n",
        "delete all | -<ipType> <ipAddr> [<ipMaskInBits>] [<vrf>]\n"
        "     -<ipType> : IP protocol (-IPv4, -IPv6) \n"
        "          <ipAddr> : IP address (1.2.3.4, 1:2:3:4:5:6:7:8 (all shortcuts for IPv6 available: 1:2:3:4::0, etc)) \n"        
        "          <ipMaskInBits> : address prefix (for IPv4: 8 - 32, for IPv6: 16 - 64) \n"
        "          <vrf> : virtual routing and forwarding (0 - 7) \n"
        "------------------------------------------------------------------\n"
        "Example: MEA LPM set delete all"
        "Example: MEA LPM set delete -IPv4 1.2.3.4 24 5 -Action 1 25\n"
        "Example: MEA LPM set delete -IPv6 1:2:3:4:5:6:7:8 24 5 -Action 1 26\n");


    return MEA_OK;
}


