/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/



#include "mea_api.h"
#include "mea_drv_common.h"
#include "cli_eng.h"
#include "mea_cli.h"

#include "mea_preATM_parser_drv.h"
#include "mea_cli_preATM_parser.h"





/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_CLI_preATM_parser_Create(int argc, char *argv[])
{
    MEA_Port_t                   port;
    MEA_Uint8                    vsp_type;
    MEA_PreATMParser_Data_dbt    entry;
    int i,num_of_params;

      if (argc < 3) {
      return MEA_ERROR;
      }
      
      MEA_OS_memset(&entry,0,sizeof(entry));
      
      
      port=MEA_OS_atoi(argv[1]);
      vsp_type=MEA_OS_atoi(argv[2]);
      
      for(i=0; i<argc ;i++){

      if(!MEA_OS_strcmp(argv[i],"-key")){ 
          num_of_params=3;
          if ((i+num_of_params)>=argc ) {
              CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
              return MEA_ERROR;
          }
                  entry.key.L2_protocol_type       = MEA_OS_atoi(argv[i+1]);
                  entry.key.vpi                          = MEA_OS_atoi(argv[i+2]);
                   entry.key.vci                         = MEA_OS_atoi(argv[i+3]);
                  
      }
    }
      
      
      
      if(MEA_API_ATM_Parser_Entry_Create(MEA_UNIT_0,
                                    port,
                                    vsp_type,
                                    &entry)!=MEA_OK){
      
            CLI_print("Error MEA_API_ATM_Parser_Entry_Create failed\n");
      return MEA_OK;
      }
      
      
      CLI_print("Done\n");
      return MEA_OK;
}

MEA_Status MEA_CLI_preATM_parser_Modify(int argc, char *argv[])
{
    MEA_Port_t                   port;
    MEA_Uint8                    vsp_type;
    MEA_PreATMParser_Data_dbt    entry;


    int i,num_of_params;
    
    if (argc < 3) {
      return MEA_ERROR;
    }
      
      MEA_OS_memset(&entry,0,sizeof(entry));
      
      
      port=MEA_OS_atoi(argv[1]);
      vsp_type=MEA_OS_atoi(argv[2]);
      
      for(i=0; i<argc ;i++){

          if(!MEA_OS_strcmp(argv[i],"-key")){ 
              num_of_params=3;
              if ((i+num_of_params)>=argc ) {
                  CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                  return MEA_ERROR;
              }
              entry.key.L2_protocol_type       = MEA_OS_atoi(argv[i+1]);
              entry.key.vpi                          = MEA_OS_atoi(argv[i+2]);
              entry.key.vci                         = MEA_OS_atoi(argv[i+3]);

          }
      }
      
      
      
    if(MEA_API_ATM_Parser_Entry_Set(MEA_UNIT_0,
                                port,
                                vsp_type,
                                &entry)!=MEA_OK){

        CLI_print("Error MEA_API_ATM_Parser_Entry_Create failed\n");
        return MEA_OK;
    }


    CLI_print("Done\n");
    return MEA_OK;
}

MEA_Status MEA_CLI_preATM_parser_Delete(int argc, char *argv[])
{
    MEA_Port_t                   port,first_port,end_port;
    MEA_Uint8                    vsp_type,first_vsp_type,end_vsp_type;
    char buf[100];
    char *up_ptr;
    MEA_Bool exist;

      if (argc < 3) {
      return MEA_ERROR;
      }
      
      
      
      
      
      if (MEA_OS_strcmp(argv[1],"all") != 0) {
            MEA_OS_strcpy(buf, argv[1]);
            if((up_ptr=strchr(buf, ':')) != 0)
            {
                  (*up_ptr) = 0;
                  up_ptr++;
                  first_port = MEA_OS_atoi(buf);
                  end_port   = MEA_OS_atoi(up_ptr);
            }
            else
            {
                  first_port = end_port = atoi(buf);
            }
            
      }else{
        first_port=0;
        end_port= MEA_MAX_PORT_NUMBER;
      }
      
      if (MEA_OS_strcmp(argv[2],"all") != 0) {
            MEA_OS_strcpy(buf, argv[2]);
            if((up_ptr=strchr(buf, ':')) != 0)
            {
                  (*up_ptr) = 0;
                  up_ptr++;
                  first_vsp_type = MEA_OS_atoi(buf);
            if(first_vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
            CLI_print("Error vsp_type out of range\n");
            return MEA_OK;
            }
                  end_vsp_type   = MEA_OS_atoi(up_ptr);
            if(end_vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
                CLI_print("Error vsp_type out of range\n");
                return MEA_OK;
            }
            }
            else
            {
                  first_vsp_type = end_vsp_type = atoi(buf);
            if(first_vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
                CLI_print("Error vsp_type out of range\n");
                return MEA_OK;
            }
            }
            
      }else{
        first_vsp_type=0;
        end_vsp_type=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE;
      }
            
      
    for(port=first_port;port<=end_port;port++){
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE)==MEA_FALSE)
            continue;
        for(vsp_type=first_vsp_type;vsp_type<=end_vsp_type;vsp_type++){

            if(MEA_API_ATM_Parser_Entry_IsExist(MEA_UNIT_0,port,vsp_type,MEA_TRUE,&exist)!=MEA_OK){
                continue;
            }
            if(!exist)
                continue;

            if(MEA_API_ATM_Parser_Entry_Delete(MEA_UNIT_0,
                                            port,
                                            vsp_type)!=MEA_OK){

                CLI_print("Error MEA_API_ATM_Parser_Entry_Create failed\n");
                return MEA_OK;
            }
        }
    }
      
      CLI_print("Done\n");
      return MEA_OK;
}
MEA_Status MEA_CLI_preATM_parser_Show(int argc, char *argv[])
{

    MEA_Port_t                   port,first_port,end_port;
    MEA_Uint8                    vsp_type,first_vsp_type,end_vsp_type;
    char buf[100];
    char *up_ptr;
    MEA_PreATMParser_Data_dbt   entry;
    MEA_Bool exist;
    MEA_Uint32 count=0;
    char proto_str[MEA_ATM_PROTOCOL_LAST][20];

    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_NONE         ][0]),  "NONE           ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_EoLLCoA      ][0]),  "EoLLCoA        ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_EoVCMUXoA    ][0]),  "EoVCMUXoA      ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_IPoLLCoA     ][0]),  "IPoLLCoA       ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_IPoVCMUXoA   ][0]),  "IPoVCMUXoA     ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_PPPoLLCoA    ][0]),  "PPPoLLCoA      ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_PPPoVCMUXoA  ][0]),  "PPPoVCMUXoA    ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_VoEoLLCoA    ][0]),  "VoEoLLCoA      ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_VoEoVCMUXoA  ][0]),  "VoEoVCMUXoA    ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_PPPoEoA      ][0]),  "PPPoEoA        ");
    MEA_OS_strcpy(&(proto_str[MEA_ATM_PROTOCOL_PPPoEVCMUXoA ][0]),  "PPPoEVCMUXoA   ");


    if (argc < 3) {
        return MEA_ERROR;
    }





    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_port = MEA_OS_atoi(buf);
            end_port   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            first_port = end_port = atoi(buf);
        }

    }else{
        first_port=0;
        end_port= MEA_MAX_PORT_NUMBER;
    }

    if (MEA_OS_strcmp(argv[2],"all") != 0) {
        MEA_OS_strcpy(buf, argv[2]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_vsp_type = MEA_OS_atoi(buf);
            if(first_vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
                CLI_print("Error vsp_type out of range\n");
                return MEA_OK;
            }
            end_vsp_type   = MEA_OS_atoi(up_ptr);
            if(end_vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
                CLI_print("Error vsp_type out of range\n");
                return MEA_OK;
            }
        }
        else
        {
            first_vsp_type = end_vsp_type = atoi(buf);
            if(first_vsp_type >=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE){
                CLI_print("Error vsp_type out of range\n");
                return MEA_OK;
            }
        }

    }else{
        first_vsp_type=0;
        end_vsp_type=MEA_PRE_ATM_PARSER_MAX_VSP_TYPE;
    }


    for(port=first_port;port<=end_port;port++){
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE)==MEA_FALSE)
            continue;
        for(vsp_type=first_vsp_type;vsp_type<=end_vsp_type;vsp_type++){

            if(MEA_API_ATM_Parser_Entry_IsExist(MEA_UNIT_0,port,vsp_type,MEA_TRUE,&exist)!=MEA_OK){
              continue;
            }
            if(!exist)
               continue;

            if(count==0){
                CLI_print("\n");
                CLI_print("--------------ATM Parser configure------------------\n");
                CLI_print(" port vsp   protocol             vpi vci   vspId    \n");
                CLI_print("-----------------------------------------------------\n");
             
            }
            count++;
            if(MEA_API_ATM_Parser_Entry_Get(MEA_UNIT_0,
                port,
                vsp_type,
                &entry)!=MEA_OK){

                    CLI_print("Error MEA_API_ATM_Parser_Entry_Create failed\n");
                    return MEA_OK;
            }
            CLI_print(" %4d %3d (%2d) %-15s %3d %5d %5d\n",port,vsp_type,entry.key.L2_protocol_type,proto_str[entry.key.L2_protocol_type],entry.key.vpi,entry.key.vci,entry.vspId);



        }
    }

    if(count){
        CLI_print("number of entry %d\n",count);
    }else{
        CLI_print("Table is empty \n");
    }
      
      return MEA_OK;
}









/************************************************************************/
/* CLI                                                                     */
/************************************************************************/
MEA_Status MEA_CLI_preATM_parser_AddDebugCmds(void)
{
    
    

        /************************************************************************/
        CLI_defineCmd("MEA ATM parser set create",
        (CmdFunc)MEA_CLI_preATM_parser_Create, 
        "set create atm parser \n",
        "set create  <port><vsp_type>  -key <atm_protocol> <vpi> <vci>\n"
        "--------------------------------------------------------------------------------\n"
        "       create \n"  
            "        <port> utopia src port \n"
            "        <vsp_type> 0..3\n"
        "        -key (parameter) <atm_protocol><vpi><vci>\n"
            "            <atm_protocol> protocol type\n"
            "                          EoLLCoA=17        \n"      
            "                          EoVCMUXoA=18      \n"        
            "                          IPoLLCoA=19       \n"       
            "                          IPoVCMUXoA=20     \n" 
            "                          PPPoLLCoA=21      \n"      
            "                          PPPoVCMUXoA=22    \n"
            "                          VoEoLLCoA=23      \n"       
            "                          VoEoVCMUXoA2=24   \n"
            "                          PPPoEoA=25        \n"
            "                          PPPoEVCMUXoA=26 \n"
            "            <vpi>   Virtual Path Tunnel interface     \n"
            "            <vci>   Virtual Channels      \n"
        "--------------------------------------------------------------------------------\n"
        "Examples: - create 10 0 -key 17 0 35\n"
        "          - create 10 1 -key 23 0 35\n");
            
            
            CLI_defineCmd("MEA ATM parser set Modify",
        (CmdFunc)MEA_CLI_preATM_parser_Modify, 
        "set modify atm parser \n",
        "set modify  <port><vsp_type>  -key <atm_protocol> <vpi> <vci>\n"
        "--------------------------------------------------------------------------------\n"
        "       modify \n"  
            "        <port> utopia src port \n"
            "        <vsp_type> 0..3\n"
        "        -key (parameter) <atm_protocol><vpi><vci>\n"
        "            <atm_protocol> protocol type\n"
            "                          EoLLCoA=17        \n"      
            "                          EoVCMUXoA=18      \n"        
            "                          IPoLLCoA=19       \n"       
            "                          IPoVCMUXoA=20     \n" 
            "                          PPPoLLCoA=21      \n"      
            "                          PPPoVCMUXoA=22    \n"
            "                          VoEoLLCoA=23      \n"       
            "                          VoEoVCMUXoA2=24   \n"
            "                          PPPoEoA=25        \n"
            "                          PPPoEVCMUXoA=26 \n"
            "            <vpi>   Virtual Path Tunnel interface     \n"
            "            <vci>   Virtual Channels      \n"
        "--------------------------------------------------------------------------------\n"
        "Examples: - modify 10 0 -key 17 0 35\n"
        "          - modify 10 1 -key 23 0 35\n");
            
            CLI_defineCmd("MEA ATM parser set delete",
        (CmdFunc)MEA_CLI_preATM_parser_Delete,
        "set delete atm parser \n",
        "set delete  <port/all><vsp_type/all>  \n"
        "--------------------------------------------------------------------------------\n"
        "       delete \n"  
            "        <port>/all utopia src port \n"
            "        <vsp_type>/all (0..3)\n"
        "--------------------------------------------------------------------------------\n"
        "Examples: - delete 10 0 \n"
        "          - delete 10 1 \n");
            
            CLI_defineCmd("MEA ATM parser show",
        (CmdFunc)MEA_CLI_preATM_parser_Show,
        " show atm parser \n",
        " show  <port/all><vsp_type/all>  \n"
        "--------------------------------------------------------------------------------\n"
        "       show \n"  
            "        <port>/all utopia src port \n"
            "        <vsp_type>/all (0..3)\n"
        "--------------------------------------------------------------------------------\n"
        "Examples: - show all 0 \n"
        "          - show 10 all \n");





    return MEA_OK;
}

