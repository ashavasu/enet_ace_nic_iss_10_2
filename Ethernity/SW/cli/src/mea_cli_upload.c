/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"

#include "mea_bm_regs.h"
#include "mea_if_regs.h"
#include "mea_device_regs.h"
#include "mea_deviceInfo_drv.h"
#include "mea_cli_upload.h"
#include "cli_eng.h"

#define  MEA_CLI_UPLOAD_SECRET_FORMAT

MEA_Status MEA_CLI_UPLOAD_device(int argc, char *argv[]);

MEA_Status MEA_CLI_UPLOAD_read_reg(MEA_module_te module, MEA_Uint32 reg);

MEA_Status MEA_CLI_UPLOAD_read_table(MEA_module_te module,
				     MEA_Uint32 table,
				     MEA_Uint32 length, MEA_Uint32 width);

MEA_Status MEA_CLI_UPLOAD_read_service_table(MEA_Uint32 max_length,
					     MEA_Uint32 width);

MEA_Status MEA_CLI_UPLOAD_AddDebugCmds(void)
{

#ifdef MEA_CLI_UPLOAD_SECRET_FORMAT
	CLI_defineCmd("MEA debug upload",
		      (CmdFunc) MEA_CLI_UPLOAD_device,
		      "Usage   :   upload device \n",
		      "Examples: - upload device \n");
#else
	CLI_defineCmd("MEA debug upload device",
		      (CmdFunc) MEA_CLI_UPLOAD_device,
		      "Usage   :   device <format> \n"
		      "          <format> : cli | gen  | all\n",
		      "Examples: - device cli\n"
		      "          - device gen\n"
		      "          - device all\n");
#endif

	return MEA_OK;
}

MEA_Status MEA_CLI_UPLOAD_device(int argc, char *argv[])
{

#ifdef MEA_CLI_UPLOAD_SECRET_FORMAT

	if (argc != 2)
		return MEA_ERROR;

	/* automatic generate file with calls to upload configuration */
#include "mea_cli_upload_calls.h"

#ifdef MEA_OS_LINUX
//#warning Need to add this call to the mea_cli_upload_calls.h 
#endif
	MEA_CLI_UPLOAD_read_service_table(512 * 4, 79);

#else				/* MEA_CLI_UPLOAD_SECRET_FORMAT */

	MEA_Bool gen = MEA_FALSE;
	MEA_Bool cli = MEA_FALSE;

	if (argc != 2)
		return MEA_ERROR;

	if (MEA_OS_strcmp(argv[1], "gen") == 0) {
		gen = MEA_TRUE;
	} else {
		if (MEA_OS_strcmp(argv[1], "cli") == 0) {
			cli = MEA_TRUE;
		} else {
			if (MEA_OS_strcmp(argv[1], "all") == 0) {
				cli = MEA_TRUE;
				gen = MEA_TRUE;
			} else {
				return MEA_ERROR;
			}
		}
	}

	if (gen) {
		CLI_print("Not support gen format \n");
		return MEA_OK;
	}

	if (cli) {

		CLI_print("proc main \n");

		/* automatic generate file with calls to upload configuration */
#include "mea_cli_upload_calls.h"

#ifdef MEA_OS_LINUX
//#warning Need to add this call to the mea_cli_upload_calls.h 
#endif

		MEA_CLI_UPLOAD_read_service_table(512 * 4, 79);

		CLI_print("endproc\n");

	}
#endif				/* MEA_CLI_UPLOAD_SECRET_FORMAT */

	return MEA_OK;
}

MEA_Status MEA_CLI_UPLOAD_read_reg(MEA_module_te module, MEA_Uint32 reg)
{

	MEA_Uint32 value;

	value = MEA_API_ReadReg(MEA_UNIT_0, reg, module);

#ifdef MEA_CLI_UPLOAD_SECRET_FORMAT
	CLI_print("1 %d %08x %08x\n", module, reg, value);
#else				/* MEA_CLI_UPLOAD_SECRET_FORMAT */
	CLI_print("transmit \"MEA debug module %s_write %08x %08x ^M\"\n",
		  (module == MEA_MODULE_BM) ? "bm" : "if", reg, value);

	CLI_print("waitfor \">\"\n");
#endif				/* MEA_CLI_UPLOAD_SECRET_FORMAT */

	return MEA_OK;

}

MEA_Status MEA_CLI_UPLOAD_read_table(MEA_module_te module,
				     MEA_Uint32 table,
				     MEA_Uint32 length, MEA_Uint32 width)
{
	MEA_ind_read_t ind_read;
	MEA_Uint32 read_data[20];
	MEA_Uint32 num_of_longs;
	MEA_Uint32 i, j;

	num_of_longs = (width / 32);
	if ((width % 32) != 0) {
		num_of_longs++;
	}
	if (num_of_longs > MEA_NUM_OF_ELEMENTS(read_data)) {
		CLI_print("%s - width (%d) too big \n",
			  __FUNCTION__, width);
		return MEA_ERROR;
	}

#ifdef MEA_OS_LINUX
//#warning Need to support also Service Table 
//#warning Need to support also IF Table #3 Reg 
#endif

	ind_read.read_data = read_data;

	switch (module) {

	case MEA_MODULE_BM:
		ind_read.cmdReg = MEA_BM_IA_CMD;
		ind_read.cmdMask = MEA_BM_IA_CMD_MASK;
		ind_read.statusReg = MEA_BM_IA_STAT;
		ind_read.statusMask = MEA_BM_IA_STAT_MASK;
		ind_read.tableAddrReg = MEA_BM_IA_ADDR;
		ind_read.tableAddrMask = MEA_BM_IA_ADDR_OFFSET_MASK;
		break;

	case MEA_MODULE_IF:
		ind_read.cmdReg = MEA_IF_CMD_REG;
		ind_read.cmdMask = MEA_IF_CMD_PARSER_MASK;
		ind_read.statusReg = MEA_IF_STATUS_REG;
		ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
		ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG;
		ind_read.tableAddrMask = MEA_IF_CMD_PARAM_TBL_TYP_MASK;
		break;

	default:
		CLI_print("%s - not supported module = %d \n",
			  __FUNCTION__, module);
		return MEA_ERROR;
	}

	for (i = 0; i < length; i++) {

		ind_read.tableType = table;
		ind_read.tableOffset = i;

		if (MEA_API_ReadIndirect(MEA_UNIT_0,
					 &ind_read,
					 num_of_longs, module) != MEA_OK) {
			CLI_print("%s - ReadIndirect failed \n",
				  __FUNCTION__);
			return MEA_ERROR;
		}
#ifdef MEA_CLI_UPLOAD_SECRET_FORMAT
		CLI_print("2 %d %d %d %d ",
			  module, table, i, num_of_longs);

		for (j = 0; j < num_of_longs; j++) {
			if (j == 3) {
				CLI_print("\n");
			}
			CLI_print("%08x ", read_data[j]);
		}
		CLI_print("\n");
#else				/* MEA_CLI_UPLOAD_SECRET_FORMAT */
		CLI_print
		    ("transmit \"MEA debug module %s_write_ind %d %d %d ",
		     (module == MEA_MODULE_BM) ? "bm" : "if", table, i,
		     num_of_longs);

		for (j = 0; j < num_of_longs; j++) {
			if (j == 3) {
				CLI_print("\"\ntransmit \"");
			}
			CLI_print("%08x ", read_data[j]);
		}
		CLI_print("^M\"\n");
		CLI_print("waitfor \">\"\n");
#endif				/* MEA_CLI_UPLOAD_SECRET_FORMAT */

	}			/* for (i=0; i<length;i++)  */

	return MEA_OK;
}

MEA_Status MEA_CLI_UPLOAD_read_service_table(MEA_Uint32 max_length,
					     MEA_Uint32 width)
{
#if 0
	MEA_Uint32 first_key;
	MEA_ind_read_t ind_read;
	MEA_Uint32 read_data[20];
	MEA_Uint32 num_of_longs;
	MEA_Uint32 i, j;
	MEA_Uint32 tmp;
	MEA_Uint32 pri, srcPort, netTag, cid, last;
	MEA_db_HwUnit_t hwUnit;
	mea_memory_mode_e save_globalMemoryMode;

	for (hwUnit = 0; hwUnit < mea_drv_num_of_hw_units; hwUnit++) {

		MEA_DRV_SET_GLOBAL_MEMORY_MODE(save_globalMemoryMode,
					       hwUnit);

		first_key = 0;

		num_of_longs = (width / 32);
		if ((width % 32) != 0) {
			num_of_longs++;
		}
		if (num_of_longs > MEA_NUM_OF_ELEMENTS(read_data)) {
			CLI_print("%s - width (%d) too big \n",
				  __FUNCTION__, width);
			return MEA_ERROR;
		}

		/* build the ind_write value */
		ind_read.tableType = 0;	/* not relevant - service hash table */
		ind_read.tableOffset = 0;	/* not relevant - service hash table */
		ind_read.cmdReg = MEA_IF_CMD_REG;
		ind_read.cmdMask = MEA_IF_CMD_RST_NEXT_MASK;
		ind_read.statusReg = MEA_IF_STATUS_REG;
		ind_read.statusMask = MEA_IF_STATUS_BUSY_MASK;
		ind_read.tableAddrReg = MEA_IF_CMD_PARAM_REG_IGNORE;	/* service hash table */
		ind_read.tableAddrMask = 0;
		ind_read.read_data = NULL;

		/* Read to the hash table as Indirect Table without the table type and entry # */
		if (MEA_API_ReadIndirect(MEA_UNIT_0,
					 &ind_read,
					 0, MEA_MODULE_IF) != MEA_OK) {
			CLI_print
			    ("%s - MEA_API_ReadIndirect srv table Reset  failed \n",
			     __FUNCTION__);
			return MEA_ERROR;
		}

		for (i = 0; i < max_length; i++) {

			ind_read.cmdMask = MEA_IF_CMD_GET_NEXT_MASK;
			ind_read.read_data = read_data;

			/* Read from the hash table as Indirect Table without the table type and entry # */
			if (MEA_API_ReadIndirect(MEA_UNIT_0,
						 &ind_read,
						 num_of_longs,
						 MEA_MODULE_IF) !=
			    MEA_OK) {
				CLI_print
				    ("%s - MEA_API_ReadIndirect srv table Next  failed \n",
				     __FUNCTION__);
				return MEA_ERROR;
			}
			pri = MEA_OS_read_value(MEA_IF_SRV_PRI_START,
						MEA_IF_SRV_PRI_WIDTH,
						read_data);
			srcPort =
			    MEA_OS_read_value(MEA_IF_SRV_SRC_PORT_START,
					      MEA_IF_SRV_SRC_PORT_WIDTH,
					      read_data);
			netTag =
			    MEA_OS_read_value(MEA_IF_SRV_NET_TAG_START,
					      MEA_IF_SRV_NET_TAG_WIDTH,
					      read_data);
			cid =
			    MEA_OS_read_value(MEA_IF_SRV_CID_START,
					      MEA_IF_SRV_CID_WIDTH
					      (MEA_UNIT_0, hwUnit),
					      read_data);
			last =
			    MEA_OS_read_value(MEA_IF_SRV_LAST_START
					      (MEA_UNIT_0, hwUnit),
					      MEA_IF_SRV_LAST_WIDTH,
					      read_data);

			if (cid != 0) {
				tmp =
				    MEA_OS_read_value(MEA_IF_SRV_PRI_START,
						      MEA_IF_SRV_PRI_WIDTH
						      +
						      MEA_IF_SRV_SRC_PORT_WIDTH
						      +
						      MEA_IF_SRV_NET_TAG_WIDTH,
						      read_data);
				if (i == 0) {
					first_key = tmp;
				} else {
					/* This done to avoid FPGA bug that 
					 *  no LAST bit support correct , 
					 *  and we get again the first key */
					if (first_key == tmp) {
						break;
					}
				}

#ifdef MEA_CLI_UPLOAD_SECRET_FORMAT
				CLI_print("2 %d %d %d %d ",
					  MEA_MODULE_IF,
					  99, i, num_of_longs);

				for (j = 0; j < num_of_longs; j++) {
					if (j == 3) {
						CLI_print("\n");
					}
					CLI_print("%08x ", read_data[j]);
				}
				CLI_print("\n");
#else				/* MEA_CLI_UPLOAD_SECRET_FORMAT */

#warning Need to add command for flush service table

				CLI_print
				    ("transmit \"MEA debug module if_srv_write ");

				for (j = 0; j < num_of_longs; j++) {
					if (j == 3) {
						CLI_print("\n");
					}
					CLI_print("%08x ", read_data[j]);
				}
				CLI_print("^M\"\n");
				CLI_print("waitfor \">\"\n");

#endif				/* MEA_CLI_UPLOAD_SECRET_FORMAT */

			}

		}
		MEA_DRV_RESTORE_GLOBAL_MEMORY_MODE(save_globalMemoryMode);

	}
#else
CLI_print("Not support:\n");
#endif
	return MEA_OK;

}





MEA_Status enet_read_Api(void)
{
    // get version

    // read port ingress

    //read port egress

    //get globals

    //read queue

    //read shaper profile

    //read policer profile

    //read action

    //read lxcp

    //read limiter

    //if TDM support
      // profile
     // interface
     // ces
    //read service








    CLI_print("Done read Api");
    return MEA_OK;
}







