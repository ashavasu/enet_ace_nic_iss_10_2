/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*---------------------------------------------------------------------------*/
/*                                 Includes                                  */
/*---------------------------------------------------------------------------*/

#include "MEA_platform.h"
#include "mea_drv_common.h"
#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_forwarder.h"

#if defined(MEA_OS_LINUX) && !defined(__KERNEL__)
#include "sys/time.h"
#include <sys/socket.h>
#include <netinet/in.h>
#endif

/*---------------------------------------------------------------------------*/
/*                                 Defines                                   */
/*---------------------------------------------------------------------------*/
#ifndef MEA_OS_P1
#define MEA_FWD_DEFAULT_NUM_OF_FLOWS 1000
#else
#define MEA_FWD_DEFAULT_NUM_OF_FLOWS 500
#endif
/*---------------------------------------------------------------------------*/
/*                                 Globals                                   */
/*---------------------------------------------------------------------------*/

static MEA_SE_filter_Entry_dbt       mea_cli_global_SE_filter;

/*---------------------------------------------------------------------------*/
/*                            Forward declaration                            */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_CLI_Forwarder_AddDebugCmds(void);


#ifdef NAT_ADAPTOR_WANTED
extern void AdapSetNATRouterStatus(int status);
extern int AdapGetNATRouterStatus(void);
extern MEA_Status MeaAdapAddStaticNatEntry(MEA_SE_Entry_dbt *pEntry);
#endif
/*---------------------------------------------------------------------------*/
/*                            CLI Functions                                  */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Clear_Counters_Forwarder?                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_Clear_Counters_Forwarder(int argc, char *argv[])
{
	return MEA_API_Clear_Counters_Forwarder(MEA_UNIT_0);

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Collect_Counters_Forwarder?                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_Collect_Counters_Forwarder(int argc, char *argv[])
{
    return MEA_API_Collect_Counters_Forwarder(MEA_UNIT_0);
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Show_Counters_Forwarder?                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_Show_Counters_Forwarder(int argc, char *argv[])
{

    MEA_Counters_Forwarder_dbt Forwarder_Counters;
   
    if (MEA_API_Get_Counters_Forwarder(MEA_UNIT_0,&Forwarder_Counters) != MEA_OK) {
        CLI_print("%s - MEA_API_Get_Counters_Forwarder failed\n",
                       __FUNCTION__);
        return MEA_ERROR;
     }


	CLI_print(" Fwd Counters              \n"
 	          " -------------------- --------------------\n");

    CLI_print(" %20s %20llu \n", "fwd match", Forwarder_Counters.fwd_key_match.val);
    CLI_print(" %20s %20llu \n", "Learn not match", Forwarder_Counters.learn_key_not_match.val);
    CLI_print(" %20s %20llu \n", "Learn reject", Forwarder_Counters.learn_failed.val);

    CLI_print(" %20s %20llu \n", "learn dynamic", Forwarder_Counters.learn_dynamic.val);
    CLI_print(" %20s %20llu \n", "static Unicast MAC", Forwarder_Counters.unicast_static_MAC.val);
    CLI_print(" %20s %20llu \n", "static multicast MAC", Forwarder_Counters.multicast_static_MAC.val);
   

     CLI_print(" -------------------- --------------------\n");

	return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Forwarder_AddCountersCmds>                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_CLI_Forwarder_AddCountersCmds(void) {

	CLI_defineCmd("MEA counters forwarder collect",
                  (CmdFunc)MEA_CLI_Collect_Counters_Forwarder,
				  "Collect Forwarding counters ",
                  "collect\n"
                  "Example: collect\n");

	CLI_defineCmd("MEA counters forwarder clear",
                  (CmdFunc)MEA_CLI_Clear_Counters_Forwarder,
				  "Clear   Forwarding counters ",
                  "clear\n"
                  "Example: clear\n");

	CLI_defineCmd("MEA counters forwarder show",
                  (CmdFunc)MEA_CLI_Show_Counters_Forwarder,
				  "Show    Forwarding counters ",
                  "show\n"
                  "Example: show\n");

	return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_ForwarderAging_set>                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_ForwarderAging_set(int argc, char *argv[]){

    MEA_Uint32 seconds;
    MEA_Bool  status;

    if ( argc < 3 )
        return MEA_ERROR;

    status = MEA_OS_atoiNum(argv[1]);
    seconds = MEA_OS_atoiNum(argv[2]);
    
    
    if (MEA_API_Set_SE_Aging(MEA_UNIT_0,seconds,status) != MEA_OK) {
        CLI_print (" Failed on MEA_API_Set_SE_Aging  \n" );
    }
    
    CLI_print("\n");
    return MEA_OK;

}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_ForwarderSearch>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_ForwarderSearch(int argc, char *argv[])
{
   MEA_Port_t                     port;
   MEA_SE_Entry_dbt               entry;
   MEA_Uint32                     extra_param_pos;
   MEA_Uint32 i;
   unsigned char buf[sizeof(struct in6_addr)];
   int  s;


   if ( argc < 2) 
        return MEA_ERROR;

   
 


    MEA_OS_memset(&entry, 0, sizeof(entry));
     
  

   //entry.key.vlan_vpn.type = MEA_OS_atoiNum(argv[1]);
   entry.key.type = MEA_OS_atoiNum(argv[1]);
    extra_param_pos = 2;
	switch (entry.key.type) {
    case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
		  if ( argc < 4) {
              return MEA_ERROR;
	 	  }
		  MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac_plus_vpn.MAC);
          entry.key.mac_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
          extra_param_pos += 2;
		  break;
#if MEA_FWD_MAC_ONLY_SUPPORT
    case MEA_SE_FORWARDING_KEY_TYPE_DA :
		  if ( argc < 3) {
              return MEA_ERROR;
	 	  }
		  MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac.MAC);
          extra_param_pos += 1;
		  break;
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :
        if ( argc < 5) {
            return MEA_ERROR;
        }
        entry.key.mpls_vpn.label_1 = MEA_OS_atoiNum(argv[extra_param_pos]);
        entry.key.mpls_vpn.label_2 = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.mpls_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
        
        extra_param_pos += 3;

        break;


#ifdef PPP_MAC 
    case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID :
		  if ( argc < 4) {
              return MEA_ERROR;
	 	  }
		  MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac_plus_pppoe_session_id.MAC);
          entry.key.mac_plus_pppoe_session_id.PPPoE_session_id = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
		  extra_param_pos += 2;
		  break;
#else
	case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:

		entry.key.PPPOE_plus_vpn.PPPoE_session_id = MEA_OS_atoiNum(argv[extra_param_pos ]);
		entry.key.PPPOE_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos+1]);

		extra_param_pos += 2;
		break;

		 
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :
		  if ( argc < 4) {
              return MEA_ERROR;
	 	  }
          entry.key.vlan_vpn.VLAN = MEA_OS_atoiNum(argv[extra_param_pos]);
          entry.key.vlan_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
          extra_param_pos += 2;
		  break;
    case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
		  if ( argc < 7) {
              return MEA_ERROR;
	 	  }
          entry.key.oam_vpn.label_1 = MEA_OS_atoiNum(argv[extra_param_pos]);
          entry.key.oam_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
          entry.key.oam_vpn.MD_level = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
          entry.key.oam_vpn.op_code = MEA_OS_atoiNum(argv[extra_param_pos + 3]);
          entry.key.oam_vpn.packet_is_untag_mpls = MEA_OS_atoiNum(argv[extra_param_pos + 4]);
          entry.key.oam_vpn.Src_port = MEA_OS_atoiNum(argv[extra_param_pos + 5]);
         

          extra_param_pos += 6;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
          if ( argc < 5) {
              return MEA_ERROR;
	 	  }
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4   = MEA_OS_inet_addr(argv[extra_param_pos]);
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4 = MEA_OS_inet_addr(argv[extra_param_pos+1]);
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
          extra_param_pos += 3;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
        if ( argc < 5) {
            return MEA_ERROR;
        }
        entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4   = MEA_OS_inet_addr(argv[extra_param_pos]);
        entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN    = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
        
        extra_param_pos += 3;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        if ( argc < 5) {
            return MEA_ERROR;
        }
        entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4   = MEA_OS_inet_addr(argv[extra_param_pos]);
        entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN    = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

        extra_param_pos += 3;
        break;

    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:


        s = inet_pton(AF_INET6, argv[extra_param_pos], buf);
        if (s <= 0) {
            CLI_print("ipv6 format error\n");
            return MEA_ERROR;
        }
        entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
        entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
        entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
        entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;




        entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

        extra_param_pos += 3;
        break;

    case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:


        s = inet_pton(AF_INET6, argv[extra_param_pos], buf);
        if (s <= 0) {
            CLI_print("ipv6 format error\n");
            return MEA_ERROR;
        }
        entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
        entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
        entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
        entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;




        entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN    = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

        extra_param_pos += 3;
        break;




    
    case MEA_SE_FORWARDING_KEY_TYPE_LAST :
    default: 
		   CLI_print("ERROR: Invalid key type (%d)\n ",
			   entry.key.type);
		   return MEA_OK;
    }

    for (i = extra_param_pos; i < (MEA_Uint32)argc; i++){
        if (!strcmp(argv[i], "-mask_type")){
            if (i + 1 >= (MEA_Uint32)argc) return MEA_ERROR;

            switch (entry.key.type)
            {
            case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN:
                break;

            case     MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
                entry.key.mpls_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
#ifdef PPP_MAC
            case     MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
                break;
#else
			case     MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
				break;
#endif
            case     MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN:
                break;
            case     MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
                entry.key.oam_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
                entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
                entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;

            default:
                break;
            }
        }
    }



   if ( MEA_API_Get_SE_Entry (MEA_UNIT_0,
	                          &entry) != MEA_OK)  {
         CLI_print("Error while getting SE entry\n");
         return MEA_OK;       
    }



 
    CLI_print("Key  Key                            \n"
              "Type                                \n"
              "---- ------------------------------------------------------------------------------\n");
 
    mea_frwarder_print_key(&entry.key,MEA_TRUE);
    CLI_print("    Data: ");
    CLI_print("%3s Age%d saD=%3s ", 
        ((entry.data.static_flag == MEA_TRUE) ? "Static " : "dynamic"),
                 entry.data.aging ,
				 MEA_STATUS_STR( entry.data.sa_discard));

	if (entry.data.actionId_valid){
        CLI_print("ActId=%6d ", entry.data.actionId);
	} else {
		CLI_print("ActId=%6s ", "D.C");
	}

	if (entry.data.limiterId_valid){
		CLI_print("LimId=%4d ", entry.data.limiterId);
	} else {
		CLI_print("LimId=%4s ", "D.C");
	}
    
    CLI_print("     Out: ");
	 for (port = 0;port <ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++) {
       if (((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port/32] & (1 << (port%32))) {
			   CLI_print("%3d ",port);
		}
	 }

     CLI_print("\n");
     return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_ForwarderShow>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_ForwarderShow(int argc, char *argv[]) 
{
   MEA_SE_Entry_dbt        *print_table;
   MEA_SE_Entry_dbt        *print_table_entry;
   MEA_Uint32               print_table_count=0;
   MEA_Uint32               print_table_max_count=MEA_FWD_DEFAULT_NUM_OF_FLOWS;  
   MEA_Uint32               print_table_size=0;
   MEA_Bool                 found;
   MEA_Uint32               index;
   MEA_Port_t               port;
   MEA_Globals_Entry_dbt    globals_entry;
   MEA_Uint32               seconds;
   MEA_Bool                 status;
//    MEA_Uint32               srcIp;
//    MEA_Uint32               dstIp;
//    char                     srcIp_str[20];
//    char                     dstIp_str[20];
   MEA_Uint32               i;
   MEA_SE_Forwarding_key_type_te key=0;
	   
   MEA_Bool                 checkByKey=MEA_FALSE;
   MEA_Bool                 bcalc=MEA_FALSE;
   MEA_Uint32               countFWD;
   MEA_SE_Forwarding_key_type_te key_type=0;
   MEA_Bool     filterEnable= MEA_FALSE;
   MEA_Bool     filterId=0;
   MEA_Bool data_newLine = MEA_FALSE;

   char mask_Ip_str[4][10];

   if ( argc < 2 )
        return MEA_ERROR;

  
       MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_32][0]), "/32");
       MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_24][0]), "/24");
       MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_16][0]), "/16");
       MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_8][0]), "/8");

   if(MEA_API_Get_SE_Aging(0,&seconds,&status) != MEA_OK){
      CLI_print("Error getting SE aging\n");
   } else {
      CLI_print("\nAging: %s (%d seconds)\n\n", 
                (status == MEA_TRUE) ? "Enable " : "Disable" , seconds);
    }
    
   if (MEA_OS_strcmp(argv[1],"all") != 0) {
      return MEA_CLI_ForwarderSearch(argc,argv);
   } 

   if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&globals_entry) != MEA_OK) {
      CLI_print("Error while get API_Get_Globals_Entry\n");
      return MEA_OK;
   }
  
  /*  this options is after option all */
    for(i=2;i<(MEA_Uint32)argc;i++){
        if (!MEA_OS_strcmp(argv[i], "-data_newLine")){
            data_newLine = MEA_TRUE;
        }
        if(!MEA_OS_strcmp(argv[i],"-key")){
            if(i+1 > (MEA_Uint32)argc){
                CLI_print(
                "Missing parameters after %s\n",argv[i]);
                return MEA_OK; 
            }
            key = MEA_OS_atoiNum(argv[i + 1]);
            checkByKey=MEA_TRUE;
        } 
        if(!MEA_OS_strcmp(argv[i],"-count")){
            if(i+1 > (MEA_Uint32)argc){
                CLI_print(
                "Missing parameters after %s\n",argv[i]);
                return MEA_OK; 
            }
            print_table_max_count = MEA_OS_atoiNum(argv[i + 1]);
        }
         if(!MEA_OS_strcmp(argv[i],"-calc")){
           if(i+1 > (MEA_Uint32)argc){
                CLI_print(
                "Missing parameters after %s\n",argv[i]);
                return MEA_OK; 
            }
           bcalc = MEA_OS_atoiNum(argv[i + 1]);
        }
         if(!MEA_OS_strcmp(argv[i],"-filter")){
             if(i+2 > (MEA_Uint32)argc){
                 CLI_print(
                     "Missing parameters after %s\n",argv[i]);
                 return MEA_OK; 
             }
             filterEnable = MEA_OS_atoiNum(argv[i + 1]);
             filterId = MEA_OS_atoiNum(argv[i + 2]);
         }
 



   }
   print_table_size = (MEA_FWD_TBL_HASH_TYPE_LAST * ((1 << (globals_entry.verInfo.val.forwarder_Addr_Width))+1))+1 ;
   if (print_table_max_count>print_table_size) {
	   print_table_max_count = print_table_size;
	   CLI_print("requested table was reduced to the maximum available entries %d (%d bytes)\n",
		         print_table_max_count, 
				 sizeof(MEA_SE_Entry_dbt)*print_table_max_count);
   }
   print_table = (MEA_SE_Entry_dbt*)
                 MEA_OS_malloc(sizeof(MEA_SE_Entry_dbt)*(print_table_max_count+1));
   if (print_table == NULL) {
      CLI_print("Alloc of print_table failed (%d)\n", 
		         sizeof(MEA_SE_Entry_dbt)*print_table_max_count);
      return MEA_OK;
   }
   MEA_OS_memset(print_table,0,sizeof(*print_table));

   print_table_count = 0;
   print_table_entry = print_table;
 ENET_FWD_Lock();  
countFWD=0;
    if(bcalc==MEA_TRUE){
        print_table_entry->filterEnable = filterEnable;
        print_table_entry->filterId= filterId;

        if (MEA_API_GetFirst_SE_Entry(MEA_UNIT_0,
	                                 print_table_entry,
                                     &found) != MEA_OK) {
	      CLI_print(
		                    "%s - MEA_API_GetFirst_SE_Entry failed \n",
						    __FUNCTION__);
           MEA_OS_free(print_table);
		   ENET_FWD_Unlock();
	      return MEA_ERROR;
       }
        while (found == MEA_TRUE){
             if(checkByKey == MEA_TRUE){
                 if(key == print_table_entry->key.type){
                     countFWD++;
                 }
             }else{
             countFWD++;
             }
             if (MEA_API_GetNext_SE_Entry(MEA_UNIT_0,
	                                  print_table_entry,
	                                  &found) != MEA_OK) {
	         CLI_print(
		                       "%s - MEA_API_GetNext_SE_Entry failed \n",
					      	   __FUNCTION__);
             MEA_OS_free(print_table);
			 ENET_FWD_Unlock();
	         return MEA_ERROR;
            }
         }
	
	}
    if(bcalc==MEA_TRUE){
       CLI_print ("Number of entries %ld\n",countFWD);
       MEA_OS_free(print_table);
	   ENET_FWD_Unlock();		 
       return MEA_OK;
    }

//--------------






    print_table_entry->filterEnable = filterEnable;
    print_table_entry->filterId= filterId;

   if (MEA_API_GetFirst_SE_Entry(MEA_UNIT_0,
	                             print_table_entry,
                                 &found) != MEA_OK) {
	  CLI_print(
		                "%s - MEA_API_GetFirst_SE_Entry failed \n",
						__FUNCTION__);
       MEA_OS_free(print_table);
	   ENET_FWD_Unlock();
	  return MEA_ERROR;
   }
   



   while ((found == MEA_TRUE)&&(print_table_count<print_table_max_count)) {


         print_table_entry++;
         print_table_count++;

         
         
         if (MEA_API_GetNext_SE_Entry(MEA_UNIT_0,
	                                  print_table_entry,
	                                  &found) != MEA_OK) {
	         CLI_print(
		                       "%s - MEA_API_GetNext_SE_Entry failed \n",
					      	   __FUNCTION__);
             MEA_OS_free(print_table);
			 ENET_FWD_Unlock();
	         return MEA_ERROR;
         }
         if(found){
             key_type = print_table_entry->key.type;
             if((checkByKey == MEA_TRUE) && (key != key_type)){
                 print_table_entry--;
                 print_table_count--;
             }
         }
   
   }
    CLI_print("\n");
ENET_FWD_Unlock();
    if (print_table_count == 0) {
       CLI_print ("Forwarder Table is empty \n");
       MEA_OS_free(print_table);
       return MEA_OK;
    }
	
    
    
	
	if (print_table_count<print_table_max_count)
		CLI_print ("\nNumber of entries is %d \n\n",print_table_count);
	else
		CLI_print ("\nReached max num of entries (%d) \n\n",print_table_count);

   
    
    CLI_print("Index Key  Key                           \n"
              "      Type                               \n"
              "----- ---- --------------------------------------------------------------------------\n");

    for (print_table_entry = print_table, index = 1;
        print_table_count-- > 0;
        print_table_entry++) {
        CLI_print("----- ---- --------------------------------------------------------------------------\n");



        key_type = print_table_entry->key.type;
        if ((checkByKey == MEA_TRUE) && (key != key_type)){
            continue;
        }


        CLI_print("%5d ", index);
        index++;

        mea_frwarder_print_key(&print_table_entry->key, data_newLine);
        if (data_newLine){
            CLI_print("           Data: ");
        }
        else {
        CLI_print("Data: ");
        }
        
      CLI_print ("%3s Age%d saD=%3s ",
		         ((print_table_entry->data.static_flag == MEA_TRUE) ? "Static " : "dynamic" ),
				 print_table_entry->data.aging,
				 MEA_STATUS_STR(print_table_entry->data.sa_discard));

				 
       if (print_table_entry->data.actionId_valid){
	        CLI_print("ActId=%6d ", print_table_entry->data.actionId);
       } else {
           if(print_table_entry->data.actionId_err)
               CLI_print("ActIdErr=%6d ", print_table_entry->data.actionId);
           else
	    	CLI_print("ActId=%-6s ", "D.C");
	   }

	   if (print_table_entry->data.limiterId_valid){
		    CLI_print("LimId=%4d ", print_table_entry->data.limiterId);
	   } else {
		    CLI_print("LimId=%-4s ", "D.C");
	   }
#if 0
       if (print_table_entry->data.cfm_oam_me_level_valid){
           CLI_print("ml=%1d ", print_table_entry->data.cfm_oam_me_level_value);
	   } else {
		    CLI_print("ml=%-1s ", "N");
	   }
#endif

       CLI_print("A_P=%04d ", print_table_entry->data.access_port);
     
       if(!print_table_entry->data.xPermissionId_err)
            CLI_print("xPerId=%4d ",print_table_entry->data.xPermissionId);
       else {
           CLI_print("xPerIdErr=%4d ", print_table_entry->data.xPermissionId);
       }
       if (data_newLine){
           CLI_print("\n           Out: ");
       }
       else{
           CLI_print("Out: ");
       }
       /*check if only */
	   for (port = 0;port <ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++)
	   {
		   if (((ENET_Uint32*)(&(print_table_entry->data.OutPorts.out_ports_0_31)))[port/32] 
		        & (1 << (port%32))) {
			   CLI_print("%3d ",port);
		   }
	   }
   
	   CLI_print("\n");

    }


    MEA_OS_free(print_table);

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_ForwarderAdd>                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_ForwarderAdd(int argc, char *argv[])
{

   MEA_SE_Entry_dbt               entry;
   MEA_Uint32                     num_of_ports;
   MEA_Uint32 i;
   MEA_Port_t                     port;
   MEA_Uint32                     extra_param_pos;
   unsigned char buf[sizeof(struct in6_addr)];
   int  s;

   
   
//   MEA_Uint32 vlan, vpn;
	
	if ( argc < 2 ) {
        return MEA_ERROR;
	}

    for (i=1; i < (MEA_Uint32)argc; i++) {
         
        if(  (argv[i][0] == '-'        )   && (
           

             (MEA_OS_strcmp(argv[i],"-limiter")          != 0) &&
//            (MEA_OS_strcmp(argv[i],"-cfm_oam_me_level") != 0) &&
            (MEA_OS_strcmp(argv[i], "-access_port") != 0) &&
            (MEA_OS_strcmp(argv[i], "-mask_type") != 0) &&

            (MEA_OS_strcmp(argv[i],"-action")         != 0))){
                CLI_print("ERROR: Not support option %s \n",argv[i]);
            return MEA_OK;
        }
    }
    
    MEA_OS_memset(&entry, 0, sizeof(entry));

    entry.key.type = MEA_OS_atoiNum(argv[1]);
    extra_param_pos = 2;
	switch (entry.key.type) {
    case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
		  if ( argc < 7) {
              return MEA_ERROR;
	 	  }
          entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		  MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac_plus_vpn.MAC);
          entry.key.mac_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
          extra_param_pos += 2;
		  break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
    case MEA_SE_FORWARDING_KEY_TYPE_DA :
		  if ( argc < 6) {
              return MEA_ERROR;
	 	  }
          entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA;
		  MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac.MAC);
          extra_param_pos += 1;
		  break;
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :
        if ( argc < 8) {
            return MEA_ERROR;
        }
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN;
        entry.key.mpls_vpn.label_1    = MEA_OS_atoiNum(argv[extra_param_pos]);
        entry.key.mpls_vpn.label_2 = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.mpls_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
        
        extra_param_pos += 3;
        break;
#ifdef PPP_MAC 
    case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID :
		  if ( argc < 7) {
              return MEA_ERROR;
	 	  }
          entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID;
		  MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac_plus_pppoe_session_id.MAC);
          entry.key.mac_plus_pppoe_session_id.PPPoE_session_id = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
		  extra_param_pos += 2;
		  break;
#else
	case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN :
		if (argc < 7) {
			return MEA_ERROR;
		}
		entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN;
		entry.key.PPPOE_plus_vpn.PPPoE_session_id = MEA_OS_atoiNum(argv[extra_param_pos]);
		entry.key.PPPOE_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos+1]);

		extra_param_pos += 2;
		break;
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :
		  if ( argc < 7) {
              return MEA_ERROR;
	 	  }
          entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN;
          entry.key.vlan_vpn.VLAN = MEA_OS_atoiNum(argv[extra_param_pos]);
          entry.key.vlan_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
//		  vlan = entry.key.vlan_vpn.VLAN;
//		  vpn = entry.key.vlan_vpn.VPN;
          extra_param_pos += 2;
		  break;
    case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :
         if ( argc < 10) {
              return MEA_ERROR;
	 	  }
         entry.key.oam_vpn.label_1 = MEA_OS_atoiNum(argv[extra_param_pos]);
         entry.key.oam_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
         entry.key.oam_vpn.MD_level = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
         entry.key.oam_vpn.op_code = MEA_OS_atoiNum(argv[extra_param_pos + 3]);
         entry.key.oam_vpn.packet_is_untag_mpls = MEA_OS_atoiNum(argv[extra_param_pos + 4]);
         entry.key.oam_vpn.Src_port = MEA_OS_atoiNum(argv[extra_param_pos + 5]);
         
         entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
         extra_param_pos += 6;
       break;
        case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
          if ( argc < 5) {
              return MEA_ERROR;
	 	  }
          entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 = MEA_OS_inet_addr(argv[extra_param_pos]);
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4  = MEA_OS_inet_addr(argv[extra_param_pos+1]);
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN      = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
          extra_param_pos += 3;
        break;
        case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
            if ( argc < 5) {
                return MEA_ERROR;
            }
            entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
            entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[extra_param_pos]);
            entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
            entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN    = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
            
            extra_param_pos += 3;
            break;

        case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
            if ( argc < 5) {
                return MEA_ERROR;
            }
            entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
            entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[extra_param_pos]);
            entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
            entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN    = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

            extra_param_pos += 3;
            break;
        case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
        {

            s = inet_pton(AF_INET6, argv[extra_param_pos], buf);
            if (s <= 0) {
                CLI_print("ipv6 format error\n");
                return MEA_ERROR;
            }
            entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
            entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
            entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
            entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

            entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;




            entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
            entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

            extra_param_pos += 3;
        }
            
            break;

        case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        {

            s = inet_pton(AF_INET6, argv[extra_param_pos], buf);
            if (s <= 0) {
                CLI_print("ipv6 format error\n");
                return MEA_ERROR;
            }
            entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
            entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
            entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
            entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

            entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;




            entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
            entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN    = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

            extra_param_pos += 3;
        }

            break;



	case MEA_SE_FORWARDING_KEY_TYPE_LAST :
    default: 
		   CLI_print("ERROR: Invalid key type (%d)\n ",
			   entry.key.type);
		   return MEA_OK;
    }
	entry.data.aging = (MEA_Uint32)MEA_OS_atoi(argv[extra_param_pos]);
    entry.data.static_flag = (MEA_Uint32)MEA_OS_atoiNum(argv[extra_param_pos + 1]);
    entry.data.sa_discard = (MEA_Uint32)MEA_OS_atoiNum(argv[extra_param_pos + 2]);
	entry.data.limiterId_valid = MEA_FALSE;
	entry.data.limiterId       = 0;
	entry.data.actionId_valid  = MEA_FALSE;
	entry.data.actionId        = 0;

    num_of_ports = MEA_OS_atoiNum(argv[extra_param_pos + 3]);

    if ( num_of_ports + (extra_param_pos+4) > (MEA_Uint32) argc   )
        return MEA_ERROR;

    for ( i = 0; i < num_of_ports; i ++ ) {
        port = (MEA_Port_t)MEA_OS_atoiNum(argv[extra_param_pos + 4 + i]);

		if (ENET_IsValid_Queue(ENET_UNIT_0,port,ENET_TRUE) != ENET_TRUE) {
		   CLI_print("Cluster id %d is not define \n",port);
		   return MEA_ERROR;
	   }

	   /* check if the out_port is valid */
		((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port/32] |=  
				(1 << (port%32));

	}
    
    extra_param_pos = extra_param_pos+ 4 + i; 

    for (i=extra_param_pos; i < (MEA_Uint32) argc ;i++){

        if (!strcmp(argv[i], "-mask_type")){
            if (i + 1 >= (MEA_Uint32)argc) return MEA_ERROR;
           
            switch (entry.key.type)
            {
            case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN:
                break;

            case     MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
                entry.key.mpls_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
#ifdef PPP_MAC 
            case     MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
                break;
#else
			case     MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
				break;
#endif

            case     MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN:
                break;
            case     MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
                entry.key.oam_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
                entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
                entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;

            default:
                break;
            }
            
           
       
    }

		if(!strcmp(argv[i],"-limiter")){
			if (i+2>=(MEA_Uint32)argc) return MEA_ERROR;
            entry.data.limiterId_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
            entry.data.limiterId = (MEA_Limiter_t)MEA_OS_atoiNum(argv[i + 2]);
        }
		if(!strcmp(argv[i],"-action")){
			if (i+2>=(MEA_Uint32)argc) return MEA_ERROR;
            entry.data.actionId_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
            entry.data.actionId = (MEA_Action_t)MEA_OS_atoiNum(argv[i + 2]);
        }
#if 0
        if(!strcmp(argv[i],"-cfm_oam_me_level")){
			if (i+2>=(MEA_Uint32)argc) return MEA_ERROR;
            entry.data.cfm_oam_me_level_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
            entry.data.cfm_oam_me_level_value = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 2]);
        }
#else
        if (!strcmp(argv[i], "-access_port")){
            if (i + 1 >= (MEA_Uint32)argc) return MEA_ERROR;
            entry.data.access_port = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
          
        }

#endif

    } 


 //  CLI_print(" before MEA_API_Create_SE  Key type %d \n",entry.key.mac.type); 
   if (MEA_API_Create_SE_Entry(MEA_UNIT_0,
 	                           &entry) != MEA_OK) {
        CLI_print("Error while create to  SE entry\n");
        return MEA_OK;
    }

    return MEA_OK;
}




static MEA_Status MEA_CLI_ForwarderAdd_router_IP(int argc, char *argv[])
{

    MEA_SE_Entry_dbt               entry;
    MEA_Uint32                     num_of_ports;
    MEA_Uint32 i;
    MEA_Port_t                     port;
    MEA_Uint32                     extra_param_pos;
    MEA_Uint32                     Loop = 1;
    MEA_Bool                       CreateAction = MEA_FALSE;
    MEA_MacAddr                    Da_Mac;
    MEA_Uint32                     EditId=0;

    MEA_Action_Entry_Data_dbt             Action_Data;
    MEA_OutPorts_Entry_dbt                Action_OutPorts;
    //MEA_Policer_Entry_dbt                 Action_Policer;

    MEA_EgressHeaderProc_Array_Entry_dbt    ehp_array;

    MEA_EHP_Info_dbt                        ehp_info;


    MEA_Action_t                          Action_Id;

	MEA_Uint32                            count_collision = 0;
    
    MEA_OS_memset(&Action_Data, 0, sizeof(Action_Data));
    MEA_OS_memset(&ehp_array, 0, sizeof(ehp_array));
    MEA_OS_memset(&ehp_info, 0, sizeof(ehp_info));
    MEA_OS_memset(&Action_OutPorts, 0, sizeof(Action_OutPorts));

    

    ehp_array.num_of_entries = 1; //NumOfMulticastEditor;
    ehp_array.ehp_info = MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt) * 1);
    if (ehp_array.ehp_info == NULL) {

        MEA_OS_free(ehp_array.ehp_info);
        return MEA_OK;
    }
    MEA_OS_memset(ehp_array.ehp_info, 0, sizeof(MEA_EHP_Info_dbt) * 1);

    if (argc < 2) {
        return MEA_ERROR;
    }

    for (i = 1; i < (MEA_Uint32)argc; i++) {

        if ((argv[i][0] == '-') && (


            (MEA_OS_strcmp(argv[i], "-limiter") != 0) &&
            //            (MEA_OS_strcmp(argv[i],"-cfm_oam_me_level") != 0) &&
            (MEA_OS_strcmp(argv[i], "-access_port") != 0) &&
            (MEA_OS_strcmp(argv[i], "-loop") != 0) &&
            (MEA_OS_strcmp(argv[i], "-nextHopMac") != 0) &&
            (MEA_OS_strcmp(argv[i], "-CreateAction") != 0) &&

            (MEA_OS_strcmp(argv[i], "-mask_type") != 0) &&

            (MEA_OS_strcmp(argv[i], "-action") != 0))) {
            CLI_print("ERROR: Not support option %s \n", argv[i]);
            return MEA_OK;
        }
    }

    MEA_OS_memset(&entry, 0, sizeof(entry));

    entry.key.type = MEA_OS_atoiNum(argv[1]);
    extra_param_pos = 2;
    switch (entry.key.type) {
    case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN:
        if (argc < 7) {
            return MEA_ERROR;
        }
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        MEA_OS_atohex_MAC(argv[extra_param_pos], &entry.key.mac_plus_vpn.MAC);
        entry.key.mac_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        extra_param_pos += 2;
        break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
    case MEA_SE_FORWARDING_KEY_TYPE_DA:
        if (argc < 6) {
            return MEA_ERROR;
        }
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA;
        MEA_OS_atohex_MAC(argv[extra_param_pos], &entry.key.mac.MAC);
        extra_param_pos += 1;
        break;
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
        if (argc < 8) {
            return MEA_ERROR;
        }
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN;
        entry.key.mpls_vpn.label_1 = MEA_OS_atoiNum(argv[extra_param_pos]);
        entry.key.mpls_vpn.label_2 = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.mpls_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

        extra_param_pos += 3;
        break;
#ifdef PPP_MAC 
    case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
        if (argc < 7) {
            return MEA_ERROR;
        }
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID;
        MEA_OS_atohex_MAC(argv[extra_param_pos], &entry.key.mac_plus_pppoe_session_id.MAC);
        entry.key.mac_plus_pppoe_session_id.PPPoE_session_id = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        extra_param_pos += 2;
        break;
#else


	case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
		if (argc < 7) {
			return MEA_ERROR;
		}
		entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN;
		
		entry.key.PPPOE_plus_vpn.PPPoE_session_id = MEA_OS_atoiNum(argv[extra_param_pos ]);
		entry.key.PPPOE_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
		extra_param_pos += 2;
		break;
#endif




    case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN:
        if (argc < 7) {
            return MEA_ERROR;
        }
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN;
        entry.key.vlan_vpn.VLAN = MEA_OS_atoiNum(argv[extra_param_pos]);
        entry.key.vlan_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        //		  vlan = entry.key.vlan_vpn.VLAN;
        //		  vpn = entry.key.vlan_vpn.VPN;
        extra_param_pos += 2;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
        if (argc < 10) {
            return MEA_ERROR;
        }
        entry.key.oam_vpn.label_1 = MEA_OS_atoiNum(argv[extra_param_pos]);
        entry.key.oam_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.oam_vpn.MD_level = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
        entry.key.oam_vpn.op_code = MEA_OS_atoiNum(argv[extra_param_pos + 3]);
        entry.key.oam_vpn.packet_is_untag_mpls = MEA_OS_atoiNum(argv[extra_param_pos + 4]);
        entry.key.oam_vpn.Src_port = MEA_OS_atoiNum(argv[extra_param_pos + 5]);

        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
        extra_param_pos += 6;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
        if (argc < 5) {
            return MEA_ERROR;
        }
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
        entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4 = MEA_OS_inet_addr(argv[extra_param_pos]);
        entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4 = MEA_OS_inet_addr(argv[extra_param_pos + 1]);
        entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
        extra_param_pos += 3;
        break;
    case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
        if (argc < 5) {
            return MEA_ERROR;
        }
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
        entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4 = MEA_OS_inet_addr(argv[extra_param_pos]);
        entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

        extra_param_pos += 3;
        break;

    case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
        if (argc < 5) {
            return MEA_ERROR;
        }
        entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;
        entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4 = MEA_OS_inet_addr(argv[extra_param_pos]);
        entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
        entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

        extra_param_pos += 3;
        break;




    case MEA_SE_FORWARDING_KEY_TYPE_LAST:
    default:
        CLI_print("ERROR: Invalid key type (%d)\n ",
            entry.key.type);
        return MEA_OK;
    }
    entry.data.aging = (MEA_Uint32)MEA_OS_atoi(argv[extra_param_pos]);
    entry.data.static_flag = (MEA_Uint32)MEA_OS_atoiNum(argv[extra_param_pos + 1]);
    entry.data.sa_discard = (MEA_Uint32)MEA_OS_atoiNum(argv[extra_param_pos + 2]);
    entry.data.limiterId_valid = MEA_FALSE;
    entry.data.limiterId = 0;
    entry.data.actionId_valid = MEA_FALSE;
    entry.data.actionId = 0;

    num_of_ports = MEA_OS_atoiNum(argv[extra_param_pos + 3]);

    if (num_of_ports + (extra_param_pos + 4) > (MEA_Uint32)argc)
        return MEA_ERROR;

    for (i = 0; i < num_of_ports; i++) {
        port = (MEA_Port_t)MEA_OS_atoiNum(argv[extra_param_pos + 4 + i]);

        if (ENET_IsValid_Queue(ENET_UNIT_0, port, ENET_TRUE) != ENET_TRUE) {
            CLI_print("Cluster id %d is not define \n", port);
            return MEA_ERROR;
        }

        /* check if the out_port is valid */
        ((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port / 32] |=
            (1 << (port % 32));

    }

    extra_param_pos = extra_param_pos + 4 + i;

    for (i = extra_param_pos; i < (MEA_Uint32)argc; i++) {

        if (!strcmp(argv[i], "-mask_type")) {
            if (i + 1 >= (MEA_Uint32)argc) return MEA_ERROR;

            switch (entry.key.type)
            {
            case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN:
                break;

            case     MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
                entry.key.mpls_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
#ifdef PPP_MAC 
            case     MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
                break;
#else
			case     MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
				break;
#endif
            case     MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN:
                break;
            case     MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
                entry.key.oam_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
                entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
                entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;

            default:
                break;
            }



        }

        if (!strcmp(argv[i], "-limiter")) {
            if (i + 2 >= (MEA_Uint32)argc) return MEA_ERROR;
            entry.data.limiterId_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
            entry.data.limiterId = (MEA_Limiter_t)MEA_OS_atoiNum(argv[i + 2]);
        }
        if (!strcmp(argv[i], "-action")) {
            if (i + 2 >= (MEA_Uint32)argc) return MEA_ERROR;
            entry.data.actionId_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
            entry.data.actionId = (MEA_Action_t)MEA_OS_atoiNum(argv[i + 2]);
        }

        if (!strcmp(argv[i], "-access_port")) {
            if (i + 1 >= (MEA_Uint32)argc) return MEA_ERROR;
            entry.data.access_port = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);

        }
        if (!strcmp(argv[i], "-loop")) {
            Loop = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
        }
        if (!strcmp(argv[i], "-nextHopMac")) {
            MEA_OS_atohex_MAC(argv[i + 1], &Da_Mac);

        }
        if (!strcmp(argv[i], "-CreateAction")) {
            CreateAction = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
        }




    }

    for (i = 0; i < Loop; i++)
    {

        if (CreateAction) {


            Action_Id = MEA_PLAT_GENERATE_NEW_ID;
#if 1
            Action_Data.pm_type = MEA_PM_TYPE_ALL;
            Action_Data.pm_id_valid = MEA_TRUE;
            Action_Data.pm_id = 0;//(MEA_MAX_NUM_OF_PM_ID-1);
            Action_Data.ed_id_valid = MEA_TRUE;
            Action_Data.ed_id = EditId;
            Action_Data.tm_id_valid = MEA_FALSE;

            Action_Data.policer_prof_id_valid = MEA_FALSE;
            Action_Data.policer_prof_id = 0;
            Action_Data.no_allow_del_Act = MEA_FALSE;
#endif
            Action_Data.Action_type = MEA_ACTION_TYPE_FWD; /* Internal */
            MEA_OS_memcpy(&ehp_array.ehp_info[0].ehp_data.martini_info.DA.b[0], &Da_Mac.b[0], sizeof(ehp_array.ehp_info[0].ehp_data.martini_info.DA));
            ehp_array.ehp_info[0].ehp_data.LmCounterId_info.valid = MEA_TRUE;
            ehp_array.ehp_info[0].ehp_data.LmCounterId_info.Command_dasa = MEA_TRUE;

            if (MEA_API_Create_Action(MEA_UNIT_0,
                &Action_Data,
                &Action_OutPorts,
                NULL,//&Action_Policer,
                &ehp_array,
                &Action_Id) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_Create_Action failed (id=%d) \n",
                    __FUNCTION__,
                    Action_Id);
                MEA_OS_free(ehp_array.ehp_info);
                return MEA_ERROR;
            }

            if(MEA_API_Get_Action(MEA_UNIT_0,
                Action_Id,
                NULL,
                NULL,
                NULL,
                &ehp_array) != MEA_OK) {
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s \n- MEA_API_Create_Action failed (id=%d) \n",
                    __FUNCTION__,
                    Action_Id);
                MEA_OS_free(ehp_array.ehp_info);
                return MEA_ERROR;
            }



            EditId = Action_Data.ed_id;
            entry.data.actionId_valid = MEA_TRUE;
            entry.data.actionId = Action_Id;

        }

       

        //  CLI_print(" before MEA_API_Create_SE  Key type %d \n",entry.key.mac.type); 
        if (MEA_API_Create_SE_Entry(MEA_UNIT_0,
            &entry) != MEA_OK) {
            //CLI_print("Error while create to  SE entry\n");
            //MEA_OS_free(ehp_array.ehp_info);
            //return MEA_OK;
			count_collision++;
        }
        if (entry.key.type == MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN)
        {   
            
            entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4++ ;
            
        }
    }
    MEA_OS_free(ehp_array.ehp_info);
	CLI_print(" Loop %d \n", Loop);
	CLI_print(" collision %d \n", count_collision);
	CLI_print(" Total fwd %d \n", Loop-count_collision);

    return MEA_OK;
}


#ifdef NAT_ADAPTOR_WANTED
static MEA_Status MEA_CLI_Forwarder_NatStatus(int argc, char *argv[])
{
    AdapSetNATRouterStatus (MEA_OS_atoiNum(argv[1]));
    return MEA_OK;	
}

static MEA_Status MEA_CLI_Forwarder_StaticNatEntry(int argc, char *argv[])
{
   MEA_SE_Entry_dbt     entry;

   if ( argc < 3 )
   {
       return MEA_ERROR;
   }

   if (AdapGetNATRouterStatus () != MEA_TRUE)
   {
       CLI_print ("NAT Status is disabled in the Adaptor\n");
       return MEA_OK;
   }

   MEA_OS_memset(&entry, 0, sizeof(entry));
   entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[1]);
   entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port = 0;
   entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[2]);
   entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port = 0;

   if (MeaAdapAddStaticNatEntry (&entry) != MEA_OK) {
        CLI_print("Error while update to Static NAT Entry Table\n");
        return MEA_ERROR;
   }

   return MEA_OK;	
}

static MEA_Status MEA_CLI_Forwarder_StaticPatEntry(int argc, char *argv[])
{
   MEA_SE_Entry_dbt     entry;

   if ( argc < 5 )
   {
       return MEA_ERROR;
   }

   if (AdapGetNATRouterStatus () != MEA_TRUE)
   {
       CLI_print ("NAT Status is disabled in the Adaptor\n");
       return MEA_OK;
   }

   MEA_OS_memset(&entry, 0, sizeof(entry));
   entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[1]);
   entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[2]);
   entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[3]);
   entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[4]);

   if (MeaAdapAddStaticNatEntry (&entry) != MEA_OK) {
        CLI_print("Error while update to Static NAT Entry Table\n");
        return MEA_ERROR;
   }

   return MEA_OK;
}

#endif //#ifdef NAT_ADAPTOR_WANTED





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_ForwarderUpdate>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_ForwarderUpdate(int argc, char *argv[])
{

   MEA_SE_Entry_dbt         entry;
   MEA_SE_Entry_dbt         oldentry;
   MEA_Uint32               num_of_ports;
   MEA_Uint32               i;
   MEA_Port_t               port;
   MEA_Uint32               extra_param_pos;
   unsigned char buf[sizeof(struct in6_addr)];
   int  s;

	if ( argc < 2 ) {
        return MEA_ERROR;
	}

    MEA_OS_memset(&entry, 0, sizeof(entry));

	entry.key.type  = MEA_OS_atoi(argv[1]);
    extra_param_pos = 2;
	switch (entry.key.type) {
    case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
		  if ( argc < 4) {
              return MEA_ERROR;
	 	  }
		  MEA_OS_atohex_MAC(argv[2],&entry.key.mac_plus_vpn.MAC);
		  entry.key.mac_plus_vpn.VPN = MEA_OS_atoi(argv[3]);
          extra_param_pos += 2;
		  break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
    case MEA_SE_FORWARDING_KEY_TYPE_DA :
		  if ( argc < 3) {
              return MEA_ERROR;
	 	  }
		  MEA_OS_atohex_MAC(argv[2],&entry.key.mac.MAC);
          extra_param_pos += 1;
		  break;
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :
        if ( argc < 5) {
            return MEA_ERROR;
        }
        entry.key.mpls_vpn.label_1 = MEA_OS_atoiNum(argv[2]);
        entry.key.mpls_vpn.label_2 = MEA_OS_atoiNum(argv[3]);
        entry.key.mpls_vpn.VPN = MEA_OS_atoiNum(argv[4]);
        
        extra_param_pos += 3;
        break;

#ifdef PPP_MAC 
    case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID :
		  if ( argc < 4) {
              return MEA_ERROR;
	 	  }
		  MEA_OS_atohex_MAC(argv[2],&entry.key.mac_plus_pppoe_session_id.MAC);
		  entry.key.mac_plus_pppoe_session_id.PPPoE_session_id = MEA_OS_atoi(argv[3]);
		  extra_param_pos += 2;
		  break;
#else
	case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
		if (argc < 4) {
			return MEA_ERROR;
	}
		
		entry.key.PPPOE_plus_vpn.PPPoE_session_id = MEA_OS_atoi(argv[2]);
		entry.key.PPPOE_plus_vpn.VPN = MEA_OS_atoi(argv[3]);
		extra_param_pos += 2;
		break;

#endif
    case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :
		  if ( argc < 4) {
              return MEA_ERROR;
	 	  }
          entry.key.vlan_vpn.VLAN = MEA_OS_atoiNum(argv[2]);
          entry.key.vlan_vpn.VPN = MEA_OS_atoiNum(argv[3]);
          extra_param_pos += 3;
		  break;
     case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :
         if ( argc < 7) {
              return MEA_ERROR;
	 	  }
         entry.key.oam_vpn.label_1 = MEA_OS_atoiNum(argv[2]);
         entry.key.oam_vpn.VPN = MEA_OS_atoiNum(argv[3]);
         entry.key.oam_vpn.MD_level = MEA_OS_atoiNum(argv[4]);
         entry.key.oam_vpn.op_code = MEA_OS_atoiNum(argv[5]);
         entry.key.oam_vpn.packet_is_untag_mpls = MEA_OS_atoiNum(argv[6]);
         entry.key.oam_vpn.Src_port = MEA_OS_atoiNum(argv[7]);
        
         extra_param_pos += 7;
       break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
          if ( argc < 5) {
              return MEA_ERROR;
	 	  }
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4  = MEA_OS_inet_addr(argv[2]);
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4  = MEA_OS_inet_addr(argv[3]);
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN = MEA_OS_atoiNum(argv[4]);
          
          extra_param_pos += 3;
        break;

       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
           if ( argc < 5) {
               return MEA_ERROR;
           }
           entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[2]);
           entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[3]);
           entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[4]);
           
           extra_param_pos += 3;
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
           if ( argc < 5) {
               return MEA_ERROR;
           }
           entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4   = MEA_OS_inet_addr(argv[2]);
           entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[3]);
           entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN    = MEA_OS_atoiNum(argv[4]);
           
           extra_param_pos += 3;
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
       {

           s = inet_pton(AF_INET6, argv[extra_param_pos], buf);
           if (s <= 0) {
               CLI_print("ipv6 format error\n");
               return MEA_ERROR;
           }
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

           entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;




           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

           extra_param_pos += 3;
       }
       case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
       {

           s = inet_pton(AF_INET6, argv[extra_param_pos], buf);
           if (s <= 0) {
               CLI_print("ipv6 format error\n");
               return MEA_ERROR;
           }
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[1] = (buf[ 8] << 24) | (buf[ 9] << 16) | (buf[10] << 8) | buf[11] << 0;
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[2] = (buf[ 4] << 24) | (buf[ 5] << 16) | (buf[ 6] << 8) | buf[ 7] << 0;
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[3] = (buf[ 0] << 24) | (buf[ 1] << 16) | (buf[ 2] << 8) | buf[ 3] << 0;

           entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;




           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

           extra_param_pos += 3;
       }
           break;



	case MEA_SE_FORWARDING_KEY_TYPE_LAST :
    default: 
		   CLI_print("ERROR: Invalid key type (%d)\n ",
			   entry.key.type);
		   return MEA_OK;
    }
    entry.data.aging = (MEA_Uint32)MEA_OS_atoiNum(argv[extra_param_pos]);
    entry.data.static_flag = (MEA_Uint32)MEA_OS_atoiNum(argv[extra_param_pos + 1]);
    entry.data.sa_discard = (MEA_Uint32)MEA_OS_atoiNum(argv[extra_param_pos + 2]);

    num_of_ports = MEA_OS_atoiNum(argv[extra_param_pos + 3]);

    if ( num_of_ports + (extra_param_pos+4) > (MEA_Uint32) argc   )
        return MEA_ERROR;

    for ( i = 0; i < num_of_ports; i ++ )
    {
        port = (MEA_Port_t) MEA_OS_atoiNum(argv[extra_param_pos + 4 + i]);

		if (ENET_IsValid_Queue(ENET_UNIT_0,port,ENET_TRUE) != ENET_TRUE) {
		   CLI_print("Cluster id %d is not define \n",port);
		   return MEA_ERROR;
	   }

	   /* check if the out_port is valid */
		((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port/32] |=  
				(1 << (port%32));

	}
    
    extra_param_pos = extra_param_pos+ 4 + i; 

    for (i = extra_param_pos; i < (MEA_Uint32)argc; i++){
        if (!strcmp(argv[i], "-mask_type")){
            if (i + 1 >= (MEA_Uint32)argc) return MEA_ERROR;

            switch (entry.key.type)
            {
            case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN:
                break;

            case     MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
                entry.key.mpls_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
#ifdef PPP_MAC 
            case     MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
                break;
#else
			case    MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
				break;
#endif
            case     MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN:
                break;
            case     MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
                entry.key.oam_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
                entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
                entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;

            default:
                break;
            }
        }


		if(!strcmp(argv[i],"-action")){
			if (i+2>=(MEA_Uint32)argc) return MEA_ERROR;
            entry.data.actionId_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
            entry.data.actionId = (MEA_Action_t)MEA_OS_atoiNum(argv[i + 2]);
        }
#if 0    
        if(!strcmp(argv[i],"-cfm_oam_me_level")){
			if (i+2>=(MEA_Uint32)argc) return MEA_ERROR;
            entry.data.cfm_oam_me_level_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
            entry.data.cfm_oam_me_level_value = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 2]);
        }
#else
        if (!strcmp(argv[i], "-access_port")){
            if (i + 1 >= (MEA_Uint32)argc) return MEA_ERROR;
            entry.data.access_port = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
           
        }

#endif
    
    
    }
	MEA_OS_memcpy(&oldentry,&entry,sizeof(oldentry));

    if ( MEA_API_Get_SE_Entry (MEA_UNIT_0, &oldentry) != MEA_OK ) {
         CLI_print("Error while getting old DSE entry\n");
         return MEA_OK;       
    }
	entry.data.limiterId_valid = oldentry.data.limiterId_valid;
    entry.data.limiterId       = oldentry.data.limiterId;

   if (MEA_API_Set_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK) {
        CLI_print("Error while update to Forwarder Table\n");
        return MEA_OK;
   }

    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_ForwarderFlushDynamic_by_port>                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_Forwarder_DeletFlushDynamic_by_port(MEA_Port_t          port)
{		

   MEA_SE_Entry_dbt    entry;
   MEA_Bool             found;


   if (MEA_API_GetFirst_SE_Entry(MEA_UNIT_0,
	                              &entry,
	                              &found) != MEA_OK) {
  	  CLI_print("Error: MEA_API_GetFirst_SE_Entry failed \n");
	  return MEA_OK;
    }

   
    while (found == MEA_TRUE) {


		/* delete mac which is dynamic and learn in portIdx */
		if ((! entry.data.static_flag) &&
			( ((MEA_Uint32*)(&(entry.data.OutPorts))) [port/32] & (1 << (port%32)))) {

			if (MEA_API_Delete_SE_Entry(MEA_UNIT_0,
	                            	    &(entry.key)) != MEA_OK) {
				 CLI_print("Error: MEA_API_Delete_SE_Entry failed\n");
		   	     return MEA_OK;
		     }
        }  
        
        if (MEA_API_GetNext_SE_Entry(MEA_UNIT_0,
	                                  &entry,
	                                  &found) != MEA_OK) {
			CLI_print("Error: MEA_API_GetNext_SE_Entry failed \n");
	        return MEA_OK;
        }
 
   
    }/*While*/
 

    
    CLI_print("Delete Done\n");
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_ForwarderDelete>                               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_ForwarderDelete(int argc, char *argv[])
{
   MEA_SE_Entry_dbt entry;
   MEA_Uint32                     extra_param_pos;
   MEA_Uint32 i;
   unsigned char buf[sizeof(struct in6_addr)];
   int  s;


   if ( argc < 2 ){
        return MEA_ERROR;
   }
    if (MEA_OS_strcmp(argv[1],"all") == 0) {
		if (argc != 2) {
			return MEA_ERROR;
		}

        if (MEA_API_DeleteAll_SE(MEA_UNIT_0) != MEA_OK)
        {
           CLI_print("Error while delete Forwarder entry\n");
           return MEA_OK;
        }

        return MEA_OK;
	}

    if (MEA_OS_strcmp(argv[1],"-p") == 0) {
         
         return MEA_CLI_Forwarder_DeletFlushDynamic_by_port((MEA_Port_t) MEA_OS_atoi(argv[2]));
    }
    MEA_OS_memset(&entry, 0, sizeof(entry));

   
	entry.key.type  = MEA_OS_atoi(argv[1]);
    extra_param_pos = 2;
	switch (entry.key.type) {
    case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
		  if ( argc < 4) {
              return MEA_ERROR;
	 	  }
		  MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac_plus_vpn.MAC);
		  entry.key.mac_plus_vpn.VPN = MEA_OS_atoi(argv[extra_param_pos+1]);
          extra_param_pos += 2;
		  break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
    case MEA_SE_FORWARDING_KEY_TYPE_DA :
		  if ( argc < 3) {
              return MEA_ERROR;
	 	  }
		  MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac.MAC);
          extra_param_pos += 1;
		  break;
#endif
    case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :
        if ( argc < 5) {
            return MEA_ERROR;
        }
        entry.key.mpls_vpn.label_1 = MEA_OS_atoi(argv[extra_param_pos+0]);
        entry.key.mpls_vpn.label_2 = MEA_OS_atoi(argv[extra_param_pos+1]);
        entry.key.mpls_vpn.VPN      = MEA_OS_atoi(argv[extra_param_pos+2]);
        extra_param_pos += 3;
        break;
#ifdef PPP_MAC 
    case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID :
		  if ( argc < 4) {
              return MEA_ERROR;
	 	  }
		  MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac_plus_pppoe_session_id.MAC);
		  entry.key.mac_plus_pppoe_session_id.PPPoE_session_id = MEA_OS_atoi(argv[extra_param_pos+1]);
		  extra_param_pos += 2;
		  break;
#else
	case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
		if (argc < 4) {
			return MEA_ERROR;
		}
		
		entry.key.PPPOE_plus_vpn.PPPoE_session_id = MEA_OS_atoi(argv[extra_param_pos   ]);
		entry.key.PPPOE_plus_vpn.VPN = MEA_OS_atoi(argv[extra_param_pos +1]);

		extra_param_pos += 2;
		break;

#endif
    case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :
		  if ( argc < 4) {
              CLI_print("Error no VPN \n");
              return MEA_ERROR;
	 	  }
		  entry.key.vlan_vpn.VLAN = MEA_OS_atoi(argv[extra_param_pos]);
		  entry.key.vlan_vpn.VPN = MEA_OS_atoi(argv[extra_param_pos+1]);
          extra_param_pos += 2;
		  break;
    case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :
         if ( argc < 6) {
              return MEA_ERROR;
	 	  }
         entry.key.oam_vpn.label_1      = MEA_OS_atoi(argv[extra_param_pos]);
         entry.key.oam_vpn.VPN       = MEA_OS_atoi(argv[extra_param_pos+1]);
         entry.key.oam_vpn.MD_level  = MEA_OS_atoi(argv[extra_param_pos+2]);
         entry.key.oam_vpn.op_code   = MEA_OS_atoi(argv[extra_param_pos+3]);
         entry.key.oam_vpn.packet_is_untag_mpls  = MEA_OS_atoi(argv[extra_param_pos+4]);
         entry.key.oam_vpn.Src_port = MEA_OS_atoi(argv[extra_param_pos + 5]);
         extra_param_pos += 6;
       break; 
       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
          if ( argc < 5) {
              return MEA_ERROR;
	 	  }
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4  = MEA_OS_inet_addr(argv[extra_param_pos]);
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4  = MEA_OS_inet_addr(argv[extra_param_pos+1]);
          entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN      = MEA_OS_atoi(argv[extra_param_pos+2]);
          extra_param_pos += 3;
        break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
           if ( argc < 5) {
               return MEA_ERROR;
           }
           entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[extra_param_pos]);
           entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port  = MEA_OS_atoi(argv[extra_param_pos+1]);
           entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN      = MEA_OS_atoi(argv[extra_param_pos+2]);
           extra_param_pos += 3;
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
           if ( argc < 5) {
               return MEA_ERROR;
           }
           entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[extra_param_pos]);
           entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port  = MEA_OS_atoi(argv[extra_param_pos+1]);
           entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN      = MEA_OS_atoi(argv[extra_param_pos+2]);
           extra_param_pos += 3;
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:


           s = inet_pton(AF_INET6, argv[extra_param_pos], buf);
           if (s <= 0) {
               CLI_print("ipv6 format error\n");
               return MEA_ERROR;
           }
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

           entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;




           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
           entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

           extra_param_pos += 3;


           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:


           s = inet_pton(AF_INET6, argv[extra_param_pos], buf);
           if (s <= 0) {
               CLI_print("ipv6 format error\n");
               return MEA_ERROR;
           }
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

           entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN;




           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
           entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

           extra_param_pos += 3;


           break;




	case MEA_SE_FORWARDING_KEY_TYPE_LAST :
    default: 
		   CLI_print("ERROR: Invalid key type (%d)\n ",
			   entry.key.type);
		   return MEA_OK;
    }

    for (i = extra_param_pos; i < (MEA_Uint32)argc; i++){
        if (!strcmp(argv[i], "-mask_type")){


            switch (entry.key.type)
            {
            case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN:
                break;

            case     MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
                entry.key.mpls_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
#ifdef PPP_MAC 
            case     MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
                break;
#else
			case     MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
				break;

#endif
            case     MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN:
                break;
            case     MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
                entry.key.oam_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
                entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
                entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
                entry.key.SE_DstIPv6_plus_L4DstPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;
            case   MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
                entry.key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                break;


            default:
                break;
            }
        }
    }

	if ( MEA_API_Delete_SE_Entry (MEA_UNIT_0,
								   &(entry.key)) != MEA_OK )
	{
        CLI_print("Error on MEA_API_Delete_SE_Entry\n");
        return MEA_OK;
    }

    return MEA_OK;

}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Forwarder_SetFilter>                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/

static MEA_Status MEA_CLI_Forwarder_SetFilter(int argc, char *argv[])
{

    MEA_SE_Entry_dbt               entry;
    //MEA_Uint32                     num_of_ports;
    MEA_Uint32 i,j,num_of_ports;
   // MEA_Port_t                     port;
    MEA_Uint32                     extra_param_pos;
    MEA_Uint32                     type=100;
    MEA_Uint16                     index=0;
    MEA_Bool                       maskKey_enable=0;
    MEA_Port_t port;   
    
    
    


 //   MEA_Uint32 vlan, vpn;

    if ( argc < 1 ) {
        return MEA_ERROR;
    }

    for (i=1; i < (MEA_Uint32)argc; i++) {

        if(  (argv[i][0] == '-'        )   && (

            (MEA_OS_strcmp(argv[i],"mask")    != 0) &&
            (MEA_OS_strcmp(argv[i],"fields")    != 0) &&
            (MEA_OS_strcmp(argv[i],"clear")    != 0) &&
            (MEA_OS_strcmp(argv[i],"promote")    != 0) &&
            (MEA_OS_strcmp(argv[i],"W_REG")    != 0) &&
            (MEA_OS_strcmp(argv[i], "-mask_type") != 0) &&
            (MEA_OS_strcmp(argv[i],"-out")    != 0) &&
            (MEA_OS_strcmp(argv[i],"-xPermissionId")    != 0) &&
            (MEA_OS_strcmp(argv[i],"-limiter")          != 0) &&
//            (MEA_OS_strcmp(argv[i],"-cfm_oam_me_level") != 0) &&
            (MEA_OS_strcmp(argv[i], "-access_port") != 0) &&

            (MEA_OS_strcmp(argv[i],"-static")    != 0) &&
            (MEA_OS_strcmp(argv[i],"-source_filter")    != 0) &&
            (MEA_OS_strcmp(argv[i],"-age")    != 0) &&
            (MEA_OS_strcmp(argv[i],"-action")         != 0))){
                CLI_print("ERROR: Not support option %s \n",argv[i]);
                return MEA_OK;
        }
    }


    
 



    MEA_OS_memset(&entry, 0, sizeof(entry));


//  index 1 (id)
//  index 2 type mask filed clear promote 
//  
    if(MEA_OS_strcmp(argv[0],"clear")== 0)
    {
      // make clear 
      MEA_OS_memset(&mea_cli_global_SE_filter,0,sizeof(mea_cli_global_SE_filter));
      CLI_print("clear filter\n");
      return MEA_OK;
    }
        
    


    if(MEA_OS_strcmp(argv[0],"mask")== 0)
    {
        if(argc < 2){
            CLI_print("error miss parameter for %s need \n",argv[0]);
            return MEA_ERROR;
        }

      type=0;
    }
    else if(MEA_OS_strcmp(argv[0],"fields")== 0)
    {
        if(argc < 2){
            CLI_print("error miss parameter for %s need \n",argv[0]);
            return MEA_ERROR;
        }

        type=1;
    }
    else if(MEA_OS_strcmp(argv[0],"W_REG")== 0)
    {
        if(argc < 8){
           CLI_print("error miss parameter for %s need \n",argv[0]);
            return MEA_ERROR;
        }

     type=2;
    }
    else if(MEA_OS_strcmp(argv[0],"promote")== 0)
    {
        type=3;
        if(argc < 2){
            CLI_print("error miss parameter for %s need \n",argv[0]);
            return MEA_ERROR;
        }
        index=MEA_OS_atoi(argv[1]);
        if(index == 0 || index >= MEA_FORWARDER_FILTER_PROFILE){
            MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                "%s - index %d not available \n",
                __FUNCTION__,index);
            return MEA_OK;
        }
    }

    if(type==100){
        CLI_print("error no parameter set \n");
        return MEA_ERROR;
    }


    if(type== 0 || type== 1){
        maskKey_enable = MEA_OS_atoi(argv[1]);
        extra_param_pos=2;
        if(maskKey_enable){
            if ( argc < 3 ) {
                return MEA_ERROR;
            }


        entry.key.type  = MEA_OS_atoiNum(argv[2]);
        extra_param_pos = 3;
        switch (entry.key.type) {
        case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
            if ( argc < 5 ) {
                return MEA_ERROR;
            }
            MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac_plus_vpn.MAC);
            entry.key.mac_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos+1]);
            extra_param_pos += 2;
            break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
        case MEA_SE_FORWARDING_KEY_TYPE_DA :
            if ( argc < 4) {
                return MEA_ERROR;
            }
            MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac.MAC);
            extra_param_pos += 1;
            break;
#endif
        case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :
            if ( argc < 6) {
                return MEA_ERROR;
            }
            entry.key.mpls_vpn.label_1 = MEA_OS_atoiNum(argv[extra_param_pos]);
            entry.key.mpls_vpn.label_2 = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
            entry.key.mpls_vpn.VPN     = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
            
            extra_param_pos += 3;
            break;
#ifdef PPP_MAC 
        case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID :
            if ( argc < 5) {
                return MEA_ERROR;
            }
            MEA_OS_atohex_MAC(argv[extra_param_pos],&entry.key.mac_plus_pppoe_session_id.MAC);
            entry.key.mac_plus_pppoe_session_id.PPPoE_session_id = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
            extra_param_pos += 2;
            break;
#else
		case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
			if (argc < 5) {
				return MEA_ERROR;
			}
			
			entry.key.PPPOE_plus_vpn.PPPoE_session_id = MEA_OS_atoiNum(argv[extra_param_pos ]);
			entry.key.PPPOE_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos]);
			extra_param_pos += 2;
			break;

#endif
        case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :
            if ( argc < 5) {
                return MEA_ERROR;
            }
            entry.key.vlan_vpn.VLAN = MEA_OS_atoiNum(argv[extra_param_pos]);
            entry.key.vlan_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
//            vlan = entry.key.vlan_vpn.VLAN;
//            vpn = entry.key.vlan_vpn.VPN;
            extra_param_pos += 2;
            break;
        case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :
            if ( argc < 9) {
                return MEA_ERROR;
            }
            entry.key.oam_vpn.label_1 = MEA_OS_atoiNum(argv[extra_param_pos]);
            entry.key.oam_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
            entry.key.oam_vpn.MD_level = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
            entry.key.oam_vpn.op_code = MEA_OS_atoiNum(argv[extra_param_pos + 3]);
            entry.key.oam_vpn.packet_is_untag_mpls = MEA_OS_atoiNum(argv[extra_param_pos + 4]);
            entry.key.oam_vpn.Src_port = MEA_OS_atoiNum(argv[extra_param_pos + 5]);
            

            extra_param_pos += 6;
            break;
        case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
            if ( argc < 6) {
                return MEA_ERROR;
            }
            entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4  = MEA_OS_inet_addr(argv[extra_param_pos]);
            entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4  = MEA_OS_inet_addr(argv[extra_param_pos+1]);
            entry.key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
            extra_param_pos += 3;
            break;
        case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
            if ( argc < 6) {
                return MEA_ERROR;
            }
            entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[extra_param_pos]);
            entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
            entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);
            
            extra_param_pos += 3;
            break;

        case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
            if ( argc < 6) {
                return MEA_ERROR;
            }
            entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4  = MEA_OS_inet_addr(argv[extra_param_pos]);
            entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port = MEA_OS_atoiNum(argv[extra_param_pos + 1]);
            entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN = MEA_OS_atoiNum(argv[extra_param_pos + 2]);

            extra_param_pos += 3;
            break;




        case MEA_SE_FORWARDING_KEY_TYPE_LAST :
        default: 
            CLI_print("ERROR: Invalid key type (%d)\n ",
                entry.key.type);
            return MEA_OK;
        }
        }
        
        entry.data.limiterId_valid = MEA_FALSE;
        entry.data.limiterId       = 0;
        entry.data.actionId_valid  = MEA_FALSE;
        entry.data.actionId        = 0;
        

        for (i=extra_param_pos; i < (MEA_Uint32) argc ;i++){

            if (!strcmp(argv[i], "-mask_type")){


                switch (entry.key.type)
                {
                case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN:
                    break;

                case     MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
                    entry.key.mpls_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                    break;
#ifdef PPP_MAC 
                case     MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
                    break;
#else
				case     MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
					break;
#endif
                case     MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN:
                    break;
                case     MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
                    entry.key.oam_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                    break;
                case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:
                    break;
                case   MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
                    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                    break;
                case   MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
                    entry.key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                    break;

                default:
                    break;
                }
            }

            if(!strcmp(argv[i],"-static")){
                if (i+1>=(MEA_Uint32)argc) return MEA_ERROR;
                entry.data.static_flag = MEA_OS_atoi(argv[i+1]);
            }
            if(!strcmp(argv[i],"-source_filter")){
                if (i+1>=(MEA_Uint32)argc) return MEA_ERROR;
                entry.data.sa_discard = MEA_OS_atoiNum(argv[i + 1]);
            }
            if(!strcmp(argv[i],"-age")){
                if (i+1>=(MEA_Uint32)argc) return MEA_ERROR;
                entry.data.aging = MEA_OS_atoiNum(argv[i + 1]);
            }


            if(!strcmp(argv[i],"-xPermissionId")){
                if (i+2>=(MEA_Uint32)argc) return MEA_ERROR;
                entry.data.xPermissionId_valid = MEA_OS_atoiNum(argv[i + 1]);
                entry.data.xPermissionId = (ENET_xPermissionId_t)MEA_OS_atoiNum(argv[i + 2]);
            }

            if(!strcmp(argv[i],"-limiter")){
                if (i+2>=(MEA_Uint32)argc) return MEA_ERROR;
                entry.data.limiterId_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                entry.data.limiterId = (MEA_Limiter_t)MEA_OS_atoiNum(argv[i + 2]);
            }
            if(!strcmp(argv[i],"-action")){
                if (i+2>=(MEA_Uint32)argc) return MEA_ERROR;
                entry.data.actionId_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
                entry.data.actionId = (MEA_Action_t)MEA_OS_atoiNum(argv[i + 2]);
            }
//             if(!strcmp(argv[i],"-cfm_oam_me_level")){
//                 if (i+2>=(MEA_Uint32)argc) return MEA_ERROR;
//                 entry.data.cfm_oam_me_level_valid = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 1]);
//                 entry.data.cfm_oam_me_level_value = (MEA_Uint32)MEA_OS_atoiNum(argv[i + 2]);
//             }
            if (!strcmp(argv[i], "-access_port")){
                if (i + 1 >= (MEA_Uint32)argc) return MEA_ERROR;
               
                entry.data.access_port = (MEA_Action_t)MEA_OS_atoiNum(argv[i + 2]);
            }

            if(!strcmp(argv[i],"-out")){
                if (i+1>=(MEA_Uint32)argc) return MEA_ERROR;
                if(type==0){
                    CLI_print("error  -out not allowed at mask \n");
                    return MEA_ERROR;
                }

                num_of_ports=(MEA_Uint32)MEA_OS_atoi(argv[i+1]);
                if(num_of_ports != 0){

                    if(i+1+num_of_ports >=(MEA_Uint32)argc){
                        CLI_print("error on -out \n");
                        return MEA_ERROR;
                    }
                    entry.data.OutPorts_valid=MEA_TRUE;

                    for ( j = 0; j < num_of_ports; j++ ) {
                        port = MEA_OS_atoi(argv[i+1+1 + j]);

                        if (ENET_IsValid_Queue(ENET_UNIT_0,port,ENET_TRUE) != ENET_TRUE) {
                            CLI_print("Cluster id %d is not define \n",port);
                            return MEA_ERROR;
                        }
                        /* check if the out_port is valid */
                        ((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port/32] |=  
                            (1 << (port%32));

                    }
                    i=i+1+num_of_ports;
               }// !=0
                
            }// -out


        }

    }



    if(type==0){  //mask 
        
        mea_cli_global_SE_filter.fwd_mask_key_enable=maskKey_enable;
        MEA_OS_memcpy( &mea_cli_global_SE_filter.fwd_mask_data,&entry.data,sizeof(mea_cli_global_SE_filter.fwd_mask_data));
        MEA_OS_memcpy( &mea_cli_global_SE_filter.fwd_mask_key,&entry.key,sizeof(mea_cli_global_SE_filter.fwd_mask_key));
        CLI_print("set mask \n");
    return MEA_OK;
    }

    if(type==1){  //fields
        mea_cli_global_SE_filter.fwd_fields_key_enable=maskKey_enable;
        MEA_OS_memcpy( &mea_cli_global_SE_filter.fwd_fields_data,&entry.data,sizeof(mea_cli_global_SE_filter.fwd_mask_data));
        MEA_OS_memcpy( &mea_cli_global_SE_filter.fwd_fields_key, &entry.key,sizeof(mea_cli_global_SE_filter.fwd_mask_key));
        CLI_print("set fields \n");
    return MEA_OK;
    }

    if(type==2){
    i=0;
    mea_cli_global_SE_filter.fwd_reg_enable=MEA_TRUE;
    mea_cli_global_SE_filter.fwd_fields_data_reg[0] = MEA_OS_atohex(argv[i+1]);
    mea_cli_global_SE_filter.fwd_fields_data_reg[1] = MEA_OS_atohex(argv[i+2]);
    mea_cli_global_SE_filter.fwd_fields_key_reg[0] = MEA_OS_atohex(argv[i+3]);
    mea_cli_global_SE_filter.fwd_fields_key_reg[1] = MEA_OS_atohex(argv[i+4]);


    mea_cli_global_SE_filter.fwd_mask_data_reg[0] = MEA_OS_atohex(argv[i+5]);
    mea_cli_global_SE_filter.fwd_mask_data_reg[1] = MEA_OS_atohex(argv[i+6]);
    mea_cli_global_SE_filter.fwd_mask_key_reg[0]  = MEA_OS_atohex(argv[i+7]);
    mea_cli_global_SE_filter.fwd_mask_key_reg[1]  = MEA_OS_atohex(argv[i+8]);

   

    }


    if(type==3){
       if (MEA_API_SE_Entry_SetFilter (MEA_UNIT_0,
                index,
                &mea_cli_global_SE_filter)!= MEA_OK) {
                CLI_print("Error while create to  SE entry\n");
                return MEA_OK;
        }
       CLI_print("Done set Filter %d\n",index); 
    }


   

    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Forwarder_CreateFilter>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/


static MEA_Status MEA_CLI_Forwarder_CreateFilter(int argc, char *argv[]){

    MEA_Uint16                index;

    if ( argc <= 1) {
        return MEA_ERROR;
    }

    index = MEA_OS_atoi(argv[1]);

    if(MEA_API_SE_Entry_CreateFilter (MEA_UNIT_0,index)!=MEA_OK){
     
       CLI_print("Error on MEA_API_SE_Entry_CreateFilter index %d\n",index); 
        return MEA_OK;
    }

    
                               
    CLI_print(" Done CreateFilter index %d\n",index); 
                               
    return MEA_OK;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Forwarder_DeleteFilter>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_Forwarder_DeleteFilter(int argc, char *argv[]){

    MEA_Uint16                index,start_index,end_index;
    char buf[500];
    char *up_ptr;

    if ( argc <= 1) {
        return MEA_ERROR;
    }

    
    MEA_OS_strcpy(buf, argv[1]);
    if((up_ptr=strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_index = MEA_OS_atoi(buf);
        end_index   = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1],"all") == 0) {
            start_index=1;
            end_index=MEA_FORWARDER_FILTER_PROFILE-1;
        } else {
            index=start_index = end_index = MEA_OS_atoi(buf);
        }
    }


 
    for(index=start_index;index<=end_index;index++){
        if(MEA_API_SE_Entry_IsExistFilter (MEA_UNIT_0,index)!=MEA_TRUE)
           continue;

        if(MEA_API_SE_Entry_DeleteFilter (MEA_UNIT_0,index)!=MEA_OK){

            CLI_print("Error on MEA_API_SE_Entry_DeleteFilter index %d\n",index); 
            return MEA_OK;
        }
        CLI_print(" DeleteFilter index %d\n",index);

    }



    
    return MEA_OK;
}
/*---------------------------------------------------------------------------*/
/*                                                                            */
/*                   <MEA_CLI_Forwarder_ShowFilter>                           */
/*                                                                            */
/*---------------------------------------------------------------------------*/

static MEA_Status MEA_CLI_Forwarder_ShowFilter(int argc, char *argv[]){

    MEA_Uint16                index,start_index,end_index;
    char buf[500];
    char *up_ptr;
    MEA_SE_filter_Entry_dbt  entry_o;
    MEA_Bool                 first=MEA_TRUE;
    MEA_SE_Entry_dbt         print_entry;
    MEA_SE_Entry_dbt        *print_table_entry=NULL;
    MEA_Port_t               port;
    


    MEA_Uint32               srcIp;
    MEA_Uint32               dstIp;
    char                     srcIp_str[20];
    char                     dstIp_str[20];
    
    MEA_SE_Forwarding_key_type_te key_type=0;

    char mask_Ip_str[4][10];



    MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_32][0]), "/32");
    MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_24][0]), "/24");
    MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_16][0]), "/16");
    MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_8][0]), "/8");


    if ( argc <= 1) {
        return MEA_ERROR;
    }


    MEA_OS_strcpy(buf, argv[1]);
    if((up_ptr=strchr(buf, ':')) != 0)
    {
        (*up_ptr) = 0;
        up_ptr++;
        start_index = MEA_OS_atoi(buf);
        end_index   = MEA_OS_atoi(up_ptr);
    }
    else
    {
        if (MEA_OS_strcmp(argv[1],"all") == 0) {
            start_index=1;
            end_index=MEA_FORWARDER_FILTER_PROFILE-1;
        } else {
            index=start_index = end_index = MEA_OS_atoi(buf);
        }
    }



    for(index=start_index;index<=end_index;index++){
        if(MEA_API_SE_Entry_IsExistFilter (MEA_UNIT_0,index)!=MEA_TRUE)
            continue;
         MEA_OS_memset(&entry_o,0,sizeof(entry_o));
        if(MEA_API_SE_Entry_GetFilter (MEA_UNIT_0,index,&entry_o)!=MEA_OK){

            CLI_print("Error on MEA_API_SE_Entry_GetFilter index %d\n",index); 
            return MEA_OK;
        }
        if(first){
            CLI_print("Index  Key Key  Key                      Stat Age SA  Action   Limiter ac    xper   out \n"
                      "       Ena Type                                       Id       Id      port  Id     cluster\n"
                      "------ --- ---- ----------------------- ---- --- --- -------- ------- ---- ------ --------\n");
                     //12345678901234567890123456789012345678901234567890123456789012345678901234567890
                            
        first=MEA_FALSE;
        }
        CLI_print("%6d \n",index);
        if(entry_o.fwd_fields_key_enable == MEA_TRUE){
            MEA_OS_memcpy(&print_entry.key,&entry_o.fwd_fields_key,sizeof(MEA_SE_Entry_key_dbt));
            MEA_OS_memcpy(&print_entry.data,&entry_o.fwd_fields_data,sizeof(MEA_SE_Entry_data_dbt));

			

            print_table_entry  = &print_entry;

            key_type=entry_o.fwd_fields_key.type;
            CLI_print("%-6s %-3s %4d ","fields",MEA_STATUS_STR(entry_o.fwd_fields_key_enable),key_type);
            switch (key_type) {
       case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
           CLI_print("%02x:%02x:%02x:%02x:%02x:%02x %5d ",
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[0]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[1]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[2]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[3]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[4]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[5]),
               (print_table_entry->key.mac_plus_vpn.VPN));
           break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
       case MEA_SE_FORWARDING_KEY_TYPE_DA :
           CLI_print("%02x:%02x:%02x:%02x:%02x:%02x %5s ",
               (unsigned char)(print_table_entry->key.mac.MAC.b[0]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[1]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[2]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[3]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[4]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[5]),
               " ");
           break;
#endif
      
       case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN:
           CLI_print("L1=%7d L2=%7d vpn=%5d  mask=%d ",
               print_table_entry->key.mpls_vpn.label_1,
               print_table_entry->key.mpls_vpn.label_2,
               print_table_entry->key.mpls_vpn.VPN,
               print_table_entry->key.mpls_vpn.mask_type
               );
           break;
#ifdef PPP_MAC 
       case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID :
           CLI_print("%02x:%02x:%02x:%02x:%02x:%02x %5d ",
               (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[0]),
               (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[1]),
               (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[2]),
               (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[3]),
               (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[4]),
               (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[5]),
               (print_table_entry->key.mac_plus_pppoe_session_id.PPPoE_session_id));
           break;
#else
	   case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
		   CLI_print("Session %5d  VPN=%05d ",
			   (print_table_entry->key.PPPOE_plus_vpn.PPPoE_session_id),
			   (print_table_entry->key.PPPOE_plus_vpn.VPN)) ;
		   break;
#endif
       case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :		   
           CLI_print("Vlan=%05d VPN=%05d    ",
               (print_table_entry->key.vlan_vpn.VLAN),
               (print_table_entry->key.vlan_vpn.VPN));
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN:
           CLI_print("l=%7d vpn=%4d MdL=%2d op=%3d um=%1d src=%4d mask=%d   ",
               print_table_entry->key.oam_vpn.label_1,
               print_table_entry->key.oam_vpn.VPN,
               print_table_entry->key.oam_vpn.MD_level,
               print_table_entry->key.oam_vpn.op_code,
               print_table_entry->key.oam_vpn.packet_is_untag_mpls,
               print_table_entry->key.oam_vpn.Src_port,
               print_table_entry->key.oam_vpn.mask_type);
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:

           dstIp = print_table_entry->key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4;
           //dstIp |= (globals_entry.Globals_Device.MC_ServerIp.DestIp& 0xff000000);


           srcIp = print_table_entry->key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4;
           //srcIp |= (globals_entry.Globals_Device.MC_ServerIp.sourceIp & 0xff000000);
           MEA_OS_inet_ntoa(MEA_OS_htonl(dstIp), dstIp_str, sizeof(dstIp_str));
           MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), srcIp_str, sizeof(srcIp_str));
           CLI_print("DstIPv4:%s \n                SrcIPv4:%s \n                vpn %4d                 ", 
               dstIp_str,
               srcIp_str,
               print_table_entry->key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN);
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
           dstIp = print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4;
           MEA_OS_inet_ntoa(MEA_OS_htonl(dstIp), dstIp_str, sizeof(dstIp_str));
           CLI_print("DstIPv4:%s %s\n           L4DstPort:%5d \n           vpn %4d                    ",
               dstIp_str,
               mask_Ip_str[print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type],
               print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port,
               print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN);

           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
           srcIp = print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4;
           MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), srcIp_str, sizeof(srcIp_str));
           CLI_print("SrcIPv4:%s %s \n           L4SrcPort:%5d \n           vpn %4d                    ",
               srcIp_str,
               mask_Ip_str[print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type],
               print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port,
               print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN);

           break;

       case MEA_SE_FORWARDING_KEY_TYPE_LAST :
       default: 
           CLI_print("Unknown              ");
           break;
            }
            CLI_print ("%-4s %3d %-3s ",
                MEA_STATUS_STR(print_table_entry->data.static_flag),
                print_table_entry->data.aging,
                MEA_STATUS_STR(print_table_entry->data.sa_discard));


            
            CLI_print("%s %6d ",((print_table_entry->data.actionId_valid== 1) ? "Y":"N") ,print_table_entry->data.actionId);
            
            CLI_print("%s %5d ",((print_table_entry->data.limiterId_valid== 1) ? "Y":"N") ,print_table_entry->data.limiterId);
            
            //CLI_print("%s %3d ",((print_table_entry->data.cfm_oam_me_level_valid== 1) ? "Y":"N"),print_table_entry->data.cfm_oam_me_level_value);
            CLI_print(" %4d ", print_table_entry->data.access_port);

            CLI_print("%s %4d ",((print_table_entry->data.xPermissionId_valid== 1) ? "Y":"N") ,print_table_entry->data.xPermissionId);
           

            if(print_table_entry->data.OutPorts_valid){
            if(print_table_entry->data.OutPorts_valid == MEA_TRUE){
                for (port = 0;port <ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++)
                {
                    if (((ENET_Uint32*)(&(print_table_entry->data.OutPorts.out_ports_0_31)))[port/32] 
                    & (1 << (port%32))) {
                        CLI_print("%3d ",port);
                    }
                }

            }
            }else{
                 CLI_print("NA");
            }

           CLI_print("\n");
        }//end fields true
        else{
            if(entry_o.fwd_reg_enable == MEA_FALSE){
                CLI_print("%-6s %-3s %-4s ","fields",MEA_STATUS_STR(entry_o.fwd_fields_key_enable),"NA");
                MEA_OS_memcpy(&print_entry.data,&entry_o.fwd_fields_data,sizeof(print_entry.data));
                print_table_entry  = &print_entry;
                CLI_print("                        ");
                CLI_print ("%-4s %3d %-3s ",
                    MEA_STATUS_STR(print_table_entry->data.static_flag),
                    print_table_entry->data.aging,
                    MEA_STATUS_STR(print_table_entry->data.sa_discard));


                CLI_print("%s %6d ",((print_table_entry->data.actionId_valid== 1) ? "Y":"N") ,print_table_entry->data.actionId);

                CLI_print("%s %5d ",((print_table_entry->data.limiterId_valid== 1) ? "Y":"N") ,print_table_entry->data.limiterId);

                //CLI_print("%s %3d ",((print_table_entry->data.cfm_oam_me_level_valid== 1) ? "Y":"N"),print_table_entry->data.cfm_oam_me_level_value);
                CLI_print(" %4d ", print_table_entry->data.access_port);
                CLI_print("%s %4d ",((print_table_entry->data.xPermissionId_valid== 1) ? "Y":"N") ,print_table_entry->data.xPermissionId);

                if(print_table_entry->data.OutPorts_valid){
                    if(print_table_entry->data.OutPorts_valid == MEA_TRUE){
                        for (port = 0;port <ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++)
                        {
                            if (((ENET_Uint32*)(&(print_table_entry->data.OutPorts.out_ports_0_31)))[port/32] 
                            & (1 << (port%32))) {
                                CLI_print("%3d ",port);
                            }
                        }

                    }
                }else{
                    CLI_print("NA");
                }



            }
          CLI_print("\n");
        }
        
        //*************************MASK***********************************
            if(entry_o.fwd_mask_key_enable ==MEA_TRUE){
                MEA_OS_memcpy(&print_entry.key,&entry_o.fwd_mask_key,sizeof(print_entry.key));
                MEA_OS_memcpy(&print_entry.data,&entry_o.fwd_mask_data,sizeof(print_entry.data));

                print_table_entry  =&print_entry;

                key_type=entry_o.fwd_mask_key.type;
                CLI_print("%-6s %-3s %4d ","mask  ",MEA_STATUS_STR(entry_o.fwd_mask_key_enable),key_type);
                switch (key_type) {
       case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
           CLI_print("%02x:%02x:%02x:%02x:%02x:%02x %5d ",
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[0]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[1]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[2]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[3]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[4]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[5]),
               (print_table_entry->key.mac_plus_vpn.VPN));
           break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
       case MEA_SE_FORWARDING_KEY_TYPE_DA :
           CLI_print("%02x:%02x:%02x:%02x:%02x:%02x %5s ",
               (unsigned char)(print_table_entry->key.mac.MAC.b[0]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[1]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[2]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[3]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[4]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[5]),
               " ");
           break;
#endif
       case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :		   
           CLI_print("L1=%07d L2=%07d VPN=%05d    ",
               (print_table_entry->key.mpls_vpn.label_1),
               (print_table_entry->key.mpls_vpn.label_2),
               (print_table_entry->key.mpls_vpn.VPN));
           break;
#ifdef PPP_MAC 
	   case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
		   CLI_print("%02x:%02x:%02x:%02x:%02x:%02x %5d ",
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[0]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[1]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[2]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[3]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[4]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[5]),
			   (print_table_entry->key.mac_plus_pppoe_session_id.PPPoE_session_id));
		   break;
#else
	   case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
		   CLI_print("Session %5d  VPN=%05d ",
			   (print_table_entry->key.PPPOE_plus_vpn.PPPoE_session_id),
			   (print_table_entry->key.PPPOE_plus_vpn.VPN));
		   break;
#endif
       case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :		   
           CLI_print("Vlan=%05d VPN=%05d    ",
               (print_table_entry->key.vlan_vpn.VLAN),
               (print_table_entry->key.vlan_vpn.VPN));
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :
           CLI_print("%07d %04d %02d %03d %03d %03d   ",
               print_table_entry->key.oam_vpn.label_1,
               print_table_entry->key.oam_vpn.VPN,
               print_table_entry->key.oam_vpn.MD_level,
               print_table_entry->key.oam_vpn.op_code,
               print_table_entry->key.oam_vpn.packet_is_untag_mpls,
               print_table_entry->key.oam_vpn.Src_port);
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:

           dstIp = print_table_entry->key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4;
           //dstIp |= (globals_entry.Globals_Device.MC_ServerIp.DestIp& 0xff000000);


           srcIp = print_table_entry->key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4;
           //srcIp |= (globals_entry.Globals_Device.MC_ServerIp.sourceIp & 0xff000000);
           MEA_OS_inet_ntoa(MEA_OS_htonl(dstIp), dstIp_str, sizeof(dstIp_str));
           MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), srcIp_str, sizeof(srcIp_str));
           CLI_print("DstIPv4:%s \n                SrcIPv4:%s \n                vpn %4d                 ", 
               dstIp_str,
               srcIp_str,
               print_table_entry->key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN);
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
           dstIp = print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4;
           MEA_OS_inet_ntoa(MEA_OS_htonl(dstIp), dstIp_str, sizeof(dstIp_str));
           CLI_print("DstIPv4:%s \n                L4DstPort:%5d \n                vpn %4d                 ", 
               dstIp_str,
               print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port,
               print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN);

           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
           srcIp = print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4;
           MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), srcIp_str, sizeof(srcIp_str));
           CLI_print("SrcIPv4:%s \n                L4SrcPort:%5d \n                vpn %4d                 ", 
               srcIp_str,
               print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port,
               print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN);

           break;

       case MEA_SE_FORWARDING_KEY_TYPE_LAST :
       default: 
           CLI_print("Unknown              ");
           break;
                }
                CLI_print ("%-4s %3d %-3s ",
                    MEA_STATUS_STR(print_table_entry->data.static_flag),
                    print_table_entry->data.aging,
                    MEA_STATUS_STR(print_table_entry->data.sa_discard));


                CLI_print("%s %6d ",((print_table_entry->data.actionId_valid== 1) ? "Y":"N") ,print_table_entry->data.actionId);

                CLI_print("%s %5d ",((print_table_entry->data.limiterId_valid== 1) ? "Y":"N") ,print_table_entry->data.limiterId);

                //CLI_print("%s %3d ",((print_table_entry->data.cfm_oam_me_level_valid== 1) ? "Y":"N"),print_table_entry->data.cfm_oam_me_level_value);
                CLI_print(" %4d ", print_table_entry->data.access_port);
                CLI_print("%s %4d ",((print_table_entry->data.xPermissionId_valid== 1) ? "Y":"N") ,print_table_entry->data.xPermissionId);



            CLI_print("\n");
            }//end mask true
            else{
                if(entry_o.fwd_reg_enable == MEA_FALSE){
                   CLI_print("%-6s %-3s %-4s ","mask  ",MEA_STATUS_STR(entry_o.fwd_mask_key_enable),"NA");
                  MEA_OS_memcpy(&print_entry.data,&entry_o.fwd_mask_data,sizeof(print_entry.data));
                  print_table_entry  = &print_entry;
                  CLI_print("                        ");
                  CLI_print ("%-4s %3d %-3s ",
                      MEA_STATUS_STR(print_table_entry->data.static_flag),
                      print_table_entry->data.aging,
                      MEA_STATUS_STR(print_table_entry->data.sa_discard));


                  CLI_print("%s %6d ",((print_table_entry->data.actionId_valid== 1) ? "Y":"N") ,print_table_entry->data.actionId);

                  CLI_print("%s %5d ",((print_table_entry->data.limiterId_valid== 1) ? "Y":"N") ,print_table_entry->data.limiterId);

                  //CLI_print("%s %3d ",((print_table_entry->data.cfm_oam_me_level_valid== 1) ? "Y":"N"),print_table_entry->data.cfm_oam_me_level_value);
                  CLI_print(" %4d ", print_table_entry->data.access_port);
                  CLI_print("%s %4d ",((print_table_entry->data.xPermissionId_valid== 1) ? "Y":"N") ,print_table_entry->data.xPermissionId);


                  

                }
             CLI_print("\n");
            }





    }



CLI_print("\n");
CLI_print("Done\n");
    return MEA_OK;
}

static MEA_Status MEA_CLI_Forwarder_BrgFlushPortFid(int argc, char *argv[])
{
    MEA_Status retVal;
    MEA_Port_t clusterId ;
    MEA_Uint16 VPN;
    MEA_SE_Forwarding_key_type_te keyType;

    


    if ( argc < 3 )
        return MEA_ERROR;

    
    





    keyType = MEA_OS_atoi(argv[1]);
    VPN = MEA_OS_atoi(argv[2]);
    clusterId=MEA_OS_atoi(argv[3]);

    

    retVal= MEA_API_FWD_BrgFlushPortFid (clusterId , VPN, keyType);

    
    
    
    
    if(retVal == MEA_OK){
        CLI_print("Done\n");
        return MEA_OK;

    }
    CLI_print("Error : FWD_BrgFlush \n");
    return MEA_OK;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/

MEA_Status MEA_CLI_Forwarder_Create_FWD_VPN_Ipv6(int argc, char *argv[])
{
    MEA_FWD_VPN_dbt                entry;
    MEA_Uint32                     vpn;
    MEA_Uint32                     fwd_KeyType;

    if (argc <= 2) {
        return MEA_ERROR;
    }


    vpn = MEA_OS_atoi(argv[1]);
    fwd_KeyType = MEA_OS_atoi(argv[2]);

    MEA_OS_memset(&entry,0,sizeof(MEA_FWD_VPN_dbt));
    
    
    entry.fwd_vpn = vpn;
    entry.fwd_KeyType = fwd_KeyType;


    if (MEA_API_Create_FWD_VPN_Ipv6(MEA_UNIT_0, &entry) != MEA_OK) {

        CLI_print("Error on MEA_API_Create_FWD_VPN_Ipv6 index %d  fail \n", vpn);
        return MEA_OK;

    }


    CLI_print(" Done Create vpn %d with key type %d \n", entry.fwd_vpn, entry.fwd_KeyType);

    return MEA_OK;
}

MEA_Status MEA_CLI_Forwarder_Delete_FWD_VPN_Ipv6(int argc, char *argv[])
{
    MEA_FWD_VPN_dbt                entry;
    
    MEA_Uint32                     vpn;
    MEA_Bool                       found=MEA_FALSE;
    MEA_Bool                       findDodelete= MEA_FALSE;

    if (argc <= 1) {
        return MEA_ERROR;
    }


    

    if (MEA_OS_strcmp(argv[1], "all") == 0) {
    }
    else {
        vpn = MEA_OS_atoi(argv[1]);
        MEA_OS_memset(&entry, 0, sizeof(MEA_FWD_VPN_dbt));
        entry.fwd_vpn = vpn;

        if (MEA_API_Delete_FWD_VPN_Ipv6(MEA_UNIT_0, &entry) != MEA_OK) {

            CLI_print("Error on MEA_API_Delete_FWD_VPN_Ipv6 index %d  fail \n", entry.fwd_vpn);
            return MEA_OK;

        }

        return MEA_OK;
    }

    MEA_OS_memset(&entry, 0, sizeof(MEA_FWD_VPN_dbt));


    if(MEA_API_GetFirst_FWD_VPN_Ipv6(MEA_UNIT_0, &entry, &found) != MEA_OK){
        return MEA_OK;
    }

    while (found) {
        findDodelete = MEA_TRUE;
        
        if (MEA_API_Delete_FWD_VPN_Ipv6(MEA_UNIT_0, &entry) != MEA_OK) {

            CLI_print("Error on MEA_API_Delete_FWD_VPN_Ipv6 index %d  fail \n", entry.fwd_vpn);
            return MEA_OK;

        }

        if (MEA_API_GetNext_FWD_VPN_Ipv6(MEA_UNIT_0, &entry, &found) == MEA_OK) {
            CLI_print("Error on MEA_API_GetNext_FWD_VPN_Ipv6 index %d  fail \n", entry.fwd_vpn);
            return MEA_OK;
        }

    }

    
    if (findDodelete == MEA_TRUE)
      CLI_print(" Done delete %d\n", entry.fwd_vpn);

    return MEA_OK;
}


MEA_Status MEA_CLI_Forwarder_Show_FWD_VPN_Ipv6(int argc, char *argv[])
{
    MEA_FWD_VPN_dbt                entry;
    
    MEA_Uint32                     vpn;
    MEA_Uint32                     keyType;
    MEA_Bool                       found = MEA_FALSE;
    //MEA_Bool                       findDodelete = MEA_FALSE;

    if (argc <= 1) {
        return MEA_ERROR;
    }




    if (MEA_OS_strcmp(argv[1], "all") == 0) {
    }
    else {
        if (argc <= 2) {
            CLI_print("Error  set vpn and key \n" );
            return MEA_ERROR;
        }
        vpn = MEA_OS_atoi(argv[1]);
        keyType = MEA_OS_atoi(argv[2]);

        MEA_OS_memset(&entry, 0, sizeof(MEA_FWD_VPN_dbt));
        entry.fwd_vpn = vpn;
        entry.fwd_KeyType = keyType;

        if (MEA_API_Get_FWD_VPN_Ipv6(MEA_UNIT_0, &entry) != MEA_OK) 
        {
            CLI_print("Error on MEA_API_Get_FWD_VPN_Ipv6  %d  fail \n", entry.fwd_vpn);
            return MEA_OK;
        }
        CLI_print("***********************************\r\n");
        CLI_print("   IPV6 FWDVPN     \r\n");
        CLI_print("----------------------------------\r\n");
        CLI_print(" VpnId  %4d  KeyType %4d \r\n", entry.fwd_vpn, entry.fwd_KeyType);
        CLI_print("----------------------------------\r\n");

        return MEA_OK;
    }


    
    MEA_OS_memset(&entry, 0, sizeof(MEA_FWD_VPN_dbt));
    

    if (MEA_API_GetFirst_FWD_VPN_Ipv6(MEA_UNIT_0, &entry, &found) == MEA_OK) {

    }

    CLI_print("**************\r\n");
    CLI_print("   IPV6 FWDVPN     \r\n");
    CLI_print("---------------\r\n");

    while (found) {
        //findDodelete = MEA_TRUE;

        if (MEA_API_Get_FWD_VPN_Ipv6(MEA_UNIT_0, &entry) != MEA_OK) {

            CLI_print("Error on MEA_API_Get_FWD_VPN_Ipv6 vpn %d  fail \n", entry.fwd_vpn);
            return MEA_OK;

        }
        CLI_print(" VpnId  %4d  KeyType %4d \r\n", entry.fwd_vpn,entry.fwd_KeyType);
        CLI_print("--------------------\r\n");

        if (MEA_API_GetNext_FWD_VPN_Ipv6(MEA_UNIT_0, &entry, &found) != MEA_OK) {
            return MEA_OK;
        }

    
    
    }



    return MEA_OK;
}


MEA_Status MEA_CLI_Forwarder_Show_FWD_VPN_Ipv6DB_byVpnId(MEA_Uint32                     vpn)
{
    unsigned char buf[sizeof(struct in6_addr)];

    char stripv6[INET6_ADDRSTRLEN];

   
    MEA_Bool                       found = MEA_FALSE;
    //MEA_Bool                       findDodelete = MEA_FALSE;
    mea_fwd_vpn_IPv6_info_dbt  IPv6_info;




    MEA_OS_memset(&IPv6_info, 0, sizeof(mea_fwd_vpn_IPv6_info_dbt));


    if (MEA_API_GetFirst_FWD_VPN_Ipv6DB(MEA_UNIT_0, vpn, &IPv6_info, &found) != MEA_OK) {
        return MEA_OK;
    }

 
    while (found) {
        //findDodelete = MEA_TRUE;

        buf[0] = (IPv6_info.DestIPv6[3] >> 24) & 0xff;
        buf[1] = (IPv6_info.DestIPv6[3] >> 16) & 0xff;
        buf[2] = (IPv6_info.DestIPv6[3] >> 8) & 0xff;
        buf[3] = (IPv6_info.DestIPv6[3] >> 0) & 0xff;

        buf[4] = (IPv6_info.DestIPv6[2] >> 24) & 0xff;
        buf[5] = (IPv6_info.DestIPv6[2] >> 16) & 0xff;
        buf[6] = (IPv6_info.DestIPv6[2] >> 8) & 0xff;
        buf[7] = (IPv6_info.DestIPv6[2] >> 0) & 0xff;

        buf[8] = (IPv6_info.DestIPv6[1] >> 24) & 0xff;
        buf[9] = (IPv6_info.DestIPv6[1] >> 16) & 0xff;
        buf[10] = (IPv6_info.DestIPv6[1] >> 8) & 0xff;
        buf[11] = (IPv6_info.DestIPv6[1] >> 0) & 0xff;

        buf[12] = (IPv6_info.DestIPv6[0] >> 24) & 0xff;
        buf[13] = (IPv6_info.DestIPv6[0] >> 16) & 0xff;
        buf[14] = (IPv6_info.DestIPv6[0] >> 8) & 0xff;
        buf[15] = (IPv6_info.DestIPv6[0] >> 0) & 0xff;

        if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
            CLI_print(">>>inet_ntop<<< \n");
        }

        CLI_print("IPv6: %s\n         Signature 0x%08x \n",stripv6, IPv6_info.IPv6_Signature);

       
        found = MEA_FALSE;
        if (MEA_API_GetNext_FWD_VPN_Ipv6DB(MEA_UNIT_0, vpn, &IPv6_info, &found) != MEA_OK) {
            return MEA_OK;
        }

    }



    return MEA_OK;
}




MEA_Status MEA_CLI_Forwarder_Show_FWD_VPN_Ipv6DB(int argc, char *argv[])
{
    MEA_FWD_VPN_dbt                entry;

    MEA_Uint32                     vpn;
    MEA_Bool                       found = MEA_FALSE;
    //MEA_Bool                       findDodelete = MEA_FALSE;

    if (argc <= 1) {
        return MEA_ERROR;
    }




    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        CLI_print("**************\r\n");
        CLI_print("   IPV6 FWDVPN DB    \r\n");
        CLI_print("-----------------------------------------------------------\r\n");
    }
    else {
        vpn = MEA_OS_atoi(argv[1]);
        MEA_OS_memset(&entry, 0, sizeof(MEA_FWD_VPN_dbt));
        entry.fwd_vpn = vpn;
        CLI_print("**************\r\n");
        CLI_print("   IPV6 FWDVPN DB    \r\n");
        CLI_print("-----------------------------------------------------------\r\n");
        MEA_CLI_Forwarder_Show_FWD_VPN_Ipv6DB_byVpnId(vpn);

        return MEA_OK;
    }



    MEA_OS_memset(&entry, 0, sizeof(MEA_FWD_VPN_dbt));


    if (MEA_API_GetFirst_FWD_VPN_Ipv6(MEA_UNIT_0, &entry, &found) != MEA_OK) {
        return MEA_OK;
    }

    CLI_print("**************\r\n");
    CLI_print("   IPV6 FWDVPN DB    \r\n");
    CLI_print("---------------\r\n");

    while (found) {
        //findDodelete = MEA_TRUE;
        CLI_print("fwdVpn %4d keyType %4d \r\n", entry.fwd_vpn, entry.fwd_KeyType);
        MEA_CLI_Forwarder_Show_FWD_VPN_Ipv6DB_byVpnId(entry.fwd_vpn);
        CLI_print("---------------------------------------------------------------\r\n");
        if (MEA_API_GetNext_FWD_VPN_Ipv6(MEA_UNIT_0, &entry, &found) != MEA_OK) {
            return MEA_OK;
        }



    }



    return MEA_OK;
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Forwarder_AddUserCmds>                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_CLI_Forwarder_AddUserCmds(void) {

    
    CLI_defineCmd("MEA forwarder VPNIPv6 DB show ",
        (CmdFunc)MEA_CLI_Forwarder_Show_FWD_VPN_Ipv6DB,
        "VPNIPv6 show entry \n",
        "   show <vpn>/all\n"
        "------------------------------------------------------------------\n"
        "Example:\n"
        "     show 1\n"

    );
    
    
    CLI_defineCmd("MEA forwarder VPNIPv6 show",
        (CmdFunc)MEA_CLI_Forwarder_Show_FWD_VPN_Ipv6,
        "VPNIPv6 show entry \n",
        "   show all\n"
        "------------------------------------------------------------------\n"
        "Example:\n"
        "     show 1\n"

    );
    
    CLI_defineCmd("MEA forwarder VPNIPv6 create",
        (CmdFunc)MEA_CLI_Forwarder_Create_FWD_VPN_Ipv6,
        "create  VPNId  (Create the vpn fwd to ipv6)\n",
        "   create <vpnID><FWD_Key>\n"
        "------------------------------------------------------------------\n"
        "Example:\n"
        "     create 100 16 \n"
        "     create 101 17 \n"

    );

    CLI_defineCmd("MEA forwarder VPNIPv6 delete",
        (CmdFunc)MEA_CLI_Forwarder_Delete_FWD_VPN_Ipv6,
        "delete VPNIPv6 (Delete the vpn FWD of ipv6)\n",
        "   delete all/<id>\n"
        "------------------------------------------------------------------\n"
        "Example:\n"
        "     delete 1\n"

    );





    CLI_defineCmd("MEA forwarder filter set create",
        (CmdFunc)MEA_CLI_Forwarder_CreateFilter,
        "createFilter profile\n",
        "   create <ID>\n"
        "------------------------------------------------------------------\n"
        "Example:\n"
        "     create 1\n"

        );

    CLI_defineCmd("MEA forwarder filter set delete",
        (CmdFunc)MEA_CLI_Forwarder_DeleteFilter,
        "deleteFilter profile\n",
        "   delete all/<id>\n"
        "------------------------------------------------------------------\n"
        "Example:\n"
        "     delete 1\n"

        );
    CLI_defineCmd("MEA forwarder filter show",
        (CmdFunc)MEA_CLI_Forwarder_ShowFilter,
        "deleteFilter profile\n",
        "   show all/<id>\n"
        "------------------------------------------------------------------\n"
        "Example:\n"
        "     show 1\n"

        );
    
    
    
    CLI_defineCmd("MEA forwarder filter set clear",
        (CmdFunc)MEA_CLI_Forwarder_SetFilter,
        "Filter clear entry\n",
        "  <type>  clear  \n"
        "Filter clear entry                                  \n"
        " \n"
        " \n"
        "Example:\n"
        "     clear \n" 
       

        );
    CLI_defineCmd("MEA debug forwarder filter set W_REG",
        (CmdFunc)MEA_CLI_Forwarder_SetFilter,
        " set filter entry by regs\n",
        "  <type>  clear  \n"
        "set filter entry by regs                                  \n"
        " \n"
        " <fieldsreg0><fieldsreg1><fieldsreg2><fieldsreg3><maskreg0><maskreg1><maskreg2><maskreg3>  \n"
        "Example:\n"
        "     W_REG 0 80000000 0 8000000  0 80000000 0 8000000  \n" 


        );

    CLI_defineCmd("MEA forwarder filter set mask",
        (CmdFunc)MEA_CLI_Forwarder_SetFilter,
        "SetFilter mask  parameters entry\n",
        "\n"
        "SetFilter  <valid>  [<key_type> <key_f1> [<key-f2> [<key-f3> <key-f4>]]] \n"
        "   <key_type> - 0 : MAC+VPN  \n"
        "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
        "                     <key-f2> : VPN (0..4095)\n"
        "                1 : MPLS+VPN  \n"
        "                     <key-f1> : MPLS L1\n"
        "                     <key-f2> : MPLS L2\n"
        "                     <key-f3> : VPN \n"
        
#ifdef PPP_MAC                 
		"                2 : MAC+PPPoE SessionId \n"
		"                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
		"                     <key-f2> : PPPoE Session Id (0-65535)\n"
#else
		"                2 : PPPoE SessionId + VPN \n"
		"                     <key-f1> : PPPoE Session Id (0-65535)\n"
		"                     <key-f2> : Vlan Id (0-4095)\n"

#endif
        "                3 : VLAN + VPN\n"
        "                     <key-f1> : Vlan Id (0-4095)\n"
        "                     <key-f2> : VPN  Id (0-4095)\n"
        "                4 :  OAM \n"
        "                     <key-f1> : Vlan Id (0-4095)\n"
        "                     <key-f2> : VPN  Id (0-4095)\n"
        "                     <key-f3> : MD level  Id (0-7)\n" 
        "                     <key-f4> : opcode (0-255)\n"
        "                     <key-f5> : TLV type Id (0-255)\n"
        "                     <key-f6> : source port (0-127)\n"
      

        "                5 : DestIpv4 + SourceIpv4 + VPN \n"
        "                     <key-f1> : DestIpv4  (IPv4 address in dot notation)\n" 
        "                     <key-f2> : SourceIpv4(IPv4 address in dot notation)\n"
        "                     <key-f3> : VPN Id  (0-4095)\n"
        "                6 : DestIpv4 + L4DestPort + VPN \n"
        "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n" 
        "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
        "                     <key-f3> : VPN Id  (0-1024)\n"
       
        "                7 : SourceIpv4 + L4SrcPort + VPN \n"
        "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n" 
        "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
        "                     <key-f3> : VPN Id  (0-1024)\n"
        
        "------------------------------------------------------------------\n"
        "    [-age    <num>] number (1..3)\n"
        "    [-static <valid>      0 | 1\n"
        "    [-mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
        "    [-source_filter  <valid>"
        "    [-limiter <valid> <id (0..8191)>]\n"
        "    [-action> <valid> <id>]\n"
        "    [-cfm_oam_me_level <valid> <threshold> ]\n"
        "    [-xPermissionId  <valid> <id> (1023:0)]\n"
        "------------------------------------------------------------------\n"
        "\n"
        "     -limiter (limiterId) \n"
        "          <valid> - 0 or 1\n"
        "          <id>    - Limiter Id   \n"
        "\n"
        "     -action  (actionId)  \n"
        "          <valid> - 0 or 1\n"
        "          <id>    - Action Id  (mask 4095)   \n"
        "\n"
        "     -cfm_oam_me_level  (Management entry level)  \n"
        "          <valid> - 0 or 1\n"
        "          <threshold>  0 .. 15     \n"
        "                      The threshold compare to packet ME Level value \n"
        "                      >(drop) / ==(cpu) / <(trans) \n"
        "\n"
        "      -xPermissionId (virtual destination)\n"
        "          <valid> - 0 or 1\n"
        "          <id>    -  xPermissionId     \n"
        "\n"
        "     -age  (aging type)  \n"
        "          <number> - 0..3\n"
        "\n"
        "     -static (static or dynamic) \n"
        "          <valid> - 0-dynamic or 1-static\n"
        "\n"
        "     -source_filter (filter by sa) \n"
        "          <valid> - 0 or 1         \n"
        "\n"

        "------------------------------------------------------------------\n"
        "Example:\n"
        "    mask  1 0 00:00:00:00:00:00 4095   -action 1 4095 -limiter 0 0 -xPermissionId 1 2047 -age 3 -static 1 -source_filter 1 \n" 
        "    mask  1 0  4095   -action 0 0 -limiter 1 1  \n"
        "    mask  1 1 0xfffff 0xfffff 0xfff 0x3        -action 0 0 -limiter 1 1  \n"
        "    mask  1 2 ff:ff:ff:ff:ff:ff 65535  -action 0 0 -limiter 1 1  \n"
        "    mask  1 3 4095 4095                -action 1 8193 -limiter 1 1  \n"
        "    mask  1 4 4095 1023 15 255 255 3    -action 1 8193 -limiter 1 1  \n"
        "    mask  1 5 255.255.255.255  255.255.255.255 4095      -action 1 8193 -limiter 1 8191  \n"
        "    mask  1 6 255.255.255.255  1023 3    -action 1 4095 -limiter 1 1  \n"
        "    mask  1 7 255.255.255.255  1023 3      -action 1 4095 -limiter 1 1  \n"


        "     mask  0  -action 1  -limiter 1 1 -xPermissionId  1 1023 \n" 
        );
    
    CLI_defineCmd("MEA forwarder filter set fields",
        (CmdFunc)MEA_CLI_Forwarder_SetFilter,
        "SetFilter fields parameters entry\n",
        "\n"
        "SetFilter fields <valid>  [<key_type> <key_f1> [<key-f2> [<key-f3> <key-f4>] \n"
        "   <key_type> - 0 : MAC+VPN  \n"
        "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
        "                     <key-f2> : VPN (0..4095)\n"
        "                1 : MPLS+VPN  \n"
        "                     <key-f1> : MPLS L1\n"
        "                     <key-f2> : MPLS L2\n"
        "                     <key-f3> : VPN \n"
        
#ifdef PPP_MAC                 
		"                2 : MAC+PPPoE SessionId \n"
		"                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
		"                     <key-f2> : PPPoE Session Id (0-65535)\n"
#else
		"                2 : PPPoE SessionId + VPN \n"
		"                     <key-f1> : PPPoE Session Id (0-65535)\n"
		"                     <key-f2> : Vlan Id (0-4095)\n"

#endif
        "                3 : VLAN + VPN\n"
        "                     <key-f1> : Vlan Id (0-4095)\n"
        "                     <key-f2> : VPN  Id (0-4095)\n"
        "                4 :  OAM \n"
        "                     <key-f1> : Vlan Id (0-4095)\n"
        "                     <key-f2> : VPN  Id (0-4095)\n"
        "                     <key-f3> : MD level  Id (0-7)\n"
        "                     <key-f4> : opcode (0-255)\n"
        "                     <key-f5> : TLV type Id (0-255)\n"
        "                     <key-f6> : source port (0-127)\n"
        

        "                5 : DestIpv4 + SourceIpv4 + VPN \n"
        "                     <key-f1> : DestIpv4  (IPv4 address in dot notation)\n"
        "                     <key-f2> : SourceIpv4(IPv4 address in dot notation)\n"
        "                     <key-f3> : VPN Id  (0-4095)\n"
        "                6 : DestIpv4 + L4DestPort + VPN \n"
        "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
        "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
        "                     <key-f3> : VPN Id  (0-1024)\n"
        
        "                7 : SourceIpv4 + L4SrcPort + VPN \n"
        "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
        "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
        "                     <key-f3> : VPN Id  (0-1024)\n"
       
        "------------------------------------------------------------------\n"
        
        "    [-age  <num>] number (1..3)\n"
        "    [-static <valid>        - 0 | 1\n"
        "    [-mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
        "    [ -source_filter  <valid>"
        "    [-limiter <valid> <id>]\n"
        "    [-action> <valid> <id>]\n"
        "    [-cfm_oam_me_level <valid> <threshold> ]\n"
        "    [-xPermissionId   <id> (2047:0)]\n"
        "    [-out <num_of_clusters> <clusterX> ...<clusterXn>\n"
        "------------------------------------------------------------------\n"
        "    -limiter (limiterId) \n"
        "          <valid> - 0 or 1\n"
        "          <id>    - Limiter Id   \n"
        "\n"
        "    -action  (actionId)  \n"
        "          <valid> - 0 or 1\n"
        "          <id>    - Action Id     \n"
        "\n"
        "    -cfm_oam_me_level  (Management entry level)  \n"
        "          <valid> - 0 or 1\n"
        "          <threshold>  0 .. 15     \n"
        "                      The threshold compare to packet ME Level value \n"
        "                      >(drop) / ==(cpu) / <(trans) \n"
        "\n"
        "    -xPermissionId (virtual destination)\n"
        "          <valid> - 0 or 1\n"
        "          <id>    -  xPermissionId     \n"
        "\n"
        "    -age  (aging type)  \n"
        "          <number> - 0..3\n"
        "\n"
        "    -static (static or dynamic) \n"
        "          <valid> - 0-dynamic or 1-static\n"
        "\n"
        "    -source_filter (filter by sa) \n"
        "          <valid> - 0 or 1         \n"
        "\n"
        "   -out \n"
        "   <num_of_clusters>  - number of cluster parameters following this parameters\n"
        "   <clusterX>         - cluster number\n"

        "------------------------------------------------------------------\n"
        "Example:\n"
        "     fields 1 0 11:22:33:44:55:66 200  -action 0  0     -limiter 1 1 -xPermissionId 0 0 -out 1 125 -source_filter 0 -static 0 -age 3\n"
        "     fields 1 1 11:22:33:44:55:66      -action 0  0     -limiter 0 0 -xPermissionId 1 125\n"
        "     fields 1 2 11:22:33:44:55:66 100  -action 0  0     -limiter 0 0 -xPermissionId 0 0 -out 1 125\n"
        "     fields 1 3 258 10                 -action 1  8193  -limiter 0 0 -xPermissionId 0 0 -out 1 125\n"
        "     fields 1 4 258 20 125 5 2         -action 1  8193  -limiter 1 1 -xPermissionId 0 0 -out 0 0\n"
        "     fields 1 5 172.16.10.1 172.16.10.50 10  -action 0  0     -limiter 0 0 -xPermissionId 0 0 -out 1 125\n"
        "     fields 1 6 172.1.2.1 100          -action 0  0     -limiter 1 1 -xPermissionId 0 0 -out 1 125\n"
        "     fields 1 7 172.1.2.1 100          -action 0  0     -limiter 1 1 -xPermissionId 0 0 -out 1 125\n"


        "     fields 0     -action 1 8193  -limiter 0 0 -xPermissionId 0 0 -out 2 125 126\n"
        

        );

    CLI_defineCmd("MEA forwarder filter set promote",
        (CmdFunc)MEA_CLI_Forwarder_SetFilter,
        "SetFilter promote the filter entry\n",
        "   promote <ID>\n"
        "------------------------------------------------------------------\n"
        "Example:\n"
        "     promote 1\n"

        );
    
    
    
    
    
    CLI_defineCmd("MEA forwarder delete",
                  (CmdFunc)MEA_CLI_ForwarderDelete,
                  "Delete Forwarder Table entry(ies)\n",
                  "delete all  \n"
                  "delete -p <cluster>  To delete all dynamic learn with <cluster>\n" 
				  "delete <key_type> <key_f1> [<key-f2> [<key-f3> <key-f4>]]}    delete by key \n"
                  "   <key_type> - 0 : MAC+VPN  \n"
                  "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
                  "                     <key-f2> : VPN (0..4095)\n"
                  "                1 : MPLS+VPN  \n"
                  "                     <key-f1> : MPLS L1\n"
                  "                     <key-f2> : MPLS L2\n"
                  "                     <key-f3> : VPN \n"
#ifdef PPP_MAC                 
                  "                2 : MAC+PPPoE SessionId \n"
                  "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
                  "                     <key-f2> : PPPoE Session Id (0-65535)\n"
#else
		"                2 : PPPoE SessionId + VPN \n"
		"                     <key-f1> : PPPoE Session Id (0-65535)\n"
		"                     <key-f2> : Vlan Id (0-4095)\n"

#endif
		          "                3 : VLAN + VPN\n"
                  "                     <key-f1> : Vlan Id (0-4095)\n"
                  "                     <key-f2> : VPN  Id (0-4095)\n"
                  "                4 :  OAM \n"
                  "                     <key-f1> : Vlan Id (0-4095)\n"
                  "                     <key-f2> : VPN  Id (0-4095)\n"
                  "                     <key-f3> : MD level  Id (0-7)\n"
                  "                     <key-f4> : opcode (0-255)\n"
                  "                     <key-f5> : TLV type Id (0-255)\n"
                  "                     <key-f6> : source port (0-127)\n"


                  "                5 : DestIpv4 + SourceIpv4 + VPN \n"
                  "                     <key-f1> : DestIpv4  (IPv4 address in dot notation)\n"
                  "                     <key-f2> : SourceIpv4(IPv4 address in dot notation)\n"
                  "                     <key-f3> : VPN Id  (0-4095)\n"
                  "                6 : DestIpv4 + L4DestPort + VPN \n"
                  "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
                  "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
                  "                     <key-f3> : VPN Id  (0-1024)\n"

                  "                7 : SourceIpv4 + L4SrcPort + VPN \n"
                  "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
                  "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
                  "                     <key-f3> : VPN Id  (0-1024)\n"
                  "               16 : DestIpv6 + L4DestPort + VPN  \n"
                  "                     <key-f1> : DstIpv6 (IPv6 address)\n"
                  "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
                  "                     <key-f3> : VPN Id  (0-1024)\n"
                  "-------------------------------------------------------------------\n"
                  "    [-mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
                  "------------------------------------------------------------------\n"
                  "Examples: - delete all\n"
                  "          - delete 0 00:00:00:00:01:02 258 \n"
				  "          - delete 1 00:00:00:00:01:02     \n"
#ifdef PPP_MAC 
				  "          - delete 2 00:00:00:00:01:02  48 \n"
#else
		          "          - delete 2 2134  10 \n"
#endif
		          "          - delete 3 258 10\n"
				  "          - delete 4 258 20 5 3 32\n"
                  "            delete 5 172.1.1.1 172.1.1.1  3 \n");

    
        CLI_defineCmd("MEA forwarder routIp",
        (CmdFunc)MEA_CLI_ForwarderAdd_router_IP,
            "Add to a Forwarder Table entry\n",
            "add <key_type> <key_f1> [<key-f2> [<key-f3> <key-f4>]] <age> <static> <source_filter> <num_of_clusters> [<cluster1>..]\n"
            "   <key_type> - \n"

            "                0 : MAC+VPN  \n"
            "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
            "                     <key-f2> : VPN (0..1024)\n"
            "                1 : MPLS+VPN  \n"
            "                     <key-f1> : mpls label_1\n"
            "                     <key-f2> : mpls label_2\n"
            "                     <key-f3> : VPN (0..1024)\n"

#ifdef MEA_FWD_MAC_ONLY_SUPPORT
            "                1 : MAC  \n"
            "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
#endif
#ifdef PPP_MAC                 
			"                2 : MAC+PPPoE SessionId \n"
			"                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
			"                     <key-f2> : PPPoE Session Id (0-65535)\n"
#else
			"                2 : PPPoE SessionId + VPN \n"
			"                     <key-f1> : PPPoE Session Id (0-65535)\n"
			"                     <key-f2> : Vlan Id (0-4095)\n"

#endif
            "                3 : VLAN + VPN\n"
            "                     <key-f1> : Vlan Id (0-4095)\n"
            "                     <key-f2> : VPN  Id (0-4095)\n"
            "                4 :  OAM \n"
            "                     <key-f1> : Vlan Id (0-4095)\n"
            "                     <key-f2> : VPN  Id (0-1024)\n"
            "                     <key-f3> : MD level  Id (0-7)\n"
            "                     <key-f4> : opcode (0-255)\n"
            "                     <key-f5> : un-tag mpls\n"
            "                     <key-f6> : source port (0-127)\n"

            "                5 : DestIpv4 + SourceIpv4 + VPN \n"
            "                     <key-f1> : DestIpv4  (IPv4 address in dot notation)\n"
            "                     <key-f2> : SourceIpv4(IPv4 address in dot notation)\n"
            "                     <key-f3> : VPN Id  (0-1024)\n"

            "                6 : DestIpv4 + L4DestPort + VPN \n"
            "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
            "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
            "                     <key-f3> : VPN Id  (0-1024)\n"

            "                7 : SourceIpv4 + L4SrcPort + VPN \n"
            "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
            "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
            "                     <key-f3> : VPN Id  (0-1024)\n"
            
            "---------------------------------------------------------------\n"
            "   <age>           - number (1..3)\n"
            "   <static>        - 0 | 1\n"
            "   <source_filter> - 0 | 1\n"
            "   <num_of_clusters>  - number of cluster parameters following this parameters\n"
            "   <clusterX>         - cluster number\n"
            "    Option \n"
            "------------------------------------------------------------------\n"
            "    [-mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
            "    [-limiter <valid> <id>]\n"
            "    [-action> <valid> <id>]\n"
            "    [-access_port> <id>]\n"
            "----------------------------------\n"
            "-loop <value>\n"
            "-nextHopMac <nextHopMac  >\n"
            "-CreateAction <enableCreateAction>\n"

            "------------------------------------------------------------------\n"
            "\n"
            "   -mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
            "       <KEY1> : mask field  0-no mask 1-Int_MPLS 2-mask Ext_MPLS                 \n"
            "       <key4> : mask field  0-no mask 1-mask MPLS/VLAN \n"
            "       <key6,7> : mask field  0-Ip/32 1- Ip/24 2 - Ip/16 3 -Ip/8 \n"
            "\n"
            "     -limiter (limiterId) \n"
            "          <valid> - 0 or 1\n"
            "          <id>    - Limiter Id   \n"
            "\n"
            "     -action  (actionId)  \n"
            "          <valid> - 0 or 1\n"
            "          <id>    - Action Id     \n"
            "\n"
#if 0
            "     -cfm_oam_me_level  (Management entry level)  \n"
            "          <valid> - 0 or 1\n"
            "          <threshold>  0 .. 15     \n"
            "                      The threshold compare to packet ME Level value \n"
            "                      >(drop) / ==(cpu) / <(trans) \n"
            "\n"
#endif
            "     -access_port <value>\n"
            "------------------------------------------------------------------\n"
            "Example:\n"

            "    - add 0 11:22:33:44:55:66 258 3 1 0 1 125 \n"
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
            "    - add 1 11:22:33:44:55:66     3 1 0 1 125  \n"
#endif                  
            "    - add 1 100 200 10            3 1 0 1 125 \n"
#ifdef PPP_MAC 
            "    - add 2 11:22:33:44:55:66  24 3 1 0 2 125 126\n"
#else
				"    - add 2 2134  24 3 1 0 1 104\n"
#endif
            "    - add 3 258 10                3 1 0 1 125 \n"
            "    - add 4 258 20 5 47 0 127     3 1 0 1 125 \n"
            "    - add 0 11:22:33:44:55:66 258 3 1 0 2 125 126 -action 1 513 \n"
            "    - add 0 11:22:33:44:55:66 258  3 1 0 2 125 126 -action 1 2305 -limiter 1 1 \n"
            "    - add 5 172.1.2.1 172.3.4.1 10 3 1 0 2 125 126 -action 1 2305 \n"
            "    - add 6 172.1.2.1 100  1 3 1 0 1 125 -action 1 2305 \n"
            "    - add 7 192.1.1.1 80  1 3 1 0 1 126 -action 1 2305 \n"
        );





   CLI_defineCmd("MEA forwarder add",
                  (CmdFunc)MEA_CLI_ForwarderAdd,
                  "Add to a Forwarder Table entry\n",
				  "add <key_type> <key_f1> [<key-f2> [<key-f3> <key-f4>]] <age> <static> <source_filter> <num_of_clusters> [<cluster1>..]\n"
				  "   <key_type> - \n"

                  "                0 : MAC+VPN  \n"
				  "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
				  "                     <key-f2> : VPN (0..1024)\n"
                  "                1 : MPLS+VPN  \n"
                  "                     <key-f1> : mpls label_1\n"
                  "                     <key-f2> : mpls label_2\n"
                  "                     <key-f3> : VPN (0..1024)\n"
         
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
				  "                1 : MAC  \n"
				  "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
#endif
#ifdef PPP_MAC                 
	   "                2 : MAC+PPPoE SessionId \n"
	   "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
	   "                     <key-f2> : PPPoE Session Id (0-65535)\n"
#else
	   "                2 : PPPoE SessionId + VPN \n"
	   "                     <key-f1> : PPPoE Session Id (0-65535)\n"
	   "                     <key-f2> : Vlan Id (0-4095)\n"

#endif
				  "                3 : VLAN + VPN\n"
				  "                     <key-f1> : Vlan Id (0-4095)\n"
				  "                     <key-f2> : VPN  Id (0-4095)\n"
                  "                4 :  OAM \n"
                  "                     <key-f1> : Vlan/lable Id (0-4095)\n"
                  "                     <key-f2> : VPN  Id (0-1024)\n"
                  "                     <key-f3> : MD level  Id (0-7)\n" 
                  "                     <key-f4> : opcode (0-255)\n"
                  "                     <key-f5> : un-tag mpls\n"
                  "                     <key-f6> : source port (0-127)\n"

                  "                5 : DestIpv4 + SourceIpv4 + VPN \n"
                  "                     <key-f1> : DestIpv4  (IPv4 address in dot notation)\n" 
                  "                     <key-f2> : SourceIpv4(IPv4 address in dot notation)\n"
                  "                     <key-f3> : VPN Id  (0-1024)\n"
                 
                  "                6 : DestIpv4 + L4DestPort + VPN \n"
                  "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n" 
                  "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
                  "                     <key-f3> : VPN Id  (0-1024)\n"

                  "                7 : SourceIpv4 + L4SrcPort + VPN \n"
                  "                     <key-f1> : SourceIpv4  (IPv4 address in dot notation)\n" 
                  "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
                  "                     <key-f3> : VPN Id  (0-1024)\n"
                  "               16 : DestIpv6 + L4DestPort + VPN  \n"
                  "                     <key-f1> : DstIpv6 (IPv6 address)\n"
                  "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
                  "                     <key-f3> : VPN Id  (0-1024)\n"
                  "                17 : SourceIpv6 + L4SrcPort + VPN \n"
                  "                     <key-f1> : SourceIpv6  (IPv6 address in dot notation)\n"
                  "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
                  "                     <key-f3> : VPN Id  (0-1024)\n"

                  "-----------------------------------------------------\n"
                  "   <age>           - number (1..3)\n"
                  "   <static>        - 0 | 1\n"
                  "   <source_filter> - 0 | 1\n"
                  "   <num_of_clusters>  - number of cluster parameters following this parameters\n"
                  "   <clusterX>         - cluster number\n"
                  "    Option \n"
                  "------------------------------------------------------------------\n"
                  "    [-mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
                  "    [-limiter <valid> <id>]\n"
                  "    [-action> <valid> <id>]\n"
//                  "    [-cfm_oam_me_level <valid> <threshold> ]\n"

                  "[-access_port> <id>]\n"
                  "------------------------------------------------------------------\n"
                  "\n"
                  "   -mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
                  "       <KEY1> : mask field  0-no mask 1-Int_MPLS 2-mask Ext_MPLS                 \n"
                  "       <key4> : mask field  0-no mask 1-mask MPLS/VLAN \n"
                  "       <key6,7> : mask field  0-Ip/32 1- Ip/24 2 - Ip/16 3 -Ip/8 \n"
                  "\n"
                  "     -limiter (limiterId) \n"
                  "          <valid> - 0 or 1\n"
                  "          <id>    - Limiter Id   \n"
                  "\n"
				  "     -action  (actionId)  \n"
                  "          <valid> - 0 or 1\n"
                  "          <id>    - Action Id     \n"
                  "\n"
#if 0
                  "     -cfm_oam_me_level  (Management entry level)  \n"
                  "          <valid> - 0 or 1\n"
                  "          <threshold>  0 .. 15     \n"
                  "                      The threshold compare to packet ME Level value \n"
                  "                      >(drop) / ==(cpu) / <(trans) \n"
                  "\n"
#endif
                  "     -access_port <value>\n"
                  "------------------------------------------------------------------\n"
                  "Example:\n"

                  "    - add 0 11:22:33:44:55:66 258 3 1 0 1 125 \n"
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
                  "    - add 1 11:22:33:44:55:66     3 1 0 1 125  \n" 
#endif                  
                  "    - add 1 100 200 10            3 1 0 1 125 \n"
#ifdef PPP_MAC 
                  "    - add 2 11:22:33:44:55:66  24 3 1 0 2 125 126\n" 
#else
				  "    - add 2 2134  10 3 1 0 1 104\n"
#endif
                  "    - add 3 258 10                3 1 0 1 125 \n"
                  "    - add 4 258 20 5 47 0 127     3 1 0 1 125 \n"
                  "    - add 0 11:22:33:44:55:66 258 3 1 0 2 125 126 -action 1 513 \n" 
                  "    - add 0 11:22:33:44:55:66 258  3 1 0 2 125 126 -action 1 2305 -limiter 1 1 \n"
                  "    - add 5 172.1.2.1 172.3.4.1 10 3 1 0 2 125 126 -action 1 2305 \n"
                  "    - add 6 172.1.2.1 100  1 3 1 0 1 100 -action 1 2305 \n"
                  "    - add 7 192.1.1.1 80  1 3 1 0 1 118 -action 1 2305 \n"
                  "    - add 16 2111:5::2222:1 0 111  3 1 0 1 104 \n"
                  );


   CLI_defineCmd("MEA forwarder update",
                  (CmdFunc)MEA_CLI_ForwarderUpdate,
                "Update a Forwarder Table entry\n",
                "update <key_type> <key_f1> [<key-f2> [<key-f3> <key-f4>]] <age> <static> <source_filter> <num_of_clusters> <cluster1>..\n"
                "   <key_type> - 0 : MAC+VPN  \n"
                "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
                "                     <key-f2> : VPN (0..4095)\n"
                "                1 : MPLS+VPN  \n"
                "                     <key-f1> : MPLS L1\n"
                "                     <key-f2> : MPLS L2\n"
                "                     <key-f3> : VPN \n"
#ifdef PPP_MAC                 
	   "                2 : MAC+PPPoE SessionId \n"
	   "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
	   "                     <key-f2> : PPPoE Session Id (0-65535)\n"
#else
	   "                2 : PPPoE SessionId + VPN \n"
	   "                     <key-f1> : PPPoE Session Id (0-65535)\n"
	   "                     <key-f2> : Vlan Id (0-4095)\n"

#endif
                "                3 : VLAN + VPN\n"
                "                     <key-f1> : Vlan Id (0-4095)\n"
                "                     <key-f2> : VPN  Id (0-4095)\n"
                "                4 :  OAM \n"
                "                     <key-f1> : Vlan/lable Id (0-4095)\n"
                "                     <key-f2> : VPN  Id (0-1024)\n"
                "                     <key-f3> : MD level  Id (0-7)\n"
                "                     <key-f4> : opcode (0-255)\n"
                "                     <key-f5> : un-tag mpls\n"
                "                     <key-f6> : source port (0-127)\n"


                "                5 : DestIpv4 + SourceIpv4 + VPN \n"
                "                     <key-f1> : DestIpv4  (IPv4 address in dot notation)\n"
                "                     <key-f2> : SourceIpv4(IPv4 address in dot notation)\n"
                "                     <key-f3> : VPN Id  (0-4095)\n"
                "                6 : DestIpv4 + L4DestPort + VPN \n"
                "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
                "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
                "                     <key-f3> : VPN Id  (0-1024)\n"

                "                7 : SourceIpv4 + L4SrcPort + VPN \n"
                "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
                "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
                "                     <key-f3> : VPN Id  (0-1024)\n"
                "               16 : DestIpv6 + L4DestPort + VPN  \n"
                "                     <key-f1> : DstIpv6 (IPv6 address)\n"
                "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
                "                     <key-f3> : VPN Id  (0-1024)\n"
                "                17 : SourceIpv6 + L4SrcPort + VPN \n"
                "                     <key-f1> : SourceIpv6  (IPv6 address in dot notation)\n"
                "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
                "                     <key-f3> : VPN Id  (0-1024)\n"

                "   <age>           - number (1..3)\n"
                "   <static>        - 0 | 1\n"
                "   <source_filter> - 0 | 1\n"
                "   <num_of_clusters>  - number of cluster parameters following this parameters\n"
                "   <clusterX>         - cluster number\n"
                  "------------------------------------------------------------------\n"
                  "    [-mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
                  "    [-action> <valid> <id>]\n"
                  "    [-cfm_oam_me_level <valid> <threshold> ]\n"
                  "------------------------------------------------------------------\n"
                  "\n"
                  "   -mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
                  "       <KEY1> : mask field  0-no mask 1-Int_MPLS 2-mask Ext_MPLS                 \n"
                  "       <key4> : mask field  0-no mask 1-mask MPLS/VLAN \n"
                  "       <key6,7> : mask field  0-Ip/32 1- Ip/24 2 - Ip/16 3 -Ip/8 \n"
                  "\n"
                  "     -action  (actionId)  \n"
                  "          <valid> - 0 or 1\n"
                  "          <id>    - Action Id     \n"
#if 0
                  "     -cfm_oam_me_level  (Management entry level)  \n"
                  "          <valid> - 0 or 1\n"
                  "          <threshold>  0 .. 15     \n"
                  "                      The threshold compare to packet ME Level value \n"
                  "                      >(drop) / ==(cpu) / <(trans) \n"
#endif
                  "    -access_port <value>\n"

                  "-------------------------------------------------------------------------\n"
                  "Examples: \n"
                  "    - update 0 11:22:33:44:55:66 120 3 1 0 3 0 1 2\n"
                  "    - update 1 100 200 10   3 1 0 3 0 1 2\n"
#ifdef PPP_MAC  
                  "    - update 2 11:22:33:44:55:66  24 3 1 0 3 0 1 2\n"
#else
	              "    - update 2 2123 10 3 1 0 3 0 1 105 \n"
#endif
	              "    - update 3 120 10                3 1 0 3 0 1 2\n"
                  "    - update 4 258 20 125 5 0 127    3 1 0 3 0 1 2\n"
                  "    - update 0 11:22:33:44:55:66 120  3 1 0 3 0 1 2 -action 1 1\n" 
                  "    - update 5 172.1.2.1 172.3.4.1 10 3 1 0 3 0 1 125 -action 1 100 \n"
                  "    - update 6 172.1.2.1 100  1       3 1 0 1 125 -action 1 2305 \n"
                  "    - update 7 192.1.1.1 80   1       3 1 0 1 126 -action 1 2305 \n");



   CLI_defineCmd("MEA forwarder show",
                  (CmdFunc)MEA_CLI_ForwarderShow,
                    "Show Forwarder Table entry(ies)\n",
                    "show all|{<key_type> <key-f1> [<key-f2> [<key-f3> <key-f4>]]}\n"
                    "   <key_type> - 0 : MAC+VPN  \n"
                    "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
                    "                     <key-f2> : VPN (0..4095)\n"
                    "                1 : MPLS+VPN  \n"
                    "                     <key-f1> : MPLS L1\n"
                    "                     <key-f2> : MPLS L2\n"
                    "                     <key-f3> : VPN \n"
#ifdef PPP_MAC                 
	   "                2 : MAC+PPPoE SessionId \n"
	   "                     <key-f1> : Mac Address (xx:xx:xx:xx:xx:xx)\n"
	   "                     <key-f2> : PPPoE Session Id (0-65535)\n"
#else
	   "                2 : PPPoE SessionId + VPN \n"
	   "                     <key-f1> : PPPoE Session Id (0-65535)\n"
	   "                     <key-f2> : Vlan Id (0-4095)\n"

#endif
                    "                3 : VLAN + VPN\n"
                    "                     <key-f1> : Vlan Id (0-4095)\n"
                    "                     <key-f2> : VPN  Id (0-4095)\n"
                    "                4 :  OAM \n"
                    "                     <key-f1> : Vlan/lable Id (0-4095)\n"
                    "                     <key-f2> : VPN  Id (0-1024)\n"
                    "                     <key-f3> : MD level  Id (0-7)\n"
                    "                     <key-f4> : opcode (0-255)\n"
                    "                     <key-f5> : un-tag mpls\n"
                    "                     <key-f6> : source port (0-127)\n"


                    "                5 : DestIpv4 + SourceIpv4 + VPN \n"
                    "                     <key-f1> : DestIpv4  (IPv4 address in dot notation)\n"
                    "                     <key-f2> : SourceIpv4(IPv4 address in dot notation)\n"
                    "                     <key-f3> : VPN Id  (0-4095)\n"
                    "                6 : DestIpv4 + L4DestPort + VPN \n"
                    "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
                    "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
                    "                     <key-f3> : VPN Id  (0-1024)\n"

                    "                7 : SourceIpv4 + L4SrcPort + VPN \n"
                    "                     <key-f1> : DstIpv4  (IPv4 address in dot notation)\n"
                    "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
                    "                     <key-f3> : VPN Id  (0-1024)\n"
                    "               16 : DestIpv6 + L4DestPort + VPN  \n"
                    "                     <key-f1> : DstIpv6 (IPv6 address)\n"
                    "                     <key-f2> : L4DestPort(Destination L4 Port)\n"
                    "                     <key-f3> : VPN Id  (0-1024)\n"
                    "                17 : SourceIpv6 + L4SrcPort + VPN \n"
                    "                     <key-f1> : SourceIpv6  (IPv6 address in dot notation)\n"
                    "                     <key-f2> : L4SrcPort(Source L4 Port)\n"
                    "                     <key-f3> : VPN Id  (0-1024)\n"
                  "--------------------------------------------------------------------------------"
                  "  option \n"
                  "        -filter <valid> <id>\n"
                  "        -count <alocate number of print> \n"
                  "        -mask_type  (key,1,4,6,7) <value> 0:3  \n"
                  "--------------------------------------------------------------------------------"
                  "   -mask_type  (key,1,4,6,7) <value> 0:3  ]\n"
                  "       <KEY1> : mask field  0-no mask 1-Int_MPLS 2-mask Ext_MPLS                 \n"
                  "       <key4> : mask field  0-no mask 1-mask MPLS/VLAN \n"
                  "       <key6,7> : mask field  0-Ip/32 1- Ip/24 2 - Ip/16 3 -Ip/8 \n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - show all -filter 1 <id>\n"
                  "          - show 0 00:00:00:00:01:02 258\n"
                  "          - show 1 100 200 10   \n"
                  
#ifdef PPP_MAC  
	   "          - show 2 00:00:00:00:01:02  24\n"
#else
	   "          - show 2 2134  10 \n"
#endif

                  "          - show 3 258 10               \n"
                  "          - show 4 258 20 5 3 32 127       \n"
                  "          - show 5 172.10.1.1 172.10.10.1 3\n"
                  "          - show 6 172.10.1.1 80 3\n"
                  "          - show 7 192.10.1.1 80 3\n");

   CLI_defineCmd("MEA forwarder aging",
                  (CmdFunc)MEA_CLI_ForwarderAging_set,
                  "Aging_set the aging time \n",
 		          "aging <aging_enable> <aging_time> \n"
		          "   <aging_enable> : 0-Disable 1 - enable\n"
                  "   <aging_time>       : aging time in seconds\n"
                  "-----------------------------------------\n"
                  "Examples: - \n"
                  "          - aging 1  120\n"
                  "          - aging 1  300\n"
                  "          - aging 0   60\n");


   CLI_defineCmd("MEA forwarder BrgFlush ",
       (CmdFunc)MEA_CLI_Forwarder_BrgFlushPortFid,
       "BrgFlush delete the virtual port with key type\n",
       "BrgFlush <keytype> <vpn><clusterId> \n"
       "   <keytype>  0 : MAC+VPN      \n"
       "              1 : MAC \n"
       
       "   <vpn>          \n"
       "   <clusterId>    \n"
       "-----------------------------------------\n"
       "Examples: - \n"
       "          - BrgFlush 0  1  125\n"
       "          - BrgFlush 1  0  125\n"
       "          - BrgFlush 0  5  126 \n");


#ifdef NAT_ADAPTOR_WANTED
   CLI_defineCmd("MEA forwarder NAT status",
                  (CmdFunc)MEA_CLI_Forwarder_NatStatus,
	"Enables Network Address Translation in the Adaptor\n",
	"NAT status <value> \n"
	"<value> : 0-Disable 1 - enable\n"
       "-----------------------------------------\n"
        "Example:\n"
        "     NAT status 1 \n"); 


   CLI_defineCmd("MEA forwarder NAT entry",
       (CmdFunc)MEA_CLI_Forwarder_StaticNatEntry,
       "NAT entry - Adds IPv4 Static NAT entry\n",
       "NAT entry <localip> <globalip>\n"
       "   <localip>      Local IP Address in LAN \n"
       "   <globalip>     Global IP Address in WAN \n"
       "-----------------------------------------\n"
       "Examples: - \n"
       "          - NAT entry 172.0.0.10 15.0.0.10\n");

   CLI_defineCmd("MEA forwarder PAT entry",
       (CmdFunc)MEA_CLI_Forwarder_StaticPatEntry,
       "PAT entry - Adds IPv4 Static PAT entry\n",
       "PAT entry <localip> <localport> <globalip> <globalport>\n"
       "   <localip>      Local IP Address in LAN \n"
       "   <localport>    Local IP Port in LAN\n"
       "   <globalip>     Global IP Address in WAN \n"
       "   <globalport>   Global IP Port in WAN\n"
       "-----------------------------------------\n"
       "Examples: - \n"
       "          - PAT entry 172.0.0.10 67 15.0.0.10 68\n");

#endif

	return MEA_OK;
}









/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Forwarder_AddCmds>                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_CLI_Forwarder_AddCmds(void) {

	if (MEA_CLI_Forwarder_AddCountersCmds() != MEA_OK) {
		CLI_print(
			              "%s - MEA_CLI_Forwarder_AddCountersCmds failed\n",
			              __FUNCTION__);
		return MEA_OK;
	}

	if (MEA_CLI_Forwarder_AddUserCmds() != MEA_OK) {
		CLI_print(
			              "%s - MEA_CLI_Forwarder_AddUserCmds failed\n",
			              __FUNCTION__);
		return MEA_OK;
	}



	if (MEA_CLI_Forwarder_AddDebugCmds() != MEA_OK) {
		CLI_print(
			              "%s - MEA_CLI_Forwarder_AddDebugCmds failed\n",
			              __FUNCTION__);
		return MEA_OK;
	}


	return MEA_OK;
}




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   Debug forwarder - not via external APIs                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/


#include "mea_fwd_tbl.h" /* For debug commands only */
#include "enet_xPermission_drv.h"




/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_ForwarderRead>                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_ForwarderRead(int argc, char *argv[]) 
{
#ifndef MEA_FWD_SUPPORT_OLD_DSE
	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not support \n",__FUNCTION__);
	return MEA_ERROR;
#else 
   MEA_Forwarder_Signature_dbt fwd_entry_signature;
   MEA_Forwarder_Result_dbt    fwd_entry_result;    
   MEA_Fwd_Tbl_HashType_t      hash_type;
   MEA_Uint32                  entry_number;

    if ( argc != 3 )
        return MEA_ERROR;

    hash_type = (MEA_Fwd_Tbl_HashType_t)MEA_OS_atoi(argv[1]);
    entry_number = MEA_OS_atoi(argv[2]);


   if ( MEA_fwd_tbl_readByIndex( hash_type,
                                 entry_number,
                                &fwd_entry_signature,
                                &fwd_entry_result ) != MEA_OK )
    {
        CLI_print("Error while reading Forwarder Table\n");
        return MEA_OK;
    }


    CLI_print("Forwarder Entry %d:\n\n", entry_number);
    CLI_print("Raw data:  0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", 
		   fwd_entry_signature.regs[0],
		   fwd_entry_signature.regs[1],
		   fwd_entry_signature.regs[2],
		   fwd_entry_result.regs[0],
		   fwd_entry_result.regs[1],
		   fwd_entry_result.regs[2]);
	CLI_print("Signature: 0x%08x 0x%08x\n", 
		fwd_entry_signature.val.key.raw.signature1, 
		fwd_entry_signature.val.key.raw.signature0);
	CLI_print("key_type:          %d\n", fwd_entry_signature.val.key.raw.key_type);
    CLI_print("Valid:             %d\n", fwd_entry_signature.val.key.raw.valid);
    CLI_print("Static:            %d\n", fwd_entry_result.val.data.static_flag);
    CLI_print("Age:               %d\n", fwd_entry_result.val.data.age);
	CLI_print("xPermissionId:     0x%05x\n", fwd_entry_result.val.data.xPermissionId);
	CLI_print("mcEditing_FID:     %d\n", fwd_entry_result.val.data.mcEditing_FID);
	CLI_print("mcFix_skipEditing: %d\n", fwd_entry_result.val.data.mcFix_skipEditing);
	CLI_print("sa_discard:        %d\n", fwd_entry_result.val.data.sa_discard);


#endif
    return MEA_OK;
}


/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_ForwarderWrite>                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
static MEA_Status MEA_CLI_ForwarderWrite(int argc, char *argv[])
{
#ifndef MEA_FWD_SUPPORT_OLD_DSE
	MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"%s - Not support \n",__FUNCTION__);
	return MEA_ERROR;
#else 
   MEA_Forwarder_Signature_dbt fwd_entry_signature;
   MEA_Forwarder_Result_dbt    fwd_entry_result;    
   MEA_Fwd_Tbl_HashType_t      hash_type;
   MEA_Uint32                  entry_number;
   int                         num_of_ports, i;
   ENET_xPermission_dbt          portMap;
   ENET_xPermissionId_t        xPermissionId;
   MEA_Port_t                  port;

    if ( argc < 3  )
        return MEA_ERROR;

    hash_type = (MEA_Fwd_Tbl_HashType_t)MEA_OS_atoi(argv[1]);
	entry_number = MEA_OS_atoi(argv[2]);
    
	if(!strcmp(argv[3],"-r")){
		if ( argc != 8  )
			return MEA_ERROR;
		fwd_entry_signature.regs[0] = (MEA_Uint32)MEA_OS_atohex(argv[4]);
		fwd_entry_signature.regs[1] = (MEA_Uint32)MEA_OS_atohex(argv[5]);
		fwd_entry_result.regs[0]       = (MEA_Uint32)MEA_OS_atohex(argv[6]);
		fwd_entry_result.regs[1]       = (MEA_Uint32)MEA_OS_atohex(argv[7]);
		
	}

	else if(!strcmp(argv[3],"-f")){
		if ( argc != 5 && argc < 10 )
			return MEA_ERROR;

		MEA_OS_memset(&fwd_entry_signature, 0, sizeof(MEA_Forwarder_Signature_dbt));
		MEA_OS_memset(&fwd_entry_result,    0, sizeof(MEA_Forwarder_Result_dbt));
		MEA_OS_memset(&portMap, 0, sizeof(portMap));

		fwd_entry_signature.val.key.raw.valid = MEA_OS_atoi(argv[4]);

		if ( fwd_entry_signature.val.key.raw.valid == 0 )
		{
			if ( argc != 5 )
			{
				CLI_print("Entered valid == 0 with too much arguments\n");
				return MEA_OK;
			}
		}
		else if ( fwd_entry_signature.val.key.raw.valid == 1 )
		{
			if ( argc < 10 )
			{
				CLI_print("Entered valid == 1 with too few arguments\n");
				return MEA_OK;
			}
		}
		else
		{
			return MEA_ERROR;
		}

		if ( fwd_entry_signature.val.key.raw.valid == 1 )
		{
			fwd_entry_signature.val.key.raw.key_type = (MEA_Uint32)MEA_OS_atohex(argv[5]);
			fwd_entry_signature.val.key.raw.signature1 = (MEA_Uint32)MEA_OS_atohex(argv[6]);
			fwd_entry_signature.val.key.raw.signature0 = (MEA_Uint32)MEA_OS_atohex(argv[7]);
			fwd_entry_result.val.data.age         = (MEA_Uint32)MEA_OS_atoi(argv[8]);
			fwd_entry_result.val.data.static_flag = (MEA_Uint32)MEA_OS_atoi(argv[9]);    
			fwd_entry_result.val.data.sa_discard  = (MEA_Uint32)MEA_OS_atoi(argv[10]);
			num_of_ports = (MEA_Uint32) MEA_OS_atoi(argv[11]);

			if ( num_of_ports != argc - 12 )
				return MEA_ERROR;

            for ( i = 12; i < argc; i ++ )
			{
               port = MEA_OS_atoi(argv[i]);

			   if (MEA_API_Get_IsPortValid(port,MEA_TRUE) != MEA_TRUE) {
				   CLI_print("Port id %d is not define \n",port);
				   return MEA_ERROR;
			   }

			   /* check if the out_port is valid */
		        ((ENET_Uint32*)(&(portMap.out_ports_0_31)))[port/32] |=  
					(1 << (port%32));

			}

			xPermissionId = ENET_PLAT_GENERATE_NEW_ID;
			if (enet_Create_xPermission(ENET_UNIT_0,
				                        &portMap,
										&xPermissionId) != ENET_OK) {
				CLI_print("Invalid port reported by mea_drv_Create_xPermission \n");
				return MEA_ERROR;
			}
			fwd_entry_result.val.data.xPermissionId = xPermissionId;
		}
	}else {
		CLI_print("option unknowns\n");
		return MEA_OK;
	}
	if ( MEA_fwd_tbl_WriteByIndex( hash_type,
								   entry_number,
								   &fwd_entry_signature,
								   &fwd_entry_result )     != MEA_OK )
	{
		CLI_print("Error while writing to Forwarder Table\n");
		return MEA_OK;
	}
    return MEA_OK;
#endif
}





/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                   <MEA_CLI_Forwarder_AddDebugCmds>                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_CLI_Forwarder_AddDebugCmds(void) {

   CLI_defineCmd("MEA debug module forwarder read",
                  (CmdFunc)MEA_CLI_ForwarderRead,
                  "Read a Forwarder Table entry\n",
                  "read <type> <entry_number>\n"
                  "   <type>         - 0 | 1\n"
                  "   <entry_number> - 0..32K-1\n"
                  "------------------------------------------------------------------\n"
                  "Example: - read 1 100\n");    


   CLI_defineCmd("MEA debug module forwarder write",
                  (CmdFunc)MEA_CLI_ForwarderWrite,
                  "Write a Forwarder Table entry\n",
                  "write  <type> <entry_number> \n"
                  "      [-r <s0> <s1> <s2> <r0> <r1> <r2>]\n"
				  "      [-f <valid> [<key_type> <signature-msb> <signature-lsb> <age> <static> <num_of_clusters> <cluster1>..]\n"
                  "------------------------------------------------------------------\n"
                  "         <type>         - 0 | 1\n"
                  "         <entry_number> - 0..32K-1\n"
                  "\n"
                  "      -r (write raw data)\n"
                  "         <s0> -  signature word (32 bit) \n"
				  "         <s1> -  signature word (32 bit) \n"
                  "         <r0> -  result    word (32 bit) \n"
				  "         <r1> -  result    word (32 bit) \n"
                  "\n"
                  "      -f (formatted data)\n"
                  "         <valid>        - 1-more parameters ahead, 0-no parameters ahead\n"
                  "         <key_type>     - 0/1/2/3\n"
                  "         <signature-msb>- 28 bit hex value\n"
                  "         <signature-lsb>- 32 but hex value\n"
                  "         <age>           - number (0..3)\n"
                  "         <static>        - 0 | 1\n"
                  "         <source_filter> - 0 | 1\n"
                  "         <num_of_clusters> - number of clusters following this parameter\n"
                  "         <clusterX>        - cluster number \n"
                  "------------------------------------------------------------------\n"
                  "Example: - write 1 100 -f  1 0 1546B333 C00A2213 3 1 0 4 0 1 2 3\n"
				  "           write 1 100 -r 80000000 bbbb dddd eeee ");    





	return MEA_OK;
}


MEA_Status mea_frwarder_print_key(MEA_SE_Entry_key_dbt *forwarder_key,MEA_Bool settintCR )
{

     MEA_SE_Entry_dbt        *print_table_entry;
     MEA_SE_Entry_dbt         print_table;
     
     MEA_Uint32               srcIp;
     char                     srcIp_str[20];
     MEA_Uint32               dstIp;
     char                     dstIp_str[20];
     MEA_Globals_Entry_dbt    globals_entry; 
     char mask_Ip_str[4][10];
     unsigned char buf[sizeof(struct in6_addr)];

     char stripv6[INET6_ADDRSTRLEN];

     

     MEA_SE_Forwarding_key_type_te key_type=0;

     MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_32][0]), "/32");
     MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_24][0]), "/24");
     MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_16][0]), "/16");
     MEA_OS_strcpy(&(mask_Ip_str[MEA_DSE_MASK_IP_8][0]), "/8");

     MEA_OS_memcpy(&print_table.key,forwarder_key,sizeof(print_table.key));
     
     if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&globals_entry) != MEA_OK) {
         CLI_print("Error while get API_Get_Globals_Entry\n");
         return MEA_OK;
     }
     
     print_table_entry=&print_table;

     key_type = print_table_entry->key.type;
    CLI_print("%4d ",key_type);
      
    switch (key_type) {
       case MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN :
           
           CLI_print("%02x:%02x:%02x:%02x:%02x:%02x vpn=%5d         ",
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[0]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[1]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[2]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[3]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[4]),
               (unsigned char)(print_table_entry->key.mac_plus_vpn.MAC.b[5]),
               (print_table_entry->key.mac_plus_vpn.VPN));
           break;
#ifdef MEA_FWD_MAC_ONLY_SUPPORT
       case MEA_SE_FORWARDING_KEY_TYPE_DA :
           CLI_print("%02x:%02x:%02x:%02x:%02x:%02x %5s ",
               (unsigned char)(print_table_entry->key.mac.MAC.b[0]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[1]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[2]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[3]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[4]),
               (unsigned char)(print_table_entry->key.mac.MAC.b[5]),
               " ");
           break;
#endif
       case MEA_SE_FORWARDING_KEY_TYPE_MPLS_VPN :		   
           CLI_print("L1=%07d L2=%07d vpn=%05d mask%1d ",
               (print_table_entry->key.mpls_vpn.label_1),
               (print_table_entry->key.mpls_vpn.label_2),
               (print_table_entry->key.mpls_vpn.VPN),
               (print_table_entry->key.mpls_vpn.mask_type));
           break;

#ifdef PPP_MAC 
	   case MEA_SE_FORWARDING_KEY_TYPE_SA_PLUS_PPPOE_SESSION_ID:
		   CLI_print("%02x:%02x:%02x:%02x:%02x:%02x %5d ",
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[0]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[1]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[2]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[3]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[4]),
			   (unsigned char)(print_table_entry->key.mac_plus_pppoe_session_id.MAC.b[5]),
			   (print_table_entry->key.mac_plus_pppoe_session_id.PPPoE_session_id));
		   break;
#else
	   case MEA_SE_FORWARDING_KEY_TYPE_PPPOE_SESSION_ID_VPN:
		   CLI_print("Session %5d  VPN=%05d ",
			   (print_table_entry->key.PPPOE_plus_vpn.PPPoE_session_id),
			   (print_table_entry->key.PPPOE_plus_vpn.VPN));
		   break;
#endif

       case MEA_SE_FORWARDING_KEY_TYPE_VLAN_VPN :		   
           CLI_print("Vlan=%05d vpn=%05d    ",
               (print_table_entry->key.vlan_vpn.VLAN),
               (print_table_entry->key.vlan_vpn.VPN));
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN :
                    
           CLI_print("l=%7d vpn=%4d \n           MdL=%2d op=%3d um=%1d src=%4d mask=%d  ",
               print_table_entry->key.oam_vpn.label_1,
               print_table_entry->key.oam_vpn.VPN,
               print_table_entry->key.oam_vpn.MD_level,
               print_table_entry->key.oam_vpn.op_code,
               print_table_entry->key.oam_vpn.packet_is_untag_mpls,
               print_table_entry->key.oam_vpn.Src_port,
               print_table_entry->key.oam_vpn.mask_type);
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN:

           dstIp = print_table_entry->key.DstIPv4_plus_SrcIPv4_plus_vpn.DstIPv4;
           dstIp |= (globals_entry.Globals_Device.MC_ServerIp.DestIp& 0xff000000);


           srcIp = print_table_entry->key.DstIPv4_plus_SrcIPv4_plus_vpn.SrcIPv4;
           srcIp |= (globals_entry.Globals_Device.MC_ServerIp.sourceIp & 0xff000000);
           MEA_OS_inet_ntoa(MEA_OS_htonl(dstIp), dstIp_str, sizeof(dstIp_str));
           MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), srcIp_str, sizeof(srcIp_str));
           CLI_print("DstIPv4:%s \n           SrcIPv4:%s \n           vpn %4d                            ", 
               dstIp_str,
               srcIp_str,
               print_table_entry->key.DstIPv4_plus_SrcIPv4_plus_vpn.VPN);
           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
           
           dstIp = print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4;
           MEA_OS_inet_ntoa(MEA_OS_htonl(dstIp), dstIp_str, sizeof(dstIp_str));
           CLI_print("DstIPv4:%s %s\n           L4DstPort:%5d \n           vpn %4d                            ", 
               dstIp_str,
               mask_Ip_str[print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type],
               print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port,
               print_table_entry->key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN);

           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:
           srcIp = print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.IPv4;
           MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), srcIp_str, sizeof(srcIp_str));
           CLI_print("SrcIPv4:%s %s\n           L4SrcPort:%5d \n           vpn %4d                            ", 
               srcIp_str,
               mask_Ip_str[print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.mask_type],
               print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.L4port,
               print_table_entry->key.SrcIPv4_plus_L4_srcPort_plus_vpn.VPN);

           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IPV6_DEST_PLUS_L4_DEST_PORT_PLUS_VPN:
           
           
           buf[0] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] >> 24) & 0xff;
           buf[1] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] >> 16) & 0xff;
           buf[2] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] >> 8) & 0xff;
           buf[3] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[3] >> 0) & 0xff;

           buf[4] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] >> 24) & 0xff;
           buf[5] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] >> 16) & 0xff;
           buf[6] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] >> 8) & 0xff;
           buf[7] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[2] >> 0) & 0xff;

           buf[8] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] >> 24) & 0xff;
           buf[9] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] >> 16) & 0xff;
           buf[10] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] >> 8) & 0xff;
           buf[11] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[1] >> 0) & 0xff;

           buf[12] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] >> 24) & 0xff;
           buf[13] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] >> 16) & 0xff;
           buf[14] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] >> 8) & 0xff;
           buf[15] = (print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.IPv6[0] >> 0) & 0xff;

           if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
               CLI_print(">>>inet_ntop<<< \n");
           }

           CLI_print("DestIPv6: %s %s\n         L4DstPort:%5d \n           vpn %4d                            ",
               stripv6,
               mask_Ip_str[print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.mask_type],
               print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.L4port,
               print_table_entry->key.SE_DstIPv6_plus_L4DstPort_plus_vpn.VPN);

           break;
       case MEA_SE_FORWARDING_KEY_TYPE_IPV6_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN:


           buf[0] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[3] >> 24) & 0xff;
           buf[1] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[3] >> 16) & 0xff;
           buf[2] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[3] >> 8) & 0xff;
           buf[3] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[3] >> 0) & 0xff;

           buf[4] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[2] >> 24) & 0xff;
           buf[5] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[2] >> 16) & 0xff;
           buf[6] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[2] >> 8) & 0xff;
           buf[7] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[2] >> 0) & 0xff;

           buf[8] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[1] >> 24) & 0xff;
           buf[9] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[1] >> 16) & 0xff;
           buf[10] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[1] >> 8) & 0xff;
           buf[11] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[1] >> 0) & 0xff;

           buf[12] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0] >> 24) & 0xff;
           buf[13] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0] >> 16) & 0xff;
           buf[14] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0] >> 8) & 0xff;
           buf[15] = (print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.srcIPv6[0] >> 0) & 0xff;

           if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
               CLI_print(">>>inet_ntop<<< \n");
           }

           CLI_print("srcIPv6: %s %s\n         L4srcPort:%5d \n           vpn %4d                            ",
               stripv6,
               mask_Ip_str[print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.mask_type],
               print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.L4port,
               print_table_entry->key.SE_SrcIPv6_plus_L4_srcPort_plus_vpn.VPN);

           break;

       case MEA_SE_FORWARDING_KEY_TYPE_LAST :
       default: 
           CLI_print("Unknown              ");
           
           break;
    }
    if (settintCR){
        CLI_print("\n");
    }
    

return MEA_OK;

}



