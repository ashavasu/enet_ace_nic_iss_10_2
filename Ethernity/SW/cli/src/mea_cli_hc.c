/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_hc.h"




/************************************************************************/
/*       MEA_CLI_HC_Comp_Profile     create show delete               */
/************************************************************************/
static MEA_Status MEA_CLI_HC_Comp_Profile_create(int argc, char *argv[])
{
    MEA_HDC_Data_dbt entry;
    int i;
    MEA_Uint16  hcId=MEA_PLAT_GENERATE_NEW_ID;
    MEA_Uint32 index;
    MEA_Uint32  regId=0;
    MEA_Globals_Entry_dbt Globalentry;
    
    if (argc < 2) {
        return MEA_ERROR;
    }
    
    
    MEA_OS_memset(&entry,0,sizeof(entry));
    
    MEA_OS_memset(&Globalentry,0,sizeof(Globalentry));
    

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&Globalentry) !=MEA_OK) {

        return MEA_ERROR;
    }

    if(Globalentry.HC_Offset_Mode == MEA_TRUE){
        entry.start_offset=16;
    }
    

    for(i=0; i<argc ;i++){

        if(!MEA_OS_strcmp(argv[i],"-prof")){ 
            hcId = MEA_OS_atoi(argv[i+1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-start_offset")){ 
            entry.start_offset = MEA_OS_atoi(argv[i+1]);
        }
        
        if(!MEA_OS_strcmp(argv[i],"-reg0")){ 
            entry.bit_Of_byte_comp[0] = MEA_OS_atohex(argv[i+1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-reg1")){ 
            entry.bit_Of_byte_comp[1] = MEA_OS_atohex(argv[i+1]);
        }

        if(!MEA_OS_strcmp(argv[i],"-reg2")){
            regId=2;
            if(MEA_HD_NM_WORD == 4)
                entry.bit_Of_byte_comp[regId] = MEA_OS_atohex(argv[i+1]);
            else{
                CLI_print("Error %s not support \n",argv[i]);
                return MEA_OK;
            }

        }
        if(!MEA_OS_strcmp(argv[i],"-reg3")){ 
            regId=3;
            if(MEA_HD_NM_WORD == 4){
                entry.bit_Of_byte_comp[regId] = MEA_OS_atohex(argv[i+1]);
            }else{
                CLI_print("Error %s not support \n",argv[i]);
                return MEA_OK;
            }
        }

        if(!MEA_OS_strcmp(argv[i],"-i")){ 
            index = MEA_OS_atoi(argv[i+1]);
            if((index/32)>3){
                CLI_print("Error the offset of bit is out of range -i %d  max is 127\n",index);

            }else{
                if(index > MEA_HD_NM_WORD*32){
                   CLI_print("Error the index is out of the range\n");
                   return MEA_OK;
                   }

                entry.bit_Of_byte_comp[index/32] |= ( (0x00000001 << ((index)%32)));
            }

        }



    }
    

    if(MEA_API_HC_Comp_Create_Profile(MEA_UNIT_0,&hcId,&entry)!=MEA_OK){
        CLI_print("Error MEA_API_HC_Comp_Create_Profile failed on hcId %d\n",hcId);
        return MEA_OK;
    }


    CLI_print("Done Create: %d\n",hcId);
    return MEA_OK;
}

static MEA_Status MEA_CLI_HC_Comp_Profile_Show(int argc, char *argv[])
{

    MEA_Uint16  hcId ,start_hcid,end_hcid;
    MEA_Bool      exist;
    MEA_HDC_Data_dbt                   entry_po;
    MEA_Uint32 count=0;
    if (argc < 2) {
        return MEA_ERROR;
    }

    if(!MEA_OS_strcmp(argv[1],"all")){
        start_hcid=0;
        end_hcid=MEA_HC_DECOMP_MAX_PROF-1;
    } else {
        start_hcid=(MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_hcid=start_hcid;
        if ((MEA_API_HC_Comp_Profile_IsExist(MEA_UNIT_0,start_hcid,&exist)!=MEA_OK) ||
            (!exist)) {
                CLI_print(" ERROR: hc id %d is not exist \n",start_hcid);
                return MEA_OK;
        }
    }
    count=0;
    for(hcId=start_hcid; hcId <=end_hcid; hcId++){    
        if(MEA_API_HC_Comp_Profile_IsExist(MEA_UNIT_0,hcId,&exist)==MEA_OK){
            if (exist){

                if(MEA_API_HC_Comp_Get_Profile(MEA_UNIT_0,hcId,&entry_po )!=MEA_OK){
                    CLI_print(" ERROR: can't Delete hc id %d \n",hcId);
                }
                if(count== 0){
                    CLI_print(" Hc  start   Compressor profile \n");
                    CLI_print(" id  offset       value             \n");
                    CLI_print("---- ------  -----------------------\n");
                    count++;
                }
                CLI_print(" %3d %6d ",hcId,entry_po.start_offset); 
                CLI_print("reg0 0x%08x\n",entry_po.bit_Of_byte_comp[0]);
                CLI_print("            reg1 0x%08x\n",entry_po.bit_Of_byte_comp[1]);
                if(MEA_HD_NM_WORD == 4){
                CLI_print("            reg2 0x%08x\n",entry_po.bit_Of_byte_comp[2]);
                CLI_print("            reg3 0x%08x\n",entry_po.bit_Of_byte_comp[3]);
                 }
                CLI_print("---- ------ -----------------------\n");

            }
        }
    }


    return MEA_OK;
}

static MEA_Status MEA_CLI_HC_Comp_Profile_Delete(int argc, char *argv[])
{
    MEA_Uint16  hcId ,start_hcid,end_hcid;
    MEA_Bool      exist;
    if (argc < 2) {
        return MEA_ERROR;
    }

    if(!MEA_OS_strcmp(argv[1],"all")){
        start_hcid=0;
        end_hcid=MEA_HC_DECOMP_MAX_PROF-1;
    } else {
        start_hcid=(MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_hcid=start_hcid;
        if ((MEA_API_HC_Comp_Profile_IsExist(MEA_UNIT_0,start_hcid,&exist)!=MEA_OK) ||
            (!exist)) {
                CLI_print(" ERROR: hc id %d is not exist \n",start_hcid);
                return MEA_OK;
        }
    }

    for(hcId=start_hcid; hcId <=end_hcid; hcId++){    
        if(MEA_API_HC_Comp_Profile_IsExist(MEA_UNIT_0,hcId,&exist)==MEA_OK){
            if (exist){

                if(MEA_API_HC_Comp_Delete_Profile(MEA_UNIT_0,hcId )!=MEA_OK){
                    CLI_print(" ERROR: can't Delete hc id %d \n",hcId);
                }
            }
        }
    }



    return MEA_OK;
}


/************************************************************************/
/*       MEA_CLI_HC_Decomp_Profile     create show delete               */
/************************************************************************/
static MEA_Status MEA_CLI_HC_Decomp_Profile_create(int argc, char *argv[])
{
    MEA_HDC_Data_dbt entry;
    int i;
    MEA_Uint16  hcId=MEA_PLAT_GENERATE_NEW_ID;
    MEA_Uint32 index;
    MEA_Uint32 regId=0;
    MEA_Globals_Entry_dbt Globalentry;

    MEA_OS_memset(&entry,0,sizeof(entry));
    if (argc < 2) {
      return MEA_ERROR;
    }
    MEA_OS_memset(&Globalentry,0,sizeof(Globalentry));


    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&Globalentry) !=MEA_OK) {

        return MEA_ERROR;
    }

    if(Globalentry.HC_Offset_Mode == MEA_TRUE){
        entry.start_offset=16;
    }

  
    for(i=0; i<argc ;i++){

        if(!MEA_OS_strcmp(argv[i],"-start_offset")){ 
            entry.start_offset = MEA_OS_atoi(argv[i+1]);
        }
        if(!MEA_OS_strcmp(argv[i],"-prof")){ 
          hcId = MEA_OS_atoi(argv[i+1]);
      }
      if(!MEA_OS_strcmp(argv[i],"-reg0")){ 
          entry.bit_Of_byte_comp[0] = MEA_OS_atohex(argv[i+1]);
      }
      if(!MEA_OS_strcmp(argv[i],"-reg1")){ 
          entry.bit_Of_byte_comp[1] = MEA_OS_atohex(argv[i+1]);
      }
      if(!MEA_OS_strcmp(argv[i],"-reg2")){ 
          regId=2;
          if(MEA_HD_NM_WORD == 4)
            entry.bit_Of_byte_comp[regId] = MEA_OS_atohex(argv[i+1]);
          else{
              CLI_print("Error %s not support \n",argv[i]);
              return MEA_OK;
          }
      }
      if(!MEA_OS_strcmp(argv[i],"-reg3")){ 
         regId=3;
          if(MEA_HD_NM_WORD == 4)
          entry.bit_Of_byte_comp[regId] = MEA_OS_atohex(argv[i+1]);
         {
             CLI_print("Error %s not support \n",argv[i]);
             return MEA_OK;
         }
      }

      if(!MEA_OS_strcmp(argv[i],"-i")){ 
          index = MEA_OS_atoi(argv[i+1]);
          if((index/32)>3){
              CLI_print("Error the offset of bit is out of range -i %d  max is 127\n",index);
              
          }else{
                entry.bit_Of_byte_comp[index/32] |= ( (0x00000001 << ((index)%32)));
          }

      }
      


  }


    if(MEA_API_HC_Decomp_Create_Profile(MEA_UNIT_0,&hcId,&entry)!=MEA_OK){
        CLI_print("Error MEA_API_HC_Decomp_Create_Profile failed on hcId %d\n",hcId);
        return MEA_OK;
    }


  CLI_print("Done Create: %d\n",hcId);
    return MEA_OK;
}

static MEA_Status MEA_CLI_HC_Decomp_Profile_Show(int argc, char *argv[])
{
    
    MEA_Uint16  hcId ,start_hcid,end_hcid;
    MEA_Bool      exist;
    MEA_HDC_Data_dbt                   entry_po;
    MEA_Uint32 count=0;
    if (argc < 2) {
        return MEA_ERROR;
    }

    if(!MEA_OS_strcmp(argv[1],"all")){
        start_hcid=0;
        end_hcid=MEA_HC_DECOMP_MAX_PROF-1;
    } else {
        start_hcid=(MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_hcid=start_hcid;
        if ((MEA_API_HC_Decomp_Profile_IsExist(MEA_UNIT_0,start_hcid,&exist)!=MEA_OK) ||
            (!exist)) {
                CLI_print(" ERROR: hc id %d is not exist \n",start_hcid);
                return MEA_OK;
        }
    }
    count=0;
    for(hcId=start_hcid; hcId <=end_hcid; hcId++){    
        if(MEA_API_HC_Decomp_Profile_IsExist(MEA_UNIT_0,hcId,&exist)==MEA_OK){
            if (exist){

                if(MEA_API_HC_Decomp_Get_Profile(MEA_UNIT_0,hcId,&entry_po )!=MEA_OK){
                    CLI_print(" ERROR: can't Delete hc id %d \n",hcId);
                }
                if(count== 0){
                     CLI_print(" Hc De-compressor profile \n");
                     CLI_print(" id     value             \n");
                     CLI_print("---- -----------------------\n");
                     count++;
                }
                 CLI_print(" %3d reg0 0x%08x\n",hcId,entry_po.bit_Of_byte_comp[0]);
                 CLI_print("     reg1 0x%08x\n",entry_po.bit_Of_byte_comp[1]);
                 if(MEA_HD_NM_WORD == 4) {
                    CLI_print("     reg2 0x%08x\n",entry_po.bit_Of_byte_comp[2]);
                    CLI_print("     reg3 0x%08x\n",entry_po.bit_Of_byte_comp[3]);
                 }
                 CLI_print("---- -----------------------\n");

            }
        }
    }

    
    return MEA_OK;
}

static MEA_Status MEA_CLI_HC_Decomp_Profile_Delete(int argc, char *argv[])
{
    MEA_Uint16  hcId ,start_hcid,end_hcid;
    MEA_Bool      exist;
    if (argc < 2) {
        return MEA_ERROR;
    }

    if(!MEA_OS_strcmp(argv[1],"all")){
        start_hcid=0;
        end_hcid=MEA_HC_DECOMP_MAX_PROF-1;
    } else {
        start_hcid=(MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_hcid=start_hcid;
        if ((MEA_API_HC_Decomp_Profile_IsExist(MEA_UNIT_0,start_hcid,&exist)!=MEA_OK) ||
            (!exist)) {
                CLI_print(" ERROR: hc id %d is not exist \n",start_hcid);
                return MEA_OK;
        }
    }

    for(hcId=start_hcid; hcId <=end_hcid; hcId++){    
        if(MEA_API_HC_Decomp_Profile_IsExist(MEA_UNIT_0,hcId,&exist)==MEA_OK){
            if (exist){
               
                if(MEA_API_HC_Decomp_Delete_Profile(MEA_UNIT_0,hcId )!=MEA_OK){
                    CLI_print(" ERROR: can't Delete hc id %d \n",hcId);
                }
            }
        }
    }



    return MEA_OK;
}


static MEA_Status MEA_CLI_HC_Comp_l2l3_Protocol_Set(int argc, char *argv[])
{
    
    MEA_Uint32 numOfArgInCommand;
    char * ch;
    int i,len;

    MEA_HC_key_proto_t             key;
    MEA_HC_L2L3_CompInfo_dbt       entry;

    if (argc < 3) {
        return MEA_ERROR;
    }
    MEA_OS_memset(&key, 0, sizeof(key));
    MEA_OS_memset(&entry, 0, sizeof(entry));



    for(i=1; i<argc ;i++){

        if(!MEA_OS_strcmp(argv[i],"-key")){ 
            numOfArgInCommand = 2;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);

            key.L2Type = MEA_OS_atoi(argv[i+1]);
            key.L3Type = MEA_OS_atoi(argv[i+2]);
        }
        if(!MEA_OS_strcmp(argv[i],"-data")){ 
            numOfArgInCommand = 2;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);

            entry.enable           = MEA_OS_atoi(argv[i+1]);
            entry.Compress_Prof_id = MEA_OS_atoi(argv[i+2]);
        }





    i+=numOfArgInCommand;
    
    }

   if(  MEA_API_HC_Set_Compress_Protocol(MEA_UNIT_0,
                                         &key,
                                         &entry)!=MEA_OK){

   CLI_print("Error: MEA_API_HC_Set_Compress_Protocol\n");
   }

    CLI_print("Done Create: \n");
    return MEA_OK;

}
static MEA_Status MEA_CLI_HC_Comp_l2l3_Protocol_Show(int argc, char *argv[])
{

   
    
   
    MEA_Uint32 count;
    MEA_Bool found = MEA_FALSE;
    MEA_Bool all_flag = MEA_FALSE;

    MEA_HC_key_proto_t            key;
    MEA_HC_L2L3_CompInfo_dbt      entry; 

    /* Check minimum number of parameters */
    if (argc < 3) {
        return MEA_ERROR;
    }



    if (MEA_OS_strcmp(argv[1],"all") != 0 || MEA_OS_strcmp(argv[2],"all") != 0 ) {
        
        key.L2Type              = MEA_OS_atoi(argv[1]);
        key.L3Type              = MEA_OS_atoi(argv[2]);
        found = MEA_TRUE;
        all_flag = MEA_FALSE;

    } else {
        key.L2Type           =0;
        key.L3Type           =0;
        if(MEA_API_HC_Comp_GetFirst_Protocol(MEA_UNIT_0,
            &key,
            NULL, 
            &found)!=MEA_OK){
                CLI_print("Error: MEA_API_HC_Comp_GetFirst_Protocol failed \n");
                return MEA_OK;
        }

        all_flag = MEA_TRUE;
    }

   



    count=0;
    while (found) {

           MEA_OS_memset(&entry,0,sizeof(entry));
        if(MEA_API_HC_Get_Compress_Protocol(MEA_UNIT_0,&key,&entry)!=MEA_OK){
            CLI_print("Error key.L2Type=%d,key.L3Type=%d  failed to Get info\n",key.L2Type,key.L3Type);
            return MEA_OK;
        }


        if(count++ ==0 || (count % 32)== 0 ){
            CLI_print(" Compress L2L3 Info \n");
            CLI_print(" l2  L3   ProfId    \n");
            CLI_print("---- ---- ------ \n");
            //12345678901234567890123456789012345678901234567890123456789012345678901234567890
        }

        CLI_print("%4d %4d %6d \n",key.L2Type,key.L3Type,entry.Compress_Prof_id);
        
        CLI_print("\n");
        if (!all_flag) {
            break;
        }

        if (MEA_API_HC_Comp_GetNext_Protocol(MEA_UNIT_0,
            &key,
            NULL,
            &found) != MEA_OK) {
                CLI_print("Error: MEA_API_HC_Comp_GetNext_Protocol failed \n");
                return MEA_OK;
        }
    }

    if(!count){
        CLI_print("l2l3 protocol comp table is empty\n");
    }else{
        CLI_print("---- ---- ------ \n");
        CLI_print("Total of l2l3 protocol valid % d \n",count);
    }


    return MEA_OK;
}



/************************************************************************/
/* RMON HC                                                              */
/************************************************************************/




static MEA_Status MEA_CLI_Collect_Counters_HC_RMON(int argc, char *argv[])
{

    MEA_HDC_t hcid,start_hcid,end_hcid;
    char buf[500];
    char *up_ptr;
    MEA_Status retVal;

#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
    MEA_Bool ffff=MEA_TRUE; 
    struct timeval tvBegin, tvEnd, tvDiff;

    time_t in_t;
    time_t out_t; 	 
#endif

    if (argc != 2)
        return MEA_ERROR;

    if (MEA_OS_strcmp(argv[1], "all") == 0){
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME) ) && !defined(__KERNEL__)
        if (ffff==MEA_TRUE){

            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Rmon time start\n");
            /*get the time*/
            time(&in_t);
            gettimeofday(&tvBegin, NULL);

            ffff=MEA_FALSE;

        } 
#endif        
         retVal=MEA_API_Collect_Counters_HC_RMONs(MEA_UNIT_0);


#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
        time(&out_t);
        gettimeofday(&tvEnd, NULL);
        MEA_OS_timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
        CLI_print("Begin    ");
        MEA_OS_timeval_print(&tvBegin);
        CLI_print("End      ");
        MEA_OS_timeval_print(&tvEnd);
        CLI_print("Diff = %ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);


#endif    
        return retVal;
    }else{
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            start_hcid = MEA_OS_atoi(buf);
            end_hcid   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            hcid=start_hcid = end_hcid = MEA_OS_atoi(buf);
        }
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME) ) && !defined(__KERNEL__)
        if (ffff==MEA_TRUE){

            MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"Hc Rmon time start\n");
            /*get the time*/
            time(&in_t);
            gettimeofday(&tvBegin, NULL);

            ffff=MEA_FALSE;

        } 
#endif    
        for(hcid=start_hcid;hcid<=end_hcid;hcid++){
            /* Check for valid port parameter */
            if(hcid >=MEA_HC_CLASSIFIER_MAX_OF_HCID){
                MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                    "%s - RMON (hcid=%d) is out of range\n",
                    __FUNCTION__,hcid);
                return MEA_ERROR;
            }

            if(MEA_API_Collect_Counters_HC_RMON(MEA_UNIT_0,
                hcid,
                NULL) !=MEA_OK){
                    return MEA_ERROR;

            }
        }//for
#if (defined(MEA_OS_LINUX) && defined(MEA_TEST_MEASURE_TIME)) && !defined(__KERNEL__)
        time(&out_t);
        gettimeofday(&tvEnd, NULL);
        MEA_OS_timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
        CLI_print("Begin    ");
        MEA_OS_timeval_print(&tvBegin);
        CLI_print("End      ");
        MEA_OS_timeval_print(&tvEnd);
        CLI_print("Diff = %ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);


#endif    

    }//else


    return MEA_OK;
}


MEA_Status MEA_CLI_Clear_Counters_HC_RMON(int argc, char *argv[])
{
    MEA_HDC_t port,start_port,end_port;
    char buf[500];
    char *up_ptr;


    if(argc<2) return MEA_ERROR;

    if(!MEA_OS_strcmp(argv[1],"all"))
        return MEA_API_Clear_Counters_HC_RMONs(MEA_UNIT_0);
    else{ 
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            start_port = MEA_OS_atoi(buf);
            end_port   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            port=start_port = end_port = MEA_OS_atoi(buf);
        }

        for(port=start_port;port<=end_port;port++){

           

            if(MEA_API_Clear_Counters_HC_RMON(MEA_UNIT_0,port)!=MEA_OK){
                return MEA_ERROR;
            }
        }
    }
    return MEA_OK;
}

#define HC_RMON_SHOW_RX 1

static MEA_Status MEA_CLI_Show_Counters_HC_RMON(int argc, char *argv[])
{
    MEA_HDC_t hcid, first_hcid, last_hcid;
    MEA_Counters_RMON_HC_dbt entry;
    MEA_Bool details = MEA_FALSE;
    MEA_Bool Show_rate=MEA_FALSE;
    MEA_Bool flag_Show_rate = MEA_FALSE;
    MEA_Bool show_title = MEA_TRUE;
   
    char buf[500];
    char *up_ptr;




    MEA_Uint16 i;


    if (argc > 3 || argc < 2)
        return MEA_ERROR;

    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        first_hcid = 1;
        last_hcid = MEA_HC_CLASSIFIER_MAX_OF_HCID-1;
    } else {

        //last_port = first_port = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_hcid = MEA_OS_atoi(buf);
            last_hcid   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            hcid=first_hcid = last_hcid = MEA_OS_atoi(buf);
        }


        
    }

    for (i = 0; i < argc; i++) {
        if (MEA_OS_strcmp(argv[i], "-d") == 0) {
            details = MEA_TRUE;
        }
        if (MEA_OS_strcmp(argv[i], "-rate") == 0) {
            flag_Show_rate=MEA_TRUE;
        }

    }



    for (hcid = first_hcid; hcid <= last_hcid; hcid++) {

        /* Check for valid port parameter */
       MEA_OS_memset(&entry,0,sizeof(entry));

        if (MEA_API_Get_Counters_HC_RMON(MEA_UNIT_0, hcid, &entry) != MEA_OK) {
                return MEA_ERROR;
        }



        /************************************************************/
        if (show_title) {
#ifdef MEA_COUNTER_64_SUPPORT

            
            if (HC_RMON_SHOW_RX == 1){
                CLI_print("hcid Counter Name                      RX                TX           \n"
                          "                                DeCompress            Compress        \n"
                          "---- ---------------------- -------------------- --------------------\n");
            }
            else{
                CLI_print("hcid Counter Name                   TX           \n"
                          "                                 Compress        \n"
                          "---- ---------------------- --------------------\n");
            }


#endif
            if (details)
                show_title = MEA_TRUE;
            else
                show_title = MEA_FALSE;
            if(flag_Show_rate)
                Show_rate = MEA_TRUE;
            else
                Show_rate = MEA_FALSE;
        }
        CLI_print("%4d %s ", hcid, "Total           Pkts  ");

#ifdef MEA_COUNTER_64_SUPPORT
        if (HC_RMON_SHOW_RX == 1)
        CLI_print("%20llu ", entry.Rx_Pkts.val);
#else
        if (HC_RMON_SHOW_RX == 1)
        CLI_print("%10lu ", entry.Rx_Pkts.s.lsw);
#endif


#ifdef MEA_COUNTER_64_SUPPORT
        CLI_print("%20llu\n", entry.Tx_Pkts.val);
#endif
        CLI_print("     %s ", "Total           Bytes ");
#ifdef MEA_COUNTER_64_SUPPORT
        if (HC_RMON_SHOW_RX == 1)
        CLI_print("%20llu ", entry.Rx_Bytes.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
        CLI_print("%20llu\n", entry.Tx_Bytes.val);
#endif
        CLI_print("     %s ", "CRC Error       Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
        if (HC_RMON_SHOW_RX == 1)
        CLI_print("%20llu ", entry.Rx_DeCompressedCRC_Err.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
        CLI_print("%20llu\n", entry.Tx_CompressedCRC_Err.val);
#endif
 



        if (details == MEA_TRUE) {

            CLI_print("     %s ", "Learn           Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ", entry.Rx_learn_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
            CLI_print("%20llu\n", entry.Tx_learn_Pkts.val);
#endif
            CLI_print("     %s ", "Compressed      Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ", entry.Rx_DeCompressed_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
            CLI_print("%20llu\n", entry.Tx_Compressed_Pkts.val);
#endif
            CLI_print("     %s ", "No-Compressed   Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ", entry.Rx_NoDeCompressed_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
            CLI_print("%20llu\n", entry.Tx_NoCompressed_Pkts.val);
#endif
            CLI_print("     %s ", "64        Octet Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ", entry.Rx_64Octets_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
            CLI_print("%20llu\n", entry.Tx_64Octets_Pkts.val);
#endif
            CLI_print("     %s ", "65-127    Octet Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ",
                entry.Rx_65to127Octets_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
            CLI_print("%20llu\n",
                entry.Tx_65to127Octets_Pkts.val);
#endif
            CLI_print("     %s ", "128-255   Octet Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ",
                entry.Rx_128to255Octets_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
            CLI_print("%20llu\n",
                entry.Tx_128to255Octets_Pkts.val);
#endif
            CLI_print("     %s ", "256-511   Octet Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ",
                entry.Rx_256to511Octets_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
            
            CLI_print("%20llu\n",
                entry.Tx_256to511Octets_Pkts.val);
#endif
            CLI_print("     %s ", "512-1023  Octet Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ",
                entry.Rx_512to1023Octets_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT

            CLI_print("%20llu\n",
                entry.Tx_512to1023Octets_Pkts.val);
#endif
            CLI_print("     %s ", "1024-1518 Octet Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ",
                entry.Rx_1024to1518Octets_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT

            CLI_print("%20llu\n",
                entry.Tx_1024to1518Octets_Pkts.val);
#endif
            CLI_print("     %s ", "1519-2047 Octet Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ",
                entry.Rx_1519to2047Octets_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
            
            CLI_print("%20llu\n",
                entry.Tx_1519to2047Octets_Pkts.val);
#endif
            CLI_print("     %s ", "2048-Max  Octet Pkts  ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu ",
                entry.Rx_2048toMaxOctets_Pkts.val);
#endif
#ifdef MEA_COUNTER_64_SUPPORT
            CLI_print("%20llu\n",
                entry.Tx_2048toMaxOctets_Pkts.val);
#endif
             CLI_print("     %s ", "Drop            Bytes ");
#ifdef MEA_COUNTER_64_SUPPORT
             if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu \n", entry.Rx_Drop_Bytes.val);
#endif
            CLI_print("\n     %s ", "Actual          Bytes ");
#ifdef MEA_COUNTER_64_SUPPORT
            if (HC_RMON_SHOW_RX == 1)
                CLI_print("                     %20llu\n", entry.Tx_actual_bytes.val);
            else
                CLI_print("%20llu \n", entry.Tx_actual_bytes.val);

#endif

        }		// details 
        if(Show_rate){
            CLI_print("     %s ", "PacketRate            ");
            if (HC_RMON_SHOW_RX == 1)
                CLI_print("%20llu %20llu\n", entry.Rx_ratePacket.val, entry.Tx_ratePacket.val);
            else
                CLI_print("%20llu ", entry.Tx_ratePacket.val);

            CLI_print("\n     %s ", "Rate              bps ");
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("%20llu %20llu\n", entry.Rx_rate.val*8,entry.Tx_rate.val*8);
            else
                CLI_print("%20llu ", entry.Tx_rate.val * 8);

            CLI_print("\n     %s ", "Actual          ratio ");
            if (HC_RMON_SHOW_RX == 1)
            CLI_print("                     %20d \n", entry.Tx_Compress_ratio);
            else
                CLI_print("%20d \n", entry.Tx_Compress_ratio);

            
        }
        if (HC_RMON_SHOW_RX == 1)
            CLI_print("-----------------------------------------------------------------------\n");
        else
            CLI_print("---- ---------------------- --------------------\n");
    }

    return MEA_OK;

}


/************************************************************************/
/* class                                                                     */
/************************************************************************/
static MEA_Status MEA_CLI_HC_Comp_config_show(int argc, char *argv[])
{

   MEA_Uint32 size;
   MEA_Uint32 count, countX;
    int i;

    MEA_HDC_t hcid, first_hcid, last_hcid;
    MEA_hdc_Get_info_dbt    entry;
    MEA_Bool flagState=MEA_FALSE;
    MEA_Bool flagheader=MEA_FALSE;
    MEA_HC_key_proto_t            key;
    MEA_HC_L2L3_CompInfo_dbt      entryL2l3;
    MEA_HDC_Data_dbt              entryProf;
    MEA_Uint32 var;

    char buf[500];
    char *up_ptr;

    

	if (argc > 3 || argc < 2) {
		return MEA_ERROR;
	}
    if (MEA_OS_strcmp(argv[2], "all") == 0) {
        first_hcid = 1;
        last_hcid = MEA_HC_CLASSIFIER_MAX_OF_HCID-1;
    } else {

        //last_port = first_port = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[2]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_hcid = MEA_OS_atoi(buf);
            last_hcid   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            hcid=first_hcid = last_hcid = MEA_OS_atoi(buf);
        }



    }

    
    MEA_OS_memset(&entry,0,sizeof(entry));
   
    size = MEA_HC_CLASSIFIER_MAX_KEY_DATA * sizeof(MEA_Uint8   );
    if (size != 0) {
         entry.header_data.header = (MEA_Uint8   *)MEA_OS_malloc(size);
         if ( entry.header_data.header    == NULL) {
             MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,
                 "%s -  entry.header_data.header    failed (size=%d)\n",
                 __FUNCTION__,size);
             return MEA_ERROR;
         }
         MEA_OS_memset(&( entry.header_data.header[0]),0,size);
    }
    
   

      




    for(i=1; i<argc ;i++){
        if(!MEA_OS_strcmp(argv[i],"control")){ 
            
            flagState=MEA_TRUE;
        }
        if(!MEA_OS_strcmp(argv[i],"header")){ 

            flagheader=MEA_TRUE;
        }


        

    }

    count=0;

    for (hcid = first_hcid; hcid <= last_hcid; hcid++) {
       
   
   

            if(flagState){
                entry.type = MEA_HDC_GET_CONTROL;
                count++;
                if(hcid==first_hcid){
                     CLI_print("   Header Compression Control\n");
                     CLI_print("HcId Type         Learn count \n");
                     CLI_print("---- ------------ ----- ----- \n");

                }

                if( MEA_API_HC_Get_Compress_Config_Info(MEA_UNIT_0,hcid,&entry)!=MEA_OK){
                    CLI_print("Error: MEA_API_HC_Get_Compress_Config_Info\n");
                    return MEA_ERROR;
                }
                CLI_print("%4d %11s %5s %5d\n",hcid,
                    ((entry.status.type == MEA_HDC_TYPE_COMPRESS_DISABLE) ? "Comp-Disable" : ((entry.status.type == MEA_HDC_TYPE_COMPRESS_ENABLE) ? "Comp-Enable " : "ERROR") ),
                        MEA_STATUS_STR(entry.status.LearnEnable),
                        entry.status.LearnCount);



            }
        if(flagheader){
            entry.type = MEA_HDC_GET_INFO;
            
              MEA_OS_memset(&( entry.header_data.header[0]),0,size);
            if( MEA_API_HC_Get_Compress_Config_Info(MEA_UNIT_0,hcid,&entry)!=MEA_OK){
                CLI_print("Error: MEA_API_HC_Get_Compress_Config_Info\n");
                return MEA_ERROR;
            }
            
            if(entry.header_valid == MEA_TRUE){
                /*get the l2l3 profile */
                
                MEA_OS_memset(&(key), 0, sizeof(key));
                MEA_OS_memset(&(entryL2l3), 0, sizeof(entryL2l3));

                key.L2Type = entry.header_data.L2Type;
                key.L3Type = entry.header_data.L3Type;
             
                if (MEA_API_HC_Get_Compress_Protocol(MEA_UNIT_0,
                    &key,
                    &entryL2l3) != MEA_OK){
                    CLI_print("Error: MEA_API_HC_Get_Compress_Protocol\n");

                }
                if (MEA_API_HC_Comp_Get_Profile(MEA_UNIT_0, (MEA_Uint16)entryL2l3.Compress_Prof_id, &entryProf) != MEA_OK){
                    CLI_print("Error: MEA_API_HC_Comp_Get_Profile for profile %d\n", entryL2l3.Compress_Prof_id);
                    return MEA_ERROR;
                }
                

                if(count == 0){
                    CLI_print("   Header Compression Header\n");
                    CLI_print("---- ---- ---- ----- ----- \n");

                }
            count++;


            CLI_print("Hcid %4d Vp %2d (L2 %2d L3 %2d) -->profId=%d\n",hcid,
                        entry.header_data.vp,        
                        entry.header_data.L2Type,
                        entry.header_data.L3Type, entryL2l3.Compress_Prof_id);
           CLI_print("Header\n");
           countX = 0;
           for(i=4;i<((MEA_HC_CLASSIFIER_MAX_KEY_WIDTH/32)*4 ); i++){
//               if (i<=3)
//                   continue;

               var = 0;
               if (((i-4) / 32) == 0){
                   var = entryProf.bit_Of_byte_comp[0];
                   var = ((var >> (i-4)) & 0x1);
               }
               else{
                   var = entryProf.bit_Of_byte_comp[1];
                   var = ((var >> (i-4 - 32)) & 0x1);
               }
               if (var)
               CLI_print("%02x ",  entry.header_data.header[i]);
               else
                   CLI_print("-- ");
               countX++;
               if ((countX) % 8 == 0) {
                   MEA_OS_printf("\n");
                   
               }
            }//for
             

           CLI_print("\n");
        }
    
    
    
    }
    
    }
    if(count != 0 )
     CLI_print("Done: \n");
    else
        CLI_print("empty \n");

    return MEA_OK;

}


static MEA_Status MEA_CLI_HC_Comp_Config_Set(int argc, char *argv[])
{

   int numOfArgInCommand;
    char * ch;
    int i,len;

  MEA_HDC_t hcId;
  MEA_HDC_control_dbt    entry;
  MEA_Bool flagState=MEA_FALSE;

    if (argc < 2) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&entry,0,sizeof(entry));
    
    hcId= MEA_OS_atoi(argv[1]);

    for(i=1; i<argc ;i++){
        numOfArgInCommand=0;
        if(!MEA_OS_strcmp(argv[i],"-control")){ 
            numOfArgInCommand = 4;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);
            entry.type                              = MEA_OS_atoi(argv[i+1]);
            entry.LearnEnable                       = MEA_OS_atoi(argv[i+2]);
            entry.LearnCount                        = MEA_OS_atoi(argv[i+3]);
            entry.InvalidateCount                   = MEA_OS_atoi(argv[i+4]);

            flagState=MEA_TRUE;
        }


        

    }

    if(flagState){
        if( MEA_API_HC_Set_Compress_Config(MEA_UNIT_0,hcId,&entry)!=MEA_OK){
            CLI_print("Error: MEA_API_HC_Set_Compress_Config hcid %d\n",hcId);
        }
    }

    CLI_print("Done: \n");
    return MEA_OK;

}


MEA_Status MEA_CLI_HC_AddDebugCmds(void)
{

    if (MEA_HC_SUPPORT == MEA_TRUE){
        /**********************************************************/
        /*                   l2l3 Compressor                      */
        /**********************************************************/
        CLI_defineCmd("MEA hc Compress l2l3 set",
        (CmdFunc)MEA_CLI_HC_Comp_l2l3_Protocol_Set,
        "set   compressor Profile(s)\n",
        "set   key data \n"
        "---------------------------------------------------\n"
        "    -key <l2><l3> -  \n"
        "    -data  1 <profId>\n"
        "---------------------------------------------------\n"
        "Examples: - set -key 1 2 -data 1 1 \n"
        "          - set -key 1 2 -data 1 1 \n");

    CLI_defineCmd("MEA hc Compress l2l3 show",
        (CmdFunc)MEA_CLI_HC_Comp_l2l3_Protocol_Show,
        "Show   compress Profile(s)\n",
        "show <all><all> \n"
        "---------------------------------------------------\n"
        "    <l2>|<l3> - all or specific Compress id \n"

        "---------------------------------------------------\n"
        "Examples: - show all all\n"
        "          - show 1 0 \n");
    /**********************************************************/
    /*                    Compress config                   */
    /**********************************************************/

    CLI_defineCmd("MEA hc Compress config set",
        (CmdFunc)MEA_CLI_HC_Comp_Config_Set,
        "Set  compressor  config \n",
        "--------------------------------------------------------------------------------\n"
        //01234567890123456789012345678901234567890123456789012345678901234567890123456789
        "Usage : set  <hcid> [option]\n"
        "--------------------------------------------------------------------------------\n"
        //01234567890123456789012345678901234567890123456789012345678901234567890123456789
        " option\n"
        " -control <type> <learnEnable><learnCount><InvalidateCount>\n"
        "           <type>  0 -disable 1 enable \n"
        "           <learnEnable> 0 -disable 1 enable \n"
        "           <learnCount>  number of packet for learn flow\n"
        "           <InvalidateCount>  number of packet invalidate the flow\n"

        //" -dataHader \n"
        "--------------------------------------------------------------------------------\n"
        "Examples: config set 1  \n"
        "    \n");


    CLI_defineCmd("MEA hc Compress config show",
        (CmdFunc)MEA_CLI_HC_Comp_config_show,
        "Show  compressor  config \n",
        "--------------------------------------------------------------------------------\n"
        //01234567890123456789012345678901234567890123456789012345678901234567890123456789
        "Usage : show  config [option] <hcid/all> \n"
        "--------------------------------------------------------------------------------\n"
        //01234567890123456789012345678901234567890123456789012345678901234567890123456789
        "   <all/1> [potion] \n"
        "option\n"
        "  control show the compress control\n"
        "  header show the header data \n"

        //" -dataHader \n"
        "--------------------------------------------------------------------------------\n"
        "Examples: config show control all\n"
        "          config show header  1  \n"
        "          config show header  1:5  \n"

        "    \n");





    CLI_defineCmd("MEA hc Compress profile create",
        (CmdFunc)MEA_CLI_HC_Comp_Profile_create,
        "Create    Compress profile\n",
        "--------------------------------------------------------------------------------\n"
        //01234567890123456789012345678901234567890123456789012345678901234567890123456789
        "Usage : Create  <-prof id> -i <index set comp>\n"
        "--------------------------------------------------------------------------------\n"
        //01234567890123456789012345678901234567890123456789012345678901234567890123456789
        " option\n"
        "    -start_offset<value>          \n"
        "    -reg0 <hex>                   \n"
        "    -reg1 <hex>                   \n"
        "    -reg2 <hex>                   \n"
        "    -reg3 <hex>                   \n"
        "    -prof <id>                    \n"
        "     -i <index>                   \n"
        "--------------------------------------------------------------------------------\n"
        "Examples: -prof 1    -reg0  0xff000000 -reg1  0x00000000  -start_offset 16\n"
        "          -prof 2    -i 16 -i 17\n"
        "    \n");

    CLI_defineCmd("MEA hc Compress profile show",
        (CmdFunc)MEA_CLI_HC_Comp_Profile_Show,
        "Show   Compressor Profile(s)\n",
        "show all|<Id> \n"
        "---------------------------------------------------\n"
        "    all|<Id> - all or specific Compressor id \n"

        "---------------------------------------------------\n"
        "Examples: - show all \n"
        "          - show 1 \n");

    CLI_defineCmd("MEA hc Compress profile delete",
        (CmdFunc)MEA_CLI_HC_Comp_Profile_Delete,
        "Delete compressor Profile(s)\n",
        "delete all|<id>\n"
        "-----------------------------------------------------------------\n"
        "    all|<id> - delete specific Compress <id> profile or all \n"
        "-----------------------------------------------------------------\n"
        " Examples: - delete all\n"
        "           - delete 2\n");
}

    
    /**********************************************************/
    /*                  DeCompressor                          */
    /**********************************************************/
    if (MEA_HDEC_SUPPORT){
        CLI_defineCmd("MEA hc DeCompress profile create",
            (CmdFunc)MEA_CLI_HC_Decomp_Profile_create,
            "Create    De-compressor profile\n",
            "--------------------------------------------------------------------------------\n"
            //01234567890123456789012345678901234567890123456789012345678901234567890123456789
            "Usage : Create  <-prof id> -i <index set comp>\n"
            "--------------------------------------------------------------------------------\n"
            //01234567890123456789012345678901234567890123456789012345678901234567890123456789
            " option\n"
            "    -start_offset<value>          \n"
            "    -reg0 <hex>                   \n"
            "    -reg1 <hex>                   \n"
            "    -reg2 <hex>                   \n"
            "    -reg3 <hex>                   \n"
            "    -prof <id>                    \n"
            "     -i <index>                   \n"
            "--------------------------------------------------------------------------------\n"
            "Examples: -prof 1    -reg0  0xff000000 -reg1  0x00000000 -start_offset 16\n"
            "          -prof 2    -i 16 -i 17\n"
            "    \n");

        CLI_defineCmd("MEA hc DeCompress profile show",
            (CmdFunc)MEA_CLI_HC_Decomp_Profile_Show,
            "Show   De-compressor Profile(s)\n",
            "show all|<Id> \n"
            "---------------------------------------------------\n"
            "    all|<Id> - all or specific De-compressor id \n"

            "---------------------------------------------------\n"
            "Examples: - show all \n"
            "          - show 1 \n");

        CLI_defineCmd("MEA hc DeCompress profile delete",
            (CmdFunc)MEA_CLI_HC_Decomp_Profile_Delete,
            "Delete De-compressor Profile(s)\n",
            "delete all|<id>\n"
            "-----------------------------------------------------------------\n"
            "    all|<id> - delete specific De-Compress <id> profile or all \n"
            "-----------------------------------------------------------------\n"
            " Examples: - delete all\n"
            "           - delete 2\n");
    }

    if (MEA_HC_SUPPORT){
        /************************************************************************/
        /* RMON HC                                                                     */
        /************************************************************************/
        CLI_defineCmd("MEA hc Counters rmon collect",
            (CmdFunc)MEA_CLI_Collect_Counters_HC_RMON,
            "Collect RMON counters",
            "collect <port>|all \n"
            "        <port> - Port Number OR all \n"
            "Examples: - collect 88 \n"
            "          - collect all\n");

        CLI_defineCmd("MEA hc Counters rmon clear",
            (CmdFunc)MEA_CLI_Clear_Counters_HC_RMON,
            "Clear   RMON counters",
            "clear <port>|all \n"
            "      <port> - Port Number OR all \n"
            "Examples: - clear 88 \n"
            "          - clear all\n");

        CLI_defineCmd("MEA hc Counters rmon show",
            (CmdFunc)MEA_CLI_Show_Counters_HC_RMON,
            "Show    RMON counters ",

            "show  <port>|all  [-d details]\n"
            "      <port> - Port Number OR all \n"
            "Examples: - show 88 \n"
            "          - show all \n");
    }

    return MEA_OK;
}


