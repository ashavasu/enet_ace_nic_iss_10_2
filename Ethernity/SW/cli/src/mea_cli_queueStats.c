/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*---------------------------------------------------------------------------*/
/*                                 Includes                                  */
/*---------------------------------------------------------------------------*/

#include "MEA_platform.h"
#include "mea_drv_common.h"
#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_queueStats.h"

/*---------------------------------------------------------------------------*/
/*                                 Defines                                   */
/*---------------------------------------------------------------------------*/

#define MEA_CLI_QueueStatistics_COMMAND_PLACE 0
#define MEA_CLI_QueueStatistics_1ST_ARG_PLACE 1
#define MEA_CLI_QueueStatistics_2ST_ARG_PLACE 2
#define MEA_CLI_QueueStatistics_NUM_OF_QUEUES_IN_A_CLUSTER 8
#define MEA_CLI_QueueStatistics_CLUSTER_OFFSET 128
#define MEA_CLI_QueueStatistics_NORMAL_COL_LEN 11
#define MEA_CLI_QueueStatistics_DELTA_COL_LEN 14
#define MEA_CLI_QueueStatistics_CURQSIZE_COL_LEN 15

/*---------------------------------------------------------------------------*/
/*                                 Globals                                   */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                            Forward declaration                            */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                            CLI Functions                                  */
/*---------------------------------------------------------------------------*/

static MEA_Status MEA_CLI_QueueStatistics_Start(int argc, char *argv[])
{
    if(argc != 1)
    {
        return MEA_ERROR;
    }
    return MEA_API_Start_QueueStatistics(MEA_UNIT_0);
}

static MEA_Status MEA_CLI_QueueStatistics_Stop(int argc, char *argv[])
{
    if(argc != 1)
    {
        return MEA_ERROR;
    }
    return MEA_API_Stop_QueueStatistics(MEA_UNIT_0);
}

static void MEA_CLI_QueueStatistics_PrintCluster(const MEA_Counters_QueueStatistics_qStats_dbt* dbStats, MEA_Uint32 clusterNum)
{
    MEA_Uint32 i = 0;
    CLI_print("\n\nCluster | %u \n", clusterNum);
    CLI_print("\n     Drop pkt |  Egr pkt  |  Ing pkt  |  Egr byte | Ingr byte | Delta Ing Pk | Delta Ing By | Cur Q Size Pk | Cur Q size By |\n");
    for(; i < MEA_QUEUESTATS_NUM_OF_QUEUES_IN_A_CLUSTER; ++i)
    {
        CLI_print("q%d:%*llu|%*llu|%*llu|%*llu|%*llu|%*d|%*d|%*d|%*d|\n", 
        i,
        MEA_CLI_QueueStatistics_NORMAL_COL_LEN, dbStats[i].droppedPackets.val,
        MEA_CLI_QueueStatistics_NORMAL_COL_LEN, dbStats[i].egressPackets.val, 
        MEA_CLI_QueueStatistics_NORMAL_COL_LEN, dbStats[i].ingressPackets.val,
        MEA_CLI_QueueStatistics_NORMAL_COL_LEN, dbStats[i].egressBytes.val, 
        MEA_CLI_QueueStatistics_NORMAL_COL_LEN, dbStats[i].ingressBytes.val, 
        MEA_CLI_QueueStatistics_DELTA_COL_LEN, dbStats[i].deltaIngressPackets, 
        MEA_CLI_QueueStatistics_DELTA_COL_LEN, dbStats[i].deltaIngressBytes, 
        MEA_CLI_QueueStatistics_CURQSIZE_COL_LEN, dbStats[i].curQueueSizePackets, 
        MEA_CLI_QueueStatistics_CURQSIZE_COL_LEN, dbStats[i].curQueueSizeBytes);
    }
}

static MEA_Status MEA_CLI_QueueStatistics_GetClusterStatistics(int argc, char *argv[])
{
    const MEA_Counters_QueueStatistics_qStats_dbt* dbStats;
    MEA_Status err;
    MEA_Uint32 clusterNum;
    if(argc != 2)
    {
        return MEA_ERROR;
    }
    clusterNum = MEA_OS_atoui(argv[MEA_CLI_QueueStatistics_1ST_ARG_PLACE]) - MEA_CLI_QueueStatistics_CLUSTER_OFFSET;
    err = MEA_API_GetStatDataBase_QueueStatistics(MEA_UNIT_0, &dbStats);
    if(err != MEA_OK)
    {
        return err;
    }
    dbStats = dbStats + (clusterNum * MEA_QUEUESTATS_NUM_OF_QUEUES_IN_A_CLUSTER);
    MEA_CLI_QueueStatistics_PrintCluster(dbStats, clusterNum + MEA_CLI_QueueStatistics_CLUSTER_OFFSET);
    return MEA_OK;
}

static MEA_Status MEA_CLI_QueueStatistics_GetStatistics(int argc, char *argv[])
{
    MEA_Uint32 i = 0;
    const MEA_Counters_QueueStatistics_qStats_dbt* dbStats;
    MEA_Status err;
    if(argc != 1)
    {
        return MEA_ERROR;
    }
    err = MEA_API_GetStatDataBase_QueueStatistics(MEA_UNIT_0, &dbStats);
    if(err != MEA_OK)
    {
        return err;
    }
    for(; i < (MEA_QUEUESTATS_NUM_OF_QUEUES / MEA_QUEUESTATS_NUM_OF_QUEUES_IN_A_CLUSTER); ++i)
    {
        MEA_CLI_QueueStatistics_PrintCluster(dbStats, i + MEA_CLI_QueueStatistics_CLUSTER_OFFSET);
    }
    return MEA_OK;
}

static MEA_Status MEA_CLI_QueueStatistics_ClearStatistics(int argc, char *argv[])
{
    MEA_Status err;
    if(argc != 1)
    {
        return MEA_ERROR;
    }
    CLI_print("\nClearing statistics... \n");
    err = MEA_API_ClearCounters_QueueStatistics(MEA_UNIT_0);
    if(err == MEA_OK)
    {
        CLI_print("\nCleared statistics.\n");   
    }
    return err; 
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                            <MEA_CLI_QueueStatistics_AddCmds>                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
MEA_Status MEA_CLI_QueueStatistics_AddCmds(void) 
{
    CLI_defineCmd("MEA counters Qstat start", (CmdFunc)MEA_CLI_QueueStatistics_Start, "Starts queue stats module\n", "start\n");

    CLI_defineCmd("MEA counters Qstat stop",
        (CmdFunc)MEA_CLI_QueueStatistics_Stop,
        "Stops queue stats module\n",
        "stop\n");
    
    CLI_defineCmd("MEA counters Qstat get cluster",
        (CmdFunc)MEA_CLI_QueueStatistics_GetClusterStatistics,
        "Get stats per cluster\n",
        "get cluster <cluster num>\n");

    CLI_defineCmd("MEA counters Qstat clear all",
        (CmdFunc)MEA_CLI_QueueStatistics_ClearStatistics,
        "Clear the counters for all clusters\n",
        "clear all\n");

    CLI_defineCmd("MEA counters Qstat get all",
        (CmdFunc)MEA_CLI_QueueStatistics_GetStatistics,
        "Get stats for all clusters\n",
        "get all\n");

    return MEA_OK;
}

