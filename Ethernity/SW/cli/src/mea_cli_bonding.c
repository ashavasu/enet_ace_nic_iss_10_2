/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/



#include "mea_api.h"
#include "mea_drv_common.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_bonding.h"
#include "mea_bonding_drv.h"






/************************************************************************/
/*                                                                      */
/************************************************************************/

static MEA_Status MEA_CLI_BondingTx_Set(int argc, char *argv[])
{
    MEA_BondingTx_dbt   entry;
    MEA_Uint32            Id;
    int i,j;
    MEA_Port_t m_port;
    MEA_Bool exist;
    MEA_Uint16 extra_param;
    int num_of_params;

    if (argc < 3) {
        return MEA_ERROR;
    }

    Id = MEA_OS_atoi(argv[1]);
    if(MEA_API_BondingTx_IsExist(MEA_UNIT_0,Id,&exist)!=MEA_OK)
    {
        CLI_print("Error MEA_API_BondingTx_IsExist failed\n");
        return MEA_OK;
    }

    for(i=2; i<argc ;i++){
        if(!MEA_OS_strcmp(argv[i],"-valid")){
            num_of_params=1;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.valid = MEA_OS_atoi(argv[i+1]);
        }

        if(!MEA_OS_strcmp(argv[i],"-multi")){
            num_of_params=1;
            if (i+num_of_params > argc )  {
                CLI_print ("Error: Minimum parameters for option -multi is missing\n");
                return MEA_OK;
            }
            entry.num_of_port = MEA_OS_atoi(argv[i+1]);

            if(i+num_of_params + (int)entry.num_of_port > argc){
                CLI_print(
                    "not enough  parameters\n");
                return MEA_ERROR; 
            }
            extra_param=i +1 +num_of_params;
            MEA_OS_memset(&entry.Multiple_Port,0,sizeof(entry.Multiple_Port));
            for ( j = 0; j < (int)entry.num_of_port; j ++ ) 
            {
                m_port = MEA_OS_atoi(argv[extra_param + j]);

                if(MEA_API_Get_IsPortValid(m_port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE)  {
                    CLI_print(
                        "port %d not Valid for multi\n");
                    return MEA_ERROR;

                }
				
                /* set the out_port is valid */
                ((ENET_Uint32*)(&(entry.Multiple_Port.out_ports_0_31)))[m_port/32] |=  
                    (1 << (m_port%32));

            }
        }//multi

    }//for
   

   
    if(!exist){
        CLI_print("Error MEA_API_BondingTx_IsExist id %d not exist\n",Id);
        return MEA_OK;
    }
    

    

    if(MEA_API_BondingTx_Set_Entry(MEA_UNIT_0,Id,&entry) !=MEA_OK){
        CLI_print("MEA_API_BondingTx_Set_Entry ID=%d failed \n",Id);
        return MEA_OK;
    }

    CLI_print("Done for Id %d\n",Id);
   
    return MEA_OK;
}
static MEA_Status MEA_CLI_BondingTx_Create(int argc, char *argv[])
{
    
  MEA_BondingTx_dbt   entry;
  MEA_Uint32            Id=MEA_PLAT_GENERATE_NEW_ID;
  int i,j;
  MEA_Port_t m_port;
  MEA_Uint16 extra_param;
  int num_of_params;

  if (argc < 3) {
      return MEA_ERROR;
  }

  MEA_OS_memset(&entry,0,sizeof(entry));
 
     Id = MEA_OS_atoi(argv[1]);

  for(i=2; i<argc ;i++){
      if(!MEA_OS_strcmp(argv[i],"-valid")){
          num_of_params=1;
          if ((i+num_of_params)>argc) {
              CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
              return MEA_ERROR;
          }
       entry.valid = MEA_OS_atoi(argv[i+1]);
      }
      
      if(!MEA_OS_strcmp(argv[i],"-multi")){
          num_of_params=1;
          if (i+num_of_params > argc )  {
              CLI_print ("Error: Minimum parameters for option -multi is missing\n");
              return MEA_OK;
          }
          entry.num_of_port = MEA_OS_atoi(argv[i+1]);

          if(i+num_of_params + (int)entry.num_of_port > argc){
              CLI_print(
                  "not enough  parameters\n");
              return MEA_ERROR; 
          }
          extra_param=i +1+ num_of_params;
          MEA_OS_memset(&entry.Multiple_Port,0,sizeof(entry.Multiple_Port));
          for ( j = 0; j < (int)entry.num_of_port; j ++ ) 
          {
              m_port = MEA_OS_atoi(argv[extra_param + j]);

              if(MEA_API_Get_IsPortValid(m_port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)==MEA_FALSE)  {
                  CLI_print(
                      "port %d not Valid for multi\n");
                  return MEA_ERROR;

              }
			 
              /* set the out_port is valid */
              ((ENET_Uint32*)(&(entry.Multiple_Port.out_ports_0_31)))[m_port/32] |=  
                  (1 << (m_port%32));

          }
      }//multi

  }//for
    
  if(MEA_API_BondingTx_Create_Entry(MEA_UNIT_0,&entry,&Id) !=MEA_OK)
  {
      CLI_print("Error MEA_API_BondingTx_Create_Entry failed\n");
      return MEA_OK;

  }

    CLI_print("Done Id %d\n",Id);
    return MEA_OK;
}


static MEA_Status MEA_CLI_BondingTx_Delete(int argc, char *argv[])
{

    MEA_TdmCesId_t      id;
    MEA_Bool                            found;

    
    /* Check minimum number of parameters */
    if (argc != 2) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        id = MEA_OS_atoi(argv[1]);
        if (MEA_API_BondingTx_Delete_Entry(MEA_UNIT_0,id) != MEA_OK) {
            CLI_print("Error: MEA_API_BondingTx_Delete_Entry failed \n");
            return MEA_OK;
        }
    } else {

        if (MEA_API_BondingTx_GetFirst_Entry(MEA_UNIT_0,
            &id,
            NULL,
            &found) != MEA_OK) {
                CLI_print("Error: MEA_API_BondingTx_GetFirst_Entry failed \n");
                return MEA_OK;
        }
        while (found) {
            if (MEA_API_BondingTx_Delete_Entry(MEA_UNIT_0,id) != MEA_OK) {
                CLI_print("Error: MEA_API_BondingTx_Delete_Entry failed \n");
                return MEA_OK;
            }
            if (MEA_API_BondingTx_GetNext_Entry(MEA_UNIT_0,
                &id,
                NULL,
                &found) != MEA_OK) {
                    CLI_print("Error: MEA_API_BondingTx_GetNext_Entry failed \n");
                    return MEA_OK;
            }
        }
    }



    
    return MEA_OK;
}


static MEA_Status MEA_CLI_BondingTx_Get(int argc, char *argv[])
{
    MEA_Uint32            Id;
    MEA_BondingTx_dbt entry;
    MEA_Bool exist;
    MEA_Uint32 count;
    MEA_Bool found = MEA_FALSE;
    
    MEA_Port_t temp_port;
    MEA_Uint16 counting_print=0;


    /* Check minimum number of parameters */
    if (argc < 2) {
        return MEA_ERROR;
    }
   
    

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        Id=MEA_OS_atoi(argv[1]);
        if(MEA_API_BondingTx_IsExist(MEA_UNIT_0,Id,&exist)!=MEA_OK)
        {
            CLI_print("Error MEA_API_BondingTx_IsExist failed\n");
            return MEA_OK;
        }
        if(!exist){
            CLI_print("Error MEA_API_BondingTx_IsExist id %d not exist\n",Id);
            return MEA_OK;
        }
        found = MEA_TRUE;
        

    } else {
        Id=0;
        if(MEA_API_BondingTx_GetFirst_Entry(MEA_UNIT_0,
                                       &Id,
                                      &entry, 
                                      &found)!=MEA_OK){
         CLI_print("Error: MEA_API_BondingTx_GetFirst_Entry failed \n");
             return MEA_OK;
        }

     
    }
    




    count=0;
    while (found) {
        
        if(MEA_API_BondingTx_IsExist(MEA_UNIT_0,Id,&exist)!=MEA_OK)
        {
            CLI_print("Error MEA_API_TDM_CES_IsExist failed\n");
            return MEA_OK;
        }
        if(!exist){
            continue;
        }
        MEA_OS_memset(&entry,0,sizeof(entry));
        if(MEA_API_BondingTx_Get_Entry(MEA_UNIT_0,Id,&entry)!=MEA_OK){
          CLI_print("Error MEA_API_BondingTx_Get_Entry failed id %d\n",Id);
          continue;
        }
        
        
        if(count++ ==0){
            CLI_print(" BondingTx Info               \n");
            CLI_print("     valid num                 \n");
            CLI_print(" ID        of_port   ports     \n");
            CLI_print("---- ----- ------- ------------------------------------------------------- \n");
                     //12345678901234567890123456789012345678901234567890123456789012345678901234567890
        }
        
        CLI_print("%4d %5s %7d ", Id ,MEA_STATUS_STR(entry.valid),entry.num_of_port);
        counting_print=0;
        for (temp_port = 0;temp_port <= MEA_MAX_PORT_NUMBER; temp_port++)
        {
            if (((ENET_Uint32*)(&(entry.Multiple_Port.out_ports_0_31)))[temp_port/32] 
            & (1 << (temp_port%32))) {
                CLI_print("%3d ",temp_port);
                counting_print++;
                if(counting_print==8){
                   counting_print=0;
                   CLI_print("\n             ");
                }
            }
        }

        CLI_print("\n"); 
        CLI_print("---- ------- ------------------------------------------------------- \n");
       
        

        if (MEA_API_BondingTx_GetNext_Entry(MEA_UNIT_0,
            &Id,
            &entry,
            &found) != MEA_OK) {
                CLI_print("Error: MEA_API_BondingTx_GetNext_Entry failed \n");
                return MEA_OK;
        }
    }

    if(!count){
    CLI_print("BondingTx table is empty\n");
    }else{
        
    CLI_print("Total of Active BondingTx % d \n",count);
    }
    
    return MEA_OK;
}




















static MEA_Status MEA_CLI_Collect_Counters_Bonding(int argc, char *argv[])
{
    MEA_Bool exist;
    MEA_PmId_t Id,start_Id,end_Id;
    char buf[500];
    char *up_ptr;


    if (argc != 2) {
        CLI_print("Invalid number of parameters\n");
        return MEA_ERROR;
    }

    if (strcmp(argv[1], "all") != 0) {

        //MEA_PmId_t pmId = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            start_Id = MEA_OS_atoi(buf);
            end_Id   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            Id=start_Id = end_Id = MEA_OS_atoi(buf);
        }

        for(Id=start_Id;Id<=end_Id;Id++){
            /* Check valid first_pmId */
            if (MEA_API_BondingTx_IsExist(MEA_UNIT_0,
                Id,
                &exist) != MEA_OK) {
                    CLI_print
                        ("%s - MEA_API_BondingTx_IsExist failed (pmId=%d) \n",
                        __FUNCTION__, Id);
                    return MEA_OK;
            }

            if (exist == MEA_FALSE) {
                continue;
            }

            /* collect the counters of this service */
            if (MEA_API_Get_Counters_Bonding(MEA_UNIT_0, Id, NULL) !=
                MEA_OK) {
                    CLI_print
                        ("%s - MEA_API_Get_Counters_Bonding failed for Id %d \n",
                        __FUNCTION__, Id);
                    return MEA_ERROR;
            }
        }

    } else {

        /* collect the counters for all PMs */
        if (MEA_API_Collect_Counters_Bonding_ALL(MEA_UNIT_0) != MEA_OK) {
            CLI_print("%s - MEA_API_Collect_Counters_Bonding_ALL failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    return MEA_OK;

}
static MEA_Status MEA_CLI_Clear_Counters_Bonding(int argc, char *argv[])
{
    
    MEA_PmId_t Id,start_Id,end_Id;
    char buf[500];
    char *up_ptr;


    if (argc != 2) {
        CLI_print("Invalid number of parameters\n");
        return MEA_ERROR;
    }

    if (strcmp(argv[1], "all") != 0) {
        //MEA_PmId_t pmId = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            start_Id = MEA_OS_atoi(buf);
            end_Id   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            Id=start_Id = end_Id = MEA_OS_atoi(buf);
        }

        for(Id=start_Id;Id<=end_Id;Id++){
            /* Check valid first_pmId */
           

            /* clear the counters of this pmId */
            if (MEA_API_Clear_Counters_Bonding(MEA_UNIT_0, Id) != MEA_OK) {
                CLI_print
                    ("%s - Clear_Counters_PM failed for pmId %d \n",
                    __FUNCTION__, Id);
                return MEA_ERROR;
            }
        }//

    } else {

        /* clear the counters for all PMs */
        if (MEA_API_Clear_Counters_Bonding_All(MEA_UNIT_0) != MEA_OK) {
            CLI_print("%s - MEA_API_Clear_Counters_Bonding_All failed \n",
                __FUNCTION__);
            return MEA_ERROR;
        }
    }

    return MEA_OK;
}
static MEA_Status MEA_CLI_Show_Counters_Bonding(int argc, char *argv[])
{
    int                      count;
    MEA_Uint16               Id;
    MEA_Uint16               first_Id;
    MEA_Uint16               last_Id;
    MEA_BondingCount_dbt          entry;
    
//    MEA_Bool details=MEA_FALSE;
    int i=0;
    char buf[500];
    char *up_ptr;




    if ((argc != 2) && (argc != 3)) {
        CLI_print("Invalid number of parameters\n");
        return MEA_ERROR; 
    } 


    if (strcmp(argv[1],"all") != 0) {



        //first_tdmId = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_Id = MEA_OS_atoi(buf);
            last_Id   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            first_Id = last_Id = atoi(buf);
        }



    } else { 

        if (argc != 2) {
            CLI_print("Invalid number of parameters\n");
            return MEA_ERROR; 
        }

        first_Id = 0;
        if(MEA_STANDART_BONDING_NUM_OF_GROUP_RX>0)
        last_Id  = MEA_STANDART_BONDING_NUM_OF_GROUP_RX-1;
        else
          last_Id=0;
        

    }

    for (i = 0; i < argc; i++) {
        if (MEA_OS_strcmp(argv[i], "-d") == 0) {
//            details = MEA_TRUE;
        }
    }




    MEA_OS_memset(&entry,0,sizeof(entry));
    CLI_print("\n");
    count=0;



    for(Id=first_Id;Id<=last_Id;Id++) {

       

        if (MEA_API_Get_Counters_Bonding(MEA_UNIT_0,
            Id,
            &entry) != MEA_OK) {
                CLI_print(
                    "%s - MEA_API_Get_Counters_Bonding for Id %d failed\n",
                    __FUNCTION__,Id);
                return MEA_ERROR;  
        }

        if (count++==0) {

#ifdef MEA_COUNTER_64_SUPPORT     
    CLI_print(  " ID   Counter                      \n"
                "      name                        \n"
                " ---- -------------------- -------------------- \n");
            //01234567890123456789012345678901234567890123456789012345678901234567890123456789    
#else
            CLI_print( "ID   Counter Name          Lsb       \n"

                "---- ---------------------- ---------- \n");
#endif

        }


        CLI_print("%5d ",Id);

        CLI_print("%-20s ","RX_seq_out_of_bound");
#ifdef MEA_COUNTER_64_SUPPORT  
        CLI_print("%20llu ",entry.RX_seq_out_of_bound.val);
#else
        CLI_print("%10lu ",entry.RX_seq_out_of_bound.s.lsw);
#endif
        CLI_print("\n");

        CLI_print("      %-20s ","RX_seq_duplication");
#ifdef MEA_COUNTER_64_SUPPORT  
        CLI_print("%20llu ",entry.RX_seq_duplication.val);
#else
        CLI_print("%10lu ",entry.RX_seq_duplication.s.lsw);
#endif
        CLI_print("\n");
        CLI_print("      %-20s ","RX_drop_not_sync");
#ifdef MEA_COUNTER_64_SUPPORT  
        CLI_print("%20llu ",entry.RX_drop_not_sync.val);
#else
        CLI_print("%10lu ",entry.RX_drop_not_sync.s.lsw);
#endif
        CLI_print("\n");
        CLI_print("      %-20s ","RX_size_drop");
#ifdef MEA_COUNTER_64_SUPPORT  
        CLI_print("%20llu ",entry.RX_size_drop.val);
#else
        CLI_print("%10lu ",entry.RX_size_drop.s.lsw);
#endif
        CLI_print("\n");



        CLI_print("      %-20s ","TX_drop");
#ifdef MEA_COUNTER_64_SUPPORT  
        CLI_print("%20llu ",entry.TX_drop.val);
#else
        CLI_print("%10lu ",entry.TX_drop.s.lsw);
#endif
        CLI_print("\n");








        if (count > 0) {
            CLI_print("------------------------------------------------------------------------------\n");
        }


    } /* for(serviceId=first_serviceId;serviceId<=last_serviceId;serviceId++) */







    CLI_print("Number Of Entries : %d\n\n",count);




    return MEA_OK;



}







MEA_Status MEA_CLI_bonding_state_configure( int argc, char *argv[])
{
    
    MEA_Port_t port;
    MEA_Bool enable;
    

    if (argc < 2) {
        return MEA_ERROR;
    }

    port = MEA_OS_atoi(argv[1]);
    enable = MEA_OS_atoi(argv[2]);
     

    if(mea_drv_bonding_state_configure(port,  enable)!=MEA_OK){
      CLI_print("failed to set bondingTx state  \n");
        return MEA_OK;
    }
  
    CLI_print("Done for Id %d valid = %s\n",port,MEA_STATUS_STR_ENABLE(enable));

    return MEA_OK;
}



 


MEA_Status MEA_CLI_bonding_state_go( int argc, char *argv[])
{

    MEA_API_check_bonding_state();
CLI_print("Done\n");

return MEA_OK;
}


/************************************************************************/
/* CLI                                                                     */
/************************************************************************/
MEA_Status MEA_CLI_Bonding_AddDebugCmds(void)
{
    
    

    
    /************************************************************************/
    /*     BondingTx                                                        */
    /************************************************************************/
    CLI_defineCmd("MEA BondingTx show",
        (CmdFunc)MEA_CLI_BondingTx_Get, 
        "Show   BondingTx Profile(s) \n",
        "show   <id>|all\n"
        "--------------------------------------------------------------------------------\n"
        "       <id>}all - Profile id  or all\n"
        "--------------------------------------------------------------------------------\n"
        "Examples: - show all \n"
        "          - show 2\n");

    CLI_defineCmd("MEA BondingTx set delete",
        (CmdFunc)MEA_CLI_BondingTx_Delete, 
        "Delete BondingTx Profile(s) \n",
        "delete <id>|all\n"
        "--------------------------------------------------------------------------------\n"
        "       <id>}all - Profile id  or all\n"
        "--------------------------------------------------------------------------------\n"
        "Examples: - delete all \n"
        "          - delete 2\n");

    CLI_defineCmd("MEA BondingTx set modify",
        (CmdFunc)MEA_CLI_BondingTx_Set, 
        "Update BondingTx Profile \n",
        "modify <id> <value>\n"
        "--------------------------------------------------------------------------------\n"
        "       <id> - group id \n"
        "\n"
        "    [-valid] <enable/disable>....\n"
        "    [-multi <numofport> port-n port-n+1]....\n"
        
        "--------------------------------------------------------------------------------\n"
        "Examples: - modify    0  -valid 1 -multi 8 0 1 2 3 10 11 12 13\n"
        "          - modify    4  -valid 1 -multi 4 16 17 18 19        \n");

    CLI_defineCmd("MEA BondingTx set create",
        (CmdFunc)MEA_CLI_BondingTx_Create, 
        "Create BondingTx \n",
        "create auto|<id>  <value> \n"
        "--------------------------------------------------------------------------------\n"
        "       <id> - group id \n"
        "\n"
        "    [-valid] <enable/disable>....\n"
        "    [-multi <numofport> port-n port-n+1]....\n"
        "--------------------------------------------------------------------------------\n"
        "Examples: - create 0 -valid 1 -multi 8 0 1 2 3 10 11 12 13\n"
        "          - create 1 -valid 1 -multi 4 4 5 6 7 \n"
        "          - create 4 -valid 1 -multi 4 16 17 18 19  \n"
        "          - create 8 -valid 1 -multi 4 31 32 33 34 \n"
        "          - create 15 -valid 1 -multi 4 44 45 46 47\n");

    /*------------------------------------------------------------*/
    CLI_defineCmd("MEA counters Bonding collect",
        (CmdFunc) MEA_CLI_Collect_Counters_Bonding,
        "Collect Bonding Counters from HW",
        "collect <BondingId>|all\n"
        "        <BondingId> - Bonding Id OR all \n"
        "Examples: - collect 1  \n"
        "          - collect all\n");

    CLI_defineCmd("MEA counters Bonding clear",
        (CmdFunc) MEA_CLI_Clear_Counters_Bonding,
        "Clear   Bonding Counters",
        "clear <BondingId>|all\n"
        "      <BondingId> - Bonding Id OR all \n"
        "Examples: - clear 1  \n" 
        "          - clear all\n");

    CLI_defineCmd("MEA counters Bonding show",
        (CmdFunc) MEA_CLI_Show_Counters_Bonding,
        "Show    Bonding Counters",

        "show all|<from BondingId> [<to_BondingId>]\n"

        "     <from BondingId> - From BondingId Id OR all \n"
        "     <to   BondingId> - To   BondingId Id \n"
        "Examples: - show 1  \n"
        "          - show 10 15\n" 
        "          - show all\n");


CLI_defineCmd("MEA debug BondingTx_state go ",
              (CmdFunc)MEA_CLI_bonding_state_go, 
              "debug BondingTx_state go\n",
              "debug BondingTx_state go  \n"
              
             
              );

    
        CLI_defineCmd("MEA debug BondingTx_state set ",
        (CmdFunc)MEA_CLI_bonding_state_configure, 
        "debug BondingTx_state \n",
        "debug  <port>  <valid> \n"
        "--------------------------------------------------------------------------------\n"
        "       <port> - bonding port\n"
        
        "    <valid> <enable/disable>....\n"
        
        "--------------------------------------------------------------------------------\n"
        "Examples: - BondingTx_state set 8 1 \n"
        "          - BondingTx_state set 16 0 \n"
        );



    return MEA_OK;
}


