/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_bfd_drv.h"
#include "mea_cli_bfd.h"

#if 1 //MEA_BFD_ENABLE


static MEA_Status MEA_CLI_BFD_Show_Event(int argc, char *argv[]);
static MEA_Status MEA_CLI_BFD_Show_group(int argc, char *argv[]);

static MEA_Status MEA_CLI_Show_Counters_BFD(int argc, char *argv[]);
static MEA_Status MEA_CLI_Collect_Counters_BFD(int argc, char *argv[]);
static MEA_Status MEA_CLI_Clear_Counters_BFD(int argc, char *argv[]);

static MEA_Status MEA_CLI_BFD_Set_BFDSrcPort_to_Cluster(int argc, char *argv[]);
static MEA_Status MEA_CLI_BFD_Get_BFDSrcPort_to_Cluster(int argc, char *argv[]);

static MEA_Status mea_Cli_bfd_CommandResult(int              argc,
    char                   *argv[],
    MEA_Bool               flagType,
    MEA_Uint16             startIndex,
    MEA_BFD_Configure_dbt  *entry);

/*******************************************************************************/


static MEA_Status MEA_CLI_BFD_Show_Event(int argc, char *argv[])
{
    MEA_BFD_Event_group_dbt entry;
    MEA_Bool                   exist;
    MEA_Uint32                 i;
    if (argc < 2) {
       return MEA_ERROR;
    }
    if (MEA_API_Get_BFD_Event(MEA_UNIT_0, &entry ,&exist)!=MEA_OK){
        CLI_print(" MEA_API_Get_BFD_Event failed\n");
    return MEA_OK;
    }
    if(exist){
        for(i=0;i<MEA_BFD_MAX_GROUP;i++){
           CLI_print("BFD Group[%d]= %s\n",i,MEA_STATUS_STR(entry.Group[i]));
        }
    }
    if(!exist){
    CLI_print(" no exist event for BFD\n");
    }
    return MEA_OK;
}

static MEA_Status MEA_CLI_BFD_Show_group(int argc, char *argv[])
{
    MEA_Uint32                 i,j;
    MEA_Uint32                 Event;
    MEA_Uint32                 value;
    MEA_Uint32                 start,end;
    
    if (argc < 2) {
       return MEA_ERROR;
    }
    
    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start=0;
        end= MEA_BFD_MAX_GROUP;
        
		if(end != 0)
		  end=end-1;

    } else{
          
          start=end=MEA_OS_atoi(argv[1]);
          
         
          

          if(start >= MEA_BFD_MAX_GROUP){
          CLI_print(" start group is out of range\n");
          return MEA_ERROR;
          }
    }

CLI_print("-----------------------------------------\n");
CLI_print("                 BFD Event               \n");
CLI_print("-----------------------------------------\n");
   for(i=start;i<=end;i++){
        Event=0;
        if(MEA_API_Get_BFD_Event_GroupId(MEA_UNIT_0,
                                        i, 
                                        &Event) !=MEA_OK){

        }
        for(j=0;j<32;j++){
            value=((Event>>j)& 0x00000001);
            if(j==0){
            CLI_print(" Group stream Event\n",i);
            CLI_print(" %5d %6d %s\n",i,(i*32)+j,MEA_STATUS_STR(value));
            }else{
            CLI_print("       %6d %s\n",(i*32)+j,MEA_STATUS_STR(value));
            }
      }
      CLI_print("-----------------------------------------\n");
   }
    return MEA_OK;



}




static MEA_Status MEA_CLI_Show_Counters_BFD(int argc, char *argv[])
{


    int                         count;
	MEA_CcmId_t                 Id;
    MEA_CcmId_t                 first_Id;
    MEA_CcmId_t                 last_Id;
    MEA_Counters_BFD_dbt       entry;
    MEA_Bool exist;
                    
    
    
    if ((argc != 2) && (argc != 3)) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR; 
    } 


    if (strcmp(argv[1],"all") != 0) {



        first_Id = MEA_OS_atoi(argv[1]);
     

        /* Check valid first_pmId */
        if (MEA_API_IsExist_BFD(MEA_UNIT_0,
                                 first_Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_BFD failed (first_pmId=%d) \n",
                              __FUNCTION__,first_Id);
            return MEA_OK;
        }

        if (!exist) {
 
            CLI_print(
                              "%s - first_Id %d not define \n",
                              __FUNCTION__,first_Id);
            return MEA_OK;  
        }

        if (argc <  3) {
            last_Id = first_Id;
        } else {
            last_Id = MEA_OS_atoi(argv[2]);


            /* Check valid last_pmId */
            if (MEA_API_IsExist_BFD(MEA_UNIT_0,
                                     last_Id,
                                     &exist) != MEA_OK) {
                CLI_print(
                                  "%s - MEA_API_IsExist_PmId failed (last=%d) \n",
                                  __FUNCTION__,last_Id);
                return MEA_OK;
            }

            if (!exist) {
 
                CLI_print(
                                  "%s - last_Id %d not define \n",
                                  __FUNCTION__,last_Id);
                return MEA_OK;  
            }

        }

    } else { 

        if (argc != 2) {
           CLI_print("Invalid number of parameters\n");
           return MEA_ERROR; 
        }

        first_Id = 0;
        last_Id  = MEA_ANALYZER_BFD_MAX_STREAMS -1;
    }
 
    CLI_print("\n");
    count=0;



    for(Id=first_Id;Id<=last_Id;Id++) {
#if 0
        /* Check valid last_pmId */
        if (MEA_API_IsExist_BFD(MEA_UNIT_0,
                                 Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_BFD failed (Id=%d) \n",
                              __FUNCTION__,Id);
            return MEA_OK;
        }

        if (!exist ) {
           continue;
        }
#endif
        if (MEA_API_Get_Counters_BFD(MEA_UNIT_0,
                                    Id,
                                    &entry) != MEA_OK) {
            CLI_print(
                              "%s - Get Counters LM for pmId %d failed\n",
                              __FUNCTION__,Id);
            return MEA_ERROR;  
        }

        if (count++==0) {

            CLI_print(" Id     Rx                   Tx                   \n"
                      " ------ -------------------- -------------------- \n" );
        }
        
        CLI_print(" %6d \n",Id);
       CLI_print(" %6s %20llu %20llu\n", 
            "Packet",        
            entry.Rx_packet,
            entry.Tx_packet.val);
       CLI_print(" %6s %20llu %20llu\n", 
           "Byte",
           entry.Rx_Byte,
           entry.Tx_Byte.val);
  
        if (count > 0) {

          CLI_print("------------------------------------------------\n");
          
        }


    } /* for(serviceId=first_serviceId;serviceId<=last_serviceId;serviceId++) */



    


    CLI_print("Number Of Entries : %d\n\n",count);
   

    return MEA_OK;

}

static MEA_Status MEA_CLI_Collect_Counters_BFD(int argc, char *argv[])
{

	if (argc != 2) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR;
    }

    if (strcmp(argv[1],"all") != 0) {
        MEA_Bool   exist;
        MEA_LmId_t Id = MEA_OS_atoi(argv[1]);


        /* Check valid first_pmId */
        if (MEA_API_IsExist_BFD(MEA_UNIT_0,
                                 Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_BFD failed (Id=%d) \n",
                              __FUNCTION__,Id);
            return MEA_OK;
        }

        if (!exist) {
 
            CLI_print(
                              "%s - Id %d not define\n",
                              __FUNCTION__,Id);
            return MEA_OK;  
        }
     
        /* collect the counters of this service */
    	if (MEA_API_Collect_Counters_BFD(MEA_UNIT_0,Id,NULL) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Collect_Counters_BFD failed for Id %d \n",
                            __FUNCTION__,Id);
            return MEA_ERROR;  
        }
           
    } else {

        /* collect the counters for all  */
    	if (MEA_API_Collect_Counters_BFDs(MEA_UNIT_0) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Collect_Counters_BFDs failed \n",
                            __FUNCTION__);
            return MEA_ERROR;  
        }
    }


	return MEA_OK;


}

static MEA_Status MEA_CLI_Clear_Counters_BFD(int argc, char *argv[])
{

	if (argc != 2) {
       CLI_print("Invalid number of parameters\n");
       return MEA_ERROR;
    }

    if (strcmp(argv[1],"all") != 0) {

        MEA_Bool   exist;
        MEA_LmId_t Id = MEA_OS_atoi(argv[1]);


        /* Check valid first_pmId */
        if (MEA_API_IsExist_BFD(MEA_UNIT_0,
                                 Id,
                                 &exist) != MEA_OK) {
            CLI_print(
                              "%s - MEA_API_IsExist_BFD failed (pmId=%d) \n",
                              __FUNCTION__,Id);
            return MEA_OK;
        }

        if (!exist ) {
 
            CLI_print(
                              "%s - Id %d not define\n",
                              __FUNCTION__,Id);
            return MEA_OK;  
        }
     

        /* clear the counters of this pmId */
    	if (MEA_API_Clear_Counters_BFD(MEA_UNIT_0,Id) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Clear_Counters_BFD failed for pmId %d \n",
                            __FUNCTION__,Id);
            return MEA_ERROR;  
        }
           
    } else {

        /* clear the counters for all PMs */
    	if (MEA_API_Clear_Counters_BFDs(MEA_UNIT_0) != MEA_OK) {
            CLI_print(
                             "%s - MEA_API_Clear_Counters_BFDs failed \n",
                            __FUNCTION__);
            return MEA_ERROR;  
        }
    }


	return MEA_OK;

}



static MEA_Status MEA_CLI_BFD_Set_BFDSrcPort_to_Cluster(int argc, char *argv[])
{

    
    MEA_CcmId_t                port;
  
    MEA_BFD_src_dbt            entry;
   

    if (argc < 3) {
        return MEA_ERROR;
    }



    port = MEA_OS_atoi(argv[1]);
    entry.enable = MEA_OS_atoi(argv[2]);

    if(entry.enable)
        entry.queue_Id = MEA_OS_atoi(argv[3]);
    
    if (MEA_API_Set_SrcPort_to_Cluster_loop(MEA_UNIT_0, port,
        &entry) != MEA_OK) {
        CLI_print("ERROR: MEA_API_Set_SrcPort_to_Cluster_loop port \n", port);
        return MEA_OK;
     }

   


    return MEA_OK;
}



static MEA_Status MEA_CLI_BFD_Get_BFDSrcPort_to_Cluster(int argc, char *argv[])
{


    MEA_CcmId_t                port, start,end;
    
    MEA_BFD_src_dbt            entry;
    MEA_Uint32                 count;
    

    if (argc < 2) {
        return MEA_ERROR;
    }



   
    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        start = 0;
       
            end = MEA_MAX_PORT_NUMBER;
    }
    else {
        port = MEA_OS_atoi(argv[1]);
        if (port > MEA_MAX_PORT_NUMBER) {
            CLI_print("ERROR port % is out of range\n", port);
            return MEA_OK;
        }
        start = end = port;
    }
    count = 0;
    

    for (port = start; port <= end; port++) 
    {
		
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE) == MEA_FALSE)
            continue;
        MEA_OS_memset(&entry, 0, sizeof(entry));
        if (MEA_API_Get_SrcPort_to_Cluster_loop(MEA_UNIT_0, port,
            &entry) != MEA_OK) {
            CLI_print("ERROR: MEA_API_Set_BFDSrcPort_to_Cluster port \n", port);
            return MEA_OK;
        }
        if(entry.enable == MEA_FALSE)
            continue;

        if (count++ == 0) {
            CLI_print("\n");
            CLI_print(" loop cluster         \n"
                      " -------------------- \n"
                      " port enable Cluster  \n"
                       " ---- ------ ------ \n");

        }
        CLI_print(" %4d %6s %6d \n",port, MEA_STATUS_STR(entry.enable), entry.queue_Id);



    }
    if (count != 0) {
        CLI_print(" -------------------- \n");
    }

    return MEA_OK;
}


static MEA_Status mea_Cli_bfd_CommandResult(int              argc,
    char                   *argv[],
    MEA_Bool               flagType,
    MEA_Uint16             startIndex,
    MEA_BFD_Configure_dbt  *entry)
{
    int i;
    int num_of_params;
    


    for (i = (startIndex); i < argc; i++) {

 
        if (!MEA_OS_strcmp(argv[i], "-counter_en")) {

            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry->counter_en = MEA_OS_atoiNum(argv[i + 1]);

            i += num_of_params;
            continue;

        }
        if (!MEA_OS_strcmp(argv[i], "-event_mask")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry->mask_group = MEA_OS_atoiNum(argv[i + 1]);


            i += num_of_params;
            continue;

        }
        if (!MEA_OS_strcmp(argv[i], "-event_block")) {
            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry->block = MEA_OS_atoiNum(argv[i + 1]);


            i += num_of_params;
            continue;

        }

        if (!MEA_OS_strcmp(argv[i], "-time_event")) {

            num_of_params = 1;
            if ((i + num_of_params) > argc) {
                CLI_print("Error: Not enough parameter for option %s \n", argv[i]);
                return MEA_ERROR;
            }
            entry->period_event = MEA_OS_atoiNum(argv[i + 1]);

            i += num_of_params;
            continue;

        }
        



    }//end for


    return MEA_OK;
}


static MEA_Status MEA_CLI_BFD_Show_entry(int argc, char *argv[])
{

    MEA_BFDId_t                  Id_i, start, end;
    MEA_BFD_Configure_dbt        entry;
    MEA_Bool                     exist;
    MEA_Uint32                   count;

    char       *str_period[] = {
        "NA",
        " 3.33ms",
        "   10ms",
        "  100ms",
        "   1sec",
        "  10sec",
        "   1min",
        "  10min"
        "ERROR  "
    };


    if (argc < 2) {
        return MEA_ERROR;
    }
    if (!MEA_PACKET_ANALYZER_BFD_SUPPORT) {
        CLI_print("%s - MEA_PACKET_ANALYZER_BFD_SUPPORT not support \n",
            __FUNCTION__);
        return MEA_OK;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        start = 0;
        end = (MEA_PacketGen_t)(MEA_ANALYZER_BFD_MAX_STREAMS - 1);
      
    }
    else {
        Id_i = MEA_OS_atoi(argv[1]);
        if (Id_i >= MEA_ANALYZER_BFD_MAX_STREAMS) {
            CLI_print("ERROR Id % is out of range\n", Id_i);
            return MEA_OK;
        }
        start = end = Id_i;
    }
    count = 0;
    MEA_OS_memset(&entry, 0, sizeof(entry));

    for (Id_i = start; Id_i <= end; Id_i++) {
        if (MEA_API_IsExist_BFD(MEA_UNIT_0, Id_i, &exist) != MEA_OK) {
            CLI_print("ERROR MEA_API_IsExist_BFD failed (id=%d)\n", Id_i);
            return MEA_OK;
        }
        if (!exist) {
            continue;
        }

        if (MEA_API_Get_BFD_Entry(MEA_UNIT_0,
            Id_i,
            &entry) != MEA_OK) {

        }

        if (count++ == 0) {
            CLI_print("\n");
            CLI_print("           BFD Entry      \n"
                " ----------------------------\n"
                "      Time     mask  block count\n"
                "  ID  event    group        en \n"
                " ---- -------- ----- ----- ----- \n");
            // 01234567890123456789012345678901234567890123456789012345678901234567890123456789
        }

        CLI_print(" %4d %8s %4s %5s %5s\n", Id_i,
           
            str_period[entry.period_event],
            MEA_STATUS_STR(entry.mask_group),
            MEA_STATUS_STR(entry.block),
            MEA_STATUS_STR(entry.counter_en)
        );

    }

    if (count == 0) {
        CLI_print("BFD Table is empty\n");
    }






    return MEA_OK;
}

static MEA_Status MEA_CLI_BFD_Create(int argc, char *argv[])
{

    MEA_BFD_Configure_dbt      entry;
    MEA_BFDId_t                id_io;
    int i;

    if (argc < 2) {
        return MEA_ERROR;
    }

    for (i = 1; i < argc; i++) {

        if ((argv[i][0] == '-') && (
            
            (MEA_OS_strcmp(argv[i], "-counter_en")  != 0) &&
            (MEA_OS_strcmp(argv[i], "-event_mask")  != 0) &&
            (MEA_OS_strcmp(argv[i], "-event_block") != 0) &&
            (MEA_OS_strcmp(argv[i], "-time_event")  != 0) ) ) {
            CLI_print("ERROR: Not support option %s \n", argv[i]);
            return MEA_OK;
        }
    }
    MEA_OS_memset(&entry, 0, sizeof(entry));

    if (!MEA_OS_strcmp(argv[1], "auto")) { /* only for create*/
        id_io = MEA_PLAT_GENERATE_NEW_ID;
    }
    else {
        id_io = MEA_OS_atoi(argv[1]);
    }

    if (mea_Cli_bfd_CommandResult(argc, argv, MEA_FALSE,
        2, &entry) != MEA_OK) {
        CLI_print("Error: mea_PacketGenCommandResult \n");
        return MEA_OK;
    }

    if (MEA_API_Create_BFD_Entry(MEA_UNIT_0, &entry, &id_io) != MEA_OK) {
        CLI_print("Error: can't create BFD_Entry \n");
        return MEA_ERROR;
    }



    CLI_print("Done create BFD with Id = %d  \n", id_io);
    return MEA_OK;
}

static MEA_Status MEA_CLI_BFD_Set(int argc, char *argv[])
{

    MEA_BFD_Configure_dbt      entry;
    MEA_BFDId_t                id_io;
    MEA_Bool                   exist;
    int i;

    if (argc < 2) {
        return MEA_ERROR;
    }

    for (i = 1; i < argc; i++) {

        if ((argv[i][0] == '-') && (
            (MEA_OS_strcmp(argv[i], "-counter_en") != 0) &&
            (MEA_OS_strcmp(argv[i], "-period_id") != 0) &&
            (MEA_OS_strcmp(argv[i], "-time_event") != 0) &&
            (MEA_OS_strcmp(argv[i], "-event_mask") != 0) &&
            (MEA_OS_strcmp(argv[i], "-event_block") != 0)) ){
            CLI_print("ERROR: Not support option %s \n", argv[i]);
            return MEA_OK;
        }
    }


    id_io = MEA_OS_atoi(argv[1]);

    // check if the id is Exist
    if (MEA_API_IsExist_BFD(MEA_UNIT_0, id_io, &exist) != MEA_OK) {
        CLI_print("Error: MEA_API_IsExist_BFD \n");
        return MEA_OK;
    }

    if (MEA_API_Get_BFD_Entry(MEA_UNIT_0,
        id_io,
        &entry) != MEA_OK) {
        CLI_print("Error: MEA_API_Get_BFD_Entry  %d \n", id_io);
        return MEA_OK;
    }


    if (mea_Cli_bfd_CommandResult(argc, argv, MEA_FALSE,
        2, &entry) != MEA_OK) {
        CLI_print("Error: mea_Cli_ccm_CommandResult \n");
        return MEA_OK;
    }

    if (MEA_API_Set_BFD_Entry(MEA_UNIT_0, id_io, &entry) != MEA_OK) {
        CLI_print("Error: MEA_API_Set_BFD_Entry %d \n", id_io);
        return MEA_ERROR;
    }


    return MEA_OK;
}

static MEA_Status MEA_CLI_BFD_Delete(int argc, char *argv[])
{
    MEA_BFDId_t  BFDId, start_bfd, end_bfd;
    MEA_Bool      exist;

    if (argc < 2) {
        return MEA_ERROR;
    }

    if (!MEA_OS_strcmp(argv[1], "all")) {
        start_bfd = 0;
        
            end_bfd = MEA_ANALYZER_BFD_MAX_STREAMS - 1;
    }
    else {
        start_bfd = (MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_bfd = start_bfd;
        if ((MEA_API_IsExist_BFD(MEA_UNIT_0, start_bfd, &exist) != MEA_OK) ||
            (!exist)) {
            CLI_print(" ERROR: BFD id %d is not exist \n", start_bfd);
            return MEA_OK;
        }
    }

    for (BFDId = start_bfd; BFDId <= end_bfd; BFDId++) {
        if (MEA_API_IsExist_BFD(MEA_UNIT_0, BFDId, &exist) == MEA_OK) {
            if (exist) {

                if (MEA_API_Delete_BFD_Entry(MEA_UNIT_0, BFDId) != MEA_OK) {
                    CLI_print(" ERROR: can't Delete BFD id %d \n", BFDId);
                }
            }
        }
    }


    return MEA_OK;
}

MEA_Status MEA_CLI_BFD_AddDebugCmds(void)
{
 
    CLI_defineCmd("MEA BFD analyzer show entry",
        (CmdFunc)MEA_CLI_BFD_Show_entry,
        "Show   BFD stream configuration \n",
        "show entry all|<BFD Id> \n"
        "---------------------------------------------------\n"
        "  entry  all|<BFD Id> - all or id \n"
        "---------------------------------------------------\n"
        "Examples: - show entry all \n"
        "          - show entry 2    \n"
        "             \n");


    CLI_defineCmd("MEA BFD analyzer set delete",
        (CmdFunc)MEA_CLI_BFD_Delete,
        "delete    CCM stream configuration\n",
        "         all|<Id>   - delete specific Id \n"
        "Examples: - delete 1\n"
        "          - delete all \n"
        "          - delete all \n");
    
    
    
    CLI_defineCmd("MEA BFD analyzer set modify",
        (CmdFunc)MEA_CLI_BFD_Set,
        "modify   BFDId configuration\n",
        
        "    [-counter_en  <enable counter>]\n"

        "    [-time_event <value> ]\n"
        "    [-event_mask <mask_group>\n"
        "    [-event_block] <block> ]\n"
        "--------------------------------------------------------------------------------\n"
        "    <id>   force BFD profile id                                \n"
        "\n"
      
        "     -counter_en  <enable counter> ]\n"
        "             <valid> 0 disable 1 enable           \n"
        "     -event_mask  Mask stream period event in the group \n"
        "             <mask_group> 0 disable 1 enable           \n"
        "     -event_block  <block all event> ]\n"
        "             <block> 0 disable 1 enable           \n"
        "     -time_event  Time to the receipt of event\n"
        "             <value>   1 -   3.3ms \n"
        "                       2 -  10msec\n"
        "                       3 - 100msec\n"
        "                       4 -   1 sec\n"
        "                       5 -  10 sec\n"
        "                       6 -   1 Min\n"
        "                       7 -  10 Min\n"
        "     -clear_segid  clear the sequence \n"
        "             <enable> 0 disable 1 enable \n"
        "--------------------------------------------------------------------------------\n"
        " Examples:\n"
        "   - modify 1  -event_block 1 -event_mask 1-time_event 2\n"
    
        "                   \n");

    CLI_defineCmd("MEA BFD analyzer set create",
        (CmdFunc)MEA_CLI_BFD_Create,
        "create BFD stream configuration \n",
       
        "    [-counter_en  <enable counter>]\n"
        "    [-time_event <value> ]\n"
        "    [-event_mask <mask_group>\n"
        "    [-event_block] <block> ]\n"
        "---------------------------------------------------------------\n"
        "    auto - generate new BFD profile id automatically           \n"
        "    <id>   force BFD profile id                                \n"
        "\n"
        "     -counter_en  <enable counter> ]\n"
        "             <valid> 0 disable 1 enable           \n"
        "     -event_mask  Mask stream period event in the group \n"
        "             <mask_group> 0 disable 1 enable           \n"
        "     -event_block  <block all event> ]\n"
        "             <block> 0 disable 1 enable           \n"
       
        "             <value> 1 to 7 \n"
        "     -time_event  Time to the receipt of event\n"
        "             <value>   1 -   3.3ms \n"
        "                       2 -  10msec\n"
        "                       3 - 100msec\n"
        "                       4 -   1 sec\n"
        "                       5 -  10 sec\n"
        "                       6 -   1 Min\n"
        "                       7 -  10 Min\n"
       
        "-------------------------------------------------------------------------\n"
        " Examples:\n"
        "  - create auto  -counter_en 1 -time_event 2 \n"
        "                   \n"
        "  - create 1  -counter_en 1 -event_block  0 -event_mask 0 -time_event 1  -\n"
        );




    CLI_defineCmd("MEA BFD loop set",
        (CmdFunc)MEA_CLI_BFD_Set_BFDSrcPort_to_Cluster,
        "set    BFD loop cluster",
        "set   <port> <enable> <clusterId> \n"

        "Examples: - set 104 1 104  \n"
        "          - set 105 1  105\n");

    CLI_defineCmd("MEA BFD loop show",
        (CmdFunc)MEA_CLI_BFD_Get_BFDSrcPort_to_Cluster,
        "Show    BFD loop ",
        "show all|<port> \n"
        "     <Id> -Id Id OR all \n"

        "Examples: - show 1  \n"
        "          - show 104\n"
        "          - show all\n");



    CLI_defineCmd("MEA counters BFD collect",
        (CmdFunc)MEA_CLI_Collect_Counters_BFD,
        "Collect BFD Counters ",
        "collect <streamId>|all\n"
        "        <Id> - Id OR all \n"
        "Examples: - collect 1  \n"
        "          - collect all\n");

    CLI_defineCmd("MEA counters BFD clear",
        (CmdFunc)MEA_CLI_Clear_Counters_BFD,
        "Clear   BFD Counters",
        "clear <Id>|all\n"
        "      <Id> - Id OR all \n"
        "Examples: - clear 1  \n"
        "          - clear all\n");

    CLI_defineCmd("MEA counters BFD show",
        (CmdFunc)MEA_CLI_Show_Counters_BFD,
        "Show    BFD Counters",
        "show all|<from Id> \n"
        "     <Id> -Id Id OR all \n"

        "Examples: - show 1  \n"
        "          - show 10 15\n"
        "          - show all\n");


    CLI_defineCmd("MEA BFD show event entry",
        (CmdFunc)MEA_CLI_BFD_Show_Event,
        "Show   event entry\n",
        "show cc all \n"
        "---------------------------------------------------\n"
        "                                                   \n"
        "---------------------------------------------------\n"
        "Examples: - show event entry all \n"
        "                              \n"
        "                              \n");

    CLI_defineCmd("MEA BFD show event group",
        (CmdFunc)MEA_CLI_BFD_Show_group,
        "Show   group\n",
        "show   group all|<group> \n"
        "---------------------------------------------------\n"
        "                                                   \n"
        "---------------------------------------------------\n"
        "Examples: - show event group all \n"
        "            show event group  1 \n"
        "                              \n");




    return MEA_OK;
}






#endif //MEA_BFD_ENABLE

