/*
This file/directory and the information contained in it are
proprietary and confidential to Ethernity Networks Ltd.
No person is allowed to copy, reprint, reproduce or publish
any part of this document, nor disclose its contents to others,
nor make any use of it, nor allow or assist others to make any
use of it - unless by prior written express
authorization of Neralink  Networks Ltd and then only to the
extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/

/*----------------------------------------------------------------------------*/
/*             Includes                                                       */
/*----------------------------------------------------------------------------*/

#include "mea_cli_globals.h"

/*----------------------------------------------------------------------------*/
/*             Defines                                                        */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             Typedefs                                                       */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             Global variables                                               */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/*             Implementation                                                 */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/*             Local functions                                                */
/*----------------------------------------------------------------------------*/

static MEA_Status  MEA_CLI_Set_Globals_iTHi(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Device_WallClock_set(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Device_WallClock_calibration(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Device_WallClock_1sec(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Device_pos_polling(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Device_Serdes_Power_Saving(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Device_utopia_threshold(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Device_Stp_mode(int argc, char* argv[]);
static MEA_Status MEA_CLI_Global_Set_1p1_Count(int argc, char *argv[]);
static MEA_Status MEA_CLI_Global_Set_Hc_Compress_Baypass(int argc, char *argv[]);
static MEA_Status MEA_CLI_Global_Set_Hc_offset_mode(int argc, char *argv[]);
static MEA_Status MEA_CLI_Global_Set_ts_measurement(int argc, char *argv[]);
static MEA_Status MEA_CLI_Set_Globals_Rmon_calc_rate_enable(int argc, char *argv[]);
static MEA_Status MEA_CLI_Set_Globals_EtytypeOffset(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Device_Counters_Enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_forwarderMCIpSubNet(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_eth0_Ip(int argc, char* argv[]);
static MEA_Status MEA_CLI_Set_Globals_Device_edit_Ip(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_acm_configure(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_BM_reorder_timeout(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_BM_Set_Delay(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_bm_watchdog(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_bcst_drp_lvl(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Cluster_mode(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_bmqs(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_mqs(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_wred(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_mc_mqs(int argc, char *argv[]);
static MEA_Status  MEA_CLI_Set_Globals_pri_q_mode(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_wred_shift(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_max_used_desc_buff_thresholds(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Pm_rate_enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_sssmii_tx_config_reg1(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_sssmii_tx_config_reg0(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_sssmii_rx_config_reg(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_drop_unmatch_pkts(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Lxcp_Enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Hash_Type(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Set_ACL_hash(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_semi_back_pressure(int argc, char* argv[]);
static MEA_Status MEA_CLI_Set_Globals_if_ingress_shaper(int argc, char *argv[]);
static MEA_Status  MEA_CLI_Set_Globals_pause_enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_bm_chunk_high_limit(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_bm_chunk_short_limit(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_bm_pkt_high_limit(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_bm_pkt_short_limit(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_network_L2CP_BaseAddr(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_network_L2TP_tunnelAddr(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_cfm_oam_Valid(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_my_SA_Mac(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_cfm_oam_EtherType(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_cfm_oam_UC_Mac(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_cfm_oam_MC_Mac(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Local_Mac_Address(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Local_IP_Address(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_Local_BFD_SW_IP_Addresss(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_MyBFD_Discriminator(int argc, char* argv[]);

 
static MEA_Status  MEA_CLI_Set_Globals_link_up_mask(int argc, char* argv[]);
static MEA_Status MEA_CLI_Queue_Handler(int argc, char *argv[]);
static MEA_Status MEA_CLI_GPIO_RW_Z70(int argc, char *argv[]);
static MEA_Status MEA_CLI_PeriodicEnable(int argc, char *argv[]);
static MEA_Status  MEA_CLI_Set_Globals_alow_ingflow_pol_zero(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_en_ddr_nic(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_ipg(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_frag_art_eop(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_mac_fifo_threshold(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_WD_Disable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_PacketGen(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_close_loop_utopia(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_forwarder_addr_width(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_SRV_extenal_hash_type(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_forwarder_Config_hash(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_forwarder_aging(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_forwarder_diseableDelReinit(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_forwarder_act_offset(int argc, char* argv[]);
static MEA_Status MEA_CLI_Set_Globals_lag_enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_forwarder_enable(int argc, char* argv[]);
static MEA_Status MEA_CLI_Set_Globals_learning_disable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_check_srv_key(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_learn_disable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_mstp_enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_rstp_enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_forwarder_OutPut(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_mirror_port_ces(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_mirror_ingress_1G(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_mirror_ingress_10G(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_PacketAnalyzer_EtherType(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_PacketAnalyzer_enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_PacketAnalyzer_Port(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_policer_enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_shaper_enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_wred_offset(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_set_target_MTU(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_set_frag_Type(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_set_reorder_frag_time_out(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_WBRG_swap(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_best_effort_enable(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_vcmux_BCM(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_RxAfdxTicks(int argc, char *argv[]);
static MEA_Status  MEA_CLI_Set_CCMTicks(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_PolicerTicks(int argc, char* argv[]);
static MEA_Status MEA_CLI_Set_IngFlowPolicerTicks(int argc, char *argv[]);
static MEA_Status  MEA_CLI_Set_ShaperPortTicks(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_ShaperClusterTicks(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_ShaperPriQueueTicks(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_GlobalOamAdd(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_GlobalOamRemove(int argc, char* argv[]);
static MEA_Status  MEA_CLI_Set_Globals_bm_enable(int argc, char* argv[]);
static MEA_Status MEA_CLI_Show_Globals(int argc, char* argv[]);
static MEA_Status MEA_CLI_GlobalParser_Set(int argc, char *argv[]);

static MEA_Status  MEA_CLI_Show_Device_MCIpSubNet(void);

/*************************************************************************************************/
//********************************
// Switching oriented "show" CLI *
//********************************

//global_parser

static MEA_Status MEA_CLI_GlobalParser_Get(void);


/*************************************************************************************************/




static MEA_Status  MEA_CLI_Set_Globals_policer_enable(int argc, char* argv[]) {



	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.policer_enable = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status MEA_CLI_Show_Device_MCIpSubNet(void)
{

	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 srcIp;
	MEA_Uint32 dstIp;
	char srcIp_str[20];
	char dstIp_str[20];

	MEA_OS_memset(&entry, 0, sizeof(MEA_Globals_Entry_dbt));
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	dstIp = (entry.Globals_Device.MC_ServerIp.DestIp);
	srcIp = (entry.Globals_Device.MC_ServerIp.sourceIp);
	MEA_OS_inet_ntoa(MEA_OS_htonl(dstIp), dstIp_str, sizeof(dstIp_str));
	MEA_OS_inet_ntoa(MEA_OS_htonl(srcIp), srcIp_str, sizeof(srcIp_str));

	CLI_print("  MC Ip Server:\n");
	CLI_print("  -------------\n");
	CLI_print("     McIp subnet: %s  \n", dstIp_str);
	CLI_print("     Mc server  : %s  \n", srcIp_str);

	return MEA_OK;

}


static MEA_Status MEA_CLI_GlobalParser_Get()
{
	MEA_Bool All_index = MEA_FALSE;
	MEA_Uint16 All_index_start = 0, All_index_end = 0;
	//MEA_Bool All_type=MEA_FALSE;
	MEA_Uint16 All_type_start = 0, All_type_end = 0;
	MEA_Uint16 index;
	MEA_Global_Parser_type_t type;
	MEA_Global_Parser_dbt         entry;
	MEA_Uint32 count;
	MEA_Uint32 offset = 0;




	All_index = MEA_TRUE;

	//All_type=MEA_TRUE;
	All_type_start = 0;
	All_type_end = MEA_INGRESS_PARSER_OFFSET_LAST - 1;


	for (type = All_type_start; type <= All_type_end; type++) {

		if (All_index) {

			All_index_start = 0;
			All_index_end = 0;
			switch (type)
			{
			case MEA_INGRESS_PARSER_OFFSET_12:
				offset = 12;
				All_index_end = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM0_OFFSET_12_LENGTH - 1;

				break;
			case MEA_INGRESS_PARSER_OFFSET_16:
				offset = 16;
				All_index_end = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM1_OFFSET_16_LENGTH - 1;

				break;
			case MEA_INGRESS_PARSER_OFFSET_20:
				offset = 20;
				All_index_end = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM2_OFFSET_20_LENGTH - 1;

				break;
			case MEA_INGRESS_PARSER_OFFSET_34:
				offset = 34;
				All_index_end = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM3_OFFSET_34_LENGTH - 1;

				break;
			case MEA_INGRESS_PARSER_OFFSET_38:
				offset = 38;
				All_index_end = ENET_IF_CMD_PARAM_TBL_TYPE_L2_CAM4_OFFSET_38_LENGTH - 1;
				break;

			case MEA_INGRESS_PARSER_OFFSET_LAST:
			default:
				offset = 12;
				continue;
				break;
			}

		}


		count = 0;
		for (index = All_index_start; index <= All_index_end; index++) {
			MEA_OS_memset(&entry, 0, sizeof(entry));
			if (MEA_API_Get_Global_ParserEntry(MEA_UNIT_0,
				index,
				type,
				&entry) != MEA_OK) {
				continue;
			}
			if (count == 0) {
				CLI_print(" Global Parser CAM%d   offset %d\n", type, offset);
				CLI_print("------------------\n");
				CLI_print("index valid value\n");
				CLI_print("----- ----- ------\n");
			}
			count++;
			CLI_print("%5d %5s 0x%04x\n", index, MEA_STATUS_STR(entry.valid), entry.etherType);

		}
		CLI_print("------------------\n\n\n");

	}





	return MEA_OK;
}


static MEA_Status MEA_CLI_GlobalParser_Set(int argc, char *argv[])
{

	MEA_Global_Parser_type_t      type;
	MEA_Global_Parser_dbt         entry;
	MEA_Uint16 index;

	if (argc < 4) {
		CLI_print("minimum of parameters \n");
		return MEA_ERROR;
	}

	MEA_OS_memset(&entry, 0, sizeof(entry));
	//<index> <type> <valid> <value> 
	index = MEA_OS_atoi(argv[1]);
	type = MEA_OS_atoi(argv[2]);
	entry.valid = MEA_OS_atoi(argv[3]);
	entry.etherType = (MEA_Uint16)MEA_OS_atohex(argv[4]);

	if (MEA_API_Set_Global_ParserEntry(MEA_UNIT_0, index, type, &entry) != MEA_OK) {
		CLI_print("Error: MEA_API_Set_Global_ParserEntry failed\n");
		return MEA_OK;

	}


	return MEA_OK;
}

static MEA_Status  MEA_CLI_Show_Globals(int argc, char* argv[]) {


	int i;



	MEA_Uint32 Ip_val;
	char                     Ip_str[20];

	MEA_Globals_Entry_dbt entry;



	MEA_Clock_t entry_clock;
#if (!defined(__KERNEL__))
	time_t result;
	struct tm* brokentime;
#endif

	if (argc < 2) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	if ((MEA_OS_strcmp(argv[1], "all") == 0) ||
		(MEA_OS_strcmp(argv[1], "if") == 0)) {

		CLI_print("IF Globals:\n"
			"-----------\n");


		CLI_print("  %-40s : %d \n",

			"thresh_frag_art_eop",
			entry.if_configure_1.val.thresh_frag_art_eop);



		CLI_print("  %-40s : %d \n",

			"close_loop_utopia_master_tx_to_slave_tx",
			entry.if_global1.val.close_loop_utopia_master_tx_to_slave_tx);


		CLI_print("  %-40s : %d \n",

			"close_loop_utopia_master_tx_to_master_rx",
			entry.if_global1.val.close_loop_utopia_master_tx_to_master_rx);




		CLI_print("  %-40s : %d \n",
			"link_up_mask",
			entry.if_global1.val.link_up_mask);



		CLI_print("  %-40s : %d \n",

			"IPG GMII",

			entry.if_global0.val.ipg_gmii);



		CLI_print("  %-40s : %d \n",

			"IPG MII",

			entry.if_global0.val.ipg_mii);

		CLI_print("  %-40s : %s \n",

			"Pause Enable ",

			MEA_STATUS_STR((entry.if_global0.val.pause_enable)));

		CLI_print("  %-40s : %s \n",

			"LXCP Enable ",

			MEA_STATUS_STR((entry.if_global1.val.Lxcp_Enable)));

		CLI_print("  %-40s : %s \n",

			"Forwarder Enable ",

			MEA_STATUS_STR((entry.if_global1.val.forwarder_enable)));
		CLI_print("  %-40s : %s \n",

			"learning disable ",

			MEA_STATUS_STR((entry.if_global1.val.learning_disable)));


		CLI_print("  %-40s : %d \n",

			"Forwarder Output Algorithm ",

			entry.if_global1.val.Dse_Output_Algoritm);

		CLI_print("  %-40s : %d \n",
			"SRV_E hash type ",
			entry.if_global0.val.srv_external_hash_function);

		CLI_print("  %-40s : %d \n",

			"Forwarder act offset",

			entry.if_global0.val.fwd_act_offset);

		CLI_print("  %-40s : %d \n", "en_ddr_nic", entry.if_global0.val.en_ddr_nic);


		CLI_print("  %-40s : %d \n",
			"service hash1 type ",
			entry.if_global1.val.hash1);
		CLI_print("  %-40s : %d \n",
			"service hash2 type ",
			entry.if_global1.val.hash2);

		CLI_print("  %-40s : %d \n",
			"Acl hash type ",
			entry.if_global1.val.Acl_hash);

		CLI_print("  %-40s : %d \n",
			"Drop unmatch packets ",
			entry.if_global1.val.drop_unmatch_pkts);
		CLI_print("  %-40s : %d \n",
			"Wd_disable",
			entry.if_global1.val.wd_disable);
		CLI_print("  %-40s : %s \n",
			"mstp_enable",
			MEA_STATUS_STR(entry.if_global1.val.mstp_enable));
		CLI_print("  %-40s : %s \n",
			"rstp_enable",
			MEA_STATUS_STR(entry.if_global1.val.rstp_enable));




		CLI_print(" Forwarder Hash function \n");
		CLI_print(" key hash1 hash2 \n");
		CLI_print(" --- ----- ----- \n");
		CLI_print(" %3d %5d %5d \n", 0, entry.if_fwd_hash.val.key0_hashO, entry.if_fwd_hash.val.key0_hash1);
		CLI_print(" %3d %5d %5d \n", 1, entry.if_fwd_hash.val.key1_hashO, entry.if_fwd_hash.val.key1_hash1);
		CLI_print(" %3d %5d %5d \n", 2, entry.if_fwd_hash.val.key2_hashO, entry.if_fwd_hash.val.key2_hash1);
		CLI_print(" %3d %5d %5d \n", 3, entry.if_fwd_hash.val.key3_hashO, entry.if_fwd_hash.val.key3_hash1);
		CLI_print(" %3d %5d %5d \n", 4, entry.if_fwd_hash.val.key4_hashO, entry.if_fwd_hash.val.key4_hash1);
		CLI_print(" %3d %5d %5d \n", 5, entry.if_fwd_hash.val.key5_hashO, entry.if_fwd_hash.val.key5_hash1);
		CLI_print(" %3d %5d %5d  \n", 6, entry.if_fwd_hash.val.key6_hashO, entry.if_fwd_hash.val.key6_hash1);
		CLI_print(" %3d %5d %5d \n", 7, entry.if_fwd_hash.val.key7_hashO, entry.if_fwd_hash.val.key7_hash1);
		CLI_print(" --- ----- ----- \n");


		CLI_print("-------------------\n");
		CLI_print(" GBE  MAC threshold\n");
		CLI_print("-------------------\n");
		CLI_print("  %-40s : %d \n", "AF_threshold", entry.if_global2_Gbe.val.AF_threshold);
		CLI_print("  %-40s : %d \n", "tx_threshold", entry.if_global2_Gbe.val.tx_threshold);
		CLI_print("  %-40s : %d \n", "RX_mac_threshold", entry.if_global2_Gbe.val.RX_mac_threshold);
		CLI_print("  %-40s : %d \n", "rx_pre_EOC_threshold", entry.if_global2_Gbe.val.rx_pre_EOC_threshold);
		CLI_print("-------------------\n\n");

		CLI_print("------------------------\n");
		CLI_print(" G999.1  MAC threshold  \n");
		CLI_print("------------------------\n");
		CLI_print("  %-40s : %d \n", "AF_threshold", entry.if_threshold_G999.val.AF_threshold);
		CLI_print("  %-40s : %d \n", "tx_threshold", entry.if_threshold_G999.val.tx_threshold);
		CLI_print("  %-40s : %d \n", "RX_mac_threshold", entry.if_threshold_G999.val.RX_mac_threshold);
		CLI_print("  %-40s : %d \n", "rx_pre_EOC_threshold", entry.if_threshold_G999.val.rx_pre_EOC_threshold);
		CLI_print("------------------------\n\n");
		CLI_print("------------------------\n");
		CLI_print("  TLS MAC threshold     \n");
		CLI_print("------------------------\n");
		CLI_print("  %-40s : %d \n", "AF_threshold", entry.if_threshold_TLS.val.AF_threshold);
		CLI_print("  %-40s : %d \n", "tx_threshold", entry.if_threshold_TLS.val.tx_threshold);
		CLI_print("  %-40s : %d \n", "RX_mac_threshold", entry.if_threshold_TLS.val.RX_mac_threshold);
		CLI_print("  %-40s : %d \n", "rx_pre_EOC_threshold", entry.if_threshold_TLS.val.rx_pre_EOC_threshold);
		CLI_print("------------------------\n\n");
		CLI_print("------------------------\n");
		CLI_print("  CPU MAC threshold      \n");
		CLI_print("-------------------------\n");
		CLI_print("  %-40s : %d \n", "AF_threshold", entry.if_threshold_CPU.val.AF_threshold);
		CLI_print("  %-40s : %d \n", "tx_threshold", entry.if_threshold_CPU.val.tx_threshold);
		CLI_print("  %-40s : %d \n", "RX_mac_threshold", entry.if_threshold_CPU.val.RX_mac_threshold);
		CLI_print("  %-40s : %d \n", "rx_pre_EOC_threshold", entry.if_threshold_CPU.val.rx_pre_EOC_threshold);
		CLI_print("-------------------------\n\n");



		CLI_print("  %-40s : %d (%-7s) \n",
			"Mac aging Enable ",
			entry.macAging.val.enable,
			(entry.macAging.val.enable == 0) ? "Disable" : "Enable");
		CLI_print("  %-40s : %d \n",
			"Mac aging last table ",
			entry.macAging.val.lastTable);
		CLI_print("  %-40s : %d \n",
			"Mac aging sweep interval",
			entry.macAging.val.sweepInterval);
		if (MEA_PACKETGEN_SUPPORT) {
			CLI_print("  %-40s :  \n",
				"PacketGen ");
			CLI_print("  %-40s : %s \n",
				"    enable",
				MEA_STATUS_STR(entry.if_global0.val.generator_enable));
		}

		CLI_print("  %-40s : %02x:%02x:%02x:%02x:%02x:%02x \n",
			"network L2CP BaseAddr",
			entry.network_L2CP_BaseAddr.b[0],
			entry.network_L2CP_BaseAddr.b[1],
			entry.network_L2CP_BaseAddr.b[2],
			entry.network_L2CP_BaseAddr.b[3],
			entry.network_L2CP_BaseAddr.b[4],
			entry.network_L2CP_BaseAddr.b[5]);
		CLI_print("  %-40s : %02x:%02x:%02x:%02x:%02x:%02x \n",
			"L2TP TunnelAddr",
			entry.L2TP_Tunnel_address.b[0],
			entry.L2TP_Tunnel_address.b[1],
			entry.L2TP_Tunnel_address.b[2],
			entry.L2TP_Tunnel_address.b[3],
			entry.L2TP_Tunnel_address.b[4],
			entry.L2TP_Tunnel_address.b[5]);

		CLI_print("  %-40s : %02x:%02x:%02x:%02x:%02x:%02x \n",
			" My_SA_mac ",
			entry.SA_My_Mac.b[0],
			entry.SA_My_Mac.b[1],
			entry.SA_My_Mac.b[2],
			entry.SA_My_Mac.b[3],
			entry.SA_My_Mac.b[4],
			entry.SA_My_Mac.b[5]);


		CLI_print("  %-40s : 0x%04x   \n",
			"CFM OAM EtherType", entry.CFM_OAM.EtherType);
		CLI_print("  %-40s : %02x:%02x:%02x:%02x:%02x:%02x \n",
			"CFM OAM UC Mac Address",
			entry.CFM_OAM.UC_Mac_Address.b[0],
			entry.CFM_OAM.UC_Mac_Address.b[1],
			entry.CFM_OAM.UC_Mac_Address.b[2],
			entry.CFM_OAM.UC_Mac_Address.b[3],
			entry.CFM_OAM.UC_Mac_Address.b[4],
			entry.CFM_OAM.UC_Mac_Address.b[5]);
		CLI_print("  %-40s : %02x:%02x:%02x:%02x:%02x:%02x \n",
			"CFM OAM MC Mac Address",
			entry.CFM_OAM.MC_Mac_Address.b[0],
			entry.CFM_OAM.MC_Mac_Address.b[1],
			entry.CFM_OAM.MC_Mac_Address.b[2],
			entry.CFM_OAM.MC_Mac_Address.b[3],
			entry.CFM_OAM.MC_Mac_Address.b[4],
			entry.CFM_OAM.MC_Mac_Address.b[5]);
		CLI_print("  %-40s : %s \n", "Periodic counter ", ENET_ASSERT_ENABLE_DISABLE_STR(entry.Periodic_enable));

		CLI_print("  SSSMII RX configuration register:\n");
		CLI_print("      %-36s : %d * 8 bytes\n", "RX memory segment size", entry.if_sssmii_rx_config.val.rx_memory_segment_size);
		CLI_print("      %-36s : %d * 8 bytes\n", "RX ready threshold", entry.if_sssmii_rx_config.val.rx_ready_threshold);
		CLI_print("      %-36s : %d \n", "RX select control", entry.if_sssmii_rx_config.val.rx_select_control);
		CLI_print("  SSSMII TX configuration register 0:\n");
		CLI_print("      %-36s : %s\n", "TX Speed", (entry.if_sssmii_tx_config0.val.speed == 0) ? "10Mbps" : "100Mbps");
		CLI_print("      %-36s : %s\n", "TX Duplex mode", (entry.if_sssmii_tx_config0.val.duplex == 0) ? "Half" : "Full");
		CLI_print("      %-36s : %s\n", "TX Jabber", (entry.if_sssmii_tx_config0.val.jabber == 0) ? "O.K" : "Error");
		CLI_print("      %-36s : %d\n", "TX IPG", entry.if_sssmii_tx_config0.val.ipg + 1);
		CLI_print("      %-36s : %d\n", "TX Preamble", entry.if_sssmii_tx_config0.val.preamble);
		CLI_print("  SSSMII TX configuration register 1:\n");
		CLI_print("      %-36s : 0x%04x (1 bit per port)\n", "TX Link Up", entry.if_sssmii_tx_config1.val.tx_link_up);
		CLI_print("      %-36s : %d * 8 bytes\n", "TX memory segment size", entry.if_sssmii_tx_config1.val.tx_memory_segment_size);
		CLI_print("      %-36s : %d * 8 bytes\n", "TX ready    threshold", entry.if_sssmii_tx_config1.val.tx_ready_threshold);
		CLI_print("      %-36s : %d * 8 bytes\n", "TX   threshold", entry.if_sssmii_tx_config1.val.tx_transmit_threshold);
		
		CLI_print("      %-36s : %d.%d Mhz\n", "System clock ", (MEA_Uint32)(((MEA_Uint32)MEA_GLOBAL_SYS_Clock) / 1000000), (MEA_Uint32)(((MEA_Uint32)MEA_GLOBAL_SYS_Clock) % 1000000) );
        {
         char buf[128];
         MEA_OS_memset(buf,'\0',sizeof(buf));

         MEA_OS_formatNumVal(buf, MEA_GLOBAL_SYS_Clock);
         CLI_print("      %-36s :  %s \n","System clock",buf);

        }
        
        CLI_print("  LX Version register:     \n");
		CLI_print("      %-36s : %d Mhz\n", "dp DDR ", entry.verInfo.val.dpDdr);

		CLI_print("      %-36s : %d \n", "forwarder Width ", entry.verInfo.val.forwarder_Addr_Width);
		//    CLI_print("  fragment chunk register:     \n");
		//     CLI_print("      %-36s : %s \n", "chunk size ", (entry.fragment.val.chunk_size == MEA_GLOBAL_FRAGMENT_CHUNK_128) ? "128" : 
		//                                                      (entry.fragment.val.chunk_size == MEA_GLOBAL_FRAGMENT_CHUNK_256) ? "256" : 
		//                                                      (entry.fragment.val.chunk_size == MEA_GLOBAL_FRAGMENT_CHUNK_512) ? "512" :"unknown");


		CLI_print("  %-40s : %s\n", "lag_enable", MEA_STATUS_STR(entry.if_global1.val.lag_enable));



		CLI_print("  1p1 count tick     \n");
		CLI_print("  %-40s : %ld\n", "down", entry.if_1p1_down_up_count.to_down);
		CLI_print("  %-40s : %ld\n", "up", entry.if_1p1_down_up_count.to_Up);


		CLI_print("  %-40s : enable =%s %s %d To >> %d\n", "Mirror ces port", MEA_STATUS_STR((entry.if_mirror_ces_port.val.enable)),
			((entry.if_mirror_ces_port.val.port_ces_type == 0) ? "portId=" : "CesId="), entry.if_mirror_ces_port.val.port_cesId, entry.if_mirror_ces_port.val.dest_src_port);

		CLI_print("  %-40s :[%3s] %d\n", "Interface_mirror_1G ", MEA_STATUS_STR(entry.Interface_mirror.val.en_monitor_1G), entry.Interface_mirror.val.port_monitor_1G);
		CLI_print("  %-40s :[%3s] %d\n", "Interface_mirror_10G ", MEA_STATUS_STR(entry.Interface_mirror.val.en_monitor_1G), entry.Interface_mirror.val.port_monitor_10G);
       

		CLI_print("  %-40s : %d\n", "Ingress shaper", entry.if_configure_3.val.if_ingress_shaper);

		CLI_print("  Header Compress and decompress\n");
		CLI_print("  %-40s : %s\n", "HC_Offset_Mode ", MEA_STATUS_STR(entry.HC_Offset_Mode));
		CLI_print("----------------------------------\n");
		CLI_print("  %-40s : %s\n", "Compress   bypass        ", MEA_STATUS_STR(entry.if_HDC_global.val.Comp_bypass));
		CLI_print("  %-40s : %s\n", "Compress   bypass aligner", MEA_STATUS_STR(entry.if_HDC_global.val.Comp_bypass_aligner));
		CLI_print("  %-40s : %s\n", "Decompress bypass        ", MEA_STATUS_STR(entry.if_HDC_global.val.Decomp_bypass));
		CLI_print("  %-40s : %s\n", "Decompress bypass aligner", MEA_STATUS_STR(entry.if_HDC_global.val.Decomp_bypass_aligner));
		CLI_print("  Header Compress EtherType\n");
		CLI_print("----------------------------------\n");
		CLI_print("  %-40s : 0x%04x\n", "Compress eth ", entry.if_HDC_EhterType.comp_eth);
		CLI_print("  %-40s : 0x%04x\n", "Learn eth ", entry.if_HDC_EhterType.learn_eth);
		CLI_print("  %-40s : 0x%04x\n", "Invalidate_eth eth ", entry.if_HDC_EhterType.Invalidate_eth);
		CLI_print("-------------------------------------------------------------\n");
		for (i = 0; i < MEA_MAX_LOCAL_MAC; i++) {
			CLI_print("  %-40s [%d]: %02x:%02x:%02x:%02x:%02x:%02x \n",
				"Local Mac Address", i,
				entry.Local_Mac_Address[i].b[0],
				entry.Local_Mac_Address[i].b[1],
				entry.Local_Mac_Address[i].b[2],
				entry.Local_Mac_Address[i].b[3],
				entry.Local_Mac_Address[i].b[4],
				entry.Local_Mac_Address[i].b[5]);
		}

		CLI_print("-------------------------------------------------------------\n");
		for (i = 0; i < MEA_MAX_LOCAL_IP; i++) {
			Ip_val = entry.Local_IP_Address[i];
			MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
			CLI_print("  %40s [%d] : %s \n", "Local_IP_Address", i, Ip_str);
			CLI_print("-------------------------------------------------------------\n");
		}

		CLI_print("-------------------------------------------------------------\n");
		CLI_print("  %40s  : 0x%x \n", "MyBFD_Discriminator", entry.MyBFD_Discriminator);
		CLI_print("-------------------------------------------------------------\n");

		CLI_print("-------------------------------------------------------------\n");
		Ip_val = entry.Local_BFD_SW_IP_Address;
		MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
		CLI_print("  %40s [%d] : %s \n", "BFD_SW_IP_Address", i, Ip_str);
		CLI_print("-------------------------------------------------------------\n");


		CLI_print("  %-40s : %02x:%02x:%02x:%02x:%02x:%02x \n",
			"Gre_src_mac",
			entry.nvgre_info.Gre_src_mac.b[0],
			entry.nvgre_info.Gre_src_mac.b[1],
			entry.nvgre_info.Gre_src_mac.b[2],
			entry.nvgre_info.Gre_src_mac.b[3],
			entry.nvgre_info.Gre_src_mac.b[4],
			entry.nvgre_info.Gre_src_mac.b[5]);
		CLI_print("  %40s  : 0x%x \n", "Gre outer vlanId", entry.nvgre_info.outer_vlanId);
		CLI_print("  %40s  : 0x%x \n", "Gre TTL", entry.nvgre_info.Gre_ttl);
		Ip_val = entry.nvgre_info.Gre_src_Ipv4;
		MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
		CLI_print("  %40s  : %s \n", "Gre_src_Ipv4", Ip_str);


		Ip_val = entry.Ipsec_info.ipsec_src_Ipv4;
		MEA_OS_inet_ntoa(MEA_OS_htonl(Ip_val), Ip_str, sizeof(Ip_str));
		CLI_print("  %40s  : %s \n", "ipsec_src_Ipv4", Ip_str);
		CLI_print("  %40s  : 0x%x \n", "ipsec_ttl", entry.Ipsec_info.ipsec_ttl);




		CLI_print("  %40s  : 0x%x \n", "EthType offset 12", entry.EthType_cam.val.offset12_EthType);
		CLI_print("  %40s  : 0x%x \n", "EthType offset 16", entry.EthType_cam.val.offset16_EthType);


		CLI_print("\n");
	}

	if ((MEA_OS_strcmp(argv[1], "all") == 0) ||
		(MEA_OS_strcmp(argv[1], "bm") == 0)) {

		CLI_print("BM Globals:\n"
			"-----------\n");

		CLI_print("  %-40s : %s \n", "BM       ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.bm_enable));
		CLI_print("  %-40s : %s \n", "Best effort  ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.mode_best_eff_enable));
		CLI_print("  %-40s : %s \n", "Policer  ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.policer_enable));
		CLI_print("  %-40s : %s \n", "Shaper Port   ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.shaper_port_enable));
		CLI_print("  %-40s : %s \n", "Shaper Cluster   ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.shaper_cluster_enable));
		CLI_print("  %-40s : %s \n", "Shaper PriQueue   ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.shaper_priQueue_enable));
		CLI_print("  %-40s : %s \n", "Bmqs     ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.bm_config_bmqs));
		CLI_print("  %-40s : %s \n", "Wred     ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.bm_config_wred));
		CLI_print("  %-40s : %s \n", "Mqs      ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.bm_config_mqs));
		CLI_print("  %-40s : %s \n", "mc_Mqs   ", MEA_STATUS_STR_ENABLE(entry.bm_config.val.mc_mqs_enable));
		CLI_print("  %-40s : %s \n", "PacketAnalyzer      ", MEA_STATUS_STR_ENABLE(entry.PacketAnalyzer.val.enable));
		CLI_print("  %-40s : 0x%04x \n", "PacketAnalyzer EtherType   ", entry.PacketAnalyzer.val.EtherType);
		CLI_print("  %-40s : 0x%04x \n", "PacketAnalyzer Port   ", entry.PacketAnalyzer.val.port);

		CLI_print("  %-40s : %s threshold=%d \n", "BM  watchdog     ", MEA_STATUS_STR_ENABLE(entry.bm_watchdog_parm.val.enable), entry.bm_watchdog_parm.val.wd_threshold);



		CLI_print("  %-40s : %s \n", "Priority Queue WFQ", (entry.bm_config.val.pri_q_mode == MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY) ? "RR" : "WFQ");
		CLI_print("  %-40s : %s \n", "Cluster WFQ", (entry.bm_config.val.Cluster_mode == MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY) ? "RR" : "WFQ");

		CLI_print("  %-40s : %d \n", "wred offset   ", entry.bm_config2.val.wred_offset);
		CLI_print("  %-40s : %d \n", "target_MTU    ", entry.bm_config2.val.target_MTU);
		CLI_print("  %-40s : %d micro-Sec \n", "reorder_frag_time_out", entry.reorder_frag_time_out);

		CLI_print("  %-40s : %d \n", "pkt short limit", entry.bm_config0.val.pkt_short_limit);
		CLI_print("  %-40s : %d \n", "pkt high limit", entry.bm_config0.val.pkt_high_limit);
		CLI_print("  %-40s : %d \n", "chunk short limit", entry.bm_config0.val.chunk_short_limit);
		CLI_print("  %-40s : %d \n", "chunk high limit", entry.bm_config0.val.chunk_high_limit);
		CLI_print("  %-40s : slow %d fast %d\n", "Policer Ticks ", entry.ticks_for_slow_service, entry.ticks_for_fast_service);
		CLI_print("  %-40s : slow %d fast %d\n", "ingflowPolicer Ticks ", entry.ticks_for_slow_IngFlow, entry.ticks_for_fast_IngFlow);

		CLI_print("  %-40s : slow %d fast %d \n", "CCM Ticks ", entry.CCM_Tick.val.slowTick, entry.CCM_Tick.val.fastTick);
		CLI_print("  %-40s : %d \n", "broadcast drop level", entry.bm_config.val.bcst_drp_lvl);
		CLI_print("WBRG EtherType swap\n");
		CLI_print("  %-40s : 0x%04x \n", "lan    EtherType", entry.WBRG_swap_Eth.val.lan_ethertype);
		CLI_print("  %-40s : 0x%04x \n", "access EtherType", entry.WBRG_swap_Eth.val.access_ethertype);



		CLI_print("  %-40s : %d (%s) \n", "w-red shift",
			entry.bm_config.val.wred_shift,
			(entry.bm_config.val.wred_shift == MEA_GLOBAL_WRED_SHIFT_K_EQ_6) ? "K=6" :
			(entry.bm_config.val.wred_shift == MEA_GLOBAL_WRED_SHIFT_K_EQ_7) ? "K=7" :
			(entry.bm_config.val.wred_shift == MEA_GLOBAL_WRED_SHIFT_K_EQ_8) ? "K=8" :
			(entry.bm_config.val.wred_shift == MEA_GLOBAL_WRED_SHIFT_K_EQ_9) ? "K=9" :
			"Unknown");



		CLI_print("  %-40s : %d (0x%04x) \n",
			"Max used descriptors high threshold",
			entry.max_used_descriptors_high,
			entry.max_used_descriptors_high);

		CLI_print("  %-40s : %d (0x%04x) \n",
			"Max used descriptors low threshold",
			entry.max_used_descriptors_low,
			entry.max_used_descriptors_low);

		CLI_print("  %-40s : %d (0x%04x) \n",
			"Max used buffers     high threshold",
			entry.max_used_buffers_high,
			entry.max_used_buffers_high);

		CLI_print("  %-40s : %d (0x%04x) \n",
			"Max used buffers     low threshold",
			entry.max_used_buffers_low,
			entry.max_used_buffers_low);
		CLI_print("  %-40s : %d (0x%04x) \n",
			"Shaper Port update fast ",
			entry.shaper_fast_port, entry.shaper_fast_port);
		CLI_print("  %-40s : %d \n",
			"Shaper Port update slow multiplexer ",
			entry.shaper_slow_multiplexer_port);

		CLI_print("  %-40s : %d (0x%04x) \n",
			"Shaper Cluster update fast ",
			entry.shaper_fast_cluster, entry.shaper_fast_cluster);
		CLI_print("  %-40s : %d \n",
			"Shaper cluster update slow multiplexer ",
			entry.shaper_slow_multiplexer_cluster);
		CLI_print("  %-40s : %d (0x%04x) \n",
			"Shaper PriQueue update fast ",
			entry.shaper_fast_PriQueue, entry.shaper_fast_PriQueue);
		CLI_print("  %-40s : %d \n",
			"Shaper PriQueue update slow multiplexer ",
			entry.shaper_slow_multiplexer_PriQueue);
		CLI_print("  %-40s : %d \n", "Afdx Ticks ", entry.ticks_for_afdx);
		CLI_print("DDR/descriptor delay \n");
		CLI_print("  %-40s : %d \n",
			"delay_data_DDR   ", entry.bm_set_delay.val.delay_data_DDR);
		CLI_print("  %-40s : %d \n",
			"delay_descriptor ", entry.bm_set_delay.val.delay_descriptor);





		CLI_print("\n");

	}
	if ((MEA_OS_strcmp(argv[1], "all") == 0) ||
		(MEA_OS_strcmp(argv[1], "device") == 0)) {
		CLI_print("Device:\n"
			"-----------\n");

		MEA_CLI_GlobalParser_Get();


		MEA_CLI_Show_Device_MCIpSubNet();
		CLI_print("-----------------------------------------------------------------\n");
		CLI_print("Spanning Tree Protocol:\n"
			"-----------------------\n");
		CLI_print("  %-40s : %s \n",
			"Stp Mode  ",
			(entry.stp_mode.val.mode == MEA_GLOBAL_STP_MODE_DISABLE) ? "DISABLE" :
			(entry.stp_mode.val.mode == MEA_GLOBAL_STP_MODE_STP) ? "STP" :
			(entry.stp_mode.val.mode == MEA_GLOBAL_STP_MODE_MSTP) ? "MSTP" : "Unknown");
		CLI_print("-----------------------------------------------------------------\n");
		CLI_print("Wall clock setting:\n"
			"--------------------\n");
		CLI_print("  %-40s : %s  %ld %ld\n",
			"Calibrate ", ((entry.WallClock_Calibrate.val.clock_Add_sub == 0) ? "Sub" : "Add"),
			entry.WallClock_Calibrate.val.clock_val, entry.WallClock_Calibrate.val.clock_grn);

		if (MEA_API_Globals_WallCLock_Get(MEA_UNIT_0, &entry_clock) != MEA_OK) {
			CLI_print("MEA_API_Globals_WallCLock_Get failed\n");
			return MEA_OK;
		}

		if (MEA_DRV_TS_MEASUREMENT_SUPPORT)
		{
			CLI_print("-----------------ts_measurement _ingress--------------------------------\n");

			mea_drv_show_event_value(1, "srcPort", entry.ts_measurement_ingress.srcPort);
			mea_drv_show_event_value(1, "L2_type", entry.ts_measurement_ingress.L2_type);
			mea_drv_show_event_value(1, "Net_Tag1", entry.ts_measurement_ingress.Net_Tag1);
			if (entry.ts_measurement_ingress.Ip_type == MEA_FALSE)
				mea_drv_show_event_value(4, "ipv4", 0);
			else
				mea_drv_show_event_value(4, "ipv6", 0);

			if (entry.ts_measurement_ingress.protocol_type == MEA_FALSE)
				mea_drv_show_event_value(4, "TCP", 0);
			else
				mea_drv_show_event_value(4, "UDP", 0);

			mea_drv_show_event_value(1, "L4_dst_port", entry.ts_measurement_ingress.L4_dst_port);

			CLI_print("-----------------ts_measurement _egress--------------------------------\n");

			mea_drv_show_event_value(1, "srcPort", entry.ts_measurement_egress.srcPort);
			mea_drv_show_event_value(1, "L2_type", entry.ts_measurement_egress.L2_type);
			mea_drv_show_event_value(1, "Net_Tag1", entry.ts_measurement_egress.Net_Tag1);
			if (entry.ts_measurement_egress.Ip_type == MEA_FALSE)
				mea_drv_show_event_value(4, "ipv4", 0);
			else
				mea_drv_show_event_value(4, "ipv6", 0);

			if (entry.ts_measurement_egress.protocol_type == MEA_FALSE)
				mea_drv_show_event_value(4, "TCP", 0);
			else
				mea_drv_show_event_value(4, "UDP", 0);


			mea_drv_show_event_value(1, "L4_dst_port", entry.ts_measurement_egress.L4_dst_port);




		}

		if (MEA_DRV_TS_MEASUREMENT_SUPPORT == MEA_TRUE)
		{
			MEA_Uint32 ret_maeasur = 0;
			if (MEA_API_Get_Ts_MEASUREMENT(MEA_UNIT_0, &ret_maeasur) != MEA_OK) {
				CLI_print("MEA_API_Get_Ts_MEASUREMENT failed\n");
			}
			CLI_print("Latency measurement :\n"
				"--------------------\n");
			if (ret_maeasur == 0xDEADDEAD) {
				CLI_print("No result DEAD\n");
			}
			else {
				CLI_print(" result: %d \n", ret_maeasur);
			}
			CLI_print("--------------------\n");




		}

#if (!defined(__KERNEL__))
		result = (time_t)entry_clock.sec;
		brokentime = localtime(&result);
		CLI_print("  %-40s : %s \n",
			"Current Clock sec ", asctime(brokentime));
#else
		CLI_print("  %-40s : %10ld  \n",
			"Current Clock sec ", entry_clock.sec);
		CLI_print("  %-40s : %ld \n",
			"Current Clock nsec ", entry_clock.nsec);
#endif
		CLI_print("-----------------------------------------------------------------\n");
		CLI_print("---------------------------------\n"
			"         POS Polling             \n"
			"---------------------------------\n");
		CLI_print("    Tx4  Tx3  Tx2  Tx1  Rx2  Rx1 \n");
		CLI_print("    ---- ---- ---- ---- ---- ----\n");
		CLI_print("    %4d %4d %4d %4d %4d %4d\n",
			entry.pos_config.polling.val.Tx4,
			entry.pos_config.polling.val.Tx3,
			entry.pos_config.polling.val.Tx2,
			entry.pos_config.polling.val.Tx1,
			entry.pos_config.polling.val.Rx2,
			entry.pos_config.polling.val.Rx1);
		CLI_print("-----------------------------------------------------------------\n");
	}



	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_bm_enable(int argc, char *argv[])
{


	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.bm_enable = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_GlobalOamRemove(int argc, char* argv[])
{
#if 0
	MEA_Uint32 index, ethertype = 0;
	MEA_Globals_Entry_dbt entry;
	MEA_Bool remove_all;

	if (argc != 2) {
		return MEA_ERROR;
	}

	if (MEA_OS_strcmp(argv[1], "all") == 0)
		remove_all = MEA_TRUE;
	else
	{
		remove_all = MEA_FALSE;
		ethertype = MEA_OS_atohex(argv[1]);
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	if (remove_all)
	{
		MEA_OS_memset(&(entry.OAM_data), 0, sizeof(entry.OAM_data));
	}
	else
	{
		for (index = 0; index < MEA_GLOBALS_OAM_NUM_OF_ENTRIES; index++)
		{
			if (entry.OAM_data[index].valid == MEA_TRUE &&
				entry.OAM_data[index].ethertype == ethertype)
			{
				entry.OAM_data[index].valid = 0;
				break;
			}
		}
	}

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
}


	CLI_print("Done.\n");
#else
	CLI_print("Not support\n");
#endif


	return MEA_OK;
}

static MEA_Status MEA_CLI_Set_GlobalOamAdd(int argc, char *argv[])
{

#if 0
	MEA_Uint32 index, l2cp, ethertype;
	MEA_Globals_Entry_dbt entry;

	if (argc != 3) {
		return MEA_ERROR;
	}
	ethertype = MEA_OS_atohex(argv[1]);
	l2cp = MEA_OS_atoi(argv[2]);

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	for (index = 0; index < MEA_GLOBALS_OAM_NUM_OF_ENTRIES; index++)
	{
		if (entry.OAM_data[index].valid != MEA_TRUE)
		{
			entry.OAM_data[index].ethertype = ethertype;
			entry.OAM_data[index].l2cp = l2cp;
			entry.OAM_data[index].valid = MEA_TRUE;
			break;
		}
	}
	if (index == MEA_GLOBALS_OAM_NUM_OF_ENTRIES)
	{
		CLI_print("Global OAM is full\n");
		return MEA_OK;
	}

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}


	CLI_print("Done.\n");
#else
	CLI_print("not support\n");
#endif
	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_ShaperPriQueueTicks(int argc, char* argv[]) {


	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 fast;
	MEA_Uint32 slow_multiplexer;

	if (argc != 3)
	{
		return MEA_ERROR;
	}

	fast = MEA_OS_atoi(argv[1]);
	slow_multiplexer = MEA_OS_atoi(argv[2]);

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.shaper_fast_PriQueue = fast;
	entry.shaper_slow_multiplexer_PriQueue = slow_multiplexer;

	if (entry.shaper_slow_multiplexer_PriQueue != slow_multiplexer) // slow_multiplexer > # of bits in shaper_slow_multiplexer
	{
		return MEA_ERROR;
	}

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}


	CLI_print("Done.\n");

	return MEA_OK;
}


static  MEA_Status MEA_CLI_Set_ShaperClusterTicks(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 fast;
	MEA_Uint32 slow_multiplexer;

	if (argc != 3)
	{
		return MEA_ERROR;
	}

	fast = MEA_OS_atoi(argv[1]);
	slow_multiplexer = MEA_OS_atoi(argv[2]);

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.shaper_fast_cluster = fast;
	entry.shaper_slow_multiplexer_cluster = slow_multiplexer;



	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}


	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_ShaperPortTicks(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 fast;
	MEA_Uint32 slow_multiplexer;

	if (argc != 3)
	{
		return MEA_ERROR;
	}

	fast = MEA_OS_atoi(argv[1]);
	slow_multiplexer = MEA_OS_atoi(argv[2]);

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.shaper_fast_port = fast;
	entry.shaper_slow_multiplexer_port = slow_multiplexer;

	if (entry.shaper_slow_multiplexer_port != slow_multiplexer) // slow_multiplexer > # of bits in shaper_slow_multiplexer
	{
		return MEA_ERROR;
	}

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}


	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status MEA_CLI_Set_IngFlowPolicerTicks(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;


	if (argc != 3)
	{
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.ticks_for_slow_IngFlow = MEA_OS_atoi(argv[1]);
	entry.ticks_for_fast_IngFlow = MEA_OS_atoi(argv[2]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status MEA_CLI_Set_PolicerTicks(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;


	if (argc != 3)
	{
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.ticks_for_slow_service = MEA_OS_atoi(argv[1]);
	entry.ticks_for_fast_service = MEA_OS_atoi(argv[2]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_CCMTicks(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;


	if (argc != 3)
	{
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.CCM_Tick.val.slowTick = MEA_OS_atoi(argv[1]);
	entry.CCM_Tick.val.fastTick = MEA_OS_atoi(argv[2]);


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_RxAfdxTicks(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 value;

	if (argc  <2)
	{
		return MEA_ERROR;
	}

	value = MEA_OS_atoi(argv[1]);


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.ticks_for_afdx = value;



	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}


	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_vcmux_BCM(int argc, char* argv[])
{


	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}


	CLI_print(" not support failed\n");
	return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;


}


static MEA_Status  MEA_CLI_Set_Globals_best_effort_enable(int argc, char* argv[])
{


	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.mode_best_eff_enable = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;


}

static MEA_Status  MEA_CLI_Set_Globals_WBRG_swap(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;



	if (argc < 3) {
		return MEA_ERROR;
	}


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.WBRG_swap_Eth.val.lan_ethertype = MEA_OS_atohex(argv[1]);
	entry.WBRG_swap_Eth.val.access_ethertype = MEA_OS_atohex(argv[2]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_set_reorder_frag_time_out(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.reorder_frag_time_out = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_set_frag_Type(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val[2];


	if (argc < 3) {
		return MEA_ERROR;
	}

	val[0] = 0;
	val[1] = 0;

	val[0] = MEA_OS_atoi(argv[1]);
	val[1] = MEA_OS_atoi(argv[2]);




	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config2.val.en_externl_frag_internal = val[0];
	entry.bm_config2.val.en_frag_after_fragment = val[1];


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}

static MEA_Status  MEA_CLI_Set_Globals_set_target_MTU(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config2.val.target_MTU = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_wred_offset(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 255) {
		CLI_print("Value (%d) out of range 0..255\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config2.val.wred_offset = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}


static MEA_Status MEA_CLI_Set_Globals_shaper_enable(int argc, char *argv[])
{


	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 shaper_port_enable;
	MEA_Uint32 shaper_cluster_enable;
	MEA_Uint32 shaper_priQueue_enable;



	if (argc < 4) {
		return MEA_ERROR;
	}

	shaper_port_enable = MEA_OS_atoi(argv[1]);
	shaper_cluster_enable = MEA_OS_atoi(argv[2]);
	shaper_priQueue_enable = MEA_OS_atoi(argv[3]);

	if ((shaper_port_enable > 1)) {
		CLI_print("shaper_port_enable Value (%d) out of range 0..1\n", shaper_port_enable);
		return MEA_ERROR;
	}
	if ((shaper_cluster_enable > 1)) {
		CLI_print("shaper_cluster_enable Value (%d) out of range 0..1\n", shaper_cluster_enable);
		return MEA_ERROR;
	}
	if ((shaper_priQueue_enable > 1)) {
		CLI_print("shaper_priQueue_enable Value (%d) out of range 0..1\n", shaper_priQueue_enable);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.shaper_port_enable = shaper_port_enable;
	entry.bm_config.val.shaper_cluster_enable = shaper_cluster_enable;
	entry.bm_config.val.shaper_priQueue_enable = shaper_priQueue_enable;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_PacketAnalyzer_Port(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	entry.PacketAnalyzer.val.port = MEA_OS_atoi(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_PacketAnalyzer_enable(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	entry.PacketAnalyzer.val.enable = MEA_OS_atoi(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;

}


static MEA_Status MEA_CLI_Set_Globals_PacketAnalyzer_EtherType(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	entry.PacketAnalyzer.val.EtherType = MEA_OS_atohex(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_mirror_ingress_10G(int argc, char* argv[])

{

	MEA_Globals_Entry_dbt entry;

	MEA_Bool            enable = MEA_FALSE;
	MEA_Uint32           port = 0;



	if (argc < 2) {
		return MEA_ERROR;
	}




	enable = MEA_OS_atoi(argv[1]);
	port = MEA_OS_atoi(argv[2]);





	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}




	entry.Interface_mirror.val.en_monitor_10G = enable;
	entry.Interface_mirror.val.port_monitor_10G = port;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;

}


static MEA_Status MEA_CLI_Set_Globals_mirror_ingress_1G(int argc, char* argv[])

{

	MEA_Globals_Entry_dbt entry;

	MEA_Bool            enable = MEA_FALSE;
	MEA_Uint32           port = 0;



	if (argc < 2) {
		return MEA_ERROR;
	}




	enable = MEA_OS_atoi(argv[1]);
	port = MEA_OS_atoi(argv[2]);

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}

	
    entry.Interface_mirror.val.en_monitor_1G = enable;
    entry.Interface_mirror.val.port_monitor_1G = port;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_mirror_port_ces(int argc, char* argv[])

{

	MEA_Globals_Entry_dbt entry;

	MEA_Bool            enable = MEA_FALSE;
	MEA_Bool            port_ces_type = 0;
	MEA_Uint32           port_cesId = 0;
	MEA_Port_t          dest_src_port = 100;
	// 	MEA_Bool invertPcmRx=MEA_FALSE;
	// 	MEA_Bool invertPcmTx=MEA_FALSE;
	// 	MEA_Bool invertSbiRx=MEA_FALSE;
	// 	MEA_Bool invertSbiTx=MEA_FALSE;

	if (argc < 2) {
		return MEA_ERROR;
	}

	if (argc < 5)
	{
		return MEA_ERROR;
	}

	if (argc == 5) {
		enable = MEA_OS_atoi(argv[1]);
		port_ces_type = MEA_OS_atoi(argv[2]);
		port_cesId = MEA_OS_atoi(argv[3]);
		dest_src_port = MEA_OS_atoi(argv[4]);
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}

	entry.if_mirror_ces_port.val.enable = enable;
	entry.if_mirror_ces_port.val.port_ces_type = port_ces_type;
	entry.if_mirror_ces_port.val.port_cesId = port_cesId;
	entry.if_mirror_ces_port.val.dest_src_port = dest_src_port;




	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;

}


static  MEA_Status  MEA_CLI_Set_Globals_forwarder_OutPut(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1)
		return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	entry.if_global1.val.Dse_Output_Algoritm = val;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static  MEA_Status MEA_CLI_Set_Globals_rstp_enable(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1)
		return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	entry.if_global1.val.rstp_enable = val;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_mstp_enable(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1)
		return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	entry.if_global1.val.mstp_enable = val;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static  MEA_Status  MEA_CLI_Set_Globals_learn_disable(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1)
		return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	entry.if_global1.val.learning_disable = val;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_check_srv_key(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	MEA_Uint32            val1;

	if (argc != 2) {

		return MEA_ERROR;

	}



	val1 = MEA_OS_atoi(argv[1]);

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}

	entry.check_srv_key = val1;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");
	return MEA_OK;

}


static MEA_Status MEA_CLI_Set_Globals_learning_disable(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1)
		return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	entry.if_global1.val.learning_disable = val;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;


}


static MEA_Status  MEA_CLI_Set_Globals_forwarder_enable(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1)
		return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	entry.if_global1.val.forwarder_enable = val;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_lag_enable(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1)
		return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	entry.if_global1.val.lag_enable = val;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}

static MEA_Status MEA_CLI_Set_Globals_forwarder_act_offset(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1)
		return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("Error MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	if (val > 3) {
		CLI_print("Error max value %d > 3\n", val);
		return MEA_OK;
	}

	entry.if_global0.val.fwd_act_offset = val;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_forwarder_diseableDelReinit(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	MEA_Uint32            val1;

	if (argc != 2) {

		return MEA_ERROR;

	}



	val1 = MEA_OS_atoi(argv[1]);

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}

	entry.forwarder.disable_fwd_del_on_reinit = val1;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");
	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_forwarder_aging(int argc, char* argv[]) {

#if 1

	CLI_print("Error: Please use the command MEA forwarder aging \n");

#else
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 enable = 0;
	MEA_Uint32 last_table = 0;
	MEA_Uint32 sweep_interval = 0;
	MEA_Uint32 seconds, clock;
	if (argc < 2) {
		return MEA_ERROR;
	}
	enable = MEA_OS_atoi(argv[1]);
	if (enable != 0) {
		if (argc != 4)
		{
			return MEA_ERROR;
		}
		last_table = MEA_OS_atoi(argv[2]);
		if (last_table>15) {
			CLI_print("Value last_table(%d) out of range 0..15\n", last_table);
			return MEA_ERROR;
		}

		sweep_interval = MEA_OS_atoi(argv[3]);
	}
	else {
		if (enable == 0) {
			if (argc != 2) {
				return MEA_ERROR;
			}
		}
		else {
			return MEA_ERROR;
		}
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	entry.macAging.val.enable = enable;
	if (enable) {
		entry.macAging.val.lastTable = last_table;
		entry.macAging.val.sweepInterval = sweep_interval;
	}


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	clock = (MEA_Uint32)MEA_GLOBAL_SYS_Clock;

	seconds = (clock);
	seconds /= (1 * (1 << entry.verInfo.val.forwarder_Addr_Width));
	seconds = (MEA_DSE_AGING_GAN * entry.macAging.val.sweepInterval) / (MEA_DSE_AGING_GAN1*(seconds));
	MEA_DSE_Aging_time = seconds;



	MEA_DSE_admin_status = entry.macAging.val.enable;



	CLI_print("Done.\n");
#endif
	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_forwarder_Config_hash(int argc, char* argv[])

{

	MEA_Globals_Entry_dbt entry;

	MEA_Uint32            index;
	MEA_Uint32            val1, val2;



	if (argc < 3) {
		return MEA_ERROR;
	}


	index = MEA_OS_atoi(argv[1]);
	val1 = MEA_OS_atoi(argv[2]);
	val2 = MEA_OS_atoi(argv[3]);

	if (index > 7) {
		CLI_print("key should be 0:7\n");
		return MEA_ERROR;
	}
	if (val1 > 3) {
		CLI_print("hash1 should be 0:2\n");
		return MEA_ERROR;
	}

	if (val2 > 3) {
		CLI_print("hash2 should be 0:2\n");
		return MEA_ERROR;
	}


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}

	switch (index)
	{
	case 0:
		entry.if_fwd_hash.val.key0_hashO = val1;
		entry.if_fwd_hash.val.key0_hash1 = val2;

		break;
	case 1:
		entry.if_fwd_hash.val.key1_hashO = val1;
		entry.if_fwd_hash.val.key1_hash1 = val2;

		break;
	case 2:
		entry.if_fwd_hash.val.key2_hashO = val1;
		entry.if_fwd_hash.val.key2_hash1 = val2;

		break;
	case 3:
		entry.if_fwd_hash.val.key3_hashO = val1;
		entry.if_fwd_hash.val.key3_hash1 = val2;

		break;
	case 4:
		entry.if_fwd_hash.val.key4_hashO = val1;
		entry.if_fwd_hash.val.key4_hash1 = val2;

		break;
	case 5:
		entry.if_fwd_hash.val.key5_hashO = val1;
		entry.if_fwd_hash.val.key5_hash1 = val2;
		break;
	case 6:
		entry.if_fwd_hash.val.key6_hashO = val1;
		entry.if_fwd_hash.val.key6_hash1 = val2;

		break;

	case 7:
		entry.if_fwd_hash.val.key7_hashO = val1;
		entry.if_fwd_hash.val.key7_hash1 = val2;
		break;
	}

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}


	CLI_print("Done.\n");
	return MEA_OK;
}


static  MEA_Status  MEA_CLI_Set_Globals_SRV_extenal_hash_type(int argc, char* argv[])

{

	MEA_Globals_Entry_dbt entry;

	MEA_Uint32            val1;



	if (argc <2) {

		return MEA_ERROR;

	}



	val1 = MEA_OS_atoi(argv[1]);





	if (val1 > 3)

		return MEA_ERROR;




	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}





	entry.if_global0.val.srv_external_hash_function = val1;



	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;

}

static MEA_Status  MEA_CLI_Set_Globals_forwarder_addr_width(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}
	val = MEA_OS_atoi(argv[1]);
	if (val > 64) {
		CLI_print("error the value is bigger then 64\n");
		return MEA_ERROR;
	}


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	/* we not write to HW because is read only we update the global variable*/
	entry.verInfo.val.forwarder_Addr_Width = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_close_loop_utopia(int argc, char* argv[])

{

	MEA_Globals_Entry_dbt entry;

	MEA_Uint32            val;



	if (argc != 2) {

		return MEA_ERROR;

	}



	val = MEA_OS_atoi(argv[1]);





	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}





	switch (val) {
	case 2:
		entry.if_global1.val.close_loop_utopia_master_tx_to_slave_tx = 0;
		entry.if_global1.val.close_loop_utopia_master_tx_to_master_rx = 1;
		break;
	case 1:
		entry.if_global1.val.close_loop_utopia_master_tx_to_slave_tx = 1;
		entry.if_global1.val.close_loop_utopia_master_tx_to_master_rx = 0;
		break;
	case 0:
		entry.if_global1.val.close_loop_utopia_master_tx_to_slave_tx = 0;
		entry.if_global1.val.close_loop_utopia_master_tx_to_master_rx = 0;
		break;
	default:
		return MEA_ERROR;
	}




	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;

}



static MEA_Status MEA_CLI_Set_Globals_PacketGen(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	entry.if_global0.val.generator_enable = MEA_OS_atoi(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;

}


static  MEA_Status  MEA_CLI_Set_Globals_WD_Disable(int argc, char* argv[])

{

	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;


	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val != 0 && val != 1) {
		CLI_print("value is not correct only 1 or 0 are allowed.\n");

		return MEA_ERROR;
	}


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}


	entry.if_global1.val.wd_disable = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}


	CLI_print("Done.\n");



	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_mac_fifo_threshold(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;
	int i;

	MEA_Uint32            type;
	MEA_rx_tx_mac_thresholds_t *MAC_thd = NULL;

	if (argc <= 2) {

		return MEA_ERROR;

	}

	type = MEA_OS_atoi(argv[1]);
	/* 0  GBE MAC   */
	/* 1  g999 MAC  */
	/* 2  TLS MAC  */
	/* 3  CPU MAC*/




	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;

	}

	switch (type)
	{
	case 0:
		MAC_thd = &entry.if_global2_Gbe;
		break;

	case 1:
		MAC_thd = &entry.if_threshold_G999;
		break;

	case 2:
		MAC_thd = &entry.if_threshold_TLS;
		break;
	case 3:
		MAC_thd = &entry.if_threshold_CPU;
		break;
	default:
		CLI_print("Type is not correct\n");
		return MEA_OK;
		break;

	}




	for (i = 2; i < argc; i++) {

		if (!MEA_OS_strcmp(argv[i], "-AF_th")) {
			if (i + 1 > argc) {
				CLI_print("miss parameter %s\n", argv[i]);
				return MEA_OK;
			}
			MAC_thd->val.AF_threshold = MEA_OS_atoiNum(argv[i + 1]);

		}

		if (!MEA_OS_strcmp(argv[i], "-tx")) {
			if (i + 1 > argc) {
				CLI_print("miss parameter %s\n", argv[i]);
				return MEA_OK;
			}
			MAC_thd->val.tx_threshold = MEA_OS_atoiNum(argv[i + 1]);
		}

		if (!MEA_OS_strcmp(argv[i], "-rx")) {
			if (i + 1 > argc) {
				CLI_print("miss parameter %s\n", argv[i]);
				return MEA_OK;
			}
			MAC_thd->val.RX_mac_threshold = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-eoc")) {
			if (i + 1 > argc) {
				CLI_print("miss parameter %s\n", argv[i]);
				return MEA_OK;
			}
			MAC_thd->val.rx_pre_EOC_threshold = MEA_OS_atoiNum(argv[i + 1]);

		}

	}

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_frag_art_eop(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;


	if (argc != 2) {

		return MEA_ERROR;

	}


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}


	entry.if_configure_1.val.thresh_frag_art_eop = MEA_OS_atoi(argv[1]);



	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_ipg(int argc, char* argv[])

{


	MEA_Globals_Entry_dbt entry;

	MEA_Uint32            val1, val2;




	if (argc != 3) {

		return MEA_ERROR;

	}



	val1 = MEA_OS_atoi(argv[1]);

	val2 = MEA_OS_atoi(argv[2]);

	if (val1 > 256)

		return MEA_ERROR;




	if (val2 > 256)

		return MEA_ERROR;


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}


	entry.if_global0.val.ipg_gmii = val1;

	entry.if_global0.val.ipg_mii = val2;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}

	CLI_print("Done.\n");



	return MEA_OK;


}


static MEA_Status  MEA_CLI_Set_Globals_en_ddr_nic(int argc, char* argv[])

{


	MEA_Globals_Entry_dbt entry;

	MEA_Uint32            val1;




	if (argc < 2) {

		return MEA_ERROR;

	}



	val1 = MEA_OS_atoi(argv[1]);






	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}





	entry.if_global0.val.en_ddr_nic = val1;



	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;


}


static MEA_Status  MEA_CLI_Set_Globals_alow_ingflow_pol_zero(int argc, char* argv[])

{


	MEA_Globals_Entry_dbt entry;



	if (argc < 2) {

		return MEA_ERROR;

	}





	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Get_Globals_Entry failed\n");

		return MEA_OK;

	}



	entry.alow_ingflow_pol_zero = MEA_OS_atoi(argv[1]);;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		CLI_print("MEA_API_Set_Globals_Entry failed\n");

		return MEA_OK;

	}



	CLI_print("Done.\n");



	return MEA_OK;


}


static MEA_Status MEA_CLI_PeriodicEnable(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	int i;

	if (argc < 2) {
		CLI_print("Error minimum parameter \n");
		return MEA_ERROR;
	}

	if (MEA_OS_atoi(argv[1]) != 1 && MEA_OS_atoi(argv[1]) != 0) {

		CLI_print("Error parameters only 0 or 1 is available \n");
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		return MEA_ERROR;
	}

	if (MEA_OS_atoi(argv[1]) == 1) {

		entry.Periodic_enable = MEA_TRUE;

	}
	else {
		entry.Periodic_enable = MEA_FALSE;
	}

	if (argc >= 3) {
		entry.portEgress_Link_update = MEA_OS_atoi(argv[2]);
	}
	if (argc >= 4) {
		entry.portIngress_Link_update = MEA_OS_atoi(argv[3]);
	}

	for (i = 1; i < argc; i++)
	{

		if (!strcmp(argv[i], "-linkEgress")) {
			if (i + 1 >= argc) {
				CLI_print("ERROR: Not enough parameters for option %s\n", argv[i]);
				return MEA_ERROR;
			}
			entry.portEgress_Link_update = MEA_OS_atoi(argv[i + 1]);


		}
		if (!strcmp(argv[i], "-linkIngress")) {
			if (i + 1 >= argc) {
				CLI_print("ERROR: Not enough parameters for option %s\n", argv[i]);
				return MEA_ERROR;
			}
			entry.portIngress_Link_update = MEA_OS_atoi(argv[i + 1]);


		}
		if (!strcmp(argv[i], "-linkBonding")) {
			if (i + 1 >= argc) {
				CLI_print("ERROR: Not enough parameters for option %s\n", argv[i]);
				return MEA_ERROR;
			}
			entry.portBondingLink_update = MEA_OS_atoi(argv[i + 1]);


		}




	}



	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("ERROR: %s to config globals", __FUNCTION__);
		return MEA_ERROR;
	}

	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_GPIO_RW_Z70(int argc, char *argv[])
{

	MEA_Uint32 value = 0;
	MEA_Uint32 read_write = 0;


	if (argc < 2) {
		CLI_print("Error minimum parameter \n");
		return MEA_ERROR;
	}



	if (!strcmp(argv[1], "read")) {
		read_write = 0;
	}
	if (!strcmp(argv[1], "write")) {
		read_write = 1;
		if (argc + 1 < 3) {
			CLI_print("Error minimum parameter \n");
			return MEA_ERROR;
		}
		value = MEA_OS_atoiNum(argv[2]);
	}



	if (MEA_API_GPIO_Z70_RW(MEA_UNIT_0, read_write, &value) != MEA_OK) {
		return MEA_ERROR;
	}
	if (read_write == 0) {
		CLI_print("GPI0 read 0x%08x \n", value);
	}
	else {
		CLI_print("GPI0 write 0x%08x \n", value);
	}


	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_Queue_Handler(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	int i;

	if (argc < 2) {
		CLI_print("Error minimum parameter \n");
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		return MEA_ERROR;
	}




	for (i = 1; i < argc; i++)
	{
		if (!strcmp(argv[i], "-reset")) {
			if (i + 1 >= argc) {
				CLI_print("ERROR: Not enough parameters for option %s\n", argv[i]);
				return MEA_ERROR;
			}

			if (MEA_OS_atoi(argv[i + 1]) == 0) {
				CLI_print("ERROR: Not reset can to be 0 sec\n");
				return MEA_ERROR;
			}
			entry.Queue_reset_sec = MEA_OS_atoi(argv[i + 1]);


		}

		if (!strcmp(argv[i], "-mqsTH")) {
			if (i + 1 >= argc) {
				CLI_print("ERROR: Not enough parameters for option %s\n", argv[i]);
				return MEA_ERROR;
			}
			entry.QueueTH_MQS = MEA_OS_atoi(argv[i + 1]);


		}

		if (!strcmp(argv[i], "-PacketTH")) {
			if (i + 1 >= argc) {
				CLI_print("ERROR: Not enough parameters for option %s\n", argv[i]);
				return MEA_ERROR;
			}
			entry.QueueTH_currentPacket = MEA_OS_atoi(argv[i + 1]);
		}




	}



	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("ERROR: %s to config globals", __FUNCTION__);
		return MEA_ERROR;
	}

	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_link_up_mask(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            val;

	if (argc != 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1)
		return MEA_ERROR;

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	entry.if_global1.val.link_up_mask = val;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_MyBFD_Discriminator(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	//MEA_Uint8  index;

	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}



	entry.MyBFD_Discriminator = MEA_OS_atoiNum(argv[1]);


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static  MEA_Status  MEA_CLI_Set_Globals_Local_BFD_SW_IP_Addresss(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.Local_BFD_SW_IP_Address = MEA_OS_inet_addr(argv[1]);


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_Globals_Local_IP_Address(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint8  index;
	if (argc < 3) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	index = MEA_OS_atoi(argv[1]);
	if (index >= MEA_MAX_LOCAL_IP) {
		CLI_print("MEA_API_Get_Globals_Entry failed the max of index for Local_IP is %d\n", MEA_MAX_LOCAL_MAC);
		return MEA_OK;
	}

	entry.Local_IP_Address[index] = MEA_OS_inet_addr(argv[2]);


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_Local_Mac_Address(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;
	MEA_Uint8  index;
	if (argc < 3) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	index = MEA_OS_atoi(argv[1]);
	if (index >= MEA_MAX_LOCAL_IP) {
		CLI_print("MEA_API_Get_Globals_Entry failed the max of index for Local_Mac_Address is %d\n", MEA_MAX_LOCAL_IP);
		return MEA_OK;
	}


	if (MEA_OS_atohex_MAC(argv[2], &(entry.Local_Mac_Address[index])) != MEA_OK) {
		CLI_print("ERROR : MEA_OS_atohex_MAC fail \n");
		return MEA_ERROR;
	}
	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;


}


static MEA_Status MEA_CLI_Set_Globals_cfm_oam_MC_Mac(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	if (MEA_OS_atohex_MAC(argv[1], &(entry.CFM_OAM.MC_Mac_Address)) != MEA_OK) {
		CLI_print("ERROR : MEA_OS_atohex_MAC fail \n");
		return MEA_ERROR;
	}
	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_cfm_oam_UC_Mac(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	if (MEA_OS_atohex_MAC(argv[1], &(entry.CFM_OAM.UC_Mac_Address)) != MEA_OK) {
		CLI_print("ERROR : MEA_OS_atohex_MAC fail \n");
		return MEA_ERROR;
	}
	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_cfm_oam_EtherType(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	entry.CFM_OAM.EtherType = (MEA_Uint16)MEA_OS_atohex(argv[1]);
	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_my_SA_Mac(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	if (MEA_OS_atohex_MAC(argv[1], &(entry.SA_My_Mac)) != MEA_OK) {
		CLI_print("ERROR : MEA_OS_atohex_MAC fail \n");
		return MEA_ERROR;
	}
	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_cfm_oam_Valid(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	entry.CFM_OAM.valid = (MEA_Uint16)MEA_OS_atoi(argv[1]);
	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static  MEA_Status MEA_CLI_Set_Globals_network_L2TP_tunnelAddr(int argc, char* argv[]) {
	MEA_Globals_Entry_dbt entry;
	if (argc < 2) {
		return MEA_ERROR;
	}
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	MEA_OS_atohex_MAC(argv[1], &(entry.L2TP_Tunnel_address));
	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_network_L2CP_BaseAddr(int argc, char* argv[]) {



	MEA_Globals_Entry_dbt entry;

	if (argc < 2) {
		return MEA_ERROR;
	}


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	MEA_OS_atohex_MAC(argv[1], &(entry.network_L2CP_BaseAddr));

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_bm_pkt_short_limit(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc != 2) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config0.val.pkt_short_limit = (MEA_Uint16)(MEA_OS_atoi(argv[1]));

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_Globals_bm_pkt_high_limit(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc != 2) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config0.val.pkt_high_limit = (MEA_Uint16)(MEA_OS_atoi(argv[1]));

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_bm_chunk_short_limit(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc != 2) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config0.val.chunk_short_limit = (MEA_Uint16)(MEA_OS_atoi(argv[1]));

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_Globals_bm_chunk_high_limit(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc != 2) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config0.val.chunk_high_limit = (MEA_Uint16)(MEA_OS_atoi(argv[1]));

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status MEA_CLI_Set_Globals_pause_enable(int argc, char *argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;
	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..63\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_global0.val.pause_enable = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;


}


static MEA_Status MEA_CLI_Set_Globals_if_ingress_shaper(int argc, char *argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;
	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val < 7) {
		CLI_print("Value (%d) out of range 7..255\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_configure_3.val.if_ingress_shaper = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	return MEA_OK;


}


static MEA_Status MEA_CLI_Set_Globals_semi_back_pressure(int argc, char *argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;
	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 63) {
		CLI_print("Value (%d) out of range 0..63\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_global0.val.semi_back_pressure = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	return MEA_OK;


}



static MEA_Status  MEA_CLI_Set_Globals_Set_ACL_hash(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_global1.val.Acl_hash = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_Hash_Type(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 hash1, hash2;


	if (argc < 2) {
		return MEA_ERROR;
	}

	hash1 = MEA_OS_atoi(argv[1]);

	hash2 = MEA_OS_atoi(argv[2]);

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_global1.val.hash1 = hash1;
	entry.if_global1.val.hash2 = hash2;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_Lxcp_Enable(int argc, char *argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_global1.val.Lxcp_Enable = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_drop_unmatch_pkts(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;

	if (argc != 2) {
		return MEA_ERROR;
	}

	if (MEA_OS_atoi(argv[1]) > 1) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_global1.val.drop_unmatch_pkts = MEA_OS_atoi(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_sssmii_rx_config_reg(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc != 4) {
		return MEA_ERROR;
	}

	if (MEA_OS_atoi(argv[1]) > 63 ||
		MEA_OS_atoi(argv[2]) > 63 ||
		MEA_OS_atoi(argv[3]) > 15)
	{
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_sssmii_rx_config.val.rx_memory_segment_size = MEA_OS_atoi(argv[1]);
	entry.if_sssmii_rx_config.val.rx_ready_threshold = MEA_OS_atoi(argv[2]);
	entry.if_sssmii_rx_config.val.rx_select_control = MEA_OS_atoi(argv[3]);



	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_sssmii_tx_config_reg0(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;

	if (argc != 6) {
		return MEA_ERROR;
	}

	if (((MEA_Uint32)MEA_OS_atoi(argv[1])) > 1 ||
		((MEA_Uint32)MEA_OS_atoi(argv[2])) > 1 ||
		((MEA_Uint32)MEA_OS_atoi(argv[3])) > 1 ||
		MEA_OS_atoi(argv[4]) > 255 ||
		MEA_OS_atoi(argv[4]) < 12 ||
		MEA_OS_atoi(argv[5]) > 15)
	{
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_sssmii_tx_config0.val.speed = MEA_OS_atoi(argv[1]);
	entry.if_sssmii_tx_config0.val.duplex = MEA_OS_atoi(argv[2]);
	entry.if_sssmii_tx_config0.val.jabber = MEA_OS_atoi(argv[3]);
	entry.if_sssmii_tx_config0.val.ipg = MEA_OS_atoi(argv[4]);

	entry.if_sssmii_tx_config0.val.preamble = MEA_OS_atoi(argv[5]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_sssmii_tx_config_reg1(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;


	if (argc != 5) {
		return MEA_ERROR;
	}

	if (((MEA_Uint32)MEA_OS_atoi(argv[2])) > 63 ||
		((MEA_Uint32)MEA_OS_atoi(argv[3])) > 63 ||
		((MEA_Uint32)MEA_OS_atoi(argv[4])) > 15)
	{
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_sssmii_tx_config1.val.tx_link_up = MEA_OS_atohex(argv[1]);
	entry.if_sssmii_tx_config1.val.tx_memory_segment_size = MEA_OS_atoi(argv[2]);
	entry.if_sssmii_tx_config1.val.tx_ready_threshold = MEA_OS_atoi(argv[3]);
	entry.if_sssmii_tx_config1.val.tx_transmit_threshold = MEA_OS_atoi(argv[4]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_Pm_rate_enable(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;


	if (argc < 2) {
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_sw_global_parmamter.en_calc_rate = (MEA_Uint32)MEA_OS_atoi(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_max_used_desc_buff_thresholds(int argc, char* argv[]) {



	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val[4];
	int i;


	if (argc < 5) {
		return MEA_ERROR;
	}

	for (i = 0; i<4; i++) {
		val[i] = MEA_OS_atoi(argv[i + 1]);

		if (val[i] > 16383) {
			CLI_print("Value %d (%d) out of range 0..16383\n", i, val[i]);
			return MEA_ERROR;
		}
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.max_used_descriptors_high = val[0];
	entry.max_used_descriptors_low = val[1];
	entry.max_used_buffers_high = val[2];
	entry.max_used_buffers_low = val[3];

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_wred_shift(int argc, char *argv[])
{


	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 3) {
		CLI_print("Value (%d) out of range 0..3\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.wred_shift = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_pri_q_mode(int argc, char *argv[])
{


	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.pri_q_mode = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_mc_mqs(int argc, char *argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;

	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.mc_mqs_enable = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_wred(int argc, char* argv[]) {
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.bm_config_wred = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status MEA_CLI_Set_Globals_mqs(int argc, char *argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;


	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.bm_config_mqs = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_bmqs(int argc, char *argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;

	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.bm_config_bmqs = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_Cluster_mode(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;

	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 1) {
		CLI_print("Value (%d) out of range 0..1\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.Cluster_mode = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_bcst_drp_lvl(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 val;

	if (argc < 2) {
		return MEA_ERROR;
	}

	val = MEA_OS_atoi(argv[1]);

	if (val > 8191) {
		CLI_print("Value (%d) out of range 0..8191\n", val);
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_config.val.bcst_drp_lvl = val;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_bm_watchdog(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	MEA_Bool   enable;
	MEA_Uint32 threshold;

	if (argc < 3) {
		return MEA_ERROR;
	}

	enable = MEA_OS_atoiNum(argv[1]);
	threshold = MEA_OS_atoiNum(argv[2]);


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_watchdog_parm.val.enable = enable;
	entry.bm_watchdog_parm.val.wd_threshold = threshold;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_BM_Set_Delay(int argc, char* argv[]) {


	MEA_Globals_Entry_dbt entry;



	if (argc < 3) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.bm_set_delay.val.delay_data_DDR = MEA_OS_atoiNum(argv[1]);
	entry.bm_set_delay.val.delay_descriptor = MEA_OS_atoiNum(argv[2]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}


static MEA_Status MEA_CLI_Set_Globals_BM_reorder_timeout(int argc, char* argv[]) {


	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 pck_reorder_timeout, age_reorder_timeout;


	if (argc < 3) {
		return MEA_ERROR;
	}

	pck_reorder_timeout = MEA_OS_atoi(argv[1]);
	age_reorder_timeout = MEA_OS_atoi(argv[2]);


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.set_reorder_timeout.val.pck_reorder_timeout = pck_reorder_timeout;
	entry.set_reorder_timeout.val.age_reorder_timeout = age_reorder_timeout;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}


static MEA_Status  MEA_CLI_Set_Globals_acm_configure(int argc, char* argv[]) {
	MEA_Globals_Entry_dbt entry;



	if (argc < 3) {
		return MEA_ERROR;
	}






	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.acm_mode_configure.val.acm_pol = MEA_OS_atoi(argv[1]);
	entry.acm_mode_configure.val.acm_wred = MEA_OS_atoi(argv[2]);


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;

}

static MEA_Status MEA_CLI_Set_Globals_Device_edit_Ip(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc < 2) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.Device_edit_src_ip.my_Ip = MEA_OS_inet_addr(argv[1]);
	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done\n");
	return MEA_OK;
}



static MEA_Status MEA_CLI_Set_Globals_eth0_Ip(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc != 3) {
		return MEA_ERROR;
	}

#if !defined (MEA_PLAT_EVALUATION_BOARD)

	CLI_print("Error This is only for Ethernity board\n");
#endif
	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.Globals_eth0_Device.my_Ip = MEA_OS_inet_addr(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");


	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_forwarderMCIpSubNet(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Uint32            dstIp;
	MEA_Uint32            sourceIp;

	if (argc != 3) {
		return MEA_ERROR;
	}

	dstIp = MEA_OS_inet_addr(argv[1]);
	sourceIp = MEA_OS_inet_addr(argv[2]);


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}


	entry.Globals_Device.MC_ServerIp.DestIp = dstIp;
	entry.Globals_Device.MC_ServerIp.sourceIp = sourceIp;


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_Globals_Device_Counters_Enable(int argc, char* argv[]) {

	MEA_Globals_Entry_dbt entry;
	MEA_Uint32 Type;

	if (argc < 2) {
		CLI_print("num pf parameter isn't correct \n");
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}
	Type = MEA_OS_atoi(argv[1]);
	switch (Type) {
	case MEA_COUNTERS_PMS_TYPE:
		entry.Counters_Enable.val.Counters_PMs = MEA_OS_atoi(argv[2]);
		break;

	case MEA_COUNTERS_QUEUE_TYPE:
		entry.Counters_Enable.val.Counters_Queues = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_BMS_TYPE:
		entry.Counters_Enable.val.Counters_BMs = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_INGRESSPORTS_TYPE:
		entry.Counters_Enable.val.Counters_IngressPorts = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_RMONS_TYPE:
		entry.Counters_Enable.val.Counters_RMONs = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_ANALYZERS_TYPE:
		entry.Counters_Enable.val.Counters_ANALYZERs = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_ANALYZERS_LM_TYPE:
		entry.Counters_Enable.val.Counters_LMs = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_ANALYZERS_CCM_TYPE:
		entry.Counters_Enable.val.Counters_CCMs = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_PMS_TDM_TYPE:
		entry.Counters_Enable.val.Counters_PMs_TDM = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_RMONS_TDM_TYPE:
		entry.Counters_Enable.val.Counters_RMONs_TDM = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_CES_EVENT_TDM_TYPE:
		entry.Counters_Enable.val.eventCes = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_UTOPIA_RMONS_TYPE:
		entry.Counters_Enable.val.Counters_UTOPIA_RMONs = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_TDM_EGRESS_FIFO_TYPE:
		entry.Counters_Enable.val.Counters_EgressFifo_TDM = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_BONDING_TYPE:
		entry.Counters_Enable.val.Counters_Bonding = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_HC_RMON_TYPE:
		entry.Counters_Enable.val.Counters_HC_RMON = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_ACL_TYPE:
		entry.Counters_Enable.val.Counters_ACL = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_QUEUE_HANDLER_TYPE:
		entry.Counters_Enable.val.Counters_QUEUE_HANDLER = MEA_OS_atoi(argv[2]);
		break;
	case MEA_COUNTERS_Virtual_Rmon_TYPE:
		entry.Counters_Enable.val.Counters_Virtual_Rmon = MEA_OS_atoi(argv[2]);
		break;





	case MEA_COUNTERS_LAST_TYPE:
	default:
		CLI_print("Last Type not support this type\n");
		break;
	}



	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}
	CLI_print("Done\n");
	return MEA_OK;
}


static MEA_Status MEA_CLI_Set_Globals_EtytypeOffset(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc != 3) {
		return MEA_ERROR;
	}


	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.EthType_cam.val.offset12_EthType = (MEA_Uint32)MEA_OS_atoiNum(argv[1]);
	entry.EthType_cam.val.offset16_EthType = (MEA_Uint32)MEA_OS_atoiNum(argv[2]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");


	return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_Globals_Rmon_calc_rate_enable(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;


	if (argc < 2) {
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.if_sw_global_parmamter.en_Rmon_calc_rate = (MEA_Uint32)MEA_OS_atoi(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}



static MEA_Status MEA_CLI_Global_Set_ts_measurement(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	int numOfArgInCommand;
	char * ch;
	int i, len;

	MEA_Uint32  srcPort = 0;
	MEA_Uint32  L2_type = 0;

	MEA_Uint32  Net_Tag1 = 0;
	MEA_Uint32  L4_dst_port = 0;
	MEA_Bool    Ip_type = 0;        /*0 -ipv4 1-ipv6*/
	MEA_Bool    protocol_type = 0; /*0 -TCP 1-UDP*/



	if (argc < 2) {
		CLI_print("Error minimum parameter \n");
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		return MEA_ERROR;
	}


	numOfArgInCommand = 0;
	for (i = 1; i < argc; i++) {
		if (!MEA_OS_strcmp(argv[i], "ingress")) {
			numOfArgInCommand = 6;
			ch = argv[i];
			len = (i + numOfArgInCommand);
			MEA_CLI_CHECK_NUM_PARM(i, len, argc, ch);
			if (MEA_OS_atoiNum(argv[i + 1]) == 1) {
				numOfArgInCommand = 5;
				len = (i + numOfArgInCommand);
				srcPort = MEA_OS_atoiNum(argv[i + 2]);
				L2_type = MEA_OS_atoiNum(argv[i + 3]);
				Net_Tag1 = MEA_OS_atoiNum(argv[i + 4]);
				if (!MEA_OS_strcmp(argv[i + 5], "ipv4")) {
					Ip_type = MEA_FALSE;
				}
				else {
					if (!MEA_OS_strcmp(argv[i + 5], "ipv6")) {
						Ip_type = MEA_TRUE;
					}
				}

				if (!MEA_OS_strcmp(argv[i + 6], "TCP")) {
					protocol_type = MEA_FALSE;
				}
				else {
					if (!MEA_OS_strcmp(argv[i + 6], "UDP")) {
						protocol_type = MEA_TRUE;
					}
				}





				L4_dst_port = MEA_OS_atoiNum(argv[i + 7]);
			}

			entry.ts_measurement_ingress.srcPort = srcPort;
			entry.ts_measurement_ingress.L2_type = L2_type;
			entry.ts_measurement_ingress.Net_Tag1 = Net_Tag1;
			entry.ts_measurement_ingress.Ip_type = Ip_type;
			entry.ts_measurement_ingress.protocol_type = protocol_type;


			entry.ts_measurement_ingress.L4_dst_port = L4_dst_port;



		}
		if (!MEA_OS_strcmp(argv[i], "egress")) {
			numOfArgInCommand = 2;
			ch = argv[i];
			len = (i + numOfArgInCommand);
			MEA_CLI_CHECK_NUM_PARM(i, len, argc, ch);
			if (MEA_OS_atoiNum(argv[i + 1]) == 1) {
				numOfArgInCommand = 5;

				len = (i + numOfArgInCommand);
				srcPort = MEA_OS_atoiNum(argv[i + 2]);
				L2_type = MEA_OS_atoiNum(argv[i + 3]);
				Net_Tag1 = MEA_OS_atoiNum(argv[i + 4]);
				if (!MEA_OS_strcmp(argv[i + 5], "ipv4")) {
					Ip_type = MEA_FALSE;
				}
				else {
					if (!MEA_OS_strcmp(argv[i + 5], "ipv6")) {
						Ip_type = MEA_TRUE;
					}
				}

				if (!MEA_OS_strcmp(argv[i + 6], "TCP")) {
					protocol_type = MEA_FALSE;
				}
				else {
					if (!MEA_OS_strcmp(argv[i + 6], "UDP")) {
						protocol_type = MEA_TRUE;
					}
				}


				L4_dst_port = MEA_OS_atoiNum(argv[i + 7]);
			}

			entry.ts_measurement_egress.srcPort = srcPort;
			entry.ts_measurement_egress.L2_type = L2_type;


			entry.ts_measurement_egress.Net_Tag1 = Net_Tag1;
			entry.ts_measurement_egress.Ip_type = Ip_type;
			entry.ts_measurement_egress.protocol_type = protocol_type;
			entry.ts_measurement_egress.L4_dst_port = L4_dst_port;
		}


		i += numOfArgInCommand;

	}

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("ERROR: %s to config globals", __FUNCTION__);
		return MEA_ERROR;
	}




	return MEA_OK;

}


static MEA_Status MEA_CLI_Global_Set_Hc_offset_mode(int argc, char *argv[])
{
	MEA_Globals_Entry_dbt entry;
	//int i;

	if (argc < 2) {
		CLI_print("Error minimum parameter \n");
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		return MEA_ERROR;
	}


	entry.HC_Offset_Mode = MEA_OS_atoi(argv[1]);


	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("ERROR: %s to config globals", __FUNCTION__);
		return MEA_ERROR;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}


static MEA_Status MEA_CLI_Global_Set_Hc_Compress_Baypass(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	int numOfArgInCommand;
	char * ch;
	int i, len;

	if (argc < 4) {
		CLI_print("Error minimum parameter \n");
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		return MEA_ERROR;
	}

	numOfArgInCommand = 0;
	for (i = 1; i<argc; i++) {
		if (!MEA_OS_strcmp(argv[i], "compress")) {
			numOfArgInCommand = 2;
			ch = argv[i];
			len = (i + numOfArgInCommand);
			MEA_CLI_CHECK_NUM_PARM(i, len, argc, ch);
			entry.if_HDC_global.val.Comp_bypass = MEA_OS_atoi(argv[i + 1]);
			entry.if_HDC_global.val.Comp_bypass_aligner = MEA_OS_atoi(argv[i + 2]);


		}
		if (!MEA_OS_strcmp(argv[i], "decompress")) {
			numOfArgInCommand = 2;
			ch = argv[i];
			len = (i + numOfArgInCommand);
			MEA_CLI_CHECK_NUM_PARM(i, len, argc, ch);
			entry.if_HDC_global.val.Decomp_bypass = MEA_OS_atoi(argv[i + 1]);
			entry.if_HDC_global.val.Decomp_bypass_aligner = MEA_OS_atoi(argv[i + 2]);


		}


		i += numOfArgInCommand;

	}

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("ERROR: %s to config globals", __FUNCTION__);
		return MEA_ERROR;
	}

	CLI_print("Done.\n");
	return MEA_OK;
}

static MEA_Status MEA_CLI_Global_Set_1p1_Count(int argc, char *argv[])
{

	MEA_Globals_Entry_dbt entry;
	//int i;

	if (argc < 3) {
		CLI_print("Error minimum parameter \n");
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {

		return MEA_ERROR;
	}


	entry.if_1p1_down_up_count.to_down = MEA_OS_atoi(argv[1]);
	entry.if_1p1_down_up_count.to_Up = MEA_OS_atoi(argv[2]);






	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("ERROR: %s to config globals", __FUNCTION__);
		return MEA_ERROR;
	}

	CLI_print("Done.\n");
	return MEA_OK;
}


static MEA_Status  MEA_CLI_Set_Globals_Device_Stp_mode(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	MEA_Global_STP_mode_te mode_val;
	MEA_PortState_te state;

	if (argc < 3) {
		return MEA_ERROR;
	}

	mode_val = MEA_OS_atoi(argv[1]);
	state = MEA_OS_atoi(argv[2]);

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.stp_mode.val.mode = mode_val;
	entry.stp_mode.val.default_state = state;

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");
	return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_Globals_Device_utopia_threshold(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc < 2) {
		return MEA_ERROR;
	}

	if (argc < 3) {
		CLI_print("missing parameter  \n");
		return MEA_OK;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.utopia_threshold_reg.val.rx_parity_check = MEA_OS_atoi(argv[1]);
	entry.utopia_threshold_reg.val.tx_parity_check = MEA_OS_atoi(argv[2]);


	CLI_print("Done.\n");
	return MEA_OK;

}

static MEA_Status  MEA_CLI_Set_Globals_Device_Serdes_Power_Saving(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;
	//MEA_Uint32   groupId=0;
	MEA_Uint32   power = 0;
	MEA_Uint32   val = 0;

	MEA_Port_t port, start_port, end_port;
	char buf[500];
	char *up_ptr;


	if (argc < 2)
		return MEA_ERROR;

	if (MEA_OS_strcmp(argv[1], "all") == 0) {
		start_port = 0;
		end_port = 5;
	}
	else {
		MEA_OS_strcpy(buf, argv[1]);
		if ((up_ptr = strchr(buf, ':')) != 0)
		{
			(*up_ptr) = 0;
			up_ptr++;
			start_port = MEA_OS_atoi(buf);
			end_port = MEA_OS_atoi(up_ptr);
		}
		else
		{
			port = start_port = end_port = MEA_OS_atoi(buf);
		}

	}






	if (argc < 3) {
		CLI_print("missing parameter  \n");
		return MEA_OK;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	//groupId = MEA_OS_atoi(argv[1]);
	power = MEA_OS_atoi(argv[2]);

	for (port = start_port; port <= end_port; port++) {
		if (port > 5) {
			CLI_print("Error groupId(%d) out of range\n", port);
			return MEA_OK;
		}
		val = (1 << port);
		entry.power_saving.reg[0] &= (~val);
		entry.power_saving.reg[0] |= ((power) << port);
	}


#if  defined(MEA_ETHERNITY) &&  defined(MEA_OS_ETH_PROJ_1)   
	{

		MEA_Bool    sfp_port[24];
		int i;


		char        str_to_out[200];
		char        *p_str_to_out;

		MEA_Uint32  val_reg;

		for (i = 0; i<24; i++) {
			sfp_port[i] = 0;
		}
		p_str_to_out = str_to_out;

#if 1
		MEA_device_environment_info.PRODUCT_NAME = "ETH24_BP_GIGA";// NULL; // debug "ETH24_BP_GIGA"
#endif
																   // this is for ETH24
		if ((strcmp(MEA_device_environment_info.PRODUCT_NAME, "ETH24_BP_GIGA") == 0)) {

			if ((entry.power_saving.reg[0] & 0x1) == 1)
			{

				sfp_port[0] = 1; // 	1	12	0
				sfp_port[1] = 1; //2	24	0

				sfp_port[12] = 1;  //	13	0	0
				sfp_port[13] = 1;  //	14	108	0
				sfp_port[14] = 1;  //	15	36	0
			}
			else {
				sfp_port[0] = 0; // 	1	12	0
				sfp_port[1] = 0; //2	24	0

				sfp_port[12] = 0;  //	13	0	0
				sfp_port[13] = 0;  //	14	108	0
				sfp_port[14] = 0;  //	15	36	0


			}

			if (((entry.power_saving.reg[0] >> 1) & 0x1) == 1)
			{
				sfp_port[2] = 1;  //3	48	1
				sfp_port[3] = 1;  //4	109	1
				sfp_port[4] = 1;  //5	126	1
				sfp_port[15] = 1; //16	72	1
				sfp_port[16] = 1; //17	125	1
			}
			else {

				sfp_port[2] = 0;  //3	48	1
				sfp_port[3] = 0;  //4	109	1
				sfp_port[4] = 0;  //5	126	1
				sfp_port[15] = 0; //16	72	1
				sfp_port[16] = 0; //17	125	1;

			}

			if (((entry.power_saving.reg[0] >> 2) & 0x1) == 1)
			{
				sfp_port[5] = 1;  //16	101	2
				sfp_port[6] = 1;  //	102	2
				sfp_port[17] = 1; //18	100	2
				sfp_port[18] = 1; //19	92	2
				sfp_port[19] = 1; //20	103	2

			}
			else {

				sfp_port[5] = 0;  //16	101	2
				sfp_port[6] = 0;  //	102	2
				sfp_port[17] = 0; //18	100	2
				sfp_port[18] = 0; //19	92	2
				sfp_port[19] = 0; //20	103	2


			}
			//goup 2 and 3 is back plan

			if (((entry.power_saving.reg[0] >> 5) & 0x1) == 1)
			{
				sfp_port[7] = 1;  //	8	104	5
				sfp_port[8] = 1;  //	9	105	5
				sfp_port[9] = 1;  //10	93	5
				sfp_port[10] = 1; //11	106	5
				sfp_port[11] = 1; //12	107	5
			}
			else {
				sfp_port[7] = 0;  //	8	104	5
				sfp_port[8] = 0;  //	9	105	5
				sfp_port[9] = 0;  //10	93	5
				sfp_port[10] = 0; //11	106	5
				sfp_port[11] = 0; //12	107	5


			}
			val_reg = 0;
			for (i = 0; i<20; i++) {
				if (sfp_port[i] == 1)
					val_reg |= ((sfp_port[i] & 0x1) << i);
			}
			entry.mea_eth24_sfp_reg = val_reg;


#if 1
			for (i = 0; i<3; i++) {
				MEA_OS_sprintf(p_str_to_out, "echo '%02x %02x' > /sys/devices/platform/fpga/mem", (MEA_Uint8)(i + 0x64), (MEA_Uint8)((entry.mea_eth24_sfp_reg >> (i * 8)) & 0xff));
				CLI_print("echo '%02x %02x' > /sys/devices/platform/fpga/mem\n", (MEA_Uint8)(i + 0x64), (MEA_Uint8)((entry.mea_eth24_sfp_reg >> (i * 8)) & 0xff));
#if defined(MEA_OS_LINUX)&& !defined(__KERNEL__)
				system(p_str_to_out);
#endif
				//                MEA_OS_sleep(1,0);
			}
#endif









			//            system("echo '0x64 0x00' > /sys/devices/platform/fpga/mem");
			//            system("echo '0x65 0x00' > /sys/devices/platform/fpga/mem");
			//            system("echo '0x66 0x00' > /sys/devices/platform/fpga/mem");


			//                 system("echo '0x64 0x00' > /sys/devices/platform/fpga/mem");
			//                 system("echo '0x65 0x00' > /sys/devices/platform/fpga/mem");
			//                 system("echo '0x66 0x00' > /sys/devices/platform/fpga/mem");

		}///eth24
		else {
			CLI_print("no ETH24\n");
		}
	}
#endif

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");
	return MEA_OK;

}

static MEA_Status  MEA_CLI_Set_Globals_Device_pos_polling(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc < 2) {
		return MEA_ERROR;
	}

	if (argc < 7) {
		CLI_print("missing parameter  \n");
		return MEA_OK;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.pos_config.polling.val.Tx4 = MEA_OS_atoi(argv[1]);
	entry.pos_config.polling.val.Tx3 = MEA_OS_atoi(argv[2]);
	entry.pos_config.polling.val.Tx2 = MEA_OS_atoi(argv[3]);
	entry.pos_config.polling.val.Tx1 = MEA_OS_atoi(argv[4]);
	entry.pos_config.polling.val.Rx2 = MEA_OS_atoi(argv[5]);
	entry.pos_config.polling.val.Rx1 = MEA_OS_atoi(argv[6]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");
	return MEA_OK;

}

static MEA_Status MEA_CLI_Set_Globals_Device_WallClock_1sec(int argc, char *argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc < 2) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.WallClock_1Sec.val.value = MEA_OS_atoi(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");
	return MEA_OK;

}

static MEA_Status  MEA_CLI_Set_Globals_iTHi(int argc, char* argv[])
{

	MEA_Globals_Entry_dbt entry;


	if (argc < 2) {
		return MEA_ERROR;
	}



	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.iTHi = (MEA_Uint32)MEA_OS_atoiNum(argv[1]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");

	return MEA_OK;
}

static MEA_Status MEA_CLI_Set_Globals_Device_WallClock_set(int argc, char* argv[])
{
	MEA_Clock_t entry;

	if (argc < 3) {
		return MEA_ERROR;
	}

	entry.sec = MEA_OS_atoi(argv[1]);
	entry.nsec = MEA_OS_atoi(argv[2]);

	MEA_API_Globals_WallCLock_Set(MEA_UNIT_0, entry);
	CLI_print("Done.\n");
	return MEA_OK;
}

static MEA_Status  MEA_CLI_Set_Globals_Device_WallClock_calibration(int argc, char* argv[])
{
	MEA_Globals_Entry_dbt entry;

	if (argc < 4) {
		return MEA_ERROR;
	}

	if (MEA_API_Get_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Get_Globals_Entry failed\n");
		return MEA_OK;
	}

	entry.WallClock_Calibrate.val.clock_Add_sub = MEA_OS_atoi(argv[1]);
	if (MEA_OS_atoi(argv[2]) > 0x7ffffff) {
		CLI_print
		("WallClock_Calibrate.val.clock_val 0x%08x > max 0x%08x\n",
			MEA_OS_atoi(argv[2]), 0x7ffffff);
		return MEA_OK;
	}
	entry.WallClock_Calibrate.val.clock_val = MEA_OS_atoi(argv[2]);
	if (MEA_OS_atoi(argv[3]) > 0x0f) {
		CLI_print
		("WallClock_Calibrate.val.clock_grn 0x%08x > max 0x%08x\n",
			MEA_OS_atoi(argv[2]), 0x0f);
		return MEA_OK;
	}
	entry.WallClock_Calibrate.val.clock_grn = MEA_OS_atoi(argv[3]);

	if (MEA_API_Set_Globals_Entry(MEA_UNIT_0, &entry) != MEA_OK) {
		CLI_print("MEA_API_Set_Globals_Entry failed\n");
		return MEA_OK;
	}

	CLI_print("Done.\n");
	return MEA_OK;

}




/*****************************************************************************************************/



void MEA_CLI_globals_defineCmd(void)
{

	CLI_defineCmd("MEA globals set device iswi",
		(CmdFunc)MEA_CLI_Set_Globals_iTHi,
		"Set global device iswi\n",
		"Usage  : iTHi <val>\n"
		"         <val>       - val\n"

		"Example: iswi 7\n");

	CLI_defineCmd("MEA globals set device wallclock_Set",
		(CmdFunc)MEA_CLI_Set_Globals_Device_WallClock_set,
		" set wallclock_set \n",
		"  wallclock_set <clock_H><clock_L>\n"
		"     ... <clock_H>   clock hi \n"
		"     ... <clock_l>   clock lo \n"
		"Example: \n"
		"       wallclock_set 100 0 \n"
		"       wallclock_set 0 0 \n"
	);


	CLI_defineCmd("MEA globals set device wallclock_calibration",
		(CmdFunc)MEA_CLI_Set_Globals_Device_WallClock_calibration,
		" set wallclock_calibration \n",
		"  WallClock_calibration <clock_Add_sub><clock_val><clock_grn>\n"
		"     ... <type> - 0= Subtract 1= Add \n"
		"     ... <clock_val>   clock hi \n"
		"     ... <clock_grn>   0--15 \n"
		"Example: \n"
		"       wallclock_calibration 0 0 1\n"
		"       wallclock_calibration 1 0 10\n"
	);

	CLI_defineCmd("MEA globals set device wallclock_1sec",
		(CmdFunc)MEA_CLI_Set_Globals_Device_WallClock_1sec,
		" set wallclock_1sec \n",
		"  wallclock_1sec <val>\n"
		"     ... <val>   val \n"
		"Example: \n"
		"       wallclock_1sec  9999999999 \n"

	);

	CLI_defineCmd("MEA globals set device pos_polling",
		(CmdFunc)MEA_CLI_Set_Globals_Device_pos_polling,
		" set pos_polling \n",
		"  pos_polling <tx4><tx3><tx2><tx1><rx2><rx1>\n"
		"      <tx4>   address poll tx4 (4bit)\n"
		"      <tx3>   address poll tx3 (4bit)\n"
		"      <tx2>   address poll tx2 (4bit)\n"
		"      <tx1>   address poll tx1 (4bit)\n"
		"      <rx2>   address poll rx1 (8bit)\n"
		"      <rx1>   address poll rx1 (8bit)\n"
		"Example: \n"
		"       pos_polling  1 1 12 12 12 12 \n"
		"       pos_polling  12 12 12 12 24 24 \n"

	);

	CLI_defineCmd("MEA globals set device power_saving",
		(CmdFunc)MEA_CLI_Set_Globals_Device_Serdes_Power_Saving,
		" set power_saving \n",
		"  pos_polling <group Id> <power>\n"
		"      <group>   0..5 \n"
		"      <power>   1-power_saving  0-power not save \n"
		"Example: \n"
		"       power_saving  1  0 \n"
		"       power_saving  0  1\n"

	);

	CLI_defineCmd("MEA globals set device utopia_threshold",
		(CmdFunc)MEA_CLI_Set_Globals_Device_utopia_threshold,
		" set utopia_threshold \n",
		"  utopia_threshold <rx_parity_check><tx_parity_check>\n"
		"      <rx_parity_check>   0-disable 1-enable\n"
		"      <tx_parity_check>   0-disable 1-enable\n"

		"Example: \n"
		"       utopia_threshold  1 0  \n"
		"       utopia_threshold  1 1  \n"

	);


	CLI_defineCmd("MEA globals set device Stp_mode",
		(CmdFunc)MEA_CLI_Set_Globals_Device_Stp_mode,
		" set Stp_mode type and default_state\n",
		"  Stp_mode <stp_type><Queues><default_state>\n"
		"     ... <type> - 0= DISABLE 1= STP 2 MSTP\n"
		"         <default_state>  0=FORWARD 1=DISCARD 2=LEARN \n"
		"Example: \n"
		"       Stp_mode 0 0         \n"
		"       Stp_mode 1 1         \n"
	);

	CLI_defineCmd("MEA globals set if 1p1_count_tick",
		(CmdFunc)MEA_CLI_Global_Set_1p1_Count,
		" set 1p1_count_tick <down><up>\n",
		"  1p1_count_tick <down><up>\n"
		"     ... <down_count> - 0:63 \n"
		"         <up_count>  0:2000 \n"
		"Example: \n"
		"       1p1_count_tick 30 100         \n"
		"       1p1_count_tick 63 100         \n"
	);

	CLI_defineCmd("MEA globals set if hc_bypass ",
		(CmdFunc)MEA_CLI_Global_Set_Hc_Compress_Baypass,
		" set if hc_bypass  \n",
		"  hc_bypass <compress/decompress> <bypass><bypass_aligner>\n"
		"     ...  compress/decompress  \n"
		"         <bypass>              \n"
		"         <bypass_aligner>      \n"
		"Example: \n"
		"       compress 1 1           \n"
		"       decompress 1 1         \n"
	);

	CLI_defineCmd("MEA globals set if hc_offset_mode ",
		(CmdFunc)MEA_CLI_Global_Set_Hc_offset_mode,
		" set if hc_offset_mode  \n",
		"  hc_offset_mode <type>\n"
		"         <type> 0-no offset   1-enable ofset   \n"
		"Example: \n"
		"       hc_offset_mode 1            \n"
		"       hc_offset_mode 0          \n"
	);


	CLI_defineCmd("MEA globals set ts_measurement ",
		(CmdFunc)MEA_CLI_Global_Set_ts_measurement,
		" set ts_measurement  <ingress/egress> <enable>  <srcport><l2Type><net_tag><ipv4/ipv6><TCP/UDP><L4_dst>\n",
		"  ts_measurement     <ingress/egress> <enable>  <srcport><l2Type><net_tag><ipv4/ipv6><TCP/UDP><L4_dst>\n"
		"     ... <enable> - 0-disable 1-enable \n"
		"        <srcport> \n"
		"        <l2Type>  type of packet\n"
		"        <net_tag>  classifier \n"
		"        <ipv4/ipv6>\n"
		"        <TCP/UDP>\n"
		"        <L4_dst>  l4 destination port\n"

		"Example: \n"
		"       ts_measurement  ingress 1 105 1 0x001 ipv4  TCP 5004      \n"
		"       ts_measurement  egress  1 105 1 0x002 ipv4  TCP 5004      \n"
		"\n"
		"       ts_measurement  ingress 1 105 1 0x001 ipv6  UDP 5004      \n"
		"       ts_measurement  egress  1 105 1 0x002 ipv6  UDP 5004      \n"
		"\n"
		"       ts_measurement  ingress 1 105 1 0x001 ipv6  TCP 5004      \n"
		"       ts_measurement  egress  1 105 1 0x002 ipv6  TCP 5004      \n"

	);


	CLI_defineCmd("MEA globals set if calc_rmon_rate",
		(CmdFunc)MEA_CLI_Set_Globals_Rmon_calc_rate_enable,
		" set calc_rmon_rate <enable>\n",
		"  calc_rmon_rate <enable>\n"
		"     ... <enable> - 0-disable 1-enable \n"

		"Example: \n"
		"       calc_rmon_rate 0          \n"
		"       calc_rmon_rate 1        \n"
	);


	CLI_defineCmd("MEA globals set if cam ethertype ",
		(CmdFunc)MEA_CLI_Set_Globals_EtytypeOffset,
		"  ethertype <offset 12 value> <offset 16 value>\n",
		"  ethertype <offset 12 value> <offset 16 value>\n"
		"     ... <offset 12 value>  ethertype value \n"
		"     ... <offset 16 value>  ethertype value \n"

		"Example: \n"
		"       ethertype 0x9200 0x9200  \n"
		"       ethertype 0x9100 0x9100  \n"
	);




	CLI_defineCmd("MEA globals set device Counters_Enable",
		(CmdFunc)MEA_CLI_Set_Globals_Device_Counters_Enable,
		" set Counters_Enable for each entity\n",
		"  Counters_Enable \n"
		"          <type>  0 = PMs    \n"
		"                  1 = Queues \n"
		"                  2 = BMs \n"
		"                  3 = IngressPort \n"
		"                  4 = RMONs \n"
		"                  5 = ANALYZERs \n"
		"                  6 = LM \n"
		"                  7 = CCM \n"
		"                  8 = PMs TDM \n"
		"                  9 = RMONs TDM \n"
		"                  10= Ces Events \n"
		"                  11= utopia rmon \n"
		"                  12= egress tdm fifo\n"
		"                  13= Bonding \n"
		"                  14= forwarder\n"
		"                  15= Ingress\n"
		"                  16= HC\n"
		"                  17= ACL\n"
		"                  18= QueueHendler\n"
		"     ... <value> - 1 (enable) / 0 (disable)\n"
		"Example: Counters_Enable 0 1  \n"
		"         Counters_Enable 4 1  \n");




	CLI_defineCmd("MEA globals set device IP_MC_Subnet",
		(CmdFunc)MEA_CLI_Set_Globals_forwarderMCIpSubNet,
		" IP_MC_Subnet <subnet destIp>  <MC server>\n",
		"    <subnet destIp> (IPv4 address in dot notation) \n"
		"    <subnet source Ip> (IPv4 address in dot notation) \n"

		"Example: MC_Server 224.255.0.0 192.255.0.0\n");




	CLI_defineCmd("MEA globals set device eth0_Ip",
		(CmdFunc)MEA_CLI_Set_Globals_eth0_Ip,
		" eth0_Ip <Ip>  \n",
		"    <Ip> (IPv4 address in dot notation) \n"
		"Example: eth0_Ip 10.0.0.1\n");

	CLI_defineCmd("MEA globals set device myEdit_Ip",
		(CmdFunc)MEA_CLI_Set_Globals_Device_edit_Ip,
		" myEdit_Ip <Ip>  \n",
		"    <Ip> (IPv4 address in dot notation) \n"
		"Example: myEdit_Ip 192.168.1.10\n");





	CLI_defineCmd("MEA globals set bm acm_configure",
		(CmdFunc)MEA_CLI_Set_Globals_acm_configure,
		"Set global acm configure acm mode\n",
		"Usage  : acm_configure <acm_pol><acm_wred> \n"
		"        <acm_pol> - 0..7 \n"
		"        <acm_wred> - 0..3 \n"
		"Example: acm_configure 1 0\n");

	CLI_defineCmd("MEA globals set bm reorder_timeout",
		(CmdFunc)MEA_CLI_Set_Globals_BM_reorder_timeout,
		" set reorder_timeout \n",
		"  reorder_timeout <packet_timeout> <age_timeout>\n"
		"     ... <packet_timeout>    (micro Sec)\n"
		"     ... <age_timeout>       (micro Sec)\n"
		"Example: \n"
		"       reorder_timeout  200 1000\n"
	);
	CLI_defineCmd("MEA globals set bm set_delay",
		(CmdFunc)MEA_CLI_Set_Globals_BM_Set_Delay,
		" set set_delay \n",
		"  set_delay <delay_data_DDR> <delay_descriptor>\n"
		"     ... <delay_data_DDR>    ()\n"
		"     ... <delay_descriptor>  ()\n"
		"Example: \n"
		"       set_delay  200 1000\n"
	);




	CLI_defineCmd("MEA globals set bm watchdog",
		(CmdFunc)MEA_CLI_Set_Globals_bm_watchdog,
		"Set global watchdog  \n",
		"Usage  : watchdog <enable><value> \n"
		"        <valid>  1 (enable) / 0 (disable)\n"
		"        <value> - 0..16k decimal\n"
		"Example: watchdog  1 100 \n");


	CLI_defineCmd("MEA globals set bm bcst_drp_lvl",
		(CmdFunc)MEA_CLI_Set_Globals_bcst_drp_lvl,
		"Set global broadcast drop level \n",
		"Usage  : bcst_drp_lvl <value> \n"
		"        <value> - 0..8191 decimal\n"
		"Example: bcst_drp_lvl 500\n");


	CLI_defineCmd("MEA globals set bm Cluster_mode",
		(CmdFunc)MEA_CLI_Set_Globals_Cluster_mode,
		"Set global Cluster mode mode \n",
		"Usage  : cluster_mode <value> \n"
		"        <value> - 0 (RR) / 1 (WFQ)\n"
		"Example: Cluster_mode 0\n");

	CLI_defineCmd("MEA globals set bm bmqs",
		(CmdFunc)MEA_CLI_Set_Globals_bmqs,
		"Set global bmqs  \n",
		"Usage  : bmqs <value> \n"
		"        <value> - 1 (enable) / 0 (disable)\n"
		"Example: bmqs 0\n");
	CLI_defineCmd("MEA globals set bm mqs",
		(CmdFunc)MEA_CLI_Set_Globals_mqs,
		"Set global mqs  \n",
		"Usage  : mqs <value> \n"
		"        <value> - 1 (enable) / 0 (disable)\n"
		"Example: mqs 0\n");
	CLI_defineCmd("MEA globals set bm wred",
		(CmdFunc)MEA_CLI_Set_Globals_wred,
		"Set global wred  \n",
		"Usage  : wred <value> \n"
		"        <value> - 1 (enable) / 0 (disable)\n"
		"Example: wred 0\n");

	CLI_defineCmd("MEA globals set bm mc_mqs",
		(CmdFunc)MEA_CLI_Set_Globals_mc_mqs,
		"Set global mc_mqs  \n",
		"Usage  : mc_mqs <valid> \n"
		"        <valid> - 1 (enable) / 0 (disable)\n"
		"Example: mc_mqs 0\n");



	CLI_defineCmd("MEA globals set bm pri_q_mode",
		(CmdFunc)MEA_CLI_Set_Globals_pri_q_mode,
		"Set global Priority Queue mode  \n",
		"Usage  : pri_q_mode <value> \n"
		"        <value> - 0 (RR) / 1 (WFQ)\n"
		"Example: pri_q_mode 0\n");

	CLI_defineCmd("MEA globals set bm wred_shift",
		(CmdFunc)MEA_CLI_Set_Globals_wred_shift,
		"Set global wred shift \n",
		"Usage  : wred_shift <value> \n"
		"        <value> - 0 (K=6) , 1(K=7) , 2(K=8) , 3(K=9)\n"
		"Example: wred_shift 1\n");



	CLI_defineCmd("MEA globals set bm max_used_desc_buff_thresholds",
		(CmdFunc)MEA_CLI_Set_Globals_max_used_desc_buff_thresholds,
		"Set global max used descriptors/buffers thresholds (high and low)\n",
		"Usage  : max_used_desc_buff_thresholds <desc-high> <desc-low> <buffers-high> <buffers-low>\n"
		"        <desc-high> - 0..16383 \n"
		"        <desc-low > - 0..16383 \n"
		"        <buff-high> - 0..16383 \n"
		"        <buff-low>  - 0..16383 \n"
		"Example: max_used_desc_buff_thresholds 16375 14200 255 224\n");




	CLI_defineCmd("MEA globals set bm pm_calc_rate",
		(CmdFunc)MEA_CLI_Set_Globals_Pm_rate_enable,
		"Set global Bm pm_calc_rate \n",
		"Usage  : pm_calc_rate <enable> \n"
		"        <value> - 0 disable 1- enable\n"
		"Example: pm_calc_rate 1\n");

	CLI_defineCmd("MEA globals set if sssmii_tx_config_register_1",
		(CmdFunc)MEA_CLI_Set_Globals_sssmii_tx_config_reg1,
		"Set global IF SSSMII TX configuration register 1\n",
		"Usage  : sssmii_tx_config_register_1 <tx_link_up> <tm_memory_segment_size> <tx_ready_threshold> <tx_ _threshold>\n"
		"         <tx_link_up>             - 4 digits hex number - 16 ports 16-bit bit field.\n"
		"                                    On each bit - 0 | 1  ( link down | link up )\n"
		"         <tx_memory_segment_size> - 0..63 (represents granularity of 8 bytes)\n"
		"         <tx_ready_threshold>    - 0..63 (represents granularity of 8 bytes)\n"
		"         <tx_ _threshold> - 0..15 (represents granularity of 8 bytes)\n"
		"Example: sssmii_tx_config_register_1 ffff 63 26 7\n");

	CLI_defineCmd("MEA globals set if sssmii_tx_config_register_0",
		(CmdFunc)MEA_CLI_Set_Globals_sssmii_tx_config_reg0,
		"Set global IF SSSMII TX configuration register 0\n",
		"Usage  : sssmii_tx_config_register_0 <speed> <duplex> <jabber> <tx_ipg> <tx_preamble>\n"
		"         <speed>       - 0 | 1 (10Mbps | 100Mbps)\n"
		"         <duplex>      - 0 | 1 (half |full)\n"
		"         <jabber>      - 0 | 1 (OK | Error)\n"
		"         <tx_ipg>      - 12..255.\n"
		"         <tx_preamble> - 0..15\n"
		"Example: sssmii_tx_config_register_0 1 0 1 12 7\n");

	CLI_defineCmd("MEA globals set if sssmii_rx_config_register",
		(CmdFunc)MEA_CLI_Set_Globals_sssmii_rx_config_reg,
		"Set global IF SSSMII RX configuration register\n",
		"Usage  : sssmii_rx_config_register <rx_memory_segment_size> <rx_ready_threshold> <rx_select_control>\n"
		"         <rx_memory_segment_size> - 0..63 (represents granularity of 8 bytes)\n"
		"         <rx_ready_threshold>    - 0..63 (represents granularity of 8 bytes)\n"
		"         <rx_select_control>      - 0..15\n"
		"Example: sssmii_rx_config_register 63 40 0\n");

	CLI_defineCmd("MEA globals set if drop_unmatch_pkts",
		(CmdFunc)MEA_CLI_Set_Globals_drop_unmatch_pkts,
		"Set global IF drop unmatch packets\n",
		"Usage  : drop_unmatch_pkts  1 | 0 \n"
		"         1 - Drop unmatch packets in preParser/Parser/Classifier\n"
		"         0 - Not Drop unmatch packets \n"
		"Example: drop_unmatch_pkts 1\n");

	CLI_defineCmd("MEA globals set if lxcp",
		(CmdFunc)MEA_CLI_Set_Globals_Lxcp_Enable,
		"Set global IF lxcp\n",
		"Usage  : lxcp  1 | 0 \n"
		"         1 - lxcp Enable \n"
		"         0 - lxcp Disable\n"
		"Example: lxcp 1\n");

	CLI_defineCmd("MEA globals set if service_hash_type",
		(CmdFunc)MEA_CLI_Set_Globals_Hash_Type,
		"Set global IF service hash type\n",
		"Usage  : service_hash_type  <hash1><hash2> \n"
		"         <hash1> (0..3)\n"
		"         <hash2> (0..3)\n"
		"Example: service_hash_type 3 0\n");

	CLI_defineCmd("MEA globals set if acl_hash_type",
		(CmdFunc)MEA_CLI_Set_Globals_Set_ACL_hash,
		"Set global IF acl hash type\n",
		"Usage  : acl_hash_type  <hash> \n"
		"         <hash1> (0..15)\n"

		"Example: acl_hash_type 12 \n");




	CLI_defineCmd("MEA globals set if semi_back_pressure",
		(CmdFunc)MEA_CLI_Set_Globals_semi_back_pressure,
		"Set global IF semi_back_pressure\n",
		"Usage  : semi_back_pressure  0..63 \n"
		"Example: semi_back_pressure 12\n");

	CLI_defineCmd("MEA globals set if ingress_shaper",
		(CmdFunc)MEA_CLI_Set_Globals_if_ingress_shaper,
		"Set global IF ingress_shaper\n",
		"Usage  : ingress_shaper  7..63 \n"
		"Example: ingress_shaper 12\n");

	CLI_defineCmd("MEA globals set if pause_enable",
		(CmdFunc)MEA_CLI_Set_Globals_pause_enable,
		"Set global IF pause_enable\n",
		"Usage  : pause_enable 0|1 \n"
		"Example: pause_enable 1\n");






	CLI_defineCmd("MEA globals set bm chunk_high_limit",
		(CmdFunc)MEA_CLI_Set_Globals_bm_chunk_high_limit,
		"Set chunk allowed maximum size\n",
		"Usage  : chunk_high_limit  <size-bytes> \n"
		"         <size-bytes> - 0 - 65535\n"
		"Example: chunk_high_limit 12\n");

	CLI_defineCmd("MEA globals set bm chunk_short_limit",
		(CmdFunc)MEA_CLI_Set_Globals_bm_chunk_short_limit,
		"Set chunk allowed minimum size\n",
		"Usage  : chunk_short_limit  <size-bytes> \n"
		"         <size-bytes> - 0 - 65535\n"
		"Example: chunk_short_limit 4\n");

	CLI_defineCmd("MEA globals set bm pkt_high_limit",
		(CmdFunc)MEA_CLI_Set_Globals_bm_pkt_high_limit,
		"Set packet allowed maximum size\n",
		"Usage  : pkt_high_limit  <size-bytes> \n"
		"         <size-bytes> - 0 - 65535\n"
		"Example: pkt_high_limit 1522\n");

	CLI_defineCmd("MEA globals set bm pkt_short_limit",
		(CmdFunc)MEA_CLI_Set_Globals_bm_pkt_short_limit,
		"Set packet allowed minimum size\n",
		"Usage  : pkt_short_limit  <size-bytes> \n"
		"         <size-bytes> - 0 - 65535\n"
		"Example: pkt_short_limit 64\n");



	CLI_defineCmd("MEA globals set if network_L2CP_BaseAddr",
		(CmdFunc)MEA_CLI_Set_Globals_network_L2CP_BaseAddr,
		"Set global network L2CP mac base address \n",
		"Usage  : network_L2CP_BaseAddr <mac>\n"
		"         <mac> - Network L2CP mac base address \n"
		"Example: network_L2CP_BaseAddr 01:80:c2:00:00:00\n");

	CLI_defineCmd("MEA globals set if l2tp_tunnel_address",
		(CmdFunc)MEA_CLI_Set_Globals_network_L2TP_tunnelAddr,
		"Set global l2tp tunnel mac address \n",
		"Usage  : l2tp_tunnel_address <mac>\n"
		"         <mac> - l2tp_tunnel_address mac address \n"
		"Example: l2tp_tunnel_address 01:00:0c:cd:cd:d0\n");

	CLI_defineCmd("MEA globals set if cfm_oam enable",
		(CmdFunc)MEA_CLI_Set_Globals_cfm_oam_Valid,
		"Set global cfm oam enable/disable \n",
		"Usage  : enable <mode>\n"
		"         <mode> - (0-disable 1-enable)  \n"
		"Example: enable  1 \n");





	CLI_defineCmd("MEA globals set bm my_sa_mac",
		(CmdFunc)MEA_CLI_Set_Globals_my_SA_Mac,
		"Set global set bm my_sa_mac address\n",
		"Usage  : my_sa_mac <MAC>\n"
		"         <mac> - (mac format xx:xx:xx:xx:xx:xx) \n"
		"Example: my_sa_mac  00:01:02:03:04:05 \n");

	CLI_defineCmd("MEA globals set if cfm_oam ether_type",
		(CmdFunc)MEA_CLI_Set_Globals_cfm_oam_EtherType,
		"Set global cfm oam ether type \n",
		"Usage  : ether_type <EtherType>\n"
		"         <EtherType> - EtherType (4 hexdigits)  \n"
		"Example: ether_type  8902 \n");

	CLI_defineCmd("MEA globals set if cfm_oam unicast_mac",
		(CmdFunc)MEA_CLI_Set_Globals_cfm_oam_UC_Mac,
		"Set global cfm oam unicast mac address\n",
		"Usage  : unicast_mac <MAC>\n"
		"         <mac> - CFM OAM Unicast mac address (mac format xx:xx:xx:xx:xx:xx) \n"
		"Example: unicast_mac  00:01:02:03:04:05 \n");

	CLI_defineCmd("MEA globals set if cfm_oam multicast_mac",
		(CmdFunc)MEA_CLI_Set_Globals_cfm_oam_MC_Mac,
		"Set global cfm oam multicast mac \n",
		"Usage  : multicast mac  <MAC>\n"
		"         <mac> - cfm oam multicast mac address (mac format xx:xx:xx:xx:xx:xx)\n"
		"Example: multicast_mac  01:80:c2:00:00:00:00\n");



	CLI_defineCmd("MEA globals set if local_mac",
		(CmdFunc)MEA_CLI_Set_Globals_Local_Mac_Address,
		"Set global local_mac address\n",
		"Usage  : local_mac <index> <MAC>\n"
		"         <index> <mac> - local_mac address (mac format xx:xx:xx:xx:xx:xx) \n"
		"Example: local_mac 0 00:01:02:03:04:05 \n");

	CLI_defineCmd("MEA globals set if local_IP",
		(CmdFunc)MEA_CLI_Set_Globals_Local_IP_Address,
		"Set global local_IP address\n",
		"Usage  : local_IP <index> <IP>\n"
		"         <index> <ip> - local_mac address (IP format x.x.x.x) \n"
		"Example: local_IP 0 192.168.1.2 \n");

	CLI_defineCmd("MEA globals set if BFD_SW_IP_Addresss",
		(CmdFunc)MEA_CLI_Set_Globals_Local_BFD_SW_IP_Addresss,
		"Set global BFD_SW_IP address\n",
		"Usage  : local_IP  <IP>\n"
		"         <ipv4> -  IPv4 address (IPv4 format x.x.x.x) \n"
		"Example: BFD_SW_IP_Addresss  192.168.1.2 \n");


	CLI_defineCmd("MEA globals set if BFD_Discriminator",
		(CmdFunc)MEA_CLI_Set_Globals_MyBFD_Discriminator,
		"Set global BFD_Discriminator <number>\n",
		"Usage  : BFD_Discriminator <number>\n"
		"          <number>  value \n"
		"Example: MyBFD_Discriminator 0x12345678 \n");

	CLI_defineCmd("MEA globals set if link_up_mask",
		(CmdFunc)MEA_CLI_Set_Globals_link_up_mask,
		"Set global IF link_up_mask\n",
		"Usage  : link_up_mask <value>\n"
		"         <value> - 0 / 1\n"
		"Example: link_up_mask 0\n");

#if 0
	CLI_defineCmd("MEA globals set if if2bm_ready",
		(CmdFunc)MEA_CLI_Set_Globals_if2bm_ready,
		"Set global IF if2bm egress ready indication mode\n",
		"Usage  : if2bm_ready <value>\n"
		"         <value> - 0 / 1\n"
		"         0 - when cell is received in the Egress Fifo, \n"
		"             IF will indicate ready to BM only when the\n"
		"             cell sent. (Default)                      \n"
		"         1 - always ready \n"
		"Example: if2bm_ready 0\n");
#endif


	CLI_defineCmd("MEA globals set QueueHandler",
		(CmdFunc)MEA_CLI_Queue_Handler,
		"set QueueHandler  \n",
		" option\n"
		"   -reset <vlaue (sec)> \n"
		"   -mqsTH  <value> \n"
		"   -PacketTH <value> \n"
		"Example: QueueHandler  -reset 20 \n"
		"QueueHandler  -reset 10 -mqsTH 5 -PacketTH 30 \n");



	CLI_defineCmd("MEA globals GPIO",
		(CmdFunc)MEA_CLI_GPIO_RW_Z70,
		" GPIO read/write to gipi \n",
		"Example:\n"
		" MEA globals GPIO read        \n"
		" MEA globals GPIO write 0x100  \n"

	);



	CLI_defineCmd("MEA globals set if periodic",
		(CmdFunc)MEA_CLI_PeriodicEnable,
		"set if periodic  (0-disable / 1-enable)\n",
		" option\n"
		"   -linkIngress <valid> \n"
		"   -linkEgress  <valid> \n"
		"   -linkBonding <valid> \n"
		"Example: periodic  0 0 \n"
		"periodic  1 0 \n");



	CLI_defineCmd("MEA debug globals  alow_ingflow_pol_zero ",
		(CmdFunc)MEA_CLI_Set_Globals_alow_ingflow_pol_zero,
		"set if periodic  (0-disable / 1-enable)\n",
		" option\n"
		"   alow_ingflow_pol_zero <valid> \n"
		"   alow_ingflow_pol_zero  <valid> \n"
		"   alow_ingflow_pol_zero <valid> \n"
		"Example: alow_ingflow_pol_zero  0  \n"
		"alow_ingflow_pol_zero  1  \n");



	CLI_defineCmd("MEA globals set if ddr_nic",

		(CmdFunc)MEA_CLI_Set_Globals_en_ddr_nic,

		"Set global IF ddr_nic \n",

		"Usage  : ddr_nic <enable/disable>  \n"

		"         <en_ddr_nic> - 0 -disable 1 -enable \n"




		"Example: en_ddr_nic 1\n"
	);




	CLI_defineCmd("MEA globals set if ipg",

		(CmdFunc)MEA_CLI_Set_Globals_ipg,

		"Set global IF ipg values\n",

		"Usage  : ipg <ipg_gmii> <ipg_mii> \n"

		"         <upg_gmii> - IPG for gmii ports (12 - 256 :default 12)\n"

		"         <upg_mii > - IPG for  mii ports (12 - 256 :default 12)\n"

		"Note:  The value is start from 0 , therefor ipg 12 is 11 here\n"

		"Example: ipg 12 12\n"
	);




	CLI_defineCmd("MEA globals set if frag_art_eop",

		(CmdFunc)MEA_CLI_Set_Globals_frag_art_eop,

		"Set global IF frag_art_eop\n",

		"Usage  : frag_art_eop <value>\n"
		"\n"
		"Example: - frag_art_eop 194 \n"
	);



	CLI_defineCmd("MEA globals set if mac_fifo_threshold ",

		(CmdFunc)MEA_CLI_Set_Globals_mac_fifo_threshold,

		"Set global IF tx mac fifo thresholds\n",

		"Usage  : mac_fifo_threshold <type> [ option ]\n"
		"   <type> 0-GBE 1 G999 2-tls 3-cpu \n"
		"  -AF_th <value> \n"
		"  -tx <value> \n"
		"  -rx <value> \n"
		"  -eoc <value> \n"
		"\n"

		"\n"
		"Example:  mac_fifo_threshold  0 -AF_th 0x40 -tx 0x40 -eoc 0x05 \n"
	);





	CLI_defineCmd("MEA globals set if wd_disable",
		(CmdFunc)MEA_CLI_Set_Globals_WD_Disable,
		"Set global IF  wd_disable\n",
		"Usage  : wd_disable <val> 1 -disable  0 -enable\n"
		"\n"
		"Example: - wd_disable 1 \n"
	);
	CLI_defineCmd("MEA globals set if PacketGen_enable",
		(CmdFunc)MEA_CLI_Set_Globals_PacketGen,
		"Set global IF  PacketGen_enable\n",
		"Usage  : PacketGen <enable> \n"
		"                   <enable> (0-disable 1-enable) \n"
		"Example: - PacketGen_enable 1\n"
	);








	CLI_defineCmd("MEA globals set if close_loop_utopia",

		(CmdFunc)MEA_CLI_Set_Globals_close_loop_utopia,

		"Set global IF utopia loop mode\n",

		"Usage  : close_loop_utopia <mod> \n"

		"         <mod> -  0 - no loop \n"
		"                  1 - master_tx to slave_tx (no ATM card) \n"
		"                  2 - master_tx to slave_rx \n"
		"Examples: - close_loop_utopia 0 \n"

		"          - close_loop_utopia 1 \n"

		"          - close_loop_utopia 2 \n");





	CLI_defineCmd("MEA globals set if forwarder_addr_width",
		(CmdFunc)MEA_CLI_Set_Globals_forwarder_addr_width,
		"Set global IF Forwarder addr width\n",
		"Usage  : forwarder_addr_width <value>\n\n"

		"Example: forwarder_addr_width 16 \n");




	CLI_defineCmd("MEA globals set if SRV_E_hash_type",
		(CmdFunc)MEA_CLI_Set_Globals_SRV_extenal_hash_type,
		"Set global IF SRV_E_hash_type \n",
		"Usage  : SRV_E_hash_type <hash1_type> <hash2_type>\n"
		"         <hash> - hash function type \n"
		"                   0 :3\n"
		"Example: SRV_E_hash_type 3 \n");

	CLI_defineCmd("MEA globals set if config_fwd_hash",
		(CmdFunc)MEA_CLI_Set_Globals_forwarder_Config_hash,
		"Set global IF forwarder hash type \n",
		"Usage  : forwarder_hash_type <hash1_type> <hash2_type>\n"
		"        <key> <hash1/2_type> - forwarder hash function type \n"
		"                   0 - VLAN / 1-random / 2-decrement / 3-increment\n"
		"Example: config_fwd_hash 0 3 3\n");




	CLI_defineCmd("MEA globals set if forwarder_aging",
		(CmdFunc)MEA_CLI_Set_Globals_forwarder_aging,
		"Set global DSE aging  values\n",
		"Usage  : forwarder_aging  <enable> <last_table> <sweep_interval>\n"
		"        <enable>             - (0-disable / 1-enable)\n"
		"        <last_table>         - (0..15)\n"
		"        <sweep_interval>     - \n"
		"Examples: - forwarder_aging 1 0 15\n");


	CLI_defineCmd("MEA globals set if forwarder_del_reinit_disable",
		(CmdFunc)MEA_CLI_Set_Globals_forwarder_diseableDelReinit,
		"Set global forwarder delete on reinit disable \n",
		"Usage  : lag_enable <value>\n"
		"         <value> - 0..1 \n"
		"Example: forwarder_del_reinit_disable  0\n");


	CLI_defineCmd("MEA globals set if forwarder_act_offset",
		(CmdFunc)MEA_CLI_Set_Globals_forwarder_act_offset,
		"Set global IF Forwarder start action offset \n",
		"Usage  : forwarder_act_offset <value>\n"
		"         <value> - 0..3\n"
		"Example: forwarder_act_offset 0\n");

	CLI_defineCmd("MEA globals set if lag_enable",
		(CmdFunc)MEA_CLI_Set_Globals_lag_enable,
		"Set global lag enable \n",
		"Usage  : lag_enable <value>\n"
		"         <value> - 0 (disable) / 1 (enable)\n"
		"Example: lag_enable 0\n");




	CLI_defineCmd("MEA globals set if forwarder_enable",
		(CmdFunc)MEA_CLI_Set_Globals_forwarder_enable,
		"Set global IF Forwarder block enable \n",
		"Usage  : forwarder_enable <value>\n"
		"         <value> - 0 (disable) / 1 (enable)\n"
		"Example: forwarder_enable 0\n");



	CLI_defineCmd("MEA globals set if learning_disable",
		(CmdFunc)MEA_CLI_Set_Globals_learning_disable,
		"Set global IF Forwarder block enable \n",
		"Usage  : learning_disable <value>\n"
		"         <value> - 0 (disable) / 1 (enable)\n"
		"Example: learning_disable 0\n");

	CLI_defineCmd("MEA globals set if check_srv_key",
		(CmdFunc)MEA_CLI_Set_Globals_check_srv_key,
		"Set global IF check_srv_key \n",
		"Usage  : check_srv_key <value>\n"
		"         <value> - 0 (disable) / 1 (enable)\n"
		"Example: check_srv_key 0\n");




	CLI_defineCmd("MEA globals set if learn_disable",
		(CmdFunc)MEA_CLI_Set_Globals_learn_disable,
		"Set global IF Forwarder block learning \n",
		"Usage  : forwarder_enable <value>\n"
		"         <value> - 0 (disable) / 1 (enable)\n"
		"Example: learn_disable 0\n");


	CLI_defineCmd("MEA globals set if mstp_enable",
		(CmdFunc)MEA_CLI_Set_Globals_mstp_enable,
		"Set global IF Forwarder mstp_enable enable \n",
		"Usage  : mstp_enable <value>\n"
		"         <value> - 0 (disable) / 1 (enable)\n"
		"Example: mstp_enable 0\n");

	CLI_defineCmd("MEA globals set if rstp_enable",
		(CmdFunc)MEA_CLI_Set_Globals_rstp_enable,
		"Set global IF Forwarder rstp_enable enable \n",
		"Usage  : mstp_enable <value>\n"
		"         <value> - 0 (disable) / 1 (enable)\n"
		"Example: rstp_enable 0\n");


	CLI_defineCmd("MEA globals set if forwarder_OutPort",
		(CmdFunc)MEA_CLI_Set_Globals_forwarder_OutPut,
		"Set global IF Forwarder out port select algorithm \n",
		"Usage  : forwarder_OutPort <value>\n"
		"         <value> - 0 (Enhanced) the selected out port is a subset of the broadcast domain "
		"                   1 (Default - regular) force out port\n"
		"Example: forwarder_OutPort 0\n");

	CLI_defineCmd("MEA globals set if mirror_ces_port",
		(CmdFunc)MEA_CLI_Set_Globals_mirror_port_ces,
		"Set global IF mirror_ces_port  port or cesId\n",
		"Usage  : mirror_ces_port <enable><type><portId/cesId><Src_Port>\n"
		"         <enable> - 0-(disable) 1-(Enable) \n"
		"         <type> - 0-(port) 1-(ces) \n"
		"         <portId/cesId> - <ces/port value> \n"
		"         <src_port>     -destination port \n"
		"Example: mirror_ces_port 1 1 10 125\n");

	CLI_defineCmd("MEA globals set if mirror_ingress_1G",
		(CmdFunc)MEA_CLI_Set_Globals_mirror_ingress_1G,
		"Set global IF mirror_ces_port  port or cesId\n",
		"Usage  : mirror_ingress_1G <enable><Port>\n"
		"         <enable> - 0-(disable) 1-(Enable) \n"
		"         <port>     - ingress interface Id   \n"
		"Example: mirror_ingress_1G 1 105 \n");

	CLI_defineCmd("MEA globals set if mirror_ingress_10G",
		(CmdFunc)MEA_CLI_Set_Globals_mirror_ingress_10G,
		"Set global IF mirror_ingress_10G  port or cesId\n",
		"Usage  : mirror_ingress_10G <enable><Port>\n"
		"         <enable> - 0-(disable) 1-(Enable) \n"
		"         <port>     -    \n"
		"Example: mirror_ingress_10G 1 118 \n");

	CLI_defineCmd("MEA globals set bm PacketAnalyzer_EtherType",
		(CmdFunc)MEA_CLI_Set_Globals_PacketAnalyzer_EtherType,
		"Set global BM  PacketAnalyzer_EtherType\n",
		"Usage  : PacketAnalyzer_EtherType <EtherType> \n"
		"                   <EtherType> ( 4 hex digits) \n"
		"Example: - PacketAnalyzer_EtherType 9999 \n"
	);
	CLI_defineCmd("MEA globals set bm PacketAnalyzer_enable",
		(CmdFunc)MEA_CLI_Set_Globals_PacketAnalyzer_enable,
		"Set global BM  PacketAnalyzer_enable\n",
		"Usage  : PacketAnalyzer <enable> \n"
		"                   <enable> (0-disable 1-enable) \n"
		"Example: - PacketAnalyzer_enable 1\n"
	);

	CLI_defineCmd("MEA globals set bm PacketAnalyzer_port",
		(CmdFunc)MEA_CLI_Set_Globals_PacketAnalyzer_Port,
		"Set global BM  PacketAnalyzer_port\n",
		"Usage  : PacketAnalyzer <port> \n"
		"                   <port> () \n"
		"Example: - PacketAnalyzer_port 127\n");


	CLI_defineCmd("MEA globals set bm policer_enable",
		(CmdFunc)MEA_CLI_Set_Globals_policer_enable,
		"Set global policer enable \n",
		"Usage  : policer_enable  <value> \n"
		"        <value> - 0 (disable) / 1 (enable) \n"
		"Examples: - policer_enable 1\n"
		"          - policer_enable 0\n");

	CLI_defineCmd("MEA globals set bm shaper_enable",
		(CmdFunc)MEA_CLI_Set_Globals_shaper_enable,
		"Set global shaper enable \n",
		"Usage  : shaper_enable  < port value> < cluster value> < Priqueue value>\n"
		"        <value> - 0 (disable) / 1 (enable) \n"
		"Examples: - shaper_enable 0 1 0\n"
		"          - shaper_enable 1 1 0 \n");

	CLI_defineCmd("MEA globals set bm wred_offset",
		(CmdFunc)MEA_CLI_Set_Globals_wred_offset,
		"Set global wred offset \n",
		"Usage  : wred_offset  <value> \n"
		"        <value> - (0..255)\n"
		"Examples: - wred_offset 255\n"
		"          - wred_offset 0\n");


	CLI_defineCmd("MEA globals set bm target_MTU",
		(CmdFunc)MEA_CLI_Set_Globals_set_target_MTU,
		"Set global target_MTU for fragment packet\n",
		"Usage  : target_MTU  <value> \n"
		"        <value> - \n"
		"Examples: - target_MTU 1500\n"
		"          - target_MTU 1536\n");

	CLI_defineCmd("MEA globals set bm frag_Type",
		(CmdFunc)MEA_CLI_Set_Globals_set_frag_Type,
		"Set global frag_Type \n",
		"Usage  : frag_Type <value> \n"
		"        <frag_type> 0-internal 1-external  \n"
		"        <frag after frag > 0-disable 1-enable  \n"
		"Examples: - frag_Type 0 0\n"
		"          - frag_Type 1 0\n");


	CLI_defineCmd("MEA globals set bm reorder_frag_time_out",
		(CmdFunc)MEA_CLI_Set_Globals_set_reorder_frag_time_out,
		"Set global reorder_frag_time_out \n",
		"Usage  : reorder_frag_time_out  <value> \n"
		"        <value> microSec - \n"
		"Examples: - reorder_frag_time_out 1000\n"
		"          - reorder_frag_time_out 1000\n");




	CLI_defineCmd("MEA globals set bm wbrg_eth",
		(CmdFunc)MEA_CLI_Set_Globals_WBRG_swap,
		"Set global wbrg_eth  \n",
		"Usage  : wbrg_eth  <lan eth><access eth> \n"

		"Examples: - wbrg_eth 0x8100 0x88a8\n"
		"          - wbrg_eth 0x9100 0x88a8\n");




	CLI_defineCmd("MEA globals set bm best_effort_enable",
		(CmdFunc)MEA_CLI_Set_Globals_best_effort_enable,
		"Set global best effort enable \n",
		"Usage  : best_effort_enable  <value> \n"
		"        <value> - 0 (disable) / 1 (enable) \n"
		"Examples: - best_effort_enable 1\n"
		"          - best_effort_enable 0\n");

	CLI_defineCmd("MEA globals set bm vcmux_bcm",
		(CmdFunc)MEA_CLI_Set_Globals_vcmux_BCM,
		"Set global bvcmux_bcm \n",
		"Usage  : vcmux_bcm  <valid> \n"
		"        <valid> - 0 (disable) / 1 (enable) \n"
		"Examples: - vcmux_bcm 1\n"
		"          - vcmux_bcm 0\n");


	CLI_defineCmd("MEA globals set bm afdx_rx_ticks",
		(CmdFunc)MEA_CLI_Set_RxAfdxTicks,
		"Set global ccm_ticks ticks values\n",
		"Usage  : afdx_rx_ticks  <value>\n"
		"        <value> - (16bit)\n"
		"Examples: - afdx_rx_ticks 1200 \n");


	CLI_defineCmd("MEA globals set bm ccm_ticks",
		(CmdFunc)MEA_CLI_Set_CCMTicks,
		"Set global ccm_ticks ticks values\n",
		"Usage  : policer_ticks  <slow> <fast>\n"
		"        <slow> - (16bit)\n"
		"        <fast> - (16bit)\n"
		"Examples: - ccm_ticks 1200 10000\n");

	CLI_defineCmd("MEA globals set bm policer_ticks",
		(CmdFunc)MEA_CLI_Set_PolicerTicks,
		"Set global policer ticks values\n",
		"Usage  : policer_ticks  <slow> <fast>\n"
		"        <slow> - (984..2^16-1)\n"
		"        <fast> - (40..2^16-1)\n"
		"Examples: - policer_ticks 10000 5000\n");

	CLI_defineCmd("MEA globals set bm ingflowpolicer_ticks",
		(CmdFunc)MEA_CLI_Set_IngFlowPolicerTicks,
		"Set global ingflowpolicer ticks values\n",
		"Usage  : ingflowpolicer_ticks  <slow> <fast>\n"
		"        <slow> - (984..2^16-1)\n"
		"        <fast> - (40..2^16-1)\n"
		"Examples: - ingflowpolicer_ticks 10000 5000\n");




	CLI_defineCmd("MEA globals set bm shaper port_ticks",
		(CmdFunc)MEA_CLI_Set_ShaperPortTicks,
		"Set global Shaper port_ticks values\n",
		"Usage  : Shaper_ticks  <fast> <slow_Multiplexer>\n"
		"        <fast>             - (1..2^16-1)\n"
		"        <slow_Multiplexer> - (0-7)\n"
		"Examples: - shaper port_ticks 10000 1\n");

	CLI_defineCmd("MEA globals set bm shaper cluster_ticks",
		(CmdFunc)MEA_CLI_Set_ShaperClusterTicks,
		"Set global Shaper cluster_ticks values\n",
		"Usage  : Shaper_ticks  <fast> <slow_Multiplexer>\n"
		"        <fast>             - (1..2^16-1)\n"
		"        <slow_Multiplexer> - (0-7)\n"
		"Examples: - shaper cluster_ticks 10000 1\n");

	CLI_defineCmd("MEA globals set bm shaper priqueue_ticks",
		(CmdFunc)MEA_CLI_Set_ShaperPriQueueTicks,
		"Set global Shaper priqueue_ticks values\n",
		"Usage  : Shaper_ticks  <fast> <slow_Multiplexer>\n"
		"        <fast>             - (1..2^16-1)\n"
		"        <slow_Multiplexer> - (0-7)\n"
		"Examples: - shaper priqueue_ticks 10000 1\n");

	CLI_defineCmd("MEA globals set if Oam add",
		(CmdFunc)MEA_CLI_Set_GlobalOamAdd,
		"Set up to 8 EtherType that shall be trapped to L2CP\n",
		"Usage  : add <EthType> <l2cp>\n"
		"        <EthType>            - ( 4 hex digits)\n"
		"        <l2cp> - suffix (0..63)\n"
		"Examples: - Oam  0806 10\n");
	CLI_defineCmd("MEA globals set if Oam remove",
		(CmdFunc)MEA_CLI_Set_GlobalOamRemove,
		"Remove 1 or all OAM\n",
		"Usage  : remove <EthType> | all\n"
		"        <EthType>            - ( 4 hex digits)\n"
		"Examples: - remove 0806\n");

	CLI_defineCmd("MEA globals set bm bm_enable",
		(CmdFunc)MEA_CLI_Set_Globals_bm_enable,
		"Set global bm enable \n",
		"Usage  : bm_enable  <value> \n"
		"        <value> - 0 (disable) / 1 (enable) \n"
		"Examples: - bm_enable 1\n"
		"          - bm_enable 0\n");

	CLI_defineCmd("MEA globals show",
		(CmdFunc)MEA_CLI_Show_Globals,
		"Show globals\n",
		"Usage: Show globals all | if | bm  | device"
		"\n"
		"Examples: - Show all\n"
		"          - Show if \n"
		"          - Show bm \n"
	);

	CLI_defineCmd("MEA globals set device global_parser  ",
		(CmdFunc)MEA_CLI_GlobalParser_Set,
		"set entry\n",
		"set <index> <type> <valid> <value> \n"
		"-------------------------------------------\n"
		"      <index>       -  id \n"
		"      <type>        - CAM (0:4)\n"
		"      <valid>       - 0 - disable / 1 - enable  \n"
		"      <value> - :  (0000) hex digits  \n"
		"Examples: - set 1 0 1 8100\n"
		"          - set 2 1 1 8100\n"

		"\n");

}

