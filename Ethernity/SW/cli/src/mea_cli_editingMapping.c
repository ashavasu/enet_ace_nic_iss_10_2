/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "mea_cli_editingMapping.h"
#include "cli_eng.h"
#include "mea_cli.h"

MEA_Status MEA_CLI_EditingMappingProfile_Create(int argc, char *argv[])
{

    MEA_EditingMappingProfile_Entry_dbt entry;
    MEA_EditingMappingProfile_Id_t      id;
    int i;
    int num_of_params;
    MEA_Uint32 ks_priority,ks_color;

    /* Check minimum number of parameters */
    if (argc < 2) {
        return MEA_ERROR;
    }

    /* Check for id */
    if (MEA_OS_strcmp(argv[1],"auto") == 0) {
        id = MEA_PLAT_GENERATE_NEW_ID;
    } else {
        id = MEA_OS_atoi(argv[1]);
    }

    /* Set default values */
    MEA_OS_memset(&entry,0,sizeof(entry));
    for (ks_priority=0;ks_priority<MEA_NUM_OF_ELEMENTS(entry.item_table);ks_priority++) {
        for (ks_color=0;ks_color<MEA_NUM_OF_ELEMENTS(entry.item_table[0].colorItem_table);ks_color++) {
            entry.item_table[ks_priority].colorItem_table[ks_color].stamp_priority_value = (MEA_Uint8)ks_priority;
            entry.item_table[ks_priority].colorItem_table[ks_color].stamp_color_value    = (MEA_Uint8)ks_color;
        }
    }

    /* Scan for options */
    i=2;
    while(i<argc) {
        if (MEA_OS_strcmp(argv[i],"-i") == 0) {
            num_of_params=5;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            ks_priority  = MEA_OS_atoi(argv[i+1]);
            if (ks_priority >= MEA_NUM_OF_ELEMENTS(entry.item_table)) {
                CLI_print("Error: <ks_priority> (%d) >= max value (%d) \n",
                          ks_priority,MEA_NUM_OF_ELEMENTS(entry.item_table));
                return MEA_ERROR;
            }
            ks_color     = MEA_OS_atoi(argv[i+2]);
            if (ks_color >= MEA_NUM_OF_ELEMENTS(entry.item_table[0].colorItem_table)) {
                CLI_print("Error: <ks_color> (%d) >= max value (%d) \n",
                          ks_color,MEA_NUM_OF_ELEMENTS(entry.item_table[0].colorItem_table));
                return MEA_ERROR;
            }
            entry.item_table[ks_priority].colorItem_table[ks_color].stamp_priority_value = MEA_OS_atoi(argv[i+3]);
            entry.item_table[ks_priority].colorItem_table[ks_color].stamp_color_value    = MEA_OS_atoi(argv[i+4]);
            i+=num_of_params;
            continue;
        }


        if (MEA_OS_strcmp(argv[i],"-d") == 0) {
            num_of_params=2;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough parameter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.direction = MEA_OS_atoi(argv[i+1]);
            i+=num_of_params;
            continue;
        }
            
        CLI_print("Error: Invalid option (i=%d)\n",i);
        return MEA_ERROR;

    }

    if (MEA_API_Create_EditingMappingProfile_Entry(MEA_UNIT_0,&entry,&id) != MEA_OK) {
        CLI_print("Error: MEA_API_Create_EditingMappingProfile_Entry failed \n");
        return MEA_OK;
    }

    CLI_print("Done. (id=%d)\n",id);

    /* Return to Caller */
    return MEA_OK;
}

MEA_Status MEA_CLI_EditingMappingProfile_Modify(int argc, char *argv[])
{

    MEA_EditingMappingProfile_Entry_dbt entry;
    MEA_EditingMappingProfile_Id_t      id;
    int i;
    int num_of_params;
    MEA_Uint32 ks_priority,ks_color;

    /* Check minimum number of prarmaters */
    if (argc < 2) {
        return MEA_ERROR;
    }



    id = MEA_OS_atoi(argv[1]);

    if (MEA_API_Get_EditingMappingProfile_Entry(MEA_UNIT_0,id,&entry) != MEA_OK) {
        CLI_print("Error: MEA_API_Get_EditingMappingProfile_Entry failed \n");
        return MEA_OK;
    }

    i=2;
    while(i<argc) {
        if (MEA_OS_strcmp(argv[i],"-i") == 0) {
            num_of_params=5;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough paramter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            ks_priority  = MEA_OS_atoi(argv[i+1]);
            if (ks_priority >= MEA_NUM_OF_ELEMENTS(entry.item_table)) {
                CLI_print("Error: <ks_priority> (%d) >= max value (%d) \n",
                          ks_priority,MEA_NUM_OF_ELEMENTS(entry.item_table));
                return MEA_ERROR;
            }
            ks_color     = MEA_OS_atoi(argv[i+2]);
            if (ks_color >= MEA_NUM_OF_ELEMENTS(entry.item_table[0].colorItem_table)) {
                CLI_print("Error: <ks_color> (%d) >= max value (%d) \n",
                          ks_color,MEA_NUM_OF_ELEMENTS(entry.item_table[0].colorItem_table));
                return MEA_ERROR;
            }
            entry.item_table[ks_priority].colorItem_table[ks_color].stamp_priority_value = MEA_OS_atoi(argv[i+3]);
            entry.item_table[ks_priority].colorItem_table[ks_color].stamp_color_value    = MEA_OS_atoi(argv[i+4]);
            i+=num_of_params;
            continue;
        }


        if (MEA_OS_strcmp(argv[i],"-d") == 0) {
            num_of_params=2;
            if ((i+num_of_params)>argc) {
                CLI_print("Error: Not enough paramter for option %s \n",argv[i]);
                return MEA_ERROR;
            }
            entry.direction = MEA_OS_atoi(argv[i+1]);
            i+=num_of_params;
            continue;
        }
            
        CLI_print("Error: Invalid option (i=%d)\n",i);
        return MEA_ERROR;


    }

    if (MEA_API_Set_EditingMappingProfile_Entry(MEA_UNIT_0,id,&entry) != MEA_OK) {
        CLI_print("Error: MEA_API_Set_EditingMappingProfile_Entry failed \n");
        return MEA_OK;
    }


    /* Return to Caller */
    return MEA_OK;
}

MEA_Status MEA_CLI_EditingMappingProfile_Delete(int argc, char *argv[])
{

    MEA_EditingMappingProfile_Id_t      id;
    MEA_Bool                            found;

    /* Check minimum number of prarmeters */
    if (argc != 2) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        id = MEA_OS_atoi(argv[1]);
        if (MEA_API_Delete_EditingMappingProfile_Entry(MEA_UNIT_0,id) != MEA_OK) {
            CLI_print("Error: MEA_API_Delete_EditingMappingProfile_Entry failed \n");
            return MEA_ERROR;
        }
    } else {

        if (MEA_API_GetFirst_EditingMappingProfile_Entry(MEA_UNIT_0,
                                                         &id,
                                                         NULL,
                                                         &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetFirst_EditingMappingProfile_Entry failed \n");
            return MEA_OK;
        }
        while (found) {
            if (MEA_API_Delete_EditingMappingProfile_Entry(MEA_UNIT_0,id) != MEA_OK) {
                CLI_print("Error: MEA_API_Delete_EditingMappingProfile_Entry failed \n");
                return MEA_OK;
            }
            if (MEA_API_GetNext_EditingMappingProfile_Entry(MEA_UNIT_0,
                                                            &id,
                                                            NULL,
                                                            &found) != MEA_OK) {
                CLI_print("Error: MEA_API_GetNext_EditingMappingProfile_Entry failed \n");
                return MEA_OK;
            }
        }
    }


    /* Return to Caller */
    return MEA_OK;
}

MEA_Status MEA_CLI_EditingMappingProfile_Show(int argc, char *argv[])
{
    MEA_EditingMappingProfile_Entry_dbt entry;
    MEA_EditingMappingProfile_Id_t      id;
    MEA_Bool                            found;
    MEA_Uint32                          count;
    MEA_Uint32                          ks_priority,ks_color;
    MEA_Bool                            all_flag=MEA_TRUE;
    MEA_Uint32                          num_of_owners;
    MEA_EditingMappingProfile_ColorItem_Entry_dbt* colorItem;


    /* Check minimum number of prarmaters */
    if (argc != 2) {
        return MEA_ERROR;
    }

    if (MEA_OS_strcmp(argv[1],"all") != 0) {
        id = MEA_OS_atoi(argv[1]);
        if (MEA_API_Get_EditingMappingProfile_Entry(MEA_UNIT_0,id,&entry) != MEA_OK) {
            CLI_print("Error: MEA_API_Get_EditingMappingProfile_Entry failed \n");
            return MEA_ERROR;
        }
        found = MEA_TRUE;
        all_flag = MEA_FALSE;
    } else {
        if (MEA_API_GetFirst_EditingMappingProfile_Entry(MEA_UNIT_0,
                                                         &id,
                                                         &entry,
                                                         &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetFirst_EditingMappingProfile_Entry failed \n");
            return MEA_OK;
        }
    }

    count=0;
    while (found) {
        if (count++ == 0) {
            CLI_print("Profile Owners Direction  Key      Key     Result   Result\n"
                      "Id                        Priority Color   Priority Color \n"
                      "------- ------ ---------- -------- ------- -------- -------\n");
        } else {
            CLI_print("------- ------ ---------- -------- ------- -------- -------\n");
        }

        if (MEA_API_GetOwner_EditingMappingProfile_Entry(MEA_UNIT_0,id,&num_of_owners) != MEA_OK) {
            CLI_print("Error: MEA_API_GetOwner_EditingMappingProfile_Entry failed \n");
            return MEA_OK;
        }

        for (ks_priority=0;ks_priority<MEA_NUM_OF_ELEMENTS(entry.item_table);ks_priority++) {
            for (ks_color=0;ks_color<MEA_NUM_OF_ELEMENTS(entry.item_table[0].colorItem_table);ks_color++) {
                if ((ks_priority==0) && (ks_color==0)) {
                    CLI_print("%7d %6d %-10s ",
                              id,
                              num_of_owners,
                              (entry.direction == MEA_DIRECTION_GENERAL   ) ? "General" :
                              (entry.direction == MEA_DIRECTION_UPSTREAM  ) ? "Upstream" :
                              (entry.direction == MEA_DIRECTION_DOWNSTREAM) ? "Downstream" :
                              "Unknown");
                } else {
                    CLI_print("%-7s %-6s %-10s ","","","");
                }


                if (ks_color == 0) {
                     CLI_print("%8d ",ks_priority);
                } else {
                     CLI_print("%-8s ","");
                }
                colorItem = &(entry.item_table[ks_priority].colorItem_table[ks_color]);
                CLI_print("%-7s %8d %-7s\n",
                          (ks_color==MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN ) ? "Green"  :
                          (ks_color==MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW) ? "Yellow" :
                          "Unknown",
                          colorItem->stamp_priority_value,
                          (colorItem->stamp_color_value==MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN ) ? "Green"  :
                          (colorItem->stamp_color_value==MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW) ? "Yellow" :
                          "Unknown");
            }
        }

        if (!all_flag) {
            break;
        }


        if (MEA_API_GetNext_EditingMappingProfile_Entry(MEA_UNIT_0,
                                                        &id,
                                                        &entry,
                                                        &found) != MEA_OK) {
            CLI_print("Error: MEA_API_GetNext_EditingMappingProfile_Entry failed \n");
            return MEA_OK;
        }
    }

    if (count==0) {
        CLI_print("No Editing Mapping Profiles\n");
        return MEA_OK;
    } 

    CLI_print("------- ------ ---------- -------- ------- -------- -------\n");
    CLI_print("Display %d proiles \n",count);

    /* Return to Caller */
    return MEA_OK;
}


MEA_Status MEA_CLI_EditingMappingProfile_AddDebugCmds(void)
{


           CLI_defineCmd("MEA editing mapping show",
                  (CmdFunc)MEA_CLI_EditingMappingProfile_Show, 
                  "Show   Editing Mapping Profile(s) \n",
                  "show   <id>|all\n"
                  "--------------------------------------------------------------------------------\n"
                  "       <id>}all - Profile id  or all\n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - show all \n"
                  "          - show 2\n");

           CLI_defineCmd("MEA editing mapping delete",
                  (CmdFunc)MEA_CLI_EditingMappingProfile_Delete, 
                  "Delete Editing Mapping Profile(s) \n",
                  "delete <id>|all\n"
                  "--------------------------------------------------------------------------------\n"
                  "       <id>}all - Profile id  or all\n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - delete all \n"
                  "          - delete 2\n");

           CLI_defineCmd("MEA editing mapping modify",
                  (CmdFunc)MEA_CLI_EditingMappingProfile_Modify, 
                  "Update Editing Mapping Profile \n",
                  "Usage: modify <id>  \n"
                  "       [-i  <ks L2_PRI> <ks COLOR> <rs L2_PRI> <ks COLOR> \n"
                  "       [-d  <direction>]\n"
                  "-----------------------------------------------------------------------------\n"
                  "      <id> - Editing Mapping Profile id or auto to generate new id \n"
                  "      (-i option) \n"
                  "          <ks L2_PRI> - Key    Mapping Stamping Priority (0-7)\n"
                  "          <ks COLOR>  - Key    Mapping Stamping Color (0-Green/1-Yellow)\n"
                  "          <rs L2_PRI> - Result Mapping Stamping Priority (0-7)\n"
                  "          <rs COLOR>  - Result Mapping Stamping Color (0-Green/1-Yellow)\n"
                  "          Notes: - The -i option can repeat many time for any item we \n"
                  "                 - In case specific combination of <ks COLOR> and <ks L2_PRI>  \n"
                  "                   Not specifiy it mark as default (no change).\n"
                  "      -d (direction option)\n"
                  "         <direction>  - 0=General / 1=Upstream / 2=Downstream\n"
                  "                        Notes: \n"
                  "                         - value 0   should be use in ENET3000\n"
                  "                         - Value 1/2 should be use in ENET4000\n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - modify 0 -i 3 0 7 0 -i 4 1 3 1\n");

           CLI_defineCmd("MEA editing mapping create",
                  (CmdFunc)MEA_CLI_EditingMappingProfile_Create, 
                  "Create Editing Mapping Profile \n",
                  "Usage: create auto|<id>  \n"
                  "       [-i  <ks L2_PRI> <ks COLOR> <rs L2_PRI> <ks COLOR> \n"
                  "       [-d  <direction>]\n"
                  "-----------------------------------------------------------------------------\n"
                  "      auto|<id> - Editing Mapping Profile id or auto to generate new id \n"
                  "      (-i option) \n"
                  "          <ks L2_PRI> - Key    Mapping Stamping Priority (0-7)\n"
                  "          <ks COLOR>  - Key    Mapping Stamping Color (0-Green/1-Yellow)\n"
                  "          <rs L2_PRI> - Result Mapping Stamping Priority (0-7)\n"
                  "          <rs COLOR>  - Result Mapping Stamping Color (0-Green/1-Yellow)\n"
                  "          Notes: - The -i option can repeat many time for any item we \n"
                  "                 - In case specific combination of <ks COLOR> and <ks L2_PRI>  \n"
                  "                   Not specifiy it mark as default (no change).\n"
                  "      -d (direction option)\n"
                  "         <direction>  - 0=General / 1=Upstream / 2=Downstream\n"
                  "                        Notes: \n"
                  "                         - value 0   should be use in ENET3000\n"
                  "                         - Value 1/2 should be use in ENET4000\n"
                  "--------------------------------------------------------------------------------\n"
                  "Examples: - create auto  \n"
                  "          - create auto -i 3 0 7 0 -i 4 1 3 1\n");


    return MEA_OK;
}

