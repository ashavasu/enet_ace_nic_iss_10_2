/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_port.h"


static MEA_Status MEA_CLI_port_Set_BRG_Pvid_Set(int argc, char *argv[])
{

    MEA_Uint32 numOfArgInCommand;
    char * ch;
    int i,len;
    MEA_Port_t port;
    MEA_BRG_PVID_dbt    entry;
    

    if (argc <= 2) {
        return MEA_ERROR;
    }

    port = MEA_OS_atoi(argv[1]);

    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_TRUE) ==MEA_FALSE) {
        CLI_print("Error: port %d (%s) is invalid \n",port,argv[1]);
        return MEA_OK;
    }    

    MEA_OS_memset(&entry,0,sizeof(entry));
    
    if(MEA_API_Get_BRG_PVID_Entry(MEA_UNIT_0, port,&entry)!=MEA_OK){

        return MEA_OK;
    }


    for(i=2; i<argc ;i++){

        if(!MEA_OS_strcmp(argv[i],"-dst_pvid")){ 
            numOfArgInCommand = 1;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);

            entry.egress.Pvid = MEA_OS_atoi(argv[i+1]);
            
        }
        if(!MEA_OS_strcmp(argv[i],"-src_pvid")){ 
            numOfArgInCommand = 1;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);

            entry.ingress.Pvid = MEA_OS_atoi(argv[i+1]);

        }
        if(!MEA_OS_strcmp(argv[i],"-portType")){ 
            numOfArgInCommand = 1;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);

            entry.egress.egress_type = MEA_OS_atoi(argv[i+1]);

        }
        if(!MEA_OS_strcmp(argv[i],"-encap_swap")){ 
            numOfArgInCommand = 1;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);

            entry.egress.encap_swap_mode = MEA_OS_atoi(argv[i+1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-encap_select")) {
            numOfArgInCommand = 2;
            ch = argv[i];
            len = (i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i, len, argc, ch);

            entry.egress.external_select = MEA_OS_atoi(argv[i + 1]);
            entry.egress.internal_select = MEA_OS_atoi(argv[i + 2]);

        }




        i += numOfArgInCommand;

    }


    if (MEA_API_Set_BRG_PVID_Entry(MEA_UNIT_0, port, &entry) != MEA_OK) {
        CLI_print("error Set_BRG_PVID \n");
        return MEA_OK;
    }


    CLI_print("Done Create: \n");
    return MEA_OK;

}
static MEA_Status MEA_CLI_port_BRG_Pvid_Show(int argc, char *argv[])
{
    MEA_Port_t port, from_port, to_port;
    MEA_BRG_PVID_dbt  entry;
    MEA_Uint32 count;

    if (argc < 2) return MEA_ERROR;


    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        from_port = 0;
        to_port = MEA_MAX_PORT_NUMBER;
    }
    else {
        port = from_port = to_port = MEA_OS_atoi(argv[1]);
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) == MEA_FALSE) {
            CLI_print("Error: port %d (%s) is invalid \n", port, argv[1]);
            return MEA_OK;
        }
    }
	
    count = 0;
    for (port = from_port; port <= to_port; port++) {
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_INGRESS_TYPE, MEA_TRUE) == MEA_FALSE) {
            continue;
        }

        if (count++ == 0) {
            CLI_print(" Port dst  egress  enc        external  Internal src    \n"
                      "      pvid type    swap_mod   type      type     pvid   \n"
                      " ---- ---- ------- ---------- -------- -------- -------  \n");


        }

        if (MEA_API_Get_BRG_PVID_Entry(MEA_UNIT_0, port, &entry) != MEA_OK) {

            return MEA_OK;
        }

        CLI_print(" %4d ", port);
        CLI_print("%4d ", entry.egress.Pvid);
        CLI_print("%7s ", (entry.egress.egress_type) == 0 ? "untag" : (
            (entry.egress.egress_type) == 1 ? "tag" : (
            (entry.egress.egress_type) == 2 ? "double" : "else")));

        CLI_print("%10d ", entry.egress.encap_swap_mode);
        CLI_print("%7s ", (entry.egress.external_select == 0) ? "LAN" : "Access");
        CLI_print("%7s ", (entry.egress.internal_select == 0) ? "LAN" : "Access");
        CLI_print("%4d ", entry.ingress.Pvid);
        CLI_print("\n");





    }

    return MEA_OK;
}

/************************************************************************/
/* ISOLATE                                                              */
/************************************************************************/
static MEA_Status MEA_CLI_ISOLATE_Delete(int argc, char *argv[])
{
    MEA_Uint16 port, from_port, to_port;
    
    //MEA_Uint32 count;
    MEA_Bool  exist = MEA_FALSE;
   
    if (argc < 2) return MEA_ERROR;


    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        from_port = 0;
        to_port = MEA_ISOLATE_MAX_PROF - 1;
    }
    else {
        port = from_port = to_port = MEA_OS_atoi(argv[1]);
        if (MEA_API_ISOLATE_IsExist(MEA_UNIT_0, port, MEA_FALSE, &exist) == MEA_ERROR)
        {
            return MEA_ERROR;
        }
        if (exist == MEA_FALSE)
            return MEA_ERROR;

    }

    //count = 0;
    for (port = from_port; port <= to_port; port++) {
        exist = MEA_FALSE;
        if (MEA_API_ISOLATE_IsExist(MEA_UNIT_0, port, MEA_TRUE, &exist) == MEA_ERROR)
        {
            continue;
        }
        if (exist == MEA_FALSE)
            continue;
        if (MEA_API_ISOLATE_Delete_Entry(MEA_UNIT_0, port) != MEA_OK) {

            return MEA_OK;
        }
        

    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_ISOLATE_Show(int argc, char *argv[])
{
    MEA_Uint16 port, from_port, to_port;
    MEA_ISOLATE_dbt  entry;
    MEA_Uint32 count;
    MEA_Bool  exist = MEA_FALSE;
    MEA_Uint32 ClusterEx_Id;
    MEA_Uint32 val;

    if (argc < 2) return MEA_ERROR;


    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        from_port = 1;
        to_port = MEA_ISOLATE_MAX_PROF - 1;
    }
    else {
        port = from_port = to_port = MEA_OS_atoi(argv[1]);
        if (MEA_API_ISOLATE_IsExist(MEA_UNIT_0, port, MEA_FALSE, &exist) == MEA_ERROR)
        {
            return MEA_ERROR;
        }
        if (exist == MEA_FALSE)
            return MEA_ERROR;
       
    }

    count = 0;
    for (port = from_port; port <= to_port; port++) {
        exist = MEA_FALSE;
        if (MEA_API_ISOLATE_IsExist(MEA_UNIT_0, port,MEA_TRUE, &exist) == MEA_ERROR) 
        {
            continue;
        }
        if(exist==MEA_FALSE)
            continue;

        if (count++ == 0) {
            CLI_print(" ISOLATE profile  \n"
                      " Id      \n"
                      " ---- --------------------------------------------  \n");


        }

        if (MEA_API_ISOLATE_Get_Entry(MEA_UNIT_0, port, &entry) != MEA_OK) {

            return MEA_OK;
        }

        CLI_print(" %4d ", port);
        CLI_print(" outPort:");
        for (ClusterEx_Id = 0; ClusterEx_Id < MEA_Platform_Get_MaxNumOfClusters(); ClusterEx_Id++)
        {
            val = (((ENET_Uint32*)(&(entry.outQueue.out_ports_0_31)))[ClusterEx_Id / 32] >> (ClusterEx_Id % 32));
            if ( (val & 0x00000001) == 0x00000001) {
                CLI_print("%4d ", ClusterEx_Id);
            }
        }


        CLI_print("\n");

    }
    if(count==0)
        CLI_print(" empty \n");

    return MEA_OK;
}

static MEA_Status MEA_CLI_ISOLATE_Set(int argc, char *argv[]) 
{
    MEA_Uint16 id_io = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Uint32 i,ii;
    MEA_ISOLATE_dbt  entry_pi;

    MEA_Uint32 extra_param_pos;
    MEA_Uint32 num_of_out;
    MEA_Uint32 port;

    if (argc <= 3) {
        return MEA_ERROR;
    }

    MEA_OS_memset(&entry_pi, 0, sizeof(MEA_ISOLATE_dbt));

    for (i = 1; i < (MEA_Uint32) argc; i++) {

      
        if (!MEA_OS_strcmp(argv[i], "-f")) {
            if (i + 1 >(MEA_Uint32)argc) {
                CLI_print(" miss parameter after -f \n");
                return MEA_ERROR;
            }
            id_io = MEA_OS_atoi(argv[i + 1]);
            
        }
        if (!MEA_OS_strcmp(argv[i], "-outport")) {
            if (i + 1 >= (MEA_Uint32)argc) {
                CLI_print("ERROR: Not enough parameters for option %s\n", argv[i]);
                return MEA_ERROR;
            }

            num_of_out = MEA_OS_atoi(argv[i + 1]);
            if (num_of_out > 0) {
                // clear 
                if (i + num_of_out + 2 > (MEA_Uint32)argc) {
                    CLI_print("-outport Invalid number of arguments num_of_out \n");
                    return MEA_ERROR;
                }
                extra_param_pos = i + 2;

                for (ii = 0; ii < num_of_out; ii++) {
                    if(extra_param_pos + ii >(MEA_Uint32)argc)
                        break;

                    port = MEA_OS_atoi(argv[extra_param_pos + ii]);

                    if (ENET_IsValid_Queue(ENET_UNIT_0, port, ENET_TRUE) != ENET_TRUE) {
                        CLI_print("Cluster id %d is not define \n", port);
                        return MEA_ERROR;
                    }

                    /* check if the out_port is valid */
                    ((ENET_Uint32*)(&(entry_pi.outQueue.out_ports_0_31)))[port / 32] |= (1 << (port % 32));

                }
            }//!0
        }//-outport

    }

   


    if (MEA_API_ISOLATE_Set_Entry(MEA_UNIT_0, id_io, &entry_pi) != MEA_OK) {
        CLI_print("Error: MEA_API_ISOLATE_Create_Entry \n");
        return MEA_OK;
    }

    CLI_print("Done : Id= %d\n", id_io);

   
    return MEA_OK;
}

static MEA_Status MEA_CLI_ISOLATE_Create(int argc, char *argv[]) {

    MEA_Uint16 id_io = MEA_PLAT_GENERATE_NEW_ID;
    MEA_Uint32 i;
    MEA_Bool   setParam = MEA_FALSE;
    
    if (argc < 2) {
        return MEA_ERROR;
    }

    for (i = 1; i <(MEA_Uint32)argc; i++) {

        if (!MEA_OS_strcmp(argv[i], "auto")) {
            id_io = MEA_PLAT_GENERATE_NEW_ID;
            setParam = MEA_TRUE;
        }
        if (!MEA_OS_strcmp(argv[i], "-f")) {
            if (i + 1 > (MEA_Uint32)argc) {
                CLI_print(" miss parameter after -f \n");
                return MEA_ERROR;
            }
            id_io = MEA_OS_atoi(argv[i+1]);
            setParam = MEA_TRUE;
        }
    }
    
    if(setParam != MEA_TRUE)
        CLI_print("not set relevant parameter \n");


    if(MEA_API_ISOLATE_Create_Entry(MEA_UNIT_0, &id_io) != MEA_OK){
        CLI_print("Error: MEA_API_ISOLATE_Create_Entry \n");
        return MEA_OK;
    }

    CLI_print("Done : Id= %d\n", id_io);

    return MEA_OK;
}


MEA_Status MEA_CLI_port_AddDebugCmds(void)
{


    CLI_defineCmd("MEA port isolated set create",
        (CmdFunc)MEA_CLI_ISOLATE_Create,
        "set   isolated set \n",
        "set  MEA port Isolated create  \n"
        "---------------------------------------------------\n"
        "    -f <id>                                        \n"
        "    auto                                           \n"
        "---------------------------------------------------\n"
        "Examples: - isolated set create auto \n"
        "          - isolated set create -f 0 \n");

    CLI_defineCmd("MEA port isolated set modify",
        (CmdFunc)MEA_CLI_ISOLATE_Set,
        "set   isolated set modify\n",
        "set  MEA port isolated set modify \n"
        "---------------------------------------------------\n"
        "      \n"
        "    -f <id>\n"
        "    -outport <numofport> <port n> <port n+1>  \n"
        "---------------------------------------------------\n"
        "Examples: - isolated set modify -f 0  -outport 5 100 101 102 104 105\n"
        "          - isolated set modify -f 0  -outport 1 105 \n");


    CLI_defineCmd("MEA port isolated set delete",
        (CmdFunc)MEA_CLI_ISOLATE_Delete,
        "set   isolated delete \n",
        "set  MEA port isolated set delete <all/id> \n"
        "---------------------------------------------------\n"
        "     <all/id>\n"
        "---------------------------------------------------\n"
        "Examples: - isolated set delete 1  \n"
        "          - isolated set delete all  \n");

    CLI_defineCmd("MEA port isolated show",
        (CmdFunc)MEA_CLI_ISOLATE_Show,
        "set   isolated delete \n",
        "set  MEA port isolated set delete <all/id> \n"
        "---------------------------------------------------\n"
        "     <all/id>\n"
        "---------------------------------------------------\n"
        "Examples: - isolated set delete 1  \n"
        "          - isolated set delete all  \n");


    /**********************************************************/
    /*                    port_Set_BRG_Pvid_Set               */
    /**********************************************************/
    CLI_defineCmd("MEA port BRG pvid set",
        (CmdFunc) MEA_CLI_port_Set_BRG_Pvid_Set, 
        "set   set pvid parameter to port \n",
        "set  set the parameter pvid to port  \n"
        "---------------------------------------------------\n"
        "      \n"
        "    -dst_pvid <value>\n"
        "              pvid value 1:4095 0-disable\n"
        "    -src_pvid <value>\n"
        "              pvid value 1:4095 0-disable\n" 
         "    -portType <value>\n"
        "               0 -untag, 1-tag , 2-double tag,3-else \n" 
        "    -encap_swap <value>\n"
       
        "               0 - replace external ethertype only  \n"
        "               1 - do not replace ethertype   \n"
        "               2 - replace internal ethertype only \n"
        "               3 - replace both ethertype \n"
        "    -encap_select  <external_select><internal_select>\n"
        "                   <external_select> 0-LAN ,1 Access \n"
        "                   <internal_select> 0-LAN ,1 Access \n"

        "---------------------------------------------------\n"
        "Examples: - set 105 -dst_pvid 1 -src_pvid 1 -portType 1 \n"
        "          - set 104 -dst_pvid 1 -src_pvid 1 -portType 0 \n"   );

    CLI_defineCmd("MEA port BRG pvid show ",
        (CmdFunc) MEA_CLI_port_BRG_Pvid_Show, 
        "Show   BRG pvid setting(s)\n",
        "show <port/all port:port>  \n"
        "---------------------------------------------------\n"
        "    <port> - all or port \n"
        "              -mode -delay       \n"
        "---------------------------------------------------\n"
        "Examples: - show all   \n"
        "          - show  104 \n");


  

    

    return MEA_OK;
}














