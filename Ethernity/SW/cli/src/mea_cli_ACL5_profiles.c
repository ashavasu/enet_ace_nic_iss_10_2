/*
This file/directory and the information contained in it are
proprietary and confidential to Ethernity Networks Ltd.
No person is allowed to copy, reprint, reproduce or publish
any part of this document, nor disclose its contents to others,
nor make any use of it, nor allow or assist others to make any
use of it - unless by prior written express
authorization of Neralink  Networks Ltd and then only to the
extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/

#include "mea_api.h"
#include "enet_queue_drv.h"
#include "mea_ACL5_profiles_drv.h"
#include "mea_if_Acl5_drv.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_ACL5_profiles.h"
#include "MEA_platform_os_memory.h"




static void Increase_MAC_Address_By_One(MEA_MacAddr *mac_address)
{
	MEA_uint64 counter=0;
	MEA_Uint32 i;


	for (i = 0; i < 6; i++)
		counter = (counter << 8) | mac_address->b[i];

	counter++;

	for (i = 6; i >0; i--)	{
		mac_address->b[i-1] = (MEA_Uint8)counter;
		counter = counter >> 8;
	}

}

static void Increase_IPV6_Address_By_One(MEA_ACL5Id_t *ipv6_address)
{
	MEA_uint64 counter = 0;


	counter = ipv6_address[1];
	counter = (counter << 32) | ipv6_address[0];

	counter++;

	ipv6_address[0] = (MEA_Uint32)counter;
	ipv6_address[1] = (MEA_Uint32)(counter >> 32);

}

/************************************************************************/
/*								ACL5 			                        */
/************************************************************************/

static MEA_Status MEA_CLI_ACL5_Create(int argc, char *argv[])
{
	int s;
	MEA_Uint16 i;
	MEA_Uint16 j;
	MEA_ACL5_Key_dbt key_entry;
	MEA_ACL5_Entry_Data_dbt action_entry;
	MEA_ACL5_Ipmask_dbt ipmask_entry;
	MEA_Uint32 num_of_ports;
	MEA_Port_t port;
	MEA_Uint32 mask_ipv4_dest = 0Xffffffff;
	MEA_Uint32 mask_ipv4_src = 0Xffffffff;
	MEA_Uint32 mask_ipv4 = 0Xff;
	MEA_Uint32 mask_ipv4_reg_num;
	MEA_Uint32 mask_ipv6_dest_reg_num;
	MEA_Uint32 mask_ipv6_src_reg_num;
	MEA_Uint32 mask_ipv6_dest = 0Xffffffff;
	MEA_Uint32 mask_ipv6_src = 0Xffffffff;
	unsigned char buf[sizeof(struct in6_addr)];
	
	MEA_Bool maskIPv6 = MEA_FALSE;
	MEA_Bool maskIPv4 = MEA_FALSE;
	MEA_ACL5Id_t Id_i = MEA_PLAT_GENERATE_NEW_ID;
	

	if (argc < 2) {
		return MEA_ERROR;
	}

	MEA_OS_memset(&key_entry, 0, sizeof(key_entry));
    MEA_OS_memset(&action_entry, 0, sizeof(action_entry));

	

	for (i = 1; i < argc; i++) {
		if (!MEA_OS_strcmp(argv[i], "-f")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			Id_i = MEA_OS_atoiNum(argv[i + 1]);
			if (Id_i == 0)
			{
				CLI_print("Error: Invalid ID \n");
				return MEA_ERROR;
			}
		}

		if (!MEA_OS_strcmp(argv[i], "keyNONE")) {
			key_entry.keyType = MEA_ACL5_KeyType_NONE;
		}
		if (!MEA_OS_strcmp(argv[i], "keyL2")) {
			key_entry.keyType = MEA_ACL5_KeyType_L2;
			}
		if (!MEA_OS_strcmp(argv[i], "keyIPV4")) {
				key_entry.keyType = MEA_ACL5_KeyType_IPV4;
			}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6full")) {
				key_entry.keyType = MEA_ACL5_KeyType_IPV6_FULL;
			}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6dest")) {
				key_entry.keyType = MEA_ACL5_KeyType_IPV6_DST;
			}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6src")) {
			key_entry.keyType = MEA_ACL5_KeyType_IPV6_SRC;
		}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6SigDest")) {
			key_entry.keyType = MEA_ACL5_KeyType_IPV6_SIG_Dest;
		}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6SigSrc")) {
			key_entry.keyType = MEA_ACL5_KeyType_IPV6_SIG_SRC;
		}

		if (!MEA_OS_strcmp(argv[i], "-UMB")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer2.Mac.MAC_UMB_en = MEA_OS_atoiNum(argv[i + 1])) == 1)
				key_entry.layer2.Mac.MAC_UMB = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer2.Mac.MAC_UMB = 0;
		}
		if (!MEA_OS_strcmp(argv[i], "-SA_MAC")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer2.Mac.MacSA_en = MEA_OS_atoiNum(argv[i + 1])) == 1)
				MEA_OS_atohex_MAC(argv[i + 2], &key_entry.layer2.Mac.MacSA);
			else
				MEA_OS_atohex_MAC("00:00:00:00:00:00", &key_entry.layer2.Mac.MacSA);

		}
		if (!MEA_OS_strcmp(argv[i], "-DA_MAC")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer2.Mac.MacDA_en = MEA_OS_atoiNum(argv[i + 1])) == 1)
				MEA_OS_atohex_MAC(argv[i + 2], &key_entry.layer2.Mac.MacDA);
			else
				MEA_OS_atohex_MAC("00:00:00:00:00:00", &key_entry.layer2.Mac.MacDA);
		}
		if (!MEA_OS_strcmp(argv[i], "-Tag1")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer2.Tag1_valid = MEA_OS_atoiNum(argv[i + 1])) == MEA_TRUE)
				key_entry.layer2.Tag1 = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer2.Tag1 = 0;

		}
		if (!MEA_OS_strcmp(argv[i], "-Tag1_p")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer2.Tag1_p_valid = MEA_OS_atoiNum(argv[i + 1])) == MEA_TRUE)
				key_entry.layer2.Tag1_p = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer2.Tag1_p = 0;
		}
		if (!MEA_OS_strcmp(argv[i], "-Tag2")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer2.Tag2_valid = MEA_OS_atoiNum(argv[i + 1])) == MEA_TRUE)
				key_entry.layer2.Tag2 = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer2.Tag2 = 0;

		}
		if (!MEA_OS_strcmp(argv[i], "-Tag3")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer2.Tag3_valid = MEA_OS_atoiNum(argv[i + 1])) == MEA_TRUE)
				key_entry.layer2.Tag3 = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer2.Tag3 = 0;
		}
		
		
		if (!MEA_OS_strcmp(argv[i], "-IPv4_Flags")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer3.IPv4_Flags_valid = MEA_OS_atoiNum(argv[i + 1])) == 1)
				key_entry.layer3.IPv4_Flags = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer3.IPv4_Flags = 0;
		}
		if (!MEA_OS_strcmp(argv[i], "-Etype3")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer3.ethertype3_valid = MEA_OS_atoiNum(argv[i + 1])) == 1)
				key_entry.layer3.ethertype3 = MEA_OS_atohex(argv[i + 2]);
			else
				key_entry.layer3.ethertype3 = 0;
		}
		if (!MEA_OS_strcmp(argv[i], "-ip_protocol")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer3.ip_protocol_valid = MEA_OS_atoiNum(argv[i + 1])) == 1)
				key_entry.layer3.ip_protocol = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer3.ip_protocol = 0;
		}
		if (!MEA_OS_strcmp(argv[i], "-priType")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.layer3.l3_pri_type = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-dscp")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer3.l3_dscp_valid = MEA_OS_atoiNum(argv[i + 1])) == 1)
				key_entry.layer3.l3_dscp_value.ipv4_dscp.dscp = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer3.l3_dscp_value.ipv4_dscp.dscp = 0;
		}
		if (!MEA_OS_strcmp(argv[i], "-dst_ip")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer3.IPv4.dst_ip_valid = MEA_OS_atoiNum(argv[i + 1])) == 1)
				key_entry.layer3.IPv4.dst_ip = MEA_OS_inet_addr(argv[i + 2]);
			else
				key_entry.layer3.IPv4.dst_ip = 0;

		}
		if (!MEA_OS_strcmp(argv[i], "-src_ip")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer3.IPv4.src_ip_valid = MEA_OS_atoiNum(argv[i + 1])) == 1)
				key_entry.layer3.IPv4.src_ip = MEA_OS_inet_addr(argv[i + 2]);
			else
				key_entry.layer3.IPv4.src_ip = 0;

		}
		if (!MEA_OS_strcmp(argv[i], "-IPv6_Flow_Label")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.layer3.IPv6.IPv6_Flow_Label = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-Dst_Ipv6")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ( (key_entry.layer3.IPv6.Dst_Ipv6_valid = MEA_OS_atoiNum(argv[i + 1])) == 1) {
				s = inet_pton(AF_INET6, argv[i+2], buf);
				if (s <= 0) {
					CLI_print("ipv6 format error\n");
					return MEA_ERROR;
				}
				key_entry.layer3.IPv6.Dst_Ipv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
				key_entry.layer3.IPv6.Dst_Ipv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
				key_entry.layer3.IPv6.Dst_Ipv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
				key_entry.layer3.IPv6.Dst_Ipv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

			}
			else
			{
				key_entry.layer3.IPv6.Dst_Ipv6[0] = 0;
				key_entry.layer3.IPv6.Dst_Ipv6[1] = 0;
				key_entry.layer3.IPv6.Dst_Ipv6[2] = 0;
				key_entry.layer3.IPv6.Dst_Ipv6[3] = 0;
			}
			
		}
		if (!MEA_OS_strcmp(argv[i], "-Src_Ipv6")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer3.IPv6.Src_Ipv6_valid = MEA_OS_atoiNum(argv[i + 1])) == 1) {
				s = inet_pton(AF_INET6, argv[i+2], buf);
				if (s <= 0) {
					CLI_print("ipv6 format error\n");
					return MEA_ERROR;
				}
				key_entry.layer3.IPv6.Src_Ipv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
				key_entry.layer3.IPv6.Src_Ipv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
				key_entry.layer3.IPv6.Src_Ipv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
				key_entry.layer3.IPv6.Src_Ipv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

			}
			else
			{
				key_entry.layer3.IPv6.Src_Ipv6[0] = 0;
				key_entry.layer3.IPv6.Src_Ipv6[1] = 0;
				key_entry.layer3.IPv6.Src_Ipv6[2] = 0;
				key_entry.layer3.IPv6.Src_Ipv6[3] = 0;
			}
		}
		if (!MEA_OS_strcmp(argv[i], "-TCP_Flags")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer4.TCP_Flags_valid = MEA_OS_atoiNum(argv[i + 1])) == MEA_TRUE)
				key_entry.layer4.TCP_Flags = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer4.TCP_Flags = 0;
		}
		if (!MEA_OS_strcmp(argv[i], "-dst_port")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}

			if ((key_entry.layer4.dst_port_valid = MEA_OS_atoiNum(argv[i + 1])) == 1)
				key_entry.layer4.dst_port = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer4.dst_port = 0;
		}
		if (!MEA_OS_strcmp(argv[i], "-src_port")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			if ((key_entry.layer4.src_port_valid = MEA_OS_atoiNum(argv[i + 1])) == 1)
				key_entry.layer4.src_port = MEA_OS_atoiNum(argv[i + 2]);
			else
				key_entry.layer4.src_port = 0;
		}
		if (!MEA_OS_strcmp(argv[i], "-group_mask")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.Acl5_mask_prof_id = MEA_OS_atoiNum(argv[i + 1]);
			if (MEA_API_ACL5_Mask_Grouping_Profiles_Get(MEA_UNIT_0, key_entry.Acl5_mask_prof_id, &key_entry.Acl5_mask_prof) != MEA_OK)
				return MEA_ERROR;
			if (MEA_API_ACL5_Key_Mask_Prof_Get(MEA_UNIT_0, key_entry.Acl5_mask_prof.key_mask_id, &key_entry.ACL5_key_mask_prof))
			return MEA_ERROR;
			if (key_entry.Acl5_mask_prof.keyType != key_entry.keyType) {
				CLI_print("Error keyType is different of block profile  %d \n", key_entry.Acl5_mask_prof_id);
				return MEA_ERROR;
			}
			if (key_entry.Acl5_mask_prof.ip_mask_id != 0)
			{
				MEA_API_ACL5_IP_Mask_Prof_Get(MEA_UNIT_0, key_entry.Acl5_mask_prof.ip_mask_id, &ipmask_entry);
				
				if (ipmask_entry.enable_Ipv4)
				{
					mask_ipv4_dest = mask_ipv4_dest << (32 - ipmask_entry.Ipv4_dest_mask);
					mask_ipv4_src = mask_ipv4_src << (32 - ipmask_entry.Ipv4_source_mask);
				}
				if (ipmask_entry.enable_Ipv6)
				{
					mask_ipv6_dest_reg_num = 3 - (ipmask_entry.Ipv6_dest_mask / 32);
					mask_ipv6_dest = mask_ipv6_dest << (32 - (ipmask_entry.Ipv6_dest_mask % 32));
					mask_ipv6_src_reg_num = 3 - (ipmask_entry.Ipv6_source_mask / 32);
					mask_ipv6_src = mask_ipv6_src << (32 - (ipmask_entry.Ipv6_source_mask % 32));
				}
			}
		}
		if (!MEA_OS_strcmp(argv[i], "-rangkProf_index")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.rangkProf_index = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-rangkProf_rule")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.rangkProf_rule = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-SID")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.ACLprof = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-lxcp_win")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			action_entry.lxcp_win = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-fwd_win")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			action_entry.fwd_Act_en = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-action")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			action_entry.Action_Valid = MEA_OS_atoiNum(argv[i + 1]);
			action_entry.Action_Id = MEA_OS_atoiNum(argv[i + 2]);

		}
		if (!MEA_OS_strcmp(argv[i], "-fwd_int_mac")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			action_entry.fwd_int_mac = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-fwd_key")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			action_entry.fwd_key = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-fwd_force")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			action_entry.fwd_force = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-out")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			action_entry.OutPorts_valid = MEA_OS_atoiNum(argv[i + 1]);
			num_of_ports = MEA_OS_atoiNum(argv[i + 2]);
			if (num_of_ports + i > (MEA_Uint32)argc)
				return MEA_ERROR;
			for (j = 0; j < num_of_ports; j++) {
				port = (MEA_Port_t)MEA_OS_atoiNum(argv[i + 3 + j]);

				if (ENET_IsValid_Queue(ENET_UNIT_0, port, ENET_TRUE) != ENET_TRUE) {
					CLI_print("Cluster id %d is not define \n", port);
					return MEA_ERROR;
				}

				/* check if the out_port is valid */
				((ENET_Uint32*)(&(action_entry.OutPorts.out_ports_0_31)))[port / 32] |=
					(1 << (port % 32));
			}

		}
	}

	if (key_entry.layer3.IPv4.dst_ip_valid == MEA_TRUE)
	{
		mask_ipv4_reg_num=((32 - ipmask_entry.Ipv4_dest_mask) /8);
		while (mask_ipv4_reg_num)
		{
			if (!(key_entry.layer3.IPv4.dst_ip & mask_ipv4)) maskIPv4 = MEA_TRUE;
			mask_ipv4 = mask_ipv4 << 2;
			mask_ipv4_reg_num--;
		}
		key_entry.layer3.IPv4.dst_ip &= mask_ipv4_dest;
	}
	if (maskIPv4)
		CLI_print("Warning: dest ipv4 change because mask ipv4 profile id= %d\n", key_entry.Acl5_mask_prof.ip_mask_id);
	mask_ipv4 = 0xff;
	maskIPv4 = MEA_FALSE;
	if (key_entry.layer3.IPv4.src_ip_valid == MEA_TRUE)
	{
		mask_ipv4_reg_num = ((32 - ipmask_entry.Ipv4_source_mask) / 8);
		while (mask_ipv4_reg_num)
		{
			if (!(key_entry.layer3.IPv4.dst_ip & mask_ipv4)) maskIPv4 = MEA_TRUE;
			mask_ipv4 = mask_ipv4 << 2;
			mask_ipv4_reg_num--;
		}
		key_entry.layer3.IPv4.src_ip &= mask_ipv4_src;
	}

	if (maskIPv4)
		CLI_print("Warning: src ipv4 change because mask ipv4 profile id= %d\n", key_entry.Acl5_mask_prof.ip_mask_id);



	if (key_entry.layer3.IPv6.Dst_Ipv6_valid == MEA_TRUE && ipmask_entry.Ipv6_dest_mask != 128)
	{
		if (key_entry.layer3.IPv6.Dst_Ipv6[mask_ipv6_dest_reg_num] != 0) maskIPv6 = MEA_TRUE;
		key_entry.layer3.IPv6.Dst_Ipv6[mask_ipv6_dest_reg_num] &= mask_ipv6_dest;
		while (mask_ipv6_dest_reg_num)
		{
			if (key_entry.layer3.IPv6.Dst_Ipv6[mask_ipv6_dest_reg_num - 1] != 0) maskIPv6 = MEA_TRUE;
			key_entry.layer3.IPv6.Dst_Ipv6[mask_ipv6_dest_reg_num-1] = 0;
			mask_ipv6_dest_reg_num--;
		}
	}
	if (maskIPv6)
		CLI_print("Warning: dest ipv6 change because mask ipv6 profile id= %d\n", key_entry.Acl5_mask_prof.ip_mask_id);
	
	maskIPv6 = MEA_FALSE;
	if (key_entry.layer3.IPv6.Src_Ipv6_valid == MEA_TRUE && ipmask_entry.Ipv6_source_mask != 128)
	{
		if (key_entry.layer3.IPv6.Src_Ipv6[mask_ipv6_dest_reg_num] != 0) maskIPv6 = MEA_TRUE;
		key_entry.layer3.IPv6.Src_Ipv6[mask_ipv6_src_reg_num] &= mask_ipv6_src;
		while (mask_ipv6_src_reg_num)
		{
			if (key_entry.layer3.IPv6.Src_Ipv6[mask_ipv6_dest_reg_num - 1] != 0) maskIPv6 = MEA_TRUE;
			key_entry.layer3.IPv6.Dst_Ipv6[mask_ipv6_src_reg_num-1] = 0;
			mask_ipv6_src_reg_num--;
		}
	}
	if (maskIPv6)
		CLI_print("Warning: src ipv6 change because mask ipv6 profile id= %d\n", key_entry.Acl5_mask_prof.ip_mask_id);


	if (MEA_API_ACL5_Create_Entry(MEA_UNIT_0, &key_entry,&action_entry, &Id_i) != MEA_OK) {
		CLI_print("Error MEA_API_ACL5_Create_Entry failed \n");
		return MEA_OK;
	}
	CLI_print("Done %d \n", Id_i);


	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Get(int argc, char *argv[])
{
	MEA_ACL5Id_t Id;
	MEA_ACL5Id_t from_Id;
	MEA_ACL5Id_t to_Id;
	MEA_Bool exist;
	MEA_ACL5_Key_dbt entry_key_dbt;
	MEA_ACL5_Entry_Data_dbt entry_data;
	MEA_Port_t port;
	MEA_Bool hash = MEA_FALSE;
	char Ip_str[20];
	unsigned char buf[sizeof(struct in6_addr)];
	char stripv6[INET6_ADDRSTRLEN];

	if (argc < 2) {
		return MEA_ERROR;
	}

	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_MAX_ENTRY_SW - 1;
	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (MEA_API_ACL5_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("ERROR: MEA_API_ACL5_isExist Id=%d\n", Id);
		}
		if (!exist) {
			CLI_print("ACL5 entry %d is not exist \n", Id);
			return MEA_OK;
		}
	}

	if (argc >= 3) {
		if (MEA_OS_strcmp(argv[2], "hash") == 0) {
			hash = MEA_TRUE;
		
		}
	
	}



	CLI_print("-----------------------------------------------------------------------\n");
	CLI_print("            ACL5_Entries                                               \n");
	CLI_print("-----------------------------------------------------------------------\n");

	for (Id = from_Id; Id <= to_Id; Id++)
	{
		if (MEA_API_ACL5_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;
		if (!exist)
			continue;

		if (MEA_API_ACL5_Get_Entry(MEA_UNIT_0, Id, &entry_key_dbt, &entry_data) != MEA_OK) {
			return MEA_ERROR;
		}


		if (hash == MEA_FALSE) {
			switch (entry_key_dbt.keyType)
			{
			case MEA_ACL5_KeyType_NONE:
				CLI_print("MEA_ACL5_KeyType is equal to 0 \n");
				break;
			case MEA_ACL5_KeyType_L2:
			{
				CLI_print("id: %3d \n", Id);
				CLI_print("\t keyType: %31s \n", "L2");
				CLI_print("\t UMB: %24s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MAC_UMB_en), entry_key_dbt.layer2.Mac.MAC_UMB);
				CLI_print("\t SID: %35d \n", entry_key_dbt.ACLprof);
				CLI_print("\t key mask profile: %22d \n", entry_key_dbt.Acl5_mask_prof.key_mask_id);
				CLI_print("\t inner frame: %16s \n", MEA_STATUS_STR(entry_key_dbt.Acl5_mask_prof.Inner_Frame));
				CLI_print("\t Mac_DA: %21s  %02x:%02x:%02x:%02x:%02x:%02x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MacDA_en),
					entry_key_dbt.layer2.Mac.MacDA.b[0],
					entry_key_dbt.layer2.Mac.MacDA.b[1],
					entry_key_dbt.layer2.Mac.MacDA.b[2],
					entry_key_dbt.layer2.Mac.MacDA.b[3],
					entry_key_dbt.layer2.Mac.MacDA.b[4],
					entry_key_dbt.layer2.Mac.MacDA.b[5]);
				CLI_print("\t Mac_SA: %21s  %02x:%02x:%02x:%02x:%02x:%02x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MacSA_en),
					entry_key_dbt.layer2.Mac.MacSA.b[0],
					entry_key_dbt.layer2.Mac.MacSA.b[1],
					entry_key_dbt.layer2.Mac.MacSA.b[2],
					entry_key_dbt.layer2.Mac.MacSA.b[3],
					entry_key_dbt.layer2.Mac.MacSA.b[4],
					entry_key_dbt.layer2.Mac.MacSA.b[5]);
				CLI_print("\t Tag1: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_valid), entry_key_dbt.layer2.Tag1);
				CLI_print("\t Tag2: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag2_valid), entry_key_dbt.layer2.Tag2);
				CLI_print("\t Tag3: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag3_valid), entry_key_dbt.layer2.Tag3);
				CLI_print("\t Tag1_p: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_p_valid), entry_key_dbt.layer2.Tag1_p);
				CLI_print("\t action: %21s %10d \n", MEA_STATUS_STR(entry_data.Action_Valid), entry_data.Action_Id);
				CLI_print("\t fwd Act en:%18s  \n", MEA_STATUS_STR(entry_data.fwd_Act_en));
				CLI_print("\t fwd force:%19s  \n", MEA_STATUS_STR(entry_data.fwd_force));
				CLI_print("\t fwd internal mac:%12s  \n", MEA_STATUS_STR(entry_data.fwd_int_mac));
				CLI_print("\t fwd key:%32d  \n", entry_data.fwd_key);
				CLI_print("\t lxcp_win:%20s  \n", MEA_STATUS_STR(entry_data.lxcp_win));
				CLI_print("\t out:    \t\t   ");
				for (port = 0; port < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++) {
					if (((ENET_Uint32*)(&(entry_data.OutPorts.out_ports_0_31)))[port / 32] & (1 << (port % 32))) {
						CLI_print("%3d ", port);
					}
				}

				CLI_print("\n-----------------------------------------------------------------------\n");
			}
				break;
			case MEA_ACL5_KeyType_IPV4:
			{
				CLI_print("id: %3d \n", Id);
				CLI_print("\t keyType: %31s \n", "IPV4");
				CLI_print("\t L3type: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer3.ethertype3_valid), entry_key_dbt.layer3.ethertype3);
				CLI_print("\t ip protocol: %16s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.ip_protocol_valid), entry_key_dbt.layer3.ip_protocol);
				CLI_print("\t L4dest: %21s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.dst_port_valid), entry_key_dbt.layer4.dst_port);
				CLI_print("\t L4src: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.src_port_valid), entry_key_dbt.layer4.src_port);
				CLI_print("\t TCP flags: %18s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.TCP_Flags_valid), entry_key_dbt.layer4.TCP_Flags);
				CLI_print("\t DSCP: %23s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.l3_dscp_valid), entry_key_dbt.layer3.l3_dscp_value);
				CLI_print("\t priType: %31d \n", entry_key_dbt.layer3.l3_pri_type);
				CLI_print("\t Range mask profile id: %17d \n", entry_key_dbt.Acl5_mask_prof.range_mask_id);
				CLI_print("\t RngRslt: %31d \n", entry_key_dbt.rangkProf_rule);
				CLI_print("\t Ip mask profile id: %20d \n", entry_key_dbt.Acl5_mask_prof.ip_mask_id);
				CLI_print("\t UMB: %24s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MAC_UMB_en), entry_key_dbt.layer2.Mac.MAC_UMB);
				CLI_print("\t SID: %35d \n", entry_key_dbt.ACLprof);
				CLI_print("\t key mask profile: %22d \n", entry_key_dbt.Acl5_mask_prof.key_mask_id);
				CLI_print("\t inner frame: %16s \n", MEA_STATUS_STR(entry_key_dbt.Acl5_mask_prof.Inner_Frame));
				CLI_print("\t Mac_DA: %21s  %02x:%02x:%02x:%02x:%02x:%02x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MacDA_en),
					entry_key_dbt.layer2.Mac.MacDA.b[0],
					entry_key_dbt.layer2.Mac.MacDA.b[1],
					entry_key_dbt.layer2.Mac.MacDA.b[2],
					entry_key_dbt.layer2.Mac.MacDA.b[3],
					entry_key_dbt.layer2.Mac.MacDA.b[4],
					entry_key_dbt.layer2.Mac.MacDA.b[5]);
				CLI_print("\t Mac_SA: %21s  %02x:%02x:%02x:%02x:%02x:%02x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MacSA_en),
					entry_key_dbt.layer2.Mac.MacSA.b[0],
					entry_key_dbt.layer2.Mac.MacSA.b[1],
					entry_key_dbt.layer2.Mac.MacSA.b[2],
					entry_key_dbt.layer2.Mac.MacSA.b[3],
					entry_key_dbt.layer2.Mac.MacSA.b[4],
					entry_key_dbt.layer2.Mac.MacSA.b[5]);
				CLI_print("\t Tag1: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_valid), entry_key_dbt.layer2.Tag1);
				CLI_print("\t Tag2: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag2_valid), entry_key_dbt.layer2.Tag2);
				CLI_print("\t Tag3: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag3_valid), entry_key_dbt.layer2.Tag3);
				CLI_print("\t Tag1_p: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_p_valid), entry_key_dbt.layer2.Tag1_p);
				MEA_OS_inet_ntoa(MEA_OS_htonl(entry_key_dbt.layer3.IPv4.src_ip), Ip_str, sizeof(Ip_str));
				CLI_print("\t %s %20s %13s \n", "IPv4 src", MEA_STATUS_STR(entry_key_dbt.layer3.IPv4.src_ip_valid), Ip_str);
				MEA_OS_inet_ntoa(MEA_OS_htonl(entry_key_dbt.layer3.IPv4.dst_ip), Ip_str, sizeof(Ip_str));
				CLI_print("\t %s %20s %13s \n", "IPv4 dst", MEA_STATUS_STR(entry_key_dbt.layer3.IPv4.dst_ip_valid), Ip_str);
				CLI_print("\t action: %21s %10d \n", MEA_STATUS_STR(entry_data.Action_Valid), entry_data.Action_Id);
				CLI_print("\t fwd Act en:%18s  \n", MEA_STATUS_STR(entry_data.fwd_Act_en));
				CLI_print("\t fwd force:%19s  \n", MEA_STATUS_STR(entry_data.fwd_force));
				CLI_print("\t fwd internal mac:%12s  \n", MEA_STATUS_STR(entry_data.fwd_int_mac));
				CLI_print("\t fwd key:%32d  \n", entry_data.fwd_key);
				CLI_print("\t lxcp_win:%20s  \n", MEA_STATUS_STR(entry_data.lxcp_win));
				CLI_print("\t out:    \t\t   ");
				for (port = 0; port < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++) {
					if (((ENET_Uint32*)(&(entry_data.OutPorts.out_ports_0_31)))[port / 32] & (1 << (port % 32))) {
						CLI_print("%3d ", port);
					}
				}

				CLI_print("\n-----------------------------------------------------------------------\n");
			}
				break;
			case MEA_ACL5_KeyType_IPV6_FULL:
			{
				CLI_print("id: %3d \n", Id);
				CLI_print("\t keyType: %31s \n", "IPV6 FULL");
				CLI_print("\t L3type: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer3.ethertype3_valid), entry_key_dbt.layer3.ethertype3);
				CLI_print("\t ip protocol: %16s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.ip_protocol_valid), entry_key_dbt.layer3.ip_protocol);
				CLI_print("\t L4dest: %21s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.dst_port_valid), entry_key_dbt.layer4.dst_port);
				CLI_print("\t L4src: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.src_port_valid), entry_key_dbt.layer4.src_port);
				CLI_print("\t TCP flags: %18s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.TCP_Flags_valid), entry_key_dbt.layer4.TCP_Flags);
				CLI_print("\t DSCP: %23s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.l3_dscp_valid), entry_key_dbt.layer3.l3_dscp_value);
				CLI_print("\t priType: %31d \n", entry_key_dbt.layer3.l3_pri_type);
				CLI_print("\t Range mask profile id: %17d \n", entry_key_dbt.Acl5_mask_prof.range_mask_id);
				CLI_print("\t RngRslt: %31d \n", entry_key_dbt.rangkProf_rule);
				CLI_print("\t Ip mask profile id: %20d \n", entry_key_dbt.Acl5_mask_prof.ip_mask_id);
				CLI_print("\t UMB: %24s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MAC_UMB_en), entry_key_dbt.layer2.Mac.MAC_UMB);
				CLI_print("\t SID: %35d \n", entry_key_dbt.ACLprof);
				CLI_print("\t key mask profile: %22d \n", entry_key_dbt.Acl5_mask_prof.key_mask_id);
				CLI_print("\t inner frame: %16s \n", MEA_STATUS_STR(entry_key_dbt.Acl5_mask_prof.Inner_Frame));
				CLI_print("\t dest ipv6: %18s ", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.Dst_Ipv6_valid));

				buf[0] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 24) & 0xff;
				buf[1] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 16) & 0xff;
				buf[2] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 8) & 0xff;
				buf[3] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 0) & 0xff;

				buf[4] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 24) & 0xff;
				buf[5] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 16) & 0xff;
				buf[6] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 8) & 0xff;
				buf[7] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 0) & 0xff;

				buf[8] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 24) & 0xff;
				buf[9] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 16) & 0xff;
				buf[10] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 8) & 0xff;
				buf[11] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 0) & 0xff;

				buf[12] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 24) & 0xff;
				buf[13] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 16) & 0xff;
				buf[14] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 8) & 0xff;
				buf[15] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 0) & 0xff;

				if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
					CLI_print(">>>inet_ntop<<< \n");
				}
				CLI_print("\t %s \n", stripv6);

				CLI_print("\t source ipv6: %16s ", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.Src_Ipv6_valid));

				buf[0] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 24) & 0xff;
				buf[1] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 16) & 0xff;
				buf[2] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 8) & 0xff;
				buf[3] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 0) & 0xff;

				buf[4] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 24) & 0xff;
				buf[5] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 16) & 0xff;
				buf[6] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 8) & 0xff;
				buf[7] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 0) & 0xff;

				buf[8] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 24) & 0xff;
				buf[9] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 16) & 0xff;
				buf[10] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 8) & 0xff;
				buf[11] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 0) & 0xff;

				buf[12] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 24) & 0xff;
				buf[13] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 16) & 0xff;
				buf[14] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 8) & 0xff;
				buf[15] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 0) & 0xff;

				if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
					CLI_print(">>>inet_ntop<<< \n");
				}
				CLI_print("\t %s \n", stripv6);
				CLI_print("\t action: %21s %10d \n", MEA_STATUS_STR(entry_data.Action_Valid), entry_data.Action_Id);
				CLI_print("\t fwd Act en:%18s  \n", MEA_STATUS_STR(entry_data.fwd_Act_en));
				CLI_print("\t fwd force:%19s  \n", MEA_STATUS_STR(entry_data.fwd_force));
				CLI_print("\t fwd internal mac:%12s  \n", MEA_STATUS_STR(entry_data.fwd_int_mac));
				CLI_print("\t fwd key:%32d  \n", entry_data.fwd_key);
				CLI_print("\t lxcp_win:%20s  \n", MEA_STATUS_STR(entry_data.lxcp_win));
				CLI_print("\t out:    \t\t   ");
				for (port = 0; port < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++) {
					if (((ENET_Uint32*)(&(entry_data.OutPorts.out_ports_0_31)))[port / 32] & (1 << (port % 32))) {
						CLI_print("%3d ", port);
					}
				}

				CLI_print("\n-----------------------------------------------------------------------\n");
			}
				break;
			case MEA_ACL5_KeyType_IPV6_DST:
			{
				CLI_print("id: %3d \n", Id);
				CLI_print("\t keyType: %31s \n", "IPV6dest");
				CLI_print("\t L3type: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer3.ethertype3_valid), entry_key_dbt.layer3.ethertype3);
				CLI_print("\t ip protocol: %16s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.ip_protocol_valid), entry_key_dbt.layer3.ip_protocol);
				CLI_print("\t L4dest: %21s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.dst_port_valid), entry_key_dbt.layer4.dst_port);
				CLI_print("\t L4src: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.src_port_valid), entry_key_dbt.layer4.src_port);
				CLI_print("\t TCP flags: %18s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.TCP_Flags_valid), entry_key_dbt.layer4.TCP_Flags);
				CLI_print("\t DSCP: %23s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.l3_dscp_valid), entry_key_dbt.layer3.l3_dscp_value);
				CLI_print("\t priType: %31d \n", entry_key_dbt.layer3.l3_pri_type);
				CLI_print("\t Ip mask profile id: %20d \n", entry_key_dbt.Acl5_mask_prof.ip_mask_id);
				CLI_print("\t Range mask profile id: %17d \n", entry_key_dbt.Acl5_mask_prof.range_mask_id);
				CLI_print("\t label: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.IPv6_Flow_Label_valid), entry_key_dbt.layer3.IPv6.IPv6_Flow_Label);
				CLI_print("\t UMB: %24s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MAC_UMB_en), entry_key_dbt.layer2.Mac.MAC_UMB);
				CLI_print("\t SID: %35d \n", entry_key_dbt.ACLprof);
				CLI_print("\t key mask profile: %22d \n", entry_key_dbt.Acl5_mask_prof.key_mask_id);
				CLI_print("\t inner frame: %16s \n", MEA_STATUS_STR(entry_key_dbt.Acl5_mask_prof.Inner_Frame));
				CLI_print("\t dest ipv6: %18s ", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.Dst_Ipv6_valid));

				buf[0] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 24) & 0xff;
				buf[1] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 16) & 0xff;
				buf[2] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 8) & 0xff;
				buf[3] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 0) & 0xff;

				buf[4] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 24) & 0xff;
				buf[5] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 16) & 0xff;
				buf[6] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 8) & 0xff;
				buf[7] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 0) & 0xff;

				buf[8] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 24) & 0xff;
				buf[9] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 16) & 0xff;
				buf[10] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 8) & 0xff;
				buf[11] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 0) & 0xff;

				buf[12] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 24) & 0xff;
				buf[13] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 16) & 0xff;
				buf[14] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 8) & 0xff;
				buf[15] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 0) & 0xff;

				if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
					CLI_print(">>>inet_ntop<<< \n");
				}
				CLI_print("\t %s \n", stripv6);
				CLI_print("\t Mac_DA: %21s  %02x:%02x:%02x:%02x:%02x:%02x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MacDA_en),
					entry_key_dbt.layer2.Mac.MacDA.b[0],
					entry_key_dbt.layer2.Mac.MacDA.b[1],
					entry_key_dbt.layer2.Mac.MacDA.b[2],
					entry_key_dbt.layer2.Mac.MacDA.b[3],
					entry_key_dbt.layer2.Mac.MacDA.b[4],
					entry_key_dbt.layer2.Mac.MacDA.b[5]);
				CLI_print("\t Range mask profile id: %17d \n", entry_key_dbt.Acl5_mask_prof.range_mask_id);
				CLI_print("\t RngRslt: %31d \n", entry_key_dbt.rangkProf_rule);
				CLI_print("\t Tag1: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_valid), entry_key_dbt.layer2.Tag1);
				CLI_print("\t Tag3: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag3_valid), entry_key_dbt.layer2.Tag3);
				CLI_print("\t Tag1_p: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_p_valid), entry_key_dbt.layer2.Tag1_p);
				CLI_print("\t action: %21s %10d \n", MEA_STATUS_STR(entry_data.Action_Valid), entry_data.Action_Id);
				CLI_print("\t fwd Act en:%18s  \n", MEA_STATUS_STR(entry_data.fwd_Act_en));
				CLI_print("\t fwd force:%19s  \n", MEA_STATUS_STR(entry_data.fwd_force));
				CLI_print("\t fwd internal mac:%12s  \n", MEA_STATUS_STR(entry_data.fwd_int_mac));
				CLI_print("\t fwd key:%32d  \n", entry_data.fwd_key);
				CLI_print("\t lxcp_win:%20s  \n", MEA_STATUS_STR(entry_data.lxcp_win));
				CLI_print("\t out:    \t\t   ");
				for (port = 0; port < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++) {
					if (((ENET_Uint32*)(&(entry_data.OutPorts.out_ports_0_31)))[port / 32] & (1 << (port % 32))) {
						CLI_print("%3d ", port);
					}
				}

				CLI_print("\n-----------------------------------------------------------------------\n");
			}
				break;
			case MEA_ACL5_KeyType_IPV6_SRC:
			{
				CLI_print("id: %3d \n", Id);
				CLI_print("\t keyType: %31s \n", "IPV6Src");
				CLI_print("\t L3type: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer3.ethertype3_valid), entry_key_dbt.layer3.ethertype3);
				CLI_print("\t ip protocol: %16s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.ip_protocol_valid), entry_key_dbt.layer3.ip_protocol);
				CLI_print("\t L4dest: %21s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.dst_port_valid), entry_key_dbt.layer4.dst_port);
				CLI_print("\t L4src: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.src_port_valid), entry_key_dbt.layer4.src_port);
				CLI_print("\t TCP flags: %18s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.TCP_Flags_valid), entry_key_dbt.layer4.TCP_Flags);
				CLI_print("\t DSCP: %23s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.l3_dscp_valid), entry_key_dbt.layer3.l3_dscp_value);
				CLI_print("\t priType: %31d \n", entry_key_dbt.layer3.l3_pri_type);
				CLI_print("\t Ip mask profile id: %20d \n", entry_key_dbt.Acl5_mask_prof.ip_mask_id);
				CLI_print("\t Range mask profile id: %17d \n", entry_key_dbt.Acl5_mask_prof.range_mask_id);
				CLI_print("\t label: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.IPv6_Flow_Label_valid), entry_key_dbt.layer3.IPv6.IPv6_Flow_Label);
				CLI_print("\t UMB: %24s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MAC_UMB_en), entry_key_dbt.layer2.Mac.MAC_UMB);
				CLI_print("\t SID: %35d \n", entry_key_dbt.ACLprof);
				CLI_print("\t key mask profile: %22d \n", entry_key_dbt.Acl5_mask_prof.key_mask_id);
				CLI_print("\t inner frame: %16s \n", MEA_STATUS_STR(entry_key_dbt.Acl5_mask_prof.Inner_Frame));
				CLI_print("\t source ipv6: %16s ", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.Src_Ipv6_valid));

				buf[0] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 24) & 0xff;
				buf[1] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 16) & 0xff;
				buf[2] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 8) & 0xff;
				buf[3] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 0) & 0xff;

				buf[4] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 24) & 0xff;
				buf[5] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 16) & 0xff;
				buf[6] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 8) & 0xff;
				buf[7] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 0) & 0xff;

				buf[8] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 24) & 0xff;
				buf[9] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 16) & 0xff;
				buf[10] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 8) & 0xff;
				buf[11] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 0) & 0xff;

				buf[12] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 24) & 0xff;
				buf[13] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 16) & 0xff;
				buf[14] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 8) & 0xff;
				buf[15] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 0) & 0xff;

				if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
					CLI_print(">>>inet_ntop<<< \n");
				}
				CLI_print("\t %s \n", stripv6);
				CLI_print("\t Mac_SA: %21s  %02x:%02x:%02x:%02x:%02x:%02x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MacSA_en),
					entry_key_dbt.layer2.Mac.MacSA.b[0],
					entry_key_dbt.layer2.Mac.MacSA.b[1],
					entry_key_dbt.layer2.Mac.MacSA.b[2],
					entry_key_dbt.layer2.Mac.MacSA.b[3],
					entry_key_dbt.layer2.Mac.MacSA.b[4],
					entry_key_dbt.layer2.Mac.MacSA.b[5]);
				CLI_print("\t Range mask profile id: %17d \n", entry_key_dbt.Acl5_mask_prof.range_mask_id);
				CLI_print("\t RngRslt: %31d \n", entry_key_dbt.rangkProf_rule);
				CLI_print("\t Tag1: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_valid), entry_key_dbt.layer2.Tag1);
				CLI_print("\t Tag3: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag3_valid), entry_key_dbt.layer2.Tag3);
				CLI_print("\t Tag1_p: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_p_valid), entry_key_dbt.layer2.Tag1_p);
				CLI_print("\t action: %21s %10d \n", MEA_STATUS_STR(entry_data.Action_Valid), entry_data.Action_Id);
				CLI_print("\t fwd Act en:%18s  \n", MEA_STATUS_STR(entry_data.fwd_Act_en));
				CLI_print("\t fwd force:%19s  \n", MEA_STATUS_STR(entry_data.fwd_force));
				CLI_print("\t fwd internal mac:%12s  \n", MEA_STATUS_STR(entry_data.fwd_int_mac));
				CLI_print("\t fwd key:%32d  \n", entry_data.fwd_key);
				CLI_print("\t lxcp_win:%20s  \n", MEA_STATUS_STR(entry_data.lxcp_win));
				CLI_print("\t out:    \t\t   ");
				for (port = 0; port < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++) {
					if (((ENET_Uint32*)(&(entry_data.OutPorts.out_ports_0_31)))[port / 32] & (1 << (port % 32))) {
						CLI_print("%3d ", port);
					}
				}

				CLI_print("\n-----------------------------------------------------------------------\n");
			}
				break;
			case MEA_ACL5_KeyType_IPV6_SIG_Dest:
			{
				CLI_print("id: %3d \n", Id);
				CLI_print("\t keyType: %31s \n", "IPV6SigDest");
				CLI_print("\t L3type: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer3.ethertype3_valid), entry_key_dbt.layer3.ethertype3);
				CLI_print("\t ip protocol: %16s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.ip_protocol_valid), entry_key_dbt.layer3.ip_protocol);
				CLI_print("\t L4dest: %21s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.dst_port_valid), entry_key_dbt.layer4.dst_port);
				CLI_print("\t L4src: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.src_port_valid), entry_key_dbt.layer4.src_port);
				CLI_print("\t TCP flags: %18s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.TCP_Flags_valid), entry_key_dbt.layer4.TCP_Flags);
				CLI_print("\t DSCP: %23s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.l3_dscp_valid), entry_key_dbt.layer3.l3_dscp_value);
				CLI_print("\t priType: %31d \n", entry_key_dbt.layer3.l3_pri_type);
				CLI_print("\t Ip mask profile id: %20d \n", entry_key_dbt.Acl5_mask_prof.ip_mask_id);
				CLI_print("\t Range mask profile id: %17d \n", entry_key_dbt.Acl5_mask_prof.range_mask_id);
				CLI_print("\t label: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.IPv6_Flow_Label_valid), entry_key_dbt.layer3.IPv6.IPv6_Flow_Label);
				CLI_print("\t UMB: %24s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MAC_UMB_en), entry_key_dbt.layer2.Mac.MAC_UMB);
				CLI_print("\t SID: %35d \n", entry_key_dbt.ACLprof);
				CLI_print("\t key mask profile: %22d \n", entry_key_dbt.Acl5_mask_prof.key_mask_id);
				CLI_print("\t inner frame: %16s \n", MEA_STATUS_STR(entry_key_dbt.Acl5_mask_prof.Inner_Frame));
				CLI_print("\t Mac_DA: %21s  %02x:%02x:%02x:%02x:%02x:%02x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MacDA_en),
					entry_key_dbt.layer2.Mac.MacDA.b[0],
					entry_key_dbt.layer2.Mac.MacDA.b[1],
					entry_key_dbt.layer2.Mac.MacDA.b[2],
					entry_key_dbt.layer2.Mac.MacDA.b[3],
					entry_key_dbt.layer2.Mac.MacDA.b[4],
					entry_key_dbt.layer2.Mac.MacDA.b[5]);
				CLI_print("\t Tag1: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_valid), entry_key_dbt.layer2.Tag1);
				CLI_print("\t Tag2: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag2_valid), entry_key_dbt.layer2.Tag2);
				CLI_print("\t Tag3: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag3_valid), entry_key_dbt.layer2.Tag3);
				CLI_print("\t Tag1_p: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_p_valid), entry_key_dbt.layer2.Tag1_p); 
				CLI_print("\t dest ipv6: %18s ", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.Dst_Ipv6_valid));

				buf[0] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 24) & 0xff;
				buf[1] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 16) & 0xff;
				buf[2] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 8) & 0xff;
				buf[3] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 0) & 0xff;

				buf[4] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 24) & 0xff;
				buf[5] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 16) & 0xff;
				buf[6] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 8) & 0xff;
				buf[7] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 0) & 0xff;

				buf[8] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 24) & 0xff;
				buf[9] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 16) & 0xff;
				buf[10] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 8) & 0xff;
				buf[11] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 0) & 0xff;

				buf[12] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 24) & 0xff;
				buf[13] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 16) & 0xff;
				buf[14] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 8) & 0xff;
				buf[15] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 0) & 0xff;

				if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
					CLI_print(">>>inet_ntop<<< \n");
				}
				CLI_print("\t %s \n", stripv6);

				CLI_print("\t source ipv6: %16s ", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.Src_Ipv6_valid));

				buf[0] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 24) & 0xff;
				buf[1] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 16) & 0xff;
				buf[2] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 8) & 0xff;
				buf[3] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 0) & 0xff;

				buf[4] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 24) & 0xff;
				buf[5] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 16) & 0xff;
				buf[6] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 8) & 0xff;
				buf[7] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 0) & 0xff;

				buf[8] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 24) & 0xff;
				buf[9] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 16) & 0xff;
				buf[10] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 8) & 0xff;
				buf[11] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 0) & 0xff;

				buf[12] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 24) & 0xff;
				buf[13] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 16) & 0xff;
				buf[14] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 8) & 0xff;
				buf[15] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 0) & 0xff;

				if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
					CLI_print(">>>inet_ntop<<< \n");
				}
				CLI_print("\t %s \n", stripv6);
				CLI_print("\t %s %17s \t   %08x \n", "IPv6 SigDst", MEA_STATUS_STR(entry_key_dbt.layer3.IPv4.dst_ip_valid), entry_key_dbt.layer3.IPv4.dst_ip);
				CLI_print("\t %s %17s \t   %08x \n", "IPv6 SigSrc", MEA_STATUS_STR(entry_key_dbt.layer3.IPv4.src_ip_valid), entry_key_dbt.layer3.IPv4.src_ip);
				CLI_print("\t action: %21s %10d \n", MEA_STATUS_STR(entry_data.Action_Valid), entry_data.Action_Id);
				CLI_print("\t fwd Act en:%18s  \n", MEA_STATUS_STR(entry_data.fwd_Act_en));
				CLI_print("\t fwd force:%19s  \n", MEA_STATUS_STR(entry_data.fwd_force));
				CLI_print("\t fwd internal mac:%12s  \n", MEA_STATUS_STR(entry_data.fwd_int_mac));
				CLI_print("\t fwd key:%32d  \n", entry_data.fwd_key);
				CLI_print("\t lxcp_win:%20s  \n", MEA_STATUS_STR(entry_data.lxcp_win));
				CLI_print("\t out:    \t\t   ");
				for (port = 0; port < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++) {
					if (((ENET_Uint32*)(&(entry_data.OutPorts.out_ports_0_31)))[port / 32] & (1 << (port % 32))) {
						CLI_print("%3d ", port);
					}
				}

				CLI_print("\n-----------------------------------------------------------------------\n");

			}
				break;
			case MEA_ACL5_KeyType_IPV6_SIG_SRC:
			{
				CLI_print("id: %3d \n", Id);
				CLI_print("\t keyType: %31s \n", "IPV6SigDest");
				CLI_print("\t L3type: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer3.ethertype3_valid), entry_key_dbt.layer3.ethertype3);
				CLI_print("\t ip protocol: %16s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.ip_protocol_valid), entry_key_dbt.layer3.ip_protocol);
				CLI_print("\t L4dest: %21s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.dst_port_valid), entry_key_dbt.layer4.dst_port);
				CLI_print("\t L4src: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.src_port_valid), entry_key_dbt.layer4.src_port);
				CLI_print("\t TCP flags: %18s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer4.TCP_Flags_valid), entry_key_dbt.layer4.TCP_Flags);
				CLI_print("\t DSCP: %23s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.l3_dscp_valid), entry_key_dbt.layer3.l3_dscp_value);
				CLI_print("\t priType: %31d \n", entry_key_dbt.layer3.l3_pri_type);
				CLI_print("\t Ip mask profile id: %20d \n", entry_key_dbt.Acl5_mask_prof.ip_mask_id);
				CLI_print("\t Range mask profile id: %17d \n", entry_key_dbt.Acl5_mask_prof.range_mask_id);
				CLI_print("\t label: %22s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.IPv6_Flow_Label_valid), entry_key_dbt.layer3.IPv6.IPv6_Flow_Label);
				CLI_print("\t UMB: %24s %10d \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MAC_UMB_en), entry_key_dbt.layer2.Mac.MAC_UMB);
				CLI_print("\t SID: %35d \n", entry_key_dbt.ACLprof);
				CLI_print("\t key mask profile: %22d \n", entry_key_dbt.Acl5_mask_prof.key_mask_id);
				CLI_print("\t inner frame: %16s \n", MEA_STATUS_STR(entry_key_dbt.Acl5_mask_prof.Inner_Frame));
				CLI_print("\t Mac_SA: %21s  %02x:%02x:%02x:%02x:%02x:%02x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Mac.MacSA_en),
					entry_key_dbt.layer2.Mac.MacSA.b[0],
					entry_key_dbt.layer2.Mac.MacSA.b[1],
					entry_key_dbt.layer2.Mac.MacSA.b[2],
					entry_key_dbt.layer2.Mac.MacSA.b[3],
					entry_key_dbt.layer2.Mac.MacSA.b[4],
					entry_key_dbt.layer2.Mac.MacSA.b[5]);
				CLI_print("\t Tag1: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_valid), entry_key_dbt.layer2.Tag1);
				CLI_print("\t Tag2: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag2_valid), entry_key_dbt.layer2.Tag2);
				CLI_print("\t Tag3: %23s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag3_valid), entry_key_dbt.layer2.Tag3);
				CLI_print("\t Tag1_p: %21s %10x \n", MEA_STATUS_STR(entry_key_dbt.layer2.Tag1_p_valid), entry_key_dbt.layer2.Tag1_p);
				CLI_print("\t dest ipv6: %18s ", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.Dst_Ipv6_valid));

				buf[0] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 24) & 0xff;
				buf[1] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 16) & 0xff;
				buf[2] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 8) & 0xff;
				buf[3] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[3] >> 0) & 0xff;

				buf[4] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 24) & 0xff;
				buf[5] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 16) & 0xff;
				buf[6] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 8) & 0xff;
				buf[7] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[2] >> 0) & 0xff;

				buf[8] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 24) & 0xff;
				buf[9] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 16) & 0xff;
				buf[10] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 8) & 0xff;
				buf[11] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[1] >> 0) & 0xff;

				buf[12] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 24) & 0xff;
				buf[13] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 16) & 0xff;
				buf[14] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 8) & 0xff;
				buf[15] = (entry_key_dbt.layer3.IPv6.Dst_Ipv6[0] >> 0) & 0xff;

				if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
					CLI_print(">>>inet_ntop<<< \n");
				}
				CLI_print("\t %s \n", stripv6);

				CLI_print("\t source ipv6: %16s ", MEA_STATUS_STR(entry_key_dbt.layer3.IPv6.Src_Ipv6_valid));

				buf[0] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 24) & 0xff;
				buf[1] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 16) & 0xff;
				buf[2] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 8) & 0xff;
				buf[3] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[3] >> 0) & 0xff;

				buf[4] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 24) & 0xff;
				buf[5] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 16) & 0xff;
				buf[6] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 8) & 0xff;
				buf[7] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[2] >> 0) & 0xff;

				buf[8] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 24) & 0xff;
				buf[9] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 16) & 0xff;
				buf[10] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 8) & 0xff;
				buf[11] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[1] >> 0) & 0xff;

				buf[12] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 24) & 0xff;
				buf[13] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 16) & 0xff;
				buf[14] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 8) & 0xff;
				buf[15] = (entry_key_dbt.layer3.IPv6.Src_Ipv6[0] >> 0) & 0xff;

				if (inet_ntop(AF_INET6, buf, stripv6, INET6_ADDRSTRLEN) == NULL) {
					CLI_print(">>>inet_ntop<<< \n");
				}
				CLI_print("\t %s \n", stripv6);
				CLI_print("\t %s %17s \t   %08x \n", "IPv6 SigDst", MEA_STATUS_STR(entry_key_dbt.layer3.IPv4.dst_ip_valid), entry_key_dbt.layer3.IPv4.dst_ip);
				CLI_print("\t %s %17s \t   %08x \n", "IPv6 SigSrc", MEA_STATUS_STR(entry_key_dbt.layer3.IPv4.src_ip_valid), entry_key_dbt.layer3.IPv4.src_ip);
				CLI_print("\t action: %21s %10d \n", MEA_STATUS_STR(entry_data.Action_Valid), entry_data.Action_Id);
				CLI_print("\t fwd Act en:%18s  \n", MEA_STATUS_STR(entry_data.fwd_Act_en));
				CLI_print("\t fwd force:%19s  \n", MEA_STATUS_STR(entry_data.fwd_force));
				CLI_print("\t fwd internal mac:%12s  \n", MEA_STATUS_STR(entry_data.fwd_int_mac));
				CLI_print("\t fwd key:%32d  \n", entry_data.fwd_key);
				CLI_print("\t lxcp_win:%20s  \n", MEA_STATUS_STR(entry_data.lxcp_win));
				CLI_print("\t out:    \t\t   ");
				for (port = 0; port < ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++) {
					if (((ENET_Uint32*)(&(entry_data.OutPorts.out_ports_0_31)))[port / 32] & (1 << (port % 32))) {
						CLI_print("%3d ", port);
					}
				}

				CLI_print("\n-----------------------------------------------------------------------\n");
			}
				break;
			default:
				CLI_print("ERROR: KeyType %d is not valid \n", entry_key_dbt.keyType);
				break;
			}
		}
		else {
			CLI_print("keyType: %10d   hashAdress 0x%08x \n", entry_key_dbt.keyType,(mea_drv_acl5_get_hashAdress(Id) ) );
		
		}



	}


	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Delete(int argc, char *argv[])
{

	MEA_Uint32 Id;
	MEA_Uint32 from_Id;
	MEA_Uint32 to_Id;
	MEA_Bool exist;
	if (argc < 2) {
		return MEA_ERROR;
	}


	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_MAX_ENTRY_SW - 1;

	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (MEA_API_ACL5_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("Error  mea_drv_ACL5_IsExist failed  MaskId=%d\n", Id);
			return MEA_OK;
		}
	}


	for (Id = from_Id; Id <= to_Id; Id++)
	{
		exist = MEA_FALSE;

		if (MEA_API_ACL5_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;

		if (!exist)
			continue;



		if (MEA_API_ACL5_Delete_Entry(MEA_UNIT_0, Id) != MEA_OK)
		{
			CLI_print("Error  MEA_API_ACL5_Delete_Entry failed to delete %d\n", Id);
			continue;
		}

		CLI_print("Done: %d \n", Id);
	}


	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Debug(int argc, char *argv[])
{
	int s;
	MEA_MacAddr     from_MacDA;
	MEA_MacAddr     from_MacSA;
	MEA_ACL5Id_t from_ipv4_src = 0;
	MEA_ACL5Id_t from_ipv4_dst = 0;
	MEA_Uint32 num_of_entries = 0;
	MEA_ACL5Id_t from_ipv6_src[4] = { 0 };
	MEA_ACL5Id_t from_ipv6_dst[4] = { 0 };
	MEA_ACL5Id_t from_Sport = 0;
	MEA_ACL5Id_t from_Dport = 0;
	MEA_ACL5_Key_dbt key_entry;
	MEA_ACL5_Entry_Data_dbt action_entry;
	MEA_Uint32 num_of_ports;
	MEA_Port_t port;
	MEA_Uint16 i;
	MEA_Uint32 j;
	MEA_Bool SID_valid = MEA_FALSE;
	unsigned char buf[sizeof(struct in6_addr)];
	MEA_ACL5Id_t Id_i = MEA_PLAT_GENERATE_NEW_ID;

	MEA_OS_memset(&key_entry, 0, sizeof(key_entry));
	MEA_OS_memset(&action_entry, 0, sizeof(action_entry));
	MEA_OS_memset(&from_MacDA, 0, sizeof(from_MacDA));
	MEA_OS_memset(&from_MacSA, 0, sizeof(from_MacSA));

	for (i = 0; i < argc; i++) {
		if (!MEA_OS_strcmp(argv[i], "keyNONE")) {
			key_entry.keyType = MEA_ACL5_KeyType_NONE;
		}
		if (!MEA_OS_strcmp(argv[i], "keyL2")) {
			key_entry.keyType = MEA_ACL5_KeyType_L2;
		}
		if (!MEA_OS_strcmp(argv[i], "keyIPV4")) {
			key_entry.keyType = MEA_ACL5_KeyType_IPV4;
		}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6full")) {
			key_entry.keyType = MEA_ACL5_KeyType_IPV6_FULL;
		}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6dest")) {
			key_entry.keyType = MEA_ACL5_KeyType_IPV6_DST;
		}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6src")) {
			key_entry.keyType = MEA_ACL5_KeyType_IPV6_SRC;
		}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6SigDest")) {
			key_entry.keyType = MEA_ACL5_KeyType_IPV6_SIG_Dest;
		}
		if (!MEA_OS_strcmp(argv[i], "keyIPV6SigSrc")) {
			key_entry.keyType = MEA_ACL5_KeyType_IPV6_SIG_SRC;
		}
		if (!MEA_OS_strcmp(argv[i], "-SA_MAC")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
				key_entry.layer2.Mac.MacSA_en = 1;
				MEA_OS_atohex_MAC(argv[i + 1], &from_MacSA);
			
		}
		if (!MEA_OS_strcmp(argv[i], "-DA_MAC")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
				key_entry.layer2.Mac.MacDA_en = 1;
				MEA_OS_atohex_MAC(argv[i + 1], &from_MacDA);
			
		}

		if (!MEA_OS_strcmp(argv[i], "-ipv4_src")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.layer3.IPv4.src_ip_valid = 1;
			from_ipv4_src = MEA_OS_inet_addr(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-ipv4_dst")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.layer3.IPv4.dst_ip_valid = 1;
			from_ipv4_dst = MEA_OS_inet_addr(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-ipv6_src")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			s = inet_pton(AF_INET6, argv[i + 1], buf);
			if (s <= 0) {
				CLI_print("ipv6 format error\n");
				return MEA_ERROR;
			}
			key_entry.layer3.IPv6.Src_Ipv6_valid = 1;
			from_ipv6_src[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
			from_ipv6_src[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
			from_ipv6_src[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
			from_ipv6_src[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;

		}
		if (!MEA_OS_strcmp(argv[i], "-ipv6_dst")) {
			if (i + 2 > argc) {
				return MEA_ERROR;
			}
			s = inet_pton(AF_INET6, argv[i + 1], buf);
			if (s <= 0) {
				CLI_print("ipv6 format error\n");
				return MEA_ERROR;
			}
			key_entry.layer3.IPv6.Dst_Ipv6_valid = 1;
			from_ipv6_dst[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
			from_ipv6_dst[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
			from_ipv6_dst[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
			from_ipv6_dst[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;
			
		}
		if (!MEA_OS_strcmp(argv[i], "-sport")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.layer4.src_port_valid = 1;
			from_Sport = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-dport")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.layer4.dst_port_valid = 1;
			from_Dport = MEA_OS_atoiNum(argv[i + 1]);
		}

		if (!MEA_OS_strcmp(argv[i], "-group_mask")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			key_entry.Acl5_mask_prof_id = MEA_OS_atoiNum(argv[i + 1]);
			if (MEA_API_ACL5_Mask_Grouping_Profiles_Get(MEA_UNIT_0, key_entry.Acl5_mask_prof_id, &key_entry.Acl5_mask_prof) != MEA_OK)
				return MEA_ERROR;
			if (key_entry.Acl5_mask_prof.key_mask_id !=0 && MEA_API_ACL5_Key_Mask_Prof_Get(MEA_UNIT_0, key_entry.Acl5_mask_prof.key_mask_id, &key_entry.ACL5_key_mask_prof))
				return MEA_ERROR;
			if (key_entry.Acl5_mask_prof.keyType != key_entry.keyType) {
				CLI_print("Error keyType is different of block profile  %d \n", key_entry.Acl5_mask_prof_id);
				return MEA_ERROR;
			}
		}

		if (!MEA_OS_strcmp(argv[i], "-SID")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			SID_valid = MEA_TRUE;
			key_entry.ACLprof = MEA_OS_atoiNum(argv[i + 1]);
		}
		if (!MEA_OS_strcmp(argv[i], "-num_of_entries")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			num_of_entries = MEA_OS_atoiNum(argv[i + 1]);
		}

		if (!MEA_OS_strcmp(argv[i], "-action")) {
			if (i + 2 > argc) {
				return MEA_ERROR;
			}
			action_entry.Action_Valid = MEA_OS_atoiNum(argv[i + 1]);
			action_entry.Action_Id = MEA_OS_atoiNum(argv[i + 2]);

		}
		if (!MEA_OS_strcmp(argv[i], "-out")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			action_entry.OutPorts_valid = 1;
			num_of_ports = MEA_OS_atoiNum(argv[i + 1]);
			if (num_of_ports + i > (MEA_Uint32)argc)
				return MEA_ERROR;
			for (j = 0; j < num_of_ports; j++) {
				port = (MEA_Port_t)MEA_OS_atoiNum(argv[i + 2 + j]);

				if (ENET_IsValid_Queue(ENET_UNIT_0, port, ENET_TRUE) != ENET_TRUE) {
					CLI_print("Cluster id %d is not define \n", port);
					return MEA_ERROR;
				}

				/* check if the out_port is valid */
				((ENET_Uint32*)(&(action_entry.OutPorts.out_ports_0_31)))[port / 32] |=
					(1 << (port % 32));
			}

		}

	}

	if(!SID_valid) {
		CLI_print("Error: missing SID \n");
		return MEA_ERROR;
	}

	if(key_entry.Acl5_mask_prof_id == 0) {
		CLI_print("Error: missing group mask id \n");
		return MEA_ERROR;
	}
		
	if (num_of_entries == 0) {
		CLI_print("Error: there is not number of entries \n");
		return MEA_ERROR;
	}

	
		while (num_of_entries && key_entry.keyType == MEA_ACL5_KeyType_L2) {

			if (key_entry.layer2.Mac.MacSA_en) MEA_OS_memcpy(key_entry.layer2.Mac.MacSA.b, from_MacSA.b, sizeof(from_MacSA.b));
			if (key_entry.layer2.Mac.MacDA_en) MEA_OS_memcpy(key_entry.layer2.Mac.MacDA.b, from_MacDA.b, sizeof(from_MacDA.b));

			if (MEA_API_ACL5_Create_Entry(MEA_UNIT_0, &key_entry, &action_entry, &Id_i) != MEA_OK) {
				CLI_print("Error MEA_API_ACL5_Create_Entry failed \n");
			}
			else
				CLI_print("Done %d \n", Id_i);
			Id_i = MEA_PLAT_GENERATE_NEW_ID;

			if (key_entry.layer2.Mac.MacSA_en)
				Increase_MAC_Address_By_One(&from_MacSA);
			
			if (key_entry.layer2.Mac.MacDA_en)
				Increase_MAC_Address_By_One(&from_MacDA);
			
			num_of_entries--;
		}
	
			while (num_of_entries && key_entry.keyType == MEA_ACL5_KeyType_IPV4)
			{
				MEA_OS_memcpy(key_entry.layer2.Mac.MacSA.b, from_MacSA.b, sizeof(from_MacSA.b));
				MEA_OS_memcpy(key_entry.layer2.Mac.MacDA.b, from_MacDA.b, sizeof(from_MacDA.b));
				key_entry.layer3.IPv4.dst_ip = from_ipv4_dst;
				key_entry.layer3.IPv4.src_ip = from_ipv4_src;
				key_entry.layer4.src_port = from_Sport;
				key_entry.layer4.dst_port = from_Dport;
				if (MEA_API_ACL5_Create_Entry(MEA_UNIT_0, &key_entry, &action_entry, &Id_i) != MEA_OK) {
					CLI_print("Error MEA_API_ACL5_Create_Entry failed \n");
				}
				else
					CLI_print("Done %d \n", Id_i);
				Id_i = MEA_PLAT_GENERATE_NEW_ID;
				if (key_entry.layer3.IPv4.dst_ip_valid) from_ipv4_dst++;
				if (key_entry.layer3.IPv4.src_ip_valid) from_ipv4_src++;
				if (key_entry.layer4.src_port_valid) from_Sport++;
				if (key_entry.layer4.dst_port_valid) from_Dport++;
				if (key_entry.layer2.Mac.MacSA_en) Increase_MAC_Address_By_One(&from_MacSA);
				if (key_entry.layer2.Mac.MacDA_en) Increase_MAC_Address_By_One(&from_MacDA);
				num_of_entries--;
			}
		
			while (num_of_entries && (key_entry.keyType != MEA_ACL5_KeyType_L2 || key_entry.keyType == MEA_ACL5_KeyType_IPV4))
			{
				MEA_OS_memcpy(key_entry.layer2.Mac.MacSA.b, from_MacSA.b, sizeof(from_MacSA.b));
				MEA_OS_memcpy(key_entry.layer2.Mac.MacDA.b, from_MacDA.b, sizeof(from_MacDA.b));
				MEA_OS_memcpy(key_entry.layer3.IPv6.Src_Ipv6, from_ipv6_src, sizeof(from_ipv6_src));
				MEA_OS_memcpy(key_entry.layer3.IPv6.Dst_Ipv6, from_ipv6_dst, sizeof(from_ipv6_dst));
				key_entry.layer4.src_port = from_Sport;
				key_entry.layer4.dst_port = from_Dport;

				if (MEA_API_ACL5_Create_Entry(MEA_UNIT_0, &key_entry, &action_entry, &Id_i) != MEA_OK) {
					CLI_print("Error MEA_API_ACL5_Create_Entry failed \n");
				}
				else
					CLI_print("Done %d \n", Id_i);
				Id_i = MEA_PLAT_GENERATE_NEW_ID;

				if (key_entry.layer3.IPv6.Dst_Ipv6_valid)
					Increase_IPV6_Address_By_One(from_ipv6_dst);

				if (key_entry.layer3.IPv6.Src_Ipv6_valid)
					Increase_IPV6_Address_By_One(from_ipv6_src);

				if (key_entry.layer2.Mac.MacSA_en) Increase_MAC_Address_By_One(&from_MacSA);
				if (key_entry.layer2.Mac.MacDA_en) Increase_MAC_Address_By_One(&from_MacDA);
				if (key_entry.layer4.src_port_valid) from_Sport++;
				if (key_entry.layer4.dst_port_valid) from_Dport++;

				num_of_entries--;
			}
		
	
		return MEA_OK;
}

/*------------------ACL5_Block_Profiles_Table--------------------*/

static MEA_Status MEA_CLI_ACL5_Mask_Grouping_Profiles_Create(int argc, char *argv[])
{
	MEA_ACL5Id_t Id= MEA_PLAT_GENERATE_NEW_ID;
	MEA_ACL5_Mask_Grouping_Profiles entry_prof;
    int i;

	if (argc < 6) {
		return MEA_ERROR;
	}

	MEA_OS_memset(&entry_prof, 0, sizeof(entry_prof));



    for (i = 0; i < argc; i++) {
        if (!MEA_OS_strcmp(argv[i], "-f")) {
            if (i + 1 > argc) {
                return MEA_ERROR;
            }
            Id = MEA_OS_atoiNum(argv[i + 1]);
			if (Id == 0)
			{
				CLI_print("Error: Invalid ID \n");
				return MEA_ERROR;
			}
        }
        if (!MEA_OS_strcmp(argv[i], "-info")) 
        {
            if (i + 5 > argc) {
                return MEA_ERROR;
            }
        
            entry_prof.keyType        = MEA_OS_atoiNum(argv[i+1]);
            entry_prof.ip_mask_id     = MEA_OS_atoiNum(argv[i+2]);
            entry_prof.range_mask_id  = MEA_OS_atoiNum(argv[i+3]);
            entry_prof.key_mask_id    = MEA_OS_atoiNum(argv[i+4]);
            entry_prof.Inner_Frame    = MEA_OS_atoiNum(argv[i+5]);


        }

   }
	
	

	if (MEA_API_ACL5_Mask_Grouping_Profiles_Create(MEA_UNIT_0, &entry_prof, &Id) != MEA_OK) {
		CLI_print("Error MEA_API_ACL5_Block_Profiles_Create failed \n");
		return MEA_OK;
	}

	CLI_print("Done %d \n", Id);


	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Mask_Grouping_Profiles_Get(int argc, char *argv[])
{
	MEA_ACL5Id_t Id;
	MEA_ACL5Id_t from_Id;
	MEA_ACL5Id_t to_Id;

	MEA_ACL5_Mask_Grouping_Profiles entry_prof;
	MEA_Uint32 count = 0;

	MEA_Bool exist;


	if (argc < 2) {
		return MEA_ERROR;
	}

	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_MASK_GROUPING_PROFILES_MAX_PROF -1;
	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (MEA_API_ACL5_Mask_Grouping_Profiles_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("ERROR: MEA_API_ACL5_Mask_Grouping_Profiles_isExist Id=%d\n", Id);
		}
		if (!exist) {
			CLI_print("ACL5 Block Profiles %d is not exist \n", Id);
			return MEA_OK;
		}
	}

	CLI_print("-------------------------------------------\n");
	CLI_print("         ACL5_Block_Profiles               \n");
	CLI_print("-------------------------------------------\n");

	for (Id = from_Id; Id <= to_Id; Id++)
	{
		if (MEA_API_ACL5_Mask_Grouping_Profiles_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;
		if (!exist)
			continue;




		if (MEA_API_ACL5_Mask_Grouping_Profiles_Get(MEA_UNIT_0, Id, &entry_prof) != MEA_OK) {
			return MEA_ERROR;
		}
		if (count == 0)
		{
			CLI_print("--------------------------------------------------\n");

			CLI_print("  Id    ACL5   keytype   ip      range    key    inner    \n"
				      "  TLB   Do               mask    mask     mask   frame    \n"
				      "	                         id       id     id			   \n");
			CLI_print("------ ------- -------  ------  ------  ------  ------ \n");
		}

		count++;


		CLI_print("%5d   %2d  %4d  %9d    %5d   %5d   %5d \n", Id,
			entry_prof.valid,
			entry_prof.keyType,
			entry_prof.ip_mask_id,
			entry_prof.range_mask_id,
			entry_prof.key_mask_id,
			entry_prof.Inner_Frame);


	}

	CLI_print("-------------------------------------------------- \n");

	
	

	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Mask_Grouping_Profiles_Delete(int argc, char *argv[])
{

	MEA_ACL5_t Id;
	MEA_ACL5_t from_Id;
	MEA_ACL5_t to_Id;
	MEA_Bool exist;
	if (argc < 2) {
		return MEA_ERROR;
	}


	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_MASK_GROUPING_PROFILES_MAX_PROF - 1;

	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (Id == 0)
		{
			CLI_print("Error: Invalid ID \n");
			return MEA_ERROR;
		}
		if (MEA_API_ACL5_Mask_Grouping_Profiles_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("Error  MEA_API_ACL5_Mask_Grouping_Profiles_isExist failed  Id=%d\n", Id);
			return MEA_OK;
		}
	}


	for (Id = from_Id; Id <= to_Id; Id++)
	{
		exist = MEA_FALSE;

		if (MEA_API_ACL5_Mask_Grouping_Profiles_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;

		if (!exist)
			continue;



		if (MEA_API_ACL5_Mask_Grouping_Profiles_Delete(MEA_UNIT_0, Id) != MEA_OK)
		{
			CLI_print("Error  MEA_API_ACL5_Mask_Grouping_Profiles_Delete failed to delete %d\n", Id);
			continue;
		}

		CLI_print("Done: %d deleted \n", Id);
	}


	return MEA_OK;
}

MEA_Status MEA_CLI_ACL5_AddUserCmds(void)
{
	CLI_defineCmd("MEA ACL5 Mask_Grouping_Profiles create",
		(CmdFunc)MEA_CLI_ACL5_Mask_Grouping_Profiles_Create,
		"create   Mask Grouping Profiles \n",
		"create   Mask Grouping Profiles \n"
        "    -f <id> value\n"
		"    -info <keyType> <ip mask id> <range mask id> <key mask id> <inner frame> \n"
		
		"---------------------------------------------------\n"
		"Examples: \n"
		"        ACL5 Mask_Grouping_Profiles create -f 1 -info 1 2 1 1 0  \n"
		"           \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_Grouping_Profiles show",
		(CmdFunc)MEA_CLI_ACL5_Mask_Grouping_Profiles_Get,
		"show   Mask Grouping Profiles\n",
		"show   Mask Grouping Profiles \n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_Grouping_Profiles show 1    \n"
		"       ACL5 Mask_Grouping_Profiles show all   \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_Grouping_Profiles delete",
		(CmdFunc)MEA_CLI_ACL5_Mask_Grouping_Profiles_Delete,
		"delete   ACL5 Mask_Grouping_Profiles delete\n",
		"delete   ACL5 Mask_Grouping_Profiles delete\n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_Grouping_Profiles delete 1    \n"
		"       ACL5 Mask_Grouping_Profiles delete all   \n"
	);

	CLI_defineCmd("MEA ACL5 Debug create",
		(CmdFunc)MEA_CLI_ACL5_Debug,
		"create   Debug \n",
		"create   Debug \n"
		"       key-type options:  keyNONE          \n"
		"                          keyL2            \n"
		"                          keyIPV4          \n"
		"                          keyIPV6full      \n"
		"                          keyIPV6dest      \n"
		"                          keyIPV6src       \n"
		"                          keyIPV6SigDest   \n"
		"                          keyIPV6SigSrc    \n"
		"       -num_of_entries - number of ACL5 entries  \n"
		"       -SA_MAC <from_source_mac>  \n"
		"       -DA_MAC  <from_dest_mac> \n"
		"       -ipv4_dst - <from_ipv4_dst> \n"
		"       -ipv4_src - <from_ipv4_src> \n"
		"       -ipv6_dst - <from_ipv6_dst> \n"
		"       -ipv6_src - <from_ipv6_src> \n"
		"       -dport - <from_dport> \n"
		"       -sport - <from_sport> \n"
		"       -group_mask \n"
		"       -action - action  (actionId) \n"
		"                 <valid> - 0 or 1\n"
		"                 <id>    - Action Id     \n"
		"       -out  - <valid> <num of cluster> <cluster><cluster>  \n"

		"------------------------------------------------------------------------------\n"
		"Examples: \n"
		"        ACL5 Debug create keyIPV4 -num_of_entries 100 -ipv4_dst 2.2.2.2 -ipv4_src 1.1.1.1 -sport 1111 -dport 3333 -action 1 1  \n"
		"           \n"
		
	);


	CLI_defineCmd("MEA ACL5 entry create",
		(CmdFunc)MEA_CLI_ACL5_Create,
		"create   profile\n",
		"create   profile \n"
		"       key-type options:  keyNONE          \n"
		"                          keyL2            \n"
		"                          keyIPV4          \n"
		"                          keyIPV6full      \n"
		"                          keyIPV6dest      \n"
		"                          keyIPV6src       \n"
		"                          keyIPV6SigDest   \n"
		"                          keyIPV6SigSrc    \n"
		"       -f - force acl5 profile  \n"
		"(L2)         1- <UMB> <Tag1> <Tag2> <SA_MAC> <DA_MAC> <Tag1_p> <Tag3> <-group_mask> \n"
		"(IPV4)       2- <UMB> <dscp> <TCP_Flags> <src_port> <dst_port> <Etype3> <-group_mask> \n"
		"                <Tag1> <Tag2> <SA_MAC> <DA_MAC> <dst_ip> <src_ip> <IPv4_Flags> <Tag1_p> <Tag3> <priType> <ip_protocol>\n"
		"(IPV6full)   3- <UMB> <priType> <dscp> <TCP_Flags> <src_port> <dst_port> <ip_protocol> <Etype3> <Dst_Ipv6> <Src_Ipv6> <-group_mask>\n"
		"(IPV6dst)    4- <UMB> <priType> <dscp> <TCP_Flags> <src_port> <dst_port> <ip_protocol> <Etype3> <Dst_Ipv6> <Tag1> <Tag1_p>\n"
		"                <Tag3> <DA_MAC> <IPv6_Flow_Label> <-group_mask>\n"
		"(IPV6src)    5- <UMB> <priType> <dscp> <TCP_Flags> <src_port> <dst_port> <ip_protocol> <Etype3> <Tag1> <Tag1_p> <Tag3>\n"
		"                <SA_MAC> <IPv6_Flow_Label> <Src_Ipv6> <-group_mask>\n"
		"(IPV6sigdst) 6- <UMB> <priType> <dscp> <TCP_Flags> <src_port> <dst_port> <ip_protocol> <Etype3> <-group_mask>\n"
		"                <Tag1> <Tag2> <Tag1_p> <Tag3> <DA_MAC> <IPv6_Flow_Label> <Dst_Ipv6> <Src_Ipv6>\n"
		"(IPV6sigsrc) 7- <UMB> <priType> <dscp> <TCP_Flags> <src_port> <dst_port> <ip_protocol> <Etype3> <-group_mask>\n"
		"                <Tag1> <Tag2> <Tag1_p> <Tag3> <SA_MAC> <IPv6_Flow_Label> <Dst_Ipv6> <Src_Ipv6>\n"
		" key parameters: \n"
		"                -UMB \n"
		"                       <valid> - 0 or 1\n"
		"                       <valu>   1 - unicast, 2 - multicast, 3 - braodcast \n"
		"                -Tag1 - VLAN/MPLS \n"
		"                       <valid> - 0 or 1\n"
		"                       <id>    - digit number \n"
		"                -Tag2 - VLAN/MPLS/I-SID (Service Instance VLAN ID for PBB MAC-IN-MAC) \n"
		"                       <valid> - 0 or 1\n"
		"                       <id>    - digit number  \n"
		"                -SA_MAC   \n"
		"                       <valid> - 0 or 1\n"
		"                       <id>    - xx:xx:xx:xx:xx:xx     \n"
		"                -DA_MAC - xx:xx:xx:xx:xx:xx \n"
		"                       <valid> - 0 or 1\n"
		"                       <id>    - xx:xx:xx:xx:xx:xx     \n"
		"                -Tag1_p \n"
		"                       <valid> - 0 or 1\n"
		"                       <id>    - digit number  \n"
		"                -Tag3 - VLAN/MPLS \n"
		"                       <valid> - 0 or 1\n"
		"                       <id>    - digit number  \n"
		"                <-group_mask> - id number  \n"
		"                -dscp - <valid> <IPv4 DSCP/IPv6 TC>  \n"
		"                -TCP_Flags - <valid> <val 0...7>  \n"
		"                -src_port - <valid> <L4src port   (0..2^16-1)> \n"
		"                -dst_port - <valid> <L4dest port  (0..2^16-1)> \n"
		"                -Etype3 - L3_EtherType   \n"
		"                       <valid> - 0 or 1\n"
		"                       <Etype3 id>      \n"
		"                -dst_ip - IPv4 address in dot notation \n"
		"                       <valid> - 0 or 1\n"
		"                       <ip address>      \n"
		"                -src_ip -IPv4 address in dot notation \n"
		"                       <valid> - 0 or 1\n"
		"                       <ip address>      \n"
		"                -IPv4_Flags - <valid> <val 0...3>  \n"
		"                -priType: \n"
		"                     0 - untag packet		  \n"
		"                     1 - Outer vlan			  \n"
		"                     2 - Inner vlan			  \n"
		"                     3 - IPv4 Precedence	  \n"
		"                     4 - IPv4 TOS			  \n"
		"                     5 - IPv4 DSCP			  \n"
		"                -ip_protocol - <valid>  <4 digits number> \n"
		"                -Dst_Ipv6 - IPv6 dest address   \n"
		"                       <valid> - 0 or 1\n"
		"                       <ip address>      \n"
		"                -Src_Ipv6 - IPv6 source address   \n"
		"                       <valid> - 0 or 1\n"
		"                       <ip address>      \n"
		"                -IPv6_Flow_Label  \n"
		" data parameters: \n"
		"    <-SID> - acl5 profile id  \n"
		"    <-action> - action  (actionId) \n"
		"                 <valid> - 0 or 1\n"
		"                 <id>    - Action Id     \n"
		"    <-lxcp_win> - <valid>  \n"
		"    <-fwd_win> - <valid>  \n"
		"    <-out> - <valid> <num of cluster> <cluster><cluster>  \n"
		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 create  keyL2   -f 5 -UMB 1 2 -Tag1 3 -SA_MAC 0 d2:ca:51:4d:7f:f3 -SID 19 -out 1 1 104 -action 1 1  \n"
		"       ACL5 create  keyIPV4 -UMB 1 2 -dscp 4 -Tag1 5 -DA_MAC 1 d2:ca:51:4d:7f:f3 -Etype3 1 0800 -dst_ip 1 1.1.1.5 -SID 31 -out 1 1 105 -action 1 1 -lxcp_win 1 \n"
		"           \n" 
	);

	CLI_defineCmd("MEA ACL5 entry show",
		(CmdFunc)MEA_CLI_ACL5_Get,
		"show   Acl5 entry\n",
		"show   Acl5 entry \n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 entry show 1    \n"
		"       ACL5 entrys show all   \n"
	);

	CLI_defineCmd("MEA ACL5 entry delete",
		(CmdFunc)MEA_CLI_ACL5_Delete,
		"delete   ACL5 delete\n",
		"delete   ACL5 delete\n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 delete 1    \n"
		"       ACL5 delete all   \n"
	);

	

	return MEA_OK;

}


/************************************************************************/
/*				ACL5 IP Mask  profile			                        */
/************************************************************************/

static MEA_Status MEA_CLI_ACL5_IP_Mask_Prof_Create(int argc, char *argv[])
{
	MEA_Uint16 i;
	MEA_ACL5_t Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_ACL5_Ipmask_dbt entry;
	

	if (argc < 2) {
		return MEA_ERROR;
	}

	MEA_OS_memset(&entry, 0, sizeof(entry));

	for (i = 0; i < argc; i++) {
		if (!MEA_OS_strcmp(argv[i], "-f")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			Id = MEA_OS_atoiNum(argv[i + 1]);
			if (Id == 0)
			{
				CLI_print("Error: Invalid ID \n");
				return MEA_ERROR;
			}
		}
			if (!MEA_OS_strcmp(argv[i], "-IPv4_mask")) {
				if (i + 1 > argc) {
					return MEA_ERROR;
				}
				if (MEA_OS_atoiNum(argv[i + 1]) == 1) {
					entry.enable_Ipv4 = MEA_TRUE;
					entry.Ipv4_source_mask = MEA_OS_atoiNum(argv[i + 2]);
					entry.Ipv4_dest_mask = MEA_OS_atoiNum(argv[i + 3]);
				}
			}
			if (!MEA_OS_strcmp(argv[i], "-IPv6_mask")) {
				if (i + 1 > argc) {
					return MEA_ERROR;
				}
				if (MEA_OS_atoiNum(argv[i + 1]) == 1) {
					entry.enable_Ipv6 = MEA_TRUE;
					entry.Ipv6_source_mask = MEA_OS_atoiNum(argv[i + 2]);
					entry.Ipv6_dest_mask = MEA_OS_atoiNum(argv[i + 3]);
				}
			}
		}
	
	

	if (MEA_API_ACL5_IP_Mask_Prof_Create(MEA_UNIT_0,  &Id, &entry) != MEA_OK) {
		CLI_print("Error MEA_API_ACL5_IP_Mask_Prof_Create failed \n");
		return MEA_OK;
	}

	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_IP_Mask_Set(int argc, char *argv[])
{

	MEA_Uint16 Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_ACL5_Ipmask_dbt entry;



	if (argc < 2) {
		return MEA_ERROR;
	}

 

	Id = MEA_OS_atoiNum(argv[1]);

	CLI_print("ACL5_IP_Mask_Prof_Modify not supported \n");

	if (MEA_API_ACL5_IP_Mask_Prof_Set(MEA_UNIT_0, Id, &entry) != MEA_OK)
	{
		CLI_print("Error MEA_API_ACL5_IP_Mask_Prof_Modify failed \n");
		return MEA_OK;
	}


	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_IP_Mask_Prof_Delete(int argc, char *argv[])
{

	MEA_ACL5_t Id;
	MEA_ACL5_t from_Id;
	MEA_ACL5_t to_Id;
	MEA_Bool exist;
	if (argc < 2) {
		return MEA_ERROR;
	}


	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_IPMASK_MAX_PROF -1;

	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (MEA_API_ACL5_IP_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("Error  MEA_API_ACL5_IP_Mask_Prof_isExist failed  MaskId=%d\n", Id);
			return MEA_OK;
		}
	}


	for (Id = from_Id; Id <= to_Id; Id++)
	{
		exist = MEA_FALSE;

		if (MEA_API_ACL5_IP_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;

		if (!exist)
			continue;



		if (MEA_API_ACL5_IP_Mask_Prof_Delete(MEA_UNIT_0, Id) != MEA_OK)
		{
			CLI_print("Error  MEA_API_ACL5_IP_Mask_Prof_Delete failed to delete %d\n", Id);
			continue;
		}

		CLI_print("Done: %d \n", Id);
	}


	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_IP_Mask_Prof_Get(int argc, char *argv[])
{
	MEA_Uint16 Id;
	MEA_Uint16 from_Id;
	MEA_Uint16 to_Id;

	MEA_ACL5_Ipmask_dbt entry;
	MEA_Uint32 count = 0;

	MEA_Bool exist;


	if (argc < 2) {
		return MEA_ERROR;
	}

	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_IPMASK_MAX_PROF -1;
	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (Id == 0)
		{
			CLI_print("Error: Invalid ID \n");
			return MEA_ERROR;
		}
		if (MEA_API_ACL5_IP_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("ERROR: MEA_API_ACL5_IP_Mask_Prof_IsExist %d\n", Id);
		}
		if (!exist) {
			CLI_print("ACL5 ip mask profile %d is not exist \n", Id);
			return MEA_OK;
		}
	}

	

	CLI_print("-------------------------------------------\n");
	CLI_print("         IP Mask Profile                  \n");
	CLI_print("-------------------------------------------\n");

	for (Id = from_Id; Id <= to_Id; Id++)
	{
		if (MEA_API_ACL5_IP_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;
		if (!exist)
			continue;




		if (MEA_API_ACL5_IP_Mask_Prof_Get(MEA_UNIT_0, Id, &entry) != MEA_OK) {
			return MEA_ERROR;
		}
		if (count == 0)
		{
			CLI_print("--------------------------------------------------\n");

			CLI_print(" Id   IPv4   IPv4     IPv4   IPv6   IPv6     IPv6   \n"
					  "      mask   source   dest   mask   source   dest   \n"
					  "      EN	                    EN					   \n");
			CLI_print(" --- ------ -------  ------ ------ -------  ------  \n");
		}  

		count++;

		
	 	    CLI_print("%5d   %2d  %5d  %5d    %2d   %5d   %5d   \n", Id,
			entry.enable_Ipv4,
			entry.Ipv4_source_mask,
			entry.Ipv4_dest_mask,
			entry.enable_Ipv6,
			entry.Ipv6_source_mask,
			entry.Ipv6_dest_mask);


	}

	CLI_print("-------------------------------------------------- \n");

	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_IP_Mask_Prof_IsExit(int argc, char *argv[])
{
	MEA_Uint16 Id;
	MEA_Uint16 from_Id;
	MEA_Uint16 to_Id;
	MEA_Bool exist;
	MEA_Uint32 count = 0;
	
	if(argc<=1)
		return MEA_ERROR;


	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_IPMASK_MAX_PROF -1;
	}
	else
	{
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (Id == 0)
		{
			CLI_print("Error: Invalid ID \n");
			return MEA_ERROR;
		}
	}


	for (Id = from_Id; Id <= to_Id; Id++)
	{

		if (MEA_API_ACL5_IP_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			continue;

		}

		if (count == 0)
		{

			CLI_print("   Id  Exist    \n");
			CLI_print(" ----- -------  \n");
		}

		count++;

		CLI_print(" %5d %s \n", Id, exist ? "YES" : "NO");

	}

	return MEA_OK;

}

static MEA_Status MEA_CLI_ACL5_IP_Mask_Prof_GetFirst(int argc, char *argv[])
{
	MEA_ACL5_t Id;
	MEA_Bool exist;
	MEA_ACL5_Ipmask_dbt entry;

	MEA_Bool found_o = MEA_FALSE;

	if (argc < 2) {
		return MEA_ERROR;
	}

	for (Id = 1; Id < MEA_ACL5_IPMASK_MAX_PROF; Id++)
	{
		if (MEA_API_ACL5_IP_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;
		if (!exist)
			continue;

		if (MEA_API_ACL5_IP_Mask_Prof_GetFirst(MEA_UNIT_0, &Id, &entry, &found_o) != MEA_OK) {
			return MEA_ERROR;
		}

		

		CLI_print("--------------------------------------------------\n");

		CLI_print(" Id   IPv4   IPv4     IPv4   IPv6   IPv6     IPv6   \n"
				  "		 mask	source	 dest   mask   source   dest   \n"
				  "		 EN					    EN					   \n");
		CLI_print(" --- ------ -------  ------ ------ -------  ------  \n");
		
		CLI_print(" %5d %2d %5d %5d %2d %5d %5d\n", Id,
			entry.enable_Ipv4,
			entry.Ipv4_source_mask,
			entry.Ipv4_dest_mask,
			entry.enable_Ipv6,
			entry.Ipv6_source_mask,
			entry.Ipv6_dest_mask);
		break;
	}

	return MEA_OK;
}


static MEA_Status MEA_CLI_ACL5_IP_Mask_Prof_GetNext(int argc, char *argv[])
{
	MEA_ACL5_t Id;
	MEA_Bool exist;
	MEA_ACL5_Ipmask_dbt entry;

	MEA_Bool found_o = MEA_FALSE;

	if (argc < 2) {
		return MEA_ERROR;
	}
	
	for (Id = MEA_OS_atoiNum(argv[1]); Id < MEA_ACL5_IPMASK_MAX_PROF - 1; Id++)
	{
		if (MEA_API_ACL5_IP_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;
		if (!exist)
			continue;

		if (Id == MEA_ACL5_IPMASK_MAX_PROF)
		{
			CLI_print("%5d is the last \n", Id);
			break;
		}

		if (MEA_API_ACL5_IP_Mask_Prof_GetNext(MEA_UNIT_0, &Id, &entry, &found_o) != MEA_OK) {
			return MEA_ERROR;
		}

		

		CLI_print("--------------------------------------------------\n");

		CLI_print(" Id   IPv4   IPv4     IPv4   IPv6   IPv6     IPv6   \n"
				  "		 mask	source	 dest   mask   source   dest   \n"
				  "		 EN					    EN					   \n");
		CLI_print(" --- ------ -------  ------ ------ -------  ------  \n");

		CLI_print(" %5d %2d %5d %5d %2d %5d %5d\n", Id,
			entry.enable_Ipv4,
			entry.Ipv4_source_mask,
			entry.Ipv4_dest_mask,
			entry.enable_Ipv6,
			entry.Ipv6_source_mask,
			entry.Ipv6_dest_mask);
		break;
	}

	return MEA_OK;
}


MEA_Status MEA_CLI_ACL5_IP_Mask_Prof_AddUserCmds(void)
{
	CLI_defineCmd("MEA ACL5 Mask_profile IP create",
		(CmdFunc)MEA_CLI_ACL5_IP_Mask_Prof_Create,
		"create   profile\n",
		"create   profile \n"
		"    -f <Id>   forced id\n"
		"   -IPv4_mask <valid> <source mask> <dest mask>\n"
		"   -IPv6_mask <valid> <source mask> <dest mask> \n"
		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile IP create -f 1 -IPv4_mask 1 16 16 -IPv6_mask 1 8 8  \n"
		"           \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile IP set",
		(CmdFunc)MEA_CLI_ACL5_IP_Mask_Set,
		"set   profile\n",
		"set   profile \n"
		"not support \n"
		
	);

	CLI_defineCmd("MEA ACL5 Mask_profile IP show",
		(CmdFunc)MEA_CLI_ACL5_IP_Mask_Prof_Get,
		"show   ACL5 IP Mask_profile show\n",
		"show   ACL5 IP Mask_profile show\n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile IP show 1    \n"
		"       ACL5 Mask_profile IP show all   \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile IP delete",
		(CmdFunc)MEA_CLI_ACL5_IP_Mask_Prof_Delete,
		"delete   ACL5 IP Mask_profile delete\n",
		"delete   ACL5 IP Mask_profile delete\n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile IP Prof delete 1    \n"
		"       ACL5 Mask_profile IP delete all   \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile IP IsExist",
		(CmdFunc)MEA_CLI_ACL5_IP_Mask_Prof_IsExit,
		"IsExist   ACL5 IP Mask_profile IsExist\n",
		"IsExist   ACL5 IP Mask_profile IsExist\n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile IP IsExist 1    \n"
		"       ACL5 Mask_profile IP IsExist all   \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile IP GetFirst",
		(CmdFunc)MEA_CLI_ACL5_IP_Mask_Prof_GetFirst,
		"GetFirst   ACL5 Mask_profile IP\n",
		"GetFirst   ACL5 Mask_profile IP\n"
		"    all\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile IP GetFirst all    \n"

	);

	CLI_defineCmd("MEA ACL5 Mask_profile IP GetNext",
		(CmdFunc)MEA_CLI_ACL5_IP_Mask_Prof_GetNext,
		"GetNext   ACL5 IP Mask_profile GetNext \n",
		"GetNext   ACL5 IP Mask_profile GetNext \n"
		"    <Id>  Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile IP GetNext 1    \n"
	
	);

	return MEA_OK;

}


/************************************************************************/
/*				ACL5 Range Mask  profile			                    */
/************************************************************************/


static MEA_Status MEA_CLI_ACL5_Range_Mask_Prof_Create(int argc, char *argv[])
{
	MEA_ACL5_t Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_Uint16 i;

	if (argc < 2) {
		return MEA_ERROR;
	}


	for (i = 0; i < argc; i++) {
		if (!MEA_OS_strcmp(argv[i], "-f")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			Id = MEA_OS_atoiNum(argv[i + 1]);
			if (Id == 0)
			{
				CLI_print("Error: Invalid ID \n");
				return MEA_ERROR;
			}
		}

	}
	
	

		if (MEA_API_ACL5_Range_Mask_Prof_Create(MEA_UNIT_0, &Id) != MEA_OK) {
		CLI_print("Error MEA_API_ACL5_Range_Mask_Prof_Create failed \n");
		return MEA_OK;
	}

	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Range_Mask_Set(int argc, char *argv[])
{

	MEA_Uint16 Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_ACL5Index_t index = MEA_PLAT_GENERATE_NEW_ID;
	MEA_ACL5_Range_Mask_data_dbt entry;
	//MEA_Uint16 index = 0;
	MEA_Uint16 i;
	MEA_Bool exist;


	if (argc < 2) {
		return MEA_ERROR;
	}



	MEA_OS_memset(&entry, 0, sizeof(entry));

		for (i = 0; i < argc; i++) {
			if (!MEA_OS_strcmp(argv[i], "-f")) {
				if (i + 1 > argc) {
					return MEA_ERROR;
				}
				Id = MEA_OS_atoiNum(argv[i + 1]);
				if (Id == 0)
				{
					CLI_print("Error: Invalid ID \n");
					return MEA_ERROR;
				}
				index = MEA_OS_atoiNum(argv[i + 2]);
				if (MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK)
				{
					CLI_print("Error MEA_API_ACL5_Range_Mask_Prof_isExist failed \n");
					return MEA_OK;
				}
				if (!exist) {
					CLI_print("ACL5 Range mask profile %d is not exist \n", Id);
					return MEA_OK;
				}
			}
			if (!MEA_OS_strcmp(argv[i], "-priValue")) {
				if (i + 1 > argc) {
					return MEA_ERROR;
				}
				entry.priValue = MEA_OS_atoiNum(argv[i + 1]);
			}
			if (!MEA_OS_strcmp(argv[i], "-L4src")) {
				if (i + 1 > argc) {
					return MEA_ERROR;
				}
				entry.L4src_min = MEA_OS_atoiNum(argv[i + 1]);
				entry.L4src_max = MEA_OS_atoiNum(argv[i + 2]);
			}
			if (!MEA_OS_strcmp(argv[i], "-L4dest")) {
				if (i + 1 > argc) {
					return MEA_ERROR;
				}
				entry.L4dest_min = MEA_OS_atoiNum(argv[i + 1]);
				entry.L4dest_max = MEA_OS_atoiNum(argv[i + 2]);
			}
			if (!MEA_OS_strcmp(argv[i], "-DSCP")) {
				if (i + 1 > argc) {
					return MEA_ERROR;
				}
				entry.dscp_min = MEA_OS_atoiNum(argv[i + 1]);
				entry.dscp_max = MEA_OS_atoiNum(argv[i + 2]);
			}
		}


		

	if (MEA_API_ACL5_Range_Mask_Prof_Set(MEA_UNIT_0, Id, index, &entry) != MEA_OK) {
		CLI_print("Error MEA_API_ACL5_Range_Mask_Prof_Set failed \n");
		return MEA_OK;
	}

	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Range_Mask_Prof_Delete(int argc, char *argv[])
{

	MEA_ACL5_t Id;
	MEA_ACL5_t from_Id;
	MEA_ACL5_t to_Id;
	MEA_Bool exist;
	if (argc < 2) {
		return MEA_ERROR;
	}


	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_RANGEMASK_MAX_PROF -1;

	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("Error  MEA_API_ACL5_Range_Mask_Prof_isExist failed  profile Id=%d\n", Id);
			return MEA_OK;
		}
		if (!exist) {
			CLI_print("Error  MEA_API_ACL5_Range_Mask_Prof_isExist  profile Id %d is not exist\n", Id);
			return MEA_OK;
		}
	}


	for (Id = from_Id; Id <= to_Id; Id++)
	{
		exist = MEA_FALSE;

		if (MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;

		if (!exist)
			continue;



		if (MEA_API_ACL5_Range_Mask_Prof_Delete(MEA_UNIT_0, Id) != MEA_OK)
		{
			CLI_print("Error  MEA_API_ACL5_Range_Mask_Prof_Delete failed to delete %d\n", Id);
			continue;
		}

		CLI_print("Done: %d \n", Id);
	}


	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Range_Mask_Index_Delete(int argc, char *argv[])
{

	MEA_ACL5_t Id;
	MEA_Uint16 index;
	
	MEA_Bool exist;
	if (argc < 2) {
		return MEA_ERROR;
	}


	Id = MEA_OS_atoiNum(argv[1]);
	index = MEA_OS_atoiNum(argv[2]);
	
		if (MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("Error  MEA_API_ACL5_Range_Mask_Prof_isExist failed  profile Id=%d\n", Id);
			return MEA_OK;
		}
	
		if (MEA_API_ACL5_Range_Mask_Prof_isExistIndex(MEA_UNIT_0, Id,index, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("Error  MEA_API_ACL5_Range_Mask_Prof_isExistIndex failed  Index id=%d\n", index);
			return MEA_OK;
		}


				if (MEA_API_ACL5_Range_Mask_Index_Delete(MEA_UNIT_0, Id, index) != MEA_OK)
				{
					CLI_print("Error  MEA_API_ACL5_Range_Mask_Index_Delete failed to delete \n");
					return MEA_ERROR;
				}

			
	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Range_Mask_Prof_Get(int argc, char *argv[])
{
	MEA_Uint16 Id;
	MEA_Uint16 index;
	MEA_Uint16 from_Id;
	MEA_Uint16 to_Id;
	MEA_Uint16 from_index;
	MEA_Uint16 to_index;

	MEA_ACL5_Range_Mask_data_dbt entry;
	MEA_Uint32 count = 0;

	MEA_Bool exist;


	if (argc < 2) {
		return MEA_ERROR;
	}


	if (MEA_OS_strcmp(argv[1], "all") == 0 && MEA_OS_strcmp(argv[2], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_RANGEMASK_MAX_PROF -1;
		from_index = 0;
		to_index = MEA_ACL5_RANGEMASK_MAX_BOLOCK-1 ;
	}
	else if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_RANGEMASK_MAX_PROF -1;
		from_index = MEA_OS_atoiNum(argv[2]);
		to_index = MEA_OS_atoiNum(argv[2]);
	}
	else if (MEA_OS_strcmp(argv[2], "all") == 0)
	{
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		if (from_Id == 0)
		{
			CLI_print("Error: Invalid ID \n");
			return MEA_ERROR;
		}
		from_index = 0;  
		to_index = MEA_ACL5_RANGEMASK_MAX_BOLOCK-1 ;
	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		from_index = MEA_OS_atoiNum(argv[2]);
		to_index = MEA_OS_atoiNum(argv[2]);
		Id = from_Id;
		index = from_index;
		if (from_Id == 0)
		{
			CLI_print("Error: Invalid ID \n");
			return MEA_ERROR;
		}

		if (MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("ERROR: MEA_API_ACL5_Range_Mask_Prof_IsExist id: %d index: %d\n", Id, index);
		}
		if (!exist) {
			CLI_print("ACL5 range mask profile %d is not exist \n", Id);
			return MEA_OK;
		}
	}

	

	CLI_print("-------------------------------------------\n");
	CLI_print("         Range Mask Profile                \n");
	CLI_print("-------------------------------------------\n");

	for (Id = from_Id; Id <= to_Id; Id++)
	{
		for (index = from_index; index <= to_index; index++)
		{
			if (MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
				continue;
			if (!exist)
				continue;
			if (MEA_API_ACL5_Range_Mask_Prof_isExistIndex(MEA_UNIT_0, Id, index, MEA_TRUE, &exist) != MEA_OK)
				continue;
			if (!exist)
				continue;

			if (MEA_API_ACL5_Range_Mask_Prof_Get(MEA_UNIT_0, Id, index, &entry) != MEA_OK) {
				return MEA_ERROR;
			}
			if (count == 0)
			{
				CLI_print("----------------------------------------------------------------   \n");

				CLI_print(" Id  index    priVal   L4src    L4src   L4dest    L4dest  DSCP    DSCP	   \n"
					  	  "                       min      max     min       max     min     max       \n");
				CLI_print(" --- ------  -------  -------  ------   ------	-------  ------  -------   \n");
			}

			count++;


			CLI_print("%3d %5d %6d %8d %8d %9d %8d %7d %5d \n",
				Id,index,
				entry.priValue,
				entry.L4src_min,
				entry.L4src_max,
				entry.L4dest_min,
				entry.L4dest_max,
				entry.dscp_min,
				entry.dscp_max);

		}
	
	}

	CLI_print("----------------------------------------------------------------   \n");

	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Range_Mask_Prof_IsExit(int argc, char *argv[])
{




	MEA_Uint16 Id;
	MEA_Uint16 from_Id;
	MEA_Uint16 to_Id;
	MEA_Bool exist;
	MEA_Uint32 count = 0;

	if (argc <= 1)
		return MEA_ERROR;


	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_RANGEMASK_MAX_PROF -1;
	}
	else
	{
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (Id == 0)
		{
			CLI_print("Error: Invalid ID \n");
			return MEA_ERROR;
		}
	}


	for (Id = from_Id; Id <= to_Id; Id++)
	{

		if (MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			continue;

		}

		if (count == 0)
		{

			CLI_print("   Id  Exist    \n");
			CLI_print(" ----- -------  \n");
		}

		count++;

		CLI_print(" %5d %s \n", Id, exist ? "YES" : "NO");

	}

	return MEA_OK;
}
#if 0
static MEA_Status MEA_CLI_ACL5_Range_Mask_Prof_GetFirst(int argc, char *argv[])
{
	MEA_Uint16 Id;
	MEA_Uint16 index;
	MEA_Bool exist;
	MEA_ACL5_Range_Mask_data_dbt entry;

	MEA_Bool found_o = MEA_FALSE;

	MEA_UNUSED_PARAM(argc);
	MEA_UNUSED_PARAM(argv[1]);

	if (argc < 2) {
		return MEA_ERROR;
	}

	for (Id = 1; Id <= MEA_ACL5_IPMASK_MAX_PROF; Id++)
	{
		for (index = 1; index <= MEA_ACL5_RANGEMASK_MAX_BOLOCK; index++)
		{
			if (MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
				continue;
			if (!exist)
				continue;

			if (MEA_API_ACL5_Range_Mask_Prof_GetFirst(MEA_UNIT_0, &Id, &index,  &found_o, &entry ) != MEA_OK) {
				return MEA_ERROR;
			}



			CLI_print("----------------------------------------------------------------   \n");

			CLI_print(" Id  index    priVal   L4src    L4src   L4dest    L4dest  DSCP    DSCP	  \n"
					  "						  min	   max     min		 max	 min	 max		  \n");
			CLI_print(" --- ------  -------  -------  ------   ------	-------  ------  -------   \n");

			CLI_print(" %5d %5d %5d %10d %10d %10d %10d %5d %5d \n",
				Id, index,
				entry.priValue,
				entry.L4src_min,
				entry.L4src_max,
				entry.L4dest_min,
				entry.L4dest_max,
				entry.dscp_min,
				entry.dscp_max);
			break;
		}
	}
	return MEA_OK;
}
#endif
static MEA_Status MEA_CLI_ACL5_Range_Mask_Prof_GetNext(int argc, char *argv[])
{
	MEA_ACL5_t Id;
	MEA_Uint16 index;
	MEA_Bool exist;
	MEA_ACL5_Range_Mask_data_dbt entry;
	MEA_Bool is_found;
	MEA_Bool found_o = MEA_FALSE;

	if (argc < 2) {
		return MEA_ERROR;
	}
	is_found = MEA_FALSE;
	for (Id = MEA_OS_atoiNum(argv[1]); Id < MEA_ACL5_RANGEMASK_MAX_PROF - 1; Id++)
	{
		if (is_found == MEA_TRUE)
			break;
		for (index = MEA_OS_atoiNum(argv[2]); index < MEA_ACL5_RANGEMASK_MAX_BOLOCK - 1; index++)
		{
			if (MEA_API_ACL5_Range_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
				continue;
			if (!exist)
				continue;
			if (MEA_API_ACL5_Range_Mask_Prof_isExistIndex(MEA_UNIT_0, Id, index, MEA_TRUE, &exist) != MEA_OK)
				continue;
			if (!exist)
				continue;

			if (Id == MEA_ACL5_RANGEMASK_MAX_PROF && index == MEA_ACL5_RANGEMASK_MAX_BOLOCK)
			{
				CLI_print("id %5d with index %5d is the last \n", Id, index);
				break;
			}

			if (MEA_API_ACL5_Range_Mask_Prof_GetNext(MEA_UNIT_0, &Id,&index,  &found_o, &entry) != MEA_OK) {
				return MEA_ERROR;
			}

			


			CLI_print("----------------------------------------------------------------   \n");

			CLI_print(" Id  index    priVal   L4src    L4src   L4dest    L4dest  DSCP    DSCP	   \n"
				"                       min      max     min       max     min     max       \n");
			CLI_print(" --- ------  -------  -------  ------   ------	-------  ------  -------   \n");

			CLI_print("%3d %5d %6d %8d %8d %9d %8d %7d %5d \n",
				Id, index,
				entry.priValue,
				entry.L4src_min,
				entry.L4src_max,
				entry.L4dest_min,
				entry.L4dest_max,
				entry.dscp_min,
				entry.dscp_max);
			is_found = MEA_TRUE;
			break;
		}
	}
	return MEA_OK;
}
#if 0
static MEA_Status MEA_CLI_ACL5_Range_Mask_Prof_GetmaxOf_Index(int argc, char *argv[])
{
	
	MEA_Uint16 index;
	MEA_Bool found_o = MEA_FALSE;
	MEA_UNUSED_PARAM(found_o);
	MEA_UNUSED_PARAM(argv[1]);

	if (argc < 2) {
		return MEA_ERROR;
	}

	MEA_API_ACL5_Range_Mask_Prof_GetmaxOf_Index(MEA_UNIT_0, &index);


	CLI_print("------------------------- \n");
	CLI_print("index: %5d \n", index);
	CLI_print("------------------------- \n");

	
	return MEA_OK;
}
#endif
MEA_Status MEA_CLI_ACL5_Range_Mask_Prof_AddUserCmds(void)
{
	CLI_defineCmd("MEA ACL5 Mask_profile Range create",
		(CmdFunc)MEA_CLI_ACL5_Range_Mask_Prof_Create,
		"create   profile\n",
		"create   profile \n"
		"    -f                        \n"
		"       <Id>      forced id (1...511)  \n"
		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Range create -f 1  \n"
		"           \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Range set",
		(CmdFunc)MEA_CLI_ACL5_Range_Mask_Set,
		"set   profile\n",
		"set   profile \n"
		"    -f <Id>   forced id (0...511)  \n"
		"       <index>   forced index (0...31)  \n"
		"   -priValue <value> \n"
		"   -L4src	<L4src_min> <L4src_max >  \n"
		"   -L4dest	<L4dest_min> <L4dest_max >  \n"
		"   -DSCP	<DSCP_min> <DSCP_max >  \n"
		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Range set -f 1 5 -priValue 1  -L4src 1111 2222 -L4dest 3333 4444  -DSCP 3 16 \n"
		"           \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Range show",
		(CmdFunc)MEA_CLI_ACL5_Range_Mask_Prof_Get,
		"show   ACL5 Range Mask_profile \n",
		"show   ACL5 Range Mask_profile \n"
		"    <Id>  all/Id \n"
		"	<index> all/index \n"
		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Range show 1 10  \n"
		"       ACL5 Mask_profile Range show 1 all  \n"
		"       ACL5 Mask_profile Range show all 1  \n"
		"       ACL5 Mask_profile Range show all all  \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Range delete profile",
		(CmdFunc)MEA_CLI_ACL5_Range_Mask_Prof_Delete,
		"delete   ACL5 Range Mask_profile delete profile\n",
		"delete   ACL5 Range Mask_profile delete profile\n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Range delete profile 1    \n"
		"       ACL5 Mask_profile Range delete profile all   \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Range delete index",
		(CmdFunc)MEA_CLI_ACL5_Range_Mask_Index_Delete,
		"delete   ACL5 Range Mask_profile delete Index\n",
		"delete   ACL5 Range Mask_profile delete Index\n"
		"    <Id> <Index> \n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Range delete Index 1 2    \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Range IsExist",
		(CmdFunc)MEA_CLI_ACL5_Range_Mask_Prof_IsExit,
		"IsExist   ACL5 Range Mask_profile IsExist\n",
		"IsExist   ACL5 Range Mask_profile IsExist\n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Range IsExist 1    \n"
		"       ACL5 Mask_profile Range IsExist all   \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Range GetNext",
		(CmdFunc)MEA_CLI_ACL5_Range_Mask_Prof_GetNext,
		"GetNext   ACL5 Range Mask_profile \n",
		"GetNext   ACL5 Range Mask_profile \n"
		"    <Id>  Id \n"
		"	<index> index \n"
		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Range GetNext 1 7    \n"

	);

	return MEA_OK;

}


/************************************************************************/
/*				ACL5 Key Mask  profile						            */
/************************************************************************/


static MEA_Status MEA_CLI_ACL5_Key_Mask_Prof_Create(int argc, char *argv[])
{
	MEA_Uint16 i;
	MEA_ACL5_t Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_ACL5_KeyMask_dbt entry;
	MEA_Uint32 enable = 1;


	if (argc < 2) {
		return MEA_ERROR;
	}

	MEA_OS_memset(&entry, 0, sizeof(entry));

	for (i = 0; i < argc; i++) {
		if (!MEA_OS_strcmp(argv[i], "-f")) {
			if (i + 1 > argc) {
				return MEA_ERROR;
			}
			Id = MEA_OS_atoiNum(argv[i + 1]);
			if (Id == 0)
			{
				CLI_print("Error: Invalid ID \n");
				return MEA_ERROR;
			}
		}
			
	
			if (!MEA_OS_strcmp(argv[i], "-TAG1")) 
				entry.TAG1 = enable;

			if (!MEA_OS_strcmp(argv[i], "-TAG1_p")) 
				entry.TAG1_p = enable;

			if (!MEA_OS_strcmp(argv[i], "-TAG2")) 
				entry.TAG2 = enable;

			if (!MEA_OS_strcmp(argv[i], "-TAG3"))
				entry.TAG3 = enable;

			if (!MEA_OS_strcmp(argv[i], "-L3_EtherType")) 
				entry.L3_EtherType = enable;
			
			if (!MEA_OS_strcmp(argv[i], "-DSCP_TC")) 
				entry.DSCP_TC = enable;
			
			if (!MEA_OS_strcmp(argv[i], "-Ip_Protocol_NH")) 
				entry.Ip_Protocol_NH = enable;

			if (!MEA_OS_strcmp(argv[i], "-IPv6FlowLabel")) 
				entry.IPv6FlowLabel = enable;

			if (!MEA_OS_strcmp(argv[i], "-L4src")) 
				entry.L4src = enable;

			if (!MEA_OS_strcmp(argv[i], "-L4dst")) 
				entry.L4dst = enable;

			if (!MEA_OS_strcmp(argv[i], "-SID")) 
				entry.SID = enable;

			if (!MEA_OS_strcmp(argv[i], "-IPv4_Flags")) 
				entry.IPv4_Flags = enable;

			if (!MEA_OS_strcmp(argv[i], "-TCP_Flags")) 
				entry.TCP_Flags = enable;
			
		}
	
	if (MEA_API_ACL5_Key_Mask_Prof_Create(MEA_UNIT_0, &Id, &entry) != MEA_OK) {
		CLI_print("Error MEA_API_ACL5_Key_Mask_Prof_Create failed \n");
		return MEA_OK;
	}

	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Key_Mask_Set(int argc, char *argv[])
{

	MEA_Uint16 Id = MEA_PLAT_GENERATE_NEW_ID;
	MEA_ACL5_KeyMask_dbt entry;



	if (argc < 2) {
		return MEA_ERROR;
	}

	Id = MEA_OS_atoiNum(argv[1]);

	CLI_print("ACL5_Key_Mask_Prof_Modify not supported \n");

	if (MEA_API_ACL5_Key_Mask_Prof_Set(MEA_UNIT_0, Id, &entry) != MEA_OK)
	{
		CLI_print("Error MEA_API_ACL5_Key_Mask_Prof_Modify failed \n");
		return MEA_OK;
	}


	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Key_Mask_Prof_Delete(int argc, char *argv[])
{

	MEA_ACL5_t Id;
	MEA_ACL5_t from_Id;
	MEA_ACL5_t to_Id;
	MEA_Bool exist;
	if (argc < 2) {
		return MEA_ERROR;
	}


	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_KEYMASK_MAX_PROF -1;

	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (Id == 0)
		{
			CLI_print("Error: Invalid ID \n");
			return MEA_ERROR;
		}
		if (MEA_API_ACL5_Key_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("Error  MEA_API_ACL5_IP_Mask_Prof_isExist failed  MaskId=%d\n", Id);
			return MEA_OK;
		}
	}


	for (Id = from_Id; Id <= to_Id; Id++)
	{
		exist = MEA_FALSE;

		if (MEA_API_ACL5_Key_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;

		if (!exist)
			continue;



		if (MEA_API_ACL5_Key_Mask_Prof_Delete(MEA_UNIT_0, Id) != MEA_OK)
		{
			CLI_print("Error  MEA_API_ACL5_Key_Mask_Prof_Delete failed to delete %d\n", Id);
			continue;
		}

		CLI_print("Done: %d \n", Id);
	}


	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Key_Mask_Prof_Get(int argc, char *argv[])
{
	MEA_Uint16 Id;
	MEA_Uint16 from_Id;
	MEA_Uint16 to_Id;

	MEA_ACL5_KeyMask_dbt entry;
	MEA_Uint32 count = 0;

	MEA_Bool exist;


	if (argc < 2) {
		return MEA_ERROR;
	}

	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_IPMASK_MAX_PROF -1;
	}
	else {
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (Id == 0)
		{
			CLI_print("Error: Invalid ID \n");
			return MEA_ERROR;
		}
		if (MEA_API_ACL5_Key_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			CLI_print("ERROR: MEA_API_ACL5_Key_Mask_Prof_IsExist %d\n", Id);
		}
		if (!exist) {
			CLI_print("ACL5 key mask profile %d is not exist \n", Id);
			return MEA_OK;
		}
	}

	CLI_print("-------------------------------------------\n");
	CLI_print("         Key Mask Profile                  \n");
	CLI_print("-------------------------------------------\n");

	for (Id = from_Id; Id <= to_Id; Id++)
	{
		if (MEA_API_ACL5_Key_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;
		if (!exist)
			continue;




		if (MEA_API_ACL5_Key_Mask_Prof_Get(MEA_UNIT_0, Id, &entry) != MEA_OK) {
			return MEA_ERROR;
		}
		if (count == 0)
		{
			CLI_print("---------------------------------------------------------------------------------------------------------------------------	\n");

			CLI_print(" Id    TAG1  TAG1_p  TAG2  TAG3    L3     DSCP     Ip	     IPv6    L4src  L4dst  SID    IPv4   TCP    		\n"
					  "                                   EType  TC	 Protocol    Flow                         Flags  Flags      \n"
					  "                                                   NH	     Label                                              \n");
			CLI_print(" --- ------- ------ ------ -----  ------ ------  ---------   -------  ------ ------ ----  ----- ------ 	\n");
		}

		count++;


		CLI_print("%5d %5d %5d   %5d %5d   %5d %5d   %7d %9d   %5d   %5d %4d %5d   %5d \n", Id,
			entry.TAG1,
			entry.TAG1_p,
			entry.TAG2,
			entry.TAG3,
			entry.L3_EtherType,
			entry.DSCP_TC,
			entry.Ip_Protocol_NH,
			entry.IPv6FlowLabel,
			entry.L4src,
			entry.L4dst,
			entry.SID,
			entry.IPv4_Flags,
			entry.TCP_Flags);
	}

	CLI_print("--------------------------------------------------------------------------------------------------------------------------	\n");

	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Key_Mask_Prof_IsExit(int argc, char *argv[])
{
	MEA_Uint16 Id;
	MEA_Uint16 from_Id;
	MEA_Uint16 to_Id;
	MEA_Bool exist;
	MEA_Uint32 count = 0;

	if (argc <= 1)
		return MEA_ERROR;

	if (MEA_OS_strcmp(argv[1], "all") == 0) {

		from_Id = 1;
		to_Id = MEA_ACL5_IPMASK_MAX_PROF -1 ;
	}
	else
	{
		from_Id = MEA_OS_atoiNum(argv[1]);
		to_Id = MEA_OS_atoiNum(argv[1]);
		Id = from_Id;
		if (Id == 0)
		{
			CLI_print("Error: Invalid ID \n");
			return MEA_ERROR;
		}
	}


	for (Id = from_Id; Id <= to_Id; Id++)
	{

		if (MEA_API_ACL5_Key_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_FALSE, &exist) != MEA_OK) {
			continue;

		}

		if (count == 0)
		{

			CLI_print("   Id  Exist    \n");
			CLI_print(" ----- -------  \n");
		}

		count++;

		CLI_print(" %5d %s \n", Id, exist ? "YES" : "NO");

	}

	return MEA_OK;

}

static MEA_Status MEA_CLI_ACL5_Key_Mask_Prof_GetFirst(int argc, char *argv[])
{
	MEA_ACL5_t Id;
	MEA_Bool exist;
	MEA_ACL5_KeyMask_dbt entry;

	MEA_Bool found_o = MEA_FALSE;

	if (argc < 2) {
		return MEA_ERROR;
	}

	for (Id = 1; Id < MEA_ACL5_KEYMASK_MAX_PROF; Id++)
	{
		if (MEA_API_ACL5_Key_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;
		if (!exist)
			continue;

		if (MEA_API_ACL5_Key_Mask_Prof_GetFirst(MEA_UNIT_0, &Id, &entry, &found_o) != MEA_OK) {
			return MEA_ERROR;
		}




		CLI_print("---------------------------------------------------------------------------------------------------------------------------------------	\n");

		CLI_print(" Id    TAG1  TAG1_p  TAG2  TAG3  L3     DSCP     Ip		   IPv6	   L4src  L4dst  SID  IPv4   TCP   		\n"
			"                                       EType  TC	    Protocol      Flow                        Flags  Flags      \n"
			"                                                       NH		   Label                                              \n");
		CLI_print(" --- ------- ------ ------ ----- ----- ------ ------  ---------   -------  ------ ------ ----  -----  ----- --------	\n");

		CLI_print("%5d %5d %5d   %5d %5d   %5d %5d   %5d %5d       %5d   %5d   %5d %5d   %5d %5d %5d\n", Id,
			entry.TAG1,
			entry.TAG1_p,
			entry.TAG2,
			entry.TAG3,
			entry.L3_EtherType,
			entry.DSCP_TC,
			entry.Ip_Protocol_NH,
			entry.IPv6FlowLabel,
			entry.L4src,
			entry.L4dst,
			entry.SID,
			entry.IPv4_Flags,
			entry.TCP_Flags);
		break;
	}

	return MEA_OK;
}

static MEA_Status MEA_CLI_ACL5_Key_Mask_Prof_GetNext(int argc, char *argv[])
{
	MEA_ACL5_t Id;
	MEA_Bool exist;
	MEA_ACL5_KeyMask_dbt entry;

	MEA_Bool found_o = MEA_FALSE;

	if (argc < 2) {
		return MEA_ERROR;
	}

	for (Id = MEA_OS_atoiNum(argv[1]); Id < MEA_ACL5_KEYMASK_MAX_PROF -1; Id++)
	{
		if (MEA_API_ACL5_Key_Mask_Prof_isExist(MEA_UNIT_0, Id, MEA_TRUE, &exist) != MEA_OK)
			continue;
		if (!exist)
			continue;

		if (Id == MEA_ACL5_KEYMASK_MAX_PROF)
		{
			CLI_print("%5d is the last \n", Id);
			break;
		}

		if (MEA_API_ACL5_Key_Mask_Prof_GetNext(MEA_UNIT_0, &Id, &entry, &found_o) != MEA_OK) {
			return MEA_ERROR;
		}



		CLI_print("---------------------------------------------------------------------------------------------------------------------------------------	\n");

		CLI_print(" Id  TAG1  TAG1_p  TAG2  TAG3  L3     DSCP     Ip		   IPv6	   L4src  L4dst  SID  IPv4   TCP  		\n"
			"                                     EType  TC	    Protocol      Flow                        Flags  Flags     \n"
			"                                                     NH		   Label                                             \n");
		CLI_print(" --- ----- ------ ------ ----- ----- ------ ------  ---------   -------  ------ ------ ----  ----- ----- --------	\n");

		CLI_print("%5d %5d %5d   %5d %5d   %5d %5d   %5d %5d       %5d   %5d   %5d %3d   %5d %5d %5d %5d\n", Id,
			entry.TAG1,
			entry.TAG1_p,
			entry.TAG2,
			entry.TAG3,
			entry.L3_EtherType,
			entry.DSCP_TC,
			entry.Ip_Protocol_NH,
			entry.IPv6FlowLabel,
			entry.L4src,
			entry.L4dst,
			entry.SID,
			entry.IPv4_Flags,
			entry.TCP_Flags);
		break;
	}

	return MEA_OK;
}

MEA_Status MEA_CLI_ACL5_Key_Mask_Prof_AddUserCmds(void)
{
	CLI_defineCmd("MEA ACL5 Mask_profile Key create",
		(CmdFunc)MEA_CLI_ACL5_Key_Mask_Prof_Create,
		"create   profile\n",
		"create   profile \n"
		"    -f <Id>   forced id\n"
		"    -TAG1				 \n"
		"    -TAG1_p			 \n"
		"    -TAG2				 \n"
		"    -TAG3				 \n"
		"    -L3_EtherType         \n"
		"    -DSCP_TC			 \n"
		"    -Ip_Protocol_NH       \n"
		"    -IPv6FlowLabel        \n"
		"    -L4src                \n"
		"    -L4dst                \n"
		"    -SID				 \n"
		"    -IPv4_Flags           \n"
		"    -TCP_Flags            \n"
		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Key create -f 1 -MAC_DA -MAC_SA -TAG1  \n"
		"           \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Key set",
		(CmdFunc)MEA_CLI_ACL5_Key_Mask_Set,
		"set   profile\n",
		"set   profile \n"
		"not support \n"
		/*"    <Id>  \n"
		"   -IPv4_mask <valid> <source mask> <dest mask>\n"
		"   -IPv6_mask <valid> <source mask> <dest mask> \n"
		"---------------------------------------------------\n"
		"Examples: \n"
		"       sflow set modify 1  -IPv4_mask 0 16 16 -IPv6_mask 1 24 24   \n"*/
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Key show",
		(CmdFunc)MEA_CLI_ACL5_Key_Mask_Prof_Get,
		"show   ACL5 Key Mask_profile \n",
		"show   ACL5 Key Mask_profile \n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Key show 1    \n"
		"       ACL5 Mask_profile Key show all   \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Key delete",
		(CmdFunc)MEA_CLI_ACL5_Key_Mask_Prof_Delete,
		"delete   ACL5 Key Mask_profile delete\n",
		"delete   ACL5 Key Mask_profile delete\n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Key delete 1    \n"
		"       ACL5 Mask_profile Key delete all   \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Key IsExist",
		(CmdFunc)MEA_CLI_ACL5_Key_Mask_Prof_IsExit,
		"IsExist   ACL5 Key Mask_profile \n",
		"IsExist   ACL5 Key Mask_profile \n"
		"    <Id>  all/Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Key IsExist 1    \n"
		"       ACL5 Mask_profile Key IsExist all   \n"
	);

	CLI_defineCmd("MEA ACL5 Mask_profile Key GetFirst",
		(CmdFunc)MEA_CLI_ACL5_Key_Mask_Prof_GetFirst,
		"GetFirst   ACL5 Key Mask_profile \n",
		"GetFirst   ACL5 Key Mask_profile \n"
		"    all\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Key GetFirst all    \n"

	);

	CLI_defineCmd("MEA ACL5 Mask_profile Key GetNext",
		(CmdFunc)MEA_CLI_ACL5_Key_Mask_Prof_GetNext,
		"GetNext   ACL5 Key Mask_profile \n",
		"GetNext   ACL5 Key Mask_profile \n"
		"    <Id>  Id\n"

		"---------------------------------------------------\n"
		"Examples: \n"
		"       ACL5 Mask_profile Key GetNext 1    \n"

	);

	return MEA_OK;

}



