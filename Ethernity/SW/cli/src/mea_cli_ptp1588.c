/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_cli_ptp1588.h"




static MEA_Status MEA_CLI_PTP1588_Show_info_fifo(int argc, char *argv[])
{


    MEA_Uint32 count;
    


    MEA_Port_t index, first_index, last_index;
    MEA_Ptp1588_statusTx_dbt status_entry;
    MEA_Ptp1588_info_fifo_dbt entry;
    MEA_Uint16 instatce, instatce_first, instatce_last;

   

    char buf[500];
    char *up_ptr;

  



    if (argc < 2) {
        return MEA_ERROR;
    }

    instatce = 0;



    if ( argc < 3)
        return MEA_ERROR;

    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        instatce_first = 0;
        instatce_last = MEA_INSTANCE_FIFO1588;
    } else {

        //last_port = first_port = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            instatce_first = MEA_OS_atoi(buf);
            instatce_last = MEA_OS_atoi(up_ptr);
        }
        else
        {
            instatce = instatce_first = instatce_last = MEA_OS_atoi(buf);
        }

    }


    if (MEA_OS_strcmp(argv[2], "all") == 0) {
        first_index = 1;
        last_index = 511;
    }
    else {

        //last_port = first_port = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if ((up_ptr = strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_index = MEA_OS_atoi(buf);
            last_index = MEA_OS_atoi(up_ptr);
        }
        else
        {
            index = first_index = last_index = MEA_OS_atoi(buf);
        }

    }



    MEA_OS_memset(&entry,0,sizeof(entry));

    for (instatce = instatce_first; instatce < instatce_last; instatce++)
    {
        if (MEA_API_Get_PTP1588_Info_statusFifo(MEA_UNIT_0, instatce, &status_entry) != MEA_OK){
            return MEA_ERROR;
        }
        if (status_entry.val.empty){
            CLI_print(" The ptp1588 indicate as empty\n");
            return MEA_OK;
        }
        if (status_entry.val.overflow_fifo){
            CLI_print(" The ptp1588 indicate as overflow\n");

        }
        if (status_entry.val.num_of_words){
            CLI_print(" The ptp1588 indicate num of words %d\n", status_entry.val.num_of_words);

        }
        else{
            CLI_print(" The ptp1588 indicate num of words %d\n", status_entry.val.num_of_words);
            return MEA_OK;
        }

        count = 0;

        for (index = 1; index <= status_entry.val.num_of_words; index++) {


            if (MEA_API_Get_PTP1588_Info_ReadFifo(MEA_UNIT_0, instatce,&entry) != MEA_OK){
                return MEA_ERROR;

            }

            if (count++ == 0) {
                CLI_print("index Port ptp Seq     sec    nanoSec     fractional\n"
                    "---- ---- ---------- ------ ----------- -----------   \n");
            }
            CLI_print("%4d %4d %10d %6d %10d %10d\n", index,
                entry.portId,
                entry.seq_number,
                entry.timestemp.sec,
                entry.timestemp.nSec,
                entry.timestemp.frac_nano);




        }
        if (count != 0)
            CLI_print("Done: count %d Tx Ts fifo\n", count);
    }

    return MEA_OK;

}




static MEA_Status MEA_CLI_PTP1588_Show(int argc, char *argv[])
{

  
   MEA_Uint32 count;
    int i;
   

    MEA_Port_t port, first_port, last_port;
    MEA_Port_Ptp1588_dbt entry;
    
    MEA_Bool flag_mode=MEA_FALSE;
    MEA_Bool flag_delay=MEA_FALSE;

    char buf[500];
    char *up_ptr;

    char ptp1588[MEA_PTP1588_MODE_LAST+1][30];
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_LAST][0])                       ,"LAST");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_DISABLE][0])                    ,"DISABLE");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_STAMPED][0])                    ,"STAMPED");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_TRANS_E2E][0])                  ,"TRANS E2E");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_TRANS_P2P][0])                  ,"TRANS P2P");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_OC_BC_MASTER_NON_PEER][0])      ,"OC/BC MASTER_NON_PEER");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_OC_BC_MASTER_PEER][0])          ,"OC/BC MASTER PEER");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_OC_BC_SLAVE_NON_PEER][0])       ,"OC/BC SLAVE NON PEER");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_OC_BC_SLAVE_PEER][0])           ,"OC/BC SLAVE PEER");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_TRANS_P2P_OC_SLAVE_PEER][0])    ,"TRANS P2P OC SLAVE_PEER");
    MEA_OS_strcpy(&(ptp1588[MEA_PTP1588_MODE_TRANS_P2P_OC_SLAVE_NON_PEER][0]),"TRANS P2P OC SLAVE NON PEER");



    if (argc < 2) {
        return MEA_ERROR;
    }


    if (MEA_OS_strcmp(argv[1], "all") == 0) {
        first_port = 0;
        last_port = MEA_MAX_PORT_NUMBER;
    } else {

        //last_port = first_port = MEA_OS_atoi(argv[1]);
        MEA_OS_strcpy(buf, argv[1]);
        if((up_ptr=strchr(buf, ':')) != 0)
        {
            (*up_ptr) = 0;
            up_ptr++;
            first_port = MEA_OS_atoi(buf);
            last_port   = MEA_OS_atoi(up_ptr);
        }
        else
        {
            port=first_port = last_port = MEA_OS_atoi(buf);
        }



    }

    
    MEA_OS_memset(&entry,0,sizeof(entry));
   
   
    for(i=1; i<argc ;i++){
       
        
            if(!MEA_OS_strcmp(argv[i],"-mode")){ 

                flag_mode=MEA_TRUE;
            }
            if(!MEA_OS_strcmp(argv[i],"-delay")){ 

                flag_delay=MEA_TRUE;
            }


        

    }

    count=0;
	
    for (port = first_port; port <= last_port; port++) {
    
        if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE,MEA_TRUE)==MEA_FALSE)
            continue;

        if(MEA_API_Get_port_PTP1588(MEA_UNIT_0,port,&entry)!=MEA_OK){
            return MEA_ERROR;

        }
        if(flag_delay){
            if (count++ == 0) {          
                CLI_print ("Port DelayType   sec   nanoSec     fractional\n"
                           "---- ----------- ----- ----------- -----------   \n");
            }
            CLI_print ("%4d %-10s %5d %10d %10d\n",port, "Asymmetry  ",entry.delay[MEA_1588_DELAY_ASYMMETRY].sec,entry.delay[MEA_1588_DELAY_ASYMMETRY].nSec,entry.delay[MEA_1588_DELAY_ASYMMETRY].frac_nano);
            CLI_print ("     %-10s %5d %10d %10d\n",     "Peer2Peer  ",entry.delay[MEA_1588_DELAY_PEER2PEER].sec,entry.delay[MEA_1588_DELAY_PEER2PEER].nSec,entry.delay[MEA_1588_DELAY_PEER2PEER].frac_nano);
            CLI_print ("     %-10s %5d %10d %10d\n",     "Egress     ",entry.delay[MEA_1588_DELAY_EGRESS].sec,   entry.delay[MEA_1588_DELAY_EGRESS].nSec   ,entry.delay[MEA_1588_DELAY_EGRESS].frac_nano);
        }
        if(flag_mode){
            if (count++ == 0) {          
                CLI_print ("Port  mode             \n"
                           "---- ----------------  \n");
            }
            CLI_print ("%4d %d %10s \n",port,entry.ptp1588_Mode,(ptp1588[entry.ptp1588_Mode] ));
                }


    }
    if(count != 0 )
     CLI_print("Done: \n");
    else
        CLI_print("empty \n");

    return MEA_OK;

}


static MEA_Status MEA_CLI_PTP1588_Set(int argc, char *argv[])
{

   int numOfArgInCommand;
    char * ch;
    int i,len;

  MEA_HDC_t port;
  MEA_Port_Ptp1588_dbt    entry;
  

    if (argc < 2) {
        return MEA_ERROR;
    }

   
    
    port= MEA_OS_atoi(argv[1]);

    
    if (MEA_API_Get_IsPortValid(port, MEA_PORTS_TYPE_EGRESS_TYPE, MEA_FALSE) ==MEA_FALSE) {
         CLI_print("Error  IsPortValid\n");
        return MEA_OK;
    }
    MEA_OS_memset(&entry,0,sizeof(entry));
    MEA_API_Get_port_PTP1588(MEA_UNIT_0,port,&entry);
    
    for(i=2; i<argc ;i++){
        if(!MEA_OS_strcmp(argv[i],"-mode")){ 
            numOfArgInCommand = 1;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);
            entry.ptp1588_Mode = MEA_OS_atoi(argv[i+1]);
            
        }

        if(!MEA_OS_strcmp(argv[i],"-asymmetry")){ 
            numOfArgInCommand = 3;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);
            entry.delay[MEA_1588_DELAY_ASYMMETRY].sec = MEA_OS_atoi(argv[i+1]);
            entry.delay[MEA_1588_DELAY_ASYMMETRY].nSec = MEA_OS_atoi(argv[i+2]);
            entry.delay[MEA_1588_DELAY_ASYMMETRY].frac_nano = MEA_OS_atoi(argv[i+3]);

        }
        if(!MEA_OS_strcmp(argv[i],"-p2p")){ 
            numOfArgInCommand = 3;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);
            entry.delay[MEA_1588_DELAY_PEER2PEER].sec = MEA_OS_atoi(argv[i+1]);
            entry.delay[MEA_1588_DELAY_PEER2PEER].nSec = MEA_OS_atoi(argv[i+2]);
            entry.delay[MEA_1588_DELAY_PEER2PEER].frac_nano = MEA_OS_atoi(argv[i+3]);

        }
        if(!MEA_OS_strcmp(argv[i],"-egress")){ 
            numOfArgInCommand = 3;
            ch=argv[i];
            len=(i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i,len,argc,ch);
            entry.delay[MEA_1588_DELAY_EGRESS].sec = MEA_OS_atoi(argv[i+1]);
            entry.delay[MEA_1588_DELAY_EGRESS].nSec = MEA_OS_atoi(argv[i+2]);
            entry.delay[MEA_1588_DELAY_EGRESS].frac_nano = MEA_OS_atoi(argv[i+3]);

        }
       


                
               


        i+=numOfArgInCommand;

    }

    if(MEA_API_Set_port_PTP1588(MEA_UNIT_0,port,&entry)!=MEA_OK){
        CLI_print("error MEA_API_Set_port_PTP1588 for port %d\n",port);
        return MEA_OK;
    }

    
    CLI_print("Done: \n");
    return MEA_OK;

}


MEA_Status MEA_CLI_ptp1588_AddDebugCmds(void)
{

    /**********************************************************/
    /*                    ptp1588 delay                      */
    /**********************************************************/
    CLI_defineCmd("MEA port ptp1588 config set",
        (CmdFunc) MEA_CLI_PTP1588_Set, 
        "set   ptp1588 config -mode/-delay(s)\n",
        "set   port ptp1588 config -mode/-delay  \n"
        "---------------------------------------------------\n"
        "      \n"
        "    -mode <value>\n"
        "              PTP1588_MODE_DISABLE=0 \n" 
        "              PTP1588_MODE_STAMPED=1\n" 
        "              PTP1588_MODE_TRANS_E2E=2\n" 
        "              PTP1588_MODE_TRANS_P2P=3\n" 
        "              PTP1588_MODE_OC_BC_MASTER_NON_PEER=4\n" 
        "              PTP1588_MODE_OC_BC_MASTER_PEER=5\n" 
        "              PTP1588_MODE_OC_BC_SLAVE_NON_PEER=6\n" 
        "              PTP1588_MODE_OC_BC_SLAVE_PEER=7\n" 
        "              PTP1588_MODE_TRANS_P2P_OC_SLAVE_PEER=8\n" 
        "              PTP1588_MODE_TRANS_P2P_OC_SLAVE_NON_PEER=9\n" 


        "    -asymmetry  <sec><nanoSec><practional nano>\n"
        "    -p2p        <sec><nanoSec><practional nano>\n"
        "    -egress     <sec><nanoSec><practional nano>\n"
        "---------------------------------------------------\n"
        "Examples: - set 125 -mode 0  \n"
        "            set 125 -asymmetry 2 125000 0 \n"
        "          - set 125 -p2p      2 100000 0 \n"
        "          - set 125 -egress      2 100000 0 \n");

    CLI_defineCmd("MEA port ptp1588 config show",
        (CmdFunc) MEA_CLI_PTP1588_Show, 
        "Show   ptp1588 mode(s)\n",
        "show <port/all port:port>  \n"
        "---------------------------------------------------\n"
        "    <port> - all or ptp1588 config \n"
        "              -mode -delay       \n"
        "---------------------------------------------------\n"
        "Examples: - show all -mode  \n"
        "          - show  125 -delay\n");

    CLI_defineCmd("MEA port ptp1588 TS show",
        (CmdFunc) MEA_CLI_PTP1588_Show_info_fifo, 
        "Show   ptp1588 Times stamping info(s)\n",
        "show  TS all \n"
        "---------------------------------------------------\n"
        "          all   \n"
        "                        \n"
        "---------------------------------------------------\n"
        "Examples: - show all  \n"
        "          - show all \n");
  

    

    return MEA_OK;
}














