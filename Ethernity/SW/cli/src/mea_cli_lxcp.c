/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "mea_api.h"
#include "mea_lxcp_drv.h"
#include "mea_cli_lxcp.h"
#include "cli_eng.h"
#include "mea_cli.h"



static MEA_Status MEA_CLI_Lxcp_Add(int argc, char *argv[]);
static MEA_Status MEA_CLI_Lxcp_Delete(int argc, char *argv[]);
static MEA_Status MEA_CLI_Lxcp_protocol_set(int argc, char *argv[]);
static MEA_Status MEA_CLI_Lxcp_show(int argc, char *argv[]);

static MEA_Status MEA_CLI_Lxcp_get_lxcp_protocol(MEA_Uint32             val_i,
	                                             MEA_LxCP_Protocol_te  *protocol_o)
{

	if (protocol_o == NULL) {
		CLI_print("Error: protocol_o == NULL\n");
		return MEA_ERROR;
	}

	switch(val_i) {
	case 128 : *protocol_o = MEA_LXCP_PROTOCOL_ARP        ; break;
	case 131 : *protocol_o = MEA_LXCP_PROTOCOL_IGMPoV4       ; break;
	case 135 : *protocol_o = MEA_LXCP_PROTOCOL_DHCP_SERVER; break;
	case 136 : *protocol_o = MEA_LXCP_PROTOCOL_DHCP_CLIENT; break;
	case 255 : *protocol_o = MEA_LXCP_PROTOCOL_OTHER      ; break;
	default : 
		if (val_i >= (MEA_Uint32)MEA_LXCP_PROTOCOL_LAST) {
			CLI_print("Error: LxCP Protocol id %d >= max value (%d) \n",
				      val_i,MEA_LXCP_PROTOCOL_LAST);
		}
		*protocol_o = (MEA_LxCP_Protocol_te)val_i;
		break;
	}
	return MEA_OK;
}




MEA_Status MEA_CLI_LXCP_AddDebugCmds(void)
{





           CLI_defineCmd("MEA Lxcp protocol",
                  (CmdFunc)MEA_CLI_Lxcp_protocol_set, 
                  "Set    LxCP protocol Action\n",
                  "protocol all|<LxCP Id> all|<LxCP Protocol id> <ActionType>\n" 
                  "--------------------------------------------------------------------------------\n"
                 //01234567890123456789012345678901234567890123456789012345678901234567890123456789
               "         all|<LxCP Id>          - Specific LxCP Profile  Id or all\n"
               "         all|<LxCP Protocol Id> - Specific LxCP Protocol Id or all \n"
               "         <LxCP Protocol Id>\n"
               "   0 BPDU 01:80:c2:00:00:00 (STP)                                          \n"
               "   1 BPDU 01:80:c2:00:00:01                                               \n"
               "   2 BPDU 01:80:c2:00:00:02 EtherType 8809                                \n"
               "   3 BPDU 01:80:c2:00:00:03                                               \n"
               "   4 BPDU 01:80:c2:00:00:04                                               \n"
               "   5 BPDU 01:80:c2:00:00:05                                               \n"
               "   6 BPDU 01:80:c2:00:00:06                                               \n"
               "   7 BPDU 01:80:c2:00:00:07 EtherType 88EE                                \n"
               "   8 BPDU 01:80:c2:00:00:08                                               \n"
               "   9 BPDU 01:80:c2:00:00:09                                               \n"
               "  10 BPDU 01:80:c2:00:00:0a                                               \n"
               "  11 BPDU 01:80:c2:00:00:0b                                               \n"
               "  12 BPDU 01:80:c2:00:00:0c                                               \n"
               "  13 BPDU 01:80:c2:00:00:0d EtherType 88f5                                \n"
               "  14 BPDU 01:80:c2:00:00:0e EtherType 88cc                                \n"
               "  15 BPDU 01:80:c2:00:00:0f                                               \n"
               "  16 BPDU 01:80:c2:00:00:10                                               \n"
               "  17 BPDU 01:80:c2:00:00:20 EtherType D.C                                 \n"
               "  18 BPDU 01:80:c2:00:00:21 EtherType D.C                                 \n"
               "  19 BPDU 01:80:c2:00:00:22-->2F                                          \n"
               "  20 BPDU 01:19:A7:00:00:00-->FF                                          \n"
               "  21 BFD-CONTROL  IPv4,UDP,L4 Dest-port=3784,[49152<=SRC<=65535]          \n"
               "  22 BFD-ECO-Loop IPv4,UDP,L4 Dest-port=3785,[49152<=SRC<=65535]          \n"
               "  23 BFD-ECO-ANALYZER IPv4,UDP,L4 Dest-port=3785,[49152<=SRC<=65535]      \n"
               "  24 BFD_ECO_SW_MANGE IPv4=SW-IP,UDP,L4Destport=3785,[49152<=IS-IS 01:80:c2:00:00:14/15  \n"
               "  25 IS-IS 01:80:c2:00:00:14/15                                            \n"
               "  26 ARP Replay                                                            \n"
               "  27 BGP IP & (TCP)(L4_Dest_port) = 179                                    \n"
               "  28 802.1x 0x888E                                                         \n"
               "  29 PIM  IP protocol = 103                                                \n"
               "  30 OSPF                                                                  \n"
               "  33 ARP                                                                   \n"
               "  34 PPPoE DISCOVER                                                        \n"
               "  35 PPP ICP/LCP/PAP                                                       \n"
               "  36 IGMPoV4                                                               \n"
               "  37 ICMPoV4                                                               \n"
               "  38 IPv4                                                                  \n"
               "  39 IPv6                                                                  \n"
               "  40 DHCP SERVER IPv4                                                      \n"
               "  41 DHCP CLIENT IPv4                                                      \n"
               "  42 DNSoIPv4                                                              \n"
               "  43 ICMPoV6                                                               \n"
               "  44 RIP                                                                   \n"
               "  45 DHCP_SERVER_IPv6                                                      \n"
               "  46 VSTP        01:00:0c:cc:cc:cd                                         \n"
               "  47 CDP_VTP     01:00:0c:cc:cc:cc                                         \n"
               "  48 BPDU_TUNNEL 01:00:0c:cd:cd:d0                                         \n"
               "  49 CFM OAM                                                               \n"
               "  50 IEEE1588_PTP_Event L4 Dst port = 319                                  \n"
               "  51 IEEE1588_PTP_General L4 Dst port = 320                                \n"
               "  52 PTP_Primary_over_IPv4  Dst=224.0.1.129                                \n"
               "  53 PTP_Primary_over_IPv6                                                 \n"
               "  54 PTP_Delay_over_IPv4   Dst=224.0.0.107                                 \n"
               "  55 PTP_Delay_over_IPv6                                                   \n"
               "  56 DHCP_CLIENT_IPv6                                                      \n"
               "  57 PPP_Session                                                           \n"
               "  58 VDSL_Vectoring                                                        \n"
               "  59 IPSEC Protocol = 50/51 & L4_dest =500                                 \n"
               "  60 MY_IP ARP/OSPF/RIP/BGP                                                \n"
               "  61 L2TPoE UDP(L4_SRC/DST_PORT=1701)MY_IP                                 \n"
               "  62 DDOS                                                                  \n"
               "  63 OTHER                                                                 \n"
               "         <ActionType>   - 0=Transparent / 1=CPU / 2= ACTION / 3=Discard      \n"
               "--------------------------------------------------------------------------------\n"
               //01234567890123456789012345678901234567890123456789012345678901234567890123456789
               " option\n"
               "  -winwhitelist <valid>\n"
			   "   -action <valid> <id>                   (relevant only at ActionType= ACTION )\n"
               "   -outport <valid> <num of> <clusters..> (relevant only at ActionType= ACTION )\n"
			   "--------------------------------------------------------------------------------\n"
                  "Examples: - protocol 1    36 1\n"
                  "          - protocol 1   all 0\n"
                  "          - protocol 1   all 2 -action 1 520 -outport 1 1 127\n"
                  "          - protocol all all 0\n"
                  "          - protocol all all 1\n");

       CLI_defineCmd("MEA Lxcp show",
                  (CmdFunc)MEA_CLI_Lxcp_show, 
                  "Show   LxCP Profile(s)\n",
                  "show all|<LxCP Id> [<LxCP Protocol Id>]\n"
                  "---------------------------------------------------\n"
                  "    all|<LxCP Id> - all or specific LxCP id \n"
                  "    <protocol>    - LxCP Protocol Id \n"
                  "---------------------------------------------------\n"
                  "Examples: - show all \n"
                  "          - show all 36\n"


                  "          - show 2    \n"
                  "          - show 2   36\n");

    CLI_defineCmd("MEA Lxcp delete",
                  (CmdFunc)MEA_CLI_Lxcp_Delete, 
                  "Delete LxCP Profile(s)\n",
                  "delete all|<id>\n"
                  "--------------------------------------------------------\n"
                  "    all|<id> - delete specific LxCP <id> profile or all \n"
                  "--------------------------------------------------------\n"
                  " Examples: - delete all\n"
                  "           - delete 2\n");


	CLI_defineCmd("MEA Lxcp add",
                  (CmdFunc)MEA_CLI_Lxcp_Add, 
                  "Add    LxCP Profile\n",
                  "add auto|<id>\n"
                  "    [-d <direction>]\n"
                  "-------------------------------------------------------------------------\n"
                  "    auto - generate new Lxcp profile id automatically                    \n"
                  "    <id>   force LxCp profile id                                         \n"
                  "        note: all the protocols will be set to transparent mode          \n"
                  "\n"
                  "    -d (direction option)\n"
                  "         <direction>  - 0=General / 1=Upstream / 2=Downstream\n"
                  "                        Notes: \n"
                  "                         - value 0 should be use in ENET3000 and ENET4000 Upstream\n"
                  "                         - Value 2 need to use in ENET4000 Downstream\n"
                  "-------------------------------------------------------------------------\n"
                  " Examples: - add auto \n"
                  "           - add 2   \n"
                  "           - add 3 -d 2\n");



    return MEA_OK;
}

static MEA_Status MEA_CLI_Lxcp_Add(int argc, char *argv[])
{

    MEA_LxCp_t  LxCpId;
    MEA_LxCP_Entry_dbt lxcp_entry;

    MEA_OS_memset(&lxcp_entry,0,sizeof(lxcp_entry));

    if (argc < 2) {
       return MEA_ERROR;
    }
    if (MEA_OS_strcmp(argv[1],"auto") == 0  ){
        LxCpId = MEA_PLAT_GENERATE_NEW_ID;
    }else{
          LxCpId = (MEA_LxCp_t)MEA_OS_atoi(argv[1]); 
    }
    if (argc >= 4) {
        if (MEA_OS_strcmp(argv[2],"-d") == 0) {
            lxcp_entry.direction = MEA_OS_atoi(argv[3]);
        }
    }


    if(MEA_API_Create_LxCP( MEA_UNIT_0,&lxcp_entry,&LxCpId)!=MEA_OK){
        CLI_print("Error: can't create Lxcp \n");
         return MEA_ERROR;
    }
    CLI_print("Done create Lxcp with Id = %d  \n",LxCpId); 
    return MEA_OK;
}

static MEA_Status MEA_CLI_Lxcp_Delete(int argc, char *argv[])
{
    MEA_LxCp_t  LxCpId ,start_lxcp,end_lxcp;
    MEA_Bool      exist;
    if (argc < 2) {
       return MEA_ERROR;
    }

    if(!MEA_OS_strcmp(argv[1],"all")){
     start_lxcp=2;
     end_lxcp=MEA_LXCP_MAX_PROF-1;
    } else {
        start_lxcp=(MEA_LxCp_t)MEA_OS_atoi(argv[1]);
        end_lxcp=start_lxcp;
        if ((MEA_API_IsExist_LxCP_ById(MEA_UNIT_0,start_lxcp,&exist)!=MEA_OK) ||
            (!exist)) {
            CLI_print(" ERROR: LxCP id %d is not exist \n",start_lxcp);
            return MEA_OK;
        }
    }
    
    for(LxCpId=start_lxcp; LxCpId <=end_lxcp; LxCpId++){    
        if(MEA_API_IsExist_LxCP_ById(MEA_UNIT_0,LxCpId,&exist)==MEA_OK){
            if (exist){
                if(LxCpId==1){
                CLI_print(" ERROR: uset can't Delete LxCP id %d used for driver\n",LxCpId);
                return MEA_OK;
                }
                if(MEA_API_Delete_LxCP(MEA_UNIT_0,LxCpId )!=MEA_OK){
                   CLI_print(" ERROR: can't Delete LxCP id %d \n",LxCpId);
                }
            }
        }
    }
    
    
    return MEA_OK;
}


static MEA_Status MEA_CLI_Lxcp_protocol_set(int argc, char *argv[])
{

    MEA_LxCp_t                          LxCpId;
    MEA_LxCp_t                          start_lxcp,end_lxcp;
    MEA_LxCP_Protocol_key_dbt           key;
    MEA_LxCP_Protocol_data_dbt          data;
    MEA_LxCP_Protocol_te                start_protocol,end_protocol;
    MEA_Bool                            exist;
    MEA_LxCP_Protocol_Action_te         action;
    MEA_Port_t                          port;
    
//    MEA_Uint32                          *pPortGrp;
    int i,ii,extra_param_pos,num_of_out;
    

    if (argc < 4) {
       return MEA_ERROR;
    }
    
    MEA_OS_memset(&data,0,sizeof(data));

    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start_lxcp = 0;
        end_lxcp   = MEA_LXCP_MAX_PROF-1; 
    } else {
        start_lxcp= MEA_OS_atoi(argv[1]);
        end_lxcp=start_lxcp;
        if ((MEA_API_IsExist_LxCP_ById(MEA_UNIT_0,start_lxcp,&exist)!=MEA_OK) ||
            (!exist)) {
            CLI_print(" ERROR: LxCP id %d is not exist \n",start_lxcp);
            return MEA_OK;
        }
    }
    

    if (MEA_OS_strcmp(argv[2],"all") == 0) {
        start_protocol = 0;
        end_protocol   = MEA_LXCP_PROTOCOL_LAST-1; 
    } else {
        if (MEA_CLI_Lxcp_get_lxcp_protocol(MEA_OS_atoi(argv[2]),
		        	                       &start_protocol) != MEA_OK) {
            return MEA_OK;
		}
        if(!MEA_API_IsSupport_LxCP_Protocol(start_protocol)) {
            CLI_print(" ERROR: LxCP protocol %d is not support \n",start_protocol);
            return MEA_OK;
        }
        end_protocol=start_protocol;
    }

    
    
    action=MEA_OS_atoi(argv[3]);
	if ((action != MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT) &&
	    (action != MEA_LXCP_PROTOCOL_ACTION_CPU) &&
        (action != MEA_LXCP_PROTOCOL_ACTION_ACTION) &&
		(action != MEA_LXCP_PROTOCOL_ACTION_DISCARD) ) {
            CLI_print("ERROR: ActionType %d is not valid \n",action);
        return MEA_OK;
    }

    data.action_type=action;
    if(action == MEA_LXCP_PROTOCOL_ACTION_ACTION){
        extra_param_pos=4; 
        if(argc < 6){
            CLI_print("ERROR: Not enough parameters for ACTION_ACTION \n");
            return MEA_ERROR;
        }
        for (i=extra_param_pos; i <  argc ;i++){
			
			if (!strcmp(argv[i], "-winwhitelist")) {
					if (i + 1 >= argc) {
						CLI_print("ERROR: Not enough parameters for option %s\n", argv[i]);
						return MEA_ERROR;
					}
					data.WinWhiteList = MEA_OS_atoi(argv[i + 1]);
			}
            if(!strcmp(argv[i],"-action")){
                if (i+2>=argc){
                    CLI_print("ERROR: Not enough parameters for option %s\n",argv[i]);
                    return MEA_ERROR;
                }
                data.ActionId_valid = MEA_OS_atoi(argv[i+1]);
                data.ActionId =MEA_OS_atoi(argv[i+2]);
                
            }
            if(!MEA_OS_strcmp(argv[i],"-outport")){
                if (i +2 >= argc ){
                    CLI_print("ERROR: Not enough parameters for option %s\n",argv[i]);
                    return MEA_ERROR;
                }
                 
                data.OutPorts_valid = MEA_OS_atoi(argv[i+1]);
                num_of_out=MEA_OS_atoi(argv[i+2]);
                if(i + num_of_out+2 > argc){
                CLI_print("-outport Invalid number of arguments num_of_out \n");
                    return MEA_ERROR;
                }
			    extra_param_pos=i+3;

         //   pPortGrp=(MEA_Uint32 *)(&data.OutPorts.out_ports_0_31);
            

            for ( ii = 0; ii < num_of_out; ii++ ) {
                port = MEA_OS_atoi(argv[extra_param_pos + ii]);

                if (ENET_IsValid_Queue(ENET_UNIT_0,port,ENET_TRUE) != ENET_TRUE) {
                   CLI_print("Cluster id %d is not define \n",port);
                   return MEA_ERROR;
                }

                /* check if the out_port is valid */
                ((ENET_Uint32*)(&(data.OutPorts.out_ports_0_31)))[port/32] |=(1 << (port%32));

            }
        }
        }
//         if ( data.OutPorts_valid == MEA_FALSE && data.ActionId_valid == MEA_TRUE){
//             data.OutPorts_valid=MEA_TRUE;
//         }

    }//ACTION_ACTION

    for (LxCpId=start_lxcp;LxCpId<=end_lxcp;LxCpId++) {

        if ((MEA_API_IsExist_LxCP_ById(MEA_UNIT_0,LxCpId,&exist)!=MEA_OK) ||
            (!exist)) {
           continue;
        }

        for(key.protocol=start_protocol;key.protocol<=end_protocol;key.protocol++)
        {  
           if(!MEA_API_IsSupport_LxCP_Protocol(key.protocol)) {
             continue;
           }

           if(MEA_API_Set_LxCP_Protocol (MEA_UNIT_0,
                                         LxCpId,			      
   			    				         &key,
                                         &data)!=MEA_OK){
   
               CLI_print("ERROR: MEA_API_Set_LxCP_Protocol failed (id=%d,protocol=%d)\n",
                         LxCpId,key.protocol);
           }
        }
    }
    
    return MEA_OK;
}	

static MEA_Status MEA_CLI_Lxcp_show(int argc, char *argv[])
{
    
    MEA_LxCP_Protocol_key_dbt           key;
    MEA_LxCP_Protocol_data_dbt          data;
    MEA_LxCp_t                          start_lxcp,end_lxcp;
    MEA_LxCp_t                          LxCpId;
    MEA_LxCP_Protocol_te                start_protocol,end_protocol;
    MEA_Bool                            exist=MEA_FALSE;
    MEA_Uint32                          i;
    MEA_Uint8                           j;
    MEA_Port_t                          outport;
    MEA_LxCP_Entry_dbt                  lxcp_entry;

    MEA_Bool                            show_title=MEA_FALSE;
    static MEA_Bool                     first=MEA_TRUE;
    static char                         actionType_str[MEA_LXCP_PROTOCOL_ACTION_LAST][14];
    static char                         protocol_str[MEA_LXCP_PROTOCOL_LAST][65];

    if ((argc < 2) || (argc > 3)) {
       return MEA_ERROR;
    }

    if (first) {
        for (i=0;i<MEA_NUM_OF_ELEMENTS(actionType_str);i++) {
            MEA_OS_strcpy(actionType_str[i],"Unknown");
        }
        MEA_OS_strcpy(&(actionType_str[MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT ][0]),"Transparent");
        MEA_OS_strcpy(&(actionType_str[MEA_LXCP_PROTOCOL_ACTION_CPU         ][0]),"Send to Cpu");
        MEA_OS_strcpy(&(actionType_str[MEA_LXCP_PROTOCOL_ACTION_ACTION      ][0]),"Action     ");
        MEA_OS_strcpy(&(actionType_str[MEA_LXCP_PROTOCOL_ACTION_DISCARD     ][0]),"Discard    ");
        

        for (i=0;i<MEA_NUM_OF_ELEMENTS(protocol_str);i++) {
            MEA_OS_strcpy(protocol_str[i],"Unknown");
        }


	    for (j=0;j<=19;j++) {
 		    
            switch(j){
            case 0:
                MEA_OS_sprintf(&(protocol_str[j][0]),
				           "BPDU 01:80:c2:00:00:00 (STP)");
                break;
            case 2:
                MEA_OS_sprintf(&(protocol_str[j][0]),
                    "BPDU 01:80:c2:00:00:02 EtherType 8809");
                break;
            case 7:
                MEA_OS_sprintf(&(protocol_str[j][0]),
                    "BPDU 01:80:c2:00:00:07 EtherType 88EE");
                break;
            case 13:
                MEA_OS_sprintf(&(protocol_str[j][0]),
                    "BPDU 01:80:c2:00:00:0d EtherType 88f5");
                break;
            case 14:
                MEA_OS_sprintf(&(protocol_str[j][0]),
                    "BPDU 01:80:c2:00:00:0e EtherType 88cc");
                break;
            case 17:
                MEA_OS_sprintf(&(protocol_str[j][0]),
                    "BPDU 01:80:c2:00:00:20 EtherType D.C");
                break;
            case 18:
                MEA_OS_sprintf(&(protocol_str[j][0]),
                    "BPDU 01:80:c2:00:00:21 EtherType D.C");
                break;

            default:
                MEA_OS_sprintf(&(protocol_str[j][0]),
                    "BPDU 01:80:c2:00:00:%02x",
                    ((j<=16)? j : 0x20+(j-17)));
                break;
           }
	    } 
 
               
                                                                                          //012345678901234567890123456789012345678901234567890123456789
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IN_BPDU_34_BPDU_47           ][0]), "BPDU 01:80:c2:00:00:22-->2F            ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IN_R_APS                     ][0]), "BPDU 01:19:A7:00:00:00-->FF            ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IN_BFD_CONTROL               ][0]), "BFD-CONTROL  IPv4,UDP,L4 Dest-port=3784, [49152<=SRC<=65535]");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IN_BFD_ECO_LOOP              ][0]), "BFD-ECO-Loop IPv4,UDP,L4 Dest-port=3785, [49152<=SRC<=65535]");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IN_BFD_ECO_ANALYZER          ][0]), "BFD-ECO-ANALYZER IPv4,UDP,L4 Dest-port=3785, [49152<=SRC<=65535]");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IN_BFD_ECO_SW_MANGE          ][0]), "BFD_ECO_SW_MANGE IPv4=Software-IP,UDP,L4 Dest-port=3785, [49152<=SRC<=65535]");

        

        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IN_IS_IS                     ][0]), "IS-IS 01:80:c2:00:00:14/15             ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_ARP_Replay                   ][0]), "ARP Replay                             ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_BGP                          ][0]), "BGP IP & (TCP)(L4_Dest_port) = 179     ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_802_1X                       ][0]), "802.1x 0x888E                          ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IN_PIM                       ][0]), "PIM  IP protocol = 103                 ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IN_OSPF                      ][0]), "OSPF                                   ");
        /*
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_ICMPv6_MLDv2_QUERY           ][0]), "ICMPv6_MLDv2_QUERY                     ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_ICMPv6_MLDv2_REPORT          ][0]), "ICMPv6_MLDv2_REPORT                    ");
        */
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_ARP                          ][0]), "ARP                                    ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_PPPoE_DISCOVERY              ][0]), "PPPoE DISCOVER                         ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_PPPoE_ICP_LCP                ][0]), "PPP ICP/LCP                            ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IGMPoV4                      ][0]), "IGMPoV4                                ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_ICMPoV4                      ][0]), "ICMPoV4                                ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IPV4                         ][0]), "IPv4                                   ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IPV6                         ][0]), "IPv6                                   ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_DHCP_SERVER                  ][0]), "DHCP SERVER IPv4                       ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_DHCP_CLIENT                  ][0]), "DHCP CLIENT IPv4                       ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_DNSoIPv4                     ][0]), "DNSoIPv4                               ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_ICMPoV6                      ][0]), "ICMPoV6                                ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_RIP                          ][0]), "RIP                                    ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_DHCP_SERVER_IPv6             ][0]), "DHCP_SERVER_IPv6                       ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_VSTP                         ][0]), "VSTP        01:00:0c:cc:cc:cd          ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_CDP_VTP                      ][0]), "CDP_VTP     01:00:0c:cc:cc:cc          ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_BPDU_TUNNEL                  ][0]), "BPDU_TUNNEL 01:00:0c:cd:cd:d0          ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_CFM_OAM                      ][0]), "CFM OAM                                ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IEEE1588_PTP_Event           ][0]), "IEEE1588_PTP_Event L4 Dst port = 319   ");    
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IEEE1588_PTP_General         ][0]), "IEEE1588_PTP_General L4 Dst port = 320 ");  
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_PTP_Primary_over_IPv4        ][0]), "PTP_Primary_over_IPv4  Dst=224.0.1.129 "); 
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_PTP_Primary_over_IPv6        ][0]), "PTP_Primary_over_IPv6                  "); 
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_PTP_Delay_over_IPv4          ][0]), "PTP_Delay_over_IPv4   Dst=224.0.0.107  ");   
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_PTP_Delay_over_IPv6          ][0]), "PTP_Delay_over_IPv6                    ");

        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_DHCP_CLIENT_IPv6            ][0]), "DHCP_CLIENT_IPv6                       ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_PPPoE_SESSION               ][0]), "PPP_Session                            ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_VDSL_Vectoring              ][0]), "VDSL_Vectoring                         ");


        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_IPSEC                       ][0]), "IPSEC Protocol = 50/51 & L4_dest =500 ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP      ][0]), "MY_IP ARP/OSPF/RIP/BGP                ");
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_L2TPoE                      ][0]), "L2TPoE UDP(L4_SRC/DST_PORT=1701)MY_IP ");
        
        
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_DDOS                        ][0]), "DDOS                                  ");
        
        MEA_OS_strcpy(&(protocol_str[MEA_LXCP_PROTOCOL_OTHER                       ][0]),"OTHER                        ");
         
        first = MEA_FALSE;
    }

	
    


    

    if (MEA_OS_strcmp(argv[1],"all") == 0) {
        start_lxcp=0;
        end_lxcp=MEA_LXCP_MAX_PROF-1; 
    }else{
        start_lxcp=MEA_OS_atoi(argv[1]);
        end_lxcp=start_lxcp; 
        if ((MEA_API_IsExist_LxCP_ById(MEA_UNIT_0,start_lxcp,&exist)!=MEA_OK) ||
            (!exist)) {
            CLI_print(" ERROR: LxCP id %d is not exist \n",start_lxcp);
            return MEA_OK;
        }
    }

    if (argc < 3){
        start_protocol = 0;
        end_protocol   = MEA_LXCP_PROTOCOL_LAST-1; 
    } else {
        start_protocol= MEA_OS_atoi(argv[2]);
        end_protocol=start_protocol;
        if(!MEA_API_IsSupport_LxCP_Protocol(start_protocol)) {
            CLI_print(" ERROR: LxCP protocol %d is not support \n",start_protocol);
            return MEA_OK;
        }
    }

    for ( LxCpId=start_lxcp; LxCpId <= end_lxcp ;LxCpId++){

        if((MEA_API_IsExist_LxCP_ById(MEA_UNIT_0,LxCpId,&exist)!=MEA_OK) ||
           (!exist)) {
               continue;
        }

        if (MEA_API_Get_LxCP(MEA_UNIT_0,LxCpId,&lxcp_entry) != MEA_OK) {
            CLI_print("ERROR: MEA_API_Get_LxCP failed \n");
            return MEA_OK; 
        }

        for(key.protocol=start_protocol;key.protocol<=end_protocol;key.protocol++)
        {  
            
            if(!MEA_API_IsSupport_LxCP_Protocol(key.protocol)) {
                continue;
            }

            if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0,
                                         LxCpId,
                                         &key,
                                         &data)!=MEA_OK){
               CLI_print("ERROR: MEA_API_Get_LxCP_Protocol failed (id=%d,protocol=%d)\n",
                         LxCpId,key.protocol);
            }

             
            if(show_title==MEA_FALSE){          
               CLI_print("Prof proto proto                                                        Action      Action win    out\n"
                         "id   id    name                                                                     ID     white  cluster\n"
                         "---- ----- ------------------------------------------------------------ ----------- ------ ------ -------\n");
                        
               show_title=MEA_TRUE;
             }//show_title
             
             CLI_print("%4d %5d %-60s %-11s", 
                       LxCpId,
                       key.protocol,
                       protocol_str[key.protocol],
                       actionType_str[data.action_type]);
            if ((data.action_type != MEA_LXCP_PROTOCOL_ACTION_ACTION) ||
                (data.ActionId_valid != MEA_TRUE)) {
                CLI_print(" %-6s ","NA");
            } else {
                CLI_print(" %6d ",data.ActionId);
            }
			CLI_print(" %6s ", MEA_STATUS_STR(data.WinWhiteList));

            if ((data.action_type != MEA_LXCP_PROTOCOL_ACTION_ACTION) ||
                (data.OutPorts_valid != MEA_TRUE)) {
                CLI_print(" %-6s \n","NA");
            } else {

                for (outport=0;outport<=MEA_MAX_PORT_NUMBER;outport++) 
                {
       	            if( ENET_IsValid_Queue(ENET_UNIT_0,outport,ENET_TRUE) == ENET_FALSE ) 
                    {
                       continue;
                    }

 	                if(mea_Lxcp_drv_IsOutPortOf_LxCP_Protocol(MEA_UNIT_0,LxCpId,&key,outport)==MEA_TRUE)
                    {
			            CLI_print(" %3d",outport);
		            }
                }
                CLI_print("\n");
            }
        } //for protocol
    }//for lxcp
    
    if(show_title!=MEA_TRUE)
       CLI_print("Lxcp table is empty\n");

    return MEA_OK;
}













