/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/


#include "mea_api.h"
#include "cli_eng.h"
#include "mea_cli.h"
#include "mea_db_drv.h"
#include "mea_filter_drv.h"
#include "mea_cli_acl_filter.h"


//filter set create
static MEA_Status MEA_CLI_CreateFilter(int argc, char *argv[]);
//filter set modify
static MEA_Status MEA_CLI_ModifyFilter(int argc, char *argv[]);
//filter set delete
static MEA_Status MEA_CLI_DeleteFilter(int argc, char *argv[]);
//filter show
static MEA_Status MEA_CLI_ShowFilter(int argc, char *argv[]);
static MEA_Status MEA_CLI_ShowFilterEditing(int argc, char *argv[]);
static MEA_Status MEA_CLI_ShowFilterOutPorts(int argc, char *argv[]);
static MEA_Status MEA_CLI_ShowFilterSLATbl(int argc, char *argv[]);
static MEA_Status MEA_CLI_ShowFilterAction(int argc, char *argv[]);
static MEA_Status MEA_CLI_ShowFilterAction_afdx(int argc, char *argv[]);
static MEA_Status MEA_CLI_ShowFilter_FWD(int argc, char *argv[]);


/* Filter */
typedef struct {
    /* commands */
    MEA_Bool bComp;
    MEA_Uint32 comp;
    MEA_Bool bsoft_wred;
    MEA_Uint32 soft_wred;

    MEA_Bool bpolicer_mode;
    MEA_Uint32 policer_mode;

    MEA_Bool  bpolicer_maxMtu;
    MEA_Uint32 policer_maxMtu;

    MEA_Bool bafdx;
    MEA_Uint32 afdx_start;

    MEA_Bool bgn_type;
    MEA_Uint32 gn_type;
    MEA_Bool bgn_sign;
    MEA_Uint32 gn_sign;
    MEA_Bool baction_type;
    MEA_Uint32 action_type;

    MEA_Bool bEgrHdrProc;
    MEA_Uint32 egr_hdr_proc;
    MEA_Bool bEgrHdrProc_session;
    MEA_Uint32 egr_hdr_proc_session;

    MEA_Bool bEgrHdrProc_lmid;
    MEA_Uint32 egr_hdr_proc_lmid;

    MEA_Bool   bdscp_stamp;
    MEA_Uint32 dscp_stamp_valid;
    MEA_Uint32 dscp_stamp_IpPrioityType;
    MEA_Uint32 dscp_stamp_value;






    MEA_Bool bEgrHdrProcEditType;
    MEA_Uint32 egr_hdr_procEditType;

    MEA_Bool bEgrHdrProcIPCS_DL;
    MEA_Uint32 egr_hdr_procIPCS_DL;

    MEA_Bool bEgrHdrProcIPCS_UL;
    MEA_Uint32 egr_hdr_procIPCS_UL;

    MEA_Bool bEgrHdrProcNVGRE;
    MEA_Uint32 egr_hdr_procNVGRE;

    MEA_Bool bEgrHdrProcIPSEC;
    MEA_Uint32 egr_hdr_procIPSEC;

    MEA_Bool   bEgrHdrProcTunnelL2TPGRE;
    MEA_Uint32 egr_hdr_procTunnelL2TPGRE;

    MEA_Bool bEgrHdrProcESP;
    MEA_Uint32 egr_hdr_procESP;


    MEA_Bool bEgrHdrProcVXLAN_DL;
    MEA_Uint32 egr_hdr_procVXLAN_DL;

  


    MEA_Bool bEgrHdrProcETHCS_DL;
    MEA_Uint32 egr_hdr_procETHCS_DL;

    MEA_Bool bEgrHdrProcETHCS_UL;
    MEA_Uint32 egr_hdr_procETHCS_UL;

    MEA_Bool bEgrHdrProccalcIp_checksum;
    MEA_Uint32 EgrHdrProccalcIp_checksum;






    MEA_Bool bset1588;
    MEA_Uint32 set1588;

    MEA_Bool blag_bypass;
    MEA_Uint32 lag_bypass;

    MEA_Bool bflood_ED;
    MEA_Uint32 flood_ED;

    MEA_Bool bDrop;
    MEA_Uint32 Drop;

    MEA_Bool bsflow;
    MEA_Uint32 sflow_info;



    MEA_Bool   boverRule_vpValid;
    MEA_Uint32 overRule_vp_start;
    

    MEA_Bool   bFrag_IP_paramter;
    MEA_Uint32 Frag_IP_paramter;

    MEA_Bool   bDeFrag_IP_paramter;
    MEA_Uint32 DeFrag_IP_paramter;

   

    MEA_Bool bsetlxcp_win;
    MEA_Uint32 setlxcp_win;

    MEA_Bool    bHIERACHY;
    MEA_Uint32  HIERACHY;

    MEA_Bool    baction;
    MEA_Uint32  action_start;

    MEA_Bool    bfwd;
    MEA_Uint32  fwd_start;

    
    MEA_Bool bfwd_field_mask;
    MEA_Uint32 fwd_field_mask_start;

    MEA_Bool   bfwd_Act_enb;
    MEA_Uint32 fwd_Act_enb_start;
    MEA_Bool     bhpm_cos;
    MEA_Uint32   hpm_cos_pram;




    MEA_Bool bEgrHdrProc_mapping;
    MEA_Uint32 egr_hdr_proc_mapping;
    MEA_Bool bAtmEgrHdrProc;
    MEA_Uint32 atm_egr_hdr_proc;

    MEA_Bool bEgrHdrProcWbrg;
    MEA_Uint32 egr_hdr_procWbrg;

	MEA_Bool bEgrHdrProcWbrgmode;
	MEA_Uint32 egr_hdr_procWbrgmode;

    MEA_Bool bEgrHdrProcPROTOLLC;
    MEA_Uint32 egr_hdr_procPROTOLLC;

    MEA_Bool    bEgrHdr802_1p;
    MEA_Uint32  egr_hdr_802_1p;


    MEA_Bool bAtmEgrHdrProc_mapping;
    MEA_Uint32 atm_egr_hdr_proc_mapping;
    MEA_Bool bNewAtmEgrHdrProc;
    MEA_Uint32 new_atm_egr_hdr_proc;
    MEA_Bool bMartiniEgrHdrProc;
    MEA_Uint32 martini_egr_hdr_proc;
    MEA_Bool bStampEgrHdrProc;
    MEA_Uint32 stamp_egr_hdr_proc;
    MEA_Bool source_port_flag;
    MEA_Uint32 source_port_val;

    MEA_Bool   sid_flag;
    MEA_Uint32 sid_val;

    //    MEA_Bool bProtocolLlc;
    //    MEA_Uint32 protocol_llc_val;

    MEA_Bool  bprotocol_enable;
    MEA_Bool   protocol_force;
    MEA_Uint32 protocol;
    MEA_Bool   bllc_enable;
    MEA_Bool   llc_valid;
    MEA_Uint32 llc;


    MEA_Bool bPolicerProc;
    MEA_Uint32 policer_proc;
    struct{
        MEA_Bool   bPolicing;
        MEA_EirCir_t CIR;
        MEA_EirCir_t EIR;
        MEA_Uint32 CBS;
        MEA_Uint32 EBS;
        MEA_Uint32 cup;
        MEA_Uint32 color_aware;
        MEA_Uint32 comp;
    }Policing_t;

    MEA_Bool bRate_enable;
    MEA_Uint32 rate_enable;

    MEA_Bool cos_Flag;
    MEA_Uint32  cos_Value;
    MEA_Bool pri_Flag;
    MEA_Uint32    pri_Value;
    MEA_Bool col_Flag;
    MEA_Uint32    col_Value;
    MEA_CLI_force_cmd_t priCmd;
    MEA_CLI_force_cmd_t cosCmd;
    MEA_CLI_force_cmd_t colCmd;
    MEA_Bool            flowCoSMapping_flag;
    MEA_Bool            flowCoSMapping_force;
    MEA_Uint32          flowCoSMappingProfile_id;

    MEA_Bool            flowMarkingMapping_flag;
    MEA_Bool            flowMarkingMapping_force;
    MEA_Uint32          flowMarkingMappingProfile_id;


    MEA_Bool pm_type_Flag;
    MEA_pm_type_te pm_type_value;
    MEA_Bool Pm_Flag;
    MEA_PmId_t PM_Value;
    MEA_Bool Tm_Flag;
    MEA_TmId_t TM_Value;
    MEA_Bool mtu_Flag;
    MEA_TmId_t mtu_Id;




    MEA_Bool pol_Id_Flag;

    MEA_Policer_prof_t pol_Id;

    MEA_Bool Ed_Flag;
    MEA_Editing_t    Ed_Value;
    MEA_Uint32 num_of_editing;
    MEA_Bool boutPort_null;

    MEA_Uint32 num_of_output;
    /*output list */
    MEA_OutPorts_Entry_dbt out_ports_tbl_entry;
}filterCommandResault_s;

/********************************************************************/
/* input command format ( in argv[startIndex] ) :                   */
/*      <num of outputs> <output1> .... <outputN>                   */
/*      -command <command argument1> ...  <command argumentN>       */
/* return :                                                         */
/*      endIndexP is the index for the next <num of output>         */
/*                     (if available)                               */
/********************************************************************/
MEA_Status    mea_filterGetNextEditor(int argc, char *argv[], MEA_Uint16 startIndex, MEA_Uint16 *endIndexP, filterCommandResault_s *resaultP)
{
    MEA_Uint16 i;
    MEA_Uint32 *pPortGrp;
    ENET_QueueId_t out_queue;
    MEA_Uint32  shiftWord = 0x00000001;
    MEA_Uint16 numOfArgInCommand;
    char *ch;
    MEA_Uint32 len;

    /* (1.) go over out port list */
    pPortGrp = (MEA_Uint32 *)(&(resaultP->out_ports_tbl_entry.out_ports_0_31));
    if ((startIndex + 1) + MEA_OS_atoi(argv[startIndex]) >argc) {
        CLI_print(
            "not enough  parameters\n");
        return MEA_ERROR;
    }
    for (i = startIndex + 1; i<(MEA_Uint32)((startIndex + 1) + MEA_OS_atoi(argv[startIndex])); i++){


        /* get the output  value */
        out_queue = MEA_OS_atoi(argv[i]);

        /* check if the out_port is valid */
        if (ENET_IsValid_Queue(ENET_UNIT_0, out_queue, ENET_FALSE) == ENET_FALSE)  {
            return MEA_ERROR;
        }

        /* set the correct bit in the out ports bit mask */
        *(pPortGrp + (out_queue / 32)) |= (shiftWord << (out_queue % 32));
    }


    /* (2. go over all command and add them to result) */
    for (i = (startIndex + 1) + MEA_OS_atoi(argv[startIndex]); i<(MEA_Uint32)argc; i++){
        if (/*argv[i][0] != '-'*/ (MEA_OS_strcmp(argv[i], "-o") == 0))
        {
            i++;
            break;
        }


        numOfArgInCommand = 0;
        if (!MEA_OS_strcmp(argv[i], "-sp")){
            resaultP->source_port_flag = MEA_TRUE;
            resaultP->source_port_val = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-sid")){
            resaultP->sid_flag = MEA_TRUE;
            resaultP->sid_val = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-col")){
            resaultP->col_Flag = MEA_TRUE;
            resaultP->col_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-cos")){
            resaultP->cos_Flag = MEA_TRUE;
            if (MEA_OS_atoi(argv[i + 1]) > 0x7){
                CLI_print(
                    "value after -cos is big then 7 < %d \n", MEA_OS_atoi(argv[i + 1]));
                return MEA_OK;
            }
            resaultP->cos_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-pri")){
            resaultP->pri_Flag = MEA_TRUE;
            resaultP->pri_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-pol")){
            resaultP->bPolicerProc = MEA_TRUE;
            resaultP->policer_proc = i + 1;
            numOfArgInCommand = 7;
        }
        if (!MEA_OS_strcmp(argv[i], "-policer")) {
            resaultP->Policing_t.bPolicing = MEA_TRUE;
            resaultP->Policing_t.CIR = MEA_OS_atoiNum64(argv[i + 1]);
            resaultP->Policing_t.EIR = MEA_OS_atoiNum64(argv[i + 2]);
            resaultP->Policing_t.CBS = MEA_OS_atoiNum(argv[i + 3]);
            resaultP->Policing_t.EBS = MEA_OS_atoiNum(argv[i + 4]);
            resaultP->Policing_t.cup = MEA_OS_atoiNum(argv[i + 5]);
            resaultP->Policing_t.color_aware = MEA_OS_atoiNum(argv[i + 6]);
            resaultP->Policing_t.comp = MEA_OS_atoiNum(argv[i + 7]);
            numOfArgInCommand = 7;
        }

        if (!MEA_OS_strcmp(argv[i], "-policer_mode")){
            resaultP->bpolicer_mode = MEA_TRUE;
            resaultP->policer_mode = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-policer_maxMtu")){
            resaultP->bpolicer_maxMtu = MEA_TRUE;
            resaultP->policer_maxMtu = i + 1;
            numOfArgInCommand = 1;
        }

        if (!MEA_OS_strcmp(argv[i], "-pgt")){
            resaultP->bgn_type = MEA_TRUE;
            resaultP->gn_type = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-pgt_sign")){
            resaultP->bgn_sign = MEA_TRUE;
            resaultP->gn_sign = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-h")){
            resaultP->bEgrHdrProc = MEA_TRUE;
            resaultP->egr_hdr_proc = i + 1;
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-h2")){
            resaultP->bNewAtmEgrHdrProc = MEA_TRUE;
            resaultP->new_atm_egr_hdr_proc = i + 1;
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-ho")){
            resaultP->bAtmEgrHdrProc = MEA_TRUE;
            resaultP->atm_egr_hdr_proc = i + 1;
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-hwbrg")){
            resaultP->bEgrHdrProcWbrg = MEA_TRUE;
            resaultP->egr_hdr_procWbrg = i + 1;
            numOfArgInCommand = 1;
        }
		if (!MEA_OS_strcmp(argv[i], "-hwmode")) {
			resaultP->bEgrHdrProcWbrgmode = MEA_TRUE;
			resaultP->egr_hdr_procWbrgmode = i + 1;
			numOfArgInCommand = 3;
		}
        if (!MEA_OS_strcmp(argv[i], "-h802_1p")) {
            resaultP->bEgrHdr802_1p = MEA_TRUE;
            resaultP->egr_hdr_802_1p = i + 1;
            numOfArgInCommand = 1;
        }
        
        if (!MEA_OS_strcmp(argv[i], "-hpl")){
            resaultP->bEgrHdrProcPROTOLLC = MEA_TRUE;
            resaultP->egr_hdr_procPROTOLLC = i + 1;
            numOfArgInCommand = 3;
        }
        if (!MEA_OS_strcmp(argv[i], "-hs")){
            resaultP->bEgrHdrProc_session = MEA_TRUE;
            resaultP->egr_hdr_proc_session = i + 1;
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-lmid")){
            resaultP->bEgrHdrProc_lmid = MEA_TRUE;
            resaultP->egr_hdr_proc_lmid = i + 1;
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-hdscp")) {
            resaultP->bdscp_stamp = MEA_TRUE;
            resaultP->dscp_stamp_valid = i + 1;
            resaultP->dscp_stamp_IpPrioityType = i+2;
            resaultP->dscp_stamp_value = i + 3;
            numOfArgInCommand = 3;
        }
        if (!MEA_OS_strcmp(argv[i], "-hm")){
            resaultP->bEgrHdrProc_mapping = MEA_TRUE;
            resaultP->egr_hdr_proc_mapping = i + 1;
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-hom")){
            resaultP->bAtmEgrHdrProc_mapping = MEA_TRUE;
            resaultP->atm_egr_hdr_proc_mapping = i + 1;
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-a")){
            resaultP->bAtmEgrHdrProc = MEA_TRUE;
            resaultP->atm_egr_hdr_proc = i + 1;
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-m")){
            resaultP->bMartiniEgrHdrProc = MEA_TRUE;
            resaultP->martini_egr_hdr_proc = i + 1;
            numOfArgInCommand = 4; //3;
        }
        if (!MEA_OS_strcmp(argv[i], "-s")){
            resaultP->bStampEgrHdrProc = MEA_TRUE;
            resaultP->stamp_egr_hdr_proc = i + 1;
            numOfArgInCommand = 10;
        }
        if (!MEA_OS_strcmp(argv[i], "-hType")){
            resaultP->bEgrHdrProcEditType = MEA_TRUE;
            resaultP->egr_hdr_procEditType = i + 1;
            numOfArgInCommand = 1;
        }

        if (!MEA_OS_strcmp(argv[i], "-hIPCS_DL")){
            resaultP->bEgrHdrProcIPCS_DL = MEA_TRUE;
            resaultP->egr_hdr_procIPCS_DL = i + 1;
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPCS_DL]) != 0)
                numOfArgInCommand = 5;
            else
                numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-hIPCS_UL")) {
            resaultP->bEgrHdrProcIPCS_UL = MEA_TRUE;
            resaultP->egr_hdr_procIPCS_UL = i + 1;
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPCS_UL]) != 0)
                numOfArgInCommand = 5;
            else
                numOfArgInCommand = 1;
        }

        if (!MEA_OS_strcmp(argv[i], "-hNVGRE")) {
            resaultP->bEgrHdrProcNVGRE = MEA_TRUE;
            resaultP->egr_hdr_procNVGRE = i + 1;
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procNVGRE]) != 0)
                numOfArgInCommand = 5;
            else
                numOfArgInCommand = 1;
        }

        if (!MEA_OS_strcmp(argv[i], "-hL2TPGRE")) {
            resaultP->bEgrHdrProcTunnelL2TPGRE = MEA_TRUE;
            resaultP->egr_hdr_procTunnelL2TPGRE = i + 1;
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procTunnelL2TPGRE]) != 0) {
                if (MEA_OS_atoi(argv[resaultP->egr_hdr_procTunnelL2TPGRE + 1]) == MEA_Tunnel_TYPE_GRE) {
                    numOfArgInCommand = 6;
                }
                if (MEA_OS_atoi(argv[resaultP->egr_hdr_procTunnelL2TPGRE + 1]) == MEA_Tunnel_TYPE_L2TP) {
                    numOfArgInCommand = 7;
                }

            }
            else {
                numOfArgInCommand = 1;
            }

        }

        if (!MEA_OS_strcmp(argv[i], "-hIPSec")) {
            resaultP->bEgrHdrProcIPSEC = MEA_TRUE;
            resaultP->egr_hdr_procIPSEC = i + 1;

            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPSEC]) != 0) {
                if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPSEC + 1]) == MEA_IPSec_tunnel_TYPE_NVGRE)
                    numOfArgInCommand = 7;
                if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPSEC + 1]) == MEA_IPSec_tunnel_TYPE_only)
                    numOfArgInCommand = 4;
                if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPSEC + 1]) == MEA_IPSec_tunnel_TYPE_VXLAN)
                    numOfArgInCommand = 10;

            }
            else {
                numOfArgInCommand = 1;
            }
        }


        if (!MEA_OS_strcmp(argv[i], "-hvxlan_DL")){
            resaultP->bEgrHdrProcVXLAN_DL = MEA_TRUE;
            resaultP->egr_hdr_procVXLAN_DL = i + 1;
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procVXLAN_DL]) != 0)
                numOfArgInCommand = 8;
            else
                numOfArgInCommand = 1;
        }
  

       

        if (!MEA_OS_strcmp(argv[i], "-hETHCS_DL")){
            resaultP->bEgrHdrProcETHCS_DL = MEA_TRUE;
            resaultP->egr_hdr_procETHCS_DL = i + 1;
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procETHCS_DL]) != 0)
                numOfArgInCommand = 6;
            else
                numOfArgInCommand = 1;

        }
        if (!MEA_OS_strcmp(argv[i], "-hETHCS_UL")){
            resaultP->bEgrHdrProcETHCS_UL = MEA_TRUE;
            resaultP->egr_hdr_procETHCS_UL = i + 1;
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procETHCS_UL]) != 0)
                numOfArgInCommand = 4;
            else
                numOfArgInCommand = 1;

        }

        if (!MEA_OS_strcmp(argv[i], "-hcalcIp_checksum")){
            resaultP->bEgrHdrProccalcIp_checksum = MEA_TRUE;
            resaultP->EgrHdrProccalcIp_checksum = i + 1;
            numOfArgInCommand = 2;
        }




        if (!MEA_OS_strcmp(argv[i], "-Pm")){
            resaultP->Pm_Flag = MEA_TRUE;
            resaultP->PM_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-Tm")){
            resaultP->Tm_Flag = MEA_TRUE;
            resaultP->TM_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-mtu_id")){
            resaultP->mtu_Flag = MEA_TRUE;
            resaultP->mtu_Id = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }








        if (!MEA_OS_strcmp(argv[i], "-pol_id")) {
            resaultP->pol_Id_Flag = MEA_TRUE;
            numOfArgInCommand = 1;
            ch = argv[i];
            len = (i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i, len, argc, ch);

            resaultP->pol_Id = MEA_OS_atoi(argv[i + 1]);

        }
        if (!MEA_OS_strcmp(argv[i], "-Ed")){
            resaultP->Ed_Flag = MEA_TRUE;
            resaultP->Ed_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        //         if(!MEA_OS_strcmp(argv[i],"-p")){
        //             resaultP->bProtocolLlc=MEA_TRUE;
        //             resaultP->protocol_llc_val=i+1;
        //             numOfArgInCommand = 2;
        //         } 
        if (!MEA_OS_strcmp(argv[i], "-protocol")) {	/* */
            resaultP->bprotocol_enable = MEA_TRUE;
            resaultP->protocol_force = i + 1;
            resaultP->protocol = i + 2;
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-llc")) {	/* */
            resaultP->bllc_enable = MEA_TRUE;
            resaultP->llc_valid = i + 1;
            resaultP->llc = i + 2;
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-afdx")) {	/* */
            resaultP->bafdx = MEA_TRUE;
            resaultP->afdx_start = i + 1;
            numOfArgInCommand = 4;
        }

        if ((i + numOfArgInCommand) >= (MEA_Uint16)argc)
        {
            CLI_print(
                "%s - missing arguments in command %s \n",
                __FUNCTION__, argv[i]);
            return MEA_OK;
        }
        i = i + numOfArgInCommand;
    }
    *endIndexP = i;

    return MEA_OK;
}



MEA_Status    mea_filterGetOutputParams(int argc, char *argv[], MEA_Uint32 startIndex, MEA_OutPorts_Entry_dbt *output_info)
{
    MEA_Uint32 num_of_q;
    MEA_Uint32 last_index;
    MEA_Uint32 *pPortGrp;
    ENET_QueueId_t out_queue;
    MEA_Uint32  i, shiftWord = 0x00000001;

    if (MEA_OS_strcmp(argv[startIndex], "-o"))
    {
        CLI_print("Non output info in index %d\n", startIndex);
        return MEA_ERROR;
    }

    num_of_q = MEA_OS_atoi(argv[startIndex + 1]);

    if ((MEA_Uint32)argc < startIndex + num_of_q + 1)
    {
        CLI_print("Non valid number of output parameters\n");
        return MEA_ERROR;
    }

    last_index = startIndex + 1 + num_of_q;
    pPortGrp = (MEA_Uint32 *)(&(output_info->out_ports_0_31));
    for (i = startIndex + 2; i <= last_index; i++){


        /* get the output  value */
        out_queue = MEA_OS_atoi(argv[i]);

        /* check if the out_port is valid */
        if (ENET_IsValid_Queue(ENET_UNIT_0, out_queue, ENET_FALSE) == ENET_FALSE)  {
            return MEA_ERROR;
        }

        if (*(pPortGrp + (out_queue / 32)) & (shiftWord << (out_queue % 32)))
        {
            CLI_print("Queue %d appears more than once\n", out_queue);
            return MEA_ERROR;
        }

        /* set the correct bit in the out ports bit mask */
        *(pPortGrp + (out_queue / 32)) |= (shiftWord << (out_queue % 32));
    }

    return MEA_OK;
}


MEA_Status    mea_filterGetGeneralOptions(int argc, char *argv[], MEA_Uint32 startIndex, filterCommandResault_s *resaultP)
{
    MEA_Uint32 i;
    MEA_Uint16 numOfArgInCommand;
    MEA_Bool in_editing_section;
    char *ch;
    MEA_Uint32 len;

    MEA_OS_memset(resaultP, 0, sizeof(*resaultP));

    resaultP->num_of_editing = 0;
    resaultP->num_of_output = 0;
#if 0
    resaultP->Pm_Flag = MEA_TRUE;
    resaultP->PM_Value = 0;
    resaultP->Ed_Flag = MEA_TRUE;
    resaultP->Ed_Value = 0;
    resaultP->Tm_Flag = MEA_TRUE;
    resaultP->TM_Value = 0;
#endif
    in_editing_section = MEA_FALSE;

    for (i = startIndex; i< (MEA_Uint32)argc; i++)
    {
        numOfArgInCommand = 0;

        if (!MEA_OS_strcmp(argv[i], "-outNULL")){
            in_editing_section = MEA_FALSE;
            resaultP->boutPort_null = MEA_TRUE;
            numOfArgInCommand = 0;
        }

        if (!MEA_OS_strcmp(argv[i], "-o")){
            in_editing_section = MEA_FALSE;
            if (mea_filterGetOutputParams(argc, argv, i, &(resaultP->out_ports_tbl_entry)) != MEA_OK)
                return MEA_ERROR;

            resaultP->num_of_output++;
            numOfArgInCommand = MEA_OS_atoi(argv[i + 1]) + 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-action_type")){
            in_editing_section = MEA_FALSE;
            resaultP->baction_type = MEA_TRUE;
            resaultP->action_type = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-sp")){
            in_editing_section = MEA_FALSE;
            resaultP->source_port_flag = MEA_TRUE;
            resaultP->source_port_val = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-sid")){
            in_editing_section = MEA_FALSE;
            resaultP->sid_flag = MEA_TRUE;
            resaultP->sid_val = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-col")){
            in_editing_section = MEA_FALSE;
            resaultP->col_Flag = MEA_TRUE;
            resaultP->col_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-cos")){
            in_editing_section = MEA_FALSE;
            resaultP->cos_Flag = MEA_TRUE;
            if (MEA_OS_atoi(argv[i + 1]) > 0x7){
                CLI_print(
                    "value after -cos is big then 7 < %d \n", MEA_OS_atoi(argv[i + 1]));
                return MEA_OK;
            }
            resaultP->cos_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-pri")){
            in_editing_section = MEA_FALSE;
            resaultP->pri_Flag = MEA_TRUE;
            resaultP->pri_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-col-cmd")){
            in_editing_section = MEA_FALSE;
            resaultP->colCmd.bCmd = MEA_TRUE;
            resaultP->colCmd.cmd = MEA_OS_atoi(argv[i + 1]);;
            resaultP->colCmd.value = MEA_OS_atoi(argv[i + 2]);
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-cos-cmd")){
            in_editing_section = MEA_FALSE;
            resaultP->cosCmd.bCmd = MEA_TRUE;
            resaultP->cosCmd.cmd = MEA_OS_atoi(argv[i + 1]);
            if (MEA_OS_atoi(argv[i + 2]) > 0x7){
                CLI_print(
                    "value after -cos is big then 7 < %d \n", MEA_OS_atoi(argv[i + 1]));
                return MEA_OK;
            }
            resaultP->cosCmd.value = MEA_OS_atoi(argv[i + 2]);
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-pri-cmd")){
            in_editing_section = MEA_FALSE;
            resaultP->priCmd.bCmd = MEA_TRUE;
            resaultP->priCmd.cmd = MEA_OS_atoi(argv[i + 1]);
            resaultP->priCmd.value = MEA_OS_atoi(argv[i + 2]);
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-cos-map")) {
            resaultP->flowCoSMapping_flag = MEA_TRUE;
            numOfArgInCommand = 2;
            ch = argv[i];
            len = (i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i, len, argc, ch);
            resaultP->flowCoSMapping_force = MEA_OS_atoi(argv[i + 1]);
            resaultP->flowCoSMappingProfile_id = MEA_OS_atoi(argv[i + 2]);

        }
        if (!MEA_OS_strcmp(argv[i], "-pri-map")) {
            numOfArgInCommand = 2;
            ch = argv[i];
            len = (i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i, len, argc, ch);

            resaultP->flowMarkingMapping_flag = MEA_TRUE;
            resaultP->flowMarkingMapping_force = MEA_OS_atoi(argv[i + 1]);
            resaultP->flowMarkingMappingProfile_id = MEA_OS_atoi(argv[i + 2]);

        }


        if (!MEA_OS_strcmp(argv[i], "-ra")){
            in_editing_section = MEA_FALSE;
            resaultP->bRate_enable = MEA_TRUE;
            resaultP->rate_enable = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-pol")){
            in_editing_section = MEA_FALSE;
            resaultP->bPolicerProc = MEA_TRUE;
            resaultP->policer_proc = i + 1;
            numOfArgInCommand = 7;
        }
        if (!MEA_OS_strcmp(argv[i], "-policer")) {
            resaultP->Policing_t.bPolicing = MEA_TRUE;
            resaultP->Policing_t.CIR = MEA_OS_atoiNum64(argv[i + 1]);
            resaultP->Policing_t.EIR = MEA_OS_atoiNum64(argv[i + 2]);
            resaultP->Policing_t.CBS = MEA_OS_atoiNum(argv[i + 3]);
            resaultP->Policing_t.EBS = MEA_OS_atoiNum(argv[i + 4]);
            resaultP->Policing_t.cup = MEA_OS_atoiNum(argv[i + 5]);
            resaultP->Policing_t.color_aware = MEA_OS_atoiNum(argv[i + 6]);
            resaultP->Policing_t.comp = MEA_OS_atoiNum(argv[i + 7]);
            numOfArgInCommand = 7;
        }
        if (!MEA_OS_strcmp(argv[i], "-policer_mode")){
            resaultP->bpolicer_mode = MEA_TRUE;
            resaultP->policer_mode = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-policer_maxMtu")){
            resaultP->bpolicer_maxMtu = MEA_TRUE;
            resaultP->policer_maxMtu = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-pgt")){
            in_editing_section = MEA_FALSE;
            resaultP->bgn_type = MEA_TRUE;
            resaultP->gn_type = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-set1588")){
            in_editing_section = MEA_FALSE;
            resaultP->bset1588 = MEA_TRUE;
            resaultP->set1588 = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-lag_bypass")){
            in_editing_section = MEA_FALSE;
            resaultP->blag_bypass = MEA_TRUE;
            resaultP->lag_bypass = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-f_ED")){
            in_editing_section = MEA_FALSE;
            resaultP->bflood_ED = MEA_TRUE;
            resaultP->flood_ED = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-Drop")){
            in_editing_section = MEA_FALSE;
            resaultP->bDrop = MEA_TRUE;
            resaultP->Drop = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-overRule_vp")) {
            in_editing_section = MEA_FALSE;
            resaultP->boverRule_vpValid = MEA_TRUE;
            resaultP->overRule_vp_start = i + 1;
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-frag_IP")) {
            in_editing_section = MEA_FALSE;
            resaultP->bFrag_IP_paramter = MEA_TRUE;
            resaultP->Frag_IP_paramter = i + 1;
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-defrag_IP")) {
            in_editing_section = MEA_FALSE;
            resaultP->bDeFrag_IP_paramter = MEA_TRUE;
            resaultP->DeFrag_IP_paramter = i + 1;
            numOfArgInCommand = 3;
        }

        if (!MEA_OS_strcmp(argv[i], "-sflow_type")) {
            in_editing_section = MEA_FALSE;
            resaultP->bsflow = MEA_TRUE;
            resaultP->sflow_info = i + 1;
            numOfArgInCommand = 1;
        }

        
      
        


        if (!MEA_OS_strcmp(argv[i], "-pgt_sign")){
            in_editing_section = MEA_FALSE;
            resaultP->bgn_sign = MEA_TRUE;
            resaultP->gn_sign = i + 1;
            numOfArgInCommand = 1;
        }

        if (!MEA_OS_strcmp(argv[i], "-h")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-ho")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-hwbrg")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 1;
        }
		if (!MEA_OS_strcmp(argv[i], "-hwmode")) {
			if (!in_editing_section)
			{
				resaultP->num_of_editing++;
				in_editing_section = MEA_TRUE;
			}
			numOfArgInCommand = 3;
		}
        if (!MEA_OS_strcmp(argv[i], "-h802_1p")) {
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 1;
        }
        
        if (!MEA_OS_strcmp(argv[i], "-h2")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-hs")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-hm")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-hom")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-a")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-m")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 4; //3;
        }
        if (!MEA_OS_strcmp(argv[i], "-hType")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;

            }
            numOfArgInCommand = 1;
        }

        if (!MEA_OS_strcmp(argv[i], "-hIPCS_DL")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
                
                if (MEA_OS_atoi(argv[i+1]) != 0)
                    numOfArgInCommand = 5;
                else
                    numOfArgInCommand = 1;
            }
            if (MEA_OS_atoi(argv[i + 1]) != 0)
                numOfArgInCommand = 5;
            else
                numOfArgInCommand = 1;

        }

        if (!MEA_OS_strcmp(argv[i], "-hIPCS_UL")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;

                if (MEA_OS_atoi(argv[i+1]) != 0)
                    numOfArgInCommand = 5;
                else
                    numOfArgInCommand = 1;
            }

           
        }

        if (!MEA_OS_strcmp(argv[i], "-hETHCS_DL")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
                
                if (MEA_OS_atoi(argv[i+1]) != 0)
                    numOfArgInCommand = 6;
                else
                    numOfArgInCommand = 1;
            }

            if (MEA_OS_atoi(argv[i + 1]) != 0)
                numOfArgInCommand = 6;
            else
                numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-hETHCS_UL")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;

                if (MEA_OS_atoi(argv[i+1]) != 0)
                    numOfArgInCommand = 4;
                else
                    numOfArgInCommand = 1;
            }
            if (MEA_OS_atoi(argv[i + 1]) != 0)
                numOfArgInCommand = 4;
            else
                numOfArgInCommand = 1;

        }

        if (!MEA_OS_strcmp(argv[i], "-hcalcIp_checksum")){
            if (!in_editing_section)
            {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            numOfArgInCommand = 2;
        }

        if (!MEA_OS_strcmp(argv[i], "-hNVGRE")) {
            resaultP->bEgrHdrProcNVGRE = MEA_TRUE;
            resaultP->egr_hdr_procNVGRE = i + 1;
            if (!in_editing_section) {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procNVGRE]) != 0)
                numOfArgInCommand = 5;
            else
                numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-hIPSec")) {
            resaultP->bEgrHdrProcIPSEC = MEA_TRUE;
            resaultP->egr_hdr_procIPSEC = i + 1;
            if (!in_editing_section) {
                resaultP->num_of_editing++;
            }
            in_editing_section = MEA_TRUE;
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPSEC]) != 0) {
                if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPSEC + 1]) == MEA_IPSec_tunnel_TYPE_NVGRE)
                    numOfArgInCommand = 7;
                if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPSEC + 1]) == MEA_IPSec_tunnel_TYPE_only)
                    numOfArgInCommand = 4;
                if (MEA_OS_atoi(argv[resaultP->egr_hdr_procIPSEC + 1]) == MEA_IPSec_tunnel_TYPE_VXLAN)
                    numOfArgInCommand = 10;

            }
            else {
                numOfArgInCommand = 1;
            }
        }


        if (!MEA_OS_strcmp(argv[i], "-hvxlan_DL")) {
            resaultP->bEgrHdrProcVXLAN_DL = MEA_TRUE;
            resaultP->egr_hdr_procVXLAN_DL = i + 1;
            if (!in_editing_section) {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
            if (MEA_OS_atoi(argv[resaultP->egr_hdr_procVXLAN_DL]) != 0)
                numOfArgInCommand = 8;
            else
                numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-hESP")) {
            resaultP->bEgrHdrProcESP = MEA_TRUE;
            resaultP->egr_hdr_procESP = i + 1;
            numOfArgInCommand = 2;
            if (!in_editing_section) {
                resaultP->num_of_editing++;
                in_editing_section = MEA_TRUE;
            }
        }


        /************************************************************/




        if (!MEA_OS_strcmp(argv[i], "-pm_type")){
            in_editing_section = MEA_FALSE;
            resaultP->pm_type_Flag = MEA_TRUE;
            resaultP->pm_type_value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-Pm")){
            in_editing_section = MEA_FALSE;
            resaultP->Pm_Flag = MEA_TRUE;
            resaultP->PM_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-Tm")){
            in_editing_section = MEA_FALSE;
            resaultP->Tm_Flag = MEA_TRUE;
            resaultP->TM_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-pol_id")) {
            in_editing_section = MEA_FALSE;
            resaultP->pol_Id_Flag = MEA_TRUE;
            numOfArgInCommand = 1;
            ch = argv[i];
            len = (i + numOfArgInCommand);
            MEA_CLI_CHECK_NUM_PARM(i, len, argc, ch);
            resaultP->pol_Id = MEA_OS_atoi(argv[i + 1]);

        }


        if (!MEA_OS_strcmp(argv[i], "-Ed")){
            in_editing_section = MEA_FALSE;
            resaultP->Ed_Flag = MEA_TRUE;
            resaultP->Ed_Value = MEA_OS_atoi(argv[i + 1]);
            numOfArgInCommand = 1;
        }
        //         if(!MEA_OS_strcmp(argv[i],"-p")){
        //             in_editing_section = MEA_FALSE;
        //             resaultP->bProtocolLlc=MEA_TRUE;
        //             resaultP->protocol_llc_val= (MEA_OS_atoi(argv[i+1]) & 0x3) | ((MEA_OS_atoi(argv[i+2]) & 0x1) << 2);
        //             numOfArgInCommand = 2;
        //         }
        if (!MEA_OS_strcmp(argv[i], "-protocol")) {	/* */
            in_editing_section = MEA_FALSE;
            resaultP->bprotocol_enable = MEA_TRUE;
            resaultP->protocol_force = i + 1;
            resaultP->protocol = i + 2;
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-llc")) {	/* */
            in_editing_section = MEA_FALSE;
            resaultP->bllc_enable = MEA_TRUE;
            resaultP->llc_valid = i + 1;
            resaultP->llc = i + 2;
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-afdx")) {	/* */
            resaultP->bafdx = MEA_TRUE;
            resaultP->afdx_start = i + 1;
            numOfArgInCommand = 4;
        }
        if (!MEA_OS_strcmp(argv[i], "-lxcp_win")){
            in_editing_section = MEA_FALSE;
            resaultP->bsetlxcp_win = MEA_TRUE;
            resaultP->setlxcp_win = i + 1;
            numOfArgInCommand = 1;
        }

                                    

        if ((!MEA_OS_strcmp(argv[i], "-HIERACHY")) || (!MEA_OS_strcmp(argv[i], "-HIERARCHY")) ){
            in_editing_section = MEA_FALSE;
            resaultP->bHIERACHY = MEA_TRUE;
            resaultP->HIERACHY = i + 1;
            numOfArgInCommand = 1;
        }

        if (!MEA_OS_strcmp(argv[i], "-action")){
            in_editing_section = MEA_FALSE;
            resaultP->baction = MEA_TRUE;
            resaultP->action_start = i + 1;
            numOfArgInCommand = 2;
        }
        if (!MEA_OS_strcmp(argv[i], "-fwd")) {	/* */
            resaultP->bfwd = MEA_TRUE;
            resaultP->fwd_start = i + 1;
            numOfArgInCommand = 4;
        }
         
        if (!MEA_OS_strcmp(argv[i], "-fwd_field_mask")) {
            resaultP->bfwd_field_mask = MEA_TRUE;
            resaultP->fwd_field_mask_start = i + 1;
            numOfArgInCommand = 1;
        }

        if (!MEA_OS_strcmp(argv[i], "-fwd_act_en")) {
            resaultP->bfwd_Act_enb = MEA_TRUE;
            resaultP->fwd_Act_enb_start = i + 1;
            numOfArgInCommand = 1;
        }
        if (!MEA_OS_strcmp(argv[i], "-cos_hpm")) {
            resaultP->bhpm_cos     = MEA_TRUE;
            resaultP->hpm_cos_pram = i + 1;
        numOfArgInCommand = 2;
        }
       


        if ((i + numOfArgInCommand) >= (MEA_Uint16)argc)
        {
            CLI_print(
                "%s - missing arguments in command %s \n",
                __FUNCTION__, argv[i]);
            return MEA_OK;
        }
        i = i + numOfArgInCommand;
    }

    return MEA_OK;
}

MEA_Status  mea_filter_FillEditing(int argc,
    char *argv[],
    MEA_Uint32 startIndex,
    MEA_EgressHeaderProc_Entry_dbt* ehp_data,
    MEA_Uint32 *num_of_params)
{
    MEA_Uint32 i = startIndex;
    MEA_Uint32 count = 0;
    MEA_Bool atm_used = MEA_FALSE;
    MEA_Bool ehp_used = MEA_FALSE;
    MEA_Bool ehp_usedOuter = MEA_FALSE;
    *num_of_params = 0;

    while (i < (MEA_Uint32)argc &&
        (!MEA_OS_strcmp(argv[i], "-h")          ||
        !MEA_OS_strcmp(argv[i], "-hType")               ||
        !MEA_OS_strcmp(argv[i], "-hIPSec")              ||
        !MEA_OS_strcmp(argv[i], "-hL2TPGRE")            ||
        !MEA_OS_strcmp(argv[i], "-hESP")                ||
        !MEA_OS_strcmp(argv[i], "-hvxlan_DL")           ||
        !MEA_OS_strcmp(argv[i], "-hIPCS_DL")            ||
        !MEA_OS_strcmp(argv[i], "-hIPCS_UL")            ||
        !MEA_OS_strcmp(argv[i], "-hETHCS_DL")           ||
        !MEA_OS_strcmp(argv[i], "-hETHCS_UL")           ||
        !MEA_OS_strcmp(argv[i], "-hcalcIp_checksum")    ||

        !MEA_OS_strcmp(argv[i], "-hwbrg")   ||
		!MEA_OS_strcmp(argv[i], "-hwmode") ||
        !MEA_OS_strcmp(argv[i], "-h802_1p") ||
         
        !MEA_OS_strcmp(argv[i], "-hpl")     ||
        !MEA_OS_strcmp(argv[i], "-h2")      ||
        !MEA_OS_strcmp(argv[i], "-hs")      ||
        !MEA_OS_strcmp(argv[i], "-hm")      ||
        !MEA_OS_strcmp(argv[i], "-hom")     ||
        !MEA_OS_strcmp(argv[i], "-lmid")    ||
        !MEA_OS_strcmp(argv[i], "-hdscp")    ||
        !MEA_OS_strcmp(argv[i], "-m")       ||
        !MEA_OS_strcmp(argv[i], "-r")       ||
        !MEA_OS_strcmp(argv[i], "-a")       ||
        !MEA_OS_strcmp(argv[i], "-s")       ||
        !MEA_OS_strcmp(argv[i], "-ho")))
    {

        if (!MEA_OS_strcmp(argv[i], "-h"))
        {
            ehp_used = MEA_TRUE;
            ehp_data->eth_info.val.all = MEA_OS_atohex(argv[i + 1]);
            ehp_data->eth_info.stamp_color = MEA_OS_atoi(argv[i + 2]);
            ehp_data->eth_info.stamp_priority = MEA_OS_atoi(argv[i + 3]);
            ehp_data->eth_info.command = MEA_OS_atoi(argv[i + 4]);
            count = 4;
        }
        else if (!MEA_OS_strcmp(argv[i], "-hType"))
        {
            ehp_used = MEA_TRUE;
            ehp_data->EditingType = MEA_OS_atohex(argv[i + 1]);
            count = 1;

        }
        else if (!MEA_OS_strcmp(argv[i], "-hvxlan_DL")) {
            ehp_used = MEA_TRUE;
            ehp_data->VxLan_info.valid = MEA_OS_atoi(argv[i + 1]);
            if (ehp_data->VxLan_info.valid) {
                MEA_OS_atohex_MAC(argv[i + 2], &ehp_data->VxLan_info.vxlan_Da);
                ehp_data->VxLan_info.vxlan_dst_Ip = MEA_OS_inet_addr(argv[i + 3]);
                ehp_data->VxLan_info.vxlan_vni = MEA_OS_atoiNum(argv[i + 4]);
                ehp_data->VxLan_info.l4_dest_Port = MEA_OS_atoiNum(argv[i + 5]);
                ehp_data->VxLan_info.l4_src_Port = MEA_OS_atoiNum(argv[i + 6]);
                ehp_data->VxLan_info.ttl = MEA_OS_atoiNum(argv[i + 7]);
                
                
                count = 7;
            }
            else {
                count = 1;
            }

        }
        else if (!MEA_OS_strcmp(argv[i], "-hNVGRE"))
        {
            ehp_used = MEA_TRUE;
            ehp_data->NVGre_info.valid = MEA_OS_atoi(argv[i+1]);
            if (ehp_data->NVGre_info.valid) {
                MEA_OS_atohex_MAC(argv[i+2], &ehp_data->NVGre_info.Outer_da_MAC);
                ehp_data->NVGre_info.GRE_DST_IP = MEA_OS_inet_addr(argv[i+3]);
                ehp_data->NVGre_info.VSID = MEA_OS_atoiNum(argv[i+4]);
                ehp_data->NVGre_info.FlowID = MEA_OS_atoiNum(argv[i+5]);
                count = 5;
            }
            else {
                count = 1;
            }

        }
        
        else if (!MEA_OS_strcmp(argv[i], "-hL2TPGRE"))
        {
            ehp_used = MEA_TRUE;
            ehp_data->GRE_L2TP_info.valid = MEA_OS_atoi(argv[i + 1]);
            count = 1;
            if (ehp_data->GRE_L2TP_info.valid) 
            {
                ehp_data->GRE_L2TP_info.Tunne_type = MEA_OS_atoiNum(argv[i + 2]);
                count = 2;
                if (ehp_data->GRE_L2TP_info.Tunne_type == MEA_Tunnel_TYPE_GRE)
                {
                    MEA_OS_atohex_MAC(argv[i + 3], &ehp_data->GRE_L2TP_info.Da);
                    MEA_OS_atohex_MAC(argv[i + 4], &ehp_data->GRE_L2TP_info.Sa);
                    ehp_data->GRE_L2TP_info.dst_IPv4 = MEA_OS_inet_addr(argv[i+ 5]);
                    ehp_data->GRE_L2TP_info.src_IPv4 = MEA_OS_inet_addr(argv[i+6]);
                    count = 6;
                }
                if (ehp_data->GRE_L2TP_info.Tunne_type == MEA_Tunnel_TYPE_L2TP)
                {
                    MEA_OS_atohex_MAC(argv[i + 3], &ehp_data->GRE_L2TP_info.Da);
                    MEA_OS_atohex_MAC(argv[i + 4], &ehp_data->GRE_L2TP_info.Sa);
                    ehp_data->GRE_L2TP_info.dst_IPv4 = MEA_OS_inet_addr(argv[i + 5]);

                    ehp_data->GRE_L2TP_info.TunnelId = MEA_OS_atoiNum(argv[i+ 6]);
                    ehp_data->GRE_L2TP_info.SessionId = MEA_OS_atoiNum(argv[i + 7]);
                    count = 7;
                }

            }

        }

        else if (!MEA_OS_strcmp(argv[i], "-hIPSec")) 
        {
            ehp_used = MEA_TRUE;
            ehp_data->IPSec_info.valid = MEA_OS_atoi(argv[i+1]);
            count = 1;
            if (ehp_data->IPSec_info.valid) {
                ehp_data->IPSec_info.type = MEA_OS_atoiNum(argv[i+2]);
                count = 2;
                if (ehp_data->IPSec_info.type == MEA_IPSec_tunnel_TYPE_NVGRE) {
                    MEA_OS_atohex_MAC(argv[i+3], &ehp_data->NVGre_info.Outer_da_MAC);
                    ehp_data->IPSec_info.IPSec_DST_IP = MEA_OS_inet_addr(argv[i+4]);
                    ehp_data->NVGre_info.GRE_DST_IP = MEA_OS_inet_addr(argv[i+5]);
                    ehp_data->NVGre_info.VSID = MEA_OS_atoiNum(argv[i+6]);
                    ehp_data->NVGre_info.FlowID = MEA_OS_atoiNum(argv[i+7]);
                    count = 7;
                }
                if (ehp_data->IPSec_info.type == MEA_IPSec_tunnel_TYPE_only)
                {
                    ehp_data->IPSec_info.IPSec_src_IP = MEA_OS_inet_addr(argv[i+3]);
                    ehp_data->IPSec_info.IPSec_DST_IP = MEA_OS_inet_addr(argv[i+4]);
                    count = 4;

                }
                if (ehp_data->IPSec_info.type == MEA_IPSec_tunnel_TYPE_VXLAN)
                {
                    ehp_data->VxLan_info.valid = MEA_TRUE;
                    MEA_OS_atohex_MAC(argv[i+3], &ehp_data->VxLan_info.vxlan_Da);
                    ehp_data->VxLan_info.vxlan_dst_Ip = MEA_OS_inet_addr(argv[i+4]);
                    ehp_data->VxLan_info.vxlan_vni = MEA_OS_atoiNum(argv[i+5]);
                    ehp_data->VxLan_info.l4_dest_Port = MEA_OS_atoiNum(argv[i+6]);
                    ehp_data->VxLan_info.l4_src_Port = MEA_OS_atoiNum(argv[i+7]);
                    ehp_data->VxLan_info.ttl = MEA_OS_atoiNum(argv[i + 8]);
                    ehp_data->VxLan_info.sgw_ipId = MEA_OS_atoiNum(argv[i + 9]);
                    ehp_data->IPSec_info.IPSec_DST_IP = MEA_OS_inet_addr(argv[i + 10]);
                    count = 10;


                }




            }
        
        
        }


        else if (!MEA_OS_strcmp(argv[i], "-hESP")) {
            ehp_used = MEA_TRUE;
            ehp_data->encryp_decryp_type = MEA_OS_atoi(argv[i + 1]);
            ehp_data->crypto_ESP_Id = MEA_OS_atoi(argv[i + 2]);

            
            count = 2;
        }



        else if (!MEA_OS_strcmp(argv[i], "-hIPCS_DL")){
            ehp_used = MEA_TRUE;
            ehp_data->IPCS_DL_info.valid = MEA_OS_atoi(argv[i+1]);
            if (ehp_data->IPCS_DL_info.valid){
                MEA_OS_atohex_MAC(argv[i + 2], &ehp_data->IPCS_DL_info.Da_eNB);
                ehp_data->IPCS_DL_info.dst_eNB_Ip = MEA_OS_inet_addr(argv[i + 3]);
                ehp_data->IPCS_DL_info.TE_ID = MEA_OS_atoiNum(argv[i + 4]);
                ehp_data->IPCS_DL_info.sgw_ipId = MEA_OS_atoiNum(argv[i + 5]);
                count = 5;
            }
            else{
                count = 1;
            }

        }

        else if (!MEA_OS_strcmp(argv[i], "-hIPCS_UL"))
        {
            ehp_used = MEA_TRUE;

            ehp_data->IPCS_UL_info.valid = MEA_OS_atoi(argv[i+1]);
            if (ehp_data->IPCS_UL_info.valid){

                MEA_OS_atohex_MAC(argv[i + 2], &ehp_data->IPCS_UL_info.Da_next_hop_mac);
                MEA_OS_atohex_MAC(argv[i + 3], &ehp_data->IPCS_UL_info.Sa_host_mac);
                ehp_data->IPCS_UL_info.apn_vlan = MEA_OS_atoiNum(argv[i+4]);
                ehp_data->IPCS_UL_info.id_pdn_IP = MEA_OS_atoiNum(argv[i + 5]);
                count = 4;
            } else{
                count = 1;
            }

            
        }



        else if (!MEA_OS_strcmp(argv[i], "-hETHCS_DL"))
        {
            ehp_used = MEA_TRUE;

            ehp_data->ETHCS_DL_info.valid = MEA_OS_atoi(argv[i+1]);
            if (ehp_data->ETHCS_DL_info.valid){
                MEA_OS_atohex_MAC(argv[i + 2], &ehp_data->ETHCS_DL_info.Da_eNB);
                ehp_data->ETHCS_DL_info.dst_eNB_Ip = MEA_OS_inet_addr(argv[i+3]);
                ehp_data->ETHCS_DL_info.TE_ID = MEA_OS_atoiNum(argv[i+4]);
                ehp_data->ETHCS_DL_info.dst_UE_Ip = MEA_OS_inet_addr(argv[i+5]);
                ehp_data->ETHCS_DL_info.sgw_ipId = MEA_OS_atoiNum(argv[i+6]);
                count = 6;
            }
            else {
                count = 1;
            }

           

        }
        else if (!MEA_OS_strcmp(argv[i], "-hETHCS_UL"))
        {
            ehp_used = MEA_TRUE;

            ehp_data->ETHCS_UL_info.valid = MEA_OS_atoi(argv[i+1]);
            if (ehp_data->ETHCS_UL_info.valid){
                ehp_data->ETHCS_UL_info.mode = MEA_OS_atoiNum(argv[i + 2]);
                ehp_data->ETHCS_UL_info.s_vlan = MEA_OS_atoiNum(argv[i+3]);
                ehp_data->ETHCS_UL_info.c_vlan = MEA_OS_atoiNum(argv[i+4]);
                count = 4;
            } else{
                count = 1;
            }
        }

        else if (!MEA_OS_strcmp(argv[i], "-hcalcIp_checksum")){
            ehp_used = MEA_TRUE;

            ehp_data->LmCounterId_info.valid = MEA_TRUE;
            ehp_data->LmCounterId_info.sw_calcIp_checksum_valid = MEA_OS_atoiNum(argv[i + 1]);
            ehp_data->LmCounterId_info.calcIp_checksum = MEA_OS_atoiNum(argv[i + 2]);
            count = 2;
        }








        else if (!MEA_OS_strcmp(argv[i], "-lmid"))
        {
            ehp_used = MEA_TRUE;
            ehp_data->LmCounterId_info.valid = MEA_OS_atoi(argv[i + 1]);
            ehp_data->LmCounterId_info.LmId = MEA_OS_atoi(argv[i + 2]);
            ehp_data->LmCounterId_info.Command_dasa = MEA_OS_atoi(argv[i + 3]);
            ehp_data->LmCounterId_info.Command_cfm = MEA_OS_atoi(argv[i + 4]);
            count = 4;
        }
        else if (!MEA_OS_strcmp(argv[i], "-hdscp"))
        {
            ehp_used = MEA_TRUE;
            ehp_data->LmCounterId_info.valid = MEA_TRUE;
            ehp_data->LmCounterId_info.Command_dscp_stamp_enable = MEA_OS_atoi(argv[i + 1]);
            ehp_data->LmCounterId_info.Command_IpPrioityType = MEA_OS_atoiNum(argv[i+2]);
            ehp_data->LmCounterId_info.Command_dscp_stamp_value  = MEA_OS_atoi(argv[i + 3]);
            count = 2;
        }
        else if (!MEA_OS_strcmp(argv[i], "-ho")){
            ehp_usedOuter = MEA_TRUE;
            ehp_data->atm_info.val.all = MEA_OS_atohex(argv[i + 1]);
            ehp_data->atm_info.stamp_color = MEA_OS_atoi(argv[i + 2]);
            ehp_data->atm_info.stamp_priority = MEA_OS_atoi(argv[i + 3]);
            ehp_data->atm_info.double_tag_cmd = MEA_OS_atoi(argv[i + 4]);
            count = 4;
        }
        else if (!MEA_OS_strcmp(argv[i], "-hwbrg")){
            ehp_used = MEA_TRUE;

            ehp_data->wbrg_info.val.data_wbrg.flow_type = MEA_OS_atoi(argv[i + 1]);


            count = 1;
        }
		else if (!MEA_OS_strcmp(argv[i], "-hwmode")) {
			ehp_used = MEA_TRUE;

			ehp_data->wbrg_info.val.data_wbrg.enc_swap_mode = MEA_OS_atoi(argv[i + 1]);
			ehp_data->wbrg_info.val.data_wbrg.enc_outer_type = MEA_OS_atoi(argv[i + 2]);
			ehp_data->wbrg_info.val.data_wbrg.enc_inner_type = MEA_OS_atoi(argv[i + 3]);

			count = 3;
		}
         
        else if (!MEA_OS_strcmp(argv[i], "-h802_1p")) {
        ehp_used = MEA_TRUE;

        ehp_data->wbrg_info.val.data_wbrg.stamp802_1p = MEA_OS_atoi(argv[i + 1]);


        count = 1;
        }
        else if (!MEA_OS_strcmp(argv[i], "-hpt")){
            ehp_used = MEA_TRUE;

            ehp_data->Attribute_info.valid    = MEA_OS_atoi(argv[i + 1]);
            ehp_data->Attribute_info.protocol = MEA_OS_atoi(argv[i + 2]);
            ehp_data->Attribute_info.llc      = MEA_OS_atoi(argv[i + 3]);

            count = 3;
        }
        else if (!MEA_OS_strcmp(argv[i], "-h2"))
        {
            MEA_Uint32 val;
            ehp_used = MEA_TRUE;
            val = MEA_OS_atohex(argv[i + 1]);

            ehp_data->atm_info.val.all = val;
            ehp_data->atm_info.double_tag_cmd = MEA_OS_atoi(argv[i + 2]);

            count = 2;
        }
        else if (!MEA_OS_strcmp(argv[i], "-hs"))
        {
            ehp_used = MEA_TRUE;

            ehp_data->fragmentSession_info.valid = MEA_OS_atoi(argv[i + 1]);
            ehp_data->fragmentSession_info.enable = MEA_OS_atoi(argv[i + 2]);
            ehp_data->fragmentSession_info.extract_enable = MEA_OS_atoi(argv[i + 3]);
            ehp_data->fragmentSession_info.Session_num = MEA_OS_atoi(argv[i + 4]);

            count = 4;
        }
        else if (!MEA_OS_strcmp(argv[i], "-hm"))
        {
            ehp_used = MEA_TRUE;

            ehp_data->eth_info.mapping_enable = MEA_OS_atoi(argv[i + 1]);
            ehp_data->eth_info.mapping_id = MEA_OS_atoi(argv[i + 2]);

            count = 2;
        }
        else if (!MEA_OS_strcmp(argv[i], "-hom"))
        {
            ehp_used = MEA_TRUE;

            ehp_data->atm_info.mapping_enable = MEA_OS_atoi(argv[i + 1]);
            ehp_data->atm_info.mapping_id = MEA_OS_atoi(argv[i + 2]);

            count = 2;
        }
        else if (!MEA_OS_strcmp(argv[i], "-m"))
        {
            MEA_OS_atohex_MAC(argv[i + 1], &(ehp_data->martini_info.DA));
            MEA_OS_atohex_MAC(argv[i + 2], &(ehp_data->martini_info.SA));
            ehp_data->martini_info.EtherType = (MEA_Uint16)(MEA_OS_atohex(argv[i + 3]));
            ehp_data->martini_info.martini_cmd = (MEA_Uint16)(MEA_OS_atohex(argv[i + 4]));

            ehp_data->martini_info.E_L_LSP = 0;
            count = 4;

        }
        else if (!MEA_OS_strcmp(argv[i], "-r")) 
        {
            MEA_OS_atohex_MAC(argv[i + 1], &(ehp_data->martini_info.DA));
            MEA_OS_atohex_MAC(argv[i + 2], &(ehp_data->martini_info.SA));
            ehp_data->martini_info.EtherType = (MEA_Uint16)(MEA_OS_atohex(argv[i + 3]));
            /* set the IP etherType on the EtherType */
            ehp_data->martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
            count = 3;

        }

        else if (!MEA_OS_strcmp(argv[i], "-a"))
        {
            MEA_Uint8 vpi;
            MEA_Uint16 vci;
            atm_used = MEA_TRUE;
            vpi = MEA_OS_atoi(argv[i + 1]);
            vci = MEA_OS_atoi(argv[i + 2]);

            ehp_data->atm_info.val.all = (vpi << 20);
            ehp_data->atm_info.val.all |= (vci << 4);
            ehp_data->atm_info.stamp_color = MEA_OS_atoi(argv[i + 3]);
            ehp_data->atm_info.stamp_priority = MEA_OS_atoi(argv[i + 4]);
            count = 4;
        }
        else if (!MEA_OS_strcmp(argv[i], "-s"))
        {
            ehp_data->eth_stamping_info.color_bit0_valid = MEA_OS_atoi(argv[i + 1]);
            ehp_data->eth_stamping_info.color_bit0_loc = MEA_OS_atoi(argv[i + 2]);
            ehp_data->eth_stamping_info.color_bit1_valid = MEA_OS_atoi(argv[i + 3]);
            ehp_data->eth_stamping_info.color_bit1_loc = MEA_OS_atoi(argv[i + 4]);
            ehp_data->eth_stamping_info.pri_bit0_valid = MEA_OS_atoi(argv[i + 5]);
            ehp_data->eth_stamping_info.pri_bit0_loc = MEA_OS_atoi(argv[i + 6]);
            ehp_data->eth_stamping_info.pri_bit1_valid = MEA_OS_atoi(argv[i + 7]);
            ehp_data->eth_stamping_info.pri_bit1_loc = MEA_OS_atoi(argv[i + 8]);
            ehp_data->eth_stamping_info.pri_bit2_valid = MEA_OS_atoi(argv[i + 9]);
            ehp_data->eth_stamping_info.pri_bit2_loc = MEA_OS_atoi(argv[i + 10]);
            count = 10;
        }
        else
        {
            CLI_print("non valid editing option %s \n", argv[i]);
            return MEA_ERROR;
        }

        i += count + 1;
        (*num_of_params) += count + 1;
    }

    if ((atm_used && ehp_used) || (atm_used && ehp_usedOuter))
    {
        CLI_print("Cannot use -a with -h -h2 -ho option\n");
        return MEA_ERROR;
    }

    return MEA_OK;
}



MEA_Status    mea_filterGetLastEditing(int argc,
    char *argv[],
    MEA_Uint32 startIndex,
    MEA_EgressHeaderProc_Entry_dbt* ehp_data,
    MEA_Bool *found)
{
    MEA_Uint32 i, numOfArgInCommand;

    MEA_OS_memset(ehp_data, 0, sizeof(*ehp_data));

    *found = MEA_FALSE;

    for (i = startIndex; i < (MEA_Uint32)argc; i++)
    {
        numOfArgInCommand = 0;


        if  (   !MEA_OS_strcmp(argv[i], "-h")         ||
                !MEA_OS_strcmp(argv[i], "-hType")     ||
                !MEA_OS_strcmp(argv[i], "-hvxlan_DL") ||
                !MEA_OS_strcmp(argv[i], "-hIPSec")    ||
                !MEA_OS_strcmp(argv[i], "-hL2TPGRE")  ||
                !MEA_OS_strcmp(argv[i], "-hIPCS_DL")  ||
                !MEA_OS_strcmp(argv[i], "-hIPCS_UL")  ||
                !MEA_OS_strcmp(argv[i], "-hETHCS_DL") ||
                !MEA_OS_strcmp(argv[i], "-hETHCS_UL") ||
                !MEA_OS_strcmp(argv[i], "-hcalcIp_checksum") ||

                !MEA_OS_strcmp(argv[i], "-hwbrg") ||
				!MEA_OS_strcmp(argv[i], "-hwmode") ||
                !MEA_OS_strcmp(argv[i], "-h802_1p") ||

                !MEA_OS_strcmp(argv[i], "-hpl") ||
                !MEA_OS_strcmp(argv[i], "-h2") ||
                !MEA_OS_strcmp(argv[i], "-hs") ||
                !MEA_OS_strcmp(argv[i], "-hm") ||
                !MEA_OS_strcmp(argv[i], "-hom") ||
                !MEA_OS_strcmp(argv[i], "-lmid") ||
                !MEA_OS_strcmp(argv[i], "-hdscp") ||
                !MEA_OS_strcmp(argv[i], "-m") ||
                !MEA_OS_strcmp(argv[i], "-r") ||
                !MEA_OS_strcmp(argv[i], "-a") ||
                !MEA_OS_strcmp(argv[i], "-s") ||
                !MEA_OS_strcmp(argv[i], "-ho")
            )
        {
            *found = MEA_TRUE;

            if (mea_filter_FillEditing(argc,
                argv,
                i,
                ehp_data,
                &numOfArgInCommand) != MEA_OK)
            {
                return MEA_ERROR;
            }
            i = i + numOfArgInCommand + 1;
        }
    }

    return MEA_OK;
}



MEA_Status    mea_filterGetEditing(int argc,
    char *argv[],
    MEA_Uint32 startIndex,
    MEA_OutPorts_Entry_dbt*  total_output_info,
    MEA_EgressHeaderProc_Array_Entry_dbt* ehp_array,
    MEA_Bool is_ehp_unicast)
{
    MEA_Uint32              i;
    MEA_Uint32              numOfArgInCommand;
    MEA_OutPorts_Entry_dbt  output_info;
    MEA_Bool                editing_set, editing_more, found;
    MEA_Uint32              editing_index;

    if (ehp_array->num_of_entries == 0)
    {
        CLI_print("mea_filterGetEditing called but no given EHP space \n");
        return MEA_OK;
    }

    if (is_ehp_unicast)
    {
        /* If is_unicast - just search for the LAST -h -m -a -h2 option.
        reallocate the EHP array to 1 entry, put the entire output info*/
        if (ehp_array->ehp_info)
            MEA_OS_free(ehp_array->ehp_info);

        ehp_array->num_of_entries = 1;
        ehp_array->ehp_info = MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt)* 1);
        if (ehp_array->ehp_info == NULL)
        {
            CLI_print("ERROR: can not allocate %d bytes for %d editors\n", sizeof(MEA_EHP_Info_dbt)* 1);
            return MEA_OK;
        }
        MEA_OS_memset(ehp_array->ehp_info, 0, sizeof(MEA_EHP_Info_dbt)* 1);
        MEA_OS_memcpy(&(ehp_array->ehp_info[0].output_info), total_output_info, sizeof(ehp_array->ehp_info[0].output_info));

        if (mea_filterGetLastEditing(argc,
            argv,
            startIndex,
            &(ehp_array->ehp_info[0].ehp_data),
            &found) != MEA_OK)
        {
            CLI_print("Error on mea_filterGetLastEditing\n");
            return MEA_OK;
        }

        if (!found)
        {
            CLI_print("mea_filterGetLastEditing called but no editing option exist\n");
            return MEA_OK;
        }
    }
    else
    {
        for (editing_index = 0, i = startIndex;
            i < (MEA_Uint32)argc && editing_index < ehp_array->num_of_entries;
            i++)
        {
            numOfArgInCommand = 0;

            if (!MEA_OS_strcmp(argv[i], "-o"))
            {
                editing_set = MEA_FALSE;
                editing_more = MEA_TRUE;

                MEA_OS_memset(&output_info, 0, sizeof(output_info));
                if (mea_filterGetOutputParams(argc, argv, i, &output_info) != MEA_OK)
                    return MEA_ERROR;

                i = i + MEA_OS_atoi(argv[i + 1]) + 2;

                while (editing_more && i< (MEA_Uint32)argc)
                {
                    if ( !MEA_OS_strcmp(argv[i], "-h") ||
                        !MEA_OS_strcmp(argv[i], "-hType") ||
                        !MEA_OS_strcmp(argv[i], "-hvxlan_DL") ||
                        !MEA_OS_strcmp(argv[i], "-hESP")      ||
                        !MEA_OS_strcmp(argv[i], "-hIPSec")    ||
                        !MEA_OS_strcmp(argv[i], "-hL2TPGRE")  ||
                        !MEA_OS_strcmp(argv[i], "-hIPCS_DL")  ||
                        !MEA_OS_strcmp(argv[i], "-hIPCS_UL")  ||
                        !MEA_OS_strcmp(argv[i], "-hETHCS_DL") ||
                        !MEA_OS_strcmp(argv[i], "-hETHCS_UL") ||
                        !MEA_OS_strcmp(argv[i], "-hcalcIp_checksum") ||

                        !MEA_OS_strcmp(argv[i], "-hwbrg") ||
						!MEA_OS_strcmp(argv[i], "-hwmode") ||
                        !MEA_OS_strcmp(argv[i], "-h802_1p") ||
                        
                        !MEA_OS_strcmp(argv[i], "-hpl") ||
                        !MEA_OS_strcmp(argv[i], "-h2") ||
                        !MEA_OS_strcmp(argv[i], "-hs") ||
                        !MEA_OS_strcmp(argv[i], "-hm") ||
                        !MEA_OS_strcmp(argv[i], "-hom") ||
                        !MEA_OS_strcmp(argv[i], "-lmid") ||
                        !MEA_OS_strcmp(argv[i], "-hdscp") ||
                        !MEA_OS_strcmp(argv[i], "-m") ||
                        !MEA_OS_strcmp(argv[i], "-r") ||
                        !MEA_OS_strcmp(argv[i], "-a") ||
                        !MEA_OS_strcmp(argv[i], "-s") ||
                        !MEA_OS_strcmp(argv[i], "-ho")
                        )
                    {
                        if (editing_set)
                        {
                            CLI_print("not allowed more than 1 editing option\n");
                            return MEA_ERROR;
                        }
                        editing_set = MEA_TRUE;

                        if (mea_filter_FillEditing(argc,
                            argv,
                            i,
                            &(ehp_array->ehp_info[editing_index].ehp_data),
                            &numOfArgInCommand) != MEA_OK)
                        {
                            return MEA_ERROR;
                        }
                        MEA_OS_memcpy(&(ehp_array->ehp_info[editing_index].output_info),
                            &output_info,
                            sizeof(ehp_array->ehp_info[editing_index].output_info));
                        editing_index++;
                    }
                    else
                    {
                        editing_more = MEA_FALSE;
                        break;
                    }

                    i = i + numOfArgInCommand + 1;
                }

                i = i - 2;
            }
        }
    }

    return MEA_OK;
}


static MEA_Status MEA_CLI_CreateFilter(int argc, char *argv[])
{
    MEA_Uint32                      i;
    MEA_Filter_Key_dbt                filter_id_key;
    MEA_Filter_Data_dbt               filter_id_data;
    MEA_Policer_Entry_dbt             sla_params_entry;
    MEA_EgressHeaderProc_Entry_dbt  ehp_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt ehp_array;
    MEA_EgressHeaderProc_EthStampingInfo_dbt    eth_stamping_info;
    MEA_EHP_Info_dbt                            ehp_info;
    MEA_Filter_Key_Type_te                      key_type;
    filterCommandResault_s                      result;
    MEA_Filter_t                                filter_id;
    MEA_Bool                                    is_ehp_unicast;
    MEA_Uint32                                  num_of_params;
    MEA_Int8                                    mac_str[20];
    int                                         mandatory_argc = 5;

    MEA_Uint32 value;
    MEA_Uint32 pri_kye_type;
    unsigned char buf[sizeof(struct in6_addr)];
    int  s;




    if (argc < mandatory_argc) return MEA_ERROR;

    MEA_OS_memset(&result, 0, sizeof(result));
    MEA_OS_memset(&ehp_array, 0, sizeof(ehp_array));
    MEA_OS_memset(&ehp_info, 0, sizeof(ehp_info));
    MEA_OS_memset(&filter_id_data, 0, sizeof(filter_id_data));
    MEA_OS_memset(&ehp_entry, 0, sizeof(ehp_entry));
    MEA_OS_memset(&filter_id_key, 0, sizeof(filter_id_key));
   
    MEA_OS_memset(&eth_stamping_info, 0, sizeof(eth_stamping_info));
    MEA_OS_memset(&sla_params_entry, 0, sizeof(sla_params_entry));


    for (i = 1; i < (MEA_Uint32)argc; i++) {

        if ((argv[i][0] == '-') && (
            (MEA_OS_strcmp(argv[i], "-sp") != 0) &&
            (MEA_OS_strcmp(argv[i], "-sid") != 0) &&
            (MEA_OS_strcmp(argv[i], "-col") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pri") != 0) &&
            (MEA_OS_strcmp(argv[i], "-cos") != 0) &&
            (MEA_OS_strcmp(argv[i], "-cos-cmd") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pri-cmd") != 0) &&
            (MEA_OS_strcmp(argv[i], "-col-cmd") != 0) &&

            (MEA_OS_strcmp(argv[i], "-cos-map") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pri-map") != 0) &&


            (MEA_OS_strcmp(argv[i], "-pol") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pol_id") != 0) &&
            (MEA_OS_strcmp(argv[i], "-policer") != 0) &&
            (MEA_OS_strcmp(argv[i], "-policer_mode") != 0) &&
            (MEA_OS_strcmp(argv[i], "-policer_maxMtu") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pgt") != 0) &&
            (MEA_OS_strcmp(argv[i], "-soft_wred") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pgt_sign") != 0) &&
            (MEA_OS_strcmp(argv[i], "-f_ED") != 0) &&
            (MEA_OS_strcmp(argv[i], "-Drop") != 0) &&
            (MEA_OS_strcmp(argv[i], "-overRule_vp") != 0) &&
            (MEA_OS_strcmp(argv[i], "-cos_hpm")     != 0) &&
            (MEA_OS_strcmp(argv[i], "-frag_IP")     != 0) &&
            (MEA_OS_strcmp(argv[i], "-defrag_IP")    != 0) &&
            (MEA_OS_strcmp(argv[i], "-sflow_type") != 0) &&
            
            /********** Editing paramters *****************************/
            
           

            (MEA_OS_strcmp(argv[i], "-h")       != 0) &&
            (MEA_OS_strcmp(argv[i], "-hType")   != 0) &&
            (MEA_OS_strcmp(argv[i], "-hIPSec")  != 0) &&
            (MEA_OS_strcmp(argv[i], "-hL2TPGRE") != 0) &&
            (MEA_OS_strcmp(argv[i], "-hwbrg")   != 0) &&
			(MEA_OS_strcmp(argv[i], "-hwmode") != 0) &&
            (MEA_OS_strcmp(argv[i], "-h802_1p") != 0) &&
            (MEA_OS_strcmp(argv[i], "-hpl")     != 0) &&
            (MEA_OS_strcmp(argv[i], "-h2")      != 0) &&
            (MEA_OS_strcmp(argv[i], "-ho")      != 0) &&
            (MEA_OS_strcmp(argv[i], "-hs")      != 0) &&
            (MEA_OS_strcmp(argv[i], "-hm")      != 0) &&
            (MEA_OS_strcmp(argv[i], "-hom")     != 0) &&
            (MEA_OS_strcmp(argv[i], "-lmid")    != 0) &&
            (MEA_OS_strcmp(argv[i], "-hdscp")    != 0) &&
            (MEA_OS_strcmp(argv[i], "-s")       != 0) &&
            (MEA_OS_strcmp(argv[i], "-a")       != 0) &&
            (MEA_OS_strcmp(argv[i], "-m")       != 0) &&
            (MEA_OS_strcmp(argv[i], "-r")       != 0) &&
            /****************************************/
            (MEA_OS_strcmp(argv[i], "-Pm") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pm_type") != 0) &&
            (MEA_OS_strcmp(argv[i], "-action_type") != 0) &&
            (MEA_OS_strcmp(argv[i], "-Tm") != 0) &&
            (MEA_OS_strcmp(argv[i], "-Ed") != 0) &&
            (MEA_OS_strcmp(argv[i], "-ra") != 0) &&
            (MEA_OS_strcmp(argv[i], "-set1588") != 0) &&
//             (MEA_OS_strcmp(argv[i], "-protocol") != 0) &&
//             (MEA_OS_strcmp(argv[i], "-llc") != 0) &&
            (MEA_OS_strcmp(argv[i], "-lxcp_win") != 0) &&
            
            (MEA_OS_strcmp(argv[i], "-HIERARCHY") != 0) &&    /* part of Key */

            (MEA_OS_strcmp(argv[i], "-action") != 0) &&      /*<valid> <id>*/
            (MEA_OS_strcmp(argv[i], "-fwd") != 0) &&        /* doda 1 key  doda_int force */
            (MEA_OS_strcmp(argv[i], "-fwd_field_mask")!= 0) &&
            (MEA_OS_strcmp(argv[i], "-fwd_act_en") != 0) &&

            (MEA_OS_strcmp(argv[i], "-o") != 0))){
            CLI_print(" Not support option %s \n", argv[i]);
            return MEA_OK;
        }
    }





    /* Fill the keys information, up to the Options */
    key_type = (MEA_Filter_Key_Type_te)MEA_OS_atoi(argv[1]);

    i = 2;

    switch (key_type)
    {
    case MEA_FILTER_KEY_TYPE_DA_MAC:
        num_of_params = 1;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer2.ethernet.DA_valid = MEA_TRUE;
        MEA_OS_atohex_MAC(argv[i], &(filter_id_key.layer2.ethernet.DA));


        break;
    case MEA_FILTER_KEY_TYPE_SA_MAC:
        num_of_params = 1;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer2.ethernet.SA_valid = MEA_TRUE;
        MEA_OS_atohex_MAC(argv[i], &(filter_id_key.layer2.ethernet.SA));
        break;
    case MEA_FILTER_KEY_TYPE_DST_IPV4_DST_PORT:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer3.IPv4.dst_ip_valid = MEA_TRUE;
        filter_id_key.layer3.IPv4.dst_ip = MEA_OS_inet_addr(argv[i]);
        filter_id_key.layer4.dst_port_valid = MEA_TRUE;
        filter_id_key.layer4.dst_port = MEA_OS_atoiNum(argv[i + 1]);


        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV4_DST_PORT:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer3.IPv4.src_ip_valid = MEA_TRUE;
        filter_id_key.layer3.IPv4.src_ip = MEA_OS_inet_addr(argv[i]);
        filter_id_key.layer4.dst_port_valid = MEA_TRUE;
        filter_id_key.layer4.dst_port = MEA_OS_atoiNum(argv[i + 1]);

        break;
    case MEA_FILTER_KEY_TYPE_DST_IPV4_SRC_PORT:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer3.IPv4.dst_ip_valid = MEA_TRUE;
        filter_id_key.layer3.IPv4.dst_ip = MEA_OS_inet_addr(argv[i]);
        filter_id_key.layer4.src_port_valid = MEA_TRUE;
        filter_id_key.layer4.src_port = MEA_OS_atoiNum(argv[i + 1]);

        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV4_SRC_PORT:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer3.IPv4.src_ip_valid = MEA_TRUE;
        filter_id_key.layer3.IPv4.src_ip = MEA_OS_inet_addr(argv[i]);
        filter_id_key.layer4.src_port_valid = MEA_TRUE;
        filter_id_key.layer4.src_port = MEA_OS_atoiNum(argv[i + 1]);

        break;
    case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG:
        num_of_params = 3;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer2.outer_vlanTag.ethertype_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.ethertype = (MEA_Uint16)(MEA_OS_atohex(argv[i]));
        filter_id_key.layer2.inner_vlanTag.ethertype_valid = MEA_TRUE;
        filter_id_key.layer2.inner_vlanTag.ethertype = (MEA_Uint16)(MEA_OS_atohex(argv[i + 1]));
        filter_id_key.layer2.outer_vlanTag.priority_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.priority = (MEA_OS_atohex(argv[i + 2]) & 0xe000) >> 13;
        filter_id_key.layer2.outer_vlanTag.cf_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.cf = (MEA_OS_atohex(argv[i + 2]) & 0x1000) >> 12;
        filter_id_key.layer2.outer_vlanTag.vlan_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.vlan = (MEA_OS_atohex(argv[i + 2]) & 0x0fff);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV4_APPL_ETHERTYPE:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer3.IPv4.src_ip_valid = MEA_TRUE;
        filter_id_key.layer3.IPv4.src_ip = MEA_OS_inet_addr(argv[i]);
        filter_id_key.layer3.ethertype3_valid = MEA_TRUE;
        filter_id_key.layer3.ethertype3 = (MEA_Uint16)(MEA_OS_atohex(argv[i + 1]));
        break;
    case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT:

        num_of_params = 5;

        if (((MEA_Uint32)argc < i + num_of_params)){
            CLI_print("ERROR on num off are wrong  for key type %d ",
                MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT);
            return MEA_ERROR;
        }
        pri_kye_type = MEA_OS_atoi(argv[i]);
        value = MEA_OS_atoi(argv[i + 1]);
        switch (pri_kye_type){
        case MEA_CLI_FILTER_PRI_TYPE_DEFAULT_UNTAG:

            filter_id_key.layer2.outer_vlanTag.priority_valid = MEA_TRUE;
            filter_id_key.layer2.outer_vlanTag.priority = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_OUTER_VLAN_TAG:
            filter_id_key.layer2.outer_vlanTag.priority_valid = MEA_TRUE;
            filter_id_key.layer2.outer_vlanTag.priority = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_INNER_VLAN_TAG:
            filter_id_key.layer2.inner_vlanTag.priority_valid = MEA_TRUE;
            filter_id_key.layer2.inner_vlanTag.priority = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_IPV4_PRECEDENCE:
            filter_id_key.layer3.l3_pri_type = MEA_FILTER_L3_TYPE_IPV4_PRECEDENCE;
            filter_id_key.layer3.l3_pri_valid = MEA_TRUE;
            filter_id_key.layer3.l3_pri_value.ipv4_precedence.precedence = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_IPV4_TOS:
            filter_id_key.layer3.l3_pri_type = MEA_FILTER_L3_TYPE_IPV4_TOS;
            filter_id_key.layer3.l3_pri_valid = MEA_TRUE;
            filter_id_key.layer3.l3_pri_value.ipv4_tos.tos = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_IPV4_DSCP:
            filter_id_key.layer3.l3_pri_type = MEA_FILTER_L3_TYPE_IPV4_DSCP;
            filter_id_key.layer3.l3_pri_valid = MEA_TRUE;
            filter_id_key.layer3.l3_pri_value.ipv4_dscp.dscp = value;
            break;
        default:
            CLI_print("ERROR on  pri type are not support %d ",
                pri_kye_type);
            return MEA_ERROR;
            break;
        } /*end switch*/

        filter_id_key.layer3.ip_protocol_valid = MEA_TRUE;
        filter_id_key.layer3.ip_protocol = (MEA_OS_atoi(argv[i + 2]));
        filter_id_key.layer3.ethertype3_valid = MEA_TRUE;
        filter_id_key.layer3.ethertype3 = (MEA_Uint16)(MEA_OS_atohex(argv[i + 3]));
        filter_id_key.layer4.dst_port_valid = MEA_TRUE;
        filter_id_key.layer4.dst_port = MEA_OS_atoi(argv[i + 4]);

        break;
    case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_SRC_PORT_DST_PORT:
        num_of_params = 5;

        if (((MEA_Uint32)argc < i + num_of_params)){
            CLI_print("ERROR on num off are wrong  for key type %d ",
                MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_SRC_PORT_DST_PORT);
            return MEA_ERROR;
        }
        pri_kye_type = MEA_OS_atoi(argv[i]);
        value = MEA_OS_atoi(argv[i + 1]);
        switch (pri_kye_type){
        case MEA_CLI_FILTER_PRI_TYPE_DEFAULT_UNTAG:
            filter_id_key.layer2.outer_vlanTag.priority_valid = MEA_TRUE;
            filter_id_key.layer2.outer_vlanTag.priority = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_OUTER_VLAN_TAG:
            filter_id_key.layer2.outer_vlanTag.priority_valid = MEA_TRUE;
            filter_id_key.layer2.outer_vlanTag.priority = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_INNER_VLAN_TAG:
            filter_id_key.layer2.inner_vlanTag.priority_valid = MEA_TRUE;
            filter_id_key.layer2.inner_vlanTag.priority = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_IPV4_PRECEDENCE:
            filter_id_key.layer3.l3_pri_type = MEA_FILTER_L3_TYPE_IPV4_PRECEDENCE;
            filter_id_key.layer3.l3_pri_valid = MEA_TRUE;
            filter_id_key.layer3.l3_pri_value.ipv4_precedence.precedence = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_IPV4_TOS:
            filter_id_key.layer3.l3_pri_type = MEA_FILTER_L3_TYPE_IPV4_TOS;
            filter_id_key.layer3.l3_pri_valid = MEA_TRUE;
            filter_id_key.layer3.l3_pri_value.ipv4_tos.tos = value;
            break;
        case MEA_CLI_FILTER_PRI_TYPE_IPV4_DSCP:
            filter_id_key.layer3.l3_pri_type = MEA_FILTER_L3_TYPE_IPV4_DSCP;
            filter_id_key.layer3.l3_pri_valid = MEA_TRUE;
            filter_id_key.layer3.l3_pri_value.ipv4_dscp.dscp = value;
            break;
        default:
            CLI_print("ERROR on  pri type are not support %d ",
                pri_kye_type);
            return MEA_ERROR;
            break;
        } /*end switch*/


        filter_id_key.layer3.ip_protocol_valid = MEA_TRUE;
        filter_id_key.layer3.ip_protocol = (MEA_OS_atoi(argv[i + 2]));
        filter_id_key.layer4.src_port_valid = MEA_TRUE;
        filter_id_key.layer4.src_port = MEA_OS_atoi(argv[i + 3]);
        filter_id_key.layer4.dst_port_valid = MEA_TRUE;
        filter_id_key.layer4.dst_port = MEA_OS_atoi(argv[i + 4]);


        break;
    case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_INNER_VLAN_TAG_OUTER_ETHERTYPE:
        num_of_params = 3;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer2.outer_vlanTag.priority_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.priority = (MEA_OS_atohex(argv[i]) & 0xe000) >> 13;
        filter_id_key.layer2.outer_vlanTag.cf_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.cf = (MEA_OS_atohex(argv[i]) & 0x1000) >> 12;
        filter_id_key.layer2.outer_vlanTag.vlan_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.vlan = (MEA_OS_atohex(argv[i]) & 0x0fff);
        filter_id_key.layer2.inner_vlanTag.priority_valid = MEA_TRUE;
        filter_id_key.layer2.inner_vlanTag.priority = (MEA_OS_atohex(argv[i + 1]) & 0xe000) >> 13;
        filter_id_key.layer2.inner_vlanTag.cf_valid = MEA_TRUE;
        filter_id_key.layer2.inner_vlanTag.cf = (MEA_OS_atohex(argv[i + 1]) & 0x1000) >> 12;
        filter_id_key.layer2.inner_vlanTag.vlan_valid = MEA_TRUE;
        filter_id_key.layer2.inner_vlanTag.vlan = (MEA_OS_atohex(argv[i + 1]) & 0x0fff);
        filter_id_key.layer2.outer_vlanTag.ethertype_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.ethertype = (MEA_Uint16)(MEA_OS_atohex(argv[i + 2]));
        break;
    case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_DST_PORT_SRC_PORT:
        num_of_params = 3;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer2.outer_vlanTag.priority_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.priority = (MEA_OS_atohex(argv[i]) & 0xe000) >> 13;
        filter_id_key.layer2.outer_vlanTag.cf_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.cf = (MEA_OS_atohex(argv[i]) & 0x1000) >> 12;
        filter_id_key.layer2.outer_vlanTag.vlan_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.vlan = (MEA_OS_atohex(argv[i]) & 0x0fff);
        filter_id_key.layer4.dst_port_valid = MEA_TRUE;
        filter_id_key.layer4.dst_port = MEA_OS_atoi(argv[i + 1]);
        filter_id_key.layer4.src_port_valid = MEA_TRUE;
        filter_id_key.layer4.src_port = MEA_OS_atoi(argv[i + 2]);
        break;
    case MEA_FILTER_KEY_TYPE_INNER_VLAN_TAG_DST_PORT_SRC_PORT:
        num_of_params = 3;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer2.inner_vlanTag.priority_valid = MEA_TRUE;
        filter_id_key.layer2.inner_vlanTag.priority = (MEA_OS_atohex(argv[i]) & 0xe000) >> 13;
        filter_id_key.layer2.inner_vlanTag.cf_valid = MEA_TRUE;
        filter_id_key.layer2.inner_vlanTag.cf = (MEA_OS_atohex(argv[i]) & 0x1000) >> 12;
        filter_id_key.layer2.inner_vlanTag.vlan_valid = MEA_TRUE;
        filter_id_key.layer2.inner_vlanTag.vlan = (MEA_OS_atohex(argv[i]) & 0x0fff);
        filter_id_key.layer4.dst_port_valid = MEA_TRUE;
        filter_id_key.layer4.dst_port = MEA_OS_atoi(argv[i + 1]);
        filter_id_key.layer4.src_port_valid = MEA_TRUE;
        filter_id_key.layer4.src_port = MEA_OS_atoi(argv[i + 2]);
        break;
    case MEA_FILTER_KEY_TYPE_PPPOE_SESSION_ID_PPP_PROTO_SA_MAC_LSB:
        num_of_params = 3;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        MEA_OS_strcpy(mac_str, "00:00:00:00:");
        MEA_OS_strcat(mac_str, argv[i + 2]);
        MEA_OS_atohex_MAC(mac_str, &(filter_id_key.layer2.ethernet.SA));
        filter_id_key.layer2.ethernet.SA_valid = MEA_TRUE;
        filter_id_key.layer2.ppp.pppoe_session_id_valid = MEA_TRUE;
        filter_id_key.layer2.ppp.pppoe_session_id_from = (MEA_Uint16)(MEA_OS_atohex(argv[i]));
        filter_id_key.layer2.ppp.ppp_protocol_valid = MEA_TRUE;
        filter_id_key.layer2.ppp.ppp_protocol = (MEA_Uint16)(MEA_OS_atohex(argv[i + 1]));
        break;
    case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_APPL_ETHERTYPE_OUTER_VLAN_TAG:
        num_of_params = 3;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;

        filter_id_key.layer2.outer_vlanTag.ethertype_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.ethertype = (MEA_Uint16)(MEA_OS_atohex(argv[i]));

        filter_id_key.layer3.ethertype3_valid = MEA_TRUE;
        filter_id_key.layer3.ethertype3 = (MEA_Uint16)(MEA_OS_atohex(argv[i + 1]));

        filter_id_key.layer2.outer_vlanTag.priority_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.priority = (MEA_OS_atohex(argv[i + 2]) & 0xe000) >> 13;
        filter_id_key.layer2.outer_vlanTag.cf_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.cf = (MEA_OS_atohex(argv[i + 2]) & 0x1000) >> 12;
        filter_id_key.layer2.outer_vlanTag.vlan_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.vlan = (MEA_OS_atohex(argv[i + 2]) & 0x0fff);
        break;

    case MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4:
        num_of_params = 3;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;

        MEA_OS_atohex_MAC(argv[i], &(filter_id_key.layer2.ethernet.DA));
        if (filter_id_key.layer2.ethernet.DA.a.lss == 0 &&
            filter_id_key.layer2.ethernet.DA.a.msw == 0)
        {
            filter_id_key.layer2.ethernet.DA_valid = MEA_FALSE;
        }
        else {

            filter_id_key.layer2.ethernet.DA_valid = MEA_TRUE;
        }
        filter_id_key.layer2.outer_vlanTag.priority_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.priority = (MEA_OS_atohex(argv[i + 1]) & 0xe000) >> 13;
        filter_id_key.layer2.outer_vlanTag.cf_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.cf = (MEA_OS_atohex(argv[i + 1]) & 0x1000) >> 12;
        filter_id_key.layer2.outer_vlanTag.vlan_valid = MEA_TRUE;
        filter_id_key.layer2.outer_vlanTag.vlan = (MEA_OS_atohex(argv[i + 1]) & 0x0fff);
        filter_id_key.layer3.IPv4.dst_ip_valid = MEA_TRUE;
        filter_id_key.layer3.IPv4.dst_ip = MEA_OS_inet_addr(argv[i + 2]);
       
        break;
    case MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer3.IPv6.dst_ipv6_valid = MEA_TRUE;
        s = inet_pton(AF_INET6, argv[i], buf);
        if (s <= 0) {
            CLI_print("ipv6 format error\n");
            return MEA_ERROR;
        }
        filter_id_key.layer3.IPv6.dst_ipv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
        filter_id_key.layer3.IPv6.dst_ipv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
        filter_id_key.layer3.IPv6.dst_ipv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
        filter_id_key.layer3.IPv6.dst_ipv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;
        filter_id_key.layer4.dst_port_valid = MEA_TRUE;
        filter_id_key.layer4.dst_port = MEA_OS_atoiNum(argv[i + 1]);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer3.IPv6.src_ipv6_valid = MEA_TRUE;
        
            s = inet_pton(AF_INET6, argv[i], buf);
        if (s <= 0) {
            CLI_print("ipv6 format error\n");
            return MEA_ERROR;
        }
        filter_id_key.layer3.IPv6.src_ipv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
        filter_id_key.layer3.IPv6.src_ipv6[1] = (buf[8] << 24)  | (buf[9] << 16) | (buf[10] << 8)  | buf[11] << 0;
        filter_id_key.layer3.IPv6.src_ipv6[2] = (buf[4] << 24)  | (buf[5] << 16) | (buf[6] << 8)   | buf[7] << 0;
        filter_id_key.layer3.IPv6.src_ipv6[3] = (buf[0] << 24)  | (buf[1] << 16) | (buf[2] << 8)   | buf[3] << 0;

        
        filter_id_key.layer4.dst_port_valid = MEA_TRUE;
        filter_id_key.layer4.dst_port = MEA_OS_atoiNum(argv[i + 1]);
        break;
    case MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
           return MEA_ERROR;
        
        s = inet_pton(AF_INET6, argv[i], buf);
        if (s <= 0) {
            CLI_print("ipv6 format error\n");
            return MEA_ERROR;
        }
        filter_id_key.layer3.IPv6.dst_ipv6_valid = MEA_TRUE;
        filter_id_key.layer3.IPv6.dst_ipv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
        filter_id_key.layer3.IPv6.dst_ipv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
        filter_id_key.layer3.IPv6.dst_ipv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
        filter_id_key.layer3.IPv6.dst_ipv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;
        filter_id_key.layer4.src_port_valid = MEA_TRUE;
        filter_id_key.layer4.src_port = MEA_OS_atoiNum(argv[i + 1]);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer3.IPv6.src_ipv6_valid = MEA_TRUE;

        s = inet_pton(AF_INET6, argv[i], buf);
        if (s <= 0) {
            CLI_print("ipv6 format error\n");
            return MEA_ERROR;
        }
        filter_id_key.layer3.IPv6.src_ipv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
        filter_id_key.layer3.IPv6.src_ipv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
        filter_id_key.layer3.IPv6.src_ipv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
        filter_id_key.layer3.IPv6.src_ipv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;
        filter_id_key.layer4.src_port_valid = MEA_TRUE;
        filter_id_key.layer4.src_port = MEA_OS_atoiNum(argv[i + 1]);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE:
        num_of_params = 2;
        if ((MEA_Uint32)argc < i + num_of_params)
            return MEA_ERROR;
        filter_id_key.layer3.IPv6.src_ipv6_valid = MEA_TRUE;

        s = inet_pton(AF_INET6, argv[i], buf);
        if (s <= 0) {
            CLI_print("ipv6 format error\n");
            return MEA_ERROR;
        }
        filter_id_key.layer3.IPv6.src_ipv6[0] = (buf[12] << 24) | (buf[13] << 16) | (buf[14] << 8) | buf[15] << 0;
        filter_id_key.layer3.IPv6.src_ipv6[1] = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11] << 0;
        filter_id_key.layer3.IPv6.src_ipv6[2] = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7] << 0;
        filter_id_key.layer3.IPv6.src_ipv6[3] = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3] << 0;
        filter_id_key.layer3.ethertype3_valid = MEA_TRUE;
        filter_id_key.layer3.ethertype3 = (MEA_Uint16)(MEA_OS_atohex(argv[i + 1]));
        break;



    default:
        return MEA_ERROR;
    }

    i = i + num_of_params;

    if ((MEA_Uint32)argc <= i)
        return MEA_ERROR;
    /* We reached the action section */
    filter_id_data.action_type = (MEA_Filter_Action_te)(MEA_OS_atoi(argv[i]));




    if (filter_id_data.action_type == MEA_FILTER_ACTION_TO_ACTION)
    {
        if (!((MEA_Uint32)argc > i + 1))
        {
            CLI_print("Too few arguments for TO_ACTION\n");
            return MEA_ERROR;
        }
    }


    /* Get general options */
    if (mea_filterGetGeneralOptions(argc, argv, i, &result) == MEA_ERROR)
    {
        CLI_print(
            "%s - mea_filterGetGeneralOptions failed\n",
            __FUNCTION__);
        return MEA_OK;
    }



    /****** 3.set non editor commands ******/
 
    if (result.source_port_flag)
    {
        filter_id_key.interface_key.src_port_valid = MEA_TRUE;
        filter_id_key.interface_key.src_port = (MEA_Port_t)(result.source_port_val);
    }
    if (result.sid_flag)
    {
        filter_id_key.interface_key.sid_valid = MEA_TRUE;
        filter_id_key.interface_key.acl_prof = (MEA_Port_t)(result.sid_val);
    }
    filter_id_data.action_params.tmId_disable = MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL;

    if (result.bRate_enable){
        filter_id_data.action_params.tmId_disable = ((result.rate_enable == MEA_TRUE) ? MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL : MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL);
    }



    if (result.bPolicerProc)
    {
        if ((int)(result.policer_proc + 6) > argc){
            CLI_print("Too few arguments for -pol\n");
            return MEA_ERROR;
        }

        sla_params_entry.CIR = MEA_OS_atoiNum64(argv[result.policer_proc]);
        sla_params_entry.EIR = MEA_OS_atoiNum64(argv[result.policer_proc + 1]);
        sla_params_entry.CBS = MEA_OS_atoiNum(argv[result.policer_proc + 2]);
        sla_params_entry.EBS = MEA_OS_atoiNum(argv[result.policer_proc + 3]);
        sla_params_entry.cup = MEA_OS_atoiNum(argv[result.policer_proc + 4]);
        sla_params_entry.color_aware = MEA_OS_atoiNum(argv[result.policer_proc + 5]);
        sla_params_entry.comp = MEA_OS_atoiNum(argv[result.policer_proc + 6]);

    }

    if (result.Policing_t.bPolicing == MEA_TRUE){
        sla_params_entry.CIR = result.Policing_t.CIR;
        sla_params_entry.EIR = result.Policing_t.EIR;
        sla_params_entry.CBS = result.Policing_t.CBS;
        sla_params_entry.EBS = result.Policing_t.EBS;
        sla_params_entry.cup = result.Policing_t.cup;
        sla_params_entry.color_aware = result.Policing_t.color_aware;
        sla_params_entry.comp = result.Policing_t.comp;
    }

    if (result.bgn_type) {
        if (!result.bPolicerProc && !result.Policing_t.bPolicing)
        {
            CLI_print("Error: -pgt must come with -pol -policer option \n");
            return MEA_OK;
        }
        sla_params_entry.gn_type = MEA_OS_atoi(argv[result.gn_type]);
    }

    if (result.bpolicer_mode) {
        if (!result.bPolicerProc && !result.Policing_t.bPolicing)
        {
            CLI_print("Error: -policer_mode must come with -pol -policer option \n");
            return MEA_OK;
        }
        sla_params_entry.type_mode = MEA_OS_atoi(argv[result.policer_mode]);
    }
    if (result.bpolicer_maxMtu) {
        if (!result.bPolicerProc && !result.Policing_t.bPolicing)
        {
            CLI_print("Error: -policer_maxMtu must come with -pol -policer option \n");
            return MEA_OK;
        }
        sla_params_entry.maxMtu = MEA_OS_atoi(argv[result.policer_maxMtu]);
    }


    if (result.bgn_sign) {
        if (!result.bPolicerProc && !result.Policing_t.bPolicing)
        {
            CLI_print("Error: -pgt_sign must come with -pol -policer option \n");
            return MEA_OK;
        }
        sla_params_entry.gn_sign = MEA_OS_atoi(argv[result.gn_sign]);
    }
    if (result.bset1588){
        filter_id_data.action_params.set_1588 = MEA_OS_atoi(argv[result.set1588]);
    }
    if (result.blag_bypass == MEA_TRUE){
        filter_id_data.action_params.lag_bypass = MEA_OS_atoi(argv[result.lag_bypass]);
    }
    if (result.bflood_ED == MEA_TRUE){
        filter_id_data.action_params.flood_ED = MEA_OS_atoi(argv[result.flood_ED]);
    }
    if (result.bDrop == MEA_TRUE){
        filter_id_data.action_params.Drop_en = MEA_OS_atoi(argv[result.Drop]);
    }
    if (result.bsflow == MEA_TRUE) {
        filter_id_data.action_params.sflow_Type = MEA_OS_atoi(argv[result.sflow_info]);
    }


    if (result.boverRule_vpValid == MEA_TRUE) {
        filter_id_data.action_params.overRule_vpValid = MEA_OS_atoi(argv[result.overRule_vp_start]);
        filter_id_data.action_params.overRule_vp_val = MEA_OS_atoi(argv[result.overRule_vp_start+1]);
    }

    

    

    if (result.col_Flag){
        filter_id_data.action_params.force_color_valid = result.col_Flag;
        filter_id_data.action_params.COLOR = result.col_Value;
    }
    else{
        filter_id_data.action_params.force_color_valid = MEA_ACTION_FORCE_COMMAND_DISABLE;//MEA_ACTION_FORCE_COMMAND_VALUE;
        filter_id_data.action_params.COLOR = MEA_POLICER_COLOR_VAL_GREEN;
    }
    filter_id_data.action_params.force_cos_valid = result.cos_Flag;
    filter_id_data.action_params.COS = result.cos_Value;

    filter_id_data.action_params.force_l2_pri_valid = result.pri_Flag;
    filter_id_data.action_params.L2_PRI = result.pri_Value;

    if (result.cosCmd.bCmd == MEA_TRUE){
        filter_id_data.action_params.force_cos_valid = result.cosCmd.cmd;
        filter_id_data.action_params.COS = result.cosCmd.value;
    }
    if (result.priCmd.bCmd == MEA_TRUE){
        filter_id_data.action_params.force_l2_pri_valid = result.priCmd.cmd;
        filter_id_data.action_params.L2_PRI = result.priCmd.value;
    }

    if (result.colCmd.bCmd == MEA_TRUE){
        filter_id_data.action_params.force_color_valid = result.colCmd.cmd;
        filter_id_data.action_params.COLOR = result.colCmd.value;
    }
    if (result.flowCoSMapping_flag == MEA_TRUE) {
        filter_id_data.action_params.flowCoSMappingProfile_force = result.flowCoSMapping_force;
        filter_id_data.action_params.flowCoSMappingProfile_id = result.flowCoSMappingProfile_id;
    }

    if (result.flowMarkingMapping_flag == MEA_TRUE) {
        filter_id_data.action_params.flowMarkingMappingProfile_force = result.flowMarkingMapping_force;
        filter_id_data.action_params.flowMarkingMappingProfile_id = result.flowMarkingMappingProfile_id;
    }

    filter_id_data.action_params.ed_id_valid = result.Ed_Flag | (result.num_of_editing > 0 ? MEA_TRUE : MEA_FALSE);
    filter_id_data.action_params.ed_id = result.Ed_Value;

    if (filter_id_data.action_type == MEA_FILTER_ACTION_TO_ACTION){
        filter_id_data.action_params.tm_id_valid = result.Tm_Flag | result.bPolicerProc | result.Policing_t.bPolicing;
        filter_id_data.action_params.tm_id = result.TM_Value;
    }
    else {
        filter_id_data.action_params.tm_id_valid = MEA_FALSE;
        filter_id_data.action_params.tm_id = 0;
    }
    filter_id_data.action_params.pm_id_valid = result.Pm_Flag;
    filter_id_data.action_params.pm_id = result.PM_Value;
    if (result.pm_type_Flag == 1){
        filter_id_data.action_params.pm_type = result.pm_type_value;
    }
    if (result.pol_Id_Flag)
    {
        filter_id_data.action_params.policer_prof_id_valid = result.pol_Id_Flag;
        filter_id_data.action_params.policer_prof_id = result.pol_Id;
    }

    if (result.bprotocol_enable){
        filter_id_data.action_params.protocol_llc_force = MEA_OS_atoi(argv[result.protocol_force]);
        filter_id_data.action_params.Protocol = MEA_OS_atoi(argv[result.protocol]);
    }
    if (result.bllc_enable){

        filter_id_data.action_params.proto_llc_valid = MEA_OS_atoi(argv[result.llc_valid]);
        filter_id_data.action_params.Llc = MEA_OS_atoi(argv[result.llc]);
    }
    if (result.bafdx){

        filter_id_data.action_params.afdx_enable = MEA_OS_atoi(argv[result.afdx_start]);
        filter_id_data.action_params.afdx_Rx_sessionId = MEA_OS_atoi(argv[result.afdx_start + 1]);
        filter_id_data.action_params.afdx_A_B = MEA_OS_atoi(argv[result.afdx_start + 2]);
        filter_id_data.action_params.afdx_Bag_info = MEA_OS_atoi(argv[result.afdx_start + 3]);

    }

    if (result.bsetlxcp_win){
        filter_id_data.lxcp_win = MEA_OS_atoi(argv[result.setlxcp_win]);
    }



    if (result.bHIERACHY){
        filter_id_key.Acl_hierarchical_id = MEA_OS_atoi(argv[result.HIERACHY]);
    }


    if (result.baction){
        filter_id_data.force_Action_en   =   MEA_OS_atoi(argv[result.action_start]);
        filter_id_data.Action_Id          =  MEA_OS_atoi(argv[result.action_start+1]);
    }



    if (result.bfwd){
        filter_id_data.fwd_enable               = MEA_OS_atoi(argv[result.fwd_start]);
        filter_id_data.fwd_Key                  = MEA_OS_atoi(argv[result.fwd_start+1]);
        filter_id_data.fwd_enable_Internal_mac  = MEA_OS_atoi(argv[result.fwd_start + 2]);
        filter_id_data.force_fwd_rule           = MEA_OS_atoi(argv[result.fwd_start+3]);
    }
    if (result.bfwd_field_mask){
        filter_id_data.DSE_mask_field_type      = MEA_OS_atoi(argv[result.fwd_field_mask_start]);
    }
   
    if (result.bfwd_Act_enb){
        filter_id_data.fwd_Act_en = MEA_OS_atoi(argv[result.fwd_Act_enb_start]);
    }

    if (result.bFrag_IP_paramter == MEA_TRUE) {
        filter_id_data.action_params.Fragment_IP = MEA_OS_atoi(argv[result.Frag_IP_paramter]);
        filter_id_data.action_params.IP_TUNNEL = MEA_OS_atoi(argv[result.Frag_IP_paramter + 1]);
        filter_id_data.action_params.fragment_ext_int = MEA_OS_atoi(argv[result.Frag_IP_paramter + 2]);
        filter_id_data.action_params.fragment_target_mtu_prof = MEA_OS_atoi(argv[result.Frag_IP_paramter + 3]);
    }
    if (result.bDeFrag_IP_paramter == MEA_TRUE) {
        filter_id_data.action_params.Defrag_ext_IP = MEA_OS_atoi(argv[result.DeFrag_IP_paramter]);
        filter_id_data.action_params.Defrag_int_IP = MEA_OS_atoi(argv[result.DeFrag_IP_paramter + 1]);
        filter_id_data.action_params.Defrag_Reassembly_L2_profile = MEA_OS_atoi(argv[result.DeFrag_IP_paramter + 2]);

    }
    
    
    if(result.bhpm_cos == MEA_TRUE){
        filter_id_data.hpm_cos_valid = MEA_OS_atoi(argv[result.hpm_cos_pram]);
        filter_id_data.hpm_cos = MEA_OS_atoi(argv[result.hpm_cos_pram+1]);
    }
     
    
    

    //     filter_id_data.action_params.proto_llc_valid = /*result.bProtocolLlc*/MEA_TRUE;
    //     filter_id_data.action_params.protocol_llc_force = result.bProtocolLlc;
    //     filter_id_data.action_params.Protocol = result.protocol_llc_val & 0x3;
    //     filter_id_data.action_params.Llc = (result.protocol_llc_val & 0x4) >> 2;



    if (result.num_of_output > 0)
        filter_id_data.action_params.output_ports_valid = MEA_TRUE;
    else
        filter_id_data.action_params.output_ports_valid = MEA_FALSE;


    /* set editing if needed */
    if (result.num_of_editing > 0)
    {
        ehp_array.num_of_entries = result.num_of_editing;
        ehp_array.ehp_info = MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt)* result.num_of_editing);
        if (ehp_array.ehp_info == NULL)
        {
            CLI_print("ERROR: can not allocate %d bytes for %d editors\n", sizeof(MEA_EHP_Info_dbt)* result.num_of_editing, result.num_of_editing);
            return MEA_OK;
        }
        MEA_OS_memset(ehp_array.ehp_info, 0, sizeof(MEA_EHP_Info_dbt)* result.num_of_editing);


        if (result.num_of_editing == 1 ||
            result.num_of_editing != result.num_of_output)
        {
            is_ehp_unicast = MEA_TRUE;
        }
        else
        {
            is_ehp_unicast = MEA_FALSE;
        }

        if (mea_filterGetEditing(argc, argv, i, &(result.out_ports_tbl_entry), &ehp_array, is_ehp_unicast) == MEA_ERROR)
        {
            if (ehp_array.ehp_info)
                MEA_OS_free(ehp_array.ehp_info);
            return MEA_OK;
        }

    }
    if (result.mtu_Flag){
        filter_id_data.action_params.mtu_Id = result.mtu_Id;

    }


    if (MEA_API_Create_Filter(MEA_UNIT_0,
        &filter_id_key,
        NULL,
        &filter_id_data,
        (result.num_of_output > 0) ? &(result.out_ports_tbl_entry) : &(result.out_ports_tbl_entry),   //NULL,    
        (result.Policing_t.bPolicing || result.bPolicerProc) ? &sla_params_entry : NULL,
        (result.num_of_editing > 0) ? &ehp_array : NULL,
        &filter_id) != MEA_OK)
    {
        if (ehp_array.ehp_info)
            MEA_OS_free(ehp_array.ehp_info);

        CLI_print("Error: MEA_API_Create_Filter Failed \n");
        return MEA_OK;
    }

    if (ehp_array.ehp_info)
        MEA_OS_free(ehp_array.ehp_info);


    if (filter_id_data.action_type == MEA_FILTER_ACTION_TO_ACTION){
        CLI_print("Done. FilterId=%d ACL%d ActioId=%d    (PmId=[%s]=%d,tmId=[%s]=%d,edId=[%s]=%d)\n",
            filter_id,
            filter_id_key.Acl_hierarchical_id,
            filter_id_data.Action_Id,
            MEA_STATUS_STR(filter_id_data.action_params.pm_id_valid), filter_id_data.action_params.pm_id,
            MEA_STATUS_STR(filter_id_data.action_params.tm_id_valid), filter_id_data.action_params.tm_id,
            MEA_STATUS_STR(filter_id_data.action_params.ed_id_valid), filter_id_data.action_params.ed_id);
    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_ModifyFilter(int argc, char *argv[])
{
    MEA_Filter_Key_dbt                filter_id_key;
    MEA_Filter_Data_dbt               filter_id_data;
    MEA_Filter_Data_dbt               curr_filter_id_data;
    MEA_Policer_Entry_dbt             sla_params_entry;
    MEA_OutPorts_Entry_dbt            OutPorts_Entry;
    MEA_EgressHeaderProc_Entry_dbt  ehp_entry;
    MEA_EgressHeaderProc_Array_Entry_dbt ehp_array;
    MEA_EHP_Info_dbt                ehp_info;
    filterCommandResault_s         result;
    MEA_Filter_t                 filter_id;
    int           mandatory_argc = 3;
    MEA_Uint32                     i;




    if (argc < mandatory_argc) return MEA_ERROR;

    MEA_OS_memset(&result, 0, sizeof(result));
    MEA_OS_memset(&ehp_array, 0, sizeof(ehp_array));
    MEA_OS_memset(&ehp_info, 0, sizeof(ehp_info));
    MEA_OS_memset(&filter_id_data, 0, sizeof(filter_id_data));
    MEA_OS_memset(&ehp_entry, 0, sizeof(ehp_entry));
    MEA_OS_memset(&filter_id_key, 0, sizeof(filter_id_key));
    MEA_OS_memset(&filter_id_data, 0, sizeof(filter_id_data));
    MEA_OS_memset(&sla_params_entry, 0, sizeof(sla_params_entry));
    MEA_OS_memset(&curr_filter_id_data, 0, sizeof(curr_filter_id_data));
    MEA_OS_memset(&OutPorts_Entry, 0, sizeof(OutPorts_Entry));


    filter_id = MEA_OS_atoi(argv[1]);

    ehp_array.ehp_info = NULL;
    if (MEA_API_Get_Filter(MEA_UNIT_0,
        filter_id,
        &filter_id_key,
        NULL,
        &curr_filter_id_data,
        &OutPorts_Entry,
        &sla_params_entry,
        &ehp_array) != MEA_OK)
    {
        return MEA_ERROR;
    }

    if (ehp_array.num_of_entries != 0)
    {
        ehp_array.ehp_info = (MEA_EHP_Info_dbt*)
            MEA_OS_malloc(
            sizeof(MEA_EHP_Info_dbt)* ehp_array.num_of_entries);

        if (ehp_array.ehp_info == NULL)
            return MEA_ERROR;
    }

    /* Call again GET */
    if (MEA_API_Get_Filter(MEA_UNIT_0,
        filter_id,
        &filter_id_key,
        NULL,
        &filter_id_data,
        &OutPorts_Entry,
        &sla_params_entry,
        (ehp_array.num_of_entries == 0) ? NULL : (&ehp_array)) != MEA_OK)
    {
        if (ehp_array.ehp_info)
            MEA_OS_free(ehp_array.ehp_info);

        return MEA_ERROR;
    }


    for (i = 1; i < (MEA_Uint32)argc; i++) {

        if ((argv[i][0] == '-') && (
            /*(MEA_OS_strcmp(argv[i],"-sp")        != 0) &&  */ /* not allow to change THE KEY*/
            /*(MEA_OS_strcmp(argv[i],"-sid")        != 0) && */ /* not allow to change THE KEY*/
            (MEA_OS_strcmp(argv[i], "-col") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pri") != 0) &&
            (MEA_OS_strcmp(argv[i], "-cos") != 0) &&
            (MEA_OS_strcmp(argv[i], "-cos-cmd") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pri-cmd") != 0) &&
            (MEA_OS_strcmp(argv[i], "-col-cmd") != 0) &&
           
            (MEA_OS_strcmp(argv[i], "-cos-map") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pri-map") != 0) &&



            (MEA_OS_strcmp(argv[i], "-pol") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pol_id") != 0) &&
            (MEA_OS_strcmp(argv[i], "-policer") != 0) &&
            (MEA_OS_strcmp(argv[i], "-policer_mode") != 0) &&
            (MEA_OS_strcmp(argv[i], "-ra") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pgt") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pgt_sign") != 0) &&
            (MEA_OS_strcmp(argv[i], "-f_ED") != 0) &&
            (MEA_OS_strcmp(argv[i], "-Drop") != 0) &&
            (MEA_OS_strcmp(argv[i], "-overRule_vp") != 0) &&
            (MEA_OS_strcmp(argv[i], "-frag_IP")     != 0) &&
            (MEA_OS_strcmp(argv[i], "-defrag_IP") != 0) &&
            (MEA_OS_strcmp(argv[i], "-sflow_type") != 0) &&
            


            /***************************************************************/
            /*                                                             */
            /***************************************************************/

            (MEA_OS_strcmp(argv[i], "-h") != 0) &&
            (MEA_OS_strcmp(argv[i], "-hType") != 0) &&
            (MEA_OS_strcmp(argv[i], "-hvxlan_DL") != 0) &&
            (MEA_OS_strcmp(argv[i], "-hwbrg") != 0) &&
			(MEA_OS_strcmp(argv[i], "-hwmode") != 0) &&
            (MEA_OS_strcmp(argv[i], "-h802_1p") != 0) &&
            (MEA_OS_strcmp(argv[i], "-h2") != 0) &&
            (MEA_OS_strcmp(argv[i], "-ho") != 0) &&
            (MEA_OS_strcmp(argv[i], "-hs") != 0) &&
            (MEA_OS_strcmp(argv[i], "-hm") != 0) &&
            (MEA_OS_strcmp(argv[i], "-hom") != 0) &&
            (MEA_OS_strcmp(argv[i], "-lmid") != 0) &&
            (MEA_OS_strcmp(argv[i], "-hdscp") != 0) &&
            (MEA_OS_strcmp(argv[i], "-s") != 0) &&
            (MEA_OS_strcmp(argv[i], "-a") != 0) &&
            (MEA_OS_strcmp(argv[i], "-m") != 0) &&
            (MEA_OS_strcmp(argv[i], "-r") != 0) &&
            (MEA_OS_strcmp(argv[i], "-Pm") != 0) &&
            (MEA_OS_strcmp(argv[i], "-pm_type") != 0) &&
            (MEA_OS_strcmp(argv[i], "-Tm") != 0) &&
            (MEA_OS_strcmp(argv[i], "-Ed") != 0) &&

            (MEA_OS_strcmp(argv[i], "-set1588") != 0) &&
            (MEA_OS_strcmp(argv[i], "-afdx") != 0) &&
            (MEA_OS_strcmp(argv[i], "-outNULL") != 0) &&
            (MEA_OS_strcmp(argv[i], "-action_type") != 0) &&
            (MEA_OS_strcmp(argv[i], "-lxcp_win") != 0) &&
            (MEA_OS_strcmp(argv[i], "-o") != 0))){
            CLI_print(" ERROR Not support option %s \n", argv[i]);
            return MEA_OK;
        }
    }
    filter_id_data.action_type = curr_filter_id_data.action_type;
    /* Get general options */
    if (mea_filterGetGeneralOptions(argc, argv, 2, &result) == MEA_ERROR)
    {
        CLI_print(
            "%s - mea_filterGetGeneralOptions failed\n",
            __FUNCTION__);
        return MEA_OK;
    }

    if (result.baction_type == MEA_TRUE){
        filter_id_data.action_type = result.action_type;

    }

    /****** 3.set non editor commands ******/

    if (result.source_port_flag)
    {
        CLI_print("%s - you can't change interface key  port failed\n",
            __FUNCTION__);
        return MEA_OK;
    }
    if (result.sid_flag)
    {
        CLI_print("%s - you can't change interface key  sid failed\n",
            __FUNCTION__);
        return MEA_OK;
    }

    if (result.bRate_enable){
        filter_id_data.action_params.tmId_disable = ((MEA_OS_atoi(argv[result.rate_enable]) == MEA_TRUE) ? MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL : MEA_SRV_RATE_MEETRING_DISABLE_DEF_VAL);
    }

    if (result.bPolicerProc)
    {
        sla_params_entry.CIR = MEA_OS_atoiNum64(argv[result.policer_proc]);
        sla_params_entry.EIR = MEA_OS_atoiNum64(argv[result.policer_proc + 1]);
        sla_params_entry.CBS = MEA_OS_atoiNum(argv[result.policer_proc + 2]);
        sla_params_entry.EBS = MEA_OS_atoiNum(argv[result.policer_proc + 3]);
        sla_params_entry.cup = MEA_OS_atoiNum(argv[result.policer_proc + 4]);
        sla_params_entry.color_aware = MEA_OS_atoiNum(argv[result.policer_proc + 5]);
        if (MEA_OS_atoi(argv[result.policer_proc + 6]) > MEA_POLICER_COMP_ENFORCE){
            CLI_print("Error: -pol the comp is bigger then %d \n", MEA_POLICER_COMP_ENFORCE);
            return MEA_OK;
        }

        sla_params_entry.comp = MEA_OS_atoiNum(argv[result.policer_proc + 6]);
    }
    if (result.Policing_t.bPolicing == MEA_TRUE){
        sla_params_entry.CIR = result.Policing_t.CIR;
        sla_params_entry.EIR = result.Policing_t.EIR;
        sla_params_entry.CBS = result.Policing_t.CBS;
        sla_params_entry.EBS = result.Policing_t.EBS;
        sla_params_entry.cup = result.Policing_t.cup;
        sla_params_entry.color_aware = result.Policing_t.color_aware;
        sla_params_entry.comp = result.Policing_t.comp;
    }
    if (result.bgn_type) {
        if (!result.bPolicerProc && result.Policing_t.bPolicing)
        {
            CLI_print("Error: -pgt must come with -pol -policer option \n");
            return MEA_OK;
        }
        sla_params_entry.gn_type = MEA_OS_atoi(argv[result.gn_type]);
    }
    if (result.bpolicer_mode) {
        if (!result.bPolicerProc && !result.Policing_t.bPolicing)
        {
            CLI_print("Error: -policer_mode must come with -pol -policer option \n");
            return MEA_OK;
        }
        sla_params_entry.type_mode = MEA_OS_atoi(argv[result.policer_mode]);
    }
    if (result.bpolicer_maxMtu) {
        if (!result.bPolicerProc && !result.Policing_t.bPolicing)
        {
            CLI_print("Error: -policer_maxMtu must come with -pol -policer option \n");
            return MEA_OK;
        }
        sla_params_entry.maxMtu = MEA_OS_atoi(argv[result.policer_maxMtu]);
    }

    if (result.bgn_sign) {
        if (!result.bPolicerProc && result.Policing_t.bPolicing)
        {
            CLI_print("Error: -pgt_sign must come with -pol -policer option \n");
            return MEA_OK;
        }
        sla_params_entry.gn_type = MEA_OS_atoi(argv[result.gn_sign]);
    }
    if (result.bset1588){
        filter_id_data.action_params.set_1588 = MEA_OS_atoi(argv[result.set1588]);
    }
    if (result.blag_bypass == MEA_TRUE){
        filter_id_data.action_params.lag_bypass = MEA_OS_atoi(argv[result.lag_bypass]);
    }

    if (result.bflood_ED == MEA_TRUE){
        filter_id_data.action_params.flood_ED = MEA_OS_atoi(argv[result.flood_ED]);
    }
    if (result.bDrop == MEA_TRUE){
        filter_id_data.action_params.Drop_en = MEA_OS_atoi(argv[result.Drop]);
    }
    if (result.bsflow == MEA_TRUE) {
        filter_id_data.action_params.sflow_Type = MEA_OS_atoi(argv[result.sflow_info]);
    }
    if (result.boverRule_vpValid == MEA_TRUE) {
        filter_id_data.action_params.overRule_vpValid = MEA_OS_atoi(argv[result.overRule_vp_start]);
        filter_id_data.action_params.overRule_vp_val = MEA_OS_atoi(argv[result.overRule_vp_start + 1]);
    }

    if (result.bFrag_IP_paramter == MEA_TRUE) {
        filter_id_data.action_params.Fragment_IP = MEA_OS_atoi(argv[result.Frag_IP_paramter]);
        filter_id_data.action_params.IP_TUNNEL = MEA_OS_atoi(argv[result.Frag_IP_paramter + 1]);
        filter_id_data.action_params.fragment_ext_int = MEA_OS_atoi(argv[result.Frag_IP_paramter + 2]);
        filter_id_data.action_params.fragment_target_mtu_prof = MEA_OS_atoi(argv[result.Frag_IP_paramter + 3]);

    }
    if (result.bDeFrag_IP_paramter == MEA_TRUE) {
            filter_id_data.action_params.Defrag_ext_IP = MEA_OS_atoi(argv[result.DeFrag_IP_paramter]);
            filter_id_data.action_params.Defrag_int_IP = MEA_OS_atoi(argv[result.DeFrag_IP_paramter + 1]);
            filter_id_data.action_params.Defrag_Reassembly_L2_profile = MEA_OS_atoi(argv[result.DeFrag_IP_paramter + 2]);
    
    }
   



    if (result.col_Flag)
    {
        filter_id_data.action_params.force_color_valid = result.col_Flag;
        filter_id_data.action_params.COLOR = result.col_Value;
    }

    if (result.cos_Flag)
    {
        filter_id_data.action_params.force_cos_valid = result.cos_Flag;
        filter_id_data.action_params.COS = result.cos_Value;
    }

    if (result.pri_Flag)
    {
        filter_id_data.action_params.force_l2_pri_valid = result.pri_Flag;
        filter_id_data.action_params.L2_PRI = result.pri_Value;
    }
    if (result.cosCmd.bCmd == MEA_TRUE){
        filter_id_data.action_params.force_cos_valid = result.cosCmd.cmd;
        filter_id_data.action_params.COS = result.cosCmd.value;
    }
    if (result.priCmd.bCmd == MEA_TRUE){
        filter_id_data.action_params.force_l2_pri_valid = result.priCmd.cmd;
        filter_id_data.action_params.L2_PRI = result.priCmd.value;
    }

    if (result.colCmd.bCmd == MEA_TRUE){
        filter_id_data.action_params.force_color_valid = result.colCmd.cmd;
        filter_id_data.action_params.COLOR = result.colCmd.value;
    }
    if (result.flowCoSMapping_flag == MEA_TRUE) {
        filter_id_data.action_params.flowCoSMappingProfile_force = result.flowCoSMapping_force;
        filter_id_data.action_params.flowCoSMappingProfile_id = result.flowCoSMappingProfile_id;
    }

    if (result.flowMarkingMapping_flag == MEA_TRUE) {
        filter_id_data.action_params.flowMarkingMappingProfile_force = result.flowMarkingMapping_force;
        filter_id_data.action_params.flowMarkingMappingProfile_id = result.flowMarkingMappingProfile_id;
    }



    if (result.Ed_Flag)
    {
        filter_id_data.action_params.ed_id_valid = result.Ed_Flag;
        filter_id_data.action_params.ed_id = result.Ed_Value;
    }

    if (result.Tm_Flag)
    {
        filter_id_data.action_params.tm_id_valid = result.Tm_Flag;
        filter_id_data.action_params.tm_id = result.TM_Value;
    }
    if (result.bPolicerProc && filter_id_data.action_params.tm_id_valid == MEA_FALSE){
        CLI_print("Error: -pol option need also tm Id\n");
        return MEA_OK;
    }
    if (result.pol_Id_Flag)
    {
        filter_id_data.action_params.policer_prof_id_valid = result.pol_Id_Flag;
        filter_id_data.action_params.policer_prof_id = result.pol_Id;
    }

    if (result.Pm_Flag)
    {
        filter_id_data.action_params.pm_id_valid = result.Pm_Flag;
        filter_id_data.action_params.pm_id = result.PM_Value;
    }
    if (result.pm_type_Flag == 1){
        filter_id_data.action_params.pm_type = result.pm_type_value;
    }
    if (result.bprotocol_enable){
        filter_id_data.action_params.protocol_llc_force = MEA_OS_atoi(argv[result.protocol_force]);
        filter_id_data.action_params.Protocol = MEA_OS_atoi(argv[result.protocol]);
    }
    if (result.bllc_enable){

        filter_id_data.action_params.proto_llc_valid = MEA_OS_atoi(argv[result.llc_valid]);
        filter_id_data.action_params.Llc = MEA_OS_atoi(argv[result.llc]);
    }

    if (result.num_of_output > 0){
        filter_id_data.action_params.output_ports_valid = MEA_TRUE;
    }

    if (result.boutPort_null == MEA_TRUE){
        filter_id_data.action_params.output_ports_valid = MEA_FALSE;
    }
    if (result.bafdx){

        filter_id_data.action_params.afdx_enable = MEA_OS_atoi(argv[result.afdx_start]);
        filter_id_data.action_params.afdx_Rx_sessionId = MEA_OS_atoi(argv[result.afdx_start + 1]);
        filter_id_data.action_params.afdx_A_B = MEA_OS_atoi(argv[result.afdx_start + 2]);
        filter_id_data.action_params.afdx_Bag_info = MEA_OS_atoi(argv[result.afdx_start + 3]);
    }
    if (result.bsetlxcp_win){
        filter_id_data.lxcp_win = MEA_OS_atoi(argv[result.setlxcp_win]);
    }

    if (result.bhpm_cos == MEA_TRUE) {
        filter_id_data.hpm_cos_valid = MEA_OS_atoi(argv[result.hpm_cos_pram]);
        filter_id_data.hpm_cos = MEA_OS_atoi(argv[result.hpm_cos_pram + 1]);
    }


    if (MEA_API_Set_Filter(MEA_UNIT_0,
        filter_id,
        &filter_id_data,
        (result.boutPort_null == MEA_TRUE) ? NULL : (result.num_of_output > 0) ? &(result.out_ports_tbl_entry) : &OutPorts_Entry,
        &sla_params_entry,
        (result.num_of_editing > 0) ? &ehp_array : NULL) != MEA_OK)
    {
        if (ehp_array.ehp_info)
            MEA_OS_free(ehp_array.ehp_info);
        CLI_print("Error: MEA_API_Set_Filter Failed\n");
        return MEA_OK;
    }

    if (ehp_array.ehp_info)
        MEA_OS_free(ehp_array.ehp_info);

    return MEA_OK;
}


MEA_Filter_Key_Type_te MEA_CLI_get_filter_key_type(MEA_Filter_Key_dbt *Filter_Key)
{
    return  mea_filter_get_key_type(Filter_Key);
}



MEA_Status MEA_CLI_Show_FilterParam(MEA_Filter_t   filterId,
    MEA_Bool      show_title)
{

    MEA_Filter_Key_Type_te key_type;
    MEA_Filter_Key_dbt            Filter_Key;
    MEA_Filter_Data_dbt            Filter_Data;
    MEA_Bool                   valid;
    MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;
    MEA_OutPorts_Entry_dbt               OutPorts_Entry;
    MEA_Policer_Entry_dbt                Policer_entry;
    char                        ip_str[20];
    MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));
    MEA_OS_memset(&Filter_Key, 0, sizeof(Filter_Key));
    MEA_OS_memset(&Filter_Data, 0, sizeof(Filter_Data));
    MEA_OS_memset(&OutPorts_Entry, 0, sizeof(OutPorts_Entry));
    MEA_OS_memset(&Policer_entry, 0, sizeof(Policer_entry));

    unsigned char buf[sizeof(struct in6_addr)];
    //int  s;
    char str[INET6_ADDRSTRLEN];

    

   

    /* check if the filter exist */
    if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0, filterId, &valid) != MEA_OK) {
        CLI_print("Error: MEA_API_IsExist_Filter_ById for filter Id %d failed\n",
            filterId);
        return MEA_OK;
    }
    if (valid == MEA_FALSE) {
        CLI_print("Error: filterId %d does not exists\n", filterId);
        return MEA_OK;
    }


    /* call GET just to retrieve the EHP number*/
    EHP_Entry.ehp_info = NULL;

    if (MEA_API_Get_Filter(MEA_UNIT_0,
        filterId,
        &Filter_Key,
        NULL,
        &Filter_Data,
        NULL,
        NULL,
        &EHP_Entry) != MEA_OK)
    {
        return MEA_ERROR;
    }

    /* if EHP num_of_entries is not 0, allocate as needed.
    otherwise, can pass NULL to EHP
    */
    if (EHP_Entry.num_of_entries != 0)
    {
        EHP_Entry.ehp_info = (MEA_EHP_Info_dbt*)
            MEA_OS_malloc(
            sizeof(MEA_EHP_Info_dbt)* EHP_Entry.num_of_entries);

        if (EHP_Entry.ehp_info == NULL)
            return MEA_ERROR;
    }

    /* Call again GET */
    if (MEA_API_Get_Filter(MEA_UNIT_0,
        filterId,
        &Filter_Key,
        NULL,
        &Filter_Data,
        &OutPorts_Entry,
        &Policer_entry,
        (EHP_Entry.num_of_entries == 0) ? NULL : (&EHP_Entry)) != MEA_OK)
    {
        if (EHP_Entry.ehp_info)
            MEA_OS_free(EHP_Entry.ehp_info);

        return MEA_ERROR;
    }



    /* show title for the output table */
    if (show_title) {
        CLI_print("ID   ACL   Key                                                    sp/       ACT \n"
                  "      H    Type                                                   sid       TYP  \n"
                  "---- ----- ------------------------------------------------------ ---- ---- --- ----------------------------------\n");
    }


    CLI_print("%04d ", filterId);
    CLI_print("Acl-%d ", Filter_Key.Acl_hierarchical_id);
    
    key_type = MEA_CLI_get_filter_key_type(&Filter_Key);
    CLI_print("(%2d) %-54s ", key_type, mea_drv_acl_Get_Filter_Key_Type_STR(key_type));
    if (Filter_Key.interface_key.src_port_valid)
        CLI_print("Port %4d ", Filter_Key.interface_key.src_port);
    else
        CLI_print("PROF %4d ", Filter_Key.interface_key.acl_prof);

    CLI_print("%-3s ", MEA_drv_acl_Get_Filter_Action_STR(Filter_Data.action_type));
    if (Filter_Data.action_params.pm_id_valid)
        CLI_print("Pm:%8d ", Filter_Data.action_params.pm_id);
    else
        CLI_print("Pm:%8s ", "NA");
    
    if (Filter_Data.action_params.tm_id_valid)
        CLI_print("TM:%8d ", Filter_Data.action_params.tm_id);
    else
        CLI_print("TM:%8s ", "NA");

    if (Filter_Data.action_params.ed_id_valid)
        CLI_print("Ed:%8d ", Filter_Data.action_params.ed_id);
    else
        CLI_print("Ed:%8s ", "NA");
    
   

    CLI_print("mtuId:%4d ", Filter_Data.action_params.mtu_Id);
	CLI_print("lxcp_win:%3s ", MEA_STATUS_STR(Filter_Data.lxcp_win));
    if(Filter_Data.hpm_cos_valid == MEA_TRUE)
        CLI_print("hpm:%3s cos:%3d \n", MEA_STATUS_STR(Filter_Data.hpm_cos_valid), Filter_Data.hpm_cos);
    else
        CLI_print("hpm:%3s cos:%3s \n", MEA_STATUS_STR(Filter_Data.hpm_cos_valid), "NA");



    switch (MEA_CLI_get_filter_key_type(&Filter_Key))
    {
    case MEA_FILTER_KEY_TYPE_DA_MAC:
        CLI_print("        DA MAC:        %02x:%02x:%02x:%02x:%02x:%02x \n",
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[0]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[1]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[2]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[3]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[4]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[5]));
        break;
    case MEA_FILTER_KEY_TYPE_SA_MAC:
        CLI_print("        SA MAC:        %02x:%02x:%02x:%02x:%02x:%02x \n",
            (MEA_Uint8)(Filter_Key.layer2.ethernet.SA.b[0]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.SA.b[1]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.SA.b[2]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.SA.b[3]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.SA.b[4]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.SA.b[5]));
        break;
    case MEA_FILTER_KEY_TYPE_DST_IPV4_DST_PORT:
        CLI_print("        DST IPv4:        %s\n", MEA_OS_inet_ntoa(MEA_OS_htonl(Filter_Key.layer3.IPv4.dst_ip), ip_str, 20));
        CLI_print("        DST Port:      %d\n", Filter_Key.layer4.dst_port);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV4_DST_PORT:
        CLI_print("        SRC IPv4:        %s\n", MEA_OS_inet_ntoa(MEA_OS_htonl(Filter_Key.layer3.IPv4.src_ip), ip_str, 20));
        CLI_print("        DST Port:      %d\n", Filter_Key.layer4.dst_port);
        break;
    case MEA_FILTER_KEY_TYPE_DST_IPV4_SRC_PORT:
        CLI_print("        DST IPv4:        %s\n", MEA_OS_inet_ntoa(MEA_OS_htonl(Filter_Key.layer3.IPv4.dst_ip), ip_str, 20));
        CLI_print("        SRC Port:      %d\n", Filter_Key.layer4.src_port);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV4_SRC_PORT:
        CLI_print("        SRC IPv4:        %s\n", MEA_OS_inet_ntoa(MEA_OS_htonl(Filter_Key.layer3.IPv4.src_ip), ip_str, 20));
        CLI_print("        SRC Port:      %d\n", Filter_Key.layer4.src_port);
        break;
    case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_INNER_ETHERTYPE_OUTER_VLAN_TAG:
        CLI_print("        Outer EthType: 0x%04x\n", Filter_Key.layer2.outer_vlanTag.ethertype );
        CLI_print("        Inner EthType: 0x%04x\n", Filter_Key.layer2.inner_vlanTag.ethertype);
        CLI_print("        Outer Vlan:     %d\n", Filter_Key.layer2.outer_vlanTag.vlan);
        CLI_print("        Outer Vlan CFI: %d\n", Filter_Key.layer2.outer_vlanTag.cf);
        CLI_print("        Outer Vlan Pri: %d\n", Filter_Key.layer2.outer_vlanTag.priority);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV4_APPL_ETHERTYPE:
        CLI_print("        SRC  IPv4:        %s\n", MEA_OS_inet_ntoa(MEA_OS_htonl(Filter_Key.layer3.IPv4.src_ip), ip_str, 20));
        CLI_print("        Appl EthType:  0x%04x\n", Filter_Key.layer3.ethertype3);
        break;
    case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_APPL_ETHERTYPE_DST_PORT:
        if (Filter_Key.layer2.outer_vlanTag.priority_valid == MEA_TRUE){
            CLI_print("        PRI TYPE        %s\n", "Outer Vlan-tag priority");
            CLI_print("        PRI value       %d\n", (Filter_Key.layer2.outer_vlanTag.priority));
        }
        else if (Filter_Key.layer2.inner_vlanTag.priority_valid == MEA_TRUE){
            CLI_print("        PRI TYPE        %s\n", "Inner Vlan-tag priority");
            CLI_print("        PRI value       %d\n", (Filter_Key.layer2.inner_vlanTag.priority));
        }
        else if (Filter_Key.layer3.l3_pri_valid){
            switch (Filter_Key.layer3.l3_pri_type){
            case MEA_FILTER_L3_TYPE_IPV4_TOS:
                CLI_print("        PRI TYPE        %s\n", "IPV4 TOS");
                CLI_print("        PRI value       %d\n", (Filter_Key.layer3.l3_pri_value.ipv4_tos.tos));
                break;
            case MEA_FILTER_L3_TYPE_IPV4_PRECEDENCE:
                CLI_print("        PRI TYPE        %s\n", "IPV4 PRECEDENCE");
                CLI_print("        PRI value       %d\n", (Filter_Key.layer3.l3_pri_value.ipv4_precedence.precedence));
                break;
            case MEA_FILTER_L3_TYPE_IPV4_DSCP:

                CLI_print("        PRI TYPE        %s\n", "IPV4 DSCP");
                CLI_print("        PRI value       %d\n", (Filter_Key.layer3.l3_pri_value.ipv4_dscp.dscp));
                break;
            case MEA_FILTER_L3_TYPE_IPV6_TC:
            case MEA_FILTER_L3_TYPE_LAST:
                break;
            }
        }
        CLI_print("        IP   Protocol:    %03d\n", Filter_Key.layer3.ip_protocol);
        CLI_print("        Appl EthType:     0x%04x\n", Filter_Key.layer3.ethertype3);
        CLI_print("        DST  Port:       %d\n", Filter_Key.layer4.dst_port);

        break;
    case MEA_FILTER_KEY_TYPE_PRI_IP_PROTO_SRC_PORT_DST_PORT:
        if (Filter_Key.layer2.outer_vlanTag.priority_valid == MEA_TRUE){
            CLI_print("        PRI TYPE        %s\n", "Outer Vlan-tag priority");
            CLI_print("        PRI value       %d\n", (Filter_Key.layer2.outer_vlanTag.priority));
        }
        else if (Filter_Key.layer2.inner_vlanTag.priority_valid == MEA_TRUE){
            CLI_print("        PRI TYPE        %s\n", "Inner Vlan-tag priority");
            CLI_print("        PRI value       %d\n", (Filter_Key.layer2.inner_vlanTag.priority));
        }
        else if (Filter_Key.layer3.l3_pri_valid){
            switch (Filter_Key.layer3.l3_pri_type){
            case MEA_FILTER_L3_TYPE_IPV4_TOS:
                CLI_print("        PRI TYPE        %s\n", "IPV4 TOS");
                CLI_print("        PRI value       %d\n", (Filter_Key.layer3.l3_pri_value.ipv4_tos.tos));
                break;
            case MEA_FILTER_L3_TYPE_IPV4_PRECEDENCE:
                CLI_print("        PRI TYPE        %s\n", "IPV4 PRECEDENCE");
                CLI_print("        PRI value       %d\n", (Filter_Key.layer3.l3_pri_value.ipv4_precedence.precedence));
                break;
            case MEA_FILTER_L3_TYPE_IPV4_DSCP:

                CLI_print("        PRI TYPE        %s\n", "IPV4 DSCP");
                CLI_print("        PRI value       %d\n", (Filter_Key.layer3.l3_pri_value.ipv4_dscp.dscp));
                break;
            case MEA_FILTER_L3_TYPE_IPV6_TC:
            case MEA_FILTER_L3_TYPE_LAST:
                break;
            }
        }
        CLI_print("        IP Protocol:    %03d\n", Filter_Key.layer3.ip_protocol);
        CLI_print("        SRC Port:       %d\n", Filter_Key.layer4.src_port);
        CLI_print("        DST Port:       %d\n", Filter_Key.layer4.dst_port);

        break;
    case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_INNER_VLAN_TAG_OUTER_ETHERTYPE:
        CLI_print("        Outer VLAN Vlan: %d\n", Filter_Key.layer2.outer_vlanTag.vlan);
        CLI_print("        Outer VLAN CFI:  %d\n", Filter_Key.layer2.outer_vlanTag.cf);
        CLI_print("        Outer VLAN Pri:  %d\n", Filter_Key.layer2.outer_vlanTag.priority);
        CLI_print("        Inner VLAN Vlan: %d\n", Filter_Key.layer2.inner_vlanTag.vlan);
        CLI_print("        Inner VLAN CFI:  %d\n", Filter_Key.layer2.inner_vlanTag.cf);
        CLI_print("        Inner VLAN Pri:  %d\n", Filter_Key.layer2.inner_vlanTag.priority);
        CLI_print("        Outer EthType: 0x%04x\n", Filter_Key.layer2.outer_vlanTag.ethertype);
        break;
    case MEA_FILTER_KEY_TYPE_OUTER_VLAN_TAG_DST_PORT_SRC_PORT:
        CLI_print("        Outer VLAN Vlan: %d\n", Filter_Key.layer2.outer_vlanTag.vlan);
        CLI_print("        Outer VLAN CFI:  %d\n", Filter_Key.layer2.outer_vlanTag.cf);
        CLI_print("        Outer VLAN Pri:  %d\n", Filter_Key.layer2.outer_vlanTag.priority);
        CLI_print("        DST Port:        %d\n", Filter_Key.layer4.dst_port);
        CLI_print("        SRC Port:        %d\n", Filter_Key.layer4.src_port);
        break;
    case MEA_FILTER_KEY_TYPE_INNER_VLAN_TAG_DST_PORT_SRC_PORT:
        CLI_print("        Inner VLAN Vlan: %d\n", Filter_Key.layer2.inner_vlanTag.vlan);
        CLI_print("        Inner VLAN CFI:  %d\n", Filter_Key.layer2.inner_vlanTag.cf);
        CLI_print("        Inner VLAN Pri:  %d\n", Filter_Key.layer2.inner_vlanTag.priority);
        CLI_print("        DST Port:        %d\n", Filter_Key.layer4.dst_port);
        CLI_print("        SRC Port:        %d\n", Filter_Key.layer4.src_port);
        break;
    case MEA_FILTER_KEY_TYPE_PPPOE_SESSION_ID_PPP_PROTO_SA_MAC_LSB:
        CLI_print("        PPPoE Session ID: 0x%04x\n", Filter_Key.layer2.ppp.pppoe_session_id_from);
        CLI_print("        PPP Protocol:     0x%04x\n", Filter_Key.layer2.ppp.ppp_protocol);
        CLI_print("        SA MAC LSB:        %02x:%02x\n",
            (MEA_Uint8)(Filter_Key.layer2.ethernet.SA.b[4]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.SA.b[5]));
        break;
    case MEA_FILTER_KEY_TYPE_OUTER_ETHERTYPE_APPL_ETHERTYPE_OUTER_VLAN_TAG:
        CLI_print("        Outer EthType: 0x%04x\n", Filter_Key.layer2.outer_vlanTag.ethertype);
        CLI_print("        Appl  EthType: 0x%04x\n", Filter_Key.layer3.ethertype3);
        CLI_print("        Outer Vlan:     %d\n", Filter_Key.layer2.outer_vlanTag.vlan);
        CLI_print("        Outer Vlan CFI: %d\n", Filter_Key.layer2.outer_vlanTag.cf);
        CLI_print("        Outer Vlan Pri: %d\n", Filter_Key.layer2.outer_vlanTag.priority);
        break;

    case MEA_FILTER_KEY_TYPE_DA_MAC_OUTER_VLAN_TAG_DST_IPV4:
        CLI_print("        DA MAC:         %02x:%02x:%02x:%02x:%02x:%02x \n",
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[0]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[1]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[2]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[3]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[4]),
            (MEA_Uint8)(Filter_Key.layer2.ethernet.DA.b[5]));
        CLI_print("        Outer VLAN Vlan: %d\n", Filter_Key.layer2.outer_vlanTag.vlan);
        CLI_print("        Outer VLAN CFI:  %d\n", Filter_Key.layer2.outer_vlanTag.cf);
        CLI_print("        Outer VLAN Pri:  %d\n", Filter_Key.layer2.outer_vlanTag.priority);
        CLI_print("        DST IP:          %s\n", MEA_OS_inet_ntoa(MEA_OS_htonl(Filter_Key.layer3.IPv4.dst_ip), ip_str, 20));
        break;


    case MEA_FILTER_KEY_TYPE_DST_IPV6_DST_PORT:
        MEA_OS_ipv6_to_buf(&Filter_Key.layer3.IPv6.dst_ipv6[0], &buf[0]);


        if (inet_ntop(AF_INET6, buf, str, INET6_ADDRSTRLEN) == NULL) {
            CLI_print(">>>inet_ntop<<< \n");
        }
        CLI_print("        DST IPv6:      %s\n", str);
        CLI_print("        DST Port:      %d\n", Filter_Key.layer4.dst_port);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_DST_PORT:
        MEA_OS_ipv6_to_buf(&Filter_Key.layer3.IPv6.src_ipv6[0], &buf[0]);


        if (inet_ntop(AF_INET6, buf, str, INET6_ADDRSTRLEN) == NULL) {
            CLI_print(">>>inet_ntop<<< \n");
        }
        CLI_print("        SRC IPv6:        %s\n", str);
        CLI_print("        DST Port:      %d\n", Filter_Key.layer4.dst_port);
        break;
    case MEA_FILTER_KEY_TYPE_DST_IPV6_SRC_PORT:
        MEA_OS_ipv6_to_buf(&Filter_Key.layer3.IPv6.dst_ipv6[0], &buf[0]);


        if (inet_ntop(AF_INET6, buf, str, INET6_ADDRSTRLEN) == NULL) {
            CLI_print(">>>inet_ntop<<< \n");
        }
        CLI_print("        DST IPv6:        %s\n", str);
        CLI_print("        SRC Port:      %d\n", Filter_Key.layer4.src_port);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_SRC_PORT:
        MEA_OS_ipv6_to_buf(&Filter_Key.layer3.IPv6.src_ipv6[0], &buf[0]);


        if (inet_ntop(AF_INET6, buf, str, INET6_ADDRSTRLEN) == NULL) {
            CLI_print(">>>inet_ntop<<< \n");
        }
        CLI_print("        SRC IPv6:        %s\n", str);
        CLI_print("        SRC Port:      %d\n", Filter_Key.layer4.src_port);
        break;
    case MEA_FILTER_KEY_TYPE_SRC_IPV6_APPL_ETHERTYPE:
        MEA_OS_ipv6_to_buf(&Filter_Key.layer3.IPv6.src_ipv6[0], &buf[0]);


        if (inet_ntop(AF_INET6, buf, str, INET6_ADDRSTRLEN) == NULL) {
            CLI_print(">>>inet_ntop<<< \n");
        }
        CLI_print("        SRC  IPv6:        %s\n", str);
        CLI_print("        Appl EthType:  0x%04x\n", Filter_Key.layer3.ethertype3);
        break;


        




    default:
        CLI_print("\n");
    }
    CLI_print("-------------------------------------------------------------------------------\n");

    return MEA_OK;
}


static MEA_Status MEA_CLI_Show_FilterParamAll(void)
{
    MEA_Filter_t      filter_id;
    MEA_Bool          found, show_title;
    MEA_Uint32        count = 0;
#if 0
    if (argc<2) {
        return MEA_ERROR;
    }
#endif
    found = MEA_FALSE;

    show_title = MEA_TRUE;
    if (MEA_API_GetFirst_Filter(MEA_UNIT_0, &filter_id, &found) != MEA_OK)
    {
        return MEA_ERROR;
    }


    while (found != MEA_FALSE)
    {
        count++;
        if (MEA_CLI_Show_FilterParam(filter_id, show_title) != MEA_OK)
        {
            CLI_print(
                "%s - MEA_CLI_Show_FilterParam(%d) Failed\n",
                __FUNCTION__, filter_id);
            return MEA_ERROR;
        }
        show_title = MEA_FALSE;

        if (MEA_API_GetNext_Filter(MEA_UNIT_0, &filter_id, &found) != MEA_OK)
        {
            CLI_print(
                "%s - MEA_API_GetNext_Filter(%d) Failed\n",
                __FUNCTION__, filter_id);
            return MEA_ERROR;
        }
    }

    if (count == 0)
        CLI_print("Filter table is empty\n");

    return MEA_OK;
}

static MEA_Status MEA_CLI_ShowFilter(int argc, char *argv[])
{
    if (argc<2) return MEA_ERROR;

    if (!MEA_OS_strcmp(argv[1], "all"))
        return MEA_CLI_Show_FilterParamAll();
    else
        return MEA_CLI_Show_FilterParam((MEA_Filter_t)MEA_OS_atoi(argv[1]), MEA_TRUE);
}



MEA_Status MEA_CLI_Show_FilterEditing(MEA_Filter_t   filterId,
    MEA_Bool      show_title,
    mea_cli_editing_info_te info)
{

    MEA_Filter_Key_dbt            Filter_Key;
    MEA_Filter_Data_dbt            Filter_Data;
    MEA_Bool                   valid;
    MEA_Uint32 i;
    ENET_QueueId_t outqueue;
    MEA_Uint32 *pPortGrp;
    MEA_Uint32  shiftWord = 0x00000001;
    MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;
    MEA_OutPorts_Entry_dbt               OutPorts_Entry;
    MEA_Policer_Entry_dbt                Policer_entry;
    char        *str_lmid[] = {
        "TRANSPARENT",
        "SWAP_OAM   ",
        "STAMP_TX   ",
        "STAMP_RX   ",
        "CCM_TX     ",
        "CCM_RX     " };


    char         *buff[] = { "transp  ",

        "append  ",

        "Extract ",

        "swap    ",

        "swap MAC",

        "Error   " };

    MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));
    MEA_OS_memset(&Filter_Key, 0, sizeof(Filter_Key));
    MEA_OS_memset(&Filter_Data, 0, sizeof(Filter_Data));
    MEA_OS_memset(&OutPorts_Entry, 0, sizeof(OutPorts_Entry));
    MEA_OS_memset(&Policer_entry, 0, sizeof(Policer_entry));


    /* check if the filter exist */
    if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0, filterId, &valid) != MEA_OK) {
        CLI_print("Error: MEA_API_IsExist_Filter_ById for filter Id %d failed\n",
            filterId);
        return MEA_OK;
    }
    if (valid == MEA_FALSE) {
        CLI_print("Error: filterId %d does not exists\n", filterId);
        return MEA_OK;
    }


    /* call GET just to retrieve the EHP number*/
    EHP_Entry.ehp_info = NULL;

    if (MEA_API_Get_Filter(MEA_UNIT_0,
        filterId,
        &Filter_Key,
        NULL,
        &Filter_Data,
        NULL,
        NULL,
        &EHP_Entry) != MEA_OK)
    {
        return MEA_ERROR;
    }

    /* if EHP num_of_entries is not 0, allocate as needed.
    otherwise, can pass NULL to EHP
    */
    if (EHP_Entry.num_of_entries != 0)
    {
        EHP_Entry.ehp_info = (MEA_EHP_Info_dbt*)
            MEA_OS_malloc(
            sizeof(MEA_EHP_Info_dbt)* EHP_Entry.num_of_entries);

        if (EHP_Entry.ehp_info == NULL)
            return MEA_ERROR;
    }

    /* Call again GET */
    if (MEA_API_Get_Filter(MEA_UNIT_0,
        filterId,
        &Filter_Key,
        NULL,
        &Filter_Data,
        &OutPorts_Entry,
        &Policer_entry,
        (EHP_Entry.num_of_entries == 0) ? NULL : (&EHP_Entry)) != MEA_OK)
    {
        if (EHP_Entry.ehp_info)
            MEA_OS_free(EHP_Entry.ehp_info);

        return MEA_ERROR;
    }



    if (EHP_Entry.num_of_entries > 0)
    {

        if (info == MEA_CLI_EDITING_INFO_EDITING)
        {
            /* show title for the output table */
            if (show_title) {
             
          CLI_print("FID    Inner      Inner Inner    Inner   Outer      Outer Outer    Outer    Stamp WBRG   Edit     HWmode  Qtag/  \n"
                    "       Value      Stamp Command  Mapping Value      Stamp Command  Mapping  802.p Info   Type             Vlan   \n"
                    "                  C / P          Profile            C / P          Profile                                     \n"
                    "------ ---------- ----- -------- ------- ---------- ----- -------- -------  ----------- -----    ------- -----   \n");

            }


            /* show service Id */
            CLI_print("%06d ", filterId);

            for (i = 0; i<EHP_Entry.num_of_entries; i++)
            {
                if (i != 0)
                    CLI_print("          ");
                CLI_print("0x%08x ", EHP_Entry.ehp_info[i].ehp_data.eth_info.val.all);
                CLI_print("%1s / %1s ", ((EHP_Entry.ehp_info[i].ehp_data.eth_info.stamp_color == 1) ? "C" : "N" )
                                      , ((EHP_Entry.ehp_info[i].ehp_data.eth_info.stamp_priority == 1) ? "P" : "N") );
                CLI_print("%-8s ", buff[EHP_Entry.ehp_info[i].ehp_data.eth_info.command]);
                if (EHP_Entry.ehp_info[i].ehp_data.eth_info.mapping_enable) {
                    CLI_print("%7d ", EHP_Entry.ehp_info[i].ehp_data.eth_info.mapping_id);
                }
                else {
                    CLI_print("%-7s ", "NA");
                }
                CLI_print("0x%08x ", EHP_Entry.ehp_info[i].ehp_data.atm_info.val.all);
                CLI_print("%1s / %1s ", ((EHP_Entry.ehp_info[i].ehp_data.atm_info.stamp_color ==1) ? "C" : "N")
                                      , ((EHP_Entry.ehp_info[i].ehp_data.atm_info.stamp_priority == 1) ? "P" : "N") );

                CLI_print("%-8s ", buff[EHP_Entry.ehp_info[i].ehp_data.atm_info.double_tag_cmd]);
                if (EHP_Entry.ehp_info[i].ehp_data.atm_info.mapping_enable) {
                    CLI_print("%7d ", EHP_Entry.ehp_info[i].ehp_data.atm_info.mapping_id);
                }
                else {
                    CLI_print("%-7s ", "NA");
                }

                
                CLI_print("%5d ", EHP_Entry.ehp_info[i].ehp_data.wbrg_info.val.data_wbrg.stamp802_1p);

				switch (EHP_Entry.ehp_info[i].ehp_data.wbrg_info.val.data_wbrg.flow_type)
				{

				case MEA_EHP_FLOW_TYPE_NONE:
					CLI_print("%6s ", "NONE");
					break;
				case MEA_EHP_FLOW_TYPE_UNTAG:
					CLI_print("%6s ", "UNTAG");
					break;
				case MEA_EHP_FLOW_TYPE_TAG:
					CLI_print("%6s ", "TAG");
					break;
				case MEA_EHP_FLOW_TYPE_QTAG:
					CLI_print("%6s ", "QTAG");
					break;
				default:
					CLI_print("%6s ", "ERROR");
					break;
				}
				CLI_print("%4d ", EHP_Entry.ehp_info[i].ehp_data.EditingType);

				switch (EHP_Entry.ehp_info[i].ehp_data.wbrg_info.val.data_wbrg.enc_swap_mode)
				{
				case MEA_EHP_SWAP_TYPE_NONE:
					CLI_print("%6s ", "NONE");
					break;
				case MEA_EHP_SWAP_TYPE_TAG:
					CLI_print("%6s ", "VLAN");
					break;
				case MEA_EHP_SWAP_TYPE_QTAG:
					CLI_print("%6s ", "ACCESS");
					break;
				case MEA_EHP_SWAP_TYPE_BOTH:
					CLI_print("%6s ", "BOTH");
					break;
				default:
					CLI_print("%6s ", "ERROR");
					break;

				}

				if (EHP_Entry.ehp_info[i].ehp_data.wbrg_info.val.data_wbrg.enc_outer_type == 0)
				{
					if (EHP_Entry.ehp_info[i].ehp_data.wbrg_info.val.data_wbrg.enc_inner_type == 0)
						CLI_print("%6s ", "NA");
					else
						CLI_print("%6s ", "VLAN");
				}
				else
				{
					if (EHP_Entry.ehp_info[i].ehp_data.wbrg_info.val.data_wbrg.enc_inner_type == 0)
						CLI_print("%6s ", "Qtag");
					else
						CLI_print("%6s ", "QV");
				}
                CLI_print("\n");

                if (EHP_Entry.num_of_entries > 1)
                {
                    CLI_print("          OutQueue:");
                    for (outqueue = 0; outqueue<ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; outqueue++)
                    {
                        if (ENET_IsValid_Queue(ENET_UNIT_0, outqueue, ENET_TRUE) == ENET_FALSE) {
                            continue;
                        }

                        pPortGrp = (MEA_Uint32 *)(&(EHP_Entry.ehp_info[i].output_info.out_ports_0_31));
                        pPortGrp += (outqueue / 32);

                        if ((*pPortGrp & (shiftWord << (outqueue % 32)))) {
                            CLI_print(" %3d", outqueue);
                        }
                    }
                    CLI_print("\n");
                }
            }
        }

        else if (info == MEA_CLI_EDITING_INFO_MARTINI)
        {
            /* show title for the output table */
            if (show_title) {
                CLI_print("FID  Martini           Martini           Martini   Martini \n"
                    "     DA                SA                EtherType Command \n"
                    "---- ----------------- ----------------- --------- -------\n");
            }

            for (i = 0; i<EHP_Entry.num_of_entries; i++)
            {
                if (i != 0)
                    CLI_print("          ");

                /* show service Id */
                CLI_print("%04d ", filterId);

                CLI_print("%02x:%02x:%02x:%02x:%02x:%02x ",
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.DA.b[0]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.DA.b[1]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.DA.b[2]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.DA.b[3]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.DA.b[4]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.DA.b[5]));

                CLI_print("%02x:%02x:%02x:%02x:%02x:%02x ",
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.SA.b[0]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.SA.b[1]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.SA.b[2]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.SA.b[3]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.SA.b[4]),
                    (MEA_Uint8)(EHP_Entry.ehp_info[i].ehp_data.martini_info.SA.b[5]));

                CLI_print("0x%04x ", EHP_Entry.ehp_info[i].ehp_data.martini_info.EtherType);
                CLI_print("%d\n\n", EHP_Entry.ehp_info[i].ehp_data.martini_info.martini_cmd);
            }
        }
        else if (info == MEA_CLI_EDITING_INFO_PROTOLLC)
        {
            /* show title for the output table */
            if (show_title) {
                CLI_print("Act  proto LLC     \n"
                          "ID                \n"
                          "---- ----- -----  \n");
            }

            for (i = 0; i < EHP_Entry.num_of_entries; i++)
            {
                if (EHP_Entry.ehp_info[i].ehp_data.Attribute_info.valid != MEA_TRUE)
                    continue;

                if (i != 0)
                    CLI_print("     ");

                /* show service Id */
                CLI_print("%04d ", filterId);

                CLI_print("%5d %5s \n",
                    EHP_Entry.ehp_info[i].ehp_data.Attribute_info.protocol,
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.Attribute_info.llc)


                    );
            }
        }
        else if (info == MEA_CLI_EDITING_INFO_STAMPING)
        {
            if (show_title) {
                CLI_print("FID  Color0 Color0 Color1 Color1 Pri0  Pri0   Pri1  Pri1   Pri2  Pri2\n"
                    "     Valid  Offset Valid  Offset Valid Offset Valid Offset Valid Offset\n"
                    "---- ------ ------ ------ ------ ----- ------ ----- ------ ----- ------\n");
            }

            for (i = 0; i<EHP_Entry.num_of_entries; i++)
            {
                if (i != 0)
                    CLI_print("     ");

                /* show service Id */
                CLI_print("%04d ", filterId);

                CLI_print("%6d %6d %6d %6d %5d %6d %5d %6d %5d %6d\n",
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.color_bit0_valid,
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.color_bit0_valid ?
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.color_bit0_loc : 0,
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.color_bit1_valid,
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.color_bit1_valid ?
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.color_bit1_loc : 0,
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.pri_bit0_valid,
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.pri_bit0_valid ?
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.pri_bit0_loc : 0,
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.pri_bit1_valid,
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.pri_bit1_valid ?
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.pri_bit1_loc : 0,
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.pri_bit2_valid,
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.pri_bit2_valid ?
                    EHP_Entry.ehp_info[i].ehp_data.eth_stamping_info.pri_bit2_loc : 0);
            }
        }

        else if (info == MEA_CLI_EDITING_INFO_LMID)
        {
            /* show title for the output table */
            if (show_title) {
                CLI_print("ID   Lm Counter ID        \n"
                    "     Valid LmID  Command      Command     calc\n"
                    "                  dasa         cfm        Ip\n"
                    "---- ----- ----- ----------- ----------- -----\n");
            }

            for (i = 0; i<EHP_Entry.num_of_entries; i++)
            {
                if (EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.valid != MEA_TRUE)
                    continue;

                if (i != 0)
                    CLI_print("     ");

                /* show service Id */
                CLI_print("%04d ", filterId);

                CLI_print("%5s %5d %11d %11s %s %s\n",
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.valid),
                    EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.LmId,
                    EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.Command_dasa,
                    str_lmid[EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.Command_cfm],
                    ((EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.sw_calcIp_checksum_valid == MEA_TRUE) ? "F" : "N"),
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.calcIp_checksum)
                    );
            }
        }
        else if (info == MEA_CLI_EDITING_INFO_MPLS)
        {
            /* show title for the output table */
            if (show_title) {
                CLI_print("ID   mpls  mpls       \n"
                    "     Valid lable      \n"
                    "---- ----- -----      \n");
            }

            for (i = 0; i<EHP_Entry.num_of_entries; i++)
            {
                if (EHP_Entry.ehp_info[i].ehp_data.mpls_label_info.valid != MEA_TRUE)
                    continue;

                if (i != 0)
                    CLI_print("     ");

                /* show service Id */
                CLI_print("%04d ", filterId);


                CLI_print("%5s %5d \n",
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.mpls_label_info.valid),
                    EHP_Entry.ehp_info[i].ehp_data.mpls_label_info.lable

                    );
            }
        }

        else if (info == MEA_CLI_EDITING_INFO_SESSION)
        {
            if (show_title) {
                CLI_print("ID          Session extract Session\n"
                    "     Valid   enable  enable  num     \n"
                    "---- ------- ------- ------- --------\n");
            }
            for (i = 0; i<EHP_Entry.num_of_entries; i++)
            {


                if (EHP_Entry.ehp_info[i].ehp_data.fragmentSession_info.valid != MEA_TRUE)
                    continue;

                if (i != 0)
                    CLI_print("     ");


                CLI_print("%04d ", filterId);



                CLI_print("%7s %7s %7d %7d\n",
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.fragmentSession_info.valid),
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.fragmentSession_info.enable),
                    EHP_Entry.ehp_info[i].ehp_data.fragmentSession_info.extract_enable,
                    EHP_Entry.ehp_info[i].ehp_data.fragmentSession_info.Session_num);
            }
        }
        else if (info == MEA_CLI_EDITING_INFO_PW)
        {
            /* show title for the output table */
            if (show_title) {
                CLI_print("ID   pw    clock clock  rtp   vlan  ces   Loss  Loss\n"
                    "     Valid res   domain exist exist type  L     R    \n"
                    "---- ----- ----- ------ ----- ----- ----- ----- -----\n");
            }


            for (i = 0; i<EHP_Entry.num_of_entries; i++)
            {
                if (EHP_Entry.ehp_info[i].ehp_data.pw_control_info.valid != MEA_TRUE)
                    continue;

                if (i != 0)
                    CLI_print("     ");

                /* show service Id */
                CLI_print("%04d ", filterId);


                CLI_print("%5s %5d %5d %5s %5s\n",
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.pw_control_info.valid),
                    EHP_Entry.ehp_info[i].ehp_data.pw_control_info.clock_resolution,
                    EHP_Entry.ehp_info[i].ehp_data.pw_control_info.clock_domain,
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.pw_control_info.rtp_exist),
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.pw_control_info.vlan_exist),
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.pw_control_info.loss_L),
                    MEA_STATUS_STR(EHP_Entry.ehp_info[i].ehp_data.pw_control_info.loss_R)

                    );
            }
        }







    }

    MEA_OS_free(EHP_Entry.ehp_info);



    return MEA_OK;
}


static MEA_Status MEA_CLI_Show_FilterEditingAll(mea_cli_editing_info_te info)
{
    MEA_Filter_t      filter_id;
    MEA_Bool          found, show_title;
#if 0
    if (argc<2) {
        return MEA_ERROR;
    }
#endif
    found = MEA_FALSE;

    show_title = MEA_TRUE;
    if (MEA_API_GetFirst_Filter(MEA_UNIT_0, &filter_id, &found) != MEA_OK)
    {
        return MEA_ERROR;
    }


    while (found != MEA_FALSE)
    {
        if (MEA_CLI_Show_FilterEditing(filter_id, show_title, info) != MEA_OK)
        {
            CLI_print(
                "%s - MEA_CLI_Show_FilterEditing(%d) Failed\n",
                __FUNCTION__, filter_id);
            return MEA_ERROR;
        }
        show_title = MEA_FALSE;

        if (MEA_API_GetNext_Filter(MEA_UNIT_0, &filter_id, &found) != MEA_OK)
        {
            return MEA_ERROR;
        }
    }

    return MEA_OK;
}

static MEA_Status MEA_CLI_ShowFilterEditing(int argc, char *argv[])
{
    int i;
    mea_cli_editing_info_te info = MEA_CLI_EDITING_INFO_EDITING;


    if (argc<2) return MEA_ERROR;

    for (i = 1; i<argc; i++)
    {
        if (!MEA_OS_strcmp(argv[i], "-m")){
            info = MEA_CLI_EDITING_INFO_MARTINI;
        }
        if (!MEA_OS_strcmp(argv[i], "-hpl")){
            info = MEA_CLI_EDITING_INFO_PROTOLLC;
        }
        
        if (!MEA_OS_strcmp(argv[i], "-s")){
            info = MEA_CLI_EDITING_INFO_STAMPING;
        }
    }

    if (!MEA_OS_strcmp(argv[1], "all"))
        return MEA_CLI_Show_FilterEditingAll(info);
    else
        return MEA_CLI_Show_FilterEditing((MEA_Filter_t)MEA_OS_atoi(argv[1]), MEA_TRUE, info);
}



MEA_Status MEA_CLI_Show_FilterOutPorts(MEA_Filter_t   filterId,
    MEA_Bool      show_title)
{

    MEA_Filter_Key_dbt            Filter_Key;
    MEA_Filter_Data_dbt            Filter_Data;
    MEA_Bool                   valid;
    MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;
    MEA_OutPorts_Entry_dbt               OutPorts_Entry;
    MEA_Policer_Entry_dbt                Policer_entry;
    MEA_Uint32 *pPortGrp;
    ENET_QueueId_t outqueue;
    MEA_Uint32  shiftWord = 0x00000001;

    MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));
    MEA_OS_memset(&Filter_Key, 0, sizeof(Filter_Key));
    MEA_OS_memset(&Filter_Data, 0, sizeof(Filter_Data));
    MEA_OS_memset(&OutPorts_Entry, 0, sizeof(OutPorts_Entry));
    MEA_OS_memset(&Policer_entry, 0, sizeof(Policer_entry));

    /* check if the filter exist */
    if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0, filterId, &valid) != MEA_OK) {
        CLI_print("Error: MEA_API_IsExist_Filter_ById for filter Id %d failed\n",
            filterId);
        return MEA_OK;
    }
    if (valid == MEA_FALSE) {
        CLI_print("Error: filterId %d does not exists\n", filterId);
        return MEA_OK;
    }


    /* call GET just to retrieve the EHP number*/
    EHP_Entry.ehp_info = NULL;

    if (MEA_API_Get_Filter(MEA_UNIT_0,
        filterId,
        &Filter_Key,
        NULL,
        &Filter_Data,
        &OutPorts_Entry,
        NULL,
        &EHP_Entry) != MEA_OK)
    {
        return MEA_ERROR;
    }

    /* show title for the output table */
    if (show_title) {
        CLI_print("ID   Output\n"
            "        Queues\n"
            "---- -------------------------------------------------\n");
    }

    if (Filter_Data.action_params.output_ports_valid)
    {
        CLI_print("%04d ", filterId);

        for (outqueue = 0; outqueue<ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; outqueue++)
        {
            if (ENET_IsValid_Queue(ENET_UNIT_0, outqueue, ENET_TRUE) == ENET_FALSE) {
                continue;
            }

            pPortGrp = (MEA_Uint32 *)(&(OutPorts_Entry.out_ports_0_31));
            pPortGrp += (outqueue / 32);

            if ((*pPortGrp & (shiftWord << (outqueue % 32)))) {
                CLI_print(" %3d", outqueue);
            }
        }
        CLI_print("\n");
    }



    return MEA_OK;
}



static MEA_Status MEA_CLI_Show_FilterOutPortsAll(void)
{
    MEA_Filter_t      filter_id;
    MEA_Bool          found, show_title;
#if 0
    if (argc<2) {
        return MEA_ERROR;
    }
#endif
    found = MEA_FALSE;

    show_title = MEA_TRUE;
    if (MEA_API_GetFirst_Filter(MEA_UNIT_0, &filter_id, &found) != MEA_OK)
    {
        return MEA_ERROR;
    }


    while (found != MEA_FALSE)
    {
        if (MEA_CLI_Show_FilterOutPorts(filter_id, show_title) != MEA_OK)
        {
            CLI_print(
                "%s - MEA_CLI_Show_FilterOutPorts(%d) Failed\n",
                __FUNCTION__, filter_id);
            return MEA_ERROR;
        }
        show_title = MEA_FALSE;

        if (MEA_API_GetNext_Filter(MEA_UNIT_0, &filter_id, &found) != MEA_OK)
        {
            return MEA_ERROR;
        }
    }

    return MEA_OK;
}


static MEA_Status MEA_CLI_ShowFilterOutPorts(int argc, char *argv[])
{
    if (argc<2) return MEA_ERROR;

    if (!MEA_OS_strcmp(argv[1], "all"))
        return MEA_CLI_Show_FilterOutPortsAll();
    else
        return MEA_CLI_Show_FilterOutPorts((MEA_Filter_t)MEA_OS_atoi(argv[1]), MEA_TRUE);

}

MEA_Status MEA_CLI_Show_FilterSLA_ParamsAll(void)
{
    MEA_Filter_t  db_filterId;
    MEA_Bool      found;
    MEA_Uint32    countEntry = 0;
    MEA_Filter_Key_dbt   filter_Entry_Key;
    MEA_Filter_Data_dbt  filter_Entry_Data;

    char str_mode[MEA_SHAPER_TYPE_LAST + 1][6];

    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_MEF][0]), "MEF");
    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_BAG][0]), "BAG");
    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_LAST][0]), "ERORR");

    if (MEA_API_GetFirst_Filter(MEA_UNIT_0, &db_filterId, &found) != MEA_OK) {
        CLI_print(
            "%s - MEA_API_GetFirst_Filterfailed\n",
            __FUNCTION__);
        return MEA_OK;
    }

    if (!found) {
        CLI_print("Filter Table is empty\n");
        return MEA_OK;
    }

    CLI_print("Fil  TM  Type CIR(bps)     EIR(bps)     CBS     EBS     s GN Coup Comp Col Tm  \n"
              "ID   ID  MODE BAG(125uS)   Jitter       Lmax            s Ty           Aw  Dis\n"
              "---- --- ---- ------------ ------------ ------- ------- - -- ---- ---- --- --- \n");

    while (found) {

        MEA_Policer_Entry_dbt sla_params_entry;

        if (MEA_API_Get_Filter(MEA_UNIT_0,
            db_filterId,
            &filter_Entry_Key, /* key  */
            NULL,
            &filter_Entry_Data, /* data */
            NULL, /* OutPorts_Entry */
            &sla_params_entry,
            NULL) /* EHP_Entry */
            != MEA_OK) {
            CLI_print("%s - MEA_API_Get_Filter failed (%d)\n",
                __FUNCTION__, db_filterId);
            return MEA_ERROR;
        }

        if (filter_Entry_Data.action_params.tm_id_valid){
            CLI_print("%4d %3d %4s %12llu %12llu %7d %7d %1d %2d %4s %4d %3s %3d \n",
                db_filterId,
                filter_Entry_Data.action_params.tm_id,
                str_mode[sla_params_entry.type_mode],
                sla_params_entry.CIR,
                sla_params_entry.EIR,
                sla_params_entry.CBS,
                sla_params_entry.EBS,
                sla_params_entry.gn_sign,
                sla_params_entry.gn_type,
                MEA_STATUS_STR(sla_params_entry.cup),
                sla_params_entry.comp,
                MEA_STATUS_STR(sla_params_entry.color_aware),
                filter_Entry_Data.action_params.tmId_disable);
        }



        if (MEA_API_GetNext_Filter(MEA_UNIT_0, &db_filterId, &found) != MEA_OK) {
            CLI_print(
                "%s - MEA_API_GetNext_Filter failed (db_FilterId=%d)\n",
                __FUNCTION__, db_filterId);
            return MEA_ERROR;
        }
        countEntry++;
    }
    CLI_print("----------------------------------------------------------------\n");/*SLA*/
    CLI_print("number of Items %d\n", countEntry);

    return MEA_OK;

}

MEA_Status MEA_CLI_Show_FilterSLA_Params(MEA_Filter_t filterId)
{
    MEA_Policer_Entry_dbt sla_params_entry;
    MEA_Bool valid;
    MEA_Filter_Key_dbt   filter_Entry_Key;
    MEA_Filter_Data_dbt  filter_Entry_Data;
    char str_mode[MEA_SHAPER_TYPE_LAST + 1][6];

    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_MEF][0]), "MEF");
    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_BAG][0]), "BAG");
    MEA_OS_strcpy(&(str_mode[MEA_SHAPER_TYPE_LAST][0]), "ERORR");


    MEA_CHECK_FILTER_IN_RANGE(filterId)

    if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0, filterId, &valid) != MEA_OK) {
        CLI_print(" %s - filter %d GetValid failed\n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }

    if (valid == MEA_FALSE) {
        CLI_print("%s - filter %d does not exists\n", __FUNCTION__, filterId);
        return MEA_ERROR;
    }

    if (MEA_API_Get_Filter(MEA_UNIT_0, filterId,
        &filter_Entry_Key, NULL, /* key  */
        &filter_Entry_Data, /* data */
        NULL, /* OutPorts_Entry */
        &sla_params_entry,
        NULL) /* EHP_Entry */
        != MEA_OK) {
        CLI_print("%s - MEA_API_Get_Filter failed (%d)\n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }

    CLI_print("Fil  TM  Type CIR(bps)     EIR(bps)      CBS     EBS     s GN Coup Comp Col Tm  MTU  \n"
              "ID   ID  MODE BAG(125uS)   Jitter                        s Ty           Aw  Dis       \n"
              "---- --- ---- ------------ ------------ ------- ------- - -- ---- ---- --- --- -----\n");

    if (filter_Entry_Data.action_params.tm_id_valid){
        CLI_print("%4d %3d %4s %12llu %12llu %7d %7d %1d %2d %4s %4d %3s %3d %4d\n",
            filterId,
            filter_Entry_Data.action_params.tm_id,
            str_mode[sla_params_entry.type_mode],
            sla_params_entry.CIR,
            sla_params_entry.EIR,
            sla_params_entry.CBS,
            sla_params_entry.EBS,
            sla_params_entry.gn_sign,
            sla_params_entry.gn_type,
            MEA_STATUS_STR(sla_params_entry.cup),
            sla_params_entry.comp,
            MEA_STATUS_STR(sla_params_entry.color_aware),
            filter_Entry_Data.action_params.tmId_disable,
            ((sla_params_entry.maxMtu) ? sla_params_entry.maxMtu : MEA_Platform_GetMaxMTU())
            );
    }


    return MEA_OK;

}

static MEA_Status MEA_CLI_ShowFilterSLATbl(int argc, char *argv[])
{
    if (argc<2) return MEA_ERROR;


    if (!MEA_OS_strcmp(argv[1], "all"))
        return MEA_CLI_Show_FilterSLA_ParamsAll();
    else
        return MEA_CLI_Show_FilterSLA_Params((MEA_Filter_t)MEA_OS_atoi(argv[1]));
}

MEA_Status MEA_CLI_Show_Filter_FWD_Params(MEA_Filter_t filterId, MEA_Bool show_title)
{
    //MEA_Policer_Entry_dbt sla_params_entry;
    MEA_Bool valid;
    MEA_Filter_Key_dbt   filter_Entry_Key;
    MEA_Filter_Data_dbt  filter_Entry_Data;
    //MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;

    //  MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));


    MEA_CHECK_FILTER_IN_RANGE(filterId)

        if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0, filterId, &valid) != MEA_OK) {
            CLI_print(" %s - filter %d GetValid failed\n",
                __FUNCTION__, filterId);
            return MEA_ERROR;
        }

    if (MEA_API_Get_Filter(MEA_UNIT_0, filterId,
        &filter_Entry_Key, NULL, /* key  */
        &filter_Entry_Data, /* data */
        NULL, /* OutPorts_Entry */
        NULL,
        NULL) /* EHP_Entry */
        != MEA_OK) {
        CLI_print("%s - MEA_API_Get_Filter failed (%d)\n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }

    if (valid == MEA_FALSE) {
        CLI_print("%s - filter %d does not exists\n", __FUNCTION__, filterId);
        return MEA_ERROR;
    }


    if (show_title)
    {
        CLI_print("    ----Filter FWD parameters ------------ \n"
            "    FId  doDa  F-Key Internal force\n"
            "    ---- ---- ------ -------- ----- \n");
        //12345678901234567890123456789012345678901234567890123456789012345678901234567890
    }


    CLI_print("    %4d %4s %6d %8s %5s",
        filterId,
        MEA_STATUS_STR(filter_Entry_Data.fwd_enable),
        filter_Entry_Data.fwd_Key,
        MEA_STATUS_STR(filter_Entry_Data.fwd_enable_Internal_mac),
        MEA_STATUS_STR(filter_Entry_Data.force_fwd_rule));

    CLI_print("\n");





    return MEA_OK;

}
MEA_Status MEA_CLI_Show_Filter_FWD_All(void)
{
    MEA_Filter_t  db_filterId;
    MEA_Bool      found;
    MEA_Uint32    countEntry = 0;

    MEA_Bool show_title = MEA_TRUE;

    if (MEA_API_GetFirst_Filter(MEA_UNIT_0, &db_filterId, &found) != MEA_OK) {
        CLI_print(
            "%s - MEA_API_GetFirst_Filterfailed\n",
            __FUNCTION__);
        return MEA_OK;
    }

    if (!found) {
        CLI_print("Filter Table is empty\n");
        return MEA_OK;
    }



    while (found) {

        MEA_CLI_Show_Filter_FWD_Params(db_filterId, show_title);

        if (show_title == MEA_TRUE)
            show_title = MEA_FALSE;

        if (MEA_API_GetNext_Filter(MEA_UNIT_0, &db_filterId, &found) != MEA_OK) {
            CLI_print(
                "%s - MEA_API_GetNext_Filter failed (db_FilterId=%d)\n",
                __FUNCTION__, db_filterId);
            return MEA_ERROR;
        }
        countEntry++;
    }
    CLI_print("-----------------------------------------------------------\n");/*SLA*/
    CLI_print("number of Items %d\n", countEntry);

    return MEA_OK;

}


static MEA_Status MEA_CLI_ShowFilter_FWD(int argc, char *argv[])
{
    if (argc < 2) return MEA_ERROR;


    if (!MEA_OS_strcmp(argv[1], "all"))
        return MEA_CLI_Show_Filter_FWD_All();
    else
        return MEA_CLI_Show_Filter_FWD_Params((MEA_Filter_t)MEA_OS_atoi(argv[1]), MEA_TRUE);
}


MEA_Status MEA_CLI_Show_FilterAction_afdx_Params(MEA_Filter_t filterId, MEA_Bool show_title)
{
    //MEA_Policer_Entry_dbt sla_params_entry;
    MEA_Bool valid;
    MEA_Filter_Key_dbt   filter_Entry_Key;
    MEA_Filter_Data_dbt  filter_Entry_Data;
    //MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;

    //  MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));


    MEA_CHECK_FILTER_IN_RANGE(filterId)

    if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0, filterId, &valid) != MEA_OK) {
        CLI_print(" %s - filter %d GetValid failed\n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }

    if (MEA_API_Get_Filter(MEA_UNIT_0, filterId,
        &filter_Entry_Key, NULL, /* key  */
        &filter_Entry_Data, /* data */
        NULL, /* OutPorts_Entry */
        NULL,
        NULL) /* EHP_Entry */
        != MEA_OK) {
        CLI_print("%s - MEA_API_Get_Filter failed (%d)\n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }

    if (valid == MEA_FALSE) {
        CLI_print("%s - filter %d does not exists\n", __FUNCTION__, filterId);
        return MEA_ERROR;
    }


    if (show_title)
    {
        CLI_print("    ----Filter Action Afdx------------ \n"
            "    SId  valid  Rx_Id   a/b     BAGType \n"
            "    ---- ------ ------- ------- ------- \n");
        //12345678901234567890123456789012345678901234567890123456789012345678901234567890
    }


    CLI_print("    %4d %6s %7d %7s %7d",
        filterId,
        MEA_STATUS_STR(filter_Entry_Data.action_params.afdx_enable),
        filter_Entry_Data.action_params.afdx_Rx_sessionId,
        MEA_STATUS_STR(filter_Entry_Data.action_params.afdx_A_B),
        filter_Entry_Data.action_params.afdx_Bag_info);

    CLI_print("\n");





    return MEA_OK;

}

MEA_Status MEA_CLI_Show_FilterAction_afdx_All(void)
{
    MEA_Filter_t  db_filterId;
    MEA_Bool      found;
    MEA_Uint32    countEntry = 0;

    MEA_Bool show_title = MEA_TRUE;

    if (MEA_API_GetFirst_Filter(MEA_UNIT_0, &db_filterId, &found) != MEA_OK) {
        CLI_print(
            "%s - MEA_API_GetFirst_Filterfailed\n",
            __FUNCTION__);
        return MEA_OK;
    }

    if (!found) {
        CLI_print("Filter Table is empty\n");
        return MEA_OK;
    }



    while (found) {

        MEA_CLI_Show_FilterAction_afdx_Params(db_filterId, show_title);

        if (show_title == MEA_TRUE)
            show_title = MEA_FALSE;

        if (MEA_API_GetNext_Filter(MEA_UNIT_0, &db_filterId, &found) != MEA_OK) {
            CLI_print(
                "%s - MEA_API_GetNext_Filter failed (db_FilterId=%d)\n",
                __FUNCTION__, db_filterId);
            return MEA_ERROR;
        }
        countEntry++;
    }
    CLI_print("-----------------------------------------------------------\n");/*SLA*/
    CLI_print("number of Items %d\n", countEntry);

    return MEA_OK;

}


static MEA_Status MEA_CLI_ShowFilterAction_afdx(int argc, char *argv[])
{
    if (argc<2) return MEA_ERROR;


    if (!MEA_OS_strcmp(argv[1], "all"))
        return MEA_CLI_Show_FilterAction_afdx_All();
    else
        return MEA_CLI_Show_FilterAction_afdx_Params((MEA_Filter_t)MEA_OS_atoi(argv[1]), MEA_TRUE);
}







MEA_Status MEA_CLI_Show_FilterAction_Params(MEA_Filter_t filterId, MEA_Bool show_title)
{
    MEA_Policer_Entry_dbt sla_params_entry;
    MEA_Bool valid;
    MEA_Filter_Key_dbt   filter_Entry_Key;
    MEA_Filter_Data_dbt  filter_Entry_Data;
    MEA_EgressHeaderProc_Array_Entry_dbt EHP_Entry;

    MEA_OS_memset(&EHP_Entry, 0, sizeof(EHP_Entry));

    MEA_CHECK_FILTER_IN_RANGE(filterId)

    if (MEA_API_IsExist_Filter_ById(MEA_UNIT_0, filterId, &valid) != MEA_OK) {
        CLI_print(" %s - filter %d GetValid failed\n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }

    if (MEA_API_Get_Filter(MEA_UNIT_0, filterId,
        &filter_Entry_Key, NULL, /* key  */
        &filter_Entry_Data, /* data */
        NULL, /* OutPorts_Entry */
        &sla_params_entry,
        &EHP_Entry) /* EHP_Entry */
        != MEA_OK) {
        CLI_print("%s - MEA_API_Get_Filter failed (%d)\n",
            __FUNCTION__, filterId);
        return MEA_ERROR;
    }

    if (valid == MEA_FALSE) {
        CLI_print("%s - filter %d does not exists\n", __FUNCTION__, filterId);
        return MEA_ERROR;
    }
    EHP_Entry.ehp_info = (MEA_EHP_Info_dbt*)MEA_OS_malloc(sizeof(MEA_EHP_Info_dbt)*EHP_Entry.num_of_entries);

    if (EHP_Entry.ehp_info == NULL)
    {
        CLI_print("Error: Malloc failed\n");
        return MEA_OK;
    }

    if (MEA_API_Get_Filter(MEA_UNIT_0, filterId,
        &filter_Entry_Key, NULL, /* key  */
        &filter_Entry_Data, /* data */
        NULL, /* OutPorts_Entry */
        &sla_params_entry,
        &EHP_Entry) /* EHP_Entry */
        != MEA_OK) {
        CLI_print("%s - MEA_API_Get_Filter failed (%d)\n",
            __FUNCTION__, filterId);
        MEA_OS_free(EHP_Entry.ehp_info);
        return MEA_ERROR;
    }

    if (show_title)
    {
        CLI_print("------------------Filter -------------------------------------------------------------------\n");
        CLI_print("filt Forc AC  Pm   Tm   Edit pro llc Forc Val  Forc Val  Forc Val  Ing wred Lag Drop ovRule \n"
                  " Id  Ac   ID  Id   Id   Id           l2p  l2p  Cos  Cos  Col  Col  TS  prof byP En   VP     \n"
                  "---- --- ---- ---- ---- ---- --- --- ---- ---- ---- ---- ---- ---- --- ---- --- ---- ----- \n");
                  //12345678901234567890123456789012345678901234567890123456789012345678901234567890 
    }
     

    CLI_print("%4d %s %4d", filterId, MEA_STATUS_STR(filter_Entry_Data.force_Action_en),
             filter_Entry_Data.Action_Id);


    if (filter_Entry_Data.action_params.pm_id_valid) {
        CLI_print("%4d ", filter_Entry_Data.action_params.pm_id);
    }
    else {
        CLI_print("%4s ", "NA");
    }
    if (filter_Entry_Data.action_params.tm_id_valid) {
        CLI_print("%4d ", filter_Entry_Data.action_params.tm_id);
    }
    else {
        CLI_print("%4s ", "NA");
    }

    if (filter_Entry_Data.action_params.ed_id_valid) {
        if (EHP_Entry.num_of_entries != 1){
            CLI_print("%4s ", "MC");
        }
        else{
            CLI_print("%4d ", filter_Entry_Data.action_params.ed_id);
        }
    }
    else {
        CLI_print("%4s ", "NA");
    }


    if (filter_Entry_Data.action_params.proto_llc_valid) {
        if (filter_Entry_Data.action_params.protocol_llc_force) {
            CLI_print("%3d %3d ",
                filter_Entry_Data.action_params.Protocol,
                filter_Entry_Data.action_params.Llc);
        }
        else {
            CLI_print("%3s %3s ", "AUT", "AUT");
        }
    }
    else {
        CLI_print("%3s %3s ", "NA", "NA");
    }

    CLI_print("%4d %4d %3d %4d %4d %3d ",
        filter_Entry_Data.action_params.force_l2_pri_valid,
        filter_Entry_Data.action_params.L2_PRI,
        filter_Entry_Data.action_params.force_cos_valid,
        filter_Entry_Data.action_params.COS,
        filter_Entry_Data.action_params.force_color_valid,
        filter_Entry_Data.action_params.COLOR);
    CLI_print("%3s ", MEA_STATUS_STR(filter_Entry_Data.action_params.fwd_ingress_TS_type));
    CLI_print("%4d ", filter_Entry_Data.action_params.wred_profId);
    CLI_print("%3s ", MEA_STATUS_STR(filter_Entry_Data.action_params.lag_bypass));
    CLI_print("%3s ", MEA_STATUS_STR(filter_Entry_Data.action_params.Drop_en));
    if (filter_Entry_Data.action_params.overRule_vpValid == MEA_TRUE) {
        CLI_print("%d ", filter_Entry_Data.action_params.overRule_vp_val);
    }
    else {
        CLI_print("NA  ");
    }

    CLI_print("\n");

    MEA_OS_free(EHP_Entry.ehp_info);
    return MEA_OK;

}


MEA_Status MEA_CLI_Show_FilterAction_All(void)
{
    MEA_Filter_t  db_filterId;
    MEA_Bool      found;
    MEA_Uint32    countEntry = 0;

    MEA_Bool show_title = MEA_TRUE;

    if (MEA_API_GetFirst_Filter(MEA_UNIT_0, &db_filterId, &found) != MEA_OK) {
        CLI_print(
            "%s - MEA_API_GetFirst_Filterfailed\n",
            __FUNCTION__);
        return MEA_OK;
    }

    if (!found) {
        CLI_print("Filter Table is empty\n");
        return MEA_OK;
    }



    while (found) {

        MEA_CLI_Show_FilterAction_Params(db_filterId, show_title);

        if (show_title == MEA_TRUE)
            show_title = MEA_FALSE;

        if (MEA_API_GetNext_Filter(MEA_UNIT_0, &db_filterId, &found) != MEA_OK) {
            CLI_print(
                "%s - MEA_API_GetNext_Filter failed (db_FilterId=%d)\n",
                __FUNCTION__, db_filterId);
            return MEA_ERROR;
        }
        countEntry++;
    }
    CLI_print("-----------------------------------------------------------\n");/*SLA*/
    CLI_print("number of Items %d\n", countEntry);

    return MEA_OK;

}

static MEA_Status MEA_CLI_ShowFilterAction(int argc, char *argv[])
{
    if (argc<2) return MEA_ERROR;


    if (!MEA_OS_strcmp(argv[1], "all"))
        return MEA_CLI_Show_FilterAction_All();
    else
        return MEA_CLI_Show_FilterAction_Params((MEA_Filter_t)MEA_OS_atoi(argv[1]), MEA_TRUE);
}





static MEA_Status MEA_CLI_DeleteFilter(int argc, char *argv[])
{
    MEA_Filter_t      filter_id;
    MEA_Filter_t      temp_filter_id;
    MEA_Bool          found;

    if (argc<2) {
        return MEA_ERROR;
    }

    if (!MEA_OS_strcmp(argv[1], "all"))
    {

        found = MEA_FALSE;

        if (MEA_API_GetFirst_Filter(MEA_UNIT_0, &filter_id, &found) != MEA_OK)
        {
            return MEA_ERROR;
        }


        while (found != MEA_FALSE)
        {
            temp_filter_id = filter_id;

            if (MEA_API_Delete_Filter(MEA_UNIT_0, temp_filter_id) != MEA_OK) {
                CLI_print(
                    "%s - failed to delete filter %d\n",
                    __FUNCTION__, temp_filter_id);
                return MEA_OK;
            }
            if (MEA_API_GetNext_Filter(MEA_UNIT_0, &filter_id, &found) != MEA_OK)
            {
                return MEA_ERROR;
            }
        }
    }
    else
    {
        filter_id = (MEA_Filter_t)MEA_OS_atoi(argv[1]);
        if (MEA_API_Delete_Filter(MEA_UNIT_0, filter_id) != MEA_OK) {
            CLI_print(
                "%s - failed to delete filter %d\n",
                __FUNCTION__, filter_id);
            return MEA_ERROR;
        }

    }

    return MEA_OK;
}




static MEA_Status MEA_CLI_Collect_Counters_Set_SID(int argc, char *argv[])
{
    MEA_Service_t Sid;
    if (argc < 2) {
        CLI_print("Invalid number of arguments\n");
        return MEA_ERROR;
    }

   
    Sid = MEA_OS_atoi(argv[1]);
     
    MEA_API_ACL_Set_CountersBySID(MEA_UNIT_0, Sid);
   
    CLI_print("Start to counting for SID %d\n\n", Sid);

    return MEA_OK;
}





static MEA_Status MEA_CLI_Collect_ACL_Default_rule(int argc, char *argv[])
{
    MEA_Uint32 Sid;

    if (argc < 2) {
        CLI_print("Invalid number of arguments\n");
        return MEA_ERROR;
    }


    Sid = MEA_OS_atoi(argv[1]);

    MEA_API_ACL_Set_Default_rule(MEA_UNIT_0, Sid);

    CLI_print("set default rule %d\n\n", Sid);

    return MEA_OK;
}


static MEA_Status MEA_CLI_Collect_Counters_ACL(int argc, char *argv[])
{
    return MEA_API_Collect_Counters_ACLs(MEA_UNIT_0);

}


static MEA_Status MEA_CLI_Clear_Counters_ACL(int argc, char *argv[])
{
    return MEA_API_Clear_Counters_ACLs(MEA_UNIT_0);


}

static MEA_Status MEA_CLI_Show_Counters_ACL(int argc, char *argv[])
{
    MEA_Counters_ACL_dbt entry;
    
    
    if (MEA_API_Get_Counters_ACL(MEA_UNIT_0, &entry) != MEA_OK){
        return MEA_OK;
    }
    CLI_print("----ACL Drop Counter -----\n");
    CLI_print("      %16s %16s %16s %16s \n",
         "ACL0", "ACL1", "ACL2", "ACL3\n" );
    CLI_print("----- ---------------- ---------------- ---------------- ---------------- \n");
    CLI_print("Drop  %16llu %16llu %16llu %16llu \n", entry.ACLDrop[0].val, entry.ACLDrop[1].val, entry.ACLDrop[2].val, entry.ACLDrop[3].val);
    CLI_print("----- ---------------- ---------------- ---------------- ---------------- \n");
    return MEA_OK;



}






MEA_Status MEA_CLI_ACL_filter_AddDebugCmds(void)
{

    
        CLI_defineCmd("MEA ACL Set_default rule",
        (CmdFunc)MEA_CLI_Collect_ACL_Default_rule,
            "set ACL Set_default rule ",
            "Set Set_default \n"
            
            "Examples: - Set_default rule 1 \n");




    CLI_defineCmd("MEA counters ACL Set_Sid",
        (CmdFunc)MEA_CLI_Collect_Counters_Set_SID,
        "set ACL counters by Sid ",
        "Set Sid to counting \n"
        "        collect all \n"
        "Examples: - set_sid 1 \n"
        "          - set_sid 100\n");


    CLI_defineCmd("MEA counters ACL collect",
        (CmdFunc)MEA_CLI_Collect_Counters_ACL,
        "Collect ACL counters",
        "collect all \n"
        "        collect all \n"
        "Examples: - collect All \n"
        "          - collect all\n");

    CLI_defineCmd("MEA counters ACL clear",
        (CmdFunc)MEA_CLI_Clear_Counters_ACL,
        "Clear   ACL counters ",
        "clear  all \n"
        "       all \n"
        "Examples: - clear 88 \n"
        "          - clear all\n");

    CLI_defineCmd("MEA counters ACL show",
        (CmdFunc)MEA_CLI_Show_Counters_ACL,
        "Show     ACL counters",
        "show  all \n"
        "       all \n"
        "Examples: - show all \n"
        "          - show all\n");


    CLI_defineCmd("MEA filter show outclusters", (CmdFunc)MEA_CLI_ShowFilterOutPorts,
        "Show filter outclusters <filterId> /all",
        "Show filter outclusters <filterId> /all");

    CLI_defineCmd("MEA filter show editing",
        (CmdFunc)MEA_CLI_ShowFilterEditing,
        "Show filter(s) editing",
        "Show editing <filterId> | all [-m | -s]\n"
        " [-m] - show Marini Editing info\n"
        " [-s] - show Stamping info\n"
        "Show editing 1 -m\n");
    CLI_defineCmd("MEA filter show policing",
        (CmdFunc)MEA_CLI_ShowFilterSLATbl,
        "Show filter policing table <filterId> /all",
        "Show filter policing table <filterId> /all");
    CLI_defineCmd("MEA filter show action",
        (CmdFunc)MEA_CLI_ShowFilterAction,
        "Show filter action <filterId> /all",
        "Show filter action <filterId> /all");

    CLI_defineCmd("MEA filter show afdx",
        (CmdFunc)MEA_CLI_ShowFilterAction_afdx,
        "Show filter afdx <filterId> /all",
        "Show filter afdx <filterId> /all");

    CLI_defineCmd("MEA filter show fwd",
        (CmdFunc)MEA_CLI_ShowFilter_FWD,
        "Show filter fwd <filterId> /all",
        "Show filter fwd <filterId> /all");

    

    CLI_defineCmd("MEA filter show entry",
        (CmdFunc)MEA_CLI_ShowFilter,
        "Show filter(s)",
        "entry <filterId> | all \n"
        "    <filterId> | all - Filter Id OR all\n"
        "Examples: - entry 3  \n"
        "          - entry all\n");



    CLI_defineCmd("MEA filter set create",
        (CmdFunc)MEA_CLI_CreateFilter,
        "Create filter \n",
        "create filter <filter-key-type> <key-params>\n"
        "              <action_type> options..\n"
        "    Options: \n"
        "------------------------------------------------------------------\n"
        "   [-HIERACHY  <type 0:3>] part of the key  \n"
        "   [-sp <src-port>]\n"
        "   [-sid <service id>]\n"
        "   [-col  <color 0 or 1>    ]\n"
        "   [-cos  <queue  0..7>  ]\n"
        "   [-pri <0..7>  ]\n"
        "   [-cos-cmd <cmd> <val>]\n"
        "   [-pri-cmd <cmd> <val>]\n"
        "   [-col-cmd <cmd> <val>]\n"
        "   [-policer <CIR> <EIR> <CBS> <EBS> <coupling> <col_aware> <comp>] \n"
        "   [-policer_mode <type>]\n"
        "   [-policer_maxMtu <value>]\n"
        "   [-soft_wred <value>](0..3)\n"
        "   [-pgt <type>]\n"
        "   [-pgt_sign <sign>]\n"
        "   [-frag_IP  <en_fragment Ip> <en_TUNNEL> <frag_ex_int> <target_mtu_prof>]   \n"
        "   [-defrag_IP  <en_defrag_ext> <en_defrag_int><Reassembly_L2_prof >   \n"
        "   -------------------------Editing --------------------------------------------\n "
        "   [-h  <VAL> <CL_ST> <PRI_ST> <CMD>] \n"
        "   [-h2 <VAL> <DOUBLE TAG CMD>]\n"
        "   [-ho  <VAL> <CL_ST> <PRI_ST> <CMD>] \n"

        "   [-hType <editType>\n"
        "   [-hNVGRE   <valid> <da_MAC><GRE_DST_IP ><VSID><FlowID>\n"
        "   [-hIPSec   <valid> <Type 0-nvgre ><da_MAC><IPSec_DST_IP><GRE_DST_IP ><VSID><FlowID>]\n"
        "   [-hIPSec   <valid> <Type 1-ipsec><IPSec_src_IP><IPSec_dst_IP>]\n"
        "   [-hIPSec   <valid> <Type 2-vxlan<DA><Ip dst ><vxlan_vni><l4_dest_Port><l4_src_Port><ttl><select_ip_mac><IPSec_dst_IP>]\n"
        "   [-hESP     <encrypt 1-enc 2-decrypt ><ESPID>\n"
        "   [-hL2TPGRE <valid> <Type 0-GRE ><da_MAC><sa_MAC><DST_IP><src_IP >]\n"
        "   [-hL2TPGRE <valid> <Type 1-L2TP ><da_MAC><sa_MAC><DST_IP><tunnelId><SessionId>]\n"
        "   [-hvxlan_DL   <valid> <DA><Ip dst ><vxlan_vni><l4_dest_Port><l4_src_Port><ttl><Id-of-sgw_ip>\n"
        "   [-hIPCS_DL   <valid> <DA><Ip dst ><TE_ID><Id-of-sgw_ip>\n"
        "   [-hIPCS_UL <valid> <Da_next_hop_mac><Sa_host_mac><apn vlan><id-PdnIp>\n"
        "   [-hETHCS_DL <valid> <DA_eNB><Ip dst enb><TE_ID><IP ue><Id-of-sgw_ip>\n"
        "   [-hETHCS_UL <valid> <mode><Stag><Ctag>\n"
        "   [-hcalcIp_checksum <force> <valid>]\n"
        "   [-hNAT <valid> <DA><Ip ><L4port><type>\n"
        "   [-hwbrg  <VAL>]\n"
		"   [-hwmode  <VAL><VAL><VAL>]\n"
        "   [-h802_1p <VAL>]\n"
        "   [-hpw  <VAL> <clock_resolution> <clock_domain> <rtp_exist><vlan_exist><ces_type><R><L>]\n"
        "   [-hmpls_lable  <VAL> <value> \n"
        "   [-hs  <valid><enable><do_extract><id>]\n"
        "   [-hm  <valid> <id> ]\n"
        "   [-hom <valid> <id> ]\n"
        "   [-lmid <valid> <id><command-dasa><command-cfm> ]\n"
        "   [-hdscp <valid><TypeIPprority><value>]\n"
        "   [-a  <VPI> <VCI> <CL_ST> <PRI_ST>]\n"
        "   [-m  <DA> <SA> <EtherType>]\n"
        "   [-s <c0v><c0l> <c1v><c1l> <p0v><p0l> <p1v><p1l> <p2v><p2l>]\n"
        " -----------------------------------------End Editing --------------------------------\n"
        "   [-Tm  <value>]\n"
        "   [-Ed  <value>]\n"
        "   [-Pm  <value>]\n"
        "   [-pm_type  <type>]\n"
        "   [-protocol <valid> <value>\n"
        "   [-llc      <valid> <value>\n"

        "   [-o <num_of_queues> <queue1>..]\n"
        
        "   [-set1588  <valid>]\n"
        "   [-sflow_type  <type>]\n"
        
        "   [-lxcp_win <Valid>]\n"
        "   [-afdx <valid> <streamId 1..255> <take A or B> <Type>\n"
        "   [-fwd  <doDa> <Fwd key> <doDa_int> <force_rule>]\n"
        "   [-fwd_field_mask (<mask type)>]\n"
        "   [-fwd_act_en       valid\n"
        "   [-cos_hpm <valid><value>]\n"
        "-------------------------------------------------------------\n"
        "  Keys: \n"
        "  ---------\n"
        "      <filter-key-type>:\n"
        "              0- <DA_MAC>\n"
        "              1- <SA MAC>\n"
        "              2- <DST IPv4> + <DST TCP/UDP Port>\n"
        "              3- <SRC IPv4> + <DST TCP/UDP Port>\n"
        "              4- <DST IPv4> + <SRC TCP/UDP Port>\n"
        "              5- <SRC IPv4> + <SRC TCP/UDP Port>\n"
        "              6- <Outer EthType> + <Inner EthType> + <Outer vlan-Tag>\n"
        "              7- <SRC IPv4> + <Appl EthType>\n"
        "              8- <pri_type> <pri_value> + <IP Protocol> + <Appl EthType> + <DST TCP/UDP Port> \n"
        "              9- <pri_type> <pri_value> + <IP Protocol> + <SRC TCP/UDP Port> + <DST TCP/UDP Port> \n"
        "             10-  <Outer vlan-Tag> + <Inner vlan-Tag> + <Outer EthType>\n"
        "             11- <Outer vlan-Tag> + <DST TCP/UDP Port> + <SRC TCP/UDP Port>\n"
        "             12- <Inner vlan-Tag> + <DST TCP/UDP Port> + <SRC TCP/UDP Port>\n"
        "             13- <PPPoE Session ID> <PPP Protocol> <SA_MAC LSB(16bit)>\n"
        "             14- <Outer EtherType> + <Appl EtherType> + <Outer vlan-Tag>\n"
        "             15- <DA MAC> + <Outer vlan-tag> + <DST IPv4>\n"
        "             22- <DST IPv6> + <DST TCP/UDP Port>\n"
        "             23- <SRC IPv6> + <DST TCP/UDP Port>\n"
        "             24- <DST IPv6> + <SRC TCP/UDP Port>\n"
        "             25- <SRC IPv6> + <SRC TCP/UDP Port>\n"
        "             27- <SRC IPv6> + <Appl EthType>\n"

        "      <key-params>:\n"
        "              0- <da-mac>\n"
        "                  <da-mac> - xx:xx:xx:xx:xx:xx\n"
        "              1- <sa-mac>\n"
        "                 <sa-mac> - xx:xx:xx:xx:xx:xx\n"
        "              2- <dst-ip> <dst-port>\n"
        "                 <dst-ip>   - IPv4 address in dot notation\n"
        "                 <dst-port> - (0..2^16-1)\n"
        "              3- <src-ip> <dst-port>\n"
        "                 <src-ip>   - IPv4 address in dot notation\n"
        "                 <dst-port> - (0..2^16-1)\n"
        "              4- <dst-ip> <src-port>\n"
        "                 <dst-ip>   - IPv4 address in dot notation\n"
        "                 <src-port> - (0..2^16-1)\n"
        "              5- <src-ip> <src-port>\n"
        "                 <src-ip>   - IPv4 address in dot notation\n"
        "                 <src-port> - (0..2^16-1)\n"
        "              6- <Outer-ethtype> <Inner-ethtype> <Outer vlan-tag>\n"
        "                 <Outer-ethtype>  - 4 digits hex number\n"
        "                 <Inner-ethtype>  - 4 digits hex number\n"
        "                 <Outer vlan-tag> - 4 digits hex number\n"
        "              7- <src-ip> <Appl-ethtype>\n"
        "                 <src-ip>     - IPv4 address in dot notation\n"
        "                 <Appl-ethtype> - 4 digits hex number\n"
        "              8- <pri Type> <pri value> <ip-protocol> <Appl-ethtype> <dst-port> \n"
        "                  <pri Type> :\n"
        "                            0 - untag packet      \n"
        "                            1 - Outer vlan        \n"
        "                            2 - Inner vlan        \n"
        "                            3 - IPv4 Precedence   \n"
        "                            4 - IPv4 TOS          \n"
        "                            5 - IPv4 DSCP         \n"
        "                 <pri_value>       0-63  digits number \n"
        "                 <ip-protocol>   - 4 digits number\n"
        "                 <Appl-ethtype>  - 4 digits hex number\n"
        "                 <dst-port>      - (0..2^16-1(65535))\n"
        "              9- <pri Type> <pri value> <ip-protocol> <src-port> <dst-port>\n"
        "                  <pri Type> :\n"
        "                            0 - untag packet      \n"
        "                            1 - Outer vlan        \n"
        "                            2 - Inner vlan        \n"
        "                            3 - IPv4 Precedence   \n"
        "                            4 - IPv4 TOS          \n"
        "                            5 - IPv4 DSCP         \n"
        "                 <pri_value>       0-63  digits number \n"
        "                 <ip-protocol>   - 4 digits number\n"
        "                 <src-port> - (0..2^16-1(65535))\n"
        "                 <dst-port> - (0..2^16-1(65535))\n"
        "             10- <Inner vlan-tag> <Outer vlan-tag> <Outer-ethtype>\n"
        "                 <Inner vlan-tag>  - 4 digits hex number\n"
        "                 <Outer vlan-tag>  - 4 digits hex number\n"
        "                 <Outer-ethtype>   - 4 digits hex number\n"
        "             11- <Outer vlan-tag> <dst-port> <src-port>\n"
        "                 <Outer vlan-tag> - 4 digits hex number\n"
        "                 <dst-port> - (0..2^16-1)\n"
        "                 <src-port> - (0..2^16-1)\n"
        "             12- <Inner vlan-tag> <dst-port> <src-port>\n"
        "                 <Inner vlan-tag> - 4 digits hex number\n"
        "                 <dst-port> - (0..2^16-1)\n"
        "                 <src-port> - (0..2^16-1)\n"
        "             13- <pppoe-session-id> <ppp-protocol> <sa-mac-lsb>\n"
        "                 <pppoe-session-id> - 4 digits hex number\n"
        "                 <ppp-protocol-id>  - 4 digits hex number\n"
        "                 <sa-mac-lsb>       - XX:XX\n"
        "             14- <Outer-ethtype> <Appl-ethtype> <Outer vlan-tag>\n"
        "                 <Outer-ethtype>  - 4 digits hex number\n"
        "                 <Appl-ethtype>   - 4 digits hex number\n"
        "                 <Outer vlan-tag> - 4 digits hex number\n"
        "             15- <da-mac> <qtag-tag> <dst-ip>\n"
        "                 <da-mac>   - xx:xx:xx:xx:xx:xx\n"
        "                 <Outer vlan-tag> - 4 digits hex number\n"
        "                 <dst-ip>   - IPv4 address in dot notation\n"
        "              22- <dst-ipv6> <dst-port>\n"
        "                 <dst-ipv6>   - IPv6 address in dot notation\n"
        "                 <dst-port> - (0..2^16-1)\n"
        "              23- <src-ipv6> <dst-port>\n"
        "                 <src-ipv6>   - IPv6 address in dot notation\n"
        "                 <dst-port> - (0..2^16-1)\n"
        "              24- <dst-ipv6> <src-port>\n"
        "                 <dst-ipv6>   - IPv6 address in dot notation\n"
        "                 <src-port> - (0..2^16-1)\n"
        "              25- <src-ipv6> <src-port>\n"
        "                 <src-ipv6>   - IPv6 address in dot notation\n"
        "                 <src-port> - (0..2^16-1)\n"
        "              27- <src-ipv6> <Appl-ethtype>\n"
        "                 <src-ipv6>     - IPv6 address in dot notation\n"
        "                 <ethtype> - 4 digits hex number\n"



        "     <action_type> - 0-discard | 1-to CPU | 2-action\n"
        "  Options (relevant only if 'to action' is selected): \n"
        "  -------- \n"
        "\n"
        "    -sp (Source Port)\n"
        "             <port 0..127>\n"
        "    -sid (aclProf Id)\n"
        "             <aclProf id>\n"
        "    -o       (output Queues\n"
        "             <num_of_queues>\n"
        "             <queue1> [<queue2>]\n"

        "\n"
        "    -cos-cmd (force service to set force/limit priority queue)\n"
        "              <cmd> - 0-disable ,1-force value,2-max value\n"
        "              <VAL> - priority queue (0..7)\n"
        "    -pri-cmd (force service to set force/limit priority)\n"
        "              <cmd> - 0-disable ,1-force value,2-max value\n"
        "              <VAL>  - Priority value (0..7)\n"
        "    -col-cmd (force service to set force/limit color)\n"
        "              <cmd> - 0-disable ,1-force value,2-max value\n"
        "              <VAL> - color (0=Yellow 1=Red 2= Green) \n"
        "\n"
        "    -policer  (policer parameter)    \n"
        "       <CIR> - Committed Information Rate in bps       \n"
        "       <EIR> - Excess  Information Rate in bps         \n"
        "       <CBS> - Committed Burst Size in bytes \n"
        "       <EBS> - Excess Burst Size in bytes    \n"
        "       <coupling   0/1>                  \n"
        "       <col_aware 0/1>\n"
        "       <comp> policer compensation  \n"
        "\n"
        "    -Policer_mode (policer mode ) \n"
        "       <mode>  0-MEF 1 -BAG  \n"
        "\n"
        "    -soft_wred (Policer soft wred)"
        "           <value>   0 - end of queue \n"
        "                     1 - offset 96 \n"
        "                     2 - offset 64 \n"
        "                     3 - offset 32 \n"
        "\n"

        "    -pgt_sign (Policer granularity sign) \n"
        "          <sign>   0 -positive and  1 - negative granularity \n"
        "    -pgt (Policer granularity type) \n"
        "         <type> - 0..7 \n"
        "                  Notes: - For CBS/EBS the granularity will be \n"
        "                           (2^type) bytes.\n"
        "                         - For CIR/EIR the granularity will be \n"
        "                           ((2^type)*<device basic granularity>\n"
        "                         - <device basic granularity> in \n"
        "                         - type 0 has no effect , and this is the \n"
        "                           default.\n"
        "\n"
        "    -h (Ethernet editing commands options)\n"
        "        Note: Can come only after -o option\n"
        "       <VAL>     - Value             (8 hexdigits) \n"
        "       <CL_ST>   - Color stamping    (0/1) \n"
        "       <PRI_ST>  - Priority Stamping (0/1)\n"
        "       <CMD>     - Command (0=trans / 1=append / 2=extract / 3=swap\n"
        "                            5=swap da/sa and swap dstIp/srcIp and set ttl\n"
        "\n"
        "    -h2(Second editing commands options)\n"
        "        Note: Can come only after -o option\n"
        "         <VAL>  - Value             (8 hexdigits)  \n"
        "                  The value can be : \n"
        "                - PPPoE Session Id (4 hexdigits LSB)\n"
        "                - Double Tag      (8 hexdigits)\n"
        "         <CMD>  - Command (0=trnas / 1=append / 2=extract / 3=swap\n"
        "\n"
        "    -ho (outer Ethernet editing commands options)\n"
        "        Note: Can come only after -o option\n"
        "       <VAL>     - Value             (8 hexdigits) \n"
        "       <CL_ST>   - Color stamping    (0/1) \n"
        "       <PRI_ST>  - Priority Stamping (0/1)\n"
        "       <CMD>     - Command (0=trans / 1=append / 2=extract / 3=swap\n"
        "\n"
        "    -hwbrg (set editing brig mode)\n"
        "         <VAL>      - 0 - NONE  1-UnTag 2-Tag 3-doubleTag        \n"
        "\n"
		"    -hwmode (set editing brig mode swap)\n"
		"         <VAL>      - 0 - NONE  1-Tag 2-ATM 3-doubleTag        \n"
		"		  <ATM>		 - 0 - LAN EtherType / 1 - access EtherType	 \n"
		"		  <Vlan>	 - 0 - LAN EtherType / 1 - access EtherType	 \n"
		"\n"
        "    -h802_1p  set priority 802.1p \n"
        "           <VAL>  0:7 \n"
        "    -hs (Editing session options)\n"
        "       <valid>   - Disable (0) or Enable (1) \n"
        "       <enable>     Disable (0) or Enable\n"
        "       <do_extract>     Disable (0) or Enable\n"
        "       <id>      - Editing Session id \n"
        "\n"
        "    -hm (inner Ethernet editing mapping options)\n"
        "       <valid>   - Disable (0) or Enable (1) mapping \n"
        "                   for the stamping color/priority values \n"
        "                   before use them for editing stamping\n"
        "       <id>      - Editing Mapping Profile id \n"
        "\n"
        "    -hom (outer Ethernet editing mapping options)\n"
        "       <valid>   - Disable (0) or Enable (1) mapping \n"
        "                   for the stamping color/priority values \n"
        "                   before use them for editing stamping\n"
        "       <id>      - Editing Mapping Profile id \n"
        "\n"
        "    -lmid (LmId options for counters at the Egress)\n"
        "       <valid>   - Disable (0) or Enable (1) mapping \n"
        "       <id>      - LmId Profile id \n"
        "       <command_dasa> - command \n"
        "                      0-TRANSPARENT \n"
        "                      1-router  \n"
        "       <command> - command \n"
        "                           0-TRANSPARENT   \n"
        "                           1-SWAP_OAM=1    \n"
        "                           2-STAMP_TX=2    \n"
        "                           3-STAMP_RX=3    \n"
        "                           4-COUNT_CCM_TX  \n"
        "                           5-COUNT_CCM_RX  \n"
        "\n"
        "    -hdscp ( editing stamping dscp value to packet)\n"
        "       <valid>   - Disable (0) or Enable (1)  \n"
		"       <TypeIPprority> \n"
		"						0 - dscp - stamp 6b \n"
		"						1 - tos    - stamp 4b \n"
		"						2 - precedence - stamp 3b \n"
        "       <value>      - dscp value \n"
        "\n"
        "      -a (Atm editing commands options)\n"
        "        Note: Can come only after -o option\n"
        "         Notes: - This option is relevant only to ATM \n"
        "                - Can not set together with -h2 option\n"
        "         <VPI>       - (0..255)\n"
        "         <VCI>       - (0..2^16-1)\n"
        "         <CL_ST>     - Color stamping    (0/1) \n"
        "         <PRI_ST>    - Priority Stamping (0/1)\n"
        "\n"
        "      -s (Stamping information)\n"
        "         <c0v>  - Color    Bit0 Valid    (0 / 1)\n"
        "         <c0l>  - Color    Bit0 Location (0..31)\n"
        "         <c1v>  - Color    Bit1 Valid    (0 / 1)\n"
        "         <c1l>  - Color    Bit1 Location (0..31)\n"
        "         <p0v>  - Priority Bit0 Valid    (0 / 1)\n"
        "         <p0l>  - Priority Bit0 Location (0..31)\n"
        "         <p1v>  - Priority Bit1 Valid    (0 / 1)\n"
        "         <p1l>  - Priority Bit1 Location (0..31)\n"
        "         <p2v>  - Priority Bit2 Valid    (0 / 1)\n"
        "         <p2l>  - Priority Bit2 Location (0..31)\n"
        "\n"
        "      -m (Martini information)\n"
        "         <DA>        - MAC address  (xx:xx:xx:xx:xx:xx)\n"
        "         <SA>        - MAC address  (xx:xx:xx:xx:xx:xx)\n"
        "         <EtherType> - 4 hex digits\n"
        "         <command>    -  (0=transparent / 1=append / 2=extract / 3=swap / \n"
        "\n"
        "     -Tm  (used policer id) \n"
        "          <value>\n"
        "\n"
        "     -Ed  (used edit id )\n"
        "        Note: Can come only after -o option\n"
        "          <value> \n"
        "\n"
        "     -Pm (pm counter ID) \n"
        "          <value>        \n"
        "\n"
        "     -pm_type (Type of pm counter )\n"
        "          <Type> - (0-All (Default) / 1-Packets_only) \n"
        "\n"
        "    -protocol  (protocol type )\n"
        "           <valid> (0-Disable 1-Enable)\n"
        "         <value> - 0-Trans/EoA,1=VLAN/IPoA,2=MPLS/PPPoEoA,3=MART/PPPoA\n"
        "    -llc  (llc info)\n"
        "          <valid> Disable (0) or Enable (1)\n"
        "          <value>    0..1 \n"
        "    -set1588 (stamping 1588)"
        "          <valid> (0-Disable 1-Enable) \n"
        "    -sflow_type (setting type for sflow)"
        "          <type>  0-Disable \n" 
        "                  1- ingress sflow \n"
        "                  2- cyber sflow \n"
        "                  3- sflow no counting \n"
        "    -afdx (Rx afdx packet)\n"
        "           <valid> (0-Disable 1-Enable)\n"
        "           <streamId 1..255> \n"
        "           <take A or B> (0=A, 1=B) \n"
        "           <Type> \n"
        "   -lxcp_win  (decision if the lxcp win or the filter)\n"
        "               <valid> Disable (0) or Enable (1)\n"
        "   -cos_hpm   ( Cos Hpm )  \n"
        "               <valid> \n"
        "               value> 0:7 cos val\n"



        "   -fwd  (forwarder over ruling  )\n"
        "           <doDa>   fwd_enable  Disable (0) or Enable (1)\n"
        "           <Fwd key>  key forwarding Type 0:7\n"
        "           <doDa_int>  forward Internal-DA  Disable (0) or Enable (1)\n"
        "            <force_rule> Disable (0) or Enable (1)\n"
        "    -fwd_field_mask (mask field on FWD key)\n"
        "         <type> - 0..3 \n"
        "        FWD-key1          |      FWD-key4       |   key6/7            \n"
        "    0-noMask              |  0-noMask           |  0-IPv4 mask/32     \n"
        "    1-mask internal MPLS  |  1- mask MPLS/VLAN  |  1-IPv4 mask/24     \n"
        "    2-mask external MPLS  |  2-TBD              |  2-IPv4 mask/16     \n"
        "    3 TBD                 |  3 TBD              |  3-IPv4 mask/8      \n"

        "    -fwd_act_en  (The action will take from fwd)]\n"
        "           <valid> (0-Disable 1-Enable)\n"
        "-------------------------------------------------------------\n"
        " Examples:\n"
        " First example - traffic from port 0, DA MAC 00:01:00:00:00:01- send to cpu\n"
        
        " create 0 00:AA:BB:CC:DD:EE 2 -Pm 100 -o 1 105 -sp 104 -HIERARCHY 0 - fwd 1 6 0 1\n"
        " create 1 00:01:02:03:04:05 2 -Pm 101 -o 1 104 -sp 104 -HIERARCHY 1 \n"
        " create 2 11.22.33.44 4660  2 -Pm 104 -o 1 104 -sp 104 -HIERARCHY 2 \n"
        " create 5 55.66.77.88 4661  2 -Pm 105 -o 1 104 -sp 104 -HIERARCHY 3 \n"

        );


        CLI_defineCmd("MEA filter set delete",
            (CmdFunc)MEA_CLI_DeleteFilter,
            "Delete Filter(s)",
            "Delete <filterId>|all\n");


        CLI_defineCmd("MEA filter set modify",
            (CmdFunc)MEA_CLI_ModifyFilter,
            "Modify <filterId>\n",
            "Modify <filterId>\n"
            "    Options: \n"
            "------------------------------------------------------------------\n"
            "     [-o <number of out cluster> <outcluster1> ...<outclustern>] \n"
            "     [-policer <CIR> <EIR> <CBS> <EBS> <coupling> <col_aware> <comp>]\n"
            "     [-policer_mode <mode>]\n"
            "     [-policer_maxMtu <value>]\n"
            "     [-pgt <type>]\n"
            "     [-soft_wred <value>](0..3)"
            "     [-pgt_sign <sign>]\n"
            "     [-protocol <valid> <value>\n"
            "     [-llc      <valid> <value>\n"

            "     [-Tm  <value>]\n"
            "     [-Pm  <value>}\n"
            "     [-pol_id <id val> ]\n"
            "     [-mtu_id <id val> ]\n"
            "     [-cos-cmd <cmd> <val>]\n"
            "     [-pri-cmd <cmd> <val>]\n"
            "     [-col-cmd <cmd> <val>]\n"
            "     [-pm_type  <type>]\n"
            "     [-set1588 <valid >\n"
            "     [-lxcp_win <Valid>]\n"
            "     [-frag_IP  <en_fragment Ip> <en_TUNNEL> <frag_ex_int> <target_mtu_prof>]   \n"
            "     [-defrag_IP  <en_defrag_ext> <en_defrag_int><Reassembly_L2_prof >   \n"
            "     [-afdx <valid> <streamId 1..255> <take A or B> <Type>\n"
            "     [-sflow_type  <type>]\n"
            "-------------------------------------------------------------\n"
            "     <filterId>  - Filter Id \n"
            "  Options (relevant only if 'to action' is selected): \n"
            "  --------------- \n"
            "    -action_type  (change type of Action)  \n"
            "   <value>     0-discard | 1-to CPU | 2-action\n"
            "    -o       (output cluster)\n"
            "             <num_of_cluster>\n"
            "             <queue1> [<queue2>]\n"
            "    -col  (Color setting force)   \n"
            "           <color 0=Yellow/1=Red/2=Green> \n"
            "    -cos  (queue setting force)    \n"
            "         <queue  0..7>            \n"
            "    -pri  (priority setting force) \n"
            "           <pri val 0..7>         \n"
            "\n"
            "    -cos-cmd (force service to set force/limit priority queue)\n"
            "              <cmd> - 0-disable ,1-force value,2-max value\n"
            "              <VAL> - priority queue (0..7)\n"
            "    -pri-cmd (force service to set force/limit priority)\n"
            "              <cmd> - 0-disable ,1-force value,2-max value\n"
            "              <VAL>  - Priority value (0..7)\n"
            "    -col-cmd (force service to set force/limit color)\n"
            "              <cmd> - 0-disable ,1-force value,2-max value\n"
            "              <VAL> - color (0=Yellow 1=Red 2= Green) \n"
            "\n"
            "    -Policer_mode (policer mode ) \n"
            "             <mode>  0-MEF 1 -BAG  \n"
            "    -policer  (policer parameter)    \n"
            "       <CIR> - Committed Information Rate in bps       \n"
            "       <EIR> - Excess  Information Rate in bps         \n"
            "       <CBS> - Committed Burst Size in bytes \n"
            "       <EBS> - Excess Burst Size in bytes    \n"
            "       <coupling   0/1>                  \n"
            "       <col_aware 0/1>\n"
            "       <comp> Policer compensation  \n"
            "\n"
            "    -pgt_sign (Policer granularity sign) \n"
            "          <sign>   0 -positive and  1 - negative granularity \n"
            "    -pgt (Policer granularity type) \n"
            "         <type> - 0..7 \n"
            "                  Notes: - For CBS/EBS the granularity will be \n"
            "                           (2^type) bytes.\n"
            "                         - For CIR/EIR the granularity will be \n"
            "                           ((2^type)*<device basic granularity>\n"
            "                         - <device basic granularity> in \n"
            "                         - type 0 has no effect , and this is the \n"
            "                           default.\n"
            "\n"
            "    -h (Ethernet editing commands options)\n"
            "        Note: Can come only after -o option\n"
            "       <VAL>     - Value             (8 hex digits) \n"
            "       <CL_ST>   - Color stamping    (0/1) \n"
            "       <PRI_ST>  - Priority Stamping (0/1)\n"
            "       <CMD>     - Command (0=trans / 1=append / 2=extract / 3=swap\n"
            "                            5=swap da/sa and swap dstIp/srcIp and set ttl\n"

            "    -h2(Second editing commands options)\n"
            "        Note: Can come only after -o option\n"
            "         <VAL>  - Value             (8 hex digits)  \n"
            "                  The value can be : \n"
            "                - PPPoE Session Id (4 hex digits LSB)\n"
            "                - Double Tag      (8 hex digits)\n"
            "         <CMD>  - Command (0=trnas / 1=append / 2=extract / 3=swap\n"

            "    -ho (outer Ethernet editing commands options)\n"
            "        Note: Can come only after -o option\n"
            "       <VAL>     - Value             (8 hex digits) \n"
            "       <CL_ST>   - Color stamping    (0/1) \n"
            "       <PRI_ST>  - Priority Stamping (0/1)\n"
            "       <CMD>     - Command (0=trans / 1=append / 2=extract / 3=swap\n"
            "    -hwbrg (set editing brig mode)\n"
            "         <VAL>      - 0 - NONE  1-UnTag 2-Tag 3-doubleTag        \n"
            "\n"
			"    -hwmode (set editing brig mode swap)\n"
			"         <VAL>      - 0 - NONE  1-Tag 2-ATM 3-doubleTag        \n"
			"		  <ATM>		 - 0 - LAN EtherType / 1 - access EtherType	 \n"
			"		  <Vlan>	 - 0 - LAN EtherType / 1 - access EtherType	 \n"
			"\n"
            "    -h802_1p  set priority 802.1p \n"
            "           <VAL>  0:7 \n"
            "      -a (Atm editing commands options)\n"
            "        Note: Can come only after -o option\n"
            "         Notes: - This option is relevant only to ATM \n"
            "                - Can not set together with -h2 option\n"
            "         <VPI>       - (0..255)\n"
            "         <VCI>       - (0..2^16-1)\n"
            "         <CL_ST>     - Color stamping    (0/1) \n"
            "         <PRI_ST>    - Priority Stamping (0/1)\n"

            "      -s (Stamping information)\n"
            "         <c0v>  - Color    Bit0 Valid    (0 / 1)\n"
            "         <c0l>  - Color    Bit0 Location (0..31)\n"
            "         <c1v>  - Color    Bit1 Valid    (0 / 1)\n"
            "         <c1l>  - Color    Bit1 Location (0..31)\n"
            "         <p0v>  - Priority Bit0 Valid    (0 / 1)\n"
            "         <p0l>  - Priority Bit0 Location (0..31)\n"
            "         <p1v>  - Priority Bit1 Valid    (0 / 1)\n"
            "         <p1l>  - Priority Bit1 Location (0..31)\n"
            "         <p2v>  - Priority Bit2 Valid    (0 / 1)\n"
            "         <p2l>  - Priority Bit2 Location (0..31)\n"

            "      -m (Martini information)\n"
            "         <DA>        - MAC address  (xx:xx:xx:xx:xx:xx)\n"
            "         <SA>        - MAC address  (xx:xx:xx:xx:xx:xx)\n"
            "         <EtherType> - 4 hex digits\n"
            "         <command>    -  (0=transparent / 1=append / 2=extract / 3=swap / \n"
            "\n"
            "     -mtu_id  (used mtu id) \n"
            "          <value>\n"
            "     -Tm  (used policer id) \n"
            "          <value>\n"

            "     -Ed  (used edit id )\n"
            "        Note: Can come only after -o option\n"
            "          <value> \n"
            "     -Pm (pm counter ID) \n"
            "          <value>        \n"
            "      -pm_type (Type of pm counter )\n"
            "          <Type> - (0-All (Default) / 1-Packets_only) \n"
            "    -protocol  (protocol type )\n"
            "           <valid> (0-Disable 1-Enable)\n"
            "         <value> - 0-Trans/EoA,1=VLAN/IPoA,2=MPLS/PPPoEoA,3=MART/PPPoA\n"
            "    -llc  (llc info)\n"
            "          <valid> Disable (0) or Enable (1)\n"
            "          <value>    0..1 \n"
            "    -set1588 (stamping 1588)"
            "          <valid> (0-Disable 1-Enable) \n"
            "    -sflow_type (setting type for )"
            "          <type>  0-Disable \n"
            "                  1- ingress sflow \n"
            "                  2- cyber sflow \n"
            "                  3- sflow no counting \n"
            "    -afdx (Rx afdx packet)\n"
            "           <valid> (0-Disable 1-Enable)\n"
            "           <streamId 1..255> \n"
            "           <take A or B> (0=A, 1=B) \n"
            "           <Type> \n"
            "   -lxcp_win  (decision if the lxcp win or the filter)\n"
            "               <valid> Disable (0) or Enable (1)\n"
            "-------------------------------------------------------------\n"
            "Examples: - modify 10 -policer 100000000 0 10000 0 0 0 20 -Pm 11 \n"
            "          - modify 10 -o 2 125 126\n"
            );

    return MEA_OK;
}
