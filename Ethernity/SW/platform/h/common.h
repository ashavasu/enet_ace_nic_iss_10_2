/*
libenet - Library for access enet's PCIe device.

This file/directory and the information contained in it are
proprietary and confidential to Ethernity Networks Ltd.
No person is allowed to copy, reprint, reproduce or publish
any part of this document, nor disclose its contents to others,
nor make any use of it, nor allow or assist others to make any
use of it - unless by prior written express
authorization of Neralink  Networks Ltd and then only to the
extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/


#define prefix "libenet"
#define pr_err(fmt, ...) \
	fprintf(stderr, "%s error: " fmt, prefix, ## __VA_ARGS__);

static int debug_flags = 0;
#define pr_dbg(fmt, ...) do { \
	if (debug_flags) \
		fprintf(stdout, "%s debug: " fmt, prefix, ## __VA_ARGS__); \
} while (0) \

#define pr_info(fmt, ...) \
	fprintf(stdout, "%s info: " fmt, prefix, ## __VA_ARGS__);

#define PCI_MAX_BARS 6

struct enet_pci_bar {
	int fd;
	void *base;
	uint32_t size;
};

struct  enet_dev {
	uint16_t domain;
	uint8_t bus;
	uint8_t dev;
	uint8_t func;

	struct enet_pci_bar bar[PCI_MAX_BARS];
};
