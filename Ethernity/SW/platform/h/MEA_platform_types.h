/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef MEA_PLATFORM_TYPES_H
#define MEA_PLATFORM_TYPES_H

#ifdef MEA_OS_OC
#include "defines.h"
#else
//#error "not including defines.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif 



#ifdef MEA_OS_LINUX
#define MEA_OS_ATTRIBUTE_PACKED  __attribute__ ((packed))
#else
/* T.B.D */
#define MEA_OS_ATTRIBUTE_PACKED
#endif

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#ifndef MEA_OS_Z1
#if !defined(MEA_OS_LINUX) && !defined(__FUNCTION__) 
#define __FUNCTION__            __FILE__ ":" TOSTRING(__LINE__)
#endif  
#endif


typedef char MEA_Int8;
typedef unsigned char MEA_Uint8;

typedef short MEA_Int16;
typedef unsigned short MEA_Uint16;

#ifdef ARCH64
typedef long MEA_Int32;
typedef unsigned long MEA_Uint32;
#else
typedef int MEA_Int32;
typedef unsigned int MEA_Uint32;
#endif


typedef signed long long int MEA_int64; 
typedef unsigned long long int MEA_uint64; 


typedef void * MEA_funcParam;

typedef   MEA_Uint32 *  MEA_PUint32_t;

typedef unsigned long  MEA_ULong_t;
typedef long  MEA_Long_t;

typedef unsigned long * MEA_pULong_t;



typedef union MEA_Int64 {
#ifdef MEA_OS_OC
	s64 val;
#else
MEA_int64 val;
#endif
	struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 lsw;
        MEA_Int32 msw;
#else
        MEA_Int32 msw;
        MEA_Uint32 lsw;
#endif
	} s;
} MEA_Int64;

typedef union MEA_Uint64 {
#ifdef MEA_OS_OC
	u64 val;
#else
    MEA_uint64 val;
#endif
	MEA_Uint32 v[2];
	struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 lsw;
        MEA_Uint32 msw;
#else
        MEA_Uint32 msw;
        MEA_Uint32 lsw;
#endif
	} s;
} MEA_Uint64;

typedef union MEA_MacAddr {
	MEA_Uint8 b[6];
	MEA_Uint16 s[3];
	struct {
		MEA_Uint32 msw;
		MEA_Uint16 lss;
	} a;
} MEA_MacAddr;

typedef union MEA_Counter_t {
MEA_uint64 val;
struct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
        MEA_Uint32 lsw;
        MEA_Uint32 msw;
#else
        MEA_Uint32 msw;
        MEA_Uint32 lsw;
#endif
	} s;
} MEA_Counter_t;

typedef struct {
    char *name;
    int bus;
    int slot;
    int func;

}MEA_PCIe_Divice_dbt;



/*****************************************************************/
typedef struct {
    int status;
    int in_param[8];
    int out_param[8];
    int data[100];
}tsctl_cmd ;


#define MEA_MAX_PCI_DEV_NUM 16
#define MAGIC_NUM 'J'

#define DEVICE_NODE "/dev/tsctl0"
#define DEVICE_NAME "tsctl" 

#define DEVICE_NAME_ENET "ethernity"


#define DEVICE_NODE_ENET_bar0 "bar0"
#define DEVICE_NODE_ENET_bar1 "bar1"
#define DEVICE_NODE_ENET_bar2 "bar2"
#define DEVICE_NODE_ENET_stat "dma-stat"



#define IOCTL_TX_MSG(cmd) _IOWR(MAGIC_NUM, cmd, tsctl_cmd)


typedef enum {
    IF_SCAN,
    GET_DEV_NUM,
    IS_TS = 10,
    GET_TS_MASTER,
    GET_TS_SLAVE,
    GET_TS_INFO,
    GET_TS_CAPS,
    SET_TS,
    GET_TS,
    SET_TS_PPS,
    GET_TS_PPS,
    SET_TS_FC,
    GET_TS_FC,
    SET_TS_COMPENSATE,
    GET_TS_COMPENSATE,
    SET_TS_VALUE,
    GET_TS_VALUE,
    SET_TS_SIGN,
    GET_TS_SIGN,
    SET_TS_LEN,
    GET_TS_LEN,
    SET_TS_ENDIAN,
    GET_TS_ENDIAN,
    SET_TS_CTLB,
    GET_TS_CTLB,
    SET_TS_PPS_OUT,
    GET_TS_PPS_OUT,
    SET_TS_PPS_CTL,
    GET_TS_PPS_CTL,
    SET_TS_CLK_COMP,
    GET_TS_CLK_COMP,
    SET_TS_CLK_CORRECT,
    GET_TS_PPS_AUTO,
    SET_TS_PPS_LOSS,
    GET_TS_PPS_LOSS,
    GET_TS_PPS_TLOSS,
    SET_TS_PPS_RESTORE,
    GET_TS_PPS_RESTORE,
    GET_TS_PPS_TRESTORE,
    GET_TS_PPS_LATCH,
    GET_TS_LATCH,
    SET_TS_COMP_VALUE,
    GET_TS_COMP_VALUE,
    SET_TS_MIX,
    GET_TS_MIX,
    GET_TS_ERR = 70,
    GET_TS_STAT,
    GET_TS_STATUS,
    RESET_TS,
    SET_TS_CRC = 100,
    GET_TS_CRC,
    SET_STORE_BAD_PKT,
    GET_STORE_BAD_PKT,
    /* #ifdef TS_RD_WR_REGS */
    GET_TS_REG = 200,
    SET_TS_REG,
    /* #endif */
    /* #ifdef TS_RD_WR_PHY_REGS */
    GET_TS_PHY_REG,
    SET_TS_PHY_REG,
    /* #endif */
    /* #ifdef TS_RD_WR_DEV_REGS */
    GET_TS_DEV_REG, /*Read for FPGA*/
    SET_TS_DEV_REG, /*Set For FPGA */

    GET_TS_DEV_REG_FPGA_DDR, /*Read for FPGA*/
    SET_TS_DEV_REG_FPGA_DDR, /*Set For FPGA */


    /* #endif */
    /* #ifdef TS_RD_WR_SCI_REGS */
    GET_TS_SCI_REG,
    SET_TS_SCI_REG,
    /* #endif */
    /* #ifdef TS_RD_WR_PCI_REGS */
    GET_TS_PCI_REG,
    SET_TS_PCI_REG,
    /* #endif */
    /* #ifdef TS_DIAG */
    SET_TS_DIAG = 300,
    /* #endif */
    /* #ifdef TS_SELF_TEST */
    SET_TS_SELF_TEST = 400,
    GET_TS_SELF_TEST,
    /* #endif */
}CMND_TYPE_TS;


typedef struct {

    MEA_Uint32 src_port;
    MEA_Uint32 frag_schd_pri;
  
    MEA_Uint32 proto_type;
    MEA_Uint32 llc;
    MEA_Uint32 fragment_enable;
    
    MEA_Uint32 pm_type;
    MEA_Uint32 pmId_swId;
    MEA_Uint32 pmId_hwId;
    
    MEA_Uint32 tmId_hwId;
    MEA_Uint32 tmId_swId;

    MEA_Uint32 edId_hwId;
    MEA_Uint32 edId_swId;
    
    MEA_Uint32 frag_header;
    MEA_Uint32 dfrag;
    MEA_Uint32 cpu;
    MEA_Uint32 pos;
    MEA_Uint32 alr_fcos;
    MEA_Uint32 alr_cos;
    MEA_Uint32 psr_cos;
    MEA_Uint32 alr_fcolor;
    MEA_Uint32 alr_color;
    MEA_Uint32 psr_color_awr;
    MEA_Uint32 psr_color;
    MEA_Uint32 alr_fl2pri;
    MEA_Uint32 alr_l2pri;
    MEA_Uint32 psr_pri_vld;
    MEA_Uint32 psr_pri;
    MEA_Uint32 port_num;
    MEA_Uint32 dest_port;
    MEA_Uint32 ing_flow_pol;
    MEA_Uint32 ip_frag_inner :1;
    MEA_Uint32 ip_frag_outer :1;
    MEA_Uint32 pcacket_reassembl_en :1;
    MEA_Uint32 ip_reassembly_session_id :8;
    MEA_Uint32 ing_flow_mtu : 9;


}mea_if_to_bm_dbt;

typedef struct {
    MEA_Uint32 byte_count      : 14;
    MEA_Uint32 error_code_type : 4;
    MEA_Uint32 session_id      : 8;
    MEA_Uint32 session_timeout : 15;
    MEA_Uint32 pm_id           : 21;
    MEA_Uint32 valid_error     : 1;




}mea_ipfrag_reassembly_dbt;


typedef struct {
    
    /*KEY */
    MEA_Uint32  Ip_ID       : 16;
    MEA_Uint32  IP_protocol : 8;
    MEA_Uint32  Src_IPV4;
    MEA_Uint32  Dst_IPV4;
    
    /**/
    MEA_Uint32 session_id    : 16;
    MEA_Uint32 enet_Src_port : 16;




}mea_ip_reassembly_lastEvent_dbt;




#define MEA_OS_UPDATE_COUNTER(counter,val)  (((counter) += (val)))
#define MEA_OS_SUM_COUNTER(counter1,counter2) ((counter1).val += (counter2).val)

#if 0
#ifdef MEA_COUNTER_64_SUPPORT
#define MEA_OS_UPDATE_COUNTER(counter,val) \
if ((0xFFFFFFFF - (counter).s.lsw) < (val)) { \
  (counter).s.lsw = (val) - ( 0xFFFFFFFF - (counter).s.lsw) - 1; \
  (counter).s.msw++; \
} else { \
  (counter).s.lsw += (val); \
}
#else
#define MEA_OS_UPDATE_COUNTER(counter,val) ((counter).s.lsw += (val))
#endif
#endif
#if 0
#ifdef MEA_COUNTER_64_SUPPORT
#define MEA_OS_SUM_COUNTER(counter1,counter2) \
if ((0xFFFFFFFF - (counter1).s.lsw) < (counter2).s.lsw) { \
  (counter1).s.lsw = (counter2).s.lsw - ( 0xFFFFFFFF - (counter1).s.lsw) - 1; \
  (counter1).s.msw++; \
  (counter1).s.msw += (counter2).s.msw; \
} else { \
  (counter1).s.lsw += (counter2).s.lsw; \
  (counter1).s.msw += (counter2).s.msw; \
}
#else
#define MEA_OS_SUM_COUNTER(counter1,counter2) ((counter1).s.lsw += (counter2).s.lsw)
#endif
#endif

typedef MEA_Int32 MEA_Status;

#define MEA_BUSY      (-3)
#define MEA_COLLISION (-2)
#define MEA_ERROR     (-1)
#define MEA_OK        ( 0)

typedef enum {
	MEA_FALSE = 0,
	MEA_TRUE  = 1
} MEA_Bool;

typedef enum {
	MEA_DISABLE,
	MEA_ENABLE
} MEA_State;

typedef enum {
	MEA_SUB_SYS_TYPE_GENERAL,
	MEA_SUB_SYS_TYPE_ENET4000_QXGMII,
	MEA_SUB_SYS_TYPE_ENET4000_5G,
	MEA_SUB_SYS_TYPE_ENET4000_8G,
	MEA_SUB_SYS_TYPE_ENET4000_10G,
	MEA_SUB_SYS_TYPE_ENET4000_12G,
	MEA_SUB_SYS_TYPE_ENET3500_4G,
    MEA_SUB_SYS_TYPE_ENET3500_8G,

	MEA_SUB_SYS_TYPE_LAST
} MEA_subSysType_te;


typedef enum {
	MEA_ACL5_Parser_NONE =0,
	MEA_ACL5_Parser_MATCH = 1,
	MEA_ACL5_Parser_UNMATCH = 2
} MEA_ACL5_PAC_MODE;




typedef MEA_Uint32 MEA_Limiter_t;
typedef MEA_Uint32 MEA_Filter_t;
typedef MEA_Uint16 MEA_FilterMask_t;
typedef MEA_Uint32 MEA_Port_t;
typedef MEA_Uint32 MEA_Service_t;
typedef MEA_Uint32 MEA_Editing_t;
typedef MEA_Uint16 MEA_Mapping_t;
typedef MEA_Uint32 MEA_Action_t;
typedef MEA_Uint32 MEA_PmId_t;
typedef MEA_Uint32 MEA_TmId_t;
typedef MEA_Uint16 MEA_Mtu_t;	/* 14 bits */
typedef MEA_Uint32 MEA_Mqs_t;	/* Ver2: 12 bits , Ver3: 9 bits */
typedef MEA_Uint16 MEA_Policer_prof_t; 
typedef MEA_Uint32 MEA_MtuMru_t;
typedef MEA_uint64 MEA_EirCir_t;
typedef MEA_Uint32 MEA_EbsCbs_t;
typedef MEA_Uint16 MEA_LxCp_t;
typedef MEA_Uint32 MEA_WredProfileId_t;
typedef MEA_Uint16 MEA_PacketGen_t;
typedef MEA_Uint16 MEA_PacketGen_profile_info_t;
typedef MEA_Uint16 MEA_Analyzer_t;
typedef MEA_Uint16 MEA_LxcpAction_t;
typedef MEA_Uint16 MEA_FlowCoSMappingProfile_Id_t;
typedef MEA_Uint16 MEA_FlowMarkingMappingProfile_Id_t;
typedef MEA_Uint16 MEA_EditingMappingProfile_Id_t;
typedef MEA_Uint16 MEA_LmId_t;
typedef MEA_Uint16 MEA_CcmId_t;
typedef MEA_Uint16 MEA_periodCCM_Id_t;
typedef MEA_Uint16 MEA_periodBFD_Id_t;
typedef MEA_Uint16 MEA_BFDId_t;
typedef MEA_Uint32 MEA_ACL5Id_t;
typedef MEA_Uint16 MEA_ACL5Index_t;


typedef MEA_Uint32 MEA_TdmCesId_t;
typedef MEA_Uint16 MEA_AcmMode_t;
typedef MEA_Uint16 MEA_Priority_t;
typedef MEA_Uint16 MEA_Interface_t;
typedef MEA_Uint16 MEA_HDC_t;
typedef MEA_Uint16 MEA_EgressFilter_t;
typedef MEA_Uint16 MEA_TFT_t;
typedef MEA_Uint16 MEA_UEPDN_t;
typedef MEA_Uint16 MEA_Virtual_Rmon_t;
typedef MEA_Uint32 MEA_ACL5_t;





typedef enum
{


	MEA_TYPE_Ports2Interf_Interface_Type = 0,
	MEA_TYPE_Ports2Interf_Interface_BW = 1,
	MEA_TYPE_Ports2Interf_Interface_Hw_index = 2,
	MEA_TYPE_Ports2Interf_Interface_Packet_mode = 3,
	MEA_TYPE_Ports2Interf_Interface_interface_ID = 4,
	MEA_TYPE_Ports2Interf_Interface_Port_type = 5,
	MEA_TYPE_Ports2Interf_Interface_LAST
	
}mea_drv_Ports2Interface_t;






typedef enum
{
    MEA_EVENT_DBG_ALL = 0,
    MEA_EVENT_DBG_IF = 1,
    MEA_EVENT_DBG_BM = 2,
    MEA_EVENT_DBG_SYS = 3,
    MEA_EVENT_DBG_PORT = 4,
    MEA_EVENT_DBG_parser = 5,
    MEA_EVENT_DBG_TDM = 6,
    MEA_EVENT_DBG_IL=7,
    MEA_EVENT_DBG_T_V=8,
    MEA_EVENT_DBG_PORT_ELIGIBILITY=9, 
    MEA_EVENT_DBG_QUEUE_PAUSE=10

}MEA_EVENT_dbg_t;

typedef union {
    struct{
#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 ip_v     : 4;
    MEA_Uint32 ip_hl    : 4;
    MEA_Uint32 ip_dscp  : 6;
    MEA_Uint32 ip_ecn   : 2;
    MEA_Uint32 ip_len   : 16;
#else
    MEA_Uint32 ip_len   : 16;
    MEA_Uint32 ip_ecn   : 2;
    MEA_Uint32 ip_dscp  : 6;
    MEA_Uint32 ip_hl    : 4;
    MEA_Uint32 ip_v     : 4;
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 ip_Identification : 16;
    MEA_Uint32 ip_flag : 3;
    MEA_Uint32 ip_frag_offset : 13;
#else
    MEA_Uint32 ip_frag_offset       :13;
    MEA_Uint32 ip_flag              : 3;
    MEA_Uint32 ip_Identification    :16;
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32 ip_ttl : 8;
    MEA_Uint32 ip_protocol : 8;
    MEA_Uint32 ip_header_checksum : 16;

#else
    MEA_Uint32 ip_header_checksum   :16;
    MEA_Uint32 ip_protocol          :8;
    MEA_Uint32 ip_ttl               :8;
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32  ip_src;
#else
    MEA_Uint32  ip_src;
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN 
    MEA_Uint32  ip_dst;
#else
    MEA_Uint32  ip_dst;
#endif
    }st;
    MEA_Uint16 regs[10];

}MEA_Ipheader;


typedef  struct{
    MEA_Uint32 regs[8];        // bit fields 0-255

} MEA_Internal_Block_256OutPorts_dbt;



/************************************************************************/
/*  IngressAfdx Bag                                                      */
/************************************************************************/
typedef struct {

    MEA_Uint32 Bag   ; /* Bag window  0- 0.5msec 1,2,4,8,16,32,64,128.msec*/

}MEA_RxAfdxBag_dbt;


typedef struct  
{
    MEA_Uint16 valid :1;
    MEA_Uint16 pad   :15;
}MEA_GlobalParser_parsing_L2KEY_t;




typedef enum{
    MEA_SETTING_TYPE_CREATE=0,
    MEA_SETTING_TYPE_UPDATE=1, // SET
    MEA_SETTING_TYPE_DELETE=2,
    MEA_SETTING_TYPE_GET=3,
    MEA_SETTING_TYPE_LAST
}MEA_SETTING_TYPE_te;


/* Note: We define the next 2 typedefs  as 16 bits and note 8 bits 
         to avoid loop problems (rap-round of the loop index) , 
         and also to avoid warning on compression is always false 
         due to limited range of data type */
typedef MEA_Uint16  MEA_DropProbability_t; /* 0 .. MEA_WRED_PROBABILITY_MAX_VALUE   */
typedef MEA_Uint16  MEA_NormalizeAQS_t;    /* 0 .. MEA_WRED_NORMALIZE_AQS_MAX_VALUE */

typedef enum {
	MEA_COUNTERS_PMS_TYPE = 0,
	MEA_COUNTERS_QUEUE_TYPE = 1,
	MEA_COUNTERS_BMS_TYPE = 2,
	MEA_COUNTERS_INGRESSPORTS_TYPE = 3,
	MEA_COUNTERS_RMONS_TYPE = 4,
	MEA_COUNTERS_ANALYZERS_TYPE = 5,
	MEA_COUNTERS_ANALYZERS_LM_TYPE = 6,
	MEA_COUNTERS_ANALYZERS_CCM_TYPE = 7,
    MEA_COUNTERS_PMS_TDM_TYPE = 8,
    MEA_COUNTERS_RMONS_TDM_TYPE = 9,
    MEA_COUNTERS_CES_EVENT_TDM_TYPE = 10,
    MEA_COUNTERS_UTOPIA_RMONS_TYPE = 11,
    MEA_COUNTERS_TDM_EGRESS_FIFO_TYPE = 12,
    MEA_COUNTERS_BONDING_TYPE=13,
    MEA_COUNTERS_FORWARDER_TYPE=14,
    MEA_COUNTERS_INGRESS_TYPE=15,
    MEA_COUNTERS_HC_RMON_TYPE=16,
    MEA_COUNTERS_ACL_TYPE=17,
    MEA_COUNTERS_QUEUE_HANDLER_TYPE=18, 
    MEA_COUNTERS_Virtual_Rmon_TYPE = 19,

    

    MEA_COUNTERS_LAST_TYPE
}MEA_Counters_Type_t;


typedef enum {
	MEA_UNIT_0 = 0,
	MEA_UNIT_LAST
} MEA_Unit_t;

typedef enum {
	MEA_GLOBAL_CLUSTER_MODE_RR_PRIORITY = 0,
	MEA_GLOBAL_CLUSTER_MODE_WFQ_PRIORITY = 1
} MEA_Global_ClusterWfq_te;

typedef enum {
	MEA_GLOBAL_PRI_Q_MODE_RR_PRIORITY = 0,
	MEA_GLOBAL_PRI_Q_MODE_WFQ_PRIORITY = 1
} MEA_Global_PriQWfq_te;

typedef int(*MEA_FUNCPTR) (MEA_funcParam, ...);	/* ptr to function returning void */


typedef void (*MEA_VOIDFUNCPTR) (void);	/* ptr to function returning void */

#define MEA_NUM_OF_ELEMENTS(x) (sizeof((x))/sizeof((x)[0]))
typedef enum {
	MEA_MODE_REGULAR_ENET3000,
	MEA_MODE_REGULAR_ENET4000,
	MEA_MODE_UPSTREEM_ONLY,
	MEA_MODE_DOWNSTREEM_ONLY,
	MEA_MODE_LAST
} mea_memory_mode_e;



#if defined(MEA_OS_Z1) &&  !defined(MEA_OS_DEVICE_MEMORY_SIMULATION)

#include "switch.h"
#include "global.h"
#include "os.h"
#include "qlm.h"
#include "qcm.h"
#include "qrm.h"
#include "mbuf.h"
#include "cbuf.h"
#include "filter.h"
#include "spt.h"
#include "timer.h"
#include "sys_proc.h"
#include "raserror.h"
#include "sys_clk.h"
#include "stdio.h"
#include "rassys.h"

typedef sig_t MEA_Semaphore_t;
typedef MEA_Semaphore_t *MEA_pSemaphore_t;
#define MEA_LOCK(x)   swait(*(x),0)
#define MEA_TRYLOCK(x) swait(*(x),0)
#define MEA_UNLOCK(x) ssignal(*(x))
#define MEA_SEMAPHORE_INIT_DEF_VALUE
#define MEA_SEMAPHORE_INIT_VALUE(x) \
    { \
       static SemInitDone = MEA_FALSE; \
 	   if  (!SemInitDone) { \
           (*(x)) = screatesem();  \
           MEA_UNLOCK(x); \
           SemInitDone = MEA_TRUE; \
       } \
	} \

#else				/* MEA_OS_Z1 */

#ifdef MEA_OS_LINUX
#ifndef __KERNEL__
#include <pthread.h>
typedef pthread_mutex_t MEA_Semaphore_t;
typedef pthread_mutex_t *MEA_pSemaphore_t;
#define MEA_SEMAPHORE_INIT_DEF_VALUE  = PTHREAD_MUTEX_INITIALIZER
#define MEA_SEMAPHORE_INIT_VALUE(x)
#define MEA_LOCK(x) pthread_mutex_lock((x))
#define MEA_TRYLOCK(x) pthread_mutex_trylock((x))
#define MEA_UNLOCK(x) pthread_mutex_unlock((x))




#else				/* __KERNEL__ */
typedef MEA_Uint32 MEA_Semaphore_t;
typedef MEA_Semaphore_t *MEA_pSemaphore_t;
#define MEA_SEMAPHORE_INIT_DEF_VALUE
#define MEA_SEMAPHORE_INIT_VALUE(x)
#define MEA_LOCK(x)
#define MEA_TRYLOCK(x) 0
#define MEA_UNLOCK(x)
#endif				/* __KERNEL__ */

#else

/* T.B.D - TO complete it to other platforms (for example PSOS , VxWorks , Windows ) */
#ifdef MEA_OS_PSOS
#include <psos.h>
#include <ctype.h>
#endif
typedef MEA_Uint32 MEA_Semaphore_t;
typedef MEA_Semaphore_t *MEA_pSemaphore_t;
#define MEA_SEMAPHORE_INIT_DEF_VALUE
#define MEA_SEMAPHORE_INIT_VALUE(x)
#define MEA_LOCK(x)
#define MEA_TRYLOCK(x) -1
#define MEA_UNLOCK(x)



#endif

#endif				/* MEA_OS_Z1 */

typedef union {
	char b[4];
	MEA_Uint32 all;
} MEA_IpAddr_t;

#define MEA_CPU_MAX_MTU_SIZE     1472
#define MEA_AAL5_TRAILER_SIZE    8
/* append vlan*/
#define MEA_CPU_INFO_SIZE        4
#define MEA_CPU_INFO_DOUBLE_SIZE        8

typedef union {
	struct {
		char ethType[2];
		char port[2];
	} single_vlan;
	struct {
		char outer_ethType[2];
		char mng_vid[2];
		char inner_ethType[2];
		char port[2];
	} double_vlan;
} MEA_CPU_ExtraHeader_t;

typedef struct {
	char DA[6];
} MEA_DA_t;
typedef struct {
	char DA[6];
	char SA[6];
	char ethType[2];
	char port[2];
} MEA_CPU_Header_t;

#define MEA_CPU_MAX_DATA_SIZE    (MEA_CPU_MAX_MTU_SIZE)

typedef struct {
	///MEA_CPU_Header_t header;
	char data[MEA_CPU_MAX_DATA_SIZE+1];
} MEA_CPU_frame_t;


typedef void *MEA_db_dbt;

typedef MEA_Uint32 MEA_db_HwUnit_t;

typedef MEA_Uint32 MEA_db_HwId_t;

typedef struct {
    MEA_db_HwUnit_t HwUnit;
    MEA_db_HwId_t   HwId;
}MEA_db_Hw_Info_t;

typedef MEA_Uint32 MEA_db_SwId_t;


typedef struct {
    MEA_Uint32 moduleOffset;
    MEA_ULong_t memory;
    MEA_ULong_t base_address;
}mea_module_memory_dbt;


#ifdef __cplusplus
 }
#endif 

#endif /* MEA_PLATFORM_TYPES_H */




