/*
libenet - Library for access enet's PCIe device.

This file/directory and the information contained in it are
proprietary and confidential to Ethernity Networks Ltd.
No person is allowed to copy, reprint, reproduce or publish
any part of this document, nor disclose its contents to others,
nor make any use of it, nor allow or assist others to make any
use of it - unless by prior written express
authorization of Neralink  Networks Ltd and then only to the
extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#ifndef _SYSFS_H_
#define _SYSFS_H_
#ifdef MEA_OS_LINUX
int enet_sysfs_is_device_exit(struct enet_dev *dev);
int enet_sysfs_open_resources(struct enet_dev *dev);
void enet_sysfs_close_resources(struct enet_dev *dev);
int enet_sysfs_mmap_resources(struct enet_dev *dev);
void enet_sysfs_unmap_resources(struct enet_dev *dev);
int enet_sysfs_is_device_enabled(struct enet_dev *dev);
int enet_sysfs_enable_device(struct enet_dev *dev);
#endif
#endif /* _SYSFS_H_ */
