/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef _MEA_PLATFORM_OS_UTILS_H_
#define _MEA_PLATFORM_OS_UTILS_H_


#include "MEA_platform_types.h"
#include "ENET_platform_types.h"

#ifdef __cplusplus
extern "C" {
#endif


#define   MEA_UNUSED_PARAM(x)   ((void)x)


#define MEA_OS_MIN(x,y) (((x)<(y))?(x):(y))
#define MEA_OS_MAX(x,y) (((x)>(y))?(x):(y))

#define MEA_STATUS_STR(status) status==0 ? "NO":"YES"
#define MEA_STATUS_STR_Y(status) status==0 ? "N":"Y"

#define MEA_STATUS_STR_ENABLE(status) status==1 ? "Enable" : "Disable"



#define MEA_MAC_STRING_LENGTH	(18+1)
#ifndef __KERNEL__
#define MEA_OS_fflush fflush
#else
#define MEA_OS_fflush #warinig flash is not supported
#endif

#define MEA_CRC_INIT 0xFFFF

#define MEA_OS_getenv getenv
#if 0
// only on linux 2.6 need to add to MV_LIBS = -lrt
MEA_int64 MEA_OS_getUpTimeMs(void);

#endif

void MEA_OS_show_valueType(MEA_Uint8 type, char *str_name, MEA_Uint32 value);

void MEA_OS_dec2bin(long decimal, char *binary);
int MEA_OS_atoi(const char *nptr);
MEA_Uint32 MEA_OS_atoui(const char *nptr);
MEA_Bool    MEA_OS_ISNumber(const char *str);
void MEA_OS_formatNumVal(char *buf, MEA_uint64 n);
MEA_Uint32 MEA_OS_calc_shift_from_mask(MEA_Uint32 mask);
MEA_Uint32 MEA_OS_calc_Number_of_bits(MEA_Uint32 val);
MEA_Uint32 MEA_OS_calc_Xor_32_bits(MEA_Uint32 xor_param);
MEA_Uint32 MEA_OS_build_field_mask(MEA_Uint32 from_bit, MEA_Uint32 length);
MEA_Uint32 MEA_OS_calc_from_size_to_width(MEA_Uint32 value);

MEA_Status MEA_OS_insert_value(MEA_Uint32 start_bit,
                                MEA_Uint32 width,
                                MEA_Uint32 valu,
                                void *array_tbl);

MEA_Uint32 MEA_OS_read_value(MEA_Uint32 start_bit,
                                          MEA_Uint32 width,
                                          void *array_tbl);
MEA_Uint32 MEA_OS_atoiNum(char *str);
MEA_uint64 MEA_OS_atoiNum64(char *str);

MEA_uint64 MEA_OS_htonll64(MEA_uint64 n);


MEA_Uint32   MEA_OS_atohex(char *str);
MEA_Status   MEA_OS_atohex_MAC(char *str,MEA_MacAddr *pVal);
void MEA_OS_ipv6_to_buf(MEA_Uint32 *Ipv6, unsigned char *buf);
void MEA_OS_MacToStr(MEA_MacAddr * MAC_Addr, char *mac_str);

MEA_Status MEA_OS_sleep(MEA_Uint32 tv_sec, MEA_Uint32 tv_nsec);

MEA_Status MEA_OS_findAndReplaceSubstr(char *str ,
                                       char *findSubstr, 
                                       char *replaceSubstr); 



MEA_Status MEA_OS_PhyRead(MEA_Uint8    phy_number_i,
                           MEA_Uint8   reg_number_i,
                           MEA_Uint32 *reg_value_o);

MEA_Status MEA_OS_PhyWrite(MEA_Uint8   phy_number_i,
                           MEA_Uint8   reg_number_i,
                           MEA_Uint32  reg_value_i);

MEA_Status MEA_Tdn_OS_PhyRead(MEA_Uint8    phy_number_i,
                              MEA_Uint8   reg_number_i,
                              MEA_Uint32 *reg_value_o);

MEA_Status MEA_Tdn_OS_PhyWrite(MEA_Uint8  phy_number_i,
                               MEA_Uint8  reg_number_i,
                               MEA_Uint16 reg_value_i);

MEA_Uint32 MEA_OS_inet_addr(MEA_Int8 *src);

MEA_Int8 * MEA_OS_inet_ntoa(MEA_Uint32 src, MEA_Int8* dst, MEA_Uint8 size);

typedef enum {
	MEA_JUMBO_TYPE_2K = 0,
	MEA_JUMBO_TYPE_4K = 1,
	MEA_JUMBO_TYPE_8K = 2,
	MEA_JUMBO_TYPE_16K = 3,	/*not support yet */
	MEA_JUMBO_TYPE_LAST
} MEA_JumboType_te;

typedef enum {
    MEA_DRV_PORT_INT_STR_PORT_TYPE = 0,
    MEA_DRV_PORT_INT_STR_INTFACE_TYPE = 1,
    MEA_DRV_PORT_INT_STR_INTFACE_RATE = 2,
    MEA_DRV_PORT_INT_STR_LAST
}mea_drv_portInterface_str_t;


extern MEA_JumboType_te MEA_JUMBO_TYPE_DEF_VAL_ENET3000;
extern MEA_JumboType_te MEA_JUMBO_TYPE_DEF_VAL_ENET4000;

#define MEA_JUMBO_SIZE_DECREASE  (((MEA_INGRESS_PORT_MTU_SUPPORT == MEA_TRUE) || \
                                  (MEA_QUEUE_MTU_SUPPORT_TYPE == MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE)) ? 384 : 400)

#define MEA_JUMBO_TYPE_2K_MAX_MTU  ( 2048 - MEA_JUMBO_SIZE_DECREASE)
#define MEA_JUMBO_TYPE_4K_MAX_MTU  ( 4096 - MEA_JUMBO_SIZE_DECREASE)
#define MEA_JUMBO_TYPE_8K_MAX_MTU  ( 8192 - MEA_JUMBO_SIZE_DECREASE)
#define MEA_JUMBO_TYPE_16K_MAX_MTU (16384 - MEA_JUMBO_SIZE_DECREASE)

MEA_JumboType_te MEA_Platform_GetJumboType(void);
MEA_Status MEA_Platform_GetNumOfDescriptors(MEA_Uint32 *desc_val_o, MEA_Uint32 *data_Buff_val_o);
MEA_Bool MEA_Platform_IngressMTU_Support(void);
MEA_Mtu_t MEA_Platform_GetMaxMTU(void);
MEA_uint64 MEA_Platform_GetDeviceSysClk(void);

MEA_Uint32 MEA_Platform_GetShaperFastGnPort(void);
MEA_Uint32 MEA_Platform_GetShaperSlowGnPort(void);

MEA_Uint32 MEA_Platform_GetShaperFastGnCluster(void);
MEA_Uint32 MEA_Platform_GetShaperSlowGnCluster(void);

MEA_Uint32 MEA_Platform_GetShaperFastGnPriQueue(void);
MEA_Uint32 MEA_Platform_GetShaperSlowGnPriQueue(void);

MEA_Uint32 MEA_Platform_GetShaperSlowGnPort_TICKs(void);
MEA_Uint32 MEA_Platform_GetShaperSlowGnCluster_TICKs(void);
MEA_Uint32 MEA_Platform_GetShaperSlowGnPriQueue_TICKs(void);

MEA_Uint32 MEA_Platform_GetShaperFastGnPort_TICKs(void);
MEA_Uint32 MEA_Platform_GetShaperFastGnCluster_TICKs(void);
MEA_Uint32 MEA_Platform_GetShaperFastGnPriQueue_TICKs(void);


MEA_Bool MEA_Platform_IsServiceRangeSupport(void);
MEA_Bool MEA_Platform_FWD_CFM_OAM_ME_LVL_Support(void);
MEA_Bool MEA_Platform_IsPauseSupport(void);
MEA_Bool MEA_Platform_IsRx_PauseSupport(void);
MEA_Bool MEA_Platform_IsDesc_external(void);
MEA_Bool  MEA_Platform_Ispre_sched_enable(void);



typedef enum {
    MEA_SHAPER_SUPPORT_PORT_TYPE      = 0,
    MEA_SHAPER_SUPPORT_CLUSTER_TYPE   = 1,
    MEA_SHAPER_SUPPORT_PRIQUEUE_TYPE  = 2,
    MEA_SHAPER_SUPPORT_LAST_TYPE
} MEA_ShaperSupportType_te ;

typedef MEA_ShaperSupportType_te ENET_ShaperSupportType_te;


MEA_ShaperSupportType_te  MEA_Platform_GetShaperSupportType(MEA_ShaperSupportType_te type);

ENET_ShaperId_t     MEA_Platform_GetMaxNumOfShaperId(MEA_Unit_t unit_i,MEA_ShaperSupportType_te type_i);

typedef enum {
   MEA_QUEUE_MTU_SUPPORT_CLUSTER_TYPE,
   MEA_QUEUE_MTU_SUPPORT_PRIQUEUE_TYPE
} MEA_QueueMtuSupportType_te;

MEA_QueueMtuSupportType_te         MEA_Platform_MtuPriQueue_Support(void);

typedef enum {
	MEA_ACTION_TYPE_FWD=0,
	MEA_ACTION_TYPE_SERVICE=1 ,
	MEA_ACTION_TYPE_FILTER=2 ,
    MEA_ACTION_TYPE_LXCP=3 ,
    MEA_ACTION_TYPE_HPM =4,
    MEA_ACTION_TYPE_SERVICE_EXTERNAL=5,
    MEA_ACTION_TYPE_ACL5 = 6,
	MEA_ACTION_TYPE_LAST
} mea_action_type_te;

#define MEA_ACTION_TYPE_DEFAULT MEA_ACTION_TYPE_FWD

MEA_uint64 MEA_systemClock_calc(MEA_Unit_t  unit_i, MEA_uint64 sysClk);

MEA_Uint16 MEA_Platform_GetMaxNumOfPort(void);
MEA_Uint16 MEA_Platform_GetMaxNumOfPort_BIT(void);

MEA_Uint16 MEA_Platform_GetMaxNumOf_EgressPort(void);
MEA_Service_t MEA_Platform_GetMaxNumOfServices_SW_EX(void);
MEA_ACL5_t MEA_Platform_GetMaxNumOfACL5_SW_EX(void);

MEA_Service_t      MEA_Platform_GetMaxNumOfServices(void);
MEA_FilterMask_t   MEA_Platform_GetMaxNumOfFilterMasks(void);
MEA_Filter_t       MEA_Platform_GetMaxNumOfFilters(void);
MEA_Bool           MEA_Platform_Get_ACL_Support(void);
MEA_Bool  MEA_Platform_Get_ACL_hierarchical_Support(void);
MEA_Bool  MEA_Platform_Get_Vxlan_Support(void);
MEA_Filter_t  MEA_Platform_Get_NumOfACL_contex(void);
MEA_Uint16  MEA_Platform_Get_instance_fifo_1588(void);
MEA_Bool  MEA_Platform_Get_ADM_Support(void);
MEA_Bool  MEA_Platform_Get_segment_switch_support(void);

MEA_Uint32 MEA_Platform_Get_crypto_db_prof_width(void);
MEA_Uint32 MEA_Platform_Get_crypto_db_numOf_profile(void);


MEA_Bool  MEA_Platform_Get_type_DB_DDR(void);

MEA_Uint32  MEA_Platform_Get_max_numof_descriptor(void);
MEA_Uint32  MEA_Platform_Get_max_numof_DataBuffer(void);
MEA_Uint32  MEA_Platform_Get_max_NumOf_FWD_ENB_DA(void);
MEA_Uint32  MEA_Platform_Get_Calculate_descriptor(void);

MEA_Uint32  MEA_Platform_Get_ip_reassembly_session_id_key_width(void);
MEA_Uint32  MEA_Platform_Get_ip_reassembly_session_id_width(void);
MEA_Uint32  MEA_Platform_Get_ip_reassembly_numof_session_id(void);



MEA_Action_t       MEA_Platform_GetMaxNumOfActionsByType(mea_action_type_te type_i);
MEA_Action_t       MEA_Platform_GetMaxNumOfActions(void);
MEA_PmId_t         MEA_Platform_GetMaxNumOfPmId(void);
MEA_TmId_t         MEA_Platform_GetMaxNumOfTmId(void);

MEA_Bool           MEA_Platform_Get_MC_Editor_new_mode_Support(void);
MEA_Uint16         MEA_Platform_Get_NumOfMC_EHPs(void);
MEA_Bool           MEA_Platform_Get_ip_fragmentation_Support(void);

MEA_Bool  MEA_Platform_Get_new_dataBase_Support(void);
MEA_Uint32 MEA_Platform_Get_new_dataBase_width(void);

MEA_Uint32        MEA_Platform_Get_PolicerGranularity(void);
MEA_Uint32        MEA_Platform_Get_IngressFlow_PolicerGranularity(void);

MEA_Editing_t      MEA_Platform_GetMaxNumOfEdId(void);
MEA_Editing_t      MEA_Platform_GetMaxNumOfEHP_Da(void);
MEA_Editing_t      MEA_Platform_GetMaxNumOfEHP_SaLss(void);
MEA_Editing_t      MEA_Platform_GetMaxNumOfEHP_ProfileId(void);
MEA_Editing_t      MEA_Platform_GetMaxNumOfEHP_Profile_Stamping(void);
MEA_Editing_t      MEA_Platform_GetMaxNumOfEHP_Profile_SaDss(void);
MEA_Editing_t      MEA_Platform_GetMaxNumOfEHP_Profile_SaMss(void);
MEA_Editing_t      MEA_Platform_GetMaxNumOfEHP_Profile_EtherType(void);
MEA_Policer_prof_t MEA_Platform_GetMaxNumOfPolicerProfile(void);
MEA_Limiter_t      MEA_Platform_GetMaxNumOfLimiters(void);
MEA_LxCp_t         MEA_Platform_GetMaxNumOfLxCps(void);
MEA_LxcpAction_t   MEA_Platform_GetMaxNumOfLxCpProtocolActions(void);
MEA_LxcpAction_t   MEA_Platform_Get_DeviceInfo_NumOfLxcpAction(MEA_Uint32 hwUnit_i);

MEA_Service_t MEA_Platform_GetMaxNumOfExternalService(void);
MEA_Bool MEA_Platform_Get_ExternalService_Support(void);

MEA_Service_t MEA_Platform_SERVICE_ACTION_LENGTH_calculate(MEA_Uint32 hwUnit_i);
MEA_Action_t  MEA_Platform_FWD_ACTION_LENGTH_calculate(MEA_Uint32 hwUnit_i);
MEA_Action_t  MEA_Platform_FWD_ACTION_START_calculate(MEA_Uint32 hwUnit_i);


MEA_FlowCoSMappingProfile_Id_t     MEA_Platform_GetMaxNumOfFlowCoSMappingProfiles(void);
MEA_FlowMarkingMappingProfile_Id_t MEA_Platform_GetMaxNumOfFlowMarkingMappingProfiles(void);

MEA_Bool MEA_Platform_IsSupportFlowCoSMappingProfile(void);
MEA_Bool MEA_Platform_IsSupportFlowMarkingMappingProfile(void);
MEA_EditingMappingProfile_Id_t MEA_Platform_GetMaxNumOfEditingMappingProfile(void);
MEA_Uint32   MEA_Platform_GetMaxNumOfPreSchedulerEntries(void);
MEA_Global_ClusterWfq_te  MEA_Platform_GetGlobalClusterModeDefVal(void);
MEA_Global_PriQWfq_te     MEA_Platform_GetGlobalPriQModeDefVal(void);
MEA_Bool                  MEA_Platform_EnhancePmCounterSupport(void);

MEA_WredProfileId_t       MEA_Platform_Get_MaxWredProfiles(void);
MEA_Bool                  MEA_Platform_Get_WredProfiles_SUPPORT(void);
MEA_AcmMode_t             MEA_Platform_Get_MaxWredACM(void);
MEA_Bool                  MEA_Platform_GetGlobalCFM_OAM_Valid_DefVal(void);

MEA_Bool         MEA_Platform_Get_PacketAnalyzer_SupportType1(void);
MEA_Bool         MEA_Platform_Get_PacketAnalyzer_SupportType2(void);

MEA_Bool         MEA_Platform_Get_PacketGen_Support(void);
MEA_Bool         MEA_Platform_Get_PacketGen_Support_Type2(void);
MEA_PacketGen_t  MEA_Platform_Get_PacketGen_MaxNumOfStreams(void);
MEA_PacketGen_t  MEA_Platform_Get_PacketGen_MaxNumOfStreams_Type1(void);
MEA_PacketGen_t  MEA_Platform_Get_PacketGen_MaxNumOfStreams_Type2(void); 

MEA_PacketGen_t  MEA_Platform_Get_PacketGen_MaxNumOf_Profile(void);

MEA_PacketGen_t  MEA_Platform_Get_Analyzer_MaxNumOfStreamsType1(void);
MEA_PacketGen_t  MEA_Platform_Get_Analyzer_MaxNumOfStreamsType2(void);
MEA_Uint32 MEA_Platform_Get_Analyzer_MaxNumCC_GROUP(void);

ENET_QueueId_t   MEA_Platform_Get_MaxNumOfClusters(void);

ENET_QueueId_t MEA_Platform_Get_MaxNumOfClusters_ClustrtToPort(void);

MEA_Bool MEA_Platform_Get_bmf_ccm_support(void);
MEA_Bool MEA_Platform_Get_ingress_count_support(void);
MEA_Bool  MEA_Platform_IsrmonVP_support(void);
MEA_Bool MEA_Platform_Get_HeaderCompress_support(void);
MEA_Bool MEA_Platform_Get_HeaderDeCompress_support(void);

MEA_Bool MEA_Platform_Get_IsActionMtuSupport(void);
MEA_Editing_t MEA_Platform_GetMaxNumOf_AC_MTU(void);
MEA_Bool MEA_Platform_Get_IsNewAllocSupport(void);

MEA_Bool MEA_Platform_Get_dest_vl_support(void);
MEA_Bool MEA_Platform_Get_tdm_act_disable(void);
MEA_Bool MEA_Platform_Get_wbrg_support(void);
MEA_Bool MEA_Platform_Get_Swap_machine_Support(void);

MEA_Uint32 MEA_Platform_Get_Max_point_2_multipoint(void);

MEA_Bool MEA_Platform_Get_ShaperPortType_support(void);
MEA_Bool MEA_Platform_Get_ShaperClusterType_support(void);
MEA_Bool MEA_Platform_Get_ShaperPriQueueType_support(void);

MEA_Bool MEA_Platform_Get_Shaper_xCIR_support(void);

MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_Profile2Id(void);
MEA_Editing_t MEA_Platform_GetMaxNumOfEHP_Profile2_Lm(void);

MEA_Bool MEA_Platform_Get_L2CP_support(void);
MEA_Bool MEA_Platform_Get_ingress_port_count_support(void);

MEA_Bool MEA_Platform_Get_my_mac_support(void);

MEA_Bool MEA_Platform_Get_fbmf_atm_over_pos_support(void);



MEA_Uint32 MEA_Platform_Get_fragment_Edit_num_of_group(void);
MEA_Uint32 MEA_Platform_Get_fragment_Edit_num_of_port(void);
MEA_Uint32 MEA_Platform_Get_fragment_Edit_num_of_session(void);

MEA_Bool MEA_Platform_Get_fragment_Edit_Support(void);
MEA_Bool MEA_Platform_Get_pw_control_Edit_Support(void);
MEA_Bool MEA_Platform_Get_forwarder_State_show(void);
MEA_Bool MEA_Platform_Get_large_BM_ind_address(void);

MEA_Uint32  MEA_Platform_Get_fragment_vsp_num_of_group(void);
MEA_Uint32  MEA_Platform_Get_fragment_vsp_num_of_port(void);
MEA_Uint32  MEA_Platform_Get_fragment_vsp_num_of_session(void);
MEA_Bool  MEA_Platform_Get_fragment_vsp_Support(void);

MEA_Bool  MEA_Platform_Get_fragment_Global_Support(void);



MEA_Bool  MEA_Platform_Get_Blocking_Class_Support(void);

MEA_Bool   MEA_Platform_IsAcmSupport(void);
MEA_AcmMode_t MEA_Platform_GetMaxNumOfAcmModes(void);
MEA_AcmMode_t MEA_Platform_GetNumOfAcmModes(void);

MEA_Bool  MEA_Platform_Get_Forwader_supportDSE(void);
MEA_Bool  MEA_Platform_Get_clusterTo_multiport_support(void);
MEA_Bool  MEA_Platform_Get_IsLimiterSupport(void);

MEA_Bool  MEA_Platform_Get_Is_Ingress_flow_PolicerSupport(void);
MEA_Uint32  MEA_Platform_Get_Ingress_flow_Policer_profile(void);

MEA_Bool  MEA_Platform_Get_IsCamATMExistSupport(void);
MEA_Bool  MEA_Platform_Get_Dual_Latency_support(void);
MEA_Bool  MEA_Platform_Get_Fragment_Bonding_support(void);
MEA_Uint16 MEA_Platform_GetMaxLQ(void);
MEA_Bool  MEA_Platform_Get_CPE_Bonding(void);
MEA_Uint16 MEA_Platform_Get_Service_NumOf_groupHash(void);
MEA_Uint16 MEA_Platform_Get_Service_NumOf_groupHash_EVC(void);
MEA_Bool MEA_Platform_Get_Service_EVC_Classifier_Support(void);
MEA_Uint16 MEA_Platform_Get_Service_EVC_classifier_key_width(void);
MEA_Bool  MEA_Platform_Get_new_parser_Support(void);
MEA_Bool MEA_Platform_Get_afdx_Support(void);

MEA_Bool  MEA_Platform_Get_Port_new_setting(void);
MEA_Bool  MEA_Platform_Get_mtu_pri_queue_reduce(void);

MEA_Bool MEA_Platform_Get_new_parser_reduce(void);
MEA_Bool MEA_Platform_Get_BFD_support(void);
MEA_Bool MEA_Platform_Get_Ip_sec_support(void);
MEA_Bool MEA_Platform_Get_NVGRE_enable(void);
MEA_Bool MEA_Platform_Get_L2TP_support(void);
MEA_Bool MEA_Platform_Get_GRE_support(void);


MEA_Bool  MEA_Platform_Get_ProtocolMapping_Support(void);
MEA_Bool  MEA_Platform_Get_cls_large_Support(void);
MEA_Bool  MEA_Platform_Get_1588_Support(void);
MEA_Bool  MEA_Platform_Get_MAC_LB_Support(void);

MEA_Bool  MEA_Platform_Get_SerdesReset_Support(void);
MEA_Bool MEA_Platform_Get_Protect_1p1_Support(void);

MEA_Uint8  MEA_Platform_Get_Xpermission_num_clusterInGroup(void);
MEA_Uint32 MEA_Platform_Get_Xpermission_num_clusterInGroup_val(void);
MEA_Uint32  MEA_Platform_Get_num_ofMSTP(void);
MEA_Bool MEA_Platform_Get_mstp_support(void);
MEA_Bool MEA_Platform_Get_rstp_support(void);
MEA_Bool MEA_Platform_Get_ECN_support(void);
MEA_Uint16 MEA_Platform_Get_BM_bus_width(void);
MEA_Bool MEA_Platform_Get_ingressMacfifo_small(void);
MEA_Bool MEA_Platform_Get_Isqueue_mc_mqs_Support(void);
MEA_Bool MEA_Platform_Get_Isqueue_count_Support(void);
MEA_Bool MEA_Platform_Get_Isqueue_adminOn_Support(void);
MEA_Bool MEA_Platform_Get_IsLAG_Support(void);
MEA_Uint16 MEA_Platform_Get_NumLAG_Profile(void);

MEA_Bool MEA_Platform_Get_ts_measuremant_support(void);

MEA_Bool MEA_Platform_Get_Parser_Support_IPV6(void);

MEA_Bool MEA_Platform_Get_extend_default_sid_Support(void);
MEA_Uint16 MEA_Platform_Get_ParsernumofBit(void);
MEA_Uint16 MEA_Platform_Get_Num_of_Slice(void);

MEA_Bool MEA_Platform_Get_AlloC_AllocR(void);
MEA_Bool MEA_Platform_Get_Utopia_Support(void);
MEA_Bool MEA_Platform_Get_st_contx_mac_support(void);
MEA_Bool MEA_Platform_Get_fwd_filter_support(void);
MEA_Bool MEA_Platform_Get_IsPolicerSupport(void);
MEA_Bool MEA_Platform_Get_IslowslowSupportSupport(void);
MEA_Uint32 MEA_Platform_Get_CIR_Token_Bits(void);
MEA_Uint32 MEA_Platform_Get_CBS_bucket_Bits(void);
MEA_Bool MEA_Platform_Get_fragment_9991_Support(void);
MEA_Bool MEA_Platform_Get_autoneg_support(void);
MEA_Bool MEA_Platform_Get_GATEWAY_EN_support(void);
MEA_Uint32 MEA_Platform_Get_numof_SER_TEID(void);

MEA_Bool MEA_Platform_Get_standard_bonding_Support(void);
MEA_Uint16 MEA_Platform_Get_standard_bonding_num_of_groupTx(void);
MEA_Uint16 MEA_Platform_Get_standard_bonding_num_of_groupRx(void);



MEA_Bool MEA_Platform_Get_IS_EPC(MEA_Uint16 type);


MEA_Bool  MEA_Platform_Get_fwd_LPM_Support(void);
MEA_Uint32  MEA_Platform_Get_fwd_LPM_LPM_Ipv4_size(void);
MEA_Uint32  MEA_Platform_Get_fwd_LPM_LPM_Ipv6_size(void);


MEA_Bool MEA_Platform_Get_QueueStatistics_Support(void);

/************************************************************************/
/* Egress_classifier                                                    */
/************************************************************************/
MEA_Bool MEA_Platform_Get_Egress_Filter_exist(void);
MEA_Bool MEA_Platform_Get_Egress_classifier_exist(void);
MEA_Bool MEA_Platform_Get_Egress_parser_exist(void);
MEA_Bool MEA_Platform_Get_Egress_classifierRang_exist(void);
MEA_Uint16 MEA_Platform_Get_Egress_classifier_number_contex(void);
MEA_Uint16 MEA_Platform_Get_Egress_classifier_Key_width(void);
MEA_Uint16 MEA_Platform_Get_Egress_classifier_hash_groups(void);
MEA_Uint16 MEA_Platform_Get_Egress_classifier_num_of_rangPort(void);
MEA_Uint16 MEA_Platform_Get_Egress_classifier_of_ranges(void);
MEA_Uint16 MEA_Platform_Get_Egress_classifier_number_hashflow(void);




/************************************************************************/
/* TDM                                                                  */
/************************************************************************/
MEA_Bool MEA_Platform_Get_TDM_Support(void);
MEA_Bool MEA_Platform_Get_TDM_Interface_X_Support(MEA_Uint8 interfaceId);
MEA_Uint16 MEA_Platform_Get_TDM_Interface_num0fspe(MEA_Uint8 interfaceId);
MEA_Uint16 MEA_Platform_Get_TDM_Interface_Type(MEA_Uint8 interfaceId);
MEA_TdmCesId_t MEA_Platform_Get_TDM_num_of_CES(void);
MEA_Bool MEA_Platform_Get_TDM_mode_Satop(void);


#define MEA_TDM_IS_SET_GROUP_EVETS(Event_group,index_i) \
    ((((MEA_Uint32*)(&((Event_group)->Group0))))[(index_i)/32] &  ( (0x00000001 << ((index_i)%32))))


//////////////////////////////////////////////////////////////////////////

/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Bool MEA_Platform_Get_Hc_Classifier_Support(void);


MEA_Bool MEA_Platform_Get_Hc_Classifier_Rang_Support(void);
MEA_Bool MEA_Platform_Get_Hc_rmon_exist(void);

MEA_Uint16 MEA_Platform_Get_Hc_Classifier_Num_of_hcid(void);

MEA_Uint16 MEA_Platform_Get_Hc_number_of_hash_groups(void);

MEA_Uint16 MEA_Platform_Get_Hc_Classifier_Hc_classifier_key_width(void);



MEA_Bool MEA_Platform_Get_PTP1588_Support(void);

MEA_Bool MEA_Platform_Get_Isvpls_access_Support(void);
MEA_Bool MEA_Platform_Get_Isvpls_chactristic_Support(void);


MEA_Uint16 MEA_OS_crcsum(const unsigned char* message, 
                         unsigned long length,
                         unsigned short crc);

MEA_Bool MEA_OS_crcverify(const unsigned char* message, unsigned long length);
void MEA_OS_crcappend(unsigned char* message, unsigned long length);



/************************************************************************/
/* HPM                                                                     */
/************************************************************************/
MEA_Bool MEA_Platform_Get_HPM_Support(void);
MEA_Bool MEA_Platform_Get_HPM_newSupport(void);
MEA_Uint32 MEA_Platform_Get_HPM_numof_UE_PDN(void);
MEA_Uint32 MEA_Platform_Get_HPM_numof_Profile(void);
MEA_Uint32 MEA_Platform_Get_HPM_numof_Mask_Profile(void);


MEA_Uint8 MEA_Platform_Get_HPM_numof_Rules(void);
MEA_Bool MEA_Platform_Get_HPM_Type_Support(void);


MEA_Uint32 MEA_Platform_Get_HPM_numof_Act(void);
MEA_Uint32 MEA_Platform_Get_HPM_BASE(void);

/************************************************************************/
/*   SFLOW                                                              */
/************************************************************************/
MEA_Bool MEA_Platform_Get_SFLOW_Support(void);
MEA_Uint32 MEA_Platform_Get_SFLOW_numof_count(void);

/************************************************************************/
/* ACL5                                                                 */
/************************************************************************/

MEA_Bool MEA_Platform_Get_ACL5_Support(void);
MEA_Bool MEA_Platform_Get_ACL5_ExternalSupport(void);
MEA_Bool MEA_Platform_Get_ACL5_InternalSupport(void);
MEA_Uint32 MEA_Platform_Get_ACL5_Ipmask_size(void);
MEA_Uint32 MEA_Platform_Get_ACL5_keymask_size(void);
MEA_Uint32 MEA_Platform_Get_ACL5_rangeProf_size(void);
MEA_Uint32 MEA_Platform_Get_ACL5_rangeBlock_size(void);

/************************************************************************/
/* CRC32                                                                */
/************************************************************************/
#define CRC32C_SWAP(crc32c_value)				\
    (((crc32c_value & 0xff000000) >> 24)	|	\
    ((crc32c_value & 0x00ff0000) >>  8)	|	\
    ((crc32c_value & 0x0000ff00) <<  8)	|	\
    ((crc32c_value & 0x000000ff) << 24))


#define MEA_CRC32_CCITT_SEED    0xFFFFFFFF
MEA_Uint32 MEA_crc32_ccitt(const MEA_Uint8 *buf, MEA_Uint32 len);
MEA_Uint32 MEA_crc32_ccitt_seed(const MEA_Uint8 *buf, MEA_Uint32 len, MEA_Uint32 seed);


MEA_Status mea_get_local_linuxaddr(const char *ifname, MEA_MacAddr *mac);
MEA_Uint32 msb32_idx(MEA_Uint32  n);
MEA_Uint16 mea_IP_Checksum(const MEA_Uint16* buf, unsigned int nbytes);
/************************************************************************/
/*                                                                      */
/************************************************************************/
MEA_Status MEA_OS_getIpAndMask(const char* ifName_i,
                               unsigned long* ip_o,
                               unsigned long* mask_o);

MEA_Uint32 MEA_OS_NumOfu32bitSet(MEA_Uint32  value);
#ifdef MEA_OS_LINUX
MEA_Status MEA_OS_timeval_subtract(struct timeval *result, struct timeval *tvEnd, struct timeval *tvBegin);

void MEA_OS_timeval_print(struct timeval *tv);
#endif


#ifdef __cplusplus
 }
#endif 

#endif


