/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef _ENET_PLATFORM_H_
#define	_ENET_PLATFORM_H_

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Conver main defines from MEA to ENET                          */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#include "MEA_platform.h"
#ifdef __cplusplus
extern "C" {
#endif 
#if defined(MEA_IF_BASE_ADDR) && !defined(ENET_IF_BASE_ADDR)
#define ENET_IF_BASE_ADDR   MEA_IF_BASE_ADDR
#endif

#if defined(MEA_BM_BASE_ADDR) && !defined(ENET_BM_BASE_ADDR)
#define ENET_BM_BASE_ADDR   MEA_BM_BASE_ADDR
#endif

#if defined(MEA_CPLD_BASE_ADDR) && !defined(ENET_CPLD_BASE_ADDR)
#define ENET_CPLD_BASE_ADDR MEA_CPLD_BASE_ADDR
#endif


#if defined(MEA_DATA_DDR_BASE_ADDR) && !defined(ENET_DATA_DDR_BASE_ADDR)
#define ENET_DATA_DDR_BASE_ADDR MEA_DATA_DDR_BASE_ADDR
#endif

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Special defines that control all platform                     */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#if defined(MEA_COUNTER_64_SUPPORT) && !defined(ENET_COUNTER_64_SUPPORT)
#define ENET_COUNTER_64_SUPPORT
#endif

#if defined(MEA_SW_CHECK_INPUT_PARAMETERS) && !defined(ENET_SW_CHECK_INPUT_PARAMETERS)
#define ENET_SW_CHECK_INPUT_PARAMETERS
#endif

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Include types and OS support definitions                      */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#include "ENET_platform_types.h"
#include "MEA_platform_os.h"

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                   Default values                                                */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/

#define ENET_PLAT_MAX_NAME_SIZE 32

#define ENET_PLAT_CHANNEL_NET_TAGS_MAX_NUM				1
#define ENET_PLAT_QUEUE_NUM_OF_PRI_Q					8
#define ENET_PLAT_POS2_ETH_CLUSTER_RESOLUTION            1
#define ENET_PLAT_POS2_CLUSTER_RESOLUTION                4
#define ENET_PLAT_UTOPIA_CLUSTER_RESOLUTION              4
#define ENET_PLAT_PORT_72_DEFAULT_CLUSTER				72
#define ENET_PLAT_PORT_120_124_DEFAULT_CLUSTER_START    120
#define ENET_PLAT_PORT_125_DEFAULT_CLUSTER				125
#define ENET_PLAT_PORT_126_DEFAULT_CLUSTER				126
#define ENET_PLAT_PORT_127_DEFAULT_CLUSTER				127

#define ENET_PLAT_PORT_119_DEFAULT_CLUSTER				119
#define ENET_PLAT_PORT_118_DEFAULT_CLUSTER				118
#define ENET_PLAT_PORT_110_DEFAULT_CLUSTER				110
#define ENET_PLAT_PORT_111_DEFAULT_CLUSTER				111

#define ENET_PLAT_PORT_24_TLS_DEFAULT_CLUSTER            24
#define ENET_PLAT_PORT_24_TLS_DEFAULT_INTERNAL_CLUSTER	24
#define ENET_PLAT_PORT_127_DEFAULT_INTERNAL_CLUSTER		63
#define ENET_PLAT_PORT_126_DEFAULT_INTERNAL_CLUSTER     62 
#define ENET_PLAT_PORT_125_DEFAULT_INTERNAL_CLUSTER		((MEA_DRV_XPER_NUM_CLUSTER_GROUP == 0) ? 61 : 125)

#define ENET_PLAT_PORT_119_DEFAULT_INTERNAL_CLUSTER     59
#define ENET_PLAT_PORT_118_DEFAULT_INTERNAL_CLUSTER     58

#define ENET_PLAT_PORT_111_DEFAULT_INTERNAL_CLUSTER     51
#define ENET_PLAT_PORT_110_DEFAULT_INTERNAL_CLUSTER     50


#define ENET_PLAT_MC_DEFAULT_CLUSTER                    45

#define ENET_PLAT_ENCAPSULATION_NUM_OF_NET_TAG_INFO		3

#define ENET_PLAT_VIRTUAL_PORT_NUM_OF_NET_TAG_INFO      ENET_PLAT_ENCAPSULATION_NUM_OF_NET_TAG_INFO

#define ENET_PLAT_HEADER_MAX_NUM_OF_NEXT_HEADERS		1

#define ENET_PLAT_KEY_PROFILE_MAX_NUM_OF_FIELDS     	1

#define ENET_PLAT_EDITING_PROFILE_MAX_NUM_OF_COMMANDS 	1

#define ENET_PLAT_DECODER_NUM_OF_NET_TAGS               1

#define ENET_PLAT_DECODER_NUM_OF_HEADERS                1

#define ENET_PLAT_EDITING_ACTION_DATA_MAX_LEN		    1

#define ENET_PLAT_MAX_PRIORITY_MASK_VALUE 7

#define ENET_PLAT_GROUP_MAX_NAME_SIZE                   ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_GROUP_ELEMENT_MAX_NAME_SIZE           ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_PORT_MAX_NAME_SIZE					ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_PORT_GROUP_MAX_NAME_SIZE				ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_QUEUE_MAX_NAME_SIZE					ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_ENCAPSULATION_MAX_NAME_SIZE			ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_CHANNEL_MAX_NAME_SIZE					ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_VIRTUAL_PORT_MAX_NAME_SIZE			ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_FORWARDER_MAX_NAME_SIZE				ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_MAPPING_MAX_NAME_SIZE					ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_CONTROL_PROTOCOLS_MAX_NAME_SIZE		ENET_PLAT_MAX_NAME_SIZE
#define ENET_PLAT_FILTER_MAX_NAME_SIZE					ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_HEADER_MAX_NAME_SIZE					ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_KEY_PROFILE_MAX_NAME_SIZE				ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_EDITING_COMMAND_MAX_NAME_SIZE			ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_EDITING_PROFILE_MAX_NAME_SIZE			ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_FIELD_MAX_NAME_SIZE					ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_CLASSIFIER_KEY_MAX_NAME_SIZE			ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_PRI_QUEUE_MAX_NAME_SIZE				ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_CLASSIFIER_ENTRY_MAX_NAME_SIZE		ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_LOGICAL_EGRESS_QUEUE_MAX_NAME_SIZE    ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_SHAPER_PROFILE_MAX_NAME_SIZE          ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_POLICER_PROFILE_MAX_NAME_SIZE         ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_FORWARDER_TBL_MAX_NAME_SIZE           ENET_PLAT_MAX_NAME_SIZE

#define ENET_PLAT_FORWARDER_ENTRY_MAX_NAME_SIZE         ENET_PLAT_MAX_NAME_SIZE

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                                                                                 */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/
#define ENET_PLAT_MAX_UNIT_ID 0

#define ENET_PLAT_MAX_GROUP_ID 1023

#define ENET_PLAT_MAX_PORT_ID 127

#define ENET_PLAT_MAX_QUEUE_ID 127

#define ENET_PLAT_MAX_ENCAPSULATION_ID 1023

#define ENET_PLAT_ENCAPSULATION_NUM_OF_FIELDS 32

#define ENET_PLAT_PACKET_SIZE_MAX_VALUE(traffic_type) ((ENET_Uint32)1820)

#define ENET_PLAT_PACKET_SIZE_MIN_VALUE(traffic_type) \
   (((traffic_type) == ENET_PORT_TRAFFIC_TYPE_PACKET) ? (ENET_Uint32)64 : (ENET_Uint32)48)

#define ENET_PLAT_GENERATE_NEW_ID MEA_PLAT_GENERATE_NEW_ID

#define ENET_PLAT_GENERATE_NEW_NAME ""

/*---------------------------------------------------------------------------------*/
/*                                                                                 */
/*                                                                                 */
/*                                                                                 */
/*                                                                                 */
/*                                                                                 */
/*---------------------------------------------------------------------------------*/

#define ENET_ASSERT_NULL(x,str) \
    if ((x) == NULL) { \
       MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR, \
                         "%s - %s == NULL\n", \
                          __FUNCTION__,str); \
       return ENET_ERROR; \
    }

#define ENET_ASSERT_BOOLEAN(x,str,id) \
	if (((x) != ENET_FALSE) && ((x) != ENET_TRUE)) { \
        ENET_OS_LOG_logMsg(ENET_OS_LOG_ERROR, \
 	                       "%s - %s (%d) is not valid value (id=%d) \n", \
				       	   __FUNCTION__,(str),(x),(id)); \
 	    return ENET_ERROR; \
    } else {}

#define ENET_ASSERT_RANGE(val,str,min,max,id) \
	if (((val) < (min)) || ((val) > (max))) { \
        ENET_OS_LOG_logMsg(ENET_OS_LOG_ERROR, \
 	                       "%s - %s (%d) is not in valid range (%d..%d) (id=%d) \n", \
				       	   __FUNCTION__,(str),(val),(min),(max),(id)); \
 	    return ENET_ERROR; \
    } else {}

#define ENET_ASSERT_ENABLE_DISABLE_STR(x) (x == ENET_TRUE)  ? "Enable" : "Disable"

#define ENET_ASSERT_UNIT_VALID(unit) \
	if (ENET_IsValid_Unit((unit),ENET_FALSE) != ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_GROUP_VALID(unit,group) \
	if (ENET_IsValid_Group((unit),(group),ENET_FALSE)!= ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_GROUP_ELEMENT_VALID(unit,group,group_element) \
	if (ENET_IsValid_GroupElement((unit),(group),(group_element),ENET_FALSE)!= ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}


#define ENET_ASSERT_PORT_VALID(unit,port) \
	if (MEA_API_Get_IsPortValid((port),MEA_PORTS_TYPE_EGRESS_TYPE,MEA_FALSE)!= MEA_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_PORT_VALID_INGRESS(unit,port) \
	if (MEA_API_Get_IsPortValid((port),MEA_PORTS_TYPE_INGRESS_TYPE,MEA_FALSE)!= MEA_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_VIRTUAL_PORT_VALID(unit,port) \
    if (MEA_API_Get_Is_VirtualPort_Valid((port),MEA_FALSE)!= MEA_TRUE) {\
    return ENET_ERROR; \
    } else {}


#define ENET_ASSERT_PORT_GROUP_VALID(unit,portGroup) \
	if (ENET_IsValid_PortGroup((unit),(portGroup),ENET_FALSE)!= ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_QUEUE_VALID(unit,queue) \
	if (ENET_IsValid_Queue((unit),(queue),ENET_FALSE)!= ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#if 0
 #define ENET_ASSERT_VIRTUAL_PORT_VALID(unit,virtual_port) \
 	if (ENET_IsValid_VirtualPort((unit),(virtual_port),ENET_FALSE)!= ENET_TRUE) {\
 	   return ENET_ERROR; \
 	} else {}
#endif

#define ENET_ASSERT_CHANNEL_VALID(unit,channel) \
	if (ENET_IsValid_Channel((unit),(channel),ENET_FALSE)!= ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_POLICER_VALID(unit,policer) \
	if (ENET_IsValid_Policer((unit),(policer),ENET_FALSE)!= ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_SHAPER_VALID(unit,shaper) \
	if (ENET_IsValid_Shaper((unit),(shaper),ENET_FALSE)!= ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_ENCAPSULATION_VALID(unit,encapsulation) \
	if (ENET_IsValid_Encapsulation((unit),(encapsulation),ENET_FALSE)!= ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_XPERMISSION_VALID(unit,xPermission) \
	if (enet_IsValid_xPermission((unit),(xPermission),ENET_FALSE)!= ENET_TRUE) {\
	   return ENET_ERROR; \
	} else {}

#define ENET_ASSERT_POLICER(unit,parent_id,str,policer) \
    ENET_ASSERT_BOOLEAN((policer)->enable,(str),(parent_id)); \
    if ((policer)->enable) { \
	   ENET_ASSERT_POLICER_VALID((unit),(policer)->id); \
	} else { }

#define ENET_ASSERT_SHAPER(unit,parent_id,str,shaper) \
    ENET_ASSERT_BOOLEAN((shaper)->enable,(str),(parent_id)); \
    if ((shaper)->enable) { \
	   ENET_ASSERT_POLICER_VALID((unit),(shaper)->id); \
	} else { }

#define ENET_ASSERT_VALID_NAME_LEN(name,size_to) \
if (MEA_OS_strlen(name) >= sizeof(size_to)) { \
		MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"At %s The name: ('%s') length (%d) is too long \n",__FUNCTION__,name,MEA_OS_strlen(name)); \
		return ENET_ERROR; \
    } else { }

#define ENET_PLAT_SOFTWARE_NOT_SUPPORT_YET(str) \
	ENET_OS_LOG_logMsg(ENET_OS_LOG_ERROR,\
	                   "%s - The software not support yet %s \n",\
	                   __FUNCTION__,(str));

#ifdef __cplusplus
 }
 #endif 
#endif				/* ENET_PLATFORM_H */
