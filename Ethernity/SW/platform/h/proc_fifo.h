#error "This file should not be included, include modules/include/linux/proc_fifo.h instead"
#ifndef _PROC_FIFO_H
#define _PROC_FIFO_H

#include <linux/module.h>


#define PROC_FIFO_IM_NAME "proc_fifo_im_data"

#define	PF_FIFO_STR "PF_FIFO"
#define	PF_FIFO_CIRCULAR_STR "PF_FIFO_CIRCULAR"
#define	PF_UNKNOWN_STR "PF_UNKNOWN"
#define PF_BUF_STATIC_STR "PF_BUF_STATIC"
#define PF_BUF_DYNAMIC_STR "PF_BUF_DYNAMIC"
#define PF_BUF_STD_STR "PF_BUF_STD"

#define PF_MAX_PRINTF 256

// #define PRINT_DEBUG  1

extern int proc_fifo_printk_flags;

#define PF_NAME_SIZE 32

typedef enum {
	PF_FIFO,
	PF_FIFO_CIRCULAR,
	PF_LAST
} proc_fifo_type_t;

typedef enum {
	PF_BUF_STATIC,
	PF_BUF_DYNAMIC,
	PF_BUF_STD,
	PF_BUF_LAST
} proc_fifo_buf_type_t;

typedef struct proc_fifo_t {
	struct list_head list;
	char name[PF_NAME_SIZE];
	proc_fifo_type_t pfType;
	proc_fifo_buf_type_t pfBufType;
	struct kfifo *kfifo;
	int flags;
	int size;
	int mem_used_max;
	wait_queue_head_t read_queue;
	struct proc_dir_entry *proc_dir_entry;
	spinlock_t lock;
	int (*write) (struct proc_fifo_t * pf, void *user_data,
		      const char *buffer, unsigned long count);
	int (*open) (struct proc_fifo_t * pf, void *user_data);
	void *user_data;
	int (*orig_open) (struct inode *, struct file *);
	int (*orig_release) (struct inode *, struct file *);
} proc_fifo_t;

/*
// The following struct is the inter module struct that contains the API used by kernel space applications
// for using this module. 
*/
typedef struct {
	/* 
	   // API : pf_im->alloc(name,pfType,size,parent)
	   //
	   // purpose : allocate a standard proc_fifo proc file
	   //
	   // where:
	   //           name            : Name of the proc file to be created
	   //           pfType  : Type of proc file (PF_FIFO | PF_FIFO_CIRCULAR)
	   //   size            : size of fifo buffer, must be a power of 2
	   //           parent  :  parent proc directory to locate this proc file
	   //
	   //   returns: pointer to proc_fifo_t or NULL if an error occures
	 */
	proc_fifo_t *(*alloc) (const char *name,
			       proc_fifo_type_t pfType,
			       int size, struct proc_dir_entry * parent);
	/*  
	   // API : pf_im->alloc_dynamic(name,pfType,size,parent,write,open,user_data)
	   //
	   // purpose : allocate a dynamic proc_fifo proc file
	   //
	   // where:
	   //           name                    : Name of the proc file to be created
	   //           pfType          : Type of proc file (PF_FIFO | PF_FIFO_CIRCULAR)
	   //   size                    : size of fifo buffer, must be a power of 2
	   //           parent          :  parent proc directory to locate this proc file
	   //           write                   : write callback routine, is called when this proc file is written to from user space   
	   //           open                    : open callback routine, is called when this proc file is opened from user space        
	   //           user_data       : user_data pointer passed to open & write routine for user private data in the kernel
	   //
	   //   returns: pointer to proc_fifo_t or NULL if an error occures
	 */
	proc_fifo_t *(*alloc_dynamic) (const char *name,
				       proc_fifo_type_t pfType,
				       int size,
				       struct proc_dir_entry * parent,
				       int (*write) (proc_fifo_t * pf,
						     void *user_data,
						     const char *buffer,
						     unsigned long count),
				       int (*open) (proc_fifo_t * pf,
						    void *user_data),
				       void *user_data);
	/*
	   // API : pf_im->alloc_static(name,pfType,pFifo,pBuf,size,parent)
	   //
	   // purpose : allocate a static proc_fifo proc file
	   //
	   // where:
	   //           name                    : Name of the proc file to be created
	   //           pfType          : Type of proc file (PF_FIFO | PF_FIFO_CIRCULAR)
	   //           pFifo                   : pointer to static memory location to use for proc_fifo_t struct
	   //   pBuf                    : pointer to static memory location to use for fifo buffer
	   //   size                    : size of fifo buffer, must be a power of 2
	   //           parent          :  parent proc directory to locate this proc file
	   //
	   //   returns: pointer to proc_fifo_t or NULL if an error occures
	 */
	proc_fifo_t *(*alloc_static) (const char *name,
				      proc_fifo_type_t pfType,
				      unsigned char *pFifo,
				      unsigned char *pBuf,
				      int size,
				      struct proc_dir_entry * parent);
	/*
	   // API : pf_im->free(id)
	   //
	   // purpose :free a proc_fifo proc file
	   //
	   // where:
	   //           id              : id of proc file
	   //
	   //   returns: 0
	 */
	int (*free) (proc_fifo_t * id);
	/* 
	   // API : pf_im->name_to_id(name)
	   //
	   // purpose :find a proc_fifo_t handel for a proc file name
	   //
	   // where:
	   //           name    : string pointer to the name of the proc file you want the id of
	   //
	   //   returns: pointer to proc_fifo_t or NULL if error
	 */
	proc_fifo_t *(*name_to_id) (const char *name);
	/* 
	   // API : pf_im->read(id,buf,count)
	   //
	   // purpose : read data from a proc_fifo buffer
	   //
	   // where:
	   //           id              : id of proc file
	   //           buf             : copy to buffer
	   //           count           : number of bytes that user wants to read
	   //
	   //   returns: number of bytes read or -1 if error
	 */
	int (*read) (proc_fifo_t * id, char *buf, unsigned int count);
	/* 
	   // API : pf_im->write(id,buf,count)
	   //
	   // purpose :write data to a proc_fifo buffer
	   //
	   // where:
	   //           id              : id of proc file
	   //           buffer  : copy from buffer
	   //           count           : number of bytes that user wants to write
	   //
	   //   returns: number of bytes writen or -1 if error
	 */
	int (*write) (proc_fifo_t * id,
		      const char *buf, unsigned int count);
	/* 
	   // API : pf_im->pf_printf(id,fmt, ...)
	   //
	   // purpose: format print data to a proc_fifo buffer
	   //
	   // where:
	   //           id              : id of proc file
	   //           fmt             : format string for printing data
	   //           ...             : arguments for format string
	   //
	   //   returns: number of bytes writen or -1 if error
	 */
	int (*pf_printf) (proc_fifo_t * id, const char *fmt, ...);
	/* 
	   // API : pf_im->reset(id)
	   //
	   // purpose : reset a proc_fifo buffer, flush all data and clear stats
	   //
	   // where:
	   //           id              : id of proc file
	   //
	   //   returns: 0 or -1 if error
	 */
	int (*reset) (proc_fifo_t * id);
	/* 
	   // API : pf_im->set_size(id,size)
	   //
	   // purpose : set the size of a proc_fifo buffer, this will also do a reset
	   //
	   // where:
	   //           id              : id of proc file
	   //   size            : size of fifo buffer, must be a power of 2
	   //
	   //   returns: 0 or -1 if error
	 */
	int (*set_size) (proc_fifo_t * id, unsigned int size);
	/* 
	   // API: pf_im->set_type(id,pfType)
	   //
	   // purpose : set the type of a proc_fifo buffer
	   //
	   // where:
	   //           id              : id of proc file
	   //           pfType  : Type of proc file (PF_FIFO | PF_FIFO_CIRCULAR)
	   //
	   //   returns: 0 or -1 if error
	 */
	int (*set_type) (proc_fifo_t * id, proc_fifo_type_t pfType);
	/* 
	   // API : pf_im->mem_size(id)
	   //
	   // purpose : get the size of a proc_fifo buffer
	   //
	   // where:
	   //           id              : id of proc file
	   //
	   //   returns: size of buffer or -1 if error
	 */
	int (*mem_size) (proc_fifo_t * id);
	/*
	   // API : pf_im->mem_used(id)
	   //
	   // purpose : get the amount of used memory in a proc_fifo buffer
	   //
	   // where:
	   //           id              : id of proc file
	   //
	   //   returns: used memory of buffer or -1 if error
	 */
	int (*mem_used) (proc_fifo_t * id);
	/* 
	   // API : pf_im->mem_used_max(id)
	   //
	   // purpose : get the maximum amount of used memory in a proc_fifo buffer
	   //
	   // where:
	   //           id              : id of proc file
	   //
	   //   returns: max used memory of buffer or -1 if error
	 */
	int (*mem_used_max) (proc_fifo_t * id);
	/* 
	   // API : pf_im->mem_free(id)
	   //
	   // purpose : get the amount of free memory in a proc_fifo buffer
	   //
	   // where:
	   //           id              : id of proc file
	   //
	   //   returns: free memory of buffer or -1 if error
	 */
	int (*mem_free) (proc_fifo_t * id);
	/* 
	   // API : pf_im->set_mode_readOnly(id)
	   //
	   // purpose : set a proc_fifo buffer to read on only mode, any future writes will not add data to the 
	   //                   fifo. The fifo can be put back into read/write mode by 2 methods. 
	   //                   1) The set_mode_readWrite is called
	   //                   2) The fifo is emptied either via a user space read or via kernel reads
	   //
	   // where:
	   //           id              : id of proc file
	   //
	   //   returns: 0 or -1 if error
	 */
	int (*set_mode_readOnly) (proc_fifo_t * id);
	/* 
	   // API : pf_im->set_mode_readWrite(id)
	   //
	   // purpose :set a proc_fifo buffer to read/write mode.
	   //
	   // where:
	   //           id              : id of proc file
	   //
	   //   returns: 0 or -1 if error
	 */
	int (*set_mode_readWrite) (proc_fifo_t * id);
} proc_fifo_im_data_t;

proc_fifo_t *pf_alloc(const char *name, proc_fifo_type_t pfType, int size,
		      struct proc_dir_entry *parent);
int pf_printf(proc_fifo_t * pf, const char *fmt, ...);

#endif
